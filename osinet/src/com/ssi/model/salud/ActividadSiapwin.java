package com.ssi.model.salud;

import com.ssi.model.salud.*;
import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
import com.ssi.persistence.cache.*;



import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;/**
* Clase que representa a una Actividad registrado por el Sistema Novahis para un
* Paciente determinado.
*/

public class ActividadSiapwin extends ActividadSalud {

			private String ve_ac_ref ;// 15 L
			private String ve_en_ref ;//15 L
			private String ve_ac_fecha ;// 10 YMD
			private String ve_ac_hora ;// 12 L
			private String ve_ac_servicio ;// 40 L
			private String ve_ac_descri ;// 255 L
			private String ve_ac_estado ;// 6 N
			private String ve_ac_tipo ;// 11 N
			private String ve_ac_coste ;// 8.3 N


/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase Actividad
* por defecto.
*/
	public ActividadSiapwin(){
		super();
	} // END OF Constructor Actividad



	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return ActividadSiapwin.getPersistenceManager();
	}

	public SistemaSalud sistema(){
		return SistemaSiapwin.getInstance();
	}


	public static String construirRestriccion(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento) {
		String unaRestriccion = null;
		if (rfc!=null && rfc.length() > 0){
			unaRestriccion = " codigo1 = '" +rfc+ "'";
			LogFile.log("ActividadSiapwin: construirRestriccion: WHERE 1: "+ unaRestriccion);
		}
		if (nombre!= null && nombre.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " nombre = '"+nombre+ "'";
				LogFile.log("ActividadSiapwin: construirRestriccion: WHERE 2.1: "+ unaRestriccion);
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" nombre = '"+nombre+"'";
				LogFile.log("ActividadSiapwin: construirRestriccion: WHERE 2.2: "+ unaRestriccion);
			}
		}
		if (unaRestriccion!=null ){
			unaRestriccion = " where "+unaRestriccion;
		} else{
			LogFile.log("ActividadSiapwin: construirRestriccion: unaRestriccion es null.");
			unaRestriccion = "";
		}
		LogFile.log("ActividadSiapwin: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion




	/**
	* M�todo que devuelve los Actividades
	*
	*@param
	*@return vector con Actividades
	*/
	public static Vector getActividad(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
									throws SQLException, Exception {

		String unaRestriccion = ActividadSiapwin.construirRestriccion(rfc,nombre,apellido1,apellido2,fechaNacimiento);

		LogFile.log("ActividadSiapwin: getActividad: WHERE-> "+unaRestriccion);

		Vector vActividad = ActividadSiapwin.instancesWithWhere(unaRestriccion);
		LogFile.log("ActividadSiapwin: getActividad: size: "+vActividad.size());

		if(vActividad == null){
			LogFile.log("ActividadSiapwin: getActividad: Vector NULO ");
			vActividad = new Vector();
		}



		return vActividad;
	} // END OF getActividad



	/**
	* M�todo qu� busca las actividades
	* Este m�todo devuelve el Vector Generado.
	*
	* @param unaRestriccion:Cadena de Caracteres qu� representa la restriccion (filtro) de la SQL-Query que busca los datos de los Episodios.
	* @return  Vector que contiene los datos de las actividades.
	*/
	public static Vector instancesWithWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
										ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		Vector vActividad = ActividadSiapwin.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.ActividadSiapwin"), unaRestriccion);
		if (vActividad == null){
		    LogFile.log("ActividadSiapwin: instancesWithWhere: No hay actividades.");
		    vActividad = new Vector();
		}
		return vActividad;
	} // END OF instancesWithWhere



	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return ActividadSiapwin.getPersistenceAccessor().persistenceManager();
	}


	public static PersistenceAccessor getPersistenceAccessor()
	{
		return ActividadSiapwinAccessor.getInstance();
	}


	public static Vector findAll(String unaRestriccion)
	{
	  try
	  {
	    Encuentro answer = null;
		Vector instances = ActividadSiapwin.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.ActividadSiapwin"), unaRestriccion);

	    return instances;

	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }

	}


	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return ActividadSiapwin.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.ActividadSiapwin"));
	}


	/**
	* M�todo qu� devuelve ve_ac_ref
	*
	* @return ve_ac_ref
	*/
	public String ve_ac_ref(){
		return ve_ac_ref;
	}

	/**
	* M�todo qu� setea ve_ac_ref
	*
	* @param unVe_ac_ref
	*/
	public void ve_ac_ref(String unVe_ac_ref){
		ve_ac_ref = unVe_ac_ref;
	}



	/**
	* M�todo qu� devuelve ve_en_ref
	*
	* @return ve_en_ref
	*/
	public String ve_en_ref(){
		return ve_en_ref;
	}

	/**
	* M�todo qu� setea ve_en_ref
	*
	* @param unVe_ep_ref
	*/
	public void ve_en_ref(String unVe_en_ref){
		ve_en_ref = unVe_en_ref;
	}


	/**
	* M�todo qu� devuelve ve_ac_fecha
	*
	* @return ve_ac_fecha
	*/
	public String ve_ac_fecha()
	{
		return ve_ac_fecha;
	}


	/**
	* M�todo qu� setea ve_ac_fecha
	*
	* @param unVe_ac_fecha
	*/
	public void ve_ac_fecha(String unVe_ac_fecha)
	{
		ve_ac_fecha = unVe_ac_fecha;
	}


	/**
	* M�todo qu� devuelve ve_ac_hora
	*
	* @return ve_ac_hora
	*/
	public String ve_ac_hora()
	{
		return ve_ac_hora;
	}


	/**
	* M�todo qu� setea ve_ac_hora
	*
	* @param unVe_ac_hora
	*/
	public void ve_ac_hora(String unVe_ac_hora)
	{
		ve_ac_hora = unVe_ac_hora;
	}


	/**
	* M�todo qu� devuelve ve_ac_servicio
	*
	* @return ve_ac_servicio
	*/
	public String ve_ac_servicio()
	{
		return ve_ac_servicio;
	}


	/**
	* M�todo qu� setea ve_ac_servicio
	*
	* @param unVe_ac_servicio
	*/
	public void ve_ac_servicio(String unVe_ac_servicio)
	{
		ve_ac_servicio = unVe_ac_servicio;
	}


	/**
	* M�todo qu� devuelve ve_ac_descri
	*
	* @return ve_ac_descri
	*/
	public String ve_ac_descri()
	{
		return ve_ac_descri;
	}


	/**
	* M�todo qu� setea ve_ac_descri
	*
	* @param unVe_ac_descri
	*/
	public void ve_ac_descri(String unVe_ac_descri)
	{
		ve_ac_descri = unVe_ac_descri;
	}


	/**
	* M�todo qu� devuelve ve_ac_estado
	*
	* @return ve_ac_estado
	*/
	public String ve_ac_estado()
	{
		return ve_ac_estado;
	}


	/**
	* M�todo qu� setea ve_ac_estado
	*
	* @param unVe_ac_estado
	*/
	public void ve_ac_estado(String unVe_ac_estado)
	{
		ve_ac_estado = unVe_ac_estado;
	}



	/**
	* M�todo qu� devuelve ve_ac_tipo
	*
	* @return ve_ac_tipo
	*/
	public String ve_ac_tipo()
	{
		return ve_ac_tipo;
	}


	/**
	* M�todo qu� setea ve_ac_tipo
	*
	* @param unVe_ac_tipo
	*/
	public void ve_ac_tipo(String unVe_ac_tipo)
	{
		ve_ac_tipo = unVe_ac_tipo;
	}

	
	/**
	* M�todo qu� devuelve ve_ac_coste
	*
	* @return ve_ac_coste
	*/
	public String ve_ac_coste()
	{
		return ve_ac_coste;
	}


	/**
	* M�todo qu� setea ve_ac_coste
	*
	* @param unVe_ac_coste
	*/
	public void ve_ac_coste(String unVe_ac_coste)
	{
		ve_ac_coste = unVe_ac_coste;
	}


	public String oid()
	{
		return ve_ac_ref;
	}

	public void oid(String unCodigo)
	{
		ve_ac_ref = unCodigo;
	}




/**
 * delete method comment.
 */
public void delete() throws java.lang.NoSuchMethodException, java.sql.SQLException, java.lang.ClassNotFoundException {}/**
 * Insert the method's description here.
 * Creation date: (15/03/01 12:54:25)
 * @return java.lang.String
 */
public java.lang.String getTipoActividad() {

	return ActividadSalud.getTipoActividad(this.ve_ac_tipo());
		
}/**
 * isPersistent method comment.
 */
public boolean isPersistent() {
	return false;
}/**
 * save method comment.
 */
public void save() throws java.lang.InstantiationException, java.lang.ClassNotFoundException, java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.NoSuchMethodException, java.sql.SQLException {}/**
 * Insert the method's description here.
 * Creation date: (15/03/01 12:54:25)
 * @param newTipoActividad java.lang.String
 */
public void setTipoActividad(java.lang.String newTipoActividad) {}} // END OF CLASS ActividadSiapwin