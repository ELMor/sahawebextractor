package com.ssi.model.salud;

import com.ssi.persistence.model.*;
import com.ssi.model.salud.*;
/**
* Clase que representa a un Episodio registrado por el Sistema Siapwin para un
* Paciente determinado.
*/

public abstract class SexoSalud extends ReadPersistentObject {

	private String codigo ;
	private String descripcion ;
	
	public String oid()
	{
		return codigo;
	}
	
	public void oid(String anOID)
	{
		this.codigo = anOID;
	}

	public String descripcion()
	{
		return descripcion;
	}

	public void descripcion(String aDescripcion)
	{
		this.descripcion = aDescripcion;
	}

}