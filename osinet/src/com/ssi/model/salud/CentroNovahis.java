package com.ssi.model.salud;

import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
/**
* Clase que representa a un Episodio registrado por el Sistema Siapwin para un
* Paciente determinado.
*/
public class CentroNovahis extends CentroSalud {
	/**
	* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase SexoNovahis con Valores
	* por defecto.
	*/
	public CentroNovahis(){
		super();
	} // END OF Constructor SexoNovahis

	public PersistenceManager persistenceManager() throws ClassNotFoundException {
	    return CentroNovahis.getPersistenceManager();
	}

	public static PersistenceManager getPersistenceManager() throws ClassNotFoundException {
		//Answers the persistence manager for the receiver
		return CentroNovahis.getPersistenceAccessor().persistenceManager();
	}

	public static PersistenceAccessor getPersistenceAccessor() {
		return SexoNovahisAccessor.getInstance();
	}
} // END OF Class CentroNovahis