package com.ssi.model.salud;

import java.util.Vector;
import com.ssi.persistence.cache.*;
import com.ssi.persistence.model.*;

public class PacienteNovahisAccessor extends PersistenceAccessor
{


	public static String className()
	{
		return "com.ssi.model.salud.PacienteNovahisAccessor";
	}

	public static PersistenceAccessor getInstance()
	{
	    return AccessorCache.getInstance().get(className());
	}
	
	public static PersistenceAccessor getInstance(PersistenceManager aManager)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new PacienteNovahisAccessor(aManager);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new PacienteNovahisAccessor(aManager, aCache);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}
	
	protected PacienteNovahisAccessor(PersistenceManager aManager)
	{
		super (aManager);
	}

	protected PacienteNovahisAccessor(PersistenceManager aManager, IPersistenceCache aCache)
	{
		super (aManager, aCache);
	}
	
	
	public  Vector addPersistentFields( Vector aAnswer )
	{
		aAnswer.addElement(new PersistentField("codigo_cliente", "oid", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("apellido1", "apellido1", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("apellido2", "apellido2", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("nombre", "nombre", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("nac_fecha", "fechaNacimiento", 1, new DBDateFieldType()));
		aAnswer.addElement(new PersistentField("codigo1", "RFC", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("telefono1", "telefono1", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("codigo_sexo", "codigo_sexo", 1, new DBFixReferenceFieldType(SexoNovahisAccessor.getInstance())));
		return aAnswer;
	}
	
	public Vector collectionSpecs()
	{
		return new Vector();
	}
	public String oidPrefix()
	{
		return "";
	}
	public String persistentClassName()
	{
		return "com.ssi.model.salud.PacienteNovahis";
	}
	public String tableName()
	{
		return "clientes";
	}
}