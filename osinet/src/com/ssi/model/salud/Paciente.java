package com.ssi.model.salud;

import java.awt.List.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import com.ssi.bean.salud.*;
import com.ssi.persistence.model.*;

public class Paciente 
{
	public String sexoDescripcion()
	{
		return "";
	}

	private PacienteNovahis pacienteNovahis;
	private PacienteSiapwin pacienteSiapwin;
	
	/**
	* Constructor
	*
	*/
	public Paciente() {
	}

	/**
	* Answers the persistence manager for the receiver. Since the
	* receiver is not mapped to a table, then answer null.
	*/
	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return null;
	}


	/**
	* Este m�todo devuelve los pacientes Siapwin y Novahis.
	*
	*@param rfc,nombre,apellido1,apellido2,fechaNacimiento
	*@return vector de pacientes Siapwin y Novahis.
	*/
	public static Vector getPacientes(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
														throws SQLException, Exception {

	 LogFile.log("Paciente: getPacientes: Inicio: ");

		
		Vector vPacientesUnificados = new Vector();
		Vector vPacientesNovahis    = new Vector();
		Vector vPacientesSiapwin    = new Vector();

		
		//Obtengo pacientes Siapwin
		vPacientesSiapwin = Paciente.getPacientesSiapwin(rfc,nombre,apellido1,apellido2,fechaNacimiento);
		LogFile.log("Paciente: getPacientes: largo de Vector Siapwin: "+vPacientesSiapwin.size());
		
		//Obtengo pacientes Novahis
		vPacientesNovahis = Paciente.getPacientesNovahis(rfc,nombre,apellido1,apellido2,fechaNacimiento);
		LogFile.log("Paciente: getPacientes: largo de Vector Novahis: "+vPacientesNovahis.size());
		
		
		//Obtengo Pacientes unificados
		vPacientesUnificados = unificarPacientes(vPacientesSiapwin, vPacientesNovahis);

		
		LogFile.log("Paciente: getPacientes: Fin: ");
		return vPacientesUnificados;

	} //END OF getPacientes
	


	/**
	* Este m�todo devuelve los pacientes Siapwin.
	*
	*@param rfc,nombre,apellido1,apellido2,fechaNacimiento
	*@return vector de pacientes Siapwin .
	*/
	public static Vector getPacientesSiapwin(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
										throws SQLException, Exception {

	   Vector vPacientesSiapwin = new Vector();

	   vPacientesSiapwin = PacienteSiapwin.getPacientes(rfc,nombre,apellido1,apellido2,fechaNacimiento);
		
		if (!(vPacientesSiapwin.isEmpty())){
			return vPacientesSiapwin;
		}else{
			LogFile.log("Paciente: getPacientesSiapwin: El vector volvio nulo.");
			vPacientesSiapwin = new Vector();
			return vPacientesSiapwin;
		}
	} // END OF getPacientesSiapwin


	
	/**
	* Este m�todo devuelve los pacientes Novahis.
	*
	*@param rfc,nombre,apellido1,apellido2,fechaNacimiento
	*@return vector de pacientes Novahis .
	*/
	public static Vector getPacientesNovahis(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
										throws SQLException, Exception {
	  
	   Vector vPacientesNovahis = new Vector();
	  
	   vPacientesNovahis = PacienteNovahis.getPacientes(rfc,nombre,apellido1,apellido2,fechaNacimiento);
	   
		if (!(vPacientesNovahis.isEmpty())){
			return vPacientesNovahis;
		}else{
			LogFile.log("Paciente: getPacientesNovahis: El vector volvio nulo.");
			vPacientesNovahis = new Vector();
			return vPacientesNovahis;
		}
	} // END OF getPacientesNovahis


	
	/**
	* M�todo est�tico qu� recibe dos listas de Pacientes para construir una s�la con los datos
	* de ambas en forma ordenada.
	* 
	*/
	public static Vector unificarPacientes(Vector pacientesSiapwin, Vector pacientesNovahis){
		Vector pacientes = new Vector();

		//Arma el vector unificado con los pacientes Siapwin 
	 	int medidaVectorSiapwin = pacientesSiapwin.size();
	    if (medidaVectorSiapwin > 0) {
			for (int i = 0; i<medidaVectorSiapwin; i++) {
				PacienteSiapwin unPaciente = (PacienteSiapwin) pacientesSiapwin.elementAt(i);
		        pacientes.addElement(unPaciente); 
			} 
  	    }
  	
		//Arma el vector unificado con los pacientes Novahis
	   	int medidaVectorNovahis = pacientesNovahis.size();
	    if (medidaVectorNovahis > 0) 
	    {
			for (int i = 0; i<medidaVectorNovahis; i++)
			{
				PacienteNovahis unPacienteSiap = (PacienteNovahis) pacientesNovahis.elementAt(i);
		        pacientes.addElement(unPacienteSiap); 
			} 
  	    } 
		
		return pacientes;
	};


	public PacienteSiapwin pacienteSiapwin()
	{
		return pacienteSiapwin;
	}
	
	
	public PacienteNovahis pacienteNovahis()
	{
		return pacienteNovahis;
	}




} //END OF CLASS Paciente()