package com.ssi.model.salud;

import com.ssi.model.salud.*;
import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
import com.ssi.persistence.cache.*;



/**
* Clase que representa a un Episodio registrado por el Sistema Siapwin para un
* Paciente determinado.
*/

public class Episodio extends EpisodioSalud {

			private String ve_ep_ref        ;// 15 L
			private String ve_ep_fecha      ;// 10 YMD
			private String ve_ep_hora       ;// 12 L
			private String ve_ep_servicio   ;//40 L
			private String ve_ep_cond       ;// 120 L
			private String ve_ep_estado     ;// 6 N
			private String ve_ep_idclient   ;// 15 L
			private String ve_sev_pk        ;// 6 N
			private String ve_fus_pk        ;// 11 N
			private String ve_cond_pk       ;// 12 L
			private String ve_ep_fechaend   ;// 10 YMD
			private String ve_ep_resolucion ;// 120 L
			private String ve_enc_apert     ;// 15 L
			private String ve_ep_icd_cod    ;// 12 L
			private String ve_ep_fecha_clin ;// 10 YMD
			private String ve_ep_hora_clin  ;// 12 L
			private String ve_ep_etiq       ;// 12 L


/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase Episodio
* por defecto.
*/
	public Episodio(){
		super();
	} // END OF Constructor Episodio

	public String centroDescripcion(){
		return "";
	}

	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return Episodio.getPersistenceManager();
	}

	public SistemaSalud sistema(){
		return SistemaNovahis.getInstance();
	}

/**
* M�todo que recibe un Vector con los Pacientes que han sido seleccionados de la lista de 
* Pacientes mostrados en la Pantalla Busqueda de Pacientes.
* 
* IMPORTANTE: PARA ESTE CASO SE ASUME QU� LOS PACIENTES SETEADOS EN EL VECTOR DE PACIENTES
* SON EL MISMO PACIENTE (PROVENIENTES DE SIAPWIN � NOVAHIS).
*
* @param pacientes    Vector de Pacientes qu� ha sido seteado desde la Pantalla de B�squeda
*                     de Pacientes y que contiene los identificadores �nicos de los Pacientes.
*/
	public static String construirRestriccion(Vector pacientes) {
		String unaRestriccion = null;
		String idPaciente     = null;
		if(pacientes.size() > 0){
			idPaciente = (String) pacientes.elementAt(0);
		}
		if (idPaciente!=null && idPaciente.length()>0){
			unaRestriccion = " WHERE ve_ep_idclient = '" +idPaciente+ "'";
			LogFile.log("Episodio: construirRestriccion: WHERE : "+ unaRestriccion);
		}
		LogFile.log("Episodio: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion

   /**
	* M�todo que devuelve los episodios
	*
	*@param
	*@return vector con episodios
	*/

	public static Vector getEpisodios(Vector pacientes) { 

	 Vector vEpisodiosSiapwin = new Vector();
 	 Vector vEpisodiosNovahis = new Vector();

	 try{
		 
	    for(int i = 0; i < pacientes.size(); i++)
		{
			 if(pacientes.elementAt(i) instanceof PacienteNovahis)
			{
				LogFile.log("Episodio: getEpisodios: es Novahis");
				String unaRestriccion = " WHERE ve_ep_idclient='" + ((PacienteNovahis)pacientes.elementAt(i)).codigoCliente()+ "'";
				vEpisodiosNovahis = EpisodioNovahis.instancesWithWhere(unaRestriccion);
			}
			else
			{
				LogFile.log("Episodio: getEpisodios: es Siapwin");
				String unaRestriccion = " WHERE ve_ep_idclient='" + ((PacienteSiapwin)pacientes.elementAt(i)).codigoCliente()+ "'";
				vEpisodiosSiapwin = EpisodioSiapwin.instancesWithWhere(unaRestriccion);
			}
		 }

	 	 LogFile.log("Episodio: getEpisodios Novahis: size: " + vEpisodiosNovahis.size());
		 LogFile.log("Episodio: getEpisodios Siapwin: size: " + vEpisodiosSiapwin.size());
	   }
	   catch(Exception e){
		   LogFile.log(e);
	   }	   
		
	   return unificarEpisodios(vEpisodiosNovahis,vEpisodiosSiapwin);	

	} // END OF getEpisodios

	/**
	* M�todo qu� busca los episodios 
	* Este m�todo devuelve el Vector Generado.
	*
	* @param unaRestriccion:Cadena de Caracteres qu� representa la restriccion (filtro) de la SQL-Query que busca los datos de los Episodios.
	* @return  Vector que contiene los datos de los Episodios.
	*/
	public static Vector instancesWithWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
										ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		Vector vEpisodio = Episodio.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.Episodio"), unaRestriccion);
		if (vEpisodio == null){
			LogFile.log("Episodio: instancesWithWhere: Vector NULO - No hay Episodios");
		    vEpisodio = new Vector();
		}
		return vEpisodio;
	} // END OF instancesWithWhere



	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return Episodio.getPersistenceAccessor().persistenceManager();
	}


	public static PersistenceAccessor getPersistenceAccessor()
	{
		return EpisodioAccessor.getInstance();
	}


	public static Vector findAll(String unaRestriccion)
	{
	  try
	  {
	    Episodio answer = null;
		Vector instances = Episodio.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.Episodio"), unaRestriccion);

	    return instances;

	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }

	}


	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return Episodio.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.Episodio"));
	}


	/**
	* M�todo qu� devuelve ve_ep_ref 
	*
	* @return ve_ep_ref
	*/
	public String ve_ep_ref(){
		return ve_ep_ref;
	}


	/**
	* M�todo qu� setea ve_ep_ref 
	*
	* @param unVe_ep_ref
	*/
	public void ve_ep_ref(String unVe_ep_ref){
		ve_ep_ref = unVe_ep_ref;
	}




	/**
	* M�todo qu� devuelve ve_ep_fecha 
	*
	* @return ve_ep_fecha
	*/
	public String ve_ep_fecha()
	{
		return ve_ep_fecha;
	}


	/**
	* M�todo qu� setea ve_ep_fecha 
	*
	* @param unVe_ep_fecha
	*/
	public void ve_ep_fecha(String unVe_ep_fecha)
	{
		ve_ep_fecha = unVe_ep_fecha;
	}


	/**
	* M�todo qu� devuelve ve_ep_hora 
	*
	* @return ve_ep_hora
	*/
	public String ve_ep_hora()
	{
		return ve_ep_hora;
	}


	/**
	* M�todo qu� setea ve_ep_hora 
	*
	* @param unVe_ep_hora
	*/
	public void ve_ep_hora(String unVe_ep_hora)
	{
		ve_ep_hora = unVe_ep_hora;
	}



	/**
	* M�todo qu� devuelve ve_ep_servicio 
	*
	* @return ve_ep_servicio
	*/
	public String ve_ep_servicio()
	{
		return ve_ep_servicio;
	}


	/**
	* M�todo qu� setea ve_ep_servicio 
	*
	* @param unVe_ep_servicio
	*/
	public void ve_ep_servicio(String unVe_ep_servicio)
	{
		ve_ep_servicio = unVe_ep_servicio;
	}



	/**
	* M�todo qu� devuelve ve_ep_cond 
	*
	* @return ve_ep_cond
	*/
	public String ve_ep_cond()
	{
		return ve_ep_cond;
	}


	/**
	* M�todo qu� setea ve_ep_cond 
	*
	* @param unVe_ep_cond
	*/
	public void ve_ep_cond(String unVe_ep_cond)
	{
		ve_ep_cond = unVe_ep_cond;
	}


	/**
	* M�todo qu� devuelve ve_ep_estado 
	*
	* @return ve_ep_estado
	*/
	public String ve_ep_estado()
	{
		return ve_ep_estado;
	}


	/**
	* M�todo qu� setea ve_ep_estado 
	*
	* @param unVe_ep_estado
	*/
	public void ve_ep_estado(String unVe_ep_estado)
	{
		ve_ep_estado = unVe_ep_estado;
	}


	/**
	* M�todo qu� devuelve ve_ep_idclient 
	*
	* @return ve_ep_idclient
	*/
	public String ve_ep_idclient()
	{
		return ve_ep_idclient;
	}


	/**
	* M�todo qu� setea ve_ep_idclient 
	*
	* @param unVe_ep_idclient
	*/
	public void ve_ep_idclient(String unVe_ep_idclient)
	{
		ve_ep_idclient = unVe_ep_idclient;
	}

	/**
	* M�todo qu� devuelve ve_sev_pk 
	*
	* @return ve_sev_pk
	*/
	public String ve_sev_pk()
	{
		return ve_sev_pk;
	}


	/**
	* M�todo qu� setea ve_sev_pk 
	*
	* @param unVe_sev_pk
	*/
	public void ve_sev_pk(String unVe_sev_pk)
	{
		ve_sev_pk = unVe_sev_pk;
	}

	/**
	* M�todo qu� devuelve ve_fus_pk 
	*
	* @return ve_fus_pk
	*/
	public String ve_fus_pk()
	{
		return ve_fus_pk;
	}


	/**
	* M�todo qu� setea ve_fus_pk 
	*
	* @param unVe_fus_pk
	*/
	public void ve_fus_pk(String unVe_fus_pk)
	{
		ve_fus_pk = unVe_fus_pk;
	}

 
	/**
	* M�todo qu� devuelve ve_cond_pk 
	*
	* @return ve_cond_pk
	*/
	public String ve_cond_pk()
	{
		return ve_cond_pk;
	}


	/**
	* M�todo qu� setea ve_cond_pk 
	*
	* @param unVe_cond_pk
	*/
	public void ve_cond_pk(String unVe_cond_pk)
	{
		ve_cond_pk = unVe_cond_pk;
	}
	
	/**
	* M�todo qu� devuelve ve_ep_fechaend 
	*
	* @return ve_ep_fechaend
	*/
	public String ve_ep_fechaend()
	{
		return ve_ep_fechaend;
	}


	/**
	* M�todo qu� setea ve_ep_fechaend 
	*
	* @param unVe_ep_fechaend
	*/
	public void ve_ep_fechaend(String unVe_ep_fechaend)
	{
		ve_ep_fechaend = unVe_ep_fechaend;
	}
	
	
	/**
	* M�todo qu� devuelve ve_ep_resolucion 
	*
	* @return ve_ep_resolucion
	*/
	public String ve_ep_resolucion()
	{
		return ve_ep_resolucion;
	}


	/**
	* M�todo qu� setea ve_ep_resolucion 
	*
	* @param unVe_ep_resolucion
	*/
	public void ve_ep_resolucion(String unVe_ep_resolucion)
	{
		ve_ep_resolucion = unVe_ep_resolucion;
	}
	

	/**
	* M�todo qu� devuelve ve_enc_apert 
	*
	* @return ve_enc_apert
	*/
	public String ve_enc_apert()
	{
		return ve_enc_apert;
	}


	/**
	* M�todo qu� setea ve_enc_apert 
	*
	* @param unVe_enc_apert
	*/
	public void ve_enc_apert(String unVe_enc_apert)
	{
		ve_enc_apert = unVe_enc_apert;
	}


	/**
	* M�todo qu� devuelve ve_ep_icd_cod 
	*
	* @return ve_ep_icd_cod
	*/
	public String ve_ep_icd_cod()
	{
		return ve_ep_icd_cod;
	}


	/**
	* M�todo qu� setea ve_ep_icd_cod 
	*
	* @param unVe_ep_icd_cod
	*/
	public void ve_ep_icd_cod(String unVe_ep_icd_cod)
	{
		ve_ep_icd_cod = unVe_ep_icd_cod;
	}

	
	/**
	* M�todo qu� devuelve ve_ep_fecha_clin 
	*
	* @return ve_ep_fecha_clin
	*/
	public String ve_ep_fecha_clin()
	{
		return ve_ep_fecha_clin;
	}


	/**
	* M�todo qu� setea ve_ep_fecha_clin 
	*
	* @param unVe_ep_fecha_clin
	*/
	public void ve_ep_fecha_clin(String unVe_ep_fecha_clin)
	{
		ve_ep_fecha_clin = unVe_ep_fecha_clin;
	}

	
	/**
	* M�todo qu� devuelve ve_ep_hora_clin 
	*
	* @return ve_ep_hora_clin
	*/
	public String ve_ep_hora_clin()
	{
		return ve_ep_hora_clin;
	}


	/**
	* M�todo qu� setea ve_ep_hora_clin 
	*
	* @param unVe_ep_hora_clin
	*/
	public void ve_ep_hora_clin(String unVe_ep_hora_clin)
	{
		ve_ep_hora_clin = unVe_ep_hora_clin;
	}


	/**
	* M�todo qu� devuelve ve_ep_etiq 
	*
	* @return ve_ep_etiq
	*/
	public String ve_ep_etiq()
	{
		return ve_ep_etiq;
	}


	/**
	* M�todo qu� setea ve_ep_etiq 
	*
	* @param unVe_ep_etiq
	*/
	public void ve_ep_etiq(String unVe_ep_etiq)
	{
		ve_ep_etiq = unVe_ep_etiq;
	}



	public String oid()
	{
		return ve_ep_ref;
	}

	public void oid(String unCodigo)
	{
		ve_ep_ref = unCodigo;
	}





/**
 * delete method comment.
 */
public void delete() throws java.lang.NoSuchMethodException, java.sql.SQLException, java.lang.ClassNotFoundException {}/**

 * Insert the method's description here.
 * Creation date: (15/03/01 16:18:33)
 * @return java.lang.String
 */
public java.lang.String getDiagnostico() {
	return null;
}
/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:18:33)
 * @return java.lang.String
 */
public java.lang.String getServicio() {
	return null;
}



/**
 * isPersistent method comment.
 */
public boolean isPersistent() {
	return false;
}/**
 * save method comment.
 */
public void save() throws java.lang.InstantiationException, java.lang.ClassNotFoundException, java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.NoSuchMethodException, java.sql.SQLException {}/**
 * Insert the method's description here.
 * Creation date: (16/02/01 11:24:55)
 * @return java.util.Vector
 */
public static Vector unificarEpisodios(Vector v1, Vector v2) {
	
	Enumeration e = v2.elements();

	while(e.hasMoreElements())
		v1.addElement(e.nextElement());
		
	return v1;
}} // END OF CLASS Episodio