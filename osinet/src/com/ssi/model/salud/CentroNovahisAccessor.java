package com.ssi.model.salud;

import java.util.Vector;
import com.ssi.persistence.cache.*;
import com.ssi.persistence.model.*;

public class CentroNovahisAccessor extends PersistenceAccessor{

	protected CentroNovahisAccessor(PersistenceManager aManager){
		super (aManager);
	} // END OF method 

	protected CentroNovahisAccessor(PersistenceManager aManager, IPersistenceCache aCache){
		super (aManager, aCache);
	} // END OF method 

	public static String className(){
		return "com.ssi.model.salud.CentroNovahisAccessor";
	}  // END OF method className

	public static PersistenceAccessor getInstance()	{
	    return AccessorCache.getInstance().get(className());
	} // END OF method getInstance
	
	public static PersistenceAccessor getInstance(PersistenceManager aManager){
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null){
			answer = new CentroNovahisAccessor(aManager);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	} // END OF method getInstance

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache){
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null){
			answer = new CentroNovahisAccessor(aManager, aCache);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	} // END OF method 

	public  Vector addPersistentFields( Vector aAnswer ){
		aAnswer.addElement(new PersistentField("cod_centro", "oid", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("nombre_centro", "descripcion", 1, new DBStringFieldType()));
		return aAnswer;
	} // END OF method 

	public Vector collectionSpecs(){
		return new Vector();
	} // END OF method 

	public String oidPrefix() {
		return "";
	} // END OF method 

	public String persistentClassName(){
		return "com.ssi.model.salud.CentroNovahis";
	} // END OF method 

	public String tableName(){
		return "centros";
	} // END OF method 
} // END OF Class CentroNovahisAccessor