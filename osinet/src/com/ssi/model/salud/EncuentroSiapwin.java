package com.ssi.model.salud;

import com.ssi.model.salud.*;
import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
import com.ssi.persistence.cache.*;



/**
* Clase que representa a un Encuentro registrado por el Sistema Siapwin para un
* Paciente determinado.
*/

public class EncuentroSiapwin extends EncuentroSalud {

			private String ve_en_ref      ;// 15 L
			private String ve_ep_ref      ;// 15 L
			private String ve_en_fecha    ;// 10 YMD
			private String ve_en_hora     ;// 12 L
			private String ve_en_servicio ;// 40 L
			private String ve_en_motivo   ;// 120 L
			private String ve_en_estado   ;// 6 N
			private String ve_en_tipo     ;// 6 N
			private String ve_en_idclient ;// 15 L
			private String ve_en_icd_cod  ;// 12 L


/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase Encuentro
* por defecto.
*/
	public EncuentroSiapwin(){
		super();
	} // END OF Constructor Encuentro



	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return EncuentroSiapwin.getPersistenceManager();
	}

	public SistemaSalud sistema(){
		return SistemaSiapwin.getInstance();
	}


	public static String construirRestriccion(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento) {
		String unaRestriccion = null;
		if (rfc!=null && rfc.length() > 0){
			unaRestriccion = " codigo1 = '" +rfc+ "'";
			LogFile.log("EncuentroSiapwin: construirRestriccion: WHERE 1: "+ unaRestriccion);
		}
		if (nombre!= null && nombre.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " nombre = '"+nombre+ "'";
				LogFile.log("EncuentroSiapwin: construirRestriccion: WHERE 2.1: "+ unaRestriccion);
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" nombre = '"+nombre+"'";
				LogFile.log("EncuentroSiapwin: construirRestriccion: WHERE 2.2: "+ unaRestriccion);
			}
		}
		if (unaRestriccion!=null ){
			unaRestriccion = " where "+unaRestriccion;
		} else{
			LogFile.log("EncuentroSiapwin: construirRestriccion: unaRestriccion es null.");
			unaRestriccion = "";
		}
		LogFile.log("EncuentroSiapwin: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion




	/**
	* M�todo que devuelve los encuentros
	*
	*@param
	*@return vector con encuentros
	*/
	public static Vector getEncuentro(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
									throws SQLException, Exception {

		String unaRestriccion = EncuentroSiapwin.construirRestriccion(rfc,nombre,apellido1,apellido2,fechaNacimiento);

		LogFile.log("EncuentroSiapwin: getEpisodios: WHERE-> "+unaRestriccion);

		Vector vEncuentro = EncuentroSiapwin.instancesWithWhere(unaRestriccion);
		LogFile.log("EncuentroSiapwin: getEncuentro: size: "+vEncuentro.size());

		if(vEncuentro == null){
			LogFile.log("EncuentroSiapwin: getEncuentro: Vector NULO ");
			vEncuentro = new Vector();
		}



		return vEncuentro;
	} // END OF getEncuentro



	/**
	* M�todo qu� busca los encuentros
	* Este m�todo devuelve el Vector Generado.
	*
	* @param unaRestriccion:Cadena de Caracteres qu� representa la restriccion (filtro) de la SQL-Query que busca los datos de los Episodios.
	* @return  Vector que contiene los datos de los encuentros.
	*/
	public static Vector instancesWithWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
										ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		Vector vEncuentro = EncuentroSiapwin.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.EncuentroSiapwin"), unaRestriccion);
		if (vEncuentro == null){
		    LogFile.log("EncuentroSiapwin: instancesWithWhere: No hay encuentros.");
		    vEncuentro = new Vector();
		}
		return vEncuentro;
	} // END OF instancesWithWhere



	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return EncuentroSiapwin.getPersistenceAccessor().persistenceManager();
	}


	public static PersistenceAccessor getPersistenceAccessor()
	{
		return EncuentroSiapwinAccessor.getInstance();
	}


	public static Vector findAll(String unaRestriccion)
	{
	  try
	  {
	    Encuentro answer = null;
		Vector instances = EncuentroSiapwin.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.EncuentroSiapwin"), unaRestriccion);

	    return instances;

	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }

	}


	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return EncuentroSiapwin.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.EncuentroSiapwin"));
	}


	/**
	* M�todo qu� devuelve ve_en_ref
	*
	* @return ve_en_ref
	*/
	public String ve_en_ref(){
		return ve_en_ref;
	}

	/**
	* M�todo qu� setea ve_en_ref
	*
	* @param unVe_ep_ref
	*/
	public void ve_en_ref(String unVe_en_ref){
		ve_en_ref = unVe_en_ref;
	}

	/**
	* M�todo qu� devuelve la fecha y hora de inicio del Encuentro como una cadena de
	* caracteres.
	*
	* @return ve_en_fecha
	*/
	public String fechayHoraInicio(){
		
		String campoFecha=null;
  
		   if(fechaInicio()!=null){
			 java.sql.Date date = java.sql.Date.valueOf(fechaInicio());
			 java.text.DateFormat myformat = new java.text.SimpleDateFormat("dd/MM/yyyy");  
			 campoFecha = myformat.format(date)+" "+ this.horaInicio();
			}
			else{ 
			 campoFecha = Parametros.sinDatos;
			}  
		
		return campoFecha;
	}

	/**
	* M�todo qu� devuelve ve_en_fecha
	*
	* @return ve_en_fecha
	*/
	public String fechaInicio(){
		return ve_en_fecha;
	}

	/**
	* M�todo qu� setea ve_en_fecha
	*
	* @param unVe_en_fecha
	*/
	public void fechaInicio(String unaFechaInicio){
		ve_en_fecha = unaFechaInicio;
	}

	/**
	* M�todo qu� devuelve ve_en_hora
	*
	* @return ve_en_hora
	*/
	public String horaInicio()
	{
		return ve_en_hora;
	}


	/**
	* M�todo qu� setea ve_en_hora
	*
	* @param unVe_en_hora
	*/
	public void horaInicio(String unahoraInicio)
	{
		ve_en_hora = unahoraInicio;
	}

	/**
	* M�todo qu� devuelve ve_ep_ref
	*
	* @return ve_ep_ref
	*/
	public String ve_ep_ref()
	{
		return ve_ep_ref;
	}


	/**
	* M�todo qu� setea ve_ep_ref
	*
	* @param unVe_ep_ref
	*/
	public void ve_ep_ref(String unVe_ep_ref)
	{
		ve_ep_ref = unVe_ep_ref;
	}

	/**
	* M�todo qu� devuelve ve_en_servicio
	*
	* @return ve_en_servicio
	*/
	public String ve_en_servicio()
	{
		return ve_en_servicio;
	}


	/**
	* M�todo qu� setea ve_en_servicio
	*
	* @param unVe_en_servicio
	*/
	public void ve_en_servicio(String unVe_en_servicio)
	{
		ve_en_servicio = unVe_en_servicio;
	}





	/**
	* M�todo qu� devuelve ve_en_motivo
	*
	* @return ve_en_motivo
	*/
	public String ve_en_motivo()
	{
		return ve_en_motivo;
	}


	/**
	* M�todo qu� setea ve_en_motivo
	*
	* @param unVe_en_motivo
	*/
	public void ve_en_motivo(String unVe_en_motivo)
	{
		ve_en_motivo = unVe_en_motivo;
	}






	/**
	* M�todo qu� devuelve ve_en_estado
	*
	* @return ve_en_estado
	*/
	public String ve_en_estado()
	{
		return ve_en_estado;
	}


	/**
	* M�todo qu� setea ve_en_estado
	*
	* @param unVe_en_estado
	*/
	public void ve_en_estado(String unVe_en_estado)
	{
		ve_en_estado = unVe_en_estado;
	}



	/**
	* M�todo qu� devuelve ve_en_idclient
	*
	* @return ve_en_idclient
	*/
	public String ve_en_idclient()
	{
		return ve_en_idclient;
	}


	/**
	* M�todo qu� setea ve_en_idclient
	*
	* @param unVe_en_idclient
	*/
	public void ve_en_idclient(String unVe_en_idclient)
	{
		ve_en_idclient = unVe_en_idclient;
	}


	/**
	* M�todo qu� devuelve ve_en_tipo
	*
	* @return ve_en_tipo
	*/
	public String ve_en_tipo()
	{
		return ve_en_tipo;
	}


	/**
	* M�todo qu� setea ve_en_tipo
	*
	* @param unVe_en_tipo
	*/
	public void ve_en_tipo(String unVe_en_tipo)
	{
		ve_en_tipo = unVe_en_tipo;
	}



	/**
	* M�todo qu� devuelve ve_en_icd_cod
	*
	* @return ve_en_icd_cod
	*/
	public String ve_en_icd_cod()
	{
		return ve_en_icd_cod;
	}


	/**
	* M�todo qu� setea ve_en_icd_cod
	*
	* @param unVe_en_icd_cod
	*/
	public void ve_en_icd_cod(String unVe_en_icd_cod)
	{
		ve_en_icd_cod = unVe_en_icd_cod;
	}



	public String oid()
	{
		return ve_en_ref;
	}

	public void oid(String unCodigo)
	{
		ve_en_ref = unCodigo;
	}




/**
 * delete method comment.
 */
public void delete() throws java.lang.NoSuchMethodException, java.sql.SQLException, java.lang.ClassNotFoundException {}/**
 * Insert the method's description here.
 * Creation date: (19/02/01 09:19:14)
 * @return java.lang.String
 */
public java.lang.String idEncuentro() {
	 return ve_en_ref;
}/**
 * isPersistent method comment.
 */
public boolean isPersistent() {
	return false;
}/**
 * save method comment.
 */
public void save() throws java.lang.InstantiationException, java.lang.ClassNotFoundException, java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.NoSuchMethodException, java.sql.SQLException {}} // END OF CLASS EncuentroSiapwin