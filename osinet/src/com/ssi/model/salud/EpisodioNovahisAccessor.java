package com.ssi.model.salud;

import java.util.Vector;
import com.ssi.persistence.cache.*;
import com.ssi.persistence.model.*;

public class EpisodioNovahisAccessor extends PersistenceAccessor
{


	public static String className()
	{
		return "com.ssi.model.salud.EpisodioNovahisAccessor";
	}

	public static PersistenceAccessor getInstance()
	{
	    return AccessorCache.getInstance().get(className());
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new EpisodioNovahisAccessor(aManager);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new EpisodioNovahisAccessor(aManager, aCache);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	protected EpisodioNovahisAccessor(PersistenceManager aManager)
	{
		super (aManager);
	}

	protected EpisodioNovahisAccessor(PersistenceManager aManager, IPersistenceCache aCache)
	{
		super(aManager, aCache);
	}

	public  Vector addPersistentFields( Vector aAnswer )
	{

		aAnswer.addElement(new PersistentField("ve_ep_ref", "oid", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_idclient", "ve_ep_idclient", 1, new DBStringFieldType()));

		aAnswer.addElement(new PersistentField("ve_ep_fecha", "ve_ep_fecha", 1, new DBStringFieldType()));
		
		aAnswer.addElement(new PersistentField("ve_ep_hora", "ve_ep_hora", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_servicio", "ve_ep_servicio", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_cond", "ve_ep_cond", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_estado", "ve_ep_estado", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_sev_pk", "ve_sev_pk", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_fus_pk", "ve_fus_pk", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_cond_pk", "ve_cond_pk", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_resolucion", "ve_ep_resolucion", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_enc_apert", "ve_enc_apert", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_icd_cod", "ve_ep_icd_cod", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_hora_clin", "ve_ep_hora_clin", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_etiq", "ve_ep_etiq", 1, new DBStringFieldType()));

		/*aAnswer.addElement(new PersistentField("ve_ep_ref", "oid", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_idclient", "ve_ep_idclient", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_hora", "ve_ep_hora", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_servicio", "ve_ep_servicio", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_cond", "ve_ep_cond", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_estado", "ve_ep_estado", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_sev_pk", "ve_sev_pk", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_fus_pk", "ve_fus_pk", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_cond_pk", "ve_cond_pk", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_resolucion", "ve_ep_resolucion", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_enc_apert", "ve_enc_apert", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_icd_cod", "ve_ep_icd_cod", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_hora_clin", "ve_ep_hora_clin", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_etiq", "ve_ep_etiq", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("cod_centro", "codigo_centro", 1, new DBFixReferenceFieldType(CentroNovahisAccessor.getInstance())));
		*/
		return aAnswer;
	}
	public Vector collectionSpecs()
	{
		return new Vector();
	}
	public String oidPrefix()
	{
		return "";
	}
	public String persistentClassName()
	{
		return "com.ssi.model.salud.EpisodioNovahis";
	}
	public String tableName()
	{
		return "ve_episodio";
	}
}