package com.ssi.model.salud;

import com.ssi.persistence.model.*;
import com.ssi.model.salud.*;
/**
* Clase que representa a un Episodio registrado por el Sistema Siapwin para un
* Paciente determinado.
*/

public class SexoNovahis extends SexoSalud {

/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase SexoNovahis con Valores
* por defecto.
*/
	public SexoNovahis(){
		super();
	} // END OF Constructor SexoNovahis
	
	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return SexoNovahis.getPersistenceManager();
	}
	
	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return SexoNovahis.getPersistenceAccessor().persistenceManager();
	}

	
	public static PersistenceAccessor getPersistenceAccessor()
	{
		return SexoNovahisAccessor.getInstance();
	}

}