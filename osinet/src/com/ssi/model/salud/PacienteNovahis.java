package com.ssi.model.salud;

import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;

public class PacienteNovahis extends PacienteSalud {

	public String sexoDescripcion()
	{
		return this.sexo().descripcion();
	}
			private String codigo_cliente;// 11 N
			private ProxyInterface codigo_sexo   ;// 11 N
			private String codigo_ecivil ;// 11 N
			private String codigo_profesion ;// 11 N
			private String codigo_estudios ;// 11 N
			private String codigo_idioma ;// 11 N
			private String apellido1     ;// 20 L
			private String apellido2     ;// 20 L
			private String nombre        ;// 20 L
			private String codigo1       ;// 30 L
			private String codigo2       ;// 30 L
			private String codigo3       ;//30 L
			private String codigo4       ;// 30 L
			private GregorianCalendar nac_fecha     ;// 10 YMD
			private String nac_LD1       ;// 11 N
			private String nac_LD2       ;// 11 N
			private String nac_LD3       ;// 11 N
			private String nac_LD4       ;// 11 N
			private String nac_LD5       ;// 11 N
			private String dom_LD1       ;// 11 N
			private String dom_LD2       ;// 11 N
			private String dom_LD3       ;// 11 N
			private String dom_LD4       ;// 11 N
			private String dom_LD5       ;// 11 N
			private String dom_direccion ;// 60 L
			private String titular       ;// 6 N
			private String telefono1     ;// 20 L
			private String telefono2     ;// 20 L
			private String Paciente_SN   ;// 6 N
			private String Codigo_contacto ;// 11 N
			private String veces_atendido;// 11 N
			private String exitus_fecha  ;// 10 YMD
			private String exitus_hora   ;// 12 L
			private String pac_peso      ;// 5.3 N
			private String pac_peso_fecha ;// 10 YMD
			private String pac_talla     ;// 5.3 N
			private String pac_talla_fecha ;// 10 YMD
			private String pac_datcomplt ;// 6 N
			private String dom_direcc_entre ;// 40 L
			private String cod_factor    ;// 6 N
			private String cod_grupo     ;// 6 N
			private String tipo_dni_pk   ;// 11 N
			private String codigo_personal ;// 11 N
			private String ficha_fecha   ;// 10 YMD
			private String ficha_hora    ;// 12 L
			private String exitus_sn     ;// 6 N
			private String datos_browser_sn ;// 6 N
			private String dom_cp        ;// 10 L
			private String nac_cp        ;// 10 L
			private String codigo1_nac   ;// 11 N
			private String codigo2_nac   ;// 11 N
			private String codigo3_nac   ;// 11 N
			private String codigo4_nac   ;// 11 N
			private String civa_pk       ;// 11 N
			private String via_tipo_pk   ;//4 L
			private String via_dom_nombr ;// 40 L
			private String via_dom_num   ;// 4 L


	/**
	 * Constructor
	 *
	 */
	public PacienteNovahis() {
	}


	public PersistenceManager persistenceManager()
		throws ClassNotFoundException {
	    return PacienteNovahis.getPersistenceManager();
	}

	public static PacienteNovahis getPacienteConID(String idNovahis){
		LogFile.log("PacienteNovahis getPacienteConID: idNovahis = "+idNovahis);
		String unaRestriccion = " WHERE codigo_cliente = "+idNovahis;
		try {
			LogFile.log("PacienteNovahis: getPacienteConID: "+unaRestriccion);
			Vector pacientes = PacienteNovahis.instancesOfWhere(unaRestriccion);
			return (PacienteNovahis)pacientes.elementAt(0);
		}catch(SQLException sqle){
			LogFile.log("PacienteNovahis: getPacientesConID: SQLExc: "+sqle);
		}
		catch(Exception exc){
			LogFile.log("PacienteNovahis: getPacientesConID: Exception: "+exc);
		}finally {
			return null;
		}
	} // END OF getPacienteConID
	
	public static String construirRestriccion(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento) {
		String unaRestriccion = null;
		if (rfc!=null && rfc.length() > 0){
			unaRestriccion = " codigo1 = '" +rfc+ "'";
		}
		if (nombre!= null && nombre.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " nombre LIKE '"+nombre+ "%'";
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" nombre LIKE '"+nombre+ "%'";
			}
		}
		if (apellido1 != null && apellido1.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " apellido1 LIKE '"+apellido1+ "%'";
			}
			else{
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" apellido1 LIKE '"+apellido1+ "%'";
			}
		}
		if (apellido2 != null && apellido2.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " apellido2 LIKE '"+apellido2+ "%'";
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" apellido2 LIKE '"+apellido2+"%' ";
			}
		}
		if (fechaNacimiento != null && fechaNacimiento.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " DATEFORMAT(nac_fecha,'dd/mm/yyyy') = '"+fechaNacimiento+ "' ";
			}
			else {
			   unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" DATEFORMAT(nac_fecha,'dd/mm/yyyy') = '"+fechaNacimiento+ "' ";
			}
		}
		if (unaRestriccion!=null ){
			unaRestriccion = " where "+unaRestriccion;
		} else{
			unaRestriccion = "";
		}
		LogFile.log("PacienteNovahis: construirRestriccion: "+unaRestriccion);
		LogFile.log("PacienteNovahis: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion

	
	
	
	/**
	* M�todo que devuelve los pacientes Novahis
	* 
	*@param rfc,nombre,apellido1,apellido2,fechaNacimiento
	*@return vector con pacientes siapwin
	*/
	public static Vector getPacientes(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
									throws SQLException, Exception {
		
		String unaRestriccion = PacienteNovahis.construirRestriccion(rfc,nombre,apellido1,apellido2,fechaNacimiento);
		LogFile.log("PacienteNovahis: getPacientes: WHERE-> "+unaRestriccion);

		Vector vPacienteNovahis = PacienteNovahis.instancesOfWhere(unaRestriccion);
		LogFile.log("PacienteNovahis: getPacientes: size: "+vPacienteNovahis.size());
		
		if(vPacienteNovahis == null){
			LogFile.log("PacienteNovahis: getPacientes: Vector NULO ");
			vPacienteNovahis = new Vector();
		}

		
		
		return vPacienteNovahis;
	} // END OF getPacientes



	/**
	* M�todo qu� busca los datos de los Pacientes en la Base de Datos del Sistema Novahis y
	* a partir de ellos genera un Vector que representa los Pacientes registrados en Novahis.
	* Este m�todo devuelve el Vector Generado.
	*
	* @param unaRestriccion      Cadena de Caracteres qu� representa la restriccion (filtro)
	*                            de la SQL-Query que busca los datos de los Pacientes.
	* @return pacientesNovahis   Vector que contiene los datos de los Pacientes.
	*/
	public static Vector instancesOfWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
									   ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		Vector pacientesNovahis = PacienteNovahis.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.PacienteNovahis"), unaRestriccion);
		if (pacientesNovahis == null){
		    LogFile.log("PacienteNovahis: instancesWithWhere: No hay Pacientes en Novahis.");
		    pacientesNovahis = new Vector();
		}
		return pacientesNovahis;
	} // END OF instancesWithWhere



	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		LogFile.log("PacienteNovahis: getPersistenceManager: inicio");
		return PacienteNovahis.getPersistenceAccessor().persistenceManager();
	}

	
	public static PersistenceAccessor getPersistenceAccessor()
	{
		return PacienteNovahisAccessor.getInstance();
	}
	

	public static Vector findAll(String unaRestriccion)
	{
	  try
	  {
	    PacienteNovahis answer = null;
		Vector instances = PacienteNovahis.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.PacienteNovahis"), unaRestriccion);
	    
	    return instances;

	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }
		
	}

	
	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return PacienteNovahis.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.PacienteNovahis"));
	}


	/**
	* M�todo qu� devuelve RFC del pacinete Novahis
	* 
	* @return codigo1
	*/
	public String RFC(){
		return codigo1;
	}
	
	
	/**
	* M�todo qu� setea RFC del pacinete Novahis
	* 
	* @param unRFC 
	*/
	public void RFC(String unRFC){
		codigo1 = unRFC;
	}


	/**
	* M�todo qu� devuelve nombre del pacinete Novahis
	* 
	* @return nombre
	*/
	public String nombre()
	{
		return nombre;
	}


	/**
	* M�todo qu� setea nombre del pacinete Novahis
	* 
	* @param unNombre 
	*/
	public void nombre(String unNombre)
	{
		nombre = unNombre;
	}


	
	/**
	* M�todo qu� devuelve apellido1 del pacinete Novahis
	* 
	* @return apellido1
	*/
	public String apellido1()
	{
		return apellido1;
	}


	/**
	* M�todo qu� setea apellido1 del pacinete Novahis
	* 
	* @param unApellido1 
	*/
	public void apellido1(String unApellido1)
	{
		apellido1 = unApellido1;
	}
	
	
	/**
	* M�todo qu� devuelve apellido1 del pacinete Novahis
	* 
	* @return apellido1
	*/
		public String apellido2()
	{
		return apellido2;
	}

	/**
	* M�todo qu� setea apellido2 del pacinete Novahis
	* 
	* @param apellido2 
	*/
	public void apellido2(String unApellido2)
	{
		apellido2 = unApellido2;
	}
	

	
	/**
	* M�todo qu� devuelve fecha de nacimiento del pacinete Novahis
	* 
	* @return nac_fecha
	*/
	public GregorianCalendar fechaNacimiento()
	{
		return nac_fecha;
	}


	/**
	* M�todo qu� setea fecha de nacimiento del pacinete Novahis
	* 
	* @param unaFecha 
	*/
	public void fechaNacimiento(GregorianCalendar unaFecha)
	{
		nac_fecha = unaFecha;
	}
	

	
	/**
	* M�todo qu� devuelve el codigo3 del pacinete Novahis
	* 
	* @return codigo3
	*/
	public String documento()
	{
		return codigo3;
	}


	/**
	* M�todo qu� setea el codigo3 Novahis
	* 
	* @param unDocumento 
	*/
	public void documento(String unDocumento)
	{
		codigo3 = unDocumento;
	}
	

	/**
	* M�todo qu� devuelve el codigo de cliente del pacinete Novahis
	* 
	* @return codigo_cliente
	*/
	public String codigoCliente()
	{
		return codigo_cliente;
	}


	/**
	* M�todo qu� setea el codigo de cliente Novahis
	* 
	* @param unCodigo 
	*/
	public void codigoCliente(String unCodigo)
	{
		codigo_cliente = unCodigo;
	}


	/**
	* M�todo qu� devuelve telefono del pacinete Novahis
	* 
	* @return telefono1
	*/
	public String telefono1(){
		return telefono1;
	}
	
	
	/**
	* M�todo qu� setea telefono del pacinete Novahis
	* 
	* @param unTelefono1 
	*/
	public void telefono1(String unTelefono1){
		telefono1 = unTelefono1;
	}


	/**
	* M�todo qu� devuelve codigo de sexo del pacinete Novahis
	*
	* @return codigo_sexo
	*/
	public ProxyInterface codigo_sexo(){
		try
		{
			codigo_sexo.fetchRealFor(this, "codigo_sexo");
		}
		catch (Exception ex)
		{
			LogFile.log("Unable to fetch object SexoNovahis" + ex);
		}
		return codigo_sexo;
	}

	public SexoNovahis sexo()
	{
		return (SexoNovahis)this.codigo_sexo();
	}

	/**
	* M�todo qu� setea codigo de sexo del pacinete Novahis
	*
	* @param unCodigoSexo
	*/
	public void codigo_sexo(ProxyInterface unCodigoSexo){
		codigo_sexo = unCodigoSexo;
	}
	

	/**
	* M�todo qu� devuelve el nombre del sistema a la cu�l pertenece el paciente
	*
	* @return String  nombre del sistema.
	*/
	public String nombreSistema() {
		return new String("Novahis");   
	} // END OF String nombreSistema()

	
	public String oid()
	{
		return codigo_cliente;
	}

	public void oid(String unCodigo)
	{
		codigo_cliente = unCodigo;
	}

}