package com.ssi.model.salud;

import java.util.Vector;
import com.ssi.persistence.cache.*;
import com.ssi.persistence.model.*;

public class EncuentroNovahisAccessor extends PersistenceAccessor
{


	public static String className()
	{
		return "com.ssi.model.salud.EncuentroNovahisAccessor";
	}

	public static PersistenceAccessor getInstance()
	{
	    return AccessorCache.getInstance().get(className());
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new EncuentroNovahisAccessor(aManager);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new EncuentroNovahisAccessor(aManager, aCache);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	protected EncuentroNovahisAccessor(PersistenceManager aManager)
	{
		super (aManager);
	}

	protected EncuentroNovahisAccessor(PersistenceManager aManager, IPersistenceCache aCache)
	{
		super(aManager, aCache);
	}

	public  Vector addPersistentFields( Vector aAnswer )
	{
		aAnswer.addElement(new PersistentField("ve_en_ref", "oid", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_ref", "ve_ep_ref", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_fecha", "fechaInicio", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_hora", "horaInicio", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_servicio", "ve_en_servicio", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_motivo", "ve_en_motivo", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_estado", "ve_en_estado", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_tipo", "ve_en_tipo", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_idclient", "ve_en_idclient", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_icd_cod", "ve_en_icd_cod", 1, new DBStringFieldType()));

		return aAnswer;
	}
	public Vector collectionSpecs()
	{
		return new Vector();
	}
	public String oidPrefix()
	{
		return "";
	}
	public String persistentClassName()
	{
		return "com.ssi.model.salud.EncuentroNovahis";
	}
	public String tableName()
	{
		return "ve_encuentro";
	}
}