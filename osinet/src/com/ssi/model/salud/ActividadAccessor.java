package com.ssi.model.salud;

import java.util.Vector;
import com.ssi.persistence.cache.*;
import com.ssi.persistence.model.*;

public class ActividadAccessor extends PersistenceAccessor
{


	public static String className()
	{
		return "com.ssi.model.salud.ActividadAccessor";
	}

	public static PersistenceAccessor getInstance()
	{
	    return AccessorCache.getInstance().get(className());
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new ActividadAccessor(aManager);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new ActividadAccessor(aManager, aCache);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	protected ActividadAccessor(PersistenceManager aManager)
	{
		super (aManager);
	}

	protected ActividadAccessor(PersistenceManager aManager, IPersistenceCache aCache)
	{
		super(aManager, aCache);
	}

	public  Vector addPersistentFields( Vector aAnswer )
	{
   	    aAnswer.addElement(new PersistentField("ve_ac_ref", "oid", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_en_ref", "ve_en_ref", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ac_fecha", "ve_ac_fecha", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ac_hora", "ve_ac_hora", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ac_servicio", "ve_ac_servicio", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ac_descri", "ve_ac_descri", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ac_estado", "ve_ac_estado", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ac_tipo", "ve_ac_tipo", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ac_coste", "ve_ac_coste", 1, new DBStringFieldType()));

		return aAnswer;
	}
	public Vector collectionSpecs()
	{
		return new Vector();
	}
	public String oidPrefix()
	{
		return "";
	}
	public String persistentClassName()
	{
		return "com.ssi.model.salud.Actividad";
	}
	public String tableName()
	{
		return "ve_actividad";
	}
}