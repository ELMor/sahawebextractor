package com.ssi.model.salud;

import java.util.Vector;
import com.ssi.persistence.cache.*;
import com.ssi.persistence.model.*;

public class SexoSiapwinAccessor extends PersistenceAccessor
{


	public static String className()
	{
		return "com.ssi.model.salud.SexoSiapwinAccessor";
	}

	public static PersistenceAccessor getInstance()
	{
	    return AccessorCache.getInstance().get(className());
	}
	
	public static PersistenceAccessor getInstance(PersistenceManager aManager)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new SexoSiapwinAccessor(aManager);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new SexoSiapwinAccessor(aManager, aCache);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}
	
	protected SexoSiapwinAccessor(PersistenceManager aManager)
	{
		super (aManager);
	}

	protected SexoSiapwinAccessor(PersistenceManager aManager, IPersistenceCache aCache)
	{
		super (aManager, aCache);
	}
	
	
	public  Vector addPersistentFields( Vector aAnswer )
	{
		aAnswer.addElement(new PersistentField("codigo_sexo", "oid", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("nombre_sexo", "descripcion", 1, new DBStringFieldType()));
		return aAnswer;
	}
	
	public Vector collectionSpecs()
	{
		return new Vector();
	}

	public String oidPrefix()
	{
		return "";
	}

	public String persistentClassName()
	{
		return "com.ssi.model.salud.SexoSiapwin";
	}
	public String tableName()
	{
		return "sexo";
	}
}