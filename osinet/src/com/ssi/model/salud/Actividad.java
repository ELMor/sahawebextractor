package com.ssi.model.salud;

import com.ssi.model.salud.*;
import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
import com.ssi.persistence.cache.*;



/**
* Clase que representa a un Actividad.
*/
public class Actividad extends ActividadSalud {
			
			private String ve_ac_ref ;// 15 L
			private String ve_en_ref ;//15 L
			private String ve_ac_fecha ;// 10 YMD
			private String ve_ac_hora ;// 12 L
			private String ve_ac_servicio ;// 40 L
			private String ve_ac_descri ;// 255 L
			private String ve_ac_estado ;// 6 N
			private String ve_ac_tipo ;// 11 N
			private String ve_ac_coste ;// 8.3 N
			
/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase Actividad
* por defecto.
*/
	public Actividad(){
		super();
	} // END OF Constructor Actividad


	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return Actividad.getPersistenceManager();
	}

	public SistemaSalud sistema(){
		return SistemaNovahis.getInstance();
	}

	/**
	* M�todo que devuelve una cadena de caracteres
	*
	* @param  idEncuentro       Cadena de Caracteres que representa el identificador �nico del
	*                           Encuentro qu� ser� asociado a las Actividades.
	* @return unaRestriccion    Cadena de Caracteres que representa la condicion de Busqueda
	*                           propia del SQLQuery que
	* 
	*/
	public static String construirRestriccion(String idEncuentro) {
		String unaRestriccion = null;
		if (idEncuentro!=null && idEncuentro.length() > 0){
			unaRestriccion = " WHERE ve_en_ref = '"+idEncuentro+"'";
			LogFile.log("Actividad: construirRestriccion: WHERE 1: "+ unaRestriccion);
		}
		LogFile.log("Actividad: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion


/**
* M�todo que devuelve un Vector que contiene los datos de las actividades asociadas
* a un Encuentro dado por la selecci�n del Usuario desde la pantalla de Busqueda de 
* Encuentros.
* 
* @return vActividades    Vector que contiene los datos de las Actividades que ser�n 
*                         mostradas en Pantalla.
*/
	public static Vector getActividades(String idEncuentro) {
		Vector vActividades = new Vector();
		String unaRestriccion = Actividad.construirRestriccion(idEncuentro);
		LogFile.log("Actividad: getActividades: WHERE-> "+unaRestriccion);
		try {
			vActividades = Actividad.instancesWithWhere(unaRestriccion);
		}catch (SQLException sqle){
			LogFile.log("Actividad: getActividades: SQLException");
		}catch (Exception exc){
			LogFile.log("Actividad: getActividades: Exception");
		}
		return vActividades;
	} // END OF getActividades

	/**
	* M�todo qu� busca los Actividad
	* Este m�todo devuelve el Vector Generado.
	*
	* @param unaRestriccion:Cadena de Caracteres qu� representa la restriccion (filtro) de la SQL-Query que busca los datos de los Actividad.
	* @return  Vector que contiene los datos de los Actividad.
	*/
	public static Vector instancesWithWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
										ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		Vector vActividad = Actividad.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.Actividad"), unaRestriccion);
		if (vActividad == null){
		    LogFile.log("Actividad: instancesWithWhere: No hay Actividad.");
		    vActividad = new Vector();
		}
		return vActividad;
	} // END OF instancesWithWhere



	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return Actividad.getPersistenceAccessor().persistenceManager();
	}


	public static PersistenceAccessor getPersistenceAccessor()
	{
		return ActividadAccessor.getInstance();
	}

/**
*
*/
	public static Vector findAll(String unaRestriccion)
	{
	  try{
	    Actividad answer = null;
		Vector instances = Actividad.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.Actividad"), unaRestriccion);
	    return instances;
	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }
	}//END OF findAll

/**
*
*/
	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return Encuentro.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.Actividad"));
	}


	/**
	* M�todo qu� devuelve ve_ac_ref
	*
	* @return ve_ac_ref
	*/
	public String ve_ac_ref(){
		return ve_ac_ref;
	}

	/**
	* M�todo qu� setea ve_ac_ref
	*
	* @param unVe_ac_ref
	*/
	public void ve_ac_ref(String unVe_ac_ref){
		ve_ac_ref = unVe_ac_ref;
	}

	/**
	* M�todo qu� devuelve ve_en_ref
	*
	* @return ve_en_ref
	*/
	public String ve_en_ref(){
		return ve_en_ref;
	}

	/**
	* M�todo qu� setea ve_en_ref
	*
	* @param unVe_ep_ref
	*/
	public void ve_en_ref(String unVe_en_ref){
		ve_en_ref = unVe_en_ref;
	}

	/**
	* M�todo qu� devuelve ve_ac_fecha
	*
	* @return ve_ac_fecha
	*/
	public String ve_ac_fecha()
	{
		return ve_ac_fecha;
	}

	/**
	* M�todo qu� setea ve_ac_fecha
	*
	* @param unVe_ac_fecha
	*/
	public void ve_ac_fecha(String unVe_ac_fecha)
	{
		ve_ac_fecha = unVe_ac_fecha;
	}

	/**
	* M�todo qu� devuelve ve_ac_hora
	*
	* @return ve_ac_hora
	*/
	public String ve_ac_hora()
	{
		return ve_ac_hora;
	}

	/**
	* M�todo qu� setea ve_ac_hora
	*
	* @param unVe_ac_hora
	*/
	public void ve_ac_hora(String unVe_ac_hora)
	{
		ve_ac_hora = unVe_ac_hora;
	}

	/**
	* M�todo qu� devuelve la fecha y hora de inicio de la Actividad como una cadena de
	* caracteres.
	*
	* @return ve_en_fecha
	*/
	public String fechayHoraInicio(){
		String fechayHora = null;
		fechayHora = this.ve_ac_fecha();
		fechayHora = fechayHora +" "+ this.ve_ac_hora();
		return fechayHora;
	}

	/**
	* M�todo qu� devuelve la descripcion del Servicio propio de la Actividad. 
	*
	* @return String    Cadena de Caracteres que indica que Servicio est� asociado
	*                   a la Actividad.
	*/
	public String servicio(){
		String respuesta =	this.ve_ac_servicio();
		if((respuesta== null)){
		   respuesta = new String();
		   respuesta = "Sin Dato";
		}
		return respuesta;
	}

	/**
	* M�todo qu� devuelve ve_ac_servicio
	*
	* @return ve_ac_servicio
	*/
	public String ve_ac_servicio()
	{
		return ve_ac_servicio;
	}

	/**
	* M�todo qu� setea ve_ac_servicio
	*
	* @param unVe_ac_servicio
	*/
	public void ve_ac_servicio(String unVe_ac_servicio)
	{
		ve_ac_servicio = unVe_ac_servicio;
	}
	/**
	* M�todo qu� devuelve una cadena de caracteres que denota la Descripcion expl�cita de
	* la Actividad.
	*
	* @return String     Cadena de Caracteres que representa la descripci�n de la Actividad.
	*/
	public String descripcion(){
		String respuesta = this.ve_ac_descri();
		if(!(respuesta.length()>0)){
		   respuesta = new String();
		   respuesta = "Sin Dato";
		}
		return respuesta;
	} //END OF Method descripcion

	/**
	* M�todo qu� devuelve ve_ac_descri
	*
	* @return ve_ac_descri
	*/
	public String ve_ac_descri()
	{
		return ve_ac_descri;
	}


	/**
	* M�todo qu� setea ve_ac_descri
	*
	* @param unVe_ac_descri
	*/
	public void ve_ac_descri(String unVe_ac_descri)
	{
		ve_ac_descri = unVe_ac_descri;
	}


	/**
	* M�todo qu� devuelve el Estado de una Actividad.
	*
	* @return String    Cadena de Caracteres que representa el estado en que se
	*                   encuentra la Actividad.
	*/
	public String estado(){
		String respuesta = this.ve_ac_estado();
		if (respuesta == null){
			respuesta = new String();
			respuesta = "Sin Dato";
		}
		return respuesta;
	} // END OF method estado

	/**
	* M�todo qu� devuelve ve_ac_estado
	*
	* @return ve_ac_estado
	*/
	public String ve_ac_estado()
	{
		return ve_ac_estado;
	}

	/**
	* M�todo qu� setea ve_ac_estado
	*
	* @param unVe_ac_estado
	*/
	public void ve_ac_estado(String unVe_ac_estado)
	{
		ve_ac_estado = unVe_ac_estado;
	}

	/**
	* M�todo qu� devuelve ve_ac_tipo
	*
	* @return ve_ac_tipo
	*/
	public String ve_ac_tipo()
	{
		return ve_ac_tipo;
	}

	/**
	* M�todo qu� setea ve_ac_tipo
	*
	* @param unVe_ac_tipo
	*/
	public void ve_ac_tipo(String unVe_ac_tipo)
	{
		ve_ac_tipo = unVe_ac_tipo;
	}

	/**
	* M�todo qu� devuelve ve_ac_coste
	*
	* @return ve_ac_coste
	*/
	public String ve_ac_coste()
	{
		return ve_ac_coste;
	}

	/**
	* M�todo qu� setea ve_ac_coste
	*
	* @param unVe_ac_coste
	*/
	public void ve_ac_coste(String unVe_ac_coste)
	{
		ve_ac_coste = unVe_ac_coste;
	}

	public String oid()
	{
		return ve_ac_ref;
	}

	public void oid(String unCodigo)
	{
		ve_ac_ref = unCodigo;
	}

/**
 * delete method comment.
 */
public void delete() throws java.lang.NoSuchMethodException, java.sql.SQLException, java.lang.ClassNotFoundException {}/**
 * Insert the method's description here.
 * Creation date: (19/02/01 16:16:36)
 * @return java.util.Vector
 */

public static Vector getActividades(String idEncuentro,String sistema) {

	 Vector vActividadesSiapwin = new Vector();
 	 Vector vActividadesNovahis = new Vector();

	 try{
		    if(sistema.equals("SistemaNovahis")) {
			   LogFile.log("Actividad: getActividades: es Novahis");
	 			String unaRestriccion = " WHERE ve_en_ref = '" +idEncuentro+ "'";
				LogFile.log("Restriccion: "+unaRestriccion);
	 			vActividadesNovahis = ActividadNovahis.instancesWithWhere(unaRestriccion);
			 }
			 else
			 {
				LogFile.log("Actividad: getActividades: es Siapwin");
				String unaRestriccion = " WHERE ve_en_ref='"+idEncuentro+ "'";
				LogFile.log("Restriccion: "+unaRestriccion);
				vActividadesSiapwin = ActividadSiapwin.instancesWithWhere(unaRestriccion);
			 }
		 }
		 catch(Exception e){
		   
	         LogFile.log(e);
	     }

	 
	 	 LogFile.log("Actividad: getActividades Novahis: size: " + vActividadesNovahis.size());
		 LogFile.log("Actividad: getActividades Siapwin: size: " + vActividadesSiapwin.size());
	   	   
	     return unificarActividades(vActividadesNovahis,vActividadesSiapwin);

	}/**
 * Insert the method's description here.
 * Creation date: (20/02/01 11:05:56)
 * @return java.util.Vector
 * @param vPacientes java.util.Vector
 */

 public static Vector getActividades(Vector pacientes,String restriccionNovahis,String restriccionSiapwin) {
	

	 Vector vActividadesSiapwin = new Vector();
 	 Vector vActividadesNovahis = new Vector();
	 
 	  try{
		 
	    for(int i = 0; i < pacientes.size(); i++)
		{
			 if(pacientes.elementAt(i) instanceof PacienteNovahis)
			{
				LogFile.log("Actividad: getActividades: es Novahis");
	 			LogFile.log("Restriccion: "+restriccionNovahis);
	 			//vActividadesNovahis = ActividadNovahis.instancesWithWhere(restriccionNovahis);
	 			//borrar:
	 			restriccionNovahis = " WHERE ve_en_ref = '10'";
	 			vActividadesNovahis = ActividadNovahis.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.ActividadNovahis"), restriccionNovahis);
			}
			else
			{
				LogFile.log("Actividad: getActividades: es Siapwin");
				LogFile.log("Restriccion: "+restriccionSiapwin);
				vActividadesSiapwin = ActividadSiapwin.instancesWithWhere(restriccionSiapwin);
			}
		 }

   	 	 LogFile.log("Actividad: getActividades Novahis: size: " + vActividadesNovahis.size());
		 LogFile.log("Actividad: getActividades Siapwin: size: " + vActividadesSiapwin.size());
	 
	   }
	   catch(Exception e){
		   LogFile.log(e);
	   }	   
		
	   return unificarActividades(vActividadesNovahis,vActividadesSiapwin);

	}

/**
 * Insert the method's description here.
 * Creation date: (15/03/01 12:44:39)
 * @return java.lang.String
 */
public java.lang.String getTipoActividad() {
	return null;
}/**
 * isPersistent method comment.
 */
public boolean isPersistent() {
	return false;
}/**
 * save method comment.
 */
public void save() throws java.lang.InstantiationException, java.lang.ClassNotFoundException, java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.NoSuchMethodException, java.sql.SQLException {}/**
 * Insert the method's description here.
 * Creation date: (15/03/01 12:44:39)
 * @param newTipoActividad java.lang.String
 */
public void setTipoActividad(java.lang.String newTipoActividad) {}/**
 * Insert the method's description here.
 * Creation date: (19/02/01 16:18:52)
 * @return java.util.Vector
 */
public static Vector unificarActividades(Vector v1, Vector v2) {
	Enumeration e = v2.elements();
	while(e.hasMoreElements())
	v1.addElement(e.nextElement());
	
	return v1;
}} // END OF CLASS Actividad