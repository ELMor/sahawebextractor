package com.ssi.model.salud;

import java.io.*;
import java.sql.*;
import com.ssi.persistence.model.*;


public abstract class SistemaSalud
{
	private PersistenceManager persistenceManager;

	public SistemaSalud()
	{
		persistenceManager = new PersistenceManager(this.getConfiguration());
	}
	
	public abstract PersistenceConfiguration getConfiguration();
	
	/**
	 * Conecta al sistema a su base de datos.
	 */
	public void connect()
		throws SQLException, Exception
	{
		this.getPersistenceManager().connect();
	}
	
	public void setPersistenceManager(PersistenceManager aManager)
	{
		this.persistenceManager = aManager;
	}
	
	public PersistenceManager getPersistenceManager()
	{
		return this.persistenceManager;
	}
 
	public abstract String nombreSistema();
}