package com.ssi.model.salud;

/**
 * Insert the type's description here.
 * Creation date: (21/02/01 18:04:20)
 * @author: Administrator
 */
public class Alergia {

	String idPaciente;   
	String sistema;
	String descripcion;
	java.util.Date fechaDeDeteccion;
	
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:09:03)
 */
public Alergia(String desc, java.util.Date fechaDesde) {
	
	setDescripcion(desc);
	setFechaDeDeteccion(fechaDesde);
 } 
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:08:20)
 * @return java.lang.String
 */
public java.lang.String getDescripcion() {
	return descripcion;
}
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:08:20)
 * @return java.util.Date
 */
public java.util.Date getFechaDeDeteccion() {
	return fechaDeDeteccion;
}
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:08:20)
 * @return java.lang.String
 */
public java.lang.String getIdPaciente() {
	return idPaciente;
}
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:08:20)
 * @return java.lang.String
 */
public java.lang.String getSistema() {
	return sistema;
}
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:08:20)
 * @param newDescripcion java.lang.String
 */
public void setDescripcion(java.lang.String newDescripcion) {
	descripcion = newDescripcion;
}
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:08:20)
 * @param newFechaDeDeteccion java.util.Date
 */
public void setFechaDeDeteccion(java.util.Date newFechaDeDeteccion) {
	fechaDeDeteccion = newFechaDeDeteccion;
}
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:08:20)
 * @param newIdPaciente java.lang.String
 */
public void setIdPaciente(java.lang.String newIdPaciente) {
	idPaciente = newIdPaciente;
}
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:08:20)
 * @param newSistema java.lang.String
 */
public void setSistema(java.lang.String newSistema) {
	sistema = newSistema;
}
}
