package com.ssi.model.salud;

import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;

public class PacienteSiapwin extends PacienteSalud {

			private String codigo_cliente ;// 11 N
			private ProxyInterface codigo_sexo ;// 11 N
			private String codigo_ecivil ;// 11 N
			private String codigo_profesion ;// 11 N
			private String codigo_estudios ;// 11 N
			private String codigo_idioma ;// 11 N
			private String apellido1 ;// 20 L
			private String apellido2 ;// 20 L
			private String nombre ;// 20 L
			private String codigo1 ;// 30 L es el RFC
			private String codigo2 ;// 30 L
			private String codigo3 ;// 30 L
			private String codigo4 ;// 30 L
			private GregorianCalendar nac_fecha ;// 10 YMD
			private String nac_LD1 ;// 11 N
			private String nac_LD2 ;// 11 N
			private String nac_LD3 ;// 11 N
			private String nac_LD4 ;// 11 N
			private String nac_LD5 ;// 11 N
			private String dom_LD1 ;// 11 N
			private String dom_LD2 ;// 11 N
			private String dom_LD3 ;// 11 N
			private String dom_LD4 ;// 11 N
			private String dom_LD5 ;// 11 N
			private String dom_direccion ;// 60 L
			private String titular ;// 6 N
			private String telefono1 ;// 20 L
			private String telefono2 ;// 20 L
			private String Paciente_SN ;// 6 N
			private String Codigo_contacto ;// 11 N
			private String veces_atendido ;// 11 N
			private String exitus_fecha ;// 10 YMD
			private String exitus_hora ;// 12 L
			private String pac_peso ;// 5.3 N
			private String pac_peso_fecha ;// 10 YMD
			private String pac_talla ;// 5.3 N
			private String pac_talla_fecha ;// 10 YMD
			private String pac_datcomplt ;// 6 N
			private String dom_direcc_entre ;// 40 L
			private String cod_factor ;// 6 N
			private String cod_grupo ;// 6 N
			private String tipo_dni_pk ;// 11 N
			private String codigo_personal ;// 11 N
			private String ficha_fecha ;// 10 YMD
			private String ficha_hora ;// 12 L
			private String exitus_sn ;// 6 N
			private String vip_sn ;// 6 N
			private String grado_ausen ;// 11 N
			private String afiliado_sn ;// 6 N
			private String control_cod1 ;// 1 L
			private String rce ;// 18 L
			private String grado_ausentismo ;// 11 N
			private String cronico_sn ;// 11 N
			private String mod_fecha ;// 10 YMD


	/**
	* Constructor
	*
	*/
	public PacienteSiapwin() {
	}

	/**
	* M�todo qu� Devuelve una Cadena de Caracteres qu� representa la sintaxis de la restriccion
	* qu� se debe aplicar a la SQL-Query qu� realiza la b�squeda de los Pacientes en el Sistema
	* Siapwin.
	*
	* @param rfc              Cadena de Caracteres qu� representa
	* @param nombre           Cadena de Caracteres qu� representa
	* @param apellido1        Cadena de Caracteres qu� representa
	* @param apellido2        Cadena de Caracteres qu� representa
	* @param fechaNacimiento  Cadena de Caracteres qu� representa la Fecha de Nacimiento del Paciente.
	*
	* @return unaRestriccion
	*/
	public static String construirRestriccion(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento) {
		
		LogFile.log("Siapwin fecha de nacimiento:"+fechaNacimiento+".");
		String unaRestriccion = null;
		if (rfc!=null && rfc.length() > 0){
			unaRestriccion = " codigo1 = '" +rfc+ "'";
		}
		if (nombre!= null && nombre.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " nombre LIKE '"+nombre+ "%'";
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" nombre LIKE '"+nombre+ "%'";
			}
		}
		if (apellido1 != null && apellido1.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " apellido1 LIKE '"+apellido1+ "%' ";
			}
			else{
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+ "apellido1 LIKE '"+apellido1+ "%' ";
			}
		}
		if (apellido2 != null && apellido2.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " apellido2 LIKE '"+apellido2+ "%' ";
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" apellido2 LIKE '"+apellido2+"%' ";
			}
		}
		if (fechaNacimiento != null && fechaNacimiento.length() > 0){
			if(unaRestriccion == null){
				//unaRestriccion = " nac_fecha = '"+fechaNacimiento+"'";
				LogFile.log("Construir restriccion 1");
				unaRestriccion = " DATEFORMAT(nac_fecha,'dd/mm/yyyy') = '"+fechaNacimiento+ "' ";
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" DATEFORMAT(nac_fecha,'dd/mm/yyyy') = '"+fechaNacimiento+ "' ";
			}
		}
		if (unaRestriccion!=null ){
			unaRestriccion = " where "+unaRestriccion;
		} else{
			unaRestriccion = "";
		}
		LogFile.log("PacienteSiapwin: construirRestriccion: "+unaRestriccion);
		LogFile.log("PacienteSiapwin: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion

/**
*
*/
	public static PacienteSiapwin getPacienteConID(String idSiapwin){
		String unaRestriccion = " WHERE codigo_cliente = "+idSiapwin;
		try {
			Vector pacientes = PacienteSiapwin.instancesWithWhere(unaRestriccion);
			return (PacienteSiapwin) pacientes.elementAt(0);
		}catch(SQLException sqle){
			LogFile.log("PacienteSiapwin: getPacientesConID: SQLExc: "+sqle);
		}
		catch(Exception exc){
			LogFile.log("PacienteSiapwin: getPacientesConID: Exception: "+exc);
		}finally {
			return null;
		}
	} // END OF getPacienteConID

/**
* M�todo qu� 
*/
	public String sexoDescripcion()
	{
		return this.codigo_sexo().descripcion();
	}

	/**
	* M�todo que devuelve los pacientes Siapwin
	* 
	*@param rfc,nombre,apellido1,apellido2,fechaNacimiento
	*@return vector con pacientes siapwin
	*/
	public static Vector getPacientes(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
													throws SQLException, Exception {
		
		String unaRestriccion = PacienteSiapwin.construirRestriccion(rfc,nombre,apellido1,apellido2,fechaNacimiento);
		LogFile.log("PacienteSiapwin: getPacientes: WHERE-> "+unaRestriccion);

		
		Vector vPacienteSiapwin = PacienteSiapwin.instancesWithWhere(unaRestriccion);
		LogFile.log("PacienteSiapwin: getPacientes: size: "+vPacienteSiapwin.size());

		if(vPacienteSiapwin == null){
			LogFile.log("PacienteSiapwin: getPacientes: Vector NULO ");
			vPacienteSiapwin = new Vector();
		}
		
		return vPacienteSiapwin;
	} // END OF getPacientes


	public static Vector instancesWithWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
										ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		return PacienteSiapwin.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.PacienteSiapwin"), unaRestriccion);
	} // END OF instancesWithWhere

	
	
	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return PacienteSiapwin.getPersistenceManager();
	}

	
	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return PacienteSiapwin.getPersistenceAccessor().persistenceManager();
	}

	
	public static PersistenceAccessor getPersistenceAccessor()
	{
		return PacienteSiapwinAccessor.getInstance();
	}
	

	public static Vector findAll(String unaRestriccion) {
	  try
	  {
	    PacienteSiapwin answer = null;
		Vector instances = PacienteSiapwin.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.PacienteSiapwin"), unaRestriccion);
	  return instances;
	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }
		
	}

	
	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return PacienteSiapwin.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.PacienteSiapwin"));
	}

	
	
	/**
	* M�todo qu� devuelve RFC del pacinete Siapwin
	* 
	* @return codigo1
	*/
	public String RFC(){
		return codigo1;
	}
	
	
	/**
	* M�todo qu� setea RFC del pacinete Siapwin
	* 
	* @param unRFC 
	*/
	public void RFC(String unRFC){
		codigo1 = unRFC;
	}

	/**
	* M�todo qu� devuelve nombre del pacinete Siapwin
	* 
	* @return nombre
	*/
	public String nombre()
	{
		return nombre;
	}


	/**
	* M�todo qu� setea nombre del pacinete Siapwin
	* 
	* @param unNombre 
	*/
	public void nombre(String unNombre)
	{
		nombre = unNombre;
	}


	/**
	* M�todo qu� devuelve apellido1 del pacinete Siapwin
	* 
	* @return apellido1
	*/
	public String apellido1()
	{
		return apellido1;
	}


	/**
	* M�todo qu� setea apellido1 del pacinete Siapwin
	* 
	* @param unApellido1 
	*/
	public void apellido1(String unApellido1)
	{
		apellido1 = unApellido1;
	}
	
		
	/**
	* M�todo qu� devuelve apellido2 del pacinete Siapwin
	* 
	* @return apellido2
	*/
	public String apellido2()
	{
		return apellido2;
	}


	/**
	* M�todo qu� setea apellido2 del pacinete Siapwin
	* 
	* @param unApellido2 
	*/
	public void apellido2(String unApellido2)
	{
		apellido2 = unApellido2;
	}
	

	/**
	* M�todo qu� devuelve fecha de nacimiento del pacinete Siapwin
	* 
	* @return nac_fecha
	*/
	public GregorianCalendar fechaNacimiento()
	{
		return nac_fecha;
	}


	/**
	* M�todo qu� setea fecha de nacimiento del pacinete Siapwin
	* 
	* @param unaFecha 
	*/
	public void fechaNacimiento(GregorianCalendar unaFecha)
	{
		nac_fecha = unaFecha;
	}
	

	/**
	* M�todo qu� devuelve el codigo3 del pacinete Siapwin
	* 
	* @return codigo3
	*/
	public String documento()
	{
		return codigo3;
	}


	/**
	* M�todo qu� setea el codigo3 Siapwin
	* 
	* @param unDocumento 
	*/
	public void documento(String unDocumento)
	{
		codigo3 = unDocumento;
	}


	/**
	* M�todo qu� devuelve el codigo de cliente del pacinete Siapwin
	* 
	* @return codigo_cliente
	*/
	public String codigoCliente()
	{
		return codigo_cliente;
	}


	/**
	* M�todo qu� setea el codigo de cliente Siapwin
	* 
	* @param unCodigo 
	*/
	public void codigoCliente(String unCodigo)
	{
		codigo_cliente = unCodigo;
	}


	/**
	* M�todo qu� devuelve telefono del pacinete Siapwin
	* 
	* @return telefono1
	*/
	public String telefono1(){
		return telefono1;
	}
	
	
	/**
	* M�todo qu� setea telefono del pacinete Siapwin
	* 
	* @param unTelefono1 
	*/
	public void telefono1(String unTelefono1){
		telefono1 = unTelefono1;
	}


	/**
	* M�todo qu� devuelve codigo de sexo del pacinete Siapwin
	* 
	* @return codigo_sexo
	*/
	public SexoSiapwin codigo_sexo(){
		try {
			codigo_sexo.fetchRealFor(this, "codigo_sexo");
		}
		catch(Exception ex){
			LogFile.log("PacienteSiapwin: codigo_sexo: No se pudo encontrar objeto."+ex);
		}
		return (SexoSiapwin)codigo_sexo;
	}
	
	
	/**
	* M�todo qu� setea codigo de sexo del pacinete Siapwin
	* 
	* @param unCodigoSexo 
	*/
	public void codigo_sexo(ProxyInterface unCodigoSexo){
		codigo_sexo = unCodigoSexo;
	}


	/**
	* M�todo qu� devuelve el nombre del sistema a la cu�l pertenece el paciente
	*
	* @return String  nombre del sistema.
	*/
	public String nombreSistema() {
		return new String("Siapwin");   
	} // END OF String nombreSistema()
	
	
	
	public String oid()
	{
		return codigo_cliente;
	}

	public void oid(String unCodigo)
	{
		codigo_cliente = unCodigo;
	}

}