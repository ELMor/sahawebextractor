package com.ssi.model.salud;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.reflect.InvocationTargetException;
import com.ssi.persistence.model.*;
import com.ssi.model.salud.*;

/**
* Clase que representa a un Episodio para un Paciente determinado.
*/

public abstract class EpisodioSalud extends ReadWritePersistentObject{
	private Paciente unPaciente = null;



	/**
	* M�todo qu� permite setear el Paciente vinculado a �sta instancia de episodio.
	*
	* @param paciente    Instancia de la Clase Paciente.
	*/
	public void paciente(Paciente paciente){
		unPaciente = paciente;
	} // END OF Episodio()

	/**
	* M�todo qu� permite devolver el Paciente vinculado a �sta instancia de episodio.
	*
	* @return unPaciente    Instancia de la Clase Paciente.
	*/
	public Paciente paciente(){
		return unPaciente;
	} // END OF getPaciente()

	/**
	* M�todo qu� permite devolver el nombre del Sistema al cu�l est� asociado �sta instancia
	* de Episodio; para ser usado por ejemplo en una pantalla como dato informativo.
	*
	* @return String   Descripcion expl�cita del nombre del Sistema al que se asocia el episodio.
	*/
	public String nombreSistema(){
		return sistema().nombreSistema();
	} // END OF getNombreSistema()

	/**
	* M�todo abstracto que permite devolver el sistema al cu�l se asocia �sta instancia de
	* Episodio.
	*
	* @return SistemaSalud   Instancia del Sistema al cu�l se asocia el Episodio. Por ejemplo
	*                        Sistema Siapwin � Sistem a Novahis.
	*/
	public abstract String ve_ep_ref();
	public abstract String ve_ep_fecha();
	public abstract String ve_ep_estado();
	public abstract SistemaSalud sistema();
	public abstract String ve_ep_icd_cod();
	public abstract String ve_ep_fechaend();
	public abstract String ve_ep_servicio();
	public abstract String centroDescripcion();
	java.lang.String diagnostico;
    java.lang.String servicio;
	
	/**
 * Insert the method's description here.
 * Creation date: (15/02/01 16:00:46)
 */
public EpisodioSalud() {
	
	unPaciente = new Paciente();
	
	
	}
/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:17:59)
 * @return java.lang.String
 */
public abstract java.lang.String getDiagnostico();

/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:17:59)
 * @return java.lang.String
 */
public abstract java.lang.String getServicio();

		
	/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:17:59)
 * @param newDiagnostico java.lang.String
 */
public void setDiagnostico(java.lang.String newDiagnostico) {
	diagnostico = newDiagnostico;
}	/**
	* M�todo qu� devuelve ve_cond_pk 
	*
	* @return ve_cond_pk
	*/
	abstract public String ve_cond_pk();
	/**
	* M�todo qu� setea ve_cond_pk 
	*
	* @param unVe_cond_pk
	*/
	abstract public void ve_cond_pk(String unVe_cond_pk);
	/**
	* M�todo qu� devuelve ve_enc_apert 
	*
	* @return ve_enc_apert
	*/
	abstract public String ve_enc_apert();	/**
	* M�todo qu� setea ve_enc_apert 
	*
	* @param unVe_enc_apert
	*/
	abstract public void ve_enc_apert(String unVe_enc_apert);
	/**
	* M�todo qu� devuelve ve_ep_cond 
	*
	* @return ve_ep_cond
	*/
abstract public String ve_ep_cond();
	/**
	* M�todo qu� setea ve_ep_cond 
	*
	* @param unVe_ep_cond
	*/
 abstract public void ve_ep_cond(String unVe_ep_cond); 	/**
	* M�todo qu� setea ve_ep_estado 
	*
	* @param unVe_ep_estado
	*/
 abstract public void ve_ep_estado(String unVe_ep_estado); 	/**
	* M�todo qu� devuelve ve_ep_etiq 
	*
	* @return ve_ep_etiq
	*/
	abstract public String ve_ep_etiq();	/**
	* M�todo qu� setea ve_ep_etiq 
	*
	* @param unVe_ep_etiq
	*/
	abstract public void ve_ep_etiq(String unVe_ep_etiq);
	/**
	* M�todo qu� setea ve_ep_fecha 
	*
	* @param unVe_ep_fecha
	*/
	abstract public void ve_ep_fecha(String unVe_ep_fecha);	/**
	* M�todo qu� devuelve ve_ep_fecha_clin 
	*
	* @return ve_ep_fecha_clin
	*/
   abstract public String ve_ep_fecha_clin();   	/**
	* M�todo qu� setea ve_ep_fecha_clin 
	*
	* @param unVe_ep_fecha_clin
	*/
   abstract public void ve_ep_fecha_clin(String unVe_ep_fecha_clin);   	/**
	* M�todo qu� setea ve_ep_fechaend 
	*
	* @param unVe_ep_fechaend
	*/
	abstract public void ve_ep_fechaend(String unVe_ep_fechaend);	/**
	* M�todo qu� devuelve ve_ep_hora 
	*
	* @return ve_ep_hora
	*/
	abstract public String ve_ep_hora();
	/**
	* M�todo qu� setea ve_ep_hora 
	*
	* @param unVe_ep_hora
	*/
	abstract public void ve_ep_hora(String unVe_ep_hora);	/**
	* M�todo qu� devuelve ve_ep_hora_clin 
	*
	* @return ve_ep_hora_clin
	*/
	abstract public String ve_ep_hora_clin();	/**
	* M�todo qu� setea ve_ep_hora_clin 
	*
	* @param unVe_ep_hora_clin
	*/
	abstract public void ve_ep_hora_clin(String unVe_ep_hora_clin);	/**
	* M�todo qu� setea ve_ep_icd_cod 
	*
	* @param unVe_ep_icd_cod
	*/
	abstract public void ve_ep_icd_cod(String unVe_ep_icd_cod);	/**
	* M�todo qu� devuelve ve_ep_idclient 
	*
	* @return ve_ep_idclient
	*/
	abstract public String ve_ep_idclient();	/**
	* M�todo qu� setea ve_ep_idclient 
	*
	* @param unVe_ep_idclient
	*/
	abstract public void ve_ep_idclient(String unVe_ep_idclient);	/**
	* M�todo qu� setea ve_ep_ref 
	*
	* @param unVe_ep_ref
	*/
	abstract public void ve_ep_ref(String unVe_ep_ref);	/**
	* M�todo qu� devuelve ve_ep_resolucion 
	*
	* @return ve_ep_resolucion
	*/
	abstract public String ve_ep_resolucion();	/**
	* M�todo qu� setea ve_ep_resolucion 
	*
	* @param unVe_ep_resolucion
	*/
	abstract public void ve_ep_resolucion(String unVe_ep_resolucion);	/**
	* M�todo qu� setea ve_ep_servicio 
	*
	* @param unVe_ep_servicio
	*/
	abstract public void ve_ep_servicio(String unVe_ep_servicio);	/**
	* M�todo qu� devuelve ve_fus_pk 
	*
	* @return ve_fus_pk
	*/
	abstract public String ve_fus_pk();	/**
	* M�todo qu� setea ve_fus_pk 
	*
	* @param unVe_fus_pk
	*/
	abstract public void ve_fus_pk(String unVe_fus_pk);	/**
	* M�todo qu� devuelve ve_sev_pk 
	*
	* @return ve_sev_pk
	*/
	abstract public String ve_sev_pk();
	/**
	* M�todo qu� setea ve_sev_pk 
	*
	* @param unVe_sev_pk
	*/
	abstract public void ve_sev_pk(String unVe_sev_pk);}// END OF Class EpisodioSalud