package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public interface ProxyInterface
{
	public Class classOfPrefix()
		throws ClassNotFoundException;

	public Class classRepresented()
		throws ClassNotFoundException;

	public abstract void fetchRealFor(PersistentObjectInterface anOwner,String aMessage)
		throws IllegalAccessException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   ClassNotFoundException,
			   InstantiationException,
			   SQLException,
			   Exception;
	//Fetches the real object represented by the proxy and sets it to
	//anOwner using aMessage as the setter.

	public boolean isProxy();

	public abstract String oid();

	public abstract void oid(String anOID);
}