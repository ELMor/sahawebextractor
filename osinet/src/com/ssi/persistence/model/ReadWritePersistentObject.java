package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.util.Vector;
import java.sql.SQLException;
//import com.ssi.persistence.ui.*;


public abstract class ReadWritePersistentObject extends PersistentObject
	implements	PersistentObjectInterface,
				ProxyInterface
{
	public String oid;


	public void delete()
		throws ClassNotFoundException,
			   NoSuchMethodException,
			   SQLException
	{
		//Deletes the receiver from the DB
		this.persistenceManager().delete(this);
	}


	public boolean isPersistent()
	{
		//Answers whether the receiver is persistent or not
		return !(this.oid() == null);
	}

	public String oid()
	{
		return oid;
	}

	public void oid(String anOID)
	{
		oid = anOID;
	}

	public void save()
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				InstantiationException,
				SQLException
	{
		//Saves the receiver from the DB
		this.persistenceManager().save(this);
	}
}