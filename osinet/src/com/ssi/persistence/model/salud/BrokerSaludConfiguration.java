package com.ssi.persistence.model.salud;

import com.sybase.jdbcx.SybDriver;
import oracle.jdbc.driver.OracleDriver;

import com.ssi.persistence.model.*;
import com.ssi.persistence.model.JDBC.*;
import com.ssi.persistence.model.access.*;


public class BrokerSaludConfiguration extends PersistenceConfiguration{
	/**
	 * Answers the driver class name to use for the connection
	 */
	public String driverName()
	{
	   return "sun.jdbc.odbc.JdbcOdbcDriver";
	}

	public DBEngineType getDBEngineType()
	{
		return new DBAccessType();
	}
	
	public String databaseName()
	{
		return "testing";
	}

	public String getConnectionUser()
	{
		return "";
	}

	public String getConnectionPassword()
	{
		return "";
	}

	public PersistenceFactory persistenceFactory()
	{
		return new JDBCFactory(this);
	}

	public void setAccessorsTo(PersistenceManager aManager)
	{
		//Sets the accesso for the persistent object to be used
		//by the application. Subclases must use super when
		//redefining this method.
		//Note that you could explicity specify a new cache for
		//each accessor. See com.ssi.persistence.cache package.

		//aManager.addAccessor(new UserAccessor(aManager));
		super.setAccessorsTo(aManager);
	}


	/**
	 * Returns the number tries to make in order to connect to the DB.
	 */
	public int connectionAttempts()
	{
		return 10;
	}


	/**
	 * Returns the number of milliseconds to hold
	 * a requester of a connection if no connections
	 * are available
	 */
	public int poolWaitTime()
	{
		return 1000;
	}

	/**
	 * Retorna la cantidad de conecciones a utilizar dentro del pool.
	 */
	public int maxConnections()
	{
		return 1;
	}

}