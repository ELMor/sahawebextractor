package com.ssi.persistence.model.salud;

import com.sybase.jdbcx.SybDriver;
import oracle.jdbc.driver.OracleDriver;

import com.ssi.persistence.model.*;
import com.ssi.persistence.model.JDBC.*;
import com.ssi.persistence.model.sybase.*;


public abstract class SistemaSaludConfiguration extends PersistenceConfiguration{
	/**
	 * Answers the driver class name to use for the connection
	 */
	public String driverName()
	{
	   return "sun.jdbc.odbc.JdbcOdbcDriver";
	}

	public DBEngineType getDBEngineType()
	{
		return new DBSybaseType();
	}
	
	public PersistenceFactory persistenceFactory()
	{
		return new JDBCFactory(this);
	}

}