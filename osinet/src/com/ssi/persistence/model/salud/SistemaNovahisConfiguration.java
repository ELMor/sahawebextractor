package com.ssi.persistence.model.salud;

import com.sybase.jdbcx.SybDriver;
import oracle.jdbc.driver.OracleDriver;

import com.ssi.persistence.model.*;
import com.ssi.persistence.model.JDBC.*;
import com.ssi.model.salud.*;


public class SistemaNovahisConfiguration extends SistemaSaludConfiguration{

	public String databaseName()
	{
		return "Novahis";
	}

	public String getConnectionUser()
	{
		return "dba";
	}

	public String getConnectionPassword()
	{
		return "sql";
	}

	public void setAccessorsTo(PersistenceManager aManager)
	{
		//Sets the accesso for the persistent object to be used
		//by the application. Subclases must use super when
		//redefining this method.
		//Note that you could explicity specify a new cache for
		//each accessor. See com.ssi.persistence.cache package.

		aManager.addAccessor(PacienteNovahisAccessor.getInstance(aManager));
		aManager.addAccessor(SexoNovahisAccessor.getInstance(aManager));
		aManager.addAccessor(EpisodioAccessor.getInstance(aManager));
		aManager.addAccessor(EncuentroAccessor.getInstance(aManager));
		aManager.addAccessor(ActividadAccessor.getInstance(aManager));
		
		//GA ini
		aManager.addAccessor(UserAccessor.getInstance(aManager));
		//GA fin
		
		//AN
		aManager.addAccessor(EpisodioNovahisAccessor.getInstance(aManager));
		aManager.addAccessor(EncuentroNovahisAccessor.getInstance(aManager));
		aManager.addAccessor(ActividadNovahisAccessor.getInstance(aManager));
		
	 } 


	/**
	 * Returns the number tries to make in order to connect to the DB.
	 */
	public int connectionAttempts()
	{
		return 10;
	}


	/**
	 * Returns the number of milliseconds to hold
	 * a requester of a connection if no connections
	 * are available
	 */
	public int poolWaitTime()
	{
		return 1000;
	}

	/**
	 * Retorna la cantidad de conecciones a utilizar dentro del pool.
	 */
	public int maxConnections()
	{
		return 3; //10
	}

}