package com.ssi.persistence.model;

import java.util.Date;
import java.sql.SQLException;
import com.ssi.log.model.*;
//import com.ssi.persistence.ui.*;

public class QueryManager
{
	// Inst Vars
	private LogInterface queryLog;
	private PersistenceManager persistenceManager;
	boolean logsQueries = false;


	public QueryManager(PersistenceManager aPersistenceManager)
	{
	    this.setPersistenceManager(aPersistenceManager);
		this.setQueryLog();
	}



	private DBCommand commandFor(String aStringCommand) throws SQLException
	{
		DBCommand newCommand = this.persistenceFactory().dbCommand(aStringCommand);
		newCommand.setActiveConnection(this.getConnection());
		newCommand.setCommandString(aStringCommand);
		return newCommand;
	}

	private DBConnection getConnection() throws SQLException
	{
		return this.getPersistenceManager().getConnection();
	}

	private DBResultSet executeCommand(DBCommand aCommand) throws SQLException
	{
		return aCommand.execute();
	}

	public DBResultSet executeCommandFor(String aStringCommand) throws SQLException
	{
		this.queryLog().logString(aStringCommand);
		Date startTime = new Date();
		DBResultSet answer = this.executeCommand(this.commandFor(aStringCommand));
		Date endTime = new Date();
		this.queryLog().logString("   Starting: " + startTime.toString());
		this.queryLog().logString("   Ending: " + endTime.toString());
		this.queryLog().logStringCR("   Lasted: " + (endTime.getTime() - startTime.getTime()));
		return answer;
	}

	public void logQueries(boolean aBoolean)
	{
		logsQueries = aBoolean;
		this.setQueryLog();
	}

	public boolean logsQueries()
	{
		return logsQueries;
	}

	private PersistenceFactory persistenceFactory()
	{
		return this.getPersistenceManager().persistenceFactory();
	}

	private PersistenceManager getPersistenceManager()
	{
		return this.persistenceManager;
	}

	private  void setPersistenceManager(PersistenceManager aManager)
	{
		this.persistenceManager = aManager;
	}

	public LogInterface queryLog()
	{
		return queryLog;
	}

	private void queryLog(LogInterface aQueryLog)
	{
		queryLog = aQueryLog;
	}

	protected void setQueryLog()
	{
		if(this.logsQueries())
		{
			this.queryLog(new Log());
		}
		else
		{
			this.queryLog(new NoLog());
		}
	}

	public void startLogging()
	{
		this.logQueries(true);
	}

	public void stopLogging()
	{
		this.logQueries(false);
	}
}