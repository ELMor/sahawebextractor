package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

public class PersistentField
{
	// Instance Vars
	DBFieldType fieldType;
	String setterName;
	String columnName;
	int positionIndex;

	//Class methods
	public PersistentField(String name, String setter, int index, DBFieldType type)
	{
		positionIndex = index;
		columnName = name;
		fieldType = type;
		setterName = setter;
	}
public String columnName()
{
	return columnName;
}
	// Instance methods
	public DBFieldType fieldType()
	{
		return fieldType;
	}
	private DBField getField(DBResultSet aSet)
	{
		return aSet.getField(this.columnName());
	}
	private Method getter(Class targetClass) throws ClassNotFoundException, NoSuchMethodException
	{
		return this.fieldType().getGetterMethodFrom(targetClass,
											  this.setterName());
	}
public String oidForQuery(String anOID)
	throws Exception
{
	return this.fieldType().oidForQuery(anOID);
}
	public int positionIndex()
	{
		return positionIndex;
	}
	private Method setter(Class targetClass) throws ClassNotFoundException, NoSuchMethodException
	{
		Method aMethod = null;
		try {
			aMethod = this.fieldType().getSetterMethodFrom(targetClass, this.setterName());
		} catch (NoSuchMethodException ex) {
			throw new NoSuchMethodException ( "Setter " + this.setterName() + " with type " + this.fieldType() + " not found" );
		} catch (ClassNotFoundException ex) {
			throw new ClassNotFoundException ( "Class "+ targetClass + " not found" );
		}
		return aMethod;
	}
	public String setterName()
	{
		return setterName;
	}
	public void setValueTo(Object anInstance, DBResultSet aSet, PersistenceAccessor anAccessor)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				SQLException
	{
		this.fieldType().setValueTo(anInstance, this.setter(anInstance.getClass()), this.getField(aSet), anAccessor);
	}
/**
 * This method was created in VisualAge.
 * @return java.lang.String
 */
public String toString() {
	return "Col:" + columnName + "|ndx:" + positionIndex + "|setter:" + setterName + "|DBFType:" + (fieldType == null ? fieldType.toString() : "null");

}
	public Object valueOf(Object anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
		return this.fieldType().getValueOf(anInstance, this.getter(anInstance.getClass()));
	}
}