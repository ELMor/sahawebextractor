package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;
import java.beans.PropertyDescriptor;

public class DBIntegerFieldType extends DBFieldType
{
	public String fieldClassName()
	{
		return "java.lang.Integer";
	}
public String oidForQuery(String anOID)
{
	return anOID;
}
public static boolean represents(Class aClass)
{
	try
	{
		return aClass == Class.forName("java.lang.Integer");
	}
	catch (ClassNotFoundException ex)
	{
		return false;
	}
}
public static boolean represents(Field aField)
{
	try
	{
		return aField.getType() == Class.forName("java.lang.Integer");
	}
	catch (ClassNotFoundException ex)
	{
		return false;
	}
}
	public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
		throws IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		aSetter.invoke(newInstance, new Integer[] {aField.getInteger()});
	}
}