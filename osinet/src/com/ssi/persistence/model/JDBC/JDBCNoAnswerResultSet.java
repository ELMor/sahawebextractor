package com.ssi.persistence.model.JDBC;

/**
 * This type was created in VisualAge.
 */

import java.sql.*;

public class JDBCNoAnswerResultSet extends JDBCResultSet
{
/**
 * Close the receiver
 */
public void close() throws SQLException
{
}
/**
 * getEOF method comment.
 */
public boolean getEOF()
{
	return true;
}
/**
 * getField method comment.
 */
public com.ssi.persistence.model.DBField getField(int aFieldIndex)
{
	return null;
}
/**
 * getField method comment.
 */
public com.ssi.persistence.model.DBField getField(String aFieldName)
{
	return null;
}
/**
 * moveNext method comment.
 */
public void moveNext() throws SQLException
{
}
}