package com.ssi.persistence.model.JDBC;

/**
 * This type was created in VisualAge.
 */

import com.ssi.persistence.model.DBConnection;
import com.ssi.persistence.model.DBResultSet;
import java.sql.*;

public class JDBCAnswerCommand extends JDBCCommand
{
/**
 * setActiveConnection method comment.
 */
static public boolean canHandle(String aCommandString)
{
	return aCommandString.toLowerCase().startsWith("select");
}
/**
 * setActiveConnection method comment.
 */
public DBResultSet execute() throws SQLException
{
	return new JDBCAnswerResultSet(this, this.commandString);
}
}