package com.ssi.persistence.model.JDBC;

/**
 * This type was created in VisualAge.
 */

import java.sql.*;

abstract public class JDBCResultSet extends com.ssi.persistence.model.DBResultSet
{
/**
 * Do nothing since this method is not supported by JDBC
 */
public int getRecordCount()
{
	return 0;
}
/**
 * Do nothing since this method is not supported by JDBC
 */
public void moveFirst()
{
}
/**
 * Do nothing since this method is not supported by JDBC
 */
public void open(com.ssi.persistence.model.DBCommand aCommand) {
}
}