package com.ssi.persistence.model.JDBC;

/**
 * This type was created in VisualAge.
 */

import java.sql.*;
import com.ssi.persistence.model.*;

public class JDBCFactory extends com.ssi.persistence.model.PersistenceFactory {

	
	public JDBCFactory(PersistenceConfiguration aConfiguration)
	{
		super(aConfiguration);
	}

	
	/**
	 * Answers an appropriate connection String for a JDBC connection
	 */
	public String connectionStringFor(String aDatabaseName, String aUserName, String aPassword)
	{
		return "jdbc:odbc:" + aDatabaseName
				   + ";UID=" + aUserName
				   + ";PWD=" + aPassword ;
	}

	/**
	 * Answers an appropriate connection String for a JDBC connection
	 */
	public String connectionStringFor(String aDatabaseName)
	{
		return "jdbc:odbc:" + aDatabaseName;
	}
	
	/**
	 * dbCommand method comment.
	 */
	public com.ssi.persistence.model.DBCommand dbCommand(String aCommandString)
	{
		return JDBCCommand.instanceFor(aCommandString);
	}
	
	/**
	 * dbConnection method comment.
	 */
	public com.ssi.persistence.model.DBConnection dbConnection()
	{
		return null;
	}

	
	/**
	 * Answers a JDBCConnection object
	 */
	public DBConnection dbConnection(String aConnectionString, ConnectionPool aPool)
		throws SQLException
	{
		try { Class.forName(this.driverName()); }
		catch (ClassNotFoundException anException)
		{ return null; }

		return new JDBCConnection(DriverManager.getConnection(aConnectionString), aPool);
	}
	

	/**
	 * Answers a JDBCConnection object
	 */
	public DBConnection dbConnection(String aConnectionString, String user, String password, ConnectionPool aPool)
		throws SQLException
	{
		try { Class.forName(this.driverName()); }
		catch (ClassNotFoundException anException)
		{ return null; }

		return new JDBCConnection(DriverManager.getConnection(aConnectionString, user, password), aPool);
	}


	/**
	 * Answers the driver class name to use for the connection
	 */
	 public String driverName()
	 {
		return this.getPersistenceConfiguration().driverName();
	 }

}