package com.ssi.persistence.model.JDBC;

/**
 * This type was created in VisualAge.
 */

import java.sql.*;
import com.ssi.persistence.model.DBField;
import java.util.Vector;

import com.ssi.util.LogFile;public class JDBCAnswerResultSet extends JDBCResultSet
{
	//Inst Vars
	Boolean isEOF = new Boolean(false);
	ResultSet resultSet;
	Vector fields = new Vector();
	JDBCAnswerCommand command = null;
/**
 * getField method comment.
 */
public JDBCAnswerResultSet(JDBCAnswerCommand aCommand, String aQueryString) throws SQLException
{
	this.command = aCommand; 	// Me guardo el command para poder cerrar el statement Statement.
	this.resultSet = command.command().executeQuery(aQueryString);
	this.moveNext();
}
/**
 * getField method comment.
 */
private void addField(JDBCField aField)
{
	this.fields().addElement(aField);
}
/**
 * getField method comment.
 */
private JDBCField basicGetField(int anIndex)
{
	JDBCField field = null;
	boolean found = false;
	int i = 0;
	JDBCField currentField = null;
	while (!found && i<this.fields().size()) {
		currentField = (JDBCField)this.fields().elementAt(i);
		found = (currentField.columnIndex() == anIndex);
		if (found) field = currentField;
		i++;
	}
	return field;
}
/**
 * getField method comment.
 */
private JDBCField basicGetField(String anColumnName)
{
	JDBCField field = null;
	boolean found = false;
	int i = 0;
	JDBCField currentField = null;
	while (!found && i<this.fields().size()) {
		currentField = (JDBCField)this.fields().elementAt(i);
		found = (currentField.columnName().equals(anColumnName));
		if (found) field = currentField;
		i++;
	}
	return field;
}
/**
 * close method comment.
 */
public void close() throws SQLException
{
	this.resultSet.close();
	LogFile.log("JDBCAnswerResultSet::close()");
	this.command.closeCommand(); // Cierro el Statement.
}
/**
 * getField method comment.
 */
public boolean containsField(int anIndex)
{
	JDBCField lFound = this.basicGetField(anIndex);
	return lFound != null;
}
/**
 * getField method comment.
 */
public boolean containsField(String aColumnName)
{
	JDBCField lFound = this.basicGetField(aColumnName);
	return lFound != null;
}
/**
 * getField method comment.
 */
public Vector fields()
{
	return fields;
}
/**
 * This method was created in VisualAge.
 */
private void flush() {
	this.fields().removeAllElements();
}
/**
 * getEOF method comment.
 */
public boolean getEOF()
{
	return isEOF.booleanValue();
}
/**
 * getField method comment.
 */
public DBField getField(int aFieldIndex)
{
	if (!this.containsField(aFieldIndex))
		this.addField(new JDBCField(this.resultSet, aFieldIndex));

	return this.basicGetField(aFieldIndex);

}
/**
 * getField method comment.
 */
public com.ssi.persistence.model.DBField getField(String aFieldName)
{
	if (!this.containsField(aFieldName))
		this.addField(new JDBCField(this.resultSet, aFieldName));

	return this.basicGetField(aFieldName);
}
/**
 * isEOF accessor
 */
public Boolean isEOF()
{
	return isEOF;
}
/**
 * isEOF accessor
 */
public void isEOF(Boolean aBoolean)
{
	isEOF = aBoolean;
}
/**
 * moveNext method comment.
 */
public void moveNext() throws SQLException
{
	if (!this.resultSet().next())
	{
		this.isEOF(new Boolean(true));
	}
	this.flush();
}
/**
 * getField method comment.
 */
public ResultSet resultSet()
{
	return resultSet;
}
}