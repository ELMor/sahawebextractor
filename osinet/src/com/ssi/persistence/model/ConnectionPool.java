package com.ssi.persistence.model;

import java.util.*;
import java.sql.*;

/**
 * Esta Clase controla la coneccion a la base de Datos
 * Crea una estructura Stack en la que se ponen las connecciones disponibles
 * Se implementan las funciones que piden y devuelven una coneccion
 *
 * @author Juan Pablo Urtar�n
 * @version 1.0
*/
public class ConnectionPool {
	
	private int waitTime;                  // tiempo de espera
	private int attempts;                  // cant de intentos en caso de que la conexion este ocupada
	private Stack stack = new Stack();
	private boolean isConnected = false;
	private PersistenceManager persistenceManager= null;

	
	/**
	 * Se ha creado el siguiente constructor como private para evitar que se pueda
	 * instanciar esta clase y de esta forma lograr un objeto singleton.
	 *
	 * @param   url  URL de la base de datos
	 * @param   login  usuario para logearse a la base de datos
	 * @param   password  password para logearse a la base de datos
	 * @param   attempts  cantidad de intentos al tratar de conectarse
	 * @param   MaxConec  cantidad m�xima de conexiones
	 * @param   waitTime  tiempo de espera entre intentos de conexi�n
	 */
	public ConnectionPool(int attempts, int waitTime, PersistenceManager unManager) {
		this.attempts = attempts;
		this.waitTime = waitTime;
		this.persistenceManager = unManager;
	}

	
	/**
	 * Este metodo coloca las conexiones disponibles en el stack.
	 *
	 * @param   url  URL de la base de datos
	 * @param   login  usuario para logearse a la base de datos
	 * @param   password  password para logearse a la base de datos
	 */
	public void connect(String login,String password) {
		int max = this.maxConections();
		String aConnectionString = this.connectionString();
		for (int i=0; i< max ;i++) 
		{
			try 
			{
		        DBConnection connection = this.persistenceFactory().dbConnection(aConnectionString, login, password, this);
				this.stack().push(connection);
			}
			catch(Exception e) {
				System.out.println("Error: " + e.getMessage());
			}
		}
		this.isConnected(true);
	}

	/**
	 * Retorna el objeto que contruye las clases del JDBC.
	 */
	public PersistenceFactory persistenceFactory()
	{
		return this.persistenceManager().persistenceFactory();
	}
	
	/**
	 * Retorna el string usado para conectarse a la DB.
	 */
	private String connectionStringFor(String aUserName, String aPassword)
	{
		return this.persistenceFactory().connectionStringFor(this.databaseName(), aUserName, aPassword);
		/*;"DSN=" + this.databaseName()
			   + ";UID=" + aUserName
			   + ";PWD=" + aPassword + ";";*/
	}

	/**
	 * Retorna el string usado para conectarse a la DB.
	 */
	private String connectionString()
	{
		return this.persistenceFactory().connectionStringFor(this.databaseName());
		/*;"DSN=" + this.databaseName()*/
	}
	
	/**
	 * Retorna el nombre de la base de datos a utilizar.
	 */
  	private String databaseName()
	{
		return this.persistenceManager().databaseName();
	}
	
	
	/**
	 * Retorna la cantidad de conecciones a utilizar dentro del pool.
	 */
	private int maxConections()
	{
		return this.persistenceManager().maxConnections();
	}

	
	/**
	 * Este metodo devuelve una conexion a quien lo solicita.
	 *
	 * @return     Connection
	 * @exception   SQLException  
	 */
	public DBConnection getConnection() throws SQLException {
		synchronized(this.stack()) {
			int cnt = attempts;
			while(this.stack().empty()) { 
				try {
					this.stack().wait(waitTime);
				}
				catch(Exception e1) {
					throw new SQLException("Se ha interrumpido la conexion.");
				}
				if(--cnt < 1)
					throw new SQLException("Todas las conexiones estan ocupadas.");
			}
			return (DBConnection)this.stack().pop();
		}
	}

	
	/**
	 * Este metodo toma la conexion que se le pasa como parametro y la
	 * agrega al stack, haciendola disponible nuevamente.
	 *
	 * @param   con  objeto de conexi�n
	 */
	 public void returnConnection(DBConnection con) {
		synchronized(this.stack()) {
			this.stack().push(con);
			this.stack().notify();
		}
	}
	
	/**
	 * Este metodo cierra las conexiones con la base de datos
	 *
	 */
	public void close() {
		for (int i=0; i<this.maxConections(); i++) {
			DBConnection connection = (DBConnection)this.stack().pop();
			try {
				 connection.close();
			}
			catch(Exception e) {
				//LogFile.log("Coneccion::finalize()::Error: " + e);
			}
			this.isConnected(false);
		}
	}
	
	
	/**
	 * Retorna el stack de conecciones a la DB.
	 */
	 public Stack stack()
	 {
		return stack;
	 }
	
	
	/**
	 * Retorna el persistence manager.
	 */
	 public PersistenceManager persistenceManager()
	 {
		return persistenceManager;
	 }

	/**
	 * Retorna si el receptor est� conectado a la DB o no.
	 */
   	public boolean isConnected()
	{
		return isConnected;
	}

	/**
	 * Setea si el receptor est� conectado a la DB o no.
	 */
   	public void isConnected(boolean aBoolean)
	{
		isConnected = aBoolean;
	}


}