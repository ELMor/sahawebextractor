package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;
import java.beans.PropertyDescriptor;

public class DBOIDFieldType extends DBFieldType
{
	// Inst Vars
	DBFieldType type;

	public String fieldClassName()
	{
		return this.type().fieldClassName();
	}
	
	public Object getValueOf(Object anInstance, Method aGetter)
		throws IllegalAccessException,
			   InvocationTargetException
	{
		return this.type().getValueOf(anInstance, aGetter);
	}
	
	//Class methods
	public static String nextOIDFromSet(DBResultSet aSet) throws SQLException
	{
		return aSet.getField("oid").getString();
	}
	
	public String oidForQuery(String anOID)
		throws Exception
	{
		return this.type().oidForQuery(anOID);
	}

	public static boolean represents(Field aField)
	{
		try
		{
			return (aField.getType() == Class.forName("java.lang.String")) & (aField.getName().equals("oid"));
		}
		catch (ClassNotFoundException ex)
		{
			return false;
		}
	}

	public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
		throws IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		this.type().setValueTo(newInstance, aSetter, aField, anAccessor);
	}

	public DBFieldType type() {
	 // Getter for the type instance variable
	 if (type == null)
	 	{
		 	this.type(new DBStringFieldType());
		}
	 return type;
	}

	public void type(DBFieldType aType) {
	 // Setter for the type instance variable
	 type = aType;
	}
}