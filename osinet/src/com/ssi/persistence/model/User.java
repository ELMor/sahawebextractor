package com.ssi.persistence.model;

import java.util.Vector;
import java.util.Enumeration;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
//import com.ssi.persistence.ui.* ;

public class User extends ReadWritePersistentObject{
    
    //Inst vars
	String name;
	String password;
	
	public static PersistenceManager getPersistenceManager() throws ClassNotFoundException{
		//Answers the persistence manager for the receiver
		return User.getPersistenceAccessor().persistenceManager();
	}	
	
	public static PersistenceAccessor getPersistenceAccessor()
	{
		return UserAccessor.getInstance();
	}

	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return User.getPersistenceManager();
	}
		
	public static User find(String aName)
	{
	  try
	  {
		PersistenceManager manager = User.getPersistenceManager() ;
		Vector instances = manager.instancesOf(Class.forName("com.ssi.persistence.model.User"));
		for ( int i = 0 ; i < instances.size() ; i++ )
		{
			User instance = ((User)instances.elementAt(i));
			if ( instance.name().equals(aName) )
			{
				return instance ;
			}
		}
		return null ;
	  }
	  catch (Exception ex)
	  {
		//MessageViewer.show( ex.getMessage(), "User error!");
		return null ;
	  }
	}

	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return User.getPersistenceManager().instancesOf(Class.forName("com.ssi.persistence.model.User"));
	}

	public String name()
	{
		return name;
	}

	public void name(String aName)
	{
		name = aName;
	}

	public String oid()
	{
		return oid;
	}

	public void oid(String anOID)
	{
		oid = anOID;
	}

	public String password()
	{
		return password;
	}

	public void password(String aPassword)
	{
		password = aPassword;
	}


	public static User userNamed(String aName)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException,Exception
	{
	    User answer = null;
	    String where = "where Syslogin = '"+ aName +"'";
	    PersistenceManager pm = null;
	    try{
    	    pm = User.getPersistenceManager();
    	}
    	catch(Exception exe){
    	    com.ssi.util.LogFile.log("exception en User.getPersistenceManager() = " + exe);
    	    throw(exe);
    	}
		Vector instances = pm.instancesOfWhere(Class.forName("com.ssi.persistence.model.User"), where);
		if (!instances.isEmpty())
		{
		    answer = (User)instances.firstElement();
		}
		return answer;
	}
}