package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;
import java.beans.PropertyDescriptor;

public class DBStringFieldType extends DBFieldType
{
	public String fieldClassName()
	{
		return "java.lang.String";
	}

	public Object getValueOf(Object anInstance, Method aGetter)
		throws IllegalAccessException,
			   InvocationTargetException
	{
		return "'" + aGetter.invoke(anInstance, null) + "'";
	}

	public String oidForQuery(String anOID)
	{
		return "'" + anOID + "'";
	}

	public static boolean represents(Field aField)
	{
		try
		{
			return (aField.getType() == Class.forName("java.lang.String")) & !(aField.getName().equals("oid"));
		}
		catch (ClassNotFoundException ex)
		{
			return false;
		}
	}

	public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
		throws IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		aSetter.invoke(newInstance, new String[] {aField.getString()});
	}
}