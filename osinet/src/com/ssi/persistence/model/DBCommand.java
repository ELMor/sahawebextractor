package com.ssi.persistence.model;

import java.sql.*;

public abstract class DBCommand
{
//Executes the receiver's command and answers the result set

public abstract DBResultSet execute() throws SQLException;
//Sets the connection the receiver will use to execute its command

public abstract void setActiveConnection(DBConnection aConnection);
//Sets the receiver's command

public abstract void setCommandString(String aCommandString);

public abstract void closeCommand() throws SQLException;

}