package com.ssi.persistence.model;

import java.util.Vector;
import com.ssi.persistence.cache.*;
import com.ssi.util.*;

public class UserAccessor extends PersistenceAccessor
{

	public static String className()
	{
		return "com.ssi.persistence.model.UserAccessor";
	}

	public static PersistenceAccessor getInstance()
	{   
	    return AccessorCache.getInstance().get(className());
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new UserAccessor(aManager);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new UserAccessor(aManager, aCache);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	protected UserAccessor(PersistenceManager aManager)
	{
		super (aManager);
	}

	protected UserAccessor(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    super(aManager, aCache);
	}

	public  Vector addPersistentFields( Vector aAnswer )
	{
		aAnswer.addElement(new PersistentField("Syslogin", "oid", 1, new DBOIDFieldType()));
		aAnswer.addElement(new PersistentField("Syslogin", "name", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("Syspwd", "password", 1, new DBStringFieldType()));
		return aAnswer;
	}
	public Vector collectionSpecs()
	{
		return new Vector();
	}
	public String oidPrefix()
	{
		return "USR";
	}
	public String persistentClassName()
	{
		return "com.ssi.persistence.model.User";
	}
	public String tableName()
	{
		//return "User";
		return "SYS_USU";
	}
}