package com.ssi.persistence.model;

/**
 * The present persistenceframework supports:
 *   - One cache for each persistence accessor. This cache can
 *     be configured in order to use different caching algorithms
 *   - Protocol mapping
 *   - Persistent collection  can contain objects from different
 *     classes. Note that an object can NOT have more than one
 *     collection with object from the same class.
 *   - Proxy operations for all objects. This includes objects
 *     whithin collections.
 *   - Query logging of all database operations.
 */


import java.util.*;
import java.lang.reflect.*;
import java.sql.SQLException;

import com.ssi.persistence.model.JDBC.*;
import com.ssi.util.LogFile;

public class PersistenceManager
{
	public DBEngineType dbEngineTypeForAccessor(PersistenceAccessor anAccessor)
	{
		DBEngineType answer = this.dbEngineType().getClone();
		answer.persistenceAccessor(anAccessor);
		return answer;
	}
	public PersistenceConfiguration persistenceConfiguration()
	{
		return this.persistenceConfiguration;
	}

	public void persistenceConfiguration(PersistenceConfiguration aConfiguration)
	{
		this.persistenceConfiguration = aConfiguration;
	}
	private PersistenceConfiguration persistenceConfiguration;
	// This is the Facade of the persistence framework.
	//WARNING: All database operations should pass through
	//this object.


	//Instance Vars
	private DBEngineType dbEngineType;
	private DBConnection connection;
	private Vector accessors;
	private ConnectionPool connectionPool;
	private QueryManager queryManager;



	public PersistenceManager(PersistenceConfiguration aConfiguration)
	{
		accessors = new Vector();
		queryManager = new QueryManager(this);
		persistenceConfiguration = aConfiguration;
		dbEngineType = aConfiguration.getDBEngineType();
		aConfiguration.configurePersistenceManager(this);
	}



	protected PersistenceAccessor accessorForClass(Class aClass)
	    throws ClassNotFoundException,
		       NoSuchMethodException
	{
		PersistenceAccessor result = null;
		Enumeration enumerator = this.accessors().elements();
		boolean notFound = true;
		while (enumerator.hasMoreElements() && notFound)
		{
			PersistenceAccessor anAccessor = ((PersistenceAccessor)enumerator.nextElement());
			if(anAccessor.persistentClass() == aClass)
			{
				notFound = false;
				result = anAccessor;
			}
		}
		return result;
	}

	public PersistenceAccessor accessorForOID(String anOID)
	{
		String prefix = this.oidParser().oidPrefixFrom(anOID);
		return this.accessorForOIDPrefix(prefix);
	}

	private PersistenceAccessor accessorForOIDPrefix(String anOIDPrefix)
	{
		PersistenceAccessor result = null;
		Enumeration enumerator = this.accessors().elements();
		boolean notFound = true;
		while (enumerator.hasMoreElements() && notFound)
		{
			PersistenceAccessor anAccessor = ((PersistenceAccessor)enumerator.nextElement());
			if(anAccessor.oidPrefix().equals(anOIDPrefix))
			{
				notFound = false;
				result = anAccessor;
			}
		}
		return result;
	}

	public QueryManager getQueryManager()
	{
		return queryManager;
	}

	public void setQueryManager(QueryManager aQueryManager)
	{
		this.queryManager = aQueryManager;
	}

	private ConnectionPool connectionPool()
	{
		return connectionPool;
	}

	public void connectionPool(ConnectionPool aPool)
	{
		connectionPool = aPool;
	}

	private Vector accessors()
	{
		return accessors;
	}

	public void addAccessor(PersistenceAccessor anAccessor)
	{
		this.accessors().addElement(anAccessor);
	}


	public Class classForOIDPrefix(String anOIDPrefix)
		throws ClassNotFoundException
	{
		return this.accessorForOIDPrefix(anOIDPrefix).persistentClass();
	}

	public void closeConnection() throws SQLException
	{
		this.connectionPool().close();
	}

	public DBConnection getConnection() throws SQLException
	{
		LogFile.log("// PM::getConnection");
		return this.connectionPool().getConnection();
	}

	private DBEngineType dbEngineType()
	{
		return dbEngineType;
	}


	public void connect()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		String aUser = this.persistenceConfiguration().getConnectionUser();
		String aPassword = this.persistenceConfiguration().getConnectionPassword();
		this.connect(aUser, aPassword);
	}


	public void connect(String aUserName, String aPassword)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
	    //Connects the user to the database
		this.connectionPool().connect(aUserName, aPassword);
	}

	private String connectionStringFor(String aUserName, String aPassword)
	{
		return this.persistenceFactory().connectionStringFor(this.databaseName(), aUserName, aPassword);
		/*;"DSN=" + this.databaseName()
			   + ";UID=" + aUserName
			   + ";PWD=" + aPassword + ";";*/
	}

	public String databaseName()
	{
		return this.persistenceConfiguration().databaseName();
	}

	public int maxConnections()
	{
		return this.persistenceConfiguration().maxConnections();
	}

	public void delete(PersistentObjectInterface anInstance)
		throws ClassNotFoundException,
			   NoSuchMethodException,
			   SQLException
	{
		this.accessorForClass(anInstance.getClass()).delete(anInstance);
	}

	public DBResultSet executeQuery (String aQueryStatement) throws SQLException {

	    return this.getQueryManager().executeCommandFor(aQueryStatement);

	}

	public int indexStartPosition()
	{
		return this.persistenceConfiguration().indexStartPosition();
	}

	public int indexStopPosition()
	{
		return this.persistenceConfiguration().indexStopPosition();
	}

	public PersistentObjectInterface instanceForOIDFrom( String aOID , String aClassName )
		throws NoSuchMethodException,
				ClassNotFoundException,
				InvocationTargetException,
				InstantiationException,
				IllegalAccessException,
				SQLException,
				Exception
	{
	    return this.accessorForClass( Class.forName(aClassName) ).instanceOfOID(aOID);
	}

	public PersistentObjectInterface instanceOfOID(String anOID)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException,
			   Exception
	{
		return this.accessorForOID(anOID).instanceOfOID(anOID);
	}

	private Vector instancesInCollection(Class aClass, PersistentCollection aCollection)
		throws InstantiationException,
			  IllegalAccessException,
			  ClassNotFoundException,
			  InvocationTargetException,
			  NoSuchMethodException,
			  SQLException

	{
		return this.accessorForClass(aClass).buildInstancesFromCollection(aCollection);
	}

	public Vector instancesOf(Class aClass)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return this.accessorForClass(aClass).instances();
	}

	public Vector instancesOfSorted(Class aClass, String sortingClause)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return this.instancesOfWhereSorted(aClass, "", sortingClause);
	}

	public Vector instancesOfWhere(Class aClass, String whereClause)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException

	{
		//AN 16/03/01
		if(!whereClause.trim().endsWith("WHERE"))
		 return this.instancesOfWhereSorted(aClass, whereClause, "");
		else
		 return new Vector();
	}

	public Vector instancesOfWhereSorted(Class aClass, String whereClause, String sortingClause)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		if(!whereClause.trim().endsWith("WHERE"))
		 return this.accessorForClass(aClass).instancesOfWhereSorted(whereClause, sortingClause);
		else
		 return new Vector();
	}

	public boolean isConnected()
	{
		return this.connectionPool().isConnected();
	}

	public OIDParser oidParser()
	{
		return new OIDParser(this);
	}

	public String oidPrefixForObject(PersistentObjectInterface anObject)
		throws	ClassNotFoundException,
				NoSuchMethodException
	{
		return this.accessorForClass(anObject.getClass()).oidPrefix();
	}

	public PersistenceFactory persistenceFactory()
	{
		return this.persistenceConfiguration().persistenceFactory();
	}

	public int prefixStartPosition()
	{
		return this.persistenceConfiguration().prefixStartPosition();
	}

	public int prefixStopPosition()
	{
		return this.persistenceConfiguration().prefixStopPosition();
	}

	protected Vector proxiesForCollection(PersistentCollection aCollection)
		throws InstantiationException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		return this.accessorForClass(aCollection.owner().getClass()).proxiesForCollection(aCollection);
	}

	public void retrieveCollection(PersistentCollection aCollection)
		throws InstantiationException,
			  IllegalAccessException,
			  ClassNotFoundException,
			  InvocationTargetException,
			  NoSuchMethodException,
			  SQLException
	{
		Dictionary dictionary;
		Vector answer = new Vector();

		dictionary = aCollection.getInstancesByClass();
		Enumeration dictionaryIterator = dictionary.keys();
		while (dictionaryIterator.hasMoreElements())
		{
			Class aClass = ((Class)dictionaryIterator.nextElement());
			Vector instances = this.instancesInCollection(aClass, aCollection);
			Enumeration instancesIterator = instances.elements();
			while (instancesIterator.hasMoreElements())
			{
				answer.addElement(instancesIterator.nextElement());
			}
		}
		aCollection.collection(answer);
	}

	public void save(PersistentObjectInterface anInstance)
		throws IllegalAccessException,
			   InvocationTargetException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   InstantiationException,
			   SQLException
	{
		this.accessorForClass(anInstance.getClass()).save(anInstance);
	}

	public String tableNameFor(PersistentObjectInterface anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
		return this.accessorForClass(anInstance.getClass()).tableName();
	}

	public void truncateTable(Class aClass)
		throws IllegalAccessException,
			   InvocationTargetException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   InstantiationException,
			   SQLException
	{
		this.accessorForClass(aClass).truncateTable();
	}
/**
 * Insert the method's description here.
 * Creation date: (15/03/01 18:12:29)
 */
public void returnConnection(DBConnection conexion)  throws SQLException {

	LogFile.log("// PM::returnConnection");
	this.connectionPool().returnConnection(conexion);

 }  }