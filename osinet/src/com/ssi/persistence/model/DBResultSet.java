package com.ssi.persistence.model;

import java.sql.*;

public abstract class DBResultSet
{
	public abstract void close() throws SQLException;
	public abstract boolean getEOF();
	public abstract DBField getField(int aFieldIndex);
	public abstract DBField getField(String aFieldName);
	public abstract int getRecordCount();
	//Instance Methods

	//Implemented by subclasses
	public abstract void moveFirst();
	public abstract void moveNext() throws SQLException;
	public abstract void open(DBCommand aCommand);
}