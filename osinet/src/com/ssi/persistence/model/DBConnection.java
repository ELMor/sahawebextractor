package com.ssi.persistence.model;

import java.sql.SQLException;

public abstract class DBConnection
{
/**
 * close method comment.
 */
public void close() throws SQLException
{
}
	private ConnectionPool connectionPool;
	
	public abstract Object connection();
	public abstract boolean isConnected();


	public void returnConnection() throws SQLException
	{
	    this.getConnectionPool().returnConnection(this);
	}
	
	public void setConnectionPool(ConnectionPool aPool)
	{
	    this.connectionPool = aPool;
	}
	
	public ConnectionPool getConnectionPool()
	{
	    return this.connectionPool;
	}

}