package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;

public class OIDParser
{
	private PersistenceManager persistenceManager;
	
	public OIDParser(PersistenceManager aManager)
	{
		this.persistenceManager = aManager;
	}


	private String blankIndexString()
	{
		return "0000000000000000000000000000000000000".substring(0, this.indexLength());
	}

	public Class classOfOID(String anOID)
		throws ClassNotFoundException
	{
		return this.persistenceManager().classForOIDPrefix(this.oidPrefixFrom(anOID));
	}

	private DBResultSet executeCommandFor(String aStringCommand) throws SQLException
	{
		return this.queryManager().executeCommandFor(aStringCommand);
	}

	private int indexLength()
	{
		return (this.indexStopPosition() + 1) - this.indexStartPosition();
	}

	private int indexStartPosition()
	{
		return this.persistenceManager().indexStartPosition();
	}

	private int indexStopPosition()
	{
		return this.persistenceManager().indexStopPosition();
	}

	private String indexStringOf(int anIndex)
	{
		String fullIndex = this.blankIndexString() + anIndex;
		return fullIndex.substring(new Integer(anIndex).toString().length(), fullIndex.length());
	}

	private int lastOIDIndexFor(PersistentObjectInterface anObject)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				SQLException
	{
		DBResultSet result = this.executeCommandFor(this.lastOIDStringCommand(anObject));
		int lVal = this.oidIndexFrom(result.getField(1).getString());
		result.close() ;
		return lVal ;
	}

	private String lastOIDStringCommand(PersistentObjectInterface anObject)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
		return "select max(oid) from " + anObject.tableName();
	}

	//Instance Methods
	public String nextOIDFromSet(DBResultSet aSet) throws SQLException
	{
		return DBOIDFieldType.nextOIDFromSet(aSet);
	}

	private int nextOIDIndexFor(PersistentObjectInterface anObject)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				SQLException
	{
		int lastOID = this.lastOIDIndexFor(anObject);
		return lastOID + 1;
	}

	private int oidIndexFrom(String anOID)
	{
		if(anOID == null)
		{
			return 0;
		}
		else
		{
			String ans = anOID.substring(this.indexStartPosition(), this.indexStopPosition() + 1);
			return Integer.valueOf(anOID.substring(this.indexStartPosition(), this.indexStopPosition() + 1)).intValue();
		}
	}

	private String oidPrefixFor(PersistentObjectInterface anObject)
		throws ClassNotFoundException,
			   NoSuchMethodException
	{
		return anObject.oidPrefix();
	}

	public String oidPrefixFrom(String anOID)
	{
		if(anOID == null)
		{
			return "";
		}
		else
		{
			return anOID.substring(this.prefixStartPosition(), this.prefixStopPosition() + 1);
		}
	}

	private PersistenceManager persistenceManager()
	{
		return this.persistenceManager;
	}
	
	private QueryManager queryManager()
	{
	    return this.persistenceManager().getQueryManager();
	}

	private int prefixStartPosition()
	{
		return this.persistenceManager().prefixStartPosition();
	}

	private int prefixStopPosition()
	{
		return this.persistenceManager().prefixStopPosition();
	}

	public void setOIDTo(PersistentObjectInterface anObject)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				SQLException
	{
		String indexString = this.indexStringOf(this.nextOIDIndexFor(anObject));
		anObject.oid(indexString + this.oidPrefixFor(anObject));
	}

}