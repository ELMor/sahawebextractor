package com.ssi.persistence.model;

import java.util.Vector;
import java.util.Enumeration;
import java.lang.reflect.*;
import java.sql.SQLException;
import com.ssi.persistence.cache.*;

import com.ssi.util.*;public abstract class PersistenceAccessor
{
	//Abstract superclass of the hierarchy of objects
	//which specify the mapping of objects to a database. Each
	//one of my subclasses will mapp a different class of
	//objects to its table.

	// Instance Vars
	private Vector fields;
	private PersistenceManager persistenceManager;
	private IPersistenceCache cache;



	//Class Methods

	public static PersistenceAccessor getInstance()
	{
	    return AccessorCache.getInstance().get("");
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get("");
	    /*if(answer == null)
	    {
			answer = new PersistenceAccessor(aManager);
			AccessorCache.getInstance().put(getName(), answer);
		}*/
		return answer;
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get("");
	    /*if(answer == null)
	    {
			answer = new PersistenceAccessor(aManager, aCache);
			AccessorCache.getInstance().put(getName(), answer);
		}*/
		return answer;
	}
	
	/**
	 * Create a new accessor, by default use a PersistenceCache
	 */
	protected PersistenceAccessor(PersistenceManager aManager)
	{
   		persistenceManager = aManager;
   		cache = (IPersistenceCache)new PersistenceCache();
	}

	protected PersistenceAccessor(PersistenceManager aManager, IPersistenceCache aCache)
	{
   		persistenceManager = aManager;
		cache = aCache;
	}

	//Instance Methods
	//Defined by subclasses

	public abstract Vector addPersistentFields( Vector aAnswer ) ;

	public abstract String oidPrefix();

	public PersistentObjectInterface buildInstanceFrom(DBResultSet aSet)
		throws	InstantiationException,
				IllegalAccessException,
				ClassNotFoundException,
				InvocationTargetException,
				NoSuchMethodException,
				SQLException
	{
		PersistentObjectInterface newInstance = this.getNewInstance();
		Enumeration enumerator = this.fieldsEnumerator();
		while (enumerator.hasMoreElements())
		{
			
			((PersistentField)enumerator.nextElement()).setValueTo(newInstance, aSet, this);
		}
		return newInstance;
	}
	
	public Vector buildInstancesFrom(DBResultSet aSet)
		throws InstantiationException,
				IllegalAccessException,
				ClassNotFoundException,
				InvocationTargetException,
				NoSuchMethodException,
				SQLException
	{

		
	
	  Vector answer = new Vector();

  	 try{
		  	 
	  if ( !aSet.getEOF() )
	  {
		aSet.moveFirst();

		while ( !aSet.getEOF())
		{
			String nextOID = "";
		    nextOID = this.nextKeyFromSet(aSet);
			if(this.cache().includesKey(nextOID) && this.cache().at(nextOID)!=null){
				answer.addElement(this.cache().at(nextOID));
			}
			else{
				PersistentObjectInterface newInstance = this.buildInstanceFrom(aSet);
				this.cache().atKeyPut(((PersistentObjectInterface)newInstance).oid(), newInstance);
				answer.addElement(newInstance);
			}
			aSet.moveNext();
		}
	  }

	 }
	 
	 catch(Exception e){
		 LogFile.log("PersistenceAccessor::buildinstancesfrom()");
		 LogFile.log(e);
	 }
	 
	 finally{
		  
	     aSet.close();
	 }    
	 
	 return answer;
	 
	}
	//
	// Collections Handling
	//

	public Vector buildInstancesFromCollection(PersistentCollection aCollection)
		throws InstantiationException,
				IllegalAccessException,
				ClassNotFoundException,
				InvocationTargetException,
				NoSuchMethodException,
				SQLException

	{
		String queryString = "SELECT * FROM "
							 + this.tableName()
							 + 	" WHERE "
							 + this.getFieldNameInCollection(aCollection)
							 + " = '"
							 + aCollection.owner().oid()
							 + "'";
		DBResultSet aSet = this.resultForCommand(queryString);
		return this.buildInstancesFrom(aSet);
	}

	public Vector buildProxiesFrom(DBResultSet aSet) throws SQLException
	{
	  Vector answer = new Vector();
	  if ( !aSet.getEOF())
	  {
		aSet.moveFirst();
		while ( !aSet.getEOF())
		{
			String nextOID = this.oidParser().nextOIDFromSet(aSet);
			if(this.cache().includesKey(nextOID))
			{
				answer.addElement(this.cache().at(nextOID));
			}
			else
			{
				ProxyInterface proxy = new PersistenceProxy(nextOID, this.persistenceManager().accessorForOID(nextOID));
				answer.addElement(proxy);
			}
			aSet.moveNext();
		}
	  }
	  aSet.close();
	  return answer;
	}

	private IPersistenceCache cache()
	{
		return cache;
	}

	public void cache(IPersistenceCache aCache)
	{
		cache = aCache;
	}

	public Vector collectionSpecs()
	{
	    /* Answer a vector of the form:
		Vector answer = new Vector();
		answer.addElement(new PersistentCollectionSpec("aFieldName","anAccessorName","anOwnerClassName","contentsClassNames","ownerAccessorName"));
		return answer;

		See class PersistentCollectionSpec for further comment on the parameteres.
	    */
		return new Vector();
	}


	public void delete(PersistentObjectInterface anInstance) throws SQLException
	{
		this.executeCommandFor(this.deleteCommandString(anInstance));
		this.cache().remove(anInstance);
	}

	private String deleteCommandString(PersistentObjectInterface anInstance)
	{
	    return this.dbEngineType().deleteCommandString(anInstance);
	}

	private void executeCommandFor(String aStringCommand) throws SQLException
	{
		DBResultSet aSet = this.resultForCommand(aStringCommand);
		aSet.close();
	}

	private Vector fields()
	{
		if(fields == null)
		{
			fields = this.persistentFields();
		}
		return fields;
	}

	public Enumeration fieldsEnumerator()
	{
		return this.fields().elements();
	}

	public String getAccessorNameInCollection(PersistentCollection aCollection)
		throws ClassNotFoundException,
			   NoSuchMethodException
	{
		PersistentCollectionSpec aSpec = this.getCollectionSpecFor(aCollection.owner().getClass());
		return aSpec.accessorName();
	}

	private Vector getAccessorsInCollection(PersistentCollection aCollection)
		throws ClassNotFoundException,
			   NoSuchMethodException
	{
	    //Answers all the PersistentAccessors of the objects contained in aCollection
		Vector answer = new Vector();
		Enumeration iterator = this.getContentClassesInCollection(aCollection).elements();
		while (iterator.hasMoreElements())
		{
			PersistenceAccessor anAccessor = this.persistenceManager().accessorForClass(((Class)iterator.nextElement()));
			answer.addElement(anAccessor);
		}
		return answer;
	}

	private Vector getCollectionsOf(PersistentObjectInterface anInstance)
		throws ClassNotFoundException,
			   NoSuchMethodException,
			   IllegalAccessException,
			   InvocationTargetException
	{
		Vector answer = new Vector();
		Enumeration iterator = this.collectionSpecs().elements();
		while (iterator.hasMoreElements())
		{
			PersistentCollectionSpec aSpec = (PersistentCollectionSpec)iterator.nextElement();
			answer.addElement(aSpec.yourCollectionIn(anInstance));
		}
		return answer;
	}

	private PersistentCollectionSpec getCollectionSpecFor(Class aClass)
		throws ClassNotFoundException,
			   NoSuchMethodException
	{
	    //Answers the Collection Spec for aClass

		PersistentCollectionSpec spec = null;
		Enumeration iterator = persistenceManager.accessorForClass(aClass).collectionSpecs().elements();
		while (iterator.hasMoreElements() & spec == null)
		{
			PersistentCollectionSpec element = ((PersistentCollectionSpec)iterator.nextElement());
			if (element.ownerClass() == aClass)
			{
				spec = element;
			}
		}
		return spec;
	}

	public Vector getContentClassesInCollection(PersistentCollection aCollection)
		throws ClassNotFoundException,
			   NoSuchMethodException
	{
		PersistentCollectionSpec aSpec = this.getCollectionSpecFor(aCollection.owner().getClass());
		return aSpec.contentClasses();
	}

	public String getFieldNameInCollection(PersistentCollection aCollection)
		throws ClassNotFoundException,
			   NoSuchMethodException
	{
		PersistentCollectionSpec aSpec = this.getCollectionSpecFor(aCollection.owner().getClass());
		return aSpec.fieldName();
	}

	private PersistentObjectInterface getNewInstance() throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{

		Class lClass = this.persistentClass() ;
		lClass.newInstance();
		return ((PersistentObjectInterface)this.persistentClass().newInstance());
	}

	private String insertCommandString(PersistentObjectInterface anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
	    return this.dbEngineType().insertCommandString(anInstance);
	}

	private void insertInstance(PersistentObjectInterface anInstance)
		throws IllegalAccessException,
			   InvocationTargetException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   InstantiationException,
			   SQLException
	{
		this.setOIDTo(anInstance);
		this.executeCommandFor(this.insertCommandString(anInstance));
		this.cache().atKeyPut(anInstance.oid(), anInstance);
		this.saveCollectionsOf(anInstance);
	}

	public PersistentObjectInterface instanceOfOID(String anOID)
		throws	InstantiationException,
				IllegalAccessException,
				ClassNotFoundException,
				InvocationTargetException,
				NoSuchMethodException,
				SQLException,
				Exception
	{
		PersistentObject target = ((PersistentObject)this.cache().at(anOID));
		if(target != null)
		{
			return target;
		}
		else
		{
			DBResultSet aSet = this.queryManager().executeCommandFor(this.instanceOfOIDStringCommand(anOID));
			PersistentObjectInterface lPersistentObjectInterface = null;
			if (!aSet.getEOF()) lPersistentObjectInterface = this.buildInstanceFrom(aSet);
			aSet.close();
			return lPersistentObjectInterface ;
		}
	}
	
	private String instanceOfOIDStringCommand(String anOID)
		throws Exception
	{
		return this.dbEngineType().instanceOfOIDStringCommand(anOID);
	}

	public Vector instances()
		throws InstantiationException,
				IllegalAccessException,
				ClassNotFoundException,
				InvocationTargetException,
				NoSuchMethodException,
				SQLException
	{
		DBResultSet aSet = this.resultForCommand(this.instancesCommandString());
		return this.buildInstancesFrom(aSet);
	}
	//
	// Command Strings
	//

	private String instancesCommandString()
	{
	    return this.dbEngineType().instancesCommandString();
	}

	public Vector instancesOfWhereSorted(String whereClause, String sortingClause)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
	    String sqlCommand = this.instancesCommandString() + " " + whereClause + " " + sortingClause;
		DBResultSet aSet = this.resultForCommand(sqlCommand);
		return this.buildInstancesFrom(aSet);
	}
	public String nextKeyFromSet(DBResultSet aSet)
		throws java.sql.SQLException
	{   
	    PersistentField campoPersistente = this.oidFieldSpec();
	    String result = aSet.getField(campoPersistente.columnName()).getString() ;
	    
		return result;
	}
	public String oidColumnName()
		throws SQLException
	{
		return this.oidFieldSpec().columnName();
	}

	public PersistentField oidFieldSpec()
		throws java.sql.SQLException
	{
		PersistentField answer = null;
		Enumeration iterator = this.fields().elements();
		while(iterator.hasMoreElements() & (answer == null))
		{
			PersistentField eachField = (PersistentField)iterator.nextElement();
			if (eachField.setterName().equals("oid"))
			{
				answer = eachField;
			}
		}
		return answer;
	}

	private OIDParser oidParser()
	{
		return this.persistenceManager().oidParser();
	}

	private PersistenceFactory persistenceFactory()
	{
		return this.persistenceManager().persistenceFactory();
	}
	public PersistenceManager persistenceManager()
	{
		return persistenceManager;
	}
	public void persistenceManager(PersistenceManager aManager)
	{
		persistenceManager = aManager;
	}
	public Class persistentClass() throws ClassNotFoundException
	{
		return Class.forName(this.persistentClassName());
	}
	public abstract String persistentClassName();
	public Vector persistentFields()
	{
		Vector answer = new Vector();
		return addPersistentFields( answer ) ;

	}
	protected Vector proxiesForCollection(PersistentCollection aCollection)
		throws ClassNotFoundException,
			   NoSuchMethodException,
			   SQLException
	{
		Vector answer = new Vector();
		Vector accessors = this.getAccessorsInCollection(aCollection);
		Enumeration iterator = accessors.elements();
		while (iterator.hasMoreElements())
		{
			String queryString = "SELECT OID FROM "
							 + ((PersistenceAccessor)iterator.nextElement()).tableName()
							 + 	" WHERE "
							 + this.getFieldNameInCollection(aCollection)
							 + " = '"
							 + aCollection.owner().oid()
							 + "'";
			DBResultSet aSet = this.resultForCommand(queryString);
			Vector proxies = this.buildProxiesFrom(aSet);
			Enumeration proxiesIterator = proxies.elements();
			while(proxiesIterator.hasMoreElements())
			{
				answer.addElement(proxiesIterator.nextElement());
			}
		}
		return answer;
	}
	
	
	private QueryManager queryManager()
	{
		return this.persistenceManager().getQueryManager();
	}
	
	
	private DBResultSet resultForCommand(String aStringCommand)
		throws SQLException
	{
		//AN 16/03/01
		LogFile.log("PersistenceAccessor::resultForCommand(String)");
		LogFile.log(aStringCommand);
		DBResultSet db = null;
				
		db = this.queryManager().executeCommandFor(aStringCommand);
		
		return db; 
	}
	
	
	public void save(PersistentObjectInterface anInstance)
		throws IllegalAccessException,
			   InvocationTargetException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   InstantiationException,
			   SQLException
	{
		if(anInstance.isPersistent())
		{
			this.updateInstance(anInstance);
		}
		else
		{
			this.insertInstance(anInstance);
		}
	}
	
	
	private void saveCollectionsOf(PersistentObjectInterface anInstance)
		throws IllegalAccessException,
			   InvocationTargetException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   InstantiationException,
			   SQLException
	{
		Enumeration iterator = this.getCollectionsOf(anInstance).elements();
		while (iterator.hasMoreElements())
		{
			((PersistentCollection)iterator.nextElement()).save();
		}
	}
	
	
	private void setOIDTo(PersistentObjectInterface anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				SQLException
	{
		this.oidParser().setOIDTo(anInstance);
	}
	
	
	public abstract String tableName();
	
	
	private DBEngineType dbEngineType()
	{
	    return this.persistenceManager().dbEngineTypeForAccessor(this);
	}
	
	
	public void truncateTable()
		throws IllegalAccessException,
			   InvocationTargetException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   InstantiationException,
			   SQLException
	{
		this.executeCommandFor(this.dbEngineType().truncateTableCommandString() );
	}
	
	private String updateCommandString(PersistentObjectInterface anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
	    return this.dbEngineType().updateCommandString(anInstance);
	}
	
	
	private void updateInstance(PersistentObjectInterface anInstance)
		throws IllegalAccessException,
			   InvocationTargetException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   InstantiationException,
			   SQLException
	{
		this.executeCommandFor(this.updateCommandString(anInstance));
		this.saveCollectionsOf(anInstance);
	}
}