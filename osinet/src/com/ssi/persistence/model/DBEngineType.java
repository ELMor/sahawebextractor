package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.util.*;
import java.sql.*;
import com.ssi.util.*;

public abstract class DBEngineType implements Cloneable
{
	public DBEngineType()
	{
	}
	
	public DBEngineType getClone()
	{
		DBEngineType answer = null;
		try {
			answer = (DBEngineType)this.clone();
		}
		catch (CloneNotSupportedException ex){
			LogFile.log("Error: Unable to clone DBEngineType" + ex);
		}
		finally {
			return answer;
		}
	}
	
	public DBEngineType(PersistenceAccessor anAccessor)
	{
		this.persistenceAccessor(anAccessor);
	}
	
	private PersistenceAccessor persistenceAccessor;
	
	public void persistenceAccessor(PersistenceAccessor anAccessor)
	{
		this.persistenceAccessor = anAccessor;
	}
	
	public PersistenceAccessor persistenceAccessor()
	{
		return this.persistenceAccessor;
	}

	private Enumeration fieldsEnumerator()
	{
		return this.persistenceAccessor().fieldsEnumerator();
	}

	private String tableName()
	{
	    return this.persistenceAccessor().tableName();
	}

	private PersistentField oidFieldSpec()
		throws java.sql.SQLException
	{
		return this.persistenceAccessor().oidFieldSpec();
	}

	private String oidColumnName()
	 throws SQLException
	{
		return this.persistenceAccessor().oidColumnName();
	}

	private String fieldsInsertString()
	{
		String answer = " (";
		Enumeration enumerator = this.fieldsEnumerator();
		while(enumerator.hasMoreElements())
		{
			answer = answer + ((PersistentField)enumerator.nextElement()).columnName();
			if(enumerator.hasMoreElements())
			{
				answer = answer + ",";
			}
		}
		return answer + ") ";
	}

	private String fieldsUpdateString(PersistentObjectInterface anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
		String answer = new String();
		Enumeration enumerator = this.fieldsEnumerator();
		while(enumerator.hasMoreElements())
		{
			PersistentField nextField = (PersistentField)enumerator.nextElement();
			answer = answer + nextField.columnName()
							+ " = "
							+ nextField.valueOf(anInstance);
			if(enumerator.hasMoreElements())
			{
				answer = answer + ", ";
			}
		}
		return answer;
	}

	private String fieldValuesInsertStringFor(PersistentObjectInterface anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
		String answer = " (";
		Enumeration enumerator = this.fieldsEnumerator();
		while(enumerator.hasMoreElements())
		{
			answer = answer + ((PersistentField)enumerator.nextElement()).valueOf(anInstance);
			if(enumerator.hasMoreElements())
			{
				answer = answer + ",";
			}
		}
		return answer + ") ";
	}

	public String insertCommandString(PersistentObjectInterface anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
		return "insert into " + this.tableName()
				+ this.fieldsInsertString() + " values "
				+ this.fieldValuesInsertStringFor(anInstance);
	}


	public String instanceOfOIDStringCommand(String anOID)
	 throws SQLException, Exception
	{
		return "select * from "
				+ this.tableName()
				+ " where "
				+ this.oidColumnName()
				+ " = "
				+ this.oidFieldSpec().oidForQuery(anOID);
	}

	public String instancesCommandString()
	{
		return "select * from " + this.tableName();
	}

	public String truncateTableCommandString()
	{
		return "delete * from " + this.tableName();
	}


	public String updateCommandString(PersistentObjectInterface anInstance)
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
		return "update " + this.tableName()
				+ " set "+ this.fieldsUpdateString(anInstance)
				+ " where "
				+ "oid = '" + anInstance.oid()
				+ "'";
	}


	public String deleteCommandString(PersistentObjectInterface anInstance)
	{
		return "delete * from " + this.tableName()
				+ " where oid = '" + anInstance.oid() + "'";
	}




}