package com.ssi.persistence.model;

/**
 * This type was created in VisualAge.
 */
public class DBCharacterFieldType extends DBFieldType {

	/**
	 * fieldClassName method comment.
	 */
	public String fieldClassName() {
		return "java.lang.Character";
	}

	/**
	 * setValueTo method comment.
	 */
	public void setValueTo(Object newInstance, java.lang.reflect.Method aSetter, DBField aField, PersistenceAccessor anAccessor) 
		throws java.lang.reflect.InvocationTargetException, 
				IllegalAccessException, 
				java.sql.SQLException 
	{
			aSetter.invoke(newInstance, new Character[] {aField.getCharacter()});
	}
}