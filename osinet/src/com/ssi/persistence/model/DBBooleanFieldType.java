package com.ssi.persistence.model;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;

public class DBBooleanFieldType extends DBFieldType
{
	public String fieldClassName()
	{
		return "java.lang.Boolean";
	}
public static boolean represents(Field aField)
{
	try
	{
		return aField.getType() == Class.forName("java.lang.Boolean");
	}
	catch (ClassNotFoundException ex)
	{
		return false;
	}
}
	public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
		throws IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		aSetter.invoke(newInstance, new Boolean[] {aField.getBoolean()});
	}
}