package com.ssi.http.salud;

import java.io.*;
import java.util.*;
import com.ssi.util.*;
import com.ssi.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.ssi.persistence.model.*;


/**
* Clase qu� representa una Conecci�n Http, incluyendo el Request, el Response y la Session
*
*/

public class ConeccionHTTPSalud extends ConeccionHTTP

{

/**
* M�todo Constructor qu� crea una instancia de la Clase
*/
	public ConeccionHTTPSalud(HttpServletRequest esteRequest, HttpServletResponse esteResponse)
		throws Exception
	{
		super(esteRequest, esteResponse);
	} //fin contructor ConeccionHTTP()

	public void setLoggedUser(User _usuario) {
		LogFile.log("SESS " + this.getSession());
		this.putInSession("loggedUser", _usuario);
	} // END OF setLoggedUser

	public User getLoggedUser() throws Exception {
		return (User)this.getFromSession("loggedUser");
	} // END OF getLoggedUser

	public boolean isUserLogged()
	{
		boolean exists = false;
		try{
			this.getLoggedUser();
			exists = true;
		}
		catch (Exception e){
		}
		return exists;
	} // END OF existsUserSession
} //fin class ConeccionHTTP