package com.ssi.log.model;

public class Log extends ApplicationLog implements LogInterface
{
	// Inst Vars

	StringBuffer loggingStream = new StringBuffer(new String());


	public void clearLog()
	{
		this.loggingStream(new StringBuffer(new String()));
		this.notifyObservers();
	}
	public void cr()
	{
		this.logString("\r");
	}
	public void crTab()
	{
		this.cr();
		this.tab();
	}
	public String loggedText()
	{
		return this.loggingStream().toString();
	}
	// Accessors
	private StringBuffer loggingStream()
	{
		return loggingStream;
	}
	private void loggingStream(StringBuffer aStream)
	{
		loggingStream = aStream;
	}
	// Logging
	public void logString(String aString)
	{
		this.loggingStream().append(aString);
		this.setChanged();
		this.notifyObservers(aString);
		System.out.println(aString);
	}
	public void logStringCR(String aString)
	{
		this.logString(aString);
//		this.cr();
	}
	public void setLog(String aString)
	{
		this.loggingStream(new StringBuffer(aString));
	}
	public void tab()
	{
		this.logString("\t");
	}
}