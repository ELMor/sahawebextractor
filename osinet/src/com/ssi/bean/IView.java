package com.ssi.bean;

import com.ssi.http.*;

public abstract interface IView
{
	public abstract String obtenerURLDestino(ConeccionHTTP unaConeccion);

	public abstract void resolverRequerimiento(ConeccionHTTP unaConeccion, ServletSSI myServlet);
}