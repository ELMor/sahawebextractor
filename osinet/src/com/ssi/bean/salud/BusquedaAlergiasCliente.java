package com.ssi.bean.salud;

import com.ssi.bean.View;

import com.ssi.persistence.model.*;
import com.ssi.util.LogFile;
import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;

import com.ssi.model.salud.*;import java.util.*;import com.ssi.model.*;/**
 * Insert the type's description here.
 * Creation date: (26/12/2000 03:15:09 p.m.)
 * @author: Administrator
 */
public class BusquedaAlergiasCliente extends View {
	String numeroPacienteReferenciado;

/**
 * Insert the method's description here.
 * Creation date: (28/12/2000 07:38:29 p.m.)
 * @return java.lang.String
 */
public java.lang.String getNumeroPacienteReferenciado() {
	return numeroPacienteReferenciado;
}

/**
 * obtenerURLDestino method comment.
 */
public String obtenerURLDestino(com.ssi.http.ConeccionHTTP unaConeccion) {
	return this.URL();
}
/**
 * Insert the method's description here.
 * Creation date: (28/12/2000 07:38:29 p.m.)
 * @param newNumeroPacienteReferenciado java.lang.String
 */
public void setNumeroPacienteReferenciado(java.lang.String newNumeroPacienteReferenciado) {
	numeroPacienteReferenciado = newNumeroPacienteReferenciado;
}
/**
 * URL method comment.
 */
public String URL() {
	return "/jsp/AlergiasCliente.jsp";
}

Vector pacientes;


public Vector ejecutarConsulta () {

	Vector vAlergiasSiapwin = new Vector();
 	Vector vAlergiasNovahis = new Vector();
	 	
		
	if(pacientes==null)
		LogFile.log("Debe setear el vector de pacientes seleccionados");
	else{	
		try{
 		for(int i = 0; i < pacientes.size(); i++){
	 		
		   if(pacientes.elementAt(i) instanceof PacienteNovahis)
		   {
			  LogFile.log("BusquedaAlergiasCliente: ejecutarConsulta: es Novahis");
	 		  String queryString = getQueryNovahis(((PacienteNovahis)pacientes.elementAt(i)).oid());  	
			  LogFile.log("Query de alergias: "+queryString);
	 		  
			  PersistenceManager manager = null;
			  JDBCAnswerResultSet rs = null;
			  
			  try {
			    manager = SistemaNovahis.getInstance().getPersistenceManager();
			    rs = (JDBCAnswerResultSet)manager.executeQuery(queryString);
			    
			    while(rs.isEOF().equals(Boolean.FALSE)){
				   vAlergiasNovahis.addElement(new Alergia(rs.getField(1).getString(), rs.getField(2).getDate()));
				   rs.moveNext();
			    }
			    
			  }
	          catch(Exception e) {
		        LogFile.log(e);
		      }

	          finally{
		        rs.close();
	          }    
		   }  
		   else{
   			  LogFile.log("BusquedaAlergiasCliente: ejecutarConsulta: es Siapwin");
	 		  String queryString = getQuerySiapwin(((PacienteSiapwin)pacientes.elementAt(i)).oid());  	
			  LogFile.log("Query de alergias: "+queryString);
	 		  try {
			    PersistenceManager manager = SistemaSiapwin.getInstance().getPersistenceManager();
			    JDBCAnswerResultSet rs = (JDBCAnswerResultSet)manager.executeQuery(queryString);
			    while(rs.isEOF().equals(Boolean.FALSE)){
			     vAlergiasSiapwin.addElement(new Alergia(rs.getField(1).getString(), rs.getField(2).getDate()));
				 rs.moveNext();
			    }
			    rs.close();
			  }
	          catch(Exception e) {
		      LogFile.log(e);
		      }
		   }  
	     }
	   }
	   catch(Exception e){
		   LogFile.log(e);
	   }	   

	 }  	
	  
	 return unificarAlergias(vAlergiasNovahis,vAlergiasSiapwin);
   
	}

	
	
/**
 * Insert the method's description here.
 * Creation date: (21/02/01 17:32:05)
 * @return java.util.Vector
 */
public java.util.Vector getPacientes() {
	return pacientes;
}public String getQueryNovahis(String idPacienteNovahis) {
	
	return "select ta.tipo_alrg_desc, cl.clialrg_fdesde " +
	       "from clien_alergias cl, tipo_alergia ta " +
	       "where ta.tipo_alrg_pk = cl.tipo_alrg_pk " +
		   "and cl.clialrg_activa_sn = 1 " +
		   "and codigo_cliente = " + idPacienteNovahis;
	
}	public String getQuerySiapwin(String idPacienteSiapwin) {

	return "select ta.tipo_alrg_desc, ha.fecha " +
	       "from tipo_alergia ta, halergia ha " +
		   "where ha.activo = 1 " +
		   "and ta.tipo_alrg_pk = ha.tipo_alrg_pk " +
		   "and ha.codigo_cliente = " + idPacienteSiapwin;
}/**
 * Insert the method's description here.
 * Creation date: (21/02/01 17:32:05)
 * @param newPacientes java.util.Vector
 */
public void setPacientes(java.util.Vector newPacientes) {
	pacientes = newPacientes;
}/**
 * Insert the method's description here.
 * Creation date: (21/02/01 18:19:41)
 */
public Vector unificarAlergias(Vector v1, Vector v2) {
	Enumeration e = v2.elements();
	while(e.hasMoreElements())
	v1.addElement(e.nextElement());
	
	return v1;
}}