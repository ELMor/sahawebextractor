package com.ssi.bean.salud;

/**
* Clase qu� se encarga de resolver las necesidades de la Pantalla de Encuentros (JSP).
*
* @see com.ssi.bean.#View
*/
import java.io.*;
import java.sql.*;
import java.util.*;
import com.ssi.bean.*;
import com.ssi.http.*; 
import com.ssi.util.*;
import com.ssi.http.*;
import javax.servlet.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import com.ssi.http.salud.*;
import com.ssi.model.salud.*;
import java.sql.SQLException;
import com.ssi.persistence.model.*;
import com.ssi.persistence.model.salud.*;

public class BusquedaEncuentros extends View {

	private Vector listaEncuentros;
	private Vector vEpisodios;

	public static View getView(ConeccionHTTP unaConeccion)
	{
		View newView = null;
		try {
			if (unaConeccion.existsObjectInSession("BusquedaEncuentros"))
			{
				newView = (View)unaConeccion.getObjectFromSession("BusquedaEncuentros");
			}
			else
			{
				newView = new BusquedaEncuentros();
				unaConeccion.putObjectInSession("BusquedaEncuentros", newView);
			}
		}catch (Exception exc) {
			LogFile.log("BusquedaEncuentros: getView: exc: "+exc);
		}
		return newView;
	} // END OF getView

	/**
	* M�todo qu� devuelve la URL del JSP qu� se debe mostrar seg�n lo qu� resolvi� el Bean
	* par aun requerimiento dado.
	*
	* @param unaConeccion  Instancia de la Clase ConeccionHTTP.
	*
	* @return answer       Cadena de Caracteres que simboliza la URL del JSP que se debe mostrar.
	*/
	public String obtenerURLDestino(ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaEncuentros: obtenerURLDestino: Inicio ");
		try {
			if(unaConeccion.existsParamValue("encuentroNumero") &&
				 !(unaConeccion.getSingleParamValue("encuentroNumero").equals("inicial")))
			{
			LogFile.log("BusquedaEncuentros: obtenerURLDestino: existe verActividades.");
				try{
					View busquedaActividades = BusquedaActividades.getView(unaConeccion);
					this.setearEncuentros((BusquedaActividades) busquedaActividades, unaConeccion);
					this.setearActividades((BusquedaActividades) busquedaActividades, unaConeccion);
					unaConeccion.putInSession("busquedaActividades", busquedaActividades);
					if(unaConeccion.existsParamValue("paginaEncuentro"))					
					   return BusquedaActividades.getURL();
					return "/jsp/actividadesEnc.jsp";
					
				} catch (Exception ex){
					LogFile.log("BusquedaEncuentros: obtenerURLDestino: Me pinche!"+ex);
				}
			} // ENDif
			if(unaConeccion.existsParamValue("view") &&
				 (unaConeccion.getSingleParamValue("view").equals("BusquedaPaciente"))){
					return BusquedaPaciente.getURL();
				 }
		} catch(Exception exc){
			LogFile.log("BusquedaEncuentros: obtenerURLDestino: Excepcion = "+exc);
		}
		LogFile.log("BusquedaEncuentros: obtenerURLDestino: fin & url "+this.getURL());
		LogFile.log("BusquedaEncuentros: obtenerURLDestino: Fin ");
		return this.getURL();
	} // END OF obtenerURLDestino

	/**
	* M�todo Est�tico qu� devuelve la URL del Bean.
	*/
	public static String getURL(){
		return "/jsp/encuentros.jsp";
	} // END OF getURL

	/**
	* M�todo qu� devuelve la URL del Bean.
	*/
	public String URL(){
		return "/jsp/encuentros.jsp";
	} // END OF getURL

	
/**
*
*/
	public void obtenerTodosLosEncuentros(ConeccionHTTP unaConeccion) {
	  LogFile.log("BusquedaEncuentros: obtenerTodosLosEncuentros: Inicio.");
	  this.obtenerEncuentros(unaConeccion);
	} // END OF obtenerEpisodios()

	/**
	* M�todo que setea un Vector que contiene los Encuentros para un Paciente determinado
	* de la Base de Datos del Sistema Siapwin y Novahis.
	*
	* @return Vector   Listado de los Encuentros de un Paciente.
	*/
	
	public void obtenerEncuentros(ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaEncuentros: obtenerEncuentros: Inicio.");
		Vector vEncuentros = new Vector();
		try {
		String sistema = unaConeccion.getSingleParamValue("sistema");
		LogFile.log("BusquedaEncuentors:obtenerEncuentros del sistema="+sistema); 
		
		
			vEncuentros = Encuentro.getEncuentros(this.getEpisodio(),sistema);
			this.setListaEncuentros(vEncuentros);
		}catch (Exception e){
			LogFile.log("BusquedaEpisodio: obtenerEpisodios: Exception: "+e);
		}
		LogFile.log("BusquedaEncuentros: obtenerEncuentros: Fin.");
	} // END OF obtenerEncuentros()

	/**
	* M�todo que devuelve un Vector
	*/
	public Vector getListaEncuentros(){
		return listaEncuentros;
	}

  /**
  * 
  */
	public void setListaEncuentros(Vector unaLista){
		listaEncuentros = unaLista;
	}

/**
* M�todo que permite setear el numero del Episodio qu� fu� seleccionado en la Pantalla de 
* episodios. Para ello utiliza un Vector que contiene los valores qu� han sido seleccionados
* en la Pantalla de Episodios.
*
* @param unaLista   Vector qu� contiene el valor del identificador que ha sido seleccionado
*                   en la pantalla de Episodios.
*/
	public void setearEpisodios(Vector unaLista){
		this.vEpisodios = unaLista;
	}

/**
* M�todo que devuelve el numero del Episodio qu� fu� seleccionado en la Pantalla de 
* episodios.
*
*/
	public String getEpisodio(){
		return (String) this.vEpisodios.elementAt(0);
	}

	public void obtenerInformacionPantalla(ConeccionHTTP unaConeccion){
		LogFile.log("BusquedaEncuentros: obtenerInformacionPantalla: Inicio");
		try {
		}
		catch (Exception ex) {
			LogFile.log("BusquedaEncuentros: obtenerInformacionPantalla: " + ex);
		}
		LogFile.log("BusquedaEncuentros: obtenerInformacionPantalla: Fin");
	} // END OF obtenerInformacionUsuario()

/**
* M�todo qu� permite setear el Encuentro seleccionado por el usuario para luego asociar
* las actividades a dicho Encuentro.
*
* @param busquedaActividades  Instancia del Bean BusquedaActividades
* @param unaConeccion         Conecci�n a la Base de Datos del Sistema.
*/
	private void setearEncuentros(BusquedaActividades busquedaActividades, ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaEncuentros: setearEncuentros: Inicio");
		Vector vEncuentros = new Vector();
		try {
			vEncuentros.addElement(unaConeccion.getSingleParamValue("encuentroNumero"));
		}catch (Exception exc){
			LogFile.log("BusquedaEncuentros: setearEncuentros: Excepcion.");
		}
		busquedaActividades.setearEncuentros(vEncuentros);
	} // END OF setearActividades

/**
* M�todo qu� permite setear al Bean BusquedaActividades las Actividades qu� debe mostrar
* en pantalla.
* 
* @param busquedaActividades  Instancia del Bean BusquedaActividades qu� ser� utilizado para
*                             mostrar los datos desde el Navegador.
* @param unaConeccion         Conecci�n a la Base de Datos del Sistema.
*/

 private void setearActividades(BusquedaActividades busquedaActividades, ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaEncuentros: setearActividades: Inicio 1");
		busquedaActividades.obtenerActividades(unaConeccion);
	} // END OF setearActividades
/**
 * Insert the method's description here.
 * Creation date: (19/02/01 13:30:00)
 */
public void obtenerTodosLosEncuentros(Vector pacientes, ConeccionHTTP unaConeccion) {
	  LogFile.log("BusquedaEncuentros: obtenerTodosLosEncuentros: Inicio.");

		Vector vEncuentros = new Vector();
		try {
			
			Vector vEpisodios = null;    
	            try{
		         vEpisodios = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getEpisodios();
				 if(vEpisodios==null){
	   				//si no esta en la sesion lo lee de la tabla
	                 LogFile.log("BusquedaEncuentros recuperando todos los episodios");
				     View busquedaEpisodios = BusquedaEpisodios.getView(unaConeccion);
				     ((BusquedaEpisodios)busquedaEpisodios).setPacientes(pacientes);
				     ((BusquedaEpisodios)busquedaEpisodios).obtenerTodosLosEpisodios(unaConeccion);
			         unaConeccion.putInSession("busquedaEpisodios", busquedaEpisodios);
			         vEpisodios = ((BusquedaEpisodios)busquedaEpisodios).getEpisodios();
			     } 

		         
	            }
				catch(Exception e){ LogFile.log(e);}
	   
			vEncuentros = Encuentro.getEncuentros(pacientes,vEpisodios);
			this.setListaEncuentros(vEncuentros);
		}catch (Exception e){
			LogFile.log("BusquedaEncuentros: obtenerTodosLosEncuentros: Exception: "+e);
		}
		LogFile.log("BusquedaEncuentros: obtenerTodosLosEncuentros: Fin."); 
	  
	}} // END OF Class BusquedaEncuentros