package com.ssi.bean.salud;

import com.ssi.bean.View;
import java.util.Vector;
/**
 * Insert the type's description here.
 * Creation date: (26/12/2000 03:15:09 p.m.)
 * @author: Administrator
 */
public class BusquedaDatosAdministrativosCliente extends View {
	String numeroPacienteReferenciado;
	Vector resultado;
/**
 * Insert the method's description here.
 * Creation date: (27/12/2000 11:43:23 a.m.)
 * @return java.lang.String
 */
public java.lang.String getNumeroPacienteReferenciado() {
	return numeroPacienteReferenciado;
}
/**
 * obtenerURLDestino method comment.
 */
public String obtenerURLDestino(com.ssi.http.ConeccionHTTP unaConeccion) {
	return this.URL();
}
/**
 * Insert the method's description here.
 * Creation date: (27/12/2000 11:43:23 a.m.)
 * @param newNumeroPacienteReferenciado java.lang.String
 */
public void setNumeroPacienteReferenciado(java.lang.String newNumeroPacienteReferenciado) {
	numeroPacienteReferenciado = newNumeroPacienteReferenciado;
}
/**
 * URL method comment.
 */
public String URL() {
	return "/jsp/DatosAdministrativos.jsp";
}
/**
 * Ejecuta la consulta en la base de datos.
 * 
 */
public void ejecutarConsulta () {
	// Codificar
}/**
 * Insert the method's description here.
 * Creation date: (27/12/2000 12:13:13 p.m.)
 * @return java.util.Vector
 */ 
public java.util.Vector getResultado() {
	return resultado;
}/**
 * Insert the method's description here.
 * Creation date: (27/12/2000 12:13:13 p.m.)
 * @param newResultado java.util.Vector
 */ 
public void setResultado(java.util.Vector newResultado) {
	resultado = newResultado;
}}