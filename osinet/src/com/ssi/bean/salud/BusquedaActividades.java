package com.ssi.bean.salud;

/**
* Clase qu� se encarga de resolver las necesidades de la Pantalla de Encuentros (JSP).
*
* @see com.ssi.bean.#View
*/
import java.io.*;
import java.sql.*;
import java.util.*;
import com.ssi.bean.*;
import com.ssi.http.*;
import com.ssi.util.*;
import com.ssi.http.*;
import javax.servlet.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import com.ssi.http.salud.*;
import com.ssi.model.salud.*;
import java.sql.SQLException;
import com.ssi.persistence.model.*;
import com.ssi.persistence.model.salud.*;

public class BusquedaActividades extends View {

	private Vector listaActividades;
	private Vector vEncuentros;

/**
*
*/
	public static View getView(ConeccionHTTP unaConeccion)
	{
		View newView = null;
		try {
			if (unaConeccion.existsObjectInSession("BusquedaActividades"))
			{
				newView = (View)unaConeccion.getObjectFromSession("BusquedaActividades");
			}
			else
			{
				newView = new BusquedaActividades();
				unaConeccion.putObjectInSession("BusquedaActividades", newView);
			}
		}catch (Exception exc) {
			LogFile.log("BusquedaActividades: getView: exc: "+exc);
		}
		return newView;
	} // END OF getView

	/**
	* M�todo qu� devuelve la URL del JSP qu� se debe mostrar seg�n lo qu� resolvi� el Bean
	* par aun requerimiento dado.
	*
	* @param unaConeccion  Instancia de la Clase ConeccionHTTP.
	*
	* @return answer       Cadena de Caracteres que simboliza la URL del JSP que se debe mostrar.
	*/
	public String obtenerURLDestino(ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaActividades: obtenerURLDestino: Inicio ");
		try {
			if(unaConeccion.existsParamValue("view") &&
				 (unaConeccion.getSingleParamValue("view").equals("buscarPacientes")))
			{
			LogFile.log("BusquedaActividades: obtenerURLDestino: existe buscarPacientes.");
			try{
				LogFile.log("BusquedaActividades: obtenerURLDestino: llama a obtenerActividades.");
				return BusquedaPaciente.getURL();
			} catch (Exception ex){
				LogFile.log("BusquedaActividades: obtenerURLDestino: Me pinche!"+ex);
			}
			} // ENDif
		} catch(Exception exc){
			LogFile.log("BusquedaActividades: obtenerURLDestino: Excepcion = "+exc);
		}
		LogFile.log("BusquedaActividades: obtenerURLDestino: Fin ");
		return this.getURL();
	} // END OF obtenerURLDestino

	/**
	* M�todo Est�tico qu� devuelve la URL del Bean.
	*/
	public static String getURL(){
		return "/jsp/actividades.jsp";
	} // END OF getURL

	/**
	* M�todo qu� devuelve la URL del Bean.
	*/
	public String URL(){
		return "/jsp/actividades.jsp";
	} // END OF getURL

	/**
	* M�todo qu� devuelve el identificador del Encuentro qu� se asocia con las Actividades
	* que muestra el Bean.
	*
	* @return 
	*/
	public String getEncuentro(){
		return (String) this.vEncuentros.elementAt(0);
	} // END OF getURL


/**
* M�todo que devuelve las Actividades de un Encuentro dado y que han sido seteadas
* desde el Bean de Busqueda de Encuentros.
* 
* @return listaActividades     Vector qu� permite 
*/
	public Vector getListaActividades(){
		return listaActividades;
	}

/**
* M�todo que permite setear las Actividades de un Encuentro dado y que luego ser�n
* mostradas en pantalla.
* 
* @param listaActividades     Vector qu� permite 
*/
	public void setListaActividades(Vector unaLista){
		listaActividades = unaLista;
	}

/**
* M�todo que permite setear el Encuentro seleccionado por el Usuario; el cu�l se asociar�
* con las Actividadse mostradas en pantalla.
* 
* @param encuentros   Vector qu� contiene el Encuentro asociado a las Actividades que muestra
*                     el Bean de Busqueda de Actividades.
*/
	public void setearEncuentros(Vector encuentros){
		vEncuentros = encuentros;
	}

/**
 * Insert the method's description here.
 * Creation date: (19/02/01 16:29:45)
 */
public void obtenerTodasLasActividades(ConeccionHTTP unaConeccion) {
	  LogFile.log("BusquedaActividades: obtenerTodasLasActividades: Inicio.");
	  this.listaActividades = this.obtenerActividades(unaConeccion);
	} // END OF obtenerTodasLasActividades()

/**
 * Insert the method's description here.
 * Creation date: (20/02/01 10:49:12)
 */
public Vector obtenerActividades()
	{
		Vector vActividades = new Vector();
		try
		{
			String strSistema = ((EncuentroSalud)this.vEncuentros.elementAt(0)).sistema().nombreSistema();
			
			LogFile.log("BusquedaActividades: obtenerActividades() para el encuentro: "+getEncuentro()+" y el sistema: "+ strSistema);
			
			vActividades = Actividad.getActividades(getEncuentro(),strSistema);
			// anterior: vActividades = Actividad.getActividades(getEncuentro());
			setListaActividades(vActividades);
		}
		catch(Exception ex)
		{
			LogFile.log("BusquedaActividades: obtenerActividades: Excepcion");
		}
		return vActividades;
	}
	public void obtenerInformacionPantalla(ConeccionHTTP unaConeccion){
		
	}
/**
* 
*/


	public Vector obtenerActividades(ConeccionHTTP unaConeccion) {

		Vector vActividades = new Vector();
		try {
			 if(!unaConeccion.existsParamValue("sistema"))
				 LogFile.log("BusquedaActividades:obtenerActividades No encuentra el parametro sistema");
			 String sistema = unaConeccion.getSingleParamValue("sistema");
		    LogFile.log("BusquedaActividades:obtenerActividades del sistema="+sistema); 
			vActividades = Actividad.getActividades(this.getEncuentro(),sistema);
			this.setListaActividades(vActividades);
		}catch (Exception exc){
			LogFile.log("BusquedaActividades: obtenerActividades: Excepcion");
		}
		return vActividades;
	} // END OF 

/**
 * Insert the method's description here.
 * Creation date: (20/02/01 10:44:20)
 * @return java.util.Vector
 */

 
 public Vector obtenerActividades(Vector pacientes, ConeccionHTTP unaConeccion) {

	  Vector vEpisodios = null;    
	  try{
		vEpisodios = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getEpisodios();
	  }
	  catch(Exception e){ LogFile.log(e);
	  }
   
	  View busquedaEncuentros = BusquedaEncuentros.getView(unaConeccion);
	  ((BusquedaEncuentros)busquedaEncuentros).setearEpisodios(vEpisodios);
	  //Vector pacientes = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getPacientes();
	  ((BusquedaEncuentros)busquedaEncuentros).obtenerTodosLosEncuentros(pacientes, unaConeccion);
		  
	  Vector vectorEncuentros = ((BusquedaEncuentros)busquedaEncuentros).getListaEncuentros();
	 
	  Enumeration e = vectorEncuentros.elements();
	  EncuentroSalud encuentro = null;
	  String sistema = null;
	  String idEncuentro = null;
	  StringBuffer restriccionNovahis = new StringBuffer(" WHERE ");
	  StringBuffer restriccionSiapwin = new StringBuffer(" WHERE ");
	  int flagNovahis = 0;
	  int flagSiapwin = 0;
	  
	  while(e.hasMoreElements()){
	   encuentro = (EncuentroSalud)e.nextElement();
	   idEncuentro = encuentro.idEncuentro();
	   sistema = encuentro.sistema().nombreSistema();
	   if(sistema.equals("SistemaNovahis")){
	       if(flagNovahis==0)
	        restriccionNovahis.append("ve_en_ref = '" +idEncuentro+ "'");
	       else
	        restriccionNovahis.append(" OR ve_en_ref = '" +idEncuentro+ "'");
	       flagNovahis++; 
	   }	 
	   if(sistema.equals("SistemaSiapwin")){
	       if(flagSiapwin==0)
	        restriccionSiapwin.append("ve_en_ref = '" +idEncuentro+ "'");
	       else
	        restriccionSiapwin.append(" OR ve_en_ref = '" +idEncuentro+ "'");
	       flagSiapwin++; 
	   }	 
	  }  
	 	 
	   Vector vActividades = null;
		try {
			LogFile.log("Llamando a Actividad.getActividades::novahis="+restriccionNovahis.toString()+" siapwin:"+restriccionSiapwin.toString());
			vActividades = Actividad.getActividades(pacientes,restriccionNovahis.toString(),restriccionSiapwin.toString());
			 this.setListaActividades(vActividades);
		}catch (Exception exc){
			LogFile.log("BusquedaActividades: obtenerActividades: Excepcion");
			LogFile.log(exc);
		}
		return vActividades;
	} // END OF 


 
/**
 * Insert the method's description here.
 * Creation date: (20/02/01 10:42:41)
 */

 public void obtenerTodasLasActividades(Vector pacientes, ConeccionHTTP unaConeccion) {
	  LogFile.log("BusquedaActividades: obtenerTodasLasActividades: Inicio.");
	  this.listaActividades = this.obtenerActividades(pacientes, unaConeccion);
	} // END OF obtenerTodasLasActividades()
} // END OF Class BusquedaActividades