package com.ssi.util;
import com.ssi.util. * ;
import java.util. * ;
import java.io. * ;

public class Parametros {
    public static boolean LOG_TO_SCREEN;
    public static boolean LOG_TO_FILE;
    public static String LOG_FILENAME;
    public static String sinDatos = "s/d";
    public static String imgpath = "/Salud/jsp/pics/";
    public static String jspath = "/Salud/jsp/js/";

    //nombre de archivo .ini
    public static final String INI_FILENAME = "salud_parametros.properties";

    public static void inicializarVariablesStaticas() {
        FileOutputStream inicVarEstatFos = null;
        PrintWriter inicVarEstatPs = null;
        try {
            inicVarEstatFos = new FileOutputStream("SALUD_LeerArchivoIni.log", false);
            inicVarEstatPs = new PrintWriter(inicVarEstatFos, true);
            Properties defaultSettings = new Properties();
            inicVarEstatPs.println("Antes de Parametros.readIniFile()");
            Properties settings = Parametros.readIniFile(defaultSettings, inicVarEstatPs);
            inicVarEstatPs.println("Despues de Parametros.readIniFile(). Antes de hacer Parametros.setVariables()");
            Parametros.setVariables(settings, inicVarEstatPs);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            settings.list(pw);
        } catch (Exception e) {
            e.printStackTrace();
            LogFile.log(e);
        } finally {
            try {
                inicVarEstatFos.flush();
                inicVarEstatFos.close();
                inicVarEstatPs.flush();
                inicVarEstatPs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    } //fin inicializarVariablesStaticas()

    public static Properties setDefaults() {
        Properties defaultSettings = new Properties();
        defaultSettings.put("LOG_TO_SCREEN","true");
        defaultSettings.put("LOG_TO_FILE","true");
        defaultSettings.put("LOG_FILENAME","SALUD.log");
        return defaultSettings;
    } //fin setDefaults()

    public static Properties readIniFile(Properties defaultSettings, PrintWriter inicVarEstatPs) {
        Properties settings = new Properties(defaultSettings);
        try {

            // read settings from file -- START
            inicVarEstatPs.println("Antes de abrir archivo ini");
            FileInputStream fis = new FileInputStream(Parametros.INI_FILENAME);
            inicVarEstatPs.println("Despues de abrir archivo ini");
            settings.load(fis);
            inicVarEstatPs.println("Despues de leer archivo");
        } catch (Exception e) {
            LogFile.log(e);
        } finally {
            return settings;
        }
    } //end of readIniFile()

    public static void setVariables(Properties settings, PrintWriter inicVarEstatPs) {
        LogFile.log("Entrando de Parametros.setVariables()");
        inicVarEstatPs.println("Parametros.setVariables() 11");
        LOG_TO_SCREEN = Boolean.valueOf(settings.getProperty("LOG_TO_SCREEN")).booleanValue();
        LOG_TO_FILE = Boolean.valueOf(settings.getProperty("LOG_TO_FILE")).booleanValue();
        LOG_FILENAME = settings.getProperty("LOG_FILENAME");
        LogFile.log("saliendo de Parametros.setVariables()");
        inicVarEstatPs.println("saliendo de Parametros.setVariables()");
    } //fin setVariables()

    public static String time(String horario) {
        if (horario == null) {
            return Parametros.sinDatos;
        }
        return horario.substring(0,16);
    }
} // end of public static class Parametros
