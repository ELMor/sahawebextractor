package com.ssi.util;

import java.io.*;
import java.util.*;
import java.sql.SQLException;


/**
 Objeto que permite crear un archivo para seguimiento de las transacciones que va haciendo el sistema. El mismo define una serie
 de m&eacute;todos en los cuales permite que facilmente se maneje dicho archivo.
 <BR>
 El archivo creado contiene los mensajes que el programador haya ingresado para la realizaci&oacute;n de Debug, en caso de algun problema
 que se efectue en el sistema.

 */
public class LogFile {
static PrintStream ps = null    ;

   /**
	 Crea una instancia del objeto LogFile con un nombre de archivo determinado. El nombre que se coloque al archivo debe ser
	  de la manera Completa (Nombre y Extension).
	  @param filename 	Nombre Completo que se le desea poner al archivo.
   */
	public LogFile(String filename) {
		if (true == Parametros.LOG_TO_FILE) {
			openLogFile(filename);
		}
	}

	/**
	  M&eacute;todo que permite cerrar el archivo de manera &oacute;ptima, agreg&aacute;ndole mensajes de cierre al mismo.
	 */
	public static void close() {
		if (true == Parametros.LOG_TO_FILE) {
			if (null!= ps) {
				ps.println ("------- C L O S I N G    L O G    F I L E -------");
				ps.println ("current date is:" + (new Date().toString()));
				ps.println (" ");
				ps.flush();
				ps.close();
				ps = null;
			}//end of IF
		} // end of IF
	} // end of close()


	/**
	 Metodo que crea e inicializa el archivo con un nombre determiado. El nombre que se coloque al archivo debe ser
	 de la manera Completa (Nombre y Extension).
	 @param filename 	Nombre Completo que se le desea poner al archivo.
	 */
	public static void openLogFile(String filename) {
		openLogFile(filename, false);
	}

	/**
	 M&eacute;todo que crea e inicializa el archivo con un nombre determiado y si el mismo reemplaza a uno existente o agrega el comienzo de las
	 nuevas transacciones al final del existente. El nombre que se coloque al archivo debe ser de la manera Completa (Nombre y Extensi&oacute;n).
	 @param filename 	Nombre Completo que se le desea poner al archivo.
	 @param append    Parametro que si es 'True' agrega los mensajes al final del archivo existente, o lo reemplaza en caso contrario.
	 */
	public static void openLogFile(String filename, boolean append) {
		if (true == Parametros.LOG_TO_FILE) {
			if (null == ps) {
				try {
					FileOutputStream fos = new FileOutputStream(filename, append);
					ps = new PrintStream(fos);
					ps.println ("------- S T A R T I N G    L O G    F I L E -------");
					ps.println ("current date is:" + (new Date().toString()));
					ps.println (" ");
				} catch (IOException ioe) {
						System.out.println (" !!! ERROR - LogFile.create()");
						System.out.println (" " + ioe);
				}  // end of CATCH
			}  // end of IF
		}//end IF
	}


	/**
	 M&eacute;todo que permite agregar mensajes al archivo creado para log.
	 @param 	message Mensaje a ser ingresado.
	 */
	public static void log(String message) {
		//log (message, 0);
		doLogging(message, "");
	}  // end of log()


	/**
	 M&eacute;todo que permite agregar mensajes al archivo creado para log y de donde proviene el mismo.
	 @param 	message 		Mensaje a ser ingresado.
	 @param  moduleName  Nombre de donde proviene el mensajes.
	 */
	private static void doLogging(String message, String moduleName) {
		GregorianCalendar gc = new GregorianCalendar();
		String timeString = gc.get(Calendar.HOUR_OF_DAY)+":"+
							gc.get(Calendar.MINUTE)     +":"+
							gc.get(Calendar.SECOND)         ;
		if (true == Parametros.LOG_TO_FILE) {
			if (null != ps) {
				ps.println(moduleName +" "+ timeString+"| "+message);
			}  // end of IF
		} // end of IF
		if (true == Parametros.LOG_TO_SCREEN) {
			System.out.println (message);
		}  // end of IF
	}  // end of doLogging


	/**
	 M&eacute;todo que permite agregar mensajes de excepci&oacute;n
	 @param 	e 		Objeto Exception a ser ingresado en el archivo.
	 @see   java.lang.Exception
	 */
	public static void log(Exception e) {
		doLogging(e);
	}  // end of log()


	/**
	 M&eacute;todo que permite agregar mensajes de excepci&oacute;n
	 @param 	e 		Objeto Exception a ser ingresado en el archivo.
	 @see   java.lang.Exception
	 */
	private static void doLogging(Exception e) {
		GregorianCalendar gc = new GregorianCalendar();
		String timeString= gc.get(Calendar.HOUR_OF_DAY) +":" +gc.get(Calendar.MINUTE)
			+":" + gc.get(Calendar.SECOND);
		if (true == Parametros.LOG_TO_FILE) {
			if (null != ps) {
				ps.println(timeString+"| La siguiente Exception:");
				ps.println(getExceptionDetailString(e));
			}  // end of IF
		} // end of IF
		if (true == Parametros.LOG_TO_SCREEN) {
			//System.out.println (message);
			e.printStackTrace();
		}  // end of IF
	}  // end of doLogging


	/**
	 M&eacute;todo que permite agregar mensajes de excepci&oacute;n de SQL.
	 @param 	sqlex	Objeto SQLException a ser ingresado en el archivo.
	 @see   java.sql.SQLException
	 */
	private static String getMessageForSQLException(SQLException sqlex) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		SQLException nextException = new SQLException();
		while (nextException!=null) {
			pw.println(sqlex.toString());
			pw.println("Error Code: "+sqlex.getErrorCode());
			pw.println("SQL State: "+sqlex.getSQLState());
			nextException = sqlex.getNextException();
		}
		pw.println("Stack Trace: ");
		sqlex.printStackTrace(pw);
		return sw.toString();
	}//fin armarStringMensajeParaSQLException()


	/**
	 M&eacute;todo que dada un Objeto Exception, devuelve el mensajes del problema del mismo.
	 @param 	sqlex	Objeto SQLException a ser ingresado en el archivo.
	 */
	private static String getExceptionDetailString(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		SQLException sqlex = new SQLException();
		String mensaje = null;
		if (sqlex.getClass().isAssignableFrom(e.getClass())) {
			mensaje = getMessageForSQLException((SQLException)e);
		}
		else {
			e.printStackTrace(pw);
			mensaje = sw.toString();
		}
		return mensaje;
	}//fin getExceptionDetailString()


}  // end of class LogFile