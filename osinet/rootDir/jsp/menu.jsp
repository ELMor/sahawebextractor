<%@ page import="java.util.*, com.ssi.model.salud.*, com.ssi.bean.salud.*" %> 
<jsp:useBean id="busquedaEpisodios" scope="session" class="com.ssi.bean.salud.BusquedaEpisodios" />
<jsp:useBean id="busquedaPaciente" scope="session" class="com.ssi.bean.salud.BusquedaPaciente" />
<%@ page import="com.ssi.util.*" %>
<%   
   BusquedaMenu busquedaMenu = new BusquedaMenu();
   request.getSession().putValue("busquedaMenu",busquedaMenu);  
   request.getSession().putValue("BusquedaMenu",busquedaMenu);  
   BusquedaActividades busquedaActividades = new BusquedaActividades();
   request.getSession().putValue("busquedaActividades",busquedaActividades);  
   
					
%>
    
<html>
<head>

  <title>Menu</title>
  <SCRIPT LANGUAGE="JavaScript" SRC="/Salud/jsp/js/menu.js"></SCRIPT>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

  
  
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body bgcolor="#f0f3e9" leftmargin="0" marginwidth="0" marginheight="0" topmargin="0" onLoad="MM_preloadImages('<%=Parametros.imgpath%>bt_contactenos1.jpg','<%=Parametros.imgpath%>bt_ayuda1.jpg','<%=Parametros.imgpath%>bt_salir1.jpg','<%=Parametros.imgpath%>bt_volver1.jpg')">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr> 
    <td height="1" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="54%" background="<%=Parametros.imgpath%>fondotitu.jpg"><img src="<%=Parametros.imgpath%>tituBrowsertop.jpg" width="340" height="62"></td>
          <td width="46%" background="<%=Parametros.imgpath%>fondotitu.jpg"> 
            <div align="right"><img src="<%=Parametros.imgpath%>fondotop.jpg" width="417" height="62"></div>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="88%" background="<%=Parametros.imgpath%>fondobarratop.jpg">&nbsp;</td>
          <td width="12%" background="<%=Parametros.imgpath%>fondobarratop.jpg"> 
            <div align="left"><a href="mailto:info@mail.com" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','<%=Parametros.imgpath%>bt_contactenos1.jpg',1)"><img name="Image42" border="0" src="<%=Parametros.imgpath%>bt_contactenos.jpg" width="90" height="28"></a><a href="/Salud/jsp/manualuso.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image52','','<%=Parametros.imgpath%>bt_ayuda1.jpg',1)"><img name="Image52" border="0" src="<%=Parametros.imgpath%>bt_ayuda.jpg" width="48" height="28"></a><a href="javascript:submitVolver()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image41','','<%=Parametros.imgpath%>bt_salir1.jpg',1)"><img name="Image41" border="0" src="<%=Parametros.imgpath%>bt_salir.jpg" width="42" height="28" alt="Salir de la Aplicaci&oacute;n"></a><img src="<%=Parametros.imgpath%>fondobarratop.jpg" width="9" height="28"></div>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td background="<%=Parametros.imgpath%>sombra_top.jpg"><img src="<%=Parametros.imgpath%>sombra_top.jpg"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td> 
      <script language="JavaScript">
<!--

function submitEpisodio(param){
	form = window.document.busquedaEpisodios;
	form.episodioNumero.value = param;
	form.submit();
}

function submitBuscar(){
	form = window.document.busquedaEpisodios;
	form.episodioNumero.value = 'inicial';
	form.view.value = 'busquedaPaciente' ;
	form.submit();
}

// -->
</script>
      <% request.getSession().putValue("BusquedaMenu",busquedaMenu);  
  %>
      <form name="menu"  method="post">
        <table width="70%" align="center" cellpadding="0" cellspacing="0" border="1" bgcolor="#D9D1C6">
          <tr> 
            <td> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" height="0">
                <tr bgcolor="#FFFDE4"> 
                  <td width="3%" valign="top" align="left" height="222"> 
                    <div align="center"> 
                      <input type='hidden' name='view' value='BusquedaMenu'>
                      <br>
                      <img src="<%=Parametros.imgpath%>modos.jpg" width="415" height="251" usemap="#Map" border="0" > 
<map name="Map"> 
 <area shape="rect" coords="66,52,234,95" href="javascript:submitMenuEpisodios()">
 <area shape="rect" coords="156,118,299,156" href="javascript:submitMenuEncuentros()">
 <area shape="rect" coords="200,170,314,203" href="javascript:submitMenuActividades()">
</map>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <div align="center">
          <input type='hidden' name='queBuscar'>
          <a href="javascript:history.go(-1)" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','<%=Parametros.imgpath%>bt_volver1.jpg',1)"><img name="Image11" border="0" src="<%=Parametros.imgpath%>bt_volver.jpg" alt="Volver a la pantalla de Pacientes Encontrados"></a> 
        </div>
      </form>
    </td>
  </tr>
  <tr> 
    <td background="<%=Parametros.imgpath%>fondo_pie.jpg" height="1" nowrap valign="top"><img src="<%=Parametros.imgpath%>pie.jpg"></td>
  </tr>

</table>



</body>

</html>