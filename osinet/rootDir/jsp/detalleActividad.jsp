<%@ page import="java.util.*,com.ssi.persistence.model.PersistenceManager,java.sql.*" %>
<%@ page import="java.util.Vector,java.lang.reflect.*,com.ssi.bean.salud.*" %>
<%@ page import="com.ssi.persistence.model.*,com.ssi.persistence.cache.*" %>
<%@ page import="com.ssi.persistence.model.JDBC.JDBCAnswerResultSet" %>
<%@ page import="java.sql.*, com.ssi.model.salud.*,com.ssi.util.*" %>





<jsp:useBean id="busquedaActividades" scope="session" class="com.ssi.bean.salud.BusquedaActividades" />

<%
	String strNumero = request.getParameter("numero");
	String sistema = request.getParameter("sistema");	
	Vector vectorActividad = busquedaActividades.getListaActividades();
	int limite = vectorActividad.size();
	boolean encontrado=false;
	int i=0;
	ActividadSalud unaActividad = null;
	while(i<limite && !encontrado){
 	   unaActividad = (ActividadSalud) vectorActividad.elementAt(i);
 	   if(unaActividad.ve_ac_ref().equals(strNumero) && sistema.equals(unaActividad.sistema().nombreSistema()))
 	    encontrado=true;
 	    i++;  
	}


	String profesionalsolicitante= "s/d";
	String servicioSolicitante= "s/d";
	String solicitadoA= "s/d";


        PersistenceManager manager = null;
        String query = null;
	LogFile.log("detalleActividad: sistema="+sistema);
	JDBCAnswerResultSet rs2 = null;
	
	   

 
// Profesional Solicitante:

	if(sistema.equals("SistemaNovahis")){	
	   try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  = "select fpersona.nombre_corto from fpersona, actividad where fpersona.codigo_personal = actividad.codigo_personal and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    profesionalsolicitante = (String)jrs2.getObject(1);
	   }	
	   catch(Exception ex){ 
	    LogFile.log(ex); 
	   }
	   finally{
	    rs2.close();	
	   } 
	}	


	if(sistema.equals("SistemaSiapwin")){	
	   try{
	    manager = SistemaSiapwin.getInstance().getPersistenceManager();
	    query  = "select fpersona.nombre_corto from fpersona, actividad where fpersona.codigo_personal = actividad.codigo_personal and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    profesionalsolicitante = (String)jrs2.getObject(1);
	   }	
	   catch(Exception ex){ 
	    LogFile.log(ex); 
	   }
	   finally{
	    rs2.close();	
	   } 
	}	

//Servicio Solicitante:

	if(sistema.equals("SistemaNovahis")){	
	   try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  = "select servicios.servicio from servicios, actividad where servicios.codigo_servicio = actividad.codigo_servicio and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    servicioSolicitante = (String)jrs2.getObject(1);
	   }	
	   catch(Exception ex){ 
	    LogFile.log(ex); 
	   }
	   finally{
	    rs2.close();	
	   } 
	}	


	if(sistema.equals("SistemaSiapwin")){	
	   try{
	    manager = SistemaSiapwin.getInstance().getPersistenceManager();
	    query  = "select servicios.servicio from servicios, actividad where servicios.codigo_servicio = actividad.codigo_servicio and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    servicioSolicitante = (String)jrs2.getObject(1);
	   }	
	   catch(Exception ex){ 
	    LogFile.log(ex); 
	   }
	   finally{
	    rs2.close();	
	   } 
	}	



//Servicio Solicitado a :


	if(sistema.equals("SistemaNovahis")){	
	   try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  = "select servicios.servicio from servicios, actividad where servicios.codigo_servicio = actividad.codigo_servicio1 and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    solicitadoA = (String)jrs2.getObject(1);
	   }	
	   catch(Exception ex){ 
	    LogFile.log(ex); 
	   }
	   finally{
	    rs2.close();	
	   } 
	}	


	if(sistema.equals("SistemaSiapwin")){	
	   try{
	    manager = SistemaSiapwin.getInstance().getPersistenceManager();
    	    query  = "select servicios.servicio from servicios, actividad where servicios.codigo_servicio = actividad.codigo_servicio1 and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    solicitadoA = (String)jrs2.getObject(1);
	   }	
	   catch(Exception ex){ 
	    LogFile.log(ex); 
	   }
	   finally{
	    rs2.close();	
	   } 
	}	

	



String tipoActividad[] = {"","PROCEDIMIENTOS","LABORATORIO","ANATOMIA PATOLOGICA","IMAGEN","CO","MEDICAMENTOS","MATERIALES","COCINA","DIETETICA","HOSTELERIA","OTROS"};
String estadoActividad[] = {"","ABIERTA","CERRADA","VALIDADA"};

%>


<html>
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script><head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>
</head>
<body onBlur="self.focus()" bgcolor="#eeeeee" marginheight="0" topmargin="0" onLoad="MM_preloadImages('<%=Parametros.imgpath%>bt_cerrar1.jpg')" leftmargin="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%"> 
<tr> <td valign="middle" height="387"> <table width="95%" border="0" cellspacing="0" cellpadding="0" valign="top" align="center"> 
<input type='hidden' name='view' value='busquedaPaciente'> <tr> <td> <table cellspacing=0 cellpadding=0 width=100% border=0> 
<tbody> <tr> <td width=14 background=<%=Parametros.imgpath%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=11 
      src="<%=Parametros.imgpath%>drwd_topleftcorner.gif" width=5></td><td width=904 background=<%=Parametros.imgpath%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=8 src="<%=Parametros.imgpath%>spacer.gif" 
    width=287></td><td width=17> <div align="right"><img height=11 src="<%=Parametros.imgpath%>drwd_toprightcorner.gif" 
      width=24></div></td></tr> </tbody> </table><table cellspacing=0 cellpadding=0 width=100% border=0 height="1"> 
<tbody> <tr> <td width=9 height="5"><img height=13 src="<%=Parametros.imgpath%>drwd_mdlleftcorner.gif" 
      width=5></td><td align=right width=35 bgcolor=#fcfdfa height="5"><img height=13 
      src="<%=Parametros.imgpath%>drwd_iconnews.gif" width=13></td><td width=865 bgcolor=#fcfdfa height="5"> 
<p><img src="<%=Parametros.imgpath%>tituDetalleActividad.jpg"></p></td><td width=26 height="5"> 
<div align="right"><img height=13 src="<%=Parametros.imgpath%>drwd_mdlrightcorner.gif" 
      width=24></div></td></tr> </tbody> </table><table cellspacing=0 cellpadding=0 width=100% border=0> 
<tbody> <tr> <td width=29 bgcolor=#f0f3e9><img height=20 
      src="<%=Parametros.imgpath%>btwd_topleftcorner.gif" width=31></td><td width=875 background=<%=Parametros.imgpath%>btwd_topbgr1.gif 
      bgcolor=#f0f3e9><img height=1 src="<%=Parametros.imgpath%>spacer.gif" 
    width=255></td><td width=31> <div align="right"><img height=20 src="<%=Parametros.imgpath%>btwd_toprightcorner.gif" 
      width=30></div></td></tr> </tbody> </table><table cellspacing=0 cellpadding=0 width=100% border=0> 
<tbody> <tr> <td width=32 background=<%=Parametros.imgpath%>btwd_mdlbgr1.gif 
      bgcolor=#f0f3e9><img src="<%=Parametros.imgpath%>btwd_mdlbgr1.gif"></td><td width=883 bgcolor=#f0f3e9> 
<table width="100%" border="0" cellspacing="3" cellpadding="0" align="center"> 
<tr> <td width="13%" height="1"> <p><img src="<%=Parametros.imgpath%>boxPaciente.jpg" width="57" height="8"></p></td><td width="39%" height="1"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=request.getSession().getValue("NombrePacienteActual")%></b></font></td><td width="18%" nowrap height="1"><img src="<%=Parametros.imgpath%>box_fechainicio.jpg" width="81" height="8"></td><td width="30%" height="1"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif" size="2"> 
<%    String campoFecha=null;
            if(unaActividad.ve_ac_fecha()!=null){
             java.sql.Date date = java.sql.Date.valueOf(unaActividad.ve_ac_fecha());
             java.text.DateFormat myformat = new java.text.SimpleDateFormat("dd/MM/yyyy");  
             campoFecha = myformat.format(date);
            }
            else{ 
             campoFecha = Parametros.sinDatos;
            }  
       %> </font> <div align="left"></div><div align="left"></div><div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#660000"><b><%=campoFecha%></b></font></div></td></tr> 
<tr> <td colspan="4" height="2" background="<%=Parametros.imgpath%>btwd_topbgr1.gif"><img src="<%=Parametros.imgpath%>btwd_topbgr1.gif" height="1"></td></tr> 
<tr> <td width="13%" nowrap height="6"><img src="<%=Parametros.imgpath%>boxHistoriaclinica.jpg" width="83" height="8"></td><td width="39%" height="6"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(String)request.getSession().getValue("NHC_Actual")%></b></font></td><td width="18%" height="6"><img src="<%=Parametros.imgpath%>box_horainicio.jpg" width="74" height="8"></td><td width="30%" height="6"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(unaActividad.ve_ac_hora()!=null)?unaActividad.ve_ac_hora().substring(0,5):Parametros.sinDatos%></b></font></td></tr> 
<tr> <td colspan="4" height="2" background="<%=Parametros.imgpath%>btwd_topbgr1.gif"><img src="<%=Parametros.imgpath%>btwd_topbgr1.gif" height="1"></td></tr> 
<tr> <td width="13%" height="1"><img src="<%=Parametros.imgpath%>Box_nactividad.jpg"> 
</td><td width="39%" height="1"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(unaActividad.ve_ac_ref()!=null)?unaActividad.ve_ac_ref():Parametros.sinDatos%></b></font></b></td><td width="18%" height="1"><img src="<%=Parametros.imgpath%>Box_ndeencuentro.jpg"></td><td width="30%" height="1"><b><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="##660000"><b><%=unaActividad.ve_en_ref()%></b></font><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"></font></b></td></tr> 
<tr> <td colspan="4" height="2" background="<%=Parametros.imgpath%>btwd_topbgr1.gif"><img src="<%=Parametros.imgpath%>btwd_topbgr1.gif" height="1"></td></tr> 
<tr> <td width="13%" height="19"><img src="<%=Parametros.imgpath%>boxprofsol.jpg"> 
</td><td width="39%" height="19"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(profesionalsolicitante!=null)?profesionalsolicitante:Parametros.sinDatos%></b></font><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#660000"><img src="pics/spacer.gif" width="100
				" height="1"></font></b></td><td width="18%" height="19"><img src="<%=Parametros.imgpath%>boxtipo.jpg"></td><td width="30%" height="19"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#660000"><%=(unaActividad.ve_ac_tipo()!=null)? tipoActividad[Integer.parseInt(unaActividad.ve_ac_tipo())]:Parametros.sinDatos%></font></b></td></tr> 
<tr> <td colspan="4" height="2" background="<%=Parametros.imgpath%>btwd_topbgr1.gif"><img src="<%=Parametros.imgpath%>btwd_topbgr1.gif" height="1"></td></tr> 
<tr> <td width="13%" height="1"><img src="<%=Parametros.imgpath%>Boxsolica.jpg" width="63" height="8"></td><td height="1"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(solicitadoA!=null)?solicitadoA:Parametros.sinDatos%></b></font></td><td height="1"><img src="<%=Parametros.imgpath%>box_serviciosol.jpg" width="100" height="8"></td><td height="1"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="##660000"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(servicioSolicitante!=null)?servicioSolicitante:Parametros.sinDatos%></b></font></b></font></td></tr> 
<tr> <td colspan="4" height="2" background="<%=Parametros.imgpath%>btwd_topbgr1.gif"><img src="<%=Parametros.imgpath%>btwd_topbgr1.gif" height="1"></td><tr> 
<td width="13%" nowrap height="38"><img src="<%=Parametros.imgpath%>Box_descripcion.jpg" width="62" height="12"></td><td colspan="3" height="38"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"></font><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#660000"><%=(unaActividad.ve_ac_servicio()!=null)?unaActividad.ve_ac_servicio():Parametros.sinDatos%></font></b></td></tr> 
</table><table width="90%" border="1" cellspacing="0" cellpadding="0"  align="center" bordercolor="#666666"> 
<tr bgcolor="#003366"> <td colspan="4" height="20" align="center"> <div align="left"><font color="#003366" face="Verdana, Arial, Helvetica, sans-serif" size="3"><b><font size="2" color="#FFFFFF">Prestaciones</font></b></font></div></td></tr> 
<%

//Prestaciones:


Vector prestaciones = new Vector();
int tamanoVector=0;

java.sql.Statement stm = null;
java.sql.ResultSet rs3  = null;

	if(sistema.equals("SistemaNovahis")){	
	   try{
	    PersistenceManager manager2 = SistemaNovahis.getInstance().getPersistenceManager();
	    query  ="select prest_item.prest_item_desc from actividad_det,prest_item where prest_item.prest_item_pk = actividad_det.prest_item_pk and actividad_det.actividad_pk ="+strNumero.trim();
	    DBConnection conexion = manager2.getConnection();
	    stm = ((java.sql.Connection)conexion.connection()).createStatement();
	    rs3 = stm.executeQuery(query);
                       
            while (rs3.next()){
		prestaciones.addElement((String)rs3.getString(1));
            }

    		rs3.close();
    		stm.close();
    		manager2.returnConnection(conexion); 


	   }	
	   catch(Exception ex){ 
	    LogFile.log(ex); 
	   }
	 finally{
		   try{
		    rs3.close();
    		    stm.close();
    		
		   }
		   catch(Exception e){
			LogFile.log("Detalle Actividad no puede cerrar el ResultSet");
		   }	
	   }	  
	}	

	if(sistema.equals("SistemaSiapwin")){	
	   try{
	    PersistenceManager manager2 = SistemaSiapwin.getInstance().getPersistenceManager();
	    query  ="select prest_item.prest_item_desc from actividad_det,prest_item where prest_item.prest_item_pk = actividad_det.prest_item_pk and actividad_det.actividad_pk ="+strNumero.trim();
	    DBConnection conexion = manager2.getConnection();
	    stm = ((java.sql.Connection)conexion.connection()).createStatement();
	    rs3 = stm.executeQuery(query);
                       
            while (rs3.next()){
		prestaciones.addElement((String)rs3.getString(1));
            }

    		rs3.close();
    		stm.close();
    		manager2.returnConnection(conexion); 


	   }	
	   catch(Exception ex){ 
	    LogFile.log(ex); 
	   }
	 finally{
		   try{
		    rs3.close();
    		    stm.close();
    		
		   }
		   catch(Exception e){
			LogFile.log("Detalle Actividad no puede cerrar el ResultSet");
		   }	
	   }	  
	}	




         tamanoVector = prestaciones.size();

if (tamanoVector > 0){
for (int h = 0; h < tamanoVector; h++) 
	{

%> <tr bgcolor="#FFFFFF"> <td colspan="4" height="30" align="center"> <div align="left"><b><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=prestaciones.elementAt(h)%></font></b></div></td></tr> 
<tr bgcolor="#FFFFCC"> <td colspan="4" height="2" bgcolor="#CCCC99" align="center"><img src="<%=Parametros.imgpath%>dottrans.gif" width="1" height="1"></td></tr> 
<%
}
%> </table></td><td width=24 background=<%=Parametros.imgpath%>btwd_mdlbgr2.gif nowrap><img src="<%=Parametros.imgpath%>btwd_mdlbgr2.gif" width="30" height="100%"></td></tr> 
</tbody> </table><table cellspacing=0 cellpadding=0 border=0 width=100%> <tbody> 
<tr> <td width=5><img height=26 src="<%=Parametros.imgpath%>btwd_btmleftcorner.gif" 
      width=5></td><td background=<%=Parametros.imgpath%>btwd_btmbgr1.gif width="20"><img height=26 
      src="<%=Parametros.imgpath%>btwd_btmmiddle.gif" width=26></td><td background=<%=Parametros.imgpath%>btwd_btmbgr1.gif width="863"><img height=1 
      src="<%=Parametros.imgpath%>spacer.gif" width=255></td><td background=<%=Parametros.imgpath%>btwd_btmbgr2.gif width="23"> 
<div align="right"><img height=26 
      src="<%=Parametros.imgpath%>btwd_btmmiddle2.gif" width=23></div></td><td width=9> 
<div align="right"><img height=26 src="<%=Parametros.imgpath%>btwd_btmrightcorner.gif" 
      width=7></div></td></tr> </tbody> </table></td></tr> </table><br> <%
}

if (tamanoVector == 0){
%><tr bgcolor="#FFFFFF"> <td colspan="3" height="30" align="center"> <div align="center"><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000" SIZE="2"><B>No 
se registraron Prestaciones</B></FONT></div></td></tr> <tr bgcolor="#FFFFCC"> 
<td colspan="4" height="2" bgcolor="#CCCC99" align="center"><img src="<%=Parametros.imgpath%>dottrans.gif" width="1" height="1"></td></tr> 
</table><%
}
%> <div align="center"><A HREF="#" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image37','','<%=Parametros.imgpath%>bt_cerrar1.jpg',1)" ONCLICK="window.close();return false"><IMG NAME="Image37" BORDER="0" SRC="<%=Parametros.imgpath%>bt_cerrar.jpg" WIDTH="78" HEIGHT="23"></A><br> 
</div>
</body></html>
