
#include "ficibm.h"

void parg(int argc, char *argv[], FILE *&fp1, FILE *& fp2, bool& edit, bool &check);
void editOpc(FILE *fp2);
void chequeo(void);

int main(int argc, char *argv[]){
	FILE *fp1,*fp2;
	bool check=false,edit=false;

	parg(argc,argv,fp1,fp2,edit,check);

	//Cargar definiciones
	Compila(fp1);
	fclose(fp1);

	//Acciones
	if(edit)
		editOpc(fp2);

	return 0;
}

