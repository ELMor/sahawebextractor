
#include <process.h>
#include <tcp4u.h>
#include <udp4u.h>
#include "fichero.h"
#include "cursor.h"
#include "campo.h"

extern CCam** lisCam;
extern ulong  nlisCam;

void editOpc( FILE *fp2 ){
	//Inicio del programa VB:
	_spawnlp(_P_NOWAIT,"EdFields.exe","EdFields.exe",NULL);

	//Inicio servidor Socket
	bool seguir=true;
	{
		char* buf=(char*)malloc(202);
		LPUDPSOCK lpSock=NULL;
		int Rc,NByt;
		ulong it;
		Tcp4uInit();
		it=0;
		Rc = UdpInit(&lpSock,"127.0.0.1",0,7);
		for ( ; Rc>=TCP4U_SUCCESS; ){
 			NByt= Rc= UdpRecv (lpSock, buf, 202, 65535, -1);
			Rc = UdpBind (lpSock, TRUE, UDP4U_SERVER);
			if(NByt==-1){
				puts("Error en la recepcion.");
				UdpBind (lpSock, FALSE, 0);
				continue;
			}
			buf[NByt>0?NByt:0]=0;
			if( !stricmp(buf,"INIT") ){
				strcpy(buf,"MAS");
				Rc = UdpSend (lpSock, buf, strlen(buf), 0, -1);
				UdpBind (lpSock, FALSE, 0);
			}else if(!stricmp(buf,"SIG")){
				if( it<nlisCam ){
					sprintf(buf,"%02ld",lisCam[it]->getNiv());
					lisCam[it]->dump(buf+2);
					Rc = UdpSend (lpSock, buf, 202, 0, -1);
					it++;
				}else
					Rc = UdpSend (lpSock, "EMPTY", 5, 0, -1);
				UdpBind (lpSock, FALSE, 0);
			}else if(!stricmp(buf,"END")){
				UdpBind (lpSock, FALSE, 0);
				break;
			}else if(!stricmp(buf,"CANCEL")){
				UdpBind (lpSock, FALSE, 0);
				seguir=false;
				break;
			}else{
				UdpBind (lpSock, FALSE, 0);
				int numcam;
				char opc[20],tabla[40];
				sscanf(buf,"%d %s %s",&numcam,opc,tabla);
				ulong it2=0;
				while(--numcam)
					it2++;
				if      (!stricmp(opc,"ENTCONSIG")){
					lisCam[it2]->getOpc()->setTip(EntConSig);
				}else if(!stricmp(opc,"ENTSINSIG")){
					lisCam[it2]->getOpc()->setTip(EntSinSig);
				}else if(!stricmp(opc,"ENTSINSIGBIN")){
					lisCam[it2]->getOpc()->setTip(EntSinSig_Binario);
				}else if(!stricmp(opc,"FECABS")){
					lisCam[it2]->getOpc()->setTip(FecAbs);
				}else if(!stricmp(opc,"FECREL")){
					lisCam[it2]->getOpc()->setTip(FecRel);
				}else if(!stricmp(opc,"DECIMAL")){
					lisCam[it2]->getOpc()->setTip(Decimal);
				}else if(!stricmp(opc,"TEXSINCOM")){
					lisCam[it2]->getOpc()->setTip(TexSinCom);
				}else if(!stricmp(opc,"TEXJUE32")){
					lisCam[it2]->getOpc()->setTip(TexJue32);
				}else if(!stricmp(opc,"TEXJUE40")){
					lisCam[it2]->getOpc()->setTip(TexJue40);
				}else if(!stricmp(opc,"BINARIO")){
					lisCam[it2]->getOpc()->setTip(Binario);
				}else if(!stricmp(opc,"TABLA")) {
					lisCam[it2]->getOpc()->setTip(Tabla,strdup(tabla));
				}
			}
		}
		if (lpSock!=NULL)   
		  UdpCleanup (lpSock);
		Tcp4uCleanup();
	}

	if(seguir){
		ulong it;
		for(it=0;it<nlisCam;it++)
			if(lisCam[it]->getTip()->getTip()==TDCL){
				lisCam[it]->dump(NULL,fp2);
				fprintf(fp2,"\n");
			}
		fclose(fp2);
	}else{
		puts("Proceso cancelado");
	}
}