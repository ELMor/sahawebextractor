
#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
#include "valor.h"
#include "parse.h"
#include "xlat.h"
#include "pack.h"
#include "tablas.h"
#include "fecha.h"
#include "fecibm.h"

static char spaces[512];
extern	CLisTab	lTab;
extern	FILE*	fpExport;
		CVal	DefVal((CVal*)NULL);

void
t_valor::Init(char *s){
	if(s){
		strcpy((char*)str,s);
		ent=atol(s);
		dec=atof(s);
		memset(key,0,MAXTAMKEY);
	}else{
		ent=0;
		dec=0;
		str[0]=0;
		memset(key,0,MAXTAMKEY);
	}
}

//------------------------- Funciones de CVal ----------------------------
CVal::CVal( CVal *th ){
	campo=	th ? th->campo	: (CCam*)NULL ;
	cursor=	th ? th->cursor	: (CCur*)NULL ;
	lastreg=th ? th->lastreg: -1;
	ioffset=th ? th->ioffset: 0 ;
	valor.Init();
}

CVal::CVal( CVal& th ){
	campo=	th.campo;
	cursor=	th.cursor;
	lastreg=th.lastreg;
	ioffset=th.ioffset;
	valor.Init();
}

CVal*
CVal::parse(CCur *c,char *def){
	fpExport=tmpfile();
	fprintf(fpExport,"$EXPR %s$",def);
	fseek(fpExport,0,SEEK_SET);
	yyparse();
	fclose(fpExport);
	DefVal.lastreg=-1;
	DefVal.cursor=c;
	if(c)
		DefVal.proc();
	return &DefVal;
}

long 
CVal::DumpHeaders( FILE *fp, char *suffix, int Modo, long abp, long prim ){
	long i,j;
	char buf[256];
	*buf=0;
	//Cadena cpon espacios en blanco.
	memset(spaces,' ',511);
	spaces[511]=0;

	for(i=0;i<campo->getVar()->getIns()->getD1();i++){
		if( i>0 )	//Sufijo de dimension >1
			sprintf(buf,"%s_%02d",suffix,i+1);
		else
			strcpy(buf,suffix);
		if(campo->nhijos>0){
			for(j=0;j<(long)campo->nhijos;j++){
				CVal tmp(this);
				tmp.proc();
				if(i){
					tmp=tmp[i];
					tmp.proc();
				}
				tmp=tmp[campo->lhijos[j]->getVar()->getNom()];
				tmp.proc();
				abp=tmp.DumpHeaders(fp,buf,Modo,abp,prim);
				prim=0;
			}
		}else{
			char saux[10],nm[256];
			campo->getNomCom(nm,0);
			strcat(nm,buf);
			switch(Modo){
			case 0:
				sprintf(saux,"%%-%ds",campo->minanch);
				fprintf(fp,saux,nm);
				break;
			case 1:
				if(!prim) fprintf(fp,",\n");
				prim=0;
				switch(campo->getOpc()->getTip()){
				case Binario:case Decimal:case EntConSig:
					fprintf(fp,"%20s NUMBER(15,2)",nm);
					break;
				default:
					fprintf(fp,"%20s VARCHAR2(%ld)",nm,campo->minanch);
				}
				break;
			case 2:
				if(!prim) fprintf(fp,",\n");
				prim=0;
				switch(campo->getOpc()->getTip()){
				case Binario:case Decimal:case EntConSig:
					fprintf(fp,"%20s POSITION(%04ld:%04ld) INTEGER EXTERNAL NULLIF %s=BLANKS",
						nm,
						abp,
						abp+campo->minanch-1,
						nm);
					break;
				default:
					fprintf(fp,"%20s POSITION(%04ld:%04ld) CHAR NULLIF %s=BLANKS",
						nm,
						abp,
						abp+campo->minanch-1,
						nm);
				}
				abp+=campo->minanch;
				break;
			}
		}
	}
	return abp;
}

void 
CVal::Dump( FILE *fp, int Modo ){
	static long empty=0;
	long i,j;
	for(i=0;i<campo->getVar()->getIns()->getD1();i++){
		if(campo->nhijos>0){
			for(j=0;j<(long)(campo->nhijos);j++){
				CVal hijo(this);
				if(i){
					hijo=hijo[i];
					hijo.proc();
				}
				hijo=hijo[campo->lhijos[j]->getVar()->getNom()];
				hijo.proc();
				if( i<campo->getVar()->getIns()->getTam(cursor) )
					hijo.Dump(fp,Modo);
				else{
					empty++;
					hijo.Dump(fp,Modo);
					empty--;
				}
			}
		}else{
			if(Modo==0){
				char saux[20],taux[MAXTAMKEY];
				t_valor vv=valor;
				if(empty)		//Por si se sale del MAX en los registros de tama�o variable.
					vv.Init();
				switch(campo->getOpc()->getTip()){
					case FecAbs:
						sprintf(saux,"%%-%dd",campo->minanch);
						fprintf(fp,saux,vv.ent);
						break;
					case FecRel:{
						if(vv.ent){
							char buffer[128];
							long anio= vv.ent/372;
							long mes=1+(vv.ent-anio*372)/31;
							long dia=1+(vv.ent-anio*372-(mes-1)*31);
							sprintf(buffer,"%04d%02d%02d",anio+1850,mes,dia);
							sprintf(saux,"%%-%ds",campo->minanch);
							fprintf(fp,saux,buffer);
						}else{
							sprintf(saux,"%%-%ds",campo->minanch);
							fprintf(fp,saux," ");
						}
						break;
								}
					case EntSinSig:
						sprintf(saux,"%%-%ds",campo->minanch);
						fprintf(fp,saux,vv.str);
						break;
					case EntConSig:case EntSinSig_Binario:
						sprintf(saux,"%%-%ds",campo->minanch);
						fprintf(fp,saux,vv.str);
						break;
					case Binario:case Binarioss:
						sprintf(saux,"%%-%dd",campo->minanch);
						fprintf(fp,saux,vv.ent);
						break;
					case Decimal:
						sprintf(saux,"%%-%df",campo->minanch);
						fprintf(fp,saux,vv.dec);
						break;
					case Tabla:
						sprintf(saux,"%%-%ds",campo->minanch);
						fprintf(fp,saux,keydec(taux));
						break;
					case TexSinCom:case TexJue32:case TexJue40:
						sprintf(saux,"%%-%ds",campo->minanch);
						fprintf(fp,saux,vv.str);
						break;
				}
			}else{
				long i;
				for(i=0;i<campo->getTam(cursor)/8;i++)
					fprintf(fp,"%02X",*(cursor->getPReg()+campo->getOff(cursor)/8+i));
				if(campo->getTam(cursor)/8+2 < campo->minanch)
					for(i=2+campo->getTam(cursor)/8;i<campo->minanch;i++)
						fprintf(fp," ");
			}
		}
	}
}

//Imprime en una cadena el valor.
char*
CVal::spr(char *saux){
	proc();
	switch(campo->getOpc()->getTip()){
		case FecAbs:
			sprintf(saux,"%s ",(char*)((*this)));
			break;
		case FecRel:{
			long valor=(long)((*this));
			if(valor){
				long anio= valor/372;
				long mes=1+(valor-anio*372)/31;
				long dia=1+(valor-anio*372-(mes-1)*31);
				sprintf(saux,"%04d%02d%02d ",anio+1850,mes,dia);
			}else{
				sprintf(saux," ");
			}
			break;
					}
		case EntConSig:case EntSinSig:case EntSinSig_Binario:
			sprintf(saux,"%d ",(long)((*this)));
			break;
		case Binario:case Binarioss:
			sprintf(saux,"%d",(long)((*this)));
			break;
		case Decimal:
			sprintf(saux,"%f",(ldou)((*this)));
			break;
		case Tabla:
			keydec(saux);
			break;
		case TexSinCom:case TexJue32:case TexJue40:
			sprintf(saux,"%s",(char*)((*this)));
			break;
	}
	return saux;
}

void 
CVal::proc(void){
	if(!cursor || cursor->getRegAct()<0)  			//cursor no inicializado todav�a
		return;						
	uchar *c=cursor->getPReg();						//Offset al inicio del registro en curso
	uchar*of=c+ioffset+campo->getOff(cursor)/8;
	ulong nb=campo->getTam(cursor)/8;
	switch(tipo()){
	case UnDef:															
		break;
	case EntConSig: 
		//Ojo: los tama�o y offsets est�n en BITS!!!!!
		//Ver archivos Pack.h y XLat.h
		valor.ent=updcbcs(of,2*nb);	
		updcbcs(of,2*nb,(char*)valor.str);
		break;
	case EntSinSig:	
		valor.ent=updcbss(of,2*nb);	
		updcbss(of,2*nb,(char*)valor.str);
		break;
	case EntSinSig_Binario:
		if(nb==3){
			valor.ent=upbincs(of,nb);
			if(valor.ent>=10485760)
				valor.ent-= 485760;
			else
				valor.ent=updcbss(of,2*nb);
		}else if(nb==6){
			if( of[0]==255 && of[1]==255 )
				valor.ent=upbincs(of+2,4);
			else{
				uchar s[6];
				se2a(s,of,6);
				valor.ent=atoi((char*)s);
			}
		}else
			valor.ent=updcbss(of,2*nb);
		break;
	case FecAbs:	
		valor.ent=updcbss(of,2*nb);	
		sprintf((char*)(valor.str),"%ld",valor.ent);
		break;
	case FecRel:
		valor.ent=upbinss(of,nb);	
		break;
	case Binarioss:
		valor.ent=upbinss(of,nb);	
		break;
	case Binario:
		valor.ent=upbincs(of,nb);
		break;
	case Decimal:
		valor.dec=updcbcs(of,2*nb);
		{	ulong decimales=campo->getIns()->getD2();
			while( decimales-- )
				valor.dec/=10;
		}
		break;
	case TexSinCom:
		se2a(valor.str,of,nb);
		valor.str[nb]=0;
		break;
	case TexJue32:
		unpack8en5(valor.str,of,nb);
		sup32(valor.str,valor.str,campo->getTam()/5);
		valor.str[campo->getTam()/5]=0;
		break;
	case TexJue40:
		unpackb40(valor.str,of,nb);
		sup40(valor.str,valor.str,campo->getTam()*3/16);
		valor.str[campo->getTam()*3/16]=0;
		break;
	case Tabla:
		uchar *ss=(valor.key);
		memset(ss,0,MAXTAMKEY);
		memcpy(valor.key,of,nb);
		break;
	};
}

char *
CVal::des(void){
	char *ret=(char*)
		lTab.getDes(campo->getOpc()->getNTab(),valor.key,1);
	return ret;
}

char *
CVal::keydec(char *s){
	CTab* tbl=lTab.getTab(campo->getOpc()->getNTab());
	switch( tbl->tconv ){
	case 1: sprintf(s,"%d",updcbcs(valor.key,2*tbl->getTamKey())); break;
	case 2: sprintf(s,"%d",upbinss(valor.key,tbl->getTamKey())); break;
	case 3: se2a((uchar*)s,valor.key,tbl->getTamKey());			 break;
	}
	return s;
}

char *
CVal::cod(void){
	char *ret=(char*)
		lTab.getDes(campo->getOpc()->getNTab(),valor.key,0);
	return ret;
}

bool operator< (CVal& v1, CVal& v2){
	v1.proc();
	v2.proc();
	switch(v1.tipo()){
	case FecRel:case EntConSig:case EntSinSig:case Binario:case Binarioss:
		if( (long)(v1) < (long)(v2) )
			return true;
		break;
	case Decimal:
		if( (ldou)(v1) < (ldou)(v2) )
			return true;
		break;
	case FecAbs:case TexSinCom:case TexJue32:case TexJue40:
		if( stricmp((char*)(v1),(char*)(v2))<0 )
			return true;
		break;
	case Tabla:
		if( stricmp(v1.cod(),v2.cod())<0 )
			return true;
		break;
	}
	return false;
}
bool operator> ( CVal& v1,  CVal& v2){
	v1.proc();
	v2.proc();
	switch(v1.tipo()){
	case FecRel:case EntConSig:case EntSinSig:case Binario:case Binarioss:
		if( (long)(v1) > (long)(v2) )
			return true;
		break;
	case Decimal:
		if( (ldou)(v1) > (ldou)(v2) )
			return true;
		break;
	case FecAbs:case TexSinCom:case TexJue32:case TexJue40:
		if( stricmp((char*)(v1),(char*)(v2))>0 )
			return true;
		break;
	case Tabla:
		if( stricmp(v1.cod(),v2.cod())>0 )
			return true;
		break;
	}
	return false;
}
bool operator==( CVal& v1,  CVal& v2){
	v1.proc();
	v2.proc();
	switch(v1.tipo()){
	case FecRel:case EntConSig:case EntSinSig:case Binario:case Binarioss:
		if( (long)(v1) == (long)(v2) )
			return true;
		break;
	case Decimal:
		if( (ldou)(v1) == (ldou)(v2) )
			return true;
		break;
	case FecAbs:case TexSinCom:case TexJue32:case TexJue40:
		if( stricmp((char*)(v1),(char*)(v2))==0 )
			return true;
		break;
	case Tabla:
		if( stricmp(v1.cod(),v2.cod())==0 )
			return true;
		break;
	}
	return false;
}
bool operator!=( CVal& v1,  CVal& v2){
	v1.proc();
	v2.proc();
	switch(v1.tipo()){
	case FecRel:case EntConSig:case EntSinSig:case Binario:case Binarioss:
		if( (long)(v1) != (long)(v2) )
			return true;
		break;
	case Decimal:
		if( (ldou)(v1) != (ldou)(v2) )
			return true;
		break;
	case FecAbs:case TexSinCom:case TexJue32:case TexJue40:
		if( stricmp((char*)(v1),(char*)(v2))!=0 )
			return true;
		break;
	case Tabla:
		if( stricmp(v1.cod(),v2.cod())!=0 )
			return true;
		break;
	}
	return false;
}

CVal 
CVal::operator[](long i){
	CVal ret(this);
	ret.ioffset+= i*ret.campo->getTam(cursor)/8;
	ret.valor.Init();
	return ret;
}

CVal 
CVal::operator[](char *s){
	CVal ret(this);
	if(!(ret.campo=CCam::find(s,ret.campo))){
		printf("Subcampo %s no encontrado\nPresione <ret> para terminar.",s);
		fgetc(stdin);
		exit(-1);
	}
	ret.valor.Init();
	//printf("CVal(%s)::operator[s=%s];offset=%ld\n",
		//campo->getVar()->getNom(),s,ret.campo->getOff()/8);
	return ret;
}

CVal::operator uchar*(){
	proc();
	return valor.str;
}

CVal::operator char*(){
	proc();
	return (char*)(valor.str);
}

CVal::operator long(){
	proc();
	long ret=campo->getTip()->getTip()==TDEC?(long)valor.dec:valor.ent;
	return ret;
}

CVal::operator ldou(){
	proc();
	return valor.dec;
}

tipInt
CVal::tipo(void){ 
	return campo->getOpc()->getTip();
}
