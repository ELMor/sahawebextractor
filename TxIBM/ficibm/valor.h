

#ifndef __VALOR_H__
#define __VALOR_H__

#include "defgen.h"
#include "cursor.h"
#include "campo.h"

//Clase buena
class CVal {
private:
	t_valor		valor;
public:
	//Var member
	CCam*		campo;		
	CCur*		cursor;
	long		lastreg;
	long		ioffset;
	//Functions
			CVal		(CVal& th);
			CVal		(CVal* th);
static CVal*parse		(CCur* c, char *def);
void		proc		(void);
tipInt		tipo		(void);				
char*		des			(void);
char*		cod			(void);
char*		keydec		(char *s);

void		Dump		(FILE *fp,int Modo=0);
long		DumpHeaders	(FILE *fp,char *suffix="", int Modo=0, long abp=1, long prim=1);
char*		spr			(char*saux);

void		setCur		(CCur* c) {cursor=c; }

CVal	operator[]	(long pos);
CVal	operator[]	(char* subcampo);
		operator uchar*	();
		operator char*	();
		operator ldou	();
		operator long	();
};


#endif
