
/*  A Bison parser, made from gramm.y
 by  GNU Bison version 1.25
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	NUM	258
#define	NOM	259
#define	DCL	260
#define	UNAL	261
#define	BASED	262
#define	CHAR	263
#define	BIN	264
#define	BIT	265
#define	DEC	266
#define	PIC	267
#define	FIXED	268
#define	MASK	269
#define	INIT	270
#define	REFER	271
#define	ENTCONSIG	272
#define	ENTSINSIG	273
#define	ENTSINSIGBIN	274
#define	FECABS	275
#define	FECREL	276
#define	DECIMAL	277
#define	TEXSINCOM	278
#define	TEXJUE32	279
#define	TEXJUE40	280
#define	TABLA	281
#define	BINARIO	282
#define	BINARIOSS	283
#define	MAXIMUM	284
#define	DEFINDICE	285
#define	EXPR	286

#line 2 "gramm.y"


//Includes Generales
#include <ctype.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "valor.h"

//Tipo de los tokens
#define YYSTYPE CObj*

//Fichero de donde extraer los tokens
extern FILE*	fpExport;

//Variables globales para parse de CVal y otros
extern CVal		DefVal;
extern char***	CamExp;
extern ulong	nCamExp;

//Prototipos.
void yyerror(char*s);
int  yylex(void);

//Ofrecer mas informacion si hay error
#define YYERROR_VERBOSE

//Para depurar:
//#define duts(a) puts(a)
#define duts(a) 
//#define drintf(a) printf(a); printf(" ");
#define drintf(a)

//Stack para los Refers
#ifndef YYSTYPE
#define YYSTYPE int
#endif
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		93
#define	YYFLAG		-32768
#define	YYNTBASE	37

#define YYTRANSLATE(x) ((unsigned)(x) <= 286 ? yytranslate[x] : 53)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    34,
    35,     2,     2,    32,     2,    36,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,    33,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     4,     6,     8,    11,    14,    21,    23,    27,
    30,    37,    38,    43,    44,    46,    48,    50,    52,    54,
    56,    58,    60,    62,    65,    67,    69,    70,    73,    79,
    82,    83,    86,    90,    94,    97,   100,   101,   105,   111,
   121,   125,   127,   130,   135,   139,   142,   144,   148,   150
};

static const short yyrhs[] = {    49,
     0,    38,     0,    50,     0,    39,     0,    38,    39,     0,
    40,    41,     0,    48,     5,     3,     4,    47,    45,     0,
    42,     0,    41,    32,    42,     0,    41,    33,     0,     3,
     4,    47,    46,    43,    44,     0,     0,    15,    34,    14,
    35,     0,     0,    17,     0,    18,     0,    19,     0,    20,
     0,    21,     0,    22,     0,    23,     0,    24,     0,    25,
     0,    26,     4,     0,    27,     0,    28,     0,     0,    45,
     6,     0,    45,     7,    34,     4,    35,     0,    45,    32,
     0,     0,     8,    47,     0,     9,    13,    47,     0,    11,
    13,    47,     0,    10,    47,     0,    12,    14,     0,     0,
    34,     3,    35,     0,    34,     3,    32,     3,    35,     0,
    34,     4,    16,    34,     4,    35,    29,     3,    35,     0,
     3,     3,     3,     0,    31,     0,    49,     4,     0,    49,
    34,     3,    35,     0,    49,    36,     4,     0,    30,    51,
     0,    52,     0,    51,    32,    52,     0,     4,     0,    52,
    36,     4,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    77,    78,    79,    81,    82,    84,    87,    96,    97,    98,
   100,   110,   111,   113,   114,   115,   116,   117,   118,   119,
   120,   121,   122,   123,   124,   125,   127,   128,   129,   130,
   132,   133,   134,   138,   139,   140,   142,   143,   144,   146,
   150,   152,   153,   158,   159,   161,   163,   170,   178,   179
};
#endif

#define YYNTOKENS 37
#define YYNNTS 16
#define YYNRULES 50
#define YYNSTATES 94
#define YYMAXUTOK 286

static const char * const yytname[] = {   "$","error","$undefined.","NUM","NOM",
"\"DCL\"","\"UNAL\"","\"BASED\"","\"CHAR\"","\"BIN\"","\"BIT\"","\"DEC\"","\"PIC\"",
"\"FIXED\"","MASK","\"INIT\"","\"REFER\"","\"ENTCONSIG\"","\"ENTSINSIG\"","\"ENTSINSIGBIN\"",
"\"FECABS\"","\"FECREL\"","\"DECIMAL\"","\"TEXSINCOM\"","\"TEXJUE32\"","\"TEXJUE40\"",
"\"TABLA\"","\"BINARIO\"","\"BINARIOSS\"","\"MAX\"","\"DEFINDICE\"","\"EXPR\"",
"','","';'","'('","')'","'.'","program","listdcl","dcl","dclhead","listcam",
"campo","init","opcam","opdcl","tipo","dim","TNUM","expr","defindi","ldefi",
"defi", NULL
};
static const short yytoknum[] = { 0,
   256,   257,   258,   259,   260,   261,   262,   263,   264,   265,
   266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
   276,   277,   278,   279,   280,   281,   282,   283,   284,   285,
   286,    44,    59,    40,    41,    46,     0
};

static const short yyr1[] = {     0,
    37,    37,    37,    38,    38,    39,    40,    41,    41,    41,
    42,    43,    43,    44,    44,    44,    44,    44,    44,    44,
    44,    44,    44,    44,    44,    44,    45,    45,    45,    45,
    46,    46,    46,    46,    46,    46,    47,    47,    47,    47,
    48,    49,    49,    49,    49,    50,    51,    51,    52,    52
};

static const short yyr2[] = {     0,
     1,     1,     1,     1,     2,     2,     6,     1,     3,     2,
     6,     0,     4,     0,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     2,     1,     1,     0,     2,     5,     2,
     0,     2,     3,     3,     2,     2,     0,     3,     5,     9,
     3,     1,     2,     4,     3,     2,     1,     3,     1,     3
};

static const short yydefact[] = {     0,
     0,     0,    42,     2,     4,     0,     0,     1,     3,     0,
    49,    46,    47,     5,     0,     6,     8,     0,    43,     0,
     0,    41,     0,     0,    37,     0,    10,     0,     0,    45,
    48,    50,     0,    31,     9,    37,    44,     0,     0,    37,
     0,    37,     0,     0,    12,    27,     0,    38,     0,    32,
    37,    35,    37,    36,     0,    14,     7,     0,     0,    33,
    34,     0,    15,    16,    17,    18,    19,    20,    21,    22,
    23,     0,    25,    26,    11,    28,     0,    30,    39,     0,
     0,    24,     0,     0,    13,     0,     0,    29,     0,    40,
     0,     0,     0
};

static const short yydefgoto[] = {    91,
     4,     5,     6,    16,    17,    56,    75,    57,    45,    34,
     7,     8,     9,    12,    13
};

static const short yypact[] = {    -2,
     1,     4,-32768,    12,-32768,    14,    -3,    -4,-32768,    22,
-32768,    -6,    -9,-32768,    41,   -11,-32768,    43,-32768,    44,
    45,-32768,     4,    46,    17,    14,-32768,    48,    13,-32768,
    -9,-32768,    20,     2,-32768,    17,-32768,   -16,    37,    17,
    42,    17,    47,    40,    49,-32768,    53,-32768,    23,-32768,
    17,-32768,    17,-32768,    24,    16,    -1,    26,    55,-32768,
-32768,    51,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,    58,-32768,-32768,-32768,-32768,    29,-32768,-32768,    31,
    32,-32768,    64,    50,-32768,    34,    67,-32768,    36,-32768,
    72,    73,-32768
};

static const short yypgoto[] = {-32768,
-32768,    70,-32768,-32768,    52,-32768,-32768,-32768,-32768,   -33,
-32768,-32768,-32768,-32768,    54
};


#define	YYLAST		79


static const short yytable[] = {    19,
     1,    18,    46,    10,    76,    77,    50,    11,    52,    40,
    41,    42,    43,    44,     1,    47,    15,    60,    48,    61,
    26,    27,    38,    39,    22,    23,    24,     2,     3,    20,
    78,    21,    63,    64,    65,    66,    67,    68,    69,    70,
    71,    72,    73,    74,    25,    28,    29,    37,    30,    32,
    33,    36,    49,    54,    51,    58,    59,    62,    80,    53,
    79,    82,    83,    55,    81,    84,    85,    86,    88,    89,
    90,    92,    93,    14,     0,     0,    31,    35,    87
};

static const short yycheck[] = {     4,
     3,     5,    36,     3,     6,     7,    40,     4,    42,     8,
     9,    10,    11,    12,     3,    32,     3,    51,    35,    53,
    32,    33,     3,     4,     3,    32,    36,    30,    31,    34,
    32,    36,    17,    18,    19,    20,    21,    22,    23,    24,
    25,    26,    27,    28,     4,     3,     3,    35,     4,     4,
    34,     4,    16,    14,    13,     3,    34,    34,     4,    13,
    35,     4,    34,    15,    14,    35,    35,     4,    35,     3,
    35,     0,     0,     4,    -1,    -1,    23,    26,    29
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 196 "bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 6:
#line 84 "gramm.y"
{ yyval=yyvsp[0];
										   duts("1.3");						;
    break;}
case 7:
#line 87 "gramm.y"
{ CVar *v=new CVar(yyvsp[-2]->getBuf(),
																new CDim(1));
										  int lev=atoi(yyvsp[-3]->getBuf());
										  CCam *cc=new CCam(v,lev); 
										  cc->setTip(new CTip(TDCL,(CDim*)NULL));
										  cc->setOpc(new COpc);
										  yyval=cc;
										  duts("4.1");						;
    break;}
case 8:
#line 96 "gramm.y"
{ yyval=yyvsp[0];							;
    break;}
case 9:
#line 97 "gramm.y"
{ yyval=yyvsp[0]; duts("1.2");				;
    break;}
case 10:
#line 98 "gramm.y"
{ yyval=yyvsp[-1]; ((CCam*)yyvsp[-1])->setPyc(true);	;
    break;}
case 11:
#line 101 "gramm.y"
{ CDim* d=(CDim*)yyvsp[-3];
										  CVar* v=new CVar(yyvsp[-4]->getBuf(),d);
										  uint lev=atoi(yyvsp[-5]->getBuf());
										  CCam* c=new CCam(v,lev);
										  c->setTip(yyvsp[-2]);
										  c->setOpc((COpc*)yyvsp[0]);
										  yyval=c;								
										  duts("4.2");						;
    break;}
case 14:
#line 113 "gramm.y"
{ yyval=new COpc();					;
    break;}
case 15:
#line 114 "gramm.y"
{ yyval=new COpc(EntConSig);			;
    break;}
case 16:
#line 115 "gramm.y"
{ yyval=new COpc(EntSinSig);			;
    break;}
case 17:
#line 116 "gramm.y"
{ yyval=new COpc(EntSinSig_Binario);	;
    break;}
case 18:
#line 117 "gramm.y"
{ yyval=new COpc(FecAbs);				;
    break;}
case 19:
#line 118 "gramm.y"
{ yyval=new COpc(FecRel);				;
    break;}
case 20:
#line 119 "gramm.y"
{ yyval=new COpc(Decimal);				;
    break;}
case 21:
#line 120 "gramm.y"
{ yyval=new COpc(TexSinCom);			;
    break;}
case 22:
#line 121 "gramm.y"
{ yyval=new COpc(TexJue32);			;
    break;}
case 23:
#line 122 "gramm.y"
{ yyval=new COpc(TexJue40);			;
    break;}
case 24:
#line 123 "gramm.y"
{ yyval=new COpc(Tabla,yyvsp[0]->getBuf());	;
    break;}
case 25:
#line 124 "gramm.y"
{ yyval=new COpc(Binario);				;
    break;}
case 26:
#line 125 "gramm.y"
{ yyval=new COpc(Binarioss);			;
    break;}
case 31:
#line 132 "gramm.y"
{ yyval=new CTip();					;
    break;}
case 32:
#line 133 "gramm.y"
{ yyval=new CTip(TCHAr,(CDim*)yyvsp[0]);		;
    break;}
case 33:
#line 134 "gramm.y"
{ if(((CDim*)yyvsp[0])->getTam()==1)
											yyval=new CTip(TBIN,new CDim(15));
										  else
										    yyval=new CTip(TBIN, (CDim*)yyvsp[0]);	;
    break;}
case 34:
#line 138 "gramm.y"
{ yyval=new CTip(TDEC, (CDim*)yyvsp[0]);		;
    break;}
case 35:
#line 139 "gramm.y"
{ yyval=new CTip(TBIT, (CDim*)yyvsp[0]);		;
    break;}
case 36:
#line 140 "gramm.y"
{ yyval=new CTip(TPIC,(CDim*)NULL,yyvsp[0]->getBuf());;
    break;}
case 37:
#line 142 "gramm.y"
{ yyval=new CDim(1);					;
    break;}
case 38:
#line 143 "gramm.y"
{ yyval=new CDim(atoi(yyvsp[-1]->getBuf()));	;
    break;}
case 39:
#line 144 "gramm.y"
{ yyval=new CDim(atoi(yyvsp[-3]->getBuf()),
													atoi(yyvsp[-1]->getBuf()));	;
    break;}
case 40:
#line 147 "gramm.y"
{ yyval=new CDim(CCam::find(yyvsp[-4]->getBuf()),
														atoi(yyvsp[-1]->getBuf()) ); ;
    break;}
case 43:
#line 153 "gramm.y"
{ DefVal.campo=CCam::find(yyvsp[0]->getBuf());
										  if(!DefVal.campo){
										    printf("%s no encontrado",yyvsp[0]->getBuf());
											exit(-1);
										  }									;
    break;}
case 44:
#line 158 "gramm.y"
{ DefVal=DefVal[(long)atoi(yyvsp[-1]->getBuf())];;
    break;}
case 45:
#line 159 "gramm.y"
{ DefVal=DefVal[yyvsp[0]->getBuf()];		;
    break;}
case 47:
#line 163 "gramm.y"
{ (*CamExp)=(char**)realloc(*CamExp,
												(1+nCamExp)*sizeof(char*));
										  (*CamExp)[nCamExp]=(char*)malloc(1+
																yyvsp[0]->len());
										  strcpy((*CamExp)[nCamExp],
															yyvsp[0]->getBuf());
										  nCamExp++;						;
    break;}
case 48:
#line 170 "gramm.y"
{ (*CamExp)=(char**)realloc(*CamExp,
												(1+nCamExp)*sizeof(char*));
										  (*CamExp)[nCamExp]=(char*)malloc(1+
																yyvsp[0]->len());
										  strcpy((*CamExp)[nCamExp],
															yyvsp[0]->getBuf());
										  nCamExp++;						;
    break;}
case 49:
#line 178 "gramm.y"
{ yyval=yyvsp[0];							;
    break;}
case 50:
#line 179 "gramm.y"
{ yyval=yyvsp[-2]; 
										  yyval->ane("."); yyval->ane(yyvsp[0]->getBuf());;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 498 "bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 183 "gramm.y"


void skipblank(FILE *fp){
	char c=' ';
	while(c==' ' || c=='\t' || c=='\n')
		c=getc(fp);
	ungetc(c,fp);
}

int yylex(){
	static bool primerDollar=false;
	char Buffer[120];
	int c;
	while(1){
		skipblank(fpExport);
		c=getc(fpExport);
		if(c=='$'){
			if(primerDollar){
				primerDollar=false;
				while( c!='\n'){
					c=fgetc(fpExport);
					if(c==EOF)
						return 0;
				}
			}else{
				primerDollar=true;
			}
			continue;
		}
		if( c=='/' ){
			c=getc(fpExport);
			if(c=='*'){
				int i=0,c1,c2;
				do{
					c1=getc(fpExport);
					c2=getc(fpExport);
					ungetc(c2,fpExport);
				}while(!(c1=='*' && c2=='/'));
				c=getc(fpExport);
				continue;
			}else{
				ungetc(c,fpExport);
				drintf("/ ");
				return '/';
			}
		}
		break;
	}
	if(isdigit(c)){
		long valor;
		ungetc(c,fpExport);
		fscanf(fpExport,"%ld",&valor);
		sprintf(Buffer,"%ld",valor);
		yylval=new CObj(Buffer);
		drintf(Buffer);
		return NUM;
	}
	if(isalpha(c)||c=='_'){
		int i=0;
		while( c!=EOF && (c=='_' || isalnum(c)) ){
			Buffer[i++]=c;
			c=getc(fpExport);
		}
		Buffer[i]=0;
		yylval=new CObj(Buffer);
		ungetc(c,fpExport);
		for(i=0;yytname[i];i++)
			if(!strncmp(Buffer,yytname[i]+1,strlen(Buffer)) &&
				*(yytname[i]+1+strlen(Buffer))=='"' ){
				drintf(Buffer);
				return yytoknum[i];
			}
		drintf(Buffer);
		return NOM;
	}
	if(c==39){ 
		int i;
		for(i=0;(c=getc(fpExport))!=39;i++)
			Buffer[i]=c;
		Buffer[i]=0;
		yylval=new CObj(Buffer);
		drintf(Buffer);
		return MASK;
	}
	if(c==EOF)
		return 0;
	return c;
}



void yyerror(char *s){
	printf("Error: %s\n",s);
}

