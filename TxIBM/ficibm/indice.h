

#ifndef __INDICE_H__
#define __INDICE_H__

#include "fichero.h"
#include "campo.h"
#include "defgen.h"

class CVal;

/**
 * Estructura de un archivo de indice:
 *  NC C1 .. CK OFF1 ... OFFM
 *
 * Donde:
 *  NC  : Numero de campos que componen el indice
 *  C1  : Primer campo del indice (ej. RMRC.RM.FHISTOR)
 *  CK  : Ultimo campo del indice.
 *	NRD : Numero de registros distintos
 *  RD# : Conjunto de t_redi
 *  OFF#: Offset en el archivo de datos del registro de posicion # en el indice.
 *
 *  Todos los datos numericos son long
 */

//10 primeras comprobaciones en la cach�. DEBE SER UN NUMERO PAR.
#define LEV_BTREE_CACHE	10
const ulong tamCache=1<<LEV_BTREE_CACHE;
	
class CInd : public CFic	{
	private:
		FILE*	fpi;			//Fichero del indice
		char**	campos;			//Campos del indice (lista)
		tipInt	tipos[MAXNDXFIELDS]; //Tipos de datos.
		ulong	numcam;			//Numero de campos del indice
		ulong	*offord;		//Offsets de registros ordenados por este indice
		uchar**	cache;			//Registros en cach�.
		int		cacheado;		//Hay registros en cach�.
		void	carCam(void);	//Carga lista de campos del fichero de indice
		void	graCam(void);	//Graba lista de campos del fichero de indice
		void	carOor(void);	//Carga los offsets ordenados
		void	graOor(void);	//Graba los offsets ordenados
		void	Indexa(char*n);	//Indexa y graba archivo.
		void	initStaticMembers();
		void	termStaticMembers();
		int		cmpRegMemFil(int numvals,CVal**pVal,t_valor*tv);
		void	loadCache(void);//Cargar algunos registros en la cach�.
	public:
		//Si la definicion es nula, intentaremos cargar el indice de un archivo
		CInd(char *nomfic, ulong tamreg, char *nomind, char *def=(char*)NULL);
		~CInd();
		//Sobrecargas de funciones virtual de CFic.
		ulong	getOff(ulong nr);
		uchar* getStat(CCur* cur);
		bool esInd(void) { return true; }
		long numCamInd(void) { return numcam; }		//Numero de campos del indice
		char*nomCamInd(int i){ return campos[i];}	//Nombre de los campos.
		//Busquedas sobre indice:
		bool busca(CCur* pCur,long PosReg, int numvals=0, va_list marker=NULL);
};

#endif
