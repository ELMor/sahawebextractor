#ifndef __DIMENS_H__
#define __DIMENS_H__

#include "objeto.h"

class CCam;
class CCur;
class CVal;

class CDim : public CObj {
	private:
uint			tipdim;					//Tipo de dimension 1:fija, 2:variable
uint			d1,d2,max;
CCam*			Refer;
CVal*			cval;
	public:

				CDim	(){ 
					d1=d2=max=tipdim=0; 
					cval=(CVal*)NULL;
				};
				CDim	(uint dim1, uint dim2=0){
					d1=dim1;
					d2=dim2;
					max=0;
					tipdim=1;
					cval=(CVal*)NULL;
				};
				~CDim	();
				CDim	(CCam *c, long m );
		void	dump	(char *s=(char*)NULL, FILE *fp=stdout);
		long	getTam	(CCur *p=(CCur*)NULL);
		long	getD1	(void);
		long	getD2	(void)	{return d2;}
		long	getmax	(void)	{return max;}
		long	getTip	(void)	{return tipdim; }
};

#endif
