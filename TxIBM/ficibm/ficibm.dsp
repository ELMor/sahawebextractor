# Microsoft Developer Studio Project File - Name="ficibm" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=ficibm - Win32 Profilling
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ficibm.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ficibm.mak" CFG="ficibm - Win32 Profilling"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ficibm - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "ficibm - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "ficibm - Win32 Profilling" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""$/xlat/ficibm", NEAAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe

!IF  "$(CFG)" == "ficibm - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "ficibm - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /vd0 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "ficibm - Win32 Profilling"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "ficibm__"
# PROP BASE Intermediate_Dir "ficibm__"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "ficibm__"
# PROP Intermediate_Dir "ficibm__"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "ficibm - Win32 Release"
# Name "ficibm - Win32 Debug"
# Name "ficibm - Win32 Profilling"
# Begin Group "Include"

# PROP Default_Filter "h"
# Begin Source File

SOURCE=.\campo.h
# End Source File
# Begin Source File

SOURCE=.\cursor.h
# End Source File
# Begin Source File

SOURCE=.\defgen.h
# End Source File
# Begin Source File

SOURCE=.\dimens.h
# End Source File
# Begin Source File

SOURCE=.\fecha.h
# End Source File
# Begin Source File

SOURCE=.\fecibm.h
# End Source File
# Begin Source File

SOURCE=.\fichero.h
# End Source File
# Begin Source File

SOURCE=.\ficibm.h
# End Source File
# Begin Source File

SOURCE=.\fmanip.h
# End Source File
# Begin Source File

SOURCE=.\indice.h
# End Source File
# Begin Source File

SOURCE=.\objeto.h
# End Source File
# Begin Source File

SOURCE=.\opcion.h
# End Source File
# Begin Source File

SOURCE=.\pack.h
# End Source File
# Begin Source File

SOURCE=.\parse.h
# End Source File
# Begin Source File

SOURCE=.\tablas.h
# End Source File
# Begin Source File

SOURCE=.\tipo.h
# End Source File
# Begin Source File

SOURCE=.\valor.h
# End Source File
# Begin Source File

SOURCE=.\variab.h
# End Source File
# Begin Source File

SOURCE=.\xlat.h
# End Source File
# End Group
# Begin Group "Source"

# PROP Default_Filter "cpp"
# Begin Source File

SOURCE=.\campo.cpp
# End Source File
# Begin Source File

SOURCE=.\cursor.cpp
# End Source File
# Begin Source File

SOURCE=.\dimens.cpp
# End Source File
# Begin Source File

SOURCE=.\fecha.cpp
# End Source File
# Begin Source File

SOURCE=.\fecibm.cpp
# End Source File
# Begin Source File

SOURCE=.\fichero.cpp
# End Source File
# Begin Source File

SOURCE=.\fmanip.cpp
# End Source File
# Begin Source File

SOURCE=.\gramm.cpp
# End Source File
# Begin Source File

SOURCE=.\gramm.y

!IF  "$(CFG)" == "ficibm - Win32 Release"

# Begin Custom Build - Pasando el YACC
InputPath=.\gramm.y

"gramm.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	..\bison --verbose -o gramm.cpp gramm.y

# End Custom Build

!ELSEIF  "$(CFG)" == "ficibm - Win32 Debug"

# Begin Custom Build - Pasando el YACC
InputPath=.\gramm.y

"gramm.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	..\bison  --verbose -o gramm.cpp gramm.y

# End Custom Build

!ELSEIF  "$(CFG)" == "ficibm - Win32 Profilling"

# Begin Custom Build - Pasando el YACC
InputPath=.\gramm.y

"gramm.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	..\bison  --verbose -o gramm.cpp gramm.y

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\indice.cpp
# End Source File
# Begin Source File

SOURCE=.\opcion.cpp
# End Source File
# Begin Source File

SOURCE=.\pack.cpp
# End Source File
# Begin Source File

SOURCE=.\parse.cpp
# End Source File
# Begin Source File

SOURCE=.\tablas.cpp
# End Source File
# Begin Source File

SOURCE=.\tipo.cpp
# End Source File
# Begin Source File

SOURCE=.\valor.cpp
# End Source File
# Begin Source File

SOURCE=.\variab.cpp
# End Source File
# Begin Source File

SOURCE=.\xlat.cpp
# End Source File
# End Group
# End Target
# End Project
