

#include "campo.h"
#include "malloc.h"

CCam**				lisCam=(CCam**)NULL;
ulong				nlisCam=0;
static CCam**		stck=(CCam**)NULL;
ulong				nstck=0;

void CCam::init(void){
	tipo=(CTip*)NULL;
	pyc=false;
	nhijos=0;
	lhijos=(CCam**)NULL;
	while(nivel<=nstck)
		nstck--;
	if( nstck==0 )
		padre=(CCam*)NULL;
	else{
		padre= stck[nstck-1];					//Padre es el top de pila
		padre->addHij(this);					//Al padre a�adir hijo.
	}
	stck=(CCam**)realloc(stck,++nstck*sizeof(CCam*));	//Nos ponemos como top.
	stck[nstck-1]=this;
	lisCam=(CCam**)realloc(lisCam,++nlisCam*sizeof(CCam*));
	lisCam[nlisCam-1]=this;
	ContieneRefers=0;
	staticOffset=-1;
	staticTamano=-1;
}

CCam::CCam(){
	nombre=(CVar*)NULL;
	init();
}

CCam::CCam(CVar* v, uint i){
	nombre=v;
	nivel=i;
	init();
}

CCam::~CCam(){
	ulong i;
	delete nombre;
	delete tipo;
	for(i=0;i<nlisCam;i++)
		if( lisCam[i]==this ){
			memmove(&lisCam[i],&lisCam[i+1],(nlisCam-i-1)*sizeof(CCam*));
			nlisCam--;
		}
}

CCam* 
CCam::find(char *nom,CCam *padre, int lev){
	ulong it;
	if( !padre ){
		if(lev==-1){
			for(it=0;it<nlisCam;it++)
				if(!stricmp(lisCam[it]->getVar()->getNom(),nom))
					return lisCam[it];
			return (CCam*)NULL;
		}else{
			for(it=0;it<nlisCam;it++)
				if(!stricmp(lisCam[it]->getVar()->getNom(),nom) &&
					lisCam[it]->getNiv()==(ulong)lev)
					return lisCam[it];
			return (CCam*)NULL;
		}
	}else{
		for(it=0;it<padre->nhijos;it++)
			if(!stricmp(padre->lhijos[it]->getVar()->getNom(),nom))
				return padre->lhijos[it];
		return (CCam*)NULL;
	}
}

void //Calcular los anchos minimos de las columnas para hacer un dump
CCam::calAnchoMinimo(int Modo){
	ulong it1,apos;
	for(it1=0;it1<nlisCam;it1++){
		if(lisCam[it1]->getPad() == NULL)
			apos=1;
		if(lisCam[it1]->nhijos==0){
			//Calcular el ancho m�nimo de una columna de texto que deba soportar este campo
			switch(lisCam[it1]->getOpc()->getTip()){
				case FecAbs:case FecRel:
					lisCam[it1]->minanch=9;
					break;
				case Binario:case Decimal:
					lisCam[it1]->minanch=12;							
					break;
				case EntConSig:case EntSinSig:case EntSinSig_Binario:
					lisCam[it1]->minanch=lisCam[it1]->getTam()/4+2;		
					break;
				case Tabla:
					lisCam[it1]->minanch=4;								
					break;
				case TexSinCom:
					lisCam[it1]->minanch=lisCam[it1]->getTam()/8+1;		
					if(Modo)
						lisCam[it1]->minanch*=2;
					break;
				case TexJue32:
					lisCam[it1]->minanch=lisCam[it1]->getTam()/5+1;		
					if(Modo)
						lisCam[it1]->minanch*=2;
					break;
				case TexJue40:
					lisCam[it1]->minanch=lisCam[it1]->getTam()/8*3/2+1;	
					if(Modo)
						lisCam[it1]->minanch*=2;
					break;
				default:
					lisCam[it1]->minanch=lisCam[it1]->getTam()/8+1;		
					if(Modo)
						lisCam[it1]->minanch*=2;
					break;

			}
			if(strlen(lisCam[it1]->nombre->getNom())+1 > (unsigned)lisCam[it1]->minanch )
				lisCam[it1]->minanch=strlen(lisCam[it1]->nombre->getNom())+1;
			CCam *pCam;
			for(pCam=lisCam[it1];pCam;pCam=pCam->padre)
				if(pCam->getVar()->getIns()->getD1()>1)
					lisCam[it1]->minanch=lisCam[it1]->minanch+3;
		}
	}
}

void 
CCam::getNomCom(char *s,int full){
	if( full ){
		char aux[256];
		sprintf(s,"%s",nombre->getNom());
		CCam* pad=padre;
		while(pad){
			strcpy(aux,s);
			sprintf(s,"%s.%s",pad->getVar()->getNom(),aux);
			pad=pad->padre;
		}
	}else{
		sprintf(s,"%s",nombre->getNom());
	}
}


void 
CCam::dump(char *s,FILE *fp){
	char aux[200];
	*aux=0;
	if(tipo->getTip()==TDCL)
		if(!s) sprintf(aux,"0 0 0 DCL ");
	else
		if(!s) sprintf(aux,"      ");
	sprintf(aux+strlen(aux),"        "+(8-nivel));
	sprintf(aux+strlen(aux),"%ld ",nivel);
	nombre->dump(aux+strlen(aux));
	  tipo->dump(aux+strlen(aux));
	if(tipo->getTip()!=TDCL)
		opccam->dump(aux+strlen(aux));
	if(pyc)
		sprintf(aux+strlen(aux),";\n");
	else
		sprintf(aux+strlen(aux),",\n");
	if( s ){
		strcpy(s,aux);
	}else{
		fprintf(fp,"%s",aux);
		ulong it;
		for(it=0;it<nhijos;it++)
			lhijos[it]->dump((char*)NULL,fp);
	}
}

long
CCam::OffUntMe( CCam *hijo, CCur *c ){
	long ret=0; ulong i;
	for(i=0; i<nhijos && hijo!=lhijos[i] ;i++)
		if(lhijos[i]!=hijo)
			ret+=lhijos[i]->nombre->getTam(c) * lhijos[i]->getTam(c);
	if( padre )
		ret += padre->OffUntMe( this, c);
	return ret;
}

long
CCam::getOff(CCur *c){
	if(staticOffset>-1) 
		return staticOffset;
	else				
		return padre->OffUntMe( this, c);
}

long
CCam::getTam(CCur *c){
	if( staticTamano>-1)	
		return staticTamano;
	else if( nhijos==0 )	
		return tipo->getTam(c);
	else{
		ulong i,tam=0;
		for(i=0;i<nhijos;i++)
			tam+=	lhijos[i]->nombre->getTam(c) * lhijos[i]->tipo->getTam(c);
		return tam;
	}
}

void
CCam::calStatics(void){
	ulong it1;
	ulong ReferFound=0;
	//Calculo de Offsets
	for(it1=0;it1<nlisCam;it1++){
		if( lisCam[it1]->padre==NULL){
			ReferFound=0;
			lisCam[it1]->staticOffset=0;
		}else if(!ReferFound){
			lisCam[it1]->staticOffset=lisCam[it1]->getOff(NULL);
		}
		if(ReferFound && lisCam[it1]->getNiv()+1<=ReferFound)
			lisCam[it1]->ContieneRefers=1;
		if( lisCam[it1]->EsRefer() ){
			CCam* aux=lisCam[it1]->padre;
			while(aux){
				aux->ContieneRefers=1;
				aux=aux->padre;
			}
			ReferFound=lisCam[it1]->getNiv()+1;
		}
	}
	//Calculo de tama�os:
	for(it1=0;it1<nlisCam;it1++){
		if( lisCam[it1]->ContieneRefers != 1)
			lisCam[it1]->staticTamano=lisCam[it1]->getTam(NULL);
	}
}

