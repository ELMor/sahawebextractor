Attribute VB_Name = "Sockets"
Option Explicit
Public Sub Conexion(sock As Winsock, Host$, Port%, l As Control)
    Dim i As Integer
    l = "Conectando..." + Chr(10)
    sock.RemoteHost = Host: sock.RemotePort = Port
    sock.Connect
    Do
        DoEvents: If i <> sock.state Then Call RegStatus(i, l)
        i = sock.state
    Loop Until sock.state = sckConnected
    Call RegStatus(sock.state, l)
End Sub
Public Function PutGet(txt As String, sck As Winsock, log As Control) As String
    Dim data$, dataTemp$, lon As Long, numbytes&
    Screen.MousePointer = vbArrowHourglass
    If txt <> "" Then Call sck.SendData(txt)
    Do Until lon = numbytes + 8
        DoEvents
        lon = lon + sck.BytesReceived
        If lon > 7 Then
            Call sck.GetData(dataTemp, vbString)
            data = data + dataTemp
            numbytes = CLng(Mid$(data, 5, 4))
        End If
    Loop
    Screen.MousePointer = vbDefault
    PutGet = Left$(data, lon)
End Function
Public Sub Desconexion(sock As Winsock, l As Control)
    Dim i As Integer
    l = "Desconectando..." + Chr(10): sock.Close
    Do
        DoEvents: If i <> sock.state Then Call RegStatus(i, l)
        i = sock.state
    Loop Until sock.state = sckClosed
    Call RegStatus(sock.state, l)
End Sub
Private Sub RegStatus(state As Integer, l As Control)
    l = "Socket "
    Select Case state
        Case sckClosed: l = l & "cerrado"
        Case sckOpen: l = l & "abierto"
        Case sckListening: l = l & "Escuchando"
        Case sckConnectionPending: l = l & "Pendiente de respuesta"
        Case sckConnecting: l = l & "Conectando"
        Case sckConnected: l = l & "Conectado"
        Case sckResolvingHost: l = l & "Resolviendo nombre"
        Case sckHostResolved: l = l & "Nombre resuelto"
        Case sckClosing: l = l & "Cerrando"
        Case sckError: l = l & "ERROR:" & Error
    End Select
End Sub


