#include "cie9.h"
#include "search.h"

#define  Global extern
#define  GlobalIni
#include "globals.h"

//Marcar con '0' los punto de separaci�n de frases
//y retornar el n�mero de frases.
long sepFrases(char *txt){
	long nf=0,len=strlen(txt);
	char *s=txt;

	while( (s=strchr(s,'.'))!=NULL ){
		if( s==txt || s>=txt+len-1 ){
			s++;
			continue;
		}
		if( !isdigit(*(s-1)) &&
			!isdigit(*(s+1)) ){
			*s=0;
			nf++;
		}
		s++;
	}
	return nf;
}


