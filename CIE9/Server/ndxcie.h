
#ifndef __NDXCIE_H__
#define __NDXCIE_H__

#ifdef UNIX
	#include <unistd.h>
#else
	#include <direct.h>
#endif

#define NDATADIR		"data"		//Nombre del directorio que contiene los textos
#define FPALCOM			"palcom.txt"
#define FCIE9			"cie9.txt"

typedef struct _dummy0 {
	char esRef;		//Es una Referencia CIE9? (c�digo)
	char *Def[3];	//Palabras que hay que incluir.
} shortCut;

typedef struct _dummy1 {
	char *nomf;
	char *estf;
} InputFile;

static shortCut nomCol[]={
	{ '0', {"",			"",				""}		},//Aclaracion:	0
	{ '1', {"Hipertension",	"Benigna",	""}		},//			1 
	{ '1', {"",			"",				""}		},//C�digo		2
	{ '0', {"",			"",				""}		},//Diagnostico	3
	{ '1', {"Neoplasia","Maligna",  "Situ"}		},//			4
	{ '1', {"Hipertension","Maligna",	""}		},//			5
	{ '1', {"Neoplasia","No","Especificada"}	},//			6
	{ '1', {"Neoplasia","Maligna","Primaria"}	},//			7
	{ '1', {"Neoplasia","Maligna","Secundaria"},},//			8
	{ '1', {"Neoplasia","Incierta",		""}		},//			9
	{ '1', {"Neoplasia","Benigna",		""}		},//			9
	{ '0', {"",			"",				""}		}
};

#define fldAclaracion	0
#define fldCodigo		2
#define fldDiagnostico	3

static InputFile Files[] = {
	{ "p001.txt","320" },
	{ "p010.txt","320" },
	{ "p020.txt","320" },
	{ "p030.txt","320" },
	{ "p040.txt","320" },
	{ "p050.txt","320" },
	{ "p060.txt","320" },
	{ "p070.txt","320" },
	{ "p080.txt","320" },
	{ "p090.txt","320" },
	{ "p100.txt","320" },
	{ "p110.txt","320" },
	{ "p120.txt","320" },
	{ "p130.txt","320" },
	{ "p140.txt","320" },
	{ "p150.txt","320" },
	{ "p160.txt","320" },
	{ "p170.txt","320" },
	{ "p180.txt","320" },
	{ "p190.txt","320" },
	{ "p200.txt","320" },
	{ "p210.txt","320" },
	{ "p220.txt","320" },
	{ "p230.txt","320" },
	{ "p240.txt","320" },
	{ "p250.txt","320" },
	{ "p260.txt","320" },
	{ "p270.txt","320" },
	{ "p280.txt","320" },
	{ "p290.txt","320" },
	{ "p300.txt","320" },
	{ "p310.txt","320" },
	{ "p320.txt","320" },
	{ "p330.txt","320" },
	{ "p340.txt","320" },
	{ "p350.txt","320" },
	{ "p360.txt","320" },
	{ "p370.txt","320" },
	{ "tab376.txt","35160" },
	{ "p380.txt","320" },
	{ "p390.txt","320" },
	{ "p400.txt","320" },
	{ "p410.txt","320" },
	{ "p420.txt","320" },
	{ "p430.txt","320" },
	{ "p440.txt","320" },
	{ "p450.txt","320" },
	{ "p460.txt","320" },
	{ "p470.txt","320" },
	{ "p480.txt","320" },
	{ "tab490.txt","3784:960" },
	{ "tab500.txt","3784:960" },
	{ "tab510.txt","3784:960" },
	{ "tab520.txt","3784:960" },
	{ "p524.txt","320" },
	{ "p530.txt","320" },
	{ "p540.txt","320" },
	{ "p550.txt","320" },
	{ "p560.txt","320" },
	{ "p570.txt","320" },
	{ "p580.txt","320" },
	{ "p590.txt","320" },
	{ "p600.txt","320" },
	{ "p610.txt","320" },
	{ "p620.txt","320" },
	{ "p630.txt","320" },
	{ "p640.txt","320" },
	{ "p650.txt","320" },
	{ "p660.txt","320" },
	{ "p670.txt","320" },
	{ "p680.txt","320" },
	{ "p690.txt","320" },
	{ "p700.txt","320" },
	{ "p710.txt","320" },
	{ "p720.txt","320" },
	{ "p730.txt","320" },
	{ "p740.txt","320" },
	{ "p750.txt","320" },
	{ "p760.txt","320" },

	{ "","" }
};

#endif