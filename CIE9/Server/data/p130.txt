� s�rico	(M8441/3)	
�� papilar	(M8460/3)	
��� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
��� sitio no especificado	183.0	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	183.0	
� seudomucinoso	(M8470/3)	
�� papilar	(M8471/3)	
��� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
��� sitio no especificado	183.0	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	183.0	
� tipo conducto biliar	155.1 (M8161/3) 	
Cistadenofibroma	(M9013/0)	
� c�lulas claras	(M8313/0)	"v�ase Neoplasia, por sitio, benigna"
� endometrioide	220 (M8381/0) 	
�� maligna	183.0 (M8381/3) 	
�� malignidad dudosa (bordeline)	236.2 (M8381/1) 	
� mucinoso	(M9015/0)	
�� sitio especificado		"v�ase Neoplasia, por sitio, benigno"
�� sitio no especificado	220	
� s�rico	(M9014/0)	
�� sitio especificado		"v�ase Neoplasia, por sitio, benigno"
�� sitio no especificado	220	
� sitio especificado		"v�ase Neoplasia, por sitio, benigno"
� sitio no especificado	220	
Cistadenoma	(M8440/0)	"v�ase adem�s Neoplasia, por sitio,  benigno"
� conducto biliar	211.5 (M8161/0) 	
� endometrioide	(M8380/0)	"v�ase adem�s Neoplasia, por sitio, benigno"
�� malignidad dudosa (bordeline)	(M8380/1)	"v�ase Neoplasia, por sitio, comportamiento incierto"
� maligno	(M8440/3)	"v�ase Neoplasia, por sitio, maligna"
� mucinoso	(M8470/0)	
�� malignidad dudosa (bordeline)	(M8470/1)	
��� sitio especificado		"v�ase Neoplasia, comportamiento incierto"
��� sitio no especificado	236.2	
�� papilar	(M8471/0)	
��� malignidad dudosa (bordeline)	(M8471/1)	
���� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
���� sitio no especificado	236.2	
��� sitio especificado		"v�ase Neoplasia, por sitio, benigna"
��� sitio no especificado	220	
�� sitio especificado		"v�ase Neoplasia, por sitio, benigna"
�� sitio no especificado	220	
� papilar	(M8450/0)	
�� linfomatoso	210.2 (M8561/0) 	
�� malignidad dudosa (bordeline)	(M8450/1)	
��� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
��� sitio no especificado	236.2	
�� mucinoso	(M8471/0)	
��� malignidad dudosa (bordeline)	(M8471/1)	
���� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
���� sitio no especificado	236.2	
��� sitio especificado		"v�ase Neoplasia, por sitio, benigna"
��� sitio no especificado	220	
�� s�rico	(M8460/0)	
��� malignidad dudosa (bordeline)	(M8460/1)	
���� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
���� sitio no especificado	236.2	
��� sitio especificado		"v�ase Neoplasia, por sitio, benigno"
��� sitio no especificado	220	
�� seudomucinoso	(M8471/0)	
��� malignidad dudosa (bordeline)	(M8471/1)	
���� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
���� sitio no especificado	236.2	
��� sitio especificado		"v�ase Neoplasia, por sitio, benigno"
��� sitio no especificado	220	
�� sitio especificado	(M8441/0)	"v�ase Neoplasia, por sitio, benigna"
�� sitio no especificado	220	
� s�rico	(M8441/0)	
�� malignidad dudosa (bordeline)	(M8441/1)	
��� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
��� sitio no especificado	236.2	
�� papilar	(M8460/0)	
��� malignidad dudosa (bordeline)	(M8460/1)	
���� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
���� sitio no especificado	236.2	
��� sitio especificado		"v�ase Neoplasia, por sitio, benigna"
��� sitio no especificado	220	
�� sitio especificado		"v�ase Neoplasia, por sitio, benigna"
�� sitio no especificado	220	
� seudomucinoso	(M8470/0)	
�� malignidad dudosa (bordeline)	(M8470/1)	
��� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
��� sitio no especificado	236.2	
�� papilar	(M8471/0)	
��� malignidad dudosa (bordeline)	(M8471/1)	
���� sitio especificado		"v�ase Neoplasia, por sitio, comportamiento incierto"
���� sitio no especificado	236.2	
��� sitio especificado		"v�ase Neoplasia, por sitio, benigna"
��� sitio no especificado	220	
�� sitio especificado		"v�ase Neoplasia, por sitio, benigna"
�� sitio no especificado	220	
� tiroideo	226	
Cistationinemia	270.4	
Cistationinuria	270.4	
"Cisterna, subaracnoide"	793.0	
Cisticerciasis	123.1	
Cisticercosis (mamaria) (subretinal)	123.1	
Cisticercus	123.1	
"� cellulosae, infestaci�n por"	123.1	
"C�stico, conducto"		v�ase enfermedad espec�fica
Cistinosis (maligna)	270.0	
Cistinuria	270.0	
Cistitis (bacilar) (colibacilar) (difusa) (exudativa) (hemorr�gica) (purulenta) (recurrente) (s�ptica) (supurativa) (ulcerativa)	595.9	
� con		
�� aborto		"v�ase Aborto, por tipo, con infecci�n del tracto urinario"
�� embarazo ect�pico	639.8	v�ase adem�s categor�as 633.0-633.9
�� embarazo molar	639.8	v�ase adem�s categor�as 630-632
�� fibrosis	595.1	
�� leucoplaquia	595.1	
�� malacoplaquia	595.1	
�� metaplasia	595.1	
� actinomic�tica	039.8 [595.4]
� aguda	595.0
�� del tr�gono	595.3
� al�rgica	595.89
� amebiana	006.8 [595.4]
� bilharzial	120.9 [595.4]
� blenorr�gica (aguda)	098.11
�� cr�nica o de duraci�n de 2 meses o m�s 	098.31
� bullosa	595.89
� calculosa	594.1
� cr�nica	595.2
�� del tr�gono	595.3
�� intersticial	595.1
"� cuando complica el embarazo, alumbramiento o puerperio"	646.6
�� que afecta al feto o al reci�n nacido	760.1
� despu�s de	
�� aborto	639.8
�� embarazo ect�pico o molar	639.8
� dift�rica	032.84
� enfisematosa	595.89
� enquistada	595.81
� equinoc�cica	
�� granulosa	122.3 [595.4]
�� multilocular	122.6 [595.4]
� especificada NCOC	595.89
� folicular	595.3
� gangrenosa	595.89
� glandular	595.89
� gonoc�cica (aguda)	098.11
�� cr�nica o de duraci�n de 2 meses o m�s	098.31
� incrustada	595.89
� intersticial	595.1
� irradiaci�n	595.82	
� irritaci�n	595.89	
� maligna	595.89	
� monilial	112.2	
� panmural	595.1	
� poliposa	595.89	
� prost�tica	601.3	
� qu�stica	595.81	
� radiaci�n	595.82	
"� Reiter, de (abacteriana)"	099.3	
� sifil�tica	095.8	
� subaguda	595.2	
� submucosa	595.1	
� tricomoniasis	131.09	
� tr�gonal	595.3	
� tuberculosa	016.1	v�ase adem�s Tuberculosis
� ulcerativa	595.1	
Cistocele (-rectocele)		
� en el embarazo o alumbramiento	654.4	
�� cuando obstruye el parto	660.2	
��� que afecta al feto o al reci�n nacido	763.1	
�� que afecta al feto o al reci�n nacido	763.8	
� femenino (sin prolapso del �tero)	618.0	
�� con prolapso del �tero	618.4	
��� completo	618.3	
��� incompleto	618.2	
� masculino	596.8	
Cistolitiasis	594.1	
Cistoma	(M8440/0)	"v�ase adem�s Neoplasia, por sitio, benigna"
"� endometrial, ovario"	617.1	
� mucinoso	(M8470/0)	
�� sitio especificado		"v�ase adem�s Neoplasia, por sitio, benigna"
�� sitio no especificado	220	
� s�rico	(M8441/0)	
�� sitio especificado		"v�ase Neoplasia, por sitio, benigno"
�� sitio no especificado	220	
� simple (ovario)	620.2	
Cistopielitis	590.80	v�ase adem�s Pielitis
Cistoplej�a	596.5	
Cistoptosis	596.8	
Cistorragia	596.8	
Cistosarcoma filoides	238.3 (M9020/1)	
� benigno	217 (M9020/0)	
� maligno	(M9020/39 	"v�ase Neoplasia, mama, maligna"
"Cistostom�a, estado de "	V44.5	
� con complicaci�n	997.5	
Cistouretritis	597.89	v�ase adem�s Uretritis
Cistouretrocele		v�ase adem�s Cistocele
� femenino (sin prolapso del �tero)	618.0	
�� con colapso del �tero	618.4	
��� completo	618.3	
��� incompleto	618.2	
� masculino	596.8	
"Citomeg�lica, enfermedad de inclusi�n"	078.5	
� cong�nita	771.1	
"Citomicosis, reticuloendotelial"	115.00	"v�ase adem�s Histoplasmosis, americana"
Citrulinemia	270.6	
Citrulinuria	270.6	
"Ciuffini-Pancoast, tumor de (carcinoma, �pice pulmonar)"	162.3 (M8010/3) 	
"Civatte, enfermedad o poiquiloderma de"	709.0	
"Clark, par�lisis de"	343.9	
"Clarke-Hadfield, s�ndrome de (infantilismo pancre�tico)"	577.8	
Clasificadores de lana	022.1	
"Clasificadores de trapos, enfermedad de"	022.1	
Clastotrix	704.2	
"Claude, s�ndrome de "	352.6	
"Claude-Bernard-Horner, s�ndrome de"	337.9	"v�ase adem�s Neuropat�a, perif�rica, auton�mica"
"Claudicaci�n, intermitente"	443.9	
� cerebral (arteria)	435.9	"v�ase adem�s Isquemia, cerebral, transitoria"
� espinal	435.1	
� m�dula espinal (arterioscler�tica)	435.1	
�� sifil�tica	094.89	
� venosa (axilar)	453.8	
Claudicatio venosa intermittens	453.8	
Claustrofobia	300.29	
"Claveteado, h�gado"		"v�ase Cirrosis, portal"
"Clavija, dientes en forma de"	520.2	
Clavo (infectado)	700	
"Cleidocraneal, disostosis"	755.59	
"Cleidotom�a, fetal"	763.8	
Cleptoman�a	312.32	
"Cl�rambault, s�ndrome de"	297.8	
� erotoman�a	302.89	
"Cl�rigos, mal de garganta de los"	784.49	
"Clifford, s�ndrome de (posmaturidad)"	766.2	
"Climat�rico, climaterio"	627.2	v�ase adem�s Menopausia
� artritis NCOC	716.3	"v�ase adem�s Artritis, climat�rica"
� depresi�n 	296.2	"v�ase adem�s Psicosis, afectiva"
� enfermedad	627.2	
�� episodio �nico	296.2	
�� episodio recurrente	296.3	
� estado paranoide	297.2	
� femenino (s�ntomas)	627.2	
� masculino (s�ntomas) (s�ndrome)	608.89	
� melancol�a	296.2	"v�ase adem�s Psicosis, afectiva"
�� episodio recurrente	296.3	
�� episodio �nico	296.2	
� parafrenia	297.2	
� poliartritis NCOC	716.39	
�� masculina	608.89	
� s�ntomas (femeninos)	627.2	
Clinodactilia	755.59	
Cl�toris		v�ase enfermedad espec�fica
"Cloaca, persistente"	751.5	
Cloasma	709.0	
� caqu�ctico	709.0	
"� embarazadas, de las"	646.8	
� idiop�tico	709.0	
� p�rpado	374.52	
�� cong�nita	757.33	
�� hipertiroidea	242.0	
� piel	709.0	
� sintom�tico	709.0	
"Clonorchis, infecci�n del h�gado por"	121.1	
Clonorquiasis	121.1	
Clonus	781.0	
Cloroma	205.3 (M9930/3)	
Clorosis	280.9	
� egipcia	126.9	v�ase adem�s Anquilostomiasis
"� mineros, de los"	126.9 	v�ase adem�s Anquilostomiasis
"Clor�tica, anemia"	280.9	
Clorquiosis	121.1	
"Clouston, displasia ectod�rmica (hidr�tica)"	757.31	
"Clutton, articulaciones de "	090.5	
Coagulaci�n intravascular (difusa) (diseminada)	286.6	v�ase adem�s Fibrin�lisis
� reci�n nacido	776.2	
"Coagulaci�n, defecto de NCOC"	286.9	"v�ase adem�s Defecto, coagulaci�n"
Co�gulo (sangre)		
� arteria (obstrucci�n) (oclusi�n)	444.9	v�ase adem�s Embolia
� cerebro (extradural o intradural)	434.0	"v�ase adem�s Trombosis, cerebro"
�� efectos tard�os		v�ase categor�a 438
� circulaci�n	444.9	
� coraz�n	410.9	"v�ase adem�s Infarto, miocardio"
� vejiga	596.7	
� vena	453.9	v�ase adem�s Trombosis
Coagulopat�a	286.9	"v�ase adem�s Defecto, coagulaci�n"
� de consumo	286.6	
� intravascular (diseminada) NCOC	286.6	
� reci�n nacido	776.2	
Coalescencia		
� calc�nea	755.67	
� calcaneoscafoide	755.67	
� tarsiana	755.67	
Coartaci�n		
� aorta (posductal) (preductal)	747.10	
� arteria pulmonar	747.3	
"Coats, enfermedad de"	362.12	
Cocainismo	304.2	v�ase adem�s Dependencia
Coccidinia	724.79	
"Coccidioide, granuloma"	114.3	
Coccidioidomicosis	114.9	
� con neumon�a	114.0	
� cut�nea (primaria)	114.1	
� diseminada	114.3	
� extrapulmonar (primaria)	114.1	
� meninges	114.2	
� primaria (pulmonar)	114.0	
� pr�stata	114.3	
� pulm�n	114.0	
� pulmonar	114.0	
� sitio especificado NCOC	114.3	
Coccidioidosis	114.9	
� meninges	114.2	
� pulm�n	114.0	
Coccidiosis (colitis) (diarrea) (disenter�a)	007.2	
Coccigodinia	724.79	
Cocciuria	599.0	
C�ccix		v�ase enfermedad espec�fica
"Coches, mareo de los"	994.6	
"Cochinchina, de"		
� diarrea	579.1	
�� anguiluliasis	127.2	
� �lcera	085.1	
"Cock, tumor de"	706.2	
"Cockayne, enfermedad o s�ndrome de (microcefalia y enanismo)"	759.8	
"Cockayne-Weber, s�ndrome de (epidermolisis bullosa)"	757.39	
Coco en orina	599.0	
"Codman, tumor de (condroblastoma benigno)"	(M9230/0)	"v�ase Neoplasia, hueso, benigno"
Codo		v�ase enfermedad espec�fica
Codo de los estudiantes	727.2	
Coeficiente de inteligencia (CI)		
� 20-34	318.1	
� 35-49	318.0	
� 50-70	317	
� menos de 20	318.2	
Coenurosis	123.8	
"Cogan, s�ndrome de"	370.52	
� apraxia oculomotora cong�nita	379.51	
� queratitis intersticial no sifil�tica	370.52	
Coiloniquia	703.8	
� cong�nita	757.5	
"Coito, doloroso (femenino)"	625.0	
� masculino	608.89	
� psic�geno	302.76	
"Col�geno, enfermedad de NCOC"	710.9	
� no vascular	710.9	
� vascular (al�rgica)	446.2	"v�ase adem�s Angi�tis, hipersensibilidad"
Colagenosis	710.9	"v�ase adem�s Col�geno, enfermedad de"
� cardiovascular	425.4	
� mediast�nica	519.3	
Colangiectasia	575.8	"v�ase adem�s Enfermedad, ves�cula biliar"
Colangiocarcinoma	(M8160/3)	
� h�gado	155.1	
� sitio especificado NCOC		"v�ase Neoplasia, por sitio, maligna"
� sitio no especificado	155.1	
"� y carcinoma hepatocelular, combinado"	155.0 (M8180/3)	
Colangiohepatitis	575.8	
� debida a infestaci�n por trematodos	121.1	
Colangiohepatoma	155.0 (M8180/3)	
Colangiolitis (aguda) (cr�nica) (extrahep�tica) (gangrenosa)	576.1	
� intrahep�tica	575.8	
� paratifoidea	002.9	"v�ase adem�s Fiebre, paratifoidea"
� tifoidea	002.0	
Colangioma	211.5 (M8160/0)	
� maligno		v�ase Colangiocarcinoma
Colangitis (aguda) (ascendente) (catarral) (cr�nica) (infecciosa) (maligna) (primaria) (recurrente) (esclerosante) (secundaria) (estenosante) (supurativa)	576.1	
� destructiva no supurativa (cr�nica)	571.6	
Colapso	780.2	
� calor	992.1	
� canal auditivo externo	380.50	
�� secundaria a		
��� cirug�a	380.52	
��� inflamaci�n	380.53	
��� trauma	380.51	
� cardiorrenal	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal"
� cardiorrespiratorio	785.51	
�� feto o reci�n nacido	779.8	
� cardiovascular	785.51	"v�ase adem�s Enfermedad, coraz�n"
�� feto o reci�n nacido	779.8	
� circulatorio (perif�rico)	785.59	
�� con		
��� aborto		"v�ase Aborto, por tipo, con choque"
��� embarazo ect�pico	639.5	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.5	v�ase adem�s categor�as 630-632
�� despu�s de 		
��� aborto	639.5	
��� embarazo ect�pico o molar	639.5	
�� durante o despu�s de parto y alumbramiento	669.1	
�� feto o reci�n nacido	779.8	
� coraz�n		"v�ase Enfermedad, coraz�n"
� durante o despu�s de parto o alumbramiento	669.1	
��� feto o reci�n nacido	779.8	
� general	780.2	
� hist�rico	300.11	
"� laberinto, membranoso (cong�nito)"	744.05	
� miocardiaco		"v�ase Enfermedad, coraz�n"
� nariz	738.0	
� nervioso	300.9	"v�ase adem�s Trastorno, mental, no psic�tico"
� neurocirculatorio	306.2	
� postoperatorio (cardiovascular)	998.0	
� pulm�n (masivo)	518.0	v�ase adem�s Atelectasia
"�� presi�n, durante el parto"	668.0	
� pulmonar	518.0	v�ase adem�s Atelectasia
�� feto o reci�n nacido	770.5	
��� parcial	770.5	
��� primario	770.4	
� suprarrenal	255.8	
� t�rax	512.8	
� tr�quea	519.1	
� valvular		v�ase Endocarditis
� vascular (perif�rico)	785.59	
�� con		
��� aborto		"v�ase Aborto, por tipo, con choque"
��� embarazo ect�pico	639.5	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.5	v�ase adem�s categor�as 630-632
�� cerebral	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
�� despu�s de		
��� aborto	639.5	
��� embarazo ect�pico o molar	639.5	
�� durante o despu�s del parto o alumbramiento	669.1	
�� feto o reci�n nacido	779.8	
� vasomotor	785.59	
� v�rtebra	733.1	
Colapso renal	586	
� con		
�� aborto		"v�ase Aborto, por tipo, con fallo renal"
�� embarazo ect�pico	639.3	v�ase adem�s categor�as 633.0-633.9
�� embarazo molar	639.3	v�ase adem�s categor�as 630-632
� cuando complica		
�� aborto	639.3	
�� embarazo ect�pico o molar	639.3	
� despu�s del trabajo y parto	669.3	
Colateral		v�ase enfermedad espec�fica
� circulaci�n (venosa)	459.89	
� dilataci�n (venas)	459.89	
Colecistitis (cr�nica) 	575.1	
� con		
"�� c�lculos, piedras en"		
��� conducto biliar (com�n) (hep�tico)	574.4	
��� ves�cula biliar	574.1	
�� coledocolitiasis	574.4	
�� colelitiasis	574.1	
� aguda	575.0	
�� con		
"��� c�lculos, piedras en"		
���� conducto biliar (com�n) (hep�tico)	574.3	
���� ves�cula biliar	574.0	
��� coledocolitiasis	574.3	
��� colelitiasis	574.0	
� enfisematosa (aguda)	575.0	"v�ase adem�s Colecistitis, aguda"
� gangrenosa	575.0	"v�ase adem�s Colecistitis, aguda"
"� paratifoidea, acutal"	002.9	"v�ase adem�s Fiebre, paratifoidea"
� supurativa	575.0	"v�ase adem�s Colecistitis, aguda"
� tifoidea	002.0	
Colecistitis	595.9	v�ase adem�s Cistitis
Colecistodocolitiasis		v�ase Coledocolitiasis
Coledocolitiasis 	574.5	
� con colecistitis (cr�nica)	574.4	
�� aguda	574.3	
Coledocolito		v�ase Coledocolitiasis
Coledocitis (supurativa)	576.1	
Colelitiasis (impactada) (m�ltiple) 	574.2	
� con colecistitis (cr�nica)	574.1	
�� aguda	574.0	
Colemia	782.4	v�ase adem�s Ictericia
� familiar	277.4	
"� Gilbert, de (no hemol�tica familiar)"	277.4	
"Col�mico, c�lculo biliar"		v�ase Colelitiasis
"Coleperitoneo, coleperitonitis"	567.8	"v�ase adem�s Enfermedad, ves�cula biliar"
C�lera (�lgido) (asf�ctico) (asi�tico) (epid�mico) (espasm�dico) (grave) (indio) (maligno) (morbo) (pestilencial)	001.9	
� antimonial	985.4	
� cl�sica	001.0	
� contacto	V01.0	
� debida a		
�� Vibrio		
"��� cholerae (serotipos Inaba, Ogawa, Hikojima)"	001.0	
���� El Tor	001.1	
� El Tor	001.1	
� exposici�n a 	V01.0	
� portador (presunto) de	V02.0	
"� vacunaci�n, profil�ctica (contra)"	V03.0	
Colerina	001.9	v�ase adem�s C�lera
Colestasis	576.8	
Colesteatoma (o�do)	385.30	
� �tico (primario)	385.31	
� cavidad mastoidea	385.30	
� cavidad posmastoidectom�a (recurrente)	383.22
� difuso	385.35
� marginal (o�do medio)	385.32
�� con implicaci�n de la cavidad mastoidea	385.33
�� secundario (con implicaci�n del o�do medio)	385.33
� o�do externo (canal)	380.21
� o�do medio (secundario)	385.32
�� con implicaci�n de la cavidad mastoidea	385.33
� primario	385.31
"� recurrente, cavidad posmastoidectom�a"	383.32
� secundario (o�do medio)	385.32
�� con implicaci�n de la cavidad mastoidea	385.33
� primario	385.31
"� recurrente, cavidad posmastoidectom�a"	383.32
� secundario (o�do medio)	385.32
�� con implicaci�n de la cavidad mastoidea	385.33
Colesteatosis (o�do medio)	385.30	v�ase adem�s Colesteatoma
� difusa	385.35	
Colesteremia	272.0	
Colesterina		
� en humor v�treo	379.22	
"� granuloma, o�do medio"	385.82	
Colesterol		
� dep�sito		
�� retina	362.82	
�� v�treo	379.22	
� imbibici�n de la ves�cula biliar	575.6	"v�ase adem�s Enfermedad, ves�cula biliar"
Colesterolemia	272.0	
� esencial	272.0	
� familiar	272.0	
� hereditaria	272.0	
"Colesterosis, colesterolosis (ves�cula biliar)"	575.6	
� con		
�� colecistitis		v�ase Colecistitis
�� colelitiasis		v�ase Colelitiasis
� o�do medio	385.30	v�ase adem�s Colesteatoma
"Colgajo, h�gado"	572.8	
"Colgante o p�ndulo, pie"	736.79	
Colibacilosis	041.4	
� generalizada	038.42	
Colibaciluria	599.0	
C�lico (recurrente)	789.0	
� abdomen	789.0	
�� psic�geno	307.89	
� ap�ndice	543.9	
� apendicular	543.9	
� biliar		v�ase Colelitiasis
� bilioso		v�ase Colelitiasis
� conducto biliar		v�ase Coledocolitiasis
� conducto com�n		v�ase Coledocolitiasis
� Devonshire NCOC	984.9	
�� tipo especificado de plomo		"v�ase Tabla de drogas, f�rmacos y productos qu�micos"
� espasm�dico	789.0	
� flatulento	787.3	
� g�strico	536.8	
� hep�tico (conducto)		v�ase Coledocolitiasis
� h�gado (conducto)		v�ase Coledocolitiasis
� hist�rico	300.11	
� infantil	789.0	
� intestinal	789.0	
� mucoso	564.1	
�� psic�geno	316 [564.1]	
� nefr�tico	788.0	
� p�ncreas	577.8	
� par�sitos intestinales NCOC	128.9	
"� pintores, de los NCOC"	984.9	
� plomo NCOC	984.9	
�� tipo de plomo especificado		"v�ase Tabla de drogas, f�rmacos y productos qu�micos"
� psic�geno	306.4	
� renal	788.0	
� ri��n	788.0	
� saturnino NCOC	984.9	
�� tipo especificado de plomo		"v�ase Tabla de drogas, f�rmacos y productos qu�micos"
� ur�ter	788.0	
� uretral	599.8	
�� debido a c�lculo	594.2	
� �tero	625.8	
�� menstrual	625.3	
� vermicular	543.9	
� ves�cula biliar o c�lculo biliar		v�ase Colelitiasis
� virus	460	
Coliculitis uretral	597.89	v�ase adem�s Uretritis
Colitis (aguda) (catarral) (c�stica superficial) (cruposa) (exudativa) (flemonosa) (hemorr�gica) (no infecciosa) (presuntamente no infecciosa)	558.9	
� adaptativa	564.1	
� al�rgica	558.9	
"� am�bica, am�bica"	006.9	v�ase adem�s Amebiasis
�� no disent�rica	006.2	
� �ntrax	022.2	
� bacilar	004.9	"v�ase adem�s Infecci�n, Shigella"
� balant�dica	007.0	
� coccidia	007.2	
� cr�nica	558.9	
�� ulcerativa	556	
� debida a radiaci�n	558.1	
� diet�tica	558.9	
� esp�stica	564.1	
�� psic�gena	316 [564.1]	
� estafilococo	008.41	
�� alimento	005.0	
� funcional	558.9	
� gangrenosa	009.0	
� giardi�sica	007.1	
� granulomatosa	555.1	
� grave	556	
� infecciosa	009.0	"v�ase adem�s Enteritis, debida a, organismo espec�fico"
�� presuntamente	009.1	
� isqu�mica	557.9	
�� aguda	557.0	
�� cr�nica	557.1	
�� debida a insuficiencia de la arteria mesent�rica	557.1	
� membranosa	564.1	
�� psic�gena	316 [564.1]	
� mucosa	564.1	
�� psic�gena	316 [564.1]	
� necr�tica	009.0	
� poliposa	556	
� protozoaria NCOC	007.9	
� regional	555.1	
� segmentaria	555.1	
� s�ptica	009.0	"v�ase adem�s Enteritis, debida a, organismo espec�fico"
� seudomembranosa	008.49	
� seudomucinosa	564.1	
� t�xica	558.2	
� transmural	555.1	
� tricomoni�sica	007.3	
� tromboulcerativa	557.0	
� tuberculosa (ulcerativa)	014.8	
� ulcerativa (cr�nica) (idiop�tica) (no espec�fica)	556	
�� fulminante	557.0
�� psic�gena	316 [556]
Collar espa�ol	605
"Colles, fractura de (cerrada) (inversa) (separaci�n)"	813.41
� abierta	813.51
"Collet, s�ndrome de"	352.6
"Collet-Sicard, s�ndrome de"	352.6
Coloboma NCOC	743.49
� coroides	743.59
� cristalino	743.36
� disco �ptico (cong�nito)	743.57
�� adquirido	377.23
� escler�tica	743.47
� fondo	743.52
� iris	743.46
� p�rpados	743.62
� retina	743.56
Colocaci�n (de)	
� anteojos o gafas	V53.1
"� anticonceptivo intrauterino, dispositivo"	V25.1
� aparato o dispositivo intestinal NCOC	V53.5
� artificial	
�� brazo (completo) (parcial)	V52.0
�� mama	V52.4
�� ojo(s)	V52.2
�� pierna(s) (completo) (parcial)	V52.1
� aud�fono	V53.2
"� cardiaco, marcapasos"	V53.3
"� cerebro, marcapasos"	V53.0
� cintur�n de colostom�a	V53.5
"� cistostom�a, dispositivo de"	V53.6
� dentadura postiza	V52.3
� diafragma (anticonceptivo)	V25.02
� dispositivo NCOC	V53.9
�� abdominal	V53.5
�� anticonceptivo intrauterino	V25.1
�� ortod�ntico	V53.4
�� ort�ptico	V53.1
�� prot�sico	V52.9
��� dental	V52.3
��� mama	V52.4
��� ocular	V52.2
��� tipo especificado NCOC	V52.8
�� sentidos	V53.0
�� sistema nervioso	V53.0
�� sustituci�n	
��� auditiva	V53.0
��� sistema nervioso	V53.0
��� visual	V53.0
�� urinario	V53.6
� gafas o anteojos (para leer)	V53.1
"� ileostom�a, dispositivo"	V53.5
� lentes de contacto	V53.1
� marcapasos	
�� cardiaco 	V53.3
�� cerebral neural	V53.0
�� neural (cerebral) (nervio perif�rico) (m�dula espinal)	V53.0
"� m�dula espinal, marcapasos"	V53.0
"� nervio perif�rico, marcapasos"	V53.0
"� ortod�ntico, dispositivo"	V53.4
� ortop�dico (dispositivo)	V53.7
�� aparato	V53.7
�� cors�	V53.7
�� escayola	V53.7
�� zapatos	V53.7	
� pr�tesis	V52.9	
�� brazo (completo) (parcial)	V52.0	
�� dental	V52.3	
�� mama	V52.4	
�� ocular	V52.2	
�� pierna (completa) (parcial)	V52.1	
�� tipo especificado NCOC	V52.8	
� silla de ruedas	V53.8	
"Coloc�lica, f�stula"	575.5	"v�ase adem�s F�stula, ves�cula biliar"
"Colodi�n, beb� (ictiosis cong�nita)"	757.1	
Coloenteritis		v�ase Enteritis
"Coloideo, milio"	709.3	
"Colomb�filos, enfermedad o pulm�n de"	495.2	
Colon		v�ase enfermedad espec�fica
Coloptosis	569.89	
Color		
� ambliop�a NCOC	368.59	
�� adquirida	368.55	
� ceguera NCOC (cong�nita)	368.59	
�� adquirida	368.55	
Color rojo		
� conjuntiva	379.93	
� nariz	478.1	
� ojo	379.93	
Colostom�a		
� atenci�n de	V55.3	
� colocaci�n o ajuste	V53.5	
� estado	V44.3	
� mal funcionamiento	569.6	
Colpitis 	616.10	v�ase adem�s Vaginitis
Colpocele	618.6	
Colpocistitis	616.10	v�ase adem�s Vaginitis
Colporrexia	665.4	
Colpospasmo	625.1	
"Columna, espinal, vertebral"		v�ase enfermedad espec�fica
Coluria	791.4	
Coma	780.0	
� apopl�tico	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� diab�tica (con quetoacidosis)	250.3	
�� hiperosmolar	250.2	
� ecl�mptico	780.3	v�ase adem�s Eclampsia
� epil�ptico	345.3	
� hep�tico	572.2	
� h�gado	572.2	
� hiperglic�mico	250.2	
� hiperosmolar (diab�tico) (no quet�tico)	250.2	
� hipoglic�mico	251.0	
�� diab�tico	250.3	
� insulina (hiperinsulinismo)	251.0	
"� Kussmaul, de (diab�tico)"	250.3	
� prediab�tico	250.2	
� reci�n nacido	779.2	
� ur�mico		v�ase Uremia
"Combate, fatiga de"	308.9	"v�ase adem�s Reacci�n, tensi�n, aguda"
Combinado		v�ase enfermedad espec�fica
Comedocarcinoma	(M8501/3)	"v�ase adem�s Neoplasia, mama, maligna"
� no infiltrante	(M8501/2)	
�� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
�� sitio no especificado	233.0	
Comedomastitis	610.4	
"Comed�n, comedones"	706.1	
� lanugo	757.4	
Compensaci�n		
� fallo		"v�ase Fallo, cardiaco, congestivo"
"� neurosis, psiconeurosis"	300.11	
� rota		"v�ase Fallo, cardiaco, congestivo"
Complejo (de)		
� cardiorrenal	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal"
� castraci�n	300.9	
� Costen	524.60	
� Eisenmenger (defecto de tabique ventricular)	745.4	
� hipersexual	302.89	
� homosexual	302.0	
� inferioridad	301.9	
"� primario, tuberculoso"	010.0	v�ase adem�s Tuberculosis
� proceso dislocado		
�� espina		"v�ase Dislocaci�n, v�rtebra"
"� Taussig-Bing (transposici�n, aorta y arteria pulmonar predominante)"	745.11	
Completo		v�ase Enfermedad espec�fica
Complicaci�n(es)		
� aborto NCOC		v�ase categor�as 634-639
� accidente terap�utico NCOC	999.9	
�� tratamiento quir�rgico	998.9	
� anastomosis (y desviaci�n) NCOC		"v�ase Complicaci�n(es), debida a (presencia de) cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"
�� intestinal (interna) NCOC	997.4	
��� con implicaci�n del tracto urinario	997.5	
�� mec�nicas		"v�ase Complicaciones, mec�nicas, injerto"
�� tracto urinario (con implicaci�n del tracto intestinal)	997.5	
"� anestesia, anest�sico NCOC"	995.2	"v�ase adem�s Anestesia, complicaci�n"
�� en parto o trabajo de parto	668.9	
��� cardiaca	668.1	
��� pulmonar	668.0	
��� que afecta al feto o al reci�n nacido	763.5	
��� sistema nervioso central	668.2	
��� tipo especificado NCOC	668.8	
"� anticonceptivo, intrauterino, dispositivo NCOC"	996.76
�� infecci�n	996.65
�� inflamaci�n	996.65
�� mec�nica	996.32
� aortocoronaria (desviaci�n) (injerto)	996.03
� apertura artificial	
�� cecostom�a	569.6
�� cistostom�a	997.5
�� colostom�a	569.6
�� enterostom�a	569.6
�� gastrostom�a	997.4
�� ileostom�a	569.6
�� nefrostom�a	997.5
�� traqueostom�a	519.0
�� ureterostom�a	997.5
�� uretrostom�a	997.5
�� yeyunostom�a	569.6	
� artroplastia	996.4	
� cardiaca	429.9	"v�ase adem�s Enfermedad, coraz�n"
"�� dispositivo, implante o injerto NCOC"	996.72	
��� efecto a largo plazo	429.4	
��� infecci�n o inflamaci�n	996.61	
��� mec�nica	996.00	"v�ase adem�s Complicaciones, mec�nicas, por tipo"
��� v�lvula prot�sica	996.71	
���� infecci�n o inflamaci�n	996.61	
�� posoperatoria NCOC	997.1	
��� efecto a largo plazo	429.4	
� cardiorrenal	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal"
� cat�ter uretral NCOC	996.76	
�� infecci�n o inflamaci�n	996.64	
�� mec�nica	996.31	
"� cateterismo NCOC, dispositivo de"		"v�ase Complicaci�n(es), debida a (presencia de) cualquier dispositivo, implante o injerto clasificado bajo 996.0-996-5 NCOC"
�� mec�nica		"v�ase Complicaciones, mec�nicas, cat�ter"
� cecostom�a	569.6	
� circulaci�n extracorp�rea NCOC	999.9	
� cistostom�a	997.5	
� colostom�a (enterostom�a)	569.6	
"� conducto biliar (prot�sico), implante de NCOC"	996.79	
�� infecci�n o inflamaci�n	996.69	
�� mec�nica	996.59	
� coraz�n	996.83 	"v�ase Enfermedad, coraz�n, trasplante (causa inmune o no inmune)"
� cord�n (umbilical)		"v�ase Complicaciones, umbilical, cord�n"
"� crecimiento �seo, estimulador NCOC"	996.78	
�� infecci�n o inflamaci�n	996.67	
"� cristalino, implante de NCOC"	996.79	
�� infecci�n o inflamaci�n	996.69	
�� mec�nica	996.53	
� cuidado m�dico NCOC	999.9	
�� cardiaco NCOC	997.1	
�� gastrointestinal NCOC	997.4	
�� respiratorio NCOC	997.3	
�� sistema nervioso NCOC	997.0	
�� urinario NCOC	997.5	
�� vascular perif�rico NCOC	997.2	
"� debida(s) (presencia de) cualquier dispositivo, implante o injerto clasificado bajo 99�.0-99�.5)"	996.7	
�� con infecci�n o inflamaci�n		"v�ase Complicaci�n(es), debida a (presencia de) cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"
�� arterial NCOC	996.74	
��� coronaria NCOC	996.03	
���� oclusi�n de	996.03	
���� tipo especificado NCOC	996.72	
���� tromb�tica	996.72	
��� di�lisis renal	996.73	
"�� arteriovenosa, f�stula o desviaci�n NCOC"	996.74	
�� bomba de infusi�n	996.74	
�� cardiaca NCOC	996.72
��� desfibrilador	996.72
��� marcapasos	996.72
��� v�lvula prot�sica	996.71
�� cat�ter NCOC	996.79
��� derivaci�n ventricular	996.75
��� espinal	996.75
"��� urinario, permanente "	996.76
��� vascular NCOC	996.74
���� di�lisis renal	996.73
"�� cat�ter urinario, permanente"	996.76
"�� coraz�n, v�lvula prot�sica NCOC"	996.71
"�� crecimiento �seo, estimulador"	996.78
�� derivaci�n coronaria (arteria) (injerto) NCOC	996.03
��� tromb�tica	996.72
�� di�lisis renal	996.73
�� electrodos		
��� cerebro	996.75	
��� coraz�n	996.72	
�� gastrointestinal NCOC	996.79	
�� genitourinario NCOC	996.76	
�� globo orbital NCOC	996.79	
�� interna		
��� articulaci�n prot�sica	996.77	
��� ortop�dica NCOC	996.78	
��� tipo especificado NCOC	996.79	
"�� intrauterino, dispositivo anticonceptivo NCOC"	996.76	
�� mama NCOC	996.79	
�� mec�nica		"v�ase Complicaciones, mec�nicas"
"�� ocular, cristalino NCOC"	996.79	
�� ortop�dica NCOC	996.78	
"��� articulaci�n, interna"	996.77	
"�� pr�tesis articular, interna NCOC"	996.77	
�� sistema nervioso NCOC	996.75	
�� tipo especificado NCOC	996.79	
�� vascular NCOC	996.74	
"�� ventricular, derivaci�n"	996.75	
� derivaci�n coronaria (arteria) (injerto) NCOC	996.03	
�� infecci�n o inflamaci�n	996.61	
�� mec�nica	996.03	
�� tipo especificado NCOC	996.72	
�� tromb�tico	996.72	
� derivaci�n NCOC		"v�ase adem�s Complicaci�n(es), debida a (presencia de) cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"
�� mec�nica		"v�ase Complicaciones, mec�nicas, derivaci�n"
� desviaci�n		"v�ase adem�s Complicaciones, anastomosis"
�� aortocoronaria	996.03	
�� arteria carot�dea	996.1	
"� desviaci�n de arteria carot�dea, injerto"	996.1	
� di�lisis (hemodi�lisis) (peritoneal) (renal) NCOC	999.9	
�� cat�ter NCOC		"v�ase Complicaci�n(es), debida a (presencia de) cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"
��� infecci�n o inflamaci�n	996.62	
��� mec�nica	996.1	
� durante di�lisis NCOC	999.9	
� embarazo ect�pico o molar NCOC	639.9	
� embarazo NCOC	646.9	
�� que afecta al feto o al reci�n nacido	761.9	
� enterostom�a	569.6	
"� estoma, externo"		
�� tracto gastrointestinal NCOC	997.4	
�� tracto urinario	997.5	
"� externa (fijaci�n), dispositivo con componentes internos NCOC"	996.78	
�� infecci�n o inflamaci�n	996.67	
�� mec�nica	996.4	
� fototerapia	990	
"� gastrointestinal, posoperatoria NCOC"	997.4	"v�ase adem�s Complicaciones, procedimientos quir�rgicos"
� gastrostom�a	997.4	
"� genitourinario, dispositivo, implante o injerto NCOC"	996.76	
�� infecci�n o inflamaci�n	996.65	
"��� cateterismo urinario, permanente"	996.64	
�� mec�nica	996.30	"v�ase adem�s Complicaciones, mec�nicas, por tipo"
��� especificada NCOC	996.39	
� hemorragia (intraoperatoria) (posoperatoria)	998.1	
� herida de c�sarea	674.3	
� implante NCOC		"v�ase adem�s Complicaci�n(es), debida a (presencia de) cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"
�� mec�nica		"v�ase Complicaciones, mec�nicas, implante"
� infecci�n e inflamaci�n		
"�� debida a (presencia de) cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"	996.60	
"��� arteria coronaria, derivaci�n"	996.61	
��� arterial NCOC	996.62	
���� coronaria	996.61	
���� di�lisis renal	996.62
"��� arteriovenosa, f�stula o desviaci�n"	996.62
��� bomba de infusi�n	996.62
��� cardiaca	996.61
��� cateterismo NCOC	996.69
���� derivaci�n ventricular	996.63
���� espinal	996.63
"���� urinario, permanente"	996.64
���� vascular NCOC	996.62
"��� crecimiento �seo, estimulador"	996.67
��� cristalino	996.69
��� derivaci�n ventricular	996.63
��� electrodos	
���� cerebro	996.63
���� coraz�n	996.61
��� gastrointestinal NCOC	996.69
��� genitourinario NCOC	996.65	
���� cat�ter urinario permanente	996.64	
��� globo orbital (implante)	996.69	
"��� intrauterino, dispositivo anticonceptivo"	996.65	
��� mama	996.69	
��� ortop�dica NCOC	996.67	
"���� articulaci�n, interna"	996.66	
"��� pr�tesis articular, interna"	996.66	
��� tipo especificado NCOC	996.69	
"��� urinario, cateterismo, permanente"	996.64	
��� v�lvula cardiaca	996.61	
� infusi�n (procedimiento)	999.9	
�� infecci�n NCOC	999.3	
�� sangre		"v�ase Complicaciones, transfusi�n"
