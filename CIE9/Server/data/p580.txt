�� del desarrollo	315.39	
��� asociada con hipercinesia	314.1	
�� secundaria a lesi�n org�nica	784.5	
� hormonas	259.9	
� laberinto vestibular	386.9	
"� laberinto, laber�ntico (vest�bulo)"	386.9	
� marcha	781.2	
�� hist�rica	300.11	
� mecanismo neuromuscular (ojo) debida a s�filis	094.84	
� memoria	780.9	v�ase adem�s Amnesia
"�� leve, despu�s de darlos cerebrales org�nicos"	310.1	
� mental	300.9	"v�ase adem�s Trastorno, mental    "
�� asociada con enfermedades clasificadas bajo otros conceptos	316	
� metabolismo (adquirida) (cong�nita)	277.9	"v�ase adem�s Trastorno, metabolismo"
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con trastorno metab�lico "
��� embarazo ect�pico	639.4	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.4	v�ase adem�s categor�as 630-632
�� �cido argininosucc�nico	270.6	
�� amino�cidos	270.9	"v�ase adem�s Trastorno, amino�cidos"
��� arom�ticos NCOC	270.2	
��� cadena ramificada	270.3	
��� cadena recta NCOC	270.7	
��� portadores de azufre	270.4	
��� tipo especificado NCOC	270.8	
��� transporte	270.0	
�� amon�aco	270.6	
�� arginina	270.6	
�� carbohidratos NCOC	271.9	
�� ciclo de urea	270.6	
�� cistationina	270.4	
�� citrulina	270.6	
�� colesterol	272.9
�� despu�s de 	
��� aborto	639.4
��� embarazo ect�pico o molar	639.4
�� en el trabajo del parto o en el parto	669.0
�� fosfatidas	272.7
�� fosfatos	275.3
�� general	277.9
��� carbohidratos	271.9
��� fosfatos	275.3
��� hierro	275.0
��� sodio	276.9
�� glucina	270.7
�� glutamina	270.7
�� grasas	272.9
�� hierro	275.0
�� histidina	270.5
�� homocistina	270.4
�� isoleucina	270.3
�� leucina	270.3
�� lipoides	272.9
��� tipo especificado NCOC	272.8
�� lisina	270.7
�� metionina	270.4
"�� neonatal, transitoria"	775.9
��� tipo especificado NCOC	775.8
�� nitr�geno	788.9
�� ornitina	270.6
�� serina	270.7
�� sodio NCOC	276.9
�� tirosina	270.2
�� treonina	270.7
�� tript�fano	270.2
�� valina	270.3
� metabolismo de fructosa	271.2
� motora	796.1
� movimiento ocular	378.87
�� psic�gena	306.7
� nervio cerebral NCOC	352.9
� nervio craneal NCOC	352.9
� nervio olfatorio	781.1
� nervio �ptico NCOC	377.49
� nutritiva	269.9
�� u�as	703.8
� ocul�gira	378.87
�� psic�gena	306.7
� oculomotora NCOC	378.87
�� psic�gena	306.7
� personalidad (patr�n) (caracter�sticas)	301.9	"v�ase adem�s Trastorno, personalidad"
�� despu�s de da�os cerebrales org�nicos	310.1	
� poliglandular	258.9	
� psicomotora	307.9	
� pupilar	379.49	
� queratinizaci�n NCOC 		
�� gingiva	523.1	
�� labio	528.5	
�� lengua	528.7	
�� oral (mucosa) (tejidos blandos)	528.7	
� reflejos	796.1	
"� ritmo, cardiaco"	427.9	
�� posoperatoria (inmediata)	997.1	
��� efecto a largo plazo de cirug�a cardiaca	429.4	
�� psic�gena	306.2	
� secreci�n salival	527.7	
� sensaci�n (fr�o) (calor) (localizaci�n) (localizaci�n por discriminaci�n tactil) (textura) (vibratoria) NCOC	782.0	
�� gusto	781.1	
�� hist�rica	300.11	
�� olfato	781.1	
�� piel	782.0	
� sensibilidad profunda 		"v�ase Perturbaci�n, sensaci�n "
� sensorial	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
�� inervaci�n	782.0	
� sentido de temperatura	782.0	
�� hist�rica	300.11	
� simp�tica (nervio)	337.9	"v�ase adem�s Neuropat�a, perif�rica, aut�noma"
� sistema nervioso central NCOC	349.9	
� situacional (tranistoria)	309.9	"v�ase adem�s Reacci�n, ajuste"
�� aguda	308.3	
� sociop�tica	301.7	
� sue�o	780.50	
"�� desvelo, vigilia"	780.54	v�ase adem�s Hipersomnio
��� origen no org�nico	307.43	
�� especificado NCOC	780.59	
��� origen no org�nico	307.49	
�� iniciaci�n o manutenci�n	780.52	v�ase adem�s Insomnio
��� origen no org�nico	307.41	
�� origen no org�nico	307.40	
��� tipo especificado NCOC	307.49	
� tacto	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
"� �tero, inervaci�n, simp�tica, parasimp�tica"	621.8	
� vascular	459.9	
�� arterioscler�tica		v�ase Arteriosclerosis 
� vasomotora	443.9	
� vasosp�stica	443.9	
"� vigilia, desvelo (iniciaci�n o mantemiento)"	780.54	v�ase adem�s Hipersomnio
�� origen no org�nico	307.43	
"� visi�n, visual NCOC"	368.9	
�� especificada NCOC	368.8	
�� psicof�sica	368.16	
�� subjetiva	368.10	
� voz	784.40	
"Perturbaci�n coraz�n, cardiaca "		"v�ase Enfermedad, coraz�n"
Pertusis	033.9	v�ase adem�s Tos ferina
� vacunaci�n profil�ctica (contra)	V03.6	
"Peruana, de Per�, verruga de"	088.0	
"Perversi�n, pervertido "		
� apetito	307.52	
�� hist�rico	300.11	
� funci�n 		
�� gl�ndula 		
��� pineal	259.8	
��� pituitaria	253.9	
���� I�bulo anterior 		
����� defectuosa	253.2	
����� excesiva	253.1	
���� I�bulo posterior	253.6	
�� placenta		"v�ase Placenta, anormal "
� sentido de olfato o gusto	781.1	
�� psic�gena	306.7	
� sexual	302.9	"v�ase adem�s Desviaci�n, sexual"
Pesadilla	307.47	
� tipo sue�o REM (movimientos r�pidos de los ojos)	307.47	
Pesado para fechas (feto o infante)	766.1	
� 4.500 		
gramos o m�s	766.0	
� excepcionalmente	766.0	
Peso 		
� aumento (anormal) (excesiva)	783.1	
�� durante el embarazo	646.1	
��� insuficiente	646.8	
� ganancia (anormal) (excesiva)	783.1	
�� durante el embarazo	646.1	
��� insuficiente	646.8	
� menos de 1.000 		
gramos al nacer	765.0	
� p�rdida (causa desconocida)	783.2	
Peso insuficiente	783.4	
� para la edad gestacional 		v�ase Ligero para las fechas Peste
Peste	020.9	v�ase adem�s Plaga
� bub�nica	020.0	
� fulminante	020.0	
� menor	020.8	
� neum�nica 	782.7	"v�ase Plaga, neum�nica "
Patequia (s)	782.7	
� feto o reci�n nacido	772.6	
Petequial 		
� fiebre	036.0	
� tifus	081.9	
"Petges-Cl�jat o Petges-Cl�gat, s�ndrome de (poiquilodermatomiositis)"	710.3	
"Petit, enfermedad de"	553.8	"v�ase adem�s Hernia, lumbar"
Petrelidosis	117.6	
Petrositis	383.20	
� aguda	383.21	
� cr�nica	383.22	
"Peutz-Jeghers, enfermedad o s�ndrome de"	759.6	
"Peyronie, enfermedad de"	607.89	
Pez�n 		v�ase enfermedad espec�fica
"Pfeiffer, enfermedad de"	075	
"Phaehypomycosis 117,8 "		
Phlegmasia 		
� alba dolens (vasos profundos)	451.19	
�� cuando complica el parto	671.3	
�� no puerperal	451.19	
"�� puerperal, posparto, parto"	671.4	
� cerulea dolens	451.19	
"Phocas, enfermedad de"	610.1	
Phthiriasis (pubis) (cualquier sitio)	132.2	
� con cualquier infestaci�n clasificable bajo 132.0 y 132.1 132.3		
"Phthirus, infestaci�n "		v�ase Phthiriasis
Pi�n	102.9	v�ase adem�s Frambesia
Pianoma	102.1	
Piarremia	272.4	v�ase adem�s Hiperlipemia
� bilharziasis	120.9	
Piartritis 		v�ase Piartrosis
Piartrosis	711.0	"v�ase adem�s Artritis, pi�gena   "
� tuberculosa 		"v�ase Tuberculosis, articulaci�n"
Pica	307.52	
� hist�rica	300.11	
Picado 		
� dientes	520.4	
� edema	782.3	v�ase adem�s Edema
�� labio	782.3	
�� u�a	703.8	
��� cong�nito	757.5	
"Picaz�n, sensaci�n de"	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
Pich�n 		
� dedos de pie	735.8	
� pecho o t�rax de (adquirido)	738.3	
�� cong�nito	754.82	
�� raqu�tico	268.0	v�ase adem�s Raquitismo
"Pick, de "		
� adenoma tubular 	(M8640/0)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, benigna "
�� sitio no especificado 		
��� femenino	220	
��� masculino	222.0	
� atrofia cerebral	331.1	
�� con demencia	290.10	
� enfermedad 		
�� cerebro	331.1	
��� demencia en	290.10	
�� h�gado (seudocirrosis peric�rdica del h�gado)	423.2	
�� histiocitosis l�pida	272.7	
�� pericardio (seudocirrosis peric�rdica del h�gado)	423.2	
� poliserositis (seudocirrosis peric�rdica del h�gado)	423.2	
� s�ndrome 		
�� cardiaco (seudocirrosis peric�rdica del h�gado)	423.2	
�� hep�tico (seudocirrosis peric�rdica del h�gado)	423.2	
"Pick-Herxheimer, s�ndrome de (atrofia cut�nea idiop�tica difusa)"	701.8	
"Pick-Niemann, enfermedad de (histiocitosis l�pida)"	272.7	
"Pickwick, s�ndrome de (obesidad cardiopulmonar)"	278.8	
"Picnoepilepsia, picnolepsia (idiop�tica)"	345.0	v�ase adem�s Epilepsia
Picor	698.9	v�ase adem�s Prurito
� abaceros	133.8	
� acu�tico	120.3	
� alba�iles	692.89	
� avicultores	133.8	
� barberos	110.0	
� buscadores de almejas	120.3	
� contacto con telas	110.3	
� cool�es	126.9	
� copra	133.8	
� cosecha	133.8	
� cuando significa sarna	133.0	
� cubano	050.1	
� de los siete a�os	V61.1	
�� cuando significa sarna	133.0	
� dhobie	110.3	
� fil�rico	125.9	"v�ase adem�s Infestaci�n, fil�rica  "
� grano	133.8	
� invernal	698.8	
� lavanderas	692.4	
� Malabar	110.9	
�� barba	110.0	
�� cuero cabelludo	110.0	
�� pie	110.4	
� malezas	134.1	
� nadadores	120.3	
� noruego	133.0	
� ojo	379.99	
� paja	133.8	
� panaderos	692.89	
� perianal	698.0	
� queso	133.8	
� roc�o	126.9	
� sarc�ptico	133.0	
� suelo	126.9	
Picor de ac�ridos (prurito)	133.8	
Pie (cong�nito)	754.70	v�ase adem�s Talipes  
� abductus (cong�nito)	754.60	
�� adquirido	736.79	
� adductus (cong�nito)	754.79	
�� adquirido	736.79	
� adquirido NCOC	736.79	
�� planus	734	
� cavus	754.71	
�� adquirido	736.73	
� planovalgus (cong�nito)	754.69	
�� adquirido	736.79	
� planus (adquirido) (cualquier grado)	734	
�� cong�nito	754.61	
� valgus (cong�nito)	754.61	
�� adquirido	736.79	
� varus (cong�nito)	754.50	
�� adquirido	736.79	
Pie 		v�ase adem�s enfermedad espec�fica 
� enfermedad de proceso de	581.3	
� y enfermedad de la boca	078.4	
Piedra	111.2	
� barba	111.2	
�� blanca	111.2	
�� negra	111.3	
� blanca	111.2	
� cuero cabelludo	111.3	
�� blanca	111.2	
�� negra	111.3	
� negra	111.3	
Piedra(s) 		v�ase adem�s C�lculo 
� cistina	270.0	
� conducto o gl�ndula salival (cualquiera)	527.5	
� pr�stata	602.0	
� pulpa (dental)	522.2	
� renal	592.0	
� ri��n	592.0	
� s�ndrome de coraz�n	428.1	"v�ase adem�s Pallo, ventricular, izquierdo"
� ur�ter	592.1	
� uretra (impactada)	594.2	
� urinaria (conducto) (impactada) (pasaje)	592.9	
�� sitio especificado	594.8	
�� tracto inferior NCOC	594.9	
�� vejiga	594.1	
��� divert�culo	594.0	
� vejiga	594.1	
�� divert�culo	594.0	
� xantina	277.2	
Piel 		v�ase adem�s enfermedad espec�fica 
� donante	V59.1	
� endurecida	710.9	
"Piel de caim�n, enfermedad de (ictiosis cong�nita)"	757.1	
� adquirida	701.1	
Piel envejecida	701.8	
Pielectasia	593.89	
Pielectasis	593.89	
Pielitis (cong�nita) (ur�mica)	590.80	
� con 		
�� aborto 		"v�ase Aborto, por tipo, con complicaci�n especificada NCOC "
�� c�lculo o piedras	592.9	
�� embarazo ect�pico	639.8	v�ase adem�s categor�as 630-632
�� embarazo molar	639.8	v�ase adem�s categor�as 630-632
�� ri��n contra�do	590.00	
� aguda	590.10	
�� con necrosis medular renal	590.11	
� cr�nica	590.00	
�� con 		
��� c�lculo	592.9	
��� necrosis medular renal	590.01	
"� cuando complica el embarazo, parto o puerperio"	646.6	
�� que afecta al feto o al reci�n nacido	760.1	
� despu�s de 		
�� aborto	639.8	
�� embarazo ect�pico o molar	639.8	
� gonoc�cica	098.19	
�� cr�nica o de duraci�n de 2 meses o m�s	098.39	
� qu�stica	590.3	
� tuberculosa	016.0[590.81]	v�ase adem�s Tuberculosis
Pielocaliectasia	593.89	
Pielocistitis	590.80	v�ase adem�s Pielitis
Pieloflebitis	451.89	
Pielohidronefrosis	591	
Pielonefritis	590.80	v�ase adem�s Pielitis
� aguda	590.10	
�� con necrosis medular renal	590.11	
� calculosa	592.9	
� cr�nica	590.00	
� sifil�tica (tard�a)	095.4	
� tuberculosa	016.0[590.81]	v�ase adem�s Tuberculosis
Pielonefrosis	590.80	v�ase adem�s Pielitis
� cr�nica	590.00	
Pieloureteritis qu�stica	590.3	
"Piemia, pi�mico (purulenta)"	038.9	v�ase adem�s Septicemia
� absceso 		v�ase Absceso 
� articulaci�n	711.0	"v�ase adem�s Artritis, pi�gena"
� artritis	711.0	"v�ase adem�s Artritis, pi�gena"
� Bacillus coli	038.42	
� embolia 		"v�ase Embolia, pi�mica "
� estafiloc�cica	038.1	
� estreptoc�cica	038.0	
� fiebre	038.9	
� flebitis 		v�ase Flebitis 
� h�gado	572.1	
� infecci�n	038.9	
� meningoc�cica	036.2	
� neumoc�cica	038.2	
� organismo especificado NCOC	038.8	
� porta	572.1	
� posvacunal	999.3	
� reci�n nacido	771.8	
� tuberculosa 		"v�ase Tuberculosis, miliar Pierna "
Pierna		v�ase enfermedad espec�fica
Pierna desigual (adquirida) (longitud)	736.81	
� cong�nita	755.30	
Pierna inquieta (s�ndrome de)	333.99	
"Pierre Mauriac, s�ndrome de (diabetes-enanismo-obesidad)"	258.1	
"Pierre Robin, deformidad o s�ndrome de (cong�nito)"	756.0	
"Pierre-Marie, s�ndrome de (osteoartropat�a hipertr�fica pulmonar)"	731.2	
"Pierre-Marie-Bamberger, s�ndrome de (osteoartropat�a hipertr�fica pulmonar)"	731.2	
"Pierson, enfermedad u osteocondrosis de"	732.1	
Pies 		v�ase enfermedad espec�fica
Pigmentaci�n (anormal)	709.0
� anomal�as NCOC	709.0
�� cong�nitas	757.33
� conjuntiva	371.10
� c�rnea	371.10
�� anterior	371.11
�� estromal	371.12
�� posterior	371.13
"� escroto, cong�nita"	757.33
� limbo de la c�rnea	371.10
� metales	709.0
"� papila �ptica, cong�nita"	743.57
� p�rpados (cong�nita)	757.33
�� adquirida	374.52
� retina (cong�nita) (confluente) (nevoide)	743.53
�� adquirida	362.74
Pigmentos biliares en orina	791.4	
"Pildoreros, mano de (intr�nseca)"	736.09	
Piletlebitis (supurativa)	572.1	
Piles 		v�ase Hemorroides
Piletromboflebitis	572.1	
Piletrombosis	572.1	
Pili 		
� annulati o torti (cong�nito)	757.4	
� incarnati	704.8	
Pilomatrixoma 	(M8110/0)	"v�ase Neoplasia, pile, benigna"
Pilonidal 		v�ase enfermedad espec�fica
Piloritis	535.5	v�ase adem�s Gastritis
"P�loro, pil�rico "		v�ase enfermedad espec�fica
Pilorospasmo (reflejo)	537.81	
� cong�nito o infantil	750.5	
� neur�tico	306.4	
� psic�geno	306.4	
� reci�n nacido	750.5	
"Pineal, cuerpo o gl�ndula "		v�ase enfermedad espec�fica
Pinealoblastoma 	194.4(M9362/3)	
Pinealoma 	237.1(M9360/1)	
� maligno 	194.4(M9360/3)	
Pineoblastoma 	194.4(M9362/3)	
Pineocitoma 	237.1(M9361/1)	
Pingu�cula	372.51	
"Pinkus, enfermedad de (liquen n�tido)"	697.1	
Pinselhaare (cong�nita)	757.4	
Pinta	103.9	
� chancro (primario)	103.0	
� hiperqueratosis	103.1	
� lesiones 		
�� cardiovasculares	103.2	
�� cut�neas (acr�micas) (cicatriciales) (discr�micas)	103.2	
��� hipercr�micas	103.1	
��� mixtas (acr�micas e hipercr�micas)   	103.3	
�� hipercr�micas	103.1	
�� intermedias	103.1	
�� mixtas	103.3	
�� primarias	103.0	
�� tard�as	103.2	
� p�pula (primaria)	103.0	
� placas eritematosas	103.1	
� vit�ligo	103.2	
Pintide	103.0	
"Pintores, c�lico de los"	984.9	
� tipo de plomo especificado 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos"
"Pinza de langosta, mano en"	755.58	
Pioartrosis 		v�ase Piartrosis Piocele 
� mastoideo	383.00	
� seno (accesorio) (nasal)	473.9	v�ase adem�s Sinusitis
� turbinado (hueso)	473.9	
� uretra	597.0	v�ase adem�s Uretritis
"Pioci�nea, dermatitis"	686.0	
Piocistitis 	595.9	v�ase adem�s Cistitis
"Pioc�cica, dermatitis"	686.0	
"Pioc�cide, piel"	686.0	
Piocolpos	616.10	v�ase adem�s Vaginitis
"Pioderma, piodermia NCOC"	686.0	
� gangrenoso	686.0	
� vegetante	686.8	
Piodermatitis	686.0	
� vegetante	686.8	
Pioflebitis 		v�ase Flebitis 
Pi�geno		v�ase enfermedad espec�fica
Piohemia		v�ase enfermedad espec�fica
Piohidronefrosis	590.80	v�ase adem�s Pielitis
Piojo (infestaci�n por) 		v�ase Pediculosis 
Piojo (infestaci�n)	132.9	
� cabeza (Pediculus capitis)	132.0	
� cuerpo (Pediculus corporis)	132.1	
� ladillas	132.2	
� mixtos (clasificables m�s de una de las categor�as 132.0-132.3)	132.2	
� pubis (Pediculus pubis)	132.2	
Pi�metra	615.9	
Piometrio	615.9	v�ase adem�s Endometritis
Piometritis	615.9	v�ase adem�s Endometritis
Piomiositis	728.0	
� osificante	728.19	
� tropical (bungpagga)	040.81	
Pionefritis	590.80	v�ase adem�s Pielitis
� cr�nica	590.00	
Pionefrosis (cong�nita)	590.80	v�ase adem�s Pielitis
� aguda	590.10	
Pioneumopericardio	420.99	
Pioneumot�rax (infeccioso)	510.9	
� con f�stula	510.0	
� subdiafragm�tico	567.2	v�ase adem�s Peritonitis
� subfr�nico	567.2	v�ase adem�s Peritonitis
� tuberculosa	012.0	v�ase adem�s Tuberculosis
Piooforitis	614.2	v�ase adem�s Salpingooforitis
Pioovario	614.2	v�ase adem�s Salpingooforitis
Piopago	759.4	
 Piopericardio	420.99	
Piopericarditis	420.99	
Piorrea (alveolar)	523.4	
� degenerativa	523.5	
Piosalpingitis	614.2	v�ase adem�s Salpingooforitis
Piosalpinx	614.2	v�ase adem�s Salpingooforitis
Piosepticemia 		v�ase Septicemia Piosis 
"� Corlett, de (imp�tigo)"	684	
"� Manson, de (p�nfigo contagioso)"	684	
Piot�rax	510.9	
� con f�stula	510.0	
� tuberculoso	012.0	v�ase adem�s Tuberculosis
Piour�ter	593.89	
� tuberculoso	016.2	v�ase adem�s Tuberculosis
"Piramidopalidonigral, s�ndrome"	332.0	
Pirexia (de origen desconocido) (P.U.O.) (P.O.D.)	780.6	
� atmosf�rica	992.0	
� calor	992.0	
� durante el trabajo del parto	659.2	
� inducida por el ambiente 		
�� reci�n nacido	778.4	
� puerperal	672	
"� reci�n nacido, inducida por el ambiente"	778.4	
Piroglobulinemia	273.8	
Piroman�a	312.33	
Pirosis	787.1	
Pirroloporfiria	277.1	
"Piry, fiebre"	066.8	
"Pistola, herida de "		"v�ase Arma de fuego, herida por"
"Pitecoide, pelvis"	755.69	
� con desproporci�n (fetopelviana)	653.2	
�� cuando obstruye el trabajo del parto	660.1	
�� que afecta al feto o al reci�n nacido	763.1	
Pitiatismo	300.11	
Pitiriasis	696.5	
� alba	696.5	
� capitis	690
� circinada (y maculosa)	696.3
"� Hebra, de (dermatitis exfoliativa)"	695.89
� liquenoide y varioliforme	696.2
� maculosa (y circinada)	696.3
� negra 	111.1
� pilaris	757.39
�� adquirida	701.1
"�� Hebra, de"	696.4
� redonda (rotunda)	696.3
� rosada	696.3
� rubra (de Hebra)	695.89
�� pilaris	696.4
� seca	690
� simple	690
� Streptogenes	696.5
� tipo especificado NCOC	696.5	
� versicolor	111.0	
�� escrotal	111.0	
Pituitaria 		
� enfermedad de aspiradores de rape	495.8	
� gl�ndula 		v�ase enfermedad espec�fica
Piuria (bacteriana)	599.0	
"Pizarra, pulm�n de cortadores o mineros de"	502	
Placa 		
"� arteria, arterial "		v�ase Arteriosclerosis 
� calc�rea 		v�ase Calcificaci�n 
� Hollenhorst (retiniana)	362.33	
� lengua	528.6	
"Placenta, placentario "		
� ablandamiento (prematura) 		"v�ase Placenta, anormal "
� ablatio	641.2	
�� que afecta al feto o al reci�n nacido	762.1	
� abruptio	641.2	
�� que afecta al feto o al reci�n nacido	762.1	
� accreta (sin hemorragia)	667.0	
�� con hemorragia	666.0	
� adherente (sin hemorragia)	667.0	
�� con hemorragia	666.0	
"� anormal, anormalidad"	656.7	
�� con hemorragia	641.8	
��� que afecta al feto o al reci�n nacido	762.1	
�� que afecta al feto o al reci�n nacido	762.2	
� apoplej�a		"v�ase Placenta, separaci�n "
� bilobular 		"v�ase Placenta, anormal "
� bipartita 		"v�ase Placenta, anormal "
� central 		"v�ase Placenta, previa "
� circunvalada		"v�ase Placenta, anormal "
� deficiencia		"v�ase Placenta, insuficiencia "
� degeneraci�n 		"v�ase Placenta, insuficiencia "
� desprendimiento (parcial) (prematuro) (con hemorragia)	641.2	
�� que afecta al feto o al reci�n nacido	762.1	
� dimidiata 		"v�ase Placenta, anormal "
� disfunci�n 		"v�ase Placenta, insuficiencia "
� doble 		"v�ase Placenta, anormal "
� enfermedad	656.7	
�� que afecta al feto o al reci�n nacido	762.2	
� espuria 		"v�ase Placenta, anormal "
� fenestrada 		"v�ase Placenta, anormal "
� fibrosis 		"v�ase Placenta, anormal "
� hematoma 		"v�ase Placenta, anormal "
� hemorragia NCOC 		"v�ase Placenta, separaci�n "
� hiperplasia 		"v�ase Placenta, anormal "
� implantaci�n o inserci�n baja 		"v�ase Placenta, previa "
� increta (sin hemorragia)	667.0	
�� con hemorragia	666.0	
� infarto	656.7	
� que afecta al feto o al reci�n nacido	762.2	
� inserci�n viciosa 		"v�ase Placenta, previa "
� insuficiencia 		
�� cuando afecta (al) 		
��� atenci�n del embarazo	656.5	
��� feto o reci�n nacido	762.2	
� lateral 		"v�ase Placenta, previa "
� I�bulo accesorio 		"v�ase Placenta, anormal "
� mala posici�n 		"v�ase Placenta, previa "
� malformaci�n 		"v�ase Placenta, anormal "
� marginal 		"v�ase Placenta, previa "
� membran�cea 		"v�ase Placenta, anormal "
� mola carnosa	631	
� multilobular		"v�ase Placenta, anormal "
� multipartita 		"v�ase Placenta, anormal "
� necrosis 		"v�ase Placenta, anormal "
� peque�a 		"v�ase Placenta, insuficiencia "
� percreta (sin hemorragia)	667.0	
�� con hemorragia	666.0	
� perturbaci�n o mal funcionamiento hormonal 		"v�ase Placenta, anormal "
� p�lipo(s)	674.4	
� posici�n baja 		"v�ase Placenta, previa "
� previa (central) (completa) (con hemorragia) (lateral) (marginal) (parcial) (total)	641.1	
�� observada 		
"��� antes del trabajo del parto, sin hemorragia (con parto por ces�rea)"	641.0	
��� durante el embarazo (sin hemorragia)	641.0	
�� que afecta al feto o al reci�n nacido	762.0	
�� sin hemorragia (antes del trabajo y parto) (durante el embarazo)	641.0	
� quiste (amni�tico) 		"v�ase Placenta, anormal "
"� raqueta, en forma de "		"v�ase Placenta, anormal "
� retenci�n (con hemorragia)	666.0	
"�� fragmentos, cuando complica el puerperio (hemorragia retardada)"	666.2	
��� sin hemorragia	667.1	
"�� posparto, puerperal"	666.2	
�� sin hemorragia	667.0	
� retenida (con hemorragia)	666.0	
�� sin hemorragia	667.0	
� seno marginal (hemorragia) (ruptura)	641.2	
�� que afecta al feto o al reci�n nacido	762.1	
� separaci�n (con hemorragia) (normalmente implantada) (parcial) (prematura)	641.2	
�� que afecta al feto o al reci�n nacido	762.1	
� s�ptuple 		"v�ase Placenta, anormal "
� sifil�tica	095.8	
� s�ndromes de transfusi�n	762.3	
� succenturiada		"v�ase Placenta, anormal "
� transmisi�n de sustancia qu�mica 		"v�ase Absorci�n, qu�mica, a trav�s de placenta "
� trilobular 		"v�ase Placenta, anormal "
� tripartita		"v�ase Placenta, anormal "
� triple 		"v�ase Placenta, anormal "
� vaso varicoso 		"v�ase Placenta, anormal"
Placentitis 		
� cuando complica el embarazo	658.4	
� que afecta al feto o al reci�n nacido	762.7	
Plaga	020.9	
� abortiva	020.8	
� ambulatoria	020.8	
� amigdaliana	020.9	
�� septic�mica	020.2	
� bub�nica	020.0	
� celulocut�nea	020.1	
� gl�ndula linf�tica	020.0	
� neum�nica	020.5	
�� primaria	020.3	
�� secundaria	020.4	
� pulmonar 		"v�ase Plaga, neum�nica "
� septic�mica	020.2	
� vacunaci�n profil�ctica (contra)	V03.3	
Plagiocefalia (cr�neo)	754.0	
Planificaci�n familiar	V25.09	
� contracepci�n	V25.9	
� procreaci�n	V26.4	
Plano 		
� c�mara (anterior) (ojo)	360.34	
� electroencefalograma (EEG)	348.8	
"� �rgano o sitio, cong�nito NCOC "		"v�ase  Anomal�a, tipo especificado NCOC "
"� pecho, cong�nito"	754.89	
� pelvis	738.6	
�� con desproporci�n (fetopelviana)	653.2	
��� cuando obstruye el parto	660.1	
���� que afecta al feto o al reci�n nacido   	763.1	
��� que afecta al feto o al reci�n nacido	763.1	
�� cong�nita	755.69	
� pie (adquirido) (doloroso) (esp�stico) (postural) (tipo fijo)	734	
�� astr�galo (taba) (cong�nito)	754.61	
�� cong�nito	754.61	
��� astr�galo (taba)	754.61	
��� talo vertical	754.61	
�� raqu�tico	268.1	
"�� talo vertical, cong�nito"	754.61	
Plantas (de los pies) picadas por los gusanos	102.3	
"Plasmacitoma, plasmocitoma (solitario) "	238.6(M9731/1)	
� benigno 	(M9731/0)	"v�ase Neoplasia, por sitio, benigna "
� maligno 	203.8(M9731/3)	
Plasmacitosis	288.8
"Plasm�ticas, mieloma de c�lulas"	203.0
Platibasia	756.0
Plationiquia (cong�nita)	757.5
� adquirida	703.8
"Platipeloide, pelvis"	738.6
� con desproporci�n (fetopelviana)	653.2
�� cuando obstruye el trabajo del parto	660.1
��� que afecta al feto o al reci�n nacido	763.1
�� que afecta al feto o al reci�n nacido	763.1
� cong�nita	755.69
Platispondilia	756.19
"Playa, otitis de"	380.12
"Plegada, lengua"	529.5
� cong�nita	750.13
Pl�tora	782.62
� reci�n nacido	776.4	
"Pleura, pleural "		v�ase enfermedad espec�fica
Pleuralgia	786.52	
Pleures�a (adherente) (aguda) (antigua) (con pleura adherente) (costal) (cr�nica) (diafragm�tica) (doble) (est�ril) (f�tida) (fibrinosa) (fibrosa) (interlobular) (latente) (no resuelta) (pl�stica) (primaria) (pulmonar) (residual) (seca) (subaguda)		
� con 		
�� derrame (sin menci�n de causa)	511.9	
"��� bacteriana, no tuberculosa"	511.1	
��� estafiloc�cica	511.1	
��� estreptoc�cica	511.1	
��� neumoc�cica	511.1	
��� no tuberculosa NCOC	511.9	
���� bacteriana	511.1	
��� tipo especificado NCOC	511.8	
��� tuberculosa	012.0	v�ase adem�s Tuberculosis
"���� primaria, progresiva"	010.1	
�� influenza o gripe	487.1	
�� tuberculosis 		"v�ase Pleures�a, tuberculosa "
� enquistada	511.8	
� estafiloc�cica	511.0	
�� con derrame	511.1	
� estreptoc�cica	511.0	
�� con derrame	511.1	
� exudativa	511.9	"v�ase adem�s Pleures�a, con derrame"
"�� bacteriana, no tuberculosa"	511.1	
� fibrinopurulenta	510.9	
�� con f�stula	510.0	
� fibropurulenta	510.9	
�� con f�stula	510.0	
� hemorr�gica	511.8	
� influenzal	487.1	
� neumoc�cica	511.0	
�� con derrame	511.1	
� purulenta	510.9	
�� con f�stula	510.0	
� s�ptica	510.9	
�� con f�stula	510.0	
� serofibrinosa	511.9	"v�ase adem�s Pleures�a, con derrame"
"�� bacteriana, no tuberculosa"	511.1	
� seropurulenta	510.9	
�� con f�stula	510.0	
� serosa	511.9	"v�ase adem�s Pleures�a, con derrame"
"�� bacteriana, no tuberculosa"	511.1	
� supurativa	510.9	
�� con f�stula	510.0	
� traum�tica o postraum�tica (actual)	862.29	
�� con herida penetrante de la cavidad	862.39	
� tuberculosa (con derrame)	012.0	"v�ase adem�s Tuberculosis, pleura"
"�� primaria, progresiva"	010.1	
Pleuritis seca 		v�ase Pleures�a
Pleurobronconeumon�a	485	"v�ase adem�s Neumon�a, bronco-"
Pleurodinia	786.52	
� epid�mica	074.1	
� v�rica	074.1	
Pleurohepatitis	573.8	
Pleuroneumon�a (aguda) (bilateral) (doble) (s�ptica)	486	v�ase adem�s Neumon�a
� cr�nica	515	"v�ase adem�s Fibrosis, pulm�n"
Pleuropericarditis	423.9	v�ase adem�s Pericarditis
� aguda	420.90	
Pleurorrea	511.8	v�ase adem�s Hidrot�rax
Plexitis braquial	353.0	
"Plexo braquial, s�ndrome de"	353.0	
Plica 		
� amigdaliana	474.8	
� polonesa o pol�nica	132.0	
Plicae dysphonia ventricularis	784.49	
Pliegue simio	757.2	
Plomo 		v�ase adem�s enfermedad espec�fica 
� enevenenamiento	984.9	
�� tipo de plomo especificado 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos "
� incrustaci�n de c�rnea	371.5	
"Plomo, pulm�n de mineros de"	503	
Plumbismo	984.9	
� tipo de plomo especificado 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos"
"Plummer, enfermedad de (bocio nodular t�xico)"	242.3	
"Plummer-Vinson, s�ndrome de (disfagia siderop�nica)"	280.8	
"Pluricarencial, s�ndrome de la infancia"	260	
"Plurideficiencia, s�ndrome de la infancia "	260	
"Plus (y minus), mano (intr�nseca)"	736.09	
"Pneumocystis carinii, neumon�a por"	136.3	
" Pobre(s), escaso(s), insuficiente(s) "		
"� contracciones, trabajo del parto"	661.2
�� que afecta al feto o al reci�n nacido	763.7
� crecimiento fetal NCOC	764.9
�� cuando afecta la atenci�n del embarazo	656.5
� historia obst�trica	V23.4
� reflejo de mamar (reci�n nacido)	796.1
� visi�n NCOC	369.9
Pobreza	V60.2
"Poco profundo, acet�bulo"	736.39
Podagra	274.9
Podenc�falo	759.8
Poiquilocitosis	790.0
Poiquilodermatomiositis	710.3
Poiquilodermia	709.0
� atr�fica vascular	696.2
"� Civatte, de"	709.0
� cong�nita	757.33	
Polaquiuria	788.4	
�� psic�gena	306.53	
Poliadenitis	289.3	v�ase adem�s Adenitis
� maligna	020.0	
Polialgia	729.9	
Poliangi�tis (esencial)	446.0	
Poliarteritis (nudosa) (renal)	446.0	
Poliartralgia	719.49	
� psic�gena	306.0	
"Poliartritis, poliartropat�a NCOC"	716.59	
� debida a o asociada con otras enfermedades especificadas 		"v�ase Artritis, debida a o asociada con "
� end�mica	716.0	"v�ase adem�s Enfermedad, Kaschin-Beck"
� inflamatoria	714.9	
�� tipo especificado NCOC	714.89	
� juvenil (cr�nica)	714.30	
�� aguda	714.31	
� migratoria 		"v�ase Fiebre, reum�tica "
� reum�tica	714.0	
�� fiebre (aguda) 		"v�ase Fiebre, reum�tica"
"Policarencial, s�ndrome de la infancia"	260	
"Polic�as, enfermedad de los"	729.2	
Policitemia (primaria) (rubra) (vera) 	238.4(M9950/1)	
� adquirida	289.0	
� benigna	289.0	
�� familiar	289.6	
� debida a 		
�� ca�da en volumen de plasma	289.0	
�� gemelo donante	776.4	
�� gran altitud	289.0	
�� tensi�n	289.0	
�� transfusi�n maternofetal	776.4	
� emocional	289.0
� eritropoyetina	289.0
� espuria	289.0
� familiar (benigna)	289.6
� Gaisb�ck (hipert�nica)	289.0
� gran altitud	289.0
� hipert�nica	289.0
� hipox�mica	289.0
� nefr�gena	289.0
� neonatal	776.4
� relativa	289.0
� secundaria	289.0
� tensi�n	289.0
Policitosis criptog�nica	289.0
Policondritis (atr�fica) (cr�nica) (recurrente o recidiva)	733.99
Policoria	743.46
"Polidactilismo, polidactilia"	755.00	
� dedos de 		
�� mano	755.01	
�� pie	755.02	
Polidipsia	783.5	
Poliembrioma 	(M9072/3)	"v�ase Neoplasia, por sitio, maligna"
Polifagia	783.6	
Poligalactia	676.6	
Poliglandular 		
� deficiencia	258.9	
� discrasia	258.9	
� disfunci�n	258.9	
� s�ndrome	258.8	
Pol�gono de Willis 		v�ase enfermedad espec�fica
Polihidramnios		v�ase adem�s Hidramnios 657
Polimastia	757.6	
Polimenorrea	626.2	
Polimialgia	725	
� arter�tica	446.5	
� reum�tica	725	
Polimicrogiria	742.2	
Polimiositis (aguda) (cr�nica) (hemorr�gica)	710.4	
� con implicaci�n de 		
�� piel	710.3	
�� pulm�n	710.4[517.8]	
� osificante (generalizada) (progresiva)	728.19	
"� Wagner, de (dermatomiositis)"	710.3	
"Polineuritis, polineur�tico"	356.9	v�ase adem�s Polineuropat�a
� alcoh�lica	357.5	
�� con psicosis	291.1	
� at�xica hereditaria	356.3	
� craneal	352.6	
� debida a falta de vitamina NCOC	269.2[357.4]
� diab�tica	250.6[357.2]
� end�mica	265.0[357.4]
� eritredema	985.0
� febril	357.0
� idiop�tica aguda	357.0
� infecciosa (aguda)	357.0
� nutricional	269.9[357.4]
� posinfecciosa	357.0
 Polineuropat�a (perif�rica)	356.9
� alcoh�lica	357.5
� amiloidea	277.3[357.4]
� ars�nica	357.7
� debida a 	
�� agente t�xico NCOC	357.7
�� ars�nico	357.7
�� compuestos organofosfatados	357.7	
�� droga o sustancia medicamentosa	357.6	
��� sobredosis o administraci�n o ingesti�n de sustancia incorrecta	977.9	
���� droga especificada 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos "
��� sustancia correcta administrada de forma correcta	357.6	
�� falta de vitamina NCOC	269.2[357.4]	
�� pelagra	265.2[357.4]	
�� plomo	357.7	
�� porfiria	277.1[357.4]	
�� suero	357.6	
�� suero antitet�nico	357.6	
� diab�tica	250.6[357.2]	
� en 		
�� amiloidosis	277.3[357.4]	
�� artritis reumatoide	714.0[357.1]	
�� avitaminosis	269.2[357.4]	
��� especificada NCOC	269.1[357.4]	
�� beriberi	265.0[357.4]	
�� carencia de 		
��� complejo B NCOC	266.2[357.4]	
��� vitamina B	266.9[357.4]	
��� vitamina B6	266.1[357.4]	
�� diabetes	250.6[357.2]	
�� difteria	032.89[357.4]	v�ase adem�s Difteria
�� enfermedad vascular del col�geno NCOC  	710.0[357.1]	
�� herpes zoster	053.13	
�� hipoglicemia	251.2[357.4]	
�� lupus eritematoso diseminado	710.0[357.1]	
�� neoplasia maligna NCOC	199.1[357.3](M8000/3)	
�� parotiditis o paperas	072.72	
�� pelagra	265.2[357.4]	
�� poliarteritis nudosa	446.0[357.1]	
�� porfiria	277.1[357.4]	
�� sarcoidosis	135 [357.4]	
�� uremia	585.[357.4]	
� hereditaria	356.0	
� idiop�tica	356.9	
�� progresiva	356.4	
� nutricional	269.9[357.4]	
�� especificada NCOC	269.8[357.4]	
� plomo	357.7	
� posherp�tica	053.13	
� progresiva	356.4	
� sensorial (hereditaria)	356.2	
Polinosis	477.0	
Polioencefalitis (aguda) (bulbar)	045.0	"v�ase adem�s Poliomielitis, bulbar"
� inferior	335.22	
� influenzal	487.8	
"� superior hemorr�gica (aguda) (Wernicke, de)"	265.1	
"� Wernicke, de (superior hemorr�gica)"	265.1	
Polioencefalomielitis (aguda) (anterior) (bulbar)	045.0	v�ase adem�s Polioencefalitis
"Polioencefalopat�a, superior hemorr�gica"	265..1	
� con 		
�� beriberi	265.0	
�� pelagra	265.2	
Poliomeningoencefalitis 		v�ase Meningoencefalitis
Poliomielitis (aguda) (anterior) (epid�mica)	045.9	
� con 		
�� par�lisis	045.1	
��� bulbar	045.0	
� abortiva	045.2	
"� antigua, con deformidad"	138	
� ascendente	045.9	
�� progresiva	045.9	
� bulbar	045.0
� cerebral	045.0
� cong�nita	771.2
� contacto	V01.2
� cr�nica	335.21
� deformidades	138
� efecto tard�o	138
"� espinal, aguda"	045.9
� exposici�n a	V01.2
� no epid�mica	045.9
� no paral�tica	045.2
"� posterior, aguda"	053.19
� residual	138
� secuelas	138
� sifil�tica (cr�nica)	094.89
� vacunaci�n profil�ctica (contra)	V04.0
Polioniquia	757.5
Poliop�a	368.2
� refractaria	368.15
Poliorquidismo (tres test�culos)	752.8
