�� retina	362.17	
�� traum�tica 		"v�ase Traumatismo, vaso sangu�neo, por sitio "
� vejiga	596.9	
� verrugosa 		v�ase Verruga 
� visual 		
�� cinta NCOC	377.63	"v�ase adem�s Trastorno, visual, cinta"
�� corteza NCOC	377.73	"v�ase adem�s Traumatismo, visual, corteza"
Let�rgico 		v�ase enfermedad espec�fica
Letargo	780.7	
� africano	086.5	
"Letterer-Siwe, enfermedad de (histiocitosis  X) "	202.5 (M9722/3)	
Leucasmo	270.2	
"Leucemia, leuc�mico (cong�nita) "	208.9 (M9800/3)	
� aguda NCOC 	208.0 (M9081/3)	
� aleuc�mica NCOC 	208.8 (M9804/3)	
�� granuloc�tica 	205.8 (M9864/3)	
� bas�fila 	205.1 (M9870/3)
� bl�stica (c�lulas) 	208.0 (M9801/3)
�� granuloc�tica 	205.0 (M9861/3)
� c�lulas 	
�� cebadas 	207.8 (M9900/3)
�� estaminales 	208.0 (M9801/3)
�� gigantes 	207.2 (M9910/3)
�� linfosarcoma 	207.8 (M9850/3)
�� plasm�ticas 	203.1 (M9830/3)
�� vellosas 	202.4 (M9940/3)
� compuesta 	207.8 (M9810/3)
� cr�nica NCOC 	208.1 (M9803/3)
� eosin�fila 	205.1 (M9880/3)
� granuloc�tica 	205.9 (M9860/3)
�� aguda 	205.0 (M9861/3)
�� aleuc�mica 	205.8 (M9864/3)
�� bl�stica 	205.0 (M9861/3)
�� cr�nica 	205.1 (M9863/3)
�� subaguda 	205.2 (M9862/3)
�� subleuc�mica 	205.8 (M9864/3)
� hemobl�stica 	208.0 (M9801/3)
� histioc�tica 	206.9 (M9890/3)
� linf�tica 	204.9 (M9820/3)
�� aguda 	204.0 (M9821/3)
�� aleuc�mica 	204.8 (M9821/3)
�� cr�nica 	204.1 (M9823/3)
�� subaguda 	204.2 (M9822/3)
�� subleuc�mica 	204.8 (M9824/3)
� linfobl�stica	204.0 (M9821/3)
� linfoc�tica 	204.9 (M9820/3)
�� aguda 	204.0 (M9821/3)
�� aleuc�mica 	204.8 (M9824/3)
�� cr�nica 	204.1 (M9820/3)	
�� subaguda 	204.2 (M9822/3)	
�� subleuc�mica 	204.8 (M9824/3)	
� linf�gena 	(M9820/3)	"v�ase Leucemia, linfoide"
� linfoide 	204.9 (M9820/3)	
�� aguda 	204.0 (M9821/3)	
�� aleuc�mica 	204.8 (M9824/3)	
�� bl�stica 	204.0 (M9821/3)	
�� cr�nica 	204.1 (M9823/3)	
�� subaguda 	204.2 (M9822/3)	
�� subleuc�mica 	204.8 (M9824/3)	
� megacarioc�tica 	207.2 (M9910/3)	
� megacariocitoide 	207.2 (M9910/3)	
� mielobl�stica 	205.0 (M9861/3)	
� mieloc�tica 	205.1 (M9863/3)	
�� aguda 	205.0 (M9861/3)	
� miel�gena 	205.9 (M9860/3)
�� aguda 	205.0 (M9861/3)
�� aleuc�mica 	205.8 (M9864/3)
�� cr�nica 	205.1 (M9863/3)
�� monocitoide 	205.1 (M9863/3)
�� subaguda 	205.2 (M9862/3)
�� subleuc�mica 	205.8 (M9864/3)
� mieloide 	205.9 (M9860/3)
�� aguda 	205.0 (M9861/3)
�� aleuc�mica 	205.8 (M9864/3)
�� cr�nica 	205.1 (M9863/3)
�� subaguda 	205.2 (M9862/3)
�� subleuc�mica 	205.8 (M9864/3)
� mielomonoc�tica 	205.9 (M9860/3)
�� aguda 	205.0 (M9861/3)
�� cr�nica 	205.1 (M9863/3)
� mixta (c�lulas) 	207.8 (M9810/3)
� monobl�stica 	206.0 (M9891/3)
� monoc�tica (tipo Schilling) 	206.9 (M9890/3)
�� aguda 	206.0 (M9891/3)
�� aleuc�mica 	206.8 (M9894/3)
�� cr�nica 	206.1 (M9893/3)
�� subaguda 	206.2 (M9892/3)
�� subleuc�mica 	206.8 (M9894/3)
�� tipo Naegeli 	205.1 (M9863/3)
� monocitoide 	206.9 (M9890/3)
�� aguda 	206.0 (M9891/3)
�� aleuc�mica 	206.8 (M9894/3)
�� cr�nica 	206.1 (M9893/3)
�� miel�gena 	205.1 (M9863/3)
�� subaguda 	206.2 (M9892/3)
�� subleuc�mica 	206.8 (M9894/3)
� monomieloc�tica 	(M9860/3)	"v�ase Leucemia, mielomonoc�tica "
� neutr�fila 	205.1 (M9865/3)	
� no diferenciada 	208.0 (M9801/3)	
� plasmac�tica 	203.1 (M9830/3)	
� prolinfoc�tica 	(M9825/3)	"v�ase Leucemia, linfoide "
"� promieloc�tica, aguda "	205.0 (M9866/3)	
� subaguda NCOC 	208.2 (M9802/3)	
� subleuc�mica NCOC 	208.8 (M9804/3)	
"� tipo Naegeli, monoc�tica "	205.1 (M9863/3)	
"� tipo Schilling, monoc�tica "	(M9890/3)	"v�ase Leucemia, monoc�tica "
� tromboc�tica 	207.2 (M9910/3)	
"Leucemoide, reacci�n (linfoc�tica) (monoc�tica) (mieloc�tica)"	288.8	
Leucinosis	270.3	
Leucocitemia 		v�ase Leucemia
"Leucocito perezoso, s�ndrome de"	288.0	
Leucocitosis	288.8	
� bas�fila	288.8	
� eosin�fila	288.3	
� linfoc�tica	288.8	
� monoc�tica	288.8	
� neutr�fila	288.8	
Leucocoria	360.44	
Leucocraurosis vulvar	624.0	
Leucoderma	709.0	
� sifil�tica	091.3	
�� tard�a	095.8	
Leucodermia	709.0	v�ase adem�s Leucoderma
Leucodistrofia (c�lulas globoides) (cerebral) (metacrom�tica) (progresiva) (sudan�fila)	330.0	
"Leucoedema, boca o lengua"	528.7	
Leucoencefalitis 		
� esclerosante subaguda	046.2	
"�� van Bogaert, de"	046.2	
� hemorr�gica aguda (posinfecciosa) NCOC	136.9 [323.6]	
�� posinmunizaci�n o posvacunaci�n	323.5	
"� van Bogaert, de (esclerosante)"	046.2	
Leucoencefalopat�a	323.9	v�ase adem�s Encefalitis
� hemorr�gica necrotizante aguda (posinfecciosa)	136.9 [323.6]	
�� posinmunizaci�n o posvacunaci�n	323.5	
� metacrom�tica	330.0	
� multifocal (progresiva)	046.3	
"� progresiva, multifocal"	046.3	
Leucoeritroblastosis	289.0	
Leucoeritrosis	289.0	
Leucolinfosarcoma 	207.8 (M9850/3)	
Leucoma (c�rnea) (cuando interfiere con la visi�n)	371.03	
� adherente	371.04	
Leucomelanopat�a hereditaria	288.2	
Leuconiquia (punctata) (estriada)	703.8	
� cong�nita	757.5	
Leucopat�a 		
� ungueal	703.8	
�� cong�nita	757.5	
Leucopenia	288.0	
� c�clica	288.0	
� familiar	288.0	
� maligna	288.0	
� neonatal transitoria	776.7	
� peri�dica	288.0	
Leucop�nica 		v�ase enfermedad espec�fica
Leucoplaquia	702	
� am�gdala	478.29	
� ano	569.49	
� boca	528.6	
� bucal	528.6	
� cuello uterino	622.2
� cuerdas vocales	478.5
� es�fago	530.8
� gingiva	528.6
� labio (boca)	528.6
� laringe	478.79
� lengua	528.6
� paladar	528.6
� peIvis (ri��n)	593.89
� pene (infecciosa)	607.0
� recto	569.49
� ri��n (peIvis)	593.89
� sifil�tica	095.8
� tejido blando oral (incluso lengua) (mucosa)	528.6
� ur�ter (posinfecciosa)	593.89
� uretra (posinfecciosa)	599.8
� �tero	621.8	
� vagina	623.1	
� vejiga (posinfecciosa)	596.8	
� vesical	596.8	
� vulva	624.0	
Leucopolioencefalopat�a	330.0	
Leucoqueratosis	702.8	v�ase adem�s Leucoplaquia
� boca	528.6	
� lengua	528.6	
� paladar por la nicotina	528.7	
Leucorrea (vagina)	623.5	
� debida a tricomonas (vaginal)	131.00	
� tricomoni�sica (Trichomonas vaginalis)	131.00	
Leucosarcoma 	207.8 (M9850/3)	
Leucosis 	(M9800/3)	v�ase Leucemia 
"Lev, enfermedad o s�ndrome de (bloqueo cardiaco completo adquirido)"	426.0	
"Levadura, infecci�n por"	112.9	v�ase adem�s Candidiasis
"Levi, s�ndrome de (enanismo pituitario)"	253.3	
Levocardia (aislada)	746.87	
� con situs inversus	759.3	
Levulosuria	271.2	
"Lewandowski, enfermedad de (primaria)"	017.0	v�ase adem�s Tuberculosis
"Lewandowski-Lutz, enfermedad de (epidermodisplasia verrugosa)"	078.1	
"Leyden, enfermedad de (v�mitos peri�dicos)"	536.2	
"Leyden-Mobius, distrofia de"	359.1	
"Leydig, c�lulas de "		
� carcinoma 	(M8650/3)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, maligna "
�� sitio no especificado 		
��� femenino	183.0	
��� masculino	186.9	
� tumor 	(M8650/1)	
�� benigno 	(M8650/0)	
��� sitio especificado 		"v�ase Neoplasia, por sitio, benigna "
��� sitio no especificado 		
���� femenino	220	
���� masculino	222.0	
�� maligno 	(M8650/3)	
��� sitio especificado 		"v�ase Neoplasia, por sitio, maligna "
��� sitio no especificado 		
���� femenino	183.0	
���� masculino	186.9	
�� sitio especificado 		"v�ase Neoplasia, por sitio, comportamiento incierto "
�� sitio no especificado 		
��� femenino	236.2	
��� masculino	236.4	
"Leydig-Sertoli, tumor de c�lulas de "	(M8631/0)	
� sitio especificado 		"v�ase Neoplasia por sitio, benigna "
� sitio no especificado 		
�� femenino	220	
�� masculino	222.0	
"Libman-Sacks, enfermedad o s�ndrome de   "	710.0 [424.91]	
Licantrop�a	298.9	v�ase adem�s Psicosis
"Lichtheim, enfermedad o s�ndrome de (esclerosis combinada subaguda con anemia perniciosa)"	281.0 [336.2]	
"Licuefacci�n, humor v�treo"	379.21	
Lien migrans	289.59	
Lienter�a	558.9	v�ase adem�s Diarrea
� infecciosa	009.2	
Ligamento 		v�ase enfermedad espec�fica
Ligero o liviano para las fechas (ni�o)   	764.0	
� con signos de desnutrici�n fetal	764.1	
� cuando afecta la atenci�n del embarazo   	656.5	
"Lightwood, enfermedad o s�ndrome de (acidosis tubular renal)"	588.8	
"Lignac (-de Toni) (-Fanconi) (Debr�), s�ndrome de (cistinosis)"	270.0	
"Lignac (-Fanconi), s�ndrome de (cistinosis)   "	270.0	
"Lignac, enfermedad de (cistinosis)"	270.0	
"L�gnea, tiroiditis"	245.3	
"Likoff, s�ndrome de (angina en mujeres menop�usicas)"	413.9	
Limitaci�n de movimiento de articulaci�n 	719.5	"v�ase adem�s Anquilosamiento, articulaci�n"
� sacroil�aca	724.6	
Limitada 		
"� ducci�n, ojo NCOC"	378.63	
� reserva cardiaca 		"v�ase Enfermedad, coraz�n"
Limite 		
� esquizofrenia	295.5	v�ase adem�s Esquizofrenia  
� funcionamiento intelectual	V62.89	
� peIvis	653.1	
�� cuando obstruye el parto	660.1	
��� que afecta al feto o al reci�n nacido   	763.1	
� psicosis	295.5	v�ase adem�s Esquizofrenia  
�� de la infancia	299.8	"v�ase adem�s Psicosis, infancia"
"L�mite, dextrinosis"	271.0	
Limpieza de abertura artificial	V55.9	"v�ase adem�s Atenci�n a, abertura artificial"
Limpieza de apertura artificial	V55.9	v�ase adem�s Atenci�n de apertura artificial
"Lindau (-von Hippel), enfermedad de (angiomatosis retinocerebelosa)"	759.6	
"Lindau, enfermedad de (angiomatosis retinocerebral)"	759.6	
L�nea c�rnea senil	371.41	
L�neas de 		
� Beau (surcos transversales en u�as de la mano)	703.8	
� Harris	733.91	
� Hudson-Stahli	371.11	
� Stahli	371.11	
Linfadenitis	289.3	
� con 		
�� aborto 		"v�ase Aborto, por tipo, con septicemia "
�� embarazo ect�pico	639.0	v�ase adem�s categor�as 633.0-633.9
�� embarazo molar	639.0	v�ase adem�s categor�as 630-632
� aguda	683	
�� mesent�rica	289.2	
� chancroidea (cong�nita)	099.0	
� cr�nica	289.1	
�� mesent�rica	289.2	
"� cualquier sitio, salvo mesenterio"	289.3	
�� aguda	683	
�� cr�nica	289.1	
�� mesent�rica (aguda) (cr�nica) (no espec�fica) (subaguda)	289.2	
�� subaguda	289.1	
��� mesent�rica	289.2	
� debida a 		
�� antracosis (ocupacional)	500	
�� Brugia (Wuchereria) malayi	125.1	
�� difteria (toxina)	032.89	
�� linfogranuloma ven�reo	099.1	
�� Wuchereria bancrofti	125.0	
� dermatop�tica	695.89	
� despu�s de 		
�� aborto	639.0	
�� embarazo ect�pico o molar	639.0	
� estreptoc�cica	683	
� generalizada	289.3	
� gonorreica	098.89	
� granulomatosa	289.1	
� infecciosa 	683	
"� mama, puerperal, posparto"	675.2	
� mesent�rica (aguda) (cr�nica) (no espec�fica) (subaguda)	289.2	
�� debida al bacilo t�fico	002.0	
�� tuberculosa	014.8	v�ase adem�s Tuberculosis
� micobacteriana	031.8	
� pi�gena	683	
� purulenta	683	
� regional	078.3	
� s�ptica	683	
� sifil�tica (precoz) (secundaria)	091.4	
�� tard�a	095.8	
"� subaguda, sitio no especificado"	289.1	
� supurativa	683	
� tuberculosa		"v�ase Tuberculosis, ganglio linf�tico "
� ven�rea	099.1	
"Linfadenoideo, bocio"	245.2	
Linfadenopat�a (general)	785.6	
� debida a toxoplasmosis (adquirida)	130.7	
�� cong�nita (activa)	771.2	
Linfadenopat�a asociada a virus (enfermedad) (infecci�n) 		v�ase Virus de inmunodeficiencia humana (enfermedad (infecci�n)
Linfadenosis	785.6	
� aguda 	075	
Linfangiectasia	457.1	
� conjuntiva	372.8	
� escroto	457.1	
� posinfecciosa	457.1	
"Linfangiect�sica, elefantiasis, no fil�rica"	457.1	
Linfangioendotelioma 	228.1 (M9170/0)	
� maligno 	(M9170/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
Linfangioma 	228.1 (M9170/0)	
� capilar 	228.1 (M9171/0)	
� cavernoso 	228.1 (M9172/0)	
� maligno 	(M9170/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� qu�stico 	228.1 (M9173/0)	
Linfangiomioma 	228.1 (M9174/0)	
Linfangiomiomatosis 	(M9174/1)	"v�ase Neoplasia, tejido conjuntivo, comportamiento incierto "
Linfangiosarcoma 	(M9170/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
Linfangitis	457.2	
� con 		
�� aborto 		"v�ase Aborto, por tipo, con septicemia "
�� absceso 		"v�ase Absceso, por sitio "
�� celulitis 		"v�ase Absceso, por sitio "
�� embarazo ect�pico	639.0	v�ase adem�s categor�as 633.0-633.9
�� embarazo molar	639.0	v�ase adem�s categor�as 630-632
� aguda (con absceso o celulitis)	682.9	
�� sitio especificado 		"v�ase Absceso, por sitio "
� chancroidea	099.0	
� cr�nica (cualquier sitio)	457.2	
� debida a 		
�� Brugia (Wuchereria) malayi	125.1	
�� Wuchereria (bancrofti)	125.0	
� despu�s de 		
�� aborto	639.0	
�� embarazo ect�pico o molar	639.0	
"� estrumosa, tuberculosa"	017.2	v�ase adem�s Tuberculosis
� gangrenosa	457.2	
"� mama, puerperal, posparto"	675.2	
� pene 		
�� aguda	607.2	
�� gonoc�cica (aguda)	098.0	
��� cr�nica o de duraci�n de 2 meses o m�s	098.2	
"� puerperal, posparto, alumbramiento"	670	
� subaguda (cualquier sitio)	457.2	
� tuberculosa 		"v�ase Tuberculosis, ganglio linf�tico"
Linf�tico 		
� escroto	125.9	"v�ase adem�s Infestaci�n, fiI�rica"
� ganglio o n�dulo 		v�ase enfermedad espec�fica 
� vaso 		v�ase enfermedad espec�fica
Linfatismo	254.8	
� escrofuloso	017.2	v�ase adem�s Tuberculosis
Linfectasia	457.1	
Linfedema	457.1	v�ase adem�s Elefantiasis
� adquirido (cr�nica)	457.1	
� cong�nito	757.0	
� hereditario 		
�� cr�nico	757.0	
�� idiop�tico	757.0	
� precoz	457.1	
� quir�rgico NCOC	997.9	
�� posmastectom�a (s�ndrome)	457.0	
� secundario	457.1	
Linfobl�stico 		v�ase enfermedad espec�fica
Linfoblastoma (difuso) 	200.1 (M9630/3)	
� folicular gigante 	202.0 (M9690/3)	
� macrofolicular 	202.0 (M9690/3)	
"Linfoblastosis, benigna aguda "	075	
Linfocele	457.8	
Linfocitemia	288.8	
Linfoc�tico/a 		v�ase adem�s enfermedad espec�fica 
� corioencefalitis (aguda) (s�rica)	049.0	
� coriomeningitis (aguda) (s�rica)	049.0	
Linfocitoma (difuso) (maligno) 	200.1 (M9620/3)	
Linfocitomatosis 	200.1 (M9620/3)	
Linfocitopenia	288.8	
Linfocitosis (sintom�tica)	288.8	
� infecciosa (aguda)	078.89	
Linfoepitelioma 	(M8082/3)	"v�ase Neoplasia, por sitio, maligna"
Linfogranuloma (maligno) 	201.9 (M9650/3)	
� inguinal	099.1	
� ven�reo (cualquier sitio)	099.1	
�� con estrechamiento del recto	099.1	
Linfogranulomatosis (maligna) 	201.9 (M9650/3)	
� benigna (sarcoide de Boeck) (de Schaumann)	135	
"� Hodgkin, de "	201.9 (M9650/3)	
Linfohemangioma 	(M9120/0)	"v�ase Hemangioma, por sitio"
Linfoide 		v�ase enfermedad espec�fica
Linfoleucoblastoma 	207.8 (M9850/3)	
Linfoleucosarcoma 	207.8 (M9850/3)	
Linfoma (maligno) 	202.8 (M9590/3)	
� benigno 	(M9590/0)	"v�ase Neoplasia, por sitio, benigna "
"� Burkitt, tipo de (linfobl�stico) (no diferenciado) "	200.2 (M9750/3)	
"� Castleman, de (hiperplasia de n�dulos linf�ticos mediast�nicos)"	785.6	
� c�lulas grandes 	200.0 (M9640/3)	
�� nodular 	200.0 (M9642/3)	
�� tipo c�lula pleom�rfica 	200.0 (M9641/3)	
"� c�lulas peque�as y grandes, mixto (difuso)  "	200.8 (M9613/3)	
�� folicular 	202.0 (M9691/3)	
�� nodular 	202.0 (M9691/3)
� c�lulas primitivas o estaminales (tipo)  	202.8 (M9601/3)
� c�lulas reticulares (tipo) 	200.0 (M9640/3)
� centrobl�stico-centroc�tico 	
�� difuso 	202.8 (M9614/3)
�� folicular 	202.0 (M9692/3)
� centroc�tico 	202.8 (M9622/3)
� compuesto 	200.8 (M9613/3)
� difuso NCOC 	202.8 (M9590/3)
� folicular (gigante) 	202.0 (M9690/3)
�� c�lulas del centro (difuso) 	202.8 (M9615/3)
��� hendidas (difuso) 	202.8 (M9623/3)
���� foliculares 	202.0 (M9695/3)
��� no hendidas (difuso) 	202.8 (M9633/3)
���� foliculares 	202.0 (M9698/3)
�� centrobl�stico-centroc�tico 	202.0 (M9692/3)
�� linfoc�tico 		
��� diferenciaci�n intermedia 	202.0 (M9694/3)	
��� poco diferenciado 	202.0 (M9696/3)	
�� mixto (c�lulas peque�as y grandes) (linfoc�tico-histioc�tico) (tipo celular)   	202.0 (M9691/3)	
�� tipo centrobl�stico 	202.0 (M9697/3)	
� germinoc�tico 	202.8 (M9622/3)	
"� gigante, folicular o fol�culos "	202.0 (M9690/3)	
� histioc�tico (difuso) 	200.0 (M9640/3)	
�� nodular 	200.0 (M9642/3)	
�� tipo c�lulas polim�rficas 	200.0 (M9641/3)	
"� Hodgkin, de "	201.9 (M9650/3)	"v�ase adem�s Enfermedad, de Hodgkin"
� inmunobl�stico (tipo) 	200.8 (M9612/3)	
� linfobl�stico (difuso) 	200.1 (M9630/3)	
�� tipo 		
��� Burkitt 	200.2 (M9750/3)	
��� c�lulas con circunvoluciones 	202.8 (M9602/3)	
� linfoc�tico (difuso) (tipo celular) 	200.1 (M9620/3)
"�� con diferenciaci�n plasmacitoide, difuso   "	200.8 (M9611/3)
�� bien diferenciado 	200.1 (M9620/3)
��� folicular 	202.0 (M9693/3)
��� nodular 	202.0 (M9693/3)
�� diferenciaci�n intermedia (difuso) 	200.1 (M9621/3)
��� folicular 	202.0 (M9694/3)
��� nodular 	202.0 (M9694/3)
�� nodular 	202.0 (M9690/3)
�� poco diferenciado (difuso) 	202.1 (M9630/3)
��� folicular 	202.0 (M9696/3)
��� nodular 	202.0 (M9696/3)
"� linfoc�tico-histioc�tico, mixto (difuso) "	200.8 (M9613/3)
�� folicular 	202.0 (M9691/3)
�� nodular 	202.0 (M9691/3)
� macrofolicular 	200.0 (M9690/3)
� no diferenciado (tipo celular) (no de Burkitt) 	202.8 (M9600/3)
�� tipo de Burkitt 	200.2 (M9750/3)
� nodular 	202.0 (M9690/3)
�� histioc�tico 	200.0 (M9642/3)
�� linfoc�tico 	202.0 (M9690/3)
��� diferenciaci�n intermedia 	202.0 (M9694/3)
��� poco diferenciado 	202.0 (M9696/3)
�� mixto (c�lulas peque�as y grandes) (linfoc�tico-histioc�tico) (tipo celular) 	202.0 (M9691/3)
� tipo 	
�� c�lulas con circunvoluciones (linfobl�stico) 	202.8 (M9602/3)
�� c�lulas mixtas (difuso) 	200.8 (M9613/3)
��� folicular 	202.0 (M9691/3)
��� nodular 	202.0 (M9691/3)
�� centrobl�stico (difuso) 	202.8 (M9632/3)
��� folicular 	202.0 (M9697/3)
�� linfoplasmacitoide 	200.8 (M9611/3)
�� linfosarcoma 	200.1 (M9610/3)	
�� no de Hodgkin NCOC 	202.8 (M9591/3)	
Linfomatosis 	(M9590/3)	v�ase adem�s  Linfoma 
� granulomatosis	099.1	
Linfopat�a ven�rea	099.1	
Linfopenia	288.8	
� familiar	279.2	
Linforeticulosis benigna (de inoculaci�n)  	078.3	
Linforrea	457.8	
Linfosarcoma 	200.1 (M9610/3)	
� c�lulas reticulares 	200.0 (M9640/3)	
� difuso 	200.1 (M9610/3)	
�� con diferenciaci�n plasmacitoide 	200.8 (M9611/3)	
�� linfoplasmoc�tico 	200.8 (M9611/3)	
� folicular (gigante) 	202.0 (M9690/3)	
�� linfobl�stico 	202.0 (M9696/3)	
"�� linfoc�tico, diferenciaci�n intermedia   "	202.0 (M9694/3)
�� tipo celular mixto 	202.0 (M9691/3)
� gigante folicular 	202.0 (M9690/3)
"� Hodgkin, de "	201.9 (M9650/3)
� inmunobl�stico 	200.8 (M9612/3)
� linfobl�stico (difuso) 	200.1 (M9630/3)
�� folicular 	202.0 (M9696/3)
�� nodular 	202.0 (M9696/3)
� linfoc�tico (difuso) 	200.1 (M9620/3)
�� diferenciaci�n intermedia (difuso) 	200.1 (M9621/3)
��� folicular 	202.0 (M9694/3)
��� nodular 	202.0 (M9694/3)
� nodular 	202.0 (M9690/3)
�� linfobl�stico 	202.0 (M9696/3)
"�� linfoc�tico, diferenciaci�n intermedia "	202.0 (M9694/3)
�� tipo celular mixto 	202.0 (M9691/3)
� prolinfoc�tico 	200.1 (M9631/3)	
� tipo celular mixto (difuso) 	200.8 (M9613/3)	
�� folicular 	202.0 (M9691/3)	
�� nodular 	202.0 (M9691/3)	
Linfostasis	457.8	
Lingual 		v�ase adem�s enfermedad espec�fica 
� tiroides	759.2	
Linitis (g�strica)	535.4	
� pl�stica 	151.9 (M8142/3)	
Lioderma esencial (con melanosis y telangiectasia)	757.33	
Lipalgia	272.8	
Lipedema 		v�ase Edema
Lipeman�a	296.2	v�ase adem�s Melancol�a
Lipemia	272.4	v�ase adem�s Hiperlipidemia
"� retina, retiniana"	272.3	
Lipidosis	272.7	
� cefalina	272.7	
� cerebral (infantil) (juvenil) (tard�a)	330.1	
� cerebrorretiniana	330.1 [362.71]	
� cerebr�sido	272.7	
� cerebrospinal	272.7	
� colesterol	272.7	
� diab�tica	250.8 [272.7]	
� dist�pica (hereditaria)	272.7	
� glucol�pido	272.7	
� hepatosplenomeg�lica	272.3	
� hereditaria dist�pica	272.7	
� inducida qu�micamente	272.7	
� sulf�tido	330.0	
Lipoadenoma 	(M8324/0)	"v�ase Neoplasia, por sitio, benigna"
Lipoblastoma 	(M8881/0)	"v�ase Lipoma, por sitio"
Lipoblastomatosis 	(M8881/0)	"v�ase Lipoma, por sitio"
Lipocondrodistrofia	277.5	
"Lipocr�mica, histiocitosis (familiar)"	288.1	
Lipodistrofia progresiva	272.6	
� insulina	272.6	
� intestinal	040.2	
Lipofagocitosis	289.8	
Lipofibroma 	(M8851/0)	"v�ase Lipoma, por sitio"
Lipoglucoproteinosis	272.8	
Lipogranuloma esclerosante	709.8	
Lipogranulomatosis (diseminada)	272.8	
� ri��n	272.8	
Lipoide 		v�ase adem�s enfermedad espec�fica 
� histiocitosis	272.7	
�� esencial	272.7	
� nefrosis	581.3	v�ase adem�s Nefrosis
� proteinosis de Urbach	272.8	
Lipoidemia	272.4	v�ase adem�s Hiperlipidemia
Lipoidosis	272.7	v�ase adem�s Lipidosis
Lipoma 	214.9 (M8850/0)	
� cara	214.0	
� c�lulas fusiformes 	(M8857/0)	"v�ase Lipoma, por sitio "
� cord�n esperm�tico	214.4	
� est�mago	214.3	
� fetal 	(M8881/0)	"v�ase adem�s Lipoma, por sitio "
�� c�lulas adiposas 	(M8880/0)	"v�ase Lipoma, por sitio "
� gl�ndula tiroidea	214.2	
� infiltrante 	(M8856/0)	"v�ase Lipoma, por sitio "
� intraabdominal	214.3	
� intramuscular 	(M8856/0)	"v�ase Lipoma, por sitio "
� intrator�cica	214.2	
� mama (piel)	214.1	
� mediastino	214.2	
� m�sculo	214.8	
� peritoneo	214.3	
� piel	214.1	
�� cara	214.0	
� retroperitoneo	214.3	
� ri��n	214.3	
� tejido subcut�neo	214.1	
�� cara	214.0	
� timo	214.2	
Lipomatosis (dolorosa)	272.8	
� fetal 	(M8881/0)	"v�ase Lipoma, por sitio "
"� Launois-Bensaude, de"	272.8	
Lipomiohemangioma 	(M8860/0)	
� sitio especificado 		"v�ase Neoplasia, tejido conjuntivo, benigna "
� sitio no especificado	223.0	
Lipomioma 	(M8869/0)	
� sitio especificado 		"v�ase Neoplasia, tejido conjuntivo, benigna "
� sitio no especificado	223.0	
Lipomixoma 	(M8852/0)	"v�ase Lipoma, por sitio"
Lipomixosarcoma 	(M8852/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
Lipoproteinemia (alfa)	272.4	
� beta 		
�� abierta	272.2	
�� hiper	272.2	
�� movible o flotante	272.2	
Lipoproteinosis (R�ssle-Urbach-Wiethe)	272.8	
Liposarcoma 	(M8850/3)	"v�ase adem�s Neoplasia, tejido conjuntivo, maligna "
� c�lulas redondas 	(M8853/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� embrionario 	(M8852/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� mixoide 	(M8852/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� pleom�rfico 	(M8854/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� tipo 		
�� bien diferenciado 	(M8851/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� diferenciado 	(M8851/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� mixto 	(M8855/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
Liposinovitis prepatelar (r�tula)	272.8	
"Lipschutz, enfermedad o �lcera de"	616.50	
Lipuria	791.1	
� bilharziasis	120.0	
Liquen	697.9	
� albus	701.0	
� anular	695.89	
� atr�fico	701.0	
� c�rneo obtuso	698.3	
� escleroso (y atr�fico)	701.0	
� escrofuloso (primario)	017.0	v�ase adem�s Tuberculosis
� espinuloso	757.39	
�� mic�tico	117.9	
� estriado	697.8	
� mixedematoso	701.8	
� n�tido	697.1	
� pilaris	757.39	
�� adquirido	701.1	
� plano (agudo) (cr�nico) (hipertr�fico) (verrugoso)	697.0	
�� escleroso (y atr�fico)	701.0	
�� morfeo	701.0	
� planopilaris	697.0	
� rojo	696.4	
�� acuminado	696.4	
�� moniliforme	697.8	
�� obtuso c�rneo	698.3	
�� plano	697.0	
"�� Wilson, de"	697.0	
� ruber 		"v�ase Liquen, rojo "
� simple (de Vidal)	698.3	
�� circunscrito	698.3	
�� cr�nico	698.3	
� urticante	698.2	
Liquenificaci�n	698.3	
� nodular	698.3	
"Liquenoide, tuberculosis (primario)"	017.0	v�ase adem�s Tuberculosis
L�quido 		
� abdomen	789.5	
� articulaci�n	719.0	"v�ase adem�s Efusi�n, articulaci�n"
� cavidad 		
�� peritoneal	789.5	
�� pleural	511.9	"v�ase adem�s Pleures�a, con efusi�n"
� coraz�n	428.0	"v�ase adem�s Fallo, coraz�n, congestivo"
� pecho	511.9	"v�ase adem�s Pleures�a, con efusi�n"
� p�rdida (aguda)	276.5	
�� con 		
��� hipernatremia	276.0	
��� hiponatremia	276.1	
� pulm�n 		"v�ase adem�s Edema, pulm�n "
�� enquistado	511.8	
� retenci�n	276.6	
Lisa	071	
Lisencefalia	742.2	
"Lissauer, par�lisis de"	094.1	
"Lista de espera, persona en"	V63.2	
� sujeta a investigaci�n por agencia social	V63.8	
Listerelosa	027.0	
Listeriosis	027.0	
� cong�nita	771.2	
� fetal	771.2	
� sospecha de da�o fetal que afecta la atenci�n del embarazo	655.4	
Listerioso	027.0	
Litemia	790.6	
Litiasis 		v�ase adem�s C�lculo 
� hep�tica (conducto) 		v�ase Coledocolitiasis
� urinaria	592.9	
Litigio	V62.5	
Litopedi�n	779.9	
� que afecta a la atenci�n del embarazo	656.9	
Litosis (ocupacional)	502	
� con tuberculosis 		"v�ase Tuberculosis, pulmonar"
"Little, enfermedad de "		"v�ase Par�lisis, cerebral"
"Littre, de "		
� gl�ndula 		v�ase enfermedad espec�fica 
� hernia 		"v�ase Hernia, de Littre"
Littritis	597.89	v�ase adem�s Uretritis
Lituria	791.9	
Livedo	782.61	
� annularis	782.61	
� racemosa	782.61	
� reticular	782.61	
"L�vida, asfixia "		
� reci�n nacido	768.6	
Llaga o �lcera 		
� blanda	099.0	
� boca	528.9	
�� chancro	528.2	
�� debida a dentadura postiza	528.9	
� Delhi	085.1	
� desierto	707.9	"v�ase adem�s Ulcera, piel"
� garganta	462	
�� con influenza o gripe	487.1	
�� aguda	462	
"�� cl�rigos, de los"	784.49
�� Coxsackie (virus de)	074.0
�� cr�nica	472.1
�� dift�rica	032.0
�� epid�mica	034.0
�� estreptoc�cica (ulcerativa)	034.0
�� gangrenosa	462
�� herp�tica	054.79
�� influenzal	487.1
�� maligna	462
�� purulenta	462
�� p�trida	462
�� s�ptica	034.0
�� ulcerada	462
�� v�rica NCOC	462
��� Coxsackie	074.0
� Lahore	085.1	
� m�sculo	729.1	
� Naga	707.9	"v�ase adem�s Ulcera, piel"
� ojo	379.99	
� oriental	085.1	
� piel NCOC	709.9	
� presi�n	707.0	
�� con gangrena	707.0 [785.4]	
� tropical	707.9	"v�ase adem�s Ulcera, piel"
� veldt	707.9	"v�ase adem�s Ulcera, piel"
"Llenadores de silos, enfermedad de"	506.9	
"Lloyd, s�ndrome de"	258.1	
Lluvia radiactiva (efecto adverso) NCOC	990	
Loa loa	125.2	
Loasis	125.2	
Lobanillo	706.2	v�ase adem�s Quiste seb�ceo
"Lobo, enfermedad o blastomicosis de"	116.2	
Lobomicosis	116.2	
"Lobotom�a, s�ndrome de"	310.0	
"Lobstein, enfermedad de (huesos fr�giles y escler�tica azul)"	756.51	
Lobulaci�n (cong�nita) 		"v�ase adem�s Anomal�a, tipo especificado NCOC, por sitio "
� bazo	759.0	
"� h�gado, anormal"	751.69	
"� ri��n, fetal"	753.3	
"L�bulo medio derecho, s�ndrome de"	518.0	
"L�bulo, lobular "		v�ase enfermedad espec�fica 
"Local, localizado "		v�ase enfermedad espec�fica 
"Locomotora, ataxia (progresiva)"	094.0	
Locura	298.9	v�ase adem�s Psicosis
� mixedema (agudo)	293.0	
�� subagudo	293.1	
"Locura, loco"	298.9	v�ase adem�s Psicosis
� adolescente	295.9	v�ase adem�s Esquizofrenia
� alternante 	296.7	"v�ase adem�s Psicosis, afectiva, circular"
� confusional	298.9	
�� aguda	293.0	
�� subaguda	293.1	
� delusional	298.9	
"� par�lisis, general"	094.1	
�� progresiva	094.1	
"� paresia, paresis"	094.1	
� senil	290.20	
"L�ffler, de "		
� endocarditis	421.0	
� eosinofilia o s�ndrome	518.3	
� neumon�a	518.3	
� s�ndrome (neumonitis eosin�fila)	518.3	
"L�fgren, s�ndrome de (sarcoidosis) "	135	
Loiasis	125.2
� p�rpado	125.2 [373.6]
Lombrices intestinales (enfermedad) (infecci�n) (infestaci�n)	127.4
"Longitudinales, estr�as o surcos, u�as"	703.8
� cong�nitos	757.5
"Looser (-Debray) -Milkman, s�ndrome de (osteomalacia con seudofracturas)"	268.2
"Lorain, enfermedad o s�ndrome de (enanismo pituitario)"	253.3
"Lorain-Levi, s�ndrome de (enanismo pituitario)"	253.3
Lordosis (adquirida) (postural)	737.20
� cong�nita	754.2
� debida a o asociada con 	
�� enfermedad de Charcot-Marie-Tooth	356.1 [737.42]
�� mucopolisacaridosis	277.5 [737.42]
�� neurofibromatosis	237.7 [737.42]
�� oste�tis 	
��� deformante	731.0 [737.42]
��� fibrosa qu�stica	252.0 [737.42]	
�� osteoporosis	733.00 [737.42]	v�ase adem�s Osteoporosis  
�� poliomielitis	138 [737.42]	v�ase adem�s Poliomielitis  
�� tuberculosis	015.0 [737.42]	v�ase adem�s Tuberculosis
� efecto tard�o de raquitismo	268.1 [737.42]	
� especificada NCOC	737.29	
� poslaminectom�a	737.21	
� posquir�rgica NCOC	737.22	
� raqu�tica	268.1 [737.42]	
� tuberculosa	015.0 [737.42]	v�ase adem�s Tuberculosis
"Loros, fiebre de los"	073.9	
"Louis-Bar, s�ndrome de (ataxia-telangiectasia)"	334.8	
"Louping, mal de"	063.1	
"Lowe(-Terrey-MacLachlan), s�ndrome de (distrofia oculocerebrorrenal)"	270.8	
"Lown (-Ganong)-Levine, s�ndrome de (intervalo P-R corto, complejo QRS normal, y taquicardia supraventricular paroxismal)"	426.81	
"LSD, reacci�n de"	305.3	"v�ase adem�s Abuso, drogas, sin dependencia"
Lucas-Champonnire (bronquitis fibrinosa)	466.0	
"Lucey-Driscoll, s�ndrome de (ictericia debida a conjugaci�n retardada)"	774.30	
"Ludwig, de "		
� angina	528.3	
� enfermedad (celulitis submaxilar)	528.3	
"L�es (ven�rea), lu�tica "		v�ase S�filis
"Luetscher, s�ndrome de (deshidrataci�n)"	276.5	
Lumbago	724.2	
"� debido a desplazamiento, disco intervertebral"	722.10	
Lumbalgia	724.2	
"� debida a desplazamiento, disco intervertebral"	722.10	
Lumbar 		v�ase adem�s enfermedad espec�fica 
� s�ndrome	724.2	
Lumbar 		v�ase condici�n
"Lumbarizaci�n, v�rtebra"	756.15	
Lunatomalacia	732.3	
"Lunes, fiebre de los "	504	
Lupoide (miliar) de Boeck 	135	
Lupus (exedens) (de Hilliard) (miliaris disseminatus faciei) (vulgar)	017.0	v�ase adem�s Tuberculosis
"� Cazenave, de (eritematoso)"	695.4	
� discoide (local)	695.4	
� diseminado	710.0	
� eritematodes (discoide) (local)	695.4	
� eritematoso (discoide) (local)	695.4	
�� diseminado	710.0	
�� p�rpado	373.34	
�� sist�mico o generalizado	710.0	
��� con implicaci�n pulmonar	710.0 [517.8]	
��� inhibidor (presencia de)	286.5	
� hidralazina 		
�� sobredosis o ingesti�n o administraci�n de sustancia incorrecta	972.6	
�� sustancia correcta administrada de forma correcta	695.4	
� nefritis	710.0 [583.81]	
�� cr�nico	710.0 [582.81]	
"� no tuberculoso, no diseminado"	695.4	
� p�rpado	017.0 [373.4]	v�ase adem�s Tuberculosis
� pernio (Besnier)	135	
� tuberculoso	017.0	v�ase adem�s Tuberculosis
�� p�rpado	017.0 [373.4]	v�ase adem�s Tuberculosis
"Luschka, enfermedad de articulaci�n de"	721.90	
Luteinoma 	220 (M8610/0)	
"Lutembacher, enfermedad o s�ndrome de (defecto septal con estenosis mitral)"	745.5	
Luteoma 	220 (M8610/0)	
"Luto, duelo"	V62.82	
� como reacci�n de ajuste	309.0	
"Lutz-Miescher, enfermedad de (elastosis perforante serpiginosa)"	701.1	
"Lutz-Splendore-de Almeida, enfermedad de (blastomicosis brasile�a)"	116.1	
Luxaci�n (articulaci�n) (cerrada) (desplazamiento) (simple) (subluxaci�n)	839.8	
� con fractura 		"v�ase Fractura, por sitio "
� abierta (compuesta) NCOC	839.9	
� acromioclavicular (articulaci�n) (cerrada)  	831.04	
�� abierta	831.14	
� antebrazo (cerrada)	839.8	
�� abierta	839.9	
� antigua 		"v�ase Luxaci�n, recurrente "
� articulaci�n NCOC (cerrada)	839.8	
�� abierta	839.9	
�� patol�gica 		"v�ase Luxaci�n, patol�gica "
�� recurrente 		"v�ase Luxaci�n, recurrente "
� astr�galo (cerrada)	837.0	
�� abierta	837.1	
� atlanto-axial (cerrada)	839.01	
�� abierta	839.11	
� atlas (cerrada)	839.01	
�� abierta	839.11
� axis (cerrada)	839.02
�� abierta	839.12
� Bell-Daly	723.8
� brazo (cerrada)	839.8
�� abierta	839.9
� cadera (cerrada)	835.00
�� abierta	835.10
�� anterior	835.03
��� abierta	835.13
��� obturador	835.02
���� abierta	835.12
�� cong�nita (unilateral)	754.30
��� con subluxaci�n de la otra cadera	754.35
��� bilateral	754.31
�� posterior	835.01
��� abierta	835.11	
�� recurrente	718.35	
"� c�psula, articulaci�n "		"v�ase Luxaci�n, por sitio "
� carpiano (hueso)		"v�ase Luxaci�n, mu�eca "
� carpometacarpiano (articulaci�n) (cerrada)	833.04	
�� abierta	833.14	
� cart�lago (articulaci�n) 		"v�ase adem�s Luxaci�n, por sitio "
�� rodilla 		"v�ase Desgarre, menisco "
� cart�lago costal (cerrada)	839.69	
�� abierta	839.79	
"� cart�lago semilunar, rodilla "		"v�ase Desgarre, menisco "
� cart�lago septal (nariz) (cerrada)	839.69	
�� abierta	839.79	
� cart�lago tiroideo (cerrada)	839.69	
�� abierta	839.79	
� cart�lago xifoideo (cerrada)	839.61	
�� abierta	839.71	
"� cervical, cervicodorsal, o cervicotor�cica (espina) (v�rtebra) "		"v�ase Luxaci�n, v�rtebra, cervical "
� clav�cula (cerrada)	831.04	
�� abierta	831.14	
� c�ccix (cerrada)	839.41	
�� abierta	839.51	
� codo (cerrada)	832.00	
�� abierta	832.10	
�� anterior (cerrada)	832.01	
��� abierta	832.11	
�� cong�nita	754.89	
�� divergente (cerrada)	832.09	
��� abierta	832.19	
�� lateral (cerrada)	832.04	
��� abierta	832.14	
�� medial (cerrada)	832.03	
��� abierta	832.13	
�� posterior (cerrada)	832.02	
��� abierta	832.12	
�� recurrente	718.32	
�� tipo especificado NCOC	832.09	
��� abierta	832.19	
� compuesta (abierta) NCOC	839.9	
� condrocostal 		"v�ase Luxaci�n, costocondral "
� cong�nita NCOC	755.8	
�� cadera	754.30	"v�ase adem�s Luxaci�n, cadera, cong�nita"
