Parascarlatina	057.8	
Parasitario 		v�ase adem�s enfermedad espec�fica 
� enfermedad NCOC	136.9	"v�ase adem�s Infestaci�n, parasitaria"
�� contacto	V01.8	
�� exposici�n a	V01.8	
�� intestinal NCOC	129	
�� piel NCOC	134.9	
� estomatitis	112.0	
� gemelo	759.4	
� sicosis	110.0	
�� barba	110.0	
�� cuero cabelludo	110.0	
Parasitismo NCOC	136.9	
� especificado 		v�ase Infestaci�n 
� intestinal NCOC	129	
� piel NCOC	134.9	
Parasitofobia	300.29	
"Paraspadias, paraspadia"	752.8	
Paraspasmo del facial	351.8	
Paratiflitis	541	v�ase adem�s Apendicitis 
Paratifoidea (fiebre) 		"v�ase Fiebre, paratifoidea"
Paratifus 		"v�ase Fiebre, paratifoidea"
"Paratiroidea, gl�ndula "		v�ase enfermedad espec�fica
Paratiroiditis (autoinmune)	252.1	
"Paratiropriva, tetania"	252.1	
Paratracoma	077.0	
"Parauretral, conducto"	753.8	
Parauretritis	597.89	
� gonoc�cica (aguda)	098.0	
�� cr�nica o de duraci�n de 2 meses o m�s	098.2	
Paravacuna NCOC	051.9	
� n�dulos de los orde�adores	051.1	
Paravaginitis	616.10	v�ase adem�s Vaginitis
Pared abdominal 		v�ase enfermedad espec�fica
Parencefalitis	323.9	v�ase adem�s Encefalitis
� efecto tard�o 	326	v�ase categor�a 
Parergasia	298.9	
"Paresia, paresis"	344.9	v�ase adem�s Par�lisis
� acomodaci�n	367.51	
� cerebro o enc�falo 		"v�ase Par�lisis, cerebro "
� coraz�n	428.9	"v�ase adem�s Fallo, coraz�n"
� del insano (demencia)	094.1	
� efecto tard�o 		"v�ase Par�lisis, efecto tard�o "
� est�mago	536.8	
� general	094.1	
�� cerebral	094.1	
�� del insano (demencia)	094.1	
"�� detenida, estacionaria"	094.1	
�� enc�falo	094.1	
�� juvenil	090.40	
��� remisi�n. remitencia	090.49	
�� progresiva	094.1	
"�� remisi�n, remitencia (sostenida)"	094.1	
�� tab�tica	094.1	
� infantil	045.9	v�ase adem�s Poliomielitis
� intestino o colon	560.1	v�ase adem�s lleo
� juvenil	090.40	
� lu�tica (general)	094.1	
� m�sculo extr�nseco (ojo)	378.55	
� progresiva perif�rica	356.9	
� senil NCOC	344.9	
� seudohipertr�fica	359.1	
� sifil�tica (general)	094.1	
�� cong�nita	090.40	
"� transitoria, de miembro"	781.4	
� vejiga (esf�nter) (esp�stica)	596.5	"v�ase adem�s Par�lisis, vejiga"
�� tab�tica	094.0	
"� vesical, vejiga (esf�nter) NCOC"	596.5	
Parestesia	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
"� Berger, de (parestesia de miembro inferior)"	782.0	
� Bernhardt. de	355.1	
"� Magnan, de"	782.0	
Par�tico 		v�ase enfermedad espec�fica
"Parinaud, de "		
� conjuntivitis	372.02	
� oftalmoplej�a	378.81	
� s�ndrome (par�lisis de la mirada conyugada hacia arriba)	378.81	
�� oculoglandular	372.02	
"Parkes Weber y Dimitri, s�ndrome de (angiomatosis encefalocut�nea)"	759.6	
"Parkinson, enfermedad, s�ndrome o temblor de "		v�ase Parkinsonismo
Parkinsonismo (arterioscler�tico) (idiop�tico) (primario)	332.0	
� asociado con hipotensi�n ortost�sico (idiop�tico) (sintom�tico)	333.0	
� debido a drogas	332.1	
� secundario	332.1	
� sifil�tico	094.82	
"Paro, parado "		
� cardiaco	427.5	
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con complicaci�n especificada NCOC "
��� embarazo ect�pico	639.8	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.8	v�ase adem�s categor�as 630-632
�� cuando complica 		
��� anestesia 		
���� obst�trica	668.1	
���� sobredosis o administraci�n de sustancia incorrecta	968.4	
����� anestesia especificada 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos "
���� sustancia correcta administrada de forma correcta	427.5	
��� cirug�a (no terap�utico) (terap�utico)	997.1	
��� embarazo molar o ect�pico	639.8	
��� parto (ces�rea) (instrumental)	669.4	
�� despu�s de 		
��� aborto	639.8	
��� embarazo ect�pico o molar	639.8	
�� feto o reci�n nacido	779.8	
�� posoperatorio (inmediato)	997.1	
��� efecto a largo plazo de cirug�a cardiaca	429.4	
� cardiorrespirarorio	427.5	"v�ase adem�s Paro, cardiaco"
� coraz�n 		"v�ase Paro, cardiaco "
� cualquier plano de pelvis 		
�� cuando complica el parto	660.1	
��� que afecta al feto o al reci�n nacido	763.1	
� del desarrollo o crecimiento 		
�� anillos traqueales	748.3	
�� feto	764.9	
��� que afecta la atenci�n del embarazo	656.5	
�� huesos	733.91	
�� infantil	783.4	
� epifisario	733.91	
� fase activa de parto	661.1	
�� que afecta al feto o al reci�n nacido	763.7	
� granulopoyesis	288.0	
� m�dula �sea	284.9	"v�ase adem�s Anemia, apl�stica"
� profundo transversal	660.3	
�� que afecta al feto o al reci�n nacido	763.1	
� respiratorio	799.1	
�� reci�n nacido	770.8	
� seno	426.6	
� transversal (profundo)	660.3	
�� que afecta al feto o al reci�n nacido	763.1	
Parodontitis	523.4	
Paroniquia (con linfangitis)	681.9	
� Candida (cr�nica)	112.3	
� cr�nica	681.9	
�� dedo de 		
��� mano	681.02	
��� pie	681.11	
�� por Candida	112.3	
� dedo de 		
�� mano	681.02	
�� pie	681.11	
� tuberculosa (primaria)	017.0	v�ase adem�s Tuberculosis
Parorexia NCOC	307.52	
� hist�rica	300.11	
Parosmia	781.1	
� psic�gena	306.7	
"Par�tida, gl�ndula "		v�ase enfermedad espec�fica
Parotiditis	072.9	
� con complicaci�n	072.8	
�� tipo especificado NCOC	072.79	
� encefalitis	072.2	
� hepatitis	072.71	
� meningitis (as�ptica)	072.1	
� meningoencefalitis	072.2	
� ooforitis	072.79	
� orquitis	072.0	
� pancreatitis	072.3	
� polineuropat�a	072.72	
� vacunaci�n profil�ctica (contra)	V04.6	
Parotiditis	527.2	
� al�rgica	527.2	
� cr�nica	527.2	
� epid�mica	072.9	v�ase adem�s Paperas
� infecciosa	072.9	v�ase adem�s Paperas
� no infecciosa	527.2	
� no paperas	527.2	
� posoperatoria	527.2	
� purulenta	527.2	
� quir�rgica	527.2	
� s�ptica	527.2	
� supurativa (aguda)	527.2	
� t�xica	527.2	
� t�xica no espec�fica	527.2	
Parotitis 		v�ase Parotiditis
Paroxismal 		v�ase adem�s enfermedad espec�fica 
� disnea (nocturna)	786.09	
Parpadeo mandibular	374.43	
� cong�nita	742.8	
"Parrot, enfermedad de (osteocondritis sifil�tica)"	090.0	
"Parry, enfermedad o s�ndrome de (bocio exoft�lmico)"	242.0	
"Parry-Romberg, s�ndrome de"	349.89	
Pars planitis	363.21	
"Parson, enfermedad de (bocio exoft�lmico)"	242.0	
"Parsonage-Aldren-Turner, s�ndrome de"	353.5	
"Parsonage-Turner, s�ndrome de"	353.5	
Parto 		
� amenaza de prematuridad	644.2	
� caso completamente normal 	650	v�ase categor�a 650
� ces�rea (debida a)	669.7	
�� abruptio placentae	641.2	
�� acci�n uterina incoordinada	661.4	
�� anormal 		
��� cuello uterino	654.6	
��� �rganos o tejidos pelvianos	654.9
��� partes blandas (de peIvis)	654.9
��� pelvis (�sea) (mayor) NCOC	653.0
��� presentaci�n o posici�n	652.9
���� en gestaci�n m�ltiple	652.6
"��� tama�o, feto"	653.5
"��� �tero, cong�nito"	654.0
��� vagina	654.7
��� vulva	654.8
�� anterior 	
��� ces�rea	654.2
��� cirug�a (sobre) 	
���� cuello uterino	654.6
���� ginecol�gica NCOC	654.9
���� �tero NCOC	654.9
����� anterior a ces�rea	654.2
���� vagina	654.7
"�� anteversi�n, cuello uterino o �tero"	654.4
"�� aton�a, �tero"	661.2
"�� cara, presentaci�n de"	652.4
�� cicatriz de cuello uterino	654.6
"�� cicatriz, cicatrices "	
��� ces�rea	654.2
��� cuello uterino	654.6
��� �tero NCOC	654.9
���� debido a ces�rea anterior	654.2
�� cistocele	654.4
"�� cord�n umbilical, presentaci�n o prolapso  "	663.0
"�� cord�n, presentaci�n o prolapso de"	663.0
�� deformidad (adquirida) (cong�nita) 	
��� �rganos o tejidos pelvianos NCOC   	654.9
��� peIvis (�sea) NCOC	653.0
�� deformidad fetal	653.7
"�� desplazamiento, �tero NCOC"	654.4
�� desproporci�n cefalopelviana (feto normalmente formado)	653.4
�� desproporci�n NCOC	653.9
"�� dilataci�n insuficiente, cuello uterino  "	661.0
�� eclampsia	642.6
"�� estenosis o estrechez, cuello uterino  "	654.6
�� feto hidrocef�lico	653.6
�� feto inusitadamente grande	653.5
"�� feto, fetal "	
��� prematuridad	656.8
��� sufrimiento	656.3
�� fibroide (tumor) (�tero)	654.1
�� fracaso de 	
��� extracci�n por aspiraci�n	660.7
��� f�rceps	660.7
��� prueba de trabajo NCOC	660.6
��� ventosa	660.7
"�� frente, presentaci�n de"	652.4
�� hemorragia (anteparto) (intraparto) NCOC	641.9
�� hemorragia fetal-materna	656.0
"�� hombro, presentaci�n de"	652.8
�� incarceraci�n de feto	654.3
"�� inercia, �tero"	661.2
��� primaria	661.0
��� secundaria	661.1
�� insuficiencia placentaria	656.5
"�� lateroversi�n, �tero o cuello uterino"	654.4
�� mala posici�n 	
��� feto	652.9
���� en gestaci�n m�ltiple	652.6
��� �rganos o tejidos pelvianos NCOC	654.9
��� �tero NCOC o cuello uterino	654.4
�� mala presentaci�n NCOC	652.9
��� en gestaci�n m�ltiple	652.6
�� materna 	
��� diabetes mellitus	648.0
��� enfermedad cardiaca NCOC	648.6
�� meconio en l�quido	656.3
��� sin menci�n de sufrimiento fetal 	
��� �nicamente coloraci�n	792.3
"�� ment�n, presentaci�n de"	652.4
"�� nalgas, presentaci�n de"	652.2
"�� oblicua, presentaci�n"	652.3
�� pelvis contra�da (general)	653.1
��� estrecho inferior	653.3
��� estrecho superior	653.2
�� placenta previa	641.0
��� con hemorragia	641.1
"�� posici�n, mala"	652.9
�� preeclampsia	642.4
��� grave	642.5
�� presentaci�n de acromion	652.8
�� prolapso 	
��� brazo o mano	652.7
��� �tero	654.4
�� que afecta al feto o al reci�n nacido	763.4
�� rectocele	654.4
"�� retroversi�n, �tero o cuello uterino"	654.3
�� r�gido 	
��� cuello uterino	654.6
��� perineo	654.8
��� suelo pelviano	654.4
��� vagina	654.7
��� vulva	654.8
"�� saculaci�n, �tero gr�vido"	654.4
�� sufrimiento 	
��� fetal	656.3
��� materno	669.0
�� sutura Shirodkar in situ	654.5
�� trabajo prolongado	662.1
"�� transversal, presentaci�n o posici�n"	652.3
�� tumor p�lvico NCOC	654.9
"�� tumor, �rganos o tejidos pelvianos NCOC     "	654.4
�� �tero bicorne	654.0
� comienzo prematuro (espont�neo)	644.2
� complicaci�n especificada NCOC	669.8
� complicado (por) NCOC	669.9
�� con presentaci�n de cord�n umbilical	663.0
"�� acromion, presentaci�n de"	652.8
��� cuando obstruye el parto	660.0
"�� adherencias, �tero (a la pared abdominal)  "	654.4
�� amnionitis	658.4
�� amnios de hidropes�a	657
�� anillo de Bandl	661.4
�� anillo de contracci�n	661.4
"�� anillo de retracci�n patol�gico, �tero"	661.4
"�� anormal, anormalidad de "	
��� contracciones uterinas NCOC	661.9
��� cuello uterino	654.6
���� cuando obstruye el parto	660.2
��� formaci�n del �tero	654.0
��� fuerzas del trabajo del parto	661.9
��� �rganos o tejidos peIvianos	654.9
���� cuando obstruye el parto	660.2
��� partes blandas (de pelvis)	654.9
���� cuando obstruye el parto	660.2
��� peIvis (�sea) (mayor) NCOC	653.0
���� cuando obstruye el parto	660.1
��� presentaci�n o posici�n NCOC	652.9
���� cuando obstruye el parto	660.0
"��� tama�o, feto"	653.5
���� cuando obstruye el parto	660.1
��� �tero (formaci�n)	654.0
���� cuando obstruye el parto	660.2
��� vagina	654.7
���� cuando obstruye el parto	660.2
�� anterior 	
��� ces�rea	654.2
��� cirug�a 	
���� cuello uterino	654.6
����� cuando obstruye el parto	660.2
��� ginecol�gica NCOC	654.9	
���� cuando obstruye el parto	660.2	
��� perineo	654.8	
��� �tero NCOC	654.9	
���� debido a ces�rea anterior	654.2	
��� vagina	654.7	
���� cuando obstruye el parto	660.2	
��� vulva	654.8	
"�� anteversi�n, cuello uterino o �tero"	654.4	
��� cuando obstruye el parto	660.2	
�� apoplej�a	674.0	
��� placenta	641.2	
"�� aton�a, �tero (hipot�nica) (inercia)"	661.2	
��� hipert�nica	661.4	
�� baja implantaci�n de placenta 		"v�ase   Parto, complicado, placenta, previa "
"�� Bandl, anillo de"	661.4	
"�� cara, presentaci�n de"	652.4
��� cuando obstruye el parto	660.0
��� en el pubis	660.3
�� choque (nacimiento) (obst�trico) (puerperal)	669.1
�� cicatriz 	
��� cuello uterino	654.6
���� cuando obstruye el parto	660.2
��� vagina	654.7
���� cuando obstruye el parto	660.2
"�� cicatriz, cicatrices "	
��� ces�rea	654.2
���� cuando obstruye el parto	660.2
��� cuello uterino	654.6
���� cuando obstruye el parto	660.2
��� perineo	654.8
���� cuando obstruye el parto	660.2
��� �tero NCOC	654.9
���� cuando obstruye el parto	660.2
���� debido a ces�rea anterior	654.2
��� vagina	654.7
���� cuando obstruye el parto	660.2
��� vulva	654.8
���� cuando obstruye el parto	660.2
�� cistocele	654.4
��� cuando obstruye el parto	660.2
�� colporrexis	665.4
��� con laceraci�n perineal	664.0
�� compresi�n de cord�n (umbilical)	663.2
��� alrededor del cuello	663.1
��� cord�n prolapsado	663.0
"�� contracci�n en reloj de arena, �tero"	661.4
"�� contracci�n, pelvis contra�da"	653.1
��� cuando obstruye el parto	660.1
��� estrecho inferior	653.3
���� cuando obstruye el parto	660.1
��� estrecho superior	653.2
���� cuando obstruye el parto	660.1
��� general	653.1
���� cuando obstruye el parto	660.1
��� pelviana media	653.8
���� cuando obstruye el parto	660.1
��� plano mediano	653.8
���� cuando obstruye el parto	660.1
�� cord�n (umbilical)	663.9
"��� alrededor del cuello, estrechamente o con compresi�n"	663.1
���� sin compresi�n	663.1
��� complicaci�n especificada NCOC	663.8
��� complicaci�n NCOC	663.9
���� tipo especificado NCOC	663.8	
��� compresi�n NCOC	663.2	
��� contusi�n	663.6	
��� corto	663.4	
��� en posici�n anterior	663.0	
��� enredamiento NCOC	663.3	
���� con compresi�n	663.2	
��� fijaci�n marginal	663.8	
��� hematoma	663.6	
��� inserci�n velamentosa	663.5	
��� lesi�n vascular	663.6	
��� presentaci�n	663.0	
��� prolapso (completo) (oculto) (parcial)	663.0	
��� trombosis (vasos)	663.6	
�� cord�n umbilical	663.9	"v�ase adem�s Parto, complicado, cord�n"
"��� alrededor del cuello estrechamente, o con compresi�n"	663.1	
��� enredamiento NCOC	663.3	
���� con compresi�n	663.2	
��� prolapso (completo) (oculto) (parcial)	663.0	
"�� Couvelaire, �tero de"	641.2	
�� deformidad (adquirida) (cong�nita) 		
��� feto	653.7	
���� cuando obstruye el parto	660.1	
��� �rganos o tejidos pelvianos NCOC	654.9	
���� cuando obstruye el parto	660.2	
��� pelvis (�sea) NCOC	653.0	
���� cuando obstruye el parto	660.1	
�� desgarre (�rgano pelviano)	664.9	"v�ase adem�s Parto, complicado, laceraci�n"
�� desplazamiento de �tero NCOC	654.4	
��� cuando obstruye el parto	660.2	
"�� desprendimiento anular, cuello uterino"	665.3	
�� desproporci�n cefalopelviana (feto normalmente formado)	653.4	
��� cuando obstruye el parto	660.1
�� desproporci�n fetopelviana	653.4
��� cuando obstruye el parto	660.1
�� desproporci�n NCOC	653.9
��� cuando obstruye el parto	660.1
�� diastasis de los rectos	665.8
�� dilataci�n 	
"��� cuello uterino, incompleta, lenta o insuficiente"	661.0
��� vejiga	654.4
���� cuando obstruye el parto	660.2
�� dilataci�n incompleta (cuello uterino)	661.0
�� disfunci�n uterina hipert�nica	661.4
�� disfunci�n uterina hipot�nica	661.2
"�� disfunci�n, �tero"	661.9
��� hipert�nica	661.4
��� hipot�nica	661.2
���� primaria	661.0	
���� secundaria	661.1	
��� incoordinada	661.4	
�� disruptio uteri 		"v�ase Parto, complicado, ruptura, �tero "
�� distocia 		
��� cervical	661.0	
��� cintura escapular	660.4	
��� fetal 		"v�ase Parto, complicado, anormal, presentaci�n "
��� materna 		"v�ase Parto, complicado, trabajo prolongado "
��� p�lvica 		"v�ase Parto, complicada, contracci�n de peIvis "
��� posicional	652.8	
�� distocia cervical	661.0	
�� doble �tero (cong�nito)	654.0	
��� cuando obstruye el parto	660.2	
�� eclampsia	642.6	
"�� edema, cuello uterino"	654.6	
��� cuando obstruye el parto	660.2
"�� efusi�n, I�quido amni�tico"	658.1
�� embolia (pulmonar)	673.2
��� aire	673.0
��� cerebral	674.0
��� co�gulo sangu�neo	673.2
��� grasa	673.8
��� I�quido amni�tico	673.1
��� pi�mica	673.3
��� s�ptica	673.3
�� embolia de aire	673.0
�� embolia por l�quido amni�tico	673.1
"�� enredamiento, cord�n umbilical"	663.3
��� con compresi�n	663.2
��� alrededor del cuello (con compresi�n)	663.1
�� entrelazamiento de fetos	660.5
�� espasmo 	
��� cuello uterino	661.4
��� �tero	661.4
�� espondilolisis (lumbosacra)	653.3
��� cuando obstruye el parto	660.1
"�� espondilolistesis, pelvis"	653.3
��� cuando obstruye el parto	660.1
�� espondilosis	653.0
��� cuando obstruye el parto	660.1
�� estenosis o estrechez 	
��� cuello uterino	654.6
���� cuando obstruye el parto	660.2
��� vagina	654.7
���� cuando obstruye el parto	660.2
"�� eversi�n, cuello uterino o �tero"	665.2
�� excesivo 	
��� crecimiento fetal	653.5
���� cuando obstruye el parto	660.1
��� tama�o del feto	653.5
���� cuando obstruye el parto	660.1
�� fallo de la cabeza fetal para entrar en el reborde de la pelvis	652.5
��� cuando obstruye el parto	660.0
�� fase activa de pendiente lento	661.2
�� fetal 	
��� deformidad	653.7
���� cuando obstruye el parto	660.1
��� equilibrio acidob�sico	656.3
��� muerte (casi a t�rmino) NCOC	656.4
���� precoz (antes de completarse 22 semanas de gestaci�n)	632
��� sufrimiento	656.3
��� velocidad o ritmo cardiaco	656.3
�� feto de tama�o inusitado	653.5
�� cuando obstruye el parto	660.1
�� feto hidrocef�lico	653.6
��� cuando obstruye el parto	660.1
�� fetos entrelazados	660.5
�� fibroide (tumor) (�tero)	654.1
��� cuando obstruye el parto	660.2
�� fibromiomata	654.1
��� cuando obstruye el parto	660.2
�� fiebre durante el parto	659.2
�� fractura de c�ccix	665.6
"�� frente, presentaci�n de"	652.4
��� cuando obstruye el parto	660.0
�� hematoma	664.5
��� cord�n umbilical	663.6
��� espina isquial	665.7
��� ligamento ancho	665.7
��� peIviano	665.7
��� perineo	664.5
��� subdural	674.0
��� tejidos blandos	665.7
��� vagina	665.7
��� vulva o perineo	664.5
�� hemorragia (uterina) (anteparto) (intraparto) (embarazo)	641.9
��� accidental	641.2
��� asociada con 	
���� afibrinogenemia	641.3
���� defecto de coagulaci�n	641.3
����� hiperfibrin�lisis	641.3
����� hipofibromogenemia	641.3
��� cerebral	674.0
��� debida a 	
���� leiomioma uterino	641.8
���� placenta baja	641.1	
���� placenta previa	641.1	
���� placenta retenida	666.0	
���� separaci�n prematura de placenta (implantada de forma normal)	641.2	
���� traumatismo	641.8	
��� placenta NCOC	641.9	
��� posparto (at�nica) (inmediata) (dentro de 24 horas)	666.1	
���� con placenta retenida o incarcerada	666.0	
���� retardada	666.2	
���� secundaria	666.2	
���� tercera fase	666.0	
��� ruptura de seno marginal	641.2	
�� hemorragia	641.9	"v�ase adem�s Parto, complicado, hemorragia"
�� hemorragia anteparto 		"v�ase Parto, complicado, hemorragia "
�� hemorragia cerebral	674.0	
�� hermanos siameses	653.7	
��� cuando obstruye el parto	660.0	
�� hidramnios	657	
�� hidropes�a fetal	653.7	
��� cuando obstruye el parto	660.1	
�� hipertensi�n 		"v�ase Hipertensi�n, cuando complica el embarazo "
"�� hombro, escapular "		
��� distocia de cintura	660.4	
��� presentaci�n	652.8	
���� cuando obstruye el parto	660.0	
�� hombros impactados	660.4	
�� hueso pelviano asim�trico	653.0	
��� cuando obstruye el parto	660.1	
"�� incarceraci�n, �tero"	654.3	
��� cuando obstruye el parto	660.2	
�� indicaci�n NCOC	659.9	
��� tipo especificado NCOC	659.8	
�� inercia uterina primaria	661.0
�� inercia uterina secundaria	661.1
"�� inercia, �tero"	661.2
��� hipert�nica	661.4
��� hipot�nica	661.2
��� primaria	661.0
��� secundaria	661.1
�� infantil 	
��� �rganos genitales	654.4
���� cuando obstruye el parto	660.2
��� �tero (orificio) (os)	654.4
���� cuando obstruye el parto	660.2
�� inserci�n velamentosa de cord�n	663.5
"�� inversi�n, �tero"	665.2
�� laceraci�n	664.9
��� ano (esf�nter)	664.2
���� con mucosa	664.3
��� central	664.4
��� cuello uterino	665.3
��� esf�nter del ano	664.2
���� con mucosa	664.3
��� himen	664.0
��� horquilla vulvar	664.0
��� intestino	665.5
��� Iabios vulvares (mayores) (menores)	664.0
��� pelviano 	
���� �rgano NCOC 	665.5
���� suelo	664.1
"��� perineo, perineal"	664.4
���� central	664.4
���� cuarto grado	664.3
���� extensa NCOC	664.4
���� leve	664.0
���� m�sculos	664.1
���� piel	664.0
���� primer grado	664.4
���� segundo grado	664.1
���� tercer grado	664.2
��� peritoneo	665.5
��� piel (perineo)	664.0
��� rectovaginal (tabique) (sin laceraci�n perineal)	665.4
���� con perineo	664.2
����� con mucosa anal o rectal	664.3
��� sitio o tipo especificado NCOC	664.8
��� tejidos periuretrales	665.5
��� uretra	665.5
��� �tero	665.1
���� antes del trabajo	665.0
"��� vagina, vaginal (profunda) (alta) (surco) (sin laceraci�n perineal)"	665.4
���� con perineo	664.0
"����� m�sculos, con perineo"	664.1
��� vejiga (urinaria)	665.5
��� vulva	664.0
"�� lateroversi�n, �tero o cuello uterino"	654.4
��� cuando obstruye el parto	660.2
�� lesi�n (a la madre) NCOC	665.9
�� lesi�n de parto que afecta a la madre NCOC	665.9
�� mala posici�n 	
��� feto NCOC	652.9
���� cuando obstruye el parto	660.0
��� �rganos o tejidos pelvianos NCOC	654.9
���� cuando obstruye el parto	660.2
��� placenta	641.1
���� sin hemorragia	641.0
��� �tero NCOC o cuello uterino	654.4	
���� cuando obstruye el parto	660.2	
�� mala presentaci�n	652.9	
��� cuando obstruye el parto	660.0	
�� malposici�n	652.9	
��� cuando obstruye el parto	660.0	
�� meconio en l�quido amni�tico	656.3	
�� membranas o porciones de placenta retenidas	666.2	
��� sin hemorragia	667.1	
"�� membranas, retenidas "		"v�ase Parto, complicado, placenta, retenida "
"�� ment�n, presentaci�n de"	652.4	
��� cuando obstruye el parto	660.0	
"�� ment�n, presentaci�n de"	652.4	
��� cuando obstruye el parto	660.0	
�� metrorragia (miopat�a) 		"v�ase Parto, complicado, hemorragia "
�� metrorrexis 		"v�ase Parto, complicado, ruptura, �tero "
"�� mielomeningocele, feto"	653.7
��� cuando obstruye el parto	660.1
�� monstruo	653.7
��� cuando obstruye el parto	660.1
�� muerte anest�sica	668.9
�� muerte del feto (cercano a t�rmino)	656.4
��� tempranamente (antes de completar las 22 semanas de gestaci�n) 	
�� muerte fetal intrauterina (casi a t�rmino) NCOC	656.4
��� precoz (antes de completarse 22 semanas de gestaci�n)	632.
"�� muerte repentina, causa desconocida"	669.9
�� multiparidad (grande)	659.4
�� nacimiento cruzado	652.3
��� con versi�n acertada	652.1
��� cuando obstruye el parto	660.0
�� nacimiento retardado (prolongado)	662.1
"�� N�gele, pelvis de"	653.0
��� cuando obstruye el parto	660.1
"�� nalgas, presentaci�n de (asistida) (espont�nea)"	652.2
��� con versi�n acertada	652.1
��� cuando obstruye el parto	660.0
"�� no colocaci�n, cabeza fetal"	652.5
��� cuando obstruye el parto	660.0
"�� nudo (verdadero), cord�n umbilical"	663.2
"�� oblicua, presentaci�n"	652.3
��� cuando obstruye el parto	660.0
�� obst�trico 	
��� choque	669.1
��� traumatismo NCOC	665.9
�� obstrucci�n del trabajo	660.9
��� debida a 	
���� anormalidad de �rganos o tejidos pelvianos (estados clasificables bajo 654.0-654.9)	660.2
���� detenci�n transversal profunda	660.3
���� distocia del hombro	660.4	
���� gemelos entrelazados	660.5	
���� hombros impactados	660.4	
���� mala posici�n y mala presentaci�n del feto (clasificables bajo 652.0-652.9)	660.0	
���� occipitoposterior persistente	660.3	
�� parada de la fase activa	661.1	
�� pelviano 		
��� deformidad (hueso) 		"v�ase adem�s Deformidad, pelvis, con desproporci�n "
���� tejidos blandos	654.9	
����� cuando obstruye el parto	660.2	
��� detenci�n (profunda) (alta) (de cabeza fetal) (transversal)	660.3	
��� tumor NCOC	654.9	
��� cuando obstruye el parto	660.2	
�� pelvis de cretinismo (tipo enano) (tipo masculino)	653.1	
��� cuando obstruye el parto	660.1	
�� pelvis escoli�tica	653.0	
��� cuando obstruye el parto	660.1	
�� pelvis inclinada	653.0	
��� cuando obstruye el parto	660.1	
�� pelvis raqu�tica	653.2	
��� cuando obstruye el parto	660.1	
"�� penetraci�n, �tero gr�vido, por instrumento"	665.1	
�� perforaci�n 		"v�ase Parto, complicado, laceraci�n "
�� persistente 		
��� himen	654.8	
���� cuando obstruye el parto	660.2	
��� occipitoposterior	660.3	
�� pirexia durante el trabajo	659.2	
�� placenta adherente	667.0	
��� con hemorragia	666.0	
�� placenta en forma de raqueta 		"v�ase Placenta, anormal "
�� placenta enferma	656.7	
"�� placenta, placentario "	
��� ablatio	641.2
��� abruptio	641.2
��� accreta	667.0
���� con hemorragia	666.0
��� adherente (sin hemorragia)	667.0
���� con hemorragia	666.0
��� anormalidad	656.7
���� con hemorragia	641.2
��� apoplej�a	641.2
��� baja (implantaci�n)	641.1
���� sin hemorragia	641.0
��� desprendimiento (prematuro)	641.2
��� en forma de raqueta	663.8
��� enfermedad	656.7
��� hemorragia NCOC	641.9
��� incarcerada	666.0
���� sin hemorragia	667.0
��� increta (sin hemorragia)	667.0
���� con hemorragia	666.0
��� inserci�n viciosa	641.1
��� mala posici�n	641.1
���� sin hemorragia	641.0
��� malformaci�n	656.7
���� sin hemorragia	641.2
��� percreta	667.0
���� con hemorragia	666.0
��� previa (central) (lateral) (marginal) (parcial)	641.1
���� sin hemorragia	641.0
��� retenida (con hemorragia)	666.0
���� sin hemorragia	667.0
��� ruptura de seno marginal	641.2
"��� seno marginal, ruptura de"	641.2
��� separaci�n (prematura)	641.2
��� separaci�n prematura	641.2
�� polihidramnios	657
"�� p�lipo, cuello uterino"	654.6
��� cuando obstruye el parto	660.2
�� posici�n anterior del cord�n umbilical	663.0
�� postura inestable	652.0
��� cuando obstruye el parto	660.0
�� prematuro 	
"��� ruptura, membranas"	658.1
���� trabajo retardado despu�s de	658.2
��� trabajo (antes de terminarse 37 semanas de gestaci�n)	644.2
�� presentaci�n compuesta	652.8
��� cuando obstruye el parto	660.0
�� prim�para a�osa	659.5
�� prolapso 	
��� brazo o mano	652.7
���� cuando obstruye el parto	660.0
��� cord�n (umbilical)	663.0
��� extremidad fetal	652.8
��� pie o pierna	652.8
���� cuando obstruye el parto	660.0
"��� umbilical, cord�n (completo) (oculto) (parcial)"	663.0
��� �tero	654.4
���� cuando obstruye el parto	660.2
�� prolapso oculto de cord�n umbilical	663.0
�� quiste (conducto de Gartner)	654.7
�� rectocele	654.4
��� cuando obstruye el parto	660.2
"�� retardo, retardado "	
��� despu�s de ruptura de membranas (espont�nea)	658.2
���� artificial	658.3
��� trabajo en embarazo m�ltiple	662.3
���� debido a fetos entrelazados	660.5
�� retenci�n de secundinas (con hemorragia)     	666.2
��� sin hemorragia	667.1
"�� retroversi�n, �tero o cuello uterino"	654.3
��� cuando obstruye el parto	660.2
�� r�gido 	
��� cuello uterino	654.6
���� cuando obstruye el parto	660.2
��� perineo o vulva	654.8
���� cuando obstruye el parto	660.2
��� suelo pelviano	654.4
���� cuando obstruye el parto	660.2
��� vagina	654.7
���� cuando obstruye el parto	660.2
�� ri��n ect�pico	654.4	
��� cuando obstruye el parto	660.2	
"�� Robert, pelvis de"	653.0	
��� cuando obstruye el parto	660.1	
�� ruptura 		"v�ase adem�s Parto, complicado, laceraci�n"
��� cuello uterino	665.3	
"��� membranas, prematura"	658.1	
��� �rgano pelviano NCOC	665.5	
��� perineo (sin menci�n de otra laceraci�n) 		"v�ase Parto, complicado, laceraci�n, perineo "
��� peritoneo	665.5	
��� seno marginal	641.2	
��� uretra	665.5	
��� �tero (durante o despu�s del trabajo)	665.1	
���� antes del trabajo	665.0	
��� vejiga (urinaria)	665.5	
"�� saculaci�n, �tero gr�vido"	654.4	
"�� secundinas, retenidas "		"v�ase Parto, complicado, placenta, retenida"
�� seno marginal (hemorragia) (ruptura)     	641.2	
�� separaci�n 		
��� hueso(s) del pubis	665.6	
��� placenta (prematura)	641.2	
��� s�nfisis pubiana	665.6	
�� s�ndrome de cord�n corto	663.4	
�� s�ndrome de hipotensi�n materna	669.2	
�� sufrimiento 		
��� fetal	656.3	
��� materno	669.0	
"�� teratomas sacros, fetales"	653.7	
��� cuando obstruye el parto	660.1	
�� tonos cardiacos fetales deprimidos	656.3	
�� trabajo precipitado	661.3	
�� trabajo prolongado	662.1	
��� debido a 	
���� anillo de contracci�n	661.4
���� distocia cervical	661.0
���� inercia uterina	661.2
����� primaria	661.0
����� secundaria	661.1
���� �tero tet�nico	661.4
��� fase activa	661.2
��� fase latente	661.0
��� primera fase	662.0
��� segunda fase	662.2
"�� trabajo, prematuro (antes de terminarse 37 semanas de gestaci�n)"	644.2
�� transversal 	
��� detenci�n (profunda)	660.3
��� presentaci�n o postura	652.3
���� con versi�n acertada	652.1
���� cuando obstruye el parto	660.0	
�� traumatismo (obst�trico) NCOC	665.9	
�� tumor 		
"��� abdominal, fetal"	653.7	
���� cuando obstruye el parto	660.1	
��� �rganos o tejidos pelvianos NCOC	654.9	
���� cuando obstruye el parto	660.2	
"�� tumor abdominal, fetal"	653.7	
��� cuando obstruye el parto	660.1	
�� uterino 		
��� espasmo	661.4	
��� inercia	661.2	"v�ase adem�s Parto, complicado, inercia, �tero"
�� �tero anormalmente formado (cualquier tipo) (cong�nito)	654.0	
��� cuando obstruye el parto	660.2	
�� �tero bicorne	654.0	
��� cuando obstruye el parto	660.2	
�� �tero incoordinado	661.4	
�� �tero tet�nico	661.4	
�� vagina tabicada	654.7	
��� cuando obstruye el parto	660.2	
�� vasa previa	663.5	
� cuatrillizos NCOC	651.2	
�� con p�rdida fetal y retenci�n de uno o m�s fetos	651.5	
� dif�cil NCOC	669.9	
�� parto anterior que afecta la atenci�n del embarazo o alumbramiento	V23.4	
� tipo especificado NCOC	669.8	
� embarazo a t�rmino (nacido con vida) NCOC 	650	v�ase categor�a 650
� mortinato NCOC	656.4	
� extracci�n por aspiraci�n NCOC	669.5	
�� que afecta al feto o al reci�n nacido	763.3	
� f�rceps NCOC	669.5	
�� que afecta al feto o al reci�n nacido	763.2	
� gemelos NCOC	651.0	
�� con p�rdida fetal y retenci�n de un feto	651.3	
�� fetos entrelazados	660.5	
�� parto retardado (uno o m�s fetos)	662.3	
� gestaci�n m�ltiple NCOC	651.9	
�� con p�rdida fetal y retenci�n de uno o m�s fetos	651.6	
�� tipo especificado NCOC	651.8	
��� con p�rdida fetal y retenci�n de uno o m�s fetos	651.6	
� infante no viable	656.4	
� mortinato NCOC	656.4	
�� precoz (antes de completarse 22 semanas de gestaci�n)	632	
� nalgas (asistido) (espont�neo)	652.2	
�� extracci�n NCOC	669.6	
�� que afecta al feto o al reci�n nacido	763.0	
� no complicado 	650	v�ase categor�a 650
� no detectado (a o cerca de t�rmino)	656.4	
� normal 	650	v�ase categor�a 650
� precipitado	661.3	
�� que afecta al feto o al reci�n nacido	763.6	
� prematuro NCOC (antes de terminarse 37 semanas de gestaci�n)	644.2	
"�� previo, que afecta la atenci�n del embarazo"	V23.4	
� quintillizos NCOC	651.8	
�� con p�rdida fetal y retenci�n de uno o m�s fetos	651.6	
� retardado NCOC	662.1	
�� despu�s de ruptura de membranas (espont�nea)	658.2	
��� artificial	658.3	
"�� segundo gemelo, trillizo, etc."	662.3	
� sixtillizos 		
�� con p�rdida fetal y retenci�n de uno o m�s fetos	651.6	
� trillizos NCOC	651.1	
�� con p�rdida fetal y retenci�n de uno o m�s fetos	651.4	
�� fetos entrelazados	660.5	
�� parto retardado (uno o m�s fetos)	662.3	
� ventosa NCOC	669.5	
�� que afecta al feto o al reci�n nacido	763.3	
Parto 		v�ase Parto
Parto lento o largo	662.1	
� primer per�odo	662.0	
� que afecta al feto o al reci�n nacido	763.8	
� segundo per�odo	662.2	
Parturici�n 		v�ase Parto 
Pasaje o paso 		
� de sondas o buj�as	V55.9	v�ase adem�s Atenci�n de apertura artificial
"� falso, uretra"	599.4	
"Pasional, psicosis"	297.8	
Pasivo 		v�ase enfermedad espec�fica
"Pastel, ri��n en forma de"	753.3	
Pasteurella s�ptica	027.2	
Pasteurelosis	027.2	"v�ase adem�s Infecci�n, Pasteurella"
"Patau, s�ndrome de (trisom�a D1)"	758.1	
Patelar (r�tula) 		v�ase enfermedad espec�fica
Patente 		"v�ase adem�s Imperfecto, cierre "
� canal de Nuck	752.41	
� conducto 		
�� arterioso o de Botal	747.0	
�� onfalomesent�rico	751.0	
�� vitelino	751.0	
� cuello uterino	622.5	
�� cuando complica el embarazo	654.5	
��� que afecta al feto o al reci�n nacido	761.0	
"� Eustaquio, de "		
�� trompa	381.7	
�� v�lvula	746.89	
� foramen 		
�� de Botal	745.5	
�� oval	745.5	
� orificio externo (�tero) 		"v�ase Patente, cuello uterino "
� ostium 		
�� atrioventricular	745.69	
�� secundum	745.5	
� tabique 		
�� interauricular	745.5	
�� interventricular	745.4	
� uraco	753.7	
"Paterson (-Brown) (-Kelly), s�ndrome de (disfagia siderop�nica)"	280.8	
"Paterson (-Kelly), s�ndrome o membrana de (disfagia siderop�nica)"	280.8	
Patizambo (adquirido)	736.41	
� cong�nito	755.64	
Patolog�a (de) 		v�ase Enfermedad
Patol�gico 		v�ase adem�s enfermedad espec�fica 
� asfixia	799.0	
� embriaguez	291.4	
� emocionalidad	301.3	
� mentiroso	301.7	
� personalidad	301.9	
"� reabsorci�n, diente"	521.4	
� sexualidad	302.9	"v�ase adem�s Desviaci�n, sexual"
Pausa sinoatrial	427.81	
Pavor nocturnus	307.46	
"Pavy, enfermedad de"	593.6	
"Paxton, enfermedad de (piedra blanca)"	111.2	
"Payr, enfermedad o s�ndrome de (s�ndrome de flexura espl�nica)"	569.89	
"Pa�ales, eritema de los"	691.0	
"Pa�ales, sarpullido"	691.0	
Pecas	709.0	
� melanoma maligno en 	[M8742/3]	v�ase Melanoma 
� melan�ticas (de Hutchinson) 	[M8742/2]	"v�ase Neoplasia, piel, in situ  "
Pecho		
� en quilla (cong�nito)	754.82	
�� adquirido	738.3	
�� raqu�tico	268.0	v�ase adem�s Raquitismo
� excavado (cong�nito)	754.81	
�� adquirido	738.3	
�� raqu�tico	268.0	v�ase adem�s Raquitismo
� recurvatum (cong�nito)	754.81	
�� adquirido	738.3	
"� zapateros, de los"	738.3	
Pecho 		v�ase enfermedad espec�fica
Pecho de zapatero	738.3	
Pecho en tonel	738.3	
Pectenitis	569.49	
Pectenosis	569.49	
Pectoral 		v�ase enfermedad espec�fica 
Pedatrofia	261	
Pederosis	302.2	
Pediculosis (infestaci�n)	132.9	
� capitis (piojo de la cabeza) (cualquier sitio)	132.0	
� corporis (piojo del cuerpo) (cualquier sitio)	132.1	
� mixta (clasificable bajo m�s de una de las categor�as 132.0-132.2)	132.3	
� p�rpado	132.0[373.6]	
� pubis (piojo del pubis) (cualquier sitio)	132.2	
� vestimenti o del vestido	132.1	
� vulva	132.2	
Pediculosis 	302.2	v�ase Piojos Pedofilia
"Pegajoso, ojo"	372.03	
"Pegamento, inhalaci�n de (pegamento de aeromodelismo)"	304.6	v�ase adem�s Dependencia
"Pel, crisis de"	094.0	
"Pel-Ebstein, enfermedad de "	704.01	"v�ase Enfermedad, Hodgkin de Pelada"
Pelagra (alcoh�lica o con alcoholismo)	265.2
� con polineuropat�a	265.2[357.4]
"Pelagra, ataxia cerebelosa y aminociduria renal, s�ndrome de"	270.0
"Peleteros, pulm�n de"	495.8
"Pelger-Hu�t, anomal�a o s�ndrome de (hiposegmentaci�n hereditaria)"	288.2
Peliosis (reum�tica)	287.0
"Pelizaeus-Merzbacher, de  "	
