�� reducci�n		"v�ase Deformidad, reducci�n "
� extremidad inferior		"v�ase Deformidad, pierna "
� extremidad superior		"v�ase Deformidad, brazo "
� faringe (cong�nita)	750.9	
�� adquirida	478.29	
� f�mur (adquirida)	736.89	
�� cong�nita	755.60	
� fetal 		
�� con desproporci�n fetop�lvica	653.7	
��� que afecta al feto o al reci�n nacido	763.1	
"�� conocida o presunta, cuando afecta la atenci�n del parto"	655.9	
�� cuando obstruye el parto	660.1	
��� que afecta al feto o al reci�n nacido	763.1	
� flexi�n (articulaci�n) (adquirida)	736.9	
�� cong�nita NCOC	755.9	
�� muslo o cadera (adquirida)	736.89	
��� cong�nita	754.32	"v�ase adem�s Subluxaci�n, cong�nita, cadera"
� frente (adquirida)	738.1	
�� cong�nita	756.0	"v�ase adem�s Deformidad, cr�neo, cong�nita"
� gl�ndula endoerina NCOC	759.2	
� gl�ndula o conducto salival (eong�nita)	750.9	
�� adquirida	527.8	
"� gl�ndula seb�cea, adquirida"	706.8	
� gl�ndula submaxilar (cong�nita)	750.9	
�� adquirida	527.8	
� gl�ndula suprarrenal (cong�nita)	759.1	
� globo (ojo) (cong�nita)	743.9	
�� adquirida	360.89	
� h�gado (cong�nita)	751.60	
�� adquirida	573.8	
�� conducto (cong�nita)	751.60	
��� adquirida	576.8	
"���� con c�lculo, coledocolitiasis o piedras"		v�ase Coledocolitiasis 	
� himen (cong�nita)	752.40		
� hip�fisis (cong�nita)	759.2		
� hombro (articulaci�n) (adquirida)	736.89		
�� cong�nita	755.50		
��� tipo especificado NCOC	755.59		
�� contracci�n	718.41		
� hueso (adquirida) NCOC	738.9		
�� cong�nita	756.9		
�� turbinados	738.0		
� hueso frontal (adquirida)	738.1		
�� cong�nita	756.0	"v�ase adem�s Deforrnidad, cr�neo, cong�nita"	Deformidad
� h�mero (adquirida)	736.89		
�� cong�nita	755.50		
� humor v�treo (cong�nita)	743.9		
�� adquirida	379.29		
� ileocecal (repliegue) (v�lvula) (cong�nita)  	751.5
�� adquirida	569.89
� �leon (intestino) (cong�nita)	751.5
�� adquirida	569.89
� ilion (adquirida)	738.6
�� cong�nita	755.60
� integumento (cong�nita)	757.9
� intestino (grueso) (delgado) (cong�nita)  	751.5
�� adquirida	569.89
� iris (adquirida)	364.75
�� cong�nita	743.9
�� prolapso	364.8
� isquion (adquirida)	738.6
�� cong�nita	755.60
� Klippel-Feil (brevicollis)	756.16
� labio (cong�nita) NCOC	750.9
�� adquirida	528.5
�� tipo especificado NCOC	750.26
� labio vulvar (mayor) (menor) (cong�nita)	752.40
�� adquirida	624.8
� laringe (m�sculo) (cong�nita)	748.3
�� adquirida	478.79
�� velo (gl�tico) (subgl�tico)	748.2
� lengua (cong�nita)	750.10
�� adquirida	529.8
� ligamento (adquirida)	728.9
�� cong�nita	756.9
� lumbosacra (articulaci�n) (regi�n) (cong�nita)	756.10
�� adquirida	738.5
"� Madelung, de (radio)"	755.54
� mama (adquirida)	611.8
�� cong�nita	757.9
� mano (adquirida)	736.00
�� cong�nita	755.50
�� en cuello de cisne (intr�nseca)	736.09
�� garra	736.06
�� minus (y plus) (intr�nseca)	736.09
"�� pildorero, de (intr�nseca)"	736.09
�� plus (y minus) (intr�nseca)	736.09
� maxilar (adquirida) (cong�nita)	524.9
� maxilar (adquirida) (cong�nita) NCOC	524.9
�� debida a mala posici�n y presi�n intrauterinas	754.0
� mejilla (adquirida)	738.1
�� cong�nita	744.9
� meninges o membrana (cong�nita)	742.9
�� cerebro	742.4
��� adquirida	349.2
�� espinal (m�dula)	742.59
��� adquirida	349.2	
� ment�n (adquirida)	738.1	
�� cong�nita	744.9	
� mesenterio (cong�nita)	751.9	
�� adquirida	568.89	
� metacarpo (adquirida)	736.00	
�� cong�nita	755.50	
� metatarso (adquirida)	736.70	
�� cong�nita	754.70	
� miembro (adquirida)	736.9	
"�� cong�nita, salvo deformidad por reducci�n"	755.9	
��� inferior	755.60	
���� reducci�n	755.30	"v�ase adem�s Deformidad, reducci�n, miembro inferior"
��� superior	755.50	
���� reducci�n	755.20	"v�ase adem�s Deformidad, reducci�n, miembro inferior"
�� especificada NCOC	736.89	
� mitral (valvas) (v�lvulas) (cong�nita)	746.9	 
�� adquirida		"v�ase Endocarditis, mitral"
"�� Ebstein, de"	746.89	
"�� estenosis, cong�nita"	746.5	
�� paracaidas	746.5	
�� tipo especificado NCOC	746.89	
"� m�ltiple, cong�nita NCOC"	759.7	
�� tipo especificado NCOC	759.89	
� m�sculo (adquirida)	728.9	
�� cong�nita	756.9	
��� esternocleidomastoideo (debida a mala posici�n y presi�n intrauterina)	754.1	
��� tipo especificado NCOC	756.89	
� m�sculo ocular (cong�nita)	743.9	
�� adquirida	378.60	
� muslo (adquirida)	736.89	
�� cong�nita	755.60	
� mu�eca (articulaci�n) (adquirida)	736.00
�� cong�nita	755.50
�� contracci�n	718.43
�� valgo	736.03
��� cong�nita	755.59
�� varo	736.04
��� cong�nita	755.59
"� nariz, nasal (cart�lago) (adquirida)"	738.0
�� cong�nita	748.1
��� aplastada	754.0
��� torcida	754.0
�� en silla de montar	738.0
��� sifil�tica	090.5
�� huesos (turbinados)	738.0
�� seno (pared) (cong�nita)	748.1
��� adquirida	738.0
�� sifil�tica (cong�nita)	090.5	
��� tard�a	095.8	
�� tabique	470	
��� cong�nita	748.1	
� nasal		"v�ase Deformidad, nariz "
� o�do (cong�nita)	744.3	
�� adquirida	380.32	
�� cuando causa deterioro de la audici�n	744.00	
�� externa	744.3	
��� cuando causa deterioro de la audici�n	744.02	
�� huesecillos	744.04	
�� interna	744.05	
�� I�bulo	744.3	
�� medio	744.03	
��� huesecillos	744.04	
�� pabell�n	744.3	
��� cuando causa deterioro de la audici�n	744.02
"� o�do medio, salvo huesecillos (cong�nita)"	744.03
�� huesecillos	744.04
� ojo (cong�nita)	743.9
�� adquirida	379.8
�� m�sculo	743.9
� �rbita (cong�nita) (ojo)	743.9
�� adquirida NCOC	376.40
��� asociada con deformidades craneofaciales	376.44
��� debida a 	
���� cirug�a	376.47
���� enfermedad �sea	376.43
���� trauma	376.47
� �rgano de Corti (cong�nita)	744.05
� �rgano(s) o genital(es) NCOC 	
�� cong�nita	752.9
�� femenino (cong�nita)	752.9
��� adquirida	629.8
��� externo	752.40
��� interno	752.9
�� masculino (cong�nita)	752.9
��� adquirida	608.89
� �rgano(s) o sistema digestivos (cong�nita) NCOC	751.9
�� tipo especificado NCOC	751.8
� orificio vesicouretral (adquirida)	596.8
�� cong�nita NCOC	753.9
��� tipo especificado NCOC	753.8
� ovario (cong�nita)	752.0
�� adquirida	620.8
� oviducto (cong�nita)	752.10
�� adquirida	620.8
� pabell�n del o�do (adquirida)  	380.32
�� cong�nita	744.3	"v�ase adem�s Deformidad, o�do"
� pabell�n del o�do (cong�nita)	744.3	
�� adquirida	380.32	
� paladar (cong�nita)	750.9	
�� adquirida	526.89	
"�� blando, adquirida"	528.9	
"�� duro, adquirida"	526.89	
�� hendidura (cong�n�ta)	749.00	"v�ase adem�s Hendidura, paladar"
� p�ncreas (cong�nita)	751.7	
�� adquirida	577.8	
� paraca�das (v�lvula mitral	746.5	
� paratiroides (gl�ndula)	759.2	
� parot�dea (gl�ndula) (cong�nita)	750.9	
�� adquirida	527.8	
� p�rpado (cong�nita)	743.9	
�� adquirida	374.89	
�� tipo especificado NCOC	743.62
� p�rpado (pliegue) (cong�nita)	743.9
�� adquirida	374.89
"� partes, �rganos o tejidos blandos (de pelvis) "	
�� en el embarazo o alumbramiento NCOC	654.9
��� cuando obstruye eI parto	660.2
���� que afecta al feto o al reci�n nacido	763.1
��� que afecta al feto o al reci�n nacido	763.8
� pecho (pared) (adquirida)	738.3
�� cong�nita	754.89
�� efecto tard�o de raquitisrno	268.1
"� pelvis, pelviana (adquirida) (�sea)"	738.6
�� con desproporci�n (fetop�lvica)	653.0
��� cuando obstruye el parto	660.1
���� que afecta al feto o al reci�n nacido	763.1
��� que afecta al feto o al reci�n nacido	763.1
�� cong�nita	755.60	
�� raqu�tica (efecto tard�o)	268.1	
� pene (glande) (cong�nita)	752.9	
�� adquirida	607.89	
� pericardio (cong�nita)	746.9	
�� adquirida		v�ase Pericarditis
� pez�n (cong�nita)	757.9	
�� adquirida	611.8	
� pie (adquirida)	736.70	
�� cavovaro	736.75	
��� cong�nita	754.59	
�� cong�nita NCOC	754.70	
��� tipo especificado NCOC	754.79	
�� valgo (adquirida)	736.79	
��� cong�nita	754.60	
���� tipo especificado NCOC	754.69	
�� varo (adquirida)	736.79	
��� cong�nita	754.50	
���� tipo especificado NCOC	754.59	
� pie zopo		v�ase Pie zopo 
� piel (cong�nita)	757.9	
�� adquirida NCOC	709.8	
� pierna (inferior) (superior) (adquirida) NCOC	736.89	
�� cong�nita	755.60	
��� reducci�n 		"v�ase Deforrnidad, reducci�n, miembro inferior "
� Pierre Robin (cong�nita)	756.0	
� p�loro (cong�nita)	750.9	
�� adquirida	537.89	
� pituitaria (cong�nita)	759.2	
� postura		"v�ase Curvatura, espina "
� prepucio (cong�nita)	752.9	
�� adquirida	607.89	
� pr�stata (cong�nita)	752.9	
�� adquirida	602.8	
� pulm�n (cong�nita)	748.60	
�� adquirida	518.89	
�� tipo especificado NCOC	748.69	
� pupila (cong�nita)	743.9	
�� adquirida	364.75	
� radio (adquirida)	736.00	
�� cong�nita	755.50	
��� reducci�n		"v�ase Deformidad, reducci�n, miembro superior "
"� raqu�tica (adquirida), curada o antigua"	268.1	
� recto (cong�nita)	751.5	
�� adquirida	569.49	
� reducci�n (extremidad) (miembro)	755.4	
�� cerebro	742.2	
�� miembro inferior	755.30	
��� con ausencia completa de elementos distales	755.31
"��� longitudinal (completa) (parcial) (con deficiencias distales, incompleta)"	755.32
���� con ausencia total de elementos distales	755.31
���� falange(s)	755.39
����� cuando significa todos los d�gitos	755.31
���� femoral	755.34
"���� femoral, tibial, peroneal combinada (incompleta)"	755.33
���� metatarsiano(s)	755.38
���� peroneal  	755.37
���� tarsiano(s)	755.38
���� tibia	755.36
���� tibioperoneal	755.35
��� transversal	755.31
�� miembro superior	755.20
��� con ausencia de elementos distales	755.21
"��� longitudinal (completa) (parcial) (con deficiencias distales, incompleta)"	755.22
���� con ausencia total de elementos distales	755.21	
���� carpiano(s)	755.28	
���� cubital	755.27	
���� falange(s)	755.29	
����� cuando significa todos los d�gitos	755.21	
���� humeral	755.24	
"���� humeral, radial, cubital combinada (incompleta)"	755.23	
���� metacarpiano(s)	755.28	
���� radial	755.26	
���� radiocubital	755.25	
��� transversal (completa) (parcial)	755.21	
"� reloj de arena, en"		"v�ase Contracci�n, reloj de arena, en "
� renal		"v�ase Deformidad, ri��n "
� repliegues pleurales (cong�nita)	748.8	
� ri��n(es) (c�lices) (pelvis) (cong�nita)	753.9	
�� adquirida	593.89	
�� vaso	747.6	
��� adquirida	459.9	
� rodilla (adquirida) NCOC	736.6	
�� cong�nita	755.64	
� rotaci�n (articulaci�n) (adquirida)	736.9	
�� cong�nita	755.9	
�� muslo o cadera	736.39	
��� cong�nita	754.32	"v�ase adem�s Subluxaci�n, cong�nita, cadera"
� r�tula (adquirida)	736.6	
�� cong�nita	755.64	
� sacro (adquirida)	738.5	
�� cong�nita	756.10	
� seno de Valsalva	747.29	
� sigmoide (flexura) (cong�nita)	751.5	
�� adquirida	569.89	
"� silla de montar, en "		
�� espalda	737.8
�� nariz	738.0
��� sifil�tica	090.5
� sistema cardiovascular (cong�nita)	746.9
� sisterna circulatorio (cong�nita)	747.9
"� sistema linf�tico, cong�nita"	759.9
"� sistema musculoesquel�tico, cong�nita NCOC"	756.9
�� tipo especificado NCOC	756.9
� sistema nervioso (cong�nita)	742.9
� sistema respiratorio (cong�nita)	748.9
�� tipo especificado NCOC	748.8
"� Sprengel, de (cong�nita)"	755.52
� tabique (nasal) (adquirida)	470
�� cong�nita	748.1
� tab�que rectovaginal (cong�nita)	752.40
�� adquirida	623.8
� talipes		v�ase Talipes 
� tal�n (adquirida)	736.76	
�� cong�nita	755.67	
� test�culo (cong�nita)	752.9	
�� adquirida	608.89	
��� torsi�n 	608.2	
� tibia (adquirida)	736.89	
�� cong�nita	755.60	
�� en sable	090.5	
� timo (tejidos) (cong�nita)	759.2	
� tiroides (gl�ndula) (cong�nita)	759.2	
�� cart�lago	748.3	
��� adquirida	478.79	
� tobillo (articulaci�n) (adquirida)	736.70	
�� abducci�n	718.47	
�� cong�nita	755.69	
�� contracci�n	718.47
� t�rax (adquirida) (pared)	738.3
�� cong�nita	754.89
�� efecto tard�o de raquitismo	268.1
"� tracto digestivo, cong�nita"	751.9
�� inferior	751.5
"�� superior (cualquier parte, salvo lengua)"	750.9
��� lengua	750.10
���� tipo especificado NCOC	750.19
��� tipo especificado NCOC	750.8
�� tipo especificado NCOC	751.8
� tracto gastrointestinal (cong�nita) NCOC	751.9
�� adquirida	569.89
�� tipo especificado NCOC	751.8
� tracto o aparato urinario (cong�nita)	753.9
�� uraco	753.7
� tracto o conducto seminal (cong�nita)	752.9	
�� adquirida	608.89	
� tr�quea (anillos) (cong�nita)	748.3	
�� adquirida	519.1	
� tric�spide (valvas) (v�lvulas) (cong�nita)	746.9	
�� adquirida		"v�ase Endocarditis, tric�spide "
�� atresia o estenosis	746.1	
�� tipo especificado NCOC	746.89	
� trompa de Eustaquio (cong�nita) NCOC	744.3	
�� tipo especificado NCOC	744.24	
� trompa de Falopio (cong�nita)	752.10	
�� adquirida	620.8	
� tronco (adquirida)	738.3	
�� cong�nita	759.9	
� uraco (cong�nita)	753.7	
� ur�ter (apertura) (cong�nita)	753.9	
�� adquirida	593.89	
� uretra (v�lvula) (cong�nita)	753.9	
�� adquirida	599.8	
� �tero (cong�nita)	752.3	
�� adquirida	621.8	
� �vula (cong�nita)	750.9	
�� adquirida	528.9	
� u�a (adquirida)	703.9	
�� cong�nita	757.9	
� vagina (cong�nita)	752.40	
�� adquirida	623.8	
� v�lvula pulmonar		"v�ase Endocarditis, pulmonar "
"� v�lvula, valvular (coraz�n) (cong�nita)"	746.9	
�� adquirida - v Endocarditis 		
�� pulmonar	746.00	
�� tipo especificado NCOC	746.89	
� vascular (cong�nita) NCOC	747.6	
�� adquirida	459.9	
� vasos de papila �ptica (cong�nita)	743.9	
� vasos opticociliares (eong�nita)	743.9	
� vejiga (cuello) (esf�nter) (tr�gono) (adquirida)	596.8	
�� cong�nita	753.9	
� vena (cong�nita) NCOC	747.6	
�� cerebro	747.81	
�� coronaria	746.9	
�� grande	747.40	
� vena cava (inferior) (superior) (cong�nita)	747.40	
� vena porta (cong�nita)	747.40	
� v�rtebra		"v�ase Deforrnidad, espina "
� ves�cula biliar (cong�nita)	751.60	
�� adquirida	575.8	
� vulva (cong�nita)	752.40	
�� adquirida	624.8	
"Degeneraci�n, degenerativo "		
� almohadilla grasosa infrarotular	729.31	
� almohadilla grasosa popl�tea	729.31	
� amiloidea (cualquier sitio) (general)	277.3	
� amiloidosis serosa (cualquier sitio)	277.3	
"� aorta, a�rtica"	440.0	
�� grasosa	447.8	
�� v�lvula (coraz�n)	424.1	"v�ase adem�s Endocarditis, a�rtica"
"� arteria, arterial (aterornatosa) (calc�rea)"		v�ase adem�s Arterioesclerosis
�� amiloidea	277.3	
�� capa media	440.2	
�� lard�cea	277.3	
� arteriovascular		v�ase Arteriosclerosis 
"� articulaci�n, enfermedad de"	715.9	v�ase adem�s Osteoartrosis
�� espina	721.90	v�ase adem�s Espondilosis
�� m�ltiples sitios	715.09	
� ateromatosa		v�ase Arterioesclerosis 
� bazo	289.59	
�� am�loidea	277.3	
�� lard�cea	277.3	
"� bola letal, v�lvula cardiaca prot�sica"	996.02	
"� Bruch, membrana de"	363.40	
� cabezal de caucho silic�nico (v�lvula prot�sico)	996.02	
� calc�reo NCOC	275.4	
"� cambios, espina o v�rtebra"	721.90	v�ase adem�s Espondilosis
"� canal�culos lagrimales, qu�stica"	375.12	
� capilares	448.9	
�� amiloidea	277.3	
�� grasosa	448.9	
�� lard�cea	277.3	
� cardiaca (parda) (calc�rea) (grasosa) (fibrosa) (hialina) (mural) (muscular) (pigmentaria) (senil) (con arterioesclerosis)	429.1	"v�ase adem�s Degeneraci�n, mioc�rdica"
"�� v�lvula, valvular"		v�ase Endocarditis 
� cardiorrenal	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal"
� cardiovascular	429.2	"v�ase adem�s Enfermedad, cardiovascular"
�� renal	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal"
� cart�lago (articulaci�n)		"v�ase Desarreglo, articulaci�n "
� cart�lago articular NCOC	718.0	"v�ase adem�s Desarreglo, cart�lago, articular"
�� codo	718.02	
�� espina	721.90	v�ase adem�s Espondilosis
�� hombro	718.01	
�� rodilla	717.5	
�� r�tula	717.7	
� cart�lago semilunar interno	717.3	
"� centros motores, senil"	331.2	
� cerebelar NCOC	334.9	
�� primaria (hereditaria) (espor�dica)	334.2	
� cerebral		"v�ase Degeneraci�n, cerebro "
� cerebro (cortical) (progresiva)	331.9	
�� arterioscler�tica)	437.0	
�� centros motores	331.89	
�� cong�nita	742.4	
�� en 		
��� alcoholismo	303.9 [331.7]	
��� beriberi	265.0 [331.7]	
��� carencia de vitamina B12	266.2 [331.7]	
��� enfermedad cerebrovascular	437.9 [331.7]	
��� enfermedad neopl�sica NCOC 	239.9 [331.7] (M8000/1)	
��� esfingolipidosis	272.7 [330.2]	
"��� Fabry, enfermedad de"	272.7 [331.7]	
"��� Gaucher, enfermedad de"	272.7 [330.2]	
��� hidrocefalia cong�nita	742.3 [331.7]	
���� con espina b�fida	741.0 [331.7]	v�ase adem�s Espina b�fida
"��� Hunter, enfermedad o s�ndrome de"	277.5 [330.3]	
��� lipodosis 		
���� cerebral	330.1	
���� generalizada	272.7 [330.2]	
��� mixedema	"244,9 [331.9]"	v�ase adem�s Mixedema
��� mucopolisacaridosis	277.5 [330.3]	
"��� Niemann-Pick, enfermedad de"	272.7 [330.2]	
�� familiar NCOC	331.89	
�� heredofamiliar NCOC	331.89	
"�� infancia, de la"	330.9	
��� tipo especificado NCOC	330.8	
�� qu�stica	348.0	
��� cong�nita	742.4	
�� senil	331.2	
�� sustancia gris	330.8	
�� tipo especificado NCOC	331.89	
� cerebromacular	330.1	
� cerebrovascular	437.1	
�� debida a hipertensi�n	437.2	
�� efecto tard�o	438	v�ase categor�a
� coclear	386.8	
"� columna lateral (posterior), m�dula espinal"	266.2 [336.2]	"v�ase adem�s Degeneraci�n, combinada"
� combinada (m�dula espinal) (subaguda)	266.2 [336.2]	
�� con anemia (perniciosa)	281.0 [336.2]	
��� debida a carencia diet�tica	281.1 [336.2]	
�� debida a anemia por carencia de vitamina Bl2 (diet�tica)	281.1 [336.2]	
� conjuntiva	372.50	
�� amiloidea	277.3 [372.50]	
� coraz�n (parda) (calc�rea) (grasosa) (fibrosa) (hialina) (mural) (muscular) (pigmentaria) (senil) (con arteriosclerosis)	429.1	"v�ase adem�s Degeneraci�n, miocardica"
�� amiloidea	277.3 [425.7]	
�� ateromatosa	414.0	
�� gotosa	274.82	
�� hipertensiva	402.90	"v�ase adem�s Hipertensi�n, coraz�n"
�� isqu�mica	414.9	
"�� v�lvula, valvular"		v�ase Endocarditis 
� coriorretinal	363.40	
�� cong�nita	743.53	
�� hereditaria	363.50	
� c�rnea	371.40	
�� calcerosa	371.44	
�� en mosaico (zapa)	371.41	
�� familiar (hereditaria)	371.50	"v�ase adem�s Distrofia, c�rnea"
��� macular	371.55	
��� reticular	371.54	
�� hialina (de cicatriz antigua)	371.41	
�� marginal (de Terrien)	371.48	
�� nodular	371.46	
�� perif�rica	371.48	
�� senil	371.41	
� coroides (coloide) (drusen)	363.40
�� hereditaria	363.50
�� senil	363.41
��� secundaria difusa	363.42
� cortical (cerebelar) (parenquimatosa)	334.2
�� alcoh�lica	303.9 [334.4]
"�� difusa, debida a artenopat�a"	437.0
� corticostriada-espinal	334.8
� cretinoide	243
� cristalino	366.9
"�� infantil, juvenil, o presenil"	366.00
�� senil	366.10
� cuello uterino	622.8
�� debida a radiaci�n (efecto buscado)	622.8
�� efecto adverso o infortunio	622.8
"� cuerno anterior, m�dula espinal"	336.8
� cutis	709.3	
�� amiloideo	277.3	
� disco intervertebral	722.6	
�� con mielopat�a	722.71	
"�� cervical, cervicotor�cico"	722.4	
��� con mielopat�a	722.71	
"�� lumbar, lumbosacro"	722.52	
��� con mielopat�a	722.73	
"�� tor�cico, toracolumbar"	722.51	
��� con mielopat�a	722.72	
"� disco, enfermedad de"		"v�ase Degeneraci�n, disco intervertebral "
� dorsolateral (m�dula espinal)		"v�ase Degeneraci�n, combinada "
� endoc�rdica	424.90	
� espina dorsal	733.90	
� espinal (m�dula)	336.8	
�� amiloidea	277.3	
�� columna	733.90	
�� combinada (subaguda)	266.2 [336.2]	"v�ase adem�s Degeneraci�n, combinada"
��� con anemia (perniciosa)	281.0 [336.2]	
�� dorsolateral	266.2 [336.2]	"v�ase adem�s Degeneraci�n, combinada"
�� familiar NCOC	336.8	
�� funicular	266.2 [336.2]	"v�ase adem�s Degeneraci�n, combinada"
�� grasosa	336.8	
�� heredofamiliar NCOC	336.8	
�� posterolateral	266.2 [336.2]	"v�ase adem�s Degeneraci�n, combinada"
"�� subaguda, combinada"		
"� v�ase Degeneraci�n, combinada "		
�� tuberculosa	013.8	v�ase adem�s Tuberculosis
� est�mago	537.89	
�� lard�cea	277.3	
� estrion�grica	333.0	
� extrapiramidal NCOC	333.90	
� fasc�culo de His	426.50	
�� derecho	426.4	
�� izquierdo	426.3	
� gl�ndula linf�tica	289.3	
�� hialina	289.3	
�� lard�cea	277.3	
� gl�ndula pineal	259.8	
� gl�ndula sudor�para	705.89	
� globo (ojo) NCOC	360.40	
�� macular		"v�ase Degeneraci�n, m�cula "
� grasosa (difusa) (general)	272.8	
�� h�gado	571.8	
��� alcoh�lica	571.0	
�� placenta		"v�ase Placenta, anormal "
�� sitio localizado		
"� v�ase Degeneraci�n, por sitio, grasosa "		
� hepatolenticular (de Wilson)	275.1	
� hepatorrenal	572.4	
� heredofamillar 		
�� cerebro NCOC	331.89	
�� m�dula espinal NCOC	336.8	
� hialina (difusa) (generalizada)	728.9	
�� Iocalizada		"v�ase adem�s Degeneraci�n, por sitio "
��� c�rnea	371.41	
��� queratitis	371.41	
� h�gado (difusa)	572.8	
�� amiloidea	277.3	
�� amiloidosis serosa	277.3	
�� cong�nita (qu�stica)	751.62	
�� grasosa	571.8	
��� alcoh�lica	571.0	
�� hipertr�fica	572.8	
�� lard�cea	277.3	
"�� parenquimatosa, aguda o subaguda"	570	"v�ase adem�s Necrosis, h�gado"
�� pigmentaria	572.8	
�� qu�stica	572.8	
��� cong�nita	751.62	
�� t�xica (aguda)	573.8	
"� hipertensiva, vascular"		v�ase Hipertensi�n 
� hueso	733.90	
� humor v�treo (con infiltraci�n)	379.21	
� intestino	569.89	
�� amiloidea	277.3	
�� lard�cea	277.3	
� iris (generalizada)	364.59	"v�ase adem�s Atrofia, iris"
�� margen pupilar	364.54	
�� pigmentaria	364.53	
� isqu�mica		v�ase Isquemia 
� Kuhnt-Junius (retina)	362.52
� laberinto (�seo)	386.8
"� laberinto membranoso, cong�nita (cuando causa deterioro de la audici�n)"	744.05
� laberinto �seo	386.8
� lard�cea (cualquier sitio)	277.3
� lenticular (familiar) (progresiva) (de Wilson) (con cirrosis del h�gado)	275.1
�� arteria estriada	437.0
� ligamento 	
�� colateral (rodilla) (mediana)	717.82
��� lateral	717.81
�� cruciforme (rodilla) (posterior)	717.84
��� anterior	717.83
� ligamento colateral (rodilla) (mediana)	717.82
�� lateral	717.81
� ligamento cruciforme (rodilla) (posterior)	717.84
�� anterior	717.83
"� m�cula, macular (adquirida) (senil)"	362.50
�� atr�fica	362.51
"�� Best, de"	362.76
�� cong�nita	362.75
�� disciforme	362.52
�� exudativa	362.52
�� hereditaria	362.76
�� h�meda	362.52
�� juvenil (de Stargardt)	362.75
�� no exudativa	362.51
�� perforada	362.54
�� qu�stica	362.54
�� quistoide	362.53
�� seca	362.51
�� seudoinflamatoria familiar	362.77
�� seudoperforada	362.54
� mama		"v�ase Enfermedad, mama "
� medular		"v�ase Degeneraci�n, cerebro "
� membrana sinovial (pulposa)	727.9	
� menisco		"v�ase Desarreglo, articulaci�n "
� microquistoide	362.62	
"� mielina, sistema nervioso central NCOC"	341.9	
"� miocardio, mioc�rdica (parda) (calc�rea) (grasosa) (fibrosa) (hialina) (mural) (muscular) (pigmentaria) (senil) (con arterioesclerosis)"	429.1	
�� con fiebre reum�tica (enfermedades ciasificables bajo 390)	398.0	
"��� activa, aguda, o subaguda"	391.2	
���� con corea	392.0	
��� inactiva o quiescente (con corea)	398.0	
�� amiloidea	277.3 [425.7]	
�� cong�nita	746.89	
�� feto o reci�n nacido	779.8	
�� gotosa	274.82	
�� hipertensiva	402.90	"v�ase adem�s Hipertensi�n, coraz�n"
�� isqu�mica	414.8	
�� reum�tica	398.0	"v�ase adem�s Degeneraci�n, miocardio, con fiebre reum�tica"
�� sifil�tica	093.82	
� mitral		"v�ase Insuficiencia, mitral "
"� M�nckeberg, de"	440.2	
� moral	301.7	
� mural	429.1	"v�ase adem�s Degeneraci�n, mioc�rdica"
"�� coraz�n, cardiaca"	429.1	"v�ase adem�s Degeneraci�n, mioc�rdica"
"�� miocardio, mioc�rdica"	429.1	"v�ase adem�s Degeneraci�n, mioc�rdica"
� m�sculo	728.9	
�� coraz�n	429.1	"v�ase adem�s Degeneraci�n, mioc�rdica"
�� fibrosa	728.9	
�� grasosa	728.9	
�� hialina	728.9	
� m�sculo papilar	429.81	
� nariz	478.1	
� nervio		"v�ase Trastorno, nervio "
� n�cleos o ganglios basales NCOC	333.0	
"� oculoacusticocerebral, congenita (progresiva)"	743.8	
� ojo NCOC	360.40	
�� macular	362.50	"v�ase adem�s Degeneraci�n, m�cula"
��� cong�nita	362.75	
��� hereditaria	362.76	
� olivopontocerebelar (familiar) (hereditaria)	333.0	
� ovario	620.8	
�� microqu�stico	620.2	
�� qu�stico	620.2	
"� palidal, pigmentaria (progresiva)"	333.0	
� p�ncreas	577.8	
�� tuberculosa  	017.9	(v�ase adem�s Tuberculosis) 
� paving stone	362.61	
� pene	607.89	
� peritoneo	568.89	
� pez�n	611.9	
� piel	709.3	
�� amiloidea	277.3	
�� coloide	709.3	
� pigmentaria (difusa) (general) 		
�� localizada		"v�ase Degeneraci�n, por sitio "
�� palidal (progresiva)	333.0	
�� secundaria	362.65	
� pituitaria (gl�ndula)	253.8	
� placenta (grasosa) (fibrinoide) (fibroide) 		"v�ase Placenta, anormal"
� plexo braquial	353.0	
� plexo cervical	353.2	
� posterolateral (m�dula espinal)	266.2 [336.2]	"v�ase adem�s Degeneraci�n, combinada"
� progresiva muscular	728.2	
� pulm�n	518.89	
� pulpa (diente)	522.2	
� pulpa dental	522.2	
� quitinosa	277.3	
� reborde pupilar	364.54	
� renal	587	"v�ase adem�s Esclerosis, renal"
�� fibroqu�stica	753.19	
�� poliqu�stica	753.12	
��� autos�mico dominante	753.13	
��� autos�mico recesivo	753.14	
��� tipo adulto (APKD)	753.13	
��� tipo infantil (CPKD)	753.14	
� reticular	362.63	
� retina (perif�rica)	362.60	
�� con defecto retinal	361.00	"v�ase adem�s Desprendimiento, retina, con defecto retinal"
"�� empalizada, en forma de"	362.63	
�� hereditaria	362.70	"v�ase adem�s Distrofia, retina"
��� cerebrorretinal	362.71	
��� cong�nita	362.75	
��� juvenil (de Stargardt) 	362.75	
��� m�cula	362.76	
�� Kuhnt-Junius	362.52	
"�� losas, en forma de"	362.61	
�� macular	362.50	"v�ase adem�s Degeneraci�n, m�cula"
�� microquistoide	362.62	
�� pigmentaria (primaria)	362.74	
��� secundaria	362.65	
�� polo posterior 	362.50	"v�ase adem�s Degeneraci�n, m�cula"
�� qu�stica (senil)	362.50	
�� quistoide	362.53	
�� reticular	362.63	
�� secundaria	362.66	
�� senil	362.60	
��� qu�stica	362.53	
��� reticular	362.64	
� ri��n	587	"v�ase adem�s Esclerosis, renal"
�� amiloidea	277.3 [583.81]	
�� cerosa	277.3 [583.81]	
�� fibroqu�stica (cong�nita)	753.19	
�� grasosa	593.89	
�� lard�cea	277.3 [583.81]	
�� poliqu�stica (cong�nita)	753.12	
��� autos�mica dominante	753.13	
��� autos�mica recesiva	753.14	
��� tipo adulto (APKD)	753.13	
��� tipo infantil (CPKD)	753.14	
"�� quiste, qu�stica (m�ltiple) (solitario)"	593.2	
��� cong�nita	753.10	
"� s�culo del o�do, cong�nita (cuando causa deterioro de la audici�n)"	744.05	
� saculococlear	386.8	
� senil	797	
"�� cardiaca, coraz�n, o rniocardio"	429.1	"v�ase adem�s Degeneraci�n, mioc�rdica"
�� centros motores	331.2	
�� cerebro	331.2	
�� ret�cula	362.64	
"�� retina, qu�stica"	365.50	
�� vascular		v�ase Arteriosclerosis 
� seno (qu�stica)	473.9	v�ase adem�s Sinusitis
�� polipoide	471.1	
� seno nasal (mucosa)	473.9	v�ase adem�s Sinusitis
�� frontal	473.1	
�� maxilar	473.0	
� sistema nervioso	349.89	
�� amiloidea	277.3 [357.4]	
�� aut�nomo	337.9	"v�ase adem�s Neuropat�a, perif�rica, aut�noma"
�� grasosa	349.89	
�� perif�rico aut�noma NCOC	337.9	"v�ase adem�s Neuropat�a, perif�rica, aut�noma"
� sistema reticuloendotelial	289.8	
� sudor�para (qu�stica)	705.89	
� suprarrenal (c�psula) (gl�ndula)	255.8	"v�ase adem�s Degeneraci�n, suprarrenal"
�� con hipofunci�n	255.4	
� suprarrenal (c�psula) (gl�ndula)	255.8	
�� con hipofunci�n	255.4	
�� grasa	255.8	
�� hialina	255.8	
�� infecciosa	255.8	
�� lard�cea	277.3	
� sustancia gris	330.8	
� tapetorretinal	362.74	
�� forma adulta o presenil	362.50	
� test�culo (posinfecciosa)	608.89	
� timo (gl�ndula)	254.8	
�� grasosa	254.8	
�� lard�cea	277.3	
� tiroides (gl�ndula)	246.8	
� tric�spide (coraz�n) (v�lvula)		"v�ase Endocarditis, tric�spide "
� tuberculosa NCOC	011.9	v�ase adem�s Tuberculosis
� turbinados	733.90	
� �tero	621.8	
�� qu�stico	621.8	
� v�lvula pulmonar (coraz�n)	424.3	"v�ase adem�s Endocarditis, pulmonar"
� vascular (senil)		v�ase adem�s Arteriosclerosis 
�� hipertensiva		
� v�ase Hipertensi�n 		
� vitreorretinal (primaria)	362.73	
�� secundaria	362.66	
� walleriana NCOC		"v�ase Desarreglo, nervio "
"� Wilson, de, hepatolenticular"	275.1	
Degluci�n 		
� neumon�a	507.0	
� par�lisis	784.9	
�� hist�rica	300.11	
"Deglutido, degluci�n "		
� cuerpo extra�o NCOC	938	v�ase adem�s Cuerpo extrano
� dificultad	787.2	v�ase adem�s Disfagia
"Degos, enfermedad o s�ndrome de"	447.8	
"Degradaci�n, trastorno de, amino�cidos de cadena ramificada"	270.3	
Dehiscencia		
� anastomosis		"v�ase Complicaciones, anastomosis "
� episiotom�a	674.2	
� herida de ces�rea	674.1	
� herida operatoria	998.3	
� herida perineal (posparto)	674.2	
� herida uterina	674.1	
� posoperatoria	998.3	
�� abdomen	998.3	
"Deiter, s�ndrome de n�cleo de"	386.19	
"D�j�rine, enfermedad de"	356.0	
"D�j�rine-Klumpke, par�lisis de"	767.6	
"D�j�rine-Roussy, s�ndrome de"	348.8	
"D�j�rine-Sottas, enfermedad o neuropat�a de (hipertr�fica)"	356.0	
"D�j�rine-Thomas, atrofia o s�ndrome de"	333.0	
"Del Castillo, s�ndrome de (aplasia germinal) "	606.0	
"Deleage, enfernledad de"	359.8	
Delhi (fur�nculo) (bot�n) (�lcera)	085.1	
Delincuencia (juvenil)	312.9	
� grupo	312.2	"v�ase adem�s Perturbaci�n, conducta"
� neur�tica	312.4	
"Delirio, delirante"	780.0	
� agotamiento	308.9	"v�ase adem�s Reacci�n, tensi�n, aguda"
� agudo (psic�tico)	293.0	
� alcoh�lico	291.0	
�� agudo	291.0	
�� cr�nico	291.1	
� cr�nico	293.89	v�ase adem�s Psicosis
�� debido a o asociado con estado f�sico  		"v�ase Psicosis, org�nica"
�� ecl�mptico	780.3	v�ase adem�s Eclampsia
�� inducido por droga	292.81	
� en 		
�� demencia presenil	290.11	
�� demencia senil	290.3	
� hist�rica	300.11	
� inducido por droga	292.81	
� man�aco (agudo)	296.0	"v�ase adem�s Psicosis, afectiva"
�� episodio recurrente	296.1	
�� episodio �nico	296.0	
� privaci�n 		
�� alcoh�lica (aguda) 	291.0	
��� cr�nica	291.1	
"�� droga, f�rmaco"	292.0	
� puerperal	293.9	
� senil	290.3	
� subagudo (psic�tica)	293.1	
� tiroideo	242.9	v�ase adem�s Tirotoxicosis
� traum�tico		"v�ase adem�s Lesi�n, intracraneal "
�� con 		
"��� choque, espinal"		"v�ase Lesi�n, espinal, por sitio "
"��� lesi�n, m�dula espinal"		"v�ase Lesi�n, espinal, por sitio "
� tremens (inminente)	291.0	
� ur�mico		v�ase Uremia
"Dellen, c�rnea"	371.41	
Delusiones (paranoides)	297.9	
� grandiosas	297.1	
� parasitosis	300.29	
� sistematizadas	297.1	
"Demasiado concienzuda, personalidad"	301.4	
Demencia	298.9	v�ase adem�s Psicosis
� alcoh�lica	291.2	"v�ase adem�s Psicosis, alcoh�lica"
"� Alzheimer, de"		"v�ase Alzheimer, demencia de "
� arterioscler�tica (tipo simple) (no complicada)	290.40	
�� con 		
��� caracter�sticas delirantes	290.42	
��� caracter�sticas depresivas	290.43	
��� delirio	290.41	
��� estado confusional agudo	290.41	
�� tipo deprimido  	290.43	
�� tipo paranoide	290.42	
"� Binswanger, de"	290.12	
� catat�nica (aguda)	295.2	v�ase adem�s Esquizofrenia
� cong�nita	319	"v�ase adem�s Retardo, mental"
� debida a o asociada con estado(s) clasificado(s) bajo otros conceptos	294.1	
�� esclerosis m�ltiple	294.1	
�� poliarteritis nudosa	294.1	
� degenerativa	290.9	
�� ataque presenil		"v�ase Demencia, presenil "
�� ataque senil		"v�ase Demencia, senil "
� del desarrollo	295.9	v�ase adem�s Esquizofrenia
� di�lisis	294.8	
�� transitorio	293.9	
� en 		
"�� Alzheimer, enfermedad de"	290.10	
�� corea de Huntington	294.1	
�� degeneraci�n hepatolenticular	294.1	
�� enfermedad arterioscler�tica cerebral	290.40	
�� epilepsia	294.1	
�� esclerosis m�ltiple	294.1	
�� Jakob- Creutzfeldt	290.10	
�� lipidosis cerebral	294.1	
�� neuros�filis	294.1	
�� par�lisis general del demente	294.1	
"�� Pelizaeus-Merzbacher, enfermedad de"	294.1	
"�� Pick, enfermedad de"	290.10	
�� poliarteritis nudosa	294.1	
�� senilidad	290.0	
"�� Wilson, enfermedad de"	294.1	
� esquizofr�nica	295.9	v�ase adem�s Esquizofrenia
� hebefr�nica (aguda)	295.1	
"� Heller, de (psicosis infantil)"	299.1	"v�ase adem�s Psicosis, infancia"
� idiop�tica	290.9	
�� ataque presenil		"v�ase Demencia, presenil "
�� ataque senil		"v�ase Demencia, senil "
� inducida por droga	292.82	
� infantil	299.0	"v�ase adem�s Psicosis, infantil"
� infarto m�ltiple (cerebrovascular)	290.40	"v�ase adem�s Demencia, arterioscler�tica"
� parafr�nica	295.3	v�ase adem�s Esquizofrenia
� paral�tica	094.1	
�� forma tab�tica	094.1	
�� juvenil	090.40	
�� sifil�tica	094.1	
��� cong�nita	090.40	
� paranoide	295.3	v�ase adem�s Esquizofrenia
"� par�sica, par�tica"	094.1	
� precoz	295.9	v�ase adem�s Esquizofrenia
� presenil	290.10	
�� con 		
��� caracter�sticas delirantes	290.12	
��� caracter�sticas depresivas	290.13	
��� delirio	290.11	
��� estado confusional agudo	290.11	
�� no complicada	290.10	
�� tipo deprimido	290.13	
�� tipo paranoide	290.12	
�� tipo simple	290.10	
� primaria (aguda)	295.0	v�ase adem�s Esquizofrenia
"� progresiva, sifil�tica"	094.1	
� puerperal		"v�ase Psicosis, puerperal "
� senil	290.0	
�� con 		
��� caracter�sticas delirantes	290.20	
��� caracter�sticas depresivas	290.21	
��� delirio	290.3	
��� estado confusional agudo	290.3	
�� agotamiento	290.0	
�� tipo deprimido	290.21	
�� tipo paranoide	290.20	
� simple (aguda)	295.0	v�ase adem�s Esquizofrenia
� tipo simple (aguda)	295.0	v�ase adem�s Esquizofrenia
� ur�mica		v�ase Uremia 
� vejez	290.0	
"Demerol, dependencia de"	304.0	v�ase adem�s Dependencia
Demodex folliculorum (infestaci�n)	133.8	
"De Morgan, manchas de (angiornas seniles)"	448.1	
Dengue (fiebre)	061	
� jej�n	061	
"� vacunaci�n, profil�ctica (contra)"	V05.1	
� virus de fiebre hemorr�gica	065.4	
� evaginatus	520.2	
� in dente	520.2	
� invaginatus	520.2	
Densidad 		
� aumentada de los huesos (diseminada) (focal) (generalizada)	733.99	
� pulm�n (nodular)	518.89	
"Dentada, lengua"	529.8	
"Dentadura, �lcera de (boca)"	528.9	
Dental		v�ase adem�s enfermedad espec�fica 
� reconocimiento s�lo	V72.2	
Dentici�n	520.7	
� anomal�a	520.6	
� anormal	520.6	
� demorada	520.6	
� dif�cil	520.7	
� precoz	520.6	
� retardada	520.6	
� trastorno de	520.6
Dentici�n	520.7
� s�ndrome de	520.7
Dent�culos (en pulpa)	522.2
"Dent�gero, quiste"	526.0
Dentina  	
