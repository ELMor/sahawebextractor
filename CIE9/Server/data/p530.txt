Neuroparal�tico 		v�ase enfermedad espec�fica
"Neuropatia, neurop�tico"	355.9	"v�ase adem�s Trastorno, nervio"
� alcoh�lica	357.5	
�� con psicosis	291.1	
� aut�noma (perif�rica) 		"v�ase Neuropat�a, perif�rica, aut�noma "
"� axilar, nervio"	353.0	
"� braquial, plexo"	353.0	
� brazo NCOC	354.9	
"� cervical, plexo"	353.2	
� ci�tica	355.0	
� compresi�n de nervio	355.9	
�� cubital	354.2	
�� cut�neo lateral del muslo (femorocut�neo)	355.1	
�� iliohipog�strico	355.7	
�� ilioinguinal	355.7	
�� mediano	354.0	
�� obturador	355.7	
�� peroneal	355.3	
�� safeno	355.7	
�� tibial posterior	355.5	
� cr�nica 		
�� progresiva segmentaria desmielinizante	357.8	
�� recurrente desmielinizante	357.8	
"� cubital, nervio"	354.2	
� D�j�rine-Sottas	356.0	
� diab�tica	250.6 [357.2]	
"� espinal, nervio NCOC"	355.9	
�� ra�z	729.2	v�ase adem�s Radiculitis
� extremidad 		
�� inferior NCOC	355.8	
�� superior NCOC	354.9	
"� facial, nervio"	351.9	
� hereditaria	356.9	
�� perif�rica	356.0	
�� sensorial (radicular)	356.2	
� hipertr�fica 		
�� Charcot-Marie-Tooth	356.1	
�� D�j�rine-Sottas	356.0	
�� intersticial	356.9	
�� Refsum	356.3	
"� hipertr�fica, progresiva intersticial"	356.9	
"� intercostal, nervio"	354.8	
� isqu�mica		"v�ase Trastorno, nervio "
"� Jamaica, de (jengibre)"	357.7	
"� lumbar, plexo"	353.1	
"� mediano, nervio"	354.1	
� m�ltiple (aguda) (cr�nica)	356.9	v�ase adem�s Polineuropat�a
� nervios plantares	355.6	
� �ptica	377.39	
�� isqu�mica	377.41	
�� nutricional	377.33	
�� t�xica	377.34	
� perif�rico (nervio)	356.9	v�ase adem�s Polineuropat�a
�� aut�noma	337.9	
��� amiloidea	277.3 [337.1]	
��� en 		
���� amiloidosis	277.3 [337.1]	
���� diabetes mellitus	250.6 [337.1]	
���� enfermedades clasificadas bajo otros conceptos	337.1	
���� gota	274.89 [337.1]	
���� hipertiroidismo	242.9 [337.1]	
��� idiop�tica	337.0	
�� brazo NCOC	354.9	
�� debida a 		
��� agente t�xico NCOC	357.7	
��� ars�nico	357.7	
��� compuestos de organofosfatos	357.7	
��� drogas	357.6	
��� plomo	357.7	
��� suero antitet�nico	357.6	
�� en enfermedades clasificadas bajo otro concepto 		"v�ase Polineuropat�a, en "
�� extremidad 		
��� inferior NCOC	355.8	
��� superior NCOC	354.9	
�� hereditaria	356.0	
�� idiop�tica	356.9	
��� progresiva	356.4	
��� tipo especificado NCOC	356.8	
�� pierna NCOC	355.8	
� pierna NCOC	355.8	
� radicular NCOC	729.2	
�� braquial	723.4	
�� cervical NCOC	723.4	
�� lumbar	724.4	
�� lumbosacra	724.4	
�� sensorial hereditaria	356.2	
�� tor�cica NCOC	724.4	
"� sacro, plexo"	353.1	
� sensorial cong�nita	356.2	
� t�xica	357.7	
"� trig�mino, sensorial"	350.8	
� ur�mica	585 [357.4]	
� vitamina Bl2	266.2 [357.4]	
�� con anemia (perniciosa)	281.0 [357.4]	
��� debida a carencia diet�tica	281.1 [357.4]	
Neuroptisis 		"v�ase adem�s Trastorno, nervio "
� perif�rica	356.9	
�� diab�tica	250.6 [357.2]	
Neurorretinitis	363.05	
� sifil�tica	094.85	
Neurosarcoma 	(M9540/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
Neurosclerosis 		"v�ase Trastorno, nervio"
Neuros�filis (arrestada) (inactiva) (latente) (precoz) (recurrente) (tard�a)	094.9	
� con ataxia (cerebelar) (esp�stica) (espinal) (locomotora)	094.0	
� aneurisma	094.89	
� aracnoidea (adherente)	094.2	
� arteritis (cualquier arteria)	094.89	
� asintom�tica	094.3	
� atrofia �ptica	094.84	
� cong�nita	090.40	
� duramadre	094.89	
"� general, paresia"	094.1	
� goma	094.9	
� hemorr�gica	094.9	
� juvenil (asintom�tica) (men�ngea)	090.40	
� leptomeninges (as�ptica)	094.2	
� men�ngea	094.2	
� meninges (adherente)	094.2	
� meningitis aguda	094.2	
� meningovascular (difusa)	094.2	
� naturaleza o sitio especificado NCOC	094.89	
� parenquimatosa (degenerativa)	094.1	
� paresia	094.1	"v�ase adem�s Paresia, general"
� par�tica	094.1	"v�ase adem�s Paresia, general"
� recidiva	094.9	
� remisi�n (sostenida)	094.9	
� serol�gica	094.3	
� tabes (dorsal)	094.0	
�� juvenil	090.40
� tab�tica	094.0
�� juvenil	090.40
� taboparesia	094.1
�� juvenil	090.40
� trombosis	094.89
� vascular	094.89
"Neurosis, neur�tico"	300.9
� accidente	300.16
� ambiental	300.89
� ananc�stica	300.3
� ansiedad (estado)	300.3
�� generalizada	300.02
�� tipo p�nico	300.01
� ast�nica	300.5
� cardiaca (refleja)	306.2
� cardiovascular	306.2
"� climat�rica, tipo no especificado"	627.2
� colon	306.4
� compensaci�n	300.16
"� compulsiva, compulsi�n"	300.3
� conversi�n	300.11
� coraz�n	306.2
� cut�nea	306.3
� depresiva (reacci�n) (tipo)	300.4
� despersonalizaci�n	300.6
� endocrina	306.6
� estado	300.9
�� con episodio de despersonalizaci�n	300.6
� est�mago	306.4
� faringe	306.1
� fatiga	300.5
� ferrocarril	300.16	
� f�bica	300.20	
� funcional	306.9	"v�ase adem�s Trastorno, psicosom�tico"
� g�strica	306.4	
� gastrointestinal	306.4	
� genitourinaria	306.50	
� guerra	300.16	
� hipocondr�aca	300.7	
� hist�rica	300.10	
�� tipo 		
��� conversi�n	300.11	
��� disociativo	300.15	
� impulsiva	300.3	
� incoordinaci�n	306.0	
�� cuerda vocal	306.1	
�� laringe	306.1	
� intestino	306.4	
� Iaringe	306.1	
�� hist�rica	300.11	
�� sensorial	306.1	
"� menopausia, tipo no especificado"	627.2	
� mixta NCOC	300.89	
� nave	300.89	
� obsesiva	300.3	
�� fobia	300.3	
� obsesivocompulsiva	300.3	
� ocular	306.7	
� ocupacional	300.89	
� oral	307.0	
� �rgano	306.9	"v�ase adem�s Trastorno, psicosom�tico"
� osteo-mio-articular	306.0	
� postraum�tica (aguda) (situacional)	308.3	
�� cr�nica	309.81	
� psicast�nica (tipo)	300.89	
� recto	306.4	
� respiratoria	306.1	
� rum�a	306.4	
� senil	300.89	
� sexual	302.70	
� situacional	300.89	
� tipo especificado NCOC	300.89	
� vasomotora	306.2	
� vejiga	306.53	
� visceral	306.4	
Neurospongioblastosis difusa	759.5	
Neur�tico	300.9	v�ase adem�s Neurosis
� excoriaci�n	698.4	
�� psic�gena	306.3	
Neurotmesis 		"v�ase Traumatismo, nervio, por sitio"
Neurotoxemia 		v�ase Toxemia
Neutroclusi�n	524.2	
"Neutrofilia, gigante hereditaria"	288.2	
"Neutropenia, neutrop�nico (cr�nica) (c�clica) (espl�nica) (esplenomegalia) (gen�tica) (idiop�tica) (inducida por drogas) (infantil) (inmune) (maligna) (peri�dica) (perniciosa) (primaria) (t�xica)"	288.0	
� cong�nita (no transitoria)	288.0	
� hipopl�sica cr�nica	288.0	
"� neonatal, transitoria (isoinmune) (transferencia materna)"	776.7	
Nevocarcinoma 	(M8720/3)	v�ase Melanoma
Nevus 	(M8720/0)	"v�ase adem�s Neoplasia, niel. benigna "
� acant�tico	702.8	
� acr�mico 	(M8730/0)	
� amelan�tico 	(M8730/0)	
� an�mico	709.0	
� angiomatoso 	228.00(M9120/0)	v�ase adem�s Hemangioma
"� ar�cneo, en ara�a"	448.1	
"� ara�a, en forma de"	448.1	
� avascular	709.0	
� azul 	(M8780/0)	
�� celular 	(M8790/0)	
�� gigante 	(M8790/0)	
"�� Jadassohn, de "	(M8780/0)	
�� maligno 	(M8780/3)	v�ase Melanoma 
� blanco esponjoso (mucosa oral)	750.26	
� capilar 	228.00(M9131/0)	v�ase adem�s Hemangioma
� cavernoso 	228.00(M9121/0)	v�ase adem�s Hemangioma
� celular 	(M8720/0)	
�� azul 	(M8790)	
� c�lulas 		
�� bal�n 	(M8722/0)	
�� epiteloides (y fusiformes) 	(M8770/0)	
�� fusiformes (y epiteloides) 	(M8770/0)	
� comed�nico	757.33	
� compuesto 	(M8760/0)	
� conjuntiva 	224.3 (M8720/0)	
� cuando significa hemangioma 	228.00 (M9120/0)	v�ase adem�s Hemangioma
� d�rmico 	(M8750/0)	
�� y epid�rmico 	(M8760/0)	
� estelar	448.1	
� flammeus	757.32	
�� osteohipertr�fico	759.89	
� fresa	757.32	
� halo 	(M8723/0)	
� hemangiomatoso 	228.00 (M9120/0)	v�ase adem�s Hemangioma
� intrad�rmico 	(M8750/0)	
� intraepid�rmico 	(M8740/0)	
� involutivo 	(M8724/0)	
"� Jadassohn, de (azul) "	(M8780/0)	
� juvenil 	(M8770/0)	
� linf�tico 	228.1(M9170/0)	
� magnocelular 	(M8726/0)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, benigna "
�� sitio no especificado	224.0	
� maligna 	(M8720/3)	v�ase Melanoma 
� melan�tico (pigmentado) 	(M8720/0)	
"� mucosa oral, blanco esponjoso"	750.26	
� m�ltiple	759.5	
� no neopl�sico	448.1	
� no pigmentado 	(M8730/0)	
� no vascular 	(M8720/0)	
"� osteohipertr�fico, flammeus"	759.89	
� papilar 	(M8720/0)	
� papil�fero siringocistadenomatoso 	(M8406/0)	
� papilomatoso 	(M8720/0)	
� pigmentado 	(M8720/0)	
�� gigante 	(M8761/1)	"v�ase adem�s Neoplasia, piel comportamiento incierto "
��� melanoma maligno en 	(M8761/3)	v�ase Melanoma 
�� sistem�tico	757.33	
� piloso 	(M8720/0)	
� sangu�neo	757.32	
� seb�ceo (senil)	702.8	
� senil	448.1	
� traje de ba�o 	238.2 (M8761/1)	
� unilateral	757.33	
� uni�n 	(M8740/0)	
�� melanoma maligno en 	(M8740/0)	v�ase Melanoma 
"� Unna, de"	757.32	
� vascular	757.32	
� velloso 	(M8720/0)	
� verrugoso	757.33	
� vino de oporto	757.32	
"Newcastle, conjuntivitis o enfermedad de"	077.8	
"Nezelof, s�ndrome de (alinfocitosis pura)"	279.13	
"Niacin (amida), carencia de"	265.2	
"Nicolas Durand-Favre, enfermedad de (bub�n clim�tico)"	099.1	
"Nicolas-Favre, enfermedad de (bub�n clim�tico)"	099.1	
Nictalop�a	368.60	"v�ase adem�s Ceguera, nocturna"
� carencia de vitamina A	264.5	
Nicturia	788.4	
� psic�gena	306.53	
"Niemann-Pick, enfermedad de (esplenomegalia) (histiocitosis l�pida)"	272.7	
"Nieve, ceguera de la"	370.24	
"Nigua, enfermedad de"	134.1	
Niguas	133.8	
Ninfoman�a	302.89	
"Nisbet, chancro de"	099.0	
"Nishimoto (-Takeuchi), enfermedad de"	437.5
Nistagmus	379.50
� asociado con trastornos del aparato vestibular	379.54
"� central, posicional"	386.2
� cong�nito	379.51
� disociado	379.55
� especificado NCOC	379.56
� latente	379.52
"� mineros, de los"	300.89
"� paroxismal benigno, posicional"	386.11
� posicional 	
�� central	386.2
�� paroxismal benigno	386.11
� privaci�n	379.53
�� visual	379.53
� vestibular	379.54
"Nitritoide, crisis o reacci�n "		"v�ase Crisis, nitritoide"
"Nitr�geno, retenci�n extrarrenal de"	788.9	
Nitrosohemoglobinemia	289.8	
Ni�o 		
� cuyo comportamiento que causa preocupaci�n	V61.20	
Njovera	104.0	
"No desarrollado, falta de desarrollo "		v�ase adem�s Hipoplasia 
� cerebral (cong�nita)	742.1	
� cerebro (cong�nita)	742.1	
� coraz�n	746.89	
� feto o reci�n nacido	764.9	
� pulm�n	748.5	
� test�culo	257.2	
� �tero	259.0	
No descendido		"v�ase adem�s Malposici�n, cong�nita "
� ciego	751.4	
� colon	751.4	
� test�culo	752.5	
No diagnosticada (enfermedad)	799.9	
No disponibilidad de facilidades m�dicas (en)	V63.9	
� cl�nica para pacientes no internados	V63.0	
� debida a 		
�� falta de servicios en el hogar	V63.1	
�� investigaci�n por agencia de servicios sociales	V63.1	
�� lejan�a de las instalaciones	V63.0	
�� lista de espera	V63.2	
� hogar	V63.1	
� motivo especificado NCOC	V63.8	
"No extra�da, ra�z dental"	525.3	
"No inhibida, vejiga (neur�gena)"	344.61	
No rotaci�n 		"v�ase Mala, rotaci�n "
No uni�n de 		
� fractura	733.82	
"� �rgano o sitio, cong�nita NCOC "		"v�ase Imperfecto, cierre "
"� sacro superior, cong�nita"	756.19	
"� s�nfisis del pubis, cong�nita"	755.69	
Nocardiasis 		v�ase Nocardiosis
Nocardiosis	039.9	
� con neumon�a	039.1	
� pulm�n	039.1	
� tipo especificado NCOC	039.8	
"Noche(de), nocturno "		
� calambre	729.82	
� ceguera	368.60	"v�ase adem�s Ceguera, nocturna"
�� carencia de vitamina A	264.5	
�� cong�nita	368.61	
� sudores	780.8	
"� terrores, ni�o"	307.46	
Nocivo 	
"� alimentos, envenenamiento por "	
�� champi�ones	988.1
�� hongos	988.1
�� mariscos	988.0
�� pescado	988.0
�� plantas (alimenticias)	988.2
�� setas	988.1
��� venenosas	988.1
�� tipo especificado NCOC	988.8
"� presuntamente, cuando afecta la atenci�n del embarazo"	655.5
� sustancias transmitidas a trav�s de la placenta o leche materna	760.70
�� agente(s) 	
��� alucin�genos NCOC	760.73
��� antiinfecciosos	760.74
��� especificado NCOC	760.79
��� medicamentosos NCOC	760.79	
�� alcohol	760.71	
�� anest�sicos o analg�sicos obst�tricos	763.5	
�� coca�na	760.75	
�� crack	760.75	
�� narc�ticos	760.72	
Nocturna 		v�ase adem�s enfermedad espec�fica 
� disnea (paroxismal)	786.09	
� emisi�n de semen	608.89	
� enuresis	788.3	
�� psic�gena	307.6	
� micci�n frecuente	788.4	
�� psic�gena	306.53	
"Nodal, trastorno de ritmo"	427.89	
"N�dulo linf�tico mucocut�neo, s�ndrome de (agudo) (febril) (infantil)"	446.1	
"N�dulo(s), nodular "		
� actinomic�tico	039.9	v�ase adem�s Actinomicosis
� artr�tico 		"v�ase Artritis, nudosa "
"� cantantes, de los"	478.5	
� cuerdas vocales	478.5	
� cut�nea	782.2	
� escroto (inflamatorio)	608.4	
"� Haygarth, de"	715.04	
"� Heberden, de"	715.04	
� inflamatorio 		v�ase Inflamaci�n 
� laringe	478.79	
� Iaringe	478.79	
� lechoso	051.1	
� linf�tico(s) 		v�ase enfermedad espec�fica 
"� orde�adores, de los"	051.1	
"� Osler, de"	421.0	
� piel NCOC	782.2	
� pr�stata	600	
"� pulm�n, solitario"	518.89	
�� enfisematoso	492.8	
� reum�tico	729.89	
� reumatoide 		"v�ase Artritis, reumatoide "
"� Schmorl, de"	722.30	
"�� lumbar, lumbosacro"	722.32	
�� regi�n especificada NCOC	722.39	
"�� tor�cico, toracolumbar"	722.31	
� solitario del pulm�n	518.89	
�� enfisematoso	492.8	
� subcut�neo	782.2	
� tiroideo (gl�ndula) (no t�xico) (uninodular)	241.0	
� con 		
��� hipertiroidismo	242.1	
��� tirotoxicosis	242.1	
�� t�xico o con hipertiroidismo	242.1	
� tuberculoso 		"v�ase Tuberculosis, ganglio linf�tico "
� yuxtaarticular	102.7	
�� frambesia	102.7	
�� sifil�tico	095.7	
N�dulos de los nudillos o de Garrod	728.79	
Noma (gangrenoso) (hospitalario) (infeccioso)	528.1	
� boca	528.1	
� pabell�n de la oreja	785.4	v�ase adem�s Gangrena
� pudendo	616.10	v�ase adem�s Vulvitis
� vulvar	616.10	v�ase adem�s Vulvitis
Nomadismo	V60.0	
"Nonne-Milroy-Meige, s�ndrome de (edema hereditaria cr�nica)"	757.0	
Normablastosis	289.8	
Normal 		
� estado (no se demuestra la enfermedad temida)	V65.5	
� menstruaci�n	V65.5	
� parto 	650	v�ase categor�a 650
"Normoc�tica, anemia (infecciosa)"	285.9	
� debida a p�rdida de sangre (cr�nica)	280.0	
�� aguda	285.1	
"Norrie, enfermedad de (cong�nita) (degeneraci�n oculoacusticocerebral progresiva)"	743.8	
"Noruega, picor o prurito de"	133.0	
Nosofobia	300.29	
Nosoman�a	298.9	
Nostalgia	309.89	
"Nostalgia, morri�a, a�oranza"	309.89	
"Nothnagel, de "		
� acroparestesia vasomotora	443.89	
� s�ndrome	378.52	
"Novillos, hacer, infancia "		"v�ase adem�s Perturbaci�n, conducta "
� infrasocializada	312.1	
� socializada	312.2	
"Novy,"		
fiebre recurrente de (americana)	087.1	
"Nublado(s), antro(s)"	473.0	
N�cleo pulposo 		v�ase enfermedad espec�fica
"Nudillos, masas grasosas (de Garrod)"	728.79	
Nudo(s) 		
� cord�n umbilical (verdadero)	663.2	
�� que afecta al feto o al reci�n nacido	762.5	
"� intestinal, s�ndrome de (v�lvulo)"	560.2	
"� surfers, de los"	919.8	
�� infectados	919.9	
Nudosidades de Haygarth	715.04	
"Nuez moscada, h�gado en"	573.8	
Nutrici�n excesiva	783.6	v�ase adem�s Alimentaci�n excesiva
"Nutrici�n, deficiente o insuficiente (tipo de alimento determinado)"	269.9	
� debida a 		
�� alimentaci�n insuficiente	994.2	
�� falta de 		
��� alimentaci�n	994.2	
��� cuidados	995.5	
"O nyong-nyong, fiebre"	066.3	
"Obermeyer, fiebre recurrente (europea) de"	087.0	
Obesidad (constitucional) (ex�gena) (familiar) (nutricional) (simple)	278.0	
� debida a hiperalimentaci�n	278.0	
� del embarazo	646.1	
� endocrina NCOC	259.9	
� end�gena	259.9	
� Fr�hlich (distrofia adiposogenital)	253.8	
� glandular NCOC	259.9	
� hipotiroidea	244.9	v�ase adem�s Hipotiroidismo
� pituitaria	253.8	
� suprarrenal	255.8	
� tiroidea	244.9	v�ase adem�s Hipotiroidismo
Oblicua 		v�ase adem�s enfermedad espec�fica 
"� presentaci�n antes del trabajo del parto, cuando afecta al reci�n nacido"	761.7	
"Oblicuidad, pelvis"	738.6	
Obliteraci�n 		
� aorta 		
�� abdominal	446.7	
�� ascendente	446.7	
� ap�ndice (abertura)	543.9	
� arteria	447.1	
� conducto 		
�� c�stico	575.8	
"��� con c�lculo, coledocolitiasis o piedras"		v�ase Coledocolitiasis 
�� com�n	576.8	
"��� con c�lculo, coledocolitiasis, o piedras"		v�ase Coledocolitiasis 
��� cong�nita	751.61	
� conductos biliares	576.8	
"�� con c�lculo, coledocolitiasis, o piedras"		v�ase Coledocolitiasis 
�� cong�nita	751.61	
��� ictericia por	751.61[774.5]	
� endometrio	621.8	
� enfermedad arteriolar	447.1	
"� ojo, c�mara anterior"	360.34	
"� �rgano o sitio, cong�nita NCOC "		v�ase Atresia 
� rama supraa�rticas	446.7	
� trompa de Falopio	628.2	
� ur�ter	593.89	
� uretra	599.8	
� vaso linf�tico	457.1	
�� posmastectom�a	457.0	
� vasos sangu�neos placentarios 		"v�ase Placenta, anormal "
� vena	459.9
� vest�bulo (oral)	525.8
Observaci�n (por)	V71.9
� accidente NCOC	V71.4
�� en el lugar de trabajo	V71.3
� embarazo 	
�� de alto riesgo	V23.9
��� problema especificado NCOC	V23.8
�� normal (sin complicaci�n)	V22.1
��� con complicaci�n no obst�trica	V22.2
�� primero	V22.0
� enfermedad	V71.9
�� cardiovascular	V71.7
�� coraz�n	V71.7
�� enfermedad especificada NCOC	V71.8
�� mental	V71.09
� ingesti�n de agente delet�reo	V71.8
� ingesti�n de cuerpo extra�o	V71.8
"� intento de suicidio, presunto"	V71.8
� intento de violaci�n	V71.6
"� neoplasia maligna, presunta"	V71.1
� posparto 	
�� inmediatamente despu�s del parto	V24.0
�� seguimiento rutinario	V24.2
� presunci�n de (no comprobado) (no diagnosticado) 	
�� contusi�n (cerebral)	V71.6
�� enfermedad cardiovascular	V71.7
�� enfermedad especificada NCOC	V71.8
�� enfermedad infecciosa que no requiere aislamiento	V71.8
�� enfermedad NCOC	V71.8
�� neoplasia 	
��� benigna	V71.8
��� maligna	V71.1
�� neoplasia maligna	V71.1
�� ni�o o esposa v�ctima de apaleamiento	V71.6
�� trastorno mental	V71.09
�� tuberculosis	V71.2
� sin necesidad de cuidados m�dicos ulteriores	V71.9
� traumatismos (accidentales)	V71.4
�� infligidos NCOC	V71.6
��� durante supuesta violaci�n o seducci�n	V71.5
"� tuberculosis, presunta"	V71.2
� variaciones de crecimiento y desarrollo	V21.8
"� violaci�n o seducci�n, presunta"	V71.5
�� traumatismo durante	V71.5
"Obsesional, obsesivo"	300.3
� estado	300.3
� fobia	300.3
� ideas e im�genes mentales	300.3	
� impulsos	300.3	
� neurosis	300.3	
� psicastenia	300.3	
� rumiaci�n de ideas	300.3	
� s�ndrome	300.3	
Obsesivocompulsivo	300.3	
� neurosis	300.3	
� reacci�n	300.3	
Obstipaci�n	564.0	v�ase adem�s Constipaci�n
� psic�gena	306.4	
"Obstrucci�n, obstruido, obstructivo "		
� acueducto de Silvio	331.4	
�� cong�nita	742.3	
��� con espina b�fida	741.0	v�ase adem�s Espina b�fida
� ampolla de Vater	576.2	
"�� con c�lculo, colelitiasis, o piedras "		v�ase Coledocolitiasis 
� anastomosis interna 		"v�ase Complicaciones, mec�nicas, injerto "
� �ngulo ileocecal	560.9	"v�ase adem�s Obstrucci�n, intestino"
� a�rtica (coraz�n) (v�lvula)	424.1	"v�ase adem�s Estenosis, a�rtica"
�� reum�tica	395.0	"v�ase adem�s Estenosis, a�rtica, reum�tica"
� aortoil�aca	444.0	
� Arnold-Chiari	741.0	v�ase adem�s Espina b�fida  
� arteria	444.9	"v�ase adem�s Embolia, arteria  "
�� basilar (completa) (parcial)	433.0	"v�ase adem�s Oclusi�n, arteria, basilar"
�� car�tida (completa) (parcial)	433.1	"v�ase adem�s Oclusi�n, arteria, car�tida"
�� precerebral 		"v�ase Oclusi�n, arteria, precerebral NCOC "
�� retiniana (central)	362.30	"v�ase adem�s Oclusi�n, retina"
�� vertebral (completa) (parcial)	433.2	"v�ase adem�s Oclusi�n, arteria, vertebral"
� asma (cr�nica) (con enfermedad pulmonar  obstructiva)	493.2	
� banda (intestinal)	560.81	
� biliar (conducto) (tracto)	576.2	
�� con c�lculo	574.51	
��� con colecistitis (cr�nica)	574.41	
���� aguda	574.31	
�� cong�nita	751.61	
��� ictericia por	751.61[774.1]	
�� ves�cula biliar	575.2	
��� con c�lculo	574.21	
���� con colecistitis (cr�nica)	574.11	
����� aguda	574.01	
� bronquio	519.1	
"� canal, o�do"	380.50	"v�ase adem�s Estrechez, canal de o�do, adquirida"
� cardias	537.89	
"� cava, vena (inferior) (superior)"	459.2	
"� cavas, venas (inferiores) (superiores)"	459.2	
� ciego	560.9	"v�ase adem�s Obstrucci�n, intestino"
� circulatoria	459.9	
� colon	560.9	"v�ase adem�s Obstrucci�n, intestino"
�� simpaticot�nica	560.89	
� conducto 		
�� c�stico	575.2	"v�ase adem�s Obstrucci�n, ves�cula, biliar"
��� cong�nita	751.61	
�� com�n	576.2	"v�ase adem�s Obstrucci�n, biliar"
��� cong�nita	751.61	
�� eyaculatorio	608.89	
�� lagrimonasal	375.56	
��� cong�nita	743.65	
��� neonatal	375.55	
�� nasolagrimal	375.56	
��� cong�nita	743.65	
��� neonatal	375.55	
�� pancre�tico	577.8	
�� salival (cualquiera)	527.8	
��� con c�lculo	527.5	
"�� Stensen, de"	527.8	
�� tor�cico	457.1	
� conducto o canal�culo biliar	576.2	"v�ase adem�s Obstrucci�n, biliar"
�� cong�nita	751.61	
��� ictericia por	751.61[774.5]	
� coronaria (arteria) (coraz�n)	414.0	
� cuello de vejiga (adquirida)	596.0	
�� cong�nita	753.6	
� cuerpo extra�o 		"v�ase Cuerpo, extra�o "
� debida a cuerpo extra�o dejado inadvertidamente en herida operatoria	998.4	
� derivaci�n ventricular	996.2	
"� dispositivo, implantaci�n, o injerto"		"v�ase adem�s Complicaciones, debida a (presencia de cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"
� duodeno	537.3	
�� cong�nita	751.1	
�� debida a 		
��� cicatrizaci�n	537.3
��� compresi�n NCOC	537.3
��� enfermedad o lesi�n intr�nseca NCOC	537.3
��� quiste	537.3
��� torsi�n	537.3
��� �lcera	537.3
��� v�lvulo	537.3
� endocardio	414.90
�� arterioscler�tica	424.99
"�� causa especificada, salvo reum�tica"	424.99
� erupci�n de diente	520.6
� es�fago	530.3
� est�mago	537.89
�� aguda	536.1
�� cong�nita	750.7
� faringe	478.29
� fecal	560.39	
�� con hernia 		"v�ase adem�s Hernia, por sitio, con obstrucci�n "
���� gangrenosa 		"v�ase Hernia, por sitio, con gangrena "
� foramen de Monro (cong�nita)	742.3	
�� con espina b�fida	741.0	v�ase adem�s Espina b�fida
� gastrointestinal	560.9	"v�ase adem�s Obstrucci�n, intestino"
� gl�ndula par�tida	527.8	
� gl�ndula submaxilar	527.8	
�� con c�lculo	527.5	
� glotis	478.79	
� hep�tica	573.8	
�� conducto	576.2	"v�ase adem�s Obstrucci�n, biliar"
��� cong�nita	751.61	
� h�gado	573.8	
�� cirr�tica	571.5	v�ase adem�s Cirrosis
� ictericia	576.8	"v�ase adem�s Obstrucci�n, biliar"
�� cong�nita	751.61	
� ictericia	576.8	"v�ase, adem�s, Obstrucci�n, biliar"
�� cong�nita	751.61	
� ileofemoral (arteria)	444.81	
� Ileon	560.9	"v�ase adem�s Obstrucci�n, intestino"
� injerto o derivaci�n vascular	996.1	
� intestino (mec�nica) (neur�gena) (paroxismal) (posinfecciosa) (refleja)	560.9	
�� con 		
��� adherencias (intestinales) (peritoneales)	560.81	
��� hernia 		"v�ase adem�s Hernia, por sitio, con obstrucci�n "
���� gangrenosa 		"v�ase Hernia, por sitio, con gangrena "
�� adin�mica	560.1	v�ase adem�s Ileo
�� causa especificada NCOC	560.89	
�� cong�nita o infantil (delgado)	751.1	
��� grueso	751.2	
�� debida a 		
��� Ascaris lumbricoides	127.0	
��� espesamiento mural	560.89	
��� procedimiento	997.4	
���� con implicaci�n del tracto urinario      	997.5	
�� impactaci�n	560.39	
�� infantil 		"v�ase Obstrucci�n, intestino, cong�nita "
�� por c�lculo biliar	560.31	
�� reci�n nacido 		
��� debida a 		
���� fecalitos	777.1	
���� leche espesa	777.2	
���� meconio (tap�n)	777.1	
����� en mucoviscidosis	277.01	
��� transitoria	777.4	
"�� transitoria, reci�n nacido"	777.4	
�� v�lvulo	560.2	
� intestino	560.9	"v�ase adem�s Obstrucci�n, intestino"
"� I�ctea, con esteatorrea"	579.2	
� lagrimal(es) 		
�� canal�culos	375.53	
�� cong�nita	743.65	
�� punto	375.52	
�� saco	375.54	
� laringe	478.79	
�� cong�nita	748.3	
� laringitis	464.0	v�ase adem�s Laringitis
� linf�tica	457.1	
� meconio 		
�� feto o reci�n nacido	777.1	
��� en mucoviscidosis	277.01	
"�� reci�n nacido, debida a fecalitos"	777.1	
� mediastino	519.3	
� mitral (reum�tica) 		"v�ase Estenosis, mitral "
� nariz	478.1	
� nasal	478.1	
�� conducto	375.56	
��� neonatal	375.55	
�� seno 		v�ase Sinusitis 
� nasofaringe	478.29	
"� �rgano o sitio, cong�nita NCOC "		v�ase Atresia 
� orificio vesicouretral	596.0	
� pi�mica 		v�ase Septicemia 
� p�loro (adquirida)	537.0	
�� cong�nita	750.5	
�� infantil	750.5	
� porta (circulaci�n) (vena)	452	
� pr�stata	600	
�� v�lvula (coraz�n)	424.3	"v�ase adem�s Endocarditis, pulmonar"
"�� vena, aislada"	747.49	
� pr�tesis de v�lvula en bal�n intracardiaca	996.02	
� pulm�n	518.89	
�� con 		
��� asma 		v�ase Asma 
��� bronquitis (cr�nica)	491.2	
��� enfisema NCOC	492.8	
�� cr�nica NCOC	496	
��� con asma (cr�nica) (obstructiva)	493.2	
"�� enfermedad, cr�nica NCOC"	496	
��� con asma (cr�nica) (obstructiva)	493.2	
�� enfisematosa	492.8	
"�� v�as a�reas respiratorias, cr�nica"	496	
� recto	569.49	
� rectosigmoidea	560.9	"v�ase adem�s Obstrucci�n, intestino"
� renal	593.89	
� respiratoria	519.8	
�� cr�nica	496	
"� respiratoria superior, cong�nita"	748.8	
� retiniana (arteria) (vena) (central)	362.30	"v�ase adem�s Oclusi�n, retina"
� ri��n	593.89	
� salida g�strica	537.0	
� seno (accesorio) (nasal)	473.9	v�ase adem�s Sinusitis
� sigmoidea	560.9	"v�ase adem�s Obstrucci�n, intestino"
� trabajo del parto	660.9	
�� por 		
��� detenci�n profunda en transversa	660.3	
��� gemelos entrelazados	660.5	
��� hombro impactado	660.4	
��� mala posici�n (feto) (enfermedades clasificables bajo 652.0-652.9)	660.0	
���� cabeza durante el trabajo	660.3	
��� peIvis �sea (enfermedades clasificables bajo 653.0-653.9)	660.1	
��� posici�n occipitoposterior persistente	660.3	
��� tejido blando pelviano (enfermedades clasificables bajo 654.0-654.9)	660.2	
�� que afecta al feto o al reci�n nacido	763.1	
� tr�quea	519.1	
"� traqueostom�a, v�a a�rea respiratoria de"	519.0	
� tric�spide 		"v�ase Endocarditis, tric�spide "
� tromb�tica 		v�ase Trombosis 
� trompa de 		
�� Eustaquio (completa) (parcial)	381.60	
��� cartilaginosa 		
���� extr�nseca	381.63	
���� intr�nseca	381.62	
��� debida a 		
���� colesteatoma	381.61	
���� lesi�n �sea NCOC	381.61	
���� p�lipo	381.61	
��� �sea	381.61	
�� trompa de Falopio (bilateral)	628.2	
� tubo digestivo	560.9	"v�ase adem�s Obstrucci�n, intestino"
� uni�n peIviureteral	593.4	"v�ase adem�s Obstrucci�n, ur�ter"
� ur�ter (uni�n peIviana) (funcional)	593.4	
�� cong�nita	753.2	
�� debida a c�lculo	592.1	
� uretra	599.6	
�� cong�nita	753.6	
� urinaria (moderada)	599.6	
�� �rgano o tracto (inferior)	599.6	
�� v�lvula prost�tica	596.0	
� uropat�a	599.6	
� �tero	621.8	
� vagina	623.2	
� valvular 		v�ase Endocarditis 
� vaso NCOC	459.9	
"� vena, venoso"	459.2	
�� cava (inferior) (superior)	459.2	
�� tromb�tica 		v�ase Trombosis 
� vesical	596.0	
� ves�cula biliar	575.2	
"�� con c�lculo, colelitiasis, o piedras"	574.21	
��� con colecistitis (cr�nica)	574.11	
���� aguda	574.01	
�� cong�nita	751.69	
��� ictericia por	751.69[774.5]	
� v�as a�reas respiratorias NCOC	519.8	
�� con 		
��� alveolitis al�rgica NCOC	495.9	
��� asma NCOC	493.9	v�ase adem�s Asma     
��� bronquiectasia	494	
��� bronquitis (cr�nica)	491.2	
��� enfisema NCOC	492.8	
�� cr�nica	496	
��� con 		
���� alveolitis al�rgica NCOC	495.5	
���� asma NCOC	493.9	v�ase adem�s Asma
���� bronquiectasia	494	
���� bronquitis (cr�nica)	491.2	
���� enfisema NCOC	492.8	
�� debida a 		
��� broncospasmo	519.1	
��� cuerpo extra�o	934.9	
��� inhalaci�n de humos o vapores	506.9	
��� laringospasmo	478.75	
� yeyuno	560.9	"v�ase adem�s Obstrucci�n, intestino"
Obturador 		v�ase enfermedad espec�fica
Oclofobia	300.29	
Oclusi�n 		
� acueducto de Silvio	331.4	
�� cong�nita	742.3	
��� con espina b�fida		v�ase adem�s Espina
� ano	569.49	
�� cong�nita	751.2	
�� infantil	751.2	
� aortoil�aca (cr�nica)	444.0	
� aparato lagrimal 		"v�ase Estenosis, lagrimal "
� arteria comunicante posterior	433.8	
� arteria(s) NCOC	444.9	"v�ase adem�s Embolia, arteria"
"�� auditiva, interna"	433.8	
�� basilar	433.0	
��� con otra arteria precerebral	433.3	
��� bilateral	433.3	
�� car�tida	433.1	
��� con otra arteria precerebral	433.3	
��� bilateral	433.3	
�� cerebelar (anterior inferior) (inferior posterior)	433.8	
�� cerebral	434.9	"v�ase adem�s Infarto, cerebro     "
�� cerebro o cerebral	434.9	"v�ase adem�s Infarto, cerebro"
�� comunicante posterior	433.8	
�� coroidea (anterior)	433.8	
�� coronaria (aguda) (tromb�tica)	410.9	"v�ase adem�s Infarto, miocardio"
��� curada o antigua	412	
��� sin infarto de miocardio	411.81	
�� espinal	433.8	
�� hipofisaria	433.8	
�� mesent�rica (emb�lica) (tromb�tica) (con gangrena)	557.0	
�� perif�ricas	444.22	
��� extremidad inferior	444.22	
��� extremidad superior	444.21	
�� pontina	433.8	
�� precerebral NCOC	433.9	
��� efecto tard�o 	438	v�ase categor�a
��� especificada NCOC	433.8	
��� m�ltiple o bilateral	433.3	
"��� puerperal, posparto, parto"	674.0	
�� renal	593.81	
�� retiniana 		"v�ase Oclusi�n, retina, arteria "
�� vertebral	433.2	
��� con otra arteria precerebral	433.3	
��� bilateral	433.3	
� arterias de extremidades 		
�� inferiores	444.22	
�� superiores	444.21	
� basilar (arteria) 		"v�ase Oclusi�n, arteria, basilar "
� canal cervical	622.4	"v�ase adem�s Estrechez, cuello uterino"
�� por paludismo debido a P. falciparum     	084.0	
� car�tida (arteria) (com�n) (interna) 		"v�ase Oclusi�n, arteria, car�tida "
"� cava, vena (inferior) (superior)"	453.2	
� cerebelar (inferior anterior) (inferior posterior) (superior)	433.8	
� cerebral (arteria)	434.9	"v�ase adem�s Infarto, cerebro"
� cerebro (arteria) (vascular)	434.9	"v�ase adem�s Infarto, cerebro"
� cerebrovascular	434.9	"v�ase adem�s Infarto, cerebro"
�� difusa	437.0	
� coanal	748.0	
� colon	560.9	"v�ase adem�s Obstrucci�n, intestino"
� conducto 		
�� biliar (cualquiera)	576.2	"v�ase adem�s Obstrucci�n, biliar"
�� c�stico	575.2	"v�ase adem�s Obstrucci�n, ves�cula biliar"
��� cong�nita	751.69	
�� linf�tico	457.1	
�� mamario	611.8	
�� tor�cico	457.1	
� conducto c�stico	575.2	"v�ase adem�s Obstrucci�n, ves�cula biliar"
� conducto mamario	611.8	
� coroidea (arteria)	433.8	
� coronaria (aguda) (arteria) (tromb�tica)	410.9	"v�ase adem�s Infarto, miocardio"
�� curada o antigua	412	
�� sin infarto de miocardio	411.81	
� cuello uterino	622.4	"v�ase adem�s Estrechez, cuello uterino"
� dientes (mandibular) (lingual posterior)	524.2	
� emb�lica 		v�ase Embolia 
"� espinal, arteria"	433.8	
"� gingiva, traum�tica"	523.8	
� himen	623.3	
�� cong�nita	752.41	
� hipofisaria (arteria)	433.8	
� intestino	560.9	"v�ase adem�s Obstrucci�n, intestino"
"� lingual posterior, de dientes mandibulares"	524.2	
� mama (conducto)	611.8	
"� mesent�rica, arteria (emb�lica) (tromb�tica) (con gangrena)"	557.0	
� nariz	478.1	
�� cong�nita	748.0	
"� �rgano o sitio, cong�nita NCOC "		v�ase Atresia 
� oviducto	628.2	
�� cong�nita	752.19	
"� period�ntica, traum�tica"	523.8	
� p�loro	537.0	"v�ase adem�s Estrechez, p�loro"
� pontina (arteria)	433.8	
"� precerebral, arteria "		"v�ase Oclusi�n, arteria, precerebral NCOC "
� pulm�n	518.89	
� punto lagrimal	375.52	
� pupila	364.74	
"� renal, arteria"	593.8
"� retina, retiniana (vascular)"	362.30
"�� arteria, arterial"	362.30
��� central (total)	362.31
��� parcial	362.33
��� rama	362.32
��� transitoria	362.34
��� tributaria	362.32
�� vena	362.30
��� central (total)	362.35
��� incipiente	362.37
��� parcial	362.37
��� rama	362.36
��� tributaria	362.36
� ri��n	593.89
� trompa de Falopio	628.2













g
