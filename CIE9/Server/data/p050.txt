� cr�neo		"v�ase Anomal�a, cr�neo"
� cresta alveolar (proceso)	525.8	
� cristalino	743.9	
�� forma	743.36	
�� tipo especificado NCOC	743.39	
� cromosomas sexuales NCOC	758.8	"v�ase Anomal�a, cromosomas"
"� cromosomas, cromos�mica"	758.9	
�� 13 (13-15)	758.1	
�� 18 (16-18)	758.2	
�� 21 � 22	758.0	
�� autosomas NCOC	758.5	"v�ase dem�s Anormalidad, autosomas"
��� supresi�n	758.3	
"�� Christchurch, de"	758.3	
�� D1	758.1	
�� E3	758.2	
�� G	758.0	
�� mosaicos	758.9
�� sexuales	758.8
��� complemento XO	758.6
��� complemento XXX	758.8
��� complemento XXY	758.7
��� complemento XYY	758.8
��� disgenesia gonadal	758.6
"��� Klinefelter, de"	758.7
"��� Turner, de"	758.6
�� trisomia 21	758.0
� c�bito	755.50
� cuello (cualquier parte)	744.9
�� tipo especificado NCOC	744.89
� cuello uterino	752.40
�� con duplicaci�n de vagina y �tero	752.2
�� en embarazo o alumbramiento	654.6
��� que afecta al feto o al reci�n nacido	763.8	
��� que obstruye el parto	660.2	
���� que afecta al feto o al reci�n nacido	763.1	
� cuerdas tendinosas	746.9	
� dedo de mano	755.50	
�� palmeado	755.11	"v�ase adem�s Sindactilia, dedos de mano"
�� supernumerario	755.01	
� dedo de pie	755.66	
�� palmeado	755.13	"v�ase adem�s Sindactilia, dedos de pie"
�� supernumerario	755.02	
� dentici�n	520.6	
� dentofacial NCOC	524.9	
�� funcional	524.5	
� tipo especificado NCOC	524.8	
� dermatogl�fica	757.2	
� desarrollo		
�� cuello uterino	752.40
�� vagina	752.40
�� vulva	752.40
"� diafragma, diafragm�tica (aperturas) NCOC"	756.6
"� diente, dientes NCOC"	520.9
�� espacimiento	524.3
�� posici�n	524.3
"� distribuci�n, arteria coronaria"	746.85
� duodeno	751.5
� dura	742.9
�� cerebro	742.4
�� m�dula espinal	742.59
"� Ebstein, de (coraz�n)"	746.2
�� v�lvula tric�spide	746.2
� ectod�rmica	757.9
"� Eisenmenger, de (defecto de tabique ventricular)"	745.4
� enc�a	750.9	
� Enfermedad como el SIDA (enfermedad) (s�ndrome)		v�ase Virus de Inmunodeficiencia humana (enfermedad) (infecci�n)
� epid�dimo	752.9	
� epiglotis	748.3	
� esc�pula	755.50	
� escler�tica	743.9	
�� tipo especificado NCOC	743.47	
� escroto	752.9	
� es�fago	750.9	
�� tipo especificado NCOC	750.4	
� espalda	759.9	
"� espina dorsal, espinal"	756.10	
�� columna	756.10	
�� m�dula	742.9	
��� espina b�fida	741.9	v�ase adem�s Espina b�fida
��� meningocele	741.9	v�ase adem�s Espina b�fida
��� tipo especificado	742.59
��� vaso	747.6
�� meninges	742.59
�� ra�z de nervio	742.9
"� esqueleto, generalizadas NCOC"	756.50
� estern�n	756.3
� est�mago	750.9
�� tipo especificado NCOC	750.7
"� estrechez, p�rpado"	743.62
� estr�as	
�� auriculares	746.9
�� cardiacas	746.9
�� ventriculares	746.9
� extremidad inferior	755.60
� extremidad superior	755.50
� faringe	750.9
�� hendidura braquial	744.41	
�� tipo especificado NCOC	750.29	
� fascia	756.9	
�� tipo especificado NCOC	756.89	
� f�mur	755.60	
"� fijaci�n, intestino"	751.4	
� flexi�n (articulaci�n)	755.9	
�� cadera o muslo	754.30	"v�ase adem�s Dislocaci�n, cadera, cong�nita"
� foramen		
�� de Botalli	745.5	
�� oval	745.5	
"� forma, dientes"	520.2	
� formaci�n de tejido duro en pulpa	522.3	
� fovea central	743.9	
� frente 	756.0	"v�ase Anomal�a, cr�neo"
� garganta	750.9	
"� genitales, �rgano(s) o aparato"	
�� femeninos	752.9
��� externos	752.40
���� tipo especificado NCOC	752.49
��� internos	752.9
�� masculinos (externos o internos)	752.9
��� epispadias	752.6
"��� hidrocele, cong�nito"	778.6
��� hipospadias	752.6
��� test�culo no descendido	752.5
�� tipo especificado NCOC	752.8
� genitourinario NCOC	752.9
"� Gerbode, de"	745.4
� gl�ndula endocrina NCOC	745.69
� gl�ndula o conducto salival	750.9
�� tipo especificado NCOC	750.26
� gl�ndula paratiroides	759.2
� globo (del ojo)	743.9
� glotis	748.3
"� granulaci�n o granulocito, gen�tica"	288.2
�� constitucional	288.2
�� leucocito	288.2
"� Hegglin, de"	288.2
� hemianencefalia	740.0
� hemicefalia	740.0
� hemicr�nea	740.0
� hendidura braquial NCOC	744.49
�� f�stula	744.41
�� persistente	744.41
�� quiste	744.42
�� seno (externo) (interno)	744.41
� h�gado (conducto)	751.60
�� atresia	751.69	
� himen	752.40	
"� hipersegmentaci�n de neutr�filos, hereditaria"	288.2	
� hipofisaria	759.2	
� hombro (cintura escapular) (articulaci�n)	755.50	
�� tipo especificado NCOC	755.59	
� hueso frontal	756.0	"v�ase Anomal�a, cr�neo"
� hueso NCOC	756.9	
�� antebrazo	755.50	
�� brazo	755.50	
�� cabeza	756.0	
�� cadera	755.63	
�� cara	756.0	
�� cintura escapular	755.50	
�� cintura pelviana	755.60	
�� costilla	756.3	
�� cr�neo	756.0	
��� con		
���� anencefalia	740.0	
���� encefalocele	742.0	
���� hidrocefalia	742.3	
����� con espina b�fida	741.0	v�ase adem�s Espina b�fida
����� microcefalia	742.1	
�� dedo de mano	755.50	
�� dedo de pie	755.66	
�� frontal	756.0	
�� lumbosacral	756.10+B193	
�� nariz	748.1	
�� pecho	756.3	
�� pie	755.67	
�� pierna	755.60	
�� raqu�tico	756.4	
�� tobillo	755.69
� h�mero	755.50
� humor v�treo	743.9
�� tipo especificado NCOC	743.51
� ileocecal (pliegue) (v�lvula)	751.5
� �leon (intestino)	751.5
� ilion	755.60
� integumento	757.9
�� tipo especificado NCOC	757.8
"� intervertebral, cart�lago o disco"	756.10
� intestino (grueso) (delgado)	751.5
�� tipo fijaci�n	751.4
� iris	743.9
�� tipo especificado NCOC	743.46
� isquion	755.60
"� Jordan, de"	288.2
� Klippel-Feil (brevicollis)	756.16	
"� laberinto, membr�neo (que causa deterioro de la capacidad auditiva)"	744.05	
� labio	750.9	
�� hendido	749.10	"v�se adem�s Hendidura, labio"
�� tipo especificado NCOC	750.26	
� labios vulvares (mayores) (menores)	752.40	
� lagrimal		
"�� aparato, conducto o tubo"	743.9	
��� tipo especificado NCOC	743.65	
�� gl�ndula	743.9	
��� tipo especificado NCOC	743.64	
"� Langdon Down, de (mongolismo)"	758.0	
"� laringe, laringeal (m�sculo)"	748.3	
"�� membrana, pterigi�n"	748.2	
� lengua	750.10	
� tipo especificado NCOC	750.19	
"� leucocitos, gen�tica"	288.2
�� granulaci�n (constitucional)	288.2
� ligamento	756.9
�� ancho	752.10
�� redondo	752.9
� ligamento ancho	752.10
�� tipo especificado NCOC	752.19
� ligamento redondo	752.9
� lumbosacra (articulaci�n) (regi�n)	756.10
"� Madelung, de (radio)"	755.54
� mama	757.9
� mand�bula	524.9
�� tama�o	524.0
� mano	755.50
� maxila	524.9
�� tama�o	524.0
� maxilar NCOC	524.9	
�� cierre	524.5	
�� tama�o (grande)	524.0	
�� tipo especificado NCOC	524.8	
"� May (-Hegglin), de"	288.2	
� meato �seo (o�do)	744.03	
� meato urinario	753.9	
�� tipo especificado NCOC	753.8	
� mejilla	744.9	
� membrana de Descemet	743.9	
�� tipo especificado NCOC	743.49	
� meninges 	742.9	
�� cerebro	742.2	
�� espinal	742.59	
� meningocele	741.9	v�ase adem�s Espina b�fida
� ment�n	744.9	
�� tipo especificado NCOC 	744.89	
� mesenterio	751.9	
� metacarpo	755.50	
� metatarso	755.67	
"� miembro, salvo deformidad por reducci�n"	755.9	
�� inferior	755.60	
��� deformidad por reducci�n 	755.30	"v�ase adem�s Deformidad, reducci�n, miembro inferior"
��� tipo especificado NCOC	755.69	
�� superior	755.50	
��� deformidad por reducci�n	755.20	"v�ase adem�s Deformidad, reducci�n, miembro superior"
��� tipo especificado NCOC	755.59	
� mitral(es) (c�spides) (v�lvula)	746.9	
�� atresia	746.89	
�� estenosis	746.5	
�� insuficiencia	746.6	
�� tipo especificado NCOC	746.89	
� m�ltiple NCOC	759.7	
�� tipo especificado NCOC	759.89	
� m�sculo	756.9	
�� ojo	743.9	
��� tipo especificado NCOC	743.69	
�� tipo especificado NCOC	756.89	
� m�sculos papilares	746.9	
� muslo	755.60	
�� flexi�n	754.32	"v�ase adem�s Subluxaci�n, cong�nita, cadera"
� mu�eca (articulaci�n)	755.50	
"� nariz, nasal (hueso) (cart�lago) (tabique) (seno)"	748.1	
"� nasal, seno o tabique"	748.1	
� nervio	742.9	
�� ac�stico	742.9	
��� tipo especificado NCOC	742.8	
�� �ptico	742.9	
��� tipo especificado NCOC	742.8
�� tipo especificado NCOC	742.8
� nervio ac�stico	742.9
� neurol�gica	742.9
� no terat�gena NCOC	754.89
"� ocular, m�sculo"	743.9
� o�do	744.3
�� externa	744.3
��� que causa deterioro de la capacidad auditiva	744.02
��� tipo especificado NCOC	744.29
�� huesecillos	744.04
�� interna (que causa deterioro de la capacidad auditiva)	744.05
"�� medio, salvo huesecillos (que causa deterioro de la capacidad auditiva)"	744.03
��� huesecillos	744.04
�� pabell�n prominente	744.29
�� que causa deterioro de la capacidad auditiva	744.00
��� tipo especificado NCOC	744.09
�� tipo especificado NCOC	744.29
��� con deterioro de la capacidad auditiva	744.09
"� o�do  medio, salvo huesecillos (que causa deterioro de la capacidad auditiva)"	744.03
�� huesecillos	744.04
� ojo (cualquier parte)	743.9
�� anexos	743.9
��� tipo especificado NCOC	743.69
�� anoftalmos	743.00
�� anterior	
��� c�mara y estructuras relacionadas	743.9
���� �ngulo	743.9
����� tipo especificado NCOC	743.44
���� tipo especificado NCOC	743.44
��� segmento	743.9
���� combinada	743.48
���� m�ltiple	743.48	
���� tipo especificado NCOC	743.49	
�� catarata	743.30	v�ase adem�s Catarara
�� escler�tica	743.9	
��� tipo espec�ficado NCOC	743.8	
�� glaucoma	743.20	v�ase adem�s Buftalmia
�� microftalmos	743.10	v�ase adem�s Microftalmos
�� p�rpado	743.9	
��� tipo especificado NCOC	743.63	
�� ptosis (p�rpado)	743.61	
�� retina	743.9	
��� tipo especificado NCOC	743.59	
�� segmento posterior	743.9	
��� tipo especificado NCOC	743.59	
��� vascular	743.58	
��� v�treo	743.9	
���� tipo especificado NCOC	743.51
� ombligo	759.9
�� arteria umbilical	747.5
� �ptico	
�� disco	743.9
��� tipo especificado NCOC	743.57
�� nervio	742.9
� �rbita (ojo)	743.9
�� tipo especificado NCOC	743.66
� �rgano	
�� de Corti (que causa deterioro de la capacidad auditiva)	744.05
�� o sitio	759.9
��� tipo especificado NCOC	759.89
� �rgano(s) o sistema digestivos	751.9
�� inferior	751.5
�� superior	750.9
�� tipo especificado NCOC	751.8
� �rganos de sentido	742.9
�� tipo especificado NCOC	742.8
� orificio vesicouretral	753.9
� origen	
�� ambas grandes arterias del mismo ventr�culo	745.11
�� arteria coronaria	746.85
�� arteria coronaria izquierda desde arteria pulmonar	746.85
�� arteria innominada	747.6
��� arteria pulmonar	747.3
�� arteria subclavia (izquierda) (derecha)	744.21
�� vasos renales	747.6
� ovario	752.0
� oviducto	752.10
� pabell�n	
�� o�do	744.3
��� que causa deterioro de capacidad auditiva	744.02	
� paladar (duro) (blando) 	750.9	
�� hendidura	749.00	"v�ase adem�s Hendidura, paladar"
� p�ncreas (conducto)	751.7	
� pared abdominal	756.7	
� parot�dea (gl�ndula)	750.9	
� p�rpado (pliegue)	743.9	
� p�rpado	743.9	
�� tipo especificado NCOC	743.63	
� pecho (pared)	756.3	
� Pelger-Hutt (hiposegmentaci�n hereditaria)	288.2	
� pelo	757.9	
�� tipo especificado NCOC	757.4	
� pelvis (�sea)	755.60	
�� que complica el parto	653.0	
�� raqu�tica	268.1	
��� fetal	756.4	
� pene (glande)	752.9	
� pericardio	746.89	
� peron�	755.60	
"� Peter, de"	743.44	
� pez�n	757.9	
� pie	755.67	
� piel (anexos)	757.9	
�� tipo especificado NCOC	757.39	
� pierna (inferior) (superior)	755.60	
�� reducci�n NCOC 	755.30	"v�ase adem�s Deformidad, reducci�n, miembro inferior"
"� Pierre Robin, de"	756.0	
� pigmentaci�n NCOC	709.0	
�� cong�nita	757.33	
� piloro	750.9	
�� estenosis	750.5	
�� hipertrofia	750.5
� pituitaria (gl�ndula)	759.2
� pliegues aritenoepigl�ticos	748.3
"� pliegues, coraz�n"	746.9
� posici�n de los dientes	524.3
� prepucio	752.9
� pr�stata	752.9
� pulgar	755.5
�� supernumerario	755.01
� pulm�n (fisura) (l�bulo) NCOC	748.6
�� agenesia	748.5
�� tipo especificado NCOC	748.69
� pulmonar	748.6
�� arteria	747.3
�� circulaci�n	747.3
�� tipo especificado NCOC	748.69
�� v�lvula	746
��� atresia	746
��� estenosis	746.02
���� infundibular	746.83
���� subvalvular	746.83
��� insuficiencia	746.09
��� tipo especificado NCOC	746.09
�� vena	747.40
�� venoso	
��� conexi�n	747.49
���� parcial	747.42
���� total	747.41
��� retorno	747.49
���� parcial	747.42
���� total (TAPVR) (completa) (subdiafragm�tica) (supradiafragm�tica)	747.41
� pupila	743.9
� radio	755.50
"� raqu�tica, fetal"	756.4
� recto	751.5
� rectovaginal (tabique)	752.40
� refracci�n	367.9
� relaci�n entre los arcos dentales	524.2
� reloj de arena	
�� est�mago	750.7
�� vejiga	753.8
�� ves�cula biliar	751.69
� renal	753.9
�� vaso	747.6
� repliegues cerebrales	742.9
� repliegues pleurales	748.8
� retorno venoso (pulmonar)	747.49
�� parcial	747.42
�� total	747.41	
"� Rieger, de"	743.44	
� ri��n(es) (c�lices) (pelvis)	753.9	
�� vaso	747.6	
� rodilla (articulaci�n)	755.64	
� rotaci�n	754.32	"v�ase adem�s Mala rotaci�n, cadera o muslo (v�ase adem�s Subluxaci�n, cong�nita, cadera"
� r�tula	755.64	
� sacro	756.10	
� sacroilicaca (articulaci�n)	755.69	
"� seb�cea, gl�ndula"	757.9	
"� seminal, conducto o tracto"	752.9	
� seno de Valsalva	747.29	
� seno preauricular	744.46	
� SIDA - complejo relacionado		v�ase Virus de inmunodeficiencia humana (enfermedad) (infecci�n)
� SIDA - condiciones relacionadas		v�ase Virus de inmunodeficiencia humana (enfermedad) (infecci�n)
� SIDA - retrovirus asociados (enfermedad) (infecci�n)		v�ase Virus de inmunodeficiencia humana (enfermedad) (infecci�n)
� SIDA - 		v�ase Virus de inmunodeficiencia humana (enfermedad) (infecci�n)
� SIDA - virus asociados (enfermedad) (infecci�n)		v�ase Virus de inmunodeficiencia humana (enfermedad) (infecci�n)
� SIDA - virus relacionados (enfermedad) (infecci�n)		v�ase Virus de inmunodeficiencia humana (enfermedad) (infecci�n)
� sigmoide (flexura)	751.5	
� silla de montar		
�� espalda	754.2	
�� nariz	754.0	
��� sifil�tica	090.5	
� sistema cardiovascular	746.9	
"�� que complica el embarazo, alumbramiento o puerperio"	648.5	
� sistema cerebrovascular	747.81	
� sistema circulatorio	747.9	
�� tipo especificado NCOC	747.89	
� sistema linf�tico	759.9	
"� sistema musculoesquel�tico, salvo miembros"	756.9	
�� tipo especificado NCOC	756.9	
� sistema nervioso NCOC	742.9	
�� cerebro	742.9	
��� tipo especificado NCOC	742.4	
�� tipo especificado NCOC	742.8	
� sistema respiratorio	748.9	
�� tipo especificado NCOC	748.8	
� sistema vascular perif�rico NCOC	747.6	
� sitio NCOC	759.9	
� sitio no especificado	759.9	
"� Sprengel, de"	755.52	
"� submaxilar, gl�ndula"	750.9	
� suprarrenal (gl�ndula)	759.1	
� tabique		
�� coraz�n		"v�ase Anomal�a, coraz�n, tabique"
�� nasal 	748.1	
� tabique a�rtico pulmonar	745.0	
� tabique bulbar	745.0	
� talipes		v�ase Talipes
� tal�n	755.67	
� tarso	755.67	
�� con ausencia total de elementos distales	755.31	
� tejidos conectivos	756.9	
�� tipo especificado NCOC	756.89	
� tend�n	756.9	
�� tipo especificado NCOC	756.89	
� terminaci�n		
�� arteria coronaria	746.85	
� test�culo	752.9	
� tibia	755.60	
�� sable	090.5	
"� timo, gl�ndula"	759.2	
� tipo especificado NCOC		
"�� ano, anal (canal)"	751.5
"�� aorta, a�rtica"	747.29
��� cayado	747.21
�� ap�ndice	751.5
�� arteria (perif�rica)	747.6
��� cerebro	747.81
��� coronaria	746.85
��� ojo	743.58
��� pulmonar	747.3
��� retinal	743.58
��� umbilical	747.5
�� articulaci�n	755.8
�� bazo	759.0
�� boca	750.26
�� brazo	755.59
�� bronquio	748.3
�� canal auditivo	744.29
��� que cause deterioro de la capacidad auditiva	744.02
�� canal de Nuck	752.8
�� cara 	744.89
��� hueso(s)	756.0
�� carpo 	755.59
�� cartilaginosa	756.9
�� cart�lago cricoides 	748.3
�� cerebro	742.4
�� ciego	751.5
�� cierre de tabique cardiaco	745.8
�� cintura escapular	755.59
�� cintura pelviana	755.69
�� clav�cula	755.51
�� cl�toris	752.49
�� c�ccix	756.19
�� colon	751.5
�� conducto c�stico	751.69
�� conducto com�n	751.69
�� conducto deferente	752.8
�� conducto eyaculatorio	752.8
�� conducto o tubo biliar	751.69
�� conducto �seo (o�do)	744.03
�� coraz�n	746.89
��� v�lvula NCOC	746.89
���� pulmonar	746.09
�� cord�n esperm�tico	752.8
�� costilla	756.3
�� cr�neo (hueso(s))	756.0
��� con	
���� anencefalia	740.0
���� encefalocele	742.0
���� hidrocefalia	742.3	
����� con espina b�fida	741.0	v�ase adem�s Espina b�fida
���� microcefalia	742.1	
�� cristalino	743.39	
�� c�bito	755.59	
�� cuello	744.89	
�� cuello uterino	752.49	
�� cuerpo ciliar	743.46	
�� dedo de mano	755.59	
�� dedo de pie	755.66	
�� diafragma	756.6	
"�� digestivo, (�rgano(s) o tracto)"	751.8	
��� inferior	751.5	
��� superior	750.8	
�� duodeno	751.5	
�� endocrina	759.2	
�� epiglotis	748.3
�� esc�pula	755.59
�� escler�tica	743.47
�� escroto	752.8
�� es�fago	750.4
�� espina dorsal	756.19
�� estern�n	756.3
�� est�mago	750.7
�� faringe	750.29
�� fascia	756.89
� f�mur	755.69
�� fovea central	743.55
"�� Gartner, conducto de"	752.8
"�� genitales, �rganos"	
��� femeninos	752.8
���� externos	752.49
���� internos NCOC	752.8
��� masculinos	752.8
�� glotis	748.3
"�� hep�tico, conducto"	751.69
�� hid�tide de Morgagni	752.8
�� h�gado	751.69
�� himen	752.49
�� hueso(s)	756.9
��� brazo	755.59
��� cara	756.0
��� cintura escapular	755.59
��� cintura pelviana	755.69
��� cr�neo	756.0
���� con	
����� anencefalia	740.0
����� encefalocele	742.0
����� hidrocefalia	742.3	
������ con espina b�fida	741.0	v�ase adem�s Espina b�fida
����� microcefalia	742.1	
��� pierna	755.69	
�� intestino (grueso) (delgado)	751.5	
��� tipo fijaci�n	751.4	
�� iris	743.46	
"�� laberinto, membranoso"	744.05	
�� labio	750.26	
�� labios vulvares (mayores) (menores)	752.49	
�� laringe	748.3	
�� lengua	750.19	
�� ligamento ancho	752.19	
�� ligamento redondo	752.8	
�� mama	757.6	
�� mano	755.59	
�� meato urinario	753.8	
�� m�dula espinal	742.59	
�� ment�n	744.89	
�� metacarpo	755.59	
"�� miembro, salvo deformidad por reducci�n"	755.8	
��� inferior	755.69	
���� deformidad por reducci�n	755.3.0	"v�ase adem�s Deformidad, reducci�n, miembro inferior"
��� superior	755.59	
���� deformidad por reducci�n	755.2.0	"v�ase adem�s Deformidad, reducci�n, miembro superior"
�� m�sculo	756.89	
��� ocular	743.69	
�� nariz	748.1	
�� nervio	742.8	
��� ac�stico	742.8	
��� �ptico	742.8	
�� o�do	744.29	
��� interno (que causa deterioro de la capacidad auditiva)	744.05
"��� medio, salvo huesecillos"	744.03
���� huesecillos	744.04
��� pabell�n	744.29
���� que causa deterioro de la capacidad auditiva	744.02
��� que causa deterioro de la capacidad auditiva	744.09
�� ojo	743.8
��� m�sculo	743.69
��� p�rpado	743.63
�� �rgano NCOC	759.89
��� de Corti	744.05
�� �rgano o sitio especificado NCOC	759.89
�� ovario	752.0
�� oviducto	752.19
�� p�ncreas	751.7
�� paratiroides	759.2
�� pecho (pared)	756.3
�� pelo	757.4
�� pene	752.8
�� pericardio	746.89
�� peron�	755.69
�� pez�n	757.6
�� pie	755.67
�� piel	757.39
�� pierna	755.69
�� pituitaria	759.2
�� pr�stata	752.8
�� pulm�n (fisura) (l�bulo)	748.69
�� radio	755.59
�� recto	751.5
�� ri��n	753.3
�� rodilla	755.64
�� r�tula	755.64
�� sacro	756.19
"�� salival, conducto o gl�ndula"	750.26
"�� seminal, conducto o tracto"	752.8
�� sistema circulatorio	747.89
"�� sistema musculoesquel�tico, salvo miembros"	756.9
�� sistema nervioso	742.8
�� sistema respiratorio	748.8
�� sistema vascular perif�rico	747.6
�� sitio NCOC	759.89
�� suprarrenal (gl�ndula)	759.1
�� tarso	755.67
�� tegumento	757.8
�� tejidos conectivos	756.89
�� tend�n	756.89
�� test�culo	752.8
�� tibia	755.69
�� timo	759.2
�� tiriodes (gl�ndula)	759.2
��� cart�lago	748.3
�� tobillo	755.69
�� t�rax (pared)	756.3
�� tracto digestivo (completo) (parcial)	751.8
��� inferior	751.5
��� superior	750.8
�� tracto gastrointestinal	751.8
�� tracto genitourinario NCOC	752.8
�� tr�quea (cart�lago)	748.3
�� trompa de Eustaquio	744.24
�� trompa de Falopio	752.19
�� uraco	753.7
�� ur�ter	753.4
��� obstructiva	753.2
�� uretra	753.8
��� obstructiva	753.6
"�� urinario, tracto"	753.8
�� �tero	752.3
�� �vula	750.26
�� u�a	757.5
�� vagina	752.49
�� vascular	747.6
��� cerebro	747.81
�� vejiga	753.8
��� cuello	753.8
�� vena cava (inferior) (superior)	747.49
�� vena(s) perif�rica(s)	747.6
��� cerebro	747.81
��� grandes	747.49
��� porta	747.49
��� pulmonar	747.49
�� v�rtebra	756.19
�� ves�cula biliar	751.69
�� vulva	752.49
�� yeyuno	751.5
� tiroides (gl�ndula)	759.2
�� cart�lago	748.3
� tobillo (articulaci�n)	755.69
� t�rax (pared)	756.3
� tracto digestivo	751.9
�� inferior	751.5
"�� superior (cualquier parte, salvo lengua)"	750.9
��� lengua	750.10
���� tipo especificado NCOC	750.19
�� tipo especificado NCOC 	751.8
� tracto gastrointestinal	751.9
�� tipo especificado NCOC	751.8
� trago	744.3
"� tr�quea, traqueal"	748.3
�� anillos	748.3
�� cart�lago	748.3
� tricrom�tica	368.59
� tricromatopsia	368.59
� tric�spide (c�spide) (v�lvula)	746.9
�� atresia	746.1
"�� Ebstein, de"	746.2
�� estenosis	746.1
�� tipo especificado NCOC	746.89
� trompa de Eustaquio	744.3
�� tipo especificado NCOC	744.24
� trompa de Falopio	752.10
�� tipo especificado NCOC	752.19
� tronco	759.9
"� Uhl, de (hipoplasia de miorcardio, ventr�culo derecho)"	746.84
"� uni�n, tr�quea con laringe"	748.3
� uraco	753.7
�� tipo especificado NCOC	753.7
� ur�ter	753.9
�� obstructiva	753.2
�� tipo especificado NCOC	753.4
� uretra (v�lvula)	753.9
�� obstructiva	753.6
�� tipo especificado NCOC	753.8
"� urinario, tracto o aparato (cualquier parte, salvo uraco)"	753.9
�� tipo especificado NCOC	753.8
�� uraco	753.7
� �tero	752.3
�� con funcionamiento de un solo cuerno	752.3
�� en el embarazo o parto	654.0
��� que afecta al feto o al reci�n nacido	763.8
��� que obstruye el parto	660.2
���� que afecta al feto o al reci�n nacido	763.1
� �vula	750.9
� u�a	757.9
�� tipo especificado NCOC	757.5
� vagina	752.40
� val�culas	748.3
� v�lvula (coraz�n) NCOC	746.9
"�� formaci�n, ur�ter"	753.2
�� pulmonar	746.00
�� tipo especificado NCOC	746.89
� v�lvula tebesiana 	746.9
� vascular NCOC	747.6
�� anillo	747.21	
� vaso deferente	752.9	
� vaso sangu�neo	747.9	
�� arteria		"v�ase Anomal�a, arteria"
� ri��n	747.6	
�� vena		"v�ase Anomal�a, vena"
� vasos NCOC	747.6	
�� papila �ptica	743.9	
� vasos opticociliares	743.9	
� vejiga (cuello) (esf�nter) (trigono)	753.9	
�� tipo especificado NCOC 	753.8	
� vena cava (inferior) (superior)	747.40	
� vena cava superior	747.40	
� vena porta	747.40	
� vena(s) perif�rica(s)	747.6	
�� cerebral	747.81	
�� cerebro	747.81	
�� coronaria	746.89	
�� grandes	747.40	
��� tipo especificado NCOC	747.49	
�� porta	747.40	
�� pulmonar	747.40	
�� retina	743.9	
"� ventr�culo, ventricular (coraz�n)"	746.9	
�� bandas	746.9	
�� repliegues	746.9	
�� tabiques	745.4	
� v�rtebra	756.10	
� ves�cula biliar	751.60	
� Virus del SIDA (enfermedad) (infecci�n)		v�ase Virus de inmunodeficiencia humana (enfermedad) (infecci�n)
� vulva	752.40	
� yeyuno	751.5	
Anomia	784.69	
Anoniquia	757.5	
� adquirida	703.8	
Anopsia (altitudinal) (cuadrante)	368.46	
Aonrexia	783.0	
� hist�rica	300.11	
� nerviosa	307.1	
"Anormal, anormalidad, anormalidades"		v�ase adem�s Anomal�a
� amnios	658.9	
�� que afecta al feto o reci�n nacido	762.9	
� andar	781.2	
�� hist�rica	300.11	
"� aperturas, cong�nita, diafragma"	756.6	
� aumento		
�� de		
��� apetito	783.6	
��� desarrollo	783.9	
� autosomas NCOC	758.5	
�� 13	758.1	
�� 18	758.2	
�� 21 � 22	758.0	
�� D1	758.1	
�� E3	758.2	
�� G	758.0	
� balistocardiograma	794.39	
"� bios�ntesis, andr�geno testicular"	257.2	
� cabeza	756.0	"v�ase adem�s Anomal�a, cr�neo"
� calidad de leche	676.8	
� cariotipo	795.2	
� cart�lago traqueal (cong�nito)	748.3	
� cierre maxilar	524.5	
� comportamiento respiratorio		v�ase Respiraci�n
"� composici�n qu�mica, sangre NCOC"	790.6	
� comunicaci�n		v�ase F�stula
� configuraci�n de pupilas	379.49	
"� contracci�n muscular, localizada"	728.85	
� coraz�n		
"�� ritmo, feto"		v�ase Sufrimiento fetal
�� ruidos NCOC	785.3	
�� sombra	793.2	
� cord�n umbilical		
�� especificado NCOC	663.8	
�� que afecta al feto o al reci�n nacido	762.6	
�� que complica el parto	663.9	
� corion	658.9	
�� que afecta al feto o al reci�n nacido	762.9	
� coronaria		
�� arteria 	746.85	
�� vena	746.9	
� correspondencia retinal	368.34	
� cromos�mica NCOC	758.9	
"�� an�lisis, resultado no espec�fico"	795.2	
�� autosomas	758.5	"v�ase adem�s Anormal, autosomas NCOC"
�� fetal (sospecha de) cuando afecta la atenci�n del embarazo	655.1	
�� sexo	758.8	
� cuello uterino (adquirido) NCOC	622.9	
�� cong�nito	752.40	
�� en embarazo o parto	654.6	
��� que causa parto obstruido	660.2	
���� que afecta al feto o reci�n nacido	763.1	
"� curso, trompa de Eustaquio"	744.24	
"� curva de adaptaci�n, oscuridad"	368.63	
� dentofacial NCOC	524.9	
�� funcional	524.5	
�� tipo especificado NCOC	524.8	
� deposiciones NCOC	787.7	
�� abultadas	787.7	
�� color (oscuro) (claro)	792.1	
�� contenido (grasa) (moco) (pus)	792.1	
�� sangre oculta	792.1	
�� sangrientas	578.1	
��� ocultas	792.1	
"� desarrollo, del desarrollo NCOC"	759.9	
�� hueso	756.9	
�� sistema nervioso central	742.9	
� dientes	520.2	
� Dinia	286.9	"v�ase adem�s Defecto, coagulaci�n"
"� direcci�n, dientes"	524.3	
� distribuci�n de grasa	782.9	
� Ebstein	746.2	
� ecocardiograma	793.2	
 econecefalograma	794.01	
� ecograma NCOC		"v�ase Resultados, anormales estructura"
� electrocardiograma (ECG) (EKG)	794.31	
� electroencefalograma (EEG)	794.02	
� electromiograma (EMG)	794.17	
�� ocular	794.14	
� elctrooculograma (EOG)	794.12	
� electrorretinograma (ERG)	794.11	
� equilibrio acidob�sico	276.4	
�� feto o reci�n nacido		"v�ase Sufrimiento, fetal"
� eritrocitos	289.9	
"�� cong�nita, con ictericia perinatal"	282.9 [774.0]	
� espermatozoides	792.2	
� esputo (cantidad) (color) (excesivo) (olor) (purulento)	786.4	
� estrechez de p�rpado	743.62	
� estudios funcionales	
�� auditivos	794.15
�� bazo	794.9
�� cardiovasculares	794.30
�� cerebro	794.00
�� endocrinos NCOC	794.6
�� h�gado	794.8
�� oculomotores	794.14
�� p�ncreas	794.9
�� placenta	794.9
�� pulmonares	794.2
�� retina	794.11
�� ri��n	794.4
�� sentidos	794.19
�� sistema nervioso	
��� central	794.00
��� perif�rico	794.19
�� tiroides	794.5
�� vejiga	794.9
�� vestibular	794.16
� ex�men rediol�gico	793.9
�� abdomen NCOC	793.6
�� cabeza	793.0
�� cr�neo	793.0
�� mama	793.8
�� �rgano intrator�cico NCOC	793.2
�� �rganos genitourinarios	793.5
�� piel y tejidos subcut�neos	793.9
�� pulm�n (campo)	793.1
�� retroperitoneo	793.6
�� sistema musculoesquel�tico	793.7
�� tracto biliar	793.3
�� tracto gastrointestinal	793.4	
� examen rayos x		"v�ase Anormal, examen radiol�gico"
� excitabilidad bajo estr�s menor	301.9	
� feto NCOC 		
�� cuando afecta la atenci�n del embarazo		"v�ase Embarazo, atenci�n afectada por, fetal"
�� cuando causa desproporci�n	653.7	
��� cuando obstruye el parto	660.1	
���� que afecta al feto o al reci�n nacido	763.1	
��� que afecta al feto o al reci�n nacido	763.1	
� fonocardiograma	794.39	
� forma		
�� c�rnea	743.41	
�� �tero gr�vido	654.4	
��� que afecta al feto o al reci�n nacido	763.8	
��� que obstruye el parto	660.2	
���� que afecta al feto o al recil�n nacido	763.1	
�� ves�cula biliar	751.69	
"�� forma, de dientes"	520.2	
� formaci�n de tejido duro en pulpa	522.3	
� fuerzas del parto NCOC	661.9	
�� que afecta al feto o al reci�n nacido	763.7	
� fuerzas o estados de gravedad (G)	994.9	
� globulina		
�� corticotr�pica	255.8	
�� tirotr�pica	246.8	
� globulina corticotr�pica	255.8	
� globulina tiroidea	246.8	
� gl�bulos rojos	790.0	
�� morfolog�a	790.0	
�� volumen	790.0	
� heces	787.7	
� hemoglobina	282.7	"v�ase adem�s Enfermedad, hemoglobina"
�� rasgo		"v�ase Rasgo, hemoglobina, anormal"
� hemorragia uterina	626.9	"v�ase adem�s Hemorragia, �tero"
�� climat�rica	627.0	
�� postmenop�usica	627.1	
"� hemorragia, �tero"		"v�ase Hemorragia, �tero"
� histolog�a NCOC	795.4	
� l�quido		
�� amni�tico	792.3	
�� cefalorraqu�deo	792.0	
�� peritoneal	792.9	
�� pleural	792.9	
�� sinovial	792.9	
�� vaginal	792.9	
"� longitud, �rgano o sitio"		v�ase Distorsi�n
� mamograma	793.8	
� membranas (fetales)		
�� que afecta al feto o al reci�n nacido	762.9	
�� que complica el embarazo	658.8	
� menstruaci�n		v�ase Menstruaci�n
� metabolismo	783.9	v�ase adem�s la enfermedad
� micci�n NCOC	788.6	
� mioglobina (Aberdeen) (Annapolis)	289.9	
� movimiento	781.0	
�� cabeza	781.0	
�� involuntario	781.0	
�� tipo especificado NCOC	333.99	
�� trastorno NCOC	333.90	
��� especificado NCOC	333.99	
� movimiento de la cabeza	781.0	
� movimiento involuntario	781.0	
� nivel sangu�neo (de)		
�� cinc	790.6	
�� cobalto	790.6	
�� cobre	790.6	
�� hierro	790.6	
�� litio	790.6	
�� magnesio	790.6	
�� minerales	790.6	
� nivel s�rico (de)		
�� amilasa	790.5	
�� enzimas NCOC	790.5	
�� fosfatasa �cida	790.5	
�� fosfatasa alcalina	790.5	
�� lipasa	790.5	
"� �rgano o sitio, cong�nito NCOC"	 	v�ase Distorsi�n
� �rganos o tejidos de pelvis NCOC en embarazo o parto	654.9	
�� que afecta al feto o al reci�n nacido	763.8	
�� que obstruye el parto	660.2	
��� que afecta al feto o al reci�n nacido	763.1	
� origen		"v�ase Mala posici�n, cong�nita"
� orina (componentes (NCOC))	791.9	
� Papanicolau (frotis)		
�� cuello uterino	795.0	
�� otro sitio	795.1	
� partes blandas de la pelvis		"v�ase Anormal, �rganos o tejidos de la pelvis"
� parto		
�� madre	 	"v�ase Parto, complicado"
�� que afecta al feto o al reci�n nacido	763.9	
� parto NCOC	661.9	
�� que afecta al feto o al reci�n nacido	763.7	
� pelo NCOC	704.2	
� pelvis �sea 		"v�ase Deformidad, pelvis"
� percepci�n auditiva NCOC	388.40	
"� percusi�n, pecho"	786.7	
� p�rdida de peso	783.2	
� per�odos (abundantes)	626.9	v�ase adem�s Menstruaci�n
� peso		
�� ganancia	783.1	
��� en el embarazo	646.1	
���� con hipertensi�n		v�ase Toxemia del embarazo
�� p�rdida	783.2	
"� piel y ap�ndices, cong�nita NCOC"	757.9	
� placenta		"v�ase Placenta, anormal"
� pliegues pleurales	748.8	
� posici�n		v�ase adem�s Malposici�n
�� �tero gr�vido	654.4	
��� que obstruye el parto	660.2	
���� que afecta al feto o al reci�n nacido	763.1	
� postura NCOC	781.9	
� potenciales evocados visuales (PEV)	794.13	
� presentaci�n (feto)		"v�ase Presentaci�n, feto, anormal"
� producto de concepci�n	631	
� prote�na pl�smica		"v�ase Carencia, plasma prote�na"
� prueba cal�rica	794.19	
� prueba de funci�n renal	794.4	
� prueba de tolerancia de glucosa	790.2	
"�� en embarazo, parto o puerperio"	648.8	
�� feto o reci�n nacido	775.0	
� prueba Mantoux	795.5	
� pubertad		v�ase Pubertad
� pulmonar		
�� arteria	747.3	
"�� funci�n, reci�n nacido"	770.8	
�� resultados de pruebas	794.2	
�� ventilaci�n reci�n nacido	770.8	
��� hiperventilaci�n	786.01	
