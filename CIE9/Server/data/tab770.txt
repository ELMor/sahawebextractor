Antiinflamatorios (t�pico) 	976.0	E858.7	E946.0	E950.4	E962.0	E980.4	
Antilewisita brit�nica	963.8	E858.1	E933.8	E950.4	E962.0	E980.4	
Antilip�micos	972.2	E858.3	E942.2	E950.4	E962.0	E980.4	
Antimicobacterianos NEC	961.8	E857	E931.8	E950.4	E962.0	E980.4	
� antibi�ticos	960.6	E856.7	E930.6	E950.4	E962.0	E980.4	
Antimonio (compuestos) (emanaciones) NEC	985.4	E866.2	-	E950.9	E962.1 	E980.9	
� antiinfecciosos	961.2	E857	E931.2	E950.4	E962.0	E980.4	
� pesticidas (emanaciones)	985.4	E863.4	-	E950.6	E962.2	E980.7	
� tartrato de potasio	961.2	E857	E931.2	E950.4	E962.0	E980.4	
� tartatrado	961.2	E857	E931.2	E950.4	E962.0	E980.4	
Antimonio tartrado (antiinfeccioso)	961.2	E857	E931.2	E950.4	E962.0	E980.4	
Antimuscar�nicos	971.1	E855.4	E941.1	E950.4	E962.0	E980.4	
Antineopl�sicos	963.1	E858.1	E933.1	E950.4	E962.0	E980.4	
� antibi�ticos	960.7	E856	E930.7	E950.4	E962.0	E980.4	
Antipal�dicos	961.4	E857	E931.4	E950.4	E962.0	E980.4	
Antipir�ticos	965.9	E850.9	E935.9	E950.0	E962.0	E980.0
� especificado NEC	965.8	E850.8	E935.8	E950.0	E962.0	E980.0
Antipirina	965.5	E850.5	E935.5	E950.0	E962.0	E980.0
Antiplog�sticos	965.6	E850.6	E935.6	E950.0	E962.0	E980.0
Antiprotozoarios NEC	961.5	E857	E931.5	E950.4	E962.0	E980.4
� sangre	961.4	E857	E931.4	E950.4	E962.0	E980.4
Antiprur�ticos (local)	976.1	E858.7	E946.1	E950.4	E962.0	E980.4
Antipsic�ticos NEC	969.3	E853.8	E939.3	E950.3	E962.0	E980.3
Antirreum�ticos	965.6	E850.6	E935.6	E950.0	E962.0	E980.0
Antiseborreicos	976.4	E858.7	E946.4	E950.4	E962.0	E980.4
Antis�pticos (uso externo) (medicinal)	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
Antistina	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
Antitiroideos	962.8	E858.0	E932.8	E950.4	E962.0	E980.4
Antitoxina cualquiera	979.9	E858.8	E949.9	E950.4	E962.0	E980.4
Antituberculosos	961.8	E857	E931.8	E950.4	E962.0	E980.4
� antibi�ticos	960.6	E856	E930.6	E950.4	E962.0	E980.4
Antitusivos	975.4	E858.6	E945.4	E950.4	E962.0	E980.4
Antivaricosos (esclerosantes)	972.7	E858.3	E942.7	E950.4	E962.0	E980.4
Antiveneno (crotalina) (picadura-mordedura)	979.9	E858.8	E949.9	E950.4	E962.0	E980.4
Antivert	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
Antivirales NEC	961.7	E857	E931.7	E950.4	E962.0	E980.4
Antizolina	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
Antopterina	963.1	E858.1	E933.1	E950.4	E962.0	E980.4
Antralina	976.4	E858.7	E946.4	E950.4	E962.0	E980.4
Antramicina	960.7	E856	E930.7	E950.4	E962.0	E980.4
Antrol	989.4 	E863.4	-	E950.6	E962.1	E980.7
� fungicida	989.4 	E863.6		E950.6	E962.1	E980.7
Apomorfina clorhidrato (em�tico)	973.6	E858.4	E943.6	E950.4	E962.0	E980.4
Apresolina	972.6	E858.3	E942.6	E950.4	E962.0	E980.4
Aprobarbital aprobarbitona	967.0	E851	E937.0	E950.1	E962.0	E980.1
Apronalida	967.8	E852.8	E937.8	E950.2	E962.0	E980.2
Aqua fortis	983.1	E864.1 	E950.7	E962.1	E980.6	
Arachis aceite (t�pico)	976.3 	E857.7	E946.3	E950.4	E962.0	E980.4
� cat�rtico	973.2 	E858.4	E443.2	E950.4	E962.0	E980.4
Aral�n	961.4 	E857	E931.4	E950.4	E962.0 	E980.4
Ara�a (picadura) (veneno)	989.5	E905.1 	-	E950.9	E962.1 	E980.9
� antiveneno	979.9	E858.8	E949.9	E950.4	E962.0 	E980.4
Ara�a marr�n (picadura) (veneno)	989.5	E905.1 	-	E950.9	E962.1	E980.9
Arginina sales	974.5 	E858.5	E944.5	E950.4	E962.0	E980.4
Argirol	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
� agente ENT	976.6	E858.7	E946.6	E950.4	E962.0	E980.4
� preparado oft�lmico	976.5	E858.7	E946.5	E950.4	E962.0	E980.4
Aristocort	962.0	E858.0	E932.0	E950.4	E962.0	E980.4
� agente ENT	976.6	E858.7	E946.6	E950.4	E962.0	E980.4
� preparado oft�lmico	976.5	E858.7	E946.5	E950.4	E962.0	E980.4
� t�pico NEC	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
"Arom�tico, corrosivo"	983.0	E864.0	-	E950.7	E962.1	E980.6
� desinfectantes	983.0	E861.4	-	E950.7	E962.1	E980.6
Arsenato de plomo (insecticida)	985.1	E863.4	-	E950.8	E962.1	E980.8
� herbicida	985.1	E863.5	-	E950.8	E962.1	E980.8
"Ars�nico, arsenicales (compuestos) (polvo) (emanaciones) (vapor) NEC"	985.1	E866.3	-	E950.8	E962.1	E980.8
� antiinfecciosos	961.1	E857	E931.1	E950.4	E962.0	E980.4
� pesticida (polvo) (emanaciones)	985.1	E863.4	-	E950.8	E962.1	E980.8
Arsfenamina (plata)	961.1	E857	E931.1	E950.4	E962.0	E980.4
Arsina (gas)	985.1	E866.3	-	E950.8	E962.1	E980.8
Arstinol	961.1	E857	E931.1	E950.4	E962.0	E980.4
Artane	966.4	E855.0	E936.4	E950.4	E962.0	E980.4
Artr�podo (venenoso) NEC	989.5	E905.5 	-	E950.9	E962.1	E980.9
Ascaridol	961.6	E857	E931.6	E950.4	E962.0	E980.4
"Asc�rbico, �cido"	963.5	E858.1	E933.5	E950.4	E962.0	E980.4
Asiaticosida	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
Aspidium (oleoresina)	961.6	E857	E931.6	E950.4	E962.0	E980.4
Aspirina	965.1	E850.3	E935.3	E950.0	E962.0	E980.0
Astringentes (local)	976.2	E858.7	E946.2	E950.4	E962.0	E980.4
Atabrina	961.3	E857	E931.3	E950.4	E962.0	E980.4
Atapulgita	973.5	E858.4	E943.5	E950.4	E962.0	E980.4
Ataracticos	969.5	E853.8	E939.5	E950.3	E962.0	E980.3
Atofano	974.7	E858.5	E944.7	E950.4	E962.0	E980.4
Aton�a intestinal medicamento	973.3	E858.4	E943.3	E950.4	E962.0	E980.4
Atropina	971.1	E855.4	E941.1	E950.4	E962.0	E980.4
Attenuvax	979.4	E858.8	E949.4	E950.4	E962.0	E980.4
Aureomicina	960.4	E856	E930.4	E950.4	E962.0	E980.4
� preparado oft�lmico	976.5	E858.7	E946.5	E950.4	E962.0	E980.4
� t�pico NEC	976.0 	E858.7	E946.0	E950.4	E962.0	E980.4
Aurotioglicanida	965.6	E850.6	E935.6	E950.0	E962.0	E980.0
Aurotioglucosa	965.6	E850.6	E935.6	E950.0	E962.0	E980.0
Aurotiomalato	965.6	E850.6	E935.6	E950.0	E962.0	E980.0
"Autom�vil, combustible"	981	E862.1	-	E950.9	E962.1	E980.9
Avispa (picadura)	989.5	E905.3	-	E950.9	E962.1	E980.9
Avisp�n (picadura)	989.5	E905.3	-	E950.9	E962.1	E980.9
Avlosulfona	961.8	E857	E931.8	E950.4	E962.0	E980.4
Avomina	967.8	E852.8	E937.8	E950.2	E962.0	E980.2
Azaciclonol	969.5	E853.8	E939.5	E950.3	E962.0	E980.3
Azafr�n de las praderas	988.2	E865.3	-	E950.9	E962.1	E980.9
Azapetina	971.3	E855.6	E941.3	E950.4	E962.0	E980.4
Azaribina	963.1	E858.1	E933.1	E950.4	E962.0	E980.4
Azaserina	960.7	E856	E930.7	E950.4	E962.0	E980.4
Azatioprina	963.1	E858.1	E933.1	E950.4	E962.0	E980.4
Azosulfamida	961.0	E857	E931.0	E950.4	E962.0	E980.4
Az�car invertida	974.5	E858.5	E944.5	E950.4	E962.0	E980.4
Azul de Evans	977.8	E858.8	E947.8	E950.4	E962.0	E980.4
Azul sulf�n (tinte de diagn�stico)	977.8	E858.8	E947.8	E950.4	E962.0	E980.4
Azulfidina 	961.0	E857	E931.0	E950.4	E962.0	E980.4
Azuresina	977.8	E858.8	E947.8	E950.4	E962.0	E980.4
Bacimicina	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
� preparado oft�lmico	976.5	E858.7	E946.5	E950.4	E962.0	E980.4
Bacitrac�n	960.8	E856	E930.8	E950.4	E962.0	E980.4
� Agente ENT	976.6 	E858.7	E946.6	E950.4	E962.0	E980.4
� preparado oft�lmico	976.5	E858.7	E946.5	E950.4	E962.0	E980.4
� t�pico NEC	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
BAL	963.8	E858.1	E933.8	E950.4	E962.0	E980.4
B�lsamo peruano	976.8	E858.7	E946.8	E950.4	E962.0	E980.4
Bametano (sulfato)	972.5	E858.3	E942.5	E950.4	E962.0	E980.4
Bamipina	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
Banewort	988.2	E865.4	-	E950.9	E962.1	E980.9
Barbenil	967.0	E851	E937.0	E950.1	E962.0	E980.1
"Barbital, barbitona"	967.0	E851	E937.0	E950.1	E962.0	E980.1
"Barbit�ricos, �cido barbit�rico"	967.0	E851	E937.0	E950.1	E962.0	E980.1
� anest�sico (intravenoso)	968.3	E855.1	E938.3	E950.4	E962.0	E980.4
Bario (carbonato) (cloruro) (sulfato)	985.8	E866.4	-	E950.9	E962.1	E980.9
� agente de diagn�stico	977.8	E858.8	E947.8	E950.4	E962.0	E980.4
� pesticida	985.8	E863.4	-	E950.6	E962.1	E980.7
� rodenticida	985.8	E863.7	-	E950.6	E962.1	E980.7
Barniz	989.8	E861.6	-	E950.9	E962.1	E980.9
� limpiador	982.8	E862.9	-	E950.9	E962.1	E980.9
"Bater�a, �cido o l�quido"	983.1	E864.1	-	E950.7	E962.1	E980.6
Bater�as de acumulaci�n (�cido) (c�lulas)	983.1	E864.1	-	E950.7	E962.1	E980.6
Baya del t�sigo	988.2	E865.4	-	E950.9	E962.1	E980.9
Bayas de acebo	988.2	E865.3	-	E950.9	E962.1	E980.9
Bayas venenosas	988.2	E865.3	-	E950.9	E962.1	E980.9
BCG vacuna	978.0	E858.8	E948.0	E950.4	E962.0	E980.4
Bearsfoot	988.2	E865.4	-	E950.9	E962.1	E980.9
Beclamida	966.3	E855.0	E936.3	E950.4	E962.0	E980.4
Befenio hidroxinaptoato	961.6	E857	E931.6	E950.4	E962.0	E980.4
Bele�o	988.2	E865.4	-	E950.9	E962.1	E980.9
Belladona (alcaloide)	971.1	E855.4	E941.1	E950.4	E962.0	E980.4
Belladona	988.2	E865.4	-	E950.9	E962.1	E980.9
Belladona 	988.2	E865.4	-	E950.9	E962.1	E980.9
� baya	988.2	E865.3	-	E950.9	E962.1	E980.9
Bemegrida	970.0	E854.3	E940.0	E950.4	E962.0	E980.4
Benacticina	969.8	E855.8	E939.8	E950.3	E962.0	E980.3
Benadril	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
Bencedrex	971.2	E855.5	E941.2	E950.4	E962.0	E980.4
Bencedrina (anfetamina)	969.7	E854.2	E939.7	E950.3	E962.0	E980.3
Benceno (acetil) (dimetil) (metil) (solvente) (vapor)	982.0	E862.4	-	E950.9	E962.1	E980.9
� hexacloruro (gama) (insecticida) (vapor)	989.2	E863.0	-	E950.6	E962.1	E980.7
Bencetonio	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
Bencexol (cloruro)	966.4 	E855.0	E936.4	E950.4	E962.0	E980.4
Bencil						
� acetato	982.8	E862.4	-	E950.9	E962.1	E980.9
� benzoato (antiinfeccioso)	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
� morfina	965.09	E850.2	E935.2	E950.0	E962.0	E980.0
� penicilina	960.0	E856	E930.0	E950.4	E962.0	E980.4
Bencilonio	971.1	E855.4	E941.1	E950.4	E962.0	E980.4
Benc�n(a) 							v�ase Ligro�na
Benciodarona	972.4	E858.3	E942.4	E950.4	E962.0	E980.4	
Bendrofluazida	974.3	E858.5	E944.3	E950.4	E962.0	E980.4	
Bendroflumetiacida	974.3	E858.5	E944.3	E950.4	E962.0	E980.4	
Benemide	974.7	E858.5	E944.7	E950.4	E962.0	E980.4	
Benetamina penicilina G	960.0	E856	E930.0	E950.4	E962.0	E980.4	
Benisona	976.0	E858.7	E946.0	E950.4	E962.0	E980.4	
Benoquina	976.8	E858.7	E946.8	E950.4	E962.0	E980.4	
Benoxinato	968.5	E855.2	E938.5	E950.4	E962.0	E980.4	
Bentonita	976.3	E858.7	E946.3	E950.4	E962.0	E980.4	
Benzalconio (cloruro)	976.0	E858.7	E946.0	E950.4	E962.0	E980.4	
� preparado oft�lmico	976.5	E858.7	E946.5	E950.4	E962.0	E980.4	
Benzamidosalicilato (calcio)	961.8	E857	E931.8	E950.4	E962.0	E980.4	
Benzatina penicilina	960.0	E856	E930.0	E950.4	E962.0	E980.4	
Benzcarbimina	963.1	E858.1	E933.1	E950.4	E962.0	E980.4	
Benzfetamina	977.0	E858.8	E947.0	E950.4	E962.0	E980.4	
Benzoca�na	968.5	E855.2	E938.5	E950.4	E962.0	E980.4
Benzodiacepinas (tranquilizantes) NEC	969.4	E853.2	E939.4	E950.3	E962.0	E980.3
Benzodiapine	969.4	E853.2	E939.4	E950.3	E962.0	E980.3
"Benzoico, �cido (con �cido salic�lico) (antiinfeccioso)"	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
Benzoilpas	961.8	E857	E931.8	E950.4	E962.0	E980.4
Benzo�na	976.3	E858.7	E946.3	E950.4	E962.0	E980.4
Benzol (vapor)	982.0 	E862.4	-	E950.9	E962.1	E980.9
Benzomorfano	965.09	E850.2	E935.2	E950.0	E962.0	E980.0
Benzonatato	975.4 	E858.6	E945.4	E950.4	E962.0	E980.4
Benzotiadiacidas	974.3	E858.5	E944.3	E950.4	E962.0	E980.4
Benzperidol	969.5	E853.8	E939.5	E950.3	E962.0	E980.3
Benzpirinio	971.0	E855.3	E941.0	E950.4	E962.0	E980.4
Benzquinamida	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
Benztiacida	974.3 	E858.8	E944.3	E950.4	E962.0	E980.4
Benztropina	971.1	E855.4	E941.1	E950.4	E962.0	E980.4
"Bergamota, aceite"	989.8	E866.8	-	E950.9	E962.1	E980.9
Berilio (compuestos) (emanaciones)	985.3	E866.4	-	E950.9	E962.1	E980.9	
Beta-caroteno	976.3	E858.7	E946.3	E950.4	E962.0	E980.4	
Beta-Clor	967.1	E852.0	E937.1	E950.2	E962.0	E980.2	
Betametasona	962.0	E858.0	E932.0	E950.4	E962.0	E980.4	
� t�pico	976.0	E858.7	E946.0	E950.4	E962.0	E980.4	
Betanecol	971.0	E855.3	E941.0	E950.4	E962.0	E980.4	
Betanidina	972.6	E858.3	E912.6	E950.4	E962.0	E980.4	
Betazol	977.8	E858.8	E947.8	E950.4	E962.0	E980.4	
Bhang	969.6	E854.1	E939.6	E950.3	E962.0	E980.3	
Bialamicol	961.5	E857	E931.5	E950.4	E962.0	E980.4	
Bicloruro de mercurio  							v�ase Mercurio cloruro
Bicromatos (calcio) (cristales) (potasio) (sodio)	983.9	E864.3	-	E950.7	E962.1	E980.6	
� emanaciones	987.8	E869.8	-	E952.8	E962.2	E982.8	
"Biguanida, derivados, oral"	962.3	E858.0	E932.3	E950.4	E962.0	E980.4	
Biligrafina	977.8	E858.8	E947.8	E950.4	E962.0	E980.4	
Bilopaque	977.8	E858.8	E947.8	E950.4	E962.0	E980.4	
Bioflavonoides	972.8	E858.3	E942.8	E950.4	E962.0	E980.4	
Biperideno	966.4	E855.0	E936.4	E950.4	E962.0	E980.4	
Bisacodyl	973.1	E858.4	E943.1	E950.4	E962.0	E980.4	
Bishydroxycoumarin 	964.2	E858.2	E934.2	E950.4	E962.0	E980.4	
Bismars�n	961.1	E857	E931.1	E950.4	E962.0	E980.4	
Bismuto (compuestos) 	985.8	E866.4	-	E950.9	E962.1	E980.9	
� antiinfecciosos	961.2	E857	E931.2	E950.4	E962.0	E980.4	
� subcarbonato	973.5	E858.4	E943.5	E950.4	E962.0	E980.4	
� sulfarsfenamina 	961.1	E857	E931.1	E950.4	E962.0	E980.4	
Bitionol	961.6	E857	E931.6	E950.4	E962.0	E980.4	
Bittersweet 	988.2	E865.4	-	E950.9	E962.1	E980.9	
Blanco							
� ars�nico 							 v�ase Ars�nico
� helecho 	988.2	E865.4	-	E950.9	E962.1	E980.9	
� loci�n (queratol�tico)	976.4	E858.7	E946.4	E950.4	E962.0	E980.4	
� tintura alcoh�lica	981	E862.0	-	E950.9	E962.1	E980.9	
Blanqueador NEC 	983.9	E864.3	-	E950.7	E962.1	E980.6	
"Blanqueantes, soluciones"	983.9	E864.3	-	E950.7	E962.1	E980.6	
Bleomicina (sulfato)	960.7	E856	E930.7	E950.4	E962.0	E980.4	
Blockain	968.9	E855.2	E938.9	E950.4	E962.0	E980.4	
� bloqueo nervioso (perif�rico) (plexo) 	968.6	E855.2	E938.6	E950.4	E962.0	E980.4	
� infiltraci�n (v�a subcut�nea) 	968.5	E855.2	E938.5	E950.4	E962.0	E980.4	
� t�pico (superficial)	968.5	E855.2	E938.5	E950.4	E962.0	E980.4	
Bloqueadores adren�rgicos 	971.3	E855.6	E941.3	E950.4	E962.0	E980.4	
Bloqueadores de la secreci�n g�strica 	973.0	E858.4	E943.0	E950.4	E962.0	E980.4	
Bolas de naftalina	989.4	E863.4	-	E950.6	E962.1	E980.7	v�ase tambi�n Pesticidas
� naftalina 	983.0	E863.4	-	E950.7	E962.1	E980.6	
Bonina	963.0	E858.1	E933.0	E950.4	E962.0	E980.4	
Borato (limpiador) (sodio)	989.6	E861.3	-	E950.9	E962.1	E980.9	
B�rax (limpiador) 	989.6	E861.3	-	E950.9	E962.1	E980.9	
"B�rico, �cido "	976.0	E858.7	E946.0	E950.4	E962.0	E980.4	
� ENT agente	976.6	E858.7	E946.6	E950.4	E962.0	E980.4	
� preparado oft�lmico 	976.5	E858.7	E946.5	E950.4	E962.0	E980.4
"B�rico, �cido "	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
� ENT agente	976.6	E858.7	E946.6	E950.4	E962.0	E980.4
� preparado oft�lmico 	976.5	E858.7	E946.5	E950.4	E962.0	E980.4
"Boro, hidruro NEC "	989.8	E866.8	-	E950.9	E962.1	E980.9
� emanaciones o gas 	987.8	E869.8	-	E952.8	E962.2	E982.8
Bota de Unna 	976.3	E858.7	E946.3	E950.4	E962.0	E980.4
Brasso	981	E861.3	-	E950.9	E962.1	E980.9
Bretilio (tosilato) 	972.6	E858.3	E942.6	E950.4	E962.0	E980.4
Brevital (sodio)	968.3	E855.1	E938.3	E950.4	E962.0	E980.4
Brionia (alba) (dioica) 	988.2	E865.4	-	E950.9	E962.1	E980.9
Bromal (hidrato)	967.3	E852.2	E937.3	E950.2	E962.0	E980.2
Bromelains	963.4	E858.1	E933.4	E950.4	E962.0	E980.4
Bromfeniramina	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
Bromisovalum	967.3	E852.2	E937.3	E950.2	E962.0	E980.2
Bromo (vapor)	987.8	E869.8	-	E952.8	E962.2	E982.8
� compuestos (medicinal)	967.3	E852.2	E937.3	E950.2	E962.0	E980.2
Bromo-seltzer	965.4	E850.4	E935.4	E950.0	E962.0	E980.0
Bromobencil cianuro	987.5	E869.3	-	E952.8	E962.8	E982.8
Bromodifenhidramina	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
Bromofenol azul reactivo	977.8	E858.8	E947.8	E950.4	E962.0	E980.4
Bromoformo	967.3	E852.2	E937.3	E950.2	E962.0	E980.2
Bromosalicilhidrox�mico �cido	961.8	E857	E931.8	E950.4	E962.0	E980.4
Bromural	967.3	E852.2	E937.3	E950.2	E962.0	E980.2
Bromuros NEC	967.3	E852.2	E937.3	E950.2	E962.0	E980.2
Bronce (compuestos) (emanaciones)	985.8	E866.4	-	E950.9	E962.1	E980.9
Brucia	988.2	E865.3	-	E950.9	E962.1	E980.9
Brucina	989.1	E863.7	-	E950.6	E962.1	E980.7
Buclicina 	969.5	E853.8	E939.5	E950.3	E962.0	E980.3
Bufenina	971.2	E855.5	E941.2	E950.4	E962.0	E980.4
Bufferina	965.1	E850.3	E935.3	E950.0	E962.0	E980.0
Bufotenina	969.6	E854.1	E939.6	E950.3	E962.0	E980.3
Bupivaca�na	968.9	E855.2	E938.9	E950.4	E962.0	E980.4	
� infiltraci�n (v�a subcut�nea)	968.5	E855.2	E938.5	E950.4	E962.0	E980.4	
� bloqueo nervioso (perif�rico) (plexo)	968.6	E855.2	E938.6	E950.4	E962.0	E980.4	
Busulfano	963.1	E858.1	E933.1	E950.4	E962.0	E980.4	
Butabarbital (sodio)	967.0	E851	E937.0	E950.1	E962.0	E980.1	
Butabarbitona	967.0	E851	E937.0	E950.1	E962.0	E980.1	
Butabarpal	967.0	E851	E937.0	E950.1	E962.0	E980.1	
Butaca�na	968.5	E855.2	E938.5	E950.4	E962.0	E980.4	
Butalilonal	967.0	E851	E937.0	E950.1	E962.0	E980.1	
Butalitona (sodio) 	968.3	E855.1	E938.3	E950.4	E962.0	E980.4	
Butano (distribuido en envase m�vil)	987.0	E868.0	-	E951.1	E962.2	E981.1	
� combusti�n incompleta de 							"v�ase Mon�xido de carbono, butano"
� distribuido por tuber�as	987.0	E867	-	E951.0	E962.2	E981.0	
Butanol	980.3	E860.4	-	E950.9	E962.1	E980.9	
Butanona	982.8	E862.4	-	E950.9	E962.1	E980.9	
Butaperacina	969.1	E853.0	E939.1	E950.3	E962.0	E980.3	
Butazolidina	965.5	E850.5	E935.5	E950.0	E962.0	E980.0
Butetal	967.0	E851	E937.0	E950.1	E962.0	E980.1
Butetamato	971.1	E855.4	E941.1	E950.4	E962.0	E980.4
Butil						
� acetato (secundario)	982.8	E862.4	-	E950.9	E962.1	E980.9
� alcohol	980.3	E860.4	-	E950.9	E962.1	E980.9
� carbinol	980.8	E860.8	-	E950.9	E962.1	E980.9
� carbitol	982.8	E862.4	-	E950.9	E962.1	E980.9
� cellosolve	982.8	E862.4	-	E950.9	E962.1	E980.9
� cloral (hidrato)	967.1	E852.0	E937.1	E950.2	E962.0	E980.2
"� escopolamonio, bromuro"	971.1	E855.4	E941.1	E950.4	E962.0	E980.4
� formato	982.8	E862.4	-	E950.9	E962.1	E980.9
But�n	968.5	E855.2	E938.5	E950.4	E962.0	E980.4
Butirofenona (tranquilizantes con base de)	969.2	E853.1	E939.2	E950.3	E962.0	E980.3
Butisol (sodio)	967.0	E851	E937.0	E950.1	E962.0	E980.1
"Butobarbital, butobarbitona"	967.0	E851	E937.0	E950.1	E962.0	E980.1
Butriptilina	969.0	E854.0	E939.0	E950.3	E962.0	E980.3	
"Cacodil, cacod�lico, �cido "							v�ase Ars�nico
Cactinomicina	960.7	E856	E930.7	E950.4	E962.0	E980.4	
"Cada, aceite"	976.4	E858.7	E946.4	E950.4	E962.0	E980.4	
Cadmio (cloruro) (compuestos) (polvo) (emanaciones) (�xido)	985.5	E866.4	-	E950.9	E962.1	E980.9	
� sulfuro (medicinal) NEC	976.4	E858.7	E946.4	E950.4	E962.0	E980.4	
Cafe�na	969.7	E854.2	E939.7	E950.3	E962.0	E980.3	
Caf�	989.8	E866.8	-	E950.9	E962.1	E980.9	
Cal (cloruro)	983.2	E864.2	-	E950.7	E962.1	E980.6	
"� soluci�n, sulfurado"	976.4	E858.7	E946.4	E950.4	E962.0	E980.4	
Cal viva	983.2	E864.2	-	E950.7	E962.1	E980.6	
Calabar haba de	988.2	E865.4	-	E950.9	E962.1	E980.9	
Caladium seguinium	988.2	E865.4	-	E950.9	E962.1	E980.9	
Calamina (linimento) (loci�n)	976.3	E858.7	E946.3	E950.4	E962.0	E980.4	
Calciferol	963.5	E858.1	E933.5	E950.4	E962.0	E980.4	
Calcio (sales) NEC	974.5	E858.5	E944.5	E950.4	E962.0	E980.4	
� acetilsalicilato 	965.1	E850.3	E935.3	E950.0	E962.0	E980.0	
� benzamidosalicilato	961.8	E857	E931.8	E950.4	E962.0	E980.4	
� carbaspirina	965.1	E850.3	E935.3	E950.0	E962.0	E980.0	
� carbamida (citratada)	977.3	E858.8	E947.3	E950.4	E962.0	E980.4	
� carbonato (anti�cido)	973.0	E858.4	E943.0	E950.4	E962.0	E980.4	
� cianuro (citratado)	977.3	E858.8	E947.3	E950.4	E962.0	E980.4	
� dioctil sulfosuccinato	973.2	E858.4	E943.2	E950.4	E962.0	E980.4	
� disodio edatamil	963.8	E858.1	E933.8	E950.4	E962.0	E980.4	
� disodio edetato	963.8	E858.1	E933.8	E950.4	E962.0	E980.4	
� EDTA	963.8	E858.1	E933.8	E950.4	E962.0	E980.4	
"� hidrato, hidr�xido"	983.2	E864.2	-	E950.7	E962.1	E980.6	
� mandelato	961.9	E857	E931.9	E950.4	E962.0	E980.4	
� �xido	983.2	E864.2	-	E950.7	E962.1	E980.6	
Calicre�na	972.5	E858.3	E942.5	E950.4	E962.0	E980.4	
Calomelanos 							v�ase Mercurio cloruro
Calusterona	963.1	E858.1	E933.1	E950.4	E962.0	E980.4	
Callicidas	976.4	E858.7	E946.4	E950.4	E962.0	E980.4
Cambogia	973.1	E858.4	E943.1	E950.4	E962.0	E980.4
Camoquina	961.4	E857	E931.4	E950.4	E962.0	E980.4
Canabinol	969.6	E854.1	E939.6	E950.3	E962.0	E980.3
Canamicina	960.6	E856	E930.6	E950.4	E962.0	E980.4
Candeptina	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
Candicidina	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
Cannabis (derivados) (indica) (sativa)	969.6	E854.1	E939.6	E950.3	E962.0	E980.3
Cant�rida	976.8	E858.7	E946.8	E950.4	E962.0	E980.4
"Cant�ridas, cantaridina, cantaris"	976.8	E858.7	E946.8	E950.4	E962.0	E980.4
Cantrex	960.6	E856	E930.6	E950.4	E962.0	E980.4
Caol�n	973.5	E858.4	E943.5	E950.4	E962.0	E980.4
Capreomicina	960.6	E856	E930.6	E950.4	E962.0	E980.4
"Captodiama, captodiamina"	969.5	E853.8	E939.5	E950.3	E962.0	E980.3
Caracolicida	989.4	E863.4	-	E950.6	E962.1	E980.7
Caramif�n (clorhidrato)	971.1	E855.4	E941.1	E950.4	E962.0	E980.4
Caraya (goma)	973.3	E858.4	E943.3	E950.4	E962.0	E980.4
Carbacol	971.0	E855.3	E941.0	E950.4	E962.0	E980.4
"Carbacrilamina, resinas"	971.0	E855.3	E944.5	E950.4	E962.0	E980.4
Carbamacepina	966.3	E855.0	E936.3	E950.4	E962.0	E980.4
Carbamato (sedante)	967.8	E852.8	E937.8	E950.2	E962.0	E980.2
� herbicida 	989.3	E863.5	-	E950.6	E962.1	E980.7
� insecticida 	989.3	E863.2	-	E950.6	E962.1	E980.7
Carb�micos �steres	967.8	E852.8	E937.8	E950.2	E962.0	E980.2
Carbamida	974.4	E858.5	E944.4	E950.4	E962.0	E980.4
� t�pico	976.8	E858.7	E946.8	E950.4	E962.0	E980.4
"Carbamilcolina, cloruro"	971.0	E855.3	E941.0	E950.4	E962.0	E980.4
Carbaril	989.3	E863.2	-	E950.6	E962.1	E980.7
Carbarsona	961.1	E857	E931.1	E950.4	E962.0	E980.4
Carbaspirina	965.1	E850.3	E935.3	E950.0	E962.0	E980.0
Carbazocromo	972.8	E858.3	E942.8	E950.4	E962.0	E980.4
Carbenicilina	960.0	E856	E930.0	E950.4	E962.0	E980.4
Carbenoxolona	973.8	E858.4	E943.8	E950.4	E962.0	E980.4	
Carbetapentano	975.4	E858.6	E945.4	E950.4	E962.0	E980.4	
Carbimazol	962.8	E858.0	E932.8	E950.4	E962.0	E980.4	
Carbinol	980.1	E860.2	-	E950.9	E962.1	E980.9	
Carbinoxamina	963.0	E858.1	E933.0	E950.4	E962.0	E980.4	
Carbitol	982.8	E862.4	-	E950.9	E962.1	E980.9	
Carbitral 	967.0	E851	E937.0	E950.1	E962.0	E980.1	
Carboca�na 	968.9	E855.2	E938.9	E950.4	E962.0	E980.4	
� bloqueo nervioso (perif�rico) (plexo)	968.6	E855.2	E938.6	E950.4	E962.0	E980.4	
� infiltraci�n (v�a subcut�nea)	968.5	E855.2	E938.5	E950.4	E962.0	E980.4	
� t�pico (superficial) 	968.5	E855.2	E938.5	E950.4	E962.0	E980.4	
"Carbol-fucsina, soluci�n "	976.0	E858.7	E946.0	E950.4	E962.0	E980.4	
"Carb�lico, �cido"	983.0	E864.0	-	E950.7	E962.1	E980.6	v�ase tambi�n Fenol 
Carbomicina	960.8	E856	E930.8	E950.4	E962.0	E980.4	
Carbono							
� bisulfuro (I�quido) (vapor)	982.2	E862.4	-	E950.9	E962.1	E980.9	
� di�xido (gas)	987.8	E869.8	-	E952.8	E962.2	E982.8
� disulfuro (I�quido) (vapor)	982.2	E862.4	-	E950.9	E962.1	E980.9
� mon�xido (de la combusti�n incompleta de) (en) NEC	986	E868.9	-	E952.1	E962.2	E982.1
�� butano (distribuido en envases m�viles)	986	E866.0	-	E951.1	E962.2	E981.1
��� distribuido por tuber�as 	986	E867	-	E951.0	E962.2	E981.0
"�� carb�n vegetal, emanaciones"	986	E868.3	-	E952.1	E962.2	E982.1
�� carb�n						
��� gas (por tuber�a)	986	E867	-	E951.0	E962.2	E981.0
"��� s�lido (en estufas, hogares, dom�sticos)"	986	E868.3	-	E952.1	E962.2	E982.1
�� combustible (de uso dom�stico) 	986	E868.3	-	E952.1	E962.2	E982.1
��� gas (por tuber�a)	986	E867	-	E951.0	E962.2	E981.0
���� en envase m�vil	986	E868.0	-	E951.1	E962.2	E981.1
"�� combustibles o gases industriales, todos"	986	E868.8	-	E952.1	E962.2	E982.1
"�� coque (en estufas, hogares, dom�sticos)"	986	E868.3	-	E952.1	E962.2	E982.1
"�� escape de gases (motor) no en transito,"	986	E868.2	-	E952.0	E962.2	E982.0
"��� motor de combusti�n, de todo tipo, pero no en embarcaciones"	986	E868.2	-	E952.0	E962.2	E982.0
��� motor de gas	986	E868.2	-	E952.0	E962.2	E982.0
��� motobombeadora	986	E868.2	-	E952.0	E962.2	E982.0
"��� tractor agr�cola, no en tr�nsito"	986	E868.2	-	E952.0	E962.2	E982.0
"��� veh�culo de motor, no en tr�nsito"	986	E868.2	-	E952.0	E962.2	E982.0
"�� escape de gases del motor, no en tr�nsito"	986	E868.2	-	E952.0	E962.2	E982.0
�� estufa de gas	986	E868.1	-	E951.8	E962.2	E981.8
��� por tuber�a	986	E867	-	E951.0	E962.2	E981.0
�� fuente especificada NEC	986	E868.8	-	E952.1	E962.2	E982.1
�� gas de altos hornos	986	E868.8	-	E952.1	E962.2	E982.1
�� gas de iluminaci�n	986	E868.1	-	E951.8	E962.2	E981.8
�� gas licuado	986	E868.1	-	E951.8	E962.2	E981.8
�� gas por tuber�a (manufacturado) (natural)	986	E867	-	E951.0	E962.2	E981.0
�� gas productor	986	E868.8	-	E952.1	E962.2	E982.1
"�� horno, gas o vapor"	986	E868.8	-	E652.1	E962.2	E982.1
"�� keroseno (en estufas, hogares, dom�sticos)"	986	E868.3	-	E952.1	E962.2	E982.1
"�� madera (en estufas, hogares, dom�sticos)"	986	E868.3	-	E952.1	E962.2	E982.1
�� servicios	986	E868.1	-	E951.8	E962.2	E981.1	
���� en envase m�vil	986	E868.0	-	E951.1	E962.2	E981.1	
���� por tuber�a (natural)	986	E867	-	E951.0	E962.2	E981.0	
�� propano (distribuido en envase m�vil)	986	E868.0	-	E951.1	E962.2	E981.1	
��� distribuido por tuber�as	986	E867	-	E951.0	E962.2	E981.0	
�� servicios de gas	986	E868.1	-	E951.8	E962.2	E981.8	
��� por tuber�a	986	E867	-	E951.0	E962.2	E981.0	
� tetracloruro (vapor) NEC	987.8	E869.8	-	E952.8	E962.2	E982.8	
�� disolvente	982.1	E862.4	-	E950.9	E962.1	E980.9	
�� l�quido (agente limpiador) NEC	982.1	E861.3	-	E950.9	E962.1	E980.9	
Carb�n (mon�xido de carbono proveniente de) 							v�ase tambi�n Carbono mon�xido carb�n
� aceite 							v�ase Keroseno
� emanaciones	987.8	E869.8	-	E952.8	E962.2	E982.8	
� medicinal (pomada)	976.4	E858.8	E946.4	E950.4	E962.0	E980.4	
�� analg�sicos NEC	965.5	E850.5	E935.5	E950.0	E962.0	E980.0	
� nafta (disolvente)	981	E862.0	-	E950.9	E962.1	E980.9	
� tar NEC	983.0	E864.0	-	E950.7	E962.1	E980.6
Carb�n vegetal						
� emanaciones (mon�xido de carbono)	986	E868.3	-	E952.1	E962.2	E982.1
�� industrial	986	E868.8	-	E952.1	E962.2	E982.1
� medicinal (activado)	973.0	E858.4	E943.0	E950.4	E962.0	E980.4
"Carb�nico, �cido (gas)"	987.8	E869.8	-	E952.8	E962.2	E982.3
� inhibidores de la anhidrasa	974.2	E858.5	E944.2	E950.4	E962.0	E980.4
Carbowax	976.3	E858.7	E946.3	E950.4	E962.0	E980.4
Carbromal (derivados)	967.3	E852.2	E937.3	E950.2	E962.0	E980.2
Cardiaco						
� depresivos	972.0	E858.3	E942.0	E950.4	E962.0	E980.4
� reguladores del ritmo	972.0	E858.3	E942.0	E950.4	E962.0	E980.4
Cardio-green	977.8	E858.8	E947.8	E950.4	E962.0	E980.4
Cardiografina	977.8	E858.8	E947.8	E950.4	E962.0	E980.4
Cardrasa	974.2	E858.5	E944.2	E950.4	E962.0	E980.4
Carfenacina (maleato)	969.1	E853.0	E939.1	E950.3	E962.0	E980.3
Carfusina	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
Carisoprodol	968.0	E855.1	E938.0	E950.4	E962.0	E980.4
Carm�n	977.8	E858.8	E947.8	E950.4	E962.0	E980.4
Carmustina	963.1	E858.1	E933.1	E950.4	E962.0	E980.4
Carne enlatada	980.1	E860.2	-	E950.9	E962.1	E980.9
Carne nociva o sin bacterias 	988.8	E865.0	-	E950.9	E962.1	E980.9
Caroteno	963.5	E858.1	E933.5	E950.4	E962.0	E980.4
C�scara (sagrada)	973.1	E858.4	E943.1	E950.4	E962.0	E980.4
Castor						
� aceite	973.1	E858.4	E943.1	E950.4	E962.0	E980.4
� haba	988.2	E865.3	-	E950.9	E962.1	E980.9
Cat�rticos NEC	973.3	E858.4	E943.3	E950.4	E962.0	E980.4
� contacto	973.1	E858.4	E943.1	E950.4	E962.0	E980.4
� emoliente	973.2	E858.4	E943.2	E950.4	E962.0	E980.4
� irritantes intestinales	973.1	E858.4	E943.1	E950.4	E962.0	E980.4
� salina	973.3	E858.4	E943.3	E950.4	E962.0	E980.4
Catha (edulis)	970.8	E854.3	E940.8	E950.4	E962.0	E980.4
Catomicina	960.8	E856	E930.8	E950.4	E962.0	E980.4
Ca�stico(s)	983.9	E864.4	-	E950.7	E962.1	E980.6
� �Icali	983.2	E864.2	-	E950.7	E962.1	E980.6
� especificado NEC	983.9	E864.3	-	E950.7	E962.1	E980.6
� hidr�xido	983.2	E864.2	-	E950.7	E962.1	E980.6
� potasa	983.2	E864.2	-	E950.7	E962.1	E980.6
� soda	983.2	E864.2	-	E950.7	E962.1	E980.6
Cazabe	988.2	E865.4	-	E950.9	E962.1	E980.9
Cebadilla						
Ceepryn	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
� agente ENT	976.0	E858.7	E946.6	E950.4	E962.0	E980.4
� grageas	976.6	E858.7	E946.6	E950.4	E962.0	E980.4
Cefalexina	960.5	E856.1	E930.5	E950.4	E962.0	E980.4
Cefaloglicina	960.5	E856	E930.5	E950.4	E962.0	E980.4
Cefaloridina	960.5	E856	E930.5	E950.4	E962.0	E980.4
Cefalosporinas NEC	960.5	E856	E930.5	E950.4	E962.0	E980.4
� N (adicil-lina)	960.0	E856	E930.0	E950.4	E962.0	E980.4
Cefir�n (t�pico)	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
� preparado oft�lmico	976.5	E858.7	E946.5	E950.4	E962.0	E980.4
Celestona	962.0	E858.0	E932.0	E950.4	E962.0	E980.4
� t�pico	976.0	E858.7	E946.0	E950.4	E962.0	E980.4
"Celulosa, derivados, cat�rticos"	973.3	E858.4	E943.3	E950.4	E962.0	E980.4
� nitratos (t�pico)	976.3	E858.7	E946.3	E950.4	E962.0	E980.4
Cellosolve	982.8	E862.4	-	E950.9	E962.1	E980.9
Cephalothin (sodio)	960.5	E856	E930.5	E950.4	E962.0	E980.4
Cera (parafina) (petr�leo)	981	E862.3	-	E950.9	E962.1	E980.9
� autom�vil 	989.8	E861.2	-	E950.9	E962.1	E980.9
� suelos	981	E862.0	-	E950.9	E962.1	E980.9
Cerbera (odallam)	988.2	E865.4	-	E950.9	E962.1	E980.9
Cerberina	972.1	E8583	E942.1	E950.4	E962.0	E980.4
Cerona	980.1	E860.2	-	E950.9	E962.1	E980.9
Cetalconio (cloruro)	976.0	E858.7	E946.0	E950.4	E962.0	E980.4	
Cetamina	968.3	E855.1	E938.3	E950.4	E962.0	E980.4	
Cetilpiridinio	976.0	E858.7	E946.0	E950.4	E962.0	E980.4	
� agente ENT	976.6	E858.7	E946.6	E950.4	E962.0	E980.4	
� grageas	976.6	E858.7	E946.6	E950.4	E962.0	E980.4	
Cetobemidona	965.09	E850.2	E935.2	E950.0	E962.0	E980.0	
"Cetona, aceites"	982.8	E862.4	-	E950.9	E962.1	E980.9	
Cetoxima	963.0	E858.1	E933.0	E950.4	E962.0	E980.4	
Cetrimida	976.2	E858.7	E946.2	E950.4	E962.0	E980.4	
"Cevit�mico, �cido"	963.5	E858.1	E933.5	E950.4	E962.0	E980.4	
"Cianh�drico, �cido "							v�ase Cianuro(s)
Ci�nico �cido  							v�ase Cianuro(s)
Cianocobalamina	964.1	E858.2	E934.1	E950.4	E962.0	E980.4	
Cian�geno (cloruro) (gas) NEC	987.8	E869.8	-	E952.8	E962.2	E982.8	
Cianuro(s) (compuestos) (hidr�geno) (potasio) (sodio) NEC	989.0	E866.8	-	E950.9	E962.1	E980.9	
� fumigante	989.0	E863.8	-	E950.6	E962.1	E980.7	
� pesticida (polvo) (emanaciones)	989.0	E863.4	-	E950.6	E962.1	E980.7
� polvo o gas (inhalaci�n) NEC	987.7	E869.8	-	E952.8	E962.2	E982.8
� merc�rico v�ase Mercurio						
Cicla�na	968.5	E855.2	E938.5	E950.4	E962.0	E980.4
Ciclandelato	972.5	E858.3	E942.5	E950.4	E962.0	E980.4
Ciclazocina	965.09	E850.2	E935.2	E950.0	E962.0	E980.0
Ciclicina	963.0	E858.1	E933.0	E950.4	E962.0	E980.4
"Ciclobarbital, ciclobarbitona"	967.0	E851	E937.0	E950.1	E962.0	E980.1
Ciclofostamida	963.1	E858.1	E933.1	E950.4	E962.0	E980.4
Cicloguanil	961.4	E857	E931.4	E950.4	E962.0	E980.4
Ciclohexano	982.0	E862.4	-	E950.9	E962.1	E980.9
Ciclohexanol	980.8	E860.8	-	E950.9	E962.1	E980.9
Ciclohexanona	982.8	E862.4	-	E950.9	E962.1	E980.9
Ciclometica�na	968.5	E855.2	E938.5	E950.4	E962.0	E980.4
Ciclopentamina	971.2	E855.5	E941.2	E950.4	E962.0	E980.4
Ciclopentiacida	974.3	E858.5	E944.3	E950.4	E962.0	E980.4
Ciclopentolato	971.1	E855.4	E941.1	E950.4	E962.0	E980.4
Ciclopropano	968.2	E855.1	E938.2	E950.4	E962.0	E980.4
Cicloserina	960.6	E856	E930.6	E950.4	E962.0	E980.4
