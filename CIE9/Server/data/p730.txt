Trombocitosis esencial	289.9	
Tromboembolia 		v�ase Embolia
Tromboflebitis	451.9	
� antepartum (superficial)	671.2	
�� profunda	671.3	
�� que afecta al feto o al reci�n nacido	760.3	
� cerebral (seno) (vena)	325	
�� efecto tard�o 	326	v�ase categor�a
��� no pi�gena	437.6	
���� efecto tard�o 	438	v�ase categor�a 438
���� en el embarazo o puerperio	671.5	
� debida a dispositivo implantado 		"v�ase Complicaciones, debida a presencia de cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC "
"� despu�s de infusi�n, perfusi�n, o transfusi�n"	999.2	
� durante o como resultado de un procedimiento NCOC	997.2	
� embarazo (superficial)	671.2	
�� profunda	671.3	
�� que afecta al feto o al reci�n nacido	760.3	
� extremidad inferior	451.2	
�� profunda (vasos)	451.19	
��� vaso especificado NCOC	451.19	
��� vena femoral	451.11	
�� superficial (vasos)	451.0	
� femoral	451.11	
� femoropopl�tea	451.0	
� hep�tica (vena)	451.89	
"� idiop�tica, recurrente"	453.1	
� iliofemoral	451.11	
"� mama, superficial"	451.89	
"� migrans, migratoria"	453.1	
� pelviana 		
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con septicemia "
��� embarazo ect�pico	639.0	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.0	v�ase adem�s categor�as 630-632
�� despu�s de 		
��� aborto	639.0	
��� embarazo ect�pico o molar	639.0	
�� puerperal	671.4	
� pierna	451.2	
�� profunda (vasos)	451.19	
��� vaso especificado NCOC	451.19	
��� vena femoral	451.11	
�� superficial (vasos)	451.0	
� portal (vena)	572.1	
� posoperatoria	997.2	
"� puerperal, posparto, parto (extremidades) (superficial)"	671.2	
�� pelviana	671.4	
�� profunda	671.4	
�� sitio especificado NCOC	671.5	
� safena	451.0	
� seno		
�� cavernoso (venoso) 		"v�ase Tromboflebitis, seno venoso intracraneal "
�� intracraneal 		"v�ase Tromboflebitis, seno venoso intracraneal "
�� lateral (venoso) 		"v�ase Tromboflebitis, seno venoso intracraneal "
�� longitudinal (venoso) 		"v�ase Tromboflebitis, seno venoso intracraneal "
�� venoso intracraneal (cualquiera)	325	
��� efecto tard�o 	326	v�ase categor�a
��� no pi�gena	437.6	
���� efecto tard�o 	438	v�ase categor�a 438
���� en el embarazo o puerperio	671.5	
� sitio especificado NCOC	451.89	
� vena 		
�� il�aca	451.81	
�� popl�tea	451.19	
�� tibial	451.19	
Trombopat�a (Bernard-Soulier)	287.1	
� constitucional	286.4	
� Willebrand-Jurgens (angiohemofilia)	286.4	
Trombopenia	287.5	v�ase adem�s Trombocitopenia
"Trombosis, tromb�tico (mar�smica) (m�ltiple) (progresiva) (s�ptica) (vena) (vaso)"	453.9	
� con el parto o durante el puerperio 		"v�ase  Trombosis, puerperal, posparto "
� anteparto 		"v�ase Trombosis, embarazo "
"� aorta, a�rtica"	444.1	
�� abdominal	444.0	
�� bifurcaci�n	444.0	
�� terminal	444.0	
�� tor�cica	444.1	
�� v�lvula 		"v�ase Endocarditis, a�rtica "
"� ap�ndice, s�ptica"	540.9	
"�� con perforaci�n, peritonitis (generalizada), o ruptura"	540.0	
� apoplej�a	434.0	"v�ase adem�s Trombosis, cerebro"
�� efecto tard�o 	438	v�ase categor�a 438
� arteria(s) (posinfecciosa)	444.9	
�� auditiva interna	433.8	
�� basilar	433.0	"v�ase adem�s Oclusi�n, arteria, basilar"
�� car�tida (com�n) (interna)	433.1	"v�ase adem�s Oclusi�n, arteria, car�tida"
��� con otra arteria precerebral	433.3	
�� cerebelosa (anterior inferior) (posterior inferior) (superior)	433.8	
�� cerebral	434.0	"v�ase adem�s Trombosis, cerebro"
�� comunicante posterior	433.8	
�� coroidal (anterior)	433.8	
�� coronaria	410.9	"v�ase adem�s Infarto, miocardio"
��� curada o especificada como antigua	412	
��� debida a s�filis	093.89	
��� sin infarto de miocardio	411.81	
"�� espinal, anterior o posterior"	433.8	
�� extremidades	444.22	
��� inferior	444.22	
��� superior	444.21	
�� femoral	444.22	
�� hep�tica	444.89	
�� hipofisaria	433.8	
"�� men�ngea, anterior o posterior"	433.8	
�� mesent�rica (con gangrena)	557.0	
�� oft�lmica	362.30	"v�ase adem�s Oclusi�n, retina"
�� pontina	433.8	
�� popl�tea	444.22	
�� precerebral 		"v�ase Oclusi�n, arteria, precerebral NCOC "
�� pulmonar	415.1	
�� renal	593.81	
�� retiniana	362.30	"v�ase adem�s Oclusi�n, retina"
�� sitio especificado NCOC	444.89	
�� traum�tica (complicaci�n) (precoz)	904.9	"v�ase adem�s Traumatismo, vaso sangu�neo, por sitio"
�� vertebral	433.2	"v�ase adem�s Oclusi�n, arteria, vertebral"
��� con otra arteria precerebral	433.3	
� arterias perif�ricas	444.22	
�� inferiores	444.22	
�� superiores	444.21	
"� arteriolar-plaqueta capilar, diseminada"	446.6	
� atrial (endoc�rdica)	424.90	
�� debida a s�filis	093.89	
� auricular	410.9	"v�ase adem�s Infarto, miocardio"
� axilar (vena)	453.8	
� basilar (arteria)	433.0	"v�ase adem�s Oclusi�n, arteria, basilar"
"� bazo, espl�nica"	289.59	
�� arteria	444.89	
� blanda NCOC	453.9	
� capilar	448.9	
"�� arteriolar, generalizada"	446.6	
� cardiaca	410.9	"v�ase adem�s Infarto, miocardio"
�� curada o especificada como antigua	412	
�� debida a s�filis	093.89	
�� v�lvula 		v�ase Endocarditis 
� car�tida (arteria) (com�n) (interna)	433.1	"v�ase adem�s Oclusi�n, arteria, car�tida"
�� con otra arteria precerebral	433.3	
"� car�tida interna, arteria"	433.1	"v�ase adem�s Oclusi�n, arteria, car�tida"
�� con otra arteria precerebral	433.3	
"� cerebelosa, arteria (anterior inferior) (posterior inferior) (superior)"	433.8	
�� efecto tard�o 	438	v�ase categor�a 438
� cerebral (arterias)	434.0	"v�ase adem�s Trombosis, cerebro"
�� efecto tard�o 	438	v�ase categor�a 438
� cerebro (arteria) (ped�nculo)	434.0	
�� debida a s�filis	094.89	
�� efecto tard�o 	438	v�ase categor�a 438
"�� puerperal, posparto, parto"	674.0	
�� seno	325	"v�ase adem�s Trombosis, seno venoso intracraneal"
� como resultado de la presencia de una derivaci�n u otro dispositivo interno		"v�ase Complicaciones, debida a presencia de cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"
� conducto deferente	608.83	
� coraz�n (c�mara)	410.9	"v�ase adem�s Infarto, miocardio"
� cord�n 		
�� esperm�tico	608.83	
�� umbilical (vasos)	663.6	
��� que afecta al feto o al reci�n nacido   	762.6	
� coronaria (arteria)	410.9	"v�ase adem�s Infarto, miocardio"
�� curada o especificada como antigua	412	
�� debida a s�filis	093.89	
�� sin infarto de miocardio	411.81	
� cortical	434.0	"v�ase adem�s Trombosis, cerebro"
� cuerpo cavernoso	607.82	
"� debida a (la presencia de) cualquier dispositivo, implante o injerto clasificable bajo 996.0-996.5 "		"v�ase Complicaciones, debida a presencia de cualquier dispositivo, implante o injerto clasificado bajo 996.0-996.5 NCOC"
� embarazo	671.9	
�� profunda (vena)	671.3	
�� superficial (vena)	671.2	
� endoc�rdica		"v�ase Infarto, miocardio "
� escroto	608.83	
� esfuerzo	453.8	
� extremidad inferior 		"v�ase Trombosis, pierna "
� femoral (vena) (profunda)	453.8	
�� con inflamaci�n o flebitis	451.11	
�� arteria	444.22	
� hep�tica (vena)	453.0	
�� arteria	444.89	
�� infecciosa o s�ptica	572.1	
� h�gado (venosa)	453.0	
�� arteria	444.89	
�� infecciosa o s�ptica	572.1	
�� vena porta	452	
� il�aca (vena)	453.8	
�� con inflamaci�n o flebitis	451.81	
�� arteria (com�n) (externa) (interna)	444.81	
"� inflamaci�n, vena "		v�ase Tromboflebitis 
� intestino (con gangrena)	557.0	
� intracraneal	434.0	"v�ase adem�s Trombosis, cerebro"
�� seno venoso (cualquiera)	325	
��� origen no pi�geno	437.6	
���� en embarazo o puerperio	671.5	
� intramural	410.9	"v�ase adem�s Infarto, miocardio"
�� curada o especificada como antigua	412	
"� mar�smica, seno, duramadre"	437.6	
� m�dula espinal	336.1	
�� debida a s�filis	094.89	
�� en el embarazo o puerperio	671.5	
�� origen no pi�geno	324.1	
��� efecto tard�o 	326	v�ase categor�a
� meninges (cerebro)	434.0	"v�ase adem�s Trombosis, cerebro"
� mesent�rica (arteria) (con gangrena)	557.0	
�� vena (inferior) (superior)	452	
� mitral 		"v�ase Insuficiencia, mitral "
� mural	410.9	"v�ase adem�s Infarto, miocardio"
�� debida a s�filis	093.89	
�� despu�s de infarto de miocardio	429.79	
� oft�lmica (arteria)	362.30	"v�ase adem�s Oclusi�n, retina"
� ojo	362.30	"v�ase adem�s Oclusi�n, retina"
� omento (con gangrena)	557.0	
"� �rgano genital, masculino"	608.83	
� parietal	410.9	"v�ase adem�s Infarto, miocardio"
� pene	607.82	
� pierna	453.8	
�� con inflamaci�n o flebitis 		v�ase Tromboflebitis 
�� profunda (vasos)	453.8	
�� superficial (vasos)	453.8	
� plaqueta	446.6	
� plexo pampiniforme (masculino)	608.83	
�� femenino	620.8	
� portal	452	
�� debida a s�filis	093.89	
�� infecciosa o s�ptica	572.1	
"� precerebral, arteria "		"v�ase adem�s Oclusi�n, arteria, precerebral NCOC "
"� puerperal, posparto, parto"	671.9	
�� cardiaca	674.8	
�� cerebral (arteria)	674.0	
��� venosa	671.5	
�� cerebro (arteria)	674.0	
��� venosa	671.5	
�� pelviana	671.4	
�� profunda (vena)	671.4	
�� pulmonar (arteria)	673.2	
�� seno intracraneal (no pi�gena) (venoso)	671.5	
�� sitio especificado NCOC	671.5	
�� superficial	671.2	
� pulm�n	415.1	
� pulmonar (arteria) (vena)	415.1	
"� reblandecimiento, cerebro"	434.0	"v�ase adem�s Trombosis, cerebro"
� renal (arteria)	593.81	
�� vena	453.3	
"� retina, retiniana (arteria)"	362.30	
�� central	362.31	
�� parcial	362.33	
�� rama arterial	362.32	
�� vena 		
��� central	362.35	
��� tributaria (rama)	362.36	
� ri��n	593.81	
�� arteria	593.81	
� seno 		
�� cavernoso (venoso) 		"v�ase Trombosis, intracraneal, seno venoso "
�� intracraneal (venoso) (cualquiera)	325	"v�ase adem�s Trombosis, intracraneal, seno venoso"
�� lateral (venoso) 		"v�ase Trombosis, intracraneal, seno venoso "
�� longitudinal (venoso) 		"v�ase Trombosis, intracraneal, seno venoso "
�� sigmoideo (venoso)	325	"v�ase adem�s Trombosis, intracraneal, seno venoso"
"� sigmoide, seno (venoso)"	325	"v�ase adem�s Trombosis, intracraneal, seno venoso"
� silenciosa NCOC	453.9	
� sitio especificado NCOC	453.8	
� test�culo	608.83	
� traum�tica (complicaci�n) (precoz)	904.9	"v�ase adem�s Traumatismo, vaso sangu�neo, por sitio"
� tric�spide 		"v�ase Endocarditis, tric�spide "
� t�nica vaginal	608.83	
� vena cava (inferior) (superior)	453.2	
� ves�cula seminal	608.83	
� yugular (bulba)	453.8	
Trompa de Falopio no insuflada	628.2	
Trompa de Falopio obstruida	628.2	
Tronco		v�ase adem�s enfermedad espec�fica 
� arterioso (persistente)	745.0	
� com�n	745.0	
Tropical 		v�ase adem�s enfermedad espec�fica 
� maceraci�n de pies (s�ndrome)	991.4	
� pies h�medos (s�ndrome)	991.4	
"Trousseau, s�ndrome de (tromboflebitis migratoria)"	453.1	
"Trypanosoma, infestaci�n por "		v�ase Tripanosomiasis
"Tsutsugamusho, fiebre de"	081.2	
Tuberc�lide (indurada) (liquenoide) (miliar) (papulonecr�tica) (primaria) (piel) (subcut�nea)	017.0	v�ase adem�s Tuberculosis
 Tub�rculo 		v�ase adem�s Tuberculosis 
"� cerebro, solitario"	013.2	
"� Darwin, de"	744.29	
� epitelioide no caseoso	135	
"� Ghon, infecci�n primaria"	010.0	
� oclusal	520.2	
� pabell�n de la oreja (auriculae)	744.29	
� paramolar	520.2	
Tuberculoma 		v�ase adem�s Tuberculosis 
� cerebro (cualquier parte)	013.2	
� m�dula espinal	013.4	
� meninges (cerebrales) (espinales)	013.1	
"Tuberculosis, tubercular, tuberculoso (bacilos crom�genos acidorresistentes) (calcificada) (caseosa) (cong�nita) (degeneraci�n) (enfermedad) (f�stula) (gangrena) (intersticial) (lesiones circunscritas aisladas) (necrosis) (parenquimatosa) (ulcerativa)"	011.9	
� abdomen	014.8	
�� ganglio linf�tico	014.8	
� absceso	011.9	
�� articulaci�n	015.9
��� cadera	015.1
��� cadera	015.2
��� especificada NCOC	015.8
��� vertebral	015.0[730.88]
�� brazo	017.9
�� cadera	015.1
�� cadera	015.2
�� cerebro	013.3
"�� Cowper, gl�ndula de"	016.5
�� duramadre	013.8
��� cerebro	013.3
��� m�dula espinal	013.5
�� epidural	013.8
��� cerebro	013.3
��� m�dula espinal	013.5
�� escrofuloso	017.2	
�� escroto	016.5	
�� espina dorsal o v�rtebra (columna)	015.0[730.88]	
�� estrumoso	017.2	
�� ganglio (linf�tico) 		"v�ase Tuberculosis, ganglio linf�tico "
�� genitourinario NCOC	016.9	
�� hueso	015.9[730.8]	"v�ase adem�s Osteomielitis, debida a, tuberculosis"
��� cadera	015.1[730.85]	
��� espinal	015.0[730.88]	
��� rodilla	015.2[730.86]	
��� sacro	015.0[730.88]	
��� sitio especificado NCOC	015.7[730.88]	
��� v�rtebra	015.0[730.88]	
�� iliopsoas	015.0[730.88]	
�� intestino	014.8	
�� isquiorrectal	014.8	
�� lumbar	015.0[730.88]
�� mama	017.9
�� m�dula espinal	013.5
�� meninges (cerebrales) (espinales)	013.0
�� muslo	017.9
�� �rganos genitales NCOC	016.9
��� femeninos	016.7
��� masculinos	016.5
�� peIviano	016.9
��� femenino	016.7
��� masculino	016.5
�� perianal	014.8
��� f�stula	014.8
�� perinefr�tico	016.0[590.81]
�� perineo	017.9
�� perirrectal	014.8
�� piel	017.0	
��� primario	017.0	
�� psoas	015.0[730.88]	
�� pulm�n	011.2	
��� primario progresivo	010.8	
�� recto	014.8	
�� retrofar�ngeo	012.8	
�� rin�n	016.0[590.81]	
�� rodilla	015.2	
�� sacro	015.0[730.88]	
�� seno frontal 		"v�ase Tuberculosis, seno "
�� subdiafragm�tico	014.8	
�� test�culo	016.5	
�� urinario	016.3	
��� rin�n	016.01590.81]	
�� �tero	016.7	
"� accesorio, seno "		"v�ase Tuberculosis, seno "
"� Addison, enfermedad de"	017.6	
� adenitis	017.2	"v�ase adem�s Tuberculosis, ganglio linf�tico"
� adenoides	012.8	
� adenopat�a	017.2	"v�ase adem�s Tuberculosis, ganglio linf�tico"
�� traqueobronquial	012.1	
��� primaria progresiva	010.8	
� adherencias del pericardio	017.9[420.0]	
� adrenal (c�psula) (suprarrenal)	017.6	
"� afiladores, de los"	011.4	
� alfareros	011.4	
� am�gdala (lingual)	012.8	
� amigdalitis	012.8	
� anemia	017.9	
� anexos (uterinos)	016.7	
� ano	014.8	
� antigua NCOC	137.0	
�� sin residuos	V12.0	
� aparato o gl�ndula lagrimal	017.3	
� ap�ndice	014.8	
� apendicitis	014.8	
� apical	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� �pice	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� aracnoidea	013.0	
� arteria	017.9	
� articulaci�n	015.9	
�� cadera	015.1	
�� rodilla	015.2	
�� sitio especificado NCOC	015.8	
�� vertebral	015.0[730.88]	
� articular 		"v�ase Tuberculosis, articulaci�n "
� artritis (cr�nica) (sinovial)	015.9[711.40]	
�� cadera	015.1[711.45]	
�� espina dorsal o v�rtebra (columna)	015.0[720.81]	
� mu�eca	015.8[730.83]	
� rodilla	015.2[711.46]	
�� sitio especificado NCOC	015.8[711.48]	
�� tobillo	015.8[730.87]	
� ascitis	014.0	
� asma	011.9	"v�ase adem�s Tuberculosis, pulmonar"
"� axila, axilar"	017.2	
�� ganglios	017.2	
� bazo	017.7	
� bilateral	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� boca	017.9	
� bolsa sinovial	015.9	"v�ase adem�s Tuberculosis, articulaci�n"
�� subdeltoidea	017.9	
"� bronconeumon�a, bronconeum�nica"	011.6	
� broncopleural	012.0	
� broncorragia	011.3	
� broncotraqueal	011.3	
�� aislada	012.2	
� bronquiectasia	011.5	
� bronquio 		"v�ase Tuberculosis, bronquio "
"� bronquio(s), bronquial"	011.3	
�� aislada	012.2	
�� ectasia	011.5	
�� f�stula	011.3	
��� primaria progresiva	010.8	
�� ganglio o n�dulo linf�tico	012.1	
��� primaria progresiva	010.8	
�� gl�ndula	012.1	
"��� primaria, progresiva"	010.8	
� bronquitis	011.3	
"� bulbouretral, gl�ndula"	016.5	
� cadera (enfermedad) (articulaci�n)	015.1	
�� hueso	015.1[730.85]	
� canteros	011.4	
� caquexia NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
"� carboneros, de los"	011.4	
� cardiomiopat�a	017.9[425.9]	
� caries	015.9[730.8]	"v�ase adem�s Tuberculosis, hueso"
� cart�lago	015.9[730.8]	"v�ase adem�s Tuberculosis, hueso"
�� intervertebral	015.0[730.88]	
� catarral	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� cavidad bucal	017.9	
� celulitis (primaria)	017.0	
� cerebelo (actual)	013.2	
� cerebral (actual)	013.2	
�� meninges	013.0	
� cerebro	013.2
� cerebrospinal	013.6
�� meninges	013.0
� cervical	017.2
�� ganglio	017.2
�� n�dulos linf�ticos	017.2
� cervicitis uterina	016.7
� ciego	014.8
� cistitis	016.1
� codo	015.8
� colicuativa (primaria)	017.0
� colitis	014.8
� colon	014.8
�� ulceraci�n	014.8
� columna vertebral	015.0[730.88]
"� compleja, primaria"	010.0
� conducto deferente	016.5
� cong�nita	771.2
� conjuntiva	017.3[370.31]
� contacto con	V01.1
� convertidora (prueba de tuberculina) (sin enfermedad)	795.5
� coraz�n	017.9[425.8]
� cord�n esperm�tico	016.5
� c�rnea (ulcera)	017.3[370.31]
� coroides	017.3[363.13]
� coroiditis	017.3[363.13]
"� Cowper, gl�ndula de"	016.5
� coxal	015.1[730.85]
� coxalgia	015.1[730.85]
"� cuando complica el embarazo, parto, o puerperio"	647.3
�� que afecta al feto o al reci�n nacido	760.2
� cuello uterino	016.7
"� cuello, ganglios"	017.2	
� cuerpo ciliar	017.3[364.11]	
� curvatura de la espina	015.0[737.40]	
� cutis (colicuativa)	017.0	
� dacriocistitis	017.3[375.32]	
� dactilitis	015.5	
� dedo de 		
�� mano	017.9	
�� pie	017.9	
� diarrea	014.8	
� difusa	018.9	"v�ase adem�s Tuberculosis, miliar"
�� meninges	013.0	
�� pulm�n 		"v�ase Tuberculosis, pulmonar "
� diseminada	018.9	"v�ase adem�s Tuberculosis, miliar"
�� meninges	013.0	
� disenter�a	014.8	
� duodeno	014.8	
� duramadre	013.9	
�� absceso	013.8	
��� cerebral	013.3	
��� espinal	013.5	
� empiema	012.0	
� encefalitis	013.6	
� enc�a	017.9	
� endarteritis	017.9	
� endocardio (cualquier v�lvula)	017.9[424.91]	
� endocarditis (cualquier v�lvula)	017.9[424.91]	
"� endocrina, gl�ndula NCOC"	017.9	
� endometrio	016.7	
� enfermedad bronceada (de Addison)	017.6	
� enfisema 		"v�ase Tuberculosis, pulmonar "
� ent�rica	014.8	
� enteritis	014.8
� enterocolitis	014.8
� epididimitis	016.4
� epid�dimo	016.4
"� epidural, absceso"	013.8
�� cerebro	013.3
�� m�dula espinal	013.5
� epiglotis	012.3
� episcleritis	017.3[379.00]
� eritema (indurado) (nudoso) (primario)	017.1
� escler�tica	017.3[379.09]
� escoliosis	015.0[737.43]
� escrofulosa	017.2
� escroto	016.5
� es�fago	017.8
� espina dorsal	015.0[730.88]
� espina dorsal il�aca (superior)	015.0[730.88]	
� espinal(es) 		
�� enfermedad	015.0[730.88]	
�� m�dula	013.4	
�� membrana	013.0	
�� meninges	013.0	
� esplenitis	017.7	
� espondilitis	015.0[720.81]	
� esquel�tica NCOC	015.9[730.8]	"v�ase adem�s Osteomielitis, debida a tuberculosis"
"� esternoclavicular, articulaci�n"	015.8	
� est�mago	017.9	
� estruma	017.2	
� exposici�n a	V01.1	
� exudativa	012.0	
"�� primaria, progresiva"	010.1	
� faringe	012.8	
� faringitis	012.8	
� fascia	017.9	
� fase de reinfecci�n	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� fauces	012.8	
� f�stula 		
�� gastroc�lica	014.8	
�� perirrectal	014.8	
� flictenulosis (conjuntiva)	017.3[370.31]	
� Florida	011.6	
� fondo de saco de Douglas	014.8	
� galopante	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� ganglio(s) o n�dulo(s) linf�tico(s) (perif�rico(s))	017.2	
�� abdomen	014.8	
�� bronquiales	012.1	
"��� primaria, progresiva"	010.8	
�� cervicales	017.2	
�� generalizada	017.2
�� hiliares	012.1
"��� primaria, progresiva"	010.8
�� infraclaviculares	017.2
�� inguinales	017.2
�� intrator�cicos	012.1
"��� primaria, progresiva"	010.8
�� mediast�nico	012.1
"���� primaria, progresiva"	010.8
�� mesent�ricos	014.8
�� perif�ricos	017.2
�� peritoneales	014.8
�� retroperitoneales	014.8
�� supraclaviculares	017.2
�� traqueales	012.1
"��� primaria, progresiva"	010.8
�� traqueobronquiales	012.1	
"��� primaria, progresiva"	010.8	
� gangli�nica	015..9	
� garganta	012.8	
� gastritis	017.9	
� gastroenteritis	014.8	
� general de los ganglios linf�ticos	017.2	
"� general, generalizada"	018.9	
�� aguda	018.0	
�� cr�nica	018.8	
� generalizada 		"v�ase Tuberculosis, miliar "
� genitourinario NCOC	016.9	
� gl�ndula(s) 		
�� adrenales (c�psula)	017.6	
�� bulbouretral	016.5	
"�� Cowper, de"	016.5	
�� endocrinas NCOC	017.9	
�� lagrimal	017.3	
�� mamaria	017.9	
�� mediast�nica	012.1	
"��� primaria, progresiva"	010.8	
�� paratiroides	017.9	
� par�tida	017.9	
�� pituitaria	017.9	
�� salival	017.9	
�� suprarrenal	017.6	
�� t�mica	017.9	
�� tiroides	017.5	
� glotis	012.3	
� hemat�gena 		"v�ase Tuberculosis, miliar "
� hemoptisis	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� hemorragia NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� hemot�rax	012.0	
� hepatitis	017.9	
� hidroc�falo	013.8	
� hidroneumot�rax	012.0	
� hidrot�rax	012.0	
� h�gado	017.9	
� hipoadrenalismo	017.6	
� hipofaringe	012.8	
� hombro	015.8	
�� om�plato	015.7[730.8]	
� hueso	015.9[730.8]	"v�ase adem�s Osteomielitis, debida a, tuberculosis"
�� cadera	015.1[730.85]	
�� columna dorsal o vertebral	015.0[730.88]	
�� miembro NCOC	015.5[730.88]	
�� rodilla	015.2[730.86]	
�� sacro	015.0[730.88]	
�� sitio especificado NCOC	015.7[730.88]	
� ileocecal (hiperpl�sica)	014.8	
� ileocolitis	014.8	
� �leon	014.8	
� incipiente NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� indurativa (primaria)	017.1	
� infantil	010.0	
� infecci�n NCOC	011.9	
�� sin manifestaci�n cl�nica	010.0	
"� infraclavicular, ganglios linf�ticos"	017.2	
� ingle	017.2	
� inguinal	017.2	
�� ganglios linf�ticos	017.2	
� intestino (cualquier parte)	014.8	
�� miliar	018.8	
� intestino delgado	014.8	
� iris	017.3[364.11]	
� iritis	017.3[364.11]	
� isquiorrectal	014.8	
� labio (boca)	017.9	
� Iaringe	012.3	
� Iaringitis	012.3	
� lengua	017.9	
"� leptomeninges, leptomeningitis (cerebral) (espinal)"	013.0	
� ligamento ancho	016.7	
� limpiadores con chorro de arena	011.4	
� linfadenitis 		"v�ase Tuberculosis, ganglio linf�tico "
� linfangitis 		"v�ase Tuberculosis, ganglio linf�tico "
� linf�tica (ganglio) (vaso) 		"v�ase Tuberculosis, ganglio linf�tico "
� lingual	017.9	
� liquenoide (primaria)	017.0	
� lordosis	015.0[737.42]	
� lupus	017.0	
�� p�rpado	017.0[373.4]	
� maligna NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� mama	017.9	
"� mamaria, gl�ndula"	017.9	
� mano	017.9	
� marasmo NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� mastoiditis	015.6	
"� materna, que afecta al feto o al reci�n nacido"	760.2	
� maxilar	015.7[730.88]	
"� mediast�nica, ganglio o n�dulo linf�tico"	012.1	
"�� primaria, progresiva"	010.8	
� mediastinitis	012.8	
"�� primaria, progresiva"	010.8	
� mediastinopericarditis	017.9[420.0]	
� mediatino	012.8	
"�� primaria, progresiva"	010.8
� m�dula	013.9
�� cerebro	013.2
�� espinal	013.4
� melanosis addisoniana	017.6
"� membrana, cerebro"	013.0
� meninges (cerebrales) (espinales)	013.0
� meningitis (basilar) (cerebro) (cerebral) (cerebroespinal) (espinal)	013.0
� meningoencefalitis	013.0
"� mesenterio, mesent�rica"	014.8
�� ganglio o n�dulo linf�tico	014.8
� mielitis	013.6
� miliar (cualquier sitio)	018.9
�� aguda	018.0
�� cr�nica	018.8
�� tipo especificado NCOC	018.8
� mineros	011.4
� miocardio	017.9[422.0]
� miocarditis	017.9[422.0]
� moldeadores	011.4
� molineros	011.4
� m�ltiple	018.9
�� aguda	018.0
�� cr�nica	018.8
� m�sculo	017.9
� mu�eca (articulaci�n)	015.8
�� hueso	015.5[730.83]
� nariz (tabique)	012.8
� nasal (conducto) (senos)	012.8
� nasofaringe	012.8
� nefritis	016.0[583.81]
� nervio	017.9
"� neumon�a, neum�nica"	011.6	
� neumot�rax	011.7	
�� espont�neo 		"v�ase Tuberculosis, pulmonar "
� neumot�rax espont�neo 		"v�ase Tuberculosis, pulmonar "
� n�dulos linf�ticos hilares	012.1	
"�� primaria, progresiva"	010.8	
� ocular	017.3	
� o�do (interno) (medio)	017.4	
�� externo (primaria)	017.0	
�� hueso	015..6	
�� piel (primaria)	017.0	
� ojo	017.3	
�� glaucoma	017.3[365.62]	
� omento	014.8	
� om�plato	015.7[730.8]	
� ooforitis (aguda) (cr�nica)	016.6	
� �ptica	017.3[377.39]
�� papila(s)	017.3[377.39]
�� tronco nervioso	017.3[377.39]
� �rbita	017.3
� �rgano o tracto urinario	016.3
�� rin�n	016.0
� �rgano pelviano NCOC	016.9
�� femenino	016.7
�� masculino	016.5
� �rgano reproductivo	016.7
"� �rgano, especificado NCOC"	017.9
� �rganos genitales NCOC	016.9
�� femeninos	016.7
�� masculinos	016.5
� orificios (primaria)	017.0
� orquitis	016.5[608.81]
� �sea	015.9[730.8]	"v�ase adem�s Tuberculosis, hueso"
� oste�tis	015.9[730.8]	"v�ase adem�s Tuberculosis, hueso"
� osteomielitis	015.9[730.8]	"v�ase adem�s Tuberculosis, hueso"
� otitis (media)	017.4	
� ovario (aguda) (cr�nica)	016.6	
� ovaritis (aguda) (cr�nica)	016.6	
� oviductos (aguda) (cr�nica)	016.6	
� paladar (blando)	017.9	
� paladar blando	017.9	
� p�ncreas	017.9	
� papulonecr�tica (primaria)	017.0	
� paquimeningitis	013.0	
� paratiroides (gl�ndula)	017.9	
� paroniquia (primaria)	017.0	
"� par�tida, gl�ndula o regi�n"	017.9	
� p�rpado (primaria)	017.0	
�� lupus	017.0[373.4]	
� pelvis (�sea)	015.7[730.85]	
�� en embudo	137.3	
� pene	016.5	
� peribronquitis	011.3	
� pericardio	017.9[420.0]	
� pericarditis	017.9[420.0]	
"� pericondritis, Iaringe"	012.3	
� periflebitis	017.9	
�� retiniana	017.3[362.18]	
�� vaso ocular	017.3[362.18]	
� perineo	017.9	
� periostitis	015.9[730.8]	"v�ase adem�s Tuberculosis, hueso"
"� perirrectal, f�stula"	014.8	
"� peritoneal, ganglios linf�ticos"	014.8	
� peritoneo	014.0	
� peritonitis	014.0	
� perniciosa NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� pie	017.9	
� piel (cualquier sitio) (primaria)	017.0	
� pielitis	016.0[590.81]	
� pielonefritis	016.0[590.81]	
� piemia 		"v�ase Tuberculosis, miliar "
� pionefrosis	016.0	
� pioneumot�rax	012.0	
� piot�rax	012.0	
"� pituitaria, gl�ndula"	017.9	
� placenta	016.7	
"� pleura, pleural, pleuresia, pleuritis (con derrame) (fibrinosa) (obliterativa) (pl�stica simple) (purulenta)"	012.0	
"�� primaria, progresiva"	010.1	
� poliserositis	018.9	
�� aguda	018.0	
�� cr�nica	018.8	
� prepucio	016.5	
� primaria	010.9	
� compleja	010.0	
� complicada	010.8	
��� con pleuresia o derrame	010.1	
�� piel	017.0	
�� progresiva	010.8	
��� con pleures�a o derrame	010.1	
� primera infecci�n	010.0	
� proctitis	014.8	
� pr�stata	016.5[601.4]	
� prostatitis	016.5[601.4]	
� ptisis NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� pulm�n 		"v�ase Tuberculosis, pulmonar "
� pulmonar (arteria) (fase de reinfecci�n) (focos redondos m�ltiples) (incipiente) (maligna) (perniciosa)	011.9	
�� bacilos crom�genos acidorresistentes	795.3
�� cavitaria o con caverna	011.2
"��� primaria, progresiva"	010.8
�� especificada NCOC	011.8
�� esputos positivos s�lo	795.3
�� estado consecutivo a colapso quir�rgico del pulm�n NCOC	011.9
�� fibrosis o fibr�tica	011.4 
�� infiltrante	011.0
"��� primaria, progresiva"	010.9
�� nodular	011.1
�� tipo infancia o primera infecci�n	010.0
� queratitis	017.3[370.31]
�� intersticial	017.3[370.59]
� queratoconjuntivitis	017.3[370.31]
� quifoscoliosis	015.0[737.43]
� quifosis	015.0[737.41]
� quiste (ovario)	016.6	
� recto (con absceso)	014.8	
�� f�stula	014.8	
� renal	016.0	
� respiratoria NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
�� sitio especificado NCOC	012.8	
� retina	017.3[363.13]	
"� retrofar�ngea, absceso"	012.8	
� retroperitoneal (ganglio o n�dulo linf�tico)	014.8	
�� ganglio	014.8	
� reumatismo	015.9	
� rinitis	012.8	
� ri��n	016.0	
� rodilla (articulaci�n) (genu)	015.2	
� rodilla (articulaci�n)	015.2	
� sacro	015.0[730.88]	
� sacroil�aca (articulaci�n)	015.8	
"� salival, gl�ndula"	017.9	
� salpingitis (aguda) (cr�nica)	016.6	
� senil NCOC	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� seno (accesorio) (nasal)	012.8	
�� epid�dimo	016.4	
�� hueso	015.7[730.88]	
� s�ptica NCOC	018.9	"v�ase adem�s Tuberculosis, miliar"
� sigmoide	014.8	
� s�nfisis del pubis	015.7[730.88]	
� sinovitis	015.9[727.01]	
�� cadera	015.1[727.01]	
�� espina dorsal o v�rtebra	015.0[727.01]	
�� rodilla	015.2[727.01]	
�� sitio especificado NCOC	015.8[727.01]	
� sistema nervioso central	013.9	
�� sitio especificado NCOC	013.8	
� sist�mica 		"v�ase Tuberculosis, miliar "
� sitio no especificado 		"v�ase Tuberculosis, pulmonar "
� subcutis (primaria)	017.0	
"� subdeltoidea, bolsa sinovial"	017.9	
� submaxilar	017.9	
�� regi�n	017.9	
"� supraclavicular, ganglios linf�ticos"	017.2	
� suprarrenal (c�psula) (gl�ndula)	017.6	
"� suprarrenales, gl�ndulas"	017.6	
� talladores de piedras de molino	011.4	
� tarsitis (p�rpado)	017.0[373.4]	
�� tobillo (hueso)	015.5[730.87]	
� tejido celular (primaria)	017.0	
� tejido conjuntivo	017.9	
�� hueso 		"v�ase Tuberculosis, hueso "
� tejido subcut�neo (celular) (primaria)	017.0	
� tend�n (vaina) 		"v�ase Tuberculosis, tenosinovitis "
� tenosinovitis	015.9[727.01]	
�� cadera	015.1[727.01]	
�� espina dorsal o v�rtebra	015.0[727.01]	
�� rodilla	015.2[727.01]	
�� sitio especificado NCOC	015.8[727.01]	
� test�culo	016.5[608.81]	
� tiflitis	014.8	
"� t�mica, gl�ndula del"	017.9	
� tipo infancia o primera infecci�n	010.0	
"� tiroides, gl�ndula"	017.5	
� tobillo (articulaci�n)	015.8	
�� hueso	015.5[730.87]	
� t�rax	011.9	"v�ase adem�s Tuberculosis, pulmonar"
� tracto digestivo	014.8	
� tracto gastrointestinal	014.8
� tracto o ves�cula seminal	016.5[608.81]
� tracto uveal	017.3[363.13]
"� tr�quea, traqueal"	012.8
�� aislada	012.2
�� ganglios linf�ticos	012.1
"��� primaria, progresiva"	010.8
� traqueobronquial	011.3
�� aislada	012.2
�� ganglios linf�ticos	012.1
"��� primaria, progresiva"	010.8
�� n�dulo o ganglio linf�tico	012.1
"��� primaria, progresiva"	010.8
� trompa de 	
�� Eustaquio	017.4
�� Falopio	016.6
� tub�rica	016.6	
� tubo digestivo	014.8	
"� tumefacci�n, articulaci�n"	015.9	"v�ase adem�s Tuberculosis, articulaci�n"
� t�nica vaginal	016.5	
� �lcera (primaria) (piel)	017.0	
�� intestino	014.8	
�� sitio especificado NCOC 		"v�ase Tuberculosis, por sitio "
� ur�ter	016.2	
"� uretra, uretral"	016.3	
� �tero	016.7	
� �vula	017.9	
� vacunaci�n profil�ctica (contra)	V03.2	
� vagina	016.7	
� vejiga	016.1	
� vena	017.9	
� verruga (primaria)	017.0	
� verrugosa (cutis) (primaria)	017.0	
� v�rtebra (columna)	015.0[730.88]	
� ves�cula biliar	017.9	
� ves�cula o tracto seminal	016.5[608.81]	
� vesiculitis	016.5[608.81]	
� v�as respiratorias NCOC	012.8	
� v�scera NCOC	014.8	
� vulva	016.7[616.51]	
� yeyuno	014.8	
Tuberculum 		
� Tub�rculo 		
�� oclusal	520.2	
�� pabell�n de la oreja (auricular)	744.29	
�� paramolar	520.2	
"Tuberosa, esclerosis (cerebro)"	759.5	
"Tubo, tub�rico, tubular "		v�ase adem�s enfermedad espec�fica 
"� ligadura (trompas), admisi�n para"	V25.2	
Tuboov�rico 		v�ase enfermedad espec�fica
"Tuboplastia, despu�s de esterilizaci�n previa"	V26.0	
Tubotimpanitis	381.10	
"Tubular, visi�n"	368.45	
Tularemia	021.9	
� con 		
�� conjuntivitis	021.3	
�� neumon�a	021.2	
� bronconeum�nica	021.2	
� conjuntivitis	021.3	
� cript�gena	021.1	
� diseminada	021.8	
� ent�rica	021.1	
� especificada NCOC	021.8	
� generalizada	021.8	
� glandular	021.8	
� intestinal	021.1	
� neumon�a	021.2	
� oculoglandular	021.3	
� oft�lmica	021.3	
� pulmonar	021.2	
� tifoidea	021.1	
� ulceroglandular	021.0	
� vacunaci�n profil�ctica (contra)	V03.4	
"Tularensis, conjuntivitis"	021.3 	
Tumefacci�n		
� abdominal (no referible a ning�n �rgano en particular)	789.3	
� ano	787.9	
� articulaci�n 	719.0	"v�ase adem�s Efusi�n, articulaci�n"
�� tuberculosa 		"v�ase Tuberculosis, articulaci�n "
� blanca 		"v�ase Tuberculosis, artritis "
� boca	784.2	
� brazo	729.81	
� cabeza	784.2	
� Calabar	125.2	
� cuello	784.2	
� dedo de 		
�� mano	729.81	
�� pie	729.81	
� enc�a	784.2	
� epig�strica	789.3	
� escroto	608.86	
� espl�nica	789.2	v�ase adem�s Esplenomegalia
� extremidad (inferior) (superior)	729.81	
� ganglios linf�ticos	785.6	
�� cervicales	785.6	
� garganta	784.2	
"� gl�ndula suprarrenal, embotada"	255.8	
� h�gado	573.8	
� inflamatoria 		v�ase Inflamaci�n 
� lengua	784.2	
� mama	611.72	
� mano	729.81	
� mediast�nica	786.6	
� mejilla	784.2	
� miembro	729.81	
"� migratoria, debida a Gnathostoma (spinigerum)"	128.1	
� m�sculo (miembro)	729.81	
� nariz o seno	784.2	
� n�dulos linf�ticos	785.6	
� o�do	388.8	
� ojo	379.92 	
� ombligo	789.3	
� �rgano genital femenino	625.8
� paladar	784.2
� pelvis	789.3
� pene	607.83
� perineo	625.8
� pie	729.81
� piel	782.2
� pierna	729.81
� pulm�n	786.6
� recto	787.9
"� rin�n, embotado"	593.89
� substernal	786.6
� superficial localizada (piel)	782.2
� test�culo	608.86
� tobillo	719.07
� t�rax	786.6
