#include "cie9.h"

static char *tknz[]= 
	{ " �\t\r\n(){}[],;:+-*\x22",
	  "\t",
	  " �\t\r\n(){}[],;:+-*\x22\t"
	};

static char *translaciones[]={ 
	"���������������\xa0\x82\xa1\xa2\xa3", 
	"aeiouAEIOUaeiouaeiou"  };



char * elmfgets(char *s, size_t sz, FILE *fp){
	char *k=fgets(s,sz,fp);
	if( s[strlen(s)-1]=='\n' )
		s[strlen(s)-1]=0;
	return k;
}	

void Warn(long nw, long lp){
	switch(nw){
		case 1:
			printf("Atenci�n: Error de transcripci�n en archivo, l�nea %ld\n",lp);
			break;
	}
}

long astrlen( char *in ){
	long l=0,ll=0;
	if( !in )
		return 0;
	while( 0!=(ll=strlen(in)) ){
		in+=1+ll;
		l +=1+ll;
	}
	return l;
}

long anumfld( char *in ){
	long l;
	if( !in )
		return 0;
	for(l=0; *in; in+=1+strlen(in) )
		l++;
	return l;
}

long Level( char *s ){
	long l=0;
	while( *s++=='�' )
		l++;
	return l;
}

int isRangeRef(char *s, char **l1, char **rp1, char **rs1,char **l2, char **rp2, char **rs2 ){
	char *aux,*c1,*c2;
	if( aux=strchr(s,'-') )
		return 0;
	*aux=0;
	c1=isRef(s,l1,rp1,rs1);
	c2=isRef(aux+1,l2,rp2,rs2);
	*aux='-';
	return c1 && c2;
}

char *isRef(char *s, char **Letra, char **RPrin, char **RSec){
	if( !s || !*s )					
		return (char*)NULL;
	if(Letra) 
		*Letra=(char*)NULL;
	if( strchr("MEV",*s) ){
		if(Letra)
			*Letra=s;
		s++;
	}
	if(RPrin)
		*RPrin= *s=='-' ? 0 : s;
	if(RSec)
		*RSec=(char*)NULL;
	if(!isdigit(*s) && *s!='-')	
		return (char*)NULL;
	while(isdigit(*s)||*s=='-') 
		s++;
	if( !*s )					
		return s;
	if(RSec)
		*RSec=++s;
	return s;
}

char *Tokenize(char *Buf, int mode){
	char *s;
	s = strtok(Buf,tknz[mode]);
	if(s && strchr(s,'.') && !isdigit(*(strchr(s,'.')-1)) )
		*strchr(s,'.')=' ';
	return s;
}

char *Translate(char *in){
	char *s=translaciones[0],*t=in;
		
	while(*t){
		switch(*t){
		case -92:
			*t='�';
			break;
		case -91:
			*t='�';
			break;
		}
		t++;
	}
	while(*s){
		while( (t=strchr(in,*s)) )
			*t=translaciones[1][s-translaciones[0]];
		s++;
	}
	if( in[strlen(in)-1]=='\n' )
		in[strlen(in)-1]=0;
	return in;
}

void *elmmalloc( size_t size ){
	void *p;
	if((p=malloc(size))==NULL){
		puts("No hay memoria. Terminando.");
		exit(-1);
	}
	memset(p,0,size);
	return p;
}

void* elmrealloc( void *p, size_t size){
	void *q;
	if((q=realloc(p,size))==NULL){
		puts("No hay memoria. Terminando.");
		exit(-1);
	}
	return q;
}
	
void elmfree( void *p ){
	free(p);
}

