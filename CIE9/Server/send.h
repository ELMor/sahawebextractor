
#ifndef __SEND_H__
#define __SEND_H__

#include "search.h"
#include "strbin.h"

void  SendAll          (char **sou, int flagpal, int flagref);
void  SendAllPal       (char **sou, int flags);
void  SendAllRef       (char **sou, int flags);
void  SendFrase        (char **sou, char *Padre, long NumFra, char *Texto, Match *MRef,long ns);
void  SendRef          (char **sou, char *Padre, long nref, int flags);
void  SendPal          (char **sou, char *Padre, long npal, int flags);
void  SendRootPal      (char **sou, char *Padre, char *Root);
void  WriteBinary      (char **sou, char *Padre, char *Ref, char *Value);

#endif

