#include "cie9.h"
#include "search.h"

#define  Global extern
#define  GlobalIni
#include "globals.h"

int cmpPal( const void *p1, const void *p2 ){
	return stricmp( (*((Pal**)p1))->Palabra,(*((Pal**)p2))->Palabra);
}

int cmpRef( const void *p1, const void *p2 ){
	Ref **r1=(Ref**)p1,**r2=(Ref**)p2;
	return stricmp( (*((Ref**)p1))->Referencia,(*((Ref**)p2))->Referencia);
}

long BusPal(char *p){
	Pal *pel,key,**pelp;
	long pos;

	key.Palabra=p;
	pel=&key;
	if( (pelp=(Pal**)bsearch(&pel,IndDic_Pal,TamDic-TamDicSO,sizeof(Pal*),cmpPal))!=NULL )
		return *pelp-&Dic[0];
	for(pos=TamDic-TamDicSO;pos<TamDic;pos++)
		if( !stricmp(IndDic_Pal[pos]->Palabra,p) )
			return IndDic_Pal[pos]-&Dic[0];
	return -1;
}

long BusRef(char *p){
	Ref *pel,key,**pelp;
	long pos;

	key.Referencia=p;
	pel=&key;
	if( (pelp=(Ref**)bsearch(&pel,IndCie_Ref,TamCie-TamCieSO,sizeof(Ref*),cmpRef))!=NULL )
		return *pelp-Cie;
	for(pos=TamCie-TamCieSO;pos<TamCie;pos++)
		if( !stricmp(IndCie_Ref[pos]->Referencia,p) )
			return IndCie_Ref[pos]-&Cie[0];
	return -1;
}

void IndexAll(void){
	qsort(IndDic_Pal,TamDic,sizeof(Pal*),cmpPal);
	TamDicSO=0;
	qsort(IndCie_Ref,TamCie,sizeof(Ref*),cmpRef);
	TamCieSO=0;
}

int cmpPalCom( const void *p1, const void *p2){
	return stricmp(*(char**)p1,*(char**)p2);
}
void IndexPalCom( void ){
	qsort( PalCom,NumPCo,sizeof(char*),cmpPalCom);
}

void *BusPalCom( char *s ){
	return bsearch(&s,PalCom,NumPCo,sizeof(char*),cmpPalCom);
}