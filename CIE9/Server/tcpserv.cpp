
extern "C" { 
	#include "tcp4u.h" 
}

#include "cie9.h"
#include "search.h"
#include "tcpserv.h"

#define  Global
#define  GlobalIni =0
#include "globals.h"

void GetIndexName(char *exe, char *fn){
	if( !strrchr(exe,'/') )
		strcpy(fn,FINDCIE);
	else
		strcpy(strrchr(strcpy(fn,exe),'/')+1,FINDCIE);
}
int main(int argc, char *argv[]){
	SOCKET LSock, CSock;
	unsigned short usPort=7100;
	int Rc;

	//Indice CIE9
  printf("Cargando indice...");
	GetIndexName(argv[0],findciename);
	LoadData(findciename);
	printf("%s cargado.\n",findciename);

	//Inicialización sockets
  printf("Inicializando sockets...");
	Tcp4uInit ();
	puts("Inicializado.\nEscuchando!!");
	//Apertura de un socket para escuchar.
	if((Rc=TcpGetListenSocket (&LSock, (char*)NULL, &usPort, 1))!=TCP4U_SUCCESS){
		puts(Tcp4uErrorString(Rc));
		return -1;
	}
#ifdef UNIX		//Desconexión de la terminal de control.
	if( fork()!=0 ) exit(0);
	close(0); close(1); close(2);
	setsid();
#endif
	while(1){
		//Aceptar conexiones
		Rc = TcpAccept (&CSock, LSock, 0);

#ifdef UNIX	// Podemos utilizar más de un proceso
		(*signal)(SIGCHLD,SIG_IGN);
		(*signal)(SIGHUP,ReloadData);
		if( fork() )
			TcpClose(&CSock);
		else
			ThreadProc( &CSock );
#else		// Win32 no tiene fork. Hay que crear otra hebra de ejecución.
		unsigned long ThreadID;
		SOCKET *vp=(SOCKET*)malloc(sizeof(SOCKET));
		*vp=CSock;
		ThreadID=_beginthread(ThreadProc,0,vp); //ThreadProc debe liberar vp
#endif
	}
	Tcp4uCleanup ();
	return 0;
}
