/*
 * a - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.utils.img;

import java.io.IOException;

public class Huffman {
	byte[] _fldelse = new byte[17];

	int _fldcase;

	int[] _fldint = new int[257];

	byte[] _fldfor = new byte[257];

	byte[] a = new byte[256];

	int _flddo;

	int[] _fldbyte = new int[18];

	int[] _fldgoto = new int[17];

	int[] _fldif = new int[256];

	int[] _fldtry = new int[17];

	int[] _fldchar = new int[256];

	static int[] _fldnew = { -1, 2147483647, 1073741823, 536870911, 268435455,
			134217727, 67108863, 33554431, 16777215, 8388607, 4194303, 2097151,
			1048575, 524287, 262143, 131071, 65535, 32767, 16383, 8191, 4095,
			2047, 1023, 511, 255, 127, 63, 31, 15, 7, 3, 1 };

	public int a(byte[] is, int i) throws IOException {
		boolean b1 = false;
		boolean b2 = false;
		boolean b3 = false;
		_flddo = is[i++] << 8;
		_flddo += is[i++];
		_flddo = _flddo - 2;
		while_0_: do {
			int aux;
			do {
				if (_flddo <= 0)
					break while_0_;
				aux = is[i++];
				_fldelse[0] = (byte) 0;
				int i_3_ = 0;
				for (int j = 1; j <= 16; j++) {
					_fldelse[j] = is[i++];
					i_3_ += _fldelse[j];
				}
				if (i_3_ > 256)
					throw new RuntimeException("Bogus DHT counts");
				for (int j = 0; j < i_3_; j++)
					a[j] = is[i++];
				_flddo -= 17 + i_3_;
				if ((aux & 0x10) == 16)
					throw new RuntimeException(
							"Huffman table for lossless JPEG is not defined");
				_fldcase = aux;
			} while (aux >= 0 && aux < 4);
			throw new RuntimeException("Bogus DHT index " + aux);
		} while (false);
		int i_6_ = 0;
		boolean bool_7_ = true;
		boolean bool_8_ = true;
		for (int i_9_ = 1; i_9_ <= 16; i_9_++) {
			for (int i_10_ = 1; i_10_ <= _fldelse[i_9_]; i_10_++)
				_fldfor[i_6_++] = (byte) i_9_;
		}
		_fldfor[i_6_] = (byte) 0;
		int i_11_ = i_6_;
		int i_12_ = 0;
		int i_13_ = _fldfor[0];
		i_6_ = 0;
		while (_fldfor[i_6_] != 0) {
			while (_fldfor[i_6_] == i_13_) {
				_fldint[i_6_++] = i_12_;
				i_12_++;
			}
			i_12_ <<= 1;
			i_13_++;
		}
		int i_14_ = 0;
		for (int i_15_ = 1; i_15_ <= 16; i_15_++) {
			if (_fldelse[i_15_] != 0) {
				_fldtry[i_15_] = i_14_;
				_fldgoto[i_15_] = _fldint[i_14_];
				i_14_ += _fldelse[i_15_];
				_fldbyte[i_15_] = _fldint[i_14_ - 1];
			} else
				_fldbyte[i_15_] = -1;
		}
		for (i_6_ = 0; i_6_ < i_11_; i_6_++) {
			int i_16_ = _fldfor[i_6_];
			if (i_16_ <= 8) {
				int i_17_ = a[i_6_];
				i_12_ = _fldint[i_6_];
				int i_18_ = i_12_ << 8 - i_16_;
				int i_19_;
				if (i_16_ < 8)
					i_19_ = i_18_ | _fldnew[24 + i_16_];
				else
					i_19_ = i_18_;
				for (int i_20_ = i_18_; i_20_ <= i_19_; i_20_++) {
					_fldif[i_20_] = i_16_;
					_fldchar[i_20_] = i_17_;
				}
			}
		}
		return i;
	}
}