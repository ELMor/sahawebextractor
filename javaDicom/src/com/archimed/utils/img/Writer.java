/*
 * g - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.utils.img;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Writer {
	private int a = 0;

	private int _fldlong = 0;

	private ByteArrayOutputStream baos;

	private static final byte[] chk1 = { 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1 };

	private static int[] chk2 = { 0, 1, 3, 7, 15, 31, 63, 127, 255, 511,
			1023, 2047, 4095, 8191, 16383, 32767, 65535, 131071, 262143,
			524287, 1048575, 2097151, 4194303, 8388607, 16777215, 33554431,
			67108863, 134217727, 268435455, 536870911, 1073741823, 2147483647,
			-1 };

	private static final int _fldfor = 196;

	private static int[] _fldtry = new int[257];

	private static byte[] _fldif = new byte[257];

	private static final int _fldnew = 217;

	private static int[] _fldelse = new int[257];

	private static byte[] _fldint = new byte[257];

	private static final byte[] _fldchar = { 0, 2, 3, 1, 4, 5, 6, 7, 8, 9, 10,
			11, 12, 13, 14, 15, 16 };

	private static int[] accuer = new int[256];

	private static final int b = 195;

	private static final int _fldbyte = 216;

	private static final int _fldvoid = 218;

	private void a(short i, int i_0_) throws IOException {
		int i_1_ = i_0_;
		int i_2_ = i & chk2[i_1_];
		i_1_ += a;
		i_2_ <<= 24 - i_1_;
		i_2_ |= _fldlong;
		for (/**/; i_1_ >= 8; i_1_ -= 8) {
			int i_3_ = i_2_ >> 16 & 0xff;
			write(i_3_);
			if (i_3_ == 255)
				write(0);
			i_2_ <<= 8;
		}
		_fldlong = i_2_;
		a = i_1_;
	}

	private void write(int i) throws IOException {
		baos.write(i);
	}

	public byte[] a(byte[] in, int i, int i_4_) throws IOException {
		a(i, i_4_, 16, 1);
		int i_5_ = (short) ((in[1] << 8) + (in[0] & 0xff));
		short s0 = (short) (i_5_ - 32768);
		_mthif(s0);
		for (int j = 1; j < i; j++) {
			short s1 = (short) ((in[2 * j + 1] << 8) + (in[2 * j] & 0xff));
			short s2 = (short) ((in[2 * j - 1] << 8) + (in[2 * j - 2] & 0xff));
			s0 = (short) (s1 - s2);
			_mthif(s0);
		}
		for (int j = 1; j < i_4_; j++) {
			short s1 = (short) ((in[2 * i * j + 1] << 8) + (in[2 * i
					* j] & 0xff));
			short s2 = (short) ((in[2 * i * (j - 1) + 1] << 8) + (in[2
					* i * (j - 1)] & 0xff));
			s0 = (short) (s1 - s2);
			_mthif(s0);
			for (int i_13_ = 1; i_13_ < i; i_13_++) {
				s1 = (short) ((in[2 * (i * j + i_13_) + 1] << 8) + (in[2 * (i
						* j + i_13_)] & 0xff));
				s2 = (short) ((in[2 * (i * j + i_13_) - 1] << 8) + (in[2 * (i
						* j + i_13_) - 2] & 0xff));
				s0 = (short) (s1 - s2);
				_mthif(s0);
			}
		}
		a((short) 127, 7);
		a(217, baos);
		return baos.toByteArray();
	}

	public byte[] a(short[] is, int i, int i_14_) throws IOException {
		a(i, i_14_, 16, 1);
		int i_15_ = is[0];
		short i_16_ = (short) (is[0] - 32768);
		_mthif(i_16_);
		for (int i_17_ = 1; i_17_ < i; i_17_++) {
			i_16_ = (short) (is[i_17_] - is[i_17_ - 1]);
			_mthif(i_16_);
		}
		for (int i_18_ = 1; i_18_ < i_14_; i_18_++) {
			i_16_ = (short) (is[i * i_18_] - is[i * (i_18_ - 1)]);
			_mthif(i_16_);
			for (int i_19_ = 1; i_19_ < i; i_19_++) {
				i_16_ = (short) (is[i * i_18_ + i_19_] - is[i * i_18_ + i_19_
						- 1]);
				_mthif(i_16_);
			}
		}
		a((short) 127, 7);
		a(217, baos);
		return baos.toByteArray();
	}

	public byte[] a(byte[] is, byte[] is_20_, byte[] is_21_, int i, int i_22_)
			throws IOException {
		a(i, i_22_, 8, 3);
		byte i_23_ = (byte) (is[0] - 128);
		_mthif(i_23_);
		i_23_ = (byte) (is_20_[0] - 128);
		_mthif(i_23_);
		i_23_ = (byte) (is_21_[0] - 128);
		_mthif(i_23_);
		for (int i_24_ = 1; i_24_ < i; i_24_++) {
			i_23_ = (byte) (is[i_24_] - is[i_24_ - 1]);
			_mthif(i_23_);
			i_23_ = (byte) (is_20_[i_24_] - is_20_[i_24_ - 1]);
			_mthif(i_23_);
			i_23_ = (byte) (is_21_[i_24_] - is_21_[i_24_ - 1]);
			_mthif(i_23_);
		}
		for (int i_25_ = 1; i_25_ < i_22_; i_25_++) {
			i_23_ = (byte) (is[i * i_25_] - is[i * (i_25_ - 1)]);
			_mthif(i_23_);
			i_23_ = (byte) (is_20_[i * i_25_] - is_20_[i * (i_25_ - 1)]);
			_mthif(i_23_);
			i_23_ = (byte) (is_21_[i * i_25_] - is_21_[i * (i_25_ - 1)]);
			_mthif(i_23_);
			for (int i_26_ = 1; i_26_ < i; i_26_++) {
				i_23_ = (byte) (is[i * i_25_ + i_26_] - is[i * i_25_ + i_26_
						- 1]);
				_mthif(i_23_);
				i_23_ = (byte) (is_20_[i * i_25_ + i_26_] - is_20_[i * i_25_
						+ i_26_ - 1]);
				_mthif(i_23_);
				i_23_ = (byte) (is_21_[i * i_25_ + i_26_] - is_21_[i * i_25_
						+ i_26_ - 1]);
				_mthif(i_23_);
			}
		}
		a((short) 127, 7);
		a(217, baos);
		return baos.toByteArray();
	}

	public byte[] _mthif(byte[] is, int i, int i_27_) throws IOException {
		a(i, i_27_, 8, 1);
		byte i_28_ = (byte) (is[0] - 128);
		_mthif(i_28_);
		for (int i_29_ = 1; i_29_ < i; i_29_++) {
			i_28_ = (byte) (is[i_29_] - is[i_29_ - 1]);
			_mthif(i_28_);
		}
		for (int i_30_ = 1; i_30_ < i_27_; i_30_++) {
			i_28_ = (byte) (is[i * i_30_] - is[i * (i_30_ - 1)]);
			_mthif(i_28_);
			for (int i_31_ = 1; i_31_ < i; i_31_++) {
				i_28_ = (byte) (is[i * i_30_ + i_31_] - is[i * i_30_ + i_31_
						- 1]);
				_mthif(i_28_);
			}
		}
		a((short) 127, 7);
		a(217, baos);
		return baos.toByteArray();
	}

	private void _mthif(int i) throws IOException {
		if (Math.abs(i) == 32768)
			a((short) -2, _fldif[16]);
		else {
			int i1;
			int i2 = i1 = i;
			if (i2 < 0) {
				i2 = -i2;
				i1--;
			}
			int i3 = 0;
			if (i2 != 0) {
				for (/**/; i2 >= 256; i2 >>= 8)
					i3 += 8;
				i3 += accuer[i2 & 0xff];
			}
			a((short) _fldtry[i3], _fldif[i3]);
			if (i3 != 0)
				a((short) i1, i3);
		}
	}

	private void a(int i, int i_35_, int i_36_, int i_37_) throws IOException {
		baos = new ByteArrayOutputStream();
		int i_38_ = 8 + 3 * i_37_;
		int i_39_ = 6 + 2 * i_37_;
		int i_40_ = 19;
		for (int i_41_ = 0; i_41_ < chk1.length; i_41_++)
			i_40_ += chk1[i_41_];
		DataOutputStream dos = new DataOutputStream(baos);
		a(216, baos);
		a(195, baos);
		dos.writeShort(i_38_);
		baos.write(i_36_);
		dos.writeShort(i_35_);
		dos.writeShort(i);
		baos.write(i_37_);
		for (int i_42_ = 0; i_42_ < i_37_; i_42_++) {
			baos.write(i_42_);
			baos.write(17);
			baos.write(0);
		}
		a(196, baos);
		dos.writeShort(i_40_);
		baos.write(0);
		for (int i_43_ = 0; i_43_ < chk1.length; i_43_++)
			baos.write(chk1[i_43_]);
		for (int i_44_ = 0; i_44_ < _fldchar.length; i_44_++)
			baos.write(_fldchar[i_44_]);
		a(218, baos);
		dos.writeShort(i_39_);
		baos.write(i_37_);
		for (int i_45_ = 0; i_45_ < i_37_; i_45_++) {
			baos.write(i_45_);
			baos.write(0);
		}
		baos.write(1);
		baos.write(0);
		baos.write(0);
	}

	private void a(int i, OutputStream outputstream) throws IOException {
		outputstream.write(255);
		outputstream.write(i);
	}

	static {
		for (int i = 0; i < 256; i++) {
			int i_46_ = i;
			int i_47_ = 1;
			while ((i_46_ >>= 1) != 0)
				i_47_++;
			accuer[i] = i_47_;
		}
		int i = 0;
		boolean bool = true;
		boolean bool_48_ = true;
		for (int i_49_ = 0; i_49_ < 16; i_49_++) {
			for (int i_50_ = 1; i_50_ <= chk1[i_49_]; i_50_++) {
				_fldint[i] = (byte) (i_49_ + 1);
				i++;
			}
		}
		_fldint[i] = (byte) 0;
		int i_51_ = i;
		int i_52_ = 0;
		int i_53_ = _fldint[0];
		i = 0;
		while (_fldint[i] != 0) {
			while (_fldint[i] == i_53_) {
				_fldelse[i] = i_52_;
				i++;
				i_52_++;
			}
			i_52_ <<= 1;
			i_53_++;
		}
		i = 0;
		do {
			byte i_54_ = _fldchar[i];
			_fldtry[i_54_] = _fldelse[i];
			_fldif[i_54_] = _fldint[i];
		} while (++i < i_51_);
	}
}