/*
 * a - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.utils.helpers;

import java.io.IOException;
import java.io.InputStream;

public class LongReader {
	public static int read(InputStream is) throws IOException {
		int i = is.read();
		i += is.read() << 8;
		i += is.read() << 16;
		i += is.read() << 24;
		return i;
	}
}