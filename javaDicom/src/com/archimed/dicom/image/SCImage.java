/*
 * SCImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.image;

import com.archimed.dicom.DicomException;

public class SCImage extends DicomImage {
	public void scEquipmentData(String string) {
		try {
			deleteItem(83);
			set(83, string);
		} catch (DicomException dicomexception) {
			System.err.println(dicomexception);
		}
	}

	public void scImageData() {
		/* empty */
	}
}