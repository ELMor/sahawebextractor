/*
 * b - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.image;

import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.IndexColorModel;
import java.util.Hashtable;
import java.util.Vector;

import com.archimed.dicom.DicomException;

class DicomImageConsumer implements ImageConsumer {
	DicomImage dcmimg;

	ImageIO imgio;

	byte[] surface;

	int[] palette;

	int width;

	int height;

	ColorModel colorModel;

	boolean colored;

	public boolean status;

	public DicomImageConsumer(ImageIO imageio) {
		imgio = imageio;
		dcmimg = imageio.dcm;
		status = false;
	}

	boolean isColor() {
		if (colored) {
			if (colorModel instanceof IndexColorModel) {
				IndexColorModel indexcolormodel = (IndexColorModel) colorModel;
				for (int i = 0; i < indexcolormodel.getMapSize(); i++) {
					int i_0_ = indexcolormodel.getRed(i);
					int i_1_ = indexcolormodel.getGreen(i);
					int i_2_ = indexcolormodel.getBlue(i);
					if (i_0_ != i_1_ || i_1_ != i_2_)
						return false;
				}
				return true;
			}
			System.out.println("??? bytes + not indexcolor");
			return false;
		}
		if (colorModel instanceof GrayColorModel)
			return true;
		if (colorModel instanceof DirectColorModel) {
			for (int i = 0; i < width * height; i++) {
				if ((colorModel.getRed(palette[i]) != colorModel
						.getGreen(palette[i]))
						|| (colorModel.getGreen(palette[i]) != colorModel
								.getBlue(palette[i])))
					return false;
			}
			return true;
		}
		System.out.println("??? ints + not directcolor");
		return false;
	}

	Vector getPalette() {
		Vector vector = new Vector(256);
		if (colored) {
			IndexColorModel indexcolormodel = (IndexColorModel) colorModel;
			for (int i = 0; i < indexcolormodel.getMapSize(); i++) {
				int i_3_ = ((indexcolormodel.getRed(i) << 16)
						+ (indexcolormodel.getGreen(i) << 8) + indexcolormodel
						.getBlue(i));
				vector.addElement(new Integer(i_3_));
			}
			return vector;
		}
		for (int i = 0; i < width * height; i++) {
			int i_4_ = palette[i];
			if (!vector.contains(new Integer(i_4_))) {
				vector.addElement(new Integer(i_4_));
				if (vector.size() > 256)
					return null;
			}
		}
		return vector;
	}

	boolean isRed() {
		IndexColorModel indexcolormodel = (IndexColorModel) colorModel;
		for (int i = 0; i < indexcolormodel.getMapSize(); i++) {
			if (indexcolormodel.getRed(i) != i)
				return false;
		}
		return true;
	}

	void tryImagePixelData() {
		if (colorModel instanceof IndexColorModel) {
			if (isRed()) {
				try {
					dcmimg.imagePixelData(height, width, 8, 8, 7, surface);
				} catch (DicomException dicomexception) {
					dicomexception.printStackTrace();
				}
			} else {
				byte[] is = new byte[width * height];
				for (int i = 0; i < width * height; i++)
					is[i] = (byte) colorModel.getRed(surface[i]);
				try {
					dcmimg.imagePixelData(height, width, 8, 8, 7, is);
				} catch (DicomException dicomexception) {
					dicomexception.printStackTrace();
				}
			}
		} else if (colorModel instanceof GrayColorModel) {
			int pixelSize = colorModel.getPixelSize();
			int byteSize = pixelSize / 8;
			byte[] surf = new byte[width * height * byteSize];
			for (int j = 0; j < width * height; j++) {
				for (int k = 0; k < byteSize; k++)
					surf[byteSize * j + k] = (byte) ((palette[j] & 255 << k * 8) >> k * 8);
			}
			try {
				dcmimg.imagePixelData(height, width, pixelSize, pixelSize, pixelSize - 1, surf);
			} catch (DicomException dicomexception) {
				dicomexception.printStackTrace();
			}
		} else if (colorModel instanceof DirectColorModel) {
			byte[] surf = new byte[width * height];
			for (int i = 0; i < width * height; i++)
				surf[i] = (byte) (palette[i] & 0xff);
			try {
				dcmimg.imagePixelData(height, width, 8, 8, 7, surf);
			} catch (DicomException dicomexception) {
				dicomexception.printStackTrace();
			}
		} else
			System.out.println("cm: " + colorModel);
	}

	void getSurface(Vector vector) {
		int i = vector.size();
		byte[] ba1 = new byte[256];
		byte[] ba2 = new byte[256];
		byte[] ba3 = new byte[256];
		for (int j = 0; j < i; j++) {
			ba1[j] = (byte) ((((Integer) vector.elementAt(j)).intValue() & 0xff0000) >> 16);
			ba3[j] = (byte) ((((Integer) vector.elementAt(j)).intValue() & 0xff00) >> 8);
			ba2[j] = (byte) (((Integer) vector.elementAt(j)).intValue() & 0xff);
		}
		if (colorModel instanceof IndexColorModel) {
			try {
				dcmimg.imagePixelData(height, width, surface, ba1, ba3, ba2);
			} catch (DicomException dicomexception) {
				dicomexception.printStackTrace();
			}
		} else if (colorModel instanceof DirectColorModel) {
			Hashtable hashtable = new Hashtable(i);
			for (int j = 0; j < i; j++)
				hashtable.put(vector.elementAt(j), new Integer(j));
			surface = new byte[width * height];
			for (int j = 0; j < width * height; j++)
				surface[j] = (byte) ((Integer) hashtable.get(new Integer(
						palette[j]))).intValue();
			try {
				dcmimg.imagePixelData(height, width, surface, ba1, ba3, ba2);
			} catch (DicomException dicomexception) {
				dicomexception.printStackTrace();
			}
		}
	}

	void imagePixelData() {
		try {
			dcmimg.imagePixelData(height, width, 0, palette);
		} catch (DicomException dicomexception) {
			dicomexception.printStackTrace();
		}
	}

	public void imageComplete(int i) {
		if (i == 3) {
			boolean bool = isColor();
			if (bool) {
				tryImagePixelData();
				status = true;
				ImageIO imageio = imgio;
				synchronized (imageio) {
					imgio.notify();
				}
			} else {
				Vector vector = getPalette();
				if (vector != null) {
					getSurface(vector);
					status = true;
					ImageIO imageio = imgio;
					synchronized (imageio) {
						imgio.notify();
					}
				} else {
					imagePixelData();
					status = true;
					ImageIO imageio = imgio;
					synchronized (imageio) {
						imgio.notify();
					}
				}
			}
		}
	}

	public void setColorModel(ColorModel colormodel) {
		colorModel = colormodel;
	}

	public void setDimensions(int i, int i_13_) {
		width = i;
		height = i_13_;
	}

	public void setHints(int i) {
		/* empty */
	}

	public void setPixels(int x, int y, int wi, int he,	ColorModel cm, byte[] ns, int x2, int y2) {
		if (x != 0 || y != 0 || wi != width || he != height) {
			if (surface == null)
				surface = new byte[width * height];
			for (int j = y; j < y + he; j++)
				System.arraycopy(ns, 0, surface, x + j * width, x + wi);
		} else
			surface = ns;
		colored = true;
	}

	public void setPixels(int x, int y, int wi, int he,	ColorModel cm, int[] is, int wi2, int he2) {
		if (x != 0 || y != 0 || wi != width || he != height) {
			if (palette == null)
				palette = new int[width * height];
			for (int j = y; j < y + he; j++)
				System.arraycopy(is, 0, palette, x + j * width, x+ wi);
		} else
			palette = is;
		colored = false;
	}

	public void setProperties(Hashtable hashtable) {
		/* empty */
	}
}