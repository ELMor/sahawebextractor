/*
 * o - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.util.Vector;

class ByteSwapper {
	static void swapBytes(byte[] is) {
		for (int i = 0; i < is.length; i += 2) {
			byte i_0_ = is[i];
			is[i] = is[i + 1];
			is[i + 1] = i_0_;
		}
	}

	public static Vector sort(Vector vector) {
		int i = vector.size();
		for (int i_1_ = 0; i_1_ < i; i_1_++) {
			boolean bool = true;
			for (int i_2_ = 0; i_2_ < i - 1; i_2_++) {
				if (((Integer) vector.elementAt(i_2_)).intValue() > ((Integer) vector
						.elementAt(i_2_ + 1)).intValue()) {
					Integer integer = (Integer) vector.elementAt(i_2_ + 1);
					vector.setElementAt(vector.elementAt(i_2_), i_2_ + 1);
					vector.setElementAt(integer, i_2_);
					bool &= false;
				}
			}
			if (bool)
				i_1_ = i;
		}
		return vector;
	}
}