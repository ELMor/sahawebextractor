/*
 * DicomObject - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

public class DicomObject extends GroupList {
	DicomObject filemetainfo = null;

	long offset = 0L;

	int length = 0;

	private void a(int _i, int i_0_, int i_1_, Object object, int i_2_)
			throws DicomException {
		Object object_3_ = null;
		boolean bool = true;
		VR var_c = getVR(_i, i_0_);
		if (var_c == null) {
			if (Debug.DEBUG >= 5) {
				if (object != null)
					Debug.printMessage(("DicomObject.addValue <new>, index: "
							+ i_2_ + ", type: " + object.getClass().getName()),
							_i, i_0_);
				else
					Debug.printMessage(("DicomObject.addValue <new>, index: "
							+ i_2_ + ", type: null"), _i, i_0_);
			}
			var_c = new VR(_i, i_0_, i_1_);
			bool = false;
		} else if (Debug.DEBUG >= 5) {
			if (object != null)
				Debug.printMessage(("DicomObject.addValue <old>, index: "
						+ i_2_ + ", type: " + object.getClass().getName()), _i,
						i_0_);
			else
				Debug.printMessage(("DicomObject.addValue <old>, index: "
						+ i_2_ + ", type: null"), _i, i_0_);
		}
		if (i_2_ > var_c.val.size())
			throw new DicomException("Index (" + i_2_
					+ ") exceeds bounds of Data Element");
		try {
			if (object == null)
				object_3_ = null;
			else if (object instanceof String)
				object_3_ = TypeDecoder.str((String) object, var_c._fldif);
			else if (object instanceof byte[])
				object_3_ = TypeDecoder.byteArray((byte[]) object, var_c._fldif);
			else if (object instanceof Integer)
				object_3_ = TypeDecoder.integ((Integer) object, var_c._fldif);
			else if (object instanceof Float)
				object_3_ = TypeDecoder.flo((Float) object, var_c._fldif);
			else if (object instanceof Double)
				object_3_ = TypeDecoder.dou((Double) object, var_c._fldif);
			else if (object instanceof Person)
				object_3_ = TypeDecoder.person((Person) object, var_c._fldif);
			else if (object instanceof DDate)
				object_3_ = TypeDecoder.dat((DDate) object, var_c._fldif);
			else if (object instanceof DDateRange)
				object_3_ = TypeDecoder.dateRange((DDateRange) object, var_c._fldif);
			else if (object instanceof Short)
				object_3_ = TypeDecoder.shor((Short) object, var_c._fldif);
			else if (object instanceof Long)
				object_3_ = TypeDecoder.lon((Long) object, var_c._fldif);
			else if (object instanceof int[])
				object_3_ = TypeDecoder.a((int[]) object, var_c._fldif);
			else if (object instanceof float[])
				object_3_ = TypeDecoder.floatArray((float[]) object, var_c._fldif);
			else if (object instanceof DicomObject)
				object_3_ = TypeDecoder.dicomObject((DicomObject) object, var_c._fldif);
			else
				throw new DicomException("Unsupported type given: "
						+ object.getClass().getName());
		} catch (DicomException dicomexception) {
			throw new DicomException(_i, i_0_, dicomexception.getMessage());
		}
		if (!bool) {
			if (object_3_ == null) {
				var_c.val = new Vector();
				var_c._flddo = true;
			} else {
				var_c.val.addElement(object_3_);
				var_c._flddo = false;
			}
			push(var_c);
		} else if (object_3_ != null) {
			if (i_2_ < var_c.val.size())
				var_c.val.setElementAt(object_3_, i_2_);
			else
				var_c.val.addElement(object_3_);
			if (var_c._flddo)
				var_c._flddo = false;
		}
	}

	public void set(int i, Object object) throws DicomException {
		int i_4_ = DDict.getGroup(i);
		int i_5_ = DDict.getElement(i);
		set_ge(i_4_, i_5_, object);
	}

	public void set_ge(int i, int i_6_, Object object) throws DicomException {
		deleteItem_ge(i, i_6_);
		set_ge(i, i_6_, object, 0);
	}

	public void append(int i, Object object) throws DicomException {
		int i_7_ = DDict.getGroup(i);
		int i_8_ = DDict.getElement(i);
		append_ge(i_7_, i_8_, object);
	}

	public void append_ge(int i, int i_9_, Object object) throws DicomException {
		if (object != null) {
			int i_10_ = getSize_ge(i, i_9_);
			if (i_10_ == -1)
				i_10_ = 0;
			set_ge(i, i_9_, object, i_10_);
		}
	}

	public void set(int i, Object object, int i_11_) throws DicomException {
		int i_12_ = DDict.getGroup(i);
		int i_13_ = DDict.getElement(i);
		set_ge(i_12_, i_13_, object, i_11_);
	}

	public void set_ge(int i, int i_14_, Object object, int i_15_)
			throws DicomException {
		int i_16_ = DDict.getTypeCode(i, i_14_);
		a(i, i_14_, i_16_, object, i_15_);
	}

	public int getType(int i) throws DicomException {
		int i_17_ = DDict.getGroup(i);
		int i_18_ = DDict.getElement(i);
		return getType_ge(i_17_, i_18_);
	}

	public int getType_ge(int i, int i_19_) throws DicomException {
		VR var_c = getVR(i, i_19_);
		if (var_c == null)
			throw new DicomException("no such tag in DicomObject");
		return var_c._fldif;
	}

	public void setType(int i, int i_20_) throws DicomException {
		int i_21_ = DDict.getGroup(i);
		int i_22_ = DDict.getElement(i);
		setType_ge(i_21_, i_22_, i_20_);
	}

	public void setType_ge(int i, int i_23_, int i_24_) throws DicomException {
		DDict.getTypeName(i_24_);
		int i_25_ = getType_ge(i, i_23_);
		switch (i_25_) {
		case 21:
			if (i_24_ != 3 && i_24_ != 23)
				throw new DicomException(
						"Illegal new type: new VR type must be tUS or tSS");
			break;
		case 22:
			if (i_24_ != 24 && i_24_ != 8)
				throw new DicomException(
						"Illegal new type: new VR type must be tOW or tOB");
			break;
		default:
			throw new DicomException("current type must be tUS_US or tOW_OB");
		}
		VR var_c = getVR(i, i_23_);
		if (var_c == null)
			throw new DicomException("no such tag in DicomObject");
		var_c._fldif = i_24_;
	}

	private boolean a() {
		boolean bool = false;
		bool = _mthif(1184);
		bool = _mthif(1505);
		bool = _mthif(498);
		bool = _mthif(499);
		bool = _mthif(500);
		bool = _mthif(501);
		bool = _mthif(502);
		bool = _mthif(503);
		for (int i = 0; i < 64; i++) {
			bool = a(24576 + i, 12288, 24);
			bool = a(20480 + i, 12288, 8);
			VR var_c = getVR(20480 + i, 8194);
			if (var_c != null && var_c.getValue() != null
					&& ((Integer) var_c.getValue()).intValue() == 0)
				bool = a(20480, 8204, 24);
			else
				bool = a(20480, 8204, 8);
		}
		return bool;
	}

	private boolean _mthif(int i) {
		int i_26_ = DDict.getGroup(i);
		int i_27_ = DDict.getElement(i);
		return a(i_26_, i_27_, 24);
	}

	private boolean a(int i, int i_28_, int i_29_) {
		VR var_c = getVR(i, i_28_);
		if (var_c != null && var_c._fldif == 22) {
			var_c._fldif = i_29_;
			return true;
		}
		return false;
	}

	public int getSize(int i) {
		int i_30_ = DDict.getGroup(i);
		int i_31_ = DDict.getElement(i);
		return getSize_ge(i_30_, i_31_);
	}

	public int getSize_ge(int i, int i_32_) {
		VR var_c = getVR(i, i_32_);
		if (var_c == null)
			return -1;
		return var_c.val.size();
	}

	public long calculateOffset(int i, int i_33_, int i_34_, boolean bool,
			boolean bool_35_) {
		return new Offsets(this).calculateOffset(i, i_33_, i_34_, bool,
				bool_35_, true);
	}

	public long getOffset(int i, int i_36_) {
		VR var_c = getVR(DDict.getGroup(i), DDict.getElement(i));
		if (var_c == null)
			return 0L;
		if (i_36_ >= var_c.val.size())
			return 0L;
		if (var_c._fldif != 10)
			return 0L;
		return ((DicomObject) var_c.val.elementAt(i_36_)).offset;
	}

	public Object get(int i) {
		return get(i, 0);
	}

	public Object get_ge(int i, int i_37_) {
		return get_ge(i, i_37_, 0);
	}

	public Object get(int i, int i_38_) {
		int i_39_ = DDict.getGroup(i);
		int i_40_ = DDict.getElement(i);
		return get_ge(i_39_, i_40_, i_38_);
	}

	public Object get_ge(int i, int i_41_, int i_42_) {
		VR var_c = getVR(i, i_41_);
		if (var_c == null || var_c._flddo)
			return null;
		if (i_42_ >= var_c.val.size())
			return null;
		return var_c.val.elementAt(i_42_);
	}

	public String getS(int i) throws DicomException {
		return getS(i, 0);
	}

	public String getS_ge(int i, int i_43_) throws DicomException {
		return getS_ge(i, i_43_, 0);
	}

	public String getS(int i, int i_44_) throws DicomException {
		int i_45_ = DDict.getGroup(i);
		int i_46_ = DDict.getElement(i);
		return getS_ge(i_45_, i_46_, i_44_);
	}

	public String getS_ge(int _i, int i_47_, int i_48_) throws DicomException {
		Object object = get_ge(_i, i_47_, i_48_);
		if (object != null) {
			int i_49_ = getVR(_i, i_47_)._fldif;
			return TypeDecoder.generic(object, i_49_);
		}
		return null;
	}

	public int getI(int i) throws DicomException {
		return getI(i, 0);
	}

	public int getI_ge(int i, int i_50_) throws DicomException {
		return getI_ge(i, i_50_, 0);
	}

	public int getI(int i, int i_51_) throws DicomException {
		int i_52_ = DDict.getGroup(i);
		int i_53_ = DDict.getElement(i);
		return getI_ge(i_52_, i_53_, i_51_);
	}

	public int getI_ge(int _i, int i_54_, int i_55_) throws DicomException {
		Object object = get_ge(_i, i_54_, i_55_);
		if (object != null) {
			int i_56_ = getVR(_i, i_54_)._fldif;
			return TypeDecoder.obj(object, i_56_);
		}
		return 2147483647;
	}

	public Vector deleteItem(int i) {
		int i_57_ = DDict.getGroup(i);
		int i_58_ = DDict.getElement(i);
		return deleteItem_ge(i_57_, i_58_);
	}

	public Vector deleteItem_ge(int i, int i_59_) {
		VR var_c = removeVR(i, i_59_);
		if (var_c == null)
			return null;
		return var_c.val;
	}

	public Object deleteItem(int i, int i_60_) {
		int i_61_ = DDict.getGroup(i);
		int i_62_ = DDict.getElement(i);
		return deleteItem_ge(i_61_, i_62_, i_60_);
	}

	public Object deleteItem_ge(int i, int i_63_, int i_64_) {
		VR var_c = getVR(i, i_63_);
		if (i_64_ >= var_c.val.size())
			return null;
		Object object = var_c.val.elementAt(i_64_);
		var_c.val.removeElementAt(i_64_);
		return object;
	}

	public void read(InputStream inputstream) throws IOException,
			DicomException {
		read(inputstream, true);
	}

	public void read(InputStream inputstream, boolean bool) throws IOException,
			DicomException {
		InputStream inputstream_65_;
		if (inputstream.markSupported())
			inputstream_65_ = inputstream;
		else
			inputstream_65_ = new BufferedInputStream(inputstream);
		inputstream_65_.mark(256);
		boolean bool_66_;
		try {
			bool_66_ = checkIfDicomFile(inputstream_65_);
		} catch (Exception exception) {
			bool_66_ = false;
		}
		inputstream_65_.reset();
		if (bool_66_)
			readDICOMFileStream(inputstream_65_, bool);
		else
			readDICOMStream(0L, inputstream_65_, bool);
	}

	long readHeader(InputStream inputstream) throws IOException, DicomException {
		if (Debug.DEBUG > 3)
			Debug.printMessage("DicomObject.readHeader.");
		byte[] is = new byte[128];
		byte[] is_67_ = new byte[4];
		FIS5 var_r = new FIS5(inputstream);
		var_r.setData1(8194);
		var_r.discard(is);
		var_r.discard(is_67_);
		if (is_67_[0] != 68 || is_67_[1] != 73 || is_67_[2] != 67
				|| is_67_[3] != 77)
			throw new DicomException("Not a valid Dicom-file");
		filemetainfo = new DicomObject();
		VR var_c = new VR();
		var_c.a((FIS3) var_r);
		var_c.a(var_r);
		if (var_c.a() != 27) {
			filemetainfo.push(var_c);
			for (;;) {
				var_c = new VR();
				var_c.a((FIS3) var_r);
				var_c.a(var_r);
				if (var_c.getGroup() != 2) {
					push(var_c);
					break;
				}
				filemetainfo.push(var_c);
				if (var_c.getGroup() == 2 && var_c.getElement() == 16) {
					try {
						var_r.setData1(UID.getUIDEntry(
								filemetainfo.getS(31).trim()).getConstant());
					} catch (UnknownUIDException unknownuidexception) {
						var_r.setData1(8192);
					}
				}
			}
		} else {
			long l = ((Long) var_c.val.elementAt(0)).longValue()
					+ var_r.getReaded();
			while (var_r.getReaded() < l) {
				var_c = new VR();
				var_c.a((FIS3) var_r);
				var_c.a(var_r);
				filemetainfo.push(var_c);
			}
		}
		return var_r.getReaded();
	}

	void readDICOMFileStream(InputStream inputstream, boolean bool)
			throws IOException, DicomException {
		long l = readHeader(inputstream);
		readDICOMStream(l, inputstream, bool);
	}

	void readDICOMStream(long l, InputStream inputstream, boolean bool)
			throws IOException, DicomException {
		readDICOMStream(l, inputstream, 8193, bool);
	}

	public void read(InputStream inputstream, int i, boolean bool)
			throws IOException, DicomException {
		readDICOMStream(0L, inputstream, i, bool);
	}

	void readDICOMStream(long l, InputStream inputstream, int i, boolean bool)
			throws IOException, DicomException {
		if (Debug.DEBUG > 3)
			Debug.printMessage("DicomObject.readDICOMStream.");
		if (filemetainfo != null) {
			try {
				i = UID.getUIDEntry(filemetainfo.getS(31).trim()).getConstant();
			} catch (UnknownUIDException unknownuidexception) {
				i = 8192;
			}
		}
		FIS5 var_r = new FIS5(inputstream, (int) l);
		var_r.setData1(i);
		readTags(var_r, bool);
		if (i == 8193) {
			boolean bool_68_ = a();
		}
	}

	void readTags(FIS5 r1, boolean flag) throws IOException, DicomException {
		/*
    void readTags(r r1, boolean flag)
        throws IOException, DicomException
    {
_L1:
        c c1 = new c();
        if(c1.a(((k) (r1)))) goto _L2; else goto _L1
_L2:
        try
        {
            if(!flag && c1.group == 32736 && c1.element == 16)
                return;
        }
        catch(EOFException eofexception)
        {
            return;
        }
        c1.a(r1);
        if(c1.element != 0)
            push(c1);
          goto _L1
    }
		 */
		while(true){
			VR c1 = new VR();
			try{
				if(!c1.a((FIS3)r1))
					continue;
				if (!flag && c1.group == 32736 && c1.element == 16)
					return;
				c1.a(r1);
				if(c1.element!=0)
					push(c1);
			}catch(EOFException eof){
				return;
			}
		}
	}

	boolean checkIfDicomFile(InputStream inputstream) throws IOException,
			DicomException {
		if (inputstream.available() < 132)
			return false;
		byte[] is = new byte[128];
		byte[] is_73_ = new byte[4];
		DataInputStream datainputstream = new DataInputStream(inputstream);
		datainputstream.readFully(is);
		datainputstream.readFully(is_73_);
		if (new String(is_73_).equals("DICM"))
			return true;
		return false;
	}

	public void write(OutputStream outputstream, boolean bool)
			throws DicomException, IOException {
		if (bool) {
			if (filemetainfo == null)
				changeDICOMFileMetaInformation(8193);
			writeHeader(outputstream);
		}
		int i;
		try {
			i = UID.getUIDEntry(filemetainfo.getS(31)).getConstant();
		} catch (Exception exception) {
			i = 8193;
		}
		OSWritter var_a = new OSWritter(outputstream);
		var_a.a(true);
		var_a._mthif(i);
		if (Debug.DEBUG > 3)
			Debug.printMessage("DicomObject.writeTags. transfer syntax: " + i);
		writeTags(var_a, true);
	}

	void writeHeader(OutputStream outputstream) throws IOException,
			DicomException {
		if (Debug.DEBUG > 3)
			Debug.printMessage("DicomObject.writeHeader.");
		byte[] is = new byte[128];
		OSWritter var_a = new OSWritter(outputstream);
		var_a._mthif(8194);
		for (int i = 0; i < 128; i++)
			is[i] = (byte) 0;
		var_a.write(is);
		var_a.write(68);
		var_a.write(73);
		var_a.write(67);
		var_a.write(77);
		filemetainfo.writeTags(var_a, true);
	}

	public void write(OutputStream outputstream, boolean bool, int i,
			boolean bool_74_) throws DicomException, IOException {
		if (bool) {
			if (filemetainfo == null)
				changeDICOMFileMetaInformation(i);
			writeHeader(outputstream);
		}
		OSWritter var_a = new OSWritter(outputstream);
		var_a.a(bool_74_);
		var_a._mthif(i);
		if (Debug.DEBUG > 3)
			Debug.printMessage("DicomObject.writeTags. transfer syntax: " + i);
		writeTags(var_a, true);
	}

	public void write(OutputStream outputstream, boolean bool, int i,
			boolean bool_75_, boolean bool_76_) throws DicomException,
			IOException {
		if (bool) {
			if (filemetainfo == null)
				changeDICOMFileMetaInformation(i);
			writeHeader(outputstream);
		}
		OSWritter var_a = new OSWritter(outputstream);
		var_a.a(bool_75_);
		var_a._mthif(i);
		if (Debug.DEBUG > 3)
			Debug.printMessage("DicomObject.writeTags. transfer syntax: " + i);
		writeTags(var_a, bool_76_);
	}

	void writeTags(OSWritter var_a, boolean bool) throws IOException, DicomException {
		Enumeration enumeration = enumerateVRs(bool, var_a._mthif(), var_a.a());
		while (enumeration.hasMoreElements()) {
			VR var_c = (VR) enumeration.nextElement();
			var_c.a((FOS3) var_a);
			var_c._mthif(var_a);
		}
	}

	void changeDICOMFileMetaInformation(int i) throws DicomException {
		if (filemetainfo == null)
			a(i);
		else {
			try {
				filemetainfo.set(31, UID.getUIDEntry(i).getValue(), 0);
			} catch (IllegalValueException illegalvalueexception) {
				illegalvalueexception.printStackTrace();
			}
		}
	}

	private void a(int i) throws DicomException {
		if (Debug.DEBUG > 3)
			Debug
					.printMessage("DicomObject.makeDICOMFileMetaInformation. transfer syntax: "
							+ i);
		if (filemetainfo == null)
			filemetainfo = new DicomObject();
		if (getSize(62) != 1 || getSize(63) != 1)
			throw new DicomException(
					"This DicomObject can't be written to a Dicom File: Unable to find SOP Common Info");
		byte[] is = { 0, 1 };
		filemetainfo.set(28, is, 0);
		filemetainfo.set(29, get(62), 0);
		filemetainfo.set(30, get(63), 0);
		String string = filemetainfo.getS(31);
		if (string != null) {
			boolean bool = false;
			int i_77_;
			try {
				i_77_ = UID.getUIDEntry(string).getConstant();
			} catch (UnknownUIDException unknownuidexception) {
				throw new DicomException(
						"Cannot change unknown transfersyntax into new one");
			}
			if (i_77_ != 8193 && i_77_ != 8194 && i_77_ != 8195)
				throw new DicomException(
						"Cannot change encapsulated transfersyntax, use Compression.decompress().");
		}
		try {
			filemetainfo.set(31, UID.getUIDEntry(i).getValue(), 0);
		} catch (IllegalValueException illegalvalueexception) {
			illegalvalueexception.printStackTrace();
		}
		if (filemetainfo.getSize(32) == 0)
			filemetainfo.set(32, "1.2.826.0.1.3680043.2.60.0.1.888");
		if (filemetainfo.getSize(33) == 0)
			filemetainfo.set(33, "SoftLink JDT 1.0");
	}

	public DicomObject getFileMetaInformation() {
		return filemetainfo;
	}

	public DicomObject setFileMetaInformation(DicomObject dicomobject_78_) {
		DicomObject dicomobject_79_ = filemetainfo;
		filemetainfo = dicomobject_78_;
		return dicomobject_79_;
	}

	public void dumpVRs(OutputStream outputstream) throws IOException {
		dumpVRs(outputstream, false);
	}

	public void dumpVRs(OutputStream outputstream, boolean bool)
			throws IOException {
		DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
		if (bool && filemetainfo != null)
			filemetainfo.dumpVRs(outputstream);
		Enumeration enumeration = enumerateVRs(false, 8193, true);
		while (enumeration.hasMoreElements()) {
			VR var_c = (VR) enumeration.nextElement();
			dataoutputstream.writeBytes(ObjPrinter.vrDump(var_c, 0));
		}
	}

}