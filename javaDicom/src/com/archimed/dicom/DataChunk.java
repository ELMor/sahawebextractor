/*
 * p - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

class DataChunk {
	int data1;
	int data2;
	Object ref;

	DataChunk next;

	protected Object clone() {
		DataChunk aux = new DataChunk();
		aux.data1 = data1;
		aux.data2 = data2;
		aux.ref = ref;
		aux.next = next != null ? (DataChunk) next.clone() : null;
		return aux;
	}
}