/*
 * UID - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.util.Enumeration;
import java.util.Hashtable;

public class UID {
	public static final int UnknownUID = 0;

	public static final int DICOMApplicationContextName = 20481;

	public static final int DICOMControlledTerminology = 20482;

	static Hashtable entries = new Hashtable();

	public static UIDEntry getUIDEntry(int i) throws IllegalValueException {
		UIDEntry uidentry = (UIDEntry) entries.get(new Integer(i));
		if (uidentry == null)
			throw new IllegalValueException("UID not found");
		return uidentry;
	}

	public static UIDEntry getUIDEntry(String string)
			throws UnknownUIDException {
		Enumeration enumeration = entries.elements();
		while_9_: do {
			UIDEntry uidentry;
			do {
				if (!enumeration.hasMoreElements())
					break while_9_;
				uidentry = (UIDEntry) enumeration.nextElement();
			} while (!uidentry.getValue().equals(string));
			return uidentry;
		} while (false);
		throw new UnknownUIDException(string, "Unknown UID: '" + string + "'");
	}

	static UIDEntry getUIDEntryForCode(String string)
			throws UnknownUIDException {
		Enumeration enumeration = entries.elements();
		while_10_: do {
			UIDEntry uidentry;
			do {
				if (!enumeration.hasMoreElements())
					break while_10_;
				uidentry = (UIDEntry) enumeration.nextElement();
			} while (!uidentry.getShortName().equals(string));
			return uidentry;
		} while (false);
		throw new UnknownUIDException(string, "Unknown UID: '" + string + "'");
	}

	static {
		try {
			Class.forName("com.archimed.dicom.TransferSyntax");
			Class.forName("com.archimed.dicom.SOPClass");
			Class.forName("com.archimed.dicom.MetaSOPClass");
			Class.forName("com.archimed.dicom.SOPInstance");
		} catch (ClassNotFoundException classnotfoundexception) {
			/* empty */
		}
		entries.put(new Integer(20481), new UIDEntry(20481,
				"1.2.840.10008.3.1.1.1", "DICOM Application Context Name",
				"AC", 5));
		entries.put(new Integer(20482), new UIDEntry(20482,
				"1.2.840.10008.2.16.4", "DICOM Controlled Terminology", "CTER",
				6));
	}
}