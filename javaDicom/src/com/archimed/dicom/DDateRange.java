/*
 * DDateRange - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.text.DateFormat;

public class DDateRange {
	DDate d1;

	DDate d2;

	public DDateRange(DDate ddate, DDate ddate_0_) {
		if (ddate != null)
			d1 = ddate;
		else
			d1 = new DDate();
		if (ddate_0_ != null)
			d2 = ddate_0_;
		else
			d2 = new DDate();
	}

	public DDateRange(String string) throws NumberFormatException {
		int i = string.indexOf('-');
		if (i == -1)
			throw new NumberFormatException(string
					+ " cannot parse this string into DDateRange");
		d1 = new DDate(string.substring(0, i));
		d2 = new DDate(string.substring(i + 1));
	}

	public String toString() {
		return d1.toString() + " - " + d2.toString();
	}

	public String toString(DateFormat dateformat) {
		return d1.toString(dateformat) + " - " + d2.toString(dateformat);
	}

	public String toDICOMString() {
		return d1.toDICOMString() + "-" + d2.toDICOMString();
	}

	public DDate getDate1() {
		if (!d1.isEmpty())
			return d1;
		return null;
	}

	public DDate getDate2() {
		if (!d2.isEmpty())
			return d2;
		return null;
	}
}