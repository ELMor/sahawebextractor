/*
 * Offsets - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.util.Enumeration;

public class Offsets {
	DicomObject dcm;

	public Offsets(DicomObject dicomobject) {
		dcm = dicomobject;
	}

	private int a(DicomObject dicomobject, boolean bool, int i, boolean bool_0_) {
		int i_1_ = 0;
		Enumeration enumeration = dicomobject.enumerateVRs(bool, i, bool_0_);
		while (enumeration.hasMoreElements()) {
			VR var_c = (VR) enumeration.nextElement();
			i_1_ += var_c._fldfor + var_c.a;
		}
		return i_1_;
	}

	public long calculateOffset_ge(DicomObject dicomobject, int i, int i_2_,
			int i_3_, boolean bool, boolean bool_4_, boolean bool_5_) {
		VR var_c = dicomobject.getVR(i, i_2_);
		long l = 0L;
		if (var_c == null)
			return 0L;
		if (bool_5_) {
			try {
				dicomobject.changeDICOMFileMetaInformation(i_3_);
			} catch (DicomException dicomexception) {
				dicomexception.printStackTrace();
			}
			DicomObject dicomobject_6_ = dicomobject.getFileMetaInformation();
			if (dicomobject_6_ != null)
				l = (long) (132 + a(dicomobject_6_, true, 8194, bool));
		}
		Enumeration enumeration = dicomobject.enumerateVRs(bool_4_, i_3_, bool);
		while (enumeration.hasMoreElements()) {
			var_c = (VR) enumeration.nextElement();
			if (var_c.group == i && var_c.element == i_2_)
				break;
			l += (long) (var_c.a + var_c._fldfor);
		}
		return l;
	}

	public long calculateOffset_ge(int i, int i_7_, int i_8_, boolean bool,
			boolean bool_9_, boolean bool_10_) {
		return calculateOffset_ge(dcm, i, i_7_, i_8_, bool, bool_9_, bool_10_);
	}

	public long calculateOffset(int i, int i_11_, boolean bool,
			boolean bool_12_, boolean bool_13_) {
		int i_14_ = DDict.getGroup(i);
		int i_15_ = DDict.getElement(i);
		return calculateOffset_ge(i_14_, i_15_, i_11_, bool, bool_12_, bool_13_);
	}

	public long calculateOffset(int i, int i_16_, int i_17_, boolean bool,
			boolean bool_18_, boolean bool_19_) {
		long l = calculateOffset(i, i_17_, bool, bool_18_, bool_19_);
		VR var_c = dcm.getVR(DDict.getGroup(i), DDict.getElement(i));
		if (i_16_ >= var_c.val.size())
			return 0L;
		if (var_c._fldif != 10)
			return 0L;
		l += (long) var_c._fldfor;
		for (int i_20_ = 0; i_20_ < i_16_; i_20_++)
			l += (long) ((DicomObject) var_c.val.elementAt(i_20_)).length;
		return l;
	}

	public long calculateOffset(int i, int i_21_, int i_22_, int i_23_,
			boolean bool, boolean bool_24_, boolean bool_25_) {
		long l = calculateOffset(i, i_21_, i_23_, bool, bool_24_, bool_25_);
		VR var_c = dcm.getVR(DDict.getGroup(i), DDict.getElement(i));
		DicomObject dicomobject = (DicomObject) var_c.val.elementAt(i_21_);
		l += 8L + calculateOffset_ge(dicomobject, DDict.getGroup(i_22_), DDict
				.getElement(i_22_), i_23_, bool, false, false);
		return l;
	}
}