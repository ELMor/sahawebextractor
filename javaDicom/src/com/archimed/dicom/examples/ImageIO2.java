
package com.archimed.dicom.examples;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.TransferSyntax;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;

/**
 * 
 * @author delaet@softlink.be
 * @author deboeck@softlink.be
 */
public class ImageIO2 {
	public ImageIO2(DicomObject dcm) throws IllegalValueException,
			UnknownUIDException, DicomException {
		this.dcm = dcm;
		process();
	}

	public int getNumberOfBits() {
		return numberOfBits;
	}

	public int getPhotometricInterpretation() {
		return photometricInterpretation;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getWindow() {
		return window;
	}

	public int getCenter() {
		return center - center_min;
	}

	public void setCenter(int center) {
		this.center = center + center_min;
	}

	public void setWindow(int window) {
		this.window = window;
	}

	public int getMaxWindow() {
		return window_max;
	}

	public int getMaxCenter() {
		return center_max - center_min;
	}

	private void process() throws IllegalValueException, UnknownUIDException,
			DicomException {
		width = dcm.getI(DDict.dColumns);
		height = dcm.getI(DDict.dRows);
		samplesPerPixel = dcm.getI(DDict.dSamplesPerPixel);
		bitsAllocated = dcm.getI(DDict.dBitsAllocated);
		if ((bitsAllocated % 8) != 0) {
			throw new IllegalValueException(
					"ImageIO2 does not support BitsAllocated values that are not multiples of 8");
		}
		numberOfBits = dcm.getI(DDict.dBitsStored);
		highBit = dcm.getI(DDict.dHighBit);
		if (dcm.getI(DDict.dPlanarConfiguration) == 0) {
			planarConfiguration = COLOR_BY_PIXEL;
		} else if (dcm.getI(DDict.dPlanarConfiguration) == 1) {
			planarConfiguration = COLOR_BY_PLANE;
			throw new IllegalValueException(
					"unsupported planar configuration: color by plane");
		}
		if (dcm.getI(DDict.dPixelRepresentation) == 0) {
			pixelRepresentation = PR_UNSIGNEDINT;
		} else if (dcm.getI(DDict.dPixelRepresentation) == 1) {
			pixelRepresentation = PR_TWOSCOMPLEMENT;
		} else {
			throw new IllegalValueException(
					"missing or illegal value for PixelRepresentation");
		}
		String s = dcm.getS(DDict.dPhotometricInterpretation);
		if (s == null) {
			throw new IllegalValueException(
					"missing value for PhotometricInterpretation");
		} else if (s.equals("MONOCHROME1")) {
			photometricInterpretation = MONOCHROME1;
		} else if (s.equals("MONOCHROME2")) {
			photometricInterpretation = MONOCHROME2;
		} else if (s.equals("PALETTE COLOR")) {
			photometricInterpretation = PALETTE_COLOR;
		} else if (s.equals("RGB")) {
			photometricInterpretation = RGB;
		} else if (s.equals("HSV")) {
			photometricInterpretation = HSV;
		} else if (s.equals("ARGB")) {
			photometricInterpretation = ARGB;
		} else if (s.equals("CMYK")) {
			photometricInterpretation = CMYK;
		} else if (s.equals("YBR_FULL")) {
			photometricInterpretation = YBR_FULL;
		} else if (s.equals("YBR_FULL_422")) {
			photometricInterpretation = YBR_FULL_422;
		} else if (s.equals("YBR_PARTIAL_422")) {
			photometricInterpretation = YBR_PARTIAL_422;
		} else {
			throw new IllegalValueException(
					"illegal value for PhotometricInterpretation: '" + s + "'");
		}
		for (int i = 0; i < numberOfBits; i++) {
			bitMask = bitMask << 1;
			bitMask += 1;
		}
		if (dcm.get(DDict.dNumberOfFrames) == null) {
			frames = 1;
		} else {
			frames = dcm.getI(DDict.dNumberOfFrames);
		}
		DicomObject fmi = dcm.getFileMetaInformation();
		String tsuid = null;
		if (fmi == null) {
			transferSyntax = TransferSyntax.ImplicitVRLittleEndian;
		} else {
			tsuid = fmi.getS(DDict.dTransferSyntaxUID);
			if (tsuid == null) {
				throw new IllegalValueException("transfer syntax missing");
			}
			UIDEntry entry = UID.getUIDEntry(tsuid);
			transferSyntax = entry.getConstant();
		}
		if ((transferSyntax != TransferSyntax.ImplicitVRLittleEndian)
				&& (transferSyntax != TransferSyntax.ExplicitVRLittleEndian)
				&& (transferSyntax != TransferSyntax.ExplicitVRBigEndian)
				&& (transferSyntax != TransferSyntax.JPEGBaseline)
				&& (transferSyntax != TransferSyntax.JPEGLossless)
				&& (transferSyntax != TransferSyntax.RLELossless)) {
			throw new IllegalValueException("transfer syntax not supported: "
					+ tsuid);
		}
		if (photometricInterpretation == PALETTE_COLOR) {
			red = getPalette(DDict.dRedPaletteColorLookupTableData,
					DDict.dRedPaletteColorLookupTableDescriptor);
			green = getPalette(DDict.dGreenPaletteColorLookupTableData,
					DDict.dGreenPaletteColorLookupTableDescriptor);
			blue = getPalette(DDict.dBluePaletteColorLookupTableData,
					DDict.dBluePaletteColorLookupTableDescriptor);
		}
	}

	/**
	 * 
	 * 
	 * @return
	 * @throws DicomException
	 * 
	 * @author Bart De Boeck
	 */
	private BufferedImage createMonochrome1Image() throws DicomException {
		gray = new byte[1 << 8];
		if (numberOfBits == 8) {
			byte_pixels = pixeldata;
			int bandOffsets[] = { 0 };
			DataBuffer dataBuffer = new DataBufferByte(byte_pixels,
					byte_pixels.length);
			wr = Raster.createInterleavedRaster(dataBuffer, width, height,
					width, 1, bandOffsets, null);
			calculateWindowCenter();
			createLUT(window, center);
			IndexColorModel model = new IndexColorModel(8, (1 << 8), gray,
					gray, gray);
			BufferedImage image = new BufferedImage(model, wr, false, null);
			return (image);
		} else if (numberOfBits > 8) {
			short_pixels = new short[width * height];
			int a, b, c, i;
			if (pixelRepresentation == PR_TWOSCOMPLEMENT) {
				for (i = 0; i < short_pixels.length; i++) {
					a = (int) pixeldata[2 * i] & 0xFF;
					b = (int) pixeldata[2 * i + 1];
					c = (b << 8) + a + 32768;
					short_pixels[i] = (short) c;
				}
			} else {
				int mask = (1 << (numberOfBits - 8)) - 1;
				for (i = 0; i < short_pixels.length; i++) {
					a = (int) pixeldata[2 * i] & 0xFF;
					b = ((int) pixeldata[2 * i + 1] & 0xFF) & mask;
					short_pixels[i] = (short) ((b << 8) + a);
				}
			}
			calculateWindowCenter();
			return (calculate8BitFrom16Bit());
		} else {
			return (null);
		}
	}

	private BufferedImage createMonochrome2Image() throws DicomException {
		gray = new byte[1 << 8];
		if (numberOfBits == 8) {
			byte_pixels = pixeldata;
			int bandOffsets[] = { 0 };
			DataBuffer dataBuffer = new DataBufferByte(byte_pixels,
					byte_pixels.length);
			wr = Raster.createInterleavedRaster(dataBuffer, width, height,
					width, 1, bandOffsets, null);
			calculateWindowCenter();
			createLUT(window, center);
			IndexColorModel model = new IndexColorModel(8, (1 << 8), gray,
					gray, gray);
			BufferedImage image = new BufferedImage(model, wr, false, null);
			return (image);
		} else if (numberOfBits > 8) {
			short_pixels = new short[width * height];
			int a, b, c, i;
			if (pixelRepresentation == PR_TWOSCOMPLEMENT) {
				for (i = 0; i < short_pixels.length; i++) {
					a = (int) pixeldata[2 * i] & 0xFF;
					b = (int) pixeldata[2 * i + 1];
					c = (b << 8) + a + 32768;
					short_pixels[i] = (short) c;
				}
			} else {
				int mask = (1 << (numberOfBits - 8)) - 1;
				for (i = 0; i < short_pixels.length; i++) {
					a = (int) pixeldata[2 * i] & 0xFF;
					b = ((int) pixeldata[2 * i + 1] & 0xFF) & mask;
					short_pixels[i] = (short) ((b << 8) + a);
				}
			}
			calculateWindowCenter();
			return (calculate8BitFrom16Bit());
		} else {
			return (null);
		}
	}

	private BufferedImage createRGBImage() {
		int[] int_pixels = new int[width * height];
		if (planarConfiguration == COLOR_BY_PIXEL) {
			for (int i = 0; i < int_pixels.length; i++) {
				int_pixels[i] = 0xFF000000
						+ (((int) pixeldata[3 * i] & 0xFF) << 16)
						+ (((int) pixeldata[3 * i + 1] & 0xFF) << 8)
						+ ((int) pixeldata[3 * i + 2] & 0xFF);
			}
		} else {
			//TODO /*wat gebeurt er hier ?*/
		}
		int bandMasks[] = { 0xFF0000, 0xFF00, 0xFF, 0xFF000000 };
		DataBuffer dataBuffer = new DataBufferInt(int_pixels, int_pixels.length);
		wr = Raster.createPackedRaster(dataBuffer, width, height, width,
				bandMasks, null);
		BufferedImage image = new BufferedImage(ColorModel.getRGBdefault(), wr,
				false, null);
		return (image);
	}

	private BufferedImage createARGBImage() {
		int[] int_pixels = new int[width * height];
		if (planarConfiguration == COLOR_BY_PIXEL) {
			for (int i = 0; i < int_pixels.length; i++) {
				int_pixels[i] = (((int) pixeldata[3 * i] & 0xFF) << 24)
						+ (((int) pixeldata[3 * i + 1] & 0xFF) << 16)
						+ (((int) pixeldata[3 * i + 2] & 0xFF) << 8)
						+ ((int) pixeldata[3 * i + 3] & 0xFF);
			}
		} else {
			//TODO /*wat gebeurt er hier ?*/
		}
		int bandMasks[] = { 0xFF0000, 0xFF00, 0xFF, 0xFF000000 };
		DataBuffer dataBuffer = new DataBufferInt(int_pixels, int_pixels.length);
		wr = Raster.createPackedRaster(dataBuffer, width, height, width,
				bandMasks, null);
		BufferedImage image = new BufferedImage(ColorModel.getRGBdefault(), wr,
				false, null);
		return (image);
	}

	private BufferedImage createPaletteColorImage() {
		ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
		ColorModel cm = new IndexColorModel(8, 256, red, green, blue);
		int bandOffsets[] = { 0 };
		byte_pixels = pixeldata;
		DataBuffer dataBuffer = new DataBufferByte(byte_pixels,
				byte_pixels.length);
		wr = Raster.createInterleavedRaster(dataBuffer, width, height, width,
				1, bandOffsets, null);
		BufferedImage im = new BufferedImage(cm, wr, cm.isAlphaPremultiplied(),
				null);
		return (im);
	}

	public BufferedImage getBufferedImage(int frame) throws DicomException,
			IOException, IllegalValueException {
		if (frame > (frames - 1)) {
			throw new ArrayIndexOutOfBoundsException(
					"frame index larger than number of frames");
		}
		if ((transferSyntax == TransferSyntax.ImplicitVRLittleEndian)
				|| (transferSyntax == TransferSyntax.ExplicitVRLittleEndian)
				|| (transferSyntax == TransferSyntax.ExplicitVRBigEndian)) {
			if (frames == 1) {
				pixeldata = (byte[]) dcm.get(DDict.dPixelData);
			} else {
				int offset = (bitsAllocated / 8) * samplesPerPixel * width
						* height * frame;
				pixeldata = new byte[(bitsAllocated / 8) * samplesPerPixel
						* width * height];
				System.arraycopy(dcm.get(DDict.dPixelData), offset, pixeldata,
						0, pixeldata.length);
			}
		} else {
			pixeldata = Compression.decompressFrame(transferSyntax,
					(byte[]) dcm.get(DDict.dPixelData, frame + 1), width,
					height);
		}
		if (photometricInterpretation == MONOCHROME1) {
			/* Code voor cradle ? */
			return createMonochrome1Image();
		} else if (photometricInterpretation == MONOCHROME2) {
			return (createMonochrome2Image());
		} else if (photometricInterpretation == PALETTE_COLOR) {
			return (createPaletteColorImage());
		} else if (photometricInterpretation == RGB) {
			return (createRGBImage());
		} else if (photometricInterpretation == ARGB) {
			return (createARGBImage());
		} else {
			throw new IllegalValueException(
					"unknown photometric interpretation");
		}
		/* return null; */
		//TODO ok indien monochrome1 ge�mplementeerd ?
	}

	public BufferedImage calculate8BitImage() {
		createLUT(window, center);
		return new BufferedImage(new IndexColorModel(8, (1 << 8), gray, gray,
				gray), wr, false, null);
	}

	private void calculateWindowCenter() throws DicomException {
		float fwindow;
		float fcenter;
		window_max = (1 << numberOfBits) - 1;
		center_max = (1 << numberOfBits) - 1;
		if (dcm.getSize(DDict.dWindowCenter) > 0) {
			fcenter = Float.valueOf(dcm.getS(DDict.dWindowCenter)).floatValue();
			center = (int) Math.round(Math.floor(fcenter));
		} else {
			center = center_max / 2;
		}
		if (dcm.getSize(DDict.dWindowWidth) > 0) {
			fwindow = Float.valueOf(dcm.getS(DDict.dWindowWidth)).floatValue();
			window = (int) Math.round(Math.floor(fwindow));
		} else {
			window = window_max;
		}
		if (numberOfBits > 8) {
			max_pixel = 0;
			min_pixel = 65535;
			int c;
			for (int i = 0; i < short_pixels.length; i++) {
				c = (int) short_pixels[i] & 0xFFFF;
				if (c > max_pixel) {
					max_pixel = c;
				}
				if (c < min_pixel) {
					min_pixel = c;
				}
			}
			window_max = max_pixel - min_pixel;
			center_max = max_pixel;
			center_min = min_pixel;
			window = window_max;
			center = center_min + (center_max - center_min) / 2;
		}
	}

	private void createLUT(int window, int center) {
		//convert to 8bit window and center values
		int window_8bit = window * 256 / (window_max);
		int center_8bit = (center - center_min) * 256
				/ (center_max - center_min);
		int i, value;
		int t1 = Math.max(center_8bit - window_8bit / 2, 0);
		int t2 = Math.min(center_8bit + window_8bit / 2, 256);
		for (i = 0; i < t1; i++) {
			gray[i] = 0;
		}
		for (; i < t2; i++) {
			gray[i] = (byte) (((i - t1) << 8) / (t2 - t1));
		}
		for (; i < gray.length; i++) {
			gray[i] = (byte) 0xFF;
		}
	}

	public BufferedImage calculate8BitFrom16Bit() {
		byte[] lut = new byte[1 << numberOfBits];
		int t1 = Math.max(center - window / 2, 0);
		int t2 = Math.min(center + window / 2, lut.length);
		int i;
		int value;
		for (i = 0; i < t1; i++) {
			lut[i] = 0;
		}
		for (i = t1; i < t2; i++) {
			lut[i] = (byte) (((i - t1) << 8) / window);
		}
		for (i = t2; i < lut.length; i++) {
			lut[i] = (byte) 0xFF;
		}
		byte_pixels = new byte[short_pixels.length];
		int a, b, c;
		int bitMask = (1 << numberOfBits) - 1;
		for (i = 0; i < byte_pixels.length; i++) {
			byte_pixels[i] = lut[(int) short_pixels[i] & 0xFFFF];
		}
		int bandOffsets[] = { 0 };
		DataBuffer dataBuffer = new DataBufferByte(byte_pixels,
				byte_pixels.length);
		wr = Raster.createInterleavedRaster(dataBuffer, width, height, width,
				1, bandOffsets, null);
		createLUT(window, center);
		return new BufferedImage(new IndexColorModel(8, (1 << 8), gray, gray,
				gray), wr, false, null);
	}

	private byte[] getPalette(int paldata, int paldesc) throws DicomException,
			IllegalValueException {
		int palentry;
		int entries = dcm.getI(paldesc, 0);
		int firstpixel = dcm.getI(paldesc, 1);
		int bits = dcm.getI(paldesc, 2);
		byte[] dicompal = (byte[]) dcm.get(paldata);
		int firstvalue = ((int) dicompal[0] & 0xFF)
				+ (((int) dicompal[1] & 0xFF) << 8);
		int lastvalue = ((int) dicompal[dicompal.length - 2] & 0xFF)
				+ (((int) dicompal[dicompal.length - 1] & 0xFF) << 8);
		if (bitsAllocated == 8) {
			byte[] pal = new byte[256];
			for (int j = 0; j < firstpixel; j++) {
				pal[j] = (byte) (firstvalue >> 8);
			}
			for (int j = 0; j < entries; j++) {
				palentry = ((int) dicompal[2 * j] & 0xFF)
						+ (((int) dicompal[2 * j + 1] & 0xFF) << 8);
				pal[firstpixel + j] = (byte) (palentry >> 8);
			}
			for (int j = firstpixel + entries; j < pal.length; j++) {
				pal[j] = (byte) (lastvalue >> 8);
			}
			return pal;
		} else {
			throw new IllegalValueException(
					"unsupported Bits Allocated value for palette color image: "
							+ bitsAllocated);
		}
	}

	private static final int COLOR_BY_PIXEL = 0;

	private static final int COLOR_BY_PLANE = 1;

	private static final int PR_UNSIGNEDINT = 0;

	private static final int PR_TWOSCOMPLEMENT = 1;

	public static final int MONOCHROME1 = 0;

	public static final int MONOCHROME2 = 1;

	private static final int PALETTE_COLOR = 2;

	private static final int RGB = 3;

	private static final int HSV = 4;

	private static final int ARGB = 5;

	private static final int CMYK = 6;

	private static final int YBR_FULL = 7;

	private static final int YBR_FULL_422 = 8;

	private static final int YBR_PARTIAL_422 = 9;

	private DicomObject dcm;

	private int width;

	private int height;

	private int samplesPerPixel;

	private int pixelRepresentation;

	private int photometricInterpretation;

	private int bitsAllocated;

	private int numberOfBits;

	private int highBit;

	private int planarConfiguration;

	private int transferSyntax;

	private int frames;

	private int bitMask;

	private byte[] pixeldata;

	private byte[] byte_pixels;

	private short[] short_pixels;

	private int[] int_pixels;

	private WritableRaster wr;

	private byte[] red;

	private byte[] green;

	private byte[] blue;

	private byte[] gray;

	private int window_max;

	private int center_max;

	private int center_min;

	private int window;

	private int center;

	private int max_pixel;

	private int min_pixel;
}
//JAVA2D bugs prevent this from working
//gray = new byte[1<<12];
//short[] short_pixels = (short[])ds.getReader().readNext();
//int bandOffsets[] = {0};
//DataBuffer dataBuffer = new
// DataBufferUShort(short_pixels,short_pixels.length);
//WritableRaster wr =
// Raster.createInterleavedRaster(dataBuffer,ds.getWidth(),ds.getHeight(),ds.getWidth(),
// 1,bandOffsets, null);
//createLUT(gray,window,center);
//IndexColorModel model = new IndexColorModel(12,(1<<12),gray,gray,gray);
//image = new BufferedImage(model,wr, false, null);
//imagePanel.setImage(image);
