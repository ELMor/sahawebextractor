
package com.archimed.dicom.examples;

import java.lang.reflect.Field;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.Vector;
import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.Jdt;
import com.archimed.dicom.SOPClass;
import com.archimed.dicom.TransferSyntax;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.DimseUtil;
import com.archimed.dicom.network.Reject;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.tool.StringTool;

class VerifySCU {
	public final static int VERIFY_OK = 0;

	public final static int VERIFY_NOT_OK = 1;

	private String host;

	private int port;

	private String called = "ANY";

	private String calling = "SOFTLINK";

	private int maxpdu = 0;

	private boolean verbose = false;

	private int numpc = 0;

	private boolean debug = false;

	private int[] tsids = { TransferSyntax.ImplicitVRLittleEndian };

	private int preftsid = TransferSyntax.ImplicitVRLittleEndian;

	private boolean combts = false;

	private Vector usablePCIndices = new Vector();

	private Association as;

	private Request request;

	private Acknowledge ack;

	boolean isVerbose() {
		return verbose;
	}

	boolean associate() throws Exception {
		if (debug)
			Debug.DEBUG = 9;
		// connect and create an association object
		Socket s = new Socket(host, port);
		as = new Association(s.getInputStream(), s.getOutputStream());
		// prepare a request object with called title,calling title,
		// abstract syntax and transfer syntax
		request = new Request();
		request.setCalledTitle(called);
		request.setCallingTitle(calling);
		request.setMaxPduSize(maxpdu);
		if (combts)
			request.addPresentationContext(SOPClass.Verification, tsids);
		else {
			for (int i = 0; i < tsids.length; i++)
				request.addPresentationContext(SOPClass.Verification,
						new int[] { tsids[i] });
		}
		Class cl = SOPClass.class;
		Field[] fields = cl.getDeclaredFields();
		int n = 0;
		int i = 0;
		int j = 0;
		while (n < numpc) {
			if (((Integer) fields[i % fields.length].get(null)).intValue() == SOPClass.Verification) {
				i++;
				continue;
			}
			if (combts) {
				request.addPresentationContext(((Integer) fields[i
						% fields.length].get(null)).intValue(), tsids);
				i++;
			} else {
				request.addPresentationContext(((Integer) fields[i
						% fields.length].get(null)).intValue(),
						new int[] { tsids[j++] });
				if (j == tsids.length) {
					i++;
					j = 0;
				}
			}
			n++;
		}
		//print information
		if (verbose) {
			System.out.println();
			System.out.println(request);
		}
		// send the request and receive the response
		as.sendAssociateRequest(request);
		Response response = as.receiveAssociateResponse();
		if (verbose) {
			System.out.println();
			System.out.println(response);
		}
		// analyse the response
		// this is always an Acknowledge,Reject or Abort
		if (response instanceof Acknowledge) {
			ack = (Acknowledge) response;
			for (i = 0; i < request.getPresentationContexts(); i++) {
				if (request.getAbstractSyntax(i).getConstant() != SOPClass.Verification)
					continue;
				for (j = 0; j < ack.getPresentationContexts(); j++) {
					if (request.getID(i) == ack.getID(j)) {
						if (ack.getResult(j) == Acknowledge.ACCEPTANCE)
							usablePCIndices.addElement(new Integer(i));
					}
				}
			}
			return true;
		} else if (response instanceof Reject) {
			System.out.println("association rejected: " + response);
			return false;
		} else if (response instanceof Abort) {
			System.out.println("association aborted: " + response);
			return false;
		}
		return false;
	}

	boolean exchangeEcho() throws Exception {
		//initialize the presentation context in which to send the C-ECHO
		//with the first usable presentation context
		int pcid = request.getID(((Integer) usablePCIndices.firstElement())
				.intValue());
		//determine presentation context in which to send C-ECHO
		//taking into acccount the preferred transfer syntax
		loop: for (int i = 0; i < usablePCIndices.size(); i++) {
			for (int j = 0; j < ack.getPresentationContexts(); j++) {
				if (ack.getID(j) == request.getID(((Integer) usablePCIndices
						.elementAt(i)).intValue())) {
					if (ack.getTransferSyntax(j).getConstant() == preftsid) {
						pcid = ack.getID(j);
						break loop;
					}
				}
			}
		}
		if (verbose)
			System.out.println("using presentation context ID: " + pcid);
		// create a C-ECHO REQUEST, send it
		// and receive the response
		DicomObject echoreq = DimseUtil.createEchoRequest(new Integer(0),
				new Integer(SOPClass.Verification));
		if (verbose) {
			System.out.println();
			System.out
					.println("**************** C-ECHO COMMAND REQUEST ****************");
			echoreq.dumpVRs(System.out);
			System.out
					.println("********************************************************");
			System.out.println();
		}
		as.sendInPresentationContext(pcid, echoreq, null);
		DicomObject echores = as.receiveCommand();
		if (verbose) {
			System.out.println();
			System.out
					.println("*************** C-ECHO COMMAND RESPONSE ****************");
			echores.dumpVRs(System.out);
			System.out
					.println("********************************************************");
			System.out.println();
		}
		// check that echores is a correct C-ECHO RESPONSE
		if (DimseUtil.getCommandType(echores) != DimseUtil.C_ECHO_RESPONSE) {
			if (verbose)
				System.out.println("No C-ECHO RESPONSE command field : "
						+ DimseUtil.getCommandType(echores));
			as.sendAbort(Abort.DICOM_UL_SERVICE_USER,
					Abort.REASON_NOT_SPECIFIED);
			return false;
		}
		String sopclass = echores.getS(DDict.dAffectedSOPClassUID);
		if (DimseUtil.getAffectedSOPClass(echores) != SOPClass.Verification) {
			if (verbose)
				System.out
						.println("Affected SOP Class UID in C-ECHO RESPONSE wrong: "
								+ DimseUtil.getAffectedSOPClass(echores));
			as.sendAbort(Abort.DICOM_UL_SERVICE_USER,
					Abort.REASON_NOT_SPECIFIED);
			return false;
		}
		if (DimseUtil.getMessageIDBeingRespondedTo(echores) != 0) {
			if (verbose)
				System.out.println("Message ID in C-ECHO RESPONSE wrong: "
						+ DimseUtil.getMessageID(echores));
			as.sendAbort(Abort.DICOM_UL_SERVICE_USER,
					Abort.REASON_NOT_SPECIFIED);
			return false;
		}
		int status = echores.getI(DDict.dStatus);
		if (status != 0) {
			if (verbose)
				System.out.println("Non succesful status in C-ECHO RESPONSE: "
						+ status);
			as.sendAbort(Abort.DICOM_UL_SERVICE_USER,
					Abort.REASON_NOT_SPECIFIED);
			return false;
		}
		return true;
	}

	boolean release() throws Exception {
		// release the association
		if (verbose)
			System.out.println("sending release request");
		as.sendReleaseRequest();
		as.receiveReleaseResponse();
		if (verbose)
			System.out.println("received release response");
		return true;
	}

	void processOptions(String[] args) throws Exception {
		String arg;
		host = args[args.length - 2];
		try {
			port = Integer.parseInt(args[args.length - 1]);
		} catch (NumberFormatException ex) {
			throw new Exception("Illegal value for port: " + ex);
		}
		args[args.length - 1] = null;
		args[args.length - 2] = null;
		for (int i = 0; i < args.length - 2; i++) {
			arg = args[i];
			if (arg.equals("-cld")) {
				this.called = nextArg(args, i);
				i++;
			} else if (arg.equals("-cli")) {
				this.calling = nextArg(args, i);
				i++;
			} else if (arg.equals("-v"))
				verbose = true;
			else if (arg.equals("-numpc")) {
				try {
					this.numpc = Integer.parseInt(nextArg(args, i));
					i++;
				} catch (NumberFormatException ex) {
					throw new Exception("Illegal value for -numpc: " + ex);
				}
			} else if (arg.equals("-d"))
				debug = true;
			else if (arg.equals("-combts"))
				combts = true;
			else if (arg.equals("-propts")) {
				parseTS(nextArg(args, i));
				i++;
			} else if (arg.equals("-prefts")) {
				String pref = nextArg(args, i);
				i++;
				if (pref.equals("IL"))
					preftsid = TransferSyntax.ImplicitVRLittleEndian;
				else if (pref.equals("EL"))
					preftsid = TransferSyntax.ExplicitVRLittleEndian;
				else if (pref.equals("EB"))
					preftsid = TransferSyntax.ExplicitVRBigEndian;
				else
					throw new Exception("unknown preferred transfer syntax: "
							+ pref);
			} else if (arg.equals("-maxpdu")) {
				try {
					this.maxpdu = Integer.parseInt(nextArg(args, i));
					i++;
				} catch (NumberFormatException ex) {
					throw new Exception("Illegal value for -maxpdu: " + ex);
				}
			} else
				throw new Exception("unknown option: " + arg);
		}
	}

	private void parseTS(String arg) throws Exception {
		StringTokenizer tkz = new StringTokenizer(arg, "-");
		tsids = new int[tkz.countTokens()];
		String token;
		int i = 0;
		while (tkz.hasMoreTokens()) {
			token = tkz.nextToken();
			if (token.equals("IL"))
				tsids[i++] = TransferSyntax.ImplicitVRLittleEndian;
			else if (token.equals("EL"))
				tsids[i++] = TransferSyntax.ExplicitVRLittleEndian;
			else if (token.equals("EB"))
				tsids[i++] = TransferSyntax.ExplicitVRBigEndian;
			else
				throw new Exception("unknown specified transfer syntax: "
						+ token);
		}
	}

	private String nextArg(String[] args, int index) throws Exception {
		if ((args.length <= (index + 1)) || (args[index + 1] == null))
			throw new Exception("missing parameter after " + args[index]);
		else
			return args[index + 1];
	}

	static void printUsage() {
		int w = 30;
		System.out.println("usage: verifyscu [options] <host> <port>");
		System.out.println();
		System.out.println("parameters:");
		System.out.println(StringTool.fstr("   host", w)
				+ "host or ip-address of the peer");
		System.out.println(StringTool.fstr("   port", w)
				+ "tcp/ip port number of the peer");
		System.out.println();
		System.out.println("options:");
		System.out.println(StringTool.fstr("   -cld <title>", w)
				+ "called AE title (default: ANY)");
		System.out.println(StringTool.fstr("   -cli <title>", w)
				+ "calling AE title (default: SOFTLINK)");
		System.out.println(StringTool.fstr("   -maxpdu <pdusize>", w)
				+ "maximum pdu size that sender accepts");
		System.out.println(StringTool.fstr("", w)
				+ "(default: 0 (means no limit))");
		System.out.println(StringTool.fstr("   -v", w) + "verbose");
		System.out.println(StringTool.fstr("   -d", w)
				+ "print JDT debug information");
		System.out.println(StringTool.fstr("   -numpc <number>", w)
				+ "number of additional proposed presentation contexts");
		System.out.println(StringTool.fstr("", w)
				+ "(default only the required verification sop class)");
		System.out.println(StringTool.fstr("   -propts <list>", w)
				+ "propose the specified transfer syntaxes in specified order");
		System.out.println(StringTool.fstr("", w)
				+ "(default: Implicit Little Endian)");
		System.out.println(StringTool.fstr("", w + 5)
				+ "IL: Implicit Little Endian");
		System.out.println(StringTool.fstr("", w + 5)
				+ "EL: Explicit Little Endian");
		System.out.println(StringTool.fstr("", w + 5)
				+ "EB: Explicit Big Endian");
		System.out
				.println(StringTool.fstr("", w + 5)
						+ "transfer syntaxes must be separated with a minus sign, eg: EB-IL-EL");
		System.out
				.println(StringTool.fstr("   -prefts <transfer syntax>", w)
						+ "the preferred transfer syntax when multiple transfer syntaxes are accepted");
		System.out.println(StringTool.fstr("", w)
				+ "(default: Implicit Little Endian)");
		System.out.println(StringTool.fstr("", w + 5)
				+ "IL: Implicit Little Endian");
		System.out.println(StringTool.fstr("", w + 5)
				+ "EL: Explicit Little Endian");
		System.out.println(StringTool.fstr("", w + 5)
				+ "EB: Explicit Big Endian");
		System.out
				.println(StringTool.fstr("   -combts", w)
						+ "combine specified transfer syntaxes in one presentation context");
		System.out
				.println(StringTool.fstr("", w)
						+ "(only in combination with -propts,default separate contexts are proposed)");
	}

	public static void main(String[] args) {
		System.out.println();
		System.out
				.println("Softlink DICOM Verification SCU (C-ECHO), JDT version: "
						+ Jdt.MAJOR_VERSION + "." + Jdt.MINOR_VERSION);
		if (args.length < 2) {
			printUsage();
			System.exit(-1);
		}
		VerifySCU verifyscu = new VerifySCU();
		try {
			verifyscu.processOptions(args);
			if (!verifyscu.associate())
				System.exit(-1);
			if (!verifyscu.exchangeEcho())
				System.exit(-1);
			if (!verifyscu.release())
				System.exit(-1);
			System.out.println("verification successful");
			System.exit(0);
		} catch (Throwable e) {
			System.out.println(e);
			if (verifyscu.isVerbose())
				e.printStackTrace();
		}
	}
}