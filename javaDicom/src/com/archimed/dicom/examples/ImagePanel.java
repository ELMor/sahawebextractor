
package com.archimed.dicom.examples;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

class ImagePanel extends JPanel {
	BufferedImage image;

	private float scalefactor = 1.0f;

	public Dimension getPreferredSize() {
		if (image != null)
			return (new Dimension((int) (scalefactor * image.getWidth(this)),
					(int) (scalefactor * image.getHeight(this))));
		else
			return (super.getPreferredSize());
	}

	void scale(float scalefactor) {
		this.scalefactor = scalefactor;
		setSize(getPreferredSize());
		repaint();
	}

	public void fit(Dimension d) {
		int client_x = d.width;
		int client_y = d.height;
		int image_width = image.getWidth(this);
		int image_height = image.getHeight(this);
		float target_x;
		float target_y;
		if (((float) image_width / (float) client_x) > ((float) image_height / (float) client_y))
			scale(1.0f / ((float) image_width / (float) client_x));
		else
			scale(1.0f / ((float) image_height / (float) client_y));
	}

	void setImage(BufferedImage image) {
		this.image = image;
		scaled_image = image;
		setSize(getPreferredSize());
		repaint();
	}

	public void update(Graphics g) {
		paint(g);
	}

	Image scaled_image;

	public void paint(Graphics g) {
		if (image != null)
			g.drawImage(image, 0, 0,
					(int) (scalefactor * image.getWidth(this)),
					(int) (scalefactor * image.getHeight(this)), null);
	}
}