
package com.archimed.dicom.examples;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TransferSyntax;

public class DicomConvert {
	boolean file = true;

	boolean gl = true;

	boolean su = true;

	int ts = TransferSyntax.ImplicitVRLittleEndian;

	void parseArgs(String[] args) {
		for (int i = 0; i < (args.length - 2); i++) {
			if (args[i].equals("-f"))
				file = false;
			else if (args[i].equals("+f"))
				file = true;
			else if (args[i].equals("+ti"))
				ts = TransferSyntax.ImplicitVRLittleEndian;
			else if (args[i].equals("+tb"))
				ts = TransferSyntax.ExplicitVRBigEndian;
			else if (args[i].equals("+te"))
				ts = TransferSyntax.ExplicitVRLittleEndian;
			else if (args[i].equals("+g"))
				gl = true;
			else if (args[i].equals("-g"))
				gl = false;
			else if (args[i].equals("+s"))
				su = true;
			else if (args[i].equals("-s"))
				su = false;
		}
	}

	void write(DicomObject dcm, OutputStream out) throws DicomException,
			IOException {
		dcm.write(out, file, ts, su);
	}

	String usage() {
		return new String("Usage: DicomConvert <options> infile outfile\n"
				+ "Options:\n" + "\t-f\t Write without metaheader\n"
				+ "\t+f\t Write with metaheader\n"
				+ "\t+ti\t Use tranfer syntax Implicit VR Little Endian\n"
				+ "\t+te\t Use tranfer syntax Explicit VR Little Endian\n"
				+ "\t+tb\t Use tranfer syntax Explicit VR Big Endian\n"
				+ "\t-g\t Write without group lengths\n"
				+ "\t+g\t Write with group lengths\n"
				+ "\t+s\t Encode sequences using undefined lengths\n"
				+ "\t-s\t Encode sequences using explicit lengths\n");
	}

	public static void main(String[] args) {
		DicomConvert dc = new DicomConvert();
		if (args.length < 2) {
			System.out.println(dc.usage());
			System.exit(0);
		}
		dc.parseArgs(args);
		try {
			DicomObject dcm = new DicomObject();
			dcm.read(new FileInputStream(args[args.length - 2]));
			dc.write(dcm, new FileOutputStream(args[args.length - 1]));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}