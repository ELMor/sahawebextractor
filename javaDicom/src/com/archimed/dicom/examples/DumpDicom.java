
package com.archimed.dicom.examples;

import java.io.File;
import java.io.FileInputStream;
import com.archimed.dicom.DicomObject;

public class DumpDicom {
	public static void main(String args[]) {
		com.archimed.dicom.Debug.DEBUG = 9;
		if (args.length != 1) {
			System.out.println("usage: DumpDicom <dicomfile>");
			return;
		}
		try {
			File f = new File(args[0]);
			if (f.exists() == false) {
				System.out.println("file '" + args[0] + "' not found");
			}
			FileInputStream fin = new FileInputStream(f);
			DicomObject dcm = new DicomObject();
			dcm.read(fin, true);
			fin.close();
			dcm.dumpVRs(System.out, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}