
package com.archimed.dicom.examples;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.Jdt;
import com.archimed.dicom.SOPClass;
import com.archimed.dicom.TransferSyntax;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.DimseUtil;
import com.archimed.dicom.network.Reject;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;
import com.archimed.tool.StringTool;

/**
 * This class implements a simple command-line Dicom SCP, supporting
 * verification (C-ECHO) and storage (C-STORE). Execute it without arguments to
 * see the possible parameters and options.
 * 
 * @author Geert De Laet
 * @version 1.0
 */
public class DicomServer extends Thread {
	private int port;

	private String title = "SOFTLINKSVR";

	private int maxpdu;

	private boolean verbose = false;

	private boolean debug = false;

	private int preftsid = TransferSyntax.ImplicitVRLittleEndian;

	private boolean refcld = false;

	private boolean onlyecho = false;

	private boolean nosave = false;

	private int[] abstractsyntaxes = new int[] { SOPClass.Verification };

	private String[] callingTitles = null;

	private String dir = "images";

	private ServerSocket serversocket;

	boolean isVerbose() {
		return verbose;
	}

	public void run() {
		Socket s;
		ServerSocket serversocket = null;
		//initialize savedir
		if (!nosave)
			initSaveDir();
		//initialize the array of acceptable abstract syntaxes
		if (!onlyecho) {
			try {
				Vector v = new Vector();
				Class cl = SOPClass.class;
				Field[] fields = cl.getDeclaredFields();
				for (int i = 0; i < fields.length; i++) {
					if (SOPClass.getUIDEntry(
							((Integer) fields[i].get(null)).intValue())
							.getName().indexOf("Storage SOP Class") != -1)
						v.addElement(fields[i].get(null));
				}
				abstractsyntaxes = new int[v.size() + 1];
				abstractsyntaxes[0] = SOPClass.Verification;
				for (int i = 1; i < abstractsyntaxes.length; i++) {
					abstractsyntaxes[i] = ((Integer) v.elementAt(i - 1))
							.intValue();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				abstractsyntaxes = new int[] { SOPClass.Verification };
			}
		}
		//start a serversocket
		try {
			if (verbose)
				System.out.println("opening server socket on port " + port);
			serversocket = new ServerSocket(port);
		} catch (IOException ex) {
			System.out.println(ex);
			if (verbose)
				ex.printStackTrace();
			return;
		}
		if (verbose)
			System.out.println("listening for incoming connections");
		while (true) {
			try {
				s = serversocket.accept();
				(new Connection(title, s)).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	void printUsage() {
		int w = 35;
		System.out
				.println("usage: java com.archimed.dicom.examples.DicomServer [options] <port>");
		System.out.println();
		System.out.println("parameters:");
		System.out.println(StringTool.fstr("   port", w)
				+ "tcp/ip port number on which to listen");
		System.out.println();
		System.out.println("options:");
		System.out.println(StringTool.fstr("   -title <title>", w)
				+ "our AE title (default: " + title + ")");
		System.out.println(StringTool.fstr("   -maxpdu <pdusize>", w)
				+ "maximum pdu size that receiver accepts");
		System.out.println(StringTool.fstr("", w)
				+ "(default: 0 (means no limit))");
		System.out.println(StringTool.fstr("   -v", w) + "verbose");
		System.out.println(StringTool.fstr("   -d", w)
				+ "print JDT debug information");
		System.out.println(StringTool.fstr("   -nosave", w)
				+ "don't save datasets on disk");
		System.out.println(StringTool.fstr("   -dir <dir>", w)
				+ "alternative directory for saving datasets");
		System.out.println(StringTool.fstr("", w)
				+ "(default: subdirectory 'images' of current directory)");
		System.out.println(StringTool.fstr("   -onlyecho", w)
				+ "accept only the verification sop class");
		System.out.println(StringTool.fstr("", w)
				+ "(default: verification and storage sop classes)");
		System.out.println(StringTool.fstr("   -refcld", w)
				+ "refuse wrong called title in request");
		System.out.println(StringTool.fstr("   -onlycli <calling title>", w)
				+ "accept only this calling title in request");
		System.out.println(StringTool.fstr("   -onlyts <transfer syntax>", w)
				+ "accept only the specified transfer syntax");
		System.out.println(StringTool.fstr("", w) + "(default: all)");
		System.out.println(StringTool.fstr("", w + 5)
				+ "IL: Implicit Little Endian");
		System.out.println(StringTool.fstr("", w + 5)
				+ "EL: Explicit Little Endian");
		System.out.println(StringTool.fstr("", w + 5)
				+ "EB: Explicit Big Endian");
	}

	void processOptions(String[] args) throws Exception {
		String arg;
		try {
			port = Integer.parseInt(args[args.length - 1]);
		} catch (NumberFormatException ex) {
			throw new Exception("Illegal value for port: " + ex);
		}
		args[args.length - 1] = null;
		for (int i = 0; i < args.length - 1; i++) {
			arg = args[i];
			if (arg.equals("-title")) {
				title = nextArg(args, i);
				i++;
			} else if (arg.equals("-dir")) {
				dir = nextArg(args, i);
				i++;
			} else if (arg.equals("-onlycli")) {
				callingTitles = new String[] { nextArg(args, i) };
				i++;
			} else if (arg.equals("-v"))
				verbose = true;
			else if (arg.equals("-d"))
				debug = true;
			else if (arg.equals("-refcld"))
				refcld = true;
			else if (arg.equals("-onlyecho"))
				onlyecho = true;
			else if (arg.equals("-nosave"))
				nosave = true;
			else if (arg.equals("-onlyts")) {
				String pref = nextArg(args, i);
				i++;
				if (pref.equals("IL"))
					preftsid = TransferSyntax.ImplicitVRLittleEndian;
				else if (pref.equals("EL"))
					preftsid = TransferSyntax.ExplicitVRLittleEndian;
				else if (pref.equals("EB"))
					preftsid = TransferSyntax.ExplicitVRBigEndian;
				else
					throw new Exception("unknown preferred transfer syntax: "
							+ pref);
			} else if (arg.equals("-maxpdu")) {
				try {
					this.maxpdu = Integer.parseInt(nextArg(args, i));
					i++;
				} catch (NumberFormatException ex) {
					throw new Exception("Illegal value for -maxpdu: " + ex);
				}
			} else
				throw new Exception("unknown option: " + arg);
		}
	}

	private String nextArg(String[] args, int index) throws Exception {
		if ((args.length <= (index + 1)) || (args[index + 1] == null))
			throw new Exception("missing parameter after " + args[index]);
		else
			return args[index + 1];
	}

	public static void main(String args[]) {
		System.out.println();
		System.out
				.println("Softlink DICOM Verification SCP (C-ECHO), JDT version: "
						+ Jdt.MAJOR_VERSION + "." + Jdt.MINOR_VERSION);
		DicomServer dicomServer = new DicomServer();
		if (args.length < 1) {
			dicomServer.printUsage();
			System.exit(-1);
		}
		try {
			dicomServer.processOptions(args);
			dicomServer.start();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			if (dicomServer.isVerbose())
				e.printStackTrace();
		}
	}

	class Connection extends Thread {
		Socket s;

		String title;

		public Connection(String title, Socket s) {
			this.s = s;
			this.title = title;
		}

		public void run() {
			try {
				InputStream in = s.getInputStream();
				OutputStream out = s.getOutputStream();
				Association as = new Association(in, out);
				Request request = as.receiveAssociateRequest();
				//print information
				if (verbose) {
					System.out.println();
					System.out.println("incoming associate request:");
					System.out.println(request);
				}
				//int[] abstractsyntaxes = {SOPClass.Verification};
				String t = null;
				if (refcld)
					t = title;
				Response response = ResponsePolicy.prepareResponse(request, t,
						callingTitles, abstractsyntaxes, preftsid, false);
				if (verbose) {
					System.out.println();
					System.out.println("sending response:");
					System.out.println(response);
				}
				as.sendAssociateResponse(response);
				if (response instanceof Reject) {
					s.close();
					return;
				} else if (response instanceof Abort) {
					s.close();
					return;
				}
				Acknowledge ack = (Acknowledge) response;
				int result;
				DicomObject commandreq;
				DicomObject commandres;
				while (true) {
					result = as.peek();
					if (result == Association.RELEASE_REQUEST) {
						as.receiveReleaseRequest();
						if (verbose)
							System.out.println("received release request");
						as.sendReleaseResponse();
						if (verbose)
							System.out.println("sent release response");
						s.close();
						return;
					} else if (result == Association.ABORT) {
						Abort abort = as.receiveAbort();
						if (verbose) {
							System.out.println("association aborted by peer");
							System.out.println(abort);
						}
						s.close();
						return;
					} else if (result != Association.PDATA_PDU) {
						System.out
								.println("unexpected message received from peer");
						s.close();
						return;
					}
					commandreq = as.receiveCommand();
					if (verbose) {
						System.out.println();
						System.out
								.println("****************** RECEIVED COMMAND ********************");
						commandreq.dumpVRs(System.out);
						System.out
								.println("********************************************************");
					}
					if (verbose) {
						System.out.println("received commandtype: "
								+ DimseUtil.getCommandName(DimseUtil
										.getCommandType(commandreq)));
						System.out.println("current presentation context id: "
								+ as.getCurrentPresentationContext());
					}
					if (DimseUtil.getCommandType(commandreq) == DimseUtil.C_ECHO_REQUEST) {
						Integer messageid = (Integer) commandreq
								.get(DDict.dMessageID);
						commandres = DimseUtil.createEchoResponse(messageid,
								new Integer(SOPClass.Verification),
								new Integer(0));
						if (verbose) {
							System.out
									.println("******************* SENDING COMMAND ********************");
							commandres.dumpVRs(System.out);
							System.out
									.println("********************************************************");
						}
						as.sendInPresentationContext(as
								.getCurrentPresentationContext(), commandres,
								null);
					} else if (DimseUtil.getCommandType(commandreq) == DimseUtil.C_STORE_REQUEST) {
						DicomObject datareq = as.receiveData();
						if (verbose) {
							System.out.println();
							System.out
									.println("******************* RECEIVED DATA **********************");
							datareq.dumpVRs(System.out);
							System.out
									.println("********************************************************");
						}
						Integer messageid = new Integer(DimseUtil
								.getMessageID(commandreq));
						Integer sopclass = new Integer(DimseUtil
								.getAffectedSOPClass(commandreq));
						String sopinstance = DimseUtil
								.getAffectedSOPInstance(commandreq);
						int status = 0;
						if (!nosave) {
							try {
								saveDataSet(datareq);
							} catch (Exception ex) {
								status = 0xA700;
								System.out.println(ex);
								if (verbose)
									ex.printStackTrace();
							}
						}
						commandres = DimseUtil.createStoreResponse(messageid,
								sopclass, sopinstance, new Integer(status));
						if (verbose) {
							System.out
									.println("******************* SENDING COMMAND ********************");
							commandres.dumpVRs(System.out);
							System.out
									.println("********************************************************");
						}
						as.sendInPresentationContext(as
								.getCurrentPresentationContext(), commandres,
								null);
					} else {
						if (verbose)
							System.out
									.println("unexpected command received: ,aborting");
						as.sendAbort(Abort.DICOM_UL_SERVICE_USER,
								Abort.REASON_NOT_SPECIFIED);
						s.close();
						return;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalValueException e) {
				e.printStackTrace();
			} catch (DicomException e) {
				e.printStackTrace();
			} catch (UnknownUIDException e) {
				e.printStackTrace();
			}
		}
	}

	private void initSaveDir() {
		try {
			File f = new File(dir);
			if (!f.exists() || !f.isDirectory())
				if (!f.mkdir()) {
					System.out.println("cannot create directory '"
							+ f.getAbsolutePath() + ",saving disabled");
					nosave = true;
				}
		} catch (Throwable ex) {
			System.out
					.println("exception when initializing savedir, saving disabled");
			nosave = true;
			if (verbose)
				ex.printStackTrace();
		}
	}

	private void saveDataSet(DicomObject dcm) throws Exception {
		File f = new File(dir);
		String s = dcm.getS(DDict.dSOPInstanceUID);
		File dcmFile = new File(f, s + ".dcm");
		int i = 1;
		while (dcmFile.exists()) {
			dcmFile = new File(f, s + "_" + i + ".dcm");
			i++;
		}
		FileOutputStream fout = new FileOutputStream(dcmFile);
		dcm.write(fout, false);
		fout.close();
		System.out.println("dataset saved to '" + dcmFile.getAbsolutePath()
				+ "'");
	}
}