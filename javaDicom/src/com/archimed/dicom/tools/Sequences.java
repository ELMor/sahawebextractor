/*
 * Sequences - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.tools;

import java.util.Vector;

import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class Sequences {
	DicomObject dcm;

	public Sequences(DicomObject dicomobject) {
		dcm = dicomobject;
	}

	public int getSize(int i, int i_0_, int i_1_) throws DicomException {
		return getSize_ge(i, i_0_, DDict.getGroup(i_1_), DDict.getElement(i_1_));
	}

	public int getSize_ge(int i, int i_2_, int i_3_, int i_4_)
			throws DicomException {
		if (DDict.getTypeCode(i) != 10)
			throw new DicomException(i, "Data element not of type SQ");
		Object object = dcm.get(i, i_2_);
		if (object == null)
			return -1;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.getSize_ge(i_3_, i_4_);
	}

	public Vector deleteItem(int i, int i_5_, int i_6_) throws DicomException {
		return deleteItem_ge(i, i_5_, DDict.getGroup(i_6_), DDict
				.getElement(i_6_));
	}

	public Vector deleteItem_ge(int i, int i_7_, int i_8_, int i_9_)
			throws DicomException {
		if (DDict.getTypeCode(i) != 10)
			throw new DicomException(i, "Data element not of type SQ");
		Object object = dcm.get(i, i_7_);
		if (object == null)
			return null;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.deleteItem_ge(i_8_, i_9_);
	}

	public Object deleteItem(int i, int i_10_, int i_11_, int i_12_)
			throws DicomException {
		return deleteItem_ge(i, i_10_, DDict.getGroup(i_11_), DDict
				.getElement(i_11_), i_12_);
	}

	public Object deleteItem_ge(int i, int i_13_, int i_14_, int i_15_,
			int i_16_) throws DicomException {
		if (DDict.getTypeCode(i) != 10)
			throw new DicomException("Data element 'seq' not of type SQ");
		Object object = dcm.get(i, i_13_);
		if (object == null)
			return null;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.deleteItem_ge(i_14_, i_15_, i_16_);
	}

	public void set(int i, int i_17_, int i_18_, Object object)
			throws DicomException {
		set_ge(i, i_17_, DDict.getGroup(i_18_), DDict.getElement(i_18_), object);
	}

	public void set_ge(int i, int i_19_, int i_20_, int i_21_, Object object)
			throws DicomException {
		boolean bool = true;
		if (DDict.getTypeCode(i) != 10)
			throw new DicomException(i, "Data element not of type SQ");
		Object object_22_ = dcm.get(i, i_19_);
		DicomObject dicomobject;
		if (object_22_ == null)
			dicomobject = new DicomObject();
		else {
			dicomobject = (DicomObject) object_22_;
			bool = false;
		}
		dicomobject.deleteItem_ge(i_20_, i_21_);
		dicomobject.set_ge(i_20_, i_21_, object, 0);
		if (bool)
			dcm.set(i, dicomobject, i_19_);
	}

	public void set(int i, int i_23_, int i_24_, Object object, int i_25_)
			throws DicomException {
		int i_26_ = DDict.getGroup(i_24_);
		int i_27_ = DDict.getElement(i_24_);
		set_ge(i, i_23_, i_26_, i_27_, object, i_25_);
	}

	public void set_ge(int i, int i_28_, int i_29_, int i_30_, Object object,
			int i_31_) throws DicomException {
		boolean bool = true;
		if (DDict.getTypeCode(i) != 10)
			throw new DicomException(i, "Data element not of type SQ");
		Object object_32_ = dcm.get(i, i_28_);
		DicomObject dicomobject;
		if (object_32_ == null)
			dicomobject = new DicomObject();
		else {
			dicomobject = (DicomObject) object_32_;
			bool = false;
		}
		dicomobject.set_ge(i_29_, i_30_, object, i_31_);
		if (bool)
			dcm.set(i, dicomobject, i_28_);
	}

	public void append(int i, int i_33_, int i_34_, Object object)
			throws DicomException {
		append_ge(i, i_33_, DDict.getGroup(i_34_), DDict.getElement(i_34_),
				object);
	}

	public void append_ge(int i, int i_35_, int i_36_, int i_37_, Object object)
			throws DicomException {
		int i_38_ = getSize_ge(i, i_35_, i_36_, i_37_);
		if (i_38_ == -1)
			i_38_ = 0;
		set_ge(i, i_35_, i_36_, i_37_, object, i_38_);
	}

	public Object get(int i, int i_39_, int i_40_) throws DicomException {
		return get(i, i_39_, i_40_, 0);
	}

	public Object get_ge(int i, int i_41_, int i_42_, int i_43_)
			throws DicomException {
		return get_ge(i, i_41_, i_42_, i_43_, 0);
	}

	public Object get(int i, int i_44_, int i_45_, int i_46_)
			throws DicomException {
		return get_ge(i, i_44_, DDict.getGroup(i_45_), DDict.getElement(i_45_),
				i_46_);
	}

	public Object get_ge(int i, int i_47_, int i_48_, int i_49_, int i_50_)
			throws DicomException {
		if (DDict.getTypeCode(i) != 10)
			throw new DicomException(i, "Data element not of type SQ");
		Object object = dcm.get(i, i_47_);
		if (object == null)
			return null;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.get_ge(i_48_, i_49_, i_50_);
	}

	public String getS(int i, int i_51_, int i_52_) throws DicomException {
		return getS(i, i_51_, i_52_, 0);
	}

	public String getS_ge(int i, int i_53_, int i_54_, int i_55_)
			throws DicomException {
		return getS_ge(i, i_53_, i_54_, i_55_, 0);
	}

	public String getS(int i, int i_56_, int i_57_, int i_58_)
			throws DicomException {
		return getS_ge(i, i_56_, DDict.getGroup(i_57_),
				DDict.getElement(i_57_), i_58_);
	}

	public String getS_ge(int i, int i_59_, int i_60_, int i_61_, int i_62_)
			throws DicomException {
		if (DDict.getTypeCode(i) != 10)
			throw new DicomException(i, "Data element not of type SQ");
		Object object = dcm.get(i, i_59_);
		if (object == null)
			return null;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.getS_ge(i_60_, i_61_, i_62_);
	}

	public int getI(int i, int i_63_, int i_64_) throws DicomException {
		return getI(i, i_63_, i_64_, 0);
	}

	public int getI_ge(int i, int i_65_, int i_66_, int i_67_)
			throws DicomException {
		return getI_ge(i, i_65_, i_66_, i_67_, 0);
	}

	public int getI(int i, int i_68_, int i_69_, int i_70_)
			throws DicomException {
		return getI_ge(i, i_68_, DDict.getGroup(i_69_),
				DDict.getElement(i_69_), i_70_);
	}

	public int getI_ge(int i, int i_71_, int i_72_, int i_73_, int i_74_)
			throws DicomException {
		if (DDict.getTypeCode(i) != 10)
			throw new DicomException(i, "Data element not of type SQ");
		Object object = dcm.get(i, i_71_);
		if (object == null)
			return 2147483647;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.getI_ge(i_72_, i_73_, i_74_);
	}

	public int getSize(int i, int i_75_, int i_76_, int i_77_, int i_78_)
			throws DicomException {
		return getSize_ge(i, i_75_, i_76_, i_77_, DDict.getGroup(i_78_), DDict
				.getElement(i_78_));
	}

	public int getSize_ge(int i, int i_79_, int i_80_, int i_81_, int i_82_,
			int i_83_) throws DicomException {
		if (DDict.getTypeCode(i_80_) != 10)
			throw new DicomException(i_80_, "Data element not of type SQ");
		Object object = get(i, i_81_, i_80_, i_81_);
		if (object == null)
			return -1;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.getSize_ge(i_82_, i_83_);
	}

	public Vector deleteItem(int i, int i_84_, int i_85_, int i_86_, int i_87_)
			throws DicomException {
		return deleteItem_ge(i, i_84_, i_85_, i_86_, DDict.getGroup(i_87_),
				DDict.getElement(i_87_));
	}

	public Vector deleteItem_ge(int i, int i_88_, int i_89_, int i_90_,
			int i_91_, int i_92_) throws DicomException {
		if (DDict.getTypeCode(i_89_) != 10)
			throw new DicomException(i_89_, "Data element not of type SQ");
		Object object = get(i, i_88_, i_89_, i_90_);
		if (object == null)
			return null;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.deleteItem_ge(i_91_, i_92_);
	}

	public Object deleteItem(int i, int i_93_, int i_94_, int i_95_, int i_96_,
			int i_97_) throws DicomException {
		return deleteItem_ge(i, i_93_, i_94_, i_95_, DDict.getGroup(i_96_),
				DDict.getElement(i_96_), i_97_);
	}

	public Object deleteItem_ge(int i, int i_98_, int i_99_, int i_100_,
			int i_101_, int i_102_, int i_103_) throws DicomException {
		if (DDict.getTypeCode(i_99_) != 10)
			throw new DicomException(i_99_, "Data element not of type SQ");
		Object object = get(i, i_98_, i_99_, i_100_);
		if (object == null)
			return null;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.deleteItem_ge(i_101_, i_102_, i_103_);
	}

	public void set(int i, int i_104_, int i_105_, int i_106_, int i_107_,
			Object object) throws DicomException {
		set_ge(i, i_104_, i_105_, i_106_, DDict.getGroup(i_107_), DDict
				.getElement(i_107_), object);
	}

	public void set_ge(int i, int i_108_, int i_109_, int i_110_, int i_111_,
			int i_112_, Object object) throws DicomException {
		boolean bool = true;
		if (DDict.getTypeCode(i_109_) != 10)
			throw new DicomException(i_109_, "Data element not of type SQ");
		Object object_113_ = get(i, i_108_, i_109_, i_110_);
		DicomObject dicomobject;
		if (object_113_ == null)
			dicomobject = new DicomObject();
		else {
			dicomobject = (DicomObject) object_113_;
			bool = false;
		}
		dicomobject.deleteItem_ge(i_111_, i_112_);
		dicomobject.set_ge(i_111_, i_112_, object, 0);
		if (bool)
			set(i, i_108_, i_109_, dicomobject, i_110_);
	}

	public void set(int i, int i_114_, int i_115_, int i_116_, int i_117_,
			Object object, int i_118_) throws DicomException {
		set_ge(i, i_114_, i_115_, i_116_, DDict.getGroup(i_117_), DDict
				.getElement(i_117_), object, i_118_);
	}

	public void set_ge(int i, int i_119_, int i_120_, int i_121_, int i_122_,
			int i_123_, Object object, int i_124_) throws DicomException {
		boolean bool = true;
		if (DDict.getTypeCode(i_120_) != 10)
			throw new DicomException(i_120_, "Data element not of type SQ");
		Object object_125_ = get(i, i_119_, i_120_, i_121_);
		DicomObject dicomobject;
		if (object_125_ == null)
			dicomobject = new DicomObject();
		else {
			dicomobject = (DicomObject) object_125_;
			bool = false;
		}
		dicomobject.set_ge(i_122_, i_123_, object, i_124_);
		if (bool)
			set(i, i_119_, i_120_, dicomobject, i_121_);
	}

	public void append(int i, int i_126_, int i_127_, int i_128_, int i_129_,
			Object object) throws DicomException {
		append_ge(i, i_126_, i_127_, i_128_, DDict.getGroup(i_129_), DDict
				.getElement(i_129_), object);
	}

	public void append_ge(int i, int i_130_, int i_131_, int i_132_,
			int i_133_, int i_134_, Object object) throws DicomException {
		int i_135_ = getSize_ge(i, i_130_, i_131_, i_132_, i_133_, i_134_);
		if (i_135_ == -1)
			i_135_ = 0;
		set_ge(i, i_130_, i_131_, i_132_, i_133_, i_134_, object, i_135_);
	}

	public Object get(int i, int i_136_, int i_137_, int i_138_, int i_139_)
			throws DicomException {
		return get(i, i_136_, i_137_, i_138_, i_139_, 0);
	}

	public Object get_ge(int i, int i_140_, int i_141_, int i_142_, int i_143_,
			int i_144_) throws DicomException {
		return get_ge(i, i_140_, i_141_, i_142_, i_143_, i_144_, 0);
	}

	public Object get(int i, int i_145_, int i_146_, int i_147_, int i_148_,
			int i_149_) throws DicomException {
		return get_ge(i, i_145_, i_146_, i_147_, DDict.getGroup(i_148_), DDict
				.getElement(i_148_), i_149_);
	}

	public Object get_ge(int i, int i_150_, int i_151_, int i_152_, int i_153_,
			int i_154_, int i_155_) throws DicomException {
		if (DDict.getTypeCode(i_151_) != 10)
			throw new DicomException(i_151_, "Data element not of type SQ");
		Object object = get(i, i_150_, i_151_, i_152_);
		if (object == null)
			return null;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.get_ge(i_153_, i_154_, i_155_);
	}

	public String getS(int i, int i_156_, int i_157_, int i_158_, int i_159_)
			throws DicomException {
		return getS(i, i_156_, i_157_, i_158_, i_159_, 0);
	}

	public String getS_ge(int i, int i_160_, int i_161_, int i_162_,
			int i_163_, int i_164_) throws DicomException {
		return getS_ge(i, i_160_, i_161_, i_162_, i_163_, i_164_, 0);
	}

	public String getS(int i, int i_165_, int i_166_, int i_167_, int i_168_,
			int i_169_) throws DicomException {
		return getS_ge(i, i_165_, i_166_, i_167_, DDict.getGroup(i_168_), DDict
				.getElement(i_168_), i_169_);
	}

	public String getS_ge(int i, int i_170_, int i_171_, int i_172_,
			int i_173_, int i_174_, int i_175_) throws DicomException {
		if (DDict.getTypeCode(i_171_) != 10)
			throw new DicomException(i_171_, "Data element not of type SQ");
		Object object = get(i, i_170_, i_171_, i_172_);
		if (object == null)
			return null;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.getS_ge(i_173_, i_174_, i_175_);
	}

	public int getI(int i, int i_176_, int i_177_, int i_178_, int i_179_)
			throws DicomException {
		return getI(i, i_176_, i_177_, i_178_, i_179_, 0);
	}

	public int getI_ge(int i, int i_180_, int i_181_, int i_182_, int i_183_,
			int i_184_) throws DicomException {
		return getI_ge(i, i_180_, i_181_, i_182_, i_183_, i_184_, 0);
	}

	public int getI(int i, int i_185_, int i_186_, int i_187_, int i_188_,
			int i_189_) throws DicomException {
		return getI_ge(i, i_185_, i_186_, i_187_, DDict.getGroup(i_188_), DDict
				.getElement(i_188_), i_189_);
	}

	public int getI_ge(int i, int i_190_, int i_191_, int i_192_, int i_193_,
			int i_194_, int i_195_) throws DicomException {
		if (DDict.getTypeCode(i_191_) != 10)
			throw new DicomException(i_191_, "Data element not of type SQ");
		Object object = get(i, i_190_, i_191_, i_192_);
		if (object == null)
			return 2147483647;
		DicomObject dicomobject = (DicomObject) object;
		return dicomobject.getI_ge(i_193_, i_194_, i_195_);
	}
}