/*
 * UnknownUIDException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class UnknownUIDException extends Exception {
	private String a;

	public UnknownUIDException(String string, String string_0_) {
		super(string_0_);
		a = string;
	}

	public String getUID() {
		return a;
	}
}