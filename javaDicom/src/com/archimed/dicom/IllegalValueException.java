/*
 * IllegalValueException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class IllegalValueException extends Exception {
	public IllegalValueException(String string) {
		super(string);
	}
}