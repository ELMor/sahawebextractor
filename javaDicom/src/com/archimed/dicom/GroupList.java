/*
 * GroupList - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.util.Enumeration;
import java.util.Vector;

public class GroupList {
	private VRGroup a = new VRGroup();

	void push(VR var_c) {
		if (var_c != null && var_c.element != 0) {
			VRGroup var_d = (VRGroup) a._mthif(var_c.group);
			if (var_d == null) {
				var_d = new VRGroup();
				a.a(var_c.group, var_d);
			}
			var_d.a(var_c.element, var_c);
		}
	}

	public void clear() {
		Enumeration enumeration = a.elements();
		while (enumeration.hasMoreElements()) {
			VRGroup var_d = (VRGroup) enumeration.nextElement();
			var_d.a();
		}
		a.a();
	}

	VR getVR(int i, int i_0_) {
		VRGroup var_d = (VRGroup) a._mthif(i);
		if (var_d == null)
			return null;
		VR var_c = (VR) var_d._mthif(i_0_);
		return var_c;
	}

	VR removeVR(int i, int i_1_) {
		VRGroup var_d = (VRGroup) a._mthif(i);
		if (var_d == null)
			return null;
		VR var_c = (VR) var_d.a(i_1_);
		return var_c;
	}

	public boolean isEmpty() {
		return a.isEmpty();
	}

	private void a(int i, boolean bool) {
		Enumeration enumeration = a.keys();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			Enumeration enumeration_2_ = ((VRGroup) a.get(integer)).elements();
			int i_3_;
			if (integer.intValue() == 2)
				i_3_ = 8194;
			else
				i_3_ = i;
			while (enumeration_2_.hasMoreElements()) {
				VR var_c = (VR) enumeration_2_.nextElement();
				var_c.a = Meter.ddictTypeLength(var_c.val, var_c._fldif, i_3_, bool);
				var_c._fldfor = Meter.is8193Bool(var_c.val, var_c._fldif, i_3_, bool);
			}
		}
	}

	public Enumeration enumerateVRs(boolean bool) {
		return enumerateVRs(bool, 8193, true);
	}

	Enumeration enumerateVRs(boolean bool, int i, boolean bool_4_) {
		Vector vector = new Vector();
		Vector vector_5_ = new Vector();
		Vector vector_6_ = new Vector();
		a(i, bool_4_);
		Enumeration enumeration = a.keys();
		while (enumeration.hasMoreElements())
			vector_5_.addElement(enumeration.nextElement());
		vector_5_ = ByteSwapper.sort(vector_5_);
		for (int i_7_ = 0; i_7_ < vector_5_.size(); i_7_++) {
			vector = new Vector();
			int i_8_ = ((Integer) vector_5_.elementAt(i_7_)).intValue();
			VRGroup var_d = (VRGroup) a._mthif(i_8_);
			if (bool) {
				if (i_8_ == 2) {
					int i_9_ = 8194;
				} else {
					int i_10_ = i;
				}
				int i_11_ = 0;
				Enumeration enumeration_12_ = var_d.elements();
				while (enumeration_12_.hasMoreElements()) {
					VR var_c = (VR) enumeration_12_.nextElement();
					i_11_ += var_c._fldfor + var_c.a;
				}
				vector_6_.addElement(new VR(i_8_, 0, 1, new Long((long) i_11_),
						4));
			}
			Enumeration enumeration_13_ = var_d.keys();
			while (enumeration_13_.hasMoreElements())
				vector.addElement(enumeration_13_.nextElement());
			vector = ByteSwapper.sort(vector);
			for (int i_14_ = 0; i_14_ < vector.size(); i_14_++) {
				VR var_c = (VR) var_d.get(vector.elementAt(i_14_));
				vector_6_.addElement(var_c);
			}
		}
		return vector_6_.elements();
	}

	public DicomObject copyGroup(int i) {
		DicomObject dicomobject = new DicomObject();
		VRGroup var_d = (VRGroup) a._mthif(i);
		Enumeration enumeration = var_d.keys();
		while (enumeration.hasMoreElements()) {
			VR var_c = (VR) var_d.get(enumeration.nextElement());
			dicomobject.push(var_c);
		}
		return dicomobject;
	}

	public DicomObject removeGroup(int i) {
		DicomObject dicomobject = new DicomObject();
		if (a == null)
			return null;
		VRGroup var_d = (VRGroup) a._mthif(i);
		if (var_d == null)
			return null;
		Enumeration enumeration = var_d.keys();
		while (enumeration.hasMoreElements()) {
			VR var_c = (VR) var_d.get(enumeration.nextElement());
			dicomobject.push(var_c);
		}
		a.a(i);
		System.gc();
		return dicomobject;
	}

	public boolean containsGroup(int i) {
		return a._mthdo(i);
	}

	void addGroup(DicomObject dicomobject) {
		Enumeration enumeration = ((GroupList) dicomobject).a.keys();
		int i = ((Integer) enumeration.nextElement()).intValue();
		if (!containsGroup(i))
			a.a(i, ((GroupList) dicomobject).a._mthif(i));
	}

	public void addGroups(DicomObject dicomobject) {
		Enumeration enumeration = ((GroupList) dicomobject).a.keys();
		while (enumeration.hasMoreElements()) {
			int i = ((Integer) enumeration.nextElement()).intValue();
			DicomObject dicomobject_15_ = dicomobject.copyGroup(i);
			addGroup(dicomobject_15_);
		}
	}

	public int numberOfGroups() {
		return a.size();
	}

	public int numberOfElements() {
		int i = 0;
		Enumeration enumeration = a.elements();
		while (enumeration.hasMoreElements()) {
			VRGroup var_d = (VRGroup) enumeration.nextElement();
			i += var_d.size();
		}
		return i;
	}
}