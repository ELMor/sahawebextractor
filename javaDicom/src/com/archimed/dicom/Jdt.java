/*
 * Jdt - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class Jdt {
	public static final int MAJOR_VERSION = 1;

	public static final int MINOR_VERSION = 16;

	public static final int BUILD = 1006;

	private static boolean dsstring = false;

	public static String getVersion() {
		return "JDT 1.16";
	}

	public static int getBuild() {
		return 1006;
	}

	public static String getAboutInfo() {
		return getVersion() + " (c) 1999 - 2003 SoftLink (www.softlink.be)";
	}

	public static boolean isDSAsString() {
		return dsstring;
	}

	public static void setDSAsString(boolean bool) {
		dsstring = bool;
	}

	static {
		dsstring = System.getProperty("jdt.types.dsstring", "false").equalsIgnoreCase(
				"true");
	}
}