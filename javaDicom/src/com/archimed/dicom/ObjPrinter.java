
package com.archimed.dicom;

import java.util.Enumeration;

class ObjPrinter {
	public static String dump(Object object, int i) {
		String string = "";
		if (object == null)
			return null;
		switch (i) {
		case 2:
		case 4:
		case 6:
		case 7:
		case 9:
		case 12:
		case 13:
		case 17:
		case 18:
		case 27:
		case 28:
			string = ((String) object).trim();
			break;
		case 14:
			string = ((Person) object).toDICOMString('^');
			break;
		case 11:
			if (object instanceof DDate)
				string = ((DDate) object).toDICOMString();
			else
				string = ((DDateRange) object).toDICOMString();
			break;
		case 16:
		case 26:
			string = object.toString();
			break;
		case 3:
		case 15:
		case 21:
			string = ((Integer) object).toString();
			break;
		case 23:
			string = ((Short) object).toString();
			break;
		case 1:
		case 19:
			string = ((Long) object).toString();
			break;
		case 20:
			string = ((Double) object).toString();
			break;
		case 0:
			string = new String((byte[]) object);
			break;
		case 8:
		case 22:
		case 24: {
			string = "";
			int i_0_ = ((byte[]) object).length;
			if (i_0_ > 12)
				i_0_ = 12;
			for (int i_1_ = 0; i_1_ < i_0_; i_1_++) {
				if (i_1_ != i_0_ - 1)
					string += a(((byte[]) object)[i_1_], 2) + "\\";
				else
					string += a(((byte[]) object)[i_1_], 2);
			}
			if (i_0_ == 12)
				string += "...";
			break;
		}
		case 5: {
			int i_2_ = ((Integer) object).intValue() >> 16;
			int i_3_ = ((Integer) object).intValue() & 0xffff;
			string = "(" + a(i_2_, 4) + "," + a(i_3_, 4) + ")";
			break;
		}
		case 29: {
			string = "";
			int i_4_ = ((float[]) object).length;
			if (i_4_ > 2)
				i_4_ = 2;
			for (int i_5_ = 0; i_5_ < i_4_; i_5_++) {
				if (i_5_ != i_4_ - 1)
					string += ((float[]) object)[i_5_] + ";";
				else
					string += ((float[]) object)[i_5_];
			}
			if (((float[]) object).length > 2)
				string += ";...";
			break;
		}
		default:
			return null;
		}
		boolean bool = true;
		for (int i_6_ = 0; i_6_ < string.length(); i_6_++) {
			if (' ' > string.charAt(i_6_) && string.charAt(i_6_) <= '~') {
				bool = false;
				i_6_ = string.length();
			}
		}
		if (bool)
			return string;
		return "Not printable";
	}

	public static String dump(Object object, int i, int i_7_) {
		String string = "";
		if (object == null)
			return null;
		switch (i) {
		case 10: {
			string += "\n";
			Enumeration enumeration = ((DicomObject) object)
					.enumerateVRs(false);
			while (enumeration.hasMoreElements())
				string += vrDump((VR) enumeration.nextElement(), i_7_ + 1);
			return string;
		}
		default:
			return null;
		}
	}

	private static String cutString(String string, int i) {
		if (string.length() > i)
			return string.substring(0, i);
		int i_8_ = i - string.length();
		for (int i_9_ = 0; i_9_ < i_8_; i_9_++)
			string += " ";
		return string;
	}

	public static String vrDump(VR vr, int i) {
		String string = "";
		boolean bool = vr._fldif == 10;
		int i_10_ = 6;
		for (int i_11_ = 0; i_11_ < i * i_10_; i_11_++)
			string += " ";
		string += "(" + a(vr.group, 4) + "," + a(vr.element, 4) + ")";
		string += " " + cutString(DDict.getTypeName(vr._fldif), 5) + " ";
		string += "[";
		String string_12_ = "";
		for (int i_13_ = 0; i_13_ < vr.val.size(); i_13_++) {
			if (bool) {
				String string_14_ = dump(vr.val.elementAt(i_13_), vr._fldif,
						i);
				if (string_14_ != null)
					string_12_ += (String) string_14_;
			} else {
				String string_15_ = dump(vr.val.elementAt(i_13_), vr._fldif);
				if (string_15_ != null)
					string_12_ += (String) string_15_;
			}
			if (i_13_ != vr.val.size() - 1) {
				if (bool) {
					for (int i_16_ = 0; i_16_ < i * i_10_; i_16_++)
						string_12_ += " ";
				}
				string_12_ += "\\";
			}
		}
		if (bool) {
			for (int i_17_ = 0; i_17_ < i * i_10_; i_17_++)
				string_12_ += " ";
		} else if (string_12_.length() > 36 - i * i_10_) {
			string_12_ = string_12_.substring(0, 36 - i * i_10_);
			string_12_ += "...";
		}
		string += (String) string_12_ + "]";
		int i_18_;
		if ((i_18_ = string.lastIndexOf("\n")) == -1)
			i_18_ = 0;
		else
			i_18_++;
		String string_19_ = string.substring(i_18_, string.length());
		for (int i_20_ = 0; i_20_ < 60 - string_19_.length(); i_20_++)
			string += " ";
		string += "#  " + vr.a;
		for (int i_21_ = 0; i_21_ < 8 - new Integer(vr.a).toString()
				.length(); i_21_++)
			string += " ";
		string += vr.val.size();
		for (int i_22_ = 0; i_22_ < 3 - new Integer(vr.val.size())
				.toString().length(); i_22_++)
			string += " ";
		String string_23_ = DDict.getDescription(DDict.lookupDDict(vr.group,
				vr.element));
		if (string_23_.equals("Undefined") && vr.group % 2 == 1)
			string_23_ = "Proprietary Tag";
		string += (String) string_23_ + "\n";
		return string;
	}

	static String a(int i, int i_24_) {
		String string = "";
		String string_25_ = Integer.toHexString(i);
		if (i_24_ < string_25_.length())
			string_25_ = string_25_.substring(0, i_24_);
		for (int i_26_ = 0; i_26_ < i_24_ - string_25_.length(); i_26_++)
			string += "0";
		return string + string_25_;
	}
}