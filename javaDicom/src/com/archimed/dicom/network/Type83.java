/*
 * r - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;

class Type83 extends ItemType {
	private int tipo = 83;

	private int data1;

	private int data2;

	public Type83() {
		/* empty */
	}

	public Type83(int i, int i_0_) {
		data1 = i;
		data2 = i_0_;
	}

	int getData1() {
		return data1;
	}

	int getData2() {
		return data2;
	}

	void setData1(int i) {
		data1 = i;
	}

	void setData2(int i) {
		data2 = i;
	}

	public int getSize() {
		return 8;
	}

	public int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException("wrong itemtype of abstract syntax");
		datainputstream.read();
		char c = datainputstream.readChar();
		if (c != '\004')
			throw new IllegalValueException(
					"length of AsynchronousOperationsWindowSubItem should be 4");
		data1 = datainputstream.readChar();
		data2 = datainputstream.readChar();
		return 8;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(4);
		dataoutputstream.writeChar(data1);
		dataoutputstream.writeChar(data2);
	}
}