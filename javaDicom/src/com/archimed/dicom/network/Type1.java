/*
 * f - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;

class Type1 {
	private int tipo = 1;

	private char _fldfor = '\001';

	String called;

	String calling;

	Type16 t16;

	Vector vec;

	Type80 t80;

	Request req;

	public Type1() {
		vec = new Vector();
	}

	public Type1(Request request) throws IllegalValueException {
		req = request;
		called = request.getCalledTitle();
		calling = request.getCallingTitle();
		vec = new Vector();
		t16 = new Type16();
		for (int i = 0; i < request.getPresentationContexts(); i++) {
			UIDEntry uidentry = request.getAbstractSyntax(i);
			Type48 var_l = new Type48(uidentry);
			Vector vector = new Vector();
			for (int i_0_ = 0; i_0_ < request.getTransferSyntaxes(i); i_0_++)
				vector.addElement(new Type64(request.getTransferSyntax(i, i_0_)));
			Type32 var_s = new Type32(request.getID(i), var_l, vector);
			vec.addElement(var_s);
		}
		t80 = new Type80();
		Type81 var_b = new Type81(request.getMaxPduSize());
		Type82 var_a = new Type82(request.getImplementationClassUID());
		Type85 var_k = new Type85(request.getImplementationVersionName());
		t80.add(var_b);
		t80.add(var_a);
		t80.add(var_k);
		for (int i = 0; i < request.getRoles(); i++) {
			UIDEntry uidentry = request.getAbstractSyntaxForRole(i);
			int scuRole = request.getScuRole(uidentry.getConstant());
			int scpRole = request.getScpRole(uidentry.getConstant());
			if (scuRole != -1 && scpRole != -1) {
				Type84 var_j = new Type84(uidentry, scuRole, scpRole);
				t80.add(var_j);
			}
		}
		for (int i = 0; i < request.getPresentationContexts(); i++) {
			byte[] is = request.getExtendedNegotiationData(i);
			if (is != null) {
				UIDEntry uidentry = request.getAbstractSyntax(i);
				Type86 var_e = new Type86(uidentry, is);
				t80.add(var_e);
			}
		}
		UIDContent[] exNeg = request.getExtendedNegotiations();
		if (exNeg != null) {
			for (int i = 0; i < exNeg.length; i++) {
				Type86 var_e = new Type86(exNeg[i].getUID(), exNeg[i].getContent());
				t80.add(var_e);
			}
		}
		if (request.getMaxOperationsInvoked() != 1
				|| request.getMaxOperationsPerformed() != 1) {
			Type83 var_r = new Type83();
			var_r.setData1(request.getMaxOperationsInvoked());
			var_r.setData2(request.getMaxOperationsPerformed());
			t80.add(var_r);
		}
	}

	public Request getReq() throws IllegalValueException {
		return req;
	}

	private void negotiate() {
		req = new Request();
		req.setCalledTitle(called);
		req.setCallingTitle(calling);
		Vector vector = t80.getVector();
		for (int i = 0; i < vec.size(); i++) {
			Type32 var_s = (Type32) vec.elementAt(i);
			Vector vector_3_ = var_s.getVector();
			UIDEntry uidentry = var_s.getT48().getUID();
			UIDEntry[] uidentrys = new UIDEntry[vector_3_.size()];
			for (int i_4_ = 0; i_4_ < vector_3_.size(); i_4_++)
				uidentrys[i_4_] = ((Type64) vector_3_.elementAt(i_4_)).getUID();
			req.addPresentationContext(var_s._mthif(), uidentry, uidentrys);
		}
		Vector vector_5_ = new Vector();
		for (int i = 0; i < vector.size(); i++) {
			ItemType var_m = (ItemType) vector.elementAt(i);
			if (var_m instanceof Type82)
				req.setImplementationClassUID(((Type82) var_m).getData());
			else if (var_m instanceof Type85)
				req.setImplementationVersionName(((Type85) var_m).getName());
			else if (var_m instanceof Type81)
				req.setMaxPduSize(((Type81) var_m).getData());
			else if (var_m instanceof Type84) {
				Type84 var_j = (Type84) var_m;
				try {
					req.setScuScpRoleSelection(var_j.getUID()
							.getConstant(), var_j._mthnew(), var_j._mthtry());
				} catch (IllegalValueException illegalvalueexception) {
					illegalvalueexception.printStackTrace(System.err);
				}
			} else if (var_m instanceof Type83) {
				Type83 var_r = (Type83) var_m;
				req.setMaxOperationsInvoked(var_r.getData1());
				req.setMaxOperationsPerformed(var_r.getData2());
			} else if (var_m instanceof Type86) {
				Type86 var_e = (Type86) var_m;
				UIDEntry uidentry = var_e.getUIDEntry();
				for (int i_6_ = 0; i_6_ < req.getPresentationContexts(); i_6_++) {
					if (req.getAbstractSyntax(i_6_).getValue().equals(
							uidentry.getValue()))
						req.setExtendedNegotiationData(i_6_, var_e
								.getBytes());
				}
				UIDContent var_g = new UIDContent();
				var_g.setUID(var_e.getUIDEntry());
				var_g.setContent(var_e.getBytes());
				vector_5_.add(var_g);
			}
		}
		UIDContent[] var_gs = new UIDContent[vector_5_.size()];
		for (int i = 0; i < vector_5_.size(); i++)
			var_gs[i] = (UIDContent) vector_5_.elementAt(i);
		req.setExtendedNegotiations(var_gs);
	}

	public String getCalled() {
		return called;
	}

	public String getCalling() {
		return calling;
	}

	public Type16 getT16() {
		return t16;
	}

	public Vector getVector() {
		return vec;
	}

	public Type80 getT80() {
		return t80;
	}

	public void setT16(Type16 var_o) {
		t16 = var_o;
	}

	public void setT80(Type80 var_p) {
		t80 = var_p;
	}

	public void add(Type32 var_s) {
		vec.addElement(var_s);
	}

	public int length() {
		int i = 0;
		i += 74;
		i += t16.length();
		for (int i_7_ = 0; i_7_ < vec.size(); i_7_++)
			i += ((Type32) vec.elementAt(i_7_)).length();
		i += t80.length();
		return i;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeInt(length() - 6);
		dataoutputstream.writeChar(1);
		dataoutputstream.writeChar(0);
		dataoutputstream.write(called.getBytes());
		for (int i = 0; i < 16 - called.length(); i++)
			dataoutputstream.write(32);
		dataoutputstream.write(calling.getBytes());
		for (int i = 0; i < 16 - calling.length(); i++)
			dataoutputstream.write(32);
		dataoutputstream.write(new byte[32]);
		t16.write(dataoutputstream);
		for (int i = 0; i < vec.size(); i++)
			((Type32) vec.elementAt(i)).write(dataoutputstream);
		t80.write(dataoutputstream);
	}

	public void read(InputStream inputstream) throws IOException,
			IllegalValueException, UnknownUIDException {
		PushbackInputStream pushbackinputstream = new PushbackInputStream(
				inputstream);
		DataInputStream datainputstream = new DataInputStream(
				pushbackinputstream);
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"PDU-type field of Associate Request PDU not " + tipo);
		datainputstream.read();
		int i_8_ = datainputstream.readInt();
		_fldfor = datainputstream.readChar();
		i_8_ -= 2;
		datainputstream.readChar();
		byte[] is = new byte[16];
		datainputstream.readFully(is);
		called = new String(is);
		called = called.trim();
		datainputstream.readFully(is);
		calling = new String(is);
		calling = calling.trim();
		datainputstream.readFully(new byte[32]);
		i_8_ -= 66;
		Type16 var_o = new Type16();
		int i_9_ = var_o.read(datainputstream);
		i_8_ -= i_9_;
		while (i_8_ > 0) {
			i = datainputstream.read();
			if (i == 32) {
				pushbackinputstream.unread(32);
				Type32 var_s = new Type32();
				i_9_ = var_s.read(datainputstream);
				vec.addElement(var_s);
				i_8_ -= i_9_;
			} else if (i == 80) {
				pushbackinputstream.unread(80);
				t80 = new Type80();
				i_9_ = t80.read(pushbackinputstream);
				i_8_ -= i_9_;
			}
		}
		negotiate();
	}
}