/*
 * p - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

class Type80 {
	private int tipo = 80;

	private Vector v;

	private int size = 0;

	public Type80() {
		v = new Vector();
	}

	public void add(ItemType var_m) {
		v.addElement(var_m);
	}

	public Vector getVector() {
		return v;
	}

	public int length() {
		int i = 4;
		for (int j = 0; j < v.size(); j++)
			i += ((ItemType) v.elementAt(j)).getSize();
		i += size;
		return i;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(length() - 4);
		for (int i = 0; i < v.size(); i++)
			((ItemType) v.elementAt(i)).write(dataoutputstream);
	}

	public int read(PushbackInputStream pushbackinputstream) throws IOException,
			IllegalValueException, UnknownUIDException {
		DataInputStream datainputstream = new DataInputStream(
				pushbackinputstream);
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"itemtype field of received User Info Item  not " + tipo);
		datainputstream.read();
		int i_1_ = datainputstream.readChar();
		int i_2_ = i_1_;
		while (i_1_ > 0) {
			i = datainputstream.read();
			pushbackinputstream.unread(i);
			if (i == 81) {
				Type81 var_b = new Type81();
				int i_3_ = var_b.read(datainputstream);
				add(var_b);
				i_1_ -= i_3_;
			} else if (i == 82) {
				Type82 var_a = new Type82();
				int i_4_ = var_a.read(datainputstream);
				add(var_a);
				i_1_ -= i_4_;
			} else if (i == 83) {
				Type83 var_r = new Type83();
				int i_5_ = var_r.read(datainputstream);
				add(var_r);
				i_1_ -= i_5_;
			} else if (i == 84) {
				Type84 var_j = new Type84();
				int i_6_ = var_j.read(datainputstream);
				add(var_j);
				i_1_ -= i_6_;
			} else if (i == 85) {
				Type85 var_k = new Type85();
				int i_7_ = var_k.read(datainputstream);
				add(var_k);
				i_1_ -= i_7_;
			} else if (i == 86) {
				Type86 var_e = new Type86();
				int i_8_ = var_e.read(datainputstream);
				add(var_e);
				i_1_ -= i_8_;
			} else {
				datainputstream.read();
				datainputstream.read();
				int i_9_ = datainputstream.readChar();
				datainputstream.read();
				i_1_ = i_1_ - i_9_ - 4;
				size += i_9_ + 4;
			}
		}
		return 4 + i_2_;
	}
}