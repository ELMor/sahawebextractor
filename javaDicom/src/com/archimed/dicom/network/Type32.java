/*
 * s - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;

class Type32 {
	private byte tipo = 32;

	private int _fldfor;

	private Vector vec;

	private Type48 t48;

	public Type32() {
		/* empty */
	}

	public Type32(int i, Type48 var_l, Vector vector) {
		_fldfor = i;
		t48 = var_l;
		vec = vector;
	}

	public int _mthif() {
		return _fldfor;
	}

	public int length() {
		return 4 + len();
	}

	public Type48 getT48() {
		return t48;
	}

	public Vector getVector() {
		return vec;
	}

	public void add(Type64 v) {
		vec.addElement(v);
	}

	private int len() {
		int i = 4;
		i += t48.length();
		for (int i_0_ = 0; i_0_ < vec.size(); i_0_++) {
			Type64 var_n = (Type64) vec.elementAt(i_0_);
			i += var_n.length();
		}
		return i;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(len());
		dataoutputstream.write(_fldfor);
		dataoutputstream.write(0);
		dataoutputstream.write(0);
		dataoutputstream.write(0);
		t48.write(dataoutputstream);
		for (int i = 0; i < vec.size(); i++)
			((Type64) vec.elementAt(i)).write(dataoutputstream);
	}

	int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"wrong item type of presentation context item");
		datainputstream.read();
		int i_1_ = datainputstream.readChar();
		int i_2_ = i_1_;
		_fldfor = datainputstream.read();
		datainputstream.read();
		datainputstream.read();
		datainputstream.read();
		i_1_ -= 4;
		t48 = new Type48();
		int i_3_ = t48.read(datainputstream);
		i_1_ -= i_3_;
		vec = new Vector();
		for (/**/; i_1_ > 0; i_1_ -= i_3_) {
			Type64 var_n = new Type64();
			i_3_ = var_n.read(datainputstream);
			vec.addElement(var_n);
		}
		return i_2_ + 4;
	}
}