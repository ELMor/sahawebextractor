/*
 * ResponsePolicy - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;

public class ResponsePolicy {
	public static Response prepareResponse(Request request, String string,
			String[] strings, int[] is, int i, boolean bool)
			throws IllegalValueException {
		return prepareResponse(request, string, strings, is, new int[] { i },
				bool);
	}

	public static Response prepareResponse(Request request, String string,
			String[] strings, int[] is, int[] is_0_, boolean bool)
			throws IllegalValueException {
		if (string != null && !request.getCalledTitle().equals(string))
			return new Reject(Reject.REJECTED_PERMANENT,
					Reject.DICOM_UL_SERVICE_USER,
					Reject.USER_CALLED_AETITLE_NOT_RECOGNIZED);
		boolean bool_1_ = true;
		if (strings != null) {
			for (int i = 0; i < strings.length; i++) {
				if (request.getCallingTitle().equals(strings[i]))
					bool_1_ = false;
			}
			if (bool_1_)
				return new Reject(Reject.REJECTED_PERMANENT,
						Reject.DICOM_UL_SERVICE_USER,
						Reject.USER_CALLING_AETITLE_NOT_RECOGNIZED);
		}
		Acknowledge acknowledge = new Acknowledge();
		acknowledge.setCalledTitle(request.getCalledTitle());
		acknowledge.setCallingTitle(request.getCallingTitle());
		while_2_: for (int i = 0; i < request.getPresentationContexts(); i++) {
			UIDEntry uidentry = request.getAbstractSyntax(i);
			for (int i_2_ = 0; i_2_ < is.length; i_2_++) {
				if (uidentry.equals(UID.getUIDEntry(is[i_2_]))) {
					for (int i_3_ = 0; i_3_ < request.getTransferSyntaxes(i); i_3_++) {
						UIDEntry uidentry_4_ = request.getTransferSyntax(i,
								i_3_);
						for (int i_5_ = 0; i_5_ < is_0_.length; i_5_++) {
							if (uidentry_4_.getConstant() == is_0_[i_5_]) {
								acknowledge.addPresentationContext(request
										.getID(i), 0, is_0_[i_5_]);
								continue while_2_;
							}
						}
					}
					acknowledge.addPresentationContext(request.getID(i), 4,
							8193);
					continue while_2_;
				}
			}
			acknowledge.addPresentationContext(request.getID(i), 3, 8193);
		}
		if (bool) {
			bool_1_ = true;
			for (int i = 0; i < acknowledge.getPresentationContexts(); i++) {
				if (acknowledge.getResult(i) == 0)
					bool_1_ = false;
			}
			if (bool_1_)
				return new Reject(Reject.REJECTED_PERMANENT,
						Reject.DICOM_UL_SERVICE_USER,
						Reject.USER_NO_REASON_GIVEN);
		}
		return acknowledge;
	}

	public static int getResultForAbstractSyntax(Request request,
			Acknowledge acknowledge, int i) throws IllegalValueException {
		int i_6_ = request.getPresentationContexts();
		for (int i_7_ = 0; i_7_ < i_6_; i_7_++) {
			UIDEntry uidentry = request.getAbstractSyntax(i_7_);
			if (uidentry.getConstant() == i) {
				int i_8_ = request.getID(i_7_);
				for (int i_9_ = 0; i_9_ < acknowledge.getPresentationContexts(); i_9_++) {
					int i_10_ = acknowledge.getID(i_9_);
					if (i_8_ == i_10_)
						return acknowledge.getResult(i_9_);
				}
				throw new IllegalValueException(
						"no matching presentation context id in request and acknowledge for specified abstract syntax");
			}
		}
		throw new IllegalValueException("abstract syntax not in request");
	}
}