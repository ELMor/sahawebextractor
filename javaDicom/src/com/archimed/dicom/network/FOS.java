/*
 * c - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class FOS extends FilterOutputStream {
	public static final byte _fldnew = 1;

	public static final byte _flddo = 0;

	private int _fldif;

	private DataOutputStream dos;

	private byte[] buffer;

	private int bufferSize;

	private byte p2;

	private int p1;

	public FOS(OutputStream outputstream, int i, int i_0_, byte i_1_) {
		super(outputstream);
		dos = new DataOutputStream(outputstream);
		if (i == 0)
			i = 100000;
		else
			_fldif = i;
		p2 = i_1_;
		p1 = i_0_;
		buffer = new byte[i - 6];
	}

	public synchronized void write(int i) throws IOException {
		if (bufferSize >= buffer.length)
			flushBuffer();
		buffer[bufferSize++] = (byte) i;
	}

	public synchronized void write(byte[] is) throws IOException {
		write(is, 0, is.length);
	}

	public synchronized void write(byte[] is, int i, int i_2_)
			throws IOException {
		if (bufferSize >= buffer.length)
			flushBuffer();
		if (i_2_ <= buffer.length - bufferSize) {
			System.arraycopy(is, i, buffer, bufferSize, i_2_);
			bufferSize += i_2_;
		} else {
			System.arraycopy(is, i, buffer, bufferSize, buffer.length
					- bufferSize);
			i += buffer.length - bufferSize;
			i_2_ -= buffer.length - bufferSize;
			bufferSize += buffer.length - bufferSize;
			flushBuffer();
			int i_3_ = i_2_ / buffer.length;
			for (int i_4_ = 0; i_4_ < i_3_; i_4_++) {
				System.arraycopy(is, i, buffer, 0, buffer.length);
				i += buffer.length;
				i_2_ -= buffer.length;
				bufferSize += buffer.length;
				flushBuffer();
			}
			if (i_2_ > 0) {
				System.arraycopy(is, i, buffer, 0, i_2_);
				bufferSize += i_2_;
			}
		}
	}

	private void flushBuffer() throws IOException {
		dos.writeByte(4);
		dos.writeByte(0);
		dos.writeInt(bufferSize + 6);
		dos.writeInt(bufferSize + 2);
		dos.writeByte(p1);
		dos.writeByte(p2);
		out.write(buffer, 0, bufferSize);
		bufferSize = 0;
	}

	public void close() throws IOException {
		if (bufferSize > 0) {
			dos.writeByte(4);
			dos.writeByte(0);
			dos.writeInt(bufferSize + 6);
			dos.writeInt(bufferSize + 2);
			dos.writeByte(p1);
			dos.writeByte(p2 + 2);
			out.write(buffer, 0, bufferSize);
		} else {
			dos.writeByte(4);
			dos.writeByte(0);
			dos.writeInt(6);
			dos.writeInt(2);
			dos.writeByte(p1);
			dos.writeByte(p2 + 2);
		}
	}
}