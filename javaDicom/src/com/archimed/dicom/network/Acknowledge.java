/*
 * Acknowledge - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.tool.StringTool;

public class Acknowledge extends Response {
	public static final int NO_SUPPORT_SCU_ROLE = 0;

	public static final int SUPPORT_SCU_ROLE = 1;

	public static final int NO_SUPPORT_SCP_ROLE = 0;

	public static final int SUPPORT_SCP_ROLE = 1;

	public static final int DEFAULT = -1;

	public static final int ACCEPTANCE = 0;

	public static final int USER_REJECTION = 1;

	public static final int NO_REASON = 2;

	public static final int ABSTRACT_SYNTAX_NOT_SUPPORTED = 3;

	public static final int TRANSFER_SYNTAXES_NOT_SUPPORTED = 4;

	private String called;

	private String calling;

	private Vector b;

	private Vector results;

	private Vector presentationContexts;

	private String impClassUID;

	private String impVersionName;

	private UIDContent[] ids;

	private int maxPDUSize;

	private int maxOpInvoked = 1;

	private int maxOpPerformed = 1;

	private Vector abstractSyntaxForRole = new Vector();

	private Vector v2;

	private Vector v3;

	public Acknowledge() {
		b = new Vector();
		results = new Vector();
		presentationContexts = new Vector();
		v2 = new Vector();
		v3 = new Vector();
		setImplementationClassUID(Association.implclassuid);
		setImplementationVersionName(Association.implversionname);
	}

	public String getCallingTitle() {
		return calling;
	}

	public String getCalledTitle() {
		return called;
	}

	public void setCalledTitle(String string) {
		called = string;
	}

	public void setCallingTitle(String string) {
		calling = string;
	}

	public String getImplementationClassUID() {
		return impClassUID;
	}

	public String getImplementationVersionName() {
		return impVersionName;
	}

	void setImplementationClassUID(String string) {
		impClassUID = string;
	}

	void setImplementationVersionName(String string) {
		impVersionName = string;
	}

	public int getMaxPduSize() {
		return maxPDUSize;
	}

	public void setMaxPduSize(int i) {
		maxPDUSize = i;
	}

	public void setMaxOperationsInvoked(int i) {
		maxOpInvoked = i;
	}

	public int getMaxOperationsInvoked() {
		return maxOpInvoked;
	}

	public void setMaxOperationsPerformed(int i) {
		maxOpPerformed = i;
	}

	public int getMaxOperationsPerformed() {
		return maxOpPerformed;
	}

	public void setScuScpRoleSelection(int i, int i_0_, int i_1_)
			throws IllegalValueException {
		UIDEntry uidentry = UID.getUIDEntry(i);
		int i_2_ = abstractSyntaxForRole.indexOf(uidentry);
		if (i_2_ != -1) {
			v2.setElementAt(new Integer(i_0_), i_2_);
			v3.setElementAt(new Integer(i_1_), i_2_);
		} else {
			abstractSyntaxForRole.addElement(uidentry);
			v2.addElement(new Integer(i_0_));
			v3.addElement(new Integer(i_1_));
		}
	}

	public int getScuRole(int i) throws IllegalValueException {
		int i_3_ = abstractSyntaxForRole.indexOf(UID.getUIDEntry(i));
		if (i_3_ == -1)
			return -1;
		return ((Integer) v2.elementAt(i_3_)).intValue();
	}

	public int getScpRole(int i) throws IllegalValueException {
		int i_4_ = abstractSyntaxForRole.indexOf(UID.getUIDEntry(i));
		if (i_4_ == -1)
			return -1;
		return ((Integer) v3.elementAt(i_4_)).intValue();
	}

	UIDEntry getAbstractSyntaxForRole(int i) {
		return (UIDEntry) abstractSyntaxForRole.elementAt(i);
	}

	int getRoles() {
		return abstractSyntaxForRole.size();
	}

	public UIDContent[] getExtendedNegotiations() {
		return ids;
	}

	public void setExtendedNegotiations(UIDContent[] var_gs) {
		if (var_gs != null)
			ids = var_gs;
	}

	public byte getPresentationContextID(int i) {
		return (byte) ((Integer) presentationContexts.elementAt(i)).intValue();
	}

	public int getID(int i) {
		return ((Integer) presentationContexts.elementAt(i)).intValue();
	}

	public int getResult(int i) {
		return ((Integer) results.elementAt(i)).intValue();
	}

	public int getPresentationContexts() {
		return presentationContexts.size();
	}

	public UIDEntry getTransferSyntax(int i) {
		return (UIDEntry) b.elementAt(i);
	}

	public void addPresentationContext(byte i, int i_5_, int i_6_)
			throws IllegalValueException {
		UIDEntry uidentry = UID.getUIDEntry(i_6_);
		presentationContexts.addElement(new Byte(i));
		results.addElement(new Integer(i_5_));
		b.addElement(uidentry);
	}

	public void addPresentationContext(int i, int i_7_, int i_8_)
			throws IllegalValueException {
		UIDEntry uidentry = UID.getUIDEntry(i_8_);
		presentationContexts.addElement(new Integer(i));
		results.addElement(new Integer(i_7_));
		b.addElement(uidentry);
	}

	public String toString() {
		int i = 30;
		String string = "**************** ASSOCIATE ACKNOWLEDGE *****************\n";
		string += (StringTool.fstr("implementation class UID:", i) + impClassUID + "\n");
		string += (StringTool.fstr("implementation version name:", i)
				+ impVersionName + "\n");
		string += StringTool.fstr("max PDU size:", i) + maxPDUSize + "\n";
		string += StringTool.fstr("max operations invoked:", i) + maxOpInvoked
				+ "\n";
		string += (StringTool.fstr("max operations performed:", i) + maxOpPerformed + "\n");
		string += (StringTool.fstr("# presentation contexts:", i)
				+ presentationContexts.size() + "\n");
		string += (StringTool.fstr("nr", 5) + StringTool.fstr("pcid", 6)
				+ StringTool.fstr("result", 32)
				+ StringTool.fstr("transfer syntax", 20) + "\n");
		for (int i_9_ = 0; i_9_ < getPresentationContexts(); i_9_++) {
			if (getResult(i_9_) == 0)
				string += (StringTool.fstr(i_9_ + "", 5)
						+ StringTool.fstr("" + getID(i_9_), 6)
						+ StringTool.fstr("accepted", 32)
						+ StringTool.fstr((""
								+ getTransferSyntax(i_9_).getShortName() + "("
								+ getTransferSyntax(i_9_) + ")"), 30) + "\n");
			else if (getResult(i_9_) == 3)
				string += (StringTool.fstr(i_9_ + "", 5)
						+ StringTool.fstr("" + getID(i_9_), 6)
						+ StringTool.fstr("unsupported abstract syntax", 32) + "\n");
			else if (getResult(i_9_) == 4)
				string += (StringTool.fstr(i_9_ + "", 5)
						+ StringTool.fstr("" + getID(i_9_), 6)
						+ StringTool.fstr("unsupported transfer syntaxes", 32) + "\n");
			else if (getResult(i_9_) == 1)
				string += (StringTool.fstr(i_9_ + "", 5)
						+ StringTool.fstr("" + getID(i_9_), 6)
						+ StringTool.fstr("user rejection", 32) + "\n");
			else if (getResult(i_9_) == 2)
				string += (StringTool.fstr(i_9_ + "", 5)
						+ StringTool.fstr("" + getID(i_9_), 6)
						+ StringTool.fstr("no reason", 32) + "\n");
		}
		string += "non-default roles set:\n";
		boolean bool = false;
		try {
			for (int i_10_ = 0; i_10_ < getRoles(); i_10_++) {
				UIDEntry uidentry = getAbstractSyntaxForRole(i_10_);
				if (getScpRole(uidentry.getConstant()) != -1
						|| getScuRole(uidentry.getConstant()) != -1) {
					bool = true;
					string += StringTool.fstr(uidentry.getName() + "", 55);
					String string_11_ = "";
					if (getScuRole(uidentry.getConstant()) == 1)
						string_11_ = "SCU";
					else if (getScuRole(uidentry.getConstant()) == 0)
						string_11_ = "NO-SCU";
					else if (getScuRole(uidentry.getConstant()) == -1)
						string_11_ = "DEFAULT";
					string_11_ += "/";
					if (getScpRole(uidentry.getConstant()) == 1)
						string_11_ += "SCP";
					else if (getScpRole(uidentry.getConstant()) == 0)
						string_11_ += "NO-SCP";
					else if (getScpRole(uidentry.getConstant()) == -1)
						string_11_ += "DEFAULT";
					string += StringTool.fstr(string_11_, 30) + "\n";
				}
			}
			if (!bool)
				string += "none\n";
		} catch (IllegalValueException illegalvalueexception) {
			illegalvalueexception.printStackTrace();
		}
		string += "********************************************************";
		return string;
	}
}