/*
 * k - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.tool.StringTool;

class Type85 extends ItemType {
	private int tipo = 85;

	private String name;

	public Type85() {
		/* empty */
	}

	public Type85(String string) {
		name = string;
	}

	public String getName() {
		return name;
	}

	public int getSize() {
		return 4 + name.length();
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(name.length());
		dataoutputstream.write(name.getBytes());
	}

	public int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"itemtype field of received Implementation Version Name Item  not "
							+ tipo);
		datainputstream.read();
		char c = datainputstream.readChar();
		byte[] is = new byte[c];
		datainputstream.readFully(is);
		is = StringTool.trimZeros(is);
		name = new String(is);
		return '\004' + c;
	}
}