/*
 * j - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.StringTool;

class Type84 extends ItemType {
	private int tipo = 84;

	private int data1;

	private int data2;

	private UIDEntry id;

	public Type84() {
		/* empty */
	}

	public Type84(UIDEntry uidentry, int i, int i_0_) {
		id = uidentry;
		data1 = i;
		data2 = i_0_;
	}

	public int getSize() {
		int i = 8;
		if (id != null)
			i += id.getValue().length();
		return i;
	}

	public UIDEntry getUID() {
		return id;
	}

	public int _mthnew() {
		return data1;
	}

	public int _mthtry() {
		return data2;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(id.getValue().length() + 4);
		dataoutputstream.writeChar(id.getValue().length());
		dataoutputstream.write(id.getValue().getBytes());
		dataoutputstream.write(data1);
		dataoutputstream.write(data2);
	}

	public int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"itemtype field of received ScuScpRoleSubItem  not "
							+ tipo);
		datainputstream.read();
		char c = datainputstream.readChar();
		char c_1_ = datainputstream.readChar();
		byte[] is = new byte[c_1_];
		datainputstream.readFully(is);
		is = StringTool.trimZeros(is);
		String string = new String(is);
		try {
			id = UID.getUIDEntry(string);
		} catch (UnknownUIDException unknownuidexception) {
			id = new UIDEntry(0, string, "unknown uid", "??", 1);
		}
		data1 = datainputstream.read();
		data2 = datainputstream.read();
		return '\006' + c_1_ + '\002';
	}
}