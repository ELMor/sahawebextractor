//Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: fieldsfirst 
//Source File Name:   VR.java

package com.archimed.dicom;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;

//Referenced classes of package com.archimed.dicom:
//         TagValue, DicomObject, IllegalValueException, DicomException, 
//         DDict, Debug, g, a, 
//         m, k, UID, UIDEntry, 
//         r, q

class VR extends TagValue
{

 protected int a;
 protected int _fldfor;
 protected int _fldif;
 protected boolean _flddo;

 protected VR()
 {
     a = 0;
     _fldfor = 0;
     _fldif = 0;
     _flddo = false;
     group = 0;
     element = 0;
     val = new Vector();
     _fldif = 0;
     a = 0;
     _fldfor = 0;
     _flddo = true;
 }

 protected VR(int i, int j, int l)
 {
     a = 0;
     _fldfor = 0;
     _fldif = 0;
     _flddo = false;
     group = i;
     element = j;
     _fldif = l;
     _flddo = true;
 }

 protected VR(int i, int j, int l, Object obj, int i1)
 {
     a = 0;
     _fldfor = 0;
     _fldif = 0;
     _flddo = false;
     group = i;
     element = j;
     val = new Vector();
     _fldif = l;
     val.addElement(obj);
     a = i1;
     _flddo = false;
     _fldfor = 8;
 }

 protected int a()
 {
     int i = DDict.lookupDDict(group, element);
     return i;
 }

 protected void a(FOS3 g1)
     throws IOException, DicomException
 {
     if(Debug.DEBUG >= 5)
         Debug.printMessage("VR.writeVRHeader", group, element);
     int i = g1._mthif();
     boolean flag = g1.a();
     int j = 0;
     byte abyte0[] = {
         0, 0
     };
     g1.a(group);
     g1.a(element);
     if(group == 2)
         i = 8194;
     j = a;
     if(_flddo)
         j = 0;
     else
     if(_fldif == 10 && flag)
         j = -1;
     if((_fldif == 24 || _fldif == 8 || _fldif == 22) && val.size() > 1)
     {
         _fldif = 8;
         j = -1;
     }
     if(i == 8193)
     {
         g1._mthdo(j);
     } else
     {
         if(_fldif == 22)
             _fldif = a(group, element, i);
         String s = DDict.getTypeName(_fldif);
         g1.a(s.substring(0, 2));
         switch(_fldif)
         {
         case 0: // '\0'
         case 8: // '\b'
         case 10: // '\n'
         case 22: // '\026'
         case 24: // '\030'
         case 27: // '\033'
             g1.write(abyte0);
             g1._mthdo(j);
             break;

         default:
             g1.a(j);
             break;
         }
     }
 }

 private void a(OSWritter a1, DicomObject dicomobject)
     throws IOException, DicomException
 {
     VR c1;
     for(Enumeration enumeration = dicomobject.enumerateVRs(false, a1._mthif(), a1.a()); enumeration.hasMoreElements(); c1._mthif(a1))
     {
         c1 = (VR)enumeration.nextElement();
         c1.a(((FOS3) (a1)));
     }

 }

 private void _mthdo(OSWritter a1)
     throws DicomException, IOException
 {
     if(a1.a())
     {
         for(int i = 0; i < val.size(); i++)
         {
             DicomObject dicomobject = (DicomObject)val.elementAt(i);
             a1.a(65534);
             a1.a(57344);
             a1._mthdo(-1);
             a(a1, dicomobject);
             a1.a(65534);
             a1.a(57357);
             a1._mthdo(0);
         }

         a1.a(65534);
         a1.a(57565);
         a1._mthdo(0);
     } else
     {
         for(int j = 0; j < val.size(); j++)
         {
             DicomObject dicomobject1 = (DicomObject)val.elementAt(j);
             a1.a(65534);
             a1.a(57344);
             a1._mthdo(Meter.dicomObjectLength(dicomobject1, a1._mthif(), a1.a()) - 8);
             a(a1, dicomobject1);
         }

     }
 }

 private void a(OSWritter a1)
     throws IOException, DicomException
 {
     for(int i = 0; i < val.size(); i++)
     {
         byte abyte0[] = (byte[])val.elementAt(i);
         a1.a(65534);
         a1.a(57344);
         a1._mthdo(abyte0.length);
         if(abyte0.length != 0)
             a1.write(abyte0);
     }

     a1.a(65534);
     a1.a(57565);
     a1._mthdo(0);
 }

 protected void _mthif(OSWritter a1)
     throws IOException, DicomException
 {
     if(a == 0 || _flddo)
     {
         if(Debug.DEBUG >= 5)
             Debug.printMessage("VR.writeVRData <empty>", group, element);
         return;
     }
     if(_fldif == 10)
     {
         if(Debug.DEBUG >= 5)
             Debug.printMessage("VR.writeVRData <sequence>", group, element);
         _mthdo(a1);
         return;
     }
     if((_fldif == 24 || _fldif == 8 || _fldif == 22) && val.size() > 1)
     {
         if(Debug.DEBUG >= 5)
             Debug.printMessage("VR.writeVRData <undefined length>", group, element);
         a(a1);
         return;
     }
     if(Debug.DEBUG >= 5)
         Debug.printMessage("VR.writeVRData <normal>", group, element);
     a1._mthfor(val, _fldif);
 }

 protected boolean a(FIS3 k1)
     throws IOException, DicomException
 {
     int i = k1.getData1();
     group = k1.read01();
     element = k1.read01();
     if(Debug.DEBUG >= 5)
         try
         {
             Debug.printMessage("VR.readVRHeader (ts: " + UID.getUIDEntry(k1.getData1()).toString() + ")", group, element);
         }
         catch(IllegalValueException illegalvalueexception) { }
     if(group == 2)
         i = 8194;
     if(element == 0)
         _fldif = 1;
     else
         _fldif = DDict.getTypeCode(group, element);
     if(i == 8193)
     {
         a = k1.read03();
         _fldfor = 8;
     } else
     {
         String s = k1.readString(2);
         if(group != 65534 || element != 57357)
             _fldif = DDict.getTypeCode(s);
         switch(_fldif)
         {
         case 0: // '\0'
         case 8: // '\b'
         case 10: // '\n'
         case 24: // '\030'
         case 27: // '\033'
             k1.skipBytes(2);
             a = k1.read03();
             _fldfor = 12;
             break;

         default:
             a = k1.read01();
             _fldfor = 8;
             break;
         }
     }
     int j = DDict.getTypeFixed(_fldif);
     if(j != 0 && a % j != 0)
     {
         k1.skipBytes(a);
         System.err.println("Data element: (" + group + "," + element + "): " + DDict.getDescription(DDict.lookupDDict(group, element)) + " has an invalid length (" + a + ")\n    Skipping...");
         return false;
     } else
     {
         return true;
     }
 }

 private DicomObject _mthfor(FIS5 r1)
     throws IOException, DicomException
 {
     DicomObject dicomobject = new DicomObject();
     dicomobject.offset = r1.getReaded() - 4L;
     long l = r1.read03();
     if(l != -1L)
     {
         dicomobject.length = 0;
         long l1 = r1.getReaded() + l;
         do
         {
             if(r1.getReaded() == l1)
                 break;
             VR c1 = new VR();
             if(c1.a(r1) && c1.a(r1))
             {
                 dicomobject.length += c1._fldfor;
                 dicomobject.length += c1.a;
                 dicomobject.push(c1);
             }
         } while(true);
     } else
     {
         boolean flag = false;
         dicomobject.length = 0;
         do
         {
             if(flag)
                 break;
             VR c2 = new VR();
             if(c2.a(r1))
                 if(c2.group == 65534 && c2.element == 57357)
                     flag = true;
                 else
                 if(c2.a(r1))
                 {
                     dicomobject.length += c2._fldfor;
                     dicomobject.length += c2.a;
                     dicomobject.push(c2);
                 }
         } while(true);
     }
     dicomobject.length += 16;
     return dicomobject;
 }

 private Vector _mthint(FIS5 r1)
     throws DicomException, IOException
 {
     Vector vector = new Vector();
     if(a != -1)
     {
         long l1 = r1.getReaded() + (long)a;
         a = 0;
         do
         {
             if(r1.getReaded() == l1)
                 break;
             int i = r1.read01();
             int l = r1.read01();
             if(i == 65534 && l == 57344)
             {
                 DicomObject dicomobject = _mthfor(r1);
                 if(dicomobject != null)
                 {
                     a += dicomobject.length;
                     vector.addElement(dicomobject);
                 }
             }
         } while(true);
     } else
     {
         boolean flag = false;
         a = 0;
         do
         {
             if(flag)
                 break;
             int j = r1.read01();
             int i1 = r1.read01();
             if(j == 65534 && i1 == 57344)
             {
                 DicomObject dicomobject1 = _mthfor(r1);
                 if(dicomobject1 != null)
                 {
                     a += dicomobject1.length;
                     vector.addElement(dicomobject1);
                 }
             } else
             if(j == 65534 && i1 == 57565)
             {
                 r1.skipBytes(4);
                 flag = true;
             } else
             {
                 throw new DicomException("Unexpected tag read: (" + j + "," + i1 + ") at offset " + r1.getReaded());
             }
         } while(true);
     }
     a += 8;
     return vector;
 }

 private Vector _mthif(FIS5 r1)
     throws DicomException, IOException
 {
     Vector vector = new Vector();
     boolean flag = true;
     boolean flag1 = false;
     a = 0;
     do
     {
         if(flag1)
             break;
         int i = r1.read01();
         int j = r1.read01();
         if(i == 65534 && j == 57344)
         {
             if(flag)
             {
                 _fldif = 10;
                 flag = false;
             }
             DicomObject dicomobject = _mthfor(r1);
             if(dicomobject != null)
             {
                 a += dicomobject.length;
                 vector.addElement(dicomobject);
             }
         } else
         if(i == 65534 && j == 57565)
         {
             r1.skipBytes(4);
             flag1 = true;
         } else
         if(flag)
             return a(i, j, r1);
         else
             throw new DicomException("Unexpected tag read: (" + i + "," + j + ") at offset " + r1.getReaded());
     } while(true);
     a += 8;
     return vector;
 }

 private Vector a(int i, int j, FIS5 r1)
     throws DicomException, IOException
 {
     Vector vector = new Vector();
     a = 0;
     ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
     DataOutputStream dataoutputstream = new DataOutputStream(bytearrayoutputstream);
     int l = r1.read01();
     int j1 = r1.read01();
     a += 4;
     if(l == 65534 && j1 == 57565)
     {
         r1.skipBytes(4);
         a += 4;
         vector.addElement(bytearrayoutputstream.toByteArray());
         return vector;
     }
     dataoutputstream.writeShort(l);
     dataoutputstream.writeShort(j1);
     do
     {
         int i1 = j1;
         j1 = r1.read01();
         a += 2;
         if(i1 == 65534 && j1 == 57565)
         {
             r1.skipBytes(4);
             a += 4;
             vector.addElement(bytearrayoutputstream.toByteArray());
             return vector;
         }
         dataoutputstream.writeShort(j1);
     } while(true);
 }

 private Vector _mthdo(FIS5 r1)
     throws DicomException, IOException
 {
     Vector vector = new Vector();
     do
     {
         int i = r1.read01();
         int j = r1.read01();
         a += 4;
         if(i == 65534 && j == 57565)
         {
             r1.skipBytes(4);
             a += 4;
             return vector;
         }
         if(i == 65534 && j == 57344)
         {
             int l = r1.read03();
             a += 4;
             byte abyte0[] = new byte[l];
             r1.discard(abyte0);
             a += l;
             vector.addElement(abyte0);
         } else
         {
             throw new DicomException("Unexpected tag encountered: (" + i + "," + j + ").");
         }
     } while(true);
 }

 protected boolean a(FIS5 r1)
     throws IOException, DicomException
 {
     if(a == 0)
     {
         if(Debug.DEBUG >= 5)
             Debug.printMessage("VR.readVRData <empty>", group, element);
         _flddo = true;
         val = new Vector();
         return true;
     }
     _flddo = false;
     if(_fldif == 10)
     {
         if(Debug.DEBUG >= 5)
             Debug.printMessage("VR.readVRData <sequence>", group, element);
         val = _mthint(r1);
         return val != null;
     }
     if(a == -1)
     {
         if(_fldif == 0)
         {
             if(Debug.DEBUG >= 5)
                 Debug.printMessage("VR.readVRData <type unknown and undefined length>", group, element);
             val = _mthif(r1);
             return val != null;
         }
         if(_fldif == 8 || _fldif == 24)
         {
             if(Debug.DEBUG >= 5)
                 Debug.printMessage("VR.readVRData <type OB or OW and undefined length>", group, element);
             val = _mthdo(r1);
             return val != null;
         } else
         {
             throw new DicomException("illegal type with undefined length: " + DDict.getTypeCode(_fldif));
         }
     }
     try
     {
         if(Debug.DEBUG >= 5)
             Debug.printMessage("VR.readVRData <normal>", group, element);
         val = r1.readVector5(_fldif, a);
     }
     catch(Exception exception)
     {
         if(Debug.DEBUG >= 1)
         {
             System.out.println(exception);
             System.out.print("Encoding error in data of +(");
             System.out.print(ObjPrinter.a(group, 4) + ",");
             System.out.print(ObjPrinter.a(element, 4) + "), ");
             System.out.println("tag excluded from dataset");
         }
         return false;
     }
     return true;
 }

 private int a(int i, int j, int l)
 {
     int i1 = DDict.lookupDDict(i, j);
     switch(i1)
     {
     case 1189: 
         return 24;

     case 1184: 
         return 24;
     }
     return 24;
 }
}
