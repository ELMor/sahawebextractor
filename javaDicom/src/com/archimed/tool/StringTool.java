/*
 * StringTool - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.tool;

public class StringTool {
	public static String fstr(String string, int i) {
		String string_0_;
		if (string.length() >= i)
			string_0_ = string.substring(0, string.length() - 3) + "...";
		else {
			string_0_ = string;
			for (int i_1_ = 0; i_1_ < i - string.length(); i_1_++)
				string_0_ += " ";
		}
		return string_0_;
	}

	public static byte[] trimZeros(byte[] is) {
		int i = 0;
		int i_2_ = is.length;
		for (int i_3_ = is.length - 1; i_3_ >= 0 && is[i_3_] == 0; i_3_--)
			i_2_--;
		for (int i_4_ = 0; i_4_ < i_2_ && is[i_4_] == 0; i_4_++)
			i++;
		byte[] is_5_ = new byte[i_2_ - i];
		System.arraycopy(is, i, is_5_, 0, i_2_ - i);
		return is_5_;
	}
}