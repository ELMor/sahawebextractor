package ssf.sescam.lesp.histo.db;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Properties;
import java.text.DecimalFormat;

import org.apache.log4j.Logger;
import ssf.utils.Global;
import java.util.Hashtable;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class SQLFileRunner {

  /**
   * Obtiene el contenido de un archivo
   * @param file Nombre del fichero
   * @return String con el contenido
   * @throws IOException
   */
  private static Logger log = Logger.getLogger(
      "ssf.sescam.lesp.regcen.db.SQLFileRunner");

  /**
   * Ejecuta un script contra la BBDD
   * @param con Conexion
   * @param preLog Cabecer de la linea de log
   * @param SQLFile Fichero con el script
   * @param log Logger de salida
   * @return true si encontro el archivo
   * @throws SQLException
   * @throws FileNotFoundException
   * @throws IOException
   * @throws Exception
   */

  public static boolean run(Connection con, String preLog, String SQLFile,
                            Hashtable ht) throws Exception {

    boolean ret = true;

    String scriptFile = "ssf/sescam/lesp/histo/db/" + SQLFile + ".sql";
    String messagFile = "ssf/sescam/lesp/histo/db/" + SQLFile + ".msg";
    String script = null;
    try {
      script = Global.getContentOfFile(scriptFile);
      if (ht != null) {
        script = Global.multipleReplace(script, ht);
      }
    }
    catch (Exception fnfe) {
      ret = false;
    }
    if (ret) {
      String messag = null;
      try {
        messag = Global.getContentOfFile(messagFile);
        if(ht!=null)
          messag=Global.multipleReplace(messag,ht);
      }
      catch (Exception e) {
        messag = "No se pudo leer el archivo " + messagFile;
      }
      log.debug(preLog + "\n (Usando " + scriptFile + "):\n ");
      log.info(messag);
      Statement st = con.createStatement();
      System.out.println(script);
      try {
        st.execute(script); 
        st.close();
      }
      catch (Exception e) {
        try {
          st.close();
        }
        catch (Exception ee) {}
        throw e;
      }
    }
    return ret;
  }

  /**
   * Ejecuta ordenadamente un conjunto de scripts presentando mensajes
   * @param ignore Ignorar errores/excepciones
   * @param c Conexion a la BBDD
   * @param prop Contiene la definicion de los directorios a ejecutar y numero de scripts
   * @param head Cabecera de las entradas en properties
   * @param preLog Cadena que se presentara en el log al principio
   * @param log Logger de salida
   * @throws Exception
   */
  public static void multipleRun(boolean ignore, Connection c,
                                 Properties prop, String head,
                                 String preLog, Hashtable ht) throws Exception {

    String pkgName = prop.getProperty(head + ".name");

    MRInternal(ignore, c, preLog, pkgName, ht);
  }

  public static void MRInternal(boolean ignore,
                                Connection c,
                                String preLog,
                                String pkgName,
                                Hashtable ht) throws Exception {
    boolean bucle = true;
    int i = 0;
    DecimalFormat df = new DecimalFormat("00");
    while (bucle) {
      try {
        i++;
        bucle = SQLFileRunner.run(c, preLog, pkgName + "/" + df.format(i), ht);
      }
      catch (Exception e) {
        if (!ignore) {
          log.error(" <<Failed>>\n " + e.toString() + " ");
          throw e;
        }
      }
      if (bucle) {
        log.info(" <<OK>>");
      }
    }
  }
}