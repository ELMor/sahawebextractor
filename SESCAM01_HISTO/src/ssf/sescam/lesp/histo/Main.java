package ssf.sescam.lesp.histo;

import java.util.Properties;
import java.io.FileInputStream;

import ssf.sescam.lesp.histo.Sescam;
import ssf.sescam.lesp.histo.Schema;

import ssf.utils.Global;
import org.apache.log4j.Logger;


/**
 * <p>
 * Title: Proceso de Carga a Histórico del Registro de Listas De Espera de SESCAM
 * </p><p>
 * Description: Punto de Entrada del proceso de Carga a Histórico.
 * </p><p>
 * Copyright: Copyright (c) 2002 Soluziona
 * </p><p>
 * Company: Soluziona
 * </p>
 *
 * @author Eladio Linares
 *
 * @version 1.0
 */

public class Main {
  public Properties prop = new Properties();
  private Logger log = Logger.getLogger(getClass());

  /**
   * Entrada principal de ejecucion
   * @param args Argumentos de la linea de comandos
   */
  public static void main(String[] args) {
    new Main(args);
  }

  /**
   * Punto de Entrada, Carga de Configuración, Comprobación del Esquema, Carga a Histórico
   * @param args Argumentos de linea de comandos
   */
  public Main(String[] args) {
    try {
      Global.initLog4J("ssf/sescam/lesp/histo/logging.properties");
      //Se carga la configuracion de config.properties
      prop = Main.cargaConfiguracion();
      log.info("Proceso de actualización del histórico de registros de Listas de Espera");
      log.info("(c) 2002 Soluziona");
      log.info("Leyendo la configuracion de 'config.properties'");
      //Se muestra dicha configuracion
     dumpConfig();
	 //Ahora comprobar la conexion y el esquema de histórico en SESCAM.
     if (!Schema.check(prop)) {
		log.error("No existe conexion con SESCAM -- abortando");
		System.exit( -1);
     }
     log.info("Conexion y esquema comprobados");
     Sescam ses = new Sescam(prop,Schema.toSescam);
     //lanzar el proceso
	 ses.run();
    }
    catch (Exception e) {
      System.out.flush();
      e.printStackTrace();
    }
    finally {
		try {
			Schema.toSescam.close();
		} catch (Exception e) {}
    }
  }

  /**
   * Carga la configuracion desde un archivo 'properties'
   * @throws Exception
   */
  public static Properties cargaConfiguracion() throws Exception {
    //Cargamos la configuracion de usuario
    Properties p = new Properties();
    String cfgp = "config.properties";
    FileInputStream fis = new FileInputStream(cfgp);
    p.load(fis);
    //Le añadimos la configuracion interna
    cfgp = "ssf/sescam/lesp/histo/internal.properties";
    p.load(ClassLoader.getSystemResourceAsStream(cfgp));
    //Carga de los drivers JDBC
    int numDrivers = Integer.parseInt(p.getProperty("jdbc.driver.size"));
    for (int i = 1; i <= numDrivers; i++) {
      Class.forName(p.getProperty("jdbc.driver." + i));
    }
    return p;
  }

  /**
   * Muestra en pantalla la configuracion actual
   */
  void dumpConfig() {
    log.debug(prop.toString());
  }

  public String now() {
    return Global.now();
  }

}