
package ssf.pkman;
import java.net.*;
import java.io.*;
import java.util.*;

//Esta clase sirve para probar el servidor
class client extends Thread {
	Socket sc;
	long desc;
	int  prt;
	String server;

	client(long descriptor, String srv, int port) {
		desc=descriptor;
		prt=port;
		server=srv;
	}

	String normNumber(long l){
		StringBuffer ret=new StringBuffer("0000000000");
		String s=new Long(l).toString();
		ret.setLength(10-s.length());
		ret.append(s);
		return ret.toString();
	}

	String normNumber2(long l){
		StringBuffer ret=new StringBuffer("00000");
		String s=new Long(l).toString();
		ret.setLength(5-s.length());
		ret.append(s);
		return ret.toString();
	}

	public void run(){
		StringBuffer sb=new StringBuffer();
		long t1,t2,t3,t4;
		try{
			t1=System.currentTimeMillis();
			sc=new Socket(server,prt);

			t2=System.currentTimeMillis();
			String toSend=normNumber(desc)+"003";
			sc.getOutputStream().write( toSend.getBytes() );

			t3=System.currentTimeMillis();
			InputStream is=sc.getInputStream();
			for(int i=0;i<20;i++)
				sb.append( (char)is.read() );
			sc.close();
			t4=System.currentTimeMillis();

			System.out.println(
				""+toSend+":"+sb.toString()+
				":crSock:"+normNumber2(t2-t1)+
				":sndInf:"+normNumber2(t3-t2)+
				":resSrv:"+normNumber2(t4-t3)
				);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void main(String arg[]){
		new client(arg);
	}

	client(String arg[]){
		int puerto=8179;
		String srv;

		if( arg.length!=4 ){
			System.out.println("java ssf.pkman.client server port cicles boost");
			return;
		}

		srv=arg[0];
		puerto=Integer.parseInt(arg[1]);
		System.out.println("Enviado......:Recibido............:Tiempos");
		int i=0,k=0;
		int cicles=Integer.parseInt(arg[2]);
		int boost =Integer.parseInt(arg[3]);
		try{
			Random ran=new Random(System.currentTimeMillis());
			for(k=0;k<cicles;k++){
				for(i=0;i<boost;i++)
					new client(5000+ran.nextInt(500),srv,puerto).start();
					//new client(5001,srv,puerto).start();
				Thread.sleep(100);
			}
			System.out.println("Fin de creacion de "+(k*i)+" hebras");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
	