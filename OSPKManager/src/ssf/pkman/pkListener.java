
package ssf.pkman;

import java.net.*;
import java.io.*;

//Esta clase escucha el socket y encola las peticiones en RequestsPool
class pkListener extends Thread {
	ServerSocket ss=null;
	RequestsPool rp=null;
	Socket       sc=null;
	int					 offset;
	int					 debug;

	pkListener(RequestsPool vrp,int opt[], int port)
		throws IOException {
		ss=new ServerSocket(port,100); //el cliente 101 tiene un 'Connection refused'
		rp=vrp;
		offset=opt[0];
		debug=opt[1];
	}

	public void run() {
		try {
			for(;;){
				try {
					sc=ss.accept();
					sc.setSoTimeout(100); //Un read solo puede durar m�ximo 0.1 seg
					InputStream is=sc.getInputStream();
					StringBuffer sb=new StringBuffer();
					StringBuffer sr=new StringBuffer();
					for(int i=0;i<10;i++)
						sb.append( (char)is.read() );
					for(int i=0;i<3;i++)
						sr.append( (char)is.read() );
					rp.insert(new Integer(sb.toString()), //Descriptor
										new Integer(sr.toString()), //Rango que desea el cliente
										sc);												//Socket
				}catch(InterruptedIOException e){
					System.out.println("Socket "+sc+" Timeout");
					sc.close(); //Esto notifica al cliente "connection reset by peer"
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			try {
				ss.close();
				sc.close();
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}
	}
}
		