
package ssf.pkman;

import java.net.*;
import java.util.*;

//Estructura que contiene los datos de un requerimiento
class Request {

	private int			descriptor;
	private Socket  sc;
	private long		rango;

	Request(int des, Socket sclient, long rng){
		descriptor=des;
		sc=sclient;
		rango=rng;
	}

	public Socket getSocket(){
		return sc;
	}

	public long getRango(){
		return rango;
	}

	public int getDescriptor(){
		return descriptor;
	}

}