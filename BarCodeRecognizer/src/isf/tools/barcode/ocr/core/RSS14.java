/* f - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class RSS14 extends Barcode {
	private static final boolean d = false;

	private static final int[] _try = { 382, 355, 337, 319, 274, 256, 238, 157,
			139 };

	private static final int[][] _char = { { 1, 3, 9, 27, 2, 6, 18, 54 },
			{ 4, 12, 36, 29, 8, 24, 72, 58 },
			{ 16, 48, 65, 37, 32, 17, 51, 74 },
			{ 64, 34, 23, 69, 49, 68, 46, 59 } };

	private static HashMap b = null;

	private ArrayList _byte;

	private int[] _goto = new int[8];

	private int[] e = new int[8];

	private int[] _else = new int[6];

	private int[] _void = new int[6];

	private int[] _long = new int[3];

	private int[] c = new int[3];

	private int[] _case = new int[3];

	RSS14(PropertiesHolder var_c) {
		super(var_c);
		_byte = new ArrayList();
	}

	int code() {
		return 18;
	}

	int _if(int i) {
		if (i == 27)
			return 27;
		return -1;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_ = 0;
		if (is[0] == 0)
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 5; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		while (i_4_ + 5 <= i_1_) {
			if (i_4_ % 2 == 0) {
				int i_8_ = ((is[i_4_ + 1] + is[i_4_ + 2]) / (is[i_4_ + 3] + is[i_4_ + 4]));
				if (i_8_ >= 4 && i_8_ <= 9) {
					int i_9_ = a(is, i_4_, true);
					if (i_9_ >= 0) {
						this.readData(i, i_0_, is, i_4_, 5, i_2_, i_9_, true, i_3_);
						if (i_5_ < i_7_)
							i_7_ = i_5_;
					}
				}
			} else {
				int i_10_ = ((is[i_4_ + 2] + is[i_4_ + 3]) / (is[i_4_ + 0] + is[i_4_ + 1]));
				if (i_10_ >= 4 && i_10_ <= 9) {
					int i_11_ = a(is, i_4_ + 2, false);
					if (i_11_ >= 0) {
						this.readData(i, i_0_, is, i_4_, 5, i_2_, i_11_, false, i_3_);
						if (i_5_ < i_7_)
							i_7_ = i_5_;
					}
				}
			}
			i_5_ -= is[i_4_];
			i_4_++;
			i_5_ += is[i_4_ + 4];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_12_) {
		int i_13_ = var_d.c % 100 + var_d_12_.c % 100;
		i_13_ %= 79;
		if (i_13_ >= 8)
			i_13_++;
		if (i_13_ >= 72)
			i_13_++;
		if (i_13_ / 9 != var_d._try() || i_13_ % 9 != var_d_12_._try())
			return false;
		long l = (long) (var_d.c / 100);
		long l_14_ = (long) (var_d_12_.c / 100);
		long l_15_ = 4537077L * l + l_14_;
		String string = Long.toString(l_15_);
		ArrayList arraylist = getAL3();
		arraylist.add(new Recognized(string, 13, var_d, var_d_12_, code(), prop));
		return true;
	}

	void _try() {
		super._try();
		Iterator iterator = getAL3().iterator();
		while (iterator.hasNext()) {
			Recognized var_bf = (Recognized) iterator.next();
			SepInten var_d = var_bf.getSepIntensity();
			SepInten var_d_16_ = var_bf._new();
			a(var_d, var_d_16_);
		}
	}

	private void a(SepInten var_d, SepInten var_d_17_) {
		UserRect var_al = var_d.getUserRect();
		UserRect var_al_18_ = var_d_17_.getUserRect();
		var_al.resuma(var_al_18_);
		var_al_18_.resuma(var_al);
		a(var_d, true);
		a(var_d_17_, false);
	}

	private void a(SepInten var_d, boolean bool) {
		double d = var_d._else();
		int i = var_d._do() * 20 / 15;
		int i_19_ = var_d.getDirection();
		double d_20_ = Math.sqrt(d * d + 1.0);
		double d_21_ = d / d_20_;
		double d_22_ = 1.0 / d_20_;
		int i_23_ = (int) ((double) i * d_22_ * d_22_);
		int i_24_ = i_23_;
		int i_25_ = (int) ((double) i * d_22_ * d_21_);
		switch (i_19_) {
		case 90:
			i_25_ = -i_25_;
			break;
		case 270:
			i_24_ = -i_24_;
			break;
		case 180: {
			int i_26_ = i_24_;
			i_24_ = -i_25_;
			i_25_ = i_26_;
			break;
		}
		case 360: {
			int i_27_ = i_24_;
			i_24_ = i_25_;
			i_25_ = -i_27_;
			break;
		}
		}
		if (bool) {
			i_24_ = -i_24_;
			i_25_ = -i_25_;
		}
		var_d.a(i_24_, i_25_);
	}

	void a(Recognizer var_a1) {
		ArrayList arraylist = new ArrayList();
		Iterator iterator = al01.iterator();
		while (iterator.hasNext()) {
			SepInten var_d = (SepInten) iterator.next();
			if (a(var_d, true, var_a1) == true)
				arraylist.add(var_d);
		}
		al01 = arraylist;
		arraylist = new ArrayList();
		Iterator iterator_28_ = al02.iterator();
		while (iterator_28_.hasNext()) {
			SepInten var_d = (SepInten) iterator_28_.next();
			if (a(var_d, false, var_a1) == true)
				arraylist.add(var_d);
		}
		al02 = arraylist;
	}

	private boolean a(SepInten var_d, boolean bool, Recognizer var_a1) {
		int i = var_d._do();
		if (!bool)
			i = -i;
		int i_29_ = var_d.getDirection();
		int i_30_ = var_d.getBWBound();
		PointDouble var_ay = var_d.getUserRect().getP1();
		PointDouble var_ay_31_ = var_d.getUserRect().getP2();
		double d = var_ay.getX();
		double d_32_ = var_ay.getY();
		double d_33_ = var_ay_31_.getX();
		double d_34_ = var_ay_31_.getY();
		PointDouble var_ay_35_ = null;
		PointDouble var_ay_36_ = null;
		switch (i_29_) {
		case 90:
			var_ay_35_ = new PointDouble(d + (double) i, d_32_);
			var_ay_36_ = new PointDouble(d_33_ + (double) i, d_34_);
			break;
		case 270:
			var_ay_35_ = new PointDouble(d - (double) i, d_32_);
			var_ay_36_ = new PointDouble(d_33_ - (double) i, d_34_);
			break;
		case 180:
			var_ay_35_ = new PointDouble(d, d_32_ + (double) i);
			var_ay_36_ = new PointDouble(d_33_, d_34_ + (double) i);
			break;
		case 360:
			var_ay_35_ = new PointDouble(d, d_32_ - (double) i);
			var_ay_36_ = new PointDouble(d_33_, d_34_ - (double) i);
			break;
		}
		PointDouble var_ay_37_ = var_d.getUserRect().mirrorPoint(var_ay_35_);
		PointDouble var_ay_38_ = var_d.getUserRect().mirrorPoint(var_ay_36_);
		int i_39_ = a(var_ay_37_, var_ay_38_, bool, true, var_d, var_a1);
		if (i_39_ == -1)
			return false;
		int i_40_ = i_39_ % 100;
		i_39_ /= 100;
		double d_41_ = var_ay.getX() - var_ay_37_.getX();
		double d_42_ = var_ay.getY() - var_ay_37_.getY();
		PointDouble var_ay_43_ = new PointDouble(var_ay_35_.getX() + d_41_, var_ay_35_.getY() + d_42_);
		d_41_ = var_ay_31_.getX() - var_ay_38_.getX();
		d_42_ = var_ay_31_.getY() - var_ay_38_.getY();
		PointDouble var_ay_44_ = new PointDouble(var_ay_36_.getX() + d_41_, var_ay_36_.getY() + d_42_);
		int i_45_ = a(var_ay_43_, var_ay_44_, bool, false, var_d, var_a1);
		if (i_45_ == -1)
			return false;
		i_40_ += i_45_ % 100;
		if (i_40_ >= 79)
			i_40_ -= 79;
		i_45_ /= 100;
		var_d.c = (1597 * i_39_ + i_45_) * 100 + i_40_;
		return true;
	}

	private int a(PointDouble var_ay, PointDouble var_ay_46_, boolean bool, boolean bool_47_,
			SepInten var_d, Recognizer var_a1) {
		int i = var_d.getDirection();
		Counters var_g = new Counters();
		double d = var_ay.getX();
		double d_48_ = var_ay.getY();
		double d_49_ = var_ay_46_.getX();
		double d_50_ = var_ay_46_.getY();
		if (i == 90 || i == 270) {
			double d_51_ = (d_49_ - d) / (d_50_ - d_48_);
			double d_52_ = d - d_48_ * d_51_;
			int i_53_ = bool ? -1 : 1;
			i_53_ = i_53_ * (i == 90 ? 1 : -1);
			i_53_ = i_53_ * (bool_47_ ? 1 : -1);
			int i_54_ = 0;
			int i_55_ = d_50_ > d_48_ ? 1 : -1;
			for (int i_56_ = (int) d_48_; i_56_ <= (int) d_50_; i_56_ += i_55_) {
				int i_57_ = (int) (d_52_ + (double) i_56_ * d_51_ + 0.5);
				int i_58_ = a(i_57_, i_53_, i_56_, i_54_, bool, bool_47_,
						var_a1, var_d);
				if (i_58_ != -1)
					var_g.incCounter(i_58_);
			}
		} else {
			double d_59_ = (d_50_ - d_48_) / (d_49_ - d);
			double d_60_ = d_48_ - d * d_59_;
			int i_61_ = bool ? -1 : 1;
			i_61_ = i_61_ * (i == 180 ? 1 : -1);
			i_61_ = i_61_ * (bool_47_ ? 1 : -1);
			int i_62_ = 0;
			int i_63_ = d_49_ > d ? 1 : -1;
			for (int i_64_ = (int) d; i_64_ <= (int) d_49_; i_64_ += i_63_) {
				int i_65_ = (int) (d_60_ + (double) i_64_ * d_59_ + 0.5);
				int i_66_ = a(i_64_, i_62_, i_65_, i_61_, bool, bool_47_,
						var_a1, var_d);
				if (i_66_ != -1)
					var_g.incCounter(i_66_);
			}
		}
		int i_67_;
		try {
			i_67_ = var_g.getMax();
		} catch (Exception exception) {
			i_67_ = -1;
		}
		return i_67_;
	}

	private int a(int i, int i_68_, int i_69_, int i_70_, boolean bool,
			boolean bool_71_, Recognizer var_a1, SepInten var_d) {
		int i_72_ = bool ? bool_71_ ? 1 : 2 : bool_71_ ? 3 : 4;
		int i_73_ = var_d.getBWBound();
		int i_74_ = var_d._do();
		int i_75_ = bool ? 0 : 255;
		int i_76_ = 0;
		int i_77_ = var_a1.a(i, i_69_) < i_73_ ? 0 : 255;
		while_13_: do {
			if (i_77_ != i_75_) {
				do {
					i += i_68_;
					i_69_ += i_70_;
					i_77_ = var_a1.a(i, i_69_) < i_73_ ? 0 : 255;
					if (i_77_ == i_75_)
						break while_13_;
				} while (++i_76_ <= i_74_ / 8);
				return -1;
			}
			do {
				i_77_ = var_a1.a(i - i_68_, i_69_ - i_70_) < i_73_ ? 0 : 255;
				if (i_77_ != i_75_)
					break while_13_;
				i -= i_68_;
				i_69_ -= i_70_;
			} while (++i_76_ <= i_74_ / 8);
			return -1;
		} while (false);
		i_76_ = 0;
		int i_78_ = 7;
		_goto[i_78_] = 0;
		int i_79_ = i_75_;
		for (;;) {
			i_77_ = var_a1.a(i, i_69_) < i_73_ ? 0 : 255;
			if (i_77_ != i_79_) {
				if (--i_78_ < 0)
					break;
				_goto[i_78_] = 1;
				i_79_ = i_77_;
			} else
				_goto[i_78_]++;
			if (++i_76_ > 2 * i_74_)
				return -1;
			i += i_68_;
			i_69_ += i_70_;
		}
		int i_80_ = a(bool_71_ ? 16 : 15, i_72_);
		return i_80_;
	}

	private int a(int i, int i_81_) {
		for (int i_82_ = 0; i_82_ < 8; i_82_++)
			e[i_82_] = _goto[i_82_];
		int i_83_ = e[0];
		for (int i_84_ = 1; i_84_ < 8; i_84_++)
			i_83_ += e[i_84_];
		for (int i_85_ = 0; i_85_ < 6; i_85_++) {
			_else[i_85_] = e[i_85_] + e[i_85_ + 1];
			_void[i_85_] = _else[i_85_] * i / i_83_;
			if (_else[i_85_] * i % i_83_ > i_83_ / 2)
				_void[i_85_]++;
			if (_void[i_85_] < 2 || _void[i_85_] > 9)
				return -1;
		}
		_if(i, _void, e);
		int[] is = new int[4];
		int[] is_86_ = new int[4];
		int i_87_ = 0;
		int i_88_ = 0;
		for (int i_89_ = 0; i_89_ < 4; i_89_++) {
			is[i_89_] = e[i_89_ * 2];
			is_86_[i_89_] = e[i_89_ * 2 + 1];
			i_87_ += is[i_89_];
			i_88_ += is_86_[i_89_];
		}
		if (i_88_ % 2 != 0 || i == 16 && i_87_ % 2 != 0 || i == 15
				&& i_87_ % 2 == 0) {
			a(i, e, _goto);
			i_87_ = 0;
			i_88_ = 0;
			for (int i_90_ = 0; i_90_ < 4; i_90_++) {
				is[i_90_] = e[i_90_ * 2];
				is_86_[i_90_] = e[i_90_ * 2 + 1];
				i_87_ += is[i_90_];
				i_88_ += is_86_[i_90_];
			}
			if (i_88_ % 2 != 0 || i == 16 && i_87_ % 2 != 0 || i == 15
					&& i_87_ % 2 == 0)
				return -1;
		}
		int i_91_ = -1;
		if (i == 15) {
			int i_92_;
			int i_93_;
			int i_94_;
			int i_95_;
			if (i_87_ == 5) {
				i_92_ = 2;
				i_93_ = 7;
				i_94_ = 0;
				i_95_ = 4;
			} else if (i_87_ == 7) {
				i_92_ = 4;
				i_93_ = 5;
				i_94_ = 336;
				i_95_ = 20;
			} else if (i_87_ == 9) {
				i_92_ = 6;
				i_93_ = 3;
				i_94_ = 1036;
				i_95_ = 48;
			} else if (i_87_ == 11) {
				i_92_ = 8;
				i_93_ = 1;
				i_94_ = 1516;
				i_95_ = 81;
			} else
				return -1;
			int i_96_ = a(is, 4, i_92_, 0);
			int i_97_ = a(is_86_, 4, i_93_, 1);
			i_91_ = i_97_ * i_95_ + i_96_ + i_94_;
		} else {
			int i_98_;
			int i_99_;
			int i_100_;
			int i_101_;
			if (i_87_ == 12) {
				i_98_ = 8;
				i_99_ = 1;
				i_100_ = 0;
				i_101_ = 1;
			} else if (i_87_ == 10) {
				i_98_ = 6;
				i_99_ = 3;
				i_100_ = 161;
				i_101_ = 10;
			} else if (i_87_ == 8) {
				i_98_ = 4;
				i_99_ = 5;
				i_100_ = 961;
				i_101_ = 34;
			} else if (i_87_ == 6) {
				i_98_ = 3;
				i_99_ = 6;
				i_100_ = 2015;
				i_101_ = 70;
			} else if (i_87_ == 4) {
				i_98_ = 1;
				i_99_ = 8;
				i_100_ = 2715;
				i_101_ = 126;
			} else
				return -1;
			int i_102_ = a(is, 4, i_98_, 1);
			int i_103_ = a(is_86_, 4, i_99_, 0);
			i_91_ = i_102_ * i_101_ + i_103_ + i_100_;
		}
		if (i_91_ != -1) {
			int i_104_ = 0;
			int i_105_ = i_81_ - 1;
			for (int i_106_ = 0; i_106_ < 8; i_106_++)
				i_104_ += e[i_106_] * _char[i_105_][i_106_];
			i_104_ %= 79;
			i_91_ = i_91_ * 100 + i_104_;
		}
		return i_91_;
	}

	private void _if(int i, int[] is, int[] is_107_) {
		int i_108_ = 10;
		int i_109_ = 1;
		int i_110_ = is_107_[0] = 1;
		for (int i_111_ = 1; i_111_ < 6; i_111_ += 2) {
			is_107_[i_111_] = is[i_111_ - 1] - is_107_[i_111_ - 1];
			is_107_[i_111_ + 1] = is[i_111_] - is_107_[i_111_];
			i_110_ += is_107_[i_111_] + is_107_[i_111_ + 1];
			if (is_107_[i_111_] < i_108_)
				i_108_ = is_107_[i_111_];
			if (is_107_[i_111_ + 1] < i_109_)
				i_109_ = is_107_[i_111_ + 1];
		}
		is_107_[7] = i - i_110_;
		if (is_107_[7] < i_108_)
			i_108_ = is_107_[7];
		int i_112_ = 0;
		if (i == 16 && i_108_ != 1)
			i_112_ = i_108_ - 1;
		else if (i == 15 && i_109_ != 1)
			i_112_ = -i_109_ + 1;
		if (i_112_ != 0) {
			for (int i_113_ = 0; i_113_ < 8; i_113_ += 2) {
				is_107_[i_113_] += i_112_;
				is_107_[i_113_ + 1] -= i_112_;
			}
		}
	}

	private void a(int i, int[] is, int[] is_114_) {
		int i_115_ = 0;
		for (int i_116_ = 0; i_116_ < 8; i_116_++) {
			i_115_ += is_114_[i_116_];
			is_114_[i_116_] *= i;
		}
		for (int i_117_ = 0; i_117_ < 8; i_117_++) {
			int i_118_ = is_114_[i_117_] / i_115_;
			int i_119_ = is_114_[i_117_] % i_115_;
			if (i_118_ == 0 || i_119_ > i_115_ / 2) {
				i_118_++;
				i_119_ -= i_115_;
			}
			if (i_117_ < 7)
				is_114_[i_117_ + 1] += i_119_;
			is[i_117_] = i_118_;
		}
	}

	private int a(int[] is, int i, int i_120_, int i_121_) {
		int i_122_ = 0;
		int i_123_ = 0;
		int i_125_;
		int i_124_ = i_125_ = 0;
		for (/**/; i_125_ < i; i_125_++)
			i_124_ += is[i_125_];
		for (int i_126_ = 0; i_126_ < i - 1; i_126_++) {
			int i_127_ = 1;
			i_123_ |= 1 << i_126_;
			while (i_127_ < is[i_126_]) {
				int i_128_ = _if(i_124_ - i_127_ - 1, i - i_126_ - 2);
				if (i_121_ == 0 && i_123_ == 0
						&& i_124_ - i_127_ - (i - i_126_ - 1) >= i - i_126_ - 1)
					i_128_ -= _if(i_124_ - i_127_ - (i - i_126_), i - i_126_
							- 2);
				if (i - i_126_ - 1 > 1) {
					int i_129_ = 0;
					for (int i_130_ = i_124_ - i_127_ - (i - i_126_ - 2); i_130_ > i_120_; i_130_--)
						i_129_ += _if(i_124_ - i_127_ - i_130_ - 1, i - i_126_
								- 3);
					i_128_ -= i_129_ * (i - 1 - i_126_);
				} else if (i_124_ - i_127_ > i_120_)
					i_128_--;
				i_122_ += i_128_;
				i_127_++;
				i_123_ &= 1 << i_126_ ^ 0xffffffff;
			}
			i_124_ -= i_127_;
		}
		return i_122_;
	}

	private int _if(int i, int i_131_) {
		int i_132_;
		int i_133_;
		if (i - i_131_ > i_131_) {
			i_132_ = i_131_;
			i_133_ = i - i_131_;
		} else {
			i_132_ = i - i_131_;
			i_133_ = i_131_;
		}
		int i_134_ = 1;
		int i_135_ = 1;
		for (int i_136_ = i; i_136_ > i_133_; i_136_--) {
			i_134_ *= i_136_;
			if (i_135_ <= i_132_) {
				i_134_ /= i_135_;
				i_135_++;
			}
		}
		for (/**/; i_135_ <= i_132_; i_135_++)
			i_134_ /= i_135_;
		return i_134_;
	}

	private final int a(int[] is, int i, boolean bool) {
		int i_137_ = 0;
		for (int i_138_ = 0; i_138_ < 3; i_138_++) {
			int i_139_ = bool ? i_138_ : 2 - i_138_;
			_long[i_138_] = is[i + i_139_] * 13;
			i_137_ += is[i + i_138_];
		}
		boolean bool_140_ = false;
		for (int i_141_ = 0; i_141_ < 3; i_141_++)
			c[i_141_] = _long[i_141_];
		for (int i_142_ = 0; i_142_ < 3; i_142_++) {
			int i_143_ = c[i_142_] / i_137_;
			int i_144_ = c[i_142_] % i_137_;
			if (i_143_ == 0 || i_144_ > i_137_ / 2) {
				i_143_++;
				i_144_ -= i_137_;
			}
			if (i_142_ == 0) {
				if (i_143_ >= 5) {
					bool_140_ = true;
					break;
				}
				if (i_143_ == 4) {
					i_143_ = 3;
					i_144_ += i_137_;
				}
			} else if (i_142_ == 1) {
				if (i_143_ >= 10) {
					bool_140_ = true;
					break;
				}
				if (i_143_ == 9) {
					i_143_ = 8;
					i_144_ += i_137_;
				}
			} else if (i_143_ >= 11)
				bool_140_ = true;
			else if (i_143_ == 10) {
				i_143_ = 9;
				c[1]++;
			} else if (i_143_ == 1) {
				i_143_ = 2;
				c[1]--;
			}
			_case[i_142_] = i_144_;
			if (i_142_ < 2)
				c[i_142_ + 1] += i_144_;
			c[i_142_] = i_143_;
		}
		if (!bool_140_) {
			int i_145_ = 0;
			int i_146_ = -1;
			for (int i_147_ = 0; i_147_ < 3; i_147_++)
				i_145_ = i_145_ * 10 + c[i_147_];
			Object object = b.get(new Integer(i_145_));
			if (object != null) {
				i_146_ = ((Integer) object).intValue();
				return i_146_;
			}
		}
		return -1;
	}

	String a(String string) {
		StringBuffer stringbuffer = new StringBuffer();
		int i = 13 - string.length();
		for (int i_148_ = 0; i_148_ < i; i_148_++)
			stringbuffer.append('0');
		stringbuffer.append(string);
		int i_149_ = 0;
		for (int i_150_ = 0; i_150_ < 13; i_150_++) {
			i = stringbuffer.charAt(i_150_) - 48;
			if (i_150_ % 2 == 0)
				i *= 3;
			i_149_ += i;
		}
		i_149_ %= 10;
		if (i_149_ != 0)
			i_149_ = 10 - i_149_;
		stringbuffer.append((char) (48 + i_149_));
		return new String(stringbuffer);
	}

	static {
		b = new HashMap();
		for (int i = 0; i < 9; i++)
			b.put(new Integer(_try[i]), new Integer(i));
	}
}
