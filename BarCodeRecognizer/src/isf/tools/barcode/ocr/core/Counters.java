/* g - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.HashMap;
import java.util.Iterator;

class Counters {
	private HashMap hmap = new HashMap();

	void incCounter(int i) {
		Integer n;
		Object object = hmap.get(n = new Integer(i));
		if (object == null)
			hmap.put(n, new Integer(1));
		else
			hmap.put(n, new Integer(((Integer) object).intValue() + 1));
	}

	int getMax() throws Exception {
		if (hmap.isEmpty())
			throw new Exception();
		Iterator it = hmap.keySet().iterator();
		int i = -1;
		int max = -1;
		while (it.hasNext()) {
			Integer integer = (Integer) it.next();
			int current = ((Integer) hmap.get(integer)).intValue();
			if (current > i) {
				i = current;
				max = integer.intValue();
			}
		}
		return max;
	}
}
