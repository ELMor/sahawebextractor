/* ab - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

class ab {
	private boolean _if;

	private int a;

	private ArrayList _do;

	ab(int i, ArrayList arraylist) {
		a(false);
		a = i;
		_do = arraylist;
	}

	synchronized boolean a() {
		return _if;
	}

	private synchronized void a(boolean bool) {
		_if = bool;
	}

	synchronized int a(ArrayList input) {
		int i = 0;
		ArrayList lst = new ArrayList();
		Iterator t = input.iterator();
		while (t.hasNext()) {
			Recognized var_bf = (Recognized) t.next();
			Rectangle r = var_bf.getRectangle();
			Iterator it = _do.iterator();
			boolean bool = true;
			while_0_: do {
				Rectangle rectangle_2_;
				do {
					if (!it.hasNext())
						break while_0_;
					Recognized var_bf_3_ = (Recognized) it.next();
					rectangle_2_ = var_bf_3_.getRectangle();
				} while (!rectangle_2_.intersects(r));
				bool = false;
			} while (false);
			if (bool) {
				lst.add(var_bf);
				i++;
			}
		}
		_do.addAll(lst);
		if (_do.size() >= a)
			a(true);
		return i;
	}
}
