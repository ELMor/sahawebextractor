/* ar - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class ar {
	private static final int[] _if = { 3, 5, 8, 12, 18, 22, 30, 36, 44, 62, 86,
			114, 144, 174, 102, 140, 92, 114, 144, 174, 136, 175, 163, 156, 5,
			10, 16, 22, 32, 49 };

	private static final int[] _byte = { 5, 7, 10, 12, 14, 18, 20, 24, 28, 36,
			42, 48, 56, 68, 42, 56, 36, 48, 56, 68, 56, 68, 62, 62, 7, 11, 14,
			18, 24, 28 };

	private static final int[] b = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			2, 2, 4, 4, 4, 4, 6, 6, 8, 10, 1, 1, 1, 1, 1, 1 };

	private int[] _for;

	private int[] _else;

	private int _try;

	private static final int _goto = 256;

	private static final int _int = 37;

	private int[] _char = new int[256];

	private int[] _new = new int[256];

	private int[] a = new int[256];

	private int[] _case = new int[1739];

	private int[] _do = new int[37];

	private int[] _long = new int[37];

	private int _void;

	ar(int i) {
		if (i == 5)
			_try = 301;
		else if (i == 17)
			_try = 285;
		_for = new int[255];
		_else = new int[256];
		for (int i_0_ = 0; i_0_ < 256; i_0_++)
			_else[i_0_] = 0;
		int i_1_ = 1;
		for (int i_2_ = 0; i_2_ < 255; i_2_++) {
			_for[i_2_] = i_1_;
			_else[i_1_] = i_2_;
			i_1_ <<= 1;
			if (i_1_ > 255)
				i_1_ ^= _try;
		}
	}

	int a(int[] is, int i) {
		int i_3_ = _if(is, i);
		return i_3_;
	}

	private int a(int i, int i_4_) {
		if (i == 0 || i_4_ == 0)
			return 0;
		int i_5_ = _else[i] + _else[i_4_];
		if (i_5_ > 254)
			i_5_ -= 255;
		i_5_ = _for[i_5_];
		return i_5_;
	}

	private int _if(int i, int i_6_) {
		if (i_6_ == 0)
			return 0;
		if (i == 0)
			return 0;
		int i_7_ = _else[i] - _else[i_6_];
		if (i_7_ < 0)
			i_7_ += 255;
		if (i_7_ > 254)
			i_7_ -= 255;
		i_7_ = _for[i_7_];
		return i_7_;
	}

	private int _if(int[] is, int i) {
		int i_8_ = _if[i];
		int i_9_ = _byte[i];
		int i_10_ = b[i];
		int i_11_ = i_8_;
		int i_12_ = 0;
		for (int i_13_ = 0; i_13_ < i_10_; i_13_++) {
			if (i_13_ == 8)
				i_11_--;
			_void = i_11_ + i_9_;
			a(is, _void, i_9_ / 2, i_10_, i_13_);
			int i_14_ = _if(i_9_ / 2);
			if (i_14_ > 0) {
				if (_case[0] == 0)
					return -1;
				a(i_14_);
				if (_do(i_14_) == 0)
					return -1;
				_for(i_14_);
				if (_if(i_14_) != i_14_)
					return -1;
				a(i_14_);
				for (int i_15_ = 0; i_15_ < i_14_; i_15_++)
					is[i_13_
							+ ((_void - (_long[i_15_] == 0 ? 255 : _long[i_15_])) * i_10_)] ^= _do[i_15_];
			}
			i_12_ += i_14_;
		}
		return i_12_;
	}

	int a(int[] is, int i, int i_16_) {
		int i_17_ = 1;
		int i_18_ = i;
		int i_19_ = 0;
		for (int i_20_ = 0; i_20_ < i_17_; i_20_++) {
			if (i_20_ == 8)
				i_18_--;
			_void = i_18_ + i_16_;
			a(is, _void, i_16_ / 2, i_17_, i_20_);
			int i_21_ = _if(i_16_ / 2);
			if (i_21_ > 0) {
				if (_case[0] == 0)
					return -1;
				a(i_21_);
				if (_do(i_21_) == 0)
					return -1;
				_for(i_21_);
				if (_if(i_21_) != i_21_)
					return -1;
				a(i_21_);
				for (int i_22_ = 0; i_22_ < i_21_; i_22_++)
					is[i_20_
							+ ((_void - (_long[i_22_] == 0 ? 255 : _long[i_22_])) * i_17_)] ^= _do[i_22_];
			}
			i_19_ += i_21_;
		}
		return i_19_;
	}

	private void a(int[] is, int i, int i_23_, int i_24_, int i_25_) {
		for (int i_26_ = 0; i_26_ < i; i_26_++) {
			int i_27_ = (is[i_25_ + i_26_ * i_24_] == 0 ? 255 : _else[is[i_25_
					+ i_26_ * i_24_]]);
			if (i_27_ != 255) {
				i_27_ += i - i_26_;
				i_27_ = i_27_ > 254 ? i_27_ - 255 : i_27_;
			}
			_char[i_26_] = i_27_;
		}
		for (int i_28_ = 0; i_28_ < i_23_ * 2; i_28_++) {
			a[i_28_] = 0;
			for (int i_29_ = 0; i_29_ < i; i_29_++) {
				if (_char[i_29_] != 255) {
					a[i_28_] ^= _for[_char[i_29_]];
					int i_30_ = _char[i_29_] + (i - i_29_);
					_char[i_29_] = i_30_ > 254 ? i_30_ - 255 : i_30_;
				}
			}
		}
		int i_32_;
		int i_31_ = i_32_ = 0;
		while (i_31_ < 37 * i_23_) {
			for (int i_33_ = 0; i_33_ <= i_23_; i_33_++)
				_case[i_31_ + i_33_] = a[i_32_ + i_33_];
			i_31_ += 37;
			i_32_++;
		}
	}

	private int _if(int i) {
		int i_35_;
		int i_34_ = i_35_ = 0;
		while (i_34_ < i) {
			do {
				if (_case[i_34_ + 37 * i_35_] == 0) {
					int i_36_;
					for (i_36_ = i_35_ + 1; i_36_ < i; i_36_++) {
						if (_case[i_34_ + 37 * i_36_] != 0) {
							for (int i_37_ = i_34_; i_37_ <= i; i_37_++) {
								int i_38_ = _case[i_37_ + 37 * i_36_];
								_case[i_37_ + 37 * i_36_] = _case[i_37_ + 37
										* i_35_];
								_case[i_37_ + 37 * i_35_] = i_38_;
							}
							break;
						}
					}
					if (i_36_ == i) {
						i_35_--;
						break;
					}
				}
				for (int i_39_ = i_35_ + 1; i_39_ < i; i_39_++) {
					if (_case[i_34_ + 37 * i_39_] != 0) {
						int i_40_ = _if(_case[i_34_ + 37 * i_39_], _case[i_34_
								+ 37 * i_35_]);
						_case[i_34_ + 37 * i_39_] = 0;
						for (int i_41_ = i_34_ + 1; i_41_ <= i; i_41_++)
							_case[i_41_ + 37 * i_39_] ^= a(i_40_, _case[i_41_
									+ 37 * i_35_]);
					}
				}
			} while (false);
			i_34_++;
			i_35_++;
		}
		return i_35_;
	}

	private void a(int i) {
		for (int i_42_ = i - 1; i_42_ >= 0; i_42_--) {
			int i_43_ = _case[i + 37 * i_42_];
			for (int i_44_ = i - 1; i_44_ > i_42_; i_44_--)
				i_43_ ^= a(_do[i_44_], _case[i_44_ + 37 * i_42_]);
			_do[i_42_] = _if(i_43_, _case[i_42_ + 37 * i_42_]);
		}
	}

	private int _do(int i) {
		int i_45_ = 0;
		int[] is = new int[37];
		for (int i_46_ = 0; i_46_ < i; i_46_++)
			is[i_46_] = _else[_do[i - 1 - i_46_]];
		for (int i_47_ = 1; i_47_ <= _void; i_47_++) {
			int i_48_ = 1;
			for (int i_49_ = 0; i_49_ < i; i_49_++) {
				if ((is[i_49_] -= i_49_ + 1) < 0)
					is[i_49_] += 255;
				i_48_ ^= _for[is[i_49_]];
			}
			if (i_48_ == 0) {
				_long[i_45_] = i_47_ < 255 ? i_47_ : 0;
				if (++i_45_ >= i)
					break;
			}
		}
		return i_45_ >= i ? 1 : 0;
	}

	private void _for(int i) {
		int[] is = new int[37];
		int[] is_50_ = new int[37];
		for (int i_51_ = 0; i_51_ < i; i_51_++)
			is[i_51_] = is_50_[i_51_] = _long[i_51_];
		for (int i_52_ = 0; i_52_ < i; i_52_++) {
			int i_53_;
			for (i_53_ = 0; i_53_ < i; i_53_++) {
				_case[i_53_ + 37 * i_52_] = _for[is_50_[i_53_]];
				if ((is_50_[i_53_] += is[i_53_]) > 254)
					is_50_[i_53_] -= 255;
			}
			_case[i_53_ + 37 * i_52_] = a[i_52_];
		}
	}
}
