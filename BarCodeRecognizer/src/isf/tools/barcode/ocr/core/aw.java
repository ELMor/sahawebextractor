/* aw - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class aw {
	private static final boolean _byte = false;

	private int _new;

	private Recognizer a;

	private int _case;

	private QRFinderPattern _try;

	private QRFinderPattern _for;

	private QRFinderPattern _if;

	private bd[][] _int;

	private PropertiesHolder _do;

	aw(int i, Recognizer var_a1, int i_0_, QRFinderPattern var_m, QRFinderPattern var_m_1_, QRFinderPattern var_m_2_, PropertiesHolder var_c) {
		_new = i;
		a = var_a1;
		_case = i_0_;
		_try = var_m;
		_for = var_m_1_;
		_if = var_m_2_;
		_do = var_c;
		if (i != 1) {
			int i_3_ = a4._if(i);
			_int = new bd[i_3_][i_3_];
			for (int i_4_ = 0; i_4_ < i_3_; i_4_++) {
				for (int i_5_ = 0; i_5_ < i_3_; i_5_++)
					_int[i_4_][i_5_] = new bd(a4.a(i, i_5_, i_4_));
			}
			_int[0][0] = new bd(a4.a(i, 0, 0));
			_int[0][i_3_ - 1] = new bd(a4.a(i, i_3_ - 1, 0));
			_int[i_3_ - 1][0] = new bd(a4.a(i, 0, i_3_ - 1));
			_int[0][0].a(var_m._do());
			_int[0][i_3_ - 1].a(var_m_1_._do());
			_int[i_3_ - 1][0].a(var_m_2_._do());
			if (i < 7)
				a(_int[1][1], _int[0][0], _int[0][1], _int[1][0], true);
			else {
				a(_int[0][1], _int[0][0], _int[0][i_3_ - 1], _int[i_3_ - 1][0],
						true);
				a(_int[1][0], _int[0][0], _int[0][i_3_ - 1], _int[i_3_ - 1][0],
						true);
				a(_int[1][1], _int[0][0], _int[0][1], _int[1][0], true);
				for (int i_6_ = 2; i_6_ < i_3_; i_6_++) {
					if (i_6_ == i_3_ - 2) {
						a(_int[0][i_6_], _int[0][i_6_ - 1], _int[0][i_6_ + 1],
								_int[1][i_6_ - 1], true);
						a(_int[i_6_][0], _int[i_6_ - 1][0], _int[i_6_ + 1][0],
								_int[i_6_ - 1][1], true);
					} else if (i_6_ < i_3_ - 2) {
						a(_int[0][i_6_], _int[0][i_6_ - 1], _int[1][i_6_ - 2],
								_int[1][i_6_ - 1], true);
						a(_int[i_6_][0], _int[i_6_ - 1][0], _int[i_6_ - 2][1],
								_int[i_6_ - 1][1], true);
					}
					for (int i_7_ = 1; i_7_ < i_6_; i_7_++) {
						a(_int[i_7_][i_6_], _int[i_7_ - 1][i_6_ - 1],
								_int[i_7_ - 1][i_6_], _int[i_7_][i_6_ - 1],
								true);
						a(_int[i_6_][i_7_], _int[i_6_ - 1][i_7_ - 1],
								_int[i_6_ - 1][i_7_], _int[i_6_][i_7_ - 1],
								true);
					}
					a(_int[i_6_][i_6_], _int[i_6_ - 1][i_6_ - 1],
							_int[i_6_ - 1][i_6_], _int[i_6_][i_6_ - 1], true);
				}
			}
			if (i < 7) {
				bd var_bd = new bd(new af(_int[1][1]._if().a + 3, _int[1][1]
						._if()._if + 3));
				a(var_bd, _int[1][1], _int[0][1], _int[1][0], false);
				_int[1][1] = var_bd;
			} else {
				bd var_bd = new bd(new af(_int[0][0]._if().a + 3, _int[0][0]
						._if()._if + 3));
				a(var_bd, _int[0][0], _int[0][1], _int[1][0], false);
				_int[0][0] = var_bd;
				var_bd = new bd(new af(_int[0][i_3_ - 1]._if().a - 3,
						_int[0][i_3_ - 1]._if()._if + 3));
				a(var_bd, _int[0][i_3_ - 1], _int[0][i_3_ - 2],
						_int[1][i_3_ - 1], false);
				_int[0][i_3_ - 1] = var_bd;
				var_bd = new bd(new af(_int[i_3_ - 1][0]._if().a + 3,
						_int[i_3_ - 1][0]._if()._if - 3));
				a(var_bd, _int[i_3_ - 1][0], _int[i_3_ - 2][0],
						_int[i_3_ - 1][1], false);
				_int[i_3_ - 1][0] = var_bd;
			}
		}
	}

	private void a(bd var_bd, bd var_bd_8_, bd var_bd_9_, bd var_bd_10_,
			boolean bool) {
		int i = var_bd_8_._if().a;
		int i_11_ = var_bd_8_._if()._if;
		double d = (double) (var_bd_10_._if().a - i);
		double d_12_ = (double) (var_bd_9_._if().a - i);
		double d_13_ = (double) (var_bd_10_._if()._if - i_11_);
		double d_14_ = (double) (var_bd_9_._if()._if - i_11_);
		double d_15_ = d * d_14_ - d_13_ * d_12_;
		double d_16_ = var_bd_8_.a().x;
		double d_17_ = var_bd_8_.a().y;
		double d_18_ = var_bd_10_.a().x - d_16_;
		double d_19_ = var_bd_10_.a().y - d_17_;
		double d_20_ = var_bd_9_.a().x - d_16_;
		double d_21_ = var_bd_9_.a().y - d_17_;
		double d_22_ = (d_18_ * d_14_ - d_20_ * d_13_) / d_15_;
		double d_23_ = (d_20_ * d - d_18_ * d_12_) / d_15_;
		double d_24_ = (d_19_ * d_14_ - d_21_ * d_13_) / d_15_;
		double d_25_ = (d_21_ * d - d_19_ * d_12_) / d_15_;
		int i_26_ = var_bd._if().a - i;
		int i_27_ = var_bd._if()._if - i_11_;
		double d_28_ = d_16_ + d_22_ * (double) i_26_ + d_23_ * (double) i_27_;
		double d_29_ = d_17_ + d_24_ * (double) i_26_ + d_25_ * (double) i_27_;
		var_bd.a(new PointDouble(d_28_, d_29_));
		if (bool) {
			PointDouble[][] var_ays = new PointDouble[5][5];
			for (int i_30_ = -2; i_30_ <= 2; i_30_++) {
				for (int i_31_ = -2; i_31_ <= 2; i_31_++)
					var_ays[2 + i_30_][2 + i_31_] = new PointDouble((d_16_ + d_22_
							* (double) (i_26_ + i_31_) + d_23_
							* (double) (i_27_ + i_30_)), (d_17_ + d_24_
							* (double) (i_26_ + i_31_) + d_25_
							* (double) (i_27_ + i_30_)));
			}
			int i_32_ = 1;
			int i_33_ = 0;
			int i_34_ = 0;
			double d_35_ = 0.0;
			double d_36_ = 0.0;
			PointDouble var_ay = null;
			int i_37_ = -2147483648;
			do {
				int i_38_ = 0;
				for (int i_39_ = -2; i_39_ <= 2; i_39_++) {
					for (int i_40_ = -2; i_40_ <= 2; i_40_++) {
						boolean bool_41_ = (a
								.a(
										(int) (var_ays[2 + i_39_][2 + i_40_].x + d_35_),
										(int) (var_ays[2 + i_39_][2 + i_40_].y + d_36_)) < _case);
						if (bool_41_
								&& (i_39_ == -2 || i_39_ == 2 || i_40_ == -2
										|| i_40_ == 2 || i_40_ == 0
										&& i_39_ == 0))
							i_38_++;
						else if (!bool_41_ && i_39_ >= -1 && i_39_ <= 1
								&& i_40_ >= -1 && i_40_ <= 1
								&& (i_40_ != 0 || i_39_ != 0))
							i_38_++;
						else
							i_38_--;
					}
				}
				if (i_38_ > i_37_) {
					i_37_ = i_38_;
					var_ay = new PointDouble(d_28_ + d_35_, d_29_ + d_36_);
				}
				if (i_38_ == 25)
					break;
				if (++i_33_ > i_32_) {
					i_33_ = 1;
					if (++i_34_ == 4)
						i_34_ = 0;
					if (i_34_ % 2 == 0)
						i_32_++;
				}
				if (i_34_ == 0) {
					d_35_ += d_22_ / 2.0;
					d_36_ += d_24_ / 2.0;
				} else if (i_34_ == 1) {
					d_35_ -= d_23_ / 2.0;
					d_36_ -= d_25_ / 2.0;
				} else if (i_34_ == 2) {
					d_35_ -= d_22_ / 2.0;
					d_36_ -= d_24_ / 2.0;
				} else if (i_34_ == 3) {
					d_35_ += d_23_ / 2.0;
					d_36_ += d_25_ / 2.0;
				}
			} while (i_32_ < 7);
			double d_42_ = Math.max(Math.abs(d_22_), Math.abs(d_23_));
			d_42_ = Math.max(Math.abs(d_42_), Math.abs(d_24_));
			d_42_ = Math.max(Math.abs(d_42_), Math.abs(d_25_));
			if (d_42_ > 3.0) {
				int i_43_ = (int) (var_ay.y - d_42_);
				int i_44_ = (int) (var_ay.y + d_42_);
				int i_45_ = (int) (var_ay.x - d_42_);
				int i_46_ = (int) (var_ay.x + d_42_);
				int i_47_ = 0;
				int i_48_ = 0;
				int i_49_ = 0;
				for (int i_50_ = i_43_; i_50_ <= i_44_; i_50_++) {
					for (int i_51_ = i_45_; i_51_ <= i_46_; i_51_++) {
						if (a.a(i_51_, i_50_) < _case) {
							i_48_ += i_51_;
							i_49_ += i_50_;
							i_47_++;
						}
					}
				}
				if (i_47_ > 0) {
					double d_52_ = (double) i_48_;
					double d_53_ = (double) i_49_;
					d_52_ /= (double) i_47_;
					d_53_ /= (double) i_47_;
					var_ay.x = d_52_;
					var_ay.y = d_53_;
				}
			}
			var_bd.a(var_ay);
		}
	}

	af _if(af var_af, af var_af_54_, af var_af_55_) {
		if (var_af_54_ == null)
			var_af_54_ = new af();
		if (_new == 1)
			return a(var_af, var_af_54_, var_af_55_);
		return a(var_af, var_af_54_);
	}

	private af a(af var_af, af var_af_56_) {
		int i = 0;
		int i_57_ = 0;
		if (_new >= 7) {
			int[] is = a4.a(_new);
			for (int i_58_ = is.length - 2; i_58_ >= 0; i_58_--) {
				if (var_af.a > is[i_58_]) {
					i = i_58_;
					break;
				}
			}
			for (int i_59_ = is.length - 2; i_59_ >= 0; i_59_--) {
				if (var_af._if > is[i_59_]) {
					i_57_ = i_59_;
					break;
				}
			}
		}
		bd var_bd = _int[i_57_][i];
		bd var_bd_60_ = _int[i_57_][i + 1];
		bd var_bd_61_ = _int[i_57_ + 1][i];
		bd var_bd_62_ = _int[i_57_ + 1][i + 1];
		double d = (double) var_af.a;
		double d_63_ = (double) var_af._if;
		double d_64_ = (double) var_bd._if().a;
		double d_65_ = (double) var_bd_60_._if().a;
		double d_66_ = (double) var_bd._if()._if;
		double d_67_ = (double) var_bd_61_._if()._if;
		double d_68_ = (d - d_64_) / (d_65_ - d_64_);
		double d_69_ = 1.0 - d_68_;
		double d_70_ = (d_63_ - d_66_) / (d_67_ - d_66_);
		double d_71_ = 1.0 - d_70_;
		double d_72_ = ((var_bd.a().x * d_69_ + var_bd_60_.a().x * d_68_)
				* d_71_ + ((var_bd_61_.a().x * d_69_ + var_bd_62_.a().x * d_68_) * d_70_));
		double d_73_ = ((var_bd.a().y * d_71_ + var_bd_61_.a().y * d_70_)
				* d_69_ + ((var_bd_60_.a().y * d_71_ + var_bd_62_.a().y
				* d_70_) * d_68_));
		var_af_56_.a = (int) d_72_;
		var_af_56_._if = (int) d_73_;
		return var_af_56_;
	}

	private af a(af var_af, af var_af_74_, af var_af_75_) {
		double d = _for._do().x - _try._do().x;
		double d_76_ = _for._do().y - _try._do().y;
		boolean bool = Math.abs(d) > Math.abs(d_76_);
		int i = 10 + _new * 4;
		double d_77_;
		double d_78_;
		double d_79_;
		double d_80_;
		if (bool) {
			d_77_ = _for._do().x - _try._do().x;
			double d_81_ = _for._do().y - _try._do().y;
			double d_82_ = _if._do().x - _try._do().x;
			d_78_ = _if._do().y - _try._do().y;
			d_79_ = (_try._do().x + d_77_ * (double) (var_af.a - 3)
					/ (double) i + (double) (-3 + var_af._if) * d_82_
					/ (double) i);
			d_80_ = (_try._do().y + (double) (-3 + var_af._if) * d_78_
					/ (double) i + d_81_ * (double) (var_af.a - 3) / (double) i);
		} else {
			d_77_ = _if._do().x - _try._do().x;
			double d_83_ = _if._do().y - _try._do().y;
			double d_84_ = _for._do().x - _try._do().x;
			d_78_ = _for._do().y - _try._do().y;
			d_79_ = (_try._do().x + (double) (-3 + var_af._if) * d_77_
					/ (double) i + d_84_ * (double) (var_af.a - 3) / (double) i);
			d_80_ = (_try._do().y + d_78_ * (double) (var_af.a - 3)
					/ (double) i + (double) (-3 + var_af._if) * d_83_
					/ (double) i);
		}
		if (var_af_75_ != null) {
			d_79_ += ((double) var_af_75_.a * d_77_ * (double) (var_af.a - 3) / (double) (2 * (10 + 4 * _new) * (17 + 4 * _new)));
			d_80_ += ((double) var_af_75_._if * d_78_
					* (double) (var_af._if - 3) / (double) (2 * (10 + 4 * _new) * (17 + 4 * _new)));
		}
		var_af_74_.a = (int) d_79_;
		var_af_74_._if = (int) d_80_;
		return var_af_74_;
	}
}
