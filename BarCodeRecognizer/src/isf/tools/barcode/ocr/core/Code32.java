/* ac - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class Code32 extends Barcode {
	private static final boolean ag = false;

	private static final int ac = 10;

	private static final int aa = 32;

	private static final int[] Z = { 111221211, 211211112, 112211112,
			212211111, 111221112, 211221111, 112221111, 111211212, 211211211,
			112211211, 112112112, 212112111, 111122112, 112122111, 111112212,
			211112211, 111122211, 211111122, 112111122, 212111121, 111121122,
			112121121, 111111222, 211111221, 112111221, 111121221, 221111112,
			122111112, 222111111, 121121112, 221121111, 122121111, 121121211 };

	private static HashMap ad = null;

	private ArrayList Y;

	private int ae;

	private int[] af = new int[9];

	private int[] ab = new int[4];

	Code32(PropertiesHolder var_c, int i) {
		super(var_c);
		Y = new ArrayList();
		ae = i;
	}

	int code() {
		return 16;
	}

	int _if(int i) {
		if (i == 79)
			return 8;
		return -1;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 8.0;
	}

	double _for() {
		return 8.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 9; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		int i_8_ = 4 * ae;
		while (i_4_ + 9 <= i_1_) {
			if ((i_4_ == 1 || is[i_4_ - 1] > i_5_ / i_8_)
					&& is[i_4_ + 9] < i_5_ / 2) {
				int i_9_ = _for(is, i_4_, 9, 12);
				if (i_9_ == 32) {
					this.readData(i, i_0_, is, i_4_, 9, i_2_, i_9_, true, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			} else if (is[i_4_ - 1] < i_5_ / 2
					&& (i_4_ + 9 == i_1_ - 1 || is[i_4_ + 9] > i_5_ / i_8_)) {
				int i_10_ = _for(is, i_4_, 9, 12);
				if (i_10_ == 32) {
					this.readData(i, i_0_, is, i_4_, 9, i_2_, i_10_, false, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 7] + is[i_4_ + 8];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		int i_12_ = 0;
		int i_13_ = 0;
		int i_14_ = _if(i);
		int i_15_ = 0;
		for (int i_16_ = 0; i_16_ < i; i_16_++)
			i_15_ += is[i_16_];
		int i_17_ = i_15_ / i_14_;
		for (int i_18_ = 0; i_18_ < i_14_; i_18_++) {
			int i_19_ = 0;
			for (int i_20_ = 0; i_20_ < 9; i_20_++)
				i_19_ += is[10 * i_18_ + i_20_];
			int i_21_ = Math.abs(i_19_ - i_17_);
			if (i_21_ > i_17_ / 5)
				return false;
			if (i_18_ < i_14_ - 1 && is[10 * i_18_ + 9] > i_17_ / 4)
				return false;
		}
		while_3_: do {
			for (;;) {
				if (i_13_ == 0) {
					int i_22_ = _for(is, i_12_, 9, 12);
					i_12_ += 10;
					i_13_ = 1;
					if (i_22_ != 32)
						break while_3_;
					Y.clear();
					Y.add(new Integer(i_22_));
				} else {
					if (i - i_12_ <= 9)
						break;
					int i_23_ = _for(is, i_12_, 9, 12);
					if (i_23_ == 32 || (i_23_ < 0 || i_23_ > 43)
							&& i_23_ != 65535)
						break while_3_;
					i_12_ += 10;
					i_13_++;
					Y.add(new Integer(i_23_));
				}
			}
			int i_24_ = _for(is, i_12_, 9, 12);
			if (i_24_ == 32) {
				Y.add(new Integer(i_24_));
				if (i_12_ == i - 9) {
					this.a(Y, -1, var_d, var_d_11_);
					return true;
				}
			}
		} while (false);
		return false;
	}

	private boolean _do(String string) {
		int i = 2 * (string.charAt(1) - 48);
		int i_25_ = 2 * (string.charAt(3) - 48);
		int i_26_ = 2 * (string.charAt(5) - 48);
		int i_27_ = 2 * (string.charAt(7) - 48);
		int i_28_ = (i / 10 + i_25_ / 10 + i_26_ / 10 + i_27_ / 10 + i % 10
				+ i_25_ % 10 + i_26_ % 10 + i_27_ % 10);
		int i_29_ = string.charAt(0) - 48;
		int i_30_ = string.charAt(2) - 48;
		int i_31_ = string.charAt(4) - 48;
		int i_32_ = string.charAt(6) - 48;
		int i_33_ = i_29_ + i_30_ + i_31_ + i_32_;
		int i_34_ = (i_28_ + i_33_) % 10;
		int i_35_ = string.charAt(8) - 48;
		return i_34_ == i_35_;
	}

	private final int _for(int[] is, int i, int i_36_, int i_37_) {
		int i_38_ = 0;
		for (int i_39_ = 0; i_39_ < 9; i_39_++) {
			af[i_39_] = is[i + i_39_];
			i_38_ += is[i + i_39_];
		}
		for (int i_40_ = 0; i_40_ < 4; i_40_++) {
			int i_41_ = 0;
			for (int i_42_ = 1; i_42_ < 9; i_42_++) {
				if (af[i_42_] > af[i_41_])
					i_41_ = i_42_;
			}
			ab[i_40_] = i_41_;
			af[i_41_] = -1;
		}
		for (int i_43_ = 0; i_43_ < 4; i_43_++)
			af[ab[i_43_]] = is[i + ab[i_43_]];
		if (af[ab[0]] > 2 * af[ab[2]])
			return -1;
		if (af[ab[3]] == af[ab[2]]) {
			if (ab[0] % 2 == 1 && ab[1] % 2 == 0 || ab[0] % 2 == 0
					&& ab[1] % 2 == 1) {
				if (ab[2] % 2 == 1 && ab[3] % 2 == 0)
					ab[2] = ab[3];
				else if (ab[2] % 2 == 0 && ab[3] % 2 == 1)
					ab[2] = ab[2];
				else
					return 65535;
			} else if (ab[0] % 2 == 1 && ab[1] % 2 == 1) {
				if (ab[2] % 2 == 1 && ab[3] % 2 == 0)
					ab[2] = ab[2];
				else if (ab[2] % 2 == 0 && ab[3] % 2 == 1)
					ab[2] = ab[3];
				else
					return 65535;
			} else if (ab[0] % 2 == 0 && ab[1] % 2 == 0) {
				if (ab[2] % 2 == 1 && ab[3] % 2 == 0)
					ab[2] = ab[2];
				else if (ab[2] % 2 == 0 && ab[3] % 2 == 1)
					ab[2] = ab[3];
				else
					return 65535;
			} else
				return 65535;
		}
		for (int i_44_ = 0; i_44_ < 9; i_44_++)
			af[i_44_] = 1;
		for (int i_45_ = 0; i_45_ < 3; i_45_++)
			af[ab[i_45_]] = 2;
		int i_46_ = 0;
		for (int i_47_ = 0; i_47_ < 9; i_47_++)
			i_46_ = i_46_ * 10 + af[i_47_];
		Object object = ad.get(new Integer(i_46_));
		if (object == null)
			return 65535;
		int i_48_ = ((Integer) object).intValue();
		return i_48_;
	}

	String a(String string) {
		int i = string.charAt(string.length() - 2);
		int i_49_ = 32;
		for (int i_50_ = string.length() - 3; i_50_ >= 1; i_50_--) {
			int i_51_ = string.charAt(i_50_);
			i += i_51_ * i_49_;
			i_49_ *= 32;
		}
		String string_52_;
		for (string_52_ = "" + i; string_52_.length() < 9; string_52_ = "0"
				+ string_52_) {
			/* empty */
		}
		if (!_do(string_52_))
			return null;
		return "A" + string_52_;
	}

	static {
		ad = new HashMap();
		for (int i = 0; i < Z.length; i++)
			ad.put(new Integer(Z[i]), new Integer(i));
	}
}
