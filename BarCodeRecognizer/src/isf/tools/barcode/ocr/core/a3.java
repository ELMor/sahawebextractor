/* a3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Dimension;
import java.awt.Polygon;
import java.awt.Rectangle;

class a3 {
	private static final boolean _case = false;

	static int _if;

	static double[] m;

	static double[] _new;

	static double[] j;

	private static final int _int = 23;

	private static final int _char = 30;

	private static final int[][] _byte = { { 8, 8 }, { 10, 10 }, { 12, 12 },
			{ 14, 14 }, { 16, 16 }, { 18, 18 }, { 20, 20 }, { 22, 22 },
			{ 24, 24 }, { 28, 28 }, { 32, 32 }, { 36, 36 }, { 40, 40 },
			{ 44, 44 }, { 48, 48 }, { 56, 56 }, { 64, 64 }, { 72, 72 },
			{ 80, 80 }, { 88, 88 }, { 96, 96 }, { 108, 108 }, { 120, 120 },
			{ 132, 132 }, { 6, 16 }, { 6, 28 }, { 10, 24 }, { 10, 32 },
			{ 14, 32 }, { 14, 44 } };

	private static final int[][] _goto = { { 8, 8 }, { 10, 10 }, { 12, 12 },
			{ 14, 14 }, { 16, 16 }, { 18, 18 }, { 20, 20 }, { 22, 22 },
			{ 24, 24 }, { 14, 14 }, { 16, 16 }, { 18, 18 }, { 20, 20 },
			{ 22, 22 }, { 24, 24 }, { 14, 14 }, { 16, 16 }, { 18, 18 },
			{ 20, 20 }, { 22, 22 }, { 24, 24 }, { 18, 18 }, { 20, 20 },
			{ 22, 22 }, { 6, 16 }, { 6, 14 }, { 10, 24 }, { 10, 16 },
			{ 14, 16 }, { 14, 22 } };

	private static final int[] b = { 10, 12, 14, 16, 18, 20, 22, 24, 26, 32,
			36, 40, 44, 48, 52, 64, 72, 80, 88, 96, 104, 120, 132, 144 };

	private static final int[] _for = { 8, 12, 16 };

	private static final int[] t = { 18, 32, 26, 36, 48 };

	private static final int[] s = { 8, 12, 18, 24, 32, 40, 50, 60, 72, 98,
			128, 162, 200, 242, 288, 392, 512, 648, 800, 968, 1152, 1458, 1800,
			2178, 12, 21, 30, 40, 56, 77 };

	private static final int[] f = { 3, 5, 8, 12, 18, 22, 30, 36, 44, 62, 86,
			114, 144, 174, 102, 140, 92, 114, 144, 174, 136, 175, 163, 156, 5,
			10, 16, 22, 32, 49 };

	private static final int[] v = { 5, 7, 10, 12, 14, 18, 20, 24, 28, 36, 42,
			48, 56, 68, 42, 56, 36, 48, 56, 68, 56, 68, 62, 62, 7, 11, 14, 18,
			24, 28 };

	private static final int[] o = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			2, 2, 4, 4, 4, 4, 6, 6, 8, 10, 1, 1, 1, 1, 1, 1 };

	private static short[][][] r = new short[30][][];

	byte[][] h;

	boolean _do;

	n[][] i;

	private int e;

	private int d;

	Polygon c;

	int _void;

	Rectangle k;

	String n;

	int[] l;

	private ar u;

	PropertiesHolder g;

	private static final int _long = 0;

	private static final int _else = 1;

	private static final int p = 2;

	private static final int q = 3;

	private static final int _try = 4;

	private static final int a = 5;

	a3(Polygon polygon, byte[][] is, boolean bool, PropertiesHolder var_c) {
		c = polygon;
		k = polygon.getBounds();
		h = is;
		_do = bool;
		g = var_c;
	}

	void _do() {
		h = null;
		if (this.i != null) {
			for (int i = 0; i < this.i.length; i++) {
				for (int i_0_ = 0; i_0_ < this.i[0].length; i_0_++) {
					this.i[i][i_0_].a();
					this.i[i][i_0_] = null;
				}
			}
		}
		this.i = null;
		c = null;
		k = null;
		n = null;
		u = null;
		g = null;
	}

	boolean _if() {
		int i = a();
		if (i == -1)
			return false;
		int i_1_ = _goto[i][1];
		int i_2_ = _goto[i][0];
		double d = Math.abs(this.i[0][0]._if[0].x - this.i[0][0]._if[1].x);
		double d_3_ = Math.abs(this.i[0][0]._if[0].y
				- this.i[0][0]._if[1].y);
		int i_4_ = (int) Math.max(d, d_3_) / i_1_;
		for (int i_5_ = 0; i_5_ < this.i.length; i_5_++) {
			for (int i_6_ = 0; i_6_ < this.i[i_5_].length; i_6_++) {
				n var_n = this.i[i_5_][i_6_];
				var_n.a(i_1_, i_2_, i_4_);
				if (i_6_ < this.i[0].length - 1 && i_5_ < this.i.length - 1) {
					n var_n_7_ = this.i[i_5_ + 1][i_6_ + 1];
					var_n_7_._if[0].x = var_n._if[2].x;
					var_n_7_._if[0].y = var_n._if[2].y;
					if (i_5_ == 0) {
						var_n_7_ = this.i[0][i_6_ + 1];
						var_n_7_._if[0].x = var_n._if[1].x;
						var_n_7_._if[0].y = var_n._if[1].y;
					}
					if (i_6_ == 0) {
						var_n_7_ = this.i[i_5_ + 1][0];
						var_n_7_._if[0].x = var_n._if[3].x;
						var_n_7_._if[0].y = var_n._if[3].y;
					}
				}
				if (i_6_ < this.i[0].length - 1) {
					n var_n_8_ = this.i[i_5_][i_6_ + 1];
					var_n_8_._if[3].x = var_n._if[2].x;
					var_n_8_._if[3].y = var_n._if[2].y;
				}
				if (i_5_ < this.i.length - 1) {
					n var_n_9_ = this.i[i_5_ + 1][i_6_];
					var_n_9_._if[1].x = var_n._if[2].x;
					var_n_9_._if[1].y = var_n._if[2].y;
				}
			}
		}
		e = _byte[i][1];
		this.d = _byte[i][0];
		for (int i_10_ = 0; i_10_ < this.i.length; i_10_++) {
			for (int i_11_ = 0; i_11_ < this.i[0].length; i_11_++) {
				n var_n = this.i[i_10_][i_11_];
				var_n.a(true, i_1_, i_2_);
				var_n.a(false, i_1_, i_2_);
			}
		}
		e = _byte[i][1];
		this.d = _byte[i][0];
		if (r[i] == null)
			a(i);
		if (u == null)
			u = new ar(5);
		int[] is = a(i, i_1_, i_2_, false);
		int i_12_ = u.a(is, i);
		if (i_12_ < 0) {
			is = a(i, i_1_, i_2_, true);
			i_12_ = u.a(is, i);
			if (i_12_ < 0)
				return false;
		}
		_if(i, is);
		return true;
	}

	private int[] a(int i, int i_13_, int i_14_, boolean bool) {
		int[] is = new int[s[i]];
		for (int i_15_ = 0; i_15_ < d; i_15_++) {
			for (int i_16_ = 0; i_16_ < e; i_16_++) {
				short i_17_ = r[i][d - i_15_ - 1][i_16_];
				if (i_17_ != 0 && i_17_ != 1) {
					int i_18_ = i_17_ / 10;
					int i_19_ = i_17_ % 10;
					int i_20_ = (this.i[i_16_ / i_13_][i_15_ / i_14_]._if(i_15_
							% i_14_, i_16_ % i_13_, i_13_, i_14_, bool));
					if (i_20_ == 1)
						is[i_18_ - 1] |= 256 >>> i_19_;
				}
			}
		}
		return is;
	}

	private int a(int i, int[] is) {
		return is[i];
	}

	private void _if(int i, int[] is) {
		int i_21_ = 0;
		int i_22_ = -1;
		int i_23_ = 0;
		int i_24_ = 0;
		StringBuffer stringbuffer = new StringBuffer();
		int i_25_ = f[i] * o[i];
		if (i == 23)
			i_25_ -= 2;
		for (int i_26_ = 0; i_26_ < i_25_; i_26_++) {
			int i_27_ = a(i_26_, is);
			if (i_21_ == 0 || i_21_ == 1 && i_26_ == i_25_ - 1 || i_21_ == 2
					&& i_26_ == i_25_ - 1 || i_21_ == 3 && i_26_ == i_25_ - 1
					|| i_21_ == 4 && i_26_ >= i_25_ - 2) {
				if (i_27_ > 0 && i_27_ <= 128)
					stringbuffer.append((char) (i_27_ - 1));
				else {
					if (i_27_ == 129)
						break;
					if (i_27_ >= 130 && i_27_ <= 229) {
						int i_28_ = i_27_ - 130;
						stringbuffer.append((char) (48 + i_28_ / 10));
						stringbuffer.append((char) (48 + i_28_ % 10));
					} else if (i_27_ == 230) {
						i_21_ = 1;
						i_22_ = -1;
					} else if (i_27_ == 231)
						i_21_ = 5;
					else if (i_27_ == 232)
						stringbuffer.append((char) i_27_);
					else if (i_27_ == 233) {
						l = new int[3];
						int i_29_ = a(++i_26_, is);
						l[0] = i_29_ & 0xf;
						l[1] = 17 - ((i_29_ & 0xf0) >> 4);
						i_29_ = a(++i_26_, is) * 254;
						l[2] = i_29_ + a(++i_26_, is);
					} else if (i_27_ == 234)
						stringbuffer.append((char) i_27_);
					else if (i_27_ == 235) {
						int i_30_ = a(++i_26_, is);
						stringbuffer.append((char) (i_30_ + 127));
					} else if (i_27_ == 236) {
						if (i_26_ == 0) {
							stringbuffer.append("[)>\03605\035");
							i_24_ = 5;
						}
					} else if (i_27_ == 237) {
						if (i_26_ == 0) {
							stringbuffer.append("[)>\03606\035");
							i_24_ = 6;
						}
					} else if (i_27_ == 238)
						i_21_ = 3;
					else if (i_27_ == 239) {
						i_21_ = 2;
						i_22_ = -1;
					} else if (i_27_ == 240)
						i_21_ = 4;
					else if (i_27_ == 241) {
						stringbuffer.append((char) i_27_);
						int i_31_ = a(++i_26_, is);
						stringbuffer.append((char) i_31_);
						if (i_31_ >= 128) {
							int i_32_ = a(++i_26_, is);
							stringbuffer.append((char) i_32_);
							if (i_31_ >= 192) {
								int i_33_ = a(++i_26_, is);
								stringbuffer.append((char) i_33_);
							}
						}
					}
				}
			} else if (i_21_ == 1 || i_21_ == 2) {
				int i_34_;
				int i_35_;
				if (i_21_ == 1) {
					i_34_ = 65;
					i_35_ = 97;
				} else {
					i_34_ = 97;
					i_35_ = 65;
				}
				if (i_27_ == 254)
					i_21_ = 0;
				else {
					int i_36_ = a(++i_26_, is);
					int i_37_ = i_27_ * 256 + i_36_ - 1;
					int[] is_38_ = new int[3];
					is_38_[2] = i_37_ % 40;
					i_37_ /= 40;
					is_38_[1] = i_37_ % 40;
					is_38_[0] = i_37_ / 40;
					for (int i_39_ = 0; i_39_ < 3; i_39_++) {
						int i_40_ = is_38_[i_39_];
						if (i_22_ == -1) {
							if (i_40_ <= 2)
								i_22_ = i_40_;
							else if (i_40_ == 3) {
								stringbuffer.append((char) (32 + i_23_));
								i_23_ = 0;
							} else if (i_40_ >= 4 && i_40_ <= 13) {
								stringbuffer
										.append((char) (48 + i_40_ - 4 + i_23_));
								i_23_ = 0;
							} else {
								stringbuffer
										.append((char) (i_34_ + i_40_ - 14 + i_23_));
								i_23_ = 0;
							}
						} else if (i_22_ == 0) {
							stringbuffer.append((char) (i_40_ + i_23_));
							i_22_ = -1;
							i_23_ = 0;
						} else if (i_22_ == 1) {
							if (i_40_ <= 14) {
								stringbuffer
										.append((char) (33 + i_40_ + i_23_));
								i_23_ = 0;
							} else if (i_40_ <= 21) {
								stringbuffer
										.append((char) (43 + i_40_ + i_23_));
								i_23_ = 0;
							} else if (i_40_ <= 26) {
								stringbuffer
										.append((char) (69 + i_40_ + i_23_));
								i_23_ = 0;
							} else if (i_40_ == 27) {
								stringbuffer.append('\u00e8');
								i_23_ = 0;
							} else if (i_40_ == 30)
								i_23_ = 128;
							i_22_ = -1;
						} else if (i_22_ == 2) {
							if (i_40_ == 0)
								stringbuffer.append((char) (96 + i_23_));
							else if (i_40_ <= 26)
								stringbuffer
										.append((char) (i_35_ + i_40_ - 1 + i_23_));
							else
								stringbuffer
										.append((char) (96 + i_40_ + i_23_));
							i_22_ = -1;
							i_23_ = 0;
						}
					}
				}
			} else if (i_21_ == 3) {
				if (i_27_ == 254)
					i_21_ = 0;
				else {
					int i_41_ = a(++i_26_, is);
					int i_42_ = i_27_ * 256 + i_41_ - 1;
					int[] is_43_ = new int[3];
					is_43_[2] = i_42_ % 40;
					i_42_ /= 40;
					is_43_[1] = i_42_ % 40;
					is_43_[0] = i_42_ / 40;
					for (int i_44_ = 0; i_44_ < 3; i_44_++) {
						if (is_43_[i_44_] == 0)
							stringbuffer.append('\r');
						else if (is_43_[i_44_] == 1)
							stringbuffer.append('*');
						else if (is_43_[i_44_] == 2)
							stringbuffer.append('>');
						else if (is_43_[i_44_] == 3)
							stringbuffer.append(' ');
						else if (is_43_[i_44_] <= 13)
							stringbuffer
									.append((char) (48 + is_43_[i_44_] - 4));
						else
							stringbuffer
									.append((char) (65 + is_43_[i_44_] - 14));
					}
				}
			} else if (i_21_ == 4) {
				int i_45_ = i_27_ >>> 2;
				if (i_45_ == 31)
					i_21_ = 0;
				else {
					stringbuffer.append(i_45_ > 31 ? (char) i_45_
							: (char) (i_45_ + 64));
					int i_46_ = a(++i_26_, is);
					i_45_ = (i_27_ & 0x3) * 16 + (i_46_ >>> 4);
					if (i_45_ == 31)
						i_21_ = 0;
					else {
						stringbuffer.append(i_45_ > 31 ? (char) i_45_
								: (char) (i_45_ + 64));
						int i_47_ = a(++i_26_, is);
						i_45_ = (i_46_ & 0xf) * 4 + (i_47_ >>> 6);
						if (i_45_ == 31)
							i_21_ = 0;
						else {
							stringbuffer.append(i_45_ > 31 ? (char) i_45_
									: (char) (i_45_ + 64));
							i_45_ = i_47_ & 0x3f;
							if (i_45_ == 31)
								i_21_ = 0;
							else
								stringbuffer.append(i_45_ > 31 ? (char) i_45_
										: (char) (i_45_ + 64));
						}
					}
				}
			} else if (i_21_ == 5) {
				int i_48_ = i_27_;
				int i_49_ = 149 * (i_26_ + 1) % 255 + 1;
				i_48_ -= i_49_;
				if (i_48_ < 0)
					i_48_ += 256;
				if (i_48_ == 0)
					i_48_ = i_25_ - i_26_ - 1;
				else if (i_48_ >= 250) {
					int i_50_ = a(++i_26_, is);
					i_50_ -= 149 * (i_26_ + 1) % 255 + 1;
					if (i_50_ < 0)
						i_50_ += 256;
					i_48_ = (i_48_ - 249) * 250 + i_50_;
				}
				for (int i_51_ = 0; i_51_ < i_48_; i_51_++) {
					int i_52_ = a(++i_26_, is);
					i_52_ -= 149 * (i_26_ + 1) % 255 + 1;
					if (i_52_ < 0)
						i_52_ += 256;
					stringbuffer.append((char) i_52_);
				}
				i_21_ = 0;
			}
		}
		if (i_24_ != 0)
			stringbuffer.append("\036\004");
		n = new String(stringbuffer);
	}

	private void a(int i) {
		r[i] = new short[d][];
		for (int i_53_ = 0; i_53_ < d; i_53_++)
			r[i][i_53_] = new short[e];
		short i_54_ = 1;
		int i_55_ = 4;
		int i_56_ = 0;
		do {
			if (i_55_ == d && i_56_ == 0) {
				a3 var_a3_57_ = this;
				int i_58_ = i;
				short i_59_ = i_54_;
				i_54_++;
				var_a3_57_._int(i_58_, i_59_);
			}
			if (i_55_ == d - 2 && i_56_ == 0 && e % 4 != 0) {
				a3 var_a3_60_ = this;
				int i_61_ = i;
				short i_62_ = i_54_;
				i_54_++;
				var_a3_60_.a(i_61_, i_62_);
			}
			if (i_55_ == d - 2 && i_56_ == 0 && e % 8 == 4) {
				a3 var_a3_63_ = this;
				int i_64_ = i;
				short i_65_ = i_54_;
				i_54_++;
				var_a3_63_._do(i_64_, i_65_);
			}
			if (i_55_ == d + 4 && i_56_ == 2 && e % 8 == 0) {
				a3 var_a3_66_ = this;
				int i_67_ = i;
				short i_68_ = i_54_;
				i_54_++;
				var_a3_66_._for(i_67_, i_68_);
			}
			do {
				if (i_55_ < d && i_56_ >= 0 && r[i][i_55_][i_56_] == 0) {
					a3 var_a3_69_ = this;
					int i_70_ = i;
					int i_71_ = i_55_;
					int i_72_ = i_56_;
					short i_73_ = i_54_;
					i_54_++;
					var_a3_69_.a(i_70_, i_71_, i_72_, i_73_);
				}
				i_55_ -= 2;
				i_56_ += 2;
			} while (i_55_ >= 0 && i_56_ < e);
			i_55_++;
			i_56_ += 3;
			do {
				if (i_55_ >= 0 && i_56_ < e && r[i][i_55_][i_56_] == 0) {
					a3 var_a3_74_ = this;
					int i_75_ = i;
					int i_76_ = i_55_;
					int i_77_ = i_56_;
					short i_78_ = i_54_;
					i_54_++;
					var_a3_74_.a(i_75_, i_76_, i_77_, i_78_);
				}
				i_55_ += 2;
				i_56_ -= 2;
			} while (i_55_ < d && i_56_ >= 0);
			i_55_ += 3;
			i_56_++;
		} while (i_55_ < d || i_56_ < e);
		if (r[i][d - 1][e - 1] == 0)
			r[i][d - 1][e - 1] = r[i][d - 2][e - 2] = (short) 1;
	}

	private void a(int i, int i_79_, int i_80_, int i_81_, int i_82_) {
		if (i_79_ < 0) {
			i_79_ += d;
			i_80_ += 4 - (d + 4) % 8;
		}
		if (i_80_ < 0) {
			i_80_ += e;
			i_79_ += 4 - (e + 4) % 8;
		}
		r[i][i_79_][i_80_] = (short) (10 * i_81_ + i_82_);
	}

	void a(int i, int i_83_, int i_84_, int i_85_) {
		a(i, i_83_ - 2, i_84_ - 2, i_85_, 1);
		a(i, i_83_ - 2, i_84_ - 1, i_85_, 2);
		a(i, i_83_ - 1, i_84_ - 2, i_85_, 3);
		a(i, i_83_ - 1, i_84_ - 1, i_85_, 4);
		a(i, i_83_ - 1, i_84_, i_85_, 5);
		a(i, i_83_, i_84_ - 2, i_85_, 6);
		a(i, i_83_, i_84_ - 1, i_85_, 7);
		a(i, i_83_, i_84_, i_85_, 8);
	}

	void _int(int i, int i_86_) {
		a(i, d - 1, 0, i_86_, 1);
		a(i, d - 1, 1, i_86_, 2);
		a(i, d - 1, 2, i_86_, 3);
		a(i, 0, e - 2, i_86_, 4);
		a(i, 0, e - 1, i_86_, 5);
		a(i, 1, e - 1, i_86_, 6);
		a(i, 2, e - 1, i_86_, 7);
		a(i, 3, e - 1, i_86_, 8);
	}

	void a(int i, int i_87_) {
		a(i, d - 3, 0, i_87_, 1);
		a(i, d - 2, 0, i_87_, 2);
		a(i, d - 1, 0, i_87_, 3);
		a(i, 0, e - 4, i_87_, 4);
		a(i, 0, e - 3, i_87_, 5);
		a(i, 0, e - 2, i_87_, 6);
		a(i, 0, e - 1, i_87_, 7);
		a(i, 1, e - 1, i_87_, 8);
	}

	void _do(int i, int i_88_) {
		a(i, d - 3, 0, i_88_, 1);
		a(i, d - 2, 0, i_88_, 2);
		a(i, d - 1, 0, i_88_, 3);
		a(i, 0, e - 2, i_88_, 4);
		a(i, 0, e - 1, i_88_, 5);
		a(i, 1, e - 1, i_88_, 6);
		a(i, 2, e - 1, i_88_, 7);
		a(i, 3, e - 1, i_88_, 8);
	}

	void _for(int i, int i_89_) {
		a(i, d - 1, 0, i_89_, 1);
		a(i, d - 1, e - 1, i_89_, 2);
		a(i, 0, e - 3, i_89_, 3);
		a(i, 0, e - 2, i_89_, 4);
		a(i, 0, e - 1, i_89_, 5);
		a(i, 1, e - 3, i_89_, 6);
		a(i, 1, e - 2, i_89_, 7);
		a(i, 1, e - 1, i_89_, 8);
	}

	private int[] a(int i, int i_90_, int[] is) {
		int[] is_91_ = new int[is.length];
		int i_92_ = i + 2;
		if (i_92_ > 3)
			i_92_ -= 4;
		int i_93_ = i_90_ + 2;
		if (i_93_ > 3)
			i_93_ -= 4;
		int i_94_ = _if(i, i_90_);
		int i_95_;
		int i_96_;
		int i_97_;
		int i_98_;
		if (!_do) {
			i_95_ = (c.xpoints[i_92_] - c.xpoints[i]) / 2;
			i_96_ = (c.ypoints[i_92_] - c.ypoints[i]) / 2;
			i_97_ = (c.xpoints[i_93_] - c.xpoints[i_90_]) / 2;
			i_98_ = (c.ypoints[i_93_] - c.ypoints[i_90_]) / 2;
		} else {
			int i_99_ = (c.xpoints[i] - c.xpoints[i_90_]) / 2;
			int i_100_ = (c.ypoints[i] - c.ypoints[i_90_]) / 2;
			i_95_ = -i_99_ + i_100_;
			i_96_ = -i_99_ - i_100_;
			i_97_ = i_99_ + i_100_;
			i_98_ = -i_99_ + i_100_;
		}
		for (int i_101_ = 0; i_101_ < is.length; i_101_++) {
			int i_102_ = is[i_101_];
			if (i_94_ < i_102_ * 2)
				break;
			int i_103_ = c.xpoints[i] + i_95_ / i_102_;
			int i_104_ = c.ypoints[i] + i_96_ / i_102_;
			int i_105_ = c.xpoints[i_90_] + i_97_ / i_102_;
			int i_106_ = c.ypoints[i_90_] + i_98_ / i_102_;
			int i_107_ = 0;
			int i_108_ = 0;
			for (int i_109_ = 0; i_109_ < is[i_101_]; i_109_++) {
				int i_110_ = i_103_ + i_109_ * (i_105_ - i_103_) / (i_102_ - 1)
						- k.x;
				if (i_110_ >= 0 && i_110_ < k.width) {
					int i_111_ = (i_104_ + i_109_ * (i_106_ - i_104_)
							/ (i_102_ - 1) - k.y);
					if (i_111_ >= 0 && i_111_ < k.height
							&& h[i_110_][i_111_] == 0) {
						if (i_109_ % 2 == 0)
							i_107_++;
						else
							i_108_++;
					}
				}
			}
			is_91_[i_101_] = Math.abs(i_107_ - i_108_) * 200 / i_102_;
		}
		return is_91_;
	}

	private int _if(int i, int i_112_) {
		if (i_112_ < 0)
			i_112_ += 4;
		else if (i_112_ > 3)
			i_112_ -= 4;
		int i_113_ = c.xpoints[i] - c.xpoints[i_112_];
		int i_114_ = c.ypoints[i] - c.ypoints[i_112_];
		return (int) Math.sqrt((double) (i_113_ * i_113_ + i_114_ * i_114_));
	}

	private int a() {
		int[][] is = new int[4][];
		int i = -1;
		int i_115_ = -1;
		int i_116_ = -1;
		if (!_do) {
			is[0] = a(0, 1, b);
			is[1] = a(1, 2, b);
			is[2] = a(2, 3, b);
			is[3] = a(3, 0, b);
			int i_117_ = -1;
			int i_118_ = -1;
			for (int i_119_ = 0; i_119_ < 4; i_119_++) {
				for (int i_120_ = 0; i_120_ < is[0].length; i_120_++) {
					if (is[i_119_][i_120_] > i_117_) {
						i_117_ = is[i_119_][i_120_];
						i_118_ = i_119_;
						i = i_120_;
					}
				}
			}
			int i_121_ = i_118_ == 0 ? 3 : i_118_ - 1;
			int i_122_ = i_118_ == 3 ? 0 : i_118_ + 1;
			if (is[i_122_][i] > is[i_121_][i])
				_void = i_118_ + 1;
			else
				_void = i_118_;
			_void += 2;
			if (_void > 3)
				_void -= 4;
			if (i < 9)
				i_115_ = i_116_ = 1;
			else if (i < 15)
				i_115_ = i_116_ = 2;
			else if (i < 21)
				i_115_ = i_116_ = 4;
			else if (i < 24)
				i_115_ = i_116_ = 6;
		} else {
			int i_123_ = _if(0, 1);
			int i_124_ = _if(1, 2);
			is[0] = a(0, 1, i_123_ < i_124_ ? _for : t);
			is[1] = a(1, 2, i_123_ < i_124_ ? t : _for);
			is[2] = a(2, 3, i_123_ < i_124_ ? _for : t);
			is[3] = a(3, 0, i_123_ < i_124_ ? t : _for);
			int i_125_ = -1;
			int i_126_ = -1;
			for (int i_127_ = 0; i_127_ < 4; i_127_++) {
				for (int i_128_ = 0; i_128_ < is[i_127_].length; i_128_++) {
					if (is[i_127_][i_128_] > i_125_) {
						i_125_ = is[i_127_][i_128_];
						i_126_ = i_127_;
						i = i_128_;
					}
				}
			}
			int[] is_129_ = is[i_126_].length == 3 ? _for : t;
			switch (is_129_[i]) {
			case 8:
				_void = i_126_ + 2;
				if (_void > 3)
					_void -= 4;
				if (--i_126_ < 0)
					i_126_ += 4;
				if (is[i_126_][0] > is[i_126_][1])
					i = 24;
				else
					i = 25;
				break;
			case 12:
				_void = i_126_ + 2;
				if (_void > 3)
					_void -= 4;
				if (--i_126_ < 0)
					i_126_ += 4;
				if (is[i_126_][2] > is[i_126_][3])
					i = 26;
				else
					i = 27;
				break;
			case 16:
				_void = i_126_ + 2;
				if (_void > 3)
					_void -= 4;
				if (--i_126_ < 0)
					i_126_ += 4;
				if (is[i_126_][3] > is[i_126_][4])
					i = 28;
				else
					i = 29;
				break;
			case 18:
				_void = i_126_ + 3;
				if (_void > 3)
					_void -= 4;
				i = 24;
				break;
			case 26:
				_void = i_126_ + 3;
				if (_void > 3)
					_void -= 4;
				i = 26;
				break;
			case 32:
				_void = i_126_ + 3;
				if (_void > 3)
					_void -= 4;
				i = 25;
				break;
			case 36:
				_void = i_126_ + 3;
				if (_void > 3)
					_void -= 4;
				if (++i_126_ > 3)
					i_126_ -= 4;
				if (is[i_126_][1] > is[i_126_][2])
					i = 27;
				else
					i = 28;
				break;
			case 48:
				_void = i_126_ + 3;
				if (_void > 3)
					_void -= 4;
				i = 29;
				break;
			}
			i_115_ = 2;
			i_116_ = 1;
			if (i == 24 || i == 26)
				i_115_ = 1;
		}
		int i_130_;
		int i_131_;
		switch (i) {
		case 24:
			i_130_ = 8;
			i_131_ = 18;
			break;
		case 25:
			i_130_ = 8;
			i_131_ = 32;
			break;
		case 26:
			i_130_ = 12;
			i_131_ = 26;
			break;
		case 27:
			i_130_ = 12;
			i_131_ = 36;
			break;
		case 28:
			i_130_ = 16;
			i_131_ = 36;
			break;
		case 29:
			i_130_ = 16;
			i_131_ = 48;
			break;
		default:
			i_131_ = i_130_ = b[i];
		}
		double[] ds = new double[4];
		double[] ds_132_ = new double[4];
		for (int i_133_ = 0; i_133_ < 4; i_133_++) {
			int i_134_ = _void + i_133_;
			if (i_134_ > 3)
				i_134_ -= 4;
			ds[i_133_] = (double) c.xpoints[i_134_];
			ds_132_[i_133_] = (double) c.ypoints[i_134_];
		}
		double d = ((ds[3] - ds[0]) / (double) (2 * i_131_) - (ds[3] - ds[2])
				/ (double) (2 * i_130_));
		double d_135_ = ((ds_132_[1] - ds_132_[0]) / (double) (2 * i_130_) - (ds_132_[1] - ds_132_[2])
				/ (double) (2 * i_131_));
		this.i = new n[i_115_][];
		for (int i_136_ = 0; i_136_ < this.i.length; i_136_++)
			this.i[i_136_] = new n[i_116_];
		Dimension dimension = new Dimension(i_115_, i_116_);
		PointDouble[][] var_ays = new PointDouble[i_115_ + 1][];
		for (int i_137_ = 0; i_137_ < i_115_ + 1; i_137_++)
			var_ays[i_137_] = new PointDouble[i_116_ + 1];
		for (int i_138_ = 0; i_138_ <= i_116_; i_138_++) {
			double d_139_ = ds[0] + (ds[1] - ds[0]) * (double) i_138_
					/ (double) i_116_;
			double d_140_ = ds_132_[0]
					+ ((ds_132_[1] - ds_132_[0]) * (double) i_138_ / (double) i_116_);
			double d_141_ = ds[3] + (ds[2] - ds[3]) * (double) i_138_
					/ (double) i_116_;
			double d_142_ = ds_132_[3]
					+ ((ds_132_[2] - ds_132_[3]) * (double) i_138_ / (double) i_116_);
			for (int i_143_ = 0; i_143_ <= i_115_; i_143_++) {
				double d_144_ = (ds[0] + (ds[3] - ds[0]) * (double) i_143_
						/ (double) i_115_);
				double d_145_ = ds_132_[0]
						+ ((ds_132_[3] - ds_132_[0]) * (double) i_143_ / (double) i_115_);
				double d_146_ = (ds[1] + (ds[2] - ds[1]) * (double) i_143_
						/ (double) i_115_);
				double d_147_ = ds_132_[1]
						+ ((ds_132_[2] - ds_132_[1]) * (double) i_143_ / (double) i_115_);
				var_ays[i_143_][i_138_] = a(d_139_, d_140_, d_141_, d_142_,
						d_144_, d_145_, d_146_, d_147_);
				var_ays[i_143_][i_138_].x += d;
				var_ays[i_143_][i_138_].y += d_135_;
			}
		}
		for (int i_148_ = 0; i_148_ < i_116_; i_148_++) {
			for (int i_149_ = 0; i_149_ < i_115_; i_149_++) {
				PointDouble[] var_ays_150_ = new PointDouble[4];
				var_ays_150_[0] = var_ays[i_149_][i_148_];
				var_ays_150_[1] = var_ays[i_149_][i_148_ + 1];
				var_ays_150_[2] = var_ays[i_149_ + 1][i_148_ + 1];
				var_ays_150_[3] = var_ays[i_149_ + 1][i_148_];
				this.i[i_149_][i_148_] = new n(this, var_ays_150_, dimension,
						i_149_, i_148_);
			}
		}
		for (int i_151_ = 0; i_151_ < i_116_; i_151_++) {
			for (int i_152_ = 0; i_152_ < i_115_; i_152_++) {
				/* empty */
			}
		}
		return i;
	}

	private af a(int[][] is) {
		int i = is[0].length;
		int i_153_ = is.length;
		int i_154_ = 0;
		int i_155_ = 0;
		int i_156_ = 0;
		for (int i_157_ = 0; i_157_ < i_153_; i_157_++) {
			for (int i_158_ = 0; i_158_ < i; i_158_++) {
				if (is[i_157_][i_158_] > i_154_)
					i_154_ = is[i_156_ = i_157_][i_155_ = i_158_];
			}
		}
		return new af(i_155_, i_156_);
	}

	private PointDouble a(double d, double d_159_, double d_160_, double d_161_,
			double d_162_, double d_163_, double d_164_, double d_165_) {
		double d_166_ = ((d_160_ - d) * (d_162_ * d_165_ - d_164_ * d_163_) - (d_164_ - d_162_)
				* (d * d_161_ - d_160_ * d_159_));
		double d_167_ = ((d_163_ - d_165_) * (d * d_161_ - d_160_ * d_159_) - (d_159_ - d_161_)
				* (d_162_ * d_165_ - d_164_ * d_163_));
		double d_168_ = ((d - d_160_) * (d_163_ - d_165_) - (d_159_ - d_161_)
				* (d_162_ - d_164_));
		d_166_ /= d_168_;
		d_167_ /= d_168_;
		return new PointDouble(d_166_, d_167_);
	}
}
