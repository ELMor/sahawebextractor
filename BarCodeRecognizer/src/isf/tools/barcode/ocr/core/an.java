/* an - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class an {
	private int _do;

	private int _if;

	private int a;

	private boolean _for;

	an(int i, boolean bool) {
		if (bool) {
			_do = i - 3;
			_if = i - 2;
			a = i - 1;
		} else {
			_do = i + 3;
			_if = i + 2;
			a = i + 1;
		}
		_for = bool;
	}

	void a(int i) {
		if (_for) {
			if (i == a + 1 || a == _if + 1) {
				_do = _if;
				_if = a;
			}
			a = i;
		} else {
			if (i == a - 1 || a == _if - 1) {
				_do = _if;
				_if = a;
			}
			a = i;
		}
	}

	int a() {
		if (_for) {
			if (a == _if + 1)
				return a;
			return _if;
		}
		if (a == _if - 1)
			return a;
		return _if;
	}
}
