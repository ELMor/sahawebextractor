/* n - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Dimension;
import java.awt.Rectangle;

class n {
	private static final boolean _char = false;

	private static final int[][] _void = {
			{ 8, 10, 12, 14, 16, 18, 20, 22, 24 }, { 14, 16, 18, 20, 22, 24 },
			null, { 14, 16, 18, 20, 22, 24 }, null, { 18, 20, 22 } };

	private static final int[][] _case = { { 6, 16 }, { 10, 24 } };

	private static final int[][] _long = { { 6, 14 }, { 10, 16 }, { 14, 16 },
			{ 14, 22 } };

	private a3 _new;

	private Rectangle _goto;

	private byte[][] _else;

	PointDouble[] _if;

	private Dimension _byte;

	private int b;

	private int _for;

	private boolean _do;

	private af[] _try = null;

	private af[] a = null;

	private PropertiesHolder _int;

	n(a3 var_a3, PointDouble[] var_ays, Dimension dimension, int i, int i_0_) {
		_new = var_a3;
		_goto = var_a3.k;
		_else = var_a3.h;
		_if = new PointDouble[var_ays.length];
		for (int i_1_ = 0; i_1_ < var_ays.length; i_1_++)
			_if[i_1_] = new PointDouble(var_ays[i_1_].x, var_ays[i_1_].y);
		_byte = dimension;
		b = i;
		_for = i_0_;
		_do = var_a3._do;
		_int = var_a3.g;
	}

	void a() {
		_goto = null;
		_else = null;
		_if = null;
	}

	private af a(int i, int i_2_, int i_3_, int i_4_, boolean bool) {
		double d = (_if[0].x + (_if[1].x - _if[0].x) * (double) (i + 1)
				/ (double) (i_4_ + 2));
		double d_5_ = _if[0].y
				+ ((_if[1].y - _if[0].y) * (double) (i + 1) / (double) (i_4_ + 2));
		double d_6_ = (_if[3].x + (_if[2].x - _if[3].x) * (double) (i + 1)
				/ (double) (i_4_ + 2));
		double d_7_ = _if[3].y
				+ ((_if[2].y - _if[3].y) * (double) (i + 1) / (double) (i_4_ + 2));
		double d_8_ = _if[0].x
				+ ((_if[3].x - _if[0].x) * (double) (i_2_ + 1) / (double) (i_3_ + 2));
		double d_9_ = _if[0].y
				+ ((_if[3].y - _if[0].y) * (double) (i_2_ + 1) / (double) (i_3_ + 2));
		double d_10_ = _if[1].x
				+ ((_if[2].x - _if[1].x) * (double) (i_2_ + 1) / (double) (i_3_ + 2));
		double d_11_ = _if[1].y
				+ ((_if[2].y - _if[1].y) * (double) (i_2_ + 1) / (double) (i_3_ + 2));
		if (bool) {
			if (_try != null) {
				d_10_ = (double) _try[i_2_].a;
				d_11_ = (double) _try[i_2_]._if;
			}
			if (_for > 0) {
				n var_n_12_ = _new.i[b][_for - 1];
				if (var_n_12_._try != null) {
					d_8_ = (double) var_n_12_._try[i_2_].a;
					d_9_ = (double) var_n_12_._try[i_2_]._if;
				}
			}
			if (a != null) {
				d_6_ = (double) a[i].a;
				d_7_ = (double) a[i]._if;
			}
			if (b > 0) {
				n var_n_13_ = _new.i[b - 1][_for];
				if (var_n_13_.a != null) {
					d = (double) var_n_13_.a[i].a;
					d_5_ = (double) var_n_13_.a[i]._if;
				}
			}
		}
		PointDouble var_ay = a(d, d_5_, d_6_, d_7_, d_8_, d_9_, d_10_, d_11_);
		int i_14_ = (int) (var_ay.x + 0.5) - _goto.x;
		int i_15_ = (int) (var_ay.y + 0.5) - _goto.y;
		return new af(i_14_, i_15_);
	}

	int _if(int i, int i_16_, int i_17_, int i_18_, boolean bool) {
		af var_af = a(i, i_16_, i_17_, i_18_, bool);
		int i_19_ = var_af.a;
		int i_20_ = var_af._if;
		if (i_19_ < 0 || i_19_ >= _else.length || i_20_ < 0
				|| i_20_ >= _else[0].length)
			return 0;
		boolean bool_21_ = _else[i_19_][i_20_] == 0;
		return bool_21_ ? 1 : 0;
	}

	private PointDouble a(double d, double d_22_, double d_23_, double d_24_,
			double d_25_, double d_26_, double d_27_, double d_28_) {
		double d_29_ = ((d_23_ - d) * (d_25_ * d_28_ - d_27_ * d_26_) - (d_27_ - d_25_)
				* (d * d_24_ - d_23_ * d_22_));
		double d_30_ = ((d_26_ - d_28_) * (d * d_24_ - d_23_ * d_22_) - (d_22_ - d_24_)
				* (d_25_ * d_28_ - d_27_ * d_26_));
		double d_31_ = ((d - d_23_) * (d_26_ - d_28_) - (d_22_ - d_24_)
				* (d_25_ - d_27_));
		d_29_ /= d_31_;
		d_30_ /= d_31_;
		return new PointDouble(d_29_, d_30_);
	}

	int a(int i, int i_32_) {
		return _if(i, i_32_, 0);
	}

	int _if(int i, int i_33_, int i_34_) {
		double d = _if[0].x;
		double d_35_ = _if[0].y;
		double d_36_ = _if[1].x;
		double d_37_ = _if[1].y;
		double d_38_ = _if[2].x;
		double d_39_ = _if[2].y;
		double d_40_ = _if[3].x;
		double d_41_ = _if[3].y;
		double d_42_ = d_36_ + (d_40_ - d_36_) / (double) (i + 2);
		double d_43_ = d_37_ + (d_41_ - d_37_) / (double) (i_33_ + 2);
		double d_44_ = d_38_ - (d_38_ - d) / (double) (i + 2);
		double d_45_ = d_39_ - (d_39_ - d_35_) / (double) (i_33_ + 2);
		int i_46_ = 0;
		for (int i_47_ = 0; i_47_ <= i; i_47_++) {
			double d_48_ = d_42_ + (d_44_ - d_42_) * (double) i_47_
					/ (double) i;
			double d_49_ = d_43_ + (d_45_ - d_43_) * (double) i_47_
					/ (double) i;
			int i_50_ = (int) (d_48_ + 0.5) - _goto.x;
			int i_51_ = (int) (d_49_ + 0.5) - _goto.y;
			if (i_50_ >= 0 && i_50_ < _else.length && i_51_ >= 0
					&& i_51_ < _else[0].length) {
				boolean bool = _else[i_50_][i_51_] == 0;
				if (i_47_ % 2 == 0 && !bool)
					i_46_++;
				else if (i_47_ % 2 != 0 && bool == true)
					i_46_++;
			}
		}
		d_42_ = d_44_;
		d_43_ = d_45_;
		d_44_ = d_40_ - (d_40_ - d_36_) / (double) (i + 2);
		d_45_ = d_41_ - (d_41_ - d_37_) / (double) (i_33_ + 2);
		for (int i_52_ = 0; i_52_ <= i_33_; i_52_++) {
			double d_53_ = d_42_ + (d_44_ - d_42_) * (double) i_52_
					/ (double) i_33_;
			double d_54_ = d_43_ + (d_45_ - d_43_) * (double) i_52_
					/ (double) i_33_;
			int i_55_ = (int) (d_53_ + 0.5) - _goto.x;
			int i_56_ = (int) (d_54_ + 0.5) - _goto.y;
			if (i_55_ >= 0 && i_55_ < _else.length && i_56_ >= 0
					&& i_56_ < _else[0].length) {
				boolean bool = _else[i_55_][i_56_] == 0;
				if (i_52_ % 2 == 0 && !bool)
					i_46_++;
				else if (i_52_ % 2 != 0 && bool == true)
					i_46_++;
			}
		}
		i_46_ *= 100;
		i_46_ /= i + i_33_;
		return i_46_;
	}

	int a(int i, int i_57_, int i_58_, int i_59_) {
		double d = _if[0].x;
		double d_60_ = _if[0].y;
		double d_61_ = _if[1].x;
		double d_62_ = _if[1].y;
		double d_63_ = _if[2].x;
		double d_64_ = _if[2].y;
		double d_65_ = _if[3].x;
		double d_66_ = _if[3].y;
		double d_67_ = d_61_ + (d_65_ - d_61_) / (double) (i_57_ + 2);
		double d_68_ = d_62_ + (d_66_ - d_62_) / (double) (i_58_ + 2);
		double d_69_ = d_63_ - (d_63_ - d) / (double) (i_57_ + 2);
		double d_70_ = d_64_ - (d_64_ - d_60_) / (double) (i_58_ + 2);
		int i_71_ = 0;
		if (i == 1) {
			for (int i_72_ = 0; i_72_ <= i_57_ / 2; i_72_++) {
				double d_73_ = (d_67_ + (d_69_ - d_67_) * (double) i_72_
						/ (double) i_57_);
				double d_74_ = (d_68_ + (d_70_ - d_68_) * (double) i_72_
						/ (double) i_57_);
				int i_75_ = (int) (d_73_ + 0.5) - _goto.x;
				int i_76_ = (int) (d_74_ + 0.5) - _goto.y;
				if (i_75_ >= 0 && i_75_ < _else.length && i_76_ >= 0
						&& i_76_ < _else[0].length) {
					boolean bool = _else[i_75_][i_76_] == 0;
					if (i_72_ % 2 == 0 && !bool)
						i_71_++;
					else if (i_72_ % 2 != 0 && bool == true)
						i_71_++;
				}
			}
		} else if (i == 2) {
			for (int i_77_ = i_57_ / 2; i_77_ <= i_57_; i_77_++) {
				double d_78_ = (d_67_ + (d_69_ - d_67_) * (double) i_77_
						/ (double) i_57_);
				double d_79_ = (d_68_ + (d_70_ - d_68_) * (double) i_77_
						/ (double) i_57_);
				int i_80_ = (int) (d_78_ + 0.5) - _goto.x;
				int i_81_ = (int) (d_79_ + 0.5) - _goto.y;
				if (i_80_ >= 0 && i_80_ < _else.length && i_81_ >= 0
						&& i_81_ < _else[0].length) {
					boolean bool = _else[i_80_][i_81_] == 0;
					if (i_77_ % 2 == 0 && !bool)
						i_71_++;
					else if (i_77_ % 2 != 0 && bool == true)
						i_71_++;
				}
			}
			d_67_ = d_69_;
			d_68_ = d_70_;
			d_69_ = d_65_ - (d_65_ - d_61_) / (double) (i_57_ + 2);
			d_70_ = d_66_ - (d_66_ - d_62_) / (double) (i_58_ + 2);
			for (int i_82_ = i_58_ / 2; i_82_ <= i_58_; i_82_++) {
				double d_83_ = (d_67_ + (d_69_ - d_67_) * (double) i_82_
						/ (double) i_58_);
				double d_84_ = (d_68_ + (d_70_ - d_68_) * (double) i_82_
						/ (double) i_58_);
				int i_85_ = (int) (d_83_ + 0.5) - _goto.x;
				int i_86_ = (int) (d_84_ + 0.5) - _goto.y;
				if (i_85_ >= 0 && i_85_ < _else.length && i_86_ >= 0
						&& i_86_ < _else[0].length) {
					boolean bool = _else[i_85_][i_86_] == 0;
					if (i_82_ % 2 == 0 && !bool)
						i_71_++;
					else if (i_82_ % 2 != 0 && bool == true)
						i_71_++;
				}
			}
		} else if (i == 3) {
			d_67_ = d_69_;
			d_68_ = d_70_;
			d_69_ = d_65_ - (d_65_ - d_61_) / (double) (i_57_ + 2);
			d_70_ = d_66_ - (d_66_ - d_62_) / (double) (i_58_ + 2);
			for (int i_87_ = 0; i_87_ <= i_58_ / 2; i_87_++) {
				double d_88_ = (d_67_ + (d_69_ - d_67_) * (double) i_87_
						/ (double) i_58_);
				double d_89_ = (d_68_ + (d_70_ - d_68_) * (double) i_87_
						/ (double) i_58_);
				int i_90_ = (int) (d_88_ + 0.5) - _goto.x;
				int i_91_ = (int) (d_89_ + 0.5) - _goto.y;
				if (i_90_ >= 0 && i_90_ < _else.length && i_91_ >= 0
						&& i_91_ < _else[0].length) {
					boolean bool = _else[i_90_][i_91_] == 0;
					if (i_87_ % 2 == 0 && !bool)
						i_71_++;
					else if (i_87_ % 2 != 0 && bool == true)
						i_71_++;
				}
			}
		}
		return i_71_;
	}

	void a(int i, int i_92_, int i_93_) {
		int i_94_ = a(i, i_92_);
		PointDouble[] var_ays = _if;
		double d = var_ays[2].x;
		double d_95_ = var_ays[2].y;
		double d_96_ = 0.0;
		double d_97_ = 0.0;
		if (b == 0) {
			d_96_ = var_ays[1].x;
			d_97_ = var_ays[1].y;
		}
		double d_98_ = 0.0;
		double d_99_ = 0.0;
		if (_for == 0) {
			d_98_ = var_ays[3].x;
			d_99_ = var_ays[3].y;
		}
		int i_100_ = -2147483648;
		int i_101_ = -1;
		int i_102_ = -1;
		int i_103_ = -1;
		int i_104_ = -1;
		for (int i_105_ = -i_93_; i_105_ <= i_93_; i_105_++) {
			for (int i_106_ = -i_93_; i_106_ <= i_93_; i_106_++) {
				var_ays[2].x = d + (double) i_106_;
				var_ays[2].y = d_95_ + (double) i_105_;
				if (b == 0 && _for < _byte.height - 1) {
					var_ays[1].x = d_96_ + (double) i_106_;
					var_ays[1].y = d_97_ + (double) i_105_;
				}
				if (_for == 0 && b < _byte.width - 1) {
					var_ays[3].x = d_98_ + (double) i_106_;
					var_ays[3].y = d_99_ + (double) i_105_;
				}
				int i_107_ = i_106_ == 0 && i_105_ == 0 ? 1 : 0;
				i_94_ = _if(i, i_92_, i_107_);
				if (i_94_ > i_100_) {
					i_100_ = i_94_;
					i_101_ = i_102_ = i_106_;
					i_103_ = i_104_ = i_105_;
				} else if (i_94_ == i_100_) {
					if (i_106_ < i_101_)
						i_101_ = i_106_;
					else if (i_106_ > i_102_)
						i_102_ = i_106_;
					if (i_105_ < i_103_)
						i_103_ = i_105_;
					else if (i_105_ > i_104_)
						i_104_ = i_105_;
				}
			}
		}
		int i_108_ = (i_101_ + i_102_) / 2;
		int i_109_ = (i_103_ + i_104_) / 2;
		var_ays[2].x = d + (double) i_108_;
		var_ays[2].y = d_95_ + (double) i_109_;
		if (b == 0 && _for < _byte.height - 1) {
			var_ays[1].x = d_96_ + (double) i_108_;
			var_ays[1].y = d_97_ + (double) i_109_;
		}
		if (_for == 0 && _for < _byte.width - 1) {
			var_ays[3].x = d_98_ + (double) i_108_;
			var_ays[3].y = d_99_ + (double) i_109_;
		}
	}

	void a(boolean bool, int i, int i_110_) {
		af var_af;
		af var_af_111_;
		if (bool) {
			var_af = a(-1, i, i, i_110_, false);
			var_af_111_ = a(i_110_, i, i, i_110_, false);
		} else {
			var_af = a(i_110_, -1, i, i_110_, false);
			var_af_111_ = a(i_110_, i, i, i_110_, false);
		}
		int i_112_ = var_af.a;
		int i_113_ = var_af._if;
		int i_114_ = var_af_111_.a;
		int i_115_ = var_af_111_._if;
		int i_116_ = Math.abs(i_114_ - i_112_);
		int i_117_ = Math.abs(i_115_ - i_113_);
		boolean bool_118_ = i_116_ > i_117_;
		int i_119_;
		int i_120_;
		if (bool_118_) {
			i_119_ = i_116_;
			i_120_ = i_117_;
		} else {
			i_119_ = i_117_;
			i_120_ = i_116_;
		}
		int i_121_ = i_119_ / 2;
		int i_122_ = Math.max(i_116_, i_117_) + 1;
		int[] is = new int[i_122_];
		af[] var_afs = new af[i_122_];
		int i_123_ = i_112_;
		int i_124_ = i_113_;
		for (int i_125_ = 0; i_125_ < i_122_; i_125_++) {
			var_afs[i_125_] = new af(i_123_, i_124_);
			if (i_123_ < 0 || i_123_ >= _else.length || i_124_ < 0
					|| i_124_ >= _else[0].length)
				is[i_125_] = 1;
			else
				is[i_125_] = _else[i_123_][i_124_];
			i_121_ -= i_120_;
			boolean bool_126_ = false;
			if (i_121_ < 0) {
				bool_126_ = true;
				i_121_ += i_119_;
			}
			if (bool_118_) {
				if (i_123_ == i_114_)
					break;
				if (i_114_ > i_112_)
					i_123_++;
				else
					i_123_--;
				if (bool_126_) {
					if (i_115_ > i_113_)
						i_124_++;
					else
						i_124_--;
				}
			} else {
				if (i_124_ == i_115_)
					break;
				if (i_115_ > i_113_)
					i_124_++;
				else
					i_124_--;
				if (bool_126_) {
					if (i_114_ > i_112_)
						i_123_++;
					else
						i_123_--;
				}
			}
		}
		int i_127_ = 1;
		int i_128_ = is[0];
		int i_129_ = 0;
		int[] is_130_ = new int[Math.max(i_110_, i) + 3];
		for (int i_131_ = 1; i_131_ < i_122_; i_131_++) {
			if (is[i_131_] != i_128_) {
				i_127_++;
				i_128_ = is[i_131_];
				int i_132_ = (i_129_ + i_131_) / 2;
				if (i_127_ < is_130_.length)
					is_130_[i_127_] = i_132_;
				i_129_ = i_131_;
			}
		}
		if (bool == true && i_127_ == i_110_ + 2 && is[0] == 0) {
			a = new af[i_110_];
			for (int i_133_ = 0; i_133_ < i_110_; i_133_++)
				a[i_133_] = new af(var_afs[is_130_[i_133_ + 3]].a + _goto.x,
						var_afs[is_130_[i_133_ + 3]]._if + _goto.y);
		} else if (!bool && i_127_ == i + 2 && is[0] == 0) {
			_try = new af[i];
			for (int i_134_ = 0; i_134_ < i; i_134_++)
				_try[i_134_] = new af(var_afs[is_130_[i_134_ + 3]].a + _goto.x,
						var_afs[is_130_[i_134_ + 3]]._if + _goto.y);
		}
	}

	public String toString() {
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 0; i < _if.length; i++)
			stringbuffer.append("[" + i + "] x=" + _if[i].x + " y="
					+ _if[i].y);
		return new String(stringbuffer);
	}
}
