/* a - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class Code11 extends Barcode {
	private static final boolean cO = false;

	private static final int cL = 6;

	private static final int cJ = 11;

	private static final int cI = 12;

	private static final int[] cK = { 111121, 211121, 121121, 221111, 112121,
			212111, 122111, 111221, 211211, 211111, 112111, 112211, 11221 };

	private static HashMap cN = null;

	private ArrayList cH = new ArrayList();

	private boolean check;

	private int cM;

	Code11(PropertiesHolder p, boolean chk, int i) {
		super(p);
		check = chk;
		cM = i;
	}

	int code() {
		return 1;
	}

	int _if(int i) {
		if (i >= 17 && i % 6 == 5)
			return i / 6 + 1;
		return -1;
	}

	boolean isCheck() {
		return check;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 6; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		int i_8_ = 2 * cM;
		while (i_4_ + 6 <= i_1_) {
			if ((i_4_ == 1 || is[i_4_ - 1] > i_5_ / i_8_)
					&& is[i_4_ + 6] < i_5_ / 2) {
				int i_9_ = _byte(is, i_4_, 6, 12);
				if (i_9_ == 11) {
					this.readData(i, i_0_, is, i_4_, 6, i_2_, i_9_, true, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			} else if (is[i_4_ - 1] < i_5_ / 2
					&& (i_4_ + 6 - 1 == i_1_ - 1 || is[i_4_ + 6 - 1] > i_5_
							/ i_8_)) {
				int i_10_ = _byte(is, i_4_, 5, 12);
				if (i_10_ == 12) {
					this.readData(i, i_0_, is, i_4_, 5, i_2_, i_10_, false, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 4] + is[i_4_ + 5];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		int i_12_ = 0;
		int i_13_ = 0;
		while_1_: do {
			for (;;) {
				if (i_13_ == 0) {
					int i_14_ = _byte(is, i_12_, 6, 12);
					i_12_ += 6;
					i_13_ = 1;
					if (i_14_ != 11)
						break while_1_;
					cH.clear();
					cH.add(new Integer(i_14_));
				} else {
					if (i_12_ >= i - 5)
						break;
					int i_15_ = _byte(is, i_12_, 6, 12);
					i_12_ += 6;
					i_13_++;
					if (i_15_ == -1)
						break while_1_;
					cH.add(new Integer(i_15_));
				}
			}
			if (i_12_ == i - 5) {
				int i_16_ = _byte(is, i_12_, 5, 12);
				if (i_16_ == 12) {
					cH.add(new Integer(i_16_));
					int i_17_ = cH.size() - 3;
					if (check)
						i_17_--;
					this.a(cH, i_17_, var_d, var_d_11_);
					return true;
				}
			}
		} while (false);
		return false;
	}

	private boolean _goto(String string) {
		int i = 0;
		int i_18_ = 1;
		int i_19_ = string.charAt(string.length() - (check ? 3 : 2));
		for (int i_20_ = string.length() - (check ? 4 : 3); i_20_ > 0; i_20_--) {
			int i_21_ = string.charAt(i_20_);
			i += i_21_ * i_18_;
			if (++i_18_ == 11)
				i_18_ = 1;
		}
		if (i % 11 != i_19_)
			return false;
		if (!check)
			return true;
		i = 0;
		i_18_ = 1;
		i_19_ = string.charAt(string.length() - 2);
		for (int i_22_ = string.length() - 3; i_22_ > 0; i_22_--) {
			int i_23_ = string.charAt(i_22_);
			i += i_23_ * i_18_;
			if (++i_18_ == 10)
				i_18_ = 1;
		}
		if (i % 11 != i_19_)
			return false;
		return true;
	}

	private final int _byte(int[] is, int i, int i_24_, int i_25_) {
		int i_26_ = 0;
		for (int i_27_ = 0; i_27_ < i_24_; i_27_++)
			i_26_ += is[i + i_27_];
		int i_28_ = i_26_ / i_24_;
		int i_29_ = 0;
		int i_30_ = 0;
		int i_31_ = 2147483647;
		int i_32_ = -1;
		for (int i_33_ = 0; i_33_ < i_24_; i_33_++) {
			i_29_ *= 10;
			if (is[i + i_33_] < i_28_ / 4)
				return -1;
			if (is[i + i_33_] <= i_28_)
				i_29_++;
			else if (is[i + i_33_] <= i_28_ * 4) {
				i_29_ += 2;
				i_30_++;
				if (is[i + i_33_] < i_31_) {
					i_31_ = is[i + i_33_];
					i_32_ = i_33_;
				}
			} else
				return -1;
		}
		if (i_30_ == 3) {
			int i_34_ = 1;
			for (int i_35_ = 0; i_35_ < i_24_ - i_32_ - 1; i_35_++)
				i_34_ *= 10;
			i_29_ -= i_34_;
		}
		Object object = cN.get(new Integer(i_29_));
		if (object == null)
			return 65535;
		int i_36_ = ((Integer) object).intValue();
		return i_36_;
	}

	String a(String string) {
		if (!_goto(string))
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 1; i < string.length() - 1; i++) {
			char c = string.charAt(i);
			if (c == '\n')
				stringbuffer.append('-');
			else
				stringbuffer.append((char) ('0' + c));
		}
		return new String(stringbuffer);
	}

	static {
		cN = new HashMap();
		for (int i = 0; i < cK.length; i++)
			cN.put(new Integer(cK[i]), new Integer(i));
	}
}
