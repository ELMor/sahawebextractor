/* x - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class Code39 extends Barcode {
	private static final boolean V = false;

	private static final int R = 10;

	private static final int P = 39;

	private static final int[] O = { 111221211, 211211112, 112211112,
			212211111, 111221112, 211221111, 112221111, 111211212, 211211211,
			112211211, 211112112, 112112112, 212112111, 111122112, 211122111,
			112122111, 111112212, 211112211, 112112211, 111122211, 211111122,
			112111122, 212111121, 111121122, 211121121, 112121121, 111111222,
			211111221, 112111221, 111121221, 221111112, 122111112, 222111111,
			121121112, 221121111, 122121111, 121111212, 221111211, 122111211,
			121121211, 121212111, 121211121, 121112121, 111212121 };

	private static HashMap T = null;

	private ArrayList M;

	private boolean W;

	private boolean X;

	private boolean N;

	private int S;

	private int[] U = new int[9];

	private int[] Q = new int[4];

	Code39(PropertiesHolder var_c, boolean bool, boolean bool_0_, boolean bool_1_, int i) {
		super(var_c);
		M = new ArrayList();
		W = bool;
		X = bool_0_;
		N = bool_1_;
		S = i;
	}

	int code() {
		return 3;
	}

	int _if(int i) {
		if (i > (X ? 10 : 20) && i % 10 == 9)
			return i / 10 + 1;
		return -1;
	}

	boolean isCheck() {
		return W;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	final int a(int i, int i_2_, int i_3_, int[] is, int i_4_, int i_5_) {
		int i_6_;
		if (is[0] == 0)
			i_6_ = 3;
		else
			i_6_ = 1;
		int i_7_ = is[i_6_];
		for (int i_8_ = 1; i_8_ < 9; i_8_++)
			i_7_ += is[i_6_ + i_8_];
		int i_9_ = 2147483647;
		int i_10_ = 4 * S;
		while (i_6_ + 9 <= i_3_) {
			if ((i_6_ == 1 || is[i_6_ - 1] > i_7_ / i_10_)
					&& is[i_6_ + 9] < i_7_ / 2) {
				int i_11_ = _do(is, i_6_, 9, 12);
				if (i_11_ == 39 || X && i_11_ >= 0 && i_11_ <= (N ? 38 : 43)) {
					this.readData(i, i_2_, is, i_6_, 9, i_4_, i_11_, true, i_5_);
					if (i_7_ < i_9_)
						i_9_ = i_7_;
				}
			} else if (is[i_6_ - 1] < i_7_ / 2
					&& (i_6_ + 9 == i_3_ - 1 || is[i_6_ + 9] > i_7_ / i_10_)) {
				int i_12_ = _do(is, i_6_, 9, 12);
				if (i_12_ == 39 || X && i_12_ >= 0 && i_12_ <= (N ? 38 : 43)) {
					this.readData(i, i_2_, is, i_6_, 9, i_4_, i_12_, false, i_5_);
					if (i_7_ < i_9_)
						i_9_ = i_7_;
				}
			}
			i_7_ -= is[i_6_] + is[i_6_ + 1];
			i_6_ += 2;
			i_7_ += is[i_6_ + 7] + is[i_6_ + 8];
		}
		return i_9_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_13_) {
		int i_14_ = 0;
		int i_15_ = 0;
		int i_16_ = _if(i);
		int i_17_ = 0;
		for (int i_18_ = 0; i_18_ < i; i_18_++)
			i_17_ += is[i_18_];
		int i_19_ = i_17_ / i_16_;
		for (int i_20_ = 0; i_20_ < i_16_; i_20_++) {
			int i_21_ = 0;
			for (int i_22_ = 0; i_22_ < 9; i_22_++)
				i_21_ += is[10 * i_20_ + i_22_];
			if (i_20_ < i_16_ - 1)
				i_21_ += is[10 * i_20_ + 9] / 2;
			else
				i_21_ += is[10 * i_20_ - 1] / 2;
			int i_23_ = Math.abs(i_21_ - i_19_);
			if (i_23_ > i_19_ / 5)
				return false;
		}
		while_4_: do {
			for (;;) {
				if (i_15_ == 0) {
					int i_24_ = _do(is, i_14_, 9, 12);
					i_14_ += 10;
					i_15_ = 1;
					if (i_24_ != 39 && !X)
						break while_4_;
					M.clear();
					M.add(new Integer(i_24_));
				} else {
					if (i - i_14_ <= 9)
						break;
					int i_25_ = _do(is, i_14_, 9, 12);
					if ((i_25_ < 0 || i_25_ > 43) && i_25_ != 65535)
						break while_4_;
					i_14_ += 10;
					i_15_++;
					M.add(new Integer(i_25_));
				}
			}
			int i_26_ = _do(is, i_14_, 9, 12);
			if (i_26_ == 39 || X) {
				M.add(new Integer(i_26_));
				if (i_14_ == i - 9) {
					this.a(M, -1, var_d, var_d_13_);
					return true;
				}
			}
		} while (false);
		return false;
	}

	private boolean _if(String string) {
		int i = 0;
		for (int i_27_ = 1; i_27_ < string.length() - 2; i_27_++) {
			int i_28_ = string.charAt(i_27_);
			if (i_28_ > 39)
				i_28_--;
			i += i_28_;
		}
		int i_29_ = string.charAt(string.length() - 2);
		if (i_29_ > 39)
			i_29_--;
		return i % 43 == i_29_;
	}

	private final int _do(int[] is, int i, int i_30_, int i_31_) {
		int i_32_ = 0;
		for (int i_33_ = 0; i_33_ < 9; i_33_++) {
			U[i_33_] = is[i + i_33_];
			i_32_ += is[i + i_33_];
		}
		for (int i_34_ = 0; i_34_ < 4; i_34_++) {
			int i_35_ = 0;
			for (int i_36_ = 1; i_36_ < 9; i_36_++) {
				if (U[i_36_] > U[i_35_])
					i_35_ = i_36_;
			}
			Q[i_34_] = i_35_;
			U[i_35_] = -1;
		}
		for (int i_37_ = 0; i_37_ < 4; i_37_++)
			U[Q[i_37_]] = is[i + Q[i_37_]];
		if (U[Q[0]] > 2 * U[Q[2]])
			return -1;
		if (U[Q[3]] == U[Q[2]]) {
			if (Q[0] % 2 == 1 && Q[1] % 2 == 0 || Q[0] % 2 == 0
					&& Q[1] % 2 == 1) {
				int i_38_ = 0;
				for (int i_39_ = 0; i_39_ < 9; i_39_ += 2) {
					if (i_39_ != Q[0] && i_39_ != Q[1] && U[i_39_] == U[Q[2]]) {
						Q[2] = i_39_;
						i_38_++;
					}
				}
				if (i_38_ != 1)
					return 65535;
			} else if (Q[0] % 2 == 1 && Q[1] % 2 == 1) {
				int i_40_ = 0;
				for (int i_41_ = 1; i_41_ < 9; i_41_ += 2) {
					if (i_41_ != Q[0] && i_41_ != Q[1] && U[i_41_] == U[Q[2]]) {
						Q[2] = i_41_;
						i_40_++;
					}
				}
				if (i_40_ != 1)
					return 65535;
			} else if (Q[0] % 2 == 0 && Q[1] % 2 == 0) {
				int i_42_ = 0;
				for (int i_43_ = 1; i_43_ < 9; i_43_ += 2) {
					if (i_43_ != Q[0] && i_43_ != Q[1] && U[i_43_] == U[Q[2]]) {
						Q[2] = i_43_;
						i_42_++;
					}
				}
				if (i_42_ != 1)
					return 65535;
			} else
				return 65535;
		}
		if (X) {
			Q = new int[9];
			for (int i_44_ = 0; i_44_ < 9; i_44_++) {
				int i_45_ = 0;
				for (int i_46_ = 1; i_46_ < 9; i_46_++) {
					if (U[i_46_] > U[i_45_])
						i_45_ = i_46_;
				}
				Q[i_44_] = i_45_;
				U[i_45_] = -1;
			}
			for (int i_47_ = 0; i_47_ < 9; i_47_++)
				U[Q[i_47_]] = is[i + Q[i_47_]];
			int i_48_ = 0;
			for (int i_49_ = 0; i_49_ < 3; i_49_++)
				i_48_ += U[Q[i_49_]];
			i_48_ /= 3;
			int i_50_ = 0;
			for (int i_51_ = 3; i_51_ < 9; i_51_++)
				i_50_ += U[Q[i_51_]];
			i_50_ /= 6;
			if (U[Q[0]] > i_48_ * 2 || U[Q[2]] < i_48_ / 2
					|| U[Q[3]] > i_50_ * 2 || U[Q[8]] < i_50_ / 2)
				return -1;
		}
		for (int i_52_ = 0; i_52_ < 9; i_52_++)
			U[i_52_] = 1;
		for (int i_53_ = 0; i_53_ < 3; i_53_++)
			U[Q[i_53_]] = 2;
		int i_54_ = 0;
		for (int i_55_ = 0; i_55_ < 9; i_55_++)
			i_54_ = i_54_ * 10 + U[i_55_];
		Object object = T.get(new Integer(i_54_));
		if (object == null)
			return 65535;
		int i_56_ = ((Integer) object).intValue();
		return i_56_;
	}

	String a(String string) {
		if (W && !X && !_if(string))
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		int i = W ? 2 : 1;
		if (X)
			i = 0;
		for (int i_57_ = 0; i_57_ < string.length() - i; i_57_++) {
			char c = string.charAt(i_57_);
			if (c < '\n')
				stringbuffer.append((char) ('0' + c));
			else if (c < '$')
				stringbuffer.append((char) ('A' + c - '\n'));
			else if (c == '$')
				stringbuffer.append('-');
			else if (c == '%')
				stringbuffer.append('.');
			else if (c == '&')
				stringbuffer.append(' ');
			else if (c == '(')
				stringbuffer.append('$');
			else if (c == ')')
				stringbuffer.append('/');
			else if (c == '*')
				stringbuffer.append('+');
			else if (c == '+')
				stringbuffer.append('%');
		}
		return new String(stringbuffer);
	}

	static {
		T = new HashMap();
		for (int i = 0; i < O.length; i++)
			T.put(new Integer(O[i]), new Integer(i));
	}
}
