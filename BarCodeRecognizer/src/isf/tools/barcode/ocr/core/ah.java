/* ah - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Dimension;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.Iterator;
import java.util.TreeSet;

class ah {
	private static final boolean h = false;

	static int _try;

	static double[] _if;

	static double[] p;

	static double[] n;

	private static final int l = 23;

	private static final int c = 30;

	private static final int[][] t = { { 8, 8 }, { 10, 10 }, { 12, 12 },
			{ 14, 14 }, { 16, 16 }, { 18, 18 }, { 20, 20 }, { 22, 22 },
			{ 24, 24 }, { 28, 28 }, { 32, 32 }, { 36, 36 }, { 40, 40 },
			{ 44, 44 }, { 48, 48 }, { 56, 56 }, { 64, 64 }, { 72, 72 },
			{ 80, 80 }, { 88, 88 }, { 96, 96 }, { 108, 108 }, { 120, 120 },
			{ 132, 132 }, { 6, 16 }, { 6, 28 }, { 10, 24 }, { 10, 32 },
			{ 14, 32 }, { 14, 44 } };

	private static final int[][] _void = { { 8, 8 }, { 10, 10 }, { 12, 12 },
			{ 14, 14 }, { 16, 16 }, { 18, 18 }, { 20, 20 }, { 22, 22 },
			{ 24, 24 }, { 14, 14 }, { 16, 16 }, { 18, 18 }, { 20, 20 },
			{ 22, 22 }, { 24, 24 }, { 14, 14 }, { 16, 16 }, { 18, 18 },
			{ 20, 20 }, { 22, 22 }, { 24, 24 }, { 18, 18 }, { 20, 20 },
			{ 22, 22 }, { 6, 16 }, { 6, 14 }, { 10, 24 }, { 10, 16 },
			{ 14, 16 }, { 14, 22 } };

	private static final int[] d = { 10, 12, 14, 16, 18, 20, 22, 24, 26, 32,
			36, 40, 44, 48, 52, 64, 72, 80, 88, 96, 104, 120, 132, 144 };

	private static final int[] _new = { 8, 12, 16 };

	private static final int[] e = { 18, 32, 26, 36, 48 };

	private static final int[] _char = { 8, 12, 18, 24, 32, 40, 50, 60, 72, 98,
			128, 162, 200, 242, 288, 392, 512, 648, 800, 968, 1152, 1458, 1800,
			2178, 12, 21, 30, 40, 56, 77 };

	private static final int[] f = { 3, 5, 8, 12, 18, 22, 30, 36, 44, 62, 86,
			114, 144, 174, 102, 140, 92, 114, 144, 174, 136, 175, 163, 156, 5,
			10, 16, 22, 32, 49 };

	private static final int[] a = { 5, 7, 10, 12, 14, 18, 20, 24, 28, 36, 42,
			48, 56, 68, 42, 56, 36, 48, 56, 68, 56, 68, 62, 62, 7, 11, 14, 18,
			24, 28 };

	private static final int[] s = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			2, 2, 4, 4, 4, 4, 6, 6, 8, 10, 1, 1, 1, 1, 1, 1 };

	private static short[][][] _else = new short[30][][];

	private Recognizer g;

	private int m;

	boolean v;

	a6[][] q;

	private int _goto;

	private int u;

	Polygon i;

	int k;

	Rectangle b;

	String o;

	int[] _long;

	private ar _int;

	PropertiesHolder r;

	private static final int _byte = 0;

	private static final int j = 1;

	private static final int w = 2;

	private static final int _for = 3;

	private static final int _case = 4;

	private static final int _do = 5;

	private class a implements Comparable {
		private int _if;

		private int a;

		private int _do;

		private a(int i, int i_0_, int[] is, int i_1_, int i_2_) {
			_if = i;
			a = i_0_;
			_do = is[2] + is[3] + (1000 - is[0]) + (1000 - is[1]);
			_do = _do * 100 + i;
		}

		public int compareTo(Object object) {
			return ((a) object)._do - _do;
		}
	}

	ah(Polygon polygon, Recognizer var_a1, int i, PropertiesHolder var_c) {
		this.i = polygon;
		b = polygon.getBounds();
		g = var_a1;
		m = i;
		r = var_c;
		PointDouble var_ay = new PointDouble((double) polygon.xpoints[0],
				(double) polygon.ypoints[0]);
		PointDouble var_ay_6_ = new PointDouble((double) polygon.xpoints[1],
				(double) polygon.ypoints[1]);
		PointDouble var_ay_7_ = new PointDouble((double) polygon.xpoints[2],
				(double) polygon.ypoints[2]);
		double d = var_ay.mod(var_ay_6_);
		double d_8_ = var_ay_6_.mod(var_ay_7_);
		double d_9_ = Math.max(d, d_8_) / Math.min(d, d_8_);
		v = d_9_ > 1.3;
	}

	void a() {
		if (q != null) {
			for (int i = 0; i < q.length; i++) {
				for (int i_10_ = 0; i_10_ < q[0].length; i_10_++) {
					q[i][i_10_].a();
					q[i][i_10_] = null;
				}
			}
		}
		q = null;
		this.i = null;
		b = null;
		o = null;
		_int = null;
		r = null;
	}

	boolean _if() {
		Iterator iterator = _do();
		while_26_: do {
			a var_a;
			a6[][] var_a6s;
			do {
				if (!iterator.hasNext())
					break while_26_;
				var_a = (a) iterator.next();
				k = var_a.a;
				var_a6s = a(var_a);
				q = var_a6s;
			} while (a(var_a, var_a6s) != true);
			return true;
		} while (false);
		return false;
	}

	private boolean a(a var_a, a6[][] var_a6s) {
		int i = var_a._if;
		int i_11_ = _void[i][1];
		int i_12_ = _void[i][0];
		double d = Math.abs(var_a6s[0][0]._if[0].x - var_a6s[0][0]._if[1].x);
		double d_13_ = Math.abs(var_a6s[0][0]._if[0].y
				- var_a6s[0][0]._if[1].y);
		int i_14_ = (int) Math.max(d, d_13_) / i_11_;
		for (int i_15_ = 0; i_15_ < var_a6s.length; i_15_++) {
			for (int i_16_ = 0; i_16_ < var_a6s[i_15_].length; i_16_++) {
				a6 var_a6 = var_a6s[i_15_][i_16_];
				var_a6.a(i_11_, i_12_, i_14_);
				if (i_16_ < var_a6s[0].length - 1 && i_15_ < var_a6s.length - 1) {
					a6 var_a6_17_ = var_a6s[i_15_ + 1][i_16_ + 1];
					var_a6_17_._if[0].x = var_a6._if[2].x;
					var_a6_17_._if[0].y = var_a6._if[2].y;
					if (i_15_ == 0) {
						var_a6_17_ = var_a6s[0][i_16_ + 1];
						var_a6_17_._if[0].x = var_a6._if[1].x;
						var_a6_17_._if[0].y = var_a6._if[1].y;
					}
					if (i_16_ == 0) {
						var_a6_17_ = var_a6s[i_15_ + 1][0];
						var_a6_17_._if[0].x = var_a6._if[3].x;
						var_a6_17_._if[0].y = var_a6._if[3].y;
					}
				}
				if (i_16_ < var_a6s[0].length - 1) {
					a6 var_a6_18_ = var_a6s[i_15_][i_16_ + 1];
					var_a6_18_._if[3].x = var_a6._if[2].x;
					var_a6_18_._if[3].y = var_a6._if[2].y;
				}
				if (i_15_ < var_a6s.length - 1) {
					a6 var_a6_19_ = var_a6s[i_15_ + 1][i_16_];
					var_a6_19_._if[1].x = var_a6._if[2].x;
					var_a6_19_._if[1].y = var_a6._if[2].y;
				}
			}
		}
		_goto = t[i][1];
		u = t[i][0];
		for (int i_20_ = 0; i_20_ < var_a6s.length; i_20_++) {
			for (int i_21_ = 0; i_21_ < var_a6s[0].length; i_21_++) {
				a6 var_a6 = var_a6s[i_20_][i_21_];
				var_a6.a(true, i_11_, i_12_);
				var_a6.a(false, i_11_, i_12_);
			}
		}
		_goto = t[i][1];
		u = t[i][0];
		if (_else[i] == null)
			a(i);
		if (_int == null)
			_int = new ar(5);
		int[] is = a(i, i_11_, i_12_, false);
		int i_22_ = _int.a(is, i);
		if (i_22_ < 0) {
			is = a(i, i_11_, i_12_, true);
			i_22_ = _int.a(is, i);
			if (i_22_ < 0)
				return false;
		}
		_if(i, is);
		return true;
	}

	private int[] a(int i, int i_23_, int i_24_, boolean bool) {
		int[] is = new int[_char[i]];
		for (int i_25_ = 0; i_25_ < u; i_25_++) {
			for (int i_26_ = 0; i_26_ < _goto; i_26_++) {
				short i_27_ = _else[i][u - i_25_ - 1][i_26_];
				if (i_27_ != 0 && i_27_ != 1) {
					int i_28_ = i_27_ / 10;
					int i_29_ = i_27_ % 10;
					int i_30_ = q[i_26_ / i_23_][i_25_ / i_24_]._if(i_25_
							% i_24_, i_26_ % i_23_, i_23_, i_24_, bool);
					if (i_30_ == 1)
						is[i_28_ - 1] |= 256 >>> i_29_;
				}
			}
		}
		return is;
	}

	private int a(int i, int[] is) {
		return is[i];
	}

	private void _if(int i, int[] is) {
		int i_31_ = 0;
		int i_32_ = -1;
		int i_33_ = 0;
		int i_34_ = 0;
		StringBuffer stringbuffer = new StringBuffer();
		int i_35_ = f[i] * s[i];
		if (i == 23)
			i_35_ -= 2;
		for (int i_36_ = 0; i_36_ < i_35_; i_36_++) {
			int i_37_ = a(i_36_, is);
			if (i_31_ == 0 || i_31_ == 1 && i_36_ == i_35_ - 1 || i_31_ == 2
					&& i_36_ == i_35_ - 1 || i_31_ == 3 && i_36_ == i_35_ - 1
					|| i_31_ == 4 && i_36_ >= i_35_ - 2) {
				if (i_37_ > 0 && i_37_ <= 128)
					stringbuffer.append((char) (i_37_ - 1));
				else {
					if (i_37_ == 129)
						break;
					if (i_37_ >= 130 && i_37_ <= 229) {
						int i_38_ = i_37_ - 130;
						stringbuffer.append((char) (48 + i_38_ / 10));
						stringbuffer.append((char) (48 + i_38_ % 10));
					} else if (i_37_ == 230) {
						i_31_ = 1;
						i_32_ = -1;
					} else if (i_37_ == 231)
						i_31_ = 5;
					else if (i_37_ == 232)
						stringbuffer.append((char) i_37_);
					else if (i_37_ == 233) {
						_long = new int[3];
						int i_39_ = a(++i_36_, is);
						_long[0] = i_39_ & 0xf;
						_long[1] = 17 - ((i_39_ & 0xf0) >> 4);
						i_39_ = a(++i_36_, is) * 254;
						_long[2] = i_39_ + a(++i_36_, is);
					} else if (i_37_ == 234)
						stringbuffer.append((char) i_37_);
					else if (i_37_ == 235) {
						int i_40_ = a(++i_36_, is);
						stringbuffer.append((char) (i_40_ + 127));
					} else if (i_37_ == 236) {
						if (i_36_ == 0) {
							stringbuffer.append("[)>\03605\035");
							i_34_ = 5;
						}
					} else if (i_37_ == 237) {
						if (i_36_ == 0) {
							stringbuffer.append("[)>\03606\035");
							i_34_ = 6;
						}
					} else if (i_37_ == 238)
						i_31_ = 3;
					else if (i_37_ == 239) {
						i_31_ = 2;
						i_32_ = -1;
					} else if (i_37_ == 240)
						i_31_ = 4;
					else if (i_37_ == 241) {
						stringbuffer.append((char) i_37_);
						int i_41_ = a(++i_36_, is);
						stringbuffer.append((char) i_41_);
						if (i_41_ >= 128) {
							int i_42_ = a(++i_36_, is);
							stringbuffer.append((char) i_42_);
							if (i_41_ >= 192) {
								int i_43_ = a(++i_36_, is);
								stringbuffer.append((char) i_43_);
							}
						}
					}
				}
			} else if (i_31_ == 1 || i_31_ == 2) {
				int i_44_;
				int i_45_;
				if (i_31_ == 1) {
					i_44_ = 65;
					i_45_ = 97;
				} else {
					i_44_ = 97;
					i_45_ = 65;
				}
				if (i_37_ == 254)
					i_31_ = 0;
				else {
					int i_46_ = a(++i_36_, is);
					int i_47_ = i_37_ * 256 + i_46_ - 1;
					int[] is_48_ = new int[3];
					is_48_[2] = i_47_ % 40;
					i_47_ /= 40;
					is_48_[1] = i_47_ % 40;
					is_48_[0] = i_47_ / 40;
					for (int i_49_ = 0; i_49_ < 3; i_49_++) {
						int i_50_ = is_48_[i_49_];
						if (i_32_ == -1) {
							if (i_50_ <= 2)
								i_32_ = i_50_;
							else if (i_50_ == 3) {
								stringbuffer.append((char) (32 + i_33_));
								i_33_ = 0;
							} else if (i_50_ >= 4 && i_50_ <= 13) {
								stringbuffer
										.append((char) (48 + i_50_ - 4 + i_33_));
								i_33_ = 0;
							} else {
								stringbuffer
										.append((char) (i_44_ + i_50_ - 14 + i_33_));
								i_33_ = 0;
							}
						} else if (i_32_ == 0) {
							stringbuffer.append((char) (i_50_ + i_33_));
							i_32_ = -1;
							i_33_ = 0;
						} else if (i_32_ == 1) {
							if (i_50_ <= 14) {
								stringbuffer
										.append((char) (33 + i_50_ + i_33_));
								i_33_ = 0;
							} else if (i_50_ <= 21) {
								stringbuffer
										.append((char) (43 + i_50_ + i_33_));
								i_33_ = 0;
							} else if (i_50_ <= 26) {
								stringbuffer
										.append((char) (69 + i_50_ + i_33_));
								i_33_ = 0;
							} else if (i_50_ == 27) {
								stringbuffer.append('\u00e8');
								i_33_ = 0;
							} else if (i_50_ == 30)
								i_33_ = 128;
							i_32_ = -1;
						} else if (i_32_ == 2) {
							if (i_50_ == 0)
								stringbuffer.append((char) (96 + i_33_));
							else if (i_50_ <= 26)
								stringbuffer
										.append((char) (i_45_ + i_50_ - 1 + i_33_));
							else
								stringbuffer
										.append((char) (96 + i_50_ + i_33_));
							i_32_ = -1;
							i_33_ = 0;
						}
					}
				}
			} else if (i_31_ == 3) {
				if (i_37_ == 254)
					i_31_ = 0;
				else {
					int i_51_ = a(++i_36_, is);
					int i_52_ = i_37_ * 256 + i_51_ - 1;
					int[] is_53_ = new int[3];
					is_53_[2] = i_52_ % 40;
					i_52_ /= 40;
					is_53_[1] = i_52_ % 40;
					is_53_[0] = i_52_ / 40;
					for (int i_54_ = 0; i_54_ < 3; i_54_++) {
						if (is_53_[i_54_] == 0)
							stringbuffer.append('\r');
						else if (is_53_[i_54_] == 1)
							stringbuffer.append('*');
						else if (is_53_[i_54_] == 2)
							stringbuffer.append('>');
						else if (is_53_[i_54_] == 3)
							stringbuffer.append(' ');
						else if (is_53_[i_54_] <= 13)
							stringbuffer
									.append((char) (48 + is_53_[i_54_] - 4));
						else
							stringbuffer
									.append((char) (65 + is_53_[i_54_] - 14));
					}
				}
			} else if (i_31_ == 4) {
				int i_55_ = i_37_ >>> 2;
				if (i_55_ == 31)
					i_31_ = 0;
				else {
					stringbuffer.append(i_55_ > 31 ? (char) i_55_
							: (char) (i_55_ + 64));
					int i_56_ = a(++i_36_, is);
					i_55_ = (i_37_ & 0x3) * 16 + (i_56_ >>> 4);
					if (i_55_ == 31)
						i_31_ = 0;
					else {
						stringbuffer.append(i_55_ > 31 ? (char) i_55_
								: (char) (i_55_ + 64));
						int i_57_ = a(++i_36_, is);
						i_55_ = (i_56_ & 0xf) * 4 + (i_57_ >>> 6);
						if (i_55_ == 31)
							i_31_ = 0;
						else {
							stringbuffer.append(i_55_ > 31 ? (char) i_55_
									: (char) (i_55_ + 64));
							i_55_ = i_57_ & 0x3f;
							if (i_55_ == 31)
								i_31_ = 0;
							else
								stringbuffer.append(i_55_ > 31 ? (char) i_55_
										: (char) (i_55_ + 64));
						}
					}
				}
			} else if (i_31_ == 5) {
				int i_58_ = i_37_;
				int i_59_ = 149 * (i_36_ + 1) % 255 + 1;
				i_58_ -= i_59_;
				if (i_58_ < 0)
					i_58_ += 256;
				if (i_58_ == 0)
					i_58_ = i_35_ - i_36_ - 1;
				else if (i_58_ >= 250) {
					int i_60_ = a(++i_36_, is);
					i_60_ -= 149 * (i_36_ + 1) % 255 + 1;
					if (i_60_ < 0)
						i_60_ += 256;
					i_58_ = (i_58_ - 249) * 250 + i_60_;
				}
				for (int i_61_ = 0; i_61_ < i_58_; i_61_++) {
					int i_62_ = a(++i_36_, is);
					i_62_ -= 149 * (i_36_ + 1) % 255 + 1;
					if (i_62_ < 0)
						i_62_ += 256;
					stringbuffer.append((char) i_62_);
				}
				i_31_ = 0;
			}
		}
		if (i_34_ != 0)
			stringbuffer.append("\036\004");
		o = new String(stringbuffer);
	}

	private void a(int i) {
		_else[i] = new short[u][];
		for (int i_63_ = 0; i_63_ < u; i_63_++)
			_else[i][i_63_] = new short[_goto];
		short i_64_ = 1;
		int i_65_ = 4;
		int i_66_ = 0;
		do {
			if (i_65_ == u && i_66_ == 0) {
				ah var_ah_67_ = this;
				int i_68_ = i;
				short i_69_ = i_64_;
				i_64_++;
				var_ah_67_.a(i_68_, i_69_);
			}
			if (i_65_ == u - 2 && i_66_ == 0 && _goto % 4 != 0) {
				ah var_ah_70_ = this;
				int i_71_ = i;
				short i_72_ = i_64_;
				i_64_++;
				var_ah_70_._if(i_71_, i_72_);
			}
			if (i_65_ == u - 2 && i_66_ == 0 && _goto % 8 == 4) {
				ah var_ah_73_ = this;
				int i_74_ = i;
				short i_75_ = i_64_;
				i_64_++;
				var_ah_73_._do(i_74_, i_75_);
			}
			if (i_65_ == u + 4 && i_66_ == 2 && _goto % 8 == 0) {
				ah var_ah_76_ = this;
				int i_77_ = i;
				short i_78_ = i_64_;
				i_64_++;
				var_ah_76_._for(i_77_, i_78_);
			}
			do {
				if (i_65_ < u && i_66_ >= 0 && _else[i][i_65_][i_66_] == 0) {
					ah var_ah_79_ = this;
					int i_80_ = i;
					int i_81_ = i_65_;
					int i_82_ = i_66_;
					short i_83_ = i_64_;
					i_64_++;
					var_ah_79_.a(i_80_, i_81_, i_82_, i_83_);
				}
				i_65_ -= 2;
				i_66_ += 2;
			} while (i_65_ >= 0 && i_66_ < _goto);
			i_65_++;
			i_66_ += 3;
			do {
				if (i_65_ >= 0 && i_66_ < _goto && _else[i][i_65_][i_66_] == 0) {
					ah var_ah_84_ = this;
					int i_85_ = i;
					int i_86_ = i_65_;
					int i_87_ = i_66_;
					short i_88_ = i_64_;
					i_64_++;
					var_ah_84_.a(i_85_, i_86_, i_87_, i_88_);
				}
				i_65_ += 2;
				i_66_ -= 2;
			} while (i_65_ < u && i_66_ >= 0);
			i_65_ += 3;
			i_66_++;
		} while (i_65_ < u || i_66_ < _goto);
		if (_else[i][u - 1][_goto - 1] == 0)
			_else[i][u - 1][_goto - 1] = _else[i][u - 2][_goto - 2] = (short) 1;
	}

	private void a(int i, int i_89_, int i_90_, int i_91_, int i_92_) {
		if (i_89_ < 0) {
			i_89_ += u;
			i_90_ += 4 - (u + 4) % 8;
		}
		if (i_90_ < 0) {
			i_90_ += _goto;
			i_89_ += 4 - (_goto + 4) % 8;
		}
		_else[i][i_89_][i_90_] = (short) (10 * i_91_ + i_92_);
	}

	void a(int i, int i_93_, int i_94_, int i_95_) {
		a(i, i_93_ - 2, i_94_ - 2, i_95_, 1);
		a(i, i_93_ - 2, i_94_ - 1, i_95_, 2);
		a(i, i_93_ - 1, i_94_ - 2, i_95_, 3);
		a(i, i_93_ - 1, i_94_ - 1, i_95_, 4);
		a(i, i_93_ - 1, i_94_, i_95_, 5);
		a(i, i_93_, i_94_ - 2, i_95_, 6);
		a(i, i_93_, i_94_ - 1, i_95_, 7);
		a(i, i_93_, i_94_, i_95_, 8);
	}

	void a(int i, int i_96_) {
		a(i, u - 1, 0, i_96_, 1);
		a(i, u - 1, 1, i_96_, 2);
		a(i, u - 1, 2, i_96_, 3);
		a(i, 0, _goto - 2, i_96_, 4);
		a(i, 0, _goto - 1, i_96_, 5);
		a(i, 1, _goto - 1, i_96_, 6);
		a(i, 2, _goto - 1, i_96_, 7);
		a(i, 3, _goto - 1, i_96_, 8);
	}

	void _if(int i, int i_97_) {
		a(i, u - 3, 0, i_97_, 1);
		a(i, u - 2, 0, i_97_, 2);
		a(i, u - 1, 0, i_97_, 3);
		a(i, 0, _goto - 4, i_97_, 4);
		a(i, 0, _goto - 3, i_97_, 5);
		a(i, 0, _goto - 2, i_97_, 6);
		a(i, 0, _goto - 1, i_97_, 7);
		a(i, 1, _goto - 1, i_97_, 8);
	}

	void _do(int i, int i_98_) {
		a(i, u - 3, 0, i_98_, 1);
		a(i, u - 2, 0, i_98_, 2);
		a(i, u - 1, 0, i_98_, 3);
		a(i, 0, _goto - 2, i_98_, 4);
		a(i, 0, _goto - 1, i_98_, 5);
		a(i, 1, _goto - 1, i_98_, 6);
		a(i, 2, _goto - 1, i_98_, 7);
		a(i, 3, _goto - 1, i_98_, 8);
	}

	void _for(int i, int i_99_) {
		a(i, u - 1, 0, i_99_, 1);
		a(i, u - 1, _goto - 1, i_99_, 2);
		a(i, 0, _goto - 3, i_99_, 3);
		a(i, 0, _goto - 2, i_99_, 4);
		a(i, 0, _goto - 1, i_99_, 5);
		a(i, 1, _goto - 3, i_99_, 6);
		a(i, 1, _goto - 2, i_99_, 7);
		a(i, 1, _goto - 1, i_99_, 8);
	}

	private int[] a(int i, int i_100_, int[] is) {
		int[] is_101_ = new int[is.length];
		int i_102_ = i + 2;
		if (i_102_ > 3)
			i_102_ -= 4;
		int i_103_ = i_100_ + 2;
		if (i_103_ > 3)
			i_103_ -= 4;
		int i_104_ = _int(i, i_100_);
		int i_105_;
		int i_106_;
		int i_107_;
		int i_108_;
		if (!v) {
			i_105_ = (this.i.xpoints[i_102_] - this.i.xpoints[i]) / 2;
			i_106_ = (this.i.ypoints[i_102_] - this.i.ypoints[i]) / 2;
			i_107_ = (this.i.xpoints[i_103_] - this.i.xpoints[i_100_]) / 2;
			i_108_ = (this.i.ypoints[i_103_] - this.i.ypoints[i_100_]) / 2;
		} else {
			int i_109_ = (this.i.xpoints[i] - this.i.xpoints[i_100_]) / 2;
			int i_110_ = (this.i.ypoints[i] - this.i.ypoints[i_100_]) / 2;
			i_105_ = -i_109_ + i_110_;
			i_106_ = -i_109_ - i_110_;
			i_107_ = i_109_ + i_110_;
			i_108_ = -i_109_ + i_110_;
		}
		for (int i_111_ = 0; i_111_ < is.length; i_111_++) {
			int i_112_ = is[i_111_];
			if (i_104_ < i_112_ * 2)
				break;
			int i_113_ = this.i.xpoints[i] + i_105_ / i_112_;
			int i_114_ = this.i.ypoints[i] + i_106_ / i_112_;
			int i_115_ = this.i.xpoints[i_100_] + i_107_ / i_112_;
			int i_116_ = this.i.ypoints[i_100_] + i_108_ / i_112_;
			int i_117_ = 0;
			boolean bool = true;
			for (int i_118_ = 0; i_118_ < is[i_111_]; i_118_++) {
				int i_119_ = ((i_113_ * (i_112_ - 1 - i_118_) + i_118_ * i_115_)
						/ (i_112_ - 1) - b.x);
				if (i_119_ >= 0 && i_119_ < b.width) {
					int i_120_ = ((i_114_ * (i_112_ - 1 - i_118_) + i_118_
							* i_116_)
							/ (i_112_ - 1) - b.y);
					if (i_120_ >= 0 && i_120_ < b.height) {
						if (g.a(b.x + i_119_, b.y + i_120_) < m) {
							if (i_118_ > 0 && bool == true)
								i_117_++;
							bool = true;
						} else {
							if (i_118_ > 0 && !bool)
								i_117_++;
							bool = false;
						}
					}
				}
			}
			is_101_[i_111_] = i_117_ * 1000 / (i_112_ - 1);
		}
		return is_101_;
	}

	private int _int(int i, int i_121_) {
		if (i_121_ < 0)
			i_121_ += 4;
		else if (i_121_ > 3)
			i_121_ -= 4;
		int i_122_ = this.i.xpoints[i] - this.i.xpoints[i_121_];
		int i_123_ = this.i.ypoints[i] - this.i.ypoints[i_121_];
		return (int) Math.sqrt((double) (i_122_ * i_122_ + i_123_ * i_123_));
	}

	private Iterator _do() {
		TreeSet treeset = new TreeSet();
		int[][] is = new int[4][];
		int[] is_124_ = new int[4];
		if (!v) {
			is[0] = a(0, 1, d);
			is[1] = a(1, 2, d);
			is[2] = a(2, 3, d);
			is[3] = a(3, 0, d);
			for (int i = 0; i < is[0].length; i++) {
				for (int i_125_ = 0; i_125_ < 4; i_125_++) {
					int i_126_ = i_125_ == 3 ? 0 : i_125_ + 1;
					int i_127_ = i_126_ == 3 ? 0 : i_126_ + 1;
					int i_128_ = i_127_ == 3 ? 0 : i_127_ + 1;
					is_124_[0] = is[i_125_][i];
					is_124_[1] = is[i_126_][i];
					is_124_[2] = is[i_127_][i];
					is_124_[3] = is[i_128_][i];
					if ((Math.abs(is_124_[0]) < 400 || Math.abs(is_124_[1]) < 400)
							&& is_124_[2] > 800 && is_124_[3] > 800) {
						int i_129_ = i_126_ + 2;
						if (i_129_ > 3)
							i_129_ -= 4;
						treeset.add(new a(i, i_129_, is_124_, d[i], d[i]));
					}
				}
			}
		} else {
			int i = _int(0, 1);
			int i_130_ = _int(1, 2);
			int i_131_;
			if (i < i_130_) {
				i_131_ = 1;
				is[0] = a(0, 1, _new);
				is[1] = a(1, 2, e);
				is[2] = a(2, 3, _new);
				is[3] = a(3, 0, e);
			} else {
				i_131_ = 0;
				is[0] = a(1, 2, _new);
				is[1] = a(2, 3, e);
				is[2] = a(3, 0, _new);
				is[3] = a(0, 1, e);
			}
			is_124_[0] = is[0][0];
			is_124_[1] = is[3][0];
			is_124_[2] = is[2][0];
			is_124_[3] = is[1][0];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(24, 3 - i_131_, is_124_, 8, 18));
			is_124_[0] = is[2][0];
			is_124_[1] = is[1][0];
			is_124_[2] = is[0][0];
			is_124_[3] = is[3][0];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(24, 1 - i_131_, is_124_, 8, 18));
			is_124_[0] = is[0][0];
			is_124_[1] = is[3][1];
			is_124_[2] = is[2][0];
			is_124_[3] = is[1][1];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(25, 3 - i_131_, is_124_, 8, 32));
			is_124_[0] = is[2][0];
			is_124_[1] = is[1][1];
			is_124_[2] = is[0][0];
			is_124_[3] = is[3][1];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(25, 1 - i_131_, is_124_, 8, 32));
			is_124_[0] = is[0][1];
			is_124_[1] = is[3][2];
			is_124_[2] = is[2][1];
			is_124_[3] = is[1][2];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(26, 3 - i_131_, is_124_, 12, 26));
			is_124_[0] = is[2][1];
			is_124_[1] = is[1][2];
			is_124_[2] = is[0][1];
			is_124_[3] = is[3][2];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(26, 1 - i_131_, is_124_, 12, 26));
			is_124_[0] = is[0][1];
			is_124_[1] = is[3][3];
			is_124_[2] = is[2][1];
			is_124_[3] = is[1][3];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(27, 3 - i_131_, is_124_, 12, 36));
			is_124_[0] = is[2][1];
			is_124_[1] = is[1][3];
			is_124_[2] = is[0][1];
			is_124_[3] = is[3][3];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(27, 1 - i_131_, is_124_, 12, 36));
			is_124_[0] = is[0][2];
			is_124_[1] = is[3][3];
			is_124_[2] = is[2][2];
			is_124_[3] = is[1][3];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(28, 3 - i_131_, is_124_, 16, 36));
			is_124_[0] = is[2][2];
			is_124_[1] = is[1][3];
			is_124_[2] = is[0][2];
			is_124_[3] = is[3][3];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(28, 1 - i_131_, is_124_, 16, 36));
			is_124_[0] = is[0][2];
			is_124_[1] = is[3][4];
			is_124_[2] = is[2][2];
			is_124_[3] = is[1][4];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(29, 3 - i_131_, is_124_, 16, 48));
			is_124_[0] = is[2][2];
			is_124_[1] = is[1][4];
			is_124_[2] = is[0][2];
			is_124_[3] = is[3][4];
			if (Math.abs(is_124_[0]) < 300 && Math.abs(is_124_[1]) < 300
					&& is_124_[2] > 700 && is_124_[3] > 700)
				treeset.add(new a(29, 1 - i_131_, is_124_, 16, 48));
		}
		return treeset.iterator();
	}

	private a6[][] a(a var_a) {
		int i;
		int i_132_;
		if (var_a._if < 9)
			i = i_132_ = 1;
		else if (var_a._if < 15)
			i = i_132_ = 2;
		else if (var_a._if < 21)
			i = i_132_ = 4;
		else if (var_a._if < 24)
			i = i_132_ = 6;
		else {
			i = 2;
			i_132_ = 1;
			if (var_a._if == 24 || var_a._if == 26)
				i = 1;
		}
		int i_133_;
		int i_134_;
		switch (var_a._if) {
		case 24:
			i_133_ = 8;
			i_134_ = 18;
			break;
		case 25:
			i_133_ = 8;
			i_134_ = 32;
			break;
		case 26:
			i_133_ = 12;
			i_134_ = 26;
			break;
		case 27:
			i_133_ = 12;
			i_134_ = 36;
			break;
		case 28:
			i_133_ = 16;
			i_134_ = 36;
			break;
		case 29:
			i_133_ = 16;
			i_134_ = 48;
			break;
		default:
			i_134_ = i_133_ = ah.d[var_a._if];
		}
		double[] ds = new double[4];
		double[] ds_135_ = new double[4];
		for (int i_136_ = 0; i_136_ < 4; i_136_++) {
			int i_137_ = var_a.a + i_136_;
			if (i_137_ > 3)
				i_137_ -= 4;
			ds[i_136_] = (double) this.i.xpoints[i_137_];
			ds_135_[i_136_] = (double) this.i.ypoints[i_137_];
		}
		double d = ((ds[3] - ds[0]) / (double) (2 * i_134_) - (ds[3] - ds[2])
				/ (double) (2 * i_133_));
		double d_138_ = ((ds_135_[1] - ds_135_[0]) / (double) (2 * i_133_) - (ds_135_[1] - ds_135_[2])
				/ (double) (2 * i_134_));
		a6[][] var_a6s = new a6[i][];
		for (int i_139_ = 0; i_139_ < var_a6s.length; i_139_++)
			var_a6s[i_139_] = new a6[i_132_];
		Dimension dimension = new Dimension(i, i_132_);
		PointDouble[][] var_ays = new PointDouble[i + 1][];
		for (int i_140_ = 0; i_140_ < i + 1; i_140_++)
			var_ays[i_140_] = new PointDouble[i_132_ + 1];
		for (int i_141_ = 0; i_141_ <= i_132_; i_141_++) {
			double d_142_ = ds[0] + (ds[1] - ds[0]) * (double) i_141_
					/ (double) i_132_;
			double d_143_ = ds_135_[0]
					+ ((ds_135_[1] - ds_135_[0]) * (double) i_141_ / (double) i_132_);
			double d_144_ = ds[3] + (ds[2] - ds[3]) * (double) i_141_
					/ (double) i_132_;
			double d_145_ = ds_135_[3]
					+ ((ds_135_[2] - ds_135_[3]) * (double) i_141_ / (double) i_132_);
			for (int i_146_ = 0; i_146_ <= i; i_146_++) {
				double d_147_ = ds[0] + (ds[3] - ds[0]) * (double) i_146_
						/ (double) i;
				double d_148_ = ds_135_[0]
						+ ((ds_135_[3] - ds_135_[0]) * (double) i_146_ / (double) i);
				double d_149_ = ds[1] + (ds[2] - ds[1]) * (double) i_146_
						/ (double) i;
				double d_150_ = ds_135_[1]
						+ ((ds_135_[2] - ds_135_[1]) * (double) i_146_ / (double) i);
				var_ays[i_146_][i_141_] = a(d_142_, d_143_, d_144_, d_145_,
						d_147_, d_148_, d_149_, d_150_);
				var_ays[i_146_][i_141_].x += d;
				var_ays[i_146_][i_141_].y += d_138_;
			}
		}
		for (int i_151_ = 0; i_151_ < i_132_; i_151_++) {
			for (int i_152_ = 0; i_152_ < i; i_152_++) {
				PointDouble[] var_ays_153_ = new PointDouble[4];
				var_ays_153_[0] = var_ays[i_152_][i_151_];
				var_ays_153_[1] = var_ays[i_152_][i_151_ + 1];
				var_ays_153_[2] = var_ays[i_152_ + 1][i_151_ + 1];
				var_ays_153_[3] = var_ays[i_152_ + 1][i_151_];
				var_a6s[i_152_][i_151_] = new a6(this, g, m, var_ays_153_,
						dimension, i_152_, i_151_);
			}
		}
		for (int i_154_ = 0; i_154_ < i_132_; i_154_++) {
			for (int i_155_ = 0; i_155_ < i; i_155_++) {
				/* empty */
			}
		}
		return var_a6s;
	}

	private af a(int[][] is) {
		int i = is[0].length;
		int i_156_ = is.length;
		int i_157_ = 0;
		int i_158_ = 0;
		int i_159_ = 0;
		for (int i_160_ = 0; i_160_ < i_156_; i_160_++) {
			for (int i_161_ = 0; i_161_ < i; i_161_++) {
				if (is[i_160_][i_161_] > i_157_)
					i_157_ = is[i_159_ = i_160_][i_158_ = i_161_];
			}
		}
		return new af(i_158_, i_159_);
	}

	private PointDouble a(double d, double d_162_, double d_163_, double d_164_,
			double d_165_, double d_166_, double d_167_, double d_168_) {
		double d_169_ = ((d_163_ - d) * (d_165_ * d_168_ - d_167_ * d_166_) - (d_167_ - d_165_)
				* (d * d_164_ - d_163_ * d_162_));
		double d_170_ = ((d_166_ - d_168_) * (d * d_164_ - d_163_ * d_162_) - (d_162_ - d_164_)
				* (d_165_ * d_168_ - d_167_ * d_166_));
		double d_171_ = ((d - d_163_) * (d_166_ - d_168_) - (d_162_ - d_164_)
				* (d_165_ - d_167_));
		d_169_ /= d_171_;
		d_170_ /= d_171_;
		return new PointDouble(d_169_, d_170_);
	}
}
