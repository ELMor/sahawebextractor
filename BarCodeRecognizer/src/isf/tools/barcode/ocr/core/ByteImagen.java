/* v - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

final class ByteImagen extends Imagen {
	private final byte[] l;

	ByteImagen(BufferedImage bufferedimage) {
		super(bufferedimage);
		java.awt.image.DataBuffer databuffer = bufferedimage.getRaster()
				.getDataBuffer();
		l = ((DataBufferByte) databuffer).getData();
	}

	public final int getPixel(int i, int i_0_) {
		int i_1_ = l[g + i_0_ * slStride + i];
		if (i_1_ < 0)
			i_1_ += 256;
		return i_1_;
	}

	public int[] getRow(int i) {
		int i_2_ = this.getWidth();
		int i_3_ = g + i * slStride;
		for (int i_4_ = 0; i_4_ < i_2_; i_4_++) {
			int i_5_ = l[i_3_++];
			if (i_5_ < 0)
				i_5_ += 256;
			xDim[i_4_] = i_5_;
		}
		return xDim;
	}

	public int[] getCol(int i) {
		int i_6_ = this.getHeight();
		int i_7_ = g + i;
		for (int i_8_ = 0; i_8_ < i_6_; i_8_++) {
			int i_9_ = l[i_7_];
			if (i_9_ < 0)
				i_9_ += 256;
			this.yDim[i_8_] = i_9_;
			i_7_ += slStride;
		}
		return this.yDim;
	}
}
