/* a6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Dimension;
import java.awt.Rectangle;

class a6 {
	private static final boolean _else = false;

	private static final int[][] b = { { 8, 10, 12, 14, 16, 18, 20, 22, 24 },
			{ 14, 16, 18, 20, 22, 24 }, null, { 14, 16, 18, 20, 22, 24 }, null,
			{ 18, 20, 22 } };

	private static final int[][] _char = { { 6, 16 }, { 10, 24 } };

	private static final int[][] _void = { { 6, 14 }, { 10, 16 }, { 14, 16 },
			{ 14, 22 } };

	private ah _try;

	private Rectangle _goto;

	private Recognizer _int;

	private int _long;

	PointDouble[] _if;

	private Dimension _case;

	private int c;

	private int _for;

	private boolean _do;

	private af[] _byte = null;

	private af[] a = null;

	private PropertiesHolder _new;

	a6(ah var_ah, Recognizer var_a1, int i, PointDouble[] var_ays, Dimension dimension,
			int i_0_, int i_1_) {
		_try = var_ah;
		_goto = var_ah.b;
		_int = var_a1;
		_long = i;
		_if = new PointDouble[var_ays.length];
		for (int i_2_ = 0; i_2_ < var_ays.length; i_2_++)
			_if[i_2_] = new PointDouble(var_ays[i_2_].x, var_ays[i_2_].y);
		_case = dimension;
		c = i_0_;
		_for = i_1_;
		_do = var_ah.v;
		_new = var_ah.r;
	}

	void a() {
		_goto = null;
		_if = null;
	}

	private af a(int i, int i_3_, int i_4_, int i_5_, boolean bool) {
		double d = (_if[0].x + (_if[1].x - _if[0].x) * (double) (i + 1)
				/ (double) (i_5_ + 2));
		double d_6_ = _if[0].y
				+ ((_if[1].y - _if[0].y) * (double) (i + 1) / (double) (i_5_ + 2));
		double d_7_ = (_if[3].x + (_if[2].x - _if[3].x) * (double) (i + 1)
				/ (double) (i_5_ + 2));
		double d_8_ = _if[3].y
				+ ((_if[2].y - _if[3].y) * (double) (i + 1) / (double) (i_5_ + 2));
		double d_9_ = _if[0].x
				+ ((_if[3].x - _if[0].x) * (double) (i_3_ + 1) / (double) (i_4_ + 2));
		double d_10_ = _if[0].y
				+ ((_if[3].y - _if[0].y) * (double) (i_3_ + 1) / (double) (i_4_ + 2));
		double d_11_ = _if[1].x
				+ ((_if[2].x - _if[1].x) * (double) (i_3_ + 1) / (double) (i_4_ + 2));
		double d_12_ = _if[1].y
				+ ((_if[2].y - _if[1].y) * (double) (i_3_ + 1) / (double) (i_4_ + 2));
		if (bool) {
			if (_byte != null) {
				d_11_ = (double) _byte[i_3_].a;
				d_12_ = (double) _byte[i_3_]._if;
			}
			if (_for > 0) {
				a6 var_a6_13_ = _try.q[c][_for - 1];
				if (var_a6_13_._byte != null) {
					d_9_ = (double) var_a6_13_._byte[i_3_].a;
					d_10_ = (double) var_a6_13_._byte[i_3_]._if;
				}
			}
			if (a != null) {
				d_7_ = (double) a[i].a;
				d_8_ = (double) a[i]._if;
			}
			if (c > 0) {
				a6 var_a6_14_ = _try.q[c - 1][_for];
				if (var_a6_14_.a != null) {
					d = (double) var_a6_14_.a[i].a;
					d_6_ = (double) var_a6_14_.a[i]._if;
				}
			}
		}
		PointDouble var_ay = a(d, d_6_, d_7_, d_8_, d_9_, d_10_, d_11_, d_12_);
		int i_15_ = (int) (var_ay.x + 0.5) - _goto.x;
		int i_16_ = (int) (var_ay.y + 0.5) - _goto.y;
		return new af(i_15_, i_16_);
	}

	int _if(int i, int i_17_, int i_18_, int i_19_, boolean bool) {
		af var_af = a(i, i_17_, i_18_, i_19_, bool);
		int i_20_ = var_af.a;
		int i_21_ = var_af._if;
		if (i_20_ < 0 || i_20_ >= _goto.width || i_21_ < 0
				|| i_21_ >= _goto.height)
			return 0;
		boolean bool_22_ = _int.a(_goto.x + i_20_, _goto.y + i_21_) < _long;
		return bool_22_ ? 1 : 0;
	}

	private PointDouble a(double d, double d_23_, double d_24_, double d_25_,
			double d_26_, double d_27_, double d_28_, double d_29_) {
		double d_30_ = ((d_24_ - d) * (d_26_ * d_29_ - d_28_ * d_27_) - (d_28_ - d_26_)
				* (d * d_25_ - d_24_ * d_23_));
		double d_31_ = ((d_27_ - d_29_) * (d * d_25_ - d_24_ * d_23_) - (d_23_ - d_25_)
				* (d_26_ * d_29_ - d_28_ * d_27_));
		double d_32_ = ((d - d_24_) * (d_27_ - d_29_) - (d_23_ - d_25_)
				* (d_26_ - d_28_));
		d_30_ /= d_32_;
		d_31_ /= d_32_;
		return new PointDouble(d_30_, d_31_);
	}

	int a(int i, int i_33_) {
		return _if(i, i_33_, 0);
	}

	int _if(int i, int i_34_, int i_35_) {
		double d = _if[0].x;
		double d_36_ = _if[0].y;
		double d_37_ = _if[1].x;
		double d_38_ = _if[1].y;
		double d_39_ = _if[2].x;
		double d_40_ = _if[2].y;
		double d_41_ = _if[3].x;
		double d_42_ = _if[3].y;
		double d_43_ = d_37_ + (d_41_ - d_37_) / (double) (i + 2);
		double d_44_ = d_38_ + (d_42_ - d_38_) / (double) (i_34_ + 2);
		double d_45_ = d_39_ - (d_39_ - d) / (double) (i + 2);
		double d_46_ = d_40_ - (d_40_ - d_36_) / (double) (i_34_ + 2);
		int i_47_ = 0;
		for (int i_48_ = 0; i_48_ <= i; i_48_++) {
			double d_49_ = d_43_ + (d_45_ - d_43_) * (double) i_48_
					/ (double) i;
			double d_50_ = d_44_ + (d_46_ - d_44_) * (double) i_48_
					/ (double) i;
			int i_51_ = (int) (d_49_ + 0.5) - _goto.x;
			int i_52_ = (int) (d_50_ + 0.5) - _goto.y;
			if (i_51_ >= 0 && i_51_ < _goto.width && i_52_ >= 0
					&& i_52_ < _goto.height) {
				boolean bool = _int.a(_goto.x + i_51_, _goto.y + i_52_) < _long;
				if (i_48_ % 2 == 0 && !bool)
					i_47_++;
				else if (i_48_ % 2 != 0 && bool == true)
					i_47_++;
			}
		}
		d_43_ = d_45_;
		d_44_ = d_46_;
		d_45_ = d_41_ - (d_41_ - d_37_) / (double) (i + 2);
		d_46_ = d_42_ - (d_42_ - d_38_) / (double) (i_34_ + 2);
		for (int i_53_ = 0; i_53_ <= i_34_; i_53_++) {
			double d_54_ = d_43_ + (d_45_ - d_43_) * (double) i_53_
					/ (double) i_34_;
			double d_55_ = d_44_ + (d_46_ - d_44_) * (double) i_53_
					/ (double) i_34_;
			int i_56_ = (int) (d_54_ + 0.5) - _goto.x;
			int i_57_ = (int) (d_55_ + 0.5) - _goto.y;
			if (i_56_ >= 0 && i_56_ < _goto.width && i_57_ >= 0
					&& i_57_ < _goto.height) {
				boolean bool = _int.a(_goto.x + i_56_, _goto.y + i_57_) < _long;
				if (i_53_ % 2 == 0 && !bool)
					i_47_++;
				else if (i_53_ % 2 != 0 && bool == true)
					i_47_++;
			}
		}
		i_47_ *= 100;
		i_47_ /= i + i_34_;
		return i_47_;
	}

	int a(int i, int i_58_, int i_59_, int i_60_) {
		double d = _if[0].x;
		double d_61_ = _if[0].y;
		double d_62_ = _if[1].x;
		double d_63_ = _if[1].y;
		double d_64_ = _if[2].x;
		double d_65_ = _if[2].y;
		double d_66_ = _if[3].x;
		double d_67_ = _if[3].y;
		double d_68_ = d_62_ + (d_66_ - d_62_) / (double) (i_58_ + 2);
		double d_69_ = d_63_ + (d_67_ - d_63_) / (double) (i_59_ + 2);
		double d_70_ = d_64_ - (d_64_ - d) / (double) (i_58_ + 2);
		double d_71_ = d_65_ - (d_65_ - d_61_) / (double) (i_59_ + 2);
		int i_72_ = 0;
		if (i == 1) {
			for (int i_73_ = 0; i_73_ <= i_58_ / 2; i_73_++) {
				double d_74_ = (d_68_ + (d_70_ - d_68_) * (double) i_73_
						/ (double) i_58_);
				double d_75_ = (d_69_ + (d_71_ - d_69_) * (double) i_73_
						/ (double) i_58_);
				int i_76_ = (int) (d_74_ + 0.5) - _goto.x;
				int i_77_ = (int) (d_75_ + 0.5) - _goto.y;
				if (i_76_ >= 0 && i_76_ < _goto.width && i_77_ >= 0
						&& i_77_ < _goto.height) {
					boolean bool = _int.a(_goto.x + i_76_, _goto.y + i_77_) < _long;
					if (i_73_ % 2 == 0 && !bool)
						i_72_++;
					else if (i_73_ % 2 != 0 && bool == true)
						i_72_++;
				}
			}
		} else if (i == 2) {
			for (int i_78_ = i_58_ / 2; i_78_ <= i_58_; i_78_++) {
				double d_79_ = (d_68_ + (d_70_ - d_68_) * (double) i_78_
						/ (double) i_58_);
				double d_80_ = (d_69_ + (d_71_ - d_69_) * (double) i_78_
						/ (double) i_58_);
				int i_81_ = (int) (d_79_ + 0.5) - _goto.x;
				int i_82_ = (int) (d_80_ + 0.5) - _goto.y;
				if (i_81_ >= 0 && i_81_ < _goto.width && i_82_ >= 0
						&& i_82_ < _goto.height) {
					boolean bool = _int.a(_goto.x + i_81_, _goto.y + i_82_) < _long;
					if (i_78_ % 2 == 0 && !bool)
						i_72_++;
					else if (i_78_ % 2 != 0 && bool == true)
						i_72_++;
				}
			}
			d_68_ = d_70_;
			d_69_ = d_71_;
			d_70_ = d_66_ - (d_66_ - d_62_) / (double) (i_58_ + 2);
			d_71_ = d_67_ - (d_67_ - d_63_) / (double) (i_59_ + 2);
			for (int i_83_ = i_59_ / 2; i_83_ <= i_59_; i_83_++) {
				double d_84_ = (d_68_ + (d_70_ - d_68_) * (double) i_83_
						/ (double) i_59_);
				double d_85_ = (d_69_ + (d_71_ - d_69_) * (double) i_83_
						/ (double) i_59_);
				int i_86_ = (int) (d_84_ + 0.5) - _goto.x;
				int i_87_ = (int) (d_85_ + 0.5) - _goto.y;
				if (i_86_ >= 0 && i_86_ < _goto.width && i_87_ >= 0
						&& i_87_ < _goto.height) {
					boolean bool = _int.a(_goto.x + i_86_, _goto.y + i_87_) < _long;
					if (i_83_ % 2 == 0 && !bool)
						i_72_++;
					else if (i_83_ % 2 != 0 && bool == true)
						i_72_++;
				}
			}
		} else if (i == 3) {
			d_68_ = d_70_;
			d_69_ = d_71_;
			d_70_ = d_66_ - (d_66_ - d_62_) / (double) (i_58_ + 2);
			d_71_ = d_67_ - (d_67_ - d_63_) / (double) (i_59_ + 2);
			for (int i_88_ = 0; i_88_ <= i_59_ / 2; i_88_++) {
				double d_89_ = (d_68_ + (d_70_ - d_68_) * (double) i_88_
						/ (double) i_59_);
				double d_90_ = (d_69_ + (d_71_ - d_69_) * (double) i_88_
						/ (double) i_59_);
				int i_91_ = (int) (d_89_ + 0.5) - _goto.x;
				int i_92_ = (int) (d_90_ + 0.5) - _goto.y;
				if (i_91_ >= 0 && i_91_ < _goto.width && i_92_ >= 0
						&& i_92_ < _goto.height) {
					boolean bool = _int.a(_goto.x + i_91_, _goto.y + i_92_) < _long;
					if (i_88_ % 2 == 0 && !bool)
						i_72_++;
					else if (i_88_ % 2 != 0 && bool == true)
						i_72_++;
				}
			}
		}
		return i_72_;
	}

	void a(int i, int i_93_, int i_94_) {
		int i_95_ = a(i, i_93_);
		PointDouble[] var_ays = _if;
		double d = var_ays[2].x;
		double d_96_ = var_ays[2].y;
		double d_97_ = 0.0;
		double d_98_ = 0.0;
		if (c == 0) {
			d_97_ = var_ays[1].x;
			d_98_ = var_ays[1].y;
		}
		double d_99_ = 0.0;
		double d_100_ = 0.0;
		if (_for == 0) {
			d_99_ = var_ays[3].x;
			d_100_ = var_ays[3].y;
		}
		int i_101_ = -2147483648;
		int i_102_ = -1;
		int i_103_ = -1;
		int i_104_ = -1;
		int i_105_ = -1;
		for (int i_106_ = -i_94_; i_106_ <= i_94_; i_106_++) {
			for (int i_107_ = -i_94_; i_107_ <= i_94_; i_107_++) {
				var_ays[2].x = d + (double) i_107_;
				var_ays[2].y = d_96_ + (double) i_106_;
				if (c == 0 && _for < _case.height - 1) {
					var_ays[1].x = d_97_ + (double) i_107_;
					var_ays[1].y = d_98_ + (double) i_106_;
				}
				if (_for == 0 && c < _case.width - 1) {
					var_ays[3].x = d_99_ + (double) i_107_;
					var_ays[3].y = d_100_ + (double) i_106_;
				}
				int i_108_ = i_107_ == 0 && i_106_ == 0 ? 1 : 0;
				i_95_ = _if(i, i_93_, i_108_);
				if (i_95_ > i_101_) {
					i_101_ = i_95_;
					i_102_ = i_103_ = i_107_;
					i_104_ = i_105_ = i_106_;
				} else if (i_95_ == i_101_) {
					if (i_107_ < i_102_)
						i_102_ = i_107_;
					else if (i_107_ > i_103_)
						i_103_ = i_107_;
					if (i_106_ < i_104_)
						i_104_ = i_106_;
					else if (i_106_ > i_105_)
						i_105_ = i_106_;
				}
			}
		}
		int i_109_ = (i_102_ + i_103_) / 2;
		int i_110_ = (i_104_ + i_105_) / 2;
		var_ays[2].x = d + (double) i_109_;
		var_ays[2].y = d_96_ + (double) i_110_;
		if (c == 0 && _for < _case.height - 1) {
			var_ays[1].x = d_97_ + (double) i_109_;
			var_ays[1].y = d_98_ + (double) i_110_;
		}
		if (_for == 0 && _for < _case.width - 1) {
			var_ays[3].x = d_99_ + (double) i_109_;
			var_ays[3].y = d_100_ + (double) i_110_;
		}
	}

	void a(boolean bool, int i, int i_111_) {
		af var_af;
		af var_af_112_;
		if (bool) {
			var_af = a(-1, i, i, i_111_, false);
			var_af_112_ = a(i_111_, i, i, i_111_, false);
		} else {
			var_af = a(i_111_, -1, i, i_111_, false);
			var_af_112_ = a(i_111_, i, i, i_111_, false);
		}
		int i_113_ = var_af.a;
		int i_114_ = var_af._if;
		int i_115_ = var_af_112_.a;
		int i_116_ = var_af_112_._if;
		int i_117_ = Math.abs(i_115_ - i_113_);
		int i_118_ = Math.abs(i_116_ - i_114_);
		boolean bool_119_ = i_117_ > i_118_;
		int i_120_;
		int i_121_;
		if (bool_119_) {
			i_120_ = i_117_;
			i_121_ = i_118_;
		} else {
			i_120_ = i_118_;
			i_121_ = i_117_;
		}
		int i_122_ = i_120_ / 2;
		int i_123_ = Math.max(i_117_, i_118_) + 1;
		int[] is = new int[i_123_];
		af[] var_afs = new af[i_123_];
		int i_124_ = i_113_;
		int i_125_ = i_114_;
		for (int i_126_ = 0; i_126_ < i_123_; i_126_++) {
			var_afs[i_126_] = new af(i_124_, i_125_);
			if (i_124_ < 0 || i_124_ >= _goto.width || i_125_ < 0
					|| i_125_ >= _goto.height)
				is[i_126_] = 1;
			else
				is[i_126_] = _int.a(_goto.x + i_124_, _goto.y + i_125_) < _long ? 0
						: 1;
			i_122_ -= i_121_;
			boolean bool_127_ = false;
			if (i_122_ < 0) {
				bool_127_ = true;
				i_122_ += i_120_;
			}
			if (bool_119_) {
				if (i_124_ == i_115_)
					break;
				if (i_115_ > i_113_)
					i_124_++;
				else
					i_124_--;
				if (bool_127_) {
					if (i_116_ > i_114_)
						i_125_++;
					else
						i_125_--;
				}
			} else {
				if (i_125_ == i_116_)
					break;
				if (i_116_ > i_114_)
					i_125_++;
				else
					i_125_--;
				if (bool_127_) {
					if (i_115_ > i_113_)
						i_124_++;
					else
						i_124_--;
				}
			}
		}
		int i_128_ = 1;
		int i_129_ = is[0];
		int i_130_ = 0;
		int[] is_131_ = new int[Math.max(i_111_, i) + 3];
		for (int i_132_ = 1; i_132_ < i_123_; i_132_++) {
			if (is[i_132_] != i_129_) {
				i_128_++;
				i_129_ = is[i_132_];
				int i_133_ = (i_130_ + i_132_) / 2;
				if (i_128_ < is_131_.length)
					is_131_[i_128_] = i_133_;
				i_130_ = i_132_;
			}
		}
		if (bool == true && i_128_ == i_111_ + 2 && is[0] == 0) {
			a = new af[i_111_];
			for (int i_134_ = 0; i_134_ < i_111_; i_134_++)
				a[i_134_] = new af(var_afs[is_131_[i_134_ + 3]].a + _goto.x,
						var_afs[is_131_[i_134_ + 3]]._if + _goto.y);
		} else if (!bool && i_128_ == i + 2 && is[0] == 0) {
			_byte = new af[i];
			for (int i_135_ = 0; i_135_ < i; i_135_++)
				_byte[i_135_] = new af(
						var_afs[is_131_[i_135_ + 3]].a + _goto.x,
						var_afs[is_131_[i_135_ + 3]]._if + _goto.y);
		}
	}

	public String toString() {
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 0; i < _if.length; i++)
			stringbuffer.append("[" + i + "] x=" + _if[i].x + " y="
					+ _if[i].y);
		return new String(stringbuffer);
	}
}
