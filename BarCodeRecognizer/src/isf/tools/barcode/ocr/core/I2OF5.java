/* am - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class I2OF5 extends Barcode {
	private static final boolean aJ = false;

	private static final String[] aE = { "00110", "10001", "01001", "11000",
			"00101", "10100", "01100", "00011", "10010", "01010" };

	private static HashMap aH = null;

	private ArrayList aD;

	private boolean aK;

	private int aI;

	private char[] aG = new char[14];

	private char[] aM = new char[5];

	private char[] aL = new char[5];

	private int[] aF = new int[14];

	I2OF5(PropertiesHolder var_c, boolean bool, int i) {
		super(var_c);
		aD = new ArrayList();
		aK = bool;
		aI = i;
	}

	int code() {
		return 8;
	}

	int _if(int i) {
		if (i >= 17 && i % 10 == 7)
			return (i - 27) / 5;
		return -1;
	}

	boolean isCheck() {
		return aK;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 14; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		int i_8_ = 2 * aI;
		while (i_4_ + 13 < i_1_) {
			int i_9_ = i_5_ - is[i_4_ + 13];
			if (i_4_ + 14 < i_1_ && (i_4_ == 1 || is[i_4_ - 1] > i_5_ / i_8_)
					&& is[i_4_ + 14] < i_5_ / 2) {
				int i_10_ = _if(is, i_4_, 14, 18, i_5_, 256, 511);
				if (i_10_ != -1) {
					i_10_ -= 256;
					i_10_ /= 16;
					if (i_10_ < 10) {
						this.readData(i, i_0_, is, i_4_, 14, i_2_, i_10_, true, i_3_);
						if (i_5_ < i_7_)
							i_7_ = i_5_;
					}
				}
			} else if (is[i_4_ - 1] < i_9_ / 2
					&& (i_4_ + 13 == i_1_ - 1 || is[i_4_ + 13] > i_9_ / i_8_)) {
				int i_11_ = _if(is, i_4_, 13, 18, i_9_, 512, 767);
				if (i_11_ != -1) {
					i_11_ -= 512;
					i_11_ %= 16;
					if (i_11_ < 10) {
						this.readData(i, i_0_, is, i_4_, 13, i_2_, i_11_, false, i_3_);
						if (i_5_ < i_7_)
							i_7_ = i_5_;
					}
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 12] + is[i_4_ + 13];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_12_) {
		int i_13_ = 0;
		for (;;) {
			if (i_13_ == 0) {
				int i_14_ = _if(is, i_13_, 14, 18, -1, 256, 511);
				i_13_ += 14;
				if (i_14_ < 256 || i_14_ >= 512)
					break;
				aD.clear();
				i_14_ -= 256;
				int i_15_ = i_14_ / 16;
				int i_16_ = i_14_ % 16;
				if (i_15_ == 10)
					i_15_ = 65535;
				if (i_16_ == 10)
					i_16_ = 65535;
				aD.add(new Integer(i_15_));
				aD.add(new Integer(i_16_));
			} else {
				if (i_13_ + 12 == i - 1) {
					int i_17_ = _if(is, i_13_, 13, 18, -1, 512, 767);
					if (i_17_ >= 512 && i_17_ < 768) {
						i_17_ -= 512;
						int i_18_ = i_17_ / 16;
						int i_19_ = i_17_ % 16;
						if (i_18_ == 10)
							i_18_ = 65535;
						if (i_19_ == 10)
							i_19_ = 65535;
						aD.add(new Integer(i_18_));
						aD.add(new Integer(i_19_));
						int i_20_ = aK ? aD.size() - 1 : -1;
						this.a(aD, i_20_, var_d, var_d_12_);
						return true;
					}
					break;
				}
				if (i_13_ + 12 > i - 1)
					break;
				int i_21_ = _if(is, i_13_, 10, 14, -1, 0, 255);
				i_13_ += 10;
				if (i_21_ < 0 || i_21_ >= 256)
					break;
				int i_22_ = i_21_ / 16;
				int i_23_ = i_21_ % 16;
				if (i_22_ == 10)
					i_22_ = 65535;
				if (i_23_ == 10)
					i_23_ = 65535;
				aD.add(new Integer(i_22_));
				aD.add(new Integer(i_23_));
			}
		}
		return false;
	}

	private boolean _int(String string) {
		int i = 0;
		int i_24_ = 0;
		int i_25_ = string.charAt(string.length() - 1);
		for (int i_26_ = string.length() - 2; i_26_ >= 0; i_26_ -= 2)
			i += string.charAt(i_26_);
		for (int i_27_ = string.length() - 3; i_27_ >= 0; i_27_ -= 2)
			i_24_ += string.charAt(i_27_);
		return (i * 3 + i_24_ + i_25_) % 10 == 0;
	}

	private final int _if(int[] is, int i, int i_28_, int i_29_, int i_30_,
			int i_31_, int i_32_) {
		if (i_30_ == -1) {
			i_30_ = 0;
			for (int i_33_ = 0; i_33_ < i_28_; i_33_++)
				i_30_ += is[i + i_33_];
		}
		int i_34_ = i_30_ * 3 / (i_29_ * 2);
		for (int i_35_ = 0; i_35_ < i_28_; i_35_++) {
			int i_36_ = is[i + i_35_];
			if (i_36_ < i_34_ / 3)
				return -1;
			if (i_36_ > i_34_ * 4)
				return -1;
		}
		for (int i_37_ = 0; i_37_ < i_28_; i_37_++) {
			aF[i_37_] = is[i + i_37_];
			aG[i_37_] = '0';
		}
		int i_38_ = i_28_ == 13 ? 3 : 2;
		int i_39_ = 0;
		for (int i_40_ = 0; i_40_ < i_38_; i_40_++) {
			int i_41_ = 0;
			i_39_ = 0;
			for (int i_42_ = 0; i_42_ < i_28_; i_42_ += 2) {
				if (aF[i_42_] > i_39_) {
					i_39_ = aF[i_42_];
					i_41_ = i_42_;
				}
			}
			aF[i_41_] = -1;
			aG[i_41_] = '1';
		}
		boolean bool = false;
		for (int i_43_ = 0; i_43_ < i_28_; i_43_ += 2) {
			if (aF[i_43_] == i_39_)
				bool = true;
		}
		for (int i_44_ = 0; i_44_ < 2; i_44_++) {
			int i_45_ = 0;
			i_39_ = 0;
			for (int i_46_ = 1; i_46_ < i_28_; i_46_ += 2) {
				if (aF[i_46_] > i_39_) {
					i_39_ = aF[i_46_];
					i_45_ = i_46_;
				}
			}
			aF[i_45_] = -1;
			aG[i_45_] = '1';
		}
		boolean bool_47_ = false;
		for (int i_48_ = 1; i_48_ < i_28_; i_48_ += 2) {
			if (aF[i_48_] == i_39_)
				bool_47_ = true;
		}
		if (i_28_ == 14) {
			for (int i_49_ = 0; i_49_ < 4; i_49_++) {
				if (aG[i_49_] != '0')
					return -1;
			}
			for (int i_50_ = 4; i_50_ < 14; i_50_++)
				aG[i_50_ - 4] = aG[i_50_];
		} else if (i_28_ == 13
				&& (aG[10] != '1' || aG[11] != '0' || aG[12] != '0'))
			return -1;
		for (int i_51_ = 0; i_51_ < 10; i_51_ += 2) {
			aM[i_51_ / 2] = aG[i_51_];
			aL[i_51_ / 2] = aG[i_51_ + 1];
		}
		String string = new String(aM);
		String string_52_ = new String(aL);
		int i_53_;
		if (bool)
			i_53_ = 10;
		else {
			Object object;
			if ((object = aH.get(string)) == null)
				return -1;
			i_53_ = ((Integer) object).intValue();
		}
		int i_54_;
		if (bool_47_)
			i_54_ = 10;
		else {
			Object object;
			if ((object = aH.get(string_52_)) == null)
				return -1;
			i_54_ = ((Integer) object).intValue();
		}
		int i_55_ = i_53_ * 16 + i_54_;
		if (i_28_ == 14)
			i_55_ += 256;
		else if (i_28_ == 13)
			i_55_ += 512;
		return i_55_;
	}

	String a(String string) {
		if (aK && !_int(string))
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			int i_56_ = c / 16;
			int i_57_ = c % 16;
			stringbuffer.append((char) (48 + c));
		}
		return new String(stringbuffer);
	}

	static {
		aH = new HashMap();
		for (int i = 0; i < aE.length; i++)
			aH.put(aE[i], new Integer(i));
	}
}
