/* aa - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

abstract class Barcode {

	protected PropertiesHolder prop;

	protected ArrayList al01;

	protected ArrayList al02;

	private ArrayList al03;

	Barcode(PropertiesHolder var_c) {
		prop = var_c;
		al01 = new ArrayList();
		al02 = new ArrayList();
		al03 = new ArrayList();
	}

	int maxValueIndex(int[] is) {
		int i = -1;
		int max = -2147483648;
		for (int k = 0; k < is.length; k++) {
			if (is[k] > max) {
				max = is[k];
				i = k;
			}
		}
		return i;
	}

	abstract boolean isCheck();

	abstract int a(int i, int i_2_, int i_3_, int[] is, int i_4_, int i_5_);

	abstract boolean a(int i, int[] is, SepInten var_d, SepInten var_d_6_);

	abstract String a(String string);

	abstract int code();

	abstract int _if(int i);

	abstract double _else();

	abstract double _for();

	protected boolean _char() {
		return false;
	}

	final ArrayList getAL1() {
		return al01;
	}

	final ArrayList getAL2() {
		return al02;
	}

	final ArrayList getAL3() {
		return al03;
	}

	void clear() {
		al03.clear();
	}

	protected final SepInten readData(int x, int y, int[] data, int row, int rows,
			int scanDirection, int i_11_, boolean bool, int i_12_) {
		int height = 0;
		for (int k = 0; k < rows; k++)
			height += data[row + k];
		int cY = 0;
		if (code() != 11 && code() != 12) {
			for (int k = 0; k < row; k++)
				cY += data[k];
		}
		Object object = null;
		Rectangle rectangle;
		if (scanDirection == 360)
			rectangle = new Rectangle(x, y - cY - height + 1, 1, height);
		else if (scanDirection == 90)
			rectangle = new Rectangle(x + cY, y, height, 1);
		else if (scanDirection == 180)
			rectangle = new Rectangle(x, y + cY, 1, height);
		else
			rectangle = new Rectangle(x - cY - height + 1, y, height, 1);
		ArrayList arraylist = bool ? al01 : al02;
		SepInten sep = null;
		boolean matched = false;
		for (int i_18_ = arraylist.size() - 1; i_18_ >= 0; i_18_--) {
			sep = (SepInten) arraylist.get(i_18_);
			if (sep.getDirection() == scanDirection
					&& sep._try() == i_11_
					&& sep.getBWBound() == i_12_
					&& sep._do() > height * 2 / 3
					&& sep._do() < height * 3 / 2
					&& (code() == 11 || code() == 12 || sep.a(rectangle,
							code() == 17))) {
				sep.a(rectangle);
				matched = true;
				break;
			}
		}
		if (!matched) {
			sep = new SepInten(scanDirection, i_11_, rectangle, bool, bool ? _else()
					: _for(), rows, i_12_, prop);
			arraylist.add(sep);
		}
		return sep;
	}

	final void init(int codigo) {
		ArrayList lst = new ArrayList();
		Iterator t1 = al01.iterator();
		while (t1.hasNext()) {
			SepInten s = (SepInten) t1.next();
			if (s._if(codigo) == true)
				lst.add(s);
		}
		al01 = lst;
		lst = new ArrayList();
		t1 = al02.iterator();
		while (t1.hasNext()) {
			SepInten s = (SepInten) t1.next();
			if (s._if(codigo) == true)
				lst.add(s);
		}
		al02 = lst;
	}

	void _do(int i) {
		if (i == 1 || i == 2) {
			Iterator iterator = al01.iterator();
			while (iterator.hasNext())
				((SepInten) iterator.next())._goto();
			iterator = al02.iterator();
			while (iterator.hasNext())
				((SepInten) iterator.next())._goto();
		}
		Iterator iterator = al01.iterator();
		while (iterator.hasNext()) {
			SepInten var_d = (SepInten) iterator.next();
			Iterator iterator_20_ = al02.iterator();
			while (iterator_20_.hasNext()) {
				SepInten var_d_21_ = (SepInten) iterator_20_.next();
				if (var_d.a(var_d_21_, i, prop)) {
					var_d.a(var_d_21_);
					var_d_21_.a(var_d);
				}
			}
		}
		if (code() == 10) {
			iterator = al01.iterator();
			while (iterator.hasNext()) {
				SepInten var_d = (SepInten) iterator.next();
				if (var_d.a() == null)
					var_d.a(var_d);
			}
		}
	}

	void _try() {
		Iterator iterator = al03.iterator();
		while (iterator.hasNext()) {
			Recognized var_bf = (Recognized) iterator.next();
			if (!var_bf.a(this))
				var_bf.setNumberOfProps(0);
		}
		Iterator iterator_22_ = al03.iterator();
		while (iterator_22_.hasNext()) {
			Recognized var_bf = (Recognized) iterator_22_.next();
			if (var_bf.getNumberOfProps() != 0) {
				SepInten var_d = var_bf.getSepIntensity();
				SepInten var_d_23_ = var_bf._new();
				int i = var_bf.getNumberOfProps();
				Iterator iterator_24_ = al03.iterator();
				while (iterator_24_.hasNext()) {
					Recognized var_bf_25_ = (Recognized) iterator_24_.next();
					if (var_bf_25_.getNumberOfProps() != 0 && var_bf != var_bf_25_) {
						SepInten var_d_26_ = var_bf_25_.getSepIntensity();
						SepInten var_d_27_ = var_bf_25_._new();
						int i_28_ = var_bf_25_.getNumberOfProps();
						if (var_d_26_ == var_d && var_d_27_ == var_d_23_) {
							if (i < i_28_)
								var_bf.setNumberOfProps(0);
							else
								var_bf_25_.setNumberOfProps(0);
						}
					}
				}
			}
		}
		ArrayList arraylist = new ArrayList();
		iterator_22_ = al03.iterator();
		while (iterator_22_.hasNext()) {
			Recognized var_bf = (Recognized) iterator_22_.next();
			if (var_bf.getNumberOfProps() != 0)
				arraylist.add(var_bf);
		}
		al03 = arraylist;
	}

	final Recognized a(ArrayList arraylist, int i, SepInten var_d, SepInten var_d_29_) {
		boolean bool = true;
		int i_30_ = arraylist.size();
		StringBuffer stringbuffer = new StringBuffer(i_30_);
		for (int i_31_ = 0; i_31_ < arraylist.size(); i_31_++)
			stringbuffer.append((char) ((Integer) arraylist.get(i_31_))
					.intValue());
		String string = new String(stringbuffer);
		Recognized var_bf = null;
		for (int i_32_ = al03.size() - 1; i_32_ >= 0; i_32_--) {
			var_bf = (Recognized) al03.get(i_32_);
			if (var_bf.getSepIntensity() == var_d && var_bf._new() == var_d_29_
					&& var_bf.existProp(string)) {
				var_bf.appendProp(string);
				bool = false;
				break;
			}
		}
		if (bool)
			al03.add(var_bf = new Recognized(string, i, var_d, var_d_29_, code(), prop));
		return var_bf;
	}

	protected void finalize() throws Throwable {
		super.finalize();
	}

	void a() {
		for (int i = 0; i < al03.size(); i++) {
			Recognized var_bf = (Recognized) al03.get(i);
			if (var_bf != null) {
				for (int i_33_ = i + 1; i_33_ < al03.size(); i_33_++) {
					Recognized var_bf_34_ = (Recognized) al03.get(i_33_);
					if (var_bf_34_ != null
							&& var_bf.getRectangle().intersects(var_bf_34_.getRectangle())
							&& var_bf.a(var_bf_34_)) {
						if (var_bf.getNumberOfProps() > var_bf_34_.getNumberOfProps())
							al03.set(i_33_, null);
						else
							al03.set(i, null);
					}
				}
			}
		}
	}
}
