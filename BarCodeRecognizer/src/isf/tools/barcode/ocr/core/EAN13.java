/* a7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class EAN13 extends BarcodeMix {
	private static final boolean ck = false;

	private static final int ch = 11111;

	private static final int[] cd = { 3211, 2221, 2122, 1411, 1132, 1231, 1114,
			1312, 1213, 3112 };

	private static final int[] cl = { 1113211, 1112221, 1112122, 1111411,
			1111132, 1111231, 1111114, 1111312, 1111213, 1113112 };

	private static final int[] ce = { 3211111, 2221111, 2122111, 1411111,
			1132111, 1231111, 1114111, 1312111, 1213111, 3112111 };

	private static final int[] cc = { 1123, 1222, 2212, 1141, 2311, 1321, 4111,
			2131, 3121, 2113 };

	private static final int[] cb = { 1111123, 1111222, 1112212, 1111141,
			1112311, 1111321, 1114111, 1112131, 1113121, 1112113 };

	private static final String[] cj = { "AAAAAA", "AABABB", "AABBAB",
			"AABBBA", "ABAABB", "ABBAAB", "ABBBAA", "ABABAB", "ABABBA",
			"ABBABA" };

	private static HashMap cg = null;

	private static HashMap ci = null;

	private ArrayList ca = new ArrayList();

	private int cf;

	EAN13(PropertiesHolder var_c, int i) {
		super(cg, var_c);
		cf = i;
	}

	int code() {
		return 6;
	}

	int _if(int i) {
		if (i == 59)
			return 59;
		return -1;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 9.5;
	}

	double _for() {
		return 9.5;
	}

	final int a(int i, int i_0_, int tope, int[] data, int scanDirection, int i_3_) {
		int init;
		if (data[0] == 0)
			init = 3;
		else
			init = 1;
		int acu = data[init];
		for (int k1 = 1; k1 < 7; k1++)
			acu += data[init + k1];
		int retVal = 2147483647;
		while (init + 7 <= tope) {
			int i_8_ = acu * 7 / 10;
			if ((init == 1 || data[init - 1] > i_8_ / cf) && data[init + 7] < i_8_) {
				int i_9_ = this._do(data, init, 7, 10, 20, 39);
				if (i_9_ >= 0) {
					this.readData(i, i_0_, data, init, 7, scanDirection, i_9_, true, i_3_);
					if (acu < retVal)
						retVal = acu;
				}
			} else if (data[init - 1] < i_8_
					&& (init + 7 == tope - 1 || data[init + 7] > i_8_ / cf)) {
				int i_10_ = this._do(data, init, 7, 10, 40, 49);
				if (i_10_ >= 0) {
					this.readData(i, i_0_, data, init, 7, scanDirection, i_10_, false, i_3_);
					if (acu < retVal)
						retVal = acu;
				}
			}
			acu -= data[init] + data[init + 1];
			init += 2;
			acu += data[init + 5] + data[init + 6];
		}
		return retVal;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		int i_12_ = 0;
		int i_13_ = 0;
		while_8_: do {
			for (;;) {
				if (i_13_ == 0) {
					int i_14_ = this._do(is, i_12_, 7, 10, 20, 39);
					if (i_14_ < 20 || i_14_ >= 40)
						break while_8_;
					i_12_ += 7;
					i_13_ = 1;
					ca.clear();
					ca.add(new Integer(i_14_ - 20));
				} else if (i_13_ > 0 && i_13_ < 6) {
					int i_15_ = this._do(is, i_12_, 4, 7, 0, 19);
					if (i_15_ < 0 || i_15_ >= 20)
						break while_8_;
					i_12_ += 4;
					i_13_++;
					ca.add(new Integer(i_15_));
				} else if (i_13_ == 6) {
					int i_16_ = this._do(is, i_12_, 5, 5, 50, 50);
					if (i_16_ != 50)
						break while_8_;
					i_12_ += 5;
					i_13_ = 7;
				} else if (i_13_ > 6 && i_13_ < 12) {
					int i_17_ = this._do(is, i_12_, 4, 7, 0, 9);
					if (i_17_ < 0 || i_17_ >= 10)
						break while_8_;
					i_12_ += 4;
					i_13_++;
					ca.add(new Integer(i_17_));
				} else if (i_13_ == 12)
					break;
			}
			int i_18_ = this._do(is, i_12_, 7, 10, 40, 49);
			if (i_18_ >= 40) {
				ca.add(new Integer(i_18_ - 40));
				StringBuffer stringbuffer = new StringBuffer();
				for (int i_19_ = 0; i_19_ < 6; i_19_++)
					stringbuffer
							.append((((Integer) ca.get(i_19_)).intValue() >= 10) ? "B"
									: "A");
				String string = new String(stringbuffer);
				if (ci.get(string) != null) {
					ca.add(0, ci.get(string));
					this.a(ca, 12, var_d, var_d_11_);
					return true;
				}
			}
		} while (false);
		return false;
	}

	private boolean _case(String string) {
		int i = string.length() - 1;
		int i_20_ = string.charAt(i);
		int i_21_ = 0;
		for (int i_22_ = i - 1; i_22_ >= 0; i_22_ -= 2) {
			int i_23_ = string.charAt(i_22_);
			if (i_23_ >= 10)
				i_23_ -= 10;
			i_21_ += i_23_ * 3;
		}
		for (int i_24_ = i - 2; i_24_ >= 0; i_24_ -= 2) {
			int i_25_ = string.charAt(i_24_);
			if (i_25_ >= 10)
				i_25_ -= 10;
			i_21_ += i_25_;
		}
		if (10 - i_21_ % 10 == i_20_ || i_20_ == 0 && i_21_ % 10 == 0)
			return true;
		return false;
	}

	String a(String string) {
		if (!_case(string))
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			int i_26_ = string.charAt(i);
			if (i_26_ >= 10)
				i_26_ -= 10;
			stringbuffer.append((char) (48 + i_26_));
		}
		return new String(stringbuffer);
	}

	static {
		cg = new HashMap();
		ci = new HashMap();
		for (int i = 0; i < 10; i++)
			cg.put(new Integer(cd[i]), new Integer(i));
		for (int i = 0; i < 10; i++)
			cg.put(new Integer(cc[i]), new Integer(i + 10));
		for (int i = 0; i < 10; i++)
			cg.put(new Integer(cl[i]), new Integer(i + 20));
		for (int i = 0; i < 10; i++)
			cg.put(new Integer(cb[i]), new Integer(i + 30));
		for (int i = 0; i < 10; i++)
			cg.put(new Integer(ce[i]), new Integer(i + 40));
		cg.put(new Integer(11111), new Integer(50));
		for (int i = 0; i < 10; i++)
			ci.put(cj[i], new Integer(i));
	}
}
