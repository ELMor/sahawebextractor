/* f - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.license;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Holder implements HolderBase {
	private static HashMap holder = null;

	private static String name = null;

	private static Integer value = new Integer(0);

	public int check(String string, Object object) throws BaseException {
		int i = 0;
		Integer integer = value;
		name = string;
		holder = l.a(string);
		if (object != null && object instanceof Date) {
			Date date = new Date();
			if (date.after((Date) object))
				BaseException.raise(2, "Expired on ".concat(new SimpleDateFormat("yyyy-MM-dd")
						.format((Date) object)));
		}
		return i;
	}

	public static String getName() {
		return name;
	}

	public synchronized String getValue(String string) {
		if (holder == null)
			return null;
		return (String) holder.get(string);
	}
}
