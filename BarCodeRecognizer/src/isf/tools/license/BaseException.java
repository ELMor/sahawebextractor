/* e - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.license;

import java.io.PrintWriter;
import java.io.Serializable;

public class BaseException extends Exception implements Serializable {
	public static final int c = 0;

	public static final int invalidLicense = 1;

	public static final int _long = 2;

	public static final int _for = 3;

	public static final int _byte = 4;

	public static final int e = 5;

	public static final int _try = 6;

	public static final int _if = 7;

	public static final int _goto = 8;

	public static final int _char = 9;

	public static final int b = 10;

	public static final int _void = 11;

	public static final int _int = 12;

	public static final int _case = 1;

	public static final int _do = 4;

	public static final int _new = 8;

	private String _else;

	private int f;

	public String getMessage() {
		return _else;
	}

	public int a() {
		return f;
	}

	public void a(String string) {
		_else = string;
	}

	public void a(int i) {
		f = i;
	}

	public static void raise(int excCode, String msg) throws BaseException {
		String exception = "";
		switch (excCode) {
		case 1:
			exception = "Invalid license key";
			break;
		case 2:
			exception = "License has expired";
			break;
		case 3:
			exception = "License quota has been exceeded";
			break;
		case 4:
			exception = "Invalid host / user / network name";
			break;
		case 5:
			exception = "Invalid custom key";
			break;
		case 6:
			exception = "Unimplemented license management feature";
			break;
		case 7:
			exception = "Error during key generation";
			break;
		case 8:
			exception = "Vendor key parse error";
			break;
		case 9:
			exception = "Internal exception condition";
			break;
		case 10:
			exception = "Operating system error condition";
			break;
		case 11:
			exception = "Invalid EasyLicenser configuration condition";
			break;
		case 12:
			exception = "You are not licensed to use this option.  Please contact your sales representative to purchase the option and obtain a new license key.";
			break;
		default:
			exception = "Unrecognized exception code: ".concat(String.valueOf(excCode));
			excCode = 9;
		}
		if (msg != null)
			exception = exception.concat(" ").concat(msg);
		a(exception, excCode);
	}

	public static void a(String string, int i) throws BaseException {
		if (EzLicense._int(1))
			new Throwable().printStackTrace();
		BaseException var_e;
		switch (i) {
		case 1:
			var_e = new InvalidLicenseException();
			break;
		case 2:
			var_e = new LicenseExpiredException();
			break;
		case 12:
			var_e = new NotLicensedOptionException();
			break;
		case 3:
			var_e = new LicenseQuotaException();
			break;
		case 4:
			var_e = new InvalidHostUserException();
			break;
		case 5:
			var_e = new InvalidCustomKeyException();
			break;
		case 6:
			var_e = new UnimplementedLicenseManagementException();
			break;
		case 7:
		case 8:
		case 9: {
			var_e = new LicenseInternalException();
			PrintWriter printwriter = new PrintWriter(System.out, true);
			var_e.printStackTrace(printwriter);
			break;
		}
		case 10:
			var_e = new OSException();
			break;
		case 11:
			var_e = new InvalidEasyLicenserException();
			break;
		default:
			try {
				var_e = (BaseException) Class.forName(string).newInstance();
			} catch (Exception exception) {
				var_e = new LicenseInternalException();
				i = 9;
				string = ": Exception: ["
						.concat(exception.getClass().getName()).concat(
								"],  Message: [").concat(string).concat("]");
			}
		}
		var_e.a(string);
		var_e.a(i);
		throw var_e;
	}
}
