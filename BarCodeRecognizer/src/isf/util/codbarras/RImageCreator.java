// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   RImageCreator.java

package isf.util.codbarras;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class RImageCreator
{

    private Image im;
    public Graphics g;

    public RImageCreator()
    {
    }

    public Image getImage(int w, int h)
    {
        int s = w;
        if(h > w)
            s = h;
        im = new BufferedImage(s, s, 13);
        g = ((BufferedImage)im).createGraphics();
        return im;
    }

    public Graphics getGraphics()
    {
        return g;
    }
}
