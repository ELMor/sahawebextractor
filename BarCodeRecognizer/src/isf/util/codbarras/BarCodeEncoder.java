// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   BarCodeEncoder.java

package isf.util.codbarras;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;


public class BarCodeEncoder
{

    String sFile;
    String sFormat;
    BarCode bc;
    public boolean result;

    public BarCodeEncoder(BarCode c, String psFormat, String psFile)
    {
        sFormat = psFormat;
        sFile = psFile;
        bc = c;
        result = encode();
    }

    private boolean encode()
    {
        if(sFormat.toUpperCase().compareTo("GIF") == 0)
            return saveToGIF();
        if(sFormat.toUpperCase().compareTo("JPEG") == 0)
            return saveToJPEG();
        if(sFormat.toUpperCase().compareTo("JPG") == 0)
            return saveToJPEG();
        if(sFormat.toUpperCase().compareTo("PNG") == 0)
            return saveToPNG();
        else
            return false;
    }

    private boolean saveToPNG()
    {
        try
        {
            BufferedImage image = new BufferedImage(bc.getSize().width, bc.getSize().height, 13);
            java.awt.Graphics imgGraphics = image.createGraphics();
            bc.paint(imgGraphics);
            File f = new File(sFile);
            f.delete();
            FileOutputStream of = new FileOutputStream(f);
            Class enClass = Class.forName("com.bigfoot.bugar.image.PNGEncoder");
            Class constructorParams[] = new Class[2];
            constructorParams[0] = Class.forName("java.awt.Image");
            constructorParams[1] = Class.forName("java.io.OutputStream");
            Object constructorObj[] = new Object[2];
            constructorObj[0] = image;
            constructorObj[1] = of;
            Object encoder = enClass.getConstructor(constructorParams).newInstance(constructorObj);
            Class encodeParams[] = new Class[0];
            Object encodeObj[] = new Object[0];
            enClass.getMethod("encodeImage", encodeParams).invoke(encoder, encodeObj);
            of.close();
        }
        catch(Exception e)
        {
            return false;
        }
        return true;
    }

    private boolean saveToGIF()
    {
        try
        {
            BufferedImage image = new BufferedImage(bc.getSize().width, bc.getSize().height, 13);
            java.awt.Graphics imgGraphics = image.createGraphics();
            bc.paint(imgGraphics);
            File f = new File(sFile);
            f.delete();
            FileOutputStream of = new FileOutputStream(f);
            Class enClass = Class.forName("Acme.JPM.Encoders.GifEncoder");
            Class constructorParams[] = new Class[2];
            constructorParams[0] = Class.forName("java.awt.Image");
            constructorParams[1] = Class.forName("java.io.OutputStream");
            Object constructorObj[] = new Object[2];
            constructorObj[0] = image;
            constructorObj[1] = of;
            Object encoder = enClass.getConstructor(constructorParams).newInstance(constructorObj);
            Class encodeParams[] = new Class[0];
            Object encodeObj[] = new Object[0];
            enClass.getMethod("encode", encodeParams).invoke(encoder, encodeObj);
            of.close();
        }
        catch(Exception e)
        {
            return false;
        }
        return true;
    }

    private boolean saveToJPEG()
    {
        try
        {
            BufferedImage image = new BufferedImage(bc.getSize().width, bc.getSize().height, 1);
            java.awt.Graphics imgGraphics = image.createGraphics();
            bc.paint(imgGraphics);
            File f = new File(sFile);
            f.delete();
            FileOutputStream of = new FileOutputStream(f);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(of);
            encoder.encode(image);
            of.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
