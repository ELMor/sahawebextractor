// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   BarCodePDF417Bean.java

package isf.util.codbarras;

import java.awt.Color;


public class BarCodePDF417Bean extends BarCode2D
{

    public int PDFMode;

    public BarCodePDF417Bean()
    {
        PDFMode = 0;
        barType = 30;
    }

    public int getPDFRows()
    {
        return PDFRows;
    }

    public void setPDFRows(int r)
    {
        PDFRows = r;
    }

    public int getPDFMaxRows()
    {
        return PDFMaxRows;
    }

    public void setPDFMaxRows(int r)
    {
        PDFMaxRows = r;
    }

    public int getPDFColumns()
    {
        return PDFColumns;
    }

    public void setPDFColumns(int c)
    {
        PDFColumns = c;
    }

    public int getPDFECLevel()
    {
        return PDFECLevel;
    }

    public void setPDFECLevel(int c)
    {
        PDFECLevel = c;
    }

    public String getPDFMode()
    {
        if(PDFMode == 0)
            return "BINARY";
        if(PDFMode == 1)
            return "TEXT";
        if(PDFMode == 2)
            return "NUMERIC";
        else
            return "TEXT";
    }

    public void setPDFMode(String m)
    {
        m = m.toUpperCase();
        if(m.compareTo("BINARY") == 0)
            PDFMode = 0;
        if(m.compareTo("TEXT") == 0)
            PDFMode = 1;
        if(m.compareTo("NUMERIC") == 0)
            PDFMode = 2;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String c)
    {
        code = c;
        resetMacroPDF();
    }

    public byte[] getCodeBinary()
    {
        return codeBinary;
    }

    public void setCode(byte c[])
    {
        codeBinary = c;
        resetMacroPDF();
    }

    public double getLeftMarginCM()
    {
        return leftMarginCM;
    }

    public void setLeftMarginCM(double d)
    {
        leftMarginCM = d;
    }

    public double getTopMarginCM()
    {
        return topMarginCM;
    }

    public void setTopMarginCM(double d)
    {
        topMarginCM = d;
    }

    public double getBarHeightCM()
    {
        return super.barHeightCM;
    }

    public void setBarHeightCM(double d)
    {
        super.barHeightCM = d;
    }

    public int getResolution()
    {
        return super.resolution;
    }

    public void setResolution(int d)
    {
        super.resolution = d;
    }

    public double getBarWith()
    {
        return super.X;
    }

    public void setBarWith(double d)
    {
        super.X = d;
    }

    public Color getBackColor()
    {
        return backColor;
    }

    public void setBackColor(Color c)
    {
        backColor = c;
    }

    public int getPDFMacroSegmentCount()
    {
        return PDFMacroSegmentCount;
    }

    public void setPDFMacroSegmentCount(int i)
    {
        PDFMacroSegmentCount = i;
    }

    public int getPDFMacroSegment()
    {
        return PDFMacroSegment;
    }

    public void setPDFMacroSegment(int i)
    {
        PDFMacroSegment = i;
    }

    public long getPDFMacroFileSize()
    {
        return PDFMacroFileSize;
    }

    public void setPDFMacroFileSize(long i)
    {
        PDFMacroFileSize = i;
    }

    public String getPDFMacroFileName()
    {
        return PDFMacroFileName;
    }

    public void setPDFMacroFileName(String i)
    {
        PDFMacroFileName = i;
    }

    public String getPDFMacroTimeStamp()
    {
        return PDFMacroTimeStamp;
    }

    public void setPDFMacroTimeStamp(String i)
    {
        PDFMacroTimeStamp = i;
    }

    public String getPDFMacroSender()
    {
        return PDFMacroSender;
    }

    public void setPDFMacroSender(String i)
    {
        PDFMacroSender = i;
    }

    public String getPDFMacroAddresse()
    {
        return PDFMacroAddresse;
    }

    public void setPDFMacroAddresse(String i)
    {
        PDFMacroAddresse = i;
    }

    public long getPDFMacroFileId()
    {
        return (long)(PDFMacroFileId[0] * (PDFMacroFileId[1] * 900) * (PDFMacroFileId[2] * 900 * 900));
    }

    public void setPDFMacroFileId(long i)
    {
        PDFMacroFileId = new int[3];
        PDFMacroFileId[0] = (int)(i % 900L);
        PDFMacroFileId[1] = (int)(Math.floor(i / 900L) % 900D);
        PDFMacroFileId[2] = (int)Math.floor(i % 0xc5c10L);
    }

    public Color getBarColor()
    {
        return barColor;
    }

    public void setBarColor(Color c)
    {
        barColor = c;
    }

    public int getRotate()
    {
        return rotate;
    }

    public void setRotate(int i)
    {
        rotate = i;
    }

    public String getName()
    {
        return "RBarCodePDF417";
    }
}
