// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   RDataMatrix.java

package isf.util.codbarras.matrix;

import isf.util.codbarras.BarCode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.io.PrintStream;


public class RDataMatrix extends BarCode
{
    private class charPlacer
    {

        public int nrow;
        public int ncol;
        public int array[];

        private void utah(int row, int col, int chr)
        {
            module(row - 2, col - 2, chr, 1);
            module(row - 2, col - 1, chr, 2);
            module(row - 1, col - 2, chr, 3);
            module(row - 1, col - 1, chr, 4);
            module(row - 1, col, chr, 5);
            module(row, col - 2, chr, 6);
            module(row, col - 1, chr, 7);
            module(row, col, chr, 8);
        }

        private void corner1(int chr)
        {
            module(nrow - 1, 0, chr, 1);
            module(nrow - 1, 1, chr, 2);
            module(nrow - 1, 2, chr, 3);
            module(0, ncol - 2, chr, 4);
            module(0, ncol - 1, chr, 5);
            module(1, ncol - 1, chr, 6);
            module(2, ncol - 1, chr, 7);
            module(3, ncol - 1, chr, 8);
        }

        private void corner2(int chr)
        {
            module(nrow - 3, 0, chr, 1);
            module(nrow - 2, 0, chr, 2);
            module(nrow - 1, 0, chr, 3);
            module(0, ncol - 4, chr, 4);
            module(0, ncol - 3, chr, 5);
            module(0, ncol - 2, chr, 6);
            module(0, ncol - 1, chr, 7);
            module(1, ncol - 1, chr, 8);
        }

        private void corner3(int chr)
        {
            module(nrow - 3, 0, chr, 1);
            module(nrow - 2, 0, chr, 2);
            module(nrow - 1, 0, chr, 3);
            module(0, ncol - 2, chr, 4);
            module(0, ncol - 1, chr, 5);
            module(1, ncol - 1, chr, 6);
            module(2, ncol - 1, chr, 7);
            module(3, ncol - 1, chr, 8);
        }

        private void corner4(int chr)
        {
            module(nrow - 1, 0, chr, 1);
            module(nrow - 1, ncol - 1, chr, 2);
            module(0, ncol - 3, chr, 3);
            module(0, ncol - 2, chr, 4);
            module(0, ncol - 1, chr, 5);
            module(1, ncol - 3, chr, 6);
            module(1, ncol - 2, chr, 7);
            module(1, ncol - 1, chr, 8);
        }

        public void make()
        {
            int chr = 1;
            int row = 4;
            int col = 0;
            for(int r = 0; r < nrow; r++)
            {
                for(int c = 0; c < ncol; c++)
                    array[r * ncol + c] = 0;

            }

            do
            {
                if(row == nrow && col == 0)
                    corner1(chr++);
                if(row == nrow - 2 && col == 0 && ncol % 4 != 0)
                    corner2(chr++);
                if(row == nrow - 2 && col == 0 && ncol % 8 == 4)
                    corner3(chr++);
                if(row == nrow + 4 && col == 2 && ncol % 8 == 0)
                    corner4(chr++);
                do
                {
                    if(row < nrow && col >= 0 && array[row * ncol + col] == 0)
                        utah(row, col, chr++);
                    row -= 2;
                    col += 2;
                } while(row >= 0 && col < ncol);
                row++;
                col += 3;
                do
                {
                    if(row >= 0 && col < ncol && array[row * ncol + col] == 0)
                        utah(row, col, chr++);
                    row += 2;
                    col -= 2;
                } while(row < nrow && col >= 0);
                row += 3;
                col++;
            } while(row < nrow || col < ncol);
            if(array[nrow * ncol - 1] == 0)
                array[nrow * ncol - 1] = array[nrow * ncol - ncol - 2] = 1;
        }

        private void module(int r, int c, int chr, int bit)
        {
            if(r < 0)
            {
                r += nrow;
                c += 4 - (nrow + 4) % 8;
            }
            if(c < 0)
            {
                c += ncol;
                r += 4 - (ncol + 4) % 8;
            }
            array[r * ncol + c] = 10 * chr + bit;
        }

        private charPlacer()
        {
        }

    }


    protected static final int d1 = 0;
    public int dotPixels;
    public int margin;
    public boolean processTilde;
    public boolean reBuild;
    private String internalCode;
    public byte codeBinary[];
    private int bitmap[][];
    private static final int ROW = 0;
    private static final int COL = 1;
    private static final int ROWDATA = 2;
    private static final int COLDATA = 3;
    private static final int REGIONS = 4;
    private static final int ROWMAP = 5;
    private static final int COLMAP = 6;
    private static final int TOTALDATA = 7;
    private static final int TOTALERR = 8;
    private static final int REEDDATA = 9;
    private static final int REEDERR = 10;
    private static final int REEDBLOCKS = 11;
    public static final int C10X10 = 0;
    public static final int C12X12 = 1;
    public static final int C14X14 = 2;
    public static final int C16X16 = 3;
    public static final int C18X18 = 4;
    public static final int C20X20 = 5;
    public static final int C22X22 = 6;
    public static final int C24X24 = 7;
    public static final int C26X26 = 8;
    public static final int C32X32 = 9;
    public static final int C36X36 = 10;
    public static final int C40X40 = 11;
    public static final int C44X44 = 12;
    public static final int C48X48 = 13;
    public static final int C52X52 = 14;
    public static final int C64X64 = 15;
    public static final int C72X72 = 16;
    public static final int C80X80 = 17;
    public static final int C88X88 = 18;
    public static final int C96X96 = 19;
    public static final int C104X104 = 20;
    public static final int C120X120 = 21;
    public static final int C132X132 = 22;
    public static final int C144X144 = 23;
    public static final int C8X18 = 24;
    public static final int C8X32 = 25;
    public static final int C12X26 = 26;
    public static final int C12X36 = 27;
    public static final int C16X36 = 28;
    public static final int C16X48 = 29;
    private static final int C40_PAD = 0;
    private static final int PAD = 129;
    private static final int LATCH_C40 = 230;
    private static final int LATCH_BASE256 = 231;
    private static final int FNC1 = 232;
    private static final int STRUCTURED_APPEND = 233;
    private static final int READER_PROGRAMMING = 234;
    private static final int SHIFT = 235;
    private static final int MACRO5 = 236;
    private static final int MACRO6 = 237;
    private static final int LATCH_TEXT = 239;
    private static final int ECI = 241;
    private static final int UNLATCH = 254;
    private int currentEncoding;
    private static int configuration[][] = {
        {
            10, 10, 8, 8, 1, 8, 8, 3, 5, 3, 
            5, 1
        }, {
            12, 12, 10, 10, 1, 10, 10, 5, 7, 5, 
            7, 1
        }, {
            14, 14, 12, 12, 1, 12, 12, 8, 10, 8, 
            10, 1
        }, {
            16, 16, 14, 14, 1, 14, 14, 12, 12, 12, 
            12, 1
        }, {
            18, 18, 16, 16, 1, 16, 16, 18, 14, 18, 
            14, 1
        }, {
            20, 20, 18, 18, 1, 18, 18, 22, 18, 22, 
            18, 1
        }, {
            22, 22, 20, 20, 1, 20, 20, 30, 20, 30, 
            20, 1
        }, {
            24, 24, 22, 22, 1, 22, 22, 36, 24, 36, 
            24, 1
        }, {
            26, 26, 24, 24, 1, 24, 24, 44, 28, 44, 
            28, 1
        }, {
            32, 32, 14, 14, 4, 28, 28, 62, 36, 62, 
            36, 1
        }, {
            36, 36, 16, 16, 4, 32, 32, 86, 42, 86, 
            42, 1
        }, {
            40, 40, 18, 18, 4, 36, 36, 114, 48, 114, 
            48, 1
        }, {
            44, 44, 20, 20, 4, 40, 40, 144, 56, 144, 
            56, 1
        }, {
            48, 48, 22, 22, 4, 44, 44, 174, 68, 174, 
            68, 1
        }, {
            52, 52, 24, 24, 4, 48, 48, 204, 84, 102, 
            42, 2
        }, {
            64, 64, 14, 14, 16, 56, 56, 280, 112, 140, 
            56, 2
        }, {
            72, 72, 16, 16, 16, 64, 64, 368, 144, 92, 
            36, 4
        }, {
            80, 80, 18, 18, 16, 72, 72, 456, 192, 114, 
            48, 4
        }, {
            88, 88, 20, 20, 16, 80, 80, 576, 224, 144, 
            56, 4
        }, {
            96, 96, 22, 22, 16, 88, 88, 696, 272, 174, 
            68, 4
        }, {
            104, 104, 24, 24, 16, 96, 96, 816, 336, 136, 
            56, 6
        }, {
            120, 120, 18, 18, 36, 108, 108, 1050, 496, 175, 
            68, 6
        }, {
            132, 132, 20, 20, 36, 120, 120, 1304, 496, 163, 
            62, 8
        }, {
            144, 144, 22, 22, 36, 132, 132, 1558, 620, 156, 
            62, 10
        }, {
            8, 18, 6, 16, 1, 6, 16, 5, 7, 5, 
            7, 1
        }, {
            8, 32, 6, 14, 2, 6, 28, 10, 11, 10, 
            11, 1
        }, {
            12, 26, 10, 24, 1, 10, 24, 16, 14, 16, 
            14, 1
        }, {
            12, 36, 10, 16, 2, 10, 32, 22, 18, 22, 
            18, 1
        }, {
            16, 36, 14, 16, 2, 14, 32, 32, 24, 32, 
            24, 1
        }, {
            16, 48, 14, 22, 2, 14, 44, 49, 28, 49, 
            28, 1
        }
    };
    private int rows;
    private int cols;
    private int datarows;
    private int datacols;
    private int maprows;
    private int mapcols;
    private int regions;
    private int totaldata;
    private int totalerr;
    private int reeddata;
    private int reederr;
    private int reedblocks;
    private int C49rest;
    private int calculatedFormat;
    public int encoding;
    public static int E_ASCII = 0;
    public static int E_C40 = 1;
    public static int E_TEXT = 2;
    public static int E_BASE256 = 3;
    public static int E_NONE = 4;
    public static int E_AUTO = 5;
    public int preferredFormat;
    private static int C40Codes[][] = {
        {
            0, 0
        }, {
            0, 1
        }, {
            0, 2
        }, {
            0, 3
        }, {
            0, 4
        }, {
            0, 5
        }, {
            0, 6
        }, {
            0, 7
        }, {
            0, 8
        }, {
            0, 9
        }, {
            0, 10
        }, {
            0, 11
        }, {
            0, 12
        }, {
            0, 13
        }, {
            0, 14
        }, {
            0, 15
        }, {
            0, 16
        }, {
            0, 17
        }, {
            0, 18
        }, {
            0, 19
        }, {
            0, 20
        }, {
            0, 21
        }, {
            0, 22
        }, {
            0, 23
        }, {
            0, 24
        }, {
            0, 25
        }, {
            0, 26
        }, {
            0, 27
        }, {
            0, 28
        }, {
            0, 29
        }, {
            0, 30
        }, {
            0, 31
        }, {
            3
        }, {
            1, 0
        }, {
            1, 1
        }, {
            1, 2
        }, {
            1, 3
        }, {
            1, 4
        }, {
            1, 5
        }, {
            1, 6
        }, {
            1, 7
        }, {
            1, 8
        }, {
            1, 9
        }, {
            1, 10
        }, {
            1, 11
        }, {
            1, 12
        }, {
            1, 13
        }, {
            1, 14
        }, {
            4
        }, {
            5
        }, {
            6
        }, {
            7
        }, {
            8
        }, {
            9
        }, {
            10
        }, {
            11
        }, {
            12
        }, {
            13
        }, {
            1, 15
        }, {
            1, 16
        }, {
            1, 17
        }, {
            1, 18
        }, {
            1, 19
        }, {
            1, 20
        }, {
            1, 21
        }, {
            14
        }, {
            15
        }, {
            16
        }, {
            17
        }, {
            18
        }, {
            19
        }, {
            20
        }, {
            21
        }, {
            22
        }, {
            23
        }, {
            24
        }, {
            25
        }, {
            26
        }, {
            27
        }, {
            28
        }, {
            29
        }, {
            30
        }, {
            31
        }, {
            32
        }, {
            33
        }, {
            34
        }, {
            35
        }, {
            36
        }, {
            37
        }, {
            38
        }, {
            39
        }, {
            1, 22
        }, {
            1, 23
        }, {
            1, 24
        }, {
            1, 25
        }, {
            1, 26
        }, {
            2, 0
        }, {
            2, 1
        }, {
            2, 2
        }, {
            2, 3
        }, {
            2, 4
        }, {
            2, 5
        }, {
            2, 6
        }, {
            2, 7
        }, {
            2, 8
        }, {
            2, 9
        }, {
            2, 10
        }, {
            2, 11
        }, {
            2, 12
        }, {
            2, 13
        }, {
            2, 14
        }, {
            2, 15
        }, {
            2, 16
        }, {
            2, 17
        }, {
            2, 18
        }, {
            2, 19
        }, {
            2, 20
        }, {
            2, 21
        }, {
            2, 22
        }, {
            2, 23
        }, {
            2, 24
        }, {
            2, 25
        }, {
            2, 26
        }, {
            2, 27
        }, {
            2, 28
        }, {
            2, 29
        }, {
            2, 30
        }, {
            2, 31
        }, {
            1, 30, 0, 0
        }, {
            1, 30, 0, 1
        }, {
            1, 30, 0, 2
        }, {
            1, 30, 0, 3
        }, {
            1, 30, 0, 4
        }, {
            1, 30, 0, 5
        }, {
            1, 30, 0, 6
        }, {
            1, 30, 0, 7
        }, {
            1, 30, 0, 8
        }, {
            1, 30, 0, 9
        }, {
            1, 30, 0, 10
        }, {
            1, 30, 0, 11
        }, {
            1, 30, 0, 12
        }, {
            1, 30, 0, 13
        }, {
            1, 30, 0, 14
        }, {
            1, 30, 0, 15
        }, {
            1, 30, 0, 16
        }, {
            1, 30, 0, 17
        }, {
            1, 30, 0, 18
        }, {
            1, 30, 0, 19
        }, {
            1, 30, 0, 20
        }, {
            1, 30, 0, 21
        }, {
            1, 30, 0, 22
        }, {
            1, 30, 0, 23
        }, {
            1, 30, 0, 24
        }, {
            1, 30, 0, 25
        }, {
            1, 30, 0, 26
        }, {
            1, 30, 0, 27
        }, {
            1, 30, 0, 28
        }, {
            1, 30, 0, 29
        }, {
            1, 30, 0, 30
        }, {
            1, 30, 0, 31
        }, {
            1, 30, 3
        }, {
            1, 30, 1, 0
        }, {
            1, 30, 1, 1
        }, {
            1, 30, 1, 2
        }, {
            1, 30, 1, 3
        }, {
            1, 30, 1, 4
        }, {
            1, 30, 1, 5
        }, {
            1, 30, 1, 6
        }, {
            1, 30, 1, 7
        }, {
            1, 30, 1, 8
        }, {
            1, 30, 1, 9
        }, {
            1, 30, 1, 10
        }, {
            1, 30, 1, 11
        }, {
            1, 30, 1, 12
        }, {
            1, 30, 1, 13
        }, {
            1, 30, 1, 14
        }, {
            1, 30, 4
        }, {
            1, 30, 5
        }, {
            1, 30, 6
        }, {
            1, 30, 7
        }, {
            1, 30, 8
        }, {
            1, 30, 9
        }, {
            1, 30, 10
        }, {
            1, 30, 11
        }, {
            1, 30, 12
        }, {
            1, 30, 13
        }, {
            1, 30, 1, 15
        }, {
            1, 30, 1, 16
        }, {
            1, 30, 1, 17
        }, {
            1, 30, 1, 18
        }, {
            1, 30, 1, 19
        }, {
            1, 30, 1, 20
        }, {
            1, 30, 1, 21
        }, {
            1, 30, 14
        }, {
            1, 30, 15
        }, {
            1, 30, 16
        }, {
            1, 30, 17
        }, {
            1, 30, 18
        }, {
            1, 30, 19
        }, {
            1, 30, 20
        }, {
            1, 30, 21
        }, {
            1, 30, 22
        }, {
            1, 30, 23
        }, {
            1, 30, 24
        }, {
            1, 30, 25
        }, {
            1, 30, 26
        }, {
            1, 30, 27
        }, {
            1, 30, 28
        }, {
            1, 30, 29
        }, {
            1, 30, 30
        }, {
            1, 30, 31
        }, {
            1, 30, 32
        }, {
            1, 30, 33
        }, {
            1, 30, 34
        }, {
            1, 30, 35
        }, {
            1, 30, 36
        }, {
            1, 30, 37
        }, {
            1, 30, 38
        }, {
            1, 30, 39
        }, {
            1, 30, 1, 22
        }, {
            1, 30, 1, 23
        }, {
            1, 30, 1, 24
        }, {
            1, 30, 1, 25
        }, {
            1, 30, 1, 26
        }, {
            1, 30, 2, 0
        }, {
            1, 30, 2, 1
        }, {
            1, 30, 2, 2
        }, {
            1, 30, 2, 3
        }, {
            1, 30, 2, 4
        }, {
            1, 30, 2, 5
        }, {
            1, 30, 2, 6
        }, {
            1, 30, 2, 7
        }, {
            1, 30, 2, 8
        }, {
            1, 30, 2, 9
        }, {
            1, 30, 2, 10
        }, {
            1, 30, 2, 11
        }, {
            1, 30, 2, 12
        }, {
            1, 30, 2, 13
        }, {
            1, 30, 2, 14
        }, {
            1, 30, 2, 15
        }, {
            1, 30, 2, 16
        }, {
            1, 30, 2, 17
        }, {
            1, 30, 2, 18
        }, {
            1, 30, 2, 19
        }, {
            1, 30, 2, 20
        }, {
            1, 30, 2, 21
        }, {
            1, 30, 2, 22
        }, {
            1, 30, 2, 23
        }, {
            1, 30, 2, 24
        }, {
            1, 30, 2, 25
        }, {
            1, 30, 2, 26
        }, {
            1, 30, 2, 27
        }, {
            1, 30, 2, 28
        }, {
            1, 30, 2, 29
        }, {
            1, 30, 2, 30
        }, {
            1, 30, 2, 31
        }
    };

    public RDataMatrix()
    {
        dotPixels = 8;
        margin = 30;
        processTilde = false;
        reBuild = false;
        internalCode = "";
        codeBinary = null;
        currentEncoding = E_ASCII;
        C49rest = 0;
        calculatedFormat = 0;
        encoding = E_ASCII;
        preferredFormat = -1;
    }

    private int random255(int code, int position)
    {
        int random = (149 * position) % 255;
        random++;
        int tmp = code + random;
        if(tmp <= 255)
            return tmp;
        else
            return tmp - 256;
    }

    public int getCalculatedFormat()
    {
        return calculatedFormat;
    }

    private boolean isDigit(int c)
    {
        return c >= 48 && c <= 57;
    }

    private void copyArray(int src[], int dest[], int initSrc, int initDest, int longi)
    {
        for(int i = 0; i < longi; i++)
            dest[initDest + i] = src[initSrc + i];

    }

    public int getPaintedWidth()
    {
        return currentX;
    }

    public int getPaintedHeight()
    {
        return currentY;
    }

    private int lookAheadTest(int Data[], int currentEnc, int pos, String specialSymbols[])
    {
        double asciiCount = 0.0D;
        double c40Count = 1.0D;
        double textCount = 1.0D;
        double b256Count = 1.25D;
        int initialPos = pos;
        if(currentEnc != E_ASCII)
        {
            asciiCount = 1.0D;
            c40Count = 2D;
            textCount = 2D;
            b256Count = 2.25D;
        }
        if(currentEnc == E_C40)
            c40Count = 0.0D;
        if(currentEnc == E_TEXT)
            textCount = 0.0D;
        if(currentEnc == E_BASE256)
            b256Count = 0.0D;
        for(; pos < Data.length; pos++)
        {
            char c = (char)Data[pos];
            if(isDigit(c))
                asciiCount += 0.5D;
            else
            if(c > '\177')
                asciiCount = Math.round(asciiCount) + 2L;
            else
                asciiCount = Math.round(asciiCount) + 1L;
            if(C40Codes[c].length == 1)
                c40Count += 0.66000000000000003D;
            else
            if(c > '\177')
                c40Count += 2.6600000000000001D;
            else
                c40Count += 1.3300000000000001D;
            char cText = c;
            String tmp = "" + c;
            if(c >= 'A' && c <= 'Z')
                cText = tmp.toLowerCase().charAt(0);
            if(c >= 'a' && c <= 'z')
                cText = tmp.toUpperCase().charAt(0);
            if(C40Codes[cText].length == 1)
                textCount += 0.66000000000000003D;
            else
            if(cText > '\177')
                textCount += 2.6600000000000001D;
            else
                textCount += 1.3300000000000001D;
            b256Count++;
            if(specialSymbols[pos] != null)
                return E_ASCII;
            if(pos - initialPos >= 4)
            {
                if(asciiCount + 1.0D <= c40Count && asciiCount + 1.0D <= textCount && asciiCount + 1.0D <= b256Count)
                    return E_ASCII;
                if(b256Count + 1.0D <= asciiCount)
                    return E_BASE256;
                if(b256Count + 1.0D < textCount && b256Count + 1.0D < c40Count)
                    return E_BASE256;
                if(textCount + 1.0D < asciiCount && textCount + 1.0D < c40Count && textCount + 1.0D < b256Count)
                    return E_TEXT;
                if(c40Count + 1.0D < asciiCount && c40Count + 1.0D < textCount && c40Count + 1.0D < b256Count)
                    return E_C40;
            }
        }

        asciiCount = Math.round(asciiCount);
        c40Count = Math.round(c40Count);
        textCount = Math.round(textCount);
        b256Count = Math.round(b256Count);
        if(asciiCount <= c40Count && asciiCount <= textCount && asciiCount <= b256Count)
            return E_ASCII;
        if(textCount < asciiCount && textCount < c40Count && textCount < b256Count)
            return E_TEXT;
        if(b256Count < asciiCount && b256Count < textCount && b256Count < c40Count)
            return E_BASE256;
        else
            return E_C40;
    }

    private int encodeAuto(int longi, int in[], int out[], String specialSymbols[])
    {
        int tmp[] = new int[6000];
        int tmpResult[] = new int[6000];
        int tmpResultLen = 0;
        int posResult = 0;
        int proposedEncoding = E_ASCII;
        int Initial[] = new int[1];
        int InitialOut[] = new int[1];
        int previousEncoding = E_ASCII;
        String specialSymbolsTMP[] = new String[10];
        for(int i = 0; i < specialSymbolsTMP.length; i++)
            specialSymbolsTMP[i] = null;

        int pos = 0;
        currentEncoding = E_ASCII;
        while(pos < longi) 
        {
            while(currentEncoding == E_ASCII && pos < longi) 
            {
                boolean done = false;
                if(pos + 1 < longi && isDigit(in[pos]) && isDigit(in[pos + 1]))
                {
                    if(previousEncoding != E_ASCII && previousEncoding != E_BASE256)
                        out[posResult++] = 254;
                    tmp[0] = in[pos];
                    tmp[1] = in[pos + 1];
                    tmpResultLen = encodeAscii(2, tmp, tmpResult, specialSymbolsTMP);
                    copyArray(tmpResult, out, 0, posResult, tmpResultLen);
                    posResult += tmpResultLen;
                    pos++;
                    pos++;
                    done = true;
                    previousEncoding = E_ASCII;
                }
                if(!done)
                {
                    proposedEncoding = lookAheadTest(in, currentEncoding, pos, specialSymbols);
                    if(proposedEncoding != E_ASCII)
                    {
                        previousEncoding = currentEncoding;
                        currentEncoding = proposedEncoding;
                    }
                }
                if(!done && currentEncoding == E_ASCII)
                {
                    if(previousEncoding != E_ASCII && previousEncoding != E_BASE256)
                        out[posResult++] = 254;
                    tmp[0] = in[pos];
                    specialSymbolsTMP[0] = specialSymbols[pos];
                    tmpResultLen = encodeAscii(1, tmp, tmpResult, specialSymbolsTMP);
                    specialSymbolsTMP[0] = null;
                    copyArray(tmpResult, out, 0, posResult, tmpResultLen);
                    posResult += tmpResultLen;
                    pos++;
                    previousEncoding = E_ASCII;
                }
            }
            for(; currentEncoding == E_C40 && pos < longi; currentEncoding = proposedEncoding)
            {
                if(previousEncoding != E_ASCII && previousEncoding != E_C40 && previousEncoding != E_BASE256)
                    out[posResult++] = 254;
                Initial[0] = pos;
                tmpResultLen = encodeC40(longi, Initial, in, tmpResult, false, previousEncoding != E_C40, true);
                pos = Initial[0];
                copyArray(tmpResult, out, 0, posResult, tmpResultLen);
                posResult += tmpResultLen;
                proposedEncoding = lookAheadTest(in, currentEncoding, pos, specialSymbols);
                previousEncoding = currentEncoding;
            }

            for(; currentEncoding == E_TEXT && pos < longi; currentEncoding = proposedEncoding)
            {
                if(previousEncoding != E_ASCII && previousEncoding != E_TEXT && previousEncoding != E_BASE256)
                    out[posResult++] = 254;
                Initial[0] = pos;
                tmpResultLen = encodeC40(longi, Initial, in, tmpResult, true, previousEncoding != E_TEXT, true);
                pos = Initial[0];
                copyArray(tmpResult, out, 0, posResult, tmpResultLen);
                posResult += tmpResultLen;
                proposedEncoding = lookAheadTest(in, currentEncoding, pos, specialSymbols);
                previousEncoding = currentEncoding;
            }

            if(currentEncoding == E_BASE256)
            {
                if(previousEncoding != E_ASCII && previousEncoding != E_BASE256)
                    out[posResult++] = 254;
                Initial[0] = pos;
                InitialOut[0] = posResult;
                encodeBase256(longi, Initial, in, InitialOut, out, true, specialSymbols);
                pos = Initial[0];
                posResult = InitialOut[0];
                proposedEncoding = lookAheadTest(in, currentEncoding, pos, specialSymbols);
                previousEncoding = currentEncoding;
                currentEncoding = proposedEncoding;
            }
        }
        return posResult;
    }

    private int encodeBase256(int longi, int initial[], int in[], int outInit[], int out[], boolean makeLookAhead, String specialSymbols[])
    {
        int count = 0;
        int tmp[] = new int[6000];
        int tmpInit = outInit[0];
        int tmpInitCount = outInit[0];
        int pos = 0;
        int k = 0;
        for(k = initial[0]; k < longi; k++)
        {
            tmp[count] = in[k];
            count++;
            pos = k + 1;
            int ii;
            if(pos == 24)
                ii = 0;
            if(!makeLookAhead || lookAheadTest(in, E_BASE256, pos, specialSymbols) == E_BASE256)
                continue;
            k++;
            break;
        }

        initial[0] = k;
        out[tmpInitCount++] = 231;
        if(count < 250)
        {
            out[tmpInitCount] = random255(count, tmpInitCount + 1);
            tmpInitCount++;
        } else
        {
            out[tmpInitCount] = random255(249 + (longi - longi % 250) / 250, tmpInitCount + 1);
            tmpInitCount++;
            out[tmpInitCount] = random255(longi % 250, tmpInitCount + 1);
            tmpInitCount++;
        }
        for(k = 0; k < count; k++)
        {
            out[tmpInitCount] = random255(tmp[k], tmpInitCount + 1);
            tmpInitCount++;
        }

        outInit[0] = tmpInitCount;
        return tmpInitCount;
    }

    private int encodeC40(int longi, int initial[], int in[], int out[], boolean doTextEncoding, boolean inititaLatch, boolean onlyOneWord)
    {
        int count = 0;
        int countMod3 = 0;
        int tmp[] = {
            0, 0, 0
        };
        int charac = 0;
        String specialSymbolsTMP[] = new String[10];
        for(int i = 0; i < specialSymbolsTMP.length; i++)
            specialSymbolsTMP[i] = null;

        if(inititaLatch)
            if(doTextEncoding)
                out[count++] = 239;
            else
                out[count++] = 230;
        for(int k = initial[0]; k < longi; k++)
        {
            charac = in[k];
            if(doTextEncoding)
            {
                String tmpS = "" + (char)charac;
                if(charac >= 97 && charac <= 122)
                    tmpS = tmpS.toUpperCase();
                if(charac >= 65 && charac <= 90)
                    tmpS = tmpS.toLowerCase();
                charac = tmpS.charAt(0);
            }
            int codes[] = C40Codes[charac];
            for(int h = 0; h < codes.length; h++)
            {
                tmp[countMod3++] = codes[h];
                if(countMod3 == 3)
                {
                    int t = tmp[0] * 1600 + tmp[1] * 40 + tmp[2] + 1;
                    out[count++] = t / 256;
                    out[count++] = t % 256;
                    countMod3 = 0;
                }
            }

            if(onlyOneWord && countMod3 == 0)
            {
                C49rest = countMod3;
                initial[0] = k + 1;
                if(initial[0] == longi)
                    out[count++] = 254;
                return count;
            }
        }

        initial[0] = longi;
        if(countMod3 > 0)
        {
            if(countMod3 == 2)
            {
                int t = tmp[0] * 1600 + tmp[1] * 40 + 0 + 1;
                out[count++] = t / 256;
                out[count++] = t % 256;
                out[count++] = 254;
            }
            if(countMod3 == 1)
            {
                out[count++] = 254;
                int encodedASCII[] = new int[2];
                int toencode[] = new int[countMod3];
                for(int h = 0; h < countMod3; h++)
                    toencode[h] = in[longi - (countMod3 - h)];

                int asciiLen = encodeAscii(countMod3, toencode, encodedASCII, specialSymbolsTMP);
                for(int h = 0; h < asciiLen; h++)
                    out[count++] = encodedASCII[h];

            }
        } else
        {
            out[count++] = 254;
        }
        C49rest = countMod3;
        return count;
    }

    protected void paintBasis(Graphics g)
    {
        RDataMatrix _tmp = this;
        if(false)
            return;
        topMarginPixels = margin;
        leftMarginPixels = margin;
        if(code.length() == 0 && codeBinary == null)
            return;
        g.setColor(backColor);
        int w = getSize().width;
        int h = getSize().height;
        g.fillRect(0, 0, w, h);
        if(bitmap == null || reBuild)
            doCode();
        if(bitmap == null)
            return;
        int x = margin;
        int y = margin;
        for(int j1 = 0; j1 < rows; j1++)
        {
            for(int i1 = 0; i1 < cols; i1++)
                if(bitmap[i1][j1] != 0)
                    paintDot(g, x + dotPixels * i1, y + dotPixels * j1, dotPixels, barColor);
                else
                    paintDot(g, x + dotPixels * i1, y + dotPixels * j1, dotPixels, backColor);

        }

        currentX = dotPixels * cols + margin;
        currentY = dotPixels * rows + margin;
        g.setColor(barColor);
        g.setFont(new Font("Arial", 0, 11));
    }

    private void paintDot(Graphics g, int x, int y, int w, Color c)
    {
        g.setColor(c);
        g.fillRect(x, y, w, w);
    }

    private int random253(int code, int position)
    {
        int random = (149 * position) % 253;
        random++;
        int tmp = code + random;
        if(tmp <= 254)
            return tmp;
        else
            return tmp - 254;
    }

    private String applyTilde(String code, String specialSymbol[])
    {
        int c = 0;
        int longi = code.length();
        String result = "";
        boolean done = false;
        for(int i = 0; i < longi; i++)
        {
            c = code.charAt(i);
            if(c == 126)
            {
                if(i < longi - 1)
                {
                    char nextc = code.charAt(i + 1);
                    if(nextc >= '@' && nextc <= 'Z')
                    {
                        i++;
                        result = result + (char)(nextc - 64);
                    }
                    if(nextc == '~')
                    {
                        result = result + '~';
                        i++;
                    }
                    if(nextc == '1')
                    {
                        if(result.length() == 0 || result.length() == 1 || result.length() == 4 || result.length() == 5)
                        {
                            specialSymbol[result.length()] = "";
                            result = result + '\350';
                        } else
                        {
                            result = result + '\035';
                        }
                        i++;
                    }
                    if(nextc == '2' && i < longi - 4)
                    {
                        specialSymbol[result.length()] = "" + code.charAt(i + 2) + code.charAt(i + 3) + code.charAt(i + 4);
                        result = result + '\351';
                        i += 4;
                    }
                    if(nextc == '3' && result.length() == 0)
                    {
                        specialSymbol[result.length()] = "";
                        result = result + '\352';
                        i++;
                    }
                    if(nextc == '5' && result.length() == 0)
                    {
                        specialSymbol[result.length()] = "";
                        result = result + '\354';
                        i++;
                    }
                    if(nextc == '6' && result.length() == 0)
                    {
                        specialSymbol[result.length()] = "";
                        result = result + '\355';
                        i++;
                    }
                    if(nextc == '7' && i < longi - 7)
                    {
                        String eciString = code.substring(i + 2, i + 8);
                        double eci = 0.0D;
                        try
                        {
                            eci = (new Double(eciString)).doubleValue();
                        }
                        catch(Exception e)
                        {
                            eci = 0.0D;
                        }
                        if(eci <= 126D)
                        {
                            specialSymbol[result.length()] = "" + (char)(int)(eci + 1.0D);
                            result = result + '\361';
                        }
                        if(eci >= 127D && eci <= 16382D)
                        {
                            int c1 = (int)((eci - 127D) / 254D) + 128;
                            int c2 = (int)((eci - 127D) % 254D) + 1;
                            specialSymbol[result.length()] = "" + (char)c1 + (char)c2;
                            result = result + '\361';
                        }
                        if(eci >= 16383D)
                        {
                            int c1 = (int)((eci - 16383D) / 64516D) + 192;
                            int c2 = (int)((eci - 16383D) / 254D);
                            c2 = c2 % 254 + 1;
                            int c3 = (int)((eci - 16383D) % 254D) + 1;
                            specialSymbol[result.length()] = "" + (char)c1 + (char)c2 + (char)c3;
                            result = result + '\361';
                        }
                        i += 7;
                    }
                    if(nextc == 'd' && i < longi - 3)
                    {
                        String ascString = code.substring(i + 2, i + 5);
                        int asc = 0;
                        try
                        {
                            asc = (new Integer(ascString)).intValue();
                        }
                        catch(Exception e)
                        {
                            asc = 0;
                        }
                        if(asc > 255)
                            asc = 255;
                        result = result + (char)asc;
                        i += 4;
                    }
                }
            } else
            {
                result = result + (char)c;
            }
        }

        return result;
    }

    private void doCode()
    {
        String specialSymbol[] = new String[5000];
        reBuild = false;
        int c[] = null;
        if(codeBinary != null)
        {
            c = new int[codeBinary.length];
            int tmp = 0;
            for(int i = 0; i < c.length; i++)
            {
                tmp = codeBinary[i];
                if(tmp < 0)
                    tmp += 256;
                c[i] = tmp;
            }

            tmp = 0;
        } else
        {
            internalCode = code;
            if(processTilde)
                internalCode = applyTilde(code, specialSymbol);
            if(internalCode.length() == 0)
                return;
            c = new int[internalCode.length()];
            for(int i = 0; i < internalCode.length(); i++)
                c[i] = internalCode.charAt(i);

        }
        bitmap = drawBarcode(c, specialSymbol);
    }

    private int[][] drawBarcode(int codeStr[], String specialSymbols[])
    {
        int encoded[] = new int[5000];
        int dummy[] = new int[1];
        int dummy2[] = new int[1];
        int encodedLength = 0;
        if(encoding != E_AUTO)
            currentEncoding = encoding;
        if(encoding == E_AUTO)
            encodedLength = encodeAuto(codeStr.length, codeStr, encoded, specialSymbols);
        if(encoding == E_ASCII)
            encodedLength = encodeAscii(codeStr.length, codeStr, encoded, specialSymbols);
        if(encoding == E_C40)
            encodedLength = encodeC40(codeStr.length, dummy, codeStr, encoded, false, true, false);
        if(encoding == E_TEXT)
            encodedLength = encodeC40(codeStr.length, dummy, codeStr, encoded, true, true, false);
        if(encoding == E_BASE256)
            encodedLength = encodeBase256(codeStr.length, dummy, codeStr, dummy2, encoded, false, specialSymbols);
        int i;
        if(encoding == E_NONE)
        {
            encodedLength = codeStr.length;
            for(i = 0; i < encodedLength; i++)
                encoded[i] = codeStr[i];

        }
        i = 0;
        if(preferredFormat != -1)
        {
            i = preferredFormat;
            if(encodedLength > configuration[i][7])
                i = 0;
        }
        for(; encodedLength > configuration[i][7] && i < 30; i++)
        {
            if(currentEncoding != E_C40 && currentEncoding != E_TEXT)
                continue;
            if(C49rest == 1 && encoded[encodedLength - 2] == 254 && configuration[i][7] == encodedLength - 1)
            {
                encoded[encodedLength - 2] = encoded[encodedLength - 1];
                encoded[encodedLength - 1] = 0;
                encodedLength--;
                break;
            }
            if(C49rest != 0 || encoded[encodedLength - 1] != 254 || configuration[i][7] != encodedLength - 1)
                continue;
            encoded[encodedLength - 1] = 0;
            encodedLength--;
            break;
        }

        if(i == 30)
            return null;
        int config = i;
        calculatedFormat = config;
        rows = configuration[config][0];
        cols = configuration[config][1];
        datarows = configuration[config][2];
        datacols = configuration[config][3];
        maprows = configuration[config][5];
        mapcols = configuration[config][6];
        regions = configuration[config][4];
        totaldata = configuration[config][7];
        totalerr = configuration[config][8];
        reeddata = configuration[config][9];
        reederr = configuration[config][10];
        reedblocks = configuration[config][11];
        if((currentEncoding == E_C40 || currentEncoding == E_TEXT) && C49rest == 0 && encodedLength == totaldata && encoded[encodedLength - 1] == 254)
            encoded[encodedLength - 1] = 129;
        int blocks[][] = new int[10][255];
        boolean firstPad = true;
        for(int h = encodedLength; h < totaldata; h++)
        {
            if(firstPad)
                encoded[h] = 129;
            else
                encoded[h] = random253(129, h + 1);
            firstPad = false;
        }

        int tmp = 0;
        int tmpcol = 0;
        for(int h = 1; h <= totaldata; h++)
        {
            blocks[tmp][tmpcol] = encoded[h - 1];
            if(++tmp == reedblocks)
            {
                tmp = 0;
                tmpcol++;
            }
        }

        int blocksLen[] = new int[10];
        int totalLen = 0;
        reed reedsol = new reed();
        reed _tmp = reedsol;
        reed.K = reeddata;
        for(int h = 0; h < reedblocks; h++)
        {
            blocksLen[h] = reeddata + reederr;
            int tmpReedData = reeddata;
            if(rows == 144 && h > 7)
            {
                blocksLen[h] = (reeddata + reederr) - 1;
                tmpReedData = 155;
            }
            reedsol.calcRS(blocks[h], tmpReedData, reederr);
            totalLen += blocksLen[h];
        }

        int finalBlock[] = new int[totalLen];
        int finalBlockLen = 0;
        int c = 0;
        for(int j = 0; j < blocksLen[0]; j++)
        {
            for(int h = 0; h < reedblocks; h++)
                if(j < blocksLen[h])
                {
                    finalBlock[c++] = blocks[h][j];
                    finalBlockLen++;
                }

        }

        int bitmap[][] = createBitmap(finalBlock);
        return bitmap;
    }

    private void debugMatrix(int m[][], int c, int r)
    {
        String s = "";
        System.out.println(s);
        System.out.println(s);
        System.out.println(s);
        for(int j1 = 0; j1 < r; j1++)
        {
            s = "";
            for(int i1 = 0; i1 < c; i1++)
                if(m[i1][j1] != 0)
                    s = s + "X";
                else
                    s = s + " ";

            System.out.println(s);
        }

        System.out.println("");
        System.out.println("");
    }

    private int[][] createBitmap(int codeStr[])
    {
        int result[][] = new int[cols][rows];
        int curx = 0;
        int cury = 0;
        if(regions == 2)
        {
            drawBorders(result, curx, cury, datacols + 2, datarows + 2);
            drawBorders(result, curx + datacols + 2, cury, datacols + 2, datarows + 2);
        } else
        {
            int tmp = (int)Math.sqrt(regions);
            for(int i = 0; i < tmp; i++)
            {
                for(int j = 0; j < tmp; j++)
                    drawBorders(result, curx + i * (datacols + 2), cury + j * (datarows + 2), datacols + 2, datarows + 2);

            }

        }
        int tmpMap[] = new int[(mapcols + 10) * maprows];
        charPlacer placer = new charPlacer();
        placer.ncol = mapcols;
        placer.nrow = maprows;
        placer.array = tmpMap;
        placer.make();
        int vertical = 1;
        int r2 = 0;
        int c2 = 0;
        for(int r = 0; r < maprows; r++)
        {
            int horizontal = 1;
            for(int c = 0; c < mapcols; c++)
            {
                c2 = c + horizontal;
                r2 = r + vertical;
                if(tmpMap[r * mapcols + c] > 9)
                {
                    int character = tmpMap[r * mapcols + c] / 10;
                    int bit = tmpMap[r * mapcols + c] % 10;
                    int dot = codeStr[character - 1] & 1 << 8 - bit;
                    result[c2][r2] = dot;
                } else
                {
                    result[c2][r2] = tmpMap[r * mapcols + c];
                }
                if(c > 0 && (c + 1) % datacols == 0)
                    horizontal += 2;
            }

            if(r > 0 && (r + 1) % datarows == 0)
                vertical += 2;
        }

        return result;
    }

    private void drawBorders(int map[][], int x, int y, int w, int h)
    {
        int alternative = 0;
        for(int i = 0; i < w; i++)
        {
            if(i % 2 == 0)
                alternative = 1;
            else
                alternative = 0;
            map[x + i][(y + h) - 1] = 1;
            map[x + i][y] = alternative;
        }

        alternative = 0;
        for(int i = 0; i < h; i++)
        {
            if((i + 1) % 2 == 0)
                alternative = 1;
            else
                alternative = 0;
            map[x][y + i] = 1;
            map[(x + w) - 1][y + i] = alternative;
        }

    }

    private int encodeAscii(int longi, int in[], int out[], String specialSymbols[])
    {
        int outp = 0;
        boolean done = false;
        for(int j = 0; j < longi; j++)
        {
label0:
            {
label1:
                {
                    done = false;
                    if(j < longi - 1 && in[j] >= 48 && in[j] <= 57 && in[j + 1] >= 48 && in[j + 1] <= 57 && j < longi)
                    {
                        int tmp = (in[j] - 48) * 10 + (in[j + 1] - 48);
                        out[outp++] = 130 + tmp;
                        j++;
                        done = true;
                    }
                    if(done || specialSymbols[j] == null)
                        break label0;
                    RDataMatrix _tmp = this;
                    if(in[j] != 234)
                    {
                        RDataMatrix _tmp1 = this;
                        if(in[j] != 237)
                        {
                            RDataMatrix _tmp2 = this;
                            if(in[j] != 236)
                            {
                                RDataMatrix _tmp3 = this;
                                if(in[j] != 232)
                                    break label1;
                            }
                        }
                    }
                    out[outp++] = in[j];
                    done = true;
                }
                RDataMatrix _tmp4 = this;
                if(in[j] != 233)
                {
                    RDataMatrix _tmp5 = this;
                    if(in[j] != 241)
                        break label0;
                }
                out[outp++] = in[j];
                for(int l = 0; l < specialSymbols[j].length(); l++)
                    out[outp++] = specialSymbols[j].charAt(l);

                done = true;
            }
            if(!done)
                if(in[j] < 128)
                {
                    out[outp++] = in[j] + 1;
                } else
                {
                    out[outp++] = 235;
                    out[outp++] = (in[j] - 128) + 1;
                }
        }

        return outp;
    }

}
