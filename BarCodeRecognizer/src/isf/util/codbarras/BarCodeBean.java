// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   BarCodeBean.java

package isf.util.codbarras;

import java.awt.Color;
import java.awt.Font;


public class BarCodeBean extends BarCode
{

    public BarCodeBean()
    {
    }

    public boolean getTextOnTop()
    {
        return textOnTop;
    }

    public void setTextOnTop(boolean b)
    {
        textOnTop = b;
    }

    public String getBarType()
    {
        switch(barType)
        {
        case 0: // '\0'
            return "BAR39";

        case 1: // '\001'
            return "BAR39EXT";

        case 2: // '\002'
            return "INTERLEAVED25";

        case 3: // '\003'
            return "CODE11";

        case 4: // '\004'
            return "CODABAR";

        case 5: // '\005'
            return "MSI";

        case 6: // '\006'
            return "UPCA";

        case 7: // '\007'
            return "IND25";

        case 8: // '\b'
            return "MAT25";

        case 9: // '\t'
            return "CODE93";

        case 10: // '\n'
            return "EAN13";

        case 11: // '\013'
            return "EAN8";

        case 12: // '\f'
            return "UPCE";

        case 13: // '\r'
            return "CODE128";

        case 16: // '\020'
            return "EAN128";

        case 14: // '\016'
            return "CODE93EXT";

        case 15: // '\017'
            return "POSTNET";
        }
        return "";
    }

    public void setBarType(String t)
    {
        t = t.toUpperCase();
        if(t.compareTo("BAR39") == 0)
            barType = 0;
        if(t.compareTo("BAR39EXT") == 0)
            barType = 1;
        if(t.compareTo("INTERLEAVED25") == 0)
            barType = 2;
        if(t.compareTo("CODE11") == 0)
            barType = 3;
        if(t.compareTo("CODABAR") == 0)
            barType = 4;
        if(t.compareTo("MSI") == 0)
            barType = 5;
        if(t.compareTo("UPCA") == 0)
            barType = 6;
        if(t.compareTo("IND25") == 0)
            barType = 7;
        if(t.compareTo("MAT25") == 0)
            barType = 8;
        if(t.compareTo("CODE93") == 0)
            barType = 9;
        if(t.compareTo("EAN13") == 0)
            barType = 10;
        if(t.compareTo("EAN128") == 0)
            barType = 16;
        if(t.compareTo("EAN8") == 0)
            barType = 11;
        if(t.compareTo("UPCE") == 0)
            barType = 12;
        if(t.compareTo("CODE128") == 0)
            barType = 13;
        if(t.compareTo("CODE93EXT") == 0)
            barType = 14;
        if(t.compareTo("POSTNET") == 0)
            barType = 15;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String c)
    {
        code = c;
    }

    public boolean getAddCheckCharacter()
    {
        return checkCharacter;
    }

    public void setAddCheckCharacter(boolean b)
    {
        checkCharacter = b;
    }

    public double getPostnetHeightTallBar()
    {
        return postnetHeightTallBar;
    }

    public void setPostnetHeightTallBar(double d)
    {
        postnetHeightTallBar = d;
    }

    public double getPostnetHeightShortBar()
    {
        return postnetHeightShortBar;
    }

    public void setPostnetHeightShortBar(double d)
    {
        postnetHeightShortBar = d;
    }

    public double getLeftMarginCM()
    {
        return leftMarginCM;
    }

    public void setLeftMarginCM(double d)
    {
        leftMarginCM = d;
    }

    public double getTopMarginCM()
    {
        return topMarginCM;
    }

    public void setTopMarginCM(double d)
    {
        topMarginCM = d;
    }

    public String getSuplement()
    {
        return supplement;
    }

    public void setSuplement(String c)
    {
        supplement = c;
    }

    public boolean getGuardBars()
    {
        return guardBars;
    }

    public void setGuardBars(boolean b)
    {
        guardBars = b;
    }

    public Color getBackColor()
    {
        return backColor;
    }

    public void setBackColor(Color c)
    {
        backColor = c;
    }

    public String getCodeText()
    {
        return codeText;
    }

    public int getResolution()
    {
        return resolution;
    }

    public void setResolution(int i)
    {
        resolution = i;
    }

    public double getBarHeightCM()
    {
        return barHeightCM;
    }

    public void setBarHeightCM(double d)
    {
        barHeightCM = d;
    }

    public Font getTextFont()
    {
        return textFont;
    }

    public void setTextFont(Font f)
    {
        textFont = f;
    }

    public Color getFontColor()
    {
        return fontColor;
    }

    public void setFontColor(Color c)
    {
        fontColor = c;
    }

    public Color getBarColor()
    {
        return barColor;
    }

    public void setBarColor(Color c)
    {
        barColor = c;
    }

    public String getUPCESytem()
    {
        return "" + UPCESytem;
    }

    public void setUPCESytem(String d)
    {
        if(d.length() > 0)
            UPCESytem = d.charAt(0);
    }

    public String getCODABARStartChar()
    {
        return "" + CODABARStartChar;
    }

    public void setCODABARStartChar(String d)
    {
        if(d.length() > 0)
            CODABARStartChar = d.charAt(0);
    }

    public String getCODABARStopChar()
    {
        return "" + CODABARStopChar;
    }

    public void setCODABARStopChar(String d)
    {
        if(d.length() > 0)
            CODABARStopChar = d.charAt(0);
    }

    public boolean getUPCEANSupplement2()
    {
        return UPCEANSupplement2;
    }

    public void setUPCEANSupplement2(boolean b)
    {
        UPCEANSupplement2 = b;
    }

    public boolean getUPCEANSupplement5()
    {
        return UPCEANSupplement5;
    }

    public void setUPCEANSupplement5(boolean b)
    {
        UPCEANSupplement5 = b;
    }

    public String getCode128Set()
    {
        return "" + Code128Set;
    }

    public void setCode128Set(String d)
    {
        if(d.length() > 0)
            Code128Set = d.charAt(0);
    }

    public double getSizeX()
    {
        return X;
    }

    public void setSizeX(double d)
    {
        X = d;
    }

    public double getN()
    {
        return N;
    }

    public void setN(double d)
    {
        N = d;
    }

    public double getI()
    {
        return I;
    }

    public void setI(double d)
    {
        I = d;
    }

    public double getH()
    {
        return H;
    }

    public void setH(double d)
    {
        H = d;
    }

    public double getL()
    {
        return L;
    }

    public void setL(double d)
    {
        L = d;
    }

    public int getRotate()
    {
        return rotate;
    }

    public void setRotate(int i)
    {
        rotate = i;
    }

    public double getSupSeparationCM()
    {
        return supSeparationCM;
    }

    public void setSupSeparationCM(double d)
    {
        supSeparationCM = d;
    }

    public double getSupHeight()
    {
        return supHeight;
    }

    public void setSupHeight(double d)
    {
        supHeight = d;
    }

    public String getName()
    {
        return "RBarCode";
    }

    public boolean getProcessTilde()
    {
        return processTilde;
    }

    public void setProcessTilde(boolean pt)
    {
        processTilde = pt;
    }
}
