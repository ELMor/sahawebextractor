// JavaScript Document

var URL = 0, LEFT = 1, TOP = 2, WIDTH = 3, HEIGHT = 4, TITULO = 5, PROPIEDADES = 6;
var ventanas = new Array();
//Caracter�sticas de las ventanas implicadas
//ventanas[id]         URL,left,top,width,height,titulo,propiedades
ventanas["Error"] = new Array("nada.jsp", 0, 0, 1000, 500, "Nada");
ventanas["Calendario"] = new Array("calendario.jsp", 100, 100, 370, 240, "Calendario");
ventanas["Servicios"] = new Array("ListaServicios.jsp", 350, 200, 570, 435, "Servicios");
ventanas["Medicos"] = new Array("ListaMedicos.jsp", 350, 200, 560, 465, "Lista de m�dicos" );
ventanas["Exploraciones"] = new Array("Exploraciones.jsp", 150, 200, 850, 380, "Exploraciones" );
ventanas["ExploracionesInf"] = new Array("ExploracionesInforme.jsp", 150, 150, 820, 450, "Exploraciones" );
ventanas["ExploracionesPac"] = new Array("Cons_Exploraciones.jsp", 150, 200, 850, 400, "Exploraciones Paciente" );
ventanas["ExploracionesSimple"] = new Array("Cons_Exploraciones_Siguientes.jsp", 350, 200, 390, 350, "Exploraciones" );
ventanas["EstadoPeticiones"] = new Array("ListaEstadoPeti.jsp", 350, 200, 520, 400, "Estado de las peticiones" );
ventanas["Pacientes"] = new Array("ListaPacientes.jsp", 200, 150, 680, 520, "Lista de pacientes" );
ventanas["Informe"] = new Array("Informe.jsp", 100, 150, 820, 450, "Datos de Informe" );
ventanas["ValoracionPeticion"] = new Array("Valoracion_Peticion.jsp", 100, 100, 820, 550, "Datos de la petici�n" );
ventanas["Peticion"] = new Array("RegPeticion.jsp", 100, 150, 820, 500, "Datos de la petici�n" );
ventanas["Tarjeta"] = new Array("registroTarjeta.jsp", 300, 200, 400, 250, "Registro de Tarjeta Sanitaria" );
ventanas["PeticionRechazada"] = new Array("PeticionRechazada.jsp", 300, 200, 400, 335, "Petici�n Rechazada" );
ventanas["Historico"] = new Array("Historico.jsp", 350, 150, 550, 580, "Hist�rico del paciente");
ventanas["Alarmas"] = new Array("Alarmas.jsp", 350, 200, 460, 295, "Alarmas del paciente");
ventanas["Observaciones"] = new Array("nota.jsp", 350, 200, 380, 305, "Observaciones");
ventanas["Agendas"] = new Array("ListaAgendas.jsp", 350, 200, 590, 440, "Agendas");
ventanas["Salas"] = new Array("ListaSalas.jsp", 350, 200, 580, 435, "Salas");
ventanas["MotivosFallo"] = new Array("ListaFallos.jsp", 350, 200, 450, 340, "Motivos de fallo");
ventanas["PacienteUrgente"] = new Array("paciente_urgente.jsp", 100, 140, 820, 500, "Paciente urgente");
ventanas["Peticiones"] = new Array("Peticion.jsp", 150, 200, 830, 530, "Peticion");
ventanas["PlantillasInformes"] = new Array("plantillasInformes.jsp", 350, 200, 430, 390, "Plantillas de Informes");
ventanas["AdjuntarFichero"] = new Array("adjuntarFichero.jsp", 300, 200, 400, 250, "Adjuntar ficheros");
ventanas["Equipos"] = new Array("Gestion_Equipos.jsp", 150, 150, 820, 500, "Gesti�n del Equipo");
ventanas["Citas"] = new Array("Citaweb.jsp", 150, 150, 850, 540, "Citaci�n");
ventanas["Pasos"] = new Array("historicoExploraciones.jsp", 150, 150, 850, 215, "Pasos Exploraci�n");
ventanas["PeticionesPac"] = new Array("Cons_PeticionesPac.jsp", 150, 200, 850, 400, "Peticiones Paciente" );
ventanas["Digitaliza"] = new Array("digitalizaPlaca.jsp", 350, 300, 400, 250, "Digitalizar Placa" );
ventanas["Incidencias"] = new Array("registroIncidencias.jsp", 150, 250, 850, 330, "Registrar incidencias" );
ventanas["PersonalRecepcion"] = new Array("ListaPersoRecep.jsp", 350, 200, 560, 465, "Lista de personal de recepci�n" );
ventanas["ImagenDicom"] = new Array("VisorDicom.jsp", 100, 75, 800, 600, "Imagen Dicom", "resizable" );
ventanas["AsistenteWeb"] = new Array("asistenteInteractivo.jsp", 100, 75, 400, 250, "Asistente Interactivo" );
ventanas["Historial"] = new Array("RegPeticion_0a.jsp", 100, 75, 820, 260, "Historial del paciente" );
ventanas["CasosInteresantes"] = new Array("Captura_CasosInteresantes.jsp", 100, 75, 650, 400, "Captura de casos interesantes");

//Abre una ventana en funci�n de un identificador con los par�metros por defecto
//si no se especifica lo contrario
function abreVentanaNueva(id,l,t,w,h,vars){
	if( ventanas[id]+"." == "undefined." ) id = "Error";
	if( l+"." == "undefined." ){
		l = ventanas[id][LEFT];
		t = ventanas[id][TOP];
	}
	if( w+"." == "undefined." ){
		w = ventanas[id][WIDTH];
		h = ventanas[id][HEIGHT];
	}
	var props = "left="+l.toString()+",top="+t.toString()+",width="+w.toString()+",height="+h.toString();
	if( vars+"." != "undefined." ) props = props + "," + vars;
	//A�adimos las propiedades por defecto
	if( ventanas[id].length > 6 ) props += "," + ventanas[id][PROPIEDADES];
	var nw = window.open(ventanas[id][URL],"",props);
	nw.focus();
}

//Abre una ventana MODAL en funci�n de un identificador con los par�metros por defecto
//si no se especifica lo contrario
function abreVentanaModal(id,l,t,w,h,vars){
	if( ventanas[id]+"." == "undefined." ) id = "Error";
	if( l+"." == "undefined." ){
		l = ventanas[id][LEFT];
		t = ventanas[id][TOP];
	}
	if( w+"." == "undefined." ){
		w = ventanas[id][WIDTH];
		h = ventanas[id][HEIGHT];
	}
	var props = "dialogLeft:"+l.toString()+"px;dialogTop:"+t.toString()+"px;dialogWidth:"+w.toString()+"px;dialogHeight:"+h.toString()+"px";
	return window.showModalDialog(ventanas[id][URL],vars,props);
}

//Abre un calendario y ubica el resultado en el lugar indicado
function abreCalendario( txtResultado,l,t ){
	if( l+"." == "undefined." ){ //Por defecto ubica el calendario en relaci�n al rat�n
		l = window.event.screenX - 100;
		t = window.event.screenY - 100;
	}
	var fecha = abreVentanaModal( "Calendario", l, t );
	if( fecha+"." != "undefined." ){
		document.all.item( txtResultado ).value = fecha;
	}
}

//
//
function abreAsistente(titulo,mensajes){
	var queryString = "?titulo="+titulo;
	for( var i=0; i<mensajes.length; i++ ){
        queryString += "&mensaje="+mensajes[i];
	}
	ventanas["AsistenteWeb"][URL] = "asistenteInteractivo.jsp" + queryString;		
	abreVentanaNueva("AsistenteWeb");
}
