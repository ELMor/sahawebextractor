<html>
<head>
<title>Historial radiológico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/style_ex.css" type="text/css">
<script language="JavaScript" src="script/css.js"></script>
<script language="JavaScript" src="./javascript/libreriaCapas.js"></script>
<script Langage="JavaScript">
<!--
function validate(theForm) {
  return true;
}

function accionAceptar(){
	window.close();
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function accionOtrosCentros(){
	muestraCapa("Layer1");
}

//-->
</script>
</head>
<body topmargin="16" leftmargin="16" rightmargin="0" onLoad="MM_preloadImages('imagenes/bot_aceptar_sel.gif','imagenes/bot_buscar_sel.gif','imagenes/bot_otros_centros_sel.gif')">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      
    <td width="25" height="24" > <img src="imagenes/Circunfai-p.gif" border="0" width="25" height="100%" alt=""> 
    </td>
      <td height="24" width="100%" bgcolor="#DAE4F2"></td>
      <td width="25" height="24">
	  	  <img src="imagenes/Circunfad-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
    </tr>
    <tr>
      <td width="25" bgcolor="#DAE4F2"></td>
      <td width="100%" bgcolor="#DAE4F2">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr height="1">
        <td colspan="3">&nbsp;</td>
      </tr>
      <tr height="1">
        <td height="1" colspan="3" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr>
        <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
        <td>
          <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
            <tr>
              <td>
                <table border="0" cellspacing="0" cellpadding="5" align="center" width="100%">
                  <tr bgcolor="#DAE4F2">
                    <td width="42%" align="left" class="titulo" >Exploración</td>
                    <td width="19%" align="left" class="titulo">Fecha</td>
                    <td width="24%" align="left" class="titulo" >Diagnóstico</td>
					<td width="7%" align="left" class="titulo" ></td>
					<td width="8%" align="left" class="titulo" ></td>
        			</tr>
				</table>
				</td>
			</tr>
			<tr> 
            <td class="standar" colspan="3" bgcolor="#002969"></td>
          	</tr>
		     <tr> 
            <td > <iframe id="iframe1"    marginheight="0" marginwidth="0" width="100%" scrolling="yes" src="datosHistorico.jsp"></iframe> 
            </td>
          	</tr>
		  <tr> 
            <td class="standar" colspan="3" bgcolor="#002969"></td>
          </tr>
		  <tr> 
		    <td valign="top" align="right">
			 <table border="0" cellspacing="0" cellpadding="3" align="center" width="100%">
                    <tr>
                      <td align="left"><a href="#" onClick="MM_callJS('accionOtrosCentros()')" onMouseOver="MM_swapImage('Image15','','imagenes/bot_otros_centros_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/bot_otros_centros.gif" alt="Otros centros" name="Image15" width="101" height="19" border="0"></a></td>
                      <td align="right"> <a href="#" onClick="MM_callJS('accionAceptar()')" onMouseOver="MM_swapImage('Image9','','imagenes/bot_aceptar_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/bot_aceptar.gif" alt="Aceptar" name="Image9" width="67" height="19" border="0"></a></td>
                    </tr>
                  </table>
           </td>
		   </tr>
          <tr height="1"> 
            <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
          </tr>
		  </table>
		  </td>
		   <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
		  </tr>
 		  <tr><td colspan="3">
<div id="Layer1" style="position:relative; left:0px; top:0px; width:100%; height:304px; z-index:1; visibility: hidden;"> 
              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr height="1"> 
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr height="1"> 
                  <td height="1" colspan="3" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                </tr>
                <tr> 
                  <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                  <td> <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                      <tr> 
                        <td> <table border="0" cellspacing="0" cellpadding="5" align="center" width="100%">
                            <tr bgcolor="#DAE4F2"> 
                              <td align="center" class="titulo" >Otros Centros</td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr> 
                        <td class="standar" colspan="3" bgcolor="#002969"></td>
                      </tr>
                      <tr> 
                        <td > <iframe id="ifOtrosCentros"  src="datosOtrosCentros.jsp"  marginheight="0" marginwidth="0" width="100%" scrolling="yes"></iframe> 
                        </td>
                      </tr>
                      <tr> 
                        <td class="standar" colspan="3" bgcolor="#002969"></td>
                      </tr>
                      <tr> 
                        <td valign="top" align="right"> <table border="0" cellspacing="0" cellpadding="3" align="center" width="100%">
                            <tr> 
                              <td align="right">&nbsp; </td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr height="1"> 
                        <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                      </tr>
                    </table></td>
                  <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                </tr>
              </table>
            </div>
</td></tr>
    </table>
</td>
      <td width="25" bgcolor="#DAE4F2"></td>
    </tr>
    <tr>
      <td width="25">
	  	  <img src="imagenes/Circunfabi-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
      <td width="100%" bgcolor="#DAE4F2"></td>
      <td height="24">
	  	  <img src="imagenes/Circunfabd-p.gif" border="0" width="25" height="100%" alt=""> 
	  </td>
    </tr>
</table>
</BODY>
</HTML>
