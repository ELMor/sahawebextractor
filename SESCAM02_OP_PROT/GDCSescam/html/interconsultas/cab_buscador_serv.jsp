<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
<script language="javascript">

 function recargar(id)
   {
   document.formulario.centroculto.value = document.formulario.centro.value;
   if (id == 1)
       document.formulario.especialidadoculto.value = 0;
   else
      document.formulario.especialidadoculto.value = document.formulario.especialidad.value;
   document.formulario.permiso.value =  document.formulario.permiso.value
   document.formulario.submit();
   }
  </script></head>
<body class="paginasbusquedavanzada" bgcolor="#FFFFFF" text="#000000">
<%
  int centro = 0;
  int especialidad = 0;
  String marca[] = new String[12];
  String permiso = "";
  if (request.getParameter("centroculto") != null) {
     centro = new Integer(request.getParameter("centroculto")).intValue();
     if (centro == -1) centro = 0;
  }
  if (request.getParameter("permitir") != null) permiso= request.getParameter("permitir");
  else permiso = request.getParameter("permiso");

  if (request.getParameter("especialidadoculto") != null)
    especialidad = new Integer(request.getParameter("especialidadoculto")).intValue();
  for (int i = 0; i<12; i++) {
    if (i == especialidad)
      marca[i] = new String("selected");
    else
      marca[i] = new String("");
  }
%>
<form name="formulario" target="cabFrame" action="cab_buscador_serv.jsp">
 <table width="100%" border=0 align="left" cellpadding=0 cellspacing=0>
 <tr>
    <td width="20%" class = "datosventana"> Centro </td>
    <td width="40%"  class= "datosventana">

       <Select name="centro" class="inputformulario9" style="width=90%" <%=permiso%> onChange="recargar(1);">
<%
  if (centro == 0) {
%>
   <option value="0" selected>C.H. LA MANCHA CENTRO</option>
   <option value="1">C.H. DE TOLEDO</option>
   <option value="2">C. H. DE CIUDAD REAL</option>
   <option value="3">H. SANTA B�RBARA</option>
   <option value="4">C. H. G. DE ALBACETE</option>
%>
<%
   } else if (centro == 1) {
%>
     <option value="0">C.H. LA MANCHA CENTRO</option>
     <option value="1" selected>C.H. DE TOLEDO</option>
     <option value="2">C. H. DE CIUDAD REAL</option>
     <option value="3">H. SANTA B�RBARA</option>
     <option value="4">C. H. G. DE ALBACETE</option>
<%
   } else if (centro == 2) {
%>
     <option value="0">C.H. LA MANCHA CENTRO</option>
     <option value="1">C.H. DE TOLEDO</option>
     <option value="2" selected>C. H. DE CIUDAD REAL</option>
     <option value="3">H. SANTA B�RBARA</option>
     <option value="4">C. H. G. DE ALBACETE</option>
<%
   }  else if (centro == 3) {
%>
     <option value="0">C.H. LA MANCHA CENTRO</option>
     <option value="1">C.H. DE TOLEDO</option>
     <option value="2">C. H. DE CIUDAD REAL</option>
     <option value="3" selected>H. SANTA B�RBARA</option>
     <option value="4">C. H. G. DE ALBACETE</option>
<%
   } else if (centro == 4) {
%>
     <option value="0">C.H. LA MANCHA CENTRO</option>
     <option value="1">C.H. DE TOLEDO</option>
     <option value="2">C. H. DE CIUDAD REAL</option>
     <option value="3">H. SANTA B�RBARA</option>
     <option value="4" selected>C. H. G. DE ALBACETE</option>
 <%  } %>
   </Select>
     </td>
</tr>
<tr>
    <td width="20%" class = "datosventana"> Especialidad </td>
    <td width="40%"  class= "datosventana">
       <Select name="especialidad" class="inputformulario9" style="width=90%" onChange="recargar(2);">
<%
       if (centro == 0) {
%>
              <option value="0" <%=marca[0]%>>Alergologia</option>
              <option value="1" <%=marca[1]%>>Cardiologia</option>
              <option value="2" <%=marca[2]%>>Cardiologia intervencionista</option>
              <option value="3" <%=marca[3]%>>Digestivo</option>
              <option value="4" <%=marca[4]%>>Endocrinologia</option>
              <option value="5" <%=marca[5]%>>Hematologia clinica</option>
              <option value="6" <%=marca[6]%>>Medicina Interna</option>
              <option value="7" <%=marca[7]%>>Nefrologia</option>
              <option value="8" <%=marca[8]%>>Neurologia</option>
              <option value="9" <%=marca[9]%>>Oncologia Medica</option>
              <option value="10" <%=marca[10]%>>Reumatologia</option>
              <option value="11" <%=marca[11]%>>Traumatologia</option>
<%      } else if (centro == 1) {
%>
        <option value="0" <%=marca[0]%>>Alergologia</option>
        <option value="1" <%=marca[1]%>>Cardiologia</option>
        <option value="2" <%=marca[2]%>>Digestivo</option>
        <option value="3" <%=marca[3]%>>Endocrinologia</option>
        <option value="4" <%=marca[4]%>>Hematologia clinica</option>
        <option value="5" <%=marca[5]%>>Medicina Interna</option>
        <option value="6" <%=marca[6]%>>Neumologia</option>
        <option value="7" <%=marca[7]%>>Neurologia</option>
        <option value="8" <%=marca[8]%>>Rehabilitacion</option>
        <option value="9" <%=marca[9]%>>Reumatologia</option>
        <option value="10" <%=marca[10]%>>Unidad de lesionados medulares</option>

<%
       }else if (centro == 2) {
%>
        <option value="0" <%=marca[0]%>>Alergologia</option>
        <option value="1" <%=marca[1]%>>Cardiologia</option>
        <option value="2" <%=marca[2]%>>Digestivo</option>
        <option value="3" <%=marca[3]%>>Endocrinologia</option>
        <option value="4" <%=marca[4]%>>Hematologia clinica</option>
        <option value="5" <%=marca[5]%>>Medicina Interna</option>
        <option value="6" <%=marca[6]%>>Nefrologia</option>
        <option value="7" <%=marca[7]%>>Neurologia</option>
        <option value="8" <%=marca[8]%>>Medicina Nuclear</option>
        <option value="9" <%=marca[9]%>>Reumatologia</option>
        <option value="10" <%=marca[10]%>>Geriatria</option>
<%
       }else if (centro == 3) {
%>
        <option value="0" <%=marca[0]%>>Alergologia</option>
        <option value="1" <%=marca[1]%>>Cardiologia</option>
        <option value="2" <%=marca[2]%>>Digestivo</option>
        <option value="3" <%=marca[3]%>>Endocrinologia</option>
        <option value="4" <%=marca[4]%>>Hematologia clinica</option>
        <option value="5" <%=marca[5]%>>Medicina Interna</option>
        <option value="6" <%=marca[6]%>>Medicina Nuclear</option>
        <option value="7" <%=marca[7]%>>Nefrologia</option>
        <option value="8" <%=marca[8]%>>Neurologia</option>
        <option value="9" <%=marca[9]%>>Rehabilitacion</option>
        <option value="10" <%=marca[10]%>>Reumatologia</option>
<%
       }else if (centro == 4) {
%>
        <option value="0" <%=marca[0]%>>Alergologia</option>
        <option value="1" <%=marca[1]%>>Cardiologia</option>
        <option value="2" <%=marca[2]%>>Digestivo</option>
        <option value="3" <%=marca[3]%>>Endocrinologia</option>
        <option value="4" <%=marca[4]%>>Hematologia clinica</option>
        <option value="5" <%=marca[5]%>>Medicina Interna</option>
        <option value="6" <%=marca[6]%>>Neumologia</option>
        <option value="7" <%=marca[7]%>>Neurologia</option>
        <option value="8" <%=marca[8]%>>Rehabilitacion</option>
        <option value="9" <%=marca[9]%>>Reumatologia</option>
<%
       }
%>
      </Select>
     </td>
</tr>
<tr>
    <td width="20%" class = "datosventana"> Servicio </td>
    <td width="40%"  class= "datosventana">
       <Select name="servicio" class="inputformulario9" style="width=90%">
<%
       if ((centro == 0)&&(especialidad == 11)) {
%>
        <option value="0">Traumatolog&iacute;a Hospitalizaci&oacute;n</option>
        <option value="1">Cirug&iacute;a Ortop&eacute;dica Infantil</option>
        <option value="2">Traumatolog&iacute;a CEX</option>

<%
       }else {
%>
        <option value="0">Servicio Generico 1</option>
        <option value="1">Servicio Generico 2</option>
        <option value="2">Servicio Generico 3</option>
<%
       }
%>
        </Select>
     </td>
</tr>
</table>
<td><input type="hidden" name="centroculto" value="<%=centro%>"></td>
<td><input type="hidden" name="permiso" value="<%=permiso%>"></td>
<td><input type="hidden" name="especialidadoculto" value="<%=especialidad%>"></td>
</form>
</body>
</html>