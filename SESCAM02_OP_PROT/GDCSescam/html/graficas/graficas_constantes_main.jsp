<%@ page contentType="text/html;charset=ISO-8859-1"  import="java.util.*,isf.negocio.ConsultaConstantes"%>
<%

String url_imagen = "../../imagenes/no_existen_datos.gif";

String constante = (String) request.getParameter("constante");
String pac = (String) request.getParameter("paciente");
String periodo = (String) request.getParameter("periodo");
String width = (String) request.getParameter("gifWidth");
String height = (String) request.getParameter("gifHeight");

String name_constante = "";
String max_min = "";
Vector v_datos = new Vector();
if (constante != null && !constante.equals("0")) {
        Properties prop_params = new Properties();
        prop_params.setProperty("paciente",pac);
        prop_params.setProperty("periodo",periodo);
        prop_params.setProperty("constante",constante);

        Properties prop_result = ConsultaConstantes.getInstance().getDatosConstantes(prop_params);// consultar los datos a partir del xquery;

        v_datos = (Vector) prop_result.get("datos");
        Properties prop_constante = (Properties) prop_result.get("datos_constante");
        if (!prop_constante.isEmpty())
         name_constante = (String) prop_constante.getProperty("nombre_largo","") + "  "+
         "(" +  (String) prop_constante.getProperty("maximo","") + "-" + (String) prop_constante.getProperty("minimo","") + ")";

        session.setAttribute("beanGrafica",prop_result);
        url_imagen = "../../imagenes/"+constante+".gif";



}


%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../css/estilo.css">
</head>
<BODY class="mainFondo">
<table width="100%" height="100%" style="border-left: 1px solid #002969;border-top: 1px solid #002969" align="center">
<tr height="50%"><td width="40%" valign="top">
 <table  cellspacing="0"  align="center">
   <tr><td align="center" class="textocampo" border="0"><%=name_constante%></TD></tr>
   <tr > <td align="center"> <img name="grafica" border="1" hspace="5" src="<%=url_imagen%>"></td></tr>
 </table>
</td></tr>
<tr><td valign="top">
<table class="busquedaencontrada2" border="1px" cellpadding="0" cellspacing="0" align="center">
<%
if (v_datos.size() >0) {
%>
        <tr class="filapar">
                <td class="colocar" ><b>periodos</b></td>
<%
        for (int i = 0; i < v_datos.size(); i++) {
         String fecha = (String) ((Hashtable) v_datos.elementAt(i)).get("v0");
        %>
    <td class="colocar1"><%=fecha%></td>
<%}%>
        </tr>
<tr class="filaimpar">
                <td class="colocar"><b>valor</b></td>
<%
        for (int i = 0; i < v_datos.size(); i++) {
   String valor = (String) ((Hashtable) v_datos.elementAt(i)).get("v2");
%>
    <td class="colocar1" align="right"><%=valor%></td>
<%}
}%>
</tr>
</table>
</td></tr>
</table>
<table class="fondoblanco" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td class="sololineassuperior4" width="100%">&nbsp;</td>
      <td class="imagen"><img src="../../imagenes/esq_01.gif"></td>
    </tr>
  </table>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
