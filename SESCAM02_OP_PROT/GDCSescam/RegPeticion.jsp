<html>
<head>
<title>Petici�n</title>
<link rel="stylesheet" href="css/style_ex.css" type="text/css">
<script language="JavaScript" src="./javascript/libreriaCapas.js"></script>
<script language="JavaScript" src="./javascript/libreriaPaginasWeb.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

//Coordenadas de la linea base
var bleft = 36;
var btop = 63;

function accionCuerpo( localizacion ){
	//Actualizamos el contenido del iframe
	document.all.item("ifArbol").src = "javascript:accionCuerpo('" + localizacion + "')";
}

var tam_pestana = 150;
function accionCapa( capa ){
	ocultaCapas("Layer1,Layer2,Layer3");
	muestraCapa( capa );
	var pos = parseInt(capa.charAt(capa.length-1)) - 1;
	posicionaCapa( "LayerLinea", bleft + pos * tam_pestana, btop );
}

function accionAceptar(){
		window.alert( "Guardando datos y comprobando si est� autorizado a realizar esta petici�n." );
}

function accionCancelar(){
	window.alert( "Deshaciendo los cambios..." );
}

function accionCitarInformar(){
 if( estado == 0 ){
	if( document.all.item("GrupoOpciones1")[0].checked == true ){
		abreVentanaNueva("Citas");
	}else{
		window.alert( "No se puede citar una exploraci�n sin justificar o no justificada." );
	}
 }else{
 	window.navigate("InformePeticionNo.jsp");
 }
}

function openHistorico() {
  abreVentanaModal("Historico");
}

function openServicios() {
  abreVentanaModal("Servicios");
}

function openGuia() {
}

function openMedicos() {
  abreVentanaModal("Medicos");
}

function openAlarma() {
  abreVentanaModal("Alarmas");
}

function openNota() {
  abreVentanaModal("Observaciones");
}

function openPacientes() {
  abreVentanaModal("Pacientes");
}

function accionRetroceder(){
	window.history.back();
}

function accionAvanzar(){
	window.history.forward();
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function accionTarjeta(){
	abreVentanaModal( "Tarjeta" );
}

var estado = 0;
function accionJustificada(){
  	   MM_swapImage('imgEstadoPeticion','','imagenes/bot_citar.gif',1);
	   estado = 0;
}

function accionNoJustificada(){
  	   MM_swapImage('imgEstadoPeticion','','imagenes/bot_informar.gif',1);
	   estado = 1;
}

function accionSeleccionarEstadoPeticion(){
	if( estado == 0 ){
  	   MM_swapImage('imgEstadoPeticion','','imagenes/bot_citar_sel.gif',1);
	}else{
  	   MM_swapImage('imgEstadoPeticion','','imagenes/bot_informar_sel.gif',1);
	}
}

function accionDeseleccionarEstadoPeticion(){
	if( estado == 0 ){
  	   MM_swapImage('imgEstadoPeticion','','imagenes/bot_citar.gif',1);
	}else{
  	   MM_swapImage('imgEstadoPeticion','','imagenes/bot_informar.gif',1);
	}
}

-->
</script>
</head>
<body rightmargin="0" onLoad="MM_preloadImages('imagenes/bot_aceptar_sel.gif','imagenes/bot_cancelar_sel.gif','imagenes/mashistorial_sel.gif','imagenes/explora_sel.gif','imagenes/informe_sel.gif','imagenes/salir_sel.gif','imagenes/siguiente_sel.gif','imagenes/calendario_sel.gif','imagenes/informa_sel.gif','imagenes/nota_sel.gif','imagenes/Tarxeta_sel.gif','imagenes/citaweb.jpg','imagenes/guia2_sel.gif','imagenes/busca_pacientes_sel.gif','imagenes/alarma_sel.gif')">
<div id="LayerLinea" style="position:absolute; left:36px; top:63px; width:178px; height:16px; z-index:1;"> 
  <table width="148" height="4" border="0" cellpadding="0" cellspacing="0">
       <tr>
		 <td bgcolor="#DAE4F2"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
       </tr>
  </table>
</div>

<div id="Layer1" style="position:absolute; left:38px; top:64px; width:740px; height:120px; z-index:2; visibility: visible;"> 
  <table  border="0" align="left" cellpadding="0" cellspacing="0" width="100%" height="100%" valign="top">
    				  <tr height="18" valign="bottom"> 
                        <td colspan="10" align="left" class="titulo"> 
                          <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                            <tr> 
                              
            <td width="96%" align="right" class="titulo"><a href="#" onClick="MM_callJS('openPacientes()')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image69','','imagenes/busca_pacientes_sel.gif',1)"><img src="imagenes/busca_pacientes.gif" alt="B�squeda de pacientes" name="Image69" width="16" height="16" border="0"></a></td>
                              
            <td width="42" height="16" align="right" class="titulo"><a href="#" onMouseOut="MM_swapImgRestore()" onClick="MM_callJS('openAlarma()')" onMouseOver="MM_swapImage('Image70','','imagenes/alarma_sel.gif',1)"><img src="imagenes/alarma.gif" alt="Alarmas" name="Image70" width="16" height="16" border="0"></a></td>
                            </tr>
                          </table> 
						 </td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="4"> 
						    <table border="0" cellspacing="0" cellpadding="3" align="left" width="326">
          <tr> 
                              <td width="85"  class="standar">NHC:</td>
                              <td width="85" class="standar"> <input name="text" type="text" class="input" size="14" maxlengh="12" > 
                              </td>
                              <td width="32" class="standar">T.I.S.:</td>
                              
            <td class="standar" > <input name="text22" type="text" class="input" size="6" maxlengh="15"></td>
			<td class="standar" >
              <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image41','','imagenes/Tarxeta_sel.gif',1)"><img src="imagenes/Tarxeta.gif" alt="Tarjeta Sanitaria" name="Image41" width="32" height="20" border="0" onClick="MM_callJS('accionTarjeta()')"></a> 
            </td>
                            </tr>
                            <tr> 
                              <td class="standar">Nombre:</td>
                              <td  colspan="4" class="standar"> <em><strong>Jos&eacute; 
                                 Garc&iacute;a Fern&aacute;ndez</strong></em></td>
                            </tr>
                            <tr> 
                              <td  class="standar">F.Nacimiento:</td>
                              <td  colspan="2" class="standar"> <em><strong>25/06/1999</strong></em></td>
                              
            <td width="48" rowspan="3" class="standar">&nbsp;</td>
                              
            <td width="46" rowspan="3" class="standar">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td  class="standar">Tel�fono:</td>
                              <td  colspan="2" class="standar"> <em><strong>645751349</strong></em></td>
                            </tr>
                            <tr> 
                              <td  class="standar">NSS:<strong> </strong></td>
                              <td  colspan="2" class="standar"><strong><em>96325872s</em></strong></td>
                            </tr>
                          </table></td>
                        <td colspan="4"> 
						   <table width="95%" border="0" align="left" cellpadding="0" cellspacing="0" >
          					<tr height="1"> 
                              <td height="1" colspan="3" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                            </tr>
                            <tr> 
                              <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                              <td> <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" >
			                	<tr bgcolor="#ffffff"> 
                                    <td colspan="4" align="left" class="titulo" >HISTORIAL:&nbsp;&nbsp; 
                                      <strong><em>Jos&eacute; Garc&iacute;a Fern&aacute;ndez</em></strong> 
                                    </td>
                                    
                  				<td width="5%" align="left" class="titulo" >
									<a href="#" onClick="MM_callJS('openHistorico()')" onMouseOver="MM_swapImage('Image60','','imagenes/mashistorial_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/mashistorial.gif" alt="Ver m�s historial del paciente" name="Image60" width="16" height="16" border="0"></a>
								</td>
                                  </tr>
                                  <tr height="1"> 
                                    <td height="1" colspan="5" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                                  </tr>
                                  <tr bgcolor="#DAE4F2"> 
                                    <td width="54%" align="left" class="titulo" >Exploraci�n</td>
                                    <td width="20%" align="left" class="titulo">Fecha</td>
                                    <td width="17%"  align="left" class="titulo" >Diagn�stico</td>
                                    <td colspan="2"  align="left" class="titulo" >&nbsp;</td>
                                  </tr>
                                  <tr height="1"> 
                                    <td height="1" colspan="5" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                                  </tr>
                                  <tr> 
                                    <td class="standar" align="left">Torax, P-A</td>
                                    
                  <td class="standar" align="left">10/12/2002</td>
                                    
                  <td class="standar" align="left">3.43</td>
                                    
                  <td width="4%" align="center" class="standar">
				  	<a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image61','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Ver exploraci�n" name="Image61" width="16" height="16" border="0"></a>
				  </td>
                                    
                  <td class="standar" align="center">
				  	<a href="#" onClick="MM_callJS('abreDoc(\'./prueba.doc\')')" onMouseOver="MM_swapImage('Image62','','imagenes/informe_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/informe.gif" alt="Informe" name="Image62" width="16" height="16" border="0"></a>
				  </td>
                                  </tr>
                     			       <tr bgcolor="#ffffff"> 
                                    <td class="standar" align="left">Articulaci�n 
                                      temporomandibular(BA/BC) bilateral</td>
                                    <td class="standar" align="left">26/03/2002</td>
                                    
                  <td class="standar" align="left">4.34</td>
                                    
                  <td class="standar" align="center">
				  		<a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image611','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Ver exploraci�n" name="Image611" width="16" height="16" border="0" id="Image611"></a> 
                  </td>
                                    
                  <td class="standar" align="center">
				  	<a href="#" onClick="MM_callJS('abreDoc(\'./prueba.doc\')')" onMouseOver="MM_swapImage('Image621','','imagenes/informe_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/informe.gif" alt="Informe" name="Image621" width="16" height="16" border="0" id="Image621"></a>
				  </td>
                                  </tr>
                                  <tr> 
                                    <td class="standar" align="left">Transito 
                                      intestinal</td>
                                    <td class="standar" align="left">10/12/2001</td>
                                    <td  align="left" class="titulo" >&nbsp;</td>
                                    
                  <td class="standar" align="center">
				  		<a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image612','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Ver exploraci�n" name="Image612" width="16" height="16" border="0" id="Image612"></a>
					</td>
                                    <td  align="left" class="titulo" >&nbsp;</td>
                                  </tr>
                                </table></td>
                              <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                            </tr>
                            <tr height="1"> 
                              <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
                            </tr>
                          </table></td>
                      </tr>
</table>

</div>
<div id="Layer2" style="position:absolute; left:38px; top:64px; width:740px; height:120px; z-index:3; visibility: hidden;"> 
  <table  border="0" align="left" cellpadding="0" cellspacing="0" width="100%" height="100%" valign="top">
    <tr valign="bottom" height="18" > 
      <td colspan="10" align="left" class="titulo"> <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr> 
            <td align="right" class="titulo"><a href="#" onClick="MM_callJS('openNota()')" onMouseOver="MM_swapImage('Image68','','imagenes/nota_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/nota.gif" alt="Observaciones" name="Image68" width="16" height="16" border="0"></a></td>
          </tr>
        </table></td>
    </tr>
    <tr valign="top"> 
      <td width="54" class="standar">C�digo:</td>
      <td width="85" class="standar"><em><strong>726</strong></em></td>
      <td width="37" class="standar" >Fecha:</td>
      <td width="101" class="standar" valign="top"> <table  border="0" cellpadding="1" cellspacing="0">
          <tr > 
            <td ><input name="txtFecha" type="text" class="input" id="txtFecha"  size="12" maxlengh="10"></td>
            <td><a href="#" onClick="MM_callJS('abreCalendario(\&quot;txtFecha\&quot;)')" onMouseOver="MM_swapImage('Image65','','imagenes/calendario_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/calendario.gif" alt="Calendario" name="Image65" width="16" height="16" border="0"></a></td>
          </tr>
        </table></td>
      <td width="235"  class="standar" >Estado:&nbsp;&nbsp;&nbsp;<em><strong>Pendiente 
        de estudio</strong></em></td>
      <td colspan="4"  class="standar" aling="left" > <input type="checkbox" name="chkEntregado"> 
        &nbsp;Volante Entregado </td>
    </tr>
    <tr> 
      <td class="standar"  >Car�cter:</td>
      <td class="standar" colspan="3"> <input name="optCaracter" type="radio" value="0" checked> 
        &nbsp;Normal&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="optCaracter" value="1"> 
        &nbsp;Preferente</td>
      <td  colspan="5" class="standar"><table border="0" cellspacing="0" cellpadding="1" align="center" width="100%">
          <tr> 
            <td width="10%" class="standar" >Servicio:&nbsp;</td>
            <td width="21%" align="right"> <input name="text5232" type="text" class="input" size="20" maxlengh="15"></td>
            <td width="19%" align="left"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image66','','imagenes/informa_sel.gif',1)"><img src="imagenes/informa.gif" alt="Posibles servicios receptores" name="Image66" width="16" height="16" border="0" onClick="MM_callJS('openServicios()')"></a></td>
            <td width="14%" class="standar" >Facultativo:&nbsp;</td>
            <td width="21%" align="right"><input name="text52222" type="text" class="input" size="20" maxlengh="15"> 
            </td>
            <td width="15%"  align="left"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image66111','','imagenes/informa_sel.gif',1)"><img src="imagenes/informa.gif" alt="Posibles facultativos receptores" name="Image66111" width="16" height="16" border="0" id="Image661" onClick="MM_callJS('openMedicos()')"></a></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td class="standar" colspan="4"> <input type="radio" name="optIngresado" checked="true" value="0" > 
        &nbsp; Pac. Ingresado&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="optIngresado" value="1"> 
        &nbsp;Pac. Ambulante </td>
      <td colspan="6"><table border="0" cellspacing="0" cellpadding="1" align="left">
          <tr> 
            <td width="49" class="standar" id="ubica1">Consulta:</td>
            <td width="13" class="standar"> <input name="text2" type="text" class="input" size="2" maxlengh="15">	
            </td>
            <td width="37" class="standar" id="ubica2">Puerta:</td>
            <td width="13" class="standar" > <input name="text2" type="text" class="input" size="2" maxlengh="15">	
            </td>
            <td width="61" class="standar" id="ubica3">Ubicaci�n:</td>
            <td width="37" class="standar" > <input name="text2" type="text" class="input" size="2" maxlengh="15">	
            </td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td class="titulo" align="left" colspan="4">Direcci�n a la que se env�a 
        el estudio:</td>
      <td colspan="6" align="left" class="titulo"><table border="0" cellspacing="0" cellpadding="1" align="center" width="100%">
          <tr> 
            <td width="10%" class="standar" >Servicio:&nbsp;</td>
            <td width="21%" align="right"><input name="text523" type="text" class="input" size="20" maxlengh="15"></td>
            <td width="19%" align="left"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image67','','imagenes/informa_sel.gif',1)"><img src="imagenes/informa.gif" alt="Posibles servicios receptores" name="Image67" width="16" height="16" border="0" onClick="MM_callJS('openServicios()')"></a></td>
            <td width="14%" class="standar" >Facultativo:&nbsp;</td>
            <td width="21%" align="right"><input name="text5222" type="text" class="input" size="20" maxlengh="15"> 
            </td>
            <td width="15%"  align="left"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6611','','imagenes/informa_sel.gif',1)"><img src="imagenes/informa.gif" alt="Posibles facultativos receptores" name="Image6611" width="16" height="16" border="0" id="Image661" onClick="MM_callJS('openMedicos()')"></a></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td class="titulo" align="left" colspan="4">Traslado del 
        paciente al Servicio de Radiolog�a:</td>
      <td class="standar"> <input type="radio" name="optTraslado" value="0"> 
        &nbsp;A pie &nbsp;&nbsp; <input type="radio" name="optTraslado" value="1"> 
        &nbsp;En silla &nbsp;&nbsp; <input type="radio" name="optTraslado" value="2" > 
        &nbsp;En cama </td>
      <td width="67"  class="standar">Dosis:</td>
      <td width="39"  class="standar"><input name="textfield" type="text" class="input" size="5" maxlength="10"></td>
      <td width="60"  align="center" class="standar">Dosis Eq.:</td>
      <td width="62"  class="standar"><input name="textfield2" type="text" class="input" size="5" maxlength="10"></td>
    </tr>
  </table>
</div>
<div id="Layer3" style="position:absolute; left:38px; top:64px; width:740px; height:120px; z-index:4; visibility: hidden;"> 
  <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td>
	     <table width="100%" height="100%" border="0"  cellpadding="0" cellspacing="0">
  <tr valign="top"><td>
  		<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0"> 
<tr height="18" valign="bottom"> 
		<td align="left" class="titulo"> 
		  <table width="100%" border="0" cellpadding="0" cellspacing="0" >
			<tr> 
                              
            <td width="98%" align="right" class="titulo"><img src="imagenes/guia1.gif" alt="Consulta de gu�a orientativa de justificaci�n de diagn�stico de la imagen" name="Image59" width="16" height="16" border="0"></td>
                              
            <td width="35" height="16" align="right" class="titulo"><a href="#" onClick="MM_callJS('openGuia()')" onMouseOver="MM_swapImage('Image58','','imagenes/guia2_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/guia2.gif" alt="Consulta de gu�a orientativa de justificaci�n de diagn�stico de la imagen" name="Image58" width="16" height="16" border="0"></a></td>
				</tr>
			  </table> 
			 </td>
		  </tr>
                      <tr> 
                        <td colspan="10" class="standar"> <table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr> 
                              <td width="10" class="standar"></td>
                              <td class="standar">Antecedentes:</td>
                              <td width="10" class="standar"></td>
                              <td class="standar" >Sospecha diagn�stica:</td>
                            </tr>
                            <tr> 
                              <td width="10" class="standar"></td>
                              <td><textarea name="textarea" cols="65"  rows="7" class="input"></textarea></td>
                              <td width="10" class="standar"></td>
                              <td><textarea name="textarea2" cols="65"  rows="7" class="input"></textarea></td>
                            </tr>
                          </table></td>
                      </tr>
	</table>
</td></tr></table>
	  </td>
    </tr>
  </table>
</div>
  <!-- Inicio Marco -->
  
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
      <td width="25" height="24">
   	      <img src="imagenes/Circunfai-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
      <td height="24" width="100%" bgcolor="#DAE4F2"></td>
      <td width="25" height="24">
	  	  <img src="imagenes/Circunfad-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
    </tr>
    <tr valign="top" height="100%">
      
    <td width="25" bgcolor="#DAE4F2"></td>
      
    <td width="100%" bgcolor="#DAE4F2"> 
      <!-- Fin Marco -->
      <table width="750" height="160" border="0" cellpadding="0" cellspacing="0">
  <tr height="25">
    <td width="150" colspan="2">
		<table width="150" border="0" cellspacing="0" cellpadding="0" bordercolor="#000000">
		  <tr>
		    <td><img src="imagenes/Circunfai-p2.gif" alt="" width="25" height="25" border="0"></td>
		    <td>
			<table width="100" height="25" border="0" cellpadding="0" cellspacing="0" bgcolor="#DAE4F2" valign="top">
		        <tr height="1%">
				 <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
		        </tr>
		        <tr height="99%"> 
		          <td bgcolor="#DAE4F2" align="center" class="titulo" onclick="accionCapa('Layer1')">Datos Paciente</td>
		        </tr>
		     </table>
			 </td>
		    <td><img src="imagenes/Circunfad-p2.gif" alt="" width="25" height="25" border="0"></td>
		  </tr>
		</table>
	</td>
    <td width="150">
		<table width="150" border="0" cellspacing="0" cellpadding="0" >
		  <tr>
		    <td><img src="imagenes/Circunfai-p2.gif" alt="" width="25" height="25" border="0"></td>
		    <td>
			<table width="100" height="25" border="0" cellpadding="0" cellspacing="0" bgcolor="#DAE4F2" valign="top">
		        <tr height="1%">
				 <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
		        </tr>
		        <tr height="99%"> 
		          <td bgcolor="#DAE4F2" align="center" class="titulo" onclick="accionCapa('Layer2')">Datos Petici�n</td>
		        </tr>
		     </table>
			 </td>
		    <td><img src="imagenes/Circunfad-p2.gif" alt="" width="25" height="25" border="0"></td>
		  </tr>
		</table>
	</td>
    <td width="150">
		<table width="150" border="0" cellspacing="0" cellpadding="0" bordercolor="#000000">
		  <tr>
		    <td><img src="imagenes/Circunfai-p2.gif" alt="" width="25" height="25" border="0"></td>
		    <td>
			<table width="100" height="25" border="0" cellpadding="0" cellspacing="0" bgcolor="#DAE4F2" valign="top">
		        <tr height="1%">
				 <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
		        </tr>
		        <tr height="99%"> 
		          <td bgcolor="#DAE4F2" align="center" class="titulo" onclick="accionCapa('Layer3')">Justificaci�n de la Exploraci�n</td>
		        </tr>
		     </table>
			 </td>
		    <td><img src="imagenes/Circunfad-p2.gif" alt="" width="25" height="25" border="0"></td>
		  </tr>
		</table>
	</td>
	<td colspan="2"></td>
  </tr>
  <tr height="1">
 	<td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="1"></td>
 	<td width="149" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="1"></td>
 	<td bgcolor="#002969" colspan="4"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
  </tr>
  <tr> 
	<td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="1"></td>
  	<td colspan="4" bgcolor="#DAE4F2">
	
	<!--Contenido-->
	</td>
    <td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="1"></td>
  </tr>
  <tr height="1">
 	 <td bgcolor="#002969" colspan="7"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
  </tr>
</table>
      <table width="750" height="170" border="0" cellspacing="0" cellpadding="0">
        <tr height="1">
 	    <td bgcolor="#002969" colspan="4"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
    </tr>
	<tr height="20">
 	    <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
		<td align="center" class="titulo"></td>
		<td align="center" class="titulo">ARBOL DE EXPLORACIONES</td>
 	    <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
	</tr>
    <tr height="1">
 	    <td bgcolor="#002969" colspan="4"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
    </tr>
	<tr>
	  <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
	      <td width="300" align="left"> <img src="imagenes/MapaCuerpo.jpg" width="299" height="150" border="0" usemap="#Map"> 
            <map name="Map">
              <area shape="rect" coords="41,89,94,103" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Genitales\&quot;)')">
              <area shape="circle" coords="67,15,13" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Cabeza\&quot;)')">
              <area shape="circle" coords="158,84,13" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Cabeza\&quot;)')">
              <area shape="circle" coords="239,24,13" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Cabeza\&quot;)')">
              <area shape="rect" coords="40,69,94,88" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Estomago\&quot;)')">
              <area shape="rect" coords="212,94,265,108" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Genitales\&quot;)')">
              <area shape="rect" coords="142,115,172,122" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Genitales\&quot;)')">
              <area shape="rect" coords="212,75,266,93" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Estomago\&quot;)')">
              <area shape="rect" coords="30,33,106,68" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Torax\&quot;)')">
              <area shape="rect" coords="138,107,176,115" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Estomago\&quot;)')">
              <area shape="rect" coords="201,39,277,75" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Torax\&quot;)')">
              <area shape="rect" coords="125,99,190,107" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Torax\&quot;)')">
            </map></td>
 	  <td>
	  	<iframe name="ifArbol" id="ifArbol" src="ArbolExploraciones.jsp" width="100%" height="100%">
		</iframe>
	  </td>
 	  <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
	</tr>
    <tr height="1">
 	    <td bgcolor="#002969" colspan="4"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
    </tr>
</table>
      <table width="750" height="50" border="0" cellspacing="0" cellpadding="0">
        <tr height="1"> 
          <td bgcolor="#002969" colspan="7"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
        </tr>
        <tr valign="middle"> 
          <td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
          <td width="206">&nbsp; </td>
          <td width="73">&nbsp;</td>
          <td width="319"></td>
          <td " width="75"><a href="#" onClick="MM_callJS('accionAceptar()')" onMouseOver="MM_swapImage('Image56','','imagenes/bot_aceptar_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/bot_aceptar.gif"  name="Image56" width="67" height="19" border="0"></a></td>
          <td width="75"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image57','','imagenes/bot_cancelar_sel.gif',1)"><img src="imagenes/bot_cancelar.gif" alt="Cancelar" name="Image57" width="67" height="19" border="0" onClick="MM_callJS('accionCancelar()')"></a></td>
          <td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
        </tr>
        <tr height="1"> 
          <td bgcolor="#002969" colspan="7"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
        </tr>
		<tr height="10"> 
          <td  colspan="7"></td>
        </tr> 
        <tr> 
            <td colspan="7" align="right" > <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                <tr> 
                  
                <td width="95%" align="right"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image63','','imagenes/salir_sel.gif',1)"><img src="imagenes/salir.gif" alt="Volver" name="Image63" width="32" height="32" border="0" onClick="MM_callJS('accionRetroceder()')"></a></td>
                <td width="5%"  align="right"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image64','','imagenes/siguiente_sel.gif',1)"><img src="imagenes/siguiente.gif" alt="Adelante" name="Image64" width="32" height="32" border="0" onClick="MM_callJS('accionAvanzar()')"></a></td>
                </tr>
              </table></td>
          </tr>
      </table>
    </td>
      
    <td width="25" bgcolor="#DAE4F2"></td>
    </tr>
    <tr>
      <td width="25">
	  	  <img src="imagenes/Circunfabi-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
      
    <td width="100%" height="24" bgcolor="#DAE4F2"></td>
      <td height="24">
	  	  <img src="imagenes/Circunfabd-p.gif" border="0" width="25" height="100%" alt=""> 
	  </td>
    </tr>
</table>
  <!-- Fin Marco -->
</body>
</html>
