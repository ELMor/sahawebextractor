<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/style_ex.css" type="text/css">
<script language="JavaScript" src="./javascript/libreriaCapas.js"></script>
<script language="JavaScript" src="./javascript/libreriaPaginasWeb.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('imagenes/explora_sel.gif','imagenes/informe_sel.gif')">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td width="44%" align="left" class="standar">TORAX, P-A</td>
                    
    <td width="23%" align="left" class="standar">10/12/2002</td>
                    
    <td width="17%" align="left" class="standar">3.43</td>
		    
    <td width="8%" align="center" class="standar"><a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image12','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Imagen asociada" name="Image12" width="20" height="20" border="0"></a></td>
		    
<!--    <td width="8%" align="center" class="standar"> <img src="imagenes/informe.gif" border="0" width="32" height="32" onClick="exec('file:///C|/Archivos%20de%20programa/Microsoft%20Office/Office/WINWORD.EXE')"></td>-->
    <td width="8%" align="center" class="standar"><a href="#" onClick="MM_callJS('abreDoc(\'./prueba.doc\')')" onMouseOver="MM_swapImage('Image9','','imagenes/informe_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/informe.gif" alt="Informe escrito" name="Image9" width="16" height="16" border="0"></a> 
    </td>
                  </tr>
                  <tr bgcolor="#dae4f2">
                    <td class="standar" align="left">ARTICULACION TEMPOROMANDIBULAR (BA/BC) BILATERAL</td>
                    
    <td class="standar" align="left">26/08/2002</td>
                    
    <td class="standar" align="left">&nbsp;</td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image121','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Imagen asociada" name="Image121" width="20" height="20" border="0" id="Image121"></a></td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreDoc(\'./prueba.doc\')')" onMouseOver="MM_swapImage('Image91','','imagenes/informe_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/informe.gif" alt="Informe escrito" name="Image91" width="16" height="16" border="0" id="Image91"></a></td>
                  </tr>
                  <tr>
                    <td class="standar" align="left">TRANSITO INTESTINAL</td>
                    
    <td class="standar" align="left">10/07/2002</td>
                    
    <td class="standar" align="left">4.34</td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image122','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Imagen asociada" name="Image122" width="20" height="20" border="0" id="Image122"></a></td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreDoc(\'./prueba.doc\')')" onMouseOver="MM_swapImage('Image92','','imagenes/informe_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/informe.gif" alt="Informe escrito" name="Image92" width="16" height="16" border="0" id="Image92"></a></td>
                  </tr>
				                    <tr bgcolor="#dae4f2">
                    
    <td class="standar" align="left">CLAVICULA</td>
    <td class="standar" align="left">15/12/2001</td>
                    
    <td class="standar" align="left"></td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image123','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Imagen asociada" name="Image123" width="20" height="20" border="0" id="Image123"></a></td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreDoc(\'./prueba.doc\')')" onMouseOver="MM_swapImage('Image93','','imagenes/informe_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/informe.gif" alt="Informe escrito" name="Image93" width="16" height="16" border="0" id="Image93"></a></td>
                  </tr>
				                    <tr>
                    
    <td class="standar" align="left">HOMBRO, 2 PROYECCIONES</td>
                    <td class="standar" align="left">10/12/2001</td>
                    <td class="standar" align="left"></td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image124','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Imagen asociada" name="Image124" width="20" height="20" border="0" id="Image124"></a></td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreDoc(\'./prueba.doc\')')" onMouseOver="MM_swapImage('Image94','','imagenes/informe_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/informe.gif" alt="Informe escrito" name="Image94" width="16" height="16" border="0" id="Image94"></a> 
    </td>
                  </tr>
				                    <tr bgcolor="#dae4f2">
                    
    <td class="standar" align="left">ARTROGRAFIA DE HOMBRO</td>
    <td class="standar" align="left">10/11/2001</td>
                    
    <td class="standar" align="left">41.23</td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreVentanaNueva(\&quot;ImagenDicom\&quot;)')" onMouseOver="MM_swapImage('Image125','','imagenes/explora_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/explora.gif" alt="Imagen asociada" name="Image125" width="20" height="20" border="0" id="Image125"></a></td>
		    
    <td class="standar" align="center"><a href="#" onClick="MM_callJS('abreDoc(\'./prueba.doc\')')" onMouseOver="MM_swapImage('Image95','','imagenes/informe_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/informe.gif" alt="Informe escrito" name="Image95" width="16" height="16" border="0" id="Image95"></a></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
        <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr height="1">
        <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
	  </table>
</body>
</html>
