<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:template match="/">
		<html>
			<head/>
			<body>
				<br/>
				<xsl:for-each select="InformeEpicrisis">
					<xsl:for-each select="Ingreso">
						<h2 align="center">
							<span style="color:#002969">Informe de Epicrisis</span>
						</h2>
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr bgcolor="#DAE4F2">
									<td align="left" colspan="2" width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">DATOS INGRESO.</span>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Fecha/Hora ingreso:</span>
									</td>
									<td>
										<xsl:for-each select="Fecha">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">&#160;</span>
										<xsl:for-each select="Hora">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Origen Internación:</span>
									</td>
									<td>
										<xsl:for-each select="Origen">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Tipo Ingreso:</span>
									</td>
									<td>
										<xsl:for-each select="Tipo">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Motivo Consulta:</span>
									</td>
									<td>
										<xsl:for-each select="MotivoConsulta">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Motivo Internación:</span>
									</td>
									<td>
										<xsl:for-each select="MotivoInternacion">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Referido Por Servicio:</span>
									</td>
									<td>
										<xsl:for-each select="Servicio">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Med. Solicitante:</span>
									</td>
									<td>
										<xsl:for-each select="Profesional">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Financiador</span>
									</td>
									<td>
										<xsl:for-each select="Financiador">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Res</span>
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">p. de Pago:</span>
									</td>
									<td>
										<xsl:for-each select="Pago">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="179">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Diagnóstico Ingre</span>
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">so</span>
									</td>
									<td>
										<xsl:for-each select="Diagnostico">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
							</tbody>
						</table>
					</xsl:for-each>
					<br/>
					<xsl:for-each select="Egreso">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr bgcolor="#DAE4F2">
									<td align="left" colspan="4">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">DATOS EGRESO.</span>
									</td>
								</tr>
								<tr>
									<td width="112">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Fecha Egreso:</span>
									</td>
									<td width="103">
										<xsl:for-each select="Fecha">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
									<td width="89">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bolder">Hora Egreso:</span>
									</td>
									<td>
										<xsl:for-each select="Hora">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="112">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Días Estada:</span>
									</td>
									<td colspan="3" width="103">
										<xsl:for-each select="Estada">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="112">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Destino:</span>
									</td>
									<td colspan="3" width="103">
										<xsl:for-each select="Destino">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
								<tr>
									<td width="112">
										<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold">Condición:</span>
									</td>
									<td colspan="3" width="103">
										<xsl:for-each select="Condicion">
											<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px">
												<xsl:apply-templates/>
											</span>
										</xsl:for-each>
									</td>
								</tr>
							</tbody>
						</table>
					</xsl:for-each>
					<br/>
					<xsl:for-each select="Conclusion">
						<br/>
						<span style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold">Conclusión:</span>
						<br/>
						<textarea style="color:#002969; font-family:Verdana, Geneva, Arial, Helvetica, sans-serif; font-size:11px" cols="100" rows="5">
							<xsl:value-of select="."/>
						</textarea>
						<br/>
					</xsl:for-each>
					<br/>
				</xsl:for-each>
				<br/>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="Derivacion">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="Diagnostico">
		<xsl:apply-templates/>
	</xsl:template>
</xsl:stylesheet>
