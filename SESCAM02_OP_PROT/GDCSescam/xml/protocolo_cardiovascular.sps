<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="protocolo_cardiovascular.xsd" workingxmlfile="protocolo_cardiovscular.xml" templatexmlfile="">
	<nspair prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<template>
				<match match="protocolo_cardiovascular"/>
				<children>
					<paragraph paragraphtag="p">
						<children>
							<template>
								<match match="soip_antecedentes"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#004080"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="S.O.I.P. y Antecedentes">
																				<styles color="#FFFFFF" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
											<template>
												<match match="soip"/>
												<children>
													<paragraph paragraphtag="p">
														<children>
															<text fixtext="Sintomas:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
															<newline/>
															<template>
																<match match="sintomas"/>
																<children>
																	<multilinefield ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties cols="75" rows="5"/>
																	</multilinefield>
																</children>
															</template>
															<newline/>
															<newline/>
															<text fixtext="Exploración:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
															<newline/>
															<template>
																<match match="exploracion"/>
																<children>
																	<multilinefield ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties cols="75" rows="5"/>
																	</multilinefield>
																</children>
															</template>
															<newline/>
															<newline/>
															<text fixtext="Impresión:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
															<newline/>
															<template>
																<match match="impresion"/>
																<children>
																	<multilinefield ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties cols="75" rows="5"/>
																	</multilinefield>
																</children>
															</template>
															<newline/>
															<newline/>
															<text fixtext="Plan:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
															<newline/>
															<template>
																<match match="plan"/>
																<children>
																	<multilinefield ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties cols="75" rows="5"/>
																	</multilinefield>
																</children>
															</template>
															<newline/>
															<newline/>
															<newline/>
														</children>
													</paragraph>
												</children>
											</template>
											<newline/>
											<template>
												<match match="antecedentes"/>
												<children>
													<paragraph paragraphtag="p">
														<children>
															<template>
																<match match="familiares"/>
																<children>
																	<paragraph paragraphtag="p">
																		<children>
																			<table>
																				<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
																				<children>
																					<tablebody>
																						<children>
																							<tablerow>
																								<properties bgcolor="#DAE4F2"/>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Antecedentes familiares">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol/>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="HTA:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="hta"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Dislipemia:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="dislipemia"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Obesidad:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="obesidad"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Diabetes:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="diabetes"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Enf. CV:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="enf_cv"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																						</children>
																					</tablebody>
																				</children>
																			</table>
																		</children>
																	</paragraph>
																</children>
															</template>
															<newline/>
															<template>
																<match match="personales"/>
																<children>
																	<paragraph paragraphtag="p">
																		<children>
																			<table>
																				<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
																				<children>
																					<tablebody>
																						<children>
																							<tablerow>
																								<properties bgcolor="#DAE4F2"/>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Antecedentes familiares">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol/>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="HTA:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="hta"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Dislipemia:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="dislipemia"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Obesidad:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="obesidad"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Diabetes:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="diabetes"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Card. Isquémica:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="card_isquemica"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Enf. Vas.Cerebral:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="enf_vas_cerebral"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Enf. Vas. Periferic:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="enf_vas_periferic"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																							<tablerow>
																								<children>
																									<tablecol>
																										<children>
																											<text fixtext="Insuf. Cardíaca:">
																												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																											</text>
																										</children>
																									</tablecol>
																									<tablecol>
																										<children>
																											<template>
																												<match match="insuf_cardiaca"/>
																												<children>
																													<select ownvalue="1">
																														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																														<properties size="0"/>
																														<selectoption description="Si" value="si"/>
																														<selectoption description="No" value="no"/>
																													</select>
																												</children>
																											</template>
																										</children>
																									</tablecol>
																								</children>
																							</tablerow>
																						</children>
																					</tablebody>
																				</children>
																			</table>
																		</children>
																	</paragraph>
																</children>
															</template>
														</children>
													</paragraph>
												</children>
											</template>
											<newline/>
											<template>
												<match match="riesgo_cv"/>
												<children>
													<paragraph paragraphtag="p">
														<children>
															<table>
																<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
																<children>
																	<tablebody>
																		<children>
																			<tablerow>
																				<properties bgcolor="#DAE4F2"/>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Riesgo CV">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol/>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<template>
																								<match match="puntuacion_check"/>
																								<children>
																									<checkbox ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																										<properties type="checkbox"/>
																									</checkbox>
																								</children>
																							</template>
																							<text fixtext="Puntuación">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																							<text fixtext=":">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="puntuacion"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Riesgo, grado:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="riesgo_grado"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																										<selectoption description="                " value=""/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<template>
																								<match match="probabilidad_check"/>
																								<children>
																									<checkbox ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																										<properties type="checkbox"/>
																									</checkbox>
																								</children>
																							</template>
																							<text fixtext="Probabilidad (%):">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="probabilidad"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																		</children>
																	</tablebody>
																</children>
															</table>
														</children>
													</paragraph>
												</children>
											</template>
											<newline/>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="En programa:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="en_programa"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="Si" value="si"/>
																						<selectoption description="No" value="no"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="numero"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="entrevista"/>
								<children>
									<newline/>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#004080"/>
														<children>
															<tablecol>
																<children>
																	<text fixtext="Entrevista">
																		<styles color="#FFFFFF" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
									<newline/>
									<newline/>
									<paragraph paragraphtag="p">
										<children>
											<template>
												<match match="habitos"/>
												<children>
													<paragraph paragraphtag="p">
														<children>
															<table>
																<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
																<children>
																	<tablebody>
																		<children>
																			<tablerow>
																				<properties bgcolor="#DAE4F2"/>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Hábitos">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol/>
																					<tablecol/>
																					<tablecol/>
																					<tablecol/>
																					<tablecol/>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Tabaco:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="tabaco"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																										<selectoption description="Si" value="si"/>
																										<selectoption description="No" value="no"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<text fixtext="Cigarrillos/día:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="cigarrillos_dia"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="3"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<text fixtext="Ex-fumador desde:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="ex_fumador_desde"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="10"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Alcohol:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="alcohol"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																										<selectoption description="Si" value="si"/>
																										<selectoption description="No" value="no"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<text fixtext="UU/semana:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="uu_semana"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="3"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<text fixtext="Gramos/semana:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="gramos_semana"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="10"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<properties align="right" colspan="4"/>
																						<children>
																							<text fixtext="Valor de la unidad de alcohol:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<properties colspan="2"/>
																						<children>
																							<template>
																								<match match="valor_unidades"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<properties colspan="2"/>
																						<children>
																							<text fixtext="Drogas:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<properties colspan="4"/>
																						<children>
																							<template>
																								<match match="tipo_droga"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																		</children>
																	</tablebody>
																</children>
															</table>
														</children>
													</paragraph>
												</children>
											</template>
											<newline/>
											<template>
												<match match="sintomas"/>
												<children>
													<paragraph paragraphtag="p">
														<children>
															<table>
																<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
																<children>
																	<tablebody>
																		<children>
																			<tablerow>
																				<properties bgcolor="#DAE4F2"/>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Sintomas">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol/>
																					<tablecol/>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Dolor precord.:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="dolor_precord"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																										<selectoption description="Si" value="si"/>
																										<selectoption description="No" value="no"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="tipo_dolor_precord"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Disnea:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<properties colspan="2"/>
																						<children>
																							<template>
																								<match match="disnea"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																										<selectoption description="Gr I: Grandes esfuerzos" value="1"/>
																										<selectoption description="Gr II: Moderados esfuerzos" value="2"/>
																										<selectoption description="Gr III: Pequeños esfuerzos" value="3"/>
																										<selectoption description="Gr IV: Disnea reposo" value="4"/>
																										<selectoption description="No" value="no"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Claudic. interm.:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="claudic_interm"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																										<selectoption description="Si" value="si"/>
																										<selectoption description="No" value="no"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="tipo_claudic_interm"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Sint. Hipogluc.:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="sint_hipogluc"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																										<selectoption description="Si" value="si"/>
																										<selectoption description="No" value="no"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="tipo_sint_hipogluc"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Neuropatia:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="neuropatia"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																										<selectoption description="Si" value="si"/>
																										<selectoption description="No" value="no"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="tipo_neuropatia"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																		</children>
																	</tablebody>
																</children>
															</table>
														</children>
													</paragraph>
												</children>
											</template>
											<newline/>
											<template>
												<match match="adherencia_al_tratamiento"/>
												<children>
													<paragraph paragraphtag="p">
														<children>
															<table>
																<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
																<children>
																	<tablebody>
																		<children>
																			<tablerow>
																				<properties bgcolor="#DAE4F2"/>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Adherencia al tratamiento">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol/>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Dieta:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="dieta"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Ejercicios:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="ejercicios"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Farmacos:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="farmacos"/>
																								<children>
																									<select ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="0"/>
																									</select>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																		</children>
																	</tablebody>
																</children>
															</table>
														</children>
													</paragraph>
												</children>
											</template>
											<newline/>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Autocontrol:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="autocontrol"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="Si" value="si"/>
																						<selectoption description="No" value="no"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="tipo_auto_control"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="exploracion"/>
								<children>
									<newline/>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#004080"/>
														<children>
															<tablecol>
																<children>
																	<text fixtext="Exploracion">
																		<styles color="#FFFFFF" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
									<newline/>
									<newline/>
									<paragraph paragraphtag="p">
										<children>
											<template>
												<match match="constantes"/>
												<children>
													<paragraph paragraphtag="p">
														<children>
															<table>
																<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
																<children>
																	<tablebody>
																		<children>
																			<tablerow>
																				<properties bgcolor="#DAE4F2"/>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Constantes">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol/>
																					<tablecol/>
																					<tablecol/>
																					<tablecol/>
																					<tablecol/>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Peso:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="peso"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="5"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<text fixtext="Talla:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="talla"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="5"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<text fixtext="I.M.C:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="imc"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="5"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="T.A.S.:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="tas"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="5"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<text fixtext="T.A.D.:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="tad"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="5"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<text fixtext="Fr. Cardiac:">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="fr_cardiaca"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties size="5"/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																		</children>
																	</tablebody>
																</children>
															</table>
														</children>
													</paragraph>
												</children>
											</template>
											<newline/>
											<template>
												<match match="peso_ideal"/>
												<children>
													<paragraph paragraphtag="p">
														<children>
															<table>
																<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
																<children>
																	<tablebody>
																		<children>
																			<tablerow>
																				<properties bgcolor="#DAE4F2"/>
																				<children>
																					<tablecol>
																						<children>
																							<text fixtext="Peso ideal">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																							</text>
																						</children>
																					</tablecol>
																					<tablecol/>
																					<tablecol/>
																				</children>
																			</tablerow>
																			<tablerow>
																				<children>
																					<tablecol>
																						<children>
																							<template>
																								<match match="campo1"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="campo2"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																					<tablecol>
																						<children>
																							<template>
																								<match match="campo3"/>
																								<children>
																									<field ownvalue="1">
																										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																										<properties value=""/>
																									</field>
																								</children>
																							</template>
																						</children>
																					</tablecol>
																				</children>
																			</tablerow>
																		</children>
																	</tablebody>
																</children>
															</table>
														</children>
													</paragraph>
												</children>
											</template>
											<newline/>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Piel:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="piel"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_piel"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Cabeza:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="cabeza"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_cabeza"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Cuello:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="cuello"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_cuello"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="A.C.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ac"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_ac"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="A.P.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ap"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_ap"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Mamas:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="mamas"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_mamas"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="ABD:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="abd"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_abd"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="MMII:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="mmii"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_mmii"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Pies:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="pies"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_pies"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Expl. neurolig.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="expl_neurolog"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_expl_neurolog"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Tacto rectal:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="tacto_rectal"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_tacto_rectal"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="analitica"/>
								<children>
									<newline/>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#004080"/>
														<children>
															<tablecol>
																<children>
																	<text fixtext="Analitica">
																		<styles color="#FFFFFF" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
									<newline/>
									<newline/>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="VSG:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="vsg"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="Leuc:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="leuc"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="Plaq:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="plaq"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Hb:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hb"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="Hto:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hto"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="VCM:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="vcm"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Conversor Glucemia Capilar-Plasmática:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="conversor_glucemia"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_conversor_glucemia"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="HbGl:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hbgl"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="TSOG:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="tsog"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="GADe:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="gade"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="GDDe:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="gdde"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="GACo:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="gaco"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="GDCo:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="gdco"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="GACe:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="gace"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="GDCe:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="gdce"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="CT:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ct"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="TG:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="tg"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hdl_check"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext="HDL:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hdl"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="LDL:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ldl"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="CT/HDL:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ct_hdl"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="CPK:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="cpk"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="GOT:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="got"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="GPT:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="gpt"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="GGT:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ggt"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="BT:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="bt"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="BDi:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="bdi"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="FA:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="fa"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Crea:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="crea"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="Urea:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="urea"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="AcU:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="acu"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Na+:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="na"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="K+:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="k"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="TSH:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="tsh"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="T4:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="t4"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="PSA:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="psa"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="AFP:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="afp"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="mam_check"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext="MAM:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="mam"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="pro2_check"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext="Pro2:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="pro2"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="5"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="complementarias_papps"/>
								<children>
									<newline/>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#004080"/>
														<children>
															<tablecol>
																<children>
																	<text fixtext="Complementarias y PAPPS">
																		<styles color="#FFFFFF" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
									<newline/>
									<newline/>
									<template>
										<match match="complementarias"/>
										<children>
											<paragraph paragraphtag="p">
												<children>
													<table>
														<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
														<children>
															<tablebody>
																<children>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="F de ollo:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="f_de_ollo"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="campo_f_de_ollo"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="ECG:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="ecg"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="campo_ecg"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Rx Tórax:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="rx_torax"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="campo_rx_torax"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Ergometría:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="ergometria"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="campo_ergometria"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Ecocardio:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="ecocardio"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="campo_ecocardio"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Doppler:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="doppler"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="campo_doppler_1_check"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<template>
																						<match match="campo_doppler_1"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="campo_doppler_2_check"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<template>
																						<match match="campo_doppler_2"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																</children>
															</tablebody>
														</children>
													</table>
													<table>
														<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
														<children>
															<tablebody>
																<children>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="protein_check"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext="Protein:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="protein"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<text fixtext="Sedimento:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="sedimento"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="campo_sedimento"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Urocultivo:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="urocultivo"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																								<selectoption description="Contaminado" value="contaminado"/>
																								<selectoption description="Negativo" value="negativo"/>
																								<selectoption description="Non" value="non"/>
																								<selectoption description="Positivo" value="positivo"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol/>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="campo_urocultivo"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																</children>
															</tablebody>
														</children>
													</table>
												</children>
											</paragraph>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="papps"/>
										<children>
											<paragraph paragraphtag="p">
												<children>
													<table>
														<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
														<children>
															<tablebody>
																<children>
																	<tablerow>
																		<properties bgcolor="#DAE4F2"/>
																		<children>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<text fixtext="Papps">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<text fixtext="Mamografía:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="mamografia"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="campo_mamografia"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<text fixtext="Cit. cervix:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="cit_cervix"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="campo_cit_cervix"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<text fixtext="Mét. anticinc.:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<properties colspan="2"/>
																				<children>
																					<template>
																						<match match="met_anticonc"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<text fixtext="V. Gripe:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="v_gripe"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Edad Menop:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="edad_menop"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="5"/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<text fixtext="Histerectomía:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="histerectomia"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<text fixtext="Sangrado Post-M.:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol>
																				<children>
																					<template>
																						<match match="sangrado_post_m"/>
																						<children>
																							<select ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties size="0"/>
																							</select>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																</children>
															</tablebody>
														</children>
													</table>
												</children>
											</paragraph>
										</children>
									</template>
									<paragraph paragraphtag="p"/>
								</children>
							</template>
							<newline/>
							<template>
								<match match="educacion_sanitaria"/>
								<children>
									<newline/>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#004080"/>
														<children>
															<tablecol>
																<children>
																	<text fixtext="Educacion Sanitaria">
																		<styles color="#FFFFFF" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
									<newline/>
									<newline/>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties colspan="3"/>
																		<children>
																			<text fixtext="Temas Educación Sanitaria ¿realizado?">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="(*) Solo para diabeticos">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Inf. enferm.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="inf_enferm"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="*Hipoglucem:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hipoglucem"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Dieta:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="dieta"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="Ejercicio:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ejercicio"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Normopeso:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="normopeso"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="Farmacos:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="farmacos"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="*Auto-inyecc.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="autoinyecc"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="*Autoanalisis:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="autoanalisis"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Higiene gral:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="higiene_gral"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="Higiene pies:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="higiene_pies"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Complicac.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="complicac"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="Expl. Compl.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="expl_compl"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="*Hipergluc:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hipergluc"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<text fixtext="*Enf. interc.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="enf_interc"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Otros F.R.:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="otros_f_r"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																						<selectoption description="No" value="no"/>
																						<selectoption description="No procede" value="no_procede"/>
																						<selectoption description="Si" value="si"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol/>
																	<tablecol/>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Nivel EpS:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties colspan="2"/>
																		<children>
																			<template>
																				<match match="nivel_eps"/>
																				<children>
																					<select ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="0"/>
																					</select>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Código enfermería:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="codigo_enfermeria"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="campo_codigo_enfermeria"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
						</children>
					</paragraph>
				</children>
			</template>
			<newline/>
		</children>
	</template>
</structure>
