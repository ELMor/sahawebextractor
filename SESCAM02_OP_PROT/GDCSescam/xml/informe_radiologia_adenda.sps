<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="informe_radiologia_adenda.xsd" workingxmlfile="informe_radiologia_adenda.xml" templatexmlfile="">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<paragraph paragraphtag="center">
				<children>
					<text fixtext="SERVICIO DE RADIODIAGNOSTICO">
						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="large"/>
					</text>
				</children>
			</paragraph>
			<newline/>
			<newline/>
			<template>
				<match match="informe_radiologia"/>
				<children>
					<paragraph paragraphtag="p">
						<children>
							<template>
								<match match="datos_paciente"/>
								<children>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#DAE4F2"/>
														<children>
															<tablecol>
																<properties align="left" colspan="2" width="140"/>
																<children>
																	<text fixtext="Datos paciente">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Nombre:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="nombre"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Sexo:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="sexo"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Fecha de nacimiento:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="fecha_nacimiento"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Edad:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="edad"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Historia clínica:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="historia_clinica"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="T.I.S.">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="tis"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
								</children>
							</template>
							<newline/>
							<template>
								<match match="datos_solicitante"/>
								<children>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#DAE4F2"/>
														<children>
															<tablecol>
																<properties align="left" colspan="2" width="140"/>
																<children>
																	<text fixtext="Datos médicos">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Médico solicitante:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="medico_solicitante"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Servicio solicitante:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="servicio_solicitante"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
								</children>
							</template>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Exploraciones:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																<match match="exploraciones"/>
																<children>
																	<xpath allchildren="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</xpath>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
							<newline/>
							<paragraph paragraphtag="center">
								<children>
									<text fixtext="INFORME RADIOLOGICO">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="medium"/>
									</text>
								</children>
							</paragraph>
							<newline/>
							<newline/>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="informe"/>
								<children>
									<xpath allchildren="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<newline/>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Fecha diagnóstico:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																<match match="fecha_diagnostico"/>
																<children>
																	<xpath allchildren="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</xpath>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Firmado:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																<match match="doctor"/>
																<children>
																	<xpath allchildren="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</xpath>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
							<newline/>
							<newline/>
							<text fixtext="Adenda:">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<newline/>
							<template>
								<match match="adenda"/>
								<children>
									<multilinefield ownvalue="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
										<properties cols="75" rows="5"/>
									</multilinefield>
								</children>
							</template>
						</children>
					</paragraph>
				</children>
			</template>
			<newline/>
		</children>
	</template>
</structure>
