// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   ImageTiledCanvas.java

package dicomviewer;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

// Referenced classes of package dicomviewer:
//            DicomData, Viewer

public class ImageTiledCanvas extends Canvas
{
    class SetPoint extends MouseAdapter
    {

        public void mouseEntered(MouseEvent mouseevent)
        {
            if(loupe)
            {
                setCursor(new Cursor(1));
                return;
            }
            if(move)
                setCursor(new Cursor(12));
        }

        public void mouseExited(MouseEvent mouseevent)
        {
            setCursor(new Cursor(0));
        }

        public void mousePressed(MouseEvent mouseevent)
        {
            x0 = x1 = mouseevent.getX();
            y0 = y1 = mouseevent.getY();
            if(loupe)
                mouseDown = true;
            else
            if(!move)
                changeCursor();
            changeActive = true;
            repaint();
        }

        public void mouseReleased(MouseEvent mouseevent)
        {
            x2 = mouseevent.getX();
            y2 = mouseevent.getY();
            if(loupe)
            {
                mouseDown = false;
                repaint();
                return;
            }
            if(move)
            {
                px = (px + x1) - x2;
                py = (py + y1) - y2;
                repaint();
                return;
            }
            if(zoom_flag)
            {
                changeCursor();
                parent.drag_changeZoom(y2 - y1);
                return;
            } else
            {
                changeCursor();
                parent.dragDone_changeWwWl(x2 - x1, x2 - x0, y2 - y1, y2 - y0);
                return;
            }
        }

        SetPoint()
        {
        }
    }

    class SetPoint_move extends MouseMotionAdapter
    {

        public void mouseDragged(MouseEvent mouseevent)
        {
            x2 = mouseevent.getX();
            y2 = mouseevent.getY();
            if(move)
            {
                px = (px + x1) - x2;
                py = (py + y1) - y2;
                x1 = x2;
                y1 = y2;
                repaint();
                return;
            }
            if(zoom_flag)
            {
                parent.drag_changeZoom(y2 - y1);
                x1 = x2;
                y1 = y2;
                return;
            }
            if(!loupe)
            {
                parent.drag_changeWwWl(x2 - x1, y2 - y1);
                x1 = x2;
                y1 = y2;
            }
        }

        public void mouseMoved(MouseEvent mouseevent)
        {
        }

        SetPoint_move()
        {
        }
    }


    Viewer parent;
    Image image[];
    Dimension canvasSize;
    int width;
    int height;
    int imgW;
    int imgH;
    int px;
    int py;
    int x0;
    int y0;
    int x1;
    int y1;
    int x2;
    int y2;
    double zoom;
    int zoomW;
    int zoomH;
    boolean zoom_flag;
    boolean hand;
    boolean loupe;
    boolean mouseDown;
    boolean move;
    int tileR;
    int tileC;
    int imageIndex;
    int activeNo;
    boolean changeActive;
    int start;
    boolean showStudyInfo;
    String patientID[];
    String patientName[];
    String patientAgeSex[];
    String studyID[];
    String studyDate[];
    String studyTime[];
    String imageNo[];
    String ww_wl[];
    Font font;
    FontMetrics fm;

    public ImageTiledCanvas(int i, int j, Viewer viewer)
    {
        zoom = 1.0D;
        zoom_flag = false;
        hand = true;
        loupe = false;
        move = false;
        changeActive = false;
        showStudyInfo = true;
        parent = viewer;
        zoomH = height = imgH = j;
        zoomW = width = imgW = i;
        addMouseListener(new SetPoint());
        addMouseMotionListener(new SetPoint_move());
        font = new Font("Helvetica", 0, 12);
        fm = getFontMetrics(font);
    }

    public void setTileType(int i, int j)
    {
        int k = i * j;
        tileR = i;
        tileC = j;
        canvasSize = new Dimension(width * tileC, height * tileR);
        setSize(canvasSize);
        image = new Image[k];
        patientID = new String[k];
        patientName = new String[k];
        patientAgeSex = new String[k];
        studyID = new String[k];
        studyDate = new String[k];
        studyTime = new String[k];
        imageNo = new String[k];
        ww_wl = new String[k];
        imageIndex = 0;
    }

    public void addImage(Image image1)
    {
        if(imageIndex < image.length)
        {
            image[imageIndex] = image1;
            imageIndex++;
        } else
        {
            System.arraycopy(image, 1, image, 0, image.length - 1);
            image[imageIndex - 1] = image1;
        }
        repaint();
    }

    public void addPreImage(Image image1)
    {
        System.arraycopy(image, 0, image, 1, image.length - 1);
        image[0] = image1;
        if(imageIndex < image.length)
            imageIndex++;
        repaint();
    }

    public void setImage(Image image1, int i)
    {
        image[i] = image1;
        imageIndex = i;
        repaint();
    }

    public void setStudyInfo_flag(boolean flag)
    {
        showStudyInfo = flag;
        repaint();
    }

    public void setStudyInfo(DicomData dicomdata, int i)
    {
        if(dicomdata == null)
        {
            patientID[i] = null;
            patientName[i] = null;
            patientAgeSex[i] = null;
            studyID[i] = null;
            studyDate[i] = null;
            studyTime[i] = null;
            imageNo[i] = null;
            ww_wl[i] = null;
            return;
        }
        String s = "(0010,0020)";
        if(dicomdata.isContain(s))
            patientID[i] = dicomdata.getAnalyzedValue(s);
        else
            patientID[i] = null;
        s = "(0010,0010)";
        if(dicomdata.isContain(s))
            patientName[i] = dicomdata.getAnalyzedValue(s);
        else
            patientName[i] = null;
        s = "(0010,1010)";
        if(dicomdata.isContain(s))
        {
            patientAgeSex[i] = dicomdata.getAnalyzedValue(s);
            s = "(0010,0040)";
            if(dicomdata.isContain(s))
                patientAgeSex[i] += " " + dicomdata.getAnalyzedValue(s);
        } else
        {
            s = "(0010,0040)";
            if(dicomdata.isContain(s))
                patientAgeSex[i] = dicomdata.getAnalyzedValue(s);
            else
                patientAgeSex[i] = null;
        }
        s = "(0020,0010)";
        if(dicomdata.isContain(s))
            studyID[i] = dicomdata.getAnalyzedValue(s);
        else
            studyID[i] = null;
        s = "(0008,0020)";
        if(dicomdata.isContain(s))
            studyDate[i] = dicomdata.getAnalyzedValue(s);
        else
            studyDate[i] = null;
        s = "(0008,0030)";
        if(dicomdata.isContain(s))
            studyTime[i] = dicomdata.getAnalyzedValue(s);
        else
            studyTime[i] = null;
        s = "(0020,0013)";
        if(dicomdata.isContain(s))
        {
            imageNo[i] = dicomdata.getAnalyzedValue(s);
            return;
        } else
        {
            imageNo[i] = null;
            return;
        }
    }

    public void setWW_WL(int i, int j, int k)
    {
        ww_wl[k] = j + "/" + i;
    }

    public void changeImage(Image image1, int i)
    {
        image[i] = image1;
        repaint();
    }

    public void setActiveNo(int i)
    {
        activeNo = i;
    }

    public void setStartNo(int i)
    {
        start = i;
    }

    public void setCanvasSize(int i, int j)
    {
        canvasSize = new Dimension(i, j);
        setSize(canvasSize);
        repaint();
    }

    public void paint(Graphics g)
    {
        update(g);
    }

    public void update(Graphics g)
    {
        if(px + zoomW > imgW)
            px = imgW - zoomW;
        if(py + zoomH > imgH)
            py = imgH - zoomH;
        if(px < 0)
            px = 0;
        if(py < 0)
            py = 0;
        Image image1 = createImage(width, height);
        Graphics g1 = image1.getGraphics();
        Image image2 = createImage(200, 200);
        Graphics g2 = image2.getGraphics();
        int i = 0;
        for(int l = 0; l < tileR; l++)
        {
            if(i > imageIndex)
                break;
            for(int i1 = 0; i1 < tileC; i1++)
            {
                if(i > imageIndex)
                    break;
                int j = width * i1;
                int k = height * l;
                if(image[i] == null)
                {
                    g1.setColor(Color.black);
                    g1.fillRect(0, 0, width, height);
                } else
                {
                    g1.drawImage(image[i], 0, 0, width, height, px, py, px + zoomW, py + zoomH, this);
                }
                g.drawImage(image1, j, k, this);
                if(showStudyInfo)
                {
                    int l1 = 0;
                    g.setColor(Color.green);
                    g.setFont(font);
                    int j1 = j + 2;
                    int k1 = k + fm.getAscent() + 2;
                    if(patientID[i] != null)
                    {
                        g.drawString(patientID[i], j1, k1);
                        k1 += fm.getHeight();
                    }
                    if(patientName[i] != null)
                    {
                        g.drawString(patientName[i], j1, k1);
                        k1 += fm.getHeight();
                    }
                    if(patientAgeSex[i] != null)
                        g.drawString(patientAgeSex[i], j1, k1);
                    k1 = (k + height) - fm.getDescent() - 2;
                    if(ww_wl[i] != null)
                        g.drawString(ww_wl[i], j1, k1);
                    if(l1 < fm.stringWidth(studyID[i]))
                        l1 = fm.stringWidth(studyID[i]);
                    if(l1 < fm.stringWidth(studyDate[i]))
                        l1 = fm.stringWidth(studyDate[i]);
                    if(l1 < fm.stringWidth(studyTime[i]))
                        l1 = fm.stringWidth(studyTime[i]);
                    j1 = (j + width) - l1 - 2;
                    k1 = k + fm.getAscent() + 2;
                    if(studyID[i] != null)
                    {
                        g.drawString(studyID[i], j1, k1);
                        k1 += fm.getHeight();
                    }
                    if(studyDate[i] != null)
                    {
                        g.drawString(studyDate[i], j1, k1);
                        k1 += fm.getHeight();
                    }
                    if(studyTime[i] != null)
                        g.drawString(studyTime[i], j1, k1);
                    j1 = (j + width) - fm.stringWidth(imageNo[i]) - 2;
                    k1 = (k + height) - fm.getDescent() - 2;
                    if(imageNo[i] != null)
                        g.drawString(imageNo[i], j1, k1);
                }
                if(changeActive)
                {
                    if(j <= x1 && x1 < j + width && k <= y1 && y1 < k + height)
                    {
                        if(image[i] == null)
                        {
                            g.setColor(Color.white);
                            g.drawRect(j, k, width - 1, height - 1);
                        } else
                        {
                            activeNo = i;
                            parent.changeActive(activeNo + start);
                            g.setColor(Color.yellow);
                            g.drawRect(j, k, width - 1, height - 1);
                        }
                    } else
                    {
                        g.setColor(Color.white);
                        g.drawRect(j, k, width - 1, height - 1);
                    }
                } else
                if(activeNo == i)
                {
                    g.setColor(Color.yellow);
                    g.drawRect(j, k, width - 1, height - 1);
                } else
                {
                    g.setColor(Color.white);
                    g.drawRect(j, k, width - 1, height - 1);
                }
                if(loupe && mouseDown && j <= x1 && x1 < j + width && k <= y1 && y1 < k + height)
                    g2.drawImage(image1, 0, 0, 200, 200, x1 - j - 50, y1 - k - 50, (x1 - j) + 50, (y1 - k) + 50, this);
                i++;
            }

        }

        changeActive = false;
        if(loupe && mouseDown)
        {
            g.drawImage(image2, x1 - 100, y1 - 100, this);
            g.setColor(Color.white);
            g.drawRect(x1 - 100, y1 - 100, 200, 200);
        }
    }

    public void changeZoom(double d)
    {
        zoom = d;
        int i = zoomW;
        int j = zoomH;
        int k = px;
        int l = py;
        zoomW = (int)((double)width * d);
        zoomH = (int)((double)height * d);
        px = (i - zoomW >> 1) + k;
        py = (j - zoomH >> 1) + l;
        repaint();
    }

    public void changeCanvasSize(double d)
    {
        width = (int)((double)imgW * d);
        height = (int)((double)imgH * d);
        canvasSize.setSize(width * tileC, height * tileR);
        setSize(canvasSize);
    }

    public void setPxPy(int i, int j)
    {
        px = i;
        py = j;
    }

    public int getPx()
    {
        return px;
    }

    public int getPy()
    {
        return py;
    }

    public void setLoupeState(boolean flag)
    {
        loupe = flag;
    }

    public void setMoveState(boolean flag)
    {
        move = flag;
    }

    public void setZoomState(boolean flag)
    {
        zoom_flag = flag;
    }

    private void changeCursor()
    {
        if(hand)
            setCursor(new Cursor(13));
        else
            setCursor(new Cursor(0));
        hand = !hand;
    }

}
