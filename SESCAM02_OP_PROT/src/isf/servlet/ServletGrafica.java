package isf.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Properties;


import isf.util.UtilGrafica;
import quadbase.ChartAPI.QbChart;
import quadbase.util.INoDataToPlotMessage;

import isf.util.log.Log;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author sin atribuir
 * @version 1.0
 */

public class ServletGrafica extends HttpServlet {



	Log fichero_log = null; // fichero de log
	Hashtable ht_max_min = null;
  //Inicializar variables globales
  public void init() throws ServletException {
		fichero_log = Log.getInstance();
	}

   /**
     * Permite formatear los datos a formato QbChart.
     * Importante los alias de la SQL deben ser v0, v1,... en el caso de los campos comunes ser� r0, r1,.....
     * <br><br>
     *
     * @param consulta Recibe los datos.
     * @param n N�mero de columnas.
     * @param tipo Array de los tipos de columnas.
     *
     * @return String[][] Datos.
     */
    public static String[][] formatear(Vector consulta, int n) throws Exception {
        int nResTot = consulta.size();

        String[][] resultado = new String[nResTot][n];
        Hashtable datos;
        int fila = 0;

        for (int i=0;i<nResTot;i++) {
            datos = (Hashtable)consulta.get(i);
            for (int cont=0;cont<n;cont++) {
                if (datos.get("v"+cont) != null) {
                         resultado[fila][cont] = (String)datos.get("v"+cont);
               }
            }
            fila++;
        }

        return resultado;
    }
	    /**
     * M�todo que imprime la gr�fica.
     * <br><br>
     *
     * @param req Recibe el HttpServletRequest.
     * @param res Recibe el HttpServletResponse.
     * @param chart Recibe el QbChart.
     *
     * @return void.
     */

    private void imprimirGrafica(HttpServletRequest req, HttpServletResponse res,QbChart chart) throws Exception {

        int gifWidth = new Integer((String)req.getParameter("gifWidth")).intValue();
        int gifHeight = new Integer((String)req.getParameter("gifHeight")).intValue();

        res.setContentType("image/gif");
        res.setHeader("Cache-Control","no-store"); //http 1.0
        res.setHeader("Pragma","no-cache");  //http 1.1
        res.setDateHeader("Expires",0);   //prevents caching at the proxy server


        try {
            // Get the response's OutputStream to return text to the client.
            OutputStream toClient = res.getOutputStream();
            ByteArrayOutputStream bout = new ByteArrayOutputStream(1024);

            // Export the chart
            try{
							//chart.export(QbChart.JPEG, toClient, gifWidth, gifHeight);
              chart.export(QbChart.JPEG, bout, gifWidth, gifHeight);
            }catch (Exception e){
                bout.close();
                toClient.close();
                e.printStackTrace();
								fichero_log.error("Error al imprimir Gr�ficas-Error en el export() " + e.getMessage());
                throw e;
            }
            bout.close();
            toClient.write(bout.toByteArray());
            toClient.flush();
            toClient.close();
        } catch (Exception e) {
            e.printStackTrace();
						fichero_log.error("Error al imprimir Gr�ficas " +  e.getMessage());
            throw e;
        }
    }

		public Vector insertarMaxMin (Vector v, long max, long min) {
			Vector v_res = new Vector();
			for (int i = 0; i < v.size(); i++) {
				// a�adir la fila real
				Hashtable ht_fila = (Hashtable) v.elementAt(i);
				String V0 = (String) ht_fila.get("v0");

				v_res.add(ht_fila);
				// a�adir la fila del maximo
				Hashtable ht_fila_max = new Hashtable();
				ht_fila_max.put("v0",V0);
				ht_fila_max.put("v1","Maximo");
				ht_fila_max.put("v2",new Long(max).toString());
				v_res.add(ht_fila_max);
			  // a�adir la fila del m�nimo
				Hashtable ht_fila_min = new Hashtable();
				ht_fila_min.put("v0",V0);
				ht_fila_min.put("v1","Minimo");
				ht_fila_min.put("v2",new Long(min).toString());
				v_res.add(ht_fila_min);
			}

			return v_res;

		}
/*
		public Vector distribuirUniformemente(Vector v) {
			Vector v_result = new Vector();
			Hashtable ht_fila = null, ht_fila_next = null ;
			for (int i = 0; i < v.size(); i++) {
				ht_fila = (Hashtable) v.elementAt(i);
				v_result.add(ht_fila);
				String fecha = (String) ht_fila.get("v0");
				double dvalor = new Double(((String) ht_fila.get("v2"))).doubleValue();
				ht_fila_next = (Hashtable) v.elementAt(i+1);
				String fecha_next = (String) ht_fila_next.get("v0");
				double dvalor_next = new Double(((String) ht_fila_next.get("v2"))).doubleValue();
				long dif = diasDiferencia(fecha,fecha_next);
				double dival = (dvalor_next - dvalor) / dif;
				Hashtable ht_fila_ajuste = new Hashtable();
				for (int d = 0; d < dif-1; d++) {
					ht_fila_ajuste = new Hashtable();
					ht_fila_ajuste.put("v0",fechaMas(d+1));
					ht_fila_ajuste.put("v1","real");
					ht_fila_ajuste.put("v2",new Double(dvalor + (dival * (d+1))).toString());
				}
			}
			return v_result;
		}
*/
		public QbChart getGrafica(HttpServletRequest req) throws Exception
		{
			try {

				Vector v_res = new Vector();
				String datos = req.getParameter("datos");
				if (datos == null || !datos.equals("no")) {
					HttpSession session = req.getSession();
					Properties prp_grafica = (Properties) session.getAttribute("beanGrafica");
					session.removeAttribute("beanGrafica");
					if (prp_grafica != null) {
						Vector result = (Vector) prp_grafica.get("datos");
						String constante = prp_grafica.getProperty("constante","");
						Properties prop_constante = (Properties) prp_grafica.get("datos_constante");
						long max = (new Long((String) prop_constante.getProperty("maximo","0"))).longValue();
						long min = (new Long((String) prop_constante.getProperty("minimo","0"))).longValue();
						if (result != null) {
							//		distribuirUniformemente(result);
							v_res = insertarMaxMin(result,max,min);
							result = null;
						}
					}
				}
				String [][] records = formatear(v_res, 3);

				return UtilGrafica.getChart(records);

			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				fichero_log.error("Error al generar Gr�fica " + e.getMessage());
				throw e;
			}
		}


  //Procesar una petici�n HTTP Get
  public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			QbChart chart = null;
			chart = getGrafica(request);
			INoDataToPlotMessage noData = chart.gethNoDataToPlotMessage();
			noData.setMessage("No existen datos disponibles");
			noData.setFont(UtilGrafica.LETRA_CORPORATIVA1);
			imprimirGrafica(request, response,chart);
		 } catch (Exception e) {
			 fichero_log.error("Error en el ServletGrafica" + e.getMessage());
		}

  }





  //Limpiar recursos
  public void destroy() {
  }
}