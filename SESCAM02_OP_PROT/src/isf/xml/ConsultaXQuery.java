package isf.xml;

import java.util.*;

import oracle.xquery.*;
import oracle.xml.parser.v2.*;
import org.w3c.dom.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author sin atribuir
 * @version 1.0
 */

public class ConsultaXQuery {

  public ConsultaXQuery() {
  }

	public static Vector getDatosConstantes(Properties prop_params) {
		XQueryContext ctx = new XQueryContext();
		Vector v_result = new Vector();
		String idPaciente = prop_params.getProperty("paciente","");
		String periodo = prop_params.getProperty("periodo","");
		String constante = prop_params.getProperty("constante","");

		try	{
			// prepared the query to a string
			String str_xq =
					"FOR $p IN document(\"../GDCSescam/xml/datos.xml\")/datosconstantes/paciente " +
					" WHERE $p/@id = \""+idPaciente+"\" AND $p/@fecha >= " + periodo +
					" RETURN <paciente>{ $p/@fecha }{$p/"+constante+"} </paciente>";
			PreparedXQuery xq = ctx.prepareXQuery(str_xq);

			// get a resultset
			XQueryResultSet rset = xq.executeQuery(true);


			while (rset.next())	{
				XMLNode node = rset.getNode();  // get result nodes
				NodeList nd_list = node.getChildNodes();
				for (int i = 0; i < nd_list.getLength(); i++) {
					Node nd = nd_list.item(i);
					Hashtable ht_fila = new Hashtable();
					String time = node.getAttributes().item(0).getNodeValue();
					String fecha = time.substring(6,8)+ "/" + time.substring(4,6) + "/" + time.substring(2,4);
					ht_fila.put("v0",fecha);
					ht_fila.put("v1","Real");
					ht_fila.put("v2", (nd.getChildNodes().item(0)).getNodeValue());
					v_result.add(ht_fila);
				}

			}
			return v_result;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
      // do something..
		}
		return v_result;

}
  public static void main(String[] args) {
    ConsultaXQuery consultaXQuery1 = new ConsultaXQuery();
  }
}