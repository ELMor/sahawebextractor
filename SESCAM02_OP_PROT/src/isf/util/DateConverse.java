// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   DateConverse.java

package isf.util;

import isf.util.log.Log;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateConverse
{
    private static class FormatDate
    {

        public String str_format;

        public FormatDate(String s)
        {
            str_format = s;
        }
    }


    static final String DD_MM_AAAA_SEP_BARRA = "dd/MM/yyyy";
    static final String DD_MM_AAAA_SEP_GUION = "dd-MM-yyyy";
    static final String DD_MM_AA_SEP_BARRA = "dd/MM/yy";
    static final String DD_MM_AA_SEP_GUION = "dd-MM-yy";
    static final String AAAA_MM_DD_SEP_BARRA = "yyyy/MM/dd";
    static final String AAAA_MM_DD_SEP_GUION = "yyyy-MM-dd";
    static final String AAAA_MM_DD = "yyyyMMdd";
    static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat formatterToQS = new SimpleDateFormat("yyyyMMdd");
    static Exception ex = new Exception("Formato de fecha incorrecto");
    static SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    public static final FormatDate FD_DD_MM_AAAA_SEP_BARRA = new FormatDate("dd/MM/yyyy");
    public static final FormatDate FD_DD_MM_AAAA_SEP_GUION = new FormatDate("dd-MM-yyyy");
    public static final FormatDate FD_DD_MM_AA_SEP_BARRA = new FormatDate("dd/MM/yy");
    public static final FormatDate FD_DD_MM_AA_SEP_GUION = new FormatDate("dd-MM-yy");
    public static final FormatDate FD_AAAA_MM_DD_SEP_BARRA = new FormatDate("yyyy/MM/dd");
    public static final FormatDate FD_AAAA_MM_DD_SEP_GUION = new FormatDate("yyyy-MM-dd");

    public DateConverse()
    {
    }

    public static String dateToQSInputStream(Date date)
    {
        String s = new String("");
        if(date != null)
            synchronized(formatterToQS)
            {
                s = formatterToQS.format(date);
            }
        return s;
    }

    public static String dateToString(Date date)
    {
        String s = new String("");
        if(date != null)
            synchronized(formatter)
            {
                s = formatter.format(date);
            }
        return s;
    }

    public static String dateToStringFormat(Date date, FormatDate formatdate)
    {
        String s = new String("");
        if(date != null)
            synchronized(formatter)
            {
                try
                {
                    formatter.applyPattern(formatdate.str_format);
                    s = formatter.format(date);
                }
                catch(Exception exception1)
                {
                    Log.getInstance().warning("Error al convertir la fecha '" + date + "' : " + exception1.getMessage());
                }
                finally
                {
                    formatter.applyPattern("yyyy-MM-dd");
                }
            }
        return s;
    }

    public static String dateToString_ddMMyyhhmmss(Date date)
    {
        String s = new String("");
        if(date != null)
            synchronized(formatter2)
            {
                s = formatter2.format(date);
            }
        return s;
    }

    public static Date stringToDate(String s)
        throws Exception
    {
        Date date = null;
        if(s != null)
        {
            synchronized(formatter)
            {
                date = formatter.parse(s);
            }
            if(date == null)
                throw ex;
        }
        return date;
    }

}
