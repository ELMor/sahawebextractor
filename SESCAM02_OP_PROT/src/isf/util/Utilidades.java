/*

*   Copyright ( 1999 Union Fenosa International Software Factory,

*   S.A.. All Rights Reserved.

*

*   This software is the confidential and proprietary information of

*   Uni�n Fenosa International Software Factory, S.A.. You shall not

*   disclose such confidential information and shall not accordance

*   with the terms of the license agreement you entered into with

*   I.S.F..

*

*/

package isf.util;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.JspWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.math.BigDecimal;

/**
   Clase: Utilidades
   Autor: YLO,SBE, JDZ
   Tipo : Publica
   Prop�sito: Clase para proporcionar diversas utilidades.
*/


public class Utilidades{

	private static final String	NOMBRE_CLASE = "Utilidades::";

	public static final String NOMBRE_FICHERO_PROPIEDADES = "config";
	private	static final String HORA_CAMBIO_VI = "Hora_v_i";
	private	static final String HORA_CAMBIO_IV = "Hora_i_v";
	private	static final String INICIO_VERANO = "Mes_inicio_verano";
	private	static final String INICIO_INVIERNO = "Mes_inicio_invierno";


	// Constantes p�blicas
	public static final int INCREMENTA_ANIO = 1;
	public static final int INCREMENTA_MES = 2;
	public static final int INCREMENTA_DIA = 3;
	public static final double PESETAS_UN_EURO = 166.386;
	public static final String TIPO_MONEDA_PESETA = "PESETA";
	public static final String TIPO_MONEDA_EURO = "EURO";
	public static final String TIPO_MONEDA_CENTIEURO = "CENTIEURO";

		// Mensaje de error GENERAL
	public static final String ERROR_GENERAL = "Error indeterminado del sistema. Consulte con el administrador.";

	public static boolean IZQUIERDA = true;
	public static boolean DERECHA = false;




	public static final int LUNES		= 1;
	public static final int MARTES		= 2;
	public static final int MIERCOLES	= 3;
	public static final int JUEVES		= 4;
	public static final int VIERNES		= 5;
	public static final int SABADO		= 6;
	public static final int DOMINGO		= 7;



/**
      Metodo: stringToDate
      Funcion: Pasa un String validado como fecha a java.sql.Date
      Return:  Date
      Argumentos: String en formato "dd/mm/aaaa"
	  Autor: JAE
*/
public static java.sql.Date stringToDate(String str_fecha) throws Exception
{
	String str_dia, str_mes, str_anio;
	int int_dia, int_mes, int_anio;
	int int_indice1, int_indice2;
	Calendar cal_fecha = null;

	// Se obtienen los �ndices donde est�n los separadores
	int_indice1 = str_fecha.indexOf("/");
	int_indice2 = str_fecha.indexOf("/",int_indice1 + 1);

	// Se obtienen los dias, meses y a�os
	str_dia = str_fecha.substring(0,int_indice1);
	str_mes = str_fecha.substring(int_indice1 + 1,int_indice2);
	str_anio = str_fecha.substring(int_indice2 + 1);

	int_dia = new Integer(str_dia).intValue();
	int_mes = new Integer(str_mes).intValue();
	int_anio = new Integer(str_anio).intValue();

	cal_fecha = Calendar.getInstance();

	cal_fecha.set(Calendar.HOUR,(int)0);
	cal_fecha.set(Calendar.MINUTE,(int)0);
	cal_fecha.set(Calendar.SECOND,(int)0);
	cal_fecha.set(Calendar.MILLISECOND,(int)0);

	cal_fecha.set(Calendar.DATE,int_dia);
	cal_fecha.set(Calendar.MONTH,int_mes-1);
	cal_fecha.set(Calendar.YEAR,int_anio);

	//	cal_fecha.set(int_anio, int_mes - 1, int_dia);

	return new java.sql.Date(cal_fecha.getTime().getTime());

}


/**
      Metodo: dateToString
      Funcion: Pasa un Date a String en el formato dd/mm/aaaa
      Return:  String
      Argumentos: java.sql.Date
	  Autor: JAE
*/
public static String dateToString(java.sql.Date dat_fecha) throws Exception
{
	Calendar cal_fecha;
	int int_anio, int_mes, int_dia;
	String str_fecha;
	String str_anio = "";
	String str_mes = "";
	String str_dia = "";

	if ((String.valueOf(dat_fecha)).equals("null"))
		str_fecha = "null";
	else
	{
		cal_fecha = Calendar.getInstance();
		cal_fecha.setTime(dat_fecha);

		int_anio = cal_fecha.get(cal_fecha.YEAR);
		int_mes = (cal_fecha.get(cal_fecha.MONTH)) + 1;
		int_dia = cal_fecha.get(cal_fecha.DAY_OF_MONTH);

		str_anio = new Integer(int_anio).toString();
		str_mes = new Integer(int_mes).toString();
		str_dia = new Integer(int_dia).toString();
		while (str_anio.length() < 4)
		{
			str_anio = "0" + str_anio;
		}
		while (str_mes.length() < 2)
		{
			str_mes = "0" + str_mes;
		}
		while (str_dia.length() < 2)
		{
			str_dia = "0" + str_dia;
		}

		str_fecha = str_dia + "/" + str_mes + "/" + str_anio;
	}

	return str_fecha;
}



/**
      Metodo: cambiarComa
      Funcion: Cambia la coma decimal (si existe) en una cadena por un punto
      Return:  float
      Argumentos: String
	  Autor: JAE
*/
public static double cambiarComa(String str_numero) throws Exception
{
	char cha_coma = ',';
	char cha_punto = '.';

	str_numero = str_numero.replace(cha_coma,cha_punto);
	return new Double(str_numero).doubleValue();

}


/**
      Metodo: quitarPuntos
      Funcion: Quita los puntos separadores de millar y transforma la coma decimal en punto
      Return:  float
      Argumentos: String
	  Autor: JAE
*/


	/**
      Metodo:  invertir
		Funcion: Invierte la cadena de caracteres pasda como argumento.
      Return:  String (cadena invertida)
      Argumentos:
			String Str_cadena	(IN) : cadena a invertir.
   */

	private static String invertir(String Str_cadena){

		if(Str_cadena != null){
			if(Str_cadena.length() == 1
				|| Str_cadena.length()==0)
			{
				return Str_cadena;
			}
			else{
				return invertir(Str_cadena.substring(1,
											Str_cadena.length()))+Str_cadena.charAt(0);
			}
		}
		else{
			return new String();
		}
	}





public static Vector generarVector(ResultSet rs) throws SQLException
{
	int int_columnas =0;
	String[] str_fila = null;
	Vector vec_resultado = null;

	if (rs != null)
	{
		int_columnas = rs.getMetaData().getColumnCount();
		vec_resultado = new Vector();
		while( rs.next() )
		{
			str_fila = new String[int_columnas];
			for(int int_pos=1;int_pos<=int_columnas;int_pos++)
				str_fila[int_pos-1] = rs.getString(int_pos);
			vec_resultado.addElement(str_fila);
		}
	}
	return vec_resultado;
}



/**
	Transforma un identificador numerico a String insertando ceros a la izquierda
	hasta alcanzar la longitud indicada por argumento.
	@return	String
	@author YLO
	@since	15-5-2001
*/
public static String construyeId(long lon_id,long lon_maximaLongitud){

	String	Str_resultado = ""+lon_id;

	while (Str_resultado.length() <  lon_maximaLongitud )
	{
		Str_resultado = "0" + Str_resultado;
	}

	return Str_resultado;

} //construyeId


/**
	Calcula una nueva fecha a partir de la pasada por argumentos, bien sumando
	el periodo pasado, bien restandolo (si el argumento es negativo).
	@param java.sql.Date dat_fecha: fecha a partir de la que se recalcula el periodo.
	@param int: Tipo de incremento: 1=a�o, 2=mes, 3=dia
	@param int: Incremento
	@return	java.sql.Date
	@since	28-6-2001
	@author JAE
*/
public static java.sql.Date calcularPeriodoFecha(java.sql.Date dat_fecha,int int_tipo, int int_incremento)
{
	Calendar cal_fecha;
	java.util.Date dat_fechaAux = null;
	java.sql.Date dat_fechaVuelta = null;
	boolean boo_error = false;

	cal_fecha = Calendar.getInstance();
	cal_fecha.setTime(dat_fecha);

	switch (int_tipo)
	{
		case 1:
		{
			cal_fecha.add(Calendar.YEAR, int_incremento);
			break;
		}
		case 2:
		{
			cal_fecha.add(Calendar.MONTH, int_incremento);
			break;
		}
		case 3:
		{
			cal_fecha.add(Calendar.DATE, int_incremento);
			break;
		}
		default:
		{
			boo_error = true;
		}
	}

	if (!boo_error)
	{
		dat_fechaAux = cal_fecha.getTime();
		dat_fechaVuelta = new java.sql.Date(dat_fechaAux.getTime());
	}
	return dat_fechaVuelta;
}

/**
	Calcula la diferencia entre las fechas pasadas como argumento,
			dat_fecha1 - dat_fecha2
	@param java.sql.Date dat_fecha1
	@param java.sql.Date dat_fecha2
	@return	long (Numero de dias de diferencia entre las fechas). Si fecha2 es posterior a fecha1 da error (devuelve -1)
	@since	28-6-2001
	@YLO
*/

public static int getDiferenciaFechas(java.sql.Date dat_fecha1, java.sql.Date dat_fecha2) throws Exception
{
	final int DIAS_ANIO_BISIESTO 	= 366;
	final int DIAS_ANIO_NO_BISIESTO = 365;

	int int_diferenciaDias = -1;

	Calendar cal_fecha_1;
	Calendar cal_fecha_2;
	int int_anio_1;
	int int_anio_2;

	if (dat_fecha1.compareTo(dat_fecha2) >= 0)
	{

		cal_fecha_1 = Calendar.getInstance();
		cal_fecha_1.setTime(dat_fecha1);
		cal_fecha_2 = Calendar.getInstance();
		cal_fecha_2.setTime(dat_fecha2);

		// Se obtienen los a�os de cada una de las fechas
		int_anio_1 = cal_fecha_1.get(Calendar.YEAR);
		int_anio_2 = cal_fecha_2.get(Calendar.YEAR);

		// Las dos fechas son del mismo a�o
		if (int_anio_1 == int_anio_2)
		{
			int_diferenciaDias = cal_fecha_1.get(Calendar.DAY_OF_YEAR)
								 - cal_fecha_2.get(Calendar.DAY_OF_YEAR);
		}
		else // El a�o 1 es mayor que el 2
		{
			int int_diasAnio_1;
			int int_diasAnio_2;
			int int_diasTotalAnio_2;
			int int_diasAniosIntermedios = 0;

			// Calcula los dias transcurridos de cada a�o
			int_diasAnio_1 = cal_fecha_1.get(Calendar.DAY_OF_YEAR);
			int_diasAnio_2 = cal_fecha_2.get(Calendar.DAY_OF_YEAR);

			// Calcula los dias totales del a�o 2 (para despu�s poder
			// calcular los dias que restan para acabar)
			if (esBisiesto(int_anio_2))
			{
				int_diasTotalAnio_2 = DIAS_ANIO_BISIESTO;
			}
			else
			{
				int_diasTotalAnio_2 = DIAS_ANIO_NO_BISIESTO;
			}

			// Calcula el n�mero de dias en a�os intermedios
			for (int int_i = 1; int_i < (int_anio_1 - int_anio_2); int_i++)
			{
				if (esBisiesto(int_anio_2 + int_i))
				{
					int_diasAniosIntermedios = int_diasAniosIntermedios + DIAS_ANIO_BISIESTO;
				}
				else
				{
					int_diasAniosIntermedios = int_diasAniosIntermedios + DIAS_ANIO_NO_BISIESTO;
				}
			}


			int_diferenciaDias = (int_diasTotalAnio_2 - int_diasAnio_2)
								+ int_diasAnio_1
								+ int_diasAniosIntermedios;
		}
	}
	return int_diferenciaDias;
}

/**
	Decide si D_fechaInicio es anterior a D_fechaFin
	@param java.util.Date D_fechaInicio
	@param java.util.Date D_fechaFin
	@return	boolean : true, si D_fechaInicio es anterior a D_fechaFin
	@since	17-9-2001
	@YLO
*/
public static boolean esAnterior(java.sql.Date D_fechaInicio,java.sql.Date D_fechaFin) throws Exception
{
	boolean boo_resultado = true;
	boo_resultado = (getDiferenciaFechas(D_fechaInicio,D_fechaFin) < 0);
	return boo_resultado;
} //esAnterior


/**
	Calcula la fecha actual del sistema.
	@return	java.sql.Date (Fecha actual)
	@since	06-07-2001
	@YLO
*/
public static java.sql.Date fechaActual(){

	java.sql.Date fechaHoy;

	Calendar calendar = Calendar.getInstance();
	java.util.Date date = calendar.getTime();
	fechaHoy = new java.sql.Date(date.getTime());

	return fechaHoy;
}

/**
	Calcula la fecha actual del sistema con horas, minutos y segundos.
	@return	Timestamp (Fecha actual)
	@since	06-02-2002
	@JAE
*/
public static Timestamp timestampActual()
{
	Timestamp fechaHoy;

	Calendar calendar = Calendar.getInstance();
	fechaHoy = new Timestamp(calendar.getTime().getTime());

	return fechaHoy;
}

/**
	Obtiene el dia de un campo fecha
	@param java.sql.Date dat_fecha
	@return	int, dia (-1 si error)
	@since	13-7-2001
	@JAE
*/
public static int getDia(java.sql.Date dat_fecha) throws Exception
{
	Calendar cal_fecha;
	int int_dia;

	if ((String.valueOf(dat_fecha)).equals("null"))
		int_dia = -1;
	else
	{
		cal_fecha = Calendar.getInstance();
		cal_fecha.setTime(dat_fecha);

		int_dia = cal_fecha.get(cal_fecha.DAY_OF_MONTH);
	}

	return int_dia;
}


/**
	Obtiene el mes de un campo fecha
	@param java.sql.Date dat_fecha
	@return	int, mes (-1 si error)
	@since	13-7-2001
	@JAE
*/
public static int getMes(java.sql.Date dat_fecha) throws Exception
{
	Calendar cal_fecha;
	int int_mes;

	if ((String.valueOf(dat_fecha)).equals("null"))
		int_mes = -1;
	else
	{
		cal_fecha = Calendar.getInstance();
		cal_fecha.setTime(dat_fecha);

		int_mes = (cal_fecha.get(cal_fecha.MONTH)) + 1;
	}

	return int_mes;
}



/**
	Obtiene el a�o de un campo fecha
	@param java.sql.Date dat_fecha
	@return	int, a�o (-1 si error)
	@since	13-7-2001
	@JAE
*/
public static int getAnio(java.sql.Date dat_fecha) throws Exception
{
	Calendar cal_fecha;
	int int_anio;

	if ((String.valueOf(dat_fecha)).equals("null"))
		int_anio = -1;
	else
	{
		cal_fecha = Calendar.getInstance();
		cal_fecha.setTime(dat_fecha);

		int_anio = cal_fecha.get(cal_fecha.YEAR);
	}

	return int_anio;
}





/**
	Calcula el n�mero de dia del �ltimo domingo de un mes
	@param int, n�mero de mes
	@param int, n�mero de a�o (para calcular mes de Febrero si es bisiesto)
	@return	int, n�mero de dias del mes, (-1 si el n�mero de mes es err�neo)
	@since	13-7-2001
	@JAE
*/
public static int ultimoDomingoMes(int int_mes, int int_anio) throws Exception
{
	int int_numDiasMes;
	int int_ultimo_domingo = 0;
	Calendar cal_fecha;

	cal_fecha = Calendar.getInstance();

	int_numDiasMes = diasMes(int_mes, int_anio);

	// Recorre la �ltima semana del mes y obtiene el n�mero de d�a del �ltimo domingo del mes
	for (int int_dia_mes = int_numDiasMes - 6; int_dia_mes <= int_numDiasMes; int_dia_mes++)
	{
		cal_fecha.setTime(stringToDate(int_dia_mes + "/" + int_mes + "/" + int_anio));
		if (cal_fecha.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
		{
			int_ultimo_domingo = int_dia_mes;
		}
	}
	return int_ultimo_domingo;
}



/**
	Calcula el n�mero de dias de un mes
	@param int, n�mero de mes
	@param int, n�mero de a�o (para calcular mes de Febrero si es bisiesto)
	@return	int, n�mero de dias del mes, (-1 si el n�mero de mes es err�neo)
	@since	13-7-2001
	@JAE
*/
public static int diasMes(int int_mes, int int_anio) throws Exception
{
	int int_diasMes;

	switch (int_mes)
	{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
		{
			int_diasMes = 31;
			break;
		}
		case 4:
		case 6:
		case 9:
		case 11:
		{
			int_diasMes = 30;
			break;
		}
		case 2:
		{
			if (int_anio < 0)
			{
				int_diasMes = -1;
			}
			else
			{
				if (esBisiesto(int_anio))
				{
					int_diasMes = 29;
				}
				else
				{
					int_diasMes = 28;
				}
			}
			break;
		}
		default:
		{
			int_diasMes = -1;
			break;
		}
	}
	return int_diasMes;
}

/**
	Obtiene el nombre de un mes (en may�sculas)
	@param int, n�mero de mes
	@return	String, nombre del mes (null si hay error en el n�mero introducido)
	@since	15-11-2001
	@JAE
*/
public static String nombreMes(int int_mes)
{
	String Str_mes = null;

	switch (int_mes)
	{
		case 1:
		{
			Str_mes = "ENERO";
			break;
		}
		case 2:
		{
			Str_mes = "FEBRERO";
			break;
		}
		case 3:
		{
			Str_mes = "MARZO";
			break;
		}
		case 4:
		{
			Str_mes = "ABRIL";
			break;
		}
		case 5:
		{
			Str_mes = "MAYO";
			break;
		}
		case 6:
		{
			Str_mes = "JUNIO";
			break;
		}
		case 7:
		{
			Str_mes = "JULIO";
			break;
		}
		case 8:
		{
			Str_mes = "AGOSTO";
			break;
		}
		case 9:
		{
			Str_mes = "SEPTIEMBRE";
			break;
		}
		case 10:
		{
			Str_mes = "OCTUBRE";
			break;
		}
		case 11:
		{
			Str_mes = "NOVIEMBRE";
			break;
		}
		case 12:
		{
			Str_mes = "DICIEMBRE";
			break;
		}
	}
	return Str_mes;
}


/**
	Calcula si un a�o es bisiesto
	@param int, n�mero de a�o
	@return	boolean
	@since	13-7-2001
	@JAE
*/
public static boolean esBisiesto(int int_anio) throws Exception
{
	boolean bol_Return = false;

	if ((int_anio % 4) == 0)
	{
		if ((int_anio % 100) == 0)
		{
			if ((int_anio % 400) == 0)
			{
	    		bol_Return = true;
	    	}
	  	}
		else
		{
	  		bol_Return = true;
	  	}
	}

    return bol_Return;
}

/**
	Calcula el n�mero de d�as 29 de febrero (en a�o bisiesto) que hay entre dos fechas
	@param java.sql.Date, fecha de inicio
	@param java.sql.Date, fecha de fin
	@return	int, n�mero de dias 29 febrero entre las dos fechas, 0 si alguna de las fechas es null o la fecha fin es anterior a la de inicio
	@since	22-02-2002
	@JAE
*/
public static int numDiasBisiesto(java.sql.Date dat_fechaInicio, java.sql.Date dat_fechaFin) throws Exception
{
	int int_numDias = 0;

	int int_anioInicio = 0;
	int int_anioFin = 0;
	int int_mesInicio = 0;
	int int_mesFin = 0;

	// S�lo entra si no hay fechas nulas y la fecha fin es posterior a la de inicio
	if ((dat_fechaInicio != null) && (dat_fechaFin != null)
		&& (dat_fechaInicio.compareTo(dat_fechaFin) < 0) )
	{
		int_anioInicio = getAnio(dat_fechaInicio);
		int_anioFin = getAnio(dat_fechaFin);
		int_mesInicio = getMes(dat_fechaInicio);
		int_mesFin = getMes(dat_fechaFin);

		// A�o inicio y fin iguales
		if (int_anioInicio == int_anioFin)
		{
			// Si el mes de inicio es enero o febrero
			if (int_mesInicio <= 2)
			{
				// Si es bisiesto, hay 29 de febrero entre las dos fechas
				if (esBisiesto(int_anioInicio))
				{
					// Si el mes de fin es posterior a febrero
					if (int_mesFin > 2)
					{
						int_numDias++;
					}
					else // Mes fin es Enero o febrero
					{
						// Febrero
						if (int_mesFin == 2)
						{
							// Si el dia es el 29 de febrero, hay un dia
							if (getDia(dat_fechaFin) > 28)
							{
								int_numDias++;
							}
						}
					}
				}
			}
		}
		else // A�o inicio menor que el de fin
		{
			for (int int_anio=int_anioInicio; int_anio<=int_anioFin; int_anio++)
			{
				if (esBisiesto(int_anio))
				{
					// A�O DE INICIO
					if (int_anio == int_anioInicio)
					{
						// Si el mes inicio es enero o febrero
						if (int_mesInicio <= 2)
						{
							int_numDias++;
						}
					}
					// A�OS DE FIN
					else if (int_anio == int_anioFin)
					{
						// Si el mes es posterior a febrero
						if (int_mesFin > 2)
						{
							int_numDias++;
						}
						else
						{
							// Si el mes es febrero
							if (int_mesFin == 2)
							{
								// Si el dia es el 29 de febrero, hay un dia
								if (getDia(dat_fechaFin) > 28)
								{
									int_numDias++;
								}
							}
						}
					}
					// A�OS INTERMEDIOS
					else
					{
						int_numDias++;
					}
				}
			}
		}
	}
	return int_numDias;
}





/**
	Dice si una fecha es alg�n dia de la semana en concreto
	@param java.sql.Date dat_fecha
	@param int, dia de la semana (1 = Lunes ... 7 = Domingo)
	@return	boolean
	@since	16-7-2001
	@JAE
*/
public static boolean esDiaSemana(java.sql.Date dat_fecha, int int_diaSemana) throws Exception
{
	boolean bol_respuesta = false;
	Calendar cal_fecha;

	cal_fecha = Calendar.getInstance();
	cal_fecha.setTime(dat_fecha);
	switch (int_diaSemana)
	{
		case 1: // Lunes
		{
			if (cal_fecha.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
			{
				bol_respuesta = true;
			}
			break;
		}
		case 2: // Martes
		{
			if (cal_fecha.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
			{
				bol_respuesta = true;
			}
			break;
		}
		case 3: // Mi�rcoles
		{
			if (cal_fecha.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
			{
				bol_respuesta = true;
			}
			break;
		}
		case 4: // Jueves
		{
			if (cal_fecha.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
			{
				bol_respuesta = true;
			}
			break;
		}
		case 5: // Viernes
		{
			if (cal_fecha.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
			{
				bol_respuesta = true;
			}
			break;
		}
		case 6: // S�bado
		{
			if (cal_fecha.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
			{
				bol_respuesta = true;
			}
			break;
		}
		case 7: // Domingo
		{
			if (cal_fecha.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
			{
				bol_respuesta = true;
			}
			break;
		}
	}
	return bol_respuesta;
}


/**
	Devuelve el dia de la semana en concreto para la fecha pasada por argumento.
	@param java.sql.Date dat_fecha
	@return	int (constantes: LUNES,MARTES,MIERCOLES,JUEVES,VIERNES,SABADO,DOMINGO)
				-1 en caso de error.
	@since	22-10-2001
	@YLO
*/
public static int getDiaSemana(java.sql.Date dat_fecha)
{
	int int_diaResultado = -1;
	Calendar cal_fecha;

	cal_fecha = Calendar.getInstance();
	cal_fecha.setTime(dat_fecha);

	switch(cal_fecha.get(Calendar.DAY_OF_WEEK) )
	{
		case Calendar.MONDAY:{
				int_diaResultado = LUNES;
				break;
		}

		case Calendar.TUESDAY:
		{
			int_diaResultado = MARTES;
			break;
		}
		case Calendar.WEDNESDAY:
		{
			int_diaResultado = MIERCOLES;
			break;
		}
		case Calendar.THURSDAY:
		{
			int_diaResultado = JUEVES;
			break;
		}
		case Calendar.FRIDAY:
		{
			int_diaResultado = VIERNES;
			break;
		}
		case Calendar.SATURDAY:
		{
			int_diaResultado = SABADO;
			break;
		}
		case Calendar.SUNDAY:
		{
			int_diaResultado = DOMINGO;
			break;
		}
	}
	return int_diaResultado;
}


/**
	Obtiene las dos �ltimas cifras de un entero (�til para los a�os)
	@param int, entero
	@return	String, Dos �ltimas cifras del entero
	@since	16-7-2001
	@JAE
*/
public static String getDosUltimasCifras(int int_numero) throws Exception
{
	String Str_numero;

	if (int_numero < 0)
	{
		int_numero = -int_numero;
	}

	Str_numero = new Integer(int_numero).toString();

	if (Str_numero.length() > 2)
	{
		Str_numero = Str_numero.substring(Str_numero.length() - 2, Str_numero.length());
	}
	else
	{
		while (Str_numero.length() < 2)
		{
			Str_numero = "0" + Str_numero;
		}
	}
	return Str_numero;
}


/**
      Metodo: stringToDate
      Funcion: Pasa un String de fecha en formato "aaaa-mm-dd" al formato "dd/mm/aaaa"
      Return:  String
      Argumentos: String en formato "aaaa-mm-dd"
	  Autor: JAE
*/
	public static java.sql.Date stringFechaToDate(String str_fecha) throws Exception
{
	return stringToDate(str_fecha.substring(8,10) + "/" + str_fecha.substring(5,7) + "/" + str_fecha.substring(0,4));
}




/**
	Convierte un vector en un array de String[]
	@param _vec_vector, vector a transformar
	@return String[][], array
*/
// Author: JDZ - 12/10/2001

public static String[][] vectorToArray(Vector _vec_vector)
{
	String[][] str_arrayVuelta=null;
	Object[] obj= null;

	if ((_vec_vector!=null) && (_vec_vector.size()>0))
	{
		obj = (Object[])_vec_vector.toArray();
		str_arrayVuelta = new String[obj.length][];
		for (int i=0; i < obj.length; i++)
			str_arrayVuelta[i] = (String[])obj[i];
	}

	return str_arrayVuelta;
}

/**
	Redondea un double a un n�mero de decimales especificado
	@param double, n�mero
	@param int, n�mero de decimales
	@return double, resultado del redondeo
*/
// Author: JAE - 10/10/2001

public static double redondea(double dou_numero, int int_decimales)
{
	String Str_numero = new Double(dou_numero).toString();
	String Str_decimales = null;
	int int_posicionComa = 0;

	// Obtiene la posicion del punto decimal
	int_posicionComa = Str_numero.indexOf('.');

	// Si hay decimales
	if (int_posicionComa != -1)
	{
		int int_numDecimales = 0;

		// Obtiene la cadena de decimales
		Str_decimales = Str_numero.substring(int_posicionComa + 1);

		int_numDecimales = Str_decimales.length();

		// Si los decimales que se quieren obtener son menos de los que tiene el n�mero
		if (int_decimales < int_numDecimales)
		{
			// Si el �ltimo decimal es 5
			if (Str_decimales.substring(Str_decimales.length() - 1).equals("5"))
			{
				// Concatena un nueve para que al redondear vaya hacia 'arriba'
				Str_numero = Str_numero + "9";
				dou_numero = new Double(Str_numero).doubleValue();
			}
		}
	}

	BigDecimal a = new BigDecimal(dou_numero).setScale(int_decimales, BigDecimal.ROUND_HALF_UP);;
	return a.doubleValue();
}


/**
	Convierte de euros a pesetas
	@param double, importe en euros
	@return long, resultado en peseatas
*/
// Author: JDZ - 12/10/2001

public static double deEuroAPeseta(double dou_importe)
{
	return dou_importe*PESETAS_UN_EURO;
}




/**
        Hace la division real de los dos paramtros
        @param double
        @param double
        @return double, resultado de la division
*/
// Author: JDZ
public static double divide(double _dou_dividendo, double _dou_divisor)
{
        int PRECISION = 20; // N�mero de decimales de precisi�n

        BigDecimal a = new BigDecimal(_dou_dividendo).setScale(PRECISION, BigDecimal.ROUND_HALF_UP);
        BigDecimal b = new BigDecimal(_dou_divisor).setScale(PRECISION, BigDecimal.ROUND_HALF_UP);
        return (a.divide(b, BigDecimal.ROUND_HALF_UP)).doubleValue();
}



/**
	Convierte de pesetas a euros
	@param double, importe en peseta
	@return long, resultado en euros
*/
// Author: JDZ - 12/10/2001


public static double dePesetaAEuro(double dou_importe)
{
	return divide(dou_importe, (double)PESETAS_UN_EURO);
}


/**
	Convierte de euros a pesetas
	@param double, importe en euros
	@return long, resultado en peseatas
*/
// Author: JDZ - 12/10/2001

public static double deCentiEuroAPeseta(double dou_importe)
{
	return divide(dou_importe, (double)100)*PESETAS_UN_EURO;
}


/**
	Convierte de pesetas a euros
	@param double, importe en peseta
	@return long, resultado en euros
*/
// Author: JDZ - 12/10/2001

public static double deCentiEuroAEuro(double dou_importe)
{
	return divide(dou_importe, (double)100);
}

/*
	Convierte el importe que est� en la moneda de entrada a un importe en la moneda de salida
	@param _strMonedaSalida
	@param _str_monedaEntrada
	@param _dou_importe
	@return double, nuevo importe
	@exception Exception
	@author: JDZ
	@sinze: 18/10/2001
 */
public static double unificarMoneda(String _strMonedaSalida, String _str_monedaEntrada, double _dou_importe) throws Exception
{
	double dou_newImporte=0;

	if (_strMonedaSalida.equals(TIPO_MONEDA_PESETA))
		if (_str_monedaEntrada.equals(TIPO_MONEDA_EURO))
			dou_newImporte = deEuroAPeseta(_dou_importe);
		else if	(_str_monedaEntrada.equals(TIPO_MONEDA_CENTIEURO))
			dou_newImporte = deCentiEuroAPeseta(_dou_importe);
		else
			dou_newImporte = _dou_importe;

	if (_strMonedaSalida.equals(TIPO_MONEDA_EURO))
		if (_str_monedaEntrada.equals(TIPO_MONEDA_PESETA))
			dou_newImporte = dePesetaAEuro(_dou_importe);
		else if (_str_monedaEntrada.equals(TIPO_MONEDA_CENTIEURO))
			dou_newImporte = deCentiEuroAEuro(_dou_importe);
		else
			dou_newImporte = _dou_importe;

	return dou_newImporte;
}


/**
	Inserta blancos en la cadena origen a la izquierda o derecha seg�n el valor de la variable 'boo_izquierda'
		hasta completar el tama�o indicado por 'int_size'.
	@param String Str_origen.
	@param boolean boo_izquierda.
			toma valores seg�n las constantes: IZQUIERDA,DERECHA
	@param int int_size.
	@return String
*/
// Author: YLO - 22/11/2001
public static String insertarBlancos(String Str_origen,
									boolean boo_izquierda,
									int int_size)
{
	String Str_resultado = "";

	for (int int_i = 0; int_i < (int_size - Str_origen.length()) ; int_i++)
	{
		Str_resultado +=" ";
	}

	if (boo_izquierda)
	{
		Str_resultado = Str_resultado+Str_origen;
	}
	else
	{
		Str_resultado = Str_origen+Str_resultado;
	}

	// System.out.println("Str_resultado: "+Str_resultado+" Str_resultado.length(): "+Str_resultado.length()+" Str_origen: "+Str_resultado+" int_size: "+int_size+" boo_izq: "+boo_izquierda);

	return Str_resultado;
}


/**
	Recibe un array de String[][] y devuelve un array simple para javascript
	@param String[][], datos en filas y columnas
	@param int, columna con la que se quiere formar el array
	@return String, array para javascript
*/
// Author: JAE - 13/12/2001
public static String arrayJavaScript(String[][] Str_datos, int int_columna)
{
	String ret = "[]";

	if (Str_datos != null && Str_datos.length != 0) {
		StringBuffer sb = new StringBuffer("");

		int tam = Str_datos.length - 1;

		sb.append("[");

		for (int i = 0; i < tam; i++) {
			sb.append("'");
			sb.append(Str_datos[i][int_columna]);
			sb.append("',");
		}
		sb.append("'");
		sb.append(Str_datos[tam][int_columna]);
		sb.append("']");

		ret = sb.toString();
	}
	return ret;
}



/**
	Sustituir en la cadena 'Str_cadenaOrigen' la cadena 'Str_subcadena' por el String 'Str_nuevo'.
	@param String Str_cadenaOrigen.
	@param String Str_subcadena.
	@param String Str_nuevo.
	@return String.
*/
// Author: YLO - 20/12/2001
public static String replace(String Str_cadenaOrigen, String Str_subcadena, String Str_nuevo)
{
	String Str_resultado ="";

	int int_posInicio = 0;
	int int_posFin = 0;

	while (int_posFin != -1)
	{
		int_posFin = Str_cadenaOrigen.indexOf(Str_subcadena,int_posInicio);

		// System.out.println("int_posInicio: "+int_posInicio+" int_posFin: "+int_posFin);

		if (int_posFin != -1)
		{
			Str_resultado += Str_cadenaOrigen.substring(int_posInicio,int_posFin)+Str_nuevo;
			// System.out.println("Str_resultado: "+Str_resultado+" int_posInicio: "+int_posInicio+" int_posFin: "+int_posFin);
			int_posInicio = int_posFin+Str_subcadena.length();
			// int_posFin += Str_subcadena.length();
		}
		else
		{
			Str_resultado += Str_cadenaOrigen.substring(int_posInicio,Str_cadenaOrigen.length());
		}

	}

	return Str_resultado;
}


	/**
		M�todo que crea un array de objetos 'java.sql.Date' con los meses comprendidos
		entre la fechaInicio y fechaFin pasadas como argumentos. Los dias de los objetos
		del array corresponden al ultimo d�a de cada mes en concreto.
		@param java.sql.Date date_fechaInicio
		@param java.sql.Date date_fechaFin
		@return java.sql.Date []
		@since 5/02/02
	*/
	// YLO.
	public static java.sql.Date[] descomponerEnMeses(java.sql.Date date_fechaInicio,java.sql.Date date_fechaFin)
	throws Exception
	{
		java.sql.Date [] meses = new java.sql.Date [0];
		Vector Vec_resultado = new Vector();
		int int_anioInicio = Utilidades.getAnio(date_fechaInicio);
		int int_anioFin = Utilidades.getAnio(date_fechaFin);
		int int_mesInicio = Utilidades.getMes(date_fechaInicio);
		int int_mesFin = Utilidades.getMes(date_fechaFin);
		int int_dias = 1;
		int int_i = 0; // contador auxiliar.

		if (int_anioInicio > int_anioFin)
		{
			return meses;
		}

		if (int_anioInicio == int_anioFin)
		{
			for (int_i = int_mesInicio; int_i<=int_mesFin; int_i++)
			{
				int_dias = Utilidades.diasMes(int_i,int_anioFin);
				Vec_resultado.addElement(Utilidades.stringToDate(int_dias+"/"+int_i+"/"+int_anioFin));
			}
		}
		else
		{
			for (int_i = int_mesInicio; int_i <= 12; int_i++)
			{
				int_dias = Utilidades.diasMes(int_i,int_anioInicio);
				Vec_resultado.addElement(Utilidades.stringToDate(int_dias+"/"+int_i+"/"+int_anioInicio));
			}

			int_anioInicio++;

			while (int_anioInicio < int_anioFin)
			{
				for (int_i = 1; int_i <=12; int_i++)
				{
					int_dias = Utilidades.diasMes(int_i,int_anioInicio);
					Vec_resultado.addElement(Utilidades.stringToDate(int_dias+"/"+int_i+"/"+int_anioInicio));
				}
				int_anioInicio++;

			}

			for (int_i = 1; int_i <= int_mesFin; int_i++)
			{
				int_dias = Utilidades.diasMes(int_i,int_anioInicio);
				Vec_resultado.addElement(Utilidades.stringToDate(int_dias+"/"+int_i+"/"+int_anioFin));
			}
		}

		meses = new java.sql.Date [Vec_resultado.size()];
		for (int_i = 0; int_i < meses.length; int_i++)
		{
			meses[int_i] = (java.sql.Date)Vec_resultado.elementAt(int_i);
		}

		return meses;
	}

	/**
      Metodo: dateToStringFormatoFecha
      Funcion: Pasa un Date a String en el formato aaaa-mm-dd
      Return:  String
      Argumentos: java.sql.Date
	  Autor: AVE
*/
public static String dateToStringFormatoFecha(java.sql.Date dat_fecha) throws Exception
{
	Calendar cal_fecha;
	int int_anio, int_mes, int_dia;
	String str_fecha;
	String str_anio = "";
	String str_mes = "";
	String str_dia = "";

	if ((String.valueOf(dat_fecha)).equals("null"))
		str_fecha = "null";
	else
	{
		cal_fecha = Calendar.getInstance();
		cal_fecha.setTime(dat_fecha);

		int_anio = cal_fecha.get(cal_fecha.YEAR);
		int_mes = (cal_fecha.get(cal_fecha.MONTH)) + 1;
		int_dia = cal_fecha.get(cal_fecha.DAY_OF_MONTH);

		str_anio = new Integer(int_anio).toString();
		str_mes = new Integer(int_mes).toString();
		str_dia = new Integer(int_dia).toString();
		while (str_anio.length() < 4)
		{
			str_anio = "0" + str_anio;
		}
		while (str_mes.length() < 2)
		{
			str_mes = "0" + str_mes;
		}
		while (str_dia.length() < 2)
		{
			str_dia = "0" + str_dia;
		}

		str_fecha = str_anio +"-" + str_mes + "-" + str_dia;
	}

	return str_fecha;
}

/**
        Cambia un double en string en formato 'exponencial' a
        su n�mero con todas sus cifras
        Ejemplos: 1.234E6 -> 1234000; 1.23E-1 -> 0.123; 110 -> 110
        @param String
        @return String
*/
// Author: JAE - 12/10/2001

public static String cambiarFormato(String Str_double)
{
        final char E = 'E';
        final char SIGNO_MENOS = '-';
        final char PUNTO = '.';
        int int_posicionMenos = 0;
        int int_posicionE = 0;
        int int_exponente = 0;
        String Str_signo = "";
        String Str_resultado = null;
        boolean boo_tieneE = false;


        // Comprueba si tiene exponente y lo obtiene
        int_posicionE = Str_double.indexOf(E);
        if (int_posicionE != -1)
        {
                int_exponente = new Integer(Str_double.substring(int_posicionE + 1)).intValue();
                boo_tieneE = true;
        }

        // Calcula el resultado
        if (boo_tieneE)
        {
                int int_posicionPunto = 0;
                int int_posicionFinalPunto = 0;
                String Str_cadena = "";
                String Str_numeroSinE = "";
                String Str_resultadoConCeros = "";

                // Comprueba si tiene signo menos
                int_posicionMenos = Str_double.indexOf(SIGNO_MENOS);
                if (int_posicionMenos == 0)
                {
                        Str_signo = "-";
                        // Obtiene el n�mero sin E y sin signo
                        Str_numeroSinE = Str_double.substring(1, int_posicionE);
                }
                else
                {
                        Str_signo = "";
                        // Obtiene el n�mero sin E y sin signo
                        Str_numeroSinE = Str_double.substring(0, int_posicionE);
                }

                // Obtiene la posicion del punto en el numero sin E
                int_posicionPunto = Str_numeroSinE.indexOf(PUNTO);

                // Comprueba si no tiene punto
                if (int_posicionPunto != -1)
                {
                        // Si hay punto lo elimina de la cadena
                        String Str_subcadena1 = Str_numeroSinE.substring(0, int_posicionPunto);
                        String Str_subcadena2 = Str_numeroSinE.substring(int_posicionPunto + 1);
                        Str_numeroSinE = Str_subcadena1 + Str_subcadena2;
                }
                else
                {
                        // Si no hay punto, fija su posicion al final de la cadena
                        int_posicionPunto = Str_numeroSinE.length();
                }

                // Calcula la posicion final del punto tras aplicar el exponente
                int_posicionFinalPunto = int_posicionPunto + int_exponente;

                Str_cadena = Str_numeroSinE;

                if (int_posicionFinalPunto <= 0)
                {
                        int int_numCeros = 0;
                        int_numCeros = -int_posicionFinalPunto;
                        for (int i=0; i<int_numCeros; i++)
                        {
                                Str_cadena = "0" + Str_cadena;
                        }
                        Str_cadena = "0." + Str_cadena;
                }
                else if ((int_posicionFinalPunto > 0) && (int_posicionFinalPunto < Str_numeroSinE.length()))
                {
                        String Str_subcadena1 = Str_cadena.substring(0, int_posicionFinalPunto);
                        String Str_subcadena2 = Str_cadena.substring(int_posicionFinalPunto);
                        Str_cadena = Str_subcadena1 + "." + Str_subcadena2;
                }
                else
                {
                        int int_numCeros = 0;
                        int_numCeros = int_posicionFinalPunto - Str_cadena.length();
                        for (int i=0; i<int_numCeros; i++)
                        {
                                Str_cadena = Str_cadena + "0";
                        }
                }

                // Elimina los ceros a la izquierda
                Str_resultadoConCeros = Str_cadena;
                while ( (Str_resultadoConCeros.substring(0, 1).equals("0"))
                        && (!Str_resultadoConCeros.substring(1, 2).equals(".")))
                {
                        Str_resultadoConCeros = Str_resultadoConCeros.substring(1);
                }

                // Elimina los ceros a la derecha (en el caso de haber decimales)
                if (Str_resultadoConCeros.indexOf(PUNTO) != -1)
                {
                        while (Str_resultadoConCeros.substring(Str_resultadoConCeros.length() - 1).equals("0"))
                        {
                                Str_resultadoConCeros = Str_resultadoConCeros.substring(0, Str_resultadoConCeros.length() - 1);
                        }
                        // Si el ultimo caracter que queda es un punto, se lo quita
                        if (Str_resultadoConCeros.substring(Str_resultadoConCeros.length() - 1).equals("."))
                        {
                                Str_resultadoConCeros = Str_resultadoConCeros.substring(0, Str_resultadoConCeros.length() - 1);
                        }
                }
                Str_cadena = Str_resultadoConCeros;

                // Pone el resultado y el signo
                Str_resultado = Str_signo + Str_cadena;
        }
        else // No tiene E
        {
                Str_resultado = Str_double;
        }

        return Str_resultado;
}


/**
   Metodo:  cambiarFormato
             Funcion: Cambia los separadores de decimales y millares, seg�n
                                     el idioma utilizado.(123,123,123.12 � 123.123.123,12)
   Return:  String (Formato del n�mero pasado por argumento segun el idioma
                                                   seleccionado.
                                                   )
   Argumentos:
           String Str_numero	(IN) : Numero que debe cambiar el formato
                     String Str_cadenaSepara	(IN) : caracter separador de los millares.
                     String Str_cadenaDecimal(IN) : caracter separador de los decimales.
                     int    int_nDecimales (IN):    N� decimales
*/

     public static String cambiarFormato(String Str_numero,
                                                                                     String Str_cadenaSepara,
                                                                                     String Str_cadenaDecimal,
                                                                                     int int_nDecimales) throws Exception{

             String Str_res = new String();
             int int_contador=0;
             int int_i=0;
             String Str_numeroEntero = new String();
             String Str_numeroDecimal = new String();



             if(Str_numero == null){
                     Str_numero = new String();
             }

             int int_posDecimal = Str_numero.indexOf('.');

             if(int_posDecimal == -1){
                     // El numero es entero.
                     Str_numeroEntero = Str_numero;

             }
             else{
                     Str_numeroEntero = Str_numero.substring(0,int_posDecimal);
//			try{
                             Str_numeroDecimal = Str_numero.substring(int_posDecimal+1,Str_numero.length());
//			}
//			catch(Exception e){
                             // No hay nada despues del caracter de los decimales.
                             //System.out.println("catch 1.");
//			}
             }

             if (Str_numeroDecimal.length() > int_nDecimales){
                     Str_numeroDecimal = Str_numeroDecimal.substring(0,int_nDecimales);
             }
             else{
                     if(Str_numeroDecimal.length() < int_nDecimales){
                             for(int_i= (int_nDecimales - Str_numeroDecimal.length() ); int_i >0 ; int_i--){
                                     Str_numeroDecimal+="0";
                             }
                     }
             }

             int_i = Str_numeroEntero.length()-1;

             if(Str_numeroEntero.length() == 0){
                     Str_res="0";
             }
             else{
                     while(int_i >= 0 ){
                             if( int_contador < 3 ){
                                     // a�adir el caracter al resultado.
                                     Str_res +=Str_numeroEntero.charAt(int_i);
                                     int_contador++;
                                     int_i--;
                             }
                             else{
                                     int_contador = 0;
                                     if (Str_numeroEntero.charAt(int_i) != '-'){
                                             Str_res +=Str_cadenaSepara;
                                     }
                             }
                     }
             }

             // La cadena resultante 'Str_res', esta en orden inverso.

             if(	Str_res.length() > 0
                     && Str_res.substring(0,1).equals(Str_cadenaSepara) ) {
                             Str_res = Str_res.substring(0,Str_res.length());
             }

             Str_res = invertir(Str_res);

             if(Str_numeroDecimal.length() > 0){
                     // Hay parte decimal.
                     Str_res += Str_cadenaDecimal+Str_numeroDecimal;
             }

             return Str_res;

	}

/**
      Metodo: cambiar formato (. por comas y a�adir los separadores de miles), dado
      Funcion: Pasa un Date a String en el formato aaaa-mm-dd
      Return:  String Str_dato con el nuevo formato
      Argumentos: String Str_numero
	  Autor: AVE, 03/04/2002
*/
public static String cambiarPuntosComasMiles(String Str_numero) throws Exception
{
	//Declaraci�n de variables
	int int_posDecimal, int_numDecimales;
	String Str_numeroDecimal, Str_dato;
	//Fin declaraci�n de variables

	int_posDecimal = Str_numero.indexOf('.');
	if (int_posDecimal!=-1)
	{
		//El n�mero tiene decimales
		Str_numeroDecimal = Str_numero.substring(int_posDecimal+1,Str_numero.length());
		int_numDecimales = Str_numeroDecimal.length();
	}
	else
	{
		//No ha decimales
		int_numDecimales=0;
	}
	Str_dato = cambiarFormato(Str_numero);
	Str_dato = cambiarFormato(Str_dato, ".", ",", int_numDecimales);

	return Str_dato;
}

public static PrintWriter AbreFicheroLog() {
  PrintWriter p_log = null;
  try {
    String fecha = dateToString(fechaActual());
    String dia = fecha.substring(0,2);
    String mes = fecha.substring(3,5);
    String anno = fecha.substring(6,10);
    String nombre_log = anno + mes + dia + ".log";
    p_log = new PrintWriter (new FileWriter (nombre_log, true), true);
  }catch (Exception e) {
  }
  return p_log;
}

public static String GetContentType(String extension) {
		  if (extension.toUpperCase().equals(".RTF")){
				return "application/rtf";
			}
			else if (extension.toUpperCase().equals(".XLS")){
				return "application/vnd.ms-excel";
			}
			else if (extension.toUpperCase().equals(".PDF")){
				return "application/pdf";
      }
			else if (extension.toUpperCase().equals(".JPG")){
				return "imagen/jpeg";
      }
			else if (extension.toUpperCase().equals(".GIF")){
				return "imagen/gif";
      }
			else if (extension.toUpperCase().equals(".AVI")){
				return "video/x-msvideo";
      }
			else if (extension.toUpperCase().equals(".MPG")){
				return "video/mpeg";
      }
			else {
				return "application/msword";
      }
}


/* ***************************************************************************************
  Metodo main para pruebas. Desactivar o anular este metodo cuando la clase este operativa.
  *************************************************************************************** */

	public static void main(String [] args){

		try
		{
			System.out.println("redondea:"+new Double(redondea(1310.8, 0)).longValue());
			//System.out.println("cambiarPuntosComasMiles:"+cambiarPuntosComasMiles("1310.0"));
		}
		catch(Exception e)
		{
			System.out.println("catch:: "+e);
		}
		/*
		try
		{

			java.sql.Date date_fechaInicio = Utilidades.stringToDate("12/4/2003");
			java.sql.Date date_fechaFin = Utilidades.stringToDate("12/6/2002");

			java.sql.Date[] meses = null;

			meses = descomponerEnMeses(date_fechaInicio,date_fechaFin);

			for (int int_i = 0 ; int_i < meses.length; int_i++)
			{
				System.out.println("int_i: "+int_i+" meses: "+meses[int_i]);
			}
		}
		catch(Exception e)
		{
			System.out.println("catch:: "+e);
		}
		*/
/*
		try
		{
			System.out.println(numDiasBisiesto(stringToDate("1/3/2000"), stringToDate("28/2/2008")));
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
*/
/*
		try
		{
			java.sql.Date date = stringToDate("25/10/2001");
			System.out.println(getDiaSemana(date));
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
*/
		/*

		int int_anio = 2000;
		try
		{
			System.out.println("int_anio:: "+int_anio+" dia del cambio horario:: "+cambioHorarioComienzoVerano(int_anio));
		}
		catch(Exception e)
		{
			System.out.println("catch main:: "+e);
		}
		*/
		// System.out.println("esAnterior??: "+esAnterior(new Date(2001,7,1),new Date(2001,6,2)));

		/*
		long lon_incrementoDias = (long)365*(long)24*(long)3600*(long)1000;
		calcularPeriodoFecha(new java.sql.Date(2001,6,28),lon_incrementoDias);
		*/
/*
		int int_dias = getDiferenciaFechas(new Date(2400,0,1),new Date(2401,0,1));
		System.out.println("int_dias: "+int_dias);

		System.out.println("fecha de hoy: "+fechaActual());
*/
		 /*Vector vecItem= new Vector();
	     Vector vecValo= new Vector();
	     vecItem.addElement("Nombre de la empresa");
	     vecItem.addElement("Mes");
	     vecValo.addElement("Uni�n Fenosa");
	     vecValo.addElement("Enero");*/
	     //Utilidades.reemplaza("c:/alicia/resu.doc", "c:/alicia/terminando.doc", vecItem, vecValo);

		 /*try
		 {
			 System.out.println(cambiarComa("25,4"));
		 }
		 catch(Exception e)
		 {}
		 */

		/*
		 try
		 {
			 System.out.println(quitarPuntos("023,24"));
		 }
		 catch(Exception e)
		 {}
		*/

		/*
		try
		{
			Calendar Cal_fechaActual;
			Cal_fechaActual = Calendar.getInstance();
			java.sql.Date dat_dia;

			dat_dia = stringToDate("25/11/001");

			System.out.println(String.valueOf(dat_dia));

			String str_dia = dateToString(dat_dia);

			System.out.println(str_dia);

		}
		catch (Exception e)
		{}
		*/

		/*
		try
		{
			Calendar Cal_fechaActual;
			Cal_fechaActual = Calendar.getInstance();
			java.sql.Date dat_dia;

			dat_dia = stringToDate("25/12/2001");

			String str_dia = dateToString(dat_dia);

			System.out.println("DIA = " + getDia(dat_dia));
			System.out.println("MES = " + getMes(dat_dia));
			System.out.println("A�O = " + getAnio(dat_dia));

		}
		catch (Exception e)
		{}
		*/

/*		try
		{
			System.out.println(esDiaSemana(stringToDate("26/2/2000"),2));
			System.out.println(esHorarioVerano(stringToDate("29/2/2000")));
		}
		catch (Exception e)
		{}
*/

/*
		try
		{
			java.sql.Date dat_fecha = stringToDate("1/1/2005");
			java.sql.Date dat_fecha2 = stringToDate("1/1/2001");
			System.out.println(calcularPeriodoFecha(dat_fecha, 2, 1));
			System.out.println("!");
			System.out.println("Diferencia =" +getDiferenciaFechas(dat_fecha,dat_fecha2));

		}
		catch (Exception e)
		{
			System.out.println(e);
		}
*/

	} // main

}	// clase



