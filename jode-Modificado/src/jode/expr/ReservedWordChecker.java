/*
 * Created on 12-ene-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package jode.expr;

/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ReservedWordChecker {
	static String[] rw={
			"do","if","goto","while","switch","for",
			"else","int","case","byte","try","char",
			"new","null","long","void"
	};
	public static String mutate(int tipo, String in){
		boolean rwm=false;
		for(int i=0;i<rw.length;i++){
			if(in.equals(rw[i])){
				rwm=true;
				break;
			}
		}
		String prefix="";
		if(rwm){
			switch(tipo){
			case 1: prefix="_mth"; break;
			case 2: prefix="_fld"; break;
			}
		}
		return prefix+in;
	}
}
