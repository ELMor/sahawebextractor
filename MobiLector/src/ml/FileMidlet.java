package ml;

import java.io.IOException;
import java.util.Enumeration;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;
import javax.microedition.io.file.FileSystemRegistry;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextBox;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.MIDlet;

public class FileMidlet extends MIDlet implements CommandListener {
	private String currDirName;

	private Command view = new Command("View", Command.ITEM, 1);
	private Command back = new Command("Back", Command.BACK, 2);
	private Command exit = new Command("Exit", Command.EXIT, 3);

	private final static String UP_DIRECTORY = "..";
	private final static String MEGA_ROOT = "/";
	private final static String SEP_STR = "/";
	private final static char SEP = '/';

	private HttpServer hs=null;
	
	public FileMidlet() {
		currDirName = MEGA_ROOT;
	}
	
	public static void Log(String txt,boolean nl){
		if(nl)
			System.out.println(txt);
		else
			System.out.print(txt);
		System.out.flush();
	}

	public void startApp() {
		boolean isAPIAvailable = false;
		if (System.getProperty("microedition.io.file.FileConnection.version") != null) {
			isAPIAvailable = true;
			try {
				showCurrDir();
				hs=new HttpServer(null);
				hs.start();
			} catch (SecurityException e) {
			} catch (Exception e) {
			}
		} else {
			StringBuffer splashText = new StringBuffer(
					getAppProperty("MIDlet-Name")).append("\n").append(
					getAppProperty("MIDlet-Vendor")).append(
					isAPIAvailable ? "" : "\nFileConnection API not available");
			Alert splashScreen = new Alert(null, splashText.toString(), null,
					AlertType.INFO);
			splashScreen.setTimeout(3000);
			Display.getDisplay(this).setCurrent(splashScreen);
		}

	}

	public void pauseApp() {
	}

	public void destroyApp(boolean cond) {
		notifyDestroyed();
	}

	public void commandAction(Command c, Displayable d) {
		if (c == view) {
			List curr = (List) d;
			final String currFile = curr.getString(curr.getSelectedIndex());
			new Thread(new Runnable() {
				public void run() {
					if (currFile.endsWith(SEP_STR)
							|| currFile.equals(UP_DIRECTORY)) {
						traverseDirectory(currFile);
					} else {
						showFile(currFile);
					}
				}
			}).start();
		} else if (c == back) {
			showCurrDir();
		} else if (c == exit) {
			destroyApp(false);
		}
	}

	void showCurrDir() {
		Enumeration e;
		FileConnection currDir = null;
		List lista;
		try {
			if (MEGA_ROOT.equals(currDirName)) {
				e = FileSystemRegistry.listRoots();
				lista = new List(currDirName, List.IMPLICIT);
			} else {
				currDir = (FileConnection) Connector.open("file://localhost/"
						+ currDirName);
				e = currDir.list();
				lista = new List(currDirName, List.IMPLICIT);
				lista.append(UP_DIRECTORY, null);
			}
			while (e.hasMoreElements()) {
				String fileName = (String) e.nextElement();
				if (fileName.charAt(fileName.length() - 1) == SEP) {
					lista.append(fileName, null);
				} else {
					lista.append(fileName, null);
				}
			}
			lista.setSelectCommand(view);
			lista.addCommand(exit);
			lista.setCommandListener(this);
			if (currDir != null) {
				currDir.close();
			}
			Display.getDisplay(this).setCurrent(lista);
		} catch (IOException ioe) {
		}
	}

	void traverseDirectory(String fileName) {
		if (currDirName.equals(MEGA_ROOT)) {
			if (fileName.equals(UP_DIRECTORY)) {
				// can not go up from MEGA_ROOT
				return;
			}
			currDirName = fileName;
		} else if (fileName.equals(UP_DIRECTORY)) {
			// Go up one directory
			// TODO use setFileConnection when implemented
			int i = currDirName.lastIndexOf(SEP, currDirName.length() - 2);
			if (i != -1) {
				currDirName = currDirName.substring(0, i + 1);
			} else {
				currDirName = MEGA_ROOT;
			}
		} else {
			currDirName = currDirName + fileName;
		}
		showCurrDir();
	}

	void showFile(String fileName) {
		try {
			if (fileName.toLowerCase().endsWith(".epub")) {
				EpubBook er = new EpubBook("file://localhost/"+ currDirName + fileName,4096);
				hs.setBook(er);
				String content="Arrancando Navegador";
				TextBox tb = new TextBox(
						"View File: " + fileName, null, content.length(),
						TextField.ANY | TextField.UNEDITABLE);
	
				tb.addCommand(back);
				tb.addCommand(exit);
				tb.setCommandListener(this);
	
				if (content.length()> 0) {
					tb.setString(content);
				}
				Display.getDisplay(this).setCurrent(tb);
				platformRequest("http://localhost:8080/");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}