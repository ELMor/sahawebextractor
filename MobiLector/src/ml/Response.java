package ml;

import java.io.IOException;
import java.io.OutputStream;

/*
 HTTP Response = Status-Line
 *(( general-header | response-header | entity-header ) CRLF)
 CRLF
 [ message-body ]
 Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
 */

public class Response {

	Request request;
	OutputStream output;
	EpubBook book=null;

	public Response(OutputStream output, EpubBook book) {
		this.output = output;
		this.book=book;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public void sendStaticResource() throws IOException {
		try {
			EpubBook.Resource rsc=book.new Resource(request.getUri());
			String hstat=null;
			if(rsc.existe()){
				hstat="HTTP/1.1 200 OK\n";
				String mt=rsc.getType();
				hstat+="Content-Type: "+mt+"; charset=UTF-8\n";
				byte cnt[]=rsc.getContent();
				hstat+="Content-Length: "+cnt.length+"\n";
				hstat+="Connection: close\n";
				hstat+="\n";
				sendIS(hstat.getBytes());
				sendIS(cnt);
			}else{
				hstat = "HTTP/1.1 404 File Not Found\r\n"
						+ "Content-Type: text/html\r\n"
						+ "Content-Length: 23\r\n" + "\r\n"
						+ "<h1>File Not Found</h1>";
				sendIS(hstat.getBytes());
			}
			FileMidlet.Log(hstat,true);
		} catch (Exception e) {
			// thrown if cannot instantiate a File object
			FileMidlet.Log(e.toString(),true);
		}
	}

	private void sendIS(byte bytes[]) throws IOException {
		output.write(bytes);
	}
}