package ml;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.ServerSocketConnection;
import javax.microedition.io.StreamConnection;

public class HttpServer extends Thread {

// shutdown command
  private static final String SHUTDOWN_COMMAND = "/SHUTDOWN";

  // the shutdown command received
  private boolean shutdown = false;

  //ebook a servir
  private EpubBook book=null;
  
  public HttpServer(EpubBook book){
	  this.book=book;
  }
  
  public void setBook(EpubBook book){
	  this.book=book;
  }
  
  public void run() {
    ServerSocketConnection serverSocket = null;
    try {
      serverSocket =  (ServerSocketConnection)Connector.open("socket://:8080");
    }
    catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }

    // Loop waiting for a request
    while (!shutdown) {
      StreamConnection socket = null;
      try {
    	FileMidlet.Log("Listening...",false); 
        socket = serverSocket.acceptAndOpen();
        FileMidlet.Log("Accepted",true);
        new Thread(new InnerThread(socket)).start();
      }
      catch (Exception e) {
        e.printStackTrace();
        continue;
      }
    }
    FileMidlet.Log("Exit from httpserver.",true);
  }
  
  public class InnerThread implements Runnable {
	  StreamConnection socket=null;
	  public InnerThread(StreamConnection sc){
		  socket=sc;
	  }
	  public void run() {
		  try {
	      InputStream input = null;
	      OutputStream output = null;
	        input = socket.openInputStream();
	        output = socket.openOutputStream();
	        Request request=null;
	        request = new Request(input);
	        request.parse();
	        // create Response object
	        Response response = new Response(output,book);
	        response.setRequest(request);
	        response.sendStaticResource();
	        FileMidlet.Log("Servido"+"'"+request.getUri()+"'",true);
	        // Close the socket
	        socket.close();
	        //check if the previous URI is a shutdown command
	        shutdown = request.getUri().equals(SHUTDOWN_COMMAND);
		  }catch(Exception e){
			  e.printStackTrace();
		  }
	  }
  }
}


