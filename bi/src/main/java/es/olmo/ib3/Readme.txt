java -Xms512M -Xmx760M -jar ib.jar -b BUD [-r ROS] [-t YYMMDD-HHMMSS] [-d zipfile] Command  [FileSpec]+

(If you use ib.exe, then -Xms512M -Xmx760M are added to VM)

Commands (precede by '-'):
==========================
 'u': Update. Take a snapshot of ROS/FileSpec into BUD/FileSpec. FileSpec can hold a relative Path, must end in a dir. If no FileSpec is specified, then all dirs on ROS are processed.

 'l': List. Search whole BUD for list *FileSpec*.

 'x': Extract. Search whole BUD for extract *FileSpec*.

 's': Stat. Recalculate all STAT.gz files.

 'p': Portable. Create a zip holding STAT.gz files to use for incremental Backup without master copy.

Mandatory:
==========
 'b': Specify root of the Backup Dir, destination of data, snapshot holder.
	
Non Mandatory:
==============
 'r': Root of Source Directory. (Mandatory for -u)

 't': Specify a date	

 'd': Destination, a zip file.

 Options can be written in a per-dir schema in a file named '.IncBackup.properties' (yes, its name begins with a '.')

Options can be (one per line):
 Ignore = true|false 
 	If true, directory is skipped during backup. Default false.
 Exclude = (Pattern)
	Ignore all files matching (Pattern). You can use multiple Exclude.
 Include = (Pattern)
	Only files matching (Pattern) will be backed up. Yo can use multiple Include.
	
