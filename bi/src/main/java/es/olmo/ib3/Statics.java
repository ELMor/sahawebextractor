package es.olmo.ib3;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

public class Statics {
	public static final Hashtable<String, Integer> tblCmp=new Hashtable<>();
	static {
		tblCmp.put(".mp3" ,1); tblCmp.put(".tgz" ,1); tblCmp.put(".avi" ,1);
		tblCmp.put(".ogg" ,1); tblCmp.put(".tbz" ,1); tblCmp.put(".mp4" ,1);
		tblCmp.put(".wma" ,1); tblCmp.put(".rar" ,1); tblCmp.put(".mkv" ,1);
		tblCmp.put(".zip" ,1); tblCmp.put(".7z"  ,1); tblCmp.put(".wmv" ,1);
		tblCmp.put(".png" ,1); tblCmp.put(".cbr" ,1); tblCmp.put(".m3a" ,1);
		tblCmp.put(".jpeg",1); tblCmp.put(".cbz" ,1); tblCmp.put(".apk" ,1);
		tblCmp.put(".epub",1); tblCmp.put(".gz"  ,1); tblCmp.put(".pak" ,1);
		tblCmp.put(".m4a" ,1); tblCmp.put(".jpg" ,1); tblCmp.put(".ipa" ,1);
		tblCmp.put(".txt" ,9); tblCmp.put(".xls" ,5); tblCmp.put(".pptx",1);
		tblCmp.put(".htm" ,9); tblCmp.put(".ppt" ,5); tblCmp.put(".m4v" ,1);
		tblCmp.put(".html",9); tblCmp.put(".docx",1); tblCmp.put(".pst" ,9);
		tblCmp.put(".doc" ,5); tblCmp.put(".xlsx",1);
	}

	public static final int defaultCmpLevel=5;
	public static final int defaultNoCmpLevel=1;
	
	public static final int consoleColumns=100;
	
	public static int getCmpLevel(String file){
		String lw=file.toLowerCase();
		int ndx=lw.lastIndexOf('.');
		if(ndx<0)
			return defaultCmpLevel;
		String ext=lw.substring(ndx);
		Integer ret=tblCmp.get(ext);
		if(ret==null)
			return defaultCmpLevel;
		return ret.intValue();
	}
	
	/**
	 * Delete mark for append to file name of entries
	 */
	public static final String delMark="<DELETED>";

	public static String readableFileSize(long size) {
	    if(size <= 0) return "0";
	    final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    String r1=new DecimalFormat("#,##0.##").format(size/Math.pow(1024, digitGroups));
	    return r1 + " " + units[digitGroups];
	}

	public static final String snapExt=".snap";
	public static final String dateFormat="yyMMdd-HHmmss";
	public static final String statName="STAT.gz";

	public static long parseDate(String s){
		long ret=0;
		try {
			SimpleDateFormat sdf=new SimpleDateFormat(Statics.dateFormat);
			Date d=sdf.parse(s);
			ret=d.getTime();
		}catch(Exception e){
			throw new RuntimeException("Bad date '"+s+"'");
		}
		return ret;
	}	

	public static String getCurrentDate() {
		SimpleDateFormat sdf=new SimpleDateFormat(Statics.dateFormat);
		String date=sdf.format(new Date().getTime());
		return date;
	}

}
