package es.olmo.ib3;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Vector;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import es.olmo.ib3.tree.IncBackupProp;
import es.olmo.ib3.tree.Nodo;
import es.olmo.ib3.xm.ExtractManager;
import es.olmo.ib3.xm.FileExtractDestination;
import es.olmo.ib3.xm.ZipExtractDestination;
import es.olmo.util.Progress;

public class Operation {
	RootBackupManager rbuManager;
	Vector<String> fileSpecs;
	String rootOfSource;
	String dateOfOperation;
	
	public Operation(RootBackupManager rbum, String rSrc, Vector<String> spec) throws Exception{
		rbuManager=rbum;
		fileSpecs=spec;
		rootOfSource=rSrc;
	}
	
	public void update(boolean forceUpdate) throws Exception{
		if(fileSpecs.size()==0){
			File ros=new File(rootOfSource);
			int rosLen=ros.getCanonicalPath().length();
			File list[]=IncBackupProp.filter(ros);
			for(File ex:list){
				if(!ex.isDirectory())
					continue;
				String rPath=ex.getCanonicalPath().substring(rosLen);
				fileSpecs.add(rPath);
			}
		}
		for(String relPath:fileSpecs){
			rbuManager.log("Update from ROS="+rootOfSource+" to BUD="+rbuManager.getRoot(), relPath);
			System.out.print("\nUpdate on "+relPath+"...\n");
			BackupManager bum=rbuManager.initBUM("Init "+relPath+" for update of ", relPath);
			String oldROS=bum.getCurrentSnapshot().getROS();
			if(oldROS!=null && !oldROS.equals(rootOfSource)){
				System.err.println("WARN: old snapshot was created using Source Root "+oldROS+".");
				System.err.println("      You are trying to update using a different one        ");
				System.err.println("      ("+rootOfSource+") and it may corrupt your backups!");
				System.err.println("      MUST BE THE SAME DIR. IF YOU ARE SURE OF THAT, USE ");
				System.err.println("      --force ON THE COMMAND LINE TO FORCE THE UPDATE");
				if(forceUpdate){
					System.err.println("WARN: forceUpdate has been specified: Updating anyway!!");
				}else{
					System.err.println("Aborting update.");
					continue;
				}
			}
			//Build File System snapshot
			String zipRef=Statics.getCurrentDate()+Statics.snapExt;
			Snapshot other=new Snapshot(zipRef,rootOfSource,relPath);
			bum.diff(other);
			bum.commit();
		}
	}
	
	public void list() throws Exception{
		if(fileSpecs.size()==0)
			fileSpecs.add("*");
		for(String relPath:fileSpecs){
			System.out.println("List of "+relPath);
			for(BackupManager bum:rbuManager){
				bum.match(relPath,null,true);
				System.out.println("Checking "+bum.getRelPath());
				bum.showMatched();
			}
		}
	}
	
	/**
	 * Extract files
	 * @param fileDest may be null.
	 * @throws Exception 
	 */
	public void extract(String dateSelected, String fileDest) throws Exception{
		if(fileDest==null)
			fileDest=".";
		long totalItems=0;
		long totalSize=0;
		for(BackupManager bum:rbuManager){
			Snapshot currSnap=bum.getCurrentSnapshot();
			currSnap.root.resetStatus(0, 0, Nodo.opUntouched, false, null, true);
			for(String relPath:fileSpecs){
				bum.match(relPath,dateSelected,true);
				totalItems+=currSnap.getTotalNumber(Nodo.opMatch);
				totalSize+=currSnap.getTotalSize(Nodo.opMatch);
			}
		}
		String msgs=String.format("Extracting %s",Statics.readableFileSize(totalSize));
		Progress p=new Progress(msgs, totalItems, totalSize, Statics.consoleColumns);
		ExtractManager xt;
		if(fileDest.toLowerCase().endsWith(".zip")){
			xt=new ZipExtractDestination(p, fileDest);
		}else{
			xt=new FileExtractDestination(new File(fileDest), p);
		}
		for(BackupManager bum:rbuManager)
			bum.extract(xt);
	}
	
	public void portable(String fileDest) throws Exception{
		if(fileDest==null)
			fileDest="Portable-"+Statics.getCurrentDate()+".zip";
		Progress prog=new Progress("Creating "+fileDest, Progress.szUnknown, Progress.szUnknown, Statics.consoleColumns);
		ExtractManager xt=new ZipExtractDestination(prog, fileDest);
		for(BackupManager bum:rbuManager){
			File stat=bum.getStatFile();
			String rp=bum.getRelPath();
			xt.itemExtract(rp+File.separator+Statics.statName, 
					new FileInputStream(stat), rp);
		}
		xt.finishExtract();
	}
	
	public void stat() throws Exception{
		//Find all STAT.gz under rootOfbackup and delete
		rbuManager.clearSTATs();
		for(BackupManager bum:rbuManager){
			System.out.println("Re-stating "+bum);
			bum.createSaveAndLoadStat();
		}
	}
	
	public void showStat() throws Exception{
		for(String relPath:fileSpecs){
			File in=new File(relPath);
			FileInputStream fis=new FileInputStream(in);
			GzipCompressorInputStream gzis=new GzipCompressorInputStream(fis);
			ObjectInputStream ois=new ObjectInputStream(gzis);
			String name=(String)ois.readObject();
			Snapshot snap=(Snapshot)ois.readObject();
			snap.root.setName(name);
			snap.root.resetStatus(0, 0, Nodo.opMatch, false, null, true);
			snap.root.showWalkerTree(0);
		}
	}
	
}
