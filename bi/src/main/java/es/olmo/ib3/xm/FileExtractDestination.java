package es.olmo.ib3.xm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import es.olmo.util.Progress;

public class FileExtractDestination extends ExtractManager {

	File root;
	
	public FileExtractDestination(File root, Progress p) {
		super(p);
		this.root=root;
	}

	@Override
	public void itemExtract(String relPath, InputStream fis, String msg)
			throws Exception {
		//prog.startPrepareItem();
		FileOutputStream fos=new FileOutputStream(new File(root,relPath));
		//prog.endPrepareItem();
		save(fis,fos,msg);
		fos.close();
	}

	@Override
	public void finishExtract() throws Exception {
		//
	}

}
