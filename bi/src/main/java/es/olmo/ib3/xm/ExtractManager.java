package es.olmo.ib3.xm;

import java.io.InputStream;
import java.io.OutputStream;

import es.olmo.util.Progress;

public abstract class ExtractManager {
	private byte buffer[]=new byte[4*1024*1024];
	Progress prog;
	
	public ExtractManager(Progress p){
		prog=p;
	}
	
	public abstract void itemExtract(String relPath, InputStream fis, String msg) throws Exception;
	
	public abstract void finishExtract() throws Exception;

	public void save(InputStream fis, OutputStream fos, String msg) throws Exception{
		int readed;
		while((readed=fis.read(buffer))>=0){
			fos.write(buffer,0,readed);
			prog.reportAdvance(readed, msg);
		}
	}
}
