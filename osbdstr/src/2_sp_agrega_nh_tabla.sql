alter procedure sp_agrega_nh_tabla(@p_tabla varchar(255)) as
begin
  set NOCOUNT on
  if(select count(*) from sysobjects where "name"=@p_tabla)=0
    begin
      select Msg_error='No existe la tabla en la base, no se cargar�n datos a NH_Tablas'
      return
    end
  if not exists(select* from nh_tablas where nombre_tabla=@p_tabla)
    insert into nh_tablas(nombre_tabla,tabla_activa) values(@p_tabla,1)
  /*Carga y actualiza las columnas*/
  execute sp_agrega_nh_columnas @p_tabla
  set NOCOUNT off
end