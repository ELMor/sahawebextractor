package sef;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EpubBook implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4378003740855168247L;
	EpubDir epubDir;
	String parent;
	String autor;
	String titulo;
	int portada = -1;
	String iTunesAutor;
	String iTunesTitulo;
	String iTunesPlayilstName;
	String calibreAutor;
	File file;
	String opfFile;

	String modOp = "";
	EpubBook modBook = null;

	public EpubBook(EpubDir ed, File f) throws Exception {
		epubDir = ed;
		file = f;
		if (!file.exists())
			throw new Exception("File not found!");

		parent = file.getParent();
		parent = parent.substring(new File(parent).getParent().length() + 1);

		// Localizamos el opf en primer lugar
		ZipInputStream zis = new ZipInputStream(new FileInputStream(file));
		for (ZipEntry ze = zis.getNextEntry(); ze != null; ze = zis
				.getNextEntry()) {
			if (ze.getName().equals("META-INF/container.xml")) {
				opfFile = ed.getNSCtx().getXPathVal("opf", getIS(zis),
						"/container/rootfiles/rootfile/@full-path");
				break;
			}
		}
		zis.close();

		zis = new ZipInputStream(new FileInputStream(file));

		for (ZipEntry ze = zis.getNextEntry(); ze != null; ze = zis
				.getNextEntry()) {
			if (opfFile != null && ze.getName().equals(opfFile)) {
				String opf = getIS(zis);
				autor = ed.getNSCtx().getXPathVal("opf", opf,
						ed.getNSCtx().autorExp);
				titulo = ed.getNSCtx().getXPathVal("opf", opf,
						ed.getNSCtx().tituloExp);
				calibreAutor = ed.getNSCtx().getXPathVal("opf", opf,
						ed.getNSCtx().calibreAutorExp);
			} else if (ze.getName().equals("iTunesArtwork")) {
				portada = getIB(zis).length;
			} else if (ze.getName().equals("iTunesMetadata.plist")) {
				String plist = getIS(zis);
				NodeList dict = ed.getNSCtx().getNodeSet("plist", plist,
						ed.getNSCtx().plistExp);
				int numNodes = dict.getLength();
				for (int i = 0; i < numNodes - 1; i++) {
					Node n = dict.item(i);
					if (n.getLocalName().equals("key")) {
						if (n.getTextContent().equals("artistName")) {
							Node m = dict.item(i + 1);
							iTunesAutor = m.getTextContent();
						} else if (n.getTextContent().equals("itemName")) {
							Node m = dict.item(i + 1);
							iTunesTitulo = m.getTextContent();
						} else if (n.getTextContent().equals("playlistName")) {
							Node m = dict.item(i + 1);
							iTunesPlayilstName = m.getTextContent();
						}
					}
				}
			}
		}
		zis.close();
	}

	public boolean esElMismo(EpubBook master) {
		return titulo.equals(master.titulo) && autor.equals(master.autor);
	}

	public InputStream getInputStream(String entName) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		ZipInputStream zis = new ZipInputStream(fis);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipEntry entry = null;
		while ((entry = zis.getNextEntry()) != null) {
			if (entry.getName().equals(entName)) {
				writeTo(zis, baos);
				break;
			}
		}
		zis.close();
		return new ByteArrayInputStream(baos.toByteArray());
	}

	/**
	 * Substituye o a�ade 'is' al epub, con nombre 'name'
	 * 
	 * @param name
	 *            Nombre del fichero dentro del zip
	 * @param is
	 *            InputStream con el contenido
	 * @throws IOException
	 */
	public void setInpuStream(String name, InputStream is) throws IOException {
		ZipEntry z1, z2 = new ZipEntry("mimetype");
		z2.setMethod(ZipEntry.STORED);
		z2.setSize(20);
		z2.setCrc(0x2cab616f);
		File tmpFile = new File(file.getCanonicalPath() + "-tmp");
		FileOutputStream fos = new FileOutputStream(tmpFile);
		ZipOutputStream zos = new ZipOutputStream(fos);
		zos.putNextEntry(z2);
		zos.write("application/epub+zip".getBytes());
		boolean added = false;
		FileInputStream fis = new FileInputStream(file);
		ZipInputStream zis = new ZipInputStream(fis);
		while ((z1 = zis.getNextEntry()) != null) {
			if (z1.getName().equals("mimetype"))
				continue;
			if (z1.getName().equals(name)) {
				zos.putNextEntry(new ZipEntry(name));
				zos.write(getIB(is));
				added = true;
			} else {
				zos.putNextEntry(z1);
				zos.write(getIB(zis));
			}
		}
		if (!added) {
			zos.putNextEntry(new ZipEntry(name));
			zos.write(getIB(is));
		}
		zos.close();
		zis.close();
		if (!file.delete())
			System.out.println("WARN:" + file + " cannot be deleted!");
		else if (!tmpFile.renameTo(file))
			System.out.println("WARN:" + tmpFile + " cannot be renamed!");
	}

	private byte[] getIB(InputStream is) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		writeTo(is, bos);
		return bos.toByteArray();
	}

	public static void writeTo(InputStream is, OutputStream os)
			throws IOException {
		int readed;
		byte buf[] = new byte[1024 * 1024];
		while ((readed = is.read(buf, 0, buf.length)) > 0) {
			os.write(buf, 0, readed);
		}
	}

	private String getIS(ZipInputStream zis) throws IOException {
		byte[] bos = getIB(zis);
		return new String(bos);
	}

	public String getParent() {
		return parent;
	}

	public void changeOnOPF(String autor, String titulo) throws Exception {
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(getInputStream(opfFile));
		Element root = document.getRootElement();
		Element meta = getElement(root, "metadata");
		Element auto = getElement(meta, "creator");
		Element titl = getElement(meta, "title");
		if (autor != null)
			auto.setText(autor);
		if (titulo != null)
			titl.setText(titulo);
		XMLOutputter xo = new XMLOutputter(Format.getPrettyFormat());
		String newOpf = xo.outputString(document);
		ByteArrayInputStream bais = new ByteArrayInputStream(newOpf.getBytes());
		setInpuStream(opfFile, bais);
	}

	public Element getElement(Element elemento, String name) {
		@SuppressWarnings("unchecked")
		List<Element> hijos = elemento.getChildren();
		for (int i = 0; i < hijos.size(); i++) {
			Element hijo = hijos.get(i);
			if (hijo.getName().equals(name))
				return hijo;
		}
		return null;
	}

}
