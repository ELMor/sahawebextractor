package sef;

import java.io.File;

public class Main {

	private static void usage() {
		System.out.println("Usage:");
		System.out
				.println("\t java -jar synchEPUBFolder.jar dir [dir1[dir2[...]]] dirN");
		System.out
				.println("Sincroniza los directorios dir1, dir2, etc... con dir");
		System.out.println("dir tiene una estructura de LETRA/Autor/Libro");
		System.out.println("dirN no tiene una estructura determinada.");
		System.out
				.println("Dos libros son iguales cuando su opf en titulo y autor coinciden.");
		System.exit(0);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 2)
			usage();
		try {
			
			EpubDir md = EpubDir.helper(new File(args[0]),true);
			md.start();

			EpubDir epDirs[] = new EpubDir[args.length - 1];
			for (int i = 1; i < args.length ; i++) {
				epDirs[i-1] = EpubDir.helper(new File(args[i]));
				epDirs[i-1].start();
			}

			boolean cont = true;

			while (cont) {
				Thread.sleep(1000);
				cont = md.isAlive();
				for (int i = 0; i < args.length - 1; i++)
					cont |= epDirs[i].isAlive();
			}

			md.master_Check();

			for (int i = 0; i < args.length - 1; i++)
				md.master_synchWith(epDirs[i]);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
