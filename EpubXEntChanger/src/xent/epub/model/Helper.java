package xent.epub.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Helper extends ByteArrayOutputStream {
	
	protected XPathFactory factory= XPathFactory.newInstance();
	protected XPath xpath=factory.newXPath();
	
	DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
	DocumentBuilder db=null;
	protected Document doc=null;
	
	String prefix,uri;
	
	public Helper(String document) throws Exception {
		if(document!=null){
			while(!document.startsWith("<"))
				document=document.substring(1);
			dbf.setValidating(false);
			dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			db=dbf.newDocumentBuilder();
			doc=db.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));
		}else{
			db=dbf.newDocumentBuilder();
			doc=db.newDocument();
		}
	}
	
	public NodeList getNodeList(String xp){
		try {
			XPathExpression xpe=xpath.compile(xp);
			return (NodeList)(xpe.evaluate(doc, XPathConstants.NODESET));
		}catch(Exception e){
			
		}
		return null;
	}
	
	public Node getNode(String xp){
		try {
			XPathExpression xpe=xpath.compile(xp);
			Node ret=(Node)(xpe.evaluate(doc, XPathConstants.NODE));
			return ret;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public String getDocAsText(){
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			//initialize StreamResult with File object to save to file
			StreamResult result = new StreamResult(new StringWriter());
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			return result.getWriter().toString();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public synchronized int size() {
		return toByteArray().length;
	}

	@Override
	public synchronized byte[] toByteArray() {
		return getDocAsText().getBytes();
	}

	@Override
	public synchronized String toString() {
		return getDocAsText();
	}

	@Override
	public synchronized void writeTo(OutputStream arg0) throws IOException {
		arg0.write(toByteArray());
	}
	
}
