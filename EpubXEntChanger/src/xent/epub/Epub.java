package xent.epub;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import net.sf.jazzlib.ZipEntry;
import net.sf.jazzlib.ZipInputStream;
import net.sf.jazzlib.ZipOutputStream;
import xent.epub.model.Container;
import xent.epub.model.Content;
import xent.epub.model.TOC;

public class Epub implements Comparable<Epub> {
	public File nameOfFile = null;
	Hashtable<String, ByteArrayOutputStream> zfList = null;
	Hashtable<String, Long> dateList = new Hashtable<String, Long>();
	public long dateOfOPF=0;
	public iTunesMetadata itunes;
	public Container container;
	public Content content;
	public TOC tocncx;
	public String compareAuthor = null, compareTitle = null;
	public boolean mark = false;

	/**
	 * Create a new epub
	 * @param tit
	 * @param aut
	 */
	public Epub(String aut, String tit) throws Exception{
		nameOfFile=new File(aut+"-"+tit);
		zfList = new Hashtable<String, ByteArrayOutputStream>();
		container=new Container(null);
	}
	
	/**
	 * Loads an epub from an existing file
	 * @param f File for epub
	 * @throws Exception
	 */
	public Epub(File f) throws Exception {
		nameOfFile = f;
		zfList = new Hashtable<String, ByteArrayOutputStream>();
		loadFilesFromEPUB();
		
		// Este siempre debe existir
		ByteArrayOutputStream ostream = zfList.get("META-INF/container.xml");
		if (ostream == null)// No container!! no parece que sea un epub
			throw new RuntimeException(nameOfFile.getName()
					+ " is not an epub! (fails container.xml)");
		container = new Container(ostream.toString());
		zfList.put("META-INF/container.xml", container);
		
		// Este tambien debe existir
		String opfLoc = container.getOBPSName();
		ostream = zfList.get(opfLoc);
		dateOfOPF=dateList.get(opfLoc);
		if (ostream == null)// No OPF! no parece que sea un epub
			throw new RuntimeException(nameOfFile.getName()
					+ " is not an epub! (fails OPF)");
		content = new Content(ostream.toString());
		zfList.put(opfLoc, content);
		
		//Cargamos tambien la TOC
		String tocLoc=content.getTOCRef();
		
		// Parsing de varios de los archivos (Este si ha sido procesado en
		// alguna ocasion por iTunes):
		ostream = zfList.get("iTunesMetadata.plist");
		if (ostream != null) {
			itunes = new iTunesMetadata(ostream.toString());
			zfList.put("iTunesMetadata.plist", itunes);
		} else {
			itunes = null;
		}
	}

	public void loadFilesFromEPUB() throws FileNotFoundException, Exception,
			IOException {
		if(nameOfFile.isDirectory())
			loadDirEPUB();
		else
			loadZippedEPUB();
	}

	public void loadZippedEPUB() throws FileNotFoundException, Exception,
			IOException {
		FileInputStream fi = new FileInputStream(nameOfFile);
		ZipInputStream zis = new ZipInputStream(fi);
		// Cargamos el epub en el hashtable
		ZipEntry ze = null;
		try {
			while ((ze = zis.getNextEntry()) != null) {
				if (ze.getName().equals("mimetype")) {
					getBAOS(zis);
				} else {
					ByteArrayOutputStream baos = getBAOS(zis);
					zfList.put(ze.getName(), baos);
					dateList.put(ze.getName(), ze.getTime());
				}
			}
		} catch (Exception e) {
			throw e;
		}
		fi.close();
		zis.close();
	}
	
	public void loadDirEPUB() throws IOException{
		String rootOfDir=nameOfFile.getCanonicalPath();
		recursedLoadDirEPUB(rootOfDir, nameOfFile);
	}

	private void recursedLoadDirEPUB(String root,File dir) throws IOException{
		File lf[]=dir.listFiles();
		for(File f:lf){
			String relName=f.getCanonicalPath().substring(root.length()+1);
			if(f.isDirectory())
				recursedLoadDirEPUB(root, f);
			else if(!relName.equals("mimetype")){
				FileInputStream fis=new FileInputStream(f);
				ByteArrayOutputStream baos=getBAOS(fis);
				zfList.put(relName, baos);
				dateList.put(relName, f.lastModified());
				fis.close();
			}
		}
		
	}
	
	public String getOPFLoc() {
		return container.getOBPSName();
	}

	public String getNameOfFile() {
		return nameOfFile.getName();
	}

	public Hashtable<String, ByteArrayOutputStream> getZFC() {
		return zfList;
	}

	private ByteArrayOutputStream getBAOS(InputStream zis)
			throws IOException {
		byte buf[] = new byte[4 * 1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int lread;
		while ((lread = zis.read(buf)) >= 0)
			baos.write(buf, 0, lread);
		return baos;
	}

	
	public void save(){
		save(nameOfFile,zfList);
	}
	/**
	 * Graba el archivo.
	 * 
	 * @return Devuelve true si fue todo bien.
	 */
	public static boolean save(File f, Hashtable<String, ByteArrayOutputStream> list) {
		try {
			File f2 = new File((new StringBuilder(
					String.valueOf(f.getCanonicalPath()))).append("-tmp").toString());
			FileOutputStream fos = new FileOutputStream(f2);
			save(fos,list);
			f.delete();
			f2.renameTo(f);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public void save(OutputStream fos) throws IOException {
		save(fos,zfList);
	}
	
	public static void save(OutputStream fos,Hashtable<String, ByteArrayOutputStream> list) throws IOException {
		ZipOutputStream zos = createAndInitEPUBContainer(fos);
		String n;
		for (Enumeration<String> fn = list.keys(); 
				fn.hasMoreElements(); 
				((ByteArrayOutputStream) list.get(n)).writeTo(zos)) {
			n = (String) fn.nextElement();
			zos.putNextEntry(new ZipEntry(n));
		}
		zos.close();
	}

	public static ZipOutputStream createAndInitEPUBContainer(OutputStream fos)
			throws IOException {
		ZipOutputStream zos = new ZipOutputStream(fos);
		ZipEntry ze = new ZipEntry("mimetype");
		ze.setMethod(0);
		ze.setSize(20L);
		ze.setCrc(0x2cab616fL);
		zos.putNextEntry(ze);
		zos.write("application/epub+zip".getBytes());
		return zos;
	}

	public boolean setAutor_iTunes(String au) {
		if (itunes != null)
			return itunes.setAutor_iTunes(au);
		return false;
	}

	public boolean setAutor_epub(String au) {
		return content.setAutor_epub(au);
	}

	public String getAutor_iTunes() {
		if (itunes != null)
			return itunes.getAutor_iTunes();
		return null;
	}

	public String getAutor_epub() {
		String ret = content.getAutor_epub();
		return ret;
	}

	public boolean setTitulo_iTunes(String ti) {
		if (itunes != null)
			return itunes.setTitle_iTunes(ti);
		return false;
	}

	public boolean setTitulo_epub(String ti) {
		return content.setTitulo_epub(ti);
	}

	private String getTitulo_iTunes() {
		if (itunes != null)
			return itunes.getTitulo_iTunes();
		return null;
	}

	public String getTitulo_epub() {
		return content.getTitulo_epub();
	}

	public String getAutor() {
		if (itunes != null) {
			String ret = getAutor_iTunes();
			if (ret != null && ret.length() > 0)
				return ret;
		}
		return getAutor_epub();
	}

	public String getTitulo() {
		if (itunes != null) {
			String ret = getTitulo_iTunes();
			if (ret != null && ret.length() > 0)
				return ret;
		}
		return getTitulo_epub();
	}

	public iTunesMetadata getITunesMetadata() {
		return itunes;
	}

	public void initComparison() {
		if (compareTitle == null)
			compareTitle = normalizeToCompare(getTitulo());
		if (compareAuthor == null)
			compareAuthor = normalizeToCompare(getAutor());
	}

	@Override
	public int compareTo(Epub o) {
		initComparison();
		o.initComparison();
		return 256 * compareAuthor.compareTo(o.compareAuthor)
				+ compareTitle.compareTo(o.compareTitle);
	}

	public static final String orig="����������";
	public static final String dest="aeiouAEIOU";

	public static String normalizeToCompare(String s) {
		for(int k=0;k<orig.length();k++)
			s=s.replace(orig.charAt(k), dest.charAt(k));
		String ret = "";
		StringTokenizer st = new StringTokenizer(s.toUpperCase(), " .,!?-_;/");
		Vector<String> toks = new Vector<String>();
		while (st.hasMoreElements())
			toks.add(st.nextToken());
		Collections.sort(toks);
		for (String tok : toks)
			ret += tok + " ";
		return ret.trim();
	}

	public Epub(File file, Vector<File> vector) throws Exception {
		nameOfFile=file;
		zfList=new Hashtable<String, ByteArrayOutputStream>();
		int bookNumber=0;
		for(File f:vector){
			Epub book=new Epub(f);
			Hashtable<String, ByteArrayOutputStream> bookStreams=book.getZFC();
			String newRoot="OEBPS/Book"+String.format("%02d/",++bookNumber);
			for(String s:bookStreams.keySet()){
				ByteArrayOutputStream baos=bookStreams.get(s);
				zfList.put(newRoot+s, baos);
			}
		}
		save(file,zfList);
	}

	public boolean isANewVersionOf(Epub other){
		if(compareTo(other)!=0)
			return false;
		return dateOfOPF>other.dateOfOPF;
	}
}
