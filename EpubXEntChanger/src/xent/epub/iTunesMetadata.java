package xent.epub;

import org.w3c.dom.Node;

import xent.epub.model.Helper;



public class iTunesMetadata extends Helper {
	
	Node nAutor,nTitulo,nSerie,nNumero,nNumeroMaximo;
	Node nl;
	
	public iTunesMetadata(String baos) throws Exception{
		super(baos);
		nl=getNode("/plist/dict");
		for(Node child=nl.getFirstChild();
				child!=null;
				child=child.getNextSibling()){
			if(child.getTextContent().equals("artistName")){
				nAutor=child.getNextSibling().getNextSibling();
			}
			if(child.getTextContent().equals("itemName")){
				nTitulo=child.getNextSibling().getNextSibling();
			}
			if(child.getTextContent().equals("playlistName")){
				nSerie=child.getNextSibling().getNextSibling();
			}
			if(child.getTextContent().equals("trackNumber")){
				nNumero=child.getNextSibling().getNextSibling();
			}
			if(child.getTextContent().equals("trackCount")){
				nNumeroMaximo=child.getNextSibling().getNextSibling();
			}
		}
	}
	
	public String getAutor_iTunes(){
		if(nAutor!=null)
			return nAutor.getTextContent();
		return null;
	}
	
	public String getTitulo_iTunes(){
		if(nTitulo!=null)
			return nTitulo.getTextContent();
		return null;
	}
	
	public boolean setAutor_iTunes(String au){
		if(au.equals(getAutor_iTunes()))
			return false;
		if(nAutor==null){
			nAutor=doc.createElement("key");
			nAutor.setTextContent("artistName");
			nl.appendChild(nAutor);
			nAutor=doc.createElement("string");
			nAutor.setTextContent(au);
			nl.appendChild(nAutor);
		}else{
			nAutor.setTextContent(au);
		}
		return true;
	}
	public boolean setTitle_iTunes(String ti){
		if(ti.equals(getTitulo_iTunes()))
			return false;
		nTitulo.setTextContent(ti);
		return true;
	}
	public String getSerie(){
		if(nSerie!=null)
			return nSerie.getTextContent();
		return null;
	}
	
	public String getNumber(){
		if(nNumero!=null)
			return nNumero.getTextContent();
		return null;
	}
	
	public String getCount(){
		if(nNumeroMaximo!=null)
			return nNumeroMaximo.getTextContent();
		return null;
	}
	
	public Node getDictNode(){
		return nl;
	}
}
