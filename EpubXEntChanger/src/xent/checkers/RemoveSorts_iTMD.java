package xent.checkers;

import java.util.Vector;

import org.w3c.dom.Node;

import xent.epub.Epub;

public class RemoveSorts_iTMD extends Checker {

	@Override
	public Class<?> getClassToProcess() {
		return Epub.class;
	}

	public static Vector<String> deleteNodes= new Vector<String>();
	static{
		deleteNodes.add("sort-album");  		deleteNodes.add("sort-album-artist");
		deleteNodes.add("sort-artist");  		deleteNodes.add("sort-name");
		deleteNodes.add("sort-composer");  		deleteNodes.add("sort-show-name");
	}
	
	@Override
	public boolean initialize() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public String getDescription(){
		String nd="";
		for(String n:deleteNodes)
			nd+= (nd.length()==0) ? n : ","+n;
		return "Remove nodes from iTunesMetadata.plist:"+nd;
	}
	
	@Override
	public boolean exec(Epub epub) throws Exception {
		boolean ret=false;
		for(Node child=epub.getITunesMetadata().getDictNode().getFirstChild();
				child!=null;
				child=child.getNextSibling()){
			for(String dn:deleteNodes){
				if(child.getTextContent().equals(dn)){
					Node a=child.getNextSibling().getNextSibling();
					String t=a.getTextContent();
					if(t!=null && t.length()>0){
						a.setTextContent("");
						ret=true;
					}
					break;
				}
			}
		}
		return ret;
	}

	@Override
	public boolean finalizar() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getMsgIsModified() {
		return "Removed";
	}

	@Override
	public String getMsgChecking() {
		
		return "Removing iTunes sort";
	}

}
