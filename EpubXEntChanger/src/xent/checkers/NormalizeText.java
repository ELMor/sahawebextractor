package xent.checkers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import lrf.Utils;
import xent.epub.Epub;

public class NormalizeText extends Checker {

	@Override
	public Class<?> getClassToProcess() {
		return Epub.class;
	}

	public String getDescription(){
		return "Replace in every '.xhtml' file strange chars.";
	}
	
	@Override
	public boolean exec(Epub epub) {
		boolean fileChanged=false;
		for(Enumeration<String> keys=epub.getZFC().keys();
				keys.hasMoreElements();){
			String fn=keys.nextElement();
			String en=fn.toLowerCase();
			if(en.endsWith(".xhtml")){
				ByteArrayOutputStream bs=epub.getZFC().get(fn);
				String ori=bs.toString();
				Vector<String> replaced=new Vector<String>();
				String cvt=Utils.toNormalText(ori,replaced);
				if(replaced.size()>0){
					fileChanged=true;
					bs=new ByteArrayOutputStream();
					try {
						bs.write(cvt.getBytes());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					epub.getZFC().put(fn, bs);
				}
			}
		}
		return fileChanged;
	}

	@Override
	public String getMsgIsModified() {
		return "Normalized";
	}

	@Override
	public boolean initialize() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean finalizar() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getMsgChecking() {
		return "Normalizing xhtml";
	}

}
