package xent;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.Vector;

import xent.checkers.Checker;
import xent.epub.Epub;

public class Recurse {

	File raiz=null;
	Vector<File> lista=new Vector<File>();
	Hashtable<File, Exception> err=new Hashtable<File, Exception>();
	
	/**
	 * Seleccionar los archivo epub del directorio
	 * @param init directorio padre de los epub
	 */
	public Recurse(File init) {
		raiz = init;
		recurse(raiz, lista);
		System.out.println(lista.size()+" files to process.");
	}
	/**
	 * Para cada epub se ejecutara cada checker por orden
	 * @param checks Lista de checkers a ejecutar
	 */
	public void proceed(Vector<Checker> checks) {
		SameLineProgress slp=new SameLineProgress(79, lista.size(), "Check");
		int avance=0;
		for(Checker chk1:checks)
			try {
				chk1.initialize();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		for(File f:lista){
			String status="";
			avance++;
			try {
				Epub epub=new Epub(f);
				boolean mustBeSaved=false;
				for(Checker chk:checks){
					boolean mod=chk.exec(epub);
					if(mod){
						status+=chk.getMsgIsModified();
					}else{
						status+=chk.getMsgIsNotModified();
					}
					mustBeSaved|=mod;
				}
				if(mustBeSaved){
					epub.save();
				}
			}catch(Exception e){
				slp.report(avance, f.getName()+":"+e.getMessage()+":"+e.toString()+"\n");
				e.printStackTrace();
				err.put(f, e);
			}
			slp.report(avance, f.getName(), status);
		}
		for(Checker chk2:checks)
			try {
				chk2.finalizar();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		try {
			PrintStream ps=new PrintStream(new File("Xent-Error.log"));
			for(File f:err.keySet()){
				ps.print(f.getName()+":\n");
				err.get(f).printStackTrace(ps);
			}
			ps.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void recurse(File dir, Vector<File> retList) {
		File lista[] = dir.listFiles();
		File afile[];
		int j = (afile = lista).length;
		for (int i = 0; i < j; i++) {
			File f = afile[i];
			if (f.isDirectory() && !f.getName().endsWith(".epub"))
				recurse(f,retList);
			else if (f.getName().toLowerCase().endsWith(".epub")) {
				try {
					retList.add(f);
				} catch (Exception e) {
					System.err.println("***Error "+f.getPath());
				}
			}
		}

	}

}
