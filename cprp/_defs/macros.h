/*
        Macros generales
*/
//Concatena dos argumentos de macro
#define __CONCAT(a,b)\
        __CONCAT_INT(a,b)

#define __CONCAT_INT(a,b)\
        a##b
