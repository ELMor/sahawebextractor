

#ifndef WM_WINETRICKS
#error WM_WINETRICKS not defined
#endif
#ifndef WM_REGPATH
#error WM_REGPATH not defined
#endif
#ifndef WM_WINETRICKSURL
#error WM_WINETRICKSURL not defined
#endif

## --Begin install wine emplate--
sudo apt-get install wine32
wget  -o WM_WINETRICKS WM_WINETRICKSURL
chmod +x WM_WINETRICKS
WM_WINETRICKS -q dotnet30
WM_WINETRICKS -q mdac28
WM_WINETRICKS -q jet40
echo REGEDIT4 > WM_REGPATH
echo [HKEY_CURRENT_USER\\Environment] >> WM_REGPATH
echo "PATH"="C:\\CSCHealthCare\\Dll" >> WM_REGPATH
wine regedit /S WM_REGPATH
## --End install wine template--

#undef WM_WINETRICKS
#undef WM_REGPATH
#undef WM_WINETRICKSURL
