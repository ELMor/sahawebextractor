package es.olmo.bi;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author elinares
 *
 */

public class Main {
	
	public static final boolean debug=false;
	
	private static void printUsage(){
		String linea="";
		BufferedReader br=new BufferedReader(
				new InputStreamReader(
						linea.getClass().getResourceAsStream("Readme.txt")));
		try {
			while((linea=br.readLine())!=null)
				System.out.println(linea);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}
	
	static String rootDest=null;
	static String rootOri=null;
	static String dateSelected=null;
	static Getopt g=null;
	static int opMode=0; // 1 to update, 2 to list, 3 to extract, 4 to restat, 5 get portable stats

	public static void main(String args[]){
		LongOpt opts[]=new LongOpt[9];
		StringBuffer sb=new StringBuffer();

		opts[0]=new LongOpt("help", LongOpt.NO_ARGUMENT, null, 'h');
		opts[1]=new LongOpt("outBackupDir", LongOpt.REQUIRED_ARGUMENT, sb, 'o');
		opts[3]=new LongOpt("update", LongOpt.NO_ARGUMENT, sb, 'u');
		opts[4]=new LongOpt("list", LongOpt.NO_ARGUMENT, sb, 'l');
		opts[5]=new LongOpt("extract", LongOpt.NO_ARGUMENT, sb, 'x');
		opts[6]=new LongOpt("restat", LongOpt.NO_ARGUMENT, sb, 'r');
		opts[7]=new LongOpt("portable", LongOpt.NO_ARGUMENT, sb, 'p');
		opts[8]=new LongOpt("date", LongOpt.OPTIONAL_ARGUMENT, sb, 'd');

		g=new Getopt("incb", args,"-:ho:ulxrpd:" ,opts);
		
		int c;
		while((c=g.getopt())!=-1){
			switch(c){
			case 0: //long option
				char i=(char)(new Integer(sb.toString())).intValue();
				singleCharOpt(i);
				break;
			case 1:
				rootOri=g.getOptarg();
				break;
			default :
				singleCharOpt((char)c);
				break;
			}
			
		}
		int ndxOri=g.getOptind();
		
		if(ndxOri<args.length)
			rootOri=args[ndxOri];
		else if(rootOri==null  && (opMode!=4 && opMode!=5)){
			System.out.println("You must specify dir to backup");
			System.exit(-1);
		}
		
		if(++ndxOri<args.length)
			System.out.println("Ignoring extra arguments:"+args[ndxOri]+"...");
		
		if(rootDest==null){
			System.out.println("You must specify -o dir");
			System.exit(-1);
		}
		try {
			switch(opMode){
			case 0: printUsage();
			case 1: new Update(rootDest, rootOri).perform(); break;
			case 2: new List(rootDest, rootOri).perform(); break;
			case 3: new Extract(rootDest, rootOri, dateSelected).perform(); break;
			case 4: new Restat(rootDest).perform(); break;
			case 5: new Portable(rootDest).perform(); break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void singleCharOpt(char c){
		switch(c){
		case 'h': printUsage();	break;
		case 'o': rootDest=g.getOptarg(); break;
		case 'd': dateSelected=g.getOptarg(); break;
		case '?': System.exit(-1); break;
		case 'u': setMode(1); break;
		case 'l': setMode(2); break;
		case 'x': setMode(3); break;
		case 'r': setMode(4); break;
		case 'p': setMode(5); break;
		}
	}
	
	public static void setMode(int m){
		if(opMode==0){
			opMode=m;
		}else{
			System.out.println("Mode was set!");
			System.exit(-1);
		}
	}
	
}
