package es.olmo.bi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.StringTokenizer;

import de.schlichtherle.truezip.zip.ZipEntry;

public class Extract extends BackupMgr {
	String fileForList="";
	String dateSelected=null;
	public Extract(String rDest, String rOri, String dateSel) throws Exception {
		super(rDest);
		dateSelected=dateSel;
		StringTokenizer st=new StringTokenizer(rOri, "\\/");
		String pthZIPs=st.nextToken();
		init(pthZIPs);
		while(st.hasMoreElements())
			fileForList+=fileForList.length()==0?st.nextToken():"/"+st.nextToken();
	}

	public void perform() throws Exception {
		if(dateSelected==null)
			dateSelected=statDes.latestZip;
		String name=fileForList;
		Object sorted[]=(statDes.faList.keySet().toArray());
		Arrays.sort(sorted);
		initMatcher(name);
		for(int i=0;i<sorted.length;i++){
			String n=sorted[i].toString();
			if(match(n)){
				ZipEntry ze=getZEForName(name, dateSelected+".zip");
				if(ze!=null){
					InputStream is=getISForFile(name, dateSelected+".zip");
					File f=new File(new File("."),name);
					f.getParentFile().mkdirs();
					FileOutputStream fos=new FileOutputStream(f);
					ProgressShower ps=new ProgressShower(80, 1, ze.getSize(), "Extracting");
					flushFrom(is, fos, ps, name);
					fos.close();
				}else{
					System.err.println("Not found "+name+", or nonexistent zip file "+dateSelected+".zip");
				}
			}
		}
	}
}
