<%@ page import="java.util.*, com.ssi.persistence.model.PersistenceManager, com.ssi.persistence.model.JDBC.JDBCAnswerResultSet" %>
<%@ page import="java.sql.*, com.ssi.model.salud.*,com.ssi.util.*" %>
<%@ page import="com.ssi.util.*" %>

<%-- Control de la sesi�n--%>
<%
Object obj = session.getAttribute("busquedaEpisodios");
if (obj == null) { %>
<jsp:forward page="dummyView.jsp"/>
<% } %>



<jsp:useBean id="busquedaEpisodios" scope="session" class="com.ssi.bean.salud.BusquedaEpisodios" />







<%
	String strNumero = request.getParameter("numero");
 	String sistema = request.getParameter("sistema");
 	EpisodioSalud unEpisodio = (EpisodioSalud) busquedaEpisodios.buscarEpisodio(strNumero,sistema);
 	LogFile.log("detalleEpisodio.jsp");

	String strEstado = "s/d";
	String strNhc = (String)request.getSession().getValue("NHC_Actual");
	String strDiagnostico= "s/d";
	String strServicio = "s/d";

	 PersistenceManager manager = null;
	 String query = null;
	 LogFile.log("detalleEpisodio: sistema="+sistema);

	 if(sistema.equals("SistemaSiapwin")){
	   strDiagnostico = (unEpisodio.ve_ep_cond()!=null)?unEpisodio.ve_ep_cond():Parametros.sinDatos();
           strServicio    = (unEpisodio.ve_ep_servicio()!=null)?unEpisodio.ve_ep_servicio():Parametros.sinDatos();
	   LogFile.log("detalleEpisodio siapwin");
	 }
	 else{
	   LogFile.log("detalleEpisodio novahis");

	  JDBCAnswerResultSet rs2 = null;

	  try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  =  "SELECT icd_nom FROM icd where icd_cod='"+unEpisodio.ve_ep_icd_cod()+"'";
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    //String strDiag = (String)jrs2.getObject("icd_nom");
	    strDiagnostico = (String)jrs2.getObject("icd_nom");
            //strDiagnostico = (strDiag!=null)?strDiag:Parametros.sinDatos();
	   }
	   catch(Exception e){
	    LogFile.log(e);
	   }
	   finally{
	    rs2.close();
	   }

	JDBCAnswerResultSet rs = null;

	try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();

	 if(manager == null)
	  	out.println("<H2>PersistenceManager es null!</H2>");
	 else {
		query  =  "SELECT * FROM ve_estado_epis where ve_ep_estado='"+unEpisodio.ve_ep_estado()+"'";
		rs = (JDBCAnswerResultSet)manager.executeQuery(query);
		java.sql.ResultSet jrs = rs.resultSet();
		strEstado = (String)jrs.getObject("ve_ep_estado_desc");
	 }
	}
	catch(Exception e){
	  LogFile.log(e);
	}
	finally{
	  rs.close();
	}

	JDBCAnswerResultSet rs3 = null;

	try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();

	 if(manager == null)
	  	out.println("<H2>PersistenceManager es null!</H2>");
	 else {
		query  =  "SELECT servicio FROM servicios where codigo_servicio='"+unEpisodio.ve_ep_servicio()+"'";
		rs3 = (JDBCAnswerResultSet)manager.executeQuery(query);
		java.sql.ResultSet jrs1 = rs3.resultSet();
		strServicio = (String)jrs1.getObject("servicio");
	 }
	}
	catch(Exception e){
	  LogFile.log(e);
	}
	finally{
	  rs3.close();
	}



	}
%>

<html>
<head>
<title>Detalle de Episodio</title> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%> <script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>
<script language="JavaScript"><!--
function supressError() {
    return true;
}

function load(url) {
    window.onerror = supressError;
    opener.location.href = url;
    setTimeout('self.close()',1000);
}
//--></script>
<body onBlur="self.focus()" bgcolor="#eeeeee" ONLOAD="MM_preloadImages('<%=Parametros.imgPath()%>bt_cerrar1.jpg')">
<TABLE WIDTH="95%" BORDER="0" CELLSPACING="0" CELLPADDING="0" VALIGN="top" ALIGN="center">
<INPUT TYPE='hidden' NAME='view' VALUE='busquedaPaciente'> <TR> <TD VALIGN="top">
<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH=100% BORDER=0> <TBODY> <TR> <TD WIDTH=14 BACKGROUND=<%=Parametros.imgPath()%>drwd_topbgr1.gif
      BGCOLOR=#fcfdfa><IMG HEIGHT=11
      SRC="<%=Parametros.imgPath()%>drwd_topleftcorner.gif" WIDTH=5></TD><TD WIDTH=904 BACKGROUND=<%=Parametros.imgPath()%>drwd_topbgr1.gif
      BGCOLOR=#fcfdfa><IMG HEIGHT=8 SRC="<%=Parametros.imgPath()%>spacer.gif"
    WIDTH=287></TD><TD WIDTH=17> <DIV ALIGN="right"><IMG HEIGHT=11 SRC="<%=Parametros.imgPath()%>drwd_toprightcorner.gif"
      WIDTH=24></DIV></TD></TR> </TBODY> </TABLE><TABLE CELLSPACING=0 CELLPADDING=0 WIDTH=100% BORDER=0 HEIGHT="1">
<TBODY> <TR> <TD WIDTH=9 HEIGHT="5"><IMG HEIGHT=13 SRC="<%=Parametros.imgPath()%>drwd_mdlleftcorner.gif"
      WIDTH=5></TD><TD ALIGN=right WIDTH=35 BGCOLOR=#fcfdfa HEIGHT="5"><IMG HEIGHT=13
      SRC="<%=Parametros.imgPath()%>drwd_iconnews.gif" WIDTH=13></TD><TD WIDTH=865 BGCOLOR=#fcfdfa HEIGHT="5">
<P><IMG SRC="<%=Parametros.imgPath()%>titu_detalleEpisodio.jpg"></P></TD><TD WIDTH=26 HEIGHT="5">
<DIV ALIGN="right"><IMG HEIGHT=13 SRC="<%=Parametros.imgPath()%>drwd_mdlrightcorner.gif"
      WIDTH=24></DIV></TD></TR> </TBODY> </TABLE><TABLE CELLSPACING=0 CELLPADDING=0 WIDTH=100% BORDER=0>
<TBODY> <TR> <TD WIDTH=29 BGCOLOR=#f0f3e9><IMG HEIGHT=20
      SRC="<%=Parametros.imgPath()%>btwd_topleftcorner.gif" WIDTH=31></TD><TD WIDTH=875 BACKGROUND=<%=Parametros.imgPath()%>btwd_topbgr1.gif
      BGCOLOR=#f0f3e9><IMG HEIGHT=1 SRC="<%=Parametros.imgPath()%>spacer.gif"
    WIDTH=255></TD><TD WIDTH=31> <DIV ALIGN="right"><IMG HEIGHT=20 SRC="<%=Parametros.imgPath()%>btwd_toprightcorner.gif"
      WIDTH=30></DIV></TD></TR> </TBODY> </TABLE><TABLE CELLSPACING=0 CELLPADDING=0 WIDTH=100% BORDER=0>
<TBODY> <TR> <TD WIDTH=28 BACKGROUND=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif
      BGCOLOR=#f0f3e9 HEIGHT="177"><IMG HEIGHT=113 SRC="<%=Parametros.imgPath()%>spacer.gif"
    WIDTH=1></TD><TD WIDTH=876 BGCOLOR=#f0f3e9 HEIGHT="177"> <TABLE WIDTH="100%" BORDER="0" CELLSPACING="3" CELLPADDING="0">
<TR> <TD WIDTH="11%" HEIGHT="10"> <P><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#FFFFFF" SIZE="2"><FONT COLOR="#000000"><IMG SRC="<%=Parametros.imgPath()%>boxPaciente.jpg" WIDTH="57" HEIGHT="8"></FONT></FONT></P></TD><TD WIDTH="53%" HEIGHT="10"><B><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000" SIZE="1"><%=request.getSession().getValue("NombrePacienteActual")%></FONT></B></TD><TD WIDTH="3%" nowrap HEIGHT="10"><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#000000" SIZE="2"><IMG SRC="<%=Parametros.imgPath()%>box_fechainicio.jpg"></FONT></TD><TD WIDTH="33%" HEIGHT="10"><B><FONT COLOR="#660000" FACE="Verdana, Arial, Helvetica, sans-serif" SIZE="1"><% String campoFecha="s/d";

          try{
           if(unEpisodio.ve_ep_fecha()!=null){
             java.sql.Date date = java.sql.Date.valueOf(unEpisodio.ve_ep_fecha().substring(0,9));
             java.text.DateFormat myformat = new java.text.SimpleDateFormat("dd/MM/yyyy");
             campoFecha = myformat.format(date);
           }
           else{
             campoFecha = Parametros.sinDatos();
           }
          }
          catch(Exception e){LogFile.log("detalleEdpisodio: Siapwin problema con el formato de fecha");  }
       %> </FONT> </B><DIV ALIGN="left"></DIV><DIV ALIGN="left"></DIV><DIV ALIGN="left"><B><FONT FACE="Verdana, Arial, Helvetica, sans-serif" SIZE="1" COLOR="#660000"><%=campoFecha%></FONT></B></DIV></TD></TR>
<TR> <TD COLSPAN="4" HEIGHT="2" BACKGROUND="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><IMG SRC="<%=Parametros.imgPath()%>btwd_topbgr1.gif" HEIGHT="1"></TD></TR>
<TR> <TD WIDTH="11%" nowrap HEIGHT="6"><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#FFFFFF" SIZE="2"><FONT COLOR="#000000"><IMG SRC="<%=Parametros.imgPath()%>boxHistoriaclinica.jpg" WIDTH="83" HEIGHT="8"></FONT></FONT></TD><TD WIDTH="53%" HEIGHT="6"><B><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000" SIZE="1"><%=strNhc%></FONT></B></TD><TD WIDTH="3%" HEIGHT="6"><IMG SRC="<%=Parametros.imgPath()%>box_horainicio.jpg"></TD><TD WIDTH="33%" HEIGHT="6"><B>
<FONT COLOR="#660000" FACE="Verdana, Arial, Helvetica, sans-serif" SIZE="1"><%=(unEpisodio.ve_ep_hora()!=null)?unEpisodio.ve_ep_hora().substring(0,5):Parametros.sinDatos()%></FONT></B></TD></TR>
<TR> <TD COLSPAN="4" HEIGHT="2" BACKGROUND="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><IMG SRC="<%=Parametros.imgPath()%>btwd_topbgr1.gif" HEIGHT="1"></TD></TR>
<TR> <TD WIDTH="11%" HEIGHT="2"><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#FFFFFF" SIZE="2"><IMG SRC="<%=Parametros.imgPath()%>boxNepisodios.jpg" WIDTH="83" HEIGHT="11"></FONT></TD><TD WIDTH="53%" HEIGHT="2"><B><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000" SIZE="1"><%=unEpisodio.ve_ep_ref()%></FONT></B></TD><TD WIDTH="3%" HEIGHT="2"><IMG SRC="<%=Parametros.imgPath()%>Box_gradoSeveridad.jpg"></TD><TD WIDTH="33%" HEIGHT="2"><B><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000" SIZE="1"><%=(unEpisodio.ve_sev_pk()!=null)?unEpisodio.ve_sev_pk():Parametros.sinDatos()%></FONT></B></TD></TR>
<TR> <TD COLSPAN="4" HEIGHT="2" BACKGROUND="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><IMG SRC="<%=Parametros.imgPath()%>btwd_topbgr1.gif" HEIGHT="1"></TD></TR>
<TR> <TD WIDTH="11%" HEIGHT="9"><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#000000" SIZE="2"><IMG SRC="<%=Parametros.imgPath()%>boxEstado.jpg" WIDTH="39" HEIGHT="8">
</FONT></TD><TD WIDTH="53%" HEIGHT="9"><B><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000" SIZE="1"><%=(strEstado!=null)?strEstado:Parametros.sinDatos()%><IMG SRC="pics/spacer.gif" WIDTH="100
				" HEIGHT="1"></FONT></B></TD><TD WIDTH="3%" HEIGHT="9"><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#000000" SIZE="2"><B><IMG SRC="<%=Parametros.imgPath()%>Box_fechaCierre.jpg"></B></FONT></TD><TD WIDTH="33%" HEIGHT="9"><B><FONT COLOR="#660000" FACE="Verdana, Arial, Helvetica, sans-serif" SIZE="1"><% LogFile.log("fecha "+unEpisodio.ve_ep_fechaend());
         if(unEpisodio.ve_ep_fechaend()!=null && !unEpisodio.ve_ep_fechaend().equals("s/d")){
            try{
             java.sql.Date date = java.sql.Date.valueOf(unEpisodio.ve_ep_fechaend().substring(0,9));
             java.text.DateFormat myformat = new java.text.SimpleDateFormat("dd/MM/yyyy");
             campoFecha = myformat.format(date);
            }catch(Exception e){}
           }
           else{
             campoFecha = Parametros.sinDatos();
           } %></FONT></B><DIV ALIGN="left"><B><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000" SIZE="1"><%=campoFecha%>
</FONT></B></DIV></TD></TR> <TR BACKGROUND="<%=Parametros.imgPath()%>btwd_topbgr1.gif">
<TD COLSPAN="4" HEIGHT="2"><IMG SRC="<%=Parametros.imgPath()%>btwd_topbgr1.gif" HEIGHT="1"></TD></TR>
<TR><TD WIDTH="11%" nowrap HEIGHT="26"><IMG SRC="<%=Parametros.imgPath()%>Box_observaciones.jpg" HEIGHT="8"></TD><TD COLSPAN="3" HEIGHT="26"><P><B><FONT COLOR="#660000" FACE="Verdana, Arial, Helvetica, sans-serif" SIZE="1"><%=(unEpisodio.ve_ep_resolucion()!=null)?unEpisodio.ve_ep_resolucion():Parametros.sinDatos()%></FONT></B></P></TD></TR><TR><TD COLSPAN="4" nowrap HEIGHT="2" BACKGROUND="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><IMG SRC="<%=Parametros.imgPath()%>btwd_topbgr1.gif" HEIGHT="1"></TD></TR>
<TR> <TD WIDTH="11%" nowrap HEIGHT="26"><IMG SRC="<%=Parametros.imgPath()%>boxDiagnostico.jpg" WIDTH="61" HEIGHT="11"></TD><TD COLSPAN="3" HEIGHT="26"><B><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000" SIZE="1"><%=(strDiagnostico!=null)?strDiagnostico:Parametros.sinDatos()%></FONT></B></TD></TR>
</TABLE></TD><TD WIDTH=31 BACKGROUND=<%=Parametros.imgPath()%>btwd_mdlbgr2.gif HEIGHT="177"><IMG HEIGHT=113
      SRC="<%=Parametros.imgPath()%>spacer.gif" WIDTH=1><IMG SRC="<%=Parametros.imgPath()%>btwd_mdlbgr2.gif" WIDTH="30" HEIGHT="100%"></TD></TR>
</TBODY> </TABLE><TABLE CELLSPACING=0 CELLPADDING=0 BORDER=0 WIDTH=100%> <TBODY>
<TR> <TD WIDTH=5><IMG HEIGHT=26 SRC="<%=Parametros.imgPath()%>btwd_btmleftcorner.gif"
      WIDTH=5></TD><TD BACKGROUND=<%=Parametros.imgPath()%>btwd_btmbgr1.gif WIDTH="20"><IMG HEIGHT=26
      SRC="<%=Parametros.imgPath()%>btwd_btmmiddle.gif" WIDTH=26></TD><TD BACKGROUND=<%=Parametros.imgPath()%>btwd_btmbgr1.gif WIDTH="863"><IMG HEIGHT=1
      SRC="<%=Parametros.imgPath()%>spacer.gif" WIDTH=255></TD><TD BACKGROUND=<%=Parametros.imgPath()%>btwd_btmbgr2.gif WIDTH="23">
<DIV ALIGN="right"><IMG HEIGHT=26
      SRC="<%=Parametros.imgPath()%>btwd_btmmiddle2.gif" WIDTH=23></DIV></TD><TD WIDTH=9>
<DIV ALIGN="right"><IMG HEIGHT=26 SRC="<%=Parametros.imgPath()%>btwd_btmrightcorner.gif"
      WIDTH=7></DIV></TD></TR> </TBODY> </TABLE></TD></TR> </TABLE><DIV ALIGN="CENTER"><A HREF="#" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image37','','<%=Parametros.imgPath()%>bt_cerrar1.jpg',1)" ONCLICK="window.close();return false"><IMG NAME="Image37" BORDER="0" SRC="<%=Parametros.imgPath()%>bt_cerrar.jpg" WIDTH="78" HEIGHT="23"></A>
</DIV>
</body>
</html>
