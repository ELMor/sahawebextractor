<%@ page import="java.util.*, com.ssi.persistence.model.PersistenceManager, com.ssi.persistence.model.JDBC.JDBCAnswerResultSet" %>
<%@ page import="java.sql.*, com.ssi.model.salud.*,com.ssi.util.*" %>

<jsp:useBean id="busquedaEncuentros" scope="session" class="com.ssi.bean.salud.BusquedaEncuentros" />
<%
	Vector vectorEncuentros = busquedaEncuentros.getListaEncuentros();
	int limite = vectorEncuentros.size();
	String strNumero = request.getParameter("numero");
	String sistema = request.getParameter("sistema");
	boolean encontrado=false;
	int i=0;
	EncuentroSalud unEncuentro =null;
	while(i<limite && !encontrado){
 	   unEncuentro = (EncuentroSalud) vectorEncuentros.elementAt(i);
 	   if(unEncuentro.idEncuentro().equals(strNumero) && sistema.equals(unEncuentro.sistema().nombreSistema()))
 	     encontrado=true;
 	   i++;
	}

         //Busco descripción del codigo de Diagnostico
         String strDiagnostico= "s/d";
         PersistenceManager manager = null;
	 String query = null;
	 LogFile.log("detalleEncuentro: sistema="+sistema);

	 JDBCAnswerResultSet rs2 = null;

	  try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  =  "SELECT icd_nom FROM icd where icd_cod='"+unEncuentro.ve_en_icd_cod()+"'";
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    strDiagnostico = (String)jrs2.getObject("icd_nom");
	   }
	   catch(Exception e){
	    LogFile.log(e);
	   }
	   finally{
	    rs2.close();
	   }


String tipoEncuentro[] = {"","URGENCIAS","CONSULTAS EXTERNAS","HOSPITALIZACION","OTROS","AMBULATORIO","PROGRAMADO"};
String estadoEncuentro[] = {"CERRADO","ABIERTO","CERRADO"};

%>

<html>
<head>
<title>Detalle de Encuentro</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

  <script language="JavaScript">
<!--

function launch(newURL, newName, newFeatures, orgName) {
  var remote = open(newURL, remote, newFeatures);
}

function launchRemote(direccion) {
 myRemote = launch(direccion, "pepe", "height=300,width=600,channelmode=0,dependent=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0", "pepe");
}

// -->
</script>


<body onBlur="self.focus()" bgcolor="#eeeeee" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('pics/bt_cerrar.jpg')">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr> <td valign="middle"> <table width="95%" border="0" cellspacing="0" cellpadding="0" valign="top" align="center">
<input type='hidden' name='view' value='busquedaPaciente'> <tr> <td valign="top">
<table cellspacing=0 cellpadding=0 width=100% border=0> <tbody> <tr> <td width=14 background=<%=Parametros.imgPath()%>drwd_topbgr1.gif
      bgcolor=#fcfdfa><img height=11
      src="<%=Parametros.imgPath()%>drwd_topleftcorner.gif" width=5></td><td width=904 background=<%=Parametros.imgPath()%>drwd_topbgr1.gif
      bgcolor=#fcfdfa><img height=8 src="<%=Parametros.imgPath()%>spacer.gif"
    width=287></td><td width=17> <div align="right"><img height=11 src="<%=Parametros.imgPath()%>drwd_toprightcorner.gif"
      width=24></div></td></tr> </tbody> </table><table cellspacing=0 cellpadding=0 width=100% border=0 height="1">
<tbody> <tr> <td width=9 height="5"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlleftcorner.gif"
      width=5></td><td align=right width=35 bgcolor=#fcfdfa height="5"><img height=13
      src="<%=Parametros.imgPath()%>drwd_iconnews.gif" width=13></td><td width=865 bgcolor=#fcfdfa height="5">
<p><img src="<%=Parametros.imgPath()%>titu_detalleEncuentro.jpg" WIDTH="133" HEIGHT="10"></p></td><td width=26 height="5">
<div align="right"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlrightcorner.gif"
      width=24></div></td></tr> </tbody> </table><table cellspacing=0 cellpadding=0 width=100% border=0>
<tbody> <tr> <td width=29 bgcolor=#f0f3e9><img height=20
      src="<%=Parametros.imgPath()%>btwd_topleftcorner.gif" width=31></td><td width=875 background=<%=Parametros.imgPath()%>btwd_topbgr1.gif
      bgcolor=#f0f3e9><img height=1 src="<%=Parametros.imgPath()%>spacer.gif"
    width=255></td><td width=31> <div align="right"><img height=20 src="<%=Parametros.imgPath()%>btwd_toprightcorner.gif"
      width=30></div></td></tr> </tbody> </table><table cellspacing=0 cellpadding=0 width=100% border=0>
<tbody> <tr> <td width=28 background=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif
      bgcolor=#f0f3e9 height="177"><img height=113 src="<%=Parametros.imgPath()%>spacer.gif"
    width=1></td><td width=876 bgcolor=#f0f3e9 height="177"> <table width="100%" border="0" cellspacing="3" cellpadding="0">
<tr> <td width="11%" height="10"> <p><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000"><img src="<%=Parametros.imgPath()%>boxPaciente.jpg" width="57" height="8"></font></font></p></td><td width="53%" height="10"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><%=request.getSession().getValue("NombrePacienteActual")%></font></b></td><td width="9%" nowrap height="10"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><img src="<%=Parametros.imgPath()%>boxNepisodios.jpg" width="83" height="11"></font></td><td width="24%" height="10"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(unEncuentro.ve_ep_ref()!=null)?unEncuentro.ve_ep_ref():Parametros.sinDatos()%></b></font></td></tr>
<tr> <td colspan="4" height="2" background="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><img src="<%=Parametros.imgPath()%>btwd_topbgr1.gif" height="1"></td></tr>
<tr> <td width="11%" nowrap height="6"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000"><img src="<%=Parametros.imgPath()%>boxHistoriaclinica.jpg" width="83" height="8"></font></font></td><td width="53%" height="6"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><%=(String)request.getSession().getValue("NHC_Actual")%></font></b></td><td width="9%" height="6"><img src="<%=Parametros.imgPath()%>boxFechaNacimiento.jpg" width="74" height="8"></td><td width="24%" height="6"><b>
<font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#660000"> <% String campoFecha=null;
           if(unEncuentro.fechaInicio()!=null){
             java.sql.Date date = java.sql.Date.valueOf(unEncuentro.fechaInicio().substring(0,9));
             java.text.DateFormat myformat = new java.text.SimpleDateFormat("dd/MM/yyyy");
             campoFecha = myformat.format(date);
           }
           else{ campoFecha = Parametros.sinDatos();
           }
       %> <%=campoFecha%></font></b></td></tr> <tr> <td colspan="4" height="2" background="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><img src="<%=Parametros.imgPath()%>btwd_topbgr1.gif" height="1"></td></tr>
<tr> <td width="11%" height="2"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000"><img src="<%=Parametros.imgPath()%>BoxNumero.jpg" width="47" height="8">
</font></font></td><td width="53%" height="2"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#660000"><%=unEncuentro.idEncuentro()%></font></b></td><td width="9%" height="2"><img src="<%=Parametros.imgPath()%>box_horainicio.jpg" width="74" height="8"></td><td width="24%" height="2"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#660000" ><%=(unEncuentro.horaInicio()!=null)?unEncuentro.horaInicio().substring(0,5):Parametros.sinDatos()%></font></b></td></tr>
<tr> <td colspan="4" height="2" background="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><img src="<%=Parametros.imgPath()%>btwd_topbgr1.gif" height="1"></td></tr>
<tr> <td width="11%" height="8"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><img src="<%=Parametros.imgPath()%>boxEstado.jpg" width="39" height="8">
</font></td><td width="53%" height="8"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#660000"><%=(unEncuentro.ve_en_estado()!=null)? estadoEncuentro[Integer.parseInt(unEncuentro.ve_en_estado())]:Parametros.sinDatos()%><img src="pics/spacer.gif" width="100
				" height="1"></font></b></td><td width="9%" height="8"><img src="<%=Parametros.imgPath()%>box_tipoencuentro.jpg" width="90" height="12"></td><td width="24%" height="8"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(unEncuentro.ve_en_tipo()!=null)? tipoEncuentro[Integer.parseInt(unEncuentro.ve_en_tipo())]:Parametros.sinDatos()%></b></font></td></tr>
<tr> <td colspan="4" height="2" background="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><img src="<%=Parametros.imgPath()%>btwd_topbgr1.gif" height="1"></td></tr>
<tr> <td width="11%" height="24"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><img src="<%=Parametros.imgPath()%>boxMotivo.jpg" width="40" height="8"></font></td><td colspan="3" height="24"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=(unEncuentro.ve_en_motivo()!=null)?unEncuentro.ve_en_motivo():Parametros.sinDatos()%></b></font></td></tr>
<td colspan="4" height="2" background="<%=Parametros.imgPath()%>btwd_topbgr1.gif"><img src="<%=Parametros.imgPath()%>btwd_topbgr1.gif" height="1"></td><td colspan="4" height="2" width="3%"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><img src="<%=Parametros.imgPath()%>btwd_topbgr1.gif" height="1"></font></td></tr>
<tr> <td width="11%" nowrap height="26"><img src="<%=Parametros.imgPath()%>boxDiagnostico.jpg" width="61" height="11"></td><td colspan="3" height="26"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000" size="1"><b><%=strDiagnostico%></b></font></td></tr>
</table></td><td width=31 background=<%=Parametros.imgPath()%>btwd_mdlbgr2.gif height="177"><img height=113
      src="<%=Parametros.imgPath()%>spacer.gif" width=1><img src="<%=Parametros.imgPath()%>btwd_mdlbgr2.gif" width="30" height="100%"></td></tr>
</tbody> </table><table cellspacing=0 cellpadding=0 border=0 width=100%> <tbody>
<tr> <td width=5><img height=26 src="<%=Parametros.imgPath()%>btwd_btmleftcorner.gif"
      width=5></td><td background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif width="20"><img height=26
      src="<%=Parametros.imgPath()%>btwd_btmmiddle.gif" width=26></td><td background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif width="863"><img height=1
      src="<%=Parametros.imgPath()%>spacer.gif" width=255></td><td background=<%=Parametros.imgPath()%>btwd_btmbgr2.gif width="23">
<div align="right"><img height=26
      src="<%=Parametros.imgPath()%>btwd_btmmiddle2.gif" width=23></div></td><td width=9>
<div align="right"><img height=26 src="<%=Parametros.imgPath()%>btwd_btmrightcorner.gif"
      width=7></div></td></tr> </tbody> </table></td></tr> </table><div align="center"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image37','','<%=Parametros.imgPath()%>bt_cerrar1.jpg',1)" onClick="window.close();return false"><br>
<img name="Image37" border="0" src="<%=Parametros.imgPath()%>bt_cerrar.jpg" width="78" height="23"></a><br>
</div></td></tr> </table>
</body>
</html>
