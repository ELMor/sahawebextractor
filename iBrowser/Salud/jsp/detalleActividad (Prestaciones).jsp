<%@ page import="java.util.*, com.ssi.persistence.model.PersistenceManager, com.ssi.persistence.model.JDBC.JDBCAnswerResultSet" %>
<%@ page import="java.sql.*, com.ssi.model.salud.*,com.ssi.util.*" %>
<jsp:useBean id="busquedaActividades" scope="session" class="com.ssi.bean.salud.BusquedaActividades" />

<%
	String strNumero = request.getParameter("numero");
	String sistema = request.getParameter("sistema");
	Vector vectorActividad = busquedaActividades.getListaActividades();
	int limite = vectorActividad.size();
	boolean encontrado=false;
	int i=0;
	ActividadSalud unaActividad = null;
	while(i<limite && !encontrado){
 	   unaActividad = (ActividadSalud) vectorActividad.elementAt(i);
 	   if(unaActividad.ve_ac_ref().equals(strNumero) && sistema.equals(unaActividad.sistema().nombreSistema()))
 	    encontrado=true;
 	    i++;
	}


	String profesionalsolicitante= "s/d";
	String servicioSolicitante= "s/d";
	String solicitadoA= "s/d";


        PersistenceManager manager = null;
        String query = null;
	LogFile.log("detalleActividad: sistema="+sistema);
	JDBCAnswerResultSet rs2 = null;


//Prestaciones:

/*
Vector prestaciones = new Vector();
	if(sistema.equals("SistemaNovahis")){
	   try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  ="select prest_item.prest_item_desc from actividad_det,prest_item where prest_item.prest_item_pk = actividad_det.prest_item_pk and actividad_det.actividad_pk ="+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    ResultSet jrs2 = rs2.resultSet();
            //Vector prestaciones = new Vector();
            while (jrs2.next()){
		prestaciones.addElement((String)jrs2.getObject(1));
            }

	   }
	   catch(Exception ex){
	    LogFile.log(ex);
	   }
	   finally{
	    rs2.close();
	   }
	}

        int tamanoVector = prestaciones.size();

*/








// Profesional Solicitante:

	if(sistema.equals("SistemaNovahis")){
	   try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  = "select fpersona.nombre_corto from fpersona, actividad where fpersona.codigo_personal = actividad.codigo_personal and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    profesionalsolicitante = (String)jrs2.getObject(1);
	   }
	   catch(Exception ex){
	    LogFile.log(ex);
	   }
	   finally{
	    rs2.close();
	   }
	}


	if(sistema.equals("SistemaSiapwin")){
	   try{
	    manager = SistemaSiapwin.getInstance().getPersistenceManager();
	    query  = "select fpersona.nombre_corto from fpersona, actividad where fpersona.codigo_personal = actividad.codigo_personal and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    profesionalsolicitante = (String)jrs2.getObject(1);
	   }
	   catch(Exception ex){
	    LogFile.log(ex);
	   }
	   finally{
	    rs2.close();
	   }
	}

//Servicio Solicitante:

	if(sistema.equals("SistemaNovahis")){
	   try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  = "select servicios.servicio from servicios, actividad where servicios.codigo_servicio = actividad.codigo_servicio and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    servicioSolicitante = (String)jrs2.getObject(1);
	   }
	   catch(Exception ex){
	    LogFile.log(ex);
	   }
	   finally{
	    rs2.close();
	   }
	}


	if(sistema.equals("SistemaSiapwin")){
	   try{
	    manager = SistemaSiapwin.getInstance().getPersistenceManager();
	    query  = "select servicios.servicio from servicios, actividad where servicios.codigo_servicio = actividad.codigo_servicio and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    servicioSolicitante = (String)jrs2.getObject(1);
	   }
	   catch(Exception ex){
	    LogFile.log(ex);
	   }
	   finally{
	    rs2.close();
	   }
	}



//Servicio Solicitado a :


	if(sistema.equals("SistemaNovahis")){
	   try{
	    manager = SistemaNovahis.getInstance().getPersistenceManager();
	    query  = "select servicios.servicio from servicios, actividad where servicios.codigo_servicio = actividad.codigo_servicio1 and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    solicitadoA = (String)jrs2.getObject(1);
	   }
	   catch(Exception ex){
	    LogFile.log(ex);
	   }
	   finally{
	    rs2.close();
	   }
	}


	if(sistema.equals("SistemaSiapwin")){
	   try{
	    manager = SistemaSiapwin.getInstance().getPersistenceManager();
    	    query  = "select servicios.servicio from servicios, actividad where servicios.codigo_servicio = actividad.codigo_servicio1 and actividad_pk = "+strNumero.trim();
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    java.sql.ResultSet jrs2 = rs2.resultSet();
	    solicitadoA = (String)jrs2.getObject(1);
	   }
	   catch(Exception ex){
	    LogFile.log(ex);
	   }
	   finally{
	    rs2.close();
	   }
	}





String tipoActividad[] = {"","PROCEDIMIENTOS","LABORATORIO","ANATOMIA PATOLOGICA","IMAGEN","CO","MEDICAMENTOS","MATERIALES","COCINA","DIETETICA","HOSTELERIA","OTROS"};
String estadoActividad[] = {"","ABIERTA","CERRADA","VALIDADA"};

%>


<html><body onBlur="self.focus()" bgcolor="#eeeeee" marginheight="0" topmargin="10">
<br>


<head>
<title>Detalle de la Actividad</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>
</head>


<br>
<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">

<tr valign="top">

<td width="3%" align="right"><img src="<%=Parametros.imgPath()%>solapa_left.gif" width="28" height="25"></td>
<td bgcolor="#C6BEA7" align="center" width="94%" valign="middle">

<div align="left"><font color="#003366" face="Verdana, Arial, Helvetica, sans-serif" size="3"><b>Detalle
      de la Actividad</b></font></div>
</td>
<td width="3%"><img src="<%=Parametros.imgPath()%>solapa_right.gif" width="29" height="25"></td>
</tr>

</table>
<table width="90%" border="0" cellspacing="0" cellpadding="0" bgcolor="#012652" align="center">
  <tr bgcolor="#FFFFCC">
    <td width="2%" height="32" align="center">
      <p align="left"><font color="#000000">&nbsp;<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
        &nbsp; </font></font></p>
    </td>
    <td width="18%" height="32" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000">Paciente:</font></font><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"></font></div>
    </td>
    <td colspan="3" height="32" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000"><b><%=request.getSession().getValue("NombrePacienteActual")%></b></font></font></div>
    </td>
  </tr>
  <tr bgcolor="#CCCC99">
    <td colspan="5" height="2" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td width="2%" height="30" align="center">&nbsp;</td>
    <td width="18%" valign="middle" height="30" align="center" nowrap>
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000">Historia
        cl�nica:</font></font></div>
    </td>
    <td width="35%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><b><%=(String)request.getSession().getValue("NHC_Actual")%></b></font></div>
    </td>
    <td width="15%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">Fecha
        de Inicio:</font></div>
    </td>
    <td width="30%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif" size="2">
        <%    String campoFecha=null;
            if(unaActividad.ve_ac_fecha()!=null){
             java.sql.Date date = java.sql.Date.valueOf(unaActividad.ve_ac_fecha().substring(0,9));
             java.text.DateFormat myformat = new java.text.SimpleDateFormat("dd/MM/yyyy");
             campoFecha = myformat.format(date);
            }
            else{
             campoFecha = Parametros.sinDatos();
            }
       %>
        </font>
        <div align="left"></div>
        <div align="left"></div>
        <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000"><b><%=campoFecha%></b>
          </font></div>
      </div>
    </td>
  </tr>
  <tr bgcolor="#CCCC99">
    <td colspan="5" height="2" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td width="2%" height="30" align="center">&nbsp;</td>
    <td width="18%" valign="middle" height="30" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000">N&uacute;mero
        de Actividad:</font></font></div>
    </td>
    <td width="35%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><b><%=(unaActividad.ve_ac_ref()!=null)?unaActividad.ve_ac_ref():Parametros.sinDatos()%></b></font></div>
    </td>
    <td width="15%" height="30" bgcolor="#FFFFCC" align="center" nowrap>
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">Hora
        de Inicio:</font></div>
    </td>
    <td width="30%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><b><%=(unaActividad.ve_ac_hora()!=null)?unaActividad.ve_ac_hora().substring(0,5):Parametros.sinDatos()%></b></font></div>
    </td>
  </tr>
  <tr bgcolor="#CCCC99">
    <td colspan="5" height="2" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td width="2%" height="30" align="center">&nbsp;</td>
    <td width="18%" valign="middle" height="30" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000">Profesional
        Solicitante:</font></font></div>
    </td>
    <td width="35%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><b>

<%=(profesionalsolicitante!=null)?profesionalsolicitante:Parametros.sinDatos()%>

    </b></font></div>
    </td>
    <td width="15%" valign="middle" height="30" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000">Servicio
        Solicitante:</font></font></div>
    </td>
    <td width="30%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><b>

<%=(servicioSolicitante!=null)?servicioSolicitante:Parametros.sinDatos()%>



</b></font></div>


    </td>
  </tr>
  <tr bgcolor="#CCCC99">
    <td colspan="5" height="2" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td width="2%" height="30" align="center">&nbsp;</td>
    <td width="18%" valign="middle" height="30" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><font color="#000000">N&uacute;mero
        de Encuentro:</font></font></div>
    </td>
    <td width="35%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="#000000"><b><%=unaActividad.ve_en_ref()%></b></font></div>
    </td>
    <td width="15%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">Tipo:</font></div>
    </td>
    <td width="30%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><b><%=(unaActividad.ve_ac_tipo()!=null)? tipoActividad[Integer.parseInt(unaActividad.ve_ac_tipo())]:Parametros.sinDatos()%></b></font></div>
    </td>
  </tr>
  <tr bgcolor="#CCCC99">
    <td colspan="5" height="2" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td width="2%" height="30" align="center">&nbsp;</td>
    <td width="18%" valign="middle" height="30" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">Estado:</font></div>
    </td>
    <td width="35%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><b><%=(unaActividad.ve_ac_estado()!=null)? estadoActividad[Integer.parseInt(unaActividad.ve_ac_estado())]:Parametros.sinDatos()%>&nbsp;</b></font></div>
    </td>
    <td width="15%" height="30" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">Solicitado
        a: </font></div>
    </td>
    <td width="30%" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><b>


	 <%=(solicitadoA!=null)?solicitadoA:Parametros.sinDatos()%>



	</b></font></div>
    </td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td colspan="5" height="2" bgcolor="#CCCC99" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td width="2%" height="30" align="center">&nbsp;</td>
    <td width="18%" valign="middle" height="30" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">Descripci&oacute;n:</font></div>
    </td>
    <td colspan="3" height="30" bgcolor="#FFFFCC" align="center">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2"><b><%=(unaActividad.ve_ac_servicio()!=null)?unaActividad.ve_ac_servicio():Parametros.sinDatos()%></b></font></div>
    </td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td colspan="5" height="2" bgcolor="#CCCC99" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>
  <tr bgcolor="#FFFFCC">
    <td colspan="5" height="2" bgcolor="#CCCC99" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>

  <!--<tr align="center">
    <td width="4%" height="0" bgcolor="#012652">&nbsp;</td>
    <td width="92%" height="32" valign="middle">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2">Clave
        de apertura:<b>1</b> </font></div>
    </td>
    <td width="4%" height="26" bgcolor="#012652">&nbsp;</td>
  </tr>-->
  <!--<tr align="center">
    <td width="4%" height="26" bgcolor="#012652">&nbsp;</td>
    <td width="92%" valign="middle" height="30">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2">C&oacute;digo
        de diagn&oacute;stico:<b>1</b> </font></div>
    </td>
    <td width="4%" height="26" bgcolor="#012652">&nbsp;</td>
  </tr>-->
  <!--<tr align="center">
    <td width="4%" height="26" bgcolor="#012652">&nbsp;</td>
    <td width="92%" valign="middle" height="31">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2">Hora
        de apertura cl&iacute;nica:</font><font color="#000000"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b><font color="#FFFFFF">15.30
        hs</font></b></font> </font></div>
    </td>
    <td width="4%" height="26" bgcolor="#012652">&nbsp;</td>
  </tr>-->
  <!--<tr align="center">
    <td width="4%" height="26" bgcolor="#012652">&nbsp;</td>
    <td width="92%" valign="middle" height="34">
      <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2">Hora
        de apertura cl&iacute;nica:</font><font color="#000000"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b><font color="#FFFFFF">15.30
        hs</font></b></font> </font></div>
    </td>
    <td width="4%" height="26" bgcolor="#012652">&nbsp;</td>
  </tr>-->
</table>



<table width="90%" border="0" cellspacing="0" cellpadding="0"  align="center" bordercolor="#000000">
  <tr bgcolor="#003366">
    <td colspan="4" height="20" align="center">
      <div align="left"><font color="#003366" face="Verdana, Arial, Helvetica, sans-serif" size="3"><b><font size="2" color="#FFFFFF">Prestaciones</font></b></font></div>
    </td>
  </tr>

<tr bgcolor="#FFFFFF">
    <td colspan="3" height="30" align="center">
      <div align="left"><font color="#000000">Poner Dato</font></div>
    </td>
  </tr>


  <tr bgcolor="#FFFFCC">
    <td colspan="4" height="2" bgcolor="#CCCC99" align="center"><img src="<%=Parametros.imgPath()%>dottrans.gif" width="1" height="1"></td>
  </tr>
</table>





<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">

<tr valign="top" bgcolor="#FFFFFF">

<td width="49%" align="right">

<div align="left">&nbsp;</div>
</td>
<td width="51%">

<div align="right"><a href="#" onClick="window.close();return false"> <img src="<%=Parametros.imgPath()%>btcerrar.jpg" width="53" height="18" border="0"></a>
        &nbsp;&nbsp;&nbsp;&nbsp; </div>
</td>
</tr>
</table>
<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">

<tr valign="top">

<td width="3%" align="right" valign="bottom" height="20"><img src="<%=Parametros.imgPath()%>solapa_left_down.gif" width="28" height="27"></td>
<td bgcolor="#C6BEA7" align="left" width="93%" height="20"><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF" size="2"><img src="<%=Parametros.imgPath()%>solapa_detalles.jpg"></font></td>
<td width="4%" valign="bottom" height="20"><img src="<%=Parametros.imgPath()%>solapa_right_down.gif" width="30" height="27"></td>
</tr></table>
<br>
</body></html>
