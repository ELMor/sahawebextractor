<%@ page import="java.util.*, com.ssi.model.salud.*" %>
<%@ page import="com.ssi.util.*" %>
<jsp:useBean id="busquedaEncuentros" scope="session" class="com.ssi.bean.salud.BusquedaEncuentros" />
<jsp:useBean id="busquedaEpisodios" scope="session" class="com.ssi.bean.salud.BusquedaEpisodios" />
<jsp:useBean id="busquedaPaciente" scope="session" class="com.ssi.bean.salud.BusquedaPaciente" />

<html>
<head>
<title>Encuentros Ant</title>
<SCRIPT LANGUAGE="JavaScript" SRC="\jsp\js\busquedaEncuentros.js"></SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>


</head>
<body bgcolor="#FFFFFF" leftmargin="0" marginwidth="0" marginheight="0">
<form name="busquedaEncuentros"  method="post">
<input type='hidden' name='view' value='busquedaEncuentros'>

<table width="85%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="3%" valign="top" align="left" bgcolor="#C6BEA7">
      <div align="left"><img src="<%=Parametros.imgPath()%>c1.jpg" width="25" height="25"></div>
    </td>
    <td width="96%" bgcolor="#C6BEA7"><font color="#003366" face="Verdana, Arial, Helvetica, sans-serif" size="3">Encuentros</font></td>
    <td width="96%" bgcolor="#C6BEA7">
      <div align="right"><img src="<%=Parametros.imgPath()%>btinfo.jpg" width="31" height="29"></div>
    </td>
    <td width="1%" valign="top" align="right" bgcolor="#C6BEA7">
      <div align="right"><img src="<%=Parametros.imgPath()%>c2.jpg" width="25" height="25"></div>
    </td>
  </tr>
</table>
<table width="85%" align="center">
<%
	Vector vectorPacientes = busquedaPaciente.getListaPacientes();
	int limite2 = vectorPacientes.size();
	for (int i=0; i< limite2; i++){
		PacienteSalud unPaciente = (PacienteSalud) vectorPacientes.elementAt(i);
		if (unPaciente.codigoCliente().trim().equals(busquedaEpisodios.getPacientes().elementAt(0))){ %>
  			<tr bgcolor="#FFFDE3">
    			<td height="13" bgcolor="#FFFDE3" width="17"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000">Nombre:</font><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>
      			</b></font></td>
    			<td width="80" nowrap height="13" valign="middle" bgcolor="#FFFDE3"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b><%= unPaciente.nombre()%></b></font></td>
    			<td width="17" nowrap height="13">
      			<div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000">Apellido1:</font><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b></font></div>
    			</td>
    			<td width="17" nowrap height="13" valign="middle"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b><%= unPaciente.apellido1()%></td>
    			<td width="181" nowrap height="13">
      			<div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000">Apellido2:&nbsp;</font><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b><%= unPaciente.apellido2()%></b></font></div>
    			</td>
    			<td width="17" nowrap height="13" valign="middle"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000">NHC:&nbsp;</font></td>
    			<td width="154" height="13">
      			<div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"></font><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"></font></div>
    			</td>
    			<td width="17" nowrap height="13" valign="middle"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000">RFC:&nbsp;</font></td>
    			<td colspan="4" width="223" height="13">
      			<div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"></font><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>&nbsp;<%= unPaciente.RFC()%></b></font></div>
    			</td>
  			</tr>

<%		} //ENDif
	} //ENDfor
	 %>
</table>
<table width="85%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#012652" height="">
  <tr>
    <td width="2%"><img src="<%=Parametros.imgPath()%>dotblue.jpg" width="2" height="2"></td>
    <td colspan="3"></td>
    <td width="9%"></td>
    <td width="14%"></td>
    <td width="8%"></td>
    <td width="7%"></td>
    <td width="11%"></td>
    <td width="15%"></td>
    <td width="22%"></td>
  </tr>
  <tr>
    <td width="2%" bgcolor="#FFFFFF" height="32">
      <div align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><font color="#003366"><font color="#FF0000"><font color="#330000"></font></font></font></font></div>
    </td>
    <td bgcolor="#FFFFFF" colspan="3" height="32">
      <div align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#330000">Encuentro N�</font></div>
    </td>
    <td width="9%" bgcolor="#FFFFFF" height="32"> <font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#330000"></font>
    </td>
    <td width="14%" bgcolor="#FFFFFF" height="32">
      <div align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#330000">Fecha
        y Hora</font></div>
    </td>
    <td width="8%" bgcolor="#FFFFFF" height="32">
      <div align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#330000">Servicio</font></div>
    </td>
    <td width="7%" bgcolor="#FFFFFF" height="32">
      <div align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#330000">Motivo</font></div>
    </td>
    <td width="11%" bgcolor="#FFFFFF" height="32">
      <div align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#330000">Estado</font></div>
    </td>
    <td width="15%" bgcolor="#FFFFFF" height="32">
      <div align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#330000">Tipo de Encuentro
        </font></div>
    </td>
    <td width="22%" bgcolor="#FFFFFF" valign="middle" height="32">
      <div align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#330000">ICD_COD
        </font></div>
    </td>
  </tr>
  <tr bgcolor="#CCCC99">
    <td width="2%" height="4">
      <div align="center"><font color="#999999"><img src="<%=Parametros.imgPath()%>botmarron.jpg" width="2" height="2"></font></div>
    </td>
    <td height="4" colspan="3"><font color="#999999"></font></td>
    <td width="9%" height="4">
      <div align="center"></div>
    </td>
    <td width="14%" height="4"><font color="#999999"></font></td>
    <td width="8%" height="4"><font color="#999999"></font></td>
    <td width="7%" height="4"><font color="#999999"></font></td>
    <td width="11%" height="4"><font color="#999999"></font></td>
    <td width="15%" height="4"><font color="#999999"></font></td>
    <td width="22%" height="4">
      <div align="right"><font color="#999999"></font></div>
    </td>
  </tr>
  <tr>
    <td width="2%" bgcolor="#FFFFFF">
      <div align="center"></div>
    </td>
<input type='hidden' name='encuentroNumero' value='inicial'>
    <%
	Vector vectorEncuentros = busquedaEncuentros.getListaEncuentros();
	int limite = vectorEncuentros.size();
	for (int i = 0; i < limite; i++) {
 			Encuentro unEncuentro = (Encuentro) vectorEncuentros.elementAt(i);
	%>
    <td bgcolor="#eeeeee" colspan="3" nowrap align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href='JavaScript: submitEncuentro(<%= unEncuentro.idEncuentro()%>)'>Encuentro <%= unEncuentro.idEncuentro()%></a></font></td>
    <td width="9%" bgcolor="#FFFFFF" nowrap align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"></font></div></td>
    <td width="14%" bgcolor="#FFFFFF" nowrap align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= unEncuentro.fechayHoraInicio()%></font></td>
    <td width="8%"  bgcolor="#FFFFFF" nowrap align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= unEncuentro.servicio()%>
      </font></td>
    <td width="7%"  bgcolor="#FFFFFF" nowrap align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= unEncuentro.motivo()%>
      </font></td>
    <td width="11%" bgcolor="#FFFFFF" nowrap align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= unEncuentro.estado()%>
      </font></td>
    <td width="15%"  bgcolor="#FFFFFF" nowrap align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= unEncuentro.tipo()%>
      </font></td>
    <td width="22%" bgcolor="#FFFFFF" nowrap align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= unEncuentro.codigoDiagnostico()%>
      </font></td>
  </tr>
  <tr bgcolor="#CCCC99">
    <td width="2%" height="4">
      <div align="center"><font color="#999999"><img src="<%=Parametros.imgPath()%>botmarron.jpg" width="2" height="2"></font></div>
    </td>
    <td height="4" colspan="3"><font color="#999999"></font></td>
    <td width="9%" height="4">
      <div align="center"></div>
    </td>
    <td width="14%" height="4"><font color="#999999"></font></td>
    <td width="8%" height="4"><font color="#999999"></font></td>
    <td width="7%" height="4"><font color="#999999"></font></td>
    <td width="11%" height="4"><font color="#999999"></font></td>
    <td width="15%" height="4"><font color="#999999"></font></td>
    <td width="22%" height="4">
      <div align="right"><font color="#999999"></font></div>
    </td>
  </tr>
<% } //ENDfor %>

  <tr>
    <td width="2%" bgcolor="#222222" ><img src="<%=Parametros.imgPath()%>botmarron.jpg" width="2" height="2"></td>
    <td bgcolor="#222222" colspan="3" ></td>
    <td width="9%" valign="top" bgcolor="#222222" > </td>
    <td width="14%" bgcolor="#222222" ></td>
    <td width="8%" bgcolor="#222222" ></td>
    <td width="7%" bgcolor="#222222" ></td>
    <td width="11%" bgcolor="#222222" ></td>
    <td width="15%" bgcolor="#222222" ></td>
    <td width="22%" bgcolor="#222222" > </td>
  </tr>
</table>
<table width="85%" border="0" cellspacing="0" cellpadding="0" align="center" height="14">
  <tr>
    <td bgcolor="#012652" height="15" colspan="2"></td>
    <td width="14" height="15"><img src="<%=Parametros.imgPath()%>esqrec.jpg" width="14" height="14" align="absmiddle"></td>
    <td rowspan="2" width="129"> 
      <div align="right"><img src="<%=Parametros.imgPath()%>btvolver.jpg" width="58" height="19" onClick='JavaScript: history.go(-1)'><img src="<%=Parametros.imgPath()%>btbuscar.jpg" width="58" height="19" onClick='submitBuscar()'>
      </div>
    </td>
    <td rowspan="2" width="37">&nbsp;</td>
  </tr>
  <tr>
    <td  bgcolor="#C6BEA7" height="2" width="646"> </td>
    <td  bgcolor="#C6BEA7" height="2" width="12"> 
      <div align="right"><img src="<%=Parametros.imgPath()%>esqbot1.jpg" width="11" height="10"></div>
    </td>
    <td height="2" width="14" ></td>
  </tr>
</table>
<table width="85%" border="0" cellspacing="0" cellpadding="0" align="center" height="8">
  <tr>
    <td width="3%" bgcolor="#C6BEA7" valign="bottom" align="left" height="18"></td>
    <td width="95%" bgcolor="#C6BEA7" height="18"><img src="<%=Parametros.imgPath()%>logosalud.jpg" width="200" height="29"></td>
    <td width="2%" bgcolor="#C6BEA7" valign="bottom" align="right" height="18">&nbsp;</td>
  </tr>
  <tr>
    <td width="3%" bgcolor="#C6BEA7" valign="bottom" align="left">
      <div align="left"><img src="<%=Parametros.imgPath()%>c3.jpg" width="26" height="25"></div>
    </td>
    <td width="95%" bgcolor="#C6BEA7">&nbsp;</td>
    <td width="2%" bgcolor="#C6BEA7" valign="bottom" align="right">
      <div align="right"><img src="<%=Parametros.imgPath()%>c4.jpg" width="25" height="25"></div>
    </td>
  </tr>
</table>
<div align="right"></div>
</body>
</html>