 <!-- HEADER.JSP  TABLA DE DATOS DE USUARIO ///////////////////////////////////////////////////////////////////////////// --> 
<%@ page import="com.ssi.util.*" %>
 <jsp:useBean id="busquedaObrasSocialesCliente" scope="session" class="com.ssi.bean.salud.BusquedaObrasSocialesCliente" />
 <jsp:useBean id="busquedaDatosAdministrativosCliente" scope="session" class="com.ssi.bean.salud.BusquedaDatosAdministrativosCliente" />
 <jsp:useBean id="busquedaFactoresRiesgoCliente" scope="session" class="com.ssi.bean.salud.BusquedaFactoresRiesgoCliente" />
 <jsp:useBean id="busquedaAlergiasCliente" scope="session" class="com.ssi.bean.salud.BusquedaAlergiasCliente" />
 <%
   request.getSession().putValue("BusquedaObrasSocialesCliente",busquedaObrasSocialesCliente);  
   request.getSession().putValue("BusquedaDatosAdministrativosCliente",busquedaDatosAdministrativosCliente);
   request.getSession().putValue("BusquedaFactoresRiesgoCliente",busquedaFactoresRiesgoCliente);
   request.getSession().putValue("BusquedaAlergiasCliente",busquedaAlergiasCliente);
  %>

<SCRIPT language=JavaScript>

function launch(newURL, newName, newFeatures, orgName) {
  var remote = open(newURL, remote, newFeatures);
}

function launchRemote(direccion) {
 myRemote = launch(direccion, "pepe", "height=360,width=600,channelmode=0,dependent=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0", "pepe");
}

</SCRIPT>
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<body bgcolor="#f0f3e9" onLoad="MM_preloadImages('<%=Parametros.imgPath()%>btalergias1.jpg','<%=Parametros.imgPath()%>btfactores1.jpg','<%=Parametros.imgPath()%>btfinanciadores1.jpg','<%=Parametros.imgPath()%>btepisodio1.jpg','<%=Parametros.imgPath()%>btencuentros0.jpg','<%=Parametros.imgPath()%>btactividades1.jpg')">
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#f0f3e9">
  <%

	String apellido2=null;
	String rfc=null;
	String strNhc=null;
	
	String codigoPac = ((PacienteSalud)busquedaEpisodios.getPacientes().elementAt(0)).codigoCliente();
	PacienteSalud unPaciente= busquedaPaciente.buscarPaciente(codigoPac);
		
	if (unPaciente!=null){
				   
	   busquedaObrasSocialesCliente.setNumeroPacienteReferenciado(unPaciente.codigoCliente());
	   busquedaFactoresRiesgoCliente.setNumeroPacienteReferenciado(unPaciente.codigoCliente());
	   busquedaAlergiasCliente.setNumeroPacienteReferenciado(unPaciente.codigoCliente());
	  
        apellido2 = (unPaciente.apellido2()!= null) ? unPaciente.apellido2() : com.ssi.util.Parametros.sinDatos();
        rfc = (unPaciente.RFC()!= null) ? unPaciente.RFC() : com.ssi.util.Parametros.sinDatos();
                
        String strApellido2 = (apellido2== com.ssi.util.Parametros.sinDatos())? "":" "+apellido2;
        String nombrePacienteActual = unPaciente.apellido1() + strApellido2 +", "+unPaciente.nombre();
        LogFile.log("Header 1");
        request.getSession().putValue("NombrePacienteActual",nombrePacienteActual);
        LogFile.log("Header 2");
        //Setea ls historia clinica del paciente actual     
        strNhc = (String)request.getSession().getValue("NHC_Actual");
        if(strNhc.equals("empty")){
           LogFile.log("Header 3");
           com.ssi.persistence.model.HistoriaClinica hc= com.ssi.persistence.model.HistoriaClinica.userNamed(unPaciente.codigoCliente());
           strNhc = hc.getId();   
           request.getSession().putValue("NHC_Actual",strNhc);
         }
      %>
  <tr> 
    <td width="31">&nbsp;</td>
    <td width="59"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"><img src="<%=Parametros.imgPath()%>titupaciente.jpg" width="49" height="8"></font></td>
    <td valign="middle" width="361"><b><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><%= nombrePacienteActual%></font></b></td>
    <td width="250"><b><font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="#000000"><img src="<%=Parametros.imgPath()%>titu_Nhc.jpg" width="28" height="8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= strNhc %></font></b></td>
    <td  width="235"><b><font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"><img src="<%=Parametros.imgPath()%>titu_rfc.jpg" width="24" height="8"></font><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;<%= rfc%></font></b></td>
    <td width="17" valign="middle">&nbsp;</td>
  </tr>
  <tr> 
    <td width="31" height="2">&nbsp;</td>
    <td width="59" height="2"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"><img src="<%=Parametros.imgPath()%>titu_sexo.jpg" width="34" height="8">&nbsp;&nbsp;&nbsp;&nbsp; 
      </font><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"> 
      </font></td>
    <td width="361" valign="middle" height="2"><b><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><%= unPaciente.sexoDescripcion()%></font></b></td>
    <td width="250" height="2"><b><font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="#000000"> 
      <img src="<%=Parametros.imgPath()%>titu_fecha.jpg" width="61" height="8"> 
      &nbsp; 
      <% 
         GregorianCalendar gc = unPaciente.fechaNacimiento();
         int iAnio = gc.get(Calendar.YEAR);
         int iMes = gc.get(Calendar.MONTH) + 1;
         int iDia = gc.get(Calendar.DAY_OF_MONTH);
         String strFecha = ""+iDia+"/"+iMes+"/"+iAnio;
      
         GregorianCalendar hoy = new GregorianCalendar();
         int hAnio = hoy.get(Calendar.YEAR);
         int hMes = hoy.get(Calendar.MONTH) + 1;
         int hDia = hoy.get(Calendar.DAY_OF_MONTH);
         
         int iDiferencia = 0;	         
	 if (iMes > hMes){iDiferencia = hAnio-iAnio-1;}
	 if (iMes < hMes){iDiferencia = hAnio-iAnio;}
 	 if (iMes == hMes){
	      if (iDia >= hDia){
		  iDiferencia = hAnio-iAnio -1;
	      }
	      if (iDia < hDia){
		  iDiferencia = hAnio-iAnio;
	      }
	

	}

         LogFile.log("Header 4"); 
      %>
      <%=strFecha%></font></b></td>
    <td width="235" height="2"><b><font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"><img src="<%=Parametros.imgPath()%>titu_edad.jpg" width="30" height="8"> 
      </font><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><%=iDiferencia%></font></b></td>
    <td width="17" valign="middle" height="2">&nbsp;</td>
  </tr>
  <%	} //ENDif
	//} //ENDfor
        %>
</table>
<!-- TABLA DE MENU DE OPCIONES //////////////////////////////////////////////////////// -->
<table width="85%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#f0f3e9">
  <tr> 
    <td width="528" height="42" valign="bottom"> 
      <div align="left"><a href="javascript:launchRemote('com.ssi.bean.salud.ServletSalud?view=BusquedaAlergiasCliente')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image13','','<%=Parametros.imgPath()%>btalergias1.jpg',1)"><img name="Image13" border="0" src="<%=Parametros.imgPath()%>btalergias0.jpg" align="absbottom"></a><a href="javascript:launchRemote('com.ssi.bean.salud.ServletSalud?view=BusquedaFactoresRiesgoCliente')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image14','','<%=Parametros.imgPath()%>btfactores1.jpg',1)"><img name="Image14" border="0" src="<%=Parametros.imgPath()%>btfactores0.jpg" align="absbottom"></a><a href="javascript:launchRemote('com.ssi.bean.salud.ServletSalud?view=BusquedaObrasSocialesCliente')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image15','','<%=Parametros.imgPath()%>btfinanciadores1.jpg',1)"><img name="Image15" border="0" src="<%=Parametros.imgPath()%>btfinanciadores0.jpg" align="absbottom"></a> 
      </div>
    </td>
    <td width="95" height="42" valign="bottom" > 
      <div align="center"><a href="javascript:submitMenuEpisodios()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image16','','<%=Parametros.imgPath()%>btepisodio1.jpg',1)"><img name="Image16" border="0" src="<%=Parametros.imgPath()%>btepisodio0.jpg" width="86" height="25"></a></div>
    </td>
    <td width="96" height="42" valign="bottom"> 
      <div align="center"><a href="javascript:submitMenuEncuentros()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image17','','<%=Parametros.imgPath()%>btencuentros0.jpg',1)"><img name="Image17" border="0" src="<%=Parametros.imgPath()%>btencuentros1.jpg"></a></div>
    </td>
    <td width="121" height="42" valign="bottom">
      <div align="center"><a href="javascript:submitMenuActividades()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','<%=Parametros.imgPath()%>btactividades0.jpg',1)"><img name="Image18" border="0" src="<%=Parametros.imgPath()%>btactividades1.jpg" width="102" height="25"></a></div>
    </td>
  </tr>
</table>

