<%@ page import="java.util.*, com.ssi.model.salud.*, com.ssi.bean.salud.*, com.ssi.util.*" %>
<%@ page import="com.ssi.util.*" %>
<%@ page import="java.util.*, com.ssi.persistence.model.PersistenceManager, com.ssi.persistence.model.JDBC.JDBCAnswerResultSet" %> 
<%@ page import="java.sql.*" %>

<jsp:useBean id="busquedaFactoresRiesgoCliente" scope="session" class="com.ssi.bean.salud.BusquedaFactoresRiesgoCliente" />


<HTML>
<HEADER><TITLE>Factores de Riesgo</TITLE>
<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>

</HEADER>
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<BODY onBlur="self.focus()" onLoad="MM_preloadImages('<%=Parametros.imgPath()%>bt_cerrar1.jpg')" bgcolor="#f0f3e9" >
<br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td colspan="3" height="8"> 
      <table cellspacing=0 cellpadding=0 width=100% border=0>
        <tbody> 
        <tr> 
          <td width=14 background=<%=Parametros.imgPath()%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=11 
      src="<%=Parametros.imgPath()%>drwd_topleftcorner.gif" width=5></td>
          <td background=<%=Parametros.imgPath()%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=8 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=287></td>
          <td width=21 nowrap> 
            <div align="right"><img height=11 src="<%=Parametros.imgPath()%>drwd_toprightcorner.gif" 
      width=24></div>
          </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="3"> 
      <table cellspacing=0 cellpadding=0 width=100% border=0 height="1">
        <tbody> 
        <tr> 
          <td width=1 height="9"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlleftcorner.gif" 
      width=5></td>
          <td align=right width=58 bgcolor=#fcfdfa height="9"><img height=13 
      src="<%=Parametros.imgPath()%>drwd_iconnews.gif" width=13></td>
          <td bgcolor=#fcfdfa height="9"><img src="<%=Parametros.imgPath()%>titu_factoresriesgo.jpg"></td>
          <td width=18 height="9" nowrap> 
            <div align="right"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlrightcorner.gif" 
      width=24></div>
          </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  <tr> 
    <td width=29 background=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif 
      bgcolor=#f0f3e9>&nbsp;</td>
    <td bgcolor="#f0f3e9" width="879"> 
      <div align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td width="77%" bgcolor="#012652"> 
                    <div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Factor 
                      de Riesgo</font></div>
                  </td>
                  <td width="23%" bgcolor="#012652"> 
                    <div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Fecha 
                      de Detecci�n</font></div>
                  </td>
                </tr>
                <%
      JDBCAnswerResultSet rs=null;
      int lineas = 0;     
      try{
 	
 	rs = busquedaFactoresRiesgoCliente.ejecutarConsulta();
	java.sql.ResultSet jrs = rs.resultSet();
	java.sql.ResultSetMetaData meta = jrs.getMetaData();
	int cols = meta.getColumnCount();

	
	String strFechaDeteccion = null;
	java.util.Date date = null;
	java.text.DateFormat miFormato = null;
	
	while(jrs.next()) {
	  lineas++;
	  LogFile.log("lineas:" + lineas);
        date = jrs.getDate(2);
        miFormato = new java.text.SimpleDateFormat("dd/MM/yyyy");
        strFechaDeteccion = miFormato.format(date);
		
%>
                <tr> 
                  <td width="77%"> 
                    <div align="center"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><%= jrs.getObject(1) %></font></div>
                  </td>
                  <td width="23%"> 
                    <div align="center"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><%= strFechaDeteccion %></font></div>
                  </td>
                </tr>
                <%
    }	
 %>
                <% }
   catch(Exception e){
    LogFile.log("Excepcion en FactoresDeRiesgoCliente.jsp");
    LogFile.log(e);
   } 
   finally{
     rs.close();
   }
   if(lineas==0){ %>
                <tr> 
                  <td colspan="12" height="61"> <b> 
                    <div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" color="#660000">No 
                      se registraron Factores de Riesgo</font></div>
                    </b></td>
                </tr>
                <%}%>
              </table>
            </td>
          </tr>
        </table></div>
    </td>
    <td width=31 background=<%=Parametros.imgPath()%>btwd_mdlbgr2.gif> 
      <div align="right"><img src="<%=Parametros.imgPath()%>btwd_mdlbgr2.gif" width="30" height="8"></div>
    </td>
  </tr>
  <tr> 
    <td colspan="3" background=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif> 
      <table cellspacing=0 cellpadding=0 width=100% border=0>
        <tbody> 
        <tr> 
          <td width=5><img height=26 src="<%=Parametros.imgPath()%>btwd_btmleftcorner.gif" 
      width=5></td>
          <td width=1 background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif nowrap><img height=26 
      src="<%=Parametros.imgPath()%>btwd_btmmiddle.gif" width=26></td>
          <td background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif><img height=1 
      src="<%=Parametros.imgPath()%>spacer.gif" width=255></td>
          <td width=1 background=<%=Parametros.imgPath()%>btwd_btmbgr2.gif> 
            <div align="right"><img height=26 
      src="<%=Parametros.imgPath()%>btwd_btmmiddle2.gif" width=23></div>
          </td>
          <td width=1> 
            <div align="right"><img height=26 src="<%=Parametros.imgPath()%>btwd_btmrightcorner.gif" 
      width=7></div>
          </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
</table>
<div align="center"><br>
  <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image37','','<%=Parametros.imgPath()%>bt_cerrar1.jpg',1)" onClick="window.close();return false"><img name="Image37" border="0" src="<%=Parametros.imgPath()%>bt_cerrar.jpg" width="78" height="23"></a> 
</div>
</BODY></HTML>

