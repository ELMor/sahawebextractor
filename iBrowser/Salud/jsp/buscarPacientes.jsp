<%@ page import="java.util.*, com.ssi.model.salud.*" %>
<%@ page import="com.ssi.util.*" %>
<jsp:useBean id="busquedaPaciente" scope="session" class="com.ssi.bean.salud.BusquedaPaciente" />

<SCRIPT LANGUAGE="JavaScript" SRC="/Salud/jsp/js/busquedapaciente.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function submitForm(e)
{

var keycode;

keycode = window.event.keyCode;

if (window.event) keycode = window.event.keyCode;
else keycode = e.which;

if (keycode == 13)
   submitBuscar();

}
//-->
</SCRIPT>

<html>
<head>


<title>B&uacute;squeda de Pacientes - SOLUZIONA Salud</title>
<%
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</HEAD>
<body bgcolor="#e0e3d8" leftmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('<%=Parametros.imgPath()%>bt_salir1.jpg','<%=Parametros.imgPath()%>bt_contactenos1.jpg','<%=Parametros.imgPath()%>bt_contactenos1.jpg','<%=Parametros.imgPath()%>bt_ayuda1.jpg','<%=Parametros.imgPath()%>bt_salir1.jpg','<%=Parametros.imgPath()%>bt_buscar1.jpg')" topmargin="0">
<form name="buscarPacientes"  method="post" onKeyPress="javascript:submitForm(event)">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="121"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="54%" background="<%=Parametros.imgPath()%>fondotitu.jpg"><img src="<%=Parametros.imgPath()%>tituBrowsertop.jpg" width="340" height="62"></td>
                  <td width="46%" background="<%=Parametros.imgPath()%>fondotitu.jpg"> 
                    <div align="right"><img src="<%=Parametros.imgPath()%>fondotop.jpg" width="417" height="62"></div>
                  </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="88%" background="<%=Parametros.imgPath()%>fondobarratop.jpg">&nbsp;</td>
                  <td width="12%" background="<%=Parametros.imgPath()%>fondobarratop.jpg"> 
                    <div align="left"><a href="mailto:info@mail.com" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','<%=Parametros.imgPath()%>bt_contactenos1.jpg',1)"><img name="Image42" border="0" src="<%=Parametros.imgPath()%>bt_contactenos.jpg" width="90" height="28"></a><a href="/Salud/jsp/manualuso.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image52','','<%=Parametros.imgPath()%>bt_ayuda1.jpg',1)"><img name="Image52" border="0" src="<%=Parametros.imgPath()%>bt_ayuda.jpg" width="48" height="28"></a><a href="javascript:submitVolver()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image41','','<%=Parametros.imgPath()%>bt_salir1.jpg',1)"><img name="Image41" border="0" src="<%=Parametros.imgPath()%>bt_salir.jpg" width="42" height="28" alt="Salir de la Aplicaci&oacute;n"></a><img src="<%=Parametros.imgPath()%>fondobarratop.jpg" width="9" height="28"></div>
                  </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td background="<%=Parametros.imgPath()%>sombra_top.jpg"><img src="<%=Parametros.imgPath()%>sombra_top.jpg"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td valign="middle"> <br>
              <table width="44%" border="0" cellspacing="0" cellpadding="0" valign="top" align="center">
                <input type='hidden' name='view' value='busquedaPaciente'>
                <tr> 
                  <td> 
                    <table cellspacing=0 cellpadding=0 width=450 border=0>
                      <tbody> 
                      <tr> 
                        <td width=7 background=<%=Parametros.imgPath()%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=11 
      src="<%=Parametros.imgPath()%>drwd_topleftcorner.gif" width=5></td>
                        <td width=428 background=<%=Parametros.imgPath()%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=8 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=287></td>
                        <td width=15> 
                          <div align="right"><img height=11 src="<%=Parametros.imgPath()%>drwd_toprightcorner.gif" 
      width=24></div>
                        </td>
                      </tr>
                      </tbody> 
                    </table>
                    <table cellspacing=0 cellpadding=0 width=450 border=0 height="1">
                      <tbody> 
                      <tr> 
                        <td width=5 height="5"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlleftcorner.gif" 
      width=5></td>
                        <td align=right width=28 bgcolor=#fcfdfa height="5"><img height=13 
      src="<%=Parametros.imgPath()%>drwd_iconnews.gif" width=13></td>
                        <td width=406 bgcolor=#fcfdfa height="5"><img src="<%=Parametros.imgPath()%>titu_busqueda.jpg" width="159" height="14"></td>
                        <td width=11 height="5"> 
                          <div align="right"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlrightcorner.gif" 
      width=24></div>
                        </td>
                      </tr>
                      </tbody> 
                    </table>
                    <table cellspacing=0 cellpadding=0 width=450 border=0>
                      <tbody> 
                      <tr> 
                        <td width=1 bgcolor=#f0f3e9><img height=20 
      src="<%=Parametros.imgPath()%>btwd_topleftcorner.gif" width=31></td>
                        <td width=383 background=<%=Parametros.imgPath()%>btwd_topbgr1.gif 
      bgcolor=#f0f3e9><img height=1 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=255></td>
                        <td width=24> 
                          <div align="right"><img height=20 src="<%=Parametros.imgPath()%>btwd_toprightcorner.gif" 
      width=30></div>
                        </td>
                      </tr>
                      </tbody> 
                    </table>
                    <table cellspacing=0 cellpadding=0 width=450 border=0>
                      <tbody> 
                      <tr> 
                        <td width=30 background=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif 
      bgcolor=#f0f3e9><img height=113 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=1></td>
                        <td width=396 bgcolor=#f0f3e9> 
                          <table width="96%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="33%"><img src="<%=Parametros.imgPath()%>label_apellpaterno.jpg" width="107" height="13"></td>
                              <td width="47%"> 
                                <input type="text" name="tfApellido1" value="<%= busquedaPaciente.getApellido1()%>" size="25">
                              </td>
                              <td width="20%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="33%"><img src="<%=Parametros.imgPath()%>label_apellmaterno.jpg" width="107" height="13"></td>
                              <td width="47%"> 
                                <input type="text" name="tfApellido2" value="<%= busquedaPaciente.getApellido2()%>" size="25">
                              </td>
                              <td width="20%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="33%"><img src="<%=Parametros.imgPath()%>label_nombre.jpg" width="107" height="13"></td>
                              <td width="47%"> 
                                <input type="text" name="tfNombre" value="<%= busquedaPaciente.getNombrePaciente()%>" size="25">
                              </td>
                              <td width="20%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="33%"><img src="<%=Parametros.imgPath()%>label_rce.jpg" width="107" height="13"></td>
                              <td width="47%"> 
                                <input type="text" name="tfRFC" value="<%= busquedaPaciente.getRFC()%>" size="25">
                              </td>
                              <td width="20%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="33%"><img src="<%=Parametros.imgPath()%>label_fechanacimiento.jpg" width="127" height="13"></td>
                              <td width="47%"> 
                                <input type="text" name="tfFechaNac" value="<%= busquedaPaciente.getFechaNac()%>" maxlength="10" size="25">
                              </td>
                              <td width="20%"><img src="<%=Parametros.imgPath()%>label_ddmmaaaa.jpg" width="62" height="10"></td>
                            </tr>
                            <tr> 
                              <td width="33%">&nbsp;</td>
                              <td width="47%"> 
                                <div align="center"><a href="javascript:submitBuscar()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image40','','<%=Parametros.imgPath()%>bt_buscar1.jpg',1)"><img name="Image40" border="0" src="<%=Parametros.imgPath()%>bt_buscar.jpg" alt="Buscar Paciente"></a></div>
                              </td>
                              <td width="20%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width=24 background=<%=Parametros.imgPath()%>btwd_mdlbgr2.gif><img height=113 
      src="<%=Parametros.imgPath()%>spacer.gif" width=1><img src="<%=Parametros.imgPath()%>btwd_mdlbgr2.gif" width="30" height="2"></td>
                      </tr>
                      </tbody> 
                    </table>
                    <table cellspacing=0 cellpadding=0 width=450 border=0>
                      <tbody> 
                      <tr> 
                        <td width=5><img height=26 src="<%=Parametros.imgPath()%>btwd_btmleftcorner.gif" 
      width=5></td>
                        <td width=33 background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif><img height=26 
      src="<%=Parametros.imgPath()%>btwd_btmmiddle.gif" width=26></td>
                        <td width=376 background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif><img height=1 
      src="<%=Parametros.imgPath()%>spacer.gif" width=255></td>
                        <td width=36 background=<%=Parametros.imgPath()%>btwd_btmbgr2.gif> 
                          <div align="right"><img height=26 
      src="<%=Parametros.imgPath()%>btwd_btmmiddle2.gif" width=23></div>
                        </td>
                        <td width=1> 
                          <div align="right"><img height=26 src="<%=Parametros.imgPath()%>btwd_btmrightcorner.gif" 
      width=7></div>
                        </td>
                      </tr>
                      </tbody> 
                    </table>
                  </td>
                </tr>
              </table>
              <a> 
              <input type='hidden' name='btbuscarlupa' value='inicial'>
              </a></td>
          </tr>
          <tr> 
            <td> </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td> <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td background="<%=Parametros.imgPath()%>fondo_pie.jpg" height="1" nowrap valign="top"><img src="<%=Parametros.imgPath()%>pie.jpg"></td>
          </tr>
        </table></td>
    </tr>
  </table></form></body></html>