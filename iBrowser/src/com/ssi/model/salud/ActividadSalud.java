// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   ActividadSalud.java

package com.ssi.model.salud;

import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.ReadWritePersistentObject;
import com.ssi.util.LogFile;
import com.ssi.util.Parametros;
import java.sql.Date;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            SistemaSiapwin, Paciente, SistemaSalud

public abstract class ActividadSalud extends ReadWritePersistentObject
{

    private Paciente unPaciente;
    public static Vector listaActividades;
    static Hashtable listaTipoActividad = null;
    String tipoActividad;

    public void paciente(Paciente paciente)
    {
        unPaciente = paciente;
    }

    public Paciente paciente()
    {
        return unPaciente;
    }

    public String nombreSistema()
    {
        return sistema().nombreSistema();
    }

    public abstract SistemaSalud sistema();

    public ActividadSalud()
    {
        unPaciente = new Paciente();
    }

    public String descripcion()
    {
        return ve_ac_descri();
    }

    public String fechayHoraInicio()
    {
        String campoFecha = null;
        if(ve_ac_fecha() != null)
        {
            Date date = Date.valueOf(ve_ac_fecha().substring(0,9));
            DateFormat myformat = ((DateFormat) (new SimpleDateFormat("dd/MM/yyyy")));
            campoFecha = myformat.format(((java.util.Date) (date))) + " " + ve_ac_hora();
        } else
        {
            campoFecha = Parametros.settings.getProperty("sinDatos","s/d");
        }
        return campoFecha;
    }

    public abstract String getTipoActividad();

    public static String getTipoActividad(String idActividad)
    {
        if(listaTipoActividad == null)
        {
            listaTipoActividad = new Hashtable();
            JDBCAnswerResultSet rs2 = null;
            try
            {
                PersistenceManager manager = ((SistemaSalud) (SistemaSiapwin.getInstance())).getPersistenceManager();
                String query = "SELECT tipo_actividad_pk,tipo_act_desc FROM tipo_actividad";
                rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
                setearTiposDeActividades(rs2.resultSet());
                LogFile.log("Finaliz\363 de setear los tipos de actividades");
            }
            catch(Exception e)
            {
                LogFile.log(e);
            }
            finally
            {
                try
                {
                    rs2.close();
                }
                catch(Exception _ex)
                {
                    LogFile.log("ActividadSalud no puede cerrar el ResultSet");
                }
            }
        }
        if(listaTipoActividad.containsKey(((Object) (idActividad))))
            return (String)listaTipoActividad.get(((Object) (idActividad)));
        else
            return Parametros.settings.getProperty("sinDatos","s/d");
    }

    public abstract String oid();

    public static void setearTiposDeActividades(ResultSet rs)
    {
        try
        {
            String clave = rs.getString("tipo_actividad_pk");
            String descripcion = rs.getString("tipo_act_desc");
            listaTipoActividad.put(((Object) (clave)), ((Object) (descripcion)));
            LogFile.log("Leyendo tipo de actividad: " + clave + ", " + descripcion);
            for(; rs.next(); LogFile.log("Leyendo tipo de actividad: " + clave + ", " + descripcion))
            {
                clave = rs.getString("tipo_actividad_pk");
                descripcion = rs.getString("tipo_act_desc");
                listaTipoActividad.put(((Object) (clave)), ((Object) (descripcion)));
            }

        }
        catch(Exception e)
        {
            LogFile.log("Excepcion en ActividadSalud::setearTiposDeActividades()");
            LogFile.log(e);
        }
    }

    public abstract void setTipoActividad(String s);

    public abstract String ve_ac_coste();

    public abstract String ve_ac_descri();

    public abstract String ve_ac_estado();

    public abstract String ve_ac_fecha();

    public abstract String ve_ac_hora();

    public abstract String ve_ac_ref();

    public abstract String ve_ac_servicio();

    public abstract String ve_ac_tipo();

    public abstract String ve_en_ref();

}
