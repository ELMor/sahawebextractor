// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Paciente.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import java.sql.SQLException;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            PacienteSiapwin, PacienteNovahis

public class Paciente
{

    private PacienteNovahis pacienteNovahis;
    private PacienteSiapwin pacienteSiapwin;

    public String sexoDescripcion()
    {
        return "";
    }

    public Paciente()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return null;
    }

    public static Vector getPacientes(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        LogFile.log("Paciente: getPacientes: Inicio: ");
        Vector vPacientesUnificados = new Vector();
        Vector vPacientesNovahis = new Vector();
        Vector vPacientesSiapwin = new Vector();
        vPacientesSiapwin = getPacientesSiapwin(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        LogFile.log("Paciente: getPacientes: largo de Vector Siapwin: " + vPacientesSiapwin.size());
        vPacientesNovahis = getPacientesNovahis(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        LogFile.log("Paciente: getPacientes: largo de Vector Novahis: " + vPacientesNovahis.size());
        vPacientesUnificados = unificarPacientes(vPacientesSiapwin, vPacientesNovahis);
        LogFile.log("Paciente: getPacientes: Fin: ");
        return vPacientesUnificados;
    }

    public static Vector getPacientesSiapwin(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        Vector vPacientesSiapwin = new Vector();
        vPacientesSiapwin = PacienteSiapwin.getPacientes(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        if(!vPacientesSiapwin.isEmpty())
        {
            return vPacientesSiapwin;
        } else
        {
            LogFile.log("Paciente: getPacientesSiapwin: El vector volvio nulo.");
            vPacientesSiapwin = new Vector();
            return vPacientesSiapwin;
        }
    }

    public static Vector getPacientesNovahis(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        Vector vPacientesNovahis = new Vector();
        vPacientesNovahis = PacienteNovahis.getPacientes(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        if(!vPacientesNovahis.isEmpty())
        {
            return vPacientesNovahis;
        } else
        {
            LogFile.log("Paciente: getPacientesNovahis: El vector volvio nulo.");
            vPacientesNovahis = new Vector();
            return vPacientesNovahis;
        }
    }

    public static Vector unificarPacientes(Vector pacientesSiapwin, Vector pacientesNovahis)
    {
        Vector pacientes = new Vector();
        int medidaVectorSiapwin = pacientesSiapwin.size();
        if(medidaVectorSiapwin > 0)
        {
            for(int i = 0; i < medidaVectorSiapwin; i++)
            {
                PacienteSiapwin unPaciente = (PacienteSiapwin)pacientesSiapwin.elementAt(i);
                pacientes.addElement(((Object) (unPaciente)));
            }

        }
        int medidaVectorNovahis = pacientesNovahis.size();
        if(medidaVectorNovahis > 0)
        {
            for(int i = 0; i < medidaVectorNovahis; i++)
            {
                PacienteNovahis unPacienteSiap = (PacienteNovahis)pacientesNovahis.elementAt(i);
                pacientes.addElement(((Object) (unPacienteSiap)));
            }

        }
        return pacientes;
    }

    public PacienteSiapwin pacienteSiapwin()
    {
        return pacienteSiapwin;
    }

    public PacienteNovahis pacienteNovahis()
    {
        return pacienteNovahis;
    }
}
