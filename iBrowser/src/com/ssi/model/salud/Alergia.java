// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Alergia.java

package com.ssi.model.salud;

import java.util.Date;

public class Alergia
{

    String idPaciente;
    String sistema;
    String descripcion;
    Date fechaDeDeteccion;

    public Alergia(String desc, Date fechaDesde)
    {
        setDescripcion(desc);
        setFechaDeDeteccion(fechaDesde);
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public Date getFechaDeDeteccion()
    {
        return fechaDeDeteccion;
    }

    public String getIdPaciente()
    {
        return idPaciente;
    }

    public String getSistema()
    {
        return sistema;
    }

    public void setDescripcion(String newDescripcion)
    {
        descripcion = newDescripcion;
    }

    public void setFechaDeDeteccion(Date newFechaDeDeteccion)
    {
        fechaDeDeteccion = newFechaDeDeteccion;
    }

    public void setIdPaciente(String newIdPaciente)
    {
        idPaciente = newIdPaciente;
    }

    public void setSistema(String newSistema)
    {
        sistema = newSistema;
    }
}
