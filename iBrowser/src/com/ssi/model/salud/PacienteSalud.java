// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   PacienteSalud.java

package com.ssi.model.salud;

import com.ssi.persistence.model.ReadWritePersistentObject;
import com.ssi.util.Parametros;
import java.util.Calendar;
import java.util.GregorianCalendar;

public abstract class PacienteSalud extends ReadWritePersistentObject
{

    public abstract String sexoDescripcion();

    public static String darAnd()
    {
        return " AND ";
    }

    public abstract String nombre();

    public abstract String apellido1();

    public abstract String apellido2();

    public abstract GregorianCalendar fechaNacimiento();

    public abstract String RFC();

    public abstract String codigoCliente();

    public abstract String telefono1();

    public abstract String nombreSistema();

    String formatFecha(Calendar fecha)
    {
        int aa = fecha.get(1);
        int mm = fecha.get(2) + 1;
        int dd = fecha.get(5);
        String ff = dd + "/" + mm + "/" + aa;
        return ff;
    }

    public String fechaNacimientoString(Calendar nac_fecha)
    {
        if(nac_fecha == null)
        {
            String nac_fechaString = Parametros.settings.getProperty("sinDatos","s/d");
            return nac_fechaString;
        } else
        {
            String nac_fechaString = formatFecha(nac_fecha);
            return nac_fechaString;
        }
    }

    public PacienteSalud()
    {
    }
}
