// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ActividadSiapwin.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            ActividadSalud, ActividadSiapwinAccessor, SistemaSiapwin, PacienteSalud, 
//            SistemaSalud

public class ActividadSiapwin extends ActividadSalud
{

    private String ve_ac_ref;
    private String ve_en_ref;
    private String ve_ac_fecha;
    private String ve_ac_hora;
    private String ve_ac_servicio;
    private String ve_ac_descri;
    private String ve_ac_estado;
    private String ve_ac_tipo;
    private String ve_ac_coste;

    public ActividadSiapwin()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public SistemaSalud sistema()
    {
        return ((SistemaSalud) (SistemaSiapwin.getInstance()));
    }

    public static String construirRestriccion(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
    {
        String unaRestriccion = null;
        if(rfc != null && rfc.length() > 0)
        {
            unaRestriccion = " codigo1 = '" + rfc + "'";
            LogFile.log("ActividadSiapwin: construirRestriccion: WHERE 1: " + unaRestriccion);
        }
        if(nombre != null && nombre.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " nombre = '" + nombre + "'";
                LogFile.log("ActividadSiapwin: construirRestriccion: WHERE 2.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nombre = '" + nombre + "'";
                LogFile.log("ActividadSiapwin: construirRestriccion: WHERE 2.2: " + unaRestriccion);
            }
        if(unaRestriccion != null)
        {
            unaRestriccion = " where " + unaRestriccion;
        } else
        {
            LogFile.log("ActividadSiapwin: construirRestriccion: unaRestriccion es null.");
            unaRestriccion = "";
        }
        LogFile.log("ActividadSiapwin: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static Vector getActividad(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        String unaRestriccion = construirRestriccion(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        LogFile.log("ActividadSiapwin: getActividad: WHERE-> " + unaRestriccion);
        Vector vActividad = instancesWithWhere(unaRestriccion);
        LogFile.log("ActividadSiapwin: getActividad: size: " + vActividad.size());
        if(vActividad == null)
        {
            LogFile.log("ActividadSiapwin: getActividad: Vector NULO ");
            vActividad = new Vector();
        }
        return vActividad;
    }

    public static Vector instancesWithWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector vActividad = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.ActividadSiapwin"), unaRestriccion);
        if(vActividad == null)
        {
            LogFile.log("ActividadSiapwin: instancesWithWhere: No hay actividades.");
            vActividad = new Vector();
        }
        return vActividad;
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return ActividadSiapwinAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            Encuentro answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.ActividadSiapwin"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.ActividadSiapwin"));
    }

    public String ve_ac_ref()
    {
        return ve_ac_ref;
    }

    public void ve_ac_ref(String unVe_ac_ref)
    {
        ve_ac_ref = unVe_ac_ref;
    }

    public String ve_en_ref()
    {
        return ve_en_ref;
    }

    public void ve_en_ref(String unVe_en_ref)
    {
        ve_en_ref = unVe_en_ref;
    }

    public String ve_ac_fecha()
    {
        return ve_ac_fecha;
    }

    public void ve_ac_fecha(String unVe_ac_fecha)
    {
        ve_ac_fecha = unVe_ac_fecha;
    }

    public String ve_ac_hora()
    {
        return ve_ac_hora;
    }

    public void ve_ac_hora(String unVe_ac_hora)
    {
        ve_ac_hora = unVe_ac_hora;
    }

    public String ve_ac_servicio()
    {
        return ve_ac_servicio;
    }

    public void ve_ac_servicio(String unVe_ac_servicio)
    {
        ve_ac_servicio = unVe_ac_servicio;
    }

    public String ve_ac_descri()
    {
        return ve_ac_descri;
    }

    public void ve_ac_descri(String unVe_ac_descri)
    {
        ve_ac_descri = unVe_ac_descri;
    }

    public String ve_ac_estado()
    {
        return ve_ac_estado;
    }

    public void ve_ac_estado(String unVe_ac_estado)
    {
        ve_ac_estado = unVe_ac_estado;
    }

    public String ve_ac_tipo()
    {
        return ve_ac_tipo;
    }

    public void ve_ac_tipo(String unVe_ac_tipo)
    {
        ve_ac_tipo = unVe_ac_tipo;
    }

    public String ve_ac_coste()
    {
        return ve_ac_coste;
    }

    public void ve_ac_coste(String unVe_ac_coste)
    {
        ve_ac_coste = unVe_ac_coste;
    }

    public String oid()
    {
        return ve_ac_ref;
    }

    public void oid(String unCodigo)
    {
        ve_ac_ref = unCodigo;
    }

    public void delete()
        throws NoSuchMethodException, SQLException, ClassNotFoundException
    {
    }

    public String getTipoActividad()
    {
        return ActividadSalud.getTipoActividad(ve_ac_tipo());
    }

    public boolean isPersistent()
    {
        return false;
    }

    public void save()
        throws InstantiationException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException
    {
    }

    public void setTipoActividad(String s)
    {
    }
}
