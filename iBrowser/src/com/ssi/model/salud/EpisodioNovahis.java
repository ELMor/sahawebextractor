// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   EpisodioNovahis.java

package com.ssi.model.salud;

import com.ssi.persistence.model.DBConnection;
import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import com.ssi.util.Parametros;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            EpisodioSalud, PacienteSalud, SistemaNovahis, SistemaSalud,
//            EpisodioNovahisAccessor

public class EpisodioNovahis extends EpisodioSalud
{

    private String ve_ep_ref;
    private String ve_ep_fecha;
    private String ve_ep_hora;
    private String ve_ep_servicio;
    private String ve_ep_cond;
    private String ve_ep_estado;
    private String ve_ep_idclient;
    private String ve_sev_pk;
    private String ve_fus_pk;
    private String ve_cond_pk;
    private String ve_ep_fechaend;
    private String ve_ep_resolucion;
    private String ve_enc_apert;
    private String ve_ep_icd_cod;
    private String ve_ep_fecha_clin;
    private String ve_ep_hora_clin;
    private String ve_ep_etiq;

    public EpisodioNovahis()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public SistemaSalud sistema()
    {
        return ((SistemaSalud) (SistemaNovahis.getInstance()));
    }

    public static String construirRestriccion(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
    {
        String unaRestriccion = null;
        if(rfc != null && rfc.length() > 0)
        {
            unaRestriccion = " codigo1 = '" + rfc + "'";
            LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 1: " + unaRestriccion);
        }
        if(nombre != null && nombre.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " nombre = '" + nombre + "'";
                LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 2.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nombre = '" + nombre + "'";
                LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 2.2: " + unaRestriccion);
            }
        if(apellido1 != null && apellido1.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " apellido1 = '" + apellido1 + "' ";
                LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 3.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido1 = '" + apellido1 + "'";
                LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 3.2: " + unaRestriccion);
            }
        if(apellido2 != null && apellido2.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " apellido2 = '" + apellido2 + "' ";
                LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 4.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido2 = '" + apellido2 + "' ";
                LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 4.2: " + unaRestriccion);
            }
        if(fechaNacimiento != null && fechaNacimiento.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " nac_fecha = '" + fechaNacimiento + "'";
                LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 5.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nac_fecha = '" + fechaNacimiento + "' ";
                LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 5.2: " + unaRestriccion);
            }
        if(unaRestriccion != null)
        {
            unaRestriccion = " where " + unaRestriccion;
        } else
        {
            LogFile.log("EpisodioNovahis: construirRestriccion: unaRestriccion es null.");
            unaRestriccion = "";
        }
        LogFile.log("EpisodioNovahis: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static Vector getEpisodios(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        String unaRestriccion = construirRestriccion(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        LogFile.log("Episodio: getEpisodios: WHERE-> " + unaRestriccion);
        Vector vEpisodios = instancesWithWhere(unaRestriccion);
        LogFile.log("Episodio: getEpisodios: size: " + vEpisodios.size());
        if(vEpisodios == null)
        {
            LogFile.log("EpisodioNovahis: getEpisodios: Vector NULO ");
            vEpisodios = new Vector();
        }
        return vEpisodios;
    }

    public static Vector instancesWithWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector vEpisodio = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.EpisodioNovahis"), unaRestriccion);
        if(vEpisodio == null)
        {
            LogFile.log("Episodio: instancesWithWhere: No hay Episodios.");
            vEpisodio = new Vector();
        }
        return vEpisodio;
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        LogFile.log("EpisodioNovahis::getPersistenceManager");
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return EpisodioNovahisAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            Episodio answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.EpisodioNovahis"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.EpisodioNovahis"));
    }

    public String ve_ep_ref()
    {
        return ve_ep_ref;
    }

    public void ve_ep_ref(String unVe_ep_ref)
    {
        ve_ep_ref = unVe_ep_ref;
    }

    public String centroDescripcion()
    {
        return "";
    }

    public String ve_ep_fecha()
    {
        return ve_ep_fecha;
    }

    public void ve_ep_fecha(String unVe_ep_fecha)
    {
        ve_ep_fecha = unVe_ep_fecha;
    }

    public String ve_ep_hora()
    {
        return ve_ep_hora;
    }

    public void ve_ep_hora(String unVe_ep_hora)
    {
        ve_ep_hora = unVe_ep_hora;
    }

    public String ve_ep_servicio()
    {
        return ve_ep_servicio;
    }

    public void ve_ep_servicio(String unVe_ep_servicio)
    {
        ve_ep_servicio = unVe_ep_servicio;
    }

    public String ve_ep_cond()
    {
        return ve_ep_cond;
    }

    public void ve_ep_cond(String unVe_ep_cond)
    {
        ve_ep_cond = unVe_ep_cond;
    }

    public String ve_ep_estado()
    {
        return ve_ep_estado;
    }

    public void ve_ep_estado(String unVe_ep_estado)
    {
        ve_ep_estado = unVe_ep_estado;
    }

    public String ve_ep_idclient()
    {
        return ve_ep_idclient;
    }

    public void ve_ep_idclient(String unVe_ep_idclient)
    {
        ve_ep_idclient = unVe_ep_idclient;
    }

    public String ve_sev_pk()
    {
        return ve_sev_pk;
    }

    public void ve_sev_pk(String unVe_sev_pk)
    {
        ve_sev_pk = unVe_sev_pk;
    }

    public String ve_fus_pk()
    {
        return ve_fus_pk;
    }

    public void ve_fus_pk(String unVe_fus_pk)
    {
        ve_fus_pk = unVe_fus_pk;
    }

    public String ve_cond_pk()
    {
        return ve_cond_pk;
    }

    public void ve_cond_pk(String unVe_cond_pk)
    {
        ve_cond_pk = unVe_cond_pk;
    }

    public String ve_ep_fechaend()
    {
        return ve_ep_fechaend;
    }

    public void ve_ep_fechaend(String unVe_ep_fechaend)
    {
        ve_ep_fechaend = unVe_ep_fechaend;
    }

    public String ve_ep_resolucion()
    {
        return ve_ep_resolucion;
    }

    public void ve_ep_resolucion(String unVe_ep_resolucion)
    {
        ve_ep_resolucion = unVe_ep_resolucion;
    }

    public String ve_enc_apert()
    {
        return ve_enc_apert;
    }

    public void ve_enc_apert(String unVe_enc_apert)
    {
        ve_enc_apert = unVe_enc_apert;
    }

    public String ve_ep_icd_cod()
    {
        return ve_ep_icd_cod;
    }

    public void ve_ep_icd_cod(String unVe_ep_icd_cod)
    {
        ve_ep_icd_cod = unVe_ep_icd_cod;
    }

    public String ve_ep_fecha_clin()
    {
        return ve_ep_fecha_clin;
    }

    public void ve_ep_fecha_clin(String unVe_ep_fecha_clin)
    {
        ve_ep_fecha_clin = unVe_ep_fecha_clin;
    }

    public String ve_ep_hora_clin()
    {
        return ve_ep_hora_clin;
    }

    public void ve_ep_hora_clin(String unVe_ep_hora_clin)
    {
        ve_ep_hora_clin = unVe_ep_hora_clin;
    }

    public String ve_ep_etiq()
    {
        return ve_ep_etiq;
    }

    public void ve_ep_etiq(String unVe_ep_etiq)
    {
        ve_ep_etiq = unVe_ep_etiq;
    }

    public String oid()
    {
        return ve_ep_ref;
    }

    public void oid(String unCodigo)
    {
        ve_ep_ref = unCodigo;
    }

    public void delete()
        throws NoSuchMethodException, SQLException, ClassNotFoundException
    {
    }

    public String getDiagnostico()
    {
        if(super.diagnostico == null)
        {
            Statement stm = null;
            ResultSet rs = null;
            try
            {
                PersistenceManager manager = ((SistemaSalud) (SistemaNovahis.getInstance())).getPersistenceManager();
                String query = "select icd_nom\tfrom icd where icd_cod='" + ve_ep_icd_cod() + "'";
                DBConnection conexion = manager.getConnection();
                String desc = null;
                stm = ((Connection)conexion.connection()).createStatement();
                for(rs = stm.executeQuery(query); rs.next();)
                    super.diagnostico = rs.getString("icd_nom");

                rs.close();
                stm.close();
                manager.returnConnection(conexion);
            }
            catch(Exception e)
            {
                LogFile.log(e);
            }
            finally
            {
                try
                {
                    rs.close();
                    stm.close();
                }
                catch(Exception _ex)
                {
                    LogFile.log("EpisodioNovahis no puede cerrar el ResultSet");
                }
            }
        }
        if(super.diagnostico == null)
            return Parametros.settings.getProperty("sinDatos","s/d");
        else
            return super.diagnostico;
    }

    public String getServicio()
    {
        if(super.servicio == null)
        {
            Statement stm = null;
            ResultSet rs = null;
            try
            {
                PersistenceManager manager = ((SistemaSalud) (SistemaNovahis.getInstance())).getPersistenceManager();
                String query = "select servicio\tfrom servicios where codigo_servicio='" + ve_ep_servicio() + "'";
                DBConnection conexion = manager.getConnection();
                String desc = null;
                stm = ((Connection)conexion.connection()).createStatement();
                for(rs = stm.executeQuery(query); rs.next();)
                    super.servicio = rs.getString("servicio");

                rs.close();
                stm.close();
                manager.returnConnection(conexion);
            }
            catch(Exception e)
            {
                LogFile.log(e);
            }
            finally
            {
                try
                {
                    rs.close();
                    stm.close();
                }
                catch(Exception _ex)
                {
                    LogFile.log("EpisodioNovahis no puede cerrar el ResultSet");
                }
            }
        }
        if(super.servicio == null)
            return Parametros.settings.getProperty("sinDatos","s/d");
        else
            return super.servicio;
    }

    public boolean isPersistent()
    {
        return false;
    }

    public void save()
        throws InstantiationException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException
    {
    }
}
