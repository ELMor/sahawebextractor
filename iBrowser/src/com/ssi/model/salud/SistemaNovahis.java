// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SistemaNovahis.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceConfiguration;
import com.ssi.persistence.model.salud.SistemaNovahisConfiguration;

// Referenced classes of package com.ssi.model.salud:
//            SistemaSalud

public class SistemaNovahis extends SistemaSalud
{

    private static SistemaNovahis instance;

    private SistemaNovahis()
    {
    }

    public static SistemaNovahis getInstance()
    {
        if(instance == null)
            instance = new SistemaNovahis();
        return instance;
    }

    public String nombreSistema()
    {
        return new String("SistemaNovahis");
    }

    public PersistenceConfiguration getConfiguration()
    {
        return ((PersistenceConfiguration) (new SistemaNovahisConfiguration()));
    }
}
