// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PacienteSiapwinAccessor.java

package com.ssi.model.salud;

import com.ssi.persistence.cache.IPersistenceCache;
import com.ssi.persistence.model.AccessorCache;
import com.ssi.persistence.model.DBDateFieldType;
import com.ssi.persistence.model.DBFixReferenceFieldType;
import com.ssi.persistence.model.DBStringFieldType;
import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.PersistentField;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            SexoSiapwinAccessor

public class PacienteSiapwinAccessor extends PersistenceAccessor
{

    public static String className()
    {
        return "com.ssi.model.salud.PacienteSiapwinAccessor";
    }

    public static PersistenceAccessor getInstance()
    {
        return AccessorCache.getInstance().get(((Object) (className())));
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new PacienteSiapwinAccessor(aManager)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new PacienteSiapwinAccessor(aManager, aCache)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    protected PacienteSiapwinAccessor(PersistenceManager aManager)
    {
        super(aManager);
    }

    protected PacienteSiapwinAccessor(PersistenceManager aManager, IPersistenceCache aCache)
    {
        super(aManager, aCache);
    }

    public Vector addPersistentFields(Vector aAnswer)
    {
        aAnswer.addElement(((Object) (new PersistentField("codigo_cliente", "oid", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("apellido1", "apellido1", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("apellido2", "apellido2", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("nombre", "nombre", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("nac_fecha", "fechaNacimiento", 1, ((com.ssi.persistence.model.DBFieldType) (new DBDateFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("codigo1", "RFC", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("telefono1", "telefono1", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("codigo_sexo", "codigo_sexo", 1, ((com.ssi.persistence.model.DBFieldType) (new DBFixReferenceFieldType(SexoSiapwinAccessor.getInstance())))))));
        return aAnswer;
    }

    public Vector collectionSpecs()
    {
        return new Vector();
    }

    public String oidPrefix()
    {
        return "";
    }

    public String persistentClassName()
    {
        return "com.ssi.model.salud.PacienteSiapwin";
    }

    public String tableName()
    {
        return "clientes";
    }
}
