// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Encuentro.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            EncuentroSalud, EncuentroAccessor, SistemaNovahis, EpisodioSalud, 
//            SistemaSalud, PacienteNovahis, EncuentroNovahis, EncuentroSiapwin

public class Encuentro extends EncuentroSalud
{

    private String ve_en_ref;
    private String ve_ep_ref;
    private String ve_en_fecha;
    private String ve_en_hora;
    private String ve_en_servicio;
    private String ve_en_motivo;
    private String ve_en_estado;
    private String ve_en_tipo;
    private String ve_en_idclient;
    private String ve_en_icd_cod;

    public Encuentro()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public SistemaSalud sistema()
    {
        return ((SistemaSalud) (SistemaNovahis.getInstance()));
    }

    public static String construirRestriccion(String idEncuentro)
    {
        String unaRestriccion = null;
        if(idEncuentro != null && idEncuentro.length() > 0)
        {
            unaRestriccion = " WHERE ve_ep_ref = '" + idEncuentro + "'";
            LogFile.log("Encuentro: construirRestriccion: WHERE 1: " + unaRestriccion);
        }
        LogFile.log("Encuentro: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static Vector instancesWithWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector vEncuentro = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.Encuentro"), unaRestriccion);
        if(vEncuentro == null)
        {
            LogFile.log("Encuentro: instancesWithWhere: No hay Encuentro.");
            vEncuentro = new Vector();
        }
        return vEncuentro;
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return EncuentroAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            Encuentro answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.Encuentro"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.Encuentro"));
    }

    public String idEncuentro()
    {
        return ve_en_ref;
    }

    public void idEncuentro(String unIdEncuentro)
    {
        ve_en_ref = unIdEncuentro;
    }

    public String fechayHoraInicio()
    {
        String fechayHora = null;
        fechayHora = fechaInicio();
        fechayHora = fechayHora + " " + horaInicio();
        return fechayHora;
    }

    public void fechaInicio(String unafechaInicio)
    {
        ve_en_fecha = unafechaInicio;
    }

    public String fechaInicio()
    {
        return ve_en_fecha;
    }

    public String horaInicio()
    {
        return ve_en_hora;
    }

    public void horaInicio(String unahoraInicio)
    {
        ve_en_hora = unahoraInicio;
    }

    public String ve_ep_ref()
    {
        return ve_ep_ref;
    }

    public void ve_ep_ref(String unVe_ep_ref)
    {
        ve_ep_ref = unVe_ep_ref;
    }

    public String servicio()
    {
        String respuesta = ve_en_servicio();
        if(respuesta == null)
        {
            respuesta = new String();
            respuesta = "Sin Dato";
        }
        return respuesta;
    }

    public String ve_en_servicio()
    {
        return ve_en_servicio;
    }

    public void ve_en_servicio(String unVe_en_servicio)
    {
        ve_en_servicio = unVe_en_servicio;
    }

    public String motivo()
    {
        String motivo = ve_en_motivo();
        if(motivo == null)
        {
            motivo = new String();
            motivo = "Sin Motivo";
        }
        return motivo;
    }

    public String ve_en_motivo()
    {
        return ve_en_motivo;
    }

    public void ve_en_motivo(String unVe_en_motivo)
    {
        ve_en_motivo = unVe_en_motivo;
    }

    public String estado()
    {
        String respuesta = ve_en_estado();
        if(respuesta == null)
        {
            respuesta = new String();
            respuesta = "Sin Dato";
        }
        return respuesta;
    }

    public String ve_en_estado()
    {
        return ve_en_estado;
    }

    public void ve_en_estado(String unVe_en_estado)
    {
        ve_en_estado = unVe_en_estado;
    }

    public String ve_en_idclient()
    {
        return ve_en_idclient;
    }

    public void ve_en_idclient(String unVe_en_idclient)
    {
        ve_en_idclient = unVe_en_idclient;
    }

    public String tipo()
    {
        String tipo = ve_en_tipo();
        if(tipo == null)
        {
            tipo = new String();
            tipo = "No se especific\363 Tipo";
        }
        return tipo;
    }

    public String ve_en_tipo()
    {
        return ve_en_tipo;
    }

    public void ve_en_tipo(String unVe_en_tipo)
    {
        ve_en_tipo = unVe_en_tipo;
    }

    public String codigoDiagnostico()
    {
        String codigo = ve_en_icd_cod();
        if(codigo == null)
        {
            codigo = new String();
            codigo = "Sin Codigo";
        }
        return codigo;
    }

    public String ve_en_icd_cod()
    {
        return ve_en_icd_cod;
    }

    public void ve_en_icd_cod(String unVe_en_icd_cod)
    {
        ve_en_icd_cod = unVe_en_icd_cod;
    }

    public String oid()
    {
        return ve_en_ref;
    }

    public void oid(String unCodigo)
    {
        ve_en_ref = unCodigo;
    }

    public void delete()
        throws NoSuchMethodException, SQLException, ClassNotFoundException
    {
    }

    public String getDiagnostico()
    {
        return null;
    }

    public String getServicio()
    {
        return null;
    }

    public static Vector getEncuentros(String idEpisodio, String sistema)
        throws SQLException
    {
        Vector vEncuentrosSiapwin = new Vector();
        Vector vEncuentrosNovahis = new Vector();
        try
        {
            if(sistema.equals("SistemaNovahis"))
            {
                LogFile.log("Encuentro: getEncuentro: es Novahis");
                String unaRestriccion = " WHERE ve_ep_ref = '" + idEpisodio + "'";
                vEncuentrosNovahis = EncuentroNovahis.instancesWithWhere(unaRestriccion);
            } else
            {
                LogFile.log("Encuentro: getEncuentro: es Siapwin");
                String unaRestriccion = " WHERE ve_ep_ref='" + idEpisodio + "'";
                vEncuentrosSiapwin = EncuentroSiapwin.instancesWithWhere(unaRestriccion);
            }
        }
        catch(Exception e)
        {
            LogFile.log(e);
        }
        LogFile.log("Encuentros: getEncuentros Novahis: size: " + vEncuentrosNovahis.size());
        LogFile.log("Encuentros: getEncuentros Siapwin: size: " + vEncuentrosSiapwin.size());
        return unificarEncuentros(vEncuentrosNovahis, vEncuentrosSiapwin);
    }

    public static Vector getEncuentros(Vector pacientes)
        throws SQLException, Exception
    {
        return null;
    }

    public static Vector getEncuentros(Vector pacientes, Vector vEpisodios)
        throws SQLException, Exception
    {
        Vector vEncuentrosSiapwin = new Vector();
        Vector vEncuentrosNovahis = new Vector();
        try
        {
            for(int i = 0; i < pacientes.size(); i++)
                if(pacientes.elementAt(i) instanceof PacienteNovahis)
                {
                    LogFile.log("Encuentro: getEncuentros: es Novahis");
                    String unaRestriccion = " WHERE ve_en_idclient='" + ((PacienteNovahis)pacientes.elementAt(i)).codigoCliente() + "'";
                    vEncuentrosNovahis = EncuentroNovahis.instancesWithWhere(unaRestriccion);
                } else
                {
                    LogFile.log("Encuentro: getEncuentros: es Siapwin");
                    Enumeration e = vEpisodios.elements();
                    EpisodioSalud episodio = null;
                    String sistema = null;
                    String idEpisodio = null;
                    StringBuffer restriccionSiapwin = new StringBuffer(" WHERE ");
                    int flagSiapwin = 0;
                    while(e.hasMoreElements()) 
                    {
                        episodio = (EpisodioSalud)e.nextElement();
                        idEpisodio = episodio.ve_ep_ref();
                        sistema = episodio.sistema().nombreSistema();
                        if(sistema.equals("SistemaSiapwin"))
                        {
                            if(flagSiapwin == 0)
                                restriccionSiapwin.append("ve_ep_ref = '" + idEpisodio + "'");
                            else
                                restriccionSiapwin.append(" OR ve_ep_ref = '" + idEpisodio + "'");
                            flagSiapwin++;
                        }
                    }
                    vEncuentrosSiapwin = EncuentroSiapwin.instancesWithWhere(restriccionSiapwin.toString());
                }

            LogFile.log("Encuentro: getEncuentros(Vector) Novahis: size: " + vEncuentrosNovahis.size());
            LogFile.log("Encuentro: getEncuentros(Vector) Siapwin: size: " + vEncuentrosSiapwin.size());
        }
        catch(Exception e)
        {
            LogFile.log(e);
        }
        return unificarEncuentros(vEncuentrosNovahis, vEncuentrosSiapwin);
    }

    public boolean isPersistent()
    {
        return false;
    }

    public void save()
        throws InstantiationException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException
    {
    }

    public void setDiagnostico(String s)
    {
    }

    public static Vector unificarEncuentros(Vector v1, Vector v2)
    {
        for(Enumeration e = v2.elements(); e.hasMoreElements(); v1.addElement(e.nextElement()));
        return v1;
    }

    public String ve_en_ref()
    {
        return null;
    }
}
