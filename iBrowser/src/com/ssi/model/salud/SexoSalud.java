// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SexoSalud.java

package com.ssi.model.salud;

import com.ssi.persistence.model.ReadPersistentObject;

public abstract class SexoSalud extends ReadPersistentObject
{

    private String codigo;
    private String descripcion;

    public String oid()
    {
        return codigo;
    }

    public void oid(String anOID)
    {
        codigo = anOID;
    }

    public String descripcion()
    {
        return descripcion;
    }

    public void descripcion(String aDescripcion)
    {
        descripcion = aDescripcion;
    }

    public SexoSalud()
    {
    }
}
