// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   EpisodioSiapwin.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import com.ssi.util.Parametros;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            EpisodioSalud, EpisodioSiapwinAccessor, SistemaSiapwin, PacienteSalud,
//            SistemaSalud

public class EpisodioSiapwin extends EpisodioSalud
{

    private String ve_ep_fecha;
    private String ve_ep_ref;
    private String ve_ep_hora;
    private String ve_ep_servicio;
    private String ve_ep_cond;
    private String ve_ep_estado;
    private String ve_ep_idclient;
    private String ve_ep_resolucion;

    public EpisodioSiapwin()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public SistemaSalud sistema()
    {
        return ((SistemaSalud) (SistemaSiapwin.getInstance()));
    }

    public String centroDescripcion()
    {
        return "s/d.";
    }

    public static String construirRestriccion(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
    {
        String unaRestriccion = null;
        if(rfc != null && rfc.length() > 0)
        {
            unaRestriccion = " codigo1 = '" + rfc + "'";
            LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 1: " + unaRestriccion);
        }
        if(nombre != null && nombre.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " nombre = '" + nombre + "'";
                LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 2.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nombre = '" + nombre + "'";
                LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 2.2: " + unaRestriccion);
            }
        if(apellido1 != null && apellido1.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " apellido1 = '" + apellido1 + "' ";
                LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 3.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido1 = '" + apellido1 + "'";
                LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 3.2: " + unaRestriccion);
            }
        if(apellido2 != null && apellido2.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " apellido2 = '" + apellido2 + "' ";
                LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 4.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido2 = '" + apellido2 + "' ";
                LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 4.2: " + unaRestriccion);
            }
        if(fechaNacimiento != null && fechaNacimiento.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " nac_fecha = '" + fechaNacimiento + "'";
                LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 5.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nac_fecha = '" + fechaNacimiento + "' ";
                LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 5.2: " + unaRestriccion);
            }
        if(unaRestriccion != null)
        {
            unaRestriccion = " where " + unaRestriccion;
        } else
        {
            LogFile.log("EpisodioSiapwin: construirRestriccion: unaRestriccion es null.");
            unaRestriccion = "";
        }
        LogFile.log("EpisodioSiapwin: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static Vector getEpisodios(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        String unaRestriccion = construirRestriccion(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        LogFile.log("EpisodioSiapwin: getEpisodios: WHERE-> " + unaRestriccion);
        Vector vEpisodiosSiapwin = instancesWithWhere(unaRestriccion);
        LogFile.log("EpisodioSiapwin: getEpisodios: size: " + vEpisodiosSiapwin.size());
        if(vEpisodiosSiapwin == null)
        {
            LogFile.log("EpisodioSiapwin: getEpisodios: Vector NULO ");
            vEpisodiosSiapwin = new Vector();
        }
        return vEpisodiosSiapwin;
    }

    public static Vector instancesWithWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector vEpisodio = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.EpisodioSiapwin"), unaRestriccion);
        if(vEpisodio == null)
        {
            LogFile.log("EpisodioSiapwin: instancesWithWhere: No hay Episodios.");
            vEpisodio = new Vector();
        }
        return vEpisodio;
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return EpisodioSiapwinAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            Episodio answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.EpisodioSiapwin"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.EpisodioSiapwin"));
    }

    public String ve_ep_hora()
    {
        return ve_ep_hora;
    }

    public void ve_ep_hora(String unVe_ep_hora)
    {
        ve_ep_hora = unVe_ep_hora;
    }

    public String ve_ep_servicio()
    {
        return ve_ep_servicio;
    }

    public void ve_ep_servicio(String unVe_ep_servicio)
    {
        ve_ep_servicio = unVe_ep_servicio;
    }

    public String ve_ep_cond()
    {
        return ve_ep_cond;
    }

    public void ve_ep_cond(String unVe_ep_cond)
    {
        ve_ep_cond = unVe_ep_cond;
    }

    public String ve_ep_estado()
    {
        return ve_ep_estado;
    }

    public void ve_ep_estado(String unVe_ep_estado)
    {
        ve_ep_estado = unVe_ep_estado;
    }

    public String ve_ep_idclient()
    {
        return ve_ep_idclient;
    }

    public void ve_ep_idclient(String unVe_ep_idclient)
    {
        ve_ep_idclient = unVe_ep_idclient;
    }

    public String ve_ep_resolucion()
    {
        return ve_ep_resolucion;
    }

    public void ve_ep_resolucion(String unVe_ep_resolucion)
    {
        ve_ep_resolucion = unVe_ep_resolucion;
    }

    public void delete()
        throws NoSuchMethodException, SQLException, ClassNotFoundException
    {
    }

    public String getDiagnostico()
    {
        if(ve_ep_cond == null)
            return Parametros.settings.getProperty("sinDatos","s/d");
        else
            return ve_ep_cond;
    }

    public String getServicio()
    {
        if(ve_ep_servicio == null)
            return Parametros.settings.getProperty("sinDatos","s/d");
        else
            return ve_ep_servicio;
    }

    public boolean isPersistent()
    {
        return false;
    }

    public String oid()
    {
        return ve_ep_ref;
    }

    public void oid(String anOID)
    {
        ve_ep_ref = anOID;
    }

    public void save()
        throws InstantiationException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException
    {
    }

    public String ve_cond_pk()
    {
        return "s/d.";
    }

    public void ve_cond_pk(String s)
    {
    }

    public String ve_enc_apert()
    {
        return "s/d.";
    }

    public void ve_enc_apert(String s)
    {
    }

    public String ve_ep_etiq()
    {
        return "s/d.";
    }

    public void ve_ep_etiq(String s)
    {
    }

    public String ve_ep_fecha()
    {
        return ve_ep_fecha;
    }

    public void ve_ep_fecha(String unVe_ep_fecha)
    {
        ve_ep_fecha = unVe_ep_fecha;
    }

    public String ve_ep_fecha_clin()
    {
        return null;
    }

    public void ve_ep_fecha_clin(String s)
    {
    }

    public String ve_ep_fechaend()
    {
        return null;
    }

    public void ve_ep_fechaend(String s)
    {
    }

    public String ve_ep_hora_clin()
    {
        return "s/d.";
    }

    public void ve_ep_hora_clin(String s)
    {
    }

    public String ve_ep_icd_cod()
    {
        return "s/d.";
    }

    public void ve_ep_icd_cod(String s)
    {
    }

    public String ve_ep_ref()
    {
        return ve_ep_ref;
    }

    public void ve_ep_ref(String unVe_ep_ref)
    {
        ve_ep_ref = unVe_ep_ref;
    }

    public String ve_fus_pk()
    {
        return "s/d.";
    }

    public void ve_fus_pk(String s)
    {
    }

    public String ve_sev_pk()
    {
        return "s/d.";
    }

    public void ve_sev_pk(String s)
    {
    }
}
