// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EpisodioSiapwinAccessor.java

package com.ssi.model.salud;

import com.ssi.persistence.cache.IPersistenceCache;
import com.ssi.persistence.model.AccessorCache;
import com.ssi.persistence.model.DBStringFieldType;
import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.PersistentField;
import java.util.Vector;

public class EpisodioSiapwinAccessor extends PersistenceAccessor
{

    public static String className()
    {
        return "com.ssi.model.salud.EpisodioSiapwinAccessor";
    }

    public static PersistenceAccessor getInstance()
    {
        return AccessorCache.getInstance().get(((Object) (className())));
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new EpisodioSiapwinAccessor(aManager)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new EpisodioSiapwinAccessor(aManager, aCache)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    protected EpisodioSiapwinAccessor(PersistenceManager aManager)
    {
        super(aManager);
    }

    protected EpisodioSiapwinAccessor(PersistenceManager aManager, IPersistenceCache aCache)
    {
        super(aManager, aCache);
    }

    public Vector addPersistentFields(Vector aAnswer)
    {
        aAnswer.addElement(((Object) (new PersistentField("ve_ep_ref", "oid", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ep_fecha", "ve_ep_fecha", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ep_hora", "ve_ep_hora", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ep_idclient", "ve_ep_idclient", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ep_servicio", "ve_ep_servicio", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ep_cond", "ve_ep_cond", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ep_estado", "ve_ep_estado", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ep_resolucion", "ve_ep_resolucion", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        return aAnswer;
    }

    public Vector collectionSpecs()
    {
        return new Vector();
    }

    public String oidPrefix()
    {
        return "";
    }

    public String persistentClassName()
    {
        return "com.ssi.model.salud.EpisodioSiapwin";
    }

    public String tableName()
    {
        return "ve_episodio";
    }
}
