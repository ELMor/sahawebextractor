// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   EncuentroNovahis.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import com.ssi.util.Parametros;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            EncuentroSalud, PacienteSalud, SistemaNovahis, EncuentroNovahisAccessor,
//            EpisodioNovahis, SistemaSalud

public class EncuentroNovahis extends EncuentroSalud
{

    private String ve_en_ref;
    private String ve_ep_ref;
    private String ve_en_fecha;
    private String ve_en_hora;
    private String ve_en_servicio;
    private String ve_en_motivo;
    private String ve_en_estado;
    private String ve_en_tipo;
    private String ve_en_idclient;
    private String ve_en_icd_cod;

    public EncuentroNovahis()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public SistemaSalud sistema()
    {
        return ((SistemaSalud) (SistemaNovahis.getInstance()));
    }

    public static String construirRestriccion(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
    {
        String unaRestriccion = null;
        if(rfc != null && rfc.length() > 0)
        {
            unaRestriccion = " codigo1 = '" + rfc + "'";
            LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 1: " + unaRestriccion);
        }
        if(nombre != null && nombre.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " nombre = '" + nombre + "'";
                LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 2.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nombre = '" + nombre + "'";
                LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 2.2: " + unaRestriccion);
            }
        if(apellido1 != null && apellido1.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " apellido1 = '" + apellido1 + "' ";
                LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 3.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido1 = '" + apellido1 + "'";
                LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 3.2: " + unaRestriccion);
            }
        if(apellido2 != null && apellido2.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " apellido2 = '" + apellido2 + "' ";
                LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 4.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido2 = '" + apellido2 + "' ";
                LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 4.2: " + unaRestriccion);
            }
        if(fechaNacimiento != null && fechaNacimiento.length() > 0)
            if(unaRestriccion == null)
            {
                unaRestriccion = " nac_fecha = '" + fechaNacimiento + "'";
                LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 5.1: " + unaRestriccion);
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nac_fecha = '" + fechaNacimiento + "' ";
                LogFile.log("EncuentroNovahis: construirRestriccion: WHERE 5.2: " + unaRestriccion);
            }
        if(unaRestriccion != null)
        {
            unaRestriccion = " where " + unaRestriccion;
        } else
        {
            LogFile.log("EncuentroNovahis: construirRestriccion: unaRestriccion es null.");
            unaRestriccion = "";
        }
        LogFile.log("EncuentroNovahis: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static Vector getEncuentro(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        String unaRestriccion = construirRestriccion(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        LogFile.log("EncuentroNovahis: getEpisodios: WHERE-> " + unaRestriccion);
        Vector vEncuentro = EpisodioNovahis.instancesWithWhere(unaRestriccion);
        LogFile.log("EncuentroNovahis: getEncuentro: size: " + vEncuentro.size());
        if(vEncuentro == null)
        {
            LogFile.log("EncuentroNovahis: getEncuentro: Vector NULO ");
            vEncuentro = new Vector();
        }
        return vEncuentro;
    }

    public static Vector instancesWithWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector vEncuentro = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.EncuentroNovahis"), unaRestriccion);
        if(vEncuentro == null)
        {
            LogFile.log("EncuentroNovahis: instancesWithWhere: No hay encuentros.");
            vEncuentro = new Vector();
        }
        return vEncuentro;
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return EncuentroNovahisAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            Encuentro answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.EncuentroNovahis"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.EncuentroNovahis"));
    }

    public String ve_en_ref()
    {
        return ve_en_ref;
    }

    public void ve_en_ref(String unVe_en_ref)
    {
        ve_en_ref = unVe_en_ref;
    }

    public String fechayHoraInicio()
    {
        String campoFecha = null;
        if(fechaInicio() != null)
        {
            Date date = Date.valueOf(fechaInicio().substring(0,9));
            DateFormat myformat = ((DateFormat) (new SimpleDateFormat("dd/MM/yyyy")));
            campoFecha = myformat.format(((java.util.Date) (date))) + " " + horaInicio();
        } else
        {
            campoFecha = Parametros.settings.getProperty("sinDatos","s/d");
        }
        return campoFecha;
    }

    public String fechaInicio()
    {
        return ve_en_fecha;
    }

    public void fechaInicio(String unVe_en_fecha)
    {
        ve_en_fecha = unVe_en_fecha;
    }

    public String horaInicio()
    {
        return ve_en_hora;
    }

    public void horaInicio(String unahoraInicio)
    {
        ve_en_hora = unahoraInicio;
    }

    public String ve_ep_ref()
    {
        return ve_ep_ref;
    }

    public void ve_ep_ref(String unVe_ep_ref)
    {
        ve_ep_ref = unVe_ep_ref;
    }

    public String ve_en_servicio()
    {
        return ve_en_servicio;
    }

    public void ve_en_servicio(String unVe_en_servicio)
    {
        ve_en_servicio = unVe_en_servicio;
    }

    public String ve_en_motivo()
    {
        return ve_en_motivo;
    }

    public void ve_en_motivo(String unVe_en_motivo)
    {
        ve_en_motivo = unVe_en_motivo;
    }

    public String ve_en_estado()
    {
        return ve_en_estado;
    }

    public void ve_en_estado(String unVe_en_estado)
    {
        ve_en_estado = unVe_en_estado;
    }

    public String ve_en_idclient()
    {
        return ve_en_idclient;
    }

    public void ve_en_idclient(String unVe_en_idclient)
    {
        ve_en_idclient = unVe_en_idclient;
    }

    public String ve_en_tipo()
    {
        return ve_en_tipo;
    }

    public void ve_en_tipo(String unVe_en_tipo)
    {
        ve_en_tipo = unVe_en_tipo;
    }

    public String ve_en_icd_cod()
    {
        return ve_en_icd_cod;
    }

    public void ve_en_icd_cod(String unVe_en_icd_cod)
    {
        ve_en_icd_cod = unVe_en_icd_cod;
    }

    public String oid()
    {
        return ve_en_ref;
    }

    public void oid(String unCodigo)
    {
        ve_en_ref = unCodigo;
    }

    public void delete()
        throws NoSuchMethodException, SQLException, ClassNotFoundException
    {
    }

    public String idEncuentro()
    {
        return ve_en_ref;
    }

    public boolean isPersistent()
    {
        return false;
    }

    public void save()
        throws InstantiationException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException
    {
    }
}
