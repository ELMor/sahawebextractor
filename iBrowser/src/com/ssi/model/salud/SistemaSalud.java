// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SistemaSalud.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceConfiguration;
import com.ssi.persistence.model.PersistenceManager;
import java.sql.SQLException;

public abstract class SistemaSalud
{

    private PersistenceManager persistenceManager;

    public SistemaSalud()
    {
        persistenceManager = new PersistenceManager(getConfiguration());
    }

    public abstract PersistenceConfiguration getConfiguration();

    public void connect()
        throws SQLException, Exception
    {
        getPersistenceManager().connect();
    }

    public void setPersistenceManager(PersistenceManager aManager)
    {
        persistenceManager = aManager;
    }

    public PersistenceManager getPersistenceManager()
    {
        return persistenceManager;
    }

    public abstract String nombreSistema();
}
