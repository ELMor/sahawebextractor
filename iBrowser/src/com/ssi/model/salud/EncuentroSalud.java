// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EncuentroSalud.java

package com.ssi.model.salud;

import com.ssi.persistence.model.ReadWritePersistentObject;

// Referenced classes of package com.ssi.model.salud:
//            SistemaSalud, Paciente

public abstract class EncuentroSalud extends ReadWritePersistentObject
{

    private Paciente unPaciente;

    public abstract void fechaInicio(String s);

    public abstract String fechaInicio();

    public abstract void horaInicio(String s);

    public abstract String horaInicio();

    public abstract String fechayHoraInicio();

    public void paciente(Paciente paciente)
    {
        unPaciente = paciente;
    }

    public Paciente paciente()
    {
        return unPaciente;
    }

    public String nombreSistema()
    {
        return sistema().nombreSistema();
    }

    public abstract SistemaSalud sistema();

    public EncuentroSalud()
    {
        unPaciente = new Paciente();
    }

    public abstract String idEncuentro();

    public abstract String oid();

    public abstract String ve_en_estado();

    public abstract String ve_en_icd_cod();

    public abstract String ve_en_idclient();

    public abstract String ve_en_motivo();

    public abstract String ve_en_ref();

    public abstract String ve_en_servicio();

    public abstract String ve_en_tipo();

    public abstract String ve_ep_ref();
}
