// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AccessorCache.java

package com.ssi.persistence.model;

import java.util.Hashtable;

// Referenced classes of package com.ssi.persistence.model:
//            PersistenceAccessor

public class AccessorCache
{

    private static AccessorCache singleton;
    private Hashtable cache;

    public static AccessorCache getInstance()
    {
        if(singleton == null)
            singleton = new AccessorCache();
        return singleton;
    }

    private AccessorCache()
    {
        cache = new Hashtable();
    }

    private Hashtable cache()
    {
        return cache;
    }

    public PersistenceAccessor put(Object key, PersistenceAccessor value)
    {
        return (PersistenceAccessor)cache().put(key, ((Object) (value)));
    }

    public PersistenceAccessor get(Object key)
    {
        return (PersistenceAccessor)cache().get(key);
    }
}
