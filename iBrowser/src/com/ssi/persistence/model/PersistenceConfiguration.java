// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistenceConfiguration.java

package com.ssi.persistence.model;


// Referenced classes of package com.ssi.persistence.model:
//            UserAccessor, ConnectionPool, PersistenceManager, DBEngineType, 
//            PersistenceFactory

public abstract class PersistenceConfiguration
{

    public abstract DBEngineType getDBEngineType();

    public abstract String databaseName();

    public abstract PersistenceFactory persistenceFactory();

    public abstract String driverName();

    public abstract int maxConnections();

    public abstract String getConnectionPassword();

    public abstract String getConnectionUser();

    public int indexStartPosition()
    {
        return 0;
    }

    public int indexStopPosition()
    {
        return 6;
    }

    public int prefixStartPosition()
    {
        return 7;
    }

    public int prefixStopPosition()
    {
        return 9;
    }

    public void configurePersistenceManager(PersistenceManager aManager)
    {
        setConnectionPoolTo(aManager);
        setAccessorsTo(aManager);
    }

    public void setConnectionPoolTo(PersistenceManager aManager)
    {
        aManager.connectionPool(new ConnectionPool(connectionAttempts(), poolWaitTime(), aManager));
    }

    public void setAccessorsTo(PersistenceManager aManager)
    {
        aManager.addAccessor(((PersistenceAccessor) (new UserAccessor(aManager))));
    }

    public abstract int connectionAttempts();

    public abstract int poolWaitTime();

    public PersistenceConfiguration()
    {
    }
}
