// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   UserAccessor.java

package com.ssi.persistence.model;

import com.ssi.persistence.cache.IPersistenceCache;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model:
//            PersistenceAccessor, DBOIDFieldType, DBStringFieldType, PersistentField,
//            AccessorCache, PersistenceManager

public class UserAccessor extends PersistenceAccessor
{

    public static String className()
    {
        return "com.ssi.persistence.model.UserAccessor";
    }

    public static PersistenceAccessor getInstance()
    {
        return AccessorCache.getInstance().get(((Object) (className())));
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new UserAccessor(aManager)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new UserAccessor(aManager, aCache)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    protected UserAccessor(PersistenceManager aManager)
    {
        super(aManager);
    }

    protected UserAccessor(PersistenceManager aManager, IPersistenceCache aCache)
    {
        super(aManager, aCache);
    }

    public Vector addPersistentFields(Vector aAnswer)
    {
        aAnswer.addElement(((Object) (new PersistentField("syslogin", "oid", 1, ((DBFieldType) (new DBOIDFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("syslogin", "name", 1, ((DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("syspwd", "password", 1, ((DBFieldType) (new DBStringFieldType()))))));
        return aAnswer;
    }

    public Vector collectionSpecs()
    {
        return new Vector();
    }

    public String oidPrefix()
    {
        return "USR";
    }

    public String persistentClassName()
    {
        return "com.ssi.persistence.model.User";
    }

    public String tableName()
    {
        return "sys_usu";
    }
}
