// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ProxyInterface.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            PersistentObjectInterface

public interface ProxyInterface
{

    public abstract Class classOfPrefix()
        throws ClassNotFoundException;

    public abstract Class classRepresented()
        throws ClassNotFoundException;

    public abstract void fetchRealFor(PersistentObjectInterface persistentobjectinterface, String s)
        throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, InstantiationException, SQLException, Exception;

    public abstract boolean isProxy();

    public abstract String oid();

    public abstract void oid(String s);
}
