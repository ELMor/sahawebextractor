// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBConnection.java

package com.ssi.persistence.model;

import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            ConnectionPool

public abstract class DBConnection
{

    private ConnectionPool connectionPool;

    public void close()
        throws SQLException
    {
    }

    public abstract Object connection();

    public abstract boolean isConnected();

    public void returnConnection()
        throws SQLException
    {
        getConnectionPool().returnConnection(this);
    }

    public void setConnectionPool(ConnectionPool aPool)
    {
        connectionPool = aPool;
    }

    public ConnectionPool getConnectionPool()
    {
        return connectionPool;
    }

    public DBConnection()
    {
    }
}
