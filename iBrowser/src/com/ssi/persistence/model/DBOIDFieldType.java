// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBOIDFieldType.java

package com.ssi.persistence.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBFieldType, DBField, DBStringFieldType, DBResultSet, 
//            PersistenceAccessor

public class DBOIDFieldType extends DBFieldType
{

    DBFieldType type;

    public String fieldClassName()
    {
        return type().fieldClassName();
    }

    public Object getValueOf(Object anInstance, Method aGetter)
        throws IllegalAccessException, InvocationTargetException
    {
        return type().getValueOf(anInstance, aGetter);
    }

    public static String nextOIDFromSet(DBResultSet aSet)
        throws SQLException
    {
        return aSet.getField("oid").getString();
    }

    public String oidForQuery(String anOID)
        throws Exception
    {
        return type().oidForQuery(anOID);
    }

    public static boolean represents(Field aField)
    {
        try
        {
            return (aField.getType() == Class.forName("java.lang.String")) & aField.getName().equals("oid");
        }
        catch(ClassNotFoundException _ex)
        {
            return false;
        }
    }

    public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
        throws IllegalAccessException, InvocationTargetException, SQLException
    {
        type().setValueTo(newInstance, aSetter, aField, anAccessor);
    }

    public DBFieldType type()
    {
        if(type == null)
            type(((DBFieldType) (new DBStringFieldType())));
        return type;
    }

    public void type(DBFieldType aType)
    {
        type = aType;
    }

    public DBOIDFieldType()
    {
    }
}
