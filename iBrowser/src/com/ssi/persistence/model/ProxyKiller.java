// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ProxyKiller.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model:
//            PersistenceManager, ProxyInterface

public class ProxyKiller
{

    private PersistenceManager persistenceManager;

    public ProxyKiller(PersistenceManager aManager)
    {
        persistenceManager = aManager;
    }

    public PersistenceManager persistenceManager()
    {
        return persistenceManager();
    }

    public Vector killProxiesInCollection(Vector aCollection)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Enumeration iterator = aCollection.elements();
        String oids = "";
        Class proxiesClass = null;
        while(iterator.hasMoreElements()) 
        {
            ProxyInterface nextElement = (ProxyInterface)iterator.nextElement();
            if(nextElement.isProxy())
            {
                proxiesClass = nextElement.classOfPrefix();
                if(oids.length() != 0)
                    oids = oids + ", ";
                oids = oids + "'" + nextElement.oid() + "'";
            }
        }
        oids = "oid in (" + oids + ")";
        if(proxiesClass != null)
            return persistenceManager().instancesOfWhere(proxiesClass, oids);
        else
            return new Vector();
    }
}
