// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ReadWritePersistentObject.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            PersistentObject, ProxyInterface, PersistentObjectInterface, PersistenceManager

public abstract class ReadWritePersistentObject extends PersistentObject
    implements PersistentObjectInterface, ProxyInterface
{

    public String oid;

    public void delete()
        throws ClassNotFoundException, NoSuchMethodException, SQLException
    {
        ((PersistentObject)this).persistenceManager().delete(((PersistentObjectInterface) (this)));
    }

    public boolean isPersistent()
    {
        return oid() != null;
    }

    public String oid()
    {
        return oid;
    }

    public void oid(String anOID)
    {
        oid = anOID;
    }

    public void save()
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        ((PersistentObject)this).persistenceManager().save(((PersistentObjectInterface) (this)));
    }

    public ReadWritePersistentObject()
    {
    }
}
