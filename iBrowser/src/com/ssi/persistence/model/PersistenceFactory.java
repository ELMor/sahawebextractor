// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistenceFactory.java

package com.ssi.persistence.model;

import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            PersistenceConfiguration, DBCommand, DBConnection, ConnectionPool

public abstract class PersistenceFactory
{

    private PersistenceConfiguration persistenceConfiguration;

    public PersistenceFactory(PersistenceConfiguration aConfiguration)
    {
        persistenceConfiguration = aConfiguration;
    }

    public void setDBEngineType(PersistenceConfiguration aConfiguration)
    {
        persistenceConfiguration = aConfiguration;
    }

    public PersistenceConfiguration getPersistenceConfiguration()
    {
        return persistenceConfiguration;
    }

    public abstract String connectionStringFor(String s, String s1, String s2);

    public abstract String connectionStringFor(String s);

    public abstract DBCommand dbCommand(String s);

    public abstract DBConnection dbConnection();

    public abstract DBConnection dbConnection(String s, ConnectionPool connectionpool)
        throws SQLException;

    public abstract DBConnection dbConnection(String s, String s1, String s2, ConnectionPool connectionpool)
        throws SQLException;
}
