// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistentCollection.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model:
//            PersistentObject, ProxyInterface, PersistentObjectInterface, PersistenceAccessor, 
//            PersistenceManager

public class PersistentCollection
{

    Vector collection;
    PersistentObjectInterface owner;

    public PersistentCollection(PersistentObjectInterface anOwner)
    {
        owner = anOwner;
    }

    public void addElement(Object anObject)
        throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, ClassNotFoundException, InstantiationException, SQLException
    {
        collection().addElement(anObject);
        Method aSetter = anObject.getClass().getMethod(ownerAccessorNameFor((PersistentObject)owner()), new Class[] {
            Class.forName("com.ssi.persistence.model.ProxyInterface")
        });
        aSetter.invoke(anObject, new Object[] {
            owner()
        });
    }

    public final int capacity()
        throws Exception
    {
        return collection().capacity();
    }

    private Vector collection()
        throws InstantiationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, SQLException
    {
        if(collection == null)
            setCollection();
        return collection;
    }

    protected void collection(Vector aVector)
    {
        collection = aVector;
    }

    public final boolean contains(Object anElement)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        fetchRealObjects();
        return collection().contains(anElement);
    }

    public final synchronized Object elementAt(int index)
        throws Exception
    {
        fetchRealObjects();
        return collection().elementAt(index);
    }

    public final synchronized Enumeration elements()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        fetchRealObjects();
        return collection().elements();
    }

    private void fetchRealObjects()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        if(needsToFetchRealObjects())
            persistenceManager().retrieveCollection(this);
    }

    public final synchronized Object firstElement()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        fetchRealObjects();
        return collection().firstElement();
    }

    protected Dictionary getInstancesByClass()
        throws InstantiationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, SQLException
    {
        Dictionary answer = ((Dictionary) (new Hashtable()));
        ProxyInterface element;
        Class aClass;
        for(Enumeration iterator = collection().elements(); iterator.hasMoreElements(); ((Vector)answer.get(((Object) (aClass)))).addElement(((Object) (element))))
        {
            element = (ProxyInterface)iterator.nextElement();
            aClass = element.classOfPrefix();
            if(answer.get(((Object) (aClass))) == null)
                answer.put(((Object) (aClass)), ((Object) (new Vector())));
        }

        return answer;
    }

    public final boolean isEmpty()
        throws Exception
    {
        return collection().isEmpty();
    }

    private boolean needsToFetchRealObjects()
        throws InstantiationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, SQLException
    {
        boolean needs = false;
        for(Enumeration iterator = collection().elements(); iterator.hasMoreElements() & (!needs);)
            if(((ProxyInterface)iterator.nextElement()).isProxy())
            {
                needs = true;
                return needs;
            }

        return needs;
    }

    protected PersistentObjectInterface owner()
    {
        return owner;
    }

    private String ownerAccessorNameFor(PersistentObject anObject)
        throws ClassNotFoundException, NoSuchMethodException
    {
        PersistenceAccessor accessor = persistenceManager().accessorForClass(((Object) (anObject)).getClass());
        return accessor.getAccessorNameInCollection(this);
    }

    private PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return owner.persistenceManager();
    }

    public final synchronized boolean removeElement(Object anObject)
        throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, InstantiationException, SQLException
    {
        boolean answer = collection().removeElement(anObject);
        Method aSetter = anObject.getClass().getMethod(ownerAccessorNameFor((PersistentObject)anObject), new Class[] {
            Class.forName("com.ssi.persistence.model.PersistenceProxy")
        });
        aSetter.invoke(anObject, ((Object []) (null)));
        return answer;
    }

    public void save()
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        for(Enumeration iterator = collection().elements(); iterator.hasMoreElements();)
        {
            ProxyInterface element = (ProxyInterface)iterator.nextElement();
            if(!element.isProxy())
                ((PersistentObjectInterface)iterator.nextElement()).save();
        }

    }

    private void setCollection()
        throws InstantiationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, SQLException
    {
        Vector myProxies = persistenceManager().proxiesForCollection(this);
        collection(myProxies);
    }

    public final int size()
        throws Exception
    {
        return collection().size();
    }
}
