// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   JDBCFactory.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.ConnectionPool;
import com.ssi.persistence.model.DBCommand;
import com.ssi.persistence.model.DBConnection;
import com.ssi.persistence.model.PersistenceConfiguration;
import com.ssi.persistence.model.PersistenceFactory;
import java.sql.DriverManager;
import java.sql.SQLException;
import com.ssi.util.Parametros;

// Referenced classes of package com.ssi.persistence.model.JDBC:
//            JDBCCommand, JDBCConnection

public class JDBCFactory extends PersistenceFactory
{

    public JDBCFactory(PersistenceConfiguration aConfiguration)
    {
        super(aConfiguration);
    }

    public String connectionStringFor(String aDatabaseName, String aUserName, String aPassword)
    {
        return Parametros.settings.getProperty("urlDB")
             + aDatabaseName + ";UID=" + aUserName + ";PWD=" + aPassword;
    }

    public String connectionStringFor(String aDatabaseName)
    {
        return Parametros.settings.getProperty("urlDB") + aDatabaseName;
    }

    public DBCommand dbCommand(String aCommandString)
    {
        return ((DBCommand) (JDBCCommand.instanceFor(aCommandString)));
    }

    public DBConnection dbConnection()
    {
        return null;
    }

    public DBConnection dbConnection(String aConnectionString, ConnectionPool aPool)
        throws SQLException
    {
        try
        {
            Class.forName(driverName());
        }
        catch(ClassNotFoundException _ex)
        {
            return null;
        }
        return ((DBConnection) (new JDBCConnection(DriverManager.getConnection(aConnectionString), aPool)));
    }

    public DBConnection dbConnection(String aConnectionString, String user, String password, ConnectionPool aPool)
        throws SQLException
    {
        try
        {
            Class.forName(driverName());
        }
        catch(ClassNotFoundException _ex)
        {
            return null;
        }
        return ((DBConnection) (new JDBCConnection(DriverManager.getConnection(aConnectionString, user, password), aPool)));
    }

    public String driverName()
    {
        return ((PersistenceFactory)this).getPersistenceConfiguration().driverName();
    }
}
