// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCNoAnswerResultSet.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.DBField;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model.JDBC:
//            JDBCResultSet

public class JDBCNoAnswerResultSet extends JDBCResultSet
{

    public void close()
        throws SQLException
    {
    }

    public boolean getEOF()
    {
        return true;
    }

    public DBField getField(int aFieldIndex)
    {
        return null;
    }

    public DBField getField(String aFieldName)
    {
        return null;
    }

    public void moveNext()
        throws SQLException
    {
    }

    public JDBCNoAnswerResultSet()
    {
    }
}
