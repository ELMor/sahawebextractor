// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCAnswerResultSet.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.DBField;
import com.ssi.util.LogFile;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model.JDBC:
//            JDBCResultSet, JDBCCommand, JDBCField, JDBCAnswerCommand

public class JDBCAnswerResultSet extends JDBCResultSet
{

    Boolean isEOF;
    ResultSet resultSet;
    Vector fields;
    JDBCAnswerCommand command;

    public JDBCAnswerResultSet(JDBCAnswerCommand aCommand, String aQueryString)
        throws SQLException
    {
        isEOF = new Boolean(false);
        fields = new Vector();
        command = aCommand;
        resultSet = ((JDBCCommand) (command)).command().executeQuery(aQueryString);
        moveNext();
    }

    private void addField(JDBCField aField)
    {
        fields().addElement(((Object) (aField)));
    }

    private JDBCField basicGetField(int anIndex)
    {
        JDBCField field = null;
        boolean found = false;
        int i = 0;
        JDBCField currentField = null;
        for(; !found && i < fields().size(); i++)
        {
            currentField = (JDBCField)fields().elementAt(i);
            found = currentField.columnIndex() == anIndex;
            if(found)
                field = currentField;
        }

        return field;
    }

    private JDBCField basicGetField(String anColumnName)
    {
        JDBCField field = null;
        boolean found = false;
        int i = 0;
        JDBCField currentField = null;
        for(; !found && i < fields().size(); i++)
        {
            currentField = (JDBCField)fields().elementAt(i);
            found = currentField.columnName().equals(((Object) (anColumnName)));
            if(found)
                field = currentField;
        }

        return field;
    }

    public void close()
        throws SQLException
    {
        resultSet.close();
        LogFile.log("JDBCAnswerResultSet::close()");
        ((JDBCCommand) (command)).closeCommand();
    }

    public boolean containsField(int anIndex)
    {
        JDBCField lFound = basicGetField(anIndex);
        return lFound != null;
    }

    public boolean containsField(String aColumnName)
    {
        JDBCField lFound = basicGetField(aColumnName);
        return lFound != null;
    }

    public Vector fields()
    {
        return fields;
    }

    private void flush()
    {
        fields().removeAllElements();
    }

    public boolean getEOF()
    {
        return isEOF.booleanValue();
    }

    public DBField getField(int aFieldIndex)
    {
        if(!containsField(aFieldIndex))
            addField(new JDBCField(resultSet, aFieldIndex));
        return ((DBField) (basicGetField(aFieldIndex)));
    }

    public DBField getField(String aFieldName)
    {
        if(!containsField(aFieldName))
            addField(new JDBCField(resultSet, aFieldName));
        return ((DBField) (basicGetField(aFieldName)));
    }

    public Boolean isEOF()
    {
        return isEOF;
    }

    public void isEOF(Boolean aBoolean)
    {
        isEOF = aBoolean;
    }

    public void moveNext()
        throws SQLException
    {
        if(!resultSet().next())
            isEOF(new Boolean(true));
        flush();
    }

    public ResultSet resultSet()
    {
        return resultSet;
    }
}
