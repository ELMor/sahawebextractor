// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCConnection.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.ConnectionPool;
import com.ssi.persistence.model.DBConnection;
import java.sql.Connection;
import java.sql.SQLException;

public class JDBCConnection extends DBConnection
{

    private Connection connection;

    public JDBCConnection(Connection aConnection, ConnectionPool aPool)
    {
        connection = aConnection;
        ((DBConnection)this).setConnectionPool(aPool);
    }

    public void close()
        throws SQLException
    {
        ((Connection)connection()).close();
        super.close();
    }

    public Object connection()
    {
        return ((Object) (connection));
    }

    public boolean isConnected()
    {
        try
        {
            return !((Connection)connection()).isClosed();
        }
        catch(SQLException _ex)
        {
            return false;
        }
    }
}
