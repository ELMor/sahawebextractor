// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCField.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.DBField;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class JDBCField extends DBField
{

    ResultSet resultSet;
    String columnName;
    int columnIndex;
    Object answer;

    public JDBCField(ResultSet aResultSet, int aColumnIndex)
    {
        resultSet = aResultSet;
        columnIndex = aColumnIndex;
    }

    public JDBCField(ResultSet aResultSet, String aColumnName)
    {
        resultSet = aResultSet;
        columnName = aColumnName;
    }

    public int columnIndex()
    {
        return columnIndex;
    }

    public String columnName()
    {
        return columnName;
    }

    public Boolean getBoolean()
        throws SQLException
    {
        if(isLoaded())
            return (Boolean)answer;
        if(columnName == null)
            answer = ((Object) (new Boolean(resultSet.getBoolean(columnIndex))));
        else
            answer = ((Object) (new Boolean(resultSet.getBoolean(columnName))));
        return (Boolean)answer;
    }

    public Date getDate()
        throws SQLException
    {
        if(isLoaded())
            return (Date)answer;
        if(columnName == null)
            answer = ((Object) (resultSet.getDate(columnIndex)));
        else
            answer = ((Object) (resultSet.getDate(columnName)));
        return (Date)answer;
    }

    public GregorianCalendar getGregorianCalendar()
        throws SQLException
    {
        if(isLoaded())
            return (GregorianCalendar)this.answer;
        Date aDate;
        if(columnName == null)
            aDate = ((Date) (resultSet.getDate(columnIndex)));
        else
            aDate = ((Date) (resultSet.getDate(columnName)));
        GregorianCalendar answer;
        if(aDate != null)
        {
            answer = new GregorianCalendar(aDate.getDate(), aDate.getMonth(), aDate.getYear() + 1900);
            ((Calendar) (answer)).setTime(aDate);
        } else
        {
            answer = null;
        }
        return answer;
    }

    public Timestamp getTimestamp()
        throws SQLException
    {
        if(isLoaded())
            return (Timestamp)answer;
        if(columnName == null)
            answer = ((Object) (resultSet.getTimestamp(columnIndex)));
        else
            answer = ((Object) (resultSet.getTimestamp(columnName)));
        return (Timestamp)answer;
    }

    public Character getCharacter()
        throws SQLException
    {
        if(isLoaded())
            return (Character)answer;
        String aString = "";
        if(columnName == null)
            aString = resultSet.getString(columnIndex);
        else
            aString = resultSet.getString(columnName);
        if(aString == null)
            answer = null;
        else
            answer = ((Object) (new Character(aString.trim().toCharArray()[0])));
        return (Character)answer;
    }

    public Double getDouble()
        throws SQLException
    {
        if(isLoaded())
            return (Double)answer;
        if(columnName == null)
            answer = ((Object) (new Double(resultSet.getDouble(columnIndex))));
        else
            answer = ((Object) (new Double(resultSet.getDouble(columnName))));
        return (Double)answer;
    }

    public Float getFloat()
        throws SQLException
    {
        if(isLoaded())
            return (Float)answer;
        if(columnName == null)
            answer = ((Object) (new Float(resultSet.getFloat(columnIndex))));
        else
            answer = ((Object) (new Float(resultSet.getFloat(columnName))));
        return (Float)answer;
    }

    public Integer getInteger()
        throws SQLException
    {
        if(isLoaded())
            if(answer.getClass().getName().equals("java.lang.Integer"))
                return (Integer)answer;
            else
                return new Integer((String)answer);
        if(columnName != null)
            answer = ((Object) (new Integer(resultSet.getInt(columnName))));
        else
            answer = ((Object) (new Integer(resultSet.getInt(columnIndex))));
        return (Integer)answer;
    }

    public Long getLong()
        throws SQLException
    {
        if(isLoaded())
            return (Long)answer;
        if(columnName == null)
            answer = ((Object) (new Long(resultSet.getLong(columnIndex))));
        else
            answer = ((Object) (new Long(resultSet.getLong(columnName))));
        return (Long)answer;
    }

    public String getString()
        throws SQLException
    {
        String result = null;
        if(isLoaded())
        {
            result = (String)answer;
            return result;
        }
        if(columnName == null)
            answer = ((Object) (resultSet.getString(columnIndex)));
        else
            answer = ((Object) (resultSet.getString(columnName)));
        result = (String)answer;
        return result;
    }

    protected boolean isLoaded()
    {
        return answer != null;
    }
}
