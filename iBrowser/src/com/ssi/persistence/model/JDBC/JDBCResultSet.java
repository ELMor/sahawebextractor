// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCResultSet.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.DBCommand;
import com.ssi.persistence.model.DBResultSet;

public abstract class JDBCResultSet extends DBResultSet
{

    public int getRecordCount()
    {
        return 0;
    }

    public void moveFirst()
    {
    }

    public void open(DBCommand dbcommand)
    {
    }

    public JDBCResultSet()
    {
    }
}
