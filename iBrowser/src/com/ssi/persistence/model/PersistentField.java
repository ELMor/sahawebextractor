// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistentField.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBFieldType, DBResultSet, DBField, PersistenceAccessor

public class PersistentField
{

    DBFieldType fieldType;
    String setterName;
    String columnName;
    int positionIndex;

    public PersistentField(String name, String setter, int index, DBFieldType type)
    {
        positionIndex = index;
        columnName = name;
        fieldType = type;
        setterName = setter;
    }

    public String columnName()
    {
        return columnName;
    }

    public DBFieldType fieldType()
    {
        return fieldType;
    }

    private DBField getField(DBResultSet aSet)
    {
        return aSet.getField(columnName());
    }

    private Method getter(Class targetClass)
        throws ClassNotFoundException, NoSuchMethodException
    {
        return fieldType().getGetterMethodFrom(targetClass, setterName());
    }

    public String oidForQuery(String anOID)
        throws Exception
    {
        return fieldType().oidForQuery(anOID);
    }

    public int positionIndex()
    {
        return positionIndex;
    }

    private Method setter(Class targetClass)
        throws ClassNotFoundException, NoSuchMethodException
    {
        Method aMethod = null;
        try
        {
            aMethod = fieldType().getSetterMethodFrom(targetClass, setterName());
        }
        catch(NoSuchMethodException _ex)
        {
            throw new NoSuchMethodException("Setter " + setterName() + " with type " + fieldType() + " not found");
        }
        catch(ClassNotFoundException _ex)
        {
            throw new ClassNotFoundException("Class " + targetClass + " not found");
        }
        return aMethod;
    }

    public String setterName()
    {
        return setterName;
    }

    public void setValueTo(Object anInstance, DBResultSet aSet, PersistenceAccessor anAccessor)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, SQLException
    {
        fieldType().setValueTo(anInstance, setter(anInstance.getClass()), getField(aSet), anAccessor);
    }

    public String toString()
    {
        return "Col:" + columnName + "|ndx:" + positionIndex + "|setter:" + setterName + "|DBFType:" + (fieldType != null ? "null" : fieldType.toString());
    }

    public Object valueOf(Object anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        return fieldType().getValueOf(anInstance, getter(anInstance.getClass()));
    }
}
