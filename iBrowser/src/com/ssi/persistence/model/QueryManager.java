// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   QueryManager.java

package com.ssi.persistence.model;

import com.ssi.log.model.Log;
import com.ssi.log.model.LogInterface;
import com.ssi.log.model.NoLog;
import java.sql.SQLException;
import java.util.Date;

// Referenced classes of package com.ssi.persistence.model:
//            PersistenceFactory, DBCommand, PersistenceManager, DBConnection,
//            DBResultSet

public class QueryManager
{

    private LogInterface queryLog;
    private PersistenceManager persistenceManager;
    boolean logsQueries;

    public QueryManager(PersistenceManager aPersistenceManager)
    {
        logsQueries = false;
        setPersistenceManager(aPersistenceManager);
        setQueryLog();
    }

    private DBCommand commandFor(String aStringCommand)
        throws SQLException
    {
        DBCommand newCommand = persistenceFactory().dbCommand(aStringCommand);
        newCommand.setActiveConnection(getConnection());
        newCommand.setCommandString(aStringCommand);
        return newCommand;
    }

    private DBConnection getConnection()
        throws SQLException
    {
        return getPersistenceManager().getConnection();
    }

    private DBResultSet executeCommand(DBCommand aCommand)
        throws SQLException
    {
        return aCommand.execute();
    }

    public DBResultSet executeCommandFor(String aStringCommand)
        throws SQLException
    {
        queryLog().logString(aStringCommand);
        Date startTime = new Date();
        DBResultSet answer = executeCommand(commandFor(aStringCommand));
        Date endTime = new Date();
        queryLog().logString("   Starting: " + startTime.toString());
        queryLog().logString("   Ending: " + endTime.toString());
        queryLog().logStringCR("   Lasted: " + (endTime.getTime() - startTime.getTime()));
        return answer;
    }

    public void logQueries(boolean aBoolean)
    {
        logsQueries = aBoolean;
        setQueryLog();
    }

    public boolean logsQueries()
    {
        return logsQueries;
    }

    private PersistenceFactory persistenceFactory()
    {
        return getPersistenceManager().persistenceFactory();
    }

    private PersistenceManager getPersistenceManager()
    {
        return persistenceManager;
    }

    private void setPersistenceManager(PersistenceManager aManager)
    {
        persistenceManager = aManager;
    }

    public LogInterface queryLog()
    {
        return queryLog;
    }

    private void queryLog(LogInterface aQueryLog)
    {
        queryLog = aQueryLog;
    }

    protected void setQueryLog()
    {
        if(logsQueries())
            queryLog(((LogInterface) (new Log())));
        else
            queryLog(((LogInterface) (new NoLog())));
    }

    public void startLogging()
    {
        logQueries(true);
    }

    public void stopLogging()
    {
        logQueries(false);
    }
}
