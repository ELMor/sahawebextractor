// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   SistemaNovahisConfiguration.java

package com.ssi.persistence.model.salud;

import com.ssi.model.salud.ActividadAccessor;
import com.ssi.model.salud.ActividadNovahisAccessor;
import com.ssi.model.salud.EncuentroAccessor;
import com.ssi.model.salud.EncuentroNovahisAccessor;
import com.ssi.model.salud.EpisodioAccessor;
import com.ssi.model.salud.EpisodioNovahisAccessor;
import com.ssi.model.salud.PacienteNovahisAccessor;
import com.ssi.model.salud.SexoNovahisAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.UserAccessor;
import com.ssi.util.Parametros;

// Referenced classes of package com.ssi.persistence.model.salud:
//            SistemaSaludConfiguration

public class SistemaNovahisConfiguration extends SistemaSaludConfiguration
{

    public String databaseName()
    {
        return Parametros.settings.getProperty("opensic.databasename");
    }

    public String getConnectionUser()
    {
        return Parametros.settings.getProperty("opensic.username");
    }

    public String getConnectionPassword()
    {
        return Parametros.settings.getProperty("opensic.password");
    }

    public void setAccessorsTo(PersistenceManager aManager)
    {
        aManager.addAccessor(PacienteNovahisAccessor.getInstance(aManager));
        aManager.addAccessor(SexoNovahisAccessor.getInstance(aManager));
        aManager.addAccessor(EpisodioAccessor.getInstance(aManager));
        aManager.addAccessor(EncuentroAccessor.getInstance(aManager));
        aManager.addAccessor(ActividadAccessor.getInstance(aManager));
        aManager.addAccessor(UserAccessor.getInstance(aManager));
        aManager.addAccessor(EpisodioNovahisAccessor.getInstance(aManager));
        aManager.addAccessor(EncuentroNovahisAccessor.getInstance(aManager));
        aManager.addAccessor(ActividadNovahisAccessor.getInstance(aManager));
    }

    public int connectionAttempts()
    {
        return 10;
    }

    public int poolWaitTime()
    {
        return 1000;
    }

    public int maxConnections()
    {
        return 3;
    }

    public SistemaNovahisConfiguration()
    {
    }
}
