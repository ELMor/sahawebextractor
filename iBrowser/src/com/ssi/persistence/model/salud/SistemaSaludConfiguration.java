// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   SistemaSaludConfiguration.java

package com.ssi.persistence.model.salud;

import com.ssi.persistence.model.DBEngineType;
import com.ssi.persistence.model.JDBC.JDBCFactory;
import com.ssi.persistence.model.PersistenceConfiguration;
import com.ssi.persistence.model.PersistenceFactory;
import com.ssi.persistence.model.sybase.DBSybaseType;
import com.ssi.util.Parametros;

public abstract class SistemaSaludConfiguration extends PersistenceConfiguration
{

    public String driverName()
    {
        return Parametros.settings.getProperty("urlDriver");
    }

    public DBEngineType getDBEngineType()
    {
        return ((DBEngineType) (new DBSybaseType()));
    }

    public PersistenceFactory persistenceFactory()
    {
        return ((PersistenceFactory) (new JDBCFactory(((PersistenceConfiguration) (this)))));
    }

    public SistemaSaludConfiguration()
    {
    }
}
