// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBStringFieldType.java

package com.ssi.persistence.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBFieldType, DBField, PersistenceAccessor

public class DBStringFieldType extends DBFieldType
{

    public String fieldClassName()
    {
        return "java.lang.String";
    }

    public Object getValueOf(Object anInstance, Method aGetter)
        throws IllegalAccessException, InvocationTargetException
    {
        return ((Object) ("'" + aGetter.invoke(anInstance, ((Object []) (null))) + "'"));
    }

    public String oidForQuery(String anOID)
    {
        return "'" + anOID + "'";
    }

    public static boolean represents(Field aField)
    {
        try
        {
            return (aField.getType() == Class.forName("java.lang.String")) & (!aField.getName().equals("oid"));
        }
        catch(ClassNotFoundException _ex)
        {
            return false;
        }
    }

    public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
        throws IllegalAccessException, InvocationTargetException, SQLException
    {
        aSetter.invoke(newInstance, ((Object []) (new String[] {
            aField.getString()
        })));
    }

    public DBStringFieldType()
    {
    }
}
