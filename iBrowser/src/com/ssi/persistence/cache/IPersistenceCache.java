// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IPersistenceCache.java

package com.ssi.persistence.cache;

import com.ssi.persistence.model.PersistentObjectInterface;

public interface IPersistenceCache
{

    public abstract Object at(Object obj);

    public abstract void atKeyPut(Object obj, PersistentObjectInterface persistentobjectinterface);

    public abstract boolean includes(PersistentObjectInterface persistentobjectinterface);

    public abstract boolean includesKey(Object obj);

    public abstract void remove(PersistentObjectInterface persistentobjectinterface);
}
