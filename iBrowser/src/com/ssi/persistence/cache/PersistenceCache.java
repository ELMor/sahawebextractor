// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistenceCache.java

package com.ssi.persistence.cache;

import com.ssi.persistence.model.PersistentObjectInterface;
import com.ssi.util.LogFile;
import java.util.Hashtable;

// Referenced classes of package com.ssi.persistence.cache:
//            IPersistenceCache

public class PersistenceCache
    implements IPersistenceCache
{

    Hashtable dictionary;

    public PersistenceCache()
    {
        dictionary = new Hashtable();
    }

    public Object at(Object aKey)
    {
        return dictionary().get(aKey);
    }

    public void atKeyPut(Object aKey, PersistentObjectInterface aValue)
    {
        if(aKey == null)
        {
            LogFile.log("PersistenceCache: recibiendo Object nulo");
            return;
        }
        if(aValue == null)
        {
            LogFile.log("PersistenceCache: recibiendo PersistentObjectInterface nulo");
            return;
        } else
        {
            dictionary().put(aKey, ((Object) (aValue)));
            return;
        }
    }

    private Hashtable dictionary()
    {
        return dictionary;
    }

    public boolean includes(PersistentObjectInterface aValue)
    {
        return dictionary().contains(((Object) (aValue)));
    }

    public boolean includesKey(Object aKey)
    {
        return dictionary().containsKey(aKey);
    }

    public void remove(PersistentObjectInterface aValue)
    {
        dictionary().remove(((Object) (aValue.oid())));
    }
}
