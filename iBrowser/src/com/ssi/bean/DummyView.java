// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DummyView.java

package com.ssi.bean;

import com.ssi.http.ConeccionHTTP;

// Referenced classes of package com.ssi.bean:
//            View

public class DummyView extends View
{

    public String URL()
    {
        return getURL();
    }

    public static String getURL()
    {
        return "/jsp/dummyView.jsp";
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        return getURL();
    }

    public DummyView()
    {
    }
}
