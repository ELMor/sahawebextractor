// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IView.java

package com.ssi.bean;

import com.ssi.http.ConeccionHTTP;

// Referenced classes of package com.ssi.bean:
//            ServletSSI

public interface IView
{

    public abstract String obtenerURLDestino(ConeccionHTTP coneccionhttp);

    public abstract void resolverRequerimiento(ConeccionHTTP coneccionhttp, ServletSSI servletssi);
}
