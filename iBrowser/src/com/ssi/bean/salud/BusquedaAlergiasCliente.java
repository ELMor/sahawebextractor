// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaAlergiasCliente.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.model.salud.Alergia;
import com.ssi.model.salud.PacienteNovahis;
import com.ssi.model.salud.PacienteSiapwin;
import com.ssi.model.salud.SistemaNovahis;
import com.ssi.model.salud.SistemaSalud;
import com.ssi.model.salud.SistemaSiapwin;
import com.ssi.persistence.model.DBField;
import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import java.util.Enumeration;
import java.util.Vector;

public class BusquedaAlergiasCliente extends View
{

    String numeroPacienteReferenciado;
    Vector pacientes;

    public String getNumeroPacienteReferenciado()
    {
        return numeroPacienteReferenciado;
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        return URL();
    }

    public void setNumeroPacienteReferenciado(String newNumeroPacienteReferenciado)
    {
        numeroPacienteReferenciado = newNumeroPacienteReferenciado;
    }

    public String URL()
    {
        return "/jsp/AlergiasCliente.jsp";
    }

    public Vector ejecutarConsulta()
    {
        Vector vAlergiasSiapwin = new Vector();
        Vector vAlergiasNovahis = new Vector();
        if(pacientes == null)
            LogFile.log("Debe setear el vector de pacientes seleccionados");
        else
            try
            {
                for(int i = 0; i < pacientes.size(); i++)
                    if(pacientes.elementAt(i) instanceof PacienteNovahis)
                    {
                        LogFile.log("BusquedaAlergiasCliente: ejecutarConsulta: es Novahis");
                        String queryString = getQueryNovahis(((PacienteNovahis)pacientes.elementAt(i)).oid());
                        LogFile.log("Query de alergias: " + queryString);
                        PersistenceManager manager = null;
                        JDBCAnswerResultSet rs = null;
                        try
                        {
                            manager = ((SistemaSalud) (SistemaNovahis.getInstance())).getPersistenceManager();
                            for(rs = (JDBCAnswerResultSet)manager.executeQuery(queryString); rs.isEOF().equals(((Object) (Boolean.FALSE))); rs.moveNext())
                                vAlergiasNovahis.addElement(((Object) (new Alergia(rs.getField(1).getString(), rs.getField(2).getDate()))));

                        }
                        catch(Exception e)
                        {
                            LogFile.log(e);
                        }
                        finally
                        {
                            rs.close();
                        }
                    } else
                    {
                        LogFile.log("BusquedaAlergiasCliente: ejecutarConsulta: es Siapwin");
                        String queryString = getQuerySiapwin(((PacienteSiapwin)pacientes.elementAt(i)).oid());
                        LogFile.log("Query de alergias: " + queryString);
                        try
                        {
                            PersistenceManager manager = ((SistemaSalud) (SistemaSiapwin.getInstance())).getPersistenceManager();
                            JDBCAnswerResultSet rs;
                            for(rs = (JDBCAnswerResultSet)manager.executeQuery(queryString); rs.isEOF().equals(((Object) (Boolean.FALSE))); rs.moveNext())
                                vAlergiasSiapwin.addElement(((Object) (new Alergia(rs.getField(1).getString(), rs.getField(2).getDate()))));

                            rs.close();
                        }
                        catch(Exception e)
                        {
                            LogFile.log(e);
                        }
                    }

            }
            catch(Exception e)
            {
                LogFile.log(e);
            }
        return unificarAlergias(vAlergiasNovahis, vAlergiasSiapwin);
    }

    public Vector getPacientes()
    {
        return pacientes;
    }

    public String getQueryNovahis(String idPacienteNovahis)
    {
        return "select ta.tipo_alrg_desc, cl.clialrg_fdesde from clien_alergias cl, tipo_alergia ta where ta.tipo_alrg_pk = cl.tipo_alrg_pk and cl.clialrg_activa_sn = 1 and codigo_cliente = " + idPacienteNovahis;
    }

    public String getQuerySiapwin(String idPacienteSiapwin)
    {
        return "select ta.tipo_alrg_desc, ha.fecha from tipo_alergia ta, halergia ha where ha.activo = 1 and ta.tipo_alrg_pk = ha.tipo_alrg_pk and ha.codigo_cliente = " + idPacienteSiapwin;
    }

    public void setPacientes(Vector newPacientes)
    {
        pacientes = newPacientes;
    }

    public Vector unificarAlergias(Vector v1, Vector v2)
    {
        for(Enumeration e = v2.elements(); e.hasMoreElements(); v1.addElement(e.nextElement()));
        return v1;
    }

    public BusquedaAlergiasCliente()
    {
    }
}
