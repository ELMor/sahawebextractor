// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   ServletSalud.java

package com.ssi.bean.salud;

import com.ssi.bean.DummyView;
import com.ssi.bean.IView;
import com.ssi.bean.ServletSSI;
import com.ssi.http.ConeccionHTTP;
import com.ssi.http.salud.ConeccionHTTPSalud;
import com.ssi.model.salud.SistemaNovahis;
import com.ssi.model.salud.SistemaSalud;
import com.ssi.model.salud.SistemaSiapwin;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.salud.BrokerSaludConfiguration;
import com.ssi.util.LogFile;
import com.ssi.util.Parametros;
import java.sql.SQLException;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import java.util.Properties;
import java.util.Enumeration;

// Referenced classes of package com.ssi.bean.salud:
//            Login

public class ServletSalud extends ServletSSI
{

    private PersistenceManager persistenceManager;
    private SistemaSalud sistemaNovahis;
    private SistemaSalud sistemaSiapwin;

    public IView getDummyView()
    {
        return ((IView) (new DummyView()));
    }

    public IView getLoginView()
    {
        return ((IView) (new Login()));
    }

    public IView obtenerView(ConeccionHTTP unaConeccion)
    {
        if(unaConeccion.existsParamValue("view"))
        {
            try
            {
                String valueView = unaConeccion.getSingleParamValue("view");
                LogFile.log("ServletSalud: obtenerView: valueView: " + valueView);
                return (IView)unaConeccion.getFromSession(valueView);
            }
            catch(Exception ex)
            {
                LogFile.log("ServletSalud: obtenerView: Excepcion: " + ex);
            }
            LogFile.log("ServletSalud: obtenerView: llamo a DummyView...");
            return getDummyView();
        } else
        {
            return getLoginView();
        }
    }

    public ConeccionHTTP getConeccionHTTP(HttpServletRequest request, HttpServletResponse response)
        throws Exception
    {
        return ((ConeccionHTTP) (new ConeccionHTTPSalud(request, response)));
    }

    public void connect()
    {
        LogFile.log("ServletSalud: connect: Inicio.");
        PersistenceManager persistenceManager = new PersistenceManager(((com.ssi.persistence.model.PersistenceConfiguration) (new BrokerSaludConfiguration())));
        try
        {
            persistenceManager.connect();
        }
        catch(SQLException _ex)
        {
            LogFile.log("ServletSalud: connect: Se produjo una SQLExcepcion.");
        }
        catch(Exception _ex)
        {
            LogFile.log("ServletSalud: connect: Se produjo una Excepcion.");
        }
        LogFile.log("ServletSalud: connect: Fin.");
    }

    public void connectNovahis()
    {
        LogFile.log("ServletSalud: connectNovahis: Inicio.");
        SistemaSalud pmNovahis = ((SistemaSalud) (SistemaNovahis.getInstance()));
        try
        {
            pmNovahis.connect();
        }
        catch(SQLException _ex)
        {
            LogFile.log("ServletSalud: connectNovahis: Se produjo una SQLExcepcion.");
        }
        catch(Exception _ex)
        {
            LogFile.log("ServletSalud: connectNovahis: Se produjo una Excepcion.");
        }
        LogFile.log("ServletSalud: connectNovahis: Fin.");
    }

    public void connectSiapwin()
    {
        LogFile.log("ServletSalud: connectSiapwin: Inicio.");
        SistemaSalud pmSiapwin = ((SistemaSalud) (SistemaSiapwin.getInstance()));
        try
        {
            pmSiapwin.connect();
        }
        catch(SQLException _ex)
        {
            LogFile.log("ServletSalud: connectSiapwin: Se produjo una SQLExcepcion.");
        }
        catch(Exception _ex)
        {
            LogFile.log("ServletSalud: connectSiapwin: Se produjo una Excepcion.");
        }
        LogFile.log("ServletSalud: connectSiapwin: Fin.");
    }

    public void openLog()
    {
        Parametros.inicializarVariablesStaticas();
        try
        {
            Parametros.settings.getProperty("LOG_FILENAME","Salud.log").length();
        }
        catch(Exception _ex) { }
        LogFile.openLogFile(Parametros.settings.getProperty("LOG_FILENAME","Salud.log"));
        LogFile.log("ServletSalud: openLog: Reinici\363 el servlet");
    }

    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
        openLog();
        for(Enumeration e=config.getInitParameterNames();e.hasMoreElements();){
          String parname=(String)e.nextElement();
          //Parametros.settings.setProperty(parname,config.getInitParameter(parname));
          Parametros.settings.put(parname,config.getInitParameter(parname));
        }
        connectNovahis();
        connectSiapwin();
    }

    public void setPersistenceManager(PersistenceManager unManager)
    {
        persistenceManager = unManager;
    }

    public PersistenceManager getPersistenceManager()
    {
        return persistenceManager;
    }

    public void destroy()
    {
        try
        {
            LogFile.log("=========================================================");
            LogFile.log("*************** FINALIZANDO SERVLET SALUD ***************");
            LogFile.log("=========================================================");
            getPersistenceManager().closeConnection();
        }
        catch(Exception _ex) { }
    }

    public ServletSalud()
    {
    }
}
