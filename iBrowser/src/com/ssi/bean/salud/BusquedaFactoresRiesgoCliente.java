// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaFactoresRiesgoCliente.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.model.salud.SistemaSalud;
import com.ssi.model.salud.SistemaSiapwin;
import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;

public class BusquedaFactoresRiesgoCliente extends View
{

    String numeroPacienteReferenciado;

    public JDBCAnswerResultSet ejecutarConsulta()
    {
        try
        {
            PersistenceManager manager = ((SistemaSalud) (SistemaSiapwin.getInstance())).getPersistenceManager();
            return (JDBCAnswerResultSet)manager.executeQuery(getQueryString());
        }
        catch(Exception e)
        {
            LogFile.log(((Throwable) (e)).toString());
        }
        return null;
    }

    public String getNumeroPacienteReferenciado()
    {
        return numeroPacienteReferenciado;
    }

    public String getQueryString()
    {
        String result = "select tfr.nom_fr, hfr.fecha from tfac_rie tfr, hfac_rie hfr where hfr.activo = 1 and tfr.cod_fr = hfr.cod_fr and hfr.codigo_cliente =" + getNumeroPacienteReferenciado();
        LogFile.log("getQueryString(): " + result);
        return result;
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        return URL();
    }

    public void setNumeroPacienteReferenciado(String newNumeroPacienteReferenciado)
    {
        numeroPacienteReferenciado = newNumeroPacienteReferenciado;
    }

    public String URL()
    {
        return "/jsp/FactoresRiesgoCliente.jsp";
    }

    public BusquedaFactoresRiesgoCliente()
    {
    }
}
