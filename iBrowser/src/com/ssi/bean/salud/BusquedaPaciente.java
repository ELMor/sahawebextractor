// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaPaciente.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.model.salud.Paciente;
import com.ssi.model.salud.PacienteNovahis;
import com.ssi.model.salud.PacienteSalud;
import com.ssi.model.salud.PacienteSiapwin;
import com.ssi.util.LogFile;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.bean.salud:
//            Login, BusquedaEpisodios

public class BusquedaPaciente extends View
{

    private String rfc;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String fechaNacimiento;
    private Vector listaPacientes;
    private String mensajeExcesoPacientes;
    private static int limitePacientes = 5;

    public BusquedaPaciente()
    {
        rfc = "";
        nombre = "";
        apellido1 = "";
        apellido2 = "";
        fechaNacimiento = "";
        mensajeExcesoPacientes = "Se han encontrado demasiados resultados, si el paciente no est&aacute; en este listado, busque nuevamente completando m&aacute;s datos.";
        limitePacientes = 1000;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public String getFechaNac()
    {
        return fechaNacimiento;
    }

    public int getLimitePacientes()
    {
        return limitePacientes;
    }

    public Vector getListaPacientes()
    {
        return listaPacientes;
    }

    public String getMensajeExcesoPacientes()
    {
        return mensajeExcesoPacientes;
    }

    public String getNombrePaciente()
    {
        return nombre;
    }

    public String getRFC()
    {
        return rfc;
    }

    public static String getURL()
    {
        return "/jsp/buscarPacientes.jsp";
    }

    public static String getURLResultados()
    {
        return "/jsp/resultadosPacientes.jsp";
    }

    public void obtenerInformacionPantalla(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: Inicio");
        try
        {
            setNombrePaciente(unaConeccion.getSingleParamValue("tfNombre").toUpperCase().trim());
            LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfNombre: " + getNombrePaciente());
            setApellido1(unaConeccion.getSingleParamValue("tfApellido1").toUpperCase().trim());
            LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfApellido1: " + getApellido1().trim());
            setApellido2(unaConeccion.getSingleParamValue("tfApellido2").toUpperCase().trim());
            LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfApellido2: " + getApellido2());
            setRFC(unaConeccion.getSingleParamValue("tfRFC").trim());
            LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfRFC: " + getRFC());
            setFechaNac(unaConeccion.getSingleParamValue("tfFechaNac").trim());
            LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfFechaNac: " + getFechaNac());
        }
        catch(Exception ex)
        {
            setNombrePaciente(((String) (null)));
            setApellido1(((String) (null)));
            setApellido2(((String) (null)));
            setRFC(((String) (null)));
            setFechaNac(((String) (null)));
            LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: " + ex);
        }
    }

    public void obtenerPacientes(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaPaciente: obtenerPacientes: Inicio.");
        Vector vPacientes = new Vector();
        try
        {
            obtenerInformacionPantalla(unaConeccion);
            vPacientes = Paciente.getPacientes(getRFC(), getNombrePaciente(), getApellido1(), getApellido2(), getFechaNac());
            setListaPacientes(vPacientes);
        }
        catch(SQLException sqle)
        {
            LogFile.log("BusquedaPaciente: obtenerPacientes: SqlException " + sqle);
        }
        catch(Exception e)
        {
            LogFile.log("BusquedaPaciente: obtenerPacientes: Exception: " + e);
        }
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        try
        {
            if(unaConeccion.existsParamValue("btbuscarlupa"))
            {
                if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("inicial"))
                    return Login.getURL();
                if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("verEpisodios"))
                {
                    View busquedaEpisodios = BusquedaEpisodios.getView(unaConeccion);
                    setearPacientes((BusquedaEpisodios)busquedaEpisodios, unaConeccion);
                    setearEpisodios((BusquedaEpisodios)busquedaEpisodios, unaConeccion);
                    unaConeccion.putInSession("busquedaEpisodios", ((Object) (busquedaEpisodios)));
                    return busquedaEpisodios.URL();
                }
                if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("verMenu"))
                {
                    View busquedaEpisodios = BusquedaEpisodios.getView(unaConeccion);
                    setearPacientes((BusquedaEpisodios)busquedaEpisodios, unaConeccion);
                    unaConeccion.putInSession("busquedaEpisodios", ((Object) (busquedaEpisodios)));
                    LogFile.log("BusquedaMenu: obtenerURLDestino: Inicio ");
                    return "/jsp/menu.jsp";
                }
                if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("BuscarRapida"))
                {
                    LogFile.log("ServletSSI: procesarRequerimiento: existe Buscar.");
                    try
                    {
                        LogFile.log("BusquedaPaciente: obtenerURLDestino: llama a obtenerPacientes.");
                        obtenerPacientes(unaConeccion);
                        return getURL();
                    }
                    catch(Exception ex)
                    {
                        LogFile.log("BusquedaPaciente: obtenerURLDestino: Me pinche!" + ex);
                    }
                }
                if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("Buscar"))
                {
                    LogFile.log("ServletSSI: procesarRequerimiento: existe Buscar.");
                    try
                    {
                        LogFile.log("BusquedaPaciente: obtenerURLDestino: llama a obtenerPacientes.");
                        obtenerPacientes(unaConeccion);
                        return getURLResultados();
                    }
                    catch(Exception ex)
                    {
                        LogFile.log("BusquedaPaciente: obtenerURLDestino: Me pinche!" + ex);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            LogFile.log("BusquedaPaciente: obtenerURLDestino: Cojonudo pinchazo!" + ex);
        }
        LogFile.log("BusquedaPaciente: obtenerURLDestino: fin & url " + getURL());
        return getURL();
    }

    public void setApellido1(String _apellido1)
    {
        apellido1 = _apellido1;
    }

    public void setApellido2(String _apellido2)
    {
        apellido2 = _apellido2;
    }

    private void setearEpisodios(BusquedaEpisodios busquedaEpisodios, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaPaciente: setearEpisodios: Inicio");
        busquedaEpisodios.obtenerEpisodios(unaConeccion);
    }

    public String setearPacienteNovahis(ConeccionHTTP unaConeccion)
    {
        String idNovahis = "";
        try
        {
            if(!unaConeccion.getSingleParamValue("checkNovahis").equals("cero"))
            {
                LogFile.log("BusquedaPaciente: setearPacientes: busco por Novahis.");
                idNovahis = unaConeccion.getSingleParamValue("checkNovahis");
            }
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaPaciente: setearPacientes: sale en Novahis " + exc);
        }
        return idNovahis;
    }

    private void setearPacientes(BusquedaEpisodios busquedaEpisodios, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaPaciente: setearPacientes: Inicio");
        Vector vectorPacientes = new Vector();
        String idNovahis = setearPacienteNovahis(unaConeccion);
        String idSiapwin = setearPacienteSiapwin(unaConeccion);
        LogFile.log("idNovahis: " + idNovahis + " idSiapwin: " + idSiapwin);
        if(!idNovahis.trim().equals(""))
        {
            PacienteNovahis pNovahis = new PacienteNovahis();
            pNovahis.codigoCliente(idNovahis);
            vectorPacientes.addElement(((Object) (pNovahis)));
        }
        if(!idSiapwin.trim().equals(""))
        {
            PacienteSiapwin pSiapwin = new PacienteSiapwin();
            pSiapwin.codigoCliente(idSiapwin);
            vectorPacientes.addElement(((Object) (pSiapwin)));
        }
        busquedaEpisodios.setPacientes(vectorPacientes);
        LogFile.log("BusquedaPaciente: setearPacientes: Fin.");
    }

    public String setearPacienteSiapwin(ConeccionHTTP unaConeccion)
    {
        String idSiapwin = "";
        try
        {
            if(!unaConeccion.getSingleParamValue("checkSiapwin").equals("cero"))
            {
                LogFile.log("BusquedaPaciente: setearPacientes: busco por Siapwin.");
                idSiapwin = unaConeccion.getSingleParamValue("checkSiapwin");
            }
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaPaciente: setearPacientes: sale en Siapwin " + exc);
        }
        return idSiapwin;
    }

    public void setFechaNac(String _fechaNac)
    {
        fechaNacimiento = _fechaNac;
    }

    public void setListaPacientes(Vector unaLista)
    {
        listaPacientes = unaLista;
    }

    public void setNombrePaciente(String _nombre)
    {
        nombre = _nombre;
    }

    public void setRFC(String _rfc)
    {
        rfc = _rfc;
    }

    public String URL()
    {
        return getURL();
    }

    public PacienteSalud buscarPaciente(String idPaciente)
    {
        Enumeration e = listaPacientes.elements();
        PacienteSalud unPaciente = null;
        while(e.hasMoreElements()) 
        {
            unPaciente = (PacienteSalud)e.nextElement();
            if(unPaciente.codigoCliente().equals(((Object) (idPaciente))))
                return unPaciente;
        }
        LogFile.log("BusquedaPaciente:No se encontr\363 paciente con id =" + idPaciente);
        return unPaciente;
    }

}
