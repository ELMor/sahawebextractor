// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaActividades.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.model.salud.Actividad;
import com.ssi.model.salud.EncuentroSalud;
import com.ssi.model.salud.SistemaSalud;
import com.ssi.util.LogFile;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.bean.salud:
//            BusquedaEpisodios, BusquedaEncuentros, BusquedaPaciente

public class BusquedaActividades extends View
{

    private Vector listaActividades;
    private Vector vEncuentros;

    public static View getView(ConeccionHTTP unaConeccion)
    {
        View newView = null;
        try
        {
            if(unaConeccion.existsObjectInSession("BusquedaActividades"))
            {
                newView = (View)unaConeccion.getObjectFromSession("BusquedaActividades");
            } else
            {
                newView = ((View) (new BusquedaActividades()));
                unaConeccion.putObjectInSession("BusquedaActividades", ((Object) (newView)));
            }
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaActividades: getView: exc: " + exc);
        }
        return newView;
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaActividades: obtenerURLDestino: Inicio ");
        try
        {
            if(unaConeccion.existsParamValue("view") && unaConeccion.getSingleParamValue("view").equals("buscarPacientes"))
            {
                LogFile.log("BusquedaActividades: obtenerURLDestino: existe buscarPacientes.");
                try
                {
                    LogFile.log("BusquedaActividades: obtenerURLDestino: llama a obtenerActividades.");
                    return BusquedaPaciente.getURL();
                }
                catch(Exception ex)
                {
                    LogFile.log("BusquedaActividades: obtenerURLDestino: Me pinche!" + ex);
                }
            }
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaActividades: obtenerURLDestino: Excepcion = " + exc);
        }
        LogFile.log("BusquedaActividades: obtenerURLDestino: Fin ");
        return getURL();
    }

    public static String getURL()
    {
        return "/jsp/actividades.jsp";
    }

    public String URL()
    {
        return "/jsp/actividades.jsp";
    }

    public String getEncuentro()
    {
        return (String)vEncuentros.elementAt(0);
    }

    public Vector getListaActividades()
    {
        return listaActividades;
    }

    public void setListaActividades(Vector unaLista)
    {
        listaActividades = unaLista;
    }

    public void setearEncuentros(Vector encuentros)
    {
        vEncuentros = encuentros;
    }

    public void obtenerTodasLasActividades(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaActividades: obtenerTodasLasActividades: Inicio.");
        listaActividades = obtenerActividades(unaConeccion);
    }

    public Vector obtenerActividades()
    {
        Vector vActividades = new Vector();
        try
        {
            String strSistema = ((EncuentroSalud)vEncuentros.elementAt(0)).sistema().nombreSistema();
            LogFile.log("BusquedaActividades: obtenerActividades() para el encuentro: " + getEncuentro() + " y el sistema: " + strSistema);
            vActividades = Actividad.getActividades(getEncuentro(), strSistema);
            setListaActividades(vActividades);
        }
        catch(Exception _ex)
        {
            LogFile.log("BusquedaActividades: obtenerActividades: Excepcion");
        }
        return vActividades;
    }

    public void obtenerInformacionPantalla(ConeccionHTTP coneccionhttp)
    {
    }

    public Vector obtenerActividades(ConeccionHTTP unaConeccion)
    {
        Vector vActividades = new Vector();
        try
        {
            if(!unaConeccion.existsParamValue("sistema"))
                LogFile.log("BusquedaActividades:obtenerActividades No encuentra el parametro sistema");
            String sistema = unaConeccion.getSingleParamValue("sistema");
            LogFile.log("BusquedaActividades:obtenerActividades del sistema=" + sistema);
            vActividades = Actividad.getActividades(getEncuentro(), sistema);
            setListaActividades(vActividades);
        }
        catch(Exception _ex)
        {
            LogFile.log("BusquedaActividades: obtenerActividades: Excepcion");
        }
        return vActividades;
    }

    public Vector obtenerActividades(Vector pacientes, ConeccionHTTP unaConeccion)
    {
        Vector vEpisodios = null;
        try
        {
            vEpisodios = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getEpisodios();
        }
        catch(Exception e)
        {
            LogFile.log(e);
        }
        View busquedaEncuentros = BusquedaEncuentros.getView(unaConeccion);
        ((BusquedaEncuentros)busquedaEncuentros).setearEpisodios(vEpisodios);
        ((BusquedaEncuentros)busquedaEncuentros).obtenerTodosLosEncuentros(pacientes, unaConeccion);
        Vector vectorEncuentros = ((BusquedaEncuentros)busquedaEncuentros).getListaEncuentros();
        Enumeration e = vectorEncuentros.elements();
        EncuentroSalud encuentro = null;
        String sistema = null;
        String idEncuentro = null;
        StringBuffer restriccionNovahis = new StringBuffer(" WHERE ");
        StringBuffer restriccionSiapwin = new StringBuffer(" WHERE ");
        int flagNovahis = 0;
        int flagSiapwin = 0;
        while(e.hasMoreElements()) 
        {
            encuentro = (EncuentroSalud)e.nextElement();
            idEncuentro = encuentro.idEncuentro();
            sistema = encuentro.sistema().nombreSistema();
            if(sistema.equals("SistemaNovahis"))
            {
                if(flagNovahis == 0)
                    restriccionNovahis.append("ve_en_ref = '" + idEncuentro + "'");
                else
                    restriccionNovahis.append(" OR ve_en_ref = '" + idEncuentro + "'");
                flagNovahis++;
            }
            if(sistema.equals("SistemaSiapwin"))
            {
                if(flagSiapwin == 0)
                    restriccionSiapwin.append("ve_en_ref = '" + idEncuentro + "'");
                else
                    restriccionSiapwin.append(" OR ve_en_ref = '" + idEncuentro + "'");
                flagSiapwin++;
            }
        }
        Vector vActividades = null;
        try
        {
            LogFile.log("Llamando a Actividad.getActividades::novahis=" + restriccionNovahis.toString() + " siapwin:" + restriccionSiapwin.toString());
            vActividades = Actividad.getActividades(pacientes, restriccionNovahis.toString(), restriccionSiapwin.toString());
            setListaActividades(vActividades);
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaActividades: obtenerActividades: Excepcion");
            LogFile.log(exc);
        }
        return vActividades;
    }

    public void obtenerTodasLasActividades(Vector pacientes, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaActividades: obtenerTodasLasActividades: Inicio.");
        listaActividades = obtenerActividades(pacientes, unaConeccion);
    }

    public BusquedaActividades()
    {
    }
}
