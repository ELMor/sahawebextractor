// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   NoLog.java

package com.ssi.log.model;


// Referenced classes of package com.ssi.log.model:
//            ApplicationLog, LogInterface

public class NoLog extends ApplicationLog
    implements LogInterface
{

    public void clearLog()
    {
    }

    public void cr()
    {
    }

    public void crTab()
    {
    }

    public String loggedText()
    {
        return "No log enabled...";
    }

    public void logString(String s)
    {
    }

    public void logStringCR(String s)
    {
    }

    public void setLog(String s)
    {
    }

    public void tab()
    {
    }

    public NoLog()
    {
    }
}
