// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ApplicationLog.java

package com.ssi.log.model;

import java.util.Observable;

// Referenced classes of package com.ssi.log.model:
//            LogInterface

public abstract class ApplicationLog extends Observable
    implements LogInterface
{

    public ApplicationLog()
    {
    }

    public abstract void clearLog();

    public abstract void cr();

    public abstract void crTab();

    public abstract String loggedText();

    public abstract void logString(String s);

    public abstract void logStringCR(String s);

    public abstract void setLog(String s);

    public abstract void tab();
}
