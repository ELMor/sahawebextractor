// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConeccionHTTPSalud.java

package com.ssi.http.salud;

import com.ssi.http.ConeccionHTTP;
import com.ssi.persistence.model.User;
import com.ssi.util.LogFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConeccionHTTPSalud extends ConeccionHTTP
{

    public ConeccionHTTPSalud(HttpServletRequest esteRequest, HttpServletResponse esteResponse)
        throws Exception
    {
        super(esteRequest, esteResponse);
    }

    public void setLoggedUser(User _usuario)
    {
        LogFile.log("SESS " + ((ConeccionHTTP)this).getSession());
        ((ConeccionHTTP)this).putInSession("loggedUser", ((Object) (_usuario)));
    }

    public User getLoggedUser()
        throws Exception
    {
        return (User)((ConeccionHTTP)this).getFromSession("loggedUser");
    }

    public boolean isUserLogged()
    {
        boolean exists = false;
        try
        {
            getLoggedUser();
            exists = true;
        }
        catch(Exception _ex) { }
        return exists;
    }
}
