// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConeccionHTTP.java

package com.ssi.http;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import javax.servlet.http.HttpUtils;

public abstract class ConeccionHTTP
{

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    public void putObjectInSession(String name, Object obj)
    {
        getSession().putValue(name, obj);
    }

    public Object getFromSession(String name)
        throws Exception
    {
        return getObjectFromSession(name);
    }

    public ConeccionHTTP(HttpServletRequest esteRequest, HttpServletResponse esteResponse)
        throws Exception
    {
        setRequest(esteRequest);
        setResponse(esteResponse);
        initializeSesison();
    }

    public void initializeSesison()
        throws Exception
    {
        HttpSession sess = getRequest().getSession(false);
        if(sess == null)
            if(requestEstaVacio())
                sess = getRequest().getSession(true);
            else
                throw new Exception("Su session expir\363 o su navegador no est\341 aceptando cookies.");
        setSession(sess);
    }

    public HttpSession getSession()
    {
        return session;
    }

    public HttpServletRequest getRequest()
    {
        return request;
    }

    public HttpServletResponse getResponse()
    {
        return response;
    }

    public void setRequest(HttpServletRequest req)
    {
        request = req;
    }

    public void setResponse(HttpServletResponse resp)
    {
        response = resp;
    }

    public void setSession(HttpSession ses)
    {
        session = ses;
    }

    public String getSingleParamValue(String param)
        throws Exception
    {
        String valorParam = null;
        if(existsParamValue(param))
            valorParam = ((ServletRequest) (request)).getParameterValues(param)[0];
        return valorParam;
    }

    public String[] getMultipleParamValues(String param)
        throws Exception
    {
        String valores[] = null;
        if(existsParamValue(param))
            valores = ((ServletRequest) (request)).getParameterValues(param);
        return valores;
    }

    public boolean existsParamValue(String param)
    {
        return ((ServletRequest) (request)).getParameterValues(param) != null;
    }

    public Enumeration getParameterNames()
    {
        return ((ServletRequest) (request)).getParameterNames();
    }

    public String getServletPath()
    {
        HttpServletRequest request = getRequest();
        return request.getServletPath();
    }

    public String getRequestURL()
    {
        HttpServletRequest request = getRequest();
        StringBuffer url = HttpUtils.getRequestURL(request);
        return url.toString();
    }

    public boolean requestEstaVacio()
    {
        return !((ServletRequest) (getRequest())).getParameterNames().hasMoreElements();
    }

    public void loginSession()
    {
        logoutSession();
        HttpSession sess = getRequest().getSession(true);
        setSession(sess);
    }

    public void removeAllDataFromSession()
    {
        HttpSession sess = getSession();
        String theValueNames[] = sess.getValueNames();
        int numberOfValues = theValueNames.length;
        for(int i = 0; i < numberOfValues; i++)
        {
            String name = theValueNames[i];
            sess.removeValue(name);
        }

    }

    public void logoutSession()
    {
        HttpSession sess = getSession();
        if(sess != null)
        {
            removeAllDataFromSession();
            sess.invalidate();
        }
    }

    public void putInSession(String name, Object obj)
    {
        putObjectInSession(name, obj);
    }

    public Object getObjectFromSession(String name)
        throws Exception
    {
        Object valor = getSession().getValue(name);
        if(valor == null)
            throw new Exception("El siguiente campo no existe en la Session: " + name);
        else
            return valor;
    }

    public void removeFromSession(String name)
    {
        getSession().removeValue(name);
    }

    public ServletOutputStream getServletOutputStream()
        throws IOException
    {
        ((ServletResponse) (getResponse())).setContentType("text/html");
        return ((ServletResponse) (getResponse())).getOutputStream();
    }

    public String getSessionId()
    {
        return getSession().getId();
    }

    public Enumeration getAllSessionIds()
    {
        return getSession().getSessionContext().getIds();
    }

    public boolean existsObjectInSession(String _sObjectName)
    {
        boolean exists = true;
        try
        {
            getFromSession(_sObjectName);
        }
        catch(Exception _ex)
        {
            exists = false;
        }
        return exists;
    }

    private void print(PrintWriter out, String name, String value)
        throws IOException
    {
        out.print(" " + name + ": ");
        out.println(value != null ? value : "&lt;none&gt;");
    }

    private void print(PrintWriter out, String name, int value)
        throws IOException
    {
        out.print(" " + name + ": ");
        if(value == -1)
            out.println("&lt;none&gt;");
        else
            out.println(value);
    }

    public int getCantidadParametros()
    {
        int numero = 0;
        for(Enumeration e = ((ServletRequest) (request)).getParameterNames(); e.hasMoreElements();)
            numero++;

        return numero;
    }
}
