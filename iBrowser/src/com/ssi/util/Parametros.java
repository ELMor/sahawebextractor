// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   Parametros.java

package com.ssi.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.ssi.util:
//            LogFile

public class Parametros
{
    public static Properties settings=new Properties();

    public static String sinDatos(){
      return settings.getProperty("sinDatos","s/d");
    }

    public static String imgPath(){
      return settings.getProperty("imgPath","/Salud/jsp/pics/");
    }

    public static void inicializarVariablesStaticas()
    {
        try
        {
            readIniFile();
        }
        catch(Exception e)
        {
            ((Throwable) (e)).printStackTrace();
            LogFile.log(e);
        }
    }

    public static void readIniFile()
    {
        try
        {
            FileInputStream fis = new FileInputStream("iBrowser.properties");
            settings.load(((java.io.InputStream) (fis)));
        }
        catch(Exception e)
        {
            LogFile.log(e);
        }
    }

    public static String time(String horario)
    {
        if(horario == null)
            return settings.getProperty("sinDatos");
        else
            return horario.substring(0, 16);
    }

}
