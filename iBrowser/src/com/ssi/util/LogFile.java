// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   LogFile.java

package com.ssi.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

// Referenced classes of package com.ssi.util:
//            Parametros

public class LogFile
{

    static PrintStream ps = null;

    public LogFile(String filename)
    {
        if(Parametros.settings.getProperty("LOG_TO_FILE","false").equals("true"))
            openLogFile(filename);
    }

    public static void close()
    {
        if(Parametros.settings.getProperty("LOG_TO_FILE","false").equals("true") && ps != null)
        {
            ps.println("------- C L O S I N G    L O G    F I L E -------");
            ps.println("current date is:" + (new Date()).toString());
            ps.println(" ");
            ps.flush();
            ps.close();
            ps = null;
        }
    }

    public static void openLogFile(String filename)
    {
        openLogFile(filename, false);
    }

    public static void openLogFile(String filename, boolean append)
    {
        if(Parametros.settings.getProperty("LOG_TO_FILE","false").equals("true") && ps == null)
            try
            {
                FileOutputStream fos = new FileOutputStream(filename, append);
                ps = new PrintStream(((java.io.OutputStream) (fos)));
                ps.println("------- S T A R T I N G    L O G    F I L E -------");
                ps.println("current date is:" + (new Date()).toString());
                ps.println(" ");
            }
            catch(IOException ioe)
            {
                System.out.println(" !!! ERROR - LogFile.create()");
                System.out.println(" " + ioe);
            }
    }

    public static void log(String message)
    {
        doLogging(message, "");
    }

    private static void doLogging(String message, String moduleName)
    {
        GregorianCalendar gc = new GregorianCalendar();
        String timeString = ((Calendar) (gc)).get(11) + ":" + ((Calendar) (gc)).get(12) + ":" + ((Calendar) (gc)).get(13);
        if(Parametros.settings.getProperty("LOG_TO_FILE","false").equals("true") && ps != null)
            ps.println(moduleName + " " + timeString + "| " + message);
        if(Parametros.settings.getProperty("LOG_TO_SCREEN","false").equals("true"))
            System.out.println(message);
    }

    public static void log(Exception e)
    {
        doLogging(e);
    }

    private static void doLogging(Exception e)
    {
        GregorianCalendar gc = new GregorianCalendar();
        String timeString = ((Calendar) (gc)).get(11) + ":" + ((Calendar) (gc)).get(12) + ":" + ((Calendar) (gc)).get(13);
        if(Parametros.settings.getProperty("LOG_TO_FILE","false").equals("true")
           && ps != null)
        {
            ps.println(timeString + "| La siguiente Exception:");
            ps.println(getExceptionDetailString(e));
        }
        if(Parametros.settings.getProperty("LOG_TO_SCREEN","false").equals("true"))
            ((Throwable) (e)).printStackTrace();
    }

    private static String getMessageForSQLException(SQLException sqlex)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(((java.io.Writer) (sw)));
        for(SQLException nextException = new SQLException(); nextException != null; nextException = sqlex.getNextException())
        {
            pw.println(((Throwable) (sqlex)).toString());
            pw.println("Error Code: " + sqlex.getErrorCode());
            pw.println("SQL State: " + sqlex.getSQLState());
        }

        pw.println("Stack Trace: ");
        ((Throwable) (sqlex)).printStackTrace(pw);
        return sw.toString();
    }

    private static String getExceptionDetailString(Exception e)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(((java.io.Writer) (sw)));
        SQLException sqlex = new SQLException();
        String mensaje = null;
        if(((Object) (sqlex)).getClass().isAssignableFrom(((Object) (e)).getClass()))
        {
            mensaje = getMessageForSQLException((SQLException)e);
        } else
        {
            ((Throwable) (e)).printStackTrace(pw);
            mensaje = sw.toString();
        }
        return mensaje;
    }

}
