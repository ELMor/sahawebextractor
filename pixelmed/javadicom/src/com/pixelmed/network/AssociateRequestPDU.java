/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.network;

import java.util.LinkedList;

/**
 * @author	dclunie
 */
class AssociateRequestPDU extends AssociateRequestAcceptPDU {
	private static final String identString = "@(#) $Header$";

	/**
	 * @param	calledAETitle
	 * @param	callingAETitle
	 * @param	implementationClassUID
	 * @param	implementationVersionName
	 * @param	ourMaximumLengthReceived	the maximum PDU length that we will offer to receive
	 * @param	presentationContexts
	 * @exception	DicomNetworkException
	 */
	public AssociateRequestPDU(String calledAETitle,String callingAETitle, String implementationClassUID, String implementationVersionName,
			int ourMaximumLengthReceived,
			LinkedList presentationContexts) throws DicomNetworkException {
		super(0x01,calledAETitle,callingAETitle,implementationClassUID,implementationVersionName,ourMaximumLengthReceived,presentationContexts);
	}

	/**
	 * @param	pdu
	 * @exception	DicomNetworkException
	 */
	public AssociateRequestPDU(byte[] pdu) throws DicomNetworkException {
		super(pdu);
		if (pduType != 0x01) throw new DicomNetworkException("Unexpected PDU type 0x"+Integer.toHexString(pduType)+" when expecting A-ASSOCIATE-RQ");
	}
}



