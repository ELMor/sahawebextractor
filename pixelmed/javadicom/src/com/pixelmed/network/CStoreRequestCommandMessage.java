/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.network;

import com.pixelmed.utils.*;
import com.pixelmed.dicom.*;

import java.util.LinkedList;
import java.io.*;

/**
 * @author	dclunie
 */
class CStoreRequestCommandMessage implements CommandMessage {
	private static final String identString = "@(#) $Header$";

	private byte bytes[];

	private static final AttributeTag groupLengthTag = new AttributeTag(0x0000,0x0000);
	private int groupLength;
	private String affectedSOPClassUID;		// unpadded
	private int commandField;
	private int messageID;
	private int priority;
	private String affectedSOPInstanceUID;		// unpadded
	
	/**
	 * @param	list
	 * @exception	IOException
	 * @exception	DicomException
	 */
	public CStoreRequestCommandMessage(AttributeList list) throws DicomException, IOException {
		           groupLength = Attribute.getSingleIntegerValueOrDefault(list,groupLengthTag,0xffff);
		   affectedSOPClassUID = Attribute.getSingleStringValueOrNull    (list,TagFromName.AffectedSOPClassUID);
		          commandField = Attribute.getSingleIntegerValueOrDefault(list,TagFromName.CommandField,0xffff);
		             messageID = Attribute.getSingleIntegerValueOrDefault(list,TagFromName.MessageID,0xffff);
		              priority = Attribute.getSingleIntegerValueOrDefault(list,TagFromName.Priority,0xffff);
		affectedSOPInstanceUID = Attribute.getSingleStringValueOrNull    (list,TagFromName.AffectedSOPInstanceUID);
	}
	
	/**
	 * @param	affectedSOPClassUID
	 * @param	affectedSOPInstanceUID
	 * @exception	IOException
	 * @exception	DicomException
	 */
	public CStoreRequestCommandMessage(String affectedSOPClassUID,String affectedSOPInstanceUID) throws DicomException, IOException {
		this.affectedSOPClassUID=affectedSOPClassUID;
		this.affectedSOPInstanceUID=affectedSOPInstanceUID;
		
		   commandField = 0x0001;	// C-STORE-RQ
		      messageID = 0x0001;
		       priority = 0x0000;	// MEDIUM
		int dataSetType = 0x0001;	// anything other than 0x0101 (none), since a C-STORE-RQ always has a data set
		
		AttributeList list = new AttributeList();		
		{ AttributeTag t = groupLengthTag;                     Attribute a = new UnsignedLongAttribute(t);     a.addValue(0);                      list.put(t,a); }
		{ AttributeTag t = TagFromName.AffectedSOPClassUID;    Attribute a = new UniqueIdentifierAttribute(t); a.addValue(affectedSOPClassUID);    list.put(t,a); }
		{ AttributeTag t = TagFromName.CommandField;           Attribute a = new UnsignedShortAttribute(t);    a.addValue(commandField);           list.put(t,a); }
		{ AttributeTag t = TagFromName.MessageID;              Attribute a = new UnsignedShortAttribute(t);    a.addValue(messageID);              list.put(t,a); }
		{ AttributeTag t = TagFromName.Priority;               Attribute a = new UnsignedShortAttribute(t);    a.addValue(priority);               list.put(t,a); }
		{ AttributeTag t = TagFromName.DataSetType;            Attribute a = new UnsignedShortAttribute(t);    a.addValue(dataSetType);            list.put(t,a); }
		{ AttributeTag t = TagFromName.AffectedSOPInstanceUID; Attribute a = new UniqueIdentifierAttribute(t); a.addValue(affectedSOPInstanceUID); list.put(t,a); }

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		DicomOutputStream dout = new DicomOutputStream(bout,null/* no meta-header */,TransferSyntax.ImplicitVRLittleEndian);
		list.write(dout);
		bytes = bout.toByteArray();

		groupLength = bytes.length-12;
		bytes[8]=(byte)groupLength;					// little endian
		bytes[9]=(byte)(groupLength>>8);
		bytes[10]=(byte)(groupLength>>16);
		bytes[11]=(byte)(groupLength>>24);
//System.err.println("CStoreRequestCommandMessage: bytes="+HexDump.dump(bytes));
	}
	
	/***/
	public int getGroupLength()			{ return groupLength; }
	/***/
	public String getAffectedSOPClassUID()		{ return affectedSOPClassUID; }		// unpadded
	/***/
	public int getCommandField()			{ return commandField; }
	/***/
	public int getMessageID()			{ return messageID; }
	/***/
	public int getPriority()			{ return priority; }
	/***/
	public String getAffectedSOPInstanceUID()	{ return affectedSOPInstanceUID; }		// unpadded

	/***/
	public byte[] getBytes() { return bytes; }
}
