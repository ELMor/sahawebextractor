/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.network;

import com.pixelmed.dicom.*;

/**
 * <p>This class provides a mechanism to process each identifier response of a C-FIND as it is received.</p>
 *
 * <p>Typically a private sub-class would be declared and instantiated with
 * overriding methods to do something useful with the identifier, rather than
 * the default behavior which is just to dump it to stderr.</p>
 *
 * @see com.pixelmed.network.FindSOPClassSCU
 *
 * @author	dclunie
 */
public class IdentifierHandler {
	private static final String identString = "@(#) $Header$";

	/**
	 * <p>Called when a response identifier has been received.</p>
	 *
	 * @param	identifier	the list of attributes received
	 */
	public void doSomethingWithIdentifier(AttributeList identifier) {
		System.err.println("IdentifierHandler.doSomethingWithIdentifier():");
		System.err.print(identifier);
	}
}
