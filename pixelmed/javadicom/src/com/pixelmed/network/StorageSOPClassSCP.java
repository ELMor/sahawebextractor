/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.network;

import com.pixelmed.utils.*;
import com.pixelmed.dicom.*;

import java.util.ListIterator;
import java.util.LinkedList;
import java.io.*;
import java.net.Socket;

/**
 * <p>This class implements the SCP role of SOP Classes of the Storage Service Class,
 * and the Verification SOP Class.</p>
 *
 * <p>The class has a constructor and a <code>run()</code> method. The
 * constructor is passed a socket on which has been received a transport
 * connection open indication. The <code>run()</code> method waits for an association to be initiated
 * (i.e. acts as an association acceptor), then waits for storage or
 * verification commands, storing data sets in Part 10 files in the specified folder.</p>
 *
 * <p>Debugging messages with a varying degree of verbosity can be activated.</p>
 *
 * <p>This class is not normally used directly, but rather is instantiated by the
 * {@link com.pixelmed.network.StorageSOPClassSCPDispatcher StorageSOPClassSCPDispatcher},
 * which takes care of listening for transport connection open indications, and
 * creates new threads and starts them to handle each incoming association request.</p>
 *
 * @see com.pixelmed.network.StorageSOPClassSCPDispatcher
 *
 * @author	dclunie
 */
public class StorageSOPClassSCP extends SOPClass implements Runnable {

	/***/
	private static final String identString = "@(#) $Header$";

	/***/
	private class CStoreReceivedPDUHandler extends ReceivedDataHandler {
		/***/
		private byte[] commandReceived;
		/***/
		private AttributeList commandList;
		/***/
		private OutputStream out;
		/***/
		private CStoreRequestCommandMessage csrq;
		/***/
		private CEchoRequestCommandMessage cerq;
		/***/
		private byte[] response;
		/***/
		private byte presentationContextIDUsed;
		//private Association association;
		/***/
		private String receivedFileName;
		/***/
		private File savedImagesFolder;
		
		/**
		 * @exception	IOException
		 * @exception	DicomException
		 */
		private void buildCEchoResponse() throws DicomException, IOException {
			response = new CEchoResponseCommandMessage(
				cerq.getAffectedSOPClassUID(),
				cerq.getMessageID(),
				0x0000				// success status
				).getBytes();
		}
		
		/**
		 * @exception	IOException
		 * @exception	DicomException
		 */
		private void buildCStoreResponse() throws DicomException, IOException {
			response = new CStoreResponseCommandMessage(
				csrq.getAffectedSOPClassUID(),
				csrq.getAffectedSOPInstanceUID(),
				csrq.getMessageID(),
				0x0000				// success status
				).getBytes();
		}
		
		/**
		 * @param	savedImagesFolder	null if we do not want to actually save received data (i.e., we want to discard it for testing)
		 * @param	debugLevel
		 */
		public CStoreReceivedPDUHandler(File savedImagesFolder,int debugLevel) {
			super(debugLevel);
			commandReceived=null;
			commandList=null;
			out=null;
			csrq=null;
			receivedFileName=null;
			this.savedImagesFolder=savedImagesFolder;
		}

		/**
		 * @param	pdata
		 * @param	association
		 * @exception	IOException
		 * @exception	DicomException
		 * @exception	DicomNetworkException
		 */
		public void sendPDataIndication(PDataPDU pdata,Association association) throws DicomNetworkException, DicomException, IOException {
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler: sendPDataIndication()");
if (debugLevel > 2) super.dumpPDVList(pdata.getPDVList());
if (debugLevel > 2) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler: finished dumping PDV list from PDU");
			// append to command ...
			LinkedList pdvList = pdata.getPDVList();
			ListIterator i = pdvList.listIterator();
			while (i.hasNext()) {
				PresentationDataValue pdv = (PresentationDataValue)i.next();
				presentationContextIDUsed = pdv.getPresentationContextID();
				if (pdv.isCommand()) {
					receivedFileName=null;
					commandReceived=ByteArray.concatenate(commandReceived,pdv.getValue());	// handles null cases
					if (pdv.isLastFragment()) {
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler: last fragment of command seen");
if (debugLevel > 0) System.err.println(HexDump.dump(commandReceived));
						commandList = new AttributeList();
						commandList.read(new DicomInputStream(new ByteArrayInputStream(commandReceived),TransferSyntax.Default,false));
if (debugLevel > 0) System.err.print(commandList);
						int command = Attribute.getSingleIntegerValueOrDefault(commandList,TagFromName.CommandField,0xffff);
						if (command == 0x0030) {	// C-ECHO-RQ
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler: C-ECHO-RQ");
							cerq = new CEchoRequestCommandMessage(commandList);
							buildCEchoResponse();
							setDone(true);
						}
						else if (command == 0x0001) {	// C-STORE-RQ
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler: C-STORE-RQ");
							csrq = new CStoreRequestCommandMessage(commandList);
						}
						else {
							throw new DicomNetworkException("Unexpected command 0x"+Integer.toHexString(command));
						}
						// 2004/06/08 DAC removed break that was here to resolve [bugs.mrmf] (000113) StorageSCP failing when data followed command in same PDU
if (debugLevel > 0 && i.hasNext()) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler: Data after command in same PDU");
					}
				}
				else {
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler.sendPDataIndication(): Storing data fragment");
					if (out == null && savedImagesFolder != null) {		// lazy opening
						FileMetaInformation fmi = new FileMetaInformation(
							csrq.getAffectedSOPClassUID(),
							csrq.getAffectedSOPInstanceUID(),
							association.getTransferSyntaxForPresentationContextID(presentationContextIDUsed),
							association.getCallingAETitle());
						receivedFileName=new File(savedImagesFolder,csrq.getAffectedSOPInstanceUID()).getPath();
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler.sendPDataIndication(): Receiving and storing "+receivedFileName);
						out = new BufferedOutputStream(new FileOutputStream(receivedFileName));
						DicomOutputStream dout = new DicomOutputStream(out,TransferSyntax.ExplicitVRLittleEndian,null);
						fmi.getAttributeList().write(dout);
						dout.flush();
					}
					if (out != null) {
						out.write(pdv.getValue());
					}
					if (pdv.isLastFragment()) {
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.CStoreReceivedPDUHandler.sendPDataIndication(): Finished storing data");
						if (out != null) {
							out.close();
							out=null;
						}
						buildCStoreResponse();
						setDone(true);
					}
				}
			}
		}
		
		/***/
		public AttributeList getCommandList() { return commandList; }
		/***/
		public byte[] getResponse() { return response; }
		/***/
		public byte getPresentationContextIDUsed() { return presentationContextIDUsed; }
		/***/
		public String getReceivedFileName() { return receivedFileName; }
	}
	
	/**
	 * @param	association
	 * @exception	IOException
	 * @exception	AReleaseException
	 * @exception	DicomException
	 * @exception	DicomNetworkException
	 */
	private void receiveOneSOPInstance(Association association) throws AReleaseException, DicomNetworkException, DicomException, IOException {
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.receiveOneSOPInstance(): start");
		CStoreReceivedPDUHandler receivedPDUHandler = new CStoreReceivedPDUHandler(savedImagesFolder,debugLevel);
		association.setReceivedDataHandler(receivedPDUHandler);
		association.waitForPDataPDUsUntilHandlerReportsDone();	// throws AReleaseException if release request instead
		{
			String receivedFileName=receivedPDUHandler.getReceivedFileName();	// null if C-ECHO
			if (receivedFileName != null) {
				byte pcid = receivedPDUHandler.getPresentationContextIDUsed();
				String ts = association.getTransferSyntaxForPresentationContextID(pcid);
				String callingAE = association.getCallingAETitle();
				receivedObjectHandler.sendReceivedObjectIndication(receivedFileName,ts,callingAE);
			}
		}
		association.send(receivedPDUHandler.getPresentationContextIDUsed(),receivedPDUHandler.getResponse(),null);
if (debugLevel > 0) System.err.println("StorageSOPClassSCP.receiveOneSOPInstance(): end");
	}
	
	/***/
	private Socket socket;
	/***/
	private String calledAETitle;
	/***/
	private int ourMaximumLengthReceived;
	/***/
	private int socketReceiveBufferSize;
	/***/
	private int socketSendBufferSize;
	/***/
	private File savedImagesFolder;
	/***/
	private ReceivedObjectHandler receivedObjectHandler;
	/***/
	private int debugLevel;
	
	/**
	 * <p>Construct an instance of an association acceptor and storage and verification SCP
	 * to be passed to the constructor of a thread that will be started.</p>
	 *
	 * @param	socket				the socket on which a transport connection open indication has been received
	 * @param	calledAETitle			our AE Title
	 * @param	ourMaximumLengthReceived	the maximum PDU length that we will offer to receive
	 * @param	socketReceiveBufferSize		the TCP socket receive buffer size to set (if possible), 0 means leave at the default
	 * @param	socketSendBufferSize		the TCP socket send buffer size to set (if possible), 0 means leave at the default
	 * @param	savedImagesFolder		the folder in which to store received data sets (may be null, to ignore received data for testing)
	 * @param	receivedObjectHandler		the handler to call after each data set has been received and stored
	 * @param	debugLevel			zero for no debugging messages, higher values more verbose messages
	 * @exception	IOException
	 * @exception	DicomException
	 * @exception	DicomNetworkException
	 */
	public StorageSOPClassSCP(Socket socket, String calledAETitle,
			int ourMaximumLengthReceived,int socketReceiveBufferSize,int socketSendBufferSize,
			File savedImagesFolder,ReceivedObjectHandler receivedObjectHandler,
			int debugLevel) throws DicomNetworkException, DicomException, IOException {
//System.err.println("StorageSOPClassSCP()");
		this.socket=socket;
		this.calledAETitle=calledAETitle;
		this.ourMaximumLengthReceived=ourMaximumLengthReceived;
		this.socketReceiveBufferSize=socketReceiveBufferSize;
		this.socketSendBufferSize=socketSendBufferSize;
		this.savedImagesFolder=savedImagesFolder;
		this.receivedObjectHandler=receivedObjectHandler;
		this.debugLevel=debugLevel;
	}
	
	/**
	 * <p>Construct an instance of an association acceptor and storage and verification SCP
	 * to be passed to the constructor of a thread that will be started.</p>
	 *
	 * @param	socket			the socket on which a transport connection open indication has been received
	 * @param	calledAETitle		our AE Title
	 * @param	savedImagesFolder	the folder in which to store received data sets (may be null, to ignore received data for testing)
	 * @param	receivedObjectHandler	the handler to call after each data set has been received and stored
	 * @param	debugLevel		zero for no debugging messages, higher values more verbose messages
	 * @exception	IOException
	 * @exception	DicomException
	 * @exception	DicomNetworkException
	 */
	public StorageSOPClassSCP(Socket socket, String calledAETitle,File savedImagesFolder,ReceivedObjectHandler receivedObjectHandler,
			int debugLevel) throws DicomNetworkException, DicomException, IOException {
//System.err.println("StorageSOPClassSCP()");
		this.socket=socket;
		this.calledAETitle=calledAETitle;
		this.ourMaximumLengthReceived=AssociationFactory.getDefaultMaximumLengthReceived();
		this.socketReceiveBufferSize=AssociationFactory.getDefaultReceiveBufferSize();
		this.socketSendBufferSize=AssociationFactory.getDefaultSendBufferSize();
		this.savedImagesFolder=savedImagesFolder;
		this.receivedObjectHandler=receivedObjectHandler;
		this.debugLevel=debugLevel;
	}
	
	/**
	 * <p>Waits for an association to be initiated (acts as an association acceptor), then waits for storage or
	 * verification commands, storing data sets in Part 10 files in the specified folder, until the association
	 * is released or the transport connection closes.</p>
	 */
	public void run() {
//System.err.println("StorageSOPClassSCP.run()");
		try {
			Association association = AssociationFactory.createNewAssociation(socket,calledAETitle,
				ourMaximumLengthReceived,socketReceiveBufferSize,socketSendBufferSize,debugLevel);
if (debugLevel > 1) System.err.println(association);
			try {
				while (true) receiveOneSOPInstance(association);
			}
			catch (AReleaseException e) {
			}
		}
		catch (Exception e) {
			System.err.println(e);
			//e.printStackTrace(System.err);
		}
	}
}




