/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.network;

import com.pixelmed.utils.ByteArray;
import com.pixelmed.utils.HexDump;
import com.pixelmed.utils.StringUtilities;
import com.pixelmed.dicom.SOPClass;
import com.pixelmed.dicom.TransferSyntax;

import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.LinkedList;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;

/**
 * @author	dclunie
 */
class AssociationAcceptor extends Association {

	private static final String identString = "@(#) $Header$";

	/**
	 * Accepts an association on the supplied open transport connection.
	 *
	 * The default Implementation Class UID, Implementation Version and Maximum PDU Size
	 * of the toolkit are used.
	 *
	 * The open association is left in state 6 - Data Transfer.
	 *
	 * @param	socket				already open transport connection on which the association is to be accepted
	 * @param	calledAETitle			the AE Title of the local (our) end of the association
	 * @param	implementationClassUID		the Implementation Class UID of the local (our) end of the association supplied as a User Information Sub-item
	 * @param	implementationVersionName	the Implementation Class UID of the local (our) end of the association supplied as a User Information Sub-item
	 * @param	ourMaximumLengthReceived	the maximum PDU length that we will offer to receive
	 * @param	socketReceiveBufferSize		the TCP socket receive buffer size to set (if possible), 0 means leave at the default
	 * @param	socketSendBufferSize		the TCP socket send buffer size to set (if possible), 0 means leave at the default
	 * @param	debugLevel			0 for no debugging, > 0 for increasingly verbose debugging
	 * @exception	IOException
	 * @exception	DicomNetworkException		thrown for A-ABORT and A-P-ABORT indications
	 */
	protected AssociationAcceptor(Socket socket,String calledAETitle,String implementationClassUID,String implementationVersionName,
			int ourMaximumLengthReceived,int socketReceiveBufferSize,int socketSendBufferSize,
			int debugLevel) throws DicomNetworkException, IOException {
		super(debugLevel);
		this.socket=socket;
		this.calledAETitle=calledAETitle;
		callingAETitle=null;
		presentationContexts=null;

		try {
												// AE-5    - TP Connect Indication
												// State 2 - Transport connection open (Awaiting A-ASSOCIATE-RQ PDU)
			setSocketOptions(socket,ourMaximumLengthReceived,socketReceiveBufferSize,socketSendBufferSize,debugLevel);

			in = socket.getInputStream();
			out = socket.getOutputStream();

			byte[] startBuffer =  new byte[6];
			//in.read(startBuffer,0,6);	// block for type and length of PDU
			readInsistently(in,startBuffer,0,6,"type and length of PDU");
			int pduType = startBuffer[0]&0xff;
			int pduLength = ByteArray.bigEndianToUnsignedInt(startBuffer,2,4);

if (debugLevel > 1) System.err.println("Them: PDU Type: 0x"+Integer.toHexString(pduType)+" (length 0x"+Integer.toHexString(pduLength)+")");

			if (pduType == 0x01) {							//           - A-ASSOCIATE-RQ PDU
												// AE-6      - Stop ARTIM and send A-ASSOCIATE indication primitive
				AssociateRequestPDU arq = new AssociateRequestPDU(getRestOfPDU(in,startBuffer,pduLength));
if (debugLevel > 1) System.err.println("Them:\n"+arq);
				presentationContexts=arq.getRequestedPresentationContexts();
				maximumLengthReceived=arq.getMaximumLengthReceived();
				callingAETitle=StringUtilities.removeTrailingSpaces(arq.getCallingAETitle());
				if (!calledAETitle.equals(StringUtilities.removeTrailingSpaces(arq.getCalledAETitle()))) {
												//	     - Implicit A-ASSOCIATE response primitive reject
												// AE-8      - Send A-ASSOCIATE-RJ PDU
					AssociateRejectPDU arj = new AssociateRejectPDU(1,1,7);	// rejected permanent, user, called AE title not recognized
					out.write(arj.getBytes());
					out.flush();						// State 13
					
					// At this point AA-6, AA-7, AA-2, AR-5 or AA-7 could be needed,
					// however let's just close the connection and be done with it
					// without worrying about whether the other end is doing the same
					// or has sent a PDU that really should trigger us to send an A-ABORT first
					// and we don't have a timmer to stop
					socket.close();
					// No "indication" is defined in the standard here, but send our own to communicate rejection
					throw new DicomNetworkException("Called AE title requested ("+arq.getCalledAETitle()+") doesn't match ours ("+calledAETitle+") - rejecting association");
												// State 1   - Idle
				}
				else {
												//	     - Implicit A-ASSOCIATE response primitive accept
												// AE-7      - Send A-ASSOCIATE-AC PDU
if (debugLevel > 1) System.err.println("Presentation contexts requested:\n"+presentationContexts);
					presentationContexts=applyAbstractSyntaxSelectionPolicy(presentationContexts);				// must be called 1st
if (debugLevel > 1) System.err.println("Presentation contexts after applyAbstractSyntaxSelectionPolicy:\n"+presentationContexts);
					presentationContexts=applyTransferSyntaxSelectionPolicy(presentationContexts);				// must be called 2nd
if (debugLevel > 1) System.err.println("Presentation contexts after applyTransferSyntaxSelectionPolicy:\n"+presentationContexts);
					presentationContexts=applyExplicitTransferSyntaxPreferencePolicy(presentationContexts);	// must be called 3rd
if (debugLevel > 1) System.err.println("Presentation contexts after applyExplicitTransferSyntaxPreferencePolicy:\n"+presentationContexts);
				// we now have presentation contexts with 1 AS, 1TS if any accepted, and a result/reason
					LinkedList presentationContextsForAssociateAcceptPDU = AssociateAcceptPDU.sanitizePresentationContextsForAcceptance(presentationContexts);
if (debugLevel > 1) System.err.println("Presentation contexts for A-ASSOCIATE-AC:\n"+presentationContextsForAssociateAcceptPDU);

if (debugLevel > 1) System.err.println("OurMaximumLengthReceived="+ourMaximumLengthReceived);

					AssociateAcceptPDU aac = new AssociateAcceptPDU(calledAETitle,callingAETitle,implementationClassUID,implementationVersionName,
							ourMaximumLengthReceived,presentationContextsForAssociateAcceptPDU);
					out.write(aac.getBytes());
					out.flush();						// State 6
				}
			}
			else if (pduType == 0x07) {						//           - A-ABORT PDU
				AAbortPDU aab = new AAbortPDU(getRestOfPDU(in,startBuffer,pduLength));
if (debugLevel > 1) System.err.println("Them:\n"+aab);
				socket.close();							// AA-2      - Stop ARTIM, close transport connection and indicate abort
				throw new DicomNetworkException("A-ABORT indication - "+aab.getInfo());
												// State 1   - Idle
			}
			else {									//           - Invalid or unrecognized PDU received
if (debugLevel > 1) System.err.println("Aborting");

				AAbortPDU aab = new AAbortPDU(0,0);				// AA-1      - Send A-ABORT PDU (service user source, reserved), and start (or restart) ARTIM
				out.write(aab.getBytes());
				out.flush();
												//             issue an A-P-ABORT indication and start ARTIM
												// State 13  - Awaiting Transport connection close
				// should wait for ARTIM but ...
				socket.close();
				throw new DicomNetworkException("A-P-ABORT indication - "+aab.getInfo());
												// State 1   - Idle
			}
		}
		catch (IOException e) {								//           - Transport connection closed (or other error)
			throw new DicomNetworkException("A-P-ABORT indication - "+e);		// AA-5      - Stop ARTIM
												// State 1   - Idle
		}

		// falls through only from State 6 - Data Transfer
	}
	
	/**
	 * Accept or reject Abstract Syntaxes (SOP Classes).
	 *
	 * Only storage or verification SOP Classes are acceptable.
	 *
	 * @param	pcs	a java.util.LinkedList of {@link PresentationContext PresentationContext} objects,
	 *			each of which contains an Abstract Syntax (SOP Class UID)
	 * @return		the java.util.LinkedList of {@link PresentationContext PresentationContext} objects,
	 *			as supplied but with the result/reason field set to either "acceptance" or
	 *			"abstract syntax not supported (provider rejection)"
	 */
	static protected LinkedList applyAbstractSyntaxSelectionPolicy(LinkedList pcs) {	// will always set result/reason one way or the other, hence call 1st
		ListIterator pcsi = pcs.listIterator();
		while (pcsi.hasNext()) {
			PresentationContext pc = (PresentationContext)(pcsi.next());
			String abstractSyntaxUID = pc.getAbstractSyntaxUID();
			pc.setResultReason(
				SOPClass.isImageStorage(abstractSyntaxUID)
			     || SOPClass.isNonImageStorage(abstractSyntaxUID)
			     || SOPClass.isVerification(abstractSyntaxUID)
				? (byte)0 : (byte)3);	// acceptance :  abstract syntax not supported (provider rejection)			      
		}
		return pcs;
	}
	
	/**
	 * Accept or reject Presentation Contexts, preferring Explicit over Implicit VR.
	 *
	 * @param	pcs	a java.util.LinkedList of {@link PresentationContext PresentationContext} objects,
	 *			each of which contains a list of Transfer Syntaxes
	 * @return		the java.util.LinkedList of {@link PresentationContext PresentationContext} objects,
	 *			as supplied but with the Transfer Syntax list culled to the one preferred
	 *			Transfer Syntax (or empty if none acceptable)
	 *			and the result/reason field left alone if one of the Transfer Syntaxes
	 *			was acceptable, or set to
	 *			"transfer syntaxes not supported (provider rejection)"
	 */
	protected LinkedList applyTransferSyntaxSelectionPolicy(LinkedList pcs) {	// leaves result/reason alone unless rejecting, hence call 2nd
												// leaves AS alone, but always cuts TS down to exactly 1 (or zero if all rejected)
		boolean canUseBzip = false;
		try {
			// this method is not static since we need this.getClass()
			Class classToUse = this.getClass().getClassLoader().loadClass("org.apache.excalibur.bzip2.CBZip2InputStream");
			//Class classForLoader = this.getClass();
			//ClassLoader loader = classForLoader.getClassLoader();
			//Class classToUse = loader.loadClass("org.apache.excalibur.bzip2.CBZip2InputStream");
			if (classToUse != null) {
				canUseBzip = true;
			}
		}
		catch (ClassNotFoundException e) {	// this should be the only exception thrown, unless we can't get a class loader
		}
		catch (Exception e) {
			e.printStackTrace(System.err);
		}
				
		ListIterator pcsi = pcs.listIterator();
		while (pcsi.hasNext()) {
			PresentationContext pc = (PresentationContext)(pcsi.next());
			boolean foundExplicitVRLittleEndian = false;
			boolean foundImplicitVRLittleEndian = false;
			boolean foundExplicitVRBigEndian = false;
			boolean foundDeflated = false;
			boolean foundBzipped = false;
			List tsuids = pc.getTransferSyntaxUIDs();
			ListIterator tsuidsi = tsuids.listIterator();
			while (tsuidsi.hasNext()) {
				String transferSyntaxUID=(String)(tsuidsi.next());
				if (transferSyntaxUID != null) {
					if      (transferSyntaxUID.equals(TransferSyntax.ImplicitVRLittleEndian)) foundImplicitVRLittleEndian = true;
					else if (transferSyntaxUID.equals(TransferSyntax.ExplicitVRLittleEndian)) foundExplicitVRLittleEndian = true;
					else if (transferSyntaxUID.equals(TransferSyntax.ExplicitVRBigEndian)) foundExplicitVRBigEndian = true;
					else if (transferSyntaxUID.equals(TransferSyntax.DeflatedExplicitVRLittleEndian)) foundDeflated = true;
					else if (transferSyntaxUID.equals(TransferSyntax.PixelMedBzip2ExplicitVRLittleEndian)) foundBzipped = true;
				}
			}
			// discard old list and make a new one ...
			pc.newTransferSyntaxUIDs();
			// Policy is prefer compressed then explicit (little then big) then implicit in the following order and ignore anything else
			if (foundBzipped && canUseBzip) {
				pc.addTransferSyntaxUID(TransferSyntax.PixelMedBzip2ExplicitVRLittleEndian);
			}
			else if (foundDeflated) {
				pc.addTransferSyntaxUID(TransferSyntax.DeflatedExplicitVRLittleEndian);
			}
			else if (foundExplicitVRLittleEndian) {
				pc.addTransferSyntaxUID(TransferSyntax.ExplicitVRLittleEndian);
			}
			else if (foundExplicitVRBigEndian) {
				pc.addTransferSyntaxUID(TransferSyntax.ExplicitVRBigEndian);
			}
			else if (foundImplicitVRLittleEndian) {
				pc.addTransferSyntaxUID(TransferSyntax.ImplicitVRLittleEndian);
			}
			else {
				pc.setResultReason((byte)4);				// transfer syntaxes not supported (provider rejection)
			}
		}
		return pcs;
	}
	
	/**
	 * Reject Implicit VR Presentation Contexts if possible.
	 *
	 * @param	pcs	a java.util.LinkedList of {@link PresentationContext PresentationContext} objects,
	 *			each of which contains a single (accepted) Transfer Syntax
	 * @return		the java.util.LinkedList of {@link PresentationContext PresentationContext} objects,
	 *			as supplied but with any Presentation Context with an Implicit VR Transfer Syntax
	 *			for which there is another Presentation Context with an Explicit VR Transfer Syntax
	 *			rejected with the result/reason field set to
	 *			"no reason (provider rejection)"
	 */
	static protected LinkedList applyExplicitTransferSyntaxPreferencePolicy(LinkedList pcs) {	// leaves result/reason alone unless rejecting
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: start");
		// Objective is to cull list so that we make the choice of
		// explicit over implicit if more than one TS offered and accepted for the same AS
		
		HashSet allAbstractSyntaxesAcceptedWithExplicitVRTransferSyntax = new HashSet();
		
		// Pass 1 - fill allAbstractSyntaxesAcceptedWithExplicitVRTransferSyntax
		
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: start pass 1");
		ListIterator pcsi = pcs.listIterator();
		while (pcsi.hasNext()) {
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: iterating");
			PresentationContext pc = (PresentationContext)(pcsi.next());
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: have pc "+pc);
			String transferSyntaxUID=pc.getTransferSyntaxUID();		// There will only be one by this time
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: have transferSyntaxUID "+transferSyntaxUID);
			if (transferSyntaxUID != null && TransferSyntax.isExplicitVR(transferSyntaxUID)) {
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: adding to allAbstractSyntaxesAcceptedWithExplicitVRTransferSyntax: "+pc);
				allAbstractSyntaxesAcceptedWithExplicitVRTransferSyntax.add(pc.getAbstractSyntaxUID());
			}
		}
		
		// Pass 2 - reject any PC with an IVR for an AS that is in allAbstractSyntaxesAcceptedWithExplicitVRTransferSyntax
		
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: start pass 2");
		pcsi = pcs.listIterator();
		while (pcsi.hasNext()) {
			PresentationContext pc = (PresentationContext)(pcsi.next());
			String transferSyntaxUID=pc.getTransferSyntaxUID();		// There will only be one by this time
			if (transferSyntaxUID != null
			 && TransferSyntax.isImplicitVR(transferSyntaxUID)
			 && allAbstractSyntaxesAcceptedWithExplicitVRTransferSyntax.contains(pc.getAbstractSyntaxUID())) {
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: rejecting: "+pc);
				pc.setResultReason((byte)2);				// no reason (provider rejection)
			}
		}
//System.err.println("applyExplicitTransferSyntaxPreferencePolicy: done");
		return pcs;
	}
	
	/*
	 * Returns a string representation of the object.
	 *
	 * @return	a string representation of the object
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		//sb.append("Port: "); sb.append(port); sb.append("\n");
		sb.append(super.toString());
		return sb.toString();
	}

	//public static void main(String arg[]) {
	//	try {
	//		new AssociationAcceptor(arg[0],arg[1]);
	//	}
	//	catch (Exception e) {
	//		e.printStackTrace(System.err);
	//		System.exit(0);
	//	}
	//}
}


