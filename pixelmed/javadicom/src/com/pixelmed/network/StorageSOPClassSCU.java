/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.network;

import com.pixelmed.utils.*;
import com.pixelmed.dicom.*;

import java.util.ListIterator;
import java.util.LinkedList;
import java.io.*;

import java.util.zip.*;
import org.apache.excalibur.bzip2.CBZip2InputStream;

/**
 * <p>This class implements the SCU role of SOP Classes of the Storage Service Class.</p>
 *
 * <p>The class has no methods other than the constructor (and a main method for testing). The
 * constructor establishes an association, sends the C-STORE request, and releases the
 * association.</p>
 *
 * <p>Debugging messages with a varying degree of verbosity can be activated.</p>
 *
 * <p>For example:</p>
 * <pre>
try {
    new StorageSOPClassSCU("theirhost","104","STORESCP","STORESCU","/tmp/testfile.dcm","1.2.840.10008.5.1.4.1.1.7","1.3.6.1.4.1.5962.1.1.0.0.0.1064923879.2077.3232235877",0,0);
}
catch (Exception e) {
    e.printStackTrace(System.err);
}
 * </pre>
 *
 * @author	dclunie
 */
public class StorageSOPClassSCU extends SOPClass {

	/***/
	private static final String identString = "@(#) $Header$";

	/***/
	protected int debugLevel;
	
	/***/
	protected class CStoreResponseHandler extends CompositeResponseHandler {
		/**
		 * @param	debugLevel
		 */
		CStoreResponseHandler(int debugLevel) {
			super(debugLevel);
		}
		
		/**
		 * @param	list
		 */
		protected void evaluateStatusAndSetSuccess(AttributeList list) {
			// could check all sorts of things, like:
			// - AffectedSOPClassUID is what we sent
			// - CommandField is 0x8001 C-STORE-RSP
			// - MessageIDBeingRespondedTo is what we sent
			// - DataSetType is 0101 (no data set)
			// - Status is success and consider associated elements
			// - AffectedSOPInstanceUID is what we sent
			//
			// for now just treat success or warning as success (and absence as failure)
			int status = Attribute.getSingleIntegerValueOrDefault(list,TagFromName.Status,0xffff);
			success =  status == 0x0000	// success
				|| status == 0xB000	// coercion of data element
				|| status == 0xB007	// data set does not match SOP Class
				|| status == 0xB006;	// element discarded
		}
	}
	
	/**
	 * @param	association
	 * @param	affectedSOPClass
	 * @param	affectedSOPInstance
	 * @param	inputTransferSyntaxUID
	 * @param	din
	 * @param	presentationContextID
	 * @param	outputTransferSyntaxUID
	 * @exception	IOException
	 * @exception	DicomException
	 * @exception	DicomNetworkException
	 * @exception	AReleaseException
	 */
	protected boolean sendOneSOPInstance(Association association,
			String affectedSOPClass,String affectedSOPInstance,
			String inputTransferSyntaxUID,DicomInputStream din,
			byte presentationContextID,String outputTransferSyntaxUID) throws AReleaseException, DicomNetworkException, DicomException, IOException {
		byte cStoreRequestCommandMessage[] = new CStoreRequestCommandMessage(affectedSOPClass,affectedSOPInstance).getBytes();
		CStoreResponseHandler receivedDataHandler = new CStoreResponseHandler(debugLevel);
		association.setReceivedDataHandler(receivedDataHandler);
		association.send(presentationContextID,cStoreRequestCommandMessage,null);
		OutputStream out = association.getAssociationOutputStream(presentationContextID);
		if (inputTransferSyntaxUID.equals(outputTransferSyntaxUID)) {
		//if (false) {
if (debugLevel > 0) System.err.println("StorageSOPClassSCU.sendOneSOPInstance(): same transfer syntax so raw binary copy");
			CopyStream.copy(din,out);		// be careful ... this will not remove DataSetTrailingPadding, which will kill GE AW
if (debugLevel > 0) System.err.println("StorageSOPClassSCU.sendOneSOPInstance(): back from raw binary copy");
			out.close();
		}
		else {
if (debugLevel > 0) System.err.println("StorageSOPClassSCU.sendOneSOPInstance(): different transfer syntaxes; converting "+inputTransferSyntaxUID+" to "+outputTransferSyntaxUID);
			// din will already be positioned after meta-header and set for reading data set
			// copier will push any transfer syntax specific decompression filter onto the stream before reading
			DicomOutputStream dout = new DicomOutputStream(out,null/*meta*/,outputTransferSyntaxUID/*dataset*/);
			new DicomStreamCopier(din,dout);
			// Do not need dout.close() since DicomStreamCopier always closes output strean itself
		}
if (debugLevel > 0) System.err.println("StorageSOPClassSCU.sendOneSOPInstance(): about to wait for PDUs");
		association.waitForCommandPDataPDUs();
		return receivedDataHandler.wasSuccessful();
	}
	
	private static String[][] supportedTransferSyntaxes = {
		{
			TransferSyntax.ImplicitVRLittleEndian,
			TransferSyntax.ExplicitVRLittleEndian,
			TransferSyntax.ExplicitVRBigEndian,
		},
		{
			TransferSyntax.ImplicitVRLittleEndian,
			TransferSyntax.ExplicitVRLittleEndian,
			TransferSyntax.ExplicitVRBigEndian,
			TransferSyntax.DeflatedExplicitVRLittleEndian
		},
		{
			TransferSyntax.ImplicitVRLittleEndian,
			TransferSyntax.ExplicitVRLittleEndian,
			TransferSyntax.ExplicitVRBigEndian,
			TransferSyntax.PixelMedBzip2ExplicitVRLittleEndian,
			TransferSyntax.DeflatedExplicitVRLittleEndian
		}
	};
	
	/**
	 * @return	true if bzip2 codec available
	 */
	private boolean haveBzip2Support() {
		boolean result=true;
		try {
			Class classToUse = this.getClass().getClassLoader().loadClass("org.apache.excalibur.bzip2.CBZip2InputStream");
		}
		catch (ClassNotFoundException e) {
			result=false;
		}
		return result;
	}
	
	/**
	 * <p>Dummy constructor allows testing subclasses to use different constructor.</p>
	 *
	 */
	protected StorageSOPClassSCU() throws DicomNetworkException, DicomException, IOException {
	}
	
	/**
	 * <p>Establish an association to the specified AE, send the instance contained in the file, and release the association.</p>
	 *
	 * @param	hostname		their hostname or IP address
	 * @param	port			their port
	 * @param	calledAETitle		their AE Title
	 * @param	callingAETitle		our AE Title
	 * @param	fileName		the name of the file containing the data set to send
	 * @param	affectedSOPClass	must be the same as the SOP Class UID contained within the data set
	 * @param	affectedSOPInstance	must be the same as the SOP Instance UID contained within the data set
	 * @param	compressionLevel	0=none,1=propose deflate,2=propose deflate and bzip2
	 * @param	debugLevel		zero for no debugging messages, higher values more verbose messages
	 * @exception	IOException
	 * @exception	DicomException
	 * @exception	DicomNetworkException
	 */
	public StorageSOPClassSCU(String hostname, String port, String calledAETitle,String callingAETitle,String fileName,
			String affectedSOPClass,String affectedSOPInstance,int compressionLevel,
			int debugLevel) throws DicomNetworkException, DicomException, IOException {
		this.debugLevel=debugLevel;
		
if (debugLevel > 0) System.err.println("StorageSOPClassSCU: storing "+fileName);
		// Don't even begin until we know we can open the file ...
		InputStream in = new BufferedInputStream(new FileInputStream(fileName));
		String inputTransferSyntax;
		DicomInputStream din = new DicomInputStream(in);
		if (din.haveMetaHeader()) {
			AttributeList metaList = new AttributeList();
			metaList.readOnlyMetaInformationHeader(din);
if (debugLevel > 0) System.err.println("Meta header information = "+metaList);
			affectedSOPClass=Attribute.getSingleStringValueOrNull(metaList,TagFromName.MediaStorageSOPClassUID);
			affectedSOPInstance=Attribute.getSingleStringValueOrNull(metaList,TagFromName.MediaStorageSOPInstanceUID);
			inputTransferSyntax=Attribute.getSingleStringValueOrNull(metaList,TagFromName.TransferSyntaxUID);
		}
		else {
			inputTransferSyntax=din.getTransferSyntaxToReadDataSet().getUID();
		}
if (debugLevel > 0) System.err.println("Using inputTransferSyntax "+inputTransferSyntax);

		if (affectedSOPClass == null) {
			throw new DicomNetworkException("Can't C-STORE SOP Instance - can't determine Affected SOP Class UID");
		}
		if (affectedSOPInstance == null) {
			throw new DicomNetworkException("Can't C-STORE SOP Instance - can't determine Affected SOP Instance UID");
		}
		
		if (!haveBzip2Support()) {
			if (compressionLevel > 1 && !inputTransferSyntax.equals(TransferSyntax.PixelMedBzip2ExplicitVRLittleEndian)) {
if (debugLevel > 0) System.err.println("Bzip2 codec not available");
				compressionLevel=1;	// do not propose the bzip2 transfer syntax if the codec is not available
			}
			// note that if the codec is not available and the transfer syntax is bzip2, may fail later if acceptor does not support bzip2 
		}

		LinkedList presentationContexts = new LinkedList();
		{		
			byte presentationContextID = (byte)0x01;	// always odd numbered, starting with 0x01
			
			// First propose a presentation context with all transfer syntaxes
			// What we get back will indicate the acceptor's preference, in case we want to use their choice ...
			
			{
				LinkedList tslist = new LinkedList();
				tslist.add(inputTransferSyntax);	// always include the actual transfer syntax in which the input file is already encoded
				for (int i=0; i<supportedTransferSyntaxes[compressionLevel].length; ++i) {
					// Don't want to add the same transfer syntax twice in the same presentation context, hence check ...
					if (!inputTransferSyntax.equals(supportedTransferSyntaxes[compressionLevel][i])) {
						tslist.add(supportedTransferSyntaxes[compressionLevel][i]);
					}
				}
				presentationContexts.add(new PresentationContext(presentationContextID,affectedSOPClass,tslist));
			}
			
			// Now propose a presentation context for each transfer syntax
			// What we get back will tell us what the acceptor actually supports, in case we want to choose ourselves ...
			
			// always include a presentation context for the actual transfer syntax in which the input file is already encoded
			presentationContextID+=2;		// always odd numbered
			presentationContexts.add(new PresentationContext(presentationContextID,affectedSOPClass,inputTransferSyntax));
			
			for (int i=0; i<supportedTransferSyntaxes[compressionLevel].length; ++i) {
				presentationContextID+=2;	// always odd numbered
				// Don't want to add the same transfer syntax twice in the same presentation context, hence check ...
				if (!inputTransferSyntax.equals(supportedTransferSyntaxes[compressionLevel][i])) {
					presentationContexts.add(new PresentationContext(presentationContextID,affectedSOPClass,supportedTransferSyntaxes[compressionLevel][i]));
				}
			}
		}

		Association association = AssociationFactory.createNewAssociation(hostname,port,calledAETitle,callingAETitle,presentationContexts,debugLevel);
if (debugLevel > 0) System.err.println(association);
		// Decide which presentation context we are going to use ...
		byte presentationContextID = association.getSuitablePresentationContextID(affectedSOPClass);
		//int presentationContextID = association.getSuitablePresentationContextID(affectedSOPClass,TransferSyntax.Default);
if (debugLevel > 0) System.err.println("Using context ID "+presentationContextID);
		String outputTransferSyntax = association.getTransferSyntaxForPresentationContextID(presentationContextID);
if (debugLevel > 0) System.err.println("Using outputTransferSyntax "+outputTransferSyntax);
		boolean success = false;
		try {
			success = sendOneSOPInstance(association,affectedSOPClass,affectedSOPInstance,
				inputTransferSyntax,din,
				presentationContextID,outputTransferSyntax);
			// State 6
			association.release();
		}
		catch (AReleaseException e) {
			// State 1
			// the other end released and didn't wait for us to do it
		}
System.err.println("Send "+(success ? "succeeded" : "failed"));
	}

	/**
	 * <p>For testing, establish an association to the specified AE and send a DICOM instance (send a C-STORE request).</p>
	 *
	 * @param	arg	array of seven or nine strings - their hostname, their port, their AE Title, our AE Title,
	 *			the filename containing the instance to send,
	 * 			optionally the SOP Class and the SOP Instance (otherwise will be read from the file),
	 *			the compression level (0=none,1=propose deflate,2=propose deflate and bzip2) and the debugging level
	 */
	public static void main(String arg[]) {
		try {
			String      theirHost=null;
			String      theirPort=null;
			String   theirAETitle=null;
			String     ourAETitle=null;
			String       fileName=null;
			String    SOPClassUID=null;
			String SOPInstanceUID=null;
			int  compressionLevel=0;
			int        debugLevel=0;
	
			if (arg.length == 9) {
				     theirHost=arg[0];
				     theirPort=arg[1];
				  theirAETitle=arg[2];
				    ourAETitle=arg[3];
				      fileName=arg[4];
				   SOPClassUID=arg[5];
				SOPInstanceUID=arg[6];
			      compressionLevel=Integer.parseInt(arg[7]);
				    debugLevel=Integer.parseInt(arg[8]);
			}
			else if (arg.length == 7) {
				     theirHost=arg[0];
				     theirPort=arg[1];
				  theirAETitle=arg[2];
				    ourAETitle=arg[3];
				      fileName=arg[4];
				   SOPClassUID=null;			// figured out by StorageSOPClassSCU() by reading the metaheader
				SOPInstanceUID=null;			// figured out by StorageSOPClassSCU() by reading the metaheader
			      compressionLevel=Integer.parseInt(arg[5]);
				    debugLevel=Integer.parseInt(arg[6]);
			}
			else {
				throw new Exception("Argument list must be 7 or 9 values");
			}
			new StorageSOPClassSCU(theirHost,theirPort,theirAETitle,ourAETitle,fileName,SOPClassUID,SOPInstanceUID,compressionLevel,debugLevel);
		}
		catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(0);
		}
	}
}




