<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">

<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:variable name="optionDescribeChecking"><xsl:value-of select="$newline"/></xsl:variable>

<xsl:variable name="optionWarnOnMayBePresentOtherwiseAnyway">F</xsl:variable>

<xsl:template name="buildFullPathInInstanceToNamedNode">
	<xsl:param name="node"/>
	<xsl:if test="name($node/..) != 'DicomObject'">
		<xsl:call-template name="buildFullPathInInstanceToNamedNode">
			<xsl:with-param name="node" select="$node/.."/>
		</xsl:call-template>
	</xsl:if>
	<xsl:text>/</xsl:text><xsl:value-of select="name($node)"/>
</xsl:template>

<xsl:template name="buildFullPathInInstanceToNamedChildOfCurrentNode">
	<xsl:param name="node"/>
	<xsl:call-template name="buildFullPathInInstanceToCurrentNode"/>
	<xsl:value-of select="name($node)"/>
</xsl:template>

<xsl:template name="buildFullPathInInstanceToCurrentNode">
	<xsl:if test="name(.) != 'DicomObject'">
		<xsl:for-each select="..">
			<xsl:call-template name="buildFullPathInInstanceToCurrentNode"/>
		</xsl:for-each>
		<xsl:value-of select="name(.)"/>
		<xsl:if test="name(.) = 'Item'">
			<xsl:text>[</xsl:text><xsl:value-of select="@number"/><xsl:text>]</xsl:text>
		</xsl:if>
		<xsl:text>/</xsl:text>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckAttributeVR">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vr"/>
	<xsl:if test="count($element) = 1 and (($vr != 'XS' and $vr != 'OX' and $element/@vr != $vr) or ($vr = 'XS' and $element/@vr != 'US' and $element/@vr != 'SS') or ($vr = 'OX' and $element/@vr != 'OB' and $element/@vr != 'OW'))">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect VR - got </xsl:text><xsl:value-of select="$element/@vr"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vr"/><xsl:value-of select="$newline"/>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckConditionalAttributeWhenConditionSatisfied">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:if test="count($element) &lt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Missing conditional attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckConditionalAttributeWhenConditionNotSatisfied">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="mbpo"/>
	<xsl:if test="count($element) &gt; 0">
		<xsl:if test="$mbpo != 'T' or $optionWarnOnMayBePresentOtherwiseAnyway = 'T'">
			<xsl:choose>
			<xsl:when test="$mbpo = 'T'">
			<xsl:text>Warning</xsl:text>
			</xsl:when>
			<xsl:otherwise>
			<xsl:text>Error</xsl:text>
			</xsl:otherwise>
			</xsl:choose>
			<xsl:text>: </xsl:text>
			<xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Conditional attribute present when condition not satisfied</xsl:text><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType1CAttributeRegardless">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:param name="vr"/>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1 and $vr != 'OB' and $vr != 'OW' and $vr != 'OX' and $vr != 'OF'">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/value) &lt; $vmmin) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/value) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect value multiplicity - got </xsl:text><xsl:value-of select="count($element/value)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
		<xsl:if test="string-length($element/value) = 0">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Type 1C attribute may not be zero length when present</xsl:text><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType1Attribute">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:param name="vr"/>
	<xsl:if test="count($element) &lt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Missing required attribute (Type 1)</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1 and $vr != 'OB' and $vr != 'OW' and $vr != 'OX' and $vr != 'OF'">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/value) &lt; $vmmin) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/value) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect value multiplicity - got </xsl:text><xsl:value-of select="count($element/value)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
		<xsl:if test="string-length($element/value) = 0">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Type 1 attribute may not be zero length</xsl:text><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType2CAttributeRegardless">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/value) &lt; $vmmin and not (count($element/value) = 0 and $vmmin = 1)) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/value) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect value multiplicity - got </xsl:text><xsl:value-of select="count($element/value)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType2Attribute">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:if test="count($element) &lt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Missing required attribute (Type 2)</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/value) &lt; $vmmin and not (count($element/value) = 0 and $vmmin = 1)) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/value) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect value multiplicity - got </xsl:text><xsl:value-of select="count($element/value)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType3Attribute">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/value) &lt; $vmmin and not (count($element/value) = 0 and $vmmin = 1)) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/value) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect value multiplicity - got </xsl:text><xsl:value-of select="count($element/value)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType1CSequenceAttributeRegardless">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/Item) &lt; $vmmin) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/Item) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect number of sequence items - got </xsl:text><xsl:value-of select="count($element/Item)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
		<xsl:if test="count($element/Item) = 0">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Type 1C sequence attribute must have at least one item when present</xsl:text><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType1SequenceAttribute">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:if test="count($element) &lt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Missing required sequence attribute (Type 1)</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/Item) &lt; $vmmin) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/Item) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect number of sequence items - got </xsl:text><xsl:value-of select="count($element/Item)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
		<xsl:if test="count($element/Item) = 0">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Type 1 sequence attribute must have at least one item</xsl:text><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType2CSequenceAttributeRegardless">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/Item) &lt; $vmmin) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/Item) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect number of sequence items - got </xsl:text><xsl:value-of select="count($element/Item)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType2SequenceAttribute">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:if test="count($element) &lt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Missing required sequence attribute (Type 2)</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) &gt; 1">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="count($element) = 1">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/Item) &lt; $vmmin) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/Item) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect number of sequence items - got </xsl:text><xsl:value-of select="count($element/Item)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckType3SequenceAttribute">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="vmmin"/>
	<xsl:param name="vmmax"/>
	<xsl:if test="count($element) &gt; 1"><xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Duplicate attribute</xsl:text><xsl:value-of select="$newline"/></xsl:if>
	<xsl:if test="count($element) = 1">
		<xsl:if test="(string-length($vmmin) &gt; 0 and count($element/Item) &lt; $vmmin) or (string-length($vmmax) &gt; 0 and $vmmax != 'n' and count($element/Item) &gt; $vmmax)">
			<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Incorrect number of sequence items - got </xsl:text><xsl:value-of select="count($element/Item)"/><xsl:text> but expected </xsl:text><xsl:value-of select="$vmmin"/><xsl:text>-</xsl:text><xsl:value-of select="$vmmax"/><xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="CheckFirstValueIfPresentIsGreaterThan">
	<xsl:param name="description"/>
	<xsl:param name="element"/>
	<xsl:param name="value"/>
	<xsl:if test="count($element/value) &gt; 0 and $element/value[@number=1] &lt; $value">
		<xsl:text>Error: </xsl:text><xsl:value-of select="$description"/><xsl:text>: </xsl:text><xsl:call-template name="buildFullPathInInstanceToNamedChildOfCurrentNode"><xsl:with-param name="node" select="$element"/></xsl:call-template><xsl:text>: Value must be greater than </xsl:text><xsl:value-of select="$value"/><xsl:text> - got </xsl:text><xsl:value-of select="$element/value[@number=1]"/><xsl:value-of select="$newline"/>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
