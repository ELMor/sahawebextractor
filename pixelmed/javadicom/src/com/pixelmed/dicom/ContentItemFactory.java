/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import java.util.Iterator;

/**
 * <p>A class with methods for constructing a {@link com.pixelmed.dicom.ContentItem ContentItem} of the appropriate class from a list of attributes.</p>
 *
 * <p>The sub-classes of {@link com.pixelmed.dicom.ContentItem ContentItem} are private internal classes of this class,
 * but specialize the methods, specifically the extractors and the string representation methods.</p>
 *
 * <p>This is not an abstract class, an the content item factory method is not static; an instance of
 * the factory needs to be created.</p>
 *
 * @see com.pixelmed.dicom.ContentItem
 * @see com.pixelmed.dicom.StructuredReport
 * @see com.pixelmed.dicom.StructuredReportBrowser
 *
 * @author	dclunie
 */
public class ContentItemFactory {

	/***/
	private static final String identString = "@(#) $Header$";

	/***/
	private class UnrecognizedContentItem extends ContentItem {

		/**
		 * @param	parent
		 */
		UnrecognizedContentItem(ContentItem parent) {
			super(parent,null);
		}

		/**
		 * @param	parent
		 * @param	list
		 */
		UnrecognizedContentItem(ContentItem parent,AttributeList list) {
			super(parent,list);
		}

		/**
		 * @param	parent
		 * @param	list
		 * @param	name
		 */
		UnrecognizedContentItem(ContentItem parent,AttributeList list,String name) {
			super(parent,list);
		}
	}

	/***/
	private class ContainerContentItem extends ContentItem {

		/**
		 * @param	parent
		 * @param	list
		 */
		ContainerContentItem(ContentItem parent,AttributeList list) {
			super(parent,list);
		}
	}

	/***/
	private class CompositeContentItem extends ContentItem {

		/***/
		protected String referencedSOPClassUID;
		/***/
		protected String referencedSOPInstanceUID;

		/**
		 * @param	parent
		 * @param	list
		 */
		CompositeContentItem(ContentItem parent,AttributeList list) {
			super(parent,list);
			SequenceAttribute a=(SequenceAttribute)(list.get(TagFromName.ReferencedSOPSequence));
			if (a != null) {
				Iterator i = a.iterator();
				if (i.hasNext()) {
					SequenceItem item = ((SequenceItem)i.next());
					if (item != null) {
						AttributeList l = item.getAttributeList();
						referencedSOPClassUID=Attribute.getSingleStringValueOrEmptyString(l,TagFromName.ReferencedSOPClassUID);
						referencedSOPInstanceUID=Attribute.getSingleStringValueOrEmptyString(l,TagFromName.ReferencedSOPInstanceUID);
					}
				}
			}
			if (referencedSOPClassUID == null) referencedSOPClassUID="";		// just for consistency with other string content items
			if (referencedSOPInstanceUID == null) referencedSOPInstanceUID="";
		}


		/***/
		public String toString() {
			return super.toString()+" = "+referencedSOPClassUID+":"+referencedSOPInstanceUID;
		}

		/***/
		public String getReferencedSOPClassUID()    { return referencedSOPClassUID; }
		/***/
		public String getReferencedSOPInstanceUID() { return referencedSOPInstanceUID; }
	}

	/***/
	private class ImageContentItem extends CompositeContentItem {

		/**
		 * @param	parent
		 * @param	list
		 */
		ImageContentItem(ContentItem parent,AttributeList list) {
			super(parent,list);
		}
	}

	/***/
	private class SpatialCoordinatesContentItem extends ContentItem {

		/***/
		private String graphicType;
		/***/
		private float[] graphicData;

		/**
		 * @param	parent
		 * @param	list
		 */
		SpatialCoordinatesContentItem(ContentItem parent,AttributeList list) {
			super(parent,list);
			graphicType=Attribute.getSingleStringValueOrDefault(list,TagFromName.GraphicType,"");
			try {
				Attribute a = list.get(TagFromName.GraphicData);
				if (a != null) graphicData = a.getFloatValues();
			}
			catch (DicomException e) {
			}
		}

		/***/
		public String toString() {
			StringBuffer str = new StringBuffer();
			str.append(super.toString());
			str.append(" = ");
			str.append(graphicType);
			str.append(" (");
			if (graphicData != null) {
				for (int j=0; j<graphicData.length; ++j) {
					if (j > 0) str.append(",");
					str.append(graphicData[j]);
				}
			}
			str.append(")");
			return str.toString();
		}

		/***/
		public String getGraphicType()              { return graphicType; }
		/***/
		public float[] getGraphicData()             { return graphicData; }
	}

	/***/
	private class NumericContentItem extends ContentItem {

		/***/
		private String numericValue;
		/***/
		private CodedSequenceItem units;

		/**
		 * @param	parent
		 * @param	list
		 */
		NumericContentItem(ContentItem parent,AttributeList list) {
			super(parent,list);
			SequenceAttribute a=(SequenceAttribute)(list.get(TagFromName.MeasuredValueSequence));
			if (a != null) {
//System.err.println("NumericContentItem: MeasuredValueSequence="+a);
				Iterator i = a.iterator();
				if (i.hasNext()) {
					SequenceItem item = ((SequenceItem)i.next());
					if (item != null) {
//System.err.println("NumericContentItem: item="+item);
						AttributeList l = item.getAttributeList();
						numericValue=Attribute.getSingleStringValueOrEmptyString(l,TagFromName.NumericValue);
						units=CodedSequenceItem.getSingleCodedSequenceItemOrNull(l,TagFromName.MeasurementUnitsCodeSequence);
					}
				}
			}
			if (numericValue == null) numericValue="";	// just for consistency with other string content items
		}

		/***/
		public String getConceptNameAndValue() {
			return super.getConceptNameAndValue()+" = "+numericValue+" "+(units == null ? "" : units.getCodeMeaning());
		}

		/***/
		public String toString() {
			return super.toString()+" = "+numericValue+" "+(units == null ? "" : units.getCodeMeaning());
		}
	}

	/***/
	private class CodeContentItem extends ContentItem {

		/***/
		private CodedSequenceItem conceptCode;

		/**
		 * @param	parent
		 * @param	list
		 */
		CodeContentItem(ContentItem parent,AttributeList list) {
			super(parent,list);
			conceptCode=CodedSequenceItem.getSingleCodedSequenceItemOrNull(list,TagFromName.ConceptCodeSequence);
		}


		/***/
		public String getConceptNameAndValue() {
			return super.getConceptNameAndValue()+" "+(conceptCode == null ? "" : conceptCode.getCodeMeaning());
		}

		/***/
		public String toString() {
			return super.toString()+" = "+(conceptCode == null ? "" : conceptCode.getCodeMeaning());
		}
	}

	/***/
	abstract class StringContentItem extends ContentItem {

		/***/
		protected String stringValue;

		/**
		 * @param	parent
		 * @param	list
		 * @param	tag
		 */
		StringContentItem(ContentItem parent,AttributeList list,AttributeTag tag) {
			super(parent,list);
			stringValue=Attribute.getSingleStringValueOrDefault(list,tag,"");
		}


		/***/
		public String getConceptNameAndValue() {
			return super.getConceptNameAndValue()+" "+stringValue;
		}

		/***/
		public String toString() {
			return super.toString()+" = "+stringValue;
		}
	}

	/***/
	private class DateTimeContentItem extends StringContentItem {

		/**
		 * @param	parent
		 * @param	list
		 */
		DateTimeContentItem(ContentItem parent,AttributeList list) {
			super(parent,list,TagFromName.DateTime);
		}
	}

	/***/
	private class DateContentItem extends StringContentItem {

		/**
		 * @param	parent
		 * @param	list
		 */
		DateContentItem(ContentItem parent,AttributeList list) {
			super(parent,list,TagFromName.Date);
		}
	}

	/***/
	private class TimeContentItem extends StringContentItem {

		/**
		 * @param	parent
		 * @param	list
		 */
		TimeContentItem(ContentItem parent,AttributeList list) {
			super(parent,list,TagFromName.Time);
		}
	}

	/***/
	private class PersonNameContentItem extends StringContentItem {

		/**
		 * @param	parent
		 * @param	list
		 */
		PersonNameContentItem(ContentItem parent,AttributeList list) {
			super(parent,list,TagFromName.PersonName);
		}
	}

	/***/
	private class UIDContentItem extends StringContentItem {

		/**
		 * @param	parent
		 * @param	list
		 */
		UIDContentItem(ContentItem parent,AttributeList list) {
			super(parent,list,TagFromName.UID);
		}
	}

	/***/
	private class TextContentItem extends StringContentItem {

		/**
		 * @param	parent
		 * @param	list
		 */
		TextContentItem(ContentItem parent,AttributeList list) {
			super(parent,list,TagFromName.TextValue);
		}
	}

	/**
	 * <p>Construct a content item of the appropriate class from a list of attributes.</p>
	 *
	 * @param	parent	the parent to add the content item to
	 * @param	list	a list of attributes that constitute the content item as it is encoded in a DICOM data set
	 * @return		a content item
	 */
	public ContentItem getNewContentItem(ContentItem parent,AttributeList list) {
		ContentItem contentItem = null;

		if (list == null) {
			contentItem = new UnrecognizedContentItem(parent);
		}
		else {
			String valueType=Attribute.getSingleStringValueOrNull(list,TagFromName.ValueType);
			if (valueType == null) {
				contentItem = new UnrecognizedContentItem(parent,list);
			}
			else if (valueType.equals("CONTAINER")) {
				contentItem = new ContainerContentItem(parent,list);
			}
			else if (valueType.equals("CODE")) {
				contentItem = new CodeContentItem(parent,list);
			}
			else if (valueType.equals("NUM")) {
				contentItem = new NumericContentItem(parent,list);
			}
			else if (valueType.equals("DATETIME")) {
				contentItem = new DateTimeContentItem(parent,list);
			}
			else if (valueType.equals("DATE")) {
				contentItem = new DateContentItem(parent,list);
			}
			else if (valueType.equals("TIME")) {
				contentItem = new TimeContentItem(parent,list);
			}
			else if (valueType.equals("PNAME")) {
				contentItem = new PersonNameContentItem(parent,list);
			}
			else if (valueType.equals("UIDREF")) {
				contentItem = new UIDContentItem(parent,list);
			}
			else if (valueType.equals("TEXT")) {
				contentItem = new TextContentItem(parent,list);
			}
			else if (valueType.equals("SCOORD")) {
				contentItem = new SpatialCoordinatesContentItem(parent,list);
			}
			else if (valueType.equals("COMPOSITE")) {
				contentItem = new CompositeContentItem(parent,list);
			}
			else if (valueType.equals("IMAGE")) {
				contentItem = new ImageContentItem(parent,list);
			}
			else {
				contentItem = new UnrecognizedContentItem(parent,list,valueType);
			}
		}

		return contentItem;
	}
}


