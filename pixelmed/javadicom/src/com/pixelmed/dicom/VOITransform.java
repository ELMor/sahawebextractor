/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import java.util.*;

/**
 * <p>A transformation constructed from a DICOM attribute list that extracts
 * those attributes that define the VOI LUT transformation, specifically the
 * window center and width attributes.</p>
 *
 * <p>Looks first for a per-frame functional group FrameVOILUTSequence
 * then looks in the shared functional groups, otherwise tries to find the
 * Window Center and Width values in the top level of the dataset.</p>
 *
 * <p>Note that multiple transformations (for each frame) may be present and are
 * supported (specifically, Window Center and Width are multi-valued
 * attributes).</p>
 *
 * <p>Does not currently support a LUT in the VOI LUT Sequence, only linear
 * window values.</p>
 *
 * @author	dclunie
 */
public class VOITransform {

	/***/
	private static final String identString = "@(#) $Header$";
	
	/***/
	private class SingleVOITransform {
	
		/***/
		double center;
		/***/
		double width;
		/***/
		String explanation;
		
		/**
		 * @param	center
		 * @param	width
		 * @param	explanation
		 */
		SingleVOITransform(double center,double width,String explanation) {
			this.center = center;
			this.width = width;
			this.explanation = explanation;
//System.err.println("SingleVOITransform: adding "+center+","+width+" "+explanation);
		}
	}
	
	/***/
	private class SingleVOITransforms extends ArrayList {
		void add(AttributeList list) {
			double      centers[] = Attribute.getDoubleValues(list,TagFromName.WindowCenter);
			double       widths[] = Attribute.getDoubleValues(list,TagFromName.WindowWidth);
			String explanations[] = Attribute.getStringValues(list,TagFromName.WindowCenterWidthExplanation);
			
			if (centers != null && widths != null) {
				int n = centers.length;
				if (widths.length < n) n = widths.length;		// should probably warn users :(
				for (int i=0; i<n; ++i) {
					add(new SingleVOITransform(centers[i],widths[i],
						explanations != null && i<explanations.length ? explanations[i] : ""));
				}
			}
		}
	}
	
	/***/
	private SingleVOITransforms[] arrayOfTransforms;		// null if not varying per-frame, if not null will have size == number of frames
	/***/
	private SingleVOITransforms commonTransforms;		// in which case this will be used
	
	/**
	 * @param	list
	 */
	public VOITransform(AttributeList list) {
//System.err.println("VOITransform:");
		arrayOfTransforms=null;
		commonTransforms=null;
		/*try*/ {
			SequenceAttribute aPerFrameFunctionalGroupsSequence = (SequenceAttribute)list.get(TagFromName.PerFrameFunctionalGroupsSequence);
			if (aPerFrameFunctionalGroupsSequence != null) {
//System.err.println("VOITransform: checking PerFrameFunctionalGroupsSequence");
				int nFrames = aPerFrameFunctionalGroupsSequence.getNumberOfItems();
				int frameNumber = 0;
				Iterator pfitems = aPerFrameFunctionalGroupsSequence.iterator();
				while (pfitems.hasNext()) {
					SequenceItem fitem = (SequenceItem)pfitems.next();
					AttributeList flist = fitem.getAttributeList();
					if (flist != null) {
						SequenceAttribute aFrameVOILUTSequence = (SequenceAttribute)flist.get(TagFromName.FrameVOILUTSequence);
						if (aFrameVOILUTSequence != null && aFrameVOILUTSequence.getNumberOfItems() >= 1) {
//System.err.println("VOITransform: found FrameVOILUTSequence");
							if (arrayOfTransforms == null) arrayOfTransforms = new SingleVOITransforms[nFrames];
							if (arrayOfTransforms[frameNumber] == null) arrayOfTransforms[frameNumber] = new SingleVOITransforms();
							Iterator fvlitems = aFrameVOILUTSequence.iterator();
							while (fvlitems.hasNext()) {
								SequenceItem fvlitem = (SequenceItem)fvlitems.next();
								AttributeList fvllist = fvlitem.getAttributeList();
								//arrayOfTransforms[frameNumber].add(new SingleVOITransform(fvllist));
								arrayOfTransforms[frameNumber].add(fvllist);
							}
						}
					}
					++frameNumber;
				}
			}
			
			if (arrayOfTransforms == null) {
//System.err.println("VOITransform: checking SharedFunctionalGroupsSequence");
				SequenceAttribute aSharedFunctionalGroupsSequence = (SequenceAttribute)list.get(TagFromName.SharedFunctionalGroupsSequence);
				if (aSharedFunctionalGroupsSequence != null) {
					// assert aSharedFunctionalGroupsSequence.getNumberOfItems() == 1
					Iterator sitems = aSharedFunctionalGroupsSequence.iterator();
					if (sitems.hasNext()) {
						SequenceItem sitem = (SequenceItem)sitems.next();
						AttributeList slist = sitem.getAttributeList();
						if (slist != null) {
							SequenceAttribute aFrameVOILUTSequence = (SequenceAttribute)slist.get(TagFromName.FrameVOILUTSequence);
							if (aFrameVOILUTSequence != null && aFrameVOILUTSequence.getNumberOfItems() >= 1) {
//System.err.println("VOITransform: found FrameVOILUTSequence");
								commonTransforms = new SingleVOITransforms();
								Iterator fvlitems = aFrameVOILUTSequence.iterator();
								while (fvlitems.hasNext()) {
									SequenceItem fvlitem = (SequenceItem)fvlitems.next();
									AttributeList fvllist = fvlitem.getAttributeList();
									//commonTransforms.add(new SingleVOITransform(fvllist));
									commonTransforms.add(fvllist);
								}
							}
						}
					}
				}

				// check for "old-fashioned" VOI LUT style attributes
			
				if (list.get(TagFromName.WindowCenter) != null && list.get(TagFromName.WindowWidth) != null) {
					if (commonTransforms == null) commonTransforms = new SingleVOITransforms();
					commonTransforms.add(list);
				}
			}
			
		}
		//catch (DicomException e) {
		//	e.printStackTrace(System.err);
		//}
	}
	
	/**
	 * Get the number of transforms available for a particular frame.
	 *
	 * @param	frame		numbered from zero; needed to select which transform if frame-specific
	 * @return			the number of transforms available for the frame, 0 if none
	 */
	public int getNumberOfTransforms(int frame) {
		SingleVOITransforms useTransform = (arrayOfTransforms == null) ? commonTransforms : arrayOfTransforms[frame];
		return useTransform == null ? 0 : useTransform.size();
	}

	/**
	 * Get the window width of the particular transform available for a particular frame.
	 *
	 * @param	frame		numbered from zero; needed to select which transform if frame-specific
	 * @param	transform	numbered from zero; needed to select which transform if more than one for that frame
	 * @return			the window width
	 */
	public double getWidth(int frame,int transform) {
		SingleVOITransforms useTransform = (arrayOfTransforms == null) ? commonTransforms : arrayOfTransforms[frame];
		return ((SingleVOITransform)(useTransform.get(transform))).width;
	}
	

	/**
	 * Get the window center of the particular transform available for a particular frame.
	 *
	 * @param	frame		numbered from zero; needed to select which transform if frame-specific
	 * @param	transform	numbered from zero; needed to select which transform if more than one for that frame
	 * @return			the window center
	 */
	public double getCenter(int frame,int transform) {
		SingleVOITransforms useTransform = (arrayOfTransforms == null) ? commonTransforms : arrayOfTransforms[frame];
		return ((SingleVOITransform)(useTransform.get(transform))).center;
	}
	
}

