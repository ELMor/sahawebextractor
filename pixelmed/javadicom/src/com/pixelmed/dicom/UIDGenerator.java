/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import java.util.StringTokenizer;
import java.rmi.server.UID;

/**
 * <p>A class for generating Study, Series and SOP Instance UIDs.</p>
 *
 * @author	dclunie
 */

public class UIDGenerator {

	private static final String identString = "@(#) $Header$";

	private static final String UIDGEN_INSTANCE_SOP = "1";
	private static final String UIDGEN_INSTANCE_STUDY = "2";
	private static final String UIDGEN_INSTANCE_SERIES = "3";
	private static final String UIDGEN_FRAMEOFREFERENCE = "4";
	private static final String UIDGEN_INSTANCE_DIR = "5";
	private static final String UIDGEN_DIMENSIONORGANIZATION = "6";
	private static final String UIDGEN_CONCATENATION = "7";
		
	private String stamp;
	private String root;

	/**
	 * <p>Create a UID generator.</p>
	 *
	 * <p>This will use random and installation specific elements to create a unique root.</p>
	 *
	 */
	public UIDGenerator() {
		String string = new UID().toString();	// e.g. "19c082:fb77ce774a:-8000"
		StringTokenizer st = new StringTokenizer(string,":");
		int  ourUnique = Integer.valueOf(st.nextToken(),16).intValue();
		long   ourTime =    Long.valueOf(st.nextToken(),16).longValue();
		short ourCount =   Short.valueOf(st.nextToken(),16).shortValue();
		doCommonConstructorStuff(
			Integer.toString(Math.abs(ourUnique))
			+"."
			+   Long.toString(Math.abs(ourTime))
			+"."
			+Integer.toString(Math.abs(ourCount))
		);
	}

	/**
	 * <p>Create a UID generator.</p>
	 *
	 * <p>This will use the supplied stamp rather than generating a unique root, to create repeatable UIDs.</p>
	 *
	 * @param	stamp	a String of dotted numeric values in UID form
	 */
	public UIDGenerator(String stamp) {
		doCommonConstructorStuff(stamp);
	}
		
	private void doCommonConstructorStuff(String stamp) {
		this.stamp=stamp;
		root=VersionAndConstants.uidRoot+"."+VersionAndConstants.uidQualifierForThisToolkit+"."+"1";
	}
		
	private String getNumericPartOfStringOrZeroAsUIDExtension(String string) {
		String addition = ".0";
		if (string != null) {
			try {
				long numericPart=Math.abs(Long.parseLong(string));
				addition="."+Long.toString(numericPart);
			} catch (NumberFormatException e) {
			}
		}
		return addition;
	}
		
	/**
	 * <p>Get a Study Instance UID.</p>
	 *
	 * <p>This will be the same for this instance of the UIDGenerator and the same parameter values.</p>
	 *
	 * @param	studyID
	 * @return			the UID
	 */
	public String getNewStudyInstanceUID(String studyID) {
		return root+"."+stamp+"."+UIDGEN_INSTANCE_STUDY
			+getNumericPartOfStringOrZeroAsUIDExtension(studyID);
	}
		
	/**
	 * <p>Get a Series Instance UID.</p>
	 *
	 * <p>This will be the same for this instance of the UIDGenerator and the same parameter values.</p>
	 *
	 * @param	studyID
	 * @param	seriesNumber
	 * @return			the UID
	 */
	public String getNewSeriesInstanceUID(String studyID,String seriesNumber) {
		return root+"."+stamp+"."+UIDGEN_INSTANCE_SERIES
			+getNumericPartOfStringOrZeroAsUIDExtension(studyID)
			+getNumericPartOfStringOrZeroAsUIDExtension(seriesNumber);
	}
		
	/**
	 * <p>Get a SOP Instance UID.</p>
	 *
	 * <p>This will be the same for this instance of the UIDGenerator and the same parameter values.</p>
	 *
	 * @param	studyID
	 * @param	seriesNumber
	 * @param	instanceNumber
	 * @return			the UID
	 */
	public String getNewSOPInstanceUID(String studyID,String seriesNumber,String instanceNumber) {
		return root+"."+stamp+"."+UIDGEN_INSTANCE_SOP
			+getNumericPartOfStringOrZeroAsUIDExtension(studyID)
			+getNumericPartOfStringOrZeroAsUIDExtension(seriesNumber)
			+getNumericPartOfStringOrZeroAsUIDExtension(instanceNumber);
	}
}

