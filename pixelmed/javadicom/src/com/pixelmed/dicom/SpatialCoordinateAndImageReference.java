/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

/**
 * <p>A class to encapsulate information describing references in a structured
 * report to images, with or without spatial coordinates.</p>
 *
 * @see com.pixelmed.dicom.StructuredReport#findAllContainedSOPInstances(ContentItem)
 *
 * @author	dclunie
 */
public class SpatialCoordinateAndImageReference {

	private static final String identString = "@(#) $Header$";

	private String sopInstanceUID;
	private String sopClassUID;
	private String graphicType;
	private float[] graphicData;
	private String annotation;

	/**
	 * <p>Construct an instance of a reference to an image, with or withoyt spatial coordinate references.</p>
	 *
	 * @param	sopInstanceUID	the SOP Instance UID (should always present)
	 * @param	sopClassUID	the SOP Class UID (may be null)
	 * @param	graphicType	the Graphic Type if a spatial coordinate, otherwise null
	 * @param	graphicData	the Graphic Data if a spatial coordinate, otherwise null
	 * @param	annotation	a {@link java.lang.String String} value describing the reference (typically a parent Concept Name)
	 */
	public SpatialCoordinateAndImageReference(String sopInstanceUID,String sopClassUID,String graphicType,float[] graphicData,String annotation) {
		this.sopInstanceUID=sopInstanceUID;
		this.sopClassUID=sopClassUID;
		this.graphicType=graphicType;
		this.graphicData=graphicData;
		this.annotation=annotation;
	}

	/**
	 * <p>Get the SOP Instance UID.</p>
	 *
	 * @return		the SOP Instance UID, or null
	 */
	public String getSOPInstanceUID() { return sopInstanceUID; }

	/**
	 * <p>Get the SOP Class UID.</p>
	 *
	 * @return		the SOP Class UID, or null
	 */
	public String getSOPClassUID()    { return sopClassUID; }

	/**
	 * <p>Get the Graphic Type.</p>
	 *
	 * @return		the Graphic Type, or null
	 */
	public String getGraphicType()    { return graphicType; }

	/**
	 * <p>Get the Graphic Data.</p>
	 *
	 * @return		the Graphic Data, or null
	 */
	public float[] getGraphicData()   { return graphicData; }

	/**
	 * <p>Get the annotation.</p>
	 *
	 * @return		the annotation
	 */
	public String getAnnotation()     { return annotation; }

	/**
	 * <p>Dump a human-readable representation as a {@link java.lang.String String}.</p>
	 *
	 * @return		the annotation
	 */
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append(sopInstanceUID);
		str.append(":");
		str.append(sopClassUID);
		str.append(" = ");
		str.append(graphicType);
		str.append(" (");
		if (graphicData != null) {
			for (int j=0; j<graphicData.length; ++j) {
				if (j > 0) str.append(",");
				str.append(graphicData[j]);
			}
		}
		str.append(")");
		str.append(annotation);
		return str.toString();
	}
}

