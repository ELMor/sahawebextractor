/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import java.util.*;
import javax.swing.tree.*;

/**
 * <p>A class for representing a node in an internal representation of a structured reporting
 * tree (an instance of {@link com.pixelmed.dicom.StructuredReport StructuredReport}).</p>
 *
 * <p>The constructor is protected. Instances of specific types of content items should normally be created by using
 * the {@link com.pixelmed.dicom.ContentItemFactory ContentItemFactory}.</p>
 *
 * @see com.pixelmed.dicom.ContentItemFactory
 * @see com.pixelmed.dicom.StructuredReport
 * @see com.pixelmed.dicom.StructuredReportBrowser
 *
 * @author	dclunie
 */
public class ContentItem implements TreeNode {

	private static final String identString = "@(#) $Header$";

	/***/
	protected String valueType;
	/***/
	protected String relationshipType;
	/***/
	protected CodedSequenceItem conceptName;

	ContentItem parent;
	List children;
	AttributeList list;

	// Methods to implement TreeNode ...

	/**
	 * <p>Returns the parent node of this node.</p>
	 *
	 * @return	the parent node, or null if the root
	 */
	public TreeNode getParent() {
		return parent;
	}

	/**
	 * <p>Returns the child at the specified index.</p>
	 *
	 * @param	index	the index of the child to be returned, numbered from 0
	 * @return		the child <code>TreeNode</code> at the specified index
	 */
	public TreeNode getChildAt(int index) {
		return (TreeNode)(children.get(index));
	}

	/**
	 * <p>Returns the index of the specified child from amongst this node's children, if present.</p>
	 *
	 * @param	child	the child to search for amongst this node's children
	 * @return		the index of the child, or -1 if not present
	 */
	public int getIndex(TreeNode child) {
//System.err.println("getIndexOfChild: looking for "+child);
		int n=children.size();
		for (int i=0; i<n; ++i) {
			if (children.get(i).equals(child)) {	// expensive comparison ? :(
//System.err.println("getIndexOfChild: found "+child);
				return i;
			}
		}
		return -1;
	}

	/**
	 * <p> Always returns true, since children may always be added.</p>
	 *
	 * @return	always true
	 */
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * <p> Returns true if the receiver is a leaf (has no children).</p>
	 *
	 * @return	true if the receiver is a leaf
	 */
	public boolean isLeaf() {
		return getChildCount() == 0;
	}

	/**
	 * <p>Return the number of children that this node contains.</p>
	 *
	 * @return	the number of children, 0 if none
	 */
	public int getChildCount() {
		return children == null ? 0 : children.size();
	}

	/**
	 * <p>Returns the children of this node as an {@link java.util.Enumeration Enumeration}.</p>
	 *
	 * @return	the children of this node
	 */
	public Enumeration children() {
		return children == null ? null : new Vector(children).elements();
	}

	// Methods specific to this kind of node ...

	private void extractCommonAttributes() {
//System.err.println("extractCommonAttributes:");
		valueType=Attribute.getSingleStringValueOrDefault(list,TagFromName.ValueType,"");
//System.err.println("extractCommonAttributes: valueType="+valueType);
		relationshipType=Attribute.getSingleStringValueOrDefault(list,TagFromName.RelationshipType,"");
//System.err.println("extractCommonAttributes: relationshipType="+relationshipType);
		conceptName=CodedSequenceItem.getSingleCodedSequenceItemOrNull(list,TagFromName.ConceptNameCodeSequence);
//System.err.println("extractCommonAttributes: conceptName="+conceptName);
	}

	/**
	 * <p>Construct a content item for a list of attributes, and add it as a child of the specified parent.</p>
	 *
	 * <p>The constructor is protected. Instances of specific types of content items should normally be created by using
	 * the {@link com.pixelmed.dicom.ContentItemFactory ContentItemFactory}.</p>
	 *
	 * @param	p	the parent
	 * @param	l	the list of attributes
	 */
	protected ContentItem(ContentItem p,AttributeList l) {
		parent=p;
		list=l;
		extractCommonAttributes();
	}

	/**
	 * <p>Add a child to this content item.</p>
	 *
	 * @param	child		the child content item to add
	 */
	public void addChild(ContentItem child) {
		if (children == null) children=new LinkedList();
		children.add(child);
	}

	/**
	 * <p>Add a sibling to this content item (a child to the parent of this content item).</p>
	 *
	 * @param	sibling		the sibling content item to add
	 * @exception	DicomException	thrown if there is no parent
	 */
	public void addSibling(ContentItem sibling) throws DicomException {
		if (parent == null) {
			throw new DicomException("Internal error - root node with sibling");
		}
		else {
			parent.addChild(sibling);
		}
	}

	/**
	 * <p>Get the parent content item of this content item.</p>
	 *
	 * <p>This method saves the caller from having to cast the value returned from {@link javax.swing.tree.TreeNode#getParent() TreeNode.getParent()}.</p>
	 *
	 * @return	the parent content item
	 */
	public ContentItem getParentAsContentItem() { return parent; }

	/**
	 * <p>Get the attribute list of this content item.</p>
	 *
	 * @return	the attribute list of this content item
	 */
	public AttributeList getAttributeList() { return list; }

	/**
	 * <p>Get the value type of this content item.</p>
	 *
	 * @return	the value type (the string used in the DICOM standard in the Value Type attribute)
	 */
	public String getValueType()                { return valueType; }

	/**
	 * <p>Get the Referenced SOP Class UID of this content item, if present and applicable.</p>
	 *
	 * @return	the Referenced SOP Class UID, or null
	 */
	public String getReferencedSOPClassUID()    { return null; }

	/**
	 * <p>Get the Referenced SOP Instance UID of this content item, if present and applicable.</p>
	 *
	 * @return	the Referenced SOP Instance UID, or null
	 */
	public String getReferencedSOPInstanceUID() { return null; }

	/**
	 * <p>Get the Graphic Type of this content item, if present and applicable.</p>
	 *
	 * @return	the Graphic Type, or null
	 */
	public String getGraphicType()              { return null; }

	/**
	 * <p>Get the Graphic Data of this content item, if present and applicable.</p>
	 *
	 * @return	the Graphic Data, or null
	 */
	public float[] getGraphicData()             { return null; }

	/**
	 * <p>Get the value of the code meaning of the Concept Name as a string, if present and applicable.</p>
	 *
	 * @return	the code meaning of the Concept Name, or an empty string
	 */
	public String getConceptNameAndValue()      { return conceptName == null ? "" : conceptName.getCodeMeaning(); }

	/**
	 * <p>Get a human-readable string representation of the content item.</p>
	 *
	 * @return	the string representation of the content item
	 */
	public String toString() {
		return relationshipType+": "+valueType+": "+(conceptName == null ? "" : conceptName.getCodeMeaning());
	}
}



