/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import java.util.HashMap;

/**
 * <p>A class of static methods to provide a means of describing SOP Classes with abbreviations
 * and hman-readable descriptions.</p>
 *
 * @author	dclunie
 */
public class SOPClassDescriptions {
	private static final String identString = "@(#) $Header$";
	
	private static SOPClassDescriptions ourself = new SOPClassDescriptions();
	
	private HashMap abbreviationsByUID;
	private HashMap descriptionsByUID;

	private SOPClassDescriptions() {
		createAbbreviationsByUID();
		createDescriptionsByUID();
	}

	/**
	 * @param	sopClassUID	UID of the SOP Class, as a String without trailing zero padding
	 * @return			a short abbreviation describing the SOP Class, or an empty string if none
	 */
	public static String getAbbreviationFromUID(String sopClassUID) {
		String abbreviation = (String)ourself.abbreviationsByUID.get(sopClassUID);
		return abbreviation == null ? "" : abbreviation;
	}

	/**
	 * @param	sopClassUID	UID of the SOP Class, as a String without trailing zero padding
	 * @return			a description of the SOP Class, or an empty string if none
	 */
	public static String getDescriptionFromUID(String sopClassUID) {
		String description = (String)ourself.descriptionsByUID.get(sopClassUID);
		return description == null ? "" : description;
	}
	
	private void createAbbreviationsByUID() {
		abbreviationsByUID = new HashMap();

		abbreviationsByUID.put(SOPClass.Verification,"VFY");
		
		abbreviationsByUID.put(SOPClass.ComputedRadiographyImageStorage,"CR");
		abbreviationsByUID.put(SOPClass.DigitalXRayImageStorageForPresentation,"DX(Pres)");
		abbreviationsByUID.put(SOPClass.DigitalXRayImageStorageForProcessing,"DX(Proc)");
		abbreviationsByUID.put(SOPClass.DigitalMammographyXRayImageStorageForPresentation,"MG(Pres)");
		abbreviationsByUID.put(SOPClass.DigitalMammographyXRayImageStorageForProcessing,"MG(Proc)");
		abbreviationsByUID.put(SOPClass.DigitalIntraoralXRayImageStorageForPresentation,"IO(Pres)");
		abbreviationsByUID.put(SOPClass.DigitalIntraoralXRayImageStorageForProcessing,"IO(Proc)");
		abbreviationsByUID.put(SOPClass.CTImageStorage,"CT");
		abbreviationsByUID.put(SOPClass.EnhancedCTImageStorage,"CT(MF)");
		abbreviationsByUID.put(SOPClass.UltrasoundMultiframeImageStorageRetired,"US(MF)");
		abbreviationsByUID.put(SOPClass.UltrasoundMultiframeImageStorage,"US");
		abbreviationsByUID.put(SOPClass.MRImageStorage,"MR");
		abbreviationsByUID.put(SOPClass.EnhancedMRImageStorage,"MR(MF)");
		abbreviationsByUID.put(SOPClass.NuclearMedicineImageStorageRetired,"NM(Ret)");
		abbreviationsByUID.put(SOPClass.UltrasoundImageStorageRetired,"US(Ret)");
		abbreviationsByUID.put(SOPClass.UltrasoundImageStorage,"US");
		abbreviationsByUID.put(SOPClass.SecondaryCaptureImageStorage,"SC");
		abbreviationsByUID.put(SOPClass.MultiframeSingleBitSecondaryCaptureImageStorage,"SC(MF1Bit)");
		abbreviationsByUID.put(SOPClass.MultiframeGrayscaleByteSecondaryCaptureImageStorage,"SC(MFGrayByte)");
		abbreviationsByUID.put(SOPClass.MultiframeGrayscaleWordSecondaryCaptureImageStorage,"SC(MFGrayWord)");
		abbreviationsByUID.put(SOPClass.MultiframeTrueColorSecondaryCaptureImageStorage,"SC(MFColor)");
		abbreviationsByUID.put(SOPClass.XrayAngiographicImageStorage,"XA");
		abbreviationsByUID.put(SOPClass.XrayRadioFlouroscopicImageStorage,"RF");
		abbreviationsByUID.put(SOPClass.XrayAngiographicBiplaneImageStorage,"XA(Bi)");
		abbreviationsByUID.put(SOPClass.NuclearMedicineImageStorage,"NM");
		abbreviationsByUID.put(SOPClass.VisibleLightEndoscopicImageStorage,"VL(Endo)");				// ES ?
		abbreviationsByUID.put(SOPClass.VideoEndoscopicImageStorage,"VLMF(Endo)");				// ES ?
		abbreviationsByUID.put(SOPClass.VisibleLightMicroscopicImageStorage,"VL(Micro)");			// GM ?
		abbreviationsByUID.put(SOPClass.VideoMicroscopicImageStorage,"VLMF(Micro)");				// GM ?
		abbreviationsByUID.put(SOPClass.VisibleLightSlideCoordinatesMicroscopicImageStorage,"VL(Slide)");	// SM ?
		abbreviationsByUID.put(SOPClass.VisibleLightPhotographicImageStorage,"VL(Photo)");			// XC ?
		abbreviationsByUID.put(SOPClass.VideoPhotographicImageStorage,"VLMF(Photo)");				// XC ?
		abbreviationsByUID.put(SOPClass.PETImageStorage,"PET");
		abbreviationsByUID.put(SOPClass.RTImageStorage,"RTIMAGE");

		abbreviationsByUID.put(SOPClass.MediaStorageDirectoryStorage,"DICOMDIR");
		abbreviationsByUID.put(SOPClass.BasicTextSRStorage,"SR(Text)");
		abbreviationsByUID.put(SOPClass.EnhancedSRStorage,"SR(Enh)");
		abbreviationsByUID.put(SOPClass.ComprehensiveSRStorage,"SR(Comp)");
		abbreviationsByUID.put(SOPClass.MammographyCADSRStorage,"CAD(Mammo)");
		abbreviationsByUID.put(SOPClass.KeyObjectSelectionDocumentStorage,"KO");
		
		abbreviationsByUID.put(SOPClass.GrayscaleSoftcopyPresentationStateStorage,"PS");
		
		abbreviationsByUID.put(SOPClass.TwelveLeadECGStorage,"ECG(12)");
		abbreviationsByUID.put(SOPClass.GeneralECGStorage,"ECG(Gen)");
		abbreviationsByUID.put(SOPClass.AmbulatoryECGStorage,"ECG(Amb)");
		abbreviationsByUID.put(SOPClass.HemodynamicWaveformStorage,"HD");
		abbreviationsByUID.put(SOPClass.CardiacElectrophysiologyWaveformStorage,"EPS");
		abbreviationsByUID.put(SOPClass.BasicVoiceStorage,"AU");

		abbreviationsByUID.put(SOPClass.StandaloneOverlayStorage,"OVERLAY");
		abbreviationsByUID.put(SOPClass.StandaloneCurveStorage,"CURVE");
		abbreviationsByUID.put(SOPClass.StandaloneModalityLUTStorage,"MODLUT");
		abbreviationsByUID.put(SOPClass.StandaloneVOILUTStorage,"VOILUT");
		abbreviationsByUID.put(SOPClass.StandalonePETCurveStorage,"PETCURVE");
		
		abbreviationsByUID.put(SOPClass.RTDoseStorage,"RTDOSE");
		abbreviationsByUID.put(SOPClass.RTStructureSetStorage,"RTSTRUCT");
		abbreviationsByUID.put(SOPClass.RTBeamsTreatmentRecordStorage,"RTRECORD(Beams)");
		abbreviationsByUID.put(SOPClass.RTPlanStorage,"RTPLAN");
		abbreviationsByUID.put(SOPClass.RTBrachyTreatmentRecordStorage,"RTRECORD(Brachy)");
		abbreviationsByUID.put(SOPClass.RTTreatmentSummaryRecordStorage,"RTRECORD(Summary)");
		
		abbreviationsByUID.put(SOPClass.MRSpectroscopyStorage,"MR(Spectro)");
		
		abbreviationsByUID.put(SOPClass.RawDataStorage,"RAW");

		abbreviationsByUID.put(SOPClass.StudyRootQueryRetrieveInformationModelFind,"FIND(Study)");
		abbreviationsByUID.put(SOPClass.StudyRootQueryRetrieveInformationModelMove,"MOVE(Study)");
	}

	private void createDescriptionsByUID() {
		descriptionsByUID = new HashMap();

		descriptionsByUID.put(SOPClass.Verification,"Verification");
		
		descriptionsByUID.put(SOPClass.ComputedRadiographyImageStorage,"Computed Radiography Image Storage");
		descriptionsByUID.put(SOPClass.DigitalXRayImageStorageForPresentation,"Digital XRay Image Storage (For Presentation)");
		descriptionsByUID.put(SOPClass.DigitalXRayImageStorageForProcessing,"Digital XRay Image Storage (For Processing)");
		descriptionsByUID.put(SOPClass.DigitalMammographyXRayImageStorageForPresentation,"Digital Mammography XRay Image Storage (For Presentation)");
		descriptionsByUID.put(SOPClass.DigitalMammographyXRayImageStorageForProcessing,"Digital Mammography XRay Image Storage (For Processing)");
		descriptionsByUID.put(SOPClass.DigitalIntraoralXRayImageStorageForPresentation,"Digital Intraoral XRay Image Storage (For Presentation)");
		descriptionsByUID.put(SOPClass.DigitalIntraoralXRayImageStorageForProcessing,"Digital Intraoral XRay Image Storage (For Processing)");
		descriptionsByUID.put(SOPClass.CTImageStorage,"CT Image Storage");
		descriptionsByUID.put(SOPClass.UltrasoundMultiframeImageStorageRetired,"Ultrasound Multiframe Image Storage (Retired)");
		descriptionsByUID.put(SOPClass.UltrasoundMultiframeImageStorage,"Ultrasound Multiframe Image Storage");
		descriptionsByUID.put(SOPClass.MRImageStorage,"MR Image Storage");
		descriptionsByUID.put(SOPClass.EnhancedMRImageStorage,"Enhanced MR Image Storage");
		descriptionsByUID.put(SOPClass.NuclearMedicineImageStorageRetired,"Nuclear Medicine Image Storage (Retired)");
		descriptionsByUID.put(SOPClass.UltrasoundImageStorageRetired,"Ultrasound Image Storage (Retired)");
		descriptionsByUID.put(SOPClass.UltrasoundImageStorage,"Ultrasound Image Storage");
		descriptionsByUID.put(SOPClass.SecondaryCaptureImageStorage,"Secondary Capture Image Storage");
		descriptionsByUID.put(SOPClass.MultiframeSingleBitSecondaryCaptureImageStorage,"Multiframe Single Bit Secondary Capture Image Storage");
		descriptionsByUID.put(SOPClass.MultiframeGrayscaleByteSecondaryCaptureImageStorage,"Multiframe Grayscale Byte Secondary Capture Image Storage");
		descriptionsByUID.put(SOPClass.MultiframeGrayscaleWordSecondaryCaptureImageStorage,"Multiframe Grayscale Word Secondary Capture Image Storage");
		descriptionsByUID.put(SOPClass.MultiframeTrueColorSecondaryCaptureImageStorage,"Multiframe True Color Secondary Capture Image Storage");
		descriptionsByUID.put(SOPClass.XrayAngiographicImageStorage,"Xray Angiographic Image Storage");
		descriptionsByUID.put(SOPClass.XrayRadioFlouroscopicImageStorage,"Xray Radio Flouroscopic Image Storage");
		descriptionsByUID.put(SOPClass.XrayAngiographicBiplaneImageStorage,"Xray Angiographic Biplane Image Storage");
		descriptionsByUID.put(SOPClass.NuclearMedicineImageStorage,"Nuclear Medicine Image Storage");
		descriptionsByUID.put(SOPClass.VisibleLightEndoscopicImageStorage,"Visible Light Endoscopic Image Storage");
		descriptionsByUID.put(SOPClass.VideoEndoscopicImageStorage,"Video Endoscopic Image Storage");
		descriptionsByUID.put(SOPClass.VisibleLightMicroscopicImageStorage,"Visible Light Microscopic Image Storage");
		descriptionsByUID.put(SOPClass.VideoMicroscopicImageStorage,"Video Microscopic Image Storage");
		descriptionsByUID.put(SOPClass.VisibleLightSlideCoordinatesMicroscopicImageStorage,"Visible Light Slide Coordinates Microscopic Image Storage");
		descriptionsByUID.put(SOPClass.VisibleLightPhotographicImageStorage,"Visible Light Photographic Image Storage");
		descriptionsByUID.put(SOPClass.VideoPhotographicImageStorage,"Video Photographic Image Storage");
		descriptionsByUID.put(SOPClass.PETImageStorage,"PET Image Storage");
		descriptionsByUID.put(SOPClass.RTImageStorage,"RT Image Storage");

		descriptionsByUID.put(SOPClass.MediaStorageDirectoryStorage,"Media Storage Directory Storage");
		descriptionsByUID.put(SOPClass.BasicTextSRStorage,"Basic Text SR Storage");
		descriptionsByUID.put(SOPClass.EnhancedSRStorage,"Enhanced SR Storage");
		descriptionsByUID.put(SOPClass.ComprehensiveSRStorage,"Comprehensive SR Storage");
		descriptionsByUID.put(SOPClass.MammographyCADSRStorage,"Mammography CAD SR Storage");
		descriptionsByUID.put(SOPClass.KeyObjectSelectionDocumentStorage,"Key Object Selection Document Storage");
		
		descriptionsByUID.put(SOPClass.GrayscaleSoftcopyPresentationStateStorage,"Grayscale Softcopy Presentation State Storage");
		
		descriptionsByUID.put(SOPClass.TwelveLeadECGStorage,"Twelve Lead ECG Storage");
		descriptionsByUID.put(SOPClass.GeneralECGStorage,"General ECG Storage");
		descriptionsByUID.put(SOPClass.AmbulatoryECGStorage,"Ambulatory ECG Storage");
		descriptionsByUID.put(SOPClass.HemodynamicWaveformStorage,"Hemodynamic Waveform Storage");
		descriptionsByUID.put(SOPClass.CardiacElectrophysiologyWaveformStorage,"Cardiac Electrophysiology Waveform Storage");
		descriptionsByUID.put(SOPClass.BasicVoiceStorage,"Basic Voice Storage");

		descriptionsByUID.put(SOPClass.StandaloneOverlayStorage,"Standalone Overlay Storage");
		descriptionsByUID.put(SOPClass.StandaloneCurveStorage,"Standalone Curve Storage");
		descriptionsByUID.put(SOPClass.StandaloneModalityLUTStorage,"Standalone Modality LUT Storage");
		descriptionsByUID.put(SOPClass.StandaloneVOILUTStorage,"Standalone VOI LUT Storage");
		descriptionsByUID.put(SOPClass.StandalonePETCurveStorage,"Standalone PET Curve Storage");
		
		descriptionsByUID.put(SOPClass.RTDoseStorage,"RT Dose Storage");
		descriptionsByUID.put(SOPClass.RTStructureSetStorage,"RT Structure Set Storage");
		descriptionsByUID.put(SOPClass.RTBeamsTreatmentRecordStorage,"RT Beams Treatment Record Storage");
		descriptionsByUID.put(SOPClass.RTPlanStorage,"RT Plan Storage");
		descriptionsByUID.put(SOPClass.RTBrachyTreatmentRecordStorage,"RT Brachy Treatment Record Storage");
		descriptionsByUID.put(SOPClass.RTTreatmentSummaryRecordStorage,"RT Treatment Summary Record Storage");
		
		descriptionsByUID.put(SOPClass.MRSpectroscopyStorage,"MR Spectroscopy Storage");
		
		descriptionsByUID.put(SOPClass.RawDataStorage,"Raw Data Storage");

		descriptionsByUID.put(SOPClass.StudyRootQueryRetrieveInformationModelFind,"Study Root Query Retrieve Information Model Find");
		descriptionsByUID.put(SOPClass.StudyRootQueryRetrieveInformationModelMove,"Study Root Query Retrieve Information Model Move");
	}

	/**
	 * <p>Unit test.</p>
	 *
	 * @param	arg	ignored
	 */
	public static void main(String arg[]) {

		try {
			System.err.println(getDescriptionFromUID(SOPClass.MRSpectroscopyStorage));
			System.err.println(getAbbreviationFromUID(SOPClass.MRSpectroscopyStorage));
		} catch (Exception e) {
			System.err.println(e);
			e.printStackTrace(System.err);
			System.exit(0);
		}
	}
}
