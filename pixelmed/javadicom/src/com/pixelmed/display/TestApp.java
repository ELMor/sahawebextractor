/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.display;

import java.awt.*; 
import java.awt.event.*; 
import java.awt.image.*; 
import java.awt.color.*; 
import java.util.*; 
import java.io.*; 
import javax.swing.*; 
import javax.swing.event.*;

import com.pixelmed.event.ApplicationEventDispatcher; 
import com.pixelmed.display.event.*; 
import com.pixelmed.dicom.*;

/**
 * @author	dclunie
 */
class TestApp extends ApplicationFrame {

	private static final String identString = "@(#) $Header$";

	/**
	 * @param	arg
	 */
	public static void main(String arg[]) { 
		TestApp af = new TestApp();

		BufferedImage img = null;
		//SourceImage sImg = null;

		int imagesPerRow=0;
		int imagesPerCol=0;

		int imgMin=65536;
		int imgMax=0;

		boolean signed=false;
		boolean inverted=false;

		if (arg.length == 6) {
			// do it with raw file
			int w=0;
			int h=0;
			int d=0;
			try {
				w=Integer.valueOf(arg[1]).intValue();
				h=Integer.valueOf(arg[2]).intValue();
				d=Integer.valueOf(arg[3]).intValue();
				imagesPerRow=Integer.valueOf(arg[4]).intValue();
				imagesPerCol=Integer.valueOf(arg[5]).intValue();
			} catch (Exception e) {
				System.err.println(e);
				System.exit(0);
			}

			try {
				FileInputStream i = new FileInputStream(arg[0]);
				SourceImage sImg=new SourceImage(i,w,h,d);
				//sImg=new SourceImage(i,w,h,d);
				img=sImg.getBufferedImage();
				imgMin=sImg.getMinimum();
				imgMax=sImg.getMaximum();
			} catch (Exception e) {
				System.err.println(e);
				System.exit(0);
			}
		}
		else {
			// do it with DICOM file

			if (arg.length > 2) {
				try {
					imagesPerRow=Integer.valueOf(arg[1]).intValue();
					imagesPerCol=Integer.valueOf(arg[2]).intValue();
				} catch (Exception e) {
					System.err.println(e);
					e.printStackTrace(System.err);
					System.exit(0);
				}
			}
			else {
				imagesPerRow=1;
				imagesPerCol=1;
			}

			try {
				DicomInputStream i = new DicomInputStream(new FileInputStream(arg[0]));
				SourceImage sImg=new SourceImage(i);
				//sImg=new SourceImage(i);
				img=sImg.getBufferedImage();
				imgMin=sImg.getMinimum();
				imgMax=sImg.getMaximum();
				signed=signed||sImg.isSigned();
				inverted=inverted||sImg.isInverted();
			} catch (Exception e) {
				System.err.println(e);
				e.printStackTrace(System.err);
				System.exit(0);
			}
		}

		try {
			new ApplicationEventDispatcher();
		}
		catch (Exception e) {
			e.printStackTrace(System.err);
		}
		
		JPanel multiPanel = new JPanel();
		multiPanel.setLayout(new GridLayout(imagesPerCol,imagesPerRow));
		multiPanel.setBackground(Color.black);

		SingleImagePanel imagePanel[] = new SingleImagePanel[imagesPerRow*imagesPerCol];

		for (int x=0; x<imagesPerCol; ++x) {
			for (int y=0; y<imagesPerRow; ++y) {
				SingleImagePanel ip = new SingleImagePanel(img,null/*EventContext*/,imgMin,imgMax,signed,inverted);
				//SingleImagePanel ip = new SingleImagePanel(sImg,af);
				ip.setPreferredSize(new Dimension(img.getWidth(),img.getHeight()));
				//ip.setPreferredSize(new Dimension(sImg.getWidth(),sImg.getHeight()));
				multiPanel.add(ip);
				imagePanel[x*imagesPerRow+y]=ip;
			}
		}


		//multiPanel.setSize(img.getWidth()*imagesPerRow,img.getHeight()*imagesPerRow);

		JScrollPane scrollPane = new JScrollPane(multiPanel);

		Container content = af.getContentPane();
		content.setLayout(new GridLayout(1,1));
		content.add(scrollPane);

		af.pack();

		int frameHeight=scrollPane.getHeight()+24;
		if (frameHeight>1024) frameHeight=1024;
		int frameWidth=scrollPane.getWidth()+24;
		if (frameWidth>1280) frameWidth=1280;
		af.setSize(frameWidth,frameHeight);
		af.show();
       } 

}






