/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.display;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

import java.awt.Font;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.BandCombineOp;
import java.awt.image.ComponentColorModel;
import java.awt.image.ComponentSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;

import javax.swing.JComponent;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingUtilities;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import com.pixelmed.event.ApplicationEventDispatcher;
import com.pixelmed.event.Event;
import com.pixelmed.event.EventContext;
import com.pixelmed.event.SelfRegisteringListener; 
import com.pixelmed.display.event.FrameSelectionChangeEvent; 
import com.pixelmed.display.event.FrameSortOrderChangeEvent; 
import com.pixelmed.display.event.RegionSelectionChangeEvent; 
import com.pixelmed.display.event.StatusChangeEvent; 
import com.pixelmed.display.event.VOIFunctionChangeEvent; 
import com.pixelmed.display.event.WindowCenterAndWidthChangeEvent; 
import com.pixelmed.dicom.RealWorldValueTransform;
import com.pixelmed.dicom.ModalityTransform;
import com.pixelmed.dicom.VOITransform;
import com.pixelmed.utils.FloatFormatter;

// for localizer ...

import javax.vecmath.*;
import com.pixelmed.geometry.*;

/**
 * <p>Implements a component that can display a single or multi-frame image in a
 * single panel with window center/width adjustment, scrolling through frames
 * of a multi-frame image, resizing to the size of the panel, annotation
 * of pre-defined text and shapes, feedback of cursor position status (2D and
 * 3D position, pixel value, window).</p>
 *
 * @see com.pixelmed.display.SourceImage
 *
 * @author	dclunie
 */
public class SingleImagePanel extends JComponent implements KeyListener, MouseListener, MouseMotionListener {

	/***/
	private static final String identString = "@(#) $Header$";

	/***/
	BufferedImage[] srcImages;
	/***/
	int nSrcImages;
	/***/
	int currentSrcImageIndex;
	/***/
	int[] currentSrcImageSortOrder;

	/***/
	BufferedImage cachedResizedImage;
	/***/
	BufferedImage cachedPreWindowedImage;

	/***/
	double windowCenter;
	/***/
	double windowWidth;

	/**
	 * the value of rescale slope to use for current frame (set when new frame selected)
	 */
	double useSlope;
	/**
	 * the value of rescale intercept to use for current frame (set when new frame selected)
	 */
	double useIntercept;

	/***/
	int imgMin;
	/***/
	int imgMax;

	/***/
	private int largestGray;
	/***/
	private int firstvalueMapped;
	/***/
	private int numberOfEntries;
	/***/
	private int bitsPerEntry;
	/***/
	private short redTable[];
	/***/
	private short greenTable[];
	/***/
	private short blueTable[];

	/***/
	private boolean signed;
	/***/
	private boolean inverted;
	/***/
	private int signMask;
	/***/
	private int signBit;
	
	/***/
	RealWorldValueTransform realWorldValueTransform;

	/***/
	ModalityTransform modalityTransform;

	/***/
	VOITransform voiTransform;
	
	/***/
	private GeometryOfVolume imageGeometry;
	
	/**
	 * <p>Get the geometry of the frames currently loaded in the single image panel.</p>
	 *
	 * @return	the geometry of the frames
	 */
	public GeometryOfVolume getImageGeometry() {
		return imageGeometry;
	}

	/***/
	Vector preDefinedShapes;			// of Shape
	/***/
	Vector preDefinedText;
	
	/***/
	private Vector localizerShapes;			// of Shape
	
	/***/
	private Vector volumeLocalizationShapes;	// of Shape
	
	
	/**
	 * <p>Set the predefined shapes to to be displayed on the currently selected and displayed frame.</p>
	 *
	 * @param	shapes	a {@link java.util.Vector java.util.Vector} of {@link java.awt.Shape java.awt.Shape}, may be null or empty
	 */
	public final void setPreDefinedShapes(Vector shapes) { this.preDefinedShapes=shapes; }

	/**
	 * <p>Set the shapes of any localizer postings to be displayed on the currently selected and displayed frame.</p>
	 *
	 * @param	shapes	a {@link java.util.Vector java.util.Vector} of {@link java.awt.Shape java.awt.Shape}, may be null or empty
	 */
	public final void setLocalizerShapes(Vector shapes) { this.localizerShapes=shapes; }

	/**
	 * <p>Set the shapes of any volume localization postings to be displayed on the currently selected and displayed frame.</p>
	 *
	 * @param	shapes	a {@link java.util.Vector java.util.Vector} of {@link java.awt.Shape java.awt.Shape}, may be null or empty
	 */
	public final void setVolumeLocalizationShapes(Vector shapes) { this.volumeLocalizationShapes=shapes; }


	/***/
	private DemographicAndTechniqueAnnotations demographicAndTechniqueAnnotations = null;
	/***/
	private Color demographicAndTechniqueColor;
	/***/
	private Font demographicAndTechniqueFont;

	/**
	 * <p>Set annotative attribute values to be displayed on the currently selected and displayed frame at defined positions.</p>
	 *
	 * @param	demographicAndTechniqueAnnotations	the set of annotations, indexed per frame
	 * @param	demographicAndTechniqueFontName		the name of the font, e.g. "SansSerif"
	 * @param	demographicAndTechniqueFontStyle	the style of the font, e.g. Font.PLAIN
	 * @param	demographicAndTechniqueFontSize		the size of the font in points, e.g. 10
	 * @param	demographicAndTechniqueColor		the color to draw, e.g. Color.pink
	 */
	public final void setDemographicAndTechniqueAnnotations(DemographicAndTechniqueAnnotations demographicAndTechniqueAnnotations,
			String demographicAndTechniqueFontName,int demographicAndTechniqueFontStyle,int demographicAndTechniqueFontSize,Color demographicAndTechniqueColor) {
		this.demographicAndTechniqueAnnotations=demographicAndTechniqueAnnotations;
		this.demographicAndTechniqueFont = new Font(demographicAndTechniqueFontName,demographicAndTechniqueFontStyle,demographicAndTechniqueFontSize);
		this.demographicAndTechniqueColor=demographicAndTechniqueColor;
	}

	/***/
	private OrientationAnnotations orientationAnnotations = null;
	/***/
	private Color orientationColor;
	/***/
	private Font orientationFont;

	/**
	 * <p>Set orientation values to be displayed on the currently selected and displayed frame.</p>
	 *
	 * @param	orientationAnnotations	the set of orientations, indexed per frame
	 * @param	orientationFontName	the name of the font, e.g. "SansSerif"
	 * @param	orientationFontStyle	the style of the font, e.g. Font.PLAIN
	 * @param	orientationFontSize	the size of the font in points, e.g. 20
	 * @param	orientationColor	the color to draw, e.g. Color.pink
	 */
	public final void setOrientationAnnotations(OrientationAnnotations orientationAnnotations,
			String orientationFontName,int orientationFontStyle,int orientationFontSize,Color orientationColor) {
		this.orientationAnnotations=orientationAnnotations;
		this.orientationFont = new Font(orientationFontName,orientationFontStyle,orientationFontSize);
		this.orientationColor=orientationColor;
	}
	
	/***/
	private double useScaleFactor = 1;
	
	/***/
	private int useVOIFunction = 0;		// 0 is linear, 1 is logistic
	
	/**
	 * <p>Set the VOI function to the (default) window center/width linear transformation.</p>
	 */
	public final void setVOIFunctionToLinear() {
//System.err.println("SingleImagePanel.setVOIFunctionToLinear()");
		useVOIFunction = 0;
	}

	/**
	 * <p>Set the VOI function to a non-linear transformation using a logistic (sigmoid) curve with window center and width as parameters.</p>
	 */
	public final void setVOIFunctionToLogistic() {
//System.err.println("SingleImagePanel.setVOIFunctionToLogistic()");
		useVOIFunction = 1;
	}

	// Region selection stuff (set by right mouse drag) ...
	
	private int regionSelectionCenterX;
	private int regionSelectionCenterY;
	private int regionSelectionTLHCX;
	private int regionSelectionTLHCY;
	private int regionSelectionBRHCX;
	private int regionSelectionBRHCY;

	/**
	 * @param	centerX
	 * @param	centerY
	 * @param	oneCornerX
	 * @param	oneCornerY
	 * @param	otherCornerX
	 * @param	otherCornerY
	 */
	private void setRegionSelection(int centerX,int centerY,int oneCornerX,int oneCornerY,int otherCornerX,int otherCornerY) {
//System.err.println("SingleImagePanel.setRegionSelection() event: centerX="+centerX+" centerY="+centerY+" oneCornerX="+oneCornerX+" oneCornerY="+oneCornerY+" otherCornerX="+otherCornerX+" otherCornerY="+otherCornerY);
		regionSelectionCenterX = centerX;
		regionSelectionCenterY = centerY;
		if (oneCornerX < otherCornerX) {
			regionSelectionTLHCX=oneCornerX;
			regionSelectionBRHCX=otherCornerX;
		}
		else {
			regionSelectionTLHCX=otherCornerX;
			regionSelectionBRHCX=oneCornerX;
		}
		if (oneCornerY < otherCornerY) {
			regionSelectionTLHCY=oneCornerY;
			regionSelectionBRHCY=otherCornerY;
		}
		else {
			regionSelectionTLHCY=otherCornerY;
			regionSelectionBRHCY=oneCornerY;
		}
	}

	// Event stuff ...

	/***/
	EventContext typeOfPanelEventContext;
	
	/***/
	int lastx;
	/***/
	int lasty;

	/***/
	int lastmiddley;
	
	/***/
	double windowingMultiplier = 1;
	
	/***/
	int trackWidth;
	/***/
	int trackHeight;

	/**
	 * if -1, then use statistical value first time then user adjusted values subsequently
	 * if >= 0, then use selected VOI transform (if there is one for that frame)
	 */
	int currentVOITransformInUse;

	/**
	 * @param	e
	 */
	public void keyPressed(KeyEvent e) {
//System.err.println("Key pressed event"+e);
		if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
//System.err.println("Shift pressed");
			windowingMultiplier=50;
		}
	}

	/**
	 * @param	e
	 */
	public void keyReleased(KeyEvent e) {
//System.err.println("Key released event"+e);
		if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
//System.err.println("Shift released");
			windowingMultiplier=1;
		}
	}

	/**
	 * @param	e
	 */
	public void keyTyped(KeyEvent e) {
//System.err.println("Key typed event"+e);
	}

	/**
	 * @param	e
	 */
	public void mouseClicked(MouseEvent e) {}

	/**
	 * @param	e
	 */
	public void mouseEntered(MouseEvent e) {
		requestFocus();		// In order to allow us to receive KeyEvents
	}

	/**
	 * @param	e
	 */
	public void mouseExited(MouseEvent e) {}

	/**
	 * @param	e
	 */
	public void mouseDragged(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			currentVOITransformInUse=-1;		// override pre-specified VOI tranform with user selection
//System.err.println("Left dragged "+e.getX()+" "+e.getY());
			int newx=e.getX();
			int newy=e.getY();
			int deltax=(int)((newx-lastx)*windowingMultiplier);
			int deltay=(int)((newy-lasty)*windowingMultiplier);

			if (deltax != 0 || deltay != 0) {
				lastx=newx;
				lasty=newy;
				double newWindowWidth=windowWidth+deltax;
				if (newWindowWidth < 1.0) newWindowWidth=1.0;
				double newWindowCenter=windowCenter+deltay;
				ApplicationEventDispatcher.applicationEventDispatcher.processEvent(new WindowCenterAndWidthChangeEvent(typeOfPanelEventContext,newWindowCenter,newWindowWidth));
			}
		}
		else if (SwingUtilities.isMiddleMouseButton(e)) {
//System.err.println("Middle dragged "+e.getX()+" "+e.getY());
			//selectNewSrcImage(e.getY()-lastmiddley);
			int delta = e.getY()-lastmiddley;
//System.err.println("Middle dragged delta = "+delta);
//System.err.println("Middle dragged currentSrcImageIndex = "+currentSrcImageIndex);
			int newSrcImageIndex = currentSrcImageIndex + delta;
//System.err.println("Middle dragged newSrcImageIndex = "+newSrcImageIndex);
			if (newSrcImageIndex >= nSrcImages) newSrcImageIndex=nSrcImages-1;
			if (newSrcImageIndex < 0) newSrcImageIndex=0;
//System.err.println("Middle dragged newSrcImageIndex clipped = "+newSrcImageIndex);
			// don't send an event unless it is actually necessary ...
			if (newSrcImageIndex != currentSrcImageIndex)
				ApplicationEventDispatcher.applicationEventDispatcher.processEvent(new FrameSelectionChangeEvent(typeOfPanelEventContext,newSrcImageIndex));
			lastmiddley=e.getY();		// helps a lot when clipped at top or bottom of range
		}
		else if (SwingUtilities.isRightMouseButton(e)) {
//System.err.println("Right dragged "+e.getX()+" "+e.getY());
			dragInteractiveDrawing(e.getX(),e.getY());
		}
	}


//	private void selectNewSrcImage(int delta) {
//		int newSrcImageIndex = currentSrcImageIndex + delta;
//		if (newSrcImageIndex >= nSrcImages) newSrcImageIndex=nSrcImages-1;
//		if (newSrcImageIndex < 0) newSrcImageIndex=0;
//		if (newSrcImageIndex != currentSrcImageIndex) {
//			currentSrcImageIndex=newSrcImageIndex;
//			cachedPreWindowedImage=null;		// forces application of new values on repaint 
//			repaint();
//		}
//	}

	/**
	 * This value is outside mouseMoved() only so that it doesn't need to be constantly reallocated - it is not used by any other method
	 */
	private double[] currentLocationIn3DSpace = new double[3];

	/**
	 * @param	e
	 */
	public void mouseMoved(MouseEvent e) {
//System.err.println(e.getX()+" "+e.getY());
		{
			double x = e.getX();
			double y = e.getY();
			// truncate, do not round
			int originalX = (int)(x/useScaleFactor);
			int originalY = (int)(y/useScaleFactor);
			// clamp to bounds of original which is in cachedPreWindowedImage, since displayed image may be padded
			//int maxX = cachedPreWindowedImage.getWidth()-1;
			int maxX = trackWidth-1;
			if (originalX > maxX) originalX=maxX;
			//int maxY = cachedPreWindowedImage.getHeight()-1;
			int maxY =trackHeight-1;
			if (originalY > maxY) originalY=maxY;
		
//System.err.println("X: "+originalX+" ("+x+") Y: "+originalY+" ("+y+")");

			int useSrcImageIndex = currentSrcImageSortOrder == null ? currentSrcImageIndex : currentSrcImageSortOrder[currentSrcImageIndex];

			StringBuffer sbuf = new StringBuffer();
			sbuf.append("(");
			sbuf.append(originalX);
			sbuf.append(",");
			sbuf.append(originalY);
			if (imageGeometry != null) {
				imageGeometry.lookupImageCoordinate(currentLocationIn3DSpace,originalX,originalY,useSrcImageIndex);
				{
					sbuf.append(": ");
					sbuf.append(FloatFormatter.toString(currentLocationIn3DSpace[0]));
					sbuf.append(",");
					sbuf.append(FloatFormatter.toString(currentLocationIn3DSpace[1]));
					sbuf.append(",");
					sbuf.append(FloatFormatter.toString(currentLocationIn3DSpace[2]));
				}
			}
			sbuf.append(")");
		
			BufferedImage src = srcImages[useSrcImageIndex];
			int[] storedPixelValues  = src.getSampleModel().getPixel(originalX,originalY,(int[])null,src.getRaster().getDataBuffer());
			int storedPixelValue=storedPixelValues[0];
//System.err.println("storedPixelValue as stored = 0x"+Integer.toHexString(storedPixelValue)+" "+storedPixelValue+" dec");
			if (signed && (storedPixelValue&signBit) != 0) {
				storedPixelValue|=signMask;	// sign extend
//System.err.println("storedPixelValue extended  = 0x"+Integer.toHexString(storedPixelValue)+" "+storedPixelValue+" dec");
			}
			
			if (realWorldValueTransform != null) {
				sbuf.append(" = ");
				sbuf.append(realWorldValueTransform.toString(useSrcImageIndex,storedPixelValue));
				sbuf.append(" [");
				sbuf.append(storedPixelValue);
				sbuf.append("]");
			}
			
			ApplicationEventDispatcher.applicationEventDispatcher.processEvent(new StatusChangeEvent(sbuf.toString()));
		}
	}

	/**
	 * @param	e
	 */
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
//System.err.println("Left pressed "+e.getX()+" "+e.getY());
			lastx=e.getX();
			lasty=e.getY();
		}
		else if (SwingUtilities.isMiddleMouseButton(e)) {
//System.err.println("Middle pressed "+e.getX()+" "+e.getY());
			lastmiddley=e.getY();
		}
		else if (SwingUtilities.isRightMouseButton(e)) {
//System.err.println("Right pressed "+e.getX()+" "+e.getY());
			startInteractiveDrawing(e.getX(),e.getY());
		}
	}

	/**
	 * @param	e
	 */
	public void mouseReleased(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
//System.err.println("Left released "+e.getX()+" "+e.getY());
			// on button release (but not during drag) propagate changed window values to self (a nop) and other registered SingleImagePanels ...
			ApplicationEventDispatcher.applicationEventDispatcher.processEvent(new WindowCenterAndWidthChangeEvent(typeOfPanelEventContext,windowCenter,windowWidth));
		}
		else if (SwingUtilities.isMiddleMouseButton(e)) {
//System.err.println("Middle released "+e.getX()+" "+e.getY());
		}
		else if (SwingUtilities.isRightMouseButton(e)) {
//System.err.println("Right released "+e.getX()+" "+e.getY());
			endInteractiveDrawing(e.getX(),e.getY());	// sets region selection parameters to propagate in change event
			ApplicationEventDispatcher.applicationEventDispatcher.processEvent(new RegionSelectionChangeEvent(typeOfPanelEventContext,
				regionSelectionCenterX,regionSelectionCenterY,regionSelectionTLHCX,regionSelectionTLHCY,regionSelectionBRHCX,regionSelectionBRHCY));
		}
	}

	// implement WindowCenterAndWidthChangeListener to respond to events from self or other SingleImagePanel or elsewhere ...
	
	private OurWindowCenterAndWidthChangeListener ourWindowCenterAndWidthChangeListener;

	class OurWindowCenterAndWidthChangeListener extends SelfRegisteringListener {
	
		public OurWindowCenterAndWidthChangeListener(EventContext eventContext) {
			super("com.pixelmed.display.event.WindowCenterAndWidthChangeEvent",eventContext);
//System.err.println("SingleImagePanel.OurWindowCenterAndWidthChangeListener():");
		}
		
		/**
		 * @param	e
		 */
		public void changed(Event e) {
			WindowCenterAndWidthChangeEvent wcwe = (WindowCenterAndWidthChangeEvent)e;
			double newWindowCenter=wcwe.getWindowCenter();
			double newWindowWidth=wcwe.getWindowWidth();
			if (windowCenter != newWindowCenter || windowWidth != newWindowWidth) {
//System.err.println("SingleImagePanel.OurWindowCenterAndWidthChangeListener.changed(): new values");
				windowCenter=newWindowCenter;
				windowWidth=newWindowWidth;
				cachedPreWindowedImage=null;		// forces application of new values on repaint 
				repaint();
			}
			else {
//System.err.println("SingleImagePanel.OurWindowCenterAndWidthChangeListener.changed(): same values");
			}
		}
	}
	
	// implement FrameSelectionChangeListener to respond to events from self or elsewhere ...
	
	private OurFrameSelectionChangeListener ourFrameSelectionChangeListener;

	class OurFrameSelectionChangeListener extends SelfRegisteringListener {
	
		public OurFrameSelectionChangeListener(EventContext eventContext) {
			super("com.pixelmed.display.event.FrameSelectionChangeEvent",eventContext);
//System.err.println("SingleImagePanel.OurFrameSelectionChangeListener():");
		}
		
		/**
		 * @param	e
		 */
		public void changed(Event e) {
			FrameSelectionChangeEvent fse = (FrameSelectionChangeEvent)e;
			int newCurrentSrcImageIndex = fse.getIndex();
			if (currentSrcImageIndex != newCurrentSrcImageIndex) {
//System.err.println("SingleImagePanel.OurFrameSelectionChangeListener.changed(): new values");
				currentSrcImageIndex=newCurrentSrcImageIndex;
				cachedResizedImage=null;				// forces application of new values on repaint 
				cachedPreWindowedImage=null;				// forces application of new values on repaint 
				repaint();
			}
			else {
//System.err.println("SingleImagePanel.OurFrameSelectionChangeListener.changed(): same values");
			}
		}
	}
	
	// implement FrameSortOrderChangeListener to respond to events from self or other SingleImagePanel or elsewhere ...
	
	private OurFrameSortOrderChangeListener ourFrameSortOrderChangeListener;

	class OurFrameSortOrderChangeListener extends SelfRegisteringListener {
	
		public OurFrameSortOrderChangeListener(EventContext eventContext) {
			super("com.pixelmed.display.event.FrameSortOrderChangeEvent",eventContext);
//System.err.println("SingleImagePanel.OurFrameSortOrderChangeListener():");
		}
		
		/**
		 * @param	e
		 */
		public void changed(Event e) {
			FrameSortOrderChangeEvent fso = (FrameSortOrderChangeEvent)e;
			int[] newSrcImageSortOrder = fso.getSortOrder();
			int newCurrentSrcImageIndex = fso.getIndex();
			if (currentSrcImageIndex != newCurrentSrcImageIndex
			 || currentSrcImageSortOrder != newSrcImageSortOrder
			 || !Arrays.equals(currentSrcImageSortOrder,newSrcImageSortOrder)) {
//System.err.println("SingleImagePanel.OurFrameSortOrderChangeListener.changed(): new values");
				currentSrcImageIndex=newCurrentSrcImageIndex;
				currentSrcImageSortOrder=newSrcImageSortOrder;		// change even if null in event (request to go back to implicit order)
				cachedResizedImage=null;				// forces application of new values on repaint 
				cachedPreWindowedImage=null;				// forces application of new values on repaint 
				repaint();
			}
			else {
//System.err.println("SingleImagePanel.OurFrameSortOrderChangeListener.changed(): same values");
			}
		}
	}
	
	// implement VOIFunctionChangeListener to respond to events from elsewhere ...
	
	private OurVOIFunctionChangeListener ourVOIFunctionChangeListener;

	class OurVOIFunctionChangeListener extends SelfRegisteringListener {
	
		public OurVOIFunctionChangeListener(EventContext eventContext) {
			super("com.pixelmed.display.event.VOIFunctionChangeEvent",eventContext);
//System.err.println("SingleImagePanel.OurVOIFunctionChangeListener():");
		}
		
		/**
		 * @param	e
		 */
		public void changed(Event e) {
			VOIFunctionChangeEvent vfce = (VOIFunctionChangeEvent)e;
			if (vfce.isLinearFunction()) {
				setVOIFunctionToLinear();
			}
			else if (vfce.isLogisticFunction()) {
				setVOIFunctionToLogistic();
			}
			cachedPreWindowedImage=null;		// forces reapplication of window values on repaint of this SingleImagePanel only
			repaint();				// force repaint
		}
	}
	
	// called by paintComponent() first time or after change create image with window values applied ...
	
	/**
	 * @param	src
	 * @param	center
	 * @param	width
	 * @param	signed
	 * @param	inverted
	 * @param	useSlope
	 * @param	useIntercept
	 */
	public static final BufferedImage applyWindowCenterAndWidthLogistic(BufferedImage src,double center,double width,
			boolean signed,boolean inverted,double useSlope,double useIntercept) {
		return WindowCenterAndWidth.applyWindowCenterAndWidthLogistic(src,center,width,signed,inverted,useSlope,useIntercept);
	}

	/**
	 * @param	src
	 * @param	center
	 * @param	width
	 * @param	signed
	 * @param	inverted
	 * @param	useSlope
	 * @param	useIntercept
	 */
	public static final BufferedImage applyWindowCenterAndWidthLinear(BufferedImage src,double center,double width,
			boolean signed,boolean inverted,double useSlope,double useIntercept) {
		return WindowCenterAndWidth.applyWindowCenterAndWidthLinear(src,center,width,signed,inverted,useSlope,useIntercept);
	}

	/**
	 * @param	src
	 * @param	center
	 * @param	width
	 * @param	signed
	 * @param	inverted
	 * @param	useSlope
	 * @param	useIntercept
	 * @param	largestGray
	 * @param	bitsPerEntry
	 * @param	numberOfEntries
	 * @param	redTable
	 * @param	greenTable
	 * @param	blueTable
	 */
	public static final BufferedImage applyWindowCenterAndWidthWithPaletteColor(BufferedImage src,double center,double width,
			boolean signed,boolean inverted,double useSlope,double useIntercept,
			int largestGray,int bitsPerEntry,int numberOfEntries,
			short[] redTable,short[] greenTable,short[] blueTable) {
		return WindowCenterAndWidth.applyWindowCenterAndWidthWithPaletteColor(src,center,width,signed,inverted,useSlope,useIntercept,
			largestGray,bitsPerEntry,numberOfEntries,redTable,greenTable,blueTable);
	}
	
	// Common constructor support ...

	/**
	 * @param	img
	 * @param	typeOfPanelEventContext			the context to use for all centrally dispatched events
	 * @param	largestGray
	 * @param	firstvalueMapped
	 * @param	numberOfEntries
	 * @param	bitsPerEntry
	 * @param	redTable
	 * @param	greenTable
	 * @param	blueTable
	 * @param	iMin
	 * @param	iMax
	 * @param	iMean
	 * @param	iSD
	 * @param	sign
	 * @param	invert
	 * @param	realWorldValueTransform
	 * @param	modalityTransform
	 * @param	voiTransform
	 * @param	preDefinedShapes
	 * @param	preDefinedText
	 * @param	imageGeometry
	 */
	private void doCommonConstructorStuff(BufferedImage img,
			EventContext typeOfPanelEventContext,
			int largestGray,int firstvalueMapped,int numberOfEntries,int bitsPerEntry,short redTable[],short greenTable[],short blueTable[],
			int iMin,int iMax,double iMean,double iSD,boolean sign,boolean invert,
			RealWorldValueTransform realWorldValueTransform,
			ModalityTransform modalityTransform,
			VOITransform voiTransform,
			Vector preDefinedShapes,Vector preDefinedText,
			GeometryOfVolume imageGeometry) {
		BufferedImage[] imgs = new BufferedImage[1];
		imgs[0]=img;
		doCommonConstructorStuff(imgs,
			typeOfPanelEventContext,
			null/* no sortOrder */,
			largestGray,firstvalueMapped,numberOfEntries,bitsPerEntry,redTable,greenTable,blueTable,
			iMin,iMax,iMean,iSD,sign,invert,
			realWorldValueTransform,modalityTransform,voiTransform,
			preDefinedShapes,preDefinedText,
			imageGeometry);
	}

	/***/
	private boolean useConvertToMostFavorableImageType;	// used in paintComponent()

	/**
	 * @param	imgs
	 * @param	typeOfPanelEventContext
	 * @param	sortOrder
	 * @param	largestGray
	 * @param	firstvalueMapped
	 * @param	numberOfEntries
	 * @param	bitsPerEntry
	 * @param	redTable
	 * @param	greenTable
	 * @param	blueTable
	 * @param	iMin
	 * @param	iMax
	 * @param	iMean
	 * @param	iSD
	 * @param	signed
	 * @param	inverted
	 * @param	realWorldValueTransform
	 * @param	modalityTransform
	 * @param	voiTransform
	 * @param	preDefinedShapes
	 * @param	preDefinedText
	 * @param	imageGeometry
	 */
	private void doCommonConstructorStuff(BufferedImage[] imgs,
			EventContext typeOfPanelEventContext,
			int[] sortOrder,
			int largestGray,int firstvalueMapped,int numberOfEntries,int bitsPerEntry,short redTable[],short greenTable[],short blueTable[],
			int iMin,int iMax,double iMean,double iSD,boolean signed,boolean inverted,
			RealWorldValueTransform realWorldValueTransform,
			ModalityTransform modalityTransform,
			VOITransform voiTransform,
			Vector preDefinedShapes,Vector preDefinedText,
			GeometryOfVolume imageGeometry) {
//System.err.println("SingleImagePanel.doCommonConstructorStuff():");
		boolean convertNonGrayscale = false;
		if (imgs != null) {
//System.err.println("SingleImagePanel.doCommonConstructorStuff(): imgs != null");
			BufferedImage img=imgs[0];
		//	SampleModel sampleModel = img.getSampleModel();
			if (img.getRaster().getNumBands() > 1) {
		//		if (sampleModel instanceof ComponentSampleModel && ((ComponentSampleModel)sampleModel).getPixelStride() != 1
		//		 || sampleModel instanceof PixelInterleavedSampleModel) {
					convertNonGrayscale = true;
		//		}
			}
		}
//System.err.println("SingleImagePanel.doCommonConstructorStuff(): convertNonGrayscale = "+convertNonGrayscale);
		try {
			useConvertToMostFavorableImageType =
				   (System.getProperty("mrj.version") != null && Double.parseDouble(System.getProperty("mrj.version")) < 4)	// because slow otherwise
				 || convertNonGrayscale == true;					// because scaling of band or interleaved fails on all except Mac, and is slower on Mac
			//useConvertToMostFavorableImageType = true;
			//useConvertToMostFavorableImageType = false;
		}
		catch (NumberFormatException e) {
		}
//System.err.println("SingleImagePanel.doCommonConstructorStuff(): useConvertToMostFavorableImageType = "+useConvertToMostFavorableImageType);
		
		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);

		srcImages=imgs;
		nSrcImages=imgs.length;
		currentSrcImageIndex=0;
		
		currentSrcImageSortOrder=sortOrder;	// may be null ... that is OK ... paintComponent() handles null as implicit frame order

		cachedResizedImage=null; 
		cachedPreWindowedImage=null;

		this.typeOfPanelEventContext=typeOfPanelEventContext;
		ourWindowCenterAndWidthChangeListener = new OurWindowCenterAndWidthChangeListener(typeOfPanelEventContext);
		ourFrameSelectionChangeListener = new OurFrameSelectionChangeListener(typeOfPanelEventContext);
		ourFrameSortOrderChangeListener = new OurFrameSortOrderChangeListener(typeOfPanelEventContext);
		ourVOIFunctionChangeListener = new OurVOIFunctionChangeListener(typeOfPanelEventContext);
	
		this.largestGray=largestGray;
		this.firstvalueMapped=firstvalueMapped;
		this.numberOfEntries=numberOfEntries;
		this.bitsPerEntry=bitsPerEntry;
		this.redTable=redTable;
		this.greenTable=greenTable;
		this.blueTable=blueTable;

		imgMin=iMin;
		imgMax=iMax;

		this.signed=signed;
		this.inverted=inverted;
		
		if (imgs != null) {
			BufferedImage img=imgs[0];
			signBit=0;
			signMask=0;
			if (signed) {
//System.err.println("signed="+signed);
				// the source image will already have been sign extended to the data type size
				// so we don't need to worry about other than exactly 8 and 16 bits
				if (img.getSampleModel().getDataType() == DataBuffer.TYPE_BYTE) {
					signBit=0x0080;
					signMask=0xffffff80;
				}
				else {	// assume short or ushort
					signBit=0x8000;
					signMask=0xffff8000;
				}
			}
//System.err.println("signMask=0x"+Integer.toHexString(signMask));
//System.err.println("signBit=0x"+Integer.toHexString(signBit));
		}
		
		this.realWorldValueTransform=realWorldValueTransform;
		this.modalityTransform=modalityTransform;
		this.voiTransform=voiTransform;
		
		// slope & intercept get set on 1st paintComponent() anyway, but
		// we need to get them now to statistically derive the default
		// window center and width ...
		
		if (modalityTransform != null) {
			    useSlope = modalityTransform.getRescaleSlope    (currentSrcImageIndex);
			useIntercept = modalityTransform.getRescaleIntercept(currentSrcImageIndex);
//System.err.println("Initially using preselected rescale slope "+useSlope+" and intercept "+useIntercept);
		}
		else {
			useSlope=1.0;
			useIntercept=0.0;
//System.err.println("Initially using default rescale slope "+useSlope+" and intercept "+useIntercept);
		}
		
		// choose the initial window center and width ...
				
		windowWidth=0;
		windowCenter=0;
		if (voiTransform != null && voiTransform.getNumberOfTransforms(currentSrcImageIndex) > 0) {
			currentVOITransformInUse=0;
			windowWidth=voiTransform.getWidth(currentSrcImageIndex,currentVOITransformInUse);
			windowCenter=voiTransform.getCenter(currentSrcImageIndex,currentVOITransformInUse);
//System.err.println("Initially using preselected center "+windowCenter+" and width "+windowWidth);
		}
		if (windowWidth == 0) {			// use supplied window only if there was one, and if its width was not zero (center may legitimately be zero)
		//if (iMean != 0.0 && iSD != 0.0) {
//System.err.println("Using mean and SD");
		//	windowWidth=iSD*2.0;
		//	windowCenter=iMean;
		//}
		//else {
			double ourMin = imgMin*useSlope+useIntercept;
			double ourMax = imgMax*useSlope+useIntercept;
			//windowWidth=(ourMax-ourMin)/2.0;
			windowWidth=(ourMax-ourMin);
			windowCenter=(ourMax+ourMin)/2.0;
			currentVOITransformInUse=-1;		// flag not to mess with values when scrolling through frames
//System.err.println("Initially using statistically derived center "+windowCenter+" and width "+windowWidth);
		//}
		}
		
		this.preDefinedShapes=preDefinedShapes;
		this.preDefinedText=preDefinedText;
		
		this.imageGeometry=imageGeometry;
	}
	
	// Actual constructors ...

	/**
	 * @param	imgs
	 * @param	typeOfPanelEventContext
	 * @param	largestGray
	 * @param	firstvalueMapped
	 * @param	numberOfEntries
	 * @param	bitsPerEntry
	 * @param	redTable
	 * @param	greenTable
	 * @param	blueTable
	 * @param	iMin
	 * @param	iMax
	 * @param	sign
	 * @param	invert
	 * @param	realWorldValueTransform
	 * @param	modalityTransform
	 * @param	voiTransform
	 * @param	imageGeometry
	 */
	public SingleImagePanel(BufferedImage[] imgs,
			EventContext typeOfPanelEventContext,
			int largestGray,int firstvalueMapped,int numberOfEntries,int bitsPerEntry,short redTable[],short greenTable[],short blueTable[],
			int iMin,int iMax,boolean sign,boolean invert,
			RealWorldValueTransform realWorldValueTransform,
			ModalityTransform modalityTransform,
			VOITransform voiTransform,
			GeometryOfVolume imageGeometry) {
		doCommonConstructorStuff(imgs,
			typeOfPanelEventContext,
			null,
			largestGray,firstvalueMapped,numberOfEntries,bitsPerEntry,redTable,greenTable,blueTable,
			iMin,iMax,0.0,0.0,sign,invert,
			realWorldValueTransform,modalityTransform,voiTransform,
			null,null,
			imageGeometry);
	}

	/**
	 * @param	img
	 * @param	typeOfPanelEventContext
	 * @param	iMin
	 * @param	iMax
	 * @param	sign
	 * @param	invert
	 * @param	preDefinedShapes
	 * @param	preDefinedText
	 */
	public SingleImagePanel(BufferedImage img,
			EventContext typeOfPanelEventContext,
			int iMin,int iMax,boolean sign,boolean invert,
			Vector preDefinedShapes,Vector preDefinedText) {
		doCommonConstructorStuff(img,
			typeOfPanelEventContext,
			0,0,0,0,null,null,null,
			iMin,iMax,0.0,0.0,sign,invert,
			null,null,null,
			preDefinedShapes,preDefinedText,
			null);
	}

	/**
	 * @param	img
	 * @param	typeOfPanelEventContext
	 * @param	iMin
	 * @param	iMax
	 * @param	sign
	 * @param	invert
	 */
	public SingleImagePanel(BufferedImage img,
			EventContext typeOfPanelEventContext,
			int iMin,int iMax,boolean sign,boolean invert) {
		doCommonConstructorStuff(img,
			typeOfPanelEventContext,
			0,0,0,0,null,null,null,
			iMin,iMax,0.0,0.0,sign,invert,
			null,null,null,
			null,null,
			null);
	}

	/**
	 * @param	imgs
	 * @param	typeOfPanelEventContext
	 * @param	iMin
	 * @param	iMax
	 * @param	sign
	 * @param	invert
	 * @param	realWorldValueTransform
	 * @param	modalityTransform
	 * @param	voiTransform
	 * @param	imageGeometry
	 */
	public SingleImagePanel(BufferedImage[] imgs,
			EventContext typeOfPanelEventContext,
			int iMin,int iMax,boolean sign,boolean invert,
			RealWorldValueTransform realWorldValueTransform,
			ModalityTransform modalityTransform,
			VOITransform voiTransform,
			GeometryOfVolume imageGeometry) {
		doCommonConstructorStuff(imgs,
			typeOfPanelEventContext,
			null,
			0,0,0,0,null,null,null,
			iMin,iMax,0.0,0.0,sign,invert,
			realWorldValueTransform,modalityTransform,voiTransform,
			null,null,
			imageGeometry);
	}

	/**
	 * @param	imgs
	 * @param	typeOfPanelEventContext
	 * @param	iMin
	 * @param	iMax
	 * @param	sign
	 * @param	invert
	 */
	public SingleImagePanel(BufferedImage[] imgs,
			EventContext typeOfPanelEventContext,
			int iMin,int iMax,boolean sign,boolean invert) {
		doCommonConstructorStuff(imgs,
			typeOfPanelEventContext,
			null,
			0,0,0,0,null,null,null,
			iMin,iMax,0.0,0.0,sign,invert,
			null,null,null,
			null,null,
			null);
	}

	public void deconstruct() {
//System.err.println("SingleImagePanel.deconstruct()");
		// avoid "listener leak"
		if (ourWindowCenterAndWidthChangeListener != null) {
			ApplicationEventDispatcher.applicationEventDispatcher.removeListener(ourWindowCenterAndWidthChangeListener);
			ourWindowCenterAndWidthChangeListener=null;
		}
		if (ourFrameSelectionChangeListener != null) {
			ApplicationEventDispatcher.applicationEventDispatcher.removeListener(ourFrameSelectionChangeListener);
			ourFrameSelectionChangeListener=null;
		}
		if (ourFrameSortOrderChangeListener != null) {
			ApplicationEventDispatcher.applicationEventDispatcher.removeListener(ourFrameSortOrderChangeListener);
			ourFrameSortOrderChangeListener=null;
		}
		if (ourVOIFunctionChangeListener != null) {
			ApplicationEventDispatcher.applicationEventDispatcher.removeListener(ourVOIFunctionChangeListener);
			ourVOIFunctionChangeListener=null;
		}
	}
	
	/*
	 * @param container
	 */
	 public static void deconstructAllSingleImagePanelsInContainer(Container container) {
		Component[] components = container.getComponents();				
//System.err.println("SingleImagePanel.deconstructAllSingleImagePanelsInContainer(): deconstructing old SingleImagePanels components.length="+components.length);
		for (int i=0; i<components.length; ++i) {
			Component component = components[i];
			if (component instanceof SingleImagePanel) {
				((SingleImagePanel)component).deconstruct();
			}
		}
	}

	protected void finalize() throws Throwable {
//System.err.println("SingleImagePanel.finalize()");
		deconstruct();		// just in case wasn't already called, and garbage collection occurs
		super.finalize();
	}

	// region detection stuff
	
	private class IntegerPointWithValue {
		private int x;
		private int y;
		private int v;
		
		public IntegerPointWithValue(int x,int y,int v) throws Exception {
			if (x > 65535 || x < 0 || y > 65535 || y < 0) {
				throw new Exception("coordinate too large");
			}
			this.x=x;
			this.y=y;
			this.v=v;
		}
		
		public final int getX() { return x; }
		public final int getY() { return y; }
		public final int getV() { return v; }
		
		public final boolean equals(Object o) {
			IntegerPointWithValue c = (IntegerPointWithValue)o;
			return x == c.x && y == c.y;
		}
		
		public final int hashCode() {
			return x<<16 + y;		// will fail if x or y > 65535 or < 0
		}
	}
	
	private IntegerPointWithValue[] findLongestAndShortestPaths(Collection points,double[] voxelSpacing) throws Exception {
		double pixelSpacing = voxelSpacing == null ? 0 : (voxelSpacing[0] == voxelSpacing[1] ? voxelSpacing[0] : 0);
	
		Object pointArray[] = points.toArray();
		int arrayLength = pointArray.length;
		int longestFromIndex=-1;
		int longestToIndex=-1;
		int longestLengthSquared=0;
		for (int from=0; from<arrayLength; ++from) {
			IntegerPointWithValue fromPoint = (IntegerPointWithValue)(pointArray[from]);
			int fromX = fromPoint.getX();
			int fromY = fromPoint.getY();
			for (int to=from+1; to<arrayLength; ++to) {
				IntegerPointWithValue toPoint = (IntegerPointWithValue)(pointArray[to]);
				int toX = toPoint.getX();
				int toY = toPoint.getY();
				int deltaX = toX-fromX;
				int deltaY = toY-fromY;
				int lengthSquared=deltaX*deltaX+deltaY*deltaY;
				if (lengthSquared > longestLengthSquared) {
					longestLengthSquared=lengthSquared;
					longestFromIndex=from;
					longestToIndex=to;
				}
			}
		}
		if (longestFromIndex < 0 || longestToIndex < 0) {
			return null;
		}
		
		IntegerPointWithValue fromPoint = (IntegerPointWithValue)(pointArray[longestFromIndex]);
		IntegerPointWithValue toPoint = (IntegerPointWithValue)(pointArray[longestToIndex]);
		int fromX = fromPoint.getX();
		int toX = toPoint.getX();
		int deltaX = toX-fromX;
		
		if (deltaX < 0) {
			// swap them around; want from point to always be on the left
			int tempIndex=longestFromIndex;
			longestFromIndex=longestToIndex;
			longestToIndex=tempIndex;
			fromPoint = (IntegerPointWithValue)(pointArray[longestFromIndex]);
			toPoint = (IntegerPointWithValue)(pointArray[longestToIndex]);
			fromX = fromPoint.getX();
			toX = toPoint.getX();
			deltaX = toX-fromX;
		}
		
		int fromY = fromPoint.getY();
		int toY = toPoint.getY();
		int deltaY = -(toY-fromY);	// think upside down
		
		double length=Math.sqrt(deltaX*deltaX+deltaY*deltaY);
		
		double angleBetweenXAxisAndLongestPath = Math.atan((double)(deltaY)/(double)(deltaX));

//System.err.println("Longest from ("+fromX+","+fromY+") to ("+toX+","+toY+") length="+length+" pixels, angle="+angleBetweenXAxisAndLongestPath+" radians or "+(angleBetweenXAxisAndLongestPath*180/Math.PI)+" degrees");
System.err.println("Long axis length="+length+" pixels ("+(length*pixelSpacing)+" mm)");

		double mostNegativeNormalDistance = 0;
		double mostPositiveNormalDistance = 0;
		
		IntegerPointWithValue mostNegativeTestPoint = null;
		IntegerPointWithValue mostPositiveTestPoint = null;
		
		IntegerPointWithValue mostNegativeIntersectionPoint = null;
		IntegerPointWithValue mostPositiveIntersectionPoint = null;
		
		for (int test=0; test<arrayLength; ++test) {
			IntegerPointWithValue testPoint = (IntegerPointWithValue)(pointArray[test]);
			int testX = testPoint.getX();
			int testY = testPoint.getY();
			int deltaFromToTestX = testX-fromX;
			int deltaFromToTestY = -(testY-fromY);	// think upside down
			double distanceBetweenFromAndTestPoint=Math.sqrt(deltaFromToTestX*deltaFromToTestX+deltaFromToTestY*deltaFromToTestY);
			double angleBetweenXAxisAndFromToTestPoint = Math.atan((double)(deltaFromToTestY)/(double)(deltaFromToTestX));
//System.err.println("Test from ("+fromX+","+fromY+") to ("+testX+","+testY+") length="+distanceBetweenFromAndTestPoint+" pixels, angle="+angleBetweenXAxisAndFromToTestPoint+" radians or "+(angleBetweenXAxisAndFromToTestPoint*180/Math.PI)+" degrees");

			double angleBetweenLongestPathAndFromToTestPoint = angleBetweenXAxisAndFromToTestPoint - angleBetweenXAxisAndLongestPath;

			double distanceBetweenFromAndTestPointProjectedNormalToLongestPath = distanceBetweenFromAndTestPoint * Math.cos(angleBetweenLongestPathAndFromToTestPoint);
			double distanceBetweenTestPointAndLongestPathAlongNormal = distanceBetweenFromAndTestPoint * Math.sin(angleBetweenLongestPathAndFromToTestPoint);
			
			int deltaFromToTestProjectedOntoLongestPathX = (int)(distanceBetweenFromAndTestPointProjectedNormalToLongestPath * Math.cos(angleBetweenXAxisAndLongestPath));
			int deltaFromToTestProjectedOntoLongestPathY = (int)(distanceBetweenFromAndTestPointProjectedNormalToLongestPath * Math.sin(angleBetweenXAxisAndLongestPath));
			
			int testProjectedOntoLongestPathX = fromX + deltaFromToTestProjectedOntoLongestPathX;
			int testProjectedOntoLongestPathY = fromY - deltaFromToTestProjectedOntoLongestPathY;	// think upside down
//System.err.println("Intersects long axis at ("+testProjectedOntoLongestPathX+","+testProjectedOntoLongestPathY+") distance="+distanceBetweenTestPointAndLongestPathAlongNormal+" pixels");

			if (distanceBetweenTestPointAndLongestPathAlongNormal < mostNegativeNormalDistance
			 && angleBetweenLongestPathAndFromToTestPoint <= Math.PI/2 && angleBetweenLongestPathAndFromToTestPoint >= -Math.PI/2) {	// Don't look "behind" from point
				mostNegativeNormalDistance=distanceBetweenTestPointAndLongestPathAlongNormal;
				mostNegativeTestPoint=testPoint;
				mostNegativeIntersectionPoint = new IntegerPointWithValue(testProjectedOntoLongestPathX,testProjectedOntoLongestPathY,0/*dummy value*/);
//System.err.println("angleBetweenXAxisAndFromToTestPoint="+(angleBetweenXAxisAndFromToTestPoint*180/Math.PI)+", angleBetweenXAxisAndLongestPath="+(angleBetweenXAxisAndLongestPath*180/Math.PI)+", angleBetweenLongestPathAndFromToTestPoint="+(angleBetweenLongestPathAndFromToTestPoint*180/Math.PI));
//System.err.println("Most negative is now from ("+testX+","+testY+") to ("+testProjectedOntoLongestPathX+","+testProjectedOntoLongestPathY+") length="+distanceBetweenTestPointAndLongestPathAlongNormal+" pixels");
			}
			if (distanceBetweenTestPointAndLongestPathAlongNormal > mostPositiveNormalDistance
			 && angleBetweenLongestPathAndFromToTestPoint <= Math.PI/2 && angleBetweenLongestPathAndFromToTestPoint >= -Math.PI/2) {	// Don't look "behind" from point
				mostPositiveNormalDistance=distanceBetweenTestPointAndLongestPathAlongNormal;
				mostPositiveTestPoint=testPoint;
				mostPositiveIntersectionPoint = new IntegerPointWithValue(testProjectedOntoLongestPathX,testProjectedOntoLongestPathY,0/*dummy value*/);
//System.err.println("angleBetweenXAxisAndFromToTestPoint="+(angleBetweenXAxisAndFromToTestPoint*180/Math.PI)+", angleBetweenXAxisAndLongestPath="+(angleBetweenXAxisAndLongestPath*180/Math.PI)+", angleBetweenLongestPathAndFromToTestPoint="+(angleBetweenLongestPathAndFromToTestPoint*180/Math.PI));
//System.err.println("Most positive is now from ("+testX+","+testY+") to ("+testProjectedOntoLongestPathX+","+testProjectedOntoLongestPathY+") length="+distanceBetweenTestPointAndLongestPathAlongNormal+" pixels");
			}
		}

System.err.println("Short axis most negative side length="+mostNegativeNormalDistance+" pixels ("+(mostNegativeNormalDistance*pixelSpacing)+" mm)");
System.err.println("Short axis most positive side length="+mostPositiveNormalDistance+" pixels ("+(mostPositiveNormalDistance*pixelSpacing)+" mm)");
System.err.println("Short axis total length="+(Math.abs(mostNegativeNormalDistance)+Math.abs(mostPositiveNormalDistance))+" pixels ("+((Math.abs(mostNegativeNormalDistance)+Math.abs(mostPositiveNormalDistance))*pixelSpacing)+" mm)");

		IntegerPointWithValue resultArray[] = new IntegerPointWithValue[6];
		resultArray[0]=fromPoint;
		resultArray[1]=toPoint;
		resultArray[2]=mostNegativeTestPoint;
		resultArray[3]=mostNegativeIntersectionPoint;
		resultArray[4]=mostPositiveTestPoint;
		resultArray[5]=mostPositiveIntersectionPoint;
		return resultArray;
	}
	
	private void detectAndDrawRegion(
			BufferedImage image,
			double[] voxelSpacing,
			Vector interactiveDrawingShapes,
			int regionSelectionCenterX,int regionSelectionCenterY,
			int regionSelectionTLHCX,int regionSelectionTLHCY,
			int regionSelectionBRHCX,int regionSelectionBRHCY) throws Exception {
		
		HashSet pointsAlreadyChecked = new HashSet();
		HashSet pointsYetToBeChecked = new HashSet();
		
		Raster raster = image.getRaster();

		int seedValue = raster.getSample(regionSelectionCenterX,regionSelectionCenterY,0/*band*/);
		pointsYetToBeChecked.add(new IntegerPointWithValue(regionSelectionCenterX,regionSelectionCenterY,seedValue));

//System.err.println("starting at ("+regionSelectionCenterX+","+regionSelectionCenterY+")");

		int n=1;
		double runningMean = seedValue;
		double runningSD = 0;
		double runningSumOfSquares = 0;

//System.err.println("start ("+regionSelectionCenterX+","+regionSelectionCenterY+")="+seedValue+" [n="+n+", runningMean="+runningMean+", SD="+runningSD+"]");

		int numberOfPointsToIncludeInInitialAverage = 16;
		double factorToUpdatethresholdDifferenceDerivedFromSD = 2.0;
		int thresholdDifference = 20;
		int differenceAbout = seedValue;
		//int thresholdDifferenceDerivedFromSD = 0;
		
		do {
			Iterator i = pointsYetToBeChecked.iterator();
			if (i.hasNext()) {	// not while ... the later addition triggers a ConcurrentModificationException
				IntegerPointWithValue point = (IntegerPointWithValue)(i.next());
				i.remove();
				pointsAlreadyChecked.add(point);
				
				int x = (int)(point.getX());
				int y = (int)(point.getY());
				int v = raster.getSample(x,y,0/*band*/);
				
				double difference = v - runningMean;
				double newMean = runningMean + difference / (n+1);
				runningSumOfSquares += difference * (v - newMean);
				runningMean = newMean;
				runningSD = Math.sqrt(runningSumOfSquares/n);
				++n;
				
				if (n > numberOfPointsToIncludeInInitialAverage) {
					thresholdDifference = (int)(runningSD*factorToUpdatethresholdDifferenceDerivedFromSD);
					//thresholdDifference = thresholdDifferenceDerivedFromSD;
					// leave differenceAbout alone from now on
				}
				else {
					//use initially set thresholdDifference
					differenceAbout = (int)runningMean;
					//thresholdDifferenceDerivedFromSD = (int)(runningSD*factorToUpdatethresholdDifferenceDerivedFromSD);
				}

//System.err.println("checking ("+x+","+y+")="+v+" [n="+n+", runningMean="+runningMean+", SD="+runningSD+", differenceAbout="+differenceAbout+", thresholdDifference="+thresholdDifference+"]");

				// check 8-connected regions
				for (int deltaX=-1; deltaX <= 1; ++deltaX) {
					for (int deltaY=-1; deltaY <= 1; ++deltaY) {
						if (deltaX != 0 || deltaY != 0) {	/// don't check center pixel against itself
							int xPrime = x + deltaX;
							int yPrime = y + deltaY;
							if (xPrime >= regionSelectionTLHCX
							 && xPrime <= regionSelectionBRHCX
							 && yPrime >= regionSelectionTLHCY
							 && yPrime <= regionSelectionBRHCY) {	// constrain to supplied boundaries (inclusive)
								int vPrime = raster.getSample(xPrime,yPrime,0/*band*/);
								//if (Math.abs(v-vPrime) <= thresholdDifference) {
								//if (Math.abs(seedValue-vPrime) <= thresholdDifference) {
								//if (Math.abs(runningMean-vPrime) <= thresholdDifference) {
								if (Math.abs(differenceAbout-vPrime) <= thresholdDifference) {
									IntegerPointWithValue newPoint = new IntegerPointWithValue(xPrime,yPrime,vPrime);
									if (!pointsAlreadyChecked.contains(newPoint)) {
//System.err.println("adding ("+xPrime+","+yPrime+")="+vPrime);
										pointsYetToBeChecked.add(newPoint);
									}
								}
							}
						}
					}
				}
			}
		} while (!pointsYetToBeChecked.isEmpty());
		
		//Iterator id = pointsAlreadyChecked.iterator();
		///*if (id.hasNext())*/ {
		//	//IntegerPointWithValue p = (IntegerPointWithValue)(id.next());
		//	GeneralPath path = new GeneralPath();
		//	//path.moveTo(p.getX(),p.getY());
		//	while (id.hasNext()) {
		//		IntegerPointWithValue p = (IntegerPointWithValue)(id.next());
		//		path.moveTo(p.getX(),p.getY());
		//		path.lineTo(p.getX(),p.getY());
		//	}
		//	interactiveDrawingShapes.add(path);
		//}

		int width = image.getWidth();
		int height = image.getHeight();
		int[] topYValueForX = new int[width];
		int[] bottomYValueForX = new int[width];
		for (int x=0; x<width; ++x) {
			topYValueForX[x]=height;
			bottomYValueForX[x]=-1;
		}
		Iterator id = pointsAlreadyChecked.iterator();
		while (id.hasNext()) {
			IntegerPointWithValue p = (IntegerPointWithValue)(id.next());
			int x = p.getX();
			int y = p.getY();
			if (y < topYValueForX[x]) {
				topYValueForX[x]=y;	// top in the sense of higher up in the displayed image
			}
			if (y > bottomYValueForX[x]) {
				bottomYValueForX[x]=y;	// bottom in the sense of lower down in the displayed image

			}
		}
		GeneralPath path = new GeneralPath();
		boolean foundOne = false;
		int firstX=-1;
		int firstY=-1;
		for (int x=0; x<width; ++x) {
			int y=topYValueForX[x];
			if (y < height) {
				if (!foundOne) {
					path.moveTo(x,y);
					foundOne=true;
					firstX=x;
					firstY=y;
				}
				else {
					path.lineTo(x,y);
				}
			}
		}
		for (int x=width-1; x>=0; --x) {
			int y=bottomYValueForX[x];
			if (y >= 0) {
				if (!foundOne) {
					path.moveTo(x,y);
					foundOne=true;
					firstX=x;
					firstY=y;
				}
				else {
					path.lineTo(x,y);
				}
			}
		}
		if (foundOne) {
			path.lineTo(firstX,firstY);

			IntegerPointWithValue longestPath[] = null;
			try {
				longestPath=findLongestAndShortestPaths(pointsAlreadyChecked,voxelSpacing);
			}
			catch (Exception e) {
				e.printStackTrace();	// possible whilst creating points
			}
			
			if (longestPath != null) {
				path.moveTo(longestPath[0].getX(),longestPath[0].getY());
				path.lineTo(longestPath[1].getX(),longestPath[1].getY());
			
				if (longestPath[2] != null && longestPath[3] != null) {
					path.moveTo(longestPath[2].getX(),longestPath[2].getY());
					path.lineTo(longestPath[3].getX(),longestPath[3].getY());
				}
				if (longestPath[4] != null && longestPath[5] != null) {
					path.moveTo(longestPath[4].getX(),longestPath[4].getY());
					path.lineTo(longestPath[5].getX(),longestPath[5].getY());
				}

				interactiveDrawingShapes.add(path);
			}
		}
	}

	// stuff to handle drawing ...

	/***/
	Vector interactiveDrawingShapes;
	/***/
	Point2D startPoint;
	/***/
	static final int crossSize = 5;		// actually just one arm of the cross

	/**
	 * @param	x
	 * @param	y
	 */
	private void startInteractiveDrawing(int x,int y) {
		int startX = (int)(x/useScaleFactor+0.5);
		int startY = (int)(y/useScaleFactor+0.5);
		startPoint=new Point(startX,startY);
	}

	/**
	 * @param	x
	 * @param	y
	 */
	private void dragInteractiveDrawing(int x,int y) {
		interactiveDrawingShapes = new Vector();
		//interactiveDrawingShapes.add(new Line2D.Float(startPoint,new Point((int)(x/useScaleFactor+0.5),(int)(y/useScaleFactor+0.5))));
	
		int startX = (int)(startPoint.getX());
		int startY = (int)(startPoint.getY());
		int endX = (int)(x/useScaleFactor+0.5);
		int endY = (int)(y/useScaleFactor+0.5);
		Point endPoint = new Point(endX,endY);
		interactiveDrawingShapes.add(new Line2D.Float(startPoint,endPoint));

		int width = (endX - startX)*2;
		if (width < 0) {
			width=-width;
		}
		int height = (endY - startY)*2;
		if (height < 0) {
			height=-height;
		}
		int tlhcX=startX-width/2;
		int tlhcY=startY-height/2;
		interactiveDrawingShapes.add(new Rectangle(tlhcX,tlhcY,width,height));

		repaint();
	}
	/**
	 * @param	x
	 * @param	y
	 */
	private void endInteractiveDrawing(int x,int y) {
		interactiveDrawingShapes = new Vector();
		//interactiveDrawingShapes.add(new Line2D.Float(startPoint,new Point((int)(x/useScaleFactor+0.5),(int)(y/useScaleFactor+0.5))));
		
		int startX = (int)(startPoint.getX());
		int startY = (int)(startPoint.getY());
		int endX = (int)(x/useScaleFactor+0.5);
		int endY = (int)(y/useScaleFactor+0.5);

		//Point endPoint = new Point(endX,endY);
		//interactiveDrawingShapes.add(new Line2D.Float(new Point(startX-crossSize,startY-crossSize),new Point(startX+crossSize,startY+crossSize)));
		//interactiveDrawingShapes.add(new Line2D.Float(new Point(startX+crossSize,startY-crossSize),new Point(startX-crossSize,startY+crossSize)));
		//interactiveDrawingShapes.add(new Line2D.Float(startPoint,endPoint));
		//interactiveDrawingShapes.add(new Line2D.Float(new Point(endX-crossSize,endY-crossSize),new Point(endX+crossSize,endY+crossSize)));
		//interactiveDrawingShapes.add(new Line2D.Float(new Point(endX+crossSize,endY-crossSize),new Point(endX-crossSize,endY+crossSize)));
		
		int width = (endX - startX)*2;
		if (width < 0) {
			width=-width;
		}
		int height = (endY - startY)*2;
		if (height < 0) {
			height=-height;
		}
		int tlhcX=startX-width/2;
		int tlhcY=startY-height/2;
		//interactiveDrawingShapes.add(new Rectangle(tlhcX,tlhcY,width,height));
		
		setRegionSelection(startX,startY,tlhcX,tlhcY,tlhcX+width,tlhcY+height);

		try {
			int whichFrame = currentSrcImageSortOrder != null ? currentSrcImageSortOrder[currentSrcImageIndex] : currentSrcImageIndex;
			GeometryOfVolume volumeGeometry = getImageGeometry();
			GeometryOfSlice[] sliceGeometries = volumeGeometry == null ? null : volumeGeometry.getGeometryOfSlices();
			GeometryOfSlice sliceGeometry = sliceGeometries == null ? null : sliceGeometries[whichFrame];
			double[] voxelSpacing = sliceGeometry == null ? null : sliceGeometry.getVoxelSpacingArray();

			detectAndDrawRegion(
				srcImages[whichFrame],
				voxelSpacing,
				interactiveDrawingShapes,
				regionSelectionCenterX,regionSelectionCenterY,
				regionSelectionTLHCX,regionSelectionTLHCY,
				regionSelectionBRHCX,regionSelectionBRHCY);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		repaint();
	}

	// override methods for JComponent ...
        
        /**
         * @param	srcImage
         */
        private final BufferedImage convertToMostFavorableImageTypeWithPixelCopy(BufferedImage srcImage) {
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: start");

                int width = srcImage.getWidth();
                int height = srcImage.getHeight();
        
                SampleModel srcSampleModel = srcImage.getSampleModel();
                WritableRaster srcRaster = srcImage.getRaster();
                DataBuffer srcDataBuffer = srcRaster.getDataBuffer();
                int srcNumBands = srcRaster.getNumBands();

//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: srcImage ="+srcImage);
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: srcSampleModel ="+srcSampleModel);
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: srcSampleModel.getPixelStride() ="+((ComponentSampleModel)srcSampleModel).getPixelStride());

                ColorModel dstColorModel = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getColorModel();
                WritableRaster dstRaster = dstColorModel.createCompatibleWritableRaster(width,height);
                DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
                BufferedImage dstImage = new BufferedImage(dstColorModel, dstRaster, dstColorModel.isAlphaPremultiplied(), null);
                SampleModel dstSampleModel = dstImage.getSampleModel();
                int dstNumBands = dstRaster.getNumBands();
        
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: dstImage ="+dstImage);
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: dstSampleModel ="+dstSampleModel);

                int srcPixels[] = null; // to disambiguate SampleModel.getPixels() method signature
                srcPixels = srcSampleModel.getPixels(0,0,width,height,srcPixels,srcDataBuffer);
                int srcPixelsLength = srcPixels.length;

                int dstPixels[] = null; // to disambiguate SampleModel.getPixels() method signature
                dstPixels = dstSampleModel.getPixels(0,0,width,height,dstPixels,dstDataBuffer);
                int dstPixelsLength = dstPixels.length;
        
                if (srcNumBands == 1 && dstNumBands == 4 && srcPixelsLength*4 == dstPixelsLength) {
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: converting gray to RGBA");
                        int dstIndex=0;
                        for (int srcIndex=0; srcIndex<srcPixelsLength; ++srcIndex) {
                                dstPixels[dstIndex++]=srcPixels[srcIndex];
                                dstPixels[dstIndex++]=srcPixels[srcIndex];
                                dstPixels[dstIndex++]=srcPixels[srcIndex];
                                dstPixels[dstIndex++]=-1;
                        }
                        dstSampleModel.setPixels(0,0,width,height,dstPixels,dstDataBuffer);
                }
                else if (srcNumBands == 1 && dstNumBands == 3 && srcPixelsLength*3 == dstPixelsLength) {
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: converting gray to RGB");
                        int dstIndex=0;
                        for (int srcIndex=0; srcIndex<srcPixelsLength; ++srcIndex) {
                                dstPixels[dstIndex++]=srcPixels[srcIndex];
                                dstPixels[dstIndex++]=srcPixels[srcIndex];
                                dstPixels[dstIndex++]=srcPixels[srcIndex];
                        }
                        dstSampleModel.setPixels(0,0,width,height,dstPixels,dstDataBuffer);
                }
                else if (srcNumBands == 3 && dstNumBands == 4 && srcPixelsLength*4 == dstPixelsLength*3) {
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: converting pixel or band interleaved 3 band to RGBA");
                        int dstIndex=0;
                        for (int srcIndex=0; srcIndex<srcPixelsLength;) {
                                dstPixels[dstIndex++]=srcPixels[srcIndex++];
                                dstPixels[dstIndex++]=srcPixels[srcIndex++];
                                dstPixels[dstIndex++]=srcPixels[srcIndex++];
                                dstPixels[dstIndex++]=-1;
                        }
                        dstSampleModel.setPixels(0,0,width,height,dstPixels,dstDataBuffer);
                }
                else if (srcNumBands == 3 && dstNumBands == 3 && srcPixelsLength == dstPixelsLength) {
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: converting pixel or band interleaved 3 band to RGB");
                        int dstIndex=0;
                        for (int srcIndex=0; srcIndex<srcPixelsLength;) {
                                dstPixels[dstIndex++]=srcPixels[srcIndex++];
                                dstPixels[dstIndex++]=srcPixels[srcIndex++];
                                dstPixels[dstIndex++]=srcPixels[srcIndex++];
                        }
                        dstSampleModel.setPixels(0,0,width,height,dstPixels,dstDataBuffer);
                }
                else {
//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: No conversion supported");
                        dstImage=srcImage;	// do no conversion and hope for the best (performance) :(
                }

//System.err.println("convertToMostFavorableImageTypeWithPixelCopy: done");
                return dstImage;
        }

        /**
         * @param	srcImage
         */
        private final BufferedImage convertToMostFavorableImageTypeWithBandCombineOp(BufferedImage srcImage) {
//System.err.println("convertToMostFavorableImageTypeWithBandCombineOp: start");
		float [][] spreadToRGBA = null;
                int srcNumBands = srcImage.getRaster().getNumBands();
		if (srcNumBands == 1) {
			float [][] spreadOneToRGBA = {
				{ 1, 0 },
				{ 1, 0 },
				{ 1, 0 },
				{ 0, 0xffff }
			};
			spreadToRGBA=spreadOneToRGBA;
		}
		else if (srcNumBands == 3) {
			float [][] spreadThreeToRGBA = {
				{ 1, 0, 0, 0 },
				{ 0, 1, 0, 0 },
				{ 0, 0, 1, 0 },
				{ 0, 0, 0, 0xffff }
			};
			spreadToRGBA=spreadThreeToRGBA;
		}
		BandCombineOp spreadToRGBAOp = new BandCombineOp(spreadToRGBA,null);
			
		ColorModel dstColorModel = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getColorModel();
		WritableRaster dstRaster = dstColorModel.createCompatibleWritableRaster(srcImage.getWidth(),srcImage.getHeight());
		BufferedImage dstImage = new BufferedImage(dstColorModel,dstRaster,dstColorModel.isAlphaPremultiplied(),null);
		spreadToRGBAOp.filter(srcImage.getRaster(),dstRaster);
//System.err.println("convertToMostFavorableImageTypeWithBandCombineOp: done");
                return dstImage;
	}
	
	private class ResamplingVector {
		//double [][] arrayOfWeights;
		int [][]    arrayOfWeights;
		int [][] arrayOfSrcIndices;
		int []     numberOfEntries;
		//double [] sumOfWeights;
		int [] sumOfWeights;
		int maxNumberOfSrcSamplesPerDst;
		int divisor;

		ResamplingVector(int nSrcSamples,int nDstSamples) {
			//divisor = nDstSamples*nSrcSamples;
			divisor = 1000;
//System.err.println("divisor ="+divisor);
			double ratioSrcToDst = ((double)nSrcSamples)/nDstSamples;
			maxNumberOfSrcSamplesPerDst = (int)java.lang.Math.ceil(ratioSrcToDst)+1;
			double srcOffset = 0;
			int srcIndex = 0;
			//   arrayOfWeights = new double [nDstSamples][];
			   arrayOfWeights = new int [nDstSamples][];
			arrayOfSrcIndices = new int [nDstSamples][];
			  numberOfEntries = new int [nDstSamples];
			//   sumOfWeights = new double [nDstSamples];
			     sumOfWeights = new int [nDstSamples];
			for (int dstIndex=0; dstIndex<nDstSamples; ++dstIndex) {
				//arrayOfWeights[dstIndex] = new double[maxNumberOfSrcSamplesPerDst];
				arrayOfWeights[dstIndex] = new int[maxNumberOfSrcSamplesPerDst];
				arrayOfSrcIndices[dstIndex] = new int[maxNumberOfSrcSamplesPerDst];
				double stillNeedFromSrcForCurrentDst = ratioSrcToDst;
				for (int i=0; stillNeedFromSrcForCurrentDst > 0.0001; ++i) {		// test against zero fails sometimes and srcIndex exceeds nSrcSamples
					double weightForThisSrc=java.lang.Math.min(1.0-srcOffset,stillNeedFromSrcForCurrentDst);
					stillNeedFromSrcForCurrentDst-=weightForThisSrc;
					srcOffset+=weightForThisSrc;
//System.err.println("dstIndex ="+dstIndex+" srcIndex="+srcIndex+" weightForThisSrc="+weightForThisSrc+" *divisor="+(int)(weightForThisSrc*divisor));
					//arrayOfWeights[dstIndex][i]=weightForThisSrc;
					arrayOfWeights[dstIndex][i]=(int)(weightForThisSrc*divisor);
					//sumOfWeights[dstIndex]+=weightForThisSrc;
					sumOfWeights[dstIndex]+=(int)(weightForThisSrc*divisor);
					arrayOfSrcIndices[dstIndex][i]=srcIndex;
					numberOfEntries[dstIndex]=i+1;
					if (srcOffset >= 1.0) {
						++srcIndex;
						srcOffset=0;
					}
				}
			}
			//dump();
		}
		
		void dump() {
			for (int dstIndex=0; dstIndex<arrayOfSrcIndices.length; ++dstIndex) {
				for (int i=0; i<numberOfEntries[dstIndex]; ++i) {
System.err.println("dstIndex ="+dstIndex+" srcIndex="+arrayOfSrcIndices[dstIndex][i]+" weight="+arrayOfWeights[dstIndex][i]);
				}
			}
		}
	}

	BufferedImage resample(BufferedImage srcImage,int dstWidth,int dstHeight) {
//System.err.println("resample: start");
//long startTime = System.currentTimeMillis();

                int srcWidth = srcImage.getWidth();
                int srcHeight = srcImage.getHeight();
        
                ColorModel srcColorModel = srcImage.getColorModel();
                SampleModel srcSampleModel = srcImage.getSampleModel();
                WritableRaster srcRaster = srcImage.getRaster();
                DataBuffer srcDataBuffer = srcRaster.getDataBuffer();
		int srcDataBufferOffset = srcDataBuffer.getOffset();
//System.err.println("resample: srcDataBuffer is "+srcDataBuffer.getClass().getName());
//System.err.println("resample: srcDataBuffer.getOffset() is "+srcDataBufferOffset);
                int srcNumBands = srcRaster.getNumBands();

		ColorModel dstColorModel = new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] {16},
			false,		// has alpha
			false,		// alpha premultipled
			Transparency.OPAQUE,
			DataBuffer.TYPE_USHORT
		);
                SampleModel dstSampleModel = new ComponentSampleModel(
			DataBuffer.TYPE_USHORT,
			dstWidth,
			dstHeight,
			1,
			dstWidth,
			new int[] {0}
		);
                BufferedImage dstImage = null;
		
//System.err.println("resample: got info ="+(System.currentTimeMillis()-startTime)+" ms");

		// DataBufferShort will not be encountered ... see comments in SourceImage.java
		if (!(srcDataBuffer instanceof DataBufferUShort /*|| srcDataBuffer instanceof DataBufferShort*/)) {
//System.err.println("resample: not doing our own resampling");
			return srcImage;		// Give up if we don't know how to get pixels
		}
		int srcmask=signed ? 0xffffffff : 0x0000ffff;
		
                short srcPixels[];
		//if (srcDataBuffer instanceof DataBufferUShort) {
			srcPixels = ((DataBufferUShort)srcDataBuffer).getData();
		//}
		//else {
		//	srcPixels = ((DataBufferShort)srcDataBuffer).getData();
		//}
                int srcPixelsLength = srcPixels.length;
//System.err.println("resample: got srcPixels ="+(System.currentTimeMillis()-startTime)+" ms");

                int dstPixelsLength = dstWidth*dstHeight;
                short dstPixels[] = new short[dstPixelsLength];
//System.err.println("resample: got dstPixels ="+(System.currentTimeMillis()-startTime)+" ms");
       
		ResamplingVector hVector =  new ResamplingVector(srcWidth,dstWidth);
		ResamplingVector vVector =  new ResamplingVector(srcHeight,dstHeight);

                if (srcNumBands == 1) {
//System.err.println("resample: single band");
//System.err.println("resample: single band ="+(System.currentTimeMillis()-startTime)+" ms");
			//double dstRowBuffers[][] = new double[srcHeight][];			// two indirections (slightly) faster than single array and multiplication
			//double dstRowBuffers[] = new double[srcHeight*dstWidth];
			int dstRowBuffers[] = new int[srcHeight*dstWidth];
			int   bufferRowIndex[] = new int[srcHeight];				// saves slow multiplication of srcY*dstWidth
			int srcPixelOffset=srcDataBufferOffset;					// not zero, since may be later in shared buffer of multiple frames
			int dstPixelOffset=0;
			int lastBufferRowIndex=0;
			for (int srcY=0; srcY<srcHeight; ++srcY,srcPixelOffset+=srcWidth/*,dstPixelOffset+=dstWidth*/) {
				//dstRowBuffers[srcY]=new double[dstWidth];
				bufferRowIndex[srcY]=lastBufferRowIndex;
				lastBufferRowIndex+=dstWidth;
				for (int dstX=0; dstX<dstWidth; ++dstX) {
					int arrayOfSrcIndices[] = hVector.arrayOfSrcIndices[dstX];
					//double arrayOfWeights[] = hVector.arrayOfWeights[dstX];
					int arrayOfWeights[] = hVector.arrayOfWeights[dstX];
					//double     sumOfWeights = hVector.sumOfWeights[dstX];
					int     sumOfWeights = hVector.sumOfWeights[dstX];
					int     numberOfEntries = hVector.numberOfEntries[dstX];
					//double dstPixel=0;
					int dstPixel=0;
					for (int x=0; x<numberOfEntries; ++x) {
						int srcX=arrayOfSrcIndices[x];
						//double weightX=arrayOfWeights[x];
						int weightX=arrayOfWeights[x];
						//dstPixel+=(double)((int)srcPixels[srcPixelOffset+srcX]&srcmask)*weightX;
						dstPixel+=((int)srcPixels[srcPixelOffset+srcX]&srcmask)*weightX;	// tested for DataBufferUShort only
					}
					//dstRowBuffers[srcY][dstX]=dstPixel/sumOfWeights;
					dstRowBuffers[dstPixelOffset++]=dstPixel/sumOfWeights;
				}
			}

			//int pixelOffset = 0;
			for (int dstY=0; dstY<dstHeight; ++dstY) {
				int arrayOfSrcIndices[] = vVector.arrayOfSrcIndices[dstY];
				//double arrayOfWeights[] = vVector.arrayOfWeights[dstY];
				int arrayOfWeights[] = vVector.arrayOfWeights[dstY];
				//double     sumOfWeights = vVector.sumOfWeights[dstY];
				int     sumOfWeights = vVector.sumOfWeights[dstY];
				int     numberOfEntries = vVector.numberOfEntries[dstY];
				int pixelOffset = dstY*dstWidth;
				for (int dstX=0; dstX<dstWidth; ++dstX) {
					//double dstPixel=0;
					int dstPixel=0;
					for (int y=0; y<numberOfEntries; ++y) {
						int srcY=arrayOfSrcIndices[y];
						//double weightY=arrayOfWeights[y];
						int weightY=arrayOfWeights[y];
						//dstPixel+=(double)dstRowBuffers[srcY][dstX]*weightY;
						//dstPixel+=(double)dstRowBuffers[srcY*dstWidth+dstX]*weightY;	// this multiplication is a big hit
						//dstPixel+=(double)dstRowBuffers[bufferRowIndex[srcY]+dstX]*weightY;
						dstPixel+=(int)dstRowBuffers[bufferRowIndex[srcY]+dstX]*weightY;
					}
					dstPixels[pixelOffset+dstX]=(short)(dstPixel/sumOfWeights);		// this is faster than single increment
					//dstPixels[pixelOffset++]=(short)(dstPixel/sumOfWeights);
				}
			}
				
//System.err.println("resample: done with pixel copy ="+(System.currentTimeMillis()-startTime)+" ms");
			DataBuffer dstDataBuffer = new DataBufferUShort(dstPixels,dstWidth,0);
			WritableRaster dstRaster = Raster.createWritableRaster(dstSampleModel,dstDataBuffer,new Point(0,0));
			dstImage = new BufferedImage(dstColorModel,dstRaster,true,null);	// no properties hash table
//System.err.println("resample: done with creating dstImage ="+(System.currentTimeMillis()-startTime)+" ms");
                }
                else {
//System.err.println("resample: No conversion supported");
                        dstImage=srcImage;	// do no resampling and hope for the best (performance) :(
                }

//System.err.println("resample() elapsed: "+(System.currentTimeMillis()-startTime)+" ms");
                return dstImage;
	}

	/**
	 * @param	g
	 */
	public void paintComponent(Graphics g) {
//System.err.println("paintComponent: start");
//long startTime = System.currentTimeMillis();
		Cursor was = getCursor();
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		
boolean resamplingOurselves=true;

		int useSrcImageIndex = currentSrcImageSortOrder == null ? currentSrcImageIndex : currentSrcImageSortOrder[currentSrcImageIndex];
//System.err.println("SingleImagePanel.paintComponent() useSrcImageIndex: "+useSrcImageIndex);
		BufferedImage useSrcImage = srcImages[useSrcImageIndex];

		 trackWidth = useSrcImage.getWidth();
		trackHeight = useSrcImage.getHeight();

		// check whether or not we need to resample the source image ourselves (again possibly),
		// and if so flush cache ...
		
		Rectangle windowSize = this.getBounds();
//System.err.println("paintComponent: windowSize.height were "+windowSize.height);
//System.err.println("paintComponent: windowSize.width  were "+windowSize.width);
		double hScaleFactor = ((double)(windowSize.width))  / useSrcImage.getWidth();
		double vScaleFactor = ((double)(windowSize.height)) / useSrcImage.getHeight();
//System.err.println("paintComponent: hScaleFactor "+hScaleFactor);
//System.err.println("paintComponent: vScaleFactor "+vScaleFactor);
		useScaleFactor = hScaleFactor > vScaleFactor ? vScaleFactor : hScaleFactor;	// choose the smallest	... also used later in mouse position tracking
//System.err.println("paintComponent: useScaleFactor "+useScaleFactor);
		windowSize.height = (int)(useScaleFactor * useSrcImage.getHeight() + 0.5);
		windowSize.width  = (int)(useScaleFactor * useSrcImage.getWidth()  + 0.5);
//System.err.println("paintComponent: windowSize.height now "+windowSize.height);
//System.err.println("paintComponent: windowSize.width  now "+windowSize.width);
		
		if (cachedResizedImage != null && (windowSize.height != cachedResizedImage.getHeight() || windowSize.width != cachedResizedImage.getWidth())) {
//System.err.println("paintComponent: size changed or never the same, so flushing cache");
			cachedResizedImage=null;
			cachedPreWindowedImage=null;
		}
		
		if (cachedPreWindowedImage == null) {
//System.err.println("paintComponent: cachedPreWindowedImage is null");
			if (useSrcImage.getColorModel().getNumComponents() == 1) {
				// only downsample ourselves, since upsampling would increase number of pixels to window
				if (resamplingOurselves && cachedResizedImage == null
				 && (windowSize.height != useSrcImage.getHeight() || windowSize.width != useSrcImage.getWidth())
				 && (windowSize.height <= useSrcImage.getHeight() && windowSize.width <= useSrcImage.getWidth())
				) {
//System.err.println("paintComponent: resizing ourselves");
					cachedResizedImage=resample(useSrcImage,windowSize.width,windowSize.height);
				}
				if (cachedResizedImage != null) {
//System.err.println("paintComponent: using cachedResizedImage");
					useSrcImage=cachedResizedImage;
				}
			
				// First, find pre-selected VOI for this frame, if any
				
//System.err.println("currentVOITransformInUse "+currentVOITransformInUse);
				if (currentVOITransformInUse != -1
				 && voiTransform != null
				 && voiTransform.getNumberOfTransforms(useSrcImageIndex) > currentVOITransformInUse) {
					windowWidth=voiTransform.getWidth(useSrcImageIndex,currentVOITransformInUse);
					windowCenter=voiTransform.getCenter(useSrcImageIndex,currentVOITransformInUse);
//System.err.println("For new frame "+useSrcImageIndex+" using preselected center "+windowCenter+" and width "+windowWidth);
				}
				else {
					//currentVOITransformInUse=-1;
					// Just leave it alone (but don't disable use of selection for other frames)
//System.err.println("For new frame "+useSrcImageIndex+" using user selected center "+windowCenter+" and width "+windowWidth);
				}
				
				// Second, find rescale attributes for this frame
				
				if (modalityTransform != null) {
					    useSlope = modalityTransform.getRescaleSlope    (useSrcImageIndex);
					useIntercept = modalityTransform.getRescaleIntercept(useSrcImageIndex);
//System.err.println("For new frame "+useSrcImageIndex+" using preselected rescale slope "+useSlope+" and intercept "+useIntercept);
				}
				else {
					useSlope=1.0;
					useIntercept=0.0;
//System.err.println("For new frame "+useSrcImageIndex+" using default rescale slope "+useSlope+" and intercept "+useIntercept);
				}
				
				// Finally, actually build the destination image
				
				cachedPreWindowedImage = (numberOfEntries == 0 || redTable == null)
					//? applyWindowCenterAndWidth(useSrcImage,windowCenter,windowWidth,signed,inverted,useSlope,useIntercept)
					? (useVOIFunction == 1
						? applyWindowCenterAndWidthLogistic(useSrcImage,windowCenter,windowWidth,signed,inverted,useSlope,useIntercept)
						: applyWindowCenterAndWidthLinear(useSrcImage,windowCenter,windowWidth,signed,inverted,useSlope,useIntercept)
					  )
					: applyWindowCenterAndWidthWithPaletteColor(useSrcImage,windowCenter,windowWidth,signed,inverted,useSlope,useIntercept,
						largestGray,bitsPerEntry,numberOfEntries,redTable,greenTable,blueTable);
                        }
			else {
//System.err.println("SingleImagePanel.paintComponent() using useSrcImage as cachedPreWindowedImage="+useSrcImage);
				cachedPreWindowedImage=useSrcImage;
                        }
//System.err.println("SingleImagePanel.paintComponent() mrj.version="+System.getProperty("mrj.version"));
                        if (useConvertToMostFavorableImageType) cachedPreWindowedImage=convertToMostFavorableImageTypeWithPixelCopy(cachedPreWindowedImage);
                        //if (useConvertToMostFavorableImageType) cachedPreWindowedImage=convertToMostFavorableImageTypeWithBandCombineOp(cachedPreWindowedImage);
                        //if (System.getProperty("mrj.version") != null) cachedPreWindowedImage=convertToMostFavorableImageTypeWithPixelCopy(cachedPreWindowedImage);
                        //if (System.getProperty("mrj.version") != null) cachedPreWindowedImage=convertToMostFavorableImageTypeWithBandCombineOp(cachedPreWindowedImage);
		}
		
//System.err.println("paintComponent: draw to dst image start");
		Graphics2D g2d=(Graphics2D)g;
		if (windowSize.height != cachedPreWindowedImage.getHeight() || windowSize.width != cachedPreWindowedImage.getWidth()) {
//System.err.println("paintComponent: resizing draw");
			//g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			//g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			g2d.drawImage(cachedPreWindowedImage,0,0,windowSize.width,windowSize.height,this);	// follows resize, but (can be) very slow
		}
		else {
//System.err.println("paintComponent: same size draw");
			g2d.drawImage(cachedPreWindowedImage,0,0,this);				// (usually) faster but doesn't scale on resize
		}
//System.err.println("paintComponent: draw to dst image end");

		Color interactiveColor = Color.red;
		Color localizerColor = Color.green;
		Color volumeLocalizationColor = Color.blue;
		Color preDefinedColor = Color.yellow;
		int lineWidth = 2;
		Font preDefinedFont = new Font("SansSerif",Font.BOLD,14);
		
		// draw any graphics that are displayed area relative ...
				
		if (demographicAndTechniqueAnnotations != null) {
			g2d.setColor(demographicAndTechniqueColor);
			g2d.setFont(demographicAndTechniqueFont);

			Iterator i = demographicAndTechniqueAnnotations.iterator(useSrcImageIndex);
			while (i.hasNext()) {
				TextAnnotationPositioned.drawPositionedString((TextAnnotationPositioned)i.next(),g2d,this,5,5);
			}

		}
		
		{
			g2d.setColor(orientationColor);
			g2d.setFont(orientationFont);
			// draw orientation
			if (orientationAnnotations != null) {
				String rowOrientation = orientationAnnotations.getRowOrientation(useSrcImageIndex);
				if (rowOrientation != null && rowOrientation.length() > 0) {
					TextAnnotationPositioned.drawVerticallyCenteredString(rowOrientation,false/*isLeft*/,g2d,this,5);
				}
				String columnOrientation = orientationAnnotations.getColumnOrientation(useSrcImageIndex);
				if (columnOrientation != null && columnOrientation.length() > 0) {
					TextAnnotationPositioned.drawHorizontallyCenteredString(columnOrientation,false/*isTop*/,g2d,this,5);
				}
			}
		}

		// draw any graphics that are image relative, by applying a scaled AffineTransform to the graphics context ...

//System.err.println("paintComponent: draw any graphics");
		g2d.transform(AffineTransform.getScaleInstance(useScaleFactor,useScaleFactor));

		if (interactiveDrawingShapes != null) {
//System.err.println("paintComponent: Draw interactive shapes:");
//LocalizerPosterFactory.dumpShapes(interactiveDrawingShapes);
			g2d.setColor(interactiveColor);
			//g2d.setXORMode(Color.blue);
			Iterator i = interactiveDrawingShapes.iterator();
			while (i.hasNext()) {
				g2d.draw((Shape)i.next());
			}
		}

		if (volumeLocalizationShapes != null) {
//System.err.println("paintComponent: draw volume localization shapes");
//LocalizerPosterFactory.dumpShapes(volumeLocalizationShapes);
			g2d.setColor(volumeLocalizationColor);
			//g2d.setXORMode(Color.blue);
			//g2d.draw(localizerShape);
			Iterator i = volumeLocalizationShapes.iterator();
			while (i.hasNext()) {
				g2d.draw((Shape)i.next());
			}
		}
		
		if (localizerShapes != null) {			// do this after volumeLocalizationShapes, in case need to draw on top
//System.err.println("paintComponent: draw localizer shapes");
//LocalizerPosterFactory.dumpShapes(localizerShapes);
			g2d.setColor(localizerColor);
			//g2d.setXORMode(Color.blue);
			//g2d.draw(localizerShape);
			Iterator i = localizerShapes.iterator();
			while (i.hasNext()) {
				g2d.draw((Shape)i.next());
			}
		}
		
		

		if (preDefinedShapes != null) {
			g2d.setColor(preDefinedColor);
			g2d.setStroke(new BasicStroke(lineWidth));
			Iterator i = preDefinedShapes.iterator();
			while (i.hasNext()) {
				g2d.draw((Shape)i.next());
			}
		}

		if (preDefinedText != null) {
			g2d.setColor(preDefinedColor);
			g2d.setFont(preDefinedFont);
			Iterator i = preDefinedText.iterator();
			while (i.hasNext()) {
				TextAnnotation text = (TextAnnotation)i.next();
				g2d.drawString(text.getString(),text.getAnchorPointXAsInt(),text.getAnchorPointYAsInt());
			}
		}
		
		setCursor(was);
//System.err.println("paintComponent() elapsed: "+(System.currentTimeMillis()-startTime)+" ms");
	}
}





