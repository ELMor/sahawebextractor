/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.display;

import java.awt.*; 
import java.awt.event.*; 
import java.awt.image.*; 
import java.awt.color.*; 
import java.util.*; 
import java.io.*; 
import javax.swing.*; 
import javax.swing.event.*;

import com.pixelmed.dicom.*;

/**
 * <p>A class that encapsulates the pixel data and features and values from an image source
 * (such as a DICOM image), usually for the purpose of displaying it.</p>
 *
 * @see com.pixelmed.display.SingleImagePanel
 *
 * @author	dclunie
 */
public class SourceImage {

	/***/
	private static final String identString = "@(#) $Header$";

	/***/
	BufferedImage[] imgs;
	/***/
	int width;
	/***/
	int height;
	/***/
	int nframes;
	/***/
	int imgMin;
	/***/
	int imgMax;
	/***/
	double imgMean;
	/***/
	double imgStandardDeviation;
	/***/
	boolean signed;
	/***/
	boolean inverted;
	/***/
	boolean isGrayscale;
	/***/
	boolean isPaletteColor;
	/***/
	String title;

	// stuff for (supplemental) palette color LUT
	/***/
	private int largestGray;
	/***/
	private int firstValueMapped;
	/***/
	private int numberOfEntries;
	/***/
	private int bitsPerEntry;
	/***/
	private short redTable[];
	/***/
	private short greenTable[];
	/***/
	private short blueTable[];
	
	/***/
	private RealWorldValueTransform realWorldValueTransform;

	/***/
	private ModalityTransform modalityTransform;

	/***/
	private VOITransform voiTransform;

	/**
	 * @param	w
	 * @param	h
	 * @param	data
	 * @param	offset
	 */
	private static BufferedImage createUnsignedShortGrayscaleImage(int w,int h,short data[],int offset) {
//System.err.println("createUnsignedShortGrayscaleImage");
		ComponentColorModel cm=new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] {16},
			false,		// has alpha
			false,		// alpha premultipled
			Transparency.OPAQUE,
			DataBuffer.TYPE_USHORT
		);

		ComponentSampleModel sm = new ComponentSampleModel(
			DataBuffer.TYPE_USHORT,
			w,
			h,
			1,
			w,
			new int[] {0}
		);

		DataBuffer buf = new DataBufferUShort(data,w,offset);

		WritableRaster wr = Raster.createWritableRaster(sm,buf,new Point(0,0));

		return new BufferedImage(cm,wr,true,null);	// no properties hash table
	}

	/**
	 * @param	w
	 * @param	h
	 * @param	data
	 * @param	offset
	 */
	private static BufferedImage createSignedShortGrayscaleImage(int w,int h,short data[],int offset) {
//System.err.println("createSignedShortGrayscaleImage");
		// DataBufferUShort and DataBuffer.TYPE_USHORT are used here,
		// otherwise lookup table operations for windowing fail;
		// concept of signedness is conveyed separately;
		// causes issues with JAI operations expecting signed shorts
		ComponentColorModel cm=new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] {16},
			false,		// has alpha
			false,		// alpha premultipled
			Transparency.OPAQUE,
			/*DataBuffer.TYPE_SHORT*/DataBuffer.TYPE_USHORT
		);

		ComponentSampleModel sm = new ComponentSampleModel(
			/*DataBuffer.TYPE_SHORT*/DataBuffer.TYPE_USHORT,
			w,
			h,
			1,
			w,
			new int[] {0}
		);

		DataBuffer buf = new /*DataBufferShort*/DataBufferUShort(data,w,offset);

		WritableRaster wr = Raster.createWritableRaster(sm,buf,new Point(0,0));

		return new BufferedImage(cm,wr,true,null);	// no properties hash table
	}

	/**
	 * @param	w
	 * @param	h
	 * @param	data
	 * @param	offset
	 */
	private static BufferedImage createByteGrayscaleImage(int w,int h,byte data[],int offset) {
//System.err.println("createByteGrayscaleImage");
		ComponentColorModel cm=new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] {8},
			false,		// has alpha
			false,		// alpha premultipled
			Transparency.OPAQUE,
			DataBuffer.TYPE_BYTE
		);

		ComponentSampleModel sm = new ComponentSampleModel(
			DataBuffer.TYPE_BYTE,
			w,
			h,
			1,
			w,
			new int[] {0}
		);

		DataBuffer buf = new DataBufferByte(data,w,offset);
                
		WritableRaster wr = Raster.createWritableRaster(sm,buf,new Point(0,0));
                
                return new BufferedImage(cm,wr,true,null);	// no properties hash table
	}

	/**
	 * @param	w
	 * @param	h
	 * @param	data
	 * @param	offset
	 */
	private static BufferedImage createPixelInterleavedByteRGBImage(int w,int h,byte data[],int offset) {
//System.err.println("createPixelInterleavedByteRGBImage");
		ComponentColorModel cm=new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] {8,8,8},
			false,		// has alpha
			false,		// alpha premultipled
			Transparency.OPAQUE,
			DataBuffer.TYPE_BYTE
		);

		ComponentSampleModel sm = new ComponentSampleModel(
			DataBuffer.TYPE_BYTE,
			w,
			h,
			3,
			w*3,
			new int[] {0,1,2}
		);
		//PixelInterleavedSampleModel sm = new PixelInterleavedSampleModel(
		//	DataBuffer.TYPE_BYTE,
		//	w,
		//	h,
		//	3,
		//	w*3,
		//	new int[] {0,1,2}
		//);

		DataBuffer buf = new DataBufferByte(data,w,offset);

		WritableRaster wr = Raster.createWritableRaster(sm,buf,new Point(0,0));

		return new BufferedImage(cm,wr,true,null);	// no properties hash table
	}

	/**
	 * @param	w
	 * @param	h
	 * @param	data
	 * @param	offset
	 */
	private static BufferedImage createBandInterleavedByteRGBImage(int w,int h,byte data[],int offset) {
//System.err.println("createBandInterleavedByteRGBImage");
		ComponentColorModel cm=new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] {8,8,8},
			false,		// has alpha
			false,		// alpha premultipled
			Transparency.OPAQUE,
			DataBuffer.TYPE_BYTE
		);

		ComponentSampleModel sm = new ComponentSampleModel(
			DataBuffer.TYPE_BYTE,
			w,
			h,
			1,
			w,
			new int[] {0,w*h,w*h*2}
		);
		//BandedSampleModel sm = new BandedSampleModel(		// doesn't work
		//	DataBuffer.TYPE_BYTE,
		//	w,
		//	h,
		//	w,
		//	new int[] {0,1,2},				// what should this be ?
		//	new int[] {0,w*h,w*h*2}
		//);

		DataBuffer buf = new DataBufferByte(data,w,offset);

		WritableRaster wr = Raster.createWritableRaster(sm,buf,new Point(0,0));

		return new BufferedImage(cm,wr,true,null);	// no properties hash table
	}

	/**
	 * <p>Construct a single-frame image from an unsigned raw image (no header) on
	 * an input stream with the specified width, height and bit depth.</p>
	 *
	 * @param	i		the input stream
	 * @param	w		image width
	 * @param	h		image height
	 * @param	depth		bit depth
	 * @exception	IOException
	 */
	public SourceImage(InputStream i,int w,int h,int depth) throws IOException {
		constructSourceImage(i,w,h,depth,1);
	}

	/**
	 * <p>Construct a multi-frame image from an unsigned raw image (no header) on
	 * an input stream with the specified width, height and bit depth.</p>
	 *
	 * @param	i		the input stream
	 * @param	w		image width
	 * @param	h		image height
	 * @param	depth		bit depth
	 * @param	frames		number of frames
	 * @exception	IOException
	 */
	public SourceImage(InputStream i,int w,int h,int depth,int frames) throws IOException {
		constructSourceImage(i,w,h,depth,frames);
	}

	/**
	 * @param	i
	 * @param	w
	 * @param	h
	 * @param	depth
	 * @param	frames
	 * @exception	IOException
	 */
	private void constructSourceImage(InputStream i,int w,int h,int depth,int frames) throws IOException {

		width = w;
		height = h;
		nframes=frames;
		if (nframes < 1) return;

		imgs=new BufferedImage[nframes];

		imgMin=65536;
		imgMax=0;

		for (int frame=0; frame < nframes; ++frame) {
			if (depth > 8) {
				short data[] = new short[width*height];
				byte buffer[] = new byte[width*height*2];
				i.read(buffer,0,width*height*2);
				int count=0;
				for (int row=0; row<height; ++row) {
					for (int col=0; col<width; ++col) {
						//int byte1=i.read();
						//int byte2=i.read();
						//if (byte1 == -1 || byte2 == -1) throw new EOFException();
						int byte1=((int)buffer[count++])&0xff;
						int byte2=((int)buffer[count++])&0xff;
						short value=(short)((byte2<<8) + byte1);
						data[row*w+col]=(short)value;
						//if (value > imgMax) imgMax=value;
						if (value > imgMax && value <= largestGray) imgMax=value;
						if (value < imgMin) imgMin=value;
					}
				}
				imgs[frame]=createUnsignedShortGrayscaleImage(width,height,data,0);
			}
			else {
				byte data[] = new byte[width*height];
				i.read(data,0,width*height);
				int count=0;
				for (int row=0; row<height; ++row) {
					for (int col=0; col<width; ++col) {
						//int value=i.read();
						//if (value == -1) throw new EOFException();
						//data[row*w+col]=(byte)value;
						int value=((int)data[count++])&0xff;
						//if (value > imgMax) imgMax=value;
						if (value > imgMax && value <= largestGray) imgMax=value;
						if (value < imgMin) imgMin=value;
					}
				}
				imgs[frame]=createByteGrayscaleImage(width,height,data,0);
			}
		}
	}

	/**
	 * @param	list
	 */
	private static String buildInstanceTitleFromAttributeList(AttributeList list) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.PatientName));
		buffer.append("[");
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.PatientID));
		buffer.append("]");
		buffer.append(":");
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.StudyID));
		buffer.append("[");
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.StudyDate));
		buffer.append(":");
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.StudyDescription));
		buffer.append("]");
		buffer.append(":");
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.SeriesNumber));
		buffer.append("[");
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.Modality));
		buffer.append(":");
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.SeriesDescription));
		buffer.append("]");
		buffer.append(":");
		buffer.append(Attribute.getSingleStringValueOrEmptyString(list,TagFromName.InstanceNumber));
		buffer.append(":");
		return buffer.toString();
	}

	/**
	 * <p>Construct an image from a single or multi-frame DICOM image from
	 * an input stream (such as from a file or the network).</p>
	 *
	 * @param	i		the input stream
	 * @exception	IOException
	 * @exception	DicomException
	 */
	public SourceImage(DicomInputStream i) throws IOException, DicomException {
		AttributeList list = new AttributeList();
		list.read(i);
//System.err.println(list);

		if (list.get(TagFromName.PixelData) != null) constructSourceImage(list);
	}

	/**
	 * <p>Construct an image from a single or multi-frame DICOM image from
	 * a list of DICOM attributes.</p>
	 *
	 * @param	list		the list of attributes that include the description and values of the pixel data
	 * @exception	DicomException
	 */
	public SourceImage(AttributeList list) throws DicomException {
		if (list.get(TagFromName.PixelData) != null) constructSourceImage(list);
	}

	/**
	 * @param	list
	 * @exception	DicomException
	 */
	private void constructSourceImage(AttributeList list) throws DicomException {

//System.err.println("constructSourceImage - start");

                //double imgSum=0;
                //double imgSumOfSquares=0;

		title=buildInstanceTitleFromAttributeList(list);

		width		= list.get(TagFromName.Columns).getIntegerValues()[0];
//System.err.println("width="+width);
		height		= list.get(TagFromName.Rows).getIntegerValues()[0];
//System.err.println("height="+height);
		int depth 	= list.get(TagFromName.BitsAllocated).getIntegerValues()[0];
//System.err.println("depth="+depth);
		int samples	= list.get(TagFromName.SamplesPerPixel).getIntegerValues()[0];
//System.err.println("samples="+samples);
		boolean byplane	= (samples > 1 && list.get(TagFromName.PlanarConfiguration).getIntegerValues()[0] == 1);
//System.err.println("byplane="+byplane);

		Attribute aNumberOfFrames = list.get(TagFromName.NumberOfFrames);
		if (aNumberOfFrames != null) {
			nframes = aNumberOfFrames.getIntegerValues()[0];
		}
		else {
			nframes = 1;
		}
//System.err.println("nframes="+nframes);

		imgs=new BufferedImage[nframes];

		int mask = 0;
		int extend = 0;
		int signbit = 1;
		{
			int stored = list.get(TagFromName.BitsStored).getIntegerValues()[0];
			while (stored-- > 0) {
				mask=mask<<1 | 1;
				signbit=signbit<<1;
			}
			signbit=signbit>>1;
			extend=~mask;
		}
//System.err.println("mask=0x"+Integer.toHexString(mask));
//System.err.println("extend=0x"+Integer.toHexString(extend));
//System.err.println("signbit=0x"+Integer.toHexString(signbit));

		signed	= list.get(TagFromName.PixelRepresentation).getIntegerValues()[0] == 1;

		imgMin=signed ? 0x00007fff : 0x0000ffff;	// i.e. start with the largest  possible 16 bit +ve value, sign extended to the full Java int 32 bits
		imgMax=signed ? 0xffff8000 : 0x00000000;	// i.e. start with the smallest possible 16 bit -ve value, sign extended to the full Java int 32 bits

//System.err.println("signed="+signed);

		int pad=0;
		boolean hasPad=false;
		Attribute aPixelPaddingValue = list.get(TagFromName.PixelPaddingValue);
		if (aPixelPaddingValue != null) {
			hasPad=true;
			pad=aPixelPaddingValue.getIntegerValues()[0];
		}
//System.err.println("hasPad="+hasPad);
//System.err.println("pad="+pad);

		String vPhotometricInterpretation = list.get(TagFromName.PhotometricInterpretation).getStringValues()[0];
		inverted = vPhotometricInterpretation.equals("MONOCHROME1");
//System.err.println("inverted="+inverted);

		isGrayscale = vPhotometricInterpretation.equals("MONOCHROME1") || vPhotometricInterpretation.equals("MONOCHROME2");
//System.err.println("isGrayscale="+isGrayscale);
		isPaletteColor = vPhotometricInterpretation.equals("PALETTE COLOR");
//System.err.println("isPaletteColor="+isPaletteColor);

		// Get palette color LUT stuff, if present ...
		
		Attribute aLargestMonochromePixelValue  = list.get(TagFromName.LargestMonochromePixelValue);
		Attribute aRedPaletteColorLookupTableDescriptor = list.get(TagFromName.RedPaletteColorLookupTableDescriptor);
		Attribute aGreenPaletteColorLookupTableDescriptor = list.get(TagFromName.GreenPaletteColorLookupTableDescriptor);
		Attribute aBluePaletteColorLookupTableDescriptor = list.get(TagFromName.BluePaletteColorLookupTableDescriptor);
	
                largestGray = signed ? 0x00007fff : 0x0000ffff;			// default to largest possible in case nothing found
		boolean usedLargestMonochromePixelValue=false;
                if (aLargestMonochromePixelValue != null && aLargestMonochromePixelValue.getVM() == 1) {
                        usedLargestMonochromePixelValue=true;
                        largestGray=aLargestMonochromePixelValue.getIntegerValues()[0];
                }
		boolean usedLargestImagePixelValue=false;
                if (usedLargestMonochromePixelValue == false) {			// encountered this in an old MR SOP Class Siemens MR image
                        Attribute aLargestImagePixelValue  = list.get(TagFromName.LargestImagePixelValue);
                        if (aLargestImagePixelValue != null && aLargestImagePixelValue.getVM() == 1) {
                                usedLargestImagePixelValue=true;
                                largestGray=aLargestImagePixelValue.getIntegerValues()[0];
                        }
                }
                
 		boolean usedFirstValueMapped=false;
 		if (aRedPaletteColorLookupTableDescriptor != null && aGreenPaletteColorLookupTableDescriptor != null && aBluePaletteColorLookupTableDescriptor != null) {
			// the descriptors should all be the same; should check but let's be lazy and just use one ...
			if (aRedPaletteColorLookupTableDescriptor != null && aRedPaletteColorLookupTableDescriptor.getVM() == 3) {
				numberOfEntries = aRedPaletteColorLookupTableDescriptor.getIntegerValues()[0];
				if (numberOfEntries == 0) numberOfEntries=65536;
				firstValueMapped = aRedPaletteColorLookupTableDescriptor.getIntegerValues()[1];
                                if ((usedLargestMonochromePixelValue == false && usedLargestImagePixelValue == false)
				 || list.get(TagFromName.PhotometricInterpretation).getStringValues()[0].equals("PALETTE COLOR")) { // [bugs.mrmf] (000102) Palette Color image displays as gray when Largest Pixel Value present
                                        usedFirstValueMapped=true;
                                        largestGray=firstValueMapped-1;			// if a pure color image then firstValueMapped will be 0, and largestGray will be -1
                                }
				bitsPerEntry = aRedPaletteColorLookupTableDescriptor.getIntegerValues()[2];
				Attribute aRedPaletteColorLookupTableData = list.get(TagFromName.RedPaletteColorLookupTableData);
				Attribute aGreenPaletteColorLookupTableData = list.get(TagFromName.GreenPaletteColorLookupTableData);
				Attribute aBluePaletteColorLookupTableData = list.get(TagFromName.BluePaletteColorLookupTableData);
				if (aRedPaletteColorLookupTableData != null && aGreenPaletteColorLookupTableData != null && aBluePaletteColorLookupTableData != null) {
					redTable = aRedPaletteColorLookupTableData.getShortValues();
					greenTable = aGreenPaletteColorLookupTableData.getShortValues();
					blueTable = aBluePaletteColorLookupTableData.getShortValues();
				}
			}
		}
//System.err.println("SourceImage.constructSourceImage(): largestGray="+largestGray);		
		int nframepixels=width*height;
		int nframesamples=nframepixels*samples;
		int nsamples=nframesamples*nframes;
//System.err.println("SourceImage.constructSourceImage(): isGrayscale="+isGrayscale);
//System.err.println("SourceImage.constructSourceImage(): samples="+samples);
//System.err.println("SourceImage.constructSourceImage(): depth="+depth);
		if ((isGrayscale || isPaletteColor) && samples == 1 && depth > 8) {
//System.err.println("SourceImage.constructSourceImage(): grayscale or palette color > 8 bits");
			Attribute a = list.get(TagFromName.PixelData);
			short data[] = a.getShortValues();
			if (signed) {
				//boolean dopad = hasPad && pad != 0;	// ignore pad if it is zero and image is signed, e.g. GE MR image pretending to be signed
				for (int count=0; count<nsamples; ++count) {
					int value=((int)data[count])&mask;
					if ((value & signbit) != 0) value=value | extend;
					//if (dopad && (value == pad || value == -32767)) {	// include old GE CT bug (0x8001 pad)
					//	value=0x8000;					// otherwise surround not less than -ve pixel values 
					//}
					data[count]=(short)value;
					//if (value > imgMax) imgMax=value;
					if (value > imgMax && value <= largestGray/* && (!hasPad || value != pad)*/) imgMax=value;
					if (value < imgMin/* && (!hasPad || value != pad)*/) imgMin=value;
                                        //imgSum+=value;
                                        //imgSumOfSquares+=value*value;
				}
				int offset=0;
				for (int frame=0; frame<nframes; ++frame) {
					imgs[frame]=createSignedShortGrayscaleImage(width,height,data,offset);
					offset+=nframesamples;
				}
			}
			else {
				for (int count=0; count<nsamples; ++count) {
					int value=((int)data[count])&mask;
					if (hasPad && value == pad ) {
						value=0;
					}
					data[count]=(short)value;
					//if (value > imgMax) imgMax=value;
					if (value > imgMax && value <= largestGray/* && (!hasPad || value != pad)*/) imgMax=value;
					if (value < imgMin/* && (!hasPad || value != pad)*/) imgMin=value;
                                        //imgSum+=value;
                                        //imgSumOfSquares+=value*value;
				}
				int offset=0;
				for (int frame=0; frame<nframes; ++frame) {
					imgs[frame]=createUnsignedShortGrayscaleImage(width,height,data,offset);
					offset+=nframesamples;
				}
			}
		}
		else if ((isGrayscale || isPaletteColor) && samples == 1 && depth <= 8){
//System.err.println("SourceImage.constructSourceImage(): grayscale or palette color <= 8 bits");
			byte data[] = null;
			Attribute a = list.get(TagFromName.PixelData);
			if (ValueRepresentation.isOtherByteVR(a.getVR())) {
				byte[] sdata = a.getByteValues();
				data = new byte[nsamples];
				for (int count=0; count<nsamples; ++count) {
					int value=((int)sdata[count])&mask;
//System.err.print("value masked = "+Integer.toHexString(value));
					if (signed && (value & signbit) != 0) value=value | extend;
//System.err.println(" extended = "+Integer.toHexString(value));
					if (hasPad && value == pad ) {
						value=0;
					}
					data[count]=(byte)value;
					//if (value > imgMax) imgMax=value;
					if (value > imgMax && value <= largestGray) imgMax=value;
					if (value < imgMin) imgMin=value;
                                        //imgSum+=value;
                                        //imgSumOfSquares+=value*value;
				}
			}
			else {
				short sdata[] = a.getShortValues();
				data = new byte[nsamples];
				int slen=nsamples/2;
				int scount=0;
				int count=0;
				while (scount<slen) {
					int value=((int)sdata[scount++])&0xffff;	// the endianness of the TS has already been accounted for
					int value1=value&mask;				// now just unpack from low part of word first
					if (signed && (value1 & signbit) != 0) value1=value1 | extend;
					if (hasPad && value1 == pad ) {
						value1=0;
					}
					data[count++]=(byte)value1;
					//if (value1 > imgMax) imgMax=value1;
					if (value1 > imgMax && value1 <= largestGray) imgMax=value1;
					if (value1 < imgMin) imgMin=value1;
                                        //imgSum+=value1;
                                        //imgSumOfSquares+=value1*value1;
					
					int value2=(value>>8)&mask;
					if (signed && (value2 & signbit) != 0) value2=value2 | extend;
					if (hasPad && value2 == pad ) {
						value2=0;
					}
					data[count++]=(byte)value2;
					//if (value2 > imgMax) imgMax=value2;
					if (value2 > imgMax && value2 <= largestGray) imgMax=value2;
					if (value2 < imgMin) imgMin=value2;
                                        //imgSum+=value2;
                                        //imgSumOfSquares+=value2*value2;
				}
			}
			int offset=0;
			for (int frame=0; frame<nframes; ++frame) {
				imgs[frame]=createByteGrayscaleImage(width,height,data,offset);
				offset+=nframesamples;
			}
		}
		else if (!isGrayscale && samples == 3 && depth <= 8){
//System.err.println("SourceImage.constructSourceImage(): not grayscale, is 3 channel and <= 8 bits");
			byte data[] = null;
			Attribute a = list.get(TagFromName.PixelData);
			if (ValueRepresentation.isOtherByteVR(a.getVR())) {
				data = a.getByteValues();
			}
			else {
				short sdata[] = a.getShortValues();
				data = new byte[nsamples];
				int slen=nsamples/2;
				int scount=0;
				int count=0;
				while (scount<slen) {
					int value=((int)sdata[scount++])&0xffff;	// the endianness of the TS has already been accounted for
					int value1=value&0xff;				// now just unpack from low part of word first
					data[count++]=(byte)value1;
					int value2=(value>>8)&0xff;
					data[count++]=(byte)value2;
				}
			}
			int offset=0;
			for (int frame=0; frame<nframes; ++frame) {
				imgs[frame]=byplane ? createBandInterleavedByteRGBImage(width,height,data,offset) : createPixelInterleavedByteRGBImage(width,height,data,offset);
				offset+=nframesamples;
			}
		}
		else {
			throw new DicomException("Unsupported image photometric interpretation or bit depth");
		}
                
		//imgMean=imgSum/nsamples;
		//imgStandardDeviation=Math.sqrt((imgSumOfSquares-imgSum*imgSum/nsamples)/nsamples);
//System.err.println("imgMin="+imgMin);
//System.err.println("imgMax="+imgMax);
//System.err.println("imgSum="+imgSum);
//System.err.println("imgSumOfSquares="+imgSumOfSquares);
//System.err.println("imgMean="+imgMean);
//System.err.println("imgStandardDeviation="+imgStandardDeviation);

		realWorldValueTransform = new RealWorldValueTransform(list);
		modalityTransform = new ModalityTransform(list);
		voiTransform = new VOITransform(list);

//System.err.println("constructSourceImage - end");
	}

	/***/
	public BufferedImage getBufferedImage() { return imgs[0]; }

	/***/
	public BufferedImage[] getBufferedImages() { return imgs; }

	/***/
	public int getWidth() { return width; }
        
	/***/
	public int getHeight() { return height; }
        
	/***/
	public int getMinimum() { return imgMin; }

	/***/
	public int getMaximum() { return imgMax; }

	//public double getMean() { return imgMean; }

	//public double getStandardDeviation() { return imgStandardDeviation; }

	/***/
	public boolean isSigned() { return signed; }

	/***/
	public boolean isInverted() { return inverted; }

	/***/
	public boolean isGrayscale() { return isGrayscale; }

	/***/
	public String getTitle() { return title; }

	/***/
	public int getNumberOfFrames() { return nframes; }

	/***/
	public int getPaletteColorLargestGray() { return largestGray; }
	
	/***/
	public int getPaletteColorFirstValueMapped() { return firstValueMapped; }
	
	/***/
	public int getPaletteColorNumberOfEntries() { return numberOfEntries; }
	
	/***/
	public int getPaletteColorBitsPerEntry() { return bitsPerEntry; }
	
	/***/
	public short[] getPaletteColorRedTable() { return redTable; }
	
	/***/
	public short[] getPaletteColorGreenTable() { return greenTable; }
	
	/***/
	public short[] getPaletteColorBlueTable() { return blueTable; }
	
	/***/
	public RealWorldValueTransform getRealWorldValueTransform() { return realWorldValueTransform; }
	
	/***/
	public ModalityTransform getModalityTransform() { return modalityTransform; }
	
	/***/
	public VOITransform getVOITransform() { return voiTransform; }
}
