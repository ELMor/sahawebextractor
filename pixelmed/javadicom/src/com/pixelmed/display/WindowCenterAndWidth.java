/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.display;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.ByteLookupTable;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.LookupOp;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;

import javax.imageio.ImageIO;

import com.pixelmed.dicom.Attribute;
import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.DicomInputStream;
import com.pixelmed.dicom.ModalityTransform;
import com.pixelmed.dicom.SOPClass;
import com.pixelmed.dicom.TagFromName;
import com.pixelmed.dicom.VOITransform;

/**
 * <p>A class of static methods to perform window operations on images.</p>
 *
 * @author	dclunie
 */
public class WindowCenterAndWidth {
	
	/**
	 * @param	src
	 * @param	center
	 * @param	width
	 * @param	signed
	 * @param	inverted
	 * @param	useSlope
	 * @param	useIntercept
	 */
	public static final BufferedImage applyWindowCenterAndWidthLogistic(BufferedImage src,double center,double width,
		boolean signed,boolean inverted,double useSlope,double useIntercept) {
//System.err.println("applyWindowCenterAndWidthLogistic(): center="+center+" width="+width);

		int       ymin = 0;
		int       ymax = 255;
				
		int startx;
		int endx;
		byte lut[] = null;
		int mask;
		
//System.err.println("applyWindowCenterAndWidth bottom="+bottom+" top="+top);
		int dataType = src.getSampleModel().getDataType();
//System.err.println("Data type "+dataType);
		if (dataType == DataBuffer.TYPE_SHORT || dataType == DataBuffer.TYPE_USHORT) {
//System.err.println("Data type is short or ushort and signed is "+signed);
			startx = signed ? -32768 : 0;
			endx   = signed ?  32768 : 65536;
			lut=new byte[65536];
			mask=0xffff;
		}
		else if (dataType == DataBuffer.TYPE_BYTE) {
//System.err.println("Data type is byte and signed is "+signed);
			startx = signed ? -128 : 0;
			endx   = signed ?  128 : 256;
			lut=new byte[256];
			mask=0xff;
		}
		else {
			throw new IllegalArgumentException();
		}
		
		//double a = ymax;
		//double b = 1/width;
		//double c = center < 0 ? 0 : center;
		//for (int xi=startx; xi<endx; ++xi) {
		//	double x = xi*useSlope+useIntercept;
		//	double y = a / (1 + c * Math.exp(-b*x)) + 0.5;
		//	if (y < ymin)  y=ymin;
		//	else if (y > ymax) y=ymax;
		//	if (inverted) y=(byte)(ymax-y);
		//	lut[xi&mask]=(byte)y;
		//}
		
		int yrange = ymax - ymin;
		
		for (int xi=startx; xi<endx; ++xi) {
			double x = xi*useSlope+useIntercept;
			double y = yrange / (1 + Math.exp(-4*(x - center)/width)) + ymin + 0.5;
			if (y < ymin)  y=ymin;
			else if (y > ymax) y=ymax;
			if (inverted) y=(byte)(ymax-y);
//if (xi%16 == 0) System.err.println(xi+"\t"+(((int)y)&0xff));
			lut[xi&mask]=(byte)y;
		}
		LookupOp lookup=new LookupOp(new ByteLookupTable(0,lut), null);
		ColorModel dstColorModel=new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] {8},
			false,		// has alpha
			false,		// alpha premultipled
			Transparency.OPAQUE,
			DataBuffer.TYPE_BYTE
		);
		BufferedImage dst = lookup.filter(src,lookup.createCompatibleDestImage(src,dstColorModel));	// Fails if src is DataBufferShort
		return dst;
	}

	/**
	 * @param	src
	 * @param	center
	 * @param	width
	 * @param	signed
	 * @param	inverted
	 * @param	useSlope
	 * @param	useIntercept
	 */
	public static final BufferedImage applyWindowCenterAndWidthLinear(BufferedImage src,double center,double width,
		boolean signed,boolean inverted,double useSlope,double useIntercept) {
//System.err.println("applyWindowCenterAndWidthLinear(): center="+center+" width="+width);

		int       ymin = 0;
		int       ymax = 255;

		byte     bymin = (byte)ymin;
		byte     bymax = (byte)ymax;
		double  yrange = ymax - ymin;

		double    cmp5 = center - 0.5;
		double     wm1 = width - 1.0;
		double halfwm1 = wm1/2.0;
		double  bottom = cmp5 - halfwm1;
		double     top = cmp5 + halfwm1;
		
		int startx;
		int endx;
		byte lut[] = null;
		int mask;
		
//System.err.println("applyWindowCenterAndWidthLinear(): bottom="+bottom+" top="+top);
		int dataType = src.getSampleModel().getDataType();
//System.err.println("applyWindowCenterAndWidthLinear(): Data type "+dataType);
		if (dataType == DataBuffer.TYPE_SHORT || dataType == DataBuffer.TYPE_USHORT) {
//System.err.println("Data type is short or ushort and signed is "+signed);
			startx = signed ? -32768 : 0;
			endx   = signed ?  32768 : 65536;
			lut=new byte[65536];
			mask=0xffff;
		}
		else if (dataType == DataBuffer.TYPE_BYTE) {
//System.err.println("applyWindowCenterAndWidthLinear(): Data type is byte and signed is "+signed);
			startx = signed ? -128 : 0;
			endx   = signed ?  128 : 256;
			lut=new byte[256];
			mask=0xff;
		}
		else {
			throw new IllegalArgumentException();
		}
		
		for (int xi=startx; xi<endx; ++xi) {
			double x = xi*useSlope+useIntercept;
			byte y;
			if (x <= bottom)  y=bymin;
			else if (x > top) y=bymax;
			else {
				y = (byte)(((x-cmp5)/wm1 + 0.5)*yrange+ymin);
//System.err.println("xi&0xffff="+(xi&0xffff)+" y="+((int)y&0xff)+" x="+x);
			}
			
			if (inverted) y=(byte)(ymax-y);
//if (xi%16 == 0) System.err.println(xi+"\t"+(((int)y)&0xff));
			lut[xi&mask]=y;
		}
		LookupOp lookup=new LookupOp(new ByteLookupTable(0,lut), null);
		ColorModel dstColorModel=new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] {8},
			false,		// has alpha
			false,		// alpha premultipled
			Transparency.OPAQUE,
			DataBuffer.TYPE_BYTE
		);
		BufferedImage dst = lookup.filter(src,lookup.createCompatibleDestImage(src,dstColorModel));	// Fails if src is DataBufferShort
//System.err.println("applyWindowCenterAndWidthLinear(): BufferedImage out of LookupOp"+dst);
		return dst;
	}

	/**
	 * @param	src
	 * @param	center
	 * @param	width
	 * @param	signed
	 * @param	inverted
	 * @param	useSlope
	 * @param	useIntercept
	 * @param	largestGray
	 * @param	bitsPerEntry
	 * @param	numberOfEntries
	 * @param	redTable
	 * @param	greenTable
	 * @param	blueTable
	 */
	public static final BufferedImage applyWindowCenterAndWidthWithPaletteColor(BufferedImage src,double center,double width,
			boolean signed,boolean inverted,double useSlope,double useIntercept,
			int largestGray,int bitsPerEntry,int numberOfEntries,
			short[] redTable,short[] greenTable,short[] blueTable) {
//System.err.println("applyWindowCenterAndWidthWithPaletteColor center="+center+" width="+width);

		int       ymin = 0;
		int       ymax = 255;

		byte     bymin = (byte)ymin;
		byte     bymax = (byte)ymax;
		double  yrange = ymax - ymin;

		double    cmp5 = center - 0.5;
		double     wm1 = width - 1.0;
		double halfwm1 = wm1/2.0;
		double  bottom = cmp5 - halfwm1;
		double     top = cmp5 + halfwm1;
		
		//double gamma = 2.2;

		int startx = signed ? -32768 : 0;		// hmmm ....
		int endx   = signed ?  32768 : 65536;
		//int endx   = largestGray;
		byte rlut[]=new byte[65536];
		byte glut[]=new byte[65536];
		byte blut[]=new byte[65536];
		for (int xi=startx; xi<endx; ++xi) {
			double x = xi*useSlope+useIntercept;
			byte y;
			if (x <= bottom)  y=bymin;
			else if (x > top) y=bymax;
			else {
				y = (byte)(((x-cmp5)/wm1 + 0.5)*yrange+ymin);
//System.err.println("xi&0xffff="+(xi&0xffff)+" y="+((int)y&0xff)+" x="+x);
			}
			
			if (inverted) y=(byte)(ymax-y);

			rlut[xi&0xffff]=y;
			glut[xi&0xffff]=y;
			blut[xi&0xffff]=y;
		}
//System.err.println("SingleImagePanel.applyWindowCenterAndWidthWithPaletteColor(): numberOfEntries = "+numberOfEntries);
//System.err.println("SingleImagePanel.applyWindowCenterAndWidthWithPaletteColor(): redTable.length = "+redTable.length);
//System.err.println("SingleImagePanel.applyWindowCenterAndWidthWithPaletteColor(): greenTable.length = "+greenTable.length);
//System.err.println("SingleImagePanel.applyWindowCenterAndWidthWithPaletteColor(): blueTable.length = "+blueTable.length);
//System.err.println("SingleImagePanel.applyWindowCenterAndWidthWithPaletteColor(): largestGray="+largestGray);

		int shiftRight = bitsPerEntry-8;
		int i;
		int xi;
		for (xi=largestGray+1,i=0; i < numberOfEntries; ++xi,++i) {
//System.err.println("applyWindowCenterAndWidthWithPaletteColor: xi="+xi+" i="+i);
//System.err.println("applyWindowCenterAndWidthWithPaletteColor: redTable[i]="+redTable[i]);
//System.err.println("applyWindowCenterAndWidthWithPaletteColor: redTable[i]>>shiftRight="+(redTable[i]>>shiftRight));
//System.err.println("applyWindowCenterAndWidthWithPaletteColor: redTable[i]>>shiftRight="+Integer.toHexString((redTable[i]>>shiftRight)&0xff));
			rlut[xi&0xffff]=(byte)(redTable[i]>>shiftRight);
			glut[xi&0xffff]=(byte)(greenTable[i]>>shiftRight);
			blut[xi&0xffff]=(byte)(blueTable[i]>>shiftRight);
		}
		
		int columns = src.getWidth();
		int rows = src.getHeight();
        
		SampleModel srcSampleModel = src.getSampleModel();
		WritableRaster srcRaster = src.getRaster();
		DataBuffer srcDataBuffer = srcRaster.getDataBuffer();
		int srcNumBands = srcRaster.getNumBands();

		ColorModel dstColorModel = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getColorModel();
		WritableRaster dstRaster = dstColorModel.createCompatibleWritableRaster(columns,rows);
		DataBuffer dstDataBuffer = dstRaster.getDataBuffer();
		BufferedImage dst = new BufferedImage(dstColorModel, dstRaster, dstColorModel.isAlphaPremultiplied(), null);
		SampleModel dstSampleModel = dst.getSampleModel();
		int dstNumBands = dstRaster.getNumBands();
        
		int srcPixels[] = null; // to disambiguate SampleModel.getPixels() method signature
		srcPixels = srcSampleModel.getPixels(0,0,columns,rows,srcPixels,srcDataBuffer);
		int srcPixelsLength = srcPixels.length;

		int dstPixels[] = null; // to disambiguate SampleModel.getPixels() method signature
		dstPixels = dstSampleModel.getPixels(0,0,columns,rows,dstPixels,dstDataBuffer);
		int dstPixelsLength = dstPixels.length;
        
//System.err.println("applyWindowCenterAndWidthWithPaletteColor: dstNumBands = "+dstNumBands);
		if (srcNumBands == 1 && dstNumBands == 4 && srcPixelsLength*4 == dstPixelsLength) {
//System.err.println("applyWindowCenterAndWidthWithPaletteColor: converting gray to RGBA");
			int dstIndex=0;
			for (int srcIndex=0; srcIndex<srcPixelsLength; ++srcIndex) {
				dstPixels[dstIndex++]=rlut[srcPixels[srcIndex]];
				dstPixels[dstIndex++]=glut[srcPixels[srcIndex]];
				dstPixels[dstIndex++]=blut[srcPixels[srcIndex]];
				dstPixels[dstIndex++]=-1;
			}
			dstSampleModel.setPixels(0,0,columns,rows,dstPixels,dstDataBuffer);
		}
		else if (srcNumBands == 1 && dstNumBands == 3 && srcPixelsLength*3 == dstPixelsLength) {
//System.err.println("applyWindowCenterAndWidthWithPaletteColor: converting gray to RGB");
			int dstIndex=0;
			for (int srcIndex=0; srcIndex<srcPixelsLength; ++srcIndex) {
				dstPixels[dstIndex++]=rlut[srcPixels[srcIndex]];
				dstPixels[dstIndex++]=glut[srcPixels[srcIndex]];
				dstPixels[dstIndex++]=blut[srcPixels[srcIndex]];
			}
			dstSampleModel.setPixels(0,0,columns,rows,dstPixels,dstDataBuffer);
		}

		return dst;
	}
	
	/**
	 * <p>Read a DICOM image input file, and create a single frame 8-bit per channel image (windowed if grayscale) from the first, or only, frame.</p>
	 *
	 * @param	arg	two parameters, the input file name, output file name, and optionally the format (defaults to jpeg)
	 */
	public static void main(String arg[]) {
		String dicomFileName = arg[0];
		String outputFileName = arg[1];
		String outputFormat = arg.length > 2 ? arg[2] : "jpeg";
		
		AttributeList list = new AttributeList();
		try {
			DicomInputStream i = new DicomInputStream(new BufferedInputStream(new FileInputStream(dicomFileName)));
			list.read(i);
			String sopClassUID = Attribute.getSingleStringValueOrEmptyString(list,TagFromName.SOPClassUID);
			if (SOPClass.isImageStorage(sopClassUID)) {
				SourceImage sImg = new SourceImage(list);
				
				BufferedImage useSrcImage = sImg.getBufferedImage(); 	// possibly the first of multiple
				BufferedImage renderedImage = null;
				
				if (useSrcImage.getColorModel().getNumComponents() != 1) {
					renderedImage=useSrcImage;
				}
				else {
					ModalityTransform modalityTransform = sImg.getModalityTransform();
					VOITransform           voiTransform = sImg.getVOITransform();
					boolean                      signed = sImg.isSigned();
					boolean                    inverted = sImg.isInverted();
					int                     largestGray = sImg.getPaletteColorLargestGray();
					int                firstvalueMapped = sImg.getPaletteColorFirstValueMapped();
					int                 numberOfEntries = sImg.getPaletteColorNumberOfEntries();
					int                    bitsPerEntry = sImg.getPaletteColorBitsPerEntry();
					short                    redTable[] = sImg.getPaletteColorRedTable();
					short                  greenTable[] = sImg.getPaletteColorGreenTable();
					short                   blueTable[] = sImg.getPaletteColorBlueTable();

					double useSlope=1;
					double useIntercept=0;
					if (modalityTransform != null) {
						    useSlope = modalityTransform.getRescaleSlope    (0);		// (first) frame index
						useIntercept = modalityTransform.getRescaleIntercept(0);
					}

					double windowWidth=0;
					double windowCenter=0;
					if (voiTransform != null && voiTransform.getNumberOfTransforms(0) > 0) {	// (first) frame index
						 windowWidth = voiTransform.getWidth(0,0);					// (first) frame index, (first) transform
						windowCenter = voiTransform.getCenter(0,0);
					}
					else {
						throw new Exception("No window in image");
					}
				
System.err.println("Using rescale slope "+useSlope+" and intercept "+useIntercept+" and window center "+windowCenter+" and width "+windowWidth);

					int useVOIFunction = 0;
			
					renderedImage = (numberOfEntries == 0 || redTable == null)
						? (useVOIFunction == 1
							? applyWindowCenterAndWidthLogistic(useSrcImage,windowCenter,windowWidth,signed,inverted,useSlope,useIntercept)
							: applyWindowCenterAndWidthLinear(useSrcImage,windowCenter,windowWidth,signed,inverted,useSlope,useIntercept)
						  )
						: applyWindowCenterAndWidthWithPaletteColor(useSrcImage,windowCenter,windowWidth,sImg.isSigned(),inverted,useSlope,useIntercept,
							largestGray,bitsPerEntry,numberOfEntries,redTable,greenTable,blueTable);
				}
				if (!ImageIO.write(renderedImage,outputFormat,new File(outputFileName))) {
					throw new Exception("Cannot find writer for format"+outputFormat);
				}
			}
			else {
				throw new Exception("SOP Class is not an image");
			}
		}
		catch (Exception e) {
			//System.err.println(e);
			e.printStackTrace(System.err);
		}
	}
}
