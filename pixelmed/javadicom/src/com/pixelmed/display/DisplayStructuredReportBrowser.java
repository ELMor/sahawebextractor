/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.display;

import java.util.*;

import com.pixelmed.dicom.*;

/**
 * @author	dclunie
 */
class DisplayStructuredReportBrowser extends StructuredReportBrowser {

	private static final String identString = "@(#) $Header$";

	private int frameWidthWanted;
	private int frameHeightWanted;

	private Map mapOfSOPInstanceUIDToReferencedFileName;

	/**
	 * @param	list
	 * @param	mapOfSOPInstanceUIDToReferencedFileName
	 * @param	frameWidthWanted
	 * @param	frameHeightWanted
	 * @exception	DicomException
	 */
	public DisplayStructuredReportBrowser(AttributeList list,Map mapOfSOPInstanceUIDToReferencedFileName,
			int frameWidthWanted,int frameHeightWanted) throws DicomException {
		super(list);
		this.mapOfSOPInstanceUIDToReferencedFileName=mapOfSOPInstanceUIDToReferencedFileName;
		this.frameWidthWanted=frameWidthWanted;
		this.frameHeightWanted=frameHeightWanted;
	}

	/**
	 * @param	instances
	 */
	protected void doSomethingWithSelectedSOPInstances(Vector instances) {
		DicomBrowser.loadAndDisplayImagesFromSOPInstances(instances,mapOfSOPInstanceUIDToReferencedFileName,
			frameWidthWanted,frameHeightWanted);
	}
}


