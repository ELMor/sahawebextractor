/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.database;

import com.pixelmed.dicom.*;

import java.sql.*;
import java.util.Iterator;
import java.util.Map;

/**
 * <p>The {@link com.pixelmed.database.StudySeriesInstanceModel StudySeriesInstanceModel} class
 * supports a minimal DICOM Study/Series/Instance model.</p>
 *
 * <p>Matching of each information entity is performed by using only the
 * instance UIDs, not the list of attributes that are used in {@link com.pixelmed.database.StudySeriesInstanceSelectiveMatchModel StudySeriesInstanceSelectiveMatchModel}.</p>
 *
 * <p>Attributes of other DICOM entities than Study, Series and Instance are included at the appropriate lower level entity.</p>
 *
 * @see com.pixelmed.database.StudySeriesInstanceSelectiveMatchModel
 * @see com.pixelmed.database.DicomDictionaryForStudySeriesInstanceModel
 *
 * @author	dclunie
 */
public class StudySeriesInstanceModel extends DicomDatabaseInformationModel {

	/***/
	private static final String identString = "@(#) $Header$";

	/**
	 * @param	databaseName
	 * @exception	DicomException
	 */
	public StudySeriesInstanceModel(String databaseName) throws DicomException {
		super(databaseName,InformationEntity.STUDY,new DicomDictionaryForStudySeriesInstanceModel());
	}

	/**
	 * @param	ie	the information entity
	 * @return		true if the information entity is in the model
	 */
	protected boolean isInformationEntityInModel(InformationEntity ie) {
		return ie == InformationEntity.STUDY
		    || ie == InformationEntity.SERIES
		    || ie == InformationEntity.INSTANCE
		    ;
	}

	/**
	 * @param	ie			the parent information entity
	 * @param	concatenationUID	the ConcatenationUID, if present, else null, as a flag to use concatenations in the model or not
	 * @return				the child information entity
	 */
	private InformationEntity getChildTypeForParent(InformationEntity ie,String concatenationUID) {
		if      (ie == InformationEntity.STUDY)         return InformationEntity.SERIES;
		else if (ie == InformationEntity.SERIES)        return InformationEntity.INSTANCE;
		else return null;
	}

	/**
	 * @param	ie	the parent information entity
	 * @return		the child information entity
	 */
	public InformationEntity getChildTypeForParent(InformationEntity ie) {
		return getChildTypeForParent(ie,"");		// not null ... this method is called e.g. when creating tables, so assume concatenation
	}

	/**
	 * @param	ie	the parent information entity
	 * @param	list	an AttributeList, in which ConcatenationUID may or may not be present,as a flag to use concatenations in the model or not
	 * @return		the child information entity
	 */
	public InformationEntity getChildTypeForParent(InformationEntity ie,AttributeList list) {
		String concatenationUID = Attribute.getSingleStringValueOrNull(list,TagFromName.ConcatenationUID);
		return getChildTypeForParent(ie,concatenationUID);
	}

	/**
	 * @param	ie	the information entity
	 * @return		the name of a column in the table that describes the instance of the information entity
	 */
	public String getDescriptiveColumnName(InformationEntity ie) {
		if      (ie == InformationEntity.STUDY)         return "StudyID";
		else if (ie == InformationEntity.SERIES)        return "SeriesNumber";
		else if (ie == InformationEntity.INSTANCE)      return "InstanceNumber";
		else return null;
	}

	/**
	 * @param	ie	the information entity
	 * @return		the name of another column in the table that describes the instance of the information entity
	 */
	public String getOtherDescriptiveColumnName(InformationEntity ie) {
		return null;
	}

	/**
	 * @param	ie	the information entity
	 * @return		the name of yet another column in the table that describes the instance of the information entity
	 */
	public String getOtherOtherDescriptiveColumnName(InformationEntity ie) {
		if      (ie == InformationEntity.STUDY)         return "StudyDescription";
		else if (ie == InformationEntity.SERIES)        return "SeriesDescription";
		else if (ie == InformationEntity.INSTANCE)      return "ImageComments";
		else return null;
	}

	/**
	 * @param	b
	 * @param	list
	 * @param	ie
	 * @exception	DicomException
	 */
	protected void extendStatementStringWithMatchingAttributesForSelectedInformationEntity(StringBuffer b,AttributeList list,InformationEntity ie) throws DicomException {

		// two possibilities ...
		// 1. iterate through whole list of attributes and insist on match for all present for that IE
		// 2. be more selective ... consider match only on "unique key(s)" for a particular level and ignore others
		//
		// adopt the latter approach ...

		// also need to escape wildcards and so on, but ignore for now ...

		if      (ie == InformationEntity.STUDY) {
			// no AND since would be no parent reference preceding
			//b.append("STUDY.StudyInstanceUID LIKE ");
			b.append("STUDY.StudyInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.StudyInstanceUID)));
		}
		else if (ie == InformationEntity.SERIES) {
			b.append(" AND ");
			//b.append("SERIES.SeriesInstanceUID LIKE ");
			b.append("SERIES.SeriesInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SeriesInstanceUID)));
		}
		else if (ie == InformationEntity.INSTANCE) {
			b.append(" AND ");
			//b.append("INSTANCE.SOPInstanceUID LIKE ");
			b.append("INSTANCE.SOPInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SOPInstanceUID)));
		}
	}
}

