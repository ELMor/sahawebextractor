/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.database;

import com.pixelmed.dicom.*;

import java.sql.*;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

/**
 * <p>The {@link com.pixelmed.database.DicomDatabaseInformationModel DicomDatabaseInformationModel} class
 * is an abstract class that specializes {@link com.pixelmed.database.DatabaseInformationModel DatabaseInformationModel}
 * by adding methods specific to a typical DICOM composite object information model.</p>
 *
 * <p>These methods include support for naming the standard DICOM information entities,
 * building insert statements using all the dictionary attributes for a
 * partcular DICOM information entity, and generating local primary keys.</p>
 *
 * @author	dclunie
 */
public abstract class DicomDatabaseInformationModel extends DatabaseInformationModel {

	/***/
	private static final String identString = "@(#) $Header$";

	/**
	 * @param	databaseName
	 * @param	rootInformationEntity
	 * @param	dictionary
	 * @exception	DicomException
	 */
	public DicomDatabaseInformationModel(String databaseName,InformationEntity rootInformationEntity,DicomDictionary dictionary) throws DicomException {
		super(databaseName,rootInformationEntity,dictionary);
	}

	/**
	 * @param	ie
	 * @param	returnedAttributes
	 */
	public String getNametoDescribeThisInstanceOfInformationEntity(InformationEntity ie,Map returnedAttributes) {
		String s = ie.toString();
		if (ie == InformationEntity.INSTANCE) {
			String sopClassUID = (String)(returnedAttributes.get("SOPCLASSUID"));
			if (sopClassUID != null) {
				if      (SOPClass.isImageStorage(sopClassUID)) s="Image";
				else if (SOPClass.isStructuredReport(sopClassUID)) s="SR Document";
				else if (SOPClass.isWaveform(sopClassUID)) s="Waveform";
				else if (SOPClass.isSpectroscopy(sopClassUID)) s="Spectra";
				else if (SOPClass.isRawData(sopClassUID)) s="Raw Data";
			}
		}
		return s;
	}

	/**
	 * <p>Extend a SQL INSERT statement in the process of being constructed with the names of the attributes in the instance for the entity.</p>
	 *
	 * <p>Called when inserting a new record for an instance of the entity.</p>
	 *
	 * <p>The default implementation adds values for all attributes that are present in the database tables
	 * and present in the instance.</p>
	 *
	 * <p>At the INSTANCE level, InstanceNumber is always added.</p>
	 *
	 * <p>At the INSTANCE level, ImageOrientationPatient within PlaneOrientationSequence within SharedFunctionalGroupsSequence is always added, if present.</p>
	 *
	 * @param	b		the statement being constructed
	 * @param	list		the DICOM attributes of a composite object, containing the attributes describing this instance of the entity
	 * @param	ie		the {@link com.pixelmed.dicom.InformationEntity InformationEntity} for which an insert statement is being constructed
	 * @exception	DicomException	thrown if there are problems extracting the DICOM attributes
	 */
	protected void extendInsertStatementStringWithAttributeNamesForSelectedInformationEntity(StringBuffer b,AttributeList list,InformationEntity ie) throws DicomException {
		TreeSet whatHasBeenAdded = new TreeSet();
		if (ie == InformationEntity.INSTANCE) {
			b.append(",");
			b.append(localFileName);
			whatHasBeenAdded.add(localFileName);
		}
		String tableName = ie.toString().toUpperCase();
		Iterator i = list.values().iterator();
		while (i.hasNext()) {
			Attribute a = (Attribute)i.next();
			AttributeTag tag = a.getTag();
			if (ie == dictionary.getInformationEntityFromTag(tag)) {
				String columnName = dictionary.getNameFromTag(tag);
				String columnType = getSQLTypeFromDicomValueRepresentation(a.getVR());					// use actual, not dictionary VR, in case was explicit
				if (columnName != null && columnType != null && isAttributeUsedInTable(tableName,columnName)		// column type check is to be consistent with value list
				 && !whatHasBeenAdded.contains(columnName)) {								// do not add same attribute twice to same table
					b.append(",");
					b.append(columnName);
					whatHasBeenAdded.add(columnName);
				}
			}
		}
		if (ie == InformationEntity.INSTANCE) {
			AttributeTag tag = TagFromName.InstanceNumber;
			Attribute a = list.get(tag);
			if (a != null) {
				String columnName = dictionary.getNameFromTag(tag);
				String columnType = getSQLTypeFromDicomValueRepresentation(a.getVR());					// use actual, not dictionary VR, in case was explicit
				if (columnName != null && columnType != null && isAttributeUsedInTable(tableName,columnName)		// column type check is to be consistent with value list
				 && !whatHasBeenAdded.contains(columnName)) {								// do not add same attribute twice to same table
					b.append(",");
					b.append(columnName);
					whatHasBeenAdded.add(columnName);
				}
			}
		}
		// special case ... in MR or CT multi-frame ImageOrientation is nested deeply ... pretend it is at the top level iff shared
		if (ie == InformationEntity.INSTANCE) {
			String sopClassUID = Attribute.getSingleStringValueOrNull(list,TagFromName.SOPClassUID);
			if (sopClassUID != null && SOPClass.isImageStorage(sopClassUID)) {
				SequenceAttribute aPlaneOrientationSequence = (SequenceAttribute)(SequenceAttribute.getNamedAttributeFromWithinSequenceWithSingleItem(list,
					TagFromName.SharedFunctionalGroupsSequence,TagFromName.PlaneOrientationSequence));
				Attribute aImageOrientationPatient = SequenceAttribute.getNamedAttributeFromWithinSequenceWithSingleItem(
					aPlaneOrientationSequence,TagFromName.ImageOrientationPatient);
				if (aImageOrientationPatient != null && aImageOrientationPatient.getVM() == 6) {
					String columnName = dictionary.getNameFromTag(TagFromName.ImageOrientationPatient);
					String columnType = getSQLTypeFromDicomValueRepresentation(aImageOrientationPatient.getVR());		// use actual, not dictionary VR, in case was explicit
					if (columnName != null && columnType != null && isAttributeUsedInTable(tableName,columnName)		// column type check is to be consistent with value list
					 && !whatHasBeenAdded.contains(columnName)) {
						b.append(",");
						b.append(columnName);
						whatHasBeenAdded.add(columnName);
					}
				}
			}
		}
	}

	/**
	 * <p>Extend a SQL INSERT statement in the process of being constructed with the values of the attributes in the instance for the entity.</p>
	 *
	 * <p>Called when inserting a new record for an instance of the entity.</p>
	 *
	 * <p>The default implementation adds values for all attributes that are present in the database tables
	 * and present in the instance.</p>
	 *
	 * <p>At the INSTANCE level, InstanceNumber is always added.</p>
	 *
	 * <p>At the INSTANCE level, ImageOrientationPatient within PlaneOrientationSequence within SharedFunctionalGroupsSequence is always added, if present.</p>
	 *
	 * @param	b		the statement being constructed
	 * @param	list		the DICOM attributes of a composite object, containing the attributes describing this instance of the entity
	 * @param	ie		the {@link com.pixelmed.dicom.InformationEntity InformationEntity} for which an insert statement is being constructed
	 * @param	fileName	the local filename, which may be non-null for <code>INSTANCE</code> level insertions
	 * @exception	DicomException	thrown if there are problems extracting the DICOM attributes
	 */
	protected void extendInsertStatementStringWithAttributeValuesForSelectedInformationEntity(StringBuffer b,AttributeList list,InformationEntity ie,String fileName) throws DicomException {
		TreeSet whatHasBeenAdded = new TreeSet();
		if (ie == InformationEntity.INSTANCE) {
			b.append(",\'");
			b.append(fileName);
			b.append("\'");
			whatHasBeenAdded.add(localFileName);		// the name not the value
		}
		String tableName = ie.toString().toUpperCase();
		Iterator i = list.values().iterator();
		while (i.hasNext()) {
			Attribute a = (Attribute)i.next();
			AttributeTag tag = a.getTag();
			if (ie == dictionary.getInformationEntityFromTag(tag)) {
				String columnName = dictionary.getNameFromTag(tag);
				String columnType = getSQLTypeFromDicomValueRepresentation(a.getVR());					// use actual, not dictionary VR, in case was explicit
				if (columnName != null && columnType != null && isAttributeUsedInTable(tableName,columnName)
				 && !whatHasBeenAdded.contains(columnName)) {								// do not add same attribute twice to same table
					b.append(",");
                                        b.append(getQuotedEscapedSingleStringValueOrNull(a));
					whatHasBeenAdded.add(columnName);
				}
			}
		}
		if (ie == InformationEntity.INSTANCE) {
			AttributeTag tag = TagFromName.InstanceNumber;
			Attribute a = list.get(tag);
			if (a != null) {
				String columnName = dictionary.getNameFromTag(tag);
				String columnType = getSQLTypeFromDicomValueRepresentation(a.getVR());					// use actual, not dictionary VR, in case was explicit
				if (columnName != null && columnType != null && isAttributeUsedInTable(tableName,columnName)
				 && !whatHasBeenAdded.contains(columnName)) {								// do not add same attribute twice to same table
					b.append(",");
					b.append(getQuotedEscapedSingleStringValueOrNull(a));
					whatHasBeenAdded.add(columnName);
				}
			}
		}
		// special case ... in MR multi-frame ImageOrientation is nested deeply ... pretend it is at the top level iff shared
		if (ie == InformationEntity.INSTANCE) {
			String sopClassUID = Attribute.getSingleStringValueOrNull(list,TagFromName.SOPClassUID);
			if (sopClassUID != null && SOPClass.isImageStorage(sopClassUID)) {
				SequenceAttribute aPlaneOrientationSequence = (SequenceAttribute)(SequenceAttribute.getNamedAttributeFromWithinSequenceWithSingleItem(list,
					TagFromName.SharedFunctionalGroupsSequence,TagFromName.PlaneOrientationSequence));
				Attribute aImageOrientationPatient = SequenceAttribute.getNamedAttributeFromWithinSequenceWithSingleItem(
					aPlaneOrientationSequence,TagFromName.ImageOrientationPatient);
				if (aImageOrientationPatient != null && aImageOrientationPatient.getVM() == 6) {
					String columnName = dictionary.getNameFromTag(TagFromName.ImageOrientationPatient);
					String columnType = getSQLTypeFromDicomValueRepresentation(aImageOrientationPatient.getVR());		// use actual, not dictionary VR, in case was explicit
					if (columnName != null && columnType != null && isAttributeUsedInTable(tableName,columnName)
					 && !whatHasBeenAdded.contains(columnName)) {								// do not add same attribute twice to same table
					 	b.append(",");
						b.append(getQuotedEscapedSingleStringValueOrNull(aImageOrientationPatient));
						whatHasBeenAdded.add(columnName);
					}
				}
			}
		}
	}

	/**
	 * @param	ie
	 */
	protected String createPrimaryKeyForSelectedInformationEntity(InformationEntity ie) {
		//return new java.rmi.server.ObjID().toString();
		return Long.toString(new java.util.Date().getTime()) + Long.toString((long)(Math.random()*100000000));
	}
}

