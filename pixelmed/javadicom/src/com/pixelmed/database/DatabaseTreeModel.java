/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.database;

import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.*;
import java.io.File;

import com.pixelmed.dicom.*;

/**
 * <p>The {@link com.pixelmed.database.DatabaseTreeModel DatabaseTreeModel} class implements a
 * {@link javax.swing.tree.TreeModel TreeModel} to abstract the contents of a database as
 * a tree in order to provide support for a {@link com.pixelmed.database.DatabaseTreeBrowser DatabaseTreeBrowser}.</p>
 *
 * <p>For details of some of the methods implemented here see {@link javax.swing.tree.TreeModel javax.swing.tree.TreeModel}.</p>
 *
 * @author	dclunie
 */
public class DatabaseTreeModel implements TreeModel {

	/***/
	private static final String identString = "@(#) $Header$";

	// Our nodes are all instances of DatabaseTreeRecord ...

	/***/
	private DatabaseTreeRecord root;

	// Stuff to support listener vector

	/***/
	private Vector listeners;

	// Methods for TreeModel

	/**
	 * @param	node
	 * @param	index
	 */
	public Object getChild(Object node,int index) {
		return ((DatabaseTreeRecord)node).getChildAt(index);
	}

	/**
	 * @param	parent
	 * @param	child
	 */
	public int getIndexOfChild(Object parent, Object child) {
		return ((DatabaseTreeRecord)parent).getIndex((DatabaseTreeRecord)child);
	}

	/***/
	public Object getRoot() { return root; }

	/**
	 * @param	parent
	 */
	public int getChildCount(Object parent) {
		return ((DatabaseTreeRecord)parent).getChildCount();
	}

	/**
	 * @param	node
	 */
	public boolean isLeaf(Object node) {
		return ((DatabaseTreeRecord)node).getChildCount() == 0;
	}

	/**
	 * @param	path
	 * @param	newValue
	 */
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	/**
	 * @param	tml
	 */
	public void addTreeModelListener(TreeModelListener tml) {
		if (listeners == null) listeners = new Vector();
		listeners.addElement(tml);
	}

	/**
	 * @param	tml
	 */
	public void removeTreeModelListener(TreeModelListener tml) {
		if (listeners == null) listeners.removeElement(tml);
	}

	// Methods specific to DatabaseTreeModel

	/**
	 * @param	parent
	 * @param	ie
	 * @param	d
	 * @param	localParentReferenceKeyValue
	 * @exception	DicomException
	 */
	private DatabaseTreeRecord processInformationEntity(DatabaseTreeRecord parent,InformationEntity ie,DatabaseInformationModel d,String localParentReferenceKeyValue) throws DicomException {
//long startTime=System.currentTimeMillis();
		String descriptiveColumnName = d.getDescriptiveColumnName(ie);
		String otherColumnName = d.getOtherDescriptiveColumnName(ie);
		String otherOtherColumnName = d.getOtherOtherDescriptiveColumnName(ie);
		String localPrimaryKeyColumnNameAsUppercase = d.getLocalPrimaryKeyColumnName(ie).toUpperCase();
		String localFileNameColumnNameAsUppercase = d.localFileNameColumnName(ie).toUpperCase();
//System.err.println("processInformationEntity: ie="+ie+" descriptiveColumnName="+descriptiveColumnName+" otherColumnName="+otherColumnName+" =localPrimaryKeyColumnNameAsUppercase"+localPrimaryKeyColumnNameAsUppercase);
//System.err.println("processInformationEntity: localParentReferenceKeyValue "+localParentReferenceKeyValue);
		DatabaseTreeRecord first = null;
		ArrayList returnedRecords = d.findAllAttributeValuesForAllRecordsForThisInformationEntityWithSpecifiedParent(ie,localParentReferenceKeyValue);
//System.err.println("processInformationEntity() "+ie.toString()+" time after database find "+(System.currentTimeMillis()-startTime)+" milliseconds");
		if (ie != null && localParentReferenceKeyValue != null && returnedRecords == null || returnedRecords.size() == 0) {
			// recurse for next lower down type of child IE (e.g. we may be skipping over concatenation from series to instance)
			InformationEntity childIE = d.getChildTypeForParent(ie);
//System.err.println("processInformationEntity: skipping empty ie "+ie+" to get to "+childIE);
			if (childIE != null) processInformationEntity(parent,childIE,d,localParentReferenceKeyValue);
		}
//System.err.println("processInformationEntity() "+ie.toString()+" time after recursion and before processing returned records "+(System.currentTimeMillis()-startTime)+" milliseconds");
//long timeInGettingReturnedAttributes = 0;
//long timeInBuildingDescription = 0;
//long timeInBuildingNode = 0;
//long timeInAddingNode = 0;
//long timeInProcessingChild = 0;
		if (returnedRecords != null && returnedRecords.size() > 0) {
			for (int i=0; i<returnedRecords.size(); ++i) {
//long gettingReturnedRecordsStartTime = System.currentTimeMillis();
//System.err.println("processInformationEntity: record "+i);
				String value = null;
				Map returnedAttributes = (Map)(returnedRecords.get(i));
//long gettingReturnedRecordsEndTime = System.currentTimeMillis();
//timeInGettingReturnedAttributes+=(gettingReturnedRecordsEndTime-gettingReturnedRecordsStartTime);
				if (returnedAttributes != null) {
//System.err.println("processInformationEntity: returnedAttributes "+returnedAttributes);
//long buildingDescriptionStartTime = System.currentTimeMillis();
					// NB. keep the name lower case in the returned map, but be sure to check upper case in what came back from database
					String descriptiveColumnValue = descriptiveColumnName == null ? null : (String)(returnedAttributes.get(descriptiveColumnName.toUpperCase()));
					String otherColumnValue = otherColumnName == null ? null : (String)(returnedAttributes.get(otherColumnName.toUpperCase()));
					String otherOtherColumnValue = otherOtherColumnName == null ? null : (String)(returnedAttributes.get(otherOtherColumnName.toUpperCase()));
					String localPrimaryKeyColumnValue = localPrimaryKeyColumnNameAsUppercase == null ? null : (String)(returnedAttributes.get(localPrimaryKeyColumnNameAsUppercase));
					String localFileNameColumnValue = localFileNameColumnNameAsUppercase == null ? null : (String)(returnedAttributes.get(localFileNameColumnNameAsUppercase));
					StringBuffer buf = new StringBuffer();
					buf.append(d.getNametoDescribeThisInstanceOfInformationEntity(ie,returnedAttributes));
					String sopClassUID = (String)(returnedAttributes.get("SOPCLASSUID"));
//System.err.println("processInformationEntity: ie "+ie);
//System.err.println("processInformationEntity: sopClassUID "+sopClassUID);
					if (ie == InformationEntity.INSTANCE && sopClassUID != null && SOPClass.isImageStorage(sopClassUID)) {
						buf.append(" ");
						buf.append(DescriptionFactory.makeImageDescription(returnedAttributes));
					}
					else if (ie == InformationEntity.SERIES) {
						buf.append(" ");
						buf.append(DescriptionFactory.makeSeriesDescription(returnedAttributes));
					}
					else {
						if (descriptiveColumnValue != null) {
							buf.append(" ");
							buf.append(descriptiveColumnValue);
						}
						if (otherColumnValue != null) {
							buf.append(" ");
							buf.append(otherColumnValue);
						}
						if (otherOtherColumnValue != null) {
							buf.append(" ");
							buf.append(otherOtherColumnValue);
						}
					}
					value=buf.toString();
//long buildingDescriptionEndTime = System.currentTimeMillis();
//timeInBuildingDescription+=(buildingDescriptionEndTime-buildingDescriptionStartTime);
//System.err.println("processInformationEntity: value "+value);
//long buildingNodeStartTime = System.currentTimeMillis();
					DatabaseTreeRecord node = new DatabaseTreeRecord(parent,value,ie,localPrimaryKeyColumnValue,localFileNameColumnValue);
//long buildingNodeEndTime = System.currentTimeMillis();
//timeInBuildingNode+=(buildingNodeEndTime-buildingNodeStartTime);
//long addingNodeStartTime = System.currentTimeMillis();
					if (first == null) {
						first=node;
						if (parent != null) parent.addChild(node);
					}
					else {
						node.addSibling(node);
					}
//long addingNodeEndTime = System.currentTimeMillis();
//timeInAddingNode+=(addingNodeEndTime-addingNodeStartTime);
//long processingChildStartTime = System.currentTimeMillis();
					InformationEntity childIE = d.getChildTypeForParent(ie);
					if (childIE != null && localPrimaryKeyColumnValue != null) processInformationEntity(node,childIE,d,localPrimaryKeyColumnValue);
//long processingChildEndTime = System.currentTimeMillis();
//timeInProcessingChild+=(processingChildEndTime-processingChildStartTime);
				}
			}
		}
//System.err.println("processInformationEntity() "+ie.toString()+" time at end "+(System.currentTimeMillis()-startTime)+" milliseconds");
//System.err.println("processInformationEntity() "+ie.toString()+" timeInGettingReturnedAttributes "+timeInGettingReturnedAttributes+" milliseconds");
//System.err.println("processInformationEntity() "+ie.toString()+" timeInBuildingDescription "+timeInBuildingDescription+" milliseconds");
//System.err.println("processInformationEntity() "+ie.toString()+" timeInBuildingNode "+timeInBuildingNode+" milliseconds");
//System.err.println("processInformationEntity() "+ie.toString()+" timeInAddingNode "+timeInAddingNode+" milliseconds");
//System.err.println("processInformationEntity() "+ie.toString()+" timeInProcessingChild "+timeInProcessingChild+" milliseconds");
		return first;
	}

	/**
	 * <p>Construct a tree model of the supplied database.</p>
	 *
	 * @param	d		the database information model to build the tree from
	 * @exception	DicomException	thrown if there are problems accessing the database
	 */
	public DatabaseTreeModel(DatabaseInformationModel d) throws DicomException {
		if (d != null) {
//long startTime=System.currentTimeMillis();
			root = new DatabaseTreeRecord(null,"Local database",null,null,null);			// we create our own (empty) root on top
			processInformationEntity(root,d.getRootInformationEntity(),d,null);
//System.err.println("DatabaseTreeModel() construct time "+(System.currentTimeMillis()-startTime)+" milliseconds");
		}
	}

	/**
	 * @param	node
	 */
	private String walkTree(DatabaseTreeRecord node) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(node.toString());
		buffer.append("\n");

		int n = getChildCount(node);
		for (int i=0; i<n; ++i) buffer.append(walkTree((DatabaseTreeRecord)getChild(node,i)));

		return buffer.toString();
	}

	/**
	 * <p>Dump the entire tree to a string.</p>
	 *
	 * <p>Performs a top-down traversal.</p>
	 *
	 * @see DatabaseTreeRecord#toString()
	 *
	 * @return	a multiline string with one line per node in the tree
	 */
	public String toString() {
		return walkTree(root);
	}
}





