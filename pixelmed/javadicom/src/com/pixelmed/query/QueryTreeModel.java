/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.query;

import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.*;
import java.io.File;

import com.pixelmed.dicom.*;

/**
 * <p>The {@link com.pixelmed.query.QueryTreeModel QueryTreeModel} class implements a
 * {@link javax.swing.tree.TreeModel TreeModel} to abstract the contents of a query response as
 * a tree in order to provide support for a {@link com.pixelmed.query.QueryTreeBrowser QueryTreeBrowser}.</p>
 *
 * @see javax.swing.tree.TreeModel
 *
 * @author	dclunie
 */
public class QueryTreeModel implements TreeModel {

	/***/
	private static final String identString = "@(#) $Header$";

	// Our nodes are all instances of QueryTreeRecord ...

	/***/
	private QueryTreeRecord root;

	// Stuff to support listener vector

	/***/
	private Vector listeners;

	// Methods for TreeModel

	/**
	 * @param	node
	 * @param	index
	 */
	public Object getChild(Object node,int index) {
		return ((QueryTreeRecord)node).getChildAt(index);
	}

	/**
	 * @param	parent
	 * @param	child
	 */
	public int getIndexOfChild(Object parent, Object child) {
		return ((QueryTreeRecord)parent).getIndex((QueryTreeRecord)child);
	}

	/***/
	public Object getRoot() { return root; }

	/**
	 * @param	parent
	 */
	public int getChildCount(Object parent) {
		return ((QueryTreeRecord)parent).getChildCount();
	}

	/**
	 * @param	node
	 */
	public boolean isLeaf(Object node) {
		return ((QueryTreeRecord)node).getChildCount() == 0;
	}

	/**
	 * @param	path
	 * @param	newValue
	 */
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	/**
	 * @param	tml
	 */
	public void addTreeModelListener(TreeModelListener tml) {
		if (listeners == null) listeners = new Vector();
		listeners.addElement(tml);
	}

	/**
	 * @param	tml
	 */
	public void removeTreeModelListener(TreeModelListener tml) {
		if (listeners == null) listeners.removeElement(tml);
	}

	// Methods specific to QueryTreeModel

	/**
	 * <p>Construct an empty tree model with an empty root node on top.</p>
	 *
	 * <p>Note that in practice the contents will be added later when a query is actually performed, usually by
	 * {@link com.pixelmed.query.QueryInformationModel#performHierarchicalQuery(com.pixelmed.dicom.AttributeList) performHierarchicalQuery()}.</p>
	 *
	 * @param	q		the query information model to build the tree from
	 * @exception	DicomException	thrown if there are problems building the tree
	 */
	public QueryTreeModel(QueryInformationModel q) throws DicomException {
		if (q != null) {
			root = new QueryTreeRecord(null,"Remote database",null,null,null,null);			// we create our own (empty) root on top
		}
	}

	/**
	 * @param	node
	 */
	private String walkTree(QueryTreeRecord node) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(node.toString());
		buffer.append("\n");

		int n = getChildCount(node);
		for (int i=0; i<n; ++i) buffer.append(walkTree((QueryTreeRecord)getChild(node,i)));

		return buffer.toString();
	}

	/**
	 * <p>Dump the entire tree to a string.</p>
	 *
	 * <p>Performs a top-down traversal.</p>
	 *
	 * @see QueryTreeRecord#toString()
	 *
	 * @return	a multiline string with one line per node in the tree
	 */
	public String toString() {
		return walkTree(root);
	}
}





