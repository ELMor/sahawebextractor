/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.query;

import com.pixelmed.dicom.*;
import com.pixelmed.network.*;

import java.io.*;
import java.util.*;

/**
 * <p>The {@link com.pixelmed.query.QueryInformationModel QueryInformationModel} class is an abstract class that contains the core
 * functionality for performing DICOM query and retrieval over the network.</p>
 *
 * <p>It hides the underlying DICOM network implementation.</p>
 *
 * <p>Concrete sub-classes implement the behavior for specific query models, such as
 * {@link com.pixelmed.query.StudyRootQueryInformationModel StudyRootQueryInformationModel},
 * the description of which contains an exampleof building an identifier and performing
 * a query.</p>
 *
 * <p>The majority of methods are protected and are for the benefit of those implementing their
 * own query models as concrete sub-classes. The public methods of primary interest to application
 * builders are:</p>
 * <ul>
 * <li> {@link #QueryInformationModel(String,String,String,String,int) QueryInformationModel()}
 * <li> {@link #performHierarchicalQuery(AttributeList) performHierarchicalQuery()}
 * <li> {@link #performHierarchicalMove(AttributeList,String) performHierarchicalMove()}
 * </ul>
 *
 * @see com.pixelmed.query.StudyRootQueryInformationModel
 *
 * @author	dclunie
 */
abstract public class QueryInformationModel {

	/***/
	private static final String identString = "@(#) $Header$";

	/***/
	private String hostname;
	/***/
	private String port;
	/***/
	private String calledAETitle;
	/***/
	private String callingAETitle;
	/***/
	protected int debugLevel;
	
	/***/
	public final String getCalledAETitle() { return calledAETitle; }

	/***/
	protected abstract InformationEntity getRoot();
	/**
	 * @param	ie
	 */
	protected abstract InformationEntity getChildTypeForParent(InformationEntity ie);
	/**
	 * @param	ie
	 */
	protected abstract HashSet getAllInformationEntitiesToIncludeAtThisQueryLevel(InformationEntity ie);
	/***/
	protected abstract String getFindSOPClassUID();
	/***/
	protected abstract String getMoveSOPClassUID();
	/**
	 * @param	ie
	 * @param	responseIdentifier
	 */
	protected abstract String getStringValueForTreeFromResponseIdentifier(InformationEntity ie,AttributeList responseIdentifier);

	/**
	 * @param	ie
	 */
	protected String getQueryLevelName(InformationEntity ie) {
		if      (ie == InformationEntity.PATIENT)       return "PATIENT";
		else if (ie == InformationEntity.STUDY)         return "STUDY";
		else if (ie == InformationEntity.SERIES)        return "SERIES";
		else if (ie == InformationEntity.INSTANCE)      return "IMAGE";
		else return null;
	}
	
	/**
	 * @param	ie
	 */
	protected AttributeTag getUniqueKeyForInformationEntity(InformationEntity ie) {
		if      (ie == InformationEntity.PATIENT)       return TagFromName.PatientID;
		else if (ie == InformationEntity.STUDY)         return TagFromName.StudyInstanceUID;
		else if (ie == InformationEntity.SERIES)        return TagFromName.SeriesInstanceUID;
		else if (ie == InformationEntity.INSTANCE)      return TagFromName.SOPInstanceUID;
		else return null;
	}
	
	/**
	 * @param	oldList
	 * @param	parentUniqueKeys
	 * @param	queryLevel
	 * @exception	DicomException
	 */
	private AttributeList makeIdentifierFromAttributesAtThisQueryLevel(AttributeList oldList,AttributeList parentUniqueKeys,InformationEntity queryLevel) throws DicomException {
if (debugLevel > 1) System.err.println("makeIdentifierFromAttributesAtThisQueryLevel: queryLevel="+queryLevel);
		HashSet allInformationEntitiesToIncludeAtThisQueryLevel = getAllInformationEntitiesToIncludeAtThisQueryLevel(queryLevel);
//System.err.println("makeIdentifierFromAttributesAtThisQueryLevel: allInformationEntitiesToIncludeAtThisQueryLevel="+allInformationEntitiesToIncludeAtThisQueryLevel);
		DicomDictionary dictionary = oldList.getDictionary();
		AttributeList newList = new AttributeList();
		Iterator i = oldList.values().iterator();
		while (i.hasNext()) {
			Attribute a = (Attribute)i.next();
			AttributeTag tag = a.getTag();
			InformationEntity ie = dictionary.getInformationEntityFromTag(tag);
//System.err.println("makeIdentifierFromAttributesAtThisQueryLevel: checking "+a);
			if (tag.equals(TagFromName.SpecificCharacterSet)
			 || (queryLevel == InformationEntity.STUDY && tag.equals(TagFromName.ModalitiesInStudy))
			 || (ie != null && allInformationEntitiesToIncludeAtThisQueryLevel.contains(ie))
			) {
//System.err.println("makeIdentifierFromAttributesAtThisQueryLevel: adding "+a);
				newList.put(tag,a);
			}
		}
		{ AttributeTag t = TagFromName.QueryRetrieveLevel; Attribute a = new CodeStringAttribute(t); a.addValue(getQueryLevelName(queryLevel)); newList.put(t,a); }
		if (parentUniqueKeys!= null) newList.putAll(parentUniqueKeys);
		AttributeTag uniqueKeyTagFromThisLevel = getUniqueKeyForInformationEntity(queryLevel);
		if (!newList.containsKey(uniqueKeyTagFromThisLevel)) {
			byte[] vr = dictionary.getValueRepresentationFromTag(uniqueKeyTagFromThisLevel);
			if (vr != null) {
				if (ValueRepresentation.isShortStringVR(vr)) {
					newList.put(uniqueKeyTagFromThisLevel,new ShortStringAttribute(uniqueKeyTagFromThisLevel,null));
				}
				else if (ValueRepresentation.isUniqueIdentifierVR(vr)) {
					newList.put(uniqueKeyTagFromThisLevel,new UniqueIdentifierAttribute(uniqueKeyTagFromThisLevel));
				}
				else {
					throw new DicomException("Internal error: cannot get suitable VR for unique key in query "+uniqueKeyTagFromThisLevel);
				}
			}
		}
if (debugLevel > 1) System.err.println("makeIdentifierFromAttributesAtThisQueryLevel: identifier="+newList);
		return newList;
	}
	
	/***/
	private class OurResponseIdentifierHandler extends IdentifierHandler {
		/***/
		private LinkedList responses;
		/***/
		private LinkedList nodes;
		/***/
		private QueryTreeRecord parentNode;
		/***/
		private InformationEntity ie;
		/***/
		private AttributeList parentUniqueKeys;
		/***/
		private AttributeTag uniqueKeyTagFromThisLevel;
		
		/**
		 * @param	parentNode
		 * @param	ie
		 * @param	parentUniqueKeys
		 * @param	uniqueKeyTagFromThisLevel
		 */
		public OurResponseIdentifierHandler(QueryTreeRecord parentNode,InformationEntity ie,AttributeList parentUniqueKeys,AttributeTag uniqueKeyTagFromThisLevel) {
			responses=new LinkedList();
			nodes=new LinkedList();
			this.parentNode=parentNode;
			this.ie=ie;
			this.parentUniqueKeys=parentUniqueKeys;
			this.uniqueKeyTagFromThisLevel=uniqueKeyTagFromThisLevel;
		}
		
		/**
		 * @param	responseIdentifier
		 */
		public void doSomethingWithIdentifier(AttributeList responseIdentifier) {
if (debugLevel > 0) System.err.println("QueryInformationModel.OurIdentifierHandler.doSomethingWithIdentifier():");
if (debugLevel > 0) System.err.print(responseIdentifier);
			responses.add(responseIdentifier);
			String value = getStringValueForTreeFromResponseIdentifier(ie,responseIdentifier);
			QueryTreeRecord node = new QueryTreeRecord(parentNode,value,ie,parentUniqueKeys,responseIdentifier.get(uniqueKeyTagFromThisLevel),responseIdentifier);
			parentNode.addChild(node);
			nodes.add(node);
		}

		/***/
		public LinkedList getResponses() { return responses; }
		/***/
		public LinkedList getTreeNodesCorrespondingToResponses() { return nodes; }
	}
	
	/**
	 * <p>Perform a hierarchical query and return the response as a tree.</p>
	 *
	 * <p>Performs a query recursively from the requested level of the information model
	 * down to the lowest level of the query model, using the matching keys present in the
	 * supplied identifier (filter) (if any), requesting the return keys listed in the
	 * request identifier.</p>
	 *
	 * <p>It starts out at the highest level of the model, and for each response returned
	 * at that level, uses each unique key returned at that level of the response to perform
	 * another query at the next level down, and so on, recursively.</p>
	 *
	 * @param	filter			the query request identifier as a list of DICOM attributes
	 * @param	parentUniqueKeys	the unique keys of the parents of this level
	 * @param	queryLevel		the level of the query
	 * @param	parentNode		the tree to add the response results to
	 * @exception	IOException		thrown if there is an generic IO problem
	 * @exception	DicomException		thrown if there is a problem performing or parsing the query
	 * @exception	DicomNetworkException	thrown if there is a problem with the DICOM network protocol
	 */
	private void performHierarchicalQuery(AttributeList filter,AttributeList parentUniqueKeys,InformationEntity queryLevel,QueryTreeRecord parentNode) throws IOException, DicomException, DicomNetworkException {
if (debugLevel > 0) System.err.println("QueryInformationModel.performHierarchicalQuery(): queryLevel="+queryLevel);
if (debugLevel > 0) System.err.println("QueryInformationModel.performHierarchicalQuery(): parentUniqueKeys="+parentUniqueKeys);
		AttributeList requestIdentifier = makeIdentifierFromAttributesAtThisQueryLevel(filter,parentUniqueKeys,queryLevel);
if (debugLevel > 1) System.err.println("QueryInformationModel.performHierarchicalQuery(): requestIdentifier="+requestIdentifier);
		AttributeTag uniqueKeyTagFromThisLevel = getUniqueKeyForInformationEntity(queryLevel);
		OurResponseIdentifierHandler ourResponseIdentifierHandler = new OurResponseIdentifierHandler(parentNode,queryLevel,parentUniqueKeys,uniqueKeyTagFromThisLevel);
		new FindSOPClassSCU(hostname,port,calledAETitle,callingAETitle,getFindSOPClassUID(),requestIdentifier,ourResponseIdentifierHandler,debugLevel);
if (debugLevel > 1) System.err.println("QueryInformationModel.performHierarchicalQuery(): uniqueKeyTagFromThisLevel="+uniqueKeyTagFromThisLevel);
		InformationEntity childQueryLevel = getChildTypeForParent(queryLevel);
		if (childQueryLevel != null) {
			Iterator iResponses = ourResponseIdentifierHandler.getResponses().iterator();
			Iterator iNodes = ourResponseIdentifierHandler.getTreeNodesCorrespondingToResponses().iterator();
			while (iResponses.hasNext()) {
				AttributeList responseIdentifier = (AttributeList)iResponses.next();
				QueryTreeRecord responseNode = (QueryTreeRecord)iNodes.next();
				Attribute uniqueKey = responseIdentifier.get(uniqueKeyTagFromThisLevel);
if (debugLevel > 0) System.err.println("QueryInformationModel.performHierarchicalQuery(): iterating on response for child queries: uniqueKey="+uniqueKey);
				AttributeList uniqueKeysForChildQuery = new AttributeList();
				if (parentUniqueKeys != null) uniqueKeysForChildQuery.putAll(parentUniqueKeys);
				uniqueKeysForChildQuery.put(uniqueKeyTagFromThisLevel,uniqueKey);
if (debugLevel > 0) System.err.println("QueryInformationModel.performHierarchicalQuery(): iterating on response for child queries: uniqueKeysForChildQuery="+uniqueKeysForChildQuery);
				performHierarchicalQuery(filter,uniqueKeysForChildQuery,childQueryLevel,responseNode);
			}
		}
	}
	
	/***/
	public String toString() {
		return	"host="+hostname
			+" port="+port
			+" calledAETitle="+calledAETitle
			+" callingAETitle="+callingAETitle;
	}
	
	/**
	 * <p>Perform a hierarchical query and return the response as a tree.</p>
	 *
	 * <p>Performs a query recursively from the requested level of the information model
	 * down to the lowest level of the query model, using the matching keys present in the
	 * supplied identifier (filter) (if any), requesting the return keys listed in the
	 * request identifier.</p>
	 *
	 * <p>It starts out at the highest level of the model, and for each response returned
	 * at that level, uses each unique key returned at that level of the response to perform
	 * another query at the next level down, and so on, recursively.</p>
	 *
	 * @param	filter			the query request identifier as a list of DICOM attributes
	 * @return				the results of query as a tree suitable for browing
	 * @exception	IOException		thrown if there is an generic IO problem
	 * @exception	DicomException		thrown if there is a problem performing or parsing the query
	 * @exception	DicomNetworkException	thrown if there is a problem with the DICOM network protocol
	 */
	public QueryTreeModel performHierarchicalQuery(AttributeList filter) throws IOException, DicomException, DicomNetworkException {
		QueryTreeModel tree = new QueryTreeModel(this);
		performHierarchicalQuery(filter,null,getRoot(),(QueryTreeRecord)(tree.getRoot()));
		return tree;
	}
	
	/**
	 * <p>Retrieve DICOM object(s) from the specified location.</p>
	 *
	 * <p>Assumes that we have a storage SCP listening as the <code>callingAETitle</code>
	 * specified in the constructor in this class instance.</p>
	 *
	 * <p>Note that the <code>retrieveAE</code> argument may differ from the
	 * <code>calledAETitle</code> used in the constructor of this class instance.</p>
	 *
	 * @param	identifier		the move request identifier as a list of DICOM attributes
	 * @param	retrieveAE		the AE title of where to move the object(s) from
	 * @exception	IOException		thrown if there is an generic IO problem
	 * @exception	DicomException		thrown if there is a problem performing or parsing the query
	 * @exception	DicomNetworkException	thrown if there is a problem with the DICOM network protocol
	 */
	public void performHierarchicalMove(AttributeList identifier,String retrieveAE) throws IOException, DicomException, DicomNetworkException {
		new MoveSOPClassSCU(hostname,port,retrieveAE,callingAETitle,callingAETitle,getMoveSOPClassUID(),identifier,debugLevel);
	}
	
	/**
	 * <p>Construct a query information model.</p>
	 *
	 * <p>Does not actually open an association or perform a query or retrieval; for that see:</p>
	 * <ul>
	 * <li> {@link #performHierarchicalQuery(AttributeList) performHierarchicalQuery()}
	 * <li> {@link #performHierarchicalMove(AttributeList,String) performHierarchicalMove()}
	 * </ul>
	 *
	 * @param	hostname		their hostname or IP address
	 * @param	port			their port number
	 * @param	calledAETitle		their AE title
	 * @param	callingAETitle		our AE title (both when we query or retrieve and where we are listening as a storage SCP)
	 * @param	debugLevel		0 is no debugging (silent), > 0 more verbose levels of debugging
	 */
	public QueryInformationModel(String hostname,String port,String calledAETitle,String callingAETitle,int debugLevel) {
		this.hostname=hostname;
		this.port=port;
		this.calledAETitle=calledAETitle;
		this.callingAETitle=callingAETitle;
		this.debugLevel=debugLevel;
	}

}
