<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--NewPage-->
<HTML>
<HEAD>
<!-- Generated by javadoc (build 1.4.2_05) on Tue Jan 04 18:32:17 EST 2005 -->
<TITLE>
DicomDictionaryForPatientStudySeriesConcatenationInstanceModel
</TITLE>

<META NAME="keywords" CONTENT="com.pixelmed.database.DicomDictionaryForPatientStudySeriesConcatenationInstanceModel class">

<LINK REL ="stylesheet" TYPE="text/css" HREF="../../../stylesheet.css" TITLE="Style">

<SCRIPT type="text/javascript">
function windowTitle()
{
    parent.document.title="DicomDictionaryForPatientStudySeriesConcatenationInstanceModel";
}
</SCRIPT>

</HEAD>

<BODY BGCOLOR="white" onload="windowTitle();">


<!-- ========= START OF TOP NAVBAR ======= -->
<A NAME="navbar_top"><!-- --></A>
<A HREF="#skip-navbar_top" title="Skip navigation links"></A>
<TABLE BORDER="0" WIDTH="100%" CELLPADDING="1" CELLSPACING="0" SUMMARY="">
<TR>
<TD COLSPAN=3 BGCOLOR="#EEEEFF" CLASS="NavBarCell1">
<A NAME="navbar_top_firstrow"><!-- --></A>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="3" SUMMARY="">
  <TR ALIGN="center" VALIGN="top">
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../overview-summary.html"><FONT CLASS="NavBarFont1"><B>Overview</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="package-summary.html"><FONT CLASS="NavBarFont1"><B>Package</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#FFFFFF" CLASS="NavBarCell1Rev"> &nbsp;<FONT CLASS="NavBarFont1Rev"><B>Class</B></FONT>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="package-tree.html"><FONT CLASS="NavBarFont1"><B>Tree</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../deprecated-list.html"><FONT CLASS="NavBarFont1"><B>Deprecated</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../index-all.html"><FONT CLASS="NavBarFont1"><B>Index</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../help-doc.html"><FONT CLASS="NavBarFont1"><B>Help</B></FONT></A>&nbsp;</TD>
  </TR>
</TABLE>
</TD>
<TD ALIGN="right" VALIGN="top" ROWSPAN=3><EM>
</EM>
</TD>
</TR>

<TR>
<TD BGCOLOR="white" CLASS="NavBarCell2"><FONT SIZE="-2">
&nbsp;<A HREF="../../../com/pixelmed/database/DicomDatabaseInformationModel.html" title="class in com.pixelmed.database"><B>PREV CLASS</B></A>&nbsp;
&nbsp;<A HREF="../../../com/pixelmed/database/DicomDictionaryForStudySeriesInstanceModel.html" title="class in com.pixelmed.database"><B>NEXT CLASS</B></A></FONT></TD>
<TD BGCOLOR="white" CLASS="NavBarCell2"><FONT SIZE="-2">
  <A HREF="../../../index.html" target="_top"><B>FRAMES</B></A>  &nbsp;
&nbsp;<A HREF="DicomDictionaryForPatientStudySeriesConcatenationInstanceModel.html" target="_top"><B>NO FRAMES</B></A>  &nbsp;
&nbsp;<SCRIPT type="text/javascript">
  <!--
  if(window==top) {
    document.writeln('<A HREF="../../../allclasses-noframe.html"><B>All Classes</B></A>');
  }
  //-->
</SCRIPT>
<NOSCRIPT>
  <A HREF="../../../allclasses-noframe.html"><B>All Classes</B></A>
</NOSCRIPT>

</FONT></TD>
</TR>
<TR>
<TD VALIGN="top" CLASS="NavBarCell3"><FONT SIZE="-2">
  SUMMARY:&nbsp;NESTED&nbsp;|&nbsp;<A HREF="#fields_inherited_from_class_com.pixelmed.dicom.DicomDictionaryBase">FIELD</A>&nbsp;|&nbsp;<A HREF="#constructor_summary">CONSTR</A>&nbsp;|&nbsp;<A HREF="#method_summary">METHOD</A></FONT></TD>
<TD VALIGN="top" CLASS="NavBarCell3"><FONT SIZE="-2">
DETAIL:&nbsp;FIELD&nbsp;|&nbsp;<A HREF="#constructor_detail">CONSTR</A>&nbsp;|&nbsp;<A HREF="#method_detail">METHOD</A></FONT></TD>
</TR>
</TABLE>
<A NAME="skip-navbar_top"></A>
<!-- ========= END OF TOP NAVBAR ========= -->

<HR>
<!-- ======== START OF CLASS DATA ======== -->
<H2>
<FONT SIZE="-1">
com.pixelmed.database</FONT>
<BR>
Class DicomDictionaryForPatientStudySeriesConcatenationInstanceModel</H2>
<PRE>
<A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html" title="class or interface in java.lang">java.lang.Object</A>
  <IMG SRC="../../../resources/inherit.gif" ALT="extended by"><A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html" title="class in com.pixelmed.dicom">com.pixelmed.dicom.DicomDictionaryBase</A>
      <IMG SRC="../../../resources/inherit.gif" ALT="extended by"><A HREF="../../../com/pixelmed/dicom/DicomDictionary.html" title="class in com.pixelmed.dicom">com.pixelmed.dicom.DicomDictionary</A>
          <IMG SRC="../../../resources/inherit.gif" ALT="extended by"><B>com.pixelmed.database.DicomDictionaryForPatientStudySeriesConcatenationInstanceModel</B>
</PRE>
<HR>
<DL>
<DT>public class <B>DicomDictionaryForPatientStudySeriesConcatenationInstanceModel</B><DT>extends <A HREF="../../../com/pixelmed/dicom/DicomDictionary.html" title="class in com.pixelmed.dicom">DicomDictionary</A></DL>

<P>
<p>The <A HREF="../../../com/pixelmed/database/DicomDictionaryForPatientStudySeriesConcatenationInstanceModel.html" title="class in com.pixelmed.database"><CODE>DicomDictionaryForPatientStudySeriesConcatenationInstanceModel</CODE></A> class
 supports a simple DICOM Patient/Study/Series/Concatenation/Instance model.</p>

 <p>Attributes of the DICOM Procedure Step entity are included at the Series level.</p>

 <p>The subset of the DICOM standard dictionary elements that is included in this dictionary
 (and hence in the database underlying any <A HREF="../../../com/pixelmed/database/DatabaseInformationModel.html" title="class in com.pixelmed.database"><CODE>DatabaseInformationModel</CODE></A>
 that uses this dictionary) consists of the following:</p>

 <ul>
 <li>SpecificCharacterSet</li>
 <li>ImageType</li>
 <li>SOPClassUID</li>
 <li>SOPInstanceUID</li>
 <li>StudyDate</li>
 <li>SeriesDate</li>
 <li>ContentDate</li>
 <li>AcquisitionDateTime</li>
 <li>StudyTime</li>
 <li>SeriesTime</li>
 <li>AccessionNumber</li>
 <li>Modality</li>
 <li>ModalitiesInStudy</li>
 <li>ConversionType</li>
 <li>PresentationIntentType</li>
 <li>Manufacturer</li>
 <li>InstitutionName</li>
 <li>ReferringPhysicianName</li>
 <li>StudyDescription</li>
 <li>SeriesDescription</li>
 <li>InstitutionalDepartmentName</li>
 <li>PhysicianOfRecord</li>
 <li>PerformingPhysicianName</li>
 <li>PhysicianReadingStudy</li>
 <li>OperatorName</li>
 <li>AdmittingDiagnosesDescription</li>
 <li>DerivationDescription</li>
 <li>PixelPresentation</li>
 <li>VolumetricProperties</li>
 <li>VolumeBasedCalculationTechnique</li>
 <li>ComplexImageComponent</li>
 <li>AcquisitionContrast</li>
 <li>PatientName</li>
 <li>PatientID</li>
 <li>PatientBirthDate</li>
 <li>PatientSex</li>
 <li>PatientAge</li>
 <li>PatientComments</li>
 <li>ContrastBolusAgent</li>
 <li>BodyPartExamined</li>
 <li>ProtocolName</li>
 <li>PulseSequenceName</li>
 <li>StudyInstanceUID</li>
 <li>SeriesInstanceUID</li>
 <li>StudyID</li>
 <li>SeriesNumber</li>
 <li>AcquisitionNumber</li>
 <li>InstanceNumber</li>
 <li>ImagePositionPatient</li>
 <li>ImageOrientationPatient</li>
 <li>Laterality</li>
 <li>ImageLaterality</li>
 <li>ImageComments</li>
 <li>ConcatenationUID</li>
 <li>InConcatenationNumber</li>
 <li>InConcatenationTotalNumber</li>
 <li>NumberOfFrames</li>
 <li>QualityControlImage</li>
 <li>BurnedInAnnotation</li>
 <li>LossyImageCompression</li>
 <li>LossyImageCompressionRatio</li>
 <li>PhotometricInterpretation</li>
 <li>BitsStored</li>
 <li>BitsAllocated</li>
 <li>PixelRepresentation</li>
 </ul>
<P>

<P>
<DL>
<DT><B>See Also:</B><DD><A HREF="../../../com/pixelmed/database/PatientStudySeriesConcatenationInstanceModel.html" title="class in com.pixelmed.database"><CODE>PatientStudySeriesConcatenationInstanceModel</CODE></A>, 
<A HREF="../../../com/pixelmed/dicom/InformationEntity.html" title="class in com.pixelmed.dicom"><CODE>InformationEntity</CODE></A></DL>
<HR>

<P>
<!-- ======== NESTED CLASS SUMMARY ======== -->


<!-- =========== FIELD SUMMARY =========== -->

<A NAME="field_summary"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TD COLSPAN=2><FONT SIZE="+2">
<B>Field Summary</B></FONT></TD>
</TR>
</TABLE>
&nbsp;<A NAME="fields_inherited_from_class_com.pixelmed.dicom.DicomDictionaryBase"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TD><B>Fields inherited from class com.pixelmed.dicom.<A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html" title="class in com.pixelmed.dicom">DicomDictionaryBase</A></B></TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE><A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#informationEntityByTag">informationEntityByTag</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#nameByTag">nameByTag</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#tagByName">tagByName</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#tagList">tagList</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#valueRepresentationsByTag">valueRepresentationsByTag</A></CODE></TD>
</TR>
</TABLE>
&nbsp;
<!-- ======== CONSTRUCTOR SUMMARY ======== -->

<A NAME="constructor_summary"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TD COLSPAN=2><FONT SIZE="+2">
<B>Constructor Summary</B></FONT></TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE><B><A HREF="../../../com/pixelmed/database/DicomDictionaryForPatientStudySeriesConcatenationInstanceModel.html#DicomDictionaryForPatientStudySeriesConcatenationInstanceModel()">DicomDictionaryForPatientStudySeriesConcatenationInstanceModel</A></B>()</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
</TR>
</TABLE>
&nbsp;
<!-- ========== METHOD SUMMARY =========== -->

<A NAME="method_summary"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TD COLSPAN=2><FONT SIZE="+2">
<B>Method Summary</B></FONT></TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD ALIGN="right" VALIGN="top" WIDTH="1%"><FONT SIZE="-1">
<CODE>protected &nbsp;void</CODE></FONT></TD>
<TD><CODE><B><A HREF="../../../com/pixelmed/database/DicomDictionaryForPatientStudySeriesConcatenationInstanceModel.html#createTagList()">createTagList</A></B>()</CODE>

<BR>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Concrete sub-classes implement this method to create a list of all tags in the dictionary.</TD>
</TR>
</TABLE>
&nbsp;<A NAME="methods_inherited_from_class_com.pixelmed.dicom.DicomDictionary"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TD><B>Methods inherited from class com.pixelmed.dicom.<A HREF="../../../com/pixelmed/dicom/DicomDictionary.html" title="class in com.pixelmed.dicom">DicomDictionary</A></B></TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE><A HREF="../../../com/pixelmed/dicom/DicomDictionary.html#createInformationEntityByTag()">createInformationEntityByTag</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionary.html#createNameByTag()">createNameByTag</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionary.html#createTagByName()">createTagByName</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionary.html#createValueRepresentationsByTag()">createValueRepresentationsByTag</A></CODE></TD>
</TR>
</TABLE>
&nbsp;<A NAME="methods_inherited_from_class_com.pixelmed.dicom.DicomDictionaryBase"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TD><B>Methods inherited from class com.pixelmed.dicom.<A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html" title="class in com.pixelmed.dicom">DicomDictionaryBase</A></B></TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE><A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#getInformationEntityFromTag(com.pixelmed.dicom.AttributeTag)">getInformationEntityFromTag</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#getNameFromTag(com.pixelmed.dicom.AttributeTag)">getNameFromTag</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#getTagFromName(java.lang.String)">getTagFromName</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#getTagIterator()">getTagIterator</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#getValueRepresentationFromTag(com.pixelmed.dicom.AttributeTag)">getValueRepresentationFromTag</A>, <A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html#main(java.lang.String[])">main</A></CODE></TD>
</TR>
</TABLE>
&nbsp;<A NAME="methods_inherited_from_class_java.lang.Object"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#EEEEFF" CLASS="TableSubHeadingColor">
<TD><B>Methods inherited from class java.lang.<A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html" title="class or interface in java.lang">Object</A></B></TD>
</TR>
<TR BGCOLOR="white" CLASS="TableRowColor">
<TD><CODE><A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#clone()" title="class or interface in java.lang">clone</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#equals(java.lang.Object)" title="class or interface in java.lang">equals</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#finalize()" title="class or interface in java.lang">finalize</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#getClass()" title="class or interface in java.lang">getClass</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#hashCode()" title="class or interface in java.lang">hashCode</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#notify()" title="class or interface in java.lang">notify</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#notifyAll()" title="class or interface in java.lang">notifyAll</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#toString()" title="class or interface in java.lang">toString</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#wait()" title="class or interface in java.lang">wait</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#wait(long)" title="class or interface in java.lang">wait</A>, <A HREF="http://java.sun.com/j2se/1.4.1/docs/api/java/lang/Object.html#wait(long, int)" title="class or interface in java.lang">wait</A></CODE></TD>
</TR>
</TABLE>
&nbsp;
<P>

<!-- ============ FIELD DETAIL =========== -->


<!-- ========= CONSTRUCTOR DETAIL ======== -->

<A NAME="constructor_detail"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TD COLSPAN=1><FONT SIZE="+2">
<B>Constructor Detail</B></FONT></TD>
</TR>
</TABLE>

<A NAME="DicomDictionaryForPatientStudySeriesConcatenationInstanceModel()"><!-- --></A><H3>
DicomDictionaryForPatientStudySeriesConcatenationInstanceModel</H3>
<PRE>
public <B>DicomDictionaryForPatientStudySeriesConcatenationInstanceModel</B>()</PRE>
<DL>
</DL>

<!-- ============ METHOD DETAIL ========== -->

<A NAME="method_detail"><!-- --></A>
<TABLE BORDER="1" WIDTH="100%" CELLPADDING="3" CELLSPACING="0" SUMMARY="">
<TR BGCOLOR="#CCCCFF" CLASS="TableHeadingColor">
<TD COLSPAN=1><FONT SIZE="+2">
<B>Method Detail</B></FONT></TD>
</TR>
</TABLE>

<A NAME="createTagList()"><!-- --></A><H3>
createTagList</H3>
<PRE>
protected void <B>createTagList</B>()</PRE>
<DL>
<DD><B>Description copied from class: <CODE><A HREF="../../../com/pixelmed/dicom/DicomDictionaryBase.html" title="class in com.pixelmed.dicom">DicomDictionaryBase</A></CODE></B></DD>
<DD><p>Concrete sub-classes implement this method to create a list of all tags in the dictionary.</p>
<P>
<DD><DL>
<DT><B>Overrides:</B><DD><CODE><A HREF="../../../com/pixelmed/dicom/DicomDictionary.html#createTagList()">createTagList</A></CODE> in class <CODE><A HREF="../../../com/pixelmed/dicom/DicomDictionary.html" title="class in com.pixelmed.dicom">DicomDictionary</A></CODE></DL>
</DD>
<DD><DL>
</DL>
</DD>
</DL>
<!-- ========= END OF CLASS DATA ========= -->
<HR>


<!-- ======= START OF BOTTOM NAVBAR ====== -->
<A NAME="navbar_bottom"><!-- --></A>
<A HREF="#skip-navbar_bottom" title="Skip navigation links"></A>
<TABLE BORDER="0" WIDTH="100%" CELLPADDING="1" CELLSPACING="0" SUMMARY="">
<TR>
<TD COLSPAN=3 BGCOLOR="#EEEEFF" CLASS="NavBarCell1">
<A NAME="navbar_bottom_firstrow"><!-- --></A>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="3" SUMMARY="">
  <TR ALIGN="center" VALIGN="top">
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../overview-summary.html"><FONT CLASS="NavBarFont1"><B>Overview</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="package-summary.html"><FONT CLASS="NavBarFont1"><B>Package</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#FFFFFF" CLASS="NavBarCell1Rev"> &nbsp;<FONT CLASS="NavBarFont1Rev"><B>Class</B></FONT>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="package-tree.html"><FONT CLASS="NavBarFont1"><B>Tree</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../deprecated-list.html"><FONT CLASS="NavBarFont1"><B>Deprecated</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../index-all.html"><FONT CLASS="NavBarFont1"><B>Index</B></FONT></A>&nbsp;</TD>
  <TD BGCOLOR="#EEEEFF" CLASS="NavBarCell1">    <A HREF="../../../help-doc.html"><FONT CLASS="NavBarFont1"><B>Help</B></FONT></A>&nbsp;</TD>
  </TR>
</TABLE>
</TD>
<TD ALIGN="right" VALIGN="top" ROWSPAN=3><EM>
</EM>
</TD>
</TR>

<TR>
<TD BGCOLOR="white" CLASS="NavBarCell2"><FONT SIZE="-2">
&nbsp;<A HREF="../../../com/pixelmed/database/DicomDatabaseInformationModel.html" title="class in com.pixelmed.database"><B>PREV CLASS</B></A>&nbsp;
&nbsp;<A HREF="../../../com/pixelmed/database/DicomDictionaryForStudySeriesInstanceModel.html" title="class in com.pixelmed.database"><B>NEXT CLASS</B></A></FONT></TD>
<TD BGCOLOR="white" CLASS="NavBarCell2"><FONT SIZE="-2">
  <A HREF="../../../index.html" target="_top"><B>FRAMES</B></A>  &nbsp;
&nbsp;<A HREF="DicomDictionaryForPatientStudySeriesConcatenationInstanceModel.html" target="_top"><B>NO FRAMES</B></A>  &nbsp;
&nbsp;<SCRIPT type="text/javascript">
  <!--
  if(window==top) {
    document.writeln('<A HREF="../../../allclasses-noframe.html"><B>All Classes</B></A>');
  }
  //-->
</SCRIPT>
<NOSCRIPT>
  <A HREF="../../../allclasses-noframe.html"><B>All Classes</B></A>
</NOSCRIPT>

</FONT></TD>
</TR>
<TR>
<TD VALIGN="top" CLASS="NavBarCell3"><FONT SIZE="-2">
  SUMMARY:&nbsp;NESTED&nbsp;|&nbsp;<A HREF="#fields_inherited_from_class_com.pixelmed.dicom.DicomDictionaryBase">FIELD</A>&nbsp;|&nbsp;<A HREF="#constructor_summary">CONSTR</A>&nbsp;|&nbsp;<A HREF="#method_summary">METHOD</A></FONT></TD>
<TD VALIGN="top" CLASS="NavBarCell3"><FONT SIZE="-2">
DETAIL:&nbsp;FIELD&nbsp;|&nbsp;<A HREF="#constructor_detail">CONSTR</A>&nbsp;|&nbsp;<A HREF="#method_detail">METHOD</A></FONT></TD>
</TR>
</TABLE>
<A NAME="skip-navbar_bottom"></A>
<!-- ======== END OF BOTTOM NAVBAR ======= -->

<HR>

</BODY>
</HTML>
