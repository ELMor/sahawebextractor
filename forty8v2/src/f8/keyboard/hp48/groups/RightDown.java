/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.groups;

import f8.Ref;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.keyboard.Menu;
import f8.keyboard.ShiftKey;
import f8.keyboard.Tecla;
import f8.keyboard.hp48.menu.UNITTOOLS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class RightDown extends Menu {
	public RightDown() {
		super(null);
		appendKey(new Tecla("Entry"));
		appendKey(new Tecla(""));
		appendKey(new Tecla(""));
		appendKey(new Tecla(""));
		appendKey(new Tecla("#"));
		appendKey(new ShiftKey(1));
		appendKey(new Tecla(""));
		appendKey(new Tecla(""));
		appendKey(new UNITTOOLS());
		appendKey(new Tecla("_"));
		appendKey(new ShiftKey(2));
		appendKey(new Tecla("POLAR"));
		appendKey(new Tecla("ARG"));
		appendKey(new Tecla("MENU", new Operation() { /*
														 * (non-Javadoc)
														 * 
														 * @see f8.Operation#exec()
														 */
			public void exec() throws F8Exception {
				Ref.ref.setLastMenu();
			}
		}));
		appendKey(new Tecla("\"\""));
		appendKey(new Tecla("OFF"));
		appendKey(new Tecla("\u008D", " \u008D "));
		appendKey(new Tecla("ret"));
		appendKey(new Tecla("\u0080", "ang"));
		appendKey(new Tecla("::", 1));
	}
}
