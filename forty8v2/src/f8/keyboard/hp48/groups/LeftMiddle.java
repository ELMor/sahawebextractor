/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.groups;

import f8.keyboard.Menu;
import f8.keyboard.PREV;
import f8.keyboard.Tecla;
import f8.keyboard.WithEnter;
import f8.keyboard.hp48.menu.IO;
import f8.keyboard.hp48.menu.LIBR;
import f8.keyboard.hp48.menu.MEMORY;
import f8.keyboard.hp48.menu.MODES;
import f8.keyboard.hp48.menu.PICTURE;
import f8.keyboard.hp48.menu.PRINT;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class LeftMiddle extends Menu {
	public LeftMiddle() {
		super(null);
		appendKey(new PRINT());
		appendKey(new IO());
		appendKey(new MODES());
		appendKey(new MEMORY());
		appendKey(new LIBR());
		appendKey(new PREV());
		appendKey(new WithEnter("UP", "UPDIR"));
		appendKey(new Tecla("DEF"));
		appendKey(new Tecla("->Q"));
		appendKey(new PICTURE());
		appendKey(new Tecla("VIEW"));
		appendKey(new WithEnter("SWAP"));
		appendKey(new WithEnter("ASIN"));
		appendKey(new WithEnter("ACOS"));
		appendKey(new WithEnter("ATAN"));
		appendKey(new WithEnter("SQ"));
		appendKey(new WithEnter("ALOG"));
		appendKey(new WithEnter("EXP"));
		appendKey(new WithEnter("EQUAT"));
		appendKey(new Tecla("EDIT"));
		appendKey(new Tecla("2D"));
		appendKey(new WithEnter("PURGE"));
		appendKey(new WithEnter("DROP"));
	}
}
