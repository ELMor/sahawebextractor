/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.CL;
import f8.Ref;
import f8.apps.LCDControl;
import f8.apps.display.CSys;
import f8.commands.Operation;
import f8.keyboard.Menu;
import f8.keyboard.Tecla;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class PICTURE extends Menu {
	static CL cl = Ref.ref.getCalcLogica();

	public PICTURE() {
		super("PICT");
		appendKey(ZoomMenu());
		appendKey(new Tecla("Z-BOX"));
		appendKey(new Tecla("CENT"));
		appendKey(new Tecla("COORD"));
		appendKey(new Tecla("LABEL"));
		appendKey(FcnMenu());

		appendKey(new Tecla("DOT+"));
		appendKey(new Tecla("DOT-"));
		appendKey(new Tecla("LINE", new Operation() {
			public void exec() {
				LCDControl lcd = Ref.ref.getLCD();
				CSys.Point c1 = lcd.getMark();
				CSys.Point c2 = lcd.getCursor();
				lcd.lineTo(c1, c2);
				lcd.setMark(c2);
			}
		}));
		appendKey(new Tecla("TLINE", new Operation() {
			public void exec() {
				LCDControl lcd = Ref.ref.getLCD();
				CSys.Point c1 = lcd.getMark();
				CSys.Point c2 = lcd.getCursor();
				lcd.lineTo(c1, c2);
			}
		}));
		appendKey(new Tecla("BOX", new Operation() {
			public void exec() {
				LCDControl lcd = Ref.ref.getLCD();
				CSys.Point c1 = lcd.getMark();
				CSys.Point c2 = lcd.getCursor();
				lcd.box(c1, c2);
			}
		}));
		appendKey(new Tecla("CIRCL", new Operation() {
			public void exec() {
				LCDControl lcd = Ref.ref.getLCD();
				CSys.Point c1 = lcd.getMark();
				CSys.Point c2 = lcd.getCursor();
				lcd.circle(c1, c2);
			}
		}));

		appendKey(new Tecla("MARK", new Operation() {
			public void exec() {
				LCDControl lcd = Ref.ref.getLCD();
				lcd.setMark(lcd.getCursor());
			}
		}));

		appendKey(new Tecla("REPL"));
		appendKey(new Tecla("SUB"));
		appendKey(new Tecla("DEL"));
		appendKey(new Tecla("+/-"));
	}

	public Menu ZoomMenu() {
		Menu m = new Menu("ZOOM");
		m.appendKey(new Tecla("XAUTO"));
		m.appendKey(new Tecla("X"));
		m.appendKey(new Tecla("Y"));
		m.appendKey(new Tecla("XY"));
		m.appendKey(null);
		m.appendKey(new Tecla("EXIT"));
		return m;
	}

	public Menu FcnMenu() {
		Menu m = new Menu("FCN");
		m.appendKey(new Tecla("ROOT"));
		m.appendKey(new Tecla("ISECT"));
		m.appendKey(new Tecla("SLOPE"));
		m.appendKey(new Tecla("AREA"));
		m.appendKey(new Tecla("EXTR"));
		m.appendKey(new Tecla("EXIT"));
		m.appendKey(new Tecla("F(X)"));
		m.appendKey(new Tecla("F'"));
		m.appendKey(new Tecla("NXEQ"));
		return m;
	}
}
