/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.keyboard.Menu;
import f8.keyboard.WithEnter;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class SYMB extends Menu {
	public SYMB() {
		super("SYMB");
		appendKey(new WithEnter("COLECT"));
		appendKey(new WithEnter("EXPAND"));
		appendKey(new WithEnter("ISOL"));
		appendKey(new WithEnter("QUAD"));
		appendKey(new WithEnter("SHOW"));
		appendKey(new WithEnter("TAYLR"));
		appendKey(new WithEnter("UMAT"));
		appendKey(new WithEnter("DMAT"));
		appendKey(new WithEnter("|"));
		appendKey(new WithEnter("APPLY"));
		appendKey(new WithEnter("QUOT"));
		appendKey(new WithEnter("->QPI"));
	}
}
