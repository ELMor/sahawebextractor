/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.keyboard.Menu;
import f8.keyboard.WithEnter;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class MEMORY extends Menu {
	public MEMORY() {
		super("MEMORY");
		appendKey(new WithEnter("MEM"));
		appendKey(new WithEnter("BYTES"));
		appendKey(new WithEnter("VARS"));
		appendKey(new WithEnter("ORDER"));
		appendKey(new WithEnter("PATH"));
		appendKey(new WithEnter("CRDIR"));

		appendKey(new WithEnter("TVARS"));
		appendKey(new WithEnter("PVARS"));
		appendKey(new WithEnter("NEWOB"));
		appendKey(new WithEnter("LIBS"));
		appendKey(new WithEnter("ATTACH"));
		appendKey(new WithEnter("DETACH"));

		appendKey(new WithEnter("MERGE"));
		appendKey(new WithEnter("FREE"));
		appendKey(new WithEnter("ARCHIVE"));
		appendKey(new WithEnter("RESTORE"));
		appendKey(new WithEnter("PGDIR"));
	}
}
