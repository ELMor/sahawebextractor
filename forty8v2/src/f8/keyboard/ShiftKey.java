/*
 * Created on 28-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.CalcGUI;
import f8.keyboard.hp48.groups.Keyboard;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class ShiftKey extends Tecla {
	int dir;

	public ShiftKey(int direccion) {
		super((direccion == 1) ? "Left" : "Right");
		dir = direccion;
	}

	public void pressed(CalcGUI ci) {
		CF cf = ci.getCalcFisica();
		int shStatus = cf.getShiftMode();
		if (dir == 1) {
			if (shStatus == 1) {
				cf.setShiftMode(0);
			} else {
				cf.setShiftMode(1);
			}
		} else {
			if (shStatus == 2) {
				cf.setShiftMode(0);
			} else {
				cf.setShiftMode(2);
			}
		}
		switch (cf.getShiftMode()) {
		case 0:
			ci.setMenu(0, null, Keyboard.normMiddle(), Keyboard.normDown());
			break;
		case 1:
			ci.setMenu(0, null, Keyboard.leftMiddle(), Keyboard.leftDown());
			break;
		case 2:
			ci.setMenu(0, null, Keyboard.rightMiddle(), Keyboard.rightDown());
			break;
		}
	}
}
