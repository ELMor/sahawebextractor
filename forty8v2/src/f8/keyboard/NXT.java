/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.CalcGUI;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class NXT extends Tecla {
	public NXT() {
		super("NEXT"); // NXT
	}

	public void pressed(CalcGUI cg) {
		CF cf = cg.getCalcFisica();
		cf.nxtUpMenuPage();
		cg.switchButtonGroup(1);
	}
}
