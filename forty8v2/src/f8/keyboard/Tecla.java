/*
 * Created on 22-jul-2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package f8.keyboard;

import f8.CL;
import f8.CalcGUI;
import f8.Ref;
import f8.commands.Command;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.objects.types.Proc;

/**
 * @author elinares
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class Tecla implements TeclaVirtual {
	Menu menu;

	String emision = null;

	Operation op = null;

	boolean isVarSolver = false;

	int backSpaces = 0;

	public Tecla(Menu m) {
		menu = m;
	}

	public Tecla(String nombre) {
		menu = new Menu(nombre);
	}

	public Tecla(String nombre, int bs) {
		this(nombre);
		backSpaces = bs;
	}

	public Tecla(String nombre, String emit) {
		this(nombre);
		emision = emit;
	}

	public Tecla(String nombre, Operation _op) {
		this(nombre);
		op = _op;
	}

	public Tecla(String nombre, Operation _op, boolean isvs) {
		this(nombre, _op);
		isVarSolver = isvs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.VirtualKey#isDir()
	 */
	public boolean isDir() {
		return menu.size() > 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.VirtualKey#getTitle(byte, byte)
	 */
	public String getTitle() {
		return menu.getTitle();
	}

	public void setMenu(CalcGUI cg) {
		cg.setMenu(1, menu, null, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.VirtualKey#pressed(f8.ui.CalcInterface, byte, byte)
	 */
	public void pressed(CalcGUI cg) throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (op != null) {
			op.exec();
		} else {
			String em = getEmit();
			Command s = cl.lookup(em);
			if (s != null) {
				if (cl.isSetting(CL.ALG_MOD)) {
					if (s.isAlgebraic()) {
						if (s.isInfix()) {
							if (s.isOperator()) {
								cg.emit(em, 0);
							} else {
								cg.emit(" " + em + " ", backSpaces);
							}
						} else {
							cg.emit(em + "()", 1);
						}
					} else {
						if (this instanceof TeclaVar) {
							cg.emit(em, 0);
						} else {
							if (!cg.getEditField().equals("")) {
								cl.enter(cg.getEditField());
							}
							s.exec();
							cg.refresh(true);
						}
					}
				} else if (cl.isSetting(CL.PRG_MOD)) {
					cg.emit(" " + em + " ", 0);
				} else if (isDir()) {
					setMenu(cg);
				} else {
					if (!cg.getEditField().equals("")) {
						cl.enter(cg.getEditField());
					}
					if (s instanceof Proc) {
						s.exec();
					} else {
						s.store();
					}
					cg.refresh(true);
				}
			} else {
				cg.emit(em, backSpaces);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#getEmit()
	 */
	public String getEmit() {
		String name = (emision == null) ? getTitle() : emision;
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#isVarSolver()
	 */
	public boolean isVarSolver() {
		return isVarSolver;
	}
}
