/*
 * Created on 28-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.CL;
import f8.CalcGUI;
import f8.commands.storage.STO;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Literal;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class TeclaVar extends Tecla {
	boolean isDir = false;

	public TeclaVar(String s, boolean _isDir) {
		super(s);
		isDir = _isDir;
	}

	public void setMenu(CalcGUI cg) {
		CL cl = cg.getCalcLogica();
		cl.changeToDir(getEmit());
		cg.setMenu(1, new VAR(), null, null);
	}

	public void pressed(CalcGUI cg) throws F8Exception {
		CL cl = cg.getCalcLogica();
		switch (cg.getCalcFisica().getShiftMode()) {
		case 0:
			super.pressed(cg);
			break;
		case 1:
			if (!cg.getEditField().equals("")) {
				cl.enter(cg.getEditField());
			}
			if (cl.check(1)) {
				STO.storeVar(getTitle(), cl.pop());
			} else {
				throw new TooFewArgumentsException(this);
			}
			cg.refresh(true);
			break;
		case 2:
			if (!cg.getEditField().equals("")) {
				cl.enter(cg.getEditField());
			}
			Stackable cont = (Stackable) cl.lookup(getTitle());
			if (cont != null) {
				cl.push(cont);
			} else {
				cl.push(new Literal(getTitle()));
			}
			cg.refresh(true);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#isDir()
	 */
	public boolean isDir() {
		return isDir;
	}
}
