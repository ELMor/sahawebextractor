/*
 * Created on 15-jun-2003
 *
 */
package f8.keyboard;

/**
 * @author elinares
 * 
 */
public interface CF {
	/**
	 * Devuelve el estado de las techas shift
	 * 
	 * @return 0 no hay shift, 1 Left shift, 2 Right shift
	 */
	public int getShiftMode();

	/**
	 * Coloca el shift
	 * 
	 * @param smode
	 *            0 no hay shift, 1 Left shift, 2 Right shift
	 */
	public void setShiftMode(int smode);

	/**
	 * Devuelve el estado de Alpha
	 * 
	 * @return 0 sin pulsar, 1 pulsacion simple, 2 pulsacion constante
	 */
	public int getAlphaMode();

	public void setAlphaMode(int amod);

	/**
	 * Devuelve el estado de edicion en modo programacion
	 * 
	 * @return verdadero o falso
	 */
	public boolean getPgmMode();

	public void setPgmMode(boolean em);

	/**
	 * Procesamiento de la pulsacion de una tecla
	 * 
	 * @param i
	 *            Numero de la tecla, comenzando por 0 arriba a la izquierda.
	 */
	public void key(int i);

	/**
	 * Devuelve las cadenas de texto de la botonera correspondiente
	 * 
	 * @return array de String con los textos de los botones
	 */
	public String[] getUpStringArray();

	public boolean[] getUpIsDirectory();

	public boolean[] getUpIsOn();

	public String[] getMiStringArray();

	public String[] getDoStringArray();

	/**
	 * Obtiene el diccionario de usuario
	 * 
	 * @return diccionario de usuario
	 */

	// public Hashtable getUserDict();
	public Menu getUpMenu();

	public Menu getMiMenu();

	public Menu getDoMenu();

	public void setUpMenu(Menu m);

	public void setMiMenu(Menu m);

	public void setDoMenu(Menu m);

	public void nxtUpMenuPage();

	public void prevUpMenuPage();

	public void resetUpMenuPage();

	public boolean[] getUpVarSolver();

	public void press(TeclaVirtual vk);

	public abstract int getUpMenuPage();

	public abstract void setUpMenuPage(int i);
}
