/*
 * Created on 23-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.CalcGUI;
import f8.Ref;
import f8.exceptions.F8Exception;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class WithEnter extends Tecla {
	int setting = -1;

	int value = -1;

	String grpCheck = null;

	public WithEnter(String name) {
		super(name);
	}

	public WithEnter(String name, String emit) {
		super(name, emit);
	}

	public WithEnter(String nombre, String emit, String grp, int s, int d) {
		this(nombre, emit);
		grpCheck = grp;
		setting = s;
		value = d;
	}

	public boolean isChecked() {
		if ((setting < 0) || (setting > 15)) {
			return false;
		}
		return Ref.ref.getCalcLogica().getSetting(setting) == value;
	}

	public void setChecked(boolean v) {
		if ((setting < 0) || (setting > 15)) {
			return;
		}
		Ref.ref.getCalcLogica().setSettings(setting, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#pressed(f8.platform.CalcGUI)
	 */
	public void pressed(CalcGUI cg) throws F8Exception {
		super.pressed(cg);
		if (setting > -1) {
			cg.refreshProgMenu();
		}
	}

}
