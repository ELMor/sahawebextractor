/*
 * Created on 01-ago-2003
 *
 
 
 */
package f8;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface Vector {
	public abstract void add(Object obj);

	public abstract void insert(int index, Object obj);

	public abstract Object get(int index);

	public abstract void set(int index, Object obj);

	public abstract int find(Object k);

	public abstract int getCount();

	public abstract Object[] toObjectArray();

	public abstract void push(Object obj);

	public abstract Object pop();

	public abstract Object peek();

	public abstract void clear();

	public abstract int size();

	public abstract int indexOf(Object elem);

	public abstract int indexOf(Object elem, int index);

	public abstract Object elementAt(int index);

	public abstract void setElementAt(Object obj, int index);

	public abstract void removeElementAt(int index);

	public abstract void insertElementAt(Object obj, int index);

	public abstract void addElement(Object obj);

	public abstract boolean removeElement(Object obj);

	public abstract void removeAllElements();
}
