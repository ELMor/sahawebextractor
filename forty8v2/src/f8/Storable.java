package f8;

public interface Storable {
	public int getID();

	public void saveState(DataStream ds);

	public void loadState(DataStream ds);
}
