/*
 * Created on 02-ago-2003
 *
 
 
 */
package f8;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface MathKernel {
	/**
	 * Comprueba si es -inf
	 * 
	 * @param d
	 *            numero a comprobar
	 * @return true si la representacion IEEE es -inf
	 */
	public boolean isNegativeInfinite(double d);

	/**
	 * Comprueba si d no es un numero
	 * 
	 * @param d
	 *            numero a comprobar
	 * @return true si d is Not A Number
	 */
	public boolean isNaN(double d);

	/**
	 * Devuelve la representacion double de una cadena
	 * 
	 * @param s
	 *            cadena que contiene el numero
	 * @return representacion double
	 */
	public double toDouble(String s);

	public int toInt(String s);

	public float toFloat(String s);

	public String format(double d, int mode, int digits);

	public String format(int d, int mode);

	public int fromDEC(String s);

	public int fromHEX(String s);

	public int fromOCT(String s);

	public int fromBIN(String s);
}
