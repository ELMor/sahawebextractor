package f8;

import f8.commands.Command;

public final class CommandDict {

	public class Entry {
		public String className;

		public int tipo;

		public int id;

		public String token;

		public Entry(String name, int tipo, int id, String token) {
			this.className = name;
			this.tipo = tipo;
			this.id = id;
			this.token = token;
		}
	};

	private static Entry bag[] = null;

	public static Command getInstance(String id){
		try {
			for(int i=0;i<bag.length;i++){
				if(bag[i].token.equals(id))
					return (Command)Class.forName(bag[i].className).newInstance();
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Command getInstance(int id){
		try {
			for(int i=0;i<bag.length;i++){
				if(bag[i].id==id)
					return (Command)Class.forName(bag[i].className).newInstance();
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public CommandDict() {
		bag = new Entry[] {
				new Entry("f8.apps.display.DRAW", 3, 6000, "DRAW"),
				new Entry("f8.modes.DEC", 3, 482, "DEC"),
				new Entry("f8.commands.math.parts.CEIL", 3, 3014, "CEIL"),
				new Entry("f8.commands.math.base.AND", 3, 1, "AND"),
				new Entry("f8.modes.DEG", 3, 489, "DEG"),
				new Entry("f8.objects.build.TYPE", 3, 3065, "TYPE"),
				new Entry("f8.commands.alg.matrx.DET", 3, 62, "DET"),
				new Entry("f8.apps.display.CIRCLE", 3, 404, "CIRCLE"),
				new Entry("f8.apps.display.AUTO", 3, 6002, "AUTO"),
				new Entry("f8.commands.math.hyp.TANH", 3, 3028, "TANH"),
				new Entry("f8.modes.FIX", 3, 75, "FIX"),
				new Entry("f8.commands.math.parts.MOD", 3, 3006, "MOD"),
				new Entry("f8.commands.math.parts.RND", 3, 3015, "RND"),
				new Entry("f8.modes.ChkCNCT", 3, 498, "ChkCNC_"),
				new Entry("f8.commands.math.trig.ACOS", 3, 506, "ACOS"),
				new Entry("f8.commands.prog.brch.IF", 1, 23, "IF 0 THEN  END"),
				new Entry("f8.modes.GRAD", 3, 487, "GRAD"),
				new Entry("f8.modes.RAD", 3, 488, "RAD"),
				new Entry("f8.commands.math.hyp.ASINH", 3, 3025, "ASINH"),
				new Entry("f8.commands.storage.HOME", 1, 124, "HOME"),
				new Entry("f8.commands.math.parts.ABS", 3, 80, "ABS"),
				new Entry("f8.modes.SCI", 3, 78, "SCI"),
				new Entry("f8.commands.stk.PICK", 3, 16, "PICK"),
				new Entry("f8.commands.compare.GE", 3, 83, ">="),
				new Entry("f8.commands.math.parts.PCTT", 3, 3009, "%T"),
				new Entry("f8.commands.math.exp.LOG", 3, 76, "LOG"),
				new Entry("f8.objects.types.Comment", 3, 3, "//null"),
				new Entry("f8.commands.alg.matrx.Gauss", 3, 64, "GAUSS"),
				new Entry("f8.objects.types.Complex", 3, 60, "(0,0)"),
				new Entry("f8.commands.compare.GT", 3, 84, ">"),
				new Entry("f8.commands.math.parts.IM", 3, 50, "IM"),
				new Entry("f8.commands.math.parts.IP", 3, 3012, "IP"),
				new Entry("f8.apps.display.BOX", 3, 403, "BOX"),
				new Entry("f8.commands.math.simple.MINUS", 3, 87, "-"),
				new Entry("f8.commands.math.deriv.Integral", 3, 1118, "?"),
				new Entry("f8.objects.types.Double", 3, 73, "3.141592653589793"),
				new Entry("f8.objects.constants.MAXR", 3, 2380, "MAXR"),
				new Entry("f8.commands.math.trig.SIN", 3, 58, "SIN"),
				new Entry("f8.commands.math.parts.XPON", 3, 3011, "XPON"),
				new Entry("f8.commands.storage.PURGE", 3, 510, "PURGE"),
				new Entry("f8.commands.alg.matrx.RowScale", 3, 71, "ROWSCALE"),
				new Entry("f8.commands.symb.Sigma", 3, 1117, "?"),
				new Entry("f8.commands.math.deriv.DER", 3, 101, "?"),
				new Entry("f8.commands.prog.brch.STOP", 3, 47, "STOP"),
				new Entry("f8.objects.build.TO_LIST", 3, 302, "->LIST"),
				new Entry("f8.commands.stk.DROP", 3, 8, "DROP"),
				new Entry("f8.objects.constants.PI", 3, 77, "?"),
				new Entry("f8.commands.math.hyp.SINH", 3, 3024, "SINH"),
				new Entry("f8.commands.math.exp.EXP", 3, 74, "EXP"),
				new Entry("f8.commands.math.parts.TRNC", 3, 3016, "TRNC"),
				new Entry("f8.apps.display.LABEL", 3, 6004, "LABEL"),
				new Entry("f8.commands.math.prob.FACTORIAL", 3, 3021, "!"),
				new Entry("f8.commands.math.trig.TAN", 3, 53, "TAN"),
				new Entry("f8.commands.math.parts.FLOOR", 3, 18, "FLOOR"),
				new Entry("f8.commands.alg.matrx.RowSwap", 3, 79, "ROWSWAP"),
				new Entry("f8.commands.units.CONVERT", 3, 104, "CONVERT"),
				new Entry("f8.commands.storage.CRDIR", 3, 125, "CRDIR"),
				new Entry("f8.commands.stk.ROLLD", 3, 11, "ROLLD"),
				new Entry("f8.modes.HEX", 3, 483, "HEX"),
				new Entry("f8.commands.symb.COLECT", 3, 100, "COLECT"),
				new Entry("f8.commands.math.simple.INV", 3, 105, "INV"),
				new Entry("f8.commands.math.exp.SQ", 3, 512, "SQ"),
				new Entry("f8.modes.ChkSTK", 3, 500, "ChkSTK_"),
				new Entry("f8.objects.build.EQ_TO", 3, 3058, "EQ->"),
				new Entry("f8.commands.alg.vectr.DFLECHAR", 3, 3043, "D?R"),
				new Entry("f8.objects.types.F8Vector", 3, 92,"[<empty vector>]"),
				new Entry("f8.commands.stk.DROP2", 3, 21, "DROP2"),
				new Entry("f8.commands.alg.matrx.DIM", 3, 63, "DIM"),
				new Entry("f8.apps.display.XRNG", 3, 6003, "XRNG"),
				new Entry("f8.commands.prog.brch.FORNEXT", 1, 117, "FOR i NEXT"),
				new Entry("f8.commands.math.trig.ALOG", 3, 511, "ALOG"),
				new Entry("f8.objects.types.F8String", 1, 48, "\"\""),
				new Entry("f8.modes.ChkARG", 3, 499, "ChkARG_"),
				new Entry("f8.commands.units.UBASE", 3, 106, "UBASE"),
				new Entry("f8.commands.stk.DROPN", 3, 22, "DROPN"),
				new Entry("f8.commands.alg.vectr.CROSS", 3, 61, "CROSS"),
				new Entry("f8.commands.math.parts.PCT", 3, 3007, "%"),
				new Entry("f8.commands.units.UFACT", 3, 103, "UFACT"),
				new Entry("f8.commands.stk.SWAP", 3, 49, "SWAP"),
				new Entry("f8.commands.storage.STEQ", 3, 2094, "STEQ"),
				new Entry("f8.objects.build.TO_ARRY", 3, 3059, "->ARRY"),
				new Entry("f8.commands.storage.UPDIR", 3, 123, "UPDIR"),
				new Entry("f8.modes.ChkBEEP", 3, 502, "ChkBEEP_"),
				new Entry("f8.commands.math.prob.COMB", 3, 3019, "COMB"),
				new Entry("f8.commands.stk.COPY", 3, 4, "COPY"),
				new Entry("f8.commands.math.parts.ARG", 3, 3003, "ARG"),
				new Entry("f8.modes.ChkCLK", 3, 496, "ChkCLK_"),
				new Entry("f8.objects.types.Int", 3, 26, "# 1 d"),
				new Entry("f8.commands.math.parts.MIN", 3, 3004, "MIN"),
				new Entry("f8.commands.math.prob.PERM", 3, 3020, "PERM"),
				new Entry("f8.commands.alg.vectr.FLECHAV2", 3, 3041, "?V2"),
				new Entry("f8.commands.alg.vectr.FLECHAV3", 3, 3042, "?V3"),
				new Entry("f8.commands.compare.LE", 3, 85, "<="),
				new Entry("f8.commands.stk.ROLL", 3, 7, "ROLL"),
				new Entry("f8.commands.stk.EVAL", 3, 15, "EVAL"),
				new Entry("f8.commands.alg.matrx.RowSub", 3, 72, "ROWSUB"),
				new Entry("f8.commands.stk.OVER", 3, 2, "OVER"),
				new Entry("f8.objects.types.Proc", 1, 40, "� "),
				new Entry("f8.commands.units.UVAL", 3, 2484, "UVAL"),
				new Entry("f8.objects.types.Lista", 3, 120, "new Entry(  )"),
				new Entry("f8.commands.math.trig.ASIN", 3, 54, "ASIN"),
				new Entry("f8.modes.ChkCMD", 3, 490, "ChkCMD_"),
				new Entry("f8.commands.compare.LT", 3, 86, "<"),
				new Entry("f8.objects.build.TO_STR", 3, 3060, "->STR"),
				new Entry("f8.commands.math.hyp.EXPM", 3, 3030, "EXPM"),
				new Entry("f8.modes.STD", 3, 504, "STD"),
				new Entry("f8.commands.math.hyp.ACOSH", 3, 3027, "ACOSH"),
				new Entry("f8.commands.math.parts.CONJ", 3, 3002, "CONJ"),
				new Entry("f8.objects.types.Literal", 3, 32, "null"),
				new Entry("f8.modes.ENG", 3, 505, "ENG"),
				new Entry("f8.objects.types.Unit", 1, 118, "0_"),
				new Entry("f8.commands.math.trig.COS", 3, 57, "COS"),
				new Entry("f8.apps.display.LINE", 3, 402, "LINE"),
				new Entry("f8.objects.types.TempUnit", 1, 430, "0_"),
				new Entry("f8.objects.constants.E", 3, 2379, "e"),
				new Entry("f8.commands.prog.FLECHA", 3, 901, "?"),
				new Entry("f8.objects.build.TO_UNIT", 3, 508, "->UNIT"),
				new Entry("f8.objects.build.TO_TAG", 3, 600, "->TAG"),
				new Entry("f8.commands.stk.DUP2", 3, 24, "DUP2"),
				new Entry("f8.commands.symb.ASSIGN", 3, 13, "="),
				new Entry("f8.objects.constants.I", 3, 65, "i"),
				new Entry("f8.commands.math.exp.LN", 3, 102, "LN"),
				new Entry("f8.commands.math.trig.ATAN", 3, 52, "ATAN"),
				new Entry("f8.commands.alg.matrx.JORDAN", 3, 66, "JORDAN"),
				new Entry("f8.commands.math.base.OR", 3, 38, "OR"),
				new Entry("f8.commands.math.parts.PCTCH", 3, 3008, "%CH"),
				new Entry("f8.commands.alg.vectr.RFLECHAD", 3, 3044, "R?D"),
				new Entry("f8.commands.alg.matrx.MkMatrix", 3, 68, "MATRIX"),
				new Entry("f8.objects.types.InfixExp", 1, 119, "''"),
				new Entry("f8.commands.prog.brch.WHILEREPEAT", 1, 10, "WHILE 0 REPEAT  END"),
				new Entry("f8.objects.build.R_TO_C", 3, 3061, "R->C"),
				new Entry("f8.apps.display.YRNG", 3, 6014, "YRNG"),
				new Entry("f8.modes.BIN", 3, 480, "BIN"),
				new Entry("f8.commands.stk.DUPN", 3, 19, "DUPN"),
				new Entry("f8.objects.build.DTAG", 3, 3063, "DTAG"),
				new Entry("f8.objects.build.VTYPE", 3, 3066, "VTYPE"),
				new Entry("f8.commands.math.hyp.COSH", 3, 3026, "COSH"),
				new Entry("f8.commands.math.base.NOT", 3, 37, "NOT"),
				new Entry("f8.commands.math.simple.DIV", 3, 81, "/"),
				new Entry("f8.commands.math.simple.PLUS", 3, 88, "+"),
				new Entry("f8.modes.ChkSYM", 3, 501, "ChkSYM_"),
				new Entry("f8.commands.math.simple.TIMES", 3, 91, "*"),
				new Entry("f8.commands.alg.CommandSequence", 1, 20, ""),
				new Entry("f8.objects.constants.MINR", 3, 2381, "MINR"),
				new Entry("f8.commands.symb.EXPAND", 3, 107, "EXPAND"),
				new Entry("f8.commands.math.exp.CARET", 3, 56, "^"),
				new Entry("f8.commands.alg.vectr.VFLECHA", 3, 3040, "V?"),
				new Entry("f8.commands.math.parts.MAX", 3, 3005, "MAX"),
				new Entry("f8.commands.math.parts.MANT", 3, 3010, "MANT"),
				new Entry("f8.commands.stk.ROT", 3, 12, "ROT"),
				new Entry("f8.apps.display.MARK", 3, 401, "MARK"),
				new Entry("f8.modes.OCT", 3, 481, "OCT"),
				new Entry("f8.commands.math.exp.SQRT", 3, 59, "?"),
				new Entry("f8.commands.storage.PGDIR", 3, 509, "PGDIR"),
				new Entry("f8.modes.D3Mode1", 3, 486, "3D_1"),
				new Entry("f8.commands.storage.MENU", 3, 436, "MENU"),
				new Entry("f8.commands.math.simple.CHS", 3, 801, "CHS"),
				new Entry("f8.modes.D3Mode2", 3, 485, "3D_2"),
				new Entry("f8.modes.D3Mode3", 3, 484, "3D_3"),
				new Entry("f8.modes.FMMode", 3, 479, "0_0"),
				new Entry("f8.commands.stk.DEPTH", 3, 17, "DEPTH"),
				new Entry("f8.commands.prog.debug.HALT", 3, 2987, "HALT"),
				new Entry("f8.commands.alg.matrx.MkVector", 3, 69, "VECTOR"),
				new Entry("f8.commands.math.hyp.LNP1", 3, 3031, "LNP1"),
				new Entry("f8.commands.math.parts.FP", 3, 3013, "FP"),
				new Entry("f8.commands.math.hyp.ATANH", 3, 3029, "ATANH"),
				new Entry("f8.commands.prog.brch.DOUNTIL", 1, 42, "DO UNTIL 1 END "),
				new Entry("f8.commands.math.parts.SIGN", 3, 3001, "SIGN"),
				new Entry("f8.commands.storage.STO", 3, 5, "STO"),
				new Entry("f8.commands.stk.DUP", 3, 9, "DUP"),
				new Entry("f8.modes.ChkML", 3, 497, "ChkML_"),
				new Entry("f8.commands.math.parts.RE", 3, 70, "RE"),
				new Entry("f8.commands.math.base.XOR", 3, 1532, "XOR"),
				new Entry("f8.objects.types.Matrix", 3, 67, "[[1 ]]"),
				new Entry("f8.commands.compare.EQ", 3, 82, "=="),
				new Entry("f8.objects.build.OBJ_TO", 3, 301, "OBJ->"),
				new Entry("f8.apps.display.ERASE", 3, 6001, "ERASE"),
				new Entry("f8.objects.types.Directory", 1, 127, "DIR  END"),
				new Entry("f8.objects.build.C_TO_R", 3, 3062, "C->R")
		};

	}
}
