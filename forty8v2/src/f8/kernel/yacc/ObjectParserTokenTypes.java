// $ANTLR 2.7.2: "object.g" -> "ObjectLexer.java"$

package f8.kernel.yacc;

public interface ObjectParserTokenTypes {
	int EOF = 1;

	int NULL_TREE_LOOKAHEAD = 3;

	int PROGRAM = 4;

	int LITERAL = 5;

	int INFIX = 6;

	int VECTOR = 7;

	int MATRIZ = 8;

	int LIST = 9;

	int OPERADOR = 10;

	int STRING = 11;

	int LOCALVAR = 12;

	int COMPLEX = 13;

	int FORNEXT = 14;

	int FORSTEP = 15;

	int IF = 16;

	int IFELSE = 17;

	int DOUNTIL = 18;

	int WHILEREPEAT = 19;

	int STARTSTEP = 20;

	int STARTNEXT = 21;

	int IFERR = 22;

	int SEQOB = 23;

	int UNIT = 24;

	int NEG = 25;

	int INV = 26;

	int TAG = 27;

	int VARAT = 28;

	int DERIV = 29;

	int INTEG = 30;

	int SIGMA = 31;

	int ID = 32;

	int INTNUMBER = 33;

	int DPUNTOS = 34;

	int NUMBER = 35;

	int UNDERSCO = 36;

	int MULT = 37;

	int DIV = 38;

	int CARET = 39;

	int STRING_LITERAL = 40;

	int ASSIGN = 41;

	int PLUS = 42;

	int MINUS = 43;

	int LT = 44;

	int LE = 45;

	int LEOC = 46;

	int EQ = 47;

	int DISTI = 48;

	int GE = 49;

	int GEOC = 50;

	int GT = 51;

	int OPENPROG = 52;

	int OPENPROGOC = 53;

	int CLOSEPROG = 54;

	int CLOSEPROGOC = 55;

	int SCOMI = 56;

	int LLAA = 57;

	int LLAC = 58;

	int CORO = 59;

	int CORC = 60;

	int LPAREN = 61;

	int COMMA = 62;

	int RPAREN = 63;

	int LITERAL_AND = 64;

	int LITERAL_OR = 65;

	int LITERAL_XOR = 66;

	int VBAR = 67;

	int LITERAL_FOR = 68;

	int LITERAL_NEXT = 69;

	int LITERAL_STEP = 70;

	int LITERAL_START = 71;

	int FLECHA = 72;

	int FLECHAOC = 73;

	int LITERAL_IF = 74;

	int LITERAL_THEN = 75;

	int LITERAL_END = 76;

	int LITERAL_ELSE = 77;

	int LITERAL_IFERR = 78;

	int LITERAL_DO = 79;

	int LITERAL_UNTIL = 80;

	int LITERAL_WHILE = 81;

	int LITERAL_REPEAT = 82;

	int TOUNIT = 83;

	int VTO = 84;

	int TOV2 = 85;

	int TOV3 = 86;

	int DTOR = 87;

	int RTOD = 88;

	int RTOB = 89;

	int BTOR = 90;

	int OBJTO = 91;

	int EQTO = 92;

	int TOARRY = 93;

	int TOLIST = 94;

	int TOSTR = 95;

	int TOTAG = 96;

	int RTOC = 97;

	int CTOR = 98;

	int FSINT = 99;

	int FCINT = 100;

	int FSINTC = 101;

	int FCINTC = 102;

	int WS = 103;

	int ESC = 104;

	int DIGIT = 105;

	int HEXCHAR = 106;

	int OCTCHAR = 107;

	int ALMOA = 108;

	int ANGLE = 109;

	int SQRT = 110;

	int PI = 111;
}
