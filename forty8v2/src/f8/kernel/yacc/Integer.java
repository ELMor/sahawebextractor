/*
 * Created on 10-ago-2003
 *
 
 
 */
package f8.kernel.yacc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Integer {
	public static final int MAX_VALUE = 2147483647;

	int val;

	public Integer(int i) {
		val = i;
	}

	public int intValue() {
		return val;
	}
}
