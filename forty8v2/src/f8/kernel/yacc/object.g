header {
package f8.kernel.yacc;
import f8.platform.Vector;
import java.io.*;
import f8.kernel.*;
import f8.objects.*;
}

class ObjectParser extends Parser;

options {
	codeGenMakeSwitchThreshold = 3;
	codeGenBitsetTestThreshold = 4;
	buildAST=true;
	ASTLabelType = "antlr.CommonAST"; // change default of "AST"
	k=5;
}

tokens {
	PROGRAM="program"; LITERAL="literal"; INFIX="infix";
	VECTOR="vector"; MATRIZ="matriz";
	LIST="list"; OPERADOR="OPERADOR";
	STRING="string"; LOCALVAR="localvar"; COMPLEX="complex";
	FORNEXT="fornext"; FORSTEP="forstep";
	IF="if"; IFELSE="ifelse"; DOUNTIL="dountil";
	WHILEREPEAT="whilerepeat";
	STARTSTEP="startstep"; STARTNEXT="startnext";
	IFERR="iferr"; SEQOB="seqob"; UNIT="unit";
	NEG="neg"; INV="inv"; TAG="tag"; VARAT="varAt";
	DERIV="deriv"; INTEG="integ"; SIGMA="sigma";
}

hp48Object![Vector itemVector] : 
	(n1:object
		{itemVector.add(#n1);}
	)+ 
	EOF!
	;

object! : 
    ( p:program	
    	{#object=#p;
    	}
	| i:infixExpression		
		{#object=#i;
		}
	| v:vector				
		{#object=#v;
		}
	| m:matriz				
		{#object=#m;
		}
	| id:ID			
		{#object=#id;
		}
	| li:list				
		{#object=#li;
		}
	| n1:INTNUMBER				
		{#object=#n1;
		}
	| n2:complex			
		{#object=#n2;
		}
	| n3:nunit			
		{#object=#n3;
		}
	| o:operador			
		{#object=#o;
		}
	| s:string				
		{#object=#s;
		}
	| po:pob
		{#object=#po;
		}
	| cmd: specialCommand
		{#object=#(#[ID]);
		 #object.setText(#cmd.getText());
		}
	| DPUNTOS tag:ID DPUNTOS tagged:object
		{#object=#(#[TAG],tag,tagged);
		}
	)
	;

nunit! :
	n2:NUMBER
		{#nunit=#n2;}
	
 	(UNDERSCO n3:uExpr 			
 		{#nunit=#(#[UNIT],#nunit,#n3);}	
 	)?
	;

uExpr
	: (uEExpr|NUMBER) ((MULT^|DIV^) uExpr )?	
	;

uEExpr
	: ID ( CARET^ NUMBER )?
	;
	
seqOb!
	: 	    				{ #seqOb=#[SEQOB]; }
		(n1:object			{ #seqOb.addChild(#n1); }
		)+		
	;

string! :
	n1:STRING_LITERAL
		{#string=#n1;}	
	;

operador! :
	( n1:CARET
		{#operador=#(#[OPERADOR],#n1);}
	| i1:ASSIGN
		{#operador=#(#[OPERADOR],#i1);}
	| n2:MULT
		{#operador=#(#[OPERADOR],#n2);}
	| n3:DIV
		{#operador=#(#[OPERADOR],#n3);}
	| n4:PLUS
		{#operador=#(#[OPERADOR],#n4);}
	| n5:MINUS
		{#operador=#(#[OPERADOR],#n5);}
	| c1:LT
		{#operador=#(#[OPERADOR],#c1);}
	| c2:( LE | LEOC )
		{#operador=#(#[OPERADOR],#c2);}
	| c3:EQ
		{#operador=#(#[OPERADOR],#c3);}
	| c6:DISTI
		{#operador=#(#[OPERADOR],#c6);}
	| c4:( GE | GEOC ) 
		{#operador=#(#[OPERADOR],#c4);}
	| c5:GT
		{#operador=#(#[OPERADOR],#c5);}
	);

program! : 
	(OPENPROG! | OPENPROGOC!)
	n1:seqOb					
	(CLOSEPROG! | CLOSEPROGOC! | EOF!)			
								{#program=#(#[PROGRAM],#n1);}
	;

literalIdentifier!:
	SCOMI!
	n1:ID 						{#literalIdentifier=#(#[LITERAL],#n1);}
	(SCOMI!|EOF!)
	;
	
list!:
	LLAA! 						{#list=#(#[LIST]);}
	(n1:object					{#list.addChild(#n1);}
	)*
	(LLAC!|EOF!)				
	;
	
infixExpression! :
	SCOMI!  					{#infixExpression=(#[INFIX]);}
	  n1:assignExpr 			{#infixExpression.addChild(#n1);}
	  ( n2:varAt                {#infixExpression.addChild(#n2);}
	  )?
	  
	(SCOMI!|EOF!)
	;
	
vector! :
	CORO! 						{#vector=(#[VECTOR]);}
	(n1:NUMBER					{#vector.addChild(#n1);}
	)+ 
	(CORC!|EOF!)
	;
	
complex! :
	LPAREN! n1:NUMBER COMMA n2:NUMBER RPAREN!
		{#complex=#(#[COMPLEX],#n1,#n2);}
	;

matriz! :
	CORO! 						{#matriz=(#[MATRIZ]);}
	(n1:vector					{#matriz.addChild(#n1);}
	)+ 
	CORC!
	;
	
assignExpr
	:	testExpr
		(
			ASSIGN^
			assignExpr 
		)?
	;

testExpr
	:
		boolExpr
		(
			(LT^|LE^|EQ^|GE^|GT^)
			boolExpr
		)?
	;

boolExpr
	:
		addExpr
		(
			("AND"^|"OR"^|"XOR")
			addExpr
		)?
	;

addExpr
	:	multExpr 
		(
			(PLUS^|MINUS^)
			multExpr
		)*
	;

multExpr
	:	exponentExpr
		(
			(MULT^|DIV^)
			exponentExpr
		)*
	;

exponentExpr
	:  postfixExpr
	   (
	   		CARET^
	   		postfixExpr
	   )*
	;

postfixExpr!
	: (ID LPAREN)=> n1:ID n2:parenArgs 
		{#postfixExpr=#n1; #n1.addChild(#n2);}
	| n3:ID
		{#postfixExpr=#n3;}
    | (ID UNDERSCO)=> u1:ID UNDERSCO u2:uExpr 
        { AST uno=#(#[NUMBER]);
          uno.setText("1");
          AST theUnit=#(#[UNIT],uno,#u2);
          #postfixExpr=#(#[MULT],#u1,theUnit);
          #postfixExpr.setText("*");
        }
	| n4:nunit
		{#postfixExpr=#n4;}
	| (LPAREN NUMBER COMMA NUMBER RPAREN)=> n5:complex
		{#postfixExpr=#n5;}
	| LPAREN! n6:assignExpr RPAREN!
		{#postfixExpr=#n6;}
	| MINUS n7:postfixExpr
		{#postfixExpr=#(#[NEG],#n7);}
	| DERIV derid:ID LPAREN derfun:assignExpr RPAREN
		{#postfixExpr=#(#[DERIV],#derfun,#derid);}
	| INTEG LPAREN intdown:assignExpr COMMA 
	               intup:assignExpr COMMA 
	               intfun:assignExpr COMMA 
	               intid:ID RPAREN
		{#postfixExpr=#(#[INTEG],#intdown,#intup,#intfun,#intid);}
	| SIGMA LPAREN sigid:ID ASSIGN sigdown:assignExpr COMMA 
	               sigup:assignExpr COMMA 
	               sigfun:assignExpr RPAREN
		{#postfixExpr=#(#[SIGMA],#sigid,#sigdown,#sigup,#sigfun);}	
	;
	
parenArgs
	:	
      LPAREN!
      (
         assignExpr
         (
            COMMA!
	        assignExpr
         )*
      )?
      RPAREN!
	;
	
varAt!
	: VBAR LPAREN n1:ID ASSIGN n2:addExpr RPAREN	
							{#varAt=#(#[VARAT],#n1,#n2);}
	;

pob! 
	: "FOR" n1:ID n2:seqOb				
	  ("NEXT"				{#pob=#(#[FORNEXT],#n1,#n2);}
	  |n3:NUMBER "STEP"		{#pob=#(#[FORSTEP],#n1,#n2,#n3);}
	  )
	| "START" s1:seqOb				
	  ("NEXT"				{#pob=#(#[STARTNEXT],#s1);}
	  |s2:NUMBER "STEP"		{#pob=#(#[STARTSTEP],#s1,#s2);}
	  )
	| (FLECHA | FLECHAOC) 	{#pob=#[LOCALVAR]; }
	  (f1:ID 				{#pob.addChild(#f1);}
	  )+ 
	  f2:program  			{#pob.addChild(#f2);}
	| (FLECHA | FLECHAOC) 	{#pob=#[LOCALVAR]; }
	  (r1:ID 				{#pob.addChild(#r1);}
	  )+ 
	  r2:infixExpression	{#pob.addChild(#r2);}
	| "IF" i1:seqOb
	  "THEN" i2:seqOb
	  ( "END"				{#pob=#(#[IF],#i1,#i2);}
	   |"ELSE"
	    i3:seqOb
	    "END"				{#pob=#(#[IFELSE],#i1,#i2,#i3);}
	  )
	| "IFERR" e1:seqOb
	  "THEN" e2:seqOb
	  "END"					{#pob=#(#[IFERR],#e1,#e2);}
	| "DO" d1:seqOb
	  "UNTIL" d2:seqOb
	  "END"					{#pob=#(#[DOUNTIL],#d1,#d2);}
	| "WHILE" w1:seqOb
	  "REPEAT" w2:seqOb
	  "END"					{#pob=#(#[WHILEREPEAT],#w1,#w2);}
;

specialCommand :
	 TOUNIT
	|VTO
	|TOV2
	|TOV3
	|DTOR
	|RTOD
	|RTOB
	|BTOR
	|OBJTO
	|EQTO
	|TOARRY
	|TOLIST
	|TOSTR
	|TOTAG
	|RTOC
	|CTOR
	|FSINT
	|FCINT
	|FSINTC
	|FCINTC
;

class ObjectLexer extends Lexer;
options {
	k=3;
}

WS	:	(' '
	|	'\t'
	|	'\n'
	|	'\r')
		{ _ttype = Token.SKIP; }
	;


protected
ESC	:	'\\'
		(	'n'
		|	'r'
		|	't'
		|	'b'
		|	'f'
		|	'"'
		|	'\''
		|	'\\'
		|	('0'..'3')
			(
				options {
					warnWhenFollowAmbig = false;
				}
			:	('0'..'9')
				(	
					options {
						warnWhenFollowAmbig = false;
					}
				:	'0'..'9'
				)?
			)?
		|	('4'..'7')
			(
				options {
					warnWhenFollowAmbig = false;
				}
			:	('0'..'9')
			)?
		)
	;

// string literals
STRING_LITERAL
	:	'"' (ESC|~('"'|'\\'))* '"'
	;

protected DIGIT: '0'..'9';
protected HEXCHAR: (DIGIT|'A'..'F'|'a'..'f');
protected OCTCHAR: ('0'..'7');

NUMBER : 
	('-')? (DIGIT)+ (('.') (DIGIT)+)?  ('E' ('-')? DIGIT (DIGIT (DIGIT)?)?)?
;

INTNUMBER : 
	  ALMOA (DIGIT)+   ('d'|'D')
	| ALMOA (HEXCHAR)+ ('h'|'H')
	| ALMOA (OCTCHAR)+ ('o'|'O')
;

ID options { testLiterals = true; }
	:	(  'a'..'z'
	      |'A'..'Z'
	    )
	    ( 'a'..'z'
	     |'A'..'Z'
	     |'0'..'9'
	    )*
	| "�F"
	| "�C"
	| "�R"
	| "�"
	| SQRT
	| PI
	;
LPAREN:	  '(' ;
RPAREN:	  ')' ;
CARET:    '^' ;
MULT:     '*' ; 
DIV:      '/' ;
PLUS:     '+' ;
MINUS:    '-' ;
ASSIGN:   '=' ;
COMMA :   ',' ;
ALMOA:    '#' ;
LLAA:     '{' ;
LLAC:     '}' ;
CORO:     '[' ;   
CORC:     ']' ;
SCOMI:	  '\'';
UNDERSCO: '_' ;
DPUNTOS : ':' ;
LT      : '<' ;
VBAR    : '|' ;
LE      : "<=";
EQ      : "==";
GE      : ">=";
GT      : '>';
OPENPROG: "<<";
CLOSEPROG:">>";
FLECHA:   "->";
TOUNIT:	  "->UNIT";
VTO:	  "V->";
TOV2:	  "->V2";
TOV3:	  "->V3";
DTOR:	  "D->R";
RTOD:	  "R->D";
RTOB:	  "R->B";
BTOR:	  "B->R";
OBJTO:	  "OBJ->";
EQTO:	  "EQ->";
TOARRY:	  "->ARRY";
TOLIST:	  "->LIST";
TOSTR:	  "->STR";
TOTAG:	  "->TAG";
RTOC:	  "R->C";
CTOR:	  "C->R";
FSINT:	  "FS?";
FCINT:	  "FC?";
FSINTC:	  "FS?C";
FCINTC:	  "FC?C";

ANGLE:	  '\u0080';
SQRT:     '\u0083';
INTEG:    '\u0084';
SIGMA:	  '\u0085';
PI:       '\u0087';
DERIV:	  '\u0088';
LEOC:     '\u0089';
GEOC:     '\u008A';
DISTI:	  '\u008B';
FLECHAOC: '\u008D';
CLOSEPROGOC: '\u00BB';
OPENPROGOC:  '\u00AB';


	