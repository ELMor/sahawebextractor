package f8.commands.units;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.TempUnit;
import f8.objects.types.Unit;

public final class CONVERT extends NonAlgebraic {
	public CONVERT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 104;
	}

	public Storable getInstance() {
		return new CONVERT();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("CONVERT");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(2)) {
			Stackable a = cl.peek(1);
			Stackable b = cl.peek(0);
			if ((a instanceof Unit || a instanceof TempUnit)
					&& (b instanceof Unit || b instanceof TempUnit)) {
				Unit from = (Unit) a;
				Unit to = (Unit) b;
				Unit converted = from.convert(to);
				cl.pop(2);
				cl.push(converted);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}
}
