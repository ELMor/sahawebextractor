package f8.commands.units;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.TempUnit;
import f8.objects.types.Unit;

public final class UBASE extends NonAlgebraic implements ObjectParserTokenTypes {
	public UBASE() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 106;
	}

	public Storable getInstance() {
		return new UBASE();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("UBASE");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			Stackable u = cl.peek();
			if (u instanceof Unit || u instanceof TempUnit) {
				cl.pop();
				Unit uu = (Unit) u;
				cl.push(uu.ubase());
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

}
