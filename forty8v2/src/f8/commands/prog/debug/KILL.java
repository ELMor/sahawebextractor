/*
 * Created on 06-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.CL;
import f8.Ref;
import f8.commands.Operation;
import f8.exceptions.F8Exception;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class KILL extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		while (cl.getProcDebug() != null)
			;
	}

}
