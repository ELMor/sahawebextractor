/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.CL;
import f8.CalcGUI;
import f8.Ref;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class SST_INTO extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CalcGUI cg = Ref.ref;
		CL cl = cg.getCalcLogica();
		Debuggable dbg = cl.getProcDebug();
		if (dbg != null) {
			cl.setProcDebug(dbg);
			dbg.execDebug(true);
			cg.refresh(false);
			if (!dbg.isAnyMore()) {
				cl.getProcDebug();
				cg.temporaryLabels(null, "(returning...)");
			} else {
				cg.temporaryLabels(null, dbg.nextCommand());
			}

		} else {
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
