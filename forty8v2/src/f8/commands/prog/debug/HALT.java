/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.DataStream;
import f8.Storable;
import f8.commands.Command;
import f8.exceptions.DebugHaltException;
import f8.exceptions.F8Exception;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class HALT extends Command {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getID()
	 */
	public int getID() {
		return 2987;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#loadState(f8.platform.DataStream)
	 */
	public void loadState(DataStream ds) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#saveState(f8.platform.DataStream)
	 */
	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		throw new DebugHaltException(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "HALT";
	}

}
