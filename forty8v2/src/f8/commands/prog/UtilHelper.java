/*
 * Created on 22-ago-2003
 *
 
 
 */
package f8.commands.prog;

import f8.CL;
import f8.MathKernel;
import f8.Ref;
import f8.UtilFactory;
import f8.commands.Command;
import f8.commands.prog.brch.IF;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Double;
import f8.objects.types.Int;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class UtilHelper {
	static MathKernel mk = UtilFactory.getMath();

	static CL cl = Ref.ref.getCalcLogica();

	public static int evalCondition() throws F8Exception {
		if (cl.size() == 0) {
			throw new TooFewArgumentsException(new IF(null));
		}

		Command c = cl.pop();

		if ((c instanceof Int || c instanceof Double)
				&& (mk.toDouble(c.toString()) == 0)) {
			return 0;
		}

		return 1;
	}
}
