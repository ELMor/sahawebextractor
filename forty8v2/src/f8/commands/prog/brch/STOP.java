package f8.commands.prog.brch;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;

public final class STOP extends NonAlgebraic {
	public STOP() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 47;
	}

	public Storable getInstance() {
		return new STOP();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() {
		CL cl = Ref.ref.getCalcLogica();
		cl.pause();
	}

	public String toString() {
		return ("STOP");
	}
}
