/*
 * Created on 28-ago-2003
 *
 
 
 */
package f8.commands.symb;

import antlr.collections.AST;
import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.commands.math.mkl;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.InfixExp;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class COLECT extends Dispatch1 implements ObjectParserTokenTypes {
	/**
	 * 
	 */
	public COLECT() {
		super();
		// To-Do Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		// To-Do Auto-generated method stub
		return 100;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		// To-Do Auto-generated method stub
		return new COLECT();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {
		// To-Do Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
		// To-Do Auto-generated method stub
	}

	public static AST colect(AST nodo) {
		nodo = colectChildsOf(nodo);
		AST child = nodo.getFirstChild();
		switch (nodo.getType()) {
		case PLUS:
			return mkl.singleSum(nodo);
		case MINUS:
			return mkl.res(mkl.ccc(child), mkl.ccc(child.getNextSibling()));
		case MULT:
			return mkl.singleMul(nodo);
		case DIV:
			return mkl.div(mkl.ccc(child), mkl.ccc(child.getNextSibling()));
		case CARET:
			return mkl.ele(mkl.ccc(child), mkl.ccc(child.getNextSibling()));
		case ID:
			if (nodo.getFirstChild() != null) {
				return mkl.fun(nodo.getText(), nodo.getFirstChild());
			}
		}
		return nodo;
	}

	public static AST colectChildsOf(AST nodo) {
		AST child = nodo.getFirstChild();
		while (child != null) {
			AST childColected = colect(child);
			if (child != childColected) {
				mkl.subst(nodo, child, childColected);
			}
			child = child.getNextSibling();
		}
		return nodo;
	}

	public String toString() {
		return "COLECT";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInfix(f8.kernel.types.InfixExp)
	 */
	public Stackable prfInfix(InfixExp a) throws F8Exception {
		AST tree = mkl.copyAST(a.getAST());
		tree = colect(tree);
		return new InfixExp(tree);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
