package f8.commands.alg;

import antlr.collections.AST;
import f8.CL;
import f8.CalcGUI;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.Command;
import f8.commands.prog.debug.Debuggable;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;
import f8.objects.types.Array;
import f8.objects.types.Double;

public class CommandSequence extends Array implements Debuggable {
	protected Vector obList;

	protected int iP = -1;

	public CommandSequence(AST def) {
		if (def == null) {
			return;
		}

		obList = UtilFactory.newVector();
		int sz = def.getNumberOfChildren();
		AST child = def.getFirstChild();

		for (int i = 0; i < sz; i++) {
			obList.add(Command.createFromAST(child));
			child = child.getNextSibling();
		}
	}

	public CommandSequence(Vector a) {
		this.obList = a;
	}

	public CommandSequence(Command a) {
		this.obList = UtilFactory.newVector();
		obList.add(a);
	}

	public Stackable copia() {
		Vector copia = UtilFactory.newVector();
		for (int i = 0; i < obList.size(); i++) {
			if (obList.elementAt(i) instanceof Stackable) {
				// Hay que copiar el stackable
				copia.add(((Stackable) obList.elementAt(i)).copia());
			} else {
				// Se a�ade simplemente debido a que no tiene inf. de estado
				copia.add(obList.elementAt(i));
			}
		}
		return new CommandSequence(copia);
	}

	public String getTypeName() {
		return "GArray";
	}

	public int getID() {
		return 20;
	}

	public Storable getInstance() {
		return new CommandSequence((AST) null);
	}

	public void loadState(DataStream ds) {
		super.loadState(ds);
		int sz = ds.readInt();
		obList = UtilFactory.newVector();

		for (int i = 0; i < sz; i++) {
			obList.add(Command.loadFromStorage(ds));
		}
	}

	public void saveState(DataStream ds) {
		super.saveState(ds);
		ds.writeInt(obList.size());

		for (int i = 0; i < obList.size(); i++) {
			Command c = (Command) (obList.elementAt(i));
			ds.writeInt(c.getID());
			c.saveState(ds);
		}
	}

	public void exec() throws F8Exception {
		CalcGUI cg = Ref.ref;
		CL cl = cg.getCalcLogica();
		for (iP = 0; iP < obList.size(); iP++) {
			Command ea = (Command) obList.elementAt(iP);
			if (ea != null) {
				ea.exec();
			}
		}
	}

	public boolean execDebug(boolean si) throws F8Exception {
		if ((iP + 1) < obList.size()) {
			Command task = (Command) obList.elementAt(++iP);
			try {
				Debuggable dtask = (Debuggable) task;
				if (!dtask.execDebug(si)) {
					iP--;
				}
			} catch (ClassCastException e) {
				task.exec();
			}
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#nextCommand()
	 */
	public String nextCommand() {
		String ret = "";
		Debuggable dtask = null;
		if (iP == -1) {
			ret = createNextString("");
		} else if ((iP + 1) < obList.size()) {
			ret = createNextString("");
		} else {
			ret = ">> ";
		}
		return ret;
	}

	private String createNextString(String ret) {
		Debuggable dtask;
		for (int i = iP + 1; (((iP + 1) - i) < 4) && (i < obList.size()); i++) {
			try {
				dtask = (Debuggable) obList.elementAt(i);
				ret += (dtask.nextCommand() + " ");
			} catch (ClassCastException e) {
				ret += (obList.elementAt(i).toString() + " ");
			}
		}
		return ret;
	}

	public int size() {
		return (obList.size());
	}

	public void put(Command v, int n) {
		obList.setElementAt(v, n);
	}

	public Command get(int n) throws IndexRangeException {
		try {
			return (Command) (obList.elementAt(n));
		} catch (Exception e) {
			throw new IndexRangeException(this);
		}
	}

	public String toString() {
		if(obList==null)
			return "";
		
		String k = ((obList.size() == 0) ? "" : obList.elementAt(0).toString());

		for (int i = 1; i < obList.size(); i++) {
			k += (" " + obList.elementAt(i));
		}

		return k;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		for (int i = 0; i < obList.size(); i++) {
			try {
				cl.push((Stackable) obList.elementAt(i));
			} catch (ClassCastException e) {
				cl.push(new CommandSequence((Command) obList.elementAt(i)));
			}
		}
		cl.push(new Double(obList.size()));
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#isAnyMore()
	 */
	public boolean isAnyMore() {
		return iP < obList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof CommandSequence) {
			CommandSequence cobj = (CommandSequence) obj;
			for (int i = 0; i < obList.size(); i++) {
				if (!obList.elementAt(i).equals(cobj.obList.elementAt(i))) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}
}
