package f8.commands.alg.vectr;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;

public final class FLECHAV3 extends Command {
	public FLECHAV3() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3042;
	}

	public Storable getInstance() {
		return new FLECHAV3();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("\u008DV3");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(3)) {
			Stackable a, b, c;
			a = cl.peek(2);
			b = cl.peek(1);
			c = cl.peek(0);
			if (a instanceof Double && b instanceof Double
					&& c instanceof Double) {
				cl.pop(3);
				double[] d = new double[3];
				d[0] = ((Double) a).x;
				d[1] = ((Double) b).x;
				d[2] = ((Double) c).x;
				cl.push(new F8Vector(d));
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

}
