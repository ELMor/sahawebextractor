package f8.commands.alg.vectr;

import f8.DataStream;
import f8.Storable;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.Dispatch1;
import f8.commands.alg.CommandSequence;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;

public final class VFLECHA extends Dispatch1 {
	public VFLECHA() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3040;
	}

	public Storable getInstance() {
		return new VFLECHA();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("V\u008D");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch1#prfVector(f8.types.F8Vector)
	 */
	public Stackable prfVector(F8Vector x) throws F8Exception {
		Vector v = UtilFactory.newVector();
		CommandSequence res = new CommandSequence(v);
		for (int i = 0; i < x.size(); i++)
			v.add(new Double(x.x[i]));
		return res;
	}

}
