package f8.commands.alg;

public interface DoubleValue {
	public double doubleValue();
}
