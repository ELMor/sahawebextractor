package f8.commands.alg.matrx;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.alg.DoubleValue;
import f8.commands.alg.IntValue;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Matrix;

public final class RowScale extends NonAlgebraic {
	public RowScale() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 71;
	}

	public Storable getInstance() {
		return new RowScale();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	// m i c rowscale
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(3)) {
			Stackable M = cl.peek(2);
			Command I = cl.peek(1);
			Command C = cl.peek(0);

			if (M instanceof Matrix && I instanceof IntValue
					&& C instanceof DoubleValue) {
				Matrix m = (Matrix) M;
				int i = ((IntValue) I).intValue();
				double c = ((DoubleValue) C).doubleValue();
				cl.pop(3);

				for (int k = 0; k < m.c; k++) {
					m.x[i][k] *= c;
				}

				cl.push(M);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROWSCALE");
	}
}
