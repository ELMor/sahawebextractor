package f8.commands.alg.matrx;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.commands.alg.IntValue;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Matrix;

public final class RowSwap extends NonAlgebraic {
	public RowSwap() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 79;
	}

	public Storable getInstance() {
		return new RowSwap();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	// m i j rowswap
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(3)) {
			Stackable a = cl.peek(2);
			Stackable b = cl.peek(1);
			Stackable c = cl.peek(0);

			if (a instanceof Matrix && b instanceof IntValue
					&& c instanceof IntValue) {
				Matrix A = (Matrix) a;
				int i = ((IntValue) b).intValue();
				int j = ((IntValue) c).intValue();

				if ((0 <= i) && (i < A.r) && (0 <= j) && (j < A.r)) {
					cl.pop(3);

					double[] t = A.x[i];
					A.x[i] = A.x[j];
					A.x[j] = t;
					cl.push(a);
				} else {
					throw new IndexRangeException(this);
				}
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROWSWAP");
	}
}
