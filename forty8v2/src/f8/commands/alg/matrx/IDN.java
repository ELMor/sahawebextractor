package f8.commands.alg.matrx;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.exceptions.InvalidDimensionException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Matrix;

public final class IDN extends Dispatch1 {
	public IDN() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3033;
	}

	public Storable getInstance() {
		return new IDN();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("IDN");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfMatrix(f8.kernel.types.Matrix)
	 */
	public Stackable prfMatrix(Matrix a) throws F8Exception {
		Matrix M = (Matrix) a;
		int r = M.rowNumber();
		int c = M.columnNumber();

		if (r == c) {
			return new Double(det(M.x));
		} else {
			throw new InvalidDimensionException(this);
		}
	}

	public static double det(double[][] matrix) {
		double[][] m = MatrixOperations.matrixCopy(matrix);
		int R = m.length;
		int C = R;
		int r = 0;
		int c = 0;
		double d = 1;

		while ((r < R) && (c < C)) {
			// find pivot in column c below row r
			int p = R;
			double max = 0;
			double x;

			for (int i = r; i < R; i++) {
				if ((x = Math.abs(m[i][c])) > max) {
					max = x;
					p = i;
				}
			}

			if (p < R) {
				// pivot
				if (p != r) {
					double[] y = m[p];
					m[p] = m[r];
					m[r] = y;
					d = -d;
				}

				double z = m[r][c];
				d *= z;
				m[r][c] = 1.0;

				for (int j = c + 1; j < m[r].length; j++) {
					m[r][j] /= z;
				}

				for (int i = r + 1; i < R; i++) {
					x = m[i][c];
					m[i][c] = 0.0;

					for (int j = c + 1; j < m[i].length; j++) {
						m[i][j] -= (x * m[r][j]);
					}
				}

				r++;
				c++;
			} else {
				return (0);
			}
		}

		return (d);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
