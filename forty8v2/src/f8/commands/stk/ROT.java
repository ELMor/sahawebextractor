package f8.commands.stk;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;

public final class ROT extends NonAlgebraic {
	public ROT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 12;
	}

	public Storable getInstance() {
		return new ROT();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(3)) {
			Stackable c = cl.pop();
			Stackable b = cl.pop();
			Stackable a = cl.pop();
			cl.push(b);
			cl.push(c);
			cl.push(a);
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROT");
	}
}
