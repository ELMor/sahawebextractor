package f8.commands.stk;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

public final class DROP extends NonAlgebraic {
	public DROP() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 8;
	}

	public Storable getInstance() {
		return new DROP();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			cl.pop(1);
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("DROP");
	}
}
