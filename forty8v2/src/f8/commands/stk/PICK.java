package f8.commands.stk;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.FunctionDomainException;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Double;

public final class PICK extends NonAlgebraic {
	public PICK() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 16;
	}

	public Storable getInstance() {
		return new PICK();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			Command pick = cl.pop();
			if (pick instanceof Double) {
				int pck = (int) ((Double) pick).x;
				if (pck < 1) {
					throw new FunctionDomainException(this);
				}
				if (cl.check(pck)) {
					cl.push(cl.peek(pck - 1));
				} else {
					throw new TooFewArgumentsException(this);
				}
			} else {
				throw new TooFewArgumentsException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("PICK");
	}
}
