/*
 * Created on 07-ago-2003
 *
 
 
 */
package f8.commands.storage;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.keyboard.CF;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class UPDIR extends NonAlgebraic {
	public UPDIR() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 123;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new UPDIR();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {
	}

	public void exec() {
		CL cl = Ref.ref.getCalcLogica();
		cl.upDir();
		CF cf = Ref.ref.getCalcFisica();
		Ref.ref.refreshProgMenu();
	}

	public String toString() {
		return "UPDIR";
	}
}
