/*
 * Created on 07-ago-2003
 *
 
 
 */
package f8.commands.storage;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Directory;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class CRDIR extends NonAlgebraic {
	public CRDIR() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 125;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new CRDIR();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();

		if (cl.check(1)) {
			Command a = cl.peek(0);
			String name;

			// define a to be b
			if (a instanceof Literal) {
				name = a.toString();
				cl.pop(1);
				createDir(cl, name);
			} else if ((name = InfixExp.isLiteral(a)) != null) {
				cl.pop(1);
				createDir(cl, name);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public static void createDir(CL cl, String name) {
		new Directory(cl.currentDict(), name);
		Ref.ref.refreshProgMenu();
	}

	public String toString() {
		return "CRDIR";
	}
}
