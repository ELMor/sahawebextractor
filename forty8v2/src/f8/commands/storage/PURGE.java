package f8.commands.storage;

import f8.CL;
import f8.DataStream;
import f8.Hashtable;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.NonEmptyDirException;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Directory;
import f8.objects.types.InfixExp;
import f8.objects.types.Lista;
import f8.objects.types.Literal;

public final class PURGE extends NonAlgebraic {
	public PURGE() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 510;
	}

	public Storable getInstance() {
		return new PURGE();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();

		if (cl.check(1)) {
			Command a = cl.peek(0);
			String name;
			if (a instanceof InfixExp
					&& ((name = InfixExp.isLiteral(a)) != null)) {
				cl.pop();
				purgeVar(cl, name);
			} else if (a instanceof Lista) {
				cl.pop();
				Lista la = (Lista) a;
				for (int i = 0; i < la.size(); i++) {
					if (la.get(i) instanceof Literal) {
						name = ((Literal) la.get(i)).nombre;
						Hashtable itemHolder = cl.currentDict().getHT();
						if ((Stackable) itemHolder.get(name) instanceof Directory) {
							throw new NonEmptyDirException(this);
						} else {
							itemHolder.remove(name);
						}
					} else {
						throw new BadArgumentTypeException(this);
					}
				}
				Ref.ref.refreshProgMenu();
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public static void purgeVar(CL cl, String name) throws NonEmptyDirException {
		Hashtable itemHolder = cl.currentDict().getHT();
		if ((Stackable) itemHolder.get(name) instanceof Directory) {
			throw new NonEmptyDirException(new PURGE());
		} else {
			itemHolder.remove(name);
		}
		Ref.ref.refreshProgMenu();
	}

	public String toString() {
		return ("PURGE");
	}
}
