package f8.commands;

import f8.exceptions.F8Exception;

public abstract class Operation {
	/**
	 * Comportamiento por defecto de exec es un push
	 * 
	 */
	public abstract void exec() throws F8Exception;

}
