package f8.commands.compare;

import f8.DataStream;
import f8.Storable;

public final class LE extends Comparison {
	public LE() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 85;
	}

	public Storable getInstance() {
		return new LE();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("<=");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.ovl.Comparison#compare(double, double)
	 */
	public boolean compare(double v1, double v2) {
		return v1 <= v2;
	}

}
