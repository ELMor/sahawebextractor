package f8.commands.math.trig;

import antlr.collections.AST;
import f8.DataStream;
import f8.Storable;
import f8.commands.math.Trigonometric;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;

public final class ASIN extends Trigonometric {
	public ASIN() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public int getID() {
		return 54;
	}

	public Storable getInstance() {
		return new ASIN();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public Stackable prfDouble(Double x) {
		if ((x.x < -1) || (x.x > 1)) {
			return new Complex(x.x, 0).asin();
		}
		return new Double(RAD2Settings(Math.asin(x.x)));
	}

	public String toString() {
		return ("ASIN");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		return mkl.mul(DER.deriveFunction(args, var), mkl.div(mkl.num(1), mkl
				.ele(mkl.res(mkl.num(1), mkl.ele(args, mkl.num(2))), mkl
						.num(0.5))));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public String undo() {
		return "SIN";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.asin();
	}

}
