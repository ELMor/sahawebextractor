package f8.commands.math.simple;

import antlr.collections.AST;
import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch2;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.exceptions.InvalidDimensionException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.objects.types.Matrix;
import f8.objects.types.Unit;

public final class TIMES extends Dispatch2 implements Derivable {
	public TIMES() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 91;
	}

	public Storable getInstance() {
		return new TIMES();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("*");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		AST a1;
		AST a2;
		AST a3;
		a1 = args;
		a2 = a1.getNextSibling();
		a3 = a2.getNextSibling();
		if (a3 == null) {
			return mkl.sum(mkl.mul(DER.deriveFunction(a1, var), a2), mkl.mul(
					a1, DER.deriveFunction(a2, var)));
		} else {
			return mkl.sum(mkl.mul(DER.deriveFunction(a1, var), mkl.opSiblings(
					ObjectParserTokenTypes.MULT, "*", a2)), mkl.mul(a1,
					deriveWithArgs(a2, var)));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfIntInt(f8.kernel.types.Int,
	 *      f8.kernel.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		return new Int(a.n * b.n);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		return new Double(a.x * b.x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfComplexComplex(f8.kernel.types.Complex,
	 *      f8.kernel.types.Complex)
	 */
	public Stackable prfComplexComplex(Complex a, Complex b) throws F8Exception {
		double[] x = a.complexValue();
		double[] y = b.complexValue();
		return new Complex((x[0] * y[0]) - (x[1] * y[1]), (x[0] * y[1])
				+ (x[1] * y[0]));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8VectorDouble(f8.kernel.types.F8Vector,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfF8VectorDouble(F8Vector a, Double b) throws F8Exception {
		int nx = a.x.length;
		double[] z = new double[nx];

		for (int i = 0; i < nx; i++) {
			z[i] = a.x[i] * b.x;
		}

		return (new F8Vector(z));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleF8Vector(f8.kernel.types.Double,
	 *      f8.kernel.types.F8Vector)
	 */
	public Stackable prfDoubleF8Vector(Double a, F8Vector b) throws F8Exception {
		return prfF8VectorDouble(b, a);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8VectorF8Vector(f8.kernel.types.F8Vector,
	 *      f8.kernel.types.F8Vector)
	 */
	public Stackable prfF8VectorF8Vector(F8Vector a, F8Vector it)
			throws F8Exception {
		double[] y = it.x;
		int nx = a.x.length;
		int ny = y.length;

		if (nx == ny) {
			double z = 0.0;

			for (int i = 0; i < nx; i++) {
				z += (a.x[i] * y[i]);
			}

			return (new Double(z));
		} else {
			throw new InvalidDimensionException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfMatrixDouble(f8.kernel.types.Matrix,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfMatrixDouble(Matrix a, Double b) throws F8Exception {
		double y = b.doubleValue();
		double[][] z = new double[a.r][a.c];

		for (int i = 0; i < a.r; i++) {
			for (int j = 0; j < a.c; j++) {
				z[i][j] = a.x[i][j] * y;
			}
		}

		return (new Matrix(z));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfMatrixF8Vector(f8.kernel.types.Matrix,
	 *      f8.kernel.types.F8Vector)
	 */
	public Stackable prfMatrixF8Vector(Matrix a, F8Vector it)
			throws F8Exception {
		if (a.c == it.x.length) {
			double[] y = it.x;
			double[] z = new double[a.r];

			for (int i = 0; i < a.r; i++) {
				double s = 0.0;

				for (int j = 0; j < a.c; j++) {
					s += (a.x[i][j] * y[j]);
				}

				z[i] = s;
			}

			return (new F8Vector(z));
		} else {
			throw new InvalidDimensionException(new TIMES());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfMatrixMatrix(f8.kernel.types.Matrix,
	 *      f8.kernel.types.Matrix)
	 */
	public Stackable prfMatrixMatrix(Matrix a, Matrix it) throws F8Exception {
		if (a.c == it.r) {
			double[][] y = it.x;
			double[][] z = new double[a.r][it.c];

			for (int i = 0; i < a.r; i++) {
				for (int k = 0; k < it.c; k++) {
					double s = 0.0;

					for (int j = 0; j < a.c; j++) {
						s += (a.x[i][j] * y[j][k]);
					}

					z[i][k] = s;
				}
			}

			return (new Matrix(z));
		} else {
			throw new InvalidDimensionException(new TIMES());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleUnit(f8.kernel.types.Double,
	 *      f8.kernel.types.Unit)
	 */
	public Stackable prfDoubleUnit(Double a, Unit b) throws F8Exception {
		return new Unit(a.x * b.getValor(), b.getUnidad());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfUnitDouble(f8.kernel.types.Unit,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfUnitDouble(Unit a, Double b) throws F8Exception {
		return prfDoubleUnit(b, a);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfUnitUnit(f8.kernel.types.Unit,
	 *      f8.kernel.types.Unit)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		double prod = ((Unit) a).getValor() * ((Unit) b).getValor();
		AST nun = mkl.mul(((Unit) a).getUnidad(), ((Unit) b).getUnidad());
		return new Unit(prod, nun);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfixOperator()
	 */
	public boolean isInfixOperator() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpInfixExp(f8.types.InfixExp,
	 *      f8.types.InfixExp)
	 */
	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b)
			throws F8Exception {
		return new InfixExp(mkl.mul(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfComplexInfixExp(f8.types.Complex, f8.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
			throws F8Exception {
		return new InfixExp(mkl.mul(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleInfixExp(f8.types.Double, f8.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b) throws F8Exception {
		return new InfixExp(mkl.mul(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpComplex(f8.types.InfixExp, f8.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
			throws F8Exception {
		return new InfixExp(mkl.mul(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpDouble(f8.types.InfixExp, f8.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b) throws F8Exception {
		return new InfixExp(mkl.mul(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfComplexDouble(f8.types.Complex, f8.types.Double)
	 */
	public Stackable prfComplexDouble(Complex a, Double b) throws F8Exception {
		return prfDoubleComplex(b, a);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleComplex(f8.types.Double, f8.types.Complex)
	 */
	public Stackable prfDoubleComplex(Double a, Complex b) throws F8Exception {
		return b.mul(a.x);
	}

}
