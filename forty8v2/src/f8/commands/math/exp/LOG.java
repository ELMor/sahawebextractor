package f8.commands.math.exp;

import antlr.collections.AST;
import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;

public final class LOG extends Dispatch1 implements Derivable {
	public LOG() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 76;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public Storable getInstance() {
		return new LOG();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public Stackable prfDouble(Double x) {
		if (x.x < 0) {
			return new Complex(x.x, 0).log().div(new Complex(Math.log(10), 0));
		}
		return new Double(Math.log(x.x) / Math.log(10));
	}

	public String toString() {
		return ("LOG");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST arg, String var) throws F8Exception {
		return mkl.div(DER.deriveFunction(arg, var), mkl.mul(arg, mkl
				.num(1 / Math.log(Math.E))));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public String undo() {
		return "ALOG";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.log().div(new Complex(10, 0));
	}

}
