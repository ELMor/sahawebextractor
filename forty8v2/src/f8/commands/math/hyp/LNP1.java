package f8.commands.math.hyp;

import antlr.collections.AST;
import f8.DataStream;
import f8.Storable;
import f8.commands.math.Trigonometric;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;

public final class LNP1 extends Trigonometric {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public int getID() {
		return 3031;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public Storable getInstance() {
		return new LNP1();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public Stackable prfDouble(Double x) {
		return new Complex(x.x + 1, 0).log().mutate();
	}

	public String toString() {
		return ("LNP1");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		return mkl.res(mkl.num(0), mkl.mul(DER.deriveFunction(args, var), mkl
				.div(mkl.num(1), mkl.ele(mkl.res(mkl.num(1), mkl.ele(args, mkl
						.num(2))), mkl.num(0.5)))));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return new Complex(x.re + 1, x.im).log().mutate();
	}

}
