package f8.commands.math.prob;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;

public final class FACTORIAL extends Dispatch1 {
	public FACTORIAL() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3021;
	}

	public String toString() {
		return ("!");
	}

	public Storable getInstance() {
		return new FACTORIAL();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		int n = (int) a.x;
		if (n == 0 || n == 1)
			return new Double(1);
		double ret = 1;
		for (int j = 2; j < n; j++)
			ret *= j;
		return new Double(ret);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
