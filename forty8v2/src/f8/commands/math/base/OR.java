package f8.commands.math.base;

import f8.DataStream;
import f8.Storable;

public final class OR extends Logical2 {
	public OR() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 38;
	}

	public Storable getInstance() {
		return new OR();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("OR");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.boo.Logical2#logic(double, double)
	 */
	public double logic(double l, double r) {
		return l + r == 0 ? 0 : 1;
	}

}
