/*
 * Created on 01-oct-2003
 *
 
 
 */
package f8.commands.math.base;

import f8.DataStream;
import f8.Storable;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class XOR extends Logical2 {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.boo.Logical2#logic(double, double)
	 */
	public double logic(double l, double r) {
		if (l == 0 && r != 0)
			return 1;
		if (l != 0 && r == 0)
			return 1;
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 1532;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "XOR";
	}

}
