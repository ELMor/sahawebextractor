package f8.commands.math.base;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;

public final class NOT extends Dispatch1 {
	public NOT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 37;
	}

	public Storable getInstance() {
		return new NOT();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("NOT");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double x) throws F8Exception {
		if (x.x == 0)
			return new Double(1);
		return new Double(1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
