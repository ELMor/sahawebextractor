package f8.commands.math.parts;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.commands.alg.IntValue;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;
import f8.objects.types.Int;

public final class ABS extends Dispatch1 {
	public ABS() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 80;
	}

	public Storable getInstance() {
		return new ABS();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	double length(double[] x) {
		double max = 0;

		for (int i = 0; i < x.length; i++) {
			double m = Math.abs(x[i]);

			if (m > max) {
				max = m;
			}
		}

		if (max == 0.0) {
			return (max);
		}

		double r = 0;

		for (int i = 0; i < x.length; i++) {
			double y = x[i] / max;
			r += (y * y);
		}

		return (max * Math.sqrt(r));
	}

	public String toString() {
		return ("ABS");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int a) throws F8Exception {
		int A = ((IntValue) a).intValue();

		if (A < 0) {
			A = -A;
		}
		return new Int(A);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		double x = a.doubleValue();

		if (x < 0) {
			x = -x;
		}

		return new Double(x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex a) throws F8Exception {
		double[] x = a.complexValue();
		double r = length(x);
		return new Double(r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfVector(f8.platform.utl.Vector)
	 */
	public Stackable prfVector(F8Vector a) throws F8Exception {
		return new Double(length(a.x));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
