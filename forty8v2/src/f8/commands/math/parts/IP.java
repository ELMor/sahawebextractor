package f8.commands.math.parts;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;

public final class IP extends Dispatch1 {
	public IP() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3012;
	}

	public String toString() {
		return ("IP");
	}

	public Storable getInstance() {
		return new IP();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		return new Double((int) a.x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
