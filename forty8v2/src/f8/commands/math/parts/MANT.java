package f8.commands.math.parts;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;

public final class MANT extends Dispatch1 {
	public MANT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3010;
	}

	public String toString() {
		return ("MANT");
	}

	public Storable getInstance() {
		return new MANT();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		int e = (int) (Math.log(a.x) / Math.log(10));
		return new Double(a.x / (Math.pow(10, e)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
