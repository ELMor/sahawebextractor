package f8.commands.math.parts;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.commands.alg.IntValue;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Int;

public final class CONJ extends Dispatch1 {
	public CONJ() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3002;
	}

	public Storable getInstance() {
		return new CONJ();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("CONJ");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int a) throws F8Exception {
		int A = ((IntValue) a).intValue();

		if (A < 0) {
			A = -A;
		}
		return new Int(A);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		return new Double(a.x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex a) throws F8Exception {
		return new Complex(a.re, -a.im);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
