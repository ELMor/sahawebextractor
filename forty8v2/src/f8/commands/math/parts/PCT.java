package f8.commands.math.parts;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch2;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Int;

public final class PCT extends Dispatch2 {
	public PCT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3007;
	}

	public String toString() {
		return ("%");
	}

	public Storable getInstance() {
		return new PCT();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		return new Double(a.x / 100 * b.x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleInt(f8.types.Double, f8.types.Int)
	 */
	public Stackable prfDoubleInt(Double a, Int b) throws F8Exception {
		return new Double(a.x / 100 * b.n);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfIntDouble(f8.types.Int, f8.types.Double)
	 */
	public Stackable prfIntDouble(Int a, Double b) throws F8Exception {
		return new Double(a.n / 100 * b.x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfIntInt(f8.types.Int, f8.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		return new Double(a.n / 100 * b.n);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
