package f8.exceptions;

public interface ICalcErr {
	int Underflow = 0;

	int Overflow = 1;

	int Type = 2;

	int Generic = 3;

	int Undefined = 4;

	int InvalidDim = 5;

	int Range = 6;

	int BadArgumentType = 7;

	int Vector = 8;

	int FunctionDomain = 9;

	int ZeroDiv = 10;

	int ExecUnderflow = 11;

	int NoCurrentPoint = 12;

	int FileNotFound = 13;

	int TooFewArguments = 14;

	int InconsistentUnits = 15;

	int UnmatchedDims = 16;

	int NonEmptyDir = 17;

	int CircularReference = 18;

	int Parse = 19;

	int InvalidUserFun = 20;

	int WrongNumberOfArg = 21;

	int BadArgumentValue = 22;

	int Halt = 23;

	int NoCurrentEQ = 24;

	int Unimplemented = 25;

	int End = 1000;

	String[] Name = { "Stack underflow\n", "Overflow\n", "Invalid type\n",
			"Generic\n", "Undefined\n", "Invalid dimension\n",
			"Index out of range\n", "Bad Argument Type\n",
			"Invalid vector entry\n", "Argument out of range\n",
			"Division by zero\n", "Execution underflow\n",
			"No current point\n", "File not found\n", "Too few Arguments\n",
			"Inconsistent Units\n", "Not valid dimensions\n",
			"Non Empty Directory\n", "Circular Reference",
			"Parser does not recognize", "Invalid User Function",
			"Wrong Number of Arguments", "Bad Argument Value", "Halt",
			"No Current EQ!", "Not yet implemented!", "End\n" };
}
