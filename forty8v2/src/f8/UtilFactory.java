/*
 * Created on 01-ago-2003
 *
 
 
 */
package f8;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class UtilFactory {
	private static Class VFac = null;

	private static Class HFac = null;

	private static Class SFac = null;

	private static Class mathClass = null;

	public static void initUF(Class vf, Class hf, Class sf, Class ma) {
		VFac = vf;
		HFac = hf;
		SFac = sf;
		mathClass = ma;
	}

	public static Vector newVector() {
		if (VFac == null) {
			return null;
		}
		try {
			return (Vector) VFac.newInstance();
		} catch (IllegalAccessException e) {
			// e.printStackTrace();
		} catch (InstantiationException e) {
			// e.printStackTrace();
		}
		return null;
	}

	public static Hashtable newHashtable() {
		if (HFac == null) {
			return null;
		}
		try {
			return (Hashtable) HFac.newInstance();
		} catch (IllegalAccessException e) {
			// e.printStackTrace();
		} catch (InstantiationException e) {
			// e.printStackTrace();
		}
		return null;
	}

	public static Stack newStack() {
		if (SFac == null) {
			return null;
		}
		try {
			return (Stack) SFac.newInstance();
		} catch (IllegalAccessException e) {
			// e.printStackTrace();
		} catch (InstantiationException e) {
			// e.printStackTrace();
		}
		return null;
	}

	public static MathKernel getMath() {
		try {
			return (MathKernel) mathClass.newInstance();
		} catch (IllegalAccessException e) {
			// e.printStackTrace();
		} catch (InstantiationException e) {
			// e.printStackTrace();
		}
		return null;
	}
}
