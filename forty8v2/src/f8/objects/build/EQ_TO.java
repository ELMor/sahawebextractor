/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.objects.build;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class EQ_TO extends NonAlgebraic {
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "EQ->";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 3058;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			if (cl.pop().decompose(cl)) {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

}
