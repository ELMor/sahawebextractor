/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.objects.build;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;
import f8.objects.types.Lista;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class TO_ARRY extends NonAlgebraic {
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "->ARRY";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 3059;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			Stackable sL = cl.peek();
			if (sL instanceof Lista) {
				Lista l = (Lista) sL;
				int sz = l.size();
				if (sz == 1) {
					mkVector(cl, l);
				} else if (sz == 2) {
					mkMatrix(cl, l);
				} else {
					throw new BadArgumentValueException(this);
				}
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	private void mkVector(CL cl, Lista l) throws IndexRangeException,
			BadArgumentValueException, TooFewArgumentsException {
		int sz;
		if (l.get(0) instanceof Double) {
			sz = ((Double) l.get(0)).intValue();
			cl.pop();
			if (cl.check(sz)) {
				double[] vals = new double[sz];
				try {
					for (int i = 1; i <= sz; i++) {
						vals[sz - i] = ((Double) cl.pop()).x;
					}
					cl.push(new F8Vector(vals));
				} catch (Exception e) {
					throw new BadArgumentValueException(this);
				}
			} else {
				throw new TooFewArgumentsException(this);
			}
		} else {
			throw new BadArgumentValueException(this);
		}
	}

	private void mkMatrix(CL cl, Lista l) throws IndexRangeException,
			TooFewArgumentsException, BadArgumentValueException {
		if (l.get(0) instanceof Double && l.get(1) instanceof Double) {
			int row = ((Double) l.get(0)).intValue();
			int col = ((Double) l.get(1)).intValue();
			cl.pop();
			if (cl.check(row * col)) {
				double[][] vals = new double[row][col];
				for (int i = 0; i < row; i++) {
					for (int j = 0; i < col; j++) {
						vals[i][j] = ((Double) cl.peek((row - i) * (col - j))).x;
					}
				}
				cl.pop(row * col);
			} else {
				throw new TooFewArgumentsException(this);
			}
		} else {
			throw new BadArgumentValueException(this);
		}
	}
}
