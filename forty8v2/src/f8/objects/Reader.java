/*
 * Created on 11-ago-2003
 *
 
 
 */
package f8.objects;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface Reader {
	public abstract char read();
}
