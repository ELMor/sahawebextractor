package f8.objects.constants;

import f8.CL;
import f8.DataStream;
import f8.Storable;
import f8.commands.alg.Const;
import f8.objects.Stackable;
import f8.objects.types.Double;

public final class MINR extends Const {
	public MINR() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 2381;
	}

	public Storable getInstance() {
		return new MINR();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public Stackable value() {
		return new Double(1E-323);
	}

	public String toString() {
		return ("MINR");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		return false;
	}

}
