/*
 * Created on 16-ago-2003
 *
 
 
 */
package f8.objects.types;

import antlr.CommonAST;
import antlr.collections.AST;
import f8.CL;
import f8.DataStream;
import f8.MathKernel;
import f8.Ref;
import f8.Storable;
import f8.UtilFactory;
import f8.apps.solver.Solverable;
import f8.commands.Command;
import f8.commands.math.mkl;
import f8.commands.math.exp.CARET;
import f8.commands.math.simple.DIV;
import f8.commands.math.simple.TIMES;
import f8.commands.symb.COLECT;
import f8.commands.units.UnitManager;
import f8.exceptions.F8Exception;
import f8.exceptions.InconsistentUnitsException;
import f8.exceptions.UndefinedException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class Unit extends Stackable implements ObjectParserTokenTypes,
		Solverable {
	static MathKernel mk = UtilFactory.getMath();

	AST unidad = null;

	double valor = 0;

	public Unit(AST def) {
		if (def == null) {
			return;
		} else {
			valor = mk.toDouble(def.getText());
			unidad = normNumbers(def.getNextSibling());
		}
	}

	public Unit(double v, AST u) {
		valor = v;
		if (u == null) {
			return;
		}
		unidad = normNumbers(mkl.copyAST(u));
	}

	public Unit(String value, AST unit) {
		this(mk.toDouble(value), unit);
	}

	public static Unit recEvalUnit(Unit inputUnit) throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		AST nod = inputUnit.getUnidad();
		AST ch1 = null;
		AST ch2 = null;
		ch1 = nod.getFirstChild();
		if (ch1 != null) {
			ch2 = ch1.getNextSibling();
		}
		switch (nod.getType()) {
		case MULT:
			cl.push(recEvalUnit(new Unit(1, ch1)));
			cl.push(recEvalUnit(new Unit(1, ch2)));
			new TIMES().exec();
			return (Unit) cl.pop();
		case DIV:
			if (ch1.getType() == NUMBER) {
				cl.push((Stackable) Command.createFromAST(ch1));
			} else {
				cl.push(recEvalUnit(new Unit(1, ch1)));
			}
			if (ch2.getType() == NUMBER) {
				cl.push((Stackable) Command.createFromAST(ch1));
			} else {
				cl.push(recEvalUnit(new Unit(1, ch2)));
			}
			new DIV().exec();
			return (Unit) cl.pop();
		case CARET:
			cl.push(recEvalUnit(new Unit(1, ch1)));
			cl.push((Stackable) Command.createFromAST(ch2));
			try {
				new CARET().exec();
			} catch (F8Exception e) {
			}
			return (Unit) cl.pop();
		case ID:
			Unit id = UnitManager.getUnitDef(nod.getText());
			if (id == null) {
				throw new UndefinedException(nod.getText());
			} else {
				if (id.getUnidad() == null) {
					return new Unit(inputUnit.valor, mkl
							.copyAST(inputUnit.unidad));
				} else {
					return id;
				}
			}
		}
		return null;
	}

	public Unit convert(Unit to) throws F8Exception {
		Unit thisBase = ubase();
		Unit toBase = new Unit(1, to.unidad).ubase();
		if (!mkl.opEquals(thisBase.getUnidad(), toBase.getUnidad())) {
			throw new InconsistentUnitsException(this);
		}
		return new Unit(thisBase.valor / toBase.valor, to.getUnidad());
	}

	public Stackable copia() {
		return new Unit(valor, mkl.copyAST(unidad));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		cl.push(new Double(valor));
		cl.push(new Unit(1, unidad));
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 118;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new Unit(null);
	}

	public String getTypeName() {
		return "Unit";
	}

	public AST getUnidad() {
		return unidad;
	}

	public double getValor() {
		return valor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DataStream ds) {
		super.loadState(ds);
		valor = ds.readDouble();
		AST[] un = new CommonAST[1];
		Command.loadState(ds, un);
		unidad = un[0];
	}

	public AST normNumbers(AST nval) {
		if (nval == null) {
			return null;
		}
		switch (nval.getType()) {
		case NUMBER:
			nval.setText("1");
			return nval;
		case CARET:
			AST base = nval.getFirstChild();
			mkl.subst(nval, base, normNumbers(base));
			return nval;
		default:
			AST child = nval.getFirstChild();
			for (; child != null; child = child.getNextSibling()) {
				mkl.subst(nval, child, normNumbers(child));
			}

			return nval;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DataStream ds) {
		super.saveState(ds);
		ds.writeDouble(valor);
		Command.saveState(ds, unidad);
	}

	/**
	 * @param d
	 */
	public void setValor(double d) {
		valor = d;
	}

	public String toString() {
		MathKernel mk = UtilFactory.getMath();
		CL cl = Ref.ref.getCalcLogica();
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret
				+ mk.format(valor, cl.getSetting(CL.DOU_MOD), cl
						.getSetting(CL.DOU_PRE)) + unit2String();
	}

	private String unit2String() {
		if(unidad==null)
			return "_";
		if (mkl.opEquals(unidad, mkl.uno)) {
			return "";
		}
		String primary = InfixExp.forma(unidad);
		if (primary.equals("angulo")) {
			return "";
		} else if (primary.equals("arcos")) {
			return "";
		} else {
			return "_" + primary;
		}
	}

	public Unit ubase() throws F8Exception {
		double mul = getValor();
		setValor(1);
		Unit res = recEvalUnit(this);
		setValor(mul);
		if (res != null) {
			return new Unit(mul * res.getValor(), res.getUnidad());
		} else {
			return this;
		}
	}

	public Unit ufact(CL cl, Unit to) throws F8Exception {
		AST toTree = mkl.copyAST(to.getUnidad());
		cl.push(ubase());
		to.setValor(1);
		cl.push(to.ubase());
		new DIV().exec();
		cl.push(new Unit(1, COLECT.colect(toTree)));
		new TIMES().exec();
		Unit u = (Unit) cl.pop();
		return new Unit(u.valor, u.getUnidad());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof TempUnit || obj instanceof Unit) {
			Unit cobj = (Unit) obj, ub1, ub2;
			try {
				ub1 = ubase();
				ub2 = cobj.ubase();
			} catch (F8Exception g) {
				return false;
			}
			return ub1.valor == ub2.valor
					&& mkl.opEquals(ub1.unidad, ub2.unidad);
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Solverable obj, double tol) {
		if (obj instanceof TempUnit || obj instanceof Unit) {
			Unit cobj = (Unit) obj, ub1, ub2;
			try {
				ub1 = ubase();
				ub2 = cobj.ubase();
			} catch (F8Exception g) {
				return false;
			}
			return (Math.abs(ub1.valor - ub2.valor) < tol)
					&& mkl.opEquals(ub1.unidad, ub2.unidad);
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#sign()
	 */
	public int sign() {
		double v = 0;
		try {
			v = ubase().valor;
		} catch (F8Exception e) {

		}
		return v < 0 ? -1 : (v > 0 ? 1 : 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#zero()
	 */
	public Solverable zero() {
		return new Unit(0, unidad);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#amplia(double)
	 */
	public Solverable amplia(double pct) {
		double k = valor;
		if (k == 0)
			return new Unit(pct, unidad);
		if (k < 0)
			k -= k * pct;
		else
			k += k * pct;
		return new Unit(k, unidad);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#compare(f8.symbolic.Solverable)
	 */
	public double compare(Solverable obj) {
		if (obj instanceof Unit) {
			Unit o = (Unit) obj;
			try {
				Unit u1 = ubase(), u2 = o.ubase();
				return u1.valor - u2.valor;
			} catch (F8Exception e) {
			}
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#mean(f8.symbolic.Solverable,
	 *      f8.symbolic.Solverable)
	 */
	public Solverable mean(Solverable o1, Solverable o2) {
		if (o1 instanceof Unit && o2 instanceof Unit) {
			Unit a = (Unit) o1, b = (Unit) o2;
			try {
				Unit converted = b.convert(a);
				return new Unit((a.valor + b.valor) / 2, a.unidad);
			} catch (F8Exception e) {
			}
		} else if (o1 instanceof Unit && o2 instanceof Unit) {
			Unit a = (TempUnit) o1, b = (TempUnit) o2;
			try {
				Unit converted = b.convert(a);
				return new Unit((a.valor + b.valor) / 2, a.unidad);
			} catch (F8Exception e) {
				e.printStackTrace();
			}
		}
		return new Double(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#getAST()
	 */
	public AST getAST() {
		AST ret = new CommonAST();
		ret.setText("unit");
		ret.setType(UNIT);
		ret.addChild(mkl.num(valor));
		ret.addChild(mkl.copyAST(unidad));
		return ret;
	}

}
