package f8.objects.types;

import f8.CL;
import f8.DataStream;
import f8.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.ParseException;
import f8.objects.Stackable;

public final class F8String extends Stackable {
	String s;

	public F8String(String s) {
		this.s = s;
	}

	public Stackable copia() {
		return new F8String(new String(s));
	}

	public String getTypeName() {
		return "String";
	}

	public int getID() {
		return 48;
	}

	public Storable getInstance() {
		return new F8String(null);
	}

	public void loadState(DataStream ds) {
		super.loadState(ds);
		s = ds.readStringSafe();
	}

	public void saveState(DataStream ds) {
		super.saveState(ds);
		ds.writeStringSafe(s);
	}

	public String toString() {
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + "\"" + s + "\"";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) throws F8Exception {
		try {
			cl.enter(s);
		} catch (F8Exception e) {
			throw new ParseException(this);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof F8String) {
			return ((F8String) obj).s.equals(s);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

}
