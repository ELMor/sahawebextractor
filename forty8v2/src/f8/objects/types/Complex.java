package f8.objects.types;

import f8.CL;
import f8.DataStream;
import f8.MathKernel;
import f8.Ref;
import f8.Storable;
import f8.UtilFactory;
import f8.apps.solver.Solverable;
import f8.commands.alg.ComplexValue;
import f8.objects.Stackable;

public final class Complex extends Stackable implements ComplexValue,
		Solverable {
	public double re;

	public double im;

	public Complex(double x, double y) {
		this.re = x;
		this.im = y;
	}

	public Complex(Complex k) {
		re = k.re;
		im = k.im;
	}

	public Stackable mutate() {
		if (im == 0)
			return new Double(re);
		return new Complex(this);
	}

	public Stackable copia() {
		return new Complex(re, im);
	}

	public String getTypeName() {
		return "Complex";
	}

	public int getID() {
		return 60;
	}

	public Storable getInstance() {
		return new Complex(0, 0);
	}

	public void loadState(DataStream ds) {
		super.loadState(ds);
		re = ds.readDouble();
		im = ds.readDouble();
	}

	public void saveState(DataStream ds) {
		super.saveState(ds);
		ds.writeDouble(re);
		ds.writeDouble(im);
	}

	public double[] complexValue() {
		double[] c = new double[2];
		c[0] = re;
		c[1] = im;

		return (c);
	}

	public String toString() {
		CL cl = Ref.ref.getCalcLogica();
		int dmod = cl.getSetting(CL.DOU_MOD);
		int dpre = cl.getSetting(CL.DOU_PRE);
		MathKernel mk = UtilFactory.getMath();
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + "(" + mk.format(re, dmod, dpre) + ","
				+ mk.format(im, dmod, dpre) + ")";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose()
	 */
	public boolean decompose(CL cl) {
		cl.push(new Double(re));
		cl.push(new Double(im));
		return false;
	}

	// ------------------------------------------------------------------------
	// Various functions

	/**
	 * Compares this object with the Complex number given as parameter
	 * 
	 * <pre>
	 * b
	 * </pre>. The
	 * 
	 * <pre>
	 * tolerance
	 * </pre>
	 * 
	 * parameter is the radius within which the
	 * 
	 * <pre>
	 * b
	 * </pre>
	 * 
	 * number must lie for the two complex numbers to be considered equal.
	 * 
	 * @return
	 * 
	 * <pre>
	 * true
	 * </pre>
	 * 
	 * if the complex number are considered equal,
	 * 
	 * <pre>
	 * false
	 * </pre>
	 * 
	 * otherwise.
	 */
	public boolean equals(Complex b, double tolerance) {
		double temp1 = (re - b.re);
		double temp2 = (im - b.im);

		return (temp1 * temp1 + temp2 * temp2) <= tolerance * tolerance;
	}

	/**
	 * Returns the absolute value of the complex number.
	 * <p>
	 * Adapted from Numerical Recipes in C - The Art of Scientific Computing<br>
	 * ISBN 0-521-43108-5
	 */
	public double abs() {
		double absRe = Math.abs(re);
		double absIm = Math.abs(im);

		if (absRe == 0 && absIm == 0) {
			return 0;
		} else if (absRe > absIm) {
			double temp = absIm / absRe;
			return absRe * Math.sqrt(1 + temp * temp);
		} else {
			double temp = absRe / absIm;
			return absIm * Math.sqrt(1 + temp * temp);
		}
	}

	/**
	 * Returns the square of the absolute value (re*re+im*im).
	 */
	public double abs2() {
		return re * re + im * im;
	}

	/**
	 * Returns the argument of this complex number (Math.atan2(re,im))
	 */
	public double arg() {
		return Math.atan2(im, re);
	}

	/**
	 * Returns the negative value of this complex number.
	 */
	public Complex neg() {
		return new Complex(-re, -im);
	}

	/**
	 * Multiply the complex number with a double value.
	 * 
	 * @return The result of the multiplication
	 */
	public Complex mul(double b) {
		return new Complex(re * b, im * b);
	}

	/**
	 * Multiply the complex number with another complex value.
	 * 
	 * @return The result of the multiplication
	 */
	public Complex mul(Complex b) {
		return new Complex(re * b.re - im * b.im, im * b.re + re * b.im);
	}

	/**
	 * Returns the result of dividing this complex number by the parameter
	 * 
	 * <pre>
	 * b
	 * </pre>.
	 */
	public Complex div(Complex b) {
		// Adapted from Numerical Recipes in C - The Art of Scientific Computing
		// ISBN 0-521-43108-5
		double resRe, resIm;
		double r, den;

		if (Math.abs(b.re) >= Math.abs(b.im)) {
			r = b.im / b.re;
			den = b.re + r * b.im;
			resRe = (re + r * im) / den;
			resIm = (im - r * re) / den;
		} else {
			r = b.re / b.im;
			den = b.im + r * b.re;
			resRe = (re * r + im) / den;
			resIm = (im * r - re) / den;
		}

		return new Complex(resRe, resIm);
	}

	/**
	 * Returns the value of this complex number raised to the power of a real
	 * component (in double precision).
	 * <p>
	 * This method considers special cases where a simpler algorithm would
	 * return "ugly" results.<br>
	 * For example when the expression (-1e40)^0.5 is evaluated without
	 * considering the special case, the argument of the base is the double
	 * number closest to pi. When sin and cos are used for the final evaluation
	 * of the result, the slight difference of the argument from pi causes a
	 * non-zero value for the real component of the result. Because the value of
	 * the base is so high, the error is magnified.Although the error is normal
	 * for floating point calculations, the consideration of commonly occuring
	 * special cases improves the accuracy and aesthetics of the results.
	 * <p>
	 * If you know a more elegant way to solve this problem, please let me know
	 * at nathanfunk@hotmail.com .
	 */
	public Complex power(double exponent) {
		// z^exp = abs(z)^exp * (cos(exp*arg(z)) + i*sin(exp*arg(z)))
		double scalar = Math.pow(abs(), exponent);
		boolean specialCase = false;
		int factor = 0;

		// consider special cases to avoid floating point errors
		// for power expressions such as (-1e20)^2
		if (im == 0 && re < 0) {
			specialCase = true;
			factor = 2;
		}
		if (re == 0 && im > 0) {
			specialCase = true;
			factor = 1;
		}
		if (re == 0 && im < 0) {
			specialCase = true;
			factor = -1;
		}

		if (specialCase && factor * exponent == (int) (factor * exponent)) {
			short[] cSin = { 0, 1, 0, -1 }; // sin of 0, pi/2, pi, 3pi/2
			short[] cCos = { 1, 0, -1, 0 }; // cos of 0, pi/2, pi, 3pi/2

			int x = ((int) (factor * exponent)) % 4;
			if (x < 0)
				x = 4 + x;

			return new Complex(scalar * cCos[x], scalar * cSin[x]);
		}

		double temp = exponent * arg();

		return new Complex(scalar * Math.cos(temp), scalar * Math.sin(temp));
	}

	/**
	 * Returns the value of this complex number raised to the power of a complex
	 * exponent
	 */
	public Complex power(Complex exponent) {
		if (exponent.im == 0)
			return power(exponent.re);

		double temp1Re = Math.log(abs());
		double temp1Im = arg();

		double temp2Re = (temp1Re * exponent.re) - (temp1Im * exponent.im);
		double temp2Im = (temp1Re * exponent.im) + (temp1Im * exponent.re);

		double scalar = Math.exp(temp2Re);

		return new Complex(scalar * Math.cos(temp2Im), scalar
				* Math.sin(temp2Im));
	}

	/**
	 * Returns the logarithm of this complex number.
	 */
	public Complex log() {
		return new Complex(Math.log(abs()), arg());
	}

	/**
	 * Calculates the square root of this object.<br>
	 * Adapted from Numerical Recipes in C - The Art of Scientific Computing
	 * (ISBN 0-521-43108-5)
	 */
	public Complex sqrt() {
		Complex c;
		double absRe, absIm, w, r;

		if (re == 0 && im == 0) {
			c = new Complex(0, 0);
		} else {
			absRe = Math.abs(re);
			absIm = Math.abs(im);

			if (absRe >= absIm) {
				r = absIm / absRe;
				w = Math.sqrt(absRe)
						* Math.sqrt(0.5 * (1.0 + Math.sqrt(1.0 + r * r)));
			} else {
				r = absRe / absIm;
				w = Math.sqrt(absIm)
						* Math.sqrt(0.5 * (r + Math.sqrt(1.0 + r * r)));
			}

			if (re >= 0) {
				c = new Complex(w, im / (2.0 * w));
			} else {
				if (im < 0)
					w = -w;
				c = new Complex(im / (2.0 * w), w);
			}
		}

		return c;
	}

	// ------------------------------------------------------------------------
	// Trigonometric functions

	/**
	 * Returns the sine of this complex number.
	 */
	public Complex sin() {
		double izRe, izIm;
		double temp1Re, temp1Im;
		double temp2Re, temp2Im;
		double scalar;

		// sin(z) = ( exp(i*z) - exp(-i*z) ) / (2*i)
		izRe = -im;
		izIm = re;

		// first exp
		scalar = Math.exp(izRe);
		temp1Re = scalar * Math.cos(izIm);
		temp1Im = scalar * Math.sin(izIm);

		// second exp
		scalar = Math.exp(-izRe);
		temp2Re = scalar * Math.cos(-izIm);
		temp2Im = scalar * Math.sin(-izIm);

		temp1Re -= temp2Re;
		temp1Im -= temp2Im;

		return new Complex(0.5 * temp1Im, -0.5 * temp1Re);
	}

	/**
	 * Returns the cosine of this complex number.
	 */
	public Complex cos() {
		double izRe, izIm;
		double temp1Re, temp1Im;
		double temp2Re, temp2Im;
		double scalar;

		// cos(z) = ( exp(i*z) + exp(-i*z) ) / 2
		izRe = -im;
		izIm = re;

		// first exp
		scalar = Math.exp(izRe);
		temp1Re = scalar * Math.cos(izIm);
		temp1Im = scalar * Math.sin(izIm);

		// second exp
		scalar = Math.exp(-izRe);
		temp2Re = scalar * Math.cos(-izIm);
		temp2Im = scalar * Math.sin(-izIm);

		temp1Re += temp2Re;
		temp1Im += temp2Im;

		return new Complex(0.5 * temp1Re, 0.5 * temp1Im);
	}

	/**
	 * Returns the tangent of this complex number.
	 */
	public Complex tan() {
		// tan(z) = sin(z)/cos(z)
		double izRe, izIm;
		double temp1Re, temp1Im;
		double temp2Re, temp2Im;
		double scalar;
		Complex sinResult, cosResult;

		// sin(z) = ( exp(i*z) - exp(-i*z) ) / (2*i)
		izRe = -im;
		izIm = re;

		// first exp
		scalar = Math.exp(izRe);
		temp1Re = scalar * Math.cos(izIm);
		temp1Im = scalar * Math.sin(izIm);

		// second exp
		scalar = Math.exp(-izRe);
		temp2Re = scalar * Math.cos(-izIm);
		temp2Im = scalar * Math.sin(-izIm);

		temp1Re -= temp2Re;
		temp1Im -= temp2Im;

		sinResult = new Complex(0.5 * temp1Re, 0.5 * temp1Im);

		// cos(z) = ( exp(i*z) + exp(-i*z) ) / 2
		izRe = -im;
		izIm = re;

		// first exp
		scalar = Math.exp(izRe);
		temp1Re = scalar * Math.cos(izIm);
		temp1Im = scalar * Math.sin(izIm);

		// second exp
		scalar = Math.exp(-izRe);
		temp2Re = scalar * Math.cos(-izIm);
		temp2Im = scalar * Math.sin(-izIm);

		temp1Re += temp2Re;
		temp1Im += temp2Im;

		cosResult = new Complex(0.5 * temp1Re, 0.5 * temp1Im);

		return sinResult.div(cosResult);
	}

	// ------------------------------------------------------------------------
	// Inverse trigonometric functions

	public Complex asin() {
		Complex result;
		double tempRe, tempIm;

		// asin(z) = -i * log(i*z + sqrt(1 - z*z))

		tempRe = 1.0 - ((re * re) - (im * im));
		tempIm = 0.0 - ((re * im) + (im * re));

		result = new Complex(tempRe, tempIm);
		result = result.sqrt();

		result.re += -im;
		result.im += re;

		tempRe = Math.log(result.abs());
		tempIm = result.arg();

		result.re = tempIm;
		result.im = -tempRe;

		return result;
	}

	public Complex acos() {
		Complex result;
		double tempRe, tempIm;

		// acos(z) = -i * log( z + i * sqrt(1 - z*z) )

		tempRe = 1.0 - ((re * re) - (im * im));
		tempIm = 0.0 - ((re * im) + (im * re));

		result = new Complex(tempRe, tempIm);
		result = result.sqrt();

		tempRe = -result.im;
		tempIm = result.re;

		result.re = re + tempRe;
		result.im = im + tempIm;

		tempRe = Math.log(result.abs());
		tempIm = result.arg();

		result.re = tempIm;
		result.im = -tempRe;

		return result;
	}

	public Complex atan() {
		// atan(z) = -i/2 * log((i-z)/(i+z))

		double tempRe, tempIm;
		Complex result = new Complex(-re, 1.0 - im);

		tempRe = re;
		tempIm = 1.0 + im;

		result = result.div(new Complex(tempRe, tempIm));

		tempRe = Math.log(result.abs());
		tempIm = result.arg();

		result.re = 0.5 * tempIm;
		result.im = -0.5 * tempRe;

		return result;
	}

	// ------------------------------------------------------------------------
	// Hyperbolic trigonometric functions

	public Complex sinh() {
		double scalar;
		double temp1Re, temp1Im;
		double temp2Re, temp2Im;
		// sinh(z) = ( exp(z) - exp(-z) ) / 2

		// first exp
		scalar = Math.exp(re);
		temp1Re = scalar * Math.cos(im);
		temp1Im = scalar * Math.sin(im);

		// second exp
		scalar = Math.exp(-re);
		temp2Re = scalar * Math.cos(-im);
		temp2Im = scalar * Math.sin(-im);

		temp1Re -= temp2Re;
		temp1Im -= temp2Im;

		return new Complex(0.5 * temp1Re, 0.5 * temp1Im);
	}

	public Complex cosh() {
		double scalar;
		double temp1Re, temp1Im;
		double temp2Re, temp2Im;
		// cosh(z) = ( exp(z) + exp(-z) ) / 2

		// first exp
		scalar = Math.exp(re);
		temp1Re = scalar * Math.cos(im);
		temp1Im = scalar * Math.sin(im);

		// second exp
		scalar = Math.exp(-re);
		temp2Re = scalar * Math.cos(-im);
		temp2Im = scalar * Math.sin(-im);

		temp1Re += temp2Re;
		temp1Im += temp2Im;

		return new Complex(0.5 * temp1Re, 0.5 * temp1Im);
	}

	public Complex tanh() {
		double scalar;
		double temp1Re, temp1Im;
		double temp2Re, temp2Im;
		Complex sinRes, cosRes;
		// tanh(z) = sinh(z) / cosh(z)

		scalar = Math.exp(re);
		temp1Re = scalar * Math.cos(im);
		temp1Im = scalar * Math.sin(im);

		scalar = Math.exp(-re);
		temp2Re = scalar * Math.cos(-im);
		temp2Im = scalar * Math.sin(-im);

		temp1Re -= temp2Re;
		temp1Im -= temp2Im;

		sinRes = new Complex(0.5 * temp1Re, 0.5 * temp1Im);

		scalar = Math.exp(re);
		temp1Re = scalar * Math.cos(im);
		temp1Im = scalar * Math.sin(im);

		scalar = Math.exp(-re);
		temp2Re = scalar * Math.cos(-im);
		temp2Im = scalar * Math.sin(-im);

		temp1Re += temp2Re;
		temp1Im += temp2Im;

		cosRes = new Complex(0.5 * temp1Re, 0.5 * temp1Im);

		return sinRes.div(cosRes);
	}

	// ------------------------------------------------------------------------
	// Inverse hyperbolic trigonometric functions

	public Complex asinh() {
		Complex result;
		// asinh(z) = log(z + sqrt(z*z + 1))

		result = new Complex(((re * re) - (im * im)) + 1, (re * im) + (im * re));

		result = result.sqrt();

		result.re += re;
		result.im += im;

		double temp = result.arg();
		result.re = Math.log(result.abs());
		result.im = temp;

		return result;
	}

	public Complex acosh() {
		Complex result;

		// acosh(z) = log(z + sqrt(z*z - 1))

		result = new Complex(((re * re) - (im * im)) - 1, (re * im) + (im * re));

		result = result.sqrt();

		result.re += re;
		result.im += im;

		double temp = result.arg();
		result.re = Math.log(result.abs());
		result.im = temp;

		return result;
	}

	public Complex atanh() {
		// atanh(z) = 1/2 * log( (1+z)/(1-z) )

		double tempRe, tempIm;
		Complex result = new Complex(1.0 + re, im);

		tempRe = 1.0 - re;
		tempIm = -im;

		result = result.div(new Complex(tempRe, tempIm));

		tempRe = Math.log(result.abs());
		tempIm = result.arg();

		result.re = 0.5 * tempRe;
		result.im = 0.5 * tempIm;

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Complex) {
			Complex cobj = (Complex) obj;
			if (re == cobj.re && im == cobj.im)
				return true;
			return false;

		} else if (obj instanceof Double) {
			Double cobj = (Double) obj;
			if (im == 0 && re == cobj.x)
				return true;
			return false;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Solverable obj, double tol) {
		if (obj instanceof Complex) {
			Complex cobj = (Complex) obj;
			if (Math.abs(re - cobj.re) + Math.abs(im - cobj.im) < tol)
				return true;
			return false;

		} else if (obj instanceof Double) {
			Double cobj = (Double) obj;
			if (Math.abs(re - cobj.x) + Math.abs(im) < tol)
				return true;
			return false;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#sign()
	 */
	public int sign() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#zero()
	 */
	public Solverable zero() {
		return zeroComplex;
	}

	public static Complex zeroComplex = new Complex(0, 0);

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#amplia(double)
	 */
	public Solverable amplia(double pct) {
		double r = re, i = im;
		if (r == 0)
			r += pct;
		if (i == 0)
			i += pct;
		r += r * pct;
		i += i * pct;
		return new Complex(r, i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#compare(f8.symbolic.Solverable)
	 */
	public double compare(Solverable obj) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#mean(f8.symbolic.Solverable,
	 *      f8.symbolic.Solverable)
	 */
	public Solverable mean(Solverable o1, Solverable o2) {
		if (o1 instanceof Complex && o2 instanceof Complex) {
			return new Complex((((Complex) o1).re + ((Complex) o2).re) / 2,
					(((Complex) o1).im + ((Complex) o2).im) / 2);
		}
		return new Complex(0, 0);
	}

}
