/*
 * Created on 14-ago-2003
 *
 
 
 */
package f8.objects.types;

import antlr.CommonAST;
import antlr.Token;
import antlr.collections.AST;
import f8.CL;
import f8.DataStream;
import f8.Storable;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.Command;
import f8.commands.alg.CommandSequence;
import f8.commands.alg.Ejecutable;
import f8.commands.math.mkl;
import f8.commands.prog.debug.Debuggable;
import f8.commands.symb.Sigma;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.kernel.yacc.OpPrior;
import f8.objects.Stackable;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class InfixExp extends Stackable implements
		ObjectParserTokenTypes, Debuggable, Ejecutable {

	/**
	 * Construye una cadena "( arg1,arg2,... )"
	 * 
	 * @param arg
	 *            Lista de argumentos
	 * @return cadena de caracteres
	 */
	public static String astArgs(AST arg) {
		String k = "(";

		while (arg != null) {
			k += forma(arg);
			arg = arg.getNextSibling();

			if (arg != null) {
				k += ",";
			}
		}

		return k + ")";
	}

	/**
	 * Retorna un generic array cuya ejecucion en la pila supone la evaluacion
	 * de la expresion infija
	 * 
	 * @param exp
	 *            Expresion a evaluar
	 * @return GenericArray.exec() evalua la expresion infija representada por
	 *         el arbol exp
	 */
	public static CommandSequence decode(AST exp) {
		// Aqui se van a guardar los StkObs
		Vector vret = UtilFactory.newVector();

		// Primero se a�aden al GArray los hijos, y luego el padre.
		for (AST child = exp.getFirstChild(); child != null; child = child
				.getNextSibling()) {
			if (child.getType() != UNIT) {
				vret.add(decode(child));
			} else {
				vret.add(Command.createFromAST(child));
			}
		}
		if (exp.getType() == MULT || exp.getType() == PLUS) {
			int n = exp.getNumberOfChildren();
			Command k = Command.createFromAST(exp);
			for (int i = 0; i < n - 1; i++)
				vret.add(k);
		} else {
			vret.add(Command.createFromAST(exp));
		}
		return new CommandSequence(vret);
	}

	/**
	 * Construye la representacion textual de la expresion nodo
	 * 
	 * @param nodo
	 *            Expresion
	 * @return Cadena
	 */
	public static String forma(AST nodo) {
		int tipo = nodo.getType();
		AST child = nodo.getFirstChild();

		switch (tipo) {
		case COMPLEX:
			return "(" + forma(child) + "+" + forma(child.getNextSibling())
					+ "*i)";
		case LITERAL:
			nodo = nodo.getFirstChild();
			child = nodo.getFirstChild();
		case ID:
			if (child == null) {
				return nodo.getText();
			} else {
				return nodo.getText() + astArgs(child);
			}
		case NUMBER:
			return new Double(UtilFactory.getMath().toDouble(nodo.getText()))
					.toString();
		case UNIT:
			return child.getText() + "_" + forma(child.getNextSibling());
		case NEG:
			return "-" + forma(child);
		case INV:
			return "INV(" + forma(child) + ")";
		case DERIV:
			String derFu = forma(child);
			child = child.getNextSibling();
			String derId = forma(child);
			return "\u0088" + derId + "(" + derFu + ")";
		case INTEG:
			String intDo = forma(child);
			child = child.getNextSibling();
			String intUp = forma(child);
			child = child.getNextSibling();
			String intFu = forma(child);
			child = child.getNextSibling();
			String intId = forma(child);
			return "\u0084(" + intDo + "," + intUp + "," + intFu + "," + intId
					+ ")";
		case SIGMA:
			return Sigma.forma(nodo);
		default:
			return normalOp(nodo);
		}
	}

	public static String isLiteral(Command a) {
		if (a instanceof InfixExp) {
			InfixExp iea = (InfixExp) a;
			if (iea.getAST().getType() == ID) {
				return iea.getAST().getText();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private static String normalOp(AST opp) {
		AST child;
		int pprio = OpPrior.val[opp.getType()];
		String ret = "";

		for (child = opp.getFirstChild(); child != null; child = child
				.getNextSibling()) {
			if (child != opp.getFirstChild()) {
				ret += opp.getText();
			}

			int cprio = OpPrior.val[child.getType()];

			if ((pprio > cprio)
					|| ((child != opp.getFirstChild()) && (pprio == cprio) && ((cprio == 64) || (cprio == 32)))) {
				ret += ("(" + forma(child) + ")");
			} else {
				ret += forma(child);
			}
		}

		return ret;
	}

	AST expression = null;

	CommandSequence run = null;

	/**
	 * Crea nua expresion infija. El arbol es exp1
	 * 
	 * @param exp1
	 */
	public InfixExp(AST exp1) {
		AST k;
		if (exp1 == null) {
			return;
		}
		if (exp1.getType() != INFIX) {
			k = new CommonAST(new Token(INFIX));
			k.addChild(exp1);
		} else {
			k = exp1;
		}
		expression = k;
	}

	public Stackable copia() {
		return new InfixExp(mkl.copyAST(expression));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run.decompose(cl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof InfixExp) {
			return mkl.opEquals(expression, ((InfixExp) obj).expression);
		} else {
			return false;
		}
	}

	public void exec() throws F8Exception {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		run.exec();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#execDebug()
	 */
	public boolean execDebug(boolean si) throws F8Exception {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run.execDebug(si);
	}

	public AST getAST() {
		return expression.getFirstChild();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Ejecutable#getEjecutable()
	 */
	public CommandSequence getEjecutable() {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 119;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new InfixExp(null);
	}

	public String getTypeName() {
		return "InfixExp";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#isAnyMore()
	 */
	public boolean isAnyMore() {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run.isAnyMore();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DataStream ds) {
		super.loadState(ds);
		AST[] exp = new CommonAST[1];
		Command.loadState(ds, exp);
		expression = exp[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#nextCommand()
	 */
	public String nextCommand() {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run.nextCommand();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DataStream ds) {
		super.saveState(ds);
		Command.saveState(ds, expression);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

	public String toString() {
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		if(expression==null)
			return "''";
		AST fc = expression.getFirstChild();
		String eExp = forma(fc);
		String vAtv = "";
		fc = fc.getNextSibling();
		if (fc != null) {
			AST idt = fc.getFirstChild();
			AST val = idt.getNextSibling();
			vAtv = "|(" + idt.toString() + "=" + val.toString() + ")";
		}
		return ret + "'" + eExp + vAtv + "'";
	}

}
