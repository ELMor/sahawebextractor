package f8.objects.types;

import antlr.collections.AST;
import f8.CL;
import f8.CalcGUI;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.commands.prog.debug.Debuggable;
import f8.exceptions.F8Exception;
import f8.exceptions.InvalidUserFunctionException;
import f8.exceptions.WrongNumberOfArgumentsException;
import f8.objects.Stackable;

public final class Literal extends Stackable implements Derivable, Debuggable {
	public String nombre;

	public Literal(String s) {
		this.nombre = s;
	}

	public Stackable copia() {
		return new Literal(nombre);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST exp, String var) throws F8Exception {
		String vExpName = null;
		if (exp != null)
			vExpName = exp.getText();
		if (exp == null) { // Var
			if (nombre.equals(var)) {
				return mkl.num(1);
			} else {
				return mkl.num(0);
			}
		} else { // Function
			CL cl = Ref.ref.getCalcLogica();
			Command f = cl.lookup(nombre);
			if ((f != null) && f instanceof Derivable) {
				return ((Derivable) f).deriveWithArgs(exp, var);
			} else {
				String derFName = "der" + nombre;
				Command cDer = cl.lookup(derFName);
				if (cDer != null) {
					if (cDer instanceof Proc) {
						Proc pDer = (Proc) cDer;
						int numSiblings = 0;
						for (AST at = exp; at != null; at = at.getNextSibling())
							numSiblings++;
						if (pDer.numOfLocalVars() == (2 * numSiblings)) {
							AST at = exp;
							while (at != null) {
								cl.push(new InfixExp(at));
								at = at.getNextSibling();
							}
							at = exp;
							while (at != null) {
								cl.push(new InfixExp(DER
										.deriveFunction(at, var)));
								at = at.getNextSibling();
							}
							pDer.exec();
							return cl.pop().getAST();
						} else {
							throw new WrongNumberOfArgumentsException(new DER());
						}
					} else {
						throw new InvalidUserFunctionException(new DER());
					}
				} else {
					return mkl.derFgen(derFName, exp.getFirstChild(), var);
				}
			}
		}
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		Command ob = cl.lookup(nombre);

		if ((ob != null) && !(ob instanceof Lista)) {
			ob.exec();
		} else {
			cl.push(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#execDebug()
	 */
	public boolean execDebug(boolean si) throws F8Exception {
		CalcGUI cg = Ref.ref;
		CL cl = cg.getCalcLogica();
		Command ob = cl.lookup(nombre);
		if (ob == null) {
			cl.push(this);
			return true;
		}
		Debuggable dtask;
		try {
			dtask = (Debuggable) ob;
			if (si) {
				cl.setProcDebug(dtask);
				cg.refresh(true);
				cg.temporaryLabels(null, dtask.nextCommand());
				return true;
			} else {
				ob.exec();
				return true;
			}
		} catch (ClassCastException e) {
			ob.exec();
			return true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StackableObject#getAST()
	 */
	public AST getAST() {
		return mkl.fca(nombre, null);
	}

	public int getID() {
		return 32;
	}

	public Storable getInstance() {
		return new Literal(null);
	}

	public String getTypeName() {
		return "Literal";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#isAnyMore()
	 */
	public boolean isAnyMore() {
		return false;
	}

	public void loadState(DataStream ds) {
		super.loadState(ds);
		nombre = ds.readStringSafe();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#nextCommand()
	 */
	public String nextCommand() {
		return nombre;
	}

	public void saveState(DataStream ds) {
		super.saveState(ds);
		ds.writeStringSafe(nombre);
	}

	public String toString() {
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + nombre;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Literal) {
			return nombre.equals(((Literal) obj).nombre);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

}
