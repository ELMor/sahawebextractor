package f8.objects.types;

import antlr.collections.AST;
import f8.CL;
import f8.DataStream;
import f8.MathKernel;
import f8.Ref;
import f8.Storable;
import f8.UtilFactory;
import f8.apps.solver.Solverable;
import f8.commands.alg.ComplexValue;
import f8.commands.alg.DoubleValue;
import f8.commands.math.mkl;
import f8.objects.Stackable;

public final class Double extends Stackable implements DoubleValue,
		ComplexValue, Solverable {
	public double x;

	public Double(double x) {
		this.x = x;
	}

	public Double(){
		
	}
	
	public double[] complexValue() {
		double[] y = new double[2];
		y[0] = x;
		y[1] = 0;

		return (y);
	}

	public Stackable copia() {
		return new Double(x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		return false;
	}

	public double doubleValue() {
		return (x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StackableObject#getAST()
	 */
	public AST getAST() {
		return mkl.num(x);
	}

	public int getID() {
		return 73;
	}

	public Storable getInstance() {
		return new Double(0);
	}

	public String getTypeName() {
		return "Double";
	}

	public int intValue() {
		return (int) x;
	}

	public void loadState(DataStream ds) {
		super.loadState(ds);
		x = ds.readDouble();
	}

	public void saveState(DataStream ds) {
		super.saveState(ds);
		ds.writeDouble(x);
	}

	public String toString() {
		return dtos(this);
	}

	private static String dtos(Double j) {
		CL cl = Ref.ref.getCalcLogica();
		MathKernel mk = UtilFactory.getMath();
		String ret = j.getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret
				+ mk.format(j.x, cl.getSetting(CL.DOU_MOD), cl
						.getSetting(CL.DOU_PRE));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Double) {
			return x == ((Double) obj).x;
		} else if (obj instanceof Complex) {
			return ((Complex) obj).equals(this);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Solverable obj, double tol) {
		if (obj instanceof Double) {
			return Math.abs(x - ((Double) obj).x) < tol;
		} else if (obj instanceof Complex) {
			return ((Complex) obj).tolerance(this, tol);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#sign()
	 */
	public int sign() {
		return (x < 0) ? (-1) : (x > 0 ? 1 : 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#zero()
	 */
	public Solverable zero() {
		return zeroDouble;
	}

	public static Double zeroDouble = new Double(0);

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#amplia(double)
	 */
	public Solverable amplia(double pct) {
		double k = x;
		if (k == 0)
			return new Double(pct);
		if (k < 0)
			k -= k * pct;
		else
			k += k * pct;
		return new Double(k);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#compare(f8.symbolic.Solverable)
	 */
	public double compare(Solverable obj) {
		if (obj instanceof Double) {
			return x - ((Double) obj).x;
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#mean(f8.symbolic.Solverable,
	 *      f8.symbolic.Solverable)
	 */
	public Solverable mean(Solverable o1, Solverable o2) {
		if (o1 instanceof Double && o2 instanceof Double) {
			return new Double((((Double) o1).x + ((Double) o2).x) / 2);
		} else
			return new Double(0);
	}

}
