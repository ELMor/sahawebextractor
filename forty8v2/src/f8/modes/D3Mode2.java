package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;

public final class D3Mode2 extends CheckGroup {
	public D3Mode2() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 485;
	}

	public Storable getInstance() {
		return new D3Mode2();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() {
		CL cl = Ref.ref.getCalcLogica();
		cl.setSettings(CL.MOD_3D, CL.RAZ);
		return false;
	}

	public String toString() {
		return ("3D_2");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "3D";
	}
}
