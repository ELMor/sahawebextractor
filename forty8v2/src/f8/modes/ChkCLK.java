package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;

public final class ChkCLK extends CheckGroup {
	public ChkCLK() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 496;
	}

	public Storable getInstance() {
		return new ChkCLK();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() {
		CL cl = Ref.ref.getCalcLogica();
		cl.setSettings(CL.CLOCK, !cl.isSetting(CL.CLOCK));
		return false;
	}

	public String toString() {
		return ("ChkCLK_");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "CK";
	}
}
