package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;

public final class ChkCMD extends CheckGroup {
	public ChkCMD() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 490;
	}

	public Storable getInstance() {
		return new ChkCMD();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() {
		CL cl = Ref.ref.getCalcLogica();
		cl.setSettings(CL.LAS_CMD, !cl.isSetting(CL.LAS_CMD));
		return false;
	}

	public String toString() {
		return ("ChkCMD_");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "CM";
	}
}
