package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;

public final class ChkML extends CheckGroup {
	public ChkML() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 497;
	}

	public Storable getInstance() {
		return new ChkML();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() {
		CL cl = Ref.ref.getCalcLogica();
		cl.setSettings(CL.MULTILI, !cl.isSetting(CL.MULTILI));
		return false;
	}

	public String toString() {
		return ("ChkML_");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "ML";
	}
}
