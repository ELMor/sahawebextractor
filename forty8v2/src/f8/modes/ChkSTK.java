package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;

public final class ChkSTK extends CheckGroup {
	public ChkSTK() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 500;
	}

	public Storable getInstance() {
		return new ChkSTK();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() {
		CL cl = Ref.ref.getCalcLogica();
		cl.setSettings(CL.LAS_STA, !cl.isSetting(CL.LAS_STA));
		return false;
	}

	public String toString() {
		return ("ChkSTK_");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "ST";
	}
}
