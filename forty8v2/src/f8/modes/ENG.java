package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Storable;
import f8.exceptions.F8Exception;

public final class ENG extends CheckGroup {
	public ENG() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 505;
	}

	public Storable getInstance() {
		return new ENG();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() throws F8Exception {
		return setTipeAndPrecission(CL.ENG);
	}

	public String toString() {
		return ("ENG");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "DM";
	}
}
