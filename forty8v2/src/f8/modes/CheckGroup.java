/*
 * Created on 09-sep-2003
 *
 
 
 */
package f8.modes;

import f8.CL;
import f8.Ref;
import f8.Vector;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.exceptions.TooFewArgumentsException;
import f8.keyboard.CF;
import f8.keyboard.Menu;
import f8.keyboard.WithEnter;
import f8.keyboard.hp48.menu.MODES;
import f8.objects.types.Double;
import f8.objects.types.Int;

/**
 * @author elinares
 * 
 * 
 * 
 */
public abstract class CheckGroup extends NonAlgebraic {
	public abstract String getGroupName();

	public abstract boolean execBefore() throws F8Exception;

	public void exec() throws F8Exception {
		if (execBefore()) {
			return;
		}
		CF cf = Ref.ref.getCalcFisica();
		Vector v = (Vector) Menu.groups.get(getGroupName());
		if (v == null) {
			return;
		}
		int sz = v.size();
		WithEnter c = null;
		if (sz == 1) {
			c = (WithEnter) v.elementAt(0);
			c.setChecked(!c.isChecked());
		} else {
			String search = toString();
			for (int i = 0; i < v.size(); i++) {
				c = (WithEnter) v.elementAt(i);
				c.setChecked(c.getEmit().equals(search));
			}
		}

		if (cf.getUpMenu() instanceof MODES) {
			Ref.ref.refreshProgMenu();
		}
	}

	protected boolean setTipeAndPrecission(int tipo) throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			Command a = (Command) cl.pop();

			if (a instanceof Int) {
				int n = ((Int) a).n;

				if (n >= 0) {
					cl.setSettings(CL.DOU_MOD, tipo);
					cl.setSettings(CL.DOU_PRE, n);
					return false;
				} else {
					throw new IndexOutOfBoundsException(null);
				}
			} else if (a instanceof Double) {
				int n = (int) ((Double) a).x;

				if (n >= 0) {
					cl.setSettings(CL.DOU_MOD, tipo);
					cl.setSettings(CL.DOU_PRE, n);
					return false;
				} else {
					throw new IndexRangeException(this);
				}
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
		return true;
	}
}
