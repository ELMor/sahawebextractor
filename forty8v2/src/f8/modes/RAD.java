package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;

public final class RAD extends CheckGroup {
	public RAD() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 488;
	}

	public Storable getInstance() {
		return new RAD();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() {
		CL cl = Ref.ref.getCalcLogica();
		cl.setSettings(CL.ANG_MOD, CL.RAD);
		return false;
	}

	public String toString() {
		return ("RAD");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "AM";
	}
}
