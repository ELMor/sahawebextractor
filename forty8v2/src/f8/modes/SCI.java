package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Storable;
import f8.exceptions.F8Exception;

public final class SCI extends CheckGroup {
	public SCI() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 78;
	}

	public Storable getInstance() {
		return new SCI();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() throws F8Exception {
		return setTipeAndPrecission(CL.SCI);
	}

	public String toString() {
		return ("SCI");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "DM";
	}
}
