/*
 * Created on 13-sep-2003
 *
 
 
 */
package f8.apps;

import f8.apps.display.CSys;
import f8.objects.types.Matrix;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface LCDControl {
	public abstract void setOrigin(CSys.Point c);

	public abstract void setMark(CSys.Point c);

	public abstract void lineTo(CSys.Point c1, CSys.Point c2);

	public abstract void circle(CSys.Point c, CSys.Point r);

	public abstract void box(CSys.Point c1, CSys.Point c2);

	public abstract void subs();

	public abstract void repl();

	public abstract void label();

	public abstract void clear();

	public abstract CSys.Point getCursor();

	public abstract CSys.Point getMark();

	public abstract boolean isMarkOn();

	public abstract void showAxes();

	public abstract void showLabels(String mx, String my, String Mx, String My);

	public abstract void setProjectation(Matrix pm);

	public abstract void initPoint(CSys.Point p);

	public abstract void nextPoint(CSys.Point o);

	public abstract void setPlotType(int tipo);

	public final int PT_FUNCTION = 0;

	public final int PT_CONIC = 1;

	public final int PT_POLAR = 2;

	public final int PT_PARAMETRIC = 3;

	public final int PT_TRUTH = 4;

	public final int PT_BAR = 5;

	public final int PT_HISTOGRAM = 6;

	public final int PT_SCATTER = 7;

	public final int PT_PROJECTION = 8;

	public abstract void setConnected(boolean c);

	public abstract void reset(double _ancho, double _alto, double ox, double oy);

}
