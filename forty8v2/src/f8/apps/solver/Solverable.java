/*
 * Created on 07-oct-2003
 *
 
 
 */
package f8.apps.solver;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface Solverable {
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public abstract boolean equals(Object obj);

	public abstract boolean tolerance(Solverable obj, double tol);

	public abstract Solverable zero();

	public abstract int sign();

	public abstract double compare(Solverable obj);

	public abstract Solverable mean(Solverable o1, Solverable o2);

	public abstract Solverable amplia(double pct);

}
