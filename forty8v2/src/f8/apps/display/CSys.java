/*
 * Created on 08-oct-2003
 *
 
 
 */
package f8.apps.display;

import f8.Hashtable;
import f8.UtilFactory;
import f8.exceptions.F8Exception;
import f8.objects.types.Matrix;

/**
 * @author elinares Gestiona sistemas de coordenadas de dimension generica y las
 *         trasformaciones de coordenadas de puntos y vectores. Supongamos el
 *         siguiente escenario que contiene 2 sistemas de coordenadas, uno "lcd"
 *         y otro "usr" x O1 +-------> | | ^ y | |y' .P | | | +-----> x' V O2
 * 
 * El punto P tiene dos representaciones de coordenadas, una usando O1 y otra
 * usando O2; respectivamente (Px,Py) y (Px',Py'). Esta clase gestiona la
 * transformación de las mismas de la siguente forma:
 * 
 * CSys lcd=new CSys("lcd"), usr=new CSys("usr");
 * 
 * Creamos la transformación:
 * 
 * [[1 0] [[O2x] lcd.setTransform("usr", [0 -1]], [O2y]] );
 * 
 * Siendo las matrices de rotacion y desplazamiento respectivamente segun la ley
 * general del algebra.
 * 
 * Ejemplos de uso: CSys.Point p=lcd.coor(1,2); CSys.Vector v=usr.vector(0,2);
 * CSys.Point r=p.plus(v);
 * 
 * Estamos sumando a un punto 'p' de coordenadas 1,2 en el sistema de
 * coordenadas lcd, el vector v que esta definido en el sistema de coordenadas
 * usr. El resultado por defecto se transforma al sistema inicial (lcd).
 * 
 */
public class CSys {
	private static Hashtable linked = UtilFactory.newHashtable();

	private Vector[] esq = new Vector[4];

	private String nombre;

	public CSys(String name) {
		nombre = name;
		linked.put(nombre, this);
	}

	public static CSys getCoorSystem(String s) {
		return (CSys) linked.get(s);
	}

	public Point bottom() {
		try {
			return esq[2].plus(esq[3]).div(2).toPoint();
		} catch (F8Exception e) {
			return null;
		}
	}

	public Point coor(double x, double y) {
		double[] c = new double[2];
		c[0] = x;
		c[1] = y;
		return new Point(c);
	}

	public Point coor(double x, double y, double z) {
		double[] c = new double[3];
		c[0] = x;
		c[1] = y;
		c[2] = z;
		return new Point(c);
	}

	public Point coor(double[] p) {
		double[] dret = new double[p.length];
		for (int i = 0; i < p.length; i++) {
			dret[i] = p[i];
		}
		return new Point(dret);
	}

	public String getCSysName() {
		return nombre;
	}

	public Transform getTransform(Point in) {
		return (Transform) linked.get(in.getCSysName() + "." + nombre);
	}

	public Transform getTransform(Vector in) {
		return (Transform) linked.get(in.getCSysName() + "." + nombre);
	}

	public double height() {
		try {
			return esq[0].minus(esq[2]).abs();
		} catch (F8Exception e) {
			return -1;
		}
	}

	public Point left() {
		try {
			return esq[0].plus(esq[2]).div(2).toPoint();
		} catch (F8Exception e) {
			return null;
		}
	}

	public Point right() {
		try {
			return esq[1].plus(esq[3]).div(2).toPoint();
		} catch (F8Exception e) {
			return null;
		}
	}

	public void setRectangle(Vector ul, Vector ur, Vector dl, Vector dr) {
		esq[0] = ul;
		esq[1] = ur;
		esq[2] = dl;
		esq[3] = dr;
	}

	public void setTransform(String other, double[][] rotate, double[][] despl)
			throws F8Exception {
		Transform direct = new Transform(rotate, despl);
		Transform inverse = direct.inv();
		linked.put(nombre + "." + other, direct);
		linked.put(other + "." + nombre, inverse);
	}

	public Point top() {
		try {
			return esq[0].plus(esq[1]).div(2).toPoint();
		} catch (F8Exception e) {
			return null;
		}
	}

	public Point transform(Point input) throws F8Exception {
		if (nombre.equals(input.getCSysName())) {
			return input;
		}
		Transform t = getTransform(input);
		Matrix Minput = input.getAsMatrix();
		return new Point(t.rotacion.times(Minput).plus(t.translacion));
	}

	public Vector transform(Vector input) throws F8Exception {
		if (nombre.equals(input.getCSysName())) {
			return input;
		}
		Transform t = getTransform(input);
		Matrix Minput = input.getAsMatrix();
		return new Vector(t.rotacion.times(Minput));
	}

	public Vector vector(double x, double y) {
		double[] c = new double[2];
		c[0] = x;
		c[1] = y;
		return new Vector(c);
	}

	public Vector vector(double x, double y, double z) {
		double[] c = new double[3];
		c[0] = x;
		c[1] = y;
		c[2] = z;
		return new Vector(c);
	}

	public double width() {
		try {
			return esq[0].minus(esq[1]).abs();
		} catch (F8Exception e) {
			return -1;
		}
	}

	public abstract class BPV {
		public double[] v;

		protected BPV(double[] p) {
			v = p;
		}

		protected BPV(Matrix m) {
			v = new double[m.r];
			for (int i = 0; i < m.r; i++) {
				v[i] = m.x[i][0];
			}
		}

		public boolean equals(BPV c) {
			for (int i = 0; i < c.v.length; i++) {
				if (v[i] != c.v[i]) {
					return false;
				}
			}
			return true;
		}

		public Matrix getAsMatrix() {
			double[][] aux = new double[v.length][1];
			for (int i = 0; i < v.length; i++) {
				aux[i][0] = v[i];
			}
			return new Matrix(aux);
		}

		public String getCSysName() {
			return nombre;
		}
	}

	public class Point extends BPV {
		protected Point(double[] p) {
			super(p);
		}

		protected Point(Matrix m) {
			super(m);
		}

		public Point copy() {
			double[] dret = new double[v.length];
			for (int i = 0; i < v.length; i++) {
				dret[i] = v[i];
			}
			return new Point(dret);
		}

		public Point minus(Vector coor) throws F8Exception {
			if (!nombre.equals(coor.getCSysName())) {
				coor = transform(coor);
			}
			double[] dret = new double[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i] - coor.v[i];
			}
			return new Point(dret);
		}

		public Point plus(Vector coor) throws F8Exception {
			if (!nombre.equals(coor.getCSysName())) {
				coor = transform(coor);
			}
			double[] dret = new double[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i] + coor.v[i];
			}
			return new Point(dret);
		}
	}

	public class Transform {
		Matrix rotacion;

		Matrix translacion;

		public Transform(double[][] m, double[][] v) {
			rotacion = new Matrix(m);
			translacion = new Matrix(v);
		}

		public Transform inv() throws F8Exception {
			Matrix Tprima = rotacion.inverse();
			Matrix oprima = Tprima.times(translacion).uminus();
			return new Transform(Tprima.x, oprima.x);
		}
	}

	public class Vector extends BPV {
		protected Vector(double[] p) {
			super(p);
		}

		protected Vector(Matrix m) {
			super(m);
		}

		public double abs() {
			double ret = 0;
			for (int i = 0; i < v.length; i++) {
				ret += (v[i] * v[i]);
			}
			return Math.sqrt(ret);
		}

		public Vector div(double d) {
			double[] dret = new double[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i] / d;
			}
			return new Vector(dret);
		}

		public Vector minus(Vector coor) throws F8Exception {
			if (!nombre.equals(coor.getCSysName())) {
				coor = transform(coor);
			}
			double[] dret = new double[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i] - coor.v[i];
			}
			return new Vector(dret);
		}

		public Vector plus(Vector coor) throws F8Exception {
			if (!nombre.equals(coor.getCSysName())) {
				coor = transform(coor);
			}
			double[] dret = new double[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i] + coor.v[i];
			}
			return new Vector(dret);
		}

		public Vector mul(double p) {
			double[] dret = new double[v.length];
			for (int i = 0; i < v.length; i++)
				dret[i] = v[i] * p;
			return new Vector(dret);
		}

		public Point toPoint() {
			return new Point(v);
		}
	}
}
