package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: CharStreamIOException.java,v 1.1 2004/03/13 10:34:46 elinares Exp $
 */

/**
 * Wrap an IOException in a CharStreamException
 */
public class CharStreamIOException extends CharStreamException {
	public Exception io;

	public CharStreamIOException(Exception io) {
		super(io.getMessage());
		this.io = io;
	}
}
