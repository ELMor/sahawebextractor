package antlr.collections;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ASTEnumeration.java,v 1.1 2004/03/13 10:34:48 elinares Exp $
 */
public interface ASTEnumeration {
	public boolean hasMoreNodes();

	public AST nextNode();
}
