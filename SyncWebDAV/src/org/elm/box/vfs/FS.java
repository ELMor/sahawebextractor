package org.elm.box.vfs;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;


public abstract class FS {
	private byte[] buffer=new byte[4*1024*1024];
	String usr,pwd;
	String root;
	/**
	 * init() debe comprobar que root existe y es un directorio
	 * En caso de que no exista init() debe emitir una Excepcion
	 * @param root
	 * @param usr
	 * @param pwd
	 * @throws Exception
	 */
	public FS(String root,String usr, String pwd) throws Exception{
		this.usr=usr;
		this.pwd=pwd;
		this.root=root;
	}
	
	public Node getRoot(){
		Node ret=load("");
		ret.parent=null;
		return ret;
	}
	
	public abstract Node load(String relPath);
	
	public abstract void putFile(String relPath, byte[] content) throws Exception;
	
	public abstract InputStream getFile(String relPath) throws Exception;
	
	public abstract String norm(String relPath);
	
	public abstract void mkdirs(Node n) throws Exception;
	
	public void synchWith(FS dst, Vector<Node> src) throws Exception{
		for(Node n:src){
			if(n.isDir()){
				dst.mkdirs(n);
				synchWith(dst, n.childs);
			}else{
				String rp=n.getRelPath();
				byte[] fileContent=fromInputStream(this.getFile(rp));
				dst.putFile(rp, fileContent);
			}
		}
	}
	
	protected byte[] fromInputStream(InputStream is) throws IOException{
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		int len;
		while((len=is.read(buffer))>=0)
			baos.write(buffer,0,len);
		return baos.toByteArray();
	}
	
}
