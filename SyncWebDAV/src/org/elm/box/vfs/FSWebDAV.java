package org.elm.box.vfs;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.client.methods.DavMethod;
import org.apache.jackrabbit.webdav.client.methods.MkColMethod;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.apache.jackrabbit.webdav.client.methods.PutMethod;
import org.apache.jackrabbit.webdav.property.DavProperty;
import org.apache.jackrabbit.webdav.property.DavPropertyName;
import org.apache.jackrabbit.webdav.property.DavPropertyNameSet;
import org.apache.jackrabbit.webdav.property.DavPropertySet;

public class FSWebDAV extends FS {
	HttpClient client;
	private final static String UNIXDIR="httpd/unix-directory";
	private DavPropertyNameSet nameSet;
	
	public FSWebDAV(String root, String usr, String pwd) throws Exception {
		super(root, usr, pwd);
		initClient();

        if(load("")==null)
        	throw new RuntimeException(root+" no existe");
	}

	private void initClient() {
		nameSet=new DavPropertyNameSet();
        nameSet.add(DavPropertyName.create( DavConstants.PROPERTY_DISPLAYNAME));
        nameSet.add(DavPropertyName.create( DavConstants.PROPERTY_GETCONTENTLENGTH ));
        nameSet.add(DavPropertyName.create( DavConstants.PROPERTY_GETLASTMODIFIED ));
        nameSet.add(DavPropertyName.create( DavConstants.PROPERTY_GETCONTENTTYPE));
		HostConfiguration hostConfig = new HostConfiguration();
        hostConfig.setHost(root); 
        HttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
        HttpConnectionManagerParams params = new HttpConnectionManagerParams();
        int maxHostConnections = 20;
        params.setMaxConnectionsPerHost(hostConfig, maxHostConnections);
        connectionManager.setParams(params);    
        client = new HttpClient(connectionManager);
        Credentials creds = new UsernamePasswordCredentials(usr, pwd);
        client.getState().setCredentials(AuthScope.ANY, creds);
        client.setHostConfiguration(hostConfig);
	}

	@Override
	public Node load(String relPath) {
		
		//Nombre del nodo es la ultima parte
		Node ret=new Node(relPath, -1, -1);
		String fullPath=norm(root+relPath);
		
		try {
			DavMethod method= new PropFindMethod( fullPath, nameSet, DavConstants.DEPTH_1 );
			client.executeMethod(method);
			if(method.succeeded()){
                MultiStatus multiStatus = method.getResponseBodyAsMultiStatus(); 
                MultiStatusResponse responses[]=multiStatus.getResponses();
                MultiStatusResponse response = responses[0]; 
                DavPropertySet props=response.getProperties(200);
                String type=dpts(props, DavPropertyName.GETCONTENTTYPE);
                if(type.equals(UNIXDIR)){
                	for(int i=1;i<responses.length;i++){
                		response=responses[i];
                		props=response.getProperties(200);
                		String childName=dpts(props, DavPropertyName.DISPLAYNAME);
                		type=dpts(props, DavPropertyName.GETCONTENTTYPE);
                		if(!type.equalsIgnoreCase(UNIXDIR)){
                			ret.addChild(buildFromProps(props));
                		}else{
	                		ret.addChild(load(relPath+"/"+childName));
                		}
                	}
                }else{
    				return buildFromProps(props);
                }
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret=null;
		}
		return ret;
	}
	
	private String dpts(DavPropertySet dps, DavPropertyName dpn){
		DavProperty<?> dp=dps.get(dpn);
		Object val=dp.getValue();
		return val.toString();
	}
	
	private Node buildFromProps(DavPropertySet props){
		try {
			String nname=dpts(props,DavPropertyName.DISPLAYNAME);
			String retSize= dpts(props, DavPropertyName.GETCONTENTLENGTH);
			String retMod=dpts(props, DavPropertyName.GETLASTMODIFIED);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
			return new Node(nname,Long.parseLong(retSize),sdf.parse(retMod).getTime());
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void putFile(String relPath, byte[] fileContent) throws Exception {
		initClient();
		String fnam=norm(root+relPath);
		System.out.println("Uploading "+fnam);
		
		PutMethod pm=new PutMethod(fnam);
		RequestEntity re=new ByteArrayRequestEntity(fileContent);
		pm.setRequestEntity(re);
		client.executeMethod(pm);
		if(!pm.succeeded()){
			System.out.println(pm.getStatusText()+"("+pm.getStatusCode()+")");
		}
	}

	@Override
	public InputStream getFile(String relPath) throws Exception {
		String fnam=norm(root+relPath);
		System.out.println("Downloading "+fnam);
		HttpMethod gm=new GetMethod(fnam);
		client.executeMethod(gm);
		return gm.getResponseBodyAsStream();
	}

	@Override
	public String norm(String relPath) {
		relPath=relPath.replace("\\", "/");
		relPath=relPath.replace(" ", "%20");
		return relPath;
	}

	@Override
	public void mkdirs(Node n) throws IOException {
		try {
			String dir=root+norm(n.getRelPath());
			System.out.println("Creando dir:"+dir);
			DavMethod mkcol=new MkColMethod(dir);
			client.executeMethod(mkcol);
			if(!mkcol.succeeded()){
				throw new RuntimeException(mkcol.getStatusText());
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
}
