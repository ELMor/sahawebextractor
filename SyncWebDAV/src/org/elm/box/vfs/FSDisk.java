package org.elm.box.vfs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class FSDisk extends FS {

	public FSDisk(String root) throws Exception{
		this(root,null,null);
	}
	
	public FSDisk(String root, String usr, String pwd) throws Exception {
		super(root, usr, pwd);
		File f=new File(root);
		if(!f.exists() || !f.isDirectory()){
			throw new RuntimeException("No es un directorio: "+root);
		}
		root=f.getCanonicalPath();
	}

	@Override
	public Node load(String relPath) {
		File f=new File(root+File.separator+relPath);
		if(!f.exists()){
			return null;
		}
		Node ret=new Node(f.getName(),-1,-1);
		if(f.isFile()){
			ret.size=f.length();
			ret.modified=f.lastModified();
		}else{
			File[] children=f.listFiles();
			for(File child:children){
				ret.addChild(load(relPath+File.separator+child.getName()));
			}
		}
		return ret;
	}

	@Override
	public void putFile(String relPath, byte[] content) throws Exception {
		String fnam=norm(root+File.separator+relPath);
		System.out.println("Uploading "+fnam);
		File f=new File(fnam);
		FileOutputStream fos=new FileOutputStream(f);
		fos.write(content);
		fos.close();
	}

	@Override
	public InputStream getFile(String relPath) throws Exception {
		String fnam=norm(root+File.separator+relPath);
		System.out.println("Downloading "+fnam);
		File f=new File(fnam);
		
		return new FileInputStream(f);
	}

	@Override
	public String norm(String relPath) {
		relPath=relPath.replace("/", "\\");
		relPath=relPath.replace("%20", " ");
		return relPath;
	}

	@Override
	public void mkdirs(Node n) {
		new File(root+File.separator+norm(n.getRelPath())).mkdirs();
	}

}
