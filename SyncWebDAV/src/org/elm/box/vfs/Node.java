package org.elm.box.vfs;

import java.util.Collections;
import java.util.Vector;

public class Node implements Comparable<Node> {

	public Node parent;
	public Vector<Node> childs;
	private String name;
	long size;
	long modified;

	public Node(String nombre, long tamano, long modificado) {
		parent = null;
		childs = null;
		name = removeInvalidChars(nombre);
		size = tamano;
		modified = modificado;
	}

	private String removeInvalidChars(String n) {
		n = n.replace("/", "");
		n = n.replace("\\", "");
		return n;
	}

	public Node addChild(Node child) {
		if (childs == null)
			childs = new Vector<Node>();
		childs.add(child);
		child.parent = this;
		return child;
	}

	public boolean isDir() {
		return childs != null;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		recToString(0, this, sb);
		return sb.toString();
	}

	private void recToString(int level, Node n, StringBuffer sb) {
		for (int i = 0; i < level; i++)
			sb.append("--");
		sb.append(">").append(n.name);
		if (n.childs == null) {
			sb.append("(").append(n.size).append(";").append(n.modified)
					.append(")\n");
		} else {
			sb.append("\n");
			for (Node child : n.childs)
				recToString(level + 1, child, sb);
		}
	}

	public Vector<Node> calculateGap(Node with) {
		if (!with.isDir()) {
			// Significa que no existe el directorio destino. O sea, todos los
			// hijos del nodo this deben ser creados
			// en el remote (o sea el with), incluyendo el root.
			return this.childs;
		}

		// Solo comparamos directorios
		if (!isDir())
			throw new RuntimeException(
					"No puedo comparar un archivo con un directorio");

		// With es un directorio pero est� vac�o,
		if (with.childs.size() == 0)
			return this.childs;

		// Ambos son directorios
		Vector<Node> ret = new Vector<Node>();
		Collections.sort(this.childs);
		Collections.sort(with.childs);
		// a retornar
		// Primero del to
		int fromPos = 0, toPos = 0, fromMax = this.childs.size(), toMax = with.childs
				.size();
		while (fromPos < fromMax && toPos < toMax) {
			Node from = this.childs.get(fromPos);
			Node to = with.childs.get(toPos);
			int comp = from.compareTo(to);
			if (comp < 0) {
				ret.add(from);
				fromPos++;
			} else if (comp == 0) {
				if (from.isDir() && to.isDir())
					ret.addAll(from.calculateGap(to));
				fromPos++;
				toPos++;
			} else if (comp > 0) {
				toPos++;
			}
		}
		//Los restantes hay que a�adirlos
		while(fromPos<fromMax)
			ret.add(this.childs.elementAt(fromPos++));
		return ret;
	}

	public int compareTo(Node o) {
		if (o == null)
			return -1;
		if (isDir() != o.isDir())
			return -1;
		// Ambos son ficheros o son directorios.
		int nameComp = name.compareTo(o.name);
		if (isDir() && o.isDir())
			return nameComp;
		if (nameComp != 0)
			return nameComp;
		int modifiedComp = (int) (modified - (o.modified));
		if (modifiedComp != 0)
			return modifiedComp;
		return (int) (size - o.size);
	}

	public String getRelPath() {
		if (parent == null)
			return "";
		return parent.getRelPath() + "/" + name;
	}

}
