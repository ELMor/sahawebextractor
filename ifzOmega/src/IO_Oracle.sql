/* ============================================================ */
/*   Database name:  NOVAHIS                                    */
/*   DBMS name:      Oracle 7.3                                 */
/*   Created on:     04/04/2002  11:36                          */
/* ============================================================ */

create table ifzastm
(
    actividad_det_pk         number                 not null,
    ifz_seq_omega            number                         ,
    ifz_seq_pet              number                         ,
    ifz_fec_envio            varchar2(14)                   ,
    ifz_senvio               varchar2(2)                    ,
    ifz_fec_recep            varchar2(14)                   ,
    ifz_srecep               varchar2(2)                    ,
    constraint PK_IFZASTM primary key (actividad_det_pk)
)
;

alter table ifzastm
    add constraint FK_IFZASTM_RELATION__ACTIVIDA foreign key  (actividad_det_pk)
       references actividad_det (actividad_det_pk)
;

create or replace trigger ti_actividad_det
after insert
on actividad_det
for each row
begin
insert into  ifzastm(actividad_det_pk) values(:new.actividad_det_pk);
end;
/
