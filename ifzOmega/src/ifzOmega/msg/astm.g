header {
package ifzOmega.msg;
import  ifzOmega.msg.reg.*;
}

class ASTM extends Parser;
options {
  k =2;
  exportVocab=ASTM;
  defaultErrorHandler = false;
  codeGenMakeSwitchThreshold = 2;
  codeGenBitsetTestThreshold = 3;
  buildAST=false;
}

//tokens { }

getMsg returns [Msg m]
{
	m=new Msg();
}
  :!
  	hreg[m]
  	(
  		preg[m]
  		(
  			oreg[m]
  			(
  				rreg[m]
  			 |mreg[m]
  			 |creg[m]
  			)+
  		)+
  	)+
  	lreg[m]
 ;

hreg[Msg m]
{
	H h=null;
}
	:
		HHEA 							{ h=new H(); m.add(h); }
		fields[h]
		NL
	;

preg[Msg m]
{
	P p=null;
}
	:
		PHEA 							{ p=new P(); m.add(p); }
		fields[p]
		NL
	;

oreg[Msg m]
{
	O o=null;
}
	:
		OHEA 							{ o=new O(); m.add(o); }
		fields[o]
		NL
	;

rreg[Msg m]
{
	R r=null;
}
	:
		RHEA 							{ r=new R(); m.add(r); }
		fields[r]
		NL
	;

mreg[Msg msg]
{
	M m=null;
}
	:
		MHEA							{ m=new M(); msg.add(m); }
		fields[m]
		NL
	;

creg[Msg m]
{
	C c=null;
	int pos=2;
}
	:
		CHEA 							{ c=new C(); m.add(c); }
		fields[c]
		NL
	;

lreg[Msg m]
{
	L l=null;
}
	:
		LHEA 							{ l=new L(); m.add(l); }
		fields[l]
		( NL | EOF )
	;

fields[gen m]
{
	int pos=2;
}
	:
		(f0:FIELD					{m.setField(pos++,f0.getText());}
		)+
	;

/////////////////////////////////////////////////////////////////////////////
class ASTMlexer extends Lexer;
options {
  importVocab=ASTM;
  testLiterals=true;
  k=2;
  charVocabulary='\u0003'..'\uFFFF';
  codeGenBitsetTestThreshold=20;
  caseSensitive=false;
  caseSensitiveLiterals=false;
}

// Whitespace -- ignored
WS
	:	(   ' '
			|	'\t'
			|	'\f'
    )+
    { _ttype = Token.SKIP; }
  ;


NL
  : ("\r\n"| '\r'| '\n')
    {newline();}
  ;

FIELD
	:
		'|' (~('|'|'\r'|'\n'))*
	;


HHEA : 'h';
PHEA : 'p';
OHEA : 'o';
RHEA : 'r';
CHEA : 'c';
MHEA : 'm';
LHEA : 'l';

