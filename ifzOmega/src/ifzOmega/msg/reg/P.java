package ifzOmega.msg.reg;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class P extends gen {

  public static final int seqPac= 2;
  public static final int numPet= 3;
  public static final int hisCli= 4;
  public static final int nomPac= 6;
  public static final int fecNac= 8;
  public static final int sexPac= 9;
  public static final int medPac=14;
  public static final int fecAdm=24;
  public static final int estAdm=25;
  public static final int serPac=33;
  public static final int hosCod=34;

  public P() {
    super("P",34);
  }

}
