package ifzOmega.run;
import ifzOmega.run.DBConnection;
import ifzOmega.msg.*;
import ifzOmega.msg.reg.*;
import java.sql.*;
import java.io.*;
import java.text.*;

/**
 * Envio de peticione a Omega. S�lo esta implementado el envio, no la anulacion
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author unascribed
 * @version 1.0
 */

public class dirWriter extends Thread {
  DBConnection conn=null;
  String initPK=null;
  String dirW;

  public dirWriter(int opt[],
                  String dbtype, String surl, String login, String pass,
                  String dw, String ip)
  {
    try {
      conn=new DBConnection(opt,dbtype,surl,login,pass);
      initPK=ip;
      dirW=dw;
    }catch(Exception e){
      e.printStackTrace();
      System.exit(-1);
    }
  }

  /**
   * Devuelve "" si la cadena es nula
   * @param k Valor del campo
   * @return k si no es nulo, "" si es nulo
   */
  private String nvl(String k){
    if(k==null)
      return "";
    return k;
  }

  public void run(){
    try {
      for(;;){
        ResultSet rs=conn.getOrders();
        if( rs.next() )
          processOrder( rs );
        Thread.sleep(30000);
      }
    }catch(Exception e){
      System.err.println("Mientras se intentaba obtener el ResultSet:");
      e.printStackTrace();
      return;
    }
  }


  /**
   * Creacion de fichero con las peticiones
   * @param ords ResultSet con los prest_item_cod solicitados
   */
  private void processOrder(ResultSet ords){
    Msg msg=new Msg();
    //Cabecera
    msg.add( new H() );
    //Variables para los segmentos
    String actividad_pk=null;
    int secuencia=0;
    int secPac=0;
    String seqomega=null;
    SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
    SimpleDateFormat sdf2=new SimpleDateFormat("yyyyMMdd");
    try{
      do {
        if(actividad_pk==null || !actividad_pk.equals(ords.getString("actividad_pk"))){
          secPac++; //Numero de paciente se incrementa
          //PK de actividad, para check si cambia
          actividad_pk=ords.getString("actividad_pk");
          P cab=new P(); //A�adimos la cabecera, hay cambio de paciente
          secuencia=0;   //Inicializamos la secuencia

          //Nueva peticion
          seqomega=conn.getFreshPet(initPK,actividad_pk);
          cab.setField(P.numPet,seqomega);
          //Secuencia de pacientes de este mensaje
          cab.setField(P.seqPac,new Integer(secPac).toString());
          //Fecha y hora de ahora
          cab.setField(P.fecAdm,sdf.format(new java.util.Date()));
          //Procedencia inpatient, outpatient, emergency
          cab.setField(P.estAdm,    ords.getString("area"));  //IP-OP-UR
          //Fecha de nacimiento
          String noRareChar="",rareChar=ords.getString("nac_fecha");
          for(int kk=0;kk<rareChar.length();kk++)
            if( rareChar.charAt(kk)>='0' && rareChar.charAt(kk)<='9')
              noRareChar=noRareChar+rareChar.charAt(kk);

          cab.setField(P.fecNac,    noRareChar);
          //Codigo y nombre del centro
          cab.setField(P.hosCod,    ords.getString("cod_centro")
                               +"^"+ords.getString("nombre_centro").trim()
                               );
          //codigo, nombre y telefono del doctor
          cab.setField(P.medPac,    nvl(ords.getString("cpd")).trim()
                               +"^"+nvl(ords.getString("a1d")).trim()
                               +" "+nvl(ords.getString("nod")).trim()
                               +"^"+nvl(ords.getString("ted"))
                               );
          //Nombre del paciente
          cab.setField(P.nomPac,    nvl(ords.getString("apellido1")).trim()
                               +"^"+nvl(ords.getString("nombre")).trim()
                               +"^"+nvl(ords.getString("apellido2")).trim()
                               );
          cab.setField(P.serPac,    ords.getString("codigo_servicio")
                               +"^"+ords.getString("servicio").trim()
                               );
          cab.setField(P.sexPac,    ords.getString("codigo_sexo"));

          String hc=conn.getHC(     ords.getString("codigo_cliente") );
          cab.setField(P.hisCli,hc);

          //Notificar el cambio de estado a actividad
          //ATENCION:CAMBIAR SERV Y PROF
          /*
          conn.setActStatus(ords.getString("actividad_pk"),"2","361","2");
          */

          //A�adir la cabecera al mensaje
          msg.add(cab);
        }
        secuencia++;
        //A�adimos ahora el O
        O order=new O();

        //Fijo el tipo de actuacion
        order.setField(O.actCod,"A");
        //Codigo de muestra
        String muest=conn.getMuestra( ords.getString("actividad_det_pk") );
        order.setField(O.codMue,muest);
        //Secuencia de la prueba
        order.setField(O.seqOrd,new Integer(secuencia).toString());
        //Secuencia de Omega
        order.setField(O.seqPet,seqomega);
        //Codigo de la prueba pedida
        order.setField(O.testID,    "^^"
                                   +"^"+ords.getString("prest_item_cod")
                                   +"^"+conn.getBoM( ords.getString("prest_item_cod") )
                                   +"^"
                                   +"^"+ords.getString("prest_item_desc")
                                   );
        //Marcamos la tabla ifzastm como enviado
        conn.sendIfzAstm(ords.getString("actividad_det_pk"),
                         seqomega,
                         new Integer(secPac).toString());
        msg.add(order);
      }while(ords.next());
      //Cola
      msg.add(new L());
      //Grabar el fichero en el filesystem
      String fn=sdf.format(new java.util.Date());
      File f=new File( dirW+File.separatorChar+fn+".pet" );
      try {
        FileOutputStream fos=new FileOutputStream( f );
        msg.write(fos);
        fos.close();
      }catch(IOException ioe){
        System.err.println("Mientras escribia "+fn+":");
        ioe.printStackTrace();
        System.err.println("Atenci�n, peticiones NO enviadas");
      }
    }catch(Exception se){
      System.err.println("Mientras se leian datos:");
      se.printStackTrace();
    }
  }
}
