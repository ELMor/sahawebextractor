package javagba.video;

import javagba.IORegConstants;
import javagba.memory.Memory;
import javagba.memory.MemoryRAM;
import javagba.memory.MemoryRegisters;

public abstract class AbstractScanlineRenderer implements IORegConstants {

	public final static int MASK_RED = 0x7c00; // Red component mask

	public final static int MASK_GRN = 0x03e0; // Green component mask

	public final static int MASK_BLU = 0x001f; // Blue component mask

	public final static int OBJ_A0_SHAPE = 0xc000; // OBJ Attribute 0, Shape
													// Mask

	public final static int OBJ_A0_SHAPE_SQUARE = 0x0000; // OBJ Attribute 0,
															// Shape Square

	public final static int OBJ_A0_SHAPE_HRECT = 0x4000; // OBJ Attribute 0,
															// Shape Hor rect

	public final static int OBJ_A0_SHAPE_VRECT = 0x8000; // OBJ Attribute 0,
															// Shape Ver Rect

	public final static int OBJ_A0_256COLOR = 0x2000; // OBJ Attribute 0, 256
														// Color Mode

	public final static int OBJ_A0_MOSAIC = 0x1000; // OBJ Attribute 0, Mosaic
													// Mode

	public final static int OBJ_A0_MODE = 0x0c00; // OBJ Attribute 0, OBJ Mode

	public final static int OBJ_A0_MODE_NORMAL = 0x0000; // OBJ Attribute 0,
															// Normal Mode

	public final static int OBJ_A0_MODE_STRANS = 0x0400; // OBJ Attribute 0,
															// Semi Transparent
															// Mode

	public final static int OBJ_A0_MODE_OBJWIN = 0x0800; // OBJ Attribute 0,
															// OBJ Window Mode

	public final static int OBJ_A0_RS_DOUBLE = 0x0200; // OBJ Attribute 0,
														// Rot/Scale Double

	public final static int OBJ_A0_RS_ENABLE = 0x0100; // OBJ Attribute 0,
														// Rot/Scale Enable

	public final static int OBJ_A0_Y = 0x00ff; // OBJ Attribute 0, Y Coord

	public final static int OBJ_A1_OBJSIZE = 0xc000; // OBJ Attribute 1,
														// Object Size

	public final static int OBJ_A1_VFLIP = 0x2000; // OBJ Attribute 1, Vertical
													// Flip

	public final static int OBJ_A1_HFLIP = 0x1000; // OBJ Attribute 1,
													// Horizontal Flip

	public final static int OBJ_A1_RSPAR = 0x3e00; // OBJ Attribute 1,
													// Rot/Scale Parameter

	public final static int OBJ_A1_X = 0x01ff; // OBJ Attribute 1, X Coord

	public final static int OBJ_A2_PALETTE = 0xf000; // OBJ Attribute 2,
														// Palette Index

	public final static int OBJ_A2_PRIORITY = 0x0c00; // OBJ Attribute 2,
														// Priority

	public final static int OBJ_A2_CHARNAME = 0x03ff; // OBJ Attribute 2,
														// Character Name

	public Renderer renderer; // Renderer reference

	public AbstractScanlineRenderer(Renderer renderer) {

		// Assign renderer
		this.renderer = renderer;
	}

	protected int color15BitsTo24Bits(short color15) {
		// the 15 bits format is "?bbbbbgggggrrrrr"
		int r = ((color15 & 0x001f) >>> 0) << 3;
		int g = ((color15 & 0x03e0) >>> 5) << 3;
		int b = ((color15 & 0x7c00) >>> 10) << 3;
		// the 32 bits format is "11111111rrrrrrrrggggggggbbbbbbbb"
		return (0xff000000 | (r << 16) | (g << 8) | b); // Alpha channel is full
														// value.
	}

	protected void drawBG0TextModeLine(int yScr, int bg, int bgcnt) {
		// Get memory bank reference
		MemoryRAM memoryVRAM = (MemoryRAM) renderer.memory.banks[Memory.MEM_VRAM];
		MemoryRAM memoryPAL = (MemoryRAM) renderer.memory.banks[Memory.MEM_PALETTE];

		// Get pixels reference
		final int[] pixels = renderer.getPixels();

		int xScroll = renderer.registers.getRegister(REG_BG0HOFS + (bg << 2));
		int yScroll = renderer.registers.getRegister(REG_BG0VOFS + (bg << 2));
		int dataAddress = ((bgcnt & BG_CHARBASEBLOCK) >>> 2) * 0x00004000;
		int mapAddress = ((bgcnt & BG_SCREENBASEBLOCK) >>> 8) * 0x00000800;
		int xNumberOfTile = ((bgcnt & BG_SCREENSIZE_X) != 0) ? 64 : 32;
		int yNumberOfTile = ((bgcnt & BG_SCREENSIZE_Y) != 0) ? 64 : 32;
		int xNumberOfTileMask = xNumberOfTile - 1;
		int yNumberOfTileMask = yNumberOfTile - 1;
		boolean is256Color = (bgcnt & BG_COLORMODE) != 0;
		boolean isTileDataUpsideDown = (xNumberOfTile != 32);
		boolean mosaicEnabled = false;// ioMem.isBG0MosaicEnabled();
		int xMosaic = 1;// ioMem.getMosaicBGXSize();
		int yMosaic = 1;// ioMem.getMosaicBGYSize();

		// if (yScr == 159) infoDisplay(0, xScroll, yScroll, mapAddress,
		// dataAddress, xNumberOfTile, yNumberOfTile, is256Color);

		int y = (mosaicEnabled ? yScr - (yScr % yMosaic) : yScr);

		for (int xScr = 0; xScr < 240; xScr++) {
			// This routine acts like a raycasting algorithm.
			// It might be slow, but I DON'T CARE !! :-)
			// Mame rulezz.

			// handle the mosaic effect
			int x = (mosaicEnabled ? xScr - (xScr % xMosaic) : xScr);

			int tileX = (x + xScroll) >>> 3; // the x coordinate of the tile
												// where to find the pixel.
			int tileY = (y + yScroll) >>> 3; // The y coordinate of the tile
												// where to find the pixel.

			// handle the wraparound effect.
			tileX &= xNumberOfTileMask;
			tileY &= yNumberOfTileMask;

			int offsetToAdd = 0;
			if (isTileDataUpsideDown) {
				if (tileX >= 32)
					offsetToAdd += 0x800;
				if (tileY >= 32)
					offsetToAdd += 0x1000;
				tileX &= 0x1f;
				tileY &= 0x1f;
			}

			int tileDataPosInMem = ((tileX + tileY * 32) << 1) + offsetToAdd;
			int tileData = memoryVRAM.readHalfWord(mapAddress
					+ tileDataPosInMem);

			int xSubTile = (x + xScroll) & 0x07;
			int ySubTile = (y + yScroll) & 0x07;

			if ((tileData & 0x0400) != 0)
				xSubTile = 7 - xSubTile; // handle horizontal tile flip
			if ((tileData & 0x0800) != 0)
				ySubTile = 7 - ySubTile; // handle vertical tile flip

			int tileNumber = tileData & 0x3ff;

			if (is256Color) // Tile are encoded in a 1 byte per pixel format
			{
				int color8 = 0xff & memoryVRAM.readByte(dataAddress
						+ tileNumber * 8 * 8 + xSubTile + ySubTile * 8);
				if (color8 != 0) // if color is zero, transparent
				{
					short color15 = memoryPAL.readHalfWord(color8 << 1);
					pixels[xScr + yScr * 240] = color15BitsTo24Bits(color15);
				}
			} else // Tiles are encoded in a 4 bits per pixel format
			{
				int paletteNumber = (tileData & 0xf000) >> 12;
				int color4 = 0xff & memoryVRAM.readByte(dataAddress
						+ tileNumber * 8 * 8 / 2
						+ ((xSubTile + ySubTile * 8) >> 1));
				if ((xSubTile & 0x01) != 0)
					color4 >>>= 4;
				else
					color4 &= 0x0f;
				if (color4 != 0) // if color is zero, transparent
				{
					short color15 = memoryPAL
							.readHalfWord((paletteNumber * 16 + color4) << 1);
					pixels[xScr + yScr * 240] = color15BitsTo24Bits(color15);
				}
			}

		}
	}

	public void renderBackgroundScanline(int index) {

		// Get pixel data reference
		final int[] pixels = renderer.getPixels();

		// Get fill color
		int pixel = renderer.memory.banks[Memory.MEM_PALETTE].readHalfWord(0);

		// Convert to 24 bits color
		int r = (pixel & MASK_BLU) << 19;
		int g = (pixel & MASK_GRN) << 6;
		int b = (pixel & MASK_RED) >>> 7;

		// Store pixel
		int color = 0xff000000 | r | g | b;

		// Fill scanline
		for (int i = index * Renderer.RES_X_SCREEN; i < (index + 1)
				* Renderer.RES_X_SCREEN; i++)
			pixels[i] = color;
	}

	public void renderScaleRotateModeLine(int index, int bg, int bgcnt) {
	}

	/**
	 * 
	 * @param index
	 *            int
	 */
	public abstract void renderScanline(int index);

	public void renderSpriteLine(int index, int priority, int offset) {

		// Initialise locals
		short objAttr0, objAttr1, objAttr2, objRotSc;
		int objX, objY, paletteIndex, objSize, objShape, objSizeX = 0, objSizeY = 0, sizeX, sizeY;
		boolean rotateScale, doubleSize, mosaic;

		// Get memory bank reference
		MemoryRAM memoryVRAM = (MemoryRAM) renderer.memory.banks[Memory.MEM_VRAM];
		MemoryRAM memoryPAL = (MemoryRAM) renderer.memory.banks[Memory.MEM_PALETTE];
		MemoryRAM memoryOAM = (MemoryRAM) renderer.memory.banks[Memory.MEM_OAM];
		MemoryRegisters registers = (MemoryRegisters) renderer.memory.banks[Memory.MEM_IOREG];

		// Get palette address
		int paletteAddress = 0x00000200;

		// Get pixels reference
		final int[] pixels = renderer.getPixels();

		// Get maximum number of OBJ to render
		int maxObjCount = 128;// (Renderer.RES_X_SCREEN * 4 - 6) / 8;

		// Process sprites
		for (int objIndex = 0; objIndex < maxObjCount; objIndex++) {

			// Get OBJ attributes and Rotation/Sale params
			objAttr0 = memoryOAM.readHalfWord((objIndex << 3));
			objAttr1 = memoryOAM.readHalfWord((objIndex << 3) + 2);
			objAttr2 = memoryOAM.readHalfWord((objIndex << 3) + 4);
			objRotSc = memoryOAM.readHalfWord((objIndex << 3) + 6);

			// Get object priority
			if ((objAttr2 & OBJ_A2_PRIORITY) >>> 10 != priority)
				continue;

			// Get OBJ coord
			objX = objAttr1 & OBJ_A1_X;
			objY = objAttr0 & OBJ_A0_Y;

			// Get OBJ flags
			rotateScale = (objAttr0 & OBJ_A0_RS_ENABLE) != 0;
			doubleSize = (objAttr0 & OBJ_A0_RS_DOUBLE) != 0;
			mosaic = (objAttr0 & OBJ_A0_MOSAIC) != 0;

			// Get palette info
			paletteIndex = (objAttr2 & OBJ_A2_PALETTE) >>> 12;

			// Get size info
			objSize = (objAttr1 & OBJ_A1_OBJSIZE) >>> 14;
			objShape = (objAttr0 & OBJ_A0_SHAPE) >>> 14;

			// Get mosaic pars
			int mosaicReg = registers.getRegister(REG_MOSAIC);
			int mosaicX = (mosaicReg & MOSAIC_OBJ_X) >>> 8;
			int mosaicY = (mosaicReg & MOSAIC_OBJ_Y) >>> 12;

			// Get tile index
			int tileIndex = objAttr2 & OBJ_A2_CHARNAME;

			// Get sizes
			switch (objShape) {

			case OBJ_A0_SHAPE_SQUARE:

				objSizeX = objSizeY = 1 << objSize;
				break;

			case OBJ_A0_SHAPE_HRECT:

				objSizeX = objSize < 2 ? objSizeX = 2 << objSize : 1 << objSize;
				objSizeY = objSize < 1 ? objSizeY = 1 : 1 << (objSize - 1);
				break;

			case OBJ_A0_SHAPE_VRECT:

				objSizeX = objSize < 1 ? objSizeX = 1 : 1 << (objSize - 1);
				objSizeY = objSize < 2 ? objSizeY = 2 << objSize : 1 << objSize;
				break;
			}

			// Get sizes in pixels
			sizeX = objSizeX << 3;
			sizeY = objSizeY << 3;

			// Render rotation/scaling OBJ
			if (rotateScale) {
			}

			// Render normal OBJ
			else {

				// Handle non double sized OBJ
				if (!doubleSize) {

					// Get color mode
					boolean color256 = (objAttr0 & OBJ_A0_256COLOR) != 0;

					// Get mapping mode
					boolean oneDimMapping = (registers.getRegister(REG_DISPCNT) & DISPCNT_OBJCHARMAP_F) != 0;

					// Get vertical tile increase value
					int tileAddY = oneDimMapping ? (color256 ? (objSizeX << 1)
							: objSizeX) : 32;

					// Get flip flags
					boolean flipH = (objAttr1 & OBJ_A1_HFLIP) != 0;
					boolean flipV = (objAttr1 & OBJ_A1_VFLIP) != 0;

					// Make sure this sprite is visiblie on this scanline
					if (index >= objY && index < objY + (objSizeY << 3)) {

						// Get tile Y coord
						int y = index - objY;

						// Adjust tile index if needed
						if (color256 && !oneDimMapping)
							tileIndex &= 0xfffe;

						// Render OBJ span
						for (int posX = 0; posX < objSizeX << 3; posX++) {

							// Get time X coord
							int x = objX + posX;

							// Make sure this pixel is visible
							if (x >= 0 && x < Renderer.RES_X_SCREEN) {

								// Get actual coords
								int ax = (flipH ? (objSizeX << 3) - 1 - posX
										: posX);
								int ay = (flipV ? (objSizeY << 3) - 1 - y : y);

								// Get tile coords
								int tx = ax >>> 3;
								int ty = ay >>> 3;

								// Get sub tile coords
								int sx = ax & 0x07;
								int sy = ay & 0x07;

								// Render 256 color mode pixel
								if (color256) {

									// Get tile number
									int tileNumber = (tileIndex + (tx << 1) + (ty * tileAddY));

									// Get palette entry
									int pixelIndex = 0xff & memoryVRAM
											.readByte(offset
													+ (tileNumber << 5)
													+ (sy << 3) + sx);

									// If not transparent write pixel
									if (pixelIndex != 0) {

										// Get color
										short pixel = memoryPAL
												.readHalfWord(paletteAddress
														+ (pixelIndex << 1));

										// Convert to 24 bits color
										int r = (pixel & MASK_BLU) << 19;
										int g = (pixel & MASK_GRN) << 6;
										int b = (pixel & MASK_RED) >>> 7;

										// Store pixel
										int color = 0xff000000 | r | g | b;
										pixels[x + index
												* Renderer.RES_X_SCREEN] = color;
									}
								}

								else {

									// Get tile number
									int tileNumber = (tileIndex + tx + ty
											* tileAddY);

									// Get palette entry
									int pixelIndex = ((0xff & memoryVRAM
											.readByte(offset
													+ (tileNumber << 5)
													+ (sx >>> 1) + (sy << 2))) >>> ((sx & 0x1) << 2)) & 0xf;

									// If not transparent render pixel
									if (pixelIndex != 0) {

										// Get color
										short pixel = memoryPAL
												.readHalfWord(paletteAddress
														+ (paletteIndex << 5)
														+ (pixelIndex << 1));

										// Convert to 24 bits color
										int r = (pixel & MASK_BLU) << 19;
										int g = (pixel & MASK_GRN) << 6;
										int b = (pixel & MASK_RED) >>> 7;

										// Store pixel
										int color = 0xff000000 | r | g | b;
										pixels[x + index
												* Renderer.RES_X_SCREEN] = color;
									}
								}
							}
						}
					}
				}
			}
		}

		// int nbSprite = 128;
		// for (int i = nbSprite - 1; i >= 0 ; i--)
		// {
		// if (priority == sMem.getSpritePriority(i))
		// {
		// // Draw the sprite
		// boolean rotScalEnabled = sMem.isRotScalEnabled(i);
		// boolean doubleSizeEnabled = sMem.isDoubleSizeEnabled(i);
		// int xNbTile = sMem.getSpriteNumberXTile(i);
		// int yNbTile = sMem.getSpriteNumberYTile(i);
		// int xSize = xNbTile * 8;
		// int ySize = yNbTile * 8;
		// int paletteAddress = 0x00000200;
		// int paletteNumber = sMem.getPal16Number(i);
		// int xPos = sMem.getSpriteXPos(i);
		// int yPos = sMem.getSpriteYPos(i);
		// boolean mosaicEnabled = sMem.isMosaicEnabled(i);
		// int xMosaic = ioMem.getMosaicOBJXSize();
		// int yMosaic = ioMem.getMosaicOBJYSize();
		// int tileBaseNumber = sMem.getTileNumber(i);

		// {
		// if (!doubleSizeEnabled)
		// {
		// boolean is256Color = sMem.is256Color(i);
		// int tileNumberYIncr = (ioMem.is1DMappingMode() ?
		// (is256Color ? xNbTile * 2 : xNbTile)
		// : 32);
		// boolean hFlip = sMem.isHFlipEnabled(i);
		// boolean vFlip = sMem.isVFlipEnabled(i);

		// if ((yScr >= yPos) && (yScr < yPos + yNbTile * 8))
		// {
		// int y = yScr - yPos;
		// if (is256Color && !ioMem.is1DMappingMode()) tileBaseNumber &= 0xfffe;

		// for (int x = 0; x < xNbTile * 8; x++)
		// {
		// int xScr = xPos + x;
		// if ((xScr >= 0) && (xScr < 240))
		// {
		// int x2 = (hFlip ? xNbTile * 8 - 1 - x : x); // handle horizontal tile
		// flip
		// int y2 = (vFlip ? yNbTile * 8 - 1 - y : y); // handle vertical tile
		// flip

		// int xTile = x2 >>> 3;
		// int yTile = y2 >>> 3;
		// int xSubTile = x2 & 0x7;
		// int ySubTile = y2 & 0x7;

		// if (is256Color)
		// {
		// int tileNumber = (tileBaseNumber + xTile * 2 + yTile *
		// tileNumberYIncr);
		// int color8 = 0xff & vMem.hardwareAccessLoadByte(tileDataAddress +
		// tileNumber * 8*8/2 +
		// xSubTile + ySubTile * 8);
		// if (color8 != 0) // if color is zero, transparent
		// {
		// short color15 = pMem.hardwareAccessLoadHalfWord(paletteAddress +
		// (color8 << 1));
		// rawPixels[xScr + yScr * 240] = color15BitsTo24Bits(color15);
		// }
		// }
		// else
		// {
		// int tileNumber = (tileBaseNumber + xTile + yTile * tileNumberYIncr);
		// int color8 = 0xff & vMem.hardwareAccessLoadByte(tileDataAddress +
		// tileNumber * 8*8/2 +
		// (xSubTile >>> 1) + ySubTile * 4);
		// int color4 = (color8 >>> ((xSubTile & 0x1) * 4)) & 0xf;
		// if (color4 != 0) // if color is zero, transparent
		// {
		// short color15 = pMem.hardwareAccessLoadHalfWord(paletteAddress +
		// ((paletteNumber * 16) << 1)+
		// (color4 << 1));
		// rawPixels[xScr + yScr * 240] = color15BitsTo24Bits(color15);
		// }
		// }
		// }
		// }
		// }
		// }
		// }

		// /*
		// System.out.println("sprite " + i + " xNbTile = " + xNbTile);
		// System.out.println("sprite " + i + " yNbTile = " + yNbTile);
		// System.out.println("sprite " + i + " xPos = " + xPos);
		// System.out.println("sprite " + i + " yPos = " + yPos);
		// System.out.println("sprite " + i + " tileNumber = " + tileNumber);
		// System.out.println("sprite " + i + " dataAddress = " + dataAddress);
		// System.out.println("sprite " + i + " paletteAddress = " +
		// paletteAddress);
		// */
		// }
		// }
	}

	public void renderTextModeLine(int index, int bg, int bgcnt) {

		int pixelIndex;

		// Get mosaic parameters
		int mosaic = renderer.registers.getRegister(REG_MOSAIC);
		int mosaicX = (mosaic & MOSAIC_BG_X);
		int mosaicY = (mosaic & MOSAIC_BG_Y) >>> 4;

		// Get memory bank reference
		MemoryRAM memoryVRAM = (MemoryRAM) renderer.memory.banks[Memory.MEM_VRAM];
		MemoryRAM memoryPAL = (MemoryRAM) renderer.memory.banks[Memory.MEM_PALETTE];

		// Get pixels reference
		final int[] pixels = renderer.getPixels();

		// Get BG scroll values
		int scrollX = renderer.registers.getRegister(REG_BG0HOFS + (bg << 2));
		int scrollY = renderer.registers.getRegister(REG_BG0VOFS + (bg << 2));

		// Get tile data address
		int tileDataAddress = ((bgcnt & BG_CHARBASEBLOCK) >>> 2) * 0x00004000;
		int tileMapAddress = ((bgcnt & BG_SCREENBASEBLOCK) >>> 8) * 0x00000800;

		// Get number of horizontal and vertical tiles
		int tileCountX = ((bgcnt & BG_SCREENSIZE_X) != 0) ? 64 : 32;
		int tileCountY = ((bgcnt & BG_SCREENSIZE_Y) != 0) ? 64 : 32;

		// Get tile upside down flag
		boolean tileUpsideDown = tileCountX != 32;

		// Get 256 color flag
		boolean color256 = (bgcnt & BG_COLORMODE) != 0;

		// Get actual y coordinate
		int y = mosaicY == 0 ? index : index - (index % mosaicY);

		// Render scanline
		for (int i = 0; i < Renderer.RES_X_SCREEN; i++) {

			// Get actual x coordinate
			int x = mosaicX == 0 ? i : i - (i % mosaicX);

			// Get tile coords
			int tileX = (x + scrollX) >>> 3;
			int tileY = (y + scrollY) >>> 3;

			// Handle tile wrapping
			tileX &= tileCountX - 1;
			tileY &= tileCountY - 1;

			// Get position in tile
			int tilePosX = (x + scrollX) & 0x07;
			int tilePosY = (y + scrollY) & 0x07;

			// Calculate offset
			int offset = 0;

			// Adjust for upside down tile data
			if (tileUpsideDown) {

				// Adjust offset for X
				if (tileX >= 32)
					offset += 0x800;

				// Adjust offset for Y
				if (tileY >= 32)
					offset += 0x1000;

				// Clamp tile coords
				tileX &= 0x1f;
				tileY &= 0x1f;
			}

			// Get tile address
			int tileAddress = (((tileY << 5) + (tileX)) << 1) + tileMapAddress
					+ offset;
			int tileData = memoryVRAM.readHalfWord(tileAddress);

			// Handle tile flips
			if ((tileData & 0x0400) != 0)
				tilePosX = 7 - tilePosX;
			if ((tileData & 0x0800) != 0)
				tilePosY = 7 - tilePosY;

			// Get index of tile to render
			int tileIndex = tileData & 0x03ff;

			// Render 256 color mode tiles
			if (color256) {

				// Get pixel
				pixelIndex = 0xff & memoryVRAM.readByte(tileDataAddress
						+ (tileIndex << 6) + (tilePosY << 3) + (tilePosX));

				// If not transparent render pixel
				if (pixelIndex != 0x00) {

					// Get pixel value
					int pixel = memoryPAL.readHalfWord(pixelIndex << 1);

					// Convert to 24-bit color
					int r = (pixel & MASK_BLU) << 19;
					int g = (pixel & MASK_GRN) << 6;
					int b = (pixel & MASK_RED) >>> 7;

					// Store pixel
					pixels[i + (index * Renderer.RES_X_SCREEN)] = 0xff000000
							| r | g | b;
				}
			}

			// Render 16 color mode tiles
			else {

				// Get pixel index
				int paletteIndex = (tileData & 0xf000) >>> 12;
				pixelIndex = 0xff & memoryVRAM.readByte(tileDataAddress
						+ (tileIndex << 5)
						+ (((tilePosY << 3) + tilePosX) >> 1));

				// Get correct nibble
				if ((tilePosX & 0x01) != 0)
					pixelIndex >>>= 4;
				else
					pixelIndex &= 0x0f;

				// If not transparent render pixel
				if (pixelIndex != 0x00) {

					// Get pixel value
					int pixel = memoryPAL
							.readHalfWord(((paletteIndex << 4) + pixelIndex) << 1);

					// Convert to 24-bit color
					int r = (pixel & MASK_BLU) << 19;
					int g = (pixel & MASK_GRN) << 6;
					int b = (pixel & MASK_RED) >>> 7;

					// Store pixel
					pixels[i + (index * Renderer.RES_X_SCREEN)] = 0xff000000
							| r | g | b;
				}
			}
		}
	}
}
