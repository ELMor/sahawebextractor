package javagba.video;

import javagba.memory.Memory;
import javagba.memory.MemoryRAM;

public class Mode3ScanlineRenderer extends AbstractScanlineRenderer {
	public Mode3ScanlineRenderer(Renderer renderer) {

		// Call inherited constructor
		super(renderer);
	}

	public void renderScanline(int index) {

		// Get mosaic parameters
		int mosaic = renderer.registers.getRegister(REG_MOSAIC);
		int mosaicX = (mosaic & MOSAIC_BG_X);
		int mosaicY = (mosaic & MOSAIC_BG_Y) >>> 4;

		if (mosaicX != 0)
			System.out.println(mosaicX);

		// Adjust scanline if needed
		index = index - (mosaicY == 0 ? 0 : (index % mosaicY));

		// Initialise locals
		int pixel;
		int offset = index * Renderer.RES_X_SCREEN;
		MemoryRAM bank = (MemoryRAM) renderer.memory.banks[Memory.MEM_VRAM];

		// Get pixel data reference
		final int[] pixels = renderer.getPixels();

		for (int x = 0; x < Renderer.RES_X_SCREEN; x++) {

			int xs = x - (mosaicX == 0 ? 0 : (x % mosaicX));

			// Get VRAM data
			pixel = bank.readHalfWord((offset + xs) << 1);

			// Get shifted RGB components
			int r = (pixel & MASK_BLU) << 19;
			int g = (pixel & MASK_GRN) << 6;
			int b = (pixel & MASK_RED) >>> 7;

			// Store pixel
			pixels[offset + x] = 0xff000000 | r | g | b;
		}
	}
}
