package javagba.video;

public class Mode0ScanlineRenderer extends AbstractScanlineRenderer {
	public Mode0ScanlineRenderer(Renderer renderer) {

		// Call inherited constructor
		super(renderer);
	}

	public void renderScanline(int index) {

		// Render background scanline
		renderBackgroundScanline(index);

		// Get BG Enabled flags
		boolean BG0ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG0_F) != 0;
		boolean BG1ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG1_F) != 0;
		boolean BG2ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG2_F) != 0;
		boolean BG3ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG3_F) != 0;
		boolean OBJENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_OBJ_F) != 0;

		// Get BG control register values
		int BG0CNT = renderer.registers.getRegister(REG_BG0CNT);
		int BG1CNT = renderer.registers.getRegister(REG_BG1CNT);
		int BG2CNT = renderer.registers.getRegister(REG_BG2CNT);
		int BG3CNT = renderer.registers.getRegister(REG_BG3CNT);

		// Get BG priorities
		int BG0PRIORITY = BG0CNT & BG_PRIORITY;
		int BG1PRIORITY = BG1CNT & BG_PRIORITY;
		int BG2PRIORITY = BG2CNT & BG_PRIORITY;
		int BG3PRIORITY = BG3CNT & BG_PRIORITY;

		// Render background
		for (int priority = 3; priority >= 0; priority--) {

			if (BG3ENABLED && (BG3PRIORITY == priority))
				renderTextModeLine(index, 3, BG3CNT);
			if (BG2ENABLED && (BG2PRIORITY == priority))
				renderTextModeLine(index, 2, BG2CNT);
			if (BG1ENABLED && (BG1PRIORITY == priority))
				renderTextModeLine(index, 1, BG1CNT);
			if (BG0ENABLED && (BG0PRIORITY == priority))
				renderTextModeLine(index, 0, BG0CNT);
			if (OBJENABLED)
				renderSpriteLine(index, priority, 0x00010000);
		}
	}
}
