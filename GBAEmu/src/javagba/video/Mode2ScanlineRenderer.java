package javagba.video;

public class Mode2ScanlineRenderer extends AbstractScanlineRenderer {
	public Mode2ScanlineRenderer(Renderer renderer) {

		// Call inherited constructor
		super(renderer);
	}

	public void renderScanline(int index) {

		// Render background scanline
		renderBackgroundScanline(index);

		// Get BG Enabled flags
		boolean BG2ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG2_F) != 0;
		boolean BG3ENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_BG3_F) != 0;
		boolean OBJENABLED = (renderer.registers.getRegister(REG_DISPCNT) & DISPCNT_DISP_OBJ_F) != 0;

		// Get BG control register values
		int BG2CNT = renderer.registers.getRegister(REG_BG2CNT);
		int BG3CNT = renderer.registers.getRegister(REG_BG3CNT);

		// Get BG priorities
		int BG2PRIORITY = BG2CNT & BG_PRIORITY;
		int BG3PRIORITY = BG3CNT & BG_PRIORITY;

		// Render background
		for (int priority = 3; priority >= 0; priority--) {

			if (BG3ENABLED && (BG3PRIORITY == priority))
				renderScaleRotateModeLine(index, 3, BG3CNT);
			if (BG2ENABLED && (BG2PRIORITY == priority))
				renderScaleRotateModeLine(index, 2, BG2CNT);
			if (OBJENABLED)
				renderSpriteLine(index, priority, 0x00010000);
		}
	}
}
