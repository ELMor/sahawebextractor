package javagba;

import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javagba.video.Viewport;

public class EmulatorFrame extends Frame implements ActionListener {

	public Emulator owner; // Owner reference

	public Viewport viewport; // Viewport reference

	// Window event handler
	class EmulatorFrameHandler extends WindowAdapter {

		public void windowClosing(WindowEvent e) {
			((Frame) e.getSource()).dispose();
			System.exit(0);
		}
	};

	public EmulatorFrame(Emulator emulator) {

		// Call inherited constructor
		super();

		// Set owner
		owner = emulator;

		// Set frame title
		setTitle(Emulator.APP_NAME + " " + Emulator.APP_VERSION);

		// Initialise frame surface
		setBackground(SystemColor.control);
		setLayout(null);
		setVisible(true);
		setSize(getInsets().left + getInsets().right + 243, getInsets().top
				+ getInsets().bottom + 163);

		// Register event listener
		addWindowListener(new EmulatorFrameHandler());

		// Initialise components
		initMenu();
		initComponent();
	}

	public void actionPerformed(ActionEvent e) {

		// Handle menu events
		if (e.getSource() instanceof Menu) {

			// Cast to menu
			Menu menu = (Menu) e.getSource();

			// Handle menu event
			handleMenuEvent(e.getActionCommand().toLowerCase());
		}
	}

	private void handleMenuEvent(String item) {

		// Handle exit
		if (item.equals("exit")) {
			dispose();
			System.exit(0);
		}

		// Handle load rom
		if (item.equals("load rom...")) {

			// Create and show file dialog
			FileDialog dialog = new FileDialog(this, "Load Rom",
					FileDialog.LOAD);
			dialog.show();

			// Load rom
			owner.coreReset();
			try {
				owner.loadRom(dialog.getDirectory() + dialog.getFile());
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		// Handle load bios
		if (item.equals("load bios...")) {

			// Create and show file dialog
			FileDialog dialog = new FileDialog(this, "Load Bios",
					FileDialog.LOAD);
			dialog.show();

			// Load bios
			owner.loadBios(dialog.getDirectory() + dialog.getFile());
		}

		// Handle run
		if (item.equals("run"))
			owner.coreRun();

		// Handle reset
		if (item.equals("reset"))
			owner.coreReset();

		// Handle pause
		if (item.equals("pause"))
			owner.corePause();

		// Handle run
		if (item.equals("step over"))
			owner.coreStep();
	}

	private void initComponent() {

		// Create viewport
		viewport = new Viewport(owner.getRenderer());
		viewport.setBounds(getInsets().left, getInsets().top, 244, 164);
		add(viewport);
	}

	private void initMenu() {

		// Create menu bar
		MenuBar menuBar = new MenuBar();

		// Create "File" Menu
		Menu fileMenu = new Menu("File");
		fileMenu.add(new MenuItem("Load Rom..."));
		fileMenu.add(new MenuItem("Load Bios..."));
		fileMenu.add(new MenuItem("-"));
		fileMenu.add(new MenuItem("Exit"));
		fileMenu.addActionListener(this);
		menuBar.add(fileMenu);

		// Create "CPU" Menu
		Menu cpuMenu = new Menu("CPU");
		cpuMenu.add(new MenuItem("Run"));
		cpuMenu.add(new MenuItem("Step Over"));
		cpuMenu.add(new MenuItem("Pause"));
		cpuMenu.add(new MenuItem("Reset"));
		cpuMenu.addActionListener(this);
		menuBar.add(cpuMenu);

		// Add menu to window
		setMenuBar(menuBar);
	}
}
