package javagba.memory;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MemoryBIOS implements AbstractMemory {

	public byte[] data; // Memory data

	public MemoryBIOS(String fileName) {

		// Initialise BIOS data
		if (fileName == null)
			data = new byte[1024 * 16];
		else
			data = readFile(fileName, 1024 * 16);
	}

	public byte readByte(int address) {

		// Read byte
		return data[address & 0x3fff];
	}

	public byte[] readFile(String fileName, int maxSize) {

		// Create result array
		byte[] result = new byte[maxSize];

		try {
			// Open input stream
			InputStream is=getClass().getResourceAsStream(fileName);
			DataInputStream in = new DataInputStream(is);
			// Reset counter
			int offset = 0;
			// Read data
			while (in.available() > 0)
				offset += in.read(result, offset, in.available());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Return result array
		return result;
	}

	public short readHalfWord(int address) {

		// Return halfword
		return (short) ((0xff & readByte(address + 1)) << 8 | (0xff & readByte(address)));
	}

	public int readWord(int address) {

		// Return word
		return (0xff & readByte(address + 3)) << 24
				| (0xff & readByte(address + 2)) << 16
				| (0xff & readByte(address + 1)) << 8
				| (0xff & readByte(address));
	}

	public void writeByte(byte value, int address) {

		// Writing to BIOS not allowed
	}

	public void writeHalfWord(short value, int address) {

		// Writing to BIOS not allowed
	}

	public void writeWord(int value, int address) {

		// Writing to BIOS not allowed
	}
}
