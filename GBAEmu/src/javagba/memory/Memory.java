package javagba.memory;

public class Memory implements AbstractMemory {

	public final static int MEM_BIOS = 0x00000000; // System BIOS memory index

	public final static int MEM_CPUEXT = 0x00000002; // External CPU memory
														// index

	public final static int MEM_CPUINT = 0x00000003; // Internal CPU memory
														// index

	public final static int MEM_IOREG = 0x00000004; // IO, Registers index

	public final static int MEM_PALETTE = 0x00000005; // Palette memory index

	public final static int MEM_VRAM = 0x00000006; // Video memory index

	public final static int MEM_OAM = 0x00000007; // Object attribute memory
													// index

	public final static int MEM_PAKROM1 = 0x00000008; // Game pak ROM Wait
														// State 1 index

	public final static int MEM_PAKROM2 = 0x0000000a; // Game pak ROM Wait
														// State 2 index

	public final static int MEM_PAKROM3 = 0x0000000c; // Game pak ROM Wait
														// State 3 index

	public final static int MEM_PAKRAM = 0x0000000e; // Game pak RAM index

	public AbstractMemory[] banks; // Memory bank list

	public DMA dma; // DMA manager reference

	public Memory(String biosName, String romName) {

		// Initialise memory banks
		banks = new AbstractMemory[0x10];
		for(int i=0;i<0x10;i++)
			banks[i]=new NonExistentMemory(i);

		// Assign banks
		banks[MEM_CPUEXT] = new MemoryRAM(1024 * 256);
		banks[MEM_CPUINT] = new MemoryRAM(1024 * 32);
		banks[MEM_IOREG] = new MemoryRegisters(this);
		banks[MEM_PALETTE] = new MemoryRAM(1024 * 1);
		banks[MEM_VRAM] = new MemoryRAM(1024 * 128); // VRAM is 96k but 128k
														// allows me to AND mask
		banks[MEM_OAM] = new MemoryRAM(1024 * 1);
		banks[MEM_PAKRAM] = new MemoryRAM(1024 * 512);
		// Initialise DMA
		dma = new DMA(this);

		// Load rom
		loadBios(biosName);
		try {
			loadRom(romName);
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void loadBios(String fileName) {

		// Create BIOS bank
		banks[MEM_BIOS] = new MemoryBIOS(fileName);
	}

	public void loadRom(String fileName) throws Exception {

		// Create PAK banks
		AbstractMemory pakBank = new MemoryROM(fileName);

		// Assign same banks to PAK
		banks[MEM_PAKROM1] = pakBank;
		banks[MEM_PAKROM2] = pakBank;
		banks[MEM_PAKROM3] = pakBank;
	}

	public byte readByte(int address) {

		// Read byte from memory
		return banks[(address & 0x0f000000) >>> 24]
				.readByte(address & 0x00ffffff);
	}

	public short readHalfWord(int address) {

		// Read halfword from memory
		return banks[(address & 0x0f000000) >>> 24]
				.readHalfWord(address & 0x00fffffe);
	}

	public int readWord(int address) {

		// Read word from memory
		int i1 = (address & 0x0f000000) >>> 24;
		int i2 = address & 0x00fffffc;
		return banks[i1].readWord(i2);
	}

	public void writeByte(byte value, int address) {

		// Write byte to memory
		banks[(address & 0x0f000000) >>> 24].writeByte(value,
				address & 0x00ffffff);
	}

	public void writeHalfWord(short value, int address) {

		// Write halfword to memory
		int i1 = (address & 0x0f000000) >>> 24;
		int i2 = (address & 0x00fffffe);
		banks[i1].writeHalfWord(value, i2);
	}

	public void writeWord(int value, int address) {

		// Write word to memory
		int i1=(address & 0x0f000000) >>> 24;
		int i2=address & 0x00fffffc;
		banks[i1].writeWord(value,i2);
	}
}
