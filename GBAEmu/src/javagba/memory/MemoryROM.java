package javagba.memory;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class MemoryROM implements AbstractMemory {

	public final static int MODE_FILE = 0;

	public final static int MODE_RESOURCE = 1;

	public final static int MODE_URL = 2;

	public static int mode;

	public byte[] data; // Memory data

	public int mask=0xFFFFFFFF; // Memory size

	public MemoryROM(String fileName) throws Exception {
		data=readFile(fileName,0);
	}

	public byte readByte(int address) {

		// Read byte
		return data[address & mask];
	}

	public byte[] readFile(String fileName, int maxSize) throws Exception {
		byte[]result=null;
		if (fileName == null) {
			return new byte[1024*1024];
		} else {
			File f = new File(fileName);
			if (f != null && f.exists()) {
				long size = 0;
				InputStream is = null;
				if (fileName.toLowerCase().endsWith(".zip")) {
					ZipFile zf = new ZipFile(f, ZipFile.OPEN_READ);
					Enumeration e = zf.entries();
					ZipEntry ze = (ZipEntry) e.nextElement();
					size = ze.getSize();
					is = zf.getInputStream(ze);
				} else {
					size = f.length();
					is = new FileInputStream(f);
				}
				result = new byte[(int) size];
				int offset=0;
				while(offset<size)
					offset+=is.read(result, offset, (int)(size-offset));
				is.close();
				System.err.println("cargados "+((float)offset)/1024/1024+" MB");
			}
		}
		return result;
	}

	public short readHalfWord(int address) {

		// Return halfword
		return (short) ((((0xff & readByte(address + 1)) << 8) & 0xff00) | ((0xff & readByte(address)) & 0x00ff));
	}

	public int readWord(int address) {

		// Return word
		return (((0xff & readByte(address + 3)) << 24) & 0xff000000)
				| (((0xff & readByte(address + 2)) << 16) & 0x0ff0000)
				| (((0xff & readByte(address + 1)) << 8) & 0x0000ff00)
				| (0xff & readByte(address)) & 0x000000ff;
	}

	public void writeByte(byte value, int address) {

		// Writing only allowed if in flash area
		if (address >= 0x1fe0000)
			data[address] = value;
	}

	public void writeHalfWord(short value, int address) {

		// Write halfword
		writeByte((byte) value, address);
		writeByte((byte) (value >>> 8), address + 1);
	}

	public void writeWord(int value, int address) {

		// Write word
		writeByte((byte) value, address);
		writeByte((byte) (value >>> 8), address + 1);
		writeByte((byte) (value >>> 16), address + 2);
		writeByte((byte) (value >>> 24), address + 3);
	}
}
