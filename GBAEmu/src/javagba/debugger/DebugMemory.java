package javagba.debugger;

import javagba.memory.Memory;

public class DebugMemory extends Memory {

	MemoryView memView;

	boolean reportBack = true;

	/**
	 * DebugMemory constructor comment.
	 * 
	 * @param biosName
	 *            java.lang.String
	 * @param romName
	 *            java.lang.String
	 */
	public DebugMemory(String biosName, String romName) {
		super(biosName, romName);
	}

	/**
	 * 
	 * @return javagba.debugger.MemoryView
	 */
	public MemoryView getMemView() {
		return memView;
	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean isReportBack() {
		return reportBack;
	}

	public byte readByte(int address) {

		// Call inherited method
		byte v = super.readByte(address);

		// Update viewer
		// if(reportBack && memView != null)
		// memView.updateItems(address & 0xfffffff0);

		return v;
	}

	public short readHalfWord(short value, int address) {

		// Call inherited method
		short v = super.readHalfWord(address);

		// Update viewer
		// if(reportBack && memView != null)
		// memView.updateItems(address & 0xfffffff0);

		return v;

	}

	public int readWord(int address) {

		// Call inherited method
		int v = super.readWord(address);

		// Update viewer
		// if(reportBack && memView != null)
		// memView.updateItems(address & 0xfffffff0);

		return v;
	}

	/**
	 * 
	 * @param newMemView
	 *            javagba.debugger.MemoryView
	 */
	public void setMemView(MemoryView newMemView) {
		memView = newMemView;
	}

	/**
	 * 
	 * @param newReportBack
	 *            boolean
	 */
	public void setReportBack(boolean newReportBack) {
		reportBack = newReportBack;
	}

	public static String toHexString(int value, int length) {

		String s = Integer.toHexString(value);

		while (s.length() < length)
			s = "0" + s;

		s = "0x" + s;

		return s;
	}

	public void writeByte(byte value, int address) {

		// Call inherited method
		super.writeByte(value, address);

		// System.out.println("WRITE BYTE ADDR=" + toHexString(address, 8) + "
		// VALUE=" + toHexString(value, 2));

		// Update viewer
		// if(reportBack && memView != null)
		// memView.updateItems(address & 0xfffffff0);
	}

	public void writeHalfWord(short value, int address) {

		// Call inherited method
		super.writeHalfWord(value, address);

		// System.out.println("WRITE HWRD ADDR=" + toHexString(address, 8) + "
		// VALUE=" + toHexString(value, 4));

		// Update viewer
		// if(reportBack && memView != null)
		// memView.updateItems(address & 0xfffffff0);
	}

	public void writeWord(int value, int address) {

		// Call inherited method
		super.writeWord(value, address);

		// System.out.println("WRITE WORD ADDR=" + toHexString(address, 8) + "
		// VALUE=" + toHexString(value, 8));
		// Update viewer
		// if(reportBack && memView != null)
		// memView.updateItems(address & 0xfffffff0);
	}
}
