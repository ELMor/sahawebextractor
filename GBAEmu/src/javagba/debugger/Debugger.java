package javagba.debugger;

import java.awt.Button;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.SystemColor;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javagba.core.Core;
import javagba.video.Renderer;
import javagba.video.Viewport;

public class Debugger extends Frame implements ActionListener {

	int[] breakpoints;

	// Core
	Core core;

	DebugMemory memory;

	Renderer renderer;

	Viewport viewport;

	// Execution thread
	Thread thread;

	// Components
	DisassemblerView disasmView;

	CoreView coreView;

	MemoryView memoryView;

	Button runPause;

	Button stepOver;

	Button executeSteps10;

	Button executeSteps50;

	Button executeSteps500;

	Button browseTo;

	TextField memAddress;

	TextField codeAddress;

	Button gotoMem;

	// Window event handler
	class CoreDebuggerHandler extends WindowAdapter {

		public void windowClosing(WindowEvent e) {
			((Frame) e.getSource()).dispose();
			System.exit(0);
		}
	};

	public Debugger(String args[]) {

		// Call inherited constructor
		super();

		breakpoints = new int[256];

		// Set debug window title
		setTitle("ARM7TDMI Core Debugger");

		// Set window size
		setBounds(100, 50, 854, 700);

		// Make frame visible
		setVisible(true);

		// Register event listener
		addWindowListener(new CoreDebuggerHandler());

		// Initialise surface
		setBackground(SystemColor.control);
		setLayout(null);

		// Initialise core
		initCore(args);

		// Initialise components
		initMenu();
		initComponents();
	}

	public void actionPerformed(ActionEvent e) {

		// Handle menu events
		if (e.getSource() instanceof Menu) {

			// Cast to menu
			Menu menu = (Menu) e.getSource();

			// Handle load rom
			if (e.getActionCommand().equals("Load Rom...")) {

				FileDialog dialog = new FileDialog(this, "Load Rom",
						FileDialog.LOAD);
				dialog.show();
				try {
					memory.loadRom(dialog.getDirectory() + dialog.getFile());
				}catch(Exception ezx){
					ezx.printStackTrace();
				}
				core.reset();
				disasmView.updateItems();
				coreView.repaint();
			}
		}

		// Handle step over
		if (e.getSource() == stepOver) {

			// Execute next instruction
			core.executeNextInstruction();

			// Update disasm viewer
			disasmView.updateItems();
			coreView.repaint();
		}

		// Handle step over
		if (e.getSource() == browseTo) {

			int address = 0x08000000;

			// Get address
			try {
				address = Integer.parseInt(codeAddress.getText(), 16);
			} catch (Exception fe) {

			}

			// Execute next instruction
			disasmView.updateItems(address);
		}

		// Handle step over
		if (e.getSource() == gotoMem) {

			int address = 0x08000000;

			// Get address
			try {
				address = Integer.parseInt(memAddress.getText(), 16);
			} catch (Exception fe) {

			}

			// Execute next instruction
			memoryView.updateItems(address);
		}

		// Handle step over
		if (e.getSource() == executeSteps10) {

			int cycleCount = core.cycleCount;

			// Execute next instruction
			for (int i = 0; i < 10; i++)
				core.executeNextInstruction();

			renderer.addCycles(core.cycleCount - cycleCount);

			// Update disasm viewer
			disasmView.updateItems();
			coreView.repaint();
		}

		// Handle step over
		if (e.getSource() == executeSteps50) {

			int cycleCount = core.cycleCount;

			// Execute next instruction
			for (int i = 0; i < 50; i++)
				core.executeNextInstruction();

			renderer.addCycles(core.cycleCount - cycleCount);

			// Update disasm viewer
			disasmView.updateItems();
			coreView.repaint();
		}

		// Handle step over
		if (e.getSource() == executeSteps500) {

			// Execute next instruction
			for (int i = 0; i < 5; i++) {

				int cycleCount = core.cycleCount;

				for (int j = 0; j < 100; j++)
					core.executeNextInstruction();

				renderer.addCycles(core.cycleCount - cycleCount);

			}

			// Update disasm viewer
			disasmView.updateItems();
			coreView.repaint();
		}

		// Handle run/pause
		if (e.getSource() == runPause) {

			// Start
			if (thread == null) {

				// Start continuous execution
				thread = new Thread("Debugger Execution Thread") {

					// Run method
					public void run() {

						// Execution loop
						while (this.isAlive()) {

							// Give other threads time
							Thread.yield();

							int cycleCount = core.cycleCount;

							// Execute next instruction
							for (int i = 0; i < 100; i++) {

								core.executeNextInstruction();

								// Check for breakpoints
								if (disasmView.owner.isBreakpoint(core
										.getRegister(Core.REG_PC))) {

									// Change label
									runPause.setLabel("Run");

									coreView.repaint();
									disasmView.updateItems();
									repaint();

									// Stop thread
									thread.stop();
									thread = null;
								}
							}

							renderer.addCycles(core.cycleCount - cycleCount);

							// Update views
							coreView.repaint();
							// disasmView.updateItems();

						}
					}
				};

				// Start thread
				thread.start();

				// Change label
				runPause.setLabel("Pause");
			}

			// Terminate
			else {

				// Stop thread
				thread.stop();
				thread = null;

				// Change label
				runPause.setLabel("Run");
			}
		}
	}

	public void addBreakpoint(int address) {

		int index = 0;

		while (breakpoints[index] != 0)
			index++;

		breakpoints[index] = address;
	}

	private void initComponents() {

		// Initialise renderer/viewer
		renderer = new Renderer(memory);
		viewport = new Viewport(renderer);
		renderer.setViewport(viewport);
		viewport.setBounds(getInsets().left + 556, getInsets().top + 456, 244,
				164);
		add(viewport);

		// Initialise diasm viewer
		disasmView = new DisassemblerView(this, core, memory, 600, 400);
		disasmView.setBounds(getInsets().left, getInsets().top, 600, 400);
		add(disasmView);

		// Initialise core viewer
		coreView = new CoreView(core, 200, 440);
		coreView.setBounds(getInsets().left + 600, getInsets().top, 200, 440);
		add(coreView);

		// Initialise core viewer
		memoryView = new MemoryView(this, memory, 540, 200);
		memoryView.setBounds(getInsets().left, getInsets().top + 420, 540, 200);
		add(memoryView);
		memory.setMemView(memoryView);

		// Initialise run buttons
		runPause = new Button("Run");
		runPause.setBounds(getInsets().left, getInsets().top + 400, 80, 20);
		runPause.addActionListener(this);
		add(runPause);

		stepOver = new Button("Step Over");
		stepOver
				.setBounds(getInsets().left + 80, getInsets().top + 400, 80, 20);
		stepOver.addActionListener(this);
		add(stepOver);

		executeSteps10 = new Button("Exec 10 Steps");
		executeSteps10.setBounds(getInsets().left + 160, getInsets().top + 400,
				80, 20);
		executeSteps10.addActionListener(this);
		add(executeSteps10);

		executeSteps50 = new Button("Exec 50 Steps");
		executeSteps50.setBounds(getInsets().left + 240, getInsets().top + 400,
				80, 20);
		executeSteps50.addActionListener(this);
		add(executeSteps50);

		executeSteps500 = new Button("Exec 500 Steps");
		executeSteps500.setBounds(getInsets().left + 320,
				getInsets().top + 400, 80, 20);
		executeSteps500.addActionListener(this);
		add(executeSteps500);

		codeAddress = new TextField("08000000");
		codeAddress.setBounds(getInsets().left + 400, getInsets().top + 400,
				80, 20);
		add(codeAddress);

		browseTo = new Button("Browse To");
		browseTo.setBounds(getInsets().left + 480, getInsets().top + 400, 80,
				20);
		browseTo.addActionListener(this);
		add(browseTo);

		memAddress = new TextField("08000000");
		memAddress.setBounds(getInsets().left, getInsets().top + 620, 100, 20);
		add(memAddress);

		gotoMem = new Button("Browse To");
		gotoMem.setBounds(getInsets().left + 100, getInsets().top + 620, 100,
				20);
		gotoMem.addActionListener(this);
		add(gotoMem);
	}

	private void initCore(String[] args) {

		// Initialise core
		memory = new DebugMemory("/gba.bios", null);
		core = new Core(memory);
	}

	private void initMenu() {

		// Create menu bar
		MenuBar menu = new MenuBar();

		// Create menus
		Menu fileMenu = new Menu("File");
		Menu runMenu = new Menu("Run");

		// Create file menu
		fileMenu.add(new MenuItem("Load Rom..."));
		fileMenu.add(new MenuItem("-"));
		fileMenu.add(new MenuItem("Exit"));

		// Create run menu
		runMenu.add(new MenuItem("Run"));
		runMenu.add(new MenuItem("Trace Into"));
		runMenu.add(new MenuItem("Step Over"));

		// Register event listeners
		fileMenu.addActionListener(this);
		runMenu.addActionListener(this);

		// Add menus to bar
		menu.add(fileMenu);
		menu.add(runMenu);

		// Set menu bar
		setMenuBar(menu);
	}

	public boolean isBreakpoint(int address) {

		for (int i = 0; i < breakpoints.length; i++)
			if (breakpoints[i] == address)
				return true;

		return false;
	}

	public static void main(String[] args) {

		// Create debugger instance
		new Debugger(args);
	}

	public void removeBreakpoint(int address) {

		int index = 0;

		while (breakpoints[index] != address)
			index++;

		breakpoints[index] = 0;
	}
}
