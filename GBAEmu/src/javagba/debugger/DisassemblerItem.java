package javagba.debugger;

public class DisassemblerItem {

	public boolean breakpoint;

	public boolean cursor;

	public String address;

	public String instruction;

	public String opcode;

	public String parameters;

	public DisassemblerItem(boolean thumb, int address, int instruction) {

		// Set item values
		this.address = Disassembler.toHexString(address, 8);
		this.instruction = Disassembler.toHexString(instruction, 8);

		// Get opcode and parameters
		String[] decodedInstruction = thumb ? Disassembler.decodeThumb(address,
				(short) instruction) : Disassembler.decodeArm(address,
				instruction);

		// Save opcode
		this.opcode = decodedInstruction[0];
		this.parameters = decodedInstruction[1];
	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean isBreakpoint() {
		return breakpoint;
	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean isCursor() {
		return cursor;
	}

	/**
	 * 
	 * @param newBreakpoint
	 *            boolean
	 */
	public void setBreakpoint(boolean newBreakpoint) {
		breakpoint = newBreakpoint;
	}

	/**
	 * 
	 * @param newCursor
	 *            boolean
	 */
	public void setCursor(boolean newCursor) {
		cursor = newCursor;
	}
}
