package javagba.debugger;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.SystemColor;

import javagba.core.Core;

public class CoreView extends Panel {

	public Image offImage; // Offscreen image

	public Graphics offGraphics; // Offscreen graphics

	public Core core; // Core reference

	protected CoreView(Core core, int width, int height) {

		// Call inherited constructor
		super();

		// Set layout
		setSize(width, height);
		setFont(new Font("Courier", Font.PLAIN, 12));

		// Initialise fields
		this.core = core;
	}

	public void paint(Graphics g) {

		// Get size
		Dimension size = getSize();

		// Fill background
		g.setColor(SystemColor.window);
		g.fillRect(0, 0, size.width, size.height);

		// Render edge
		g.setColor(SystemColor.controlShadow);
		g.drawRect(0, 0, size.width - 1, size.height - 1);
		g.setColor(SystemColor.controlDkShadow);
		g.drawRect(1, 1, size.width - 3, size.height - 3);
		g.setColor(SystemColor.controlHighlight);
		g.drawLine(size.width - 1, 0, size.width - 1, size.height);
		g.drawLine(0, size.height - 1, size.width - 1, size.height - 1);
		g.setColor(SystemColor.controlLtHighlight);
		g.drawLine(size.width - 2, 1, size.width - 2, size.height - 2);
		g.drawLine(1, size.height - 2, size.width - 3, size.height - 2);

		// Set clip
		g.setClip(2, 2, size.width - 4, size.height - 4);

		// Render margins
		g.setColor(SystemColor.control);
		g.fillRect(2, 2, size.width - 2, 20);
		g.setColor(SystemColor.controlShadow);
		g.drawLine(2, 21, size.width - 2, 21);

		// Render labels
		g.setColor(SystemColor.controlText);
		g.drawString("REG", 4, 16);
		g.drawString("VALUE", 44, 16);

		// Render registers
		for (int i = 0; i < 18; i++) {

			g.setColor(SystemColor.controlText);

			if (i <= 12)
				g.drawString("R" + Integer.toString(i), 4, i * 16 + 36);
			else if (i == 13)
				g.drawString("SP", 4, i * 16 + 36);
			else if (i == 14)
				g.drawString("LR", 4, i * 16 + 36);
			else if (i == 15)
				g.drawString("PC", 4, i * 16 + 36);
			else if (i == 16)
				g.drawString("CPSR", 4, i * 16 + 36);
			else if (i == 17)
				g.drawString("SPSR", 4, i * 16 + 36);

			g.setColor(Color.blue);
			g.drawString(toHexString(core.getRegister(i), 8), 44, i * 16 + 36);

		}

		// Render flag label
		g.setColor(SystemColor.controlText);
		g
				.drawString("NEGATIVE  "
						+ (core.getRegisterBit(Core.REG_CPSR,
								Core.PSR_FLAG_NEGATIVE) ? "ON" : "OFF"), 4, 330);
		g.drawString(
				"ZERO      "
						+ (core.getRegisterBit(Core.REG_CPSR,
								Core.PSR_FLAG_ZERO) ? "ON" : "OFF"), 4, 344);
		g.drawString(
				"CARRY     "
						+ (core.getRegisterBit(Core.REG_CPSR,
								Core.PSR_FLAG_CARRY) ? "ON" : "OFF"), 4, 358);
		g
				.drawString("OVERFLOW  "
						+ (core.getRegisterBit(Core.REG_CPSR,
								Core.PSR_FLAG_OVERFLOW) ? "ON" : "OFF"), 4, 372);
		g.drawString(
				"IRQ DIS   "
						+ (core.getRegisterBit(Core.REG_CPSR,
								Core.PSR_FLAG_IRQDISABLE) ? "ON" : "OFF"), 4,
				386);
		g.drawString(
				"FIQ DIS   "
						+ (core.getRegisterBit(Core.REG_CPSR,
								Core.PSR_FLAG_FIQDISABLE) ? "ON" : "OFF"), 4,
				400);
		g
				.drawString("STATE     "
						+ (core.getRegisterBit(Core.REG_CPSR,
								Core.PSR_FLAG_STATE) ? "THUMB" : "ARM"), 4, 414);

		String mode = "";
		switch (core.getRegister(Core.REG_CPSR) & Core.PSR_MODE) {

		case Core.PSR_MODE_ABORT:
			mode = "ABORT";
			break;
		case Core.PSR_MODE_FIQ:
			mode = "FIQ";
			break;
		case Core.PSR_MODE_IRQ:
			mode = "IRQ";
			break;
		case Core.PSR_MODE_SUPERVISOR:
			mode = "SUPERVISOR";
			break;
		case Core.PSR_MODE_SYSTEM:
			mode = "SYSTEM";
			break;
		case Core.PSR_MODE_UNDEFINED:
			mode = "UNDEFINED";
			break;
		case Core.PSR_MODE_USER:
			mode = "USER";
			break;
		}

		g.drawString("MODE     	" + mode, 4, 428);
	}

	public static String toHexString(int value, int length) {

		String s = Integer.toHexString(value);

		while (s.length() < length)
			s = "0" + s;

		s = "0x" + s;

		return s;
	}

	public void update(Graphics g) {

		// Create offscreen
		if (offGraphics == null) {

			offImage = createImage(getSize().width, getSize().height);
			offGraphics = offImage.getGraphics();
		}

		// Get size
		paint(offGraphics);

		// Blit
		g.drawImage(offImage, 0, 0, this);
	}
}
