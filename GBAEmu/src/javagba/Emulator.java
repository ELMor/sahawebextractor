package javagba;

import javagba.core.Core;
import javagba.memory.Memory;
import javagba.memory.MemoryROM;
import javagba.video.Renderer;

public class Emulator {

	public final static String APP_NAME = "Java GBA Emu"; // Application name

	public final static String APP_VERSION = "0.01a"; // Application version

	private EmulatorFrame frame; // Main window reference

	private Core core; // ARM7TDMI Core reference

	private Memory memory; // Memory Manager reference

	private Renderer renderer; // Renderer reference

	private Thread thread; // Thread reference

	public Emulator(String[] args) {

		// Initialise core and memory
		memory = new Memory("/gba.bios", null);
		core = new Core(memory);
		renderer = new Renderer(memory);

		// Set file load mode
		MemoryROM.mode = MemoryROM.MODE_FILE;

		// Create frame
		frame = new EmulatorFrame(this);

		// Assign viewport
		renderer.setViewport(frame.viewport);

		// Load default bios file
		loadBios("/gba.bios");
	}

	public void corePause() {

		// Stop
		if (thread != null) {

			// Stop continuous execution
			thread = null;
		}
	}

	public void coreReset() {

		// Stop execution
		corePause();

		// Reset core
		core.reset();
	}

	public void coreRun() {

		// Start
		if (thread == null) {

			// Start continuous execution
			thread = new Thread("Debugger Execution Thread") {

				// Run method
				public void run() {

					// Execution loop
					while (this.isAlive()) {

						// Give other threads time
						// Thread.yield();

						// Execute next instruction
						coreStep();// Execute(renderer.CYCLES_HBLANK);

						// Execute next instruction
						// coreExecute(renderer.CYCLES_SCANLINE -
						// renderer.CYCLES_HBLANK);
					}
				}
			};

			// Start thread
			thread.start();
		}
	}

	public void coreStep() {

		// Get current cycle count
		int cycleCount = core.cycleCount;

		// Execute next instruction
		for (int i = 0; i < 100; i++)
			core.executeNextInstruction();

		// Pass passed cycles to renderer
		renderer.addCycles(core.cycleCount - cycleCount);
	}

	public void coreStepScanline() {

		// Get current cycle count
		int cycleCount = core.cycleCount;

		// Execute until HBLANK start
		while (core.cycleCount - cycleCount <= Renderer.CYCLES_SCANLINE) {

			// Execute next instruction
			core.executeNextInstruction();

			// If HBLANK starts update render cycles
			if (core.cycleCount - cycleCount <= Renderer.CYCLES_HBLANK)
				renderer.addCycles(core.cycleCount - cycleCount);
		}

		// Execyte HBLANK cycles
		renderer.addCycles(core.cycleCount - (cycleCount + Renderer.CYCLES_HBLANK));
	}

	public Core getCore() {

		// Return core reference
		return core;
	}

	public Memory getMemory() {

		// Return memory manager reference
		return memory;
	}

	public Renderer getRenderer() {

		// Return renderer reference
		return renderer;
	}

	public void loadBios(String fileName) {

		// Load bios rom
		memory.loadBios(fileName);
	}

	public void loadRom(String fileName) throws Exception {

		// Load rom
		memory.loadRom(fileName);
	}

	public static void main(String[] args) {

		// Construct new instance
		new Emulator(args);
	}
}
