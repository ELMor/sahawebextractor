%% @author linarese
%% @doc @todo Add description to calc.


-module(calc).

%% ====================================================================
%% API functions
%% ====================================================================
-export([rpn/1,test/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
read(N) ->
    case string:to_float(N) of
        {error,no_float} -> list_to_integer(N);
        {F,_} -> F
    end.

rpn(L) when is_list(L) ->
	Res = lists:foldl(fun rpn/2,[], string:tokens(L," ")),
	Res.
rpn("+",     [N1,N2|S]) -> [N2+N1|S];
rpn("-",     [N1,N2|S]) -> [N2-N1|S];
rpn("*",     [N1,N2|S]) -> [N2*N1|S];
rpn("/",     [N1,N2|S]) -> [N2/N1|S];
rpn("^",     [N1,N2|S]) -> [math:pow(N2,N1)|S];
rpn("ln",    [N|S])     -> [math:log(N)|S];
rpn("log10", [N|S])     -> [math:log10(N)|S];
rpn("swap",  [N1,N2|S]) -> [N2,N1|S];
rpn("dup",   [N|S])     -> [N,N|S];
rpn("drop",  [_|S])     -> [S];
rpn(X, Stack) -> [read(X)|Stack].

test() ->
	rpn("2 3 + 6 - 10 +").

