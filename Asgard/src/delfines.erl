%% @author linarese
%% @doc @todo Add description to 'Delfines'.


-module('delfines').

%% ====================================================================
%% API functions
%% ====================================================================
-compile(export_all).

dolphin1() ->
    receive
        do_a_flip ->
            io:format("How about no?~n");
        fish ->
            io:format("So long and thanks for all the fish!~n");
        _ ->
            io:format("Heh, we're smarter than you humans.~n")
 end.

dolphin2() ->
    receive
        {From, do_a_flip} ->
            From ! "How about no?";
        {From, fish} ->
            From ! "So long and thanks for all the fish!";
        _ ->
            io:format("Heh, we're smarter than you humans.~n")
end.
%% ====================================================================
%% Internal functions
%% ====================================================================


