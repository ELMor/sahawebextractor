<?xml version='1.0' encoding='UTF-8'?>

<!ENTITY % AnyValue    "Text|Code|DateTime|Date|Time|UIDref|Pname|Composite|Image|Waveform|Container">
<!ENTITY % NoContainer "Text|Code|DateTime|Date|Time|UIDref|Pname|Composite|Image|Waveform">
<!ENTITY % ObsAcqContext  "Text|Code|DateTime|Date|Time|UIDref|Pname">

<!ELEMENT srDocument (concept, Container)>

<!ELEMENT concept (code, HasConceptMod*)>
<!ELEMENT code EMPTY>
<!ATTLIST code
    value   CDATA  #REQUIRED
    scheme  CDATA  #REQUIRED
    meaning CDATA  #IMPLIED
>

<!ELEMENT Contains      (concept?, (%AnyValue;))>
<!ELEMENT HasObsContext (concept,  (%ObsAcqContext;))>
<!ELEMENT HasAcqContext (concept?, (%ObsAcqContext;))>
<!ELEMENT HasProperties (concept?, (%NoContainer;))>
<!ELEMENT InferredFrom  (concept?, (%NoContainer;))>
<!ELEMENT HasConceptMod (concept,  (Text|Code))>

<!ATTLIST Contains      id   CDATA  #REQUIRED>
<!ATTLIST HasObsContext id   CDATA  #REQUIRED>
<!ATTLIST HasAcqContext id   CDATA  #REQUIRED>
<!ATTLIST HasProperties id   CDATA  #REQUIRED>
<!ATTLIST InferredFrom  id   CDATA  #REQUIRED>
<!ATTLIST SelectedFrom  id   CDATA  #REQUIRED>
<!ATTLIST HasConceptMod id   CDATA  #REQUIRED>



<!ELEMENT Container (Contains|HasObsContext|HasAcqContext)+>
<!ATTLIST Container
    Continuity   (SEPARATE|CONTINUOUS)   #REQUIRED
>


<!ELEMENT Text (body, (HasProperties|InferredFrom)*)>
<!ELEMENT body (#PCDATA)>

<!ELEMENT Num  (unit)>
<!ATTLIST Num
    value   CDATA   #REQUIRED
>
<!ELEMENT unit (code)>

<!ELEMENT Code (code)>

<!ELEMENT DateTime EMPTY>
<!ATTLIST DateTime
    year   CDATA   #REQUIRED
    month  CDATA   #REQUIRED
    day	   CDATA   #REQUIRED
    hour   CDATA   #REQUIRED
    min    CDATA   #REQUIRED
    sec	   CDATA   #IMPLIED
    frac   CDATA   #IMPLIED
>

<!ELEMENT Date EMPTY>
<!ATTLIST Date
    year   CDATA   #REQUIRED
    month  CDATA   #REQUIRED
    day	   CDATA   #REQUIRED
>

<!ELEMENT Time EMPTY>
<!ATTLIST Time
    hour   CDATA   #REQUIRED
    min    CDATA   #REQUIRED
    sec	   CDATA   #IMPLIED
    frac   CDATA   #IMPLIED
>

<!ELEMENT UIDref EMPTY>
<!ATTLIST UIDref
	value   CDATA   #REQUIRED
>

<!ELEMENT Pname EMPTY>
<!ATTLIST Pname
	last      CDATA   #REQUIRED
	first     CDATA   #IMPLIED
	middle    CDATA   #IMPLIED
	prefix    CDATA   #IMPLIED
	suffix    CDATA   #IMPLIED
>

<!ELEMENT Composite (refComposite+, HasAcqContext*)>
<!ELEMENT refComposite EMPTY>
<!ATTLIST refComposite
	classUID    CDATA   #REQUIRED
	instanceUID CDATA   #REQUIRED
>

<!ELEMENT Image (refImage+, HasAcqContext*)>
<!ELEMENT refImage (frameNo*, refPresentation?)>
<!ATTLIST refImage
	classUID    CDATA   #REQUIRED
	instanceUID CDATA   #REQUIRED
>
<!ELEMENT frameNo EMPTY>
<!ATTLIST frameNo
	value   CDATA   #REQUIRED
>
<!ELEMENT refPresentation EMPTY>
<!ATTLIST refPresentation
	classUID    CDATA   #REQUIRED
	instanceUID CDATA   #REQUIRED
>

<!ELEMENT Waveform (refWaveform+, HasAcqContext*)>
<!ELEMENT refWaveform (channel*)>
<!ATTLIST refWaveform
	classUID    CDATA   #REQUIRED
	instanceUID CDATA   #REQUIRED
>
<!ELEMENT channel EMPTY>
<!ATTLIST channel
	M   CDATA   #REQUIRED
	C   CDATA   #REQUIRED
>
