/* jdtVersion - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package version;

public class jdtVersion
{
    public static int versionMajor = 1;
    public static int versionMinor = 7;
    public static int versionPatch = 35;
    public static String type = "beta";
    
    public static String getVersion() {
	return ("jdt v" + versionMajor + "." + versionMinor + "."
		+ versionPatch);
    }
}
