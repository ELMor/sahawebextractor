/* ClassServer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package examples.classServer;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class ClassServer implements Runnable
{
    private ServerSocket server = null;
    private int port;
    
    protected ClassServer(int i) throws IOException {
	port = i;
	server = new ServerSocket(i);
	newListener();
    }
    
    public abstract byte[] getBytes(String string)
	throws IOException, ClassNotFoundException;
    
    public void run() {
	Socket socket;
	try {
	    socket = server.accept();
	} catch (IOException ioexception) {
	    System.out
		.println("Class Server died: " + ioexception.getMessage());
	    ioexception.printStackTrace();
	    return;
	}
	newListener();
	try {
	    DataOutputStream dataoutputstream
		= new DataOutputStream(socket.getOutputStream());
	    try {
		DataInputStream datainputstream
		    = new DataInputStream(socket.getInputStream());
		String string = getPath(datainputstream);
		byte[] is = getBytes(string);
		try {
		    dataoutputstream.writeBytes("HTTP/1.0 200 OK\r\n");
		    dataoutputstream
			.writeBytes("Content-Length: " + is.length + "\r\n");
		    dataoutputstream
			.writeBytes("Content-Type: application/java\r\n\r\n");
		    dataoutputstream.write(is);
		    dataoutputstream.flush();
		} catch (IOException ioexception) {
		    /* empty */
		}
	    } catch (Exception exception) {
		dataoutputstream.writeBytes("HTTP/1.0 400 "
					    + exception.getMessage() + "\r\n");
		dataoutputstream.writeBytes("Content-Type: text/html\r\n\r\n");
		dataoutputstream.flush();
	    }
	} catch (IOException ioexception) {
	    System.out.println("error writing response: "
			       + ioexception.getMessage());
	    ioexception.printStackTrace();
	} finally {
	    try {
		socket.close();
	    } catch (IOException ioexception) {
		/* empty */
	    }
	}
    }
    
    private void newListener() {
	new Thread(this).start();
    }
    
    private static String getPath(DataInputStream datainputstream)
	throws IOException {
	String string = datainputstream.readLine();
	String string_0_ = "";
	if (string.startsWith("GET /")) {
	    string = string.substring(5, string.length() - 1).trim();
	    int i = string.indexOf(".class ");
	    if (i != -1)
		string_0_ = string.substring(0, i).replace('/', '.');
	}
	do
	    string = datainputstream.readLine();
	while (string.length() != 0 && string.charAt(0) != '\r'
	       && string.charAt(0) != '\n');
	if (string_0_.length() != 0)
	    return string_0_;
	throw new IOException("Malformed Header");
    }
}
