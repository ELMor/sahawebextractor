/* ClassFileServer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package examples.classServer;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ClassFileServer extends ClassServer
{
    private String classpath;
    private static int DefaultServerPort = 2001;
    
    public ClassFileServer(int i, String string) throws IOException {
	super(i);
	classpath = string;
    }
    
    public byte[] getBytes(String string)
	throws IOException, ClassNotFoundException {
	System.out.println("reading: " + string);
	File file
	    = new File(classpath + File.separator
		       + string.replace('.', File.separatorChar) + ".class");
	int i = (int) file.length();
	if (i == 0)
	    throw new IOException("File length is zero: " + string);
	FileInputStream fileinputstream = new FileInputStream(file);
	DataInputStream datainputstream = new DataInputStream(fileinputstream);
	byte[] is = new byte[i];
	datainputstream.readFully(is);
	return is;
    }
    
    public static void main(String[] strings) {
	int i = DefaultServerPort;
	String string = "";
	if (strings.length >= 1)
	    i = Integer.parseInt(strings[0]);
	if (strings.length >= 2)
	    string = strings[1];
	try {
	    new ClassFileServer(i, string);
	} catch (IOException ioexception) {
	    System.out.println("Unable to start ClassServer: "
			       + ioexception.getMessage());
	    ioexception.printStackTrace();
	}
    }
}
