// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package com.tiani.rmicfg;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.rmi.MarshalException;
import java.rmi.Remote;
import java.rmi.UnmarshalException;
import java.rmi.server.Operation;
import java.rmi.server.RemoteCall;
import java.rmi.server.Skeleton;
import java.rmi.server.SkeletonMismatchException;
import java.util.Properties;

// Referenced classes of package com.tiani.rmicfg:
//            ServerImpl

public final class ServerImpl_Skel
    implements Skeleton
{

    private static final Operation operations[] = {
        new Operation("java.util.Properties getProperties()"), new Operation("java.lang.String getPropertyNames()[]"), new Operation("java.lang.String getType()"), new Operation("boolean isRunning()"), new Operation("void setProperties(java.util.Properties)"), new Operation("void start()"), new Operation("void start(java.util.Properties)"), new Operation("void stop(boolean, boolean)")
    };
    private static final long interfaceHash = 0x84c909b22154ab00L;

    public ServerImpl_Skel()
    {
    }

    public void dispatch(Remote remote, RemoteCall remotecall, int i, long l)
        throws Exception
    {
        if(i < 0)
        {
            if(l == 0x5c98f51d7e4942b1L)
                i = 0;
            else
            if(l == 0xa1e89d47ee7414fcL)
                i = 1;
            else
            if(l == 0x57fb612cb4898661L)
                i = 2;
            else
            if(l == 0x9ee61a22afa5aff2L)
                i = 3;
            else
            if(l == 0x5814d52e33c0c5e7L)
                i = 4;
            else
            if(l == 0x90a04074b61c5129L)
                i = 5;
            else
            if(l == 0xb4d285fba1560d91L)
                i = 6;
            else
            if(l == 0x5a8f6945b23c220fL)
                i = 7;
            else
                throw new UnmarshalException("invalid method hash");
        } else
        if(l != 0x84c909b22154ab00L)
            throw new SkeletonMismatchException("interface hash mismatch");
        ServerImpl serverimpl = (ServerImpl)remote;
        switch(i)
        {
        case 0: // '\0'
            remotecall.releaseInputStream();
            Properties properties = serverimpl.getProperties();
            try
            {
                ObjectOutput objectoutput = remotecall.getResultStream(true);
                objectoutput.writeObject(properties);
            }
            catch(IOException ioexception1)
            {
                throw new MarshalException("error marshalling return", ioexception1);
            }
            break;

        case 1: // '\001'
            remotecall.releaseInputStream();
            String as[] = serverimpl.getPropertyNames();
            try
            {
                ObjectOutput objectoutput1 = remotecall.getResultStream(true);
                objectoutput1.writeObject(as);
            }
            catch(IOException ioexception2)
            {
                throw new MarshalException("error marshalling return", ioexception2);
            }
            break;

        case 2: // '\002'
            remotecall.releaseInputStream();
            String s = serverimpl.getType();
            try
            {
                ObjectOutput objectoutput2 = remotecall.getResultStream(true);
                objectoutput2.writeObject(s);
            }
            catch(IOException ioexception3)
            {
                throw new MarshalException("error marshalling return", ioexception3);
            }
            break;

        case 3: // '\003'
            remotecall.releaseInputStream();
            boolean flag = serverimpl.isRunning();
            try
            {
                ObjectOutput objectoutput3 = remotecall.getResultStream(true);
                objectoutput3.writeBoolean(flag);
            }
            catch(IOException ioexception4)
            {
                throw new MarshalException("error marshalling return", ioexception4);
            }
            break;

        case 4: // '\004'
            Properties properties1;
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                properties1 = (Properties)objectinput.readObject();
            }
            catch(IOException ioexception8)
            {
                throw new UnmarshalException("error unmarshalling arguments", ioexception8);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling arguments", classnotfoundexception);
            }
            finally
            {
                remotecall.releaseInputStream();
            }
            serverimpl.setProperties(properties1);
            try
            {
                remotecall.getResultStream(true);
            }
            catch(IOException ioexception5)
            {
                throw new MarshalException("error marshalling return", ioexception5);
            }
            break;

        case 5: // '\005'
            remotecall.releaseInputStream();
            serverimpl.start();
            try
            {
                remotecall.getResultStream(true);
            }
            catch(IOException ioexception)
            {
                throw new MarshalException("error marshalling return", ioexception);
            }
            break;

        case 6: // '\006'
            Properties properties2;
            try
            {
                ObjectInput objectinput1 = remotecall.getInputStream();
                properties2 = (Properties)objectinput1.readObject();
            }
            catch(IOException ioexception9)
            {
                throw new UnmarshalException("error unmarshalling arguments", ioexception9);
            }
            catch(ClassNotFoundException classnotfoundexception1)
            {
                throw new UnmarshalException("error unmarshalling arguments", classnotfoundexception1);
            }
            finally
            {
                remotecall.releaseInputStream();
            }
            serverimpl.start(properties2);
            try
            {
                remotecall.getResultStream(true);
            }
            catch(IOException ioexception6)
            {
                throw new MarshalException("error marshalling return", ioexception6);
            }
            break;

        case 7: // '\007'
            boolean flag1;
            boolean flag2;
            try
            {
                ObjectInput objectinput2 = remotecall.getInputStream();
                flag1 = objectinput2.readBoolean();
                flag2 = objectinput2.readBoolean();
            }
            catch(IOException ioexception10)
            {
                throw new UnmarshalException("error unmarshalling arguments", ioexception10);
            }
            finally
            {
                remotecall.releaseInputStream();
            }
            serverimpl.stop(flag1, flag2);
            try
            {
                remotecall.getResultStream(true);
            }
            catch(IOException ioexception7)
            {
                throw new MarshalException("error marshalling return", ioexception7);
            }
            break;

        default:
            throw new UnmarshalException("invalid method number");
        }
    }

    public Operation[] getOperations()
    {
        return (Operation[])operations.clone();
    }

}
