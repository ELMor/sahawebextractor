// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package com.tiani.rmicfg;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.lang.reflect.Method;
import java.rmi.MarshalException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.UnexpectedException;
import java.rmi.UnmarshalException;
import java.rmi.server.Operation;
import java.rmi.server.RemoteCall;
import java.rmi.server.RemoteObject;
import java.rmi.server.RemoteRef;
import java.rmi.server.RemoteStub;

// Referenced classes of package com.tiani.rmicfg:
//            IServer, IServerFactory

public final class ServerFactoryImpl_Stub extends RemoteStub
    implements IServerFactory, Remote
{

    private static final Operation operations[] = {
        new Operation("com.tiani.rmicfg.IServer createServer(java.lang.String, java.lang.String)"), new Operation("com.tiani.rmicfg.IServer getServer(java.lang.String)"), new Operation("java.lang.String listClassNames()[]"), new Operation("java.lang.String listServerNames()[]"), new Operation("com.tiani.rmicfg.IServer removeServer(java.lang.String)")
    };
    private static final long interfaceHash = 0x8cc52487e907c018L;
    private static final long serialVersionUID = 2L;
    private static boolean useNewInvoke;
    private static Method $method_createServer_0;
    private static Method $method_getServer_1;
    private static Method $method_listClassNames_2;
    private static Method $method_listServerNames_3;
    private static Method $method_removeServer_4;

    public ServerFactoryImpl_Stub()
    {
    }

    public ServerFactoryImpl_Stub(RemoteRef remoteref)
    {
        super(remoteref);
    }

    static Class _mthclass$(String s)
    {
        try
        {
            return Class.forName(s);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            throw new NoClassDefFoundError(classnotfoundexception.getMessage());
        }
    }

    public IServer createServer(String s, String s1)
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_createServer_0, new Object[] {
                    s, s1
                }, 0x5aeb46e92b9e92e5L);
                return (IServer)obj;
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 0, 0x8cc52487e907c018L);
            try
            {
                ObjectOutput objectoutput = remotecall.getOutputStream();
                objectoutput.writeObject(s);
                objectoutput.writeObject(s1);
            }
            catch(IOException ioexception)
            {
                throw new MarshalException("error marshalling arguments", ioexception);
            }
            super.ref.invoke(remotecall);
            IServer iserver;
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                iserver = (IServer)objectinput.readObject();
            }
            catch(IOException ioexception1)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception1);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling return", classnotfoundexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return iserver;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public IServer getServer(String s)
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_getServer_1, new Object[] {
                    s
                }, 0x6be44bb7de8f8bf9L);
                return (IServer)obj;
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 1, 0x8cc52487e907c018L);
            try
            {
                ObjectOutput objectoutput = remotecall.getOutputStream();
                objectoutput.writeObject(s);
            }
            catch(IOException ioexception)
            {
                throw new MarshalException("error marshalling arguments", ioexception);
            }
            super.ref.invoke(remotecall);
            IServer iserver;
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                iserver = (IServer)objectinput.readObject();
            }
            catch(IOException ioexception1)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception1);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling return", classnotfoundexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return iserver;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public String[] listClassNames()
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_listClassNames_2, null, 0x54a6495242aa96c2L);
                return (String[])obj;
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 2, 0x8cc52487e907c018L);
            super.ref.invoke(remotecall);
            String as[];
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                as = (String[])objectinput.readObject();
            }
            catch(IOException ioexception)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling return", classnotfoundexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return as;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public String[] listServerNames()
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_listServerNames_3, null, 0x13952f238c8d0428L);
                return (String[])obj;
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 3, 0x8cc52487e907c018L);
            super.ref.invoke(remotecall);
            String as[];
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                as = (String[])objectinput.readObject();
            }
            catch(IOException ioexception)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling return", classnotfoundexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return as;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public IServer removeServer(String s)
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_removeServer_4, new Object[] {
                    s
                }, 0xd768738c5b51872aL);
                return (IServer)obj;
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 4, 0x8cc52487e907c018L);
            try
            {
                ObjectOutput objectoutput = remotecall.getOutputStream();
                objectoutput.writeObject(s);
            }
            catch(IOException ioexception)
            {
                throw new MarshalException("error marshalling arguments", ioexception);
            }
            super.ref.invoke(remotecall);
            IServer iserver;
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                iserver = (IServer)objectinput.readObject();
            }
            catch(IOException ioexception1)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception1);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling return", classnotfoundexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return iserver;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    static 
    {
        try
        {
            (java.rmi.server.RemoteRef.class).getMethod("invoke", new Class[] {
                java.rmi.Remote.class, java.lang.reflect.Method.class, java.lang.Object[].class, Long.TYPE
            });
            useNewInvoke = true;
            $method_createServer_0 = (com.tiani.rmicfg.IServerFactory.class).getMethod("createServer", new Class[] {
                java.lang.String.class, java.lang.String.class
            });
            $method_getServer_1 = (com.tiani.rmicfg.IServerFactory.class).getMethod("getServer", new Class[] {
                java.lang.String.class
            });
            $method_listClassNames_2 = (com.tiani.rmicfg.IServerFactory.class).getMethod("listClassNames", new Class[0]);
            $method_listServerNames_3 = (com.tiani.rmicfg.IServerFactory.class).getMethod("listServerNames", new Class[0]);
            $method_removeServer_4 = (com.tiani.rmicfg.IServerFactory.class).getMethod("removeServer", new Class[] {
                java.lang.String.class
            });
        }
        catch(NoSuchMethodException _ex)
        {
            useNewInvoke = false;
        }
    }
}
