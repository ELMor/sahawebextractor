/* JTreePanel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.modalityscu;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.ModalityPPS;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;

public final class JTreePanel extends JPanel
{
    private static final DicomObjectTableModel _NULL_MODEL
	= new DicomObjectTableModel();
    private static final Tag[] MODALITY_WORKLIST_ITEM
	= IOD.accumulate(IOD.MODALITY_WORKLIST_ITEM);
    private static final Tag[] MODALITY_PERFORMED_PROCEDURE_STEP
	= IOD.accumulate(IOD.MODALITY_PERFORMED_PROCEDURE_STEP);
    private static final SimpleDateFormat _dateFormater
	= new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat _timeFormater
	= new SimpleDateFormat("HHmmss");
    private DefaultMutableTreeNode _root = new DefaultMutableTreeNode();
    private DefaultMutableTreeNode _wlNode
	= new DefaultMutableTreeNode("Worklist");
    private DefaultMutableTreeNode _ppsNode
	= new DefaultMutableTreeNode("Performed Procedure Steps");
    private JTree _tree = new JTree(_root);
    private DefaultTreeModel _treeModel = (DefaultTreeModel) _tree.getModel();
    private TreePath _selPath = null;
    private JButton _addButton = new JButton("+");
    private JButton _delButton = new JButton("-");
    private JButton _addTagsButton = new JButton(">>");
    private JButton _remTagsButton = new JButton("<<");
    private JButton _createButton = null;
    private JButton _setButton = null;
    private JSizeColumnsToFitTable _recordTable = null;
    private DicomObjectTableModel _tableModel = null;
    private PrintStream _log = null;
    
    public JTreePanel(JButton jbutton, JButton jbutton_0_,
		      JSizeColumnsToFitTable jsizecolumnstofittable,
		      PrintStream printstream) {
	super(new BorderLayout());
	_createButton = jbutton;
	_setButton = jbutton_0_;
	_recordTable = jsizecolumnstofittable;
	_tableModel = (DicomObjectTableModel) _recordTable.getModel();
	_log = printstream;
	_delButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (_selPath != null) {
		    DefaultMutableTreeNode defaultmutabletreenode
			= ((DefaultMutableTreeNode)
			   _selPath.getLastPathComponent());
		    if (defaultmutabletreenode.getLevel() == 1)
			JTreePanel.this.clear(defaultmutabletreenode);
		    else
			_treeModel
			    .removeNodeFromParent(defaultmutabletreenode);
		    _selPath = null;
		    enableButtons(_selPath);
		    _recordTable.setModel(JTreePanel._NULL_MODEL);
		}
	    }
	});
	_addButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    JTreePanel.this.insertNewNodeInto
			(_ppsNode,
			 new UserObject((JTreePanel.this.createPPS
					 (JTreePanel.this.getSelectedSPSs())),
					UserObject.PPS_LABELS));
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	});
	_addTagsButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    if (JTreePanel.this.isDicomObjectSelected())
			_tableModel.addTags
			    (JTreePanel.this.isPPSSelected()
			     ? JTreePanel.MODALITY_PERFORMED_PROCEDURE_STEP
			     : JTreePanel.MODALITY_WORKLIST_ITEM);
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	});
	_remTagsButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (JTreePanel.this.isDicomObjectSelected())
		    JTreePanel.this.load();
	    }
	});
	_tree.addTreeSelectionListener(new TreeSelectionListener() {
	    public void valueChanged(TreeSelectionEvent treeselectionevent) {
		_selPath = treeselectionevent.getNewLeadSelectionPath();
		_tableModel = JTreePanel._NULL_MODEL;
		if (JTreePanel.this.isDicomObjectSelected())
		    JTreePanel.this.load();
		else
		    _recordTable.setModel(_tableModel);
		enableButtons(_selPath);
	    }
	});
	JPanel jpanel = new JPanel();
	jpanel.add(_addButton);
	jpanel.add(_delButton);
	jpanel.add(_remTagsButton);
	jpanel.add(_addTagsButton);
	this.add(new JScrollPane(_tree), "Center");
	this.add(jpanel, "South");
	_root.add(_wlNode);
	_root.add(_ppsNode);
	_tree.setVisibleRowCount(10);
	_tree.putClientProperty("JTree.lineStyle", "Angled");
	_tree.expandRow(0);
	_tree.setRootVisible(false);
	enableButtons(_selPath);
    }
    
    public JTree getTree() {
	return _tree;
    }
    
    private boolean isPPSSelected() {
	return (isDicomObjectSelected()
		&& _selPath.getPathComponent(1) == _ppsNode);
    }
    
    private boolean isDicomObjectSelected() {
	return _selPath != null && _selPath.getPathCount() == 3;
    }
    
    private void load() {
	UserObject userobject = (UserObject) ((DefaultMutableTreeNode)
					      _selPath.getLastPathComponent())
						 .getUserObject();
	try {
	    _tableModel = new DicomObjectTableModel(userobject.getDataset(),
						    true, true, true);
	    _tableModel.addTableModelListener(new TableModelListener() {
		public void tableChanged(TableModelEvent tablemodelevent) {
		    DefaultMutableTreeNode defaultmutabletreenode
			= ((DefaultMutableTreeNode)
			   _selPath.getLastPathComponent());
		    ((UserObject) defaultmutabletreenode.getUserObject())
			.setDataset(_tableModel.getDicomObject());
		    _treeModel.nodeChanged(defaultmutabletreenode);
		}
	    });
	    _recordTable.setModel(_tableModel);
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private DicomObject[] getSelectedSPSs() throws DicomException {
	Vector vector = new Vector();
	TreePath[] treepaths = _tree.getSelectionPaths();
	UserObject userobject = null;
	if (treepaths != null) {
	    for (int i = 0; i < treepaths.length; i++) {
		Object[] objects = treepaths[i].getPath();
		if (objects.length > 2 && objects[1] == _wlNode) {
		    UserObject userobject_7_
			= (UserObject) ((DefaultMutableTreeNode) objects[2])
					   .getUserObject();
		    if (userobject == null)
			userobject = userobject_7_;
		    else if (!userobject.equalsPatient(userobject_7_))
			throw new DicomException
				  ("Cannot associate PPS to several SPS for different patients");
		    DicomObject dicomobject = userobject_7_.getDataset();
		    vector.addElement(userobject_7_.getDataset());
		}
	    }
	}
	DicomObject[] dicomobjects = new DicomObject[vector.size()];
	vector.copyInto(dicomobjects);
	return dicomobjects;
    }
    
    private DicomObject createPPS(DicomObject[] dicomobjects)
	throws DicomException {
	Date date = new Date();
	DicomObject dicomobject
	    = ModalityPPS.createCreateMPPS(UIDUtils.createUID(),
					   UIDUtils.createID(8), "ModalitySCU",
					   _dateFormater.format(date),
					   _timeFormater.format(date), "OT");
	dicomobject.setFileMetaInformation
	    (new FileMetaInformation("1.2.840.10008.3.1.2.3.3",
				     UIDUtils.createUID()));
	if (dicomobjects.length == 0)
	    return dicomobject;
	DicomObject dicomobject_8_ = dicomobjects[0];
	dicomobject.deleteItem(1210);
	for (int i = 0; i < dicomobjects.length; i++) {
	    dicomobject
		.append(1210, ModalityPPS.createSSAttributes(dicomobjects[i]));
	    DicomObject dicomobject_9_
		= (DicomObject) dicomobjects[i].get(587);
	    if (dicomobject_9_ != null)
		copyCodeSequence(dicomobject_9_, 581, dicomobject, 1209);
	}
	dicomobject.set(147, dicomobject_8_.get(147));
	dicomobject.set(148, dicomobject_8_.get(148));
	dicomobject.set(150, dicomobject_8_.get(150));
	dicomobject.set(152, dicomobject_8_.get(152));
	dicomobject.set(110, null);
	if (dicomobject_8_.getSize(110) > 0) {
	    DicomObject dicomobject_10_
		= (DicomObject) dicomobject_8_.get(107);
	    DicomObject dicomobject_11_ = new DicomObject();
	    dicomobject_11_.set(115, dicomobject_10_.get(115));
	    dicomobject_11_.set(116, dicomobject_10_.get(116));
	    dicomobject.set(110, dicomobject_11_);
	}
	DicomObject dicomobject_12_ = (DicomObject) dicomobject_8_.get(587);
	if (dicomobject_12_ == null)
	    return dicomobject;
	dicomobject.set(1198, dicomobject_12_.get(574));
	dicomobject.set(81, dicomobject_12_.get(81));
	dicomobject.set(1199, dicomobject_12_.get(583));
	dicomobject.set(1200, dicomobject_12_.get(584));
	dicomobject.set(1207, dicomobject_12_.get(580));
	if (dicomobjects.length == 1 && dicomobject_12_.getSize(548) > 0)
	    copyCodeSequence(dicomobject_8_, 548, dicomobject, 96);
	return dicomobject;
    }
    
    private static void copyCodeSequence(DicomObject dicomobject, int i,
					 DicomObject dicomobject_13_,
					 int i_14_) throws DicomException {
	int i_15_ = dicomobject.getSize(i);
	for (int i_16_ = 0; i_16_ < i_15_; i_16_++) {
	    DicomObject dicomobject_17_
		= (DicomObject) dicomobject.get(i, i_16_);
	    DicomObject dicomobject_18_ = new DicomObject();
	    dicomobject_18_.set(91, dicomobject_17_.get(91));
	    dicomobject_18_.set(92, dicomobject_17_.get(92));
	    if (dicomobject_17_.getSize(1337) > 0)
		dicomobject_18_.set(1337, dicomobject_17_.get(1337));
	    if (dicomobject_17_.getSize(93) > 0)
		dicomobject_18_.set(93, dicomobject_17_.get(93));
	    dicomobject_13_.append(i_14_, dicomobject_18_);
	}
    }
    
    private void insertNewNodeInto
	(DefaultMutableTreeNode defaultmutabletreenode,
	 UserObject userobject) {
	DefaultMutableTreeNode defaultmutabletreenode_19_
	    = new DefaultMutableTreeNode(userobject);
	_treeModel.insertNodeInto(defaultmutabletreenode_19_,
				  defaultmutabletreenode,
				  defaultmutabletreenode.getChildCount());
	_tree.addSelectionPath(new TreePath(defaultmutabletreenode_19_
						.getPath()));
    }
    
    public void addWLItem(DicomObject dicomobject) {
	try {
	    UserObject userobject
		= new UserObject(dicomobject, UserObject.WL_LABELS);
	    int i = _wlNode.getChildCount();
	    for (int i_20_ = 0; i_20_ < i; i_20_++) {
		DefaultMutableTreeNode defaultmutabletreenode
		    = (DefaultMutableTreeNode) _wlNode.getChildAt(i_20_);
		if (userobject.equalsWLitem((UserObject)
					    defaultmutabletreenode
						.getUserObject())) {
		    defaultmutabletreenode.setUserObject(userobject);
		    _treeModel.nodeChanged(defaultmutabletreenode);
		    _tree.setSelectionPath(new TreePath(defaultmutabletreenode
							    .getPath()));
		    return;
		}
	    }
	    insertNewNodeInto(_wlNode, userobject);
	} catch (DicomException dicomexception) {
	    _log.println(dicomexception);
	}
    }
    
    private void clear(DefaultMutableTreeNode defaultmutabletreenode) {
	defaultmutabletreenode.removeAllChildren();
	_treeModel.nodeStructureChanged(defaultmutabletreenode);
    }
    
    public void enableButtons(TreePath treepath) {
	boolean bool = isPPSSelected();
	_createButton.setEnabled(bool);
	_setButton.setEnabled(bool);
    }
}
