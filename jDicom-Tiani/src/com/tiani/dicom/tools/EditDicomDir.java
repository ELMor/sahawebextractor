/* EditDicomDir - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.Document;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.archimed.dicom.DDate;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.Person;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.media.DRFactory;
import com.tiani.dicom.media.DicomDir;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.media.FileSet;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JFileListDialog;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.DicomObjectFilterPath;
import com.tiani.dicom.util.DicomObjectPath;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;

public class EditDicomDir extends JApplet
{
    private static final Tag[] _EDITABLE_META_INFO_TAGS
	= { new Tag(2, 2), new Tag(2, 3), new Tag(2, 16), new Tag(2, 18),
	    new Tag(2, 19), new Tag(2, 22), new Tag(2, 256), new Tag(2, 258) };
    private static final Tag[] _EDITABLE_DICOMDIR_ID_TAGS
	= { new Tag(4, 4400), new Tag(4, 4417), new Tag(4, 4418) };
    private static final Tag[] _EDITABLE_PAT_TAGS
	= IOD.accumulate(new Tag[][] { IOD.PATIENT_MODULE,
				       { new Tag(8, 5) } });
    private static final Tag[] _EDITABLE_STUDY_TAGS
	= IOD.accumulate(new Tag[][] { IOD.GENERAL_STUDY_MODULE,
				       IOD.PATIENT_STUDY_MODULE,
				       { new Tag(8, 5) } });
    private static final Tag[] _EDITABLE_SERIES_TAGS
	= IOD.accumulate(new Tag[][] { IOD.GENERAL_SERIES_MODULE,
				       { new Tag(8, 5) } });
    private static final Tag[] _EDITABLE_IMAGE_TAGS
	= IOD.accumulate(new Tag[][] { IOD.GENERAL_IMAGE_MODULE,
				       { new Tag(4, 5376), new Tag(4, 5392),
					 new Tag(4, 5393), new Tag(4, 5394),
					 new Tag(8, 5) } });
    private static final Tag[][] _EDITABLE_RECORD_TAGS
	= { _EDITABLE_PAT_TAGS, _EDITABLE_STUDY_TAGS, _EDITABLE_SERIES_TAGS,
	    _EDITABLE_IMAGE_TAGS };
    private static final SimpleDateFormat _dateToString
	= new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat _timeToString
	= new SimpleDateFormat("HHmmss");
    private static int _seriesNo = 0;
    private static int _imageNo = 0;
    private int _verbose = 1;
    private JFrame _frame = null;
    private FileSet _fileSet = null;
    private DicomDir _dirInfo;
    private JFileChooser _fileChooser = null;
    private JFileListDialog _fileListdlg = null;
    private DRFactory _drFactory = new DRFactory();
    private JButton _newFileButton = new JButton("New");
    private JButton _loadFileButton = new JButton("Load");
    private JButton _saveFileButton = new JButton("Save");
    private JButton _compactButton = new JButton("Compact");
    private JButton _importImageButton = new JButton("Import");
    private JButton _importMultiImageButton = new JButton("Import *");
    private JButton _deleteFromFSButton = new JButton("Remove");
    private JButton _replaceDRButton = new JButton("*");
    private JButton _addDRButton = new JButton("+");
    private JButton _deleteDRButton = new JButton("-");
    private JButton _clearDRsButton = new JButton("C");
    private JSplitPane _idRecSplitPane;
    private JSplitPane _treeSplitPane;
    private JSplitPane _logTextSplitPane;
    private JTextArea _logTextArea = new JTextArea();
    private Document _logDoc = _logTextArea.getDocument();
    private PrintStream _log
	= new PrintStream(new DocumentOutputStream(_logDoc, 10000), true);
    private static final DicomObjectTableModel _NULL_MODEL
	= new DicomObjectTableModel();
    private DicomObjectTableModel _identificationTableModel = _NULL_MODEL;
    private DicomObjectTableModel _recordTableModel = _NULL_MODEL;
    private JSizeColumnsToFitTable _identificationTable;
    private JSizeColumnsToFitTable _recordTable;
    private JPanel _recordPanel;
    private MyTreeNode _selNode = null;
    private MyTreeNode _rootNode = new MyTreeNode("root");
    private MyTreeNode _lastRecNode = null;
    private JTree _tree = new JTree(_rootNode);
    private TreePath _rootPath = new TreePath(_rootNode);
    private DefaultTreeModel _treeModel = (DefaultTreeModel) _tree.getModel();
    private String[] _REC_TYPE = { "PATIENT", "STUDY", "SERIES", "IMAGE" };
    private int[] LABELS = { 147, 64, 428, 430 };
    
    private class MyTreeNode extends DefaultMutableTreeNode
    {
	public MyTreeNode(Object object) {
	    super(object);
	}
	
	public String toString() {
	    int i = this.getLevel();
	    if (i > 0) {
		try {
		    DicomObject dicomobject
			= (DicomObject) this.getUserObject();
		    String string = dicomobject.getS(LABELS[i - 1]);
		    return this == _lastRecNode ? string + "*" : string;
		} catch (DicomException dicomexception) {
		    /* empty */
		}
	    }
	    return super.toString();
	}
	
	public void addDR() throws DicomException {
	    int i = this.getLevel();
	    DicomObject dicomobject = EditDicomDir.this._createInitRec(i);
	    if (i > 0)
		_dirInfo.addLowerDR(dicomobject,
				    (DicomObject) this.getUserObject());
	    else
		_dirInfo.addRootDR(dicomobject);
	    MyTreeNode mytreenode_0_ = _lastRecNode;
	    _lastRecNode = new MyTreeNode(dicomobject);
	    int i_1_ = this.getChildCount();
	    _treeModel.insertNodeInto(_lastRecNode, this, i_1_);
	    if (mytreenode_0_ != null)
		_treeModel.nodeChanged(mytreenode_0_);
	    _tree.setSelectionPath(new TreePath(((DefaultMutableTreeNode)
						 this.getChildAt(i_1_))
						    .getPath()));
	}
	
	public Vector deleteDR() throws DicomException {
	    DicomObject dicomobject = (DicomObject) this.getUserObject();
	    Vector vector = _dirInfo.deleteDR(dicomobject);
	    _treeModel.removeNodeFromParent(this);
	    return vector;
	}
	
	public void replaceDR() throws DicomException {
	    DicomObject dicomobject = (DicomObject) this.getUserObject();
	    DicomObject dicomobject_2_ = _cloneDicomObject(dicomobject);
	    _dirInfo.replaceDR(dicomobject, dicomobject_2_);
	    this.setUserObject(dicomobject_2_);
	    int i = this.getLevel();
	    DicomObjectTableModel dicomobjecttablemodel
		= new DicomObjectTableModel(dicomobject_2_);
	    dicomobjecttablemodel
		.addTableModelListener(new TableModelListener() {
		public void tableChanged(TableModelEvent tablemodelevent) {
		    _treeModel.nodeChanged(_selNode);
		}
	    });
	    _recordTable.setModel(dicomobjecttablemodel);
	    MyTreeNode mytreenode_4_ = _lastRecNode;
	    _lastRecNode = this;
	    if (mytreenode_4_ != null)
		_treeModel.nodeChanged(mytreenode_4_);
	    _treeModel.nodeChanged(this);
	}
    }
    
    public static void main(String[] strings) {
	EditDicomDir editdicomdir = new EditDicomDir();
	editdicomdir._frame
	    = new AppletFrame("EditDicomDir v1.7.26", editdicomdir, 700, 500);
    }
    
    public void init() {
	_newFileButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		EditDicomDir.this._newFile();
	    }
	});
	_loadFileButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File file
		    = EditDicomDir.this
			  ._chooseFile("Open DICOM Directory File", "Open", 0);
		if (file != null)
		    EditDicomDir.this._load(file);
	    }
	});
	_saveFileButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File file
		    = EditDicomDir.this
			  ._chooseFile("Save DICOM Directory File", "Save", 0);
		if (file != null)
		    EditDicomDir.this._save(file);
	    }
	});
	_compactButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    _dirInfo.compact();
		    EditDicomDir.this._updateIdentificationModel();
		    MyTreeNode mytreenode = _lastRecNode;
		    _lastRecNode = EditDicomDir.this._findLastRecNode();
		    if (mytreenode != null)
			_treeModel.nodeChanged(mytreenode);
		    if (_lastRecNode != null)
			_treeModel.nodeChanged(_lastRecNode);
		    _replaceDRButton.setEnabled(_selNode != _rootNode
						&& _selNode != _lastRecNode);
		} catch (DicomException dicomexception) {
		    System.out.println("DicomException: "
				       + dicomexception.getMessage());
		}
	    }
	});
	_importImageButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File file = EditDicomDir.this._chooseFile("Select Image File",
							  "Import", 0);
		if (file != null)
		    EditDicomDir.this._importImage(file);
	    }
	});
	_importMultiImageButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File file = EditDicomDir.this._chooseFile("Select Directory",
							  "Select", 1);
		if (file != null) {
		    if (_fileListdlg == null)
			_fileListdlg
			    = new JFileListDialog(_frame, "Select files");
		    File[] files = _fileListdlg.getSelectedFiles(file);
		    if (files != null)
			EditDicomDir.this._importImages(files);
		}
	    }
	});
	_deleteFromFSButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		EditDicomDir.this._deleteFromFS();
	    }
	});
	_clearDRsButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		EditDicomDir.this._clearDRs();
	    }
	});
	_deleteDRButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		EditDicomDir.this._deleteDR();
	    }
	});
	_addDRButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		EditDicomDir.this._addDR();
	    }
	});
	_replaceDRButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		EditDicomDir.this._replaceDR();
		_replaceDRButton.setEnabled(false);
	    }
	});
	_tree.addTreeSelectionListener(new TreeSelectionListener() {
	    public void valueChanged(TreeSelectionEvent treeselectionevent) {
		TreePath treepath
		    = treeselectionevent.getNewLeadSelectionPath();
		if (treepath != null) {
		    _selNode = (MyTreeNode) treepath.getLastPathComponent();
		    int i = _selNode.getLevel();
		    DicomObjectTableModel dicomobjecttablemodel
			= EditDicomDir._NULL_MODEL;
		    boolean bool = false;
		    if (i > 0) {
			try {
			    DicomObject dicomobject
				= (DicomObject) _selNode.getUserObject();
			    dicomobjecttablemodel
				= new DicomObjectTableModel(dicomobject);
			    if (_selNode == _lastRecNode)
				dicomobjecttablemodel.addTags
				    (EditDicomDir._EDITABLE_RECORD_TAGS[i - 1],
				     true);
			    dicomobjecttablemodel
				.addTableModelListener(new TableModelListener() {
				public void tableChanged
				    (TableModelEvent tablemodelevent) {
				    _treeModel.nodeChanged(_selNode);
				}
			    });
			} catch (DicomException dicomexception) {
			    System.out.println("DicomException: "
					       + dicomexception.getMessage());
			}
		    }
		    _recordTable.setModel(dicomobjecttablemodel);
		    _addDRButton.setEnabled(i != 4);
		    _deleteDRButton.setEnabled(i != 0);
		    _deleteFromFSButton.setEnabled(i != 0 && _fileSet != null);
		    _replaceDRButton
			.setEnabled(i != 0 && _selNode != _lastRecNode);
		}
	    }
	});
	AppletContext appletcontext = this.getAppletContext();
	if (appletcontext instanceof AppletFrame)
	    appletcontext = null;
	Debug.out = _log;
	Box box = Box.createHorizontalBox();
	box.add(_newFileButton);
	box.add(_loadFileButton);
	box.add(_saveFileButton);
	box.add(_compactButton);
	box.add(_importImageButton);
	box.add(_importMultiImageButton);
	box.add(_deleteFromFSButton);
	box.add(Box.createGlue());
	box.add(new JTianiButton(appletcontext));
	JPanel jpanel = new JPanel(new BorderLayout());
	JPanel jpanel_18_ = new JPanel();
	jpanel_18_.add(_replaceDRButton);
	jpanel_18_.add(_addDRButton);
	jpanel_18_.add(_deleteDRButton);
	jpanel_18_.add(_clearDRsButton);
	jpanel.add(new JScrollPane(_tree));
	jpanel.add(jpanel_18_, "South");
	_identificationTable
	    = new JSizeColumnsToFitTable(_identificationTableModel);
	JScrollPane jscrollpane = new JScrollPane(_identificationTable);
	jscrollpane.setPreferredSize(new Dimension(200, 150));
	_recordTable = new JSizeColumnsToFitTable(_NULL_MODEL);
	_idRecSplitPane
	    = new JSplitPane(0, jscrollpane, new JScrollPane(_recordTable));
	_idRecSplitPane.setOneTouchExpandable(true);
	_treeSplitPane = new JSplitPane(1, jpanel, _idRecSplitPane);
	_treeSplitPane.setOneTouchExpandable(true);
	_logTextSplitPane = new JSplitPane(0, _treeSplitPane,
					   new JAutoScrollPane(_logTextArea));
	_logTextSplitPane.setOneTouchExpandable(true);
	Container container = this.getContentPane();
	container.add(box, "North");
	container.add(_logTextSplitPane, "Center");
	_tree.setVisibleRowCount(16);
	_tree.putClientProperty("JTree.lineStyle", "Angled");
	_tree.setCellRenderer(new MyTreeCellRenderer());
	_newFile();
    }
    
    private MyTreeNode _findLastRecNode() {
	DicomObject dicomobject = _lastRec();
	if (dicomobject == null)
	    return null;
	Enumeration enumeration = _rootNode.preorderEnumeration();
	enumeration.nextElement();
	while (enumeration.hasMoreElements()) {
	    MyTreeNode mytreenode = (MyTreeNode) enumeration.nextElement();
	    if (mytreenode.getUserObject() == dicomobject)
		return mytreenode;
	}
	return null;
    }
    
    private DicomObject _lastRec() {
	int i = _dirInfo.getSize(44);
	return i > 0 ? (DicomObject) _dirInfo.get(44, i - 1) : null;
    }
    
    private void _importImage(File file) {
	try {
	    _insertImage(file);
	    _updateIdentificationModel();
	    _clear();
	    _addNodes(_dirInfo.getFirstDR(), _rootNode, _lastRec());
	    _tree.expandPath(new TreePath(_rootNode.getPath()));
	    _treeSplitPane.resetToPreferredSizes();
	    _enableButtons();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private void _importImages(File[] files) {
	try {
	    for (int i = 0; i < files.length; i++)
		_insertImage(files[i]);
	    _updateIdentificationModel();
	    _clear();
	    _addNodes(_dirInfo.getFirstDR(), _rootNode, _lastRec());
	    _tree.expandPath(new TreePath(_rootNode.getPath()));
	    _treeSplitPane.resetToPreferredSizes();
	    _enableButtons();
	} catch (Exception exception) {
	    System.out.println(exception);
	}
    }
    
    private void _setNoSelection() {
	_recordTable.setModel(_NULL_MODEL);
	_replaceDRButton.setEnabled(false);
	_deleteDRButton.setEnabled(false);
	_deleteFromFSButton.setEnabled(false);
	_tree.setSelectionPath(_rootPath);
    }
    
    private void _enableButtons() {
	_importImageButton.setEnabled(_fileSet != null);
	_importMultiImageButton.setEnabled(_fileSet != null);
    }
    
    private String[] _fileID(File file) {
	String[] strings = null;
	try {
	    String string = new File(_fileSet.getDirFile().getParent())
				.getCanonicalPath();
	    String string_19_ = new File(file.getParent()).getCanonicalPath();
	    if (!string_19_.startsWith(string))
		return null;
	    StringTokenizer stringtokenizer
		= new StringTokenizer(file.getCanonicalPath()
					  .substring(string.length()),
				      File.separator);
	    strings = new String[stringtokenizer.countTokens()];
	    for (int i = 0; i < strings.length; i++)
		strings[i] = stringtokenizer.nextToken();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
	return strings;
    }
    
    private void _insertImage(File file) {
	String[] strings = _fileID(file);
	if (strings != null) {
	    try {
		DicomObject dicomobject = new DicomObject();
		TianiInputStream tianiinputstream
		    = new TianiInputStream(new FileInputStream(file));
		try {
		    tianiinputstream.read(dicomobject);
		} finally {
		    tianiinputstream.close();
		}
		DicomObject dicomobject_20_
		    = dicomobject.getFileMetaInformation();
		if (dicomobject_20_ == null)
		    dicomobject.setFileMetaInformation
			(new FileMetaInformation(dicomobject));
		DicomObjectPath dicomobjectpath
		    = _drFactory.createInstanceDRPath(dicomobject, strings);
		if (this != null) {
		    /* empty */
		}
		DicomObjectFilterPath dicomobjectfilterpath
		    = DRFactory.createRefSOPInstanceFilterPath(dicomobject);
		DicomObjectPath dicomobjectpath_21_
		    = _dirInfo.addRootDRpaths(dicomobjectpath,
					      dicomobjectfilterpath);
	    } catch (Exception exception) {
		System.out
		    .println(exception.toString() + exception.getMessage());
	    }
	}
    }
    
    private void _newFile() {
	try {
	    _clear();
	    _fileSet = null;
	    _dirInfo = new DicomDir(null);
	    _updateIdentificationModel();
	    _enableButtons();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private File _chooseFile(String string, String string_22_, int i) {
	try {
	    if (_fileChooser == null)
		_fileChooser = new JFileChooser(".");
	    _fileChooser.setDialogTitle(string);
	    _fileChooser.setFileSelectionMode(i);
	    if (_fileSet != null)
		_fileChooser.setSelectedFile(_fileSet.getDirFile());
	    else
		_fileChooser.setSelectedFile(new File("DICOMDIR"));
	    int i_23_ = _fileChooser.showDialog(_frame, string_22_);
	    File file = _fileChooser.getSelectedFile();
	    return i_23_ == 0 ? file : null;
	} catch (SecurityException securityexception) {
	    _log.println(securityexception);
	    showPolicyFile();
	    return null;
	}
    }
    
    private void showPolicyFile() {
	try {
	    URL url = new URL(this.getDocumentBase(),
			      this.getParameter("PolicyFile"));
	    this.getAppletContext().showDocument(url, "_blank");
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private void _load(File file) {
	try {
	    _clear();
	    _fileSet = new FileSet(file);
	    _fileSet.read(_dirInfo);
	    int i = _dirInfo.getSize(44);
	    _addNodes(_dirInfo.getFirstDR(), _rootNode, _lastRec());
	    _updateIdentificationModel();
	    _tree.expandPath(new TreePath(_rootNode.getPath()));
	    _treeSplitPane.resetToPreferredSizes();
	    _enableButtons();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private void _updateIdentificationModel() throws DicomException {
	_identificationTableModel
	    = new DicomObjectTableModel(_dirInfo, true, false, false);
	if (_dirInfo.getSize(44) == 0) {
	    _identificationTableModel
		.addFileMetaInfoTags(_EDITABLE_META_INFO_TAGS, true);
	    _identificationTableModel.addTags(_EDITABLE_DICOMDIR_ID_TAGS,
					      true);
	}
	_identificationTable.setModel(_identificationTableModel);
	_idRecSplitPane.resetToPreferredSizes();
    }
    
    private void _clear() throws DicomException {
	_rootNode.removeAllChildren();
	_lastRecNode = null;
	_treeModel.reload();
	_setNoSelection();
    }
    
    private void _save(File file) {
	try {
	    _fileSet = new FileSet(file);
	    _fileSet.write(_dirInfo);
	    _enableButtons();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private void _addDR() {
	try {
	    (_selNode != null ? _selNode : _rootNode).addDR();
	    _identificationTableModel.fireTableDataChanged();
	    _updateIdentificationModel();
	    _enableButtons();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private void _clearDRs() {
	try {
	    _dirInfo.clearDRs();
	    _identificationTableModel.fireTableDataChanged();
	    _clear();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private void _deleteDR() {
	try {
	    _selNode.deleteDR();
	    _setNoSelection();
	    _enableButtons();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private void _deleteFromFS() {
	try {
	    Vector vector = _selNode.deleteDR();
	    _setNoSelection();
	    _enableButtons();
	    int i = 0;
	    Enumeration enumeration = vector.elements();
	    while (enumeration.hasMoreElements()) {
		if (_fileSet.delete((DicomObject) enumeration.nextElement(),
				    true)
		    != null)
		    i++;
	    }
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private void _replaceDR() {
	try {
	    _selNode.replaceDR();
	    _identificationTableModel.fireTableDataChanged();
	} catch (Exception exception) {
	    System.out.println(exception.toString() + exception.getMessage());
	}
    }
    
    private void _addNodes(DicomObject dicomobject, MyTreeNode mytreenode,
			   DicomObject dicomobject_24_) throws DicomException {
	for (/**/; dicomobject != null;
	     dicomobject = _dirInfo.getNextDR(dicomobject)) {
	    MyTreeNode mytreenode_25_ = new MyTreeNode(dicomobject);
	    if (dicomobject == dicomobject_24_)
		_lastRecNode = mytreenode_25_;
	    _treeModel.insertNodeInto(mytreenode_25_, mytreenode,
				      mytreenode.getChildCount());
	    _addNodes(_dirInfo.getLowerDR(dicomobject), mytreenode_25_,
		      dicomobject_24_);
	}
    }
    
    private DicomObject _createInitRec(int i) throws DicomException {
	DicomObject dicomobject = DRFactory.create(_REC_TYPE[i]);
	switch (i) {
	case 0:
	    dicomobject.set(147, "NEW^PATIENT");
	    dicomobject.set(148, UIDUtils.createID(16));
	    break;
	case 1: {
	    Date date = new Date();
	    dicomobject.set(64, _dateToString.format(date));
	    dicomobject.set(70, _timeToString.format(date));
	    dicomobject.set(77, UIDUtils.createID(16));
	    dicomobject.set(427, UIDUtils.createID(16));
	    dicomobject.set(425, UIDUtils.createUID());
	    break;
	}
	case 2:
	    dicomobject.set(81, "OT");
	    dicomobject.set(428, new Integer(--_seriesNo));
	    dicomobject.set(426, UIDUtils.createUID());
	    break;
	case 3:
	    dicomobject.set(430, new Integer(--_imageNo));
	    break;
	}
	return dicomobject;
    }
    
    private static DicomObject _cloneDicomObject(DicomObject dicomobject)
	throws DicomException {
	DicomObject dicomobject_26_ = new DicomObject();
	Enumeration enumeration = dicomobject.enumerateVRs(false, false);
	while (enumeration.hasMoreElements())
	    _copyTagValue((TagValue) enumeration.nextElement(),
			  dicomobject_26_);
	return dicomobject_26_;
    }
    
    private static void _copyTagValue
	(TagValue tagvalue, DicomObject dicomobject) throws DicomException {
	int i = tagvalue.getGroup();
	int i_27_ = tagvalue.getElement();
	int i_28_ = tagvalue.size();
	for (int i_29_ = 0; i_29_ < i_28_; i_29_++)
	    dicomobject.append_ge(i, i_27_,
				  _cloneValue(tagvalue.getValue(i_29_)));
    }
    
    private static Object _cloneValue(Object object) throws DicomException {
	if (object instanceof String)
	    return new String((String) object);
	if (object instanceof Integer)
	    return new Integer(((Integer) object).intValue());
	if (object instanceof Long)
	    return new Long(((Long) object).longValue());
	if (object instanceof DDate)
	    return ((DDate) object).toDICOMString();
	if (object instanceof Person)
	    return ((Person) object).toDICOMString();
	if (object instanceof DicomObject)
	    return _cloneDicomObject((DicomObject) object);
	return object.toString();
    }
}
