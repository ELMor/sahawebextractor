/* QueryRetrieveSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.applet.AppletContext;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.tools.queryretrievescu.JFilterPanel;
import com.tiani.dicom.tools.queryretrievescu.JTreePanel;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.CheckParam;

public class QueryRetrieveSCU extends JApplet implements IAssociationListener
{
    private DimseExchange _dimseExchange = null;
    private VerboseAssociation _as = null;
    private static final int[] FIND_SOP_CLASS = { 4132, 4135, 4138 };
    private static final int[] MOVE_SOP_CLASS = { 4133, 4136, 4139 };
    private boolean _autoClose = false;
    private int _curQRMsgID = 0;
    private int _curQRSOPclass = 0;
    private final IDimseRspListener _queryFindSCU;
    private final IDimseRspListener _retrieveMoveSCU;
    private Properties _params;
    public static final String[] PARAM_NAMES
	= { "Host", "Port", "CalledTitle", "CallingTitle", "MoveDestination",
	    "InfoModel", "Priority", "Verbose" };
    public static final String[] INFO_MODELS
	= { "PatientRoot", "StudyRoot", "PatientStudyOnly" };
    public static final String[] PRIORITIES = { "MEDIUM", "HIGH", "LOW" };
    private static Hashtable PARAM_CHECKS = new Hashtable();
    private JButton _openButton;
    private JButton _closeButton;
    private JButton _echoButton;
    private JButton _findButton;
    private JButton _moveButton;
    private JButton _cancelButton;
    private static final DicomObjectTableModel _NULL_MODEL;
    private JSizeColumnsToFitTable _recordTable;
    private JFilterPanel _filterPanel;
    private JTreePanel _treePanel;
    private JTabbedPane tabbedPane;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;
    private JProgressBar _progressBar;
    
    public static void main(String[] strings) {
	final String propSpec
	    = strings.length > 0 ? strings[0] : "QueryRetrieveSCU.properties";
	Properties properties = null;
	try {
	    properties = loadProperties(new FileInputStream(propSpec));
	} catch (Exception exception) {
	    /* empty */
	}
	final QueryRetrieveSCU instance = new QueryRetrieveSCU(properties);
	new AppletFrame("QueryRetrieveSCU v1.7.26", instance, 700, 500, new WindowAdapter() {
	    public void windowClosing(WindowEvent windowevent) {
		try {
		    instance.storeParams(propSpec);
		} catch (Exception exception) {
		    System.out.println(exception);
		}
		System.exit(0);
	    }
	});
    }
    
    public QueryRetrieveSCU() {
	_queryFindSCU = new IDimseRspListener() {
	    private Vector rspQueue = new Vector();
	    private Runnable addChild = new Runnable() {
		public void run() {
		    try {
			while (!rspQueue.isEmpty()) {
			    _treePanel.addQueryResult((DicomObject)
						      rspQueue.elementAt(0));
			    rspQueue.removeElementAt(0);
			}
		    } catch (DicomException dicomexception) {
			_log.println(dicomexception);
		    }
		}
	    };
	    private Runnable finish = new Runnable() {
		public void run() {
		    _cancelButton.setEnabled(false);
		    if (_autoClose) {
			_autoClose = false;
			QueryRetrieveSCU.this.close();
		    }
		}
	    };
	    
	    public void handleRSP
		(DimseExchange dimseexchange, int i, int i_1_,
		 DicomMessage dicommessage)
		throws DicomException {
		if (Status.isPending(i_1_)) {
		    rspQueue.addElement(dicommessage.getDataset());
		    SwingUtilities.invokeLater(addChild);
		} else
		    SwingUtilities.invokeLater(finish);
	    }
	};
	_retrieveMoveSCU = new IDimseRspListener() {
	    private int max = 0;
	    private int val = 0;
	    private Runnable update = new Runnable() {
		public void run() {
		    _progressBar.setMinimum(0);
		    _progressBar.setMaximum(max);
		    _progressBar.setValue(val);
		}
	    };
	    private Runnable finish = new Runnable() {
		public void run() {
		    _progressBar.setValue(0);
		    _cancelButton.setEnabled(false);
		    if (_autoClose) {
			_autoClose = false;
			QueryRetrieveSCU.this.close();
		    }
		}
	    };
	    
	    public void handleRSP
		(DimseExchange dimseexchange, int i, int i_3_,
		 DicomMessage dicommessage)
		throws DicomException {
		Object object = dicommessage.get(20);
		if (object != null) {
		    val = ((Integer) object).intValue();
		    object = dicommessage.get(19);
		    if (object != null) {
			max = val + ((Integer) object).intValue();
			SwingUtilities.invokeLater(update);
		    }
		}
		if (!Status.isPending(i_3_))
		    SwingUtilities.invokeLater(finish);
	    }
	};
	_params = null;
	_openButton = new JButton("Open");
	_closeButton = new JButton("Close");
	_echoButton = new JButton("Echo");
	_findButton = new JButton("Query");
	_moveButton = new JButton("Retrieve");
	_cancelButton = new JButton("Cancel");
	_recordTable = new JSizeColumnsToFitTable(_NULL_MODEL);
	_filterPanel = new JFilterPanel();
	_treePanel = new JTreePanel(_findButton, _moveButton, _recordTable,
				    _filterPanel);
	tabbedPane = new JTabbedPane(2);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 10000), true);
	_progressBar = new JProgressBar();
    }
    
    protected QueryRetrieveSCU(Properties properties) {
	_queryFindSCU = new IDimseRspListener() {
	    private Vector rspQueue = new Vector();
	    private Runnable addChild = new Runnable() {
		public void run() {
		    try {
			while (!rspQueue.isEmpty()) {
			    _treePanel.addQueryResult((DicomObject)
						      rspQueue.elementAt(0));
			    rspQueue.removeElementAt(0);
			}
		    } catch (DicomException dicomexception) {
			_log.println(dicomexception);
		    }
		}
	    };
	    private Runnable finish = new Runnable() {
		public void run() {
		    _cancelButton.setEnabled(false);
		    if (_autoClose) {
			_autoClose = false;
			QueryRetrieveSCU.this.close();
		    }
		}
	    };
	    
	    public void handleRSP
		(DimseExchange dimseexchange, int i, int i_5_,
		 DicomMessage dicommessage)
		throws DicomException {
		if (Status.isPending(i_5_)) {
		    rspQueue.addElement(dicommessage.getDataset());
		    SwingUtilities.invokeLater(addChild);
		} else
		    SwingUtilities.invokeLater(finish);
	    }
	};
	_retrieveMoveSCU = new IDimseRspListener() {
	    private int max = 0;
	    private int val = 0;
	    private Runnable update = new Runnable() {
		public void run() {
		    _progressBar.setMinimum(0);
		    _progressBar.setMaximum(max);
		    _progressBar.setValue(val);
		}
	    };
	    private Runnable finish = new Runnable() {
		public void run() {
		    _progressBar.setValue(0);
		    _cancelButton.setEnabled(false);
		    if (_autoClose) {
			_autoClose = false;
			QueryRetrieveSCU.this.close();
		    }
		}
	    };
	    
	    public void handleRSP
		(DimseExchange dimseexchange, int i, int i_7_,
		 DicomMessage dicommessage)
		throws DicomException {
		Object object = dicommessage.get(20);
		if (object != null) {
		    val = ((Integer) object).intValue();
		    object = dicommessage.get(19);
		    if (object != null) {
			max = val + ((Integer) object).intValue();
			SwingUtilities.invokeLater(update);
		    }
		}
		if (!Status.isPending(i_7_))
		    SwingUtilities.invokeLater(finish);
	    }
	};
	_params = null;
	_openButton = new JButton("Open");
	_closeButton = new JButton("Close");
	_echoButton = new JButton("Echo");
	_findButton = new JButton("Query");
	_moveButton = new JButton("Retrieve");
	_cancelButton = new JButton("Cancel");
	_recordTable = new JSizeColumnsToFitTable(_NULL_MODEL);
	_filterPanel = new JFilterPanel();
	_treePanel = new JTreePanel(_findButton, _moveButton, _recordTable,
				    _filterPanel);
	tabbedPane = new JTabbedPane(2);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 10000), true);
	_progressBar = new JProgressBar();
	_params = properties;
    }
    
    public void init() {
	_openButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		QueryRetrieveSCU.this.open();
	    }
	});
	_closeButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		QueryRetrieveSCU.this.close();
	    }
	});
	_echoButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		QueryRetrieveSCU.this.echo(QueryRetrieveSCU.this.isOpen());
	    }
	});
	_findButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		QueryRetrieveSCU.this.find(QueryRetrieveSCU.this.isOpen());
	    }
	});
	_moveButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		QueryRetrieveSCU.this.move(QueryRetrieveSCU.this.isOpen());
	    }
	});
	_cancelButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		QueryRetrieveSCU.this.cancel();
	    }
	});
	final JTree tree = _treePanel.getTree();
	tree.addMouseListener(new MouseAdapter() {
	    public void mouseClicked(MouseEvent mouseevent) {
		TreePath treepath = tree.getPathForLocation(mouseevent.getX(),
							    mouseevent.getY());
		if (treepath != null) {
		    DefaultMutableTreeNode defaultmutabletreenode
			= ((DefaultMutableTreeNode)
			   treepath.getLastPathComponent());
		    if (mouseevent.getClickCount() == 2
			&& defaultmutabletreenode.getLevel() < 4
			&& defaultmutabletreenode.isLeaf())
			QueryRetrieveSCU.this
			    .find(QueryRetrieveSCU.this.isOpen());
		}
	    }
	});
	tree.setVisibleRowCount(5);
	tree.putClientProperty("JTree.lineStyle", "Angled");
	tree.setCellRenderer(new MyTreeCellRenderer());
	AppletContext appletcontext = this.getAppletContext();
	if (appletcontext instanceof AppletFrame)
	    appletcontext = null;
	Box box = Box.createHorizontalBox();
	box.add(_openButton);
	box.add(_closeButton);
	box.add(_echoButton);
	box.add(_findButton);
	box.add(_moveButton);
	box.add(_cancelButton);
	box.add(Box.createGlue());
	box.add(new JTianiButton(appletcontext));
	Debug.out = _log;
	if (appletcontext != null)
	    getAppletParams();
	if (_params == null || !verifyParams()) {
	    try {
		_params
		    = loadProperties(EditDicomObject.class.getResourceAsStream
				     ("QueryRetrieveSCU.properties"));
	    } catch (Exception exception) {
		Assert.fail
		    ("Failed to load QueryRetrieveSCU.properties ressource");
	    }
	}
	initTabbedPane();
	Container container = this.getContentPane();
	container.add(box, "North");
	container.add(tabbedPane, "Center");
	container.add(_progressBar, "South");
	enableButtons(false);
	_treePanel.enableButtons(0);
    }
    
    private void initTabbedPane() {
	JPropertiesTable jpropertiestable
	    = new JPropertiesTable(PARAM_NAMES, _params, PARAM_CHECKS);
	tabbedPane.add("Props", new JScrollPane(jpropertiestable));
	tabbedPane.add("Filter", _filterPanel);
	JSplitPane jsplitpane
	    = new JSplitPane(1, _treePanel, new JScrollPane(_recordTable));
	tabbedPane.add("Result", jsplitpane);
	tabbedPane.add("Log", new JAutoScrollPane(_logTextArea));
    }
    
    static Properties loadProperties(InputStream inputstream)
	throws IOException {
	Properties properties = new Properties();
	try {
	    properties.load(inputstream);
	} finally {
	    inputstream.close();
	}
	return properties;
    }
    
    private void getAppletParams() {
	_params = new Properties();
	for (int i = 0; i < PARAM_NAMES.length; i++) {
	    String string;
	    if ((string = this.getParameter(PARAM_NAMES[i])) != null)
		_params.put(PARAM_NAMES[i], string);
	}
    }
    
    private void storeParams(String string) throws IOException {
	CheckParam.verify(_params, PARAM_CHECKS);
	FileOutputStream fileoutputstream = new FileOutputStream(string);
	try {
	    _params.store(fileoutputstream, "Properties for QueryRetrieveSCU");
	} finally {
	    fileoutputstream.close();
	}
    }
    
    private boolean verifyParams() {
	try {
	    CheckParam.verify(_params, PARAM_CHECKS);
	    Debug.DEBUG = getIntParam("Verbose");
	    return true;
	} catch (Exception exception) {
	    _log.println(exception);
	    return false;
	}
    }
    
    private boolean getBooleanParam(String string) {
	String string_15_ = _params.getProperty(string);
	return (string_15_ != null
		&& "true".compareTo(string_15_.toLowerCase()) == 0);
    }
    
    private int getIntParam(String string) {
	return Integer.parseInt(_params.getProperty(string));
    }
    
    private void enableButtons(boolean bool) {
	_openButton.setEnabled(!bool);
	_closeButton.setEnabled(bool);
	if (!bool)
	    _cancelButton.setEnabled(false);
    }
    
    public void associateRequestReceived(VerboseAssociation verboseassociation,
					 Request request) {
	/* empty */
    }
    
    public void associateResponseReceived
	(VerboseAssociation verboseassociation, Response response) {
	/* empty */
    }
    
    public void associateRequestSent(VerboseAssociation verboseassociation,
				     Request request) {
	/* empty */
    }
    
    public void associateResponseSent(VerboseAssociation verboseassociation,
				      Response response) {
	/* empty */
    }
    
    public void releaseRequestReceived(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseReceived
	(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseRequestSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void abortReceived(VerboseAssociation verboseassociation,
			      Abort abort) {
	/* empty */
    }
    
    public void abortSent(VerboseAssociation verboseassociation, int i,
			  int i_16_) {
	/* empty */
    }
    
    public void socketClosed(VerboseAssociation verboseassociation) {
	enableButtons(false);
    }
    
    private boolean isOpen() {
	return _as != null && _as.isOpen();
    }
    
    private void open() {
	tabbedPane.setSelectedIndex(3);
	if (verifyParams()) {
	    if (isOpen())
		close();
	    try {
		Request request = new Request();
		request.setCalledTitle(_params.getProperty("CalledTitle"));
		request.setCallingTitle(_params.getProperty("CallingTitle"));
		int[] is = { 8193 };
		request.addPresentationContext(4097, is);
		request.addPresentationContext(FIND_SOP_CLASS[0], is);
		request.addPresentationContext(FIND_SOP_CLASS[1], is);
		request.addPresentationContext(FIND_SOP_CLASS[2], is);
		request.addPresentationContext(MOVE_SOP_CLASS[0], is);
		request.addPresentationContext(MOVE_SOP_CLASS[1], is);
		request.addPresentationContext(MOVE_SOP_CLASS[2], is);
		Socket socket = new Socket(_params.getProperty("Host"),
					   getIntParam("Port"));
		Requestor requestor = new Requestor(socket, request);
		requestor.addAssociationListener(this);
		_as = requestor.openAssoc();
		if (_as != null) {
		    _dimseExchange
			= new DimseExchange(_as, null, false, false, 1);
		    Thread thread = new Thread(_dimseExchange);
		    thread.start();
		    enableButtons(true);
		}
	    } catch (SecurityException securityexception) {
		_log.println(securityexception.getMessage());
		showPolicyFile();
	    } catch (Exception exception) {
		_log.println(exception.getMessage());
	    }
	}
    }
    
    private void showPolicyFile() {
	try {
	    URL url = new URL(this.getDocumentBase(),
			      this.getParameter("PolicyFile"));
	    this.getAppletContext().showDocument(url, "_blank");
	} catch (Exception exception) {
	    _log.println(exception.getMessage());
	}
    }
    
    private void close() {
	if (isOpen()) {
	    if (_cancelButton.isEnabled()) {
		_autoClose = true;
		cancel();
	    } else {
		try {
		    _dimseExchange.releaseAssoc();
		    _dimseExchange = null;
		    _as = null;
		} catch (Exception exception) {
		    _log.println(exception);
		}
	    }
	}
    }
    
    private void echo(boolean bool) {
	tabbedPane.setSelectedIndex(3);
	if (!bool)
	    open();
	if (isOpen()) {
	    try {
		int i = _as.nextMessageID();
		DicomMessage dicommessage = new DicomMessage(48, i, null);
		dicommessage.affectedSOPclassUID("1.2.840.10008.1.1");
		_dimseExchange.sendRQ(i, 4097, dicommessage, null);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	    if (!bool)
		close();
	}
    }
    
    private void find(boolean bool) {
	tabbedPane.setSelectedIndex(3);
	if (verifyParams()) {
	    if (!bool)
		open();
	    if (isOpen()) {
		try {
		    int i = _as.nextMessageID();
		    int i_17_ = (FIND_SOP_CLASS
				 [indexOf(INFO_MODELS,
					  _params.getProperty("InfoModel"))]);
		    DicomObject dicomobject
			= _treePanel.getFindIdentifer(i_17_);
		    DicomMessage dicommessage
			= new DicomMessage(32, i, dicomobject);
		    dicommessage.affectedSOPclassUID(UID.getUIDEntry(i_17_)
							 .getValue());
		    dicommessage.priority
			(indexOf(PRIORITIES, _params.getProperty("Priority")));
		    _dimseExchange.sendRQ(i, i_17_, dicommessage,
					  _queryFindSCU);
		    _cancelButton.setEnabled(true);
		    _curQRMsgID = i;
		    _curQRSOPclass = i_17_;
		    tabbedPane.setSelectedIndex(2);
		} catch (Exception exception) {
		    _log.println(exception);
		}
		if (!bool)
		    _autoClose = true;
	    }
	}
    }
    
    private void move(boolean bool) {
	tabbedPane.setSelectedIndex(3);
	if (verifyParams()) {
	    if (!bool)
		open();
	    if (isOpen()) {
		try {
		    int i = _as.nextMessageID();
		    int i_18_ = (MOVE_SOP_CLASS
				 [indexOf(INFO_MODELS,
					  _params.getProperty("InfoModel"))]);
		    DicomObject dicomobject
			= _treePanel.getMoveIdentifer(i_18_);
		    DicomMessage dicommessage
			= new DicomMessage(33, i, dicomobject);
		    dicommessage.affectedSOPclassUID(UID.getUIDEntry(i_18_)
							 .getValue());
		    dicommessage.priority
			(indexOf(PRIORITIES, _params.getProperty("Priority")));
		    dicommessage.moveDestination
			(_params.getProperty("MoveDestination"));
		    _dimseExchange.sendRQ(i, i_18_, dicommessage,
					  _retrieveMoveSCU);
		    _cancelButton.setEnabled(true);
		    _curQRMsgID = i;
		    _curQRSOPclass = i_18_;
		} catch (Exception exception) {
		    _log.println(exception);
		}
		if (!bool)
		    _autoClose = true;
	    }
	}
    }
    
    private void cancel() {
	if (isOpen()) {
	    try {
		_dimseExchange.sendCancelRQ(_curQRMsgID, _curQRSOPclass);
	    } catch (Exception exception) {
		_log.println(exception);
	    }
	}
    }
    
    public static int indexOf(String[] strings, String string) {
	int i = strings.length;
	while (i-- > 0) {
	    if (strings[i].equals(string))
		return i;
	}
	return i;
    }
    
    static {
	PARAM_CHECKS.put("Port", CheckParam.range(100, 65535));
	PARAM_CHECKS.put("InfoModel", CheckParam.enum(INFO_MODELS));
	PARAM_CHECKS.put("Priority", CheckParam.enum(PRIORITIES));
	PARAM_CHECKS.put("Verbose",
			 CheckParam.enum(new String[] { "0", "1", "2", "3",
							"4", "5" }));
	_NULL_MODEL = new DicomObjectTableModel();
    }
}
