/* IODNode - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import javax.swing.tree.DefaultMutableTreeNode;

import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.Tag;

public final class IODNode extends DefaultMutableTreeNode
{
    private final String _name;
    private final String _sopClassUID;
    private static IODNode _CT_NODE
	= new IODNode("CT", IOD.accumulate(IOD.COMPUTED_TOMOGRAPHY_IMAGE),
		      "1.2.840.10008.5.1.4.1.1.2");
    private static IODNode _MR_NODE;
    private static IODNode _NM_NODE;
    private static IODNode _BASIC_STUDY_DESCRIPTOR_NODE;
    private static IODNode _MWL_NODE;
    private static IODNode _MPPS_NODE;
    private static IODNode _PS_NODE;
    private static IODNode _BASIC_TEXT_SR_NODE;
    private static IODNode _ENHANCED_SR_NODE;
    private static IODNode _COMPREHENSIVE_SR_NODE;
    private static DefaultMutableTreeNode _ROOT;
    
    public IODNode(String string, Tag[] tags, String string_0_) {
	super(tags);
	_name = string;
	_sopClassUID = string_0_;
    }
    
    public IODNode(String string, Tag[] tags) {
	super(tags);
	_name = string;
	_sopClassUID = null;
    }
    
    public String toString() {
	return _name;
    }
    
    public String getSOPClassUID() {
	return _sopClassUID;
    }
    
    public boolean isFileMetaInfo() {
	return this.getUserObject() == IOD.FILE_META_INFO;
    }
    
    public static DefaultMutableTreeNode root() {
	return _ROOT;
    }
    
    static {
	_CT_NODE.add(new IODNode("Patient", IOD.PATIENT_MODULE));
	_CT_NODE.add(new IODNode("General Study", IOD.GENERAL_STUDY_MODULE));
	_CT_NODE.add(new IODNode("Patient Study", IOD.PATIENT_STUDY_MODULE));
	_CT_NODE.add(new IODNode("Frame of Reference",
				 IOD.FRAME_OF_REFERENCE_MODULE));
	_CT_NODE.add(new IODNode("General Series", IOD.GENERAL_SERIES_MODULE));
	_CT_NODE.add(new IODNode("General Equipment",
				 IOD.GENERAL_EQUIPMENT_MODULE));
	_CT_NODE.add(new IODNode("General Image", IOD.GENERAL_IMAGE_MODULE));
	_CT_NODE.add(new IODNode("Image Plane", IOD.IMAGE_PLANE_MODULE));
	_CT_NODE.add(new IODNode("Image Pixel", IOD.IMAGE_PIXEL_MODULE));
	_CT_NODE.add(new IODNode("Contrast/Bolus", IOD.CONTRAST_BOLUS_MODULE));
	_CT_NODE.add(new IODNode("CT Image", IOD.CT_IMAGE_MODULE));
	_CT_NODE.add(new IODNode("Overlay Plane", IOD.OVERLAY_PLANE_MODULE));
	_CT_NODE.add(new IODNode("VOI LUT", IOD.VOI_LUT_MODULE));
	_CT_NODE.add(new IODNode("SOP Common", IOD.COMMON_SOP_MODULE));
	_MR_NODE
	    = new IODNode("MR", IOD.accumulate(IOD.MAGNETIC_RESONANCE_IMAGE),
			  "1.2.840.10008.5.1.4.1.1.4");
	_MR_NODE.add(new IODNode("Patient", IOD.PATIENT_MODULE));
	_MR_NODE.add(new IODNode("General Study", IOD.GENERAL_STUDY_MODULE));
	_MR_NODE.add(new IODNode("Patient Study", IOD.PATIENT_STUDY_MODULE));
	_MR_NODE.add(new IODNode("Frame of Reference",
				 IOD.FRAME_OF_REFERENCE_MODULE));
	_MR_NODE.add(new IODNode("General Series", IOD.GENERAL_SERIES_MODULE));
	_MR_NODE.add(new IODNode("General Equipment",
				 IOD.GENERAL_EQUIPMENT_MODULE));
	_MR_NODE.add(new IODNode("General Image", IOD.GENERAL_IMAGE_MODULE));
	_MR_NODE.add(new IODNode("Image Plane", IOD.IMAGE_PLANE_MODULE));
	_MR_NODE.add(new IODNode("Image Pixel", IOD.IMAGE_PIXEL_MODULE));
	_MR_NODE.add(new IODNode("Contrast/Bolus", IOD.CONTRAST_BOLUS_MODULE));
	_MR_NODE.add(new IODNode("MR Image", IOD.MR_IMAGE_MODULE));
	_MR_NODE.add(new IODNode("Overlay Plane", IOD.OVERLAY_PLANE_MODULE));
	_MR_NODE.add(new IODNode("VOI LUT", IOD.VOI_LUT_MODULE));
	_MR_NODE.add(new IODNode("SOP Common", IOD.COMMON_SOP_MODULE));
	_NM_NODE
	    = new IODNode("NM", IOD.accumulate(IOD.NUCLEAR_MEDICINE_IMAGE),
			  "1.2.840.10008.5.1.4.1.1.20");
	_NM_NODE.add(new IODNode("Patient", IOD.PATIENT_MODULE));
	_NM_NODE.add(new IODNode("General Study", IOD.GENERAL_STUDY_MODULE));
	_NM_NODE.add(new IODNode("Patient Study", IOD.PATIENT_STUDY_MODULE));
	_NM_NODE.add(new IODNode("Frame of Reference",
				 IOD.FRAME_OF_REFERENCE_MODULE));
	_NM_NODE.add(new IODNode("General Series", IOD.GENERAL_SERIES_MODULE));
	_NM_NODE.add(new IODNode("NM/PET Patient Orientation",
				 IOD.NM_PET_PATIENT_ORIENTATION_MODULE));
	_NM_NODE.add(new IODNode("General Equipment",
				 IOD.GENERAL_EQUIPMENT_MODULE));
	_NM_NODE.add(new IODNode("General Image", IOD.GENERAL_IMAGE_MODULE));
	_NM_NODE.add(new IODNode("Image Pixel", IOD.IMAGE_PIXEL_MODULE));
	_NM_NODE.add(new IODNode("NM Image Pixel", IOD.NM_IMAGE_PIXEL_MODULE));
	_NM_NODE.add(new IODNode("Multi-frame", IOD.MULTIFRAME_MODULE));
	_NM_NODE.add(new IODNode("NM Multi-frame", IOD.NM_MULTIFRAME_MODULE));
	_NM_NODE.add(new IODNode("NM Image", IOD.NM_IMAGE_MODULE));
	_NM_NODE.add(new IODNode("NM Isotope", IOD.NM_ISOTOPE_MODULE));
	_NM_NODE.add(new IODNode("NM Detector", IOD.NM_DETECTOR_MODULE));
	_NM_NODE.add(new IODNode("NM TOMO Acquisition",
				 IOD.NM_TOMO_ACQUISITION_MODULE));
	_NM_NODE.add(new IODNode("NM Multi-gated Acquisition",
				 IOD.NM_MULTIGATED_ACQUISITION_MODULE));
	_NM_NODE.add(new IODNode("NM Phase", IOD.NM_PHASE_MODULE));
	_NM_NODE.add(new IODNode("NM Reconstruction",
				 IOD.NM_RECONSTRUCTION_MODULE));
	_NM_NODE.add(new IODNode("Overlay Plane", IOD.OVERLAY_PLANE_MODULE));
	_NM_NODE.add(new IODNode("Multi-frame Overlay",
				 IOD.MULTIFRAME_OVERLAY_MODULE));
	_NM_NODE.add(new IODNode("Curve", IOD.CURVE_MODULE));
	_NM_NODE.add(new IODNode("VOI LUT", IOD.VOI_LUT_MODULE));
	_NM_NODE.add(new IODNode("SOP Common", IOD.COMMON_SOP_MODULE));
	_BASIC_STUDY_DESCRIPTOR_NODE
	    = new IODNode("Basic Study Descriptor",
			  IOD.accumulate(IOD.BASIC_STUDY_DESCRIPTOR),
			  "1.2.840.10008.5.1.4.1.1.9");
	_BASIC_STUDY_DESCRIPTOR_NODE
	    .add(new IODNode("Patient Summary", IOD.PATIENT_SUMMARY_MODULE));
	_BASIC_STUDY_DESCRIPTOR_NODE
	    .add(new IODNode("Study Content", IOD.STUDY_CONTENT_MODULE));
	_BASIC_STUDY_DESCRIPTOR_NODE.add(new IODNode("SOP Common",
						     IOD.COMMON_SOP_MODULE));
	_MWL_NODE = new IODNode("Modality Worklist Item",
				IOD.accumulate(IOD.MODALITY_WORKLIST_ITEM));
	_MWL_NODE.add(new IODNode("Specific Character Set",
				  IOD.SPECIFIC_CHARACTER_SET));
	_MWL_NODE.add(new IODNode("Patient Relationship",
				  IOD.PATIENT_RELATIONSHIP_MODULE));
	_MWL_NODE.add(new IODNode("Patient Identification",
				  IOD.PATIENT_IDENTIFICATION_MODULE));
	_MWL_NODE.add(new IODNode("Patient Demographic",
				  IOD.PATIENT_DEMOGRAPHIC_MODULE));
	_MWL_NODE.add(new IODNode("Patient Medical",
				  IOD.PATIENT_MEDICAL_MODULE));
	_MWL_NODE.add(new IODNode("Visit Relationship",
				  IOD.VISIT_RELATIONSHIP_MODULE));
	_MWL_NODE.add(new IODNode("Visit Identification",
				  IOD.VISIT_IDENTIFICATION_MODULE));
	_MWL_NODE.add(new IODNode("Visit Status", IOD.VISIT_STATUS_MODULE));
	_MWL_NODE.add(new IODNode("Visit Admission",
				  IOD.VISIT_ADMISSION_MODULE));
	_MWL_NODE.add(new IODNode("Scheduled Procedure Step",
				  IOD.SCHEDULED_PROCEDURE_STEP_MODULE));
	_MWL_NODE.add(new IODNode("Requested Procedure",
				  IOD.REQUESTED_PROCEDURE_MODULE));
	_MWL_NODE.add(new IODNode("Imaging Service Request",
				  IOD.IMAGING_SERVICE_REQUEST_MODULE));
	_MPPS_NODE
	    = new IODNode("Modality PPS",
			  IOD.accumulate(IOD
					 .MODALITY_PERFORMED_PROCEDURE_STEP),
			  "1.2.840.10008.3.1.2.3.3");
	_MPPS_NODE.add(new IODNode("PPS Relationship",
				   IOD.PPS_RELATIONSHIP_MODULE));
	_MPPS_NODE.add(new IODNode("PPS Information",
				   IOD.PPS_INFORMATION_MODULE));
	_MPPS_NODE.add(new IODNode("Image Acquisition Results",
				   IOD.IMAGE_ACQUISITION_RESULTS_MODULE));
	_MPPS_NODE.add(new IODNode("Radiation Dose",
				   IOD.RADIATION_DOSE_MODULE));
	_MPPS_NODE.add
	    (new IODNode("Billing and Material",
			 IOD.BILLING_AND_MATERIAL_MANAGEMENT_CODE_MODULE));
	_MPPS_NODE.add(new IODNode("SOP Common", IOD.COMMON_SOP_MODULE));
	_PS_NODE = (new IODNode
		    ("Presentation State",
		     IOD.accumulate(IOD.GRAYSCALE_SOFTCOPY_PRESENTATION_STATE),
		     "1.2.840.10008.5.1.4.1.1.11.1"));
	_PS_NODE.add(new IODNode("Patient", IOD.PATIENT_MODULE));
	_PS_NODE.add(new IODNode("General Study", IOD.GENERAL_STUDY_MODULE));
	_PS_NODE.add(new IODNode("Patient Study", IOD.PATIENT_STUDY_MODULE));
	_PS_NODE.add(new IODNode("General Series", IOD.GENERAL_SERIES_MODULE));
	_PS_NODE.add(new IODNode("General Equipment",
				 IOD.GENERAL_EQUIPMENT_MODULE));
	_PS_NODE.add(new IODNode("Mask", IOD.MASK_MODULE));
	_PS_NODE.add(new IODNode("Display Shutter",
				 IOD.DISPLAY_SHUTTER_MODULE));
	_PS_NODE.add(new IODNode("Bitmap Display Shutter",
				 IOD.BM_DISPLAY_SHUTTER_MODULE));
	_PS_NODE.add(new IODNode("Overlay Plane", IOD.OVERLAY_PLANE_MODULE));
	_PS_NODE.add(new IODNode("Displayed Area", IOD.DISPLAYED_AREA_MODULE));
	_PS_NODE.add(new IODNode("Overlay/Curve Activation",
				 IOD.OVERLAY_CURVE_ACTIVATION_MODULE));
	_PS_NODE.add(new IODNode("Graphic Annotation",
				 IOD.GRAPHIC_ANNOTATION_MODULE));
	_PS_NODE.add(new IODNode("Spatial Transformation",
				 IOD.SPATIAL_TRANSFORMATION_MODULE));
	_PS_NODE.add(new IODNode("Graphic Layer", IOD.GRAPHIC_LAYER_MODULE));
	_PS_NODE.add(new IODNode("Modality LUT", IOD.MODALITY_LUT_MODULE));
	_PS_NODE.add(new IODNode("Softcopy VOI LUT",
				 IOD.SOFTCOPY_VOI_LUT_MODULE));
	_PS_NODE.add(new IODNode("Softcopy Presentation LUT",
				 IOD.SOFTCOPY_PRESENTATION_LUT_MODULE));
	_PS_NODE.add(new IODNode("Presentation State",
				 IOD.PRESENTATION_STATE_MODULE));
	_PS_NODE.add(new IODNode("SOP Common", IOD.COMMON_SOP_MODULE));
	_BASIC_TEXT_SR_NODE
	    = new IODNode("Basic Text SR",
			  IOD.accumulate(IOD.STRUCTURED_REPORT_DOCUMENT),
			  "1.2.840.10008.5.1.4.1.1.88.11");
	_BASIC_TEXT_SR_NODE.add(new IODNode("Patient", IOD.PATIENT_MODULE));
	_BASIC_TEXT_SR_NODE.add
	    (new IODNode("Specimen Identification",
			 IOD.SPECIMEN_IDENTIFICATION_MODULE));
	_BASIC_TEXT_SR_NODE.add(new IODNode("General Study",
					    IOD.GENERAL_STUDY_MODULE));
	_BASIC_TEXT_SR_NODE.add(new IODNode("Patient Study",
					    IOD.PATIENT_STUDY_MODULE));
	_BASIC_TEXT_SR_NODE.add(new IODNode("SR Document Series",
					    IOD.SR_DOCUMENT_SERIES_MODULE));
	_BASIC_TEXT_SR_NODE.add(new IODNode("General Equipment",
					    IOD.GENERAL_EQUIPMENT_MODULE));
	_BASIC_TEXT_SR_NODE.add(new IODNode("SR Document General",
					    IOD.SR_DOCUMENT_GENERAL_MODULE));
	_BASIC_TEXT_SR_NODE.add(new IODNode("SR Document Content",
					    IOD.SR_DOCUMENT_CONTENT_MODULE));
	_BASIC_TEXT_SR_NODE.add(new IODNode("SOP Common",
					    IOD.COMMON_SOP_MODULE));
	_ENHANCED_SR_NODE
	    = new IODNode("Enhanced SR",
			  IOD.accumulate(IOD.STRUCTURED_REPORT_DOCUMENT),
			  "1.2.840.10008.5.1.4.1.1.88.22");
	_ENHANCED_SR_NODE.add(new IODNode("Patient", IOD.PATIENT_MODULE));
	_ENHANCED_SR_NODE.add(new IODNode("Specimen Identification",
					  IOD.SPECIMEN_IDENTIFICATION_MODULE));
	_ENHANCED_SR_NODE.add(new IODNode("General Study",
					  IOD.GENERAL_STUDY_MODULE));
	_ENHANCED_SR_NODE.add(new IODNode("Patient Study",
					  IOD.PATIENT_STUDY_MODULE));
	_ENHANCED_SR_NODE.add(new IODNode("SR Document Series",
					  IOD.SR_DOCUMENT_SERIES_MODULE));
	_ENHANCED_SR_NODE.add(new IODNode("General Equipment",
					  IOD.GENERAL_EQUIPMENT_MODULE));
	_ENHANCED_SR_NODE.add(new IODNode("SR Document General",
					  IOD.SR_DOCUMENT_GENERAL_MODULE));
	_ENHANCED_SR_NODE.add(new IODNode("SR Document Content",
					  IOD.SR_DOCUMENT_CONTENT_MODULE));
	_ENHANCED_SR_NODE.add(new IODNode("SOP Common",
					  IOD.COMMON_SOP_MODULE));
	_COMPREHENSIVE_SR_NODE
	    = new IODNode("Comprehensive SR",
			  IOD.accumulate(IOD.STRUCTURED_REPORT_DOCUMENT),
			  "1.2.840.10008.5.1.4.1.1.88.33");
	_COMPREHENSIVE_SR_NODE.add(new IODNode("Patient", IOD.PATIENT_MODULE));
	_COMPREHENSIVE_SR_NODE.add
	    (new IODNode("Specimen Identification",
			 IOD.SPECIMEN_IDENTIFICATION_MODULE));
	_COMPREHENSIVE_SR_NODE.add(new IODNode("General Study",
					       IOD.GENERAL_STUDY_MODULE));
	_COMPREHENSIVE_SR_NODE.add(new IODNode("Patient Study",
					       IOD.PATIENT_STUDY_MODULE));
	_COMPREHENSIVE_SR_NODE.add(new IODNode("SR Document Series",
					       IOD.SR_DOCUMENT_SERIES_MODULE));
	_COMPREHENSIVE_SR_NODE.add(new IODNode("General Equipment",
					       IOD.GENERAL_EQUIPMENT_MODULE));
	_COMPREHENSIVE_SR_NODE.add(new IODNode("SR Document General",
					       (IOD
						.SR_DOCUMENT_GENERAL_MODULE)));
	_COMPREHENSIVE_SR_NODE.add(new IODNode("SR Document Content",
					       (IOD
						.SR_DOCUMENT_CONTENT_MODULE)));
	_COMPREHENSIVE_SR_NODE.add(new IODNode("SOP Common",
					       IOD.COMMON_SOP_MODULE));
	_ROOT = new DefaultMutableTreeNode("IODs");
	_ROOT.add(new IODNode("File Meta Information", IOD.FILE_META_INFO));
	_ROOT.add(_CT_NODE);
	_ROOT.add(_MR_NODE);
	_ROOT.add(_NM_NODE);
	_ROOT.add(_BASIC_STUDY_DESCRIPTOR_NODE);
	_ROOT.add(_MWL_NODE);
	_ROOT.add(_MPPS_NODE);
	_ROOT.add(_PS_NODE);
	_ROOT.add(_BASIC_TEXT_SR_NODE);
	_ROOT.add(_ENHANCED_SR_NODE);
	_ROOT.add(_COMPREHENSIVE_SR_NODE);
    }
}
