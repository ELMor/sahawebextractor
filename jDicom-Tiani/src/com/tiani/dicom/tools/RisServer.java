/* RisServer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

import com.archimed.dicom.Debug;
import com.tiani.dicom.framework.Acceptor;
import com.tiani.dicom.framework.DefAcceptorListener;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.framework.SimpleAcceptancePolicy;
import com.tiani.dicom.tools.risserver.ModalityPPSCreateSCP;
import com.tiani.dicom.tools.risserver.ModalityPPSSetSCP;
import com.tiani.dicom.tools.risserver.ModalityWorklistSCP;
import com.tiani.dicom.tools.risserver.Repository;
import com.tiani.dicom.tools.risserver.StudyContentNotificationSCP;
import com.tiani.dicom.util.CheckParam;

public final class RisServer
{
    private Properties _params0;
    private File _paramFile = new File("RisServer.properties");
    private Repository _repository = new Repository();
    private Acceptor acceptor = null;
    private Thread thread = null;
    public static final String[] PARAM_NAMES
	= { "Port", "CalledTitle", "CallingTitles", "Verification",
	    "ModalityWorklist", "ModalityPPS", "StudyContentNotification",
	    "Repository.Path", "MultiThreadTCP", "MultiThreadAssoc", "QueueRQ",
	    "MaxInvoke", "MaxPduSize", "ARTIM[ms]", "Verbose",
	    "DumpCmdsetIntoDir", "DumpDatasetIntoDir" };
    private static String[] VERBOSE = { "0", "1", "2", "3", "4", "5" };
    public static Hashtable PARAM_CHECKS = new Hashtable();
    
    public static void main(String[] strings) {
	RisServer risserver = new RisServer();
	if (risserver.parseCmdLine(strings))
	    risserver.start();
    }
    
    public File getParamFile() {
	return _paramFile;
    }
    
    public Repository getRepository() {
	return _repository;
    }
    
    public boolean parseCmdLine(String[] strings) {
	switch (strings.length) {
	case 1:
	    _paramFile = new File(strings[0]);
	    /* fall through */
	case 0:
	    break;
	default:
	    System.out.println("illformed commandline");
	    System.out.println("usage: RisServer [<paramPath>]");
	    return false;
	}
	return loadParams();
    }
    
    private boolean verifyParams(Properties properties) {
	try {
	    CheckParam.verify(properties, PARAM_CHECKS);
	    return true;
	} catch (Exception exception) {
	    Debug.out.println(exception);
	    return false;
	}
    }
    
    private boolean loadParams() {
	Properties properties = new Properties();
	if (!_load(properties, _paramFile) || !verifyParams(properties)) {
	    try {
		properties.load(RisServer.class.getResourceAsStream
				("RisServer.properties"));
	    } catch (Exception exception) {
		Debug.out.println(exception);
		return false;
	    }
	}
	return init(new Properties[] { properties });
    }
    
    public boolean _load(Properties properties, File file) {
	try {
	    if (!file.exists())
		return false;
	    FileInputStream fileinputstream = new FileInputStream(file);
	    try {
		properties.load(fileinputstream);
	    } finally {
		fileinputstream.close();
	    }
	    Debug.out.println("Load Properties from " + file.getPath());
	} catch (IOException ioexception) {
	    Debug.out
		.println("Could not load Properties from " + file.getPath());
	    return false;
	}
	return true;
    }
    
    public boolean init(Properties[] propertieses) {
	if (!verifyParams(propertieses[0]))
	    return false;
	_params0 = propertieses[0];
	return true;
    }
    
    public boolean stop() {
	if (thread == null) {
	    Debug.out.println("server is not running");
	    return false;
	}
	try {
	    acceptor.stop(true);
	    Debug.out.println("Stopped RisServer v1.7.25 at "
			      + new GregorianCalendar().getTime());
	    return true;
	} catch (Exception exception) {
	    Debug.out.println(exception);
	} finally {
	    thread = null;
	    acceptor = null;
	}
	return false;
    }
    
    public Properties getParams() {
	return _params0;
    }
    
    public boolean loadParams(File file) {
	Properties properties = new Properties();
	if (!_load(properties, file))
	    return false;
	return init(new Properties[] { properties });
    }
    
    private int[] _asids() {
	int i = 0;
	int[] is = new int[4];
	if (getBooleanParam("Verification"))
	    is[i++] = 4097;
	if (getBooleanParam("ModalityWorklist"))
	    is[i++] = 4141;
	if (getBooleanParam("ModalityPPS"))
	    is[i++] = 4142;
	if (getBooleanParam("StudyContentNotification"))
	    is[i++] = 4099;
	int[] is_0_ = new int[i];
	System.arraycopy(is, 0, is_0_, 0, i);
	return is_0_;
    }
    
    private String[] _callingTitles() {
	StringTokenizer stringtokenizer
	    = new StringTokenizer(_params0.getProperty("CallingTitles"), "\\");
	int i = stringtokenizer.countTokens();
	if (i == 0)
	    return null;
	String[] strings = new String[i];
	for (int i_1_ = 0; i_1_ < i; i_1_++)
	    strings[i_1_] = stringtokenizer.nextToken();
	return strings;
    }
    
    public void start() {
	if (thread != null)
	    Debug.out.println("server is running");
	else if (verifyParams(_params0)) {
	    try {
		Debug.out.println("Start RisServer v1.3.1 at "
				  + new GregorianCalendar().getTime());
		for (int i = 0; i < PARAM_NAMES.length; i++)
		    Debug.out.println(PARAM_NAMES[i] + '='
				      + _params0.getProperty(PARAM_NAMES[i]));
		Debug.DEBUG = getIntParam("Verbose");
		String string = _params0.getProperty("DumpCmdsetIntoDir");
		Debug.dumpCmdsetIntoDir
		    = string != null && string.length() > 0 ? string : null;
		string = _params0.getProperty("DumpDatasetIntoDir");
		Debug.dumpDatasetIntoDir
		    = string != null && string.length() > 0 ? string : null;
		_repository
		    .init(new File(_params0.getProperty("Repository.Path")));
		String string_2_ = _params0.getProperty("CalledTitle");
		int i = getIntParam("MaxInvoke");
		int i_3_ = getIntParam("MaxPduSize");
		int i_4_ = getIntParam("ARTIM[ms]");
		DimseRqManager dimserqmanager = new DimseRqManager();
		dimserqmanager.regCEchoScp("1.2.840.10008.1.1",
					   new DefCEchoSCP());
		dimserqmanager.regCFindScp
		    ("1.2.840.10008.5.1.4.31",
		     new ModalityWorklistSCP(_repository));
		dimserqmanager.regNCreateScp
		    ("1.2.840.10008.3.1.2.3.3",
		     new ModalityPPSCreateSCP(_repository));
		dimserqmanager.regNSetScp("1.2.840.10008.3.1.2.3.3",
					  new ModalityPPSSetSCP(_repository));
		dimserqmanager.regCStoreScp
		    ("1.2.840.10008.1.9",
		     new StudyContentNotificationSCP(_repository));
		boolean bool = getBooleanParam("QueueRQ");
		SimpleAcceptancePolicy simpleacceptancepolicy
		    = new SimpleAcceptancePolicy((string_2_.length() != 0
						  ? string_2_ : null),
						 _callingTitles(), _asids(),
						 i_3_, bool ? i : 1, 1);
		DefAcceptorListener defacceptorlistener
		    = (new DefAcceptorListener
		       (getBooleanParam("MultiThreadAssoc"), dimserqmanager,
			bool, false));
		defacceptorlistener.setARTIM(i_4_);
		acceptor
		    = new Acceptor(new ServerSocket(getIntParam("Port")),
				   getBooleanParam("MultiThreadTCP"), _asids(),
				   bool ? i : 1, 1, defacceptorlistener);
		acceptor.setARTIM(i_4_);
		thread = new Thread(acceptor);
		thread.start();
		Debug.out.println("Waiting for invocations from clients...");
	    } catch (Exception exception) {
		Debug.out.println(exception);
	    }
	}
    }
    
    private boolean getBooleanParam(String string) {
	String string_5_ = _params0.getProperty(string);
	return (string_5_ != null
		&& "true".compareTo(string_5_.toLowerCase()) == 0);
    }
    
    private int getIntParam(String string) {
	return Integer.parseInt(_params0.getProperty(string));
    }
    
    static {
	PARAM_CHECKS.put("Port", CheckParam.range(100, 65535));
	PARAM_CHECKS.put("Verification", CheckParam.bool());
	PARAM_CHECKS.put("ModalityWorklist", CheckParam.bool());
	PARAM_CHECKS.put("ModalityPPS", CheckParam.bool());
	PARAM_CHECKS.put("StudyContentNotification", CheckParam.bool());
	PARAM_CHECKS.put("MultiThreadTCP", CheckParam.bool());
	PARAM_CHECKS.put("MultiThreadAssoc", CheckParam.bool());
	PARAM_CHECKS.put("QueueRQ", CheckParam.bool());
	PARAM_CHECKS.put("MaxInvoke", CheckParam.range(0, 65535));
	PARAM_CHECKS.put("MaxPduSize", CheckParam.range(0, 65535));
	PARAM_CHECKS.put("ARTIM[ms]", CheckParam.range(0, 65535));
	PARAM_CHECKS.put("Verbose", CheckParam.enum(VERBOSE));
    }
}
