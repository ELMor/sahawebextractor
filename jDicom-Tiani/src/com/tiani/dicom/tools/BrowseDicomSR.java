/* BrowseDicomSR - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.applet.AppletContext;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.Document;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.tools.sr.DicomObjectNode;
import com.tiani.dicom.tools.sr.XMLWriter;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.ExampleFileFilter;

public class BrowseDicomSR extends JApplet
{
    private JFrame _frame = null;
    private JFileChooser _fileChooser = null;
    private final FileFilter _XMLFileFilter
	= new ExampleFileFilter("xml", "XML");
    private JTextField _url = new JTextField();
    private JButton _chooseFileButton = new JButton("Choose File");
    private JButton _xmlButton = new JButton("XML");
    private JTree _tree = new JTree(new DefaultMutableTreeNode("No Data"));
    private DefaultTreeModel _model = (DefaultTreeModel) _tree.getModel();
    private DicomObjectTableModel _tableModel = new DicomObjectTableModel();
    private DicomObject _dicomObject = _tableModel.getDicomObject();
    private JSizeColumnsToFitTable _table
	= new JSizeColumnsToFitTable(_tableModel);
    private JTextArea _logTextArea = new JTextArea();
    private Document _logDoc = _logTextArea.getDocument();
    private PrintStream _log
	= new PrintStream(new DocumentOutputStream(_logDoc), true);
    
    public static void main(String[] strings) {
	BrowseDicomSR browsedicomsr = new BrowseDicomSR();
	browsedicomsr._frame = new AppletFrame("BrowseDicomSR v1.7.26",
					       browsedicomsr, 600, 500);
    }
    
    public void init() {
	_url.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		BrowseDicomSR.this.load(_url.getText());
	    }
	});
	_chooseFileButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File file
		    = BrowseDicomSR.this.chooseFile("Load from DICOM file",
						    "Load", null);
		if (file != null)
		    BrowseDicomSR.this.load(file);
	    }
	});
	_xmlButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File file
		    = BrowseDicomSR.this.chooseFile("Save as XML file", "Save",
						    _XMLFileFilter);
		if (file != null)
		    BrowseDicomSR.this.saveXML(file);
	    }
	});
	_tree.addTreeSelectionListener(new TreeSelectionListener() {
	    public void valueChanged(TreeSelectionEvent treeselectionevent) {
		TreePath treepath
		    = treeselectionevent.getNewLeadSelectionPath();
		if (treepath != null) {
		    DicomObjectNode dicomobjectnode
			= (DicomObjectNode) treepath.getLastPathComponent();
		    BrowseDicomSR.this
			.updateTable(dicomobjectnode.getDataset());
		}
	    }
	});
	_tree.setVisibleRowCount(10);
	_tree.putClientProperty("JTree.lineStyle", "Angled");
	AppletContext appletcontext = this.getAppletContext();
	if (appletcontext instanceof AppletFrame)
	    appletcontext = null;
	JPanel jpanel = new JPanel(new GridBagLayout());
	GridBagConstraints gridbagconstraints = new GridBagConstraints();
	jpanel.add(new JLabel("URL:"), gridbagconstraints);
	gridbagconstraints.fill = 2;
	gridbagconstraints.weightx = 100.0;
	jpanel.add(_url, gridbagconstraints);
	gridbagconstraints.fill = 0;
	gridbagconstraints.weightx = 0.0;
	jpanel.add(_chooseFileButton, gridbagconstraints);
	jpanel.add(_xmlButton, gridbagconstraints);
	jpanel.add(new JTianiButton(appletcontext), gridbagconstraints);
	_xmlButton.setEnabled(false);
	JSplitPane jsplitpane = new JSplitPane(0, new JScrollPane(_tree),
					       new JScrollPane(_table));
	jsplitpane.setOneTouchExpandable(true);
	JTabbedPane jtabbedpane = new JTabbedPane();
	jtabbedpane.add("DICOM", jsplitpane);
	jtabbedpane.add("XML", new JScrollPane(_logTextArea));
	Container container = this.getContentPane();
	container.add(jpanel, "North");
	container.add(jtabbedpane, "Center");
	String string = this.getParameter("AutoLoad");
	if (string != null && string.length() > 0) {
	    try {
		load(new URL(this.getDocumentBase(), string));
	    } catch (Exception exception) {
		prompt(exception);
	    }
	}
    }
    
    private File chooseFile(String string, String string_4_,
			    FileFilter filefilter) {
	try {
	    if (_fileChooser == null) {
		_fileChooser = new JFileChooser(".");
		_fileChooser.addChoosableFileFilter(_XMLFileFilter);
	    }
	    _fileChooser.setFileFilter(filefilter != null ? filefilter
				       : _fileChooser
					     .getAcceptAllFileFilter());
	    _fileChooser.setDialogTitle(string);
	    int i = _fileChooser.showDialog(_frame, string_4_);
	    File file = _fileChooser.getSelectedFile();
	    return i == 0 ? file : null;
	} catch (SecurityException securityexception) {
	    prompt(securityexception);
	    showPolicyFile();
	    return null;
	}
    }
    
    private void showPolicyFile() {
	try {
	    URL url = new URL(this.getDocumentBase(),
			      this.getParameter("PolicyFile"));
	    this.getAppletContext().showDocument(url, "_blank");
	} catch (Exception exception) {
	    prompt(exception);
	}
    }
    
    private URL toURL(String string) throws MalformedURLException {
	try {
	    return new URL(string);
	} catch (MalformedURLException malformedurlexception) {
	    try {
		return toURL(new File(string));
	    } catch (MalformedURLException malformedurlexception_5_) {
		throw malformedurlexception;
	    }
	}
    }
    
    private URL toURL(File file) throws MalformedURLException {
	String string = file.getAbsolutePath();
	if (File.separatorChar != '/')
	    string = string.replace(File.separatorChar, '/');
	if (!string.startsWith("/"))
	    string = "/" + string;
	if (!string.endsWith("/") && file.isDirectory())
	    string += "/";
	return new URL("file", "", string);
    }
    
    private void load(String string) {
	try {
	    load(toURL(string));
	} catch (Exception exception) {
	    prompt(exception);
	}
    }
    
    private void load(File file) {
	try {
	    load(toURL(file));
	} catch (Exception exception) {
	    prompt(exception);
	}
    }
    
    private void load(URL url) {
	try {
	    TianiInputStream tianiinputstream
		= new TianiInputStream(url.openStream());
	    DicomObject dicomobject = new DicomObject();
	    try {
		tianiinputstream.read(dicomobject, true);
	    } finally {
		tianiinputstream.close();
	    }
	    _url.setText(url.toString());
	    update(dicomobject);
	} catch (SecurityException securityexception) {
	    prompt(securityexception);
	    showPolicyFile();
	} catch (Throwable throwable) {
	    prompt(throwable);
	}
    }
    
    private void saveXML(File file) {
	try {
	    XMLWriter xmlwriter = new XMLWriter(new FileOutputStream(file));
	    try {
		xmlwriter.write(_dicomObject);
	    } finally {
		xmlwriter.close();
	    }
	} catch (Exception exception) {
	    prompt(exception);
	}
    }
    
    public void update(DicomObject dicomobject) {
	_dicomObject = dicomobject;
	updateTable(dicomobject);
	try {
	    _model.setRoot(new DicomObjectNode("1", _dicomObject, null));
	    _xmlButton.setEnabled(true);
	    _logTextArea.setText("");
	    XMLWriter xmlwriter = new XMLWriter(_log);
	    try {
		xmlwriter.write(_dicomObject);
	    } finally {
		xmlwriter.close();
	    }
	} catch (Exception exception) {
	    _xmlButton.setEnabled(false);
	    prompt(exception);
	}
    }
    
    private void updateTable(DicomObject dicomobject) {
	try {
	    _table.setModel(new DicomObjectTableModel(dicomobject));
	} catch (DicomException dicomexception) {
	    prompt(dicomexception);
	}
    }
    
    private void prompt(Object object) {
	JOptionPane.showMessageDialog(_frame, object, "BrowseDicomSR", 0);
    }
}
