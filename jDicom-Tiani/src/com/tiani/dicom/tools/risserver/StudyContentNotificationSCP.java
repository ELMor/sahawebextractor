/* StudyContentNotificationSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.risserver;
import java.io.File;
import java.io.FileOutputStream;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefCStoreSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.media.FileMetaInformation;

public class StudyContentNotificationSCP extends DefCStoreSCP
{
    private Repository _repository;
    private boolean _validate;
    
    public StudyContentNotificationSCP(Repository repository) {
	_repository = repository;
    }
    
    protected int store(DimseExchange dimseexchange, String string,
			String string_0_, DicomMessage dicommessage,
			DicomMessage dicommessage_1_) {
	try {
	    DicomObject dicomobject = dicommessage.getDataset();
	    dicomobject
		.setFileMetaInformation(new FileMetaInformation(dicomobject));
	    File file = _repository.createSCNFile(string_0_);
	    if (file.exists())
		System.out.println
		    ("WARNING: Received Basic Study Content Notification SOP instance ["
		     + string_0_ + "] overwrites existing one");
	    FileOutputStream fileoutputstream = new FileOutputStream(file);
	    try {
		dicomobject.write(fileoutputstream, true);
	    } finally {
		fileoutputstream.close();
	    }
	    if (Debug.DEBUG > 0)
		Debug.out.println
		    ("Store Basic Study Content Notification SOP instance to "
		     + file);
	} catch (Exception exception) {
	    System.out.println(exception);
	}
	return 3;
    }
}
