/* JTreePanel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.risserver;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.ModalityWorklist;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;

public final class JTreePanel extends JPanel
{
    private JButton _addButton = new JButton("+");
    private JButton _delButton = new JButton("-");
    private JButton _updateButton = new JButton("?");
    private JButton _loadButton = new JButton(">>");
    private JButton _saveButton = new JButton("<<");
    private DefaultMutableTreeNode _root
	= new DefaultMutableTreeNode("Repository");
    private DirNode _mwlNode
	= new DirNode("ModalityWorklist", IOD.MODALITY_WORKLIST_ITEM);
    private DirNode _mppsNode
	= new DirNode("ModalityPPS", IOD.MODALITY_WORKLIST_ITEM);
    private DirNode _scnNode
	= new DirNode("StudyContentNotification", IOD.BASIC_STUDY_DESCRIPTOR);
    private JTree _tree = new JTree(_root);
    private DefaultTreeModel _model = (DefaultTreeModel) _tree.getModel();
    private DefaultMutableTreeNode _selNode = null;
    private File _curFile = null;
    private Properties _params;
    private JSizeColumnsToFitTable _recordTable;
    private Repository _repository = null;
    private static final SimpleDateFormat _dateFormater
	= new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat _timeFormater
	= new SimpleDateFormat("HHmmss");
    private static final DicomObjectTableModel _NULL_MODEL
	= new DicomObjectTableModel();
    
    private static class DirNode extends DefaultMutableTreeNode
    {
	private String _prompt;
	
	public DirNode(String string, Tag[][] tags) {
	    super(IOD.accumulate(tags));
	    _prompt = string;
	}
	
	public String toString() {
	    return _prompt;
	}
    }
    
    private static class FileNode extends DefaultMutableTreeNode
    {
	public FileNode(File file) {
	    super((Object) file);
	}
	
	public String toString() {
	    return ((File) userObject).getName();
	}
    }
    
    public JTreePanel(Properties properties,
		      JSizeColumnsToFitTable jsizecolumnstofittable,
		      Repository repository) {
	super(new BorderLayout());
	_loadButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		JTreePanel.this.load();
		JTreePanel.this.addIOD();
	    }
	});
	_saveButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		JTreePanel.this.save();
	    }
	});
	_addButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		JTreePanel.this.addTo
		    ((DirNode)
		     (_selNode.getLevel() == 2
		      ? (javax.swing.tree.TreeNode) _selNode.getParent()
		      : _selNode));
	    }
	});
	_delButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		JTreePanel.this.del(_selNode);
		_selNode = null;
		_recordTable.setModel(JTreePanel._NULL_MODEL);
	    }
	});
	_updateButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		JTreePanel.this.update();
	    }
	});
	_tree.addTreeSelectionListener(new TreeSelectionListener() {
	    public void valueChanged(TreeSelectionEvent treeselectionevent) {
		TreePath treepath
		    = treeselectionevent.getNewLeadSelectionPath();
		_selNode = null;
		if (treepath != null) {
		    _selNode = ((DefaultMutableTreeNode)
				treepath.getLastPathComponent());
		    if (_selNode.getLevel() == 2)
			JTreePanel.this.load();
		}
		JTreePanel.this.enableButtons();
	    }
	});
	JPanel jpanel = new JPanel(new GridLayout(0, 3));
	jpanel.add(_updateButton);
	jpanel.add(_saveButton);
	jpanel.add(_loadButton);
	jpanel.add(_addButton);
	jpanel.add(_delButton);
	this.add(new JScrollPane(_tree));
	this.add(jpanel, "South");
	_tree.setVisibleRowCount(5);
	_root.add(_mwlNode);
	_root.add(_mppsNode);
	_root.add(_scnNode);
	_params = properties;
	_recordTable = jsizecolumnstofittable;
	_repository = repository;
	update();
	enableButtons();
    }
    
    public File getCurFile() {
	return _curFile;
    }
    
    private void enableButtons() {
	int i = _selNode == null ? -1 : _selNode.getLevel();
	_addButton.setEnabled(i > 0);
	_delButton.setEnabled(i >= 0);
	_saveButton.setEnabled(i > 1);
	_loadButton.setEnabled(i > 1);
    }
    
    private void save() {
	try {
	    DicomObjectTableModel dicomobjecttablemodel
		= (DicomObjectTableModel) _recordTable.getModel();
	    DicomObject dicomobject = dicomobjecttablemodel.getDicomObject();
	    save(dicomobject, _curFile = (File) _selNode.getUserObject());
	    _recordTable.setModel(new DicomObjectTableModel(dicomobject, false,
							    true, true));
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
    }
    
    private static void save(DicomObject dicomobject, File file)
	throws IOException, DicomException {
	FileOutputStream fileoutputstream = new FileOutputStream(file);
	try {
	    dicomobject.write(fileoutputstream,
			      dicomobject.getFileMetaInformation() != null);
	} finally {
	    fileoutputstream.close();
	}
    }
    
    private void load() {
	try {
	    _curFile = (File) _selNode.getUserObject();
	    FileInputStream fileinputstream = new FileInputStream(_curFile);
	    DicomObject dicomobject = new DicomObject();
	    try {
		dicomobject.read(fileinputstream);
	    } finally {
		fileinputstream.close();
	    }
	    DicomObjectTableModel dicomobjecttablemodel
		= new DicomObjectTableModel(dicomobject, false, true, true);
	    _recordTable.setModel(dicomobjecttablemodel);
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
    }
    
    private void del(DefaultMutableTreeNode defaultmutabletreenode) {
	if (defaultmutabletreenode.getLevel() == 2) {
	    File file = (File) defaultmutabletreenode.getUserObject();
	    if (!file.delete())
		Debug.out.println("ERROR: Could not delete - " + file);
	    else
		_model.removeNodeFromParent(defaultmutabletreenode);
	} else {
	    int i = defaultmutabletreenode.getChildCount();
	    while (i-- > 0)
		del((DefaultMutableTreeNode)
		    defaultmutabletreenode.getChildAt(i));
	}
    }
    
    private void addTo(DirNode dirnode) {
	try {
	    if (dirnode != _mppsNode) {
		DicomObject dicomobject = new DicomObject();
		File file = (dirnode == _mwlNode ? createMWLitem(dicomobject)
			     : createSCN(dicomobject));
		save(dicomobject, file);
		FileNode filenode = new FileNode(file);
		_model.insertNodeInto(filenode, dirnode,
				      dirnode.getChildCount());
		_tree.setSelectionPath(new TreePath(filenode.getPath()));
	    }
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
    }
    
    private File createMWLitem(DicomObject dicomobject)
	throws DicomException, IOException {
	String string = UIDUtils.createID(8);
	Date date = new Date();
	DicomObject dicomobject_6_
	    = ModalityWorklist.createSPS(string, _dateFormater.format(date),
					 _timeFormater.format(date), "OT",
					 "StationAET",
					 "ScheduledProcedureStepDescription");
	ModalityWorklist.initItem(dicomobject, "Patient^Name",
				  UIDUtils.createID(8), UIDUtils.createID(8),
				  UIDUtils.createUID(), UIDUtils.createID(8),
				  "RequestedProcedureDescription",
				  dicomobject_6_);
	return _repository.createMWLFile(string);
    }
    
    private File createSCN(DicomObject dicomobject)
	throws DicomException, IOException {
	String string = UIDUtils.createUID();
	dicomobject.set(62, "1.2.840.10008.1.9");
	dicomobject.set(63, string);
	dicomobject.set(147, "Patient^Name");
	dicomobject.set(148, UIDUtils.createID(8));
	dicomobject.set(427, UIDUtils.createID(8));
	dicomobject.set(425, UIDUtils.createUID());
	dicomobject.set(109, null);
	dicomobject
	    .setFileMetaInformation(new FileMetaInformation(dicomobject));
	return _repository.createSCNFile(string);
    }
    
    private void addIOD() {
	try {
	    DicomObjectTableModel dicomobjecttablemodel
		= (DicomObjectTableModel) _recordTable.getModel();
	    dicomobjecttablemodel.addTags((Tag[]) ((DefaultMutableTreeNode)
						   _selNode.getParent())
						      .getUserObject());
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
    }
    
    private void update() {
	try {
	    _repository.init(new File(_params.getProperty("Repository.Path")));
	    update(_mwlNode, _repository.listMWLFiles());
	    update(_mppsNode, _repository.listMPPSFiles());
	    update(_scnNode, _repository.listSCNFiles());
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
	_model.nodeStructureChanged(_mwlNode);
	_model.nodeStructureChanged(_mppsNode);
	_model.nodeStructureChanged(_scnNode);
    }
    
    private static void update(DefaultMutableTreeNode defaultmutabletreenode,
			       File[] files) {
	int i = defaultmutabletreenode.getChildCount();
	while (i-- > 0)
	    defaultmutabletreenode.remove(i);
	for (int i_7_ = 0; i_7_ < files.length; i_7_++)
	    defaultmutabletreenode.insert(new FileNode(files[i_7_]),
					  defaultmutabletreenode
					      .getChildCount());
    }
}
