/* Repository - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.risserver;
import java.io.File;
import java.io.IOException;

import com.archimed.dicom.Debug;

public final class Repository
{
    static final String MWL = "ModalityWorklist";
    static final String MPPS = "ModalityPPS";
    static final String SCN = "StudyContentNotification";
    private File _mwlDir = null;
    private File _mppsDir = null;
    private File _scnDir = null;
    
    public void init(File file) throws IOException {
	if (!file.exists() || !file.isDirectory() || !file.canWrite())
	    throw new IOException(file + " is not a writeable directory!");
	_mwlDir = new File(file, "ModalityWorklist");
	_mppsDir = new File(file, "ModalityPPS");
	_scnDir = new File(file, "StudyContentNotification");
    }
    
    public File[] listMWLFiles() {
	return listFiles(_mwlDir);
    }
    
    public File[] listMPPSFiles() {
	return listFiles(_mppsDir);
    }
    
    public File[] listSCNFiles() {
	return listFiles(_scnDir);
    }
    
    public File createMWLFile(String string) throws IOException {
	return createFile(_mwlDir, string);
    }
    
    public File createMPPSFile(String string) throws IOException {
	return createFile(_mppsDir, string);
    }
    
    public File createSCNFile(String string) throws IOException {
	return createFile(_scnDir, string);
    }
    
    private static File[] listFiles(File file) {
	if (!file.exists())
	    return new File[0];
	String[] strings = file.list();
	File[] files = new File[strings.length];
	for (int i = 0; i < strings.length; i++)
	    files[i] = new File(file, strings[i]);
	return files;
    }
    
    private File createFile(File file, String string) throws IOException {
	if (!file.exists()) {
	    if (!file.mkdir())
		throw new IOException("Creating " + file + " failed!");
	    if (Debug.DEBUG > 0)
		Debug.out.println(file + "created");
	}
	return new File(file, string);
    }
}
