/* ModalityWorklistSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.risserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefCFindSCP;
import com.tiani.dicom.framework.DefMultiResponse;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IMultiResponse;
import com.tiani.dicom.framework.SingleResponse;
import com.tiani.dicom.util.DOFFactory;
import com.tiani.dicom.util.DefDicomObjectFilter;
import com.tiani.dicom.util.IDicomObjectFilter;

public final class ModalityWorklistSCP extends DefCFindSCP
{
    private Repository _repository;
    
    public ModalityWorklistSCP(Repository repository) {
	_repository = repository;
    }
    
    protected IMultiResponse query(DimseExchange dimseexchange, int i,
				   String string, DicomMessage dicommessage) {
	try {
	    Vector vector = new Vector();
	    File[] files = _repository.listMWLFiles();
	    DicomObject dicomobject = dicommessage.getDataset();
	    IDicomObjectFilter idicomobjectfilter
		= DOFFactory.getMWLDOFFactory().getFilter(dicomobject);
	    for (int i_0_ = 0; i_0_ < files.length; i_0_++) {
		DicomObject dicomobject_1_;
		if ((dicomobject_1_ = query(files[i_0_], idicomobjectfilter))
		    != null)
		    vector.addElement
			(DefDicomObjectFilter.createReturn(dicomobject,
							   dicomobject_1_));
	    }
	    return new DefMultiResponse(vector.elements());
	} catch (Exception exception) {
	    Debug.out.println(exception);
	    return new SingleResponse(257);
	}
    }
    
    private DicomObject query
	(File file, IDicomObjectFilter idicomobjectfilter)
	throws IOException, DicomException {
	DicomObject dicomobject = new DicomObject();
	FileInputStream fileinputstream = new FileInputStream(file);
	try {
	    dicomobject.read(fileinputstream);
	} finally {
	    fileinputstream.close();
	}
	return idicomobjectfilter.accept(dicomobject) ? dicomobject : null;
    }
}
