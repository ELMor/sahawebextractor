/* TS - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.io.IOException;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;

public final class TS
{
    public static final int[] ID
	= { 8193, 8194, 8195, 8198, 8197, 8205, 8206, 8213, 8214, 8196, 8199,
	    8200, 8201, 8202, 8203, 8204, 8207, 8208, 8209, 8210, 8211, 8212 };
    public static final String[] SAVE
	= { "ImplicitVRLittleEndian", "ExplicitVRLittleEndian",
	    "ExplicitVRBigEndian", "JPEG_14_SelectionValue1" };
    public static final String[] LABEL
	= { "ImplicitVRLittleEndian", "ExplicitVRLittleEndian",
	    "ExplicitVRBigEndian", "RLELossless", "JPEG_14_SelectionValue1",
	    "JPEG_14", "JPEG_15", "JPEG_28", "JPEG_29", "JPEG_1", "JPEG_2_4",
	    "JPEG_3_5", "JPEG_6_8", "JPEG_7_9", "JPEG_10_12", "JPEG_11_13",
	    "JPEG_16_18", "JPEG_17_19", "JPEG_20_22", "JPEG_21_23",
	    "JPEG_24_26", "JPEG_25_27", "MG1.1" };
    public static final int IVR_LE = 0;
    public static final int EVR_LE = 1;
    public static final int EVR_BE = 2;
    public static final int MG1_1 = LABEL.length - 1;
    
    public static boolean isCompress(int i) {
	return i > 2;
    }
    
    public static int indexOf(Object object) {
	int i = LABEL.length;
	while (i-- > 0) {
	    if (LABEL[i].equals(object))
		return i;
	}
	return i;
    }
    
    public static int indexOf(int i) {
	int i_0_ = ID.length;
	while (i_0_-- > 0) {
	    if (ID[i_0_] == i)
		return i_0_;
	}
	return i_0_;
    }
    
    public static int indexOf(int i, DicomObject dicomobject)
	throws DicomException, UnknownUIDException {
	switch (i) {
	case 1:
	    return indexOf(UID.getUIDEntry
			       (dicomobject.getFileMetaInformation().getS(31))
			       .getConstant());
	case 2:
	    return 0;
	case 3:
	    return 1;
	case 4:
	    return 2;
	case 8:
	    return MG1_1;
	default:
	    return 0;
	}
    }
    
    private static int getPixelSize(DicomObject dicomobject) {
	int i = 0;
	int i_1_ = dicomobject.getSize(1184);
	while (i_1_-- > 0)
	    i += ((byte[]) dicomobject.get(1184, i_1_)).length;
	return i > 0 ? i : 1;
    }
    
    public static void changeCompression
	(DicomObject dicomobject, int i, int i_2_)
	throws IOException, DicomException {
	if (i != i_2_) {
	    Compression compression = new Compression(dicomobject);
	    if (isCompress(i)) {
		int i_3_ = getPixelSize(dicomobject);
		if (Debug.DEBUG > 0)
		    Debug.out.println("Start Decompression from " + LABEL[i]);
		compression.decompress();
		if (Debug.DEBUG > 0)
		    Debug.out.println("Finished Decompression 1 : "
				      + ((float) getPixelSize(dicomobject)
					 / (float) i_3_));
	    }
	    if (isCompress(i_2_)) {
		int i_4_ = getPixelSize(dicomobject);
		if (Debug.DEBUG > 0)
		    Debug.out.println("Start Compression to " + LABEL[i_2_]);
		compression.compress(ID[i_2_]);
		if (Debug.DEBUG > 0)
		    Debug.out.println("Finished Compression "
				      + ((float) i_4_
					 / (float) getPixelSize(dicomobject))
				      + " : 1");
	    }
	}
    }
}
