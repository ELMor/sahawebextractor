/* JTreePanel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.queryretrievescu;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;

public final class JTreePanel extends JPanel
{
    private static final DicomObjectTableModel _NULL_MODEL
	= new DicomObjectTableModel();
    private DefaultMutableTreeNode _root
	= new DefaultMutableTreeNode("Query Results");
    private JTree _tree = new JTree(_root);
    private DefaultTreeModel _model = (DefaultTreeModel) _tree.getModel();
    private DefaultMutableTreeNode _selNode = null;
    private JButton _delButton = new JButton("-");
    private JButton _clearButton = new JButton("C");
    private JFilterPanel _filter = null;
    private JButton _findButton = null;
    private JButton _moveButton = null;
    private JSizeColumnsToFitTable _recordTable = null;
    
    public JTreePanel(JButton jbutton, JButton jbutton_0_,
		      JSizeColumnsToFitTable jsizecolumnstofittable,
		      JFilterPanel jfilterpanel) {
	super(new BorderLayout());
	_findButton = jbutton;
	_moveButton = jbutton_0_;
	_recordTable = jsizecolumnstofittable;
	_filter = jfilterpanel;
	_delButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (_selNode == _root)
		    JTreePanel.this.clear();
		else
		    _model.removeNodeFromParent(_selNode);
		_selNode = null;
		_recordTable.setModel(JTreePanel._NULL_MODEL);
		enableButtons(0);
	    }
	});
	_clearButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		JTreePanel.this.clear();
		_selNode = null;
		_recordTable.setModel(JTreePanel._NULL_MODEL);
		enableButtons(0);
	    }
	});
	_tree.addTreeSelectionListener(new TreeSelectionListener() {
	    public void valueChanged(TreeSelectionEvent treeselectionevent) {
		TreePath treepath
		    = treeselectionevent.getNewLeadSelectionPath();
		DicomObjectTableModel dicomobjecttablemodel
		    = JTreePanel._NULL_MODEL;
		_selNode = null;
		int i = treepath != null ? treepath.getPathCount() - 1 : 0;
		if (i > 0) {
		    _selNode = ((DefaultMutableTreeNode)
				treepath.getLastPathComponent());
		    try {
			QueryResult queryresult
			    = (QueryResult) _selNode.getUserObject();
			dicomobjecttablemodel
			    = new DicomObjectTableModel(queryresult
							    .getDataset());
		    } catch (DicomException dicomexception) {
			/* empty */
		    }
		}
		_recordTable.setModel(dicomobjecttablemodel);
		enableButtons(i);
	    }
	});
	JPanel jpanel = new JPanel();
	jpanel.add(_delButton);
	jpanel.add(_clearButton);
	this.add(new JScrollPane(_tree), "Center");
	this.add(jpanel, "South");
    }
    
    public JTree getTree() {
	return _tree;
    }
    
    public DicomObject getFindIdentifer(int i) throws DicomException {
	Assert.isTrue(i == 4132 || i == 4135 || i == 4138);
	int i_4_ = _selNode != null ? _selNode.getLevel() : 0;
	if (i == 4138 && i_4_ > 1)
	    throw new DicomException
		      ("Cannot perform " + JFilterPanel.QR_LEVEL[i_4_]
		       + " level query using Patient Study Only Query Retrieve Information Model FIND SOP Class");
	return i == 4135 ? getStudyRootFindFilter(i_4_) : getFindFilter(i_4_);
    }
    
    private DicomObject getFindFilter(int i) throws DicomException {
	DicomObject dicomobject = new DicomObject();
	copyDicomObject(_filter.getFilter(i), dicomobject);
	dicomobject.set(78, JFilterPanel.QR_LEVEL[i]);
	if (i > 0)
	    addUniqueKeys(_selNode, dicomobject);
	return dicomobject;
    }
    
    private DicomObject getStudyRootFindFilter(int i) throws DicomException {
	if (i > 1) {
	    DicomObject dicomobject = getFindFilter(i);
	    dicomobject.deleteItem(148);
	    return dicomobject;
	}
	DicomObject dicomobject = new DicomObject();
	copyDicomObject(_filter.getFilter(0), dicomobject);
	copyDicomObject(_filter.getFilter(1), dicomobject);
	dicomobject.deleteItem(453);
	dicomobject.deleteItem(454);
	dicomobject.deleteItem(455);
	dicomobject.set(78, "STUDY");
	if (i > 0)
	    addUniqueKeys(_selNode, dicomobject);
	return dicomobject;
    }
    
    private void addUniqueKeys(DefaultMutableTreeNode defaultmutabletreenode,
			       DicomObject dicomobject) throws DicomException {
	DefaultMutableTreeNode defaultmutabletreenode_5_
	    = defaultmutabletreenode;
	int i = defaultmutabletreenode_5_.getLevel();
	while (i-- > 0) {
	    QueryResult queryresult
		= (QueryResult) defaultmutabletreenode_5_.getUserObject();
	    dicomobject.set(QueryResult.UNIQUE_KEYS[i],
			    queryresult.getUniqueKey());
	    defaultmutabletreenode_5_
		= ((DefaultMutableTreeNode)
		   defaultmutabletreenode_5_.getParent());
	}
    }
    
    public DicomObject getMoveIdentifer(int i) throws DicomException {
	Assert.isTrue(i == 4133 || i == 4136 || i == 4139);
	int i_6_ = _selNode.getLevel() - 1;
	if (i == 4139 && i_6_ > 1)
	    throw new DicomException
		      ("Cannot perform " + JFilterPanel.QR_LEVEL[i_6_]
		       + " level retrieve using Patient Study Only Query Retrieve Information Model MOVE SOP Class");
	if (i == 4136 && i_6_ == 0)
	    throw new DicomException
		      ("Cannot perform " + JFilterPanel.QR_LEVEL[i_6_]
		       + " level retrieve using Study Root Query Retrieve Information Model MOVE SOP Class");
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(78, JFilterPanel.QR_LEVEL[i_6_]);
	addUniqueKeys(_selNode, dicomobject);
	if (i == 4136)
	    dicomobject.deleteItem(148);
	return dicomobject;
    }
    
    private static void copyDicomObject
	(DicomObject dicomobject, DicomObject dicomobject_7_)
	throws DicomException {
	Enumeration enumeration = dicomobject.enumerateVRs(false, false);
	while (enumeration.hasMoreElements())
	    copyTagValue((TagValue) enumeration.nextElement(), dicomobject_7_);
    }
    
    private static void copyTagValue
	(TagValue tagvalue, DicomObject dicomobject) throws DicomException {
	int i = tagvalue.getGroup();
	int i_8_ = tagvalue.getElement();
	dicomobject.deleteItem_ge(i, i_8_);
	int i_9_ = tagvalue.size();
	if (i_9_ > 0) {
	    for (int i_10_ = 0; i_10_ < i_9_; i_10_++)
		dicomobject.append_ge(i, i_8_, tagvalue.getValue(i_10_));
	} else
	    dicomobject.set_ge(i, i_8_, null);
    }
    
    public void addQueryResult(DicomObject dicomobject) throws DicomException {
	if (dicomobject.getSize(78) <= 0)
	    throw new DicomException
		      ("Missing Attribute Query Retrieve Level in C-FIND-RSP");
	String string = dicomobject.getS(78);
	int i = JFilterPanel.levelToInt(string);
	if (i == -1)
	    throw new DicomException
		      (string
		       + "is not a valid value for Attribute Query Retrieve Level in C-FIND-RSP");
	if (i == 0)
	    addQueryResult(_root, new QueryResult(dicomobject, 0), true);
	else {
	    DefaultMutableTreeNode defaultmutabletreenode
		= getStudyNode(dicomobject, i == 1);
	    for (int i_11_ = 2; i_11_ <= i; i_11_++)
		defaultmutabletreenode
		    = addQueryResult(defaultmutabletreenode,
				     new QueryResult(dicomobject, i_11_),
				     i == i_11_);
	}
    }
    
    private DefaultMutableTreeNode getStudyNode
	(DicomObject dicomobject, boolean bool) throws DicomException {
	if (dicomobject.getSize(425) <= 0)
	    throw new DicomException
		      ("Missing Attribute Study Instance UID in C-FIND-RSP");
	QueryResult queryresult = new QueryResult(dicomobject, 1);
	int i = _root.getChildCount();
	for (int i_12_ = 0; i_12_ < i; i_12_++) {
	    DefaultMutableTreeNode defaultmutabletreenode
		= getChildNode(((DefaultMutableTreeNode)
				_root.getChildAt(i_12_)),
			       queryresult, bool);
	    if (defaultmutabletreenode != null)
		return defaultmutabletreenode;
	}
	DefaultMutableTreeNode defaultmutabletreenode
	    = addQueryResult(_root, new QueryResult(dicomobject, 0), false);
	return addQueryResult(defaultmutabletreenode, queryresult, bool);
    }
    
    private DefaultMutableTreeNode getChildNode
	(DefaultMutableTreeNode defaultmutabletreenode,
	 QueryResult queryresult, boolean bool) {
	int i = defaultmutabletreenode.getChildCount();
	for (int i_13_ = 0; i_13_ < i; i_13_++) {
	    DefaultMutableTreeNode defaultmutabletreenode_14_
		= ((DefaultMutableTreeNode)
		   defaultmutabletreenode.getChildAt(i_13_));
	    if (queryresult
		    .equals(defaultmutabletreenode_14_.getUserObject())) {
		if (bool) {
		    defaultmutabletreenode_14_.setUserObject(queryresult);
		    _model.nodeChanged(defaultmutabletreenode_14_);
		}
		return defaultmutabletreenode_14_;
	    }
	}
	return null;
    }
    
    private DefaultMutableTreeNode addQueryResult
	(DefaultMutableTreeNode defaultmutabletreenode,
	 QueryResult queryresult, boolean bool)
	throws DicomException {
	DefaultMutableTreeNode defaultmutabletreenode_15_
	    = getChildNode(defaultmutabletreenode, queryresult, bool);
	if (defaultmutabletreenode_15_ == null) {
	    defaultmutabletreenode_15_
		= new DefaultMutableTreeNode(queryresult);
	    _model.insertNodeInto(defaultmutabletreenode_15_,
				  defaultmutabletreenode,
				  defaultmutabletreenode.getChildCount());
	    _tree.expandPath(new TreePath(defaultmutabletreenode.getPath()));
	}
	return defaultmutabletreenode_15_;
    }
    
    private void clear() {
	_root.removeAllChildren();
	_model.nodeStructureChanged(_root);
    }
    
    public void enableButtons(int i) {
	_findButton.setEnabled(i < 4);
	_moveButton.setEnabled(i > 0);
	_delButton.setEnabled(i > 0);
    }
}
