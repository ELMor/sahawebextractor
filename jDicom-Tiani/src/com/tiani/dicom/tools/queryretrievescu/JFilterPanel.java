/* JFilterPanel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.queryretrievescu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.Tag;

public final class JFilterPanel extends JTabbedPane
{
    public static final String[] QR_LEVEL
	= { "PATIENT", "STUDY", "SERIES", "IMAGE" };
    private DicomObject[] _filter = new DicomObject[4];
    private static final int[] _PAT_INIT_KEYS
	= { 147, 148, 152, 150, 151, 154, 155, 170, 177, 453, 454, 455 };
    private static final int[] _STUDY_INIT_KEYS
	= { 64, 70, 77, 427, 425, 88, 95, 99, 101, 103, 157, 158, 159, 171,
	    173, 82, 456, 457 };
    private static final int[] _SERIES_INIT_KEYS = { 81, 428, 426, 458 };
    private static final int[] _IMAGE_INIT_KEYS = { 430, 63 };
    private static final int[][] _RECORD_INIT_KEYS
	= { _PAT_INIT_KEYS, _STUDY_INIT_KEYS, _SERIES_INIT_KEYS,
	    _IMAGE_INIT_KEYS };
    private static final Tag[] _PAT_TAGS
	= IOD.accumulate(new Tag[][] { IOD.PATIENT_MODULE,
				       { new Tag(32, 4608), new Tag(32, 4610),
					 new Tag(32, 4612) } });
    private static final Tag[] _STUDY_TAGS
	= IOD.accumulate(new Tag[][] { IOD.GENERAL_STUDY_MODULE,
				       IOD.PATIENT_STUDY_MODULE,
				       { new Tag(8, 97), new Tag(32, 4614),
					 new Tag(32, 4616) } });
    private static final Tag[] _SERIES_TAGS
	= IOD.accumulate(new Tag[][] { IOD.GENERAL_SERIES_MODULE,
				       { new Tag(32, 4617) } });
    private static final Tag[] _IMAGE_TAGS
	= IOD.accumulate(new Tag[][] { IOD.GENERAL_IMAGE_MODULE,
				       IOD.MULTIFRAME_MODULE,
				       IOD.IMAGE_PIXEL_MODULE,
				       IOD.PRESENTATION_STATE_MODULE,
				       IOD.SR_DOCUMENT_GENERAL_MODULE,
				       IOD.SR_DOCUMENT_CONTENT_MODULE });
    private static final Tag[][] _RECORD_TAGS
	= { _PAT_TAGS, _STUDY_TAGS, _SERIES_TAGS, _IMAGE_TAGS };
    
    public static final int levelToInt(String string) {
	int i = QR_LEVEL.length;
	while (--i >= 0) {
	    if (QR_LEVEL[i].equals(string))
		return i;
	}
	return -1;
    }
    
    public JFilterPanel() {
	try {
	    for (int i = 0; i < QR_LEVEL.length; i++) {
		_filter[i] = createFilter(i);
		DicomObjectTableModel dicomobjecttablemodel
		    = new DicomObjectTableModel(_filter[i], false, true, true);
		dicomobjecttablemodel.addTags(_RECORD_TAGS[i]);
		this.add(QR_LEVEL[i],
			 new JScrollPane(new JSizeColumnsToFitTable
					 (dicomobjecttablemodel)));
	    }
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.getMessage());
	}
    }
    
    public DicomObject getFilter(int i) {
	return _filter[i];
    }
    
    private static DicomObject createFilter(int i) throws DicomException {
	DicomObject dicomobject = new DicomObject();
	int[] is = _RECORD_INIT_KEYS[i];
	for (int i_0_ = 0; i_0_ < is.length; i_0_++)
	    dicomobject.set(is[i_0_], null);
	return dicomobject;
    }
}
