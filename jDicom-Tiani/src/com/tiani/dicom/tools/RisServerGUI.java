/* RisServerGUI - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.Document;

import com.tiani.dicom.tools.risserver.JTreePanel;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesPanel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.ExampleFileFilter;

public class RisServerGUI extends JFrame
{
    private JFileChooser _fileChooser = new JFileChooser(".");
    private final FileFilter _HTMLFileFilter
	= new ExampleFileFilter("html", "HTML");
    private static final DicomObjectTableModel _NULL_MODEL
	= new DicomObjectTableModel();
    private JSizeColumnsToFitTable _table
	= new JSizeColumnsToFitTable(_NULL_MODEL);
    private JTreePanel _treePanel;
    private JButton _startButton = new JButton("Start");
    private JButton _stopButton = new JButton("Stop");
    private JButton _saveHtmlButton = new JButton("HTML");
    private JTabbedPane tabbedPane = new JTabbedPane(2);
    private JTextArea _logTextArea = new JTextArea();
    private Document _logDoc = _logTextArea.getDocument();
    private PrintStream _log
	= new PrintStream(new DocumentOutputStream(_logDoc, 30000), true);
    private RisServer _server = new RisServer();
    
    public static void main(String[] strings) {
	RisServerGUI risservergui = new RisServerGUI(strings);
	risservergui.show();
    }
    
    public RisServerGUI(String[] strings) {
	this.setTitle("DICOM RisServer 1.7.26");
	Toolkit toolkit = Toolkit.getDefaultToolkit();
	Dimension dimension = toolkit.getScreenSize();
	this.setSize(700, 500);
	this.setLocation((dimension.width - 700) / 2,
			 (dimension.height - 500) / 2);
	this.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent windowevent) {
		System.exit(0);
	    }
	});
	_startButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		_server.init(new Properties[] { _server.getParams() });
		_server.start();
	    }
	});
	_stopButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		_server.stop();
	    }
	});
	_saveHtmlButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		StringBuffer stringbuffer = new StringBuffer("Save ");
		File file = _treePanel.getCurFile();
		if (file != null) {
		    stringbuffer.append(file.getName());
		    _fileChooser
			.setSelectedFile(new File(file.getName() + ".html"));
		}
		stringbuffer.append(" as HTML File");
		File file_4_
		    = RisServerGUI.this._chooseFile(stringbuffer.toString());
		if (file_4_ != null)
		    RisServerGUI.this._saveHtml(file_4_,
						(file != null ? file.getName()
						 : "unknown source"));
	    }
	});
	_fileChooser.setFileSelectionMode(0);
	_fileChooser.setFileFilter(_HTMLFileFilter);
	JPanel jpanel = new JPanel();
	jpanel.add(_startButton);
	jpanel.add(_stopButton);
	jpanel.add(_saveHtmlButton);
	JPanel jpanel_5_ = new JPanel(new BorderLayout());
	jpanel_5_.add(new JTianiButton((java.applet.AppletContext) null),
		      "East");
	jpanel_5_.add(jpanel, "Center");
	this.getContentPane().add(jpanel_5_, "North");
	System.setOut(_log);
	_server.parseCmdLine(strings);
	JPropertiesPanel jpropertiespanel
	    = new JPropertiesPanel(_server.getParamFile(),
				   "Properties for " + this.getTitle(),
				   _server.getParams(), RisServer.PARAM_NAMES,
				   RisServer.PARAM_CHECKS);
	_treePanel = new JTreePanel(_server.getParams(), _table,
				    _server.getRepository());
	tabbedPane.add("Props", new JScrollPane(jpropertiespanel));
	JSplitPane jsplitpane
	    = new JSplitPane(1, _treePanel, new JScrollPane(_table));
	tabbedPane.add("Result", jsplitpane);
	tabbedPane.add("Log", new JAutoScrollPane(_logTextArea));
	this.getContentPane().add(tabbedPane, "Center");
    }
    
    private File _chooseFile(String string) {
	_fileChooser.setDialogTitle(string);
	int i = _fileChooser.showSaveDialog(this);
	File file = _fileChooser.getSelectedFile();
	return i == 0 ? file : null;
    }
    
    private void _saveHtml(File file, String string) {
	try {
	    FileOutputStream fileoutputstream = new FileOutputStream(file);
	    PrintWriter printwriter = new PrintWriter(fileoutputstream);
	    try {
		_table.toHTML(printwriter, string);
	    } finally {
		printwriter.close();
	    }
	    _log.println("Save as HTML to " + file);
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
}
