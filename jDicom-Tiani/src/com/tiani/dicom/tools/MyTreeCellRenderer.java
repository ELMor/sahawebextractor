/* MyTreeCellRenderer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

class MyTreeCellRenderer extends DefaultTreeCellRenderer
{
    private static final Icon[] _icons
	= { new ImageIcon(MyTreeCellRenderer.class
			      .getResource("/com/tiani/dicom/icons/root.gif")),
	    new ImageIcon(MyTreeCellRenderer.class.getResource
			  ("/com/tiani/dicom/icons/patient.gif")),
	    new ImageIcon(MyTreeCellRenderer.class.getResource
			  ("/com/tiani/dicom/icons/study.gif")),
	    new ImageIcon(MyTreeCellRenderer.class.getResource
			  ("/com/tiani/dicom/icons/series.gif")),
	    new ImageIcon(MyTreeCellRenderer.class.getResource
			  ("/com/tiani/dicom/icons/image.gif")) };
    
    public Component getTreeCellRendererComponent
	(JTree jtree, Object object, boolean bool, boolean bool_0_,
	 boolean bool_1_, int i, boolean bool_2_) {
	super.getTreeCellRendererComponent(jtree, object, bool, bool_0_,
					   bool_1_, i, bool_2_);
	DefaultMutableTreeNode defaultmutabletreenode
	    = (DefaultMutableTreeNode) object;
	this.setIcon(_icons[defaultmutabletreenode.getLevel()]);
	return this;
    }
}
