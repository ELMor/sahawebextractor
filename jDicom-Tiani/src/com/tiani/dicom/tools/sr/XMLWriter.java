/* XMLWriter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.sr;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class XMLWriter extends BufferedWriter
{
    private static final String[] PNAME_ATTRB
	= { "last", "first", "middle", "prefix", "suffix" };
    
    public XMLWriter(OutputStream outputstream)
	throws UnsupportedEncodingException {
	super(new OutputStreamWriter(outputstream, "UTF8"));
    }
    
    public synchronized void write(DicomObject dicomobject)
	throws IOException, DicomException {
	this.write("<?xml version='1.0' encoding='UTF-8'?>");
	nl(0);
	this.write("<srDocument>");
	writeConceptName(dicomobject, "1", 1);
	writeValue(dicomobject, "1", 1);
	nl(0);
	this.write("</srDocument>");
    }
    
    private void writeConceptName(DicomObject dicomobject, String string,
				  int i) throws IOException, DicomException {
	if (dicomobject.getSize(1322) > 0) {
	    nl(i);
	    this.write("<concept>");
	    writeCodeAttribs((DicomObject) dicomobject.get(1322), i + 1);
	    int i_0_ = dicomobject.getSize(1451);
	    for (int i_1_ = 0; i_1_ < i_0_; i_1_++) {
		DicomObject dicomobject_2_
		    = (DicomObject) dicomobject.get(1451, i_1_);
		if (dicomobject_2_.getSize(1425) > 0
		    && dicomobject_2_.getS(1425).equals("HAS CONCEPT MOD"))
		    writeContent("HasConceptMod", dicomobject_2_,
				 string + '.' + (i_1_ + 1), i + 1);
	    }
	    nl(i);
	    this.write("</concept>");
	}
    }
    
    protected void writeCodeAttribs(DicomObject dicomobject, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<code");
	if (dicomobject.getSize(91) > 0)
	    writeEncodedAttrib("value", dicomobject.getS(91));
	if (dicomobject.getSize(92) > 0)
	    writeEncodedAttrib("scheme", dicomobject.getS(92));
	if (dicomobject.getSize(93) > 0)
	    writeEncodedAttrib("meaning", dicomobject.getS(93));
	this.write("/>");
    }
    
    protected void writeValue
	(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	if (dicomobject.getSize(1429) > 0) {
	    String string_3_ = dicomobject.getS(1429);
	    if (string_3_.equals("CONTAINER"))
		writeContainer(dicomobject, string, i);
	    else if (string_3_.equals("TEXT"))
		writeText(dicomobject, string, i);
	    else if (string_3_.equals("DATETIME"))
		writeDateTime(dicomobject, string, i);
	    else if (string_3_.equals("DATE"))
		writeDate(dicomobject, string, i);
	    else if (string_3_.equals("TIME"))
		writeTime(dicomobject, string, i);
	    else if (string_3_.equals("PNAME"))
		writePname(dicomobject, string, i);
	    else if (string_3_.equals("UIDREF"))
		writeUIDref(dicomobject, string, i);
	    else if (string_3_.equals("NUM"))
		writeNum(dicomobject, string, i);
	    else if (string_3_.equals("CODE"))
		writeCode(dicomobject, string, i);
	    else if (string_3_.equals("COMPOSITE"))
		writeComposite(dicomobject, string, i);
	    else if (string_3_.equals("IMAGE"))
		writeImage(dicomobject, string, i);
	    else if (string_3_.equals("WAVEFORM"))
		writeWaveform(dicomobject, string, i);
	    else if (string_3_.equals("SCOORD"))
		writeScoord(dicomobject, string, i);
	    else if (string_3_.equals("TCOORD"))
		writeTcoord(dicomobject, string, i);
	}
    }
    
    private void writeContainer
	(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Container");
	if (dicomobject.getSize(1430) > 0)
	    writeEncodedAttrib("Continuity", dicomobject.getS(1430));
	this.write(">");
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Container>");
    }
    
    private void writeEncodedAttrib(String string, String string_4_)
	throws IOException {
	this.write(' ');
	this.write(string);
	this.write("=\"");
	writeEncoded(string_4_);
	this.write('\"');
    }
    
    private void writeEncoded(String string) throws IOException {
	int i = string.length();
	for (int i_5_ = 0; i_5_ < i; i_5_++) {
	    char c = string.charAt(i_5_);
	    switch (c) {
	    case '&':
		this.write("&amp;");
		break;
	    case '<':
		this.write("&lt;");
		break;
	    case '>':
		this.write("&gt;");
		break;
	    case '\"':
		this.write("&quot;");
		break;
	    case '\'':
		this.write("&apos;");
		break;
	    default:
		this.write(c);
	    }
	}
    }
    
    private void writeText(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Text>");
	if (dicomobject.getSize(1327) > 0) {
	    nl(i + 1);
	    this.write("<body>");
	    writeEncoded(dicomobject.getS(1327));
	    this.write("</body>");
	}
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Text>");
    }
    
    private void writeDateTime
	(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<DateTime");
	if (dicomobject.getSize(1434) > 0)
	    writeDateTimeAttribs(dicomobject.getS(1434));
	this.write("/>");
    }
    
    private void writeDateTimeAttribs(String string) throws IOException {
	StringTokenizer stringtokenizer = new StringTokenizer(string, "&");
	String string_6_ = stringtokenizer.nextToken();
	int i = string_6_.length();
	if (i >= 4) {
	    writeEncodedAttrib("year", string_6_.substring(0, 4));
	    if (i >= 6) {
		writeEncodedAttrib("month", string_6_.substring(4, 6));
		if (i >= 8) {
		    writeEncodedAttrib("day", string_6_.substring(6, 8));
		    if (i >= 10) {
			writeEncodedAttrib("hour", string_6_.substring(8, 10));
			if (i >= 12) {
			    writeEncodedAttrib("min",
					       string_6_.substring(10, 12));
			    if (i >= 14) {
				writeEncodedAttrib("sec",
						   string_6_.substring(12,
								       14));
				if (i > 15)
				    writeEncodedAttrib("frac",
						       string_6_
							   .substring(15));
			    }
			}
		    }
		}
	    }
	}
    }
    
    private void writeDate(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Date>");
	if (dicomobject.getSize(1323) > 0) {
	    String string_7_ = dicomobject.getS(1323);
	    int i_8_ = string_7_.length();
	    if (i_8_ >= 4) {
		writeEncodedAttrib("year", string_7_.substring(0, 4));
		if (i_8_ >= 6) {
		    writeEncodedAttrib("month", string_7_.substring(4, 6));
		    if (i_8_ >= 8)
			writeEncodedAttrib("day", string_7_.substring(6, 8));
		}
	    }
	}
	this.write("/>");
    }
    
    private void writeTime(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Time>");
	if (dicomobject.getSize(1324) > 0) {
	    String string_9_ = dicomobject.getS(1324);
	    int i_10_ = string_9_.length();
	    if (i_10_ >= 2) {
		writeEncodedAttrib("hour", string_9_.substring(0, 2));
		if (i_10_ >= 4) {
		    writeEncodedAttrib("min", string_9_.substring(2, 4));
		    if (i_10_ >= 6) {
			writeEncodedAttrib("sec", string_9_.substring(4, 6));
			if (i_10_ > 7)
			    writeEncodedAttrib("frac", string_9_.substring(7));
		    }
		}
	    }
	}
	this.write("/>");
    }
    
    private void writePname(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Pname");
	if (dicomobject.getSize(1325) > 0) {
	    StringTokenizer stringtokenizer
		= new StringTokenizer(dicomobject.getS(1325), "^", true);
	    for (int i_11_ = 0;
		 stringtokenizer.hasMoreTokens() && i_11_ < PNAME_ATTRB.length;
		 i_11_++) {
		String string_12_ = stringtokenizer.nextToken();
		if (!string_12_.equals("^")) {
		    writeEncodedAttrib(PNAME_ATTRB[i_11_], string_12_);
		    if (!stringtokenizer.hasMoreTokens())
			break;
		    stringtokenizer.nextToken();
		}
	    }
	}
	this.write("/>");
    }
    
    private void writeUIDref(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<UIDref");
	if (dicomobject.getSize(1435) > 0)
	    writeEncodedAttrib("value", dicomobject.getS(1435));
	this.write("/>");
    }
    
    private void writeNum(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Num");
	if (dicomobject.getSize(1440) > 0) {
	    DicomObject dicomobject_13_ = (DicomObject) dicomobject.get(1440);
	    if (dicomobject_13_.getSize(1329) > 0)
		writeEncodedAttrib("value", dicomobject_13_.getS(1329));
	    this.write(">");
	    nl(i + 1);
	    this.write("<unit>");
	    if (dicomobject_13_.getSize(1321) > 0)
		writeCodeAttribs((DicomObject) dicomobject_13_.get(1321),
				 i + 2);
	    nl(i + 1);
	    this.write("</unit>");
	} else
	    this.write(">");
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Num>");
    }
    
    private void writeCode(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Code>");
	if (dicomobject.getSize(1328) > 0)
	    writeCodeAttribs((DicomObject) dicomobject.get(1328), i + 1);
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Code>");
    }
    
    private void writeComposite
	(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Composite>");
	int i_14_ = dicomobject.getSize(121);
	for (int i_15_ = 0; i_15_ < i_14_; i_15_++) {
	    nl(i + 1);
	    this.write("<refComposite");
	    writeSOPAttrib((DicomObject) dicomobject.get(121, i_15_));
	    this.write("/>");
	}
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Composite>");
    }
    
    private void writeSOPAttrib(DicomObject dicomobject)
	throws IOException, DicomException {
	if (dicomobject.getSize(115) > 0)
	    writeEncodedAttrib("classUID", dicomobject.getS(115));
	if (dicomobject.getSize(116) > 0)
	    writeEncodedAttrib("instanceUID", dicomobject.getS(116));
    }
    
    private void writeImage(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Image>");
	int i_16_ = dicomobject.getSize(121);
	for (int i_17_ = 0; i_17_ < i_16_; i_17_++) {
	    nl(i + 1);
	    this.write("<refImage");
	    DicomObject dicomobject_18_
		= (DicomObject) dicomobject.get(121, i_17_);
	    writeSOPAttrib(dicomobject_18_);
	    this.write(">");
	    int i_19_ = dicomobject_18_.getSize(117);
	    for (int i_20_ = 0; i_20_ < i_19_; i_20_++) {
		nl(i + 2);
		this.write("<frameNo");
		writeEncodedAttrib("value", dicomobject_18_.getS(117, i_20_));
		this.write("/>");
	    }
	    int i_21_ = dicomobject_18_.getSize(121);
	    for (int i_22_ = 0; i_22_ < i_21_; i_22_++) {
		nl(i + 2);
		this.write("<refPresentation");
		writeSOPAttrib((DicomObject) dicomobject_18_.get(121, i_22_));
		this.write("/>");
	    }
	    nl(i + 1);
	    this.write("</refImage>");
	}
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Image>");
    }
    
    private void writeWaveform
	(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Waveform>");
	int i_23_ = dicomobject.getSize(121);
	for (int i_24_ = 0; i_24_ < i_23_; i_24_++) {
	    nl(i + 1);
	    this.write("<refWaveform");
	    DicomObject dicomobject_25_
		= (DicomObject) dicomobject.get(121, i_24_);
	    writeSOPAttrib(dicomobject_25_);
	    this.write(">");
	    int i_26_ = dicomobject_25_.getSize_ge(64, 41136) >> 1;
	    for (int i_27_ = 0; i_27_ < i_26_; i_27_++) {
		nl(i + 2);
		this.write("<channel");
		writeEncodedAttrib("M", dicomobject_25_.getS_ge(64, 41136,
								i_27_ << 1));
		writeEncodedAttrib("C",
				   dicomobject_25_.getS_ge(64, 41136,
							   (i_27_ << 1) + 1));
		this.write("/>");
	    }
	    nl(i + 1);
	    this.write("</refWaveform>");
	}
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Waveform>");
    }
    
    private void writeScoord(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Scoord");
	if (dicomobject.getSize(1396) > 0)
	    writeEncodedAttrib("type", dicomobject.getS(1396));
	this.write(">");
	int i_28_ = dicomobject.getSize(1395) >> 1;
	for (int i_29_ = 0; i_29_ < i_28_; i_29_++) {
	    nl(i + 1);
	    this.write("<graphicData");
	    writeEncodedAttrib("column", dicomobject.getS(1395, i_29_ << 1));
	    writeEncodedAttrib("row",
			       dicomobject.getS(1395, (i_29_ << 1) + 1));
	    this.write("/>");
	}
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Scoord>");
    }
    
    private void writeTcoord(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<Tcoord");
	if (dicomobject.getSize(1436) > 0)
	    writeEncodedAttrib("type", dicomobject.getS(1436));
	this.write(">");
	int i_30_ = dicomobject.getSize(1437);
	for (int i_31_ = 0; i_31_ < i_30_; i_31_++) {
	    nl(i + 1);
	    this.write("<position");
	    writeEncodedAttrib("value", dicomobject.getS(1437, i_31_));
	    this.write("/>");
	}
	int i_32_ = dicomobject.getSize(1438);
	for (int i_33_ = 0; i_33_ < i_32_; i_33_++) {
	    nl(i + 1);
	    this.write("<offset");
	    writeEncodedAttrib("value", dicomobject.getS(1438, i_33_));
	    this.write("/>");
	}
	int i_34_ = dicomobject.getSize(1439);
	for (int i_35_ = 0; i_35_ < i_34_; i_35_++) {
	    nl(i + 1);
	    this.write("<DateTime");
	    writeDateTimeAttribs(dicomobject.getS(1439, i_35_));
	    this.write("/>");
	}
	writeRelations(dicomobject, string, i + 1);
	nl(i);
	this.write("</Tcoord>");
    }
    
    private void writeRelations
	(DicomObject dicomobject, String string, int i)
	throws IOException, DicomException {
	int i_36_ = dicomobject.getSize(1451);
	for (int i_37_ = 0; i_37_ < i_36_; i_37_++) {
	    String string_38_ = string + '.' + (i_37_ + 1);
	    DicomObject dicomobject_39_
		= (DicomObject) dicomobject.get(1451, i_37_);
	    if (dicomobject_39_.getSize(1425) > 0) {
		String string_40_ = dicomobject_39_.getS(1425);
		if (string_40_.equals("CONTAINS"))
		    writeContent("Contains", dicomobject_39_, string_38_, i);
		else if (string_40_.equals("HAS PROPERTIES"))
		    writeContent("HasProperties", dicomobject_39_, string_38_,
				 i);
		else if (string_40_.equals("HAS OBS CONTEXT"))
		    writeContent("HasObsContext", dicomobject_39_, string_38_,
				 i);
		else if (string_40_.equals("HAS ACQ CONTEXT"))
		    writeContent("HasAcqContext", dicomobject_39_, string_38_,
				 i);
		else if (string_40_.equals("INFERRED FROM"))
		    writeContent("InferredFrom", dicomobject_39_, string_38_,
				 i);
		else if (string_40_.equals("SELECTED FROM"))
		    writeContent("SelectedFrom", dicomobject_39_, string_38_,
				 i);
	    }
	}
    }
    
    private void writeContent
	(String string, DicomObject dicomobject, String string_41_, int i)
	throws IOException, DicomException {
	nl(i);
	this.write("<" + string + " id=\"" + string_41_ + "\">");
	int i_42_ = dicomobject.getSize(1458);
	if (i_42_ > 0) {
	    nl(i + 1);
	    this.write("<see id=\"1");
	    for (int i_43_ = 1; i_43_ < i_42_; i_43_++)
		this.write("." + dicomobject.getS(1458, i_43_));
	    this.write("\"/>");
	} else {
	    writeConceptName(dicomobject, string_41_, i + 1);
	    writeValue(dicomobject, string_41_, i + 1);
	}
	nl(i);
	this.write("</" + string + ">");
    }
    
    private void nl(int i) throws IOException {
	this.write('\n');
	for (int i_44_ = 0; i_44_ < i; i_44_++)
	    this.write("  ");
    }
}
