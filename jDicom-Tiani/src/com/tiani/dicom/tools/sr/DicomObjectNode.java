/* DicomObjectNode - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools.sr;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public final class DicomObjectNode implements TreeNode
{
    TreeNode _parent = null;
    String _id = null;
    DicomObject _dataset = null;
    int _childcount;
    
    public DicomObjectNode(String string, DicomObject dicomobject,
			   TreeNode treenode) {
	_id = string;
	_dataset = dicomobject;
	_parent = treenode;
	_childcount = _dataset.getSize(1451);
    }
    
    public DicomObject getDataset() {
	return _dataset;
    }
    
    public TreeNode getChildAt(int i) {
	return (i < _childcount
		? new DicomObjectNode(_id + '.' + (i + 1),
				      (DicomObject) _dataset.get(1451, i),
				      this)
		: null);
    }
    
    public int getIndex(TreeNode treenode) {
	DicomObject dicomobject = ((DicomObjectNode) treenode)._dataset;
	for (int i = 0; i < _childcount; i++) {
	    if (_dataset.get(1451, i) == dicomobject)
		return i;
	}
	return -1;
    }
    
    public int getChildCount() {
	return _childcount;
    }
    
    public TreeNode getParent() {
	return _parent;
    }
    
    public boolean getAllowsChildren() {
	return true;
    }
    
    public boolean isLeaf() {
	return _childcount <= 0;
    }
    
    public Enumeration children() {
	return new Enumeration() {
	    int childindex = 0;
	    
	    public boolean hasMoreElements() {
		return childindex < _childcount;
	    }
	    
	    public Object nextElement() {
		return getChildAt(childindex++);
	    }
	};
    }
    
    public String toString() {
	try {
	    StringBuffer stringbuffer = new StringBuffer();
	    stringbuffer.append('(');
	    stringbuffer.append(_id);
	    stringbuffer.append(')');
	    if (_dataset.getSize(1425) > 0)
		stringbuffer.append(_dataset.getS(1425));
	    stringbuffer.append(':');
	    int i = _dataset.getSize(1458);
	    if (i > 0) {
		stringbuffer.append("->(");
		stringbuffer.append(_dataset.getI(1458));
		for (int i_1_ = 1; i_1_ < i; i_1_++) {
		    stringbuffer.append('.');
		    stringbuffer.append(_dataset.getI(1458, i_1_));
		}
		stringbuffer.append(')');
		return stringbuffer.toString();
	    }
	    if (_dataset.getSize(1322) > 0) {
		DicomObject dicomobject = (DicomObject) _dataset.get(1322);
		if (dicomobject.getSize(93) > 0)
		    stringbuffer.append(dicomobject.getS(93));
	    }
	    if (_dataset.getSize(1429) > 0)
		appendValueTo(stringbuffer);
	    return stringbuffer.toString();
	} catch (Exception exception) {
	    return exception.toString();
	}
    }
    
    private void appendValueTo(StringBuffer stringbuffer)
	throws DicomException {
	String string = _dataset.getS(1429);
	stringbuffer.append('[');
	stringbuffer.append(string);
	stringbuffer.append("] = ");
	if (string.equals("CONTAINER")) {
	    if (_dataset.getSize(1430) > 0) {
		stringbuffer.append('{');
		stringbuffer.append(_dataset.getS(1430));
		stringbuffer.append('}');
	    }
	} else if (string.equals("TEXT")) {
	    if (_dataset.getSize(1327) > 0)
		stringbuffer.append(_dataset.getS(1327));
	} else if (string.equals("DATETIME")) {
	    if (_dataset.getSize(1434) > 0)
		stringbuffer.append(_dataset.getS(1434));
	} else if (string.equals("DATE")) {
	    if (_dataset.getSize(1323) > 0)
		stringbuffer.append(_dataset.getS(1323));
	} else if (string.equals("TIME")) {
	    if (_dataset.getSize(1324) > 0)
		stringbuffer.append(_dataset.getS(1324));
	} else if (string.equals("PNAME")) {
	    if (_dataset.getSize(1325) > 0)
		stringbuffer.append(_dataset.getS(1325));
	} else if (string.equals("UIDREF")) {
	    if (_dataset.getSize(1435) > 0)
		stringbuffer.append(_dataset.getS(1435));
	} else if (string.equals("NUM")) {
	    int i = _dataset.getSize(1440);
	    if (i > 0) {
		appendNumValueTo(stringbuffer,
				 (DicomObject) _dataset.get(1440));
		for (int i_2_ = 1; i_2_ < i; i_2_++) {
		    stringbuffer.append('\\');
		    appendNumValueTo(stringbuffer,
				     (DicomObject) _dataset.get(1440, i_2_));
		}
	    }
	} else if (string.equals("CODE")) {
	    if (_dataset.getSize(1328) > 0) {
		DicomObject dicomobject = (DicomObject) _dataset.get(1328);
		if (dicomobject.getSize(93) > 0)
		    stringbuffer.append(dicomobject.getS(93));
	    }
	} else if (string.equals("COMPOSITE") || string.equals("IMAGE")
		   || string.equals("WAVEFORM")) {
	    stringbuffer.append(_dataset.getSize(121));
	    stringbuffer.append(" Referenced SOP items");
	} else if (string.equals("SCOORD")) {
	    if (_dataset.getSize(1396) > 0) {
		stringbuffer.append('[');
		stringbuffer.append(_dataset.getS(1396));
		stringbuffer.append(']');
	    }
	} else if (string.equals("TCOORD") && _dataset.getSize(1436) > 0) {
	    stringbuffer.append('[');
	    stringbuffer.append(_dataset.getS(1436));
	    stringbuffer.append(']');
	}
    }
    
    private void appendNumValueTo
	(StringBuffer stringbuffer, DicomObject dicomobject)
	throws DicomException {
	if (dicomobject.getSize(1329) > 0)
	    stringbuffer.append(dicomobject.getS(1329));
	if (dicomobject.getSize(1321) > 0) {
	    DicomObject dicomobject_3_ = (DicomObject) dicomobject.get(1321);
	    if (dicomobject_3_.getSize(93) > 0) {
		stringbuffer.append(' ');
		stringbuffer.append(dicomobject_3_.getS(93));
	    }
	}
    }
}
