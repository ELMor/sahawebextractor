/* Echo - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.io.IOException;
import java.net.Socket;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

public class Echo
{
    public static final int CECHORQ = 48;
    public static final int CECHORSP = 32816;
    public static final int NODATASET = 257;
    private static Echo _instance = new Echo();
    private Socket _sock = null;
    private Association _assoc = null;
    private Request _request = new Request();
    private Response _response = null;
    private int _messageID = 0;
    
    public static void main(String[] strings) {
	if (strings.length != 4)
	    System.out
		.println("Usage: Echo <calledAET> <callingAET> <host> <port>");
	else {
	    boolean bool = false;
	    try {
		_instance.open(strings[0], strings[1], strings[2],
			       Integer.parseInt(strings[3]));
		if (_instance._response instanceof Acknowledge) {
		    System.out.println("Association accepted");
		    bool = true;
		    _instance.send();
		    _instance.release();
		    System.out.println("Association released");
		    bool = false;
		} else
		    System.out.println("Association not accepted: "
				       + _instance._response);
	    } catch (Exception exception) {
		if (bool) {
		    try {
			_instance._assoc.sendAbort(0, 0);
		    } catch (Exception exception_0_) {
			/* empty */
		    }
		}
		System.out.println(exception);
	    }
	}
    }
    
    public void open
	(String string, String string_1_, String string_2_, int i)
	throws Exception {
	_request.setCalledTitle(string);
	_request.setCallingTitle(string_1_);
	int[] is = { 8193 };
	_request.addPresentationContext(4097, is);
	_sock = new Socket(string_2_, i);
	_assoc
	    = new Association(_sock.getInputStream(), _sock.getOutputStream());
	_assoc.sendAssociateRequest(_request);
	_response = _assoc.receiveAssociateResponse();
    }
    
    public void send() throws IllegalValueException, UnknownUIDException,
			      DicomException, IOException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(3, new Integer(48));
	dicomobject.set(4, new Integer(++_messageID));
	dicomobject.set(8, new Integer(257));
	dicomobject.set(1, "1.2.840.10008.1.1");
	System.out.println("Send C-ECHO-RQ");
	_assoc.send(4097, dicomobject, null);
	DicomObject dicomobject_3_ = _assoc.receiveCommand();
	if (dicomobject_3_.getI(3) != 32816)
	    System.out
		.println("ERROR: Received Dicom Message is not a C-ECHO-RSP");
	else {
	    int i;
	    if ((i = dicomobject_3_.getI(9)) != 0)
		System.out
		    .println("ERROR: Received C-ECHO-RSP with Status - " + i);
	    else
		System.out.println("Received C-ECHO-RSP");
	}
    }
    
    public void release() throws Exception {
	_assoc.sendReleaseRequest();
	_assoc.receiveReleaseResponse();
	_sock.close();
    }
}
