/* EditDicomObject - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.tools;
import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.Document;
import javax.swing.tree.DefaultMutableTreeNode;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.ExampleFileFilter;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;

public class EditDicomObject extends JApplet
{
    static final String[] PARAM_NAMES
	= { "Show.FileMetaInfo", "Show.SequenceItem", "Load.Append",
	    "Load.PixelData", "Save.FileMetaInfo", "Save.TransferSyntax",
	    "Save.UndefSeqLen", "Verbose" };
    static Hashtable PARAM_CHECKS = new Hashtable();
    private Properties _params = null;
    private final FileFilter _HTMLFileFilter;
    private JFrame _frame;
    private JFileChooser _fileChooser;
    private int _curTSindex;
    private File _curFile;
    private JButton _newButton;
    private JButton _loadButton;
    private JButton _saveButton;
    private JButton _htmlButton;
    private DicomObjectTableModel _tableModel;
    private DicomObject _dicomObject;
    private JSizeColumnsToFitTable _table;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;
    
    private final class JSelectIODPanel extends JPanel
    {
	private DefaultMutableTreeNode _root = IODNode.root();
	private JTree _tree = new JTree(_root);
	private JButton _removeButton = new JButton("<<");
	private JButton _addButton = new JButton(">>");
	
	public JSelectIODPanel() {
	    super(new BorderLayout());
	    _addButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent actionevent) {
		    if (EditDicomObject.this.verifyParams()) {
			Object object = _tree.getLastSelectedPathComponent();
			if (object != null && object != _root) {
			    IODNode iodnode = (IODNode) object;
			    Tag[] tags = (Tag[]) iodnode.getUserObject();
			    try {
				if (iodnode.isFileMetaInfo()) {
				    DicomObject dicomobject
					= _dicomObject
					      .getFileMetaInformation();
				    if (dicomobject == null) {
					FileMetaInformation filemetainformation
					    = (new FileMetaInformation
					       (_dicomObject,
						UID.getUIDEntry
						    (TS.ID
						     [(TS.indexOf
						       (_params.getProperty
							("Save.TransferSyntax")))])
						    .getValue()));
					_dicomObject.setFileMetaInformation
					    (filemetainformation);
				    }
				    _tableModel.addFileMetaInfoTags(tags);
				} else if (JSelectIODPanel.this
					       .checkSOPClassUID
					   (iodnode.getSOPClassUID()))
				    _tableModel.addTags(tags);
				else
				    _log.println("SOPClassUID mismatch");
			    } catch (Exception exception) {
				_log.println(exception);
			    }
			}
		    }
		}
	    });
	    _removeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent actionevent) {
		    EditDicomObject.this.updateTable();
		}
	    });
	    this.add(new JScrollPane(_tree), "Center");
	    JPanel jpanel = new JPanel();
	    jpanel.add(_removeButton);
	    jpanel.add(_addButton);
	    this.add(jpanel, "South");
	    _tree.setVisibleRowCount(10);
	    _tree.putClientProperty("JTree.lineStyle", "Angled");
	}
	
	private boolean checkSOPClassUID(String string) throws DicomException {
	    if (string == null)
		return true;
	    String string_2_ = _dicomObject.getS(62);
	    if (string_2_ != null && string_2_.length() != 0)
		return string_2_.equals(string);
	    _dicomObject.set(62, string);
	    String string_3_ = _dicomObject.getS(63);
	    if (string_3_ == null || string_3_.length() == 0)
		_dicomObject.set(63, UIDUtils.createUID());
	    return true;
	}
    }
    
    public static void main(String[] strings) {
	final String propSpec
	    = strings.length > 0 ? strings[0] : "EditDicomObject.properties";
	Properties properties = null;
	try {
	    properties = loadProperties(new FileInputStream(propSpec));
	} catch (Exception exception) {
	    /* empty */
	}
	final EditDicomObject instance = new EditDicomObject(properties);
	instance
	    ._frame = new AppletFrame("EditDicomObject v1.7.26", instance, 700, 500, new WindowAdapter() {
	    public void windowClosing(WindowEvent windowevent) {
		try {
		    instance.storeParams(propSpec);
		} catch (Exception exception) {
		    /* empty */
		}
		System.exit(0);
	    }
	});
    }
    
    public EditDicomObject() {
	_HTMLFileFilter = new ExampleFileFilter("html", "HTML");
	_frame = null;
	_fileChooser = null;
	_curTSindex = -1;
	_curFile = null;
	_newButton = new JButton("New");
	_loadButton = new JButton("Load");
	_saveButton = new JButton("Save");
	_htmlButton = new JButton("HTML");
	_tableModel = new DicomObjectTableModel();
	_dicomObject = _tableModel.getDicomObject();
	_table = new JSizeColumnsToFitTable(_tableModel);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 30000), true);
    }
    
    protected EditDicomObject(Properties properties) {
	_HTMLFileFilter = new ExampleFileFilter("html", "HTML");
	_frame = null;
	_fileChooser = null;
	_curTSindex = -1;
	_curFile = null;
	_newButton = new JButton("New");
	_loadButton = new JButton("Load");
	_saveButton = new JButton("Save");
	_htmlButton = new JButton("HTML");
	_tableModel = new DicomObjectTableModel();
	_dicomObject = _tableModel.getDicomObject();
	_table = new JSizeColumnsToFitTable(_tableModel);
	_logTextArea = new JTextArea();
	_logDoc = _logTextArea.getDocument();
	_log = new PrintStream(new DocumentOutputStream(_logDoc, 30000), true);
	_params = properties;
    }
    
    public void init() {
	Debug.out = _log;
	_newButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (EditDicomObject.this.verifyParams()) {
		    _dicomObject = new DicomObject();
		    _curTSindex = -1;
		    EditDicomObject.this.updateTable();
		}
	    }
	});
	_loadButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (EditDicomObject.this.verifyParams()) {
		    File file
			= EditDicomObject.this.chooseFile("Load DICOM File",
							  "Load", false);
		    if (file != null)
			EditDicomObject.this.load(file);
		}
	    }
	});
	_saveButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (EditDicomObject.this.verifyParams()) {
		    if (_curTSindex == TS.MG1_1)
			_log.println
			    ("Cannot store file with with MG1.1 compressed Pixeldata!");
		    else {
			int i = (TS.indexOf
				 (_params.getProperty("Save.TransferSyntax")));
			boolean bool
			    = EditDicomObject.this
				  .getBooleanParam("Save.FileMetaInfo");
			if (!bool && i != 0)
			    _log.println
				("Cannot store file without File Meta Information with Transfersyntax "
				 + TS.LABEL[i]);
			else {
			    File file
				= (EditDicomObject.this.chooseFile
				   ("Save as DICOM File", "Save", false));
			    if (file != null)
				EditDicomObject.this.save(file, bool, i);
			}
		    }
		}
	    }
	});
	_htmlButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (_curFile != null)
		    _fileChooser.setSelectedFile(new File(_curFile.getParent(),
							  (_curFile.getName()
							   + ".html")));
		File file
		    = EditDicomObject.this.chooseFile("Save as HTML file",
						      "Save", true);
		if (file != null)
		    EditDicomObject.this.saveHtml(file, (_curFile != null
							 ? _curFile.getName()
							 : "unknown source"));
	    }
	});
	AppletContext appletcontext = this.getAppletContext();
	if (appletcontext instanceof AppletFrame)
	    appletcontext = null;
	Box box = Box.createHorizontalBox();
	box.add(_newButton);
	box.add(_loadButton);
	box.add(_saveButton);
	box.add(_htmlButton);
	box.add(Box.createGlue());
	box.add(new JTianiButton(appletcontext));
	if (appletcontext != null)
	    getAppletParams();
	if (_params == null || !verifyParams()) {
	    try {
		_params
		    = loadProperties(EditDicomObject.class.getResourceAsStream
				     ("EditDicomObject.properties"));
	    } catch (IOException ioexception) {
		Assert.fail
		    ("Failed to load EditDicomObject.properties ressource");
	    }
	}
	JPropertiesTable jpropertiestable
	    = new JPropertiesTable(PARAM_NAMES, _params, PARAM_CHECKS);
	JSplitPane jsplitpane
	    = new JSplitPane(0, new JScrollPane(jpropertiestable),
			     new JSelectIODPanel());
	jsplitpane.setOneTouchExpandable(true);
	JSplitPane jsplitpane_8_
	    = new JSplitPane(1, jsplitpane, new JScrollPane(_table));
	jsplitpane_8_.setOneTouchExpandable(true);
	JSplitPane jsplitpane_9_
	    = new JSplitPane(0, jsplitpane_8_,
			     new JAutoScrollPane(_logTextArea));
	jsplitpane_9_.setOneTouchExpandable(true);
	Container container = this.getContentPane();
	container.add(box, "North");
	container.add(jsplitpane_9_, "Center");
	updateTable();
    }
    
    static Properties loadProperties(InputStream inputstream)
	throws IOException {
	Properties properties = new Properties();
	try {
	    properties.load(inputstream);
	} finally {
	    inputstream.close();
	}
	return properties;
    }
    
    private void getAppletParams() {
	_params = new Properties();
	for (int i = 0; i < PARAM_NAMES.length; i++) {
	    String string;
	    if ((string = this.getParameter(PARAM_NAMES[i])) != null)
		_params.put(PARAM_NAMES[i], string);
	}
    }
    
    private void storeParams(String string) throws IOException {
	CheckParam.verify(_params, PARAM_CHECKS);
	FileOutputStream fileoutputstream = new FileOutputStream(string);
	try {
	    _params.store(fileoutputstream, "Properties for EditDicomObject");
	} finally {
	    fileoutputstream.close();
	}
    }
    
    private File chooseFile(String string, String string_10_, boolean bool) {
	try {
	    if (_fileChooser == null) {
		_fileChooser = new JFileChooser(".");
		_fileChooser.addChoosableFileFilter(_HTMLFileFilter);
	    }
	    _fileChooser.setFileFilter(bool ? _HTMLFileFilter
				       : _fileChooser
					     .getAcceptAllFileFilter());
	    _fileChooser.setDialogTitle(string);
	    int i = _fileChooser.showDialog(_frame, string_10_);
	    File file = _fileChooser.getSelectedFile();
	    return i == 0 ? file : null;
	} catch (SecurityException securityexception) {
	    _log.println(securityexception);
	    showPolicyFile();
	    return null;
	}
    }
    
    private void showPolicyFile() {
	try {
	    URL url = new URL(this.getDocumentBase(),
			      this.getParameter("PolicyFile"));
	    this.getAppletContext().showDocument(url, "_blank");
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private void updateTable() {
	try {
	    _tableModel = (new DicomObjectTableModel
			   (_dicomObject, getBooleanParam("Show.FileMetaInfo"),
			    getBooleanParam("Show.SequenceItem"), true));
	    _table.setModel(_tableModel);
	} catch (DicomException dicomexception) {
	    _log.println(dicomexception);
	}
    }
    
    private boolean verifyParams() {
	try {
	    CheckParam.verify(_params, PARAM_CHECKS);
	    Debug.DEBUG = getIntParam("Verbose");
	    return true;
	} catch (Exception exception) {
	    _log.println(exception);
	    return false;
	}
    }
    
    private boolean getBooleanParam(String string) {
	String string_11_ = _params.getProperty(string);
	return (string_11_ != null
		&& "true".compareTo(string_11_.toLowerCase()) == 0);
    }
    
    private int getIntParam(String string) {
	return Integer.parseInt(_params.getProperty(string));
    }
    
    private void load(File file) {
	if (!getBooleanParam("Load.Append"))
	    _dicomObject = new DicomObject();
	try {
	    TianiInputStream tianiinputstream
		= new TianiInputStream(new FileInputStream(file));
	    try {
		tianiinputstream.read(_dicomObject,
				      getBooleanParam("Load.PixelData"));
	    } finally {
		tianiinputstream.close();
	    }
	    _curFile = file;
	    _curTSindex
		= TS.indexOf(tianiinputstream.getFormat(), _dicomObject);
	    _log.println("Load from " + file + '['
			 + tianiinputstream.getFormatName() + '-'
			 + TS.LABEL[_curTSindex] + ']');
	    int i = _dicomObject.getPixelDataLength();
	    if (i > 0) {
		_log.println("Pixel data length = " + Integer.toHexString(i)
			     + "H");
		_log.println("RxC = "
			     + Integer.toHexString(_dicomObject.getI(466)
						   * _dicomObject.getI(467))
			     + "H" + ", bits allocated = "
			     + _dicomObject.getI(475));
	    }
	    if (Debug.DEBUG > 2)
		_dicomObject.dumpVRs(_log, true);
	} catch (Exception exception) {
	    exception.printStackTrace(_log);
	} finally {
	    updateTable();
	}
    }
    
    private void save(File file, boolean bool, int i) {
	try {
	    TS.changeCompression(_dicomObject, _curTSindex, i);
	    if (getBooleanParam("Save.FileMetaInfo"))
		ajustFileMetaInfo(_curTSindex = i);
	    FileOutputStream fileoutputstream = new FileOutputStream(file);
	    try {
		_dicomObject.write(fileoutputstream, bool, TS.ID[i],
				   getBooleanParam("Save.UndefSeqLen"));
	    } finally {
		fileoutputstream.close();
	    }
	    _curFile = file;
	    _log.println("Save to " + file + '[' + TS.LABEL[_curTSindex]
			 + ']');
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private void saveHtml(File file, String string) {
	try {
	    FileOutputStream fileoutputstream = new FileOutputStream(file);
	    PrintWriter printwriter = new PrintWriter(fileoutputstream);
	    try {
		_table.toHTML(printwriter, string);
	    } finally {
		printwriter.close();
	    }
	    _log.println("Save as HTML to " + file);
	} catch (Exception exception) {
	    _log.println(exception);
	}
    }
    
    private void ajustFileMetaInfo(int i)
	throws DicomException, IllegalValueException {
	String string = UID.getUIDEntry(TS.ID[i]).getValue();
	DicomObject dicomobject = _dicomObject.getFileMetaInformation();
	if (dicomobject == null) {
	    dicomobject = new FileMetaInformation(_dicomObject, string);
	    dicomobject.set(34, "EditDicomObject");
	    _dicomObject.setFileMetaInformation(dicomobject);
	} else {
	    dicomobject.set(31, string);
	    String string_12_ = _dicomObject.getS(62);
	    if (string_12_ != null && string_12_.length() != 0)
		dicomobject.set(29, string_12_);
	    String string_13_ = _dicomObject.getS(63);
	    if (string_13_ != null && string_13_.length() != 0)
		dicomobject.set(30, string_13_);
	}
	updateTable();
	FileMetaInformation.check(dicomobject);
    }
    
    static {
	PARAM_CHECKS.put("Show.FileMetaInfo", CheckParam.bool());
	PARAM_CHECKS.put("Show.SequenceItem", CheckParam.bool());
	PARAM_CHECKS.put("Load.PixelData", CheckParam.bool());
	PARAM_CHECKS.put("Load.Append", CheckParam.bool());
	PARAM_CHECKS.put("Save.FileMetaInfo", CheckParam.bool());
	PARAM_CHECKS.put("Save.TransferSyntax", CheckParam.enum(TS.SAVE));
	PARAM_CHECKS.put("Save.UndefSeqLen", CheckParam.bool());
	PARAM_CHECKS.put("Verbose",
			 CheckParam.enum(new String[] { "0", "1", "2", "3",
							"4", "5" }));
    }
}
