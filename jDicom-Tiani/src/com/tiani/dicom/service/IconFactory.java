/* IconFactory - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.sun.image.codec.jpeg.ImageFormatException;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.util.LUT;
import com.tiani.dicom.util.LUTFactory;
import com.tiani.dicom.util.PixelMatrix;
import com.tiani.dicom.util.UIDUtils;

public class IconFactory
{
    public static final int COLOR_TO_GRAY = 0;
    public static final int COLOR_AS_ORIGINAL = 1;
    public static final int COLOR_TO_PALETTE = 2;
    private int maxRows = -1;
    private int maxColumns = -1;
    private int convertColor = 1;
    private boolean jpeg = false;
    private float jpegQuality = 0.5F;
    private boolean jpegMultiframe = false;
    private static final float MM_PER_INCH = 25.4F;
    
    public IconFactory() {
	/* empty */
    }
    
    public IconFactory(int i, int i_0_) {
	maxRows = i;
	maxColumns = i_0_;
    }
    
    public void setMaxRows(int i) {
	maxRows = i;
    }
    
    public int getMaxRows() {
	return maxRows;
    }
    
    public void setMaxColumns(int i) {
	maxColumns = i;
    }
    
    public int getMaxColumns() {
	return maxColumns;
    }
    
    public final int getColorConvert() {
	return convertColor;
    }
    
    public final boolean isJpeg() {
	return jpeg;
    }
    
    public final float getJpegQuality() {
	return jpegQuality;
    }
    
    public final boolean isJpegMultiframe() {
	return jpegMultiframe;
    }
    
    public void setColorConvert(int i) {
	convertColor = i;
    }
    
    public void setJpeg(boolean bool) {
	jpeg = bool;
    }
    
    public void setJpegQuality(float f) {
	jpegQuality = f;
    }
    
    public void setJpegMultiframe(boolean bool) {
	jpegMultiframe = bool;
    }
    
    public PixelMatrix createIconMatrix
	(PixelMatrix pixelmatrix, LUT.Byte1 byte1)
	throws DicomException, IOException {
	if (maxRows > 0 && maxColumns > 0) {
	    PixelMatrix.Dimension dimension
		= (pixelmatrix.adjustRescaleDimension
		   (Math.min(maxRows, pixelmatrix.getRows()),
		    Math.min(maxColumns, pixelmatrix.getColumns())));
	    if (dimension.columns != pixelmatrix.getColumns()
		|| dimension.rows != pixelmatrix.getRows())
		pixelmatrix
		    = (jpeg && jpegMultiframe
		       ? pixelmatrix.rescale(dimension.rows, dimension.columns,
					     null)
		       : (pixelmatrix.rescaleFrame
			  (pixelmatrix.getRepresentativeFrameNumber() - 1,
			   dimension.rows, dimension.columns, null)));
	}
	if (pixelmatrix.isMonochrome())
	    return pixelmatrix.preformatGrayscale(byte1, null);
	switch (convertColor) {
	case 1:
	    return pixelmatrix;
	case 0:
	    return pixelmatrix.convertToGrayscale(null);
	case 2:
	    return pixelmatrix.convertToPaletteColor(null);
	default:
	    throw new RuntimeException();
	}
    }
    
    public DicomObject createIconDicomObject(DicomObject dicomobject)
	throws DicomException, IOException {
	PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
	LUT.Byte1 byte1
	    = (pixelmatrix.isMonochrome()
	       ? LUTFactory.createByteLUT(pixelmatrix, dicomobject) : null);
	return createIconDicomObject(pixelmatrix, byte1);
    }
    
    public DicomObject createIconDicomObject
	(PixelMatrix pixelmatrix, LUT.Byte1 byte1)
	throws DicomException, IOException {
	DicomObject dicomobject = new DicomObject();
	setIconDicomObject(dicomobject, pixelmatrix, byte1);
	return dicomobject;
    }
    
    public void createJpegDicomObject(DicomObject dicomobject)
	throws DicomException, IOException {
	PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
	LUT.Byte1 byte1
	    = (pixelmatrix.isMonochrome()
	       ? LUTFactory.createByteLUT(pixelmatrix, dicomobject) : null);
	createJpegDicomObject(dicomobject, pixelmatrix, byte1);
    }
    
    public void createJpegDicomObject
	(DicomObject dicomobject, PixelMatrix pixelmatrix, LUT.Byte1 byte1)
	throws DicomException, IOException {
	if (!jpeg)
	    throw new IllegalStateException("setJpeg(true)");
	float f = (float) ((byte[]) dicomobject.get(1184)).length;
	int i = pixelmatrix.getRows();
	int i_1_ = pixelmatrix.getColumns();
	setIconDicomObject(dicomobject, pixelmatrix, byte1);
	dicomobject.set(58, "DERIVED", 0);
	dicomobject.set
	    (122,
	     (i > dicomobject.getI(466) || i_1_ > dicomobject.getI(467)
	      ? "Reduced pixel resolution, 8-bit rendering, JPEG lossy compression"
	      : "8-bit rendering, JPEG lossy compression"));
	DicomObject dicomobject_2_ = new DicomObject();
	dicomobject_2_.set(115, dicomobject.get(62));
	dicomobject_2_.set(116, dicomobject.get(63));
	dicomobject.set(123, dicomobject_2_);
	dicomobject.set(62, "1.2.840.10008.5.1.4.1.1.7");
	String string = UIDUtils.createUID();
	dicomobject.set(63, string);
	dicomobject.set(83, "WKS");
	dicomobject.set(504, "01");
	dicomobject.set(1313,
			new Float(f / (float) calcPixelDataSize(dicomobject)));
	dicomobject.deleteItem(490);
	dicomobject.deleteItem(489);
	dicomobject.deleteItem(491);
	dicomobject.deleteItem(505);
	dicomobject.deleteItem(487);
	dicomobject.deleteItem(488);
	dicomobject.deleteItem(492);
	dicomobject.deleteItem(510);
	dicomobject.deleteItem(470);
	dicomobject.deleteItem(308);
	dicomobject.deleteItem(473);
	dicomobject.setFileMetaInformation
	    (new FileMetaInformation("1.2.840.10008.5.1.4.1.1.7", string,
				     "1.2.840.10008.1.2.4.50"));
    }
    
    private static int calcPixelDataSize(DicomObject dicomobject) {
	int i = 0;
	int i_3_ = dicomobject.getSize(1184);
	for (int i_4_ = 0; i_4_ < i_3_; i_4_++)
	    i += ((byte[]) dicomobject.get(1184, i_4_)).length;
	return i;
    }
    
    void setIconDicomObject
	(DicomObject dicomobject, PixelMatrix pixelmatrix, LUT.Byte1 byte1)
	throws DicomException, IOException {
	pixelmatrix = createIconMatrix(pixelmatrix, byte1);
	dicomobject.set(461, new Integer(1));
	dicomobject.set(462, pixelmatrix.getPhotometricInterpretation());
	dicomobject.set(466, new Integer(pixelmatrix.getRows()));
	dicomobject.set(467, new Integer(pixelmatrix.getColumns()));
	dicomobject.set(475, new Integer(8));
	dicomobject.set(476, new Integer(8));
	dicomobject.set(477, new Integer(7));
	dicomobject.set(478, new Integer(0));
	if (!jpeg)
	    dicomobject.set(1184, pixelmatrix.getPixelData());
	else {
	    int i = pixelmatrix.getNumberOfFrames();
	    if (i > 1)
		dicomobject.set(464, new Integer(i));
	    dicomobject.set(1184, new byte[0], 0);
	    ByteArrayOutputStream bytearrayoutputstream
		= new ByteArrayOutputStream();
	    for (int i_5_ = 0; i_5_ < i; i_5_++) {
		writeJpeg(bytearrayoutputstream, pixelmatrix, i_5_,
			  jpegQuality);
		if ((bytearrayoutputstream.size() & 0x1) != 0)
		    bytearrayoutputstream.write(0);
		dicomobject.set(1184, bytearrayoutputstream.toByteArray(),
				i_5_ + 1);
		bytearrayoutputstream.reset();
	    }
	}
    }
    
    public static void storeJpeg
	(File file, PixelMatrix pixelmatrix, int i, float f)
	throws IOException, ImageFormatException {
	if (Debug.DEBUG > 1)
	    Debug.out.println("jdicom: store jpeg to " + file);
	FileOutputStream fileoutputstream = new FileOutputStream(file);
	try {
	    writeJpeg(fileoutputstream, pixelmatrix, i, f);
	} finally {
	    fileoutputstream.close();
	}
    }
    
    public static void writeJpeg
	(OutputStream outputstream, PixelMatrix pixelmatrix, int i, float f)
	throws IOException, ImageFormatException {
	BufferedImage bufferedimage = pixelmatrix.createBufferedImage(i);
	JPEGImageEncoder jpegimageencoder
	    = JPEGCodec.createJPEGEncoder(outputstream);
	if (pixelmatrix.isPaletteColor())
	    jpegimageencoder.encode(bufferedimage);
	else {
	    JPEGEncodeParam jpegencodeparam
		= JPEGCodec.getDefaultJPEGEncodeParam(bufferedimage);
	    jpegencodeparam.setQuality(f, false);
	    float[] fs = pixelmatrix.getPixelSpacing();
	    if (fs != null) {
		jpegencodeparam.setDensityUnit(1);
		jpegencodeparam.setXDensity((int) (25.4F / fs[1]));
		jpegencodeparam.setYDensity((int) (25.4F / fs[0]));
	    } else {
		jpegencodeparam.setDensityUnit(0);
		jpegencodeparam.setXDensity(pixelmatrix.getAspectRatio()[0]);
		jpegencodeparam.setYDensity(pixelmatrix.getAspectRatio()[1]);
	    }
	    jpegimageencoder.encode(bufferedimage, jpegencodeparam);
	}
    }
}
