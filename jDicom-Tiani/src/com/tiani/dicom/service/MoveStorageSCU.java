/* MoveStorageSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.IMultiResponse;
import com.tiani.dicom.framework.Status;

public class MoveStorageSCU implements IMultiResponse
{
    private final IStrategy strategy;
    private final String moveOriginatorAET;
    private final int moveOriginatorMsgID;
    private final String[] instanceUIDs;
    private final int[] asids;
    private final StorageCmtSCU scu = new StorageCmtSCU();
    private final Vector failedSOPInstanceUIDList = new Vector();
    private final Vector moveRspListener = new Vector();
    private int index = 0;
    private boolean canceled = false;
    
    public static interface IStrategy
    {
	public int load(DicomObject dicomobject, String string)
	    throws UnknownUIDException, DicomException, IOException;
    }
    
    public MoveStorageSCU(IStrategy istrategy, String string, int i,
			  String[] strings) {
	this(istrategy, string, i, strings, null);
    }
    
    public MoveStorageSCU(IStrategy istrategy, String string, int i,
			  String[] strings, int[] is) {
	if (is != null && is.length != strings.length)
	    throw new IllegalArgumentException
		      ("Number of instanceUIDs is not equal number of asids");
	strategy = istrategy;
	moveOriginatorAET = string;
	moveOriginatorMsgID = i;
	instanceUIDs = strings;
	asids = is;
    }
    
    public void addMoveRspListener(IDimseRspListener idimsersplistener) {
	if (moveRspListener.indexOf(idimsersplistener) == -1)
	    moveRspListener.addElement(idimsersplistener);
    }
    
    public void removeMoveRspListener(IDimseRspListener idimsersplistener) {
	moveRspListener.removeElement(idimsersplistener);
    }
    
    public void setARTIM(int i) {
	scu.setARTIM(i);
    }
    
    public void setMaxNumOpInvoked(int i) {
	scu.setMaxNumOpInvoked(i);
    }
    
    public void setMaxPduSize(int i) {
	scu.setMaxPduSize(i);
    }
    
    public void setPriority(int i) {
	scu.setPriority(i);
    }
    
    public Response connect
	(String string, int i, String string_0_, String string_1_, int[] is)
	throws UnknownHostException, IOException, UnknownUIDException,
	       IllegalValueException, DicomException {
	return scu.connect(string, i, string_0_, string_1_, is);
    }
    
    public Response connect(String string, int i, Request request)
	throws UnknownHostException, IOException, UnknownUIDException,
	       IllegalValueException, DicomException {
	return scu.connect(string, i, request);
    }
    
    public boolean isConnected() {
	return scu.isConnected();
    }
    
    public boolean isEnabled(int i) {
	return scu.isEnabled(i);
    }
    
    public UIDEntry[] listAcceptedTransferSyntaxes(int i)
	throws IllegalValueException {
	return scu.listAcceptedTransferSyntaxes(i);
    }
    
    public int nextResponse(DimseExchange dimseexchange,
			    DicomMessage dicommessage) {
	if (Debug.DEBUG > 1)
	    Debug.out.println("jdicom: Enter MoveStorageSCU.nextResponse");
	int i = nextRsp(dimseexchange, dicommessage);
	try {
	    dicommessage.set(9, new Integer(i));
	} catch (DicomException dicomexception) {
	    throw new RuntimeException(dicomexception.toString());
	}
	int i_2_ = ((Integer) dicommessage.get(5)).intValue();
	int i_3_ = moveRspListener.size();
	for (int i_4_ = 0; i_4_ < i_3_; i_4_++) {
	    try {
		((IDimseRspListener) moveRspListener.elementAt(i_4_))
		    .handleRSP(dimseexchange, i_2_, i, dicommessage);
	    } catch (Exception exception) {
		exception.printStackTrace(Debug.out);
	    }
	}
	if (Debug.DEBUG > 1)
	    Debug.out.println("jdicom: Leave MoveStorageSCU.nextResponse with "
			      + Status.toString(i));
	return i;
    }
    
    private int nextRsp(DimseExchange dimseexchange,
			DicomMessage dicommessage) {
	try {
	    boolean bool = scu.isConnected();
	    if (Debug.DEBUG > 2)
		Debug.out.println
		    ("jdicom: Enter MoveStorageSCU.nextRsp, isConnected="
		     + bool);
	    if (bool) {
		if (canceled || index >= instanceUIDs.length) {
		    if (Debug.DEBUG > 2)
			Debug.out
			    .println("jdicom: Enter StorageCmtSCU.release");
		    scu.release();
		    if (Debug.DEBUG > 2)
			Debug.out
			    .println("jdicom: Leave StorageCmtSCU.release");
		} else {
		    sendNext();
		    index++;
		}
	    }
	    int i = scu.getNumFailed() + failedSOPInstanceUIDList.size();
	    int i_5_ = scu.getNumCompleted();
	    int i_6_ = scu.getNumWarning();
	    bool = scu.isConnected();
	    if (!bool) {
		if (Debug.DEBUG > 2)
		    Debug.out.println("jdicom: Create result C-MOVE-RSP");
		completeFailedSOPInstanceUIDList();
		if ((i = failedSOPInstanceUIDList.size()) > 0)
		    dicommessage.setDataset(createRspIdentifer());
		if (Debug.DEBUG > 2)
		    Debug.out.println("jdicom: Result C-MOVE-RSP created");
	    }
	    int i_7_ = instanceUIDs.length - i - i_5_ - i_6_;
	    if (i_7_ > 0)
		dicommessage.set(19, new Integer(i_7_));
	    dicommessage.set(20, new Integer(i_5_));
	    dicommessage.set(22, new Integer(i_6_));
	    dicommessage.set(21, new Integer(i));
	    if (Debug.DEBUG > 1)
		Debug.out.println
		    ("jdicom: Leave MoveStorageSCU.nextRsp, isConnected="
		     + bool);
	    return (bool ? 65280 : canceled ? 65024
		    : index < instanceUIDs.length ? 42754 : i > 0 ? 45056 : 0);
	} catch (Exception exception) {
	    exception.printStackTrace(Debug.out);
	    return 49152;
	}
    }
    
    public void cancel() {
	canceled = true;
    }
    
    private void sendNext() {
	DicomObject dicomobject = new DicomObject();
	String string = instanceUIDs[index];
	do {
	    if (asids == null || isEnabled(asids[index])) {
		try {
		    if (Debug.DEBUG > 1)
			Debug.out.println
			    ("jdicom: Enter MoveStorageSCU.IStrategy.load with Instance UID "
			     + string);
		    strategy.load(dicomobject, string);
		    if (Debug.DEBUG > 1)
			Debug.out.println
			    ("jdicom: Leave MoveStorageSCU.IStrategy.load");
		    DicomObject dicomobject_8_
			= dicomobject.getFileMetaInformation();
		    if (dicomobject_8_ != null) {
			String string_9_ = dicomobject_8_.getS(31);
			switch (UID.getUIDEntry(string_9_).getConstant()) {
			case 8193:
			case 8194:
			case 8195:
			    break;
			default: {
			    Compression compression
				= new Compression(dicomobject);
			    compression.decompress();
			    if (Debug.DEBUG > 1)
				Debug.out.println
				    ("Decompressed image data before send");
			}
			}
		    }
		    scu.storeAsync(dicomobject, 8193, moveOriginatorAET,
				   moveOriginatorMsgID);
		    return;
		} catch (Exception exception) {
		    Debug.out.println("jdicom: " + exception);
		    break;
		}
	    }
	    if (Debug.DEBUG > 0) {
		String string_10_;
		try {
		    string_10_ = UID.getUIDEntry(asids[index]).getName();
		} catch (IllegalValueException illegalvalueexception) {
		    string_10_ = illegalvalueexception.getMessage();
		}
		Debug.out.println("jdicom: Skip storage of instance " + string
				  + " (" + string_10_
				  + " was rejected by SCP)");
	    }
	} while (false);
	failedSOPInstanceUIDList.addElement(string);
    }
    
    private void completeFailedSOPInstanceUIDList() {
	DicomObject dicomobject = scu.getStoreResult();
	int i = dicomobject.getSize(120);
	for (int i_11_ = 0; i_11_ < i; i_11_++) {
	    DicomObject dicomobject_12_
		= (DicomObject) dicomobject.get(120, i_11_);
	    failedSOPInstanceUIDList
		.addElement((String) dicomobject_12_.get(116));
	}
	if (!canceled) {
	    for (int i_13_ = index; i_13_ < instanceUIDs.length; i_13_++)
		failedSOPInstanceUIDList.addElement(instanceUIDs[i_13_]);
	}
    }
    
    private DicomObject createRspIdentifer() throws DicomException {
	DicomObject dicomobject = new DicomObject();
	int i = failedSOPInstanceUIDList.size();
	for (int i_14_ = 0; i_14_ < i; i_14_++)
	    dicomobject.append(80, failedSOPInstanceUIDList.elementAt(i_14_));
	return dicomobject;
    }
}
