/* SendImage - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Reject;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.util.CheckParam;

public class SendImage
{
    public static final int OK_STATUS = 0;
    public static final int ERR_URL_MALFORMED = -1;
    public static final int ERR_SOCK_IO = -10;
    public static final int ERR_SOCK_UNKNOWNHOST = -11;
    public static final int ERR_ASSOC_IO = -20;
    public static final int ERR_ASSOC_ISOPEN = -21;
    public static final int ERR_ASSOC_ISCLOSED = -22;
    public static final int ERR_ASSOC_ILLEGALVALUE = -23;
    public static final int ERR_ASSOC_UNKNOWNUID = -24;
    public static final int ERR_IMAGE_IO = -30;
    public static final int ERR_IMAGE_OUTOFMEMORY = -31;
    public static final int ERR_IMAGE_DICOM = -32;
    public static final int ERR_IMAGE_UNKNOWNUID = -33;
    public static final int ERR_SEND_IO = -40;
    public static final int ERR_SEND_ILLEGALVALUE = -41;
    public static final int ERR_SEND_DICOM = -42;
    public static final int ERR_RECEIVE_IO = -50;
    public static final int ERR_RECEIVE_ILLEGALVALUE = -51;
    public static final int ERR_RECEIVE_DICOM = -52;
    public static final int ERR_RECEIVE_UNKNOWNUID = -53;
    public static final int ERR_RECEIVE_WRONGMSGID = -54;
    public static final int ERR_FILE_IO = -60;
    public static final int DEL_NONE = 0;
    public static final int DEL_STUDY = 1;
    public static final int DEL_IMAGE = 2;
    public static final int ON_ERROR_CONTINUE = 0;
    public static final int ON_ERROR_SKIP_STUDY = 1;
    public static final int ON_ERROR_EXIT = 2;
    public static final int ON_WARNING_CONTINUE = 0;
    public static final int ON_WARNING_SKIP_STUDY = 1;
    public static final int ON_WARNING_EXIT = 2;
    private static SendImage _instance = new SendImage();
    private static String[] OPTIONS = { "0", "1", "2" };
    private static Hashtable PARAM_CHECKS = new Hashtable();
    private String _url = null;
    private int _delOpt = 0;
    private int _onErrorOpt = 0;
    private int _onWarningOpt = 0;
    private File _rootDir = null;
    private File[] _dirList = null;
    private File[][] _fileList = null;
    private int _msgID = 0;
    private String _callingTitle = null;
    private String _calledTitle = null;
    private String _host = null;
    private int _port = 0;
    private String _classUID = null;
    private int _sopClass = 0;
    private String _instanceUID = null;
    private int _status = 0;
    private Socket _sock = null;
    private Association _assoc = null;
    private Request _request = null;
    private Response _response = null;
    private DicomObject _dcmObj = null;
    
    private static final class SendImageException extends Exception
    {
	private final int _errcode;
	
	public SendImageException(int i, String string) {
	    super(string);
	    _errcode = i;
	}
	
	public int errcode() {
	    return _errcode;
	}
    }
    
    public static void main(String[] strings) {
	String string = "SendImage.properties";
	switch (strings.length) {
	case 1:
	    string = strings[0];
	    /* fall through */
	case 0:
	    instance().send(string);
	    break;
	default:
	    System.out.println("Wrong number of Commandline Parameters");
	}
    }
    
    public void send(String string) {
	if (_init(string) && open(_url) == 0) {
	while_7_:
	    for (int i = 0; i < _dirList.length; i++) {
		int i_0_ = _fileList[i].length;
	    while_6_:
		for (int i_1_ = 0; i_1_ < _fileList[i].length; i_1_++) {
		    File file = _fileList[i][i_1_];
		    switch (send(file)) {
		    case 45056:
		    case 45062:
		    case 45063:
			switch (_onWarningOpt) {
			case 2:
			    break while_7_;
			case 1:
			    break while_6_;
			}
			/* fall through */
		    case 0:
			i_0_--;
			System.out.println("Sent Image: " + file
					   + " ; Status: " + _status);
			if (_delOpt == 2)
			    _delFile(file);
			break;
		    default:
			if (!_isOpen())
			    return;
			switch (_onErrorOpt) {
			case 2:
			    break while_7_;
			case 1:
			    break while_6_;
			}
		    }
		}
		if (i_0_ == 0) {
		    switch (_delOpt) {
		    case 1:
		    case 2:
			if (_delDir(_dirList[i]))
			    _delLog(_dirList[i].getName() + '.');
			break;
		    }
		}
	    }
	    close();
	}
    }
    
    public static SendImage instance() {
	return _instance;
    }
    
    public String openS(String string) {
	return String.valueOf(open(string));
    }
    
    public int open(String string) {
	try {
	    if (_isOpen())
		throw new SendImageException(-21, "Association already open");
	    if (!_parseURL(string))
		throw new SendImageException(-1, ("MalformedURLException: "
						  + string));
	    _createRequest();
	    _openSocket();
	    _openAssoc();
	} catch (SendImageException sendimageexception) {
	    System.out.println(sendimageexception.getMessage());
	    return sendimageexception.errcode();
	}
	return 0;
    }
    
    public int send(DicomObject dicomobject) {
	try {
	    _dcmObj = dicomobject;
	    _checkDicomObject();
	    _sendReceive();
	    return _status;
	} catch (SendImageException sendimageexception) {
	    System.out.println(sendimageexception.getMessage());
	    return sendimageexception.errcode();
	}
    }
    
    public String sendS(byte[] is, int i) {
	return String.valueOf(send(is, i));
    }
    
    public int send(byte[] is, int i) {
	try {
	    _createDicomObject(is, i);
	    _sendReceive();
	    return _status;
	} catch (SendImageException sendimageexception) {
	    System.out.println(sendimageexception.getMessage());
	    return sendimageexception.errcode();
	}
    }
    
    public int send(File file) {
	try {
	    _createDicomObject(file);
	    _sendReceive();
	    return _status;
	} catch (SendImageException sendimageexception) {
	    System.out.println(sendimageexception.getMessage());
	    return sendimageexception.errcode();
	}
    }
    
    public String closeS() {
	return String.valueOf(close());
    }
    
    public int close() {
	try {
	    if (!_isOpen())
		throw new SendImageException(-22, "Association is closed");
	    _close();
	} catch (SendImageException sendimageexception) {
	    System.out.println(sendimageexception.getMessage());
	    return sendimageexception.errcode();
	}
	return 0;
    }
    
    private boolean _init(String string) {
	Properties properties = new Properties();
	try {
	    FileInputStream fileinputstream = new FileInputStream(string);
	    try {
		properties.load(fileinputstream);
	    } finally {
		fileinputstream.close();
	    }
	    CheckParam.verify(properties, PARAM_CHECKS);
	    _url = properties.getProperty("URL");
	    _delOpt = Integer.parseInt(properties.getProperty("DeleteOption"));
	    _onErrorOpt
		= Integer.parseInt(properties.getProperty("OnErrorOption"));
	    _onWarningOpt
		= Integer.parseInt(properties.getProperty("OnWarningOption"));
	    return _initDirList(properties.getProperty("DirectoryPath"));
	} catch (Exception exception) {
	    System.out.println(exception);
	    return false;
	}
    }
    
    private boolean _initDirList(String string) throws IOException {
	_rootDir = new File(string);
	if (!_rootDir.isDirectory()) {
	    System.out.println(string + " is not a directory!");
	    return false;
	}
	String[] strings = _rootDir.list(new FilenameFilter() {
	    public boolean accept(File file, String string_3_) {
		File file_4_ = new File(file, string_3_);
		return file_4_.isDirectory();
	    }
	});
	_dirList = new File[strings.length];
	_fileList = new File[strings.length][];
	for (int i = 0; i < _dirList.length; i++) {
	    File file = _dirList[i] = new File(_rootDir, strings[i]);
	    String[] strings_5_ = file.list();
	    if (strings_5_ == null)
		throw new IOException("Couldnot list " + file);
	    _fileList[i] = new File[strings_5_.length];
	    for (int i_6_ = 0; i_6_ < strings_5_.length; i_6_++)
		_fileList[i][i_6_] = new File(file, strings_5_[i_6_]);
	}
	return true;
    }
    
    private boolean _delFile(File file) {
	try {
	    file.delete();
	    System.out.println("Deleted " + file);
	    return true;
	} catch (SecurityException securityexception) {
	    System.out.println("Failed to delete " + file);
	    return false;
	}
    }
    
    private boolean _delDir(File file) {
	try {
	    String[] strings = file.list();
	    for (int i = 0; i < strings.length; i++) {
		if (!_delFile(new File(file, strings[i])))
		    return false;
	    }
	    file.delete();
	    System.out.println("Deleted " + file);
	    return true;
	} catch (SecurityException securityexception) {
	    System.out.println("Failed to delete " + file);
	    return false;
	}
    }
    
    private void _delLog(final String namePrefix) {
	String[] strings = _rootDir.list(new FilenameFilter() {
	    public boolean accept(File file, String string) {
		return string.startsWith(namePrefix);
	    }
	});
	for (int i = 0; i < strings.length; i++)
	    _delFile(new File(_rootDir, strings[i]));
    }
    
    private boolean _parseURL(String string) {
	int i = string.indexOf("@dicom://");
	if (i == -1)
	    return false;
	String string_8_ = string.substring(0, i);
	String string_9_ = string.substring(i + 9);
	StringTokenizer stringtokenizer = new StringTokenizer(string_8_, "/");
	if (stringtokenizer.countTokens() != 2)
	    return false;
	_callingTitle = stringtokenizer.nextToken();
	StringTokenizer stringtokenizer_10_
	    = new StringTokenizer(string_9_, "/:");
	if (stringtokenizer_10_.countTokens() != 3)
	    return false;
	_host = stringtokenizer_10_.nextToken();
	try {
	    _port = Integer.parseInt(stringtokenizer_10_.nextToken());
	} catch (NumberFormatException numberformatexception) {
	    return false;
	}
	_calledTitle = stringtokenizer_10_.nextToken();
	return true;
    }
    
    private void _createRequest() throws SendImageException {
	try {
	    _request = new Request();
	    _request.setCalledTitle(_calledTitle);
	    _request.setCallingTitle(_callingTitle);
	    int[] is = { 8193 };
	    _request.addPresentationContext(4118, is);
	    _request.addPresentationContext(4119, is);
	    _request.addPresentationContext(4121, is);
	    _request.addPresentationContext(4120, is);
	    _request.addPresentationContext(4159, is);
	    _request.addPresentationContext(4122, is);
	    _request.addPresentationContext(4123, is);
	    _request.addPresentationContext(4128, is);
	    _request.addPresentationContext(4129, is);
	    _request.addPresentationContext(4131, is);
	    _request.addPresentationContext(4151, is);
	} catch (IllegalValueException illegalvalueexception) {
	    throw new SendImageException(-23, ("IllegalValueException: "
					       + illegalvalueexception
						     .getMessage()));
	}
    }
    
    private void _openSocket() throws SendImageException {
	try {
	    _sock = new Socket(_host, _port);
	} catch (UnknownHostException unknownhostexception) {
	    _sock = null;
	    throw new SendImageException(-11, ("UnknownHostException: "
					       + unknownhostexception
						     .getMessage()));
	} catch (IOException ioexception) {
	    _sock = null;
	    throw new SendImageException(-10, ("IOException: "
					       + ioexception.getMessage()));
	}
    }
    
    private void _openAssoc() throws SendImageException {
	try {
	    _msgID = 0;
	    _assoc = new Association(_sock.getInputStream(),
				     _sock.getOutputStream());
	    _assoc.sendAssociateRequest(_request);
	    _response = _assoc.receiveAssociateResponse();
	} catch (IllegalValueException illegalvalueexception) {
	    _assoc = null;
	    throw new SendImageException(-23, ("IllegalValueException: "
					       + illegalvalueexception
						     .getMessage()));
	} catch (UnknownUIDException unknownuidexception) {
	    _assoc = null;
	    throw new SendImageException(-24,
					 ("UnknownUIDException: "
					  + unknownuidexception.getMessage()));
	} catch (IOException ioexception) {
	    _assoc = null;
	    throw new SendImageException(-20, ("IOException: "
					       + ioexception.getMessage()));
	}
	if (_response instanceof Abort) {
	    _assoc = null;
	    _closeSocket();
	    throw new SendImageException(((Abort) _response).getReason(),
					 "Association aborted: " + _response);
	}
	if (_response instanceof Reject) {
	    _assoc = null;
	    _closeSocket();
	    throw new SendImageException(((Reject) _response).getReason(),
					 "Association rejected: " + _response);
	}
    }
    
    private void _close() throws SendImageException {
	try {
	    _assoc.sendReleaseRequest();
	    _assoc.receiveReleaseResponse();
	} catch (IllegalValueException illegalvalueexception) {
	    throw new SendImageException(-23, ("IllegalValueException: "
					       + illegalvalueexception
						     .getMessage()));
	} catch (IOException ioexception) {
	    throw new SendImageException(-20, ("IOException: "
					       + ioexception.getMessage()));
	} finally {
	    _assoc = null;
	    _closeSocket();
	}
    }
    
    private void _abort() {
	try {
	    _assoc.sendAbort(Abort.DICOM_UL_SERVICE_USER,
			     Abort.REASON_NOT_SPECIFIED);
	} catch (IOException ioexception) {
	    System.out.println("Send Abort throws IOException: "
			       + ioexception.getMessage());
	} finally {
	    _assoc = null;
	    _closeSocket();
	}
    }
    
    private void _closeSocket() {
	try {
	    _sock.close();
	} catch (IOException ioexception) {
	    System.out.println("Close Socket throws IOException: "
			       + ioexception.getMessage());
	}
	_sock = null;
    }
    
    private void _createDicomObject(byte[] is, int i)
	throws SendImageException {
	byte[] is_11_ = new byte[i];
	System.arraycopy(is, 0, is_11_, 0, i);
	ByteArrayInputStream bytearrayinputstream
	    = new ByteArrayInputStream(is_11_);
	_createDicomObject(bytearrayinputstream);
    }
    
    private void _createDicomObject(File file) throws SendImageException {
	try {
	    FileInputStream fileinputstream = new FileInputStream(file);
	    try {
		_createDicomObject(fileinputstream);
	    } finally {
		try {
		    fileinputstream.close();
		} catch (IOException ioexception) {
		    /* empty */
		}
	    }
	} catch (IOException ioexception) {
	    throw new SendImageException(-60, ("Open File throws IOException: "
					       + ioexception.getMessage()));
	}
    }
    
    private void _createDicomObject(InputStream inputstream)
	throws SendImageException {
	try {
	    _dcmObj = new DicomObject();
	    _dcmObj.read(inputstream);
	    _checkDicomObject();
	} catch (OutOfMemoryError outofmemoryerror) {
	    throw new SendImageException
		      (-31, ("Parsing Image throws OutOfMemoryError : "
			     + outofmemoryerror.getMessage()));
	} catch (IOException ioexception) {
	    throw new SendImageException(-30,
					 ("Parsing Image throws IOException: "
					  + ioexception.getMessage()));
	} catch (DicomException dicomexception) {
	    throw new SendImageException
		      (-32, ("Parsing Image throws DicomException: "
			     + dicomexception.getMessage()));
	}
    }
    
    private void _checkDicomObject() throws SendImageException {
	try {
	    _classUID = _dcmObj.getS(62);
	    _instanceUID = _dcmObj.getS(63);
	    _sopClass = UID.getUIDEntry(_classUID).getConstant();
	} catch (DicomException dicomexception) {
	    throw new SendImageException
		      (-32, ("Parsing Image throws DicomException: "
			     + dicomexception.getMessage()));
	} catch (UnknownUIDException unknownuidexception) {
	    throw new SendImageException
		      (-33, ("Parsing Image throws UnknownUIDException: "
			     + unknownuidexception.getMessage()));
	}
    }
    
    private void _sendReceive() throws SendImageException {
	if (!_isOpen())
	    throw new SendImageException(-22, "Association is closed");
	_sendImage();
	_receiveStatus();
    }
    
    private void _sendImage() throws SendImageException {
	try {
	    DicomMessage dicommessage
		= new DicomMessage(1, _nextMsgID(), _dcmObj);
	    dicommessage.affectedSOP(_classUID, _instanceUID);
	    dicommessage.priority(0);
	    _assoc.send(_sopClass, dicommessage, _dcmObj);
	} catch (IllegalValueException illegalvalueexception) {
	    _abort();
	    throw new SendImageException(-41, ("IllegalValueException: "
					       + illegalvalueexception
						     .getMessage()));
	} catch (DicomException dicomexception) {
	    _abort();
	    throw new SendImageException(-42, ("DicomException: "
					       + dicomexception.getMessage()));
	} catch (IOException ioexception) {
	    _abort();
	    throw new SendImageException(-40, ("IOException: "
					       + ioexception.getMessage()));
	}
    }
    
    private void _receiveStatus() throws SendImageException {
	try {
	    DicomObject dicomobject = _assoc.receiveCommand();
	    int i = dicomobject.getI(5);
	    if (i != _msgID) {
		_abort();
		throw new SendImageException(-54,
					     ("Wrong Message ID in Response: "
					      + i));
	    }
	    _status = dicomobject.getI(9);
	    if (_status != 0)
		System.out.println(_promptStatus());
	} catch (UnknownUIDException unknownuidexception) {
	    _abort();
	    throw new SendImageException(-53,
					 ("UnknownUIDException: "
					  + unknownuidexception.getMessage()));
	} catch (IllegalValueException illegalvalueexception) {
	    _abort();
	    throw new SendImageException(-51, ("IllegalValueException: "
					       + illegalvalueexception
						     .getMessage()));
	} catch (DicomException dicomexception) {
	    _abort();
	    throw new SendImageException(-52, ("DicomException: "
					       + dicomexception.getMessage()));
	} catch (IOException ioexception) {
	    _abort();
	    throw new SendImageException(-50, ("IOException: "
					       + ioexception.getMessage()));
	}
    }
    
    private boolean _isOpen() {
	return _assoc != null;
    }
    
    private String _promptStatus() {
	switch (_status) {
	case 0:
	    return "Success";
	case 65024:
	    return "Cancel";
	case 263:
	    return "Warning: Attribute list error";
	case 278:
	    return "Warning: Attribute Value Out of Range";
	case 290:
	    return "Refused: SOP class not supported";
	case 281:
	    return "Error: Class-instance conflict";
	case 273:
	    return "Error: Duplicate SOP instance";
	case 528:
	    return "Error: Duplicate invocation";
	case 277:
	    return "Error: Invalid argument value";
	case 262:
	    return "Error: Invalid attribute value";
	case 279:
	    return "Error: Invalid object instance";
	case 288:
	    return "Error: Missing attribute";
	case 289:
	    return "Error: Missing attribute value";
	case 530:
	    return "Error: Mistyped argument";
	case 276:
	    return "Error: No such argument";
	case 261:
	    return "Error: No such attribute";
	case 275:
	    return "Error: No such event type";
	case 274:
	    return "Error: No such object instance";
	case 280:
	    return "Error: No Such SOP class";
	case 272:
	    return "Error: Processing failure";
	case 531:
	    return "Error: Resource limitation";
	case 529:
	    return "Error: Unrecognized operation";
	case 42752:
	    return "Error: Out of Resources";
	case 42753:
	    return "Refused: Out of Resources - Unable to calculate number of matches";
	case 42754:
	    return "Refused: Out of Resources - Unable to perform sub-operations";
	case 43009:
	    return "Refused: Move Destination unknown";
	case 43264:
	    return "Failed: Data Set/Identifier does not match SOP Class";
	case 45056:
	    return "Warning: Coercion of Data Elements";
	case 45063:
	    return "Warning: Data Set does not match SOP Class";
	case 45062:
	    return "Warning: Elements Discarded";
	case 49152:
	    return "Error: Cannot understand/Unable to process/Failed operation";
	case 1:
	    return "Success: Partial Study Content exists on system supporting SCP";
	case 2:
	    return "Success: None of the Study Content exists on system supporting SCP";
	case 3:
	    return "Success: It is unknown whether or not study content exists on system supporting SCP";
	case 65280:
	    return "Pending";
	case 65281:
	    return "Pending - one or more Optional Keys were not supported";
	default:
	    switch (_status & 0xff00) {
	    case 42752:
		return "Refused: Out of Resources";
	    case 43264:
		return "Error: Data Set does not match SOP";
	    default:
		switch (_status & 0xf000) {
		case 49152:
		    return "Error: Cannot understand";
		default:
		    return "Unrecognized Error: " + _status;
		}
	    }
	}
    }
    
    private int _nextMsgID() {
	_msgID++;
	_msgID &= 0xffff;
	return _msgID;
    }
    
    static {
	PARAM_CHECKS.put("DeleteOption", CheckParam.enum(OPTIONS));
	PARAM_CHECKS.put("OnErrorOption", CheckParam.enum(OPTIONS));
	PARAM_CHECKS.put("OnWarningOption", CheckParam.enum(OPTIONS));
    }
}
