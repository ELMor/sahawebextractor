/* Resampler - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.PixelMatrix;
import com.tiani.dicom.util.UIDUtils;

public class Resampler
{
    public static boolean resample
	(DicomObject dicomobject, int i, int i_0_) throws DicomException {
	if (i <= 0 || i_0_ <= 0 || dicomobject.getSize(1184) != 1)
	    throw new IllegalArgumentException();
	PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
	if (pixelmatrix.getRows() <= i && pixelmatrix.getColumns() <= i_0_)
	    return false;
	PixelMatrix.Dimension dimension
	    = pixelmatrix.adjustRescaleDimension(i, i_0_);
	setDerived(dicomobject, pixelmatrix.rescale(dimension.rows,
						    dimension.columns, null));
	return true;
    }
    
    private static void setDerived
	(DicomObject dicomobject, PixelMatrix pixelmatrix)
	throws DicomException {
	dicomobject.set(58, "DERIVED", 0);
	dicomobject.set(122, ("Reduced pixel resolution. Original: rows="
			      + dicomobject.get(466) + ", columns="
			      + dicomobject.get(467)));
	DicomObject dicomobject_1_ = new DicomObject();
	dicomobject_1_.set(115, dicomobject.get(62));
	dicomobject_1_.set(116, dicomobject.get(63));
	dicomobject.set(123, dicomobject_1_);
	dicomobject.set(63, UIDUtils.createUID());
	dicomobject.set(504, "01");
	dicomobject.set(1313,
			new Float((float) (dicomobject.getI(466)
					   * dicomobject.getI(467))
				  / (float) (pixelmatrix.getRows()
					     * pixelmatrix.getColumns())));
	dicomobject.set(466, new Integer(pixelmatrix.getRows()));
	dicomobject.set(467, new Integer(pixelmatrix.getColumns()));
	setPixelAspectRatio(dicomobject, pixelmatrix.getAspectRatio());
	setPixelSpacing(dicomobject, pixelmatrix.getPixelSpacing());
	dicomobject.set(1184, pixelmatrix.getPixelData());
    }
    
    private static void setPixelAspectRatio
	(DicomObject dicomobject, int[] is) throws DicomException {
	dicomobject.set(473, new Integer(is[0]), 0);
	dicomobject.set(473, new Integer(is[1]), 1);
    }
    
    private static void setPixelSpacing
	(DicomObject dicomobject, float[] fs) throws DicomException {
	int i;
	if (fs != null && (dicomobject.getSize(i = 470) == 2
			   || dicomobject.getSize(i = 308) == 2)) {
	    dicomobject.set(i, new Float(fs[0]), 0);
	    dicomobject.set(i, new Float(fs[1]), 1);
	}
    }
}
