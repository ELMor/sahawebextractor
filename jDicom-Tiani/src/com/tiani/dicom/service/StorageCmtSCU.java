/* StorageCmtSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Hashtable;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.Acceptor;
import com.tiani.dicom.framework.AdvancedAcceptancePolicy;
import com.tiani.dicom.framework.DefAcceptorListener;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DefNEventReportSCU;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.IDimseListener;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.util.UIDUtils;

public class StorageCmtSCU implements StorageCmtConstants
{
    public static final int[] CMT_SOP_CLASSES = { 4097, 4100 };
    public static final int[] CMT_STORE_SOP_CLASSES
	= { 4097, 4100, 4165, 4166, 4167, 4168, 4194, 4118, 4119, 4120, 4121,
	    4122, 4123, 4128, 4129, 4130, 4131, 4148, 4149, 4151, 4153, 4120,
	    4158, 4159, 4161, 4162, 4163, 4164, 4176, 4177, 4178, 4179, 4180,
	    4181, 4124, 4125, 4126, 4127, 4152, 4154, 4155, 4156, 4147, 4196,
	    4195 };
    public static final int[] CMT_NON_IMAGE_STORE_SOP_CLASSES
	= { 4097, 4100, 4165, 4166, 4167, 4168, 4194, 4154, 4155, 4156, 4147 };
    public static final int[] IMAGE_STORE_SOP_CLASSES
	= { 4118, 4119, 4120, 4121, 4122, 4123, 4128, 4129, 4130, 4131, 4148,
	    4149, 4151, 4153, 4120, 4158, 4159, 4161, 4162, 4163, 4164, 4176,
	    4177, 4178, 4179, 4180, 4181 };
    public static final int[] SC_IMAGE_STORAGE_SOP_CLASS = { 4123 };
    static final int[] DEFAULT_TS = { 8193 };
    static final int[] NATIVE_TS = { 8194, 8193 };
    static final int[] JPEG_LOSSLESS_TS = { 8197, 8194, 8193 };
    static final int[] JPEG_BASELINE = { 8196 };
    private final Hashtable cmtRQinProcess = new Hashtable();
    private DicomObject storeResult = new DicomObject();
    private int numCompleted = 0;
    private int numFailed = 0;
    private int numWarning = 0;
    private Requestor requestor = null;
    private DimseExchange connection = null;
    private Thread thread = null;
    private Thread server = null;
    private Acceptor acceptor = null;
    private final AdvancedAcceptancePolicy policy
	= new AdvancedAcceptancePolicy();
    private final DimseRqManager rqManager = new DimseRqManager();
    private final DefAcceptorListener acceptorListener
	= new DefAcceptorListener(false, rqManager, false, false);
    private final DefNEventReportSCU myNEventReportSCU = new DefNEventReportSCU() {
	protected int eventReport
	    (DimseExchange dimseexchange, String string, String string_0_,
	     DicomMessage dicommessage, DicomMessage dicommessage_1_)
	    throws DicomException {
	    DicomObject dicomobject;
	    String string_2_;
	    IDimseListener idimselistener;
	    if ((dicomobject = dicommessage.getDataset()) == null
		|| (string_2_ = (String) dicomobject.get(118)) == null
		|| ((idimselistener
		     = (IDimseListener) cmtRQinProcess.remove(string_2_))
		    == null))
		return 277;
	    idimselistener.notify(dicommessage);
	    synchronized (this) {
		this.notifyAll();
	    }
	    return 0;
	}
    };
    private boolean checkAET = false;
    private boolean multiThreadTCP = false;
    private int maxPduSize = 16352;
    private int assocTimeout = 3000;
    private int releaseTimeout = 1000;
    private int maxNumOpInvoked = 1;
    private int priority = 0;
    private final IAssociationListener assocListener = new IAssociationListener() {
	public void associateRequestReceived
	    (VerboseAssociation verboseassociation, Request request) {
	    /* empty */
	}
	
	public void associateResponseReceived
	    (VerboseAssociation verboseassociation, Response response) {
	    /* empty */
	}
	
	public void associateRequestSent(VerboseAssociation verboseassociation,
					 Request request) {
	    /* empty */
	}
	
	public void associateResponseSent
	    (VerboseAssociation verboseassociation, Response response) {
	    /* empty */
	}
	
	public void releaseRequestReceived
	    (VerboseAssociation verboseassociation) {
	    /* empty */
	}
	
	public void releaseResponseReceived
	    (VerboseAssociation verboseassociation) {
	    /* empty */
	}
	
	public void releaseRequestSent(VerboseAssociation verboseassociation) {
	    /* empty */
	}
	
	public void releaseResponseSent
	    (VerboseAssociation verboseassociation) {
	    /* empty */
	}
	
	public void abortReceived(VerboseAssociation verboseassociation,
				  Abort abort) {
	    /* empty */
	}
	
	public void abortSent(VerboseAssociation verboseassociation, int i,
			      int i_3_) {
	    /* empty */
	}
	
	public void socketClosed(VerboseAssociation verboseassociation) {
	    thread = null;
	    connection = null;
	}
    };
    
    private class MyStoreRspListener implements IDimseRspListener
    {
	private DicomObject item = new DicomObject();
	
	MyStoreRspListener(String string, String string_4_)
	    throws DicomException {
	    item.set(115, string);
	    item.set(116, string_4_);
	}
	
	public void handleRSP
	    (DimseExchange dimseexchange, int i, int i_5_,
	     DicomMessage dicommessage)
	    throws IOException, DicomException, IllegalValueException,
		   UnknownUIDException {
	    int i_6_ = Status.getStatusEntry(i_5_).getType();
	    if (i_6_ == 5) {
		item.set(119, new Integer(i_5_));
		storeResult.append(120, item);
		access$204(StorageCmtSCU.this);
	    } else {
		storeResult.append(121, item);
		if (i_6_ == 1)
		    access$304(StorageCmtSCU.this);
		else
		    access$404(StorageCmtSCU.this);
	    }
	}
    }
    
    public StorageCmtSCU() {
	try {
	    policy.addPresentationContext(4097, DEFAULT_TS);
	    policy.addPresentationContext(4100, DEFAULT_TS);
	    policy.setScuScpRoleSelection(4100, 0, 1);
	    rqManager.regCEchoScp(UID.toString(4097),
				  DefCEchoSCP.getInstance());
	    rqManager.regNEventReportScu("1.2.840.10008.1.20.1",
					 myNEventReportSCU);
	} catch (IllegalValueException illegalvalueexception) {
	    throw new RuntimeException(illegalvalueexception.getMessage());
	}
    }
    
    public void regCStoreScp(int i, IDimseRqListener idimserqlistener)
	throws IllegalValueException {
	policy.addPresentationContext(i, DEFAULT_TS);
	rqManager.regCStoreScp(UID.toString(i), idimserqlistener);
    }
    
    public void setPriority(int i) {
	priority = i;
    }
    
    public void setARTIM(int i) {
	setARTIM(i, i);
    }
    
    public void setARTIM(int i, int i_7_) {
	assocTimeout = i;
	releaseTimeout = i_7_;
	if (acceptor != null)
	    acceptor.setARTIM(i, i_7_);
	acceptorListener.setARTIM(i_7_);
	if (connection != null)
	    connection.setARTIM(i_7_);
    }
    
    public DicomObject getStoreResult() {
	return storeResult;
    }
    
    public int getNumCompleted() {
	return numCompleted;
    }
    
    public int getNumWarning() {
	return numWarning;
    }
    
    public int getNumFailed() {
	return numFailed;
    }
    
    public void clearStoreResult() {
	storeResult = new DicomObject();
	numCompleted = 0;
	numWarning = 0;
	numFailed = 0;
    }
    
    public void setMaxNumOpInvoked(int i) {
	maxNumOpInvoked = i;
    }
    
    public void setCheckAET(boolean bool) {
	checkAET = bool;
	if (!bool) {
	    policy.setCallingTitles(null);
	    policy.setCalledTitle(null);
	}
    }
    
    public int countCmtRQinProcess() {
	return cmtRQinProcess.size();
    }
    
    public void clearCmtRQinProcess() {
	cmtRQinProcess.clear();
    }
    
    public void setMultiThreadAssoc(boolean bool) {
	acceptorListener.setStartThread(bool);
    }
    
    public void setMultiThreadTCP(boolean bool) {
	multiThreadTCP = bool;
	if (acceptor != null)
	    acceptor.setStartThread(bool);
    }
    
    public void setMaxPduSize(int i) {
	maxPduSize = i;
	policy.setMaxPduSize(i);
    }
    
    public void start(int i) {
	if (server != null)
	    throw new IllegalStateException("server is running");
	try {
	    if (Debug.DEBUG > 0)
		Debug.out
		    .println("jdicom: Start Server listening on port " + i);
	    acceptor = new Acceptor(new ServerSocket(i), multiThreadTCP,
				    policy, acceptorListener);
	    acceptor.setARTIM(assocTimeout, releaseTimeout);
	    server = new Thread(acceptor);
	    server.start();
	} catch (Exception exception) {
	    Debug.out.println("jdicom: " + exception);
	    server = null;
	    acceptor = null;
	}
    }
    
    public void stop(boolean bool, boolean bool_8_) {
	try {
	    if (server == null)
		throw new IllegalStateException("server is not running");
	    if (!bool) {
		synchronized (this) {
		    while (cmtRQinProcess.size() > 0)
			this.wait();
		}
	    }
	    acceptor.stop(bool);
	    if (bool_8_)
		server.join();
	    server = null;
	    acceptor = null;
	    if (Debug.DEBUG > 0)
		Debug.out.println("jdicom: Server stopped");
	} catch (Throwable throwable) {
	    throwable.printStackTrace(Debug.out);
	}
    }
    
    public boolean isServerRunning() {
	return server != null;
    }
    
    public Response connect
	(String string, int i, String string_9_, String string_10_, int[] is)
	throws UnknownHostException, IOException, UnknownUIDException,
	       IllegalValueException, DicomException {
	return connect(string, i, string_9_, string_10_, new int[][] { is },
		       new int[][] { DEFAULT_TS });
    }
    
    public Response connect
	(String string, int i, String string_11_, String string_12_,
	 int[][] is, int[][] is_13_)
	throws UnknownHostException, IOException, UnknownUIDException,
	       IllegalValueException, DicomException {
	if (is.length != is_13_.length)
	    throw new IllegalArgumentException();
	Request request = new Request();
	request.setCalledTitle(string_11_);
	request.setCallingTitle(string_12_);
	request.setMaxPduSize(maxPduSize);
	if (checkAET) {
	    policy.setCalledTitle(string_12_);
	    policy.addCallingTitle(string_11_);
	}
	if (maxNumOpInvoked != 1) {
	    request.setMaxOperationsInvoked(maxNumOpInvoked);
	    request.setMaxOperationsPerformed(1);
	}
	for (int i_14_ = 0; i_14_ < is.length; i_14_++)
	    addPresContextsTo(request, is[i_14_], is_13_[i_14_]);
	return connect(string, i, request);
    }
    
    private void addPresContextsTo(Request request, int[] is, int[] is_15_)
	throws IllegalValueException {
	Arrays.sort(is);
	int i = 0;
	for (int i_16_ = 0; i_16_ < is.length; i_16_++) {
	    if (i != is[i_16_])
		request.addPresentationContext(i = is[i_16_], is_15_);
	}
    }
    
    public Response connect(String string, int i, Request request)
	throws UnknownHostException, IOException, UnknownUIDException,
	       IllegalValueException, DicomException {
	if (connection != null)
	    throw new IllegalStateException("existing connection");
	Socket socket = new Socket(string, i);
	requestor = new Requestor(socket, request);
	requestor.addAssociationListener(assocListener);
	VerboseAssociation verboseassociation = requestor.openAssoc();
	if (verboseassociation != null) {
	    try {
		if (thread != null)
		    thread.join(5000L);
	    } catch (InterruptedException interruptedexception) {
		/* empty */
	    }
	    Acknowledge acknowledge = (Acknowledge) requestor.response();
	    connection
		= new DimseExchange(verboseassociation, rqManager, false,
				    false,
				    acknowledge.getMaxOperationsInvoked());
	    connection.setARTIM(releaseTimeout);
	    (thread = new Thread(connection)).start();
	}
	return requestor.response();
    }
    
    public boolean isConnected() {
	return connection != null;
    }
    
    public boolean isEnabled(int i) {
	try {
	    return (isConnected()
		    && (connection.listAcceptedPresentationContexts(i).length
			> 0));
	} catch (IllegalValueException illegalvalueexception) {
	    return false;
	}
    }
    
    public boolean isEnabled(int i, int i_17_) {
	try {
	    if (isConnected()) {
		UIDEntry[] uidentrys
		    = connection.listAcceptedTransferSyntaxes(i);
		for (int i_18_ = 0; i_18_ < uidentrys.length; i_18_++) {
		    if (uidentrys[i_18_].getConstant() == i_17_)
			return true;
		}
	    }
	} catch (IllegalValueException illegalvalueexception) {
	    /* empty */
	}
	return false;
    }
    
    public DimseExchange getConnection() {
	return connection;
    }
    
    public byte[] listAcceptedPresentationContexts(int i)
	throws IllegalValueException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	return connection.listAcceptedPresentationContexts(i);
    }
    
    public UIDEntry[] listAcceptedTransferSyntaxes(int i)
	throws IllegalValueException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	return connection.listAcceptedTransferSyntaxes(i);
    }
    
    public byte getPresentationContext(int i) throws IllegalValueException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	return connection.getPresentationContext(i);
    }
    
    public byte getPresentationContext(int i, int i_19_)
	throws IllegalValueException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	return connection.getPresentationContext(i, i_19_);
    }
    
    public UIDEntry getTransferSyntax(byte i) throws IllegalValueException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	return connection.getTransferSyntax(i);
    }
    
    public DicomMessage echo() throws IOException, IllegalValueException,
				      DicomException, InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	return connection.cecho();
    }
    
    public void storeAsync(DicomObject dicomobject, int i)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException, UnknownUIDException {
	storeAsync(dicomobject, i, null, 0);
    }
    
    public void storeAsync
	(DicomObject dicomobject, int i, String string, int i_20_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException, UnknownUIDException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	String string_21_ = dicomobject.getS(62);
	String string_22_ = dicomobject.getS(63);
	int i_23_ = UID.getUIDEntry(string_21_).getConstant();
	byte i_24_ = getPresentationContext(i_23_, i);
	MyStoreRspListener mystorersplistener
	    = new MyStoreRspListener(string_21_, string_22_);
	if (string == null)
	    connection.cstoreAsync(i_24_, string_21_, string_22_, priority,
				   dicomobject, mystorersplistener);
	else
	    connection.cstoreAsync(i_24_, string_21_, string_22_, priority,
				   string, i_20_, dicomobject,
				   mystorersplistener);
    }
    
    public void waitForAllRSP() throws InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	connection.waitForAllRSP();
    }
    
    public DicomMessage commit
	(DicomObject dicomobject, IDimseListener idimselistener, int i)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	byte i_25_ = getPresentationContext(4100, i);
	return doCommit(dicomobject, idimselistener, i_25_);
    }
    
    public DicomMessage commit
	(DicomObject dicomobject, IDimseListener idimselistener)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	byte i = getPresentationContext(4100);
	return doCommit(dicomobject, idimselistener, i);
    }
    
    private DicomMessage doCommit
	(DicomObject dicomobject, IDimseListener idimselistener, byte i)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (server == null)
	    throw new IllegalStateException("server is not running");
	String string = (String) dicomobject.get(118);
	if (string == null) {
	    string = UIDUtils.createUID();
	    dicomobject.set(118, string);
	}
	dicomobject.deleteItem(120);
	cmtRQinProcess.put(string, idimselistener);
	DicomMessage dicommessage
	    = connection.naction(i, "1.2.840.10008.1.20.1",
				 "1.2.840.10008.1.20.1.1", 1, dicomobject);
	if (Status.getStatusEntry(dicommessage.getI(9)).getType() == 5)
	    cmtRQinProcess.remove(string);
	return dicommessage;
    }
    
    public void release() throws InterruptedException, IOException,
				 IllegalValueException, DicomException {
	if (connection != null) {
	    connection.releaseAssoc();
	    connection = null;
	}
    }
    
    protected void finalize() {
	try {
	    if (connection != null)
		release();
	    if (server != null)
		stop(true, false);
	} catch (Exception exception) {
	    /* empty */
	}
    }
    
    /*synthetic*/ static int access$204(StorageCmtSCU storagecmtscu) {
	return ++storagecmtscu.numFailed;
    }
    
    /*synthetic*/ static int access$304(StorageCmtSCU storagecmtscu) {
	return ++storagecmtscu.numCompleted;
    }
    
    /*synthetic*/ static int access$404(StorageCmtSCU storagecmtscu) {
	return ++storagecmtscu.numWarning;
    }
}
