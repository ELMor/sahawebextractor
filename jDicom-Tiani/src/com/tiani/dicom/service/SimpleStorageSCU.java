/* SimpleStorageSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Request;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.VerboseAssociation;

public class SimpleStorageSCU
{
    public static VerboseAssociation openAssociation
	(String string, int i, String string_0_, String string_1_, int[] is)
	throws IOException, DicomException, UnknownUIDException,
	       IllegalValueException {
	Request request = new Request();
	request.setCalledTitle(string_0_);
	request.setCallingTitle(string_1_);
	int[] is_2_ = { 8193 };
	for (int i_3_ = 0; i_3_ < is.length; i_3_++)
	    request.addPresentationContext(is[i_3_], is_2_);
	Socket socket = new Socket(string, i);
	Requestor requestor = new Requestor(socket, request);
	return requestor.openAssoc();
    }
    
    public static DicomObject echo(VerboseAssociation verboseassociation)
	throws IOException, DicomException, UnknownUIDException,
	       IllegalValueException {
	DicomMessage dicommessage
	    = new DicomMessage(48, verboseassociation.nextMessageID(), null);
	dicommessage.affectedSOPclassUID("1.2.840.10008.1.1");
	verboseassociation.send(4097, dicommessage, null);
	return readRSP(verboseassociation);
    }
    
    public static DicomObject sendDicomObject
	(VerboseAssociation verboseassociation, DicomObject dicomobject)
	throws IOException, DicomException, UnknownUIDException,
	       IllegalValueException {
	DicomMessage dicommessage
	    = new DicomMessage(1, verboseassociation.nextMessageID(),
			       dicomobject);
	String string = dicomobject.getS(63);
	String string_4_ = dicomobject.getS(62);
	dicommessage.affectedSOP(string_4_, string);
	dicommessage.priority(0);
	verboseassociation.send(UID.getUIDEntry(string_4_).getConstant(),
				dicommessage, dicomobject);
	return readRSP(verboseassociation);
    }
    
    public static DicomObject find
	(VerboseAssociation verboseassociation, int i, DicomObject dicomobject,
	 List list)
	throws IOException, DicomException, UnknownUIDException,
	       IllegalValueException {
	DicomMessage dicommessage
	    = new DicomMessage(32, verboseassociation.nextMessageID(),
			       dicomobject);
	dicommessage.affectedSOPclassUID(UID.getUIDEntry(i).getValue());
	dicommessage.priority(0);
	verboseassociation.send(i, dicommessage, dicomobject);
	DicomObject dicomobject_5_ = null;
	for (;;) {
	    DicomObject dicomobject_6_ = readRSP(verboseassociation);
	    if (dicomobject_6_.getI(8) != 257)
		dicomobject_5_ = verboseassociation.receiveData();
	    switch (dicomobject_6_.getI(9)) {
	    case 65280:
	    case 65281:
		list.add(dicomobject_5_);
		break;
	    default:
		return dicomobject_6_;
	    }
	}
    }
    
    public static DicomObject readRSP
	(VerboseAssociation verboseassociation)
	throws IOException, DicomException, UnknownUIDException,
	       IllegalValueException {
	switch (verboseassociation.peek()) {
	case 10:
	    return verboseassociation.receiveCommand();
	case 11:
	    verboseassociation.receiveReleaseRequest();
	    verboseassociation.sendReleaseResponse();
	    verboseassociation.closesocket();
	    throw new IOException("Receive A-RELEASE-RQ");
	case 13:
	    verboseassociation.receiveReleaseResponse();
	    verboseassociation.closesocket();
	    throw new IOException("Receive A-RELEASE-RP");
	case 12:
	    verboseassociation.receiveAbort();
	    verboseassociation.closesocket();
	    throw new IOException("Receive A-ABORT");
	default:
	    verboseassociation.closesocket();
	    throw new IOException("Receive unrecognized PDU");
	}
    }
    
    public static void closeAssociation
	(VerboseAssociation verboseassociation)
	throws IOException, IllegalValueException {
	if (verboseassociation.isOpen()) {
	    verboseassociation.sendReleaseRequest();
	    verboseassociation.receiveReleaseResponse();
	    verboseassociation.closesocket();
	}
    }
}
