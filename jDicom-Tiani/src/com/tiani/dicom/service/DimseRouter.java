/* DimseRouter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.service;
import java.io.IOException;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.IDimseRspListener;

public class DimseRouter implements IDimseRqListener
{
    private final IConnectionFactory factory;
    private final IDimseRqListener localHandler;
    private int curMessageID = 0;
    
    private class BackwardRsp implements IDimseRspListener
    {
	private final DimseExchange rqSrc;
	private final byte pcid;
	
	BackwardRsp(DimseExchange dimseexchange, byte i) {
	    rqSrc = dimseexchange;
	    pcid = i;
	}
	
	public void handleRSP
	    (DimseExchange dimseexchange, int i, int i_0_,
	     DicomMessage dicommessage)
	    throws IOException, DicomException, IllegalValueException,
		   UnknownUIDException {
	    rqSrc.getAssociation().sendInPresentationContext
		(pcid, dicommessage, dicommessage.getDataset());
	    factory.receivedRSP(rqSrc, dimseexchange, i, i_0_, dicommessage);
	}
    }
    
    public static interface IConnectionFactory
    {
	public DimseExchange getConnectionForDimseRQ
	    (DimseExchange dimseexchange, int i, String string,
	     DicomMessage dicommessage)
	    throws IOException, IllegalValueException, UnknownUIDException;
	
	public DimseExchange getConnectionForCancelRQ
	    (DimseExchange dimseexchange, int i)
	    throws IOException, IllegalValueException, UnknownUIDException;
	
	public void receivedRSP(DimseExchange dimseexchange,
				DimseExchange dimseexchange_1_, int i,
				int i_2_, DicomMessage dicommessage);
    }
    
    public DimseRouter(IConnectionFactory iconnectionfactory,
		       IDimseRqListener idimserqlistener) {
	factory = iconnectionfactory;
	localHandler = idimserqlistener;
    }
    
    public void handleRQ
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	curMessageID = i;
	DimseExchange dimseexchange_3_
	    = factory.getConnectionForDimseRQ(dimseexchange, i, string,
					      dicommessage);
	if (dimseexchange_3_ == null)
	    localHandler.handleRQ(dimseexchange, i, string, dicommessage);
	else {
	    try {
		dimseexchange_3_.sendRQ
		    (i, dicommessage.getAbstractSyntax().getConstant(),
		     dicommessage,
		     new BackwardRsp(dimseexchange,
				     dicommessage.getPresentationContext()));
	    } catch (InterruptedException interruptedexception) {
		Debug.out.println("jdicom: " + interruptedexception);
	    }
	}
    }
    
    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	if (curMessageID != i)
	    return false;
	try {
	    DimseExchange dimseexchange_4_
		= factory.getConnectionForCancelRQ(dimseexchange, i);
	    if (dimseexchange_4_ == null)
		return localHandler.handleCancelRQ(dimseexchange, i);
	    dimseexchange_4_.sendCancelRQ(i, dimseexchange
						 .getCurrentAbstractSyntax
						 ().getConstant());
	} catch (Exception exception) {
	    Debug.out.println("jdicom: " + exception);
	}
	return true;
    }
}
