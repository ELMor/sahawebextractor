/* PrintManagementSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.print;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Vector;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DefNEventReportSCU;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.IDimseListener;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.StatusEntry;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.util.UIDUtils;

public class PrintManagementSCU implements IPrintManagement
{
    public static final int[] SOP_CLASSES
	= { 4097, 12292, 12294, 4160, 4114, 4145, 4113, 4175 };
    static final int[] ONLY_DEF_TS = { 8193 };
    private final IAssociationListener _assocListener = new IAssociationListener() {
	public void associateRequestReceived
	    (VerboseAssociation verboseassociation, Request request) {
	    /* empty */
	}
	
	public void associateResponseReceived
	    (VerboseAssociation verboseassociation, Response response) {
	    /* empty */
	}
	
	public void associateRequestSent(VerboseAssociation verboseassociation,
					 Request request) {
	    /* empty */
	}
	
	public void associateResponseSent
	    (VerboseAssociation verboseassociation, Response response) {
	    /* empty */
	}
	
	public void releaseRequestReceived
	    (VerboseAssociation verboseassociation) {
	    /* empty */
	}
	
	public void releaseResponseReceived
	    (VerboseAssociation verboseassociation) {
	    /* empty */
	}
	
	public void releaseRequestSent(VerboseAssociation verboseassociation) {
	    /* empty */
	}
	
	public void releaseResponseSent
	    (VerboseAssociation verboseassociation) {
	    /* empty */
	}
	
	public void abortReceived(VerboseAssociation verboseassociation,
				  Abort abort) {
	    /* empty */
	}
	
	public void abortSent(VerboseAssociation verboseassociation, int i,
			      int i_0_) {
	    /* empty */
	}
	
	public void socketClosed(VerboseAssociation verboseassociation) {
	    connection = null;
	    _sessionUID = null;
	    _curFilmBoxUID = null;
	    _curFilmBoxAttribs = null;
	}
    };
    private boolean grouplens = true;
    private Requestor _requestor = null;
    private DimseExchange connection = null;
    private int _metaSOPid = -1;
    private byte _metaPCid = 0;
    private String _sessionUID = null;
    private DicomObject _refSessionSqItem = new DicomObject();
    private String _curFilmBoxUID = null;
    private DicomObject _curFilmBoxAttribs = null;
    private Vector _statusListener = new Vector();
    private Vector _jobListener = new Vector();
    private int maxPduSize = 32768;
    private int artim = 1000;
    
    private static final class MyNEventReportSCU extends DefNEventReportSCU
    {
	final Vector _listeners;
	
	MyNEventReportSCU(Vector vector) {
	    _listeners = vector;
	}
	
	protected int eventReport
	    (DimseExchange dimseexchange, String string, String string_1_,
	     DicomMessage dicommessage, DicomMessage dicommessage_2_)
	    throws DicomException {
	    synchronized (_listeners) {
		Enumeration enumeration = _listeners.elements();
		while (enumeration.hasMoreElements())
		    ((IDimseListener) enumeration.nextElement())
			.notify(dicommessage);
	    }
	    return 0;
	}
    }
    
    public void setGrouplens(boolean bool) {
	grouplens = bool;
    }
    
    public void setMaxPduSize(int i) {
	maxPduSize = i;
    }
    
    public void setARTIM(int i) {
	artim = i;
	if (connection != null)
	    connection.setARTIM(i);
    }
    
    public Response connect
	(String string, int i, String string_3_, String string_4_, int[] is)
	throws UnknownHostException, IOException, UnknownUIDException,
	       IllegalValueException, DicomException {
	if (connection != null)
	    throw new IllegalStateException("existing connection");
	Socket socket = new Socket(string, i);
	Request request = new Request();
	request.setCalledTitle(string_3_);
	request.setCallingTitle(string_4_);
	request.setMaxPduSize(maxPduSize);
	for (int i_5_ = 0; i_5_ < is.length; i_5_++)
	    request.addPresentationContext(is[i_5_], ONLY_DEF_TS);
	_requestor = new Requestor(socket, request);
	_requestor.addAssociationListener(_assocListener);
	VerboseAssociation verboseassociation = _requestor.openAssoc();
	if (verboseassociation != null) {
	    verboseassociation.setGrouplens(grouplens);
	    DimseRqManager dimserqmanager = new DimseRqManager();
	    dimserqmanager.regNEventReportScu
		(UID.toString(4115), new MyNEventReportSCU(_statusListener));
	    dimserqmanager.regNEventReportScu
		(UID.toString(4113), new MyNEventReportSCU(_jobListener));
	    connection = new DimseExchange(verboseassociation, dimserqmanager,
					   false, false, 1);
	    connection.setARTIM(artim);
	    new Thread(connection).start();
	}
	return _requestor.response();
    }
    
    public boolean isConnected() {
	return connection != null;
    }
    
    public boolean isEnabled(int i) {
	try {
	    return (isConnected()
		    && (connection.listAcceptedPresentationContexts(i).length
			> 0));
	} catch (IllegalValueException illegalvalueexception) {
	    return false;
	}
    }
    
    public String getFilmSessionUID() {
	return _sessionUID;
    }
    
    public String getFilmBoxUID() {
	return _curFilmBoxUID;
    }
    
    public boolean isFilmSession() {
	return _sessionUID != null;
    }
    
    public boolean isGrayscaleFilmSession() {
	return isFilmSession(12292);
    }
    
    public boolean isColorFilmSession() {
	return isFilmSession(12294);
    }
    
    public boolean isFilmSession(int i) {
	return isFilmSession() && _metaSOPid == i;
    }
    
    public boolean isFilmBox() {
	return _curFilmBoxUID != null;
    }
    
    public void addPrinterStatusListener(IDimseListener idimselistener) {
	_statusListener.addElement(idimselistener);
    }
    
    public void removePrinterStatusListener(IDimseListener idimselistener) {
	_statusListener.removeElement(idimselistener);
    }
    
    public void addPrinterJobListener(IDimseListener idimselistener) {
	_jobListener.addElement(idimselistener);
    }
    
    public void removePrinterJobListener(IDimseListener idimselistener) {
	_jobListener.removeElement(idimselistener);
    }
    
    public DicomMessage echo() throws IOException, IllegalValueException,
				      DicomException, InterruptedException {
	return connection.cecho();
    }
    
    public DicomMessage getColorPrinterStatus()
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	return getPrinterStatus(12294);
    }
    
    public DicomMessage getGrayscalePrinterStatus()
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	return getPrinterStatus(12292);
    }
    
    public DicomMessage getPrinterStatus(int i)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	byte i_6_ = connection.getPresentationContext(i);
	return connection.nget(i_6_, UID.toString(4115), UID.toString(16387),
			       null);
    }
    
    public DicomMessage getPrintJobStatus(String string)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	byte i = connection.getPresentationContext(4113);
	return connection.nget(i, UID.toString(4113), string, null);
    }
    
    public DicomMessage getPrinterConfiguration()
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	byte i = connection.getPresentationContext(4175);
	return connection.nget(i, UID.toString(4175), UID.toString(16389),
			       null);
    }
    
    public DicomMessage createGrayscaleFilmSession(DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	return createFilmSession(12292, dicomobject);
    }
    
    public DicomMessage createColorFilmSession(DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	return createFilmSession(12294, dicomobject);
    }
    
    public DicomMessage createFilmSession(int i, DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID != null)
	    throw new IllegalStateException("Existing FilmSession");
	_metaSOPid = i;
	_metaPCid = connection.getPresentationContext(_metaSOPid);
	String string = UIDUtils.createUID();
	DicomMessage dicommessage
	    = connection.ncreate(_metaPCid, UID.toString(4108), string,
				 dicomobject);
	StatusEntry statusentry
	    = Status.getStatusEntry(4108, dicommessage.getI(9));
	if (statusentry != null && statusentry.getType() != 5) {
	    _sessionUID = string;
	    _refSessionSqItem.set(115, UID.toString(4108));
	    _refSessionSqItem.set(116, _sessionUID);
	}
	return dicommessage;
    }
    
    public DicomMessage setFilmSession(DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID == null)
	    throw new IllegalStateException("Missing FilmSession");
	return connection.nset(_metaPCid, UID.toString(4108), _sessionUID,
			       dicomobject);
    }
    
    public DicomMessage printFilmSession()
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID == null)
	    throw new IllegalStateException("Missing FilmSession");
	return connection.naction(_metaPCid, UID.toString(4108), _sessionUID,
				  1, null);
    }
    
    public DicomMessage deleteFilmSession()
	throws IOException, IllegalValueException, InterruptedException,
	       DicomException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID == null)
	    throw new IllegalStateException("Missing FilmSession");
	DicomMessage dicommessage
	    = connection.ndelete(_metaPCid, UID.toString(4108), _sessionUID);
	_sessionUID = null;
	_curFilmBoxUID = null;
	_curFilmBoxAttribs = null;
	return dicommessage;
    }
    
    public DicomMessage createFilmBox(DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID == null)
	    throw new IllegalStateException("Missing FilmSession");
	String string = UIDUtils.createUID();
	dicomobject.set(725, _refSessionSqItem);
	DicomMessage dicommessage
	    = connection.ncreate(_metaPCid, UID.toString(4109), string,
				 dicomobject);
	if (Status.getStatusEntry(4109, dicommessage.getI(9)).getType() != 5) {
	    _curFilmBoxUID = string;
	    _curFilmBoxAttribs = dicommessage.getDataset();
	}
	return dicommessage;
    }
    
    public DicomMessage setFilmBox(DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_curFilmBoxUID == null)
	    throw new IllegalStateException("Missing FilmBox");
	return connection.nset(_metaPCid, UID.toString(4109), _curFilmBoxUID,
			       dicomobject);
    }
    
    public DicomMessage printFilmBox()
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_curFilmBoxUID == null)
	    throw new IllegalStateException("Missing FilmBox");
	return connection.naction(_metaPCid, UID.toString(4109),
				  _curFilmBoxUID, 1, null);
    }
    
    public DicomMessage deleteFilmBox()
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_curFilmBoxUID == null)
	    throw new IllegalStateException("Missing FilmBox");
	DicomMessage dicommessage
	    = connection.ndelete(_metaPCid, UID.toString(4109),
				 _curFilmBoxUID);
	_curFilmBoxUID = null;
	_curFilmBoxAttribs = null;
	return dicommessage;
    }
    
    public DicomMessage createPresentationLUT
	(String string, int i, int i_7_, int[] is, String string_8_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(506, new Integer(is.length), 0);
	dicomobject.set(506, new Integer(i), 1);
	dicomobject.set(506, new Integer(i_7_), 2);
	if (string_8_ != null)
	    dicomobject.set(507, string_8_);
	for (int i_9_ = 0; i_9_ < is.length; i_9_++)
	    dicomobject.set(509, new Integer(is[i_9_]), i_9_);
	DicomObject dicomobject_10_ = new DicomObject();
	dicomobject_10_.set(1239, dicomobject);
	return createPresentationLUT(string, dicomobject_10_);
    }
    
    public DicomMessage createShapePresentationLUT
	(String string, String string_11_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(1240, string_11_);
	return createPresentationLUT(string, dicomobject);
    }
    
    public DicomMessage createPresentationLUT
	(String string, DicomObject dicomobject)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	byte i = connection.getPresentationContext(4145);
	return connection.ncreate(i, UID.toString(4145), string,
				  PrintManagementUtils
				      .getPresentationLUTModule(dicomobject));
    }
    
    public DicomMessage deletePresentationLUT(String string)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID == null)
	    throw new IllegalStateException("Missing FilmSession");
	byte i = connection.getPresentationContext(4145);
	return connection.ndelete(i, UID.toString(4145), string);
    }
    
    public DicomMessage createOverlayBox
	(String string, DicomObject dicomobject, DicomObject dicomobject_12_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID == null)
	    throw new IllegalStateException("Missing FilmSession");
	dicomobject_12_.set(1366, dicomobject);
	byte i = connection.getPresentationContext(4160);
	return connection.ncreate(i, UID.toString(4160), string,
				  dicomobject_12_);
    }
    
    public DicomMessage setOverlayBox
	(String string, DicomObject dicomobject, DicomObject dicomobject_13_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID == null)
	    throw new IllegalStateException("Missing FilmSession");
	if (dicomobject != null)
	    dicomobject_13_.set(1366, dicomobject);
	byte i = connection.getPresentationContext(4160);
	return connection.nset(i, UID.toString(4160), string, dicomobject_13_);
    }
    
    public DicomMessage deleteOverlayBox(String string)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (_sessionUID == null)
	    throw new IllegalStateException("Missing FilmSession");
	byte i = connection.getPresentationContext(4160);
	return connection.ndelete(i, UID.toString(4160), string);
    }
    
    public int countImageBoxes() {
	if (_curFilmBoxAttribs == null)
	    throw new IllegalStateException("Missing FilmBox");
	return _curFilmBoxAttribs.getSize(726);
    }
    
    public int countAnnotationBoxes() {
	if (_curFilmBoxAttribs == null)
	    throw new IllegalStateException("Missing FilmBox");
	return Math.max(0, _curFilmBoxAttribs.getSize(727));
    }
    
    private DicomMessage setImageBox
	(int i, DicomObject dicomobject, DicomObject dicomobject_14_,
	 int i_15_, int i_16_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (i <= 0 || i > countImageBoxes())
	    throw new IllegalArgumentException("Invalid ImagePosition - " + i);
	DicomObject dicomobject_17_
	    = (DicomObject) _curFilmBoxAttribs.get(726, i - 1);
	dicomobject_14_.set(729, new Integer(i));
	dicomobject_14_.set(i_15_,
			    PrintManagementUtils.getPixelModule(dicomobject));
	return connection.nset(_metaPCid, UID.getUIDEntry(i_16_).getValue(),
			       dicomobject_17_.getS(116), dicomobject_14_);
    }
    
    public DicomMessage setGrayscaleImageBox
	(int i, DicomObject dicomobject, DicomObject dicomobject_18_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	return setImageBox(i, dicomobject, dicomobject_18_, 732, 4110);
    }
    
    public DicomMessage setColorImageBox
	(int i, DicomObject dicomobject, DicomObject dicomobject_19_)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	return setImageBox(i, dicomobject, dicomobject_19_, 733, 4111);
    }
    
    public DicomMessage setAnnotationBox(int i, String string)
	throws IOException, IllegalValueException, DicomException,
	       InterruptedException {
	if (connection == null)
	    throw new IllegalStateException("no connection");
	if (i <= 0 || i > countAnnotationBoxes())
	    throw new IllegalArgumentException("Invalid AnnotationPosition - "
					       + i);
	DicomObject dicomobject
	    = (DicomObject) _curFilmBoxAttribs.get(727, i - 1);
	DicomObject dicomobject_20_ = new DicomObject();
	dicomobject_20_.set(737, new Integer(i));
	dicomobject_20_.set(738, string);
	byte i_21_ = connection.getPresentationContext(4114);
	return connection.nset(i_21_, dicomobject.getS(115),
			       dicomobject.getS(116), dicomobject_20_);
    }
    
    public void release() throws InterruptedException, IOException,
				 IllegalValueException, DicomException {
	if (_sessionUID != null)
	    deleteFilmSession();
	if (connection != null) {
	    connection.releaseAssoc();
	    connection = null;
	}
    }
    
    protected void finalize() {
	if (connection != null) {
	    try {
		release();
	    } catch (Exception exception) {
		/* empty */
	    }
	}
    }
}
