/* IPrintManagement - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.print;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.IDimseListener;

public interface IPrintManagement
{
    public String getFilmSessionUID();
    
    public String getFilmBoxUID();
    
    public int countImageBoxes();
    
    public int countAnnotationBoxes();
    
    public boolean isEnabled(int i);
    
    public boolean isFilmSession();
    
    public boolean isGrayscaleFilmSession();
    
    public boolean isColorFilmSession();
    
    public boolean isFilmBox();
    
    public DicomMessage getGrayscalePrinterStatus() throws Exception;
    
    public DicomMessage getColorPrinterStatus() throws Exception;
    
    public DicomMessage getPrinterConfiguration() throws Exception;
    
    public void addPrinterStatusListener(IDimseListener idimselistener);
    
    public void removePrinterStatusListener(IDimseListener idimselistener);
    
    public void addPrinterJobListener(IDimseListener idimselistener);
    
    public void removePrinterJobListener(IDimseListener idimselistener);
    
    public DicomMessage getPrintJobStatus(String string) throws Exception;
    
    public DicomMessage createGrayscaleFilmSession(DicomObject dicomobject)
	throws Exception;
    
    public DicomMessage createColorFilmSession(DicomObject dicomobject)
	throws Exception;
    
    public DicomMessage setFilmSession(DicomObject dicomobject)
	throws Exception;
    
    public DicomMessage printFilmSession() throws Exception;
    
    public DicomMessage deleteFilmSession() throws Exception;
    
    public DicomMessage createFilmBox(DicomObject dicomobject)
	throws Exception;
    
    public DicomMessage setFilmBox(DicomObject dicomobject) throws Exception;
    
    public DicomMessage printFilmBox() throws Exception;
    
    public DicomMessage deleteFilmBox() throws Exception;
    
    public DicomMessage setGrayscaleImageBox
	(int i, DicomObject dicomobject, DicomObject dicomobject_0_)
	throws Exception;
    
    public DicomMessage setColorImageBox
	(int i, DicomObject dicomobject, DicomObject dicomobject_1_)
	throws Exception;
    
    public DicomMessage setAnnotationBox(int i, String string)
	throws Exception;
    
    public DicomMessage createPresentationLUT
	(String string, DicomObject dicomobject) throws Exception;
    
    public DicomMessage createPresentationLUT
	(String string, int i, int i_2_, int[] is, String string_3_)
	throws Exception;
    
    public DicomMessage createShapePresentationLUT
	(String string, String string_4_) throws Exception;
    
    public DicomMessage deletePresentationLUT(String string) throws Exception;
    
    public DicomMessage createOverlayBox
	(String string, DicomObject dicomobject, DicomObject dicomobject_5_)
	throws Exception;
    
    public DicomMessage setOverlayBox
	(String string, DicomObject dicomobject, DicomObject dicomobject_6_)
	throws Exception;
    
    public DicomMessage deleteOverlayBox(String string) throws Exception;
}
