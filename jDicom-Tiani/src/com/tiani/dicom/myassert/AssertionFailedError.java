/* AssertionFailedError - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.myassert;

public class AssertionFailedError extends Error
{
    public AssertionFailedError() {
	/* empty */
    }
    
    public AssertionFailedError(String string) {
	super(string);
    }
}
