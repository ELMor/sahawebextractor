/* PrintServerDummyGUI - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.printserver;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.util.CheckParam;

public class PrintServerDummyGUI
{
    public static void main(String[] strings) {
	try {
	    final String propFile = (strings.length > 0 ? strings[0]
				     : "PrintServerDummy.properties");
	    final PrintServerDummyApplet applet
		= new PrintServerDummyApplet(PrintServerDummy
						 .loadProperties(propFile));
	    new AppletFrame("PrintServerDummy v1.4.4", applet, 400, 400, new WindowAdapter() {
		public void windowClosing(WindowEvent windowevent) {
		    try {
			storeParams(propFile, applet.getParams());
		    } catch (Exception exception) {
			System.out.println(exception);
		    }
		    System.exit(0);
		}
	    });
	} catch (Throwable throwable) {
	    throwable.printStackTrace(System.out);
	}
    }
    
    private static void storeParams(String string, Properties properties)
	throws IOException {
	CheckParam.verify(properties, Param.CHECKS);
	FileOutputStream fileoutputstream = new FileOutputStream(string);
	try {
	    properties.store(fileoutputstream,
			     "Properties for PrintServerDummy");
	} finally {
	    fileoutputstream.close();
	}
    }
}
