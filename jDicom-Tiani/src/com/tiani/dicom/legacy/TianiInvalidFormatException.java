/* TianiInvalidFormatException - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.legacy;
import java.io.IOException;

public class TianiInvalidFormatException extends IOException
{
    public TianiInvalidFormatException() {
	/* empty */
    }
    
    public TianiInvalidFormatException(String string) {
	super(string);
    }
}
