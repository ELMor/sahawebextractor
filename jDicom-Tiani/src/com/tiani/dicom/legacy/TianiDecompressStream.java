/* TianiDecompressStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.legacy;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class TianiDecompressStream extends FilterInputStream
{
    public static final byte SH_INIT_SWAP = 1;
    public static final byte SH_INIT_NOSWAP = 2;
    public static final byte CH_INIT = 3;
    public static final byte CH_END = -2;
    public static final byte SH_END = -1;
    public static final byte SH_REP = 10;
    public static final byte SH_NIB_DIF = 12;
    public static final byte SH_CHAR_DIF = 14;
    public static final byte SH_INCOMP_12 = 16;
    public static final byte SH_INCOMP = 18;
    public static final byte CH_REP = 110;
    public static final byte CH_NIB_DIF = 112;
    public static final byte CH_INCOMP = 118;
    private byte lB = 0;
    private short lS = 0;
    private int outCnt = 0;
    private byte[] outBuff = new byte[512];
    private int outPos = 0;
    private int outPosRead = 0;
    private boolean endOfDecompress = false;
    private int inCnt = 0;
    
    public TianiDecompressStream(InputStream inputstream) throws IOException {
	super(inputstream);
	outBuff = new byte[512];
	decompress_init();
    }
    
    public final int read() throws IOException {
	if (outPosRead >= outPos) {
	    outPos = 0;
	    outPosRead = 0;
	    decompress_more();
	}
	if (endOfDecompress)
	    return -1;
	int i = outBuff[outPosRead++];
	i &= 0xff;
	return i;
    }
    
    public final int read(byte[] is, int i, int i_0_) throws IOException {
	if (outPosRead >= outPos) {
	    outPos = 0;
	    outPosRead = 0;
	    decompress_more();
	}
	if (endOfDecompress && outPosRead >= outPos)
	    return -1;
	int i_1_ = Math.min(i_0_, outPos - outPosRead);
	System.arraycopy(outBuff, outPosRead, is, i, i_1_);
	outPosRead += i_1_;
	return i_1_;
    }
    
    public final int read(byte[] is) throws IOException {
	return read(is, 0, is.length);
    }
    
    public final void readFully(byte[] is) throws IOException {
	readFully(is, 0, is.length);
    }
    
    public final void readFully(byte[] is, int i, int i_2_)
	throws IOException {
	if (i_2_ < 0)
	    throw new IndexOutOfBoundsException();
	int i_4_;
	for (int i_3_ = 0; i_3_ < i_2_; i_3_ += i_4_) {
	    i_4_ = read(is, i + i_3_, i_2_ - i_3_);
	    if (i_4_ < 0)
		throw new EOFException();
	}
    }
    
    private final void decompress_init() throws IOException {
	boolean bool = false;
	byte i = readByte();
	switch (i) {
	case 1:
	case 2:
	case 3: {
	    int i_5_ = readByte() & 0xff;
	    i_5_ <<= 8;
	    i_5_ |= readByte() & 0xff;
	    i_5_ <<= 8;
	    i_5_ |= readByte() & 0xff;
	    i_5_ <<= 8;
	    i_5_ |= readByte() & 0xff;
	}
	    /* fall through */
	default:
	    switch (i) {
	    case 3: {
		byte i_6_ = readByte();
		out(i_6_);
		break;
	    }
	    case 1:
	    case 2: {
		byte i_7_ = readByte();
		byte i_8_ = readByte();
		out(i_7_, i_8_);
		break;
	    }
	    }
	}
    }
    
    private final void decompress_more() throws IOException {
	boolean bool = false;
	if (!endOfDecompress) {
	    byte i = readByte();
	    switch (i) {
	    case -2:
		endOfDecompress = true;
		break;
	    case -1:
		endOfDecompress = true;
		break;
	    default: {
		int i_9_ = readByte() & 0xff;
	    switch_0_:
		switch (i) {
		case 118:
		    while (i_9_-- > 0) {
			byte i_10_ = readByte();
			out(i_10_);
		    }
		    break;
		case 18:
		    while (i_9_-- > 0) {
			byte i_11_ = readByte();
			byte i_12_ = readByte();
			out(i_11_, i_12_);
		    }
		    break;
		case 16:
		    do {
			int i_13_ = readByte();
			int i_14_ = readByte();
			byte i_15_ = (byte) (i_13_ >> 4 & 0xf);
			byte i_16_
			    = (byte) (i_13_ << 4 & 0xf0 | i_14_ >> 4 & 0xf);
			out(i_15_, i_16_);
			if (--i_9_ == 0)
			    break switch_0_;
			if (i_9_ < 0)
			    throw new TianiInvalidFormatException
				      ("length decoding mismatch");
			i_15_ = (byte) (i_14_ & 0xf);
			byte i_17_ = readByte();
			out(i_15_, i_17_);
			if (--i_9_ == 0)
			    break switch_0_;
		    } while (i_9_ >= 0);
		    throw new TianiInvalidFormatException
			      ("length decoding mismatch");
		case 14: {
		    int i_18_ = getLastShort();
		    i_18_ &= 0xffff;
		    while (i_9_-- > 0) {
			int i_19_ = readByte();
			int i_20_ = i_19_;
			i_20_ &= 0xff;
			i_20_ -= 127;
			i_18_ += i_20_;
			out((short) i_18_);
		    }
		    break;
		}
		case 112: {
		    int i_21_ = getLastByte();
		    i_21_ &= 0xff;
		    do {
			int i_22_ = readByte();
			int i_23_ = i_22_;
			i_23_ >>= 4;
			i_23_ &= 0xf;
			i_23_ -= 7;
			i_21_ += i_23_;
			byte i_24_ = (byte) i_21_;
			out(i_24_);
			if (--i_9_ == 0)
			    break;
			i_23_ = i_22_;
			i_23_ &= 0xf;
			i_23_ -= 7;
			i_21_ += i_23_;
			i_24_ = (byte) i_21_;
			out(i_24_);
		    } while (--i_9_ != 0);
		    break;
		}
		case 12: {
		    int i_25_ = getLastShort();
		    i_25_ &= 0xffff;
		    do {
			int i_26_ = readByte();
			int i_27_ = i_26_;
			i_27_ >>= 4;
			i_27_ &= 0xf;
			i_27_ -= 7;
			i_25_ += i_27_;
			out((short) i_25_);
			if (--i_9_ == 0)
			    break;
			i_27_ = i_26_;
			i_27_ &= 0xf;
			i_27_ -= 7;
			i_25_ += i_27_;
			out((short) i_25_);
		    } while (--i_9_ != 0);
		    break;
		}
		case 110: {
		    byte i_28_ = getLastByte();
		    while (i_9_-- > 0)
			out(i_28_);
		    break;
		}
		case 10: {
		    short i_29_ = getLastShort();
		    while (i_9_-- > 0)
			out(i_29_);
		    break;
		}
		default:
		    throw new TianiInvalidFormatException
			      ("illegal function code while decompressing: "
			       + i);
		}
	    }
	    }
	}
    }
    
    private final void out(byte i) {
	lB = i;
	outCnt++;
	outBuff[outPos++] = i;
    }
    
    private final void out(short i) {
	lS = i;
	outCnt += 2;
	outBuff[outPos++] = (byte) (i & 0xff);
	outBuff[outPos++] = (byte) (i >> 8);
    }
    
    private final void out(byte i, byte i_30_) {
	short i_31_ = (short) (i << 8 | i_30_ & 0xff);
	out(i_31_);
    }
    
    private final byte readByte() throws IOException {
	int i = super.read();
	if (i == -1)
	    throw new TianiInvalidFormatException("Unexpected end of file");
	inCnt++;
	return (byte) i;
    }
    
    private final byte getLastByte() {
	return lB;
    }
    
    private final short getLastShort() {
	return lS;
    }
}
