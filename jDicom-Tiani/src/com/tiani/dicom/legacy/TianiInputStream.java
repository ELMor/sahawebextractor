/* TianiInputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.legacy;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.StringTokenizer;

import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.util.MarkableInputStream;

public class TianiInputStream extends MarkableInputStream
{
    public static final int UNKNOWN = 0;
    public static final int DICOM_FILE = 1;
    public static final int DICOM_STREAM_IMPLVR_LE = 2;
    public static final int DICOM_STREAM_EXPLVR_LE = 3;
    public static final int DICOM_STREAM_EXPLVR_BE = 4;
    public static final int BIRNI_HISTORY = 5;
    public static final int BIRNI_ROI = 6;
    public static final int ENCAPSED_UNCOMPRESSED = 7;
    public static final int ENCAPSED_COMPRESSED = 8;
    public static final int ENCAPSED_JPEG_LOSSLESS = 9;
    private static final int MAX_FORMAT = 9;
    private static final String[] FORMAT_NAMES
	= { "Unknown", "DICOM File", "DICOM Stream ImplVR/LE",
	    "DICOM Stream ExplVR/LE", "DICOM Stream ExplVR/BE",
	    "BIRNI History", "BIRNI ROI", "TIANI Encapsed",
	    "Encapsed uncompressed", "Encapsed compressed" };
    private static final byte[] ACRN_LE = { 8, 0 };
    private static final byte[] ACRN_BE = { 0, 8 };
    private static final byte[] DICM = { 68, 73, 67, 77 };
    private static final byte[] HISTORY = { 91, 32, 72, 73 };
    private static final byte[] ROI = { 91, 32, 82, 79 };
    private static final byte[] MAGI = { 77, 65, 71, 73 };
    private static final int[] TSID = { 8193, 8194, 8195 };
    private int _format = 0;
    private Properties _encapseheader;
    private Thread _loadThread;
    
    public TianiInputStream(InputStream inputstream, int i) {
	super(inputstream);
	_encapseheader = new Properties();
	_loadThread = null;
	_format = i;
	if (_format <= 0 || _format > 9)
	    throw new IllegalArgumentException("Invalid format:" + _format);
    }
    
    public TianiInputStream(InputStream inputstream)
	throws IOException, TianiInvalidFormatException {
	super(inputstream);
	_encapseheader = new Properties();
	_loadThread = null;
	_format = detectFormat(this);
	if (_format == 0)
	    throw new TianiInvalidFormatException("Unknown Format");
    }
    
    private int readInt() throws IOException {
	InputStream inputstream = in;
	int i = inputstream.read();
	int i_0_ = inputstream.read();
	int i_1_ = inputstream.read();
	int i_2_ = inputstream.read();
	if ((i | i_0_ | i_1_ | i_2_) < 0)
	    throw new EOFException();
	return (i_2_ << 24) + (i_1_ << 16) + (i_0_ << 8) + (i << 0);
    }
    
    private final int readTag() throws IOException {
	InputStream inputstream = in;
	int i = inputstream.read();
	int i_3_ = inputstream.read();
	int i_4_ = inputstream.read();
	int i_5_ = inputstream.read();
	if ((i | i_3_ | i_4_ | i_5_) < 0)
	    throw new EOFException();
	return (i_3_ << 24) + (i << 16) + (i_5_ << 8) + (i_4_ << 0);
    }
    
    private static int readFully(InputStream inputstream, byte[] is)
	throws IOException {
	int i = is.length;
	int i_6_;
	int i_7_;
	for (i_7_ = 0; i_7_ < i; i_7_ += i_6_) {
	    i_6_ = inputstream.read(is, i_7_, i - i_7_);
	    if (i_6_ < 0)
		return i_7_;
	}
	return i_7_;
    }
    
    public static int detectFormat
	(InputStream inputstream, Properties properties) throws IOException {
	byte[] is = new byte[512];
	inputstream.mark(is.length);
	int i = readFully(inputstream, is);
	inputstream.reset();
	if (i < 8)
	    return 0;
	if (_contains(is, 128, DICM))
	    return 1;
	if (_contains(is, 0, ACRN_LE))
	    return _isExplicit(is) ? 3 : 2;
	if (_contains(is, 0, ACRN_BE))
	    return 4;
	if (_contains(is, 0, HISTORY))
	    return 5;
	if (_contains(is, 0, ROI))
	    return 6;
	if (_contains(is, 0, MAGI))
	    return _decapse(is, i, inputstream, properties);
	if (canParseImplVRLE(is))
	    return 2;
	return 0;
    }
    
    public static int detectFormat(InputStream inputstream)
	throws IOException {
	return detectFormat(inputstream, new Properties());
    }
    
    public int getFormat() {
	return _format;
    }
    
    public String getFormatName() {
	return FORMAT_NAMES[_format];
    }
    
    public Properties getEncapseHeader() {
	return _encapseheader;
    }
    
    public String toString() {
	return super.toString() + '[' + FORMAT_NAMES[_format] + ']';
    }
    
    public void read(DicomObject dicomobject, boolean bool)
	throws IOException, TianiInvalidFormatException, DicomException {
	switch (_format) {
	case 1:
	    dicomobject.read(this, bool);
	    break;
	case 2:
	case 3:
	case 4:
	    dicomobject.read(this, TSID[_format - 2], bool);
	    break;
	case 5: {
	    BirniHistoryReader birnihistoryreader
		= new BirniHistoryReader(this);
	    birnihistoryreader.read(dicomobject);
	    break;
	}
	case 6: {
	    BirniROIReader birniroireader = new BirniROIReader(this);
	    birniroireader.read(dicomobject);
	    break;
	}
	case 7:
	    dicomobject.read(this, 8193, bool);
	    break;
	case 9:
	    dicomobject.read(this, 8193, false);
	    if (bool) {
		while (readTag() == -73728) {
		    byte[] is = new byte[readInt()];
		    readFully(this, is);
		    dicomobject.append(1184, is);
		}
	    }
	    dicomobject.setFileMetaInformation
		(new FileMetaInformation(dicomobject,
					 "1.2.840.10008.1.2.4.70"));
	    break;
	case 8:
	    dicomobject.read(this, 8193, false);
	    if (bool) {
		int i = dicomobject.getI(466);
		int i_8_ = dicomobject.getI(467);
		int i_9_ = dicomobject.getI(475);
		int i_10_ = dicomobject.getI(461);
		int i_11_ = 1;
		if (dicomobject.getSize(464) > 0)
		    i_11_ = dicomobject.getI(464);
		TianiDecompressStream tianidecompressstream
		    = new TianiDecompressStream(this);
		byte[] is = new byte[i * i_8_ * (i_9_ >> 3) * i_10_ * i_11_];
		tianidecompressstream.readFully(is);
		dicomobject.set(1184, is);
	    }
	    break;
	default:
	    throw new TianiInvalidFormatException("Unknown Format");
	}
    }
    
    public void read(DicomObject dicomobject)
	throws IOException, TianiInvalidFormatException, DicomException {
	read(dicomobject, true);
    }
    
    public int getTransferSyntaxId(DicomObject dicomobject)
	throws DicomException, UnknownUIDException,
	       TianiInvalidFormatException {
	switch (_format) {
	case 1: {
	    DicomObject dicomobject_12_ = dicomobject.getFileMetaInformation();
	    return UID.getUIDEntry(dicomobject_12_.getS(31)).getConstant();
	}
	case 2:
	case 3:
	case 4:
	    return TSID[_format - 2];
	case 7:
	case 8:
	    return 8193;
	case 9:
	    return 8197;
	default:
	    throw new RuntimeException("Invalid Format for prepareImage");
	}
    }
    
    private static int _decapse(byte[] is, int i, InputStream inputstream,
				Properties properties) throws IOException {
	BufferedReader bufferedreader
	    = (new BufferedReader
	       (new InputStreamReader(new ByteArrayInputStream(is, 0, i))));
	int i_13_ = 0;
	String string;
	while ((string = bufferedreader.readLine()) != null) {
	    i_13_ += string.length() + 1;
	    if (string.equals("ENDINFO")) {
		inputstream.skip((long) i_13_);
		return _getEncapsedFormat(properties);
	    }
	    StringTokenizer stringtokenizer = new StringTokenizer(string);
	    if (!stringtokenizer.hasMoreTokens())
		return 0;
	    String string_14_ = stringtokenizer.nextToken();
	    int i_15_ = 0;
	    while (stringtokenizer.hasMoreTokens()) {
		properties.put(string_14_ + i_15_,
			       stringtokenizer.nextToken());
		i_15_++;
	    }
	}
	return 0;
    }
    
    private static int _getEncapsedFormat(Properties properties) {
	String string = properties.getProperty("COMPRESSION0");
	return ("NONE".equals(string) ? 7 : "JPEG_LOSSLESS".equals(string) ? 9
		: 8);
    }
    
    private static boolean canParseImplVRLE(byte[] is) {
	int i = is[0] & 0xff | (is[1] & 0xff) << 8;
	int i_16_ = is[2] & 0xff | (is[3] & 0xff) << 8;
	int i_17_ = (is[4] & 0xff | (is[5] & 0xff) << 8 | (is[6] & 0xff) << 16
		     | (is[7] & 0xff) << 24);
	if (16 + i_17_ > is.length)
	    return false;
	int i_18_ = 8 + i_17_;
	int i_19_ = is[i_18_] & 0xff | (is[i_18_ + 1] & 0xff) << 8;
	int i_20_ = is[i_18_ + 2] & 0xff | (is[i_18_ + 3] & 0xff) << 8;
	return i == i_19_;
    }
    
    private static boolean _contains(byte[] is, int i, byte[] is_21_) {
	if (is.length < is_21_.length + i)
	    return false;
	int i_22_ = i;
	for (int i_23_ = 0; i_23_ < is_21_.length; i_23_++) {
	    if (is[i_22_] != is_21_[i_23_])
		return false;
	    i_22_++;
	}
	return true;
    }
    
    private static boolean _isExplicit(byte[] is) {
	byte[] is_24_ = _vrBytes(DDict.getTypeCode(is[0] + (is[1] << 8),
						   is[2] + (is[3] << 8)));
	if (is_24_ == null)
	    return false;
	return _contains(is, 4, is_24_);
    }
    
    private static byte[] _vrBytes(int i) {
	switch (i) {
	case 4:
	    return new byte[] { 65, 69 };
	case 17:
	    return new byte[] { 65, 83 };
	case 5:
	    return new byte[] { 65, 84 };
	case 9:
	    return new byte[] { 67, 83 };
	case 11:
	    return new byte[] { 68, 65 };
	case 16:
	    return new byte[] { 68, 83 };
	case 28:
	    return new byte[] { 68, 84 };
	case 20:
	    return new byte[] { 70, 68 };
	case 26:
	    return new byte[] { 70, 76 };
	case 15:
	    return new byte[] { 73, 83 };
	case 6:
	    return new byte[] { 76, 79 };
	case 18:
	    return new byte[] { 76, 84 };
	case 8:
	    return new byte[] { 79, 66 };
	case 24:
	    return new byte[] { 79, 87 };
	case 14:
	    return new byte[] { 80, 78 };
	case 7:
	    return new byte[] { 83, 72 };
	case 19:
	    return new byte[] { 83, 76 };
	case 10:
	    return new byte[] { 83, 81 };
	case 23:
	    return new byte[] { 83, 83 };
	case 13:
	    return new byte[] { 83, 84 };
	case 12:
	    return new byte[] { 84, 77 };
	case 2:
	    return new byte[] { 85, 73 };
	case 0:
	    return new byte[] { 85, 78 };
	case 1:
	    return new byte[] { 85, 76 };
	case 3:
	    return new byte[] { 85, 83 };
	case 27:
	    return new byte[] { 85, 84 };
	default:
	    return null;
	}
    }
}
