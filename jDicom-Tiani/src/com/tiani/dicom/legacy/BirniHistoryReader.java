/* BirniHistoryReader - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.legacy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.StringTokenizer;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class BirniHistoryReader extends BufferedReader
{
    private String _line;
    private String[] _tokens;
    private Hashtable _action = new Hashtable();
    private Float _center = null;
    private Float _width = null;
    private int _rotate = 0;
    private boolean _hFlip = false;
    private int _x0 = 0;
    private int _y0 = 0;
    private int _dx = 0;
    private int _dy = 0;
    private boolean _inverse = false;
    
    public BirniHistoryReader(InputStream inputstream) {
	super(new InputStreamReader(inputstream));
    }
    
    public void read(DicomObject dicomobject)
	throws IOException, TianiInvalidFormatException, DicomException {
	parse();
	fill(dicomobject);
    }
    
    private void parse() throws IOException, TianiInvalidFormatException {
	int i = 0;
	int i_0_ = 0;
	while ((_line = this.readLine()) != null) {
	    if (i_0_ == 0) {
		if (_line.equals("[ WORKLH"))
		    i_0_ = 1;
	    } else {
		StringTokenizer stringtokenizer = new StringTokenizer(_line);
		int i_1_ = stringtokenizer.countTokens();
		if (i_1_ != 0) {
		    _tokens = new String[i_1_];
		    for (int i_2_ = 0; i_2_ < i_1_; i_2_++)
			_tokens[i_2_] = stringtokenizer.nextToken();
		    switch (i_0_) {
		    case 1:
			i = tkToInt("#WORKS");
			i_0_ = 2;
			break;
		    case 2:
			if (i == 0)
			    checkToken("]", "WORKLH");
			else {
			    checkToken("[", "PIC_ACTION");
			    i_0_ = 3;
			    break;
			}
			return;
		    case 3:
			_action.clear();
			_action.put("#ACTION",
				    new Integer(tkToInt("#ACTION")));
			i_0_ = 4;
			break;
		    case 4:
			if (_tokens[0].equals("]")) {
			    checkToken("]", "PIC_ACTION");
			    storeAction();
			    i--;
			    i_0_ = 2;
			} else
			    _action.put(_tokens[0], tkToFloat());
			break;
		    }
		}
	    }
	}
	throw new TianiInvalidFormatException(i_0_ == 0
					      ? "Missing Tag: [ WORKLH"
					      : "Unexpected end of file");
    }
    
    private void checkToken(String string, String string_3_)
	throws TianiInvalidFormatException {
	if (_tokens.length != 2 || !_tokens[0].equals(string)
	    || !_tokens[1].equals(string_3_))
	    throw new TianiInvalidFormatException(_line);
    }
    
    private int tkToInt(String string) throws TianiInvalidFormatException {
	if (_tokens.length == 2 && _tokens[0].equals(string)) {
	    try {
		return Integer.parseInt(_tokens[1]);
	    } catch (NumberFormatException numberformatexception) {
		/* empty */
	    }
	}
	throw new TianiInvalidFormatException(_line);
    }
    
    private Float tkToFloat() throws TianiInvalidFormatException {
	if (_tokens.length == 2) {
	    try {
		return new Float(_tokens[1]);
	    } catch (NumberFormatException numberformatexception) {
		/* empty */
	    }
	}
	throw new TianiInvalidFormatException(_line);
    }
    
    private void storeAction() throws TianiInvalidFormatException {
	if (_action.containsKey("#CENTER")) {
	    _center = itemToFloat("#CENTER");
	    _width = itemToFloat("#WIDTH");
	} else if (_action.containsKey("#SCALE")) {
	    _x0 = itemToFloat("#X_OFF").intValue();
	    _y0 = itemToFloat("#Y_OFF").intValue();
	    _dx = itemToFloat("#WIDTH").intValue();
	    _dy = itemToFloat("#HEIGHT").intValue();
	} else if (_action.containsKey("#ROTATE"))
	    storeRotate(itemToFloat("#ROTATE").intValue());
	else if (_action.containsKey("#FLIP_HOR_NUMBER"))
	    _hFlip = !_hFlip;
	else if (_action.containsKey("#FLIP_VER_NUMBER")) {
	    _hFlip = !_hFlip;
	    _rotate += 180;
	} else if (_action.containsKey("#INVERT"))
	    _inverse = !_inverse;
	else
	    Debug.out.println
		("jdicom: BirniHistoryReader: WARNING: Unrecognized #ACTION "
		 + _action.get("#ACTION"));
    }
    
    private void storeRotate(int i) {
	if (_hFlip)
	    _rotate -= i;
	else
	    _rotate += i;
    }
    
    private Float itemToFloat(String string)
	throws TianiInvalidFormatException {
	Float var_float = (Float) _action.get(string);
	if (var_float == null)
	    throw new TianiInvalidFormatException("Missing Tag: " + string
						  + " in #ACTION "
						  + _action.get("#ACTION"));
	return var_float;
    }
    
    private void fill(DicomObject dicomobject) throws DicomException {
	if (_center != null && _width != null) {
	    DicomObject dicomobject_4_ = new DicomObject();
	    dicomobject_4_.set_ge(41, 16, "TIANI");
	    dicomobject_4_.set(1512, _center);
	    dicomobject_4_.set(1513, _width);
	    dicomobject.set(1408, dicomobject_4_);
	}
	_rotate %= 360;
	if (_rotate < 0)
	    _rotate += 360;
	if (_rotate != 0 || _hFlip) {
	    dicomobject.set(1410, new Integer(_rotate));
	    dicomobject.set(1398, _hFlip ? "Y" : "N");
	}
	DicomObject dicomobject_5_ = new DicomObject();
	dicomobject_5_.set(1411, new Long((long) _x0), 0);
	dicomobject_5_.set(1411, new Long((long) (_y0 + _dy)), 1);
	dicomobject_5_.set(1412, new Long((long) (_x0 + _dx)), 0);
	dicomobject_5_.set(1412, new Long((long) _y0), 1);
	dicomobject_5_.set(1415, "SCALE TO FIT");
	dicomobject.set(1413, dicomobject_5_);
	dicomobject.set(1240, _inverse ? "INVERSE" : "IDENTITY");
    }
}
