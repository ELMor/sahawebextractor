/* BirniROIReader - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.legacy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class BirniROIReader extends BufferedReader
{
    private static int _boxWidth = 0;
    private static int _boxHeight = 0;
    private String _line;
    private String[] _tokens;
    private Hashtable _rois = new Hashtable();
    
    private static final class ROIText
    {
	public final int[] start;
	public final String text;
	
	public ROIText(int[] is, String string) {
	    start = is;
	    text = string;
	}
    }
    
    private static final class ROI
    {
	public final Vector draws = new Vector();
	public final Vector texts = new Vector();
	
	private ROI() {
	    /* empty */
	}
    }
    
    public BirniROIReader(InputStream inputstream) {
	super(new InputStreamReader(inputstream));
    }
    
    public void read(DicomObject dicomobject)
	throws IOException, TianiInvalidFormatException, DicomException {
	parse();
	fill(dicomobject);
    }
    
    public static void setBoundingBoxDimension(int i, int i_0_) {
	_boxWidth = i;
	_boxHeight = i_0_;
    }
    
    private void parse() throws IOException, TianiInvalidFormatException {
	int i = 0;
	int i_1_ = 0;
	Vector vector = null;
	Vector vector_2_ = null;
	int[] is = null;
	int i_3_ = 0;
	while ((_line = this.readLine()) != null) {
	    if (i_3_ == 0) {
		if (_line.equals("] BIRNI"))
		    return;
		if (_line.equals("[ OBJECT_LH"))
		    i_3_ = 1;
		else if (_line.equals("[ TEXT_LH"))
		    i_3_ = 9;
	    } else {
		StringTokenizer stringtokenizer = new StringTokenizer(_line);
		int i_4_ = stringtokenizer.countTokens();
		if (i_4_ != 0) {
		    _tokens = new String[i_4_];
		    for (int i_5_ = 0; i_5_ < i_4_; i_5_++)
			_tokens[i_5_] = stringtokenizer.nextToken();
		    switch (i_3_) {
		    case 1:
			if (isToken("[", "OBJECT"))
			    i_3_ = 2;
			else if (isToken("]", "OBJECT_LH"))
			    i_3_ = 0;
			break;
		    case 2:
			tkToInt("#IDENTIFY");
			i_3_ = 3;
			break;
		    case 3:
			tkToInt("#COUNT");
			i_3_ = 4;
			break;
		    case 4:
			vector = getROI(tkToKey("#CREATE")).draws;
			i_3_ = 5;
			break;
		    case 5:
			i = tkToInt("[", "DRAWS");
			i_3_ = i > 0 ? 6 : 8;
			break;
		    case 6:
			vector.addElement(tkTo2Int("#START"));
			i_3_ = 7;
			break;
		    case 7: {
			int[] is_6_ = tkTo2Int("#END");
			if (--i > 0)
			    i_3_ = 6;
			else {
			    vector.addElement(is_6_);
			    i_3_ = 8;
			}
			break;
		    }
		    case 8:
			checkToken("]", "DRAWS");
			i_3_ = 1;
			break;
		    case 9:
			if (isToken("[", "TEXT"))
			    i_3_ = 10;
			else if (isToken("]", "TEXT_LH"))
			    i_3_ = 0;
			else if (isToken("[", "EDITOR_INFO"))
			    i_3_ = 19;
			break;
		    case 19:
			if (isToken("]", "EDITOR_INFO"))
			    i_3_ = 9;
			break;
		    case 10:
			tkToInt("#IDENTIFY");
			i_3_ = 11;
			break;
		    case 11:
			tkToInt("#COUNT");
			i_3_ = 12;
			break;
		    case 12:
			vector_2_ = getROI(tkToKey("#CREATE")).texts;
			i_3_ = 13;
			break;
		    case 13:
			i_1_ = tkToInt("[", "WORDS");
			i_3_ = i_1_ > 0 ? 14 : 18;
			break;
		    case 14:
			is = tkTo2Int("#START");
			i_3_ = 15;
			break;
		    case 15: {
			String string = tkToText();
			if (string.length() > 0)
			    vector_2_.addElement(new ROIText(is, string));
			i_3_ = 16;
			break;
		    }
		    case 16:
			tkToInt("#FONT");
			i_3_ = 17;
			break;
		    case 17:
			tkToInt("#DIR");
			i_3_ = --i_1_ > 0 ? 14 : 18;
			break;
		    case 18:
			checkToken("]", "WORDS");
			i_3_ = 9;
			break;
		    }
		}
	    }
	}
	throw new TianiInvalidFormatException("Unexpected end of file");
    }
    
    private boolean isToken(String string, String string_7_) {
	return (_tokens.length == 2 && _tokens[0].equals(string)
		&& _tokens[1].equals(string_7_));
    }
    
    private void checkToken(String string, String string_8_)
	throws TianiInvalidFormatException {
	if (!isToken(string, string_8_))
	    throw new TianiInvalidFormatException(_line);
    }
    
    private String tkToKey(String string) throws TianiInvalidFormatException {
	if (_tokens.length != 2 || !_tokens[0].equals(string))
	    throw new TianiInvalidFormatException(_line);
	return _tokens[1];
    }
    
    private int tkToInt(String string) throws TianiInvalidFormatException {
	if (_tokens.length == 2 && _tokens[0].equals(string)) {
	    try {
		return Integer.parseInt(_tokens[1]);
	    } catch (NumberFormatException numberformatexception) {
		/* empty */
	    }
	}
	throw new TianiInvalidFormatException(_line);
    }
    
    private int tkToInt(String string, String string_9_)
	throws TianiInvalidFormatException {
	if (_tokens.length == 3 && _tokens[0].equals(string)
	    && _tokens[1].equals(string_9_)) {
	    try {
		return Integer.parseInt(_tokens[2]);
	    } catch (NumberFormatException numberformatexception) {
		/* empty */
	    }
	}
	throw new TianiInvalidFormatException(_line);
    }
    
    private int[] tkTo2Int(String string) throws TianiInvalidFormatException {
	if (_tokens.length == 3 && _tokens[0].equals(string)) {
	    try {
		return new int[] { Integer.parseInt(_tokens[1]),
				   Integer.parseInt(_tokens[2]) };
	    } catch (NumberFormatException numberformatexception) {
		/* empty */
	    }
	}
	throw new TianiInvalidFormatException(_line);
    }
    
    private String tkToText() throws TianiInvalidFormatException {
	if (!_tokens[0].equals("#TEXT"))
	    throw new TianiInvalidFormatException(_line);
	return _line.substring(_line.indexOf("#TEXT") + 6);
    }
    
    private ROI getROI(String string) {
	ROI roi = (ROI) _rois.get(string);
	if (roi == null) {
	    roi = new ROI();
	    _rois.put(string, roi);
	}
	return roi;
    }
    
    private void fill(DicomObject dicomobject) throws DicomException {
	dicomobject.set(57, "ISO_IR 100");
	Enumeration enumeration = _rois.keys();
	while (enumeration.hasMoreElements()) {
	    String string = (String) enumeration.nextElement();
	    ROI roi = (ROI) _rois.get(string);
	    DicomObject dicomobject_10_ = new DicomObject();
	    dicomobject.append(1399, dicomobject_10_);
	    int i = dicomobject.getSize(1399);
	    dicomobject_10_.set(1382, string);
	    dicomobject_10_.set(1400, new Integer(i));
	    dicomobject_10_.set(1402, "BIRNI_ROI");
	    DicomObject dicomobject_11_ = new DicomObject();
	    dicomobject.append(1381, dicomobject_11_);
	    dicomobject_11_.set(1382, string);
	    fillDraws(dicomobject_11_, roi.draws);
	    fillTexts(dicomobject_11_, roi.texts);
	}
    }
    
    private void fillDraws(DicomObject dicomobject, Vector vector)
	throws DicomException {
	int i = vector.size();
	if (i != 0) {
	    DicomObject dicomobject_12_ = new DicomObject();
	    dicomobject.append(1388, dicomobject_12_);
	    dicomobject_12_.set(1385, "PIXEL");
	    dicomobject_12_.set(1393, new Integer(2));
	    dicomobject_12_.set(1394, new Integer(i));
	    Enumeration enumeration = vector.elements();
	    while (enumeration.hasMoreElements()) {
		int[] is = (int[]) enumeration.nextElement();
		dicomobject_12_.append(1395, new Float((float) is[0]));
		dicomobject_12_.append(1395, new Float((float) is[1]));
	    }
	    dicomobject_12_.set(1396, "POLYLINE");
	    dicomobject_12_.set(1397, "N");
	}
    }
    
    private void fillTexts(DicomObject dicomobject, Vector vector)
	throws DicomException {
	int i = vector.size();
	if (i != 0) {
	    Enumeration enumeration = vector.elements();
	    while (enumeration.hasMoreElements()) {
		DicomObject dicomobject_13_ = new DicomObject();
		dicomobject.append(1387, dicomobject_13_);
		dicomobject_13_.set(1383, "PIXEL");
		ROIText roitext = (ROIText) enumeration.nextElement();
		dicomobject_13_.set(1386, roitext.text);
		int[] is = roitext.start;
		dicomobject_13_.set(1389, new Float((float) is[0]), 0);
		dicomobject_13_
		    .set(1389, new Float((float) (is[1] + _boxHeight)), 1);
		dicomobject_13_.set(1390,
				    new Float((float) (is[0] + _boxWidth)), 0);
		dicomobject_13_.set(1390, new Float((float) is[1]), 1);
		dicomobject_13_.set(1409, "LEFT");
	    }
	}
    }
}
