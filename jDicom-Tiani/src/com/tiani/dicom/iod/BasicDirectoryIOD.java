/* BasicDirectoryIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class BasicDirectoryIOD
{
    static final UserOption cFileSetCharacterSet
	= new UserOption("C:Specific File-set Character Set");
    public static final Attribute[] FILE_SET_IDENTIFICATION
	= { new Attribute(38, 2, null, null), new Attribute(39, 3, null, null),
	    new Attribute(40, 1, cFileSetCharacterSet, null) };
    static final UserOption cReferencedObject
	= new UserOption("C:Referenced Object");
    static final UserOption cIndirectReferencedObject
	= new UserOption("C:Indirect Referenced Object");
    static final ICondition IF_REFERENCED_FILE
	= new IfOr(new ICondition[] { new IfPresent(50), new IfPresent(51) });
    public static final Attribute[] DIRECTORY_RECORD_SEQUENCE
	= { new Attribute(45, 1, null, null),
	    new Attribute(46, 1, null,
			  new IfEqualInt(46, new int[] { 0, 65535 })),
	    new Attribute(47, 1, null, null),
	    new Attribute(48, 1, null,
			  new IfEqual(48,
				      (new String[]
				       { "PATIENT", "STUDY", "SERIES", "IMAGE",
					 "OVERLAY", "MODALITY LUT", "VOI LUT",
					 "CURVE", "TOPIC", "VISIT", "RESULTS",
					 "INTERPRETATION", "STUDY COMPONENT",
					 "STORED PRINT", "PRESENTATION",
					 "PRIVATE", "MRDR" }))),
	    new Attribute(49, 1, new IfEqual(48, "PRIVATE"), null),
	    new Attribute(50, 1, cReferencedObject, null),
	    new Attribute(51, 1, cIndirectReferencedObject, null),
	    new Attribute(52, 1, IF_REFERENCED_FILE, null),
	    new Attribute(53, 1, IF_REFERENCED_FILE, null),
	    new Attribute(54, 1, IF_REFERENCED_FILE, null) };
    public static final Attribute[] DIRECTORY_INFORMATION
	= { new Attribute(41, 1, null, null), new Attribute(42, 1, null, null),
	    new Attribute(43, 1, null,
			  new IfEqualInt(43, new int[] { 0, 65535 })),
	    new Attribute(44, 2, null, null, DIRECTORY_RECORD_SEQUENCE) };
    public static final Attribute[] PATIENT_KEYS
	= { new Attribute(57, 1, GeneralModules.cSpecificCharacterSet, null),
	    new Attribute(147, 2, null, null),
	    new Attribute(148, 1, null, null) };
    public static final Attribute[] STUDY_KEYS
	= { new Attribute(57, 1, GeneralModules.cSpecificCharacterSet, null),
	    new Attribute(64, 1, null, null), new Attribute(70, 1, null, null),
	    new Attribute(95, 2, null, null),
	    new Attribute(425, 1, new IfNot(new IfPresent(53)), null),
	    new Attribute(427, 1, null, null),
	    new Attribute(77, 2, null, null) };
    public static final Attribute[] SERIES_KEYS
	= { new Attribute(57, 1, GeneralModules.cSpecificCharacterSet, null),
	    new Attribute(81, 1, null, null),
	    new Attribute(426, 1, null, null),
	    new Attribute(428, 1, null, null),
	    new Attribute(699, 3, null, new IfSizeEqual(699, 1),
			  CommonImage.imagePixelModule) };
    public static final Attribute[] IMAGE_KEYS
	= { new Attribute(57, 1, GeneralModules.cSpecificCharacterSet, null),
	    new Attribute(430, 1, null, null),
	    new Attribute(699, 3, null, new IfSizeEqual(699, 1),
			  CommonImage.imagePixelModule) };
    public static final UserOption[] options
	= { cFileSetCharacterSet, cReferencedObject,
	    cIndirectReferencedObject };
    
    private BasicDirectoryIOD() {
	/* empty */
    }
}
