/* IODError - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class IODError
{
    public static final int MISSING_ATTRIB = 0;
    public static final int EMPTY_DATA = 1;
    public static final int INVALID_DATA = 2;
    public static final int INVALID_LENGTH = 3;
    public static final int INVALID_RANGE = 4;
    public static final int INVALID_LONG = 5;
    public static final int INVALID_AS = 6;
    public static final int INVALID_CS = 7;
    public static final int INVALID_DA = 8;
    public static final int INVALID_DS = 9;
    public static final int INVALID_DT = 10;
    public static final int INVALID_FD = 11;
    public static final int INVALID_FL = 12;
    public static final int INVALID_IS = 13;
    public static final int INVALID_PN = 14;
    public static final int INVALID_TM = 15;
    public static final int INVALID_UI = 16;
    public static final int EMPTY_SQ_ITEM = 17;
    static final String[] ERROR_PROMPT
	= { "missing:", "empty:", "invalid value:", "too long data:",
	    "out of range:", "no 32-Bit integer:", "illformed age string:",
	    "illformed code string:", "illformed date string:",
	    "illformed decimal string:", "illformed date-time string:",
	    "no 64-bit float:", "no 32-bit float:",
	    "illformed integer string:", "illformed person name:",
	    "illformed time string:", "illformed uid string:",
	    "empty sequence item:" };
    private int kind;
    private Attribute attrib;
    private AttributeContext context;
    
    IODError(int i, Attribute attribute, AttributeContext attributecontext) {
	kind = i;
	attrib = attribute;
	context = attributecontext;
    }
    
    public int kind() {
	return kind;
    }
    
    public Attribute attrib() {
	return attrib;
    }
    
    public AttributeContext context() {
	return context;
    }
    
    public int type() {
	return attrib.type();
    }
    
    public int[] getAT() {
	return attrib.getAT();
    }
    
    public String toString() {
	StringBuffer stringbuffer = new StringBuffer(ERROR_PROMPT[kind]);
	if (context != null)
	    stringbuffer.append(context.toString());
	stringbuffer.append(attrib.toString());
	return stringbuffer.toString();
    }
}
