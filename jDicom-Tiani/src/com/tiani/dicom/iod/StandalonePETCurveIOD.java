/* StandalonePETCurveIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class StandalonePETCurveIOD extends PETCommonIOD
{
    static final Attribute[] curveModule = new Attribute[0];
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem(PETCommonIOD.seriesModule, null),
	    new CompositeIOD.ModuleTableItem(PETCommonIOD.isotopeModule, null),
	    new CompositeIOD.ModuleTableItem((PETCommonIOD
					      .multiGatedAcquisitionModule),
					     PETCommonIOD.ifSeriesTypeGated),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem((CurveModules
					      .curveIdentificationModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CurveModules.curveModule, null),
	    new CompositeIOD.ModuleTableItem(curveModule, null),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, GeneralModules.cSpecificCharacterSet };
    
    private StandalonePETCurveIOD() {
	/* empty */
    }
}
