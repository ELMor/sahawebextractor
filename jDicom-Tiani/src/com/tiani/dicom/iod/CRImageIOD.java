/* CRImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class CRImageIOD
{
    static final Attribute[] seriesModule
	= { new Attribute(182, 2, null, null),
	    new Attribute(381, 2, null, null),
	    new Attribute(305, 3, null, null),
	    new Attribute(311, 3, null, null),
	    new Attribute(316, 3, null, null),
	    new Attribute(325, 3, null, null),
	    new Attribute(326, 3, null, null) };
    static final Attribute[] imageModule
	= { new Attribute(206, 3, null, null),
	    new Attribute(229, 3, null, null),
	    new Attribute(277, 3, null, null),
	    new Attribute(278, 3, null, null),
	    new Attribute(298, 3, null, null),
	    new Attribute(299, 3, null, null),
	    new Attribute(300, 3, null, null),
	    new Attribute(1195, 3, null, null),
	    new Attribute(308, 3, null, null),
	    new Attribute(310, 3, null, null),
	    new Attribute(336, 3, null, null),
	    new Attribute(337, 3, null, null),
	    new Attribute(338, 3, null,
			  new IfEqual(338, new String[] { "LANDSCAPE",
							  "PORTRAIT" })),
	    new Attribute(339, 3, null, null),
	    new Attribute(340, 3, null, null),
	    new Attribute(341, 3, null, null),
	    new Attribute(384, 3, null, null) };
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem(seriesModule, null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule,
					     CommonImage.cContrastBolusUsed),
	    new CompositeIOD.ModuleTableItem(imageModule, null),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.uOverlayPlane),
	    new CompositeIOD.ModuleTableItem(CurveModules.curveModule,
					     CurveModules.uCurve),
	    new CompositeIOD.ModuleTableItem(LUTModules.modalityLUTModule,
					     LUTModules.uModalityLUT),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule,
					     LUTModules.uVOILUT),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated,
	    CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed,
	    OverlayModules.uOverlayPlane, CurveModules.uCurve,
	    LUTModules.uModalityLUT, LUTModules.uVOILUT,
	    GeneralModules.cSpecificCharacterSet };
    
    private CRImageIOD() {
	/* empty */
    }
}
