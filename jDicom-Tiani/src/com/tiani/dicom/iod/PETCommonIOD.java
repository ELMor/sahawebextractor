/* PETCommonIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

class PETCommonIOD
{
    static final Attribute[] energyWindowRangeSequence
	= { new Attribute(623, 3, null, null),
	    new Attribute(624, 3, null, null) };
    static final ICondition ifSeriesTypeGated = new IfEqual(669, 0, "GATED");
    static final ICondition ifSeriesTypeGatedAndBeatRejection
	= new IfAnd(new ICondition[] { ifSeriesTypeGated,
				       new IfEqual(266, "Y") });
    public static final Attribute[] seriesModule
	= { new Attribute(65, 1, null, null), new Attribute(71, 1, null, null),
	    new Attribute(670, 1, null, null),
	    new Attribute(671, 1, null,
			  new IfEqual(671, new String[] { "EMISSION",
							  "TRANSMISSION" })),
	    new Attribute(669, 1, null,
			  new IfMultiEqual(669,
					   (new String[][]
					    { { "STATIC", "DYNAMIC", "GATED",
						"WHOLE BODY" },
					      { "IMAGE", "REPROJECTION" } }))),
	    new Attribute(672, 2, new IfEqual(669, 1, "REPROJECTION"), null),
	    new Attribute(642, 1, ifSeriesTypeGated, null),
	    new Attribute(646, 1, ifSeriesTypeGated, null),
	    new Attribute(653, 1, new IfEqual(669, 0, "DYNAMIC"), null),
	    new Attribute(650, 1, null, null),
	    new Attribute(474, 2, null, null),
	    new Attribute(673, 3, null, null),
	    new Attribute(674, 3, null, null),
	    new Attribute(678, 3, null, null),
	    new Attribute(675, 1, null, null),
	    new Attribute(276, 3, null, null),
	    new Attribute(319, 3, null, null),
	    new Attribute(676, 3, null, null),
	    new Attribute(677, 3, null, null),
	    new Attribute(210, 3, null, null),
	    new Attribute(211, 3, null, null),
	    new Attribute(208, 3, null, null),
	    new Attribute(212, 3, null, null),
	    new Attribute(296, 3, null, null),
	    new Attribute(297, 3, null, null),
	    new Attribute(280, 3, null, null),
	    new Attribute(281, 3, null, null),
	    new Attribute(655, 3, null, null),
	    new Attribute(312, 2, null, null),
	    new Attribute(311, 3, null, null),
	    new Attribute(679, 3, null, null),
	    new Attribute(680, 3, null, null),
	    new Attribute(681, 3, null, null),
	    new Attribute(682, 3, null, null),
	    new Attribute(683, 3, null, null),
	    new Attribute(622, 3, null, null, energyWindowRangeSequence),
	    new Attribute(684, 3, null, null) };
    static final Attribute[] radiopharmaceuticalInformationSequence
	= { new Attribute(660, 2, null, new IfSizeEqual(660, 1),
			  CommonImage.codeSequence),
	    new Attribute(258, 3, null, null),
	    new Attribute(661, 3, null, new IfSizeEqual(661, 1),
			  CommonImage.codeSequence),
	    new Attribute(259, 3, null, null),
	    new Attribute(260, 3, null, null),
	    new Attribute(261, 3, null, null),
	    new Attribute(262, 3, null, null),
	    new Attribute(263, 3, null, null),
	    new Attribute(264, 3, null, null),
	    new Attribute(265, 3, null, null),
	    new Attribute(195, 3, null, null),
	    new Attribute(662, 3, null, new IfSizeEqual(662, 1),
			  CommonImage.codeSequence) };
    static final Attribute[] interventionDrugInformationSequence
	= { new Attribute(198, 3, null, null),
	    new Attribute(192, 3, null, new IfSizeEqual(192, 1),
			  CommonImage.codeSequence),
	    new Attribute(199, 3, null, null),
	    new Attribute(190, 3, null, null),
	    new Attribute(191, 3, null, null) };
    static final Attribute[] isotopeModule
	= { new Attribute(625, 2, null, new IfSizeInRange(625, 1, 2147483647),
			  radiopharmaceuticalInformationSequence),
	    new Attribute(189, 3, null, null,
			  interventionDrugInformationSequence) };
    static final Attribute[] multiGatedAcquisitionModule
	= { new Attribute(266, 2, null,
			  new IfEqual(266, new String[] { "Y", "N" })),
	    new Attribute(252, 3, null, null),
	    new Attribute(271, 3, null, null),
	    new Attribute(272, 3, null, null),
	    new Attribute(273, 3, null, null),
	    new Attribute(255, 3, null, null) };
}
