/* OverlayModules - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

final class OverlayModules
{
    static final Attribute[] overlayIdentificationModule
	= { new Attribute(437, 2, null, null),
	    new Attribute(68, 3, null, null), new Attribute(74, 3, null, null),
	    new Attribute(113, 3, null, null,
			  CommonImage.referencedSOPSequence) };
    static final UserOption uOverlayPlane = new UserOption("U:Overlay Plane");
    static final UserOption cOverlayPlane
	= new UserOption("C:Apply Overlay Plane");
    static final Attribute[] overlayPlaneModule = new Attribute[0];
    static final UserOption uMultiFrameOverlay
	= new UserOption("U:Multi-Frame Overlay");
    static final UserOption cMultiFrameOverlay
	= new UserOption("C:Multi-Frame Overlay");
    static final Attribute[] multiFrameOverlayModule = new Attribute[0];
    
    private OverlayModules() {
	/* empty */
    }
}
