/* PETImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class PETImageIOD extends PETCommonIOD
{
    public static final Attribute[] anatomicRegionSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(139, 3, null, new IfSizeInRange(139, 1, 2147483647),
			  CommonImage.codeSequence) };
    public static final Attribute[] primaryAnatomicStructureSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(141, 3, null, new IfSizeInRange(141, 1, 2147483647),
			  CommonImage.codeSequence) };
    static final Attribute[] imageModule
	= { new Attribute(58, 1, null,
			  new IfMultiEqual(58,
					   new String[][] { { "ORIGINAL",
							      "DERIVED" },
							    { "PRIMARY" } })),
	    new Attribute(461, 1, null, CommonImage.is1SamplePerPixel),
	    new Attribute(462, 1, null, CommonImage.isMonochrome2),
	    new Attribute(475, 1, null, CommonImage.is16BitsAllocated),
	    new Attribute(476, 1, null,
			  CommonImage.isBitsStoredEqualsBitsAllocated),
	    new Attribute(477, 1, null,
			  CommonImage.isHighBitEqualsBitsStoredLess1),
	    new Attribute(489, 1, null, null),
	    new Attribute(490, 1, null, null),
	    new Attribute(685, 1, null, null),
	    new Attribute(251, 1, PETCommonIOD.ifSeriesTypeGated, null),
	    new Attribute(254, 1, PETCommonIOD.ifSeriesTypeGated, null),
	    new Attribute(267, 1,
			  PETCommonIOD.ifSeriesTypeGatedAndBeatRejection,
			  null),
	    new Attribute(268, 1,
			  PETCommonIOD.ifSeriesTypeGatedAndBeatRejection,
			  null),
	    new Attribute(504, 1, CommonImage.cLossyImageCompression,
			  CommonImage.enumLossyImageCompression),
	    new Attribute(693, 1, null, null),
	    new Attribute(66, 2, null, null), new Attribute(72, 2, null, null),
	    new Attribute(320, 2, null, null),
	    new Attribute(253, 3, null, null),
	    new Attribute(269, 3, null, null),
	    new Attribute(270, 3, null, null),
	    new Attribute(686, 3, null, null),
	    new Attribute(687, 3, null, null),
	    new Attribute(688, 3, null, null),
	    new Attribute(689, 1, new IfNot(new IfEqual(675, "NONE")), null),
	    new Attribute(690, 3, null, null),
	    new Attribute(692, 3, null, null),
	    new Attribute(112, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(114, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(138, 3, null, new IfSizeEqual(138, 1),
			  anatomicRegionSequence),
	    new Attribute(140, 3, null, new IfSizeInRange(140, 1, 2147483647),
			  primaryAnatomicStructureSequence) };
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem(PETCommonIOD.seriesModule, null),
	    new CompositeIOD.ModuleTableItem(PETCommonIOD.isotopeModule, null),
	    new CompositeIOD.ModuleTableItem((PETCommonIOD
					      .multiGatedAcquisitionModule),
					     PETCommonIOD.ifSeriesTypeGated),
	    new CompositeIOD.ModuleTableItem((NMImageIOD
					      .patientOrientationModule),
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .frameOfReferenceModule),
					     CommonImage.uFrameOfReference),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePlaneModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule,
					     null),
	    new CompositeIOD.ModuleTableItem(imageModule, null),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.uOverlayPlane),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule,
					     LUTModules.uVOILUT),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, CommonImage.cLossyImageCompression,
	    OverlayModules.uOverlayPlane, LUTModules.uVOILUT,
	    GeneralModules.cSpecificCharacterSet };
    
    private PETImageIOD() {
	/* empty */
    }
}
