/* USImageCommonIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

class USImageCommonIOD
{
    static final UserOption cImagesSpatiallyRelated
	= new UserOption("C:Images Spatially Related");
    static final Attribute[] frameOfReferenceModule
	= { new Attribute(389, 1, null, null),
	    new Attribute(390, 1, null, null),
	    new Attribute(391, 1, null, null),
	    new Attribute(392, 1, null, null),
	    new Attribute(395, 1, null, new IfInRange(395, 0, 12)),
	    new Attribute(396, 1, null, new IfInRange(396, 0, 12)),
	    new Attribute(399, 1, null, null),
	    new Attribute(400, 1, null, null),
	    new Attribute(393, 3, null, null),
	    new Attribute(394, 3, null, null),
	    new Attribute(397, 3, null, null),
	    new Attribute(398, 3, null, null) };
    static final UserOption uUSRegionCalibration
	= new UserOption("U:US Region Calibration");
    static final UserOption cUSComponentCalibration
	= new UserOption("C:US Component Calibration");
    static final ICondition ifPixelComponentOrganizationExists
	= new IfPresent(412);
    static final ICondition ifPixelComponentOrganization0
	= new IfEqualInt(412, 0);
    static final ICondition ifPixelComponentOrganization1
	= new IfEqualInt(412, 1);
    static final ICondition ifPixelComponentOrganization2
	= new IfEqualInt(412, 2);
    static final ICondition ifPixelComponentOrganization01
	= new IfEqualInt(412, new int[] { 0, 1 });
    static final Attribute[] sequenceOfUltrasoundRegions
	= { new Attribute(389, 1, null, null),
	    new Attribute(390, 1, null, null),
	    new Attribute(391, 1, null, null),
	    new Attribute(392, 1, null, null),
	    new Attribute(395, 1, null, new IfInRange(395, 0, 12)),
	    new Attribute(396, 1, null, new IfInRange(396, 0, 12)),
	    new Attribute(399, 1, null, null),
	    new Attribute(400, 1, null, null),
	    new Attribute(393, 3, null, null),
	    new Attribute(394, 3, null, null),
	    new Attribute(397, 3, null, null),
	    new Attribute(398, 3, null, null),
	    new Attribute(386, 1, null, new IfInRange(386, 0, 5)),
	    new Attribute(387, 1, null, new IfInRange(387, 0, 18)),
	    new Attribute(388, 1, null, new IfInRange(388, 0, 7)),
	    new Attribute(412, 1, cUSComponentCalibration,
			  new IfInRange(412, 0, 2)),
	    new Attribute(413, 1, ifPixelComponentOrganization0, null),
	    new Attribute(414, 1, ifPixelComponentOrganization1, null),
	    new Attribute(415, 1, ifPixelComponentOrganization1, null),
	    new Attribute(416, 1, ifPixelComponentOrganizationExists,
			  new IfInRange(416, 0, 12)),
	    new Attribute(417, 1, ifPixelComponentOrganizationExists,
			  new IfInRange(417, 0, 9)),
	    new Attribute(418, 1, ifPixelComponentOrganization01, null),
	    new Attribute(419, 1, ifPixelComponentOrganization01, null),
	    new Attribute(420, 1, ifPixelComponentOrganization01, null),
	    new Attribute(421, 1, ifPixelComponentOrganization2, null),
	    new Attribute(422, 1, ifPixelComponentOrganization2, null),
	    new Attribute(423, 1, ifPixelComponentOrganization2, null),
	    new Attribute(401, 3, null, null),
	    new Attribute(403, 3, null, null),
	    new Attribute(404, 3, null, null),
	    new Attribute(405, 3, null, null),
	    new Attribute(406, 3, null, null),
	    new Attribute(407, 3, null, null),
	    new Attribute(408, 3, null, null),
	    new Attribute(409, 3, null, null),
	    new Attribute(410, 3, null, null),
	    new Attribute(411, 3, null, null) };
    static final Attribute[] regionCalibrationModule
	= { new Attribute(385, 1, null, null, sequenceOfUltrasoundRegions) };
    static final UserOption cImageAcquiredInStageProtocol
	= new UserOption("C:Image Acquired In Stage Protocol");
    static final String[] piWith8BitsAllocated
	= { "MONOCHROME2", "RGB", "YBR_FULL", "YBR_FULL_422",
	    "YBR_PARTIAL_422" };
    static final ICondition bitsAllocatedValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    String string = dicomobject.getS(462);
	    if (string == null || string.length() == 0)
		return true;
	    int i = dicomobject.getI(475);
	    if (string.equals("PALETTE COLOR"))
		return i == 8 || i == 16;
	    for (int i_0_ = 0;
		 i_0_ < USImageCommonIOD.piWith8BitsAllocated.length; i_0_++) {
		if (string.equals(USImageCommonIOD.piWith8BitsAllocated[i_0_]))
		    return i == 8;
	    }
	    return true;
	}
    };
    static final ICondition bitsStoredValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    String string = dicomobject.getS(462);
	    if (string == null || string.length() == 0)
		return true;
	    int i = dicomobject.getI(476);
	    if (string.equals("PALETTE COLOR"))
		return i == 8 || i == 16;
	    for (int i_1_ = 0;
		 i_1_ < USImageCommonIOD.piWith8BitsAllocated.length; i_1_++) {
		if (string.equals(USImageCommonIOD.piWith8BitsAllocated[i_1_]))
		    return i == 8;
	    }
	    return true;
	}
    };
    static final ICondition highBitValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    String string = dicomobject.getS(462);
	    if (string == null || string.length() == 0)
		return true;
	    int i = dicomobject.getI(477);
	    if (string.equals("PALETTE COLOR"))
		return i == 7 || i == 15;
	    for (int i_2_ = 0;
		 i_2_ < USImageCommonIOD.piWith8BitsAllocated.length; i_2_++) {
		if (string.equals(USImageCommonIOD.piWith8BitsAllocated[i_2_]))
		    return i == 7;
	    }
	    return true;
	}
    };
    static final ICondition planarConfigurationValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    String string = dicomobject.getS(462);
	    if (string == null || string.length() == 0)
		return true;
	    int i = dicomobject.getI(463);
	    if (string.equals("RGB"))
		return i == 0 || i == 1;
	    if (string.equals("YBR_FULL"))
		return i == 1;
	    if (string.equals("YBR_FULL_422")
		|| string.equals("YBR_PARTIAL_422"))
		return i == 0;
	    return true;
	}
    };
    public static final Attribute[] anatomicRegionSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(139, 3, null, new IfSizeInRange(139, 1, 2147483647),
			  CommonImage.codeSequence) };
    public static final Attribute[] primaryAnatomicStructureSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(141, 3, null, new IfSizeInRange(141, 1, 2147483647),
			  CommonImage.codeSequence) };
    public static final Attribute[] transducerPositionSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(143, 3, null, new IfSizeInRange(143, 1, 2147483647),
			  CommonImage.codeSequence) };
    public static final Attribute[] transducerOrientationSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(145, 3, null, new IfSizeInRange(145, 1, 2147483647),
			  CommonImage.codeSequence) };
    static final Attribute[] imageModule
	= { new Attribute(462, 1, null,
			  new IfEqual(462,
				      (new String[]
				       { "MONOCHROME2", "PALETTE COLOR", "RGB",
					 "YBR_FULL", "YBR_FULL_422",
					 "YBR_PARTIAL_422" }))),
	    new Attribute(475, 1, null, bitsAllocatedValueFilter),
	    new Attribute(476, 1, null, bitsStoredValueFilter),
	    new Attribute(477, 1, null, highBitValueFilter),
	    new Attribute(463, 1, new IfNot(CommonImage.is1SamplePerPixel),
			  planarConfigurationValueFilter),
	    new Attribute(478, 1, null, CommonImage.isPixelRepresentation0),
	    new Attribute(465, 1, new IfPresent(464), null),
	    new Attribute(58, 2, null, null),
	    new Attribute(504, 1, CommonImage.cLossyImageCompression,
			  CommonImage.enumLossyImageCompression),
	    new Attribute(126, 2, cImageAcquiredInStageProtocol, null),
	    new Attribute(129, 2, cImageAcquiredInStageProtocol, null),
	    new Attribute(469, 3, null,
			  new IfEqualInt(469, new int[] { 0, 1 })),
	    new Attribute(112, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(114, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(124, 3, null, null),
	    new Attribute(125, 3, null, null),
	    new Attribute(127, 3, null, null),
	    new Attribute(128, 3, null, null),
	    new Attribute(130, 3, null, null),
	    new Attribute(131, 3, null, null),
	    new Attribute(138, 3, null, new IfSizeEqual(138, 1),
			  anatomicRegionSequence),
	    new Attribute(140, 3, null, new IfSizeInRange(140, 1, 2147483647),
			  primaryAnatomicStructureSequence),
	    new Attribute(142, 3, null, new IfSizeInRange(142, 1, 2147483647),
			  transducerPositionSequence),
	    new Attribute(144, 3, null, new IfSizeInRange(144, 1, 2147483647),
			  transducerOrientationSequence),
	    new Attribute(251, 3, null, null),
	    new Attribute(253, 3, null, null),
	    new Attribute(266, 3, null,
			  new IfEqual(266, 3, new String[] { "Y", "N" })),
	    new Attribute(267, 3, null, null),
	    new Attribute(268, 3, null, null),
	    new Attribute(273, 3, null, null),
	    new Attribute(368, 3, null, null),
	    new Attribute(369, 3, null, null),
	    new Attribute(402, 3, null, null),
	    new Attribute(370, 3, null, null),
	    new Attribute(371, 3, null, null),
	    new Attribute(373, 3, null, null),
	    new Attribute(374, 3, null, null),
	    new Attribute(375, 3, null, null),
	    new Attribute(376, 3, null, null),
	    new Attribute(377, 3, null, null),
	    new Attribute(378, 3, null, null),
	    new Attribute(379, 3, null, null),
	    new Attribute(382, 3, null, null),
	    new Attribute(383, 3, null, null),
	    new Attribute(1164, 3, null, null) };
}
