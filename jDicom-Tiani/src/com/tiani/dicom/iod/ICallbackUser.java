/* ICallbackUser - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public interface ICallbackUser
{
    public boolean isTrue(String string, DicomObject dicomobject)
	throws DicomException;
}
