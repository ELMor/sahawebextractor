/* IfPresent - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfPresent implements ICondition
{
    private int dname;
    private int index;
    
    public IfPresent(int i, int i_0_) {
	dname = i;
	index = i_0_;
    }
    
    public IfPresent(int i) {
	this(i, 0);
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	return dicomobject.getSize(dname) > index;
    }
}
