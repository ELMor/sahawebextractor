/* MRImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class MRImageIOD
{
    static final Attribute[] imageModule
	= { new Attribute(58, 1, null, CommonImage.enumImageType),
	    new Attribute(461, 1, null, CommonImage.is1SamplePerPixel),
	    new Attribute(462, 1, null, CommonImage.isMonochrome),
	    new Attribute(475, 1, null, CommonImage.is16BitsAllocated),
	    new Attribute(183, 1, null, null),
	    new Attribute(184, 1, null, null),
	    new Attribute(185, 2, null, null),
	    new Attribute(186, 2, null,
			  new IfEqual(186, new String[] { "2D", "3D" })),
	    new Attribute(213, 2,
			  new IfOr(new ICondition[]
				   { new IfAnyEqual(184, "SK"),
				     new IfNot(new IfAnyEqual(183, "EP")) }),
			  null),
	    new Attribute(214, 2, null, null),
	    new Attribute(224, 2, null, null),
	    new Attribute(215, 2, new IfAnyEqual(183, "IR"), null),
	    new Attribute(251, 2,
			  new IfAnyEqual(185, new String[] { "CG", "PPG" }),
			  null),
	    new Attribute(187, 3, null, null),
	    new Attribute(188, 3, null,
			  new IfEqual(188, new String[] { "Y", "N" })),
	    new Attribute(216, 3, null, null),
	    new Attribute(217, 3, null, null),
	    new Attribute(218, 3, null, null),
	    new Attribute(220, 3, null, null),
	    new Attribute(221, 3, null, null),
	    new Attribute(222, 3, null, null),
	    new Attribute(225, 3, null, null),
	    new Attribute(226, 3, null, null),
	    new Attribute(227, 3, null, null),
	    new Attribute(253, 3, null, null),
	    new Attribute(266, 3, null,
			  new IfEqual(266, new String[] { "Y", "N" })),
	    new Attribute(267, 3, null, null),
	    new Attribute(268, 3, null, null),
	    new Attribute(269, 3, null, null),
	    new Attribute(270, 3, null, null),
	    new Attribute(271, 3, null, null),
	    new Attribute(272, 3, null, null),
	    new Attribute(273, 3, null, null),
	    new Attribute(274, 3, null, null),
	    new Attribute(275, 3, null, null),
	    new Attribute(276, 3, null, null),
	    new Attribute(323, 3, null, null),
	    new Attribute(324, 3, null, null),
	    new Attribute(330, 3, null, null),
	    new Attribute(331, 3, null,
			  new IfEqual(331, new String[] { "ROW", "COL" })),
	    new Attribute(332, 3, null, null),
	    new Attribute(334, 3, null, null),
	    new Attribute(333, 3, null,
			  new IfEqual(333, new String[] { "Y", "N" })),
	    new Attribute(335, 3, null, null),
	    new Attribute(444, 3, null, null),
	    new Attribute(445, 3, null, null),
	    new Attribute(446, 3, null, null) };
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .frameOfReferenceModule),
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePlaneModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule,
					     CommonImage.cContrastBolusUsed),
	    new CompositeIOD.ModuleTableItem(imageModule, null),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.uOverlayPlane),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule,
					     LUTModules.uVOILUT),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated,
	    CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed,
	    OverlayModules.uOverlayPlane, LUTModules.uVOILUT,
	    GeneralModules.cSpecificCharacterSet };
    
    private MRImageIOD() {
	/* empty */
    }
}
