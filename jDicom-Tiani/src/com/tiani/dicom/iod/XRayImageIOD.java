/* XRayImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

class XRayImageIOD
{
    public static final Attribute[] anatomicRegionSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(139, 3, null, new IfSizeInRange(139, 1, 2147483647),
			  CommonImage.codeSequence) };
    public static final Attribute[] primaryAnatomicStructureSequence
	= { new Attribute(91, 1, null, null), new Attribute(92, 1, null, null),
	    new Attribute(93, 3, null, null),
	    new Attribute(141, 3, null, new IfSizeInRange(141, 1, 2147483647),
			  CommonImage.codeSequence) };
    static final ICondition bitStoredValueFilter = new ICondition() {
	public boolean isTrue
	    (DicomObject dicomobject, ICallbackUser icallbackuser)
	    throws DicomException {
	    switch (dicomobject.getI(476)) {
	    case 8:
		return true;
	    case 12:
	    case 16:
		return dicomobject.getI(475) != 8;
	    default:
		return false;
	    }
	}
    };
    static final Attribute[] imageModule
	= { new Attribute(465, 1, CommonImage.cMultiFrameImage,
			  new IfEqualInt(465, new int[] { 1577059, 1577061 })),
	    new Attribute(504, 1, CommonImage.cLossyImageCompression,
			  CommonImage.enumLossyImageCompression),
	    new Attribute(58, 1, null, CommonImage.enumImageType),
	    new Attribute(486, 1, null, null),
	    new Attribute(461, 1, null, CommonImage.is1SamplePerPixel),
	    new Attribute(462, 1, null, CommonImage.isMonochrome2),
	    new Attribute(475, 1, null, CommonImage.is8or16BitsAllocated),
	    new Attribute(476, 1, null, bitStoredValueFilter),
	    new Attribute(477, 1, null,
			  CommonImage.isHighBitEqualsBitsStoredLess1),
	    new Attribute(478, 1, null, CommonImage.isPixelRepresentation0),
	    new Attribute(185, 3, null, null),
	    new Attribute(138, 3, null, new IfSizeEqual(138, 1),
			  anatomicRegionSequence),
	    new Attribute(140, 3, null, new IfSizeInRange(140, 1, 2147483647),
			  primaryAnatomicStructureSequence),
	    new Attribute(516, 3, null, null),
	    new Attribute(113, 3, null, null,
			  CommonImage.referencedSOPSequence),
	    new Attribute(122, 3, null, null),
	    new Attribute(336, 3, null, null),
	    new Attribute(610, 3, null,
			  new IfEqual(610, new String[] { "YES", "NO" })) };
    static final ICondition exposureNotPresent = new IfNot(new IfPresent(300));
    static final Attribute[] acquisitionModule
	= { new Attribute(206, 2, null, null),
	    new Attribute(302, 1, null,
			  new IfEqual(302, new String[] { "SC", "GR" })),
	    new Attribute(299, 2, exposureNotPresent, null),
	    new Attribute(298, 2, exposureNotPresent, null),
	    new Attribute(300, 2,
			  new IfNot(new IfAnd(new ICondition[]
					      { new IfPresent(299),
						new IfPresent(298) })),
			  null),
	    new Attribute(309, 3, null, null),
	    new Attribute(301, 3, null, null),
	    new Attribute(303, 3, null, null),
	    new Attribute(306, 3, null, null),
	    new Attribute(307, 3, null, null),
	    new Attribute(296, 3, null, null),
	    new Attribute(297, 3, null, null),
	    new Attribute(308, 3, null, null),
	    new Attribute(316, 3, null, null),
	    new Attribute(304, 3, null, null) };
    static final UserOption uXRayCollimator
	= new UserOption("U:X-Ray Collimator");
    static final ICondition ifCollimatorRectangular
	= new IfAnyEqual(360, "RECTANGULAR");
    static final ICondition ifCollimatorCircular
	= new IfAnyEqual(360, "CIRCULAR");
    static final ICondition ifCollimatorPolygonal
	= new IfAnyEqual(360, "POLYGONAL");
    static final Attribute[] collimatorModule
	= { new Attribute(360, 1, null,
			  new IfAllEqual(360, new String[] { "RECTANGULAR",
							     "CIRCULAR",
							     "POLYGONAL" })),
	    new Attribute(361, 1, ifCollimatorRectangular, null),
	    new Attribute(362, 1, ifCollimatorRectangular, null),
	    new Attribute(363, 1, ifCollimatorRectangular, null),
	    new Attribute(364, 1, ifCollimatorRectangular, null),
	    new Attribute(365, 1, ifCollimatorCircular, null),
	    new Attribute(366, 1, ifCollimatorCircular, null),
	    new Attribute(367, 1, ifCollimatorPolygonal, null) };
    static final UserOption uXRayTable = new UserOption("U:X-Ray Table");
    static final UserOption cImageCreatedWithTableMotion
	= new UserOption("C:Image Created With Table Motion");
    static final ICondition ifTableMotionDynamic = new IfEqual(284, "DYNAMIC");
    static final Attribute[] tableModule
	= { new Attribute(284, 2, null, null),
	    new Attribute(285, 2, ifTableMotionDynamic, null),
	    new Attribute(287, 2, ifTableMotionDynamic, null),
	    new Attribute(286, 2, ifTableMotionDynamic, null),
	    new Attribute(288, 3, null, null) };
    static final Attribute[] acquisitionDoseModule
	= { new Attribute(206, 3, null, null),
	    new Attribute(299, 3, null, null),
	    new Attribute(298, 3, null, null),
	    new Attribute(300, 3, null, null),
	    new Attribute(1195, 3, null, null),
	    new Attribute(277, 3, null, null),
	    new Attribute(278, 3, null, null),
	    new Attribute(304, 3, null, null),
	    new Attribute(1264, 3, null, null),
	    new Attribute(341, 3, null, null),
	    new Attribute(1219, 3, null, null),
	    new Attribute(1220, 3, null, null),
	    new Attribute(1221, 3, null, null),
	    new Attribute(1222, 3, null, null),
	    new Attribute(1315, 3, null, null),
	    new Attribute(1316, 3, null, null),
	    new Attribute(1317, 3, null, null),
	    new Attribute(1318, 3, null, null),
	    new Attribute(1263, 3, null, null),
	    new Attribute(1301, 3, null, null),
	    new Attribute(1302, 3, null, null),
	    new Attribute(1303, 3, null, null),
	    new Attribute(1262, 3, null, null) };
    static final Attribute[] generationModule
	= { new Attribute(206, 3, null, null),
	    new Attribute(299, 3, null, null),
	    new Attribute(298, 3, null, null),
	    new Attribute(300, 3, null, null),
	    new Attribute(1195, 3, null, null),
	    new Attribute(1304, 3, null, null),
	    new Attribute(1305, 3, null, null),
	    new Attribute(1306, 3, null, null),
	    new Attribute(1307, 3, null, null),
	    new Attribute(316, 3, null, null),
	    new Attribute(1263, 3, null, null),
	    new Attribute(1262, 3, null, null) };
    static final Attribute[] filtrationModule
	= { new Attribute(305, 3, null, null),
	    new Attribute(1301, 3, null, null),
	    new Attribute(1302, 3, null, null),
	    new Attribute(1303, 3, null, null) };
    static final Attribute[] gridModule
	= { new Attribute(309, 3, null, null),
	    new Attribute(1294, 3, null, null),
	    new Attribute(1295, 3, null, null),
	    new Attribute(1296, 3, null, null),
	    new Attribute(1297, 3, null, null),
	    new Attribute(1298, 3, null, null),
	    new Attribute(1299, 3, null, null),
	    new Attribute(1300, 3, null, null) };
}
