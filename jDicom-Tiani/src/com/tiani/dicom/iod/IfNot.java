/* IfNot - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfNot implements ICondition
{
    private ICondition arg;
    
    public IfNot(ICondition icondition) {
	arg = icondition;
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	return !arg.isTrue(dicomobject, icallbackuser);
    }
}
