/* IfEqual - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfEqual implements ICondition
{
    private int dname;
    private int index;
    private Object[] values;
    
    public IfEqual(int i, int i_0_, Object[] objects) {
	dname = i;
	index = i_0_;
	values = objects;
    }
    
    public IfEqual(int i, Object[] objects) {
	this(i, 0, objects);
    }
    
    public IfEqual(int i, int i_1_, Object object) {
	this(i, i_1_, new Object[] { object });
    }
    
    public IfEqual(int i, Object object) {
	this(i, 0, object);
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	Object object = dicomobject.get(dname, index);
	if (object != null) {
	    for (int i = 0; i < values.length; i++) {
		if (object.equals(values[i]))
		    return true;
	    }
	}
	return false;
    }
}
