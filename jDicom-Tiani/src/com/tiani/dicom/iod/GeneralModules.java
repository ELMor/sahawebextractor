/* GeneralModules - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

final class GeneralModules
{
    static final UserOption cSpecificCharacterSet
	= new UserOption("C:Specific Character Set");
    static final Attribute[] SOP_COMMON
	= { new Attribute(62, 1, null, null), new Attribute(63, 1, null, null),
	    new Attribute(57, 1, cSpecificCharacterSet, null),
	    new Attribute(59, 3, null, null), new Attribute(60, 3, null, null),
	    new Attribute(61, 3, null, null),
	    new Attribute(430, 3, null, null) };
    
    private GeneralModules() {
	/* empty */
    }
}
