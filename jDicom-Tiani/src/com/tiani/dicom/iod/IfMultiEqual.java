/* IfMultiEqual - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfMultiEqual implements ICondition
{
    private int dname;
    private Object[][] multiValues;
    
    public IfMultiEqual(int i, Object[][] objects) {
	dname = i;
	multiValues = objects;
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	int i = dicomobject.getSize(dname);
	if (i < multiValues.length)
	    return false;
    while_5_:
	for (int i_0_ = 0; i_0_ < multiValues.length; i_0_++) {
	    Object object = dicomobject.get(dname, i_0_);
	    Object[] objects = multiValues[i_0_];
	    for (int i_1_ = 0; i_1_ < objects.length; i_1_++) {
		if (objects[i_1_].equals(object))
		    continue while_5_;
	    }
	    return false;
	}
	return true;
    }
}
