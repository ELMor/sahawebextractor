/* CTImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class CTImageIOD
{
    static final Attribute[] imageModule
	= { new Attribute(58, 1, null, CommonImage.enumImageType),
	    new Attribute(461, 1, null, CommonImage.is1SamplePerPixel),
	    new Attribute(462, 1, null, CommonImage.isMonochrome),
	    new Attribute(475, 1, null, CommonImage.is16BitsAllocated),
	    new Attribute(476, 1, null, new IfInRange(476, 12, 16)),
	    new Attribute(477, 1, null,
			  CommonImage.isHighBitEqualsBitsStoredLess1),
	    new Attribute(489, 1, null, null),
	    new Attribute(490, 1, null, null),
	    new Attribute(206, 2, null, null),
	    new Attribute(429, 2, null, null),
	    new Attribute(185, 3, null, null),
	    new Attribute(276, 3, null, null),
	    new Attribute(277, 3, null, null),
	    new Attribute(278, 3, null, null),
	    new Attribute(280, 3, null, null),
	    new Attribute(282, 3, null, null),
	    new Attribute(289, 3, null,
			  new IfEqual(289, new String[] { "CW", "CC" })),
	    new Attribute(298, 3, null, null),
	    new Attribute(299, 3, null, null),
	    new Attribute(300, 3, null, null),
	    new Attribute(305, 3, null, null),
	    new Attribute(310, 3, null, null),
	    new Attribute(316, 3, null, null),
	    new Attribute(319, 3, null, null) };
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .frameOfReferenceModule),
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePlaneModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule,
					     CommonImage.cContrastBolusUsed),
	    new CompositeIOD.ModuleTableItem(imageModule, null),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.uOverlayPlane),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule,
					     LUTModules.uVOILUT),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated,
	    CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed,
	    OverlayModules.uOverlayPlane, LUTModules.uVOILUT,
	    GeneralModules.cSpecificCharacterSet };
}
