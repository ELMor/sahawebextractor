/* Attribute - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import java.util.StringTokenizer;
import java.util.Vector;

import com.archimed.dicom.DDate;
import com.archimed.dicom.DDateRange;
import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.Person;

public class Attribute
{
    private int _dname;
    private int _type;
    private ICondition _cond;
    private ICondition _check;
    private _IVRcheck _vrCheck;
    private static final _IVRcheck _AEcheck = new _StringLengthCheck(16);
    private static final _IVRcheck _AScheck = new _StringLengthCheck(4) {
	protected boolean validateString
	    (Attribute attribute, String string, Vector vector,
	     ICallbackUser icallbackuser, AttributeContext attributecontext) {
	    if (_isValidAS(string))
		return true;
	    vector.addElement(new IODError(6, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _CScheck = new _StringLengthCheck(16) {
	protected boolean validateString
	    (Attribute attribute, String string, Vector vector,
	     ICallbackUser icallbackuser, AttributeContext attributecontext) {
	    if (_isValidCS(string))
		return true;
	    vector.addElement(new IODError(7, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _DAcheck = new _IVRcheck() {
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_0_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    Object object = dicomobject.get(i, i_0_);
	    if (object instanceof DDate || object instanceof DDateRange)
		return true;
	    vector.addElement(new IODError(8, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _DScheck = new _IVRcheck() {
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_1_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    Object object = dicomobject.get(i, i_1_);
	    if (object instanceof Float)
		return true;
	    vector.addElement(new IODError(9, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _DTcheck = new _StringLengthCheck(24) {
	protected boolean validateString
	    (Attribute attribute, String string, Vector vector,
	     ICallbackUser icallbackuser, AttributeContext attributecontext) {
	    if (_isValidDT(string))
		return true;
	    vector.addElement(new IODError(10, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _FDcheck = new _IVRcheck() {
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_2_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    Object object = dicomobject.get(i, i_2_);
	    if (object instanceof Double)
		return true;
	    vector.addElement(new IODError(11, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _FLcheck = new _IVRcheck() {
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_3_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    Object object = dicomobject.get(i, i_3_);
	    if (object instanceof Float)
		return true;
	    vector.addElement(new IODError(12, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _IScheck = new _IVRcheck() {
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_4_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    Object object = dicomobject.get(i, i_4_);
	    if (object instanceof Integer)
		return true;
	    vector.addElement(new IODError(13, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _LOcheck = new _StringLengthCheck(64);
    private static final _IVRcheck _LTcheck = new _StringLengthCheck(10240);
    private static final _IVRcheck _PNcheck = new _IVRcheck() {
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_5_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    Object object = dicomobject.get(i, i_5_);
	    if (object instanceof Person)
		return true;
	    vector.addElement(new IODError(14, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _SHcheck = new _StringLengthCheck(16);
    private static final _IVRcheck _SLcheck
	= new _LongRangeCheck(-2147483648L, 2147483647L);
    private static final _IVRcheck _SScheck
	= new _IntRangeCheck(-32768, 32767);
    private static final _IVRcheck _STcheck = new _StringLengthCheck(1024);
    private static final _IVRcheck _TMcheck = new _StringLengthCheck(16) {
	protected boolean validateString
	    (Attribute attribute, String string, Vector vector,
	     ICallbackUser icallbackuser, AttributeContext attributecontext) {
	    if (_isValidTM(string))
		return true;
	    vector.addElement(new IODError(15, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _UIcheck = new _StringLengthCheck(64) {
	protected boolean validateString
	    (Attribute attribute, String string, Vector vector,
	     ICallbackUser icallbackuser, AttributeContext attributecontext) {
	    if (_isValidUI(string))
		return true;
	    vector.addElement(new IODError(16, attribute, attributecontext));
	    return false;
	}
    };
    private static final _IVRcheck _ULcheck
	= new _LongRangeCheck(0L, 4294967295L);
    private static final _IVRcheck _UScheck = new _IntRangeCheck(0, 65535);
    private static final _IVRcheck _USSScheck
	= new _IntRangeCheck(-32768, 65535);
    
    private static class _LongRangeCheck implements _IVRcheck
    {
	private long _minVal;
	private long _maxVal;
	
	protected _LongRangeCheck(long l, long l_6_) {
	    _minVal = l;
	    _maxVal = l_6_;
	}
	
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_7_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    Object object = dicomobject.get(i, i_7_);
	    if (object instanceof Long) {
		long l = ((Long) object).longValue();
		if (l >= _minVal && l <= _maxVal)
		    return true;
		vector.addElement(new IODError(4, attribute,
					       attributecontext));
		return false;
	    }
	    vector.addElement(new IODError(5, attribute, attributecontext));
	    return false;
	}
    }
    
    private static class _IntRangeCheck implements _IVRcheck
    {
	private int _minVal;
	private int _maxVal;
	
	protected _IntRangeCheck(int i, int i_8_) {
	    _minVal = i;
	    _maxVal = i_8_;
	}
	
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_9_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    int i_10_ = dicomobject.getI(i, i_9_);
	    if (i_10_ >= _minVal && i_10_ <= _maxVal)
		return true;
	    vector.addElement(new IODError(4, attribute, attributecontext));
	    return false;
	}
    }
    
    private static class _StringLengthCheck implements _IVRcheck
    {
	private int _maxLen;
	
	protected _StringLengthCheck(int i) {
	    _maxLen = i;
	}
	
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_11_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    String string = dicomobject.getS(i, i_11_);
	    boolean bool = true;
	    int i_12_ = string.length();
	    if (i_12_ > _maxLen) {
		vector.addElement(new IODError(3, attribute,
					       attributecontext));
		bool = false;
	    }
	    if (!validateString(string, vector, icallbackuser,
				attributecontext))
		bool = false;
	    return bool;
	}
	
	protected boolean validateString(String string, Vector vector,
					 ICallbackUser icallbackuser,
					 AttributeContext attributecontext) {
	    return true;
	}
    }
    
    private static class SQcheck implements _IVRcheck
    {
	private Attribute[] _itemAttribs;
	
	public SQcheck(Attribute[] attributes) {
	    _itemAttribs = attributes;
	}
	
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_13_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException {
	    DicomObject dicomobject_14_
		= (DicomObject) dicomobject.get(i, i_13_);
	    if (dicomobject_14_.numberOfElements() == 0) {
		vector.addElement(new IODError(17, attribute,
					       attributecontext));
		return false;
	    }
	    boolean bool = true;
	    AttributeContext attributecontext_15_
		= new AttributeContext(attributecontext, i, i_13_);
	    for (int i_16_ = 0; i_16_ < _itemAttribs.length; i_16_++) {
		if (!_itemAttribs[i_16_].validate(dicomobject_14_, vector,
						  icallbackuser,
						  attributecontext_15_))
		    bool = false;
	    }
	    return bool;
	}
    }
    
    private static interface _IVRcheck
    {
	public boolean validate
	    (Attribute attribute, DicomObject dicomobject, int i, int i_17_,
	     Vector vector, ICallbackUser icallbackuser,
	     AttributeContext attributecontext)
	    throws DicomException;
    }
    
    public Attribute(int i, int i_18_, ICondition icondition,
		     ICondition icondition_19_) {
	_dname = i;
	_type = i_18_;
	_cond = icondition;
	_check = icondition_19_;
	_vrCheck = _getVRcheck(i);
    }
    
    public Attribute(int i, int i_20_, ICondition icondition,
		     ICondition icondition_21_, Attribute[] attributes) {
	_dname = i;
	_type = i_20_;
	_cond = icondition;
	_check = icondition_21_;
	_vrCheck = new SQcheck(attributes);
    }
    
    public final int dname() {
	return _dname;
    }
    
    public final int type() {
	return _type;
    }
    
    public final ICondition cond() {
	return _cond;
    }
    
    public final int[] getAT() {
	return new int[] { DDict.getGroup(_dname), DDict.getElement(_dname) };
    }
    
    public String toString() {
	StringBuffer stringbuffer
	    = new StringBuffer(getTagDescription(_dname));
	stringbuffer.append(", type");
	stringbuffer.append(_type);
	if (_cond != null)
	    stringbuffer.append('C');
	return stringbuffer.toString();
    }
    
    static String getTagDescription(int i) {
	StringBuffer stringbuffer = new StringBuffer(DDict.getDescription(i));
	stringbuffer.append('(');
	stringbuffer.append(toHexString(DDict.getGroup(i), 4));
	stringbuffer.append(',');
	stringbuffer.append(toHexString(DDict.getElement(i), 4));
	stringbuffer.append(')');
	return stringbuffer.toString();
    }
    
    static String toHexString(int i, int i_22_) {
	String string = Integer.toHexString(i);
	StringBuffer stringbuffer = new StringBuffer(i_22_);
	for (int i_23_ = string.length(); i_23_ < i_22_; i_23_++)
	    stringbuffer.append('0');
	stringbuffer.append(string);
	return stringbuffer.toString();
    }
    
    private static _IVRcheck _getVRcheck(int i) {
	switch (DDict.getTypeCode(i)) {
	case 4:
	    return _AEcheck;
	case 17:
	    return _AScheck;
	case 5:
	    return null;
	case 9:
	    return _CScheck;
	case 11:
	    return _DAcheck;
	case 16:
	    return _DScheck;
	case 28:
	    return _DTcheck;
	case 20:
	    return _FDcheck;
	case 26:
	    return _FLcheck;
	case 15:
	    return _IScheck;
	case 6:
	    return _LOcheck;
	case 18:
	    return _LTcheck;
	case 8:
	    return null;
	case 24:
	    return null;
	case 14:
	    return _PNcheck;
	case 7:
	    return _SHcheck;
	case 19:
	    return _SLcheck;
	case 10:
	    return null;
	case 23:
	    return _SScheck;
	case 13:
	    return _STcheck;
	case 12:
	    return _TMcheck;
	case 2:
	    return _UIcheck;
	case 0:
	    return null;
	case 1:
	    return _ULcheck;
	case 3:
	    return _UScheck;
	case 27:
	    return null;
	case 25:
	    return null;
	case 21:
	    return _USSScheck;
	case 22:
	    return null;
	default:
	    return null;
	}
    }
    
    public boolean validate
	(DicomObject dicomobject, Vector vector, ICallbackUser icallbackuser,
	 AttributeContext attributecontext)
	throws DicomException {
	int i = dicomobject.getSize(_dname);
	switch (i) {
	case -1:
	    if (_type == 3
		|| _cond != null && !_cond.isTrue(dicomobject, icallbackuser))
		return true;
	    vector.addElement(new IODError(0, this, attributecontext));
	    return false;
	case 0:
	    if (_type != 1
		|| _cond != null && !_cond.isTrue(dicomobject, icallbackuser))
		return true;
	    vector.addElement(new IODError(1, this, attributecontext));
	    return false;
	default: {
	    boolean bool = true;
	    if (_vrCheck != null) {
		for (int i_24_ = 0; i_24_ < i; i_24_++) {
		    if (!_vrCheck.validate(this, dicomobject, _dname, i_24_,
					   vector, icallbackuser,
					   attributecontext))
			bool = false;
		}
	    }
	    if (_check != null && !_check.isTrue(dicomobject, icallbackuser)) {
		vector.addElement(new IODError(2, this, attributecontext));
		bool = false;
	    }
	    return bool;
	}
	}
    }
    
    private static boolean _areDigits(String string) {
	int i = string.length();
	for (int i_25_ = 0; i_25_ < i; i_25_++) {
	    if (!Character.isDigit(string.charAt(i_25_)))
		return false;
	}
	return true;
    }
    
    private static boolean _isValidAS(String string) {
	if (string.length() == 4 && Character.isDigit(string.charAt(0))
	    && Character.isDigit(string.charAt(1))
	    && Character.isDigit(string.charAt(2))) {
	    switch (string.charAt(3)) {
	    case 'D':
	    case 'M':
	    case 'W':
	    case 'Y':
		return true;
	    }
	}
	return false;
    }
    
    private static boolean _isValidCS(String string) {
	int i = string.length();
	for (int i_26_ = 0; i_26_ < i; i_26_++) {
	    char c = string.charAt(i_26_);
	    if (c == ' ' && c != '_' && !Character.isDigit(c)
		&& !Character.isUpperCase(c))
		return false;
	}
	return true;
    }
    
    private static boolean _isValidDA(String string) {
	boolean bool = false;
	boolean bool_27_ = false;
	boolean bool_28_ = false;
	try {
	    int i;
	    int i_29_;
	    switch (string.length()) {
	    case 8: {
		int i_30_ = Integer.parseInt(string.substring(0, 4));
		i = Integer.parseInt(string.substring(4, 6));
		i_29_ = Integer.parseInt(string.substring(6, 8));
		break;
	    }
	    case 10: {
		if (string.charAt(4) != '.' || string.charAt(8) != '.')
		    return false;
		int i_31_ = Integer.parseInt(string.substring(0, 4));
		i = Integer.parseInt(string.substring(5, 7));
		i_29_ = Integer.parseInt(string.substring(8, 10));
		break;
	    }
	    default:
		return false;
	    }
	    return i > 0 && i <= 12 && i_29_ > 0 && i_29_ <= 31;
	} catch (NumberFormatException numberformatexception) {
	    return false;
	}
    }
    
    private static boolean _isValidDT(String string) {
	switch (string.length()) {
	case 26: {
	    char c = string.charAt(21);
	    if (c != '+' && c != '-')
		return false;
	    if (!_isValidTMpart1(string.substring(22)))
		return false;
	}
	    /* fall through */
	case 21:
	    return (_isValidDA(string.substring(0, 8))
		    && _isValidTM(string.substring(8, 21)));
	default:
	    return false;
	}
    }
    
    private static boolean _isValidTM(String string) {
	StringTokenizer stringtokenizer = new StringTokenizer(string, ".");
	switch (stringtokenizer.countTokens()) {
	case 2:
	    return (_isValidTMpart1(stringtokenizer.nextToken())
		    && _areDigits(stringtokenizer.nextToken()));
	case 1:
	    return _isValidTMpart1(stringtokenizer.nextToken());
	default:
	    return false;
	}
    }
    
    private static boolean _isValidTMpart1(String string) {
	boolean bool = false;
	int i = 0;
	int i_32_ = 0;
	try {
	    int i_33_;
	    if (string.indexOf(':') == -1) {
		switch (string.length()) {
		case 6:
		    i_32_ = Integer.parseInt(string.substring(4, 6));
		    /* fall through */
		case 4:
		    i = Integer.parseInt(string.substring(2, 4));
		    /* fall through */
		case 2:
		    i_33_ = Integer.parseInt(string.substring(0, 2));
		    break;
		default:
		    return false;
		}
	    } else {
		switch (string.length()) {
		case 8:
		    if (string.charAt(5) != ':')
			return false;
		    i_32_ = Integer.parseInt(string.substring(6, 8));
		    /* fall through */
		case 5:
		    if (string.charAt(2) != ':')
			return false;
		    i = Integer.parseInt(string.substring(3, 5));
		    /* fall through */
		case 2:
		    i_33_ = Integer.parseInt(string.substring(0, 2));
		    break;
		default:
		    return false;
		}
	    }
	    return i_33_ < 24 && i < 60 && i_32_ < 60;
	} catch (NumberFormatException numberformatexception) {
	    return false;
	}
    }
    
    private static boolean _isValidUI(String string) {
	if (string.charAt(0) == '.'
	    || string.charAt(string.length() - 1) == '.'
	    || string.indexOf("..") != -1)
	    return false;
	StringTokenizer stringtokenizer = new StringTokenizer(string, ".");
	if (!stringtokenizer.hasMoreTokens())
	    return false;
	do {
	    if (!_isValidUIpart(stringtokenizer.nextToken()))
		return false;
	} while (stringtokenizer.hasMoreTokens());
	return true;
    }
    
    private static boolean _isValidUIpart(String string) {
	return ((string.charAt(0) != '0' || string.length() == 1)
		&& _areDigits(string));
    }
}
