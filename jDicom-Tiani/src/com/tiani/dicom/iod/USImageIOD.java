/* USImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class USImageIOD extends USImageCommonIOD
{
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .frameOfReferenceModule),
					     CommonImage.uFrameOfReference),
	    new CompositeIOD.ModuleTableItem((USImageCommonIOD
					      .frameOfReferenceModule),
					     (USImageCommonIOD
					      .cImagesSpatiallyRelated)),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule,
					     CommonImage.cContrastBolusUsed),
	    new CompositeIOD.ModuleTableItem(CommonImage.paletteColorLUTModule,
					     new IfEqual(462, 0,
							 "PALETTE COLOR")),
	    new CompositeIOD.ModuleTableItem((USImageCommonIOD
					      .regionCalibrationModule),
					     (USImageCommonIOD
					      .uUSRegionCalibration)),
	    new CompositeIOD.ModuleTableItem(USImageCommonIOD.imageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.uOverlayPlane),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule,
					     LUTModules.uVOILUT),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    static final CompositeIOD.ModuleTableItem[] moduleTableCurve
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .frameOfReferenceModule),
					     CommonImage.uFrameOfReference),
	    new CompositeIOD.ModuleTableItem((USImageCommonIOD
					      .frameOfReferenceModule),
					     (USImageCommonIOD
					      .cImagesSpatiallyRelated)),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem((CurveModules
					      .curveIdentificationModule),
					     null),
	    new CompositeIOD.ModuleTableItem(CurveModules.curveModule, null),
	    new CompositeIOD.ModuleTableItem(CurveModules.audioModule,
					     CurveModules.uAudio),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, CommonImage.uFrameOfReference,
	    USImageCommonIOD.cImagesSpatiallyRelated,
	    CommonImage.cImagesTemporallyRelated,
	    CommonImage.cLossyImageCompression,
	    CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed,
	    USImageCommonIOD.uUSRegionCalibration,
	    USImageCommonIOD.cUSComponentCalibration,
	    USImageCommonIOD.cImageAcquiredInStageProtocol,
	    OverlayModules.uOverlayPlane, LUTModules.uVOILUT,
	    CurveModules.uAudio, GeneralModules.cSpecificCharacterSet };
    
    private USImageIOD() {
	/* empty */
    }
}
