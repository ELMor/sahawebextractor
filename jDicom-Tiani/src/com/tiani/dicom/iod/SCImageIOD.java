/* SCImageIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class SCImageIOD
{
    static final Attribute[] equipmentModule
	= { new Attribute(83, 1, null, null), new Attribute(81, 3, null, null),
	    new Attribute(230, 3, null, null),
	    new Attribute(233, 3, null, null),
	    new Attribute(234, 3, null, null),
	    new Attribute(235, 3, null, null),
	    new Attribute(237, 3, null, null),
	    new Attribute(238, 3, null, null) };
    static final Attribute[] imageModule
	= { new Attribute(231, 3, null, null),
	    new Attribute(232, 3, null, null) };
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     CommonImage.uGeneralEquipment),
	    new CompositeIOD.ModuleTableItem(equipmentModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule,
					     null),
	    new CompositeIOD.ModuleTableItem(imageModule, null),
	    new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule,
					     OverlayModules.uOverlayPlane),
	    new CompositeIOD.ModuleTableItem(LUTModules.modalityLUTModule,
					     LUTModules.uModalityLUT),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule,
					     LUTModules.uVOILUT),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, CommonImage.uGeneralEquipment,
	    CommonImage.cImagesTemporallyRelated,
	    CommonImage.cPixelAspectRatioNot11, OverlayModules.uOverlayPlane,
	    LUTModules.uModalityLUT, LUTModules.uVOILUT,
	    GeneralModules.cSpecificCharacterSet };
    
    private SCImageIOD() {
	/* empty */
    }
}
