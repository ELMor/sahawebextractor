/* StandaloneVOILUTIOD - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;

public final class StandaloneVOILUTIOD
{
    static final CompositeIOD.ModuleTableItem[] moduleTable
	= { new CompositeIOD.ModuleTableItem(CommonImage.patientModule, null),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule,
					     null),
	    new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule,
					     CommonImage.uPatientStudy),
	    new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule,
					     null),
	    new CompositeIOD.ModuleTableItem((CommonImage
					      .generalEquipmentModule),
					     null),
	    new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, null),
	    new CompositeIOD.ModuleTableItem((LUTModules
					      .LUTIdentificationModule),
					     null),
	    new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON,
					     null) };
    public static final UserOption[] userOptions
	= { CommonImage.uPatientStudy, GeneralModules.cSpecificCharacterSet };
    
    private StandaloneVOILUTIOD() {
	/* empty */
    }
}
