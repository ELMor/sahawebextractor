/* IfInRange - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfInRange implements ICondition
{
    private int dname;
    private int index;
    private int min;
    private int max;
    
    public IfInRange(int i, int i_0_, int i_1_, int i_2_) {
	dname = i;
	index = i_0_;
	min = i_1_;
	max = i_2_;
    }
    
    public IfInRange(int i, int i_3_, int i_4_) {
	this(i, 0, i_3_, i_4_);
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	int i = dicomobject.getI(dname, index);
	return i >= min && i <= max;
    }
}
