/* IfSizeInRange - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.iod;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

final class IfSizeInRange implements ICondition
{
    private int dname;
    private int min;
    private int max;
    
    public IfSizeInRange(int i, int i_0_, int i_1_) {
	dname = i;
	min = i_0_;
	max = i_1_;
    }
    
    public boolean isTrue
	(DicomObject dicomobject, ICallbackUser icallbackuser)
	throws DicomException {
	int i = dicomobject.getSize(dname);
	return i >= min && i <= max;
    }
}
