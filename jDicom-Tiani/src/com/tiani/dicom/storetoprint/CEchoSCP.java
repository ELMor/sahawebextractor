/* CEchoSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.storetoprint;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;

final class CEchoSCP extends DefCEchoSCP
{
    private PrintSCURelay _relay;
    
    public CEchoSCP(PrintSCURelay printscurelay) {
	_relay = printscurelay;
    }
    
    public void handleRQ
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	_relay.echo(dimseexchange);
	super.handleRQ(dimseexchange, i, string, dicommessage);
    }
}
