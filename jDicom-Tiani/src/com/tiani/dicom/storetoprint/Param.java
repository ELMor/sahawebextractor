/* Param - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.storetoprint;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

import com.archimed.dicom.Debug;
import com.tiani.dicom.util.CheckParam;

final class Param
{
    public static final String[] KEYS
	= { "Port", "MultiThreadTCP", "MultiThreadAssoc", "MaxPduSize",
	    "AssocTimeout[ms]", "ReleaseTimeout[ms]", "Print.Host",
	    "Print.Port", "Print.Verification",
	    "Print.BasicGrayscalePrintManagement",
	    "Print.BasicColorPrintManagement", "Print.WarningAsFailure",
	    "Session.NumberOfCopies", "Session.PrintPriority",
	    "Session.MediumType", "Session.FilmDestination",
	    "Session.FilmSessionLabel", "Session.MemoryAllocation",
	    "Session.OwnerID", "Session.Illumination",
	    "Session.ReflectedAmbientLight", "FilmBox.ImageDisplayFormat",
	    "FilmBox.FilmOrientation", "FilmBox.FilmSizeID",
	    "FilmBox.MagnificationType", "FilmBox.SmoothingType",
	    "FilmBox.BorderDensity", "FilmBox.EmptyImageDensity",
	    "FilmBox.MinDensity", "FilmBox.MaxDensity", "FilmBox.Trim",
	    "FilmBox.ConfigurationInformation",
	    "FilmBox.RequestedResolutionID", "ImageBox.Polarity",
	    "ImageBox.RequestedDecimateCropBehavior",
	    "ImageBox.RequestedImageSize", "User.SendAspectRatio",
	    "User.RequestedZoom", "User.BurnInInfo",
	    "User.BurnInInfo.Properties", "User.BitDepth",
	    "User.InflateBitsAlloc", "User.MinMaxWindowing", "Verbose",
	    "DumpCmdsetIntoDir", "DumpDatasetIntoDir" };
    public static Hashtable CHECKS = new Hashtable();
    private static final String[] PRINT_PRIORITY
	= { "", "HIGH", "MED", "LOW" };
    private static final String[] MEDIUM_TYPE
	= { "", "PAPER", "CLEAR FILM", "BLUE FILM" };
    private static final String[] FILM_DESTINATION
	= { "", "MAGAZINE", "PROCESSOR", "BIN_1", "BIN_2", "BIN_3", "BIN_4",
	    "BIN_5", "BIN_6", "BIN_7", "BIN_8" };
    private static final String[] IMAGE_DISPLAY_FORMAT
	= { "STANDARD\\1,1", "STANDARD\\2,3", "ROW\\2", "COL\\2", "SLIDE",
	    "SUPERSLIDE", "CUSTOM\\1" };
    private static final String[] FILM_ORIENTATION
	= { "", "PORTRAIT", "LANDSCAPE" };
    private static final String[] FILM_SIZE_ID
	= { "", "8INX10IN", "10INX12IN", "10INX14IN", "11INX14IN", "14INX14IN",
	    "14INX17IN", "24CMX24CM", "24CMX30CM" };
    private static final String[] MAGNIFICATION_TYPE
	= { "", "REPLICATE", "BILINEAR", "CUBIC", "NONE" };
    private static final String[] DENSITY = { "", "BLACK", "WHITE" };
    private static final String[] YES_NO = { "", "YES", "NO" };
    private static final String[] REQUESTED_RESOLUTION_ID
	= { "", "STANDARD", "HIGH" };
    private static final String[] POLARITY = { "", "NORMAL", "REVERSE" };
    private static final String[] REQUESTED_DECIMATE_CROP_BEHAVIOR
	= { "", "DECIMATE", "CROP", "FAIL" };
    private static final String[] BURNIN_INFO
	= { "No", "IfNoOverlays", "Always" };
    private static final String[] SEND_ASPECTRATIO = { "Always", "IfNot1/1" };
    private static final String[] INFLATE_BIT_DEPTH
	= { "Always", "IfNonLinear", "Never" };
    private static final String[] VERBOSE = { "0", "1", "2", "3", "4", "5" };
    private final int _port;
    private final boolean _multiThreadTCP;
    private final boolean _multiThreadAssoc;
    private final int _assocTimeout;
    private final int _releaseTimeout;
    private final int _maxPduSize;
    private final int _printPort;
    private final boolean _verification;
    private final boolean _basicGrayscalePrintManagement;
    private final boolean _basicColorPrintManagement;
    private final boolean _warningAsFailure;
    private final int _requestedZoom;
    private final boolean sendAspectRatioAlways;
    private final int _burnInInfo;
    private final int bitDepth;
    private final int inflateBitsAlloc;
    private final boolean minMaxWindowing;
    private final int _verbose;
    private final Properties _prop;
    
    public Param(Properties properties) throws IllegalArgumentException {
	_prop = (Properties) properties.clone();
	CheckParam.verify(_prop, CHECKS);
	_port = Integer.parseInt(_prop.getProperty("Port"));
	_multiThreadTCP = parseBoolean(_prop.getProperty("MultiThreadTCP"));
	_multiThreadAssoc
	    = parseBoolean(_prop.getProperty("MultiThreadAssoc"));
	_maxPduSize = Integer.parseInt(_prop.getProperty("MaxPduSize"));
	_assocTimeout
	    = Integer.parseInt(_prop.getProperty("AssocTimeout[ms]"));
	_releaseTimeout
	    = Integer.parseInt(_prop.getProperty("ReleaseTimeout[ms]"));
	_printPort = Integer.parseInt(_prop.getProperty("Print.Port"));
	_verification = parseBoolean(_prop.getProperty("Print.Verification"));
	_basicGrayscalePrintManagement
	    = (parseBoolean
	       (_prop.getProperty("Print.BasicGrayscalePrintManagement")));
	_basicColorPrintManagement
	    = (parseBoolean
	       (_prop.getProperty("Print.BasicColorPrintManagement")));
	String string = _prop.getProperty("User.RequestedZoom");
	_requestedZoom = (string != null && string.length() > 0
			  ? Integer.parseInt(string) : 0);
	_warningAsFailure
	    = parseBoolean(_prop.getProperty("Print.WarningAsFailure"));
	sendAspectRatioAlways
	    = indexOf(SEND_ASPECTRATIO,
		      _prop.getProperty("User.SendAspectRatio")) == 0;
	_burnInInfo
	    = indexOf(BURNIN_INFO, _prop.getProperty("User.BurnInInfo"));
	bitDepth = Integer.parseInt(_prop.getProperty("User.BitDepth"));
	inflateBitsAlloc = indexOf(INFLATE_BIT_DEPTH,
				   _prop.getProperty("User.InflateBitsAlloc"));
	minMaxWindowing
	    = parseBoolean(_prop.getProperty("User.MinMaxWindowing"));
	_verbose = Integer.parseInt(_prop.getProperty("Verbose"));
	String string_0_ = _prop.getProperty("DumpCmdsetIntoDir");
	Debug.dumpCmdsetIntoDir
	    = string_0_ != null && string_0_.length() > 0 ? string_0_ : null;
	string_0_ = _prop.getProperty("DumpDatasetIntoDir");
	Debug.dumpDatasetIntoDir
	    = string_0_ != null && string_0_.length() > 0 ? string_0_ : null;
    }
    
    public String toString() {
	StringBuffer stringbuffer = new StringBuffer();
	stringbuffer.append("Param:\n");
	for (int i = 0; i < KEYS.length; i++)
	    stringbuffer.append(KEYS[i]).append('=').append
		(_prop.getProperty(KEYS[i])).append('\n');
	return stringbuffer.toString();
    }
    
    public String getPrintHost() {
	return _prop.getProperty("Print.Host");
    }
    
    public int getRequestedZoom() {
	return _requestedZoom;
    }
    
    public String getBurnInInfoProperties() {
	return _prop.getProperty("User.BurnInInfo.Properties");
    }
    
    public int getPort() {
	return _port;
    }
    
    public boolean isMultiThreadTCP() {
	return _multiThreadTCP;
    }
    
    public boolean isMultiThreadAssoc() {
	return _multiThreadAssoc;
    }
    
    public int getMaxPduSize() {
	return _maxPduSize;
    }
    
    public int getAssocTimeout() {
	return _assocTimeout;
    }
    
    public int getReleaseTimeout() {
	return _releaseTimeout;
    }
    
    public int getPrintPort() {
	return _printPort;
    }
    
    public boolean isVerification() {
	return _verification;
    }
    
    public boolean isBasicGrayscalePrintManagement() {
	return _basicGrayscalePrintManagement;
    }
    
    public boolean isBasicColorPrintManagement() {
	return _basicColorPrintManagement;
    }
    
    public boolean isWarningAsFailure() {
	return _warningAsFailure;
    }
    
    public boolean isSendAspectRatioAlways() {
	return sendAspectRatioAlways;
    }
    
    public int getBurnInInfo() {
	return _burnInInfo;
    }
    
    public int getBitDepth() {
	return bitDepth;
    }
    
    public int getInflateBitsAlloc() {
	return inflateBitsAlloc;
    }
    
    public boolean isMinMaxWindowing() {
	return minMaxWindowing;
    }
    
    public int getVerbose() {
	return _verbose;
    }
    
    public Properties getProperties() {
	return _prop;
    }
    
    private static String[] tokenize(String string, String string_1_) {
	StringTokenizer stringtokenizer
	    = new StringTokenizer(string, string_1_);
	String[] strings = new String[stringtokenizer.countTokens()];
	for (int i = 0; i < strings.length; i++)
	    strings[i] = stringtokenizer.nextToken();
	return strings;
    }
    
    private static boolean parseBoolean(String string) {
	return string != null && "true".compareTo(string.toLowerCase()) == 0;
    }
    
    public int[] getAbstractSyntaxes() {
	int[] is = new int[3];
	int i = 0;
	if (_verification)
	    is[i++] = 4097;
	if (_basicGrayscalePrintManagement)
	    is[i++] = 12292;
	if (_basicColorPrintManagement)
	    is[i++] = 12294;
	int[] is_2_ = new int[i];
	System.arraycopy(is, 0, is_2_, 0, i);
	return is_2_;
    }
    
    private int indexOf(String[] strings, String string) {
	int i = strings.length;
	while (i-- > 0 && !strings[i].equals(string)) {
	    /* empty */
	}
	return i;
    }
    
    static {
	CHECKS.put("Port", CheckParam.range(100, 65535));
	CHECKS.put("MultiThreadTCP", CheckParam.bool());
	CHECKS.put("MultiThreadAssoc", CheckParam.bool());
	CHECKS.put("MaxPduSize", CheckParam.range(0, 65535));
	CHECKS.put("AssocTimeout[ms]", CheckParam.range(0, 65535));
	CHECKS.put("ReleaseTimeout[ms]", CheckParam.range(0, 65535));
	CHECKS.put("Print.Port", CheckParam.range(100, 65535));
	CHECKS.put("Print.Verification", CheckParam.bool());
	CHECKS.put("Print.BasicGrayscalePrintManagement", CheckParam.bool());
	CHECKS.put("Print.BasicColorPrintManagement", CheckParam.bool());
	CHECKS.put("Print.WarningAsFailure", CheckParam.bool());
	CHECKS.put("Session.PrintPriority",
		   CheckParam.defined(PRINT_PRIORITY, 3));
	CHECKS.put("Session.MediumType", CheckParam.defined(MEDIUM_TYPE, 3));
	CHECKS.put("Session.FilmDestination",
		   CheckParam.defined(FILM_DESTINATION, 3));
	CHECKS.put("FilmBox.ImageDisplayFormat",
		   CheckParam.defined(IMAGE_DISPLAY_FORMAT, 1));
	CHECKS.put("FilmBox.FilmOrientation",
		   CheckParam.enum(FILM_ORIENTATION, 3));
	CHECKS.put("FilmBox.FilmSizeID", CheckParam.defined(FILM_SIZE_ID, 3));
	CHECKS.put("FilmBox.MagnificationType",
		   CheckParam.defined(MAGNIFICATION_TYPE, 3));
	CHECKS.put("FilmBox.BorderDensity", CheckParam.defined(DENSITY, 3));
	CHECKS.put("FilmBox.EmptyImageDensity",
		   CheckParam.defined(DENSITY, 3));
	CHECKS.put("FilmBox.Trim", CheckParam.enum(YES_NO, 3));
	CHECKS.put("FilmBox.RequestedResolutionID",
		   CheckParam.defined(REQUESTED_RESOLUTION_ID, 3));
	CHECKS.put("ImageBox.Polarity", CheckParam.enum(POLARITY, 3));
	CHECKS.put("ImageBox.RequestedDecimateCropBehavior",
		   CheckParam.enum(REQUESTED_DECIMATE_CROP_BEHAVIOR, 3));
	CHECKS.put("User.SendAspectRatio", CheckParam.enum(SEND_ASPECTRATIO));
	CHECKS.put("User.RequestedZoom", CheckParam.range(10, 400, 3));
	CHECKS.put("User.BurnInInfo", CheckParam.enum(BURNIN_INFO));
	CHECKS.put("User.BitDepth", CheckParam.range(8, 16));
	CHECKS.put("User.InflateBitsAlloc",
		   CheckParam.enum(INFLATE_BIT_DEPTH));
	CHECKS.put("User.MinMaxWindowing", CheckParam.bool());
	CHECKS.put("Verbose", CheckParam.enum(VERBOSE));
    }
}
