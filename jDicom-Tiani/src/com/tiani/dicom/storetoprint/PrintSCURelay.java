/* PrintSCURelay - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.storetoprint;
import java.io.IOException;
import java.util.Hashtable;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAcceptancePolicy;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.VerboseAssociation;

class PrintSCURelay implements IAcceptancePolicy, IAssociationListener
{
    private Param _param;
    private Hashtable _printMgtSCUs = new Hashtable();
    private static final int[] _abstractSyntaxIDs
	= { 4097, 4118, 4119, 4120, 4121, 4122, 4123, 4128, 4129, 4131, 4151,
	    4157, 4158, 4159, 4153, 4161, 4162, 4163, 4164, 4176, 4177, 4178,
	    4179, 4180, 4181 };
    private static int _transferSyntaxID = 8193;
    private static boolean _rejectnocontext = true;
    
    public PrintSCURelay(Param param) {
	_param = param;
    }
    
    public void setParams(Param param) {
	_param = param;
    }
    
    public Response prepareResponse
	(VerboseAssociation verboseassociation, Request request)
	throws IllegalValueException {
	Response response = ResponsePolicy.prepareResponse(request, null, null,
							   _abstractSyntaxIDs,
							   _transferSyntaxID,
							   _rejectnocontext);
	if (!(response instanceof Acknowledge))
	    return response;
	try {
	    PrintMgtSCU printmgtscu = new PrintMgtSCU(_param);
	    printmgtscu.setMaxPduSize(_param.getMaxPduSize());
	    printmgtscu.setARTIM(_param.getReleaseTimeout());
	    Response response_0_ = printmgtscu.open(request);
	    if (!(response_0_ instanceof Acknowledge))
		return response_0_;
	    _printMgtSCUs.put(verboseassociation, printmgtscu);
	    ((Acknowledge) response).setMaxPduSize(_param.getMaxPduSize());
	    ((Acknowledge) response).setMaxOperationsInvoked
		(((Acknowledge) response_0_).getMaxOperationsInvoked());
	    return response;
	} catch (Exception exception) {
	    Debug.out.println(exception);
	    return new Abort(Abort.DICOM_UL_SERVICE_USER,
			     Abort.REASON_NOT_SPECIFIED);
	}
    }
    
    public void echo(DimseExchange dimseexchange)
	throws IOException, IllegalValueException, DicomException {
	PrintMgtSCU printmgtscu
	    = (PrintMgtSCU) _printMgtSCUs.get(dimseexchange.getAssociation());
	if (printmgtscu.isEnabled(4097)) {
	    try {
		printmgtscu.echo();
	    } catch (Exception exception) {
		Debug.out.println(exception);
	    }
	}
    }
    
    public int store(DimseExchange dimseexchange, String string,
		     String string_1_, DicomMessage dicommessage,
		     DicomMessage dicommessage_2_) {
	return ((PrintMgtSCU)
		_printMgtSCUs.get(dimseexchange.getAssociation()))
		   .store(string, string_1_, dicommessage, dicommessage_2_);
    }
    
    public void associateRequestReceived(VerboseAssociation verboseassociation,
					 Request request) {
	/* empty */
    }
    
    public void associateResponseReceived
	(VerboseAssociation verboseassociation, Response response) {
	/* empty */
    }
    
    public void associateRequestSent(VerboseAssociation verboseassociation,
				     Request request) {
	/* empty */
    }
    
    public void associateResponseSent(VerboseAssociation verboseassociation,
				      Response response) {
	/* empty */
    }
    
    public void releaseRequestReceived(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseReceived
	(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseRequestSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void abortReceived(VerboseAssociation verboseassociation,
			      Abort abort) {
	/* empty */
    }
    
    public void abortSent(VerboseAssociation verboseassociation, int i,
			  int i_3_) {
	/* empty */
    }
    
    public void socketClosed(VerboseAssociation verboseassociation) {
	PrintMgtSCU printmgtscu
	    = (PrintMgtSCU) _printMgtSCUs.remove(verboseassociation);
	if (printmgtscu != null) {
	    try {
		printmgtscu.close();
	    } catch (Exception exception) {
		Debug.out.println(exception);
	    }
	}
    }
}
