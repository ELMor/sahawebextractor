/* StoreToPrintException - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.storetoprint;

final class StoreToPrintException extends Exception
{
    static final int NO_ASSOCIATION = 42752;
    static final int NO_GRAYSCALE_PRINTMGT = 42753;
    static final int NO_COLOR_PRINTMGT = 42754;
    static final int PRINTER_ERROR = 42755;
    static final int PRINTER_WARNING = 42756;
    static final int PRINTER_FAILURE = 42757;
    static final int FILMSESSION_ERROR = 42758;
    static final int FILMBOX_ERROR = 42759;
    static final int IMAGEBOX_ERROR = 42760;
    static final int CATCH_EXCEPTION = 43007;
    private final int _status;
    
    public StoreToPrintException(String string, int i) {
	super(string);
	_status = i;
    }
    
    public int getStatus() {
	return _status;
    }
}
