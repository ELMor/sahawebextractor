/* Overlay - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.overlay;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public final class Overlay
{
    private final int _rows;
    private final int _columns;
    private final int _originRow;
    private final int _originColumn;
    private final int _bitsAllocated;
    private final int _bitPosition;
    private final boolean _imbedded;
    private final int _bytePos;
    private final int _bitmask;
    private final byte[] _data;
    
    public Overlay(DicomObject dicomobject, int i) throws DicomException {
	_rows = dicomobject.getI_ge(i, 16);
	_columns = dicomobject.getI_ge(i, 17);
	_originRow = dicomobject.getI_ge(i, 80, 0);
	_originColumn = dicomobject.getI_ge(i, 80, 1);
	_bitsAllocated = dicomobject.getI_ge(i, 256);
	_bitPosition = dicomobject.getI_ge(i, 258);
	byte[] is = (byte[]) dicomobject.get_ge(i, 12288);
	_data = (_imbedded = is == null) ? (byte[]) dicomobject.get(1184) : is;
	if (_bitsAllocated != 1 && _bitsAllocated != 8 && _bitsAllocated != 16)
	    throw new DicomException("Overlay with " + _bitsAllocated
				     + " bits allocated not supported");
	if (_imbedded && dicomobject.getI(461) > 1)
	    throw new DicomException
		      ("Overlay imbedded into mulit banded pixel data not supported");
	_bytePos = _bitPosition / 8;
	_bitmask = 1 << _bitPosition % 8;
    }
    
    public int getRows() {
	return _rows;
    }
    
    public int getColumns() {
	return _columns;
    }
    
    public int getOriginRow() {
	return _originRow;
    }
    
    public int getOriginColumn() {
	return _originColumn;
    }
    
    public boolean isOverlayAt(int i, int i_0_) {
	if (i < 0 || i >= _rows || i_0_ < 0 || i_0_ >= _columns)
	    return false;
	int i_1_ = i * _columns + i_0_;
	switch (_bitsAllocated) {
	case 1:
	    return (_data[i_1_ / 8] & 1 << (i_1_ & 0x7)) != 0;
	case 8:
	    return (_data[i_1_] & _bitmask) != 0;
	case 16:
	    return (_data[i_1_ * 2 + _bytePos] & _bitmask) != 0;
	default:
	    return false;
	}
    }
    
    public static int[] listOverlayGroups(DicomObject dicomobject)
	throws DicomException {
	int[] is = new int[16];
	int i = 0;
	int i_2_ = 0;
	int i_3_ = 24576;
	while (i < is.length) {
	    if (dicomobject.getSize_ge(i_3_, 16) > 0)
		is[i_2_++] = i_3_;
	    i++;
	    i_3_++;
	    i_3_++;
	}
	int[] is_4_ = new int[i_2_];
	System.arraycopy(is, 0, is_4_, 0, i_2_);
	return is_4_;
    }
    
    public static int getFreeOverlayGroup(DicomObject dicomobject)
	throws DicomException {
	for (int i = 24576; i < 24832; i += 2) {
	    if (!dicomobject.containsGroup(i))
		return i;
	}
	return -1;
    }
}
