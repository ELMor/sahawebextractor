/* BurnInOverlay - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.overlay;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.EnumPMI;

public class BurnInOverlay
{
    private DicomObject _dcmObj;
    private int _rows;
    private int _columns;
    private int _pixelRepresentation;
    private int _planarConfiguration;
    private int _pmi;
    private int _samplesPerPixel;
    private int _sampleGap;
    private int _bitsAllocated;
    private int _bitsStored;
    private int _highBit;
    private byte[] _pixeldata;
    private int _bytesAllocated;
    private int _bandOffset;
    private int _bytesPerCell;
    private byte _whiteLoByte;
    private byte _whiteHiByte;
    
    public static void burnInAll(DicomObject dicomobject)
	throws DicomException {
	int[] is = Overlay.listOverlayGroups(dicomobject);
	if (is.length != 0) {
	    BurnInOverlay burninoverlay = new BurnInOverlay(dicomobject);
	    for (int i = 0; i < is.length; i++)
		burninoverlay.burnIn(is[i]);
	}
    }
    
    public BurnInOverlay(DicomObject dicomobject) throws DicomException {
	setDicomObject(dicomobject);
    }
    
    public DicomObject getDicomObject() {
	return _dcmObj;
    }
    
    public void setDicomObject(DicomObject dicomobject) throws DicomException {
	_dcmObj = dicomobject;
	_rows = dicomobject.getI(466);
	_columns = dicomobject.getI(467);
	_pixelRepresentation = dicomobject.getI(478);
	_pmi = EnumPMI.getConstant(dicomobject.getS(462));
	_samplesPerPixel = dicomobject.getI(461);
	_planarConfiguration
	    = _samplesPerPixel > 1 ? dicomobject.getI(463) : 0;
	_bitsAllocated = dicomobject.getI(475);
	_bitsStored = dicomobject.getI(476);
	_highBit = dicomobject.getI(477);
	_pixeldata = (byte[]) dicomobject.get(1184);
	if (_samplesPerPixel != EnumPMI.SAMPLES[_pmi])
	    throw new DicomException
		      ("Samples Per Pixel does not match Photometric Interpretion");
	if (_pmi != 0 && _pmi != 1 && _pmi != 3)
	    throw new DicomException
		      ("Photometric Interpretion not support by current version of BurnInOverlay");
	if (_bitsAllocated != 8 && _bitsAllocated != 16)
	    throw new DicomException
		      ("" + _bitsAllocated
		       + "bits allocated not support by current version of BurnInOverlay");
	_bytesAllocated = _bitsAllocated / 8;
	_bandOffset = ((_planarConfiguration == 0 ? 1 : _rows * _columns)
		       * _bytesAllocated);
	_bytesPerCell = ((_planarConfiguration == 0 ? _samplesPerPixel : 1)
			 * _bytesAllocated);
	int i = (1 << _bitsStored) - 1;
	int i_0_ = 0;
	if (_pixelRepresentation != 0) {
	    i >>= 1;
	    i_0_ = -(i + 1);
	}
	int i_1_ = _pmi == 0 ? i_0_ : i;
	i_1_ <<= _bitsStored - _highBit - 1;
	_whiteLoByte = (byte) i_1_;
	_whiteHiByte = (byte) (i_1_ >> 8);
    }
    
    public void burnIn(int i) throws DicomException {
	Overlay overlay = new Overlay(_dcmObj, i);
	int i_2_ = overlay.getOriginRow() - 1;
	int i_3_ = overlay.getOriginColumn() - 1;
	int i_4_ = overlay.getRows();
	int i_5_ = overlay.getColumns();
	for (int i_6_ = 0; i_6_ < i_4_; i_6_++) {
	    for (int i_7_ = 0; i_7_ < i_5_; i_7_++) {
		if (overlay.isOverlayAt(i_6_, i_7_)) {
		    int i_8_ = (((i_2_ + i_6_) * _columns + (i_3_ + i_7_))
				* _bytesPerCell);
		    int i_9_ = 0;
		    while (i_9_ < _samplesPerPixel) {
			_pixeldata[i_8_] = _whiteLoByte;
			if (_bytesAllocated == 2)
			    _pixeldata[i_8_ + 1] = _whiteHiByte;
			i_9_++;
			i_8_ += _bandOffset;
		    }
		}
	    }
	}
	_dcmObj.removeGroup(i);
    }
}
