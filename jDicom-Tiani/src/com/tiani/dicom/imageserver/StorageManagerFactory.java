/* StorageManagerFactory - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.media.FileSet2;
import com.tiani.dicom.util.UIDUtils;

public class StorageManagerFactory
{
    private Param param;
    private final Hashtable table = new Hashtable();
    
    public StorageManagerFactory(Param param) {
	setParam(param);
    }
    
    public void setParam(Param param) {
	this.param = param;
	Enumeration enumeration = table.elements();
	while (enumeration.hasMoreElements())
	    ((StorageManager) enumeration.nextElement()).setParam(param);
    }
    
    public File getDicomDirFileForAET(String string) {
	return new File((param.isFilesetSeparate()
			 ? new File(param.getFilesetPath(), string)
			 : new File(param.getFilesetPath())),
			"DICOMDIR");
    }
    
    public StorageManager createFileset(File file)
	throws IOException, DicomException, IllegalValueException {
	String string = param.getFilesetID();
	String string_0_ = UIDUtils.createUID();
	FileSet2 fileset2 = new FileSet2(file, string);
	DicomDir2 dicomdir2
	    = (param.isFilesetIconJpeg()
	       ? new DicomDir2(string, string_0_, "1.2.40.0.13.0.1.3.10", 8196)
	       : new DicomDir2(string, string_0_));
	StorageManager storagemanager
	    = new StorageManager(param, dicomdir2, fileset2);
	storagemanager.setDirty(true);
	storagemanager.writeDicomDir();
	table.put(file, storagemanager);
	return storagemanager;
    }
    
    public StorageManager getStorageManager(File file, boolean bool)
	throws IOException, DicomException, IllegalValueException {
	if (!file.exists())
	    return bool ? createFileset(file) : null;
	StorageManager storagemanager;
	if ((storagemanager = (StorageManager) table.get(file)) != null)
	    return storagemanager;
	FileSet2 fileset2 = new FileSet2(file);
	DicomDir2 dicomdir2 = new DicomDir2();
	fileset2.read(dicomdir2);
	storagemanager = new StorageManager(param, dicomdir2, fileset2);
	table.put(file, storagemanager);
	return storagemanager;
    }
    
    public StorageManager getStorageManagerForAET
	(String string, boolean bool)
	throws IOException, DicomException, IllegalValueException {
	return getStorageManager(getDicomDirFileForAET(string), bool);
    }
}
