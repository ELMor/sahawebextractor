/* FwdConnectionPool - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.io.IOException;
import java.util.Hashtable;

import com.archimed.dicom.Debug;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.VerboseAssociation;

class FwdConnectionPool implements IAssociationListener
{
    private final Hashtable table = new Hashtable();
    
    public FwdConnectionPool() {
	/* empty */
    }
    
    public void associateRequestReceived(VerboseAssociation verboseassociation,
					 Request request) {
	/* empty */
    }
    
    public void associateResponseReceived
	(VerboseAssociation verboseassociation, Response response) {
	/* empty */
    }
    
    public void associateRequestSent(VerboseAssociation verboseassociation,
				     Request request) {
	/* empty */
    }
    
    public void associateResponseSent(VerboseAssociation verboseassociation,
				      Response response) {
	/* empty */
    }
    
    public void releaseRequestReceived(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseReceived
	(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseRequestSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void abortReceived(VerboseAssociation verboseassociation,
			      Abort abort) {
	/* empty */
    }
    
    public void abortSent(VerboseAssociation verboseassociation, int i,
			  int i_0_) {
	/* empty */
    }
    
    public void socketClosed(VerboseAssociation verboseassociation) {
	removeAndRelease(verboseassociation);
    }
    
    public DimseExchange get(VerboseAssociation verboseassociation) {
	return (DimseExchange) table.get(verboseassociation);
    }
    
    public void put(VerboseAssociation verboseassociation,
		    DimseExchange dimseexchange) throws IOException {
	if (table.contains(verboseassociation))
	    throw new RuntimeException();
	if (!verboseassociation.isOpen())
	    throw new IOException();
	verboseassociation.addAssociationListener(this);
	table.put(verboseassociation, dimseexchange);
    }
    
    public DimseExchange remove(VerboseAssociation verboseassociation) {
	DimseExchange dimseexchange
	    = (DimseExchange) table.get(verboseassociation);
	if (dimseexchange != null)
	    verboseassociation.removeAssociationListener(this);
	return dimseexchange;
    }
    
    public void removeAndRelease(VerboseAssociation verboseassociation) {
	DimseExchange dimseexchange = remove(verboseassociation);
	if (dimseexchange != null && dimseexchange.isOpen()) {
	    try {
		dimseexchange.releaseAssoc();
	    } catch (Exception exception) {
		Debug.out.println("jdicom: " + exception);
	    }
	}
    }
}
