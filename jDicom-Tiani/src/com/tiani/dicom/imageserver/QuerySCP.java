/* QuerySCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefCFindSCP;
import com.tiani.dicom.framework.DefMultiResponse;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IMultiResponse;
import com.tiani.dicom.framework.SingleResponse;
import com.tiani.dicom.media.DREntity;
import com.tiani.dicom.media.DRFactory;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.util.AndDicomObjectFilter;
import com.tiani.dicom.util.DOFFactory;
import com.tiani.dicom.util.DefDicomObjectFilter;
import com.tiani.dicom.util.IDicomObjectFilter;

class QuerySCP extends DefCFindSCP
{
    private StorageManagerFactory storageMgrFty;
    private Param param;
    
    public QuerySCP(Param param, StorageManagerFactory storagemanagerfactory) {
	storageMgrFty = storagemanagerfactory;
	setParam(param);
    }
    
    public void setParam(Param param) {
	this.param = param;
    }
    
    protected IMultiResponse query(DimseExchange dimseexchange, int i,
				   String string, DicomMessage dicommessage) {
	try {
	    String string_0_ = dimseexchange.localAET();
	    StorageManager storagemanager
		= storageMgrFty.getStorageManagerForAET(string_0_, false);
	    if (storagemanager == null)
		return SingleResponse.NO_MATCH;
	    DicomObject dicomobject = dicommessage.getDataset();
	    String string_1_ = dicomobject.getS(78);
	    String string_2_ = null;
	    boolean bool = string_1_.equals("STUDY");
	    switch (dicomobject.getSize(82)) {
	    case -1:
	    case 0:
		break;
	    case 1:
		if (bool) {
		    string_2_ = dicomobject.getS(82);
		    break;
		}
		/* fall through */
	    default:
		return new SingleResponse(43264);
	    }
	    IDicomObjectFilter idicomobjectfilter
		= DOFFactory.getPatientDOFFactory().getFilter(dicomobject);
	    IDicomObjectFilter idicomobjectfilter_3_
		= DOFFactory.getStudyDOFFactory().getFilter(dicomobject);
	    IDicomObjectFilter idicomobjectfilter_4_
		= DOFFactory.getSeriesDOFFactory().getFilter(dicomobject);
	    IDicomObjectFilter idicomobjectfilter_5_
		= DOFFactory.getImageDOFFactory().getFilter(dicomobject);
	    IDicomObjectFilter idicomobjectfilter_6_
		= DRFactory.createRefSOPFilter((String) dicomobject.get(62),
					       (String) dicomobject.get(63));
	    if (idicomobjectfilter_6_ != null)
		idicomobjectfilter_5_
		    = new AndDicomObjectFilter(new IDicomObjectFilter[]
					       { idicomobjectfilter_5_,
						 idicomobjectfilter_6_ });
	    DicomDir2 dicomdir2 = storagemanager.getDirInfo();
	    Vector vector = new Vector();
	    dicomdir2.addReadLock();
	    try {
		DREntity drentity = dicomdir2.getRootEntity();
		Iterator iterator
		    = drentity.iterator("PATIENT", idicomobjectfilter);
		while (iterator.hasNext()) {
		    DRNode drnode = (DRNode) iterator.next();
		    DicomObject dicomobject_7_ = drnode.getDataset();
		    int i_8_ = 0;
		    int i_9_ = 0;
		    int i_10_ = 0;
		    DREntity drentity_11_ = drnode.getLowerEntity();
		    Iterator iterator_12_
			= drentity_11_.iterator("STUDY",
						idicomobjectfilter_3_);
		    while (iterator_12_.hasNext()) {
			DRNode drnode_13_ = (DRNode) iterator_12_.next();
			DicomObject dicomobject_14_ = drnode_13_.getDataset();
			i_8_++;
			int i_15_ = 0;
			int i_16_ = 0;
			DREntity drentity_17_ = drnode_13_.getLowerEntity();
			Vector vector_18_ = new Vector();
			Iterator iterator_19_
			    = drentity_17_.iterator("SERIES",
						    idicomobjectfilter_4_);
			while (iterator_19_.hasNext()) {
			    DRNode drnode_20_ = (DRNode) iterator_19_.next();
			    DicomObject dicomobject_21_
				= drnode_20_.getDataset();
			    i_9_++;
			    i_15_++;
			    int i_22_ = 0;
			    DREntity drentity_23_
				= drnode_20_.getLowerEntity();
			    String string_24_
				= drnode_20_.getDataset().getS(81);
			    if (string_24_ != null && string_24_.length() != 0
				&& vector_18_.indexOf(string_24_) == -1)
				vector_18_.addElement(string_24_);
			    Iterator iterator_25_
				= drentity_23_.iterator(null,
							idicomobjectfilter_5_);
			    while (iterator_25_.hasNext()) {
				DRNode drnode_26_
				    = (DRNode) iterator_25_.next();
				DicomObject dicomobject_27_
				    = drnode_26_.getDataset();
				i_10_++;
				i_16_++;
				i_22_++;
				if (string_1_.equals("IMAGE")) {
				    DicomObject dicomobject_28_
					= new DicomObject();
				    dicomobject_28_
					.set(63, dicomobject_27_.getS(53));
				    if (dicomobject.getSize(62) >= 0)
					dicomobject_28_
					    .set(62, dicomobject_27_.getS(52));
				    vector.addElement
					(makeResult(string_1_, string_0_,
						    dicomobject,
						    (new DicomObject[]
						     { dicomobject_7_,
						       dicomobject_14_,
						       dicomobject_21_,
						       dicomobject_27_,
						       dicomobject_28_ })));
				}
			    }
			    if (string_1_.equals("SERIES")) {
				DicomObject dicomobject_29_
				    = new DicomObject();
				dicomobject_29_.set(458, new Integer(i_22_));
				vector.addElement
				    (makeResult(string_1_, string_0_,
						dicomobject,
						(new DicomObject[]
						 { dicomobject_7_,
						   dicomobject_14_,
						   dicomobject_21_,
						   dicomobject_29_ })));
			    }
			}
			if (bool && (string_2_ == null
				     || vector_18_.indexOf(string_2_) != -1)) {
			    DicomObject dicomobject_30_ = new DicomObject();
			    dicomobject_30_.set(456, new Integer(i_15_));
			    dicomobject_30_.set(457, new Integer(i_16_));
			    Enumeration enumeration = vector_18_.elements();
			    while (enumeration.hasMoreElements())
				dicomobject_30_
				    .append(82, enumeration.nextElement());
			    vector.addElement
				(makeResult(string_1_, string_0_, dicomobject,
					    (new DicomObject[]
					     { dicomobject_7_, dicomobject_14_,
					       dicomobject_30_ })));
			}
		    }
		    if (string_1_.equals("PATIENT")) {
			DicomObject dicomobject_31_ = new DicomObject();
			dicomobject_31_.set(453, new Integer(i_8_));
			dicomobject_31_.set(454, new Integer(i_9_));
			dicomobject_31_.set(455, new Integer(i_10_));
			vector.addElement(makeResult(string_1_, string_0_,
						     dicomobject,
						     (new DicomObject[]
						      { dicomobject_7_,
							dicomobject_31_ })));
		    }
		}
	    } finally {
		dicomdir2.releaseReadLock();
	    }
	    return new DefMultiResponse(vector.elements());
	} catch (Exception exception) {
	    Debug.out.println(exception);
	    return new SingleResponse(42752);
	}
    }
    
    private static DicomObject makeResult
	(String string, String string_32_, DicomObject dicomobject,
	 DicomObject[] dicomobjects)
	throws DicomException {
	DicomObject dicomobject_33_
	    = DefDicomObjectFilter.createReturn(dicomobject, dicomobjects);
	dicomobject_33_.set(78, string);
	dicomobject_33_.set(79, string_32_);
	return dicomobject_33_;
    }
}
