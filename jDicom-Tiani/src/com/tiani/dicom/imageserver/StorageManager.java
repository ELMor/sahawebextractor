/* StorageManager - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.media.DREntity;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.media.FileSet2;

final class StorageManager
{
    private Param param;
    private final DicomDir2 dirInfo;
    private final FileSet2 fileSet;
    private boolean dirty = false;
    private int deleteCount = 0;
    private long usedSize = 0L;
    private Hashtable studyFolderInfos = null;
    private Vector sortedInfos = null;
    
    private static class StudyFolderInfo
    {
	private final String studyInstUID;
	private long size = 0L;
	private long lastModified = 0L;
	
	public StudyFolderInfo(String string) {
	    studyInstUID = string;
	}
	
	public void add(File file) {
	    size += file.length();
	    lastModified = Math.max(lastModified, file.lastModified());
	}
	
	public void delete(File file) {
	    size -= file.length();
	}
	
	public String studyInstUID() {
	    return studyInstUID;
	}
	
	public long size() {
	    return size;
	}
	
	public long lastModified() {
	    return lastModified;
	}
    }
    
    public StorageManager(Param param, DicomDir2 dicomdir2,
			  FileSet2 fileset2) {
	this.param = param;
	dirInfo = dicomdir2;
	fileSet = fileset2;
    }
    
    public DicomDir2 getDirInfo() {
	return dirInfo;
    }
    
    public FileSet2 getFileset() {
	return fileSet;
    }
    
    public File getDirFile() {
	return fileSet.getDirFile();
    }
    
    public File getRootDir() {
	return fileSet.getRootDir();
    }
    
    public void setDirty(boolean bool) {
	dirty = bool;
    }
    
    public boolean isDirty() {
	return dirty;
    }
    
    public void setParam(Param param) {
	this.param = param;
    }
    
    public void write(DicomObject dicomobject, DicomObject dicomobject_0_,
		      String string) {
	try {
	    if (studyFolderInfos == null)
		initInfos();
	    File file = fileSet.write(dicomobject, dicomobject_0_,
				      param.isFileMetaInformation());
	    StudyFolderInfo studyfolderinfo
		= (StudyFolderInfo) studyFolderInfos.get(string);
	    if (studyfolderinfo == null) {
		studyfolderinfo = new StudyFolderInfo(string);
		studyFolderInfos.put(string, studyfolderinfo);
	    } else
		sortedInfos.removeElement(studyfolderinfo);
	    sortedInfos.addElement(studyfolderinfo);
	    studyfolderinfo.add(file);
	    usedSize += file.length();
	    runFIFO();
	} catch (Exception exception) {
	    Debug.out.println(exception.getMessage());
	}
    }
    
    public void writeDicomDir() throws IOException, DicomException {
	if (dirty) {
	    dirInfo.addReadLock();
	    try {
		fileSet.getRootDir().mkdirs();
		fileSet.write(dirInfo);
		dirty = false;
	    } finally {
		dirInfo.releaseReadLock();
	    }
	}
    }
    
    public void delete(DicomObject dicomobject, String string) {
	try {
	    if (studyFolderInfos == null)
		initInfos();
	    StudyFolderInfo studyfolderinfo
		= (StudyFolderInfo) studyFolderInfos.get(string);
	    if (studyfolderinfo != null)
		studyfolderinfo.delete(fileSet.getRefFile(dicomobject));
	    else
		Debug.out.println
		    ("Internal error concerning FIFO Storage Management");
	    fileSet.delete(dicomobject, true);
	} catch (Exception exception) {
	    Debug.out.println(exception.getMessage());
	}
    }
    
    public boolean compact() {
	if (deleteCount == 0)
	    return false;
	if (Debug.DEBUG > 0)
	    Debug.out.println(fileSet.getFileSetID()
			      + ": Compact Directory Information");
	dirInfo.addWriteLock();
	try {
	    dirty = true;
	    dirInfo.compact();
	    deleteCount = 0;
	} catch (DicomException dicomexception) {
	    Debug.out
		.println("DicomException: " + dicomexception.getMessage());
	} finally {
	    dirInfo.releaseWriteLock();
	}
	return true;
    }
    
    private boolean runFIFO() throws IOException, DicomException {
	long l = param.getFilesetMaxSize();
	if (Debug.DEBUG > 0)
	    Debug.out.println(fileSet.getFileSetID() + ": " + usedSize
			      + " Bytes (" + 100L * usedSize / l + "%) used");
	boolean bool = false;
	while (usedSize >= l) {
	    deleteStudy((StudyFolderInfo) sortedInfos.firstElement());
	    bool = true;
	}
	return bool;
    }
    
    private DRNode[] findStudyNodePath(String string) {
	DREntity drentity = dirInfo.getRootEntity();
	Iterator iterator = drentity.iterator("PATIENT");
	while (iterator.hasNext()) {
	    DRNode drnode = (DRNode) iterator.next();
	    DREntity drentity_1_ = drnode.getLowerEntity();
	    if (drentity_1_ != null) {
		DRNode drnode_2_ = drentity_1_.find("STUDY", string);
		if (drnode_2_ != null)
		    return new DRNode[] { drnode, drnode_2_ };
	    }
	}
	return null;
    }
    
    private void deleteStudy(StudyFolderInfo studyfolderinfo)
	throws IOException, DicomException {
	String string = studyfolderinfo.studyInstUID();
	sortedInfos.removeElement(studyfolderinfo);
	studyFolderInfos.remove(string);
	ArrayList arraylist = new ArrayList();
	try {
	    dirInfo.addWriteLock();
	    DRNode[] drnodes = findStudyNodePath(string);
	    if (drnodes == null) {
		Debug.out.println
		    ("ERROR: Missing Directory Record for Study - " + string);
		return;
	    }
	    DREntity drentity = drnodes[1].getLowerEntity();
	    if (drentity == null)
		Debug.out.println
		    ("WARNING: No Series Directory Records for Study - "
		     + string);
	    else {
		Iterator iterator = drentity.iterator();
		while (iterator.hasNext()) {
		    DRNode drnode = (DRNode) iterator.next();
		    DREntity drentity_3_ = drnode.getLowerEntity();
		    if (drentity_3_ == null)
			Debug.out.println
			    ("WARNING: No Object Directory Records for Series - "
			     + drnode.getKey());
		    else {
			Iterator iterator_4_ = drentity_3_.iterator();
			while (iterator_4_.hasNext()) {
			    DRNode drnode_5_ = (DRNode) iterator_4_.next();
			    arraylist.add(drnode_5_.getDataset());
			}
		    }
		}
	    }
	    if (drnodes[0].getLowerEntity().getCount() > 1)
		drnodes[1].delete();
	    else
		drnodes[0].delete();
	} finally {
	    dirInfo.releaseWriteLock();
	}
	Iterator iterator = arraylist.iterator();
	while (iterator.hasNext())
	    fileSet.delete((DicomObject) iterator.next(), true);
	usedSize -= studyfolderinfo.size();
	deleteCount++;
    }
    
    private void initInfos() throws DicomException {
	usedSize = 0L;
	studyFolderInfos = new Hashtable();
	sortedInfos = new Vector();
	DREntity drentity = dirInfo.getRootEntity();
	Iterator iterator = drentity.iterator("PATIENT");
	while (iterator.hasNext()) {
	    DRNode drnode = (DRNode) iterator.next();
	    DREntity drentity_6_ = drnode.getLowerEntity();
	    if (drentity_6_ != null) {
		Iterator iterator_7_ = drentity_6_.iterator("STUDY");
		while (iterator_7_.hasNext()) {
		    DRNode drnode_8_ = (DRNode) iterator_7_.next();
		    DREntity drentity_9_ = drnode_8_.getLowerEntity();
		    if (drentity_9_ != null) {
			DicomObject dicomobject = drnode_8_.getDataset();
			String string = (String) dicomobject.get(425);
			Iterator iterator_10_ = drentity_9_.iterator("SERIES");
			while (iterator_10_.hasNext()) {
			    DRNode drnode_11_ = (DRNode) iterator_10_.next();
			    DREntity drentity_12_
				= drnode_11_.getLowerEntity();
			    if (drentity_12_ != null) {
				Iterator iterator_13_
				    = drentity_12_.iterator();
				while (iterator_13_.hasNext()) {
				    DRNode drnode_14_
					= (DRNode) iterator_13_.next();
				    DicomObject dicomobject_15_
					= drnode_14_.getDataset();
				    StudyFolderInfo studyfolderinfo
					= ((StudyFolderInfo)
					   studyFolderInfos.get(string));
				    if (studyfolderinfo == null) {
					studyfolderinfo
					    = new StudyFolderInfo(string);
					studyFolderInfos.put(string,
							     studyfolderinfo);
					sortedInfos
					    .addElement(studyfolderinfo);
				    }
				    studyfolderinfo.add
					(fileSet.getRefFile(dicomobject_15_));
				}
			    }
			}
		    }
		}
	    }
	}
	Enumeration enumeration = sortedInfos.elements();
	while (enumeration.hasMoreElements())
	    usedSize += ((StudyFolderInfo) enumeration.nextElement()).size();
	sortInfos();
    }
    
    private void sortInfos() {
	for (int i = 0; i < sortedInfos.size() - 1; i++) {
	    StudyFolderInfo studyfolderinfo
		= (StudyFolderInfo) sortedInfos.elementAt(i);
	    int i_16_ = i;
	    for (int i_17_ = i + 1; i_17_ < sortedInfos.size(); i_17_++) {
		StudyFolderInfo studyfolderinfo_18_
		    = (StudyFolderInfo) sortedInfos.elementAt(i_17_);
		if (studyfolderinfo.lastModified()
		    > studyfolderinfo_18_.lastModified()) {
		    studyfolderinfo = studyfolderinfo_18_;
		    i_16_ = i_17_;
		}
	    }
	    if (i_16_ != i) {
		sortedInfos.setElementAt(sortedInfos.elementAt(i), i_16_);
		sortedInfos.setElementAt(studyfolderinfo, i);
	    }
	}
    }
}
