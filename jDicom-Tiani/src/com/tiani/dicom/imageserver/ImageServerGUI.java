/* ImageServerGUI - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.util.CheckParam;

public class ImageServerGUI
{
    public static void main(String[] strings) {
	try {
	    final String propFile
		= strings.length > 0 ? strings[0] : "ImageServer.properties";
	    final ImageServerApplet applet
		= new ImageServerApplet(ImageServer.loadProperties(propFile));
	    new AppletFrame("ImageServer v1.7.31", applet, 500, 400, new WindowAdapter() {
		public void windowClosing(WindowEvent windowevent) {
		    try {
			storeParams(propFile, applet.getParams());
		    } catch (Exception exception) {
			System.out.println(exception);
		    }
		    System.exit(0);
		}
	    });
	} catch (Throwable throwable) {
	    throwable.printStackTrace(System.out);
	}
    }
    
    private static void storeParams(String string, Properties properties)
	throws IOException {
	CheckParam.verify(properties, Param.CHECKS);
	FileOutputStream fileoutputstream = new FileOutputStream(string);
	try {
	    properties.store(fileoutputstream, "Properties for ImageServer");
	} finally {
	    fileoutputstream.close();
	}
    }
}
