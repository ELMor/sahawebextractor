/* StgCmtSCPStrategy - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.imageserver;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.media.DREntity;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.service.StorageCmtSCP;
import com.tiani.dicom.util.AETable;

class StgCmtSCPStrategy implements StorageCmtSCP.IStrategy
{
    private AETable aets;
    private StorageManagerFactory storageMgrFty;
    private static final String DIRINFO = "DIRINFO";
    private static final Integer NO_SUCH_OBJECT_INSTANCE = new Integer(274);
    private static final Integer CLASS_INSTANCE_CONFLICT = new Integer(281);
    
    private static final class ToCommitItem
    {
	final String uid;
	final DicomObject refSop;
	
	ToCommitItem(String string, DicomObject dicomobject) {
	    uid = string;
	    refSop = dicomobject;
	}
    }
    
    public StgCmtSCPStrategy(StorageManagerFactory storagemanagerfactory,
			     AETable aetable) {
	aets = aetable;
	storageMgrFty = storagemanagerfactory;
    }
    
    public int prepare(DimseExchange dimseexchange, DicomObject dicomobject,
		       DicomMessage dicommessage, Hashtable hashtable) {
	try {
	    StorageManager storagemanager
		= storageMgrFty.getStorageManagerForAET(dimseexchange
							    .localAET(),
							false);
	    if (storagemanager != null) {
		hashtable.put("DIRINFO", storagemanager.getDirInfo());
		return 0;
	    }
	} catch (Exception exception) {
	    Debug.out.println(exception);
	}
	try {
	    dicommessage.set(11, "access file set directory failed");
	} catch (DicomException dicomexception) {
	    /* empty */
	}
	return 272;
    }
    
    public int commit(DimseExchange dimseexchange, DicomObject dicomobject,
		      DicomMessage dicommessage, Hashtable hashtable,
		      DicomObject dicomobject_0_) {
	try {
	    ArrayList arraylist = refSopSeqToArrayList(dicomobject);
	    DicomDir2 dicomdir2 = (DicomDir2) hashtable.get("DIRINFO");
	    dicomdir2.addReadLock();
	    try {
		DREntity drentity = dicomdir2.getRootEntity();
		Iterator iterator = drentity.iterator("PATIENT");
		while (iterator.hasNext()) {
		    DRNode drnode = (DRNode) iterator.next();
		    DREntity drentity_1_ = drnode.getLowerEntity();
		    if (drentity_1_ != null) {
			Iterator iterator_2_ = drentity_1_.iterator("STUDY");
			while (iterator_2_.hasNext()) {
			    DRNode drnode_3_ = (DRNode) iterator_2_.next();
			    DREntity drentity_4_ = drnode_3_.getLowerEntity();
			    if (drentity_4_ != null) {
				Iterator iterator_5_
				    = drentity_4_.iterator("SERIES");
				while (iterator_5_.hasNext()) {
				    DRNode drnode_6_
					= (DRNode) iterator_5_.next();
				    DREntity drentity_7_
					= drnode_6_.getLowerEntity();
				    if (drentity_7_ != null) {
					Iterator iterator_8_
					    = arraylist.iterator();
					while (iterator_8_.hasNext()) {
					    ToCommitItem tocommititem
						= ((ToCommitItem)
						   iterator_8_.next());
					    DRNode drnode_9_
						= (drentity_7_.find
						   (null, tocommititem.uid));
					    if (drnode_9_ != null) {
						iterator_8_.remove();
						DicomObject dicomobject_10_
						    = drnode_9_.getDataset();
						DicomObject dicomobject_11_
						    = tocommititem.refSop;
						if (dicomobject_11_.get
							(115).equals
						    (dicomobject_10_.get(52)))
						    dicomobject_0_.append
							(121, dicomobject_11_);
						else {
						    dicomobject_11_.set
							(119,
							 CLASS_INSTANCE_CONFLICT);
						    dicomobject_0_.append
							(120, dicomobject_11_);
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    } finally {
		dicomdir2.releaseReadLock();
	    }
	    Iterator iterator = arraylist.iterator();
	    while (iterator.hasNext()) {
		ToCommitItem tocommititem = (ToCommitItem) iterator.next();
		DicomObject dicomobject_12_ = tocommititem.refSop;
		dicomobject_12_.set(119, NO_SUCH_OBJECT_INSTANCE);
		dicomobject_0_.append(120, dicomobject_12_);
	    }
	    if (dicomobject_0_.getSize(121) > 0) {
		dicomobject_0_.set(79, dimseexchange.localAET());
		dicomobject_0_.set(697, dicomdir2.getFileSetID());
		dicomobject_0_.set(698, dicomdir2.getFileSetUID());
	    }
	    dicomobject_0_.set(118, dicomobject.get(118));
	    return 0;
	} catch (Exception exception) {
	    Debug.out.println(exception);
	    if (dicommessage != null) {
		try {
		    dicommessage.set(11, "query file set directory failed");
		} catch (DicomException dicomexception) {
		    /* empty */
		}
	    }
	    return 272;
	}
    }
    
    private ArrayList refSopSeqToArrayList(DicomObject dicomobject)
	throws DicomException {
	ArrayList arraylist = new ArrayList();
	int i = dicomobject.getSize(121);
	for (int i_13_ = 0; i_13_ < i; i_13_++) {
	    DicomObject dicomobject_14_
		= (DicomObject) dicomobject.get(121, i_13_);
	    String string = (String) dicomobject_14_.get(116);
	    DicomObject dicomobject_15_ = new DicomObject();
	    dicomobject_15_.set(116, string);
	    dicomobject_15_.set(115, dicomobject_14_.get(115));
	    arraylist.add(new ToCommitItem(string, dicomobject_15_));
	}
	return arraylist;
    }
}
