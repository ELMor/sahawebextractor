/* AET - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public class AET
{
    public final String title;
    public final String host;
    public final int port;
    private final int hash;
    
    public AET(String string, String string_0_, int i) {
	title = string;
	host = string_0_;
	port = i;
	hash = string.hashCode() + string_0_.hashCode() + i;
    }
    
    public int hashCode() {
	return hash;
    }
    
    public boolean equals(Object object) {
	if (object == null || !(object instanceof AET))
	    return false;
	AET aet_1_ = (AET) object;
	return (title.equals(aet_1_.title) && host.equals(aet_1_.host)
		&& port == aet_1_.port);
    }
    
    public String toString() {
	return (this.getClass().getName() + "[title=" + title + ",host=" + host
		+ ",port=" + port + "]");
    }
}
