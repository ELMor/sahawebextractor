/* PaletteColorLUTs - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class PaletteColorLUTs
{
    private final DicomObject dcmObj;
    
    public PaletteColorLUTs(DicomObject dicomobject) {
	dcmObj = dicomobject;
    }
    
    public byte[] getRedPaletteColorLUTData16Bit() throws DicomException {
	return getPaletteColorLUTData16Bit(494, 498, 501);
    }
    
    public byte[] getGreenPaletteColorLUTData16Bit() throws DicomException {
	return getPaletteColorLUTData16Bit(495, 499, 502);
    }
    
    public byte[] getBluePaletteColorLUTData16Bit() throws DicomException {
	return getPaletteColorLUTData16Bit(496, 500, 503);
    }
    
    private byte[] getPaletteColorLUTData16Bit(int i, int i_0_, int i_1_)
	throws DicomException {
	if (dcmObj.getSize(i) != 3)
	    return null;
	int i_2_ = dcmObj.getI(i, 2);
	if (i_2_ != 16)
	    throw new DicomException("bits:" + i_2_);
	int i_3_ = dcmObj.getI(i, 1);
	if (i_3_ < 0)
	    throw new DicomException("off:" + i_3_);
	byte[] is = (byte[]) dcmObj.get(i_0_);
	if (is != null && i_3_ == 0)
	    return is;
	int i_4_ = dcmObj.getI(i);
	if (i_4_ == 0)
	    i_4_ = 65536;
	byte[] is_5_ = new byte[i_3_ + i_4_ << 1];
	int i_6_ = i_3_ << 1;
	if (is != null)
	    System.arraycopy(is, 0, is_5_, i_6_, is.length);
	else
	    inflateSegmentsTo16Bit((byte[]) dcmObj.get(i_1_), is_5_, i_6_);
	int i_7_ = 0;
	while (i_7_ < i_6_) {
	    is_5_[i_7_++] = is_5_[i_6_];
	    is_5_[i_7_++] = is_5_[i_6_ + 1];
	}
	return is_5_;
    }
    
    public static void inflateSegmentsTo16Bit
	(byte[] is, byte[] is_8_, int i) throws DicomException {
	if (Debug.DEBUG > 2)
	    Debug.out.println("jdicom: inflateSegmentsTo16Bit(src[" + is.length
			      + "],dst[" + is_8_.length + ",off2:" + i);
	int i_9_ = i;
	int i_10_ = 0;
	int i_11_ = 0;
	while (i_11_ < is.length) {
	    int i_12_ = is[i_11_++] & 0xff | (is[i_11_++] & 0xff) << 8;
	    int i_13_ = is[i_11_++] & 0xff | (is[i_11_++] & 0xff) << 8;
	    switch (i_12_) {
	    case 0:
		if (Debug.DEBUG > 3)
		    Debug.out.println("jdicom: segm@" + i_11_ + " " + i_9_
				      + "[+" + i_13_ + "]: Discrete");
		for (int i_14_ = 0; i_14_ < i_13_; i_14_++) {
		    is_8_[i_9_++] = is[i_11_++];
		    is_8_[i_9_++] = is[i_11_++];
		}
		i_10_ = is[i_11_ - 2] & 0xff | (is[i_11_ - 1] & 0xff) << 8;
		break;
	    case 1: {
		int i_15_ = is[i_11_++] & 0xff | (is[i_11_++] & 0xff) << 8;
		if (Debug.DEBUG > 3)
		    Debug.out.println("jdicom: segm@" + i_11_ + " " + i_9_
				      + "-" + (i_9_ + i_13_) + ": @Linear "
				      + i_10_ + "-" + i_15_);
		int i_16_ = i_15_ - i_10_;
		int i_17_ = 0;
		while (i_17_ < i_13_) {
		    int i_18_ = i_10_ + i_16_ * ++i_17_ / i_13_;
		    is_8_[i_9_++] = (byte) i_18_;
		    is_8_[i_9_++] = (byte) (i_18_ >> 8);
		}
		i_10_ = i_15_;
		break;
	    }
	    case 2: {
		int i_19_ = (is[i_11_++] & 0xff | (is[i_11_++] & 0xff) << 8
			     | (is[i_11_++] & 0xff) << 16
			     | (is[i_11_++] & 0xff) << 24);
		if (Debug.DEBUG > 3)
		    Debug.out.println("jdicom: segm@" + i_11_ + " " + i_9_
				      + "{+" + i_13_ + "}: Indirect @"
				      + i_19_);
		for (int i_20_ = 0; i_20_ < i_13_; i_20_++) {
		    int i_21_ = is[i_19_++] & 0xff | (is[i_19_++] & 0xff) << 8;
		    int i_22_ = is[i_19_++] & 0xff | (is[i_19_++] & 0xff) << 8;
		    switch (i_21_) {
		    case 0:
			if (Debug.DEBUG > 3)
			    Debug.out.println("jdicom: segm@" + i_19_ + " "
					      + i_9_ + "-" + (i_9_ + i_22_)
					      + ": @Discrete");
			for (int i_23_ = 0; i_23_ < i_22_; i_23_++) {
			    is_8_[i_9_++] = is[i_19_++];
			    is_8_[i_9_++] = is[i_19_++];
			}
			i_10_ = (is[i_19_ - 2] & 0xff
				 | (is[i_19_ - 1] & 0xff) << 8);
			break;
		    case 1: {
			int i_24_
			    = is[i_19_++] & 0xff | (is[i_19_++] & 0xff) << 8;
			if (Debug.DEBUG > 3)
			    Debug.out.println("jdicom: segm@" + i_19_ + " "
					      + i_9_ + "[+" + i_22_
					      + "]: @Linear " + i_10_ + "-"
					      + i_24_);
			int i_25_ = i_24_ - i_10_;
			int i_26_ = 0;
			while (i_26_ < i_22_) {
			    int i_27_ = i_10_ + i_25_ * ++i_26_ / i_22_;
			    is_8_[i_9_++] = (byte) i_27_;
			    is_8_[i_9_++] = (byte) (i_27_ >> 8);
			}
			i_10_ = i_24_;
			break;
		    }
		    default:
			throw new DicomException("illegal op code:" + i_21_
						 + ", index:" + (i_19_ - 4));
		    }
		}
		break;
	    }
	    default:
		throw new DicomException("illegal op code:" + i_12_
					 + ", index:" + (i_11_ - 4));
	    }
	}
    }
    
    public byte[] getRedPaletteColorLUTData8Bit() throws DicomException {
	return getPaletteColorLUTData8Bit(494, 498, 501);
    }
    
    public byte[] getGreenPaletteColorLUTData8Bit() throws DicomException {
	return getPaletteColorLUTData8Bit(495, 499, 502);
    }
    
    public byte[] getBluePaletteColorLUTData8Bit() throws DicomException {
	return getPaletteColorLUTData8Bit(496, 500, 503);
    }
    
    private byte[] getPaletteColorLUTData8Bit(int i, int i_28_, int i_29_)
	throws DicomException {
	if (dcmObj.getSize(i) != 3)
	    return null;
	int i_30_ = dcmObj.getI(i, 2);
	if (i_30_ != 16)
	    throw new DicomException("bits:" + i_30_);
	int i_31_ = dcmObj.getI(i, 1);
	if (i_31_ < 0)
	    throw new DicomException("off:" + i_31_);
	byte[] is = to8Bit((byte[]) dcmObj.get(i_28_));
	if (is != null && i_31_ == 0)
	    return is;
	int i_32_ = dcmObj.getI(i);
	if (i_32_ == 0)
	    i_32_ = 65536;
	byte[] is_33_ = new byte[i_31_ + i_32_];
	if (is != null)
	    System.arraycopy(is, 0, is_33_, i_31_, is.length);
	else
	    inflateSegmentsTo8Bit((byte[]) dcmObj.get(i_29_), is_33_, i_31_);
	int i_34_ = 0;
	while (i_34_ < i_31_)
	    is_33_[i_34_++] = is_33_[i_31_];
	return is_33_;
    }
    
    public static void inflateSegmentsTo8Bit
	(byte[] is, byte[] is_35_, int i) throws DicomException {
	if (Debug.DEBUG > 2)
	    Debug.out.println("jdicom: inflateSegmentsTo8Bit(src[" + is.length
			      + "],dst[" + is_35_.length + ",off:" + i);
	int i_36_ = i;
	int i_37_ = 0;
	int i_38_ = 0;
	while (i_38_ < is.length) {
	    int i_39_ = is[i_38_++] & 0xff | (is[i_38_++] & 0xff) << 8;
	    int i_40_ = is[i_38_++] & 0xff | (is[i_38_++] & 0xff) << 8;
	    switch (i_39_) {
	    case 0: {
		if (Debug.DEBUG > 3)
		    Debug.out.println("jdicom: segm@" + i_38_ + " " + i_36_
				      + "[+" + i_40_ + "]: Discrete");
		int i_41_ = 0;
		while (i_41_ < i_40_) {
		    is_35_[i_36_++] = is[++i_38_];
		    i_41_++;
		    i_38_++;
		}
		i_37_ = is[i_38_ - 2] & 0xff | (is[i_38_ - 1] & 0xff) << 8;
		break;
	    }
	    case 1: {
		int i_42_ = is[i_38_++] & 0xff | (is[i_38_++] & 0xff) << 8;
		if (Debug.DEBUG > 3)
		    Debug.out.println("jdicom: segm@" + i_38_ + " " + i_36_
				      + "[+" + i_40_ + "]: Linear " + i_37_
				      + "-" + i_42_);
		int i_43_ = i_42_ - i_37_;
		int i_44_ = 0;
		while (i_44_ < i_40_)
		    is_35_[i_36_++]
			= (byte) (i_37_ + i_43_ * ++i_44_ / i_40_ >> 8);
		i_37_ = i_42_;
		break;
	    }
	    case 2: {
		int i_45_ = (is[i_38_++] & 0xff | (is[i_38_++] & 0xff) << 8
			     | (is[i_38_++] & 0xff) << 16
			     | (is[i_38_++] & 0xff) << 24);
		if (Debug.DEBUG > 3)
		    Debug.out.println("jdicom: segm@" + i_38_ + " " + i_36_
				      + "{+" + i_40_ + "}: Indirect @"
				      + i_45_);
		for (int i_46_ = 0; i_46_ < i_40_; i_46_++) {
		    int i_47_ = is[i_45_++] & 0xff | (is[i_45_++] & 0xff) << 8;
		    int i_48_ = is[i_45_++] & 0xff | (is[i_45_++] & 0xff) << 8;
		    switch (i_47_) {
		    case 0: {
			if (Debug.DEBUG > 3)
			    Debug.out.println("jdicom: segm@" + i_45_ + " "
					      + i_36_ + "[+" + i_48_
					      + "]: @Discrete");
			int i_49_ = 0;
			while (i_49_ < i_48_) {
			    is_35_[i_36_++] = is[++i_45_];
			    i_49_++;
			    i_45_++;
			}
			i_37_ = (is[i_45_ - 2] & 0xff
				 | (is[i_45_ - 1] & 0xff) << 8);
			break;
		    }
		    case 1: {
			int i_50_
			    = is[i_45_++] & 0xff | (is[i_45_++] & 0xff) << 8;
			if (Debug.DEBUG > 3)
			    Debug.out.println("jdicom: segm@" + i_45_ + " "
					      + i_36_ + "[+" + i_48_
					      + "]: @Linear " + i_37_ + "-"
					      + i_50_);
			int i_51_ = i_50_ - i_37_;
			int i_52_ = 0;
			while (i_52_ < i_48_)
			    is_35_[i_36_++]
				= (byte) (i_37_ + i_51_ * ++i_52_ / i_48_
					  >> 8);
			i_37_ = i_50_;
			break;
		    }
		    default:
			throw new DicomException("illegal op code:" + i_47_
						 + ", index:" + (i_45_ - 4));
		    }
		}
		break;
	    }
	    default:
		throw new DicomException("illegal op code:" + i_39_
					 + ", index:" + (i_38_ - 4));
	    }
	}
    }
    
    private static byte[] to8Bit(byte[] is) {
	if (is == null)
	    return null;
	byte[] is_53_ = new byte[is.length >> 1];
	for (int i = 0; i < is_53_.length; i++)
	    is_53_[i] = is[(i << 1) + 1];
	return is_53_;
    }
}
