/* IDicomObjectFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public interface IDicomObjectFilter
{
    public boolean accept(DicomObject dicomobject) throws DicomException;
}
