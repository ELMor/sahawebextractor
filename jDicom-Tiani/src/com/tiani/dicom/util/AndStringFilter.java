/* AndStringFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public class AndStringFilter implements IStringFilter
{
    private IStringFilter[] _filter;
    
    public AndStringFilter(IStringFilter[] istringfilters) {
	_filter = istringfilters;
    }
    
    public boolean accept(String string) {
	for (int i = 0; i < _filter.length; i++) {
	    if (!_filter[i].accept(string))
		return false;
	}
	return true;
    }
}
