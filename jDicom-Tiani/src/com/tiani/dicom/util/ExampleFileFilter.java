/* ExampleFileFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.filechooser.FileFilter;

public class ExampleFileFilter extends FileFilter
{
    private static String TYPE_UNKNOWN = "Type Unknown";
    private static String HIDDEN_FILE = "Hidden File";
    private Hashtable filters = null;
    private String description = null;
    private String fullDescription = null;
    private boolean useExtensionsInDescription = true;
    
    public ExampleFileFilter() {
	filters = new Hashtable();
    }
    
    public ExampleFileFilter(String string) {
	this(string, null);
    }
    
    public ExampleFileFilter(String string, String string_0_) {
	this();
	if (string != null)
	    addExtension(string);
	if (string_0_ != null)
	    setDescription(string_0_);
    }
    
    public ExampleFileFilter(String[] strings) {
	this(strings, null);
    }
    
    public ExampleFileFilter(String[] strings, String string) {
	this();
	for (int i = 0; i < strings.length; i++)
	    addExtension(strings[i]);
	if (string != null)
	    setDescription(string);
    }
    
    public boolean accept(File file) {
	if (file != null) {
	    if (file.isDirectory())
		return true;
	    String string = getExtension(file);
	    if (string != null && filters.get(getExtension(file)) != null)
		return true;
	}
	return false;
    }
    
    public String getExtension(File file) {
	if (file != null) {
	    String string = file.getName();
	    int i = string.lastIndexOf('.');
	    if (i > 0 && i < string.length() - 1)
		return string.substring(i + 1).toLowerCase();
	}
	return null;
    }
    
    public void addExtension(String string) {
	if (filters == null)
	    filters = new Hashtable(5);
	filters.put(string.toLowerCase(), this);
	fullDescription = null;
    }
    
    public String getDescription() {
	if (fullDescription == null) {
	    if (description == null || isExtensionListInDescription()) {
		fullDescription
		    = description == null ? "(" : description + " (";
		Enumeration enumeration = filters.keys();
		if (enumeration != null) {
		    StringBuffer stringbuffer = new StringBuffer();
		    ExampleFileFilter examplefilefilter_1_ = this;
		    examplefilefilter_1_.fullDescription
			= stringbuffer.append
			      (examplefilefilter_1_.fullDescription).append
			      (".").append
			      ((String) enumeration.nextElement()).toString();
		    while (enumeration.hasMoreElements()) {
			StringBuffer stringbuffer_2_ = new StringBuffer();
			ExampleFileFilter examplefilefilter_3_ = this;
			examplefilefilter_3_.fullDescription
			    = stringbuffer_2_.append
				  (examplefilefilter_3_.fullDescription).append
				  (", ").append
				  ((String) enumeration.nextElement())
				  .toString();
		    }
		}
		StringBuffer stringbuffer = new StringBuffer();
		ExampleFileFilter examplefilefilter_4_ = this;
		examplefilefilter_4_.fullDescription
		    = stringbuffer.append
			  (examplefilefilter_4_.fullDescription).append
			  (")").toString();
	    } else
		fullDescription = description;
	}
	return fullDescription;
    }
    
    public void setDescription(String string) {
	description = string;
	fullDescription = null;
    }
    
    public void setExtensionListInDescription(boolean bool) {
	useExtensionsInDescription = bool;
	fullDescription = null;
    }
    
    public boolean isExtensionListInDescription() {
	return useExtensionsInDescription;
    }
}
