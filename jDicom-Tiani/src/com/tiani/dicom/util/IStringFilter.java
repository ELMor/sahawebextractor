/* IStringFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public interface IStringFilter
{
    public boolean accept(String string);
}
