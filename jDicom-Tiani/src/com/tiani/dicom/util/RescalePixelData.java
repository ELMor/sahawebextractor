/* RescalePixelData - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import com.archimed.dicom.DicomException;

public class RescalePixelData
{
    private final PixelMatrix pixelMatrix;
    private final short[] rescaleRows;
    private final short[] rescaleColumns;
    private final int toRows;
    private final int toColumns;
    
    public RescalePixelData(PixelMatrix pixelmatrix, int i, int i_0_)
	throws IllegalArgumentException, UnsupportedOperationException,
	       DicomException {
	pixelMatrix = pixelmatrix;
	if (i < 1 || i > 10000)
	    throw new IllegalArgumentException();
	toRows = i;
	toColumns = i_0_;
	rescaleRows = ShortRampFactory.create(toRows, 0, pixelmatrix.rows - 1);
	rescaleColumns
	    = ShortRampFactory.create(toColumns, 0, pixelmatrix.columns - 1);
    }
    
    public byte[] rescale(byte[] is)
	throws IndexOutOfBoundsException, IllegalArgumentException {
	int i = toRows * pixelMatrix.numberOfFrames;
	is = checkDest(is, i, toColumns);
	for (int i_1_ = 0; i_1_ < pixelMatrix.numberOfFrames; i_1_++)
	    rescaleFrame(i_1_, is, toRows * i_1_, 0, i, toColumns);
	return is;
    }
    
    public byte[] rescaleFrame(int i, byte[] is)
	throws IndexOutOfBoundsException, IllegalArgumentException {
	return rescaleFrame(i, is, 0, 0, toRows, toColumns);
    }
    
    public byte[] rescaleFrame
	(int i, byte[] is, int i_2_, int i_3_, int i_4_, int i_5_)
	throws IndexOutOfBoundsException, IllegalArgumentException {
	if (i >= pixelMatrix.numberOfFrames)
	    throw new IndexOutOfBoundsException();
	if (i_2_ < 0 || i_2_ + toRows > i_4_)
	    throw new IllegalArgumentException();
	if (i_3_ < 0 || i_3_ + toColumns > i_5_)
	    throw new IllegalArgumentException();
	is = checkDest(is, i_4_, i_5_);
	return (pixelMatrix.planarConfiguration == 0
		? rescaleSamplesByPixel(i, is, i_2_, i_3_, i_4_, i_5_)
		: pixelMatrix.bytesAllocated == 1
		? rescaleByteDataByPlane(i, is, i_2_, i_3_, i_4_, i_5_)
		: rescaleWordDataByPlane(i, is, i_2_, i_3_, i_4_, i_5_));
    }
    
    private byte[] checkDest(byte[] is, int i, int i_6_) {
	int i_7_ = (i * i_6_ * pixelMatrix.bytesAllocated
		    * pixelMatrix.samplesPerPixel);
	if (is == null)
	    is = new byte[i_7_];
	else if (is.length != i_7_)
	    throw new IllegalArgumentException("dest.length[" + is.length
					       + "] != totRows[" + i
					       + " * totColumns[" + i_6_
					       + "] * "
					       + (pixelMatrix.bytesAllocated
						  * (pixelMatrix
						     .samplesPerPixel)));
	return is;
    }
    
    private byte[] rescaleWordDataByPlane(int i, byte[] is, int i_8_, int i_9_,
					  int i_10_, int i_11_) {
	int i_12_ = i_10_ * i_11_ * 2;
	int i_13_ = pixelMatrix.columns * 2;
	byte[] is_14_ = pixelMatrix.pixelData;
	for (int i_15_ = 0; i_15_ < pixelMatrix.samplesPerPixel; i_15_++) {
	    for (int i_16_ = 0; i_16_ < toRows; i_16_++) {
		int i_17_ = i_9_ + (i_8_ + i_16_) * i_11_ << 1 + i_15_ * i_12_;
		int i_18_ = (i * pixelMatrix.frameSize
			     + i_15_ * pixelMatrix.planeSize
			     + rescaleRows[i_16_] * i_13_);
		for (int i_19_ = 0; i_19_ < toColumns; i_19_++) {
		    int i_20_;
		    is[i_17_++]
			= is_14_[i_20_ = i_18_ + (rescaleColumns[i_19_] << 1)];
		    is[i_17_++] = is_14_[++i_20_];
		}
	    }
	}
	return is;
    }
    
    private byte[] rescaleByteDataByPlane(int i, byte[] is, int i_21_,
					  int i_22_, int i_23_, int i_24_) {
	int i_25_ = i_23_ * i_24_;
	int i_26_ = pixelMatrix.columns;
	byte[] is_27_ = pixelMatrix.pixelData;
	for (int i_28_ = 0; i_28_ < pixelMatrix.samplesPerPixel; i_28_++) {
	    for (int i_29_ = 0; i_29_ < toRows; i_29_++) {
		int i_30_ = i_22_ + (i_21_ + i_29_) * i_24_ + i_28_ * i_25_;
		int i_31_ = (i * pixelMatrix.frameSize
			     + i_28_ * pixelMatrix.planeSize
			     + rescaleRows[i_29_] * i_26_);
		for (int i_32_ = 0; i_32_ < toColumns; i_32_++)
		    is[i_30_++] = is_27_[i_31_ + rescaleColumns[i_32_]];
	    }
	}
	return is;
    }
    
    private byte[] rescaleSamplesByPixel(int i, byte[] is, int i_33_,
					 int i_34_, int i_35_, int i_36_) {
	int i_37_ = pixelMatrix.samplesPerPixel * pixelMatrix.bytesAllocated;
	int i_38_ = pixelMatrix.columns * i_37_;
	byte[] is_39_ = pixelMatrix.pixelData;
	for (int i_40_ = 0; i_40_ < toRows; i_40_++) {
	    int i_41_ = (i_34_ + (i_33_ + i_40_) * i_36_) * i_37_;
	    int i_42_ = i * pixelMatrix.frameSize + rescaleRows[i_40_] * i_38_;
	    for (int i_43_ = 0; i_43_ < toColumns; i_43_++) {
		int i_44_ = i_42_ + rescaleColumns[i_43_] * i_37_;
		int i_45_ = i_37_;
		while (i_45_-- > 0)
		    is[i_41_++] = is_39_[i_44_++];
	    }
	}
	return is;
    }
}
