/* DefinedParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

class DefinedParam extends CheckParam
{
    protected String[] _tags;
    
    public DefinedParam(String[] strings, int i) {
	super(i);
	if (strings == null || strings.length == 0)
	    throw new IllegalArgumentException
		      ("enumeration must not be empty");
	_tags = strings;
    }
    
    public void check(String string) {
	super.check(string);
    }
    
    public String[] getTags() {
	return _tags;
    }
}
