/* CheckMultiEnumParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.StringTokenizer;

class CheckMultiEnumParam extends CheckParam
{
    private final CheckEnumParam _checkEnum;
    
    public CheckMultiEnumParam(String[] strings) {
	super(3);
	_checkEnum = new CheckEnumParam(strings, 1);
    }
    
    public String[] getTags() {
	return _checkEnum.getTags();
    }
    
    public boolean isMulti() {
	return true;
    }
    
    public void check(String string) throws IllegalArgumentException {
	if (string != null && string.length() != 0) {
	    StringTokenizer stringtokenizer
		= new StringTokenizer(string, " ,/\\");
	    while (stringtokenizer.hasMoreTokens())
		_checkEnum.check(stringtokenizer.nextToken());
	}
    }
}
