/* PixelMatrix - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.math.BigInteger;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public abstract class PixelMatrix
{
    final int rows;
    final int columns;
    final int samplesPerPixel;
    final int numberOfSamples;
    final String photometricInterpretation;
    final int pmiConst;
    final int planarConfiguration;
    final int numberOfFrames;
    final int representativeFrameNumber;
    final int bytesAllocated;
    final int pixelsPerFrame;
    final int planeSize;
    final int frameSize;
    final int bitsStored;
    final int shift;
    final int bitmask;
    final int[] aspectRatio;
    final float[] pixelSpacing;
    final byte[] pixelData;
    final byte[] redPalette;
    final byte[] greenPalette;
    final byte[] bluePalette;
    int minVal;
    int maxVal;
    private static final ColorModel GRAY8_COLORMODEL
	= new ComponentColorModel(ColorSpace.getInstance(1003),
				  new int[] { 8 }, false, false, 1, 0);
    private static final ColorModel RGB_COLORMODEL
	= new ComponentColorModel(ColorSpace.getInstance(1000),
				  new int[] { 8, 8, 8 }, false, false, 1, 0);
    private static final int[] INT0 = { 0 };
    private static final int[] INT000 = { 0, 0, 0 };
    private static final int[] INT012 = { 0, 1, 2 };
    
    public static class SignedShort extends PixelMatrix
    {
	public SignedShort(DicomObject dicomobject) throws DicomException {
	    super(dicomobject);
	    maxVal = bitmask >> 1;
	    minVal = maxVal ^ 0xffffffff;
	}
	
	private SignedShort(PixelMatrix pixelmatrix, int i, int i_0_, int i_1_,
			    int i_2_, byte[] is) {
	    super(pixelmatrix, i, i_0_, i_1_, i_2_, is);
	}
	
	protected PixelMatrix clone(PixelMatrix pixelmatrix, int i, int i_3_,
				    int i_4_, int i_5_, byte[] is) {
	    return new PixelMatrix.SignedShort(pixelmatrix, i, i_3_, i_4_,
					       i_5_, is);
	}
	
	public int[] calcMinMax() {
	    int i = 2147483647;
	    int i_6_ = -2147483648;
	    for (int i_7_ = 0; i_7_ < numberOfSamples; i_7_++) {
		int i_8_ = getSample(i_7_);
		i = Math.min(i_8_, i);
		i_6_ = Math.max(i_8_, i_6_);
	    }
	    return new int[] { i, i_6_ };
	}
	
	public final int getSample(int i) {
	    int i_9_ = i << 1;
	    int i_10_
		= ((pixelData[i_9_] & 0xff | pixelData[i_9_ + 1] << 8) >> shift
		   & bitmask);
	    return (i_10_ & minVal) == 0 ? i_10_ : i_10_ | minVal;
	}
	
	protected void rgbPixelToGrayscale(byte[] is) {
	    throw new UnsupportedOperationException();
	}
	
	protected void rgbPlaneToGrayscale(byte[] is) {
	    throw new UnsupportedOperationException();
	}
	
	protected void paletteColorToGrayscale(byte[] is) {
	    throw new UnsupportedOperationException();
	}
	
	protected void doPreformatGrayscale(LUT.Byte1 byte1, byte[] is) {
	    for (int i = 0; i < numberOfSamples; i++)
		is[i] = byte1.lookupByte(getSample(i));
	}
	
	public BufferedImage createBufferedImage(int i) {
	    throw new UnsupportedOperationException
		      ("Cannot create BufferedImage for " + this);
	}
	
	public String toString() {
	    return "Signed Short" + super.toString();
	}
    }
    
    public static class UnsignedShort extends PixelMatrix
    {
	public UnsignedShort(DicomObject dicomobject) throws DicomException {
	    super(dicomobject);
	    minVal = 0;
	    maxVal = bitmask;
	}
	
	private UnsignedShort(PixelMatrix pixelmatrix, int i, int i_11_,
			      int i_12_, int i_13_, byte[] is) {
	    super(pixelmatrix, i, i_11_, i_12_, i_13_, is);
	}
	
	protected PixelMatrix clone(PixelMatrix pixelmatrix, int i, int i_14_,
				    int i_15_, int i_16_, byte[] is) {
	    return new PixelMatrix.UnsignedShort(pixelmatrix, i, i_14_, i_15_,
						 i_16_, is);
	}
	
	public int[] calcMinMax() {
	    int i = 2147483647;
	    int i_17_ = -2147483648;
	    for (int i_18_ = 0; i_18_ < numberOfSamples; i_18_++) {
		int i_19_ = getSample(i_18_);
		i = Math.min(i_19_, i);
		i_17_ = Math.max(i_19_, i_17_);
	    }
	    return new int[] { i, i_17_ };
	}
	
	public final int getSample(int i) {
	    int i_20_ = i << 1;
	    return (((pixelData[i_20_] & 0xff | pixelData[i_20_ + 1] << 8)
		     >> shift)
		    & bitmask);
	}
	
	protected void rgbPixelToGrayscale(byte[] is) {
	    throw new UnsupportedOperationException();
	}
	
	protected void rgbPlaneToGrayscale(byte[] is) {
	    throw new UnsupportedOperationException();
	}
	
	protected void paletteColorToGrayscale(byte[] is) {
	    throw new UnsupportedOperationException();
	}
	
	protected void doPreformatGrayscale(LUT.Byte1 byte1, byte[] is) {
	    for (int i = 0; i < numberOfSamples; i++)
		is[i] = byte1.lookupByte(getSample(i));
	}
	
	public BufferedImage createBufferedImage(int i) {
	    if (!super.isPaletteColor())
		throw new UnsupportedOperationException
			  ("Cannot create BufferedImage for " + this);
	    DataBufferUShort databufferushort
		= new DataBufferUShort(pixelsPerFrame);
	    short[] is = databufferushort.getData();
	    int i_21_ = 0;
	    int i_22_ = pixelsPerFrame * i;
	    while (i_21_ < pixelsPerFrame) {
		is[i_21_] = (short) getSample(i_22_);
		i_21_++;
		i_22_++;
	    }
	    WritableRaster writableraster
		= Raster.createInterleavedRaster(databufferushort, columns,
						 rows, columns, 1,
						 PixelMatrix.INT0, null);
	    IndexColorModel indexcolormodel
		= new IndexColorModel(bitsStored, redPalette.length,
				      redPalette, greenPalette, bluePalette);
	    return new BufferedImage(indexcolormodel, writableraster, true,
				     null);
	}
	
	public String toString() {
	    return "Unsigned Short" + super.toString();
	}
    }
    
    public static class SignedByte extends PixelMatrix
    {
	public SignedByte(DicomObject dicomobject) throws DicomException {
	    super(dicomobject);
	    maxVal = bitmask >> 1;
	    minVal = maxVal ^ 0xffffffff;
	}
	
	private SignedByte(PixelMatrix pixelmatrix, int i, int i_23_,
			   int i_24_, int i_25_, byte[] is) {
	    super(pixelmatrix, i, i_23_, i_24_, i_25_, is);
	}
	
	protected PixelMatrix clone(PixelMatrix pixelmatrix, int i, int i_26_,
				    int i_27_, int i_28_, byte[] is) {
	    return new PixelMatrix.SignedByte(pixelmatrix, i, i_26_, i_27_,
					      i_28_, is);
	}
	
	public final int getSample(int i) {
	    int i_29_ = (pixelData[i] & 0xff) >> shift & bitmask;
	    return (i_29_ & minVal) == 0 ? i_29_ : i_29_ | minVal;
	}
	
	public int[] calcMinMax() {
	    int i = 2147483647;
	    int i_30_ = -2147483648;
	    for (int i_31_ = 0; i_31_ < numberOfSamples; i_31_++) {
		int i_32_ = getSample(i_31_);
		i = Math.min(i_32_, i);
		i_30_ = Math.max(i_32_, i_30_);
	    }
	    return new int[] { i, i_30_ };
	}
	
	protected void paletteColorToGrayscale(byte[] is) {
	    throw new UnsupportedOperationException();
	}
	
	protected void rgbPixelToGrayscale(byte[] is) {
	    int i = 0;
	    for (int i_33_ = 0; i_33_ < is.length; i_33_++)
		is[i_33_] = (byte) ((getSample(i++) + getSample(i++)
				     + getSample(i++))
				    / 3);
	}
	
	protected void rgbPlaneToGrayscale(byte[] is) {
	    int i = 0;
	    for (int i_34_ = 0; i_34_ < numberOfFrames; i_34_++) {
		int i_35_ = i_34_ * frameSize;
		int i_36_ = i_35_ + planeSize;
		int i_37_ = i_36_ + planeSize;
		for (int i_38_ = 0; i_38_ < planeSize; i_38_++)
		    is[i++] = (byte) ((getSample(i_35_++) + getSample(i_36_++)
				       + getSample(i_37_++))
				      / 3);
	    }
	}
	
	protected void doPreformatGrayscale(LUT.Byte1 byte1, byte[] is) {
	    for (int i = 0; i < numberOfSamples; i++)
		is[i] = byte1.lookupByte(getSample(i));
	}
	
	public BufferedImage createBufferedImage(int i) {
	    throw new UnsupportedOperationException
		      ("Cannot create BufferedImage for " + this);
	}
	
	public String toString() {
	    return "Signed Byte" + super.toString();
	}
    }
    
    public static class UnsignedByte extends PixelMatrix
    {
	public UnsignedByte(DicomObject dicomobject) throws DicomException {
	    super(dicomobject);
	    minVal = 0;
	    maxVal = bitmask;
	}
	
	private UnsignedByte(PixelMatrix pixelmatrix, int i, int i_39_,
			     int i_40_, int i_41_, byte[] is) {
	    super(pixelmatrix, i, i_39_, i_40_, i_41_, is);
	}
	
	private UnsignedByte(PixelMatrix pixelmatrix, byte[] is) {
	    super(pixelmatrix, is);
	}
	
	protected PixelMatrix clone(PixelMatrix pixelmatrix, int i, int i_42_,
				    int i_43_, int i_44_, byte[] is) {
	    return new PixelMatrix.UnsignedByte(pixelmatrix, i, i_42_, i_43_,
						i_44_, is);
	}
	
	public final int getSample(int i) {
	    return (pixelData[i] & 0xff) >> shift & bitmask;
	}
	
	public int[] calcMinMax() {
	    int i = 2147483647;
	    int i_45_ = -2147483648;
	    for (int i_46_ = 0; i_46_ < numberOfSamples; i_46_++) {
		int i_47_ = getSample(i_46_);
		i = Math.min(i_47_, i);
		i_45_ = Math.max(i_47_, i_45_);
	    }
	    return new int[] { i, i_45_ };
	}
	
	protected void paletteColorToGrayscale(byte[] is) {
	    for (int i = 0; i < is.length; i++) {
		int i_48_ = getSample(i);
		is[i] = (byte) (((redPalette[i_48_] & 0xff)
				 + (greenPalette[i_48_] & 0xff)
				 + (bluePalette[i_48_] & 0xff))
				/ 3);
	    }
	}
	
	protected void rgbPixelToGrayscale(byte[] is) {
	    int i = 0;
	    for (int i_49_ = 0; i_49_ < is.length; i_49_++)
		is[i_49_] = (byte) ((getSample(i++) + getSample(i++)
				     + getSample(i++))
				    / 3);
	}
	
	protected void rgbPlaneToGrayscale(byte[] is) {
	    int i = 0;
	    for (int i_50_ = 0; i_50_ < numberOfFrames; i_50_++) {
		int i_51_ = i_50_ * frameSize;
		int i_52_ = i_51_ + planeSize;
		int i_53_ = i_52_ + planeSize;
		for (int i_54_ = 0; i_54_ < planeSize; i_54_++)
		    is[i++] = (byte) ((getSample(i_51_++) + getSample(i_52_++)
				       + getSample(i_53_++))
				      / 3);
	    }
	}
	
	protected void doPreformatGrayscale(LUT.Byte1 byte1, byte[] is) {
	    for (int i = 0; i < numberOfSamples; i++)
		is[i] = byte1.lookupByte(getSample(i));
	}
	
	public BufferedImage createBufferedImage(int i) {
	    DataBufferByte databufferbyte
		= new DataBufferByte(pixelData, frameSize, frameSize * i);
	    WritableRaster writableraster;
	    ColorModel colormodel;
	    if (super.isMonochrome()) {
		writableraster
		    = Raster.createBandedRaster(databufferbyte, columns, rows,
						columns, PixelMatrix.INT0,
						PixelMatrix.INT0, null);
		colormodel = PixelMatrix.GRAY8_COLORMODEL;
	    } else if (super.isRGB()) {
		if (super.isColorByPlane())
		    writableraster
			= Raster.createBandedRaster(databufferbyte, columns,
						    rows, columns,
						    PixelMatrix.INT000,
						    (new int[]
						     { 0, planeSize,
						       planeSize << 1 }),
						    null);
		else
		    writableraster
			= Raster.createInterleavedRaster(databufferbyte,
							 columns, rows,
							 columns * 3, 3,
							 PixelMatrix.INT012,
							 null);
		colormodel = PixelMatrix.RGB_COLORMODEL;
	    } else if (super.isPaletteColor()) {
		writableraster
		    = Raster.createBandedRaster(databufferbyte, columns, rows,
						columns, PixelMatrix.INT0,
						PixelMatrix.INT0, null);
		colormodel
		    = new IndexColorModel(8, redPalette.length, redPalette,
					  greenPalette, bluePalette);
	    } else
		throw new UnsupportedOperationException
			  ("Cannot create BufferedImage for " + this);
	    return new BufferedImage(colormodel, writableraster, true, null);
	}
	
	public String toString() {
	    return "Unsigned Byte" + super.toString();
	}
    }
    
    public static class Dimension
    {
	public final int rows;
	public final int columns;
	
	public Dimension(int i, int i_55_) {
	    rows = i;
	    columns = i_55_;
	}
    }
    
    public static PixelMatrix create(DicomObject dicomobject)
	throws DicomException {
	int i = getI(dicomobject, 478);
	int i_56_ = getI(dicomobject, 475);
	switch (i_56_) {
	case 8:
	    return (i == 0 ? (PixelMatrix) new UnsignedByte(dicomobject)
		    : new SignedByte(dicomobject));
	case 16:
	    return (i == 0 ? (PixelMatrix) new UnsignedShort(dicomobject)
		    : new SignedShort(dicomobject));
	default:
	    throw new UnsupportedOperationException
		      ("" + i_56_ + " bits allocated not supported");
	}
    }
    
    static int toEvenLength(int i) {
	return i + 1 & ~0x1;
    }
    
    protected PixelMatrix(PixelMatrix pixelmatrix_57_, byte[] is) {
	pixelData = is;
	rows = pixelmatrix_57_.rows;
	columns = pixelmatrix_57_.columns;
	samplesPerPixel = 1;
	photometricInterpretation = "MONOCHROME2";
	pmiConst = 1;
	bytesAllocated = 1;
	planarConfiguration = 1;
	pixelsPerFrame = pixelmatrix_57_.pixelsPerFrame;
	planeSize = pixelsPerFrame * bytesAllocated;
	frameSize = planeSize * samplesPerPixel;
	numberOfFrames = pixelmatrix_57_.numberOfFrames;
	representativeFrameNumber = pixelmatrix_57_.representativeFrameNumber;
	if (toEvenLength(is.length)
	    != toEvenLength(frameSize * numberOfFrames))
	    throw new RuntimeException("mismatch length of pixel pixelData");
	numberOfSamples = pixelsPerFrame * samplesPerPixel * numberOfFrames;
	bitsStored = 8;
	bitmask = 255;
	shift = 0;
	aspectRatio = pixelmatrix_57_.aspectRatio;
	pixelSpacing = pixelmatrix_57_.pixelSpacing;
	minVal = 0;
	maxVal = 255;
	redPalette = null;
	greenPalette = null;
	bluePalette = null;
    }
    
    protected PixelMatrix(PixelMatrix pixelmatrix_58_, int i, int i_59_,
			  int i_60_, int i_61_, byte[] is) {
	pixelData = is;
	rows = i;
	columns = i_59_;
	photometricInterpretation = pixelmatrix_58_.photometricInterpretation;
	pmiConst = pixelmatrix_58_.pmiConst;
	samplesPerPixel = pixelmatrix_58_.samplesPerPixel;
	bytesAllocated = pixelmatrix_58_.bytesAllocated;
	planarConfiguration = pixelmatrix_58_.planarConfiguration;
	pixelsPerFrame = rows * columns;
	planeSize = pixelsPerFrame * bytesAllocated;
	frameSize = planeSize * samplesPerPixel;
	numberOfFrames = i_60_;
	representativeFrameNumber = i_61_;
	if (toEvenLength(is.length)
	    != toEvenLength(frameSize * numberOfFrames))
	    throw new RuntimeException("mismatch length of pixel pixelData");
	numberOfSamples = pixelsPerFrame * samplesPerPixel * numberOfFrames;
	bitsStored = pixelmatrix_58_.bitsStored;
	bitmask = pixelmatrix_58_.bitmask;
	shift = pixelmatrix_58_.shift;
	pixelSpacing = pixelmatrix_58_.calcPixelSpacingFor(rows, columns);
	aspectRatio = (pixelSpacing != null ? getAspectRatioFrom(pixelSpacing)
		       : pixelmatrix_58_.calcAspectRatioFor(rows, columns));
	minVal = pixelmatrix_58_.minVal;
	maxVal = pixelmatrix_58_.maxVal;
	redPalette = pixelmatrix_58_.redPalette;
	greenPalette = pixelmatrix_58_.greenPalette;
	bluePalette = pixelmatrix_58_.bluePalette;
    }
    
    private static int[] getAspectRatioFrom(float[] fs) {
	return cancel((long) (fs[0] * 1000.0F), (long) (fs[1] * 1000.0F));
    }
    
    private static int[] cancel(long l, long l_62_) {
	BigInteger biginteger = BigInteger.valueOf(l);
	BigInteger biginteger_63_ = BigInteger.valueOf(l_62_);
	BigInteger biginteger_64_ = biginteger.gcd(biginteger_63_);
	return (biginteger_64_.intValue() == 0
		? new int[] { (int) l, (int) l_62_ } : new int[]
		{ biginteger.divide(biginteger_64_).intValue(),
		  biginteger_63_.divide(biginteger_64_).intValue() });
    }
    
    private float[] calcPixelSpacingFor(int i, int i_65_) {
	return (pixelSpacing == null ? null : new float[]
		{ pixelSpacing[0] * (float) rows / (float) i,
		  pixelSpacing[1] * (float) columns / (float) i_65_ });
    }
    
    private int[] calcAspectRatioFor(int i, int i_66_) {
	int i_67_ = columns * i;
	int i_68_ = rows * i_66_;
	return (i_67_ == i_68_ ? aspectRatio
		: cancel((long) aspectRatio[0] * (long) i_68_,
			 (long) aspectRatio[1] * (long) i_67_));
    }
    
    protected PixelMatrix(DicomObject dicomobject) throws DicomException {
	switch (dicomobject.getSize(1184)) {
	case 1:
	    break;
	case -1:
	case 0:
	    throw new DicomException("missing pixel pixelData");
	default:
	    throw new IllegalArgumentException("encapsulate pixel pixelData");
	}
	pixelData = (byte[]) dicomobject.get(1184);
	rows = getI(dicomobject, 466);
	columns = getI(dicomobject, 467);
	photometricInterpretation = (String) dicomobject.get(462);
	if (photometricInterpretation == null)
	    throw new DicomException("missing photometric interpretation");
	pmiConst = EnumPMI.getConstant(photometricInterpretation);
	if (pmiConst == -1)
	    throw new DicomException("unrecognized photometric interpretation "
				     + photometricInterpretation);
	samplesPerPixel = getI(dicomobject, 461);
	if (samplesPerPixel != EnumPMI.SAMPLES[pmiConst])
	    throw new DicomException("samples per pixel[" + samplesPerPixel
				     + "] mismatch photometric interpretation["
				     + photometricInterpretation + "]");
	int i = getI(dicomobject, 475);
	if (i != 8 && i != 16)
	    throw new UnsupportedOperationException
		      ("" + i + " bits allocated with not supported");
	bytesAllocated = i >> 3;
	planarConfiguration = getI(dicomobject, 463, 1);
	pixelsPerFrame = rows * columns;
	planeSize = pixelsPerFrame * bytesAllocated;
	frameSize = planeSize * samplesPerPixel;
	numberOfFrames = getI(dicomobject, 464, 1);
	representativeFrameNumber = getI(dicomobject, 512, 1);
	if (toEvenLength(pixelData.length)
	    != toEvenLength(frameSize * numberOfFrames))
	    throw new DicomException("mismatch length of pixel pixelData");
	numberOfSamples = pixelsPerFrame * samplesPerPixel * numberOfFrames;
	bitsStored = Math.min(getI(dicomobject, 476), i);
	bitmask = 65535 >> 16 - bitsStored;
	shift = Math.min(i - bitsStored,
			 Math.max(0, bitsStored - getI(dicomobject, 477) - 1));
	pixelSpacing = getPixelSpacingFrom(dicomobject);
	aspectRatio = (pixelSpacing != null ? getAspectRatioFrom(pixelSpacing)
		       : getAspectRatioFrom(dicomobject));
	PaletteColorLUTs palettecolorluts = new PaletteColorLUTs(dicomobject);
	redPalette = palettecolorluts.getRedPaletteColorLUTData8Bit();
	greenPalette = palettecolorluts.getGreenPaletteColorLUTData8Bit();
	bluePalette = palettecolorluts.getBluePaletteColorLUTData8Bit();
	if (isPaletteColor() && (redPalette == null || greenPalette == null
				 || bluePalette == null))
	    throw new DicomException("Missing Palette Color LUT");
    }
    
    public static int[] getAspectRatioFrom(DicomObject dicomobject) {
	int[] is = { 1, 1 };
	if (dicomobject.getSize(473) == 2) {
	    is[0] = ((Integer) dicomobject.get(473, 0)).intValue();
	    is[1] = ((Integer) dicomobject.get(473, 1)).intValue();
	    if (is[0] == is[1]) {
		is[0] = 1;
		is[1] = 1;
	    }
	}
	return is;
    }
    
    public static float[] getPixelSpacingFrom(DicomObject dicomobject) {
	int i;
	if (dicomobject.getSize(i = 470) == 2
	    || dicomobject.getSize(i = 308) == 2) {
	    float[] fs = { ((Float) dicomobject.get(i, 0)).floatValue(),
			   ((Float) dicomobject.get(i, 1)).floatValue() };
	    if (fs[0] > 0.0F && fs[1] > 0.0F)
		return fs;
	    Debug.out.println("jdicom: Illegal values of "
			      + DDict.getDescription(i) + ": " + fs[0] + "\\"
			      + fs[1]);
	}
	return null;
    }
    
    public final byte[] getPixelData() {
	return pixelData;
    }
    
    public final byte[] getRedPalette() {
	return redPalette;
    }
    
    public final byte[] getGreenPalette() {
	return greenPalette;
    }
    
    public final byte[] getBluePalette() {
	return bluePalette;
    }
    
    public final int getRows() {
	return rows;
    }
    
    public final int getColumns() {
	return columns;
    }
    
    public final String getPhotometricInterpretation() {
	return photometricInterpretation;
    }
    
    public final int getSamplesPerPixel() {
	return samplesPerPixel;
    }
    
    public final boolean isMonochrome() {
	return pmiConst == 0 || pmiConst == 1;
    }
    
    public final boolean isMonochrome1() {
	return pmiConst == 0;
    }
    
    public final boolean isRGB() {
	return pmiConst == 3;
    }
    
    public final boolean isPaletteColor() {
	return pmiConst == 2;
    }
    
    public final boolean isColorByPlane() {
	return planarConfiguration != 0;
    }
    
    public final boolean isWordAllocated() {
	return bytesAllocated == 2;
    }
    
    public final int getBytesAllocated() {
	return bytesAllocated;
    }
    
    public final int getNumberOfFrames() {
	return numberOfFrames;
    }
    
    public final int getRepresentativeFrameNumber() {
	return representativeFrameNumber;
    }
    
    public final int getPixelsPerFrame() {
	return pixelsPerFrame;
    }
    
    public final int getPlaneSize() {
	return planeSize;
    }
    
    public final int getFrameSize() {
	return frameSize;
    }
    
    public final int[] getAspectRatio() {
	return aspectRatio;
    }
    
    public final float[] getPixelSpacing() {
	return pixelSpacing;
    }
    
    public final int getMinVal() {
	return minVal;
    }
    
    public final int getMaxVal() {
	return maxVal;
    }
    
    static int getI(DicomObject dicomobject, int i) throws DicomException {
	Integer integer = (Integer) dicomobject.get(i);
	if (integer == null)
	    throw new DicomException("Missing " + DDict.getDescription(i));
	return integer.intValue();
    }
    
    static int getI(DicomObject dicomobject, int i, int i_69_) {
	Integer integer = (Integer) dicomobject.get(i);
	return integer != null ? integer.intValue() : i_69_;
    }
    
    public String toString() {
	return (" Pixelmatrix " + photometricInterpretation
		+ ", colorByPlane= " + planarConfiguration + ", rows=" + rows
		+ ", columns=" + columns + ", frames=" + numberOfFrames
		+ ", bits stored=" + bitsStored + ", shift=" + shift
		+ ", min val=" + minVal + ", max val=" + maxVal
		+ ", aspect ratio=" + aspectRatio[0] + ":" + aspectRatio[1]
		+ (pixelSpacing != null ? (", pixel spacing=" + pixelSpacing[0]
					   + "\\" + pixelSpacing[1]) : ""));
    }
    
    public final int getNumberOfSamples() {
	return numberOfSamples;
    }
    
    public Dimension adjustRescaleDimension(int i, int i_70_) {
	long l = (long) (rows * aspectRatio[0]);
	long l_71_ = (long) (columns * aspectRatio[1]);
	return (l * (long) i_70_ > l_71_ * (long) i
		? new Dimension(i, (int) ((long) i * l_71_ / l))
		: new Dimension((int) ((long) i_70_ * l / l_71_), i_70_));
    }
    
    public PixelMatrix rescale(int i, int i_72_, byte[] is)
	throws DicomException {
	if (i == rows && i_72_ == columns)
	    return this;
	RescalePixelData rescalepixeldata
	    = new RescalePixelData(this, i, i_72_);
	PixelMatrix pixelmatrix_73_
	    = clone(this, i, i_72_, numberOfFrames, representativeFrameNumber,
		    rescalepixeldata.rescale(is));
	if (Debug.DEBUG > 2) {
	    Debug.out.println("jdicom: rescale pixel matrix: " + this);
	    Debug.out
		.println("jdicom:                   to: " + pixelmatrix_73_);
	}
	return pixelmatrix_73_;
    }
    
    public PixelMatrix rescaleFrame(int i, int i_74_, int i_75_, byte[] is)
	throws DicomException {
	RescalePixelData rescalepixeldata
	    = new RescalePixelData(this, i_74_, i_75_);
	PixelMatrix pixelmatrix_76_
	    = clone(this, i_74_, i_75_, 1, 0,
		    rescalepixeldata.rescaleFrame(i, is));
	if (Debug.DEBUG > 2) {
	    Debug.out.println("jdicom: rescale pixel matrix frame #" + i + ": "
			      + this);
	    Debug.out.println("jdicom:                             to: "
			      + pixelmatrix_76_);
	}
	return pixelmatrix_76_;
    }
    
    public PixelMatrix convertToPaletteColor(byte[] is) {
	switch (pmiConst) {
	case 0:
	case 1:
	    throw new UnsupportedOperationException
		      ("cannot convert monochrome to palette color image");
	case 2:
	    return this;
	default:
	    throw new UnsupportedOperationException
		      ("convertToPaletteColor not yet implemented");
	}
    }
    
    public PixelMatrix convertToGrayscale(byte[] is) {
	switch (pmiConst) {
	case 0:
	case 1:
	    return this;
	case 3:
	    is = getByteArray(is, pixelsPerFrame * numberOfFrames);
	    if (planarConfiguration == 0)
		rgbPixelToGrayscale(is);
	    else
		rgbPlaneToGrayscale(is);
	    break;
	case 2:
	    is = getByteArray(is, pixelsPerFrame * numberOfFrames);
	    paletteColorToGrayscale(is);
	    break;
	default:
	    throw new UnsupportedOperationException
		      ("convertToGrayscale not supported for - "
		       + photometricInterpretation);
	}
	return new UnsignedByte(this, is);
    }
    
    private static byte[] getByteArray(byte[] is, int i) {
	if (is == null)
	    return new byte[i];
	if (is.length != i)
	    throw new IllegalArgumentException("illegal pixeldata.length -"
					       + i);
	return is;
    }
    
    public PixelMatrix preformatGrayscale(LUT.Byte1 byte1, byte[] is) {
	if (byte1 == null)
	    return this;
	is = getByteArray(is, pixelsPerFrame * numberOfFrames);
	doPreformatGrayscale(byte1, is);
	UnsignedByte unsignedbyte = new UnsignedByte(this, is);
	if (Debug.DEBUG > 2) {
	    Debug.out.println("jdicom: preformat pixel matrix: " + this);
	    Debug.out
		.println("jdicom:                     to: " + unsignedbyte);
	}
	return unsignedbyte;
    }
    
    public abstract int[] calcMinMax();
    
    public abstract int getSample(int i);
    
    protected abstract PixelMatrix clone(PixelMatrix pixelmatrix_77_, int i,
					 int i_78_, int i_79_, int i_80_,
					 byte[] is);
    
    public abstract BufferedImage createBufferedImage(int i);
    
    protected abstract void rgbPixelToGrayscale(byte[] is);
    
    protected abstract void rgbPlaneToGrayscale(byte[] is);
    
    protected abstract void paletteColorToGrayscale(byte[] is);
    
    protected abstract void doPreformatGrayscale(LUT.Byte1 byte1, byte[] is);
}
