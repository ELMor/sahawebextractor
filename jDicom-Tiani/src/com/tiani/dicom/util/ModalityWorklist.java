/* ModalityWorklist - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public final class ModalityWorklist
{
    public static void initSPS(DicomObject dicomobject, String string,
			       String string_0_, String string_1_,
			       String string_2_, String string_3_,
			       String string_4_) throws DicomException {
	dicomobject.set(582, string);
	dicomobject.set(575, string_0_);
	dicomobject.set(576, string_1_);
	dicomobject.set(81, string_2_);
	dicomobject.set(574, string_3_);
	dicomobject.set(580, string_4_);
    }
    
    public static DicomObject createSPS
	(String string, String string_5_, String string_6_, String string_7_,
	 String string_8_, String string_9_)
	throws DicomException {
	DicomObject dicomobject = new DicomObject();
	initSPS(dicomobject, string, string_5_, string_6_, string_7_,
		string_8_, string_9_);
	return dicomobject;
    }
    
    public static void initItem
	(DicomObject dicomobject, String string, String string_10_,
	 String string_11_, String string_12_, String string_13_,
	 String string_14_, DicomObject dicomobject_15_)
	throws DicomException {
	dicomobject.set(147, string);
	dicomobject.set(148, string_10_);
	dicomobject.set(77, string_11_);
	dicomobject.set(425, string_12_);
	dicomobject.set(589, string_13_);
	dicomobject.set(547, string_14_);
	dicomobject.set(587, dicomobject_15_);
    }
    
    public static DicomObject createItem
	(String string, String string_16_, String string_17_,
	 String string_18_, String string_19_, String string_20_,
	 DicomObject dicomobject)
	throws DicomException {
	DicomObject dicomobject_21_ = new DicomObject();
	initItem(dicomobject_21_, string, string_16_, string_17_, string_18_,
		 string_19_, string_20_, dicomobject);
	return dicomobject_21_;
    }
}
