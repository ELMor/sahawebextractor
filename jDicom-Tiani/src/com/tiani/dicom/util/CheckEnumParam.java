/* CheckEnumParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

class CheckEnumParam extends DefinedParam
{
    public CheckEnumParam(String[] strings, int i) {
	super(strings, i);
    }
    
    public void check(String string) throws IllegalArgumentException {
	super.check(string);
	if (string != null && string.length() != 0) {
	    for (int i = 0; i < _tags.length; i++) {
		if (_tags[i].equals(string))
		    return;
	    }
	    throw new IllegalArgumentException(errMsg());
	}
    }
    
    private String errMsg() {
	StringBuffer stringbuffer = new StringBuffer();
	stringbuffer.append("not one of {");
	stringbuffer.append(_tags[0]);
	for (int i = 1; i < _tags.length; i++) {
	    stringbuffer.append(',');
	    stringbuffer.append(_tags[i]);
	}
	stringbuffer.append('}');
	return stringbuffer.toString();
    }
}
