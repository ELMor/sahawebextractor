/* WildcardStringFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.StringTokenizer;

public class WildcardStringFilter implements IStringFilter
{
    private String _prefix;
    private String[] _middle;
    private String _suffix;
    
    public WildcardStringFilter(String string) {
	StringTokenizer stringtokenizer = new StringTokenizer(string, "*");
	int i = stringtokenizer.countTokens();
	if (i > 0) {
	    if (string.charAt(0) != '*') {
		_prefix = stringtokenizer.nextToken();
		i--;
	    }
	    boolean bool = string.charAt(string.length() - 1) != '*';
	    if (bool)
		i--;
	    if (i > 0) {
		_middle = new String[i];
		for (int i_0_ = 0; i_0_ < i; i_0_++)
		    _middle[i_0_] = stringtokenizer.nextToken();
	    }
	    if (bool)
		_suffix = i < 0 ? _prefix : stringtokenizer.nextToken();
	}
    }
    
    public boolean accept(String string) {
	int i = 0;
	if (_prefix != null && (i = _prefix.length()) != 0
	    && !_match(string, _prefix))
	    return false;
	int i_1_ = string.length();
	boolean bool = false;
	int i_2_;
	if (_suffix != null && (i_2_ = _suffix.length()) != 0
	    && ((i_1_ -= i_2_) < 0
		|| !_match(string.substring(i_1_), _suffix)))
	    return false;
	if (_middle != null) {
	    for (int i_3_ = 0; i_3_ < _middle.length; i_3_++) {
		String string_4_ = _middle[i_3_];
		int i_5_ = string_4_.length();
		int i_6_ = i_1_ - (i + i_5_);
		do {
		    if (i_6_-- < 0)
			return false;
		} while (!_match(string.substring(i++), string_4_));
		i += i_5_ - 1;
	    }
	}
	return true;
    }
    
    private static boolean _match(String string, String string_7_) {
	int i;
	if (string_7_ == null || (i = string_7_.length()) == 0)
	    return true;
	if (string.length() < i)
	    return false;
	for (int i_8_ = 0; i_8_ < i; i_8_++) {
	    char c = string_7_.charAt(i_8_);
	    if (c != '?' && c != string.charAt(i_8_))
		return false;
	}
	return true;
    }
}
