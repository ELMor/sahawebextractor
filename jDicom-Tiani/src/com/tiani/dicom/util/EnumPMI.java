/* EnumPMI - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

public class EnumPMI
{
    public static final int MONOCHROME1 = 0;
    public static final int MONOCHROME2 = 1;
    public static final int PALETTE_COLOR = 2;
    public static final int RGB = 3;
    public static final int HSV = 4;
    public static final int ARGB = 5;
    public static final int CMYK = 6;
    public static final int YBR_FULL = 7;
    public static final int YBR_FULL_422 = 8;
    public static final int YBR_PARTIAL_422 = 9;
    public static final int OTHER = -1;
    public static final int[] SAMPLES = { 1, 1, 1, 3, 3, 4, 4, 4, 4, 4 };
    public static final String[] TO_STRING
	= { "MONOCHROME1", "MONOCHROME2", "PALETTE COLOR", "RGB", "HSV",
	    "ARGB", "CMYK", "YBR_FULL", "YBR_FULL_422", "YBR_PARTIAL_422" };
    
    public static int getConstant(String string) {
	int i = TO_STRING.length;
	while (--i >= 0 && !TO_STRING[i].equals(string)) {
	    /* empty */
	}
	return i;
    }
    
    public static boolean isGrayscale(String string) {
	return "MONOCHROME1".equals(string) || "MONOCHROME2".equals(string);
    }
    
    public static boolean isGrayscale(int i) {
	return i == 0 || i == 1;
    }
    
    public static boolean isMonochrome1(String string) {
	return "MONOCHROME1".equals(string);
    }
    
    public static boolean isMonochrome1(int i) {
	return i == 0;
    }
}
