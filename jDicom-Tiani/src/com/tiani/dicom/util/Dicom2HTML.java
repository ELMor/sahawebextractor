/* Dicom2HTML - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Enumeration;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;

public class Dicom2HTML extends PrintWriter
{
    private static ExtDDict _extDDict = new ExtDDict();
    
    public Dicom2HTML(OutputStream outputstream) {
	super(outputstream);
    }
    
    public void write(DicomObject dicomobject) throws DicomException {
	_writeHeader();
	DicomObject dicomobject_0_ = dicomobject.getFileMetaInformation();
	if (dicomobject_0_ != null)
	    _write("", dicomobject_0_);
	_write("", dicomobject);
	_writeFooter();
    }
    
    private void _writeHeader() {
	this.println("<HTML>");
	this.println("<HEAD><TITLE> Dicom2HTML Output </TITLE></HEAD>");
	this.println("<BODY>");
	this.println("<TABLE WIDTH=\"100%\" BORDER>");
	this.println
	    ("<TR><TH>Tag</TH><TH>Attribute Name</TH><TH>VR</TH><TH>VM</TH><TH>Value</TH></TR>");
    }
    
    private void _writeFooter() {
	this.println("</TABLE>");
	this.println("</BODY>");
	this.println("</HTML>");
    }
    
    private void _write(String string, DicomObject dicomobject)
	throws DicomException {
	Enumeration enumeration = dicomobject.enumerateVRs(false, true);
	int i = 0;
	while (enumeration.hasMoreElements()) {
	    TagValue tagvalue = (TagValue) enumeration.nextElement();
	    Tag tag = new Tag(tagvalue.getGroup(), tagvalue.getElement());
	    _writeAttribute(string, tag, dicomobject);
	    if (tag.isTypeSQ()) {
		for (int i_1_ = 0; i_1_ < tagvalue.size(); i_1_++) {
		    DicomObject dicomobject_2_
			= (DicomObject) tagvalue.getValue(i_1_);
		    this.println("<TR><TD>" + string + "></TD><TD>ITEM " + i_1_
				 + "</TD></TR>");
		    _write(string + '>', dicomobject_2_);
		}
	    }
	    i++;
	}
    }
    
    private void _writeAttribute
	(String string, Tag tag, DicomObject dicomobject)
	throws DicomException {
	this.print("<TR><TD>");
	this.print(string);
	this.print(tag.toString());
	this.print("</TD><TD>");
	this.print(tag.getDescription());
	this.print("</TD><TD>");
	this.print(tag.getTypePrompt());
	this.print("</TD><TD>");
	int i = tag.getGroup();
	int i_3_ = tag.getElement();
	int i_4_ = dicomobject.getSize_ge(i, i_3_);
	if (i_4_ == -1)
	    this.print('-');
	else
	    this.print(i_4_);
	if (!tag.isTypeSQ()) {
	    this.print("</TD><TD>");
	    for (int i_5_ = 0; i_5_ < i_4_; i_5_++) {
		if (i_5_ != 0)
		    this.print('\\');
		this.print(dicomobject.getS_ge(i, i_3_, i_5_));
	    }
	}
	this.println("</TD></TR>");
    }
}
