/* CommandLineParser - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Hashtable;
import java.util.Vector;

public class CommandLineParser
{
    private final String[] _options;
    private String _opt = "";
    private Vector _param = new Vector();
    private Hashtable _result = new Hashtable();
    
    public static Hashtable parse(String[] strings) {
	return new CommandLineParser(strings, null, null)._result;
    }
    
    public static String get(Hashtable hashtable, String string, int i) {
	return ((String[]) hashtable.get(string))[i];
    }
    
    public static Hashtable parse(String[] strings, String[] strings_0_)
	throws IllegalArgumentException {
	return new CommandLineParser(strings, strings_0_, null)._result;
    }
    
    public static Hashtable parse(String[] strings, String[] strings_1_,
				  int[] is) throws IllegalArgumentException {
	return new CommandLineParser(strings, strings_1_, is)._result;
    }
    
    private CommandLineParser
	(String[] strings, String[] strings_2_, int[] is)
	throws IllegalArgumentException {
	_options = strings_2_;
	for (int i = 0; i < strings.length; i++)
	    add(strings[i]);
	_result.put(_opt, paramToArray());
	_param = null;
	if (is != null)
	    checkParamCount(is);
    }
    
    private void checkParamCount(int[] is) {
	for (int i = 0; i < is.length; i++) {
	    String[] strings = (String[]) _result.get(_options[i]);
	    if (strings != null) {
		int i_3_ = strings.length - is[i];
		if (i_3_ < 0)
		    throw new IllegalArgumentException
			      ("Missing parameter for option: " + _options[i]);
		if (i_3_ > 0)
		    throw new IllegalArgumentException
			      ("Unrecognized parameter: " + strings[is[i]]);
	    }
	}
    }
    
    private void add(String string) {
	if (string.charAt(0) == '-') {
	    if (!isValidOption(string))
		throw new IllegalArgumentException("Unrecognized option: "
						   + string);
	    _result.put(_opt, paramToArray());
	    _opt = string;
	    _param = new Vector();
	} else
	    _param.addElement(string);
    }
    
    private String[] paramToArray() {
	String[] strings = new String[_param.size()];
	_param.copyInto(strings);
	return strings;
    }
    
    private boolean isValidOption(String string) {
	if (_options == null)
	    return true;
	for (int i = 0; i < _options.length; i++) {
	    if (string.equals(_options[i]))
		return true;
	}
	return false;
    }
}
