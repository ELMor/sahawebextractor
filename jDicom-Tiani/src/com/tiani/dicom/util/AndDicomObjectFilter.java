/* AndDicomObjectFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.util.Vector;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class AndDicomObjectFilter implements IDicomObjectFilter
{
    private IDicomObjectFilter[] _components;
    
    public AndDicomObjectFilter(IDicomObjectFilter[] idicomobjectfilters) {
	_components = idicomobjectfilters;
    }
    
    public AndDicomObjectFilter(Vector vector) {
	_components = new IDicomObjectFilter[vector.size()];
	vector.copyInto(_components);
    }
    
    public boolean accept(DicomObject dicomobject) throws DicomException {
	for (int i = 0; i < _components.length; i++) {
	    if (_components[i] != null && !_components[i].accept(dicomobject))
		return false;
	}
	return true;
    }
}
