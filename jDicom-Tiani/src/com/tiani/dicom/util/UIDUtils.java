/* UIDUtils - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.rmi.server.UID;
import java.util.Random;

public final class UIDUtils
{
    public static String root = "1.2.40.0.13.0.";
    public static String lha = getLocalHostAddress() + '.';
    private static Random _random = new Random();
    
    public static String createUID(String string, String string_0_) {
	return ((string != null ? string : root) + lha + toString(new UID())
		+ (string_0_ != null ? string_0_ : ""));
    }
    
    public static String createUID(String string) {
	return createUID(string, "");
    }
    
    public static String createUID() {
	return createUID(root, "");
    }
    
    public static String createID(int i) {
	StringBuffer stringbuffer = new StringBuffer();
	for (int i_1_ = 0; i_1_ < i; i_1_++)
	    stringbuffer.append((_random.nextInt() & 0x7fffffff) % 10);
	return stringbuffer.toString();
    }
    
    public static String getLocalHostAddress() {
	try {
	    return InetAddress.getLocalHost().getHostAddress();
	} catch (Exception exception) {
	    return "0.0.0.0";
	}
    }
    
    private static String toString(UID uid) {
	ByteArrayOutputStream bytearrayoutputstream
	    = new ByteArrayOutputStream(14);
	try {
	    uid.write(new DataOutputStream(bytearrayoutputstream));
	} catch (IOException ioexception) {
	    throw new RuntimeException(ioexception.toString());
	}
	byte[] is = bytearrayoutputstream.toByteArray();
	return ("" + toLong(is, 0, 4) + '.' + toLong(is, 4, 8) + '.'
		+ toLong(is, 12, 2));
    }
    
    private static long toLong(byte[] is, int i, int i_2_) {
	long l = 0L;
	for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
	    l <<= 8;
	    l |= (long) (is[i++] & 0xff);
	}
	return l;
    }
}
