/* CheckIntParam - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.util;

class CheckIntParam extends CheckParam
{
    private int _min;
    private int _max;
    
    public CheckIntParam(int i, int i_0_, int i_1_) {
	super(i_1_);
	if (i > i_0_)
	    throw new IllegalArgumentException("not a valid intervall");
	_min = i;
	_max = i_0_;
    }
    
    public void check(String string) throws IllegalArgumentException {
	super.check(string);
	if (string != null && string.length() != 0) {
	    int i = Integer.parseInt(string);
	    if (i < _min || i > _max)
		throw new IllegalArgumentException("outside [" + _min + ','
						   + _max + ']');
	}
    }
}
