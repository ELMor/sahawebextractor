/* FutureDimseRsp - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

public class FutureDimseRsp implements IDimseRspListener, IAssociationListener
{
    private DicomMessage _rsp = null;
    private boolean _open = true;
    
    public synchronized DicomMessage getResponse()
	throws InterruptedException {
	while (_open && _rsp == null)
	    this.wait();
	DicomMessage dicommessage = _rsp;
	_rsp = null;
	return dicommessage;
    }
    
    public synchronized void handleRSP
	(DimseExchange dimseexchange, int i, int i_0_,
	 DicomMessage dicommessage)
	throws DicomException {
	if (!Status.isPending(i_0_)) {
	    _rsp = dicommessage;
	    this.notifyAll();
	}
    }
    
    public synchronized void socketClosed
	(VerboseAssociation verboseassociation) {
	_open = false;
	this.notifyAll();
    }
    
    public void associateRequestReceived(VerboseAssociation verboseassociation,
					 Request request) {
	/* empty */
    }
    
    public void associateResponseReceived
	(VerboseAssociation verboseassociation, Response response) {
	/* empty */
    }
    
    public void associateRequestSent(VerboseAssociation verboseassociation,
				     Request request) {
	/* empty */
    }
    
    public void associateResponseSent(VerboseAssociation verboseassociation,
				      Response response) {
	/* empty */
    }
    
    public void releaseRequestReceived(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseReceived
	(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseRequestSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void releaseResponseSent(VerboseAssociation verboseassociation) {
	/* empty */
    }
    
    public void abortReceived(VerboseAssociation verboseassociation,
			      Abort abort) {
	/* empty */
    }
    
    public void abortSent(VerboseAssociation verboseassociation, int i,
			  int i_1_) {
	/* empty */
    }
}
