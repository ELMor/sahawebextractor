/* DefCMoveSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;
import java.util.Hashtable;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

public class DefCMoveSCP implements IDimseRqListener
{
    private Hashtable register = new Hashtable();
    
    public void handleRQ
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	IMultiResponse imultiresponse
	    = query(dimseexchange, i, string, dicommessage);
	Integer integer = new Integer(i);
	register.put(integer, imultiresponse);
	try {
	    int i_0_;
	    do {
		DicomMessage dicommessage_1_
		    = new DicomMessage(dicommessage.getPresentationContext(),
				       dicommessage.getAbstractSyntax(), 32801,
				       i, null);
		i_0_ = imultiresponse.nextResponse(dimseexchange,
						   dicommessage_1_);
		dicommessage_1_.affectedSOPclassUID(string);
		dicommessage_1_.status(i_0_);
		dimseexchange.getAssociation().sendMessage(dicommessage_1_);
	    } while (i_0_ == 65280 || i_0_ == 65281);
	} finally {
	    register.remove(integer);
	}
    }
    
    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	IMultiResponse imultiresponse
	    = (IMultiResponse) register.get(new Integer(i));
	if (imultiresponse == null)
	    return false;
	imultiresponse.cancel();
	return true;
    }
    
    protected IMultiResponse query(DimseExchange dimseexchange, int i,
				   String string, DicomMessage dicommessage) {
	return new SingleResponse(0);
    }
}
