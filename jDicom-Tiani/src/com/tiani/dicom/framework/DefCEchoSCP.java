/* DefCEchoSCP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

public class DefCEchoSCP implements IDimseRqListener
{
    private static IDimseRqListener _instance = new DefCEchoSCP();
    
    public void handleRQ
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	DicomMessage dicommessage_0_
	    = new DicomMessage(dicommessage.getPresentationContext(),
			       dicommessage.getAbstractSyntax(), 32816, i,
			       null);
	dicommessage_0_.status(0);
	dimseexchange.getAssociation().sendMessage(dicommessage_0_);
    }
    
    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	throw new DicomException("Error: cancel request for C-ECHO service");
    }
    
    public static IDimseRqListener getInstance() {
	return _instance;
    }
}
