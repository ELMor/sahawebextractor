/* IAssociationListener - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

public interface IAssociationListener
{
    public void associateRequestReceived(VerboseAssociation verboseassociation,
					 Request request);
    
    public void associateResponseReceived
	(VerboseAssociation verboseassociation, Response response);
    
    public void associateRequestSent(VerboseAssociation verboseassociation,
				     Request request);
    
    public void associateResponseSent(VerboseAssociation verboseassociation,
				      Response response);
    
    public void releaseRequestReceived(VerboseAssociation verboseassociation);
    
    public void releaseResponseReceived(VerboseAssociation verboseassociation);
    
    public void releaseRequestSent(VerboseAssociation verboseassociation);
    
    public void releaseResponseSent(VerboseAssociation verboseassociation);
    
    public void abortReceived(VerboseAssociation verboseassociation,
			      Abort abort);
    
    public void abortSent(VerboseAssociation verboseassociation, int i,
			  int i_0_);
    
    public void socketClosed(VerboseAssociation verboseassociation);
}
