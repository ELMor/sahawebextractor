/* DefAcceptancePolicy - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;

public class DefAcceptancePolicy implements IAcceptancePolicy
{
    private String _calledTitle;
    private String[] _callingTitles;
    private int[] _asids;
    private int _maxOpInvoked;
    private int _maxOpPerformed;
    private int _tsid;
    private boolean _rejectnocontext;
    
    public DefAcceptancePolicy(String string, String[] strings, int[] is,
			       int i, int i_0_, int i_1_, boolean bool) {
	_calledTitle = string;
	_callingTitles = strings;
	_asids = is;
	_maxOpInvoked = i;
	_maxOpPerformed = i_0_;
	_tsid = i_1_;
	_rejectnocontext = bool;
    }
    
    public Response prepareResponse
	(VerboseAssociation verboseassociation, Request request)
	throws IllegalValueException {
	Response response
	    = ResponsePolicy.prepareResponse(request, _calledTitle,
					     _callingTitles, _asids, _tsid,
					     _rejectnocontext);
	if (!(response instanceof Acknowledge))
	    return response;
	int i = request.getMaxOperationsInvoked();
	int i_2_ = request.getMaxOperationsPerformed();
	int i_3_ = (i == 0 || _maxOpInvoked != 0 && i > _maxOpInvoked
		    ? _maxOpInvoked : i);
	int i_4_ = (i_2_ == 0 || _maxOpPerformed != 0 && i_2_ > _maxOpPerformed
		    ? _maxOpPerformed : i_2_);
	Acknowledge acknowledge = (Acknowledge) response;
	acknowledge.setMaxOperationsInvoked(i_3_);
	acknowledge.setMaxOperationsPerformed(i_4_);
	return acknowledge;
    }
}
