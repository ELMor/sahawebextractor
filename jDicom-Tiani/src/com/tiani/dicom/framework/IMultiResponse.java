/* IMultiResponse - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;

public interface IMultiResponse
{
    public int nextResponse(DimseExchange dimseexchange,
			    DicomMessage dicommessage);
    
    public void cancel();
}
