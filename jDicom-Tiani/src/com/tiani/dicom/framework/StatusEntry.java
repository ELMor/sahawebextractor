/* StatusEntry - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;

public class StatusEntry
{
    public static final int UNKNOWN = 0;
    public static final int SUCCESS = 1;
    public static final int PENDING = 2;
    public static final int CANCEL = 3;
    public static final int WARNING = 4;
    public static final int FAILURE = 5;
    private final int code;
    private final int type;
    private final String message;
    
    public StatusEntry(int i, int i_0_, String string) {
	code = i;
	type = i_0_;
	message = string;
    }
    
    public int getCode() {
	return code;
    }
    
    public int getType() {
	return type;
    }
    
    public String getMessage() {
	return message;
    }
    
    public int hashCode(Object object) {
	return code;
    }
    
    public boolean equals(Object object) {
	if (!(object instanceof StatusEntry))
	    return false;
	return code == ((StatusEntry) object).code;
    }
    
    public String toString() {
	return Status.toString(code) + " [" + message + "]";
    }
}
