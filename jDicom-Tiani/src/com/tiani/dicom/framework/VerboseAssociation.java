/* VerboseAssociation - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Vector;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

public class VerboseAssociation extends Association
{
    private static int _count = 0;
    private String _id;
    private int _msgID;
    private Thread _closeThread;
    private Vector associationListeners;
    private Socket _socket;
    private String _remoteAET;
    private String _localAET;
    private static int[] dumpTags = { 147, 148, 427, 428, 430 };
    
    public VerboseAssociation(Socket socket) throws IOException {
	super(socket.getInputStream(), socket.getOutputStream());
	_id = "#" + ++_count + ":";
	_msgID = 0;
	_closeThread = null;
	associationListeners = new Vector();
	_remoteAET = "unknown";
	_localAET = "unknown";
	_socket = socket;
    }
    
    public int nextMessageID() {
	return ++_msgID & 0xffff;
    }
    
    public boolean isOpen() {
	return _socket != null && _closeThread == null;
    }
    
    public String localAET() {
	return _localAET;
    }
    
    public String remoteAET() {
	return _remoteAET;
    }
    
    public Socket socket() {
	return _socket;
    }
    
    public static void setDumpTags(int[] is) {
	dumpTags = is;
    }
    
    public void addAssociationListener
	(IAssociationListener iassociationlistener) {
	associationListeners.addElement(iassociationlistener);
    }
    
    public boolean removeAssociationListener
	(IAssociationListener iassociationlistener) {
	return associationListeners.removeElement(iassociationlistener);
    }
    
    public Request receiveAssociateRequest()
	throws IOException, UnknownUIDException, IllegalValueException {
	Request request = super.receiveAssociateRequest();
	_remoteAET = request.getCallingTitle();
	_localAET = request.getCalledTitle();
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.associateRequestReceived(this, request);
	if (Debug.DEBUG > 0) {
	    Debug.out.println("jdicom: " + _id + _remoteAET
			      + " >> A-ASSOCIATE-RQ PDU");
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: " + request);
	}
	return request;
    }
    
    public Response receiveAssociateResponse()
	throws IOException, UnknownUIDException, IllegalValueException {
	Response response = super.receiveAssociateResponse();
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.associateResponseReceived(this, response);
	if (Debug.DEBUG > 0) {
	    Debug.out.println("jdicom: " + _id + _remoteAET
			      + (response instanceof Acknowledge
				 ? " >> A-ASSOCIATE-AC PDU"
				 : " >> A-ASSOCIATE-RJ PDU"));
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: " + response);
	}
	return response;
    }
    
    public void sendAssociateRequest(Request request)
	throws IOException, IllegalValueException {
	_remoteAET = request.getCalledTitle();
	_localAET = request.getCallingTitle();
	if (Debug.DEBUG > 0) {
	    Debug.out.println("jdicom: " + _id + _remoteAET
			      + " << A-ASSOCIATE-RQ PDU");
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: " + request);
	}
	super.sendAssociateRequest(request);
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.associateRequestSent(this, request);
    }
    
    public void sendAssociateResponse(Response response)
	throws IOException, IllegalValueException {
	if (Debug.DEBUG > 0) {
	    Debug.out.println("jdicom: " + _id + _remoteAET
			      + (response instanceof Acknowledge
				 ? " << A-ASSOCIATE-AC PDU"
				 : " << A-ASSOCIATE-RJ PDU"));
	    if (Debug.DEBUG > 1)
		Debug.out.println("jdicom: " + response);
	}
	super.sendAssociateResponse(response);
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.associateResponseSent(this, response);
    }
    
    public DicomObject receiveData() throws IOException, IllegalValueException,
					    DicomException,
					    UnknownUIDException {
	DicomObject dicomobject = super.receiveData();
	if (Debug.DEBUG > 0) {
	    Debug.out.println("jdicom: " + _id + _remoteAET + " >> Dataset");
	    if (Debug.DEBUG > 1) {
		if (Debug.DEBUG > 2 || dicomobject.getSize(1184) == -1) {
		    dicomobject.dumpVRs(Debug.out);
		    Debug.out.flush();
		} else {
		    for (int i = 0; i < dumpTags.length; i++)
			Debug.out.println("jdicom: "
					  + DDict.getDescription(dumpTags[i])
					  + "[" + dicomobject.getS(dumpTags[i])
					  + "]");
		}
	    }
	}
	return dicomobject;
    }
    
    public DicomObject receiveCommand()
	throws IOException, IllegalValueException, DicomException,
	       UnknownUIDException {
	DicomObject dicomobject = super.receiveCommand();
	if (Debug.DEBUG > 0) {
	    Debug.out.println("jdicom: " + _id + _remoteAET + " >> "
			      + cmdName(dicomobject));
	    if (Debug.DEBUG > 1) {
		dicomobject.dumpVRs(Debug.out);
		Debug.out.flush();
	    }
	}
	return dicomobject;
    }
    
    public void sendMessage(DicomMessage dicommessage)
	throws IOException, IllegalValueException, DicomException {
	sendInPresentationContext(dicommessage.getPresentationContext(),
				  dicommessage, dicommessage.getDataset());
    }
    
    public void sendInPresentationContext
	(byte i, DicomObject dicomobject, DicomObject dicomobject_0_)
	throws IOException, IllegalValueException, DicomException {
	if (Debug.DEBUG > 0)
	    logSend(dicomobject, dicomobject_0_);
	super.sendInPresentationContext(i, dicomobject, dicomobject_0_);
    }
    
    private void logSend
	(DicomObject dicomobject, DicomObject dicomobject_1_)
	throws IOException, DicomException {
	Debug.out.println("jdicom: " + _id + _remoteAET + " << "
			  + cmdName(dicomobject));
	if (Debug.DEBUG > 1) {
	    dicomobject.dumpVRs(Debug.out);
	    Debug.out.flush();
	}
	if (dicomobject_1_ != null) {
	    Debug.out.println("jdicom: " + _id + _remoteAET + " << Dataset");
	    if (Debug.DEBUG > 1) {
		if (Debug.DEBUG > 2 || dicomobject_1_.getSize(1184) == -1) {
		    dicomobject_1_.dumpVRs(Debug.out);
		    Debug.out.flush();
		} else {
		    for (int i = 0; i < dumpTags.length; i++)
			Debug.out.println("jdicom: "
					  + DDict.getDescription(dumpTags[i])
					  + "["
					  + dicomobject_1_.getS(dumpTags[i])
					  + "]");
		}
	    }
	}
    }
    
    public void receiveReleaseRequest()
	throws IOException, IllegalValueException {
	super.receiveReleaseRequest();
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.releaseRequestReceived(this);
	if (Debug.DEBUG > 0)
	    Debug.out.println("jdicom: " + _id + _remoteAET
			      + " >> A-RELEASE-RQ PDU");
    }
    
    public void receiveReleaseResponse()
	throws IOException, IllegalValueException {
	super.receiveReleaseResponse();
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.releaseResponseReceived(this);
	if (Debug.DEBUG > 0)
	    Debug.out.println("jdicom: " + _id + _remoteAET
			      + " >> A-RELEASE-RP PDU");
    }
    
    public void sendReleaseRequest()
	throws IOException, IllegalValueException {
	if (Debug.DEBUG > 0)
	    Debug.out.println("jdicom: " + _id + _remoteAET
			      + " << A-RELEASE-RQ PDU");
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.releaseRequestSent(this);
	super.sendReleaseRequest();
    }
    
    public void sendReleaseResponse()
	throws IOException, IllegalValueException {
	if (Debug.DEBUG > 0)
	    Debug.out.println("jdicom: " + _id + _remoteAET
			      + " << A-RELEASE-RP PDU");
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.releaseResponseSent(this);
	super.sendReleaseResponse();
    }
    
    public Abort receiveAbort() throws IllegalValueException, IOException {
	Abort abort = super.receiveAbort();
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.abortReceived(this, abort);
	if (Debug.DEBUG > 0) {
	    Debug.out
		.println("jdicom: " + _id + _remoteAET + " >> A-ABORT PDU");
	    Debug.out.println("jdicom: " + abort);
	}
	return abort;
    }
    
    public void sendAbort(int i, int i_2_) throws IOException {
	super.sendAbort(i, i_2_);
	if (Debug.DEBUG > 0) {
	    Debug.out
		.println("jdicom: " + _id + _remoteAET + " << A-ABORT PDU:");
	    Debug.out.println("jdicom: " + new Abort(i, i_2_));
	}
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.abortSent(this, i, i_2_);
    }
    
    public void closesocket(final int artim) {
	if (_socket != null) {
	    notifySocketClose();
	    _closeThread = new Thread(new Runnable() {
		public void run() {
		    try {
			Thread.sleep((long) artim);
			try {
			    _socket.close();
			} finally {
			    _socket = null;
			}
		    } catch (Exception exception) {
			Debug.out.println("jdicom: " + exception.getMessage());
		    }
		}
	    });
	    _closeThread.start();
	}
    }
    
    private void notifySocketClose() {
	if (Debug.DEBUG > 0)
	    Debug.out
		.println("jdicom: " + _id + _remoteAET + " closing socket");
	Enumeration enumeration = associationListeners.elements();
	while (enumeration.hasMoreElements())
	    ((IAssociationListener) enumeration.nextElement())
		.socketClosed(this);
    }
    
    public void closesocket() throws IOException {
	if (_socket != null) {
	    notifySocketClose();
	    try {
		_socket.close();
	    } finally {
		_socket = null;
	    }
	}
    }
    
    private String cmdName(DicomObject dicomobject) {
	try {
	    UIDEntry uidentry = getUIDEntry(dicomobject);
	    StringBuffer stringbuffer = new StringBuffer();
	    stringbuffer.append(cmdName(dicomobject.getI(3)));
	    if (uidentry != null)
		stringbuffer.append(uidentry.getName());
	    if (dicomobject.getSize(9) > 0) {
		int i = dicomobject.getI(9);
		StatusEntry statusentry
		    = (uidentry != null
		       ? Status.getStatusEntry(uidentry.getConstant(), i)
		       : Status.getStatusEntry(i));
		stringbuffer.append(", ");
		stringbuffer.append(Status.toString(i));
		stringbuffer.append("[");
		stringbuffer.append(statusentry.getMessage());
		stringbuffer.append("]");
		if (dicomobject.getSize(11) > 0) {
		    stringbuffer.append(", error comment: ");
		    stringbuffer.append(dicomobject.getS(11));
		}
	    }
	    return stringbuffer.toString();
	} catch (Exception exception) {
	    return exception.toString();
	}
    }
    
    private UIDEntry getUIDEntry(DicomObject dicomobject) {
	try {
	    String string;
	    if ((string = (String) dicomobject.get(1)) != null
		|| (string = (String) dicomobject.get(2)) != null)
		return UID.getUIDEntry(string);
	} catch (Exception exception) {
	    /* empty */
	}
	return null;
    }
    
    public static String cmdName(int i) {
	switch (i) {
	case 32816:
	    return "C-ECHO-RSP ";
	case 32769:
	    return "C-STORE-RSP ";
	case 32784:
	    return "C-GET-RSP ";
	case 32800:
	    return "C-FIND-RSP ";
	case 32801:
	    return "C-MOVE-RSP ";
	case 33024:
	    return "N-EVENTREPORT-RSP ";
	case 33088:
	    return "N-CREATE-RSP ";
	case 33040:
	    return "N-GET-RSP ";
	case 33056:
	    return "N-SET-RSP ";
	case 33072:
	    return "N-ACTION-RSP ";
	case 33104:
	    return "N-DELETE-RSP ";
	case 48:
	    return "C-ECHO-RQ ";
	case 1:
	    return "C-STORE-RQ ";
	case 16:
	    return "C-GET-RQ ";
	case 32:
	    return "C-FIND-RQ ";
	case 33:
	    return "C-MOVE-RQ ";
	case 256:
	    return "N-EVENTREPORT-RQ ";
	case 320:
	    return "N-CREATE-RQ ";
	case 272:
	    return "N-GET-RQ ";
	case 288:
	    return "N-SET-RQ ";
	case 304:
	    return "N-ACTION-RQ ";
	case 336:
	    return "N-DELETE-RQ ";
	case 4095:
	    return "C-CANCEL-RQ ";
	default:
	    return "UNKNOWN COMMAND! ";
	}
    }
}
