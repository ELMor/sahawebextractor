/* SingleResponse - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import com.archimed.dicom.DicomException;

public class SingleResponse implements IMultiResponse
{
    public static final SingleResponse NO_MATCH = new SingleResponse(0);
    private final int status;
    private String errorComment;
    
    public SingleResponse(int i, String string) {
	status = i;
	errorComment = string;
    }
    
    public SingleResponse(int i) {
	this(i, null);
    }
    
    public int nextResponse(DimseExchange dimseexchange,
			    DicomMessage dicommessage) {
	if (errorComment != null) {
	    try {
		dicommessage.set(11, errorComment);
	    } catch (DicomException dicomexception) {
		/* empty */
	    }
	}
	return status;
    }
    
    public void cancel() {
	/* empty */
    }
}
