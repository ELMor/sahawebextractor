/* DefNEventReportSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.framework;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

public class DefNEventReportSCU implements IDimseRqListener
{
    public void handleRQ
	(DimseExchange dimseexchange, int i, String string,
	 DicomMessage dicommessage)
	throws IOException, DicomException, IllegalValueException,
	       UnknownUIDException {
	String string_0_ = dicommessage.getS(13);
	DicomMessage dicommessage_1_
	    = new DicomMessage(dicommessage.getPresentationContext(),
			       dicommessage.getAbstractSyntax(), 33024, i,
			       null);
	dicommessage_1_.affectedSOP(string, string_0_);
	dicommessage_1_.status(eventReport(dimseexchange, string, string_0_,
					   dicommessage, dicommessage_1_));
	dimseexchange.getAssociation().sendMessage(dicommessage_1_);
    }
    
    protected int eventReport
	(DimseExchange dimseexchange, String string, String string_2_,
	 DicomMessage dicommessage, DicomMessage dicommessage_3_)
	throws DicomException {
	return 0;
    }
    
    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
	throws DicomException {
	throw new DicomException
		  ("Error: cancel request for N-EVENT-REPORT service");
    }
}
