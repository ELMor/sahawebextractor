/* PropertiesTableModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.util.Properties;

import javax.swing.table.AbstractTableModel;

public class PropertiesTableModel extends AbstractTableModel
{
    private static final String[] _HEADERS = { "Property", "Value" };
    private String[] _keys;
    private Properties _data;
    
    public PropertiesTableModel() {
	this(null, null);
    }
    
    public PropertiesTableModel(String[] strings, Properties properties) {
	_keys = strings != null ? strings : new String[0];
	_data = properties != null ? properties : new Properties();
    }
    
    public String getColumnName(int i) {
	return _HEADERS[i];
    }
    
    public int getColumnCount() {
	return 2;
    }
    
    public int getRowCount() {
	return _keys.length;
    }
    
    public Object getValueAt(int i, int i_0_) {
	if (i < 0 || i >= _keys.length)
	    return null;
	switch (i_0_) {
	case 0:
	    return _keys[i];
	case 1:
	    return _data.getProperty(_keys[i]);
	default:
	    return null;
	}
    }
    
    public boolean isCellEditable(int i, int i_1_) {
	return i_1_ == 1;
    }
    
    public void setValueAt(Object object, int i, int i_2_) {
	if (isCellEditable(i, i_2_))
	    _data.put(_keys[i], object);
    }
    
    public boolean put(String string, String string_3_) {
	for (int i = 0; i < _keys.length; i++) {
	    if (_keys[i].equals(string)) {
		_data.put(string, string_3_);
		this.fireTableCellUpdated(i, 1);
		return true;
	    }
	}
	return false;
    }
}
