/* JFileListDialog - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class JFileListDialog extends JDialog
{
    private boolean _ok = false;
    private JList _list = new JList();
    private JButton _okButton = new JButton("OK");
    private JButton _cancelButton = new JButton("Cancel");
    
    public JFileListDialog(Frame frame, String string) {
	super(frame, string, true);
	_okButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		_ok = true;
		JFileListDialog.this.setVisible(false);
	    }
	});
	_cancelButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		JFileListDialog.this.setVisible(false);
	    }
	});
	JPanel jpanel = new JPanel();
	jpanel.add(_okButton);
	jpanel.add(_cancelButton);
	this.getContentPane().add(new JScrollPane(_list), "Center");
	this.getContentPane().add(jpanel, "South");
	this.setSize(400, 400);
	this.setLocation(20, 20);
    }
    
    public File[] getSelectedFiles(File file) {
	Vector vector = listFiles(file);
	if (vector.isEmpty())
	    return null;
	_list.setListData(vector);
	_list.setSelectionInterval(0, vector.size() - 1);
	_ok = false;
	this.show();
	if (!_ok)
	    return null;
	Object[] objects = _list.getSelectedValues();
	File[] files = new File[objects.length];
	for (int i = 0; i < objects.length; i++)
	    files[i] = (File) objects[i];
	return files;
    }
    
    private static Vector listFiles(File file) {
	Vector vector = new Vector();
	File[] files = file.listFiles();
	for (int i = 0; i < files.length; i++) {
	    if (files[i].isFile())
		vector.add(files[i]);
	    else
		vector.addAll(listFiles(files[i]));
	}
	return vector;
    }
}
