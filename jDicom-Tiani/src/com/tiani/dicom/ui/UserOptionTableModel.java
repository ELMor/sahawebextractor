/* UserOptionTableModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import javax.swing.table.AbstractTableModel;

import com.tiani.dicom.iod.DefCallbackUser;
import com.tiani.dicom.iod.UserOption;
import com.tiani.dicom.myassert.Assert;

public class UserOptionTableModel extends AbstractTableModel
{
    private static final String[] _HEADERS = { "Y/N", "Option" };
    private static final String[] _CLASSES
	= { "java.lang.Boolean", "java.lang.String" };
    private UserOption[] _options;
    private DefCallbackUser _callback;
    
    public UserOptionTableModel(UserOption[] useroptions,
				DefCallbackUser defcallbackuser) {
	_options = useroptions;
	_callback = defcallbackuser;
    }
    
    public String getColumnName(int i) {
	return _HEADERS[i];
    }
    
    public Class getColumnClass(int i) {
	try {
	    return Class.forName(_CLASSES[i]);
	} catch (ClassNotFoundException classnotfoundexception) {
	    return super.getColumnClass(i);
	}
    }
    
    public int getColumnCount() {
	return _HEADERS.length;
    }
    
    public int getRowCount() {
	return _options.length;
    }
    
    public boolean isCellEditable(int i, int i_0_) {
	return i_0_ == 0;
    }
    
    public void setValueAt(Object object, int i, int i_1_) {
	Assert.equals(0L, (long) i_1_);
	String string = _options[i].option();
	_callback.put(string, ((Boolean) object).booleanValue());
    }
    
    public Object getValueAt(int i, int i_2_) {
	String string = _options[i].option();
	if (i_2_ == 0)
	    return new Boolean(_callback.isTrue(string));
	return string;
    }
}
