/* JPropertiesPanel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileFilter;

import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.ExampleFileFilter;

public class JPropertiesPanel extends JPanel
{
    public static final FileFilter FILE_FILTER
	= new ExampleFileFilter("properties", "Properties");
    private String _header;
    private Properties _properties = null;
    private Hashtable _constraints;
    private PropertiesTableModel _model;
    private JSizeColumnsToFitTable _table;
    private JFileChooser _fileChooser = new JFileChooser(".");
    private JButton _loadButton = new JButton("Load");
    private JButton _saveButton = new JButton("Save");
    
    public JPropertiesPanel(File file, String string, Properties properties,
			    String[] strings, Hashtable hashtable) {
	this(file, string, properties, strings, hashtable, new JButton[0]);
    }
    
    public JPropertiesPanel(File file, String string, Properties properties,
			    String[] strings, Hashtable hashtable,
			    JButton jbutton) {
	this(file, string, properties, strings, hashtable,
	     new JButton[] { jbutton });
    }
    
    public JPropertiesPanel(File file, String string, Properties properties,
			    String[] strings, Hashtable hashtable,
			    JButton[] jbuttons) {
	super(new BorderLayout());
	_header = string;
	_properties = properties;
	_constraints = hashtable;
	_model = new PropertiesTableModel(strings, properties);
	_table = new JSizeColumnsToFitTable(_model);
	_fileChooser.setSelectedFile(file);
	_fileChooser.setFileFilter(FILE_FILTER);
	_loadButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		File file_1_
		    = JPropertiesPanel.this._chooseFile("Load Properties",
							"Open");
		if (file_1_ != null)
		    JPropertiesPanel.this._load(file_1_);
	    }
	});
	_saveButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		if (verifyParams()) {
		    File file_3_
			= JPropertiesPanel.this._chooseFile("Save Properties",
							    "Save");
		    if (file_3_ != null)
			JPropertiesPanel.this._save(file_3_);
		}
	    }
	});
	JPanel jpanel = new JPanel();
	jpanel.add(_loadButton);
	jpanel.add(_saveButton);
	for (int i = 0; i < jbuttons.length; i++)
	    jpanel.add(jbuttons[i]);
	this.add(new JScrollPane(_table), "Center");
	this.add(jpanel, "South");
    }
    
    public JSizeColumnsToFitTable getTable() {
	return _table;
    }
    
    public PropertiesTableModel getModel() {
	return _model;
    }
    
    public boolean verifyParams() {
	try {
	    CheckParam.verify(_properties, _constraints);
	    return true;
	} catch (Exception exception) {
	    System.out.println(exception);
	    return false;
	}
    }
    
    private File _chooseFile(String string, String string_4_) {
	_fileChooser.setDialogTitle(string);
	int i = _fileChooser.showDialog(this, string_4_);
	File file = _fileChooser.getSelectedFile();
	return i == 0 ? file : null;
    }
    
    public static Properties load(File file, Hashtable hashtable) {
	try {
	    System.out.println("Load Properties from " + file);
	    FileInputStream fileinputstream = new FileInputStream(file);
	    Properties properties = load(fileinputstream, hashtable);
	    fileinputstream.close();
	    return properties;
	} catch (IOException ioexception) {
	    System.out.println("JPropertiesPanel.load(" + file
			       + ")->Exception: " + ioexception.getMessage());
	    return null;
	}
    }
    
    public static Properties load(InputStream inputstream,
				  Hashtable hashtable) {
	try {
	    Properties properties = new Properties();
	    properties.load(inputstream);
	    CheckParam.verify(properties, hashtable);
	    return properties;
	} catch (Exception exception) {
	    System.out.println(exception);
	    return null;
	}
    }
    
    private void _load(File file) {
	Properties properties = load(file, _constraints);
	if (properties != null) {
	    _properties.clear();
	    Enumeration enumeration = properties.keys();
	    while (enumeration.hasMoreElements()) {
		String string = (String) enumeration.nextElement();
		_properties.put(string, properties.get(string));
	    }
	    _model.fireTableDataChanged();
	}
    }
    
    private void _save(File file) {
	try {
	    FileOutputStream fileoutputstream = new FileOutputStream(file);
	    try {
		_properties.store(fileoutputstream, _header);
		System.out.println("Save Properties to " + file);
	    } finally {
		fileoutputstream.close();
	    }
	} catch (IOException ioexception) {
	    System.out.println("JPropertiesPanel.store(" + file
			       + ")->Exception: " + ioexception.getMessage());
	}
    }
}
