/* DocumentOutputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class DocumentOutputStream extends OutputStream
{
    private byte[] buffer = new byte[1024];
    private int writePos = 0;
    private Document doc;
    private AttributeSet a;
    private int maxLength;
    
    public DocumentOutputStream(Document document, int i,
				AttributeSet attributeset) {
	doc = document;
	maxLength = i;
	a = attributeset;
    }
    
    public DocumentOutputStream(Document document, int i) {
	this(document, i, null);
    }
    
    public DocumentOutputStream(Document document) {
	this(document, 2147483647, null);
    }
    
    public final void write(int i) throws IOException {
	buffer[writePos] = (byte) i;
	if (++writePos == buffer.length)
	    flush();
    }
    
    public void flush() throws IOException {
	try {
	    int i = doc.getLength();
	    int i_0_ = i + writePos - maxLength;
	    if (i_0_ > 0) {
		doc.remove(0, i_0_);
		i -= i_0_;
	    }
	    doc.insertString(i, new String(buffer, 0, writePos), a);
	} catch (BadLocationException badlocationexception) {
	    throw new IOException(badlocationexception.getMessage());
	}
	writePos = 0;
    }
    
    public void write(byte[] is, int i, int i_1_) throws IOException {
	while (i_1_-- > 0)
	    write(is[i++]);
    }
}
