/* DicomObjectTableRow - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.util.StringTokenizer;
import java.util.Vector;

import com.archimed.dicom.DDate;
import com.archimed.dicom.DDateRange;
import com.archimed.dicom.DDateTime;
import com.archimed.dicom.DDateTimeRange;
import com.archimed.dicom.DTime;
import com.archimed.dicom.DTimeRange;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.util.Tag;

public final class DicomObjectTableRow
{
    private Tag _tag;
    private DicomObject _dicomObject;
    private DicomObjectTableRow _parent = null;
    private int _itemIndex = -1;
    private boolean _editable;
    private String _tagPrompt;
    private String _itemPrompt = null;
    private int _level = 0;
    
    public DicomObjectTableRow(Tag tag, DicomObject dicomobject,
			       DicomObjectTableRow dicomobjecttablerow_0_,
			       int i, boolean bool) {
	_tag = tag;
	_dicomObject = dicomobject;
	_parent = dicomobjecttablerow_0_;
	_itemIndex = i;
	_editable = bool;
	_initPrompts();
    }
    
    public boolean isItem() {
	return _tag == null;
    }
    
    public boolean isTypeSQ() {
	return _tag != null && _tag.isTypeSQ();
    }
    
    public void setEditable(boolean bool) {
	_editable = bool;
    }
    
    public Tag getTag() {
	return _tag;
    }
    
    public int getLevel() {
	return _level;
    }
    
    public DicomObjectTableRow getParent() {
	return _parent;
    }
    
    public int getItemIndex() {
	return _itemIndex;
    }
    
    public void incItemIndex() {
	_itemPrompt = "ITEM " + (++_itemIndex + 1);
    }
    
    public void decItemIndex() {
	_itemPrompt = "ITEM " + (--_itemIndex + 1);
    }
    
    public Object getValueAt(int i) throws DicomException {
	if (_tag == null) {
	    switch (i) {
	    case 0:
		return new Boolean(_itemIndex < _parent.getSize());
	    case 1:
		return _tagPrompt + _itemPrompt;
	    default:
		return null;
	    }
	}
	switch (i) {
	case 0:
	    return new Boolean(getSize() >= 0);
	case 1:
	    return _tagPrompt;
	case 2:
	    return _tag.getDescription();
	case 3:
	    return _tag.getTypePrompt();
	case 4:
	    return _sizeToString();
	case 5:
	    return _valueToString();
	default:
	    throw new IllegalArgumentException("no column #" + i);
	}
    }
    
    public boolean isCellEditable(int i) {
	return (_editable
		&& (i == 0 || i == 5 && _tag != null && !_tag.isTypeSQ()));
    }
    
    public int getSize() {
	return _dicomObject.getSize_ge(_tag.getGroup(), _tag.getElement());
    }
    
    public DicomObject getDicomObject() {
	return _dicomObject;
    }
    
    public Object getValue(int i) {
	return _dicomObject.get_ge(_tag.getGroup(), _tag.getElement(), i);
    }
    
    public void setValue(Object object, int i) throws DicomException {
	_dicomObject.set_ge(_tag.getGroup(), _tag.getElement(), object, i);
    }
    
    public void setMultiValue(String string) throws DicomException {
	Assert.isTrue(!_tag.isTypeSQ());
	StringTokenizer stringtokenizer = new StringTokenizer(string, "\\");
	setNull();
	while (stringtokenizer.hasMoreTokens())
	    append(_stringToValue(stringtokenizer.nextToken()));
    }
    
    public void setNull() throws DicomException {
	_dicomObject.set_ge(_tag.getGroup(), _tag.getElement(), null);
    }
    
    public void append(Object object) throws DicomException {
	_dicomObject.append_ge(_tag.getGroup(), _tag.getElement(), object);
    }
    
    public Vector deleteItem() {
	return _dicomObject.deleteItem_ge(_tag.getGroup(), _tag.getElement());
    }
    
    public Object deleteItem(int i) {
	return _dicomObject.deleteItem_ge(_tag.getGroup(), _tag.getElement(),
					  i);
    }
    
    private void _initPrompts() {
	StringBuffer stringbuffer = new StringBuffer();
	for (DicomObjectTableRow dicomobjecttablerow_1_ = _parent;
	     dicomobjecttablerow_1_ != null;
	     dicomobjecttablerow_1_ = dicomobjecttablerow_1_.getParent()) {
	    stringbuffer.append('>');
	    _level++;
	}
	if (_tag != null)
	    stringbuffer.append(_tag.toString());
	else
	    _itemPrompt = "ITEM " + (_itemIndex + 1);
	_tagPrompt = stringbuffer.toString();
    }
    
    private Object _stringToValue(String string) throws DicomException {
	switch (_tag.getTypeCode()) {
	case 11:
	    if (string.indexOf('-') != -1)
		return new DDateRange(string);
	    return new DDate(string);
	case 28:
	    if (string.indexOf('-') != -1)
		return new DDateTimeRange(string);
	    return new DDateTime(string);
	case 12:
	    if (string.indexOf('-') != -1)
		return new DTimeRange(string);
	    return new DTime(string);
	default:
	    return string;
	}
    }
    
    private String _sizeToString() throws DicomException {
	int i = getSize();
	return i == -1 ? "-" : String.valueOf(i);
    }
    
    private String _valueToString() throws DicomException {
	if (_tag.isTypeSQ())
	    return null;
	StringBuffer stringbuffer = new StringBuffer();
	int i = getSize();
	int i_2_ = _tag.getGroup();
	int i_3_ = _tag.getElement();
	for (int i_4_ = 0; i_4_ < i; i_4_++) {
	    if (i_4_ != 0)
		stringbuffer.append('\\');
	    stringbuffer.append(_dicomObject.getS_ge(i_2_, i_3_, i_4_));
	}
	return stringbuffer.toString();
    }
}
