/* JPropertiesTable - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Hashtable;
import java.util.Properties;

import javax.swing.JOptionPane;

import com.tiani.dicom.util.CheckParam;

public class JPropertiesTable extends JSizeColumnsToFitTable
{
    private Hashtable _checks;
    
    public JPropertiesTable(String[] strings, Properties properties,
			    Hashtable hashtable) {
	super(new PropertiesTableModel(strings, properties));
	_checks = hashtable;
	this.addMouseListener(new MouseAdapter() {
	    public void mouseClicked(MouseEvent mouseevent) {
		int i
		    = JPropertiesTable.this.rowAtPoint(mouseevent.getPoint());
		if (i != -1 && mouseevent.getClickCount() == 2)
		    JPropertiesTable.this.update(i);
	    }
	});
    }
    
    private void update(int i) {
	String string = (String) this.getValueAt(i, 0);
	CheckParam checkparam = (CheckParam) _checks.get(string);
	if (checkparam != null) {
	    String[] strings = checkparam.getTags();
	    if (strings != null) {
		if (!checkparam.isMulti()) {
		    Object object = this.getValueAt(i, 1);
		    Object object_1_
			= JOptionPane.showInputDialog(null, "Choose one",
						      string, 3, null, strings,
						      object);
		    if (object_1_ != null && object_1_ != object) {
			this.setValueAt(object_1_, i, 1);
			((PropertiesTableModel) this.getModel())
			    .fireTableCellUpdated(i, 1);
		    }
		} else {
		    Object object
			= JOptionPane.showInputDialog(null,
						      "Choose one to add",
						      string, 3, null, strings,
						      strings[0]);
		    if (object != null) {
			String string_2_ = (String) object;
			String string_3_ = (String) this.getValueAt(i, 1);
			if (string_3_.trim().length() > 0)
			    string_2_ += ',' + (String) string_3_;
			this.setValueAt(string_2_, i, 1);
			((PropertiesTableModel) this.getModel())
			    .fireTableCellUpdated(i, 1);
		    }
		}
	    }
	}
    }
}
