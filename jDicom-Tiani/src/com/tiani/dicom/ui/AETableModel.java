/* AETableModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import javax.swing.table.AbstractTableModel;

import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;

public class AETableModel extends AbstractTableModel
{
    private final AETable aetable;
    private String[] keys;
    private static String[] HEADER = { "Title", "Host", "Port" };
    
    public AETableModel(AETable aetable) {
	this.aetable = aetable;
	update();
    }
    
    public void update() {
	keys = aetable.list();
	int i = getRowCount();
	this.fireTableRowsInserted(0, i);
    }
    
    public String getColumnName(int i) {
	return HEADER[i];
    }
    
    public int getColumnCount() {
	return 3;
    }
    
    public int getRowCount() {
	return keys.length;
    }
    
    public Object getValueAt(int i, int i_0_) {
	AET aet = aetable.lookup(keys[i]);
	switch (i_0_) {
	case 0:
	    return aet.title;
	case 1:
	    return aet.host;
	case 2:
	    return "" + aet.port;
	default:
	    throw new IndexOutOfBoundsException();
	}
    }
    
    public void setValueAt(Object object, int i, int i_1_) {
	String string = (String) object;
	AET aet = aetable.removeAET(keys[i]);
	AET aet_2_;
	switch (i_1_) {
	case 0:
	    aet_2_ = new AET(string, aet.host, aet.port);
	    break;
	case 1:
	    aet_2_ = new AET(aet.title, string, aet.port);
	    break;
	case 2:
	    aet_2_ = new AET(aet.title, aet.host, Integer.parseInt(string));
	    break;
	default:
	    throw new IndexOutOfBoundsException();
	}
	aetable.add(aet_2_);
	keys[i] = aet_2_.title;
    }
    
    public boolean isCellEditable(int i, int i_3_) {
	return true;
    }
    
    public void deleteAET(int i) {
	aetable.removeAET(keys[i]);
	update();
    }
    
    public void addAET(AET aet) {
	aetable.add(aet);
	update();
    }
}
