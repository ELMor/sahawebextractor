/* DicomObjectTableModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.util.Tag;

public class DicomObjectTableModel extends AbstractTableModel
{
    private DicomObject _dicomObject;
    private boolean _editable = false;
    private boolean _showFileMetaInfo;
    private boolean _showSequenceItems;
    private Vector _rowdata;
    private static final String[] _HEADERS
	= { "Y/N", "Tag", "Attribute Name", "VR", "VM", "Value" };
    
    public DicomObjectTableModel(boolean bool, boolean bool_0_,
				 boolean bool_1_) {
	_rowdata = new Vector();
	_dicomObject = new DicomObject();
	_showFileMetaInfo = bool;
	_showSequenceItems = bool_0_;
	_editable = bool_1_;
    }
    
    public DicomObjectTableModel() {
	this(true, true, false);
    }
    
    public DicomObjectTableModel(DicomObject dicomobject, boolean bool,
				 boolean bool_2_,
				 boolean bool_3_) throws DicomException {
	_rowdata = new Vector();
	Assert.notNull(dicomobject);
	_dicomObject = dicomobject;
	_showFileMetaInfo = bool;
	_showSequenceItems = bool_2_;
	_editable = bool_3_;
	DicomObject dicomobject_4_ = _dicomObject.getFileMetaInformation();
	if (_showFileMetaInfo && dicomobject_4_ != null)
	    _initRowData(dicomobject_4_, null, -1);
	_initRowData(_dicomObject, null, -1);
    }
    
    public DicomObjectTableModel(DicomObject dicomobject)
	throws DicomException {
	this(dicomobject, true, true, false);
    }
    
    public void setEditable(boolean bool) {
	if (_editable != bool) {
	    _editable = bool;
	    Enumeration enumeration = _rowdata.elements();
	    while (enumeration.hasMoreElements())
		((DicomObjectTableRow) enumeration.nextElement())
		    .setEditable(_editable);
	}
    }
    
    public void addFileMetaInfoTags(Tag[] tags, boolean bool)
	throws DicomException {
	DicomObject dicomobject = _dicomObject.getFileMetaInformation();
	if (dicomobject == null) {
	    dicomobject
		= new FileMetaInformation(_dicomObject, "1.2.840.10008.1.2");
	    _dicomObject.setFileMetaInformation(dicomobject);
	}
	int i = _addTags(tags, dicomobject, null, -1, bool, 0);
	this.fireTableRowsInserted(0, i);
    }
    
    public void addFileMetaInfoTags(Tag[] tags) throws DicomException {
	addFileMetaInfoTags(tags, _editable);
    }
    
    public void addTags(Tag[] tags, boolean bool) throws DicomException {
	int i = _rowdata.size();
	int i_5_;
	for (i_5_ = 0;
	     i_5_ < i && getRowAt(i_5_).getDicomObject() != _dicomObject;
	     i_5_++) {
	    /* empty */
	}
	int i_6_ = _addTags(tags, _dicomObject, null, -1, bool, i_5_);
	this.fireTableRowsInserted(i_5_, i_6_);
    }
    
    public void addTags(Tag[] tags) throws DicomException {
	addTags(tags, _editable);
    }
    
    public DicomObject getDicomObject() {
	return _dicomObject;
    }
    
    public Class getColumnClass(int i) {
	return i == 0 ? Boolean.class : String.class;
    }
    
    public String getColumnName(int i) {
	return _HEADERS[i];
    }
    
    public int getColumnCount() {
	return _HEADERS.length;
    }
    
    public int getRowCount() {
	return _rowdata.size();
    }
    
    public DicomObjectTableRow getRowAt(int i) {
	return (DicomObjectTableRow) _rowdata.elementAt(i);
    }
    
    public Object getValueAt(int i, int i_7_) {
	try {
	    return getRowAt(i).getValueAt(i_7_);
	} catch (DicomException dicomexception) {
	    System.out
		.println("DicomException: " + dicomexception.getMessage());
	    return null;
	}
    }
    
    public boolean isCellEditable(int i, int i_8_) {
	return getRowAt(i).isCellEditable(i_8_);
    }
    
    public void setValueAt(Object object, int i, int i_9_) {
	try {
	    switch (i_9_) {
	    case 0:
		if (((Boolean) object).booleanValue())
		    _setNullValueAt(i);
		else
		    _deleteValueAt(i);
		break;
	    case 5:
		getRowAt(i).setMultiValue((String) object);
		this.fireTableRowsUpdated(i, i);
		break;
	    }
	} catch (Exception exception) {
	    System.out.println(exception);
	}
    }
    
    private void _setNullValueAt(int i) throws DicomException {
	DicomObjectTableRow dicomobjecttablerow = getRowAt(i);
	if (dicomobjecttablerow.isItem()) {
	    DicomObjectTableRow dicomobjecttablerow_10_
		= dicomobjecttablerow.getParent();
	    dicomobjecttablerow_10_.append(new DicomObject());
	    int i_11_ = _rowdata.indexOf(dicomobjecttablerow_10_);
	    this.fireTableRowsUpdated(i_11_, i_11_);
	    dicomobjecttablerow.incItemIndex();
	    int i_12_ = _addSeqTags(dicomobjecttablerow_10_, _editable, i,
				    dicomobjecttablerow.getItemIndex() - 1);
	    this.fireTableRowsInserted(i, i_12_ - 1);
	} else {
	    dicomobjecttablerow.setNull();
	    this.fireTableRowsUpdated(i, i);
	    if (dicomobjecttablerow.isTypeSQ()) {
		_rowdata.insertElementAt
		    (new DicomObjectTableRow(null, null, dicomobjecttablerow,
					     0, _editable),
		     i + 1);
		this.fireTableRowsInserted(i + 1, i + 1);
	    }
	}
    }
    
    private void _deleteValueAt(int i) throws DicomException {
	DicomObjectTableRow dicomobjecttablerow = getRowAt(i);
	if (dicomobjecttablerow.isItem()) {
	    DicomObjectTableRow dicomobjecttablerow_13_
		= dicomobjecttablerow.getParent();
	    dicomobjecttablerow_13_
		.deleteItem(dicomobjecttablerow.getItemIndex());
	    int i_14_ = _rowdata.indexOf(dicomobjecttablerow_13_);
	    this.fireTableRowsUpdated(i_14_, i_14_);
	    int i_15_ = dicomobjecttablerow.getLevel();
	    int i_16_ = i;
	    int i_17_ = i_16_;
	    DicomObjectTableRow dicomobjecttablerow_18_;
	    do {
		_rowdata.removeElementAt(i_16_);
		i_17_++;
		dicomobjecttablerow_18_ = getRowAt(i_16_);
	    } while (!dicomobjecttablerow_18_.isItem()
		     || dicomobjecttablerow_18_.getLevel() > i_15_);
	    this.fireTableRowsDeleted(i_16_, i_17_ - 1);
	    for (;;) {
		if (dicomobjecttablerow_18_.isItem()
		    && dicomobjecttablerow_18_.getLevel() == i_15_) {
		    dicomobjecttablerow_18_.decItemIndex();
		    this.fireTableRowsUpdated(i_16_, i_16_);
		    if (dicomobjecttablerow_18_.getDicomObject() == null)
			break;
		}
		dicomobjecttablerow_18_ = getRowAt(++i_16_);
	    }
	} else {
	    dicomobjecttablerow.deleteItem();
	    this.fireTableRowsUpdated(i, i);
	    if (dicomobjecttablerow.isTypeSQ()) {
		int i_19_ = dicomobjecttablerow.getLevel();
		int i_20_ = i + 1;
		int i_21_ = i_20_;
		while (i_20_ < _rowdata.size()
		       && getRowAt(i_20_).getLevel() > i_19_) {
		    _rowdata.removeElementAt(i_20_);
		    i_21_++;
		}
		this.fireTableRowsDeleted(i_20_, i_21_ - 1);
	    }
	}
    }
    
    private void _initRowData(DicomObject dicomobject,
			      DicomObjectTableRow dicomobjecttablerow,
			      int i) throws DicomException {
	Enumeration enumeration = dicomobject.enumerateVRs(false, true);
	int i_22_ = 0;
	while (enumeration.hasMoreElements()) {
	    TagValue tagvalue = (TagValue) enumeration.nextElement();
	    Tag tag = new Tag(tagvalue.getGroup(), tagvalue.getElement());
	    DicomObjectTableRow dicomobjecttablerow_23_
		= new DicomObjectTableRow(tag, dicomobject,
					  dicomobjecttablerow, i, _editable);
	    _rowdata.addElement(dicomobjecttablerow_23_);
	    if (_showSequenceItems && tag.isTypeSQ()) {
		int i_24_ = tagvalue.size();
		for (int i_25_ = 0; i_25_ < i_24_; i_25_++) {
		    DicomObject dicomobject_26_
			= (DicomObject) tagvalue.getValue(i_25_);
		    _rowdata.addElement
			(new DicomObjectTableRow(null, dicomobject_26_,
						 dicomobjecttablerow_23_,
						 i_25_, _editable));
		    _initRowData(dicomobject_26_, dicomobjecttablerow_23_,
				 i_25_);
		}
		_rowdata.addElement
		    (new DicomObjectTableRow(null, null,
					     dicomobjecttablerow_23_, i_24_,
					     _editable));
	    }
	    i_22_++;
	}
    }
    
    private int _addTag(Tag tag, DicomObject dicomobject,
			DicomObjectTableRow dicomobjecttablerow, int i,
			boolean bool, int i_27_) throws DicomException {
	DicomObjectTableRow dicomobjecttablerow_28_
	    = new DicomObjectTableRow(tag, dicomobject, dicomobjecttablerow, i,
				      bool);
	int i_29_ = dicomobjecttablerow_28_.getLevel();
	int i_30_ = _rowdata.size();
	int i_31_ = -1;
	int i_32_;
	for (i_32_ = i_27_; i_32_ < i_30_; i_32_++) {
	    DicomObjectTableRow dicomobjecttablerow_33_ = getRowAt(i_32_);
	    if (dicomobjecttablerow_33_.getLevel() <= i_29_) {
		if (dicomobjecttablerow_33_.getDicomObject() != dicomobject)
		    break;
		Tag tag_34_ = dicomobjecttablerow_33_.getTag();
		if (tag_34_ != null) {
		    i_31_ = tag_34_.compareTo(tag);
		    if (i_31_ >= 0)
			break;
		}
	    }
	}
	if (i_31_ == 0)
	    _rowdata.setElementAt(dicomobjecttablerow_28_, i_32_++);
	else
	    _rowdata.insertElementAt(dicomobjecttablerow_28_, i_32_++);
	if (tag.isTypeSQ())
	    i_32_ = _addSeqTags(dicomobjecttablerow_28_, bool, i_32_, 0);
	return i_32_;
    }
    
    private int _addTags(Tag[] tags, DicomObject dicomobject,
			 DicomObjectTableRow dicomobjecttablerow, int i,
			 boolean bool, int i_35_) throws DicomException {
	int i_36_ = i_35_;
	for (int i_37_ = 0; i_37_ < tags.length; i_37_++)
	    i_36_ = _addTag(tags[i_37_], dicomobject, dicomobjecttablerow, i,
			    bool, i_36_);
	return i_36_;
    }
    
    private int _addSeqTags(DicomObjectTableRow dicomobjecttablerow,
			    boolean bool, int i,
			    int i_38_) throws DicomException {
	Tag[] tags = dicomobjecttablerow.getTag().getSQTags();
	int i_39_ = i;
	int i_40_ = dicomobjecttablerow.getSize();
	int i_41_ = dicomobjecttablerow.getLevel() + 1;
	for (/**/; i_38_ < i_40_; i_38_++) {
	    DicomObject dicomobject
		= (DicomObject) dicomobjecttablerow.getValue(i_38_);
	    if (i_39_ == _rowdata.size()
		|| getRowAt(i_39_).getItemIndex() != i_38_)
		_rowdata.insertElementAt
		    (new DicomObjectTableRow(null, dicomobject,
					     dicomobjecttablerow, i_38_, bool),
		     i_39_++);
	    if (tags != null)
		i_39_ = _addTags(tags, dicomobject, dicomobjecttablerow, i_38_,
				 bool, i_39_);
	    for (/**/; i_39_ < _rowdata.size(); i_39_++) {
		DicomObjectTableRow dicomobjecttablerow_42_ = getRowAt(i_39_);
		if (dicomobjecttablerow_42_.getLevel() <= i_41_
		    && dicomobjecttablerow_42_.getDicomObject() != dicomobject)
		    break;
	    }
	}
	if (i_40_ >= 0 && (i_39_ == _rowdata.size()
			   || getRowAt(i_39_).getItemIndex() != i_40_))
	    _rowdata.insertElementAt
		(new DicomObjectTableRow(null, null, dicomobjecttablerow,
					 i_40_, bool),
		 i_39_++);
	return i_39_;
    }
}
