/* JSizeColumnsToFitTable - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.awt.Component;
import java.awt.Dimension;
import java.io.PrintWriter;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

public class JSizeColumnsToFitTable extends JTable
{
    private int _columnExtraWidth = 0;
    
    public JSizeColumnsToFitTable(TableModel tablemodel) {
	super(tablemodel);
	this.setAutoResizeMode(0);
	sizeColumnWidthToFit();
    }
    
    public void tableChanged(TableModelEvent tablemodelevent) {
	super.tableChanged(tablemodelevent);
	if (defaultRenderersByColumnClass != null)
	    sizeColumnWidthToFit();
    }
    
    public void sizeColumnWidthToFit() {
	this.getColumnModel().setColumnMargin(5);
	int i = this.getColumnCount();
	for (int i_0_ = 0; i_0_ < i; i_0_++)
	    _sizeColumnWidthToFit(i_0_);
	this.setPreferredScrollableViewportSize
	    (new Dimension(this.getColumnModel().getTotalColumnWidth(),
			   ((1 + this.getRowCount()) * this.getRowHeight()
			    + 5)));
	this.getTableHeader().resizeAndRepaint();
	this.resizeAndRepaint();
    }
    
    public void toHTML(PrintWriter printwriter, String string) {
	_writeHTMLHeader(printwriter, string);
	int i = this.getRowCount();
	for (int i_1_ = 0; i_1_ < i; i_1_++)
	    _writeHTMLRow(printwriter, i_1_);
	_writeHTMLFooter(printwriter);
    }
    
    private void _writeHTMLHeader(PrintWriter printwriter, String string) {
	printwriter.println("<HTML>");
	printwriter.print("<HEAD><TITLE>");
	printwriter.print(string);
	printwriter.println("</TITLE></HEAD>");
	printwriter.println("<BODY>");
	printwriter.println("<TABLE WIDTH=\"100%\" BORDER>");
	printwriter.print("<TR>");
	int i = this.getColumnCount();
	for (int i_2_ = 0; i_2_ < i; i_2_++) {
	    printwriter.print("<TH>");
	    printwriter.print(this.getColumnName(i_2_));
	    printwriter.print("</TH>");
	}
	printwriter.println("</TR>");
    }
    
    private void _writeHTMLRow(PrintWriter printwriter, int i) {
	printwriter.print("<TR>");
	int i_3_ = this.getColumnCount();
	for (int i_4_ = 0; i_4_ < i_3_; i_4_++) {
	    printwriter.print("<TD>");
	    printwriter.print(this.getValueAt(i, i_4_));
	    printwriter.print("</TD>");
	}
	printwriter.println("</TR>");
    }
    
    private void _writeHTMLFooter(PrintWriter printwriter) {
	printwriter.println("</TABLE>");
	printwriter.println("</BODY>");
	printwriter.println("</HTML>");
    }
    
    private void _sizeColumnWidthToFit(int i) {
	TableColumn tablecolumn = this.getColumnModel().getColumn(i);
	if (tablecolumn != null) {
	    TableCellRenderer tablecellrenderer
		= tablecolumn.getHeaderRenderer();
	    if (tablecellrenderer == null) {
		tablecellrenderer = this.getDefaultRenderer(String.class);
		_columnExtraWidth = 5;
	    }
	    int i_5_ = _columnHeaderWidth(tablecellrenderer, tablecolumn);
	    int i_6_ = this.getRowCount();
	    for (int i_7_ = 0; i_7_ < i_6_; i_7_++)
		i_5_ = Math.max(_columnCellWidth(i_7_, i), i_5_);
	    tablecolumn.setWidth(i_5_);
	    tablecolumn.setPreferredWidth(i_5_);
	}
    }
    
    private int _columnHeaderWidth(TableCellRenderer tablecellrenderer,
				   TableColumn tablecolumn) {
	Component component = (tablecellrenderer.getTableCellRendererComponent
			       (this, tablecolumn.getHeaderValue(), false,
				false, -1, tablecolumn.getModelIndex()));
	return component.getPreferredSize().width + _columnExtraWidth;
    }
    
    private int _columnCellWidth(int i, int i_8_) {
	Component component
	    = (this.getCellRenderer(i, i_8_).getTableCellRendererComponent
	       (this, this.getValueAt(i, i_8_), false, false, i, i_8_));
	return component.getPreferredSize().width + _columnExtraWidth;
    }
}
