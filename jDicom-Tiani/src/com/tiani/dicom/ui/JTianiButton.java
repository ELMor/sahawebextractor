/* JTianiButton - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.ui;
import java.applet.AppletContext;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class JTianiButton extends JButton
{
    public JTianiButton(final AppletContext acx) {
	super(new ImageIcon(JTianiButton.class.getResource
			    ("/com/tiani/dicom/icons/Tiani.gif")));
	this.setMargin(new Insets(0, 0, 0, 0));
	this.setBorderPainted(false);
	this.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent actionevent) {
		try {
		    if (acx != null)
			acx.showDocument(new URL("http://www.tiani.com"),
					 "_blank");
		    else {
			try {
			    Runtime.getRuntime()
				.exec("start http://www.tiani.com");
			} catch (Exception exception) {
			    Runtime.getRuntime()
				.exec("cmd.exe /c start http://www.tiani.com");
			}
		    }
		} catch (Exception exception) {
		    /* empty */
		}
	    }
	});
    }
}
