/* DicomDir2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.media;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.util.IDicomObjectFilter;
import com.tiani.dicom.util.UIDUtils;

public class DicomDir2 extends DicomObject
{
    public static final String TIANI_DICOMDIR_UID = "1.2.40.0.13.0.1.3.10";
    public static final String TIANI_RECORD_UID = "1.2.40.0.13.0.1.3.10.1";
    static final Long LONG0 = new Long(0L);
    private int readLock = 0;
    private int writeLock = 0;
    private DREntity root = null;
    private int size = 0;
    private boolean itemOffset = false;
    
    public DicomDir2() {
	/* empty */
    }
    
    public DicomDir2(String string, String string_0_)
	throws DicomException, IllegalValueException {
	this(string, string_0_, "1.2.840.10008.1.3.10", 8194);
    }
    
    public DicomDir2
	(String string, String string_1_, String string_2_, int i)
	throws DicomException, IllegalValueException {
	this.set(38, string);
	this.setFileMetaInformation(new FileMetaInformation(string_2_,
							    string_1_,
							    UID.getUIDEntry
								(i)
								.getValue()));
	reset();
    }
    
    public void reset() throws DicomException {
	this.set(44, null);
	this.set(41, LONG0);
	this.set(42, LONG0);
	this.set(43, new Integer(0));
	root = null;
	size = 0;
    }
    
    public DicomDir2(String string)
	throws DicomException, IllegalValueException {
	this(string, UIDUtils.createUID());
    }
    
    public final String getFileSetID() {
	return (String) this.get(38);
    }
    
    public final String getFileSetUID() {
	DicomObject dicomobject = this.getFileMetaInformation();
	return dicomobject != null ? (String) dicomobject.get(30) : null;
    }
    
    public final String getFileSetTransferSyntaxUID() {
	DicomObject dicomobject = this.getFileMetaInformation();
	return dicomobject != null ? (String) dicomobject.get(31) : null;
    }
    
    private int getTransferSyntax() {
	DicomObject dicomobject = this.getFileMetaInformation();
	String string = (String) dicomobject.get(31);
	try {
	    return UID.getUIDEntry(string).getConstant();
	} catch (UnknownUIDException unknownuidexception) {
	    throw new RuntimeException(unknownuidexception.getMessage());
	}
    }
    
    public void write(OutputStream outputstream)
	throws DicomException, IOException {
	int i = getTransferSyntax();
	if (itemOffset) {
	    ByteArrayOutputStream bytearrayoutputstream
		= new ByteArrayOutputStream();
	    super.write(bytearrayoutputstream, true, i, false, false);
	    this.clear();
	    ByteArrayInputStream bytearrayinputstream
		= new ByteArrayInputStream(bytearrayoutputstream
					       .toByteArray());
	    super.read(bytearrayinputstream, false);
	    init();
	}
	super.write(outputstream, true, i, false, false);
    }
    
    public void write(OutputStream outputstream, boolean bool, int i,
		      boolean bool_3_, boolean bool_4_) throws DicomException {
	throw new DicomException
		  ("Invalid use of DicomObject.write instead DicomDir.write");
    }
    
    private Long resolveItemOffset(DicomObject dicomobject, int i)
	throws DicomException {
	Long var_long = (Long) dicomobject.get(i);
	if (var_long.longValue() < 0L) {
	    var_long = new Long(this.getOffset(44, -var_long.intValue()));
	    dicomobject.set(i, var_long);
	}
	return var_long;
    }
    
    public void init() throws DicomException {
	size = this.getSize(44);
	if (size <= 0)
	    size = 0;
	else {
	    DRNode drnode = new DRNode((DicomObject) this.get(44, 0));
	    HashMap hashmap = new HashMap(size * 4 / 3 | 0x1);
	    for (int i = 1; i < size; i++)
		hashmap.put(new Long(this.getOffset(44, i)),
			    new DRNode((DicomObject) this.get(44, i)));
	    resolve(drnode, hashmap);
	    if (!hashmap.isEmpty())
		throw new DicomException("unreferenced DRs detected");
	    root = new DREntity(drnode);
	    resolveItemOffset(this, 41);
	    resolveItemOffset(this, 42);
	    itemOffset = false;
	}
    }
    
    public DREntity getRootEntity() {
	return root;
    }
    
    private void resolve(DRNode drnode, HashMap hashmap)
	throws DicomException {
	DicomObject dicomobject = drnode.getDataset();
	Long var_long = resolveItemOffset(dicomobject, 47);
	if (var_long.longValue() != 0L) {
	    DRNode drnode_5_ = (DRNode) hashmap.remove(var_long);
	    if (drnode_5_ == null)
		throw new DicomException
			  ("cannot resolve reference to lower level DR (offset: "
			   + Long.toHexString(var_long.longValue()) + "H )");
	    resolve(drnode_5_, hashmap);
	    drnode.setLowerEntity(new DREntity(drnode_5_));
	}
	Long var_long_6_ = resolveItemOffset(dicomobject, 45);
	if (var_long_6_.longValue() != 0L) {
	    DRNode drnode_7_ = (DRNode) hashmap.remove(var_long_6_);
	    if (drnode_7_ == null)
		throw new DicomException
			  ("cannot resolve reference to next DR (offset: "
			   + Long.toHexString(var_long_6_.longValue())
			   + "H )");
	    drnode.setNext(drnode_7_);
	    resolve(drnode_7_, hashmap);
	}
    }
    
    private Long appendDR(DicomObject dicomobject) throws DicomException {
	this.set(44, dicomobject, size);
	Long var_long
	    = new Long(size > 0 ? (long) -size
		       : this.calculateOffset(44, size, getTransferSyntax(),
					      false, false));
	size++;
	itemOffset = true;
	return var_long;
    }
    
    public DRNode addRootNode(DRNode drnode) throws DicomException {
	Long var_long = appendDR(drnode.getDataset());
	if (root == null) {
	    root = new DREntity(drnode);
	    this.set(41, var_long);
	} else
	    root.add(drnode, var_long);
	this.set(42, var_long);
	return drnode;
    }
    
    public DRNode addRootNodeIfNew(DRNode drnode) throws DicomException {
	DRNode drnode_8_;
	if (root != null && (drnode_8_ = root.find(drnode)) != null)
	    return drnode_8_;
	addRootNode(drnode);
	return drnode;
    }
    
    public DRNode findRootNode(String string, String string_9_) {
	return root != null ? root.find(string, string_9_) : null;
    }
    
    public void addLowerNode(DRNode drnode, DRNode drnode_10_)
	throws DicomException {
	Long var_long = appendDR(drnode.getDataset());
	drnode_10_.addLower(drnode, var_long);
    }
    
    public DRNode addLowerNodeIfNew(DRNode drnode, DRNode drnode_11_)
	throws DicomException {
	DRNode drnode_12_;
	if (drnode_11_.getLowerEntity() != null
	    && (drnode_12_ = drnode_11_.getLowerEntity().find(drnode)) != null)
	    return drnode_12_;
	addLowerNode(drnode, drnode_11_);
	return drnode;
    }
    
    public DRNode findLowerNode(String string, String string_13_,
				DRNode drnode) {
	return (drnode.getLowerEntity() != null
		? drnode.getLowerEntity().find(string, string_13_) : null);
    }
    
    public int addNodePath(DRNode[] drnodes) throws DicomException {
	int i = 0;
	DRNode drnode = addRootNodeIfNew(drnodes[0]);
	if (drnode != drnodes[0])
	    drnodes[0] = drnode;
	else
	    i++;
	for (int i_14_ = 1; i_14_ < drnodes.length; i_14_++) {
	    drnode = addLowerNodeIfNew(drnodes[i_14_], drnodes[i_14_ - 1]);
	    if (drnode != drnodes[i_14_])
		drnodes[i_14_] = drnode;
	    else
		i++;
	}
	return i;
    }
    
    private DRNode[] cloneNodePath(DRNode[] drnodes) {
	DRNode[] drnodes_15_ = new DRNode[drnodes.length];
	System.arraycopy(drnodes, 0, drnodes_15_, 0, drnodes.length);
	return drnodes_15_;
    }
    
    private void fillNodePathsList
	(DREntity drentity, int i, DRNode[] drnodes, String[] strings,
	 IDicomObjectFilter[] idicomobjectfilters, ArrayList arraylist)
	throws DicomException {
	int i_16_ = i + 1;
	Iterator iterator
	    = drentity.iterator(strings[i], (idicomobjectfilters != null
					     ? idicomobjectfilters[i] : null));
	while (iterator.hasNext()) {
	    DRNode drnode;
	    drnodes[i] = drnode = (DRNode) iterator.next();
	    if (i_16_ == drnodes.length)
		arraylist.add(cloneNodePath(drnodes));
	    else {
		DREntity drentity_17_;
		if ((drentity_17_ = drnode.getLowerEntity()) != null)
		    fillNodePathsList(drentity_17_, i_16_, drnodes, strings,
				      idicomobjectfilters, arraylist);
	    }
	}
    }
    
    public ArrayList listNodePaths
	(String[] strings, IDicomObjectFilter[] idicomobjectfilters)
	throws DicomException {
	if (strings == null)
	    throw new NullPointerException();
	if (strings.length == 0)
	    throw new IllegalArgumentException();
	if (idicomobjectfilters != null
	    && idicomobjectfilters.length != strings.length)
	    throw new IllegalArgumentException();
	ArrayList arraylist = new ArrayList();
	if (root == null)
	    return arraylist;
	DRNode[] drnodes = new DRNode[strings.length];
	fillNodePathsList(root, 0, drnodes, strings, idicomobjectfilters,
			  arraylist);
	return arraylist;
    }
    
    public void compact() throws DicomException {
	Iterator iterator = root.iterator();
	reset();
	while (iterator.hasNext()) {
	    DRNode drnode = (DRNode) iterator.next();
	    DicomObject dicomobject = drnode.getDataset();
	    dicomobject.set(45, LONG0);
	    dicomobject.set(47, LONG0);
	    DRNode drnode_18_ = new DRNode(dicomobject);
	    addRootNode(drnode_18_);
	    DREntity drentity;
	    if ((drentity = drnode.getLowerEntity()) != null)
		compactLowerEntity(drentity, drnode_18_);
	}
    }
    
    private void compactLowerEntity(DREntity drentity, DRNode drnode)
	throws DicomException {
	Iterator iterator = drentity.iterator();
	while (iterator.hasNext()) {
	    DRNode drnode_19_ = (DRNode) iterator.next();
	    DicomObject dicomobject = drnode_19_.getDataset();
	    dicomobject.set(45, LONG0);
	    dicomobject.set(47, LONG0);
	    DRNode drnode_20_ = new DRNode(dicomobject);
	    addLowerNode(drnode_20_, drnode);
	    DREntity drentity_21_;
	    if ((drentity_21_ = drnode_19_.getLowerEntity()) != null)
		compactLowerEntity(drentity_21_, drnode_20_);
	}
    }
    
    public synchronized int addReadLock() {
	while (writeLock > 0) {
	    try {
		this.wait();
	    } catch (InterruptedException interruptedexception) {
		/* empty */
	    }
	}
	return ++readLock;
    }
    
    public synchronized int addWriteLock() {
	while (writeLock > 0 || readLock > 0) {
	    try {
		this.wait();
	    } catch (InterruptedException interruptedexception) {
		/* empty */
	    }
	}
	return ++writeLock;
    }
    
    public synchronized int releaseReadLock() {
	readLock--;
	this.notifyAll();
	return readLock;
    }
    
    public synchronized int releaseWriteLock() {
	writeLock--;
	this.notifyAll();
	return writeLock;
    }
}
