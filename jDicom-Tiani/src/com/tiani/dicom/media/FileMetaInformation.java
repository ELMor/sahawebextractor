/* FileMetaInformation - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.media;
import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public final class FileMetaInformation extends DicomObject
{
    private static final byte[] _FMI_VERSION = { 0, 1 };
    public static final String IMPLEMENTATION_CLASS_UID
	= "1.2.40.0.13.0.0.113";
    public static final String IMPLEMENTATION_VERSION_NAME
	= "TIANI_JDICOM_113";
    
    public FileMetaInformation(String string, String string_0_,
			       String string_1_, String string_2_,
			       String string_3_) {
	try {
	    this.set(28, _FMI_VERSION);
	    this.set(29, string);
	    this.set(30, string_0_);
	    this.set(31, string_1_);
	    this.set(32, string_2_);
	    this.set(33, string_3_);
	} catch (DicomException dicomexception) {
	    Debug.out.println("jdicom: " + dicomexception);
	}
    }
    
    public FileMetaInformation(String string, String string_4_,
			       String string_5_) {
	this(string, string_4_, string_5_, "1.2.40.0.13.0.0.113",
	     "TIANI_JDICOM_113");
    }
    
    public FileMetaInformation(String string, String string_6_) {
	this(string, string_6_, "1.2.840.10008.1.2", "1.2.40.0.13.0.0.113",
	     "TIANI_JDICOM_113");
    }
    
    public FileMetaInformation(DicomObject dicomobject, String string,
			       String string_7_, String string_8_) {
	this((String) dicomobject.get(62), (String) dicomobject.get(63),
	     string, string_7_, string_8_);
    }
    
    public FileMetaInformation(DicomObject dicomobject, String string) {
	this(dicomobject, string, "1.2.40.0.13.0.0.113", "TIANI_JDICOM_113");
    }
    
    public FileMetaInformation(DicomObject dicomobject) {
	this(dicomobject, "1.2.840.10008.1.2", "1.2.40.0.13.0.0.113",
	     "TIANI_JDICOM_113");
    }
    
    public void setSourceAET(String string) throws DicomException {
	this.set(34, string);
    }
    
    public void setPrivateInformation(String string, byte[] is)
	throws DicomException {
	this.set(35, string);
	this.set(36, is);
    }
    
    public void check() throws DicomException {
	check(this);
    }
    
    public static void check(DicomObject dicomobject) throws DicomException {
	if (dicomobject == null)
	    throw new IllegalArgumentException("Missing FileMetaInformation");
	_checkExist(dicomobject, 28);
	_checkExist(dicomobject, 29);
	_checkExist(dicomobject, 30);
	_checkExist(dicomobject, 31);
	_checkExist(dicomobject, 32);
    }
    
    private static void _checkExist(DicomObject dicomobject, int i)
	throws DicomException {
	if (dicomobject.getSize(i) < 1)
	    throw new DicomException("Missing " + DDict.getDescription(i));
    }
}
