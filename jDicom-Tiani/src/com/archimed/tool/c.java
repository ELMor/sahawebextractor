/* c - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.tool;
import java.util.Enumeration;

public class c
{
    private m[] a;
    private int b;
    private int c;
    private float d;
    
    public c(int i, float f) {
	if (i <= 0 || (double) f <= 0.0)
	    throw new IllegalArgumentException();
	d = f;
	a = new m[i];
	c = (int) ((float) i * f);
    }
    
    public c(int i) {
	this(i, 0.75F);
    }
    
    public c() {
	this(101, 0.75F);
    }
    
    public int size() {
	return b;
    }
    
    public boolean isEmpty() {
	return b == 0;
    }
    
    public Enumeration keys() {
	return new z(a, true);
    }
    
    public Enumeration elements() {
	return new z(a, false);
    }
    
    public boolean contains(int i) {
	m[] var_ms = a;
	int i_0_ = var_ms.length;
	while (i_0_-- > 0) {
	    for (m var_m = var_ms[i_0_]; var_m != null; var_m = var_m.d) {
		if (var_m.c == i)
		    return true;
	    }
	}
	return false;
    }
    
    public boolean containsKey(int i) {
	m[] var_ms = a;
	int i_1_ = i;
	int i_2_ = (i_1_ & 0x7fffffff) % var_ms.length;
	for (m var_m = var_ms[i_2_]; var_m != null; var_m = var_m.d) {
	    if (var_m.a == i_1_ && var_m.b == i)
		return true;
	}
	return false;
    }
    
    public int get(int i) {
	m[] var_ms = a;
	int i_3_ = i;
	int i_4_ = (i_3_ & 0x7fffffff) % var_ms.length;
	for (m var_m = var_ms[i_4_]; var_m != null; var_m = var_m.d) {
	    if (var_m.a == i_3_ && var_m.b == i)
		return var_m.c;
	}
	return 2147483647;
    }
    
    protected void a() {
	int i = a.length;
	m[] var_ms = a;
	int i_5_ = i * 2 + 1;
	m[] var_ms_6_ = new m[i_5_];
	c = (int) ((float) i_5_ * d);
	a = var_ms_6_;
	int i_7_ = i;
	while (i_7_-- > 0) {
	    m var_m = var_ms[i_7_];
	    while (var_m != null) {
		m var_m_8_ = var_m;
		var_m = var_m.d;
		int i_9_ = (var_m_8_.a & 0x7fffffff) % i_5_;
		var_m_8_.d = var_ms_6_[i_9_];
		var_ms_6_[i_9_] = var_m_8_;
	    }
	}
    }
    
    public int put(int i, int i_10_) {
	m[] var_ms = a;
	int i_11_ = i;
	int i_12_ = (i_11_ & 0x7fffffff) % var_ms.length;
	for (m var_m = var_ms[i_12_]; var_m != null; var_m = var_m.d) {
	    if (var_m.a == i_11_ && var_m.b == i) {
		int i_13_ = var_m.c;
		var_m.c = i_10_;
		return i_13_;
	    }
	}
	if (b >= c) {
	    a();
	    return put(i, i_10_);
	}
	m var_m = new m();
	var_m.a = i_11_;
	var_m.b = i;
	var_m.c = i_10_;
	var_m.d = var_ms[i_12_];
	var_ms[i_12_] = var_m;
	b++;
	return 2147483647;
    }
    
    public int remove(int i) {
	m[] var_ms = a;
	int i_14_ = i;
	int i_15_ = (i_14_ & 0x7fffffff) % var_ms.length;
	m var_m = var_ms[i_15_];
	m var_m_16_ = null;
	for (/**/; var_m != null; var_m = var_m.d) {
	    if (var_m.a == i_14_ && var_m.b == i) {
		if (var_m_16_ != null)
		    var_m_16_.d = var_m.d;
		else
		    var_ms[i_15_] = var_m.d;
		b--;
		return var_m.c;
	    }
	    var_m_16_ = var_m;
	}
	return 2147483647;
    }
    
    public synchronized void clear() {
	m[] var_ms = a;
	int i = var_ms.length;
	while (--i >= 0)
	    var_ms[i] = null;
	b = 0;
    }
}
