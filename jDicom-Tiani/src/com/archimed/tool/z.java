/* z - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.tool;
import java.util.Enumeration;
import java.util.NoSuchElementException;

class z implements Enumeration
{
    boolean a;
    int b;
    m[] c;
    m d;
    
    z(m[] var_ms, boolean bool) {
	c = var_ms;
	a = bool;
	b = var_ms.length;
    }
    
    public boolean hasMoreElements() {
	if (d != null)
	    return true;
	while (b-- > 0) {
	    if ((d = c[b]) != null)
		return true;
	}
	return false;
    }
    
    public Object nextElement() {
	if (d == null) {
	    while (b-- > 0 && (d = c[b]) == null) {
		/* empty */
	    }
	}
	if (d != null) {
	    m var_m = d;
	    d = var_m.d;
	    return a ? new Integer(var_m.b) : new Integer(var_m.c);
	}
	throw new NoSuchElementException("IntHashtableEnumerator");
    }
}
