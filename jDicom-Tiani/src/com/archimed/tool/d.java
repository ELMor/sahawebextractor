/* d - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.tool;
import java.util.Enumeration;

public class d
{
    protected Object[] a;
    protected int b;
    
    public d(int i) {
	a = new Object[i];
    }
    
    public d() {
	this(10);
    }
    
    public d(Object[] objects, int i) {
	b = i;
	a = new Object[b];
	System.arraycopy(objects, 0, a, 0, b);
    }
    
    public d(Object[] objects) {
	this(objects, objects.length);
    }
    
    public void trimToSize() {
	int i = a.length;
	if (b < i) {
	    Object[] objects = a;
	    a = new Object[b];
	    System.arraycopy(objects, 0, a, 0, b);
	}
    }
    
    public void ensureCapacity(int i) {
	int i_0_ = a.length;
	if (i > i_0_) {
	    Object[] objects = a;
	    int i_1_ = i_0_ * 3 / 2 + 1;
	    if (i_1_ < i)
		i_1_ = i;
	    a = new Object[i_1_];
	    System.arraycopy(objects, 0, a, 0, b);
	}
    }
    
    public int size() {
	return b;
    }
    
    public boolean isEmpty() {
	return b == 0;
    }
    
    public boolean contains(Object object) {
	return indexOf(object) >= 0;
    }
    
    public int indexOf(Object object) {
	if (object == null) {
	    for (int i = 0; i < b; i++) {
		if (a[i] == null)
		    return i;
	    }
	} else {
	    for (int i = 0; i < b; i++) {
		if (object.equals(a[i]))
		    return i;
	    }
	}
	return -1;
    }
    
    public int lastIndexOf(Object object) {
	if (object == null) {
	    for (int i = b - 1; i >= 0; i--) {
		if (a[i] == null)
		    return i;
	    }
	} else {
	    for (int i = b - 1; i >= 0; i--) {
		if (object.equals(a[i]))
		    return i;
	    }
	}
	return -1;
    }
    
    public Object[] toArray() {
	Object[] objects = new Object[b];
	System.arraycopy(a, 0, objects, 0, b);
	return objects;
    }
    
    public Object get(int i) {
	a(i);
	return a[i];
    }
    
    public Object set(int i, Object object) {
	a(i);
	Object object_2_ = a[i];
	a[i] = object;
	return object_2_;
    }
    
    public boolean add(Object object) {
	ensureCapacity(b + 1);
	a[b++] = object;
	return true;
    }
    
    public void add(int i, Object object) {
	if (i > b || i < 0)
	    throw new IndexOutOfBoundsException("Index: " + i + ", Size: "
						+ b);
	ensureCapacity(b + 1);
	System.arraycopy(a, i, a, i + 1, b - i);
	a[i] = object;
	b++;
    }
    
    public Object remove(int i) {
	a(i);
	Object object = a[i];
	int i_3_ = b - i - 1;
	if (i_3_ > 0)
	    System.arraycopy(a, i + 1, a, i, i_3_);
	a[--b] = null;
	return object;
    }
    
    public void clear() {
	for (int i = 0; i < b; i++)
	    a[i] = null;
	b = 0;
    }
    
    public Enumeration elements() {
	return new l(this);
    }
    
    private void a(int i) {
	if (i >= b || i < 0)
	    throw new IndexOutOfBoundsException("Index: " + i + ", Size: "
						+ b);
    }
}
