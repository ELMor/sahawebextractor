/* p - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.codec.jpeg;
import java.io.IOException;

public class p
{
    public static final int SOF3 = 195;
    public static final int DHT = 196;
    public static final int SOI = 216;
    public static final int EOI = 217;
    public static final int SOS = 218;
    public static final int DRI = 221;
    public static final int RST0 = 208;
    public static final int[] bmask
	= { 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191,
	    16383, 32767, 65535 };
    private q a = new q();
    private int b;
    private int c;
    private int d;
    private int e;
    private long f;
    private int g = 0;
    private byte h;
    private byte i;
    private byte[] j;
    private byte[] k;
    private int l = 0;
    private int m = 0;
    static final int[] n = { 0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024,
			     2048, 4096, 8192, 16384, 32768 };
    static final int[] o
	= { 0, -1, -3, -7, -15, -31, -63, -127, -255, -511, -1023, -2047,
	    -4095, -8191, -16383, -32767, -65535 };
    
    private final int a() {
	int i = (j[l++] & 0xff) << 8;
	i += j[l++] & 0xff;
	return i;
    }
    
    private final void b() throws IOException {
	boolean bool = false;
	boolean bool_0_ = false;
	boolean bool_1_ = false;
	boolean bool_2_ = false;
	int i = a();
	if ((i & 0xff) != 216)
	    throw new IOException("Not a valid lossless JPEG file");
	do {
	    i = a();
	    switch (i & 0xff) {
	    case 196: {
		r var_r = new r();
		l = var_r.read(j, l);
		a.hts[var_r.a] = var_r;
		break;
	    }
	    case 195: {
		int i_3_ = a();
		d = j[l++];
		c = a();
		b = a();
		int i_4_ = j[l++];
		a.fcomps = new s[i_4_ + 1];
		for (int i_5_ = 0; i_5_ < i_4_; i_5_++) {
		    byte i_6_ = j[l++];
		    a.fcomps[i_6_] = new s();
		    int i_7_ = j[l++];
		    a.fcomps[i_6_].a = (i_7_ & 0xf0) >> 4;
		    a.fcomps[i_6_].b = i_7_ & 0xf;
		    a.fcomps[i_6_].c = j[l++];
		}
		break;
	    }
	    case 218: {
		int i_8_ = a();
		e = j[l++];
		a.scomps = new t[e];
		for (int i_9_ = 0; i_9_ < e; i_9_++) {
		    byte i_10_ = j[l++];
		    a.scomps[i_9_] = new t();
		    int i_11_ = j[l++];
		    a.scomps[i_9_].a = (i_11_ & 0xf0) >> 4;
		    a.scomps[i_9_].b = i_11_ & 0xf;
		}
		a.a = j[l++];
		l += 2;
		break;
	    }
	    case 221: {
		int i_12_ = a();
		a.d = a();
		break;
	    }
	    default:
		throw new RuntimeException("Unexpected marker: "
					   + Integer.toHexString(i));
	    }
	} while ((i & 0xff) != 218);
    }
    
    private final void a(int i) {
	for (/**/; g < 57; g += 8) {
	    if (l == j.length) {
		h = (byte) 0;
		break;
	    }
	    h = j[l++];
	    if (h == -1) {
		this.i = j[l++];
		if (this.i != 0) {
		    l = l - 2;
		    if (g >= i)
			break;
		    h = (byte) 0;
		}
	    }
	    f = f << 8 | (long) (h & 0xff);
	}
    }
    
    private final int c() {
	if (g < 8)
	    a(8);
	return (int) (f >> g - 8 & 0xffL);
    }
    
    private final void b(int i) {
	g -= i;
    }
    
    private final int d() {
	if (g == 0)
	    a(1);
	return (int) (f >> --g) & 0x1;
    }
    
    private final int c(int i) {
	if (g < i)
	    a(i);
	return (int) (f >> (g -= i) & (long) bmask[i]);
    }
    
    private final int a(int i, int i_13_) {
	if (i < n[i_13_])
	    return i + o[i_13_];
	return i;
    }
    
    private int a(int i, int i_14_, int i_15_) {
	switch (a.a) {
	case 0:
	    break;
	case 1:
	    return i;
	case 2:
	    return i_14_;
	case 3:
	    return i_15_;
	case 4:
	    return i + i_14_ - i_15_;
	case 5:
	    return i + (i_14_ - i_15_ >> 1);
	case 6:
	    return i_14_ + (i - i_15_ >> 1);
	case 7:
	    return i + i_14_ >> 1;
	}
	return 0;
    }
    
    private int d(int i) throws IOException {
	int i_16_ = c();
	int i_17_;
	if (a.hts[i].j[i_16_] != 0) {
	    b(a.hts[i].j[i_16_]);
	    i_17_ = a.hts[i].k[i_16_];
	} else {
	    b(8);
	    int i_18_;
	    for (i_18_ = 8; i_16_ > a.hts[i].h[i_18_]; i_18_++) {
		int i_19_ = d();
		i_16_ = i_16_ << 1 | i_19_;
	    }
	    if (i_18_ > 16)
		throw new IOException("bad Huffman code");
	    int i_20_ = a.hts[i].i[i_18_];
	    i_20_ += i_16_ - a.hts[i].g[i_18_];
	    i_17_ = a.hts[i].d[i_20_];
	}
	return i_17_;
    }
    
    private void e() throws IOException {
	boolean bool = false;
	boolean bool_21_ = false;
	boolean bool_22_ = false;
	for (int i = 0; i < e; i++) {
	    int i_23_ = a.scomps[i].a;
	    int i_24_ = d(i_23_);
	    int i_25_;
	    if (i_24_ == 0)
		i_25_ = 0;
	    else if (i_24_ == 16)
		i_25_ = 32768;
	    else {
		i_25_ = c(i_24_);
		i_25_ = a(i_25_, i_24_);
	    }
	    if (d > 8) {
		byte i_26_ = (byte) ((1 << d - 1) + i_25_);
		k[m] = i_26_;
		i_26_ = (byte) ((1 << d - 1) + i_25_ >> 8);
		k[m + 1] = i_26_;
		m += 2;
	    } else {
		byte i_27_ = (byte) ((1 << d - 1) + i_25_);
		k[m] = i_27_;
		m++;
	    }
	}
	for (int i = 1; i < b; i++) {
	    for (int i_28_ = 0; i_28_ < e; i_28_++) {
		int i_29_ = a.scomps[i_28_].a;
		int i_30_ = d(i_29_);
		int i_31_;
		if (i_30_ == 0)
		    i_31_ = 0;
		else if (i_30_ == 16)
		    i_31_ = 32768;
		else {
		    i_31_ = c(i_30_);
		    i_31_ = a(i_31_, i_30_);
		}
		if (d > 8) {
		    byte i_32_ = (byte) (a(k, m - 2 * e) + i_31_);
		    k[m] = i_32_;
		    i_32_ = (byte) (a(k, m - 2 * e) + i_31_ >> 8);
		    k[m + 1] = i_32_;
		    m += 2;
		} else {
		    byte i_33_ = (byte) ((k[m - e] & 0xff) + i_31_);
		    k[m] = i_33_;
		    m++;
		}
	    }
	}
	if (a.b != 0)
	    a.c--;
    }
    
    private void f() throws IOException {
	int i = g / 8;
	g = 0;
	int i_34_;
	for (;;) {
	    i_34_ = j[l++] & 0xff;
	    if (i_34_ == 255) {
		do
		    i_34_ = j[l++] & 0xff;
		while (i_34_ == 255);
		if (i_34_ != 0)
		    break;
	    }
	}
	if (i_34_ != 208 + a.e)
	    throw new IOException("messed up restart markers");
	a.c = a.b;
	a.e = a.e + 1 & 0x7;
    }
    
    private void g() throws IOException {
	m = 0;
	a.b = a.d / b;
	a.c = a.b;
	a.e = 0;
	e();
	for (int i = 1; i < c; i++) {
	    if (i != 0 && a.b != 0) {
		if (a.c == 0) {
		    f();
		    e();
		    continue;
		}
		a.c--;
	    }
	    for (int i_35_ = 0; i_35_ < e; i_35_++) {
		int i_36_ = a.scomps[i_35_].a;
		int i_37_ = d(i_36_);
		int i_38_;
		if (i_37_ == 0)
		    i_38_ = 0;
		else if (i_37_ == 16)
		    i_38_ = 32768;
		else {
		    i_38_ = c(i_37_);
		    i_38_ = a(i_38_, i_37_);
		}
		if (d > 8) {
		    byte i_39_ = (byte) (a(k, m - 2 * e * b) + i_38_);
		    k[m] = i_39_;
		    i_39_ = (byte) (a(k, m - 2 * e * b) + i_38_ >> 8);
		    k[m + 1] = i_39_;
		    m += 2;
		} else {
		    byte i_40_ = (byte) ((k[m - e * b] & 0xff) + i_38_);
		    k[m] = i_40_;
		    m++;
		}
	    }
	    for (int i_41_ = 1; i_41_ < b; i_41_++) {
		for (int i_42_ = 0; i_42_ < e; i_42_++) {
		    int i_43_ = a.scomps[i_42_].a;
		    int i_44_ = d(i_43_);
		    int i_45_;
		    if (i_44_ == 0)
			i_45_ = 0;
		    else if (i_44_ == 16)
			i_45_ = 32768;
		    else {
			i_45_ = c(i_44_);
			i_45_ = a(i_45_, i_44_);
		    }
		    if (d > 8) {
			int i_46_ = (a(a(k, m - 2 * e), a(k, m - b * 2 * e),
				       a(k, m - b * 2 * e - 2 * e))
				     + i_45_);
			byte i_47_ = (byte) i_46_;
			k[m] = i_47_;
			i_47_ = (byte) (i_46_ >> 8);
			k[m + 1] = i_47_;
			m += 2;
		    } else {
			byte i_48_
			    = (byte) (a(k[m - e] & 0xff, k[m - b * e] & 0xff,
					k[m - b * e - e] & 0xff)
				      + i_45_);
			k[m] = i_48_;
			m++;
		    }
		}
	    }
	}
    }
    
    private int a(byte[] is, int i) {
	return (is[i] & 0xff) + ((is[i + 1] & 0xff) << 8);
    }
    
    public byte[] decode(byte[] is, byte[] is_49_) throws IOException {
	g = 0;
	f = 0L;
	l = 0;
	j = is;
	k = is_49_;
	b();
	int i;
	if (d > 8)
	    i = b * c * e * 2;
	else
	    i = b * c * e;
	if (k == null)
	    k = new byte[i];
	else if (k.length != i)
	    throw new IllegalArgumentException
		      ("outbuffer.length does not match frame size");
	g();
	return k;
    }
}
