/* w - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.codec.jpeg;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class w
{
    private static int[] a
	= { 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191,
	    16383, 32767, 65535, 131071, 262143, 524287, 1048575, 2097151,
	    4194303, 8388607, 16777215, 33554431, 67108863, 134217727,
	    268435455, 536870911, 1073741823, 2147483647, -1 };
    private static final byte[] b
	= { 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    private static final byte[] c
	= { 0, 2, 3, 1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
    private static final int d = 216;
    private static final int e = 217;
    private static final int f = 195;
    private static final int g = 218;
    private static final int h = 196;
    private static int[] i = new int[257];
    private static byte[] j = new byte[257];
    private static int[] k = new int[257];
    private static byte[] l = new byte[257];
    private static int[] m = new int[256];
    private int n = 0;
    private int o = 0;
    private ByteArrayOutputStream p;
    
    private void a(int i) throws IOException {
	p.write(i);
    }
    
    private void a(short i, int i_0_) throws IOException {
	int i_1_ = i_0_;
	int i_2_ = i & a[i_1_];
	i_1_ += o;
	i_2_ <<= 24 - i_1_;
	i_2_ |= n;
	for (/**/; i_1_ >= 8; i_1_ -= 8) {
	    int i_3_ = i_2_ >> 16 & 0xff;
	    a(i_3_);
	    if (i_3_ == 255)
		a(0);
	    i_2_ <<= 8;
	}
	n = i_2_;
	o = i_1_;
    }
    
    private void b(int i) throws IOException {
	int i_4_ = i % 65536;
	if (i_4_ > 32768)
	    i_4_ -= 65536;
	else if (i_4_ < -32767)
	    i_4_ += 65536;
	int i_5_ = i_4_;
	if (i_4_ < 0) {
	    i_4_ = -i_4_;
	    i_5_--;
	}
	int i_6_ = 0;
	if (i_4_ != 0) {
	    for (/**/; i_4_ >= 256; i_4_ >>= 8)
		i_6_ += 8;
	    i_6_ += m[i_4_ & 0xff];
	}
	a((short) k[i_6_], l[i_6_]);
	if (i_6_ != 0 && i_6_ != 16)
	    a((short) i_5_, i_6_);
    }
    
    private void a(int i, OutputStream outputstream) throws IOException {
	outputstream.write(255);
	outputstream.write(i);
    }
    
    private void a(int i, int i_7_, int i_8_, int i_9_) throws IOException {
	p = new ByteArrayOutputStream();
	int i_10_ = 8 + 3 * i_9_;
	int i_11_ = 6 + 2 * i_9_;
	int i_12_ = 19;
	for (int i_13_ = 0; i_13_ < b.length; i_13_++)
	    i_12_ += b[i_13_];
	DataOutputStream dataoutputstream = new DataOutputStream(p);
	a(216, p);
	a(195, p);
	dataoutputstream.writeShort(i_10_);
	p.write(i_8_);
	dataoutputstream.writeShort(i_7_);
	dataoutputstream.writeShort(i);
	p.write(i_9_);
	for (int i_14_ = 0; i_14_ < i_9_; i_14_++) {
	    p.write(i_14_);
	    p.write(17);
	    p.write(0);
	}
	a(196, p);
	dataoutputstream.writeShort(i_12_);
	p.write(0);
	for (int i_15_ = 0; i_15_ < b.length; i_15_++)
	    p.write(b[i_15_]);
	for (int i_16_ = 0; i_16_ < c.length; i_16_++)
	    p.write(c[i_16_]);
	a(218, p);
	dataoutputstream.writeShort(i_11_);
	p.write(i_9_);
	for (int i_17_ = 0; i_17_ < i_9_; i_17_++) {
	    p.write(i_17_);
	    p.write(0);
	}
	p.write(1);
	p.write(0);
	p.write(0);
    }
    
    public byte[] encode8bitGrayscale(byte[] is, int i, int i_18_)
	throws IOException {
	a(i, i_18_, 8, 1);
	byte i_19_ = (byte) (is[0] - 128);
	b(i_19_);
	for (int i_20_ = 1; i_20_ < i; i_20_++) {
	    i_19_ = (byte) (is[i_20_] - is[i_20_ - 1]);
	    b(i_19_);
	}
	for (int i_21_ = 1; i_21_ < i_18_; i_21_++) {
	    i_19_ = (byte) (is[i * i_21_] - is[i * (i_21_ - 1)]);
	    b(i_19_);
	    for (int i_22_ = 1; i_22_ < i; i_22_++) {
		i_19_ = (byte) (is[i * i_21_ + i_22_]
				- is[i * i_21_ + i_22_ - 1]);
		b(i_19_);
	    }
	}
	a((short) 127, 7);
	a(217, p);
	return p.toByteArray();
    }
    
    public byte[] encode16bitGrayscale(byte[] is, int i, int i_23_)
	throws IOException {
	a(i, i_23_, 16, 1);
	int i_24_ = (short) ((is[1] << 8) + (is[0] & 0xff));
	short i_25_ = (short) (i_24_ - 32768);
	b(i_25_);
	for (int i_26_ = 1; i_26_ < i; i_26_++) {
	    short i_27_
		= (short) ((is[2 * i_26_ + 1] << 8) + (is[2 * i_26_] & 0xff));
	    short i_28_ = (short) ((is[2 * i_26_ - 1] << 8)
				   + (is[2 * i_26_ - 2] & 0xff));
	    i_25_ = (short) (i_27_ - i_28_);
	    b(i_25_);
	}
	for (int i_29_ = 1; i_29_ < i_23_; i_29_++) {
	    short i_30_ = (short) ((is[2 * i * i_29_ + 1] << 8)
				   + (is[2 * i * i_29_] & 0xff));
	    short i_31_ = (short) ((is[2 * i * (i_29_ - 1) + 1] << 8)
				   + (is[2 * i * (i_29_ - 1)] & 0xff));
	    i_25_ = (short) (i_30_ - i_31_);
	    b(i_25_);
	    for (int i_32_ = 1; i_32_ < i; i_32_++) {
		i_30_ = (short) ((is[2 * (i * i_29_ + i_32_) + 1] << 8)
				 + (is[2 * (i * i_29_ + i_32_)] & 0xff));
		i_31_ = (short) ((is[2 * (i * i_29_ + i_32_) - 1] << 8)
				 + (is[2 * (i * i_29_ + i_32_) - 2] & 0xff));
		i_25_ = (short) (i_30_ - i_31_);
		b(i_25_);
	    }
	}
	a((short) 127, 7);
	a(217, p);
	if ((p.size() & 0x1) != 0)
	    a(0);
	return p.toByteArray();
    }
    
    public byte[] encode16bitGrayscale(short[] is, int i, int i_33_)
	throws IOException {
	a(i, i_33_, 16, 1);
	short i_34_ = (short) (is[0] - 32768);
	b(i_34_);
	for (int i_35_ = 1; i_35_ < i; i_35_++) {
	    i_34_ = (short) (is[i_35_] - is[i_35_ - 1]);
	    b(i_34_);
	}
	for (int i_36_ = 1; i_36_ < i_33_; i_36_++) {
	    i_34_ = (short) (is[i * i_36_] - is[i * (i_36_ - 1)]);
	    b(i_34_);
	    for (int i_37_ = 1; i_37_ < i; i_37_++) {
		i_34_ = (short) (is[i * i_36_ + i_37_]
				 - is[i * i_36_ + i_37_ - 1]);
		b(i_34_);
	    }
	}
	a((short) 127, 7);
	if ((p.size() & 0x1) != 0)
	    a(0);
	return p.toByteArray();
    }
    
    public byte[] encode24bitColor(byte[] is, byte[] is_38_, byte[] is_39_,
				   int i, int i_40_) throws IOException {
	a(i, i_40_, 8, 3);
	byte i_41_ = (byte) (is[0] - 128);
	b(i_41_);
	i_41_ = (byte) (is_38_[0] - 128);
	b(i_41_);
	i_41_ = (byte) (is_39_[0] - 128);
	b(i_41_);
	for (int i_42_ = 1; i_42_ < i; i_42_++) {
	    i_41_ = (byte) (is[i_42_] - is[i_42_ - 1]);
	    b(i_41_);
	    i_41_ = (byte) (is_38_[i_42_] - is_38_[i_42_ - 1]);
	    b(i_41_);
	    i_41_ = (byte) (is_39_[i_42_] - is_39_[i_42_ - 1]);
	    b(i_41_);
	}
	for (int i_43_ = 1; i_43_ < i_40_; i_43_++) {
	    i_41_ = (byte) (is[i * i_43_] - is[i * (i_43_ - 1)]);
	    b(i_41_);
	    i_41_ = (byte) (is_38_[i * i_43_] - is_38_[i * (i_43_ - 1)]);
	    b(i_41_);
	    i_41_ = (byte) (is_39_[i * i_43_] - is_39_[i * (i_43_ - 1)]);
	    b(i_41_);
	    for (int i_44_ = 1; i_44_ < i; i_44_++) {
		i_41_ = (byte) (is[i * i_43_ + i_44_]
				- is[i * i_43_ + i_44_ - 1]);
		b(i_41_);
		i_41_ = (byte) (is_38_[i * i_43_ + i_44_]
				- is_38_[i * i_43_ + i_44_ - 1]);
		b(i_41_);
		i_41_ = (byte) (is_39_[i * i_43_ + i_44_]
				- is_39_[i * i_43_ + i_44_ - 1]);
		b(i_41_);
	    }
	}
	a((short) 127, 7);
	a(217, p);
	if ((p.size() & 0x1) != 0)
	    a(0);
	return p.toByteArray();
    }
    
    static {
	for (int i = 0; i < 256; i++) {
	    int i_45_ = i;
	    int i_46_ = 1;
	    while ((i_45_ >>= 1) != 0)
		i_46_++;
	    m[i] = i_46_;
	}
	int i = 0;
	boolean bool = true;
	boolean bool_47_ = true;
	for (int i_48_ = 0; i_48_ < 16; i_48_++) {
	    for (int i_49_ = 1; i_49_ <= b[i_48_]; i_49_++) {
		j[i] = (byte) (i_48_ + 1);
		i++;
	    }
	}
	j[i] = (byte) 0;
	int i_50_ = i;
	int i_51_ = 0;
	int i_52_ = j[0];
	i = 0;
	while (j[i] != 0) {
	    while (j[i] == i_52_) {
		w.i[i] = i_51_;
		i++;
		i_51_++;
	    }
	    i_51_ <<= 1;
	    i_52_++;
	}
	i = 0;
	do {
	    byte i_53_ = c[i];
	    k[i_53_] = w.i[i];
	    l[i_53_] = j[i];
	} while (++i < i_50_);
    }
}
