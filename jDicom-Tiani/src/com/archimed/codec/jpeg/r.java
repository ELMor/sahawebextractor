/* r - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.codec.jpeg;
import java.io.IOException;

public class r
{
    int a;
    int b;
    byte[] c = new byte[17];
    byte[] d = new byte[256];
    int[] e = new int[257];
    byte[] f = new byte[257];
    int[] g = new int[17];
    int[] h = new int[18];
    int[] i = new int[17];
    int[] j = new int[256];
    int[] k = new int[256];
    static int[] l
	= { -1, 2147483647, 1073741823, 536870911, 268435455, 134217727,
	    67108863, 33554431, 16777215, 8388607, 4194303, 2097151, 1048575,
	    524287, 262143, 131071, 65535, 32767, 16383, 8191, 4095, 2047,
	    1023, 511, 255, 127, 63, 31, 15, 7, 3, 1 };
    
    public int read(byte[] is, int i) throws IOException {
	boolean bool = false;
	boolean bool_0_ = false;
	boolean bool_1_ = false;
	b = is[i++] << 8;
	b += is[i++];
	b = b - 2;
	while (b > 0) {
	    int i_2_ = is[i++];
	    c[0] = (byte) 0;
	    int i_3_ = 0;
	    for (int i_4_ = 1; i_4_ <= 16; i_4_++) {
		c[i_4_] = is[i++];
		i_3_ += c[i_4_];
	    }
	    if (i_3_ > 256)
		throw new RuntimeException("Bogus DHT counts");
	    for (int i_5_ = 0; i_5_ < i_3_; i_5_++)
		d[i_5_] = is[i++];
	    b -= 17 + i_3_;
	    if ((i_2_ & 0x10) == 16)
		throw new RuntimeException
			  ("Huffman table for lossless JPEG is not defined");
	    a = i_2_;
	    if (i_2_ < 0 || i_2_ >= 4)
		throw new RuntimeException("Bogus DHT index " + i_2_);
	}
	int i_6_ = 0;
	boolean bool_7_ = true;
	boolean bool_8_ = true;
	for (int i_9_ = 1; i_9_ <= 16; i_9_++) {
	    for (int i_10_ = 1; i_10_ <= c[i_9_]; i_10_++)
		f[i_6_++] = (byte) i_9_;
	}
	f[i_6_] = (byte) 0;
	int i_11_ = i_6_;
	int i_12_ = 0;
	int i_13_ = f[0];
	i_6_ = 0;
	while (f[i_6_] != 0) {
	    while (f[i_6_] == i_13_) {
		e[i_6_++] = i_12_;
		i_12_++;
	    }
	    i_12_ <<= 1;
	    i_13_++;
	}
	int i_14_ = 0;
	for (int i_15_ = 1; i_15_ <= 16; i_15_++) {
	    if (c[i_15_] != 0) {
		this.i[i_15_] = i_14_;
		g[i_15_] = e[i_14_];
		i_14_ += c[i_15_];
		h[i_15_] = e[i_14_ - 1];
	    } else
		h[i_15_] = -1;
	}
	for (i_6_ = 0; i_6_ < i_11_; i_6_++) {
	    int i_16_ = f[i_6_];
	    if (i_16_ <= 8) {
		int i_17_ = d[i_6_];
		i_12_ = e[i_6_];
		int i_18_ = i_12_ << 8 - i_16_;
		int i_19_;
		if (i_16_ < 8)
		    i_19_ = i_18_ | l[24 + i_16_];
		else
		    i_19_ = i_18_;
		for (int i_20_ = i_18_; i_20_ <= i_19_; i_20_++) {
		    j[i_20_] = i_16_;
		    k[i_20_] = i_17_;
		}
	    }
	}
	return i;
    }
}
