/* v - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.codec.rle;
import java.io.IOException;
import java.io.InputStream;

public class v
{
    public static int get4bytes(InputStream inputstream) throws IOException {
	int i = inputstream.read();
	i += inputstream.read() << 8;
	i += inputstream.read() << 16;
	i += inputstream.read() << 24;
	return i;
    }
}
