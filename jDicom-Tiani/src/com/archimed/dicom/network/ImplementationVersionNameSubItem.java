/* ImplementationVersionNameSubItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.tool.n;

class ImplementationVersionNameSubItem extends SubItem
{
    private int a = 85;
    private String b;
    
    public ImplementationVersionNameSubItem() {
	/* empty */
    }
    
    public ImplementationVersionNameSubItem(String string) {
	b = string;
    }
    
    public String getImplementationVersionName() {
	return b;
    }
    
    public int getLength() {
	return 4 + b.length();
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(b.length());
	dataoutputstream.write(b.getBytes());
    }
    
    public int read(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("itemtype field of received Implementation Version Name Item  not "
		       + a);
	datainputstream.read();
	char c = datainputstream.readChar();
	byte[] is = new byte[c];
	datainputstream.readFully(is);
	is = n.trimZeros(is);
	b = new String(is);
	return '\004' + c;
    }
}
