/* ImplementationClassUIDSubItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.tool.n;

class ImplementationClassUIDSubItem extends SubItem
{
    private int a = 82;
    private String b;
    
    public ImplementationClassUIDSubItem() {
	/* empty */
    }
    
    public ImplementationClassUIDSubItem(String string) {
	b = string;
    }
    
    public int getLength() {
	return 4 + b.length();
    }
    
    public String getImplementationClassUID() {
	return b;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(b.length());
	dataoutputstream.write(b.getBytes());
    }
    
    public int read(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("itemtype field of received Implementation Class UID Item  not "
		       + a);
	datainputstream.read();
	char c = datainputstream.readChar();
	byte[] is = new byte[c];
	datainputstream.readFully(is);
	is = n.trimZeros(is);
	b = new String(is);
	return '\004' + c;
    }
}
