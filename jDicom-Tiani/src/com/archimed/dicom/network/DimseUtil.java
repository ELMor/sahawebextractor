/* DimseUtil - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;

public class DimseUtil
{
    public static final int N_EVENT_REPORT_REQUEST = 256;
    public static final int N_EVENT_REPORT_RESPONSE = 33024;
    public static final int N_GET_REQUEST = 272;
    public static final int N_GET_RESPONSE = 33040;
    public static final int N_SET_REQUEST = 288;
    public static final int N_SET_RESPONSE = 33056;
    public static final int N_ACTION_REQUEST = 304;
    public static final int N_ACTION_RESPONSE = 33072;
    public static final int N_CREATE_REQUEST = 320;
    public static final int N_CREATE_RESPONSE = 33088;
    public static final int N_DELETE_REQUEST = 336;
    public static final int N_DELETE_RESPONSE = 336;
    public static final int C_STORE_REQUEST = 1;
    public static final int C_STORE_RESPONSE = 32769;
    public static final int C_GET_REQUEST = 16;
    public static final int C_GET_RESPONSE = 32784;
    public static final int C_FIND_REQUEST = 32;
    public static final int C_FIND_RESPONSE = 32800;
    public static final int C_MOVE_REQUEST = 33;
    public static final int C_MOVE_RESPONSE = 32801;
    public static final int C_ECHO_REQUEST = 48;
    public static final int C_ECHO_RESPONSE = 32816;
    public static final int C_CANCEL_FIND_REQUEST = 4095;
    public static final int MEDIUM_PRIORITY = 0;
    public static final int HIGH_PRIORITY = 1;
    public static final int LOW_PRIORITY = 2;
    private static final Integer a = new Integer(0);
    private static final Integer b = new Integer(257);
    
    public static int getCommandType(DicomObject dicomobject)
	throws IllegalValueException {
	Integer integer = (Integer) dicomobject.get(3);
	if (integer == null)
	    throw new IllegalValueException("dicomset is not a DIMSE command");
	return integer.intValue();
    }
    
    public static int getMessageID(DicomObject dicomobject)
	throws IllegalValueException {
	Integer integer = (Integer) dicomobject.get(4);
	if (integer == null)
	    throw new IllegalValueException
		      ("dicomset does not contain MessageID tag");
	return integer.intValue();
    }
    
    public static int getPriority(DicomObject dicomobject)
	throws IllegalValueException {
	Integer integer = (Integer) dicomobject.get(4);
	if (integer == null)
	    throw new IllegalValueException
		      ("dicomset does not contain Priority tag");
	return integer.intValue();
    }
    
    public static int getAffectedSOPClass(DicomObject dicomobject)
	throws IllegalValueException, UnknownUIDException {
	String string = (String) dicomobject.get(1);
	return UID.getUIDEntry(string).getConstant();
    }
    
    public static String getAffectedSOPInstance(DicomObject dicomobject)
	throws IllegalValueException {
	String string = (String) dicomobject.get(13);
	return string;
    }
    
    public static int getRequestedSOPClass(DicomObject dicomobject)
	throws IllegalValueException, UnknownUIDException {
	String string = (String) dicomobject.get(2);
	return UID.getUIDEntry(string).getConstant();
    }
    
    public static String getRequestedSOPInstance(DicomObject dicomobject)
	throws IllegalValueException, UnknownUIDException {
	String string = (String) dicomobject.get(14);
	return string;
    }
    
    public static boolean dataPresent(DicomObject dicomobject)
	throws IllegalValueException {
	Integer integer = (Integer) dicomobject.get(8);
	if (integer == null)
	    throw new IllegalValueException
		      ("no DataSetType tag in dicomobject");
	if (integer.intValue() == 257)
	    return false;
	return true;
    }
    
    public static DicomObject createEventReportRequest
	(Integer integer, Integer integer_0_, String string, boolean bool,
	 Integer integer_1_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(3, new Integer(256));
	dicomobject.set(1, UID.getUIDEntry(integer_0_.intValue()).getValue());
	dicomobject.set(4, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	dicomobject.set(13, string);
	dicomobject.set(15, integer_1_);
	return dicomobject;
    }
    
    public static DicomObject createEventReportResponse
	(Integer integer, Integer integer_2_, String string, boolean bool,
	 Integer integer_3_, Integer integer_4_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_2_ != null)
	    dicomobject.set(1,
			    UID.getUIDEntry(integer_2_.intValue()).getValue());
	dicomobject.set(3, new Integer(33024));
	dicomobject.set(5, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	if (string != null)
	    dicomobject.set(13, string);
	if (integer_4_ != null)
	    dicomobject.set(15, integer_4_);
	dicomobject.set(9, integer_3_);
	return dicomobject;
    }
    
    public static DicomObject createNGetRequest
	(Integer integer, Integer integer_5_, String string, int[] is)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(2, UID.getUIDEntry(integer_5_.intValue()).getValue());
	dicomobject.set(3, new Integer(272));
	dicomobject.set(4, integer);
	dicomobject.set(8, b);
	dicomobject.set(14, string);
	return dicomobject;
    }
    
    public static DicomObject createNGetResponse
	(Integer integer, Integer integer_6_, String string, boolean bool,
	 Integer integer_7_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_6_ != null)
	    dicomobject.set(1,
			    UID.getUIDEntry(integer_6_.intValue()).getValue());
	dicomobject.set(3, new Integer(33040));
	dicomobject.set(5, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	if (string != null)
	    dicomobject.set(13, string);
	dicomobject.set(9, integer_7_);
	return dicomobject;
    }
    
    public static DicomObject createSetRequest
	(Integer integer, Integer integer_8_, String string)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(2, UID.getUIDEntry(integer_8_.intValue()).getValue());
	dicomobject.set(3, new Integer(288));
	dicomobject.set(4, integer);
	dicomobject.set(8, a);
	dicomobject.set(14, string);
	return dicomobject;
    }
    
    public static DicomObject createSetResponse
	(Integer integer, Integer integer_9_, String string, boolean bool,
	 Integer integer_10_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_9_ != null)
	    dicomobject.set(1,
			    UID.getUIDEntry(integer_9_.intValue()).getValue());
	dicomobject.set(3, new Integer(33056));
	dicomobject.set(5, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, a);
	if (string != null)
	    dicomobject.set(13, string);
	dicomobject.set(9, integer_10_);
	return dicomobject;
    }
    
    public static DicomObject createActionRequest
	(Integer integer, Integer integer_11_, String string, boolean bool,
	 Integer integer_12_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(2, UID.getUIDEntry(integer_11_.intValue()).getValue());
	dicomobject.set(3, new Integer(304));
	dicomobject.set(4, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	dicomobject.set(14, string);
	dicomobject.set(18, integer_12_);
	return dicomobject;
    }
    
    public static DicomObject createActionResponse
	(Integer integer, Integer integer_13_, String string, boolean bool,
	 Integer integer_14_, Integer integer_15_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_13_ != null)
	    dicomobject
		.set(1, UID.getUIDEntry(integer_13_.intValue()).getValue());
	dicomobject.set(3, new Integer(33072));
	dicomobject.set(5, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	if (string != null)
	    dicomobject.set(13, string);
	if (integer_15_ != null)
	    dicomobject.set(18, integer_15_);
	dicomobject.set(9, integer_14_);
	return dicomobject;
    }
    
    public static DicomObject createCreateRequest
	(Integer integer, Integer integer_16_, String string, boolean bool)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(1, UID.getUIDEntry(integer_16_.intValue()).getValue());
	dicomobject.set(3, new Integer(320));
	dicomobject.set(4, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	if (string != null)
	    dicomobject.set(13, string);
	return dicomobject;
    }
    
    public static DicomObject createCreateResponse
	(Integer integer, Integer integer_17_, String string, boolean bool,
	 Integer integer_18_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_17_ != null)
	    dicomobject
		.set(1, UID.getUIDEntry(integer_17_.intValue()).getValue());
	dicomobject.set(3, new Integer(33088));
	dicomobject.set(5, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	if (string != null)
	    dicomobject.set(13, string);
	dicomobject.set(9, integer_18_);
	return dicomobject;
    }
    
    public static DicomObject createDeleteRequest
	(Integer integer, Integer integer_19_, String string)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(2, UID.getUIDEntry(integer_19_.intValue()).getValue());
	dicomobject.set(3, new Integer(336));
	dicomobject.set(4, integer);
	dicomobject.set(8, b);
	dicomobject.set(14, string);
	return dicomobject;
    }
    
    public static DicomObject createDeleteResponse
	(Integer integer, Integer integer_20_, String string,
	 Integer integer_21_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_20_ != null)
	    dicomobject
		.set(1, UID.getUIDEntry(integer_20_.intValue()).getValue());
	dicomobject.set(3, new Integer(336));
	dicomobject.set(5, integer);
	dicomobject.set(8, b);
	if (string != null)
	    dicomobject.set(13, string);
	dicomobject.set(9, integer_21_);
	return dicomobject;
    }
    
    public static DicomObject createEchoRequest
	(Integer integer, Integer integer_22_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(1, UID.getUIDEntry(integer_22_.intValue()).getValue());
	dicomobject.set(3, new Integer(48));
	dicomobject.set(4, integer);
	dicomobject.set(8, b);
	return dicomobject;
    }
    
    public static DicomObject createEchoResponse
	(Integer integer, Integer integer_23_, Integer integer_24_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_23_ != null)
	    dicomobject
		.set(1, UID.getUIDEntry(integer_23_.intValue()).getValue());
	dicomobject.set(3, new Integer(32816));
	dicomobject.set(5, integer);
	dicomobject.set(8, b);
	dicomobject.set(9, integer_24_);
	return dicomobject;
    }
    
    public static DicomObject createMoveRequest
	(Integer integer, Integer integer_25_, Integer integer_26_,
	 String string)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(1, UID.getUIDEntry(integer_25_.intValue()).getValue());
	dicomobject.set(3, new Integer(33));
	dicomobject.set(4, integer);
	dicomobject.set(7, integer_26_);
	dicomobject.set(8, a);
	dicomobject.set(6, string);
	return dicomobject;
    }
    
    public static DicomObject createMoveResponse
	(Integer integer, Integer integer_27_, boolean bool,
	 Integer integer_28_, Integer integer_29_, Integer integer_30_,
	 Integer integer_31_, Integer integer_32_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_27_ != null)
	    dicomobject
		.set(1, UID.getUIDEntry(integer_27_.intValue()).getValue());
	dicomobject.set(3, new Integer(32801));
	dicomobject.set(5, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	dicomobject.set(9, integer_28_);
	if (integer_29_ != null)
	    dicomobject.set(19, integer_29_);
	if (integer_30_ != null)
	    dicomobject.set(20, integer_30_);
	if (integer_31_ != null)
	    dicomobject.set(21, integer_31_);
	if (integer_32_ != null)
	    dicomobject.set(22, integer_32_);
	return dicomobject;
    }
    
    public static DicomObject createCGetRequest
	(Integer integer, Integer integer_33_, Integer integer_34_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(1, UID.getUIDEntry(integer_33_.intValue()).getValue());
	dicomobject.set(3, new Integer(16));
	dicomobject.set(4, integer);
	dicomobject.set(7, integer_34_);
	dicomobject.set(8, a);
	return dicomobject;
    }
    
    public static DicomObject createCGetResponse
	(Integer integer, Integer integer_35_, boolean bool,
	 Integer integer_36_, Integer integer_37_, Integer integer_38_,
	 Integer integer_39_, Integer integer_40_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_35_ != null)
	    dicomobject
		.set(1, UID.getUIDEntry(integer_35_.intValue()).getValue());
	dicomobject.set(3, new Integer(32784));
	dicomobject.set(5, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	dicomobject.set(9, integer_36_);
	if (integer_37_ != null)
	    dicomobject.set(19, integer_37_);
	if (integer_38_ != null)
	    dicomobject.set(20, integer_37_);
	if (integer_39_ != null)
	    dicomobject.set(21, integer_37_);
	if (integer_40_ != null)
	    dicomobject.set(22, integer_37_);
	return dicomobject;
    }
    
    public static DicomObject createStoreRequest
	(Integer integer, Integer integer_41_, Integer integer_42_,
	 String string, String string_43_, String string_44_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(1, UID.getUIDEntry(integer_41_.intValue()).getValue());
	dicomobject.set(3, new Integer(1));
	dicomobject.set(4, integer);
	dicomobject.set(7, integer_42_);
	dicomobject.set(8, a);
	dicomobject.set(13, string);
	if (string_43_ != null)
	    dicomobject.set(23, string_43_);
	if (string_44_ != null)
	    dicomobject.set(24, new Integer(string_44_));
	return dicomobject;
    }
    
    public static DicomObject createStoreResponse
	(Integer integer, Integer integer_45_, String string,
	 Integer integer_46_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_45_ != null)
	    dicomobject
		.set(1, UID.getUIDEntry(integer_45_.intValue()).getValue());
	dicomobject.set(3, new Integer(32769));
	dicomobject.set(5, integer);
	dicomobject.set(8, b);
	dicomobject.set(9, integer_46_);
	if (string != null)
	    dicomobject.set(13, string);
	return dicomobject;
    }
    
    public static DicomObject createFindRequest
	(Integer integer, Integer integer_47_, Integer integer_48_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(1, UID.getUIDEntry(integer_47_.intValue()).getValue());
	dicomobject.set(3, new Integer(32));
	dicomobject.set(4, integer);
	dicomobject.set(8, a);
	dicomobject.set(7, integer_48_);
	return dicomobject;
    }
    
    public static DicomObject createFindResponse
	(Integer integer, Integer integer_49_, boolean bool,
	 Integer integer_50_)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	if (integer_49_ != null)
	    dicomobject
		.set(1, UID.getUIDEntry(integer_49_.intValue()).getValue());
	dicomobject.set(3, new Integer(32800));
	dicomobject.set(5, integer);
	if (bool)
	    dicomobject.set(8, a);
	else
	    dicomobject.set(8, b);
	dicomobject.set(9, integer_50_);
	return dicomobject;
    }
    
    public static DicomObject createCancelRequest(Integer integer)
	throws DicomException, IllegalValueException {
	DicomObject dicomobject = new DicomObject();
	dicomobject.set(3, new Integer(4095));
	dicomobject.set(5, integer);
	return dicomobject;
    }
}
