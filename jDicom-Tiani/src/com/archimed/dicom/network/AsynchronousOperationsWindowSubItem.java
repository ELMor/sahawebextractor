/* AsynchronousOperationsWindowSubItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;

class AsynchronousOperationsWindowSubItem extends SubItem
{
    private int a = 83;
    private int b;
    private int c;
    
    public AsynchronousOperationsWindowSubItem() {
	/* empty */
    }
    
    public AsynchronousOperationsWindowSubItem(int i, int i_0_) {
	b = i;
	c = i_0_;
    }
    
    int a() {
	return b;
    }
    
    int b() {
	return c;
    }
    
    void a(int i) {
	b = i;
    }
    
    void b(int i) {
	c = i;
    }
    
    public int getLength() {
	return 8;
    }
    
    public int read(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("wrong itemtype of abstract syntax");
	datainputstream.read();
	char c = datainputstream.readChar();
	if (c != '\004')
	    throw new IllegalValueException
		      ("length of AsynchronousOperationsWindowSubItem should be 4");
	b = datainputstream.readChar();
	this.c = datainputstream.readChar();
	return 8;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(4);
	dataoutputstream.writeChar(b);
	dataoutputstream.writeChar(c);
    }
}
