/* PresentationContextResponseItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;

class PresentationContextResponseItem
{
    private byte a = 33;
    private int b;
    private int c;
    private int d;
    TransferSyntaxItem e;
    
    public PresentationContextResponseItem() {
	e = new TransferSyntaxItem();
    }
    
    public PresentationContextResponseItem
	(int i, int i_0_, TransferSyntaxItem transfersyntaxitem) {
	e = new TransferSyntaxItem();
	b = i;
	d = i_0_;
	e = transfersyntaxitem;
	c = 4;
	if (transfersyntaxitem != null)
	    c += transfersyntaxitem.getLength();
    }
    
    public int getID() {
	return b;
    }
    
    public int getResult() {
	return d;
    }
    
    public UIDEntry getTransferSyntax() {
	return e.getTransferSyntax();
    }
    
    public int getLength() {
	return c + 4;
    }
    
    public void write(DataOutputStream dataoutputstream)
	throws IOException, IllegalValueException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(c);
	dataoutputstream.write(b);
	dataoutputstream.write(0);
	dataoutputstream.write(d);
	dataoutputstream.write(0);
	if (e != null)
	    e.a(dataoutputstream);
    }
    
    public void read(DataInputStream datainputstream)
	throws IllegalValueException, IOException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("itemtype field of received Presentation Context Item  not "
		       + a);
	datainputstream.skip(1L);
	c = datainputstream.readChar();
	if (Debug.DEBUG > 3)
	    Debug.out.println("PresentationContextResponseItem length: " + c);
	b = datainputstream.read();
	datainputstream.skip(1L);
	d = datainputstream.read();
	datainputstream.skip(1L);
	if (d == 0)
	    e.a(datainputstream);
	else
	    datainputstream.skip((long) (c - 4));
    }
}
