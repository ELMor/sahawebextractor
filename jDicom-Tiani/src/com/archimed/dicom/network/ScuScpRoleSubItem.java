/* ScuScpRoleSubItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.n;

class ScuScpRoleSubItem extends SubItem
{
    private int a = 84;
    private int b;
    private int c;
    private UIDEntry d;
    
    public ScuScpRoleSubItem() {
	/* empty */
    }
    
    public ScuScpRoleSubItem(UIDEntry uidentry, int i, int i_0_) {
	d = uidentry;
	b = i;
	c = i_0_;
    }
    
    public int getLength() {
	int i = 8;
	if (d != null)
	    i += d.getValue().length();
	return i;
    }
    
    public UIDEntry getAbstractSyntax() {
	return d;
    }
    
    public int getScuRole() {
	return b;
    }
    
    public int getScpRole() {
	return c;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(d.getValue().length() + 4);
	dataoutputstream.writeChar(d.getValue().length());
	dataoutputstream.write(d.getValue().getBytes());
	dataoutputstream.write(b);
	dataoutputstream.write(c);
    }
    
    public int read(DataInputStream datainputstream)
	throws IOException, IllegalValueException {
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("itemtype field of received ScuScpRoleSubItem  not "
		       + a);
	datainputstream.read();
	char c = datainputstream.readChar();
	char c_1_ = datainputstream.readChar();
	byte[] is = new byte[c_1_];
	datainputstream.readFully(is);
	is = n.trimZeros(is);
	String string = new String(is);
	try {
	    d = UID.getUIDEntry(string);
	} catch (UnknownUIDException unknownuidexception) {
	    d = new UIDEntry(0, string, "unknown uid", "??", 1);
	}
	b = datainputstream.read();
	this.c = datainputstream.read();
	return '\006' + c_1_ + '\002';
    }
}
