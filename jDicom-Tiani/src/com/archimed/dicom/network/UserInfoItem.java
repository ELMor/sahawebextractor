/* UserInfoItem - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

class UserInfoItem
{
    private int a = 80;
    private Vector b;
    private int c = 0;
    
    public UserInfoItem() {
	b = new Vector();
    }
    
    public void addSubItem(SubItem subitem) {
	b.addElement(subitem);
    }
    
    public Vector getSubItems() {
	return b;
    }
    
    public int getLength() {
	int i = 4;
	for (int i_0_ = 0; i_0_ < b.size(); i_0_++)
	    i += ((SubItem) b.elementAt(i_0_)).getLength();
	i += c;
	return i;
    }
    
    public void write(DataOutputStream dataoutputstream) throws IOException {
	dataoutputstream.write(a);
	dataoutputstream.write(0);
	dataoutputstream.writeChar(getLength() - 4);
	for (int i = 0; i < b.size(); i++)
	    ((SubItem) b.elementAt(i)).write(dataoutputstream);
    }
    
    public int read(PushbackInputStream pushbackinputstream)
	throws IOException, IllegalValueException, UnknownUIDException {
	DataInputStream datainputstream
	    = new DataInputStream(pushbackinputstream);
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("itemtype field of received User Info Item  not " + a);
	datainputstream.read();
	int i_1_ = datainputstream.readChar();
	if (Debug.DEBUG > 3)
	    Debug.out.println("UserInfoItem length: " + i_1_);
	int i_2_ = i_1_;
	while (i_1_ > 0) {
	    i = datainputstream.read();
	    pushbackinputstream.unread(i);
	    if (i == 81) {
		if (Debug.DEBUG > 3)
		    Debug.out.println("Detect MaximumLengthSubItem");
		MaximumLengthSubItem maximumlengthsubitem
		    = new MaximumLengthSubItem();
		int i_3_ = maximumlengthsubitem.read(datainputstream);
		addSubItem(maximumlengthsubitem);
		i_1_ -= i_3_;
	    } else if (i == 82) {
		if (Debug.DEBUG > 3)
		    Debug.out.println("Detect ImplementationClassUIDSubItem");
		ImplementationClassUIDSubItem implementationclassuidsubitem
		    = new ImplementationClassUIDSubItem();
		int i_4_ = implementationclassuidsubitem.read(datainputstream);
		addSubItem(implementationclassuidsubitem);
		i_1_ -= i_4_;
	    } else if (i == 83) {
		if (Debug.DEBUG > 3)
		    Debug.out
			.println("Detect AsynchronousOperationsWindowSubItem");
		AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem
		    = new AsynchronousOperationsWindowSubItem();
		int i_5_ = asynchronousoperationswindowsubitem
			       .read(datainputstream);
		addSubItem(asynchronousoperationswindowsubitem);
		i_1_ -= i_5_;
	    } else if (i == 84) {
		if (Debug.DEBUG > 3)
		    Debug.out.println("Detect ScuScpRoleSubItem");
		ScuScpRoleSubItem scuscprolesubitem = new ScuScpRoleSubItem();
		int i_6_ = scuscprolesubitem.read(datainputstream);
		addSubItem(scuscprolesubitem);
		i_1_ -= i_6_;
	    } else if (i == 85) {
		if (Debug.DEBUG > 3)
		    Debug.out
			.println("Detect ImplementationVersionNameSubItem");
		ImplementationVersionNameSubItem implementationversionnamesubitem
		    = new ImplementationVersionNameSubItem();
		int i_7_
		    = implementationversionnamesubitem.read(datainputstream);
		addSubItem(implementationversionnamesubitem);
		i_1_ -= i_7_;
	    } else if (i == 86) {
		if (Debug.DEBUG > 3)
		    Debug.out.println("Detect ExtendedNegotiationSubItem");
		ExtendedNegotiationSubItem extendednegotiationsubitem
		    = new ExtendedNegotiationSubItem();
		int i_8_ = extendednegotiationsubitem.read(datainputstream);
		addSubItem(extendednegotiationsubitem);
		i_1_ -= i_8_;
	    } else {
		datainputstream.read();
		datainputstream.read();
		int i_9_ = datainputstream.readChar();
		datainputstream.skip((long) i_9_);
		i_1_ = i_1_ - i_9_ - 4;
		c += i_9_ + 4;
	    }
	    if (Debug.DEBUG > 3)
		Debug.out.println("UserInfoItem remaining length: " + i_1_);
	}
	return 4 + i_2_;
    }
}
