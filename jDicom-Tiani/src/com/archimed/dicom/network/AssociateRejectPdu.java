/* AssociateRejectPdu - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;

import com.archimed.dicom.IllegalValueException;

class AssociateRejectPdu
{
    private int a = 3;
    private int b;
    private int c;
    private int d;
    private int e;
    
    public AssociateRejectPdu() {
	/* empty */
    }
    
    public AssociateRejectPdu(int i, int i_0_, int i_1_) {
	c = i;
	d = i_0_;
	e = i_1_;
    }
    
    public Reject getReject() {
	Reject reject = new Reject(c, d, e);
	return reject;
    }
    
    public void read(InputStream inputstream)
	throws IOException, IllegalValueException {
	PushbackInputStream pushbackinputstream
	    = new PushbackInputStream(inputstream);
	DataInputStream datainputstream
	    = new DataInputStream(pushbackinputstream);
	int i = datainputstream.read();
	if (i != a)
	    throw new IllegalValueException
		      ("PDU-type field of Associate Reject PDU not " + a);
	datainputstream.read();
	b = datainputstream.readInt();
	datainputstream.read();
	c = inputstream.read();
	d = inputstream.read();
	e = inputstream.read();
    }
    
    public void write(OutputStream outputstream) throws IOException {
	DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
	outputstream.write(a);
	outputstream.write(0);
	dataoutputstream.writeInt(4);
	outputstream.write(0);
	outputstream.write(c);
	outputstream.write(d);
	outputstream.write(e);
	outputstream.flush();
    }
}
