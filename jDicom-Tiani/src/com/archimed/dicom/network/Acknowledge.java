/* Acknowledge - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.tool.n;

public class Acknowledge extends Response
{
    public static final int NO_SUPPORT_SCU_ROLE = 0;
    public static final int SUPPORT_SCU_ROLE = 1;
    public static final int NO_SUPPORT_SCP_ROLE = 0;
    public static final int SUPPORT_SCP_ROLE = 1;
    public static final int DEFAULT = -1;
    public static final int ACCEPTANCE = 0;
    public static final int USER_REJECTION = 1;
    public static final int NO_REASON = 2;
    public static final int ABSTRACT_SYNTAX_NOT_SUPPORTED = 3;
    public static final int TRANSFER_SYNTAXES_NOT_SUPPORTED = 4;
    private String a;
    private String b;
    private String c;
    private Vector d;
    private Vector e;
    private Vector f;
    private String g;
    private String h;
    private Vector i;
    private int j = 32768;
    private int k = 1;
    private int l = 1;
    private Vector m = new Vector();
    private Vector n;
    private Vector o;
    
    public Acknowledge() {
	d = new Vector();
	e = new Vector();
	f = new Vector();
	i = new Vector();
	n = new Vector();
	o = new Vector();
	a("1.2.826.0.1.3680043.2.60.0.1");
	b("softlink_jdt103");
    }
    
    public String getCallingTitle() {
	return b;
    }
    
    public String getCalledTitle() {
	return a;
    }
    
    public String getApplicationContextUid() {
	return c;
    }
    
    public boolean isPrivateApplicationContext() {
	return !ApplicationContextItem.isPrivate(c);
    }
    
    public void setApplicationContextUid(String string) {
	c = string;
    }
    
    public void setCalledTitle(String string) {
	a = string;
    }
    
    public void setCallingTitle(String string) {
	b = string;
    }
    
    public String getImplementationClassUID() {
	return g;
    }
    
    public String getImplementationVersionName() {
	return h;
    }
    
    void a(String string) {
	g = string;
    }
    
    void b(String string) {
	h = string;
    }
    
    public int getMaxPduSize() {
	return j;
    }
    
    public void setMaxPduSize(int i) {
	j = i;
    }
    
    public void setMaxOperationsInvoked(int i) {
	k = i;
    }
    
    public int getMaxOperationsInvoked() {
	return k;
    }
    
    public void setMaxOperationsPerformed(int i) {
	l = i;
    }
    
    public int getMaxOperationsPerformed() {
	return l;
    }
    
    public void setScuScpRoleSelection(int i, int i_0_, int i_1_)
	throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i);
	int i_2_ = m.indexOf(uidentry);
	if (i_2_ != -1) {
	    n.setElementAt(new Integer(i_0_), i_2_);
	    o.setElementAt(new Integer(i_1_), i_2_);
	} else {
	    m.addElement(uidentry);
	    n.addElement(new Integer(i_0_));
	    o.addElement(new Integer(i_1_));
	}
    }
    
    public final int getScuRole(int i) throws IllegalValueException {
	return getScuRole(UID.getUIDEntry(i));
    }
    
    public int getScuRole(UIDEntry uidentry) {
	int i = m.indexOf(uidentry);
	if (i == -1)
	    return -1;
	return ((Integer) n.elementAt(i)).intValue();
    }
    
    public int getScpRole(int i) throws IllegalValueException {
	return getScpRole(UID.getUIDEntry(i));
    }
    
    public int getScpRole(UIDEntry uidentry) {
	int i = m.indexOf(uidentry);
	if (i == -1)
	    return -1;
	return ((Integer) o.elementAt(i)).intValue();
    }
    
    UIDEntry a(int i) {
	return (UIDEntry) m.elementAt(i);
    }
    
    int a() {
	return m.size();
    }
    
    public byte getPresentationContextID(int i) {
	return ((Byte) f.elementAt(i)).byteValue();
    }
    
    public int getResult(int i) {
	return ((Integer) e.elementAt(i)).intValue();
    }
    
    public int getPresentationContexts() {
	return f.size();
    }
    
    public UIDEntry getTransferSyntax(int i) {
	return (UIDEntry) d.elementAt(i);
    }
    
    public void addPresentationContext(byte i, int i_3_, int i_4_)
	throws IllegalValueException {
	UIDEntry uidentry = UID.getUIDEntry(i_4_);
	f.addElement(new Byte(i));
	e.addElement(new Integer(i_3_));
	d.addElement(uidentry);
    }
    
    public String toString() {
	String string = "*** acknowledge ***\n";
	string += "max pdu size: " + j + "\n";
	string += "max operation invoked: " + k + "\n";
	string += "max operation performed: " + l + "\n";
	string += "implementation class UID: " + g + "\n";
	string += "implementation version name: " + h + "\n";
	string += (com.archimed.tool.n.fstr("abstract syntax", 32) + com.archimed.tool.n.fstr("scu", 4)
		   + com.archimed.tool.n.fstr("scp", 4) + "\n");
	for (int i = 0; i < a(); i++) {
	    UIDEntry uidentry = a(i);
	    string += (com.archimed.tool.n.fstr(uidentry.getValue(), 32)
		       + com.archimed.tool.n.fstr("" + getScuRole(uidentry), 4)
		       + com.archimed.tool.n.fstr("" + getScpRole(uidentry), 4) + "\n");
	}
	string += (com.archimed.tool.n.fstr("nr", 5) + com.archimed.tool.n.fstr("pcid", 6) + com.archimed.tool.n.fstr("result", 32)
		   + com.archimed.tool.n.fstr("transfer syntax", 20) + "\n");
	for (int i = 0; i < getPresentationContexts(); i++) {
	    if (getResult(i) == 0)
		string += (com.archimed.tool.n.fstr(i + "", 5)
			   + com.archimed.tool.n.fstr("" + getPresentationContextID(i), 6)
			   + com.archimed.tool.n.fstr("accepted", 32)
			   + com.archimed.tool.n.fstr("" + getTransferSyntax(i), 20) + "\n");
	    else if (getResult(i) == 3)
		string += (com.archimed.tool.n.fstr(i + "", 5)
			   + com.archimed.tool.n.fstr("" + getPresentationContextID(i), 6)
			   + com.archimed.tool.n.fstr("unsupported abstract syntax", 32) + "\n");
	    else if (getResult(i) == 4)
		string
		    += (com.archimed.tool.n.fstr(i + "", 5)
			+ com.archimed.tool.n.fstr("" + getPresentationContextID(i), 6)
			+ com.archimed.tool.n.fstr("unsupported transfer syntaxes", 32) + "\n");
	    else if (getResult(i) == 1)
		string += (com.archimed.tool.n.fstr(i + "", 5)
			   + com.archimed.tool.n.fstr("" + getPresentationContextID(i), 6)
			   + com.archimed.tool.n.fstr("user rejection", 32) + "\n");
	    else if (getResult(i) == 2)
		string += (com.archimed.tool.n.fstr(i + "", 5)
			   + com.archimed.tool.n.fstr("" + getPresentationContextID(i), 6)
			   + com.archimed.tool.n.fstr("no reason", 32) + "\n");
	}
	string += "*******************";
	return string;
    }
    
    public int indexOf(byte i) {
	return f.indexOf(new Byte(i));
    }
}
