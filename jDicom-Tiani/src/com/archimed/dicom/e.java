/* e - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

class e
{
    static Object a(String string, int i) throws DicomException {
	string = string.trim();
	if (string.equals(""))
	    return null;
	switch (i) {
	case 2:
	case 4:
	case 6:
	case 7:
	case 9:
	case 13:
	case 18:
	case 27:
	    return string;
	case 28:
	    return (string.indexOf('-') == -1 ? (Object) new DDateTime(string)
		    : new DDateTimeRange(string));
	case 12:
	    return (string.indexOf('-') == -1 ? (Object) new DTime(string)
		    : new DTimeRange(string));
	case 17:
	    if (string.length() != 0 && string.length() != 4)
		throw new DicomException("Type AS has to have 4 bytes fixed.");
	    return string;
	case 14:
	    return new Person(string.trim());
	case 11:
	    return (string.indexOf('-') == -1 ? (Object) new DDate(string)
		    : new DDateRange(string));
	case 16:
	case 26:
	    return new Float(string);
	case 15:
	    return new Integer(string);
	case 23:
	    return new Short(string);
	case 3:
	case 21:
	    return new Integer(string);
	case 1:
	case 19:
	    return new Long(string);
	case 20:
	    return new Double(string);
	case 0:
	case 8:
	case 22:
	case 24:
	    return string.getBytes();
	default:
	    throw new DicomException("String used for type: " + DDict.a(i));
	}
    }
    
    static Object a(byte[] is, int i) throws DicomException {
	switch (i) {
	case 0:
	case 8:
	case 22:
	case 24:
	    return is;
	default:
	    throw new DicomException("byte[] used for type: " + DDict.a(i));
	}
    }
    
    static Object a(Integer integer, int i) throws DicomException {
	if (integer == null)
	    return null;
	switch (i) {
	case 23:
	    return new Short(integer.shortValue());
	case 3:
	case 5:
	case 15:
	case 21:
	    return integer;
	case 1:
	case 19:
	    return new Long((long) integer.intValue());
	default:
	    throw new DicomException("Integer used for type: " + DDict.a(i));
	}
    }
    
    static Object a(Float var_float, int i) throws DicomException {
	if (var_float == null)
	    return null;
	switch (i) {
	case 16:
	case 26:
	    return var_float;
	case 20:
	    return new Double(var_float.doubleValue());
	default:
	    throw new DicomException("Float used for type: " + DDict.a(i));
	}
    }
    
    static Object a(Double var_double, int i) throws DicomException {
	if (var_double == null)
	    return null;
	switch (i) {
	case 20:
	    return var_double;
	case 16:
	case 26:
	    return new Float(var_double.floatValue());
	default:
	    throw new DicomException("Double used for type: " + DDict.a(i));
	}
    }
    
    static Object a(Person person, int i) throws DicomException {
	switch (i) {
	case 14:
	    return person;
	default:
	    throw new DicomException("Person used for type: " + DDict.a(i));
	}
    }
    
    static Object a(DDate ddate, int i) throws DicomException {
	switch (i) {
	case 11:
	    return ddate;
	default:
	    throw new DicomException("DDate used for type: " + DDict.a(i));
	}
    }
    
    static Object a(DDateRange ddaterange, int i) throws DicomException {
	switch (i) {
	case 11:
	    return ddaterange;
	default:
	    throw new DicomException("DDateRange used for type: "
				     + DDict.a(i));
	}
    }
    
    static Object a(DTime dtime, int i) throws DicomException {
	switch (i) {
	case 12:
	    return dtime;
	default:
	    throw new DicomException("DTime used for type: " + DDict.a(i));
	}
    }
    
    static Object a(DTimeRange dtimerange, int i) throws DicomException {
	switch (i) {
	case 12:
	    return dtimerange;
	default:
	    throw new DicomException("DTimeRange used for type: "
				     + DDict.a(i));
	}
    }
    
    static Object a(DDateTime ddatetime, int i) throws DicomException {
	switch (i) {
	case 28:
	    return ddatetime;
	default:
	    throw new DicomException("DDateTime used for type: " + DDict.a(i));
	}
    }
    
    static Object a(DDateTimeRange ddatetimerange, int i)
	throws DicomException {
	switch (i) {
	case 28:
	    return ddatetimerange;
	default:
	    throw new DicomException("DDateTimeRange used for type: "
				     + DDict.a(i));
	}
    }
    
    static Object a(Short var_short, int i) throws DicomException {
	switch (i) {
	case 23:
	    return var_short;
	default:
	    throw new DicomException("Short used for type: " + DDict.a(i));
	}
    }
    
    static Object a(Long var_long, int i) throws DicomException {
	switch (i) {
	case 1:
	case 19:
	    return var_long;
	default:
	    throw new DicomException("Long used for type: " + DDict.a(i));
	}
    }
    
    static Object a(int[] is, int i) throws DicomException {
	if (is == null)
	    return null;
	if (is.length != 2)
	    throw new DicomException("Only int[2] can be parsed");
	switch (i) {
	case 5:
	    return new Integer((is[0] << 16) + is[1]);
	default:
	    throw new DicomException("2 ints used for type: " + DDict.a(i));
	}
    }
    
    static Object a(DicomObject dicomobject, int i) throws DicomException {
	if (i == 10)
	    return dicomobject;
	throw new DicomException("DicomObject used for type: " + DDict.a(i));
    }
    
    static String a(Object object, int i) {
	String string = "";
	if (object == null)
	    return null;
	switch (i) {
	case 2:
	case 4:
	case 6:
	case 7:
	case 9:
	case 13:
	case 17:
	case 18:
	case 27:
	    string = ((String) object).trim();
	    break;
	case 14:
	    string = ((Person) object).a('^');
	    break;
	case 11:
	    try {
		string = ((DDate) object).toDICOMString();
	    } catch (ClassCastException classcastexception) {
		string = ((DDateRange) object).toDICOMString();
	    }
	    break;
	case 28:
	    try {
		string = ((DDateTime) object).toDICOMString();
	    } catch (ClassCastException classcastexception) {
		string = ((DDateTimeRange) object).toDICOMString();
	    }
	    break;
	case 12:
	    try {
		string = ((DTime) object).toDICOMString();
	    } catch (ClassCastException classcastexception) {
		string = ((DTimeRange) object).toDICOMString();
	    }
	    break;
	case 16:
	case 26:
	    string = ((Float) object).toString();
	    break;
	case 3:
	case 15:
	case 21:
	    string = ((Integer) object).toString();
	    break;
	case 23:
	    string = ((Short) object).toString();
	    break;
	case 1:
	case 19:
	    string = ((Long) object).toString();
	    break;
	case 20:
	    string = ((Double) object).toString();
	    break;
	case 0:
	    string = new String((byte[]) object);
	    break;
	case 8:
	case 22:
	case 24: {
	    string = "";
	    int i_0_ = ((byte[]) object).length;
	    if (i_0_ > 12)
		i_0_ = 12;
	    for (int i_1_ = 0; i_1_ < i_0_; i_1_++) {
		if (i_1_ != i_0_ - 1)
		    string += DumpUtils.a(((byte[]) object)[i_1_], 2) + "\\";
		else
		    string += DumpUtils.a(((byte[]) object)[i_1_], 2);
	    }
	    if (i_0_ == 12)
		string += "...";
	    break;
	}
	case 5: {
	    int i_2_ = ((Integer) object).intValue() >> 16;
	    int i_3_ = ((Integer) object).intValue() & 0xffff;
	    string = ("(" + DumpUtils.a(i_2_, 4) + "," + DumpUtils.a(i_3_, 4)
		      + ")");
	    break;
	}
	default:
	    return null;
	}
	return string;
    }
    
    static int b(Object object, int i) throws DicomException {
	if (object == null)
	    return 2147483647;
	switch (i) {
	case 23:
	    return ((Short) object).intValue();
	case 3:
	case 5:
	case 15:
	case 21:
	    return ((Integer) object).intValue();
	case 1:
	case 19:
	    return ((Long) object).intValue();
	default:
	    throw new DicomException("unable to convert " + DDict.a(i)
				     + " to int");
	}
    }
}
