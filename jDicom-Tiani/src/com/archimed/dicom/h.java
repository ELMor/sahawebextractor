/* h - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.IOException;
import java.io.InputStream;

class h extends OffsetInputStream
{
    int a = 8193;
    int b = 0;
    byte[] c;
    byte[] d;
    byte[] e;
    byte[][] f;
    
    h(InputStream inputstream) {
	super(inputstream);
	c = new byte[2];
	d = new byte[4];
	e = new byte[8];
	f = new byte[][] { c, d, new byte[6], e, new byte[10], new byte[12],
			   new byte[14], new byte[16] };
    }
    
    h(InputStream inputstream, int i) {
	super(inputstream, i);
	c = new byte[2];
	d = new byte[4];
	e = new byte[8];
	f = new byte[][] { c, d, new byte[6], e, new byte[10], new byte[12],
			   new byte[14], new byte[16] };
    }
    
    private int a(byte i) {
	return i & 0xff;
    }
    
    void c(int i) {
	a = i;
    }
    
    int j() {
	return a;
    }
    
    int k() throws IOException {
	this.readFully(c);
	if (a == 8195)
	    return (a(c[0]) << 8) + a(c[1]);
	return (a(c[1]) << 8) + a(c[0]);
    }
    
    int l() throws IOException {
	this.readFully(c);
	if (a == 8195)
	    return (c[0] << 8) + a(c[1]);
	return (c[1] << 8) + a(c[0]);
    }
    
    int m() throws IOException {
	this.readFully(d);
	if (a == 8195) {
	    b = a(d[3]);
	    b += a(d[2]) << 8;
	    b += a(d[1]) << 16;
	    b += a(d[0]) << 24;
	} else {
	    b = a(d[0]);
	    b += a(d[1]) << 8;
	    b += a(d[2]) << 16;
	    b += a(d[3]) << 24;
	}
	return b;
    }
    
    int n() throws IOException {
	this.readFully(d);
	if (a == 8195) {
	    b = a(d[3]);
	    b += a(d[2]) << 8;
	    b += a(d[1]) << 16;
	    b += d[0] << 24;
	} else {
	    b = a(d[0]);
	    b += a(d[1]) << 8;
	    b += a(d[2]) << 16;
	    b += d[3] << 24;
	}
	return b;
    }
    
    long o() throws IOException {
	this.readFully(e);
	long l;
	if (a == 8195) {
	    l = (long) a(e[7]);
	    l += (long) (a(e[6]) << 8);
	    l += (long) (a(e[5]) << 16);
	    l += (long) (a(e[4]) << 24);
	    l += (long) a(e[3]) << 32;
	    l += (long) a(e[2]) << 40;
	    l += (long) a(e[1]) << 48;
	    l += (long) a(e[0]) << 56;
	} else {
	    l = (long) a(e[0]);
	    l += (long) (a(e[1]) << 8);
	    l += (long) (a(e[2]) << 16);
	    l += (long) (a(e[3]) << 24);
	    l += (long) a(e[4]) << 32;
	    l += (long) a(e[5]) << 40;
	    l += (long) a(e[6]) << 48;
	    l += (long) a(e[7]) << 56;
	}
	return l;
    }
    
    String d(int i) throws IOException {
	byte[] is = i > 16 ? new byte[i] : f[(i >> 1) - 1];
	this.readFully(is);
	return new String(is);
    }
}
