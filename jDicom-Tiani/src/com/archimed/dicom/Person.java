/* Person - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class Person
{
    public String familyName = "";
    public String givenName = "";
    public String middleName = "";
    public String prefix = "";
    public String suffix = "";
    
    public Person(String string) {
	int i;
	if ((i = string.indexOf("^")) != -1)
	    familyName = string.substring(0, i);
	else {
	    familyName = string;
	    return;
	}
	int i_0_;
	if ((i_0_ = string.indexOf("^", i + 1)) != -1)
	    givenName = string.substring(i + 1, i_0_);
	else {
	    givenName = string.substring(i + 1);
	    return;
	}
	if ((i = string.indexOf("^", i_0_ + 1)) != -1)
	    middleName = string.substring(i_0_ + 1, i);
	else {
	    middleName = string.substring(i_0_ + 1);
	    return;
	}
	if ((i_0_ = string.indexOf("^", i + 1)) != -1)
	    prefix = string.substring(i + 1, i_0_);
	else {
	    prefix = string.substring(i + 1);
	    return;
	}
	suffix = string.substring(i_0_ + 1);
    }
    
    public String toString() {
	String string = "";
	if (!prefix.equals(""))
	    string += prefix + " ";
	if (!givenName.equals(""))
	    string += givenName + " ";
	if (!middleName.equals(""))
	    string += middleName + " ";
	string += familyName;
	if (!suffix.equals(""))
	    string += ", " + suffix;
	return string;
    }
    
    public String toDICOMString() {
	return a('^');
    }
    
    String a(char c) {
	String string = familyName;
	if (!givenName.equals("") || !middleName.equals("")
	    || !prefix.equals("") || !suffix.equals(""))
	    string += c + givenName;
	if (!middleName.equals("") || !prefix.equals("") || !suffix.equals(""))
	    string += c + middleName;
	if (!prefix.equals("") || !suffix.equals(""))
	    string += c + prefix;
	if (!suffix.equals(""))
	    string += c + suffix;
	return string;
    }
}
