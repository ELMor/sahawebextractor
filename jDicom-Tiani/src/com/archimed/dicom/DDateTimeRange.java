/* DDateTimeRange - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.text.DateFormat;

public class DDateTimeRange
{
    DDateTime a;
    DDateTime b;
    
    public DDateTimeRange(DDateTime ddatetime, DDateTime ddatetime_0_) {
	if (ddatetime != null)
	    a = ddatetime;
	else
	    a = new DDateTime();
	if (ddatetime_0_ != null)
	    b = ddatetime_0_;
	else
	    b = new DDateTime();
    }
    
    public DDateTimeRange(String string) throws NumberFormatException {
	int i = string.indexOf('-');
	if (i == -1)
	    throw new NumberFormatException
		      (string
		       + " cannot parse this string into DDateTimeRange");
	a = new DDateTime(string.substring(0, i));
	b = new DDateTime(string.substring(i + 1));
    }
    
    public String toString() {
	return a.toString() + " - " + b.toString();
    }
    
    public String toString(DateFormat dateformat) {
	return a.toString(dateformat) + " - " + b.toString(dateformat);
    }
    
    public String toDICOMString() {
	return a.toDICOMString() + "-" + b.toDICOMString();
    }
    
    public DDateTime getDateTime1() {
	if (!a.isEmpty())
	    return a;
	return null;
    }
    
    public DDateTime getDateTime2() {
	if (!b.isEmpty())
	    return b;
	return null;
    }
}
