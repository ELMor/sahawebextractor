/* IntHashtable - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.util.Dictionary;
import java.util.Enumeration;

class IntHashtable extends Dictionary implements Cloneable
{
    private IntHashtableEntry[] a;
    private int b;
    private int c;
    private float d;
    
    public IntHashtable(int i, float f) {
	if (i <= 0 || (double) f <= 0.0)
	    throw new IllegalArgumentException();
	d = f;
	a = new IntHashtableEntry[i];
	c = (int) ((float) i * f);
    }
    
    public IntHashtable(int i) {
	this(i, 0.75F);
    }
    
    public IntHashtable() {
	this(101, 0.75F);
    }
    
    public int size() {
	return b;
    }
    
    public boolean isEmpty() {
	return b == 0;
    }
    
    public synchronized Enumeration keys() {
	return new IntHashtableEnumerator(a, true);
    }
    
    public synchronized Enumeration elements() {
	return new IntHashtableEnumerator(a, false);
    }
    
    public synchronized boolean contains(Object object) {
	if (object == null)
	    throw new NullPointerException();
	IntHashtableEntry[] inthashtableentrys = a;
	int i = inthashtableentrys.length;
	while (i-- > 0) {
	    for (IntHashtableEntry inthashtableentry = inthashtableentrys[i];
		 inthashtableentry != null;
		 inthashtableentry = inthashtableentry.d) {
		if (inthashtableentry.c.equals(object))
		    return true;
	    }
	}
	return false;
    }
    
    public synchronized boolean containsKey(int i) {
	IntHashtableEntry[] inthashtableentrys = a;
	int i_0_ = i;
	int i_1_ = (i_0_ & 0x7fffffff) % inthashtableentrys.length;
	for (IntHashtableEntry inthashtableentry = inthashtableentrys[i_1_];
	     inthashtableentry != null;
	     inthashtableentry = inthashtableentry.d) {
	    if (inthashtableentry.a == i_0_ && inthashtableentry.b == i)
		return true;
	}
	return false;
    }
    
    public synchronized Object get(int i) {
	IntHashtableEntry[] inthashtableentrys = a;
	int i_2_ = i;
	int i_3_ = (i_2_ & 0x7fffffff) % inthashtableentrys.length;
	for (IntHashtableEntry inthashtableentry = inthashtableentrys[i_3_];
	     inthashtableentry != null;
	     inthashtableentry = inthashtableentry.d) {
	    if (inthashtableentry.a == i_2_ && inthashtableentry.b == i)
		return inthashtableentry.c;
	}
	return null;
    }
    
    public Object get(Object object) {
	if (!(object instanceof Integer))
	    throw new InternalError("key is not an Integer");
	Integer integer = (Integer) object;
	int i = integer.intValue();
	return get(i);
    }
    
    protected void rehash() {
	int i = a.length;
	IntHashtableEntry[] inthashtableentrys = a;
	int i_4_ = i * 2 + 1;
	IntHashtableEntry[] inthashtableentrys_5_
	    = new IntHashtableEntry[i_4_];
	c = (int) ((float) i_4_ * d);
	a = inthashtableentrys_5_;
	int i_6_ = i;
	while (i_6_-- > 0) {
	    IntHashtableEntry inthashtableentry = inthashtableentrys[i_6_];
	    while (inthashtableentry != null) {
		IntHashtableEntry inthashtableentry_7_ = inthashtableentry;
		inthashtableentry = inthashtableentry.d;
		int i_8_ = (inthashtableentry_7_.a & 0x7fffffff) % i_4_;
		inthashtableentry_7_.d = inthashtableentrys_5_[i_8_];
		inthashtableentrys_5_[i_8_] = inthashtableentry_7_;
	    }
	}
    }
    
    public synchronized Object put(int i, Object object) {
	if (object == null)
	    throw new NullPointerException();
	IntHashtableEntry[] inthashtableentrys = a;
	int i_9_ = i;
	int i_10_ = (i_9_ & 0x7fffffff) % inthashtableentrys.length;
	for (IntHashtableEntry inthashtableentry = inthashtableentrys[i_10_];
	     inthashtableentry != null;
	     inthashtableentry = inthashtableentry.d) {
	    if (inthashtableentry.a == i_9_ && inthashtableentry.b == i) {
		Object object_11_ = inthashtableentry.c;
		inthashtableentry.c = object;
		return object_11_;
	    }
	}
	if (b >= c) {
	    rehash();
	    return put(i, object);
	}
	IntHashtableEntry inthashtableentry = new IntHashtableEntry();
	inthashtableentry.a = i_9_;
	inthashtableentry.b = i;
	inthashtableentry.c = object;
	inthashtableentry.d = inthashtableentrys[i_10_];
	inthashtableentrys[i_10_] = inthashtableentry;
	b++;
	return null;
    }
    
    public Object put(Object object, Object object_12_) {
	if (!(object instanceof Integer))
	    throw new InternalError("key is not an Integer");
	Integer integer = (Integer) object;
	int i = integer.intValue();
	return put(i, object_12_);
    }
    
    public synchronized Object remove(int i) {
	IntHashtableEntry[] inthashtableentrys = a;
	int i_13_ = i;
	int i_14_ = (i_13_ & 0x7fffffff) % inthashtableentrys.length;
	IntHashtableEntry inthashtableentry = inthashtableentrys[i_14_];
	IntHashtableEntry inthashtableentry_15_ = null;
	for (/**/; inthashtableentry != null;
	     inthashtableentry = inthashtableentry.d) {
	    if (inthashtableentry.a == i_13_ && inthashtableentry.b == i) {
		if (inthashtableentry_15_ != null)
		    inthashtableentry_15_.d = inthashtableentry.d;
		else
		    inthashtableentrys[i_14_] = inthashtableentry.d;
		b--;
		return inthashtableentry.c;
	    }
	    inthashtableentry_15_ = inthashtableentry;
	}
	return null;
    }
    
    public Object remove(Object object) {
	if (!(object instanceof Integer))
	    throw new InternalError("key is not an Integer");
	Integer integer = (Integer) object;
	int i = integer.intValue();
	return remove(i);
    }
    
    public synchronized void clear() {
	IntHashtableEntry[] inthashtableentrys = a;
	int i = inthashtableentrys.length;
	while (--i >= 0)
	    inthashtableentrys[i] = null;
	b = 0;
    }
    
    public synchronized Object clone() {
	try {
	    IntHashtable inthashtable_16_ = (IntHashtable) super.clone();
	    inthashtable_16_.a = new IntHashtableEntry[a.length];
	    int i = a.length;
	    while (i-- > 0)
		inthashtable_16_.a[i]
		    = a[i] != null ? (IntHashtableEntry) a[i].clone() : null;
	    return inthashtable_16_;
	} catch (CloneNotSupportedException clonenotsupportedexception) {
	    throw new InternalError();
	}
    }
    
    public synchronized String toString() {
	int i = size() - 1;
	StringBuffer stringbuffer = new StringBuffer();
	Enumeration enumeration = keys();
	Enumeration enumeration_17_ = elements();
	stringbuffer.append("{");
	for (int i_18_ = 0; i_18_ <= i; i_18_++) {
	    String string = enumeration.nextElement().toString();
	    String string_19_ = enumeration_17_.nextElement().toString();
	    stringbuffer.append(string + "=" + string_19_);
	    if (i_18_ < i)
		stringbuffer.append(", ");
	}
	stringbuffer.append("}");
	return stringbuffer.toString();
    }
}
