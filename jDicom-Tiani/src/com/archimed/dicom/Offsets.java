/* Offsets - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.util.Enumeration;

public class Offsets
{
    DicomObject a;
    
    public Offsets(DicomObject dicomobject) {
	a = dicomobject;
    }
    
    private int a(DicomObject dicomobject, boolean bool, int i,
		  boolean bool_0_) {
	int i_1_ = 0;
	Enumeration enumeration = dicomobject.a(bool, i, bool_0_, true);
	while (enumeration.hasMoreElements()) {
	    VR vr = (VR) enumeration.nextElement();
	    i_1_ += vr.headerLen + vr.dataLen;
	}
	return i_1_;
    }
    
    public long calculateOffset_ge(DicomObject dicomobject, int i, int i_2_,
				   int i_3_, boolean bool, boolean bool_4_,
				   boolean bool_5_) {
	VR vr = dicomobject.a(i, i_2_);
	long l = 0L;
	if (vr == null)
	    return 0L;
	if (bool_5_) {
	    try {
		dicomobject.a(i_3_);
	    } catch (DicomException dicomexception) {
		dicomexception.printStackTrace();
	    }
	    DicomObject dicomobject_6_ = dicomobject.getFileMetaInformation();
	    if (dicomobject_6_ != null)
		l = (long) (132 + a(dicomobject_6_, true, 8194, bool));
	}
	Enumeration enumeration = dicomobject.a(bool_4_, i_3_, bool, true);
	while (enumeration.hasMoreElements()) {
	    vr = (VR) enumeration.nextElement();
	    if (vr.group == i && vr.element == i_2_)
		break;
	    l += (long) (vr.dataLen + vr.headerLen);
	}
	return l;
    }
    
    public long calculateOffset_ge(int i, int i_7_, int i_8_, boolean bool,
				   boolean bool_9_, boolean bool_10_) {
	return calculateOffset_ge(a, i, i_7_, i_8_, bool, bool_9_, bool_10_);
    }
    
    public long calculateOffset(int i, int i_11_, boolean bool,
				boolean bool_12_, boolean bool_13_) {
	int i_14_ = DDict.getGroup(i);
	int i_15_ = DDict.getElement(i);
	return calculateOffset_ge(i_14_, i_15_, i_11_, bool, bool_12_,
				  bool_13_);
    }
    
    public long calculateOffset(int i, int i_16_, int i_17_, boolean bool,
				boolean bool_18_, boolean bool_19_) {
	long l = calculateOffset(i, i_17_, bool, bool_18_, bool_19_);
	VR vr = a.a(DDict.getGroup(i), DDict.getElement(i));
	if (i_16_ >= vr.val.size())
	    return 0L;
	if (vr.dcm_type != 10)
	    return 0L;
	l += (long) vr.headerLen;
	for (int i_20_ = 0; i_20_ < i_16_; i_20_++)
	    l += (long) ((DicomObject) vr.val.elementAt(i_20_)).c;
	return l;
    }
    
    public long calculateOffset(int i, int i_21_, int i_22_, int i_23_,
				boolean bool, boolean bool_24_,
				boolean bool_25_) {
	long l = calculateOffset(i, i_21_, i_23_, bool, bool_24_, bool_25_);
	VR vr = a.a(DDict.getGroup(i), DDict.getElement(i));
	DicomObject dicomobject = (DicomObject) vr.val.elementAt(i_21_);
	l += 8L + calculateOffset_ge(dicomobject, DDict.getGroup(i_22_),
				     DDict.getElement(i_22_), i_23_, bool,
				     false, false);
	return l;
    }
}
