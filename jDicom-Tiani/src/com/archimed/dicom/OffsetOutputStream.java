/* OffsetOutputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class OffsetOutputStream extends FilterOutputStream
{
    private long a = 0L;
    
    public OffsetOutputStream(OutputStream outputstream) {
	this(outputstream, 0);
    }
    
    public OffsetOutputStream(OutputStream outputstream, int i) {
	super(outputstream);
	a = (long) i;
    }
    
    public void write(byte[] is, int i, int i_0_) throws IOException {
	a += (long) i_0_;
	out.write(is, i, i_0_);
    }
    
    public void write(int i) throws IOException {
	a++;
	out.write(i);
    }
    
    public void write(byte[] is) throws IOException {
	write(is, 0, is.length);
    }
}
