/* i - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

class i extends j
{
    Vector a;
    
    i(OutputStream outputstream) {
	super(outputstream);
    }
    
    i(OutputStream outputstream, int i) {
	super(outputstream, i);
    }
    
    private int d(int i) {
	switch (i) {
	case 2:
	case 8:
	case 22:
	case 24:
	    return 0;
	default:
	    return 32;
	}
    }
    
    int a(Vector vector, int i) throws IOException {
	int i_0_ = 0;
	for (int i_1_ = 0; i_1_ < vector.size(); i_1_++)
	    i_0_ += this.b(vector.elementAt(i_1_), i);
	return i_0_;
    }
    
    String a(Object object, int i) {
	switch (i) {
	case 2:
	case 4:
	case 6:
	case 7:
	case 9:
	case 13:
	case 18:
	case 27:
	    return (String) object;
	case 16:
	    return ((Float) object).toString();
	case 15:
	    return ((Integer) object).toString();
	case 14:
	    return ((Person) object).a('^');
	case 11:
	    try {
		return ((DDate) object).toDICOMString();
	    } catch (ClassCastException classcastexception) {
		return ((DDateRange) object).toDICOMString();
	    }
	case 28:
	    try {
		return ((DDateTime) object).toDICOMString();
	    } catch (ClassCastException classcastexception) {
		return ((DDateTimeRange) object).toDICOMString();
	    }
	case 12:
	    try {
		return ((DTime) object).toDICOMString();
	    } catch (ClassCastException classcastexception) {
		return ((DTimeRange) object).toDICOMString();
	    }
	case 0:
	    return new String((byte[]) object);
	default:
	    return null;
	}
    }
    
    int b(Vector vector, int i) throws IOException {
	int i_2_ = 0;
	for (int i_3_ = 0; i_3_ < vector.size(); i_3_++) {
	    String string = a(vector.elementAt(i_3_), i);
	    i_2_ += this.a(string);
	    if (i_3_ != vector.size() - 1) {
		this.write(92);
		i_2_++;
	    }
	}
	return i_2_;
    }
    
    int c(Vector vector, int i) throws IOException {
	byte[] is = (byte[]) vector.elementAt(0);
	if (i == 24 && this.a() == 8195) {
	    byte[] is_4_ = new byte[is.length];
	    System.arraycopy(is, 0, is_4_, 0, is_4_.length);
	    DicomUtils.a(is_4_);
	    this.write(is_4_);
	    return is_4_.length;
	}
	this.write(is);
	return is.length;
    }
    
    int d(Vector vector, int i) throws IOException {
	int i_5_ = DDict.c(i);
	int i_6_ = 0;
	if (i_5_ != 0)
	    return a(vector, i);
	switch (i) {
	case 8:
	case 22:
	case 24:
	    i_6_ += c(vector, i);
	    break;
	default:
	    i_6_ += b(vector, i);
	}
	if (i_6_ % 2 == 1) {
	    this.write(d(i));
	    i_6_++;
	}
	return i_6_;
    }
}
