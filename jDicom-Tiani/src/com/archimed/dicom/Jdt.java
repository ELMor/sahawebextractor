/* Jdt - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class Jdt
{
    public static String getVersion() {
	return "JDT 1.05";
    }
    
    public static String getAboutInfo() {
	return getVersion() + " (c) 1999, SoftLink (www.softlink.be)";
    }
}
