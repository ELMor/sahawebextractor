/* GroupList - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.util.Enumeration;
import java.util.Vector;

public class GroupList
{
    private IntHashtable a = new IntHashtable();
    
    void a(VR vr) {
	if (vr != null && vr.element != 0) {
	    IntHashtable inthashtable = (IntHashtable) a.get(vr.group);
	    if (inthashtable == null) {
		inthashtable = new IntHashtable();
		a.put(vr.group, inthashtable);
	    }
	    inthashtable.put(vr.element, vr);
	}
    }
    
    public void clear() {
	Enumeration enumeration = a.elements();
	while (enumeration.hasMoreElements()) {
	    IntHashtable inthashtable
		= (IntHashtable) enumeration.nextElement();
	    inthashtable.clear();
	}
	a.clear();
    }
    
    VR a(int i, int i_0_) {
	IntHashtable inthashtable = (IntHashtable) a.get(i);
	if (inthashtable == null)
	    return null;
	VR vr = (VR) inthashtable.get(i_0_);
	return vr;
    }
    
    VR b(int i, int i_1_) {
	IntHashtable inthashtable = (IntHashtable) a.get(i);
	if (inthashtable == null)
	    return null;
	VR vr = (VR) inthashtable.remove(i_1_);
	return vr;
    }
    
    public boolean isEmpty() {
	return a.isEmpty();
    }
    
    private void a(int i, boolean bool) {
	Enumeration enumeration = a.keys();
	while (enumeration.hasMoreElements()) {
	    Integer integer = (Integer) enumeration.nextElement();
	    Enumeration enumeration_2_
		= ((IntHashtable) a.get(integer)).elements();
	    int i_3_;
	    if (integer.intValue() == 2)
		i_3_ = 8194;
	    else
		i_3_ = i;
	    while (enumeration_2_.hasMoreElements()) {
		VR vr = (VR) enumeration_2_.nextElement();
		vr.dataLen = b.a(vr.val, vr.dcm_type, i_3_, bool);
		vr.headerLen = b.b(vr.val, vr.dcm_type, i_3_, bool);
	    }
	}
    }
    
    public Enumeration enumerateVRs(boolean bool, boolean bool_4_) {
	if (bool && !bool_4_)
	    throw new IllegalArgumentException();
	return a(bool, 8193, true, bool_4_);
    }
    
    Enumeration a(boolean bool, int i, boolean bool_5_, boolean bool_6_) {
	Vector vector = new Vector();
	Vector vector_7_ = new Vector();
	Vector vector_8_ = new Vector();
	if (bool || bool_6_)
	    a(i, bool_5_);
	Enumeration enumeration = a.keys();
	while (enumeration.hasMoreElements())
	    vector_7_.addElement(enumeration.nextElement());
	if (bool_6_)
	    vector_7_ = DicomUtils.bubbleSort(vector_7_);
	for (int i_9_ = 0; i_9_ < vector_7_.size(); i_9_++) {
	    vector = new Vector();
	    int i_10_ = ((Integer) vector_7_.elementAt(i_9_)).intValue();
	    IntHashtable inthashtable = (IntHashtable) a.get(i_10_);
	    if (bool) {
		if (i_10_ == 2) {
		    int i_11_ = 8194;
		} else {
		    int i_12_ = i;
		}
		int i_13_ = 0;
		Enumeration enumeration_14_ = inthashtable.elements();
		while (enumeration_14_.hasMoreElements()) {
		    VR vr = (VR) enumeration_14_.nextElement();
		    i_13_ += vr.headerLen + vr.dataLen;
		}
		vector_8_.addElement(new VR(i_10_, 0, 1,
					    new Long((long) i_13_), 4));
	    }
	    Enumeration enumeration_15_ = inthashtable.keys();
	    while (enumeration_15_.hasMoreElements())
		vector.addElement(enumeration_15_.nextElement());
	    vector = DicomUtils.bubbleSort(vector);
	    for (int i_16_ = 0; i_16_ < vector.size(); i_16_++) {
		VR vr = (VR) inthashtable.get(vector.elementAt(i_16_));
		vector_8_.addElement(vr);
	    }
	}
	return vector_8_.elements();
    }
    
    public DicomObject copyGroup(int i) {
	DicomObject dicomobject = new DicomObject();
	IntHashtable inthashtable = (IntHashtable) a.get(i);
	Enumeration enumeration = inthashtable.keys();
	while (enumeration.hasMoreElements()) {
	    VR vr = (VR) inthashtable.get(enumeration.nextElement());
	    dicomobject.a(vr);
	}
	return dicomobject;
    }
    
    public DicomObject removeGroup(int i) {
	DicomObject dicomobject = new DicomObject();
	if (a == null)
	    return null;
	IntHashtable inthashtable = (IntHashtable) a.get(i);
	if (inthashtable == null)
	    return null;
	Enumeration enumeration = inthashtable.keys();
	while (enumeration.hasMoreElements()) {
	    VR vr = (VR) inthashtable.get(enumeration.nextElement());
	    dicomobject.a(vr);
	}
	a.remove(i);
	System.gc();
	return dicomobject;
    }
    
    public boolean containsGroup(int i) {
	return a.containsKey(i);
    }
    
    void a(DicomObject dicomobject) {
	Enumeration enumeration = ((GroupList) dicomobject).a.keys();
	int i = ((Integer) enumeration.nextElement()).intValue();
	if (!containsGroup(i))
	    a.put(i, ((GroupList) dicomobject).a.get(i));
    }
    
    public void addGroups(DicomObject dicomobject) {
	Enumeration enumeration = ((GroupList) dicomobject).a.keys();
	while (enumeration.hasMoreElements()) {
	    int i = ((Integer) enumeration.nextElement()).intValue();
	    DicomObject dicomobject_17_ = dicomobject.copyGroup(i);
	    a(dicomobject_17_);
	}
    }
    
    public int numberOfGroups() {
	return a.size();
    }
    
    public int numberOfElements() {
	int i = 0;
	Enumeration enumeration = a.elements();
	while (enumeration.hasMoreElements()) {
	    IntHashtable inthashtable
		= (IntHashtable) enumeration.nextElement();
	    i += inthashtable.size();
	}
	return i;
    }
}
