/* Compression - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.codec;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import com.archimed.codec.jpeg.p;
import com.archimed.codec.jpeg.w;
import com.archimed.codec.rle.u;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;

public class Compression
{
    DicomObject a;
    
    public Compression() {
	a = null;
    }
    
    public Compression(DicomObject dicomobject) throws DicomException {
	setDicomObject(dicomobject);
    }
    
    public void decompress() throws DicomException, IOException {
	if (a == null)
	    throw new DicomException
		      ("Use setDicomObject first to specify a DicomObject");
	int i;
	try {
	    DicomObject dicomobject = a.getFileMetaInformation();
	    if (dicomobject == null)
		throw new DicomException("no file meta information");
	    i = UID.getUIDEntry(dicomobject.getS(31)).getConstant();
	} catch (UnknownUIDException unknownuidexception) {
	    throw new DicomException("Decompression not possible: "
				     + unknownuidexception.getMessage());
	} catch (Exception exception) {
	    throw new DicomException
		      ("Decompression not possible: no transfersyntax found");
	}
	int i_0_ = a.getI(467);
	int i_1_ = a.getI(466);
	int i_2_ = a.getSize(1184) - 1;
	int i_3_ = a.getI(475);
	int i_4_ = a.getI(461);
	int i_5_ = i_0_ * i_1_ * (i_3_ >> 3) * i_4_;
	byte[] is = i == 8197 ? new byte[i_5_] : null;
	a.deleteItem(1184, 0);
	if (i_2_ == 1) {
	    is = decompressFrame(i, (byte[]) a.deleteItem(1184, 0), i_0_, i_1_,
				 is);
	    a.set(1184, is);
	} else {
	    byte[] is_6_ = new byte[i_5_ * i_2_];
	    for (int i_7_ = 0; i_7_ < i_2_; i_7_++) {
		is = decompressFrame(i, (byte[]) a.deleteItem(1184, 0), i_0_,
				     i_1_, is);
		System.arraycopy(is, 0, is_6_, i_7_ * i_5_, i_5_);
	    }
	    a.set(1184, is_6_);
	}
	try {
	    a.getFileMetaInformation().set(31,
					   UID.getUIDEntry(8194).getValue());
	} catch (IllegalValueException illegalvalueexception) {
	    /* empty */
	}
    }
    
    public static byte[] decompressFrame
	(int i, byte[] is, int i_8_, int i_9_, byte[] is_10_)
	throws DicomException, IOException {
	if (i == 8197) {
	    p var_p;
	    try {
		var_p = (p) Class.forName
				("com.tiani.codec.jpeg.Jpeg14Decoder")
				.newInstance();
		if (Debug.DEBUG > 1)
		    Debug.out
			.println("Use com.tiani.codec.jpeg.Jpeg14Decoder");
	    } catch (Exception exception) {
		var_p = new p();
		if (Debug.DEBUG > 1)
		    Debug.out
			.println("Use com.archimed.codec.jpeg.Jpeg14Decoder");
	    }
	    byte[] is_11_ = var_p.decode(is, is_10_);
	    return is_11_;
	}
	if (i == 8198) {
	    ByteArrayInputStream bytearrayinputstream
		= new ByteArrayInputStream(is);
	    u var_u = new u();
	    var_u.setDimension(i_8_, i_9_);
	    return var_u.read(bytearrayinputstream);
	}
	if (i == 8196) {
	    Image image = Toolkit.getDefaultToolkit().createImage(is);
	    PixelGrabber pixelgrabber
		= new PixelGrabber(image, 0, 0, i_8_, i_9_, false);
	    try {
		pixelgrabber.grabPixels();
	    } catch (InterruptedException interruptedexception) {
		interruptedexception.printStackTrace();
	    }
	    try {
		return (byte[]) pixelgrabber.getPixels();
	    } catch (ClassCastException classcastexception) {
		return a((int[]) pixelgrabber.getPixels());
	    }
	}
	throw new DicomException
		  ("unsupported transfer syntax for decompression");
    }
    
    private static byte[] a(int[] is) {
	byte[] is_12_ = new byte[is.length * 3];
	for (int i = 0; i < is.length; i++) {
	    is_12_[3 * i] = (byte) ((is[i] & 0xff0000) >> 16);
	    is_12_[3 * i + 1] = (byte) ((is[i] & 0xff00) >> 8);
	    is_12_[3 * i + 2] = (byte) (is[i] & 0xff);
	}
	return is_12_;
    }
    
    public void compress(int i) throws IOException, DicomException {
	if (a == null)
	    throw new DicomException
		      ("Use setDicomObject first to specify a DicomObject");
	if (i != 8197 && i != 8198)
	    throw new DicomException
		      ("unsupported transfer syntax for compression");
	int i_13_;
	try {
	    DicomObject dicomobject = a.getFileMetaInformation();
	    if (dicomobject == null)
		i_13_ = 8193;
	    else if (dicomobject.getSize(31) > 0)
		i_13_ = UID.getUIDEntry(dicomobject.getS(31)).getConstant();
	    else
		i_13_ = 8193;
	} catch (UnknownUIDException unknownuidexception) {
	    throw new DicomException("Decompression not possible: "
				     + unknownuidexception.getMessage());
	}
	if (i_13_ != 8195 && i_13_ != 8194 && i_13_ != 8193)
	    throw new DicomException
		      ("associated DicomObject already has a compressed transfer syntax");
	int i_14_ = a.getI(467);
	int i_15_ = a.getI(466);
	int i_16_ = 1;
	if (a.getSize(464) > 0)
	    i_16_ = a.getI(464);
	byte[] is = (byte[]) a.get(1184);
	a.deleteItem(1184, 0);
	a.set(1184, new byte[0], 0);
	int i_17_ = a.getI(475);
	int i_18_ = a.getI(461);
	if (i_16_ == 1 && i_18_ == 1)
	    a.set(1184, compressFrame(i, is, i_14_, i_15_), 1);
	else {
	    int i_19_ = i_14_ * i_15_ * (i_17_ >> 3);
	    if (i_18_ == 1) {
		byte[] is_20_ = new byte[i_19_];
		for (int i_21_ = 0; i_21_ < i_16_; i_21_++) {
		    System.arraycopy(is, i_21_ * is_20_.length, is_20_, 0,
				     is_20_.length);
		    a.append(1184, compressFrame(i, is_20_, i_14_, i_15_));
		}
	    } else {
		if (i_18_ != 3 || i_17_ != 8)
		    throw new DicomException("compression of pixel data with "
					     + i_18_ + " samples per bit and "
					     + i_17_
					     + " bits allocated unsupported");
		byte[] is_22_ = new byte[i_19_];
		byte[] is_23_ = new byte[i_19_];
		byte[] is_24_ = new byte[i_19_];
		int i_25_ = 0;
		if (a.getI(463) == 1) {
		    for (int i_26_ = 0; i_26_ < i_16_; i_26_++) {
			System.arraycopy(is, i_25_, is_22_, 0, i_19_);
			i_25_ += i_19_;
			System.arraycopy(is, i_25_, is_23_, 0, i_19_);
			i_25_ += i_19_;
			System.arraycopy(is, i_25_, is_24_, 0, i_19_);
			i_25_ += i_19_;
			a.append(1184, compressFrame(i, is_22_, is_23_, is_24_,
						     i_14_, i_15_));
		    }
		    a.set(463, new Integer(0));
		} else {
		    for (int i_27_ = 0; i_27_ < i_16_; i_27_++) {
			for (int i_28_ = 0; i_28_ < i_19_; i_28_++) {
			    is_22_[i_28_] = is[i_25_++];
			    is_23_[i_28_] = is[i_25_++];
			    is_24_[i_28_] = is[i_25_++];
			}
			a.append(1184, compressFrame(i, is_22_, is_23_, is_24_,
						     i_14_, i_15_));
		    }
		}
	    }
	}
    }
    
    public static byte[] compressFrame
	(int i, byte[] is, int i_29_, int i_30_)
	throws IOException, DicomException {
	if (i == 8197) {
	    w var_w = new w();
	    if (is.length == i_29_ * i_30_)
		return var_w.encode8bitGrayscale(is, i_29_, i_30_);
	    if (is.length == i_29_ * i_30_ * 2)
		return var_w.encode16bitGrayscale(is, i_29_, i_30_);
	    if (is.length == i_29_ * i_30_ * 3)
		throw new DicomException
			  ("24 bit color lossless jpeg compression unsupported");
	    throw new DicomException
		      ("length of frame to compress is not equal to width*height or width*height*2 or width*height*3");
	}
	throw new DicomException
		  ("unsupported transfer syntax for compression");
    }
    
    public static byte[] compressFrame
	(int i, byte[] is, byte[] is_31_, byte[] is_32_, int i_33_, int i_34_)
	throws IOException, DicomException {
	if (i == 8197) {
	    w var_w = new w();
	    return var_w.encode24bitColor(is, is_31_, is_32_, i_33_, i_34_);
	}
	throw new DicomException
		  ("unsupported transfer syntax for compression");
    }
    
    public void setDicomObject(DicomObject dicomobject) throws DicomException {
	a = dicomobject;
    }
    
    public DicomObject getDicomObject() {
	return a;
    }
}
