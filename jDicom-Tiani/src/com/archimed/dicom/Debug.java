/* Debug - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.io.PrintStream;

public class Debug
{
    public static PrintStream out = System.out;
    public static String dumpCmdsetIntoDir = null;
    public static String dumpDatasetIntoDir = null;
    public static int DEBUG = 0;
    
    public static void printMessage(String string, int i, int i_0_) {
	out.println(string + ": (" + DumpUtils.a(i, 4) + ","
		    + DumpUtils.a(i_0_, 4) + ")  "
		    + DDict.getDescription(DDict.lookupDDict(i, i_0_)) + ".");
    }
    
    static void a(String string) {
	out.println(string);
    }
}
