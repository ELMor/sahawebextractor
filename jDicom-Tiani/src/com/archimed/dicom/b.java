/* b - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.util.Enumeration;
import java.util.Vector;

class b
{
    static int a(int i) {
	if ((i & 0x1) == 1)
	    return i + 1;
	return i;
    }
    
    private static int a(Vector vector) {
	if (vector.size() > 0)
	    return vector.size() - 1;
	return 0;
    }
    
    private static int b(Vector vector) {
	int i = 0;
	for (int i_0_ = 0; i_0_ < vector.size(); i_0_++)
	    i += ((String) vector.elementAt(i_0_)).length();
	return i;
    }
    
    private static int c(Vector vector) {
	int i = 0;
	for (int i_1_ = 0; i_1_ < vector.size(); i_1_++)
	    i += ((Float) vector.elementAt(i_1_)).toString().length();
	return i;
    }
    
    private static int d(Vector vector) {
	int i = 0;
	for (int i_2_ = 0; i_2_ < vector.size(); i_2_++)
	    i += ((Integer) vector.elementAt(i_2_)).toString().length();
	return i;
    }
    
    private static int e(Vector vector) {
	int i = 0;
	for (int i_3_ = 0; i_3_ < vector.size(); i_3_++)
	    i += ((Person) vector.elementAt(i_3_)).a('^').length();
	return i;
    }
    
    private static int f(Vector vector) {
	int i = 0;
	for (int i_4_ = 0; i_4_ < vector.size(); i_4_++) {
	    Object object = vector.elementAt(i_4_);
	    if (object instanceof DDateTime)
		i += ((DDateTime) object).toDICOMString().length();
	    else
		i += ((DDateTimeRange) object).toDICOMString().length();
	}
	return i;
    }
    
    private static int g(Vector vector) {
	int i = 0;
	for (int i_5_ = 0; i_5_ < vector.size(); i_5_++) {
	    Object object = vector.elementAt(i_5_);
	    if (object instanceof DTime)
		i += ((DTime) object).toDICOMString().length();
	    else
		i += ((DTimeRange) object).toDICOMString().length();
	}
	return i;
    }
    
    private static int h(Vector vector) {
	boolean bool = false;
	if (vector.size() == 0)
	    return 0;
	int i;
	try {
	    i = ((DDateRange) vector.elementAt(0)).toDICOMString().length();
	} catch (ClassCastException classcastexception) {
	    i = vector.size() * 8;
	}
	return i;
    }
    
    private static int i(Vector vector) {
	int i = 0;
	for (int i_6_ = 0; i_6_ < vector.size(); i_6_++)
	    i += ((byte[]) vector.elementAt(i_6_)).length;
	return i;
    }
    
    static int a(DicomObject dicomobject, int i, boolean bool) {
	boolean bool_7_ = false;
	int i_8_;
	if (bool)
	    i_8_ = 16;
	else
	    i_8_ = 8;
	Enumeration enumeration = dicomobject.a(false, i, bool, true);
	while (enumeration.hasMoreElements()) {
	    VR vr = (VR) enumeration.nextElement();
	    i_8_ += b(vr.val, vr.dcm_type, i, bool);
	    i_8_ += a(vr.val, vr.dcm_type, i, bool);
	}
	return i_8_;
    }
    
    private static int a(Vector vector, int i, boolean bool) {
	int i_9_ = 0;
	for (int i_10_ = 0; i_10_ < vector.size(); i_10_++) {
	    DicomObject dicomobject = (DicomObject) vector.elementAt(i_10_);
	    dicomobject.c = a(dicomobject, i, bool);
	    i_9_ += dicomobject.c;
	}
	return i_9_;
    }
    
    private static int a(Vector vector, boolean bool) {
	int i;
	if (bool) {
	    i = 8;
	    int i_11_ = 16;
	} else {
	    i = 0;
	    int i_12_ = 8;
	}
	return i;
    }
    
    private static int j(Vector vector) {
	if (vector.size() == 1 || vector.size() == 0)
	    return 0;
	return 8 + vector.size() * 8;
    }
    
    static int a(Vector vector, int i, int i_13_, boolean bool) {
	int i_14_ = DDict.c(i);
	boolean bool_15_ = false;
	if (i_14_ != 0)
	    return i_14_ * vector.size();
	int i_16_;
	switch (i) {
	case 2:
	case 4:
	case 6:
	case 7:
	case 9:
	case 13:
	case 18:
	case 27:
	    i_16_ = b(vector);
	    break;
	case 12:
	    i_16_ = g(vector);
	    break;
	case 28:
	    i_16_ = f(vector);
	    break;
	case 16:
	    i_16_ = c(vector);
	    break;
	case 15:
	    i_16_ = d(vector);
	    break;
	case 14:
	    i_16_ = e(vector);
	    break;
	case 11:
	    i_16_ = h(vector);
	    break;
	case 10:
	    if (vector.isEmpty())
		return 0;
	    return a(a(vector, i_13_, bool) + a(vector, bool));
	case 8:
	case 22:
	case 24:
	    return a(i(vector) + j(vector));
	default:
	    i_16_ = i(vector);
	}
	i_16_ += a(vector);
	return a(i_16_);
    }
    
    private static int a(int i, int i_17_) {
	if (i_17_ == 8193)
	    return 8;
	switch (i) {
	case 0:
	case 8:
	case 10:
	case 22:
	case 24:
	case 27:
	    return 12;
	default:
	    return 8;
	}
    }
    
    static int b(Vector vector, int i, int i_18_, boolean bool) {
	return a(i, i_18_);
    }
    
    static int a(IntHashtable inthashtable, int i, boolean bool) {
	int i_19_ = 0;
	Enumeration enumeration = inthashtable.elements();
	while (enumeration.hasMoreElements()) {
	    VR vr = (VR) enumeration.nextElement();
	    i_19_ += b(vr.val, vr.dcm_type, i, bool);
	    i_19_ += a(vr.val, vr.dcm_type, i, bool);
	}
	return i_19_;
    }
}
