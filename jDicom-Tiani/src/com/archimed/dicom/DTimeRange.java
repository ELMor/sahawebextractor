/* DTimeRange - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.text.DateFormat;

public class DTimeRange
{
    DTime a;
    DTime b;
    
    public DTimeRange(DTime dtime, DTime dtime_0_) {
	if (dtime != null)
	    a = dtime;
	else
	    a = new DTime();
	if (dtime_0_ != null)
	    b = dtime_0_;
	else
	    b = new DTime();
    }
    
    public DTimeRange(String string) throws NumberFormatException {
	int i = string.indexOf('-');
	if (i == -1)
	    throw new NumberFormatException
		      (string + " cannot parse this string into DTimeRange");
	a = new DTime(string.substring(0, i));
	b = new DTime(string.substring(i + 1));
    }
    
    public String toString() {
	return a.toString() + " - " + b.toString();
    }
    
    public String toString(DateFormat dateformat) {
	return a.toString(dateformat) + " - " + b.toString(dateformat);
    }
    
    public String toDICOMString() {
	return a.toDICOMString() + "-" + b.toDICOMString();
    }
    
    public DTime getTime1() {
	if (!a.isEmpty())
	    return a;
	return null;
    }
    
    public DTime getTime2() {
	if (!b.isEmpty())
	    return b;
	return null;
    }
}
