/* ImageIO - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.image;
import java.awt.Toolkit;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageProducer;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.IOException;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;

public class ImageIO
{
    DicomImage a;
    int b;
    int c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    String j;
    boolean k = false;
    int l;
    
    void a(int i) {
	switch (i) {
	case 8193:
	case 8194:
	case 8195:
	    l = 0;
	    break;
	default:
	    l = i;
	}
    }
    
    Object b(int i) {
	if (l != 0) {
	    try {
		return Compression.decompressFrame(l,
						   (byte[]) a.get(1184, i + 1),
						   b, c, null);
	    } catch (DicomException dicomexception) {
		if (Debug.DEBUG > 0)
		    dicomexception.printStackTrace();
		return null;
	    } catch (IOException ioexception) {
		if (Debug.DEBUG > 0)
		    ioexception.printStackTrace();
		return null;
	    }
	}
	byte[] is;
	if (f == 1)
	    is = (byte[]) a.get(1184);
	else if (d > 8) {
	    is = new byte[b * c * d / 8];
	    System.arraycopy(a.get(1184), b * c * i * d / 8, is, 0,
			     b * c * d / 8);
	} else if (j.equals("RGB")) {
	    is = new byte[b * c * 3];
	    System.arraycopy(a.get(1184), b * c * i * 3, is, 0, b * c * 3);
	} else {
	    is = new byte[b * c];
	    System.arraycopy(a.get(1184), b * c * i, is, 0, b * c);
	}
	if (j.equals("PALETTE COLOR"))
	    return is;
	if (j.startsWith("MONOCHROME")) {
	    if (d == 8)
		return is;
	    if (d == 16) {
		int[] is_0_ = new int[b * c];
		for (int i_1_ = 0; i_1_ < b * c; i_1_++) {
		    is_0_[i_1_] = x.a(is[2 * i_1_]);
		    is_0_[i_1_] += x.a(is[2 * i_1_ + 1]) << 8;
		    is_0_[i_1_] &= this.i;
		}
		return is_0_;
	    }
	    if (d == 24) {
		int[] is_2_ = new int[b * c];
		for (int i_3_ = 0; i_3_ < b * c; i_3_++) {
		    is_2_[i_3_] = x.a(is[3 * i_3_]);
		    is_2_[i_3_] += x.a(is[3 * i_3_ + 1]) << 8;
		    is_2_[i_3_] += x.a(is[3 * i_3_ + 2]) << 16;
		    is_2_[i_3_] &= this.i;
		}
		return is_2_;
	    }
	    if (d == 32) {
		int[] is_4_ = new int[b * c];
		for (int i_5_ = 0; i_5_ < b * c; i_5_++) {
		    is_4_[i_5_] = x.a(is[4 * i_5_]);
		    is_4_[i_5_] += x.a(is[4 * i_5_ + 1]) << 8;
		    is_4_[i_5_] += x.a(is[4 * i_5_ + 2]) << 16;
		    is_4_[i_5_] += x.a(is[4 * i_5_ + 3]) << 24;
		    is_4_[i_5_] &= this.i;
		}
		return is_4_;
	    }
	    System.out.println("BA: " + d);
	}
	if (j.equals("RGB")) {
	    int[] is_6_ = new int[b * c];
	    if (h == 0) {
		for (int i_7_ = 0; i_7_ < b * c; i_7_++) {
		    is_6_[i_7_] = x.a(is[3 * i_7_]) << 16;
		    is_6_[i_7_] += x.a(is[3 * i_7_ + 1]) << 8;
		    is_6_[i_7_] += x.a(is[3 * i_7_ + 2]);
		}
	    } else {
		for (int i_8_ = 0; i_8_ < b * c; i_8_++) {
		    is_6_[i_8_] = x.a(is[i_8_]) << 16;
		    is_6_[i_8_] += x.a(is[b * c + i_8_]) << 8;
		    is_6_[i_8_] += x.a(is[2 * b * c + i_8_]);
		}
	    }
	    return is_6_;
	}
	return null;
    }
    
    byte[] a(int i, int i_9_) throws DicomException {
	int i_10_ = a.getI(i_9_, 0);
	int i_11_ = a.getI(i_9_, 1);
	byte[] is = new byte[256];
	int i_12_;
	if ((i_12_ = a.getSize(i)) == 1) {
	    byte[] is_13_ = (byte[]) a.get(i);
	    int i_14_ = x.a(is_13_[0]);
	    i_14_ += x.a(is_13_[1]) << 8;
	    for (i_12_ = 0; i_12_ < i_11_; i_12_++)
		is[i_12_] = (byte) (i_14_ >> 8);
	    for (int i_15_ = 0; i_15_ < i_10_; i_15_++) {
		i_14_ = x.a(is_13_[2 * i_15_]);
		i_14_ += x.a(is_13_[2 * i_15_ + 1]) << 8;
		is[i_15_ + i_11_] = (byte) (i_14_ >> 8);
	    }
	} else {
	    int i_16_ = a.getI(i, 0);
	    for (i_12_ = 0; i_12_ < i_11_; i_12_++)
		is[i_12_] = (byte) (i_16_ >> 8);
	    for (int i_17_ = 0; i_17_ < i_12_; i_17_++) {
		i_16_ = a.getI(i, i_17_);
		is[i_17_ + i_11_] = (byte) (i_16_ >> 8);
	    }
	}
	return is;
    }
    
    int a(int[] is) {
	int i = 0;
	for (int i_18_ = 0; i_18_ < is.length; i_18_++) {
	    if ((is[i_18_] & this.i) > i)
		i = is[i_18_] & this.i;
	}
	return i;
    }
    
    int a(byte[] is) {
	int i = 0;
	if (d == 8)
	    i = 255;
	else if (d == 16) {
	    boolean bool = false;
	    for (int i_19_ = 0; i_19_ < is.length / 2; i_19_++) {
		int i_20_ = x.a(is[2 * i_19_]) + (x.a(is[2 * i_19_ + 1]) << 8);
		if ((i_20_ & this.i) > i)
		    i = i_20_ & this.i;
	    }
	} else if (d == 32) {
	    boolean bool = false;
	    for (int i_21_ = 0; i_21_ < is.length / 4; i_21_++) {
		int i_22_ = (x.a(is[2 * i_21_]) + (x.a(is[2 * i_21_ + 1]) << 8)
			     + (x.a(is[2 * i_21_ + 2]) << 16)
			     + (x.a(is[2 * i_21_ + 3]) << 24));
		if ((i_22_ & this.i) > i)
		    i = i_22_ & this.i;
	    }
	}
	return i;
    }
    
    public ImageIO(DicomImage dicomimage) throws DicomException {
	a = dicomimage;
	b = a.getI(467);
	c = a.getI(466);
	d = a.getI(475);
	e = a.getI(476);
	this.i = (1 << e) - 1;
	h = a.getI(463);
	if ((j = a.getS(462)) != null)
	    j = j.trim();
	if ((f = a.getI(464)) == 2147483647)
	    f = 1;
	if ((g = a.getI(480)) == 2147483647)
	    g = 0;
	try {
	    int i = 0;
	    Object object = null;
	    String string;
	    try {
		string = a.getFileMetaInformation().getS(31);
	    } catch (Exception exception) {
		string = null;
	    }
	    if (string != null)
		i = UID.getUIDEntry(string).getConstant();
	    a(i);
	} catch (UnknownUIDException unknownuidexception) {
	    throw new DicomException("ImageIO not possible: "
				     + unknownuidexception.getMessage());
	}
    }
    
    public ImageProducer getImageProducer() throws DicomException {
	return getImageProducer(0);
    }
    
    public ImageProducer getImageProducer(int i) throws DicomException {
	if (i >= f)
	    throw new DicomException("Index exceeds number of frames");
	if (l == 8196)
	    return Toolkit.getDefaultToolkit().createImage
		       ((byte[]) a.get(1184, i + 1)).getSource();
	Object object = b(i);
	if (object == null)
	    throw new DicomException("Error ocurred building ImageProducer "
				     + i);
	if (j.trim().equals("MONOCHROME1")) {
	    if (d == 8) {
		GrayColorModel graycolormodel = new GrayColorModel(8);
		return new MemoryImageSource(b, c, graycolormodel,
					     (byte[]) object, 0, b);
	    }
	    if (g == 0)
		g = a((byte[]) a.get(1184));
	    GrayColorModel graycolormodel = new GrayColorModel(d, g);
	    return new MemoryImageSource(b, c, graycolormodel, (int[]) object,
					 0, b);
	}
	if (j.trim().equals("MONOCHROME2")) {
	    if (d == 8) {
		byte[] is = new byte[256];
		byte[] is_23_ = new byte[256];
		byte[] is_24_ = new byte[256];
		for (int i_25_ = 0; i_25_ < 256; i_25_++)
		    is[i_25_] = is_23_[i_25_] = is_24_[i_25_] = (byte) i_25_;
		IndexColorModel indexcolormodel
		    = new IndexColorModel(8, 256, is, is_23_, is_24_, -1);
		return new MemoryImageSource(b, c, indexcolormodel,
					     (byte[]) object, 0, b);
	    }
	    if (g == 0)
		g = a((byte[]) a.get(1184));
	    GrayColorModel graycolormodel = new GrayColorModel(d, g);
	    return new MemoryImageSource(b, c, graycolormodel, (int[]) object,
					 0, b);
	}
	if (j.trim().equals("PALETTE COLOR")) {
	    byte[] is = a(498, 494);
	    byte[] is_26_ = a(499, 495);
	    byte[] is_27_ = a(500, 496);
	    IndexColorModel indexcolormodel
		= new IndexColorModel(8, 256, is, is_26_, is_27_);
	    return new MemoryImageSource(b, c, indexcolormodel,
					 (byte[]) object, 0, b);
	}
	if (j.equals("RGB")) {
	    DirectColorModel directcolormodel
		= new DirectColorModel(24, 16711680, 65280, 255);
	    return new MemoryImageSource(b, c, directcolormodel,
					 (int[]) object, 0, b);
	}
	return null;
    }
    
    public Vector getImageProducers() throws DicomException {
	Vector vector = new Vector(f);
	for (int i = 0; i < f; i++)
	    vector.addElement(getImageProducer(i));
	return vector;
    }
    
    public synchronized void setImageProducer(ImageProducer imageproducer) {
	DICOMImageConsumer dicomimageconsumer = new DICOMImageConsumer(this);
	imageproducer.startProduction(dicomimageconsumer);
	if (!dicomimageconsumer.ready) {
	    try {
		this.wait();
	    } catch (InterruptedException interruptedexception) {
		interruptedexception.printStackTrace();
	    }
	}
	imageproducer.removeConsumer(dicomimageconsumer);
    }
    
    public int size() {
	return f;
    }
    
    public DicomImage getSource() {
	return a;
    }
    
    boolean a() {
	return k;
    }
}
