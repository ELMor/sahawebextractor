/* DICOMImageConsumer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.image;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.IndexColorModel;
import java.util.Hashtable;
import java.util.Vector;

import com.archimed.dicom.DicomException;

class DICOMImageConsumer implements ImageConsumer
{
    DicomImage a;
    ImageIO b;
    byte[] c;
    int[] d;
    int e;
    int f;
    ColorModel g;
    boolean h;
    public boolean ready;
    
    public DICOMImageConsumer(ImageIO imageio) {
	b = imageio;
	a = imageio.a;
	ready = false;
    }
    
    boolean a() {
	if (h) {
	    if (g instanceof IndexColorModel) {
		IndexColorModel indexcolormodel = (IndexColorModel) g;
		for (int i = 0; i < indexcolormodel.getMapSize(); i++) {
		    int i_0_ = indexcolormodel.getRed(i);
		    int i_1_ = indexcolormodel.getGreen(i);
		    int i_2_ = indexcolormodel.getBlue(i);
		    if (i_0_ != i_1_ || i_1_ != i_2_)
			return false;
		}
		return true;
	    }
	    System.out.println("??? bytes + not indexcolor");
	    return false;
	}
	if (g instanceof GrayColorModel)
	    return true;
	if (g instanceof DirectColorModel) {
	    for (int i = 0; i < e * f; i++) {
		if (g.getRed(d[i]) != g.getGreen(d[i])
		    || g.getGreen(d[i]) != g.getBlue(d[i]))
		    return false;
	    }
	    return true;
	}
	System.out.println("??? ints + not directcolor");
	return false;
    }
    
    Vector b() {
	Vector vector = new Vector(256);
	if (h) {
	    IndexColorModel indexcolormodel = (IndexColorModel) g;
	    for (int i = 0; i < indexcolormodel.getMapSize(); i++) {
		int i_3_ = ((indexcolormodel.getRed(i) << 16)
			    + (indexcolormodel.getGreen(i) << 8)
			    + indexcolormodel.getBlue(i));
		vector.addElement(new Integer(i_3_));
	    }
	    return vector;
	}
	for (int i = 0; i < e * f; i++) {
	    int i_4_ = d[i];
	    if (!vector.contains(new Integer(i_4_))) {
		vector.addElement(new Integer(i_4_));
		if (vector.size() > 256)
		    return null;
	    }
	}
	return vector;
    }
    
    boolean c() {
	IndexColorModel indexcolormodel = (IndexColorModel) g;
	for (int i = 0; i < indexcolormodel.getMapSize(); i++) {
	    if (indexcolormodel.getRed(i) != i)
		return false;
	}
	return true;
    }
    
    void d() {
	if (g instanceof IndexColorModel) {
	    if (c()) {
		try {
		    a.imagePixelData(f, e, 8, 8, 7, c);
		} catch (DicomException dicomexception) {
		    dicomexception.printStackTrace();
		}
	    } else {
		byte[] is = new byte[e * f];
		for (int i = 0; i < e * f; i++)
		    is[i] = (byte) g.getRed(c[i]);
		try {
		    a.imagePixelData(f, e, 8, 8, 7, is);
		} catch (DicomException dicomexception) {
		    dicomexception.printStackTrace();
		}
	    }
	} else if (g instanceof GrayColorModel) {
	    int i = g.getPixelSize();
	    int i_5_ = i / 8;
	    byte[] is = new byte[e * f * i_5_];
	    for (int i_6_ = 0; i_6_ < e * f; i_6_++) {
		for (int i_7_ = 0; i_7_ < i_5_; i_7_++)
		    is[i_5_ * i_6_ + i_7_]
			= (byte) ((d[i_6_] & 255 << i_7_ * 8) >> i_7_ * 8);
	    }
	    try {
		a.imagePixelData(f, e, i, i, i - 1, is);
	    } catch (DicomException dicomexception) {
		dicomexception.printStackTrace();
	    }
	} else if (g instanceof DirectColorModel) {
	    byte[] is = new byte[e * f];
	    for (int i = 0; i < e * f; i++)
		is[i] = (byte) (d[i] & 0xff);
	    try {
		a.imagePixelData(f, e, 8, 8, 7, is);
	    } catch (DicomException dicomexception) {
		dicomexception.printStackTrace();
	    }
	} else
	    System.out.println("cm: " + g);
    }
    
    void a(Vector vector) {
	int i = vector.size();
	byte[] is = new byte[256];
	byte[] is_8_ = new byte[256];
	byte[] is_9_ = new byte[256];
	for (int i_10_ = 0; i_10_ < i; i_10_++) {
	    is[i_10_] = (byte) ((((Integer) vector.elementAt(i_10_)).intValue()
				 & 0xff0000)
				>> 16);
	    is_9_[i_10_]
		= (byte) ((((Integer) vector.elementAt(i_10_)).intValue()
			   & 0xff00)
			  >> 8);
	    is_8_[i_10_]
		= (byte) (((Integer) vector.elementAt(i_10_)).intValue()
			  & 0xff);
	}
	if (g instanceof IndexColorModel) {
	    try {
		a.imagePixelData(f, e, c, is, is_9_, is_8_);
	    } catch (DicomException dicomexception) {
		dicomexception.printStackTrace();
	    }
	} else if (g instanceof DirectColorModel) {
	    Hashtable hashtable = new Hashtable(i);
	    for (int i_11_ = 0; i_11_ < i; i_11_++)
		hashtable.put(vector.elementAt(i_11_), new Integer(i_11_));
	    c = new byte[e * f];
	    for (int i_12_ = 0; i_12_ < e * f; i_12_++)
		c[i_12_]
		    = (byte) ((Integer) hashtable.get(new Integer(d[i_12_])))
				 .intValue();
	    try {
		a.imagePixelData(f, e, c, is, is_9_, is_8_);
	    } catch (DicomException dicomexception) {
		dicomexception.printStackTrace();
	    }
	}
    }
    
    void e() {
	try {
	    a.imagePixelData(f, e, 0, d);
	} catch (DicomException dicomexception) {
	    dicomexception.printStackTrace();
	}
    }
    
    public void imageComplete(int i) {
	if (i == 3) {
	    boolean bool = a();
	    if (bool) {
		d();
		ready = true;
		synchronized (b) {
		    b.notify();
		}
	    } else {
		Vector vector = b();
		if (vector != null) {
		    a(vector);
		    ready = true;
		    synchronized (b) {
			b.notify();
		    }
		} else {
		    e();
		    ready = true;
		    synchronized (b) {
			b.notify();
		    }
		}
	    }
	}
    }
    
    public void setColorModel(ColorModel colormodel) {
	g = colormodel;
    }
    
    public void setDimensions(int i, int i_13_) {
	e = i;
	f = i_13_;
    }
    
    public void setHints(int i) {
	/* empty */
    }
    
    public void setPixels(int i, int i_14_, int i_15_, int i_16_,
			  ColorModel colormodel, byte[] is, int i_17_,
			  int i_18_) {
	if (i != 0 || i_14_ != 0 || i_15_ != e || i_16_ != f) {
	    if (c == null)
		c = new byte[e * f];
	    for (int i_19_ = i_14_; i_19_ < i_14_ + i_16_; i_19_++)
		System.arraycopy(is, 0, c, i + i_19_ * e, i + i_15_);
	} else
	    c = is;
	h = true;
    }
    
    public void setPixels(int i, int i_20_, int i_21_, int i_22_,
			  ColorModel colormodel, int[] is, int i_23_,
			  int i_24_) {
	if (i != 0 || i_20_ != 0 || i_21_ != e || i_22_ != f) {
	    if (d == null)
		d = new int[e * f];
	    for (int i_25_ = i_20_; i_25_ < i_20_ + i_22_; i_25_++)
		System.arraycopy(is, 0, d, i + i_25_ * e, i + i_21_);
	} else
	    d = is;
	h = false;
    }
    
    public void setProperties(Hashtable hashtable) {
	/* empty */
    }
}
