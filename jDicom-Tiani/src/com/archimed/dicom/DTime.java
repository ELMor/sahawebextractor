/* DTime - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DTime
{
    private Calendar a;
    private boolean b = false;
    
    public DTime() {
	b = true;
    }
    
    public DTime(String string) throws NumberFormatException {
	string = string.trim();
	int i = string.length();
	if (i == 0)
	    b = true;
	else {
	    int i_1_;
	    int i_2_;
	    int i_3_;
	    int i_0_ = i_1_ = i_2_ = i_3_ = 0;
	    int i_4_ = string.indexOf(':') != -1 ? 1 : 0;
	    int i_5_ = 2 + i_4_;
	    if (i < i_5_ + 2)
		throw new NumberFormatException
			  ("Cannot parse string into DTime");
	    i_0_ = Integer.parseInt(string.substring(0, 2));
	    i_1_ = Integer.parseInt(string.substring(i_5_, i_5_ + 2));
	    i_5_ += 2 + i_4_;
	    if (i_5_ < i) {
		if (i < i_5_ + 2)
		    throw new NumberFormatException
			      ("Cannot parse string into DTime");
		i_2_ = Integer.parseInt(string.substring(i_5_, i_5_ + 2));
		i_5_ += 3;
		if (i_5_ < i) {
		    if (string.charAt(i_5_ - 1) != '.')
			throw new NumberFormatException
				  ("Cannot parse string into DTime");
		    i_3_ = (int) (Float.parseFloat(string.substring(i_5_ - 1))
				  * 1000.0F);
		}
	    }
	    a = new GregorianCalendar();
	    a.set(11, i_0_);
	    a.set(12, i_1_);
	    a.set(13, i_2_);
	    a.set(14, i_3_);
	}
    }
    
    public String toString() {
	if (b)
	    return "";
	SimpleDateFormat simpledateformat
	    = new SimpleDateFormat("HH:mm:ss.SSS");
	return simpledateformat.format(a.getTime());
    }
    
    public String toString(DateFormat dateformat) {
	if (b)
	    return "";
	return dateformat.format(a.getTime());
    }
    
    public String toDICOMString() {
	if (b)
	    return "";
	SimpleDateFormat simpledateformat = new SimpleDateFormat("HHmmss.SSS");
	return simpledateformat.format(a.getTime());
    }
    
    public boolean isEmpty() {
	return b;
    }
}
