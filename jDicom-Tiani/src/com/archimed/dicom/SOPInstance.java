/* SOPInstance - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class SOPInstance extends UID
{
    public static final int StorageCommitmentPushModel = 16385;
    public static final int StorageCommitmentPullModel = 16386;
    public static final int Printer = 16387;
    public static final int PrintQueue = 16388;
    public static final int PrinterConfigurationRetrieval = 16389;
    
    static void a() {
	UID.a.put(new Integer(16385),
		  new UIDEntry(16385, "1.2.840.10008.1.20.1.1",
			       "Storage Commitment Push Model SOP Instance",
			       "SP", 2));
	UID.a.put(new Integer(16386),
		  new UIDEntry(16386, "1.2.840.10008.1.20.2.1",
			       "Storage Commitment Pull Model SOP Instance",
			       "SP", 2));
	UID.a.put(new Integer(16387),
		  new UIDEntry(16387, "1.2.840.10008.5.1.1.17",
			       "Printer SOP Instance", "P ", 2));
	UID.a.put(new Integer(16388),
		  new UIDEntry(16388, "1.2.840.10008.5.1.1.25",
			       "Print Queue SOP Instance", "DP", 2));
	UID.a.put(new Integer(16389),
		  new UIDEntry(16389, "1.2.840.10008.5.1.1.17.376",
			       "Printer Configuration Retrieval SOP Class",
			       "CP", 1));
    }
}
