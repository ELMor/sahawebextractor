/* CompareDicom - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package test.com.tiani.dicom.codec;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;

public final class CompareDicom
{
    public static void main(String[] strings) {
	if (strings.length != 2) {
	    System.out.println("Usage: CompareDicom <File1> <File2>");
	    System.out.println("or     CompareDicom <Dir1>  <Dir2>");
	} else {
	    try {
		File file = new File(strings[0]);
		boolean bool = file.isDirectory();
		File file_0_ = new File(strings[1]);
		if (!bool)
		    compare(file, file_0_);
		else {
		    if (!file_0_.isDirectory())
			throw new IOException("Error accessing directory "
					      + file_0_);
		    String[] strings_1_ = file.list();
		    for (int i = 0; i < strings_1_.length; i++) {
			try {
			    compare(new File(file, strings_1_[i]),
				    new File(file_0_, strings_1_[i]));
			} catch (Exception exception) {
			    System.out.println(exception);
			}
		    }
		}
	    } catch (Exception exception) {
		exception.printStackTrace();
	    }
	}
    }
    
    private static void compare(File file, File file_2_)
	throws IOException, DicomException {
	DicomObject dicomobject = new DicomObject();
	DicomObject dicomobject_3_ = new DicomObject();
	TianiInputStream tianiinputstream
	    = new TianiInputStream(new FileInputStream(file));
	TianiInputStream tianiinputstream_4_
	    = new TianiInputStream(new FileInputStream(file_2_));
	try {
	    tianiinputstream.read(dicomobject, true);
	} finally {
	    tianiinputstream.close();
	}
	try {
	    tianiinputstream_4_.read(dicomobject_3_, true);
	} finally {
	    tianiinputstream_4_.close();
	}
	byte[] is = (byte[]) dicomobject.get(1184);
	byte[] is_5_ = (byte[]) dicomobject_3_.get(1184);
	int i = dicomobject.getI(475);
	int i_6_ = dicomobject.getI(476);
	int i_7_ = dicomobject.getI(477);
	int i_8_ = 0;
	int i_9_ = (1 << i_6_) - 1;
	i_9_ <<= i_7_ + 1 - i_6_;
	byte i_10_ = (byte) (i_9_ & 0xff);
	byte i_11_ = (byte) (i_9_ >>> 8 & 0xff);
	if (i == 8) {
	    for (int i_12_ = 0; i_12_ < is.length; i_12_++) {
		if ((is[i_12_] & i_10_) != (is_5_[i_12_] & i_10_)) {
		    System.out.println("*Error index:" + i_12_ + " "
				       + is[i_12_] + "-" + is_5_[i_12_]);
		    i_8_++;
		}
	    }
	} else if (i == 16) {
	    for (int i_13_ = 0; i_13_ < is.length / 2; i_13_ += 2) {
		if ((is[i_13_] & i_10_) != (is_5_[i_13_] & i_10_)
		    || (is[i_13_ + 1] & i_11_) != (is_5_[i_13_ + 1] & i_11_)) {
		    System.out.println("*Error index:" + i_13_ + " "
				       + is[i_13_] + "-" + is_5_[i_13_]);
		    i_8_++;
		}
	    }
	}
	System.out.println("Error#: " + i_8_ + " von:" + is.length);
    }
}
