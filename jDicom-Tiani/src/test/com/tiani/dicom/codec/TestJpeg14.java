/* TestJpeg14 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package test.com.tiani.dicom.codec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import com.tiani.dicom.legacy.TianiInputStream;

public class TestJpeg14
{
    private int n = 0;
    private int minSize = 2147483647;
    private int maxSize = 0;
    private long sumSize = 0L;
    private float minRate = 3.4028235E38F;
    private float maxRate = 0.0F;
    private float sumRate = 0.0F;
    private long minDt = 9223372036854775807L;
    private long maxDt = 0L;
    private long sumDt = 0L;
    
    private void addLog(int i, float f, long l) {
	n++;
	minSize = Math.min(i, minSize);
	maxSize = Math.max(i, maxSize);
	sumSize += (long) i;
	minRate = Math.min(f, minRate);
	maxRate = Math.max(f, maxRate);
	sumRate += f;
	minDt = Math.min(l, minDt);
	maxDt = Math.max(l, maxDt);
	sumDt += l;
    }
    
    private void writeSummary() {
	System.out.println("N=" + n + ", size ~" + (sumSize / (long) n >> 10)
			   + "kB [" + (minSize >> 10) + "-" + (maxSize >> 10)
			   + "]");
	System.out.println("rate ~" + sumRate / (float) n + " [" + minRate
			   + "-" + maxRate + "]");
	System.out.println("time ~" + sumDt / (long) n + "ms [" + minDt + "-"
			   + maxDt + "]");
    }
    
    public static void main(String[] strings) {
	if (strings.length != 2) {
	    System.out.println("Usage: TestJpeg14 <inFile> <outFile>");
	    System.out.println("or     TestJpeg14 <inDir> <outDir>");
	} else {
	    TestJpeg14 testjpeg14 = new TestJpeg14();
	    try {
		File file = new File(strings[0]);
		File file_0_ = new File(strings[1]);
		if (!file.isDirectory())
		    testjpeg14.transform(file, file_0_);
		else {
		    if (!file_0_.isDirectory())
			throw new IOException("Error accesing directory "
					      + file_0_);
		    String[] strings_1_ = file.list();
		    for (int i = 0; i < strings_1_.length; i++) {
			try {
			    testjpeg14.transform(new File(file, strings_1_[i]),
						 new File(file_0_,
							  strings_1_[i]));
			} catch (Exception exception) {
			    exception.printStackTrace(System.out);
			}
		    }
		    testjpeg14.writeSummary();
		}
	    } catch (Exception exception) {
		exception.printStackTrace(System.out);
	    }
	}
    }
    
    private void transform(File file, File file_2_)
	throws IOException, DicomException, UnknownUIDException {
	DicomObject dicomobject = new DicomObject();
	TianiInputStream tianiinputstream
	    = new TianiInputStream(new FileInputStream(file));
	try {
	    tianiinputstream.read(dicomobject, true);
	} finally {
	    tianiinputstream.close();
	}
	DicomObject dicomobject_3_ = dicomobject.getFileMetaInformation();
	Compression compression = new Compression(dicomobject);
	int i = (dicomobject_3_ != null
		 ? UID.getUIDEntry(dicomobject_3_.getS(31)).getConstant()
		 : 8193);
	int i_4_ = getPixelSize(dicomobject);
	long l = System.currentTimeMillis();
	int i_5_;
	float f;
	long l_6_;
	switch (i) {
	case 8193:
	case 8194:
	case 8195:
	    compression.compress(8197);
	    try {
		dicomobject_3_.set(31, UID.getUIDEntry(8197).getValue());
	    } catch (IllegalValueException illegalvalueexception) {
		/* empty */
	    }
	    i_5_ = i_4_;
	    f = (float) i_4_ / (float) getPixelSize(dicomobject);
	    l_6_ = System.currentTimeMillis() - l;
	    System.out.println("compress " + file + " [" + f + " : 1] takes "
			       + l_6_ + "ms");
	    break;
	default:
	    compression.decompress();
	    i_5_ = getPixelSize(dicomobject);
	    f = (float) i_5_ / (float) i_4_;
	    l_6_ = System.currentTimeMillis() - l;
	    System.out.println("decompress " + file + " [1 : " + f + "] takes "
			       + l_6_ + "ms");
	}
	addLog(i_5_, f, l_6_);
	FileOutputStream fileoutputstream = new FileOutputStream(file_2_);
	try {
	    dicomobject.write(fileoutputstream, true);
	} finally {
	    fileoutputstream.close();
	}
    }
    
    private static int getPixelSize(DicomObject dicomobject) {
	int i = 0;
	int i_7_ = dicomobject.getSize(1184);
	while (i_7_-- > 0)
	    i += ((byte[]) dicomobject.get(1184, i_7_)).length;
	return i > 0 ? i : 1;
    }
}
