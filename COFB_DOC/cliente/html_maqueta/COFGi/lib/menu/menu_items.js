/*	(c) Ger Versluis 2000 version 5 October 9th 2001	*
*	You may remove all comments for faster loading	*/		

	var NoOffFirstLineMenus=3;			// Number of first level items
	var LowBgColor='666666';			// Background color when mouse is not over
	var LowSubBgColor='666666';			// Background color when mouse is not over on subs
	var HighBgColor='666666';			// Background color when mouse is over
	var HighSubBgColor='666666';			// Background color when mouse is over on subs
	var FontLowColor='979797';			// Font color when mouse is not over
	var FontSubLowColor='979797';			// Font color subs when mouse is not over
	var FontHighColor='FFFFFF';			// Font color when mouse is over
	var FontSubHighColor='FFFFFF';			// Font color subs when mouse is over
	var BorderColor='666666';			// Border color
	var BorderSubColor='666666';			// Border color for subs
	var BorderWidth=1;				// Border width
	var BorderBtwnElmnts=1;			// Border between elements 1 or 0
	var FontFamily="arial,comic sans ms,technical"	// Font family menu items
	var FontSize=9;				// Font size menu items
	var FontBold=1;				// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered='left';			// Item text position 'left', 'center' or 'right'
	var MenuCentered='left';			// Menu horizontal position 'left', 'center' or 'right'
	var MenuVerticalCentered='top';		// Menu vertical position 'top', 'middle' or 'bottom'
	var ChildOverlap=0;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=0;			// vertical overlap child/ parent
	var StartTop=69;				// Menu offset x coordinate
	var StartLeft=10;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=0;				// Multiple frames x correction
	var LeftPaddng=3;				// Left padding
	var TopPaddng=2;				// Top padding
	var FirstLineHorizontal=1;			// First level items layout horizontal 1 or 0
	var MenuFramesVertical=1;			// Frames in cols or rows 1 or 0
	var DissapearDelay=1000;			// delay before menu folds in
	var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
	var FirstLineFrame='navig';			// Frame where first level appears
	var SecLineFrame='space';			// Frame where sub levels appear
	var DocTargetFrame='space';			// Frame where target documents appear
	var TargetLoc='';				// DIV id for relative positioning (refer to config.htm for info)
	var HideTop=0;				// Hide first level when loading new document 1 or 0
	var MenuWrap=1;				// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var WebMasterCheck=0;			// menu tree checking on or off 1 or 0

function BeforeStart(){return}
function AfterBuild(){return}
function BeforeFirstOpen(){return}
function AfterCloseAll(){return}

// Menu tree
//	MenuX=new Array(Text to show, Link, background image (optional), number of sub elements, height, width);
//	For rollover images set "Text to show" to:  "rollover:Image1.jpg:Image2.jpg"

Menu1=new Array("Principal","./base.htm","",0,20,70);

Menu2=new Array("Expedients","#","",2,20,85);
	Menu2_1=new Array("Expedients de Farm�cia.","./base01.htm","",0,15,154);
	Menu2_2=new Array("Expedients de col�legiats","#","",0,15,154);
	
Menu3=new Array("Manteniments","#","",2,20,100);
	Menu3_1=new Array("Manteniment01","#","",0,15,154);
	Menu3_2=new Array("Manteniment02","#","",0,15,154);
	

