/*
   DynAPI Distribution menu ampliation by Toni Felguera (afelguera@ictnet.es)
   Menu Class

   The DynAPI Distribution is distributed under the terms of the GNU LGPL license.

   Requirements:
	  dynapi.api [dynlayer, dyndocument, browser, events]
	  dynapi.gui [label]
*/

//Menu item constructor
function MenuItem(text, value){
	this.Label = Label;                               //Label object
	this.Label(text);                                 //Sets the text of the label
	this.value = value;                               //Sets the value of the label
	this.setSelectable(false);                        //The label is not selectable
	this.deselectable = true;                         //It can be deselected 
	this.isMouseOver = false;                         //The mouse is not over it
	this.selected = false;                            //It's not selected
	this.itemStyle = {};                              //The style of the item
	this.itemStyle.bos=false;                         //Not bold on select
	this.itemStyle.ios=false;                         //Not italic on select
	this.addEventListener(MenuItem.menuevents);       //Add the event listener to the menuitem layer
}

//The base prototype is a label
MenuItem.prototype = new Label();

//Listener of the item events
MenuItem.menuevents = new EventListener();

//Oncreate event handler
MenuItem.menuevents.oncreate = function(e) {
	var o = e.getSource();
	if (o.menu.created) o.menu.arrangeItems();
};

//Onmousedown event handler
MenuItem.menuevents.onmousedown = function (e) {
	var o = e.getSource();
	o.setSelected(!o.selected);
};

//Onmouseover event handler
MenuItem.menuevents.onmouseover = function (e) {
	var o = e.getSource();
	if (!o.selected && !o.isMouseOver) {
		o.setBgColor(o.itemStyle.bgColorRoll);
		o.setFontColor(o.itemStyle.textRoll);
		for (var i=0;i<o.menu.items.length;i++) {
			if (o.menu.items[i]!=o && !o.menu.items[i].selected && o.menu.items[i].isMouseOver) {
				o.menu.items[i].setBgColor(o.menu.items[i].itemStyle.bgcolor);
				o.menu.items[i].setFontColor(o.menu.items[i].itemStyle.textNormal);
				o.menu.items[i].isMouseOver = false;
			}
		}
	}
	o.isMouseOver = true;
};

//onmouseout event handler
MenuItem.menuevents.onmouseout = function (e) {
	var o = e.getSource();
	if (!o.selected && o.isMouseOver) {
		o.setBgColor(o.itemStyle.bgcolor);
		o.setFontColor(o.itemStyle.textNormal);
	}
	o.isMouseOver = false;
};

//Sets the format of a menu item
MenuItem.prototype.setColors = function(bg, bgr, bgs, tn, tr, ts) {
  var s = this.itemStyle;
	s.bgcolor = bg||'#eeeeee';
	s.bgColorRoll = bgr||'#cccccc';
	s.bgColorSelect = bgs||'lightblue';
	s.textNormal = tn||'#000000';
	s.textRoll = tr||'#000000';
	s.textSelect = ts||'#000000';
	this.setBgColor(s.bgcolor);
};

//Sets if bold on select the item
MenuItem.prototype.boldOnSelect = function(b) {
	this.itemStyle.bos=b;
};

//Sets if italic on select the item
MenuItem.prototype.italicOnSelect = function(b) {
	this.itemStyle.ios=b;
};

//Sets this item as a selected item
MenuItem.prototype.setSelected = function(b) {
	if (this.selected=b) {
		if (this.itemStyle.bos) this.setFontBold(true,false);
		if (this.itemStyle.ios) this.setFontItalic(true,false);
		this.setBgColor(this.itemStyle.bgColorSelect);
		this.setFontColor(this.itemStyle.textSelect);
		this.invokeEvent("select");
	}
	else if (this.deselectable) {
		if (this.itemStyle.bos) this.setFontBold(false,false);
		if (this.itemStyle.ios) this.setFontItalic(false,false);
		this.setBgColor(this.isMouseOver?this.itemStyle.bgColorRoll:this.itemStyle.bgcolor)
		this.setFontColor(this.itemStyle.textNormal);
		this.invokeEvent("deselect");
	}
}

//Sets the value of the item
MenuItem.prototype.setValue = function(value) {
	this.value=value;
};

//Gets the value of the item
MenuItem.prototype.getValue = function() {
	return this.value;
};

//Menu constructor
function Menu(){
	this.DynLayer = DynLayer;
	this.DynLayer();
	this.items = [];
	this.addEventListener(Menu.listener);
	this.ievents = new EventListener(this);
	this.ievents.onselect = function(e){
		e.getTarget().select(e.getSource());
	};
	this.ievents.ondeselect = function(e){
  		e.getTarget().deselect(e.getSource());
	};
	
	/*default style*/
	this.menuStyle = {};
	this.menuStyle.borders = 1;
	this.menuStyle.spacing = 1;
	this.menuStyle.padding = 4;
	this.menuStyle.bos = false;
	this.menuStyle.ios = false;
	this.menuStyle.bg = "#eeeeee";
	this.menuStyle.bgRoll = "#cccccc";
	this.menuStyle.bgSelect = "lightblue";
	this.menuStyle.textNormal = this.menuStyle.textRoll = this.menuStyle.textSelect = "#000000";
	this.totalWidth = this.menuStyle.borders;
}

//The base prototype of a menu is a layer
Menu.prototype = new DynLayer();

//Add an item to the menu
Menu.prototype.add = function(text, value){
	var i = new MenuItem(text, value);
	i.menu = this;
	var ls = this.menuStyle;
	i.setColors(ls.bg, ls.bgRoll, ls.bgSelect, ls.textNormal, ls.textRoll, ls.textSelect);
	if (ls.bos) i.boldOnSelect(true, false);
	if (ls.ios) i.italicOnSelect(true, false);
	i.setPadding(ls.padding);
	i.addEventListener(this.ievents);
	this.items[this.items.length] = i;
	this.addChild(i);
};

//
Menu.prototype.arrangeItems = function(){
	this.totalWidth = this.menuStyle.borders;
	for (var i=0;i<this.items.length;i++){
		this.items[i].moveTo(this.menuStyle.borders, this.totalWidth);
		this.items[i].setWidth((this.w/this.items.length)-this.menuStyle.borders*2);
		this.totalWidth = this.totalWidth+this.items[i].w+this.menuStyle.spacing;	
	}
	this.setWidth(this.totalWidth-this.menuStyle.spacing+this.menuStyle.borders);
};

//Remove an item from the menu
Menu.prototype.remove = function(item){
	var i = this.getIndexOf(item);
	if (i==-1) return;
	this.items[i].deleteFromParent();
	DynAPI.removeFromArray(this.items,item);
	if (this.selectedIndex==i){
		this.selectedIndex=-1;
		this.selectedItem=null;
	}
	this.arrangeItems();
};

//Sinonym to the DynLayer setwidth
Menu.prototype.origSetWidth = DynLayer.prototype.setWidth;

//Sets the width of the menu
Menu.prototype.setWidth = function(w){
	this.origSetWidth(w);
	for (var i=0;i<this.items.length;i++){
		this.items[i].setWidth(w-this.menuStyle.borders*2);
	}
};

//Defines the listener of the menu
Menu.listener = new EventListener()

//When the menu is created, the items are arranged
Menu.listener.oncreate = function(e){
  var o = e.getSource();
	o.arrangeItems();
};

//Returns the selected index
Menu.prototype.getIndexOf = function(item){
	for (var i=0;i<this.items.length;i++){
		if (this.items[i]==item) return i;
	}
	return -1;
};

//Selects an item from the menu
Menu.prototype.select = function(item){
	this.selectedIndex = this.getIndexOf(item);
	this.selectedItem = item;
	for (var i=0;i<this.items.length;i++){
		if (this.items[i] != item) this.items[i].setSelected(false);
	}
	this.invokeEvent("select");
};

//Deselects an item from the menu
Menu.prototype.deselect = function(item){
	if (this.selectedItem == item){
		this.selectedItem = null;
		this.selectedIndex = -1;
	}
};

//Deselects all the items
Menu.prototype.deselectAll = function(){
	for (var i=0;i<this.items.length;i++) {
		if (this.items[i].selected) this.items[i].setSelected(false);
	}
};

//Sets the format of the item
Menu.prototype.setColors = function(bg, bgRoll, bgSelect, textNormal, textRoll, textSelect){
  var ls = this.menuStyle;
	ls.bg = bg||ls.bg;
	ls.bgRoll = bgRoll||ls.bgRoll;
	ls.bgSelect = bgSelect||ls.bgSelect;
	ls.textNormal = textNormal||ls.textNormal;
	ls.textRoll = textRoll||ls.textRoll;
	ls.textSelect = textSelect||ls.textSelect;
	if (this.items.length == 0) return;
	for (var i=0;i<this.items.length;i++) {
		this.items[i].setColors(bg,bgRoll,bgSelect,textNormal,textRoll,textSelect);
	}
};

//Sets if bold when we select an item
Menu.prototype.boldOnSelect = function(b) {
	this.menuStyle.bos = b;
};

//Sets if italic when we select an item
Menu.prototype.italicOnSelect = function(b) {
	this.menuStyle.ios = b;
};

//Gets the selected index on the menu
Menu.prototype.getSelectedIndex = function() {
    return this.selectedIndex;
};

//Gets the selected item on the menu
Menu.prototype.getSelectedItem = function() {
	return this.selectedItem;
};

//Sets the width of the borders
Menu.prototype.setBorders = function(b){
	this.menuStyle.borders = b;
	if (this.created) this.arrangeItems();
};

//Sets the width of the spacing
Menu.prototype.setSpacing = function(b){
	this.menuStyle.spacing = b;
	if (this.created) this.arrangeItems();
};