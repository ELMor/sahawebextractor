grammar SymbMath;

@header {
package org.symbmath.grammar;
import org.symbmath.grammar.Nodo;
}

console 
	: (objeto)+
	;

objeto 
	: functionDefinition
	| expresion
	| noEvalExpresion
	| vVector                      
	| matriz                                                
	| lList                        
	| labeledObject 
	| sString				                                      
	| magnitude
	| assign
	;

functionDefinition
	: identifier parListIds '=' noEvalExpresion
	;	
	
assign
	: identifier '=' expresion
	;
	
noEvalExpresion
	: '\'' expresion '\''
	;

expresion
	:  multiplication (('+' | '-') multiplication)*
	;

multiplication
	:  power (('*' | '/') power)*
	;

power
	: unary ('^' unary)*
	;
	
unary
	: '+' atom
	| '-' atom
	| atom
	;

atom 
	: nNumber
	| functionCall
	| identifier
	| '(' expresion ')'
	| derivative 
	| integral 
	| sumatoria 
	;

nNumber : 
	  NUMBER 
	| '(' NUMBER ';' NUMBER ')'
	;
	
derivative 
	: '(>' identifier expresion ')'
	;
	
integral
	: '(<' identifier expresion expresion expresion ')'
	;

sumatoria
	: '(+' identifier expresion expresion expresion ')'
	;

functionCall
	: identifier functionCallParams
	;
	
functionCallParams
	: '(' ( expresion ( ',' expresion )* )? ')'
	;	

vVector :
	'[' (expresion)+ ']'
	;
	
matriz :
	'[' (vVector)+ ']'
	;
	
lList:
	'{' seqOb '}'				
	;
		
labeledObject
	: ':' identifier ':' objeto
	;

magnitude 
	: NUMBER '_' uExpr
	;
	
uExpr 
	: uExprPower
	| uExpr ('*' | '/') uExpr
	;
	
uExprPower
	: ID ('^' ('-')? NUMBER)?
	;
	
seqOb
	:  ( objeto )*		
	;

sString : STRING_LITERAL ;

parListIds
	: '(' ( identifier ( ',' identifier )* )? ')'
	;

identifier : ID ;

WS	:	[ \t\r\n] -> skip 
	;

fragment
ESC	:	'\\'
		(	'n'
		|	'r'
		|	't'
		|	'b'
		|	'f'
		|	'"'
		|	'\''
		|	'\\'
		|	('0'..'3')
			( ('0'..'9') ( '0'..'9' )? )?
		|	('4'..'7')
			( ('0'..'9') )?
		)
	;

// string literals
STRING_LITERAL
	:	'"' (ESC|~('"'|'\\'))* '"'
	;

ID
  :  ('a'..'z' | 'A'..'Z') ('a'..'z' | 'A'..'Z' | DIGIT)*
  ;

NUMBER 
  :  DIGIT+ '.' DIGIT* EXPONENT?
  |  '.'? DIGIT+ EXPONENT?
  ;

fragment EXPONENT :  ('e' | 'E') ('+' | '-') ? DIGIT+ ;
fragment DIGIT :  '0'..'9' ;
	
CARET : '^' ;
MULT  : '*' ;
DIV   : '/' ;