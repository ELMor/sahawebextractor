// Generated from SymbMath.g4 by ANTLR 4.5.2

package org.symbmath.grammar;
import org.symbmath.grammar.Nodo;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SymbMathLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		WS=18, STRING_LITERAL=19, ID=20, NUMBER=21, CARET=22, MULT=23, DIV=24;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"WS", "ESC", "STRING_LITERAL", "ID", "NUMBER", "EXPONENT", "DIGIT", "CARET", 
		"MULT", "DIV"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'='", "'''", "'+'", "'-'", "'('", "')'", "';'", "'(>'", "'(<'", 
		"'(+'", "','", "'['", "']'", "'{'", "'}'", "':'", "'_'", null, null, null, 
		null, "'^'", "'*'", "'/'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "WS", "STRING_LITERAL", "ID", "NUMBER", 
		"CARET", "MULT", "DIV"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public SymbMathLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "SymbMath.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\32\u00b0\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3"+
		"\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f"+
		"\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3"+
		"\23\3\23\3\24\3\24\3\24\3\24\3\24\5\24h\n\24\5\24j\n\24\3\24\3\24\5\24"+
		"n\n\24\5\24p\n\24\3\25\3\25\3\25\7\25u\n\25\f\25\16\25x\13\25\3\25\3\25"+
		"\3\26\3\26\3\26\7\26\177\n\26\f\26\16\26\u0082\13\26\3\27\6\27\u0085\n"+
		"\27\r\27\16\27\u0086\3\27\3\27\7\27\u008b\n\27\f\27\16\27\u008e\13\27"+
		"\3\27\5\27\u0091\n\27\3\27\5\27\u0094\n\27\3\27\6\27\u0097\n\27\r\27\16"+
		"\27\u0098\3\27\5\27\u009c\n\27\5\27\u009e\n\27\3\30\3\30\5\30\u00a2\n"+
		"\30\3\30\6\30\u00a5\n\30\r\30\16\30\u00a6\3\31\3\31\3\32\3\32\3\33\3\33"+
		"\3\34\3\34\2\2\35\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31"+
		"\16\33\17\35\20\37\21!\22#\23%\24\'\2)\25+\26-\27/\2\61\2\63\30\65\31"+
		"\67\32\3\2\b\5\2\13\f\17\17\"\"\n\2$$))^^ddhhppttvv\4\2$$^^\4\2C\\c|\4"+
		"\2GGgg\4\2--//\u00be\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2"+
		"\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3"+
		"\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2"+
		"\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2"+
		"\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\39\3\2\2\2\5;\3\2\2\2\7=\3\2\2\2"+
		"\t?\3\2\2\2\13A\3\2\2\2\rC\3\2\2\2\17E\3\2\2\2\21G\3\2\2\2\23J\3\2\2\2"+
		"\25M\3\2\2\2\27P\3\2\2\2\31R\3\2\2\2\33T\3\2\2\2\35V\3\2\2\2\37X\3\2\2"+
		"\2!Z\3\2\2\2#\\\3\2\2\2%^\3\2\2\2\'b\3\2\2\2)q\3\2\2\2+{\3\2\2\2-\u009d"+
		"\3\2\2\2/\u009f\3\2\2\2\61\u00a8\3\2\2\2\63\u00aa\3\2\2\2\65\u00ac\3\2"+
		"\2\2\67\u00ae\3\2\2\29:\7?\2\2:\4\3\2\2\2;<\7)\2\2<\6\3\2\2\2=>\7-\2\2"+
		">\b\3\2\2\2?@\7/\2\2@\n\3\2\2\2AB\7*\2\2B\f\3\2\2\2CD\7+\2\2D\16\3\2\2"+
		"\2EF\7=\2\2F\20\3\2\2\2GH\7*\2\2HI\7@\2\2I\22\3\2\2\2JK\7*\2\2KL\7>\2"+
		"\2L\24\3\2\2\2MN\7*\2\2NO\7-\2\2O\26\3\2\2\2PQ\7.\2\2Q\30\3\2\2\2RS\7"+
		"]\2\2S\32\3\2\2\2TU\7_\2\2U\34\3\2\2\2VW\7}\2\2W\36\3\2\2\2XY\7\177\2"+
		"\2Y \3\2\2\2Z[\7<\2\2[\"\3\2\2\2\\]\7a\2\2]$\3\2\2\2^_\t\2\2\2_`\3\2\2"+
		"\2`a\b\23\2\2a&\3\2\2\2bo\7^\2\2cp\t\3\2\2di\4\62\65\2eg\4\62;\2fh\4\62"+
		";\2gf\3\2\2\2gh\3\2\2\2hj\3\2\2\2ie\3\2\2\2ij\3\2\2\2jp\3\2\2\2km\4\66"+
		"9\2ln\4\62;\2ml\3\2\2\2mn\3\2\2\2np\3\2\2\2oc\3\2\2\2od\3\2\2\2ok\3\2"+
		"\2\2p(\3\2\2\2qv\7$\2\2ru\5\'\24\2su\n\4\2\2tr\3\2\2\2ts\3\2\2\2ux\3\2"+
		"\2\2vt\3\2\2\2vw\3\2\2\2wy\3\2\2\2xv\3\2\2\2yz\7$\2\2z*\3\2\2\2{\u0080"+
		"\t\5\2\2|\177\t\5\2\2}\177\5\61\31\2~|\3\2\2\2~}\3\2\2\2\177\u0082\3\2"+
		"\2\2\u0080~\3\2\2\2\u0080\u0081\3\2\2\2\u0081,\3\2\2\2\u0082\u0080\3\2"+
		"\2\2\u0083\u0085\5\61\31\2\u0084\u0083\3\2\2\2\u0085\u0086\3\2\2\2\u0086"+
		"\u0084\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u0088\3\2\2\2\u0088\u008c\7\60"+
		"\2\2\u0089\u008b\5\61\31\2\u008a\u0089\3\2\2\2\u008b\u008e\3\2\2\2\u008c"+
		"\u008a\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u0090\3\2\2\2\u008e\u008c\3\2"+
		"\2\2\u008f\u0091\5/\30\2\u0090\u008f\3\2\2\2\u0090\u0091\3\2\2\2\u0091"+
		"\u009e\3\2\2\2\u0092\u0094\7\60\2\2\u0093\u0092\3\2\2\2\u0093\u0094\3"+
		"\2\2\2\u0094\u0096\3\2\2\2\u0095\u0097\5\61\31\2\u0096\u0095\3\2\2\2\u0097"+
		"\u0098\3\2\2\2\u0098\u0096\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u009b\3\2"+
		"\2\2\u009a\u009c\5/\30\2\u009b\u009a\3\2\2\2\u009b\u009c\3\2\2\2\u009c"+
		"\u009e\3\2\2\2\u009d\u0084\3\2\2\2\u009d\u0093\3\2\2\2\u009e.\3\2\2\2"+
		"\u009f\u00a1\t\6\2\2\u00a0\u00a2\t\7\2\2\u00a1\u00a0\3\2\2\2\u00a1\u00a2"+
		"\3\2\2\2\u00a2\u00a4\3\2\2\2\u00a3\u00a5\5\61\31\2\u00a4\u00a3\3\2\2\2"+
		"\u00a5\u00a6\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\60"+
		"\3\2\2\2\u00a8\u00a9\4\62;\2\u00a9\62\3\2\2\2\u00aa\u00ab\7`\2\2\u00ab"+
		"\64\3\2\2\2\u00ac\u00ad\7,\2\2\u00ad\66\3\2\2\2\u00ae\u00af\7\61\2\2\u00af"+
		"8\3\2\2\2\24\2gimotv~\u0080\u0086\u008c\u0090\u0093\u0098\u009b\u009d"+
		"\u00a1\u00a6\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}