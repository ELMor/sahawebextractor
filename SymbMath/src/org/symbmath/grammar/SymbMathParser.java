// Generated from SymbMath.g4 by ANTLR 4.5.2

package org.symbmath.grammar;
import org.symbmath.grammar.Nodo;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SymbMathParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		WS=18, STRING_LITERAL=19, ID=20, NUMBER=21, CARET=22, MULT=23, DIV=24;
	public static final int
		RULE_console = 0, RULE_objeto = 1, RULE_functionDefinition = 2, RULE_assign = 3, 
		RULE_noEvalExpresion = 4, RULE_expresion = 5, RULE_multiplication = 6, 
		RULE_power = 7, RULE_unary = 8, RULE_atom = 9, RULE_nNumber = 10, RULE_derivative = 11, 
		RULE_integral = 12, RULE_sumatoria = 13, RULE_functionCall = 14, RULE_functionCallParams = 15, 
		RULE_vVector = 16, RULE_matriz = 17, RULE_lList = 18, RULE_labeledObject = 19, 
		RULE_magnitude = 20, RULE_uExpr = 21, RULE_uExprPower = 22, RULE_seqOb = 23, 
		RULE_sString = 24, RULE_parListIds = 25, RULE_identifier = 26;
	public static final String[] ruleNames = {
		"console", "objeto", "functionDefinition", "assign", "noEvalExpresion", 
		"expresion", "multiplication", "power", "unary", "atom", "nNumber", "derivative", 
		"integral", "sumatoria", "functionCall", "functionCallParams", "vVector", 
		"matriz", "lList", "labeledObject", "magnitude", "uExpr", "uExprPower", 
		"seqOb", "sString", "parListIds", "identifier"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'='", "'''", "'+'", "'-'", "'('", "')'", "';'", "'(>'", "'(<'", 
		"'(+'", "','", "'['", "']'", "'{'", "'}'", "':'", "'_'", null, null, null, 
		null, "'^'", "'*'", "'/'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "WS", "STRING_LITERAL", "ID", "NUMBER", 
		"CARET", "MULT", "DIV"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SymbMath.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SymbMathParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ConsoleContext extends ParserRuleContext {
		public List<ObjetoContext> objeto() {
			return getRuleContexts(ObjetoContext.class);
		}
		public ObjetoContext objeto(int i) {
			return getRuleContext(ObjetoContext.class,i);
		}
		public ConsoleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_console; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterConsole(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitConsole(this);
		}
	}

	public final ConsoleContext console() throws RecognitionException {
		ConsoleContext _localctx = new ConsoleContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_console);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(54);
				objeto();
				}
				}
				setState(57); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__11) | (1L << T__13) | (1L << T__15) | (1L << STRING_LITERAL) | (1L << ID) | (1L << NUMBER))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjetoContext extends ParserRuleContext {
		public FunctionDefinitionContext functionDefinition() {
			return getRuleContext(FunctionDefinitionContext.class,0);
		}
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public NoEvalExpresionContext noEvalExpresion() {
			return getRuleContext(NoEvalExpresionContext.class,0);
		}
		public VVectorContext vVector() {
			return getRuleContext(VVectorContext.class,0);
		}
		public MatrizContext matriz() {
			return getRuleContext(MatrizContext.class,0);
		}
		public LListContext lList() {
			return getRuleContext(LListContext.class,0);
		}
		public LabeledObjectContext labeledObject() {
			return getRuleContext(LabeledObjectContext.class,0);
		}
		public SStringContext sString() {
			return getRuleContext(SStringContext.class,0);
		}
		public MagnitudeContext magnitude() {
			return getRuleContext(MagnitudeContext.class,0);
		}
		public AssignContext assign() {
			return getRuleContext(AssignContext.class,0);
		}
		public ObjetoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objeto; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterObjeto(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitObjeto(this);
		}
	}

	public final ObjetoContext objeto() throws RecognitionException {
		ObjetoContext _localctx = new ObjetoContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_objeto);
		try {
			setState(69);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(59);
				functionDefinition();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(60);
				expresion();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(61);
				noEvalExpresion();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(62);
				vVector();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(63);
				matriz();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(64);
				lList();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(65);
				labeledObject();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(66);
				sString();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(67);
				magnitude();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(68);
				assign();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinitionContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ParListIdsContext parListIds() {
			return getRuleContext(ParListIdsContext.class,0);
		}
		public NoEvalExpresionContext noEvalExpresion() {
			return getRuleContext(NoEvalExpresionContext.class,0);
		}
		public FunctionDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterFunctionDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitFunctionDefinition(this);
		}
	}

	public final FunctionDefinitionContext functionDefinition() throws RecognitionException {
		FunctionDefinitionContext _localctx = new FunctionDefinitionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_functionDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			identifier();
			setState(72);
			parListIds();
			setState(73);
			match(T__0);
			setState(74);
			noEvalExpresion();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public AssignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitAssign(this);
		}
	}

	public final AssignContext assign() throws RecognitionException {
		AssignContext _localctx = new AssignContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_assign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			identifier();
			setState(77);
			match(T__0);
			setState(78);
			expresion();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NoEvalExpresionContext extends ParserRuleContext {
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public NoEvalExpresionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_noEvalExpresion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterNoEvalExpresion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitNoEvalExpresion(this);
		}
	}

	public final NoEvalExpresionContext noEvalExpresion() throws RecognitionException {
		NoEvalExpresionContext _localctx = new NoEvalExpresionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_noEvalExpresion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			match(T__1);
			setState(81);
			expresion();
			setState(82);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionContext extends ParserRuleContext {
		public List<MultiplicationContext> multiplication() {
			return getRuleContexts(MultiplicationContext.class);
		}
		public MultiplicationContext multiplication(int i) {
			return getRuleContext(MultiplicationContext.class,i);
		}
		public ExpresionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterExpresion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitExpresion(this);
		}
	}

	public final ExpresionContext expresion() throws RecognitionException {
		ExpresionContext _localctx = new ExpresionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_expresion);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(84);
			multiplication();
			setState(89);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(85);
					_la = _input.LA(1);
					if ( !(_la==T__2 || _la==T__3) ) {
					_errHandler.recoverInline(this);
					} else {
						consume();
					}
					setState(86);
					multiplication();
					}
					} 
				}
				setState(91);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplicationContext extends ParserRuleContext {
		public List<PowerContext> power() {
			return getRuleContexts(PowerContext.class);
		}
		public PowerContext power(int i) {
			return getRuleContext(PowerContext.class,i);
		}
		public MultiplicationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplication; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterMultiplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitMultiplication(this);
		}
	}

	public final MultiplicationContext multiplication() throws RecognitionException {
		MultiplicationContext _localctx = new MultiplicationContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_multiplication);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			power();
			setState(97);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==MULT || _la==DIV) {
				{
				{
				setState(93);
				_la = _input.LA(1);
				if ( !(_la==MULT || _la==DIV) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(94);
				power();
				}
				}
				setState(99);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PowerContext extends ParserRuleContext {
		public List<UnaryContext> unary() {
			return getRuleContexts(UnaryContext.class);
		}
		public UnaryContext unary(int i) {
			return getRuleContext(UnaryContext.class,i);
		}
		public PowerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_power; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterPower(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitPower(this);
		}
	}

	public final PowerContext power() throws RecognitionException {
		PowerContext _localctx = new PowerContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_power);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			unary();
			setState(105);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==CARET) {
				{
				{
				setState(101);
				match(CARET);
				setState(102);
				unary();
				}
				}
				setState(107);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryContext extends ParserRuleContext {
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public UnaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterUnary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitUnary(this);
		}
	}

	public final UnaryContext unary() throws RecognitionException {
		UnaryContext _localctx = new UnaryContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_unary);
		try {
			setState(113);
			switch (_input.LA(1)) {
			case T__2:
				enterOuterAlt(_localctx, 1);
				{
				setState(108);
				match(T__2);
				setState(109);
				atom();
				}
				break;
			case T__3:
				enterOuterAlt(_localctx, 2);
				{
				setState(110);
				match(T__3);
				setState(111);
				atom();
				}
				break;
			case T__4:
			case T__7:
			case T__8:
			case T__9:
			case ID:
			case NUMBER:
				enterOuterAlt(_localctx, 3);
				{
				setState(112);
				atom();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public NNumberContext nNumber() {
			return getRuleContext(NNumberContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public DerivativeContext derivative() {
			return getRuleContext(DerivativeContext.class,0);
		}
		public IntegralContext integral() {
			return getRuleContext(IntegralContext.class,0);
		}
		public SumatoriaContext sumatoria() {
			return getRuleContext(SumatoriaContext.class,0);
		}
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitAtom(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_atom);
		try {
			setState(125);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(115);
				nNumber();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(116);
				functionCall();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(117);
				identifier();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(118);
				match(T__4);
				setState(119);
				expresion();
				setState(120);
				match(T__5);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(122);
				derivative();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(123);
				integral();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(124);
				sumatoria();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NNumberContext extends ParserRuleContext {
		public List<TerminalNode> NUMBER() { return getTokens(SymbMathParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(SymbMathParser.NUMBER, i);
		}
		public NNumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nNumber; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterNNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitNNumber(this);
		}
	}

	public final NNumberContext nNumber() throws RecognitionException {
		NNumberContext _localctx = new NNumberContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_nNumber);
		try {
			setState(133);
			switch (_input.LA(1)) {
			case NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(127);
				match(NUMBER);
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 2);
				{
				setState(128);
				match(T__4);
				setState(129);
				match(NUMBER);
				setState(130);
				match(T__6);
				setState(131);
				match(NUMBER);
				setState(132);
				match(T__5);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DerivativeContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public DerivativeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_derivative; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterDerivative(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitDerivative(this);
		}
	}

	public final DerivativeContext derivative() throws RecognitionException {
		DerivativeContext _localctx = new DerivativeContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_derivative);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(135);
			match(T__7);
			setState(136);
			identifier();
			setState(137);
			expresion();
			setState(138);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegralContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public IntegralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterIntegral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitIntegral(this);
		}
	}

	public final IntegralContext integral() throws RecognitionException {
		IntegralContext _localctx = new IntegralContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_integral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			match(T__8);
			setState(141);
			identifier();
			setState(142);
			expresion();
			setState(143);
			expresion();
			setState(144);
			expresion();
			setState(145);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SumatoriaContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public SumatoriaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sumatoria; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterSumatoria(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitSumatoria(this);
		}
	}

	public final SumatoriaContext sumatoria() throws RecognitionException {
		SumatoriaContext _localctx = new SumatoriaContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_sumatoria);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(147);
			match(T__9);
			setState(148);
			identifier();
			setState(149);
			expresion();
			setState(150);
			expresion();
			setState(151);
			expresion();
			setState(152);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public FunctionCallParamsContext functionCallParams() {
			return getRuleContext(FunctionCallParamsContext.class,0);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitFunctionCall(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_functionCall);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154);
			identifier();
			setState(155);
			functionCallParams();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallParamsContext extends ParserRuleContext {
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public FunctionCallParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCallParams; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterFunctionCallParams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitFunctionCallParams(this);
		}
	}

	public final FunctionCallParamsContext functionCallParams() throws RecognitionException {
		FunctionCallParamsContext _localctx = new FunctionCallParamsContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_functionCallParams);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(T__4);
			setState(166);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << ID) | (1L << NUMBER))) != 0)) {
				{
				setState(158);
				expresion();
				setState(163);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__10) {
					{
					{
					setState(159);
					match(T__10);
					setState(160);
					expresion();
					}
					}
					setState(165);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(168);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VVectorContext extends ParserRuleContext {
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public VVectorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_vVector; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterVVector(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitVVector(this);
		}
	}

	public final VVectorContext vVector() throws RecognitionException {
		VVectorContext _localctx = new VVectorContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_vVector);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			match(T__11);
			setState(172); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(171);
				expresion();
				}
				}
				setState(174); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << ID) | (1L << NUMBER))) != 0) );
			setState(176);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrizContext extends ParserRuleContext {
		public List<VVectorContext> vVector() {
			return getRuleContexts(VVectorContext.class);
		}
		public VVectorContext vVector(int i) {
			return getRuleContext(VVectorContext.class,i);
		}
		public MatrizContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matriz; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterMatriz(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitMatriz(this);
		}
	}

	public final MatrizContext matriz() throws RecognitionException {
		MatrizContext _localctx = new MatrizContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_matriz);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			match(T__11);
			setState(180); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(179);
				vVector();
				}
				}
				setState(182); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__11 );
			setState(184);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LListContext extends ParserRuleContext {
		public SeqObContext seqOb() {
			return getRuleContext(SeqObContext.class,0);
		}
		public LListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterLList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitLList(this);
		}
	}

	public final LListContext lList() throws RecognitionException {
		LListContext _localctx = new LListContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_lList);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			match(T__13);
			setState(187);
			seqOb();
			setState(188);
			match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabeledObjectContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ObjetoContext objeto() {
			return getRuleContext(ObjetoContext.class,0);
		}
		public LabeledObjectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_labeledObject; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterLabeledObject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitLabeledObject(this);
		}
	}

	public final LabeledObjectContext labeledObject() throws RecognitionException {
		LabeledObjectContext _localctx = new LabeledObjectContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_labeledObject);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			match(T__15);
			setState(191);
			identifier();
			setState(192);
			match(T__15);
			setState(193);
			objeto();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MagnitudeContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(SymbMathParser.NUMBER, 0); }
		public UExprContext uExpr() {
			return getRuleContext(UExprContext.class,0);
		}
		public MagnitudeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_magnitude; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterMagnitude(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitMagnitude(this);
		}
	}

	public final MagnitudeContext magnitude() throws RecognitionException {
		MagnitudeContext _localctx = new MagnitudeContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_magnitude);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			match(NUMBER);
			setState(196);
			match(T__16);
			setState(197);
			uExpr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UExprContext extends ParserRuleContext {
		public UExprPowerContext uExprPower() {
			return getRuleContext(UExprPowerContext.class,0);
		}
		public List<UExprContext> uExpr() {
			return getRuleContexts(UExprContext.class);
		}
		public UExprContext uExpr(int i) {
			return getRuleContext(UExprContext.class,i);
		}
		public UExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_uExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterUExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitUExpr(this);
		}
	}

	public final UExprContext uExpr() throws RecognitionException {
		return uExpr(0);
	}

	private UExprContext uExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		UExprContext _localctx = new UExprContext(_ctx, _parentState);
		UExprContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_uExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(200);
			uExprPower();
			}
			_ctx.stop = _input.LT(-1);
			setState(207);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new UExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_uExpr);
					setState(202);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(203);
					_la = _input.LA(1);
					if ( !(_la==MULT || _la==DIV) ) {
					_errHandler.recoverInline(this);
					} else {
						consume();
					}
					setState(204);
					uExpr(2);
					}
					} 
				}
				setState(209);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UExprPowerContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SymbMathParser.ID, 0); }
		public TerminalNode NUMBER() { return getToken(SymbMathParser.NUMBER, 0); }
		public UExprPowerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_uExprPower; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterUExprPower(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitUExprPower(this);
		}
	}

	public final UExprPowerContext uExprPower() throws RecognitionException {
		UExprPowerContext _localctx = new UExprPowerContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_uExprPower);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			match(ID);
			setState(216);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(211);
				match(CARET);
				setState(213);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(212);
					match(T__3);
					}
				}

				setState(215);
				match(NUMBER);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SeqObContext extends ParserRuleContext {
		public List<ObjetoContext> objeto() {
			return getRuleContexts(ObjetoContext.class);
		}
		public ObjetoContext objeto(int i) {
			return getRuleContext(ObjetoContext.class,i);
		}
		public SeqObContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_seqOb; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterSeqOb(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitSeqOb(this);
		}
	}

	public final SeqObContext seqOb() throws RecognitionException {
		SeqObContext _localctx = new SeqObContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_seqOb);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(221);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__11) | (1L << T__13) | (1L << T__15) | (1L << STRING_LITERAL) | (1L << ID) | (1L << NUMBER))) != 0)) {
				{
				{
				setState(218);
				objeto();
				}
				}
				setState(223);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SStringContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(SymbMathParser.STRING_LITERAL, 0); }
		public SStringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sString; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterSString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitSString(this);
		}
	}

	public final SStringContext sString() throws RecognitionException {
		SStringContext _localctx = new SStringContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_sString);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(224);
			match(STRING_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParListIdsContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public ParListIdsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parListIds; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterParListIds(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitParListIds(this);
		}
	}

	public final ParListIdsContext parListIds() throws RecognitionException {
		ParListIdsContext _localctx = new ParListIdsContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_parListIds);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226);
			match(T__4);
			setState(235);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(227);
				identifier();
				setState(232);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__10) {
					{
					{
					setState(228);
					match(T__10);
					setState(229);
					identifier();
					}
					}
					setState(234);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(237);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SymbMathParser.ID, 0); }
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).enterIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SymbMathListener ) ((SymbMathListener)listener).exitIdentifier(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_identifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 21:
			return uExpr_sempred((UExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean uExpr_sempred(UExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\32\u00f4\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\3\2\6\2:\n\2\r\2\16\2;\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\5\3H\n\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5"+
		"\3\6\3\6\3\6\3\6\3\7\3\7\3\7\7\7Z\n\7\f\7\16\7]\13\7\3\b\3\b\3\b\7\bb"+
		"\n\b\f\b\16\be\13\b\3\t\3\t\3\t\7\tj\n\t\f\t\16\tm\13\t\3\n\3\n\3\n\3"+
		"\n\3\n\5\nt\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13"+
		"\u0080\n\13\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0088\n\f\3\r\3\r\3\r\3\r\3\r"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\20\3\20\3\20\3\21\3\21\3\21\3\21\7\21\u00a4\n\21\f\21\16\21\u00a7\13"+
		"\21\5\21\u00a9\n\21\3\21\3\21\3\22\3\22\6\22\u00af\n\22\r\22\16\22\u00b0"+
		"\3\22\3\22\3\23\3\23\6\23\u00b7\n\23\r\23\16\23\u00b8\3\23\3\23\3\24\3"+
		"\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\27\3\27\3"+
		"\27\3\27\3\27\3\27\7\27\u00d0\n\27\f\27\16\27\u00d3\13\27\3\30\3\30\3"+
		"\30\5\30\u00d8\n\30\3\30\5\30\u00db\n\30\3\31\7\31\u00de\n\31\f\31\16"+
		"\31\u00e1\13\31\3\32\3\32\3\33\3\33\3\33\3\33\7\33\u00e9\n\33\f\33\16"+
		"\33\u00ec\13\33\5\33\u00ee\n\33\3\33\3\33\3\34\3\34\3\34\2\3,\35\2\4\6"+
		"\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\66\2\4\3\2\5\6\3\2"+
		"\31\32\u00f8\29\3\2\2\2\4G\3\2\2\2\6I\3\2\2\2\bN\3\2\2\2\nR\3\2\2\2\f"+
		"V\3\2\2\2\16^\3\2\2\2\20f\3\2\2\2\22s\3\2\2\2\24\177\3\2\2\2\26\u0087"+
		"\3\2\2\2\30\u0089\3\2\2\2\32\u008e\3\2\2\2\34\u0095\3\2\2\2\36\u009c\3"+
		"\2\2\2 \u009f\3\2\2\2\"\u00ac\3\2\2\2$\u00b4\3\2\2\2&\u00bc\3\2\2\2(\u00c0"+
		"\3\2\2\2*\u00c5\3\2\2\2,\u00c9\3\2\2\2.\u00d4\3\2\2\2\60\u00df\3\2\2\2"+
		"\62\u00e2\3\2\2\2\64\u00e4\3\2\2\2\66\u00f1\3\2\2\28:\5\4\3\298\3\2\2"+
		"\2:;\3\2\2\2;9\3\2\2\2;<\3\2\2\2<\3\3\2\2\2=H\5\6\4\2>H\5\f\7\2?H\5\n"+
		"\6\2@H\5\"\22\2AH\5$\23\2BH\5&\24\2CH\5(\25\2DH\5\62\32\2EH\5*\26\2FH"+
		"\5\b\5\2G=\3\2\2\2G>\3\2\2\2G?\3\2\2\2G@\3\2\2\2GA\3\2\2\2GB\3\2\2\2G"+
		"C\3\2\2\2GD\3\2\2\2GE\3\2\2\2GF\3\2\2\2H\5\3\2\2\2IJ\5\66\34\2JK\5\64"+
		"\33\2KL\7\3\2\2LM\5\n\6\2M\7\3\2\2\2NO\5\66\34\2OP\7\3\2\2PQ\5\f\7\2Q"+
		"\t\3\2\2\2RS\7\4\2\2ST\5\f\7\2TU\7\4\2\2U\13\3\2\2\2V[\5\16\b\2WX\t\2"+
		"\2\2XZ\5\16\b\2YW\3\2\2\2Z]\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2\\\r\3\2\2\2]"+
		"[\3\2\2\2^c\5\20\t\2_`\t\3\2\2`b\5\20\t\2a_\3\2\2\2be\3\2\2\2ca\3\2\2"+
		"\2cd\3\2\2\2d\17\3\2\2\2ec\3\2\2\2fk\5\22\n\2gh\7\30\2\2hj\5\22\n\2ig"+
		"\3\2\2\2jm\3\2\2\2ki\3\2\2\2kl\3\2\2\2l\21\3\2\2\2mk\3\2\2\2no\7\5\2\2"+
		"ot\5\24\13\2pq\7\6\2\2qt\5\24\13\2rt\5\24\13\2sn\3\2\2\2sp\3\2\2\2sr\3"+
		"\2\2\2t\23\3\2\2\2u\u0080\5\26\f\2v\u0080\5\36\20\2w\u0080\5\66\34\2x"+
		"y\7\7\2\2yz\5\f\7\2z{\7\b\2\2{\u0080\3\2\2\2|\u0080\5\30\r\2}\u0080\5"+
		"\32\16\2~\u0080\5\34\17\2\177u\3\2\2\2\177v\3\2\2\2\177w\3\2\2\2\177x"+
		"\3\2\2\2\177|\3\2\2\2\177}\3\2\2\2\177~\3\2\2\2\u0080\25\3\2\2\2\u0081"+
		"\u0088\7\27\2\2\u0082\u0083\7\7\2\2\u0083\u0084\7\27\2\2\u0084\u0085\7"+
		"\t\2\2\u0085\u0086\7\27\2\2\u0086\u0088\7\b\2\2\u0087\u0081\3\2\2\2\u0087"+
		"\u0082\3\2\2\2\u0088\27\3\2\2\2\u0089\u008a\7\n\2\2\u008a\u008b\5\66\34"+
		"\2\u008b\u008c\5\f\7\2\u008c\u008d\7\b\2\2\u008d\31\3\2\2\2\u008e\u008f"+
		"\7\13\2\2\u008f\u0090\5\66\34\2\u0090\u0091\5\f\7\2\u0091\u0092\5\f\7"+
		"\2\u0092\u0093\5\f\7\2\u0093\u0094\7\b\2\2\u0094\33\3\2\2\2\u0095\u0096"+
		"\7\f\2\2\u0096\u0097\5\66\34\2\u0097\u0098\5\f\7\2\u0098\u0099\5\f\7\2"+
		"\u0099\u009a\5\f\7\2\u009a\u009b\7\b\2\2\u009b\35\3\2\2\2\u009c\u009d"+
		"\5\66\34\2\u009d\u009e\5 \21\2\u009e\37\3\2\2\2\u009f\u00a8\7\7\2\2\u00a0"+
		"\u00a5\5\f\7\2\u00a1\u00a2\7\r\2\2\u00a2\u00a4\5\f\7\2\u00a3\u00a1\3\2"+
		"\2\2\u00a4\u00a7\3\2\2\2\u00a5\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6"+
		"\u00a9\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a8\u00a0\3\2\2\2\u00a8\u00a9\3\2"+
		"\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab\7\b\2\2\u00ab!\3\2\2\2\u00ac\u00ae"+
		"\7\16\2\2\u00ad\u00af\5\f\7\2\u00ae\u00ad\3\2\2\2\u00af\u00b0\3\2\2\2"+
		"\u00b0\u00ae\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2\u00b3"+
		"\7\17\2\2\u00b3#\3\2\2\2\u00b4\u00b6\7\16\2\2\u00b5\u00b7\5\"\22\2\u00b6"+
		"\u00b5\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2"+
		"\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bb\7\17\2\2\u00bb%\3\2\2\2\u00bc\u00bd"+
		"\7\20\2\2\u00bd\u00be\5\60\31\2\u00be\u00bf\7\21\2\2\u00bf\'\3\2\2\2\u00c0"+
		"\u00c1\7\22\2\2\u00c1\u00c2\5\66\34\2\u00c2\u00c3\7\22\2\2\u00c3\u00c4"+
		"\5\4\3\2\u00c4)\3\2\2\2\u00c5\u00c6\7\27\2\2\u00c6\u00c7\7\23\2\2\u00c7"+
		"\u00c8\5,\27\2\u00c8+\3\2\2\2\u00c9\u00ca\b\27\1\2\u00ca\u00cb\5.\30\2"+
		"\u00cb\u00d1\3\2\2\2\u00cc\u00cd\f\3\2\2\u00cd\u00ce\t\3\2\2\u00ce\u00d0"+
		"\5,\27\4\u00cf\u00cc\3\2\2\2\u00d0\u00d3\3\2\2\2\u00d1\u00cf\3\2\2\2\u00d1"+
		"\u00d2\3\2\2\2\u00d2-\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d4\u00da\7\26\2\2"+
		"\u00d5\u00d7\7\30\2\2\u00d6\u00d8\7\6\2\2\u00d7\u00d6\3\2\2\2\u00d7\u00d8"+
		"\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9\u00db\7\27\2\2\u00da\u00d5\3\2\2\2"+
		"\u00da\u00db\3\2\2\2\u00db/\3\2\2\2\u00dc\u00de\5\4\3\2\u00dd\u00dc\3"+
		"\2\2\2\u00de\u00e1\3\2\2\2\u00df\u00dd\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0"+
		"\61\3\2\2\2\u00e1\u00df\3\2\2\2\u00e2\u00e3\7\25\2\2\u00e3\63\3\2\2\2"+
		"\u00e4\u00ed\7\7\2\2\u00e5\u00ea\5\66\34\2\u00e6\u00e7\7\r\2\2\u00e7\u00e9"+
		"\5\66\34\2\u00e8\u00e6\3\2\2\2\u00e9\u00ec\3\2\2\2\u00ea\u00e8\3\2\2\2"+
		"\u00ea\u00eb\3\2\2\2\u00eb\u00ee\3\2\2\2\u00ec\u00ea\3\2\2\2\u00ed\u00e5"+
		"\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f0\7\b\2\2\u00f0"+
		"\65\3\2\2\2\u00f1\u00f2\7\26\2\2\u00f2\67\3\2\2\2\24;G[cks\177\u0087\u00a5"+
		"\u00a8\u00b0\u00b8\u00d1\u00d7\u00da\u00df\u00ea\u00ed";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}