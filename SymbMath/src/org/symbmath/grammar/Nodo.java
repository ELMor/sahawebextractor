package org.symbmath.grammar;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Vector;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.symbmath.actions.Derivable;
import org.symbmath.grammar.rules.Expresion;
import org.symbmath.grammar.rules.NNumber;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class Nodo implements InfixHelper, Stackable, Serializable, Derivable, Cloneable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3643358099511699025L;
	private String text=null;
	public int type=-1;
	public OPExp subtype=null;
	public Vector<Nodo> childs=new Vector<>();
	public Nodo sibling=null;
	
	public Nodo mutate(Nodo other){
		text=other.text;
		type=other.type;
		subtype=other.subtype;
		childs=other.childs;
		sibling=other.sibling;
		return this;
	}
	
	public static HashMap<Integer, Class<?>> rules=new HashMap<>();
	static {		
		int type=0;
		for(String cn:SymbMathParser.ruleNames){
			//Upper first letter
			cn="org.symbmath.grammar.rules."+cn.substring(0, 1).toUpperCase()+cn.substring(1);
			try {
				rules.put(type++, Class.forName(cn));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.exit(-2);
			}
		}	
	}
	
	/**
	 * Transform Tree from Parsing to a more convenient Tree
	 * @param p Parser
	 * @param rc RuleContext
	 * @return Nodo
	 */
	public static Nodo transform(Parser p, RuleContext rc) throws Exception{
		//Class type as index
		int ruleNdx=rc.getRuleIndex();
		//Load Class and Constructor for the Nodo, and call it
		Class<?> dynClass=rules.get(ruleNdx);
		Constructor<?> dynCtor=dynClass.getConstructor(new Class[]{Parser.class,RuleContext.class});
		Nodo dynNodo=(Nodo)dynCtor.newInstance(new Object[]{p,rc});
		if(ruleNdx!=dynNodo.type){
			//Nodo need mutate. Sometimes is convenient
			//that Expresion change its type to other.
			Class<?> cMutated=rules.get(dynNodo.type);
			Constructor<?> ctor=cMutated.getConstructor();	
			Nodo newNodo= (Nodo)ctor.newInstance(new Object[]{});
			newNodo.mutate(dynNodo);
			return newNodo;
		}
		return dynNodo;
	}
	
	public static Nodo parse(int type, String from) throws Exception{
		InputStream is=new ByteArrayInputStream(from.getBytes());
		SymbMathLexer lexer=new SymbMathLexer(new ANTLRInputStream(is));
		SymbMathParser parser=new SymbMathParser(new CommonTokenStream(lexer));
		parser.setBuildParseTree(true);
		String ruleName=SymbMathParser.ruleNames[type];
		Method method=SymbMathParser.class.getMethod(ruleName, new Class[]{});
		RuleContext rcInit=(RuleContext)method.invoke(parser, new Object[]{});
		return transform(parser,rcInit);
	}
	
	public static RuleContext gC(RuleContext rc, int j){
		ParseTree pt=rc.getChild(j);
		Object pl=pt.getPayload();
		if(pl instanceof RuleContext)
			return (RuleContext)pl;
		System.err.println("It's a Terminal on stream:"+pt.toString());
		return null;
	}

	public int getSTNdx(){
		int i;
		for(i=0;i<subtInt.length&&!(subtype==subtInt[i]);i++)
			;
		return i;
	}
	
	public Nodo(int t){
		type=t;
	}

	public int size(){
		return childs.size();
	}

	public Nodo g(int i){
		return childs.elementAt(i);
	}
	
	public final Nodo getFirstChild(){
		return childs.size()>0? childs.elementAt(0):null; 
	}
	
	public final Nodo getLastChild(){
		return childs.size()>0? childs.elementAt(childs.size()-1) : null;
	}

	public final Nodo getNextSibling(){
		return sibling;
	}

	public final void setNextSibling(Nodo ns){
		sibling=ns;
	}
	
	public void setText(String t){
		text=t;
	}
	
	public final Nodo addChild(Nodo n){
		Nodo last=getLastChild();
		if(last!=null)
			last.sibling=n;
		n.sibling=null;
		childs.addElement(n);
		return this;
	}
	
	public String toString(){
		return "Oops!!";
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public boolean pushNoExec() {
		return true;
	}

	public Nodo sum(Nodo r){
		if(this.equals(NNumber.cero))
			return r;
		if(r.equals(NNumber.cero))
			return this;
		Expresion res=new Expresion();
		res.subtype=OPExp.Sum;
		res.addChild(this);
		res.addChild(r);
		return res;
	}

	public Nodo res(Nodo r){
		if(this.equals(r))
			return NNumber.cero;
		Expresion res=new Expresion();
		res.subtype=OPExp.Res;
		res.addChild(this);
		res.addChild(r);
		return res;
	}

	public Nodo mul(Nodo r){
		if(this.equals(NNumber.uno))
			return r;
		if(r.equals(NNumber.uno))
			return this;
		if(this.equals(NNumber.cero))
			return NNumber.cero;
		if(r.equals(NNumber.cero))
			return NNumber.cero;
		Expresion res=new Expresion();
		res.subtype=OPExp.Mul;
		res.addChild(this);
		res.addChild(r);
		return res;
	}

	public Nodo div(Nodo r){
		if(this.equals(r))
			return NNumber.uno;
		if(r.equals(NNumber.uno))
			return this;
		Expresion res=new Expresion();
		res.subtype=OPExp.Div;
		res.addChild(this);
		res.addChild(r);
		return res;
	}

	public Nodo pow(Nodo r){
		if(r.equals(NNumber.cero))
			return NNumber.uno;
		if(r.equals(NNumber.uno))
			return this;
		Expresion res=new Expresion();
		res.subtype=OPExp.Pow;
		res.addChild(this);
		res.addChild(r);
		return res;
	}

	@Override
	public int argsNeeded() {
		return 0;
	}

	@Override
	public int returnNumber() {
		return 0;
	}

	@Override
	public void exec(Ctx c) throws Exception{
		for(Nodo n:childs)
			n.exec(c);
	}
	@Override
	public boolean mustRegister() {
		return false;
	}
	public void localizeVars(String prepend){
		for(Nodo n:childs)
			if(n.type==SymbMathParser.RULE_identifier)
				n.setText(prepend+n.getText());
		for(Nodo n:childs)
			if(n.type==SymbMathParser.RULE_expresion)
				((Expresion)n).localizeVars(prepend);
	}
	@Override
	public Nodo derive(Nodo expresion, Nodo listVar) {
		return null;
	}
	
	public String showTree(){
		return "Generic node";
	}
	
	public Nodo simplify(){
		return this;
	}

	@Override
	public Object clone() {
		Object ret=null;
		try {
			ret = super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
	
}
