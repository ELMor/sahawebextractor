package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class FunctionCall extends Nodo {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2713280482867029044L;
	public final static int fName=0,params=1;
	
	public FunctionCall(){
		super(SymbMathParser.RULE_functionCall);
	}
	// identifier functoinCallPar
	public FunctionCall(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());		
		addChild(transform(p,gC(pt,0)));
		addChild(transform(p,gC(pt,1)));
	}

	@Override
	public String toString() {
		return ""+g(fName)+g(params);
	}
	
	@Override
	public void exec(Ctx c) throws Exception {
		Stackable st=c.get(g(fName).getText());
		if(st!=null){
			g(params).exec(c);
			st.exec(c);
		}else{
			c.push(this);
		}
	}
}
