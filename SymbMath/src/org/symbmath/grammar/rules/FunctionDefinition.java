package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Exceptions.FunctionNotDefined;
import org.symbmath.runtime.Exceptions.TooFewArgs;

public class FunctionDefinition extends Nodo {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8141756749477168257L;
	public static final int fName=0,vars=1,exp=2;
	
	public FunctionDefinition(){
		super(SymbMathParser.RULE_functionDefinition);
	}
	
	public FunctionDefinition(Parser p, RuleContext pt)  throws Exception { 
		super(pt.getRuleIndex());
		
		RuleContext f= gC(pt,0);
		RuleContext v=  gC(pt,1);
		RuleContext e=  gC(pt,3);
		
		Nodo functionName=transform(p,f);
		Nodo parListIds=transform(p,v);
		Nodo expresion=transform(p,e);
		
		addChild(functionName);
		addChild(parListIds);
		addChild(expresion);
		
		expresion.localizeVars("$");
		parListIds.localizeVars("$");
	}
	
	public String toString(){
		return "" + g(fName) + g(vars) + "=" + g(exp); 
	}

	@Override
	public void exec(Ctx c) throws TooFewArgs, FunctionNotDefined {
		c.push(g(exp));
		c.push(g(vars));
		c.push(g(fName));
		c.dynExec("$$DefineFunction");
	}
}
