package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;

public class Objeto extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -16942311422452830L;

	public Objeto(){
		super(SymbMathParser.RULE_objeto);
	}
	
	public Objeto(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		addChild(transform(p,gC(pt,0)));
	}

	@Override
	public String toString(){
		return g(0).toString();
	}
	
	@Override
	public void exec(Ctx c) throws Exception {
		g(0).exec(c);
	}
}
