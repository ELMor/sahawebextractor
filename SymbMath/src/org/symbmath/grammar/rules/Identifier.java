package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class Identifier extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7779670176102302589L;
	public Identifier(){
		super(SymbMathParser.RULE_identifier);
	}
	
	public Identifier(Parser p, RuleContext pt) {
		super(pt.getRuleIndex());
		setText(pt.getText());
	}
	public Identifier(String id){
		this();
		setText(id);
	}
	@Override
	public String toString() {
		return getText();
	}
	@Override
	public void exec(Ctx c) throws Exception {
		Stackable res=c.dir.get(getText());
		if(res==null)
			c.push(this);
		else
			res.exec(c);
	}
	@Override
	public boolean equals(Object obj) {
		if(! (obj instanceof Identifier))
			return false;
		Identifier o=(Identifier) obj;
		return getText().equals(o.getText());
	}
	@Override
	public String showTree() {
		return getText();
	}
}
