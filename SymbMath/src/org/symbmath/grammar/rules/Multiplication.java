package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;

public class Multiplication extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4295347465473635872L;

	public Multiplication(){
		super(SymbMathParser.RULE_multiplication);
	}
	
	public Multiplication(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		Nodo left=transform(p, gC(pt,0));
		for(int i=1; i<pt.getChildCount();i+=2){
			Nodo right=transform(p,gC(pt,i+1));
			switch(pt.getChild(i).getText().charAt(0)){
			case '*': left=left.mul(right); break;
			case '/': left=left.div(right); break;
			}
		}
		mutate(left);
	}

	@Override
	public String toString(){
		String ret="";
		boolean first=true;
		for(Nodo n:childs){
			if(first)
				first=false;
			else
				ret+="*";
			ret+=n.toString();
		}
		return ret;
	}
	
	@Override
	public void exec(Ctx c) throws Exception {
		g(0).exec(c);
		g(1).exec(c);
		c.dynExec(subtype==OPExp.Mul ? "*":"/");
	}
}
