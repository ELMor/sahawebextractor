package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;

public class Derivative extends Nodo {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2736215049979256024L;
	public static final int var=0,exp=1;
	
	public Derivative(){
		super(SymbMathParser.RULE_derivative);
	}
	
	public Derivative(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		
		RuleContext v = gC(pt,1);
		RuleContext e=  gC(pt,2);
		
		addChild(transform(p,v));
		addChild(transform(p,e));
	}

	@Override
	public String toString(){
		return "(>"+g(var)+" "+g(exp)+")";
	}	
}
