package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
/**
 * unary
	: '+' atom
	| '-' atom
	| atom
	;
 * @author boss
 *
 */
public class Unary extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7946015336654603034L;

	public Unary(){
		super(SymbMathParser.RULE_unary);
	}
	
	public Unary(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		switch(pt.getChildCount()){
		case 1: //(Unary->atom)=>(atom)
			mutate(transform(p,gC(pt,0)));
			break;
		case 2:
			String op=pt.getChild(0).getText();
			if(op.equals("+")){
				mutate(transform(p,gC(pt,1)));
			}else{ //CHS
				Nodo m=transform(p,gC(pt,1));
				if(m instanceof NNumber){
					NNumber num=(NNumber)m;
					num=num.chs(null);
					mutate(num);
				}else{
					Nodo n=new Expresion();
					n.subtype=OPExp.Neg;
					n.addChild(m);
					mutate(n);
				}
			}
		}
	}

	@Override
	public String toString(){
		return g(0).toString();
	}
	
	@Override
	public void exec(Ctx c) throws Exception {
		g(0).exec(c);
	}	
}
