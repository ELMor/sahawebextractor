package org.symbmath.grammar.rules;

import java.util.Vector;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Stackable;

public class FunctionCallParams extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7706888572770866656L;

	public FunctionCallParams(){
		super(SymbMathParser.RULE_functionCallParams);
	}
	
	public FunctionCallParams(Parser p, RuleContext pt) throws Exception  {
		super(pt.getRuleIndex());
		for(int i=1;i<pt.getChildCount()-1;i+=2)
			addChild(transform(p,gC(pt,i)));
	}

	@Override
	public String toString(){
		String ret="(";
		boolean f=true;
		for(Nodo n:childs){
			if(f)
				f=false;
			else
				ret+=",";
			ret+=n.toString();
		}
		return ret+")";
	}	
}
