package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;

public class SeqOb extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2577598657788971576L;

	public SeqOb(){
		super(SymbMathParser.RULE_seqOb);
	}
	
	public SeqOb(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		
		for(int i=0;i<pt.getChildCount();i++)
			addChild(transform(p,gC(pt,i)));
	}

	@Override
	public String toString() {
		String ret="";
		for(Nodo n:childs)
			ret+=n+" ";
		return ret;
	}
	@Override
	public void exec(Ctx c) {
		c.push(this);
	}	
}
