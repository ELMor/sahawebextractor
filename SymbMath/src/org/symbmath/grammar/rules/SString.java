package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;

public class SString extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3401393128101512388L;

	public SString(){
		super(SymbMathParser.RULE_sString);
	}
	
	public SString(Parser p, RuleContext pt) {
		super(pt.getRuleIndex());
		setText(pt.getText());
	}

	@Override
	public String toString() {
		return "\""+getText()+"\"";
	}
	
}
