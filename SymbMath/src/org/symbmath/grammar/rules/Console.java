package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;

public class Console extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6741930377686448445L;

	public Console(){
		super(SymbMathParser.RULE_objeto);
	}
	
	public Console(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		for(int i=0;i<pt.getChildCount();i++)	
			addChild(transform(p,gC(pt,i)));
	}

	@Override
	public String toString(){
		String ret="";
		for(Nodo n:childs)
			ret+=n.toString()+"\n";
		return ret;
	}
	
}
