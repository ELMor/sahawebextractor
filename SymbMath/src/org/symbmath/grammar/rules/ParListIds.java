package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class ParListIds extends Nodo implements Stackable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7352458498943326794L;

	public ParListIds(){
		super(SymbMathParser.RULE_parListIds);
	}
	
	public ParListIds(Parser p, RuleContext pt) throws Exception  {
		super(pt.getRuleIndex());
		int nIds=(pt.getChildCount()-1)/2; //( , )
		for(int i=1;i<nIds*2;i+=2)
			addChild( transform(p,gC(pt,i)) );
	}
	
	public String toString(){
		String ret="(";
		boolean first=true;
		for(Nodo n:childs){
			if(first) first=false; else ret+=",";
			ret+=n.toString();
		}
		ret+=")";
		if(sibling!=null)
			ret+="="+sibling;
		return ret;
	}

	@Override
	public int argsNeeded() {
		return childs.size();
	}

	@Override
	public int returnNumber() {
		return 0;
	}

	@Override
	public void exec(Ctx c) throws Exception{
		Ctx.createLocal();
		for(int i=childs.size()-1;i>=0;i--){
			String localName=childs.get(i).getText();
			Ctx.putLocal(localName, c.pop());
		}
		if(sibling!=null){ //NoEvalExpression, must eval its child
			try {
				sibling.g(0).exec(c);
			} catch (Exception e) {
				throw e;
			}finally {
				Ctx.removeLocal();
			}
		}
	}
	
}
