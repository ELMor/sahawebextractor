package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;

public class Integral extends Nodo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4272679837638985985L;
	public static final int var=0,init=1,end=2,expr=3;
	
	public Integral(){
		super(SymbMathParser.RULE_integral);
	}
	
	public Integral(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		
		RuleContext
			v  = gC(pt,1),
			i  = gC(pt,2),
			e  = gC(pt,3),
			ex = gC(pt,4)
			;

		addChild(transform(p, v));
		addChild(transform(p, i));
		addChild(transform(p, e));
		addChild(transform(p, ex));
	}
	
	@Override
	public String toString() {
		return "(<"+g(var)+" "+g(init)+" "+g(end)+" "+g(expr)+")";
	}

	@Override
	public void exec(Ctx c) throws Exception {
		g(expr).exec(c);
		g(init).exec(c);
		g(end).exec(c);
		c.push(g(var));
		c.dynExec("$$Integral");
	}
}
