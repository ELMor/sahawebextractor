package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.runtime.Exceptions.FunctionNotDefined;
import org.symbmath.runtime.Exceptions.TooFewArgs;
import org.symbmath.tree.SymbExp;

public class Expresion extends Nodo implements Stackable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -737334234468623074L;
	public static final int singleOp=0, leftOp=0, rightOp=1;
	public Expresion(){
		super(SymbMathParser.RULE_expresion);
	}
	
	public Expresion(OPExp st){
		this();
		subtype=st;
	}
	public static boolean separate[]={
		false,false,false,false,false,true,true,
		true,false,false,false,false,false,false
	};
	
	public Expresion(Parser p, RuleContext pt) throws Exception  {
		super( pt.getRuleIndex() );
		Nodo left=transform(p, gC(pt,0));
		for(int i=1; i<pt.getChildCount();i+=2){
			Nodo right=transform(p,gC(pt,i+1));
			switch(pt.getChild(i).getText().charAt(0)){
			case '+': left=left.sum(right); break;
			case '-': left=left.res(right); break;
			}
		}
		mutate(left);
	}

	public int getPriority(){
		return prior[getSTNdx()];
	}
	
	@Override
	public String toString() {
		int p=getPriority();
		String ret=parentesis(g(0), p);
		if(childs.size()>1){
			switch(subtype){
			case Sum: ret+="+"+parentesis(g(1), p); break;
			case Res: ret+="-"+parentesis(g(1), p); break;
			case Div: ret+="/"+parentesis(g(1), p); break;
			case Mul: ret+="*"+parentesis(g(1), p); break;
			case Pow: ret+="^"+parentesis(g(1), p); break;
			default: throw new RuntimeException("KK");
			}
		}
		return (subtype==OPExp.Neg?"-("+ret+")":ret);
	}

	private String parentesis(Nodo n, int pr) {
		boolean par=false;
		String s=n.toString();
		if(n.type==SymbMathParser.RULE_expresion){
			int leftPr=((Expresion)n).getPriority();
			if(leftPr<pr)
				par=true;
		}
		if(par)
			return "("+s.toString()+")";
		else
			return s;
	}
	
	@Override
	public boolean pushNoExec() {
		return true;
	}

	@Override
	public int argsNeeded() {
		return 0;
	}

	@Override
	public int returnNumber() {
		return 1;
	}
	
	@Override
	public void exec(Ctx c) throws Exception{
		for(Nodo n:childs)
			n.exec(c);
		try {
			switch(subtype){
			case Sum: c.dynExec("+"); break;
			case Res: c.dynExec("-"); break;
			case Div: c.dynExec("/"); break;
			case Mul: c.dynExec("*"); break;
			case Pow: c.dynExec("^"); break;
			case Neg: c.dynExec("CHS"); break;
			default: throw new RuntimeException("KK");
			}
		} catch (TooFewArgs e) {
			e.printStackTrace();
		}
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Expresion))
			return false;
		Expresion r=(Expresion) obj;
		//Same number of childs
		if(childs.size()!=r.childs.size())
			return false;
		//Same type of Op
		if(subtype!=r.subtype)
			return false;
		switch(childs.size()){
		//Only one child? then must be equal
		case 1: return g(0).equals(r.g(0));
		//Two Childs: Notice order
		case 2:
			return (g(0).equals(r.g(0)) || g(0).equals(r.g(1)))
				&& (g(1).equals(r.g(0)) || g(1).equals(r.g(1)));
		}
		return false;
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	public String showTree() {
		if(size()==1) {
			switch(subtype){
			case Neg: return "Neg("+g(0).showTree()+")";
			case Inv: return "Inv("+g(0).showTree()+")";
			}
		}
		if(size()==2){
			Nodo l=g(0),r=g(1);
			switch(subtype){
			case Mul: return "*("+l.showTree()+","+r.showTree()+")";
			case Div: return "/("+l.showTree()+","+r.showTree()+")";
			case Sum: return "+("+l.showTree()+","+r.showTree()+")";
			case Res: return "-("+l.showTree()+","+r.showTree()+")";
			case Pow: return "^("+l.showTree()+","+r.showTree()+")";
			}
		}
		return getText();
	}
	@Override
	public Nodo simplify(){
		return new SymbExp(this).colect();
	}
	@Override
	public String getText() {
		return toString();
	}
}
