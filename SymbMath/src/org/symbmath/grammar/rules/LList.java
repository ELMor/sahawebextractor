package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;

public class LList extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5371590126453431887L;
	public LList(){
		super(SymbMathParser.RULE_lList);
	}
	public LList(Parser p, RuleContext pt) throws Exception  {
		super(pt.getRuleIndex());
		
		RuleContext seq=gC(pt,1);
		for(int i=0;i<seq.getChildCount();i++)
			addChild(transform(p,gC(seq,i)));		
	}
	@Override
	public String toString() {
		String ret="{";
		for(Nodo n:childs)
			ret+=n.toString()+" ";
		return ret+"}";
	}	
	@Override
	public void exec(Ctx c) {
		c.push(this);
	}
}
