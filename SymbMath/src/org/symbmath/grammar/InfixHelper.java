package org.symbmath.grammar;

public interface InfixHelper {
	
	public enum OPExp {
		Inv, Mul, Div, Neg, Pow, Sum, Res, And, Or, Xor, LT, LE, EQ, GE, GT, Disti
	}
	
	public static final String subtName[]={
		"INV","*","/","NEG","^","+","-","AND","OR", "XOR","<","<=","==",">=",">","<>"
	};
		
	public static final OPExp subtInt[]={
		OPExp.Inv, OPExp.Mul, OPExp.Div, OPExp.Neg, OPExp.Pow, OPExp.Sum, OPExp.Res, OPExp.And, OPExp.Or,
		OPExp.Xor, OPExp.LT, OPExp.LE, OPExp.EQ, OPExp.GE, OPExp.GT, OPExp.Disti
	};
	
	public static int prior[]={
		10, 10 , 10, 15, 20, 5, 5, 3, 3, 3, 2, 2, 2, 2, 2, 2
	};

	public default OPExp reverse(OPExp in){ 
		switch(in){
		case And: return OPExp.Or;
		case Disti: return OPExp.EQ;
		case Div: return OPExp.Mul;
		case EQ: return OPExp.Disti;
		case GE: return OPExp.LT;
		case GT: return OPExp.LE;
		case Inv: return OPExp.Inv;
		case LE: return OPExp.GT;
		case LT: return OPExp.GE;
		case Mul: return OPExp.Div;
		case Neg: return OPExp.Neg;
		case Or: return OPExp.And;
		case Pow: return OPExp.Pow;
		case Res: return OPExp.Sum;
		case Sum: return OPExp.Res;
		case Xor: return OPExp.Xor;
		}
		return null;
	}
	public default int getSTNdx(OPExp st){
		int i;
		for(i=0;i<subtInt.length&&!(st==subtInt[i]);i++)
			;
		return i;
	}

}
