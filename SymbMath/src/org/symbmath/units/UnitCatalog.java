package org.symbmath.units;

import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.grammar.rules.Magnitude;
import org.symbmath.runtime.Ctx;

public class UnitCatalog {
 
	public void Def(String s, String m) throws Exception{
		Magnitude nueva= (Magnitude) Nodo.parse(SymbMathParser.RULE_magnitude, m);
		Ctx.addUnit(s, nueva);
	}
	
	public void Def(String s, SIUnit m) throws Exception{
		Ctx.addUnit(s, m);
	}
	
	public UnitCatalog(){
		 try {
			Def("m",new SIUnit());
			Def("sqMeter","1_m^2");
			Def("cuMeter","1_m^3");
			Def("s",new SIUnit());
			Def("mps","1_m/s");
			Def("mps2","1_m/s^2");
			Def("kg", "1000_g");
			Def("mol", new SIUnit());
			Def("N","1_kg*m/s^2");
			Def("J","1_Kg*m^2/s^2");
			Def("W","1_J/s");
			Def("Pa","1_kg/m/s^2");
			Def("A", "1_C/s");
			Def("cd", new SIUnit());
			Def("cdpm2","1_cd/m^2");
			Def("m2ps2", "1_m^2/s^2");
			Def("�K",new SIUnit());
			Def("angulo", new SIUnit());
			Def("arcos", new SIUnit());
			Def("cm", "0.01_m");
			Def("mm", "0.001_m");
			Def("yd","0.9144_m");
			Def("ft","0.3048_m");
			Def("in","0.0254_m");
			Def("Mpc","3.08567818585E22_m");
			Def("pc","3.08567818585E16_m");
			Def("lyr","9.46052840488E15_m");
			Def("au","14959790E4_m");
			Def("km","1000_m");
			Def("mi","1609.344_m");
			Def("nmi","1852_m");
			Def("miUS","1609.34721869_m");
			Def("chain","20.1168402337_m");
			Def("rd","5.02921005842_m");
			Def("fath","1.82880365761_m");
			Def("ftUS","0.304800609601_m");
			Def("mil","0.0000254_m");
			Def("micro","0.000001_m");
			Def("argst","0.0000000001_m");
			Def("fermi","1E-15_m");
			
			Def("b","1E-28_sqMeter");
			Def("ha","10000_sqMeter");
			Def("a","100_sqMeter");
			Def("acre","4046.87260987_sqMeter");
			
			Def("fbm","0.002359737216_cuMeter");
			Def("pk","0.0088097675_cuMeter");
			Def("bu","0.03523907_cuMeter");
			Def("bbl","0.158987294928_cuMeter");
			Def("tsp","4.92892159375E-6_cuMeter");
			Def("tbsp","1.47867647813E-5_cuMeter");
			Def("ozUK","0.000028413075_cuMeter");
			Def("ozfl","2.95735295625E-5_cuMeter");
			Def("cu","2.365882365E-4_cuMeter");
			Def("ml","0.000001_cuMeter");
			Def("pt","0.000473176473_cuMeter");
			Def("qt","0.000946352946_cuMeter");
			Def("gal","0.003785411784_cuMeter");
			Def("galC","0.00454609_cuMeter");
			Def("galUK","0.004546092_cuMeter");
			Def("l","0.001_cuMeter");
			Def("st","1.0_cuMeter");
			Def("Hz","1_s^-1");
			Def("h","3600_s");
			Def("min","60_s");
			Def("d","86400_s");
			Def("yr","31556925.9747_s");
			Def("ga","9.80665_m/s^2");
			 
			Def("c","299792458_mps");
			Def("knot","0.514444444444_mps");
			Def("mph","0.44704_mps");
			Def("kph","0.277777777778_mps");
			
			Def("g",new SIUnit());
			Def("lb","0.45359237_kg");
			Def("u","1.66057E-27_kg");
			Def("grain","0.06479891E-3_kg");
			Def("ct","0.2E-3_kg");
			Def("ozt","31.103475E-3_kg");
			Def("t","1000000E-3_kg");
			Def("tonUK","1016046.9088E-3_kg");
			Def("ton","907184.74E-3_kg");
			Def("lbt","373.2417E-3_kg");
			Def("slug","14593.9029372E-3_kg");
			Def("oz","28.349523125E-3_kg");
			
			Def("pdl","138.254954376E-3_N");
			Def("lbf","4448.22161526E-3_N");
			Def("kip","4448221.61526E-3_N");
			Def("gf","9.80665E-3_N");
			Def("dyn","0.01E-3_N");
			
			Def("MeV","1.60219E-13_J");
			Def("eV","1.60219E-19_J");
			Def("therm","105506000_J");
			Def("Btu","1055.05585262_J");
			Def("cal","4.1868_J");
			Def("kcal","4186.8_J");
			Def("erg","0.0000001_J");
			
			Def("hp","745.699871582_W");
			
			Def("inH20","248.84_Pa");
			Def("inHg","3386.38815789_Pa");
			Def("mmHg","133.322368421_Pa");
			Def("torr","133.322368421_Pa");
			Def("psi","6894.75729317_Pa");
			Def("bar","100000_Pa");
			Def("atm","101325_Pa");
			Def("lam","3183.09886184_cdpm2");
			Def("lm","7.957747154559E-2_cd");
			Def("sb","10000_cdpm2");
			Def("ph","795.774715459_cdpm2");
			Def("lx","7.95774715459E-2_cdpm2");
			Def("flam","3.42625909964_cdpm2");
			Def("fc","0.856564774909_cdpm2");
			Def("Sv","1_m2ps2");
			Def("rem","0.01_m2ps2");
			Def("rad","0.01_m2ps2");
			Def("Gy","1_m2ps2");
			Def("�C","1_gK");
			Def("C",new SIUnit());
			Def("V","1_m^2*kg/s^3/A");
			Def("F","1_A*s/V");
			Def("Ohm","1_kg*m^2/A^2/s^3");
			Def("T","1_kg/s^2/A");
			Def("H","1_m^2*kg/s^2/A^2");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
