package org.symbmath.runtime.Exceptions;

public class FunctionNotDefined extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1896096401543842296L;

	public FunctionNotDefined() {
		super();
	}
	public FunctionNotDefined(String s){
		super(s);
	}
}
