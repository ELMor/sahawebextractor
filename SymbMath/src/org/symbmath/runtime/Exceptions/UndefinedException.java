package org.symbmath.runtime.Exceptions;

public class UndefinedException extends Exception {
	public UndefinedException(String p) {
		super(p);
	}

	public UndefinedException() {
		super();
	}
}
