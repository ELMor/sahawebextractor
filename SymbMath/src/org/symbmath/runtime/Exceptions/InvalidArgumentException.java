package org.symbmath.runtime.Exceptions;

public class InvalidArgumentException extends Exception {
	public InvalidArgumentException(String p) {
		super(p);
	}

	public InvalidArgumentException() {
		super();
	}
}
