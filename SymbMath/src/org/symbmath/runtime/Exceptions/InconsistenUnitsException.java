package org.symbmath.runtime.Exceptions;

public class InconsistenUnitsException extends Exception {
	public InconsistenUnitsException(String p) {
		super(p);
	}

	public InconsistenUnitsException() {
		super();
	}
}
