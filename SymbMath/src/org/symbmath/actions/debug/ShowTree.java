package org.symbmath.actions.debug;

import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Expresion;
import org.symbmath.grammar.rules.Magnitude;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.tree.SymbExp;

public class ShowTree implements Stackable {

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return "showtree";
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	public Nodo exec(Ctx c, Nodo n) throws Exception {
		//System.out.println(n.showTree());
		if(n instanceof Magnitude)	{		
			SymbExp se= new SymbExp((Expresion)(n.g(1)));
			System.out.println( se.toExp() );
		}
		//if(n instanceof Expresion)
			//System.out.println( new Chain((Expresion)n) );		
		if(n instanceof Expresion){
			SymbExp se=new SymbExp((Expresion)n);
			System.out.println( se.toString() );
			System.out.println( se.toExp() );
		}
		return n;
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

	@Override
	public void exec(Ctx c) throws Exception {
		c.dynExec(getText());
	}

}
