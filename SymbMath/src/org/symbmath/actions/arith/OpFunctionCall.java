package org.symbmath.actions.arith;

import org.symbmath.actions.DynExecAction;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.FunctionCallParams;
import org.symbmath.grammar.rules.Identifier;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class OpFunctionCall extends DynExecAction  implements Stackable {

	@Override
	public String getText() {
		return "$$FCall";
	}

	@Override
	public int argsNeeded() {
		return 2;
	}
	
	@Override
	public int returnNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Nodo exec(Ctx c, FunctionCallParams params, Identifier id) throws Exception{
		for(Nodo n:params.childs)
			n.exec(c);
		Stackable fdef=c.dir.get(id.getText());
		fdef.exec(c);
		return null;
	}

}
