package org.symbmath.actions.arith;

import org.symbmath.actions.DynExecAction;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Magnitude;
import org.symbmath.grammar.rules.NNumber;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class OpDiv  extends DynExecAction implements Stackable{

	public NNumber exec(Ctx c, NNumber l, NNumber r) throws Exception{
		return l.div(c, r);
	}
	public Magnitude exec(Ctx c, Magnitude l, Magnitude r){
		return l.div(c,r);
	}
	
	public Magnitude exec(Ctx c, Magnitude l, NNumber r){
		return l.div(c,r);
	}
	
	public Nodo exec(Ctx c, Nodo l, Nodo r) throws Exception{
		return l.div(r);
	}
	
	@Override
	public String getText() {
		return "/";
	}
	@Override
	public int argsNeeded() {
		return 2;
	}
	@Override
	public int returnNumber() {
		return 1;
	}
}
