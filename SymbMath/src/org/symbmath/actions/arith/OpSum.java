package org.symbmath.actions.arith;

import org.symbmath.actions.Derivable;
import org.symbmath.actions.DynExecAction;
import org.symbmath.grammar.InfixHelper.OPExp;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.grammar.rules.Magnitude;
import org.symbmath.grammar.rules.NNumber;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class OpSum extends DynExecAction  implements Stackable, Derivable {

	public NNumber exec(Ctx c, NNumber l, NNumber r) throws Exception{
		return l.sum(c, r);
	}
	public Nodo exec(Ctx c, Nodo l, Nodo r) throws Exception{
		return l.sum(r);
	}

	@Override
	public int argsNeeded() {
		return 2;
	}

	@Override
	public int returnNumber() {
		return 1;
	}

	@Override
	public String getText() {
		return "+";
	}
	@Override
	public Nodo derive(Nodo expresion, Nodo listVar) {
		Nodo ret=new Nodo(SymbMathParser.RULE_expresion);
		ret.subtype=OPExp.Sum;
		for(Nodo n:ret.childs)
			ret.addChild(n.derive(expresion, listVar));
		return ret;
	}
	public Nodo exec(Ctx c, Magnitude l, Magnitude r) throws Exception{
		return l.sum(c,r);
	}
}
