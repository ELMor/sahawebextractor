package org.symbmath.actions.arith;

import org.symbmath.actions.DynExecAction;
import org.symbmath.grammar.InfixHelper;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Expresion;
import org.symbmath.grammar.rules.Identifier;
import org.symbmath.grammar.rules.Magnitude;
import org.symbmath.grammar.rules.NNumber;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class OpCHS extends DynExecAction implements Stackable, InfixHelper {

	public NNumber exec(Ctx c, NNumber r) throws Exception{
		return r.chs(c);
	}
	
	public Nodo exec(Ctx c, Identifier x) throws Exception {
		Expresion e=new Expresion();
		e.subtype=OPExp.Neg;
		e.addChild(x);
		return e;
	}

	public Nodo exec(Ctx c, Expresion eo) throws Exception {
		if(eo.subtype==OPExp.Neg)
			return eo.g(0);
		Expresion e=new Expresion();
		e.subtype=OPExp.Neg;
		e.addChild(eo);
		return e;
	}

	public Nodo exec(Ctx c, Magnitude m) throws Exception {
		return m.chs(c);
	}

	@Override
	public String getText() {
		return "CHS";
	}

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		return 1;
	}

}
