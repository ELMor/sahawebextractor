package org.symbmath.actions;

import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Exceptions.FunctionNotDefined;
import org.symbmath.runtime.Exceptions.TooFewArgs;

public abstract class DynExecAction {
	
	public abstract String getText();
	
	public void exec(Ctx c) throws TooFewArgs, FunctionNotDefined {
		c.dynExec(getText());
	}

	public boolean mustRegister() {
		return true;
	}

	public boolean pushNoExec() {
		return false;
	}

}
