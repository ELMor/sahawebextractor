package org.symbmath.actions.def;

import org.symbmath.actions.DynExecAction;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Identifier;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class OpVariableDef  extends DynExecAction implements Stackable  {

	public Nodo exec(Ctx c, Nodo e, Identifier id){
		c.put(id.getText(), e);
		return null;
	}

	@Override
	public int argsNeeded() {
		return 2;
	}

	@Override
	public int returnNumber() {
		return 0;
	}

	@Override
	public String getText() {
		return "$$DefineVariable";
	}
}
