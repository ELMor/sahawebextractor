package org.symbmath.actions.dict;

import org.symbmath.actions.DynExecAction;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Identifier;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class Sto extends DynExecAction  implements Stackable {

	@Override
	public String getText() {
		return "sto";
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

	@Override
	public int argsNeeded() {
		return 2;
	}

	@Override
	public int returnNumber() {
		return 0;
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	public void exec(Ctx c, Nodo n, Identifier i) {
		c.put(i.getText(),n);
	}
}
