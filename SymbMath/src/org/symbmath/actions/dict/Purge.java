package org.symbmath.actions.dict;

import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Identifier;
import org.symbmath.grammar.rules.NoEvalExpresion;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.runtime.Exceptions.InvalidArgumentException;

public class Purge implements Stackable {

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		return 0;
	}

	@Override
	public String getText() {
		return "purge";
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	@Override
	public void exec(Ctx c) throws Exception {
		c.dynExec(getText());
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

	public Nodo exec(Ctx c, NoEvalExpresion nev) throws InvalidArgumentException{
		if(nev.childs.size()!=1)
			throw new InvalidArgumentException();
		Nodo in=nev.g(0);
		if(in instanceof Identifier){
			c.dir.purge(in.getText());
		}else{
			throw new InvalidArgumentException();
		}
		return null;
	}
}
