package org.symbmath.actions.stack;

import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.runtime.Exceptions.TooFewArgs;

public class Drop implements Stackable {

	@Override
	public String getText() {
		return "drop";
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		return 0;
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	@Override
	public void exec(Ctx c) throws TooFewArgs {
		c.pop();
	}

}
