package org.symbmath;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class TestGrammar {
	public void run(){
		try {		
			Ctx c=new Ctx("Session.ser",true);
			c.debug();
			while(true){
		        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		        System.out.print(":");
		        String linea = br.readLine();
		        if(linea.equals("quit"))
		        	break;
		        try {
			        Nodo n=Nodo.parse(SymbMathParser.RULE_console, linea);
					n.exec(c);
		        }catch(Exception e){
		        	Stackable s=c.get(linea);
		        	if(s!=null)
		        		c.dynExec(linea);
		        }finally {
					c.debug();
				}
			}
			System.out.println("Bye!");
			c.quit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new TestGrammar().run();
	}

}
