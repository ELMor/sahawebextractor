/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaNodosProcedimiento </p>
 * <p>Description: Bean que realiza la consulta en ses_procedimientos para asociar los procedimientos a una clasificacion sin repeticiones.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Jose Vicente Nieto-Marquez Fdez-Medina
 * @version 1.0
 */
public class ConsultaNodosProcedimientos {
    private int sesclass_pkpadre;
    private String scodigo;
    private String sdesc;
    private int resultCount;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 500;

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de registros en Ses_Procedimientos para su asociacion a una clasificacion.
     * Utiliza objetos de la clase NodosProcedimiento para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos NodosProcedimiento, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase NodosProcedimiento.
     * @throws Exception
     */
    public Vector resultado() throws Exception {
        boolean lb_buscar_codigo = false;
        boolean lb_buscar_desc = false;

        // Validaci�n de parametros
        sdesc = sdesc.toUpperCase();
        scodigo = scodigo.toUpperCase();

        if (sesclass_pkpadre <= 0) {
            return (null);
        }

        if ((scodigo == null) || (scodigo.equals(""))) {
            lb_buscar_codigo = false;
        } else {
            lb_buscar_codigo = true;
        }

        if ((sdesc == null) || (sdesc.equals(""))) {
            lb_buscar_desc = false;
        } else {
            lb_buscar_desc = true;
        }

        if ((lb_buscar_codigo == false) && (lb_buscar_desc == false)) {
            return (null);
        }

        String sentBusqueda = new String("");
        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_Nodos = new Vector();
        sentBusqueda = " select  p.PROC_PK,p.PROC_COD,p.PROC_DESC,p.PROC_GARANTIA,p.PROC_MONTO_MAX_EST, p.PROC_TIPO " +
            " from ses_procedimientos p, " + " ( " + " select proc_pk " +
            " from ses_procedimientos p ";

        if (lb_buscar_codigo) {
            scodigo = scodigo + "%";
            sentBusqueda = sentBusqueda + " where  p.PROC_COD like ? ";
        }

        if (lb_buscar_desc) {
            sdesc = sdesc + "%";

            if (lb_buscar_codigo) {
                sentBusqueda = sentBusqueda + " AND p.PROC_DESC_CORTA like ? ";
            } else {
                sentBusqueda = sentBusqueda +
                    " WHERE p.PROC_DESC_CORTA like ? ";
            }
        }

        sentBusqueda = sentBusqueda + "minus " + " select proc_pk " +
            " from ses_clasificaciones " + " where proc_pk is not null " +
            " start with sesclas_pk=" + " (select sesclas_pk " +
            " from ses_clasificaciones " + " where level=(select max(level) " +
            " from ses_clasificaciones " + " start with sesclas_pk= " +
            sesclass_pkpadre +
            " connect by  sesclas_pk=prior sesclas_pkpadre) " +
            "  start with sesclas_pk= " + sesclass_pkpadre +
            " connect by  sesclas_pk=prior sesclas_pkpadre)  " +
            " connect by prior sesclas_pk=sesclas_pkpadre " + " ) q " +
            " where p.PROC_PK=q.PROC_PK ";
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection("db");

        try {
            conn.setAutoCommit(false);

            PreparedStatement st = conn.prepareStatement(sentBusqueda);

            if (lb_buscar_codigo) {
                st.setString(1, scodigo);
            }

            if (lb_buscar_desc) {
                if (lb_buscar_codigo) {
                    st.setString(2, sdesc);
                } else {
                    st.setString(1, sdesc);
                }
            }

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                NodoProcedimiento nodo = new NodoProcedimiento();
                nodo.NodoProcPk = rs.getInt(1);
                nodo.NodoCodigo = rs.getString(2);
                nodo.NodoDesc = rs.getString(3);
                nodo.NodoGarantia = rs.getInt(4);
                nodo.NodoMonto = rs.getDouble(5);
                nodo.NodoProcTipo = rs.getLong(6);

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_Nodos.addElement(nodo);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection("db", conn);
        }

        return vec_Nodos;
    }

    /**
     * Asigna el parametro al atributo sesclass_pkpadre de la clase.
     * @param pkPadre :Long, Padre al cual se le van a asociar los procedimientos.
     * @since 05/02/2003
     * @author JVN
     */
    public void setPkPadre(int pkPadre) {
        if (pkPadre > 0) {
            this.sesclass_pkpadre = pkPadre;
        }
    }

    /**
     * Obtiene el parametro al atributo sesclass_pkpadre de la clase.
     * @return long, sesclass_pkpadre.

     * @since 05/02/2003
     * @author JVN
     */
    public int GetPkPadre() {
        return this.sesclass_pkpadre;
    }

    /**
     * Asigna el parametro al atributo scodigo de la clase.
     * @param codigo :String, Filtro para aplicar al codigo de procedimiento a devolver.
     * @since 05/02/2003
     * @author JVN
     */
    public void setCodigo(String codigo) {
        this.scodigo = codigo;
    }

    /**
     * Obtiene el parametro al atributo scodigo de la clase.
     * @return string, scodigo. Filtro aplicado para el codigo de procedimiento
     * @since 05/02/2003
     * @author JVN
     */
    public String getCodigo() {
        return this.scodigo;
    }

    /**
       * Asigna el parametro al atributo sdesc de la clase.
       * @param desc :String, Filtro para aplicar a la descripcion del procedimiento a devolver.
       * @since 05/02/2003
       * @author JVN
       */
    public void setDesc(String desc) {
        this.sdesc = desc;
    }

    /**
     * Obtiene el parametro al atributo sdesc de la clase.
     * @return String, sdesc.
     * @since 05/02/2003
     * @author JVN
     */
    public String getDesc() {
        return this.sdesc;
    }

    /*

      public static void main(String args[]){
              ConsultaNodosProcedimientos cp=new ConsultaNodosProcedimientos();
              NodoProcedimiento np;
              Vector vp=null;
              cp.setCodigo("12");
              cp.setDesc("");
              cp.setPkPadre(3);
              try{
              vp=cp.resultado();
              }catch(Exception ex){}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: Centro /"+cp.getCodigo()+"/ - HICL /"+cp.GetPkPadre()+"/");
                      }
              else        {
                        Enumeration e = vp.elements();
                        while (e.hasMoreElements()) {
                            np=(NodoProcedimiento)e.nextElement();
                            System.out.println(np.NodoDesc);
                            }
                      }
              }
    */
}
