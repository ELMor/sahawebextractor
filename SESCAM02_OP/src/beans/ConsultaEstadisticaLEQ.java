/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.exceptions.*;

import isf.negocio.*;

import isf.persistencia.SesEstadisticasSync;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaEstadisticaLEQ</p>
 * <p>Descripcion: Bean que realiza diversas estadisticas sobre la tabla Lista de Espera Quir�rgica</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Empresa: Soluziona </p>
 * @author Jos� Vicente Nieto-Marquez
 */
public class ConsultaEstadisticaLEQ {
    private String periodo = "";
    private int tramos = 0;
    private int resultCount;
    public final int MAX_RESULT_SET = 10000; //Maximo n� de registros resultantes por consulta.

    /**
     * metodo que construye la parte de la consulta de estad�sticas com�n a todos los tipos.
     * @return String con la parte del select de la consulta correspondientes a campos parametrizados
    * @autor Fco. Pascual Romero
     */
    private String getConsultaParametros() {
        String str_consulta = "";

        //1. A�ador los totales por tramos
        for (int int_pos = 0; int_pos < tramos; int_pos++) {
            int pos = int_pos + 1;
            str_consulta += (" sum(decode(EST_CATALOG,'" +
            ConsultaEntradasLE.STR_SITUACION[ConsultaEntradasLE.TG] +
            "',decode(c.POSICION," + pos + ",EST_CNT_PAC,0),0)) tramo" + pos +
            ", ");
        }

        // 2. A�adir los totales  por tipos
        for (int int_pos = 2;
                int_pos < (ConsultaEntradasLE.STR_SITUACION.length - 1);
                int_pos++) {
            str_consulta += ("  sum(decode(EST_CATALOG,'" +
            ConsultaEntradasLE.STR_SITUACION[int_pos] +
            "',EST_CNT_PAC,0)) TotalAcActivos" +
            ConsultaEntradasLE.STR_SITUACION[int_pos] + ",");
        }

        // 3. A�adir los campos totales fijos.
        str_consulta += ("  sum(decode(EST_CATALOG,'" +
        ConsultaEntradasLE.STR_SITUACION[ConsultaEntradasLE.TG] +
        "',EST_CNT_PAC,0)) TotalAcActivos, " + "  sum(decode(EST_CATALOG,'" +
        ConsultaEntradasLE.STR_SITUACION[ConsultaEntradasLE.TG] +
        "',EST_CNT_DIAS,0)) TotalDiasActivos, " +
        "  sum(EST_CNT_PAC) TotalPdtes, " +
        "  sum(EST_CNT_DIAS)/sum(EST_CNT_PAC) mediaPdtes ");

        return str_consulta;
    }

    /**
     * M�todo utilizado para la realizaci�n de la consulta de estad�sticas
     * Los resultados se regresan en forma de vector de  objetos de la clase DatosEstadisticasLEQ.
     * uno por cada registro obtenido
     * @param query consulta de estad�sticas a ejecutar
     * @param nparametros n�mero de parametros periodo de la consulta
     * @throws ExceptionSESCAM
     * @throws SQLException
     * @return Vector
     */
    private Vector ejecutarConsultaEstadisticas(String query, int nparametros)
        throws ExceptionSESCAM, SQLException {
        Vector vec_estadistica = new Vector();
        Connection conn = null;
        resultCount = 0;

        Pool ref_pool = null;
        PreparedStatement stm;

        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            stm = conn.prepareStatement(query);

            // establecer los parametros periodo.
            for (int i = 1; i <= nparametros; i++)
                stm.setString(i, periodo);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                resultCount++;

                DatosEstadisticasLEQ estadistica = new DatosEstadisticasLEQ(tramos,
                        rs);

                if (resultCount == MAX_RESULT_SET) {
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.MAX_RESULTSET);
                    throw es;
                }

                vec_estadistica.addElement(estadistica);
            }

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_estadistica;
    }

    /**
     * Metodo que se encarga de la construccion de la consulta y de su ejecucion.
     * Como resultado se obtendra un vector de objetos DatosEstadisticasLEQ,
           * en la que cada uno atender� a un registro resultado de la consulta.
     * @return Vector de objetos de la clase DatosEstadisticasLEQ.
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @autor Jose Vicente Nieto
     */
    public Vector ConsultaTramoCentro() throws ExceptionSESCAM, SQLException {
        //1. Comprobar si el periodo est� definido
        if (periodo.equals("")) {
            //1.1 Si no se define el periodo la consulta no se ejecuta
            return null;
        }

        // 2. Construir la consulta
        String sentBuscarTramoCentro = " select des.descentro, " +
            getConsultaParametros() +
            "  from ses_estadisticas c,centrosescam des " +
            "  where c.EST_TLISTA = 'Q' " + "  and c.EST_PERIODO = ? " +
            "  and c.CODCENTRO = des.CODCENTRO " +
            "   group by des.descentro ";

        //3. Ejecutar la consulta y regresar resultados.
        Vector vec_estadistica = ejecutarConsultaEstadisticas(sentBuscarTramoCentro,
                1);

        return vec_estadistica;
    }

    /**
     * Metodo que se encarga de la construccion de la consulta y de su ejecucion.
     * Utiliza objetos de la clase DatosEstadisticasLEQ para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello,
     * de la consulta.
     * @return Vector de objetos de la clase DatosEstadisticasLEQ.
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @autor Jose Vicente Nieto
     */
    public Vector ConsultaTramoServicio() throws ExceptionSESCAM, SQLException {
        if (periodo.equals("")) {
            return null;
        }

        // 2. Construir la consulta
        String sentBuscarTramoServicio = " select esp.SESESP_DESC, " +
            getConsultaParametros() +
            " from ses_estadisticas c,ses_srv_map srmp, ses_servicios serv,ses_especialidades esp " +
            " where c.EST_TLISTA = 'Q' " + " and c.EST_PERIODO = ? " +
            " and c.SERV_CEN_COD = srmp.SRV_CEN_COD and srmp.SESSRV_COD = serv.SESSRV_COD " +
            " and serv.SESESP_COD = esp.SESESP_COD " +
            " group by esp.SESESP_DESC " + " UNION ALL " +
            " select 'OTRAS ESPECIALIDADES', " + getConsultaParametros() +
            "from ses_estadisticas c " + " where  c.EST_TLISTA = 'Q' " +
            " and c.EST_PERIODO = ? and (c.SERV_CEN_COD not in " +
            "	 (select smrp.SRV_CEN_COD from ses_srv_map smrp where smrp.CENTRO = c.CODCENTRO)) ";

        //3. Ejecutar la consulta y regresar resultados.
        Vector vec_estadistica = ejecutarConsultaEstadisticas(sentBuscarTramoServicio,
                2);

        return vec_estadistica;
    }

    public Vector ConsultaTramoDiagnostico()
        throws ExceptionSESCAM, SQLException {
        //1. Comprobar si el periodo est� definido
        if (periodo.equals("")) {
            return null;
        }

        // 2. Construir la consulta
        String sentBuscarTramoDiag = " select diag.DIAG_DESC, " +
            getConsultaParametros() +
            " from ses_estadisticas c,ses_diag_map sdmp, ses_diagnosticos diag " +
            " where c.EST_TLISTA = 'Q' " + " and c.EST_PERIODO = ? " +
            " and c.DIAG_CEN_COD = sdmp.DIAG_CEN_COD and sdmp.DIAG_PK = diag.DIAG_PK " +
            " group by diag.DIAG_DESC " + " UNION ALL " +
            " select 'OTROS DIAGN�STICOS', " + getConsultaParametros() +
            " from ses_estadisticas c " + " where c.EST_TLISTA = 'Q' " +
            " and c.EST_PERIODO = ? " + " and c.DIAG_CEN_COD not in " +
            " (select smap.DIAG_CEN_COD from ses_diag_map smap where smap.CENTRO = c.CODCENTRO) ";

        //3. Ejecutar la consulta y regresar resultados.
        Vector vec_estadistica = ejecutarConsultaEstadisticas(sentBuscarTramoDiag,
                2);

        return vec_estadistica;
    }

    public Vector DamePeriodos() throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        Vector vec = new Vector();
        Pool ref_pool = null;

        String sentBuscarTecnicas = " select ESYNC_PERIODO " +
            "    from SES_ESTADISTICAS_SYNC " + "    where EST_TLISTA = 'Q' ";

        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sentBuscarTecnicas);

            while (rs.next()) {
                resultCount++;

                SesEstadisticasSync per = new SesEstadisticasSync();
                per.setEsyncPeriodo(rs.getString(1));
                vec.addElement(per);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec;
    }

    /**
        * Asigna el parametro al atributo periodo de la clase.
        * @param sper :String, periodo por el que filtramos la consulta.
        * @since 25/02/2003
        * @author JVN
        */
    /**
     * Obtiene el parametro al atributo periodo de la clase.
     * @return String, periodo del procedimiento.
     * @since 25/02/2003
     * @author JVN
     */
    public String GetPeriodo() {
        return this.periodo;
    }

    public int getTramos() {
        return tramos;
    }

    public void setTramos(int tramos) {
        this.tramos = tramos;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /*
      public static void main(String args[]){
              ConsultaPacienteSolicitud cps=new ConsultaPacienteSolicitud();
              Solilcitud sol;
              Vector vp;
              cps.setNombre("nom");
              cps.setApellido1("");
              cps.setApellido2("");
              cps.setCip("");
              cps.setCentro("");
              cps.setHicl("");
              cps.setOrden("cip");
              try{
                vp=cpb.resultado();
                if (vp==null){
                  System.out.println("No se trajo nada");
                }
                else        {
                  Enumeration e = vp.elements();
                  while (e.hasMoreElements()) {
                    pac1=(Paciente)e.nextElement();
                    System.out.println(pac1);
                  }
                  System.out.println("Cantidad de registros:"+cpb.getResultCount());
                }
              }
              catch(Exception e){
                System.out.println("Error al ejecutar la Select.");
                e.printStackTrace();
              }
              }*/
}
