/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.exceptions.*;

import isf.persistencia.SesEstadisticasSync;

import java.io.*;

import java.sql.*;


/**
 * <p>Clase: ConsultaEstadisticaLEQ</p>
 * <p>Descripcion: Bean que realiza diversas estadisticas sobre la tabla Lista de Espera Quir�rgica</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Empresa: Soluziona </p>
 * @author Jos� Vicente Nieto-Marquez
 */
public class ConsultaTramos {
    private String periodo = "";
    private String CLOB = "[(De 0 a 15 d�as,0,15),(De 16 a 25,16,25),(De 26 a 40,26,40),(De 41 a 90,41,90),(De 91 a 120,91,120),(De 121 a 150,121,150),(De 151 a 180,151,180),(De 181 a 210,181,210)]";
    private String[] descripcion;
    private Integer[] inicio;
    private Integer[] fin;
    private int ntramos = 0;
    private int resultCount;
    public final int MAX_RESULT_SET = 10000; //Maximo n� de registros resultantes por consulta.

    /**
     * Metodo que se encarga de la construccion de la consulta y de su ejecucion.
     * Utiliza objetos de la clase DatosEstadisticasLEQ para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos DatosEstadisticasLEQ, en la que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase DatosEstadisticasLEQ.
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @author Jose Vicente Nieto
     */
    public int CargarCLOB() throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        int i_tramos = 0;
        String[] descrip = null;
        Integer[] pos_inicio = null;
        Integer[] pos_final = null;
        Pool ref_pool = null;
        StringWriter out = new StringWriter();
        PreparedStatement stm;

        if (periodo.equals("")) {
            return -1;
        }

        String sentBusqueda = " select esync_cntramos " +
            "  from ses_estadisticas_sync c " + "  where c.EST_TLISTA = 'Q' " +
            "  and c.ESYNC_PERIODO = ? ";

        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            stm = conn.prepareStatement(sentBusqueda);
            stm.setString(1, periodo);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                i_tramos = rs.getInt(1);
            }

            if (i_tramos > 0) {
                try {
                    SesEstadisticasSync sestsync = new SesEstadisticasSync();
                    String s_where = " ESYNC_PERIODO= '" + periodo + "'" +
                        " AND EST_TLISTA='Q' ";
                    CLOB = sestsync.GetEsyncTramos(out, conn, s_where);
                } catch (Exception e) {
                }
            }

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        int index_inicial = -1;
        int index_final;
        this.setNtramos(i_tramos);
        descrip = new String[i_tramos];

        pos_inicio = new Integer[i_tramos];
        pos_final = new Integer[i_tramos];

        for (int int_pos = 0; int_pos < i_tramos; int_pos++) {
            index_inicial = CLOB.indexOf('(', index_inicial + 1);

            index_final = CLOB.indexOf(',', index_inicial);

            String desc = CLOB.substring(index_inicial + 1, index_final);
            descrip[int_pos] = new String(desc);
            index_inicial = index_final;
            index_final = CLOB.indexOf(',', index_inicial + 1);

            String inicio = CLOB.substring(index_inicial + 1, index_final);
            pos_inicio[int_pos] = new Integer(inicio);
            index_inicial = index_final;
            index_final = CLOB.indexOf(')', index_inicial + 1);

            String fin = CLOB.substring(index_inicial + 1, index_final);
            pos_final[int_pos] = new Integer(fin);
            index_inicial = index_final;
        }

        this.setDescripcion(descrip);
        this.setInicio(pos_inicio);
        this.setFin(pos_final);

        return i_tramos;
    }

    /**
     * Obtiene el valor del atributo result_count de la clase.
     * @return int, numero de filas resultantes de la consulta.
     * @since 31/12/2002
     * @author JVN
     */
    public int getResultCount() {
        return resultCount;
    }

    /**
        * Asigna el parametro al atributo periodo de la clase.
        * @param sper :String, periodo por el que filtramos la consulta.
        * @since 25/02/2003
        * @author JVN
        */
    public void setPerido(String sper) {
        if (!sper.equals("")) {
            this.periodo = sper;
        }
    }

    /**
     * Obtiene el parametro al atributo periodo de la clase.
     * @return String, periodo del procedimiento.
     * @since 25/02/2003
     * @author JVN
     */
    public String GetPeriodo() {
        return this.periodo;
    }

    public Integer[] getFin() {
        return fin;
    }

    public Integer[] getInicio() {
        return inicio;
    }

    public void setInicio(Integer[] inicio) {
        this.inicio = inicio;
    }

    public void setFin(Integer[] fin) {
        this.fin = fin;
    }

    public String[] getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String[] descripcion) {
        this.descripcion = descripcion;
    }

    public int getNtramos() {
        return ntramos;
    }

    public void setNtramos(int ntramos) {
        this.ntramos = ntramos;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /*
      public static void main(String args[]){
              ConsultaPacienteSolicitud cps=new ConsultaPacienteSolicitud();
              Solilcitud sol;
              Vector vp;
              cps.setNombre("nom");
              cps.setApellido1("");
              cps.setApellido2("");
              cps.setCip("");
              cps.setCentro("");
              cps.setHicl("");
              cps.setOrden("cip");
              try{
                vp=cpb.resultado();
                if (vp==null){
                  System.out.println("No se trajo nada");
                }
                else        {
                  Enumeration e = vp.elements();
                  while (e.hasMoreElements()) {
                    pac1=(Paciente)e.nextElement();
                    System.out.println(pac1);
                  }
                  System.out.println("Cantidad de registros:"+cpb.getResultCount());
                }
              }
              catch(Exception e){
                System.out.println("Error al ejecutar la Select.");
                e.printStackTrace();
              }
              }*/
}
