/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import isf.util.log.Log;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaEntradasLEDet </p>
 * <p>Description: Bean que realiza la consulta de entradas en la lista de espera.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Iv�n Enrique Caneri
 * @version 1.0
 */
public class ConsultaEntradasLEPasivo {
    private String centro;
    private String norden;
    private String cq;
    private int resultCount;
    private NegEvento negevent;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 500;

    /**
              * Instancia el objeto de clase NegEvento
              * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
              * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
    */
    public void SetNegEvento(NegEvento evento) {
        negevent = evento;
    }

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de registros de Lista de Espera y de su ejecucion.
     * Utiliza objetos de la clase EntradasLEDet para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos EntradasLEDet, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * Se realizan dos consultas, una para realizar la busqueda de entradas en LESP (Lista de Espera Quirurgia) y
     * otra para realizar la busqueda en la tabla LESPCEX (Lista de Espera de CEX).
     * @return Vector de objetos de la clase EntradasLEDet.
     * @throws Exception
     */
    public Vector resultado() throws Exception {
        Centros c = new Centros();
        Log fichero_log = null;

        boolean lb_buscar_quir = false;
        boolean lb_buscar_cex = false;

        // Validaci�n de parametros
        if ((centro == null) || (centro.equals(""))) {
            return (null);
        }

        if ((norden == null) || (norden.equals(""))) {
            return (null);
        }

        if (cq == null) {
            return (null);
        } else if (cq.equals("Q")) {
            lb_buscar_quir = true;
        } else {
            lb_buscar_cex = true;
        }

        String sentBusqueda = new String("");
        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_EntradasLEDet = new Vector();

        if (lb_buscar_quir == true) {
            sentBusqueda = "select " + "level orden," + "to_char(finc,'" +
                "DD/MM/YYYY" + "') Entrada," + "to_char(fsde,'" + "DD/MM/YYYY" +
                "') FDemora," + "tdes TDemora," + "to_char(fsal,'" +
                "DD/MM/YYYY" + "') Salida ," + "moti Motivo ," + "dpr1 Proc ," +
                "centro," + "norden," + "cip ," + "cega ," + "hicl ," +
                "cias ," + "ape1 ," + "ape2 ," + "nomb ," + "to_char(fnac,'" +
                "DD/MM/YYYY" + "') fnac ," + "sexo ," + "domi ," + "pobl ," +
                "cpos ," + "prov ," + "tel1 ," + "tel2 ," + "tel3 ," +
                "slab ," + "fina ," + "gara ," + "serv ," + "secc ," +
                "acli ," + "cmed ," + "apno_med ," + "cdi1  ," + "ddi1  ," +
                "cdi2  ," + "ddi2  ," + "cpr1  ," + "cpr2  ," + "dpr2  ," +
                "tcir  ," + "tane  ," + "prio  ," + "cinc  ," + "preo  ," +
                "to_char(frep,'" + "DD/MM/YYYY" + "') frep ," +
                "to_char(fcap,'" + "DD/MM/YYYY" + "') fcap ," + "spac  ," +
                "to_char(fder,'" + "DD/MM/YYYY" + "') fder ," + "dder  ," +
                "to_char(fqui,'" + "DD/MM/YYYY" + "') fqui ," + "obse  " +
                "from lesp_histo Start With centro = ? And norden = ? " +
                "Connect By Prior Link = norden";

            //        " from lesp,centrosescam where centro=? and norden=? and centro=codcentro ";
        } else {
            if (lb_buscar_cex == true) {
                sentBusqueda = "select " + "null orden," +
                    "to_char(finclusi,'" + "DD/MM/YYYY" + "') Entrada," +
                    "null FDemora," + "null TDemora," + "to_char(fsalida,'" +
                    "DD/MM/YYYY" + "') Salida ," + "0 Motivo ," +
                    "prestaci_des ||' '||servproc_des Proc ," + "cega centro," +
                    "ncita norden," + "cip ," + "null cega," + "hicl ," +
                    "cias ," + "ape1 ," + "ape2 ," + "nomb ," +
                    "to_char(fnac,'" + "DD/MM/YYYY" + "') fnac ," + "sexo ," +
                    "domi ," + "pobl ," + "cpos ," + "prov ," + "tel1 ," +
                    "tel2 ," + "null tel3 ," + "slab ," + "fina ," +
                    "null gara ," + "servproc_des serv ," + "servreal secc ," +
                    "area_des acli ," + "medipeti cmed ," +
                    "medipeti_des apno_med ," + "null cdi1  ," +
                    "null ddi1  ," + "null cdi2  ," + "null ddi2  ," +
                    "prestaci cpr1  ," + "null cpr2  ," + "null dpr2  ," +
                    "null tcir  ," + "null tane  ," + "priorida prio  ," +
                    "null cinc  ," + "null preo  ," + "null frep ," +
                    "null fcap ," + "null spac  ," + "null fder ," +
                    "null dder  ," + "null fqui ," + "observac obse  ," +
                    "to_char(fechacit,'" + "DD/MM/YYYY" + "') Fechacita " +
                    "from lespcex_histo where cega=? and ncita=?";
            }
        }

        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            PreparedStatement st = conn.prepareStatement(sentBusqueda);

            try {
                if (negevent != null) {
                    negevent.addVar(83, centro);
                    negevent.addVar(84, norden);
                    negevent.addVar(85, cq);
                    negevent.guardar(conn);
                }
            } catch (Exception ex) {
                fichero_log = Log.getInstance();
                fichero_log.warning(
                    " No se ha realizado el evento de consulta de entrada en LESP � LESPCEX con centro � cega " +
                    centro + " y norden o ncita " + norden + " Error -> " +
                    ex.toString());
            }

            st.setInt(1, Integer.parseInt(centro));
            st.setInt(2, Integer.parseInt(norden));

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                EntradaLEPasivo entLEPasivo = new EntradaLEPasivo();
                entLEPasivo.fNOrden = rs.getInt("norden");
                entLEPasivo.fCentro = rs.getInt("centro");
                entLEPasivo.fOrden = rs.getInt("orden");
                entLEPasivo.fEntrada = rs.getString("Entrada");
                entLEPasivo.fFDemora = rs.getString("FDemora");
                entLEPasivo.fTDemora = rs.getInt("TDemora");
                entLEPasivo.fSalida = rs.getString("Salida");
                entLEPasivo.fMotivo = rs.getString("Motivo");
                entLEPasivo.fProc = rs.getString("Proc");
                entLEPasivo.fCip = rs.getString("cip");
                entLEPasivo.fCega = rs.getInt("cega");
                entLEPasivo.fHicl = rs.getInt("hicl");
                entLEPasivo.fCias = rs.getString("cias");
                entLEPasivo.fApe1 = rs.getString("ape1");
                entLEPasivo.fApe2 = rs.getString("ape2");
                entLEPasivo.fNomb = rs.getString("nomb");
                entLEPasivo.fFnac = rs.getString("fnac");
                entLEPasivo.fSexo = rs.getString("sexo");
                entLEPasivo.fDomi = rs.getString("domi");
                entLEPasivo.fPobl = rs.getString("pobl");
                entLEPasivo.fCpos = rs.getInt("cpos");
                entLEPasivo.fProv = rs.getString("prov");
                entLEPasivo.fTel1 = rs.getString("tel1");
                entLEPasivo.fTel2 = rs.getString("tel2");
                entLEPasivo.fTel3 = rs.getString("tel3");
                entLEPasivo.fSlab = rs.getString("slab");
                entLEPasivo.fFina = rs.getString("fina");
                entLEPasivo.fGara = rs.getInt("gara");
                entLEPasivo.fServ = rs.getString("serv");
                entLEPasivo.fSecc = rs.getString("secc");
                entLEPasivo.fAcli = rs.getString("acli");

                try {
                    entLEPasivo.fCmed = new Integer(rs.getString("cmed")).intValue();
                } catch (SQLException e) {
                    entLEPasivo.fCmed = 0;
                }

                entLEPasivo.fApno_med = rs.getString("apno_med");
                entLEPasivo.fCdi1 = rs.getString("cdi1");
                entLEPasivo.fDdi1 = rs.getString("ddi1");
                entLEPasivo.fCdi2 = rs.getString("cdi2");
                entLEPasivo.fDdi2 = rs.getString("ddi2");
                entLEPasivo.fCpr1 = rs.getString("cpr1");
                entLEPasivo.fCpr2 = rs.getString("cpr2");
                entLEPasivo.fDpr2 = rs.getString("dpr2");
                entLEPasivo.fTcir = rs.getString("tcir");
                entLEPasivo.fTane = rs.getString("tane");
                entLEPasivo.fPrio = rs.getString("prio");
                entLEPasivo.fCinc = rs.getString("cinc");
                entLEPasivo.fPreo = rs.getString("preo");
                entLEPasivo.fFrep = rs.getString("frep");
                entLEPasivo.fFcap = rs.getString("fcap");
                entLEPasivo.fSpac = rs.getString("spac");
                entLEPasivo.fFder = rs.getString("fder");
                entLEPasivo.fDder = rs.getString("dder");
                entLEPasivo.fFqui = rs.getString("fqui");
                entLEPasivo.fObse = rs.getString("obse");
                entLEPasivo.fDescentro = c.dameDescCentro(entLEPasivo.fCentro);
                entLEPasivo.fDescentroLarga = c.dameDescCentroLarga(entLEPasivo.fCentro);

                if (lb_buscar_cex == true) {
                    entLEPasivo.fFCita = rs.getString("Fechacita");
                }

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_EntradasLEDet.addElement(entLEPasivo);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_EntradasLEDet;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param pCentro :String, Centro.
     * @since 15/10/2002
     * @author IEC
     */
    public void setCentro(String pCentro) {
        if (pCentro != null) {
            this.centro = pCentro;
        }
    }

    /**
     * Obtiene el valor del atributo centro de la clase.
     * @return String, Centro.
     * @since 15/10/2002
     * @author IEC
     */
    public String getCentro() {
        return centro;
    }

    /**
     * Asigna el parametro al atributo norden de la clase.
     * @param pNOrden :String, NOrden.
     * @since 15/10/2002
     * @author IEC
     */
    public void setNOrden(String pNOrden) {
        if (pNOrden != null) {
            this.norden = pNOrden;
        }
    }

    /**
      * Obtiene el valor del atributo norden de la clase.
      * @return String, NOrden.
      * @since 15/10/2002
      * @author IEC
      */
    public String getNOrden() {
        return norden;
    }

    /**
      * Asigna el parametro al atributo cq de la clase.
      * @param gar_cq :String, Indica si el registro es de LESP ("Q") o de LESPCEX ("C").
      * @since 15/10/2002
      * @author IEC
      */
    public void setCQ(String gar_cq) {
        if (gar_cq != null) {
            this.cq = gar_cq;
        }
    }

    /**
     * Obtiene el valor del atributo cq de la clase.
     * @return String, Indica si el registro es de LESP ("Q") o de LESPCEX ("C").
     * @since 15/10/2002
     * @author IEC
     */
    public String getCQ() {
        return cq;
    }

    /**
     * Obtiene el valor del atributo resultCount de la clase.
     * @return int,  numero de filas resultantes de la consulta.
     * @since 15/10/2002
     * @author IEC
     */
    public int getResultCount() {
        return resultCount;
    }

    /*

      public static void main(String args[]){
              ConsultaEntradasLEPasivo cpb=new ConsultaEntradasLEPasivo();
              EntradaLEPasivo entLEPasivo;
              Vector vp=null;
              cpb.setCentro("207");
              cpb.setNOrden("23323");
              cpb.setCQ("Q");
              try{
              vp=cpb.resultado();
              }catch(Exception ex){}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: Centro /"+cpb.getCentro()+"/ - HICL /"+cpb.getNOrden()+"/");
                      }
              else        {
                        Enumeration e = vp.elements();
                        while (e.hasMoreElements()) {
                            entLEPasivo=(EntradaLEDet)e.nextElement();
                            }
                      }
              }
    */
}
