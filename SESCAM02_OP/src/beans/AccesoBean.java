/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.FicheroClave;

import isf.db.*;

import isf.negocio.*;

import isf.util.*;

import isf.util.log.Log;

import java.sql.*;

import java.util.*;


/**
 * <p>Clase: AccesoBean </p>
 * <p>Description: Bean que lleva el control de acceso de usuarios.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Iv�n Enrique Caneri
 * @version 1.0
 */
public class AccesoBean {
    private String usuario = "";
    private String password = "";
    int iActivo = 0;
    private Hashtable hashPermisos;
    private Hashtable hashEventos;
    private String strSess_id = "";
    private String strSrvRmt = "";
    private long lSessPK = 0;
    private long iCntFallos = 0;
    private final int QMAXFALLOS = 3;

    /**
     * Constructor de la clase AccesoBean.
     */
    public AccesoBean() {
        hashPermisos = new Hashtable();
        hashEventos = new Hashtable();
        this.initEventos();
    }

    /**
     * Obtiene el valor del atributo usuario de la clase.
     * @return String, Usuario de conexion.
     * @since 15/10/2002
     * @author IEC
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Asigna el parametro al atributo usuario de la clase.
     * @param usuario String, Usuario de conexion.
     * @since 15/10/2002
     * @author IEC
     */
    public void setUsuario(String usuario) {
        if (usuario != null) {
            this.usuario = usuario;
        }

        this.usuario = usuario;
    }

    /**
     * Asigna el parametro al atributo password de la clase.
     * @param password String, Clave del usuario de conexion.
     * @since 15/10/2002
     * @author IEC
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Obtiene el valor del atributo password de la clase.
     * @return String,Clave del usuario de conexion.
     * @since 15/10/2002
     * @author IEC
     */
    public String getPassword() {
        return password;
    }

    /**
     * Obtiene el valor del atributo lSessPK de la clase.
     * @return long, PK de la sesscion establecida con el servidor se utiliza,
     * despues de ejecutar initAcceso().
     * @since 10/12/2002
     * @author IEC
     */
    public long getLSessPK() {
        return lSessPK;
    }

    /**
     * Asigna el parametro al atributo lSessPK de la clase.
     * @param plSessPK long, N�mero de la sessi�n establecida.
     * @since 10/12/2002
     * @author IEC
     */
    public void setLSessPK(long plSessPK) {
        this.lSessPK = plSessPK;
    }

    /**
     * Indica si el usuario tiene el permiso necesario para acceder.
     * @param permiso String, Permiso que ha de tener el usuario.
     * @return boolean, true:tiene permiso, � false:no tiene permiso.
     * @since 15/10/2002
     * @author IEC
     */
    public boolean accesoA(String permiso) {
        String chkpermiso = (String) hashPermisos.get(permiso);
		if (chkpermiso != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo que limpia los atributos de la clase.
     */
    public void reset() {
        usuario = null;
        password = null;
        lSessPK = 0;
        hashPermisos.clear();
    }

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de permisos del usuario e inicializa
     * la HashTable de la clase con los mismos.
     * @return boolean, true: si la clave es valida, false: si el usuario no tiene permisos definidos, esta inhabilitado
     * o la clave es invalida.
     * @since 15/10/2002
     * @author IEC
     */
    public int initAcceso() {
        // Validaci�n de parametros
        if ((usuario == null) || (usuario.equals(""))) {
            return -1; //valores nulos
        }

        if ((password == null) || (password.equals(""))) {
            return -1; //valores nulos
        }

        //    if (iCntFallos>=QMAXFALLOS){
        //      return -6; //valores nulos
        //    }
        Connection conn = null;
        hashPermisos.clear();

        Pool ref_pool = null;
        String strPSW = "";
        int iOficina = 0;
        int dias_clave = 0;
        boolean hayFilas = false;
        FicheroClave fc = new FicheroClave();
        Usuarios_clave usu_clave = new Usuarios_clave();
        Vector v = new Vector();
        boolean bBloqueotemporal = false;
        double dBloqueado = 0;
        int iTBloqueo = 0;
        double dTBloqueo = 0;
        String sentBusqueda =
            "select syslogin,syspwd,sys_perm_nombre,activo_sn,oficina_pk, fecha_bloqueo-sysdate bloqueado " +
            "from sys_usu su,sys_perm_rol spr,sys_permisos sp " + "where " +
            "su.sys_rol_pk=spr.sys_rol_pk " +
            "and sp.sys_perm_pk=spr.sys_perm_pk " + "and su.syslogin=?";
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection("db");

        PreparedStatement st;
        ResultSet rs;

        try {
            conn.setAutoCommit(false);
            st = conn.prepareStatement(sentBusqueda);
            st.setString(1, usuario);
            rs = st.executeQuery();

            String strPermiso = "";

            while (rs.next()) {
                hayFilas = true;
                iActivo = rs.getInt("activo_sn");
                iOficina = rs.getInt("oficina_pk");
                strPermiso = rs.getString("sys_perm_nombre");
                dBloqueado = rs.getDouble("bloqueado");

                if (strPermiso != null) {
                    hashPermisos.put(strPermiso, strPermiso);
                }
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
                e.printStackTrace();
            }

            e.printStackTrace();
        } finally {
            ref_pool.freeConnection("db", conn);
        }

        int iRet = 0;

        // Chequeo si existe el usuario y tiene accesos definidos
        Log fichero_log;

        if (hayFilas) {
            TrataClave tC = new TrataClave();

            if (tC.initClave(usuario)) {
                strPSW = tC.getClaveDes();

                if (strPSW.equals(password)) {
                    // OK acceso concedido
                    iRet = 1;
                } else {
                    //System.out.println("Acceso denegado, usuario sin clave.");
                    fichero_log = Log.getInstance();
                    fichero_log.error("Password incorrecta -> Login = " +
                        usuario + " Clave Insertada  = " + password + " ");
                    iRet = -5;
                }
            } else {
                fichero_log = Log.getInstance();
                fichero_log.error(
                    " Usuario no encontrado � no tiene clave definida en el sistema  -> Login = " +
                    usuario + "");

                //System.out.println("");
                iRet = -2;
            }

            if (iRet == 1) {
                if (tC.esClaveCaducada(usuario)) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(
                        " Usuario tiene la clave caducada  -> Login = " +
                        usuario);
                    iRet = -3;
                }
            }
        } else {
            //System.out.println("Acceso denegado, usuario sin permisos definidos.");
            fichero_log = Log.getInstance();
            fichero_log.error(" Usuario inexistente -> Login = " + usuario);
            iRet = -2;
        }

        // Chequeo si est� activado
        if ((iActivo == 0) && (iRet == 0)) {
            fichero_log = Log.getInstance();
            fichero_log.error(" Usuario No Activo -> Login = " + usuario);

            //System.out.println("Acceso denegado usuario inhabilitado");
            iRet = -4;
        }

        // Chequeo si est� bloqueado por fallos reiterados
        if (dBloqueado > 0) {
            fichero_log = Log.getInstance();
            fichero_log.error(
                " Usuario bloqueado por acceso erroneo reiterado -> Login = " +
                usuario);

            //System.out.println("Acceso denegado por fallos reiterados, usuario inhabilitado temporalmente");
            iRet = -6;
        }

        /*Control de intentos fallidos*/
        if ((iRet < 0) && (iRet != -3)) {
            iCntFallos++;

            if (iCntFallos >= QMAXFALLOS) {
                try {
                    sentBusqueda = " update sys_usu set fecha_bloqueo=sysdate+? where syslogin=? ";
                    conn.setAutoCommit(false);
                    st = conn.prepareStatement(sentBusqueda);
                    iTBloqueo = fc.DameTBloqueo();

                    if (iTBloqueo == 0) {
                        iTBloqueo = 10;
                    }

                    dTBloqueo = iTBloqueo * 0.000694;
                    st.setDouble(1, dTBloqueo);
                    st.setString(2, this.usuario);
                    st.executeUpdate();
                    conn.commit();

                    try {
                        st.close();
                    } catch (Exception ex) {
                    }
                } catch (Exception e) {
                    try {
                        conn.rollback();
                    } catch (Exception esql) {
                        e.printStackTrace();
                    }

                    e.printStackTrace();
                } finally {
                    ref_pool.freeConnection("db", conn);
                }
            }
        } else {
            iCntFallos = 0;
        }

        if (iRet == 1) {
            fichero_log = Log.getInstance();
            fichero_log.info("Acceso del Usuario se ha concedido -> Login " +
                usuario);
        }

        /*Creaci�n de la sessi�n establecida con el usuario o rechazo.*/
        NegEventoSession nes = new NegEventoSession();
        nes.setIOficina_pk(iOficina);
        nes.setStrSyslogin(usuario);
        nes.setStrSess_id(this.strSess_id);
        nes.setStrSess_SrvRmt(this.strSrvRmt);
        nes.setIRet(iRet);

        if (!nes.guardar()) {
            System.out.println("Error al crear la sessi�n");
        }

        this.setLSessPK(nes.getISess_pk());

        return iRet;
    }

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de eventos activados y
     * carga en la HashTable de la clase con los mismos.
     * @since 09/12/2002
     * @author IEC
     */
    private boolean initEventos() {
        Connection conn = null;
        hashEventos.clear();

        Pool ref_pool = null;

        /* Carga las variables activas de eventos activos.*/
        String sentBusqueda = "select t.tpoevto_pk||'/'||tpoevto_var_pk strevt  " +
            "from evento_tipo_var v,evento_tipo t " +
            "where v.tpoevto_pk(+)=t.tpoevto_pk " +
            "and (tpoevto_var_act=1)  " + "and tpoevto_act=1 ";
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection("db");

        PreparedStatement st;
        ResultSet rs;

        try {
            conn.setAutoCommit(false);
            st = conn.prepareStatement(sentBusqueda);
            rs = st.executeQuery();

            String strEvt;

            while (rs.next()) {
                strEvt = rs.getString("strevt");

                if (strEvt != null) {
                    hashEventos.put(strEvt, "1");
                }
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
                ref_pool.freeConnection("db", conn);
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
                e.printStackTrace();
            }

            e.printStackTrace();
            ref_pool.freeConnection("db", conn);
        }

        /* Carga los eventos activos.*/
        sentBusqueda = "select distinct t.tpoevto_pk||'/' strevt from evento_tipo t where tpoevto_act=1";

        try {
            conn.setAutoCommit(false);
            st = conn.prepareStatement(sentBusqueda);
            rs = st.executeQuery();

            String strEvt;

            while (rs.next()) {
                strEvt = rs.getString("strevt");

                if (strEvt != null) {
                    hashEventos.put(strEvt, "1");
                }
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
                e.printStackTrace();
            }

            e.printStackTrace();
        } finally {
            ref_pool.freeConnection("db", conn);
        }

        return true;
    }

    synchronized public NegEventoTram getNegEventoTramInstance(int pNegEvento) {
        return (new NegEventoTram(pNegEvento, this.hashEventos,
            this.getLSessPK()));
    }

    synchronized public NegEvento getNegEventoInstance(int pNegEvento) {
        return (new NegEvento(pNegEvento, this.hashEventos, this.getLSessPK()));
    }

    public void setStrSess_id(String strSess_id) {
        this.strSess_id = strSess_id;
    }

    public String getStrSess_id() {
        return strSess_id;
    }

    public void setStrSrvRmt(String strSrvRmt) {
        this.strSrvRmt = strSrvRmt;
    }

    public String getStrSrvRmt() {
        return strSrvRmt;
    }

    //  public static void main(String args[]){
    //AccesoBean acc=new AccesoBean();
    //acc.setUsuario("admin");
    //acc.setPassword("admin");
    //if (acc.initAcceso()>0){
    // if (!acc.accesoA("acceso_LEQ"))
    //   System.out.println("No tiene acceso");
    // else{
    // NegEvento evn=acc.getNegEventoInstance(2);
    // evn.addVar(1,"Prueba 1");
    //  evn.addVar(6,"Prueba 6");
    //   evn.guardar();
    //  }
    // }else{System.out.println("Error al inicliazar");}
    //}
}
