package conf;

import java.io.*;

import java.util.*;


/**
 * <p>Title: FicheroConfiguracion </p>
 * <p>Description: Lee del fichero db.properties las entradas que contiene para su uso en la aplicacion.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author F�lix Alberto Bravo Hermoso
 */
public class FicheroClave {
    private String fichero = "clave";

    /**
     * Metodo que recupera las propiedades del archivo clave.properties
     * @param _dbProfileName
     * @return InputStream que contiene las distintas entradas del fichero.
     */
    private InputStream CargaProperties(String _Prefijo) {
        InputStream is = getClass().getResourceAsStream("/" + _Prefijo +
                ".properties");

        return is;
    }

    /**
     * Metodo que lee el valor de la entrada ncar_clave del fichero de configuracion.
     * @return int, numero maximo de caracteres de la clave.
     */
    public int DameLongitudClave() {
        int ncarclave = 0;
        Hashtable profileList = new Hashtable();
        InputStream is = CargaProperties(fichero);
        Properties dbProps = new Properties();

        try {
            dbProps.load(is);
        } catch (Exception e) {
            System.out.println("No se puede leer el fichero de propiedades");

            return 0;
        }

        profileList.put(fichero, dbProps);

        Properties propObj = (Properties) profileList.get(fichero);

        if (propObj.getProperty("ncar_clave") != null) {
            ncarclave = Integer.parseInt(propObj.getProperty("ncar_clave"));
        }

        return ncarclave;
    }

    /**
     * Metodo que lee el valor de la entrada ncar_clave_min del fichero de configuracion.
     * En esta entrada se indica el numero minimo de caracteres de la clave.
     * @return int, numero minimo de caracteres de la clave.
     */
    public int DameLongitudClaveMin() {
        int ncarclavemin = 0;
        Hashtable profileList = new Hashtable();
        InputStream is = CargaProperties(fichero);
        Properties dbProps = new Properties();

        try {
            dbProps.load(is);
        } catch (Exception e) {
            System.out.println("No se puede leer el fichero de propiedades");

            return 0;
        }

        profileList.put(fichero, dbProps);

        Properties propObj = (Properties) profileList.get(fichero);

        if (propObj.getProperty("ncar_clave_min") != null) {
            ncarclavemin = Integer.parseInt(propObj.getProperty(
                        "ncar_clave_min"));
        }

        return ncarclavemin;
    }

    /**
     * Metodo que lee el valor de la entrada duraclave del fichero de configuracion.
     * @return int, duracion en dias de la clave actual.
     */
    public int DameDuracionClave() {
        int duraclave = 0;
        Hashtable profileList = new Hashtable();
        InputStream is = CargaProperties(fichero);
        Properties dbProps = new Properties();

        try {
            dbProps.load(is);
        } catch (Exception e) {
            System.out.println("No se puede leer el fichero de propiedades");

            return 0;
        }

        profileList.put(fichero, dbProps);

        Properties propObj = (Properties) profileList.get(fichero);

        if (propObj.getProperty("duraclave") != null) {
            duraclave = Integer.parseInt(propObj.getProperty("duraclave"));
        }

        return duraclave;
    }

    /**
     * Metodo que lee el valor de la entrada tbloqueo del fichero de configuracion.
     * @return int, duracion en dias de la clave actual.
     */
    public int DameTBloqueo() {
        int tbloqueo = 0;
        Hashtable profileList = new Hashtable();
        InputStream is = CargaProperties(fichero);
        Properties dbProps = new Properties();

        try {
            dbProps.load(is);
        } catch (Exception e) {
            System.out.println("No se puede leer el fichero de propiedades");

            return 0;
        }

        profileList.put(fichero, dbProps);

        Properties propObj = (Properties) profileList.get(fichero);

        if (propObj.getProperty("tbloqueo") != null) {
            tbloqueo = Integer.parseInt(propObj.getProperty("tbloqueo"));
        }

        return tbloqueo;
    }

    public static void main(String[] args) {
        FicheroClave fc = new FicheroClave();
        System.out.println("Longitud de la clave:" + fc.DameLongitudClave());
        System.out.println("Duracion de la clave:" + fc.DameDuracionClave());
    }
}
