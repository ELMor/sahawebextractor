/*

*   Copyright ( 1999 Uni�n Fenosa International Software Factory,

*   S.A.. All Rights Reserved.

*

*   This software is the confidential and proprietary information of

*   Uni�n Fenosa International Software Factory, S.A.. You shall not

*   disclose such confidential information and shall not accordance

*   with the terms of the license agreement you entered into with

*   I.S.F..

*

*/
package isf.db;

import oracle.jdbc.driver.*;

import oracle.sql.*;

import java.io.*;

import java.sql.*;


/**
   Prop�sito: Tratamiento de los campos CLOBs en bd Oracle.
   @author PSN, JDZ
   @version 2.0
*/
public class CampoCLOBOracle extends CampoCLOB {
    /**
            Recupera el contenido de un campo CLOB
            @param                        OutputStream out         Stream por el que sale el CLOB
            @param                        Connection conexion conexi�n de BD a utilizar
            @param                        String Str_tabla                 Tabla de la que se que quiere leer
            @param                        String Str_campo                 nombre del campo que tiene el CLOB
            @param                        String Str_condicion         condicion que debe cumplir la fila que se quiere modificar
            @return                        void
            @exception                SQLException, Exception
            @since                        29-11-2000

    */
    public void obtenerCampoCLOB(Writer out, Connection conexion,
        String Str_tabla, String Str_campo, String Str_condicion)
        throws SQLException, Exception {
        Statement sentencia = null;
        ResultSet resultado = null;
        int int_i = 0;
        int int_bytesLeidos = 0;
        CLOB CLOB;

        char[] buffer;
        Reader CLOBInputStream = null;
        String Str_sentencia = null;
        int int_numeroFilasProcesadas = 0;

        try {
            sentencia = conexion.createStatement();

            Str_sentencia = "SELECT " + Str_campo + " FROM " + Str_tabla +
                " WHERE " + Str_condicion;
            resultado = sentencia.executeQuery(Str_sentencia);

            if (resultado.next()) {
                CLOB = ((OracleResultSet) resultado).getCLOB(Str_campo);

                Reader CLOBString = CLOB.getCharacterStream();

                buffer = new char[254];

                while ((int_bytesLeidos = CLOBString.read(buffer, 0, 254)) != -1) {
                    out.write(buffer, 0, int_bytesLeidos);
                }
            }
        } catch (SQLException eSql) {
            eSql.printStackTrace();
            throw eSql;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            sentencia.close();

            if (resultado != null) {
                resultado.close();
            }

            if (CLOBInputStream != null) {
                CLOBInputStream.close();
            }
        }
    }
     // obtenerCampoCLOB

    /**
      Recupera el contenido de un campo CLOB y lo inserta en otro
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tablaOrigen                 tabla origen
      @param                String Str_campoOrigen                  nombre del campo origen que tiene el CLOB
      @param                String Str_condicionOrigen         condicion de la fila de origen
      @param                String Str_tablaDestino                 tabla origen
      @param                String Str_campoDestino                  nombre del campo destino que tiene el CLOB
      @param                String Str_condicionDestino         condicion de la fila de origen
      @return                void
      @exception        SQLException, Exception
      @since                29-11-2000
    */

    /*        public void copiarCampoCLOB(        Connection                conexion,
                                                                          String                        Str_tablaOrigen,
                                                                          String                        Str_campoOrigen,
                                                                          String                        Str_condicionOrigen,
                                                                          String                        Str_tablaDestino,
                                                                          String                        Str_campoDestino,
                                                                          String                        Str_condicionDestino) throws SQLException, Exception
          {
                  byte                                   buffer[] = new byte[10 * 1024];
              byte                                   bufferIntermedio[];
                int                                    int_i = 0;
                int                                    int_bytesLeidos = 0;
                CLOB                                  CLOBOrigen;
                CLOB                                   CLOBDestino;
                InputStream                            CLOBInputStream = null;
                ResultSet                              resOrigen = null;
                  ResultSet                                 resDestino = null;
                  Statement                                sentencia = null;
                  OracleCallableStatement mandatoLocal = null;
                  String                                        Str_sentenciaOrigen = null;
                  String                                        Str_sentenciaDestino = null;
                  String                                         Str_sentencia = null;
                  int                                                int_numeroFilasProcesadas = 0;


                    Str_sentenciaOrigen =         "SELECT " + Str_campoOrigen
                                                          +         " FROM " + Str_tablaOrigen
                                                          +        " WHERE " + Str_condicionOrigen;

                  Str_sentenciaDestino =         "SELECT " + Str_campoDestino
                                                          +         " FROM " + Str_tablaDestino
                                                          +        " WHERE " + Str_condicionDestino
                                                          +        " FOR UPDATE";

                try
                {
                    sentencia = conexion.createStatement();

                    Str_sentencia =        "UPDATE " + Str_tablaDestino
                                            +         " SET " + Str_campoDestino
                                            +        " = empty_CLOB()"
                                            +        " WHERE " + Str_condicionDestino;

                    int_numeroFilasProcesadas = sentencia.executeUpdate(Str_sentencia);

                    // Si ya existe el registro -> int_numeroFilasProcesadas > 0
                    if (int_numeroFilasProcesadas > 0)
                    {
                             resOrigen = sentencia.executeQuery(Str_sentenciaOrigen);
                           if (resOrigen.next())
                           {
                              CLOBOrigen = ((OracleResultSet)resOrigen).getCLOB(Str_campoOrigen);
                              CLOBInputStream = CLOBOrigen.getStream();
                              resDestino = sentencia.executeQuery(Str_sentenciaDestino);
                              if (resDestino.next())
                              {
                                         CLOBDestino = ((OracleResultSet)resDestino).getCLOB(Str_campoDestino);
                                         mandatoLocal = (OracleCallableStatement) conexion.prepareCall("begin dbms_lob.write(:1, :2, :3, :4); end;");
                                         mandatoLocal.registerOutParameter(1, OracleTypes.CLOB);
                                         int offset = 1;

                                         while((int_bytesLeidos = CLOBInputStream.read(buffer)) != -1)
                                                  {
                                            mandatoLocal.setCLOB(1, CLOBDestino);
                                            mandatoLocal.setInt(2, int_bytesLeidos);
                                            mandatoLocal.setInt(3, offset);
                                            mandatoLocal.setBytes(4, buffer);
                                            mandatoLocal.executeUpdate();
                                            CLOBDestino = mandatoLocal.getCLOB(1);
                                            offset = offset + int_bytesLeidos;
                                         }
                              }
                           }
                    }
                  }
                catch(SQLException eSql)
                {
                   eSql.printStackTrace();
              throw eSql;
                }
                catch(Exception e)
                {
                        e.printStackTrace();
                   throw e;
                }
                finally
                {
                  sentencia.close();
                          if (mandatoLocal != null)
                                  mandatoLocal.close();
                          if (CLOBInputStream != null)
                                  CLOBInputStream.close();
                          if (resDestino != null)
                            resDestino.close();
                          if (resOrigen != null)
                                  resOrigen.close();
                }
          } // copiarCampoCLOB

    */

    /**
      Actualiza el contenido de un campo CLOB
    @param                String Str_Datos         Cadena de caracteres a almacenar
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tabla        tabla donde est� el campo CLOB
      @param                String Str_campo          nombre del campo que tiene el CLOB
      @param                String Str_condicion condicion de la fila a modificar
      @return                void
      @exception        SQLException, Exception
      @since                16-11-2000

    */
    public void escribirCampoCLOB(String Str_Datos, Connection conexion,
        String Str_tabla, String Str_campo, String Str_condicion)
        throws SQLException, Exception {
        char[] buffer;
        int int_i = 0;
        int int_bytesLeidos = 0;
        CLOB CLOB;
        File fichero;
        StringReader buferdatos;
        InputStream inputStream = null;
        int offset = 1;
        int int_tamanoOptimo = 3000; // tama�o �ptimo 3000 bytes
        Statement sentencia = null;
        Statement sentencia1 = null;

        OracleCallableStatement mandatoLocal = null;
        ResultSet resultado = null;
        String Str_sentencia = null;
        int int_numeroFilasProcesadas = 0;

        try {
            sentencia = conexion.createStatement();
            Str_sentencia = "UPDATE " + Str_tabla + " SET " + Str_campo +
                " = EMPTY_CLOB()" + " WHERE " + Str_condicion;

            int_numeroFilasProcesadas = sentencia.executeUpdate(Str_sentencia);

            // Si ya existe el registro -> int_numeroFilasProcesadas > 0
            if (int_numeroFilasProcesadas > 0) {
                sentencia.close();
                sentencia1 = conexion.createStatement();
                Str_sentencia = "SELECT " + Str_campo + " FROM " + Str_tabla +
                    " WHERE " + Str_condicion + " FOR UPDATE ";

                // Ejecutar la sentencia
                resultado = sentencia1.executeQuery(Str_sentencia);

                if (resultado.next()) {
                    // Obtener el CLOB
                    CLOB = ((OracleResultSet) resultado).getCLOB(Str_campo);
                    buferdatos = new StringReader(Str_Datos);
                    buffer = new char[254];
                    mandatoLocal = (OracleCallableStatement) conexion.prepareCall(
                            "begin dbms_lob.write(:1, :2, :3, :4); end;");
                    mandatoLocal.registerOutParameter(1, OracleTypes.CLOB);

                    while ((int_bytesLeidos = buferdatos.read(buffer, 0, 254)) != -1) {
                        mandatoLocal.setCLOB(1, CLOB);
                        mandatoLocal.setInt(2, int_bytesLeidos);
                        mandatoLocal.setInt(3, offset);

                        String cadena = new String(buffer);
                        mandatoLocal.setString(4, cadena);
                        mandatoLocal.executeUpdate();
                        CLOB = mandatoLocal.getCLOB(1);
                        offset = offset + int_bytesLeidos;
                    }
                }
            }
        } catch (SQLException eSql) {
            eSql.printStackTrace();
            throw eSql;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            //           	  sentencia.close();
            sentencia1.close();

            if (mandatoLocal != null) {
                mandatoLocal.close();
            }

            if (resultado != null) {
                resultado.close();
            }

            if (inputStream != null) {
                inputStream.close();
            }
        }
    }
     // escribirCampoCLOB

    /**
      Actualiza el contenido de un campo CLOB
    @param                ByteArrayOutputStream, outputStream de un multiparte
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tabla        tabla donde est� el campo CLOB
      @param                String Str_campo          nombre del campo que tiene el CLOB
      @param                String Str_condicion condicion de la fila a modificar
      @return                void
      @exception        SQLException, Exception
      @since                16-11-2000

    */

    /*        public void escribirCampoCLOB(
                                                          ByteArrayOutputStream        out,
                                                          Connection                                conexion,
                                                          String                Str_tabla,
                                                          String                Str_campo,
                                                          String                Str_condicion) throws SQLException, Exception
          {
                CLOB                   CLOBDestino;
                OutputStream           CLOBOutputStream = null;
                ResultSet              resDestino = null;
                  Statement                sentencia = null;
                  String                        Str_sentencia = null;
                  int                                int_numeroFilasProcesadas = 0;

                try
                {
                          sentencia = conexion.createStatement();

                          Str_sentencia =        "UPDATE " + Str_tabla
                                                  +         " SET " + Str_campo
                                                  +        " = empty_CLOB()"
                                                  +        " WHERE " + Str_condicion;

                          int_numeroFilasProcesadas = sentencia.executeUpdate(Str_sentencia);

                          // Si ya existe el registro -> int_numeroFilasProcesadas > 0
                          if (int_numeroFilasProcesadas > 0)
                          {
                                  Str_sentencia =        "SELECT " + Str_campo
                                                          +         " FROM " + Str_tabla
                                                          +        " WHERE " + Str_condicion;
    //                                                        +        " FOR UPDATE";

                                  resDestino = sentencia.executeQuery(Str_sentencia);
                      if (resDestino.next())
                      {

                              CLOBDestino = ((OracleResultSet)resDestino).getCLOB(Str_campo);
                              CLOBOutputStream = CLOBDestino.getAsciiOutputStream();
                          out.writeTo(CLOBOutputStream);
                      }
                          }
                  }
                catch(SQLException eSql)
                {
                        eSql.printStackTrace();
                   throw eSql;
                }
                catch(Exception e)
                {
                        e.printStackTrace();
                   throw e;
                }
                finally
                {
                         sentencia.close();
                          if (resDestino != null)
                                  resDestino.close();
                          if (CLOBOutputStream != null)
                                  CLOBOutputStream.close();
                }
      }// escribirCampoCLOB

    */
}
 // CampoCLOBOracle
