/*

*   Copyright ( 1999 Uni�n Fenosa International Software Factory,

*   S.A.. All Rights Reserved.

*

*   This software is the confidential and proprietary information of

*   Uni�n Fenosa International Software Factory, S.A.. You shall not

*   disclose such confidential information and shall not accordance

*   with the terms of the license agreement you entered into with

*   I.S.F..

*

*/
package isf.db;

import java.sql.*;

import java.util.*;


/**
   Prop�sito: Ejecuci�n de updates y deletes masivos
   @author IOF
   @version 1.0
*/
public class Sentence {
    // Atributos
    PreparedStatement pstmt = null;
    Statement statement = null;

    /**
    * Parsea la query, sustituye los parametros por valores y ejecuta la actualizacion contra la BD
    * @param conn Conexi�n a la BD
    * @param query Sentencia a ejecutar
    * @param arguments Vector de objetos con los argumentos de la sentencia
    *@return int Numero de registros afectados por la ejecucion de la sentencia
    * @exception java.lang.SQLException, Exception
    */
    public int execute(Connection conn, String query, Vector arguments)
        throws SQLException, Exception {
        Object argument = null;
        int int_affectedRows = 0;

        // Si no se le pasa argumentos, ejecutamos la sentencia estaticamente
        if ((arguments == null) || (arguments.size() == 0)) {
            // Si el statement ya se uso en una vez anterior hay que cerrarlo antes de crear otro
            if (statement != null) {
                close();
            }

            statement = conn.createStatement();
            int_affectedRows = statement.executeUpdate(query);
        } else {
            // Preparamos la sentencia
            // Si el preparedStatement ya se uso en una vez anterior hay que cerrarlo antes de crear otro
            if (pstmt != null) {
                close();
            }

            pstmt = conn.prepareStatement(query);

            // Recorremos el vector de argumentos y comprobamos sus tipos
            for (int x = 1; x <= arguments.size(); x++) {
                argument = arguments.elementAt(x - 1);

                if (argument instanceof Integer) {
                    pstmt.setInt(x, ((Integer) argument).intValue());
                }

                if (argument instanceof Long) {
                    pstmt.setLong(x, ((Long) argument).longValue());
                }

                if (argument instanceof Boolean) {
                    pstmt.setBoolean(x, ((Boolean) argument).booleanValue());
                }

                if (argument instanceof Float) {
                    pstmt.setFloat(x, ((Float) argument).floatValue());
                }

                if (argument instanceof Double) {
                    pstmt.setDouble(x, ((Double) argument).doubleValue());
                }

                if (argument instanceof String) {
                    pstmt.setString(x, (String) argument);
                }

                if (argument instanceof java.sql.Date) {
                    pstmt.setDate(x, (java.sql.Date) argument);
                }

                if (argument instanceof java.sql.Time) {
                    pstmt.setTime(x, (java.sql.Time) argument);
                }

                if (argument instanceof java.sql.Timestamp) {
                    pstmt.setTimestamp(x, (java.sql.Timestamp) argument);
                }

                if (argument instanceof java.util.Calendar) {
                    pstmt.setDate(x,
                        new java.sql.Date(((java.util.Calendar) argument).getTime()
                                           .getTime()));
                }
            }

            int_affectedRows = pstmt.executeUpdate();
        }

        close();

        return int_affectedRows;
    }

    /**
    * Ejecuta la sentencia que resulta de componer los tres vectores que se pasan por par�metro
    * @param conn Conexi�n a la BD
    * @param query Sentencia a ejecutar
    * @param fields Vector de Strings con los nombres de los campos de la tabla que van en el where
    * @param operators Vector de Strings con los operadores
    * @param values Vector de objetos con los valores de la condicion
    *@return int Numero de registros afectados por la modificacion o eliminacion
    * @exception java.lang.SQLException, Exception
    */
    public int execute(Connection conn, String query, Vector fields,
        Vector operators, Vector values) throws SQLException, Exception {
        String str_aux = null;
        String str_composicion = null;
        int int_affectedRows = 0;

        if (!((fields == null) || (operators == null) || (values == null))) {
            str_aux = query.toUpperCase();

            // Si la sentencia tiene ya la clausula where no la volvemos a poner
            if ((str_aux.indexOf("WHERE") != -1)) {
                // a�adimos la clausula WHERE con las distintas condiciones
                str_composicion = query + " AND ";
            } else {
                // a�adimos a las condiciones iniciales, las de busqueda
                str_composicion = query + " WHERE ";
            }

            for (int int_i = 0; int_i < fields.size(); int_i++) {
                str_composicion = str_composicion + fields.elementAt(int_i) +
                    operators.elementAt(int_i) + " ? ";

                /*
                                                // A�adir comillas simples '
                                                // Si hay comillas simples en el contenido del campo,
                                                // hay que insertar ''
                                                if(values.elementAt(int_i) instanceof String )
                                                {
                                                        str_composicion = str_composicion
                                                                + aniadeComillas((String)values.elementAt(int_i));
                                                }
                                                else if (values.elementAt(int_i) instanceof java.sql.Date)
                                                {
                                                        str_composicion = str_composicion +
                                                                aniadeComillas(((java.sql.Date)values.elementAt(int_i)).toString());
                                                }
                                                else
                                                        str_composicion = str_composicion + values.elementAt(int_i);
                */
                if (int_i < (fields.size() - 1)) {
                    str_composicion = str_composicion + " AND ";
                }
            }
             // del for

            int_affectedRows = execute(conn, str_composicion, values);
        }

        return int_affectedRows;
    }

    /**
    *        Prepara una cadena que contiene comillas simples, para ser
    *        insertada en la B.D., le a�ade otra comilla.
    *        @return: String (Ya actualizado.)
    *        @param: str_valor  String a tratar.
    */
    private String aniadeComillas(String str_valor) {
        int int_j;
        String str_res = "'"; // Resultado a devolver.

        for (int_j = 0; int_j < str_valor.length(); int_j++) {
            str_res = str_res + str_valor.charAt(int_j);

            if (str_valor.charAt(int_j) == '\'') {
                str_res = str_res + "'";
            }
        }
         // for.

        str_res = str_res + "'";

        if (str_res == null) {
            return str_valor;
        } else {
            return str_res;
        }
    }
     // aniadeComillas.	

    /**
    *          Libera los recursos JDBC
    *
    *        @exception SQLException
    */
    public void close() throws SQLException {
        if (pstmt != null) {
            pstmt.close();
        }

        if (statement != null) {
            statement.close();
        }
    }
}
