package isf.db;

import isf.util.log.Log;

import java.io.*;

import java.net.InetAddress;
import java.net.UnknownHostException;

import java.sql.*;

import java.util.*;


/**
 * Esta clase interna representa un pool de conexiones. Crea nuevas conexiones
 * bajo demanda, hasta el n�mero m�ximo (si se especifica).
 * Tambi�n se asegura de que una conexi�n est� abierta antes de devolverla
 * al cliente.
 * @version 2.0
 * @author FRU 26-04-2000
 */
public final class Pool {
    static private Pool instance; // La �nica instancia
    static private PrintWriter log;

    // FRU 3-11-2000
    static private long lastFreeMemory;
    static private Runtime runtime;
    static private int clients = 0;
    static private boolean writeClientInfo = false;
    private Hashtable checkedOut = new Hashtable();

    // Se guarda ademas del contador de conexiones asignadas las propias conexiones asignadas.
    private Hashtable caughtConnections = new Hashtable();
    private Hashtable freeConnections = new Hashtable();

    // Se guarda una hashtable de objetos Properties.
    private Hashtable profileList = new Hashtable();

    // Se guarda una hashtable con el nombre del programa al que esta asignada una conexion
    // (en caso de estarlo)
    private Hashtable assignedToList = new Hashtable();

    /**
     * Constructor protegido. Evita que nadie pueda crear otras instancias de la clase.
     */
    protected Pool() {
        runtime = Runtime.getRuntime();
    }

    /**
     * Retorna la instancia �nica, creando una si es la primera vez
     * que se llama al m�todo.
     *
     * @return Pool La instancia �nica.
     */
    static synchronized public Pool getInstance() {
        if (instance == null) {
            try {
                instance = new Pool();

                PoolLoader refPoolLoader = new PoolLoader();
                InputStream is = refPoolLoader.loadPoolProperties("Pool");
                Properties dbProps = new Properties();

                try {
                    dbProps.load(is);
                } catch (Exception e) {
                    System.out.println(
                        "No se puede leer el fichero de propiedades");

                    return null;
                }

                String str = dbProps.getProperty("WRITE_CLIENT_INFO");

                if (str.compareTo("ON") == 0) {
                    writeClientInfo = true;
                }

                if (str.compareTo("OFF") == 0) {
                    writeClientInfo = false;
                }

                instance.loadInitialConnections();
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        clients++;

        return instance;
    }

    /**
         * Devuelve la conexi�n al pool. Notifica a otros Threads que est�n esperando
         * por una conexi�n, la liberaci�n. Este metodo en el que no se  especifica perfil
         * toma por defecto las propiedades del fichero db.properties.
         *
         * @param con La conexi�n a liberar
         */
    public synchronized void freeConnection(Connection conn) {
        freeConnection("db", conn);
    }

    /**
     * PARA DEPURACION SOLAMENTE!!!!!
     */

    /*
    public void viewAssignedConnections(String _dbProfileName)
    {
       String _assigned = null;
       try
       {
           Vector profileConnections = (Vector)caughtConnections.get(_dbProfileName);
           for (int i = 0; i < profileConnections.size(); i++)
           {
               Connection conn = (Connection) profileConnections.elementAt(i);
               Long key = new Long(conn.hashCode());
               System.out.println(key);
               _assigned = (String)assignedToList.get(key);

               // DEBUG.
               System.out.println(_assigned);
               if (_assigned == null)
               {
                   System.out.println ("Conexi�n " + conn.hashCode() + " no se sabe a quien esta asignada");
               }
               else
               {
                   System.out.println ("Conexi�n " + conn.hashCode() + " asignada a " + _assigned);

               }
           }
       }
       catch(Exception e)
       {
           System.out.println(e.getMessage());
       }
    }
    */

    /**
     * Retorna la conexi�n al pool.
     */
    public synchronized void freeConnection(String _dbProfileName,
        String _assignedTo, Connection conn) {
        String _assigned = null;
        _assigned = (String) assignedToList.get(new Long(conn.hashCode()));

        if (_assigned == null) {
            // System.out.println("No se sabe a quien est� asignada esta conexi�n ...");
        } else {
            if (_assigned.compareTo("_assignedTo") == 0) {
                assignedToList.remove(new Long(conn.hashCode()));
            }
        }

        freeConnection(_dbProfileName, conn);
    }

    /**
         * Devuelve la conexi�n al pool. Notifica a otros Threads que est�n esperando
         * por una conexi�n, la liberaci�n. Hay que especificar a que perfil pertenece
         * la conexi�n a liberar.
         * @param dbProfileName El perfil al que pertenece la conexion a liberar.
         * @param con La conexi�n a liberar
         */
    public synchronized void freeConnection(String _dbProfileName,
        Connection conn) {
        try {
            if (conn != null) {
                //  Quitamos del vector de conexiones asignadas la conexion liberada.
                Vector profileConnections2 = (Vector) caughtConnections.get(_dbProfileName);

                // Se comprueba si la conexion a liberar existe como conexion "asignada"
                // Si no existe el m�todo no hace ninguna actualizaci�n.
                if (profileConnections2.contains(conn)) {
                    profileConnections2.removeElement(conn);

                    Vector profileConnections = (Vector) freeConnections.get(_dbProfileName);

                    // pone la conexion al final del vector de conexiones libres.
                    profileConnections.addElement(conn);

                    // decrementar el numero de conexiones asignadas para el perfil especificado.
                    Integer Int_assignedConnections = (Integer) checkedOut.get(_dbProfileName);
                    int int_assignedConnections = Int_assignedConnections.intValue();
                    int_assignedConnections--;

                    Integer Int_assignedConnectionsMinusOne = new Integer(int_assignedConnections);
                    checkedOut.put(_dbProfileName,
                        Int_assignedConnectionsMinusOne);

                    // Notificar a todos los hilos que esten esperando
                    notifyAll();
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
         * Coge una conexi�n del pool del perfil especificado. Si no hay conexiones abiertas disponibles
         * se crea una nueva conexi�n, a no ser que el m�ximo n�mero de conexiones sea
         * superado. Si la conexi�n disponible ha sido cerrada por la base de datos,
         * se retira del pool y se llama al m�todo recursivamente.
         */
    private Connection getOneConnection(String _dbProfileName) {
        Connection conn = null;

        // Se comprueba si los datos del perfil solicitado ya existen, sino se cargan del fichero
        // de propiedades correspondiente.
        if (!profileList.containsKey(_dbProfileName)) {
            // Recuperamos los par�metros del fichero db.properties.
            PoolLoader refPoolLoader = new PoolLoader();
            InputStream is = refPoolLoader.loadPoolProperties(_dbProfileName);
            Properties dbProps = new Properties();

            try {
                dbProps.load(is);
            } catch (Exception e) {
                System.out.println("No se puede leer el fichero de propiedades");

                return null;
            }

            profileList.put(_dbProfileName, dbProps);

            // Tambien se inicializa el vector de conexiones libres para ese perfil con el vector vacio.
            freeConnections.put(_dbProfileName, new Vector());

            // Tambien se inicializa el contador de conexiones asignadas para ese perfil a cero.
            checkedOut.put(_dbProfileName, new Integer(0));

            // Tambien se inicializa el vector de conexiones asignadas para ese perfil con el vector vacio.
            caughtConnections.put(_dbProfileName, new Vector());
        }

        // Se obtiene el numero maximo de conexiones que se pueden entregar para el perfil especificado.
        int maxConn = 0;
        Properties propObj = (Properties) profileList.get(_dbProfileName);
        maxConn = Integer.parseInt(propObj.getProperty("max"));

        // Se obtiene tambien el numero de conexiones que ya se han entregado para ese perfil.
        // Si ya se han entregado tantas como el numero maximo el thread se echara a dormir hasta que
        // alguien suelte alguna.
        Integer assignedConnections = (Integer) checkedOut.get(_dbProfileName);
        int int_assignedConnections = assignedConnections.intValue();

        // Se obtiene el vector de conexiones libres perteneciente al perfil especificado.
        Vector profileFreeConnections = (Vector) freeConnections.get(_dbProfileName);

        if (profileFreeConnections.size() > 0) {
            // Toma la primera conexi�n del Vector
            // para simular un uso en round-robin
            conn = (Connection) profileFreeConnections.firstElement();
            profileFreeConnections.removeElementAt(0);

            try {
                conn.rollback();
            } catch (SQLException e) {
                // Si el rollback falla la conexi�n se cierra. (FRU 3-10-2000)
                try {
                    conn.close();
                } catch (SQLException sqlEx) {
                }

                // Se prueba de nuevo recursivamente
                conn = getOneConnection(_dbProfileName);
            }
        }
        // si no hay conexiones libres o el numero de conexiones asignadas no ha llegado al maximo
        // permitido para este perfil, se crea una nueva conexi�n fisica.
        else if ((maxConn == 0) || (int_assignedConnections < maxConn)) {
            conn = newConnection(_dbProfileName);
        }

        if (conn != null) {
        } else {
            try {
                wait();
            } catch (InterruptedException e) {
            }

            conn = getOneConnection(_dbProfileName);
        }

        return conn;
    }

    /**
     * Pide una conexi�n al pool. Como no se especifica perfil se cogen las propiedades
     * del fichero db.properties.
     * @return un objeto Connection
     */
    public synchronized Connection getConnection() {
        return getConnection("db");
    }

    /*
     * Pide una conexion al pool indicandole de que base de datos la quiere (dbProfileName) y
     * que programa para que programa o clase.
     */
    public synchronized Connection getConnection(String _dbProfileName,
        String _assignedTo) {
        Connection dbConn = getConnection(_dbProfileName);
        assignedToList.put(new Long(dbConn.hashCode()), _assignedTo);

        // DEBUG
        // System.out.println(dbConn.hashCode() + _assignedTo);
        return dbConn;
    }

    /**
     * Pide una conexi�n al pool.
     * @param _dbProfileName Nombre del perfil seleccionado.
     * @return un objeto Connection
     */
    public synchronized Connection getConnection(String _dbProfileName) {
        Connection conn = null;

        while ((conn = getOneConnection(_dbProfileName)) == null) {
        }

        // Incrementa el contador de conexiones asignadas para el perfil especificado.
        Integer Int_assignedConnections = (Integer) checkedOut.get(_dbProfileName);
        int int_assignedConnections = Int_assignedConnections.intValue();
        int_assignedConnections++;

        Integer Int_assignedConnectionsMinusOne = new Integer(int_assignedConnections);
        checkedOut.put(_dbProfileName, Int_assignedConnectionsMinusOne);

        // Guarda la conexion que se va a entregar en el vector de conexiones asignadas.
        Vector profileConnections = (Vector) caughtConnections.get(_dbProfileName);
        profileConnections.addElement(conn);

        return conn;
    }

    /**
     * Crea una nueva conexi�n a una BD con un user y password especificado en el fichero
     * de propiedades especificado.
     */
    private Connection newConnection(String _dbProfileName) {
        Connection conn = null;

        try {
            Properties profileObject = (Properties) profileList.get(_dbProfileName);

            Class.forName(profileObject.getProperty("driver"));

            // Se escribe en el fichero de log correspondiente.
            String cadena = " Driver : " + profileObject.getProperty("driver") +
                " URL : " + profileObject.getProperty("url") + " User : " +
                profileObject.getProperty("user") + " N� max : " +
                profileObject.getProperty("max") + " Encriptacion cliente : " +
                profileObject.getProperty("oracle.net.encryption_client") +
                " Encr.Tipo : " +
                profileObject.getProperty("oracle.net.encryption_types_client") +
                "";

            //	        conn = DriverManager.getConnection(profileObject.getProperty ("url"), profileObject.getProperty ("user"), profileObject.getProperty ("password"));
            conn = DriverManager.getConnection(profileObject.getProperty("url"),
                    profileObject);

            Log fichero_log;
            fichero_log = Log.getInstance();
            fichero_log.info(cadena);
            fichero_log.info(" Conexion creada correctamente");

            // Le configuramos el formato de las fechas
            estableceFormatoFechas(conn, profileObject.getProperty("driver"),
                profileObject.getProperty("formato_fechas"));
            fichero_log.info(" La conexion ha ocupado : " +
                ((lastFreeMemory - runtime.freeMemory()) / 1024) + "Kb");

            // Se almacena informaci�n asociada a la conexi�n.
            if (writeClientInfo) {
                writeConnectionInformation(conn);
            }

            //
        } catch (SQLException e) {
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }

        return conn;
    }

    /*
     * Este m�todo devuelve una secuencia para la base de datos.
     * Es necesario que las secuencias sean gestionadas por progamaci�n,
     * sin necesidad de que el gestor genere la secuencias. Para esto,
     * solo es necesario tener una tabla en la base de datos denominada
     * SEQUENCE, que contiene dos atributos:
     *                name : varchar2(200) -> Corresponde con el nombre de la clase
     *                                                            que se hace persistente.
     *                value: number(10)   -> Corresponde con el valor siguiente de
     *                                                           la secuencia.
    * @param         name        El nombre de la tabla de la que queremos sacar la
    * @param   name        secuencia.
    * @return         El valor de la secuncia para esa tabla. Este valor ser� uno
    * @return         si la secuencia no exist�a previamente, pasando a estar
    * @return  creada.
    */
    public long getSequence(String name) throws SQLException {
        long sequence;
        Connection myConnection;
        Statement st;
        ResultSet rs;

        sequence = 0;
        myConnection = this.getConnection();
        myConnection.setAutoCommit(false);
        st = myConnection.createStatement();
        rs = st.executeQuery("SELECT * FROM CLAVE " + "WHERE CLAVE_NOMBRE = '" +
                name + "' " + "FOR UPDATE");

        if (rs.next()) {
            sequence = rs.getLong("CLAVE_NUMERO_ACTUAL") + 1;
            st.executeUpdate("UPDATE CLAVE " + "SET CLAVE_NUMERO_ACTUAL = " +
                (sequence) + " " + "WHERE CLAVE_NOMBRE = '" + name + "'");
        } else {
            sequence = 1;
            st.executeUpdate("INSERT INTO CLAVE " +
                "(CLAVE_NOMBRE, CLAVE_NUMERO_ACTUAL) " + "VALUES ('" + name +
                "', " + (sequence) + ")");
        }

        myConnection.commit();
        rs.close();
        st.close();
        this.freeConnection(myConnection);

        return sequence;
    }

    /*
     * Este m�todo devuelve una secuencia para la base de datos.
     * Es necesario que las secuencias sean gestionadas por progamaci�n,
     * sin necesidad de que el gestor genere la secuencias. Para esto,
     * solo es necesario tener una tabla en la base de datos denominada
     * SEQUENCE, que contiene dos atributos:
     *                name : varchar2(200) -> Corresponde con el nombre de la clase
     *                                                            que se hace persistente.
     *                value: number(10)   -> Corresponde con el valor siguiente de
     *                                                           la secuencia.
    * @param         name        El nombre de la tabla de la que queremos sacar la
    * @param   name        secuencia.
     * @param   profile        Perfil que usar� para la conexion.
    * @return         El valor de la secuncia para esa tabla. Este valor ser� uno
    * @return         si la secuencia no exist�a previamente, pasando a estar
    * @return  creada.
    */
    public synchronized long getSequence(String name, String profile)
        throws SQLException {
        long sequence;
        Connection myConnection;
        Statement st;
        ResultSet rs;

        sequence = 0;
        myConnection = this.getConnection(profile);
        myConnection.setAutoCommit(false);
        st = myConnection.createStatement();
        rs = st.executeQuery("SELECT * FROM CLAVE " +
                "WHERE CLAVE_NOMBRE LIKE '" + name + "' ");

        if (rs.next()) {
            sequence = rs.getLong("CLAVE_NUMERO_ACTUAL") + 1;
            st.executeUpdate("UPDATE CLAVE " + "SET CLAVE_NUMERO_ACTUAL = " +
                (sequence) + " " + "WHERE CLAVE_NOMBRE LIKE '" + name + "'");
        } else {
            sequence = 1;
            st.executeUpdate("INSERT INTO CLAVE " +
                "(CLAVE_NOMBRE, CLAVE_NUMERO_ACTUAL) " + "VALUES ('" + name +
                "', " + (sequence) + ")");
        }

        myConnection.commit();
        rs.close();
        st.close();
        this.freeConnection(profile, myConnection);

        return sequence;
    }

    /**
     * Elimina fisicamente una conexion, este o no asignada . Se busca
     * esa conexi�n basandose en el hashCode del objeto.
     *
     */
    public void deleteConnection(int hashCode, String dbProfileName,
        boolean isFree) {
        try {
            boolean found = false;

            if (isFree == false) //  conexiones asignadas.
             {
                int i = 0;
                Vector assignedConnections = (Vector) caughtConnections.get(dbProfileName);

                while ((i < assignedConnections.size()) && (!found)) {
                    Connection conn = (Connection) assignedConnections.elementAt(i);

                    if (hashCode == conn.hashCode()) {
                        found = true;

                        // System.out.println("Cerrando conexion ...." + conn.hashCode());
                        conn.close();
                    } else {
                        i++;
                    }
                }

                if (found) {
                    assignedConnections.removeElementAt(i);

                    // System.out.println("Eliminando conexion ....");
                    // decrementar el numero de conexiones asignadas para el perfil especificado.
                    Integer Int_assignedConnections = (Integer) checkedOut.get(dbProfileName);
                    int int_assignedConnections = Int_assignedConnections.intValue();
                    int_assignedConnections--;

                    Integer Int_assignedConnectionsMinusOne = new Integer(int_assignedConnections);
                    checkedOut.put(dbProfileName,
                        Int_assignedConnectionsMinusOne);
                }
            } else // conexiones libres.
             {
                int j = 0;
                Vector freeConns = (Vector) freeConnections.get(dbProfileName);

                while ((j < freeConns.size()) && (!found)) {
                    Connection conn = (Connection) freeConns.elementAt(j);

                    if (hashCode == conn.hashCode()) {
                        found = true;

                        // System.out.println("Cerrando conexion ...." + conn.hashCode());
                        conn.close();
                    } else {
                        j++;
                    }
                }

                if (found) {
                    freeConns.removeElementAt(j);

                    // System.out.println("Eliminando conexion ....");
                } else {
                    // System.out.println("La conexi�n no existe ...");
                }
            }
        } catch (Exception e) {
        }
    }

    /**
    * Retorna un vector con todos los objetos conexion que est�n libres en el pool.
    * @return Vector
    */
    public Vector getFreeConnections(String _dbProfileName) {
        return (Vector) freeConnections.get(_dbProfileName);
    }

    /**
    *
    *
    */
    public Hashtable getClassNamesOfAssignedConnections() {
        return assignedToList;
    }

    /**
    * Retorna un vector con todos los objetos conexion que est�n asignados por el pool.
    * @return Vector
    */
    public Vector getAssignedConnections(String _dbProfileName) {
        return (Vector) caughtConnections.get(_dbProfileName);
    }

    /**
    * Retorna la tabla hash con la lista de objetos Properties identificados por su clave
    * @return Hashtable
    */
    public Hashtable getProfileList() {
        return profileList;
    }

    /**
    * Este metodo solamente se invoca la primera vez que se crea el objeto Pool
    *
    *
    *
    */
    private void loadInitialConnections() {
        PoolLoader refPoolLoader = new PoolLoader();
        InputStream is = refPoolLoader.loadPoolProperties("InitialConnections");
        Properties dbProps = new Properties();
        String cadena = "";

        try {
            dbProps.load(is);
        } catch (Exception e) {
            System.out.println(
                "No se puede leer el fichero de propiedades -InitialConnections.properties-");
            System.exit(0);
        }

        Enumeration keyList = dbProps.propertyNames();

        while (keyList.hasMoreElements()) {
            String key = (String) keyList.nextElement();
            int initConn = 0;
            initConn = Integer.parseInt(dbProps.getProperty(key));

            ///
            Connection conn = null;

            // Recuperamos los par�metros del fichero .properties correspondiente
            // al perfil .
            InputStream inputStream = refPoolLoader.loadPoolProperties(key);
            Properties profileProps = new Properties();

            try {
                profileProps.load(inputStream);
            } catch (Exception e) {
                System.out.println("No se puede leer el fichero de propiedades");
                System.exit(0);
            }

            profileList.put(key, profileProps);

            // Tambien se inicializa el vector de conexiones libres para ese perfil con el vector vacio.
            freeConnections.put(key, new Vector());

            // Tambien se inicializa el contador de conexiones asignadas para ese perfil a cero.
            checkedOut.put(key, new Integer(0));

            // Tambien se inicializa el vector de conexiones asignadas para ese perfil con el vector vacio.
            caughtConnections.put(key, new Vector());

            // Se obtiene el numero maximo de conexiones que se pueden entregar para el perfil especificado.
            int maxConn = 0;
            maxConn = Integer.parseInt(profileProps.getProperty("max"));

            // FOR DEBUG ONLY
            // System.out.println(initConn);
            if (initConn > maxConn) {
                System.out.println(
                    "El n�m. de conexiones iniciales no puede ser mayor que el n�m. maximo de conexiones");
                System.exit(0);
            }

            // Se obtiene el vector de conexiones libres perteneciente al perfil especificado.
            Vector profileFreeConnections = (Vector) freeConnections.get(key);

            try {
                Class.forName(profileProps.getProperty("driver"));
            } catch (ClassNotFoundException e) {
                System.out.println(e.toString());
            }

            try {
                cadena = " Driver : " + profileProps.getProperty("driver") +
                    " URL : " + profileProps.getProperty("url") + " User : " +
                    profileProps.getProperty("user") + " N� max : " +
                    profileProps.getProperty("max") +
                    " Encriptacion cliente : " +
                    profileProps.getProperty("oracle.net.encryption_client") +
                    " Encr.Tipo : " +
                    profileProps.getProperty(
                        "oracle.net.encryption_types_client") + "";

                for (int i = 0; i < initConn; i++) {
                    // Se escribe en el fichero de log correspondiente.
                    // FRU 3-11-2000
                    lastFreeMemory = runtime.freeMemory();

                    // conn = DriverManager.getConnection(profileProps.getProperty ("url"), profileProps.getProperty ("user"), profileProps.getProperty ("password"));
                    conn = DriverManager.getConnection(profileProps.getProperty(
                                "url"), profileProps);

                    Log fichero_log;
                    fichero_log = Log.getInstance();
                    fichero_log.info(cadena);
                    fichero_log.info(" Conexion creada satisfactoriamente");
                    fichero_log.info(" La conexion ha ocupado : " +
                        ((lastFreeMemory - runtime.freeMemory()) / 1024) +
                        "Kb");

                    // Le configuramos el formato de las fechas
                    estableceFormatoFechas(conn,
                        profileProps.getProperty("driver"),
                        profileProps.getProperty("formato_fechas"));

                    //
                    if (writeClientInfo) {
                        writeConnectionInformation(conn);
                    }

                    profileFreeConnections.addElement(conn);
                }
            } catch (SQLException sqle) {
                Log fichero_log;
                fichero_log = Log.getInstance();
                fichero_log.info(cadena);
                fichero_log.criticalError(" Error al crear Conexion :" +
                    sqle.toString() + " ");
            }
        }
    }

    /*
    *
    */
    private void writeConnectionInformation(Connection dbConn) {
        ResultSet rs = null;
        String outSQL = null;
        CallableStatement st = null;
        final String MODULE_NAME = "Adm.de Contratos";
        final String ACTION = "New connection created";
        String CLIENT_INFO = "";

        try {
            outSQL = dbConn.nativeSQL(
                    "{call dbms_application_info.set_module(?,?)}");
            st = dbConn.prepareCall(outSQL);
            st.setString(1, MODULE_NAME);
            st.setString(2, "");
            rs = st.executeQuery();

            outSQL = dbConn.nativeSQL(
                    "{call dbms_application_info.set_action(?)}");
            st = dbConn.prepareCall(outSQL);
            st.setString(1, ACTION);
            rs = st.executeQuery();

            CLIENT_INFO = InetAddress.getLocalHost().toString();
            outSQL = dbConn.nativeSQL(
                    "{call dbms_application_info.set_client_info(?)}");
            st = dbConn.prepareCall(outSQL);
            st.setString(1, CLIENT_INFO);
            rs = st.executeQuery();
        } catch (UnknownHostException e) {
        } catch (SQLException e) {
        }
    }

    /**
     * @param         name        El nombre de la tabla de la que queremos sacar la
     * @param   name        secuencia.
     * @return         El valor de la secuncia para esa tabla. Este valor ser� uno
     */
    private void estableceFormatoFechas(Connection con, String str_driver,
        String str_formatoFechas) throws SQLException {
        Statement sentencia = null;

        if (con != null) {
            // Compruebo que conexi�n se est� realizando, buscando en la cadena del driver la
            // palabra Oracle, en ese caso, evidentemente se tratar� de una conexi�n Oracle, en caso
            // contrario ser� Sql Server
            sentencia = con.createStatement();

            if (str_driver.indexOf("oracle") != -1) //Oracle
             {
                sentencia.execute("ALTER SESSION set NLS_DATE_FORMAT=" +
                    str_formatoFechas);
            } else //Sql Server
             {
                sentencia.execute("SET DATEFORMAT " + str_formatoFechas);
            }

            sentencia.close();
        }
    }
}
