//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: Formato
// Table: FORMATO
// Package: isf.db
//////////////////////////////////////////////////////////
// Package declaration
package isf.db;

import isf.db.Pool;

import isf.persistence.SQLLanguageHelper;

// Imports
import java.sql.*;

import java.util.Vector;


/**
 * Persistent class for table:FORMATO
 * Columns for the table:
 *         (contenido - STRING),
 *         (lanzador - STRING),
 *         (id_formato - STRING),
 *         (tipo - STRING),
 *         (formato_descripcion - STRING),
 *         (ciclo_depuracion - LONG),
 *         (ciclo_vigor - LONG),
 *         (num_documento - LONG),
 *         (fecha_creacion - DATE),
 *         (vigor - BOOLEAN),
 * Primary columns for the table:
 *         (id_formato - STRING)
 */
public class Formato {
    // Properties
    private String contenido;
    private boolean contenidoNull = false;
    private boolean contenidoModified = false;
    private String lanzador;
    private boolean lanzadorNull = false;
    private boolean lanzadorModified = false;
    private String idFormato;
    private boolean idFormatoNull = false;
    private boolean idFormatoModified = false;
    private String tipo;
    private boolean tipoNull = false;
    private boolean tipoModified = false;
    private String formatoDescripcion;
    private boolean formatoDescripcionNull = false;
    private boolean formatoDescripcionModified = false;
    private long cicloDepuracion;
    private boolean cicloDepuracionNull = false;
    private boolean cicloDepuracionModified = false;
    private long cicloVigor;
    private boolean cicloVigorNull = false;
    private boolean cicloVigorModified = false;
    private long numDocumento;
    private boolean numDocumentoNull = false;
    private boolean numDocumentoModified = false;
    private java.sql.Date fechaCreacion;
    private boolean fechaCreacionNull = false;
    private boolean fechaCreacionModified = false;
    private boolean vigor;
    private boolean vigorNull = false;
    private boolean vigorModified = false;

    // Access Method

    /** Contenido
     * Get the value of the property Contenido.
     * The column for the database is 'CONTENIDO'Contenido.
     */
    public String getContenido() {
        if (contenido == null) {
            return "";
        }

        if (contenido.compareTo("null") == 0) {
            return "";
        }

        return contenido;
    }

    /** Contenido
     * Set the value of the property Contenido.
     * The column for the database is 'CONTENIDO'Contenido.
     */
    public void setContenido(String _contenido) {
        this.contenido = _contenido;
        this.contenidoModified = true;
        this.contenidoNull = false;
    }

    /** Contenido
     * Set Null the value of the property Contenido.
     * The column for the database is 'CONTENIDO'Contenido.
     */
    public void setNullContenido() {
        this.contenido = null;
        this.contenidoModified = true;
        this.contenidoNull = true;
    }

    /** Lanzador
     * Get the value of the property Lanzador.
     * The column for the database is 'LANZADOR'Lanzador.
     */
    public String getLanzador() {
        if (lanzador == null) {
            return "";
        }

        if (lanzador.compareTo("null") == 0) {
            return "";
        }

        return lanzador;
    }

    /** Lanzador
     * Set the value of the property Lanzador.
     * The column for the database is 'LANZADOR'Lanzador.
     */
    public void setLanzador(String _lanzador) {
        this.lanzador = _lanzador;
        this.lanzadorModified = true;
        this.lanzadorNull = false;
    }

    /** Lanzador
     * Set Null the value of the property Lanzador.
     * The column for the database is 'LANZADOR'Lanzador.
     */
    public void setNullLanzador() {
        this.lanzador = null;
        this.lanzadorModified = true;
        this.lanzadorNull = true;
    }

    /** IdFormato
     * Get the value of the property IdFormato.
     * The column for the database is 'ID_FORMATO'IdFormato.
     */
    public String getIdFormato() {
        if (idFormato == null) {
            return "";
        }

        if (idFormato.compareTo("null") == 0) {
            return "";
        }

        return idFormato;
    }

    /** IdFormato
     * Set the value of the property IdFormato.
     * The column for the database is 'ID_FORMATO'IdFormato.
     */
    public void setIdFormato(String _idFormato) {
        this.idFormato = _idFormato;
        this.idFormatoModified = true;
        this.idFormatoNull = false;
    }

    /** IdFormato
     * Set Null the value of the property IdFormato.
     * The column for the database is 'ID_FORMATO'IdFormato.
     */
    public void setNullIdFormato() {
        this.idFormato = null;
        this.idFormatoModified = true;
        this.idFormatoNull = true;
    }

    /** Tipo
     * Get the value of the property Tipo.
     * The column for the database is 'TIPO'Tipo.
     */
    public String getTipo() {
        if (tipo == null) {
            return "";
        }

        if (tipo.compareTo("null") == 0) {
            return "";
        }

        return tipo;
    }

    /** Tipo
     * Set the value of the property Tipo.
     * The column for the database is 'TIPO'Tipo.
     */
    public void setTipo(String _tipo) {
        this.tipo = _tipo;
        this.tipoModified = true;
        this.tipoNull = false;
    }

    /** Tipo
     * Set Null the value of the property Tipo.
     * The column for the database is 'TIPO'Tipo.
     */
    public void setNullTipo() {
        this.tipo = null;
        this.tipoModified = true;
        this.tipoNull = true;
    }

    /** FormatoDescripcion
     * Get the value of the property FormatoDescripcion.
     * The column for the database is 'FORMATO_DESCRIPCION'FormatoDescripcion.
     */
    public String getFormatoDescripcion() {
        if (formatoDescripcion == null) {
            return "";
        }

        if (formatoDescripcion.compareTo("null") == 0) {
            return "";
        }

        return formatoDescripcion;
    }

    /** FormatoDescripcion
     * Set the value of the property FormatoDescripcion.
     * The column for the database is 'FORMATO_DESCRIPCION'FormatoDescripcion.
     */
    public void setFormatoDescripcion(String _formatoDescripcion) {
        this.formatoDescripcion = _formatoDescripcion;
        this.formatoDescripcionModified = true;
        this.formatoDescripcionNull = false;
    }

    /** FormatoDescripcion
     * Set Null the value of the property FormatoDescripcion.
     * The column for the database is 'FORMATO_DESCRIPCION'FormatoDescripcion.
     */
    public void setNullFormatoDescripcion() {
        this.formatoDescripcion = null;
        this.formatoDescripcionModified = true;
        this.formatoDescripcionNull = true;
    }

    /** CicloDepuracion
     * Get the value of the property CicloDepuracion.
     * The column for the database is 'CICLO_DEPURACION'CicloDepuracion.
     */
    public long getCicloDepuracion() {
        return cicloDepuracion;
    }

    /** CicloDepuracion
     * Set the value of the property CicloDepuracion.
     * The column for the database is 'CICLO_DEPURACION'CicloDepuracion.
     */
    public void setCicloDepuracion(long _cicloDepuracion) {
        this.cicloDepuracion = _cicloDepuracion;
        this.cicloDepuracionModified = true;
        this.cicloDepuracionNull = false;
    }

    /** CicloDepuracion
     * Set Null the value of the property CicloDepuracion.
     * The column for the database is 'CICLO_DEPURACION'CicloDepuracion.
     */
    public void setNullCicloDepuracion() {
        this.cicloDepuracion = 0;
        this.cicloDepuracionModified = true;
        this.cicloDepuracionNull = true;
    }

    /** CicloDepuracion
     * Sumatory of value of the property CicloDepuracion.
     * The column for the database is 'CICLO_DEPURACION'CicloDepuracion.
     */
    static public double sumCicloDepuracion(Connection _connection,
        String _where) throws SQLException {
        return sumByCriteria(_connection, _where, "CICLO_DEPURACION");
    }

    /** CicloVigor
     * Get the value of the property CicloVigor.
     * The column for the database is 'CICLO_VIGOR'CicloVigor.
     */
    public long getCicloVigor() {
        return cicloVigor;
    }

    /** CicloVigor
     * Set the value of the property CicloVigor.
     * The column for the database is 'CICLO_VIGOR'CicloVigor.
     */
    public void setCicloVigor(long _cicloVigor) {
        this.cicloVigor = _cicloVigor;
        this.cicloVigorModified = true;
        this.cicloVigorNull = false;
    }

    /** CicloVigor
     * Set Null the value of the property CicloVigor.
     * The column for the database is 'CICLO_VIGOR'CicloVigor.
     */
    public void setNullCicloVigor() {
        this.cicloVigor = 0;
        this.cicloVigorModified = true;
        this.cicloVigorNull = true;
    }

    /** CicloVigor
     * Sumatory of value of the property CicloVigor.
     * The column for the database is 'CICLO_VIGOR'CicloVigor.
     */
    static public double sumCicloVigor(Connection _connection, String _where)
        throws SQLException {
        return sumByCriteria(_connection, _where, "CICLO_VIGOR");
    }

    /** NumDocumento
     * Get the value of the property NumDocumento.
     * The column for the database is 'NUM_DOCUMENTO'NumDocumento.
     */
    public long getNumDocumento() {
        return numDocumento;
    }

    /** NumDocumento
     * Set the value of the property NumDocumento.
     * The column for the database is 'NUM_DOCUMENTO'NumDocumento.
     */
    public void setNumDocumento(long _numDocumento) {
        this.numDocumento = _numDocumento;
        this.numDocumentoModified = true;
        this.numDocumentoNull = false;
    }

    /** NumDocumento
     * Set Null the value of the property NumDocumento.
     * The column for the database is 'NUM_DOCUMENTO'NumDocumento.
     */
    public void setNullNumDocumento() {
        this.numDocumento = 0;
        this.numDocumentoModified = true;
        this.numDocumentoNull = true;
    }

    /** NumDocumento
     * Sumatory of value of the property NumDocumento.
     * The column for the database is 'NUM_DOCUMENTO'NumDocumento.
     */
    static public double sumNumDocumento(Connection _connection, String _where)
        throws SQLException {
        return sumByCriteria(_connection, _where, "NUM_DOCUMENTO");
    }

    /** FechaCreacion
     * Get the value of the property FechaCreacion.
     * The column for the database is 'FECHA_CREACION'FechaCreacion.
     */
    public java.sql.Date getFechaCreacion() {
        return fechaCreacion;
    }

    /** FechaCreacion
     * Set the value of the property FechaCreacion.
     * The column for the database is 'FECHA_CREACION'FechaCreacion.
     */
    public void setFechaCreacion(java.sql.Date _fechaCreacion) {
        this.fechaCreacion = _fechaCreacion;
        this.fechaCreacionModified = true;
        this.fechaCreacionNull = false;
    }

    /** FechaCreacion
     * Set Null the value of the property FechaCreacion.
     * The column for the database is 'FECHA_CREACION'FechaCreacion.
     */
    public void setNullFechaCreacion() {
        this.fechaCreacion = null;
        this.fechaCreacionModified = true;
        this.fechaCreacionNull = true;
    }

    /** Vigor
     * Get the value of the property Vigor.
     * The column for the database is 'VIGOR'Vigor.
     */
    public boolean getVigor() {
        return vigor;
    }

    /** Vigor
     * Set the value of the property Vigor.
     * The column for the database is 'VIGOR'Vigor.
     */
    public void setVigor(boolean _vigor) {
        this.vigor = _vigor;
        this.vigorModified = true;
        this.vigorNull = false;
    }

    /** Vigor
     * Set Null the value of the property Vigor.
     * The column for the database is 'VIGOR'Vigor.
     */
    public void setNullVigor() {
        this.vigor = false;
        this.vigorModified = true;
        this.vigorNull = true;
    }

    /** Vigor
     * Sumatory of value of the property Vigor.
     * The column for the database is 'VIGOR'Vigor.
     */
    static public double sumVigor(Connection _connection, String _where)
        throws SQLException {
        return sumByCriteria(_connection, _where, "VIGOR");
    }

    // Persistent Method

    /**
     * Insert the current object in the database.
     * @param connection to use in insert action
     * @return true - OK, false - not necessary insert because not modified values
     */
    public boolean insert(Connection _connection) throws SQLException {
        String ls_columns = new String();
        String ls_values = new String();
        boolean lb_FirstTime = true;

        if (contenidoModified) {
            if (lb_FirstTime) {
                ls_columns += "CONTENIDO";

                if (contenidoNull) {
                    ls_values += "null";
                } else {
                    ls_values += ("'" +
                    SQLLanguageHelper.escapeQuote(contenido) + "'");
                }
            } else {
                ls_columns += ("," + "CONTENIDO");

                if (contenidoNull) {
                    ls_values += ",null";
                } else {
                    ls_values += (",'" +
                    SQLLanguageHelper.escapeQuote(contenido) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (lanzadorModified) {
            if (lb_FirstTime) {
                ls_columns += "LANZADOR";

                if (lanzadorNull) {
                    ls_values += "null";
                } else {
                    ls_values += ("'" +
                    SQLLanguageHelper.escapeQuote(lanzador) + "'");
                }
            } else {
                ls_columns += ("," + "LANZADOR");

                if (lanzadorNull) {
                    ls_values += ",null";
                } else {
                    ls_values += (",'" +
                    SQLLanguageHelper.escapeQuote(lanzador) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (idFormatoModified) {
            if (lb_FirstTime) {
                ls_columns += "ID_FORMATO";

                if (idFormatoNull) {
                    ls_values += "null";
                } else {
                    ls_values += ("'" +
                    SQLLanguageHelper.escapeQuote(idFormato) + "'");
                }
            } else {
                ls_columns += ("," + "ID_FORMATO");

                if (idFormatoNull) {
                    ls_values += ",null";
                } else {
                    ls_values += (",'" +
                    SQLLanguageHelper.escapeQuote(idFormato) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (tipoModified) {
            if (lb_FirstTime) {
                ls_columns += "TIPO";

                if (tipoNull) {
                    ls_values += "null";
                } else {
                    ls_values += ("'" + SQLLanguageHelper.escapeQuote(tipo) +
                    "'");
                }
            } else {
                ls_columns += ("," + "TIPO");

                if (tipoNull) {
                    ls_values += ",null";
                } else {
                    ls_values += (",'" + SQLLanguageHelper.escapeQuote(tipo) +
                    "'");
                }
            }

            lb_FirstTime = false;
        }

        if (formatoDescripcionModified) {
            if (lb_FirstTime) {
                ls_columns += "FORMATO_DESCRIPCION";

                if (formatoDescripcionNull) {
                    ls_values += "null";
                } else {
                    ls_values += ("'" +
                    SQLLanguageHelper.escapeQuote(formatoDescripcion) + "'");
                }
            } else {
                ls_columns += ("," + "FORMATO_DESCRIPCION");

                if (formatoDescripcionNull) {
                    ls_values += ",null";
                } else {
                    ls_values += (",'" +
                    SQLLanguageHelper.escapeQuote(formatoDescripcion) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (cicloDepuracionModified) {
            if (lb_FirstTime) {
                ls_columns += "CICLO_DEPURACION";

                if (cicloDepuracionNull) {
                    ls_values += "null";
                } else {
                    ls_values += cicloDepuracion;
                }
            } else {
                ls_columns += ("," + "CICLO_DEPURACION");

                if (cicloDepuracionNull) {
                    ls_values += ",null";
                } else {
                    ls_values += ("," + cicloDepuracion);
                }
            }

            lb_FirstTime = false;
        }

        if (cicloVigorModified) {
            if (lb_FirstTime) {
                ls_columns += "CICLO_VIGOR";

                if (cicloVigorNull) {
                    ls_values += "null";
                } else {
                    ls_values += cicloVigor;
                }
            } else {
                ls_columns += ("," + "CICLO_VIGOR");

                if (cicloVigorNull) {
                    ls_values += ",null";
                } else {
                    ls_values += ("," + cicloVigor);
                }
            }

            lb_FirstTime = false;
        }

        if (numDocumentoModified) {
            if (lb_FirstTime) {
                ls_columns += "NUM_DOCUMENTO";

                if (numDocumentoNull) {
                    ls_values += "null";
                } else {
                    ls_values += numDocumento;
                }
            } else {
                ls_columns += ("," + "NUM_DOCUMENTO");

                if (numDocumentoNull) {
                    ls_values += ",null";
                } else {
                    ls_values += ("," + numDocumento);
                }
            }

            lb_FirstTime = false;
        }

        if (fechaCreacionModified) {
            if (lb_FirstTime) {
                ls_columns += "FECHA_CREACION";

                if (fechaCreacionNull) {
                    ls_values += "null";
                } else {
                    ls_values += ("'" + fechaCreacion.toString() + "'");
                }
            } else {
                ls_columns += ("," + "FECHA_CREACION");

                if (fechaCreacionNull) {
                    ls_values += ",null";
                } else {
                    ls_values += (",'" + fechaCreacion.toString() + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (vigorModified) {
            if (lb_FirstTime) {
                ls_columns += "VIGOR";

                if (vigorNull) {
                    ls_values += "null";
                } else {
                    if (vigor) {
                        ls_values += "1";
                    } else {
                        ls_values += "0";
                    }
                }
            } else {
                ls_columns += ("," + "VIGOR");

                if (vigorNull) {
                    ls_values += ",null";
                } else {
                    if (vigor) {
                        ls_values += ",1";
                    } else {
                        ls_values += ",0";
                    }
                }
            }

            lb_FirstTime = false;
        }

        if (lb_FirstTime) {
            return false;
        }

        Statement myStatement = null;

        try {
            myStatement = _connection.createStatement();
            myStatement.executeUpdate("INSERT INTO FORMATO (" + ls_columns +
                ") VALUES (" + ls_values + ")");
            myStatement.close();
        } catch (SQLException e) {
            if (myStatement != null) {
                myStatement.close();
            }

            throw e;
        }

        return true;
    }

    /**
     * Update the current object in the database.
     * @param connection to use in update action
     * @return true - OK, false - not necessary update because not modified values
     */
    public boolean update(Connection _connection)
        throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
        String ls_setValues = new String();
        String ls_where = new String();
        boolean lb_FirstTime = true;
        ResultSet ls_rs = null;
        Statement myStatement = null;
        long ll_recordCount = 0;

        try {
            myStatement = _connection.createStatement();
            ls_where = "ID_FORMATO  = '" +
                SQLLanguageHelper.escapeQuote(idFormato) + "'";
            ls_rs = myStatement.executeQuery(
                    "SELECT COUNT(*) AS RECORD_COUNT FROM FORMATO WHERE " +
                    ls_where);

            // Procesamos el ResultSet.
            ls_rs.next();
            ll_recordCount = ls_rs.getLong("RECORD_COUNT");
            ls_rs.close();
            myStatement.close();
        } catch (SQLException e) {
            if (ls_rs != null) {
                ls_rs.close();
            }

            if (myStatement != null) {
                myStatement.close();
            }
        }

        ls_where = "";

        if (ll_recordCount == 0) {
            throw (new isf.persistence.GcgcCorruptedRegisterException(
                "Se ha modificado el registro que est� intentando actualizar, vuelva a obtenerlo de nuevo."));
        }

        if (contenidoModified) {
            if (lb_FirstTime) {
                ls_setValues += "CONTENIDO";

                if (contenidoNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(contenido) + "'");
                }
            } else {
                ls_setValues += ("," + "CONTENIDO");

                if (contenidoNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(contenido) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (lanzadorModified) {
            if (lb_FirstTime) {
                ls_setValues += "LANZADOR";

                if (lanzadorNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(lanzador) + "'");
                }
            } else {
                ls_setValues += ("," + "LANZADOR");

                if (lanzadorNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(lanzador) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (idFormatoModified) {
            if (lb_FirstTime) {
                ls_setValues += "ID_FORMATO";

                if (idFormatoNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(idFormato) + "'");
                }
            } else {
                ls_setValues += ("," + "ID_FORMATO");

                if (idFormatoNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(idFormato) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (tipoModified) {
            if (lb_FirstTime) {
                ls_setValues += "TIPO";

                if (tipoNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(tipo) + "'");
                }
            } else {
                ls_setValues += ("," + "TIPO");

                if (tipoNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(tipo) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (formatoDescripcionModified) {
            if (lb_FirstTime) {
                ls_setValues += "FORMATO_DESCRIPCION";

                if (formatoDescripcionNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(formatoDescripcion) + "'");
                }
            } else {
                ls_setValues += ("," + "FORMATO_DESCRIPCION");

                if (formatoDescripcionNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" +
                    SQLLanguageHelper.escapeQuote(formatoDescripcion) + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (cicloDepuracionModified) {
            if (lb_FirstTime) {
                ls_setValues += "CICLO_DEPURACION";

                if (cicloDepuracionNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("=" + cicloDepuracion);
                }
            } else {
                ls_setValues += ("," + "CICLO_DEPURACION");

                if (cicloDepuracionNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("=" + cicloDepuracion);
                }
            }

            lb_FirstTime = false;
        }

        if (cicloVigorModified) {
            if (lb_FirstTime) {
                ls_setValues += "CICLO_VIGOR";

                if (cicloVigorNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("=" + cicloVigor);
                }
            } else {
                ls_setValues += ("," + "CICLO_VIGOR");

                if (cicloVigorNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("=" + cicloVigor);
                }
            }

            lb_FirstTime = false;
        }

        if (numDocumentoModified) {
            if (lb_FirstTime) {
                ls_setValues += "NUM_DOCUMENTO";

                if (numDocumentoNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("=" + numDocumento);
                }
            } else {
                ls_setValues += ("," + "NUM_DOCUMENTO");

                if (numDocumentoNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("=" + numDocumento);
                }
            }

            lb_FirstTime = false;
        }

        if (fechaCreacionModified) {
            if (lb_FirstTime) {
                ls_setValues += "FECHA_CREACION";

                if (fechaCreacionNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" + fechaCreacion.toString() + "'");
                }
            } else {
                ls_setValues += ("," + "FECHA_CREACION");

                if (fechaCreacionNull) {
                    ls_setValues += "=null";
                } else {
                    ls_setValues += ("='" + fechaCreacion.toString() + "'");
                }
            }

            lb_FirstTime = false;
        }

        if (vigorModified) {
            if (lb_FirstTime) {
                ls_setValues += "VIGOR";

                if (vigorNull) {
                    ls_setValues += "=null";
                } else {
                    if (vigor) {
                        ls_setValues += "=1";
                    } else {
                        ls_setValues += "=0";
                    }
                }
            } else {
                ls_setValues += ("," + "VIGOR");

                if (vigorNull) {
                    ls_setValues += "=null";
                } else {
                    if (vigor) {
                        ls_setValues += "=1";
                    } else {
                        ls_setValues += "=0";
                    }
                }
            }

            lb_FirstTime = false;
        }

        if (lb_FirstTime) {
            return false;
        }

        lb_FirstTime = true;

        if (lb_FirstTime) {
            ls_where += "ID_FORMATO";
            ls_where += ("='" + SQLLanguageHelper.escapeQuote(idFormato) + "'");
        } else {
            ls_where += (" AND " + "ID_FORMATO");
            ls_where += ("='" + SQLLanguageHelper.escapeQuote(idFormato) + "'");
        }

        lb_FirstTime = false;

        try {
            myStatement = _connection.createStatement();
            myStatement.executeUpdate("UPDATE FORMATO SET " + ls_setValues +
                " WHERE " + ls_where);
            myStatement.close();
        } catch (SQLException e) {
            if (myStatement != null) {
                myStatement.close();
            }

            throw e;
        }

        return true;
    }

    /**
     * Delete the current object in the database.
     * @param connection to use in delete action
     * @return true OK, false Error
     */
    public boolean delete(Connection _connection)
        throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
        String ls_where = new String();
        boolean lb_FirstTime = true;
        ResultSet ls_rs = null;
        Statement myStatement = null;
        long ll_recordCount = 0;

        try {
            myStatement = _connection.createStatement();
            ls_where = "ID_FORMATO  = '" +
                SQLLanguageHelper.escapeQuote(idFormato) + "'";
            ls_rs = myStatement.executeQuery(
                    "SELECT COUNT(*) AS RECORD_COUNT FROM FORMATO WHERE " +
                    ls_where);

            // Procesamos el ResultSet.
            ls_rs.next();
            ll_recordCount = ls_rs.getLong("RECORD_COUNT");
            ls_rs.close();
            myStatement.close();
        } catch (SQLException e) {
            if (ls_rs != null) {
                ls_rs.close();
            }

            if (myStatement != null) {
                myStatement.close();
            }
        }

        ls_where = "";

        if (ll_recordCount == 0) {
            throw (new isf.persistence.GcgcCorruptedRegisterException(
                "Se ha modificado el registro que est� intentando actualizar, vuelva a obtenerlo de nuevo."));
        }

        if (lb_FirstTime) {
            ls_where += "ID_FORMATO";
            ls_where += ("='" + SQLLanguageHelper.escapeQuote(idFormato) + "'");
        } else {
            ls_where += (" AND " + "ID_FORMATO");
            ls_where += ("='" + SQLLanguageHelper.escapeQuote(idFormato) + "'");
        }

        lb_FirstTime = false;

        try {
            myStatement = _connection.createStatement();
            myStatement.executeUpdate("DELETE FORMATO WHERE " + ls_where);
            myStatement.close();
        } catch (SQLException e) {
            if (myStatement != null) {
                myStatement.close();
            }

            throw e;
        }

        return true;
    }

    /**
     * Retrieve an object in the database for the param identifier
     * @param connection - the conection for retrieve object
     * @param Identifiers - ,String _idFormato
     * @return Formato - Retrieved object
     */
    static public Formato read(Connection _connection, String _idFormato)
        throws SQLException {
        String ls_where = new String();
        ResultSet ls_rs;
        Formato ls_Formato = new Formato();
        ls_where = "ID_FORMATO  = '" +
            SQLLanguageHelper.escapeQuote(_idFormato) + "'";

        Statement myStatement = _connection.createStatement();
        ls_rs = myStatement.executeQuery("SELECT * FROM FORMATO WHERE " +
                ls_where);

        if (ls_rs.next()) {
            ls_Formato.loadResultSet(ls_rs);
        } else {
            ls_Formato = null;
        }

        ls_rs.close();
        myStatement.close();

        return ls_Formato;
    }

    public void loadResultSet(ResultSet _rs) throws SQLException {
        contenido = _rs.getString("CONTENIDO");
        lanzador = _rs.getString("LANZADOR");
        idFormato = _rs.getString("ID_FORMATO");
        tipo = _rs.getString("TIPO");
        formatoDescripcion = _rs.getString("FORMATO_DESCRIPCION");
        cicloDepuracion = _rs.getLong("CICLO_DEPURACION");
        cicloVigor = _rs.getLong("CICLO_VIGOR");
        numDocumento = _rs.getLong("NUM_DOCUMENTO");

        if (_rs.getDate("FECHA_CREACION") != null) {
            fechaCreacion = new java.sql.Date(_rs.getDate("FECHA_CREACION")
                                                 .getTime());
        } else {
            fechaCreacion = null;
        }

        vigor = _rs.getBoolean("VIGOR");
    }

    /**
     * Retrieve an objects list in the database for the condition specified
     * @param connection - the conection for retrieve objects list
     * @param where - condition for retrieve objects list
     * @param sort - condition for sort retrieve objects list
     * @return vector - objects list retrieved
     */
    static public Vector search(Connection _connection, String _where,
        String _sort) throws SQLException {
        //Construimos la query.
        String query = "SELECT * FROM FORMATO ";

        if ((_where != null) && (_where.trim().compareTo("") != 0)) {
            query += ("WHERE " + _where + " ");
        }

        if ((_sort != null) && (_sort.trim().compareTo("") != 0)) {
            query += (" ORDER BY " + _sort + " ");
        }

        // Creamos el Statement y el ResultSet.
        Statement myStatement = _connection.createStatement();
        ResultSet ls_rs = myStatement.executeQuery(query);

        // Procesamos el ResultSet.
        Formato myFormato;
        Vector myFormatoes = new Vector();

        while (ls_rs.next()) {
            myFormato = new Formato();
            myFormato.loadResultSet(ls_rs);
            myFormatoes.addElement(myFormato);
        }

        ls_rs.close();
        myStatement.close();

        return myFormatoes;
    }

    /**
     * Retrieve the number of objects that accomplishe the condition specified
     * @param connection - the conection for retrieve objects list
     * @param where - condition for retrieve objects list
     * @return long - number of objects found. 0 no one
     */
    static public long countByCriteria(Connection _connection, String _where)
        throws SQLException {
        //Construimos la query.
        String query = "SELECT COUNT(*) AS RECORD_COUNT FROM FORMATO ";

        if ((_where != null) && (_where.trim().compareTo("") != 0)) {
            query += ("WHERE " + _where + " ");
        }

        // Creamos el Statement y el ResultSet.
        Statement myStatement = _connection.createStatement();
        ResultSet ls_rs = myStatement.executeQuery(query);
        long ll_recordCount;

        // Procesamos el ResultSet.
        ls_rs.next();
        ll_recordCount = ls_rs.getLong("RECORD_COUNT");
        ls_rs.close();
        myStatement.close();

        return ll_recordCount;
    }

    /**
     * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
     * @param connection - the conection for retrieve objects list
     * @param where - condition for retrieve objects list
     * @return double - Sumatory result
     */
    static private double sumByCriteria(Connection _connection, String _where,
        String _columnName) throws SQLException {
        //Construimos la query.
        String query = "SELECT SUM(" + _columnName +
            ") AS SUMATORY FROM FORMATO ";

        if ((_where != null) && (_where.trim().compareTo("") != 0)) {
            query += ("WHERE " + _where + " ");
        }

        // Creamos el Statement y el ResultSet.
        Statement myStatement = _connection.createStatement();
        ResultSet ls_rs = myStatement.executeQuery(query);
        double ld_sumatory;

        // Procesamos el ResultSet.
        ls_rs.next();
        ld_sumatory = ls_rs.getDouble("SUMATORY");
        ls_rs.close();
        myStatement.close();

        return ld_sumatory;
    }

    /**
     * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
     * @param connection - the conection for retrieve objects list
     * @param where - condition for retrieve objects list
     * @return Object - Maximum or minimum result.
     * this method returns datatypes according to this list:<ul>
             <li>for any numeric type: java.lang.Double</li>
             <li>for date and/or times: java.sql.Date</li>
             <li>for any text type: java.lang.String</li>
             </ul>
    */
    static private Object functionByCriteria(Connection _connection,
        String _where, String _columnName, String _function)
        throws SQLException {
        //Construimos la query.
        String query = "SELECT " + _function + "(" + _columnName +
            ") AS CALCULATION FROM FORMATO ";

        if ((_where != null) && (_where.trim().compareTo("") != 0)) {
            query += ("WHERE " + _where + " ");
        }

        // Creamos el Statement y el ResultSet.
        Statement myStatement = _connection.createStatement();
        ResultSet ls_rs = myStatement.executeQuery(query);
        Object obj_result = null;

        // Procesamos el ResultSet.
        if (ls_rs.next()) {
            obj_result = ls_rs.getObject("CALCULATION");

            // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
            if (obj_result instanceof java.math.BigDecimal) {
                obj_result = new Double(((java.math.BigDecimal) obj_result).doubleValue());
            } else if (obj_result instanceof java.sql.Timestamp) {
                obj_result = new java.sql.Date(((java.sql.Timestamp) obj_result).getTime());
            }
        }

        ls_rs.close();
        myStatement.close();

        return obj_result;
    }

    static public Object maxByCriteria(Connection _connection, String _where,
        String _columnName) throws SQLException {
        return functionByCriteria(_connection, _where, _columnName, "MAX");
    }

    static public Object minByCriteria(Connection _connection, String _where,
        String _columnName) throws SQLException {
        return functionByCriteria(_connection, _where, _columnName, "MIN");
    }

    /**
     * Create the unique identifier for the object.
     */
    public void newId() throws SQLException {
        Pool myPool = Pool.getInstance();
    }
}
