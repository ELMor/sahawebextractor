/*

*   Copyright ( 1999 Uni�n Fenosa International Software Factory,

*   S.A.. All Rights Reserved.

*

*   This software is the confidential and proprietary information of

*   Uni�n Fenosa International Software Factory, S.A.. You shall not

*   disclose such confidential information and shall not accordance

*   with the terms of the license agreement you entered into with

*   I.S.F..

*

*/
package isf.db;

import conf.*;

import java.sql.*;

import java.util.*;


/**
   Clase:      ConsultaSQL
   Autor:      JDI - 28/05/2001
   Prop�sito:  Realiza una consulta SQL, dada una conexi�n y una cadena SQL
*/
public class ConsultaSQL {
    //atributos
    Statement stmt = null;
    ResultSet rs = null;

    /**
                   M�todo:     EjecutarSQL
                   Autor:      JDI - 29/05/2001
                   Prop�sito:  Realiza una consulta SQL, dada una conexi�n, una cadena SQL
                                                                   y el n�mero de columnas que devolver� la consulta, es decir,
                                                                   el n�mero de campos del select.
                */
    public Vector EjecutarSQL(String sql, int nColumnas) {
        Pool pool = null;
        Connection conexion = null;
        Vector vConsulta = new Vector();

        try {
            pool = Pool.getInstance();
            conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);

            vConsulta = EjecutarSQL(conexion, sql, nColumnas);
        } catch (Exception sqlEx) {
            System.out.println(
                "Se produjo un error consulta en ConsultaSQL.EjecutarSQL =>" +
                sqlEx.getMessage());
            System.out.println("SQL => " + sql);
        } finally {
            pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conexion);

            return vConsulta;
        }
    }

    /**
                   M�todo:     EjecutarSQL
                   Autor:      JDI - 29/05/2001
                   Prop�sito:  Realiza una consulta SQL, dada una conexi�n, una cadena SQL
                                                                   y el n�mero de columnas que devolver� la consulta, es decir,
                                                                   el n�mero de campos del select.
                */
    public Vector EjecutarSQL(Connection conn, String sql, int nColumnas) {
        Vector vConsulta = new Vector();
        int nFilas = 0;

        try {
            // Si el statement ya se uso en una vez anterior hay que cerrarlo antes de crear otro
            if (stmt != null) {
                stmt.close();
            }

            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Vector vTemp = new Vector();

                for (int int_pos = 1; int_pos <= nColumnas; int_pos++) {
                    vTemp.addElement(rs.getString(int_pos));
                }

                vConsulta.addElement(vTemp);
            }

            stmt.close();
        } catch (SQLException sqlEx) {
            System.out.println(
                "Se produjo un error consulta en ConsultaSQL.EjecutarSQL =>" +
                sqlEx.getMessage());
            System.out.println("SQL => " + sql);
        } finally {
            return vConsulta;
        }
    }

    /**
                   M�todo:     EjecutarSQL
                   Autor:      JDI - 29/05/2001
                   Prop�sito:  Realiza una consulta SQL, ddada una cadena SQL
                */
    public int EjecutarSQL(String sql) {
        Pool pool = null;
        Connection conexion = null;
        int resultado = 0;

        try {
            pool = Pool.getInstance();
            conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);

            resultado = EjecutarSQL(conexion, sql);
        } catch (Exception sqlEx) {
            System.out.println(
                "Se produjo un error consulta en ConsultaSQL.EjecutarSQL =>" +
                sqlEx.getMessage());
            System.out.println("SQL => " + sql);
        } finally {
            pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conexion);

            return resultado;
        }
    }

    /**
                   M�todo:     EjecutarSQL
                   Autor:      JDI - 27/06/2001
                   Prop�sito:  Realiza una consulta SQL de acci�n, dada una conexi�n,
                                                                   y una cadena SQL
                */
    public int EjecutarSQL(Connection conn, String sql) {
        int resultado = 0;

        try {
            // Si el statement ya se uso en una vez anterior hay que cerrarlo antes de crear otro
            if (stmt != null) {
                stmt.close();
            }

            stmt = conn.createStatement();
            resultado = stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException sqlEx) {
            System.out.println(
                "Se produjo un error consulta en ConsultaSQL.EjecutarSQL =>" +
                sqlEx.getMessage());
            System.out.println("SQL => " + sql);
        } finally {
            return resultado;
        }
    }
}
