//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: Prestacion
// Table: PRESTACION
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:PRESTACION
 * Columns for the table:
 * 	(centro - LONG), 
 * 	(codprest - STRING), 
 * 	(desprest - STRING), 
 * 	(hayprep - STRING), 
 * 	(durprest - LONG), 
 * 	(resuldias - LONG), 
 * 	(resulhoras - LONG), 
 * 	(identpres - STRING), 
 * 	(prep - STRING), 
 * 	(longprep - STRING), 
 * 	(cnormal - STRING), 
 * 	(codtecnica - DECIMAL), 
 * Primary columns for the table:
 * 	(centro - LONG) 
 * 	(codprest - STRING) 
 */
public class Prestacion
{
	// Properties
	private long centro;
	private boolean centroNull=true;
	private boolean centroModified=false;
	private String codprest;
	private boolean codprestNull=true;
	private boolean codprestModified=false;
	private String desprest;
	private boolean desprestNull=true;
	private boolean desprestModified=false;
	private String hayprep;
	private boolean hayprepNull=true;
	private boolean hayprepModified=false;
	private long durprest;
	private boolean durprestNull=true;
	private boolean durprestModified=false;
	private long resuldias;
	private boolean resuldiasNull=true;
	private boolean resuldiasModified=false;
	private long resulhoras;
	private boolean resulhorasNull=true;
	private boolean resulhorasModified=false;
	private String identpres;
	private boolean identpresNull=true;
	private boolean identpresModified=false;
	private String prep;
	private boolean prepNull=true;
	private boolean prepModified=false;
	private String longprep;
	private boolean longprepNull=true;
	private boolean longprepModified=false;
	private String cnormal;
	private boolean cnormalNull=true;
	private boolean cnormalModified=false;
	private double codtecnica;
	private boolean codtecnicaNull=true;
	private boolean codtecnicaModified=false;
	// Access Method
	/** Centro
	 * Get the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public long getCentro() {
		return centro;
	}
	/** Centro
	 * Set the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setCentro(long _centro) {
		this.centro=_centro;
		this.centroModified=true;
		this.centroNull=false;
	}
	/** Centro
	 * Set Null the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setNullCentro() {
		this.centro=0;
		this.centroModified=true;
		this.centroNull=true;
	}
	/** Centro
	 * Sumatory of value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	static public double sumCentro(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CENTRO");
	}
	/** Centro
	 * Indicates if the value of the property Centro is null or not.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public boolean isNullCentro() {
		return centroNull;
	}
	/** Codprest
	 * Get the value of the property Codprest.
	 * The column for the database is 'CODPREST'Codprest.
	 */
	public String getCodprest() {
		if (codprest==null) return "";
		if (codprest.compareTo("null")==0) return "";
		return codprest;
	}
	/** Codprest
	 * Set the value of the property Codprest.
	 * The column for the database is 'CODPREST'Codprest.
	 */
	public void setCodprest(String _codprest) {
		this.codprest=_codprest;
		this.codprestModified=true;
		this.codprestNull=false;
	}
	/** Codprest
	 * Set Null the value of the property Codprest.
	 * The column for the database is 'CODPREST'Codprest.
	 */
	public void setNullCodprest() {
		this.codprest=null;
		this.codprestModified=true;
		this.codprestNull=true;
	}
	/** Codprest
	 * Indicates if the value of the property Codprest is null or not.
	 * The column for the database is 'CODPREST'Codprest.
	 */
	public boolean isNullCodprest() {
		return codprestNull;
	}
	/** Desprest
	 * Get the value of the property Desprest.
	 * The column for the database is 'DESPREST'Desprest.
	 */
	public String getDesprest() {
		if (desprest==null) return "";
		if (desprest.compareTo("null")==0) return "";
		return desprest;
	}
	/** Desprest
	 * Set the value of the property Desprest.
	 * The column for the database is 'DESPREST'Desprest.
	 */
	public void setDesprest(String _desprest) {
		this.desprest=_desprest;
		this.desprestModified=true;
		this.desprestNull=false;
	}
	/** Desprest
	 * Set Null the value of the property Desprest.
	 * The column for the database is 'DESPREST'Desprest.
	 */
	public void setNullDesprest() {
		this.desprest=null;
		this.desprestModified=true;
		this.desprestNull=true;
	}
	/** Desprest
	 * Indicates if the value of the property Desprest is null or not.
	 * The column for the database is 'DESPREST'Desprest.
	 */
	public boolean isNullDesprest() {
		return desprestNull;
	}
	/** Hayprep
	 * Get the value of the property Hayprep.
	 * The column for the database is 'HAYPREP'Hayprep.
	 */
	public String getHayprep() {
		if (hayprep==null) return "";
		if (hayprep.compareTo("null")==0) return "";
		return hayprep;
	}
	/** Hayprep
	 * Set the value of the property Hayprep.
	 * The column for the database is 'HAYPREP'Hayprep.
	 */
	public void setHayprep(String _hayprep) {
		this.hayprep=_hayprep;
		this.hayprepModified=true;
		this.hayprepNull=false;
	}
	/** Hayprep
	 * Set Null the value of the property Hayprep.
	 * The column for the database is 'HAYPREP'Hayprep.
	 */
	public void setNullHayprep() {
		this.hayprep=null;
		this.hayprepModified=true;
		this.hayprepNull=true;
	}
	/** Hayprep
	 * Indicates if the value of the property Hayprep is null or not.
	 * The column for the database is 'HAYPREP'Hayprep.
	 */
	public boolean isNullHayprep() {
		return hayprepNull;
	}
	/** Durprest
	 * Get the value of the property Durprest.
	 * The column for the database is 'DURPREST'Durprest.
	 */
	public long getDurprest() {
		return durprest;
	}
	/** Durprest
	 * Set the value of the property Durprest.
	 * The column for the database is 'DURPREST'Durprest.
	 */
	public void setDurprest(long _durprest) {
		this.durprest=_durprest;
		this.durprestModified=true;
		this.durprestNull=false;
	}
	/** Durprest
	 * Set Null the value of the property Durprest.
	 * The column for the database is 'DURPREST'Durprest.
	 */
	public void setNullDurprest() {
		this.durprest=0;
		this.durprestModified=true;
		this.durprestNull=true;
	}
	/** Durprest
	 * Sumatory of value of the property Durprest.
	 * The column for the database is 'DURPREST'Durprest.
	 */
	static public double sumDurprest(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"DURPREST");
	}
	/** Durprest
	 * Indicates if the value of the property Durprest is null or not.
	 * The column for the database is 'DURPREST'Durprest.
	 */
	public boolean isNullDurprest() {
		return durprestNull;
	}
	/** Resuldias
	 * Get the value of the property Resuldias.
	 * The column for the database is 'RESULDIAS'Resuldias.
	 */
	public long getResuldias() {
		return resuldias;
	}
	/** Resuldias
	 * Set the value of the property Resuldias.
	 * The column for the database is 'RESULDIAS'Resuldias.
	 */
	public void setResuldias(long _resuldias) {
		this.resuldias=_resuldias;
		this.resuldiasModified=true;
		this.resuldiasNull=false;
	}
	/** Resuldias
	 * Set Null the value of the property Resuldias.
	 * The column for the database is 'RESULDIAS'Resuldias.
	 */
	public void setNullResuldias() {
		this.resuldias=0;
		this.resuldiasModified=true;
		this.resuldiasNull=true;
	}
	/** Resuldias
	 * Sumatory of value of the property Resuldias.
	 * The column for the database is 'RESULDIAS'Resuldias.
	 */
	static public double sumResuldias(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"RESULDIAS");
	}
	/** Resuldias
	 * Indicates if the value of the property Resuldias is null or not.
	 * The column for the database is 'RESULDIAS'Resuldias.
	 */
	public boolean isNullResuldias() {
		return resuldiasNull;
	}
	/** Resulhoras
	 * Get the value of the property Resulhoras.
	 * The column for the database is 'RESULHORAS'Resulhoras.
	 */
	public long getResulhoras() {
		return resulhoras;
	}
	/** Resulhoras
	 * Set the value of the property Resulhoras.
	 * The column for the database is 'RESULHORAS'Resulhoras.
	 */
	public void setResulhoras(long _resulhoras) {
		this.resulhoras=_resulhoras;
		this.resulhorasModified=true;
		this.resulhorasNull=false;
	}
	/** Resulhoras
	 * Set Null the value of the property Resulhoras.
	 * The column for the database is 'RESULHORAS'Resulhoras.
	 */
	public void setNullResulhoras() {
		this.resulhoras=0;
		this.resulhorasModified=true;
		this.resulhorasNull=true;
	}
	/** Resulhoras
	 * Sumatory of value of the property Resulhoras.
	 * The column for the database is 'RESULHORAS'Resulhoras.
	 */
	static public double sumResulhoras(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"RESULHORAS");
	}
	/** Resulhoras
	 * Indicates if the value of the property Resulhoras is null or not.
	 * The column for the database is 'RESULHORAS'Resulhoras.
	 */
	public boolean isNullResulhoras() {
		return resulhorasNull;
	}
	/** Identpres
	 * Get the value of the property Identpres.
	 * The column for the database is 'IDENTPRES'Identpres.
	 */
	public String getIdentpres() {
		if (identpres==null) return "";
		if (identpres.compareTo("null")==0) return "";
		return identpres;
	}
	/** Identpres
	 * Set the value of the property Identpres.
	 * The column for the database is 'IDENTPRES'Identpres.
	 */
	public void setIdentpres(String _identpres) {
		this.identpres=_identpres;
		this.identpresModified=true;
		this.identpresNull=false;
	}
	/** Identpres
	 * Set Null the value of the property Identpres.
	 * The column for the database is 'IDENTPRES'Identpres.
	 */
	public void setNullIdentpres() {
		this.identpres=null;
		this.identpresModified=true;
		this.identpresNull=true;
	}
	/** Identpres
	 * Indicates if the value of the property Identpres is null or not.
	 * The column for the database is 'IDENTPRES'Identpres.
	 */
	public boolean isNullIdentpres() {
		return identpresNull;
	}
	/** Prep
	 * Get the value of the property Prep.
	 * The column for the database is 'PREP'Prep.
	 */
	public String getPrep() {
		if (prep==null) return "";
		if (prep.compareTo("null")==0) return "";
		return prep;
	}
	/** Prep
	 * Set the value of the property Prep.
	 * The column for the database is 'PREP'Prep.
	 */
	public void setPrep(String _prep) {
		this.prep=_prep;
		this.prepModified=true;
		this.prepNull=false;
	}
	/** Prep
	 * Set Null the value of the property Prep.
	 * The column for the database is 'PREP'Prep.
	 */
	public void setNullPrep() {
		this.prep=null;
		this.prepModified=true;
		this.prepNull=true;
	}
	/** Prep
	 * Indicates if the value of the property Prep is null or not.
	 * The column for the database is 'PREP'Prep.
	 */
	public boolean isNullPrep() {
		return prepNull;
	}
	/** Longprep
	 * Get the value of the property Longprep.
	 * The column for the database is 'LONGPREP'Longprep.
	 */
	public String getLongprep() {
		if (longprep==null) return "";
		if (longprep.compareTo("null")==0) return "";
		return longprep;
	}
	/** Longprep
	 * Set the value of the property Longprep.
	 * The column for the database is 'LONGPREP'Longprep.
	 */
	public void setLongprep(String _longprep) {
		this.longprep=_longprep;
		this.longprepModified=true;
		this.longprepNull=false;
	}
	/** Longprep
	 * Set Null the value of the property Longprep.
	 * The column for the database is 'LONGPREP'Longprep.
	 */
	public void setNullLongprep() {
		this.longprep=null;
		this.longprepModified=true;
		this.longprepNull=true;
	}
	/** Longprep
	 * Indicates if the value of the property Longprep is null or not.
	 * The column for the database is 'LONGPREP'Longprep.
	 */
	public boolean isNullLongprep() {
		return longprepNull;
	}
	/** Cnormal
	 * Get the value of the property Cnormal.
	 * The column for the database is 'CNORMAL'Cnormal.
	 */
	public String getCnormal() {
		if (cnormal==null) return "";
		if (cnormal.compareTo("null")==0) return "";
		return cnormal;
	}
	/** Cnormal
	 * Set the value of the property Cnormal.
	 * The column for the database is 'CNORMAL'Cnormal.
	 */
	public void setCnormal(String _cnormal) {
		this.cnormal=_cnormal;
		this.cnormalModified=true;
		this.cnormalNull=false;
	}
	/** Cnormal
	 * Set Null the value of the property Cnormal.
	 * The column for the database is 'CNORMAL'Cnormal.
	 */
	public void setNullCnormal() {
		this.cnormal=null;
		this.cnormalModified=true;
		this.cnormalNull=true;
	}
	/** Cnormal
	 * Indicates if the value of the property Cnormal is null or not.
	 * The column for the database is 'CNORMAL'Cnormal.
	 */
	public boolean isNullCnormal() {
		return cnormalNull;
	}
	/** Codtecnica
	 * Get the value of the property Codtecnica.
	 * The column for the database is 'CODTECNICA'Codtecnica.
	 */
	public double getCodtecnica() {
		return codtecnica;
	}
	/** Codtecnica
	 * Set the value of the property Codtecnica.
	 * The column for the database is 'CODTECNICA'Codtecnica.
	 */
	public void setCodtecnica(double _codtecnica) {
		this.codtecnica=_codtecnica;
		this.codtecnicaModified=true;
		this.codtecnicaNull=false;
	}
	/** Codtecnica
	 * Set Null the value of the property Codtecnica.
	 * The column for the database is 'CODTECNICA'Codtecnica.
	 */
	public void setNullCodtecnica() {
		this.codtecnica=0;
		this.codtecnicaModified=true;
		this.codtecnicaNull=true;
	}
	/** Codtecnica
	 * Sumatory of value of the property Codtecnica.
	 * The column for the database is 'CODTECNICA'Codtecnica.
	 */
	static public double sumCodtecnica(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CODTECNICA");
	}
	/** Codtecnica
	 * Indicates if the value of the property Codtecnica is null or not.
	 * The column for the database is 'CODTECNICA'Codtecnica.
	 */
	public boolean isNullCodtecnica() {
		return codtecnicaNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!centroNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CENTRO");
			ls_values.append((lb_FirstTime?"":",")+centro);
			lb_FirstTime = false;
		}
		if (!codprestNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CODPREST");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(codprest)+"'");
			lb_FirstTime = false;
		}
		if (!desprestNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DESPREST");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(desprest)+"'");
			lb_FirstTime = false;
		}
		if (!hayprepNull) {
			ls_columns.append((lb_FirstTime?"":",")+"HAYPREP");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(hayprep)+"'");
			lb_FirstTime = false;
		}
		if (!durprestNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DURPREST");
			ls_values.append((lb_FirstTime?"":",")+durprest);
			lb_FirstTime = false;
		}
		if (!resuldiasNull) {
			ls_columns.append((lb_FirstTime?"":",")+"RESULDIAS");
			ls_values.append((lb_FirstTime?"":",")+resuldias);
			lb_FirstTime = false;
		}
		if (!resulhorasNull) {
			ls_columns.append((lb_FirstTime?"":",")+"RESULHORAS");
			ls_values.append((lb_FirstTime?"":",")+resulhoras);
			lb_FirstTime = false;
		}
		if (!identpresNull) {
			ls_columns.append((lb_FirstTime?"":",")+"IDENTPRES");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(identpres)+"'");
			lb_FirstTime = false;
		}
		if (!prepNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PREP");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(prep)+"'");
			lb_FirstTime = false;
		}
		if (!longprepNull) {
			ls_columns.append((lb_FirstTime?"":",")+"LONGPREP");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(longprep)+"'");
			lb_FirstTime = false;
		}
		if (!cnormalNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CNORMAL");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(cnormal)+"'");
			lb_FirstTime = false;
		}
		if (!codtecnicaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CODTECNICA");
			ls_values.append((lb_FirstTime?"":",")+codtecnica);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO PRESTACION ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (centroModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CENTRO");
			if(centroNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+centro);
			lb_FirstTime = false;
		}
		if (codprestModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CODPREST");
			if(codprestNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(codprest)+"'");
			lb_FirstTime = false;
		}
		if (desprestModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DESPREST");
			if(desprestNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(desprest)+"'");
			lb_FirstTime = false;
		}
		if (hayprepModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("HAYPREP");
			if(hayprepNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(hayprep)+"'");
			lb_FirstTime = false;
		}
		if (durprestModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DURPREST");
			if(durprestNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+durprest);
			lb_FirstTime = false;
		}
		if (resuldiasModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("RESULDIAS");
			if(resuldiasNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+resuldias);
			lb_FirstTime = false;
		}
		if (resulhorasModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("RESULHORAS");
			if(resulhorasNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+resulhoras);
			lb_FirstTime = false;
		}
		if (identpresModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("IDENTPRES");
			if(identpresNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(identpres)+"'");
			lb_FirstTime = false;
		}
		if (prepModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PREP");
			if(prepNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(prep)+"'");
			lb_FirstTime = false;
		}
		if (longprepModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("LONGPREP");
			if(longprepNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(longprep)+"'");
			lb_FirstTime = false;
		}
		if (cnormalModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CNORMAL");
			if(cnormalNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(cnormal)+"'");
			lb_FirstTime = false;
		}
		if (codtecnicaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CODTECNICA");
			if(codtecnicaNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+codtecnica);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("CENTRO");
			ls_where.append("="+centro);
		} else {
			ls_where.append(" AND "+"CENTRO");
			ls_where.append("="+centro);
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("CODPREST");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(codprest)+"'");
		} else {
			ls_where.append(" AND "+"CODPREST");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(codprest)+"'");
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE PRESTACION SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("CENTRO");
			ls_where.append("="+centro);
		} else {
			ls_where.append(" AND "+"CENTRO");
			ls_where.append("="+centro);
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("CODPREST");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(codprest)+"'");
		} else {
			ls_where.append(" AND "+"CODPREST");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(codprest)+"'");
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE PRESTACION WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _centro,String _codprest
	 * @return Prestacion - Retrieved object
	 */
	static public Prestacion read(Connection _connection,long _centro,String _codprest) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		Prestacion ls_Prestacion=new Prestacion();
		ls_where="CENTRO = " + _centro + " AND " + "CODPREST  = '"+ SQLLanguageHelper.escapeQuote(  _codprest) + "'";
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM PRESTACION WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_Prestacion.loadResultSet(ls_rs);
		} else {
			ls_Prestacion=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_Prestacion;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		centro=_rs.getLong("CENTRO");
		centroNull=_rs.wasNull();
		codprest=_rs.getString("CODPREST");
		codprestNull=_rs.wasNull();
		desprest=_rs.getString("DESPREST");
		desprestNull=_rs.wasNull();
		hayprep=_rs.getString("HAYPREP");
		hayprepNull=_rs.wasNull();
		durprest=_rs.getLong("DURPREST");
		durprestNull=_rs.wasNull();
		resuldias=_rs.getLong("RESULDIAS");
		resuldiasNull=_rs.wasNull();
		resulhoras=_rs.getLong("RESULHORAS");
		resulhorasNull=_rs.wasNull();
		identpres=_rs.getString("IDENTPRES");
		identpresNull=_rs.wasNull();
		prep=_rs.getString("PREP");
		prepNull=_rs.wasNull();
		longprep=_rs.getString("LONGPREP");
		longprepNull=_rs.wasNull();
		cnormal=_rs.getString("CNORMAL");
		cnormalNull=_rs.wasNull();
		codtecnica=_rs.getDouble("CODTECNICA");
		codtecnicaNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM PRESTACION ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		Prestacion myPrestacion;
		Vector myPrestaciones = new Vector();
		while (ls_rs.next()) {
			myPrestacion = new Prestacion();
			myPrestacion.loadResultSet (ls_rs);
			myPrestaciones.addElement(myPrestacion);
		}
		ls_rs.close();
		myStatement.close();
		return myPrestaciones;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM PRESTACION ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM PRESTACION ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM PRESTACION ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setCentro(myPool.getSequence(this.getClass().getName(),profile));
	}
}
