//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: Nolesp
// Table: NOLESP
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:NOLESP
 * Columns for the table:
 * 	(gar_mot_rechazo - STRING), 
 * 	(nnolesp - DECIMAL), 
 * 	(nombre - STRING), 
 * 	(apellido1 - STRING), 
 * 	(apellido2 - STRING), 
 * 	(cip - STRING), 
 * 	(domicilio - STRING), 
 * 	(fecha_nac - DATE), 
 * 	(poblacion - STRING), 
 * 	(provincia - STRING), 
 * 	(nhc - DECIMAL), 
 * 	(centro - DECIMAL), 
 * 	(prestacion - STRING), 
 * 	(telefono - STRING), 
 * 	(nss - STRING), 
 * 	(gar_fechor - DATE), 
 * 	(gar_repleg_dni - STRING), 
 * 	(gar_repleg_apenom - STRING), 
 * 	(gar_oficina - DECIMAL), 
 * 	(sys_usu - STRING), 
 * Primary columns for the table:
 * 	(nnolesp - DECIMAL) 
 */
public class Nolesp
{
	// Properties
	private String garMotRechazo;
	private boolean garMotRechazoNull=true;
	private boolean garMotRechazoModified=false;
	private double nnolesp;
	private boolean nnolespNull=true;
	private boolean nnolespModified=false;
	private String nombre;
	private boolean nombreNull=true;
	private boolean nombreModified=false;
	private String apellido1;
	private boolean apellido1Null=true;
	private boolean apellido1Modified=false;
	private String apellido2;
	private boolean apellido2Null=true;
	private boolean apellido2Modified=false;
	private String cip;
	private boolean cipNull=true;
	private boolean cipModified=false;
	private String domicilio;
	private boolean domicilioNull=true;
	private boolean domicilioModified=false;
	private java.sql.Date fechaNac;
	private boolean fechaNacNull=true;
	private boolean fechaNacModified=false;
	private String poblacion;
	private boolean poblacionNull=true;
	private boolean poblacionModified=false;
	private String provincia;
	private boolean provinciaNull=true;
	private boolean provinciaModified=false;
	private double nhc;
	private boolean nhcNull=true;
	private boolean nhcModified=false;
	private double centro;
	private boolean centroNull=true;
	private boolean centroModified=false;
	private String prestacion;
	private boolean prestacionNull=true;
	private boolean prestacionModified=false;
	private String telefono;
	private boolean telefonoNull=true;
	private boolean telefonoModified=false;
	private String nss;
	private boolean nssNull=true;
	private boolean nssModified=false;
	private java.sql.Date garFechor;
	private boolean garFechorNull=true;
	private boolean garFechorModified=false;
	private String garReplegDni;
	private boolean garReplegDniNull=true;
	private boolean garReplegDniModified=false;
	private String garReplegApenom;
	private boolean garReplegApenomNull=true;
	private boolean garReplegApenomModified=false;
	private double garOficina;
	private boolean garOficinaNull=true;
	private boolean garOficinaModified=false;
	private String sysUsu;
	private boolean sysUsuNull=true;
	private boolean sysUsuModified=false;
	// Access Method
	/** GarMotRechazo
	 * Get the value of the property GarMotRechazo.
	 * The column for the database is 'GAR_MOT_RECHAZO'GarMotRechazo.
	 */
	public String getGarMotRechazo() {
		if (garMotRechazo==null) return "";
		if (garMotRechazo.compareTo("null")==0) return "";
		return garMotRechazo;
	}
	/** GarMotRechazo
	 * Set the value of the property GarMotRechazo.
	 * The column for the database is 'GAR_MOT_RECHAZO'GarMotRechazo.
	 */
	public void setGarMotRechazo(String _garMotRechazo) {
		this.garMotRechazo=_garMotRechazo;
		this.garMotRechazoModified=true;
		this.garMotRechazoNull=false;
	}
	/** GarMotRechazo
	 * Set Null the value of the property GarMotRechazo.
	 * The column for the database is 'GAR_MOT_RECHAZO'GarMotRechazo.
	 */
	public void setNullGarMotRechazo() {
		this.garMotRechazo=null;
		this.garMotRechazoModified=true;
		this.garMotRechazoNull=true;
	}
	/** GarMotRechazo
	 * Indicates if the value of the property GarMotRechazo is null or not.
	 * The column for the database is 'GAR_MOT_RECHAZO'GarMotRechazo.
	 */
	public boolean isNullGarMotRechazo() {
		return garMotRechazoNull;
	}
	/** Nnolesp
	 * Get the value of the property Nnolesp.
	 * The column for the database is 'NNOLESP'Nnolesp.
	 */
	public double getNnolesp() {
		return nnolesp;
	}
	/** Nnolesp
	 * Set the value of the property Nnolesp.
	 * The column for the database is 'NNOLESP'Nnolesp.
	 */
	public void setNnolesp(double _nnolesp) {
		this.nnolesp=_nnolesp;
		this.nnolespModified=true;
		this.nnolespNull=false;
	}
	/** Nnolesp
	 * Set Null the value of the property Nnolesp.
	 * The column for the database is 'NNOLESP'Nnolesp.
	 */
	public void setNullNnolesp() {
		this.nnolesp=0;
		this.nnolespModified=true;
		this.nnolespNull=true;
	}
	/** Nnolesp
	 * Sumatory of value of the property Nnolesp.
	 * The column for the database is 'NNOLESP'Nnolesp.
	 */
	static public double sumNnolesp(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"NNOLESP");
	}
	/** Nnolesp
	 * Indicates if the value of the property Nnolesp is null or not.
	 * The column for the database is 'NNOLESP'Nnolesp.
	 */
	public boolean isNullNnolesp() {
		return nnolespNull;
	}
	/** Nombre
	 * Get the value of the property Nombre.
	 * The column for the database is 'NOMBRE'Nombre.
	 */
	public String getNombre() {
		if (nombre==null) return "";
		if (nombre.compareTo("null")==0) return "";
		return nombre;
	}
	/** Nombre
	 * Set the value of the property Nombre.
	 * The column for the database is 'NOMBRE'Nombre.
	 */
	public void setNombre(String _nombre) {
		this.nombre=_nombre;
		this.nombreModified=true;
		this.nombreNull=false;
	}
	/** Nombre
	 * Set Null the value of the property Nombre.
	 * The column for the database is 'NOMBRE'Nombre.
	 */
	public void setNullNombre() {
		this.nombre=null;
		this.nombreModified=true;
		this.nombreNull=true;
	}
	/** Nombre
	 * Indicates if the value of the property Nombre is null or not.
	 * The column for the database is 'NOMBRE'Nombre.
	 */
	public boolean isNullNombre() {
		return nombreNull;
	}
	/** Apellido1
	 * Get the value of the property Apellido1.
	 * The column for the database is 'APELLIDO1'Apellido1.
	 */
	public String getApellido1() {
		if (apellido1==null) return "";
		if (apellido1.compareTo("null")==0) return "";
		return apellido1;
	}
	/** Apellido1
	 * Set the value of the property Apellido1.
	 * The column for the database is 'APELLIDO1'Apellido1.
	 */
	public void setApellido1(String _apellido1) {
		this.apellido1=_apellido1;
		this.apellido1Modified=true;
		this.apellido1Null=false;
	}
	/** Apellido1
	 * Set Null the value of the property Apellido1.
	 * The column for the database is 'APELLIDO1'Apellido1.
	 */
	public void setNullApellido1() {
		this.apellido1=null;
		this.apellido1Modified=true;
		this.apellido1Null=true;
	}
	/** Apellido1
	 * Indicates if the value of the property Apellido1 is null or not.
	 * The column for the database is 'APELLIDO1'Apellido1.
	 */
	public boolean isNullApellido1() {
		return apellido1Null;
	}
	/** Apellido2
	 * Get the value of the property Apellido2.
	 * The column for the database is 'APELLIDO2'Apellido2.
	 */
	public String getApellido2() {
		if (apellido2==null) return "";
		if (apellido2.compareTo("null")==0) return "";
		return apellido2;
	}
	/** Apellido2
	 * Set the value of the property Apellido2.
	 * The column for the database is 'APELLIDO2'Apellido2.
	 */
	public void setApellido2(String _apellido2) {
		this.apellido2=_apellido2;
		this.apellido2Modified=true;
		this.apellido2Null=false;
	}
	/** Apellido2
	 * Set Null the value of the property Apellido2.
	 * The column for the database is 'APELLIDO2'Apellido2.
	 */
	public void setNullApellido2() {
		this.apellido2=null;
		this.apellido2Modified=true;
		this.apellido2Null=true;
	}
	/** Apellido2
	 * Indicates if the value of the property Apellido2 is null or not.
	 * The column for the database is 'APELLIDO2'Apellido2.
	 */
	public boolean isNullApellido2() {
		return apellido2Null;
	}
	/** Cip
	 * Get the value of the property Cip.
	 * The column for the database is 'CIP'Cip.
	 */
	public String getCip() {
		if (cip==null) return "";
		if (cip.compareTo("null")==0) return "";
		return cip;
	}
	/** Cip
	 * Set the value of the property Cip.
	 * The column for the database is 'CIP'Cip.
	 */
	public void setCip(String _cip) {
		this.cip=_cip;
		this.cipModified=true;
		this.cipNull=false;
	}
	/** Cip
	 * Set Null the value of the property Cip.
	 * The column for the database is 'CIP'Cip.
	 */
	public void setNullCip() {
		this.cip=null;
		this.cipModified=true;
		this.cipNull=true;
	}
	/** Cip
	 * Indicates if the value of the property Cip is null or not.
	 * The column for the database is 'CIP'Cip.
	 */
	public boolean isNullCip() {
		return cipNull;
	}
	/** Domicilio
	 * Get the value of the property Domicilio.
	 * The column for the database is 'DOMICILIO'Domicilio.
	 */
	public String getDomicilio() {
		if (domicilio==null) return "";
		if (domicilio.compareTo("null")==0) return "";
		return domicilio;
	}
	/** Domicilio
	 * Set the value of the property Domicilio.
	 * The column for the database is 'DOMICILIO'Domicilio.
	 */
	public void setDomicilio(String _domicilio) {
		this.domicilio=_domicilio;
		this.domicilioModified=true;
		this.domicilioNull=false;
	}
	/** Domicilio
	 * Set Null the value of the property Domicilio.
	 * The column for the database is 'DOMICILIO'Domicilio.
	 */
	public void setNullDomicilio() {
		this.domicilio=null;
		this.domicilioModified=true;
		this.domicilioNull=true;
	}
	/** Domicilio
	 * Indicates if the value of the property Domicilio is null or not.
	 * The column for the database is 'DOMICILIO'Domicilio.
	 */
	public boolean isNullDomicilio() {
		return domicilioNull;
	}
	/** FechaNac
	 * Get the value of the property FechaNac.
	 * The column for the database is 'FECHA_NAC'FechaNac.
	 */
	public java.sql.Date getFechaNac() {
		return fechaNac;
	}
	/** FechaNac
	 * Set the value of the property FechaNac.
	 * The column for the database is 'FECHA_NAC'FechaNac.
	 */
	public void setFechaNac(java.sql.Date _fechaNac) {
		this.fechaNac=_fechaNac;
		this.fechaNacModified=true;
		this.fechaNacNull=false;
	}
	/** FechaNac
	 * Set Null the value of the property FechaNac.
	 * The column for the database is 'FECHA_NAC'FechaNac.
	 */
	public void setNullFechaNac() {
		this.fechaNac=null;
		this.fechaNacModified=true;
		this.fechaNacNull=true;
	}
	/** FechaNac
	 * Indicates if the value of the property FechaNac is null or not.
	 * The column for the database is 'FECHA_NAC'FechaNac.
	 */
	public boolean isNullFechaNac() {
		return fechaNacNull;
	}
	/** Poblacion
	 * Get the value of the property Poblacion.
	 * The column for the database is 'POBLACION'Poblacion.
	 */
	public String getPoblacion() {
		if (poblacion==null) return "";
		if (poblacion.compareTo("null")==0) return "";
		return poblacion;
	}
	/** Poblacion
	 * Set the value of the property Poblacion.
	 * The column for the database is 'POBLACION'Poblacion.
	 */
	public void setPoblacion(String _poblacion) {
		this.poblacion=_poblacion;
		this.poblacionModified=true;
		this.poblacionNull=false;
	}
	/** Poblacion
	 * Set Null the value of the property Poblacion.
	 * The column for the database is 'POBLACION'Poblacion.
	 */
	public void setNullPoblacion() {
		this.poblacion=null;
		this.poblacionModified=true;
		this.poblacionNull=true;
	}
	/** Poblacion
	 * Indicates if the value of the property Poblacion is null or not.
	 * The column for the database is 'POBLACION'Poblacion.
	 */
	public boolean isNullPoblacion() {
		return poblacionNull;
	}
	/** Provincia
	 * Get the value of the property Provincia.
	 * The column for the database is 'PROVINCIA'Provincia.
	 */
	public String getProvincia() {
		if (provincia==null) return "";
		if (provincia.compareTo("null")==0) return "";
		return provincia;
	}
	/** Provincia
	 * Set the value of the property Provincia.
	 * The column for the database is 'PROVINCIA'Provincia.
	 */
	public void setProvincia(String _provincia) {
		this.provincia=_provincia;
		this.provinciaModified=true;
		this.provinciaNull=false;
	}
	/** Provincia
	 * Set Null the value of the property Provincia.
	 * The column for the database is 'PROVINCIA'Provincia.
	 */
	public void setNullProvincia() {
		this.provincia=null;
		this.provinciaModified=true;
		this.provinciaNull=true;
	}
	/** Provincia
	 * Indicates if the value of the property Provincia is null or not.
	 * The column for the database is 'PROVINCIA'Provincia.
	 */
	public boolean isNullProvincia() {
		return provinciaNull;
	}
	/** Nhc
	 * Get the value of the property Nhc.
	 * The column for the database is 'NHC'Nhc.
	 */
	public double getNhc() {
		return nhc;
	}
	/** Nhc
	 * Set the value of the property Nhc.
	 * The column for the database is 'NHC'Nhc.
	 */
	public void setNhc(double _nhc) {
		this.nhc=_nhc;
		this.nhcModified=true;
		this.nhcNull=false;
	}
	/** Nhc
	 * Set Null the value of the property Nhc.
	 * The column for the database is 'NHC'Nhc.
	 */
	public void setNullNhc() {
		this.nhc=0;
		this.nhcModified=true;
		this.nhcNull=true;
	}
	/** Nhc
	 * Sumatory of value of the property Nhc.
	 * The column for the database is 'NHC'Nhc.
	 */
	static public double sumNhc(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"NHC");
	}
	/** Nhc
	 * Indicates if the value of the property Nhc is null or not.
	 * The column for the database is 'NHC'Nhc.
	 */
	public boolean isNullNhc() {
		return nhcNull;
	}
	/** Centro
	 * Get the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public double getCentro() {
		return centro;
	}
	/** Centro
	 * Set the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setCentro(double _centro) {
		this.centro=_centro;
		this.centroModified=true;
		this.centroNull=false;
	}
	/** Centro
	 * Set Null the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setNullCentro() {
		this.centro=0;
		this.centroModified=true;
		this.centroNull=true;
	}
	/** Centro
	 * Sumatory of value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	static public double sumCentro(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CENTRO");
	}
	/** Centro
	 * Indicates if the value of the property Centro is null or not.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public boolean isNullCentro() {
		return centroNull;
	}
	/** Prestacion
	 * Get the value of the property Prestacion.
	 * The column for the database is 'PRESTACION'Prestacion.
	 */
	public String getPrestacion() {
		if (prestacion==null) return "";
		if (prestacion.compareTo("null")==0) return "";
		return prestacion;
	}
	/** Prestacion
	 * Set the value of the property Prestacion.
	 * The column for the database is 'PRESTACION'Prestacion.
	 */
	public void setPrestacion(String _prestacion) {
		this.prestacion=_prestacion;
		this.prestacionModified=true;
		this.prestacionNull=false;
	}
	/** Prestacion
	 * Set Null the value of the property Prestacion.
	 * The column for the database is 'PRESTACION'Prestacion.
	 */
	public void setNullPrestacion() {
		this.prestacion=null;
		this.prestacionModified=true;
		this.prestacionNull=true;
	}
	/** Prestacion
	 * Indicates if the value of the property Prestacion is null or not.
	 * The column for the database is 'PRESTACION'Prestacion.
	 */
	public boolean isNullPrestacion() {
		return prestacionNull;
	}
	/** Telefono
	 * Get the value of the property Telefono.
	 * The column for the database is 'TELEFONO'Telefono.
	 */
	public String getTelefono() {
		if (telefono==null) return "";
		if (telefono.compareTo("null")==0) return "";
		return telefono;
	}
	/** Telefono
	 * Set the value of the property Telefono.
	 * The column for the database is 'TELEFONO'Telefono.
	 */
	public void setTelefono(String _telefono) {
		this.telefono=_telefono;
		this.telefonoModified=true;
		this.telefonoNull=false;
	}
	/** Telefono
	 * Set Null the value of the property Telefono.
	 * The column for the database is 'TELEFONO'Telefono.
	 */
	public void setNullTelefono() {
		this.telefono=null;
		this.telefonoModified=true;
		this.telefonoNull=true;
	}
	/** Telefono
	 * Indicates if the value of the property Telefono is null or not.
	 * The column for the database is 'TELEFONO'Telefono.
	 */
	public boolean isNullTelefono() {
		return telefonoNull;
	}
	/** Nss
	 * Get the value of the property Nss.
	 * The column for the database is 'NSS'Nss.
	 */
	public String getNss() {
		if (nss==null) return "";
		if (nss.compareTo("null")==0) return "";
		return nss;
	}
	/** Nss
	 * Set the value of the property Nss.
	 * The column for the database is 'NSS'Nss.
	 */
	public void setNss(String _nss) {
		this.nss=_nss;
		this.nssModified=true;
		this.nssNull=false;
	}
	/** Nss
	 * Set Null the value of the property Nss.
	 * The column for the database is 'NSS'Nss.
	 */
	public void setNullNss() {
		this.nss=null;
		this.nssModified=true;
		this.nssNull=true;
	}
	/** Nss
	 * Indicates if the value of the property Nss is null or not.
	 * The column for the database is 'NSS'Nss.
	 */
	public boolean isNullNss() {
		return nssNull;
	}
	/** GarFechor
	 * Get the value of the property GarFechor.
	 * The column for the database is 'GAR_FECHOR'GarFechor.
	 */
	public java.sql.Date getGarFechor() {
		return garFechor;
	}
	/** GarFechor
	 * Set the value of the property GarFechor.
	 * The column for the database is 'GAR_FECHOR'GarFechor.
	 */
	public void setGarFechor(java.sql.Date _garFechor) {
		this.garFechor=_garFechor;
		this.garFechorModified=true;
		this.garFechorNull=false;
	}
	/** GarFechor
	 * Set Null the value of the property GarFechor.
	 * The column for the database is 'GAR_FECHOR'GarFechor.
	 */
	public void setNullGarFechor() {
		this.garFechor=null;
		this.garFechorModified=true;
		this.garFechorNull=true;
	}
	/** GarFechor
	 * Indicates if the value of the property GarFechor is null or not.
	 * The column for the database is 'GAR_FECHOR'GarFechor.
	 */
	public boolean isNullGarFechor() {
		return garFechorNull;
	}
	/** GarReplegDni
	 * Get the value of the property GarReplegDni.
	 * The column for the database is 'GAR_REPLEG_DNI'GarReplegDni.
	 */
	public String getGarReplegDni() {
		if (garReplegDni==null) return "";
		if (garReplegDni.compareTo("null")==0) return "";
		return garReplegDni;
	}
	/** GarReplegDni
	 * Set the value of the property GarReplegDni.
	 * The column for the database is 'GAR_REPLEG_DNI'GarReplegDni.
	 */
	public void setGarReplegDni(String _garReplegDni) {
		this.garReplegDni=_garReplegDni;
		this.garReplegDniModified=true;
		this.garReplegDniNull=false;
	}
	/** GarReplegDni
	 * Set Null the value of the property GarReplegDni.
	 * The column for the database is 'GAR_REPLEG_DNI'GarReplegDni.
	 */
	public void setNullGarReplegDni() {
		this.garReplegDni=null;
		this.garReplegDniModified=true;
		this.garReplegDniNull=true;
	}
	/** GarReplegDni
	 * Indicates if the value of the property GarReplegDni is null or not.
	 * The column for the database is 'GAR_REPLEG_DNI'GarReplegDni.
	 */
	public boolean isNullGarReplegDni() {
		return garReplegDniNull;
	}
	/** GarReplegApenom
	 * Get the value of the property GarReplegApenom.
	 * The column for the database is 'GAR_REPLEG_APENOM'GarReplegApenom.
	 */
	public String getGarReplegApenom() {
		if (garReplegApenom==null) return "";
		if (garReplegApenom.compareTo("null")==0) return "";
		return garReplegApenom;
	}
	/** GarReplegApenom
	 * Set the value of the property GarReplegApenom.
	 * The column for the database is 'GAR_REPLEG_APENOM'GarReplegApenom.
	 */
	public void setGarReplegApenom(String _garReplegApenom) {
		this.garReplegApenom=_garReplegApenom;
		this.garReplegApenomModified=true;
		this.garReplegApenomNull=false;
	}
	/** GarReplegApenom
	 * Set Null the value of the property GarReplegApenom.
	 * The column for the database is 'GAR_REPLEG_APENOM'GarReplegApenom.
	 */
	public void setNullGarReplegApenom() {
		this.garReplegApenom=null;
		this.garReplegApenomModified=true;
		this.garReplegApenomNull=true;
	}
	/** GarReplegApenom
	 * Indicates if the value of the property GarReplegApenom is null or not.
	 * The column for the database is 'GAR_REPLEG_APENOM'GarReplegApenom.
	 */
	public boolean isNullGarReplegApenom() {
		return garReplegApenomNull;
	}
	/** GarOficina
	 * Get the value of the property GarOficina.
	 * The column for the database is 'GAR_OFICINA'GarOficina.
	 */
	public double getGarOficina() {
		return garOficina;
	}
	/** GarOficina
	 * Set the value of the property GarOficina.
	 * The column for the database is 'GAR_OFICINA'GarOficina.
	 */
	public void setGarOficina(double _garOficina) {
		this.garOficina=_garOficina;
		this.garOficinaModified=true;
		this.garOficinaNull=false;
	}
	/** GarOficina
	 * Set Null the value of the property GarOficina.
	 * The column for the database is 'GAR_OFICINA'GarOficina.
	 */
	public void setNullGarOficina() {
		this.garOficina=0;
		this.garOficinaModified=true;
		this.garOficinaNull=true;
	}
	/** GarOficina
	 * Sumatory of value of the property GarOficina.
	 * The column for the database is 'GAR_OFICINA'GarOficina.
	 */
	static public double sumGarOficina(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_OFICINA");
	}
	/** GarOficina
	 * Indicates if the value of the property GarOficina is null or not.
	 * The column for the database is 'GAR_OFICINA'GarOficina.
	 */
	public boolean isNullGarOficina() {
		return garOficinaNull;
	}
	/** SysUsu
	 * Get the value of the property SysUsu.
	 * The column for the database is 'SYS_USU'SysUsu.
	 */
	public String getSysUsu() {
		if (sysUsu==null) return "";
		if (sysUsu.compareTo("null")==0) return "";
		return sysUsu;
	}
	/** SysUsu
	 * Set the value of the property SysUsu.
	 * The column for the database is 'SYS_USU'SysUsu.
	 */
	public void setSysUsu(String _sysUsu) {
		this.sysUsu=_sysUsu;
		this.sysUsuModified=true;
		this.sysUsuNull=false;
	}
	/** SysUsu
	 * Set Null the value of the property SysUsu.
	 * The column for the database is 'SYS_USU'SysUsu.
	 */
	public void setNullSysUsu() {
		this.sysUsu=null;
		this.sysUsuModified=true;
		this.sysUsuNull=true;
	}
	/** SysUsu
	 * Indicates if the value of the property SysUsu is null or not.
	 * The column for the database is 'SYS_USU'SysUsu.
	 */
	public boolean isNullSysUsu() {
		return sysUsuNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!garMotRechazoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_MOT_RECHAZO");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(garMotRechazo)+"'");
			lb_FirstTime = false;
		}
		if (!nnolespNull) {
			ls_columns.append((lb_FirstTime?"":",")+"NNOLESP");
			ls_values.append((lb_FirstTime?"":",")+nnolesp);
			lb_FirstTime = false;
		}
		if (!nombreNull) {
			ls_columns.append((lb_FirstTime?"":",")+"NOMBRE");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(nombre)+"'");
			lb_FirstTime = false;
		}
		if (!apellido1Null) {
			ls_columns.append((lb_FirstTime?"":",")+"APELLIDO1");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(apellido1)+"'");
			lb_FirstTime = false;
		}
		if (!apellido2Null) {
			ls_columns.append((lb_FirstTime?"":",")+"APELLIDO2");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(apellido2)+"'");
			lb_FirstTime = false;
		}
		if (!cipNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CIP");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(cip)+"'");
			lb_FirstTime = false;
		}
		if (!domicilioNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DOMICILIO");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(domicilio)+"'");
			lb_FirstTime = false;
		}
		if (!fechaNacNull) {
			ls_columns.append((lb_FirstTime?"":",")+"FECHA_NAC");
			ls_values.append((lb_FirstTime?"":",")+"'"+fechaNac.toString() + "'"); 
			lb_FirstTime = false;
		}
		if (!poblacionNull) {
			ls_columns.append((lb_FirstTime?"":",")+"POBLACION");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(poblacion)+"'");
			lb_FirstTime = false;
		}
		if (!provinciaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROVINCIA");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(provincia)+"'");
			lb_FirstTime = false;
		}
		if (!nhcNull) {
			ls_columns.append((lb_FirstTime?"":",")+"NHC");
			ls_values.append((lb_FirstTime?"":",")+nhc);
			lb_FirstTime = false;
		}
		if (!centroNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CENTRO");
			ls_values.append((lb_FirstTime?"":",")+centro);
			lb_FirstTime = false;
		}
		if (!prestacionNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PRESTACION");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(prestacion)+"'");
			lb_FirstTime = false;
		}
		if (!telefonoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"TELEFONO");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(telefono)+"'");
			lb_FirstTime = false;
		}
		if (!nssNull) {
			ls_columns.append((lb_FirstTime?"":",")+"NSS");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(nss)+"'");
			lb_FirstTime = false;
		}
		if (!garFechorNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_FECHOR");
			ls_values.append((lb_FirstTime?"":",")+"'"+garFechor.toString() + "'"); 
			lb_FirstTime = false;
		}
		if (!garReplegDniNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_REPLEG_DNI");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(garReplegDni)+"'");
			lb_FirstTime = false;
		}
		if (!garReplegApenomNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_REPLEG_APENOM");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(garReplegApenom)+"'");
			lb_FirstTime = false;
		}
		if (!garOficinaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_OFICINA");
			ls_values.append((lb_FirstTime?"":",")+garOficina);
			lb_FirstTime = false;
		}
		if (!sysUsuNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_USU");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sysUsu)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO NOLESP ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (garMotRechazoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_MOT_RECHAZO");
			if(garMotRechazoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(garMotRechazo)+"'");
			lb_FirstTime = false;
		}
		if (nnolespModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("NNOLESP");
			if(nnolespNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+nnolesp);
			lb_FirstTime = false;
		}
		if (nombreModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("NOMBRE");
			if(nombreNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(nombre)+"'");
			lb_FirstTime = false;
		}
		if (apellido1Modified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("APELLIDO1");
			if(apellido1Null) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(apellido1)+"'");
			lb_FirstTime = false;
		}
		if (apellido2Modified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("APELLIDO2");
			if(apellido2Null) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(apellido2)+"'");
			lb_FirstTime = false;
		}
		if (cipModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CIP");
			if(cipNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(cip)+"'");
			lb_FirstTime = false;
		}
		if (domicilioModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DOMICILIO");
			if(domicilioNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(domicilio)+"'");
			lb_FirstTime = false;
		}
		if (fechaNacModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("FECHA_NAC");
			if(fechaNacNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+ fechaNac.toString() + "'"); 
			lb_FirstTime = false;
		}
		if (poblacionModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("POBLACION");
			if(poblacionNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(poblacion)+"'");
			lb_FirstTime = false;
		}
		if (provinciaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROVINCIA");
			if(provinciaNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(provincia)+"'");
			lb_FirstTime = false;
		}
		if (nhcModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("NHC");
			if(nhcNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+nhc);
			lb_FirstTime = false;
		}
		if (centroModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CENTRO");
			if(centroNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+centro);
			lb_FirstTime = false;
		}
		if (prestacionModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PRESTACION");
			if(prestacionNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(prestacion)+"'");
			lb_FirstTime = false;
		}
		if (telefonoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("TELEFONO");
			if(telefonoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(telefono)+"'");
			lb_FirstTime = false;
		}
		if (nssModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("NSS");
			if(nssNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(nss)+"'");
			lb_FirstTime = false;
		}
		if (garFechorModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_FECHOR");
			if(garFechorNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+ garFechor.toString() + "'"); 
			lb_FirstTime = false;
		}
		if (garReplegDniModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_REPLEG_DNI");
			if(garReplegDniNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(garReplegDni)+"'");
			lb_FirstTime = false;
		}
		if (garReplegApenomModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_REPLEG_APENOM");
			if(garReplegApenomNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(garReplegApenom)+"'");
			lb_FirstTime = false;
		}
		if (garOficinaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_OFICINA");
			if(garOficinaNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+garOficina);
			lb_FirstTime = false;
		}
		if (sysUsuModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_USU");
			if(sysUsuNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sysUsu)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("NNOLESP");
			ls_where.append("="+nnolesp);
		} else {
			ls_where.append(" AND "+"NNOLESP");
			ls_where.append("="+nnolesp);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE NOLESP SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("NNOLESP");
			ls_where.append("="+nnolesp);
		} else {
			ls_where.append(" AND "+"NNOLESP");
			ls_where.append("="+nnolesp);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE NOLESP WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,double _nnolesp
	 * @return Nolesp - Retrieved object
	 */
	static public Nolesp read(Connection _connection,double _nnolesp) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		Nolesp ls_Nolesp=new Nolesp();
		ls_where="NNOLESP = " + _nnolesp;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM NOLESP WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_Nolesp.loadResultSet(ls_rs);
		} else {
			ls_Nolesp=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_Nolesp;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		garMotRechazo=_rs.getString("GAR_MOT_RECHAZO");
		garMotRechazoNull=_rs.wasNull();
		nnolesp=_rs.getDouble("NNOLESP");
		nnolespNull=_rs.wasNull();
		nombre=_rs.getString("NOMBRE");
		nombreNull=_rs.wasNull();
		apellido1=_rs.getString("APELLIDO1");
		apellido1Null=_rs.wasNull();
		apellido2=_rs.getString("APELLIDO2");
		apellido2Null=_rs.wasNull();
		cip=_rs.getString("CIP");
		cipNull=_rs.wasNull();
		domicilio=_rs.getString("DOMICILIO");
		domicilioNull=_rs.wasNull();
		fechaNac=_rs.getDate("FECHA_NAC");
		fechaNacNull=_rs.wasNull();
		poblacion=_rs.getString("POBLACION");
		poblacionNull=_rs.wasNull();
		provincia=_rs.getString("PROVINCIA");
		provinciaNull=_rs.wasNull();
		nhc=_rs.getDouble("NHC");
		nhcNull=_rs.wasNull();
		centro=_rs.getDouble("CENTRO");
		centroNull=_rs.wasNull();
		prestacion=_rs.getString("PRESTACION");
		prestacionNull=_rs.wasNull();
		telefono=_rs.getString("TELEFONO");
		telefonoNull=_rs.wasNull();
		nss=_rs.getString("NSS");
		nssNull=_rs.wasNull();
		garFechor=_rs.getDate("GAR_FECHOR");
		garFechorNull=_rs.wasNull();
		garReplegDni=_rs.getString("GAR_REPLEG_DNI");
		garReplegDniNull=_rs.wasNull();
		garReplegApenom=_rs.getString("GAR_REPLEG_APENOM");
		garReplegApenomNull=_rs.wasNull();
		garOficina=_rs.getDouble("GAR_OFICINA");
		garOficinaNull=_rs.wasNull();
		sysUsu=_rs.getString("SYS_USU");
		sysUsuNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM NOLESP ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		Nolesp myNolesp;
		Vector myNolespes = new Vector();
		while (ls_rs.next()) {
			myNolesp = new Nolesp();
			myNolesp.loadResultSet (ls_rs);
			myNolespes.addElement(myNolesp);
		}
		ls_rs.close();
		myStatement.close();
		return myNolespes;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM NOLESP ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM NOLESP ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM NOLESP ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
	}
}
