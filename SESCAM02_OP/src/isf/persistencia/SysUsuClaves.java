//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SysUsuClaves
// Table: SYS_USU_CLAVES
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SYS_USU_CLAVES
 * Columns for the table:
 * 	(sysclv_pk - LONG),
 * 	(syslogin - STRING),
 * 	(sysclv_fecha - DATE),
 * 	(sysclv_clave - STRING),
 * Primary columns for the table:
 * 	(sysclv_pk - LONG)
 */
public class SysUsuClaves
{
	// Properties
	private long sysclvPk;
	private boolean sysclvPkNull=true;
	private boolean sysclvPkModified=false;
	private String syslogin;
	private boolean sysloginNull=true;
	private boolean sysloginModified=false;
	private java.sql.Date sysclvFecha;
	private boolean sysclvFechaNull=true;
	private boolean sysclvFechaModified=false;
	private String sysclvClave;
	private boolean sysclvClaveNull=true;
	private boolean sysclvClaveModified=false;
	// Access Method
	/** SysclvPk
	 * Get the value of the property SysclvPk.
	 * The column for the database is 'SYSCLV_PK'SysclvPk.
	 */
	public long getSysclvPk() {
		return sysclvPk;
	}
	/** SysclvPk
	 * Set the value of the property SysclvPk.
	 * The column for the database is 'SYSCLV_PK'SysclvPk.
	 */
	public void setSysclvPk(long _sysclvPk) {
		this.sysclvPk=_sysclvPk;
		this.sysclvPkModified=true;
		this.sysclvPkNull=false;
	}
	/** SysclvPk
	 * Set Null the value of the property SysclvPk.
	 * The column for the database is 'SYSCLV_PK'SysclvPk.
	 */
	public void setNullSysclvPk() {
		this.sysclvPk=0;
		this.sysclvPkModified=true;
		this.sysclvPkNull=true;
	}
	/** SysclvPk
	 * Sumatory of value of the property SysclvPk.
	 * The column for the database is 'SYSCLV_PK'SysclvPk.
	 */
	static public double sumSysclvPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SYSCLV_PK");
	}
	/** SysclvPk
	 * Indicates if the value of the property SysclvPk is null or not.
	 * The column for the database is 'SYSCLV_PK'SysclvPk.
	 */
	public boolean isNullSysclvPk() {
		return sysclvPkNull;
	}
	/** Syslogin
	 * Get the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public String getSyslogin() {
		if (syslogin==null) return "";
		if (syslogin.compareTo("null")==0) return "";
		return syslogin;
	}
	/** Syslogin
	 * Set the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setSyslogin(String _syslogin) {
		this.syslogin=_syslogin;
		this.sysloginModified=true;
		this.sysloginNull=false;
	}
	/** Syslogin
	 * Set Null the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setNullSyslogin() {
		this.syslogin=null;
		this.sysloginModified=true;
		this.sysloginNull=true;
	}
	/** Syslogin
	 * Indicates if the value of the property Syslogin is null or not.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public boolean isNullSyslogin() {
		return sysloginNull;
	}
	/** SysclvFecha
	 * Get the value of the property SysclvFecha.
	 * The column for the database is 'SYSCLV_FECHA'SysclvFecha.
	 */
	public java.sql.Date getSysclvFecha() {
		return sysclvFecha;
	}
	/** SysclvFecha
	 * Set the value of the property SysclvFecha.
	 * The column for the database is 'SYSCLV_FECHA'SysclvFecha.
	 */
	public void setSysclvFecha(java.sql.Date _sysclvFecha) {
		this.sysclvFecha=_sysclvFecha;
		this.sysclvFechaModified=true;
		this.sysclvFechaNull=false;
	}
	/** SysclvFecha
	 * Set Null the value of the property SysclvFecha.
	 * The column for the database is 'SYSCLV_FECHA'SysclvFecha.
	 */
	public void setNullSysclvFecha() {
		this.sysclvFecha=null;
		this.sysclvFechaModified=true;
		this.sysclvFechaNull=true;
	}
	/** SysclvFecha
	 * Indicates if the value of the property SysclvFecha is null or not.
	 * The column for the database is 'SYSCLV_FECHA'SysclvFecha.
	 */
	public boolean isNullSysclvFecha() {
		return sysclvFechaNull;
	}
	/** SysclvClave
	 * Get the value of the property SysclvClave.
	 * The column for the database is 'SYSCLV_CLAVE'SysclvClave.
	 */
	public String getSysclvClave() {
		if (sysclvClave==null) return "";
		if (sysclvClave.compareTo("null")==0) return "";
		return sysclvClave;
	}
	/** SysclvClave
	 * Set the value of the property SysclvClave.
	 * The column for the database is 'SYSCLV_CLAVE'SysclvClave.
	 */
	public void setSysclvClave(String _sysclvClave) {
		this.sysclvClave=_sysclvClave;
		this.sysclvClaveModified=true;
		this.sysclvClaveNull=false;
	}
	/** SysclvClave
	 * Set Null the value of the property SysclvClave.
	 * The column for the database is 'SYSCLV_CLAVE'SysclvClave.
	 */
	public void setNullSysclvClave() {
		this.sysclvClave=null;
		this.sysclvClaveModified=true;
		this.sysclvClaveNull=true;
	}
	/** SysclvClave
	 * Indicates if the value of the property SysclvClave is null or not.
	 * The column for the database is 'SYSCLV_CLAVE'SysclvClave.
	 */
	public boolean isNullSysclvClave() {
		return sysclvClaveNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!sysclvPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYSCLV_PK");
			ls_values.append((lb_FirstTime?"":",")+sysclvPk);
			lb_FirstTime = false;
		}
		if (!sysloginNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYSLOGIN");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(syslogin)+"'");
			lb_FirstTime = false;
		}
		if (!sysclvFechaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYSCLV_FECHA");
			ls_values.append((lb_FirstTime?"":",")+"'"+sysclvFecha.toString() + "'");
			lb_FirstTime = false;
		}
		if (!sysclvClaveNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYSCLV_CLAVE");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sysclvClave)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SYS_USU_CLAVES ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;

		if (sysclvPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYSCLV_PK");
			if(sysclvPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sysclvPk);
			lb_FirstTime = false;
		}
		if (sysloginModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYSLOGIN");
			if(sysloginNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(syslogin)+"'");
			lb_FirstTime = false;
		}
		if (sysclvFechaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYSCLV_FECHA");
			if(sysclvFechaNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+ sysclvFecha.toString() + "'");
			lb_FirstTime = false;
		}
		if (sysclvClaveModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYSCLV_CLAVE");
			if(sysclvClaveNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sysclvClave)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) {
			ls_where.append("SYSCLV_PK");
			ls_where.append("="+sysclvPk);
		} else {
			ls_where.append(" AND "+"SYSCLV_PK");
			ls_where.append("="+sysclvPk);
		}
		lb_FirstTime = false;

		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SYS_USU_CLAVES SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;

		if (lb_FirstTime ) {
			ls_where.append("SYSCLV_PK");
			ls_where.append("="+sysclvPk);
		} else {
			ls_where.append(" AND "+"SYSCLV_PK");
			ls_where.append("="+sysclvPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SYS_USU_CLAVES WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _sysclvPk
	 * @return SysUsuClaves - Retrieved object
	 */
	static public SysUsuClaves read(Connection _connection,long _sysclvPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SysUsuClaves ls_SysUsuClaves=new SysUsuClaves();
		ls_where="SYSCLV_PK = " + _sysclvPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SYS_USU_CLAVES WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SysUsuClaves.loadResultSet(ls_rs);
		} else {
			ls_SysUsuClaves=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SysUsuClaves;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		sysclvPk=_rs.getLong("SYSCLV_PK");
		sysclvPkNull=_rs.wasNull();
		syslogin=_rs.getString("SYSLOGIN");
		sysloginNull=_rs.wasNull();
		sysclvFecha=_rs.getDate("SYSCLV_FECHA");
		sysclvFechaNull=_rs.wasNull();
		sysclvClave=_rs.getString("SYSCLV_CLAVE");
		sysclvClaveNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SYS_USU_CLAVES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SysUsuClaves mySysUsuClaves;
		Vector mySysUsuClaveses = new Vector();
		while (ls_rs.next()) {
			mySysUsuClaves = new SysUsuClaves();
			mySysUsuClaves.loadResultSet (ls_rs);
			mySysUsuClaveses.addElement(mySysUsuClaves);
		}
		ls_rs.close();
		myStatement.close();
		return mySysUsuClaveses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SYS_USU_CLAVES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SYS_USU_CLAVES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SYS_USU_CLAVES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setSysclvPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
