//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: Clave
// Table: CLAVE
// Package: ssf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:CLAVE
 * Columns for the table:
 * 	(clave_nombre - STRING),
 * 	(clave_numero_actual - LONG),
 */
public class Clave
{
	// Properties
	private String claveNombre;
	private boolean claveNombreNull=true;
	private boolean claveNombreModified=false;
	private long claveNumeroActual;
	private boolean claveNumeroActualNull=true;
	private boolean claveNumeroActualModified=false;
	// Access Method
	/** ClaveNombre
	 * Get the value of the property ClaveNombre.
	 * The column for the database is 'CLAVE_NOMBRE'ClaveNombre.
	 */
	public String getClaveNombre() {
		if (claveNombre==null) return "";
		if (claveNombre.compareTo("null")==0) return "";
		return claveNombre;
	}
	/** ClaveNombre
	 * Set the value of the property ClaveNombre.
	 * The column for the database is 'CLAVE_NOMBRE'ClaveNombre.
	 */
	public void setClaveNombre(String _claveNombre) {
		this.claveNombre=_claveNombre;
		this.claveNombreModified=true;
		this.claveNombreNull=false;
	}
	/** ClaveNombre
	 * Set Null the value of the property ClaveNombre.
	 * The column for the database is 'CLAVE_NOMBRE'ClaveNombre.
	 */
	public void setNullClaveNombre() {
		this.claveNombre=null;
		this.claveNombreModified=true;
		this.claveNombreNull=true;
	}
	/** ClaveNombre
	 * Indicates if the value of the property ClaveNombre is null or not.
	 * The column for the database is 'CLAVE_NOMBRE'ClaveNombre.
	 */
	public boolean isNullClaveNombre() {
		return claveNombreNull;
	}
	/** ClaveNumeroActual
	 * Get the value of the property ClaveNumeroActual.
	 * The column for the database is 'CLAVE_NUMERO_ACTUAL'ClaveNumeroActual.
	 */
	public long getClaveNumeroActual() {
		return claveNumeroActual;
	}
	/** ClaveNumeroActual
	 * Set the value of the property ClaveNumeroActual.
	 * The column for the database is 'CLAVE_NUMERO_ACTUAL'ClaveNumeroActual.
	 */
	public void setClaveNumeroActual(long _claveNumeroActual) {
		this.claveNumeroActual=_claveNumeroActual;
		this.claveNumeroActualModified=true;
		this.claveNumeroActualNull=false;
	}
	/** ClaveNumeroActual
	 * Set Null the value of the property ClaveNumeroActual.
	 * The column for the database is 'CLAVE_NUMERO_ACTUAL'ClaveNumeroActual.
	 */
	public void setNullClaveNumeroActual() {
		this.claveNumeroActual=0;
		this.claveNumeroActualModified=true;
		this.claveNumeroActualNull=true;
	}
	/** ClaveNumeroActual
	 * Sumatory of value of the property ClaveNumeroActual.
	 * The column for the database is 'CLAVE_NUMERO_ACTUAL'ClaveNumeroActual.
	 */
	static public double sumClaveNumeroActual(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CLAVE_NUMERO_ACTUAL");
	}
	/** ClaveNumeroActual
	 * Indicates if the value of the property ClaveNumeroActual is null or not.
	 * The column for the database is 'CLAVE_NUMERO_ACTUAL'ClaveNumeroActual.
	 */
	public boolean isNullClaveNumeroActual() {
		return claveNumeroActualNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!claveNombreNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CLAVE_NOMBRE");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(claveNombre)+"'");
			lb_FirstTime = false;
		}
		if (!claveNumeroActualNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CLAVE_NUMERO_ACTUAL");
			ls_values.append((lb_FirstTime?"":",")+claveNumeroActual);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO CLAVE ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		claveNombre=_rs.getString("CLAVE_NOMBRE");
		claveNombreNull=_rs.wasNull();
		claveNumeroActual=_rs.getLong("CLAVE_NUMERO_ACTUAL");
		claveNumeroActualNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM CLAVE ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		Clave myClave;
		Vector myClavees = new Vector();
		while (ls_rs.next()) {
			myClave = new Clave();
			myClave.loadResultSet (ls_rs);
			myClavees.addElement(myClave);
		}
		ls_rs.close();
		myStatement.close();
		return myClavees;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM CLAVE ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM CLAVE ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM CLAVE ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
}
