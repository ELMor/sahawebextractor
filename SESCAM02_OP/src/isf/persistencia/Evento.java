//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: Evento
// Table: EVENTO
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:EVENTO
 * Columns for the table:
 * 	(gar_pk - LONG), 
 * 	(evto_pk - LONG), 
 * 	(tpoevto_pk - LONG), 
 * 	(sess_pk - LONG), 
 * 	(gar_operacion - LONG), 
 * Primary columns for the table:
 * 	(evto_pk - LONG) 
 */
public class Evento
{
	// Properties
	private long garPk;
	private boolean garPkNull=true;
	private boolean garPkModified=false;
	private long evtoPk;
	private boolean evtoPkNull=true;
	private boolean evtoPkModified=false;
	private long tpoevtoPk;
	private boolean tpoevtoPkNull=true;
	private boolean tpoevtoPkModified=false;
	private long sessPk;
	private boolean sessPkNull=true;
	private boolean sessPkModified=false;
	private long garOperacion;
	private boolean garOperacionNull=true;
	private boolean garOperacionModified=false;
	// Access Method
	/** GarPk
	 * Get the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public long getGarPk() {
		return garPk;
	}
	/** GarPk
	 * Set the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public void setGarPk(long _garPk) {
		this.garPk=_garPk;
		this.garPkModified=true;
		this.garPkNull=false;
	}
	/** GarPk
	 * Set Null the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public void setNullGarPk() {
		this.garPk=0;
		this.garPkModified=true;
		this.garPkNull=true;
	}
	/** GarPk
	 * Sumatory of value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	static public double sumGarPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_PK");
	}
	/** GarPk
	 * Indicates if the value of the property GarPk is null or not.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public boolean isNullGarPk() {
		return garPkNull;
	}
	/** EvtoPk
	 * Get the value of the property EvtoPk.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	public long getEvtoPk() {
		return evtoPk;
	}
	/** EvtoPk
	 * Set the value of the property EvtoPk.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	public void setEvtoPk(long _evtoPk) {
		this.evtoPk=_evtoPk;
		this.evtoPkModified=true;
		this.evtoPkNull=false;
	}
	/** EvtoPk
	 * Set Null the value of the property EvtoPk.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	public void setNullEvtoPk() {
		this.evtoPk=0;
		this.evtoPkModified=true;
		this.evtoPkNull=true;
	}
	/** EvtoPk
	 * Sumatory of value of the property EvtoPk.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	static public double sumEvtoPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"EVTO_PK");
	}
	/** EvtoPk
	 * Indicates if the value of the property EvtoPk is null or not.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	public boolean isNullEvtoPk() {
		return evtoPkNull;
	}
	/** TpoevtoPk
	 * Get the value of the property TpoevtoPk.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	public long getTpoevtoPk() {
		return tpoevtoPk;
	}
	/** TpoevtoPk
	 * Set the value of the property TpoevtoPk.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	public void setTpoevtoPk(long _tpoevtoPk) {
		this.tpoevtoPk=_tpoevtoPk;
		this.tpoevtoPkModified=true;
		this.tpoevtoPkNull=false;
	}
	/** TpoevtoPk
	 * Set Null the value of the property TpoevtoPk.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	public void setNullTpoevtoPk() {
		this.tpoevtoPk=0;
		this.tpoevtoPkModified=true;
		this.tpoevtoPkNull=true;
	}
	/** TpoevtoPk
	 * Sumatory of value of the property TpoevtoPk.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	static public double sumTpoevtoPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"TPOEVTO_PK");
	}
	/** TpoevtoPk
	 * Indicates if the value of the property TpoevtoPk is null or not.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	public boolean isNullTpoevtoPk() {
		return tpoevtoPkNull;
	}
	/** SessPk
	 * Get the value of the property SessPk.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	public long getSessPk() {
		return sessPk;
	}
	/** SessPk
	 * Set the value of the property SessPk.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	public void setSessPk(long _sessPk) {
		this.sessPk=_sessPk;
		this.sessPkModified=true;
		this.sessPkNull=false;
	}
	/** SessPk
	 * Set Null the value of the property SessPk.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	public void setNullSessPk() {
		this.sessPk=0;
		this.sessPkModified=true;
		this.sessPkNull=true;
	}
	/** SessPk
	 * Sumatory of value of the property SessPk.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	static public double sumSessPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SESS_PK");
	}
	/** SessPk
	 * Indicates if the value of the property SessPk is null or not.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	public boolean isNullSessPk() {
		return sessPkNull;
	}
	/** GarOperacion
	 * Get the value of the property GarOperacion.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	public long getGarOperacion() {
		return garOperacion;
	}
	/** GarOperacion
	 * Set the value of the property GarOperacion.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	public void setGarOperacion(long _garOperacion) {
		this.garOperacion=_garOperacion;
		this.garOperacionModified=true;
		this.garOperacionNull=false;
	}
	/** GarOperacion
	 * Set Null the value of the property GarOperacion.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	public void setNullGarOperacion() {
		this.garOperacion=0;
		this.garOperacionModified=true;
		this.garOperacionNull=true;
	}
	/** GarOperacion
	 * Sumatory of value of the property GarOperacion.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	static public double sumGarOperacion(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_OPERACION");
	}
	/** GarOperacion
	 * Indicates if the value of the property GarOperacion is null or not.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	public boolean isNullGarOperacion() {
		return garOperacionNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!garPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_PK");
			ls_values.append((lb_FirstTime?"":",")+garPk);
			lb_FirstTime = false;
		}
		if (!evtoPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"EVTO_PK");
			ls_values.append((lb_FirstTime?"":",")+evtoPk);
			lb_FirstTime = false;
		}
		if (!tpoevtoPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"TPOEVTO_PK");
			ls_values.append((lb_FirstTime?"":",")+tpoevtoPk);
			lb_FirstTime = false;
		}
		if (!sessPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESS_PK");
			ls_values.append((lb_FirstTime?"":",")+sessPk);
			lb_FirstTime = false;
		}
		if (!garOperacionNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_OPERACION");
			ls_values.append((lb_FirstTime?"":",")+garOperacion);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO EVENTO ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (garPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_PK");
			if(garPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+garPk);
			lb_FirstTime = false;
		}
		if (evtoPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("EVTO_PK");
			if(evtoPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+evtoPk);
			lb_FirstTime = false;
		}
		if (tpoevtoPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("TPOEVTO_PK");
			if(tpoevtoPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+tpoevtoPk);
			lb_FirstTime = false;
		}
		if (sessPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESS_PK");
			if(sessPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sessPk);
			lb_FirstTime = false;
		}
		if (garOperacionModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_OPERACION");
			if(garOperacionNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+garOperacion);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("EVTO_PK");
			ls_where.append("="+evtoPk);
		} else {
			ls_where.append(" AND "+"EVTO_PK");
			ls_where.append("="+evtoPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE EVENTO SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("EVTO_PK");
			ls_where.append("="+evtoPk);
		} else {
			ls_where.append(" AND "+"EVTO_PK");
			ls_where.append("="+evtoPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE EVENTO WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _evtoPk
	 * @return Evento - Retrieved object
	 */
	static public Evento read(Connection _connection,long _evtoPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		Evento ls_Evento=new Evento();
		ls_where="EVTO_PK = " + _evtoPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM EVENTO WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_Evento.loadResultSet(ls_rs);
		} else {
			ls_Evento=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_Evento;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		garPk=_rs.getLong("GAR_PK");
		garPkNull=_rs.wasNull();
		evtoPk=_rs.getLong("EVTO_PK");
		evtoPkNull=_rs.wasNull();
		tpoevtoPk=_rs.getLong("TPOEVTO_PK");
		tpoevtoPkNull=_rs.wasNull();
		sessPk=_rs.getLong("SESS_PK");
		sessPkNull=_rs.wasNull();
		garOperacion=_rs.getLong("GAR_OPERACION");
		garOperacionNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM EVENTO ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		Evento myEvento;
		Vector myEventoes = new Vector();
		while (ls_rs.next()) {
			myEvento = new Evento();
			myEvento.loadResultSet (ls_rs);
			myEventoes.addElement(myEvento);
		}
		ls_rs.close();
		myStatement.close();
		return myEventoes;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM EVENTO ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM EVENTO ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM EVENTO ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setEvtoPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
