//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesTramos
// Table: SES_TRAMOS
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_TRAMOS
 * Columns for the table:
 * 	(codtramo - LONG), 
 * 	(posicion - LONG), 
 * 	(inicio - DECIMAL), 
 * 	(fin - DECIMAL), 
 * 	(descripcion - STRING), 
 * Primary columns for the table:
 * 	(codtramo - LONG) 
 * 	(posicion - LONG) 
 */
public class SesTramos
{
	// Properties
	private long codtramo;
	private boolean codtramoNull=true;
	private boolean codtramoModified=false;
	private long posicion;
	private boolean posicionNull=true;
	private boolean posicionModified=false;
	private double inicio;
	private boolean inicioNull=true;
	private boolean inicioModified=false;
	private double fin;
	private boolean finNull=true;
	private boolean finModified=false;
	private String descripcion;
	private boolean descripcionNull=true;
	private boolean descripcionModified=false;
	// Access Method
	/** Codtramo
	 * Get the value of the property Codtramo.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	public long getCodtramo() {
		return codtramo;
	}
	/** Codtramo
	 * Set the value of the property Codtramo.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	public void setCodtramo(long _codtramo) {
		this.codtramo=_codtramo;
		this.codtramoModified=true;
		this.codtramoNull=false;
	}
	/** Codtramo
	 * Set Null the value of the property Codtramo.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	public void setNullCodtramo() {
		this.codtramo=0;
		this.codtramoModified=true;
		this.codtramoNull=true;
	}
	/** Codtramo
	 * Sumatory of value of the property Codtramo.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	static public double sumCodtramo(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CODTRAMO");
	}
	/** Codtramo
	 * Indicates if the value of the property Codtramo is null or not.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	public boolean isNullCodtramo() {
		return codtramoNull;
	}
	/** Posicion
	 * Get the value of the property Posicion.
	 * The column for the database is 'POSICION'Posicion.
	 */
	public long getPosicion() {
		return posicion;
	}
	/** Posicion
	 * Set the value of the property Posicion.
	 * The column for the database is 'POSICION'Posicion.
	 */
	public void setPosicion(long _posicion) {
		this.posicion=_posicion;
		this.posicionModified=true;
		this.posicionNull=false;
	}
	/** Posicion
	 * Set Null the value of the property Posicion.
	 * The column for the database is 'POSICION'Posicion.
	 */
	public void setNullPosicion() {
		this.posicion=0;
		this.posicionModified=true;
		this.posicionNull=true;
	}
	/** Posicion
	 * Sumatory of value of the property Posicion.
	 * The column for the database is 'POSICION'Posicion.
	 */
	static public double sumPosicion(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"POSICION");
	}
	/** Posicion
	 * Indicates if the value of the property Posicion is null or not.
	 * The column for the database is 'POSICION'Posicion.
	 */
	public boolean isNullPosicion() {
		return posicionNull;
	}
	/** Inicio
	 * Get the value of the property Inicio.
	 * The column for the database is 'INICIO'Inicio.
	 */
	public double getInicio() {
		return inicio;
	}
	/** Inicio
	 * Set the value of the property Inicio.
	 * The column for the database is 'INICIO'Inicio.
	 */
	public void setInicio(double _inicio) {
		this.inicio=_inicio;
		this.inicioModified=true;
		this.inicioNull=false;
	}
	/** Inicio
	 * Set Null the value of the property Inicio.
	 * The column for the database is 'INICIO'Inicio.
	 */
	public void setNullInicio() {
		this.inicio=0;
		this.inicioModified=true;
		this.inicioNull=true;
	}
	/** Inicio
	 * Sumatory of value of the property Inicio.
	 * The column for the database is 'INICIO'Inicio.
	 */
	static public double sumInicio(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"INICIO");
	}
	/** Inicio
	 * Indicates if the value of the property Inicio is null or not.
	 * The column for the database is 'INICIO'Inicio.
	 */
	public boolean isNullInicio() {
		return inicioNull;
	}
	/** Fin
	 * Get the value of the property Fin.
	 * The column for the database is 'FIN'Fin.
	 */
	public double getFin() {
		return fin;
	}
	/** Fin
	 * Set the value of the property Fin.
	 * The column for the database is 'FIN'Fin.
	 */
	public void setFin(double _fin) {
		this.fin=_fin;
		this.finModified=true;
		this.finNull=false;
	}
	/** Fin
	 * Set Null the value of the property Fin.
	 * The column for the database is 'FIN'Fin.
	 */
	public void setNullFin() {
		this.fin=0;
		this.finModified=true;
		this.finNull=true;
	}
	/** Fin
	 * Sumatory of value of the property Fin.
	 * The column for the database is 'FIN'Fin.
	 */
	static public double sumFin(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"FIN");
	}
	/** Fin
	 * Indicates if the value of the property Fin is null or not.
	 * The column for the database is 'FIN'Fin.
	 */
	public boolean isNullFin() {
		return finNull;
	}
	/** Descripcion
	 * Get the value of the property Descripcion.
	 * The column for the database is 'DESCRIPCION'Descripcion.
	 */
	public String getDescripcion() {
		if (descripcion==null) return "";
		if (descripcion.compareTo("null")==0) return "";
		return descripcion;
	}
	/** Descripcion
	 * Set the value of the property Descripcion.
	 * The column for the database is 'DESCRIPCION'Descripcion.
	 */
	public void setDescripcion(String _descripcion) {
		this.descripcion=_descripcion;
		this.descripcionModified=true;
		this.descripcionNull=false;
	}
	/** Descripcion
	 * Set Null the value of the property Descripcion.
	 * The column for the database is 'DESCRIPCION'Descripcion.
	 */
	public void setNullDescripcion() {
		this.descripcion=null;
		this.descripcionModified=true;
		this.descripcionNull=true;
	}
	/** Descripcion
	 * Indicates if the value of the property Descripcion is null or not.
	 * The column for the database is 'DESCRIPCION'Descripcion.
	 */
	public boolean isNullDescripcion() {
		return descripcionNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!codtramoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CODTRAMO");
			ls_values.append((lb_FirstTime?"":",")+codtramo);
			lb_FirstTime = false;
		}
		if (!posicionNull) {
			ls_columns.append((lb_FirstTime?"":",")+"POSICION");
			ls_values.append((lb_FirstTime?"":",")+posicion);
			lb_FirstTime = false;
		}
		if (!inicioNull) {
			ls_columns.append((lb_FirstTime?"":",")+"INICIO");
			ls_values.append((lb_FirstTime?"":",")+inicio);
			lb_FirstTime = false;
		}
		if (!finNull) {
			ls_columns.append((lb_FirstTime?"":",")+"FIN");
			ls_values.append((lb_FirstTime?"":",")+fin);
			lb_FirstTime = false;
		}
		if (!descripcionNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DESCRIPCION");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(descripcion)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_TRAMOS ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (codtramoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CODTRAMO");
			if(codtramoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+codtramo);
			lb_FirstTime = false;
		}
		if (posicionModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("POSICION");
			if(posicionNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+posicion);
			lb_FirstTime = false;
		}
		if (inicioModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("INICIO");
			if(inicioNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+inicio);
			lb_FirstTime = false;
		}
		if (finModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("FIN");
			if(finNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+fin);
			lb_FirstTime = false;
		}
		if (descripcionModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DESCRIPCION");
			if(descripcionNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(descripcion)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("CODTRAMO");
			ls_where.append("="+codtramo);
		} else {
			ls_where.append(" AND "+"CODTRAMO");
			ls_where.append("="+codtramo);
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("POSICION");
			ls_where.append("="+posicion);
		} else {
			ls_where.append(" AND "+"POSICION");
			ls_where.append("="+posicion);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_TRAMOS SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("CODTRAMO");
			ls_where.append("="+codtramo);
		} else {
			ls_where.append(" AND "+"CODTRAMO");
			ls_where.append("="+codtramo);
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("POSICION");
			ls_where.append("="+posicion);
		} else {
			ls_where.append(" AND "+"POSICION");
			ls_where.append("="+posicion);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_TRAMOS WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _codtramo,long _posicion
	 * @return SesTramos - Retrieved object
	 */
	static public SesTramos read(Connection _connection,long _codtramo,long _posicion) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesTramos ls_SesTramos=new SesTramos();
		ls_where="CODTRAMO = " + _codtramo + " AND " + "POSICION = " + _posicion;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_TRAMOS WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesTramos.loadResultSet(ls_rs);
		} else {
			ls_SesTramos=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesTramos;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		codtramo=_rs.getLong("CODTRAMO");
		codtramoNull=_rs.wasNull();
		posicion=_rs.getLong("POSICION");
		posicionNull=_rs.wasNull();
		inicio=_rs.getDouble("INICIO");
		inicioNull=_rs.wasNull();
		fin=_rs.getDouble("FIN");
		finNull=_rs.wasNull();
		descripcion=_rs.getString("DESCRIPCION");
		descripcionNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_TRAMOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesTramos mySesTramos;
		Vector mySesTramoses = new Vector();
		while (ls_rs.next()) {
			mySesTramos = new SesTramos();
			mySesTramos.loadResultSet (ls_rs);
			mySesTramoses.addElement(mySesTramos);
		}
		ls_rs.close();
		myStatement.close();
		return mySesTramoses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_TRAMOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_TRAMOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_TRAMOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setCodtramo(myPool.getSequence(this.getClass().getName(),profile));
		setPosicion(myPool.getSequence(this.getClass().getName(),profile));
	}
}
