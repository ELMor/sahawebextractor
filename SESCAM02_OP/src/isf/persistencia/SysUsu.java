//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SysUsu
// Table: SYS_USU
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SYS_USU
 * Columns for the table:
 * 	(fecha_bloqueo - DATE), 
 * 	(syslogin - STRING), 
 * 	(sys_rol_pk - LONG), 
 * 	(sys_cargos_pk - LONG), 
 * 	(oficina_pk - LONG), 
 * 	(syspwd - STRING), 
 * 	(apellido1 - STRING), 
 * 	(apellido2 - STRING), 
 * 	(nombre - STRING), 
 * 	(activo_sn - LONG), 
 * Primary columns for the table:
 * 	(syslogin - STRING) 
 */
public class SysUsu
{
	// Properties
	private java.sql.Date fechaBloqueo;
	private boolean fechaBloqueoNull=true;
	private boolean fechaBloqueoModified=false;
	private String syslogin;
	private boolean sysloginNull=true;
	private boolean sysloginModified=false;
	private long sysRolPk;
	private boolean sysRolPkNull=true;
	private boolean sysRolPkModified=false;
	private long sysCargosPk;
	private boolean sysCargosPkNull=true;
	private boolean sysCargosPkModified=false;
	private long oficinaPk;
	private boolean oficinaPkNull=true;
	private boolean oficinaPkModified=false;
	private String syspwd;
	private boolean syspwdNull=true;
	private boolean syspwdModified=false;
	private String apellido1;
	private boolean apellido1Null=true;
	private boolean apellido1Modified=false;
	private String apellido2;
	private boolean apellido2Null=true;
	private boolean apellido2Modified=false;
	private String nombre;
	private boolean nombreNull=true;
	private boolean nombreModified=false;
	private long activoSn;
	private boolean activoSnNull=true;
	private boolean activoSnModified=false;
	// Access Method
	/** FechaBloqueo
	 * Get the value of the property FechaBloqueo.
	 * The column for the database is 'FECHA_BLOQUEO'FechaBloqueo.
	 */
	public java.sql.Date getFechaBloqueo() {
		return fechaBloqueo;
	}
	/** FechaBloqueo
	 * Set the value of the property FechaBloqueo.
	 * The column for the database is 'FECHA_BLOQUEO'FechaBloqueo.
	 */
	public void setFechaBloqueo(java.sql.Date _fechaBloqueo) {
		this.fechaBloqueo=_fechaBloqueo;
		this.fechaBloqueoModified=true;
		this.fechaBloqueoNull=false;
	}
	/** FechaBloqueo
	 * Set Null the value of the property FechaBloqueo.
	 * The column for the database is 'FECHA_BLOQUEO'FechaBloqueo.
	 */
	public void setNullFechaBloqueo() {
		this.fechaBloqueo=null;
		this.fechaBloqueoModified=true;
		this.fechaBloqueoNull=true;
	}
	/** FechaBloqueo
	 * Indicates if the value of the property FechaBloqueo is null or not.
	 * The column for the database is 'FECHA_BLOQUEO'FechaBloqueo.
	 */
	public boolean isNullFechaBloqueo() {
		return fechaBloqueoNull;
	}
	/** Syslogin
	 * Get the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public String getSyslogin() {
		if (syslogin==null) return "";
		if (syslogin.compareTo("null")==0) return "";
		return syslogin;
	}
	/** Syslogin
	 * Set the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setSyslogin(String _syslogin) {
		this.syslogin=_syslogin;
		this.sysloginModified=true;
		this.sysloginNull=false;
	}
	/** Syslogin
	 * Set Null the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setNullSyslogin() {
		this.syslogin=null;
		this.sysloginModified=true;
		this.sysloginNull=true;
	}
	/** Syslogin
	 * Indicates if the value of the property Syslogin is null or not.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public boolean isNullSyslogin() {
		return sysloginNull;
	}
	/** SysRolPk
	 * Get the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public long getSysRolPk() {
		return sysRolPk;
	}
	/** SysRolPk
	 * Set the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public void setSysRolPk(long _sysRolPk) {
		this.sysRolPk=_sysRolPk;
		this.sysRolPkModified=true;
		this.sysRolPkNull=false;
	}
	/** SysRolPk
	 * Set Null the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public void setNullSysRolPk() {
		this.sysRolPk=0;
		this.sysRolPkModified=true;
		this.sysRolPkNull=true;
	}
	/** SysRolPk
	 * Sumatory of value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	static public double sumSysRolPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SYS_ROL_PK");
	}
	/** SysRolPk
	 * Indicates if the value of the property SysRolPk is null or not.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public boolean isNullSysRolPk() {
		return sysRolPkNull;
	}
	/** SysCargosPk
	 * Get the value of the property SysCargosPk.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	public long getSysCargosPk() {
		return sysCargosPk;
	}
	/** SysCargosPk
	 * Set the value of the property SysCargosPk.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	public void setSysCargosPk(long _sysCargosPk) {
		this.sysCargosPk=_sysCargosPk;
		this.sysCargosPkModified=true;
		this.sysCargosPkNull=false;
	}
	/** SysCargosPk
	 * Set Null the value of the property SysCargosPk.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	public void setNullSysCargosPk() {
		this.sysCargosPk=0;
		this.sysCargosPkModified=true;
		this.sysCargosPkNull=true;
	}
	/** SysCargosPk
	 * Sumatory of value of the property SysCargosPk.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	static public double sumSysCargosPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SYS_CARGOS_PK");
	}
	/** SysCargosPk
	 * Indicates if the value of the property SysCargosPk is null or not.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	public boolean isNullSysCargosPk() {
		return sysCargosPkNull;
	}
	/** OficinaPk
	 * Get the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public long getOficinaPk() {
		return oficinaPk;
	}
	/** OficinaPk
	 * Set the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public void setOficinaPk(long _oficinaPk) {
		this.oficinaPk=_oficinaPk;
		this.oficinaPkModified=true;
		this.oficinaPkNull=false;
	}
	/** OficinaPk
	 * Set Null the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public void setNullOficinaPk() {
		this.oficinaPk=0;
		this.oficinaPkModified=true;
		this.oficinaPkNull=true;
	}
	/** OficinaPk
	 * Sumatory of value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	static public double sumOficinaPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"OFICINA_PK");
	}
	/** OficinaPk
	 * Indicates if the value of the property OficinaPk is null or not.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public boolean isNullOficinaPk() {
		return oficinaPkNull;
	}
	/** Syspwd
	 * Get the value of the property Syspwd.
	 * The column for the database is 'SYSPWD'Syspwd.
	 */
	public String getSyspwd() {
		if (syspwd==null) return "";
		if (syspwd.compareTo("null")==0) return "";
		return syspwd;
	}
	/** Syspwd
	 * Set the value of the property Syspwd.
	 * The column for the database is 'SYSPWD'Syspwd.
	 */
	public void setSyspwd(String _syspwd) {
		this.syspwd=_syspwd;
		this.syspwdModified=true;
		this.syspwdNull=false;
	}
	/** Syspwd
	 * Set Null the value of the property Syspwd.
	 * The column for the database is 'SYSPWD'Syspwd.
	 */
	public void setNullSyspwd() {
		this.syspwd=null;
		this.syspwdModified=true;
		this.syspwdNull=true;
	}
	/** Syspwd
	 * Indicates if the value of the property Syspwd is null or not.
	 * The column for the database is 'SYSPWD'Syspwd.
	 */
	public boolean isNullSyspwd() {
		return syspwdNull;
	}
	/** Apellido1
	 * Get the value of the property Apellido1.
	 * The column for the database is 'APELLIDO1'Apellido1.
	 */
	public String getApellido1() {
		if (apellido1==null) return "";
		if (apellido1.compareTo("null")==0) return "";
		return apellido1;
	}
	/** Apellido1
	 * Set the value of the property Apellido1.
	 * The column for the database is 'APELLIDO1'Apellido1.
	 */
	public void setApellido1(String _apellido1) {
		this.apellido1=_apellido1;
		this.apellido1Modified=true;
		this.apellido1Null=false;
	}
	/** Apellido1
	 * Set Null the value of the property Apellido1.
	 * The column for the database is 'APELLIDO1'Apellido1.
	 */
	public void setNullApellido1() {
		this.apellido1=null;
		this.apellido1Modified=true;
		this.apellido1Null=true;
	}
	/** Apellido1
	 * Indicates if the value of the property Apellido1 is null or not.
	 * The column for the database is 'APELLIDO1'Apellido1.
	 */
	public boolean isNullApellido1() {
		return apellido1Null;
	}
	/** Apellido2
	 * Get the value of the property Apellido2.
	 * The column for the database is 'APELLIDO2'Apellido2.
	 */
	public String getApellido2() {
		if (apellido2==null) return "";
		if (apellido2.compareTo("null")==0) return "";
		return apellido2;
	}
	/** Apellido2
	 * Set the value of the property Apellido2.
	 * The column for the database is 'APELLIDO2'Apellido2.
	 */
	public void setApellido2(String _apellido2) {
		this.apellido2=_apellido2;
		this.apellido2Modified=true;
		this.apellido2Null=false;
	}
	/** Apellido2
	 * Set Null the value of the property Apellido2.
	 * The column for the database is 'APELLIDO2'Apellido2.
	 */
	public void setNullApellido2() {
		this.apellido2=null;
		this.apellido2Modified=true;
		this.apellido2Null=true;
	}
	/** Apellido2
	 * Indicates if the value of the property Apellido2 is null or not.
	 * The column for the database is 'APELLIDO2'Apellido2.
	 */
	public boolean isNullApellido2() {
		return apellido2Null;
	}
	/** Nombre
	 * Get the value of the property Nombre.
	 * The column for the database is 'NOMBRE'Nombre.
	 */
	public String getNombre() {
		if (nombre==null) return "";
		if (nombre.compareTo("null")==0) return "";
		return nombre;
	}
	/** Nombre
	 * Set the value of the property Nombre.
	 * The column for the database is 'NOMBRE'Nombre.
	 */
	public void setNombre(String _nombre) {
		this.nombre=_nombre;
		this.nombreModified=true;
		this.nombreNull=false;
	}
	/** Nombre
	 * Set Null the value of the property Nombre.
	 * The column for the database is 'NOMBRE'Nombre.
	 */
	public void setNullNombre() {
		this.nombre=null;
		this.nombreModified=true;
		this.nombreNull=true;
	}
	/** Nombre
	 * Indicates if the value of the property Nombre is null or not.
	 * The column for the database is 'NOMBRE'Nombre.
	 */
	public boolean isNullNombre() {
		return nombreNull;
	}
	/** ActivoSn
	 * Get the value of the property ActivoSn.
	 * The column for the database is 'ACTIVO_SN'ActivoSn.
	 */
	public long getActivoSn() {
		return activoSn;
	}
	/** ActivoSn
	 * Set the value of the property ActivoSn.
	 * The column for the database is 'ACTIVO_SN'ActivoSn.
	 */
	public void setActivoSn(long _activoSn) {
		this.activoSn=_activoSn;
		this.activoSnModified=true;
		this.activoSnNull=false;
	}
	/** ActivoSn
	 * Set Null the value of the property ActivoSn.
	 * The column for the database is 'ACTIVO_SN'ActivoSn.
	 */
	public void setNullActivoSn() {
		this.activoSn=0;
		this.activoSnModified=true;
		this.activoSnNull=true;
	}
	/** ActivoSn
	 * Sumatory of value of the property ActivoSn.
	 * The column for the database is 'ACTIVO_SN'ActivoSn.
	 */
	static public double sumActivoSn(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"ACTIVO_SN");
	}
	/** ActivoSn
	 * Indicates if the value of the property ActivoSn is null or not.
	 * The column for the database is 'ACTIVO_SN'ActivoSn.
	 */
	public boolean isNullActivoSn() {
		return activoSnNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!fechaBloqueoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"FECHA_BLOQUEO");
			ls_values.append((lb_FirstTime?"":",")+"'"+fechaBloqueo.toString() + "'"); 
			lb_FirstTime = false;
		}
		if (!sysloginNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYSLOGIN");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(syslogin)+"'");
			lb_FirstTime = false;
		}
		if (!sysRolPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_ROL_PK");
			ls_values.append((lb_FirstTime?"":",")+sysRolPk);
			lb_FirstTime = false;
		}
		if (!sysCargosPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_CARGOS_PK");
			ls_values.append((lb_FirstTime?"":",")+sysCargosPk);
			lb_FirstTime = false;
		}
		if (!oficinaPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"OFICINA_PK");
			ls_values.append((lb_FirstTime?"":",")+oficinaPk);
			lb_FirstTime = false;
		}
		if (!syspwdNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYSPWD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(syspwd)+"'");
			lb_FirstTime = false;
		}
		if (!apellido1Null) {
			ls_columns.append((lb_FirstTime?"":",")+"APELLIDO1");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(apellido1)+"'");
			lb_FirstTime = false;
		}
		if (!apellido2Null) {
			ls_columns.append((lb_FirstTime?"":",")+"APELLIDO2");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(apellido2)+"'");
			lb_FirstTime = false;
		}
		if (!nombreNull) {
			ls_columns.append((lb_FirstTime?"":",")+"NOMBRE");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(nombre)+"'");
			lb_FirstTime = false;
		}
		if (!activoSnNull) {
			ls_columns.append((lb_FirstTime?"":",")+"ACTIVO_SN");
			ls_values.append((lb_FirstTime?"":",")+activoSn);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SYS_USU ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (fechaBloqueoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("FECHA_BLOQUEO");
			if(fechaBloqueoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+ fechaBloqueo.toString() + "'"); 
			lb_FirstTime = false;
		}
		if (sysloginModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYSLOGIN");
			if(sysloginNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(syslogin)+"'");
			lb_FirstTime = false;
		}
		if (sysRolPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_ROL_PK");
			if(sysRolPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sysRolPk);
			lb_FirstTime = false;
		}
		if (sysCargosPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_CARGOS_PK");
			if(sysCargosPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sysCargosPk);
			lb_FirstTime = false;
		}
		if (oficinaPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("OFICINA_PK");
			if(oficinaPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+oficinaPk);
			lb_FirstTime = false;
		}
		if (syspwdModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYSPWD");
			if(syspwdNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(syspwd)+"'");
			lb_FirstTime = false;
		}
		if (apellido1Modified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("APELLIDO1");
			if(apellido1Null) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(apellido1)+"'");
			lb_FirstTime = false;
		}
		if (apellido2Modified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("APELLIDO2");
			if(apellido2Null) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(apellido2)+"'");
			lb_FirstTime = false;
		}
		if (nombreModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("NOMBRE");
			if(nombreNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(nombre)+"'");
			lb_FirstTime = false;
		}
		if (activoSnModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("ACTIVO_SN");
			if(activoSnNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+activoSn);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("SYSLOGIN");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(syslogin)+"'");
		} else {
			ls_where.append(" AND "+"SYSLOGIN");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(syslogin)+"'");
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SYS_USU SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("SYSLOGIN");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(syslogin)+"'");
		} else {
			ls_where.append(" AND "+"SYSLOGIN");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(syslogin)+"'");
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SYS_USU WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,String _syslogin
	 * @return SysUsu - Retrieved object
	 */
	static public SysUsu read(Connection _connection,String _syslogin) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SysUsu ls_SysUsu=new SysUsu();
		ls_where="SYSLOGIN  = '"+ SQLLanguageHelper.escapeQuote(  _syslogin) + "'";
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SYS_USU WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SysUsu.loadResultSet(ls_rs);
		} else {
			ls_SysUsu=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SysUsu;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		fechaBloqueo=_rs.getDate("FECHA_BLOQUEO");
		fechaBloqueoNull=_rs.wasNull();
		syslogin=_rs.getString("SYSLOGIN");
		sysloginNull=_rs.wasNull();
		sysRolPk=_rs.getLong("SYS_ROL_PK");
		sysRolPkNull=_rs.wasNull();
		sysCargosPk=_rs.getLong("SYS_CARGOS_PK");
		sysCargosPkNull=_rs.wasNull();
		oficinaPk=_rs.getLong("OFICINA_PK");
		oficinaPkNull=_rs.wasNull();
		syspwd=_rs.getString("SYSPWD");
		syspwdNull=_rs.wasNull();
		apellido1=_rs.getString("APELLIDO1");
		apellido1Null=_rs.wasNull();
		apellido2=_rs.getString("APELLIDO2");
		apellido2Null=_rs.wasNull();
		nombre=_rs.getString("NOMBRE");
		nombreNull=_rs.wasNull();
		activoSn=_rs.getLong("ACTIVO_SN");
		activoSnNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SYS_USU ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SysUsu mySysUsu;
		Vector mySysUsues = new Vector();
		while (ls_rs.next()) {
			mySysUsu = new SysUsu();
			mySysUsu.loadResultSet (ls_rs);
			mySysUsues.addElement(mySysUsu);
		}
		ls_rs.close();
		myStatement.close();
		return mySysUsues;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SYS_USU ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SYS_USU ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SYS_USU ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
	}
}
