/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: Servicios </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesServicios </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 29/01/2003
 * @author Felix Alberto Bravo Hermoso
 */

public class Servicios {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio Servicios
            */
	public static final String ID_TABLA="SES_SERVICIOS";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS SesServicios(la clase de persistencia) con los servicios que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesServicios
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesServicios (la clase de persistencia) con los servicios que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

	public Vector busquedaServ(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
		try{

	  		v = SesServicios.search(conexion,_where,_sort);
		}
		catch(Exception e){
			System.out.println("Error al Buscar Servicio:"+ e) ;
		}

		finally{
 			pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
		}
		return v;
	}


        /**
          * Realiza la insercion del Servicio.
          * @param _sesServ Objeto de clase SesServicios (clase de persistencia asociada a tabla Ses_Servicios)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Servicios
          * @throws Exception
         */

	public void insertarServ(SesServicios _sesServ) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
		Pool pool=null;
		Connection conexion = null;
		Vector v = new Vector();
 		pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
		conexion.setAutoCommit(false);
		try{
                   _sesServ.insert(conexion);
                   try {
                     if (negevent != null) {
                      negevent.addVar(88,_sesServ.getSessrvCod());
                      negevent.addVar(89,_sesServ.getSessrvDesc());
                      negevent.addVar(90,_sesServ.getSesespCod());
                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.warning(" No se ha insertado el evento de inserci�n del Servicio con C�digo = " + _sesServ.getSessrvCod() + " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(SQLException e){
//			System.out.println("Error al Insertar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion del servicio con Codigo = " + _sesServ.getSessrvCod() + " Error -> " + e.toString());
			conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
		   }finally{
			pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
		   }
		}

        /**
          * Realiza la modificacion del Servicio.
          * @param _sesServ Objeto de clase SesServicios (clase de persistencia asociada a tabla Ses_Servicios)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Servicios
         */
	public void modificarServ(SesServicios _sesServ) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
		Pool pool=null;
		Connection conexion = null;
                pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
		try{
	  	 _sesServ.update(conexion);
                  try {
                    if (negevent != null) {
                      negevent.addVar(91,_sesServ.getSessrvCod());
                      negevent.addVar(92,_sesServ.getSessrvDesc());
                      negevent.addVar(93,_sesServ.getSesespCod());
                      negevent.guardar(conexion);
                    }
                  }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion del Servicio con C�digo = " + _sesServ.getSessrvCod() + " Error -> " + e.toString());
                  }
                 conexion.commit();
   		 }catch(Exception e){
			conexion.rollback();
//                        System.out.println("Error al Modificar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion del servicio con Codigo = " + _sesServ.getSessrvCod() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
			throw es;
                 }

		 finally{
			pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
		 }

	}

        /**
         * Realiza la eliminacion del servicio.
         * @param _sesServ Objeto de clase SesServicios (clase de persistencia asociada a tabla Ses_Servicios)
         * @throws Exception
         */

        public void eliminarServ(SesServicios _sesServ) throws Exception {
          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesServ.delete(conexion);
            try{
              if (negevent != null) {
                      negevent.addVar(94,_sesServ.getSessrvCod());
                      negevent.guardar(conexion);
              }
            }catch(Exception e){
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha insertado el evento de eliminacion del Servicio con C�digo = " + _sesServ.getSessrvCod() + " Error -> " + e.toString());
            }
            conexion.commit();
          }catch(SQLException e){
            conexion.rollback();
            //System.out.println(" Error al borrar Servicio: "+e);
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
           }
           finally{
             pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
           }
	}

} //fin de la clase

