/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: EventosGarantia </p>
 * <p>Descripcion: Clase que tiene la informacion referida a los eventos relacionados con tramites de garantia</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 16/12/2002
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */

public class EventosGarantia {

    /**
     *  evtoTipoEvto - Pk del codigo de evento, de la tabla Evento_Tipo (14,15,16,17 � 18)
     */

     public long 	evtoTipoEvto=0;
    /**
     * evtoGarPk - Pk de la garantia a la que viene referido el evento
     */
    public long 	evtoGarPk=0;
    /**
     * evtoTramPk - Pk del tramo de  garantia a la que viene referido el evento
     */
    public long  	evtoGarTramPk=0;
    /**
     * evtoGarOperacion - Operacion que se ha realizado sobre el tramite de garancia
     */
    public long  	evtoGarOperacion=0;

    /**
     *  evtoMonto - Monto de la aprobacion de solicitud de garantia (si la hubiese)
     */
    public double        evtoMonto =0;
    /**
     *  evtoMotivo - Motivo de la denegacion de solicitud de garantia (si la hubiese)
     */
    public String 	evtoMotivo="SD";

    /**
      * evtoFecha - Fecha en la que se ha realizado la garantia
      */
    public String 	evtoFecha ="SD";

    /**
     *  evtonss - N�mero de la Seguridad Social del responsable del paciente de LESP � LESPCEX que ha realizado la solicitud de garantia
     */

    public String 	evtonss ="SD";
    /**
     *  evtodni - Numero de NIF del responsable de la operacion que se este realizando
     */

    public String       evtodni = "SD";
    /**
     *  evtoapenom - Apellidos y Nombre del responsable de la operacion que se este realizando
     */

    public String       evtoapenom = "SD";
    /**
     *  evtoGarCQ - "C" para garancias relativas a una entrada en LESPCEX � "Q" para garancias relativas a una entrada en LESP
     */

    public String       evtoGarCQ = "SD";
    /**
     *  evtoOficinaPk - Pk de la oficina en la que se ha realizado el tramite de garantia
     */
    public long         evtoOficinaPk = 0;

}