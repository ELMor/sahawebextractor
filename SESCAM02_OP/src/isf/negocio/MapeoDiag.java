/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: MapeoDiag </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesDiagMap </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 03/02/2003
 * @author Felix Alberto Bravo Hermoso
 */

public class MapeoDiag {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio MapeoDiag
            */
        public static final String ID_TABLA="Ses_Diag_Map";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS SesDiagMap(la clase de persistencia) con los Mapeos de Diagnosticos que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesDiagMap
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesDiagMap (la clase de persistencia) con los Mapeos de Diagnosticos que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

        public Vector busquedaDiagMap(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SesDiagMap.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar el Mapeo de Diagnostico:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion del Mapeo de Diagnostico.
          * @param _sesDiagMap Objeto de clase SesDiagMap (clase de persistencia asociada a tabla Ses_DiagMap)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Diag_Map
          * @throws Exception
         */

        public void insertarDiagMap(SesDiagMap _sesDiagMap) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                v = busquedaDiagMap("DIAG_CEN_COD='"+_sesDiagMap.getDiagCenCod()+"' AND CENTRO="+_sesDiagMap.getCentro(),"");
                if (v.size()>0){
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_MAPEO_EXISTENTE);
                  throw es;
                }else{
                   try{
                    _sesDiagMap.insert(conexion);
                    try {
                      if (negevent != null) {
                        String diagpk = new Long(_sesDiagMap.getDiagPk()).toString();
                        negevent.addVar(109,diagpk);
                        negevent.addVar(110,_sesDiagMap.getDiagCenCod());
                        String centro = new Long(_sesDiagMap.getCentro()).toString();
                        negevent.addVar(111,centro);
                        negevent.guardar(conexion);
                      }
                    }catch (Exception e) {
                      fichero_log = Log.getInstance();
                      fichero_log.warning(" No se ha insertado el evento de inserci�n del Mapeo de Diagnostico con C�digo = " + _sesDiagMap.getDiagPk() + " Error -> " + e.toString());
                    }
                    conexion.commit();
                  }catch(SQLException e){
                    fichero_log = Log.getInstance();
                    fichero_log.error(" No se ha realizado la insercion del Diagnostico con pk = " + _sesDiagMap.getDiagPk() + " Error -> " + e.toString());
                    conexion.rollback();
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                    throw es;
                  }finally{
                    pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                  }
                }
        }

        /**
          * Realiza la modificacion del Mapeo del Diagnostico.
          * @param _sesDiagMap Objeto de clase SesDiagMap (clase de persistencia asociada a tabla Ses_Diag_Map)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Diag_Map
         */
        public void modificarDiagMap(SesDiagMap _sesDiagMap) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesDiagMap.update(conexion);
                  try {
                    if (negevent != null) {
                      String diagpk = new Long(_sesDiagMap.getDiagPk()).toString();
                      negevent.addVar(112,diagpk);
                      negevent.addVar(113,_sesDiagMap.getDiagCenCod());
                      String centro = new Long(_sesDiagMap.getCentro()).toString();
                      negevent.addVar(114,centro);
                      negevent.guardar(conexion);
                    }
                  }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion del Diagnostico con C�digo = " + _sesDiagMap.getDiagPk() + " Error -> " + e.toString());
                  }
                 conexion.commit();
                    }catch(Exception e){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion del Diagnostico con Codigo = " + _sesDiagMap.getDiagPk() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                 }

                 finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                 }

        }

        /**
         * Realiza la eliminacion del Mapeo del Diagnostico.
         * @param _sesDiagMap Objeto de clase SesDiagMap (clase de persistencia asociada a tabla Ses_Diag_Map)
         * @throws Exception
         */

        public void eliminarDiagMap(SesDiagMap _sesDiagMap) throws Exception {
          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesDiagMap.delete(conexion);
            try{
              if (negevent != null) {

                      negevent.addVar(115,_sesDiagMap.getDiagCenCod());
                      String centro = new Long(_sesDiagMap.getCentro()).toString();
                      negevent.addVar(116,centro);
                      negevent.guardar(conexion);
              }
            }catch(Exception e){
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha insertado el evento de eliminacion del Diagnostico con C�digo = " + _sesDiagMap.getDiagPk() + " Error -> " + e.toString());
            }
            conexion.commit();
          }catch(SQLException e){
            conexion.rollback();
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
           }
           finally{
             pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
           }
        }

} //fin de la clase

