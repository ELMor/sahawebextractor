/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: PacientesPorCatalogacion </p>
 * <p>Descripcion: Clase que guarda informaci�n resultante de la busqueda de paciente por tramo de antiguedad y catalogacion</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 30/12/2002
 * @author Jose Vicente Nieto
 */

public class PacientesPorCatalogacion {
     /**
       * Nombre del Centro al cual pertence el registro en LESP � LESPCEX (tabla CentrSesCam)
       */
     public String 	Centro="SD";
     /**
      *  pk Centro, valor del campo CENTRO si la solicitud la ha realizado un paciente de LESP o valor del campo CEGA si viene de un paciente en LEPCEX
      */
     public long        CodCentro='0';
     /**
       * Cip del paciente
       */
     public String 	Cip="SD";

     /**
       * Primer Apellido del paciente que realiza la solicitud
       */
     public String 	Ape1="SD";
     /**
      * Segundo Apellido del paciente que realiza la solicitud
      */
     public String 	Ape2="SD";
     /**
       * Nombre del paciente que realiza la solicitud
       */
     public String 	Nombre="SD";

     /**
       * Numero de Orden (campo NOrden) si la solicitud se realiza sobre paciente de LESP o Numero de Cita (Campo NCita) si la solicitud se realiza sobre paciente de LESPCEX
       */
     public long        Norden=0;

     /**
       * Distingue si se trata de una solicitud para un paciente en LESP (Q) o si la es solicitud para un paciente en LESPCEX (C)
       */

     public String      Cq="SD";

     public int antiguedad = 0;
     public String      Situacion="SD";
}