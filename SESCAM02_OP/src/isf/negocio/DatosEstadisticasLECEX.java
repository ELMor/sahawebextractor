/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: DatosEstadisticasLEQ </p>
 * <p>Descripcion: Clase generica donde se guardan los registros obtenidos por la consulta</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 29/01/2003
 * @author Jose Vicente Nieto
 */

public class DatosEstadisticasLECEX {
     /**
       * Nombre del Dato por el que se a agrupar el listado puede ser nombre servicio, nombre de centro, especialidad ..
       */
     public String 	DatoGroupby="SD";
     /**
      *  Valor obtenido en la estadistica para cada uno de los tramos
      */

     public int tramos[] = null;
     public int tramo1=0;
     public int tramo2=0;
     public int tramo3=0;
     public int tramo4=0;
     public int tramo5=0;

     /**
      *  Valor del total de pacientes resultantes de la estadistica
      */
     public long        pacientes=0;
     /**
      *  Valor del total de pacientes pendientes resultantes de la estadistica
      */
     public long        pendientes=0;
     /**
      *  Valor de la demora media para ese servicio segun el numero de pacientes
      */
     public double       demoramedia=0;
     /**
     *  Valor de la demora media para pendientes
     */
     public double       demmediapendientes=0;

}