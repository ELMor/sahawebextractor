package isf.negocio;

public class EntradaLEPasivo{
   public int 		fNOrden=0;
   public int 		fCentro=0;
   public int 		fOrden=0;
   public String	fFDemora="SD";
   public int 		fTDemora=0;
   public String 	fEntrada="SD";
   public String 	fSalida="SD";
   public String	fMotivo;
   public String 	fProc="SD";
   public String    fCip ="SD";
   public int       fCega =0;
   public int       fHicl =0;
   public String    fCias ="SD";
   public String    fApe1 ="SD";
   public String    fApe2 ="SD";
   public String    fNomb ="SD";
   public String    fFnac ="SD";
   public String    fSexo ="SD";
   public String    fDomi ="SD";
   public String    fPobl ="SD";
   public int       fCpos =0;
   public String    fProv ="SD";
   public String    fTel1 ="SD";
   public String    fTel2 ="SD";
   public String    fTel3 ="SD";
   public String    fSlab ="SD";
   public String    fFina ="SD";
   public int       fGara =0;
   public String    fServ ="SD";
   public String    fSecc ="SD";
   public String    fAcli ="SD";
   public int       fCmed =0;
   public String    fApno_med ="SD";
   public String    fCdi1  ="SD";
   public String    fDdi1  ="SD";
   public String    fCdi2  ="SD";
   public String    fDdi2  ="SD";
   public String    fCpr1  ="SD";
   public String    fCpr2  ="SD";
   public String    fDpr2  ="SD";
   public String    fTcir  ="SD";
   public String    fTane  ="SD";
   public String    fPrio  ="SD";
   public String    fCinc  ="SD";
   public String    fPreo  ="SD";
   public String    fFrep  ="SD";
   public String    fFcap  ="SD";
   public String    fSpac  ="SD";
   public String    fFder  ="SD";
   public String    fDder  ="SD";
   public String    fFqui  ="SD";
   public String    fObse  ="SD";
   public String    fDescentro  ="SD";
   public String    fDescentroLarga  ="SD";
   public String    fFCita  ="SD";
   public String getSfTDemora(){
     if (fTDemora==0)return"";
     else {
       try {return (String.valueOf(fTDemora));}
       catch(Exception e){return "";}
     }
   }

/*
   public String getSfNOrden(){
     if (fNOrden==null)return"";
     else {
       try{return (String.valueOf(fNOrden));
       }catch(Exception e){return "";}
     }
   }
   public String getSfCentro(){
     if (fCentro==null)return"";
     else {
       try {return (String.valueOf(fCentro));}
       catch(Exception e){return "";}
     }
   }
   public String getSfOrden(){
     if (fOrden==null)return"";
     else {
       try {return (String.valueOf(fOrden));}
       catch(Exception e){return "";}
     }
   }
   public String getSfMotivo(){
     if (fMotivo==null)return"";
     else {
       try {return (String.valueOf(fMotivo));}
       catch(Exception e){return "";}
     }
   }
   public String getSfCega(){
     if (fCega==null)return"";
     else {
       try {return (String.valueOf(fCega));}
       catch(Exception e){return "";}
     }
   }
   public String getSfHicl(){
     if (fHicl==null)return"";
     else {
       try {return (String.valueOf(fHicl));}
       catch(Exception e){return "";}
     }
   }
   public String getSfCpos(){
     if (fCpos==null)return"";
     else {
       try {return (String.valueOf(fCpos));}
       catch(Exception e){return "";}
     }
   }
   public String getSfGara(){
     if (fGara==null)return"";
     else {
       try {return (String.valueOf(fGara));}
       catch(Exception e){return "";}
     }
   }
   public String getSfCmed(){
     if (fCmed==null)return"";
     else {
       try {return (String.valueOf(fCmed));}
       catch(Exception e){return "";}
     }
   }
*/
   public String getfFDemora(){if (fFDemora==null) return"";else return fFDemora;}
   public String getfEntrada(){if (fEntrada==null) return"";else return fEntrada;}
   public String getfSalida(){if (fSalida==null) return"";else return fSalida;}
   public String getfProc(){if (fProc==null) return"";else return fProc;}
   public String getfCip (){if (fCip==null) return"";else return fCip;}
   public String getfCias (){if (fCias==null) return"";else return fCias;}
   public String getfApe1 (){if (fApe1==null) return"";else return fApe1;}
   public String getfApe2 (){if (fApe2==null) return"";else return fApe2;}
   public String getfNomb (){if (fNomb==null) return"";else return fNomb;}
   public String getfFnac (){if (fFnac==null) return"";else return fFnac;}
   public String getfSexo (){if (fSexo==null) return"";else return fSexo;}
   public String getfDomi (){if (fDomi==null) return"";else return fDomi;}
   public String getfPobl (){if (fPobl==null) return"";else return fPobl;}
   public String getfProv (){if (fProv==null) return"";else return fProv;}
   public String getfTel1 (){if (fTel1==null) return"";else return fTel1;}
   public String getfTel2 (){if (fTel2==null) return"";else return fTel2;}
   public String getfTel3 (){if (fTel3==null) return"";else return fTel3;}
   public String getfSlab (){if (fSlab==null) return"";else return fSlab;}
   public String getfFina (){if (fFina==null) return"";else return fFina;}
   public String getfServ (){if (fServ==null) return"";else return fServ;}
   public String getfSecc (){if (fSecc==null) return"";else return fSecc;}
   public String getfAcli (){if (fAcli==null) return"";else return fAcli;}
   public String getfApno_med (){if (fApno_med ==null) return"";else return fApno_med ;}
   public String getfCdi1  (){if (fCdi1==null) return"";else return fCdi1;}
   public String getfDdi1  (){if (fDdi1==null) return"";else return fDdi1;}
   public String getfCdi2  (){if (fCdi2==null) return"";else return fCdi2;}
   public String getfDdi2  (){if (fDdi2==null) return"";else return fDdi2;}
   public String getfCpr1  (){if (fCpr1==null) return"";else return fCpr1;}
   public String getfCpr2  (){if (fCpr2==null) return"";else return fCpr2;}
   public String getfDpr2  (){if (fDpr2==null) return"";else return fDpr2;}
   public String getfTcir  (){if (fTcir==null) return"";else return fTcir;}
   public String getfTane  (){if (fTane==null) return"";else return fTane;}
   public String getfPrio  (){if (fPrio==null) return"";else return fPrio;}
   public String getfCinc  (){if (fCinc==null) return"";else return fCinc;}
   public String getfPreo  (){if (fPreo==null) return"";else return fPreo;}
   public String getfFrep  (){if (fFrep==null) return"";else return fFrep;}
   public String getfFcap  (){if (fFcap==null) return"";else return fFcap;}
   public String getfSpac  (){if (fSpac==null) return"";else return fSpac;}
   public String getfFder  (){if (fFder==null) return"";else return fFder;}
   public String getfDder  (){if (fDder==null) return"";else return fDder;}
   public String getfFqui  (){if (fFqui==null) return"";else return fFqui;}
   public String getfObse  (){if (fObse==null) return"";else return fObse;}
   public String getfMotivo(){if (fMotivo==null) return"";else return fMotivo;}
   public String getfDescentro (){if (fDescentro==null) return"";else return fDescentro;}
   public String getfDescentroLarga (){if (fDescentroLarga==null) return"";else return fDescentroLarga;}
   public String getfFCita (){if (fFCita==null) return"";else return fFCita;}
   }
