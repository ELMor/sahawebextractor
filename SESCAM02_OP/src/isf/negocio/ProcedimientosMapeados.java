/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;



/**
 * <p>Clase: ProcedimientosMapeados </p>
 * <p>Descripcion: Clase que maneja el bean de consulta de procedimientos mapeados</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 19/02/2003
 * @author Jos� Vicente Nieto-M�rquez Fdez-Medina
 */

public class ProcedimientosMapeados {
     /**
        * Codigo del Procedimiento  obtenido en la consulta
        */
       public String 	CodProcSESCAM="SD";
       /**
         * Descripci�n del procedimiento obtenido en la consulta
         */
       public String 	ProcDesc="SD";

       /**
        *  Codigo Mapeado
        */
       public String        CodProcMapeado="SD";

       /**
        * Descripci�n del procedimiento obtenido en la consulta
        */
       public String 	ProcDescMapeado="SD";


       /**
        *  Codigo de Tipo Proced
        */
       public String        CodTipoSescam="SD";
       /**
        *  Codigo de Tipo Proced
        */
       public int       Tipo=0;
       /**
        *  Codigo de Tipo Procedimiento mapeado
        */
       public String        CodTipoMapeado="SD";
       /**
        *  Centro
        */
       public long      centro=0;
}
