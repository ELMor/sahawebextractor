/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import isf.negocio.NegEventoVar;
import isf.persistencia.Evento;
import isf.persistencia.EventoVar;
import isf.db.*;
import java.sql.*;
import conf.Constantes;


/**
 * <p>Clase: Solicitud </p>
 * <p>Descripcion: Clase que guarda informaci�n de una solicitud de garantia tanto de pacientes en LESP como en LESPCEX</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 09/12/2002
 * @author Iv�n Enrique Caneri
 */

public class NegEvento {
  /**
   vVar : Vector de variables pertenecientes al evento.
   */
  protected Vector vVar;
  /**
   idEv : Identificador del Evento.
   */
  protected int idEv;
  /**
   lSessPK : Identificador del Session
   */
  protected long lSessPK;

  /**
   hte : Puntero a la Hashtable con los eventos activados.
   */
  protected Hashtable hte;

  public NegEvento(int pidEv,Hashtable phte, long plSessPK) {
    vVar= new Vector();
    idEv=pidEv;
    lSessPK=plSessPK;
    hte=phte;
  }

  public boolean addVar(int pidVar,String strValue){
    vVar.add(new NegEventoVar(pidVar,strValue));
    return true;
  }
  public boolean removeVar(int pidVar){
    return true;
  }
  public boolean guardar(Connection conn) throws SQLException {
    if( hte.get(this.idEv+"/")!=null){
    Evento ev=new Evento();
    ev.setTpoevtoPk(this.idEv);
    ev.setSessPk(this.lSessPK);
    ev.newId(Constantes.FICHERO_CONFIGURACION);
    ev.insert(conn);
    // Asigno el Pk de evento a la variable local
    long lEvtoPk;
    lEvtoPk=ev.getEvtoPk();
    // Almaceno los valores de las variables
    Enumeration enumv= vVar.elements();
    EventoVar evv=new EventoVar();
    NegEventoVar nevn;
    while(enumv.hasMoreElements()){
      nevn=(NegEventoVar)enumv.nextElement();
      if( hte.get(this.idEv+"/"+nevn.id)==null) continue;
      evv.setEvtoPk(lEvtoPk);
      evv.setTpoevtoVarPk(nevn.id);
      evv.setEvtoVarValor(nevn.valor);
      evv.newId(Constantes.FICHERO_CONFIGURACION);
      evv.insert(conn);
      }
    }
    return true;
  }
  public boolean guardar() throws SQLException {
    Connection conn = null;
    Pool ref_pool=null;
    ref_pool = Pool.getInstance();
    conn = ref_pool.getConnection(conf.Constantes.FICHERO_CONFIGURACION);
    try {
      this.guardar(conn);
      conn.commit();
    }
    catch (SQLException ex) {
      conn.rollback();
      throw ex;
    }
    finally{
        ref_pool.freeConnection(conf.Constantes.FICHERO_CONFIGURACION, conn);
      }
    return true;
  }

}
