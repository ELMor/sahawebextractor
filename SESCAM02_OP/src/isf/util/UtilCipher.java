package isf.util;

import java.io.InputStream;
import java.util.Properties;
import java.io.IOException;


/**
 * Esta clase se utiliza para realizar la encriptaci�n de datos y la
 * comprobaci�n de si determinados datos coinciden con su "supuesta" imagen
 * encriptada.
 *
 * @version			1.0 06 Mar 2001
 * @author			Jose Luis Diaz (JLD)
 */
public class UtilCipher implements Cifrado {

/** Clave utilizada para el cifrado. */
	private String clave;


/**
 * Este m�todo inicializa la clave.
 *
 * @param _clave Valor de inicializaci�n de la clave.
 */
	public void setClave (String _clave) {
		this.clave = _clave;
	}

/**
 * Este m�todo comprueba si un determinado texto sin encriptar es el mismo
 * que otro texto encriptado.<br>
 * Es necesario inicializar el objeto a trav�s del m�todo <b>init(...)</b>.
 *
 * @param textoPlano Texto sin encripta de la comparaci�n.
 * @param textoEncriptado Texto encriptado utilizado en la comparaci�n.
 * @return Si el texto encriptado es el mismo que el texto plano, se devuelve
 *					<i>true</i>, en caso contrario, se devuelve <i>false</i>.
 * @exception El m�todo devuelve una excepci�n en caso de que la clave que
 *					se utilizar� para la comparaci�n no est� correctamente
 *					iniciada<br>.
 *					Esto se puede resolver a trav�s del m�todo <b>init(...)</b> que
 *					inicializa la clase.
 */

	public boolean esCorrecto (String textoPlano, String textoEncriptado){

		boolean resultado = false;			// Resultado de la ejecuci�n del m�todo.

		String textoComparacion = null;	// String auxiliar utilizado para
										// encriptar el texto plano recibido.
		textoComparacion=this.desencriptar(textoEncriptado);

		if (textoComparacion.equals (textoPlano)) {
			resultado = true;
		}

		return resultado;
	}

/**
 * Este m�todo encripta un determinado texto pasado como par�metro<br>.
 * Es necesario inicializar el objeto a trav�s del m�todo <b>init(...)</b>.
 *
 * @param texto Texto que se desea encriptar.
 * @return El texto pasado como par�metro pero encriptado.
 * @exception El m�todo devuelve una excepci�n en caso de que la clave que
 *					se utilizar� para la encriptaci�n no est� correctamente
 *					iniciada<br>.
 *					Esto se puede resolver a trav�s del m�todo <b>init(...)</b> que
 *					inicializa la clase.
 * @see #init init (String)
 */


	public String encriptar (String valor) {

		if ((valor==null) || (valor.equals(""))){
			return("");
		}
		String enc="";
		double rnd = java.lang.Math.random();

		// 191 caracteres validos que existen en el codigo ASCII
		// Del 32 al 223
		long padd=java.lang.Math.round(rnd*191)+32;

		char c = (char) padd;
		enc=enc+c;

		for (int bucle=0;bucle<valor.length();bucle++){
			char letra = valor.charAt(bucle);
			long ascii= (long) letra;
			long asciiCod= ascii+padd;
			asciiCod = (asciiCod % 191)+32;
			padd=asciiCod;
			c = (char) asciiCod;
			enc=enc+c;
		}
		return (enc);
	}


	public String encriptar (String texto,int tamMax){

		return ( encriptar(texto));
	}


/**
 * Este m�todo desencripta un determinado texto pasado como par�metro<br>.
 * Es necesario inicializar el objeto a trav�s del m�todo <b>init(...)</b>.
 *
 * @param texto Texto que se desea desencriptar.
 * @return El texto pasado como par�metro pero desencriptado.
 * @exception El m�todo devuelve una excepci�n en caso de que la clave que
 *					se utilizar� para la desencriptaci�n no est� correctamente
 *					iniciada<br>.
 *					Esto se puede resolver a trav�s del m�todo <b>init(...)</b> que
 *					inicializa la clase.
 * @see #init init (String)
 */

	public String desencriptar (String valor) {

		if ((valor==null) || (valor.equals(""))){
			return("");
		}

		String des="";

		char c=valor.charAt(0);
		long padd = (long) c;


		for (int bucle=1;bucle<valor.length();bucle++){
			char letra = valor.charAt(bucle);
			long asciiCod= (long) letra;
			long ascii= asciiCod-padd;
			ascii= ascii-32;
			if (ascii<32){
				ascii = ascii+191;
			}

			padd=asciiCod;
			c = (char) ascii;
			des=des+c;
		}
		return (des);


	}


/**
 * Este m�todo inicializa la clase. Con �l se recupera la clave que se va a
 * utilizar en el resto de m�todos.<br>
 * Para realizar esta inicializaci�n, recupera los datos de un fichero de
 * propiedades cuyo nombre se pasa como par�metro.<br>
 * Este fichero de propiedades debe existir y debe estar accesible para el
 * entorno java. Para ello, se coloca bajo un directorio apuntado directamente
 * por la variable de entorno <i>CLASSPATH</i>.
 *
 * @param ficheroPropiedades Nombre del fichero de propiedades que contiene
 *					el par�metro que queremos recuperar.
 * @exception El m�todo devuelve una excepci�n en caso de que el fichero de
 *					propiedades cuyo nombre se ha pasado como par�metro no exista o
 *					no pueda ser accedido por el sistema java. Tambi�n se devolver�
 *					una excepci�n en caso de que la, a pesar de poder recuperar el
 *					fichero de propiedades, �ste no contenga la propiedad que se
 *					recuperar.
 */
	public void init2 (String ficheroPropiedades){
		String clave = null;			// Clave que se recuperar�.
		InputStream is = null;		// Enlace con el fichero de propiedades.
		Properties props = null;	// Parseado de las propiedades.

		// Comprobamos que se ha enviado un nombre para buscar el fichero de
		// propiedades.
		if (ficheroPropiedades != null) {
			// Recuperamos el fichero.
	        is = ficheroPropiedades.getClass().getResourceAsStream(ficheroPropiedades);

			props = new Properties();

			try {
				// Cargamos el fichero de propiedades en la clase que las gestiona.
				props.load(is);
				// Buscamos la propiedad.
				clave = props.getProperty ("CIPHERSTRING");
				// Comprobamos que existe.
				if (clave == null) {
					System.out.print ("No existe la propiedad adecuada en el " +
						"fichero de properties (" + ficheroPropiedades + ")");
				}
			} catch (IOException e) {
				// Si no existe el fichero.
				System.out.print ("No se puede leer el fichero de propiedades (" +
					ficheroPropiedades + ")");
			} catch (NullPointerException e) {
				// Si no existe el fichero.
				System.out.print ("No se puede leer el fichero de propiedades (" +
					ficheroPropiedades + ")");
			} finally {
				// Cerramos el fichero.
				try {
					is.close();
				} catch (Exception ex) {
				}
			}
		} else {
			System.out.print ("No se ha indicado un fichero de propiedades.");
		}

		this.setClave (clave);
	}


	public static void main(String args[])
	{
		UtilCipher myUtilCipher = new UtilCipher();
		String Str_valor = "La casa azul";
		String Str_valorEnc;
		String Str_valorDesc;

		Str_valorEnc = myUtilCipher.encriptar(Str_valor);
		System.out.println("���> Str_valorEnc: " + Str_valorEnc);
//		Str_valorEnc = "�_�E?";
		Str_valorDesc = myUtilCipher.desencriptar(Str_valorEnc);
		System.out.println("���> Str_valorDesc: " + Str_valorDesc);

	}


 }
