package isf.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class SimpleExpandText implements ExpandText {
  public String expand(String texto) {
    char chars[]=new char[texto.length()];
    char encChars[]=new char[2*chars.length];
    texto.getChars(0,texto.length(),chars,0);
    for(int i=0;i<chars.length;i++){
      char l1=(char)(chars[i]&0xf0);
      char l2=(char)(l1>>4);
      char l3=(char)(l2+64);
      char r1=(char)(chars[i]&0x0f);
      char r2=(char)(r1>>0);
      char r3=(char)(r2+32);
      encChars[2*i  ]=l3;
      encChars[2*i+1]=r3;
    }
    return new String(encChars);
  }
  public String compress(String texto) {
    char chars[]=new char[texto.length()];
    char decChars[]=new char[chars.length/2];
    texto.getChars(0,texto.length(),chars,0);
    for(int i=0;i<chars.length;i+=2){
      char l1=(char)(chars[i+0]);
      char l2=(char)(l1-64);
      char l3=(char)(l2<<4);
      char r1=(char)(chars[i+1]);
      char r2=(char)(r1-32);
      char r3=(char)(r2<<0);
      decChars[i/2]=(char)(l3+r3);
    }
    return new String(decChars);
  }

}