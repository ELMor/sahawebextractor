/**
	Clase SQLLanguageHelper:
	
	Helper con metodos estaticos para trabajar con valores y sentencias SQL
	
	@author SGA
	@version 2000.0
*/
package isf.persistence;

public class SQLLanguageHelper
{
    
	/* **************************************************************
	*                                                               *
	* CONSTANTES                                                    *
	*                                                               *
	************************************************************** */

    /** Car�cter de delimitacion de literales */
    public static final char CHAR_QUOTE='\'';

    

	/* **************************************************************
	*                                                               *
	* METODOS                                                       *
	*                                                               *
	************************************************************** */
    
    /**
    	Cuenta el n� de ocurrencias de un caracter dentro de un String
    	@param strIn	    String de entrada
    	@param chaSearch	Caracter de busqueda
    	@return N� de ocurrencias
    */
    public static int countNumberOfOcurrences(String strIn, char chaSearch)
    {
        int p=-1;
        int n=0;
        do
        {
            p=strIn.indexOf(chaSearch,p);
            if (p>=0)
            {
                n++;
                p++;
            }
        }
        while (p>=0);
        return n;
    }
    
    /**
    	Escapea los caracteres de delimitacion de literales, mediante 
    	duplicar cada ocurrencia del caracter de quote en el string de entrada
    	@param strIn	String de entrada
    	@return String resultante
    */
    public static String escapeQuote(String strIn)
    {
        int nNumQuotes=countNumberOfOcurrences(strIn,CHAR_QUOTE);
        StringBuffer stbOut=new StringBuffer(strIn.length()+nNumQuotes);
        stbOut.append(strIn);

        int p=-1;
        int n=0;
        do
        {
            p=strIn.indexOf(CHAR_QUOTE,p);
            if (p>=0)
            {
                stbOut.insert(p+n,CHAR_QUOTE);
                n++;
                p++;
            }
        }
        while (p>=0);
        return stbOut.toString();
    }

/*
    
    /**
    	Solo para test: escapea los quotes con Strings en lugar de StringBuffers
    	@param strIn	
    	@return 
    *
    public static String escapeQuote2(String strIn)
    {
        int p,p2=0;
        String strSalida="";
        char[] x={CHAR_QUOTE,CHAR_QUOTE};
        String strNuevo=new String(x);
        do
        {
            p=strIn.indexOf(CHAR_QUOTE,p2);
            if (p>=0)
            {
                strSalida+=strIn.substring(p2,p);
                strSalida+=strNuevo;
                p2=p+1;
            }
        }
        while (p>=0);
        if (p2<strIn.length())
        {
            strSalida+=strIn.substring(p2);
        }
        return strSalida;
    }
    
    */
    
/*	public static void main(String[] args)
	throws Exception
	{
		if (args.length<1)
		{
			System.out.println("Invocacion: SQLLanguageHelper texto");
		}
		else
		{
		    String strIn=args[0];
		    String strOut="";
		    long t1,t2;
		    t1=System.currentTimeMillis();
		    for (int i=0;i<10000;i++) 
		    {
			    strOut=escapeQuote(strIn);
		    }
		    t2=System.currentTimeMillis();
System.out.println("tiempo="+(t2-t1));
		    
		    System.out.println("strIn="+strIn);
            System.out.println("strOut=["+strOut+"]");
        }
        System.in.read();
	}*/

    
}
