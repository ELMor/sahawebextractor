// Mover un item de un select a otro
function selecciona(controlOrigen, controlDestino) 
{
	var dato;
	var codigo;

	for (var i=0; i < controlOrigen.length; i++)
	{
		//Obtener la posicion del elemento seleccionado
		option = controlOrigen.options[i];
	
		//Si hay alguna posicion seleccionada
		if (option.selected)
		{
			var newOption = new Option();
			newOption.value = option.value;
			newOption.text = option.text;
			
			
			// Las inserta en el select multiple
			if (noEsta(newOption, controlDestino))
				controlDestino.options[controlDestino.options.length] = newOption;
		}
	}
}	

// Comprobar si una opci�n esta ya en un lista 
function noEsta(opcion, control)
{
	ret = true;
	for (var i=0; i < control.length; i++)
	{
		if (opcion.value == control.options[i].value)
		{
			ret = false;
			break;
		}
	}
	return ret;
}

//Eliminar un item de una lista
function deselecciona(control)
{
	for (var i=0; i < control.length; i++)
	{
		//Obtener la posicion del elemento seleccionado
		option = control.options[i];
	
		//Si hay alguna posicion seleccionada
		if (option.selected)
		{
			//Eliminar el select
			control.options[i] = null;
		}
	}
}

// Mover todos los items de un select a otro
function seleccionaTodos(controlOrigen, controlDestino) 
{
	for (var i=0; i < controlOrigen.length; i++)
	{
		//Obtener la descripcion y codigo
		descripcion = controlOrigen.options[i].text;
		codigo = controlOrigen.options[i].value;
		var newOption = new Option();
		newOption.value = codigo;
		newOption.text = descripcion;
				
		// Las inserta en el select multiple
		if (noEsta(newOption, controlDestino))
			controlDestino.options[controlDestino.options.length] = newOption;
	}
}

//Eliminar todos los items de una lista
function deseleccionaTodos(control)
{
	control.length=0;
}