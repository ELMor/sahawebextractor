// CONSTRUCTOR for the CalendarPopup Object
function CalendarPopup() {
        var newCalendar;
        if (arguments.length>0) {
                newCalendar = new PopupWindow(arguments[0]);
                }
        else {
                newCalendar = new PopupWindow();
                newCalendar.setSize(150,175);
                }
        newCalendar.offsetX = -110; //-110
        newCalendar.offsetY = 25;   //25
        newCalendar.autoHide();
        // Calendar-specific properties
        newCalendar.monthNames = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
        newCalendar.dayHeaders = new Array("S","M","T","W","T","F","S");
        newCalendar.returnFunction = "tmpReturnFunction";
        newCalendar.weekStartDay = 0;
        newCalendar.isShowYearNavigation = false;
        // Method mappings
        newCalendar.setReturnFunction = CalendarPopup_setReturnFunction;
        newCalendar.setMonthNames = CalendarPopup_setMonthNames;
        newCalendar.setDayHeaders = CalendarPopup_setDayHeaders;
        newCalendar.setWeekStartDay = CalendarPopup_setWeekStartDay;
        newCalendar.showYearNavigation = CalendarPopup_showYearNavigation;
        newCalendar.showCalendar = CalendarPopup_showCalendar;
        newCalendar.hideCalendar = CalendarPopup_hideCalendar;
        newCalendar.getStyles = CalendarPopup_getStyles;
        newCalendar.refreshCalendar = CalendarPopup_refreshCalendar;
        newCalendar.getCalendar = CalendarPopup_getCalendar;
        // Return the object
        return newCalendar;
        }

// Temporary default function to be called when a date is clicked, so no error is thrown
function CalendarPopup_tmpReturnFunction(y,m,d) {
        alert('Use setReturnFunction() to define which function will get the clicked results!');
        }

// Set the name of the function to call to get the clicked date
function CalendarPopup_setReturnFunction(name) {
        this.returnFunction = name;
        }

// Over-ride the built-in month names
function CalendarPopup_setMonthNames() {
        for (var i=0; i<arguments.length; i++) {
                this.monthNames[i] = arguments[i];
                }
        }

// Over-ride the built-in column headers for each day
function CalendarPopup_setDayHeaders() {
        for (var i=0; i<arguments.length; i++) {
                this.dayHeaders[i] = arguments[i];
                }
        }

// Set the day of the week (0-7) that the calendar display starts on
// This is for countries other than the US whose calendar displays start on Monday(1), for example
function CalendarPopup_setWeekStartDay(day) {
        this.weekStartDay = day;
        }

// Show next/last year navigation links
function CalendarPopup_showYearNavigation() {
        this.isShowYearNavigation = true;
        }

// Hide a calendar object
function CalendarPopup_hideCalendar() {
        if (arguments.length > 0) {
                window.popupWindowObjects[arguments[0]].hidePopup();
                }
        else {
                this.hidePopup();
                }
        }

// Refresh the contents of the calendar display
function CalendarPopup_refreshCalendar(index) {
        var calObject = window.popupWindowObjects[index];
        if (arguments.length>1) {
                calObject.populate(calObject.getCalendar(arguments[1],arguments[2]));
                }
        else {
                calObject.populate(calObject.getCalendar());
                }
        calObject.refresh();
        }

// Populate the calendar and display it
function CalendarPopup_showCalendar(anchorname) {
        this.populate(this.getCalendar());
        this.showPopup(anchorname);
        }

// Get style block needed to display the calendar correctly
function CalendarPopup_getStyles() {
        var result = "";
        result += "<STYLE>\n";
        result += "TD.cal { font-family:arial; font-size: 8pt;}\n";
        result += "TD.cal_mes { font-family:arial; font-size: 8pt; background-color:#002751; color:#FFFFFF;}\n";
        result += "TD.calmonth { font-family:arial; font-size: 8pt; text-align: right;}\n";
        //result += "TD.caltoday { font-family:arial; font-size: 8pt; text-align: right; color: white; background-color:#C0C0C0; border-width:1; border-type:solid; border-color:#800000; }\n";
        result += "TD.caltoday { font-family:arial; font-size: 8pt; text-align: right; color: white; background-color:#FFF9B3; border-width:1; border-type:solid; border-color:#800000; }\n";
        //result += "A.todaylink { font-family:arial; font-size: 8pt; height: 20px; color: black; }\n";
        result += "A.todaylink { font-family:arial; font-size: 8pt; height: 20px; color: 002751; }\n";
        result += "A.cal { text-decoration:none; color:#FFFFFF; }\n";
        result += "A.cal:active{ text-decoration:none; color:#FFFFFF; }\n";
        result += "A.cal:hover{ text-decoration:none; color:#FFFFFF; }\n";
        result += "A.calthismonth { text-decoration:none; color:#002751;}\n";
        result += "A.calothermonth { text-decoration:none; color:#F8F3E8; }\n";
        result += "</STYLE>\n";
        return result;
        }

// Return a string containing all the calendar code to be displayed
function CalendarPopup_getCalendar() {
        var now = new Date();
        if (arguments.length > 0) { var month = arguments[0]; }
                else { var month = now.getMonth()+1; }
        if (arguments.length > 1) { var year = arguments[1]; }
                else { var year = now.getFullYear(); }
        var daysinmonth= new Array(0,31,28,31,30,31,30,31,31,30,31,30,31);
        if ( ( (year%4 == 0)&&(year%100 != 0) ) || (year%400 == 0) ) { // leap year
                daysinmonth[2] = 29;
                }
        var current_month = new Date(year,month-1,1);
        var display_year = year;
        var display_month = month;
        var display_date = 1;
        var weekday= current_month.getDay();
        var offset = 0;
        if (weekday >= this.weekStartDay) {
                offset = weekday - this.weekStartDay;
                }
        else {
                offset = 7-this.weekStartDay+weekday;
                }
        if (offset > 0) {
                display_month--;
                if (display_month < 1) { display_month = 12; display_year--; }
                display_date = daysinmonth[display_month]-offset+1;
                }
        var next_month = month+1;
        var next_month_year = year;
        if (next_month > 12) { next_month=1; next_month_year++; }
        var last_month = month-1;
        var last_month_year = year;
        if (last_month < 1) { last_month=12; last_month_year--; }
        var date_class;
        var result = "";
        if (this.type == "WINDOW") {
                var windowref = "window.opener.";
                }
        else {
                var windowref = "";
                }
        // If POPUP, write entire HTML document
        if (this.type == "WINDOW") {
                result += "<HTML><HEAD><TITLE>Calendario</TITLE>"+this.getStyles()+"</HEAD><BODY BGCOLOR=#F5FBF9 MARGINWIDTH=0 MARGINHEIGHT=0 TOPMARGIN=0 RIGHTMARGIN=0 LEFTMARGIN=0>\n";
                result += '<CENTER><TABLE WIDTH=100% BORDER=0 BORDERWIDTH=0 CELLSPACING=0 CELLPADDING=0>\n';
                }
        else {
                result += '<TABLE WIDTH=144 BORDER=1 BORDERWIDTH=1 BORDERCOLOR="#808080" CELLSPACING=0 CELLPADDING=1>\n';
                result += '<TR><TD ALIGN=CENTER>\n';
                result += '<CENTER>\n';
                result += '<TABLE WIDTH=144 BORDER=0 BORDERWIDTH=0 CELLSPACING=0 CELLPADDING=0>\n';
                }
        result += '<TR BGCOLOR="#002751">\n';
        if (this.isShowYearNavigation) {
                result += '<TD BGCOLOR="#002751" CLASS="cal" WIDTH=144 ALIGN=CENTER VALIGN=MIDDLE>';
                result += '<B><A CLASS="cal" HREF="javascript:'+windowref+'CalendarPopup_refreshCalendar('+this.index+','+last_month+','+last_month_year+');">&lt;</A></B>&nbsp;&nbsp;'+this.monthNames[month-1]+'&nbsp;&nbsp;<B><A CLASS="cal" HREF="javascript:'+windowref+'CalendarPopup_refreshCalendar('+this.index+','+next_month+','+next_month_year+');">&gt;</A></B>\n';
                result += '&nbsp;&nbsp;&nbsp;&nbsp;';
                result += '<B><A CLASS="cal" HREF="javascript:'+windowref+'CalendarPopup_refreshCalendar('+this.index+','+month+','+(year-1)+');">&lt;</A></B>&nbsp;&nbsp;'+year+'&nbsp;&nbsp;<B><A CLASS="cal" HREF="javascript:'+windowref+'CalendarPopup_refreshCalendar('+this.index+','+month+','+(year+1)+');">&gt;</A></B>\n';
                result += '</TD>\n';
                }
        else {
                result += '     <TD BGCOLOR="#002751" CLASS="cal_mes" WIDTH=22 ALIGN=CENTER VALIGN=MIDDLE><B><A CLASS="cal" HREF="javascript:'+windowref+'CalendarPopup_refreshCalendar('+this.index+','+last_month+','+last_month_year+');">&lt;&lt;</A></B></TD>\n';
                result += '     <TD BGCOLOR="#002751" CLASS="cal_mes" WIDTH=100 ALIGN=CENTER>'+this.monthNames[month-1]+' '+year+'</TD>\n';
                result += '     <TD BGCOLOR="#002751" CLASS="cal_mes" WIDTH=22 ALIGN=CENTER VALIGN=MIDDLE><B><A CLASS="cal" HREF="javascript:'+windowref+'CalendarPopup_refreshCalendar('+this.index+','+next_month+','+next_month_year+');">&gt;&gt;</A></B></TD>\n';
                }
        result += '</TR></TABLE>\n';
        result += '<TABLE WIDTH=120 BORDER=0 CELLSPACING=1 CELLPADDING=0 ALIGN=CENTER>\n';
        result += '<TR>\n';
        result += '     <TD CLASS="cal" ALIGN=RIGHT WIDTH=14%>'+this.dayHeaders[(this.weekStartDay)%7]+'</TD>\n';
        result += '     <TD CLASS="cal" ALIGN=RIGHT WIDTH=14%>'+this.dayHeaders[(this.weekStartDay+1)%7]+'</TD>\n';
        result += '     <TD CLASS="cal" ALIGN=RIGHT WIDTH=14%>'+this.dayHeaders[(this.weekStartDay+2)%7]+'</TD>\n';
        result += '     <TD CLASS="cal" ALIGN=RIGHT WIDTH=14%>'+this.dayHeaders[(this.weekStartDay+3)%7]+'</TD>\n';
        result += '     <TD CLASS="cal" ALIGN=RIGHT WIDTH=14%>'+this.dayHeaders[(this.weekStartDay+4)%7]+'</TD>\n';
        result += '     <TD CLASS="cal" ALIGN=RIGHT WIDTH=14%>'+this.dayHeaders[(this.weekStartDay+5)%7]+'</TD>\n';
        result += '     <TD CLASS="cal" ALIGN=RIGHT WIDTH=14%>'+this.dayHeaders[(this.weekStartDay+6)%7]+'</TD>\n';
        result += '</TR>\n';
        result += '<TR><TD COLSPAN=7 ALIGN=CENTER><IMG SRC="graypixel.gif" WIDTH=120 HEIGHT=1></TD></TR>\n';
        for (var row=1; row<=6; row++) {
                result += '<TR>\n';
                for (var col=1; col<=7; col++) {
                        if (display_month == month) {
                                date_class = "calthismonth";
                                }
                        else {
                                date_class = "calothermonth";
                                }
                        if ((display_month == now.getMonth()+1) && (display_date==now.getDate()) && (display_year==now.getFullYear())) {
                                td_class="caltoday";
                                }
                        else {
                                td_class="calmonth";
                                }
                        result += '     <TD CLASS="'+td_class+'"><A HREF="javascript:'+windowref+this.returnFunction+'('+display_year+','+display_month+','+display_date+');'+windowref+'CalendarPopup_hideCalendar(\''+this.index+'\');" CLASS="'+date_class+'">'+display_date+'</A></TD>\n';
                        display_date++;
                        if (display_date > daysinmonth[display_month]) {
                                display_date=1;
                                display_month++;
                                }
                        if (display_month > 12) {
                                display_month=1;
                                display_year++;
                                }
                        }
                result += '</TR>';
                }
        result += '<TR><TD COLSPAN=7 ALIGN=CENTER><IMG SRC="graypixel.gif" WIDTH=120 HEIGHT=1></TD></TR>\n';
        result += '<TR>\n';
        result += '     <TD COLSPAN=7 ALIGN=CENTER>\n';
        result += '             <A CLASS="todaylink" HREF="javascript:'+windowref+this.returnFunction+'(\''+now.getFullYear()+'\',\''+(now.getMonth()+1)+'\',\''+now.getDate()+'\');'+windowref+'CalendarPopup_hideCalendar(\''+this.index+'\');">Hoy</A>\n';
        result += '             <BR>\n';
        result += '     </TD></TR></TABLE></CENTER></TD></TR></TABLE>\n';
        if (this.type == "WINDOW") {
                result += "</BODY></HTML>\n";
                }
        return result;
        }
