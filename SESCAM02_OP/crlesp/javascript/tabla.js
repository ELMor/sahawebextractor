var MESES = 'EneFebMarAbrMayJunJulAgoSepOctNovDic';
var tabla;

/* M�todo para crear una tabla ordenada
*/
function Tabla (tBody, vAnchos) {
  this.tBody = tBody;
  this.vAnchos = vAnchos;
  this.sort = Tabla_sort;
  this.mostrar = Tabla_mostrar;
  this.convierteHTML = Tabla_convierteHTML;
  this.mostrar();
  
  this.Ascendente=true;
  this.ultimoOrdenado=-1;
}

function Tabla_sort (Tipo, columna) {
  Tipo = Tipo || ordenAlfanumerico; //Orden alfanumerico por defecto
  
  /* Ordena columna 0 por defecto. Lo mete por variable miembro ya que el
     m�todo sort solo admite como funci�n comparadora "compara (elem1, elem2)"
  */
  Tipo.columna = columna ? columna : 0;
  
  //Determinamos el si la ordenacion es ascentente o descentente
  this.Ascendente= (this.ultimoOrdenado==Tipo.columna)?!this.Ascendente:true;
  
  //Le pasamos el orden a la funcion de ordenacion
  Tipo.Ascendente=this.Ascendente;
  
  //Ordena el array mediante la correspondiente funci�n de comparaci�n
  this.tBody.sort(Tipo);
  
  //Guardamos la columna de la ultima ordenacion
  this.ultimoOrdenado=Tipo.columna;
  
  this.mostrar(); //Actualizar la tabla
}

function ordenAlfanumerico (tupla1, tupla2) {
  var columnaObjetivo = ordenAlfanumerico.columna; //Recoge la columna a ordenar
  var valor1 = tupla1[columnaObjetivo];  //Recoge los elementos a comparar
  var valor2 = tupla2[columnaObjetivo];
  if (ordenAlfanumerico.Ascendente)
     return valor1 < valor2 ? -1 : valor1 == valor2 ? 0 : 1;
  else
     return valor1 < valor2 ? 1 : valor1 == valor2 ? 0 : -1;
}


function ordenNumerico (tupla1, tupla2) {
  var columnaObjetivo = ordenNumerico.columna; //Recoge la columna a ordenar
  var valor1 = parseFloat(tupla1[columnaObjetivo]);  //Recoge los elementos a comparar
  var valor2 = parseFloat(tupla2[columnaObjetivo]);
  
  if (ordenNumerico.Ascendente)
  	 return valor1 < valor2 ? -1 : valor1 == valor2 ? 0 : 1;
  else
  	 return valor1 < valor2 ? 1 : valor1 == valor2 ? 0 : -1;
}

function ordenPeriodo (tupla1, tupla2) {
  var columnaObjetivo = ordenPeriodo.columna; //Recoge la columna a ordenar
  var anyo1 = parseFloat(tupla1[columnaObjetivo].substring(4,7));  //Recoge el a�o
  var anyo2 = parseFloat(tupla2[columnaObjetivo].substring(4,7));
  var mes1 = MESES.indexOf(tupla1[columnaObjetivo].substring(0,2));  //Recoge el mes
  var mes2 = MESES.indexOf(tupla2[columnaObjetivo].substring(0,2));

  if (ordenPeriodo.Ascendente) {
     if (anyo1 < anyo2)
        return -1;
     else if (anyo1 > anyo2)
        return 1;
     else 
        return mes1 < mes2 ? -1 : mes1 == mes2 ? 0 : 1;
  }
  else {
     if (anyo1 < anyo2)
        return 1;
     else if (anyo1 > anyo2)
        return -1;
     else
        return mes1 < mes2 ? 1 : mes1 == mes2 ? 0 : -1;
  }
}

/* Tabla_mostrar muetra la tabla. Versi�n para dos frames: en 'columna' muestra la
*  primera columna y en 'datos' el resto de la tabla
*/
function Tabla_mostrar() {
  var html1 = this.convierteHTML('columna');
  var html2 = this.convierteHTML('datos');
  
  if (columna.document.layers) {  //Procedimientos Netscape
    columna.document.cuerpo.document.open();
    columna.document.cuerpo.document.write(html1);
    columna.document.cuerpo.document.close();
    datos.document.cuerpo.document.open();
    datos.document.cuerpo.document.write(html2);
    datos.document.cuerpo.document.close();
  } else if (columna.document.all) {  //Procedimientos Explorer
    columna.cuerpo.innerHTML = html1;
    datos.cuerpo.innerHTML = html2;
  } else if (columna.document.createRange) { //Procedimientos ???
    var r1 = columna.cuerpo.createRange();
    var r2 = columna.cuerpo.createRange();
    while (columna.cuerpo.hasChildNodes())
          columna.cuerpo.removeChild(columna.cuerpo.lastChild);
    while (datos.cuerpo.hasChildNodes())
          datos.cuerpo.removeChild(datos.cuerpo.lastChild);
    r1.setStartAfter(columna.cuerpo);
    r2.setStartAfter(datos.cuerpo);
    var docFrag1 = r1.createContextualFragment(html1);
    var docFrag2 = r2.createContextualFragment(html2);
    columna.cuerpo.appendChild(docFrag1);
    datos.cuerpo.appendChild(docFrag2);
  }
}

function Tabla_convierteHTML(Tipo) {
  var tuplas = this.tBody;
  var anchos = this.vAnchos;
  var html='';
  var cmin=0; cmax=1;
  var totales= new Array();
  
  if (Tipo=='datos') {cmin=1;cmax=tuplas[0].length;}
  html +='<TABLE BORDER="0" WIDTH="'+suma(anchos, Tipo)+'px">';
  for (var r = 0; r < tuplas.length; r++) {
    html += '<TR>'; 
    for (var c = cmin; c < cmax; c++) {
      if (r==0) totales[c]=0;
      totales[c] += parseInt(tuplas[r][c]);
      html += '<TD width="'+anchos[c]+'px" class='
      html += (r%2)==1 ? '"par">' : '"impar">';
      html +=tuplas[r][c];
      html +='<\/TD>';
    }
    html += '<\/TR>';
  }
  html += '<TR>';
  for (c = cmin; c < cmax; c++) {
    html += '<TD width="'+anchos[c]+'px" class="pie">';
    if (Tipo=='datos') 
       html += isNaN(parseInt(totales[c])) ? '-' : totales[c];
    else
       html += 'TOTAL:';
    html += '<\/TD>';
  }
  html += '<\/TR><TR>'
  for (c = cmin; c < cmax; c++) {
    html += '<TD width="'+anchos[c]+'px" class="pie">';
    if (Tipo=='datos') 
       html += isNaN(parseInt(totales[c])) ? '-' : redondea(totales[c]/tuplas.length, 2);
    else
       html += 'MEDIA:';
    html += '<\/TD>';
  }
  html += '<\/TR>';
  if (Tipo!='datos') html += '<TR><TD>&nbsp;<\/TD><\/TR>';
  html += '<\/TABLE>';
  return html;
}
       
function redondea (numero, decimales) {
  numero *= Math.pow(10, decimales);
  numero = Math.round(numero);
  return numero/Math.pow(10, decimales);
}

function suma (vector, Tipo) {
  var total=0;
  if (Tipo=='datos') {
    for(i=1; i<vector.length; total += vector[i++]);
    return total;
  } else return vector[0];
}
