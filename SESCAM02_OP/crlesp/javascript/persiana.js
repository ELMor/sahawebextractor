/*VARIABLES GLOBALES*/
var abiertoIZQI = 0; //para saber si estamos cerrando o abriendo la persiana
var retardoIZQI = 1; //el retardo del movimiento
var saltoIZQI = 175;	 //umbral en el que se cierra o se abre la persiana completamente

var navegadorIZQI;
var ie;
navegadorIZQI = navigator.appName;
ie = (navegadorIZQI.indexOf("Explorer")!=-1) ? true:false;

	
	// Datos para el hijo que trabajará con las filas.
var filasIZQI=parent.menu.rows;
var colsIZQI=parent.menu.cols;
var framesIZQI= new Array(4); // array que contiene las longitudes de los frames
		    			  // que componen el frameset del hijo
		    			  

var vacioIZQI='';
var nuevoIZQI='';
var IZQI;
var indiceIZQI=0;


//desgloso filas y almaceno las longitudes en el array
for (IZQI=0;IZQI<colsIZQI.length;IZQI++){
	if (colsIZQI.charAt(IZQI)!=','){
		nuevoIZQI=nuevoIZQI.concat(vacioIZQI,colsIZQI.charAt(IZQI).toString());
	}
	else{
		frames[indiceIZQI++]=nuevoIZQI;
		nuevoIZQI='';
	}
	
}
framesIZQI[indiceIZQI]=nuevoIZQI;

var tamanioIZQI=framesIZQI[0];//longitud del frame 1 efecto de la persiana
var totalIZQI= 0;	//longitud total que se abre y cierra el frame
var cabeceraIZQI=framesIZQI[1];//longitud que ocupa la cabecera
var paginaIZQI=framesIZQI[2];
var valorIZQI = tamanioIZQI + ",*," + paginaIZQI;
//var valorIZQI = tamanioIZQI + ",*";

function actualizar() 
{
	filasIZQI=parent.menu.rows;
	colsIZQI=parent.menu.cols;
	vacioIZQI='';
	nuevoIZQI='';
	IZQI=0;
	indiceIZQI=0;
	
	//desgloso filas y almaceno las longitudes en el array
	for (IZQI=0;IZQI<colsIZQI.length;IZQI++){
		if (colsIZQI.charAt(IZQI)!=','){
			nuevoIZQI=nuevoIZQI.concat(vacioIZQI,colsIZQI.charAt(IZQI).toString());
		}
		else{
			framesIZQI[indiceIZQI++]=nuevoIZQI;
			nuevoIZQI='';
		}
		
	}
	
	framesIZQI[indiceIZQI]=nuevoIZQI;
	tamanioIZQI=framesIZQI[0];//longitud del frame 1 efecto de la persiana
	totalIZQI= 0;	//longitud total que se abre y cierra el frame
	cabeceraIZQI=framesIZQI[1];//longitud que ocupa la cabecera
	paginaIZQI=framesIZQI[2];
	valorIZQI = tamanioIZQI + ",*," + paginaIZQI;
	//valorIZQI = tamanioIZQI + ",*";
}

// funcion que se encarga de establecer la aceleracion y llamar a abrir que es
// quien realmente se encarga de abrir y cerrar el frame persiana

function persianaIZQI(num){
	actualizar();	
	aceleracionIZQI=num;
	retardoIZQI=num;
	abrirIZQI();
}

function abrirIZQI( ) {
	if (abiertoIZQI==0) {// Abrimos la ventana		
// Para el hijo

	 	if (tamanioIZQI<(totalIZQI+5))
	 		tamanioIZQI=totalIZQI;
	 	else {
	 		//establecemos el incremento de movimiento
	 		tamanioIZQI=parseInt(tamanioIZQI-(tamanioIZQI)/aceleracionIZQI); 
		}
		var valorIZQI = tamanioIZQI + ",*," + paginaIZQI;
		//var valorIZQI = tamanioIZQI + ",*";
		parent.menu.cols=valorIZQI;
		
		if (tamanioIZQI>totalIZQI){
   			 setTimeout('abrirIZQI()',retardoIZQI);
   		}
 		else {
 			abiertoIZQI=1;
		}
		
	}
	else {// Cerramos la ventana
// Para el hijo	
		
	 	if (tamanioIZQI>saltoIZQI)
	 		tamanioIZQI=180;
	 	else
	 		//establecemos el decremento de movimiento
	 		tamanioIZQI=parseInt(tamanioIZQI+(tamanioIZQI+40)/aceleracionIZQI);	 	
		
	 	//var valorIZQI = tamanioIZQI + ",*," + paginaIZQI;
	 	var valorIZQI = tamanioIZQI + ",*";
	 	parent.menu.cols=valorIZQI;
	 	
	 	if (tamanioIZQI<180)
	 		setTimeout('abrirIZQI()',retardoIZQI);
	 	else {
	  		abiertoIZQI=0;
			
	 	}
	 }
}

function correrPersiana()
	{
		persianaIZQI(20);
	}