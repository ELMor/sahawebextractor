<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="pacientes/error.jsp"%>
<%@ page import="conf.*,isf.negocio.*" %>

<%
 int longitud;
 FicheroClave fc = new FicheroClave();
 longitud = fc.DameLongitudClave();
%>

<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<title>
Acceso
</title>
<link rel=stylesheet type="text/css" href="../css/estilo.css">
<jsp:useBean id="beanClave" scope="session" class="java.util.HashMap"/>
</head>
<script language="javascript" type="text/javascript" src="../javascript/botonera.js"></script>
</script>
<script language="javascript">
  var txt="Registro de Pacientes de Lista de Espera de Castilla-La Mancha.                                         ";

  function scroll(){
   window.status = txt;
   //txt = txt.substring(1, txt.length) + txt.charAt(0);
   //window.setTimeout("scroll()",150);
  }

  function hacer_submit() {
    this.document.forms('controlAccesoForm').submit();
  }

  function control_teclado(){
    if (event.keyCode == 13) hacer_submit();
    if (((event.keyCode > 47) && (event.keyCode < 58)) || ((event.keyCode >64) && (event.keyCode <91)) || ((event.keyCode >96) && (event.keyCode <123)) || (event.keyCode==45) || (event.keyCode==95) || (event.keyCode==209) || (event.keyCode==241) ) event.returnValue=true;
         else event.returnValue=false;
  }

  function abrir_modal(){
    window.showModalDialog("nueva_clave.jsp?expirada='no'","","dialogWidth:25em;dialogHeight:15em");
  }

  function cambiar_clave(){
    this.document.all.desde.value="b";
    hacer_submit();
    this.document.all.desde.value="a";
  }
</script>
<body class="paginaPrincipalCentro" onLoad="scroll();document.controlAccesoForm.usuario.focus();">
<form name="controlAccesoForm" method="post" target="ctrl_acceso" action="acceso.jsp" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" vspace="0" hspace="0">
<tr>
<td width="30%"></td>
<td width="40%">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" vspace="0" hspace="0">
  <tr class="cabeceraSuperior">
    <td>Usuario</td>
    <td><input type="text" name="usuario" id="usuario" maxlength=8></td>
  </tr>
  <tr class="cabeceraSuperior">
    <td>Clave</td>
    <td><input type="password" name="password" id="password" onkeypress="control_teclado();" maxlength=<%=longitud%>></td>
    <td align="center"><a onClick="cambiar_clave();"style=cursor:"hand"><img src="../imagenes/candado.gif" alt="Cambiar clave"></a></td>
  </tr>
  <tr class="cabeceraSuperior">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="cabeceraSuperior">
    <td></td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="document.forms('controlAccesoForm').submit();">Acceder</td>
  </tr>
</table>
<br>
<br>
<br>
<img src="../imagenes/logoSescam.gif">
</td>
<td width="30%"><input type="hidden" name="desde" value="a"></td>
</tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
