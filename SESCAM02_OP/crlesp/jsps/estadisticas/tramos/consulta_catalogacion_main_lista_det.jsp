<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaCatalogacionBean" %>
<%@ page import="isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Estad";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
function seleccionaFila(filas, id){
    filas[id].className = "filaSelecionada";
    }
function seleccion(pSelCip,centro,norden,cq){
  parent.parent.parent.location='../../pacientes/ficha_paciente_index.jsp?cip='+escape(pSelCip)+'&centro='+centro+'&norden='+norden+'&gar_cq='+cq;
}
</script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="consulta">
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
    <table style="width:100%;" align="left" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral2" >
	<table class="centroTabla" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
<%
		PacientesPorCatalogacion pacCat;
		Vector vp=new Vector();
                String scatalogacion = "";
                int i_tramo,i_lista;
                long l_centro;
		boolean bFilaPar=true;
		int idCounter=0;
                ConsultaCatalogacionBean ccatalogacion =new ConsultaCatalogacionBean();
                //System.out.println("Creo el ConsultaSolicitudesBean");
                 if (request.getParameter("catalogacion")!=null) {
                  i_tramo=new Integer(request.getParameter("tramo")).intValue();
                  scatalogacion=request.getParameter("catalogacion");
                  i_lista =new Integer(request.getParameter("tipolista")).intValue();
                  l_centro=new Double(request.getParameter("centro")).longValue();

                  ccatalogacion.setTramo(i_tramo);
                  ccatalogacion.setCatalogacion(scatalogacion);
                  ccatalogacion.setOrden(request.getParameter("orden"));
                  ccatalogacion.setTipoLista(i_lista);
                  ccatalogacion.setCentro(l_centro);
//                  ccatalogacion.setFechaini(request.getParameter("fechaini"));
//                  ccatalogacion.setFechafin(request.getParameter("fechafin"));

                  //System.out.println("Seteo los filtros");
                  try {
                    vp=ccatalogacion.resultado();
                  }catch (ExceptionSESCAM eSES){
%>
                    <script language="javascript">
                      alert("<%=eSES%>");
                    </script>
<%
                 }


%>
                <Script language="javaScript">
                  parent.pie.location="../../pie_buscador.jsp?estado=Generando Resultado...";
                </script>

<%
		if (vp.size()>0){
	  		Enumeration e = vp.elements();
                        pacCat = new PacientesPorCatalogacion();
	    		while(e.hasMoreElements()) {
	      			pacCat=(PacientesPorCatalogacion)e.nextElement();
	      			if (bFilaPar){
	      			   	bFilaPar=false;
                                        //System.out.println("Estado"+idCounter+": "+solRet.fEstado);
%>
					<tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);seleccion('<%=pacCat.Cip%>',<%=pacCat.CodCentro%>,<%=pacCat.Norden%>,'<%=pacCat.Cq%>')" style="cursor:hand;">
<%       			}else{
					bFilaPar=true;
                                        //System.out.println("Estado"+idCounter+": "+solRet.fEstado);
%>
					<tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);seleccion('<%=pacCat.Cip%>',<%=pacCat.CodCentro%>,<%=pacCat.Norden%>,'<%=pacCat.Cq%>')" style="cursor:hand;">
<%
				}
%>
                                <td width="12%"  height="12px" name="norden"><%=pacCat.Norden%></td>
				<td width="10%"  height="12px" name="centro" title="<%=pacCat.Centro%>"><%=pacCat.CodCentro%></td>
				<td width="22%"  height="12px" name="cip"><%=pacCat.Cip%></td>
				<td width="33%" height="12px" name="nom"><%=pacCat.Ape1+' '+pacCat.Ape2+','+pacCat.Nombre%></td>
                                <td width="13%"  height="12px" name="antiguedad"><%=pacCat.antiguedad%></td>
                                <td width="10%"  height="12px" name="antiguedad"><%=pacCat.Situacion%></td>

                             </tr>

<%
  				idCounter++;
  				}
                                %>
                          <script language="javascript">
                              parent.pie.location="../../pie_buscador.jsp?estado=Listo&filas=<%=idCounter%>";
                           </script>
<%
                        }else{
%>
                           <Script language="javaScript">
                              parent.pie.location="../../pie_buscador.jsp?estado=No se han encontrado filas.";
                           </script>
<%			}
                 }
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
