<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.util.Utilidades,isf.negocio.DatosEstadisticasLEQ,beans.ConsultaEntradasLE,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Estad";%>
<%@ include file="/jsps/control_acceso.txt" %>

<jsp:useBean id="datosEstadisticasBean" scope="session" class="java.util.Properties"/>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/SelecUtil.js"></script>
</head>
<script language="javascript">
</script>
<BODY class="mainFondo">
<%
   String str_criterio = (String) request.getParameter("criterio");
   Vector v_estad = (Vector) datosEstadisticasBean.get("v_estadistica");

	 if (v_estad != null && str_criterio != null) {
		 int nsituaciones = ConsultaEntradasLE.STR_SITUACION.length - 1;
		 int anchura = 100/ (nsituaciones  + 2);

%>
<table class="cabeceraTabla"  border="0" cellspacing="0" cellpadding="0" align="center">
    <tr class="filaCabecera">
    <td align="left" width="<%=anchura*2%>%"><a href="#" class="menuOpcion1"><%=str_criterio%></a></td>
<%
	for (int int_pos = 1; int_pos < ConsultaEntradasLE.STR_SITUACION.length - 1; int_pos++) {
%>
   <td align="left" width="<%=anchura%>%" ><a href="#" class="menuOpcion1"><%=ConsultaEntradasLE.STR_SITUACION[int_pos]%></a></td>
<% } %>

  </tr>
	<tr>
<table class="centroTabla"  border="0" cellspacing="0" cellpadding="0" align="center">
<%
		 Enumeration e = v_estad.elements();
     DatosEstadisticasLEQ EstEntidad = null;
		 int idCounter = 0;
	    while(e.hasMoreElements()) {
				EstEntidad=(DatosEstadisticasLEQ)e.nextElement();
				if ((idCounter % 2) == 0) {
%>
       <tr class="filaPar">
<%
       } else {
%>
     <tr class="filaImpar">
<%		 } %>
     <td align="left" width="<%=anchura*2%>%"><%=EstEntidad.DatoGroupby.trim()%></td>
     <td align="left" width="<%=anchura%>%"><%=Utilidades.cambiarFormato(new Long(EstEntidad.pacientes).toString(),",",".",0)%></td>
<%

	 for (int int_pos = 0; int_pos < EstEntidad.situaciones.length; int_pos++) {

%>
    <td align="left" width="<%=anchura%>%"><%=Utilidades.cambiarFormato(new Long(EstEntidad.situaciones[int_pos]).toString(),",",".",0)%></td>

	<% } %>
  </tr>

<%
       idCounter++;
			}
%>
</table>
</tr>
</table>

<%
	 }
%>

</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
