<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,beans.ConsultaTramos"%>
<%@ page import="java.util.Vector" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Estad";%>
<%@ include file="/jsps/control_acceso.txt" %>

<jsp:useBean id="datosEstadisticasBean" scope="session" class="java.util.Properties"/>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">

<script language="javascript">

function submit(periodo,anchura,ntramo){
  parent.detalle.location= "consulta_lesp_servicio_detalle.jsp?periodo="+periodo+"&anchura="+anchura+"&tramos="+ntramo;
}

</script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<%
  String campoCLOB = "";
  String periodo = request.getParameter("periodo");
  ConsultaTramos ctramos = new ConsultaTramos();
  ctramos.setPeriodo(periodo);
  int n_tramos = ctramos.CargarCLOB();
  int anchuraminima = 100/(n_tramos+6);
  int anchuracentro = anchuraminima*2;
%>
  <script language="javascript">
    submit("<%=periodo%>",<%=anchuraminima%>,<%=n_tramos%>);
  </script>
<%
%>
<form name="date">
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
  <table style="width:100%;" align="left" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral2" >
	<table class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="0">
	   <tr class="filaCabecera">
            <td width="<%=anchuracentro%>%" height="30px">
                  <a href="#" class="menuOpcion1">Especialidad</a>
            </td>
<%
          String[] desc;
          desc = ctramos.getDescripcion();
          Integer[] pos_inicio = ctramos.getInicio();
          Integer[] pos_final = ctramos.getFin();
          String cabecera = "";
          String descrip = "";
          int inicio = 0, fin = 0;
					Vector vec_tramos = new Vector(); // vector con las definiciones de los tramos.
          for(int int_pos = 0;int_pos < n_tramos;int_pos++) {
           descrip = desc[int_pos];
           inicio = pos_inicio[int_pos].intValue();
           fin = pos_final[int_pos].intValue();
           if (fin == -1) {
             inicio = inicio -1;
             cabecera =  "> " + inicio;
           }else {
             cabecera =  inicio + "-" + fin;
           }
					 vec_tramos.add(cabecera);

%>
           <td width="<%=anchuraminima%>%" height="12px">
                  <a href="#" class="menuOpcion1"><%=cabecera%></a>
           </td>
<%
          }
					// a�adir los tramos al bean.
					datosEstadisticasBean.put("v_tramos",vec_tramos);

%>
           <td width="<%=anchuraminima%>%" height="12px">
                  <a href="#" class="menuOpcion1">LEQ</a>
           </td>
            <td width="<%=anchuraminima%>%" height="12px">
                  <a href="#" class="menuOpcion1">D. Media</a>
            </td>
           <td width="<%=anchuraminima%>%" height="12px">
                  <a href="#" class="menuOpcion1" onclick="window.showModalDialog('ficha_desglose_lesp_index.jsp?criterio=Especialidades','','dialogWidth:40em;dialogHeight:15em;status=no');">Pdtes.</a>
           </td>
           <td width="<%=anchuraminima%>%" height="12px">
                  <a href="#" class="menuOpcion1">D. Pdtes.</a>
            </td>
          </tr>
       </table>
   </td>
  </tr>
</table>
</td>
</tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
