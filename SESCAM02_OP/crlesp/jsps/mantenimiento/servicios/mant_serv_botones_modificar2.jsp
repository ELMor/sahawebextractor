<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</script>

<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">

  function guardar(dato_origen,pk) {
        respuesta = confirm("Desea guardar los datos?")
        if (respuesta == true){
            parent.centro.document.formulario.origen.value = dato_origen;
            parent.centro.document.formulario.pk_esp.value = pk;
            parent.centro.document.formulario.submit();
        }else {
            switch (dato_origen) {
                    case 1: {
                      break;
                    }
                    case 2:  {
                     parent.centro.location.href = "../especialidades/mant_esp_modificar_datos_especialidad.jsp?codesp="+pk;
                     parent.botonera.location.href = "../especialidades/mant_esp_botones_modificar.jsp";
                     break;
                    }
                    case 3:  {
                    }
            }
        }
  }


      function cancelar(dato_origen,pk) {
        alert("No se guardar�n los cambios.");
        switch (dato_origen) {
          case 2: {
              parent.centro.location.href = "../especialidades/mant_esp_modificar_datos_especialidad.jsp?codesp="+pk;
              parent.botonera.location.href = "../especialidades/mant_esp_botones_modificar.jsp";
              break;
         }
        }
      }
</script>

</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

<%
  String l_origen = request.getParameter("origen");
  String l_reqpk = request.getParameter("pk_destino");
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="20%" class="botonAccion">&nbsp;</td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="guardar(<%=l_origen%>,'<%=l_reqpk%>');">Guardar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar(<%=l_origen%>,'<%=l_reqpk%>');">Cancelar</td>
    <td  width="20%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
