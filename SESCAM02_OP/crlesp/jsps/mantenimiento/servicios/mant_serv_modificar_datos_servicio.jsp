<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave, isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/SelecUtil.js"></script>
</head>
<jsp:useBean id="beanServicios" class="java.util.HashMap" scope="session"/>
<BODY class="mainFondo">
	<%
		Servicios serv= new Servicios();
		Vector vec = new Vector();
		String str_codserv = request.getParameter("codserv");
		vec = serv.busquedaServ("trim(SESSRV_COD)=trim('"+str_codserv+"')","");
                SesServicios sesServ=(SesServicios)vec.elementAt(0);
                beanServicios.put("SERVICIO",sesServ);
	%>
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> Editar Servicio</td>
  </tr>
</table>
<form name="formulario" action="modificarServicio.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto" width="20%">Codigo:</td>
    <td class= "normal" width="80%"><input type="text" style="width:10%" maxlength=4 name="codserv" class="cajatexto2" value="<%=sesServ.getSessrvCod()%>" readonly></td>
  </tr>
  <tr>
    <td class= "texto">Descripci&oacute;n: </td>
    <td class= "normal"><input type="text" style="width:50%" maxlength=30 name="descserv" class="cajatexto3" value="<%=sesServ.getSessrvDesc()%>" ></td>
  </tr>
  <tr>
    <td class= "texto">Especialidad: </td>
    <td class= "normal"><div id="divEspecialidad">
          <Select name="especialidad" class="cajatexto3" style="width:50%">
           <%
	   Especialidades esp = new Especialidades();
	   Vector v = new Vector();
	   v = esp.busquedaEsp("","");
	   for(int int_pos = 0;int_pos < v.size(); int_pos++){
		SesEspecialidades sesEsp=(SesEspecialidades)v.elementAt(int_pos);
	   	if (sesServ.getSesespCod().equals(sesEsp.getSesespCod())) {
	   %>
		<option  value='<%=sesServ.getSesespCod()%>' selected><%=sesEsp.getSesespDesc()%></option>
 	   <%
 	        }else{
           %>
 	   	<option value='<%=sesEsp.getSesespCod()%>'><%= sesEsp.getSesespDesc()%></option>
 	   <%
              }
 	   }
%>
	   </Select></div>
    </td>
  </tr>
</table>
<input type="hidden" name="origen" value="0">
<input type="hidden" name="pk_esp" value="">
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
