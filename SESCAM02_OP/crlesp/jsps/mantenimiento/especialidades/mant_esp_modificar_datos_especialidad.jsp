<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave, isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/SelecUtil.js"></script>
</head>
<jsp:useBean id="beanEspecialidad" class="java.util.HashMap" scope="session"/>
<script language="javascript">
  var FilaSelected=-1;
  function seleccionaFila(filas, id){
      if (FilaSelected!=-1){
          if((FilaSelected%2)==0)
            filas[FilaSelected].className = "filaPar";
          else
            filas[FilaSelected].className = "filaImpar";
         }
      filas[id].className = "filaSelecionada";
      FilaSelected=id;
    }

  function selec(codserv,dato_origen,destino){

    parent.centro.location = "../servicios/mant_serv_modificar_datos_servicio.jsp?codserv="+codserv;
    parent.botonera.location.href = "../servicios/mant_serv_botones_modificar2.jsp?origen="+dato_origen +"&pk_destino="+destino;

}
</script>
<BODY class="mainFondo">
	<%
		Especialidades esp= new Especialidades();
		Vector vec = new Vector();
		String str_codesp = request.getParameter("codesp");
		vec = esp.busquedaEsp("trim(SESESP_COD)=trim('"+str_codesp+"')","");
		//System.out.println("Codigo de la Especialidad:"+str_codesp);
	    //System.out.println("Parametro:SESESP_COD='"+str_codesp+"'");
		if (vec.size()>0){
	        SesEspecialidades sesEsp=(SesEspecialidades)vec.elementAt(0);
	        beanEspecialidad.put("ESPECIALIDAD",sesEsp);
	%>
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> Editar Especialidad</td>
  </tr>
</table>
<form name="formulario" action="modificarEspecialidad.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <br>
  <tr>
    <td class= "texto" width="20%">Codigo:</td>
    <td class= "normal" width="80%"><input type="text" style="width:10%" maxlength=4 name="codserv" class="cajatexto2" value="<%=sesEsp.getSesespCod()%>" readonly></td>
  </tr>
  <tr>
    <td class= "texto">Descripci&oacute;n: </td>
    <td class= "normal"><input type="text" style="width:50%" maxlength=30 name="descserv" class="cajatexto3" value="<%=sesEsp.getSesespDesc()%>" ></td>
  </tr>
</table>
</form>
</BODY>
<form name="seleccion">
<table width="100%" class = "textoTop" border = "0">
  <tr>
    <td bgcolor=#5C5CAD>Servicios Asociados </td>
  </tr>
</table>
<br>
<table class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="1" width="100%">
        <tr class="filaCabecera">
            <td width="25%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Codigo</a>
             </td>
            <td width="75%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Descripcion</a>
             </td>
</table>
     <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral" >
       <table class="centroTabla" width="100%" id="tablaUsu" cellspacing="0" cellpadding="0" border="0">
       <%
         boolean bFilaPar=true;
         int idCounter=0;
         String s;
         Servicios serv = new Servicios();
         Vector v = new Vector();
         v = serv.busquedaServ("trim(SESESP_COD)=trim('"+str_codesp+"')","");
         if (v!=null){
                 for(int int_pos = 0;int_pos < v.size(); int_pos++){
                         SesServicios sesServ=(SesServicios)v.elementAt(int_pos);
                         String codserv = sesServ.getSessrvCod();
                         if (bFilaPar){
                                          bFilaPar=false;
 %>
                                 <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaUsu'].rows, this.id);selec('<%=codserv%>',2,'<%=str_codesp%>');" style="cursor:hand;">
 <%				}else{
                                         bFilaPar=true;
 %>
                                 <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaUsu'].rows, this.id);selec('<%=codserv%>',2,'<%=str_codesp%>');" style="cursor:hand;">
 <%
                         }
 %>
                          <td width="24%" height="12px"><%=sesServ.getSessrvCod()%></td>
                          <td width="75%" height="12px"><%=sesServ.getSessrvDesc()%></td>
                    </tr>
<%
                           idCounter++;
                   }
         }
  }else {
  	System.out.println("No se encuentra la especialidad.");
  }
%>
        </table>
        </td>
      </tr>
    </table>

</form>

<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
