<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
   <link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
   <jsp:useBean id="beanBusquedaDiagnostico" class="java.util.HashMap" scope="session"/>

<script language = "javascript">
  function hacer_submit() {
     var cadena_texto;
     cadena_texto = this.document.formulario.codigo.value.toUpperCase();
     this.document.formulario.codigo.value = cadena_texto;
     cadena_texto = this.document.formulario.descrip.value.toUpperCase();
     this.document.formulario.descrip.value = cadena_texto;
     this.document.formulario.submit();
   }
</script>
</head>

<BODY class="mainFondo">

<TABLE width="100%" class="textoTop" border = "0">
  <tr>
  	 <td bgcolor=#5C5CAD> Buscador Diagn&oacute;sticos</td>
  </tr>
</table>
<form name="formulario" action="mant_diag_seleccion_datos.jsp"  target = "datos">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
          <td>&nbsp</td>
          <td>&nbsp</td>
  </tr>
  <tr>
    <td class= "texto" width="10%" >C�digo: </td>
    <td class= "normal"width="22%"><input type="text" name="codigo" class="cajatexto2" maxlength = 10 style="width:65%" value="" onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
    <td class= "texto" width="13%">Descripci�n: </td>
    <td class= "normal" width="55%"><input type="text" name="descrip" class="cajatexto3" maxlength = 80 style="width:100%" value="" onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
  </tr>

  <tr>
          <td>&nbsp</td>
          <td>&nbsp</td>
  </tr>

</table>
</form>
<%
    String s_codigo = (String)beanBusquedaDiagnostico.get("CODIGO_DIAGNOSTICO");

    if (s_codigo != null) {
%>
        <script language="javascript">
           parent.parent.botonera.location = "mant_diag_botones_buscador.jsp";
        </script>
<%
  }
%>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
