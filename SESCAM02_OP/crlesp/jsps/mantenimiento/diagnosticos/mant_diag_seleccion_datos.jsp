<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Diagnosticos,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(pk){
  parent.parent.centro.location='mant_diag_modificar_datos_diagnostico.jsp?diagpk='+pk;
  parent.parent.botonera.location.href = "mant_diag_botones_modificar.jsp"
}

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
  <%
    String s_cod_diag = "";
    String s_desc_diag = "";
    boolean b_nobuscar = false;
    boolean b_buscarcodigo = true;
    boolean b_buscardesc = true;

    if (request.getParameter("codigo") != null) {
      s_cod_diag = new String(request.getParameter("codigo").toUpperCase());
    }
    if (request.getParameter("descrip") != null) {
      s_desc_diag = new String(request.getParameter("descrip").toUpperCase());
    }

    if ((s_cod_diag.equals("")) && (s_desc_diag.equals(""))) {
      b_nobuscar = true;
    }
    if (s_cod_diag.equals("")) {
      b_buscarcodigo = false;
    }
    if (s_desc_diag.equals("")) {
        b_buscardesc = false;
    }
  %>

<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaDiag" cellspacing="0" cellpadding="0" border="0">
        <%
        if (b_nobuscar == false) {
           %>
            <script language="javascript">
              parent.pie.document.pieEtado.lEstado.value="Generando resultado...";
            </script>
          <%
          boolean bFilaPar=true;
          int idCounter=0;
          String s_where = "";
          Diagnosticos diag = new Diagnosticos();
          Vector v = new Vector();
          boolean b_pasa = false;
          if (b_buscarcodigo == true) {
            s_where = "(upper(diag_cod) like '" + s_cod_diag + "%')";
            b_pasa = true;
          }
          if (b_buscardesc == true) {
            if (b_pasa == false) {
              s_where = "(upper(diag_desc) like  '%" + s_desc_diag + "%')";
            } else {
              s_where = s_where + " AND (upper(diag_desc) like  '%" + s_desc_diag + "%')";
            }
          }
   //          System.out.println(s_where);

          v = diag.busquedaDiag(s_where, "DIAG_COD ASC");
          if (v.size()>0){

            for(int int_pos = 0;int_pos < v.size(); int_pos++){
              SesDiagnosticos sesdiag=(SesDiagnosticos)v.elementAt(int_pos);
              long diagpk = sesdiag.getDiagPk();
              if (bFilaPar){
                bFilaPar=false;
	%>
                <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaDiag'].rows, this.id);selec(<%=diagpk%>)" style="cursor:hand;">
	<%
              }else{
                bFilaPar=true;

	%>
                <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaDiag'].rows, this.id);selec(<%=diagpk%>)" style="cursor:hand;">
	<%
              }
	%>

              <td width="30%" height="12px"><%=sesdiag.getDiagCod()%></td>
              <td width="70%" height="12px"><%=sesdiag.getDiagDesc()%></td>

              </tr>
<%
              idCounter++;
           }
           %>
           <script language="javascript">
              parent.pie.document.pieEtado.lEstado.value="Listo";
              parent.pie.document.pieEtado.lFilas.value=<%=v.size()%>;
           </script>
           <%
         }else {
             %>
             <Script language="javaScript">
               parent.pie.document.pieEtado.lEstado.value="Ingrese condiciones de busqueda";
               parent.pie.document.pieEtado.lFilas.value="0";
             </Script>
             <%
           }
     }
%>
     </table>
    </td>
   </tr>
   </table>
  </form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

