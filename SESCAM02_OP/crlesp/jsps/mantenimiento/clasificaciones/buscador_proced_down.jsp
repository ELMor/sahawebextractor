<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanBusquedaProcedimiento" class="java.util.HashMap" scope="session"/>

<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">

  function buscar(){
            parent.cabecera.buscador.document.formulario.submit();
        }

  function cancelar(){
            top.close();
        }

  function creaInputDinamicos() {
      var linea='';
      var campo1 = '',campo = '',campo2 = '',campo3 = '',campo4 = '';
      var desc = '',codigo = '';
      var int_pos = 0;
      var proc_pk = 0;
      var contfilas = 0;
      var modfilas = 0;
      var modificadas = 0;
      modfilas = parent.cabecera.datos.document.seleccion.filas_modificadas.value;
      parseInt(contfilas);
      parseInt(modfilas);
      parseInt(modificadas);
      parseInt(int_pos);

      contfilas=parent.cabecera.datos.document.seleccion.filas.value;
      parseInt(contfilas);
      parseInt(int_pos);
      for (int_pos=0;int_pos < contfilas;int_pos++) {
        if (modificadas < modfilas) {
          campo = 'marca_'+int_pos;
          campo1 ='proc_'+int_pos;
          campo2 ='desc_'+int_pos;
          campo3 = 'tipo_'+int_pos;
          campo4 = 'cod_'+int_pos;


          marca = parent.cabecera.datos.document.all(campo).checked

          if (marca == true) {
            proc_pk = parent.cabecera.datos.document.all(campo1).value
            desc = parent.cabecera.datos.document.all(campo2).value
            tipo = parent.cabecera.datos.document.all(campo3).value
            codigo = parent.cabecera.datos.document.all(campo4).value
            linea=linea + '<input type=\'hidden\' name=\'proc_'+int_pos+'\' value=\''+proc_pk+'\'>';
            linea=linea + '<input type=\'hidden\' name=\'desc_'+int_pos+'\' value=\''+desc+'\'>';
            linea=linea + '<input type=\'hidden\' name=\'tipo_'+int_pos+'\' value=\''+tipo+'\'>';
            linea=linea + '<input type=\'hidden\' name=\'cod_'+int_pos+'\' value=\''+codigo+'\'>';
          }
        }else{
           int_pos = contfilas;
         }
      }
      linea=linea + '<input type=\'hidden\' name=\'filas\' value=\''+contfilas+'\'>';
      linea=linea + '<input type=\'hidden\' name=\'filas_modificadas\' value=\''+modfilas+'\'>';
      parent.cabecera.datos.document.all['datos'].insertAdjacentHTML ('BeforeEnd',linea);
  }

  function AnnadirProced(){
    var filas = parent.cabecera.datos.document.seleccion.filas.value;
    var hay_seleccion=false;
    for (i=0;i<filas;i++){
      if (parent.cabecera.datos.document.all('marca_'+i).checked==true) hay_seleccion=true;
    }
    if (hay_seleccion){
      parent.cabecera.pie.location  = "pie_arbol.htm";
      resp = confirm("Esta seguro que desea asociar los procedimientos seleccionados");
      if (resp == true ) {
      //  parent.cabecera.pie.ActivarReloj();
        if (parent.cabecera.datos.document.seleccion.marcar_todas.value == "checked" ) {
           parent.cabecera.datos.document.seleccion.filas_modificadas.value =  parent.cabecera.datos.document.seleccion.filas.value
        }
        creaInputDinamicos();
        parent.cabecera.datos.document.guardar.submit();
      //  parent.cabecera.pie.ActivarReloj();
      }
    }else{
      parent.cabecera.pie.location  = "buscador_proced_pie.htm";
      alert("No hay ningun procedimiento seleccionado.Seleccione alguno para asociar a la Clasificacion.");
    }
  }

</script>

</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
  String s_codigo = (String)beanBusquedaProcedimiento.get("CODIGO_PROCEDIMIENTO");
   if (s_codigo != null) {
     beanBusquedaProcedimiento.clear();
   }
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="15%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="buscar();">Buscar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="30%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="AnnadirProced();">Asociar Procedimiento</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="15%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar();">Cancelar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
