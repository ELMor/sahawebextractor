<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanPadre" class="java.util.HashMap" scope="session"/>
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

   <%
   int i,i_filas,i_filas_modificadas,i_modificadas = 0;
   long pk_padre = -1;
   long pk_proc = 0;
   long tipo = 0;
   String campo_proc;
   String campo_desc;
   String campo_tipo;
   String campo_cod;
   String Nodo = new String("");
   String desc = "";
   String codigo = "";
   long l_activo = 0;
   SesProcTipos sesproctipos= new SesProcTipos();

   i_filas = new Integer(request.getParameter("filas")).intValue();
   i_filas_modificadas = new Integer(request.getParameter("filas_modificadas")).intValue();

   if (beanPadre.size() > 0) {
     try {
       Long padre = (Long) beanPadre.get("PADRE");
       pk_padre = padre.longValue();
     }catch(Exception e) {
     }
   }else {
   %>
     <script language = "javascript">
       parent.parent.DevolverParametro(<%=""%>);
     </script>
   <%
     return;
   }
   Vector v = new Vector();
   NodoArbol np = new NodoArbol();
   try{

     np.setId(pk_padre);
     for (i=0;i<i_filas;i++) {
       if (i_modificadas < i_filas_modificadas) {
         campo_proc = "proc_"+i;
         campo_desc = "desc_"+i;
         campo_tipo = "tipo_"+i;
         campo_cod = "cod_"+i;
         if (request.getParameter(campo_proc) != null) {
           pk_proc = new Long(request.getParameter(campo_proc)).longValue();
           tipo = new Long(request.getParameter(campo_tipo)).longValue();
           desc = request.getParameter(campo_desc);
           codigo = request.getParameter(campo_cod);
           i_modificadas = i_modificadas + 1;
           ProcTipos proctipos = new ProcTipos();

           v = proctipos.busquedaProcTipo("PROC_TIPO ="+tipo,"");
           sesproctipos = (SesProcTipos) v.elementAt(0);
           String codtipo = sesproctipos.getProcTcod();
           Clasificaciones clasif = new Clasificaciones();
           clasif.SetNegEvento(accesoBeanId.getNegEventoInstance(35));
           NodoArbol hijo = new NodoArbol();
           hijo.setIEstado(1);
           hijo.setNType(0);
           hijo.setId(-1);
           hijo.setDbType(3);
           hijo.setDesc(codtipo+" "+codigo+" "+desc);
           hijo.setProc_pk(pk_proc);
           np.addChild(hijo);
         }
       }else {
         break;
       }
    }

    if (i_filas_modificadas > 0) {
      np.saveChilds();
      Nodo = np.childsToString();
    }else {
      Nodo = null;
    }
   }catch ( ExceptionSESCAM es ){
     Nodo = null;
    %>
      <script language="javascript">
        alert("<%=es%>");
      </script>
    <%
    }
   %>
   <script language = "javascript">
     parent.parent.DevolverParametro(<%=Nodo%>);
   </script>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>