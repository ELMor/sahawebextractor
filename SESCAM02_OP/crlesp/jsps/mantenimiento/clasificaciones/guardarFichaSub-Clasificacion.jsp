<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.arboles.Nodo,isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
               String descripcion = request.getParameter("descclas");
               long padre_pk = new Long(request.getParameter("padre_pk")).longValue();
               Nodo nodoPadre = new NodoArbol();
               Nodo nodoHijo  = new NodoArbol();
               nodoPadre.setId(padre_pk);
               nodoPadre.ParentsNumber();
               nodoPadre.LevelsCount();
               long num_padres = nodoPadre.getParentN();
               long num_levels = nodoPadre.getLevelsN();
               if ((num_levels - (2 + num_padres))>0){
                 nodoHijo.setDbType(2);
               }else{
                 nodoHijo.setDbType(4);
               }
               nodoHijo.setIEstado(1);// Estado Nodo Insertado
               nodoHijo.setNType(1);
               nodoHijo.setId(-1);
               nodoHijo.setDesc(descripcion);
               nodoPadre.addChild(nodoHijo);
               nodoPadre.saveChilds();

               String retorno = nodoPadre.childsToString();
%>
                <script language="javascript">
                    parent.return_nodo(<%=retorno%>)
                </script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>