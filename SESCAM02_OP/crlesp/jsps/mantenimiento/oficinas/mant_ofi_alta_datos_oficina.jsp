<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>

<style>
input {background-color: transparent}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
</head>
<script language="javascript">

function recargar(){
  this.document.formulario.action="mant_ofi_alta_datos_oficina.jsp";
  this.document.formulario.target="centro";
  this.document.formulario.submit();
  this.document.formulario.action="guardarOficina.jsp";
  this.document.formulario. target="oculto1";

}
</script>

<%
  String nombre;
  if  ((request.getParameter("nombre")!=null) && (!request.getParameter("nombre").equals(""))){
       nombre = request.getParameter("nombre");
       }else{
       nombre = "";
  }
  String dir;
  if  ((request.getParameter("direccion")!=null) && (!request.getParameter("direccion").equals(""))){
     dir = request.getParameter("direccion");
     }else{
     dir = "";
  }
  long telef;
  if  ((request.getParameter("telefono")!=null) && (!request.getParameter("telefono").equals(""))){
     telef = new Long(request.getParameter("telefono")).longValue();
     }else{
     telef = 0;
  }
  long cp;
  //System.out.println("CP:"+request.getParameter("codigopostal")+".");
  if  ((request.getParameter("codigopostal")!=null) && (!request.getParameter("codigopostal").equals(""))&& (!request.getParameter("codigopostal").equals("null"))){
     cp = new Long(request.getParameter("codigopostal")).longValue();
   }else{
   cp = 0;
  }
  long pk_prov_sel;
  if  ((request.getParameter("provincias")!=null) && (!request.getParameter("provincias").equals(""))){
       pk_prov_sel = new Long(request.getParameter("provincias")).longValue();
       }else{
       pk_prov_sel = 0;
  }
%>

<BODY class="mainFondo">
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> A�adir Oficina Nueva</td>
  </tr>
</table>
<form name="formulario" action="guardarOficina.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
	 <td></td>
  </tr>
  <tr>
    <td width="15%" class= "texto">Oficina: </td>
    <%
    if (nombre==""){
    %>
        <td width="30%" class= "normal"><input type="text" name="nombre" class="cajatexto2" maxlength = 20 value="" ></td>
    <%
    }else{
    %>
        <td width="30%" class= "normal"><input type="text" name="nombre" class="cajatexto2" maxlength = 20 value="<%=nombre%>"></td>
    <%
    }
    %>
    <td width="15%"></td>
    <td width="35%"></td>
  </tr>
  <tr>
  </tr>
  <tr>
    <td class= "texto">Direcci&oacute;n: </td>
<%
    if (dir==""){
%>
    <td class= "normal" colspan=3><input type="text" name="direccion" class="cajatexto3" maxlength = 30 value="" ></td>
<%
    }else{
%>
    <td class= "normal" colspan=3><input type="text" name="direccion" class="cajatexto3" maxlength = 30 value="<%=dir%>" ></td>
<%
    }
%>
  </tr>
  <tr>
    <td class= "texto">Telefono: </td>
<%
    if (telef==0){
%>
    <td class= "normal"><input type="text" name="telefono" class="cajatexto4" value="" maxlength=9 onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57))) event.returnValue = false;"></td>
<%
    }else{
%>
    <td class= "normal"><input type="text" name="telefono" class="cajatexto4" value="<%=telef%>" maxlength=9 onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57))) event.returnValue = false;"></td>
<%
    }
%>

    <td class= "texto">Codigo Postal: </td>
<%
    if (cp==0){
%>
    <td class= "normal"><input type="text" name="codigopostal" class="cajatexto4" value="" maxlength=5 onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57))) event.returnValue = false;"></td>
<%
    }else{
%>
    <td class= "normal"><input type="text" name="codigopostal" class="cajatexto4" value="<%=cp%>" maxlength=5 onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57))) event.returnValue = false;"></td>
<%
    }
%>

  </tr>
  <tr>
    <td class= "texto">Provincia: </td>
    <td class= "normal"><div id="divProvincias">
           <Select name="provincias" class="cajatexto2" onChange="recargar();">
           <%
           Provincias prov = new Provincias();
           Vector vp = new Vector();
           vp = prov.busquedaProv("","PROVINCIA ASC");
           for(int int_pos = 0;int_pos < vp.size(); int_pos++){
                SesProvincias sesProv=(SesProvincias)vp.elementAt(int_pos);
                if ((pk_prov_sel>0) && (pk_prov_sel==sesProv.getProvinciaPk())){
            %>
                   <option selected value='<%= sesProv.getProvinciaPk()%>'><%=sesProv.getProvincia() %></option>
            <%
                }else{
           %>
                   <option value='<%= sesProv.getProvinciaPk()%>'><%=sesProv.getProvincia() %></option>
            <%
                }
           }
            %>
           </Select></div>
    </td>
    <td class= "texto">Localidad: </td>
    <td class= "normal"><div id="divLocalidades">
           <Select name="localidades" class="cajatexto2">
           <%
           Localidades loc = new Localidades();
           Vector vl = new Vector();
           if (pk_prov_sel>0) {
             vl = loc.busquedaLoc("PROVINCIA_PK="+pk_prov_sel,"LOCALIDAD ASC");
           }else{
             vl = loc.busquedaLoc("","LOCALIDAD ASC");
           }
           for(int int_pos = 0;int_pos < vl.size(); int_pos++){
                SesLocalidades sesLoc=(SesLocalidades)vl.elementAt(int_pos);
           %>
                   <option default value='<%= sesLoc.getPkLocalidad()%>'><%=sesLoc.getLocalidad() %></option>
            <%
            }
            %>
           </Select></div>
    </td>
  </tr>
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
