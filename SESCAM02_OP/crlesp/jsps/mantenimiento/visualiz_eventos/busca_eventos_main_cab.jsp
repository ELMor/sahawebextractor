<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/validarFecha.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/AnchorPosition.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/botones.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/CalendarPopup.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/PopupWindow.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/validacion_campos_fecha.js"></script>
<script language="javascript">

 function chequeoFechaInicio (){
    if ((document.buscaeven.fechaini.value!=null)&&(document.buscaeven.fechaini.value!="")){
      if (!esStringFecha(document.buscaeven.fechaini.value)){
         alert('Formato de fecha incorrecto (dd/mm/aaaa)');
         document.buscaeven.fechaini.focus();
      }else{
         var dia = getDia(document.buscaeven.fechaini.value);
         var mes = getMes(document.buscaeven.fechaini.value);
         var anno = getAnno(document.buscaeven.fechaini.value);
         if (validacion_fecha(dia,mes,anno));
         else alert ("Fecha no valida");
      }
    }
  }
 function chequeoFechaFinal (){
    if ((document.buscaeven.fechafin.value!=null)&&(document.buscaeven.fechafin.value!="")){
      if (!esStringFecha(document.buscaeven.fechafin.value)){
         alert('Formato de fecha incorrecto (dd/mm/aaaa)');
         document.buscaeven.fechafin.focus();
      }else{
         var dia = getDia(document.buscaeven.fechafin.value);
         var mes = getMes(document.buscaeven.fechafin.value);
         var anno = getAnno(document.buscaeven.fechafin.value);
         if (validacion_fecha(dia,mes,anno)) ;
         else alert ("Fecha no valida");
      }
    }
  }

var selectedInput = "";
var cal = new CalendarPopup();
cal.setMonthNames('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
cal.setDayHeaders('D','L','M','X','J','V','S');
cal.setWeekStartDay(1);
cal.setReturnFunction("selectDate");
document.write(cal.getStyles());
function getDate(input,linkname) {
                selectedInput=input;
                cal.showCalendar(linkname);
  }

function selectDate(y,m,d) {
                  if (selectedInput!="")
                  {
                          selectedInput.value=d + "/" + m + "/" + y;
                  }
  }
</script>
</head>

<body class="mainFondo" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0">
<form name="buscaeven" action="busca_eventos_main_lista_det.jsp" target="consultaeventos" height="10%" method="post">
        <table width="100%" align="center" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0">
                <tr height="12px">
                  <td width="2%">&nbsp;</td>
                  <td width="28%" class="texto">Usuario</td>
                  <td width="30%" class="texto">Oficina</td>
                  <td width="20%" class="texto">Fecha Inicial</td>
                  <td width="20%" class="texto">Fecha Final</td>
                </tr>
                <tr>
                  <td width="1%">&nbsp;</td>
                  <td class= "normal"><div id="divusuario">
                  <Select name="usuario" class="cajatexto2" style="width=90%">
                  <option value=''>TODOS</option>
<%
           Usuarios usu = new Usuarios();
           Vector vu = new Vector();
           vu = usu.busquedaUsu("","SYSLOGIN ASC");
           for(int int_pos = 0;int_pos < vu.size(); int_pos++){
                SysUsu sysusu=(SysUsu)vu.elementAt(int_pos);
%>
                   <option value=<%=sysusu.getSyslogin()%>><%=sysusu.getSyslogin()%></option>
<%
           }
%>
                   </Select></div>
                  </td>
<%
          Calendar Cal_fechaActual = Calendar.getInstance();
%>
                  <td class= "normal"><div id="divoficina">
                  <Select name="oficina" class="cajatexto2" style="width=90%">
                  <option value='0'>TODOS</option>
<%
           Oficina ofic = new Oficina();
           Vector vo = new Vector();
           vo = ofic.busquedaOfi("","OFICINA_NOMBRE ASC");
           for(int int_pos = 0;int_pos < vo.size(); int_pos++){
                SesOficina sysofic=(SesOficina)vo.elementAt(int_pos);
%>
                   <option value='<%= sysofic.getOficinaPk()%>'><%= sysofic.getOficinaNombre()%></option>
<%
           }
%>
                  </Select></div>
                  </td>

                  <td class="normal">
                  <INPUT style="width:60%" type="text" class="cajaTexto2" name="fechaini" value ="<%=Utilidades.dateToString(new java.sql.Date(Cal_fechaActual.getTime().getTime()))%>" onKeypress="validarEntradaFecha(this.value);" onBlur="chequeoFechaInicio();">
                                <A HREF="#" NAME='selector1' ID='selector1' onClick="getDate(document.buscaeven.fechaini, this.name); return false;">
                                        <IMG src="../../../imagenes/calendar.jpg" border=0 align=middle>
                                </A>
                  </td>
                  <td class="normal">
                  <INPUT style="width:60%" type="text" class="cajaTexto2" name="fechafin" value ="<%=Utilidades.dateToString(new java.sql.Date(Cal_fechaActual.getTime().getTime()))%>" onKeypress="validarEntradaFecha(this.value);" onBlur="chequeoFechaFinal();">
                                <A HREF="#" NAME='selector2' ID='selector2' onClick="getDate(document.buscaeven.fechafin, this.name); return false;">
                                        <IMG src="../../../imagenes/calendar.jpg" border=0 align=middle>
                                </A>
                  </td>
                </tr>
           </table>
           <table width="100%" align="center" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0">
                <tr height="12px">
                  <td width="2%">&nbsp;</td>
                  <td width="30%" class="texto">Perfil</td>
                  <td width="25%" class="texto">IP usuario</td>
                  <td width="40%" class="texto">Tipo Evento</td>
                  <td width="3%">&nbsp;</td>
                </tr>
                <td width="2%">&nbsp;</td>
                <td class= "normal"><div id="divrol">
                  <Select name="rol" class="cajatexto2" style="width=90%">
                  <option value='0'>TODOS</option>
<%
          Perfiles per = new Perfiles();
          Vector vp = new Vector();
          vp = per.busquedaPerfil("","SYS_ROL_NOMBRE ASC");
          for(int int_pos = 0;int_pos < vp.size(); int_pos++){
                SysRoles rol=(SysRoles)vp.elementAt(int_pos);
%>
                   <option value='<%=rol.getSysRolPk()%>'><%=rol.getSysRolNombre()%></option>
<%
           }
%>
                  </Select></div>
                  </td>


                  <td class= "normal"><input type="text" name="ip" style="width:92%" maxlenght = "15" class="cajatexto2" value="" onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false"></td>
                  <td class= "normal"><div id="diveventos">
                  <Select name="eventos" class="cajatexto2" style="width=90%">
                  <option value='0'>TODOS</option>
<%
           TipoEvento tipoevento = new TipoEvento();
           Vector vtipoev = new Vector();
           vtipoev = tipoevento.busquedaTiposEventos("","TPOEVTO_DESC ASC");
           for(int int_pos = 0;int_pos < vtipoev.size(); int_pos++){
                EventoTipo evtotipo =(EventoTipo)vtipoev.elementAt(int_pos);
%>
                   <option value='<%=evtotipo.getTpoevtoPk()%>'><%=evtotipo.getTpoevtoDesc()%></option>
<%
           }
%>
                   </Select></div>
                  </td>
               </tr>
        </table>
<td><input type="hidden" value="tpoevto_pk" name="orden"></td>
<br>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

