<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaEventosFicha" %>
<%@ page import="isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Visualización de Eventos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript">
</script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="ficha_evento">

<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
<%
  ConsultaEventosFicha cpb=new ConsultaEventosFicha();
  EventosFicha evFicha;
  boolean bFilaPar=true;
  boolean bPrimeraVez=true;
  boolean bEventoTram=false;
  int idCounter=0;
  Vector vp;
  String evto_pk = request.getParameter("evto_pk").trim();
  if (evto_pk.equals("0") ) {
    cpb.setEvto_pk(0);
  }else {
    cpb.setEvto_pk(Integer.parseInt(evto_pk));
  }
  cpb.setSess_pk(Integer.parseInt(request.getParameter("sess_pk")));
  vp=cpb.resultado();
  if (vp!=null){
    Enumeration e = vp.elements();
    while (e.hasMoreElements()) {
	evFicha=(EventosFicha)e.nextElement();
        if (bPrimeraVez){
          if ((evFicha.fTpoevtoO_pk>=14)&&(evFicha.fTpoevtoO_pk<=18)) bEventoTram=true;
%>
          <!-- Ini Encabezado de ficha de evento -->

            <tr height="10%" width="100%">
             <td>
               <table width="100%">
                 <tr width="100%">
                   <td class="texto">
                     Fecha:
                     <input class="cajaTexto2" style="width:150px" type="text" readonly value="<%=evFicha.fSess_fecha_inicio%>">
                   </td>
                   <td class="texto">
                     Evento:
                     <input class="cajaTexto2" style="width:300px" type="text" readonly value="<%=evFicha.fTpoevto_desc%>">
                   </td>
                 </tr>
                 <tr width="100%">
                   <td class="texto">
                     Login:
                     <input class="cajaTexto2" style="width:150px" type="text" readonly value="<%=evFicha.fsyslogin%>">
                   </td>
                   <td class="texto">
                     Oficina:
                     <input class="cajaTexto2" style="width:300px" type="text" readonly value="<%=evFicha.fOficina_nombre%>">
                   </td>
                 </tr>
          <%
            if (evFicha.fTpoevtoO_pk==19) {
          %>
                 <tr width="100%">
                   <td class="texto">
                     Dir.IP:
                     <input class="cajaTexto2" style="width:150px" type="text" readonly value="<%=evFicha.fSess_srv_rmt%>">
                   </td>
                   <td class="texto">
                     Acceso:
                     <input class="cajaTexto2" style="width:300px" type="text" readonly value="<%=evFicha.fSess_cod_acc%>">
                   </td>
                 </tr>
            <%
            }
            %>
              </table>
             </td>
            </tr>
            <!-- Fin Encabezado de ficha de evento -->
        <%
          bPrimeraVez=false;
        }
	if (idCounter==0){
        %>
                 <tr height="1%" width="100%">
                   <td>
                     <!-- CABECERA INICIO-->
                     <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                         <td class="areaCentral">
                     	  <table style="width:100%" align="left" border="0" cellspacing="0" cellpadding="0">
                     	   <tr>
                     	    <td class="areaCentral2" >
                     		<table class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="0">
                     		   <tr class="filaCabecera" height="12px">
                     		    <td width="50%">Variable</td>
                     		    <td width="50%">Valor</td>
                     		   </tr>
                     		</table>
                     	    </td>
                     	   </tr>
                     	   </table>
                        </td>
                       </tr>
                     </table>
                     <!-- CABECERA FIN-->
                    <td>
                 </tr>
              <!-- Ini Variableso de ficha de evento -->
             <tr height="1%" width="100%">
              <td>
               <table height="100%" width="100%">
        <%
        }
	if (bFilaPar){
          bFilaPar=false;
 %>
         <tr class="filaPar">
 <%	 }else{
         bFilaPar=true;
 %>
         <tr class="filaImpar">
 <%
	 }
 %>
	   <td width="50%"><%=evFicha.fTpoevto_var_desc%></td>
	   <td width="50%"><%=evFicha.fEvto_var_valor%></td>
      </tr>
<%
			   idCounter++;
	}
%>

                </table>
               </td>
              </tr>
              <!-- Fin Variableso de ficha de evento -->
<%
    }
    if (bEventoTram){
%>
<!-- Ini ficha de eventos Tramos -->
  <tr height="34%" width="100%">
   <td >
     <iframe name="Ficha_evto" align="center" src="ficha_evto_solicitud.jsp?evto_pk=<%=request.getParameter("evto_pk")%>" frameborder="no" height="100%" width="100%"> </iframe>
   </td>
  </tr>
<!-- Fin ficha de eventos Tramos -->
<%
    }else{
%>
  <tr height="34%" width="100%">
   <td >&nbsp;</td>
  </tr>
<%
    }
%>
</table>

</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
