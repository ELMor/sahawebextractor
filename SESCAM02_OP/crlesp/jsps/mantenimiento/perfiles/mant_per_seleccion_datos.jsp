<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Perfiles,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanPerfiles" class="java.util.HashMap" scope="session"/>
<jsp:useBean id="pkPerfiles" class="java.util.HashMap" scope="session"/>

<style>
input {background-color: transparent}
</style>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(pk){
  parent.parent.centro.location ='mant_per_main_modificar.jsp?perpk='+pk;
  parent.parent.botonera.location.href = "mant_per_botones_modificar_deshabilitada.jsp"

}

</script>

</head>
<body class="mainFondo">
<%
  beanPerfiles.clear();
  pkPerfiles.clear();
%>
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
		<%
		boolean bFilaPar=true;
		int idCounter=0;
		Perfiles per = new Perfiles();
		Vector v = new Vector();
		v = per.busquedaPerfil("","SYS_ROL_NOMBRE ASC");
		if (v!=null){
			for(int int_pos = 0;int_pos < v.size(); int_pos++){
				SysRoles sysRoles=(SysRoles)v.elementAt(int_pos);
				long rolpk = sysRoles.getSysRolPk();
				if (bFilaPar){
	      			   	bFilaPar=false;
	%>
					<tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec(<%=rolpk%>)" style="cursor:hand;">
	<%				}else{
						bFilaPar=true;
	%>
						<tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec(<%=rolpk%>)" style="cursor:hand;">
	<%
				}
	%>
				<td width="30%" height="12px"><%=sysRoles.getSysRolNombre()%></td>
	   			<td width="37%" height="12px"><%=sysRoles.getSysRolDesc()%></td></td>
			</tr>
<%
  				idCounter++;
  			}
		}
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</layer>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>