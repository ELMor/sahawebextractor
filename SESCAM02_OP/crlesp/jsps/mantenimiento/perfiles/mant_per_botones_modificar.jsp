<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<jsp:useBean id="beanPerfiles" class="java.util.HashMap" scope="session"/>
<jsp:useBean id="pkPerfiles" class="java.util.HashMap" scope="session"/>
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">

<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">

      function selTodosDestino(){
	for (var i=0; i<parent.centro.datos_tab.detalle_dat.document.formular.listaPermisosSelec.length; i=i+1){
		parent.centro.datos_tab.detalle_dat.document.formular.listaPermisosSelec.options[i].selected = true;
	}
      }

      function guardar(pk) {
        var dato_perfil_nombre,dato_perfil_descrip,dato_pk_destino
      		resp = confirm("Desea guardar los datos?")
      		if (resp == true){
           		selTodosDestino();
                        dato_perfil_nombre = parent.centro.perfil.document.formulario.nombre.value;
                        dato_perfil_descrip = parent.centro.perfil.document.formulario.descripcion.value;
                        parent.centro.datos_tab.detalle_dat.document.formular.nombre.value = dato_perfil_nombre;
                        parent.centro.datos_tab.detalle_dat.document.formular.descripcion.value = dato_perfil_descrip;
                        parent.centro.datos_tab.detalle_dat.document.formular.pk_destino.value = pk;
           		parent.centro.datos_tab.detalle_dat.document.formular.submit();
            	}else{
                   if ((pk != null) && (pk > 0)) {
                     parent.centro.location.href = '../permisos/mant_perm_modificar_datos_permiso.jsp?permpk='+pk;
                     parent.botonera.location.href = "../permisos/mant_perm_botones_modificar.jsp";
                   }else {
      			parent.document.location.href = "mant_per_index.htm";
                   }
      		}
      }

      function borrar(pk) {
        var dato_perfil_nombre,dato_perfil_descrip;
      	       resp = confirm("Desea eliminar el permiso?")
      		if (resp == true){
                        dato_perfil_nombre = parent.centro.perfil.document.formulario.nombre.value;
                        dato_perfil_descrip = parent.centro.perfil.document.formulario.descripcion.value;
                        parent.centro.datos_tab.detalle_dat.document.formular.nombre.value = dato_perfil_nombre;
                        parent.centro.datos_tab.detalle_dat.document.formular.descripcion.value = dato_perfil_descrip;

           		parent.centro.datos_tab.detalle_dat.document.formular.action="eliminarPerfil.jsp";
                        parent.centro.datos_tab.detalle_dat.document.formular.pk_destino.value = pk;
           		parent.centro.datos_tab.detalle_dat.document.formular.submit();
           		parent.centro.datos_tab.detalle_dat.document.formular.action="modificarPerfil.jsp";
             	}else{
                       if ((pk != null) && (pk > 0)) {
                          parent.centro.location.href = '../permisos/mant_perm_modificar_datos_permiso.jsp?permpk='+pk;
                          parent.botonera.location.href = "../permisos/mant_perm_botones_modificar.jsp";
                       }else {
                          parent.document.location.href = "mant_per_index.htm";
                       }
                 }
      }

      function cancelar(pk) {
      		alert("No se guardar�n los cambios.");
                if ((pk != null) && (pk > 0)) {
                  parent.centro.location.href = '../permisos/mant_perm_modificar_datos_permiso.jsp?permpk='+pk;
                  parent.botonera.location.href = "../permisos/mant_perm_botones_modificar.jsp";
                }else {
                  parent.document.location.href = "mant_per_index.htm";
                }
      }

</script>


</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

<%

  long l_reqpk;

  if ((request.getParameter("pk_destino") != null) && (!request.getParameter("pk_destino").equals(""))) {
      l_reqpk = new Long(request.getParameter("pk_destino")).longValue();
  }
  else {
      l_reqpk = 0;
  }
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="guardar(<%=l_reqpk%>);">Guardar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="borrar(<%=l_reqpk%>);">Eliminar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar(<%=l_reqpk%>);">Cancelar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
