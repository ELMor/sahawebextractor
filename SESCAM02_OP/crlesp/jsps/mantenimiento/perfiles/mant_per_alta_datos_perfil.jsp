<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
   <link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
   <jsp:useBean id="beanPerfiles" class="java.util.HashMap" scope="session"/>
<script language="javascript" type="text/javascript" src="../../../javascript/selects.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/FormUtil.js"></script>
</head>

<BODY class="mainFondo">
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> A�adir Perfil</td>
  </tr>
</table>
<form name="formulario" action="guardarPerfil.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
	 <td>&nbsp</td>
	 <td>&nbsp</td>
  </tr>
  <tr>
    <td class= "texto">Nombre Perfil: </td>
    <td class= "normal"><input type="text" name="nombre" class="cajatexto2" maxlength = 30 value="" ></td>
  </tr>
  <tr>
	 <td>&nbsp</td>
	 <td>&nbsp</td>
  </tr>
  <tr>
    <td class= "texto">Descripcion: </td>
    <td class= "normal"><input type="text" name="descripcion" class="cajatexto3" maxlength = 255 value="" ></td>
  </tr>
</table>
<br><br>

<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> Permisos del Perfil</th>
  </tr>
</table>
<TABLE width="90%" border="0" cellspacing="0" cellpadding="0" vspace="0">
	<tr>
 		<td class="texto" width="40%" rowspan="4"> Permisos disponibles:</td>
 		<td width="20%">&nbsp;</td>
 		<td class="texto" width="40%" rowspan="4"> Permisos asociados al perfil:</td>
	</tr>
</TABLE>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr>
       <td class="desplegable" rowspan="4" width="40%">
	  <select name="listaPermisos" class="cajaTextoMultiple"  multiple size="8">
		    <%
		    Permisos perm= new Permisos();
		    Vector vperm = new Vector();
		    vperm = perm.busquedaPerm("","SYS_PERM_NOMBRE ASC");
		    for (int int_i = 0;int_i<vperm.size();int_i++){
		    	SysPermisos sysPerm = (SysPermisos)vperm.elementAt(int_i);

		    %>
		    <option value="<%=sysPerm.getSysPermPk()%>"><%=sysPerm.getSysPermNombre()%></option>
		    <%
		    }
		    %>
	  </select>
	</td>
        <td align="center" width="20%"><a href="javascript:selecciona(document.formulario.listaPermisos, document.formulario.listaPermisosSelec);"><img src="../../../imagenes/flecha.gif" width="20" height="13" border="0" alt="A�adir"></a></td>
        <td class="desplegable" width="40%" rowspan="4">
	    <select name="listaPermisosSelec" class="cajaTextoMultiple" multiple size="8">
	    </select>
        </td>
</tr>
<tr>
    <td align="center"><a href="javascript:deselecciona(document.formulario.listaPermisosSelec);"><img src="../../../imagenes/flechamenos.gif" width="20" height="13" border="0" alt="Eliminar"></a></td>
</tr>
<tr>
    <td align="center"><a href="javascript:seleccionaTodos(document.formulario.listaPermisos, document.formulario.listaPermisosSelec);"><img src="../../../imagenes/flecha1.gif" width="20" height="13" border="0" alt="A�adir Todas"></a></td>
</tr>
<tr>
    <td align="center"><a href="javascript:deseleccionaTodos(document.formulario.listaPermisosSelec);"><img src="../../../imagenes/flechamenos1.gif" width="20" height="13" border="0" alt="Eliminar Todas"></a></td>
</tr>
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
























