<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
   <link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
   <jsp:useBean id="beanPerfiles" class="java.util.HashMap" scope="session"/>
   <jsp:useBean id="pkPerfiles" class="java.util.HashMap" scope="session"/>
<script language="javascript" type="text/javascript" src="../../../javascript/selects.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/FormUtil.js"></script>
</head>

<BODY class="mainFondoInterno">
<%
                long l_reqpk = 0;
                long l_pk_destino = 0;
                SysRoles sysRoles = new SysRoles();

	        if (pkPerfiles.size() > 0) {
                  Perfiles per= new Perfiles();
                  Vector vec = new Vector();
                  Long pk = (Long)pkPerfiles.get("PK");
                  l_reqpk = pk.longValue();
                  if (pkPerfiles.size() > 1) {
                   Long destino = (Long)pkPerfiles.get("PK_DESTINO");
                   l_pk_destino = destino.longValue();
                  }
                  vec = per.busquedaPerfil("SYS_ROL_PK="+l_reqpk,"");
                  sysRoles=(SysRoles)vec.elementAt(0);
                  beanPerfiles.clear();
                  beanPerfiles.put("PERFILES",sysRoles);
         //         System.out.println(beanPerfiles);
                }

%>

<TABLE width="100%" class="textoTop">
  <tr>
  <td bgcolor=#5C5CAD> Permisos del Perfil</th>
  </tr>
</table>

<form name="formular" action="modificarPerfil.jsp" target="oculto1">
<TABLE width="60%" border="0" cellspacing="0" cellpadding="1" vspace="0">
        <tr>
                 <td class="texto" width="40%" rowspan="4"> Permisos disponibles:</td>
                 <td width="20%">&nbsp;</td>
                 <td class="texto" width="40%" rowspan="4"> Permisos asociados al perfil:</td>
        </tr>
</TABLE>
<table width="60%" border="0" cellspacing="0" cellpadding="1">
<tr>

          <td class="desplegable" rowspan="4" width="40%">
          <select name="listaPermisos" class="cajaTextoMultiple"  multiple size="8">
                    <%
                    Permisos perm= new Permisos();
                    Vector vperm = new Vector();
                    vperm = perm.busquedaPerm("","SYS_PERM_NOMBRE ASC");
                    for (int int_i = 0;int_i<vperm.size();int_i++){
                            SysPermisos sysPerm = (SysPermisos)vperm.elementAt(int_i);

                    %>
                    <option value="<%=sysPerm.getSysPermPk()%>"><%=sysPerm.getSysPermNombre()%></option>
                    <%
                    }
                    %>
          </select>
        </td>
        <td align="center" width="20%"><a href="javascript:selecciona(document.formular.listaPermisos, document.formular.listaPermisosSelec);"><img src="../../../imagenes/flecha.gif" width="20" height="13" border="0" alt="A�adir"></a></td>
        <td class="desplegable" width="40%" rowspan="4">
            <select name="listaPermisosSelec" class="cajaTextoMultiple" multiple size="8">
                    <%
                    PermRol permRol = new PermRol();
                    Vector v = new Vector();
                    Vector vp = new Vector();
                    v = permRol.busquedaPermRol("SYS_ROL_PK="+l_reqpk,"");
                    beanPerfiles.put("PERM_ORIGEN",v);
                    for(int int_pos = 0;int_pos < v.size(); int_pos++){
                        SysPermRol sysPermRol=(SysPermRol)v.elementAt(int_pos);
                        Permisos p = new Permisos();
                        vp = p.busquedaPerm("SYS_PERM_PK="+sysPermRol.getSysPermPk(),"");
                        SysPermisos sysPermisos=(SysPermisos)vp.elementAt(0);
                    %>
                            <option value="<%=sysPermisos.getSysPermPk()%>"><%=sysPermisos.getSysPermNombre()%></option>
                    <%
                    }
                    %>
                    <script language="javascript">
                       parent.parent.parent.botonera.location.href = "mant_per_botones_modificar.jsp?pk_destino="+<%=l_pk_destino%>;
                    </script>
            </select>
        </td>
</tr>
<tr>
    <td align="center"><a href="javascript:deselecciona(document.formular.listaPermisosSelec);"><img src="../../../imagenes/flechamenos.gif" width="20" height="13" border="0" alt="Eliminar"></a></td>
</tr>
<tr>
    <td align="center"><a href="javascript:seleccionaTodos(document.formular.listaPermisos, document.formular.listaPermisosSelec);"><img src="../../../imagenes/flecha1.gif" width="20" height="13" border="0" alt="A�adir Todas"></a></td>
</tr>
<tr>
    <td align="center"><a href="javascript:deseleccionaTodos(document.formular.listaPermisosSelec);"><img src="../../../imagenes/flechamenos1.gif" width="20" height="13" border="0" alt="Eliminar Todas"></a></td>
</tr>
</table>
<td><input type="hidden" name="nombre" value=""></td>
<td><input type="hidden" name="descripcion" value=""></td>
<td><input type="hidden" name="pk_destino" value=""></td>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
























