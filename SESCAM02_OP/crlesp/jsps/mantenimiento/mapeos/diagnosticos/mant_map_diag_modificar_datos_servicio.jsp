<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave, isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../../javascript/SelecUtil.js"></script>
</head>
<jsp:useBean id="beanMapeoDiagnosticos" class="java.util.HashMap" scope="session"/>
<script language="javascript">
  function abrir_buscador(){
    var retorno=0;
    llamado="buscador_diag_index.htm";
    retorno=window.showModalDialog(llamado,"","dialogWidth:45em;dialogHeight:35em;status=no");
    document.form.diagpk.value=retorno;
    document.formulario.diagpk.value=retorno;
    document.form.submit();
  }

</script>
<BODY class="mainFondo">
	<%
		MapeoDiag mapdiag= new MapeoDiag();
		Vector vec = new Vector();
		String str_coddiag = request.getParameter("coddiag");
                String str_centro = request.getParameter("centro");
                String str_coddiagsescam = "S/D";
                String str_descdiagsescam = "S/D";
		vec = mapdiag.busquedaDiagMap("DIAG_CEN_COD='"+str_coddiag+"' AND CENTRO="+str_centro,"");
                SesDiagMap sDM=(SesDiagMap)vec.elementAt(0);
                beanMapeoDiagnosticos.put("MAPEODIAGNOSTICO",sDM);
                Diagnosticos diag = new Diagnosticos();

                Vector vd = new Vector();
                vd = diag.busquedaDiag("DIAG_PK="+sDM.getDiagPk(),"");
                if (vd.size()>0){
                  SesDiagnosticos sD = (SesDiagnosticos)vd.elementAt(0);
                  str_coddiagsescam = sD.getDiagCod();
                  str_descdiagsescam = sD.getDiagDesc();
                }

	%>
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> Editar Mapeo Diagn&oacute;stico</td>
  </tr>
</table>
<form name="form" action="busquedaPkDiagnostico.jsp" target="oculto1">
<input type="hidden" name="diagpk" value="">
</form>
<form name="formulario" action="modificarMapDiagnostico.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto" width="20%">Codigo Sescam:</td>
    <td class= "normal" width="50%"><input type="text" style="width:12%" maxlength=4 name="coddiagses" class="cajatexto2" value="<%=str_coddiagsescam%>" readonly></td>
    <td class= "normal" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td class= "texto">Diagnostico Sescam: </td>
    <td class= "normal"><input type="text" name="descdiagses" class="cajatexto3" value="<%=str_descdiagsescam%>" readonly></td>
    <td class= "normal"><a href="#"><img src="../../../../imagenes/lupa_2.gif" onClick="return abrir_buscador();" alt="Buscador" border=0></a></td>
  </tr>
  <tr>
    <td class= "texto">Centro: </td>
    <td  width="65%"  class= "normal"><div id="divCentro">
      <Select name="centro" class="cajatexto2" style="width:35%" disabled >
<%
           Centros centro = new Centros();
           Vector vc = new Vector();
           vc = centro.busquedaCentro("","CODCENTRO ASC");
           for(int int_pos = 0;int_pos < vc.size(); int_pos++){
                Centrosescam cs=(Centrosescam)vc.elementAt(int_pos);
                if (cs.getCodcentro()==sDM.getCentro()){
%>
                <option  value='<%=cs.getCodcentro()%>' selected><%=cs.getDescentro()%></option>
<%
                }else{
%>
                <option  value='<%=cs.getCodcentro()%>'><%=cs.getDescentro()%></option>
<%
                }
            }
%>
           </Select></div>
    </td>
  </tr>
  <tr>
    <td class= "texto">Diagnostico Mapeado: </td>
    <td class= "normal" width="80%"><input type="text" style="width:12%" maxlength=6 name="coddiagmap" class="cajatexto2" value="<%=sDM.getDiagCenCod()%>" readonly></td>
  </tr>
  <input type="hidden" name="diagpk" value="">
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
