<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../../css/estilo.css">
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
                /* Recojo los parametros */
                long diag_pk = new Long(request.getParameter("diagpk")).longValue();
                long cod_centro = new Long(request.getParameter("centro")).longValue();
                String diag_cen_cod = request.getParameter("coddiagmap");
                MapeoDiag mapdiag = new MapeoDiag();
                SesDiagMap sDM= new SesDiagMap();

                /*Realizo los set */
                sDM.setDiagPk(diag_pk);
                sDM.setCentro(cod_centro);
                sDM.setDiagCenCod(diag_cen_cod);
                try{
                    mapdiag.SetNegEvento(accesoBeanId.getNegEventoInstance(32));
                  /*modifico el objeto*/
                    mapdiag.insertarDiagMap(sDM);
                }catch(ExceptionSESCAM eses){
                    System.out.println("Excepcion Sescam capturada en insertarMapDiagnostico.jsp");
%>
                    <script language="javascript">
                      alert("<%=eses%>");
                    </script>
<%
                }
%>

                 <script language="javascript">
                    parent.centro.location.href = "mant_map_diag_main.htm"
                    parent.botonera.location.href = "mant_map_diag_down.htm"
                </script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>