<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

   <%
   int i,i_filas,i_filas_modificadas,i_modificadas = 0;
   String campo_codigo;
   String campo_centro;
   String campo_pk;
   String coddiag = "";
   long centro=0;
   long diagpk = 0;
   i_filas = new Integer(request.getParameter("filas")).intValue();
   i_filas_modificadas = new Integer(request.getParameter("filas_modificadas")).intValue();

   Vector v = new Vector();
   for (i=0;i<i_filas;i++) {
       if (i_modificadas < i_filas_modificadas) {
         campo_codigo = "codigo_"+i;
         campo_pk = "pk_"+i;

         campo_centro = "centro";
         if (request.getParameter(campo_codigo) != null) {
           coddiag = request.getParameter(campo_codigo);
           diagpk =  new Long(request.getParameter(campo_pk)).longValue();
           centro = new Double(request.getParameter(campo_centro)).longValue();
           i_modificadas = i_modificadas + 1;
           MapeoDiag mapdiag = new MapeoDiag();
           SesDiagMap sdm = new SesDiagMap();
           /* realizo los sets */

           sdm.setDiagPk(diagpk);
           sdm.setCentro(centro);
           sdm.setDiagCenCod(coddiag);
           /*inserto el objeto */
           try{
             mapdiag.SetNegEvento(accesoBeanId.getNegEventoInstance(32));
             mapdiag.insertarDiagMap(sdm);
           }catch(ExceptionSESCAM eses){
             System.out.println("Excepcion Sescam capturada en guardarMapeoAutomaticoDiag.jsp");
%>
              <script language="javascript">
               alert("<%=eses%>");
              </script>
<%
           }
         }
       }else{
         break;
       }
  }
  %>
  <script language="javascript">
    parent.parent.document.location = "mant_map_diag_index.htm";
  </script>
<%

%>


</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>