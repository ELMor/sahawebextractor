<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.ServMapeados,isf.persistencia.*,java.util.*,isf.util.*,beans.ConsultaMapeosServ" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/menus.js"></script>
<script language="javascript">
  function seleccionadas(contador) {
    var numero_filas = 0;
    parseInt(contador);
    if (this.document.all("marca_"+contador).checked == true) {
      numero_filas = this.document.seleccion.filas_modificadas.value;
      this.document.seleccion.filas_modificadas.value = parseInt(numero_filas)+1;
    }else{
      numero_filas = this.document.seleccion.filas_modificadas.value;
      this.document.seleccion.filas_modificadas.value = parseInt(numero_filas)-1;
    }
  }
</script>
</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
  <%
    long l_cod_centro = 0;
    int idCounter=0;
    boolean b_nobuscar = false;
    String marcar = "";

    if (request.getParameter("centro") != null) {
      l_cod_centro = new Double(request.getParameter("centro")).longValue();
    }

    if (request.getParameter("marcar_todas") != null) {
        marcar = request.getParameter("marcar_todas");
    }
  %>

<form name="seleccion2">
   <input type="hidden" name="marcar_todas" value = "">
   <input type="hidden" name="centro" value = "<%=l_cod_centro%>">
</form>
<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaProc" cellspacing="0" cellpadding="0" border="0">
        <%
     Vector v = new Vector();
     if (b_nobuscar == false) {
          boolean bFilaPar=true;
          ServMapeados sm = new ServMapeados();
          ConsultaMapeosServ cserv = new ConsultaMapeosServ();
          cserv.setCentro(l_cod_centro);
          v = cserv.ConsultaMapAutoServ();
          if (v.size()>0){
            for(int int_pos = 0;int_pos < v.size(); int_pos++){
              sm = (ServMapeados)v.elementAt(int_pos);
              String codigo=sm.CodServMap;
              String sescam=sm.CodServSESCAM;
              if (bFilaPar){
                bFilaPar=false;
	%>
                <tr class="filaPar" id=<%=int_pos%> style="cursor:hand;">
	<%
              }else{
                bFilaPar=true;

	%>
                <tr class="filaImpar" id=<%=int_pos%> style="cursor:hand;">
	<%
              }
	%>
              <td width="5%" height="12px"><input type="checkbox" id="marca_<%=idCounter%>" name="marca_<%=idCounter%>" <%=marcar%> onClick="seleccionadas(<%=idCounter%>);"></td>
              <td width="10%" height="12px"><%=sm.CodServSESCAM%></td>
              <td width="55%" height="12px"><%=sm.DescServSESCAM%></td>
              <td width="15%" height="12px"><%=sm.CodCentroMapeo%></td>
              <td width="20%" height="12px"><%=sm.CodServMap%></td>
              </tr>
              <td><input type="hidden" id="centro_<%=int_pos%>" name="centro_<%=idCounter%>"  value = <%=l_cod_centro%>></td>
              <td><input type="hidden" id="codigo_<%=int_pos%>" name="codigo_<%=idCounter%>"  value = "<%=codigo%>"></td>
              <td><input type="hidden" id="sescam_<%=int_pos%>" name="sescam_<%=idCounter%>"  value = "<%=sescam%>"></td>
<%
              idCounter++;
           }
         }else {
             %>
             <Script language="javaScript">
               alert("No exiten ningun Servicio a mapear automaticamente.");
               parent.parent.centro.location="mant_map_main.htm";
               parent.parent.botonera.location="mant_map_down.htm";
             </Script>
             <%
         }
     }
%>
           <script language="javascript">
              parent.pie.location="mant_map_serv_automatico_pie.jsp?estado=Listo&filas=<%=v.size()%>";
           </script>


     </table>
    </td>
   </tr>
   </table>
   <td><input type="hidden" name="filas" value = <%=idCounter%>></td>
   <td><input type="hidden" name="marcar_todas" <%=marcar%> value = "<%=marcar%>"></td>
   <td><input type="hidden" name="filas_modificadas" value ="0"></td>

  </form>
  <form name="guardar" action="GuardarMapeoAutomaticoServ.jsp" target="oculto1" method = "post">
  <div id="datos">
  </div>
  </form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

