<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Procedimientos,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
  <%
    String s_cod_proc = "";
    String s_desc_proc = "";
    boolean b_nobuscar = false;
    boolean b_buscarcodigo = true;
    boolean b_buscardesc = true;

   if (request.getParameter("codigo") != null) {
      s_cod_proc = new String(request.getParameter("codigo").toUpperCase());
    }
    if (request.getParameter("descrip") != null) {
      s_desc_proc = new String(request.getParameter("descrip").toUpperCase());
    }

    if ((s_cod_proc.equals("")) && (s_desc_proc.equals(""))) {
      b_nobuscar = true;
    }
    if (s_cod_proc.equals("")) {
      b_buscarcodigo = false;
    }
    if (s_desc_proc.equals("")) {
        b_buscardesc = false;
    }
  %>

<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaProc" cellspacing="0" cellpadding="0" border="0">
        <%
        if (b_nobuscar == false) {
           %>
            <script language="javascript">
              parent.pie.location="../../../pie_buscador.jsp?estado=Generando resultado";
            </script>
          <%
          boolean bFilaPar=true;
          int idCounter=0;
          String s_where = "";
          Procedimientos proc = new Procedimientos();
          Vector v = new Vector();
          boolean b_pasa = false;
          if (b_buscarcodigo == true) {
            s_where = "(upper(proc_cod) like '" + s_cod_proc + "%')";
            b_pasa = true;
          }
          if (b_buscardesc == true) {
            if (b_pasa == false) {
              s_where = "(upper(proc_desc) like  '" + s_desc_proc + "%')";
            } else {
              s_where = s_where + " AND (upper(proc_desc) like  '" + s_desc_proc + "%')";
            }
          }
   //          System.out.println(s_where);

          v = proc.busquedaProced(s_where,"PROC_DESC ASC");
          if (v.size()>0){
            for(int int_pos = 0;int_pos < v.size(); int_pos++){
              SesProcedimientos sesproc=(SesProcedimientos)v.elementAt(int_pos);
              long procpk = sesproc.getProcPk();
              if (bFilaPar){
                bFilaPar=false;
	%>
                <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaProc'].rows, this.id);parent.parent.save_pk(<%=procpk%>);" style="cursor:hand;">
	<%
              }else{
                bFilaPar=true;
	%>
                <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaProc'].rows, this.id);parent.parent.save_pk(<%=procpk%>);" style="cursor:hand;">
	<%
              }
	%>
              <td width="20%" height="12px"><%=sesproc.getProcCod()%></td>
              <td width="80%" height="12px"><%=sesproc.getProcDescCorta()%></td>
              </tr>
<%
              idCounter++;
           }
           %>
           <script language="javascript">
              parent.pie.location="../../../pie_buscador.jsp?estado=Listo&filas=<%=v.size()%>";
           </script>
           <%
         }else {
             %>
             <Script language="javaScript">
              parent.pie.location="../../../pie_buscador.jsp?estado=No se han encontrado filas.";
             </Script>
             <%
           }
     }
%>
     </table>
    </td>
   </tr>
   </table>
  </form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

