<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">

<script language="javascript">
  function origen(dato_origen,pk,pk_perm) {
    switch (dato_origen) {
      case 1: {
              parent.centro.location.href = "../cargos/mant_car_modificar_datos_cargos.jsp?carpk="+pk;
              parent.botonera.location.href = "../cargos/mant_car_botones_modificar.jsp";
              break;
      }
      case 2:  {
              parent.centro.location.href = "../oficinas/mant_ofi_modificar_datos_oficina.jsp?ofipk="+pk;
              parent.botonera.location.href = "../oficinas/mant_ofi_botones_modificar.jsp";
              break;
      }
      case 3:  {
        if ((pk_perm != null) && (pk_perm > 0)) {
         parent.centro.location.href = '../perfiles/mant_per_main_modificar.jsp?perpk='+pk+'&pk_destino='+pk_perm;
        }else {
         parent.centro.location.href = '../perfiles/mant_per_main_modificar.jsp?perpk='+pk;
        }
        parent.botonera.location.href = "../perfiles/mant_per_botones_modificar.jsp";
        break;
      }
      default: {
              parent.centro.location.href = "mant_usu_main.htm"
              parent.botonera.location.href = "mant_usu_down.htm"
      }
   }
 }
</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
                long l_pk_perm = 0;
		/* Recojo los parametros */
		//System.out.println(request.getParameter("estados"));
		String str_nombre = request.getParameter("nombre");
		String str_apellido1 = request.getParameter("apellido1");
		String str_apellido2 = request.getParameter("apellido2");
		String str_login = request.getParameter("login");
		int i_sysrol = new Integer(request.getParameter("perfiles")).intValue();
		//System.out.println(i_sysrol);
		long l_activo_sn = new Long(request.getParameter("estados")).longValue();
                long l_oficina = new Long(request.getParameter("oficina")).longValue();
                long l_cargo = new Long(request.getParameter("cargo")).longValue();
                long l_origen = new Long(request.getParameter("origen")).longValue();
                long l_reqpk = new Long(request.getParameter("pk_destino")).longValue();
                if (request.getParameter("pk_perm") != null) {
                  l_pk_perm =  new Long(request.getParameter("pk_perm")).longValue();
                }
   //             long l_reqpk;
   //             if ((Long)beanCargo.get("PK_CARGO") == null) {
   //               l_reqpk = 0;
   //             }
   //             else {
   //               Long pk = (Long)beanCargo.get("PK_CARGO");
   //               l_reqpk = pk.longValue();
   //             }

                /*Comprobacion de las claves */
                String str_pass="",str_repetida="",clave;
                int salir = 0;
                Usuarios_clave uC = new Usuarios_clave();
                SysUsuClaves sUC = new SysUsuClaves();
                TrataClave tC = new TrataClave();
                if (tC.initClave(str_login)){
                 clave=tC.getClaveDes();
                 if ((request.getParameter("password")!=null)&&(request.getParameter("password")!="")){
                    str_pass = request.getParameter("password");
                    if ((request.getParameter("repetida")!=null)&&(request.getParameter("repetida")!="")&&(request.getParameter("repetida").equals(str_pass))){
                      str_repetida = request.getParameter("repetida");
                       if (tC.esClaveRepetida(str_pass)){
                         salir =3;
                       }else{
                         if (tC.CaracteresIguales(str_pass)){
                           salir =4;
                         }
                       }
                    }else{
                       salir=2;
                    }
                 }else{
                    salir=1;
                 }
                }else{
                  salir=5;
                }
                switch (salir){
                  case 1:
                  {
%>
                  <script language="javascript">
                  alert("La clave no puede ser vacia");
                  </script>
<%
                  break;
                  }
                  case 2:
                  {
%>
                   <script language="javascript">
                   alert("Por favor, la clave y su repeticion ha de ser la misma.");
                   </script>
<%
                   break;
                  }
                  case 3:
                  {
%>
                  <script language="javascript">
                  alert("Clave invalida, ya ha utilizado esa clave anteriormente");
                  </script>
<%
                  break;
                  }
                  case 4:
                  {
%>
                  <script language="javascript">
                  alert("La clave no puede contener mas de 4 caracteres utilizados anteriormente");
                  </script>
<%
                  break;
                  }
                  case 5:
                  {
%>
                  <script language="javascript">
                  alert("El usuario no tiene claves asociadas.");
                  </script>
<%
                  break;
                  }
                  case 0: //clave lista para ser guardada.
                  {

                    Usuarios user = new Usuarios();
                    SysUsu sU = new SysUsu();
                    try{
                    /*Realizo los set */
                    sU.setNombre(str_nombre);
                    sU.setApellido1(str_apellido1);
                    sU.setApellido2(str_apellido2);
                    sU.setSyslogin(str_login);
                    sU.setSysRolPk(i_sysrol);
                    sU.setActivoSn(l_activo_sn);
                    sU.setOficinaPk(l_oficina);
                    sU.setSysCargosPk (l_cargo);
                    user.SetNegEvento(accesoBeanId.getNegEventoInstance(4));
                    /*modifico el objeto*/
                    user.modificarUsu(sU,str_pass);
                    }catch(ExceptionSESCAM eses){
                      System.out.println("Excepcion Sescam capturada en modificarUsuario.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                    }
%>

		<script language="javascript">
                    origen(<%=l_origen%>,<%=l_reqpk%>,<%=l_pk_perm%>);
		</script>

<%
                  break;
                  }
                }
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>