<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave, isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<%
  int ncar_min;
  FicheroClave fC = new FicheroClave();
  ncar_min = fC.DameLongitudClaveMin();
%>
<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">
  function control_longitud(n){
       var control=false;
       if ((parent.centro.document.formulario.accion.value=="m")&&(parent.centro.document.formulario.password.value=="")&&(parent.centro.document.formulario.repetida.value=="")){
          control=true;
       }else{
         if ((parent.centro.document.formulario.password.value.length>=n) && (parent.centro.document.formulario.repetida.value.length>=n)) {
           control=true;
         }else{
           alert("La longitud minima de la clave ha de ser de "+n+" caracteres");
         }
       }
       return control;
}

      function guardar(n) {
      	if (parent.centro.document.formulario.login.value==""){
      		alert("EL campo LOGIN no pueden ser vacio");
      	}else{
     	     if (control_longitud(n)){
                respuesta = confirm("�Desea guardar los datos?")
      		if (respuesta == true){
           		parent.centro.document.formulario.submit();
               	}else{
      			parent.document.location.href = "mant_usu_index.htm";
      		}
             }
        }
      }

      function cancelar() {
      		alert("No se guardar�n los cambios.");
      		parent.document.location.href = "mant_usu_index.htm";
      }
</script>

</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="20%" class="botonAccion">&nbsp;</td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="guardar(<%=ncar_min%>);">Guardar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar();">Cancelar</td>
    <td  width="20%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
