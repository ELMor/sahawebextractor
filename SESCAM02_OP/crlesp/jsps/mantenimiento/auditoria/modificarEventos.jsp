<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanEventoTipo" class="java.util.HashMap" scope="session"/>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
                long pk_evento = 0;
                String campo = "";
                int int_pos;
                Long fila_selec;
                int activo = 0;
                long fila_destino = 0;
                TipoEvento tipo_evento;
                EventoTipo eventpo;
                beanEventoTipo.clear();
                long filas = new Long(request.getParameter("filas")).longValue();
                Vector vec = new Vector();

                if (filas == -1) {
                  if (request.getParameter("fila_destino") != null) {
                     try {
                       fila_destino = new Long(request.getParameter("fila_destino")).longValue();
                     }catch(Exception ex) {
                       fila_destino = 1;
                     }
                  }else {
                    fila_destino = 1;
                  }
                  tipo_evento= new TipoEvento();
                  vec = tipo_evento.busquedaTiposEventos("","");
                  eventpo = new EventoTipo();
                  if (vec != null) {
                    for (int_pos = 0;int_pos < vec.size();int_pos++) {
                      activo = 0;
                      int pos = int_pos +1;
                      campo = "estado"+pos;

                      if (request.getParameter(campo) != null) {
                        activo = 1;
                      }
                      eventpo = (EventoTipo) vec.elementAt(int_pos);
                      eventpo.setTpoevtoAct(activo);
                      try{
                       tipo_evento.modificarTipoEvento(eventpo);
                      }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en modificarEventos.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                      }
                    }
                    fila_selec = new Long(fila_destino);
                    beanEventoTipo.put("FILA",fila_selec);
                  }
                } else {
                  if (filas > 0) {
                    pk_evento = new Long(request.getParameter("codigo_"+filas)).longValue();

                    campo = "estado"+filas;
                    if (request.getParameter(campo) != null) {
                     activo = 1;
                    }

                    tipo_evento= new TipoEvento();
                    eventpo = new EventoTipo();
                    vec = tipo_evento.busquedaTiposEventos("TPOEVTO_PK = "+pk_evento,"");
                    if (vec != null) {
                      eventpo = (EventoTipo) vec.elementAt(0);
                      eventpo.setTpoevtoAct(activo);
                      tipo_evento.modificarTipoEvento(eventpo);
                      fila_selec = new Long(filas);
                      beanEventoTipo.put("FILA",fila_selec);
                    }
                 }
                }
              %>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>