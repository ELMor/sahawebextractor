<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
   <link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
   <jsp:useBean id="beanPermisos" class="java.util.HashMap" scope="session"/>

<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(pk,destino){
  parent.centro.location = "../perfiles/mant_per_main_modificar.jsp?perpk="+pk+"&pk_destino="+destino;
  parent.botonera.location.href = "../perfiles/mant_per_botones_modificar_deshabilitada.jsp"
}

</script>

</head>

<BODY class="mainFondo">
<%
		beanPermisos.clear();
		Permisos perm= new Permisos();
		Vector vec = new Vector();
		long l_reqpk = new Long(request.getParameter("permpk")).longValue();
		vec = perm.busquedaPerm("SYS_PERM_PK="+l_reqpk,"");
		SysPermisos sysPerm=(SysPermisos)vec.elementAt(0);
		beanPermisos.put("PERMISO",sysPerm);
		//System.out.print(beanPermisos);
%>
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> Modificar Permiso</td>
  </tr>
</table>
<form name="formulario" action="modificarPermiso.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
	 <td></td>
  </tr>
  <tr>
    <td class= "texto">Nombre Permiso: </td>
    <td class= "normal"><input type="text" name="nombre" class="cajatexto2" maxlength = 30 value="<%= sysPerm.getSysPermNombre()%>" readonly></td>
  </tr>
  <tr>
  	<td>&nbsp</td>
  	<td>&nbsp</td>
  </tr>
  <tr>
    <td class= "texto">Descripcion Permiso: </td>
    <td class= "normal"><input type="text" name="descripcion" class="cajatexto3" maxlength = 255 value="<%= sysPerm.getSysPermDesc()%>" readonly></td>
  </tr>
</table>
</form>
<form name="seleccion">
<table width="100%" class = "textoTop" border = "0">
  <tr>
    <td bgcolor=#5C5CAD>Perfiles Asociados </td>
  </tr>
</table>
<br>
<br>
<table class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="1" width="100%">
        <tr class="filaCabecera">
            <td width="40%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Nombre</a>
             </td>
            <td width="60%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Descripción</a>
             </td>
        </tr>
</table>
<table height="100%" width="100%" align="center" border="1" cellspacing="0" cellpadding="0">
  <tr>
  <td class="areaCentral" >
    <table class="centroTabla" width="100%" id="tablaperfil" cellspacing="0" cellpadding="0" border="1">
       <%
         boolean bFilaPar=true;
         int idCounter=0;
         String s;
         Perfiles per = new Perfiles();
         PermRol rol = new PermRol();
         Vector v = new Vector();
         v = rol.busquedaPermRol("SYS_PERM_PK =" +l_reqpk,"");
         if (v!=null){
                 for(int int_pos = 0;int_pos < v.size(); int_pos++){
                         SysPermRol sysperrol=(SysPermRol)v.elementAt(int_pos);
                         long l_rol_pk = sysperrol.getSysRolPk();
                         Vector v1 = new Vector();
                         v1 = per.busquedaPerfil("SYS_ROL_PK = " +l_rol_pk,"SYS_ROL_NOMBRE");
                         if (v1 != null) {
                           SysRoles sysroles = (SysRoles)v1.elementAt(0);
                           if (bFilaPar){
                              bFilaPar=false;
 %>
                              <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaperfil'].rows, this.id);selec(<%=l_rol_pk%>,<%=l_reqpk%>);" style="cursor:hand;">
 <%				}else{
                                         bFilaPar=true;
 %>
                                 <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaperfil'].rows, this.id);selec(<%=l_rol_pk%>,<%=l_reqpk%>);" style="cursor:hand;">
 <%
                          }
 %>
                                   <td width="40%" height="12px"><%=sysroles.getSysRolNombre()%></td>
                                   <td width="60%" height="12px"><%=sysroles.getSysRolDesc()%></td>
                              </tr>
<%
                       idCounter++;
                      }
                   }
         }
%>
        </table>
        </td>
      </tr>
    </table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
