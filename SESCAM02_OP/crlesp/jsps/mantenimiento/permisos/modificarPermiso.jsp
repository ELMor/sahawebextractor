<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Permisos,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanPermisos" class="java.util.HashMap" scope="session"/>
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
		String str_nombre = request.getParameter("nombre");
		String str_descripcion = request.getParameter("descripcion");
		Permisos perm = new Permisos();
		SysPermisos sP = (SysPermisos)beanPermisos.get("PERMISO");
		//System.out.print(beanPermisos);

		/*Realizo los set */
		sP.setSysPermNombre(str_nombre);
		sP.setSysPermDesc(str_descripcion);

		/*modifico el objeto*/
		perm.modificarPerm(sP);
		beanPermisos.clear();
		%>

		<script language="javascript">
			parent.centro.location.href = "mant_perm_main.htm"
			parent.botonera.location.href = "mant_perm_down.htm"
		</script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>