<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(pk){
  parent.parent.centro.location='mant_pro_main_ficha_procedimiento.jsp?procpk='+pk;
  parent.parent.botonera.location.href = "mant_pro_botones_modificar.jsp"
}

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
  <%
    String s_cod_proc = "";
    String s_desc_proc = "";
    boolean b_nobuscar = false;
    boolean b_buscarcodigo = true;
    boolean b_buscardesc = true;

    if (request.getParameter("codigo") != null) {
      s_cod_proc = new String(request.getParameter("codigo").toUpperCase());
    }
    if (request.getParameter("descrip") != null) {
      s_desc_proc = new String(request.getParameter("descrip").toUpperCase());
    }

    if ((s_cod_proc.equals("")) && (s_desc_proc.equals(""))) {
      b_nobuscar = true;
    }
    if (s_cod_proc.equals("")) {
      b_buscarcodigo = false;
    }
    if (s_desc_proc.equals("")) {
        b_buscardesc = false;
    }
  %>

<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaProc" cellspacing="0" cellpadding="0" border="0">
        <%
        if (b_nobuscar == false) {
           %>
            <script language="javascript">
                    parent.pie.location="../../pie_buscador.jsp?estado=Generando resultado...";
            </script>
          <%
          boolean bFilaPar=true;
          int idCounter=0;
          String s_where = "";
          Procedimientos proc = new Procedimientos();
          Vector v = new Vector();
          boolean b_pasa = false;
          if (b_buscarcodigo == true) {
            s_where = "(upper(proc_cod) like '" + s_cod_proc + "%')";
            b_pasa = true;
          }
          if (b_buscardesc == true) {
            if (b_pasa == false) {
              s_where = "(upper(proc_desc_corta) like  '%" + s_desc_proc + "%')";
            } else {
              s_where = s_where + " AND (upper(proc_desc_corta) like  '%" + s_desc_proc + "%')";
            }
          }
   //          System.out.println(s_where);

          v = proc.busquedaProced(s_where,"PROC_DESC_CORTA ASC");
          String tipo = "";
          double d_garantia = 0;
          Vector ve = new Vector();
          if (v.size()>0){

            for(int int_pos = 0;int_pos < v.size(); int_pos++){
              SesProcedimientos sesproc=(SesProcedimientos)v.elementAt(int_pos);
              long procpk = sesproc.getProcPk();
              ProcGarantias procgar = new  ProcGarantias();
              d_garantia = 0;
              ve = procgar.busquedaProcGarantia("proc_pk = "+procpk+" and sesesp_cod is null ","");
              if (ve.size() == 1) {
                SesProcGarantia sesprocgar = (SesProcGarantia)ve.elementAt(0);
                d_garantia = sesprocgar.getProcGarantia();
              }

              long tipopk = sesproc.getProcTipo();
              ProcTipos tipoproc = new ProcTipos();

              ve = tipoproc.busquedaProcTipo("proc_tipo = "+tipopk,"");
              if (ve.size() > 0) {
                SesProcTipos sestipos = (SesProcTipos)ve.elementAt(0);
                tipo = sestipos.getProcTcod();
              }
              if (bFilaPar){
                bFilaPar=false;
	%>
                <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaProc'].rows, this.id);selec(<%=procpk%>)" style="cursor:hand;">
	<%
              }else{
                bFilaPar=true;

	%>
                <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaProc'].rows, this.id);selec(<%=procpk%>)" style="cursor:hand;">
	<%
              }
	%>
              <td width="6%" height="12px"><%=tipo%></td>
              <td width="10%" height="12px"><%=sesproc.getProcCod()%></td>
              <td width="55%" height="12px"><%=sesproc.getProcDescCorta()%></td>
              <td width="10%" height="12px"><%=sesproc.getProcMontoMaxEst()%></td>
              <td width="10%" height="12px"><%=d_garantia%></td>

        <%
             String estado = "Baja";
             if (sesproc.getProcActivo()== 1) {
                estado = "Alta";
             }
        %>
              <td width="9%" height="12px"><%=estado%></td>
             </tr>
<%
              idCounter++;
           }
           %>
           <script language="javascript">
                    parent.pie.location="../../pie_buscador.jsp?estado=Listo&filas=<%=v.size()%>";
           </script>
           <%
         }else {
             %>
             <Script language="javaScript">
                    parent.pie.location="../../pie_buscador.jsp?estado=No hay se han encontrado filas.";
             </Script>
             <%
           }
     }
%>
     </table>
    </td>
   </tr>
   </table>
  </form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

