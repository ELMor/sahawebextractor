<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcedimiento" class="java.util.HashMap" scope="session"/>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
                double l_garantia = 0;
                Long pk = (Long)beanProcedimiento.get("PROCEDIMIENTO");
                long l_procpk = pk.longValue();
                long fila = new Long(request.getParameter("filaseleccionada")).longValue();
                if (fila >= 0) {
                  String campo_esp = "especialidad_"+fila;
                  String campo_gar = "garantia_"+fila;
                  String campo_pk = "pk_"+fila;

                  String str_codigo = request.getParameter(campo_esp);

                  long pkgar = new Long(request.getParameter(campo_pk)).longValue();
                  int i_error = 1;
                  try {
                    l_garantia  = new Double(request.getParameter(campo_gar)).doubleValue();
                  }catch (Exception ex) {

                   ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);

                   %>
                   <script language="javascript">
                      alert ('<%=es.getMensaje()%>');
                   </script>
                   <%
                   i_error = -1;
                  }
                if (i_error > 0) {
                /*Realizo los set */
                 ProcGarantias procGarantia = new ProcGarantias();

                 SesProcGarantia sesprocgar = new SesProcGarantia();
                 Vector v = new Vector();
                 v = procGarantia.busquedaProcGarantia("procgar_pk ="+pkgar,"");
                 sesprocgar = (SesProcGarantia)v.elementAt(0);
                 sesprocgar.setSesespCod(str_codigo);
                 sesprocgar.setProcGarantia(l_garantia);

                 /*modifico el objeto*/
                 try{
                   procGarantia.modificarProcGar(sesprocgar);
                 }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en GuardaGarantia.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                }
               // beanProcedimiento.clear();

%>
                 <script language="javascript">
                       parent.fichagar.location.href = 'mant_pro_modificar_garantia.jsp?procpk='+<%=l_procpk%>;
                       parent.downgar.location.href = "mant_pro_botones_garantia.jsp";
		 </script>
                <%
                }

            }
          %>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>