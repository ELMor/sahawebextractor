<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcedimientos" class="java.util.HashMap" scope="session"/>

<script language="javascript">
        function consultar_actualizacion(filas){
          resp = confirm("Se van actualizar "+filas+ " registros.�Desea continuar la operacion? ")
          if (resp == true){
            return true
          }
          else {
            return false
          }
        }
</script>
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

   <%
   int i,i_error=1;
   boolean act_garantia = false,act_monto = false, act_estado = false;
   long l_garantia = 0;
   double d_monto  = 0;
   long l_estado = 1;

   if (beanProcedimientos.size() > 0) {
     Vector v = new Vector();
     Procedimientos proc = new Procedimientos();
     SesProcedimientos sO = new SesProcedimientos();

     v = (Vector) beanProcedimientos.get("VECTOR");
     if (v.size()>0){
     Boolean actgar,actmont;
     Long garantia;
     Double monto;
     if (request.getParameter("act_monto") != null) {
      act_monto = true;
     }
     if (request.getParameter("act_garantia") != null) {
       act_garantia = true;
      }
     if (request.getParameter("act_estado") != null) {
       act_estado = true;
     }

     try {
       l_garantia = new Long(request.getParameter("garantia")).longValue();
       l_estado = new Long(request.getParameter("estado")).longValue();
       d_monto = new Double (request.getParameter("monto")).doubleValue();
     }catch (Exception ex) {
       ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);
       %>
        <script language="javascript">
         alert ('<%=es.getMensaje()%>');
        </script>
       <%
       i_error = -1;
     }
     String campo="";
     long procpk=0;
     if ((i_error > 0) && (v.size() > 0)) {
       for (i=0;i<v.size();i++) {
         sO = (SesProcedimientos)v.elementAt(i);
         procpk = sO.getProcPk();
         // Si se actualiza la garantia hay que modificarla en SES_PROC_GARANTIAS
         if (act_garantia == true) {
          SesProcGarantia sesprocgar = new SesProcGarantia();
          ProcGarantias procgar = new  ProcGarantias();
          Vector ve = new Vector();
          ve = procgar.busquedaProcGarantia("proc_pk = "+procpk+" and sesesp_cod is null ","");
          if (ve.size() == 1) {
            sesprocgar = (SesProcGarantia)ve.elementAt(0);
            sesprocgar.setProcGarantia(l_garantia);
          }
          try{
            procgar.modificarProcGar(sesprocgar);
          }catch (Exception eses){
            System.out.println("Excepcion Sescam capturada en ActualizarProcedimientos.jsp");
%>
           <script language="javascript">
           alert("<%=eses%>");
           </script>
<%
          }
         }

         if (act_monto == true ) {
          sO.setProcMontoMaxEst(d_monto);
         }

         if (act_estado == true) {
           sO.setProcActivo(l_estado);
         }

         if (act_monto == true || act_estado == true){
          /*Modifico el Procedimiento*/
          try{
           proc.modificarProced(sO);
           }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en ActualizarProcedimientos.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
           }
        }
       }
      }
     }
   }else{
%>
       <script language = "javascript">
           alert("No existen procedimientos a actualizar");
       </script>
<%
   }
%>

   <script language = "javascript">
     parent.parent.location = "mant_pro_index_carga_masiva.htm";
   </script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>