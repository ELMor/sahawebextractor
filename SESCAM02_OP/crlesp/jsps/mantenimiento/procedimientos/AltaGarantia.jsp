<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.ProcGarantias,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcedimiento" class="java.util.HashMap" scope="session"/>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */

                Long pk = (Long)beanProcedimiento.get("PROCEDIMIENTO");
                long l_procpk = pk.longValue();
             //   System.out.println(l_procpk);
                String str_codigo_esp = request.getParameter("especialidad");
                long l_garantia = 0;
                int i_error;
                try {
                   l_garantia = new Long(request.getParameter("garantia")).longValue();
                   i_error = 1;
                } catch (Exception ex) {
                    i_error = -1;
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);
                    %>
                  <script language="javascript">
                      alert ('<%=es.getMensaje()%>');
                  </script>
                    <%
                }
                /*Realizo los set */
                if (i_error > 0) {
                 ProcGarantias procgar = new ProcGarantias();
                 SesProcGarantia sesprocgar = new SesProcGarantia();

                 sesprocgar.setSesespCod(str_codigo_esp);
                 sesprocgar.setProcGarantia(l_garantia);
                 sesprocgar.setProcPk(l_procpk);
                 try{
                 procgar.insertarProcGar(sesprocgar);
                 }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en AltaGarantia.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                  }

   		 //System.out.println("fin insertar ");
		%>

		 <script language="javascript">
                   parent.fichagar.location.href = 'mant_pro_modificar_garantia.jsp?procpk='+<%=l_procpk%>;
                   parent.downgar.location.href = "mant_pro_botones_garantia.jsp";
		 </script>
                <%
                }
                %>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>