<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanBusquedaProcedimiento" class="java.util.HashMap" scope="session"/>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */

                beanBusquedaProcedimiento.clear();
                String str_codigo = request.getParameter("codigo");
                String str_descrip_corta = request.getParameter("descrip");
                String str_descrip = request.getParameter("descrip_larga");
                long l_tipo = new Long(request.getParameter("valor_tipo")).longValue();

                long l_activo = 0;
                double d_garantia = 0;
                double d_monto = 0;
                int i_error;
                try {
                   l_activo = new Long(request.getParameter("activo")).longValue();
                   d_garantia = new Double(request.getParameter("garantia")).doubleValue();
                   d_monto = new Double (request.getParameter("monto")).doubleValue();
                   i_error = 1;
                } catch (Exception ex) {
                    i_error = -1;
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);
                    %>
                  <script language="javascript">
                      alert ('<%=es.getMensaje()%>');
                  </script>
                    <%
                }
                /*Realizo los set */
                if (i_error > 0) {
                 Procedimientos proc = new Procedimientos();
                 SesProcedimientos sO = new SesProcedimientos();

                 sO.setProcCod(str_codigo);
                 sO.setProcDescCorta(str_descrip_corta);
                 sO.setProcDesc(str_descrip);
                 sO.setProcTipo(l_tipo);
                 sO.setProcActivo(l_activo);
            //     sO.setProcGarantia(l_garantia);
                 sO.setProcMontoMaxEst(d_monto);
                 try{
                  proc.SetNegEvento(accesoBeanId.getNegEventoInstance(1));
                  proc.insertarProced(sO);
                  if (d_garantia >= 0) {
                    ProcGarantias procgar = new ProcGarantias();
                    SesProcGarantia sesprocgar;
                    sesprocgar = new SesProcGarantia();
                    sesprocgar.setProcGarantia(d_garantia);
                    sesprocgar.setProcPk(sO.getProcPk());
                    procgar.insertarProcGar(sesprocgar);
                  }
                 }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en guardarProcedimiento.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                  }

                 String s_codigo = sO.getProcCod();
                 beanBusquedaProcedimiento.put("CODIGO_PROCEDIMIENTO",s_codigo);
   		 //System.out.println("fin insertar ");
		%>

		 <script language="javascript">
//                        parent.botonera.location = "mant_pro_botones_buscador.jsp"
			parent.centro.location = "mant_pro_main.htm"
		 </script>
                <%
                }
                %>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>