<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcedimientos" class="java.util.HashMap" scope="session"/>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

   <%
   int i,i_filas,i_error = 1,i_filas_modificadas,i_modificadas = 0;
   String str_codigo,str_descrip_corta;
   String campo_cod,campo_des,campo_mon,campo_gar;

   long l_garantia = 0,l_activo = 0;
   double d_monto = 0;

   i_filas = new Integer(request.getParameter("filas")).intValue();
   i_filas_modificadas = new Integer(request.getParameter("filas_modificadas")).intValue();

   for (i=0;i<i_filas;i++) {

    if (i_modificadas < i_filas_modificadas) {
     campo_cod = "codigo_"+i;
     campo_des = "descrip_"+i;
     campo_mon = "monto_"+i;
     campo_gar = "garantia_"+i;
     if (request.getParameter(campo_cod) != null) {
       i_modificadas = i_modificadas + 1;
       str_codigo = request.getParameter(campo_cod);
       str_descrip_corta = request.getParameter(campo_des);
       try {
        l_activo = 1;
        l_garantia = new Long(request.getParameter(campo_gar)).longValue();
        d_monto = new Double (request.getParameter(campo_mon)).doubleValue();
        i_error = 1;
       } catch (Exception ex) {
          i_error = -1;
          ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);
          %>
          <script language="javascript">
            alert ('<%=es.getMensaje()%>');
          </script>
          <%
         }
         if (i_error > 0) {
          SesProcedimientos sO = new SesProcedimientos();
          Procedimientos proc = new Procedimientos();
          sO.setProcCod(str_codigo);
          sO.setProcDescCorta(str_descrip_corta);
          sO.setProcDesc(str_descrip_corta);
          sO.setProcActivo(l_activo);
          sO.setProcGarantia(l_garantia);
          sO.setProcMontoMaxEst(d_monto);
          /*modifico el objeto*/
          try{
          proc.insertarProced(sO);
          }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en AnnadirProced.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                  }
        }
        else {
         break;
        }
      }
    }else {
      break;
    }
   }
   %>
     <script language = "javascript">
      parent.location = "mant_pro_index_nuevos_proced.htm";
     </script>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>