<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanTramos" class="java.util.HashMap" scope="session"/>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */

                Long l_codTramo = (Long) beanTramos.get("COD_TRAMO");
                Integer l_maxDias = (Integer) beanTramos.get("MAX_DIAS");
                long codTramo = l_codTramo.longValue();
                int MAX_DIAS = l_maxDias.intValue();
                Vector nuevo = new Vector();
                int i = 0;
                boolean ultimoTramo =false;
                while (ultimoTramo == false) {
                  SesTramos sesTramo = new SesTramos();
                  if (i == 0) {
                    sesTramo.setInicio(0);
                  } else {
                    SesTramos anterior = (SesTramos) nuevo.elementAt(i - 1);
                    sesTramo.setInicio(anterior.getFin() + 1);
                  }
                  String nombre = "fin" + i;
                  String sFin = request.getParameter(nombre);

                  double fin = -1;
                  if (sFin == null) {
                    fin = 0;
                  } else {
                    Double d_fin = new Double(sFin);
                    if ((d_fin.longValue() == 0) || (d_fin.longValue() > MAX_DIAS)) {
                      fin = MAX_DIAS;
                    } else {
                      fin = d_fin.doubleValue();
                    }
                  }
                  nombre = "descripcion" + i;
                  String descripcion = request.getParameter(nombre);
                  if ( (((int) sesTramo.getInicio()) == (MAX_DIAS) + 1) || (((int) sesTramo.getInicio()) == MAX_DIAS)){
                    sesTramo.setFin(0);
                    ultimoTramo = true;
                  } else {
                    sesTramo.setFin(fin);
                  }
                  sesTramo.setPosicion(i + 1);
                  sesTramo.setCodtramo(codTramo);
                  sesTramo.setDescripcion(descripcion);
                  i++;
                  nuevo.addElement(sesTramo);
                }
                beanTramos.put("TRAMOS", nuevo);
//                beanTramos.put("COD_TRAMO", new Long (codTramo));
//                beanTramos.put("MAX_DIAS", new Integer (MAX_DIAS));

                %>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>