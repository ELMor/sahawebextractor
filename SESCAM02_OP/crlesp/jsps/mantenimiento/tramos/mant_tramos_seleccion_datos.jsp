<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Tramos,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<jsp:useBean id="beanTramos" class="java.util.HashMap" scope="session"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(){
  parent.parent.centro.location='mant_tramos_modificar_datos_tramos.jsp';
  parent.parent.botonera.location.href = "mant_tramos_botones_modificar.jsp"
}

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
  <%
    long cod_tramo = 0;
    boolean b_nobuscar = false;

    if (request.getParameter("codigo") != null) {
      Long cTramo = new Long(request.getParameter("codigo"));
      cod_tramo = cTramo.longValue();
//      beanTramos.put("COD_TRAMO", cTramo);
    }
    beanTramos.clear();

    if (cod_tramo == 0) {
      b_nobuscar = true;
    }
  %>

<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaDiag" cellspacing="0" cellpadding="0" border="0">
        <%
        if (b_nobuscar == false) {
           %>
            <script language="javascript">
              parent.pie.document.pieEtado.lEstado.value="Generando resultado...";
            </script>
          <%
          boolean bFilaPar=true;
          int idCounter=0;
          String s_where = "CODTRAMO = " + cod_tramo;
          Tramos tramo = new Tramos();
          Vector v = new Vector();
          boolean b_pasa = false;
   //          System.out.println(s_where);

          v = tramo.busquedaTramo(s_where, "POSICION ASC");
          if (v.size()>0){

            for(int int_pos = 0;int_pos < v.size(); int_pos++){
              SesTramos sesTramo = (SesTramos) v.elementAt(int_pos);
//              long codigo = sesTramo.getCodtramo();
              if (bFilaPar){
                bFilaPar=false;
	%>
                <tr class="filaPar" id=<%=idCounter%>>
	<%
              }else{
                bFilaPar=true;

	%>
                <tr class="filaImpar" id=<%=idCounter%>>
	<%
              }
	%>

        <%
        long ini = new Double (sesTramo.getInicio()).longValue();
        long fin = new Double (sesTramo.getFin()).longValue();
        %>

              <td width="15%" height="12px"><%=sesTramo.getCodtramo()%></td>
              <td width="15%" height="12px"><%=sesTramo.getPosicion()%></td>
              <td width="15%" height="12px"><%=ini%></td>
        <% if (fin != 0) {
          %>
              <td width="15%" height="12px"><%=fin%></td>
          <%
          } else {
          %>
              <td width="15%" height="12px">&nbsp;</td>
          <%
          }
          %>
              <td width="40%" height="12px"><%=sesTramo.getDescripcion()%></td>

              </tr>
<%
              idCounter++;
           }
           %>

           <script language="javascript">
           <%
           SesTramos ultimo = (SesTramos) v.elementAt(v.size() - 1);
           int max = new Double (ultimo.getInicio()).intValue() - 1;
           %>
           parent.buscador.document.formulario.maxDias.value = <%=max%>

           </script>

           <script language="javascript">
              parent.pie.document.pieEtado.lEstado.value="Listo";
              parent.pie.document.pieEtado.lFilas.value=<%=v.size()%>;
           </script>
           <%
         }else {
             %>
             <Script language="javaScript">
               parent.pie.document.pieEtado.lEstado.value="Ingrese condiciones de busqueda";
               parent.pie.document.pieEtado.lFilas.value="0";
             </Script>
             <%
           }
     }
%>
     </table>
    </td>
   </tr>
   </table>
  </form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

