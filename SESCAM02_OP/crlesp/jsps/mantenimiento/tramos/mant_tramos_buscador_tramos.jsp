<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language = "javascript">
  function recargar() {
     this.document.formulario.submit();
   }
</script>
</head>

<BODY class="mainFondo">
<TABLE width="100%" class="textoTop" border = "0">
  <tr>
  	 <td bgcolor=#5C5CAD> Buscador Tramos</td>
  </tr>
</table>
<form name="formulario" action="mant_tramos_seleccion_datos.jsp"  target = "datos">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto" width="25%" >C&oacute;digo tramo: </td>
    <td class= "normal" width="25%" ><div id="divCodigo">
           <Select name="codigo" class="cajatexto2" onChange="recargar();">
             <option selected value="0"> &nbsp; </option>
              <%
               for (int int_pos = 0;int_pos < Tramos.STR_CODTRAMOS.length;int_pos++) {
              %>
                 <option  value=<%=int_pos+1%>><%=Tramos.STR_CODTRAMOS[int_pos]%></option>
              <%
               }
              %>
           </select>
    </div>
    </td>
    <td class="normal" width="5%">&nbsp</td>
    <td class="texto" width="25%"> Duraci&oacute;n m&aacute;xima: </td>
    <td class="normal" width="20%"> <input type="text" name="maxDias" style="width:40%"class="cajatexto2" value=""></td>
  </tr>
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
