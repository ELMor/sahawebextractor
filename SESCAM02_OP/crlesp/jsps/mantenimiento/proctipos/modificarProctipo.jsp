<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcTipo" class="java.util.HashMap" scope="session"/>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
                String str_codproc = request.getParameter("codproc");
		String str_descproc = request.getParameter("descproc");
                SesProcTipos sesPT =  (SesProcTipos)beanProcTipo.get("PROCTIPO");
                ProcTipos pT = new ProcTipos();

                /*Realizo los set */
                sesPT.setProcTcod(str_codproc);
                sesPT.setProcTdesc(str_descproc);

                try{
                   pT.SetNegEvento(accesoBeanId.getNegEventoInstance(27));
                   /*modifico el objeto*/
                   pT.modificarEsp(sesPT);
                }catch(ExceptionSESCAM eses){
                      System.out.println("Excepcion Sescam capturada en modificarProcTipo.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                 }
       		beanProcTipo.clear();
%>

                <script language="javascript">
                    parent.centro.location.href = "mant_proctipo_main.htm"
                    parent.botonera.location.href = "mant_proctipo_down.htm"
		</script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>