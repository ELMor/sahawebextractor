<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="pacientes/error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,isf.util.Utilidades,conf.*,java.util.*" %>

<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../css/estilo.css">
<jsp:useBean id="beanClave" scope="session" class="java.util.HashMap"/>
</head>
<%
  int ncar_min;
  FicheroClave fC = new FicheroClave();
  ncar_min = fC.DameLongitudClaveMin();
%>
<script language="javascript" type="text/javascript" src="../javascript/botonera.js"></script>
<script language="javascript">
function control_teclado(){
    if (((event.keyCode > 47) && (event.keyCode < 58)) || ((event.keyCode >64) && (event.keyCode <91)) || ((event.keyCode >96) && (event.keyCode <123)) || (event.keyCode==45) || (event.keyCode==95) || (event.keyCode==209) || (event.keyCode==241) ) event.returnValue=true;
         else event.returnValue=false;
  }

function control_longitud(n){
    var control=false;
    if ((document.cambio_clave.nueva.value.length>=n) && (document.cambio_clave.repetida.value.length>=n)) {
      control=true;
    }else{
      alert("La longitud minima de la clave ha de ser de "+n+" caracteres");
    }
    return control;
}

function aceptar(n) {
      if (control_longitud(n)){
        top.frames[1].location = "grabar_cambio_clave.jsp?nueva="+document.all.nueva.value+"&repetida="+document.all.repetida.value;
      }
}

</script>
<%
     String usuario="",clave="",expirada="no",texto="";
     int longitud;
     if ((request.getParameter("expirada")!="")&&(request.getParameter("expirada")!=null)){
       expirada= request.getParameter("expirada");
     }
     if (beanClave.get("usuario")!=null){
       usuario = new String((String)beanClave.get("usuario").toString());
     }
     if (beanClave.get("clave")!=null){
       clave = new String((String)beanClave.get("clave").toString());
     }
     //espirada = new String((String)beanClave.get("espirada").toString());
     FicheroClave fc = new FicheroClave();
     longitud = fc.DameLongitudClave();

     if (expirada.equals("si")){
       texto="La clave ha caducado.Ha de cambiarla por una nueva.";
     }


%>
<body class="mainFondoAzul">
<form name="cambio_clave" action="grabar_cambio_clave.jsp" target="oculto_nc" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" vspace="0" hspace="0">
 <br>
 <tr class="cabeceraSuperior">
  <td>&nbsp;</td>
  <td>Clave Nueva:</td>
  <td><input type="password" name="nueva" onkeypress="control_teclado();" maxlength=<%=longitud%> ></td>
 </tr>
 <tr class="cabeceraSuperior">
  <td>&nbsp;</td>
  <td>Repetir Clave Nueva:</td>
  <td><input type="password" name="repetida" onkeypress="control_teclado();" maxlength=<%=longitud%> ></td>
 </tr>
 <tr class="cabeceraSuperior">
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
 </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" vspace="0" hspace="0">
 <tr class="cabeceraSuperior">
  <td>&nbsp;&nbsp;<font color="yellow"><%=texto%></font></td>
 </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" vspace="0" hspace="0">
 <tr class="cabeceraSuperior">
  <td>&nbsp;</td>
  <td  width="30%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="aceptar(<%=ncar_min%>);">Aceptar</td>
  <td  width="30%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="top.close()">Cancelar</td>
 </tr>
</table>
</form>
</body>
</html>
