<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.*" %>
<jsp:useBean id="beanEntradaHisto" scope="session" class="java.util.HashMap"/>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">
        function hacer_submit() {
          var cadena_texto;
          cadena_texto = this.document.buscapac.apellido1.value.toUpperCase();
          this.document.buscapac.apellido1.value = cadena_texto;
          cadena_texto = this.document.buscapac.apellido2.value.toUpperCase();
          this.document.buscapac.apellido2.value = cadena_texto;
          cadena_texto = this.document.buscapac.nombre.value.toUpperCase();
          this.document.buscapac.nombre.value = cadena_texto;
          if ((this.document.buscapac.cip.value != '') && (this.document.buscapac.cip.value != null)) {
            this.document.buscapac.viene_cip.value = "1";
          }
          else {
            this.document.buscapac.viene_cip.value = "";
          }
          this.document.buscapac.submit();
          this.document.buscapac.viene_cip.value = "";
        }
        function calcular_cadena(){
          var buscamos = this.document.buscapac.cip.value;
          if (buscamos.length > 0) {
            i = buscamos.indexOf("&");
            if (i != -1) {
              i = i + 1;
              j = buscamos.indexOf('&', i);
              if (j != -1) {
                 buscamos = buscamos.substring(i,j)
                 this.document.buscapac.cip.value = buscamos;
                 cadena_texto = this.document.buscapac.apellido1.value.toUpperCase();
                 this.document.buscapac.apellido1.value = cadena_texto;
                 cadena_texto = this.document.buscapac.apellido2.value.toUpperCase();
                 this.document.buscapac.apellido2.value = cadena_texto;
                 cadena_texto = this.document.buscapac.nombre.value.toUpperCase();
                 this.document.buscapac.nombre.value = cadena_texto;
                 this.document.buscapac.viene_cip.value = "1";
                 this.document.buscapac.submit();
                 this.document.buscapac.viene_cip.value = "";
              }
            }
          }
        }
</script>
</head>
<%

   if (beanEntradaHisto.size()>0) beanEntradaHisto.clear(); //se limpia el bean ya que se va a realizar una nueva busqueda.
%>
<body class="mainFondo" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0" onLoad="document.buscapac.hicl.focus();">
<form name="buscapac" action="busca_paciente_main_lista_det.jsp" target="consultaPacDetalle" height="10%" method="post">
        <table width="100%" align="center" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0">
                <tr height="12px">
                  <td width="1%">&nbsp;</td>
                  <td width="30%" class="texto">Primer Apellido</td>
                  <td width="30%" class="texto">Segundo Apellido</td>
                  <td width="30%" class="texto">Nombre</td>
                  <td width="8%" class="texto">Hist&oacute;rico</td>
                  <td width="1%">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td class="normal"><input type="text" class="cajaTexto2" name="apellido1" maxlength = 255 value = "" onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
                  <td class="normal"><input type="text" class="cajaTexto2" name="apellido2" maxlength = 255 value = ""onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
                  <td class="normal"><input type="text" class="cajaTexto2" name="nombre" maxlength = 255 value = "" onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
	              <td class="normal"><input type="checkbox" name="historico"></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width="1%">&nbsp;</td>
                  <td width="30%" class="texto">Tarjeta sanitaria (CIP)</td>
                  <td width="30%" class="texto">Historia Cl&iacute;nica</td>
                  <td width="30%" class="texto">Centro</td>
                  <td width="8%">&nbsp;</td>
                  <td width="1%">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                        <td class="normal"><input type="text" class="cajaTexto2" name="cip" maxlength = 64 value="" onblur = "calcular_cadena();" onkeypress = "if (event.keyCode == 13) hacer_submit();"> </td>
                        <td class="normal"><input type="text" class="cajaTexto2" name="hicl" value = "" onkeypress = "if (event.keyCode == 13) hacer_submit();"> </td>
                        <td class="normal"><div id="centros">
                        <Select name="centro" class="cajatexto2" value="" style="width:90%">
                            <option default value='0'>TODOS</option>
<%
                        Centros c = new Centros();
                        Vector v = new Vector();
                        v = c.busquedaCentro("","DESCENTRO ASC");
                        for (int i=0;i<v.size();i++){
                            Centrosescam cs = (Centrosescam)v.elementAt(i);
%>
                            <option default value='<%=cs.getCodcentro()%>'><%=cs.getDescentro()%></option>
<%
                        }
%>
            			</div></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                </tr>
        </table>
        <td><input type="hidden" value="cip" name="orden"></td>
        <td><input type="hidden" name="viene_cip" value=""></td>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

