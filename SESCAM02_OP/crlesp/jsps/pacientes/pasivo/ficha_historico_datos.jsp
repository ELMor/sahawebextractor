<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page import="beans.ConsultaEntradasLEHisto,isf.persistencia.*,java.util.*" %>
<%@ page import="isf.negocio.EntradaLEHisto,isf.negocio.Garantia,isf.negocio.GaranTram,isf.negocio.OficinaHelper,isf.util.*" %>

<jsp:useBean id="beanEntradaHisto" scope="session" class="java.util.HashMap"/>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="../../../jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript">
var FilaSelected=-1;
var id_ant ="";

function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
}

function mostrar(id){
	if ((id_ant != "") && (id_ant != id)){
		document.all[id_ant].style.display = "none";
	}
	id_ant = id;
	if (document.all[id].style.display == ""){
		document.all[id].style.display = "none";
	}else {
		document.all[id].style.display ="";
	}
}
			

function imprimir(norden,centro,cq,doc,garpk,operacion){

     llamado='imprime_doc_pasivo.jsp?centro='+centro+'&norden='+norden+'&cq='+cq+'&documento='+doc+'&garpk='+garpk+'&operacion='+operacion+'&impresion='+'1'+'&usuario=<%=accesoBeanId.getUsuario()%>';
     window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
}
</script>
</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" >
<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
		<%
		boolean bFilaPar=true;
		int idCounter=0;
		String s="",codigo,descripcion;
		for (int i=0;i<beanEntradaHisto.size();i++){
				idCounter++;
				String ident = "submenu"+ new Integer(idCounter).toString();
				//String ident1 = "MFX" + new Integer(idCounter+1).toString();
				EntradaLEHisto enthisto=(EntradaLEHisto)beanEntradaHisto.get("entrada"+i);
				if (enthisto.fCDiag!=null) {
					codigo = enthisto.fCDiag;
					descripcion = enthisto.fDDiag;
				}else{
					codigo = enthisto.fCProc;
					descripcion = enthisto.fDProc;
				} 

				String garcq_sol= enthisto.fTipoCQ;
				long norden_sol= enthisto.fNorden;
				long centro_sol= enthisto.fCentro;
				String where="";
				if (garcq_sol.equals("Q")){
					where = "CENTRO="+centro_sol+" AND NORDEN="+norden_sol;
				}else{
					where = "CEGA="+centro_sol+" AND NCITA="+norden_sol;			
				}
				Garantia g = new Garantia();
				Vector vg=new Vector();
				Vector vgt=new Vector();
				long gar_pk,gar_estado,gar_tipo;
				String estado="";
				vg = g.busquedaGaran(where,"GAR_ESTADO,GAR_PK");
%>
				<table width="100%" id="tablaPac1" cellspacing="0" cellpadding="0" border="0">			   									
				  <tr> 
					<td><table class="centroTabla" width="100%" id="tablaPac1" cellspacing="0" cellpadding="0" border="0">			   					
<%				
				if (bFilaPar){
	      			   	bFilaPar=false;
%>
							<tr class="filaPar" style="cursor:hand;" onClick="mostrar('<%=ident%>');">
<%				}else{
						bFilaPar=true;
%>
							<tr class="filaImpar" style="cursor:hand;" onClick="mostrar('<%=ident%>');">
<%
				}
				if (vg.size()>0){
%>
								<td width="2%"><img src="../../../imagenes/lupapequena.gif" border=0></td>
<%              }else{
%>
								<td width="2%">&nbsp;</td>	
<%
				}
%>
								<td width="10%" height="15px"><%=enthisto.fNorden%></td>
					   			<td width="15%" height="15px"><%=enthisto.fCentro%>/<%=enthisto.fHicl%></td>
					   			<td width="65%" height="15px"><%=codigo%> - <%=descripcion%></td>
					   			<td width="10%" height="15px"><%=enthisto.fTipoCQ%></td>
							</tr>
				</table>
			   	<DIV style="display:none" id="<%=ident%>">				
<%
				bFilaPar = !bFilaPar;
				for(int int_pos = 0;int_pos < vg.size(); int_pos++){
                         SesGarantia sesGar=(SesGarantia)vg.elementAt(int_pos);
                         gar_estado=sesGar.getGarEstado();
                         gar_pk=sesGar.getGarPk();
                         gar_tipo=sesGar.getGarTipo();
                         GaranTram gt = new GaranTram();
                         vgt = gt.busquedaGaranTram("GAR_PK="+gar_pk+" AND GAR_OPERACION="+gar_estado,"");
                         SesGaranTram sgt = (SesGaranTram)vgt.elementAt(0);
						 if (gar_estado==1) {
                            estado="Solicitada";
                         }else{
                            if (sesGar.getGarEstado()==2) {
                              estado="Aprobada";
                            }else{
                               if (sesGar.getGarEstado()==3) {
                                   estado="Denegada";;
                               }else {
                                     estado= "Renunciada";
                               }
                            }
                         }
                         
%>
						<table class="centroTabla" width="100%" id="tablaPac2" cellspacing="0" cellpadding="0" border="0">
<%				
						if (bFilaPar){
%>
								<tr class="filaPar"">
<%						}else{
%>
								<tr class="filaImpar">
<%
						}
%>	
		                           <td width="25%" height="12px">- <%=Utilidades.dateToString(sgt.getGarFechor())%></td>
		                           <td width="25%" height="12px">&nbsp;<%=estado%></td>
		                           <td width="25%" height="12px">&nbsp;<%=OficinaHelper.DameOficina(sgt.getOficinaPk())%></td>
		                           
<%
			           if (sgt.getGarOperacion()==1){
%>
		                           <td align="center" width=""><a onClick="imprimir(<%=enthisto.fNorden%>,<%=enthisto.fCentro%>,'<%=enthisto.fTipoCQ%>','DOC01',<%=sgt.getGarPk()%>,<%=sgt.getGarOperacion()%>);" style=cursor:"hand"><img  alt="Documento de Solicitud" src="../../../imagenes/impresora.gif"></a></td>
<%					   }else { 
							if (sgt.getGarOperacion()==2){                           
%>
        		                   <td align="center" width=""><a onClick="imprimir(<%=enthisto.fNorden%>,<%=enthisto.fCentro%>,'<%=enthisto.fTipoCQ%>','DOC02',<%=sgt.getGarPk()%>,<%=sgt.getGarOperacion()%>);" style=cursor:"hand"><img  alt="Documento de Aprobacion" src="../../../imagenes/impresora.gif"></a></td>
<%		                    }else{
							 	 if (sgt.getGarOperacion()==3){
%>
		                           <td align="center" width=""><a onClick="imprimir(<%=enthisto.fNorden%>,<%=enthisto.fCentro%>,'<%=enthisto.fTipoCQ%>','DOC03',<%=sgt.getGarPk()%>,<%=sgt.getGarOperacion()%>);" style=cursor:"hand"><img  alt="Documento de Denegacion" src="../../../imagenes/impresora.gif"></a></td>                    
<%			                     }else {
									 if (sgt.getGarOperacion()==4){
%>
        		                   <td align="center" width=""><a onClick="imprimir(<%=enthisto.fNorden%>,<%=enthisto.fCentro%>,'<%=enthisto.fTipoCQ%>','DOC04',<%=sgt.getGarPk()%>,<%=sgt.getGarOperacion()%>);" style=cursor:"hand"><img  alt="Documento de Renuncia" src="../../../imagenes/impresora.gif"></a></td>                    
<%			  		                 }
								 }
						    }
						}
%>
                		           <td width="80%" height="12px"><a class="texto" href="" target="oculto"></a>
                         		</tr>
							</table>

<%
                }// end for
                bFilaPar = !bFilaPar;
%>
           		</DIV>
<%                
  		}//end del for
%>				
			
	 	<td>
       <tr> 
      </table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>