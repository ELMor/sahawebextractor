<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.Utilidades,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanParamDocNolesp" class="java.util.HashMap" scope="session"/>

<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<%
     NoRPLE norple = new NoRPLE();
     Nolesp nl = new Nolesp();
     Localidades local = new Localidades();
     boolean imprimir = true;

     //Recojida de parametros

     if ((request.getParameter("nombre")!=null)&&(!request.getParameter("nombre").equals(""))){
         nl.setNombre(request.getParameter("nombre").toUpperCase());
         beanParamDocNolesp.put("NOMBRE",request.getParameter("nombre"));
     }

     if ((request.getParameter("apellido1")!=null)&&(!request.getParameter("apellido1").equals(""))){
         nl.setApellido1(request.getParameter("apellido1").toUpperCase());
         beanParamDocNolesp.put("APELLIDO1",request.getParameter("apellido1"));
     }

     if ((request.getParameter("apellido2")!=null)&&(!request.getParameter("apellido2").equals(""))){
         nl.setApellido2(request.getParameter("apellido2").toUpperCase());
         beanParamDocNolesp.put("APELLIDO2",request.getParameter("apellido2"));
     }

     if ((request.getParameter("direccion")!=null)&&(!request.getParameter("direccion").equals(""))){
         nl.setDomicilio(request.getParameter("direccion"));
         beanParamDocNolesp.put("DIRECCION",request.getParameter("direccion"));
     }

     long poblacion=0;
     String[] localprov=null;
     String localidad="";
     String provincia="";
     if ((request.getParameter("poblacion")!=null)&&(!request.getParameter("poblacion").equals(""))){
         poblacion=new Long(request.getParameter("poblacion")).longValue();
         localprov=local.DameProvinciaLocalidad(poblacion);
         if ((localprov[0]!=null)&&(!localprov[0].equals(""))){
            nl.setPoblacion(localprov[0]);
            beanParamDocNolesp.put("POBLACION",localprov[0]);
         }
         if ((localprov[1]!=null)&&(!localprov[1].equals(""))){
            nl.setProvincia(localprov[1]);
            beanParamDocNolesp.put("PROVINCIA",localprov[1]);
         }
     }


     if ((request.getParameter("fechanac")!=null)&&(!request.getParameter("fechanac").equals(""))){
         nl.setFechaNac(Utilidades.stringToDate(request.getParameter("fechanac")));
         beanParamDocNolesp.put("FECHANAC",request.getParameter("fechanac"));
     }

     if ((request.getParameter("telefono")!=null)&&(!request.getParameter("telefono").equals(""))){
         nl.setTelefono(request.getParameter("telefono"));
         beanParamDocNolesp.put("TELEFONO",request.getParameter("telefono"));
     }

     if ((request.getParameter("nss")!=null)&&(!request.getParameter("nss").equals(""))){
         nl.setNss(request.getParameter("nss"));
         beanParamDocNolesp.put("NSS",request.getParameter("nss"));
     }

     if ((request.getParameter("cip")!=null)&&(!request.getParameter("cip").equals(""))){
         nl.setCip(request.getParameter("cip"));
         beanParamDocNolesp.put("CIP",request.getParameter("cip"));
     }


     if (((request.getParameter("centro")!=null))&&(!request.getParameter("centro").equals(""))){
         nl.setCentro(new Double(request.getParameter("centro")).doubleValue());
         beanParamDocNolesp.put("CENTRO",new Double(request.getParameter("centro")));
     }

     if ((request.getParameter("nhc")!=null)&&(!request.getParameter("nhc").equals(""))){
         nl.setNhc(new Double(request.getParameter("nhc")).doubleValue());
         beanParamDocNolesp.put("NHC",new Double(request.getParameter("nhc")));
     }

     if ((request.getParameter("prestacion")!=null)&&(!request.getParameter("prestacion").equals(""))){
         nl.setPrestacion(request.getParameter("prestacion"));
         beanParamDocNolesp.put("PRESTACION",request.getParameter("prestacion"));
     }

     if ((request.getParameter("nomape")!=null)&&(!request.getParameter("nomape").equals(""))){
         nl.setGarReplegApenom(request.getParameter("nomape"));
         beanParamDocNolesp.put("RESP_NOMAPE",request.getParameter("nomape"));
     }

     String nif="";
     if ((request.getParameter("dni")!=null)&&(!request.getParameter("dni").equals(""))){
         nif = request.getParameter("dni")+request.getParameter("letradni");
         nl.setGarReplegDni(nif);
         beanParamDocNolesp.put("NIF",nif);
     }


     if (request.getParameter("type").equals("1")){ // motivo del select
	     if ((request.getParameter("motivo")!=null)&&(!request.getParameter("motivo").equals(""))){
	         nl.setGarMotRechazo(request.getParameter("motivo"));
	         beanParamDocNolesp.put("MOTIVO",request.getParameter("motivo"));
	         //System.out.println("Motivo:"+request.getParameter("motivo"));
	     }
	 }

     if (request.getParameter("type").equals("0")){ // motivo libre
  	     if ((request.getParameter("motivo_libre")!=null)&&(!request.getParameter("motivo_libre").equals(""))){
	         nl.setGarMotRechazo(request.getParameter("motivo_libre"));
	         beanParamDocNolesp.put("MOTIVO",request.getParameter("motivo_libre"));
	         //System.out.println("Motivo libre:"+request.getParameter("motivo_libre"));
	     }
	 }

     String usuario = accesoBeanId.getUsuario();
     nl.setSysUsu(usuario);
     nl.setGarOficina(new Double(OficinaHelper.DameOficinaPk(usuario)).doubleValue());

     //Se realiza la insercion:
     try{
         norple.SetNegEvento(accesoBeanId.getNegEventoInstance(17));
         /*inserto el objeto*/
         norple.insertarNoLesp(nl);
     }catch(ExceptionSESCAM eses){
         imprimir = false;
         System.out.println("Excepcion Sescam capturada en guardarNolesp.jsp");
%>
        <script language="javascript">
            alert("<%=eses%>");
        </script>
<%
     }
     double garpk=nl.getNnolesp();
%>
<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript">
    var docaprobacion="DOC01";
    var docdenegacion="DOC03";
    function ImprimirDocAprob(){
        llamado='imprime_doc_nolesp.jsp?documento='+docaprobacion+'&garpk=<%=garpk%>&usuario=<%=accesoBeanId.getUsuario()%>&cip=<%=request.getParameter("cip")%>';
        window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
    }

    function ImprimirDocDeneg(){
        llamado='imprime_doc_nolesp.jsp?documento='+docdenegacion+'&garpk=<%=garpk%>&usuario=<%=accesoBeanId.getUsuario()%>&cip=<%=request.getParameter("cip")%>';
        window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
    }
</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
   /* Si la insercion ha sido correcta imprimir documento */
   if (imprimir){
%>
                <script language="javascript">
                        ImprimirDocAprob();
                        ImprimirDocDeneg();
                        parent.main.location.href = "ficha_paciente_nolesp_main.jsp";
                        parent.down.location.href = "ficha_paciente_nolesp_down.htm";
                </script>
<%
     }
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>