<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaSolicitudesBean" %>
<%@ page import="isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
function seleccionaFila(filas, id){
    filas[id].className = "filaSelecionada";
    }
function seleccion(pSelCip,centro,norden,cq){
  parent.parent.parent.location='../ficha_paciente_index.jsp?cip='+escape(pSelCip)+'&centro='+centro+'&norden='+norden+'&gar_cq='+cq;
}
</script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="listasol">
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
    <table style="width:100%;" align="left" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral2" >
	<table class="centroTabla" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
<%
		Solicitud solRet;
		Vector vp=null;
        long l_centro,l_oficina;
		boolean bFilaPar=true;
		int idCounter=0;
        ConsultaSolicitudesBean cs=new ConsultaSolicitudesBean();
        //System.out.println("Creo el ConsultaSolicitudesBean");
        if (request.getParameter("centro")!=null){
        vp=new Vector();
        l_centro=new Double(request.getParameter("centro")).longValue();
        l_oficina=new Double(request.getParameter("oficina")).longValue();
        cs.setCentro(l_centro);
        cs.setOficina(l_oficina);
        cs.setFechaini(request.getParameter("fechaini"));
        cs.setFechafin(request.getParameter("fechafin"));
        cs.setEstado(new Long(request.getParameter("estado")).longValue());
        cs.setOrden(request.getParameter("orden"));
        //System.out.println("Seteo los filtros");
        try {
        	vp=cs.resultado();
        }catch (ExceptionSESCAM eSES){
%>
                 <script language="javascript">
                     alert("<%=eSES%>");
                 </script>
<%
                 }
                }

        String str_estado="Solicitada";
        if (vp!=null){
		  if (vp.size()>0){
%>
                <script language="javascript">
                  parent.pie.location="../../pie_buscador.jsp?estado=Generando resultado...";
                </script>
<%
	  		Enumeration e = vp.elements();
            solRet = new Solicitud();
	    		while(e.hasMoreElements()) {
	      			solRet=(Solicitud)e.nextElement();
	      			if (bFilaPar){
	      			   	bFilaPar=false;
                                        //System.out.println("Estado"+idCounter+": "+solRet.fEstado);
%>
					<tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);seleccion('<%=solRet.fCip%>',<%=solRet.fCodCentro%>,<%=solRet.fNorden%>,'<%=solRet.fCq%>')" style="cursor:hand;">
<%       			}else{
					bFilaPar=true;
                                        //System.out.println("Estado"+idCounter+": "+solRet.fEstado);
%>
					<tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);seleccion('<%=solRet.fCip%>',<%=solRet.fCodCentro%>,<%=solRet.fNorden%>,'<%=solRet.fCq%>')" style="cursor:hand;">
<%
				}
%>
				<td width="13%"  height="12px" name="fecha"><%=solRet.fFecha%></td>
                                <td width="14%"  height="12px" name="oficina"><%=solRet.fOficina%></td>
				<td width="10%"  height="12px" name="centro" title="<%=solRet.fCentro%>"><%=solRet.fCodCentro%></td>
<%
                                if (solRet.fEstado==1){str_estado="Solicitada";}
                                if (solRet.fEstado==2){str_estado="Aprobada";}
                                if (solRet.fEstado==3){str_estado="Denegada";}
                                if (solRet.fEstado==4){str_estado="Renunciada";}
%>
                                <td width="13%"  height="12px" name="estado"><%=str_estado%></td>
				<td width="20%"  height="12px" name="cip"><%=solRet.fCip%></td>
				<td width="25%" height="12px" name="nom"><%=solRet.fApe1+' '+solRet.fApe2+','+solRet.fNomb%></td>
                                <td width="5%"  height="12px" name="Cq"><%=solRet.fCq%></td>
                             </tr>

<%
  				idCounter++;
  				}
%>
                                <script language="javascript">
                                   parent.pie.location="../../pie_buscador.jsp?estado=Listo&filas=<%=cs.getResultCount()%>";
                                </script>
<%
			}else{
%>
                                <script language="javascript">
                                   parent.pie.location="../../pie_buscador.jsp?estado=No se han encontrado filas.";
                                </script>
<%
                        }
                }
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
