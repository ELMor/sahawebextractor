<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/validarFecha.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/AnchorPosition.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/botones.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/CalendarPopup.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/PopupWindow.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/validacion_campos_fecha.js"></script>
<script language="javascript">
 function chequeoFechaInicio (){
    if ((document.buscasol.fechaini.value!=null)&&(document.buscasol.fechaini.value!="")){
      if (!esStringFecha(document.buscasol.fechaini.value)){
         alert('Formato de fecha incorrecto (dd/mm/aaaa)');
         document.buscasol.fechaini.focus();
      }else{
         var dia = getDia(document.buscasol.fechaini.value);
         var mes = getMes(document.buscasol.fechaini.value);
         var anno = getAnno(document.buscasol.fechaini.value);
         if (validacion_fecha(dia,mes,anno));
         else alert ("Fecha no valida");
      }
    }
  }
 function chequeoFechaFinal (){
    if ((document.buscasol.fechafin.value!=null)&&(document.buscasol.fechafin.value!="")){
      if (!esStringFecha(document.buscasol.fechafin.value)){
         alert('Formato de fecha incorrecto (dd/mm/aaaa)');
         document.buscasol.fechafin.focus();
      }else{
         var dia = getDia(document.buscasol.fechafin.value);
         var mes = getMes(document.buscasol.fechafin.value);
         var anno = getAnno(document.buscasol.fechafin.value);
         if (validacion_fecha(dia,mes,anno));
         else alert ("Fecha no valida");
      }
    }
  }

var selectedInput = "";
var cal = new CalendarPopup();
cal.setMonthNames('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
cal.setDayHeaders('D','L','M','X','J','V','S');
cal.setWeekStartDay(1);
cal.setReturnFunction("selectDate");
document.write(cal.getStyles());
function getDate(input,linkname) {
                selectedInput=input;
                cal.showCalendar(linkname);
  }

function selectDate(y,m,d) {
                  if (selectedInput!="")
                  {
                          selectedInput.value=d + "/" + m + "/" + y;
                  }
  }
</script>
</head>

<body class="mainFondo" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0">
<form name="buscasol" action="busca_solicitudes_main_lista_det.jsp" target="consultaSolDetalle" height="10%" method="post">
        <table width="100%" align="center" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0">
                <tr height="12px">
                  <td width="1%">&nbsp;</td>
                  <td width="19%" class="texto">Oficina</td>
                  <td width="19%" class="texto">Centro</td>
                  <td width="20%" class="texto">Estado</td>
                  <td width="20%" class="texto">Fecha Inicial</td>
                  <td width="20%" class="texto">Fecha Final</td>
                </tr>
                <tr>
                  <td width="1%">&nbsp;</td>
                  <td class= "normal"><div id="divoficina">
                  <Select name="oficina" class="cajatexto2" style="width=90%">
                  <option value='0'>TODOS</option>
<%
           Oficina ofic = new Oficina();
           Vector vo = new Vector();
           vo = ofic.busquedaOfi("","OFICINA_NOMBRE ASC");
           for(int int_pos = 0;int_pos < vo.size(); int_pos++){
                SesOficina sysofic=(SesOficina)vo.elementAt(int_pos);
%>
                   <option value='<%= sysofic.getOficinaPk()%>'><%= sysofic.getOficinaNombre()%></option>
<%
           }
%>
                  </Select></div>
                  </td>
                  <td class= "normal"><div id="divcentro">
                  <Select name="centro" class="cajatexto2" style="width=90%">
                  <option value='0'>TODOS</option>
<%
          Centros c = new Centros();
          Vector vc = new Vector();
          vc = c.busquedaCentro("","");
          for(int int_pos = 0;int_pos < vc.size(); int_pos++){
                Centrosescam centroS=(Centrosescam)vc.elementAt(int_pos);
%>
                   <option value='<%=centroS.getCodcentro()%>'><%=centroS.getDescentro()%></option>
<%
           }
%>
                  </Select></div>
                  </td>
<%
          Calendar Cal_fechaActual = Calendar.getInstance();
%>
                  <td class= "normal"><div id="divestado">
                  <Select name="estado" class="cajatexto2" style="width:80%">
                        <option selected value='0'>TODOS</option>
                        <option value='1'>Solicitadas</option>
                        <option value='2'>Aprobadas</option>
                        <option value='3'>Denegadas</option>
                        <option value='4'>Renunciadas</option>
                  </Select></div>
                  </td>
                  <td class="normal">
                  <INPUT style="width:60%" type="text" class="cajaTexto2" name="fechaini" value ="<%=Utilidades.dateToString(new java.sql.Date(Cal_fechaActual.getTime().getTime()))%>" onKeypress="validarEntradaFecha(this.value);" onBlur="chequeoFechaInicio();">
                                <A HREF="#" NAME='selector1' ID='selector1' onClick="getDate(document.buscasol.fechaini, this.name); return false;">
                                        <IMG src="../../../imagenes/calendar.jpg" border=0 align=middle>
                                </A>
                  </td>
                  <td class="normal">
                  <INPUT style="width:60%" type="text" class="cajaTexto2" name="fechafin" value ="<%=Utilidades.dateToString(new java.sql.Date(Cal_fechaActual.getTime().getTime()))%>" onKeypress="validarEntradaFecha(this.value);" onBlur="chequeoFechaFinal();">
                                <A HREF="#" NAME='selector2' ID='selector2' onClick="getDate(document.buscasol.fechafin, this.name); return false;">
                                        <IMG src="../../../imagenes/calendar.jpg" border=0 align=middle>
                                </A>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
        </table>
<td><input type="hidden" value="cip" name="orden"></td>
<br>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

