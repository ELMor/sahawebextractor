<%@ page contentType="text/html;charset=ISO-8859-15"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="beans.ConsultaEntradasNoLE,isf.negocio.*,isf.persistencia.*,java.util.*,conf.*,isf.util.Utilidades,isf.util.log.Log" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">

<script language="javascript">
    function imprimir(doc,garpk,cip){
         llamado='../nolesp/imprime_doc_nolesp.jsp?documento='+doc+'&garpk='+garpk+'&usuario=<%=accesoBeanId.getUsuario()%>&cip='+cip;
         window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
    }

</script>

</head>

<%
    /* Variables */
    String foco=""; //variable para controlar el foco en la visualizacion
    String nombre_oficina="",estado1="Solicitada",estado2="Denegada",nif,letra="",dni="",nss="";
    long oficina_pk=0;
    ConsultaEntradasNoLE nole = new ConsultaEntradasNoLE();
    EntradaNoLE entnoLE = new EntradaNoLE();
    Vector vn = new Vector();

    /* Recogida de parametros */
    long l_pkgar = new Long(request.getParameter("garpk")).longValue();

    /* Busqueda de la Solicitud NOLESP */
    nole.setGarpk(l_pkgar);
    nole.setTipo(0); //busqueda por garpk
    vn = nole.resultado();
    Log fichero_log = null;
    if (vn.size()>0){

      NegEvento negevent = accesoBeanId.getNegEventoInstance(22);
      try {
        if (negevent != null) {
          Long dato = new Long (l_pkgar);
          negevent.addVar(86,dato.toString());
          negevent.addVar(87,"3");
          negevent.guardar();
        }
      }catch (Exception ex) {
        fichero_log = Log.getInstance();
        fichero_log.warning(" No se ha realizado el evento de consulta de solicitud con pk " + l_pkgar + "  Error -> +" + ex.toString());
      }

      Enumeration e = vn.elements();
      while (e.hasMoreElements()) {
         entnoLE=(EntradaNoLE)e.nextElement();
         oficina_pk = entnoLE.getFOficina();
         nif=entnoLE.getFResp_dni();
         if ((nif!=null) && (nif!="")){
           letra=nif.substring(nif.length()-1);
           dni=nif.substring(0,nif.length()-1);
         }
         nombre_oficina=OficinaHelper.DameOficina(oficina_pk);
         nss=entnoLE.getFNSS();
      }
    }
%>

<BODY class="mainFondoInterno" onLoad="<%=foco%>" >
<form name="solicitud" action="guardarSolicitud.jsp" target="oculto1">
 <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
  <td>
   <FIELDSET >
   <LEGEND><a class="texto"><b>DATOS SOLICITUD</b></a></LEGEND>
    <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
     <tr>
      <td width="80%">
       <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
         <td class= "texto" width="20%">Oficina:</td>
         <td class= "texto"><input type="text" name="Oficina" class="cajatexto2" value="<%=nombre_oficina%>" readonly></td>
         <td class= "texto">Fecha:</td>
         <td class= "texto"><input type="text" name="fecha" class="cajatexto2" value="<%=entnoLE.getFFecha()%>" readonly ></td>
        </tr>
        <tr>
         <td class= "texto">Responsable Legal:</td>
         <td class= "texto" COLSPAN=4><input type="text" name="nombre" class="cajatexto3" value="<%=entnoLE.getFResp_apenom()%>" readonly></td>
        </tr>
        <tr>
         <td class= "texto">N.I.F:</td>
         <td class= "texto"><input maxlength=9 type="text" name="dni"  style="width:35%"class="cajatexto2"  value="<%=dni%>" readonly>&nbsp;
         - <input maxlength=1 type="text" name="letradni" style="width:8%" class="cajatexto2" value="<%=letra%>" readonly></td>
         <td class= "texto">Estado:</td>
         <td class= "texto"><input type="text" name="estado" class="cajatexto2" value="<%=estado1%>" readonly></td>
        </tr>
        <tr>
         <td class= "texto">N.S.S.:</td>
         <td class= "texto"><input type="text" name="nss" class="cajatexto2" value="<%=nss%>" readonly></td>
        </tr>
       </table>
      </td>
      <td align="center"><a onClick="imprimir('DOC01',<%=entnoLE.getFNnolesp()%>,'<%=entnoLE.getFCip()%>');" style=cursor:"hand"><img  alt="Documento de Solicitud" src="../../../imagenes/impresora.gif"></a></td>
     </tr>
    </table>
   </FIELDSET>
  </td>
  <td>&nbsp;</td>
  </tr>
 </table>

 <td>&nbsp;</td>
 <td>&nbsp;</td>

      <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
         <td>
           <FIELDSET id="marco1">
           <LEGEND><a class="texto"><b>DATOS DENEGACION</b></a></LEGEND>
            <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
             <tr>
              <td width="80%">
               <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="c1enter">
                <tr>
                 <td class= "texto">Oficina:</td>
                  <td class= "texto"><input type="text" name="oficina_a" class="cajatexto2" value="<%=nombre_oficina%>" readonly></td>
                  <td class= "texto">Fecha:</td>
                  <td class= "texto"><input type="text" name="fecha_a" class="cajatexto2" value="<%=entnoLE.getFFecha()%>" readonly ></td>
                </tr>
                <tr>
                  <td class= "texto">Responsable Legal:</td>
                  <td class= "texto" COLSPAN=3><input type="text" name="nombre_a" class="cajatexto3" value="<%=entnoLE.getFResp_apenom()%>" readonly></td>
                </tr>
                <tr>
                 <td class= "texto">N.I.F:</td>
                 <td class= "texto"><input maxlength=9 type="text" name="dni" style="width:35%" class="cajatexto2"  value="<%=dni%>" readonly>&nbsp;
                 - <input maxlength=1 type="text" name="letradni" style="width:8%" class="cajatexto2" value="<%=letra%>" readonly></td>
                 <td class= "texto">Estado:</td>
                 <td class= "texto"><input type="text" name="estado_a" class="cajatexto2" value="<%=estado2%>" readonly></td>
                </tr>
                 <tr>
                  <td class= "texto">Motivo de Rechazo:</td>
                  <td class= "texto" COLSPAN=3><textarea name="rechazo_a" maxlength=10000 class="textarea" rows="2" readonly><%=entnoLE.getFMotivo_rechazo()%></textarea></td>
                 </tr>
            </table>
           </td>
           <td align="center"><a onClick="imprimir('DOC03',<%=entnoLE.getFNnolesp()%>,'<%=entnoLE.getFCip()%>');" style=cursor:"hand"><img  alt="Documento de Denegacion" src="../../../imagenes/impresora.gif"></a></td>
          </tr>
        </table>
       </FIELDSET>
      </td>
      <td>&nbsp;</td>
     </tr>
    </table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
