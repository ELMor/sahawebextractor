<%@ page contentType="text/html;charset=ISO-8859-15"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.*,isf.util.Utilidades,isf.util.log.Log" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">

<script language="javascript">
    function imprimir(doc,garpk,operacion){
         cq = this.document.solicitud.gar_cq.value;
         if (cq == "Q") {
           centro=this.document.solicitud.centro.value;
           norden=this.document.solicitud.norden.value;
         } else {
           centro=this.document.solicitud.centro.value;
           norden=this.document.solicitud.ncita.value;
         }

         llamado='../imprime_doc.jsp?centro='+centro+'&norden='+norden+'&cq='+cq+'&documento='+doc+'&garpk='+garpk+'&operacion='+operacion+'&impresion='+'1'+'&usuario=<%=accesoBeanId.getUsuario()%>';
         window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
    }

</script>

</head>

<%
    /* Variables */
    String foco=""; //variable para controlar el foco en la visualizacion
    String nombre_oficina,estado,nif,letra="",dni="",nss;
    long oficina_pk=0;
    Garantia gar = new Garantia();
    SesGarantia sesGar = new SesGarantia();
    GaranTram gT = new GaranTram();
    Vector vg = new Vector();
    Vector vgt = new Vector();

    /* Recogida de parametros */
    long l_pkgar = new Long(request.getParameter("garpk")).longValue();


    /* Busqueda de la Solicitud */
    vg = gar.busquedaGaran("GAR_PK="+l_pkgar,"");
    sesGar = (SesGarantia)vg.elementAt(0);
    Log fichero_log = null;
    if (vg != null) {
     NegEvento negevent = accesoBeanId.getNegEventoInstance(22);
     try {
        if (negevent != null) {
          Long dato = new Long (l_pkgar);
          negevent.addVar(86,dato.toString());
          dato = new Long (sesGar.getGarEstado());
          negevent.addVar(87,dato.toString());

          negevent.guardar();
        }
      }
      catch (Exception ex) {
        fichero_log = Log.getInstance();
        fichero_log.warning(" No se ha realizado el evento de consulta de solicitud con pk " + l_pkgar + "  Error -> +" + ex.toString());
        }
    }
    //Se toma el primer tramo de garantia realizado, que sera siempre la solicitud
    vgt = gT.busquedaGaranTram("GAR_PK="+sesGar.getGarPk(),"GAR_TRAM_PK ASC");
    SesGaranTram sesGT = (SesGaranTram)vgt.elementAt(0);
    if (sesGar.getGarEstado()==1) {
        foco="";
    }else{
        long pos = vgt.size() -  1;
        String marco = "marco"+pos;
        foco="this.document.all."+marco+".focus();";
    }
    oficina_pk = sesGT.getOficinaPk();
    nif=sesGT.getGarReplegDni();
    if ((nif!=null) && (nif!="")){
     letra=nif.substring(nif.length()-1);
     dni=nif.substring(0,nif.length()-1);
    }
    nombre_oficina=OficinaHelper.DameOficina(oficina_pk);
    estado = Constantes.EST_TEXTO[new Long(sesGT.getGarOperacion()).intValue()-1];
    nss=sesGar.getPacNss();
%>

<BODY class="mainFondoInterno" onLoad="<%=foco%>" >
<form name="solicitud" action="guardarSolicitud.jsp" target="oculto1">
 <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
  <td>
   <FIELDSET >
   <LEGEND><a class="texto"><b>DATOS SOLICITUD</b></a></LEGEND>
    <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
     <tr>
      <td width="80%">
       <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
         <td class= "texto" width="20%">Oficina:</td>
         <td class= "texto"><input type="text" name="Oficina" class="cajatexto2" value="<%=nombre_oficina%>" readonly></td>
         <td class= "texto">Fecha:</td>
         <td class= "texto"><input type="text" name="fecha" class="cajatexto2" value="<%=Utilidades.dateToString(sesGT.getGarFechor())%>" readonly ></td>
        </tr>
        <tr>
         <td class= "texto">Responsable Legal:</td>
         <td class= "texto" COLSPAN=4><input type="text" name="nombre" class="cajatexto3" value="<%=sesGT.getGarReplegApenom()%>" readonly></td>
        </tr>
        <tr>
         <td class= "texto">N.I.F:</td>
         <td class= "texto"><input maxlength=9 type="text" name="dni"  style="width:35%"class="cajatexto2"  value="<%=dni%>" readonly>&nbsp;
         - <input maxlength=1 type="text" name="letradni" style="width:8%" class="cajatexto2" value="<%=letra%>" readonly></td>
         <td class= "texto">Estado:</td>
         <td class= "texto"><input type="text" name="estado" class="cajatexto2" value="<%=estado%>" readonly></td>
        </tr>
        <tr>
         <td class= "texto">N.S.S.:</td>
         <td class= "texto"><input type="text" name="nss" class="cajatexto2" value="<%=nss%>" readonly></td>
        </tr>
       </table>
      </td>
      <td align="center"><a onClick="imprimir('DOC01',<%=sesGT.getGarPk()%>,<%=sesGT.getGarOperacion()%>);" style=cursor:"hand"><img  alt="Documento de Solicitud" src="../../../imagenes/impresora.gif"></a></td>
     </tr>
    </table>
   </FIELDSET>
  </td>
  <td>&nbsp;</td>
  </tr>
 </table>

 <td>&nbsp;</td>
 <td>&nbsp;</td>

<%
  int i_cont;
  for (i_cont=1;i_cont<vgt.size();i_cont++){

  //Se toma ahora el ultimo tramo de garantia generado, que sera la aprobacion o la denegacion.
    SesGaranTram sGT = (SesGaranTram)vgt.elementAt(i_cont);
%>

<%
       if (sGT.getGarOperacion()==2){
%>
       <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
         <td>
          <FIELDSET id="marco1">
          <LEGEND><a class="texto"><b>DATOS APROBACION</b></a></LEGEND>
<%
       }else{
          if (sGT.getGarOperacion()==3){
%>

      <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
         <td>
           <FIELDSET id="marco1">
           <LEGEND><a class="texto"><b>DATOS DENEGACION</b></a></LEGEND>
<%
         }else{
%>
       <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
          <td>
             <FIELDSET id="marco2">
             <LEGEND><a class="texto"><b>DATOS RENUNCIA</b></a></LEGEND>
<%
         }
       }
%>
       <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
         <tr>
          <td width="80%">
            <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="c1enter">
<%
            oficina_pk = sGT.getOficinaPk();
            nombre_oficina=OficinaHelper.DameOficina(oficina_pk);
            estado = Constantes.EST_TEXTO[new Long(sGT.getGarOperacion()).intValue()-1];
            nif=sGT.getGarReplegDni();
            if ((nif != null) && (!nif.equals(""))) {
               letra=nif.substring(nif.length()-1);
               dni=nif.substring(0,nif.length()-1);
            }
%>

            <tr>
             <td class= "texto">Oficina:</td>
             <td class= "texto"><input type="text" name="oficina_a" class="cajatexto2" value="<%=nombre_oficina%>" readonly></td>
             <td class= "texto">Fecha:</td>
             <td class= "texto"><input type="text" name="fecha_a" class="cajatexto2" value="<%=Utilidades.dateToString(sGT.getGarFechor())%>" readonly ></td>
            </tr>
            <tr>
             <td class= "texto">Responsable Legal:</td>
             <td class= "texto" COLSPAN=3><input type="text" name="nombre_a" class="cajatexto3" value="<%=sGT.getGarReplegApenom()%>" readonly></td>
            </tr>
            <tr>
             <td class= "texto">N.I.F:</td>
             <td class= "texto"><input maxlength=9 type="text" name="dni" style="width:35%" class="cajatexto2"  value="<%=dni%>" readonly>&nbsp;
             - <input maxlength=1 type="text" name="letradni" style="width:8%" class="cajatexto2" value="<%=letra%>" readonly></td>
             <td class= "texto">Estado:</td>
             <td class= "texto"><input type="text" name="estado_a" class="cajatexto2" value="<%=estado%>" readonly></td>
            </tr>
<%
            if (sGT.getGarOperacion()==2){
%>
            <tr>
             <td class= "texto">Precio:</td>
             <td class= "texto"><input type="text" name="monto_a" class="cajatexto4" style="width:30%" value="<%=sesGar.getGarMonto()%>" readonly>&nbsp;<img align="middle" src="../../../imagenes/euro.gif"></td>
            </tr>
<%
            }else{
               if (sGT.getGarOperacion()==3){
%>
                 <tr>
                  <td class= "texto">Motivo de Rechazo:</td>
                  <td class= "texto" COLSPAN=3><textarea name="rechazo_a" maxlength=10000 class="textarea" rows="2" readonly><%=sesGar.getGarMotivoRech()%></textarea></td>
                 </tr>
<%
               }
            }
%>
            </table>
           </td>
<%
           // Dependiendo de la visualizacion de la Aprobacion, Denegacion y/o Renuncia se imprime uno u otro documento.
           if (sGT.getGarOperacion()==2){
%>
             <td align="center"><a onClick="imprimir('DOC02',<%=sGT.getGarPk()%>,<%=sGT.getGarOperacion()%>);" style=cursor:"hand"><img  alt="Documento de Aprobacion" src="../../../imagenes/impresora.gif"></a></td>
<%
          }else{
             if (sGT.getGarOperacion()==3){
%>
                <td align="center"><a onClick="imprimir('DOC03',<%=sGT.getGarPk()%>,<%=sGT.getGarOperacion()%>);" style=cursor:"hand"><img  alt="Documento de Denegacion" src="../../../imagenes/impresora.gif"></a></td>
<%
             }else{
%>
                <td align="center"><a onClick="imprimir('DOC04',<%=sGT.getGarPk()%>,<%=sGT.getGarOperacion()%>);" style=cursor:"hand"><img  alt="Documento de Renuncia" src="../../../imagenes/impresora.gif"></a></td>
<%
             }
          }
%>
         </tr>
        </table>
       </FIELDSET>
      </td>
      <td>&nbsp;</td>
     </tr>
    </table>
<%
 } //for
 if (sesGar.getGarCq().equals("Q")){
%>
   <td><input type="hidden" name="centro" value="<%=new Double(sesGar.getCentro()).longValue()%>"></td>
<%
 }else{
%>
   <td><input type="hidden" name="centro" value="<%=new Double(sesGar.getCega()).longValue()%>"></td>
<%
 }
%>
   <td><input type="hidden" name="norden" value="<%=new Double(sesGar.getNorden()).longValue()%>"></td>
   <td><input type="hidden" name="ncita" value="<%=new Double(sesGar.getNcita()).longValue()%>"></td>
   <td><input type="hidden" name="gar_cq" value=<%=sesGar.getGarCq()%>></td>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
