<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<script language="javascript" type="text/javascript" src="../../../javascript/validarNif.js"></script>
</head>
<BODY class="mainFondoInterno">
<form name="solicitud" action="guardarSolicitud.jsp" target="oculto1">
<a><b><u>FICHA NUEVA SOLICITUD</u></b></a>
<br>
<br>
<FIELDSET >
<LEGEND><a class="texto"><b>RESPONSABLE LEGAL</b></a></LEGEND>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
   <tr>
       <td class= "texto">Nombre:</td>
       <td class= "texto"><input type="text" name="nombre" class="cajatexto3" maxlength = 255 value=""></td>
   </tr>
   <tr>
       <td class= "texto">N.I.F:</td>
       <td class= "texto"><input maxlength=8 type="text" name="dni" class="cajatexto2" style="width:12%" value="" onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;" onBlur="CalculaNIF(this.value);">
        - <input maxlength=1 type="text" name="letradni" style="width:3%" class="cajatexto2" value="" readonly></td>
   </tr>
</table>
<br>
</FIELDSET>
<br>
<FIELDSET >
<LEGEND><a class="texto"><b>PACIENTE</b></a></LEGEND>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
   <tr>
       <td class= "texto">N.S.S:</td>
       <td class= "texto"><input align="right" type="text" name="nss" class="cajatexto2" maxlength=12 value="" onkeypress = "if ((event.keyCode < 48) || (event.keyCode > 57)) event.returnValue = false;"></td>
   </tr>
</table>
<br>
</FIELDSET>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
