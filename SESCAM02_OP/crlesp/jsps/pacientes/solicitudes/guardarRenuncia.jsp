<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<%
                //System.out.println("Entro en guardarSolicitud");

                /* Variables */
                long l_centro,l_norden= 0;
                String s_garcq;
                boolean imprimir=false;

                /* Recojida de parametros */
                s_garcq = new String((String)beanParamListaSolic.get("GAR_CQ").toString());
                l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();

                /* Diferenciacion de registro de LESP o LESPCEX */
                if (s_garcq.equals("Q")) {
                  l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
                } else {
                  l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
                }
                /* Diferenciacion de registro de LESP o LESPCEX */
                String str_nombre = request.getParameter("nombre");
                String str_dni = request.getParameter("dni");
                String str_letra = request.getParameter("letradni");
                String str_nif = str_dni+str_letra;
                /* Fin recogida de parametros */
                Garantia gar = new Garantia();
                long garpk= new Long(request.getParameter("garpk")).longValue();

                Vector v = new Vector();
                SesGarantia sG = new SesGarantia();
                v = gar.busquedaGaran("GAR_PK="+garpk,"");
                sG = (SesGarantia)v.elementAt(0);

                /* Insercion de la renuncia */
                try {
                  sG.setGarTipo(1);
                  sG.setGarEstado(4);  //Estado 4: Renunciada

                  try{
                   gar.SetNegEvento(accesoBeanId.getNegEventoTramInstance(18),73,"DOC04",l_centro,l_norden);
                   gar.modificarGaran(sG,4,accesoBeanId.getUsuario(),str_nombre,str_nif);
                  }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en guardarRenuncia.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                  }
                  imprimir=true;
                }catch (Exception e){
                  System.out.println(" Error " +e+ "al generar la renuncia");
                  imprimir=false;
                }
%>
<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript">
    var doc="DOC04";
    function ImprimirDoc(){
      //llamado='../imprime_doc.jsp?centro=<%=l_centro%>&norden=<%=l_norden%>&cq=<%=s_garcq%>&documento='+doc+'&usuario=<%=accesoBeanId.getUsuario()%>';
      llamado='../imprime_doc.jsp?centro=<%=l_centro%>&norden=<%=l_norden%>&cq=<%=s_garcq%>&documento='+doc+'&garpk=<%=sG.getGarPk()%>&operacion=4&usuario=<%=accesoBeanId.getUsuario()%>';
      window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
    }
</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
  /* Si la insercion ha sido correcta imprimir documento */
  if (imprimir){
%>
       <script language="javascript">
          ImprimirDoc();
          parent.centro.location.href = "lista_solicitudes_main.jsp"
          parent.botonera.location.href = "lista_solicitudes_down.jsp"
       </script>
<%
    }
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>