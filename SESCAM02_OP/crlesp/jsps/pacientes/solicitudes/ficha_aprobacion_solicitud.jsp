<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<script language="javascript" type="text/javascript" src="../../../javascript/validarDecimal.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/validarNif.js"></script>

<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<%
       /***************************/
       /* Definicion de variables */
       /***************************/
       long l_garpk,l_centro,l_norden;
       double d_monto;
       String gar_cq;

       /**************************/
       /* Recogida de Parametros */
       /**************************/
       long gar_pk= new Long(request.getParameter("garpk")).longValue();
       l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
       gar_cq = new String ((String)beanParamListaSolic.get("GAR_CQ").toString());
       if (gar_cq.equals("Q")) {
         l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
       }else{
         l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
       }

       /****************************/

       try {
         Traduccion t = new Traduccion();
         TraduccionProc tP = new TraduccionProc();
         t.setCentro(l_centro);
         t.setNorden(l_norden);
         t.setGar_cq(gar_cq);
         tP = t.ConsultaMontoLE();
         d_monto = tP.monto;
       }catch(Exception e){
        d_monto = 0;
        System.out.println("Error al calcular monto procedimiento:"+ e);
       }
%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
</head>
<BODY class="mainFondoInterno">
<form name="aprobacion" action="guardarAprobacion.jsp" target="oculto1">
<a class="texto"><b><u>FICHA APROBACION</u></b></a>
<br>
<br>
<FIELDSET >
<LEGEND><a class="texto"><b>RESPONSABLE LEGAL</b></a></LEGEND>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
   <tr>
       <td width="10%" class= "texto">Nombre :<td>
       <td class= "texto"><input type="text" name="nombre" class="cajatexto3" maxlength = 255 value=""></td>
   </tr>
   <tr>
       <td class= "texto">N.I.F:<td>
       <td class= "texto"><input maxlength=8 type="text" name="dni" class="cajatexto2" style="width:12%" value="" onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;" onBlur="CalculaNIF(this.value);">
        - <input maxlength=1 type="text" name="letradni" style="width:3%" class="cajatexto2" value="" readonly></td>
   </tr>
</table>
<br>
</FIELDSET>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
   <tr>
       <td width="10%" class="texto">Precio:<td>
       <td class= "texto"><input onClick="this.value=''" maxlength=20 type="text" name="monto" class="cajatexto4" value= '<%=d_monto%>' onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;" onBlur = "validaDatoDecimal(this.value,13,2);">&nbsp;<img align="mid2le" src="../../../imagenes/euro.gif"></td>
   </tr>
</table>
<td><input type="hidden" name="garpk" value="<%=gar_pk%>"></td>
<br>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
