<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>

<%@ page import="isf.negocio.ContinuosDet" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaContinuos"%>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
</head>
<body class="mainFondoInterno" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
  <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentralInterno" >
	<table id="entradasDet" class="centroTabla" cellspacing="0" cellpadding="0" border="0">
  <%
  ConsultaContinuos CC=new ConsultaContinuos();
  ContinuosDet CDet;
  boolean bFilaPar=true;
  int idCounter=0;
  Vector vp;
  if (request.getParameter("centro")!=null && (!request.getParameter("centro").equals(""))){ //si no hay parametros no hace nada.
    CC.setCentro(request.getParameter("centro"));
    CC.setNorden(request.getParameter("norden"));
    CC.setCq(request.getParameter("gar_cq"));
    //CC.SetNegEvento(accesoBeanId.getNegEventoInstance(21));
    vp=CC.resultado();
    if ((vp!=null)){
      Enumeration e = vp.elements();
      while (e.hasMoreElements()) {
	CDet=(ContinuosDet)e.nextElement();
	if (bFilaPar){
          bFilaPar=false;
 %>
         <tr class="filaPar" id=<%=idCounter%> >
 <%	 }else{
         bFilaPar=true;
 %>
         <tr class="filaImpar" id=<%=idCounter%> >
 <%
	 }
 %>
           <td width="13%"><%=CDet.getClave()%></td>
           <td width="20%"><%=CDet.getFecha_inicio()%></td>
           <td width="20%"><%=CDet.getFecha_fin()%></td>
           <td width="10%"><%=CDet.getSituacion()%></td>
           <td width="17%"><%=CDet.getDias_acumulados()%></td>
         </tr>
<%
         }
	 idCounter++;
    }else{
%>
      <script language="javascript">
       alert('Error al realizar la consulta.No se puede acceder a la informacion.');
      </script>
<%
    }
  }
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
<!-- DETALLE FIN-->
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
