<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page import="beans.ConsultaEntradasLEHisto,isf.negocio.EntradaLEHisto,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="beanEntradaHisto" scope="session" class="java.util.HashMap"/>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
<script language="javascript">
var bMuestraHist=0;
function muestraHistorico(){
// Valores posibles:
// 0:valores actuales
// 1:valores historicos y actuales
   var strLlamada="";
// Las siguientes lineas controlan si existen frames cambiando
   if (((parent.frames("main").frames("datos").document.readyState)!="complete")||
       (parent.frames("main").frames("datos_tabs").frames("detalle_dat").document.readyState!="complete")||
       (parent.frames("main").frames("datos_tabs").frames("solicitud_index").frames("centro").document.readyState!="complete")||
       (parent.frames("main").frames("datos_tabs").frames("solicitud_index").frames("botonera").document.readyState!="complete"))
   {
     alert("Todav�a no se han cargado todas las fichas.");
     return;
   }
   strLlamada="ficha_paciente_main_datos.jsp?cip="
   strLlamada+=escape('<%=request.getParameter("cip")%>')
   strLlamada+='&centro=<%=request.getParameter("centro")%>';
   strLlamada+='&norden=<%=request.getParameter("norden")%>';
   strLlamada+='&gar_cq=<%=request.getParameter("gar_cq")%>';
  if (bMuestraHist==0){
    bMuestraHist=1;
    document.all.img_historico.src="../../imagenes/fichero_hist.gif"
    document.all.img_historico.alt="Activos e historicos"
    strLlamada+='&historico=1';
    parent.frames("main").frames("datos").location=strLlamada;
  }
  else{
    bMuestraHist=0;
    document.all.img_historico.src="../../imagenes/historia.gif"
    document.all.img_historico.alt="Solo activos"
    strLlamada+='&historico=0';
    parent.frames("main").frames("datos").location=strLlamada;
    }
}

function muestraHistoricoPasivo(){
// Valores posibles:
// 0:valores actuales
// 1:valores historicos y actuales
   var strLlamada="";
// Las siguientes lineas controlan si existen frames cambiando
   if (((parent.frames("main").frames("datos").document.readyState)!="complete")||
       (parent.frames("main").frames("datos_tabs").frames("detalle_dat").document.readyState!="complete")||
       (parent.frames("main").frames("datos_tabs").frames("solicitud_index").frames("centro").document.readyState!="complete")||
       (parent.frames("main").frames("datos_tabs").frames("solicitud_index").frames("botonera").document.readyState!="complete"))
   {
     alert("Todav�a no se han cargado todas las fichas.");
     return;
   }
   strLlamada="./pasivo/ficha_historico_pasivo_index.jsp?cip=";
   strLlamada+=escape('<%=request.getParameter("cip")%>');
   window.showModalDialog(strLlamada,"","dialogWidth:40em;dialogHeight:25em");	
}

</script>

</head>
<body class="top" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<table  class="textoTop" width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<%
	boolean tiene_pasivo=false;
	boolean historico_seleccionado=false; 
	ConsultaEntradasLEHisto enthisto=null;
	if (request.getParameter("historico").equals("1")){
		historico_seleccionado=true;
	}
	Vector v = new Vector();
	if (historico_seleccionado){
		enthisto = new ConsultaEntradasLEHisto();
		enthisto.setCip(request.getParameter("cip"));
		v = enthisto.resultado(); 
		if (v.size()>0){
		    tiene_pasivo=true;
		    for (int i=0;i<v.size();i++){
		    	String entrada = "entrada"+i;
		    	beanEntradaHisto.put(entrada,(EntradaLEHisto)v.get(i));
		    }
		}
	}
	if (tiene_pasivo) {	   
%>
  <tr>
    <td width="5%" align="left" height="50%">
    	<img  src="../../imagenes/flechavolver.gif" width="14px" height="15px" border="0" style="cursor:hand" alt="Volver" onclick="history.back();">
    </td>
    <td width="3%" align="left" height="50%">
  	    <img id="img_historico" name="img_historico" src="../../imagenes/historia.gif" border="0" style="cursor:hand" alt="Solo activos" onclick="muestraHistorico();">
    </td>
    <td width="3%" align="left" height="100%">
    	<img id="img_historico_pasivo" name="img_historico_pasivo" src="../../imagenes/tablon.gif" border="0" style="cursor:hand" alt="Historico Pasivo" onclick="muestraHistoricoPasivo();">     
    </td>
    <td width="89%" align="right" height="100%">>> Consulta de pacientes >> Ficha Paciente</td>
  </tr>
<%
	}else{
%>
  <tr>
    <td width="5%" align="left" height="50%">
    	<img  src="../../imagenes/flechavolver.gif" width="14px" height="15px" border="0" style="cursor:hand" alt="Volver" onclick="history.back();">
    </td>
    <td width="3%" align="left" height="50%">
    	<img id="img_historico" name="img_historico" src="../../imagenes/historia.gif" border="0" style="cursor:hand" alt="Solo activos" onclick="muestraHistorico();">
    </td>
    <td width="92%" align="right" height="100%">>> Consulta de pacientes >> Ficha Paciente</td>
  </tr>
<%
	}
%>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
