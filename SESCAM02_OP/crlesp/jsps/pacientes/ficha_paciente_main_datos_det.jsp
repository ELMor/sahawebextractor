<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>

<%@ page import="isf.negocio.EntradaLEDet" %>
<%@ page import="isf.negocio.MotSalida" %>
<%@ page import="isf.negocio.ServMapeados" %>
<%@ page import="isf.persistencia.SesMotSalida" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaEntradasLEDet"%>
<%@ page import="beans.ConsultaServMapeados"%>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
</head>
<body class="mainFondoInterno" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<!-- DETALLE INI-->
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentralInterno" >
	   <table id="entradasDet" class="centroTabla" cellspacing="0" cellpadding="0" border="0">
  <%
  ConsultaEntradasLEDet cpb=new ConsultaEntradasLEDet();
  EntradaLEDet entLEDet;
  boolean bFilaPar=true;
  int idCounter=0;
  Vector vp;
  if (request.getParameter("centro")!=null && (!request.getParameter("centro").equals(""))){ //si no hay parametros no hace nada.
    String centro = request.getParameter("centro");
    cpb.setCentro(centro);
    cpb.setNOrden(request.getParameter("norden"));
    cpb.setCQ(request.getParameter("gar_cq"));
    cpb.SetNegEvento(accesoBeanId.getNegEventoInstance(21));

    vp=cpb.resultado();
    if (vp!=null){
      Enumeration e = vp.elements();
      while (e.hasMoreElements()) {
	entLEDet=(EntradaLEDet)e.nextElement();
	if (bFilaPar){
          bFilaPar=false;
 %>
         <tr class="filaPar" id=<%=idCounter%>>
 <%	 }else{
         bFilaPar=true;
 %>
         <tr class="filaImpar" id=<%=idCounter%>>
 <%
	 }
/*
         Vector v = new Vector();
         MotSalida ms = new MotSalida();
         int mot = entLEDet.fMotivo;
         String desc_motivo;
         if (mot!=0){
           v = ms.busquedaMotSalida("MOTSAL_COD="+mot,"");
           SesMotSalida sms = (SesMotSalida) v.elementAt(0);
           desc_motivo = sms.getMotsalDesc();
         }else{
           desc_motivo = "Sin motivo salida.Permanece en Lista de Espera.";
         }
*/
         String seccion = entLEDet.getfSecc(); 
         ConsultaServMapeados csm = new ConsultaServMapeados();
         csm.setCod_mapeo(seccion);
         csm.setCentro(new Long(centro).longValue());
         //System.out.println("Servicio:"+seccion);
         //System.out.println("Centro:"+centro);
         Vector servicios = csm.ServiciosMapeados();
         String descseccion = "Servicio no mapeado";
         if (servicios.size()>0){
         	ServMapeados s = (ServMapeados)servicios.get(0);
         	seccion = s.CodServSESCAM;
         	descseccion = s.DescServSESCAM;
         }else{
            seccion = seccion +" - NOMAP";
         }
         //System.out.println("Servicio visualizado:"+seccion);

         if (cpb.getCQ().equals("Q")){
%>
           <td width="6%"><%=entLEDet.fOrden%></td>
           <td width="22%"><%=entLEDet.getfEntrada()%></td>
           <td width="20%"><%=entLEDet.getfFDemora()%></td>
           <td width="20%"><%=entLEDet.getSfTDemora()%></td>
           <td width="20%"><%=entLEDet.getfSalida()%></td>
           <td width="10%"><%=entLEDet.getfMotivo()%></a></td>
         </tr>
<%
         }else{
%>
           <td width="5%"><%=entLEDet.fOrden%></td>
           <td width="13%"><%=entLEDet.getfEntrada()%></td>
           <td width="20%"><%=entLEDet.fFCita%></td>
           <td width="8%"><%=entLEDet.getfFDemora()%></td>
           <td width="15%"><%=entLEDet.getSfTDemora()%></td>
           <td width="13%"><%=entLEDet.getfSalida()%></td>
           <td width="6%"><%=entLEDet.getfMotivo()%></td>
           <td width="21%"><a title="<%=descseccion%>"><%=seccion%></td>
         </tr>
<%
         }
	 idCounter++;
      }
    }else{
%>
      <script language="javascript">
       alert('Error al realizar la consulta.No se puede acceder a la informacion.');
      </script>
<%
    }
  }
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
<!-- DETALLE FIN-->
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
