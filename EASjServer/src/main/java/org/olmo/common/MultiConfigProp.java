package org.olmo.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

public class MultiConfigProp {
	Properties props;
	Hashtable<String, String> sels=new Hashtable<String, String>();
	public MultiConfigProp(InputStream is){
		props=new Properties();
		try {
			props.load(is);
		} catch (IOException e) {
			props=null;
			e.printStackTrace();
		}
	}
	public MultiConfigProp(File f) throws IOException {
		this(new FileInputStream(f));
	}
	public MultiConfigProp(Properties p){
		props=p;
	}
	public void addSelector(String name){
		String value=props.getProperty(name+".active","");
		sels.put(name, value);
	}
	public int getInt(String v){
		return Integer.parseInt(get(v));
	}
	public boolean getBoolean(String v){
		String s=get(v);
		if(s!=null)
			return s.equals("true") || s.equals("1");
		return false;
	}
	public String get(String v){
		for(Enumeration<String> sel=sels.keys();sel.hasMoreElements();){
			String s=sel.nextElement();
			if(v.startsWith(s)){
				String realKey=s+"."+sels.get(s)+v.substring(s.length());
				return props.getProperty(realKey);
			}
		}
		return props.getProperty(v);
	}
	public Vector<String> getList(String value){
		String sz=get(value+".size");
		int n=Integer.parseInt(sz);
		Vector<String> ret=new Vector<String>();
		for(int i=1;i<=n;i++){
			ret.add(get(value+"."+i));
		}
		return ret;
	}
	public Hashtable<String, String> get(String k, String n, String v){
		Hashtable<String, String> ret=new Hashtable<String, String>();
		int size=Integer.parseInt(get(k+".size"));
		for(int i=1;i<=size;i++){
			String h=k+"."+i+".";
			String key=get(h+n);
			String val=get(h+v);
			ret.put(key, val);
		}
		return ret;
	}
}
