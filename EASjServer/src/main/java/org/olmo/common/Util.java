package org.olmo.common;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Util {
	/**
	 * Retira el prefijo de un nodo tag
	 * Devuelve bbbb si se env�a aaa:bbbb
	 * @param k
	 * @return
	 */
	public static String rp(String k){
		int ndk=k.indexOf(":");
		if(ndk==-1)
			return k;
		return k.substring(1+ndk); 
	}

	public static String toHexString(byte bytes[]){
		StringBuffer sb=new StringBuffer();
		sb.append("\n");
		for(int i=0;i<bytes.length;i++){
			if(i>0 && i%32==0){
				sb.append(":");
				sb.append(new String(bytes,i-32,32));
				sb.append("\n");
			}
			if(bytes[i]<16)
				sb.append("0");
			sb.append(Long.toString((int) bytes[i] & 0xff, 16));
		}
		return sb.toString();
	}
	
    public static String asHex(byte buf[])
    {
            StringBuffer strbuf = new StringBuffer(buf.length * 2);

            for(int i=0; i< buf.length; i++)
            {
                    if(((int) buf[i] & 0xff) < 0x10)
                            strbuf.append("0");
                    strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
            }
            return strbuf.toString();
    }
	public static String readFile(String f){
		String ret=null;
		try {
			InputStream fis=Class.class.getResourceAsStream(f);
			byte  buff[]=new byte[1024];
			int readed=0;
			ret = "";
			while((readed=fis.read(buff))>=0){
				ret+=new String(buff,0,readed);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public static void removeSingleFiles(File dir) {
		//Borrar ficheros anteriores
		File files[]=dir.listFiles();
		for(int i=0;i<files.length;i++){
			if(!files[i].isDirectory())
				files[i].delete();
		}
	}

	public static byte[] inputStreamToByteArray(InputStream is) throws IOException{
		byte buffer[]=new byte[1024];
		int len=0;
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		while((len=is.read(buffer))!=-1)
			baos.write(buffer, 0, len);
		return baos.toByteArray();
	}

	public static String gen64Char(){
		String ret="";
		for(int i=0;i<16;i++)
			ret+=String.format("%04x", (int)(Math.random()*0xffff));
		return ret;
	}

	public static Map<String, String> getQueryMap(String query) {
		Map<String, String> map = new HashMap<String, String>();
		if (query == null)
			return map;
		String[] params = query.split("&");
		for (String param : params) {
			String name = param.split("=")[0];
			String value = param.split("=")[1];
			map.put(name, value);
		}
		return map;
	}


}
