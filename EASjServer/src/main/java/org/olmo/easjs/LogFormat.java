package org.olmo.easjs;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormat extends Formatter {
	public String format(LogRecord record) {
		String ret="";
		ret+=""+record.getMillis();
		ret+=":";
		ret+=record.getLevel().getName();
		ret+=":";
		ret+=record.getSourceMethodName();
		ret+=":";
		ret+=record.getMessage();
		ret+="\n";
		return ret;
	}

}
