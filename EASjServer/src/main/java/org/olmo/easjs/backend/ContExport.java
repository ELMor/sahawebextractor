package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.server.object.SyncObject;

public class ContExport extends DiffState{
	
	int restrict;
	int trunc;
	String fid;
	ContImport contImport;
	
	/**
	 * Numero del paso en el procesamiento synchronize
	 */
	int step = 0;

	/**
	 * Crea un Exporter de Contenidos
	 * @param b  Backend al que est� conectado
	 * @param folderId Id del Folder que se quiere Exportar
	 * @param gs Storage
	 * @param imp Importer que se quiere empear para sincronizar
	 * @param type Tipo del folder
	 * @param restric
	 * @param st Estado actual conocido del Folder
	 * @param flg Flags
	 * @param trunCod Codigo de truncamiento de mail.
	 */
	public ContExport(Backend b, 
				String folderId, 
				ContImport imp, 
				String type, 
				int restric, 
				Vector<SyncObject> st, 
				int flg, 
				int trunCod) {
		super(b);
		this.fid = folderId;
		this.restrict = restric;
		this.syncstate = st;
		this.flags = flg;
		this.trunc = trunCod;
		this.contImport = imp;
		this.changes = new Vector<SyncObject>();
		int cutoffdate = getCutOffDate(restrict);
		// Get the changes since the last sync
		if (syncstate == null)
			syncstate = new Vector<SyncObject>();
		/*
		 * Se comprueba esto por si hay metodos menos costosos en recursos que estar
		 * continuamente inquiriendo al servidor. 
		 */
		if (type == null && flags == BACKEND_DISCARD_DATA && backend.alterPing()) {
			changes = backend.alterPingChanges(folderId, syncstate, cutoffdate);
		} else {
		/*
		 * Vamos con el m�todo tradicional
		 */
			Vector<SyncObject> msglist = backend.objectList(fid, cutoffdate);
			if (msglist == null)
				return;
			changes = GetDiff(syncstate, msglist);
		}
	}

	public int getChangeCount() {
		return changes != null ? changes.size() : 0;
	}

	public void setContImport(ContImport imp) {
		contImport = imp;
	}

	public boolean synchronize() {
		boolean mustExec = false;
		if (step < changes.size()) {
			SyncObject c = changes.get(step);
			if (c.type.equals("change")) {
				int truncsize = getTruncSize(trunc);
				SyncObject stat = backend.objectStat(fid, c.id);
				SyncObject message = backend.objectGet(fid, c.id, truncsize, true);
				message.flags = c.flags != null ? c.flags : "0";
				if (stat != null && message != null) {
					if (contImport != null)
						mustExec = contImport.change(c.id, message) != null;
					if (0 != (flags & BACKEND_DISCARD_DATA) || mustExec) 
						updateState("change", stat);
				}
			} else if (c.type.equals("delete")) {
				if (contImport != null)
					mustExec = contImport.delete(c.id);
				if (0 != (flags & BACKEND_DISCARD_DATA) || mustExec) {
					updateState("delete", c);
				}
			} else if (c.type.equals("flags")) {
				if (contImport != null)
					mustExec = contImport.readFlag(c.id, c.flags);
				if (0 != (flags & BACKEND_DISCARD_DATA) || mustExec) {
					updateState("flags", c);
				}
			} else if (c.type.equals("move")) {
				if (contImport != null)
					mustExec = contImport.move(c.id,
							c.parentid) != null;
				if (0 != (flags & BACKEND_DISCARD_DATA) || mustExec) {
					updateState("move", c);
				}
			}
			step++;
			return true;
		} else {
			return false;
		}
	}

}
