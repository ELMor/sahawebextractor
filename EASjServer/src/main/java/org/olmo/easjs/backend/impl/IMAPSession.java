package org.olmo.easjs.backend.impl;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.olmo.easjs.backend.impl.google.GmailAuthenticator;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.IMAPResponse;

public class IMAPSession {
	String imapUser,imapPassword;
	String imapSrv, imapProto;
	String smtpProto, smtpSrv, smtpUser, smtpPassword, smtpPort;
	boolean smtpTLS, smtpAuth;
	Logger log=Logger.getLogger("org.olmo.easjs.backend.impl");
	
	protected Session session;
	private Store store;
	private Hashtable<String, IMAPFolder> openFolders=new Hashtable<String, IMAPFolder>();
	IMAPFolder root=null;
	int mode=0;
	char pathSep=0;
	public Hashtable<String, String> folderTypes=new Hashtable<String, String>();

	
	protected IMAPSession(boolean write,
			String imapUsr, String imapPwd, String imapSrv, String imapProt,
			String smtpUsr, String smtpPwd, String smtpSrv, String smtpProt, 
			String smtpPort, boolean smtpTLS, boolean smtpAuth){
		mode=write ? Folder.READ_WRITE : Folder.READ_ONLY;
		setIMAP(imapUsr, imapPwd, imapSrv, imapProt);
		setSMTP(smtpProt, smtpTLS, smtpSrv, smtpUsr, smtpPwd, smtpPort, smtpAuth);
		if(!logon()){
			throw new RuntimeException("Cannot connect to IMAP store");
		}
	}
	
	public void close(){
		for(Enumeration<IMAPFolder> fs=openFolders.elements();
			fs.hasMoreElements();){
			IMAPFolder f=fs.nextElement();
			if(f.isOpen())
				try {
					f.close(mode==Folder.READ_WRITE ? true : false);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
		}
		try {
			store.close();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	protected IMAPFolder open(String fid){
		IMAPFolder ret=null;
		fid=fid.replace('.', pathSep);
		ret=openFolders.get(fid);
		if(ret==null){
			try {
				ret=(IMAPFolder)store.getFolder(fid);
				ret.open(mode);
				openFolders.put(fid, ret);
				if(pathSep==0)
					pathSep=ret.getSeparator();
			} catch (MessagingException e) {
				e.printStackTrace();
				ret=null;
			}
		}
		return ret;
	}
	
	private void setIMAP(String use, String pas,String srv, String protocol){
		imapUser=use;
		imapPassword=pas;
		imapSrv = srv;
		//Puerde ser imap o imaps
		imapProto = (protocol==null ? "imap" : protocol);
	}

	private void setSMTP(String prot, boolean tls, String host, String usr, String pwd, String port, boolean auth){
		//Puede ser smtp o smtps al menos
		smtpProto=prot;
		smtpTLS=tls;
		smtpSrv=host;
		smtpUser=usr;
		smtpPassword=pwd;
		smtpPort=port;
		smtpAuth=auth;
	}
	
	private boolean logon() {
		Properties props=new Properties();
		if (session == null) {
			props.put("mail.user", imapUser);
			props.put("mail.store.protocol", imapProto);

			props.put("mail.transport.protocol", smtpProto);
			if(smtpTLS)
				props.put("mail.smtp.starttls.enable","true");
			props.put("mail.smtp.host", smtpSrv);
			props.put("mail.smtp.user", smtpUser);
			props.put("mail.smtp.from", imapUser);
			props.put("mail.smtp.password", smtpPassword);
			if(smtpPort!=null)
				props.put("mail.smtp.port", smtpPort);
			if(smtpAuth)
				props.put("mail.smtp.auth", "true");
			//props.put("mail.smtps.quitwait", "false");
			
			session = Session.getInstance(props, new GmailAuthenticator(imapUser,imapPassword));
		}
		try {
			store = session.getStore("imaps");
			store.connect(imapSrv, imapUser, imapPassword);
			root=(IMAPFolder)store.getDefaultFolder();
			//Gmail soporta XLIST
			xlistFolders(root);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	// * XLIST (\HasNoChildren \AllMail) "/" "[Gmail]/Todos"
	private void xlistFolders(IMAPFolder f) {
		boolean stat=session.getDebug();
		session.setDebug(true);
		try {
			Object val = f.doCommand(new IMAPFolder.ProtocolCommand() {
				public Object doCommand(IMAPProtocol p)
						throws ProtocolException {
				    // Issue command
				    Response[] r = p.command("xlist \"\" \"%/%\"", null);
				    Response response = r[r.length-1];
				    // Grab response
				    if (response.isOK()) { // command succesful 
						for (int i = 0, len = r.length; i < len; i++) {
						    if (!(r[i] instanceof IMAPResponse))
							continue;
						    IMAPResponse ir = (IMAPResponse)r[i];
						    if (ir.keyEquals("XLIST")) {
								String[] rest=ir.getRest().split("\\s");
								r[i] = null;
								String key=null,value=null;
								for(int j=0;j<rest.length;j++){
									String tok=rest[j];
									if(tok.toLowerCase().contains("children"))
										continue;
									if(tok.equals("\"/\""))
										continue;
									if(tok.startsWith("\\")){
										value=tok.substring(1);
										value=value.replace(")", "");
									}
									if(tok.startsWith("\""))
										key=tok.substring(1,tok.length()-1);
									if(key!=null && value!=null){
										folderTypes.put(key,value);
										log.info("XList: "+key+"="+value);
										break;
									}
								}
						    }
						}
				    }

				    // dispatch remaining untagged responses
				    p.notifyResponseHandlers(r);
				    p.handleResult(response);

				    return null;
				}
			 });
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		session.setDebug(stat);
	}

	public boolean Logoff(){
		try {
			for(Enumeration<IMAPFolder> f=openFolders.elements();
			    f.hasMoreElements();){
				f.nextElement().close(true);
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {
			try {
				store.close();
			}catch(MessagingException me){
				
			}
		}
		return true;
	}

}
