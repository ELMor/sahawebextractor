package org.olmo.easjs.backend;

import org.olmo.easjs.Cons;
import org.olmo.easjs.server.object.SyncObject;

public interface _IObjectSetter extends Cons {
	/**
	 * Crea o Modifica un Folder
	 * @param parent Padre de oldid
	 * @param oldid Folder a modificar (null si se va a crear)
	 * @param newid Folder destino (nuevo nombre del folder)
	 * @return folderStat sobre el nuvo folder.
	 */
	public SyncObject folderChange(String parent, String oldid, String newid);
	/**
	 * Borra un folder
	 * @param fid Id del folder a borrar. No se implementa por ahora.
	 * @return true si fue todo bien
	 */
	public boolean    folderDelete(String fid);
	/**
	 * Crea o modifica un nuevo objeto. Si es un Mail, hay que usar SendMail.
	 * @param fid Id del folder donde se desea crear el objeto o modificar el existente
	 * @param id Identificador del objeto a cambiar o null si se va a crear
	 * @param nuevo SyncContact, SyncMail, SyncCalendar o SyncTask con las modificaciones.
	 * @return objectStat sobre el objeto recien creado o modificado.
	 */
	public SyncObject objectChange(String fid, String id, SyncObject nuevo);
	/**
	 * Borrar un objeto
	 * @param fid Id del Folder donde esta el objeto a borrar
	 * @param id Id del objeto a borrar
	 * @return true si fue todo bien
	 */
	public boolean    objectDelete(String fid, String id);
	/**
	 * Mover un objeto de un folder a otro (solo Mail)
	 * @param fid Id del Folder que contiene el objeto a mover
	 * @param id Id del objeto a mover
	 * @param newFid id del folder destino.
	 * @return true si fue todo bien
	 */
	public boolean    objectMove(String fid, String id, String newFid);
	/**
	 * Env�a un correo (no se usa objectChange)
	 * @param rfc822 Codificacion MIME del mensaje a enviar
	 * @param forward true si es un samrtForward
	 * @param reply true si es un smartReply
	 * @param parent 
	 * @return true si fue todo bien
	 */
	public boolean sendMail(String rfc822, boolean forward, boolean reply, String parent);
	/**
	 * Marca un correo en el backend como leido
	 * @param fid Id del Folder al que pertenece el correo
	 * @param id Id del correo
	 * @param flags Valor a setear
	 * @return true si fue todo bien
	 */
	public boolean setReadFlag(String fid, String id, String flags);

}
 