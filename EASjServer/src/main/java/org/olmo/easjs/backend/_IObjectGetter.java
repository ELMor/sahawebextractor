package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;

public interface _IObjectGetter extends _IStater {
	/**
	 * Obtiene el Folder desde el backend en forma de SyncFolder
	 * @param fid Identificador del folder
	 * @return El Folder o null
	 */
	public SyncFolder folderGet(String fid);
	/**
	 * Obtiene el objeto del backend en forma de SyncMail, SuncTask, SyncCalendar o SyncMail
	 * @param fid Identificador del Folder
	 * @param id Identificador del Objeto
	 * @param truncSize Si hay que truncar el tama�o (solo para SyncMail)
	 * @return
	 */
	public SyncObject objectGet(String fid, String id, int truncSize);
	/**
	 * Obtiene el Attachment de un Mail
	 * @param attname Nombre del Attachment (Actualmente FID:ID:ATTNAME)
	 * @return
	 */
	public byte[] getAttachment(String attname);
	
	/**
	 * Coloca un monitor sobre el folder fid
	 * @param fid folder a monitorizar
	 * @return true si pudo colocarlo
	 */
	public boolean monitorFolder(String fid);
	
	/**
	 * Obtiene los cambios directaente monitorizados
	 * @param fid Id del folder a monitorizar
	 * @return changes
	 */
	public Vector<SyncObject> getMonitorizedChanges(String fid);
}
