package org.olmo.easjs.backend.impl;

import java.util.Enumeration;
import java.util.Hashtable;


public class CacheTime<T> {
	long valWindow;
	Hashtable<String, T> cache=new Hashtable<String,T>();;
	Hashtable<String, Long> times=new Hashtable<String, Long>();
	
	public CacheTime(long valWindow){
		this.valWindow=valWindow;
		new Remover().start();
	}
	
	public void add(String key, T value){
		cache.put(key, value);
		times.put(key, System.currentTimeMillis());
	}
	
	public T get(String key){
		T ret=cache.get(key);
		Long time=times.get(key);
		if(ret==null)
			return null;
		if(System.currentTimeMillis()-time>valWindow){
			cache.remove(key);
			times.remove(key);
			return null;
		}
		return ret;
	}
	
	class Remover extends Thread {
		@Override
		public void run() {
			while(true){
				try {
					Thread.sleep(30000);
					for(Enumeration<String> keys=times.keys();
					    keys.hasMoreElements();){
						String key=keys.nextElement();
						long time=times.get(key);
						if(System.currentTimeMillis()-time>valWindow){
							cache.remove(key);
							times.remove(key);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
}
