package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.Conf;
import org.olmo.easjs.Cons;
import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;

public abstract class Backend implements Cons {
	protected String _user;
	protected String _devid;
	protected String _protocolversion;

	/**
	 * Called to logon a user. These are the three authentication strings that
	 * you must specify in ActiveSync on the PDA. Normally you would do some
	 * kind of password check here. Alternatively, you could ignore the password
	 * here and have Apache do authentication via mod_auth_*
	 */
	public abstract boolean logon(String dom, String u, String pwd);

	/**
	 * Called before shutting down the request to close the connection
	 */
	public abstract boolean logoff();

	/**
	 * Called directly after the logon. This specifies the client's protocol
	 * version and device id. The device ID can be used for various things,
	 * including saving per-device state information. The $user parameter here
	 * is normally equal to the $username parameter from the Logon() call. In
	 * theory though, you could log on a 'foo', and then sync the emails of user
	 * 'bar'. The $user here is the username specified in the request URL, while
	 * the $username in the Logon() call is the username which was sent as a
	 * part of the HTTP authentication.
	 */
	public abstract boolean Setup(String us, String devid, String protocolv);

	public HierImport getHierImporter() {
		return new HierImport(this);
	}

	public ContImport getContImporter(String folderid,Vector<SyncObject> state, int flags) {
		return new ContImport(this, folderid, state,flags);
	}
	/**
	 * Obtener un exporter 
	 * @param folderid Id del Folder
	 * @param imp Importer a usar (para sincronizar)
	 * @param type Tipo del Folder
	 * @param restric
	 * @param syncstate Estado actual del Folder
	 * @param flags 
	 * @param truncation Necesidad ed truncar los mails
	 * @return
	 */
	public ContExport getContExporter(String folderid, ContImport imp, String type, 
			int restric, Vector<SyncObject> syncstate, int flags, int truncation) {
		return new ContExport(this, folderid, imp, type, restric,
				syncstate, flags, truncation);
	}

	public HierExport getHierExporter(HierImport imp,
			String folderid, int restric, Vector<SyncObject> syncstate,
			int flags, int truncation) {
		return new HierExport(this, imp, syncstate, flags);
	}

	public Vector<SyncObject> getHierarchy() {
		return folderList();
	}

	public SyncObject fetch(String folderid, String id, boolean mimesupport) {
		return objectGet(folderid, id, 1024 * 1024, mimesupport);
	}

	public abstract byte[] getAttachment(String attname);

	/**
	 * Sends a message which is passed as rfc822. You basically can do two
	 * things 1) Send the message to an SMTP server as-is 2) Parse the message
	 * yourself, and send it some other way It is up to you whether you want to
	 * put the message in the sent items folder. If you want it in 'sent items',
	 * then the next sync on the 'sent items' folder should return the new
	 * message as any other new message in a folder.
	 */
	public abstract boolean sendMail(String rfc822, boolean forward,
			boolean reply, String parent);

	/**
	 * Should return a wastebasket folder if there is one. This is used when
	 * deleting items; if this function returns a valid folder ID, then all
	 * deletes are handled as moves and are sent to your backend as a move. If
	 * it returns FALSE, then deletes are always handled as real deletes and
	 * will be sent to your importer as a DELETE
	 */
	public abstract String getWasteBasket();

	/**
	 * Should return a list (array) of messages, each entry being an associative
	 * array with the same entries as StatMessage(). This function should return
	 * stable information; ie if nothing has changed, the items in the array
	 * must be exactly the same. The order of the items within the array is not
	 * important though.
	 * 
	 * The cutoffdate is a date in the past, representing the date since which
	 * items should be shown. This cutoffdate is determined by the user's
	 * setting of getting 'Last 3 days' of e-mail, etc. If you ignore the
	 * cutoffdate, the user will not be able to select their own cutoffdate, but
	 * all will work OK apart from that.
	 */
	public abstract Vector<SyncObject> objectList(String fid, int cutoffdate);

	/**
	 * StatMessage should return message stats, analogous to the folder stats
	 * (StatFolder). Entries are: 'id' => Server unique identifier for the
	 * message. Again, try to keep this short (under 20 chars) 'flags' => simply
	 * '0' for unread, '1' for read 'mod' => modification signature. As soon as
	 * this signature changes, the item is assumed to be completely changed, and
	 * will be sent to the PDA as a whole. Normally you can use something like
	 * the modification time for this field, which will change as soon as the
	 * contents have changed.
	 */

	public abstract SyncObject objectStat(String fid, String id);

	/**
	 * GetMessage should return the actual SyncXXX object type. You may or may
	 * not use the '$folderid' parent folder identifier here. Note that mixing
	 * item types is illegal and will be blocked by the engine; ie returning an
	 * Email object in a Tasks folder will not do anything. The SyncXXX objects
	 * should be filled with as much information as possible, but at least the
	 * subject, body, to, from, etc.
	 * 
	 * Truncsize is the size of the body that must be returned. If the message
	 * is under this size, bodytruncated should be 0 and body should contain the
	 * entire body. If the body is over $truncsize in bytes, then bodytruncated
	 * should be 1 and the body should be truncated to $truncsize bytes.
	 * 
	 * Bodysize should always be the original body size.
	 */
	public abstract SyncObject objectGet(String fid, String id, int truncsize,
			boolean mimesupport);

	/**
	 * This function is called when the user has requested to delete (really
	 * delete) a message. Usually this means just unlinking the file its in or
	 * somesuch. After this call has succeeded, a call to GetMessageList()
	 * should no longer list the message. If it does, the message will be
	 * re-sent to the PDA as it will be seen as a 'new' item. This means that if
	 * you don't implement this function, you will be able to delete messages on
	 * the PDA, but as soon as you sync, you'll get the item back
	 */
	public abstract boolean objectDelete(String fid, String id);

	/**
	 * This should change the 'read' flag of a message on disk. The $flags
	 * parameter can only be '1' (read) or '0' (unread). After a call to
	 * SetReadFlag(), GetMessageList() should return the message with the new
	 * 'flags' but should not modify the 'mod' parameter. If you do change
	 * 'mod', simply setting the message to 'read' on the PDA will trigger a
	 * full resync of the item from the server
	 * 
	 * @param flags
	 */
	public abstract boolean setReadFlag(String fid, String id, String flags);

	/**
	 * This function is called when a message has been changed on the PDA. You
	 * should parse the new message here and save the changes to disk. The
	 * return value must be whatever would be returned from StatMessage() after
	 * the message has been saved. This means that both the 'flags' and the
	 * 'mod' properties of the StatMessage() item may change via
	 * ChangeMessage(). Note that this function will never be called on E-mail
	 * items as you can't change e-mail items, you can only set them as 'read'.
	 * 
	 * @param message
	 */
	public abstract SyncObject objectChange(String fid, String id,
			SyncObject message);

	/**
	 * This function is called when the user moves an item on the PDA. You
	 * should do whatever is needed to move the message on disk. After this
	 * call, StatMessage() and GetMessageList() should show the items to have a
	 * new parent. This means that it will disappear from GetMessageList() will
	 * not return the item at all on the source folder, and the destination
	 * folder will show the new message
	 */
	public abstract boolean objectMoveTo(String fid, String id);

	public String meetingResponse(String requestid, String fid, String error,
			String calendarid) {
		if (calendarid == null || calendarid.length() == 0)
			return null;
		return calendarid;
	}

	public int getTruncSize(int truncation) {
		switch (truncation) {
		case TRUNCATION_HEADERS:
			return 0;
		case TRUNCATION_512B:
			return 512;
		case TRUNCATION_1K:
			return 1024;
		case TRUNCATION_5K:
			return 5 * 1024;
		case TRUNCATION_ALL:
			return 16 * 1024 * 1024;
		default:
			return 1024;
		}
	}

	/**
	 * Returns array of items which contain contact information
	 * 
	 * @param string
	 *            $searchquery
	 * 
	 * @return array
	 */
	public abstract Vector<SyncObject> getSearchResults(String searchQuery);

	/**
	 * Checks if the sent policykey matches the latest policykey on the server
	 * 
	 * @param user
	 * @param auth_pwd
	 * @param string
	 *            $policykey
	 * @param string
	 *            $devid
	 * 
	 * @return status flag
	 */
	public int checkPolicy(String polk, String devid, String user,
			String auth_pwd) {
		int status = PROVISION_STATUS_SUCCESS;
		String user_policy_key = getPolicyKey(user, auth_pwd, devid);
		if (!polk.equals(user_policy_key)) {
			status = PROVISION_STATUS_POLKEYMISM;
		}
		if (polk == null)
			polk = user_policy_key;
		// TODO se pasa por referencia polk. Se modifica aqu�!!!!
		return status;
	}

	/**
	 * Return a policy key for given user with a given device id. If there is no
	 * combination user-deviceid available, a new key should be generated.
	 * 
	 * @param string
	 *            $user
	 * @param string
	 *            $pass
	 * @param string
	 *            $devid
	 * 
	 * @return unknown
	 */
	public String getPolicyKey(String user, String pwd, String devid) {
		String pk = (String) Conf.st.get("PolicyKey-" + devid);
		if (pk == null) {
			pk = generatePolicyKey();
			setPolicyKey(pk, devid);
		}
		return pk;
	}

	/**
	 * Generate a random policy key. Right now it's a 10-digit number.
	 * 
	 * @return unknown
	 */
	public String generatePolicyKey() {
		String ret = "" + (long) (Math.random() * 10000000000L);
		while (ret.length() > 10)
			ret = ret.substring(1);
		while (ret.length() < 10)
			ret = ret + "0";
		return ret;
	}

	/**
	 * Set a new policy key for the given device id.
	 * 
	 * @param string
	 *            $policykey
	 * @param string
	 *            $devid
	 * @return unknown
	 */
	public boolean setPolicyKey(String polk, String devid) {
		Conf.st.put("PolicyKey-" + devid, polk);
		return true;
	}

	/**
	 * Return a device wipe status
	 * 
	 * @param string
	 *            $user
	 * @param string
	 *            $pass
	 * @param string
	 *            $devid
	 * @return int
	 */
	public boolean getDeviceStatus(String us, String pwd, String devid) {
		return false; // TODO DeviceStatus (WIPE STATUS) Change!
	}

	/**
	 * Set a new rw status for the device
	 * 
	 * @param string
	 *            $user
	 * @param string
	 *            $pass
	 * @param string
	 *            $devid
	 * @param string
	 *            $status
	 * 
	 * @return boolean
	 */
	public boolean setDeviceRWStatus(String us, String pwd, String di,
			String status) {
		return false; // TODO DeviceStatus (WIPE STATUS) Change!
	}

	/**
	 * new ping mechanism
	 * 
	 * @return if exist an alter ping mechanism
	 */
	public abstract boolean alterPing();

	/**
	 * El comando Ping informa mediante este m�todo de un folder que quire monitorizar
	 * @param fid Id Del Folder
	 * @return true si el backend puede monitorizar el folder con un m�todo alternativo
	 */
	public abstract boolean wantMonitorFolder(String fid);

	/**
	 * Devuelve un array de changes como el que devuelve DiffState.GetDiff
	 * 
	 * @param folderid
	 * @param syncstate
	 * @param cutoffdate 
	 * @return
	 */
	public abstract Vector<SyncObject> alterPingChanges(String folderid,
			Vector<SyncObject> syncstate, int cutoffdate);

	public abstract boolean folderDelete(String id, String parent);

	/**
	 * Creates or modifies a Collection "CollectionId" => id of the parent Collection
	 * "oldid" => if empty -> new folder created, else folder is to be renamed
	 * "displayname" => new folder name (to be created, or to be renamed to)
	 * "type" => folder type, ignored in IMAP
	 * 
	 */
	public abstract SyncObject folderChange(String fid, String oldid,
			String displayName, String type);

	/**
	 * GetFolder should return an actual SyncFolder object with all the
	 * properties set. Folders are pretty simple really, having only a type, a
	 * name, a parent and a server ID.
	 */
	public abstract SyncFolder folderGet(String id);

	/**
	 * Return folder stats. This means you must return a SyncObject with the
	 * following properties: "id" The server ID that will be used to identify
	 * the folder. It must be unique, and not too long How long exactly is not
	 * known, but try keeping it under 20 chars or so. It must be a string.
	 * 
	 * "parent" The server ID of the parent of the folder. Same restrictions as
	 * 'id' apply.
	 * 
	 * "mod" This is the modification signature. It is any arbitrary string
	 * which is constant as long as the folder has not changed. In practice this
	 * means that 'mod' can be equal to the folder name as this is the only
	 * thing that ever changes in folders. (the type is normally constant)
	 */
	public abstract SyncObject folderStat(String id);

	/**
	 * This function is analogous to GetMessageList.
	 * 
	 */
	public abstract Vector<SyncObject> folderList();

	/**
	 * Comprueba el estado del Remote Wipe del dispositivo DEvuelve valores del
	 * tipo SYNC_PROVISION_RWSTATUS_*
	 * 
	 * @param user
	 * @param pwd
	 * @param devid
	 * @return
	 */
	public abstract int getDeviceRWStatus(String user, String pwd, String devid);
	
	/**
	 * ServerId for EAS must not be greater tahn 64 chars. This creates a map.
	 * @param bendId BackendId for (msg, Cal, Contact, task, etc)
	 * @return
	 */
	public String getSrvIdFromBendId(String bendId){
		return Conf.st.getSrvIdFromBendId(_user, bendId);
	}

	/**
	 * ServerId for EAS must not be greater tahn 64 chars. This creates a map.
	 * @param srvId ServerId for (msg, Cal, Contact, task, etc)
	 * @return
	 */
	public String getBendIdFromSrvId(String srvId){
		return Conf.st.getBendIdFromSrvId(_user, srvId);
	}
}
