package org.olmo.easjs.store;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Hashtable;

import org.olmo.common.Util;

public class FileSystemStore implements GenericStore {
	String baseDir;
	
	public Object get(String key) {
		String file=key;
		Object obj=null;
		if(file==null)
			return null;
		try {
			File f=getRelativeFile(file);
			if(!f.exists())
				return null;
			FileInputStream fis=new FileInputStream(f);
			ObjectInputStream ois=new ObjectInputStream(fis);
			obj=ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	public boolean login(String user, String password, String url) {
		baseDir=url;
		return true;
	}
	

	public boolean put(String key, Serializable value) {
		String file=key;
		try {
			FileOutputStream fos=new FileOutputStream(getRelativeFile(file));
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(value);
			oos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	public boolean logout() {
		return true;
	}

	private boolean real_remove(String key) {
		String fname=key;
		if(fname!=null){
			File f=getRelativeFile(fname);
			return f.delete();
		}
		return false;
	}

	public boolean remove(String key) {
		return real_remove(key);
	}

	private File getRelativeFile(String name){
		return new File(baseDir,name);
	}

	
	
	public String getBendIdFromSrvId(String user, String srvId) {
		Hashtable<String, String> bendFromSrv = getBendFromSrv(user);
		String bendId=bendFromSrv.get(srvId);
		if(bendId==null){
			bendId=Util.gen64Char();
			Hashtable<String, String> srvFromBend = getSrvFromBend(user);
			while(srvFromBend.get(bendId)!=null)
				bendId=Util.gen64Char();
			mapBendSrv(user, bendFromSrv, srvFromBend, bendId, srvId);
		}
		return bendId;
	}

	public String getSrvIdFromBendId(String user, String bendId) {
		Hashtable<String, String> srvFromBend = getSrvFromBend(user);
		String srvId=srvFromBend.get(bendId);
		if(srvId==null){
			srvId=Util.gen64Char();
			Hashtable<String, String> bendFromSrv = getBendFromSrv(user);
			while(bendFromSrv.get(srvId)!=null)
				srvId=Util.gen64Char();
			mapBendSrv(user, bendFromSrv, srvFromBend, bendId, srvId);
		}
		return srvId;
	}

	private void mapBendSrv(
			String user,
			Hashtable<String, String> bfs, 
			Hashtable<String, String> sfb,
			String bendId, 
			String srvId){
		bfs.put(srvId, bendId);
		sfb.put(bendId, srvId);
		putBendFromSrv(user, bfs);
		putSrvFromBend(user, sfb);
	}
	
	private Hashtable<String, String> getSrvFromBend(String user) {
		Hashtable<String, String> srvFromBend=(Hashtable<String, String>)get(user+"-sfb.bin");
		if(srvFromBend==null){
			srvFromBend=new Hashtable<String, String>();
			putSrvFromBend(user, srvFromBend);
		}
		return srvFromBend;
	}

	private void putSrvFromBend(String user,
			Hashtable<String, String> srvFromBend) {
		put(user+"-sfb.bin",srvFromBend);
	}

	private Hashtable<String, String> getBendFromSrv(String user) {
		Hashtable<String, String> bendFromSrv=(Hashtable<String, String>)get(user+"-bfs.bin");
		if(bendFromSrv==null){
			bendFromSrv=new Hashtable<String, String>();
			putBendFromSrv(user, bendFromSrv);
		}
		return bendFromSrv;
	}

	private void putBendFromSrv(String user, Hashtable<String, String> bendFromSrv) {
		put(user+"-bfs.bin",bendFromSrv);
	}

}
