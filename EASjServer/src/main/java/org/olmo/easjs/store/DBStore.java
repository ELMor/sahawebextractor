package org.olmo.easjs.store;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.hsqldb.jdbc.JDBCBlob;
import org.olmo.common.MultiConfigProp;
import org.olmo.common.Util;

public class DBStore implements GenericStore {
	Connection conn=null;
	PreparedStatement insert,update,select,remove,count;
	PreparedStatement insertMap,selectSrvMap,selectBendMap;
	
	
	public synchronized boolean put(String key, Serializable value) {
		try {
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			ObjectOutputStream oos=new ObjectOutputStream(baos);
			oos.writeObject(value);
			oos.close();
			JDBCBlob b=new JDBCBlob(baos.toByteArray());
			count.setString(1, key);
			ResultSet rs=count.executeQuery();
			rs.next();
			if(rs.getInt(1)==0){
				insert.setString(1, key);
				insert.setBlob(2, b);
				insert.executeUpdate();
			}else{
				update.setBlob(1, b);
				update.setString(2, key);
				update.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
	public synchronized Object get(String key) {
		Object ret=null;
		try {
			select.setString(1, key);
			ResultSet rs=select.executeQuery();
			if(rs.next()){
				Blob b=rs.getBlob(1);
				ObjectInputStream ois=new ObjectInputStream(b.getBinaryStream());
				ret=ois.readObject();
				ois.close();
			}
			return ret;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public synchronized String getBendIdFromSrvId(String user, String srvId) {
		String bendId=null;
		try {
			selectBendMap.setString(1, srvId);
			ResultSet rs=selectBendMap.executeQuery();
			if(rs.next()){ //Existe dato
				bendId=rs.getString(1);
			}else{ //No existe: error
				bendId=null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bendId;
	}

	
	public synchronized String getSrvIdFromBendId(String user, String backendId) {
		String srvId=null;
		try {
			selectSrvMap.setString(1, backendId);
			ResultSet rs=selectSrvMap.executeQuery();
			if(rs.next()){ //Existe dato
				srvId=rs.getString(1);
			}else{ //No existe: crearlo
				while(null!=getBendIdFromSrvId(user, (srvId=Util.gen64Char())))
						;
				insertMap.setString(1, srvId);
				insertMap.setString(2, backendId);
				insertMap.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return srvId;
	}

	
	public boolean login(String user, String password, String url) {
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			initConnection(user, password, url);
			initPS();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private void initConnection(String user, String password, String url)
			throws SQLException {
		try {
			tryConnect(user, password, url);
		}catch(SQLException sql){
			//No esta inicializada
			MultiConfigProp mcp=new MultiConfigProp(
					Class.class.getResourceAsStream("/org/olmo/easjs/store/HSQLDB.properties"));
			Vector<String> scr=mcp.getList("script");
			try {
				conn=DriverManager.getConnection(url, "SA", "");
				for(int i=0;i<scr.size();i++){
					String command=scr.get(i);
					PreparedStatement ps=conn.prepareStatement(command);
					ps.execute();
				}
				tryConnect(user, password, url);
			}catch(SQLException sql2){
				sql2.printStackTrace();
				throw new RuntimeException("Cant handle DB init");
			}
		}
	}

	private void tryConnect(String user, String password, String url)
			throws SQLException {
		conn=DriverManager.getConnection(url, user, password);
		PreparedStatement ps=conn.prepareStatement("SET SCHEMA EASJS");
		ps.execute();
	}

	private void initPS() throws SQLException {
		insert=conn.prepareStatement("INSERT INTO DATOS(CLAVE,VALOR) VALUES(?,?)");
		update=conn.prepareStatement("UPDATE DATOS SET VALOR=? WHERE CLAVE=?");
		count=conn.prepareStatement("SELECT COUNT(*) FROM DATOS WHERE CLAVE=?");
		select=conn.prepareStatement("SELECT VALOR FROM DATOS WHERE CLAVE=?");
		remove=conn.prepareStatement("DELETE FROM DATOS WHERE CLAVE=?");
		insertMap=conn.prepareStatement("INSERT INTO MAPEO(SRVID,BENDID) VALUES(?,?)");
		selectSrvMap=conn.prepareStatement("SELECT SRVID FROM MAPEO WHERE BENDID=?");
		selectBendMap=conn.prepareStatement("SELECT BENDID FROM MAPEO WHERE SRVID=?");
	}

	
	public boolean logout() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
	public synchronized boolean remove(String key) {
		try {
			remove.setString(1, key);
			remove.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
