package org.olmo.easjs.store;

import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Logger;

import org.olmo.easjs.Conf;
import org.olmo.easjs.server.object.SyncObject;

public class StateMachine {
	Logger log=Logger.getLogger(StateMachine.class.getCanonicalName());
	
	// "{%04x%04x-%04x-%04x-%04x-%04x%04x%04x}n"
	@SuppressWarnings("unchecked")
	public Vector<SyncObject> getSyncState(String synckey){
		if(synckey.equals("0"))
			return null;
		Vector<SyncObject> ret=(Vector<SyncObject>)Conf.st.get(synckey);
		if(Conf.debug){
			log.info("get SS "+synckey+":"+ (ret==null? "null":ret.size()));
		}
		//Borramos la anterior.
		//TODO Borrar el penultimo estado de sincronizacion
		//Conf.st.remove(synckey);
		return ret;
	}
	
	public boolean setSyncState(String syncKey, Serializable syncstate){
		int ndx=getIndexOfNumber(syncKey);
		int number=Integer.parseInt(syncKey.substring(ndx+1));
		number--;
		if(Conf.debug){
			log.info("set SS "+syncKey+":"+ syncstate.toString());
		}
		return Conf.st.put(syncKey, syncstate);
	}
	
	public String getNewSyncKey(String syncKey){
		if(syncKey==null || syncKey.equals("0"))
			return "{"+uuid()+"}1";
		int number;
		int ndx = getIndexOfNumber(syncKey);
		number=Integer.parseInt(syncKey.substring(ndx+1));
		return ""+syncKey.substring(0,ndx+1)+(number+1);
	}

	private int getIndexOfNumber(String synckey) {
		int ndx=synckey.lastIndexOf('}');
		if(ndx<0 || ndx+1>=synckey.length())
			throw new RuntimeException("SyncKey Invalida!");
		return ndx;
	}
	
	private String uuid(){
		int p1=(int)(Math.random()*0xffff);
		int p2=(int)(Math.random()*0xffff);
		int p3=(int)(Math.random()*0xffff);
		int p4=(int)(Math.random()*0x0fff) | 0x4000;
		int p5=(int)(Math.random()*0x3fff) | 0x8000;
		int p6=(int)(Math.random()*0xffff);
		int p7=(int)(Math.random()*0xffff);
		int p8=(int)(Math.random()*0xffff);
		return String.format("%04x%04x-%04x-%04x-%04x-%04x%04x%04x",p1,p2,p3,p4,p5,p6,p7,p8);
	}

}
