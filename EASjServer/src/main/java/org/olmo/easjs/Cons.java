package org.olmo.easjs;
/**
 * Corresponde a z-push/zpushdefs.php
 * 
 * @author elinares
 *
 */
public interface Cons {
	// Other constants
	public static final int FOLDER_TYPE_OTHER= 1;
	public static final int FOLDER_TYPE_INBOX= 2;
	public static final int FOLDER_TYPE_DRAFTS= 3;
	public static final int FOLDER_TYPE_WASTEBASKET= 4;
	public static final int FOLDER_TYPE_SENTMAIL= 5;
	public static final int FOLDER_TYPE_OUTBOX= 6;
	public static final int FOLDER_TYPE_TASK= 7;
	public static final int FOLDER_TYPE_APPOINTMENT= 8;
	public static final int FOLDER_TYPE_CONTACT= 9;
	public static final int FOLDER_TYPE_NOTE= 10;
	public static final int FOLDER_TYPE_JOURNAL= 11;
	public static final int FOLDER_TYPE_USER_MAIL= 12;
	public static final int FOLDER_TYPE_USER_APPOINTMENT= 13;
	public static final int FOLDER_TYPE_USER_CONTACT= 14;
	public static final int FOLDER_TYPE_USER_TASK= 15;
	public static final int FOLDER_TYPE_USER_JOURNAL= 16;
	public static final int FOLDER_TYPE_USER_NOTE= 17;
	public static final int FOLDER_TYPE_UNKNOWN= 18;
	public static final int FOLDER_TYPE_RECIPIENT_CACHE= 19;
	public static final String FOLDER_TYPE_APPOINTMENT_ID= "CalendarFolder-ABQ123";
	public static final String FOLDER_TYPE_CONTACT_ID= "ContacsFolder-ABQ123";

	public static final int CONFLICT_OVERWRITE_SERVER= 0;
	public static final int CONFLICT_OVERWRITE_PIM= 1;

	public static final int FILTERTYPE_ALL= 0;
	public static final int FILTERTYPE_1DAY= 1;
	public static final int FILTERTYPE_3DAYS= 2;
	public static final int FILTERTYPE_1WEEK= 3;
	public static final int FILTERTYPE_2WEEKS= 4;
	public static final int FILTERTYPE_1MONTH= 5;
	public static final int FILTERTYPE_3MONTHS= 6;
	public static final int FILTERTYPE_6MONTHS= 7;

	public static final int TRUNCATION_HEADERS= 0;
	public static final int TRUNCATION_512B= 1;
	public static final int TRUNCATION_1K= 2;
	public static final int TRUNCATION_5K= 4;
	public static final int TRUNCATION_SEVEN= 7;
	public static final int TRUNCATION_ALL= 9;

	public static final int PROVISION_STATUS_SUCCESS= 1;
	public static final int PROVISION_STATUS_PROTERROR= 2;
	public static final int PROVISION_STATUS_SERVERERROR= 3;
	public static final int PROVISION_STATUS_DEVEXTMANAGED= 4;
	public static final int PROVISION_STATUS_POLKEYMISM= 5;

	public static final int PROVISION_RWSTATUS_NA= 0;
	public static final int PROVISION_RWSTATUS_OK= 1;
	public static final int PROVISION_RWSTATUS_PENDING= 2;
	public static final int PROVISION_RWSTATUS_WIPED= 3;
	
	public final int EXPORT_HIERARCHY = 1;
	public final int EXPORT_CONTENTS=2;
	
	public final int BACKEND_DISCARD_DATA=1;

}
