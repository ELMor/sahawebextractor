package org.olmo.easjs.server.commands.objects;

import java.io.Serializable;
import java.util.Vector;

public class PingStatus implements Serializable {
	private static final long serialVersionUID = 2831879915292605068L;
	
	public int lifetime=0;
	public Vector<PingCol> collections=new Vector<PingCol>(); 
}
