package org.olmo.easjs.server.object;

import java.util.Date;
import java.util.Vector;

import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPEmail;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SyncMail extends SyncObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7629727687699891362L;
	public String flags;
	public String body;
	public int bodysize;
	public boolean bodytruncated;
	public Date datereceived;
	public String displayto;
	public String importance;
	public String messageclass;
	public String subject;
	public String threadtopic;
	public String to;
	public String cc;
	public String from;
	public String reply_to;
	public String internetcpid=null;
	public String mimetruncated;
	public Vector<SyncObject> attachments;
	public String mimedata;
	public int mimesize;
	public SyncMeetingRequest meetingrequest;

	public SyncMail(Node n) {
		if(n==null)
			return;
		to=getTextSNN(n, CPEmail.To);
		cc=getTextSNN(n, CPEmail.Cc);
		from=getTextSNN(n, CPEmail.From);
		subject=getTextSNN(n, CPEmail.Subject);
		threadtopic=getTextSNN(n, CPEmail.ThreadTopic);
		datereceived=parseDateDashes(getTextSNN(n, CPEmail.DateReceived));
		displayto=getTextSNN(n, CPEmail.DisplayTo);
		importance=getTextSNN(n, CPEmail.Importance);
		read=getTextSNN(n, CPEmail.Read);
		
		Node atts=getSNN(n, CPEmail.Attachments);
		if(atts!=null){
			attachments=new Vector<SyncObject>();
			NodeList attList=atts.getChildNodes();
			for(int j=0;j<attList.getLength();j++){
				attachments.add(new SyncAttachment(attList.item(j)));
			}
		}
		mimetruncated=getTextSNN(n, CPEmail.MIMETruncated);
		mimedata=getTextSNN(n, CPEmail.MIMEData);
		mimesize=getIntSNN(n, CPEmail.MIMESize);
		bodytruncated=getBooSNN(n, CPEmail.BodyTruncated);
		bodysize=getIntSNN(n, CPEmail.BodySize);
		body=getTextSNN(n, CPEmail.Body);
		messageclass=getTextSNN(n, CPEmail.MessageClass);
		Node meeting=getSNN(n, CPEmail.MeetingRequest);
		if(meeting!=null){
			meetingrequest=new SyncMeetingRequest(meeting);
		}
		reply_to=getTextSNN(n, CPEmail.ReplyTo);
		internetcpid=getTextSNN(n, CPEmail.InternetCPID);
		
	}
	@Override
	public void encode(EASEncoder xo) {
		xo.element(CPEmail.To,to);
		xo.element(CPEmail.Cc,cc);
		xo.element(CPEmail.From,from);
		xo.element(CPEmail.Subject,subject);
		xo.element(CPEmail.ThreadTopic,threadtopic);
		xo.element(CPEmail.DateReceived,encodeDateDashes(datereceived));
		xo.element(CPEmail.DisplayTo,displayto);
		xo.element(CPEmail.Importance,importance);
		xo.element(CPEmail.Read,read);
		if(mimedata!=null){
			xo.element(CPEmail.MIMETruncated,mimetruncated);
			xo.element(CPEmail.MIMESize,""+mimesize);
			xo.element(CPEmail.MIMEData,mimedata);
		}else if(body!=null){
			xo.element(CPEmail.BodyTruncated,bodytruncated ? "1":"0");
			xo.element(CPEmail.BodySize,""+bodysize);
			xo.element(CPEmail.Body,body);
		}
		xo.element(CPEmail.MessageClass,messageclass);
		xo.element(CPEmail.ReplyTo,reply_to);
		if(attachments!=null){
			xo.open(CPEmail.Attachments);
			xo.element(CPEmail.Attachment,attachments);
			xo.close();
		}
	}
}
