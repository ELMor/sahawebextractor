package org.olmo.easjs.server;

import java.io.InputStream;
import java.util.logging.LogManager;

import org.olmo.common.MultiConfigProp;
import org.olmo.easjs.Conf;

public class Start {
	public static void main(String args[]) {
		//Logging properties
		try {
			LogManager lm=LogManager.getLogManager();
			InputStream is=Class.class.getResourceAsStream("/logging.properties");
			lm.readConfiguration(is);
		}catch(Exception e1){
			e1.printStackTrace();
		}
		//Config
		Conf.load(new MultiConfigProp(Class.class.getResourceAsStream("/easjs.properties")));
		//Instanciar el servidor
		try {
			ServerType srv=(ServerType)(Class.forName(Conf.serverClass).newInstance());
			//Start https server if configured
			if(Conf.httpServer)
				srv.httpServerSetup();
			//Start http server if configured
			if(Conf.httpsServer)
				srv.httpsServerSetup();
		}catch(Exception e){
			e.printStackTrace();
		}
	}


}
