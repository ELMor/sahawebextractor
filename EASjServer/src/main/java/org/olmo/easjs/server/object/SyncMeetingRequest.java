package org.olmo.easjs.server.object;

import java.util.Date;

import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPEmail;
import org.w3c.dom.Node;

public class SyncMeetingRequest extends SyncObject{
	
	private static final long serialVersionUID = 246775709604234561L;
	
	public SyncMeetingRequest(Node n){
		if(n==null)
			return;
		alldayevent=getBooSNN(n, CPEmail.AllDayEvent);
		starttime=parseDateDashes(getTextSNN(n, CPEmail.StartTime));
		dtstamp=parseDateDashes(getTextSNN(n, CPEmail.DtStamp));
		endtime=parseDateDashes(getTextSNN(n, CPEmail.EndTime));
		instancetype=getTextSNN(n, CPEmail.InstanceType);
		location=getTextSNN(n, CPEmail.Location);
		organizer=getTextSNN(n, CPEmail.Organizer);
		reminder=getTextSNN(n, CPEmail.Reminder);
		responserequested=getBooSNN(n, CPEmail.ResponseRequested);
		sensitivity=getTextSNN(n, CPEmail.Sensitivity);
		busystatus=getTextSNN(n, CPEmail.BusyStatus);
		timezone=getTextSNN(n, CPEmail.TimeZone);
		recurrence=new SyncRecurrence(getSNN(n, CPEmail.Recurrence));
		recurrenceid=getTextSNN(n, CPEmail.RecurrenceId);
		globalobjid=getTextSNN(n, CPEmail.GlobalObjId);
	}

	public boolean alldayevent;
	public Date starttime;
	public Date dtstamp;
	public Date endtime;
	public String instancetype;
	public String location;
	public String organizer;
	public String reminder;
	public boolean responserequested;
	public String sensitivity;
	public String busystatus;
	public String timezone;
	public String globalobjid;
	public SyncRecurrence recurrence;
	public String recurrenceid;
	@Override
	public void encode(EASEncoder xo) {
		xo.element(CPEmail.AllDayEvent,alldayevent ? "1" : "0");
		xo.element(CPEmail.StartTime,encodeDateDashes(starttime));
		xo.element(CPEmail.DtStamp,encodeDateDashes(dtstamp));
		xo.element(CPEmail.EndTime,encodeDateDashes(endtime));
		xo.element(CPEmail.InstanceType,instancetype);
		xo.element(CPEmail.Location,location);
		xo.element(CPEmail.Organizer,organizer);
		xo.element(CPEmail.Reminder,reminder);
		xo.element(CPEmail.ResponseRequested,responserequested ? "1":"0");
		xo.element(CPEmail.Sensitivity,sensitivity);
		xo.element(CPEmail.BusyStatus,busystatus);
		xo.element(CPEmail.TimeZone,timezone);
		xo.element(CPEmail.RecurrenceId,recurrenceid);
		xo.element(CPEmail.Recurrence,recurrence);
		xo.element(CPEmail.GlobalObjId,globalobjid);
	}
}
