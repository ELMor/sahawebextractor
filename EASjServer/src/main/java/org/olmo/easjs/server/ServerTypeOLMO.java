package org.olmo.easjs.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.olmo.easjs.Conf;

public class ServerTypeOLMO implements ServerType {

	int socketTimeout = 480 * 1000;

	class Server implements Runnable {
		boolean ssl=false;
		ServerSocket ss;
		public Server(ServerSocket s, boolean ssl){
			this.ssl=ssl;
			ss=s;
		}
		
		public void run() {
			try {
				ss.setSoTimeout(socketTimeout);
				while (true) {
					Socket sck = ss.accept();
					try {
						if(ssl)
							((SSLSocket)sck).startHandshake();
						OLMOExchange x=new OLMOExchange(sck);
						ThreadHandler thr=new ThreadHandler(x);
						Thread th=new Thread(thr,"OLMOExchange "+x.stat);
						th.start();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class ThreadHandler implements Runnable{
		OLMOExchange xch;
		public ThreadHandler(OLMOExchange x) {
			xch = x;
		}
		
		public void run() {
			try {
				new EASHandler().handle(xch);
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	
	public void httpServerSetup() {
		try {
			ServerSocket ss=new ServerSocket(80, Conf.maxThreads);
			Server server=new Server(ss,false);
			Thread th=new Thread(server,"OLMO HTTP Web Server");
			th.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	public void httpsServerSetup() {
		InputStream is;
		try {
			KeyStore ks = KeyStore.getInstance("JKS");
			is=Class.class.getResourceAsStream(Conf.keyStoreName);
			ks.load(is,Conf.keyStorePwd.toCharArray());

			KeyStore ts = KeyStore.getInstance("JKS");
			is=Class.class.getResourceAsStream(Conf.trustStoreName);
			ts.load(is,Conf.trustStorePwd.toCharArray());

			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, Conf.keyStorePwd.toCharArray());

			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(ts);

			SSLContext ssl = SSLContext.getInstance("TLS");
			
			KeyManager kms[]=kmf.getKeyManagers();
			TrustManager tms[]=tmf.getTrustManagers();
			ssl.init(kms, tms, new SecureRandom());

			SSLServerSocketFactory ssf = ssl.getServerSocketFactory();
			SSLServerSocket ss = (SSLServerSocket) ssf.createServerSocket(443);

			Server server=new Server(ss,true);
			Thread th=new Thread(server,"OLMO HTTPS Web Server");
			th.start();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
