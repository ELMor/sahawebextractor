package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.xml.sax.SAXException;


public class GetAttachment extends CommandHandler {

	String name=null;
	
	public GetAttachment(String an){
		name=an;
	}
	
	@Override
	public void handle(EASHandler x, Backend backend, 
			String deviceid, String protocolversion, Logger log, String user, String pwd) throws SAXException,
			IOException, ParserConfigurationException, XPathExpressionException {
		byte ret[]=backend.getAttachment(name);
		x.addResponseHeader("Content-Type", "application/octet-stream");
		x.sendResponseHeaders(0, 0);
		x.getResponseBody().write(ret);
		log.info("Enviados "+ret.length+" bytes");
		return;
	}
}
