package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.Cons;
import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.wbxml.eas.EASDecoder;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;


public class Provision extends CommandHandler {

	public void handle(
				EASHandler x, 
				Backend backend, 
				String deviceid, 
				String protocolversion, Logger log, String user, String pwd)
		throws SAXException, IOException, ParserConfigurationException, XPathExpressionException
	{
		int status=Cons.PROVISION_STATUS_SUCCESS;
		
		String policyKey=null,policyType=null;
		boolean mustGenPolicy=true;
		
		EASDecoder dec=new EASDecoder(x.getRequestBody());
		Node nodo = null;
		
		nodo=dec.xprNode("/Provision/RemoteWipe/Status");
		if(nodo!=null){
			status=Integer.parseInt(nodo.getTextContent());
		}else{
			nodo=dec.xprNode("/Provision/Policies/Policy/PolicyType");
			if(nodo==null)
				return ;
			policyType=nodo.getTextContent();
			if(!"MS-WAP-Provisioning-XML".equals(policyType)){
				status=Cons.PROVISION_STATUS_SERVERERROR;
			}
			nodo=dec.xprNode("/Provision/Policies/Policy/PolicyKey");
			if(nodo!=null){
				policyKey=nodo.getTextContent();
				mustGenPolicy=false;
				status=Cons.PROVISION_STATUS_SUCCESS;
			}
		}
		if(mustGenPolicy){
			policyKey=backend.generatePolicyKey();
			backend.setPolicyKey(policyKey, deviceid);
		}
		x.sendResponseHeaders(0, 0);
		EASEncoder xo=new EASEncoder("Provision",x.getResponseBody());
		xo.open("Provision");
		xo.element("Status",""+status);
		xo.open("Policies");
		xo.open("Policy");
		xo.element("PolicyType",policyType);
		xo.element("Status",""+status);
		xo.element("PolicyKey",policyKey);
		xo.close();
		xo.close();
		if(mustGenPolicy){
			if("MS-WAP-Provisioning-XML".equals(policyType)) {
				xo.element("Data","<wap-provisioningdoc>" +
						"<characteristic type=\"SecurityPolicy\">" +
						"<parm name=\"4131\" value=\"1\"/>" +
						"<parm name=\"4133\" value=\"1\"/>" +
						"</characteristic>" +
						"</wap-provisioningdoc>");
			}
		}
		xo.close();
		//TODO wipe for android
		xo.getBytes();
	}

}
