package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.Conf;
import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.backend.HierImport;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.server.object.SyncObject;
import org.olmo.easjs.store.StateMachine;
import org.olmo.easjs.wbxml.eas.EASDecoder;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPFolderHierarchy;
import org.xml.sax.SAXException;


public class FolderCreate extends CommandHandler {

	@Override
	public void handle(
			EASHandler x, 
			Backend backend, 
			String deviceid, 
			String protocolversion, 
			Logger log, String user, String pwd) 
	throws SAXException,IOException, ParserConfigurationException, XPathExpressionException 
	{
		EASDecoder dec=new EASDecoder(x.getRequestBody());
		boolean create=false,update=false,delete=false;
		if(dec.xprNode(CPFolderHierarchy.FolderCreate)!=null){
			create=true;
		}else if(null!=dec.xprNode(CPFolderHierarchy.FolderUpdate)){
			update=true;
		}else if(null!=dec.xprNode(CPFolderHierarchy.FolderDelete)){
			delete=true;
		}
		if(!create || !delete || !update)
			return ;
		String synckey=dec.xprText("//"+CPFolderHierarchy.SyncKey);
		String serverid=dec.xprText("//"+CPFolderHierarchy.ServerEntryId);
		if(synckey==null || serverid==null)
			return ;
		String parentid=null,displayName=null,type=null,deletedstat=null;
		if(create || update){
			parentid=dec.xprText("//"+CPFolderHierarchy.ParentId);
			if(parentid==null)
				return ;
			displayName=dec.xprText("//"+CPFolderHierarchy.DisplayName);
			if(displayName==null)
				return ;
			type=dec.xprText("//"+CPFolderHierarchy.Type);
			if(type==null)
				return ;
		}
		StateMachine statemachine=new StateMachine();
		Vector<SyncObject> syncstate=statemachine.getSyncState(synckey);
		String newsynckey=statemachine.getNewSyncKey(synckey);
		Vector<String>seenfolders=(Vector<String>)Conf.st.get("s"+synckey);
		if(seenfolders==null)
			seenfolders=new Vector<String>();
		HierImport importer=backend.getHierImporter();
		importer.Config(syncstate);
		if(create || update){
			serverid=importer.folderChange(serverid, parentid, displayName, type);
		}else{
			deletedstat=importer.folderDeletion(serverid, null);
		}
		x.sendResponseHeaders(0, 0);
		EASEncoder enc=new EASEncoder("FolderHierarchy",x.getResponseBody());
		if(create){
			seenfolders.add(serverid);
			enc.open(CPFolderHierarchy.FolderCreate);
			enc.element(CPFolderHierarchy.Status,"1");
			enc.element(CPFolderHierarchy.SyncKey,newsynckey);
			enc.element(CPFolderHierarchy.ServerEntryId,serverid);
			enc.close();
		}else if(delete){
			enc.open(CPFolderHierarchy.FolderDelete);
			enc.element(CPFolderHierarchy.Status,deletedstat);
			enc.element(CPFolderHierarchy.SyncKey,newsynckey);
			enc.close();
			seenfolders.remove(serverid);
		}else if(update){
			enc.open(CPFolderHierarchy.FolderUpdate);
			enc.element(CPFolderHierarchy.Status,"1");
			enc.element(CPFolderHierarchy.SyncKey,newsynckey);
			enc.close();
		}
		statemachine.setSyncState(newsynckey, importer.getState());
		Conf.st.put("s"+newsynckey, seenfolders);
	}

}
