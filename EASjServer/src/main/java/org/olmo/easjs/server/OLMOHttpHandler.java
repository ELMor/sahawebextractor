package org.olmo.easjs.server;

import java.io.IOException;

public interface OLMOHttpHandler {
	public void handle(OLMOExchange x) throws IOException;
}
