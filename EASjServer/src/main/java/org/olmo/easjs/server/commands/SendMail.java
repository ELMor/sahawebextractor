package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.common.Util;
import org.olmo.easjs.Conf;
import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.wbxml.Debug;
import org.xml.sax.SAXException;


public class SendMail extends CommandHandler {

	@Override
	public void handle(EASHandler x, Backend backend, 
			String deviceid, String protocolversion, Logger log, String user, String pwd) throws SAXException,
			IOException, ParserConfigurationException, XPathExpressionException {

		byte input[]=Util.inputStreamToByteArray(x.getRequestBody());
		if(Conf.debug)
			Debug.dumpFile("sendMail.rfc", input);
		backend.sendMail(new String(input), false, false, null);
		return ;
	}

}
