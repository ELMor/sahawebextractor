package org.olmo.easjs.server;

import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.Conf;
import org.olmo.easjs.Cons;
import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.server.object.NodeMgr;
import org.olmo.easjs.server.object.SyncObject;
import org.xml.sax.SAXException;


public abstract class CommandHandler extends NodeMgr implements Cons {
	
	public abstract void handle(
			EASHandler x,
			Backend backend, 
			String deviceid, 
			String protocolversion, Logger log, String user, String pwd) 
		throws SAXException, IOException, ParserConfigurationException, XPathExpressionException;
	
	public boolean saveFolderData(String deviceId,Vector<SyncObject> folders){
		Vector<String> unique=new Vector<String>();
		Vector<SyncObject> selected=new Vector<SyncObject>();
		for(int i=0;i<folders.size();i++){
			SyncObject c=folders.elementAt(i);
			//No se guarda en compat el inbox
			if(c.type.equals(FOLDER_TYPE_INBOX)){
				continue;
			}
			if(!unique.contains(c.type) || c.parentid==null){
				selected.add(c);
				unique.add(c.type);
			}
		}
		if(!unique.contains(""+FOLDER_TYPE_APPOINTMENT)){
			SyncObject fakeCal=new SyncObject();
			fakeCal.type=""+FOLDER_TYPE_APPOINTMENT;
			fakeCal.id=FOLDER_TYPE_APPOINTMENT_ID;
			selected.add(fakeCal);
		}
		if(!unique.contains(""+FOLDER_TYPE_CONTACT)){
			SyncObject fakeCon=new SyncObject();
			fakeCon.type=""+FOLDER_TYPE_CONTACT;
			fakeCon.id=FOLDER_TYPE_CONTACT_ID;
			selected.add(fakeCon);
		}
		Conf.st.put("compat-"+deviceId, selected);
		return true;
	}
	
	public String getFolderID(String devid, String folderType){
		Vector<SyncObject> folders=(Vector<SyncObject>)Conf.st.get("compat-"+devid);
		if(folders==null)
			folders=new Vector<SyncObject>();
		if(folderType.equals("Calendar")){
			for(int i=0;i<folders.size();i++){
				SyncObject c=folders.elementAt(i);
				if(c.type.equals(FOLDER_TYPE_APPOINTMENT))
					return c.id;
			}
			return FOLDER_TYPE_APPOINTMENT_ID;
		}else if(folderType.equals("Contacts")){
			for(int i=0;i<folders.size();i++){
				SyncObject c=folders.elementAt(i);
				if(c.type.equals(FOLDER_TYPE_CONTACT))
					return c.id;
			}
			return FOLDER_TYPE_CONTACT_ID;
		}
		return null;
	}
	
	
}
