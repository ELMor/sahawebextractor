package org.olmo.easjs.server.object;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

import org.olmo.easjs.backend.ContImport;



public class Coleccion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8541679300531927744L;
	public int     						trunc;
	public Hashtable<String,SyncObject> clientIds;
	public Vector<String> 				fetchIds;
	public String         				type;
	public String         				syncKey;
	public String         				newSyncKey;
	public String         				id;
	public boolean        				deletesAsMoves;
	public boolean        				getChanges;
	public int            				maxItems;
	public int         	  				filter;
	public int            				rtfTruncation;
	public String         				mimeSupport;
	public int            				mimeTruncation;
	public int            				conflict;
	public Vector<SyncObject> 			syncstate;
	public boolean        				importedChanges;
	public transient ContImport			importer;
}
