package org.olmo.easjs.server.object;

import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPCalendar;
import org.w3c.dom.Node;

public class SyncAttendee extends SyncObject {
	private static final long serialVersionUID = 5049321644620995146L;
	public String name,email;
	@Override
	public void encode(EASEncoder xo) {
		xo.element(CPCalendar.AttendeeEmail,email);
		xo.element(CPCalendar.AttendeeName,name);
	}
	public SyncAttendee(Node n){
		if(n!=null){
			name=getTextSNN(n, CPCalendar.AttendeeName);
			email=getTextSNN(n, CPCalendar.AttendeeEmail);
		}else{
			name=email=null;
		}
	}
}
