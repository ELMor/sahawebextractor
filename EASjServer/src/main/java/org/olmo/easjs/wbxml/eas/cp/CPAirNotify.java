package org.olmo.easjs.wbxml.eas.cp;

public class CPAirNotify extends CodePage {

	public static final String Notify="AirNotify:Notify";
	public static final String Notification="AirNotify:Notification";
	public static final String Version="AirNotify:Version";
	public static final String Lifetime="AirNotify:Lifetime";
	public static final String DeviceInfo="AirNotify:DeviceInfo";
	public static final String Enable="AirNotify:Enable";
	public static final String Folder="AirNotify:Folder";
	public static final String ServerEntryId="AirNotify:ServerEntryId";
	public static final String DeviceAddress="AirNotify:DeviceAddress";
	public static final String ValidCarrierProfiles="AirNotify:ValidCarrierProfiles";
	public static final String CarrierProfile="AirNotify:CarrierProfile";
	public static final String Status="AirNotify:Status";
	public static final String Replies="AirNotify:Replies";
	public static final String Devices="AirNotify:Devices";
	public static final String Device="AirNotify:Device";
	public static final String Id="AirNotify:Id";
	public static final String Expiry="AirNotify:Expiry";
	public static final String NotifyGUID="AirNotify:NotifyGUID";

	public CPAirNotify() {
		super(3,"AirNotify");
        set(0x05 , Notify);
        set(0x06 , Notification);
        set(0x07 , Version);
        set(0x08 , Lifetime);
        set(0x09 , DeviceInfo);
        set(0x0a , Enable);
        set(0x0b , Folder);
        set(0x0c , ServerEntryId);
        set(0x0d , DeviceAddress);
        set(0x0e , ValidCarrierProfiles);
        set(0x0f , CarrierProfile);
        set(0x10 , Status);
        set(0x11 , Replies);
        set(0x12 , Devices);
        set(0x13 , Device);
        set(0x14 , Id);
        set(0x15 , Expiry);
        set(0x16 , NotifyGUID);
	}

}
