package org.olmo.easjs.wbxml.eas.cp;

public class CPCalendar extends CodePage {

	public static final String Timezone="Calendar:Timezone";
	public static final String AllDayEvent="Calendar:AllDayEvent";
	public static final String Attendees="Calendar:Attendees";
	public static final String Attendee="Calendar:Attendee";
	public static final String AttendeeEmail="Calendar:AttendeeEmail";
	public static final String AttendeeName="Calendar:AttendeeName";
	public static final String Body="Calendar:Body";
	public static final String BodyTruncated="Calendar:BodyTruncated";
	public static final String BusyStatus="Calendar:BusyStatus";
	public static final String Categories="Calendar:Categories";
	public static final String Category="Calendar:Category";
	public static final String Rtf="Calendar:Rtf";
	public static final String DtStamp="Calendar:DtStamp";
	public static final String EndTime="Calendar:EndTime";
	public static final String Exception="Calendar:Exception";
	public static final String Exceptions="Calendar:Exceptions";
	public static final String ExceptionDeleted="Calendar:ExceptionDeleted";
	public static final String ExceptionStartTime="Calendar:ExceptionStartTime";
	public static final String Location="Calendar:Location";
	public static final String MeetingStatus="Calendar:MeetingStatus";
	public static final String OrganizerEmail="Calendar:OrganizerEmail";
	public static final String OrganizerName="Calendar:OrganizerName";
	public static final String Recurrence="Calendar:Recurrence";
	public static final String RecurrenceType="Calendar:RecurrenceType";
	public static final String RecurrenceUntil="Calendar:RecurrenceUntil";
	public static final String RecurrenceOccurrences="Calendar:RecurrenceOccurrences";
	public static final String RecurrenceInterval="Calendar:RecurrenceInterval";
	public static final String RecurrenceDayOfWeek="Calendar:RecurrenceDayOfWeek";
	public static final String RecurrenceDayOfMonth="Calendar:RecurrenceDayOfMonth";
	public static final String RecurrenceWeekOfMonth="Calendar:RecurrenceWeekOfMonth";
	public static final String RecurrenceMonthOfYear="Calendar:RecurrenceMonthOfYear";
	public static final String ReminderMinsBefore="Calendar:ReminderMinsBefore";
	public static final String Sensitivity="Calendar:Sensitivity";
	public static final String Subject="Calendar:Subject";
	public static final String StartTime="Calendar:StartTime";
	public static final String UID="Calendar:UID";
	public static final String AttendeeStatus="Calendar:AttendeeStatus";
	public static final String AttendeeType="Calendar:AttendeeType";
	public static final String DisallowNewTimeProposal="Calendar:DisallowNewTimeProposal";
	public static final String ResponseRequested="Calendar:ResponseRequested";
	public static final String AppointmentReplyTime="Calendar:AppointmentReplyTime";
	public static final String ResponseType="Calendar:ResponseType";
	public static final String CalendarType="Calendar:CalendarType";
	public static final String IsLeapMonth="Calendar:sIsLeapMonth";

	public CPCalendar() {
		super(4,"Calendar");
        set(0x05 , Timezone);
        set(0x06 , AllDayEvent);
        set(0x07 , Attendees);
        set(0x08 , Attendee);
        set(0x09 , AttendeeEmail);
        set(0x0a , AttendeeName);
        set(0x0b , Body);
        set(0x0c , BodyTruncated);
        set(0x0d , BusyStatus);
        set(0x0e , Categories);
        set(0x0f , Category);
        set(0x10 , Rtf);
        set(0x11 , DtStamp);
        set(0x12 , EndTime);
        set(0x13 , Exception);
        set(0x14 , Exceptions);
        set(0x15 , ExceptionDeleted);
        set(0x16 , ExceptionStartTime);
        set(0x17 , Location);
        set(0x18 , MeetingStatus);
        set(0x19 , OrganizerEmail);
        set(0x1a , OrganizerName);
        set(0x1b , Recurrence);
        set(0x1c , RecurrenceType);
        set(0x1d , RecurrenceUntil);
        set(0x1e , RecurrenceOccurrences);
        set(0x1f , RecurrenceInterval);
        set(0x20 , RecurrenceDayOfWeek);
        set(0x21 , RecurrenceDayOfMonth);
        set(0x22 , RecurrenceWeekOfMonth);
        set(0x23 , RecurrenceMonthOfYear);
        set(0x24 , ReminderMinsBefore);
        set(0x25 , Sensitivity);
        set(0x26 , Subject);
        set(0x27 , StartTime);
        set(0x28 , UID);
        set(0x28 , AttendeeStatus);
        set(0x28 , AttendeeType);
        set(0x28 , DisallowNewTimeProposal);
        set(0x28 , ResponseRequested);
        set(0x28 , AppointmentReplyTime);
        set(0x28 , ResponseType);
        set(0x28 , CalendarType);
        set(0x28 , IsLeapMonth);
	
	}

}
