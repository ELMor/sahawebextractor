package org.olmo.easjs.wbxml.eas.cp;

import java.util.Hashtable;

public class CodePage {
	public static Hashtable<String, CodePage> namePages=new Hashtable<String, CodePage>();
	public static Hashtable<Integer, CodePage> codePages=new Hashtable<Integer, CodePage>();
	
	static {
		new CPAirNotify();
		new CPAirSync();
		new CPCalendar();
		new CPContacts();
		new CPContacts2();
		new CPEmail();
		new CPFolderHierarchy();
		new CPGAL();
		new CPItemEstimate();
		new CPMeetingResponse();
		new CPMove();
		new CPPing();
		new CPProvision();
		new CPResolveRecipients();
		new CPSearch();
		new CPTasks();
		new CPValidateCert();
	}
	
	public static CodePage getCodePage(String pageName){
		return namePages.get(pageName);
	}
	
	/**
	 * Nombre de la pagina
	 */
	String pageName;
	/**
	 * Codigo de la pagina
	 */
	int    pageCode;
	/**
	 * Mapa int->nombre
	 */
	Hashtable<Integer, String> codeName=new Hashtable<Integer, String>();
	/**
	 * Mapa nombre->int
	 */
	Hashtable<String, Integer> nameCode=new Hashtable<String, Integer>();
	/**
	 * Constructor
	 * @param pc Codigo de la pagina
	 * @param pn Nombre de la pagina
	 */
	public CodePage(int pc, String pn){
		pageName=pn;
		pageCode=pc;
		//A�adimos esta pagina al listin estatico de paginas
		namePages.put(pageName, this);
		codePages.put(pageCode, this);
	}
	/**
	 * A�ade un mapeo a la pagina
	 * @param code codigo de la etiqueta (tag)
	 * @param tag Nombre de la etiqueta
	 */
	public void set(int code, String tag){
		codeName.put(code, tag);
		nameCode.put(tag, code);
	}
	
	public int getCode(){
		return pageCode;
	}
	
	public String getName(){
		return pageName;
	}
	
	public int get(String tag){
		if(tag.indexOf(':')<0)
			tag=pageName+":"+tag;
		return nameCode.get(tag);
	}
	
	public String get(int code){
		return codeName.get(code);
	}
}
