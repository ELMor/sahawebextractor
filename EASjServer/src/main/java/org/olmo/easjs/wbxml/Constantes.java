package org.olmo.easjs.wbxml;

/** contains the WBXML constants */

public interface Constantes {

	public final int SWITCH_PAGE = 0;
	public final int END = 1;
	public final int ENTITY = 2;
	public final int STR_I = 3;
	public final int LITERAL = 4;
	public final int EXT_I_0 = 0x40;
	public final int EXT_I_1 = 0x41;
	public final int EXT_I_2 = 0x42;
	public final int PI = 0x43;
	public final int LITERAL_C = 0x44;
	public final int EXT_T_0 = 0x80;
	public final int EXT_T_1 = 0x81;
	public final int EXT_T_2 = 0x82;
	public final int STR_T = 0x83;
	public final int LITERAL_A = 0x084;
	public final int EXT_0 = 0x0c0;
	public final int EXT_1 = 0x0c1;
	public final int EXT_2 = 0x0c2;
	public final int OPAQUE = 0x0c3;
	public final int LITERAL_AC = 0x0c4;
}
