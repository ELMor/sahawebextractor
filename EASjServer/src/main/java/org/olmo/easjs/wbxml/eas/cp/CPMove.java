package org.olmo.easjs.wbxml.eas.cp;

public class CPMove extends CodePage {

	public static final String Moves="Move:Moves";
	public static final String Move="Move:Move";
	public static final String SrcMsgId="Move:SrcMsgId";
	public static final String SrcFldId="Move:SrcFldId";
	public static final String DstFldId="Move:DstFldId";
	public static final String Response="Move:Response";
	public static final String Status="Move:Status";
	public static final String DstMsgId="Move:DstMsgId";

	public CPMove() {
		super(5,"Move");
        set(0x05 , Moves);
        set(0x06 , Move);
        set(0x07 , SrcMsgId);
        set(0x08 , SrcFldId);
        set(0x09 , DstFldId);
        set(0x0a , Response);
        set(0x0b , Status);
        set(0x0c , DstMsgId);
	}

}
