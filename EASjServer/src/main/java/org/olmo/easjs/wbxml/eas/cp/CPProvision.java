package org.olmo.easjs.wbxml.eas.cp;

public class CPProvision extends CodePage {

	public static final String Provision= "Provision:Provision";
	public static final String Policies= "Provision:Policies";
	public static final String Policy= "Provision:Policy";
	public static final String PolicyType= "Provision:PolicyType";
	public static final String PolicyKey= "Provision:PolicyKey";
	public static final String Data= "Provision:Data";
	public static final String Status= "Provision:Status";
	public static final String RemoteWipe= "Provision:RemoteWipe";
	public static final String EASProvisionDoc= "Provision:EASProvisionDoc";
	public static final String DevicePasswordEnabled="Provision:DevicePasswordEnabled"; 
	public static final String AlphanumericDevicePasswordRequired="Provision:AlphanumericDevicePasswordRequired"; 
	public static final String DeviceEncryptionEnabled="Provision:DeviceEncryptionEnabled"; 

	public CPProvision() {
		super(14,"Provision");
        set(0x05 , Provision);
        set(0x06 , Policies);
        set(0x07 , Policy);
        set(0x08 , PolicyType);
        set(0x09 , PolicyKey);
        set(0x0A , Data);
        set(0x0B , Status);
        set(0x0C , RemoteWipe);
        set(0x0D , EASProvisionDoc);
        set(0x0E , DevicePasswordEnabled);
        set(0x0F , AlphanumericDevicePasswordRequired);
        set(0x10 , DeviceEncryptionEnabled);
	}

}
