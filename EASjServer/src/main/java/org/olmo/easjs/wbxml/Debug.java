package org.olmo.easjs.wbxml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.olmo.common.Util;
import org.olmo.easjs.Conf;
import org.w3c.dom.Document;


public class Debug {
	public static File dd=new File(Conf.debugDir);
	public static int outNumber=0;

	static {
		Util.removeSingleFiles(dd);
	}
	
	public static String dumpFile(String extension, String xml, boolean tryBeautify){
		String bonito= tryBeautify ? beautify(xml) : xml ;
		String fn=null;
		if(bonito!=null)
			fn=dumpFile(extension, bonito.getBytes());
		else
			fn=dumpFile(extension, xml.getBytes());
		return fn;
	}
	
	
	public static String dumpFile(String extension, byte b[]){
		String fn=""+String.format("%06d", ++outNumber)+"-"+extension;
		File f=new File(dd,fn);
		try {
			FileOutputStream fos=new FileOutputStream(f);
			fos.write(b);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fn;
	}
	
    public static String beautify(String xml) {
        
        try {
	    	DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
	    	DocumentBuilder db=dbf.newDocumentBuilder();
	    	Document doc=db.parse(new ByteArrayInputStream(xml.getBytes()));
	    	ByteArrayOutputStream baos= new ByteArrayOutputStream();
	        TransformerFactory tfactory = TransformerFactory.newInstance();
	        Transformer serializer;
            serializer = tfactory.newTransformer();
            //Setup indenting to "pretty print"
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            serializer.transform(new DOMSource(doc), new StreamResult(baos));
            return new String(baos.toByteArray());
        } catch (Exception e) {
            // this is fatal, just dump the stack and throw a runtime exception
            //e.printStackTrace();
        }
        return null;
    }

}
