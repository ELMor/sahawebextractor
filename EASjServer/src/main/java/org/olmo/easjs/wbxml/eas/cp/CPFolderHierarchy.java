package org.olmo.easjs.wbxml.eas.cp;

public class CPFolderHierarchy extends CodePage {

	public static final String Folders="FolderHierarchy:Folders";
	public static final String Folder="FolderHierarchy:Folder";
	public static final String DisplayName="FolderHierarchy:DisplayName";
	public static final String ServerEntryId="FolderHierarchy:ServerEntryId";
	public static final String ParentId="FolderHierarchy:ParentId";
	public static final String Type="FolderHierarchy:Type";
	public static final String Response="FolderHierarchy:Response";
	public static final String Status="FolderHierarchy:Status";
	public static final String ContentClass="FolderHierarchy:ContentClass";
	public static final String Changes="FolderHierarchy:Changes";
	public static final String Add="FolderHierarchy:Add";
	public static final String Remove="FolderHierarchy:Remove";
	public static final String Update="FolderHierarchy:Update";
	public static final String SyncKey="FolderHierarchy:SyncKey";
	public static final String FolderCreate="FolderHierarchy:FolderCreate";
	public static final String FolderDelete="FolderHierarchy:FolderDelete";
	public static final String FolderUpdate="FolderHierarchy:FolderUpdate";
	public static final String FolderSync="FolderHierarchy:FolderSync";
	public static final String Count="FolderHierarchy:Count";
	public static final String Version="FolderHierarchy:Version";

	public CPFolderHierarchy() {
		super(7,"FolderHierarchy");
        set(0x05 , Folders);
        set(0x06 , Folder);
        set(0x07 , DisplayName);
        set(0x08 , ServerEntryId);
        set(0x09 , ParentId);
        set(0x0a , Type);
        set(0x0b , Response);
        set(0x0c , Status);
        set(0x0d , ContentClass);
        set(0x0e , Changes);
        set(0x0f , Add);
        set(0x10 , Remove);
        set(0x11 , Update);
        set(0x12 , SyncKey);
        set(0x13 , FolderCreate);
        set(0x14 , FolderDelete);
        set(0x15 , FolderUpdate);
        set(0x16 , FolderSync);
        set(0x17 , Count);
        set(0x18 , Version);
	
	}

}
