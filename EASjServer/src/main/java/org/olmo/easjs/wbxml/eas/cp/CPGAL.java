package org.olmo.easjs.wbxml.eas.cp;

public class CPGAL extends CodePage {

	public static final String DisplayName= "GAL:DisplayName";
	public static final String Phone= "GAL:Phone";
	public static final String Office= "GAL:Office";
	public static final String Title= "GAL:Title";
	public static final String Company= "GAL:Company";
	public static final String Alias= "GAL:Alias";
	public static final String FirstName= "GAL:FirstName";
	public static final String LastName= "GAL:LastName";
	public static final String HomePhone= "GAL:HomePhone";
	public static final String MobilePhone= "GAL:MobilePhone";
	public static final String EmailAddress= "GAL:EmailAddress";

	public CPGAL() {
		super(16,"GAL");
        set(0x05 , DisplayName);
        set(0x06 , Phone);
        set(0x07 , Office);
        set(0x08 , Title);
        set(0x09 , Company);
        set(0x0A , Alias);
        set(0x0B , FirstName);
        set(0x0C , LastName);
        set(0x0D , HomePhone);
        set(0x0E , MobilePhone);
        set(0x0F , EmailAddress);
	}

}
