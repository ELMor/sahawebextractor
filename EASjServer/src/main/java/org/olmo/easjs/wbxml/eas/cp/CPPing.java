package org.olmo.easjs.wbxml.eas.cp;

public class CPPing extends CodePage {

	public static final String Ping="Ping:Ping";
	public static final String Status="Ping:Status";
	public static final String LifeTime= "Ping:LifeTime";
	public static final String Folders= "Ping:Folders";
	public static final String Folder= "Ping:Folder";
	public static final String ServerEntryId= "Ping:ServerEntryId";
	public static final String FolderType= "Ping:FolderType";

	public CPPing() {
		super(13,"Ping");
        set(0x05 , Ping);
        set(0x07 , Status);
        set(0x08 , LifeTime);
        set(0x09 , Folders);
        set(0x0a , Folder);
        set(0x0b , ServerEntryId);
        set(0x0c , FolderType);
	}

}
