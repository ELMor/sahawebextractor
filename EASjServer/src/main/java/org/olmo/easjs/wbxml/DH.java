package org.olmo.easjs.wbxml;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class DH extends DefaultHandler {
	public StringBuffer xml=new StringBuffer();
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		xml.append(new String(ch,start,length));
	}

	@Override
	public void endElement(String uri, String localName, String name)
			throws SAXException {
		super.endElement(uri, localName, name);
		xml.append("</").append(uri).append(">");
	}

	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String name,
			Attributes a) throws SAXException {
		super.startElement(uri, localName, name, a);
		xml.append("<").append(uri);
		for(int i=0;i<a.getLength();i++){
			xml.append(" ").append(a.getQName(i)).append("='").append(a.getValue(i)).append("'");
		}
		xml.append(">");
	}

}
