// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.tool;

import java.util.Enumeration;
import java.util.NoSuchElementException;

// Referenced classes of package com.archimed.tool:
//            m

class z
    implements Enumeration
{

    boolean a;
    int b;
    m c[];
    m d;

    z(m am[], boolean flag)
    {
        c = am;
        a = flag;
        b = am.length;
    }

    public boolean hasMoreElements()
    {
        if(d != null)
            return true;
        while(b-- > 0) 
            if((d = c[b]) != null)
                return true;
        return false;
    }

    public Object nextElement()
    {
        if(d == null)
            while(b-- > 0 && (d = c[b]) == null) ;
        if(d != null)
        {
            m m1 = d;
            d = m1.d;
            return ((Object) (a ? new Integer(m1.b) : new Integer(m1.c)));
        } else
        {
            throw new NoSuchElementException("IntHashtableEnumerator");
        }
    }
}
