// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.tool;


public class n
{

    public n()
    {
    }

    public static String fstr(String s, int i)
    {
        String s1;
        if(s.length() >= i)
        {
            s1 = s.substring(0, s.length() - 3) + "...";
        } else
        {
            s1 = s;
            for(int j = 0; j < i - s.length(); j++)
                s1 = s1 + " ";

        }
        return s1;
    }

    public static byte[] trimZeros(byte abyte0[])
    {
        int i = 0;
        int j = abyte0.length;
        for(int k = abyte0.length - 1; k >= 0; k--)
        {
            if(abyte0[k] != 0)
                break;
            j--;
        }

        for(int l = 0; l < j; l++)
        {
            if(abyte0[l] != 0)
                break;
            i++;
        }

        byte abyte1[] = new byte[j - i];
        System.arraycopy(((Object) (abyte0)), i, ((Object) (abyte1)), 0, j - i);
        return abyte1;
    }
}
