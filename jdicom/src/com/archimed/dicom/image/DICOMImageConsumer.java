// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.image;

import com.archimed.dicom.DicomException;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.IndexColorModel;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.image:
//            ImageIO, GrayColorModel, DicomImage

class DICOMImageConsumer
    implements ImageConsumer
{

    DicomImage a;
    ImageIO b;
    byte c[];
    int d[];
    int e;
    int f;
    ColorModel g;
    boolean h;
    public boolean ready;

    public DICOMImageConsumer(ImageIO imageio)
    {
        b = imageio;
        a = imageio.a;
        ready = false;
    }

    boolean a()
    {
        if(h)
            if(g instanceof IndexColorModel)
            {
                IndexColorModel indexcolormodel = (IndexColorModel)g;
                for(int i1 = 0; i1 < indexcolormodel.getMapSize(); i1++)
                {
                    int i = indexcolormodel.getRed(i1);
                    int k = indexcolormodel.getGreen(i1);
                    int l = indexcolormodel.getBlue(i1);
                    if(i != k || k != l)
                        return false;
                }

                return true;
            } else
            {
                System.out.println("??? bytes + not indexcolor");
                return false;
            }
        if(g instanceof GrayColorModel)
            return true;
        if(g instanceof DirectColorModel)
        {
            for(int j = 0; j < e * f; j++)
                if(g.getRed(d[j]) != g.getGreen(d[j]) || g.getGreen(d[j]) != g.getBlue(d[j]))
                    return false;

            return true;
        } else
        {
            System.out.println("??? ints + not directcolor");
            return false;
        }
    }

    Vector b()
    {
        Vector vector = new Vector(256);
        if(h)
        {
            IndexColorModel indexcolormodel = (IndexColorModel)g;
            for(int l = 0; l < indexcolormodel.getMapSize(); l++)
            {
                int i = (indexcolormodel.getRed(l) << 16) + (indexcolormodel.getGreen(l) << 8) + indexcolormodel.getBlue(l);
                vector.addElement(((Object) (new Integer(i))));
            }

            return vector;
        }
        for(int k = 0; k < e * f; k++)
        {
            int j = d[k];
            if(!vector.contains(((Object) (new Integer(j)))))
            {
                vector.addElement(((Object) (new Integer(j))));
                if(vector.size() > 256)
                    return null;
            }
        }

        return vector;
    }

    boolean c()
    {
        IndexColorModel indexcolormodel = (IndexColorModel)g;
        for(int i = 0; i < indexcolormodel.getMapSize(); i++)
            if(indexcolormodel.getRed(i) != i)
                return false;

        return true;
    }

    void d()
    {
        if(g instanceof IndexColorModel)
        {
            if(c())
            {
                try
                {
                    a.imagePixelData(f, e, 8, 8, 7, c);
                }
                catch(DicomException dicomexception)
                {
                    ((Throwable) (dicomexception)).printStackTrace();
                }
            } else
            {
                byte abyte0[] = new byte[e * f];
                for(int j = 0; j < e * f; j++)
                    abyte0[j] = (byte)g.getRed(((int) (c[j])));

                try
                {
                    a.imagePixelData(f, e, 8, 8, 7, abyte0);
                }
                catch(DicomException dicomexception1)
                {
                    ((Throwable) (dicomexception1)).printStackTrace();
                }
            }
        } else
        if(g instanceof GrayColorModel)
        {
            int i = g.getPixelSize();
            int k = i / 8;
            byte abyte2[] = new byte[e * f * k];
            for(int i1 = 0; i1 < e * f; i1++)
            {
                for(int j1 = 0; j1 < k; j1++)
                    abyte2[k * i1 + j1] = (byte)((d[i1] & 255 << j1 * 8) >> j1 * 8);

            }

            try
            {
                a.imagePixelData(f, e, i, i, i - 1, abyte2);
            }
            catch(DicomException dicomexception3)
            {
                ((Throwable) (dicomexception3)).printStackTrace();
            }
        } else
        if(g instanceof DirectColorModel)
        {
            byte abyte1[] = new byte[e * f];
            for(int l = 0; l < e * f; l++)
                abyte1[l] = (byte)(d[l] & 0xff);

            try
            {
                a.imagePixelData(f, e, 8, 8, 7, abyte1);
            }
            catch(DicomException dicomexception2)
            {
                ((Throwable) (dicomexception2)).printStackTrace();
            }
        } else
        {
            System.out.println("cm: " + g);
        }
    }

    void a(Vector vector)
    {
        int i = vector.size();
        byte abyte0[] = new byte[256];
        byte abyte1[] = new byte[256];
        byte abyte2[] = new byte[256];
        for(int j = 0; j < i; j++)
        {
            abyte0[j] = (byte)((((Integer)vector.elementAt(j)).intValue() & 0xff0000) >> 16);
            abyte2[j] = (byte)((((Integer)vector.elementAt(j)).intValue() & 0xff00) >> 8);
            abyte1[j] = (byte)(((Integer)vector.elementAt(j)).intValue() & 0xff);
        }

        if(g instanceof IndexColorModel)
            try
            {
                a.imagePixelData(f, e, c, abyte0, abyte2, abyte1);
            }
            catch(DicomException dicomexception)
            {
                ((Throwable) (dicomexception)).printStackTrace();
            }
        else
        if(g instanceof DirectColorModel)
        {
            Hashtable hashtable = new Hashtable(i);
            for(int k = 0; k < i; k++)
                hashtable.put(vector.elementAt(k), ((Object) (new Integer(k))));

            c = new byte[e * f];
            for(int l = 0; l < e * f; l++)
                c[l] = (byte)((Integer)hashtable.get(((Object) (new Integer(d[l]))))).intValue();

            try
            {
                a.imagePixelData(f, e, c, abyte0, abyte2, abyte1);
            }
            catch(DicomException dicomexception1)
            {
                ((Throwable) (dicomexception1)).printStackTrace();
            }
        }
    }

    void e()
    {
        try
        {
            a.imagePixelData(f, e, 0, d);
        }
        catch(DicomException dicomexception)
        {
            ((Throwable) (dicomexception)).printStackTrace();
        }
    }

    public void imageComplete(int i)
    {
        if(i == 3)
        {
            boolean flag = a();
            if(flag)
            {
                d();
                ready = true;
                synchronized(b)
                {
                    ((Object) (b)).notify();
                }
                return;
            }
            Vector vector = b();
            if(vector != null)
            {
                a(vector);
                ready = true;
                synchronized(b)
                {
                    ((Object) (b)).notify();
                }
                return;
            }
            e();
            ready = true;
            synchronized(b)
            {
                ((Object) (b)).notify();
            }
            return;
        } else
        {
            return;
        }
    }

    public void setColorModel(ColorModel colormodel)
    {
        g = colormodel;
    }

    public void setDimensions(int i, int j)
    {
        e = i;
        f = j;
    }

    public void setHints(int i)
    {
    }

    public void setPixels(int i, int j, int k, int l, ColorModel colormodel, byte abyte0[], int i1, 
            int j1)
    {
        if(i != 0 || j != 0 || k != e || l != f)
        {
            if(c == null)
                c = new byte[e * f];
            for(int k1 = j; k1 < j + l; k1++)
                System.arraycopy(((Object) (abyte0)), 0, ((Object) (c)), i + k1 * e, i + k);

        } else
        {
            c = abyte0;
        }
        h = true;
    }

    public void setPixels(int i, int j, int k, int l, ColorModel colormodel, int ai[], int i1, 
            int j1)
    {
        if(i != 0 || j != 0 || k != e || l != f)
        {
            if(d == null)
                d = new int[e * f];
            for(int k1 = j; k1 < j + l; k1++)
                System.arraycopy(((Object) (ai)), 0, ((Object) (d)), i + k1 * e, i + k);

        } else
        {
            d = ai;
        }
        h = false;
    }

    public void setProperties(Hashtable hashtable)
    {
    }
}
