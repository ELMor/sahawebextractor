// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            g, OffsetInputStream, DTime, DTimeRange, 
//            DDateTime, DDateTimeRange, Person, DDate, 
//            DDateRange, h, DicomUtils, DDict

class f extends g
{

    Vector a;

    f(InputStream inputstream)
    {
        super(inputstream);
    }

    f(InputStream inputstream, int i1)
    {
        super(inputstream, i1);
    }

    Vector a(int i1, int j1)
        throws IOException
    {
        a = new Vector();
        ((OffsetInputStream)this).resetCounter();
        for(int k1 = 0; k1 < j1; k1++)
            a.addElement(((g)this).b(i1));

        return a;
    }

    String a(String s)
    {
        return s;
    }

    String b(String s)
    {
        return s;
    }

    String c(String s)
    {
        return s;
    }

    String d(String s)
    {
        return s;
    }

    String e(String s)
    {
        return s;
    }

    String f(String s)
    {
        return s;
    }

    Object g(String s)
    {
        try
        {
            return ((Object) (new DTime(s)));
        }
        catch(NumberFormatException numberformatexception)
        {
            return ((Object) (new DTimeRange(s)));
        }
    }

    String h(String s)
    {
        return s;
    }

    Object i(String s)
    {
        try
        {
            return ((Object) (new DDateTime(s)));
        }
        catch(NumberFormatException numberformatexception)
        {
            return ((Object) (new DDateTimeRange(s)));
        }
    }

    String j(String s)
    {
        return s;
    }

    Float k(String s)
    {
        return new Float(s);
    }

    Integer l(String s)
    {
        return new Integer(s);
    }

    Person m(String s)
    {
        return new Person(s);
    }

    Object n(String s)
    {
        try
        {
            return ((Object) (new DDate(s)));
        }
        catch(NumberFormatException numberformatexception)
        {
            return ((Object) (new DDateRange(s)));
        }
    }

    Object a(String s, int i1)
    {
        switch(i1)
        {
        case 4: // '\004'
            return ((Object) (a(s)));

        case 9: // '\t'
            return ((Object) (b(s)));

        case 16: // '\020'
            return ((Object) (k(s)));

        case 11: // '\013'
            return n(s);

        case 15: // '\017'
            return ((Object) (l(s)));

        case 6: // '\006'
            return ((Object) (c(s)));

        case 18: // '\022'
            return ((Object) (d(s)));

        case 14: // '\016'
            return ((Object) (m(s)));

        case 7: // '\007'
            return ((Object) (e(s)));

        case 13: // '\r'
            return ((Object) (f(s)));

        case 12: // '\f'
            return g(s);

        case 27: // '\033'
            return ((Object) (h(s)));

        case 28: // '\034'
            return i(s);

        case 2: // '\002'
            return ((Object) (j(s)));

        case 3: // '\003'
        case 5: // '\005'
        case 8: // '\b'
        case 10: // '\n'
        case 17: // '\021'
        case 19: // '\023'
        case 20: // '\024'
        case 21: // '\025'
        case 22: // '\026'
        case 23: // '\027'
        case 24: // '\030'
        case 25: // '\031'
        case 26: // '\032'
        default:
            return ((Object) (null));
        }
    }

    Vector a(int i1)
        throws IOException
    {
        a = new Vector();
        for(StringTokenizer stringtokenizer = new StringTokenizer(((h)this).d(i1), "\\"); stringtokenizer.hasMoreTokens(); a.addElement(((Object) (stringtokenizer.nextToken()))));
        return a;
    }

    Vector b(int i1, int j1)
        throws IOException
    {
        Vector vector;
        if(i1 == 11 && j1 % 8 == 0)
        {
            String s = ((h)this).d(j1).trim();
            if(s.length() != j1)
                throw new IllegalArgumentException();
            vector = new Vector();
            for(int l1 = 0; l1 < j1 / 8; l1++)
                vector.addElement(((Object) (s.substring(l1 * 8, (l1 + 1) * 8))));

        } else
        {
            vector = a(j1);
        }
        for(int k1 = 0; k1 < vector.size(); k1++)
        {
            Object obj = a(((String)vector.elementAt(k1)).trim(), i1);
            vector.setElementAt(obj, k1);
        }

        return vector;
    }

    Vector c(int i1, int j1)
        throws IOException
    {
        byte abyte0[] = new byte[j1];
        ((OffsetInputStream)this).readFully(abyte0);
        if(i1 == 24 && ((h)this).j() == 8195)
            DicomUtils.a(abyte0);
        a = new Vector();
        a.addElement(((Object) (abyte0)));
        return a;
    }

    Vector d(int i1, int j1)
        throws IOException
    {
        int k1 = DDict.c(i1);
        if(k1 != 0)
            return a(i1, j1 / k1);
        switch(i1)
        {
        case 0: // '\0'
        case 8: // '\b'
        case 22: // '\026'
        case 24: // '\030'
            return c(i1, j1);
        }
        return b(i1, j1);
    }
}
