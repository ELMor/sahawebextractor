// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;


public class DDictEntry
{

    int a;
    int b;
    int c;
    String d;
    String e;

    public DDictEntry(int i, int j, int k, String s, String s1)
    {
        a = i;
        b = j;
        c = k;
        d = s;
        e = s1;
    }

    public int getGroup()
    {
        return a;
    }

    public int getElement()
    {
        return b;
    }

    public String getDescription()
    {
        return d;
    }

    public int getType()
    {
        return c;
    }

    public String getVM()
    {
        return e;
    }
}
