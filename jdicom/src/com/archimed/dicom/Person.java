// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;


public class Person
{

    public String familyName;
    public String givenName;
    public String middleName;
    public String prefix;
    public String suffix;

    public Person(String s)
    {
        familyName = "";
        givenName = "";
        middleName = "";
        prefix = "";
        suffix = "";
        int i;
        if((i = s.indexOf("^")) != -1)
        {
            familyName = s.substring(0, i);
        } else
        {
            familyName = s;
            return;
        }
        int j;
        if((j = s.indexOf("^", i + 1)) != -1)
        {
            givenName = s.substring(i + 1, j);
        } else
        {
            givenName = s.substring(i + 1);
            return;
        }
        if((i = s.indexOf("^", j + 1)) != -1)
        {
            middleName = s.substring(j + 1, i);
        } else
        {
            middleName = s.substring(j + 1);
            return;
        }
        if((j = s.indexOf("^", i + 1)) != -1)
        {
            prefix = s.substring(i + 1, j);
        } else
        {
            prefix = s.substring(i + 1);
            return;
        }
        suffix = s.substring(j + 1);
    }

    public String toString()
    {
        String s = "";
        if(!prefix.equals(""))
            s = s + prefix + " ";
        if(!givenName.equals(""))
            s = s + givenName + " ";
        if(!middleName.equals(""))
            s = s + middleName + " ";
        s = s + familyName;
        if(!suffix.equals(""))
            s = s + ", " + suffix;
        return s;
    }

    public String toDICOMString()
    {
        return a('^');
    }

    String a(char c)
    {
        String s = familyName;
        if(!givenName.equals("") || !middleName.equals("") || !prefix.equals("") || !suffix.equals(""))
            s = s + c + givenName;
        if(!middleName.equals("") || !prefix.equals("") || !suffix.equals(""))
            s = s + c + middleName;
        if(!prefix.equals("") || !suffix.equals(""))
            s = s + c + prefix;
        if(!suffix.equals(""))
            s = s + c + suffix;
        return s;
    }
}
