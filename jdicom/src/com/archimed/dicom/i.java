// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            j, Person, DDate, DDateRange, 
//            DDateTime, DDateTimeRange, DTime, DTimeRange, 
//            k, OffsetOutputStream, DicomUtils, DDict

class i extends j
{

    Vector a;

    i(OutputStream outputstream)
    {
        super(outputstream);
    }

    i(OutputStream outputstream, int l)
    {
        super(outputstream, l);
    }

    private int d(int l)
    {
        switch(l)
        {
        case 2: // '\002'
        case 8: // '\b'
        case 22: // '\026'
        case 24: // '\030'
            return 0;
        }
        return 32;
    }

    int a(Vector vector, int l)
        throws IOException
    {
        int i1 = 0;
        for(int j1 = 0; j1 < vector.size(); j1++)
            i1 += ((j)this).b(vector.elementAt(j1), l);

        return i1;
    }

    String a(Object obj, int l)
    {
        switch(l)
        {
        case 2: // '\002'
        case 4: // '\004'
        case 6: // '\006'
        case 7: // '\007'
        case 9: // '\t'
        case 13: // '\r'
        case 18: // '\022'
        case 27: // '\033'
            return (String)obj;

        case 16: // '\020'
            return ((Float)obj).toString();

        case 15: // '\017'
            return ((Integer)obj).toString();

        case 14: // '\016'
            return ((Person)obj).a('^');

        case 11: // '\013'
            try
            {
                return ((DDate)obj).toDICOMString();
            }
            catch(ClassCastException classcastexception)
            {
                return ((DDateRange)obj).toDICOMString();
            }

        case 28: // '\034'
            try
            {
                return ((DDateTime)obj).toDICOMString();
            }
            catch(ClassCastException classcastexception1)
            {
                return ((DDateTimeRange)obj).toDICOMString();
            }

        case 12: // '\f'
            try
            {
                return ((DTime)obj).toDICOMString();
            }
            catch(ClassCastException classcastexception2)
            {
                return ((DTimeRange)obj).toDICOMString();
            }

        case 0: // '\0'
            return new String((byte[])obj);

        case 1: // '\001'
        case 3: // '\003'
        case 5: // '\005'
        case 8: // '\b'
        case 10: // '\n'
        case 17: // '\021'
        case 19: // '\023'
        case 20: // '\024'
        case 21: // '\025'
        case 22: // '\026'
        case 23: // '\027'
        case 24: // '\030'
        case 25: // '\031'
        case 26: // '\032'
        default:
            return null;
        }
    }

    int b(Vector vector, int l)
        throws IOException
    {
        int i1 = 0;
        for(int j1 = 0; j1 < vector.size(); j1++)
        {
            String s = a(vector.elementAt(j1), l);
            i1 += ((k)this).a(s);
            if(j1 != vector.size() - 1)
            {
                ((OffsetOutputStream)this).write(92);
                i1++;
            }
        }

        return i1;
    }

    int c(Vector vector, int l)
        throws IOException
    {
        byte abyte0[] = (byte[])vector.elementAt(0);
        if(l == 24 && ((k)this).a() == 8195)
        {
            byte abyte1[] = new byte[abyte0.length];
            System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), 0, abyte1.length);
            DicomUtils.a(abyte1);
            ((OffsetOutputStream)this).write(abyte1);
            return abyte1.length;
        } else
        {
            ((OffsetOutputStream)this).write(abyte0);
            return abyte0.length;
        }
    }

    int d(Vector vector, int l)
        throws IOException
    {
        int i1 = DDict.c(l);
        int j1 = 0;
        if(i1 != 0)
            return a(vector, l);
        switch(l)
        {
        case 8: // '\b'
        case 22: // '\026'
        case 24: // '\030'
            j1 += c(vector, l);
            break;

        default:
            j1 += b(vector, l);
            break;
        }
        if(j1 % 2 == 1)
        {
            ((OffsetOutputStream)this).write(d(l));
            j1++;
        }
        return j1;
    }
}
