// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.codec;

import com.archimed.codec.jpeg.p;
import com.archimed.codec.jpeg.w;
import com.archimed.codec.rle.u;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Compression
{

    DicomObject a;

    public Compression()
    {
        a = null;
    }

    public Compression(DicomObject dicomobject)
        throws DicomException
    {
        setDicomObject(dicomobject);
    }

    public void decompress()
        throws DicomException, IOException
    {
        if(a == null)
            throw new DicomException("Use setDicomObject first to specify a DicomObject");
        int i;
        try
        {
            DicomObject dicomobject = a.getFileMetaInformation();
            if(dicomobject == null)
                throw new DicomException("no file meta information");
            i = UID.getUIDEntry(dicomobject.getS(31)).getConstant();
        }
        catch(UnknownUIDException unknownuidexception)
        {
            throw new DicomException("Decompression not possible: " + ((Throwable) (unknownuidexception)).getMessage());
        }
        catch(Exception exception)
        {
            throw new DicomException("Decompression not possible: no transfersyntax found");
        }
        int j = a.getI(467);
        int k = a.getI(466);
        int l = a.getSize(1184) - 1;
        int i1 = a.getI(475);
        int j1 = a.getI(461);
        int k1 = j * k * (i1 >> 3) * j1;
        byte abyte0[] = i != 8197 ? null : new byte[k1];
        a.deleteItem(1184, 0);
        if(l == 1)
        {
            abyte0 = decompressFrame(i, (byte[])a.deleteItem(1184, 0), j, k, abyte0);
            a.set(1184, ((Object) (abyte0)));
        } else
        {
            byte abyte1[] = new byte[k1 * l];
            for(int l1 = 0; l1 < l; l1++)
            {
                abyte0 = decompressFrame(i, (byte[])a.deleteItem(1184, 0), j, k, abyte0);
                System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), l1 * k1, k1);
            }

            a.set(1184, ((Object) (abyte1)));
        }
        try
        {
            a.getFileMetaInformation().set(31, ((Object) (UID.getUIDEntry(8194).getValue())));
        }
        catch(IllegalValueException illegalvalueexception) { }
    }

    public static byte[] decompressFrame(int i, byte abyte0[], int j, int k, byte abyte1[])
        throws DicomException, IOException
    {
        if(i == 8197)
        {
            p p1;
            try
            {
                p1 = (p)Class.forName("com.tiani.codec.jpeg.Jpeg14Decoder").newInstance();
                if(Debug.DEBUG > 1)
                    Debug.out.println("Use com.tiani.codec.jpeg.Jpeg14Decoder");
            }
            catch(Exception exception)
            {
                p1 = new p();
                if(Debug.DEBUG > 1)
                    Debug.out.println("Use com.archimed.codec.jpeg.Jpeg14Decoder");
            }
            byte abyte2[] = p1.decode(abyte0, abyte1);
            return abyte2;
        }
        if(i == 8198)
        {
            ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(abyte0);
            u u1 = new u();
            u1.setDimension(j, k);
            return u1.read(((java.io.InputStream) (bytearrayinputstream)));
        }
        if(i == 8196)
        {
            java.awt.Image image = Toolkit.getDefaultToolkit().createImage(abyte0);
            PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, j, k, false);
            try
            {
                pixelgrabber.grabPixels();
            }
            catch(InterruptedException interruptedexception)
            {
                ((Throwable) (interruptedexception)).printStackTrace();
            }
            try
            {
                return (byte[])pixelgrabber.getPixels();
            }
            catch(ClassCastException classcastexception)
            {
                return a((int[])pixelgrabber.getPixels());
            }
        } else
        {
            throw new DicomException("unsupported transfer syntax for decompression");
        }
    }

    private static byte[] a(int ai[])
    {
        byte abyte0[] = new byte[ai.length * 3];
        for(int i = 0; i < ai.length; i++)
        {
            abyte0[3 * i] = (byte)((ai[i] & 0xff0000) >> 16);
            abyte0[3 * i + 1] = (byte)((ai[i] & 0xff00) >> 8);
            abyte0[3 * i + 2] = (byte)(ai[i] & 0xff);
        }

        return abyte0;
    }

    public void compress(int i)
        throws IOException, DicomException
    {
        if(a == null)
            throw new DicomException("Use setDicomObject first to specify a DicomObject");
        if(i != 8197 && i != 8198)
            throw new DicomException("unsupported transfer syntax for compression");
        int j;
        try
        {
            DicomObject dicomobject = a.getFileMetaInformation();
            if(dicomobject == null)
                j = 8193;
            else
            if(dicomobject.getSize(31) > 0)
                j = UID.getUIDEntry(dicomobject.getS(31)).getConstant();
            else
                j = 8193;
        }
        catch(UnknownUIDException unknownuidexception)
        {
            throw new DicomException("Decompression not possible: " + ((Throwable) (unknownuidexception)).getMessage());
        }
        if(j != 8195 && j != 8194 && j != 8193)
            throw new DicomException("associated DicomObject already has a compressed transfer syntax");
        int k = a.getI(467);
        int l = a.getI(466);
        int i1 = 1;
        if(a.getSize(464) > 0)
            i1 = a.getI(464);
        byte abyte0[] = (byte[])a.get(1184);
        a.deleteItem(1184, 0);
        a.set(1184, ((Object) (new byte[0])), 0);
        int j1 = a.getI(475);
        int k1 = a.getI(461);
        if(i1 == 1 && k1 == 1)
        {
            a.set(1184, ((Object) (compressFrame(i, abyte0, k, l))), 1);
            return;
        }
        int l1 = k * l * (j1 >> 3);
        if(k1 == 1)
        {
            byte abyte1[] = new byte[l1];
            for(int i2 = 0; i2 < i1; i2++)
            {
                System.arraycopy(((Object) (abyte0)), i2 * abyte1.length, ((Object) (abyte1)), 0, abyte1.length);
                a.append(1184, ((Object) (compressFrame(i, abyte1, k, l))));
            }

            return;
        }
        if(k1 != 3 || j1 != 8)
            throw new DicomException("compression of pixel data with " + k1 + " samples per bit and " + j1 + " bits allocated unsupported");
        byte abyte2[] = new byte[l1];
        byte abyte3[] = new byte[l1];
        byte abyte4[] = new byte[l1];
        int j2 = 0;
        if(a.getI(463) == 1)
        {
            for(int k2 = 0; k2 < i1; k2++)
            {
                System.arraycopy(((Object) (abyte0)), j2, ((Object) (abyte2)), 0, l1);
                j2 += l1;
                System.arraycopy(((Object) (abyte0)), j2, ((Object) (abyte3)), 0, l1);
                j2 += l1;
                System.arraycopy(((Object) (abyte0)), j2, ((Object) (abyte4)), 0, l1);
                j2 += l1;
                a.append(1184, ((Object) (compressFrame(i, abyte2, abyte3, abyte4, k, l))));
            }

            a.set(463, ((Object) (new Integer(0))));
            return;
        }
        for(int l2 = 0; l2 < i1; l2++)
        {
            for(int i3 = 0; i3 < l1; i3++)
            {
                abyte2[i3] = abyte0[j2++];
                abyte3[i3] = abyte0[j2++];
                abyte4[i3] = abyte0[j2++];
            }

            a.append(1184, ((Object) (compressFrame(i, abyte2, abyte3, abyte4, k, l))));
        }

    }

    public static byte[] compressFrame(int i, byte abyte0[], int j, int k)
        throws IOException, DicomException
    {
        if(i == 8197)
        {
            w w1 = new w();
            if(abyte0.length == j * k)
                return w1.encode8bitGrayscale(abyte0, j, k);
            if(abyte0.length == j * k * 2)
                return w1.encode16bitGrayscale(abyte0, j, k);
            if(abyte0.length == j * k * 3)
                throw new DicomException("24 bit color lossless jpeg compression unsupported");
            else
                throw new DicomException("length of frame to compress is not equal to width*height or width*height*2 or width*height*3");
        } else
        {
            throw new DicomException("unsupported transfer syntax for compression");
        }
    }

    public static byte[] compressFrame(int i, byte abyte0[], byte abyte1[], byte abyte2[], int j, int k)
        throws IOException, DicomException
    {
        if(i == 8197)
        {
            w w1 = new w();
            return w1.encode24bitColor(abyte0, abyte1, abyte2, j, k);
        } else
        {
            throw new DicomException("unsupported transfer syntax for compression");
        }
    }

    public void setDicomObject(DicomObject dicomobject)
        throws DicomException
    {
        a = dicomobject;
    }

    public DicomObject getDicomObject()
    {
        return a;
    }
}
