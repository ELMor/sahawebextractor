// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;


// Referenced classes of package com.archimed.dicom:
//            DDict

public class DicomException extends Exception
{

    public DicomException()
    {
    }

    public DicomException(String s)
    {
        super(s);
    }

    public DicomException(int i, int j, String s)
    {
        super("(" + i + "," + j + "): " + s);
    }

    public DicomException(int i, String s)
    {
        super(DDict.getDescription(i) + ": " + s);
    }
}
