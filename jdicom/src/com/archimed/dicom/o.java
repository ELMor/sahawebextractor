// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

// Referenced classes of package com.archimed.dicom:
//            Jdt

class o
{

    private long a;
    private long b;

    o()
    {
        a = 122L;
        b = 0x7fffffffffffffffL;
    }

    private long a(InputStream inputstream)
    {
        long l = 0L;
        try
        {
            BufferedReader bufferedreader = new BufferedReader(((java.io.Reader) (new InputStreamReader(inputstream))));
            for(int i = 0; i < 19; i++)
            {
                l *= 10L;
                String s = bufferedreader.readLine();
                long l1 = Long.parseLong(s) % a;
                l += l1;
            }

        }
        catch(Throwable throwable)
        {
            return 0L;
        }
        return l;
    }

    private long b()
    {
        long l = b - System.currentTimeMillis();
        return l / 0x5265c00L;
    }

    private boolean c()
    {
        if(b == 0x7fffffffffffffffL)
            return true;
        if(b == 0L)
            return false;
        if(b > System.currentTimeMillis())
        {
            System.out.println("Your evaluation copy expires in " + b() + " days.");
            return true;
        } else
        {
            return false;
        }
    }

    private void d()
    {
        System.out.println("Your evaluation copy of " + Jdt.getVersion() + " has expired.");
        System.out.println("Please contact SoftLink (www.softlink.be) for a license.");
    }

    void a()
    {
        InputStream inputstream = ((Object)this).getClass().getResourceAsStream("/jdt.key");
        b = a(inputstream);
        if(!c())
        {
            d();
            System.exit(0);
        }
    }
}
