// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DDate
{

    private Calendar a;
    private boolean b;

    public DDate()
    {
        b = false;
        a = ((Calendar) (new GregorianCalendar()));
        b = true;
    }

    public DDate(String s)
        throws NumberFormatException
    {
        b = false;
        if(s.equals(""))
        {
            b = true;
            return;
        }
        boolean flag = s.length() == 10;
        if(s.length() != 8 && !flag)
        {
            throw new NumberFormatException("Cannot parse string into DDate");
        } else
        {
            byte byte0 = ((byte)(flag ? 5 : 4));
            byte byte1 = ((byte)(flag ? 8 : 6));
            int i = Integer.parseInt(s.substring(0, 4));
            int j = Integer.parseInt(s.substring(((int) (byte0)), byte0 + 2));
            int k = Integer.parseInt(s.substring(((int) (byte1))));
            a(i, j, k);
            a = ((Calendar) (new GregorianCalendar(i, j - 1, k)));
            return;
        }
    }

    static void a(int i, int j, int k)
    {
        switch(j)
        {
        default:
            break;

        case 1: // '\001'
        case 3: // '\003'
        case 5: // '\005'
        case 7: // '\007'
        case 8: // '\b'
        case 10: // '\n'
        case 12: // '\f'
            if(k > 0 && k <= 31)
                return;
            break;

        case 4: // '\004'
        case 6: // '\006'
        case 9: // '\t'
        case 11: // '\013'
            if(k > 0 && k <= 30)
                return;
            break;

        case 2: // '\002'
            if(k > 0 && k <= 29)
                return;
            break;
        }
        throw new NumberFormatException("Cannot parse string into DDate");
    }

    public String toString()
    {
        if(b)
        {
            return "";
        } else
        {
            SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE dd MMM yyyy");
            return ((DateFormat) (simpledateformat)).format(a.getTime());
        }
    }

    public String toString(DateFormat dateformat)
    {
        if(b)
            return "";
        else
            return dateformat.format(a.getTime());
    }

    public String toDICOMString()
    {
        if(b)
        {
            return "";
        } else
        {
            SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMdd");
            return ((DateFormat) (simpledateformat)).format(a.getTime());
        }
    }

    public boolean isEmpty()
    {
        return b;
    }
}
