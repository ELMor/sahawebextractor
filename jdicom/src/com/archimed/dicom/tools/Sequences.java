// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.tools;

import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.util.Vector;

public class Sequences
{

    DicomObject a;

    public Sequences(DicomObject dicomobject)
    {
        a = dicomobject;
    }

    public int getSize(int i, int j, int k)
        throws DicomException
    {
        return getSize_ge(i, j, DDict.getGroup(k), DDict.getElement(k));
    }

    public int getSize_ge(int i, int j, int k, int l)
        throws DicomException
    {
        if(DDict.getTypeCode(i) != 10)
            throw new DicomException(i, "Data element not of type SQ");
        Object obj = a.get(i, j);
        if(obj == null)
        {
            return -1;
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.getSize_ge(k, l);
        }
    }

    public Vector deleteItem(int i, int j, int k)
        throws DicomException
    {
        return deleteItem_ge(i, j, DDict.getGroup(k), DDict.getElement(k));
    }

    public Vector deleteItem_ge(int i, int j, int k, int l)
        throws DicomException
    {
        if(DDict.getTypeCode(i) != 10)
            throw new DicomException(i, "Data element not of type SQ");
        Object obj = a.get(i, j);
        if(obj == null)
        {
            return null;
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.deleteItem_ge(k, l);
        }
    }

    public Object deleteItem(int i, int j, int k, int l)
        throws DicomException
    {
        return deleteItem_ge(i, j, DDict.getGroup(k), DDict.getElement(k), l);
    }

    public Object deleteItem_ge(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        if(DDict.getTypeCode(i) != 10)
            throw new DicomException("Data element 'seq' not of type SQ");
        Object obj = a.get(i, j);
        if(obj == null)
        {
            return ((Object) (null));
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.deleteItem_ge(k, l, i1);
        }
    }

    public void set(int i, int j, int k, Object obj)
        throws DicomException
    {
        set_ge(i, j, DDict.getGroup(k), DDict.getElement(k), obj);
    }

    public void set_ge(int i, int j, int k, int l, Object obj)
        throws DicomException
    {
        boolean flag = true;
        if(DDict.getTypeCode(i) != 10)
            throw new DicomException(i, "Data element not of type SQ");
        Object obj1 = a.get(i, j);
        DicomObject dicomobject;
        if(obj1 == null)
        {
            dicomobject = new DicomObject();
        } else
        {
            dicomobject = (DicomObject)obj1;
            flag = false;
        }
        dicomobject.deleteItem_ge(k, l);
        dicomobject.set_ge(k, l, obj, 0);
        if(flag)
            a.set(i, ((Object) (dicomobject)), j);
    }

    public void set(int i, int j, int k, Object obj, int l)
        throws DicomException
    {
        int i1 = DDict.getGroup(k);
        int j1 = DDict.getElement(k);
        set_ge(i, j, i1, j1, obj, l);
    }

    public void set_ge(int i, int j, int k, int l, Object obj, int i1)
        throws DicomException
    {
        boolean flag = true;
        if(DDict.getTypeCode(i) != 10)
            throw new DicomException(i, "Data element not of type SQ");
        Object obj1 = a.get(i, j);
        DicomObject dicomobject;
        if(obj1 == null)
        {
            dicomobject = new DicomObject();
        } else
        {
            dicomobject = (DicomObject)obj1;
            flag = false;
        }
        dicomobject.set_ge(k, l, obj, i1);
        if(flag)
            a.set(i, ((Object) (dicomobject)), j);
    }

    public void append(int i, int j, int k, Object obj)
        throws DicomException
    {
        append_ge(i, j, DDict.getGroup(k), DDict.getElement(k), obj);
    }

    public void append_ge(int i, int j, int k, int l, Object obj)
        throws DicomException
    {
        int i1 = getSize_ge(i, j, k, l);
        if(i1 == -1)
            i1 = 0;
        set_ge(i, j, k, l, obj, i1);
    }

    public Object get(int i, int j, int k)
        throws DicomException
    {
        return get(i, j, k, 0);
    }

    public Object get_ge(int i, int j, int k, int l)
        throws DicomException
    {
        return get_ge(i, j, k, l, 0);
    }

    public Object get(int i, int j, int k, int l)
        throws DicomException
    {
        return get_ge(i, j, DDict.getGroup(k), DDict.getElement(k), l);
    }

    public Object get_ge(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        if(DDict.getTypeCode(i) != 10)
            throw new DicomException(i, "Data element not of type SQ");
        Object obj = a.get(i, j);
        if(obj == null)
        {
            return ((Object) (null));
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.get_ge(k, l, i1);
        }
    }

    public String getS(int i, int j, int k)
        throws DicomException
    {
        return getS(i, j, k, 0);
    }

    public String getS_ge(int i, int j, int k, int l)
        throws DicomException
    {
        return getS_ge(i, j, k, l, 0);
    }

    public String getS(int i, int j, int k, int l)
        throws DicomException
    {
        return getS_ge(i, j, DDict.getGroup(k), DDict.getElement(k), l);
    }

    public String getS_ge(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        if(DDict.getTypeCode(i) != 10)
            throw new DicomException(i, "Data element not of type SQ");
        Object obj = a.get(i, j);
        if(obj == null)
        {
            return null;
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.getS_ge(k, l, i1);
        }
    }

    public int getI(int i, int j, int k)
        throws DicomException
    {
        return getI(i, j, k, 0);
    }

    public int getI_ge(int i, int j, int k, int l)
        throws DicomException
    {
        return getI_ge(i, j, k, l, 0);
    }

    public int getI(int i, int j, int k, int l)
        throws DicomException
    {
        return getI_ge(i, j, DDict.getGroup(k), DDict.getElement(k), l);
    }

    public int getI_ge(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        if(DDict.getTypeCode(i) != 10)
            throw new DicomException(i, "Data element not of type SQ");
        Object obj = a.get(i, j);
        if(obj == null)
        {
            return 0x7fffffff;
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.getI_ge(k, l, i1);
        }
    }

    public int getSize(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        return getSize_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1));
    }

    public int getSize_ge(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        if(DDict.getTypeCode(k) != 10)
            throw new DicomException(k, "Data element not of type SQ");
        Object obj = get(i, l, k, l);
        if(obj == null)
        {
            return -1;
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.getSize_ge(i1, j1);
        }
    }

    public Vector deleteItem(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        return deleteItem_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1));
    }

    public Vector deleteItem_ge(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        if(DDict.getTypeCode(k) != 10)
            throw new DicomException(k, "Data element not of type SQ");
        Object obj = get(i, j, k, l);
        if(obj == null)
        {
            return null;
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.deleteItem_ge(i1, j1);
        }
    }

    public Object deleteItem(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        return deleteItem_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1), j1);
    }

    public Object deleteItem_ge(int i, int j, int k, int l, int i1, int j1, int k1)
        throws DicomException
    {
        if(DDict.getTypeCode(k) != 10)
            throw new DicomException(k, "Data element not of type SQ");
        Object obj = get(i, j, k, l);
        if(obj == null)
        {
            return ((Object) (null));
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.deleteItem_ge(i1, j1, k1);
        }
    }

    public void set(int i, int j, int k, int l, int i1, Object obj)
        throws DicomException
    {
        set_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1), obj);
    }

    public void set_ge(int i, int j, int k, int l, int i1, int j1, Object obj)
        throws DicomException
    {
        boolean flag = true;
        if(DDict.getTypeCode(k) != 10)
            throw new DicomException(k, "Data element not of type SQ");
        Object obj1 = get(i, j, k, l);
        DicomObject dicomobject;
        if(obj1 == null)
        {
            dicomobject = new DicomObject();
        } else
        {
            dicomobject = (DicomObject)obj1;
            flag = false;
        }
        dicomobject.deleteItem_ge(i1, j1);
        dicomobject.set_ge(i1, j1, obj, 0);
        if(flag)
            set(i, j, k, ((Object) (dicomobject)), l);
    }

    public void set(int i, int j, int k, int l, int i1, Object obj, int j1)
        throws DicomException
    {
        set_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1), obj, j1);
    }

    public void set_ge(int i, int j, int k, int l, int i1, int j1, Object obj, 
            int k1)
        throws DicomException
    {
        boolean flag = true;
        if(DDict.getTypeCode(k) != 10)
            throw new DicomException(k, "Data element not of type SQ");
        Object obj1 = get(i, j, k, l);
        DicomObject dicomobject;
        if(obj1 == null)
        {
            dicomobject = new DicomObject();
        } else
        {
            dicomobject = (DicomObject)obj1;
            flag = false;
        }
        dicomobject.set_ge(i1, j1, obj, k1);
        if(flag)
            set(i, j, k, ((Object) (dicomobject)), l);
    }

    public void append(int i, int j, int k, int l, int i1, Object obj)
        throws DicomException
    {
        append_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1), obj);
    }

    public void append_ge(int i, int j, int k, int l, int i1, int j1, Object obj)
        throws DicomException
    {
        int k1 = getSize_ge(i, j, k, l, i1, j1);
        if(k1 == -1)
            k1 = 0;
        set_ge(i, j, k, l, i1, j1, obj, k1);
    }

    public Object get(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        return get(i, j, k, l, i1, 0);
    }

    public Object get_ge(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        return get_ge(i, j, k, l, i1, j1, 0);
    }

    public Object get(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        return get_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1), j1);
    }

    public Object get_ge(int i, int j, int k, int l, int i1, int j1, int k1)
        throws DicomException
    {
        if(DDict.getTypeCode(k) != 10)
            throw new DicomException(k, "Data element not of type SQ");
        Object obj = get(i, j, k, l);
        if(obj == null)
        {
            return ((Object) (null));
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.get_ge(i1, j1, k1);
        }
    }

    public String getS(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        return getS(i, j, k, l, i1, 0);
    }

    public String getS_ge(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        return getS_ge(i, j, k, l, i1, j1, 0);
    }

    public String getS(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        return getS_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1), j1);
    }

    public String getS_ge(int i, int j, int k, int l, int i1, int j1, int k1)
        throws DicomException
    {
        if(DDict.getTypeCode(k) != 10)
            throw new DicomException(k, "Data element not of type SQ");
        Object obj = get(i, j, k, l);
        if(obj == null)
        {
            return null;
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.getS_ge(i1, j1, k1);
        }
    }

    public int getI(int i, int j, int k, int l, int i1)
        throws DicomException
    {
        return getI(i, j, k, l, i1, 0);
    }

    public int getI_ge(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        return getI_ge(i, j, k, l, i1, j1, 0);
    }

    public int getI(int i, int j, int k, int l, int i1, int j1)
        throws DicomException
    {
        return getI_ge(i, j, k, l, DDict.getGroup(i1), DDict.getElement(i1), j1);
    }

    public int getI_ge(int i, int j, int k, int l, int i1, int j1, int k1)
        throws DicomException
    {
        if(DDict.getTypeCode(k) != 10)
            throw new DicomException(k, "Data element not of type SQ");
        Object obj = get(i, j, k, l);
        if(obj == null)
        {
            return 0x7fffffff;
        } else
        {
            DicomObject dicomobject = (DicomObject)obj;
            return dicomobject.getI_ge(i1, j1, k1);
        }
    }
}
