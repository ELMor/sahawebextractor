// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;

// Referenced classes of package com.archimed.dicom.network:
//            SubItem

class MaximumLengthSubItem extends SubItem
{

    private int a;
    private int b;

    public MaximumLengthSubItem()
    {
        a = 81;
    }

    public MaximumLengthSubItem(int i)
    {
        a = 81;
        b = i;
    }

    public int getLength()
    {
        return 8;
    }

    public int getMaxPduSize()
    {
        return b;
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(a);
        dataoutputstream.write(0);
        dataoutputstream.writeChar(4);
        dataoutputstream.writeInt(b);
    }

    public int read(DataInputStream datainputstream)
        throws IOException, IllegalValueException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
        {
            throw new IllegalValueException("itemtype field of received Implementation Class UID Item  not " + a);
        } else
        {
            ((FilterInputStream) (datainputstream)).read();
            char c = datainputstream.readChar();
            b = datainputstream.readInt();
            return c + 4;
        }
    }
}
