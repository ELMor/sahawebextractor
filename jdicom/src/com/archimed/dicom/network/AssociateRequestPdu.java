// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.network:
//            Request, ApplicationContextItem, AbstractSyntaxItem, TransferSyntaxItem, 
//            PresentationContextItem, UserInfoItem, ImplementationClassUIDSubItem, ImplementationVersionNameSubItem, 
//            MaximumLengthSubItem, ScuScpRoleSubItem, ExtendedNegotiationSubItem, AsynchronousOperationsWindowSubItem, 
//            SubItem

class AssociateRequestPdu
{

    private int a;
    private char b;
    String c;
    String d;
    ApplicationContextItem e;
    Vector f;
    UserInfoItem g;
    Request h;

    public AssociateRequestPdu()
    {
        a = 1;
        b = '\001';
        f = new Vector();
    }

    public AssociateRequestPdu(Request request)
        throws IllegalValueException
    {
        a = 1;
        b = '\001';
        h = request;
        c = request.getCalledTitle();
        d = request.getCallingTitle();
        f = new Vector();
        e = new ApplicationContextItem();
        for(int i = 0; i < request.getPresentationContexts(); i++)
        {
            UIDEntry uidentry = request.getAbstractSyntax(i);
            AbstractSyntaxItem abstractsyntaxitem = new AbstractSyntaxItem(uidentry);
            Vector vector = new Vector();
            for(int j = 0; j < request.getTransferSyntaxes(i); j++)
                vector.addElement(((Object) (new TransferSyntaxItem(request.getTransferSyntax(i, j)))));

            PresentationContextItem presentationcontextitem = new PresentationContextItem(((int) (request.getPresentationContextID(i))), abstractsyntaxitem, vector);
            f.addElement(((Object) (presentationcontextitem)));
        }

        g = new UserInfoItem();
        ImplementationClassUIDSubItem implementationclassuidsubitem = new ImplementationClassUIDSubItem(request.getImplementationClassUID());
        ImplementationVersionNameSubItem implementationversionnamesubitem = new ImplementationVersionNameSubItem(request.getImplementationVersionName());
        MaximumLengthSubItem maximumlengthsubitem = new MaximumLengthSubItem(request.getMaxPduSize());
        g.addSubItem(((SubItem) (maximumlengthsubitem)));
        g.addSubItem(((SubItem) (implementationclassuidsubitem)));
        g.addSubItem(((SubItem) (implementationversionnamesubitem)));
        for(int i1 = 0; i1 < request.getRoles(); i1++)
        {
            UIDEntry uidentry1 = request.getAbstractSyntaxForRole(i1);
            int k = request.getScuRole(uidentry1.getConstant());
            int l = request.getScpRole(uidentry1.getConstant());
            if(k != -1 && l != -1)
            {
                ScuScpRoleSubItem scuscprolesubitem = new ScuScpRoleSubItem(uidentry1, k, l);
                g.addSubItem(((SubItem) (scuscprolesubitem)));
            }
        }

        for(int j1 = 0; j1 < request.getPresentationContexts(); j1++)
        {
            byte abyte0[] = (byte[])request.getExtendedNegotiationData(j1);
            if(abyte0 != null)
            {
                UIDEntry uidentry2 = request.getAbstractSyntax(j1);
                ExtendedNegotiationSubItem extendednegotiationsubitem = new ExtendedNegotiationSubItem(uidentry2, abyte0);
                g.addSubItem(((SubItem) (extendednegotiationsubitem)));
            }
        }

        if(request.getMaxOperationsInvoked() != 1 || request.getMaxOperationsPerformed() != 1)
        {
            AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem = new AsynchronousOperationsWindowSubItem();
            asynchronousoperationswindowsubitem.a(request.getMaxOperationsInvoked());
            asynchronousoperationswindowsubitem.b(request.getMaxOperationsPerformed());
            g.addSubItem(((SubItem) (asynchronousoperationswindowsubitem)));
        }
    }

    public Request getRequest()
        throws IllegalValueException
    {
        return h;
    }

    private void a()
    {
        h = new Request();
        h.setCalledTitle(c);
        h.setCallingTitle(d);
        h.setApplicationContextUid(e.getUid());
        Vector vector = g.getSubItems();
        for(int i = 0; i < f.size(); i++)
        {
            PresentationContextItem presentationcontextitem = (PresentationContextItem)f.elementAt(i);
            Vector vector1 = presentationcontextitem.getTransferSyntaxes();
            UIDEntry uidentry = presentationcontextitem.getAbstractSyntaxItem().getAbstractSyntax();
            UIDEntry auidentry[] = new UIDEntry[vector1.size()];
            for(int j = 0; j < vector1.size(); j++)
                auidentry[j] = ((TransferSyntaxItem)vector1.elementAt(j)).getTransferSyntax();

            h.a((byte)presentationcontextitem.getID(), uidentry, auidentry);
        }

        for(int k = 0; k < vector.size(); k++)
        {
            SubItem subitem = (SubItem)vector.elementAt(k);
            if(subitem instanceof ImplementationClassUIDSubItem)
                h.a(((ImplementationClassUIDSubItem)subitem).getImplementationClassUID());
            else
            if(subitem instanceof ImplementationVersionNameSubItem)
                h.b(((ImplementationVersionNameSubItem)subitem).getImplementationVersionName());
            else
            if(subitem instanceof MaximumLengthSubItem)
                h.setMaxPduSize(((MaximumLengthSubItem)subitem).getMaxPduSize());
            else
            if(subitem instanceof ScuScpRoleSubItem)
            {
                ScuScpRoleSubItem scuscprolesubitem = (ScuScpRoleSubItem)subitem;
                h.a(scuscprolesubitem.getAbstractSyntax(), scuscprolesubitem.getScuRole(), scuscprolesubitem.getScpRole());
            } else
            if(subitem instanceof AsynchronousOperationsWindowSubItem)
            {
                AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem = (AsynchronousOperationsWindowSubItem)subitem;
                h.setMaxOperationsInvoked(asynchronousoperationswindowsubitem.a());
                h.setMaxOperationsPerformed(asynchronousoperationswindowsubitem.b());
            } else
            if(subitem instanceof ExtendedNegotiationSubItem)
            {
                ExtendedNegotiationSubItem extendednegotiationsubitem = (ExtendedNegotiationSubItem)subitem;
                UIDEntry uidentry1 = extendednegotiationsubitem.getUIDEntry();
                for(int l = 0; l < h.getPresentationContexts(); l++)
                    if(h.getAbstractSyntax(l).getValue().equals(((Object) (uidentry1.getValue()))))
                        h.setExtendedNegotiationData(l, extendednegotiationsubitem.getApplicationInfo());

            }
        }

    }

    public String getCalledTitle()
    {
        return c;
    }

    public String getCallingTitle()
    {
        return d;
    }

    public ApplicationContextItem getApplicationContextItem()
    {
        return e;
    }

    public Vector getPresentationContextItems()
    {
        return f;
    }

    public UserInfoItem getUserInfoItem()
    {
        return g;
    }

    public void setApplicationContextItem(ApplicationContextItem applicationcontextitem)
    {
        e = applicationcontextitem;
    }

    public void setUserInfoItem(UserInfoItem userinfoitem)
    {
        g = userinfoitem;
    }

    public void addPresentationContextItem(PresentationContextItem presentationcontextitem)
    {
        f.addElement(((Object) (presentationcontextitem)));
    }

    public int getLength()
    {
        int i = 0;
        i = (i += 74) + e.a();
        for(int j = 0; j < f.size(); j++)
            i += ((PresentationContextItem)f.elementAt(j)).getLength();

        i += g.getLength();
        return i;
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(a);
        dataoutputstream.write(0);
        dataoutputstream.writeInt(getLength() - 6);
        dataoutputstream.writeChar(1);
        dataoutputstream.writeChar(0);
        ((FilterOutputStream) (dataoutputstream)).write(c.getBytes());
        for(int i = 0; i < 16 - c.length(); i++)
            dataoutputstream.write(32);

        ((FilterOutputStream) (dataoutputstream)).write(d.getBytes());
        for(int j = 0; j < 16 - d.length(); j++)
            dataoutputstream.write(32);

        ((FilterOutputStream) (dataoutputstream)).write(new byte[32]);
        e.a(dataoutputstream);
        for(int k = 0; k < f.size(); k++)
            ((PresentationContextItem)f.elementAt(k)).write(dataoutputstream);

        g.write(dataoutputstream);
    }

    public void read(InputStream inputstream)
        throws IOException, IllegalValueException, UnknownUIDException
    {
        PushbackInputStream pushbackinputstream = new PushbackInputStream(inputstream);
        DataInputStream datainputstream = new DataInputStream(((InputStream) (pushbackinputstream)));
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
            throw new IllegalValueException("PDU-type field of Associate Request PDU not " + a);
        ((FilterInputStream) (datainputstream)).read();
        int k = datainputstream.readInt();
        b = datainputstream.readChar();
        k -= 2;
        datainputstream.readChar();
        byte abyte0[] = new byte[16];
        datainputstream.readFully(abyte0);
        c = new String(abyte0);
        c = c.trim();
        datainputstream.readFully(abyte0);
        d = new String(abyte0);
        d = d.trim();
        inputstream.read(new byte[32]);
        k -= 66;
        e = new ApplicationContextItem();
        int l = e.read(datainputstream);
        for(k -= l; k > 0;)
        {
            int j = ((FilterInputStream) (datainputstream)).read();
            if(j == 32)
            {
                pushbackinputstream.unread(32);
                PresentationContextItem presentationcontextitem = new PresentationContextItem();
                int i1 = presentationcontextitem.a(datainputstream);
                f.addElement(((Object) (presentationcontextitem)));
                k -= i1;
            } else
            if(j == 80)
            {
                pushbackinputstream.unread(80);
                g = new UserInfoItem();
                int j1 = g.read(pushbackinputstream);
                k -= j1;
            }
        }

        a();
    }
}
