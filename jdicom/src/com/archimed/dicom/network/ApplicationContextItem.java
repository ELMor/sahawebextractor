// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.tool.n;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.PrintStream;

class ApplicationContextItem
{

    private byte a;
    static final String b = "1.2.840.10008.3.1.1.1";
    private String c;

    ApplicationContextItem()
    {
        a = 16;
        c = "1.2.840.10008.3.1.1.1";
    }

    int a()
    {
        return c.length() + 4;
    }

    public String getUid()
    {
        return c;
    }

    public static boolean isPrivate(String s)
    {
        return "1.2.840.10008.3.1.1.1".equals(((Object) (s)));
    }

    void a(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(((int) (a)));
        dataoutputstream.write(0);
        dataoutputstream.writeChar(c.length());
        ((FilterOutputStream) (dataoutputstream)).write(c.getBytes());
    }

    protected int read(DataInputStream datainputstream)
        throws IOException, IllegalValueException
    {
        boolean flag = false;
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
            throw new IllegalValueException("itemtype field of received Application Context Item  not " + a);
        ((FilterInputStream) (datainputstream)).read();
        char c1 = datainputstream.readChar();
        byte abyte0[] = new byte[c1];
        datainputstream.readFully(abyte0);
        abyte0 = n.trimZeros(abyte0);
        c = new String(abyte0);
        if(Debug.DEBUG > 3)
            Debug.out.println("ApplicationContextItem [" + (int)c1 + "]=" + c);
        return 4 + c1;
    }
}
