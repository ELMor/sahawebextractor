// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.n;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

// Referenced classes of package com.archimed.dicom.network:
//            SubItem

class ExtendedNegotiationSubItem extends SubItem
{

    private int a;
    private UIDEntry b;
    private byte c[];

    public ExtendedNegotiationSubItem()
    {
        a = 86;
        c = new byte[0];
    }

    public ExtendedNegotiationSubItem(UIDEntry uidentry, byte abyte0[])
    {
        a = 86;
        c = new byte[0];
        if(abyte0 == null)
            throw new NullPointerException();
        else
            return;
    }

    public byte[] getApplicationInfo()
    {
        return c;
    }

    public UIDEntry getUIDEntry()
    {
        return b;
    }

    public int getLength()
    {
        return 6 + b.getValue().length() + c.length;
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(a);
        dataoutputstream.write(0);
        dataoutputstream.writeChar(2 + b.getValue().length() + c.length);
        dataoutputstream.writeChar(b.getValue().length());
        ((FilterOutputStream) (dataoutputstream)).write(b.getValue().getBytes());
        ((FilterOutputStream) (dataoutputstream)).write(c);
    }

    public int read(DataInputStream datainputstream)
        throws IOException, IllegalValueException, UnknownUIDException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
            throw new IllegalValueException("itemtype field of received Implementation Version Name Item  not " + a);
        ((FilterInputStream) (datainputstream)).read();
        char c1 = datainputstream.readChar();
        char c2 = datainputstream.readChar();
        byte abyte0[] = new byte[c2];
        datainputstream.read(abyte0);
        abyte0 = n.trimZeros(abyte0);
        String s = new String(abyte0);
        try
        {
            b = UID.getUIDEntry(s);
        }
        catch(UnknownUIDException unknownuidexception)
        {
            b = new UIDEntry(0, s, "unknown uid", "??", 1);
        }
        c = new byte[c1 - c2 - 2];
        datainputstream.read(c);
        return 4 + c1;
    }
}
