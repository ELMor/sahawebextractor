// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;


// Referenced classes of package com.archimed.dicom.network:
//            Response

public class Reject extends Response
{

    public static int REJECTED_PERMANENT = 1;
    public static int REJECTED_TRANSIENT = 2;
    public static int DICOM_UL_SERVICE_USER = 1;
    public static int DICOM_UL_SERVICE_PROVIDER_ACSE = 2;
    public static int DICOM_UL_SERVICE_PROVIDER_PRESENTATION = 3;
    public static int USER_NO_REASON_GIVEN = 1;
    public static int USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED = 2;
    public static int USER_CALLING_AETITLE_NOT_RECOGNIZED = 3;
    public static int USER_CALLED_AETITLE_NOT_RECOGNIZED = 7;
    public static int ACSE_NO_REASON_GIVEN = 1;
    public static int ACSE_PROTOCOL_NOT_SUPPORTED = 2;
    public static int PRESENTATION_TEMPORARY_CONGESTION = 1;
    public static int PRESENTATION_LOCAL_LIMIT_EXCEEDED = 2;
    private int a;
    private int b;
    private int c;

    public Reject(int i, int j, int k)
    {
        a = i;
        b = j;
        c = k;
    }

    public int getResult()
    {
        return a;
    }

    public int getSource()
    {
        return b;
    }

    public int getReason()
    {
        return c;
    }

    public String toString()
    {
        String s = "[reject, result: ";
        if(a == REJECTED_PERMANENT)
            s = s + "REJECTED_PERMANENT";
        else
        if(a == REJECTED_TRANSIENT)
            s = s + "REJECTED_TRANSIENT";
        else
            s = s + "UNKNOWN";
        s = s + ", source: ";
        if(b == DICOM_UL_SERVICE_USER)
            s = s + "DICOM_UL_SERVICE_USER";
        else
        if(b == DICOM_UL_SERVICE_PROVIDER_ACSE)
            s = s + "DICOM_UL_SERVICE_PROVIDER_ACSE";
        else
        if(b == DICOM_UL_SERVICE_PROVIDER_PRESENTATION)
            s = s + "DICOM_UL_SERVICE_PROVIDER_PRESENTATION";
        else
            s = s + "UNKNOWN";
        s = s + ", reason: ";
        if(c == USER_NO_REASON_GIVEN)
            s = s + "NO_REASON_GIVEN";
        else
        if(c == USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED)
            s = s + "APPLICATIONCONTEXTNAME_NOT_SUPPORTED";
        else
        if(c == USER_CALLING_AETITLE_NOT_RECOGNIZED)
            s = s + "CALLING_AETITILE_NOT_RECOGNIZED";
        else
        if(c == USER_CALLED_AETITLE_NOT_RECOGNIZED)
            s = s + "CALLED_AETITILE_NOT_RECOGNIZED";
        else
        if(c == ACSE_NO_REASON_GIVEN)
            s = s + "NO_REASON_GIVEN";
        else
        if(c == ACSE_PROTOCOL_NOT_SUPPORTED)
            s = s + "PROTOCOL_NOT_SUPPORTED";
        else
        if(c == PRESENTATION_TEMPORARY_CONGESTION)
            s = s + "TEMPORARY_CONGESTION";
        else
        if(c == PRESENTATION_LOCAL_LIMIT_EXCEEDED)
            s = s + "LOCAL_LIMIT_EXCEEDED";
        else
            s = s + "UNKNOWN";
        s = s + "]";
        return s;
    }

}
