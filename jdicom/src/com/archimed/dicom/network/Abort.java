// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;


// Referenced classes of package com.archimed.dicom.network:
//            Response

public class Abort extends Response
{

    public static int DICOM_UL_SERVICE_USER = 0;
    public static int DICOM_UL_SERVICE_PROVIDER = 2;
    public static int REASON_NOT_SPECIFIED = 0;
    public static int UNRECOGNIZED_PDU = 1;
    public static int UNEXPECTED_PDU = 2;
    public static int UNRECOGNIZED_PDU_PARAMETER = 4;
    public static int UNEXPECTED_PDU_PARAMETER = 5;
    public static int INVALID_PDU_PARAMETER_VALUE = 6;
    private int a;
    private int b;

    public Abort(int i, int j)
    {
        a = i;
        b = j;
    }

    public int getSource()
    {
        return a;
    }

    public int getReason()
    {
        return b;
    }

    public String toString()
    {
        String s = "[abort, source: ";
        if(a == DICOM_UL_SERVICE_USER)
            s = s + "DICOM_UL_SERVICE_USER";
        else
        if(a == DICOM_UL_SERVICE_PROVIDER)
            s = s + "DICOM_UL_SERVICE_PROVIDER";
        else
            s = s + "UNKNOWN";
        s = s + ", reason: ";
        if(b == REASON_NOT_SPECIFIED)
            s = s + "REASON_NOT_SPECIFIED";
        else
        if(b == UNRECOGNIZED_PDU)
            s = s + "UNRECOGNIZED_PDU";
        else
        if(b == UNEXPECTED_PDU)
            s = s + "UNEXPECTED_PDU";
        else
        if(b == UNRECOGNIZED_PDU_PARAMETER)
            s = s + "UNRECOGNIZED_PDU_PARAMETER";
        else
        if(b == UNEXPECTED_PDU_PARAMETER)
            s = s + "UNEXPECTED_PDU_PARAMETER";
        else
        if(b == INVALID_PDU_PARAMETER_VALUE)
            s = s + "INVALID_PDU_PARAMETER_VALUE";
        else
            s = s + "UNKNOWN";
        s = s + "]";
        return s;
    }

}
