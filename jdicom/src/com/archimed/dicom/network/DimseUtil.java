// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;

public class DimseUtil
{

    public static final int N_EVENT_REPORT_REQUEST = 256;
    public static final int N_EVENT_REPORT_RESPONSE = 33024;
    public static final int N_GET_REQUEST = 272;
    public static final int N_GET_RESPONSE = 33040;
    public static final int N_SET_REQUEST = 288;
    public static final int N_SET_RESPONSE = 33056;
    public static final int N_ACTION_REQUEST = 304;
    public static final int N_ACTION_RESPONSE = 33072;
    public static final int N_CREATE_REQUEST = 320;
    public static final int N_CREATE_RESPONSE = 33088;
    public static final int N_DELETE_REQUEST = 336;
    public static final int N_DELETE_RESPONSE = 336;
    public static final int C_STORE_REQUEST = 1;
    public static final int C_STORE_RESPONSE = 32769;
    public static final int C_GET_REQUEST = 16;
    public static final int C_GET_RESPONSE = 32784;
    public static final int C_FIND_REQUEST = 32;
    public static final int C_FIND_RESPONSE = 32800;
    public static final int C_MOVE_REQUEST = 33;
    public static final int C_MOVE_RESPONSE = 32801;
    public static final int C_ECHO_REQUEST = 48;
    public static final int C_ECHO_RESPONSE = 32816;
    public static final int C_CANCEL_FIND_REQUEST = 4095;
    public static final int MEDIUM_PRIORITY = 0;
    public static final int HIGH_PRIORITY = 1;
    public static final int LOW_PRIORITY = 2;
    private static final Integer a = new Integer(0);
    private static final Integer b = new Integer(257);

    public DimseUtil()
    {
    }

    public static int getCommandType(DicomObject dicomobject)
        throws IllegalValueException
    {
        Integer integer = (Integer)dicomobject.get(3);
        if(integer == null)
            throw new IllegalValueException("dicomset is not a DIMSE command");
        else
            return integer.intValue();
    }

    public static int getMessageID(DicomObject dicomobject)
        throws IllegalValueException
    {
        Integer integer = (Integer)dicomobject.get(4);
        if(integer == null)
            throw new IllegalValueException("dicomset does not contain MessageID tag");
        else
            return integer.intValue();
    }

    public static int getPriority(DicomObject dicomobject)
        throws IllegalValueException
    {
        Integer integer = (Integer)dicomobject.get(4);
        if(integer == null)
            throw new IllegalValueException("dicomset does not contain Priority tag");
        else
            return integer.intValue();
    }

    public static int getAffectedSOPClass(DicomObject dicomobject)
        throws IllegalValueException, UnknownUIDException
    {
        String s = (String)dicomobject.get(1);
        return UID.getUIDEntry(s).getConstant();
    }

    public static String getAffectedSOPInstance(DicomObject dicomobject)
        throws IllegalValueException
    {
        String s = (String)dicomobject.get(13);
        return s;
    }

    public static int getRequestedSOPClass(DicomObject dicomobject)
        throws IllegalValueException, UnknownUIDException
    {
        String s = (String)dicomobject.get(2);
        return UID.getUIDEntry(s).getConstant();
    }

    public static String getRequestedSOPInstance(DicomObject dicomobject)
        throws IllegalValueException, UnknownUIDException
    {
        String s = (String)dicomobject.get(14);
        return s;
    }

    public static boolean dataPresent(DicomObject dicomobject)
        throws IllegalValueException
    {
        Integer integer = (Integer)dicomobject.get(8);
        if(integer == null)
            throw new IllegalValueException("no DataSetType tag in dicomobject");
        return integer.intValue() != 257;
    }

    public static DicomObject createEventReportRequest(Integer integer, Integer integer1, String s, boolean flag, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(3, ((Object) (new Integer(256))));
        dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(4, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        dicomobject.set(13, ((Object) (s)));
        dicomobject.set(15, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createEventReportResponse(Integer integer, Integer integer1, String s, boolean flag, Integer integer2, Integer integer3)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(33024))));
        dicomobject.set(5, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        if(s != null)
            dicomobject.set(13, ((Object) (s)));
        if(integer3 != null)
            dicomobject.set(15, ((Object) (integer3)));
        dicomobject.set(9, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createNGetRequest(Integer integer, Integer integer1, String s, int ai[])
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(2, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(272))));
        dicomobject.set(4, ((Object) (integer)));
        dicomobject.set(8, ((Object) (b)));
        dicomobject.set(14, ((Object) (s)));
        return dicomobject;
    }

    public static DicomObject createNGetResponse(Integer integer, Integer integer1, String s, boolean flag, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(33040))));
        dicomobject.set(5, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        if(s != null)
            dicomobject.set(13, ((Object) (s)));
        dicomobject.set(9, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createSetRequest(Integer integer, Integer integer1, String s)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(2, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(288))));
        dicomobject.set(4, ((Object) (integer)));
        dicomobject.set(8, ((Object) (a)));
        dicomobject.set(14, ((Object) (s)));
        return dicomobject;
    }

    public static DicomObject createSetResponse(Integer integer, Integer integer1, String s, boolean flag, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(33056))));
        dicomobject.set(5, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (a)));
        if(s != null)
            dicomobject.set(13, ((Object) (s)));
        dicomobject.set(9, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createActionRequest(Integer integer, Integer integer1, String s, boolean flag, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(2, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(304))));
        dicomobject.set(4, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        dicomobject.set(14, ((Object) (s)));
        dicomobject.set(18, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createActionResponse(Integer integer, Integer integer1, String s, boolean flag, Integer integer2, Integer integer3)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(33072))));
        dicomobject.set(5, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        if(s != null)
            dicomobject.set(13, ((Object) (s)));
        if(integer3 != null)
            dicomobject.set(18, ((Object) (integer3)));
        dicomobject.set(9, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createCreateRequest(Integer integer, Integer integer1, String s, boolean flag)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(320))));
        dicomobject.set(4, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        if(s != null)
            dicomobject.set(13, ((Object) (s)));
        return dicomobject;
    }

    public static DicomObject createCreateResponse(Integer integer, Integer integer1, String s, boolean flag, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(33088))));
        dicomobject.set(5, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        if(s != null)
            dicomobject.set(13, ((Object) (s)));
        dicomobject.set(9, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createDeleteRequest(Integer integer, Integer integer1, String s)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(2, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(336))));
        dicomobject.set(4, ((Object) (integer)));
        dicomobject.set(8, ((Object) (b)));
        dicomobject.set(14, ((Object) (s)));
        return dicomobject;
    }

    public static DicomObject createDeleteResponse(Integer integer, Integer integer1, String s, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(336))));
        dicomobject.set(5, ((Object) (integer)));
        dicomobject.set(8, ((Object) (b)));
        if(s != null)
            dicomobject.set(13, ((Object) (s)));
        dicomobject.set(9, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createEchoRequest(Integer integer, Integer integer1)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(48))));
        dicomobject.set(4, ((Object) (integer)));
        dicomobject.set(8, ((Object) (b)));
        return dicomobject;
    }

    public static DicomObject createEchoResponse(Integer integer, Integer integer1, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(32816))));
        dicomobject.set(5, ((Object) (integer)));
        dicomobject.set(8, ((Object) (b)));
        dicomobject.set(9, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createMoveRequest(Integer integer, Integer integer1, Integer integer2, String s)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(33))));
        dicomobject.set(4, ((Object) (integer)));
        dicomobject.set(7, ((Object) (integer2)));
        dicomobject.set(8, ((Object) (a)));
        dicomobject.set(6, ((Object) (s)));
        return dicomobject;
    }

    public static DicomObject createMoveResponse(Integer integer, Integer integer1, boolean flag, Integer integer2, Integer integer3, Integer integer4, Integer integer5, Integer integer6)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(32801))));
        dicomobject.set(5, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        dicomobject.set(9, ((Object) (integer2)));
        if(integer3 != null)
            dicomobject.set(19, ((Object) (integer3)));
        if(integer4 != null)
            dicomobject.set(20, ((Object) (integer4)));
        if(integer5 != null)
            dicomobject.set(21, ((Object) (integer5)));
        if(integer6 != null)
            dicomobject.set(22, ((Object) (integer6)));
        return dicomobject;
    }

    public static DicomObject createCGetRequest(Integer integer, Integer integer1, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(16))));
        dicomobject.set(4, ((Object) (integer)));
        dicomobject.set(7, ((Object) (integer2)));
        dicomobject.set(8, ((Object) (a)));
        return dicomobject;
    }

    public static DicomObject createCGetResponse(Integer integer, Integer integer1, boolean flag, Integer integer2, Integer integer3, Integer integer4, Integer integer5, Integer integer6)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(32784))));
        dicomobject.set(5, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        dicomobject.set(9, ((Object) (integer2)));
        if(integer3 != null)
            dicomobject.set(19, ((Object) (integer3)));
        if(integer4 != null)
            dicomobject.set(20, ((Object) (integer3)));
        if(integer5 != null)
            dicomobject.set(21, ((Object) (integer3)));
        if(integer6 != null)
            dicomobject.set(22, ((Object) (integer3)));
        return dicomobject;
    }

    public static DicomObject createStoreRequest(Integer integer, Integer integer1, Integer integer2, String s, String s1, String s2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(1))));
        dicomobject.set(4, ((Object) (integer)));
        dicomobject.set(7, ((Object) (integer2)));
        dicomobject.set(8, ((Object) (a)));
        dicomobject.set(13, ((Object) (s)));
        if(s1 != null)
            dicomobject.set(23, ((Object) (s1)));
        if(s2 != null)
            dicomobject.set(24, ((Object) (new Integer(s2))));
        return dicomobject;
    }

    public static DicomObject createStoreResponse(Integer integer, Integer integer1, String s, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(32769))));
        dicomobject.set(5, ((Object) (integer)));
        dicomobject.set(8, ((Object) (b)));
        dicomobject.set(9, ((Object) (integer2)));
        if(s != null)
            dicomobject.set(13, ((Object) (s)));
        return dicomobject;
    }

    public static DicomObject createFindRequest(Integer integer, Integer integer1, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(32))));
        dicomobject.set(4, ((Object) (integer)));
        dicomobject.set(8, ((Object) (a)));
        dicomobject.set(7, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createFindResponse(Integer integer, Integer integer1, boolean flag, Integer integer2)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        if(integer1 != null)
            dicomobject.set(1, ((Object) (UID.getUIDEntry(integer1.intValue()).getValue())));
        dicomobject.set(3, ((Object) (new Integer(32800))));
        dicomobject.set(5, ((Object) (integer)));
        if(flag)
            dicomobject.set(8, ((Object) (a)));
        else
            dicomobject.set(8, ((Object) (b)));
        dicomobject.set(9, ((Object) (integer2)));
        return dicomobject;
    }

    public static DicomObject createCancelRequest(Integer integer)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(3, ((Object) (new Integer(4095))));
        dicomobject.set(5, ((Object) (integer)));
        return dicomobject;
    }

}
