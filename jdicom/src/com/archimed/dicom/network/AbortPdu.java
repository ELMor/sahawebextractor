// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;

// Referenced classes of package com.archimed.dicom.network:
//            Abort

class AbortPdu
{

    private int a;
    private int b;
    private int c;
    private int d;

    public AbortPdu()
    {
        a = 7;
    }

    public AbortPdu(int i, int j)
    {
        a = 7;
        c = i;
        d = j;
    }

    public Abort getAbort()
    {
        Abort abort = new Abort(c, d);
        return abort;
    }

    public void read(InputStream inputstream)
        throws IOException, IllegalValueException
    {
        PushbackInputStream pushbackinputstream = new PushbackInputStream(inputstream);
        DataInputStream datainputstream = new DataInputStream(((InputStream) (pushbackinputstream)));
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
        {
            throw new IllegalValueException("PDU-type field of Abort PDU not " + a);
        } else
        {
            inputstream.read();
            b = datainputstream.readInt();
            inputstream.read();
            c = inputstream.read();
            d = inputstream.read();
            return;
        }
    }

    public void write(OutputStream outputstream)
        throws IOException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
        outputstream.write(a);
        outputstream.write(0);
        outputstream.write(0);
        outputstream.write(0);
        outputstream.write(0);
        dataoutputstream.write(4);
        outputstream.write(0);
        outputstream.write(0);
        outputstream.write(c);
        outputstream.write(d);
    }
}
