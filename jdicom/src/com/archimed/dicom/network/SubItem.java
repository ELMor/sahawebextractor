// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

abstract class SubItem
{

    SubItem()
    {
    }

    public abstract int getLength();

    public abstract void write(DataOutputStream dataoutputstream)
        throws IOException;

    public abstract int read(DataInputStream datainputstream)
        throws IOException, IllegalValueException, UnknownUIDException;
}
