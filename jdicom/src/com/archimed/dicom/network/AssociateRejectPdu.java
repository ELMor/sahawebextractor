// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;

// Referenced classes of package com.archimed.dicom.network:
//            Reject

class AssociateRejectPdu
{

    private int a;
    private int b;
    private int c;
    private int d;
    private int e;

    public AssociateRejectPdu()
    {
        a = 3;
    }

    public AssociateRejectPdu(int i, int j, int k)
    {
        a = 3;
        c = i;
        d = j;
        e = k;
    }

    public Reject getReject()
    {
        Reject reject = new Reject(c, d, e);
        return reject;
    }

    public void read(InputStream inputstream)
        throws IOException, IllegalValueException
    {
        PushbackInputStream pushbackinputstream = new PushbackInputStream(inputstream);
        DataInputStream datainputstream = new DataInputStream(((InputStream) (pushbackinputstream)));
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
        {
            throw new IllegalValueException("PDU-type field of Associate Reject PDU not " + a);
        } else
        {
            ((FilterInputStream) (datainputstream)).read();
            b = datainputstream.readInt();
            ((FilterInputStream) (datainputstream)).read();
            c = inputstream.read();
            d = inputstream.read();
            e = inputstream.read();
            return;
        }
    }

    public void write(OutputStream outputstream)
        throws IOException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
        outputstream.write(a);
        outputstream.write(0);
        dataoutputstream.writeInt(4);
        outputstream.write(0);
        outputstream.write(c);
        outputstream.write(d);
        outputstream.write(e);
        outputstream.flush();
    }
}
