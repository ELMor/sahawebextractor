// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.n;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

class AbstractSyntaxItem
{

    private byte a;
    private UIDEntry b;

    public AbstractSyntaxItem(UIDEntry uidentry)
    {
        a = 48;
        b = uidentry;
    }

    public AbstractSyntaxItem()
    {
        a = 48;
    }

    public UIDEntry getAbstractSyntax()
    {
        return b;
    }

    public int getLength()
    {
        return b.getValue().length() + 4;
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(((int) (a)));
        dataoutputstream.write(0);
        dataoutputstream.writeChar(b.getValue().length());
        ((FilterOutputStream) (dataoutputstream)).write(b.getValue().getBytes());
    }

    public int read(DataInputStream datainputstream)
        throws IOException, IllegalValueException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
            throw new IllegalValueException("wrong itemtype of abstract syntax");
        ((FilterInputStream) (datainputstream)).read();
        char c = datainputstream.readChar();
        byte abyte0[] = new byte[c];
        datainputstream.readFully(abyte0);
        abyte0 = n.trimZeros(abyte0);
        String s = new String(abyte0);
        try
        {
            b = UID.getUIDEntry(s);
        }
        catch(UnknownUIDException unknownuidexception)
        {
            b = new UIDEntry(0, s, "unknown uid", "??", 1);
        }
        return 4 + c;
    }
}
