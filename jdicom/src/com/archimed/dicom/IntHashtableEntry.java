// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;


class IntHashtableEntry
{

    int a;
    int b;
    Object c;
    IntHashtableEntry d;

    IntHashtableEntry()
    {
    }

    protected Object clone()
    {
        IntHashtableEntry inthashtableentry = new IntHashtableEntry();
        inthashtableentry.a = a;
        inthashtableentry.b = b;
        inthashtableentry.c = c;
        inthashtableentry.d = d == null ? null : (IntHashtableEntry)d.clone();
        return ((Object) (inthashtableentry));
    }
}
