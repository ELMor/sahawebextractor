// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;


public class UnknownUIDException extends Exception
{

    private String a;

    public UnknownUIDException(String s, String s1)
    {
        super(s1);
        a = s;
    }

    public String getUID()
    {
        return a;
    }
}
