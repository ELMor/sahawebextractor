// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.codec.rle;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package com.archimed.codec.rle:
//            v

public class u
{

    static final int a = 128;
    int b[];
    int c;
    int d;
    int e;
    int f;

    public u()
    {
        d = 0;
        e = 0;
        f = d * e;
    }

    public void setDimension(int i, int j)
    {
        d = i;
        e = j;
        f = i * j;
    }

    public void processHeader(InputStream inputstream)
        throws IOException
    {
        c = v.get4bytes(inputstream);
        b = new int[15];
        for(int i = 0; i < 15; i++)
            b[i] = v.get4bytes(inputstream);

    }

    public byte[] decodeBytes(InputStream inputstream)
        throws IOException
    {
        int i = 0;
        boolean flag = false;
        byte abyte0[] = new byte[f];
        while(i < f) 
        {
            byte byte0 = (byte)inputstream.read();
            if(byte0 >= 0 && byte0 <= 127)
            {
                for(int j = 0; j < byte0 + 1; j++)
                    if(i < f)
                        abyte0[i++] = (byte)inputstream.read();

            } else
            if(byte0 <= -1 && byte0 >= -127)
            {
                byte byte1 = (byte)inputstream.read();
                for(int k = 0; k < -byte0 + 1; k++)
                    if(i < f)
                        abyte0[i++] = byte1;

            }
        }
        return abyte0;
    }

    public byte[] encodeBytes(InputStream inputstream)
    {
        boolean flag = false;
        int j = 0;
        int k = 0;
        int l = 0;
        byte abyte0[] = new byte[128];
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        do
        {
            int i;
            try
            {
                i = inputstream.read();
            }
            catch(Exception exception)
            {
                return bytearrayoutputstream.toByteArray();
            }
            if(k == 0)
            {
                if(l == 0)
                {
                    j = i;
                    l++;
                } else
                if(j == i)
                {
                    if(++l == 128)
                    {
                        bytearrayoutputstream.write(1 - l);
                        bytearrayoutputstream.write(j);
                        l = 0;
                    }
                } else
                if(l > 2)
                {
                    bytearrayoutputstream.write(1 - l);
                    bytearrayoutputstream.write(j);
                    l = 1;
                    j = i;
                } else
                {
                    for(int i1 = 0; i1 < l; i1++)
                    {
                        abyte0[k++] = (byte)j;
                        if(k == 128)
                        {
                            bytearrayoutputstream.write(k - 1);
                            bytearrayoutputstream.write(abyte0, 0, k);
                            k = 0;
                        }
                    }

                    l = 0;
                    abyte0[k++] = (byte)i;
                    if(k == 128)
                    {
                        bytearrayoutputstream.write(k - 1);
                        bytearrayoutputstream.write(abyte0, 0, k);
                        k = 0;
                    }
                }
            } else
            if(l != 0)
            {
                if(i == j)
                {
                    if(++l == 128)
                    {
                        bytearrayoutputstream.write(k - 1);
                        bytearrayoutputstream.write(abyte0, 0, k);
                        bytearrayoutputstream.write(1 - l);
                        bytearrayoutputstream.write(j);
                        k = l = 0;
                    }
                } else
                {
                    bytearrayoutputstream.write(k - 1);
                    bytearrayoutputstream.write(abyte0, 0, k);
                    bytearrayoutputstream.write(1 - l);
                    bytearrayoutputstream.write(j);
                    k = 0;
                    l = 1;
                    j = i;
                }
            } else
            if(abyte0[k - 1] == i)
            {
                j = i;
                l = 2;
                k--;
            } else
            {
                abyte0[k++] = (byte)i;
                if(k == 128)
                {
                    bytearrayoutputstream.write(k - 1);
                    bytearrayoutputstream.write(abyte0, 0, k);
                }
            }
        } while(true);
    }

    public byte[] read(InputStream inputstream)
        throws IOException
    {
        processHeader(inputstream);
        byte abyte0[] = new byte[c * f];
        int i = 0;
        for(int j = 0; j < c;)
        {
            System.arraycopy(((Object) (decodeBytes(inputstream))), 0, ((Object) (abyte0)), i, f);
            j++;
            i += f;
        }

        return abyte0;
    }

    public byte[] write(InputStream inputstream)
        throws IOException
    {
        return null;
    }
}
