// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.codec.jpeg;

import java.io.IOException;

// Referenced classes of package com.archimed.codec.jpeg:
//            q, r, s, t

public class p
{

    public static final int SOF3 = 195;
    public static final int DHT = 196;
    public static final int SOI = 216;
    public static final int EOI = 217;
    public static final int SOS = 218;
    public static final int DRI = 221;
    public static final int RST0 = 208;
    public static final int bmask[] = {
        0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 
        1023, 2047, 4095, 8191, 16383, 32767, 65535
    };
    private q a;
    private int b;
    private int c;
    private int d;
    private int e;
    private long f;
    private int g;
    private byte h;
    private byte i;
    private byte j[];
    private byte k[];
    private int l;
    private int m;
    static final int n[] = {
        0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 
        512, 1024, 2048, 4096, 8192, 16384, 32768
    };
    static final int o[] = {
        0, -1, -3, -7, -15, -31, -63, -127, -255, -511, 
        -1023, -2047, -4095, -8191, -16383, -32767, -65535
    };

    public p()
    {
        a = new q();
        g = 0;
        l = 0;
        m = 0;
    }

    private final int a()
    {
        int i1 = (j[l++] & 0xff) << 8;
        i1 += j[l++] & 0xff;
        return i1;
    }

    private final void b()
        throws IOException
    {
        boolean flag = false;
        boolean flag1 = false;
        boolean flag2 = false;
        boolean flag3 = false;
        int l1 = a();
        if((l1 & 0xff) != 216)
            throw new IOException("Not a valid lossless JPEG file");
        do
        {
            l1 = a();
            switch(l1 & 0xff)
            {
            case 196: 
                r r1 = new r();
                l = r1.read(j, l);
                a.hts[r1.a] = r1;
                break;

            case 195: 
                int i1 = a();
                d = ((int) (j[l++]));
                c = a();
                b = a();
                byte byte0 = j[l++];
                a.fcomps = new s[byte0 + 1];
                for(int i2 = 0; i2 < byte0; i2++)
                {
                    byte byte3 = j[l++];
                    a.fcomps[byte3] = new s();
                    byte byte1 = j[l++];
                    a.fcomps[byte3].a = (byte1 & 0xf0) >> 4;
                    a.fcomps[byte3].b = byte1 & 0xf;
                    a.fcomps[byte3].c = ((int) (j[l++]));
                }

                break;

            case 218: 
                int j1 = a();
                e = ((int) (j[l++]));
                a.scomps = new t[e];
                for(int j2 = 0; j2 < e; j2++)
                {
                    byte byte4 = j[l++];
                    a.scomps[j2] = new t();
                    byte byte2 = j[l++];
                    a.scomps[j2].a = (byte2 & 0xf0) >> 4;
                    a.scomps[j2].b = byte2 & 0xf;
                }

                a.a = ((int) (j[l++]));
                l += 2;
                break;

            case 221: 
                int k1 = a();
                a.d = a();
                break;

            default:
                throw new RuntimeException("Unexpected marker: " + Integer.toHexString(l1));
            }
        } while((l1 & 0xff) != 218);
    }

    private final void a(int i1)
    {
        for(; g < 57; g += 8)
        {
            if(l == j.length)
            {
                h = 0;
                break;
            }
            h = j[l++];
            if(h == -1)
            {
                i = j[l++];
                if(i != 0)
                {
                    l = l - 2;
                    if(g >= i1)
                        break;
                    h = 0;
                }
            }
            f = f << 8 | (long)(h & 0xff);
        }

    }

    private final int c()
    {
        if(g < 8)
            a(8);
        return (int)(f >> g - 8 & 255L);
    }

    private final void b(int i1)
    {
        g -= i1;
    }

    private final int d()
    {
        if(g == 0)
            a(1);
        return (int)(f >> --g) & 1;
    }

    private final int c(int i1)
    {
        if(g < i1)
            a(i1);
        return (int)(f >> (g -= i1) & (long)bmask[i1]);
    }

    private final int a(int i1, int j1)
    {
        if(i1 < n[j1])
            return i1 + o[j1];
        else
            return i1;
    }

    private int a(int i1, int j1, int k1)
    {
        switch(a.a)
        {
        case 1: // '\001'
            return i1;

        case 2: // '\002'
            return j1;

        case 3: // '\003'
            return k1;

        case 4: // '\004'
            return (i1 + j1) - k1;

        case 5: // '\005'
            return i1 + (j1 - k1 >> 1);

        case 6: // '\006'
            return j1 + (i1 - k1 >> 1);

        case 7: // '\007'
            return i1 + j1 >> 1;
        }
        return 0;
    }

    private int d(int i1)
        throws IOException
    {
        int j1 = c();
        int k1;
        if(a.hts[i1].j[j1] != 0)
        {
            b(a.hts[i1].j[j1]);
            k1 = a.hts[i1].k[j1];
        } else
        {
            b(8);
            int i2;
            for(i2 = 8; j1 > a.hts[i1].h[i2]; i2++)
            {
                int l1 = d();
                j1 = j1 << 1 | l1;
            }

            if(i2 > 16)
                throw new IOException("bad Huffman code");
            int j2 = a.hts[i1].i[i2];
            j2 += j1 - a.hts[i1].g[i2];
            k1 = ((int) (a.hts[i1].d[j2]));
        }
        return k1;
    }

    private void e()
        throws IOException
    {
        boolean flag = false;
        boolean flag1 = false;
        boolean flag2 = false;
        for(int k2 = 0; k2 < e; k2++)
        {
            int i1 = a.scomps[k2].a;
            int i2 = d(i1);
            int k1;
            if(i2 == 0)
                k1 = 0;
            else
            if(i2 == 16)
            {
                k1 = 32768;
            } else
            {
                k1 = c(i2);
                k1 = a(k1, i2);
            }
            if(d > 8)
            {
                byte byte0 = (byte)((1 << d - 1) + k1);
                k[m] = byte0;
                byte0 = (byte)((1 << d - 1) + k1 >> 8);
                k[m + 1] = byte0;
                m += 2;
            } else
            {
                byte byte1 = (byte)((1 << d - 1) + k1);
                k[m] = byte1;
                m++;
            }
        }

        for(int l2 = 1; l2 < b; l2++)
        {
            for(int i3 = 0; i3 < e; i3++)
            {
                int j1 = a.scomps[i3].a;
                int j2 = d(j1);
                int l1;
                if(j2 == 0)
                    l1 = 0;
                else
                if(j2 == 16)
                {
                    l1 = 32768;
                } else
                {
                    l1 = c(j2);
                    l1 = a(l1, j2);
                }
                if(d > 8)
                {
                    byte byte2 = (byte)(a(k, m - 2 * e) + l1);
                    k[m] = byte2;
                    byte2 = (byte)(a(k, m - 2 * e) + l1 >> 8);
                    k[m + 1] = byte2;
                    m += 2;
                } else
                {
                    byte byte3 = (byte)((k[m - e] & 0xff) + l1);
                    k[m] = byte3;
                    m++;
                }
            }

        }

        if(a.b != 0)
            a.c--;
    }

    private void f()
        throws IOException
    {
        int j1 = g / 8;
        g = 0;
        int i1;
        do
        {
            do
                i1 = j[l++] & 0xff;
            while(i1 != 255);
            do
                i1 = j[l++] & 0xff;
            while(i1 == 255);
        } while(i1 == 0);
        if(i1 != 208 + a.e)
        {
            throw new IOException("messed up restart markers");
        } else
        {
            a.c = a.b;
            a.e = a.e + 1 & 7;
            return;
        }
    }

    private void g()
        throws IOException
    {
        m = 0;
        a.b = a.d / b;
        a.c = a.b;
        a.e = 0;
        e();
        for(int l2 = 1; l2 < c; l2++)
        {
            if(l2 != 0 && a.b != 0)
            {
                if(a.c == 0)
                {
                    f();
                    e();
                    continue;
                }
                a.c--;
            }
            for(int i3 = 0; i3 < e; i3++)
            {
                int i2 = a.scomps[i3].a;
                int i1 = d(i2);
                int k1;
                if(i1 == 0)
                    k1 = 0;
                else
                if(i1 == 16)
                {
                    k1 = 32768;
                } else
                {
                    k1 = c(i1);
                    k1 = a(k1, i1);
                }
                if(d > 8)
                {
                    byte byte0 = (byte)(a(k, m - 2 * e * b) + k1);
                    k[m] = byte0;
                    byte0 = (byte)(a(k, m - 2 * e * b) + k1 >> 8);
                    k[m + 1] = byte0;
                    m += 2;
                } else
                {
                    byte byte1 = (byte)((k[m - e * b] & 0xff) + k1);
                    k[m] = byte1;
                    m++;
                }
            }

            for(int j3 = 1; j3 < b; j3++)
            {
                for(int k3 = 0; k3 < e; k3++)
                {
                    int j2 = a.scomps[k3].a;
                    int j1 = d(j2);
                    int l1;
                    if(j1 == 0)
                        l1 = 0;
                    else
                    if(j1 == 16)
                    {
                        l1 = 32768;
                    } else
                    {
                        l1 = c(j1);
                        l1 = a(l1, j1);
                    }
                    if(d > 8)
                    {
                        int k2 = a(a(k, m - 2 * e), a(k, m - b * 2 * e), a(k, m - b * 2 * e - 2 * e)) + l1;
                        byte byte2 = (byte)k2;
                        k[m] = byte2;
                        byte2 = (byte)(k2 >> 8);
                        k[m + 1] = byte2;
                        m += 2;
                    } else
                    {
                        byte byte3 = (byte)(a(k[m - e] & 0xff, k[m - b * e] & 0xff, k[m - b * e - e] & 0xff) + l1);
                        k[m] = byte3;
                        m++;
                    }
                }

            }

        }

    }

    private int a(byte abyte0[], int i1)
    {
        return (abyte0[i1] & 0xff) + ((abyte0[i1 + 1] & 0xff) << 8);
    }

    public byte[] decode(byte abyte0[], byte abyte1[])
        throws IOException
    {
        g = 0;
        f = 0L;
        l = 0;
        j = abyte0;
        k = abyte1;
        b();
        int i1;
        if(d > 8)
            i1 = b * c * e * 2;
        else
            i1 = b * c * e;
        if(k == null)
            k = new byte[i1];
        else
        if(k.length != i1)
            throw new IllegalArgumentException("outbuffer.length does not match frame size");
        g();
        return k;
    }

}
