// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 

package com.tiani.rmicfg;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.lang.reflect.Method;
import java.rmi.MarshalException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.UnexpectedException;
import java.rmi.UnmarshalException;
import java.rmi.server.Operation;
import java.rmi.server.RemoteCall;
import java.rmi.server.RemoteObject;
import java.rmi.server.RemoteRef;
import java.rmi.server.RemoteStub;
import java.util.Properties;

// Referenced classes of package com.tiani.rmicfg:
//            IServer

public final class ServerImpl_Stub extends RemoteStub
    implements IServer, Remote
{

    private static final Operation operations[] = {
        new Operation("java.util.Properties getProperties()"), new Operation("java.lang.String getPropertyNames()[]"), new Operation("java.lang.String getType()"), new Operation("boolean isRunning()"), new Operation("void setProperties(java.util.Properties)"), new Operation("void start()"), new Operation("void start(java.util.Properties)"), new Operation("void stop(boolean, boolean)")
    };
    private static final long interfaceHash = 0x84c909b22154ab00L;
    private static final long serialVersionUID = 2L;
    private static boolean useNewInvoke;
    private static Method $method_getProperties_0;
    private static Method $method_getPropertyNames_1;
    private static Method $method_getType_2;
    private static Method $method_isRunning_3;
    private static Method $method_setProperties_4;
    private static Method $method_start_5;
    private static Method $method_start_6;
    private static Method $method_stop_7;

    public ServerImpl_Stub()
    {
    }

    public ServerImpl_Stub(RemoteRef remoteref)
    {
        super(remoteref);
    }

    static Class _mthclass$(String s)
    {
        try
        {
            return Class.forName(s);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            throw new NoClassDefFoundError(classnotfoundexception.getMessage());
        }
    }

    public Properties getProperties()
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_getProperties_0, null, 0x5c98f51d7e4942b1L);
                return (Properties)obj;
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 0, 0x84c909b22154ab00L);
            super.ref.invoke(remotecall);
            Properties properties;
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                properties = (Properties)objectinput.readObject();
            }
            catch(IOException ioexception)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling return", classnotfoundexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return properties;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public String[] getPropertyNames()
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_getPropertyNames_1, null, 0xa1e89d47ee7414fcL);
                return (String[])obj;
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 1, 0x84c909b22154ab00L);
            super.ref.invoke(remotecall);
            String as[];
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                as = (String[])objectinput.readObject();
            }
            catch(IOException ioexception)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling return", classnotfoundexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return as;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public String getType()
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_getType_2, null, 0x57fb612cb4898661L);
                return (String)obj;
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 2, 0x84c909b22154ab00L);
            super.ref.invoke(remotecall);
            String s;
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                s = (String)objectinput.readObject();
            }
            catch(IOException ioexception)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling return", classnotfoundexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return s;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public boolean isRunning()
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                Object obj = super.ref.invoke(this, $method_isRunning_3, null, 0x9ee61a22afa5aff2L);
                return ((Boolean)obj).booleanValue();
            }
            RemoteCall remotecall = super.ref.newCall(this, operations, 3, 0x84c909b22154ab00L);
            super.ref.invoke(remotecall);
            boolean flag;
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                flag = objectinput.readBoolean();
            }
            catch(IOException ioexception)
            {
                throw new UnmarshalException("error unmarshalling return", ioexception);
            }
            finally
            {
                super.ref.done(remotecall);
            }
            return flag;
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public void setProperties(Properties properties)
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                super.ref.invoke(this, $method_setProperties_4, new Object[] {
                    properties
                }, 0x5814d52e33c0c5e7L);
            } else
            {
                RemoteCall remotecall = super.ref.newCall(this, operations, 4, 0x84c909b22154ab00L);
                try
                {
                    ObjectOutput objectoutput = remotecall.getOutputStream();
                    objectoutput.writeObject(properties);
                }
                catch(IOException ioexception)
                {
                    throw new MarshalException("error marshalling arguments", ioexception);
                }
                super.ref.invoke(remotecall);
                super.ref.done(remotecall);
            }
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public void start()
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                super.ref.invoke(this, $method_start_5, null, 0x90a04074b61c5129L);
            } else
            {
                RemoteCall remotecall = super.ref.newCall(this, operations, 5, 0x84c909b22154ab00L);
                super.ref.invoke(remotecall);
                super.ref.done(remotecall);
            }
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public void start(Properties properties)
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                super.ref.invoke(this, $method_start_6, new Object[] {
                    properties
                }, 0xb4d285fba1560d91L);
            } else
            {
                RemoteCall remotecall = super.ref.newCall(this, operations, 6, 0x84c909b22154ab00L);
                try
                {
                    ObjectOutput objectoutput = remotecall.getOutputStream();
                    objectoutput.writeObject(properties);
                }
                catch(IOException ioexception)
                {
                    throw new MarshalException("error marshalling arguments", ioexception);
                }
                super.ref.invoke(remotecall);
                super.ref.done(remotecall);
            }
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    public void stop(boolean flag, boolean flag1)
        throws RemoteException
    {
        try
        {
            if(useNewInvoke)
            {
                super.ref.invoke(this, $method_stop_7, new Object[] {
                    new Boolean(flag), new Boolean(flag1)
                }, 0x5a8f6945b23c220fL);
            } else
            {
                RemoteCall remotecall = super.ref.newCall(this, operations, 7, 0x84c909b22154ab00L);
                try
                {
                    ObjectOutput objectoutput = remotecall.getOutputStream();
                    objectoutput.writeBoolean(flag);
                    objectoutput.writeBoolean(flag1);
                }
                catch(IOException ioexception)
                {
                    throw new MarshalException("error marshalling arguments", ioexception);
                }
                super.ref.invoke(remotecall);
                super.ref.done(remotecall);
            }
        }
        catch(RuntimeException runtimeexception)
        {
            throw runtimeexception;
        }
        catch(RemoteException remoteexception)
        {
            throw remoteexception;
        }
        catch(Exception exception)
        {
            throw new UnexpectedException("undeclared checked exception", exception);
        }
    }

    static 
    {
        try
        {
            (java.rmi.server.RemoteRef.class).getMethod("invoke", new Class[] {
                java.rmi.Remote.class, java.lang.reflect.Method.class, java.lang.Object[].class, Long.TYPE
            });
            useNewInvoke = true;
            $method_getProperties_0 = (com.tiani.rmicfg.IServer.class).getMethod("getProperties", new Class[0]);
            $method_getPropertyNames_1 = (com.tiani.rmicfg.IServer.class).getMethod("getPropertyNames", new Class[0]);
            $method_getType_2 = (com.tiani.rmicfg.IServer.class).getMethod("getType", new Class[0]);
            $method_isRunning_3 = (com.tiani.rmicfg.IServer.class).getMethod("isRunning", new Class[0]);
            $method_setProperties_4 = (com.tiani.rmicfg.IServer.class).getMethod("setProperties", new Class[] {
                java.util.Properties.class
            });
            $method_start_5 = (com.tiani.rmicfg.IServer.class).getMethod("start", new Class[0]);
            $method_start_6 = (com.tiani.rmicfg.IServer.class).getMethod("start", new Class[] {
                java.util.Properties.class
            });
            $method_stop_7 = (com.tiani.rmicfg.IServer.class).getMethod("stop", new Class[] {
                Boolean.TYPE, Boolean.TYPE
            });
        }
        catch(NoSuchMethodException _ex)
        {
            useNewInvoke = false;
        }
    }
}
