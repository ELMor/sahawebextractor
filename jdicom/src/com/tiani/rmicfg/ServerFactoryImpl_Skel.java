// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 

package com.tiani.rmicfg;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.rmi.MarshalException;
import java.rmi.Remote;
import java.rmi.UnmarshalException;
import java.rmi.server.Operation;
import java.rmi.server.RemoteCall;
import java.rmi.server.Skeleton;
import java.rmi.server.SkeletonMismatchException;

// Referenced classes of package com.tiani.rmicfg:
//            ServerFactoryImpl

public final class ServerFactoryImpl_Skel
    implements Skeleton
{

    private static final Operation operations[] = {
        new Operation("com.tiani.rmicfg.IServer createServer(java.lang.String, java.lang.String)"), new Operation("com.tiani.rmicfg.IServer getServer(java.lang.String)"), new Operation("java.lang.String listClassNames()[]"), new Operation("java.lang.String listServerNames()[]"), new Operation("com.tiani.rmicfg.IServer removeServer(java.lang.String)")
    };
    private static final long interfaceHash = 0x8cc52487e907c018L;

    public ServerFactoryImpl_Skel()
    {
    }

    public void dispatch(Remote remote, RemoteCall remotecall, int i, long l)
        throws Exception
    {
        if(i < 0)
        {
            if(l == 0x5aeb46e92b9e92e5L)
                i = 0;
            else
            if(l == 0x6be44bb7de8f8bf9L)
                i = 1;
            else
            if(l == 0x54a6495242aa96c2L)
                i = 2;
            else
            if(l == 0x13952f238c8d0428L)
                i = 3;
            else
            if(l == 0xd768738c5b51872aL)
                i = 4;
            else
                throw new UnmarshalException("invalid method hash");
        } else
        if(l != 0x8cc52487e907c018L)
            throw new SkeletonMismatchException("interface hash mismatch");
        ServerFactoryImpl serverfactoryimpl = (ServerFactoryImpl)remote;
        switch(i)
        {
        case 0: // '\0'
            String s;
            String s3;
            try
            {
                ObjectInput objectinput2 = remotecall.getInputStream();
                s = (String)objectinput2.readObject();
                s3 = (String)objectinput2.readObject();
            }
            catch(IOException ioexception7)
            {
                throw new UnmarshalException("error unmarshalling arguments", ioexception7);
            }
            catch(ClassNotFoundException classnotfoundexception2)
            {
                throw new UnmarshalException("error unmarshalling arguments", classnotfoundexception2);
            }
            finally
            {
                remotecall.releaseInputStream();
            }
            IServer iserver2 = serverfactoryimpl.createServer(s, s3);
            try
            {
                ObjectOutput objectoutput4 = remotecall.getResultStream(true);
                objectoutput4.writeObject(iserver2);
            }
            catch(IOException ioexception4)
            {
                throw new MarshalException("error marshalling return", ioexception4);
            }
            break;

        case 1: // '\001'
            String s1;
            try
            {
                ObjectInput objectinput = remotecall.getInputStream();
                s1 = (String)objectinput.readObject();
            }
            catch(IOException ioexception5)
            {
                throw new UnmarshalException("error unmarshalling arguments", ioexception5);
            }
            catch(ClassNotFoundException classnotfoundexception)
            {
                throw new UnmarshalException("error unmarshalling arguments", classnotfoundexception);
            }
            finally
            {
                remotecall.releaseInputStream();
            }
            IServer iserver = serverfactoryimpl.getServer(s1);
            try
            {
                ObjectOutput objectoutput2 = remotecall.getResultStream(true);
                objectoutput2.writeObject(iserver);
            }
            catch(IOException ioexception2)
            {
                throw new MarshalException("error marshalling return", ioexception2);
            }
            break;

        case 2: // '\002'
            remotecall.releaseInputStream();
            String as[] = serverfactoryimpl.listClassNames();
            try
            {
                ObjectOutput objectoutput = remotecall.getResultStream(true);
                objectoutput.writeObject(as);
            }
            catch(IOException ioexception)
            {
                throw new MarshalException("error marshalling return", ioexception);
            }
            break;

        case 3: // '\003'
            remotecall.releaseInputStream();
            String as1[] = serverfactoryimpl.listServerNames();
            try
            {
                ObjectOutput objectoutput1 = remotecall.getResultStream(true);
                objectoutput1.writeObject(as1);
            }
            catch(IOException ioexception1)
            {
                throw new MarshalException("error marshalling return", ioexception1);
            }
            break;

        case 4: // '\004'
            String s2;
            try
            {
                ObjectInput objectinput1 = remotecall.getInputStream();
                s2 = (String)objectinput1.readObject();
            }
            catch(IOException ioexception6)
            {
                throw new UnmarshalException("error unmarshalling arguments", ioexception6);
            }
            catch(ClassNotFoundException classnotfoundexception1)
            {
                throw new UnmarshalException("error unmarshalling arguments", classnotfoundexception1);
            }
            finally
            {
                remotecall.releaseInputStream();
            }
            IServer iserver1 = serverfactoryimpl.removeServer(s2);
            try
            {
                ObjectOutput objectoutput3 = remotecall.getResultStream(true);
                objectoutput3.writeObject(iserver1);
            }
            catch(IOException ioexception3)
            {
                throw new MarshalException("error marshalling return", ioexception3);
            }
            break;

        default:
            throw new UnmarshalException("invalid method number");
        }
    }

    public Operation[] getOperations()
    {
        return (Operation[])operations.clone();
    }

}
