// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerImpl.java

package com.tiani.rmicfg;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;

// Referenced classes of package com.tiani.rmicfg:
//            IServer

public class ServerImpl extends UnicastRemoteObject
    implements IServer
{

    private IServer _local;

    public ServerImpl(IServer iserver)
        throws RemoteException
    {
        _local = iserver;
    }

    public String getType()
        throws RemoteException
    {
        return _local.getType();
    }

    public String[] getPropertyNames()
        throws RemoteException
    {
        return _local.getPropertyNames();
    }

    public void setProperties(Properties properties)
        throws RemoteException
    {
        _local.setProperties(properties);
    }

    public Properties getProperties()
        throws RemoteException
    {
        return _local.getProperties();
    }

    public void start()
        throws RemoteException
    {
        _local.start();
    }

    public void start(Properties properties)
        throws RemoteException
    {
        _local.start(properties);
    }

    public void stop(boolean flag, boolean flag1)
        throws RemoteException
    {
        _local.stop(flag, flag1);
    }

    public boolean isRunning()
        throws RemoteException
    {
        return _local.isRunning();
    }
}
