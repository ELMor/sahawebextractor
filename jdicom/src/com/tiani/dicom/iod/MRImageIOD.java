// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MRImageIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, IfEqual, IfOr, ICondition, 
//            IfAnyEqual, IfNot, UserOption, CommonImage, 
//            CompositeIOD, OverlayModules, LUTModules, GeneralModules

public final class MRImageIOD
{

    static final Attribute imageModule[];
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private MRImageIOD()
    {
    }

    static 
    {
        imageModule = (new Attribute[] {
            new Attribute(58, 1, ((ICondition) (null)), CommonImage.enumImageType), new Attribute(461, 1, ((ICondition) (null)), CommonImage.is1SamplePerPixel), new Attribute(462, 1, ((ICondition) (null)), CommonImage.isMonochrome), new Attribute(475, 1, ((ICondition) (null)), CommonImage.is16BitsAllocated), new Attribute(183, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(184, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(185, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(186, 2, ((ICondition) (null)), ((ICondition) (new IfEqual(186, ((Object []) (new String[] {
                "2D", "3D"
            })))))), new Attribute(213, 2, ((ICondition) (new IfOr(new ICondition[] {
                new IfAnyEqual(184, "SK"), new IfNot(((ICondition) (new IfAnyEqual(183, "EP"))))
            }))), ((ICondition) (null))), new Attribute(214, 2, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(224, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(215, 2, ((ICondition) (new IfAnyEqual(183, "IR"))), ((ICondition) (null))), new Attribute(251, 2, ((ICondition) (new IfAnyEqual(185, ((Object []) (new String[] {
                "CG", "PPG"
            }))))), ((ICondition) (null))), new Attribute(187, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(188, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(188, ((Object []) (new String[] {
                "Y", "N"
            })))))), new Attribute(216, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(217, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(218, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(220, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(221, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(222, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(225, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(226, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(227, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(253, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(266, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(266, ((Object []) (new String[] {
                "Y", "N"
            })))))), new Attribute(267, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(268, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(269, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(270, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(271, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(272, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(273, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(274, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(275, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(276, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(323, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(324, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(330, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(331, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(331, ((Object []) (new String[] {
                "ROW", "COL"
            })))))), 
            new Attribute(332, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(334, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(333, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(333, ((Object []) (new String[] {
                "Y", "N"
            })))))), new Attribute(335, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(444, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(445, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(446, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.frameOfReferenceModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePlaneModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule, ((ICondition) (CommonImage.cContrastBolusUsed))), 
            new CompositeIOD.ModuleTableItem(imageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.uOverlayPlane))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated, CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed, OverlayModules.uOverlayPlane, LUTModules.uVOILUT, GeneralModules.cSpecificCharacterSet
        });
    }
}
