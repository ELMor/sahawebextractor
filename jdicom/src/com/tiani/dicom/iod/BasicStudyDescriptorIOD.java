// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BasicStudyDescriptorIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            UserOption, CompositeIOD, CommonImage, GeneralModules

public final class BasicStudyDescriptorIOD
{

    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private BasicStudyDescriptorIOD()
    {
    }

    static 
    {
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientSummaryModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.studyContentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.cSeriesRetrieveAET, CommonImage.cSeriesFilesetUID, CommonImage.cImageRetrieveAET, CommonImage.cImageFilesetUID
        });
    }
}
