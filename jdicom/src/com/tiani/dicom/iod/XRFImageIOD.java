// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   XRFImageIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            XRayImageIOD, UserOption, Attribute, IfAnyEqual, 
//            IfOr, ICondition, IfEqual, IfAnd, 
//            CompositeIOD, CommonImage, OverlayModules, CurveModules, 
//            LUTModules, GeneralModules

public final class XRFImageIOD extends XRayImageIOD
{

    static final UserOption uXRFPositioner;
    static final Attribute positionerModule[] = {
        new Attribute(278, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(277, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(279, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1196, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute tomoAcquisitionModule[] = {
        new Attribute(342, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(343, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(344, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1266, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1267, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1268, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private XRFImageIOD()
    {
    }

    static 
    {
        uXRFPositioner = new UserOption("U:XRF Positioner");
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule, ((ICondition) (CommonImage.cContrastBolusUsed))), new CompositeIOD.ModuleTableItem(CommonImage.cineModule, ((ICondition) (CommonImage.cMultiFrameCineImage))), new CompositeIOD.ModuleTableItem(CommonImage.multiFrameModule, ((ICondition) (CommonImage.cMultiFrameCineImage))), 
            new CompositeIOD.ModuleTableItem(CommonImage.framePointersModule, ((ICondition) (CommonImage.uFramePointers))), new CompositeIOD.ModuleTableItem(CommonImage.maskModule, ((ICondition) (CommonImage.cImageMayBeSubtracted))), new CompositeIOD.ModuleTableItem(CommonImage.displayShutterModule, ((ICondition) (CommonImage.uDisplayShutter))), new CompositeIOD.ModuleTableItem(CommonImage.deviceModule, ((ICondition) (CommonImage.uDevice))), new CompositeIOD.ModuleTableItem(CommonImage.therapyModule, ((ICondition) (CommonImage.uTherapy))), new CompositeIOD.ModuleTableItem(XRayImageIOD.imageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(XRayImageIOD.acquisitionModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(XRayImageIOD.collimatorModule, ((ICondition) (XRayImageIOD.uXRayCollimator))), new CompositeIOD.ModuleTableItem(XRayImageIOD.tableModule, ((ICondition) (XRayImageIOD.uXRayTable))), new CompositeIOD.ModuleTableItem(positionerModule, ((ICondition) (uXRFPositioner))), 
            new CompositeIOD.ModuleTableItem(tomoAcquisitionModule, ((ICondition) (new IfAnyEqual(185, "TOMO")))), new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.uOverlayPlane))), new CompositeIOD.ModuleTableItem(OverlayModules.multiFrameOverlayModule, ((ICondition) (OverlayModules.cMultiFrameOverlay))), new CompositeIOD.ModuleTableItem(CurveModules.curveModule, ((ICondition) (CurveModules.uCurve))), new CompositeIOD.ModuleTableItem(LUTModules.modalityLUTModule, ((ICondition) (new IfOr(new ICondition[] {
                new IfEqual(486, "LOG"), new IfAnd(new ICondition[] {
                    new IfEqual(486, "DISP"), LUTModules.uModalityLUT
                })
            })))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated, CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed, CommonImage.cMultiFrameImage, CommonImage.cMultiFrameCineImage, CommonImage.uFramePointers, CommonImage.cImageMayBeSubtracted, XRayImageIOD.uXRayCollimator, CommonImage.uDisplayShutter, 
            CommonImage.uTherapy, CommonImage.uDevice, XRayImageIOD.uXRayTable, uXRFPositioner, OverlayModules.uOverlayPlane, OverlayModules.cMultiFrameOverlay, CurveModules.uCurve, LUTModules.uModalityLUT, LUTModules.uVOILUT, GeneralModules.cSpecificCharacterSet
        });
    }
}
