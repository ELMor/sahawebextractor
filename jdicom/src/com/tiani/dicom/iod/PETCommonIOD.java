// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PETCommonIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, IfEqual, IfAnd, ICondition, 
//            IfMultiEqual, IfSizeEqual, IfSizeInRange, CommonImage

class PETCommonIOD
{

    static final Attribute energyWindowRangeSequence[] = {
        new Attribute(623, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(624, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final ICondition ifSeriesTypeGated;
    static final ICondition ifSeriesTypeGatedAndBeatRejection;
    public static final Attribute seriesModule[];
    static final Attribute radiopharmaceuticalInformationSequence[];
    static final Attribute interventionDrugInformationSequence[];
    static final Attribute isotopeModule[];
    static final Attribute multiGatedAcquisitionModule[] = {
        new Attribute(266, 2, ((ICondition) (null)), ((ICondition) (new IfEqual(266, ((Object []) (new String[] {
            "Y", "N"
        })))))), new Attribute(252, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(271, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(272, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(273, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(255, 3, ((ICondition) (null)), ((ICondition) (null)))
    };

    PETCommonIOD()
    {
    }

    static 
    {
        ifSeriesTypeGated = ((ICondition) (new IfEqual(669, 0, "GATED")));
        ifSeriesTypeGatedAndBeatRejection = ((ICondition) (new IfAnd(new ICondition[] {
            ifSeriesTypeGated, new IfEqual(266, "Y")
        })));
        seriesModule = (new Attribute[] {
            new Attribute(65, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(71, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(670, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(671, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(671, ((Object []) (new String[] {
                "EMISSION", "TRANSMISSION"
            })))))), new Attribute(669, 1, ((ICondition) (null)), ((ICondition) (new IfMultiEqual(669, ((Object [][]) (new String[][] {
                new String[] {
                    "STATIC", "DYNAMIC", "GATED", "WHOLE BODY"
                }, new String[] {
                    "IMAGE", "REPROJECTION"
                }
            })))))), new Attribute(672, 2, ((ICondition) (new IfEqual(669, 1, "REPROJECTION"))), ((ICondition) (null))), new Attribute(642, 1, ifSeriesTypeGated, ((ICondition) (null))), new Attribute(646, 1, ifSeriesTypeGated, ((ICondition) (null))), new Attribute(653, 1, ((ICondition) (new IfEqual(669, 0, "DYNAMIC"))), ((ICondition) (null))), new Attribute(650, 1, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(474, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(673, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(674, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(678, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(675, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(276, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(319, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(676, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(677, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(210, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(211, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(208, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(212, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(296, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(297, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(280, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(281, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(655, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(312, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(311, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(679, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(680, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(681, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(682, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(683, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(622, 3, ((ICondition) (null)), ((ICondition) (null)), energyWindowRangeSequence), new Attribute(684, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        radiopharmaceuticalInformationSequence = (new Attribute[] {
            new Attribute(660, 2, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(660, 1))), CommonImage.codeSequence), new Attribute(258, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(661, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(661, 1))), CommonImage.codeSequence), new Attribute(259, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(260, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(261, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(262, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(263, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(264, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(265, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(195, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(662, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(662, 1))), CommonImage.codeSequence)
        });
        interventionDrugInformationSequence = (new Attribute[] {
            new Attribute(198, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(192, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(192, 1))), CommonImage.codeSequence), new Attribute(199, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(190, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(191, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        isotopeModule = (new Attribute[] {
            new Attribute(625, 2, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(625, 1, 0x7fffffff))), radiopharmaceuticalInformationSequence), new Attribute(189, 3, ((ICondition) (null)), ((ICondition) (null)), interventionDrugInformationSequence)
        });
    }
}
