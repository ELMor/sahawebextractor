// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfAllEqual.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfAllEqual
    implements ICondition
{

    private int dname;
    private Object values[];

    public IfAllEqual(int i, Object aobj[])
    {
        dname = i;
        values = aobj;
    }

    public IfAllEqual(int i, Object obj)
    {
        this(i, new Object[] {
            obj
        });
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        int i = dicomobject.getSize(dname);
label0:
        for(int j = 0; j < i; j++)
        {
            Object obj = dicomobject.get(dname, j);
            for(int k = 0; k < values.length; k++)
                if(obj.equals(values[k]))
                    continue label0;

            return false;
        }

        return true;
    }
}
