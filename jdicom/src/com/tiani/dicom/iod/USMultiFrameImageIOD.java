// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   USMultiFrameImageIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            USImageCommonIOD, IfEqual, UserOption, CompositeIOD, 
//            CommonImage, LUTModules, GeneralModules, CurveModules, 
//            OverlayModules

public final class USMultiFrameImageIOD extends USImageCommonIOD
{

    static final CompositeIOD.ModuleTableItem moduleTable[];
    static final CompositeIOD.ModuleTableItem moduleTableCurve[];
    public static final UserOption userOptions[];

    private USMultiFrameImageIOD()
    {
    }

    static 
    {
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.frameOfReferenceModule, ((ICondition) (CommonImage.uFrameOfReference))), new CompositeIOD.ModuleTableItem(USImageCommonIOD.frameOfReferenceModule, ((ICondition) (USImageCommonIOD.cImagesSpatiallyRelated))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule, ((ICondition) (CommonImage.cContrastBolusUsed))), 
            new CompositeIOD.ModuleTableItem(CommonImage.cineModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.multiFrameModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.paletteColorLUTModule, ((ICondition) (new IfEqual(462, 0, "PALETTE COLOR")))), new CompositeIOD.ModuleTableItem(USImageCommonIOD.regionCalibrationModule, ((ICondition) (USImageCommonIOD.uUSRegionCalibration))), new CompositeIOD.ModuleTableItem(USImageCommonIOD.imageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        moduleTableCurve = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.frameOfReferenceModule, ((ICondition) (CommonImage.uFrameOfReference))), new CompositeIOD.ModuleTableItem(USImageCommonIOD.frameOfReferenceModule, ((ICondition) (USImageCommonIOD.cImagesSpatiallyRelated))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CurveModules.curveIdentificationModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CurveModules.curveModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CurveModules.audioModule, ((ICondition) (CurveModules.uAudio))), 
            new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, CommonImage.uFrameOfReference, USImageCommonIOD.cImagesSpatiallyRelated, CommonImage.cImagesTemporallyRelated, CommonImage.cLossyImageCompression, CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed, USImageCommonIOD.uUSRegionCalibration, USImageCommonIOD.cUSComponentCalibration, USImageCommonIOD.cImageAcquiredInStageProtocol, 
            OverlayModules.uOverlayPlane, LUTModules.uVOILUT, CurveModules.uAudio, GeneralModules.cSpecificCharacterSet
        });
    }
}
