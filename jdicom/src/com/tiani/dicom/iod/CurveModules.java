// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CurveModules.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, UserOption, CommonImage

final class CurveModules
{

    static final Attribute curveIdentificationModule[];
    static final UserOption uCurve = new UserOption("U:Curve");
    static final Attribute curveModule[] = new Attribute[0];
    static final UserOption uAudio = new UserOption("U:Audio");
    static final Attribute audioModule[] = new Attribute[0];

    private CurveModules()
    {
    }

    static 
    {
        curveIdentificationModule = (new Attribute[] {
            new Attribute(438, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(69, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(75, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(113, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(112, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(114, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence)
        });
    }
}
