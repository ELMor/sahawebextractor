// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StandaloneVOILUTIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            UserOption, CompositeIOD, CommonImage, LUTModules, 
//            GeneralModules

public final class StandaloneVOILUTIOD
{

    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private StandaloneVOILUTIOD()
    {
    }

    static 
    {
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(LUTModules.LUTIdentificationModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, GeneralModules.cSpecificCharacterSet
        });
    }
}
