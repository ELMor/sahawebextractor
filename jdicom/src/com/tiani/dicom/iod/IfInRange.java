// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfInRange.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfInRange
    implements ICondition
{

    private int dname;
    private int index;
    private int min;
    private int max;

    public IfInRange(int i, int j, int k, int l)
    {
        dname = i;
        index = j;
        min = k;
        max = l;
    }

    public IfInRange(int i, int j, int k)
    {
        this(i, 0, j, k);
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        int i = dicomobject.getI(dname, index);
        return i >= min && i <= max;
    }
}
