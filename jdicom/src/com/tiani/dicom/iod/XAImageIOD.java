// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   XAImageIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            XRayImageIOD, IfEqual, Attribute, UserOption, 
//            CommonImage, CompositeIOD, OverlayModules, CurveModules, 
//            LUTModules, GeneralModules, ICondition

public final class XAImageIOD extends XRayImageIOD
{

    static final ICondition ifPositionerMotionDynamic;
    static final Attribute positionerModule[];
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private XAImageIOD()
    {
    }

    static 
    {
        ifPositionerMotionDynamic = ((ICondition) (new IfEqual(345, "DYNAMIC")));
        positionerModule = (new Attribute[] {
            new Attribute(278, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(277, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(279, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(345, 2, ((ICondition) (CommonImage.cMultiFrameImage)), ((ICondition) (null))), new Attribute(346, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(347, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(348, 2, ifPositionerMotionDynamic, ((ICondition) (null))), new Attribute(349, 2, ifPositionerMotionDynamic, ((ICondition) (null))), new Attribute(350, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(351, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule, ((ICondition) (CommonImage.cContrastBolusUsed))), new CompositeIOD.ModuleTableItem(CommonImage.cineModule, ((ICondition) (CommonImage.cMultiFrameCineImage))), new CompositeIOD.ModuleTableItem(CommonImage.multiFrameModule, ((ICondition) (CommonImage.cMultiFrameCineImage))), 
            new CompositeIOD.ModuleTableItem(CommonImage.framePointersModule, ((ICondition) (CommonImage.uFramePointers))), new CompositeIOD.ModuleTableItem(CommonImage.maskModule, ((ICondition) (CommonImage.cImageMayBeSubtracted))), new CompositeIOD.ModuleTableItem(CommonImage.displayShutterModule, ((ICondition) (CommonImage.uDisplayShutter))), new CompositeIOD.ModuleTableItem(CommonImage.deviceModule, ((ICondition) (CommonImage.uDevice))), new CompositeIOD.ModuleTableItem(CommonImage.therapyModule, ((ICondition) (CommonImage.uTherapy))), new CompositeIOD.ModuleTableItem(XRayImageIOD.imageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(XRayImageIOD.acquisitionModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(XRayImageIOD.collimatorModule, ((ICondition) (XRayImageIOD.uXRayCollimator))), new CompositeIOD.ModuleTableItem(XRayImageIOD.tableModule, ((ICondition) (XRayImageIOD.cImageCreatedWithTableMotion))), new CompositeIOD.ModuleTableItem(positionerModule, ((ICondition) (null))), 
            new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.uOverlayPlane))), new CompositeIOD.ModuleTableItem(OverlayModules.multiFrameOverlayModule, ((ICondition) (OverlayModules.cMultiFrameOverlay))), new CompositeIOD.ModuleTableItem(CurveModules.curveModule, ((ICondition) (CurveModules.uCurve))), new CompositeIOD.ModuleTableItem(LUTModules.modalityLUTModule, ((ICondition) (LUTModules.uModalityLUT))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated, CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed, CommonImage.cMultiFrameImage, CommonImage.cMultiFrameCineImage, CommonImage.uFramePointers, CommonImage.cImageMayBeSubtracted, CommonImage.uDisplayShutter, CommonImage.uDevice, 
            CommonImage.uTherapy, XRayImageIOD.uXRayCollimator, XRayImageIOD.cImageCreatedWithTableMotion, OverlayModules.uOverlayPlane, OverlayModules.cMultiFrameOverlay, CurveModules.uCurve, LUTModules.uModalityLUT, LUTModules.uVOILUT, GeneralModules.cSpecificCharacterSet
        });
    }
}
