// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   XRayImageIOD.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, IfSizeInRange, IfEqualInt, IfSizeEqual, 
//            IfEqual, IfNot, IfPresent, IfAnd, 
//            ICondition, UserOption, IfAnyEqual, IfAllEqual, 
//            CommonImage, ICallbackUser

class XRayImageIOD
{

    public static final Attribute anatomicRegionSequence[];
    public static final Attribute primaryAnatomicStructureSequence[];
    static final ICondition bitStoredValueFilter;
    static final Attribute imageModule[];
    static final ICondition exposureNotPresent;
    static final Attribute acquisitionModule[];
    static final UserOption uXRayCollimator = new UserOption("U:X-Ray Collimator");
    static final ICondition ifCollimatorRectangular;
    static final ICondition ifCollimatorCircular;
    static final ICondition ifCollimatorPolygonal;
    static final Attribute collimatorModule[];
    static final UserOption uXRayTable = new UserOption("U:X-Ray Table");
    static final UserOption cImageCreatedWithTableMotion = new UserOption("C:Image Created With Table Motion");
    static final ICondition ifTableMotionDynamic;
    static final Attribute tableModule[];
    static final Attribute acquisitionDoseModule[] = {
        new Attribute(206, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(299, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(298, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(300, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1195, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(277, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(278, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(304, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1264, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(341, 3, ((ICondition) (null)), ((ICondition) (null))), 
        new Attribute(1219, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1220, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1221, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1222, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1315, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1316, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1317, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1318, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1263, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1301, 3, ((ICondition) (null)), ((ICondition) (null))), 
        new Attribute(1302, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1303, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1262, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute generationModule[] = {
        new Attribute(206, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(299, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(298, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(300, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1195, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1304, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1305, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1306, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1307, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(316, 3, ((ICondition) (null)), ((ICondition) (null))), 
        new Attribute(1263, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1262, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute filtrationModule[] = {
        new Attribute(305, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1301, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1302, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1303, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute gridModule[] = {
        new Attribute(309, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1294, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1295, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1296, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1297, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1298, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1299, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1300, 3, ((ICondition) (null)), ((ICondition) (null)))
    };

    XRayImageIOD()
    {
    }

    static 
    {
        anatomicRegionSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(139, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(139, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        primaryAnatomicStructureSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(141, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(141, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        bitStoredValueFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                switch(dicomobject.getI(476))
                {
                case 8: // '\b'
                    return true;

                case 12: // '\f'
                case 16: // '\020'
                    return dicomobject.getI(475) != 8;
                }
                return false;
            }

        };
        imageModule = (new Attribute[] {
            new Attribute(465, 1, ((ICondition) (CommonImage.cMultiFrameImage)), ((ICondition) (new IfEqualInt(465, new int[] {
                0x181063, 0x181065
            })))), new Attribute(504, 1, ((ICondition) (CommonImage.cLossyImageCompression)), CommonImage.enumLossyImageCompression), new Attribute(58, 1, ((ICondition) (null)), CommonImage.enumImageType), new Attribute(486, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(461, 1, ((ICondition) (null)), CommonImage.is1SamplePerPixel), new Attribute(462, 1, ((ICondition) (null)), CommonImage.isMonochrome2), new Attribute(475, 1, ((ICondition) (null)), CommonImage.is8or16BitsAllocated), new Attribute(476, 1, ((ICondition) (null)), bitStoredValueFilter), new Attribute(477, 1, ((ICondition) (null)), CommonImage.isHighBitEqualsBitsStoredLess1), new Attribute(478, 1, ((ICondition) (null)), CommonImage.isPixelRepresentation0), 
            new Attribute(185, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(138, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(138, 1))), anatomicRegionSequence), new Attribute(140, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(140, 1, 0x7fffffff))), primaryAnatomicStructureSequence), new Attribute(516, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(113, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(122, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(336, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(610, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(610, ((Object []) (new String[] {
                "YES", "NO"
            }))))))
        });
        exposureNotPresent = ((ICondition) (new IfNot(((ICondition) (new IfPresent(300))))));
        acquisitionModule = (new Attribute[] {
            new Attribute(206, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(302, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(302, ((Object []) (new String[] {
                "SC", "GR"
            })))))), new Attribute(299, 2, exposureNotPresent, ((ICondition) (null))), new Attribute(298, 2, exposureNotPresent, ((ICondition) (null))), new Attribute(300, 2, ((ICondition) (new IfNot(((ICondition) (new IfAnd(new ICondition[] {
                new IfPresent(299), new IfPresent(298)
            })))))), ((ICondition) (null))), new Attribute(309, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(301, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(303, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(306, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(307, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(296, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(297, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(308, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(316, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(304, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        ifCollimatorRectangular = ((ICondition) (new IfAnyEqual(360, "RECTANGULAR")));
        ifCollimatorCircular = ((ICondition) (new IfAnyEqual(360, "CIRCULAR")));
        ifCollimatorPolygonal = ((ICondition) (new IfAnyEqual(360, "POLYGONAL")));
        collimatorModule = (new Attribute[] {
            new Attribute(360, 1, ((ICondition) (null)), ((ICondition) (new IfAllEqual(360, ((Object []) (new String[] {
                "RECTANGULAR", "CIRCULAR", "POLYGONAL"
            })))))), new Attribute(361, 1, ifCollimatorRectangular, ((ICondition) (null))), new Attribute(362, 1, ifCollimatorRectangular, ((ICondition) (null))), new Attribute(363, 1, ifCollimatorRectangular, ((ICondition) (null))), new Attribute(364, 1, ifCollimatorRectangular, ((ICondition) (null))), new Attribute(365, 1, ifCollimatorCircular, ((ICondition) (null))), new Attribute(366, 1, ifCollimatorCircular, ((ICondition) (null))), new Attribute(367, 1, ifCollimatorPolygonal, ((ICondition) (null)))
        });
        ifTableMotionDynamic = ((ICondition) (new IfEqual(284, "DYNAMIC")));
        tableModule = (new Attribute[] {
            new Attribute(284, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(285, 2, ifTableMotionDynamic, ((ICondition) (null))), new Attribute(287, 2, ifTableMotionDynamic, ((ICondition) (null))), new Attribute(286, 2, ifTableMotionDynamic, ((ICondition) (null))), new Attribute(288, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
    }
}
