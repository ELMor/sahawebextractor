// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SCImageIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, UserOption, CompositeIOD, CommonImage, 
//            OverlayModules, LUTModules, GeneralModules

public final class SCImageIOD
{

    static final Attribute equipmentModule[] = {
        new Attribute(83, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(81, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(230, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(233, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(234, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(235, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(237, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(238, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute imageModule[] = {
        new Attribute(231, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(232, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private SCImageIOD()
    {
    }

    static 
    {
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (CommonImage.uGeneralEquipment))), new CompositeIOD.ModuleTableItem(equipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(imageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.uOverlayPlane))), 
            new CompositeIOD.ModuleTableItem(LUTModules.modalityLUTModule, ((ICondition) (LUTModules.uModalityLUT))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, CommonImage.uGeneralEquipment, CommonImage.cImagesTemporallyRelated, CommonImage.cPixelAspectRatioNot11, OverlayModules.uOverlayPlane, LUTModules.uModalityLUT, LUTModules.uVOILUT, GeneralModules.cSpecificCharacterSet
        });
    }
}
