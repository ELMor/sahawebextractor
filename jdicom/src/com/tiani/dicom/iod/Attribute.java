// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Attribute.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DDate;
import com.archimed.dicom.DDateRange;
import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.Person;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.iod:
//            IODError, ICondition, ICallbackUser, AttributeContext

public class Attribute
{
    private static class _LongRangeCheck
        implements _IVRcheck
    {

        private long _minVal;
        private long _maxVal;

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            Object obj = dicomobject.get(i, j);
            if(obj instanceof Long)
            {
                long l = ((Long)obj).longValue();
                if(l >= _minVal && l <= _maxVal)
                {
                    return true;
                } else
                {
                    vector.addElement(((Object) (new IODError(4, attribute, attributecontext))));
                    return false;
                }
            } else
            {
                vector.addElement(((Object) (new IODError(5, attribute, attributecontext))));
                return false;
            }
        }

        protected _LongRangeCheck(long l, long l1)
        {
            _minVal = l;
            _maxVal = l1;
        }
    }

    private static class _IntRangeCheck
        implements _IVRcheck
    {

        private int _minVal;
        private int _maxVal;

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            int k = dicomobject.getI(i, j);
            if(k >= _minVal && k <= _maxVal)
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(4, attribute, attributecontext))));
                return false;
            }
        }

        protected _IntRangeCheck(int i, int j)
        {
            _minVal = i;
            _maxVal = j;
        }
    }

    private static class _StringLengthCheck
        implements _IVRcheck
    {

        private int _maxLen;

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            String s = dicomobject.getS(i, j);
            boolean flag = true;
            int k = s.length();
            if(k > _maxLen)
            {
                vector.addElement(((Object) (new IODError(3, attribute, attributecontext))));
                flag = false;
            }
            if(!validateString(s, vector, icallbackuser, attributecontext))
                flag = false;
            return flag;
        }

        protected boolean validateString(String s, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
        {
            return true;
        }

        protected _StringLengthCheck(int i)
        {
            _maxLen = i;
        }
    }

    private static class SQcheck
        implements _IVRcheck
    {

        private Attribute _itemAttribs[];

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(i, j);
            if(((GroupList) (dicomobject1)).numberOfElements() == 0)
            {
                vector.addElement(((Object) (new IODError(17, attribute, attributecontext))));
                return false;
            }
            boolean flag = true;
            AttributeContext attributecontext1 = new AttributeContext(attributecontext, i, j);
            for(int k = 0; k < _itemAttribs.length; k++)
                if(!_itemAttribs[k].validate(dicomobject1, vector, icallbackuser, attributecontext1))
                    flag = false;

            return flag;
        }

        public SQcheck(Attribute aattribute[])
        {
            _itemAttribs = aattribute;
        }
    }

    private static interface _IVRcheck
    {

        public abstract boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException;
    }


    private int _dname;
    private int _type;
    private ICondition _cond;
    private ICondition _check;
    private _IVRcheck _vrCheck;
    private static final _IVRcheck _AEcheck = new _StringLengthCheck(16);
    private static final _IVRcheck _AScheck = new _StringLengthCheck(4) {

        protected boolean validateString(Attribute attribute, String s, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
        {
            if(Attribute._isValidAS(s))
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(6, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _CScheck = new _StringLengthCheck(16) {

        protected boolean validateString(Attribute attribute, String s, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
        {
            if(Attribute._isValidCS(s))
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(7, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _DAcheck = new _IVRcheck() {

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            Object obj = dicomobject.get(i, j);
            if((obj instanceof DDate) || (obj instanceof DDateRange))
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(8, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _DScheck = new _IVRcheck() {

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            Object obj = dicomobject.get(i, j);
            if(obj instanceof Float)
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(9, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _DTcheck = new _StringLengthCheck(24) {

        protected boolean validateString(Attribute attribute, String s, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
        {
            if(Attribute._isValidDT(s))
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(10, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _FDcheck = new _IVRcheck() {

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            Object obj = dicomobject.get(i, j);
            if(obj instanceof Double)
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(11, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _FLcheck = new _IVRcheck() {

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            Object obj = dicomobject.get(i, j);
            if(obj instanceof Float)
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(12, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _IScheck = new _IVRcheck() {

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            Object obj = dicomobject.get(i, j);
            if(obj instanceof Integer)
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(13, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _LOcheck = new _StringLengthCheck(64);
    private static final _IVRcheck _LTcheck = new _StringLengthCheck(10240);
    private static final _IVRcheck _PNcheck = new _IVRcheck() {

        public boolean validate(Attribute attribute, DicomObject dicomobject, int i, int j, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
            throws DicomException
        {
            Object obj = dicomobject.get(i, j);
            if(obj instanceof Person)
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(14, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _SHcheck = new _StringLengthCheck(16);
    private static final _IVRcheck _SLcheck = new _LongRangeCheck(0xffffffff80000000L, 0x7fffffffL);
    private static final _IVRcheck _SScheck = new _IntRangeCheck(-32768, 32767);
    private static final _IVRcheck _STcheck = new _StringLengthCheck(1024);
    private static final _IVRcheck _TMcheck = new _StringLengthCheck(16) {

        protected boolean validateString(Attribute attribute, String s, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
        {
            if(Attribute._isValidTM(s))
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(15, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _UIcheck = new _StringLengthCheck(64) {

        protected boolean validateString(Attribute attribute, String s, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
        {
            if(Attribute._isValidUI(s))
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(16, attribute, attributecontext))));
                return false;
            }
        }

    };
    private static final _IVRcheck _ULcheck = new _LongRangeCheck(0L, 0xffffffffL);
    private static final _IVRcheck _UScheck = new _IntRangeCheck(0, 65535);
    private static final _IVRcheck _USSScheck = new _IntRangeCheck(-32768, 65535);

    public Attribute(int i, int j, ICondition icondition, ICondition icondition1)
    {
        _dname = i;
        _type = j;
        _cond = icondition;
        _check = icondition1;
        _vrCheck = _getVRcheck(i);
    }

    public Attribute(int i, int j, ICondition icondition, ICondition icondition1, Attribute aattribute[])
    {
        _dname = i;
        _type = j;
        _cond = icondition;
        _check = icondition1;
        _vrCheck = ((_IVRcheck) (new SQcheck(aattribute)));
    }

    public final int dname()
    {
        return _dname;
    }

    public final int type()
    {
        return _type;
    }

    public final ICondition cond()
    {
        return _cond;
    }

    public final int[] getAT()
    {
        return (new int[] {
            DDict.getGroup(_dname), DDict.getElement(_dname)
        });
    }

    public String toString()
    {
        StringBuffer stringbuffer = new StringBuffer(getTagDescription(_dname));
        stringbuffer.append(", type");
        stringbuffer.append(_type);
        if(_cond != null)
            stringbuffer.append('C');
        return stringbuffer.toString();
    }

    static String getTagDescription(int i)
    {
        StringBuffer stringbuffer = new StringBuffer(DDict.getDescription(i));
        stringbuffer.append('(');
        stringbuffer.append(toHexString(DDict.getGroup(i), 4));
        stringbuffer.append(',');
        stringbuffer.append(toHexString(DDict.getElement(i), 4));
        stringbuffer.append(')');
        return stringbuffer.toString();
    }

    static String toHexString(int i, int j)
    {
        String s = Integer.toHexString(i);
        StringBuffer stringbuffer = new StringBuffer(j);
        for(int k = s.length(); k < j; k++)
            stringbuffer.append('0');

        stringbuffer.append(s);
        return stringbuffer.toString();
    }

    private static _IVRcheck _getVRcheck(int i)
    {
        switch(DDict.getTypeCode(i))
        {
        case 4: // '\004'
            return _AEcheck;

        case 17: // '\021'
            return _AScheck;

        case 5: // '\005'
            return null;

        case 9: // '\t'
            return _CScheck;

        case 11: // '\013'
            return _DAcheck;

        case 16: // '\020'
            return _DScheck;

        case 28: // '\034'
            return _DTcheck;

        case 20: // '\024'
            return _FDcheck;

        case 26: // '\032'
            return _FLcheck;

        case 15: // '\017'
            return _IScheck;

        case 6: // '\006'
            return _LOcheck;

        case 18: // '\022'
            return _LTcheck;

        case 8: // '\b'
            return null;

        case 24: // '\030'
            return null;

        case 14: // '\016'
            return _PNcheck;

        case 7: // '\007'
            return _SHcheck;

        case 19: // '\023'
            return _SLcheck;

        case 10: // '\n'
            return null;

        case 23: // '\027'
            return _SScheck;

        case 13: // '\r'
            return _STcheck;

        case 12: // '\f'
            return _TMcheck;

        case 2: // '\002'
            return _UIcheck;

        case 0: // '\0'
            return null;

        case 1: // '\001'
            return _ULcheck;

        case 3: // '\003'
            return _UScheck;

        case 27: // '\033'
            return null;

        case 25: // '\031'
            return null;

        case 21: // '\025'
            return _USSScheck;

        case 22: // '\026'
            return null;
        }
        return null;
    }

    public boolean validate(DicomObject dicomobject, Vector vector, ICallbackUser icallbackuser, AttributeContext attributecontext)
        throws DicomException
    {
        int i = dicomobject.getSize(_dname);
        switch(i)
        {
        case -1: 
            if(_type == 3 || _cond != null && !_cond.isTrue(dicomobject, icallbackuser))
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(0, this, attributecontext))));
                return false;
            }

        case 0: // '\0'
            if(_type != 1 || _cond != null && !_cond.isTrue(dicomobject, icallbackuser))
            {
                return true;
            } else
            {
                vector.addElement(((Object) (new IODError(1, this, attributecontext))));
                return false;
            }
        }
        boolean flag = true;
        if(_vrCheck != null)
        {
            for(int j = 0; j < i; j++)
                if(!_vrCheck.validate(this, dicomobject, _dname, j, vector, icallbackuser, attributecontext))
                    flag = false;

        }
        if(_check != null && !_check.isTrue(dicomobject, icallbackuser))
        {
            vector.addElement(((Object) (new IODError(2, this, attributecontext))));
            flag = false;
        }
        return flag;
    }

    private static boolean _areDigits(String s)
    {
        int i = s.length();
        for(int j = 0; j < i; j++)
            if(!Character.isDigit(s.charAt(j)))
                return false;

        return true;
    }

    private static boolean _isValidAS(String s)
    {
        if(s.length() == 4 && Character.isDigit(s.charAt(0)) && Character.isDigit(s.charAt(1)) && Character.isDigit(s.charAt(2)))
            switch(s.charAt(3))
            {
            case 68: // 'D'
            case 77: // 'M'
            case 87: // 'W'
            case 89: // 'Y'
                return true;
            }
        return false;
    }

    private static boolean _isValidCS(String s)
    {
        int i = s.length();
        for(int j = 0; j < i; j++)
        {
            char c = s.charAt(j);
            if(c == ' ' && c != '_' && !Character.isDigit(c) && !Character.isUpperCase(c))
                return false;
        }

        return true;
    }

    private static boolean _isValidDA(String s)
    {
        boolean flag = false;
        boolean flag1 = false;
        boolean flag2 = false;
        try
        {
            int k;
            int l;
            switch(s.length())
            {
            case 8: // '\b'
                int i = Integer.parseInt(s.substring(0, 4));
                k = Integer.parseInt(s.substring(4, 6));
                l = Integer.parseInt(s.substring(6, 8));
                break;

            case 10: // '\n'
                if(s.charAt(4) != '.' || s.charAt(8) != '.')
                    return false;
                int j = Integer.parseInt(s.substring(0, 4));
                k = Integer.parseInt(s.substring(5, 7));
                l = Integer.parseInt(s.substring(8, 10));
                break;

            default:
                return false;
            }
            return k > 0 && k <= 12 && l > 0 && l <= 31;
        }
        catch(NumberFormatException numberformatexception)
        {
            return false;
        }
    }

    private static boolean _isValidDT(String s)
    {
        switch(s.length())
        {
        case 26: // '\032'
            char c = s.charAt(21);
            if(c != '+' && c != '-')
                return false;
            if(!_isValidTMpart1(s.substring(22)))
                return false;
            // fall through

        case 21: // '\025'
            return _isValidDA(s.substring(0, 8)) && _isValidTM(s.substring(8, 21));

        default:
            return false;
        }
    }

    private static boolean _isValidTM(String s)
    {
        StringTokenizer stringtokenizer = new StringTokenizer(s, ".");
        switch(stringtokenizer.countTokens())
        {
        case 2: // '\002'
            return _isValidTMpart1(stringtokenizer.nextToken()) && _areDigits(stringtokenizer.nextToken());

        case 1: // '\001'
            return _isValidTMpart1(stringtokenizer.nextToken());
        }
        return false;
    }

    private static boolean _isValidTMpart1(String s)
    {
        boolean flag = false;
        int j = 0;
        int k = 0;
        try
        {
            int i;
            if(s.indexOf(':') == -1)
                switch(s.length())
                {
                case 6: // '\006'
                    k = Integer.parseInt(s.substring(4, 6));
                    // fall through

                case 4: // '\004'
                    j = Integer.parseInt(s.substring(2, 4));
                    // fall through

                case 2: // '\002'
                    i = Integer.parseInt(s.substring(0, 2));
                    break;

                case 3: // '\003'
                case 5: // '\005'
                default:
                    return false;
                }
            else
                switch(s.length())
                {
                case 8: // '\b'
                    if(s.charAt(5) != ':')
                        return false;
                    k = Integer.parseInt(s.substring(6, 8));
                    // fall through

                case 5: // '\005'
                    if(s.charAt(2) != ':')
                        return false;
                    j = Integer.parseInt(s.substring(3, 5));
                    // fall through

                case 2: // '\002'
                    i = Integer.parseInt(s.substring(0, 2));
                    break;

                default:
                    return false;
                }
            return i < 24 && j < 60 && k < 60;
        }
        catch(NumberFormatException numberformatexception)
        {
            return false;
        }
    }

    private static boolean _isValidUI(String s)
    {
        if(s.charAt(0) == '.' || s.charAt(s.length() - 1) == '.' || s.indexOf("..") != -1)
            return false;
        StringTokenizer stringtokenizer = new StringTokenizer(s, ".");
        if(!stringtokenizer.hasMoreTokens())
            return false;
        do
            if(!_isValidUIpart(stringtokenizer.nextToken()))
                return false;
        while(stringtokenizer.hasMoreTokens());
        return true;
    }

    private static boolean _isValidUIpart(String s)
    {
        return (s.charAt(0) != '0' || s.length() == 1) && _areDigits(s);
    }






}
