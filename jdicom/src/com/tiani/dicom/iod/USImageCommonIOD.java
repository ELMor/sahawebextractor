// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   USImageCommonIOD.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            UserOption, Attribute, IfInRange, IfPresent, 
//            IfEqualInt, IfSizeInRange, IfEqual, IfNot, 
//            IfSizeEqual, CommonImage, ICondition, ICallbackUser

class USImageCommonIOD
{

    static final UserOption cImagesSpatiallyRelated = new UserOption("C:Images Spatially Related");
    static final Attribute frameOfReferenceModule[] = {
        new Attribute(389, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(390, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(391, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(392, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(395, 1, ((ICondition) (null)), ((ICondition) (new IfInRange(395, 0, 12)))), new Attribute(396, 1, ((ICondition) (null)), ((ICondition) (new IfInRange(396, 0, 12)))), new Attribute(399, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(400, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(393, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(394, 3, ((ICondition) (null)), ((ICondition) (null))), 
        new Attribute(397, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(398, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption uUSRegionCalibration = new UserOption("U:US Region Calibration");
    static final UserOption cUSComponentCalibration;
    static final ICondition ifPixelComponentOrganizationExists;
    static final ICondition ifPixelComponentOrganization0;
    static final ICondition ifPixelComponentOrganization1;
    static final ICondition ifPixelComponentOrganization2;
    static final ICondition ifPixelComponentOrganization01;
    static final Attribute sequenceOfUltrasoundRegions[];
    static final Attribute regionCalibrationModule[];
    static final UserOption cImageAcquiredInStageProtocol;
    static final String piWith8BitsAllocated[] = {
        "MONOCHROME2", "RGB", "YBR_FULL", "YBR_FULL_422", "YBR_PARTIAL_422"
    };
    static final ICondition bitsAllocatedValueFilter;
    static final ICondition bitsStoredValueFilter;
    static final ICondition highBitValueFilter;
    static final ICondition planarConfigurationValueFilter;
    public static final Attribute anatomicRegionSequence[];
    public static final Attribute primaryAnatomicStructureSequence[];
    public static final Attribute transducerPositionSequence[];
    public static final Attribute transducerOrientationSequence[];
    static final Attribute imageModule[];

    USImageCommonIOD()
    {
    }

    static 
    {
        cUSComponentCalibration = new UserOption("C:US Component Calibration");
        ifPixelComponentOrganizationExists = ((ICondition) (new IfPresent(412)));
        ifPixelComponentOrganization0 = ((ICondition) (new IfEqualInt(412, 0)));
        ifPixelComponentOrganization1 = ((ICondition) (new IfEqualInt(412, 1)));
        ifPixelComponentOrganization2 = ((ICondition) (new IfEqualInt(412, 2)));
        ifPixelComponentOrganization01 = ((ICondition) (new IfEqualInt(412, new int[] {
            0, 1
        })));
        sequenceOfUltrasoundRegions = (new Attribute[] {
            new Attribute(389, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(390, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(391, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(392, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(395, 1, ((ICondition) (null)), ((ICondition) (new IfInRange(395, 0, 12)))), new Attribute(396, 1, ((ICondition) (null)), ((ICondition) (new IfInRange(396, 0, 12)))), new Attribute(399, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(400, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(393, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(394, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(397, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(398, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(386, 1, ((ICondition) (null)), ((ICondition) (new IfInRange(386, 0, 5)))), new Attribute(387, 1, ((ICondition) (null)), ((ICondition) (new IfInRange(387, 0, 18)))), new Attribute(388, 1, ((ICondition) (null)), ((ICondition) (new IfInRange(388, 0, 7)))), new Attribute(412, 1, ((ICondition) (cUSComponentCalibration)), ((ICondition) (new IfInRange(412, 0, 2)))), new Attribute(413, 1, ifPixelComponentOrganization0, ((ICondition) (null))), new Attribute(414, 1, ifPixelComponentOrganization1, ((ICondition) (null))), new Attribute(415, 1, ifPixelComponentOrganization1, ((ICondition) (null))), new Attribute(416, 1, ifPixelComponentOrganizationExists, ((ICondition) (new IfInRange(416, 0, 12)))), 
            new Attribute(417, 1, ifPixelComponentOrganizationExists, ((ICondition) (new IfInRange(417, 0, 9)))), new Attribute(418, 1, ifPixelComponentOrganization01, ((ICondition) (null))), new Attribute(419, 1, ifPixelComponentOrganization01, ((ICondition) (null))), new Attribute(420, 1, ifPixelComponentOrganization01, ((ICondition) (null))), new Attribute(421, 1, ifPixelComponentOrganization2, ((ICondition) (null))), new Attribute(422, 1, ifPixelComponentOrganization2, ((ICondition) (null))), new Attribute(423, 1, ifPixelComponentOrganization2, ((ICondition) (null))), new Attribute(401, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(403, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(404, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(405, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(406, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(407, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(408, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(409, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(410, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(411, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        regionCalibrationModule = (new Attribute[] {
            new Attribute(385, 1, ((ICondition) (null)), ((ICondition) (null)), sequenceOfUltrasoundRegions)
        });
        cImageAcquiredInStageProtocol = new UserOption("C:Image Acquired In Stage Protocol");
        bitsAllocatedValueFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                String s = dicomobject.getS(462);
                if(s == null || s.length() == 0)
                    return true;
                int i = dicomobject.getI(475);
                if(s.equals("PALETTE COLOR"))
                    return i == 8 || i == 16;
                for(int j = 0; j < USImageCommonIOD.piWith8BitsAllocated.length; j++)
                    if(s.equals(((Object) (USImageCommonIOD.piWith8BitsAllocated[j]))))
                        return i == 8;

                return true;
            }

        };
        bitsStoredValueFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                String s = dicomobject.getS(462);
                if(s == null || s.length() == 0)
                    return true;
                int i = dicomobject.getI(476);
                if(s.equals("PALETTE COLOR"))
                    return i == 8 || i == 16;
                for(int j = 0; j < USImageCommonIOD.piWith8BitsAllocated.length; j++)
                    if(s.equals(((Object) (USImageCommonIOD.piWith8BitsAllocated[j]))))
                        return i == 8;

                return true;
            }

        };
        highBitValueFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                String s = dicomobject.getS(462);
                if(s == null || s.length() == 0)
                    return true;
                int i = dicomobject.getI(477);
                if(s.equals("PALETTE COLOR"))
                    return i == 7 || i == 15;
                for(int j = 0; j < USImageCommonIOD.piWith8BitsAllocated.length; j++)
                    if(s.equals(((Object) (USImageCommonIOD.piWith8BitsAllocated[j]))))
                        return i == 7;

                return true;
            }

        };
        planarConfigurationValueFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                String s = dicomobject.getS(462);
                if(s == null || s.length() == 0)
                    return true;
                int i = dicomobject.getI(463);
                if(s.equals("RGB"))
                    return i == 0 || i == 1;
                if(s.equals("YBR_FULL"))
                    return i == 1;
                if(s.equals("YBR_FULL_422") || s.equals("YBR_PARTIAL_422"))
                    return i == 0;
                else
                    return true;
            }

        };
        anatomicRegionSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(139, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(139, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        primaryAnatomicStructureSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(141, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(141, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        transducerPositionSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(143, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(143, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        transducerOrientationSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(145, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(145, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        imageModule = (new Attribute[] {
            new Attribute(462, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(462, ((Object []) (new String[] {
                "MONOCHROME2", "PALETTE COLOR", "RGB", "YBR_FULL", "YBR_FULL_422", "YBR_PARTIAL_422"
            })))))), new Attribute(475, 1, ((ICondition) (null)), bitsAllocatedValueFilter), new Attribute(476, 1, ((ICondition) (null)), bitsStoredValueFilter), new Attribute(477, 1, ((ICondition) (null)), highBitValueFilter), new Attribute(463, 1, ((ICondition) (new IfNot(CommonImage.is1SamplePerPixel))), planarConfigurationValueFilter), new Attribute(478, 1, ((ICondition) (null)), CommonImage.isPixelRepresentation0), new Attribute(465, 1, ((ICondition) (new IfPresent(464))), ((ICondition) (null))), new Attribute(58, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(504, 1, ((ICondition) (CommonImage.cLossyImageCompression)), CommonImage.enumLossyImageCompression), new Attribute(126, 2, ((ICondition) (cImageAcquiredInStageProtocol)), ((ICondition) (null))), 
            new Attribute(129, 2, ((ICondition) (cImageAcquiredInStageProtocol)), ((ICondition) (null))), new Attribute(469, 3, ((ICondition) (null)), ((ICondition) (new IfEqualInt(469, new int[] {
                0, 1
            })))), new Attribute(112, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(114, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(124, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(125, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(127, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(128, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(130, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(131, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(138, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(138, 1))), anatomicRegionSequence), new Attribute(140, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(140, 1, 0x7fffffff))), primaryAnatomicStructureSequence), new Attribute(142, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(142, 1, 0x7fffffff))), transducerPositionSequence), new Attribute(144, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(144, 1, 0x7fffffff))), transducerOrientationSequence), new Attribute(251, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(253, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(266, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(266, 3, ((Object []) (new String[] {
                "Y", "N"
            })))))), new Attribute(267, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(268, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(273, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(368, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(369, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(402, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(370, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(371, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(373, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(374, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(375, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(376, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(377, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(378, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(379, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(382, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(383, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1164, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
    }
}
