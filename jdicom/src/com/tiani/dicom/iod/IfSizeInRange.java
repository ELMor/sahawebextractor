// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfSizeInRange.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfSizeInRange
    implements ICondition
{

    private int dname;
    private int min;
    private int max;

    public IfSizeInRange(int i, int j, int k)
    {
        dname = i;
        min = j;
        max = k;
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        int i = dicomobject.getSize(dname);
        return i >= min && i <= max;
    }
}
