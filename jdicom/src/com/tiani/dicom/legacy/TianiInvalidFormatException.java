// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   TianiInvalidFormatException.java

package com.tiani.dicom.legacy;

import java.io.IOException;

public class TianiInvalidFormatException extends IOException
{

    public TianiInvalidFormatException()
    {
    }

    public TianiInvalidFormatException(String s)
    {
        super(s);
    }
}
