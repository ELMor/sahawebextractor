// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BirniHistoryReader.java

package com.tiani.dicom.legacy;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.StringTokenizer;

// Referenced classes of package com.tiani.dicom.legacy:
//            TianiInvalidFormatException

public class BirniHistoryReader extends BufferedReader
{

    private String _line;
    private String _tokens[];
    private Hashtable _action;
    private Float _center;
    private Float _width;
    private int _rotate;
    private boolean _hFlip;
    private int _x0;
    private int _y0;
    private int _dx;
    private int _dy;
    private boolean _inverse;

    public BirniHistoryReader(InputStream inputstream)
    {
        super(((java.io.Reader) (new InputStreamReader(inputstream))));
        _action = new Hashtable();
        _center = null;
        _width = null;
        _rotate = 0;
        _hFlip = false;
        _x0 = 0;
        _y0 = 0;
        _dx = 0;
        _dy = 0;
        _inverse = false;
    }

    public void read(DicomObject dicomobject)
        throws IOException, TianiInvalidFormatException, DicomException
    {
        parse();
        fill(dicomobject);
    }

    private void parse()
        throws IOException, TianiInvalidFormatException
    {
        int i = 0;
        int j = 0;
        while((_line = ((BufferedReader)this).readLine()) != null) 
            if(j == 0)
            {
                if(_line.equals("[ WORKLH"))
                    j = 1;
            } else
            {
                StringTokenizer stringtokenizer = new StringTokenizer(_line);
                int k = stringtokenizer.countTokens();
                if(k != 0)
                {
                    _tokens = new String[k];
                    for(int l = 0; l < k; l++)
                        _tokens[l] = stringtokenizer.nextToken();

                    switch(j)
                    {
                    default:
                        break;

                    case 1: // '\001'
                        i = tkToInt("#WORKS");
                        j = 2;
                        break;

                    case 2: // '\002'
                        if(i == 0)
                        {
                            checkToken("]", "WORKLH");
                            return;
                        }
                        checkToken("[", "PIC_ACTION");
                        j = 3;
                        break;

                    case 3: // '\003'
                        _action.clear();
                        _action.put("#ACTION", ((Object) (new Integer(tkToInt("#ACTION")))));
                        j = 4;
                        break;

                    case 4: // '\004'
                        if(_tokens[0].equals("]"))
                        {
                            checkToken("]", "PIC_ACTION");
                            storeAction();
                            i--;
                            j = 2;
                        } else
                        {
                            _action.put(((Object) (_tokens[0])), ((Object) (tkToFloat())));
                        }
                        break;
                    }
                }
            }
        throw new TianiInvalidFormatException(j != 0 ? "Unexpected end of file" : "Missing Tag: [ WORKLH");
    }

    private void checkToken(String s, String s1)
        throws TianiInvalidFormatException
    {
        if(_tokens.length != 2 || !_tokens[0].equals(((Object) (s))) || !_tokens[1].equals(((Object) (s1))))
            throw new TianiInvalidFormatException(_line);
        else
            return;
    }

    private int tkToInt(String s)
        throws TianiInvalidFormatException
    {
        if(_tokens.length == 2 && _tokens[0].equals(((Object) (s))))
            try
            {
                return Integer.parseInt(_tokens[1]);
            }
            catch(NumberFormatException numberformatexception) { }
        throw new TianiInvalidFormatException(_line);
    }

    private Float tkToFloat()
        throws TianiInvalidFormatException
    {
        if(_tokens.length == 2)
            try
            {
                return new Float(_tokens[1]);
            }
            catch(NumberFormatException numberformatexception) { }
        throw new TianiInvalidFormatException(_line);
    }

    private void storeAction()
        throws TianiInvalidFormatException
    {
        if(_action.containsKey("#CENTER"))
        {
            _center = itemToFloat("#CENTER");
            _width = itemToFloat("#WIDTH");
        } else
        if(_action.containsKey("#SCALE"))
        {
            _x0 = itemToFloat("#X_OFF").intValue();
            _y0 = itemToFloat("#Y_OFF").intValue();
            _dx = itemToFloat("#WIDTH").intValue();
            _dy = itemToFloat("#HEIGHT").intValue();
        } else
        if(_action.containsKey("#ROTATE"))
            storeRotate(itemToFloat("#ROTATE").intValue());
        else
        if(_action.containsKey("#FLIP_HOR_NUMBER"))
            _hFlip = !_hFlip;
        else
        if(_action.containsKey("#FLIP_VER_NUMBER"))
        {
            _hFlip = !_hFlip;
            _rotate += 180;
        } else
        if(_action.containsKey("#INVERT"))
            _inverse = !_inverse;
        else
            Debug.out.println("jdicom: BirniHistoryReader: WARNING: Unrecognized #ACTION " + _action.get("#ACTION"));
    }

    private void storeRotate(int i)
    {
        if(_hFlip)
            _rotate -= i;
        else
            _rotate += i;
    }

    private Float itemToFloat(String s)
        throws TianiInvalidFormatException
    {
        Float float1 = (Float)_action.get(((Object) (s)));
        if(float1 == null)
            throw new TianiInvalidFormatException("Missing Tag: " + s + " in #ACTION " + _action.get("#ACTION"));
        else
            return float1;
    }

    private void fill(DicomObject dicomobject)
        throws DicomException
    {
        if(_center != null && _width != null)
        {
            DicomObject dicomobject1 = new DicomObject();
            dicomobject1.set_ge(41, 16, "TIANI");
            dicomobject1.set(1512, ((Object) (_center)));
            dicomobject1.set(1513, ((Object) (_width)));
            dicomobject.set(1408, ((Object) (dicomobject1)));
        }
        _rotate %= 360;
        if(_rotate < 0)
            _rotate += 360;
        if(_rotate != 0 || _hFlip)
        {
            dicomobject.set(1410, ((Object) (new Integer(_rotate))));
            dicomobject.set(1398, ((Object) (_hFlip ? "Y" : "N")));
        }
        DicomObject dicomobject2 = new DicomObject();
        dicomobject2.set(1411, ((Object) (new Long(_x0))), 0);
        dicomobject2.set(1411, ((Object) (new Long(_y0 + _dy))), 1);
        dicomobject2.set(1412, ((Object) (new Long(_x0 + _dx))), 0);
        dicomobject2.set(1412, ((Object) (new Long(_y0))), 1);
        dicomobject2.set(1415, "SCALE TO FIT");
        dicomobject.set(1413, ((Object) (dicomobject2)));
        dicomobject.set(1240, ((Object) (_inverse ? "INVERSE" : "IDENTITY")));
    }
}
