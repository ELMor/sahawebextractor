// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PrintMgtSCU.java

package com.tiani.dicom.storetoprint;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.IDimseListener;
import com.tiani.dicom.overlay.AttribOverlayFactory;
import com.tiani.dicom.overlay.BurnInOverlay;
import com.tiani.dicom.overlay.Overlay;
import com.tiani.dicom.print.PrintManagementSCU;
import com.tiani.dicom.print.PrintManagementUtils;
import com.tiani.dicom.util.EnumPMI;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.Properties;

// Referenced classes of package com.tiani.dicom.storetoprint:
//            StoreToPrintException, Param

final class PrintMgtSCU extends PrintManagementSCU
{

    private static final int UNKNOWN = 0;
    private static final int NORMAL = 1;
    private static final int WARNING = 2;
    private static final int FAILURE = 3;
    private final Param param;
    private final DicomObject filmSessionAttribs = new DicomObject();
    private final DicomObject filmBoxAttribs = new DicomObject();
    private final DicomObject imageBoxAttribs = new DicomObject();
    private int imagePos;
    private int printerStatus;
    private String printerStatusInfo;

    private static int toPrinterStatusConst(String s)
    {
        if("NORMAL".equals(((Object) (s))))
            return 1;
        if("WARNING".equals(((Object) (s))))
            return 2;
        return !"FAILURE".equals(((Object) (s))) ? 0 : 3;
    }

    private void checkPrinterStatus(int i)
        throws StoreToPrintException
    {
        if(printerStatus == 0)
        {
            Object obj = null;
            DicomObject dicomobject = null;
            int j = -1;
            try
            {
                DicomMessage dicommessage = ((PrintManagementSCU)this).getPrinterStatus(i);
                dicomobject = dicommessage.getDataset();
                j = ((DicomObject) (dicommessage)).getI(9);
            }
            catch(Exception exception)
            {
                Debug.out.println(((Object) (exception)));
            }
            if(dicomobject == null)
                throw new StoreToPrintException("Retrieve of printer status failed! Error status: " + Integer.toHexString(j), 42755);
            printerStatus = toPrinterStatusConst((String)dicomobject.get(760));
            printerStatusInfo = (String)dicomobject.get(761);
        }
        switch(printerStatus)
        {
        case 1: // '\001'
            return;

        case 2: // '\002'
            if(param.isWarningAsFailure())
                throw new StoreToPrintException(printerStatusInfo, 42756);
            else
                return;
        }
        throw new StoreToPrintException(printerStatusInfo, 42755);
    }

    public PrintMgtSCU(Param param1)
        throws DicomException
    {
        imagePos = 0;
        printerStatus = 0;
        printerStatusInfo = "";
        param = param1;
        PrintManagementUtils.setFilmSessionCreateAttribs(filmSessionAttribs, param1.getProperties());
        PrintManagementUtils.setFilmBoxCreateAttribs(filmBoxAttribs, param1.getProperties());
        PrintManagementUtils.setImageBoxSetAttribs(imageBoxAttribs, param1.getProperties());
        ((PrintManagementSCU)this).addPrinterStatusListener(new IDimseListener() {

            public void notify(DicomMessage dicommessage)
            {
                try
                {
                    printerStatus = ((DicomObject) (dicommessage)).getI(15);
                    DicomObject dicomobject = dicommessage.getDataset();
                    printerStatusInfo = dicomobject == null ? "" : dicomobject.getS(761);
                }
                catch(DicomException dicomexception)
                {
                    Debug.out.println(((Object) (dicomexception)));
                }
            }

        });
    }

    public int store(String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
    {
        DicomObject dicomobject = dicommessage.getDataset();
        try
        {
            if(!((PrintManagementSCU)this).isConnected())
                throw new StoreToPrintException("Association to printer aborted!", 42752);
            if(isMonochrome(dicomobject))
            {
                if(!((PrintManagementSCU)this).isEnabled(12292))
                    throw new StoreToPrintException("No BasicGrayscalePrintManagement SOP negotiated!", 42753);
                checkPrinterStatus(12292);
                if(((PrintManagementSCU)this).isColorFilmSession())
                    printAndDeleteFilmSession();
                if(!((PrintManagementSCU)this).isFilmSession())
                {
                    DicomMessage dicommessage2 = ((PrintManagementSCU)this).createGrayscaleFilmSession(filmSessionAttribs);
                    if(!((PrintManagementSCU)this).isFilmSession())
                        throw new StoreToPrintException("Creation of Basic Film Session failed! Error status: " + Integer.toHexString(((DicomObject) (dicommessage2)).getI(9)), 42758);
                }
                if(!((PrintManagementSCU)this).isFilmBox())
                {
                    DicomMessage dicommessage3 = ((PrintManagementSCU)this).createFilmBox(filmBoxAttribs);
                    imagePos = 0;
                    if(!((PrintManagementSCU)this).isFilmBox())
                        throw new StoreToPrintException("Creation of Film Box failed! Error status: " + Integer.toHexString(((DicomObject) (dicommessage3)).getI(9)), 42759);
                }
                ((PrintManagementSCU)this).setGrayscaleImageBox(++imagePos, prepareImageBox(dicomobject), imageBoxAttribs);
            } else
            {
                if(!((PrintManagementSCU)this).isEnabled(12294))
                    throw new StoreToPrintException("No BasicColorPrintManagement SOP negotiated!", 42754);
                checkPrinterStatus(12294);
                if(!((PrintManagementSCU)this).isGrayscaleFilmSession())
                    printAndDeleteFilmSession();
                if(!((PrintManagementSCU)this).isFilmSession())
                {
                    DicomMessage dicommessage4 = ((PrintManagementSCU)this).createColorFilmSession(filmSessionAttribs);
                    if(!((PrintManagementSCU)this).isFilmSession())
                        throw new StoreToPrintException("Creation of Basic Film Session failed! Error status: " + Integer.toHexString(((DicomObject) (dicommessage4)).getI(9)), 42758);
                }
                if(!((PrintManagementSCU)this).isFilmBox())
                {
                    DicomMessage dicommessage5 = ((PrintManagementSCU)this).createFilmBox(filmBoxAttribs);
                    imagePos = 0;
                    if(!((PrintManagementSCU)this).isFilmBox())
                        throw new StoreToPrintException("Creation of Film Box failed! Error status: " + Integer.toHexString(((DicomObject) (dicommessage5)).getI(9)), 42759);
                }
                ((PrintManagementSCU)this).setColorImageBox(++imagePos, prepareImageBox(dicomobject), imageBoxAttribs);
            }
            if(imagePos >= ((PrintManagementSCU)this).countImageBoxes())
            {
                ((PrintManagementSCU)this).printFilmBox();
                ((PrintManagementSCU)this).deleteFilmBox();
            }
            return 0;
        }
        catch(StoreToPrintException storetoprintexception)
        {
            ((Throwable) (storetoprintexception)).printStackTrace(Debug.out);
            try
            {
                ((DicomObject) (dicommessage1)).set(11, ((Object) (((Throwable) (storetoprintexception)).getMessage())));
            }
            catch(DicomException dicomexception) { }
            return storetoprintexception.getStatus();
        }
        catch(Exception exception)
        {
            ((Throwable) (exception)).printStackTrace(Debug.out);
            try
            {
                ((DicomObject) (dicommessage1)).set(11, ((Object) (((Throwable) (exception)).getMessage())));
            }
            catch(DicomException dicomexception1) { }
            return 43007;
        }
    }

    public void close()
        throws IOException, IllegalValueException, InterruptedException, DicomException
    {
        if(!((PrintManagementSCU)this).isConnected())
            return;
        if(((PrintManagementSCU)this).isFilmSession())
            printAndDeleteFilmSession();
        ((PrintManagementSCU)this).release();
    }

    public Response open(Request request)
        throws IOException, IllegalValueException, UnknownUIDException, UnknownHostException, DicomException
    {
        return ((PrintManagementSCU)this).connect(param.getPrintHost(), param.getPrintPort(), request.getCalledTitle(), request.getCallingTitle(), param.getAbstractSyntaxes());
    }

    private static boolean isMonochrome(DicomObject dicomobject)
        throws DicomException
    {
        String s = dicomobject.getS(462);
        if(s == null || s.length() == 0)
            throw new DicomException("Missing Photometric Intepretation");
        else
            return s.startsWith("MONOCHROME");
    }

    private void printAndDeleteFilmSession()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(((PrintManagementSCU)this).isFilmBox())
        {
            ((PrintManagementSCU)this).printFilmBox();
            ((PrintManagementSCU)this).deleteFilmBox();
        }
        ((PrintManagementSCU)this).deleteFilmSession();
    }

    private DicomObject prepareImageBox(DicomObject dicomobject)
        throws DicomException, IllegalValueException
    {
        String s = (String)dicomobject.get(462);
        if(EnumPMI.isGrayscale(s))
            PrintManagementUtils.preformatGrayscale(dicomobject, ((DicomObject) (null)), param.getBitDepth(), param.getInflateBitsAlloc(), param.isMinMaxWindowing());
        if(prepareOverlays(dicomobject) > 0)
        {
            if(param.getVerbose() > 0)
                Debug.out.println("Burn in overlays");
            BurnInOverlay.burnInAll(dicomobject);
        }
        PrintManagementUtils.setPixelAspectRatio(dicomobject, param.isSendAspectRatioAlways());
        return PrintManagementUtils.getPixelModule(dicomobject);
    }

    private int prepareOverlays(DicomObject dicomobject)
        throws DicomException
    {
        int i = dicomobject.getI(466);
        int j = dicomobject.getI(467);
        int k = Overlay.listOverlayGroups(dicomobject).length;
        if(param.getBurnInInfo() > (k <= 0 ? 0 : 1))
        {
            int l = Overlay.getFreeOverlayGroup(dicomobject);
            if(l == -1)
            {
                Debug.out.println("No free overlay group 60xx to burn in attribute info");
            } else
            {
                if(param.getVerbose() > 0)
                    Debug.out.println("Create info overlay");
                createOverlayFactory().createOverlay(i, j & -8, dicomobject, l);
                k++;
            }
        }
        return k;
    }

    private AttribOverlayFactory createOverlayFactory()
    {
        Properties properties = null;
        String s = param.getBurnInInfoProperties();
        if(s.length() > 0)
            try
            {
                FileInputStream fileinputstream = new FileInputStream(s);
                try
                {
                    properties = new Properties();
                    properties.load(((java.io.InputStream) (fileinputstream)));
                }
                finally
                {
                    fileinputstream.close();
                }
            }
            catch(Exception exception)
            {
                Debug.out.println(((Object) (exception)));
                properties = null;
            }
        return new AttribOverlayFactory(properties);
    }


}
