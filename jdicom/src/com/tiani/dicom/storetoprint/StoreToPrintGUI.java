// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StoreToPrintGUI.java

package com.tiani.dicom.storetoprint;

import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.util.CheckParam;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;

// Referenced classes of package com.tiani.dicom.storetoprint:
//            StoreToPrintApplet, StoreToPrint, Param

public class StoreToPrintGUI
{

    public StoreToPrintGUI()
    {
    }

    public static void main(String args[])
    {
        try
        {
            final String propFile = args.length <= 0 ? "StoreToPrint.properties" : args[0];
            final StoreToPrintApplet applet = new StoreToPrintApplet(StoreToPrint.loadProperties(propFile));
            new AppletFrame("StoreToPrint v1.4.5", ((java.applet.Applet) (applet)), 400, 400, ((java.awt.event.WindowListener) (new WindowAdapter() {

                public void windowClosing(WindowEvent windowevent)
                {
                    try
                    {
                        StoreToPrintGUI.storeParams(propFile, applet.getParams());
                    }
                    catch(Exception exception)
                    {
                        System.out.println(((Object) (exception)));
                    }
                    System.exit(0);
                }

            })));
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(System.out);
        }
    }

    private static void storeParams(String s, Properties properties)
        throws IOException
    {
        CheckParam.verify(properties, Param.CHECKS);
        FileOutputStream fileoutputstream = new FileOutputStream(s);
        try
        {
            properties.store(((java.io.OutputStream) (fileoutputstream)), "Properties for StoreToPrint");
        }
        finally
        {
            fileoutputstream.close();
        }
    }

}
