// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PrintSCURelay.java

package com.tiani.dicom.storetoprint;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAcceptancePolicy;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.print.PrintManagementSCU;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Hashtable;

// Referenced classes of package com.tiani.dicom.storetoprint:
//            PrintMgtSCU, Param

class PrintSCURelay
    implements IAcceptancePolicy, IAssociationListener
{

    private Param _param;
    private Hashtable _printMgtSCUs;
    private static final int _abstractSyntaxIDs[] = {
        4097, 4118, 4119, 4120, 4121, 4122, 4123, 4128, 4129, 4131, 
        4151, 4157, 4158, 4159, 4153, 4161, 4162, 4163, 4164, 4176, 
        4177, 4178, 4179, 4180, 4181
    };
    private static int _transferSyntaxID = 8193;
    private static boolean _rejectnocontext = true;

    public PrintSCURelay(Param param)
    {
        _printMgtSCUs = new Hashtable();
        _param = param;
    }

    public void setParams(Param param)
    {
        _param = param;
    }

    public Response prepareResponse(VerboseAssociation verboseassociation, Request request)
        throws IllegalValueException
    {
        Response response = ResponsePolicy.prepareResponse(request, ((String) (null)), ((String []) (null)), _abstractSyntaxIDs, _transferSyntaxID, _rejectnocontext);
        if(!(response instanceof Acknowledge))
            return response;
        try
        {
            PrintMgtSCU printmgtscu = new PrintMgtSCU(_param);
            ((PrintManagementSCU) (printmgtscu)).setMaxPduSize(_param.getMaxPduSize());
            ((PrintManagementSCU) (printmgtscu)).setARTIM(_param.getReleaseTimeout());
            Response response1 = printmgtscu.open(request);
            if(!(response1 instanceof Acknowledge))
            {
                return response1;
            } else
            {
                _printMgtSCUs.put(((Object) (verboseassociation)), ((Object) (printmgtscu)));
                ((Acknowledge)response).setMaxPduSize(_param.getMaxPduSize());
                ((Acknowledge)response).setMaxOperationsInvoked(((Acknowledge)response1).getMaxOperationsInvoked());
                return response;
            }
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
        return ((Response) (new Abort(Abort.DICOM_UL_SERVICE_USER, Abort.REASON_NOT_SPECIFIED)));
    }

    public void echo(DimseExchange dimseexchange)
        throws IOException, IllegalValueException, DicomException
    {
        PrintMgtSCU printmgtscu = (PrintMgtSCU)_printMgtSCUs.get(((Object) (dimseexchange.getAssociation())));
        if(((PrintManagementSCU) (printmgtscu)).isEnabled(4097))
            try
            {
                ((PrintManagementSCU) (printmgtscu)).echo();
            }
            catch(Exception exception)
            {
                Debug.out.println(((Object) (exception)));
            }
    }

    public int store(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
    {
        return ((PrintMgtSCU)_printMgtSCUs.get(((Object) (dimseexchange.getAssociation())))).store(s, s1, dicommessage, dicommessage1);
    }

    public void associateRequestReceived(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseReceived(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void associateRequestSent(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseSent(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void releaseRequestReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseRequestSent(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseSent(VerboseAssociation verboseassociation)
    {
    }

    public void abortReceived(VerboseAssociation verboseassociation, Abort abort)
    {
    }

    public void abortSent(VerboseAssociation verboseassociation, int i, int j)
    {
    }

    public void socketClosed(VerboseAssociation verboseassociation)
    {
        PrintMgtSCU printmgtscu = (PrintMgtSCU)_printMgtSCUs.remove(((Object) (verboseassociation)));
        if(printmgtscu != null)
            try
            {
                printmgtscu.close();
            }
            catch(Exception exception)
            {
                Debug.out.println(((Object) (exception)));
            }
    }

}
