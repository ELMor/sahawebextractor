// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CEchoSCP.java

package com.tiani.dicom.storetoprint;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import java.io.IOException;

// Referenced classes of package com.tiani.dicom.storetoprint:
//            PrintSCURelay

final class CEchoSCP extends DefCEchoSCP
{

    private PrintSCURelay _relay;

    public CEchoSCP(PrintSCURelay printscurelay)
    {
        _relay = printscurelay;
    }

    public void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        _relay.echo(dimseexchange);
        super.handleRQ(dimseexchange, i, s, dicommessage);
    }
}
