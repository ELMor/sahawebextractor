// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   AdvancedAcceptancePolicy.java

package com.tiani.dicom.framework;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Reject;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.framework:
//            IAcceptancePolicy, VerboseAssociation

public class AdvancedAcceptancePolicy
    implements IAcceptancePolicy {

  private Vector _calledtitles;
  private Vector _callingtitles;
  private final Hashtable _aetSpecificPolicy = new Hashtable();
  private final Hashtable _ipcheck = new Hashtable();
  private final Vector _abstractsyntaxes = new Vector();
  private final Vector _transfersyntaxes = new Vector();
  private int _maxopinvoked;
  private int _maxopperformed;
  private int _maxpdusize;
  private final Vector _asroles = new Vector();
  private final Vector _scuroles = new Vector();
  private final Vector _scproles = new Vector();
  public static final Reject APPLICATIONCONTEXTNAME_NOT_SUPPORTED;
  public static final Reject CALLED_AETITLE_NOT_RECOGNIZED;
  public static final Reject CALLING_AETITLE_NOT_RECOGNIZED;
  public static final Reject NO_REASON_GIVEN;

  public AdvancedAcceptancePolicy() {
    _calledtitles = null;
    _callingtitles = null;
    _maxopinvoked = 1;
    _maxopperformed = 1;
    _maxpdusize = 0;
  }

  public String[] getCallingTitles() {
    if (_callingtitles == null) {
      return null;
    }
    else {
      String as[] = new String[_callingtitles.size()];
      _callingtitles.copyInto( ( (Object[]) (as)));
      return as;
    }
  }

  public String getCalledTitle() {
    if (_calledtitles == null || _calledtitles.size() == 0) {
      return null;
    }
    else {
      return (String) _calledtitles.elementAt(0);
    }
  }

  public String[] getCalledTitles() {
    if (_calledtitles == null) {
      return null;
    }
    else {
      String as[] = new String[_calledtitles.size()];
      _calledtitles.copyInto( ( (Object[]) (as)));
      return as;
    }
  }

  public void setCalledTitle(String s) {
    if (s == null) {
      _calledtitles = null;
      return;
    }
    else {
      setCalledTitles(new String[] {
                      s
      });
      return;
    }
  }

  public void setCalledTitles(String as[]) {
    if (as == null) {
      _calledtitles = null;
      return;
    }
    _calledtitles = new Vector();
    for (int i = 0; i < as.length; i++) {
      _calledtitles.addElement( ( (Object) (as[i])));

    }
  }

  public void addCalledTitle(String s) {
    if (_calledtitles == null) {
      _calledtitles = new Vector();
    }
    if (_calledtitles.indexOf( ( (Object) (s))) == -1) {
      _calledtitles.addElement( ( (Object) (s)));
    }
  }

  public boolean removeCalledTitle(String s) {
    if (_calledtitles == null) {
      return false;
    }
    else {
      return _calledtitles.removeElement( ( (Object) (s)));
    }
  }

  public void setCallingTitles(String as[]) {
    if (as == null) {
      _callingtitles = null;
      return;
    }
    _callingtitles = new Vector();
    for (int i = 0; i < as.length; i++) {
      _callingtitles.addElement( ( (Object) (as[i])));

    }
  }

  public void addCallingTitle(String s) {
    if (_callingtitles == null) {
      _callingtitles = new Vector();
    }
    if (_callingtitles.indexOf( ( (Object) (s))) == -1) {
      _callingtitles.addElement( ( (Object) (s)));
    }
  }

  public boolean removeCallingTitle(String s) {
    if (_callingtitles == null) {
      return false;
    }
    else {
      return _callingtitles.removeElement( ( (Object) (s)));
    }
  }

  public void setMaxOperationsInvoked(int i) {
    _maxopinvoked = i;
  }

  public int getMaxOperationsInvoked() {
    return _maxopinvoked;
  }

  public void setMaxOperationsPerformed(int i) {
    _maxopperformed = i;
  }

  public int getMaxOperationsPerformed() {
    return _maxopperformed;
  }

  public void setMaxPduSize(int i) {
    _maxpdusize = i;
  }

  public int getMaxPduSize() {
    return _maxpdusize;
  }

  public int getScuRole(int i) throws IllegalValueException {
    int j = _asroles.indexOf( ( (Object) (UID.getUIDEntry(i))));
    if (j == -1) {
      return -1;
    }
    else {
      return ( (Integer) _scuroles.elementAt(j)).intValue();
    }
  }

  public int getScpRole(int i) throws IllegalValueException {
    int j = _asroles.indexOf( ( (Object) (UID.getUIDEntry(i))));
    if (j == -1) {
      return -1;
    }
    else {
      return ( (Integer) _scproles.elementAt(j)).intValue();
    }
  }

  public int getAbstractSyntaxes() {
    return _abstractsyntaxes.size();
  }

  public UIDEntry getAbstractSyntax(int i) {
    return (UIDEntry) _abstractsyntaxes.elementAt(i);
  }

  public int getTransferSyntaxes(int i) {
    return ( (Vector) _transfersyntaxes.elementAt(i)).size();
  }

  public UIDEntry getTransferSyntax(int i, int j) {
    return (UIDEntry) ( (Vector) _transfersyntaxes.elementAt(i)).elementAt(j);
  }

  public void addPresentationContext(int i, int ai[]) throws
      IllegalValueException {
    UIDEntry uidentry = UID.getUIDEntry(i);
    UIDEntry auidentry[] = new UIDEntry[ai.length];
    for (int j = 0; j < ai.length; j++) {
      auidentry[j] = UID.getUIDEntry(ai[j]);

    }
    int k = _abstractsyntaxes.indexOf( ( (Object) (uidentry)));
    Vector vector;
    if (k == -1) {
      _abstractsyntaxes.addElement( ( (Object) (uidentry)));
      _transfersyntaxes.addElement( ( (Object) (vector = new Vector())));
    }
    else {
      vector = (Vector) _transfersyntaxes.elementAt(k);
    }
    for (int l = 0; l < auidentry.length; l++) {
      vector.addElement( ( (Object) (auidentry[l])));

    }
  }

  public void setScuScpRoleSelection(int i, int j, int k) throws
      IllegalValueException {
    UIDEntry uidentry = UID.getUIDEntry(i);
    int l = _asroles.indexOf( ( (Object) (uidentry)));
    if (l != -1) {
      _scuroles.setElementAt( ( (Object) (new Integer(j))), l);
      _scproles.setElementAt( ( (Object) (new Integer(k))), l);
    }
    else {
      _asroles.addElement( ( (Object) (uidentry)));
      _scuroles.addElement( ( (Object) (new Integer(j))));
      _scproles.addElement( ( (Object) (new Integer(k))));
    }
  }

  public void setPolicyForCallingTitle(String s,
                                       AdvancedAcceptancePolicy advancedacceptancepolicy) {
    _aetSpecificPolicy.put( ( (Object) (s)),
                           ( (Object) (advancedacceptancepolicy)));
  }

  public AdvancedAcceptancePolicy removePolicyForCallingTitle(String s) {
    return (AdvancedAcceptancePolicy) _aetSpecificPolicy.remove( ( (Object) (s)));
  }

  public void setIpCheckForCallingTitle(String s, InetAddress inetaddress) {
    _ipcheck.put( ( (Object) (s)), ( (Object) (inetaddress)));
  }

  public InetAddress removeIpCheckForCallingTitle(String s) {
    return (InetAddress) _ipcheck.remove( ( (Object) (s)));
  }

  public void removeIpChecks() {
    _ipcheck.clear();
  }

  public Response prepareResponse(VerboseAssociation verboseassociation,
                                  Request request) throws IllegalValueException {
    if (request.isPrivateApplicationContext()) {
      return ( (Response) (APPLICATIONCONTEXTNAME_NOT_SUPPORTED));
    }
    if (_calledtitles != null &&
        _calledtitles.indexOf( ( (Object) (request.getCalledTitle()))) == -1) {
      return ( (Response) (CALLED_AETITLE_NOT_RECOGNIZED));
    }
    String s = request.getCallingTitle();
    InetAddress inetaddress = (InetAddress) _ipcheck.get( ( (Object) (s)));
    if (_callingtitles != null &&
        _callingtitles.indexOf( ( (Object) (s))) == -1 ||
        inetaddress != null &&
        !
        inetaddress.equals( ( (Object) (verboseassociation.socket().getInetAddress())))) {
      return ( (Response) (CALLING_AETITLE_NOT_RECOGNIZED));
    }
    AdvancedAcceptancePolicy advancedacceptancepolicy = (
        AdvancedAcceptancePolicy) _aetSpecificPolicy.get( ( (Object) (s)));
    if (advancedacceptancepolicy == null) {
      advancedacceptancepolicy = this;
    }
    Acknowledge acknowledge = new Acknowledge();
    acknowledge.setCalledTitle(request.getCalledTitle());
    acknowledge.setCallingTitle(request.getCallingTitle());
    acknowledge.setMaxPduSize(_maxpdusize);
    return ( (advancedacceptancepolicy.negotiate(request, acknowledge) <= 0 ?
              (Response) NO_REASON_GIVEN : (Response) acknowledge));
  }

  private int negotiate(Request request, Acknowledge acknowledge) throws
      IllegalValueException {
    int i = 0;
    int j = request.getPresentationContexts();
    for (int k = 0; k < j; k++) {
      if (negotiateTransferSyntax(request, acknowledge, k)) {
        i++;

      }
    }
    if (i == 0) {
      return 0;
    }
    else {
      negotiateAsyncOperations(request, acknowledge);
      return i;
    }
  }

  private boolean negotiateTransferSyntax(Request request,
                                          Acknowledge acknowledge, int i) throws
      IllegalValueException {
    UIDEntry uidentry = request.getAbstractSyntax(i);
    int j = _abstractsyntaxes.indexOf( ( (Object) (uidentry)));
    if (j == -1) {
      acknowledge.addPresentationContext(request.getPresentationContextID(i), 3,
                                         8193);
      return false;
    }
    int k = request.getScuRole(uidentry);
    int l = request.getScpRole(uidentry);
    if (k != -1) {
      int i1 = 1;
      int j1 = 0;
      int l1 = _asroles.indexOf( ( (Object) (uidentry)));
      if (l1 != -1) {
        i1 = ( (Integer) _scuroles.elementAt(l1)).intValue();
        j1 = ( (Integer) _scproles.elementAt(l1)).intValue();
      }
      i1 &= k;
      j1 &= l;
      if (i1 == 0 && j1 == 0) {
        acknowledge.addPresentationContext(request.getPresentationContextID(i),
                                           3, 8193);
        return false;
      }
      acknowledge.setScuScpRoleSelection(uidentry.getConstant(), i1, j1);
    }
    int k1 = request.getTransferSyntaxes(i);
    Vector vector = (Vector) _transfersyntaxes.elementAt(j);
    int i2 = vector.size();
    for (int j2 = 0; j2 < i2; j2++) {
      for (int k2 = 0; k2 < k1; k2++) {
        UIDEntry uidentry1 = (UIDEntry) vector.elementAt(j2);
        if (uidentry1.equals( ( (Object) (request.getTransferSyntax(i, k2))))) {
          acknowledge.addPresentationContext(request.getPresentationContextID(i),
                                             0, uidentry1.getConstant());
          return true;
        }
      }

    }

    acknowledge.addPresentationContext(request.getPresentationContextID(i), 4,
                                       8193);
    return false;
  }

  private void negotiateAsyncOperations(Request request,
                                        Acknowledge acknowledge) {
    int i = request.getMaxOperationsInvoked();
    int j = request.getMaxOperationsPerformed();
    acknowledge.setMaxOperationsInvoked(i != 0 &&
                                        (_maxopinvoked == 0 ||
                                         _maxopinvoked >= i) ? i :
                                        _maxopinvoked);
    acknowledge.setMaxOperationsPerformed(j != 0 &&
                                          (_maxopperformed == 0 ||
                                           _maxopperformed >= j) ? j :
                                          _maxopperformed);
  }

  static {
    APPLICATIONCONTEXTNAME_NOT_SUPPORTED = new Reject(Reject.REJECTED_PERMANENT,
        Reject.DICOM_UL_SERVICE_USER,
        Reject.USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED);
    CALLED_AETITLE_NOT_RECOGNIZED = new Reject(Reject.REJECTED_PERMANENT,
                                               Reject.DICOM_UL_SERVICE_USER,
                                               Reject.
        USER_CALLED_AETITLE_NOT_RECOGNIZED);
    CALLING_AETITLE_NOT_RECOGNIZED = new Reject(Reject.REJECTED_PERMANENT,
                                                Reject.DICOM_UL_SERVICE_USER,
                                                Reject.
        USER_CALLING_AETITLE_NOT_RECOGNIZED);
    NO_REASON_GIVEN = new Reject(Reject.REJECTED_PERMANENT,
                                 Reject.DICOM_UL_SERVICE_USER,
                                 Reject.USER_NO_REASON_GIVEN);
  }
}
