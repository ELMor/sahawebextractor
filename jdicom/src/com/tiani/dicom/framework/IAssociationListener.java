// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IAssociationListener.java

package com.tiani.dicom.framework;

import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

// Referenced classes of package com.tiani.dicom.framework:
//            VerboseAssociation

public interface IAssociationListener
{

    public abstract void associateRequestReceived(VerboseAssociation verboseassociation, Request request);

    public abstract void associateResponseReceived(VerboseAssociation verboseassociation, Response response);

    public abstract void associateRequestSent(VerboseAssociation verboseassociation, Request request);

    public abstract void associateResponseSent(VerboseAssociation verboseassociation, Response response);

    public abstract void releaseRequestReceived(VerboseAssociation verboseassociation);

    public abstract void releaseResponseReceived(VerboseAssociation verboseassociation);

    public abstract void releaseRequestSent(VerboseAssociation verboseassociation);

    public abstract void releaseResponseSent(VerboseAssociation verboseassociation);

    public abstract void abortReceived(VerboseAssociation verboseassociation, Abort abort);

    public abstract void abortSent(VerboseAssociation verboseassociation, int i, int j);

    public abstract void socketClosed(VerboseAssociation verboseassociation);
}
