// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefAcceptorListener.java

package com.tiani.dicom.framework;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;
import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package com.tiani.dicom.framework:
//            DimseExchange, IAcceptorListener, VerboseAssociation, DimseRqManager

public class DefAcceptorListener
    implements IAcceptorListener
{

    private boolean _startThread;
    private int _artim;
    private DimseRqManager _rqManager;
    private boolean _queueRQ;
    private boolean _queueRSP;

    public DefAcceptorListener(boolean flag, DimseRqManager dimserqmanager, boolean flag1, boolean flag2)
    {
        _startThread = flag;
        _rqManager = dimserqmanager;
        _queueRQ = flag1;
        _queueRSP = flag2;
    }

    public void setARTIM(int i)
    {
        _artim = i;
    }

    public void setStartThread(boolean flag)
    {
        _startThread = flag;
    }

    public boolean isStartThread()
    {
        return _startThread;
    }

    public void setRqManager(DimseRqManager dimserqmanager)
    {
        _rqManager = dimserqmanager;
    }

    public DimseRqManager getRqManager()
    {
        return _rqManager;
    }

    public void setQueueRQ(boolean flag)
    {
        _queueRQ = flag;
    }

    public boolean isQueueRQ()
    {
        return _queueRQ;
    }

    public void setQueueRSP(boolean flag)
    {
        _queueRSP = flag;
    }

    public boolean isQueueRSP()
    {
        return _queueRSP;
    }

    public void handle(Acknowledge acknowledge, VerboseAssociation verboseassociation)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        DimseExchange dimseexchange = new DimseExchange(verboseassociation, _rqManager, _queueRQ, _queueRSP, acknowledge.getMaxOperationsPerformed());
        dimseexchange.setARTIM(_artim);
        if(_startThread)
        {
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: " + verboseassociation.remoteAET() + " Start new Thread for DimseExchange.run()");
            Thread thread = new Thread(((Runnable) (dimseexchange)));
            thread.start();
        } else
        {
            dimseexchange.run();
        }
    }
}
