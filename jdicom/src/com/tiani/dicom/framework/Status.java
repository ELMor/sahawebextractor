// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Status.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import java.util.Hashtable;

// Referenced classes of package com.tiani.dicom.framework:
//            StatusEntry

public class Status
{

    private static Hashtable defaultTab;
    private static Hashtable specificTabs;

    public Status()
    {
    }

    private static int maskBits(int i)
    {
        int j;
        switch(j = i & 0xff00)
        {
        case 42752: 
        case 43264: 
            return j;
        }
        switch(j = i & 0xf000)
        {
        case 49152: 
            return j;
        }
        return 0;
    }

    public static boolean isPending(int i)
    {
        return i == 65280 || i == 65281;
    }

    public static StatusEntry getStatusEntry(int i)
    {
        StatusEntry statusentry = getStatusEntry(defaultTab, i);
        if(statusentry == null)
            statusentry = new StatusEntry(i, 0, "?");
        return statusentry;
    }

    private static StatusEntry getStatusEntry(Hashtable hashtable, int i)
    {
        StatusEntry statusentry = (StatusEntry)hashtable.get(((Object) (new Integer(i))));
        if(statusentry == null && (i = maskBits(i)) != 0)
            statusentry = (StatusEntry)hashtable.get(((Object) (new Integer(i))));
        return statusentry;
    }

    public static StatusEntry getStatusEntry(int i, int j)
        throws DicomException
    {
        StatusEntry statusentry = null;
        Hashtable hashtable = (Hashtable)specificTabs.get(((Object) (new Integer(i))));
        if(hashtable != null)
            statusentry = getStatusEntry(hashtable, j);
        if(statusentry == null)
            statusentry = getStatusEntry(j);
        return statusentry;
    }

    public static String toString(int i)
    {
        String s = Integer.toHexString(i);
        return "status #" + "0000".substring(s.length()) + s + "H";
    }

    static 
    {
        defaultTab = new Hashtable();
        specificTabs = new Hashtable();
        defaultTab.put(((Object) (new Integer(0))), ((Object) (new StatusEntry(0, 1, "Success"))));
        defaultTab.put(((Object) (new Integer(1))), ((Object) (new StatusEntry(1, 1, "Success: Partial Study Content exists on system supporting SCP"))));
        defaultTab.put(((Object) (new Integer(2))), ((Object) (new StatusEntry(2, 1, "Success: None of the Study Content exists on system supporting SCP"))));
        defaultTab.put(((Object) (new Integer(3))), ((Object) (new StatusEntry(3, 1, "Success: It is StatusEntry.UNKNOWN whether or not study content exists on system supporting SCP"))));
        defaultTab.put(((Object) (new Integer(261))), ((Object) (new StatusEntry(261, 5, "Failure: No such attribute"))));
        defaultTab.put(((Object) (new Integer(262))), ((Object) (new StatusEntry(262, 5, "Failure: Invalid attribute value"))));
        defaultTab.put(((Object) (new Integer(263))), ((Object) (new StatusEntry(263, 4, "Warning: Attribute list error"))));
        defaultTab.put(((Object) (new Integer(272))), ((Object) (new StatusEntry(272, 5, "Failure: Processing StatusEntry.FAILURE"))));
        defaultTab.put(((Object) (new Integer(273))), ((Object) (new StatusEntry(273, 5, "Failure: Duplicate SOP instance"))));
        defaultTab.put(((Object) (new Integer(274))), ((Object) (new StatusEntry(274, 5, "Failure: No such object instance"))));
        defaultTab.put(((Object) (new Integer(275))), ((Object) (new StatusEntry(275, 5, "Failure: No such event type"))));
        defaultTab.put(((Object) (new Integer(276))), ((Object) (new StatusEntry(276, 5, "Failure: No such argument"))));
        defaultTab.put(((Object) (new Integer(277))), ((Object) (new StatusEntry(277, 5, "Failure: Invalid argument value"))));
        defaultTab.put(((Object) (new Integer(278))), ((Object) (new StatusEntry(278, 4, "Warning: Attribute Value Out of Range"))));
        defaultTab.put(((Object) (new Integer(279))), ((Object) (new StatusEntry(279, 5, "Failure: Invalid object instance"))));
        defaultTab.put(((Object) (new Integer(280))), ((Object) (new StatusEntry(280, 5, "Failure: No such SOP class"))));
        defaultTab.put(((Object) (new Integer(281))), ((Object) (new StatusEntry(281, 5, "Failure: Class-instance conflict"))));
        defaultTab.put(((Object) (new Integer(288))), ((Object) (new StatusEntry(288, 5, "Failure: Missing attribute"))));
        defaultTab.put(((Object) (new Integer(289))), ((Object) (new StatusEntry(289, 5, "Failure: Missing attribute value"))));
        defaultTab.put(((Object) (new Integer(290))), ((Object) (new StatusEntry(290, 5, "Refused: SOP class not supported"))));
        defaultTab.put(((Object) (new Integer(528))), ((Object) (new StatusEntry(528, 5, "Failure: Duplicate invocation"))));
        defaultTab.put(((Object) (new Integer(529))), ((Object) (new StatusEntry(529, 5, "Failure: Unrecognized operation"))));
        defaultTab.put(((Object) (new Integer(530))), ((Object) (new StatusEntry(530, 5, "Failure: Mistyped argument"))));
        defaultTab.put(((Object) (new Integer(531))), ((Object) (new StatusEntry(531, 5, "Failure: Resource limitation"))));
        defaultTab.put(((Object) (new Integer(42752))), ((Object) (new StatusEntry(42752, 5, "Refused: Out of Resources"))));
        defaultTab.put(((Object) (new Integer(43264))), ((Object) (new StatusEntry(43264, 5, "Failure: Data Set does not match SOP Class"))));
        defaultTab.put(((Object) (new Integer(45056))), ((Object) (new StatusEntry(45056, 4, "Warning: Coercion of Data Elements"))));
        defaultTab.put(((Object) (new Integer(45062))), ((Object) (new StatusEntry(45062, 4, "Warning: Elements Discarded"))));
        defaultTab.put(((Object) (new Integer(45063))), ((Object) (new StatusEntry(45063, 4, "Warning: Data Set does not match SOP Class"))));
        defaultTab.put(((Object) (new Integer(46592))), ((Object) (new StatusEntry(46592, 4, "Warning: Memory allocation not supported"))));
        defaultTab.put(((Object) (new Integer(46593))), ((Object) (new StatusEntry(46593, 4, "Warning: Film session printing (collation) is not supported"))));
        defaultTab.put(((Object) (new Integer(46594))), ((Object) (new StatusEntry(46594, 4, "Warning: Film Session SOP Instance hierarchy does not contain Image Box SOP Instances (empty page)"))));
        defaultTab.put(((Object) (new Integer(46595))), ((Object) (new StatusEntry(46595, 4, "Warning: Film Box SOP Instance hierarchy does not contain Image Box SOP Instances (empty page)"))));
        defaultTab.put(((Object) (new Integer(46596))), ((Object) (new StatusEntry(46596, 4, "Warning: Image size is larger than image box size, the image has been demagnified"))));
        defaultTab.put(((Object) (new Integer(46597))), ((Object) (new StatusEntry(46597, 4, "Warning: Requested Min Density or Max Density outside of printers operating range"))));
        defaultTab.put(((Object) (new Integer(46601))), ((Object) (new StatusEntry(46601, 4, "Warning: Image size is larger than the Image Box size. The Image has been cropped to fit"))));
        defaultTab.put(((Object) (new Integer(46602))), ((Object) (new StatusEntry(46602, 4, "Warning: Image size is larger than the Image Box size. Image or Combined Print Image has been decimated to fit"))));
        defaultTab.put(((Object) (new Integer(50688))), ((Object) (new StatusEntry(50688, 5, "Failure: Film Session SOP Instance hierarchy does not contain Film Box SOP Instances"))));
        defaultTab.put(((Object) (new Integer(50689))), ((Object) (new StatusEntry(50689, 5, "Failure: Unable to create Print Job SOP Instance; print queue is full"))));
        defaultTab.put(((Object) (new Integer(50690))), ((Object) (new StatusEntry(50690, 5, "Failure: Unable to create Print Job SOP Instance; print queue is full"))));
        defaultTab.put(((Object) (new Integer(50691))), ((Object) (new StatusEntry(50691, 5, "Failure: Image size is larger than image box size"))));
        defaultTab.put(((Object) (new Integer(49152))), ((Object) (new StatusEntry(49152, 5, "Failure: Cannot understand"))));
        defaultTab.put(((Object) (new Integer(50693))), ((Object) (new StatusEntry(50693, 5, "Failure: Insufficient memory in printer to store the image"))));
        defaultTab.put(((Object) (new Integer(50707))), ((Object) (new StatusEntry(50707, 5, "Failure: Combined Print Image size is larger than the Image Box size"))));
        defaultTab.put(((Object) (new Integer(50710))), ((Object) (new StatusEntry(50710, 5, "Failure: There is an existing Film Box that has not been printed and N-ACTION at the Film Session level is not supported"))));
        defaultTab.put(((Object) (new Integer(65024))), ((Object) (new StatusEntry(65024, 3, "StatusEntry.CANCEL"))));
        defaultTab.put(((Object) (new Integer(65280))), ((Object) (new StatusEntry(65280, 2, "StatusEntry.PENDING"))));
        defaultTab.put(((Object) (new Integer(65281))), ((Object) (new StatusEntry(65281, 2, "StatusEntry.PENDING - one or more Optional Keys were not supported"))));
        Hashtable hashtable = new Hashtable(11);
        hashtable.put(((Object) (new Integer(42753))), ((Object) (new StatusEntry(42753, 5, "Refused: Out of Resources - Unable to calculate number of matches"))));
        hashtable.put(((Object) (new Integer(42754))), ((Object) (new StatusEntry(42754, 5, "Refused: Out of Resources - Unable to perform sub-operations"))));
        hashtable.put(((Object) (new Integer(43009))), ((Object) (new StatusEntry(43009, 5, "Refused: Move Destination StatusEntry.UNKNOWN"))));
        hashtable.put(((Object) (new Integer(43264))), ((Object) (new StatusEntry(43264, 5, "Failure: Identifier does not match SOP Class"))));
        hashtable.put(((Object) (new Integer(45056))), ((Object) (new StatusEntry(45056, 4, "Warning: Sub-operations Complete - One or more Failures"))));
        hashtable.put(((Object) (new Integer(49152))), ((Object) (new StatusEntry(49152, 5, "Failure: Unable to process"))));
        specificTabs.put(((Object) (new Integer(4132))), ((Object) (hashtable)));
        specificTabs.put(((Object) (new Integer(4133))), ((Object) (hashtable)));
        specificTabs.put(((Object) (new Integer(4134))), ((Object) (hashtable)));
        specificTabs.put(((Object) (new Integer(4135))), ((Object) (hashtable)));
        specificTabs.put(((Object) (new Integer(4136))), ((Object) (hashtable)));
        specificTabs.put(((Object) (new Integer(4137))), ((Object) (hashtable)));
        specificTabs.put(((Object) (new Integer(4138))), ((Object) (hashtable)));
        specificTabs.put(((Object) (new Integer(4139))), ((Object) (hashtable)));
        specificTabs.put(((Object) (new Integer(4140))), ((Object) (hashtable)));
        Hashtable hashtable1 = new Hashtable(31);
        hashtable1.put(((Object) (new Integer(46596))), ((Object) (new StatusEntry(46596, 4, "Warning: Annotation Box not supported, image printed without annotation"))));
        hashtable1.put(((Object) (new Integer(46597))), ((Object) (new StatusEntry(46597, 4, "Warning: Image Overlay Box not supported, image printed without overlay"))));
        hashtable1.put(((Object) (new Integer(46598))), ((Object) (new StatusEntry(46598, 4, "Warning: Presentation LUT not supported, image printed without applying any Presentation LUT"))));
        hashtable1.put(((Object) (new Integer(46600))), ((Object) (new StatusEntry(46600, 4, "Warning: Presentation LUT not supported at Image Box level, image printed with Film Box Presentation LUT"))));
        hashtable1.put(((Object) (new Integer(50688))), ((Object) (new StatusEntry(50688, 5, "Failure: Stored Print Storage SOP Instance does not contain Film Boxes"))));
        hashtable1.put(((Object) (new Integer(50695))), ((Object) (new StatusEntry(50695, 5, "Failure: Stored Print Storage SOP Instance not available from Retrieve AE"))));
        hashtable1.put(((Object) (new Integer(50696))), ((Object) (new StatusEntry(50696, 5, "Failure: Image SOP Instance not available from Retrieve AE"))));
        hashtable1.put(((Object) (new Integer(50697))), ((Object) (new StatusEntry(50697, 5, "Failure: StatusEntry.FAILURE in retrieving Stored Print Storage SOP Instance"))));
        hashtable1.put(((Object) (new Integer(50698))), ((Object) (new StatusEntry(50698, 5, "Failure: StatusEntry.FAILURE in retrieving Image SOP Instance"))));
        hashtable1.put(((Object) (new Integer(50699))), ((Object) (new StatusEntry(50699, 5, "Failure: StatusEntry.UNKNOWN Retrieve AE title"))));
        hashtable1.put(((Object) (new Integer(50700))), ((Object) (new StatusEntry(50700, 5, "Failure: Print request rejected because printer cannot handle color image"))));
        hashtable1.put(((Object) (new Integer(50701))), ((Object) (new StatusEntry(50701, 5, "Failure: Stored Print Storage SOP Instance does not contain Image Boxes (empty page)"))));
        hashtable1.put(((Object) (new Integer(50702))), ((Object) (new StatusEntry(50702, 5, "Failure: Annotation Box not supported"))));
        hashtable1.put(((Object) (new Integer(50703))), ((Object) (new StatusEntry(50703, 5, "Failure: Image Overlay Box not supported"))));
        hashtable1.put(((Object) (new Integer(50704))), ((Object) (new StatusEntry(50704, 5, "Failure: Presentation LUT not supported"))));
        hashtable1.put(((Object) (new Integer(50707))), ((Object) (new StatusEntry(50707, 5, "Failure: Combined Print Image size is larger than the Image Box size"))));
        hashtable1.put(((Object) (new Integer(50708))), ((Object) (new StatusEntry(50708, 5, "Failure: Presentation LUT not supported at Image Box level"))));
        hashtable1.put(((Object) (new Integer(50709))), ((Object) (new StatusEntry(50709, 5, "Failure: Unable to establish an Association with the Retrieve AE"))));
        specificTabs.put(((Object) (new Integer(4150))), ((Object) (hashtable1)));
        Hashtable hashtable2 = new Hashtable(3);
        hashtable2.put(((Object) (new Integer(50710))), ((Object) (new StatusEntry(50710, 5, "Failure: Combined Print Image requires cropping to fit Image Box. This is not supported in this SOP Class"))));
        specificTabs.put(((Object) (new Integer(4160))), ((Object) (hashtable2)));
    }
}
