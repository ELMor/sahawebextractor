// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefCEchoSCP.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.IOException;

// Referenced classes of package com.tiani.dicom.framework:
//            DicomMessage, IDimseRqListener, DimseExchange, VerboseAssociation

public class DefCEchoSCP
    implements IDimseRqListener
{

    private static IDimseRqListener _instance = new DefCEchoSCP();

    public DefCEchoSCP()
    {
    }

    public void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        DicomMessage dicommessage1 = new DicomMessage(dicommessage.getPresentationContext(), dicommessage.getAbstractSyntax(), 32816, i, ((com.archimed.dicom.DicomObject) (null)));
        dicommessage1.status(0);
        dimseexchange.getAssociation().sendMessage(dicommessage1);
    }

    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        throw new DicomException("Error: cancel request for C-ECHO service");
    }

    public static IDimseRqListener getInstance()
    {
        return _instance;
    }

}
