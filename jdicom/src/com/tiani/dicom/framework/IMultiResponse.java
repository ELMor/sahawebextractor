// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IMultiResponse.java

package com.tiani.dicom.framework;


// Referenced classes of package com.tiani.dicom.framework:
//            DimseExchange, DicomMessage

public interface IMultiResponse
{

    public abstract int nextResponse(DimseExchange dimseexchange, DicomMessage dicommessage);

    public abstract void cancel();
}
