// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Requestor.java

package com.tiani.dicom.framework;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.framework:
//            VerboseAssociation, IAssociationListener

public class Requestor
{

    private Socket _socket;
    private Request _request;
    private Response _response;
    private Vector _associationListeners;

    public Requestor(Socket socket, Request request)
    {
        _associationListeners = new Vector();
        _socket = socket;
        _request = request;
        _response = null;
    }

    public void addAssociationListener(IAssociationListener iassociationlistener)
    {
        _associationListeners.addElement(((Object) (iassociationlistener)));
    }

    public VerboseAssociation openAssoc()
        throws UnknownHostException, IOException, UnknownUIDException, IllegalValueException
    {
        VerboseAssociation verboseassociation = new VerboseAssociation(_socket);
        for(Enumeration enumeration = _associationListeners.elements(); enumeration.hasMoreElements(); verboseassociation.addAssociationListener((IAssociationListener)enumeration.nextElement()));
        verboseassociation.sendAssociateRequest(_request);
        _response = verboseassociation.receiveAssociateResponse();
        if(!(_response instanceof Acknowledge))
            return null;
        else
            return verboseassociation;
    }

    public final Response response()
    {
        return _response;
    }
}
