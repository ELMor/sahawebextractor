// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IDimseRspListener.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.IOException;

// Referenced classes of package com.tiani.dicom.framework:
//            DimseExchange, DicomMessage

public interface IDimseRspListener
{

    public abstract void handleRSP(DimseExchange dimseexchange, int i, int j, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException;
}
