// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IAcceptancePolicy.java

package com.tiani.dicom.framework;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

// Referenced classes of package com.tiani.dicom.framework:
//            VerboseAssociation

public interface IAcceptancePolicy
{

    public abstract Response prepareResponse(VerboseAssociation verboseassociation, Request request)
        throws IllegalValueException;
}
