// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefCMoveSCP.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.IOException;
import java.util.Hashtable;

// Referenced classes of package com.tiani.dicom.framework:
//            DicomMessage, IMultiResponse, SingleResponse, IDimseRqListener, 
//            DimseExchange, VerboseAssociation

public class DefCMoveSCP
    implements IDimseRqListener
{

    private Hashtable register;

    public DefCMoveSCP()
    {
        register = new Hashtable();
    }

    public void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        IMultiResponse imultiresponse = query(dimseexchange, i, s, dicommessage);
        Integer integer = new Integer(i);
        register.put(((Object) (integer)), ((Object) (imultiresponse)));
        int j;
        try
        {
            do
            {
                DicomMessage dicommessage1 = new DicomMessage(dicommessage.getPresentationContext(), dicommessage.getAbstractSyntax(), 32801, i, ((com.archimed.dicom.DicomObject) (null)));
                j = imultiresponse.nextResponse(dimseexchange, dicommessage1);
                dicommessage1.affectedSOPclassUID(s);
                dicommessage1.status(j);
                dimseexchange.getAssociation().sendMessage(dicommessage1);
            } while(j == 65280 || j == 65281);
        }
        finally
        {
            register.remove(((Object) (integer)));
        }
    }

    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        IMultiResponse imultiresponse = (IMultiResponse)register.get(((Object) (new Integer(i))));
        if(imultiresponse == null)
        {
            return false;
        } else
        {
            imultiresponse.cancel();
            return true;
        }
    }

    protected IMultiResponse query(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
    {
        return ((IMultiResponse) (new SingleResponse(0)));
    }
}
