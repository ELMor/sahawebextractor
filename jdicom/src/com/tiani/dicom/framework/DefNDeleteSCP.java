// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefNDeleteSCP.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.IOException;

// Referenced classes of package com.tiani.dicom.framework:
//            DicomMessage, IDimseRqListener, DimseExchange, VerboseAssociation

public class DefNDeleteSCP
    implements IDimseRqListener
{

    public DefNDeleteSCP()
    {
    }

    public void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        String s1 = ((DicomObject) (dicommessage)).getS(14);
        DicomMessage dicommessage1 = new DicomMessage(dicommessage.getPresentationContext(), dicommessage.getAbstractSyntax(), 33104, i, ((DicomObject) (null)));
        dicommessage1.affectedSOP(s, s1);
        dicommessage1.status(delete(dimseexchange, s, s1, dicommessage, dicommessage1));
        dimseexchange.getAssociation().sendMessage(dicommessage1);
    }

    protected int delete(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
        throws DicomException
    {
        return 0;
    }

    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
        throws DicomException
    {
        throw new DicomException("Error: cancel request for N-DELETE service");
    }
}
