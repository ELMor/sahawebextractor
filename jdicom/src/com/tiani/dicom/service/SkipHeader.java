// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SkipHeader.java

package com.tiani.dicom.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;

public final class SkipHeader extends DataInputStream
{

    private static byte MAGI[] = {
        77, 65, 71, 73
    };
    private static byte DICM[] = {
        68, 73, 67, 77
    };
    private static byte GR0008[] = {
        8, 0, 0, 0
    };
    private static final String TS_LE = "1.2.840.10008.1.2";
    private String ts;
    private String mgCompress;
    private byte b4[];
    public static boolean skipGroups = false;
    private InputStream myIn;

    public SkipHeader(InputStream inputstream)
    {
        super(inputstream);
        ts = "1.2.840.10008.1.2";
        mgCompress = null;
        b4 = new byte[4];
        myIn = inputstream;
    }

    public InputStream getUnderlyingStream()
    {
        return myIn;
    }

    public String getTransferSyntax()
    {
        return ts;
    }

    public String getMgCompress()
    {
        return mgCompress;
    }

    public static void main(String args[])
    {
        try
        {
            BufferedInputStream bufferedinputstream = new BufferedInputStream(((InputStream) (new FileInputStream(args[0]))));
            try
            {
                SkipHeader skipheader = new SkipHeader(((InputStream) (bufferedinputstream)));
                int i = skipheader.seekPixelData();
                System.out.println("instance.seekPixelData() returns " + i);
                System.out.println("instance.getTransferSyntax() returns " + skipheader.getTransferSyntax());
                System.out.println("instance.getMgCompress() returns " + skipheader.getMgCompress());
                if(args.length > 1)
                {
                    System.out.println("write retaining file date to " + args[1]);
                    BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(((OutputStream) (new FileOutputStream(args[1]))));
                    int j;
                    try
                    {
                        while((j = ((InputStream) (bufferedinputstream)).read()) != -1) 
                            ((OutputStream) (bufferedoutputstream)).write(j);
                    }
                    finally
                    {
                        ((InputStream) (bufferedinputstream)).close();
                    }
                }
            }
            finally
            {
                ((InputStream) (bufferedinputstream)).close();
            }
        }
        catch(Exception exception)
        {
            ((Throwable) (exception)).printStackTrace();
        }
    }

    private void skipFully(int i)
        throws IOException
    {
        int k;
        for(int j = i; j > 0; j -= k)
            if((k = ((DataInputStream)this).skipBytes(i)) == 0)
                throw new EOFException();

    }

    private int readVRLen()
        throws IOException
    {
        int i = readLen();
        switch(i & 0xffff)
        {
        case 16975: 
        case 20053: 
        case 20819: 
        case 21589: 
        case 22351: 
            return readLen();
        }
        return i >> 16;
    }

    private int readTag()
        throws IOException
    {
        ((DataInputStream)this).readFully(b4);
        return ((b4[0] & 0xff) << 16) + ((b4[1] & 0xff) << 24) + ((b4[2] & 0xff) << 0) + ((b4[3] & 0xff) << 8);
    }

    private int readLen()
        throws IOException
    {
        ((DataInputStream)this).readFully(b4);
        return ((b4[0] & 0xff) << 0) + ((b4[1] & 0xff) << 8) + ((b4[2] & 0xff) << 16) + ((b4[3] & 0xff) << 24);
    }

    private boolean isExplicit(int i)
    {
        return ts != "1.2.840.10008.1.2" || (i & 0xffff0000) == 0x20000;
    }

    private void skipAsciiHeader()
        throws IOException
    {
        String s;
        while(!"ENDINFO".equals(((Object) (s = ((DataInputStream)this).readLine())))) 
            if(s.startsWith("COMPRESSION"))
                mgCompress = s.substring(12);
    }

    private void skipFileMetaInfo()
        throws IOException
    {
        ((DataInputStream)this).skipBytes(124);
        ((DataInputStream)this).readFully(b4, 0, 4);
        if(!Arrays.equals(b4, DICM))
            throw new IOException("DICOM Format error!");
        ((DataInputStream)this).skipBytes(12);
        parseUntil(0x20010);
        int i = readVRLen();
        byte abyte0[] = new byte[i];
        ((DataInputStream)this).readFully(abyte0, 0, i);
        if(abyte0[i - 1] == 0)
            i--;
        String s = new String(abyte0, 0, 0, i);
        if(!"1.2.840.10008.1.2".equals(((Object) (s))))
            ts = s;
    }

    public int seekPixelData_skipAsciiHeader()
        throws IOException
    {
        parseUntil(0x7fe00010);
        if(ts != "1.2.840.10008.1.2")
            ((DataInputStream)this).readFully(b4);
        return readLen();
    }

    public int seekPixelData()
        throws IOException
    {
        ((DataInputStream)this).readFully(b4, 0, 4);
        if(Arrays.equals(b4, MAGI))
            skipAsciiHeader();
        else
        if(Arrays.equals(b4, GR0008))
        {
            ((DataInputStream)this).readFully(b4, 0, 4);
            ((DataInputStream)this).skipBytes(readLen());
        } else
        {
            skipFileMetaInfo();
        }
        parseUntil(0x7fe00010);
        if(ts != "1.2.840.10008.1.2")
            ((DataInputStream)this).readFully(b4);
        return readLen();
    }

    private void parseUntil(int i)
        throws IOException
    {
        int j;
        while((j = readTag()) != i) 
        {
            int k = isExplicit(j) ? readVRLen() : readLen();
            if(skipGroups && (j & 0xffff) == 0 && j != 0x7fe00000)
            {
                if(k != 4)
                    throw new IOException("DICOM Format error!");
                ((DataInputStream)this).skipBytes(readLen());
            } else
            if(k != -1)
                ((DataInputStream)this).skipBytes(k);
            else
                parseSQ();
        }
    }

    private void parseSQ()
        throws IOException
    {
        int i;
        while((i = readTag()) == 0xfffee000) 
        {
            int j = readLen();
            if(j != -1)
            {
                ((DataInputStream)this).skipBytes(j);
            } else
            {
                parseUntil(0xfffee00d);
                if(readLen() != 0)
                    throw new IOException("DICOM Format error!");
            }
        }
        if(i != 0xfffee0dd || readLen() != 0)
            throw new IOException("DICOM Format error!");
        else
            return;
    }

}
