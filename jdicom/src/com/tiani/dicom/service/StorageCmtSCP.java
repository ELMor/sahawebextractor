// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StorageCmtSCP.java

package com.tiani.dicom.service;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Request;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.StatusEntry;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Hashtable;

// Referenced classes of package com.tiani.dicom.service:
//            StorageCmtConstants

public class StorageCmtSCP
    implements IDimseRqListener, StorageCmtConstants
{
    private class Context
    {

        DicomObject actionInfo;
        DicomObject result;
        Hashtable cookie;
        AET aet;
        int status;
        String remoteAET;
        String localAET;

        void postProcess(DimseExchange dimseexchange)
        {
            if(Status.getStatusEntry(status).getType() == 5)
                return;
            if(timing == 11)
                status = strategy.commit(dimseexchange, actionInfo, ((DicomMessage) (null)), cookie, result);
            if(assocMode != 20)
                try
                {
                    sendResult(dimseexchange);
                    return;
                }
                catch(Exception exception)
                {
                    if(assocMode == 21)
                    {
                        Debug.out.println("jdicom: ERROR: reuse of association failed: " + ((Throwable) (exception)).getMessage());
                        return;
                    }
                    if(Debug.DEBUG > 0)
                    {
                        Debug.out.println("jdicom: WARNING: reuse of association failed: " + ((Throwable) (exception)).getMessage());
                        Debug.out.println("jdicom: -> initiate separate association to send commitment result");
                    }
                }
            try
            {
                DimseExchange dimseexchange1 = connect();
                sendResult(dimseexchange1);
                dimseexchange1.releaseAssoc();
            }
            catch(Exception exception1)
            {
                Debug.out.println("jdicom: ERROR: send commitment result in separate association failed: " + ((Throwable) (exception1)).getMessage());
            }
        }

        private void sendResult(DimseExchange dimseexchange)
            throws IOException, IllegalValueException, DicomException, InterruptedException
        {
            byte byte0 = ((byte)(result.getSize(120) != -1 ? 2 : 1));
            byte byte1 = dimseexchange.getPresentationContext(4100, 8193);
            DicomMessage dicommessage = dimseexchange.neventReport(byte1, "1.2.840.10008.1.20.1", "1.2.840.10008.1.20.1.1", ((int) (byte0)), result);
            int i = ((DicomObject) (dicommessage)).getI(9);
            if(i != 0)
                Debug.out.println("jdicom: WARNING: receive N-EVENT-REPORT RSP with status " + Status.getStatusEntry(i));
        }

        private DimseExchange connect()
            throws IOException, IllegalValueException, DicomException, UnknownUIDException
        {
            Request request = new Request();
            request.setCalledTitle(remoteAET);
            request.setCallingTitle(localAET);
            request.setMaxPduSize(maxPduSize);
            request.addPresentationContext(4100, tsids);
            request.setScuScpRoleSelection(4100, 0, 1);
            Socket socket = new Socket(aet.host, aet.port);
            Requestor requestor = new Requestor(socket, request);
            VerboseAssociation verboseassociation = requestor.openAssoc();
            if(verboseassociation == null)
            {
                throw new DicomException("Association rejected by StorageCommitment - " + requestor.response());
            } else
            {
                DimseExchange dimseexchange = new DimseExchange(verboseassociation, ((com.tiani.dicom.framework.DimseRqManager) (null)), false, false, 1);
                dimseexchange.setARTIM(artim);
                (new Thread(((Runnable) (dimseexchange)))).start();
                return dimseexchange;
            }
        }

        Context(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
            throws DicomException
        {
            cookie = new Hashtable();
            aet = null;
            status = 0;
            actionInfo = dicommessage.getDataset();
            result = new DicomObject();
            result.set(118, actionInfo.get(118));
            remoteAET = dimseexchange.remoteAET();
            localAET = dimseexchange.localAET();
            if(!"1.2.840.10008.1.20.1.1".equals(((Object) (s1))))
                status = 274;
            else
            if(((DicomObject) (dicommessage)).getSize(18) != 1)
                status = 530;
            else
            if(((DicomObject) (dicommessage)).getI(18) != 1)
            {
                ((DicomObject) (dicommessage1)).set(18, ((DicomObject) (dicommessage)).get(18));
                status = 276;
            } else
            if(actionInfo.getSize(118) != 1 || actionInfo.getSize(121) < 1)
                status = 277;
            else
            if(assocMode != 21 && (aet = aets.lookup(remoteAET)) == null)
            {
                ((DicomObject) (dicommessage1)).set(11, ((Object) ("Unrecognized AET of Storage Commitment SCU - " + remoteAET)));
                status = 272;
            } else
            {
                status = strategy.prepare(dimseexchange, actionInfo, dicommessage1, cookie);
                if(status == 0 && timing == 10)
                    status = strategy.commit(dimseexchange, actionInfo, dicommessage1, cookie, result);
            }
            ((DicomObject) (dicommessage1)).set(9, ((Object) (new Integer(status))));
        }
    }

    public static interface IStrategy
    {

        public abstract int prepare(DimseExchange dimseexchange, DicomObject dicomobject, DicomMessage dicommessage, Hashtable hashtable);

        public abstract int commit(DimseExchange dimseexchange, DicomObject dicomobject, DicomMessage dicommessage, Hashtable hashtable, DicomObject dicomobject1);
    }


    public static final int BEFORE_ACTION_RSP = 10;
    public static final int AFTER_ACTION_RSP = 11;
    public static final int OPEN_ASSOC = 20;
    public static final int REUSE_ASSOC = 21;
    public static final int TRY_REUSE_ASSOC = 22;
    private IStrategy strategy;
    private AETable aets;
    private int tsids[] = {
        8193
    };
    private int timing;
    private int assocMode;
    private int artim;
    private int maxPduSize;

    public StorageCmtSCP(IStrategy istrategy, AETable aetable)
    {
        timing = 10;
        assocMode = 22;
        artim = 1000;
        maxPduSize = 32768;
        strategy = istrategy;
        aets = aetable;
    }

    public void setStrategy(IStrategy istrategy)
    {
        strategy = istrategy;
    }

    public void setAEaets(AETable aetable)
    {
        aets = aetable;
    }

    public void setTiming(int i)
    {
        timing = i;
    }

    public void setAssocMode(int i)
    {
        assocMode = i;
    }

    public void setTransferSyntaxes(int ai[])
    {
        tsids = ai;
    }

    public void setARTIM(int i)
    {
        artim = i;
    }

    public void setMaxPduSize(int i)
    {
        maxPduSize = i;
    }

    public void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        if(!"1.2.840.10008.1.20.1".equals(((Object) (s))))
        {
            throw new RuntimeException("com.tiani.dicom.service.StorageCommitmentSCP was registered under wrong SOP Class UID: " + s);
        } else
        {
            String s1 = ((DicomObject) (dicommessage)).getS(14);
            DicomMessage dicommessage1 = new DicomMessage(dicommessage.getPresentationContext(), dicommessage.getAbstractSyntax(), 33072, i, ((DicomObject) (null)));
            dicommessage1.affectedSOP(s, s1);
            Context context = new Context(dimseexchange, s, s1, dicommessage, dicommessage1);
            dimseexchange.getAssociation().sendMessage(dicommessage1);
            context.postProcess(dimseexchange);
            return;
        }
    }

    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        throw new DicomException("Error: cancel request for N-ACTION service");
    }







}
