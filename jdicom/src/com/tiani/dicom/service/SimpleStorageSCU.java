// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SimpleStorageSCU.java

package com.tiani.dicom.service;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Request;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.VerboseAssociation;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class SimpleStorageSCU
{

    public SimpleStorageSCU()
    {
    }

    public static VerboseAssociation openAssociation(String s, int i, String s1, String s2, int ai[])
        throws IOException, DicomException, UnknownUIDException, IllegalValueException
    {
        Request request = new Request();
        request.setCalledTitle(s1);
        request.setCallingTitle(s2);
        int ai1[] = {
            8193
        };
        for(int j = 0; j < ai.length; j++)
            request.addPresentationContext(ai[j], ai1);

        Socket socket = new Socket(s, i);
        Requestor requestor = new Requestor(socket, request);
        return requestor.openAssoc();
    }

    public static DicomObject echo(VerboseAssociation verboseassociation)
        throws IOException, DicomException, UnknownUIDException, IllegalValueException
    {
        DicomMessage dicommessage = new DicomMessage(48, verboseassociation.nextMessageID(), ((DicomObject) (null)));
        dicommessage.affectedSOPclassUID("1.2.840.10008.1.1");
        ((Association) (verboseassociation)).send(4097, ((DicomObject) (dicommessage)), ((DicomObject) (null)));
        return readRSP(verboseassociation);
    }

    public static DicomObject sendDicomObject(VerboseAssociation verboseassociation, DicomObject dicomobject)
        throws IOException, DicomException, UnknownUIDException, IllegalValueException
    {
        DicomMessage dicommessage = new DicomMessage(1, verboseassociation.nextMessageID(), dicomobject);
        String s = dicomobject.getS(63);
        String s1 = dicomobject.getS(62);
        dicommessage.affectedSOP(s1, s);
        dicommessage.priority(0);
        ((Association) (verboseassociation)).send(UID.getUIDEntry(s1).getConstant(), ((DicomObject) (dicommessage)), dicomobject);
        return readRSP(verboseassociation);
    }

    public static DicomObject find(VerboseAssociation verboseassociation, int i, DicomObject dicomobject, List list)
        throws IOException, DicomException, UnknownUIDException, IllegalValueException
    {
        DicomMessage dicommessage = new DicomMessage(32, verboseassociation.nextMessageID(), dicomobject);
        dicommessage.affectedSOPclassUID(UID.getUIDEntry(i).getValue());
        dicommessage.priority(0);
        ((Association) (verboseassociation)).send(i, ((DicomObject) (dicommessage)), dicomobject);
        DicomObject dicomobject2 = null;
        do
        {
            DicomObject dicomobject1 = readRSP(verboseassociation);
            if(dicomobject1.getI(8) != 257)
                dicomobject2 = verboseassociation.receiveData();
            switch(dicomobject1.getI(9))
            {
            case 65280: 
            case 65281: 
                list.add(((Object) (dicomobject2)));
                break;

            default:
                return dicomobject1;
            }
        } while(true);
    }

    public static DicomObject readRSP(VerboseAssociation verboseassociation)
        throws IOException, DicomException, UnknownUIDException, IllegalValueException
    {
        switch(((Association) (verboseassociation)).peek())
        {
        case 10: // '\n'
            return verboseassociation.receiveCommand();

        case 11: // '\013'
            verboseassociation.receiveReleaseRequest();
            verboseassociation.sendReleaseResponse();
            verboseassociation.closesocket();
            throw new IOException("Receive A-RELEASE-RQ");

        case 13: // '\r'
            verboseassociation.receiveReleaseResponse();
            verboseassociation.closesocket();
            throw new IOException("Receive A-RELEASE-RP");

        case 12: // '\f'
            verboseassociation.receiveAbort();
            verboseassociation.closesocket();
            throw new IOException("Receive A-ABORT");
        }
        verboseassociation.closesocket();
        throw new IOException("Receive unrecognized PDU");
    }

    public static void closeAssociation(VerboseAssociation verboseassociation)
        throws IOException, IllegalValueException
    {
        if(verboseassociation.isOpen())
        {
            verboseassociation.sendReleaseRequest();
            verboseassociation.receiveReleaseResponse();
            verboseassociation.closesocket();
        }
    }
}
