// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StorageCmtConstants.java

package com.tiani.dicom.service;


public interface StorageCmtConstants
{

    public static final String SOP_CLASS_UID = "1.2.840.10008.1.20.1";
    public static final String SOP_INSTANCE_UID = "1.2.840.10008.1.20.1.1";
    public static final int PROCESSING_FAILURE = 272;
    public static final int NO_SUCH_OBJECT_INSTANCE = 274;
    public static final int RESOURCE_LIMITATION = 275;
    public static final int REFERENCED_SOP_CLASS_NOT_SUPPORTED = 290;
    public static final int CLASS_INSTANCE_CONFLICT = 281;
    public static final int DUPLICATE_TRANSACTION_UID = 305;
}
