// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   Param.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.tiani.dicom.media.DRFactory;
import com.tiani.dicom.service.IconFactory;
import com.tiani.dicom.util.CheckParam;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

// Referenced classes of package com.tiani.dicom.imageserver:
//            WebsiteBuilder

final class Param
{

    public static final String AET_PROPERTIES_KEY = "AET.Properties";
    public static final String KEYS[] = {
        "Port", "CalledTitles", "CallingTitles", "AET.Properties", "MultiThreadTCP", "MultiThreadAssoc", "QueueRQ", "MaxOpInvoked", "MaxPduSize", "AssocTimeout[ms]",
        "ReleaseTimeout[ms]", "Verification", "Verification.TransferSyntax", "Storage", "Storage.WarningAsSuccess", "Storage.Validate", "Storage.TransferSyntax", "StorageCmt", "StorageCmt.TransferSyntax", "StorageCmt.Forward",
        "StorageCmt.BeforeRSP", "StorageCmt.OpenAssoc", "QR.PatRoot", "QR.StudyRoot", "QR.PatStudyOnly", "QR.TransferSyntax", "QR.Forward.Query", "QR.Forward.Retrieve", "QR.Priority", "Forward.AET",
        "Forward.KeepOpen", "Forward.CallingTitle", "SCN", "SCN.AET", "SCN.Total", "Fileset", "Fileset.Path", "Fileset.Separate", "Fileset.SplitSeries", "Fileset.ID",
        "Fileset.Scheme", "Fileset.TransferSyntax", "Fileset.FilenameExt", "Fileset.Icon", "Fileset.Icon.MaxRows", "Fileset.Icon.MaxColumns", "Fileset.Icon.Jpeg", "Fileset.Icon.Jpeg.Quality", "Fileset.Icon.Jpeg.Multiframe", "Fileset.Icon.Color",
        "Fileset.MaxSize[MB]", "Website", "Website.Path", "Website.Separate", "Website.Scheme", "Website.Jpeg.MaxRows", "Website.Jpeg.MaxColumns", "Website.Jpeg.Multiframe", "Website.Jpeg.Quality", "Website.Jpeg.BurnInOverlays",
        "Verbose", "DumpCmdsetIntoDir", "DumpDatasetIntoDir"
    };
    public static final Hashtable CHECKS;
    private static final String PRIORITIES[] = {
        "MEDIUM", "HIGH", "LOW"
    };
    private static final String NATIVE_TRANSFER_SYNTAX_NAMES[] = {
        "ImplicitVRLittleEndian", "ExplicitVRLittleEndian", "ExplicitVRBigEndian"
    };
    private static final String ACCEPT_TRANSFER_SYNTAX_NAMES[] = {
        "ImplicitVRLittleEndian", "ExplicitVRLittleEndian", "ExplicitVRBigEndian", "RLELossless", "JPEGLossless", "JPEG_Baseline", "AllSupported"
    };
    private static final int IVR_LE_TS[] = {
        8193
    };
    private static final int EVR_LE_TS[] = {
        8194, 8193
    };
    private static final int EVR_BE_TS[] = {
        8195, 8194, 8193
    };
    private static final int RLE_LL_TS[] = {
        8198, 8194, 8193
    };
    private static final int JPEG_LL_TS[] = {
        8197, 8194, 8193
    };
    private static final int JPEG_BASELINE_TS[] = {
        8196, 8194, 8193
    };
    private static final int ALL_TS[] = {
        8197, 8198, 8194, 8195, 8193, 8196
    };
    private static final int NATIVE_TS[] = {
        8194, 8195, 8193
    };
    private static final int IMG_TS[][] = {
        IVR_LE_TS, EVR_LE_TS, EVR_BE_TS, RLE_LL_TS, JPEG_LL_TS, JPEG_BASELINE_TS, ALL_TS
    };
    private static final int NON_IMG_TS[][] = {
        IVR_LE_TS, EVR_LE_TS, EVR_BE_TS, EVR_LE_TS, EVR_LE_TS, EVR_LE_TS, NATIVE_TS
    };
    private static final String MEDIA_TRANSFER_SYNTAX_NAMES[] = {
        "ImplicitVRLittleEndian", "ExplicitVRLittleEndian", "ExplicitVRBigEndian", "JPEGLossless", "SameAsReceived", "AcrNema"
    };
    private static final String ICON_COLOR[] = {
        "ColorToGray", "ColorAsOriginal"
    };
    private static final String COMMIT_ASSOC_MODE_NAMES[] = {
        "Reuse", "TryReuse", "OpenNew"
    };
    private static final int COMMIT_ASSOC_MODES[] = {
        21, 22, 20
    };
    private static final String VERBOSE[] = {
        "0", "1", "2", "3", "4", "5"
    };
    private final Properties prop;
    private final int port;
    private final String calledTitles[];
    private final String callingTitles[];
    private final String aetProperties;
    private final boolean multiThreadTCP;
    private final boolean multiThreadAssoc;
    private final boolean queueRQ;
    private final int maxOpInvoked;
    private final int maxPduSize;
    private final int assocTimeout;
    private final int releaseTimeout;
    private final boolean verification;
    private final int verificationTS;
    private final boolean storage;
    private final int storageTS;
    private final boolean warningAsSuccess;
    private final boolean storageValidate;
    private final boolean storageCmt;
    private final int storageCmtTS;
    private final int storageCmtTiming;
    private final int storageCmtAssocMode;
    private final boolean qrPatRoot;
    private final boolean qrStudyRoot;
    private final boolean qrPatStudyOnly;
    private final int qrTS;
    private final int qrPriority;
    private final boolean fwdStorageCmt;
    private final boolean fwdQuery;
    private final boolean fwdRetrieve;
    private final boolean fwdKeepOpen;
    private final boolean fwdCallingTitle;
    private final boolean scn;
    private final boolean scnTotal;
    private final boolean fileSet;
    private final boolean fileSetSeparate;
    private final boolean fileSetSplitSeries;
    private final IconFactory iconFactory;
    private final WebsiteBuilder websiteBuilder;
    private final boolean filesetIconJpeg;
    private final UIDEntry mediaTS4Img;
    private final UIDEntry mediaTS4NonImg;
    private final boolean compress;
    private final boolean fmi;
    private final long fileSetMaxSize;
    private final String filenameExt;
    private final DRFactory drFactory;
    private final int verbose;

    public Param(Properties properties)
        throws IllegalArgumentException
    {
        prop = (Properties)((Hashtable) (properties)).clone();
        CheckParam.verify(properties, CHECKS);
        drFactory = createDRFactory();
        port = Integer.parseInt(prop.getProperty("Port"));
        String s = prop.getProperty("CalledTitles");
        calledTitles = s == null || s.length() <= 0 ? null : tokenize(s, "\\/,");
        s = prop.getProperty("CallingTitles");
        callingTitles = s == null || s.length() <= 0 ? null : tokenize(s, "\\/,");
        s = prop.getProperty("AET.Properties");
        aetProperties = s == null || s.length() <= 0 ? null : s;
        s = prop.getProperty("DumpCmdsetIntoDir");
        Debug.dumpCmdsetIntoDir = s == null || s.length() <= 0 ? null : s;
        s = prop.getProperty("DumpDatasetIntoDir");
        Debug.dumpDatasetIntoDir = s == null || s.length() <= 0 ? null : s;
        multiThreadTCP = parseBoolean(prop.getProperty("MultiThreadTCP"));
        multiThreadAssoc = parseBoolean(prop.getProperty("MultiThreadAssoc"));
        queueRQ = parseBoolean(prop.getProperty("QueueRQ"));
        maxOpInvoked = queueRQ ? Integer.parseInt(prop.getProperty("MaxOpInvoked")) : 1;
        maxPduSize = Integer.parseInt(prop.getProperty("MaxPduSize"));
        assocTimeout = Integer.parseInt(prop.getProperty("AssocTimeout[ms]"));
        releaseTimeout = Integer.parseInt(prop.getProperty("ReleaseTimeout[ms]"));
        verification = parseBoolean(prop.getProperty("Verification"));
        verificationTS = indexOfIn(prop.getProperty("Verification.TransferSyntax"), NATIVE_TRANSFER_SYNTAX_NAMES);
        storage = parseBoolean(prop.getProperty("Storage"));
        storageTS = indexOfIn(prop.getProperty("Storage.TransferSyntax"), ACCEPT_TRANSFER_SYNTAX_NAMES);
        warningAsSuccess = parseBoolean(prop.getProperty("Storage.WarningAsSuccess"));
        storageValidate = parseBoolean(prop.getProperty("Storage.Validate"));
        storageCmt = parseBoolean(prop.getProperty("StorageCmt"));
        storageCmtTS = indexOfIn(prop.getProperty("StorageCmt.TransferSyntax"), NATIVE_TRANSFER_SYNTAX_NAMES);
        storageCmtTiming = parseBoolean(prop.getProperty("StorageCmt.BeforeRSP")) ? 10 : 11;
        storageCmtAssocMode = COMMIT_ASSOC_MODES[indexOfIn(prop.getProperty("StorageCmt.OpenAssoc"), COMMIT_ASSOC_MODE_NAMES)];
        verbose = Integer.parseInt(prop.getProperty("Verbose"));
        qrPatRoot = parseBoolean(prop.getProperty("QR.PatRoot"));
        qrStudyRoot = parseBoolean(prop.getProperty("QR.StudyRoot"));
        qrPatStudyOnly = parseBoolean(prop.getProperty("QR.PatStudyOnly"));
        qrTS = indexOfIn(prop.getProperty("QR.TransferSyntax"), NATIVE_TRANSFER_SYNTAX_NAMES);
        qrPriority = indexOfIn(prop.getProperty("QR.Priority"), PRIORITIES);
        fwdStorageCmt = parseBoolean(prop.getProperty("StorageCmt.Forward"));
        fwdQuery = parseBoolean(prop.getProperty("QR.Forward.Query"));
        fwdRetrieve = parseBoolean(prop.getProperty("QR.Forward.Retrieve"));
        fwdKeepOpen = parseBoolean(prop.getProperty("Forward.KeepOpen"));
        fwdCallingTitle = parseBoolean(prop.getProperty("Forward.CallingTitle"));
        scn = parseBoolean(prop.getProperty("SCN"));
        scnTotal = parseBoolean(prop.getProperty("SCN.Total"));
        fileSet = parseBoolean(prop.getProperty("Fileset"));
        fileSetSeparate = parseBoolean(prop.getProperty("Fileset.Separate"));
        fileSetSplitSeries = parseBoolean(prop.getProperty("Fileset.SplitSeries"));
        try
        {
            switch(indexOfIn(prop.getProperty("Fileset.TransferSyntax"), MEDIA_TRANSFER_SYNTAX_NAMES))
            {
            case 0: // '\0'
                mediaTS4Img = UID.getUIDEntry(8193);
                mediaTS4NonImg = UID.getUIDEntry(8193);
                compress = false;
                fmi = true;
                break;

            case 1: // '\001'
                mediaTS4Img = UID.getUIDEntry(8194);
                mediaTS4NonImg = UID.getUIDEntry(8194);
                compress = false;
                fmi = true;
                break;

            case 2: // '\002'
                mediaTS4Img = UID.getUIDEntry(8195);
                mediaTS4NonImg = UID.getUIDEntry(8195);
                compress = false;
                fmi = true;
                break;

            case 3: // '\003'
                mediaTS4Img = UID.getUIDEntry(8197);
                mediaTS4NonImg = UID.getUIDEntry(8194);
                compress = true;
                fmi = true;
                break;

            case 4: // '\004'
                mediaTS4Img = null;
                mediaTS4NonImg = null;
                compress = false;
                fmi = true;
                break;

            case 5: // '\005'
                mediaTS4Img = UID.getUIDEntry(8193);
                mediaTS4NonImg = UID.getUIDEntry(8193);
                compress = false;
                fmi = false;
                break;

            default:
                throw new RuntimeException(prop.getProperty("Fileset.TransferSyntax"));
            }
        }
        catch(IllegalValueException illegalvalueexception)
        {
            throw new RuntimeException("" + illegalvalueexception);
        }
        iconFactory = parseBoolean(prop.getProperty("Fileset.Icon")) ? new IconFactory(Integer.parseInt(prop.getProperty("Fileset.Icon.MaxRows")), Integer.parseInt(prop.getProperty("Fileset.Icon.MaxColumns"))) : null;
        filesetIconJpeg = parseBoolean(prop.getProperty("Fileset.Icon.Jpeg"));
        if(iconFactory != null)
        {
            iconFactory.setColorConvert(indexOfIn(prop.getProperty("Fileset.Icon.Color"), ICON_COLOR));
            iconFactory.setJpeg(filesetIconJpeg);
            iconFactory.setJpegQuality((float)Integer.parseInt(prop.getProperty("Fileset.Icon.Jpeg.Quality")) / 100F);
            iconFactory.setJpegMultiframe(parseBoolean(prop.getProperty("Fileset.Icon.Jpeg.Multiframe")));
        }
        fileSetMaxSize = (long)Integer.parseInt(prop.getProperty("Fileset.MaxSize[MB]")) * 0x100000L;
        s = prop.getProperty("Fileset.FilenameExt").trim();
        filenameExt = s.length() != 0 && !s.startsWith(".") ? "." + s : s;
        websiteBuilder = parseBoolean(prop.getProperty("Website")) ? new WebsiteBuilder(prop.getProperty("Website.Path"), parseBoolean(prop.getProperty("Website.Separate")), Integer.parseInt(prop.getProperty("Website.Jpeg.MaxRows")), Integer.parseInt(prop.getProperty("Website.Jpeg.MaxColumns")), parseBoolean(prop.getProperty("Website.Jpeg.Multiframe")), (float)Integer.parseInt(prop.getProperty("Website.Jpeg.Quality")) / 100F, parseBoolean(prop.getProperty("Website.Jpeg.BurnInOverlays")), loadWebsiteScheme(prop.getProperty("Website.Scheme"))) : null;
    }

    private Properties loadWebsiteScheme(String s)
    {
        if(s != null && s.length() > 0)
        {
            try
            {
                Properties properties = new Properties();
                Object obj = s.indexOf(':') <= 2 ? ((Object) (new FileInputStream(s))) : ((Object) ((new URL(s)).openStream()));
                try
                {
                    properties.load(((InputStream) (obj)));
                }
                finally
                {
                    ((InputStream) (obj)).close();
                }
                if(Debug.DEBUG > 1)
                    Debug.out.println("Loaded scheme for website from " + s);
                return properties;
            }
            catch(Exception exception)
            {
                Debug.out.println("WARNING: Loading scheme for website from " + s + " throws" + exception);
            }
            Debug.out.println("-> Use default scheme");
        }
        return null;
    }

    private static int indexOfIn(String s, String as[])
    {
        int i;
        for(i = as.length; --i >= 0;)
            if(as[i].equals(((Object) (s))))
                break;

        return i;
    }

    private DRFactory createDRFactory()
    {
        String s = getFilesetScheme();
        if(s != null && s.length() > 0)
        {
            try
            {
                Properties properties = new Properties();
                Object obj = s.indexOf(':') <= 2 ? ((Object) (new FileInputStream(s))) : ((Object) ((new URL(s)).openStream()));
                try
                {
                    properties.load(((InputStream) (obj)));
                }
                finally
                {
                    ((InputStream) (obj)).close();
                }
                if(Debug.DEBUG > 1)
                    Debug.out.println("Loaded scheme for directory records from " + s);
                return new DRFactory(properties);
            }
            catch(Exception exception)
            {
                Debug.out.println("WARNING: Loading scheme for directory records from " + s + " throws" + exception);
            }
            Debug.out.println("-> Use default scheme");
        }
        return new DRFactory();
    }

    public DRFactory getDRFactory()
    {
        return drFactory;
    }

    public String toString()
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("Param:\n");
        for(int i = 0; i < KEYS.length; i++)
            stringbuffer.append(KEYS[i]).append('=').append(prop.getProperty(KEYS[i])).append('\n');

        return stringbuffer.toString();
    }

    public int getPort()
    {
        return port;
    }

    public String[] getCalledTitles()
    {
        return calledTitles;
    }

    public String[] getCallingTitles()
    {
        return callingTitles;
    }

    public String getAETProperties()
    {
        return aetProperties;
    }

    public boolean isMultiThreadTCP()
    {
        return multiThreadTCP;
    }

    public boolean isMultiThreadAssoc()
    {
        return multiThreadAssoc;
    }

    public boolean isQueueRQ()
    {
        return queueRQ;
    }

    public int getMaxOperationsInvoked()
    {
        return maxOpInvoked;
    }

    public int getMaxPduSize()
    {
        return maxPduSize;
    }

    public int getReleaseTimeout()
    {
        return releaseTimeout;
    }

    public int getAssocTimeout()
    {
        return assocTimeout;
    }

    public boolean isVerification()
    {
        return verification;
    }

    public int[] getVerificationTS()
    {
        return NON_IMG_TS[verificationTS];
    }

    public boolean isStorage()
    {
        return storage;
    }

    public int[] getNonImageStorageTS()
    {
        return NON_IMG_TS[storageTS];
    }

    public int[] getImageStorageTS()
    {
        return IMG_TS[storageTS];
    }

    public boolean isWarningAsSuccess()
    {
        return warningAsSuccess;
    }

    public boolean isStorageValidate()
    {
        return storageValidate;
    }

    public boolean isStorageCmt()
    {
        return storageCmt;
    }

    public int[] getStorageCmtTS()
    {
        return NON_IMG_TS[storageCmtTS];
    }

    public int getStorageCmtAssocMode()
    {
        return storageCmtAssocMode;
    }

    public int getStorageCmtTiming()
    {
        return storageCmtTiming;
    }

    public boolean isQRPatRoot()
    {
        return qrPatRoot;
    }

    public boolean isQRStudyRoot()
    {
        return qrStudyRoot;
    }

    public boolean isQRPatStudyOnly()
    {
        return qrPatStudyOnly;
    }

    public int[] getQueryRetrieveTS()
    {
        return NON_IMG_TS[qrTS];
    }

    public int getMovePriority()
    {
        return qrPriority;
    }

    public boolean isFwdStorageCmt()
    {
        return fwdStorageCmt;
    }

    public boolean isFwdQuery()
    {
        return fwdQuery;
    }

    public boolean isFwdRetrieve()
    {
        return fwdRetrieve;
    }

    public boolean isFwdKeepOpen()
    {
        return fwdKeepOpen;
    }

    public boolean isFwdCallingTitle()
    {
        return fwdCallingTitle;
    }

    public String getFwdAET()
    {
        return prop.getProperty("Forward.AET");
    }

    public boolean isSCN()
    {
        return scn;
    }

    public String getSCNAET()
    {
        return prop.getProperty("SCN.AET");
    }

    public boolean isSCNTotal()
    {
        return scnTotal;
    }

    public boolean isFileset()
    {
        return fileSet;
    }

    public boolean isFilesetSeparate()
    {
        return fileSetSeparate;
    }

    public boolean isFilesetSplitSeries()
    {
        return fileSetSplitSeries;
    }

    public String getFilesetPath()
    {
        return prop.getProperty("Fileset.Path");
    }

    public String getFilesetScheme()
    {
        return prop.getProperty("Fileset.Scheme");
    }

    public String getFilesetID()
    {
        return prop.getProperty("Fileset.ID");
    }

    public UIDEntry getMediaTS4Img()
    {
        return mediaTS4Img;
    }

    public UIDEntry getMediaTS4NonImg()
    {
        return mediaTS4NonImg;
    }

    public final boolean isCompress()
    {
        return compress;
    }

    public final boolean isFileMetaInformation()
    {
        return fmi;
    }

    public final String getFilenameExt()
    {
        return filenameExt;
    }

    public final IconFactory getIconFactory()
    {
        return iconFactory;
    }

    public final boolean isFilesetIconJpeg()
    {
        return filesetIconJpeg;
    }

    public final long getFilesetMaxSize()
    {
        return fileSetMaxSize;
    }

    public final WebsiteBuilder getWebsiteBuilder()
    {
        return websiteBuilder;
    }

    public final int getVerbose()
    {
        return verbose;
    }

    public Properties getProperties()
    {
        return prop;
    }

    private static String[] tokenize(String s, String s1)
    {
        StringTokenizer stringtokenizer = new StringTokenizer(s, s1);
        String as[] = new String[stringtokenizer.countTokens()];
        for(int i = 0; i < as.length; i++)
            as[i] = stringtokenizer.nextToken();

        return as;
    }

    private static boolean parseBoolean(String s)
    {
        return s != null && "true".compareTo(s.toLowerCase()) == 0;
    }

    static
    {
        CHECKS = new Hashtable();
        CHECKS.put("Port", ((Object) (CheckParam.range(100, 65535))));
        CHECKS.put("MultiThreadTCP", ((Object) (CheckParam.bool())));
        CHECKS.put("MultiThreadAssoc", ((Object) (CheckParam.bool())));
        CHECKS.put("QueueRQ", ((Object) (CheckParam.bool())));
        CHECKS.put("MaxOpInvoked", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("MaxPduSize", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("AssocTimeout[ms]", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("ReleaseTimeout[ms]", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("Verification", ((Object) (CheckParam.bool())));
        CHECKS.put("Verification.TransferSyntax", ((Object) (CheckParam.enum(NATIVE_TRANSFER_SYNTAX_NAMES))));
        CHECKS.put("Storage", ((Object) (CheckParam.bool())));
        CHECKS.put("Storage.TransferSyntax", ((Object) (CheckParam.enum(ACCEPT_TRANSFER_SYNTAX_NAMES))));
        CHECKS.put("Storage.WarningAsSuccess", ((Object) (CheckParam.bool())));
        CHECKS.put("Storage.Validate", ((Object) (CheckParam.bool())));
        CHECKS.put("StorageCmt", ((Object) (CheckParam.bool())));
        CHECKS.put("StorageCmt.TransferSyntax", ((Object) (CheckParam.enum(NATIVE_TRANSFER_SYNTAX_NAMES))));
        CHECKS.put("StorageCmt.Forward", ((Object) (CheckParam.bool())));
        CHECKS.put("StorageCmt.BeforeRSP", ((Object) (CheckParam.bool())));
        CHECKS.put("StorageCmt.OpenAssoc", ((Object) (CheckParam.enum(COMMIT_ASSOC_MODE_NAMES))));
        CHECKS.put("QR.PatRoot", ((Object) (CheckParam.bool())));
        CHECKS.put("QR.StudyRoot", ((Object) (CheckParam.bool())));
        CHECKS.put("QR.PatStudyOnly", ((Object) (CheckParam.bool())));
        CHECKS.put("QR.Priority", ((Object) (CheckParam.enum(PRIORITIES))));
        CHECKS.put("QR.Forward.Query", ((Object) (CheckParam.bool())));
        CHECKS.put("QR.Forward.Retrieve", ((Object) (CheckParam.bool())));
        CHECKS.put("QR.TransferSyntax", ((Object) (CheckParam.enum(NATIVE_TRANSFER_SYNTAX_NAMES))));
        CHECKS.put("Forward.KeepOpen", ((Object) (CheckParam.bool())));
        CHECKS.put("Forward.CallingTitle", ((Object) (CheckParam.bool())));
        CHECKS.put("SCN", ((Object) (CheckParam.bool())));
        CHECKS.put("SCN.Total", ((Object) (CheckParam.bool())));
        CHECKS.put("Fileset", ((Object) (CheckParam.bool())));
        CHECKS.put("Fileset.Separate", ((Object) (CheckParam.bool())));
        CHECKS.put("Fileset.SplitSeries", ((Object) (CheckParam.bool())));
        CHECKS.put("Fileset.TransferSyntax", ((Object) (CheckParam.enum(MEDIA_TRANSFER_SYNTAX_NAMES))));
        CHECKS.put("Fileset.Icon", ((Object) (CheckParam.bool())));
        CHECKS.put("Fileset.Icon.MaxRows", ((Object) (CheckParam.range(32, 512))));
        CHECKS.put("Fileset.Icon.MaxColumns", ((Object) (CheckParam.range(32, 512))));
        CHECKS.put("Fileset.Icon.Jpeg", ((Object) (CheckParam.bool())));
        CHECKS.put("Fileset.Icon.Jpeg.Quality", ((Object) (CheckParam.range(0, 100))));
        CHECKS.put("Fileset.Icon.Jpeg.Multiframe", ((Object) (CheckParam.bool())));
        CHECKS.put("Fileset.Icon.Color", ((Object) (CheckParam.enum(ICON_COLOR))));
        CHECKS.put("Fileset.MaxSize[MB]", ((Object) (CheckParam.range(10, 0x186a0))));
        CHECKS.put("Website", ((Object) (CheckParam.bool())));
        CHECKS.put("Website.Separate", ((Object) (CheckParam.bool())));
        CHECKS.put("Website.Jpeg.MaxRows", ((Object) (CheckParam.range(32, 10000))));
        CHECKS.put("Website.Jpeg.MaxColumns", ((Object) (CheckParam.range(32, 10000))));
        CHECKS.put("Website.Jpeg.Multiframe", ((Object) (CheckParam.bool())));
        CHECKS.put("Website.Jpeg.Quality", ((Object) (CheckParam.range(0, 100))));
        CHECKS.put("Website.Jpeg.BurnInOverlays", ((Object) (CheckParam.bool())));
        CHECKS.put("Verbose", ((Object) (CheckParam.enum(VERBOSE))));
    }
}
