// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PrintServerDummy.java

package com.tiani.dicom.printserver;

import com.archimed.dicom.Debug;
import com.kcmultimedia.demo.SCMEvent;
import com.kcmultimedia.demo.SCMEventListener;
import com.kcmultimedia.demo.SCMEventManager;
import com.tiani.dicom.framework.Acceptor;
import com.tiani.dicom.framework.DefAcceptorListener;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DefNActionSCP;
import com.tiani.dicom.framework.DefNCreateSCP;
import com.tiani.dicom.framework.DefNDeleteSCP;
import com.tiani.dicom.framework.DefNGetSCP;
import com.tiani.dicom.framework.DefNSetSCP;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.framework.IAcceptancePolicy;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.SimpleAcceptancePolicy;
import com.tiani.rmicfg.IServer;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

// Referenced classes of package com.tiani.dicom.printserver:
//            Param, BasicFilmBoxSCP, PrinterSCP

public class PrintServerDummy
    implements IServer, SCMEventListener
{

    private static final IDimseRqListener _ncreateSCP = new DefNCreateSCP();
    private static final IDimseRqListener _ndeleteSCP = new DefNDeleteSCP();
    private static final IDimseRqListener _nactionSCP = new DefNActionSCP();
    private static final IDimseRqListener _nsetSCP = new DefNSetSCP();
    private static final IDimseRqListener _ngetSCP = new DefNGetSCP();
    private Param _param;
    private Acceptor _acceptor;
    private Thread _thread;
    private BasicFilmBoxSCP _filmBoxSCP;
    private PrinterSCP _printerSCP;
    private DimseRqManager _rqManager;
    private DefAcceptorListener _acceptorListener;

    public PrintServerDummy()
    {
        this(((Properties) (null)));
    }

    public PrintServerDummy(Properties properties)
    {
        _acceptor = null;
        _thread = null;
        SCMEventManager scmeventmanager = SCMEventManager.getInstance();
        scmeventmanager.addSCMEventListener(((SCMEventListener) (this)));
        _param = new Param(properties == null ? loadDefProperties() : properties);
        Debug.DEBUG = _param.getVerbose();
        _filmBoxSCP = new BasicFilmBoxSCP(_param);
        _printerSCP = new PrinterSCP(_param);
        _rqManager = createRqManager(_filmBoxSCP, _printerSCP);
    }

    public String getType()
        throws RemoteException
    {
        return "PrintServerDummy";
    }

    public void setProperties(Properties properties)
        throws RemoteException
    {
        _param = new Param(properties);
        Debug.DEBUG = _param.getVerbose();
        synchronized(this)
        {
            if(_thread != null)
            {
                _acceptor.setARTIM(_param.getAssocTimeout(), _param.getReleaseTimeout());
                _acceptor.setStartThread(_param.isMultiThreadTCP());
                _acceptorListener.setStartThread(_param.isMultiThreadAssoc());
                _acceptorListener.setQueueRQ(_param.isQueueRQ());
                _acceptorListener.setARTIM(_param.getReleaseTimeout());
                _acceptor.setAcceptancePolicy(createPolicy());
            }
        }
        _printerSCP.setParam(_param);
        _filmBoxSCP.setParam(_param);
    }

    public String[] getPropertyNames()
        throws RemoteException
    {
        return Param.KEYS;
    }

    public Properties getProperties()
        throws RemoteException
    {
        return _param.getProperties();
    }

    public void start()
        throws RemoteException
    {
        if(_thread != null)
            throw new IllegalStateException("server is running");
        try
        {
            Debug.out.println("Start PrintServerDummy v1.4.5 at " + ((Calendar) (new GregorianCalendar())).getTime() + " with " + _param);
            Debug.DEBUG = _param.getVerbose();
            _acceptorListener = new DefAcceptorListener(_param.isMultiThreadAssoc(), _rqManager, _param.isQueueRQ(), false);
            _acceptor = new Acceptor(new ServerSocket(_param.getPort()), _param.isMultiThreadTCP(), createPolicy(), ((com.tiani.dicom.framework.IAcceptorListener) (_acceptorListener)));
            _thread = new Thread(((Runnable) (_acceptor)));
            _thread.start();
            Debug.out.println("Waiting for invocations from clients...");
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    public void start(Properties properties)
        throws RemoteException
    {
        setProperties(properties);
        start();
    }

    public void stop(boolean flag, boolean flag1)
        throws RemoteException
    {
        try
        {
            if(_thread == null)
                throw new IllegalStateException("server is not running");
            _acceptor.stop(flag);
            if(flag1)
                _thread.join();
            synchronized(this)
            {
                _thread = null;
                _acceptor = null;
                _acceptorListener = null;
            }
            Debug.out.println("Stopped PrintServerDummy v1.4.5 at " + ((Calendar) (new GregorianCalendar())).getTime());
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(Debug.out);
            throw new RemoteException("", throwable);
        }
    }

    public boolean isRunning()
        throws RemoteException
    {
        return _thread != null;
    }

    public static void setLog(PrintStream printstream)
    {
        Debug.out = printstream;
    }

    public void handleSCMEvent(SCMEvent scmevent)
    {
        if(scmevent.getID() == 1)
            try
            {
                if(isRunning())
                    stop(true, false);
            }
            catch(Exception exception)
            {
                ((Throwable) (exception)).printStackTrace(System.out);
            }
    }

    public static void main(String args[])
    {
        try
        {
            String s = args.length <= 0 ? "PrintServerDummy.properties" : args[0];
            (new PrintServerDummy(loadProperties(s))).start();
        }
        catch(Throwable throwable)
        {
            System.out.println(((Object) (throwable)));
        }
    }

    static Properties loadProperties(String s)
    {
        try
        {
            Properties properties = new Properties();
            File file = new File(s);
            System.out.println("load properties from " + file.getAbsolutePath());
            FileInputStream fileinputstream = new FileInputStream(file);
            try
            {
                properties.load(((InputStream) (fileinputstream)));
                Properties properties1 = properties;
                return properties1;
            }
            finally
            {
                ((InputStream) (fileinputstream)).close();
            }
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
        return null;
    }

    private static Properties loadDefProperties()
    {
        Properties properties = new Properties();
        InputStream inputstream = (com.tiani.dicom.printserver.PrintServerDummy.class).getResourceAsStream("PrintServerDummy.properties");
        try
        {
            properties.load(inputstream);
            inputstream.close();
            Debug.out.println("Load default properties");
        }
        catch(Exception exception)
        {
            throw new RuntimeException("PrintServerDummy.class.getResourceAsStream(\"PrintServerDummy.properties\") failed!");
        }
        return properties;
    }

    private IAcceptancePolicy createPolicy()
    {
        int i = 0;
        int ai[] = new int[4];
        if(_param.isVerification())
            ai[i++] = 4097;
        if(_param.isBasicGrayscalePrintManagement())
            ai[i++] = 12292;
        if(_param.isBasicColorPrintManagement())
            ai[i++] = 12294;
        if(_param.isPresentationLUT())
            ai[i++] = 4145;
        int ai1[] = new int[i];
        System.arraycopy(((Object) (ai)), 0, ((Object) (ai1)), 0, i);
        return ((IAcceptancePolicy) (new SimpleAcceptancePolicy(_param.getCalledTitle(), _param.getCallingTitles(), ai1, _param.getMaxPduSize(), _param.getMaxInvoke(), 1)));
    }

    private DimseRqManager createRqManager(BasicFilmBoxSCP basicfilmboxscp, PrinterSCP printerscp)
    {
        DimseRqManager dimserqmanager = new DimseRqManager();
        dimserqmanager.regCEchoScp("1.2.840.10008.1.1", DefCEchoSCP.getInstance());
        dimserqmanager.regNCreateScp("1.2.840.10008.5.1.1.23", _ncreateSCP);
        dimserqmanager.regNDeleteScp("1.2.840.10008.5.1.1.23", _ndeleteSCP);
        dimserqmanager.regNCreateScp("1.2.840.10008.5.1.1.1", _ncreateSCP);
        dimserqmanager.regNSetScp("1.2.840.10008.5.1.1.1", _nsetSCP);
        dimserqmanager.regNDeleteScp("1.2.840.10008.5.1.1.1", _ndeleteSCP);
        dimserqmanager.regNActionScp("1.2.840.10008.5.1.1.1", _nactionSCP);
        dimserqmanager.regNCreateScp("1.2.840.10008.5.1.1.2", _filmBoxSCP.getNCreateListener());
        dimserqmanager.regNSetScp("1.2.840.10008.5.1.1.2", _nsetSCP);
        dimserqmanager.regNDeleteScp("1.2.840.10008.5.1.1.2", _ndeleteSCP);
        dimserqmanager.regNActionScp("1.2.840.10008.5.1.1.2", _nactionSCP);
        dimserqmanager.regNSetScp("1.2.840.10008.5.1.1.4", _filmBoxSCP.getGrayscaleImageBoxNSetListener());
        dimserqmanager.regNSetScp("1.2.840.10008.5.1.1.4.1", _filmBoxSCP.getColorImageBoxNSetListener());
        dimserqmanager.regNGetScp("1.2.840.10008.5.1.1.16", printerscp.getNGetListener());
        return dimserqmanager;
    }

}
