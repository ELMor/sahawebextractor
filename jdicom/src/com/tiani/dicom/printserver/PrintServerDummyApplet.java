// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PrintServerDummyApplet.java

package com.tiani.dicom.printserver;

import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.CheckParam;
import java.applet.Applet;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.Properties;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.tiani.dicom.printserver:
//            PrintServerDummy, Param

public class PrintServerDummyApplet extends JApplet
{

    private Properties _params;
    private PrintServerDummy _server;
    private JButton _applyButton;
    private JButton _startButton;
    private JButton _stopButton;
    private JTabbedPane _tabbedPane;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;

    public PrintServerDummyApplet()
    {
        _params = null;
        _server = null;
        _applyButton = new JButton("Apply");
        _startButton = new JButton("Start");
        _stopButton = new JButton("Stop");
        _tabbedPane = new JTabbedPane(2);
        _logTextArea = new JTextArea();
        _logDoc = ((JTextComponent) (_logTextArea)).getDocument();
        _log = new PrintStream(((java.io.OutputStream) (new DocumentOutputStream(_logDoc, 30000))), true);
        _params = null;
    }

    PrintServerDummyApplet(Properties properties)
    {
        _params = null;
        _server = null;
        _applyButton = new JButton("Apply");
        _startButton = new JButton("Start");
        _stopButton = new JButton("Stop");
        _tabbedPane = new JTabbedPane(2);
        _logTextArea = new JTextArea();
        _logDoc = ((JTextComponent) (_logTextArea)).getDocument();
        _log = new PrintStream(((java.io.OutputStream) (new DocumentOutputStream(_logDoc, 30000))), true);
        _params = properties;
    }

    public void init()
    {
        PrintServerDummy.setLog(_log);
        if(isApplet())
            _params = getAppletParam();
        if(_params != null)
            try
            {
                CheckParam.verify(_params, Param.CHECKS);
            }
            catch(Exception exception)
            {
                _log.println(((Object) (exception)));
                _params = null;
            }
        _server = new PrintServerDummy(_params);
        try
        {
            _params = _server.getProperties();
        }
        catch(Exception exception1)
        {
            _log.println(((Object) (exception1)));
        }
        ((AbstractButton) (_applyButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                try
                {
                    _server.setProperties(_params);
                }
                catch(Exception exception2)
                {
                    _log.println(((Object) (exception2)));
                }
            }

        });
        ((AbstractButton) (_startButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                try
                {
                    _tabbedPane.setSelectedIndex(1);
                    _server.start(_params);
                }
                catch(Exception exception2)
                {
                    _log.println(((Object) (exception2)));
                }
            }

        });
        ((AbstractButton) (_stopButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                try
                {
                    _server.stop(true, false);
                }
                catch(Exception exception2)
                {
                    _log.println(((Object) (exception2)));
                }
            }

        });
        Box box = Box.createHorizontalBox();
        ((Container) (box)).add(((java.awt.Component) (_applyButton)));
        ((Container) (box)).add(((java.awt.Component) (_startButton)));
        ((Container) (box)).add(((java.awt.Component) (_stopButton)));
        ((Container) (box)).add(Box.createGlue());
        ((Container) (box)).add(((java.awt.Component) (new JTianiButton(isApplet() ? ((Applet)this).getAppletContext() : null))));
        JPropertiesTable jpropertiestable = new JPropertiesTable(Param.KEYS, _params, Param.CHECKS);
        _tabbedPane.add("Props", ((java.awt.Component) (new JScrollPane(((java.awt.Component) (jpropertiestable))))));
        _tabbedPane.add("Log", ((java.awt.Component) (new JAutoScrollPane(((JTextComponent) (_logTextArea))))));
        Container container = ((JApplet)this).getContentPane();
        container.add(((java.awt.Component) (box)), "North");
        container.add(((java.awt.Component) (_tabbedPane)), "Center");
    }

    private boolean isApplet()
    {
        return ((Applet)this).getDocumentBase() != null;
    }

    Properties getParams()
    {
        return _params;
    }

    private Properties getAppletParam()
    {
        Properties properties = new Properties();
        for(int i = 0; i < Param.KEYS.length; i++)
            ((Hashtable) (properties)).put(((Object) (Param.KEYS[i])), ((Object) (((Applet)this).getParameter(Param.KEYS[i]))));

        return properties;
    }




}
