// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Param.java

package com.tiani.dicom.printserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.tiani.dicom.util.CheckParam;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

final class Param
{

    public static final String KEYS[] = {
        "Port", "CalledTitle", "CallingTitles", "Verification", "BasicGrayscalePrintManagement", "BasicColorPrintManagement", "PresentationLUT", "MultiThreadTCP", "MultiThreadAssoc", "QueueRQ", 
        "MaxPduSize", "AssocTimeout[ms]", "ReleaseTimeout[ms]", "Printer.Status", "Printer.StatusInfo", "Printer.Name", "Printer.Manufacturer", "Printer.ManufacturerModelName", "Printer.DeviceSerialNumber", "Printer.SoftwareVersion", 
        "Printer.DateOfLastCalibration", "Printer.TimeOfLastCalibration", "StoreHC", "StoreHC.Path", "StoreHC.TransferSyntax", "StoreHC.FilenameExt", "Verbose", "DumpCmdsetIntoDir", "DumpDatasetIntoDir"
    };
    public static Hashtable CHECKS;
    public static final String STATUS[] = {
        "NORMAL", "WARNING", "FAILURE"
    };
    public static final String STATUS_INFO[] = {
        "NORMAL", "BAD RECEIVE MGZ", "BAD SUPPLY MGZ", "CALIBRATING", "CHECK CHEMISTRY", "CHECK SORTER", "CHEMICALS EMPTY", "CHEMICALS LOW", "COVER OPEN", "ELEC CONFIG ERR", 
        "ELEC DOWN", "ELEC SW ERROR", "EMPTY 8X10", "EMPTY 8X10 BLUE", "EMPTY 8X10 CLR", "EMPTY 8X10 PAPR", "EMPTY 10X12", "EMPTY 10X12 BLUE", "EMPTY 10X12 CLR", "EMPTY 10X12 PAPR", 
        "EMPTY 10X14", "EMPTY 10X14 BLUE", "EMPTY 10X14 CLR", "EMPTY 10X14 PAPR", "EMPTY 11X14", "EMPTY 11X14 BLUE", "EMPTY 11X14 CLR", "EMPTY 11X14 PAPR", "EMPTY 14X14", "EMPTY 14X14 BLUE", 
        "EMPTY 14X14 CLR", "EMPTY 14X14 PAPR", "EMPTY 14X17", "EMPTY 14X17 BLUE", "EMPTY 14X17 CLR", "EMPTY 14X17 PAPR", "EMPTY 24X24", "EMPTY 24X24 BLUE", "EMPTY 24X24 CLR", "EMPTY 24X24 PAPR", 
        "EMPTY 24X30", "EMPTY 24X30 BLUE", "EMPTY 24X30 CLR", "EMPTY 24X30 PAPR", "EMPTY A4 PAPR", "EMPTY A4 TRANS", "EXPOSURE FAILURE", "FILM JAM", "FILM TRANSP ERR", "FINISHER EMPTY", 
        "FINISHER ERROR", "FINISHER LOW", "LOW 8X10", "LOW 8X10 BLUE", "LOW 8X10 CLR", "LOW 8X10 PAPR", "LOW 10X12", "LOW 10X12 BLUE", "LOW 10X12 CLR", "LOW 10X12 PAPR", 
        "LOW 10X14", "LOW 10X14 BLUE", "LOW 10X14 CLR", "LOW 10X14 PAPR", "LOW 11X14", "LOW 11X14 BLUE", "LOW 11X14 CLR", "LOW 11X14 PAPR", "LOW 14X14", "LOW 14X14 BLUE", 
        "LOW 14X14 CLR", "LOW 14X14 PAPR", "LOW 14X17", "LOW 14X17 BLUE", "LOW 14X17 CLR", "LOW 14X17 PAPR", "LOW 24X24", "LOW 24X24 BLUE", "LOW 24X24 CLR", "LOW 24X24 PAPR", 
        "LOW 24X30", "LOW 24X30 BLUE", "LOW 24X30 CLR", "LOW 24X30 PAPR", "LOW A4 PAPR", "LOW A4 TRANS", "NO RECEIVE MGZ", "NO RIBBON", "NO SUPPLY MGZ", "CHECK PRINTER", 
        "CHECK PROC", "PRINTER DOWN", "PRINTER INIT", "PRINTER OFFLINE", "PROC DOWN", "PROC INIT", "PROC OVERFLOW FL", "PROC OVERFLOW HI", "QUEUED", "RECEIVER FULL", 
        "REQ MED NOT INST", "REQ MED NOT AVAI", "RIBBON ERROR", "SUPPLY EMPTY", "SUPPLY LOW", "UNKNOWN"
    };
    private static final String TRANSFER_SYNTAX_NAMES[] = {
        "ImplicitVRLittleEndian", "ExplicitVRLittleEndian", "ExplicitVRBigEndian"
    };
    private static final int TRANSFER_SYNTAX_CONST[] = {
        8193, 8194, 8195
    };
    private static final String VERBOSE[] = {
        "0", "1", "2", "3", "4", "5"
    };
    private final int _port;
    private final boolean _multiThreadTCP;
    private final boolean _multiThreadAssoc;
    private final boolean _queueRQ;
    private final int _maxInvoke;
    private final int _assocTimeout;
    private final int _releaseTimeout;
    private final int _maxPduSize;
    private final boolean _verification;
    private final boolean _basicGrayscalePrintManagement;
    private final boolean _basicColorPrintManagement;
    private final boolean _presentationLUT;
    private final boolean storeHC;
    private final UIDEntry storageTS;
    private final int _verbose;
    private final Properties _prop;
    private final String filenameExt;

    public Param(Properties properties)
        throws IllegalArgumentException
    {
        _prop = (Properties)((Hashtable) (properties)).clone();
        CheckParam.verify(_prop, CHECKS);
        _port = Integer.parseInt(_prop.getProperty("Port"));
        _multiThreadTCP = parseBoolean(_prop.getProperty("MultiThreadTCP"));
        _multiThreadAssoc = parseBoolean(_prop.getProperty("MultiThreadAssoc"));
        _queueRQ = parseBoolean(_prop.getProperty("QueueRQ"));
        _maxInvoke = _queueRQ ? Integer.parseInt(_prop.getProperty("MaxInvoke")) : 1;
        _maxPduSize = Integer.parseInt(_prop.getProperty("MaxPduSize"));
        _assocTimeout = Integer.parseInt(_prop.getProperty("AssocTimeout[ms]"));
        _releaseTimeout = Integer.parseInt(_prop.getProperty("ReleaseTimeout[ms]"));
        _verification = parseBoolean(_prop.getProperty("Verification"));
        _basicGrayscalePrintManagement = parseBoolean(_prop.getProperty("BasicGrayscalePrintManagement"));
        _basicColorPrintManagement = parseBoolean(_prop.getProperty("BasicColorPrintManagement"));
        _presentationLUT = parseBoolean(_prop.getProperty("PresentationLUT"));
        _verbose = Integer.parseInt(_prop.getProperty("Verbose"));
        storeHC = parseBoolean(_prop.getProperty("StoreHC"));
        int i = indexOfIn(_prop.getProperty("StoreHC.TransferSyntax"), TRANSFER_SYNTAX_NAMES);
        try
        {
            storageTS = UID.getUIDEntry(TRANSFER_SYNTAX_CONST[i]);
        }
        catch(IllegalValueException illegalvalueexception)
        {
            throw new RuntimeException(((Throwable) (illegalvalueexception)).toString());
        }
        String s = _prop.getProperty("StoreHC.FilenameExt").trim();
        filenameExt = s.length() != 0 && !s.startsWith(".") ? "." + s : s;
        s = _prop.getProperty("DumpCmdsetIntoDir");
        Debug.dumpCmdsetIntoDir = s == null || s.length() <= 0 ? null : s;
        s = _prop.getProperty("DumpDatasetIntoDir");
        Debug.dumpDatasetIntoDir = s == null || s.length() <= 0 ? null : s;
    }

    public String toString()
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("Param:\n");
        for(int i = 0; i < KEYS.length; i++)
            stringbuffer.append(KEYS[i]).append('=').append(_prop.getProperty(KEYS[i])).append('\n');

        return stringbuffer.toString();
    }

    public String getCalledTitle()
    {
        String s = _prop.getProperty("CalledTitle");
        return s == null || s.length() <= 0 ? null : s;
    }

    public String[] getCallingTitles()
    {
        String s = _prop.getProperty("CallingTitle");
        return s == null || s.length() <= 0 ? null : tokenize(s, "\\/,");
    }

    public String getPrinterStatus()
    {
        return _prop.getProperty("Printer.Status");
    }

    public String getPrinterStatusInfo()
    {
        return _prop.getProperty("Printer.StatusInfo");
    }

    public String getPrinterName()
    {
        return _prop.getProperty("Printer.Name");
    }

    public String getManufacturer()
    {
        return _prop.getProperty("Printer.Manufactorer");
    }

    public String getManufacturerModelName()
    {
        return _prop.getProperty("Printer.ManufacturerModelName");
    }

    public String getDeviceSerialNumber()
    {
        return _prop.getProperty("Printer.DeviceSerialNumber");
    }

    public String getSoftwareVersion()
    {
        return _prop.getProperty("Printer.SoftwareVersion");
    }

    public String getDateOfLastCalibration()
    {
        return _prop.getProperty("Printer.DateOfLastCalibration");
    }

    public String getTimeOfLastCalibration()
    {
        return _prop.getProperty("Printer.TimeOfLastCalibration");
    }

    public String getStoreHCPath()
    {
        return _prop.getProperty("StoreHC.Path");
    }

    public String getStoreHCFilenameExt()
    {
        return filenameExt;
    }

    public UIDEntry getStoreHCTS()
    {
        return storageTS;
    }

    public int getPort()
    {
        return _port;
    }

    public boolean isMultiThreadTCP()
    {
        return _multiThreadTCP;
    }

    public boolean isMultiThreadAssoc()
    {
        return _multiThreadAssoc;
    }

    public boolean isQueueRQ()
    {
        return _queueRQ;
    }

    public int getMaxInvoke()
    {
        return _maxInvoke;
    }

    public int getMaxPduSize()
    {
        return _maxPduSize;
    }

    public int getAssocTimeout()
    {
        return _assocTimeout;
    }

    public int getReleaseTimeout()
    {
        return _releaseTimeout;
    }

    public boolean isVerification()
    {
        return _verification;
    }

    public boolean isBasicGrayscalePrintManagement()
    {
        return _basicGrayscalePrintManagement;
    }

    public boolean isBasicColorPrintManagement()
    {
        return _basicColorPrintManagement;
    }

    public boolean isPresentationLUT()
    {
        return _presentationLUT;
    }

    public boolean isStoreHC()
    {
        return storeHC;
    }

    public int getVerbose()
    {
        return _verbose;
    }

    public Properties getProperties()
    {
        return _prop;
    }

    private static String[] tokenize(String s, String s1)
    {
        StringTokenizer stringtokenizer = new StringTokenizer(s, s1);
        String as[] = new String[stringtokenizer.countTokens()];
        for(int i = 0; i < as.length; i++)
            as[i] = stringtokenizer.nextToken();

        return as;
    }

    private static boolean parseBoolean(String s)
    {
        return s != null && "true".compareTo(s.toLowerCase()) == 0;
    }

    static int indexOfIn(String s, String as[])
    {
        int i;
        for(i = as.length; --i >= 0;)
            if(as[i].equals(((Object) (s))))
                break;

        return i;
    }

    static 
    {
        CHECKS = new Hashtable();
        CHECKS.put("Port", ((Object) (CheckParam.range(100, 65535))));
        CHECKS.put("MultiThreadTCP", ((Object) (CheckParam.bool())));
        CHECKS.put("MultiThreadAssoc", ((Object) (CheckParam.bool())));
        CHECKS.put("QueueRQ", ((Object) (CheckParam.bool())));
        CHECKS.put("MaxInvoke", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("MaxPduSize", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("AssocTimeout[ms]", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("ReleaseTimeout[ms]", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("Verification", ((Object) (CheckParam.bool())));
        CHECKS.put("BasicGrayscalePrintManagement", ((Object) (CheckParam.bool())));
        CHECKS.put("BasicColorPrintManagement", ((Object) (CheckParam.bool())));
        CHECKS.put("PresentationLUT", ((Object) (CheckParam.bool())));
        CHECKS.put("Printer.Status", ((Object) (CheckParam.enum(STATUS))));
        CHECKS.put("Printer.StatusInfo", ((Object) (CheckParam.enum(STATUS_INFO))));
        CHECKS.put("StoreHC", ((Object) (CheckParam.bool())));
        CHECKS.put("StoreHC.TransferSyntax", ((Object) (CheckParam.enum(TRANSFER_SYNTAX_NAMES))));
        CHECKS.put("Verbose", ((Object) (CheckParam.enum(VERBOSE))));
    }
}
