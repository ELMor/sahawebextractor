// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileMetaInformation.java

package com.tiani.dicom.media;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.io.PrintStream;

public final class FileMetaInformation extends DicomObject
{

    private static final byte _FMI_VERSION[] = {
        0, 1
    };
    public static final String IMPLEMENTATION_CLASS_UID = "1.2.40.0.13.0.0.113";
    public static final String IMPLEMENTATION_VERSION_NAME = "TIANI_JDICOM_113";

    public FileMetaInformation(String s, String s1, String s2, String s3, String s4)
    {
        try
        {
            ((DicomObject)this).set(28, ((Object) (_FMI_VERSION)));
            ((DicomObject)this).set(29, ((Object) (s)));
            ((DicomObject)this).set(30, ((Object) (s1)));
            ((DicomObject)this).set(31, ((Object) (s2)));
            ((DicomObject)this).set(32, ((Object) (s3)));
            ((DicomObject)this).set(33, ((Object) (s4)));
        }
        catch(DicomException dicomexception)
        {
            Debug.out.println("jdicom: " + dicomexception);
        }
    }

    public FileMetaInformation(String s, String s1, String s2)
    {
        this(s, s1, s2, "1.2.40.0.13.0.0.113", "TIANI_JDICOM_113");
    }

    public FileMetaInformation(String s, String s1)
    {
        this(s, s1, "1.2.840.10008.1.2", "1.2.40.0.13.0.0.113", "TIANI_JDICOM_113");
    }

    public FileMetaInformation(DicomObject dicomobject, String s, String s1, String s2)
    {
        this((String)dicomobject.get(62), (String)dicomobject.get(63), s, s1, s2);
    }

    public FileMetaInformation(DicomObject dicomobject, String s)
    {
        this(dicomobject, s, "1.2.40.0.13.0.0.113", "TIANI_JDICOM_113");
    }

    public FileMetaInformation(DicomObject dicomobject)
    {
        this(dicomobject, "1.2.840.10008.1.2", "1.2.40.0.13.0.0.113", "TIANI_JDICOM_113");
    }

    public void setSourceAET(String s)
        throws DicomException
    {
        ((DicomObject)this).set(34, ((Object) (s)));
    }

    public void setPrivateInformation(String s, byte abyte0[])
        throws DicomException
    {
        ((DicomObject)this).set(35, ((Object) (s)));
        ((DicomObject)this).set(36, ((Object) (abyte0)));
    }

    public void check()
        throws DicomException
    {
        check(((DicomObject) (this)));
    }

    public static void check(DicomObject dicomobject)
        throws DicomException
    {
        if(dicomobject == null)
        {
            throw new IllegalArgumentException("Missing FileMetaInformation");
        } else
        {
            _checkExist(dicomobject, 28);
            _checkExist(dicomobject, 29);
            _checkExist(dicomobject, 30);
            _checkExist(dicomobject, 31);
            _checkExist(dicomobject, 32);
            return;
        }
    }

    private static void _checkExist(DicomObject dicomobject, int i)
        throws DicomException
    {
        if(dicomobject.getSize(i) < 1)
            throw new DicomException("Missing " + DDict.getDescription(i));
        else
            return;
    }

}
