// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DRNode.java

package com.tiani.dicom.media;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.IDicomObjectFilter;
import java.util.Iterator;

// Referenced classes of package com.tiani.dicom.media:
//            DREntity

public class DRNode
{

    private final DicomObject dataset;
    private final int hashCode;
    private final String type;
    private final String key;
    private DRNode next;
    private DREntity lowerEntity;
    private int inUse;

    public DRNode(DicomObject dicomobject)
        throws DicomException
    {
        next = null;
        lowerEntity = null;
        dataset = dicomobject;
        type = dicomobject.getS(48);
        key = getKeyFrom(dicomobject, type);
        hashCode = type.hashCode() + (key == null ? 0 : key.hashCode());
        inUse = dicomobject.getI(46);
    }

    public final DicomObject getDataset()
    {
        return dataset;
    }

    public final String getType()
    {
        return type;
    }

    public final String getKey()
    {
        return key;
    }

    public final DRNode getNext()
    {
        return next;
    }

    DRNode getLast()
    {
        DRNode drnode;
        for(drnode = this; drnode.next != null; drnode = drnode.next);
        return drnode;
    }

    DRNode findInUse()
    {
        for(DRNode drnode = this; drnode != null; drnode = drnode.next)
            if(drnode.inUse != 0)
                return drnode;

        return null;
    }

    DRNode find(DRNode drnode)
    {
        for(DRNode drnode1 = this; drnode1 != null; drnode1 = drnode1.next)
            if(drnode1.inUse != 0 && drnode1.equals(drnode))
                return drnode1;

        return null;
    }

    DRNode find(String s, IDicomObjectFilter idicomobjectfilter)
        throws DicomException
    {
        for(DRNode drnode = this; drnode != null; drnode = drnode.next)
            if(drnode.inUse != 0 && (s == null || s.equals(((Object) (drnode.type)))) && (idicomobjectfilter == null || idicomobjectfilter.accept(drnode.getDataset())))
                return drnode;

        return null;
    }

    DRNode find(String s, String s1)
    {
        for(DRNode drnode = this; drnode != null; drnode = drnode.next)
            if(drnode.inUse != 0 && (s1 == null || s1.equals(((Object) (drnode.key)))) && (s == null || s.equals(((Object) (drnode.type)))))
                return drnode;

        return null;
    }

    DRNode find(String s)
    {
        for(DRNode drnode = this; drnode != null; drnode = drnode.next)
            if(drnode.inUse != 0 && (s == null || s.equals(((Object) (drnode.type)))))
                return drnode;

        return null;
    }

    public final DREntity getLowerEntity()
    {
        return lowerEntity;
    }

    public final int getInUse()
    {
        return inUse;
    }

    public final boolean isInUse()
    {
        return inUse != 0;
    }

    public final void setNext(DRNode drnode)
    {
        next = drnode;
    }

    public void setNext(DRNode drnode, Long long1)
    {
        setNext(drnode);
        try
        {
            dataset.set(45, ((Object) (long1)));
        }
        catch(DicomException dicomexception)
        {
            ((Throwable) (dicomexception)).printStackTrace(Debug.out);
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
    }

    public final void setLowerEntity(DREntity drentity)
    {
        lowerEntity = drentity;
    }

    void setLowerEntity(DREntity drentity, Long long1)
    {
        setLowerEntity(drentity);
        try
        {
            dataset.set(47, ((Object) (long1)));
        }
        catch(DicomException dicomexception)
        {
            ((Throwable) (dicomexception)).printStackTrace(Debug.out);
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
    }

    void addLower(DRNode drnode, Long long1)
    {
        if(lowerEntity != null)
            lowerEntity.add(drnode, long1);
        else
            setLowerEntity(new DREntity(drnode), long1);
    }

    public final void setInactive()
    {
        setInUse(0);
    }

    public final void setInUse()
    {
        setInUse(65535);
    }

    public void setInUse(int i)
    {
        try
        {
            dataset.set(46, ((Object) (new Integer(i))));
        }
        catch(DicomException dicomexception)
        {
            ((Throwable) (dicomexception)).printStackTrace(Debug.out);
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
        inUse = i;
    }

    public void delete()
    {
        if(!isInUse())
            throw new IllegalStateException();
        if(lowerEntity != null)
        {
            for(Iterator iterator = lowerEntity.iterator(); iterator.hasNext(); ((DRNode)iterator.next()).delete());
        }
        setInactive();
    }

    public static String getKeyFrom(DicomObject dicomobject, String s)
    {
        String s1 = (String)dicomobject.get(53);
        if(s1 != null)
            return s1;
        char c;
        if("SERIES".equals(((Object) (s))))
            c = '\u01AA';
        else
        if("STUDY".equals(((Object) (s))))
            c = '\u01A9';
        else
        if("PATIENT".equals(((Object) (s))))
            c = '\224';
        else
            c = '?';
        return (String)dicomobject.get(((int) (c)));
    }

    public final int hashCode()
    {
        return hashCode;
    }

    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if(obj == null || !(obj instanceof DRNode))
            return false;
        else
            return equals((DRNode)obj);
    }

    public boolean equals(DRNode drnode)
    {
        if(dataset == drnode.dataset)
            return true;
        else
            return hashCode == drnode.hashCode && key != null && key.equals(((Object) (drnode.key))) && type.equals(((Object) (drnode.type)));
    }

    public static DicomObject createComposite(DRNode adrnode[], boolean flag)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        for(int i = 0; i < adrnode.length; i++)
        {
            DicomObject dicomobject1 = adrnode[i].getDataset();
            if(flag)
                dicomobject.appendVRs(dicomobject1);
            else
                dicomobject.setVRs(dicomobject1);
        }

        return dicomobject;
    }
}
