// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileSet.java

package com.tiani.dicom.media;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package com.tiani.dicom.media:
//            FileMetaInformation, DicomDir

public class FileSet
{

    private String fileSetID;
    private File dirFile;
    private File rootDir;

    public FileSet(File file)
    {
        this(file, "");
    }

    public FileSet(File file, String s)
    {
        dirFile = file;
        rootDir = new File(file.getParent());
        fileSetID = s;
    }

    public File getDirFile()
    {
        return dirFile;
    }

    public File getRootDir()
    {
        return rootDir;
    }

    public String getFileSetID()
    {
        return fileSetID;
    }

    public void setFileSetID(String s)
    {
        s = s;
    }

    public File read(DicomDir dicomdir)
        throws IOException, DicomException
    {
        FileInputStream fileinputstream = new FileInputStream(dirFile);
        try
        {
            ((DicomObject) (dicomdir)).read(((java.io.InputStream) (fileinputstream)));
        }
        finally
        {
            fileinputstream.close();
        }
        dicomdir.init();
        fileSetID = (String)((DicomObject) (dicomdir)).get(38);
        if(fileSetID == null)
            fileSetID = "";
        if(Debug.DEBUG > 0)
            logRead(dirFile);
        return dirFile;
    }

    public File write(DicomDir dicomdir)
        throws IOException, DicomException
    {
        FileOutputStream fileoutputstream = new FileOutputStream(dirFile);
        try
        {
            dicomdir.write(((java.io.OutputStream) (fileoutputstream)));
        }
        finally
        {
            fileoutputstream.close();
        }
        if(Debug.DEBUG > 0)
            logWrite(dirFile);
        return dirFile;
    }

    public File getRefFile(DicomObject dicomobject)
        throws DicomException
    {
        int i = dicomobject.getSize(50);
        if(i <= 0)
            return null;
        File file = new File(rootDir, (String)dicomobject.get(50, 0));
        for(int j = 1; j < i; j++)
            file = new File(file, (String)dicomobject.get(50, j));

        return file;
    }

    public File read(DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, DicomException
    {
        File file = getRefFile(dicomobject1);
        if(file != null)
        {
            FileInputStream fileinputstream = new FileInputStream(file);
            try
            {
                dicomobject.read(((java.io.InputStream) (fileinputstream)));
            }
            finally
            {
                fileinputstream.close();
            }
            if(Debug.DEBUG > 0)
                logRead(file);
        }
        return file;
    }

    public File write(DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, DicomException, UnknownUIDException
    {
        File file = getRefFile(dicomobject1);
        if(file != null)
        {
            String s = dicomobject1.getS(54);
            DicomObject dicomobject2 = dicomobject.getFileMetaInformation();
            if(dicomobject2 == null)
            {
                FileMetaInformation filemetainformation = new FileMetaInformation(dicomobject, s);
                dicomobject.setFileMetaInformation(((DicomObject) (filemetainformation)));
            }
            File file1 = new File(file.getParent());
            file1.mkdirs();
            FileOutputStream fileoutputstream = new FileOutputStream(file);
            try
            {
                dicomobject.write(((java.io.OutputStream) (fileoutputstream)), true, UID.getUIDEntry(s).getConstant(), false);
            }
            finally
            {
                fileoutputstream.close();
            }
            if(Debug.DEBUG > 0)
                logWrite(file);
        }
        return file;
    }

    public File delete(DicomObject dicomobject, boolean flag)
        throws IOException, DicomException
    {
        File file = getRefFile(dicomobject);
        if(file != null)
        {
            file.delete();
            if(flag)
            {
                for(File file1 = new File(file.getParent()); file1.list().length == 0; file1 = new File(file1.getParent()))
                    file1.delete();

            }
            if(Debug.DEBUG > 0)
                logDelete(file);
        }
        return file;
    }

    private void logRead(File file)
    {
        log(" << M-READ[", file);
    }

    private void logWrite(File file)
    {
        log(" << M-WRITE[", file);
    }

    private void logDelete(File file)
    {
        log(" << M-DELETE[", file);
    }

    private void log(String s, File file)
    {
        Debug.out.println("jdicom: " + fileSetID + s + file + "]");
    }
}
