// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Dicom2HTML.java

package com.tiani.dicom.util;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.TagValue;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Enumeration;

// Referenced classes of package com.tiani.dicom.util:
//            Tag, ExtDDict

public class Dicom2HTML extends PrintWriter
{

    private static ExtDDict _extDDict = new ExtDDict();

    public Dicom2HTML(OutputStream outputstream)
    {
        super(outputstream);
    }

    public void write(DicomObject dicomobject)
        throws DicomException
    {
        _writeHeader();
        DicomObject dicomobject1 = dicomobject.getFileMetaInformation();
        if(dicomobject1 != null)
            _write("", dicomobject1);
        _write("", dicomobject);
        _writeFooter();
    }

    private void _writeHeader()
    {
        ((PrintWriter)this).println("<HTML>");
        ((PrintWriter)this).println("<HEAD><TITLE> Dicom2HTML Output </TITLE></HEAD>");
        ((PrintWriter)this).println("<BODY>");
        ((PrintWriter)this).println("<TABLE WIDTH=\"100%\" BORDER>");
        ((PrintWriter)this).println("<TR><TH>Tag</TH><TH>Attribute Name</TH><TH>VR</TH><TH>VM</TH><TH>Value</TH></TR>");
    }

    private void _writeFooter()
    {
        ((PrintWriter)this).println("</TABLE>");
        ((PrintWriter)this).println("</BODY>");
        ((PrintWriter)this).println("</HTML>");
    }

    private void _write(String s, DicomObject dicomobject)
        throws DicomException
    {
        Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, true);
        for(int i = 0; enumeration.hasMoreElements(); i++)
        {
            TagValue tagvalue = (TagValue)enumeration.nextElement();
            Tag tag = new Tag(tagvalue.getGroup(), tagvalue.getElement());
            _writeAttribute(s, tag, dicomobject);
            if(tag.isTypeSQ())
            {
                for(int j = 0; j < tagvalue.size(); j++)
                {
                    DicomObject dicomobject1 = (DicomObject)tagvalue.getValue(j);
                    ((PrintWriter)this).println("<TR><TD>" + s + "></TD><TD>ITEM " + j + "</TD></TR>");
                    _write(s + '>', dicomobject1);
                }

            }
        }

    }

    private void _writeAttribute(String s, Tag tag, DicomObject dicomobject)
        throws DicomException
    {
        ((PrintWriter)this).print("<TR><TD>");
        ((PrintWriter)this).print(s);
        ((PrintWriter)this).print(tag.toString());
        ((PrintWriter)this).print("</TD><TD>");
        ((PrintWriter)this).print(tag.getDescription());
        ((PrintWriter)this).print("</TD><TD>");
        ((PrintWriter)this).print(tag.getTypePrompt());
        ((PrintWriter)this).print("</TD><TD>");
        int i = tag.getGroup();
        int j = tag.getElement();
        int k = dicomobject.getSize_ge(i, j);
        if(k == -1)
            ((PrintWriter)this).print('-');
        else
            ((PrintWriter)this).print(k);
        if(!tag.isTypeSQ())
        {
            ((PrintWriter)this).print("</TD><TD>");
            for(int l = 0; l < k; l++)
            {
                if(l != 0)
                    ((PrintWriter)this).print('\\');
                ((PrintWriter)this).print(dicomobject.getS_ge(i, j, l));
            }

        }
        ((PrintWriter)this).println("</TD></TR>");
    }

}
