// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ByteArrayPool.java

package com.tiani.dicom.util;

import com.archimed.dicom.Debug;
import java.io.PrintStream;
import java.util.AbstractSequentialList;
import java.util.Iterator;
import java.util.LinkedList;

public class ByteArrayPool
{

    private static int defaultMaxSize = 0xf4240;
    private static ByteArrayPool instance = null;
    private final int maxSize;
    private int curSize;
    private final LinkedList pool;

    public static ByteArrayPool getInstance()
    {
        if(instance == null)
            synchronized(com.tiani.dicom.util.ByteArrayPool.class)
            {
                if(instance == null)
                    instance = new ByteArrayPool();
            }
        return instance;
    }

    public static void setDefaultMaxSize(int i)
    {
        defaultMaxSize = i;
    }

    public static int getDefaultMaxSize()
    {
        return defaultMaxSize;
    }

    public ByteArrayPool()
    {
        this(defaultMaxSize);
    }

    public ByteArrayPool(int i)
    {
        pool = new LinkedList();
        maxSize = i;
        curSize = 0;
    }

    public byte[] getByteArray(int i)
    {
        byte abyte0[] = remove(i);
        if(Debug.DEBUG > 1)
            Debug.out.println(abyte0 != null ? "jdicom: ByteArrayPool -> reuse byte[" + i + "]" : "jdicom: ByteArrayPool -> new byte[" + i + "]");
        return abyte0 == null ? new byte[i] : abyte0;
    }

    public void release(byte abyte0[])
    {
        if(abyte0.length < maxSize)
            add(abyte0);
    }

    private synchronized byte[] remove(int i)
    {
        byte abyte0[];
        for(Iterator iterator = ((AbstractSequentialList) (pool)).iterator(); iterator.hasNext();)
            if((abyte0 = (byte[])iterator.next()).length == i)
            {
                iterator.remove();
                curSize -= i;
                return abyte0;
            }

        return null;
    }

    private synchronized void add(byte abyte0[])
    {
        pool.addLast(((Object) (abyte0)));
        for(curSize += abyte0.length; curSize > maxSize; curSize -= ((byte[])pool.removeFirst()).length);
    }

}
