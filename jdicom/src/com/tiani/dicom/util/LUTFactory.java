// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LUTFactory.java

package com.tiani.dicom.util;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.io.PrintStream;

// Referenced classes of package com.tiani.dicom.util:
//            LUT, PixelMatrix

public class LUTFactory
{

    public LUTFactory()
    {
    }

    public static short[] toShortLUTData(byte abyte0[], int i)
        throws DicomException
    {
        short aword0[] = new short[i];
        if(abyte0.length == i)
        {
            for(int j = 0; j < i; j++)
                aword0[j] = (short)(abyte0[j] & 0xff);

            return aword0;
        }
        if(abyte0.length == i << 1)
        {
            int k = 0;
            for(int l = 0; l < i; l++)
                aword0[l] = (short)(abyte0[k++] & 0xff | abyte0[k++] << 8);

            return aword0;
        } else
        {
            throw new DicomException("LUT Data length does not match LUT Descriptor");
        }
    }

    public static int[] calcMinMax(DicomObject dicomobject)
        throws DicomException
    {
        byte abyte0[] = (byte[])dicomobject.get(509);
        if(abyte0 == null)
            throw new DicomException("Missing Modality LUT data");
        if(dicomobject.getSize(506) != 3)
            throw new DicomException("Missing or Invalid LUT Descriptor");
        int i = dicomobject.getI(506, 0);
        if(i == 0)
            i = 0x10000;
        if(abyte0.length == i)
            return (new int[] {
                abyte0[0] & 0xff, abyte0[i - 1]
            });
        if(abyte0.length == i << 1)
            return (new int[] {
                abyte0[0] & 0xff | (abyte0[1] & 0xff) << 8, abyte0[abyte0.length - 2] & 0xff | (abyte0[abyte0.length - 1] & 0xff) << 8
            });
        else
            throw new DicomException("LUT Data length does not match LUT Descriptor");
    }

    public static LUT createLUT(DicomObject dicomobject)
        throws DicomException
    {
        if(dicomobject == null)
            return null;
        if(dicomobject.getSize(506) != 3)
            throw new DicomException("Missing or Invalid LUT Descriptor");
        int i = dicomobject.getI(506, 0);
        if(i == 0)
            i = 0x10000;
        int j = dicomobject.getI(506, 1);
        if((j & 0xffff8000) != 0)
            j |= 0xffff8000;
        int k = dicomobject.getI(506, 2);
        int l = (1 << k) - 1;
        short aword0[] = toShortLUTData((byte[])dicomobject.get(509), i);
        return ((LUT) (new LUT.Short(j, aword0, l)));
    }

    public static LUT.Byte1 createByteLUT(int i, int j, int k, boolean flag, DicomObject dicomobject, DicomObject dicomobject1, DicomObject dicomobject2)
        throws DicomException
    {
        LUT lut = prepareLUT(i, j, k, flag, dicomobject, dicomobject1, dicomobject2);
        return lut.rescaleToByte(k);
    }

    public static LUT.Short createShortLUT(int i, int j, int k, boolean flag, DicomObject dicomobject, DicomObject dicomobject1, DicomObject dicomobject2)
        throws DicomException
    {
        LUT lut = prepareLUT(i, j, k, flag, dicomobject, dicomobject1, dicomobject2);
        return lut.rescaleToShort(k);
    }

    private static LUT prepareLUT(int i, int j, int k, boolean flag, DicomObject dicomobject, DicomObject dicomobject1, DicomObject dicomobject2)
        throws DicomException
    {
        int l = (j - i) + 1;
        int i1 = k;
        Float float1 = null;
        Float float2 = null;
        Float float3 = null;
        Float float4 = null;
        LUT alut[] = new LUT[4];
        if(dicomobject != null)
        {
            alut[0] = createLUT((DicomObject)dicomobject.get(505));
            if(alut[0] == null)
            {
                float1 = (Float)dicomobject.get(490);
                float2 = (Float)dicomobject.get(489);
                if(float1 != null ? float2 == null : float2 != null)
                {
                    Debug.out.println("jdicom: WARNING: Missing Rescale component");
                    float1 = float2 = null;
                }
                if(Debug.DEBUG > 1 && float1 != null)
                    Debug.out.println("jdicom: Apply Modality LUT: Rescale Slope=" + float1 + ", Rescale Intercept=" + float2);
            } else
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: Apply Modality LUT: " + alut[0]);
        }
        if(dicomobject1 != null)
        {
            alut[1] = createLUT((DicomObject)dicomobject1.get(510));
            if(alut[1] == null)
            {
                float3 = (Float)dicomobject1.get(487);
                float4 = (Float)dicomobject1.get(488);
                if(float3 != null ? float4 == null : float4 != null)
                {
                    Debug.out.println("jdicom: WARNING: Missing VOI Window component");
                    float3 = float4 = null;
                }
                if(Debug.DEBUG > 1 && float3 != null)
                    Debug.out.println("jdicom: Apply VOI LUT: Window Center=" + float3 + ", Window Width=" + float4);
            } else
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: Apply VOI LUT: " + alut[1]);
        }
        if(dicomobject2 != null)
        {
            alut[3] = createLUT((DicomObject)dicomobject2.get(1239));
            if(alut[3] != null)
            {
                if(alut[3].offset() != 0)
                    throw new DicomException("Presentation LUT offset not zero - " + alut[3].offset());
                if(flag)
                    alut[3].reverse();
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: Apply Presentation LUT: " + alut[3] + ", inverse=" + flag);
                i1 = alut[3].length() - 1;
                flag = false;
            } else
            {
                String s = (String)dicomobject2.get(1240);
                if(s == null)
                    throw new DicomException("Missing Presentation LUT Shape Attribute");
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: Apply Presentation LUT: " + s);
                if(s.equals("INVERSE"))
                    flag = !flag;
                else
                if(!s.equals("IDENTITY"))
                    throw new DicomException("Presentation LUT Shape " + s + " not supported");
            }
        }
        if(float3 != null)
        {
            float f = float3.floatValue();
            float f1 = float4.floatValue();
            if(float1 != null)
            {
                float f2 = float1.floatValue();
                if(f2 <= 0.0F)
                    throw new DicomException("Rescale Slope of " + f2 + " not supported");
                f -= float2.floatValue();
                f /= f2;
                f1 /= f2;
            }
            int l1 = (int)(f - f1 / 2.0F);
            int i2 = Math.max(1, (int)f1);
            alut[1] = LUT.createLUT(i2, l1, 0, i1, flag);
            if(Debug.DEBUG > 2)
                Debug.out.println("jdicom: Create Ramp for VOI LUT: " + alut[1] + ", inverse=" + flag);
            flag = false;
        } else
        {
            int j1 = i;
            int k1 = l;
            LUT lut1 = alut[1] == null ? alut[0] : alut[1];
            if(alut[3] != null)
            {
                int j2 = alut[3].length();
                if(lut1 != null)
                {
                    j1 = 0;
                    k1 = lut1.maxY() + 1;
                }
                if(j1 != 0 || k1 != j2)
                {
                    alut[2] = LUT.createLUT(k1, j1, 0, i1);
                    if(Debug.DEBUG > 2)
                        Debug.out.println("jdicom: Create Scaling Ramp for Presentation LUT: " + alut[2]);
                }
            } else
            if(lut1 == null)
            {
                alut[0] = LUT.createLUT(l, i, 0, k, flag);
                if(Debug.DEBUG > 2)
                    Debug.out.println("jdicom: Create Ramp LUT: " + alut[0] + ", inverse=" + flag);
                flag = false;
            }
        }
        LUT lut = LUT.join(alut);
        if(flag)
        {
            lut.inverse();
            if(Debug.DEBUG > 2)
                Debug.out.println("jdicom: Inverse LUT: " + lut);
        }
        return lut;
    }

    public static LUT.Byte1 createByteLUT(PixelMatrix pixelmatrix, DicomObject dicomobject)
        throws DicomException
    {
        if(!pixelmatrix.isMonochrome())
        {
            throw new IllegalArgumentException();
        } else
        {
            DicomObject dicomobject1 = (dicomobject.getSize(487) <= 0 || dicomobject.getSize(488) <= 0) && dicomobject.getSize(510) <= 0 ? createVOILUT(pixelmatrix, dicomobject) : dicomobject;
            boolean flag = pixelmatrix.isMonochrome1();
            return dicomobject1 == null && !flag ? null : createByteLUT(pixelmatrix.getMinVal(), pixelmatrix.getMaxVal(), 255, flag, isXRaySOPClass(dicomobject) ? null : dicomobject, dicomobject1, ((DicomObject) (null)));
        }
    }

    public static boolean isXRaySOPClass(DicomObject dicomobject)
    {
        return isXRaySOPClass((String)dicomobject.get(62));
    }

    public static boolean isXRaySOPClass(String s)
    {
        return "1.2.840.10008.5.1.4.1.1.12.1".equals(((Object) (s))) || "1.2.840.10008.5.1.4.1.1.12.2".equals(((Object) (s))) || "1.2.840.10008.5.1.4.1.1.12.3".equals(((Object) (s)));
    }

    static DicomObject createVOILUT(PixelMatrix pixelmatrix, DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = (DicomObject)dicomobject.get(505);
        int ai[] = dicomobject1 == null ? pixelmatrix.calcMinMax() : calcMinMax(dicomobject1);
        float f = (float)(ai[1] + ai[0]) / 2.0F;
        float f1 = (ai[1] - ai[0]) + 1;
        Float float1 = (Float)dicomobject.get(490);
        Float float2 = (Float)dicomobject.get(489);
        if(float1 != null && float2 != null)
        {
            f = f * float1.floatValue() + float2.floatValue();
            f1 = Math.abs(f1 * float1.floatValue());
        }
        DicomObject dicomobject2 = new DicomObject();
        dicomobject2.set(487, ((Object) (new Float(f))));
        dicomobject2.set(488, ((Object) (new Float(f1))));
        if(Debug.DEBUG > 1)
            Debug.out.println("jdicom: Generate VOI LUT: c=" + f + ", w=" + f1);
        return dicomobject2;
    }
}
