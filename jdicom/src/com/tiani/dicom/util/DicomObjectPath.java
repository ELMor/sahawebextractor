// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DicomObjectPath.java

package com.tiani.dicom.util;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.TagValue;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;

/**
 * @deprecated Class DicomObjectPath is deprecated
 */

public class DicomObjectPath extends Vector
{

    public DicomObjectPath()
    {
    }

    public DicomObjectPath(DicomObject dicomobject)
    {
        super(1);
        ((Vector)this).addElement(((Object) (dicomobject)));
    }

    public DicomObjectPath(DicomObject dicomobject, DicomObjectPath dicomobjectpath)
    {
        super(dicomobjectpath == null ? 1 : ((Vector) (dicomobjectpath)).size() + 1);
        ((Vector)this).addElement(((Object) (dicomobject)));
        if(dicomobjectpath != null)
        {
            for(Enumeration enumeration = ((Vector) (dicomobjectpath)).elements(); enumeration.hasMoreElements(); ((Vector)this).addElement(enumeration.nextElement()));
        }
    }

    public void add(DicomObject dicomobject)
    {
        ((Vector)this).addElement(((Object) (dicomobject)));
    }

    public DicomObject first()
    {
        return (DicomObject)((Vector)this).firstElement();
    }

    public DicomObject last()
    {
        return (DicomObject)((Vector)this).lastElement();
    }

    public DicomObject getAt(int i)
    {
        return (DicomObject)((Vector)this).elementAt(i);
    }

    public DicomObjectPath remaining()
    {
        DicomObjectPath dicomobjectpath = (DicomObjectPath)((Vector)this).clone();
        ((Vector) (dicomobjectpath)).removeElementAt(0);
        return dicomobjectpath;
    }

    public DicomObject getComposite(boolean flag)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        for(Enumeration enumeration = ((Vector)this).elements(); enumeration.hasMoreElements(); _addDicomObject((DicomObject)enumeration.nextElement(), dicomobject, flag));
        return dicomobject;
    }

    private static void _addDicomObject(DicomObject dicomobject, DicomObject dicomobject1, boolean flag)
        throws DicomException
    {
        for(Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, false); enumeration.hasMoreElements(); _addTagValue((TagValue)enumeration.nextElement(), dicomobject1, flag));
    }

    private static void _addTagValue(TagValue tagvalue, DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        int i = tagvalue.getGroup();
        int j = tagvalue.getElement();
        if((i & 1) != 0)
        {
            Debug.out.println("jdicom: [WARN] skipping private attribute (" + Integer.toHexString(i) + "," + Integer.toHexString(j) + ")");
            return;
        }
        if(!flag)
            dicomobject.deleteItem_ge(i, j);
        int k = tagvalue.size();
        for(int l = 0; l < k; l++)
            dicomobject.append_ge(i, j, tagvalue.getValue(l));

    }
}
