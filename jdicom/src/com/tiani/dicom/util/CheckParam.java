// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CheckParam.java

package com.tiani.dicom.util;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.tiani.dicom.util:
//            CheckIntParam, CheckDoubleParam, CheckEnumParam, CheckMultiEnumParam, 
//            DefinedParam, URLParam

public class CheckParam
{

    private static final String TRUE_FALSE[] = {
        "true", "false"
    };
    private static final CheckParam _BOOL = new CheckEnumParam(TRUE_FALSE, 1);
    protected final int _type;

    public CheckParam(int i)
    {
        _type = i;
    }

    public void check(String s)
        throws IllegalArgumentException
    {
        if(s == null)
            if(_type <= 2)
                throw new IllegalArgumentException("missing parameter");
            else
                return;
        if(s.length() == 0 && _type == 1)
            throw new IllegalArgumentException("missing value");
        else
            return;
    }

    public String[] getTags()
    {
        return null;
    }

    public boolean isMulti()
    {
        return false;
    }

    public static CheckParam bool()
    {
        return _BOOL;
    }

    public static CheckParam range(int i, int j)
    {
        return ((CheckParam) (new CheckIntParam(i, j, 1)));
    }

    public static CheckParam range(int i, int j, int k)
    {
        return ((CheckParam) (new CheckIntParam(i, j, k)));
    }

    public static CheckParam range(double d, double d1)
    {
        return ((CheckParam) (new CheckDoubleParam(d, d1, 1)));
    }

    public static CheckParam range(double d, double d1, int i)
    {
        return ((CheckParam) (new CheckDoubleParam(d, d1, i)));
    }

    public static CheckParam enum(String as[])
    {
        return ((CheckParam) (new CheckEnumParam(as, 1)));
    }

    public static CheckParam multiEnum(String as[])
    {
        return ((CheckParam) (new CheckMultiEnumParam(as)));
    }

    public static CheckParam enum(String as[], int i)
    {
        return ((CheckParam) (new CheckEnumParam(as, i)));
    }

    public static CheckParam defined(String as[])
    {
        return ((CheckParam) (new DefinedParam(as, 1)));
    }

    public static CheckParam defined(String as[], int i)
    {
        return ((CheckParam) (new DefinedParam(as, i)));
    }

    public static CheckParam url()
    {
        return ((CheckParam) (new URLParam(1)));
    }

    public static CheckParam url(int i)
    {
        return ((CheckParam) (new URLParam(i)));
    }

    public static void verify(Properties properties, Hashtable hashtable)
        throws IllegalArgumentException
    {
        for(Enumeration enumeration = hashtable.keys(); enumeration.hasMoreElements();)
        {
            String s = (String)enumeration.nextElement();
            CheckParam checkparam = (CheckParam)hashtable.get(((Object) (s)));
            try
            {
                checkparam.check(properties.getProperty(s));
            }
            catch(IllegalArgumentException illegalargumentexception)
            {
                throw new IllegalArgumentException(s + ": " + illegalargumentexception);
            }
        }

    }

}
