// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RelationStringFilter.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            IStringFilter

public class RelationStringFilter
    implements IStringFilter
{

    public static final int EQ = 0;
    public static final int GE = 1;
    public static final int GT = 2;
    public static final int LE = 3;
    public static final int LT = 4;
    public static final int NE = 5;
    private String _val;
    private int _rel;

    public RelationStringFilter(String s, int i)
    {
        _val = s;
        _rel = i;
    }

    public boolean accept(String s)
    {
        int i = s.compareTo(_val);
        switch(_rel)
        {
        case 0: // '\0'
            return i == 0;

        case 5: // '\005'
            return i != 0;

        case 4: // '\004'
            return i < 0;

        case 3: // '\003'
            return i <= 0;

        case 2: // '\002'
            return i > 0;

        case 1: // '\001'
            return i >= 0;
        }
        return false;
    }
}
