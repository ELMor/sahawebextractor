// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CheckParam.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            CheckParam

class CheckIntParam extends CheckParam
{

    private int _min;
    private int _max;

    public CheckIntParam(int i, int j, int k)
    {
        super(k);
        if(i > j)
        {
            throw new IllegalArgumentException("not a valid intervall");
        } else
        {
            _min = i;
            _max = j;
            return;
        }
    }

    public void check(String s)
        throws IllegalArgumentException
    {
        super.check(s);
        if(s == null || s.length() == 0)
            return;
        int i = Integer.parseInt(s);
        if(i < _min || i > _max)
            throw new IllegalArgumentException("outside [" + _min + ',' + _max + ']');
        else
            return;
    }
}
