// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CheckParam.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            DefinedParam

class CheckEnumParam extends DefinedParam
{

    public CheckEnumParam(String as[], int i)
    {
        super(as, i);
    }

    public void check(String s)
        throws IllegalArgumentException
    {
        super.check(s);
        if(s == null || s.length() == 0)
            return;
        for(int i = 0; i < super._tags.length; i++)
            if(super._tags[i].equals(((Object) (s))))
                return;

        throw new IllegalArgumentException(errMsg());
    }

    private String errMsg()
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("not one of {");
        stringbuffer.append(super._tags[0]);
        for(int i = 1; i < super._tags.length; i++)
        {
            stringbuffer.append(',');
            stringbuffer.append(super._tags[i]);
        }

        stringbuffer.append('}');
        return stringbuffer.toString();
    }
}
