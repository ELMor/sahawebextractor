// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AndStringFilter.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            IStringFilter

public class AndStringFilter
    implements IStringFilter
{

    private IStringFilter _filter[];

    public AndStringFilter(IStringFilter aistringfilter[])
    {
        _filter = aistringfilter;
    }

    public boolean accept(String s)
    {
        for(int i = 0; i < _filter.length; i++)
            if(!_filter[i].accept(s))
                return false;

        return true;
    }
}
