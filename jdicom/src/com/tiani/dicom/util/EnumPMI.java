// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EnumPMI.java

package com.tiani.dicom.util;


public class EnumPMI
{

    public static final int MONOCHROME1 = 0;
    public static final int MONOCHROME2 = 1;
    public static final int PALETTE_COLOR = 2;
    public static final int RGB = 3;
    public static final int HSV = 4;
    public static final int ARGB = 5;
    public static final int CMYK = 6;
    public static final int YBR_FULL = 7;
    public static final int YBR_FULL_422 = 8;
    public static final int YBR_PARTIAL_422 = 9;
    public static final int OTHER = -1;
    public static final int SAMPLES[] = {
        1, 1, 1, 3, 3, 4, 4, 4, 4, 4
    };
    public static final String TO_STRING[] = {
        "MONOCHROME1", "MONOCHROME2", "PALETTE COLOR", "RGB", "HSV", "ARGB", "CMYK", "YBR_FULL", "YBR_FULL_422", "YBR_PARTIAL_422"
    };

    public EnumPMI()
    {
    }

    public static int getConstant(String s)
    {
        int i;
        for(i = TO_STRING.length; --i >= 0;)
            if(TO_STRING[i].equals(((Object) (s))))
                break;

        return i;
    }

    public static boolean isGrayscale(String s)
    {
        return "MONOCHROME1".equals(((Object) (s))) || "MONOCHROME2".equals(((Object) (s)));
    }

    public static boolean isGrayscale(int i)
    {
        return i == 0 || i == 1;
    }

    public static boolean isMonochrome1(String s)
    {
        return "MONOCHROME1".equals(((Object) (s)));
    }

    public static boolean isMonochrome1(int i)
    {
        return i == 0;
    }

}
