// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AETable.java

package com.tiani.dicom.util;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.util:
//            AET, IAETable

public class AETable extends Properties
    implements IAETable
{

    public AETable()
    {
    }

    public AETable(Properties properties)
    {
        super(properties);
    }

    public AET lookup(String s)
    {
        try
        {
            String s1 = getHost(s);
            if(s1 != null)
                return new AET(s, s1, getPort(s));
        }
        catch(Exception exception) { }
        return null;
    }

    public final String getHost(String s)
    {
        return ((Properties)this).getProperty(s + ".host");
    }

    public final int getPort(String s)
    {
        return Integer.parseInt(((Properties)this).getProperty(s + ".port"));
    }

    public void add(AET aet)
    {
        ((Hashtable)this).put(((Object) (aet.title + ".host")), ((Object) (aet.host)));
        ((Hashtable)this).put(((Object) (aet.title + ".port")), ((Object) ("" + aet.port)));
    }

    public AET removeAET(String s)
    {
        AET aet = lookup(s);
        if(s != null)
        {
            ((Hashtable)this).remove(((Object) (s + ".host")));
            ((Hashtable)this).remove(((Object) (s + ".port")));
        }
        return aet;
    }

    public String[] list()
    {
        Vector vector = new Vector();
        for(Enumeration enumeration = ((Hashtable)this).keys(); enumeration.hasMoreElements();)
        {
            String s = (String)enumeration.nextElement();
            if(s.endsWith(".host"))
                try
                {
                    String s1;
                    getPort(s1 = s.substring(0, s.length() - 5));
                    vector.addElement(((Object) (s1)));
                }
                catch(Exception exception) { }
        }

        String as[] = new String[vector.size()];
        vector.copyInto(((Object []) (as)));
        return as;
    }
}
