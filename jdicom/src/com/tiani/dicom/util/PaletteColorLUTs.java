// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PaletteColorLUTs.java

package com.tiani.dicom.util;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.io.PrintStream;

public class PaletteColorLUTs
{

    private final DicomObject dcmObj;

    public PaletteColorLUTs(DicomObject dicomobject)
    {
        dcmObj = dicomobject;
    }

    public byte[] getRedPaletteColorLUTData16Bit()
        throws DicomException
    {
        return getPaletteColorLUTData16Bit(494, 498, 501);
    }

    public byte[] getGreenPaletteColorLUTData16Bit()
        throws DicomException
    {
        return getPaletteColorLUTData16Bit(495, 499, 502);
    }

    public byte[] getBluePaletteColorLUTData16Bit()
        throws DicomException
    {
        return getPaletteColorLUTData16Bit(496, 500, 503);
    }

    private byte[] getPaletteColorLUTData16Bit(int i, int j, int k)
        throws DicomException
    {
        if(dcmObj.getSize(i) != 3)
            return null;
        int l = dcmObj.getI(i, 2);
        if(l != 16)
            throw new DicomException("bits:" + l);
        int i1 = dcmObj.getI(i, 1);
        if(i1 < 0)
            throw new DicomException("off:" + i1);
        byte abyte0[] = (byte[])dcmObj.get(j);
        if(abyte0 != null && i1 == 0)
            return abyte0;
        int j1 = dcmObj.getI(i);
        if(j1 == 0)
            j1 = 0x10000;
        byte abyte1[] = new byte[i1 + j1 << 1];
        int k1 = i1 << 1;
        if(abyte0 != null)
            System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), k1, abyte0.length);
        else
            inflateSegmentsTo16Bit((byte[])dcmObj.get(k), abyte1, k1);
        for(int l1 = 0; l1 < k1;)
        {
            abyte1[l1++] = abyte1[k1];
            abyte1[l1++] = abyte1[k1 + 1];
        }

        return abyte1;
    }

    public static void inflateSegmentsTo16Bit(byte abyte0[], byte abyte1[], int i)
        throws DicomException
    {
        if(Debug.DEBUG > 2)
            Debug.out.println("jdicom: inflateSegmentsTo16Bit(src[" + abyte0.length + "],dst[" + abyte1.length + ",off2:" + i);
        int j = i;
        int k = 0;
        for(int j2 = 0; j2 < abyte0.length;)
        {
            int k2 = abyte0[j2++] & 0xff | (abyte0[j2++] & 0xff) << 8;
            int l2 = abyte0[j2++] & 0xff | (abyte0[j2++] & 0xff) << 8;
            switch(k2)
            {
            case 0: // '\0'
                if(Debug.DEBUG > 3)
                    Debug.out.println("jdicom: segm@" + j2 + " " + j + "[+" + l2 + "]: Discrete");
                for(int i3 = 0; i3 < l2; i3++)
                {
                    abyte1[j++] = abyte0[j2++];
                    abyte1[j++] = abyte0[j2++];
                }

                k = abyte0[j2 - 2] & 0xff | (abyte0[j2 - 1] & 0xff) << 8;
                break;

            case 1: // '\001'
                int l = abyte0[j2++] & 0xff | (abyte0[j2++] & 0xff) << 8;
                if(Debug.DEBUG > 3)
                    Debug.out.println("jdicom: segm@" + j2 + " " + j + "-" + (j + l2) + ": @Linear " + k + "-" + l);
                int l1 = l - k;
                for(int j3 = 0; j3 < l2;)
                {
                    int j1 = k + (l1 * ++j3) / l2;
                    abyte1[j++] = (byte)j1;
                    abyte1[j++] = (byte)(j1 >> 8);
                }

                k = l;
                break;

            case 2: // '\002'
                int k3 = abyte0[j2++] & 0xff | (abyte0[j2++] & 0xff) << 8 | (abyte0[j2++] & 0xff) << 16 | (abyte0[j2++] & 0xff) << 24;
                if(Debug.DEBUG > 3)
                    Debug.out.println("jdicom: segm@" + j2 + " " + j + "{+" + l2 + "}: Indirect @" + k3);
                for(int l3 = 0; l3 < l2; l3++)
                {
                    int i4 = abyte0[k3++] & 0xff | (abyte0[k3++] & 0xff) << 8;
                    int j4 = abyte0[k3++] & 0xff | (abyte0[k3++] & 0xff) << 8;
                    switch(i4)
                    {
                    case 0: // '\0'
                        if(Debug.DEBUG > 3)
                            Debug.out.println("jdicom: segm@" + k3 + " " + j + "-" + (j + j4) + ": @Discrete");
                        for(int k4 = 0; k4 < j4; k4++)
                        {
                            abyte1[j++] = abyte0[k3++];
                            abyte1[j++] = abyte0[k3++];
                        }

                        k = abyte0[k3 - 2] & 0xff | (abyte0[k3 - 1] & 0xff) << 8;
                        break;

                    case 1: // '\001'
                        int i1 = abyte0[k3++] & 0xff | (abyte0[k3++] & 0xff) << 8;
                        if(Debug.DEBUG > 3)
                            Debug.out.println("jdicom: segm@" + k3 + " " + j + "[+" + j4 + "]: @Linear " + k + "-" + i1);
                        int i2 = i1 - k;
                        for(int l4 = 0; l4 < j4;)
                        {
                            int k1 = k + (i2 * ++l4) / j4;
                            abyte1[j++] = (byte)k1;
                            abyte1[j++] = (byte)(k1 >> 8);
                        }

                        k = i1;
                        break;

                    default:
                        throw new DicomException("illegal op code:" + i4 + ", index:" + (k3 - 4));
                    }
                }

                break;

            default:
                throw new DicomException("illegal op code:" + k2 + ", index:" + (j2 - 4));
            }
        }

    }

    public byte[] getRedPaletteColorLUTData8Bit()
        throws DicomException
    {
        return getPaletteColorLUTData8Bit(494, 498, 501);
    }

    public byte[] getGreenPaletteColorLUTData8Bit()
        throws DicomException
    {
        return getPaletteColorLUTData8Bit(495, 499, 502);
    }

    public byte[] getBluePaletteColorLUTData8Bit()
        throws DicomException
    {
        return getPaletteColorLUTData8Bit(496, 500, 503);
    }

    private byte[] getPaletteColorLUTData8Bit(int i, int j, int k)
        throws DicomException
    {
        if(dcmObj.getSize(i) != 3)
            return null;
        int l = dcmObj.getI(i, 2);
        if(l != 16)
            throw new DicomException("bits:" + l);
        int i1 = dcmObj.getI(i, 1);
        if(i1 < 0)
            throw new DicomException("off:" + i1);
        byte abyte0[] = to8Bit((byte[])dcmObj.get(j));
        if(abyte0 != null && i1 == 0)
            return abyte0;
        int j1 = dcmObj.getI(i);
        if(j1 == 0)
            j1 = 0x10000;
        byte abyte1[] = new byte[i1 + j1];
        if(abyte0 != null)
            System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), i1, abyte0.length);
        else
            inflateSegmentsTo8Bit((byte[])dcmObj.get(k), abyte1, i1);
        for(int k1 = 0; k1 < i1;)
            abyte1[k1++] = abyte1[i1];

        return abyte1;
    }

    public static void inflateSegmentsTo8Bit(byte abyte0[], byte abyte1[], int i)
        throws DicomException
    {
        if(Debug.DEBUG > 2)
            Debug.out.println("jdicom: inflateSegmentsTo8Bit(src[" + abyte0.length + "],dst[" + abyte1.length + ",off:" + i);
        int j = i;
        int k = 0;
        for(int l1 = 0; l1 < abyte0.length;)
        {
            int i2 = abyte0[l1++] & 0xff | (abyte0[l1++] & 0xff) << 8;
            int j2 = abyte0[l1++] & 0xff | (abyte0[l1++] & 0xff) << 8;
            switch(i2)
            {
            case 0: // '\0'
                if(Debug.DEBUG > 3)
                    Debug.out.println("jdicom: segm@" + l1 + " " + j + "[+" + j2 + "]: Discrete");
                for(int k2 = 0; k2 < j2;)
                {
                    abyte1[j++] = abyte0[++l1];
                    k2++;
                    l1++;
                }

                k = abyte0[l1 - 2] & 0xff | (abyte0[l1 - 1] & 0xff) << 8;
                break;

            case 1: // '\001'
                int l = abyte0[l1++] & 0xff | (abyte0[l1++] & 0xff) << 8;
                if(Debug.DEBUG > 3)
                    Debug.out.println("jdicom: segm@" + l1 + " " + j + "[+" + j2 + "]: Linear " + k + "-" + l);
                int j1 = l - k;
                for(int l2 = 0; l2 < j2;)
                    abyte1[j++] = (byte)(k + (j1 * ++l2) / j2 >> 8);

                k = l;
                break;

            case 2: // '\002'
                int i3 = abyte0[l1++] & 0xff | (abyte0[l1++] & 0xff) << 8 | (abyte0[l1++] & 0xff) << 16 | (abyte0[l1++] & 0xff) << 24;
                if(Debug.DEBUG > 3)
                    Debug.out.println("jdicom: segm@" + l1 + " " + j + "{+" + j2 + "}: Indirect @" + i3);
                for(int j3 = 0; j3 < j2; j3++)
                {
                    int k3 = abyte0[i3++] & 0xff | (abyte0[i3++] & 0xff) << 8;
                    int l3 = abyte0[i3++] & 0xff | (abyte0[i3++] & 0xff) << 8;
                    switch(k3)
                    {
                    case 0: // '\0'
                        if(Debug.DEBUG > 3)
                            Debug.out.println("jdicom: segm@" + i3 + " " + j + "[+" + l3 + "]: @Discrete");
                        for(int i4 = 0; i4 < l3;)
                        {
                            abyte1[j++] = abyte0[++i3];
                            i4++;
                            i3++;
                        }

                        k = abyte0[i3 - 2] & 0xff | (abyte0[i3 - 1] & 0xff) << 8;
                        break;

                    case 1: // '\001'
                        int i1 = abyte0[i3++] & 0xff | (abyte0[i3++] & 0xff) << 8;
                        if(Debug.DEBUG > 3)
                            Debug.out.println("jdicom: segm@" + i3 + " " + j + "[+" + l3 + "]: @Linear " + k + "-" + i1);
                        int k1 = i1 - k;
                        for(int j4 = 0; j4 < l3;)
                            abyte1[j++] = (byte)(k + (k1 * ++j4) / l3 >> 8);

                        k = i1;
                        break;

                    default:
                        throw new DicomException("illegal op code:" + k3 + ", index:" + (i3 - 4));
                    }
                }

                break;

            default:
                throw new DicomException("illegal op code:" + i2 + ", index:" + (l1 - 4));
            }
        }

    }

    private static byte[] to8Bit(byte abyte0[])
    {
        if(abyte0 == null)
            return null;
        byte abyte1[] = new byte[abyte0.length >> 1];
        for(int i = 0; i < abyte1.length; i++)
            abyte1[i] = abyte0[(i << 1) + 1];

        return abyte1;
    }
}
