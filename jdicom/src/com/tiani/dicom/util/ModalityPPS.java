// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ModalityPPS.java

package com.tiani.dicom.util;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class ModalityPPS
{

    public static final String MPPS_UID = "1.2.840.10008.3.1.2.3.3";
    public static final String IN_PROGRESS = "IN PROGRESS";
    public static final String DISCONTINUED = "DISCONTINUED";
    public static final String COMPLETED = "COMPLETED";
    public static final int NCREATE_REQ_VALUE[] = {
        1210, 1206, 1198, 1201, 1202, 1205, 81
    };
    public static final int NCREATE_REQ_ATTRIB[] = {
        1210, 1206, 1198, 1201, 1202, 1205, 81, 147, 148, 150, 
        152, 110, 1199, 1200, 1207, 1208, 96, 1203, 1204, 427, 
        1209, 1227
    };
    public static final int NCREATE_SSA_SQ_REQ_VALUE[] = {
        425
    };
    public static final int NCREATE_SSA_SQ_REQ_ATTRIB[] = {
        425, 107, 77, 589, 547, 582, 580, 581
    };
    public static final int NSET_NOT_ALLOWED[] = {
        1210, 1206, 1198, 1201, 1202, 147, 148, 150, 152, 110, 
        1199, 1200, 81, 427
    };
    public static final int NSET_FINAL_REQ_VALUE[] = {
        1203, 1204, 1227
    };
    public static final int NSET_FINAL_PS_SQ_REQ_VALUE[] = {
        239, 426
    };
    public static final int NSET_FINAL_PS_REQ_ATTRIB[] = {
        100, 239, 102, 426, 97, 79
    };

    public ModalityPPS()
    {
    }

    public static void initCreateMPPS(DicomObject dicomobject, String s, String s1, String s2, String s3, String s4, String s5)
        throws DicomException
    {
        dicomobject.set(1210, ((Object) (createSSAttributes(s))));
        initCreateMPPS2(dicomobject, s1, s2, s3, s4, s5);
    }

    public static void initCreateMPPS(DicomObject dicomobject, DicomObject adicomobject[], String s, String s1, String s2, String s3, String s4)
        throws DicomException
    {
        for(int i = 0; i < adicomobject.length; i++)
            dicomobject.append(1210, ((Object) (createSSAttributes(adicomobject[i]))));

        initCreateMPPS2(dicomobject, s, s1, s2, s3, s4);
    }

    private static void initCreateMPPS2(DicomObject dicomobject, String s, String s1, String s2, String s3, String s4)
        throws DicomException
    {
        dicomobject.set(1206, ((Object) (s)));
        dicomobject.set(1198, ((Object) (s1)));
        dicomobject.set(1201, ((Object) (s2)));
        dicomobject.set(1202, ((Object) (s3)));
        dicomobject.set(1205, "IN PROGRESS");
        dicomobject.set(81, ((Object) (s4)));
        dicomobject.set(147, ((Object) (null)));
        dicomobject.set(148, ((Object) (null)));
        dicomobject.set(150, ((Object) (null)));
        dicomobject.set(152, ((Object) (null)));
        dicomobject.set(110, ((Object) (null)));
        dicomobject.set(1199, ((Object) (null)));
        dicomobject.set(1200, ((Object) (null)));
        dicomobject.set(1207, ((Object) (null)));
        dicomobject.set(1208, ((Object) (null)));
        dicomobject.set(96, ((Object) (null)));
        dicomobject.set(1203, ((Object) (null)));
        dicomobject.set(1204, ((Object) (null)));
        dicomobject.set(427, ((Object) (null)));
        dicomobject.set(1209, ((Object) (null)));
        dicomobject.set(1227, ((Object) (null)));
    }

    public static DicomObject createCreateMPPS(DicomObject adicomobject[], String s, String s1, String s2, String s3, String s4)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        initCreateMPPS(dicomobject, adicomobject, s, s1, s2, s3, s4);
        return dicomobject;
    }

    public static DicomObject createCreateMPPS(String s, String s1, String s2, String s3, String s4, String s5)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        initCreateMPPS(dicomobject, s, s1, s2, s3, s4, s5);
        return dicomobject;
    }

    public static void initSSAttributes(DicomObject dicomobject, String s)
        throws DicomException
    {
        dicomobject.set(425, ((Object) (s)));
        dicomobject.set(107, ((Object) (null)));
        dicomobject.set(77, ((Object) (null)));
        dicomobject.set(589, ((Object) (null)));
        dicomobject.set(547, ((Object) (null)));
        dicomobject.set(582, ((Object) (null)));
        dicomobject.set(580, ((Object) (null)));
        dicomobject.set(581, ((Object) (null)));
    }

    public static void initSSAttributes(DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        dicomobject.set(425, dicomobject1.get(425));
        dicomobject.set(107, ((Object) (null)));
        if(dicomobject1.getSize(107) > 0)
        {
            DicomObject dicomobject2 = (DicomObject)dicomobject1.get(107);
            DicomObject dicomobject4 = new DicomObject();
            dicomobject4.set(115, dicomobject2.get(115));
            dicomobject4.set(116, dicomobject2.get(116));
            dicomobject.set(107, ((Object) (dicomobject4)));
        }
        dicomobject.set(77, dicomobject1.get(77));
        dicomobject.set(589, dicomobject1.get(589));
        dicomobject.set(547, dicomobject1.get(547));
        if(dicomobject1.getSize(1419) > 0)
            dicomobject.set(1419, dicomobject1.get(1419));
        if(dicomobject1.getSize(1420) > 0)
            dicomobject.set(1420, dicomobject1.get(1420));
        dicomobject.set(582, ((Object) (null)));
        dicomobject.set(580, ((Object) (null)));
        dicomobject.set(581, ((Object) (null)));
        if(dicomobject1.getSize(587) > 0)
        {
            DicomObject dicomobject3 = (DicomObject)dicomobject1.get(587);
            dicomobject.set(582, dicomobject3.get(582));
            dicomobject.set(580, dicomobject3.get(580));
            int i = dicomobject3.getSize(581);
            for(int j = 0; j < i; j++)
            {
                DicomObject dicomobject5 = (DicomObject)dicomobject3.get(581, j);
                DicomObject dicomobject6 = new DicomObject();
                dicomobject6.set(91, dicomobject5.get(91));
                dicomobject6.set(92, dicomobject5.get(92));
                if(dicomobject5.getSize(1337) > 0)
                    dicomobject6.set(1337, dicomobject5.get(1337));
                if(dicomobject5.getSize(93) > 0)
                    dicomobject6.set(93, dicomobject5.get(93));
                dicomobject.append(581, ((Object) (dicomobject6)));
            }

        }
    }

    public static DicomObject createSSAttributes(String s)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        initSSAttributes(dicomobject, s);
        return dicomobject;
    }

    public static DicomObject createSSAttributes(DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        initSSAttributes(dicomobject1, dicomobject);
        return dicomobject1;
    }

}
