// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CommandLineParser.java

package com.tiani.dicom.util;

import java.util.Hashtable;
import java.util.Vector;

public class CommandLineParser
{

    private final String _options[];
    private String _opt;
    private Vector _param;
    private Hashtable _result;

    public static Hashtable parse(String as[])
    {
        return (new CommandLineParser(as, ((String []) (null)), ((int []) (null))))._result;
    }

    public static String get(Hashtable hashtable, String s, int i)
    {
        return ((String[])hashtable.get(((Object) (s))))[i];
    }

    public static Hashtable parse(String as[], String as1[])
        throws IllegalArgumentException
    {
        return (new CommandLineParser(as, as1, ((int []) (null))))._result;
    }

    public static Hashtable parse(String as[], String as1[], int ai[])
        throws IllegalArgumentException
    {
        return (new CommandLineParser(as, as1, ai))._result;
    }

    private CommandLineParser(String as[], String as1[], int ai[])
        throws IllegalArgumentException
    {
        _opt = "";
        _param = new Vector();
        _result = new Hashtable();
        _options = as1;
        for(int i = 0; i < as.length; i++)
            add(as[i]);

        _result.put(((Object) (_opt)), ((Object) (paramToArray())));
        _param = null;
        if(ai != null)
            checkParamCount(ai);
    }

    private void checkParamCount(int ai[])
    {
        for(int i = 0; i < ai.length; i++)
        {
            String as[] = (String[])_result.get(((Object) (_options[i])));
            if(as != null)
            {
                int j = as.length - ai[i];
                if(j < 0)
                    throw new IllegalArgumentException("Missing parameter for option: " + _options[i]);
                if(j > 0)
                    throw new IllegalArgumentException("Unrecognized parameter: " + as[ai[i]]);
            }
        }

    }

    private void add(String s)
    {
        if(s.charAt(0) == '-')
        {
            if(!isValidOption(s))
                throw new IllegalArgumentException("Unrecognized option: " + s);
            _result.put(((Object) (_opt)), ((Object) (paramToArray())));
            _opt = s;
            _param = new Vector();
        } else
        {
            _param.addElement(((Object) (s)));
        }
    }

    private String[] paramToArray()
    {
        String as[] = new String[_param.size()];
        _param.copyInto(((Object []) (as)));
        return as;
    }

    private boolean isValidOption(String s)
    {
        if(_options == null)
            return true;
        for(int i = 0; i < _options.length; i++)
            if(s.equals(((Object) (_options[i]))))
                return true;

        return false;
    }
}
