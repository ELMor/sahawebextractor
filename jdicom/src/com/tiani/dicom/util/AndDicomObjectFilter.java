// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AndDicomObjectFilter.java

package com.tiani.dicom.util;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.util:
//            IDicomObjectFilter

public class AndDicomObjectFilter
    implements IDicomObjectFilter
{

    private IDicomObjectFilter _components[];

    public AndDicomObjectFilter(IDicomObjectFilter aidicomobjectfilter[])
    {
        _components = aidicomobjectfilter;
    }

    public AndDicomObjectFilter(Vector vector)
    {
        _components = new IDicomObjectFilter[vector.size()];
        vector.copyInto(((Object []) (_components)));
    }

    public boolean accept(DicomObject dicomobject)
        throws DicomException
    {
        for(int i = 0; i < _components.length; i++)
            if(_components[i] != null && !_components[i].accept(dicomobject))
                return false;

        return true;
    }
}
