// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UIDgenerator.java

package com.tiani.dicom.util;

import com.tiani.dicom.myassert.Assert;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Random;

/**
 * @deprecated Class UIDgenerator is deprecated
 */

public final class UIDgenerator
{

    public static String prefix = "1.2.40.0.13.0.";
    public static String defaultSuffix = ".0";
    public static String studySuffix = ".1";
    public static String seriesSuffix = ".2";
    public static Hashtable suffixForClassUID = new Hashtable();
    private static final Random _random = new Random();
    private static final byte _byte1[] = new byte[1];
    private static final SimpleDateFormat _formater = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    public UIDgenerator()
    {
    }

    public static String createStudyInstanceUID()
    {
        return createUID(studySuffix);
    }

    public static String createSeriesInstanceUID()
    {
        return createUID(seriesSuffix);
    }

    public static String createSOPInstanceUIDFor(String s)
    {
        String s1 = (String)suffixForClassUID.get(((Object) (s)));
        return createUID(s1 == null ? defaultSuffix : s1);
    }

    public static String createUID(String s)
    {
        return prefix + createID(64 - prefix.length() - s.length()) + s;
    }

    public static String createID(int i)
    {
        Assert.isTrue(i > 0);
        _random.nextBytes(_byte1);
        String s = ((DateFormat) (_formater)).format(new Date()) + (_byte1[0] + 128);
        int j = s.length() - i;
        if(j <= 0)
            return s;
        else
            return "1" + s.substring(j + 1);
    }

}
