// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JFilterPanel.java

package com.tiani.dicom.tools.modalityscu;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.Tag;
import java.util.Enumeration;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public final class JFilterPanel extends JTabbedPane
{

    private DicomObject _filter[];
    private static final String _TAB_LABEL[] = {
        "SPS", "PROCEDURE", "SERVICE REQUEST", "PATIENT", "VISIT"
    };
    private static final int _SPS_SQ_INIT_KEYS[] = {
        81, 574, 575, 576, 579, 580, 582, 581
    };
    private static final int _PROC_INIT_KEYS[] = {
        425, 107, 589, 547, 548
    };
    private static final int _SERVICE_INIT_KEYS[] = {
        77, 540, 88
    };
    private static final int _PAT_INIT_KEYS[] = {
        147, 148, 150, 152, 609, 571, 174, 165, 166, 159, 
        568
    };
    private static final int _VISIT_INIT_KEYS[] = {
        554, 569, 110
    };
    private static final int _FILTER_INIT_KEYS[][] = {
        _SPS_SQ_INIT_KEYS, _PROC_INIT_KEYS, _SERVICE_INIT_KEYS, _PAT_INIT_KEYS, _VISIT_INIT_KEYS
    };
    private static final Tag _SPS_TAGS[];
    private static final Tag _PROC_TAGS[];
    private static final Tag _SERVICE_TAGS[];
    private static final Tag _PAT_TAGS[];
    private static final Tag _VISIT_TAGS[];
    private static final Tag _FILTER_TAGS[][];

    public JFilterPanel()
    {
        _filter = new DicomObject[_TAB_LABEL.length];
        try
        {
            for(int i = 0; i < _TAB_LABEL.length; i++)
            {
                _filter[i] = createFilter(i);
                DicomObjectTableModel dicomobjecttablemodel = new DicomObjectTableModel(_filter[i], false, true, true);
                dicomobjecttablemodel.addTags(_FILTER_TAGS[i]);
                ((JTabbedPane)this).add(_TAB_LABEL[i], ((java.awt.Component) (new JScrollPane(((java.awt.Component) (new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (dicomobjecttablemodel)))))))));
            }

        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).getMessage());
        }
    }

    public DicomObject getFilter()
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        for(int i = 0; i < _TAB_LABEL.length; i++)
            copyDicomObject(_filter[i], dicomobject);

        return dicomobject;
    }

    private static void copyDicomObject(DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        for(Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, false); enumeration.hasMoreElements(); copyTagValue((TagValue)enumeration.nextElement(), dicomobject1));
    }

    private static void copyTagValue(TagValue tagvalue, DicomObject dicomobject)
        throws DicomException
    {
        int i = tagvalue.getGroup();
        int j = tagvalue.getElement();
        dicomobject.deleteItem_ge(i, j);
        int k = tagvalue.size();
        if(k > 0)
        {
            for(int l = 0; l < k; l++)
                dicomobject.append_ge(i, j, tagvalue.getValue(l));

        } else
        {
            dicomobject.set_ge(i, j, ((Object) (null)));
        }
    }

    private static DicomObject createFilter(int i)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        int ai[] = _FILTER_INIT_KEYS[i];
        for(int j = 0; j < ai.length; j++)
            dicomobject.set(ai[j], ((Object) (null)));

        if(i != 0)
        {
            return dicomobject;
        } else
        {
            DicomObject dicomobject1 = new DicomObject();
            dicomobject1.set(587, ((Object) (dicomobject)));
            return dicomobject1;
        }
    }

    static 
    {
        _SPS_TAGS = IOD.accumulate(new Tag[][] {
            IOD.SPECIFIC_CHARACTER_SET, IOD.SCHEDULED_PROCEDURE_STEP_MODULE
        });
        _PROC_TAGS = IOD.REQUESTED_PROCEDURE_MODULE;
        _SERVICE_TAGS = IOD.IMAGING_SERVICE_REQUEST_MODULE;
        _PAT_TAGS = IOD.accumulate(new Tag[][] {
            IOD.PATIENT_RELATIONSHIP_MODULE, IOD.PATIENT_IDENTIFICATION_MODULE, IOD.PATIENT_DEMOGRAPHIC_MODULE, IOD.PATIENT_MEDICAL_MODULE
        });
        _VISIT_TAGS = IOD.accumulate(new Tag[][] {
            IOD.VISIT_RELATIONSHIP_MODULE, IOD.VISIT_IDENTIFICATION_MODULE, IOD.VISIT_STATUS_MODULE, IOD.VISIT_ADMISSION_MODULE
        });
        _FILTER_TAGS = (new Tag[][] {
            _SPS_TAGS, _PROC_TAGS, _SERVICE_TAGS, _PAT_TAGS, _VISIT_TAGS
        });
    }
}
