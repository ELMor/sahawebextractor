// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JTreePanel.java

package com.tiani.dicom.tools.modalityscu;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.ModalityPPS;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

// Referenced classes of package com.tiani.dicom.tools.modalityscu:
//            UserObject

public final class JTreePanel extends JPanel
{

    private static final DicomObjectTableModel _NULL_MODEL = new DicomObjectTableModel();
    private static final Tag MODALITY_WORKLIST_ITEM[];
    private static final Tag MODALITY_PERFORMED_PROCEDURE_STEP[];
    private static final SimpleDateFormat _dateFormater = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat _timeFormater = new SimpleDateFormat("HHmmss");
    private DefaultMutableTreeNode _root;
    private DefaultMutableTreeNode _wlNode;
    private DefaultMutableTreeNode _ppsNode;
    private JTree _tree;
    private DefaultTreeModel _treeModel;
    private TreePath _selPath;
    private JButton _addButton;
    private JButton _delButton;
    private JButton _addTagsButton;
    private JButton _remTagsButton;
    private JButton _createButton;
    private JButton _setButton;
    private JSizeColumnsToFitTable _recordTable;
    private DicomObjectTableModel _tableModel;
    private PrintStream _log;

    public JTreePanel(JButton jbutton, JButton jbutton1, JSizeColumnsToFitTable jsizecolumnstofittable, PrintStream printstream)
    {
        super(((java.awt.LayoutManager) (new BorderLayout())));
        _root = new DefaultMutableTreeNode();
        _wlNode = new DefaultMutableTreeNode("Worklist");
        _ppsNode = new DefaultMutableTreeNode("Performed Procedure Steps");
        _tree = new JTree(((javax.swing.tree.TreeNode) (_root)));
        _treeModel = (DefaultTreeModel)_tree.getModel();
        _selPath = null;
        _addButton = new JButton("+");
        _delButton = new JButton("-");
        _addTagsButton = new JButton(">>");
        _remTagsButton = new JButton("<<");
        _createButton = null;
        _setButton = null;
        _recordTable = null;
        _tableModel = null;
        _log = null;
        _createButton = jbutton;
        _setButton = jbutton1;
        _recordTable = jsizecolumnstofittable;
        _tableModel = (DicomObjectTableModel)((JTable) (_recordTable)).getModel();
        _log = printstream;
        ((AbstractButton) (_delButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(_selPath == null)
                    return;
                DefaultMutableTreeNode defaultmutabletreenode = (DefaultMutableTreeNode)_selPath.getLastPathComponent();
                if(defaultmutabletreenode.getLevel() == 1)
                    clear(defaultmutabletreenode);
                else
                    _treeModel.removeNodeFromParent(((javax.swing.tree.MutableTreeNode) (defaultmutabletreenode)));
                _selPath = null;
                enableButtons(_selPath);
                ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (JTreePanel._NULL_MODEL)));
            }

        });
        ((AbstractButton) (_addButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                try
                {
                    insertNewNodeInto(_ppsNode, new UserObject(createPPS(getSelectedSPSs()), UserObject.PPS_LABELS));
                }
                catch(Exception exception)
                {
                    _log.println(((Object) (exception)));
                }
            }

        });
        ((AbstractButton) (_addTagsButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                try
                {
                    if(isDicomObjectSelected())
                        _tableModel.addTags(isPPSSelected() ? JTreePanel.MODALITY_PERFORMED_PROCEDURE_STEP : JTreePanel.MODALITY_WORKLIST_ITEM);
                }
                catch(Exception exception)
                {
                    _log.println(((Object) (exception)));
                }
            }

        });
        ((AbstractButton) (_remTagsButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(isDicomObjectSelected())
                    load();
            }

        });
        _tree.addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent treeselectionevent)
            {
                _selPath = treeselectionevent.getNewLeadSelectionPath();
                _tableModel = JTreePanel._NULL_MODEL;
                if(isDicomObjectSelected())
                    load();
                else
                    ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (_tableModel)));
                enableButtons(_selPath);
            }

        });
        JPanel jpanel = new JPanel();
        ((Container) (jpanel)).add(((java.awt.Component) (_addButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_delButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_remTagsButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_addTagsButton)));
        ((Container)this).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (_tree))))), "Center");
        ((Container)this).add(((java.awt.Component) (jpanel)), "South");
        _root.add(((javax.swing.tree.MutableTreeNode) (_wlNode)));
        _root.add(((javax.swing.tree.MutableTreeNode) (_ppsNode)));
        _tree.setVisibleRowCount(10);
        ((JComponent) (_tree)).putClientProperty("JTree.lineStyle", "Angled");
        _tree.expandRow(0);
        _tree.setRootVisible(false);
        enableButtons(_selPath);
    }

    public JTree getTree()
    {
        return _tree;
    }

    private boolean isPPSSelected()
    {
        return isDicomObjectSelected() && _selPath.getPathComponent(1) == _ppsNode;
    }

    private boolean isDicomObjectSelected()
    {
        return _selPath != null && _selPath.getPathCount() == 3;
    }

    private void load()
    {
        UserObject userobject = (UserObject)((DefaultMutableTreeNode)_selPath.getLastPathComponent()).getUserObject();
        try
        {
            _tableModel = new DicomObjectTableModel(userobject.getDataset(), true, true, true);
            ((AbstractTableModel) (_tableModel)).addTableModelListener(new TableModelListener() {

                public void tableChanged(TableModelEvent tablemodelevent)
                {
                    DefaultMutableTreeNode defaultmutabletreenode = (DefaultMutableTreeNode)_selPath.getLastPathComponent();
                    ((UserObject)defaultmutabletreenode.getUserObject()).setDataset(_tableModel.getDicomObject());
                    _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (defaultmutabletreenode)));
                }

            });
            ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (_tableModel)));
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }

    private DicomObject[] getSelectedSPSs()
        throws DicomException
    {
        Vector vector = new Vector();
        TreePath atreepath[] = _tree.getSelectionPaths();
        UserObject userobject = null;
        if(atreepath != null)
        {
            for(int i = 0; i < atreepath.length; i++)
            {
                Object aobj[] = atreepath[i].getPath();
                if(aobj.length > 2 && aobj[1] == _wlNode)
                {
                    UserObject userobject1 = (UserObject)((DefaultMutableTreeNode)aobj[2]).getUserObject();
                    if(userobject == null)
                        userobject = userobject1;
                    else
                    if(!userobject.equalsPatient(userobject1))
                        throw new DicomException("Cannot associate PPS to several SPS for different patients");
                    DicomObject dicomobject = userobject1.getDataset();
                    vector.addElement(((Object) (userobject1.getDataset())));
                }
            }

        }
        DicomObject adicomobject[] = new DicomObject[vector.size()];
        vector.copyInto(((Object []) (adicomobject)));
        return adicomobject;
    }

    private DicomObject createPPS(DicomObject adicomobject[])
        throws DicomException
    {
        Date date = new Date();
        DicomObject dicomobject = ModalityPPS.createCreateMPPS(UIDUtils.createUID(), UIDUtils.createID(8), "ModalitySCU", ((DateFormat) (_dateFormater)).format(date), ((DateFormat) (_timeFormater)).format(date), "OT");
        dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation("1.2.840.10008.3.1.2.3.3", UIDUtils.createUID()))));
        if(adicomobject.length == 0)
            return dicomobject;
        DicomObject dicomobject1 = adicomobject[0];
        dicomobject.deleteItem(1210);
        for(int i = 0; i < adicomobject.length; i++)
        {
            dicomobject.append(1210, ((Object) (ModalityPPS.createSSAttributes(adicomobject[i]))));
            DicomObject dicomobject2 = (DicomObject)adicomobject[i].get(587);
            if(dicomobject2 != null)
                copyCodeSequence(dicomobject2, 581, dicomobject, 1209);
        }

        dicomobject.set(147, dicomobject1.get(147));
        dicomobject.set(148, dicomobject1.get(148));
        dicomobject.set(150, dicomobject1.get(150));
        dicomobject.set(152, dicomobject1.get(152));
        dicomobject.set(110, ((Object) (null)));
        if(dicomobject1.getSize(110) > 0)
        {
            DicomObject dicomobject3 = (DicomObject)dicomobject1.get(107);
            DicomObject dicomobject5 = new DicomObject();
            dicomobject5.set(115, dicomobject3.get(115));
            dicomobject5.set(116, dicomobject3.get(116));
            dicomobject.set(110, ((Object) (dicomobject5)));
        }
        DicomObject dicomobject4 = (DicomObject)dicomobject1.get(587);
        if(dicomobject4 == null)
            return dicomobject;
        dicomobject.set(1198, dicomobject4.get(574));
        dicomobject.set(81, dicomobject4.get(81));
        dicomobject.set(1199, dicomobject4.get(583));
        dicomobject.set(1200, dicomobject4.get(584));
        dicomobject.set(1207, dicomobject4.get(580));
        if(adicomobject.length == 1 && dicomobject4.getSize(548) > 0)
            copyCodeSequence(dicomobject1, 548, dicomobject, 96);
        return dicomobject;
    }

    private static void copyCodeSequence(DicomObject dicomobject, int i, DicomObject dicomobject1, int j)
        throws DicomException
    {
        int k = dicomobject.getSize(i);
        for(int l = 0; l < k; l++)
        {
            DicomObject dicomobject2 = (DicomObject)dicomobject.get(i, l);
            DicomObject dicomobject3 = new DicomObject();
            dicomobject3.set(91, dicomobject2.get(91));
            dicomobject3.set(92, dicomobject2.get(92));
            if(dicomobject2.getSize(1337) > 0)
                dicomobject3.set(1337, dicomobject2.get(1337));
            if(dicomobject2.getSize(93) > 0)
                dicomobject3.set(93, dicomobject2.get(93));
            dicomobject1.append(j, ((Object) (dicomobject3)));
        }

    }

    private void insertNewNodeInto(DefaultMutableTreeNode defaultmutabletreenode, UserObject userobject)
    {
        DefaultMutableTreeNode defaultmutabletreenode1 = new DefaultMutableTreeNode(((Object) (userobject)));
        _treeModel.insertNodeInto(((javax.swing.tree.MutableTreeNode) (defaultmutabletreenode1)), ((javax.swing.tree.MutableTreeNode) (defaultmutabletreenode)), defaultmutabletreenode.getChildCount());
        _tree.addSelectionPath(new TreePath(((Object []) (defaultmutabletreenode1.getPath()))));
    }

    public void addWLItem(DicomObject dicomobject)
    {
        try
        {
            UserObject userobject = new UserObject(dicomobject, UserObject.WL_LABELS);
            int i = _wlNode.getChildCount();
            for(int j = 0; j < i; j++)
            {
                DefaultMutableTreeNode defaultmutabletreenode = (DefaultMutableTreeNode)_wlNode.getChildAt(j);
                if(userobject.equalsWLitem((UserObject)defaultmutabletreenode.getUserObject()))
                {
                    defaultmutabletreenode.setUserObject(((Object) (userobject)));
                    _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (defaultmutabletreenode)));
                    _tree.setSelectionPath(new TreePath(((Object []) (defaultmutabletreenode.getPath()))));
                    return;
                }
            }

            insertNewNodeInto(_wlNode, userobject);
        }
        catch(DicomException dicomexception)
        {
            _log.println(((Object) (dicomexception)));
        }
    }

    private void clear(DefaultMutableTreeNode defaultmutabletreenode)
    {
        defaultmutabletreenode.removeAllChildren();
        _treeModel.nodeStructureChanged(((javax.swing.tree.TreeNode) (defaultmutabletreenode)));
    }

    public void enableButtons(TreePath treepath)
    {
        boolean flag = isPPSSelected();
        ((AbstractButton) (_createButton)).setEnabled(flag);
        ((AbstractButton) (_setButton)).setEnabled(flag);
    }

    static 
    {
        MODALITY_WORKLIST_ITEM = IOD.accumulate(IOD.MODALITY_WORKLIST_ITEM);
        MODALITY_PERFORMED_PROCEDURE_STEP = IOD.accumulate(IOD.MODALITY_PERFORMED_PROCEDURE_STEP);
    }


















}
