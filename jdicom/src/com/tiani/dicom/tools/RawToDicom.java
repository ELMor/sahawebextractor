// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RawToDicom.java

package com.tiani.dicom.tools;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.util.UIDUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;

public final class RawToDicom
{

    public RawToDicom()
    {
    }

    public static void main(String args[])
    {
        if(args.length <= 4)
        {
            System.out.println("Usage: java com.tiani.tool.RawToDicom <dicomHeader> <inFile> <outFile> <LE|BE>\n                                      [(gggg,eeee)=val ..]\n  or   java com.tiani.tool.RawToDicom <dicomHeader> <inDir> <outDir> <LE|BE>\n                                      [(gggg,eeee)=val ..]\n\nExample:\n  java com.tiani.tool.RawToDicom img0.dcm img1.raw img1.dcm LE\n                     \"(0020,000D)=1.22.333\" \"(0020,000E)=1.22.333.4444\"\n  Supplements Raw Data img1.raw in little Endian Format with attributes taken\n  from DICOM file img0.dcm and explicit specified Study+Series Instance UIDs\n  to new DICOM file img1.dcm.\n");
            return;
        }
        try
        {
            DicomObject dicomobject = new DicomObject();
            TianiInputStream tianiinputstream = new TianiInputStream(((java.io.InputStream) (new FileInputStream(args[0]))));
            try
            {
                tianiinputstream.read(dicomobject, false);
            }
            finally
            {
                ((FilterInputStream) (tianiinputstream)).close();
            }
            boolean flag = args[3].equalsIgnoreCase("BE");
            File file = new File(args[1]);
            boolean flag1 = file.isDirectory();
            File file1 = new File(args[2]);
            if(!flag1)
            {
                transfer(file, file1, dicomobject, UIDUtils.createUID(), flag, args);
                return;
            }
            if(!file1.isDirectory())
                throw new IOException("Error accesing directory " + file1);
            String args1[] = file.list();
            for(int i = 0; i < args1.length; i++)
                transfer(new File(file, args1[i]), new File(file1, args1[i]), dicomobject, UIDUtils.createUID(), flag, args);

        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
    }

    private static void transfer(File file, File file1, DicomObject dicomobject, String s, boolean flag, String as[])
        throws IOException, DicomException
    {
        byte abyte0[] = new byte[(int)file.length()];
        FileInputStream fileinputstream = new FileInputStream(file);
        try
        {
            fileinputstream.read(abyte0);
        }
        finally
        {
            fileinputstream.close();
        }
        if(flag)
            swapBytes(abyte0);
        dicomobject.set(63, ((Object) (s)));
        dicomobject.set(1184, ((Object) (abyte0)));
        for(int i = 4; i < as.length; i++)
        {
            String s1 = as[i];
            dicomobject.set_ge(Integer.parseInt(s1.substring(1, 5), 16), Integer.parseInt(s1.substring(6, 10), 16), ((Object) (s1.substring(12))));
        }

        FileOutputStream fileoutputstream = new FileOutputStream(file1);
        try
        {
            dicomobject.write(((java.io.OutputStream) (fileoutputstream)), dicomobject.getFileMetaInformation() != null);
        }
        finally
        {
            fileoutputstream.close();
        }
        System.out.println("Create " + file1);
    }

    private static void swapBytes(byte abyte0[])
    {
        int i = abyte0.length - 1;
        for(int j = 1; j < i; j++)
        {
            byte byte0 = abyte0[j];
            abyte0[j] = abyte0[++j];
            abyte0[j] = byte0;
        }

    }
}
