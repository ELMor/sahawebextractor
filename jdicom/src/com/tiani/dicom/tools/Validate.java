// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Validate.java

package com.tiani.dicom.tools;

import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.tiani.dicom.iod.CRImageIOD;
import com.tiani.dicom.iod.CTImageIOD;
import com.tiani.dicom.iod.CompositeIOD;
import com.tiani.dicom.iod.DefCallbackUser;
import com.tiani.dicom.iod.GSPresentationStateIOD;
import com.tiani.dicom.iod.MRImageIOD;
import com.tiani.dicom.iod.NMImageIOD;
import com.tiani.dicom.iod.PETImageIOD;
import com.tiani.dicom.iod.SCImageIOD;
import com.tiani.dicom.iod.StandaloneOverlayIOD;
import com.tiani.dicom.iod.USImageIOD;
import com.tiani.dicom.iod.USMultiFrameImageIOD;
import com.tiani.dicom.iod.UserOption;
import com.tiani.dicom.iod.XAImageIOD;
import com.tiani.dicom.iod.XRFImageIOD;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JFileListDialog;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.ui.UserOptionTableModel;
import com.tiani.dicom.util.ExampleFileFilter;
import com.tiani.dicom.util.ExtDDict;
import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;

public class Validate extends JApplet
{

    private static final ExtDDict extDDict = new ExtDDict();
    private DefCallbackUser callbackUser;
    private String _IOD_LABEL[] = {
        "CR", "CT", "MR", "NM", "PET", "US", "US MF", "SC", "XA", "RF", 
        "St.OV", "St.CV", "St.LUT", "GSPS"
    };
    private UserOption _IOD_USER_OPTIONS[][];
    private JFileChooser _fileChooser;
    private JFileListDialog _fileListDlg;
    private JFrame _frame;
    private final FileFilter _HTMLFileFilter = new ExampleFileFilter("html", "HTML");
    private File _curFile;
    private JButton _validateButton;
    private JButton _validateMultiButton;
    private JButton _saveHtmlButton;
    private JTextArea _logTextArea;
    private PrintStream _log;
    private JTabbedPane tabbedPane;
    private JSplitPane splitPane;
    private static final DicomObjectTableModel _NULL_MODEL = new DicomObjectTableModel();
    private JSizeColumnsToFitTable _table;

    public Validate()
    {
        callbackUser = new DefCallbackUser();
        _IOD_USER_OPTIONS = (new UserOption[][] {
            CRImageIOD.userOptions, CTImageIOD.userOptions, MRImageIOD.userOptions, NMImageIOD.userOptions, PETImageIOD.userOptions, USImageIOD.userOptions, USMultiFrameImageIOD.userOptions, SCImageIOD.userOptions, XAImageIOD.userOptions, XRFImageIOD.userOptions, 
            StandaloneOverlayIOD.userOptions, StandaloneOverlayIOD.userOptions, StandaloneOverlayIOD.userOptions, GSPresentationStateIOD.userOptions
        });
        _fileChooser = null;
        _fileListDlg = null;
        _frame = null;
        _curFile = null;
        _validateButton = new JButton("Validate");
        _validateMultiButton = new JButton("Validate *");
        _saveHtmlButton = new JButton("HTML");
        _logTextArea = new JTextArea();
        _log = new PrintStream(((java.io.OutputStream) (new DocumentOutputStream(((JTextComponent) (_logTextArea)).getDocument(), 50000))), true);
        tabbedPane = new JTabbedPane(2);
        _table = new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (_NULL_MODEL)));
    }

    public static void main(String args[])
    {
        Validate validate1 = new Validate();
        validate1._frame = ((JFrame) (new AppletFrame("Validate v1.7.26", ((Applet) (validate1)), 500, 500)));
    }

    public void init()
    {
        ((AbstractButton) (_validateButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File file = chooseFile("Validate File", "Validate", 0, false);
                if(file != null)
                    validate(file, true);
            }

        });
        ((AbstractButton) (_validateMultiButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File afile[] = chooseFiles();
                if(afile != null)
                    validate(afile);
            }

        });
        ((AbstractButton) (_saveHtmlButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(_curFile != null)
                    _fileChooser.setSelectedFile(new File(_curFile.getParent(), _curFile.getName() + ".html"));
                File file = chooseFile("Save as HTML file", "Save", 0, true);
                if(file != null)
                    saveHtml(file);
            }

        });
        AppletContext appletcontext = ((Applet)this).getAppletContext();
        if(appletcontext instanceof AppletFrame)
            appletcontext = null;
        Box box = Box.createHorizontalBox();
        ((Container) (box)).add(((java.awt.Component) (_validateButton)));
        ((Container) (box)).add(((java.awt.Component) (_validateMultiButton)));
        ((Container) (box)).add(((java.awt.Component) (_saveHtmlButton)));
        ((Container) (box)).add(Box.createGlue());
        ((Container) (box)).add(((java.awt.Component) (new JTianiButton(appletcontext))));
        for(int i = 0; i < _IOD_LABEL.length; i++)
            tabbedPane.add(_IOD_LABEL[i], ((java.awt.Component) (new JScrollPane(((java.awt.Component) (new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (new UserOptionTableModel(_IOD_USER_OPTIONS[i], callbackUser))))))))));

        tabbedPane.add("Attributs", ((java.awt.Component) (new JScrollPane(((java.awt.Component) (_table))))));
        splitPane = new JSplitPane(0, ((java.awt.Component) (tabbedPane)), ((java.awt.Component) (new JAutoScrollPane(((JTextComponent) (_logTextArea))))));
        Container container = ((JApplet)this).getContentPane();
        container.add(((java.awt.Component) (box)), "North");
        container.add(((java.awt.Component) (splitPane)), "Center");
    }

    private File chooseFile(String s, String s1, int i, boolean flag)
    {
        try
        {
            if(_fileChooser == null)
            {
                _fileChooser = new JFileChooser(".");
                _fileChooser.addChoosableFileFilter(_HTMLFileFilter);
            }
            _fileChooser.setFileFilter(flag ? _HTMLFileFilter : _fileChooser.getAcceptAllFileFilter());
            _fileChooser.setDialogTitle(s);
            _fileChooser.setFileSelectionMode(i);
            int j = _fileChooser.showDialog(((java.awt.Component) (_frame)), s1);
            File file = _fileChooser.getSelectedFile();
            return j != 0 ? null : file;
        }
        catch(SecurityException securityexception)
        {
            _log.println(((Object) (securityexception)));
        }
        showPolicyFile();
        return null;
    }

    private void showPolicyFile()
    {
        try
        {
            URL url = new URL(((Applet)this).getDocumentBase(), ((Applet)this).getParameter("PolicyFile"));
            ((Applet)this).getAppletContext().showDocument(url, "_blank");
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }

    private File[] chooseFiles()
    {
        File file = chooseFile("Select Directory", "Select", 2, false);
        if(file == null)
            return null;
        if(_fileListDlg == null)
            _fileListDlg = new JFileListDialog(((java.awt.Frame) (_frame)), "Validate files");
        return _fileListDlg.getSelectedFiles(file.isDirectory() ? file : new File(file.getParent()));
    }

    private void validate(File file, boolean flag)
    {
        try
        {
            _log.println("Validate file " + file);
            DicomObject dicomobject = new DicomObject();
            TianiInputStream tianiinputstream = new TianiInputStream(((java.io.InputStream) (new FileInputStream(file))));
            try
            {
                tianiinputstream.read(dicomobject);
            }
            finally
            {
                ((FilterInputStream) (tianiinputstream)).close();
            }
            _curFile = file;
            if(flag)
                ((JTable) (_table)).setModel(((javax.swing.table.TableModel) (new DicomObjectTableModel(dicomobject))));
            String s = dicomobject.getS(62);
            String s1 = UID.getUIDEntry(s).getName();
            CompositeIOD compositeiod = CompositeIOD.getIOD(s, dicomobject);
            if(compositeiod == null)
            {
                _log.println("No IOD for  " + s1 + " available");
            } else
            {
                Vector vector = new Vector();
                if(!compositeiod.validate(dicomobject, vector, ((com.tiani.dicom.iod.ICallbackUser) (callbackUser))))
                {
                    _log.println("ERROR detected for " + s1 + " :");
                    for(Enumeration enumeration = vector.elements(); enumeration.hasMoreElements(); _log.println(enumeration.nextElement()));
                } else
                {
                    _log.println("No error detected for " + s1);
                }
            }
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }

    private void validate(final File files[])
    {
        (new Thread(new Runnable() {

            public void run()
            {
                for(int i = 0; i < files.length; i++)
                    validate(files[i], false);

            }

        })).start();
    }

    private void saveHtml(File file)
    {
        try
        {
            FileOutputStream fileoutputstream = new FileOutputStream(file);
            PrintWriter printwriter = new PrintWriter(((java.io.OutputStream) (fileoutputstream)));
            try
            {
                _table.toHTML(printwriter, _curFile.getName());
            }
            finally
            {
                printwriter.close();
            }
            _log.println("Save as HTML to " + file);
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }








}
