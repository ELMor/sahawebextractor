// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ModifyDicomObject.java

package com.tiani.dicom.tools;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.legacy.TianiInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

public final class ModifyDicomObject
{
    private static class MyTagValue extends TagValue
    {

        public MyTagValue(String s)
        {
            StringTokenizer stringtokenizer = new StringTokenizer(s, ", \t\r\n\f");
            super.group = Integer.parseInt(stringtokenizer.nextToken(), 16);
            super.element = Integer.parseInt(stringtokenizer.nextToken(), 16);
            for(; stringtokenizer.hasMoreTokens(); super.val.addElement(((Object) (stringtokenizer.nextToken("\\")))));
        }
    }


    public ModifyDicomObject()
    {
    }

    public static void main(String args[])
    {
        if(args.length != 3)
        {
            System.out.println("Usage: ModifyDicomObject <inFile> <outFile> <attribFile>");
            System.out.println("or     ModifyDicomObject <inDir> <outDir> <attribFile>");
            return;
        }
        try
        {
            Vector vector = readAttribsFrom(args[2]);
            File file = new File(args[0]);
            boolean flag = file.isDirectory();
            File file1 = new File(args[1]);
            if(!flag)
            {
                modify(file, file1, vector);
                return;
            }
            if(!file1.isDirectory())
                throw new IOException("Error accesing directory " + file1);
            String args1[] = file.list();
            for(int i = 0; i < args1.length; i++)
                try
                {
                    modify(new File(file, args1[i]), new File(file1, args1[i]), vector);
                }
                catch(Exception exception1)
                {
                    System.out.println(((Object) (exception1)));
                }

        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
    }

    private static Vector readAttribsFrom(String s)
        throws IOException
    {
        Vector vector = new Vector();
        BufferedReader bufferedreader = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(s)))))));
        String s1;
        try
        {
            while((s1 = bufferedreader.readLine()) != null) 
            {
                s1 = s1.trim();
                if(s1.length() > 0 && s1.charAt(0) != '#')
                    vector.addElement(((Object) (new MyTagValue(s1))));
            }
        }
        finally
        {
            bufferedreader.close();
        }
        return vector;
    }

    private static void modify(File file, File file1, Vector vector)
        throws IOException, DicomException
    {
        DicomObject dicomobject = new DicomObject();
        TianiInputStream tianiinputstream = new TianiInputStream(((java.io.InputStream) (new FileInputStream(file))));
        try
        {
            tianiinputstream.read(dicomobject, true);
        }
        finally
        {
            ((FilterInputStream) (tianiinputstream)).close();
        }
        for(Enumeration enumeration = vector.elements(); enumeration.hasMoreElements(); modify(dicomobject, (TagValue)enumeration.nextElement()));
        FileOutputStream fileoutputstream = new FileOutputStream(file1);
        try
        {
            dicomobject.write(((java.io.OutputStream) (fileoutputstream)), dicomobject.getFileMetaInformation() != null);
        }
        finally
        {
            fileoutputstream.close();
        }
        System.out.print('.');
    }

    private static void modify(DicomObject dicomobject, TagValue tagvalue)
        throws DicomException
    {
        dicomobject.set_ge(tagvalue.getGroup(), tagvalue.getElement(), tagvalue.getValue());
        for(int i = 1; i < tagvalue.size(); i++)
            dicomobject.set_ge(tagvalue.getGroup(), tagvalue.getElement(), tagvalue.getValue(i), i);

    }
}
