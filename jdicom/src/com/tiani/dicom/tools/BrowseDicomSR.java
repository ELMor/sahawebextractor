// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BrowseDicomSR.java

package com.tiani.dicom.tools;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.tools.sr.DicomObjectNode;
import com.tiani.dicom.tools.sr.XMLWriter;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.ExampleFileFilter;
import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.AbstractButton;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

public class BrowseDicomSR extends JApplet
{

    private JFrame _frame;
    private JFileChooser _fileChooser;
    private final FileFilter _XMLFileFilter = new ExampleFileFilter("xml", "XML");
    private JTextField _url;
    private JButton _chooseFileButton;
    private JButton _xmlButton;
    private JTree _tree;
    private DefaultTreeModel _model;
    private DicomObjectTableModel _tableModel;
    private DicomObject _dicomObject;
    private JSizeColumnsToFitTable _table;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;

    public BrowseDicomSR()
    {
        _frame = null;
        _fileChooser = null;
        _url = new JTextField();
        _chooseFileButton = new JButton("Choose File");
        _xmlButton = new JButton("XML");
        _tree = new JTree(((javax.swing.tree.TreeNode) (new DefaultMutableTreeNode("No Data"))));
        _model = (DefaultTreeModel)_tree.getModel();
        _tableModel = new DicomObjectTableModel();
        _dicomObject = _tableModel.getDicomObject();
        _table = new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (_tableModel)));
        _logTextArea = new JTextArea();
        _logDoc = ((JTextComponent) (_logTextArea)).getDocument();
        _log = new PrintStream(((java.io.OutputStream) (new DocumentOutputStream(_logDoc))), true);
    }

    public static void main(String args[])
    {
        BrowseDicomSR browsedicomsr = new BrowseDicomSR();
        browsedicomsr._frame = ((JFrame) (new AppletFrame("BrowseDicomSR v1.7.26", ((Applet) (browsedicomsr)), 600, 500)));
    }

    public void init()
    {
        _url.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                load(((JTextComponent) (_url)).getText());
            }

        });
        ((AbstractButton) (_chooseFileButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File file = chooseFile("Load from DICOM file", "Load", ((FileFilter) (null)));
                if(file != null)
                    load(file);
            }

        });
        ((AbstractButton) (_xmlButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File file = chooseFile("Save as XML file", "Save", _XMLFileFilter);
                if(file != null)
                    saveXML(file);
            }

        });
        _tree.addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent treeselectionevent)
            {
                TreePath treepath = treeselectionevent.getNewLeadSelectionPath();
                if(treepath != null)
                {
                    DicomObjectNode dicomobjectnode = (DicomObjectNode)treepath.getLastPathComponent();
                    updateTable(dicomobjectnode.getDataset());
                }
            }

        });
        _tree.setVisibleRowCount(10);
        ((JComponent) (_tree)).putClientProperty("JTree.lineStyle", "Angled");
        AppletContext appletcontext = ((Applet)this).getAppletContext();
        if(appletcontext instanceof AppletFrame)
            appletcontext = null;
        JPanel jpanel = new JPanel(((java.awt.LayoutManager) (new GridBagLayout())));
        GridBagConstraints gridbagconstraints = new GridBagConstraints();
        ((Container) (jpanel)).add(((java.awt.Component) (new JLabel("URL:"))), ((Object) (gridbagconstraints)));
        gridbagconstraints.fill = 2;
        gridbagconstraints.weightx = 100D;
        ((Container) (jpanel)).add(((java.awt.Component) (_url)), ((Object) (gridbagconstraints)));
        gridbagconstraints.fill = 0;
        gridbagconstraints.weightx = 0.0D;
        ((Container) (jpanel)).add(((java.awt.Component) (_chooseFileButton)), ((Object) (gridbagconstraints)));
        ((Container) (jpanel)).add(((java.awt.Component) (_xmlButton)), ((Object) (gridbagconstraints)));
        ((Container) (jpanel)).add(((java.awt.Component) (new JTianiButton(appletcontext))), ((Object) (gridbagconstraints)));
        ((AbstractButton) (_xmlButton)).setEnabled(false);
        JSplitPane jsplitpane = new JSplitPane(0, ((java.awt.Component) (new JScrollPane(((java.awt.Component) (_tree))))), ((java.awt.Component) (new JScrollPane(((java.awt.Component) (_table))))));
        jsplitpane.setOneTouchExpandable(true);
        JTabbedPane jtabbedpane = new JTabbedPane();
        jtabbedpane.add("DICOM", ((java.awt.Component) (jsplitpane)));
        jtabbedpane.add("XML", ((java.awt.Component) (new JScrollPane(((java.awt.Component) (_logTextArea))))));
        Container container = ((JApplet)this).getContentPane();
        container.add(((java.awt.Component) (jpanel)), "North");
        container.add(((java.awt.Component) (jtabbedpane)), "Center");
        String s = ((Applet)this).getParameter("AutoLoad");
        if(s != null && s.length() > 0)
            try
            {
                load(new URL(((Applet)this).getDocumentBase(), s));
            }
            catch(Exception exception)
            {
                prompt(((Object) (exception)));
            }
    }

    private File chooseFile(String s, String s1, FileFilter filefilter)
    {
        try
        {
            if(_fileChooser == null)
            {
                _fileChooser = new JFileChooser(".");
                _fileChooser.addChoosableFileFilter(_XMLFileFilter);
            }
            _fileChooser.setFileFilter(filefilter == null ? _fileChooser.getAcceptAllFileFilter() : filefilter);
            _fileChooser.setDialogTitle(s);
            int i = _fileChooser.showDialog(((java.awt.Component) (_frame)), s1);
            File file = _fileChooser.getSelectedFile();
            return i != 0 ? null : file;
        }
        catch(SecurityException securityexception)
        {
            prompt(((Object) (securityexception)));
        }
        showPolicyFile();
        return null;
    }

    private void showPolicyFile()
    {
        try
        {
            URL url = new URL(((Applet)this).getDocumentBase(), ((Applet)this).getParameter("PolicyFile"));
            ((Applet)this).getAppletContext().showDocument(url, "_blank");
        }
        catch(Exception exception)
        {
            prompt(((Object) (exception)));
        }
    }

    private URL toURL(String s)
        throws MalformedURLException
    {
        try
        {
            return new URL(s);
        }
        catch(MalformedURLException malformedurlexception)
        {
            try
            {
                return toURL(new File(s));
            }
            catch(MalformedURLException malformedurlexception1)
            {
                throw malformedurlexception;
            }
        }
    }

    private URL toURL(File file)
        throws MalformedURLException
    {
        String s = file.getAbsolutePath();
        if(File.separatorChar != '/')
            s = s.replace(File.separatorChar, '/');
        if(!s.startsWith("/"))
            s = "/" + s;
        if(!s.endsWith("/") && file.isDirectory())
            s = s + "/";
        return new URL("file", "", s);
    }

    private void load(String s)
    {
        try
        {
            load(toURL(s));
        }
        catch(Exception exception)
        {
            prompt(((Object) (exception)));
        }
    }

    private void load(File file)
    {
        try
        {
            load(toURL(file));
        }
        catch(Exception exception)
        {
            prompt(((Object) (exception)));
        }
    }

    private void load(URL url)
    {
        try
        {
            TianiInputStream tianiinputstream = new TianiInputStream(url.openStream());
            DicomObject dicomobject = new DicomObject();
            try
            {
                tianiinputstream.read(dicomobject, true);
            }
            finally
            {
                ((FilterInputStream) (tianiinputstream)).close();
            }
            ((JTextComponent) (_url)).setText(url.toString());
            update(dicomobject);
        }
        catch(SecurityException securityexception)
        {
            prompt(((Object) (securityexception)));
            showPolicyFile();
        }
        catch(Throwable throwable)
        {
            prompt(((Object) (throwable)));
        }
    }

    private void saveXML(File file)
    {
        try
        {
            XMLWriter xmlwriter = new XMLWriter(((java.io.OutputStream) (new FileOutputStream(file))));
            try
            {
                xmlwriter.write(_dicomObject);
            }
            finally
            {
                ((BufferedWriter) (xmlwriter)).close();
            }
        }
        catch(Exception exception)
        {
            prompt(((Object) (exception)));
        }
    }

    public void update(DicomObject dicomobject)
    {
        _dicomObject = dicomobject;
        updateTable(dicomobject);
        try
        {
            _model.setRoot(((javax.swing.tree.TreeNode) (new DicomObjectNode("1", _dicomObject, ((javax.swing.tree.TreeNode) (null))))));
            ((AbstractButton) (_xmlButton)).setEnabled(true);
            ((JTextComponent) (_logTextArea)).setText("");
            XMLWriter xmlwriter = new XMLWriter(((java.io.OutputStream) (_log)));
            try
            {
                xmlwriter.write(_dicomObject);
            }
            finally
            {
                ((BufferedWriter) (xmlwriter)).close();
            }
        }
        catch(Exception exception)
        {
            ((AbstractButton) (_xmlButton)).setEnabled(false);
            prompt(((Object) (exception)));
        }
    }

    private void updateTable(DicomObject dicomobject)
    {
        try
        {
            ((JTable) (_table)).setModel(((javax.swing.table.TableModel) (new DicomObjectTableModel(dicomobject))));
        }
        catch(DicomException dicomexception)
        {
            prompt(((Object) (dicomexception)));
        }
    }

    private void prompt(Object obj)
    {
        JOptionPane.showMessageDialog(((java.awt.Component) (_frame)), obj, "BrowseDicomSR", 0);
    }







}
