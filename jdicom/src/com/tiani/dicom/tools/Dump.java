// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Dump.java

package com.tiani.dicom.tools;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class Dump
{

    private final DataInputStream in;

    public static void main(String args[])
        throws Exception
    {
        if(args.length != 1)
        {
            System.err.println("Usage: java -jar dcmdump.jar <file>");
            System.exit(1);
        }
        DataInputStream datainputstream = new DataInputStream(((java.io.InputStream) (new BufferedInputStream(((java.io.InputStream) (new FileInputStream(args[0])))))));
        try
        {
            (new Dump(datainputstream)).dump(((OutputStream) (System.out)));
        }
        finally
        {
            try
            {
                ((FilterInputStream) (datainputstream)).close();
            }
            catch(IOException ioexception) { }
        }
    }

    public Dump(DataInputStream datainputstream)
    {
        in = datainputstream;
    }

    public void dump(OutputStream outputstream)
        throws IOException, DicomException
    {
        byte abyte0[] = new byte[5];
        ((FilterInputStream) (in)).mark(5);
        in.readFully(abyte0);
        if(abyte0[0] == 77 && abyte0[1] == 65 && abyte0[2] == 71 && abyte0[3] == 73 && abyte0[4] == 67)
            dumpEncapsHeader(outputstream);
        else
            ((FilterInputStream) (in)).reset();
        DicomObject dicomobject = new DicomObject();
        dicomobject.read(((java.io.InputStream) (in)), false);
        dicomobject.dumpVRs(outputstream, true);
        if(dicomobject.getPixelDataLength() > 0)
        {
            String s = "Pixel Data Length: " + dicomobject.getPixelDataLength() + "\n";
            outputstream.write(s.getBytes());
        }
    }

    private void dumpEncapsHeader(OutputStream outputstream)
        throws IOException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
        dataoutputstream.writeBytes("MAGIC");
        String s;
        do
        {
            s = in.readLine();
            dataoutputstream.writeBytes(s);
            dataoutputstream.writeBytes("\n");
        } while(!"ENDINFO".equals(((Object) (s))));
    }
}
