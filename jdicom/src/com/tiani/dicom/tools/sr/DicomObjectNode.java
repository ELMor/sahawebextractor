// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DicomObjectNode.java

package com.tiani.dicom.tools.sr;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.util.Enumeration;
import javax.swing.tree.TreeNode;

public final class DicomObjectNode
    implements TreeNode
{

    TreeNode _parent;
    String _id;
    DicomObject _dataset;
    int _childcount;

    public DicomObjectNode(String s, DicomObject dicomobject, TreeNode treenode)
    {
        _parent = null;
        _id = null;
        _dataset = null;
        _id = s;
        _dataset = dicomobject;
        _parent = treenode;
        _childcount = _dataset.getSize(1451);
    }

    public DicomObject getDataset()
    {
        return _dataset;
    }

    public TreeNode getChildAt(int i)
    {
        return ((TreeNode) (i >= _childcount ? null : new DicomObjectNode(_id + '.' + (i + 1), (DicomObject)_dataset.get(1451, i), ((TreeNode) (this)))));
    }

    public int getIndex(TreeNode treenode)
    {
        DicomObject dicomobject = ((DicomObjectNode)treenode)._dataset;
        for(int i = 0; i < _childcount; i++)
            if(_dataset.get(1451, i) == dicomobject)
                return i;

        return -1;
    }

    public int getChildCount()
    {
        return _childcount;
    }

    public TreeNode getParent()
    {
        return _parent;
    }

    public boolean getAllowsChildren()
    {
        return true;
    }

    public boolean isLeaf()
    {
        return _childcount <= 0;
    }

    public Enumeration children()
    {
        return new Enumeration() {

            int childindex;

            public boolean hasMoreElements()
            {
                return childindex < _childcount;
            }

            public Object nextElement()
            {
                return ((Object) (getChildAt(childindex++)));
            }

            
            {
                childindex = 0;
            }
        };
    }

    public String toString()
    {
        try
        {
            StringBuffer stringbuffer = new StringBuffer();
            stringbuffer.append('(');
            stringbuffer.append(_id);
            stringbuffer.append(')');
            if(_dataset.getSize(1425) > 0)
                stringbuffer.append(_dataset.getS(1425));
            stringbuffer.append(':');
            int i = _dataset.getSize(1458);
            if(i > 0)
            {
                stringbuffer.append("->(");
                stringbuffer.append(_dataset.getI(1458));
                for(int j = 1; j < i; j++)
                {
                    stringbuffer.append('.');
                    stringbuffer.append(_dataset.getI(1458, j));
                }

                stringbuffer.append(')');
                return stringbuffer.toString();
            }
            if(_dataset.getSize(1322) > 0)
            {
                DicomObject dicomobject = (DicomObject)_dataset.get(1322);
                if(dicomobject.getSize(93) > 0)
                    stringbuffer.append(dicomobject.getS(93));
            }
            if(_dataset.getSize(1429) > 0)
                appendValueTo(stringbuffer);
            return stringbuffer.toString();
        }
        catch(Exception exception)
        {
            return ((Throwable) (exception)).toString();
        }
    }

    private void appendValueTo(StringBuffer stringbuffer)
        throws DicomException
    {
        String s = _dataset.getS(1429);
        stringbuffer.append('[');
        stringbuffer.append(s);
        stringbuffer.append("] = ");
        if(s.equals("CONTAINER"))
        {
            if(_dataset.getSize(1430) > 0)
            {
                stringbuffer.append('{');
                stringbuffer.append(_dataset.getS(1430));
                stringbuffer.append('}');
            }
        } else
        if(s.equals("TEXT"))
        {
            if(_dataset.getSize(1327) > 0)
                stringbuffer.append(_dataset.getS(1327));
        } else
        if(s.equals("DATETIME"))
        {
            if(_dataset.getSize(1434) > 0)
                stringbuffer.append(_dataset.getS(1434));
        } else
        if(s.equals("DATE"))
        {
            if(_dataset.getSize(1323) > 0)
                stringbuffer.append(_dataset.getS(1323));
        } else
        if(s.equals("TIME"))
        {
            if(_dataset.getSize(1324) > 0)
                stringbuffer.append(_dataset.getS(1324));
        } else
        if(s.equals("PNAME"))
        {
            if(_dataset.getSize(1325) > 0)
                stringbuffer.append(_dataset.getS(1325));
        } else
        if(s.equals("UIDREF"))
        {
            if(_dataset.getSize(1435) > 0)
                stringbuffer.append(_dataset.getS(1435));
        } else
        if(s.equals("NUM"))
        {
            int i = _dataset.getSize(1440);
            if(i > 0)
            {
                appendNumValueTo(stringbuffer, (DicomObject)_dataset.get(1440));
                for(int j = 1; j < i; j++)
                {
                    stringbuffer.append('\\');
                    appendNumValueTo(stringbuffer, (DicomObject)_dataset.get(1440, j));
                }

            }
        } else
        if(s.equals("CODE"))
        {
            if(_dataset.getSize(1328) > 0)
            {
                DicomObject dicomobject = (DicomObject)_dataset.get(1328);
                if(dicomobject.getSize(93) > 0)
                    stringbuffer.append(dicomobject.getS(93));
            }
        } else
        if(s.equals("COMPOSITE") || s.equals("IMAGE") || s.equals("WAVEFORM"))
        {
            stringbuffer.append(_dataset.getSize(121));
            stringbuffer.append(" Referenced SOP items");
        } else
        if(s.equals("SCOORD"))
        {
            if(_dataset.getSize(1396) > 0)
            {
                stringbuffer.append('[');
                stringbuffer.append(_dataset.getS(1396));
                stringbuffer.append(']');
            }
        } else
        if(s.equals("TCOORD") && _dataset.getSize(1436) > 0)
        {
            stringbuffer.append('[');
            stringbuffer.append(_dataset.getS(1436));
            stringbuffer.append(']');
        }
    }

    private void appendNumValueTo(StringBuffer stringbuffer, DicomObject dicomobject)
        throws DicomException
    {
        if(dicomobject.getSize(1329) > 0)
            stringbuffer.append(dicomobject.getS(1329));
        if(dicomobject.getSize(1321) > 0)
        {
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(1321);
            if(dicomobject1.getSize(93) > 0)
            {
                stringbuffer.append(' ');
                stringbuffer.append(dicomobject1.getS(93));
            }
        }
    }
}
