// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ModalityWorklistSCP.java

package com.tiani.dicom.tools.risserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefCFindSCP;
import com.tiani.dicom.framework.DefMultiResponse;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IMultiResponse;
import com.tiani.dicom.framework.SingleResponse;
import com.tiani.dicom.util.DOFFactory;
import com.tiani.dicom.util.DefDicomObjectFilter;
import com.tiani.dicom.util.IDicomObjectFilter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.tools.risserver:
//            Repository

public final class ModalityWorklistSCP extends DefCFindSCP
{

    private Repository _repository;

    public ModalityWorklistSCP(Repository repository)
    {
        _repository = repository;
    }

    protected IMultiResponse query(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
    {
        try
        {
            Vector vector = new Vector();
            File afile[] = _repository.listMWLFiles();
            DicomObject dicomobject = dicommessage.getDataset();
            IDicomObjectFilter idicomobjectfilter = DOFFactory.getMWLDOFFactory().getFilter(dicomobject);
            for(int j = 0; j < afile.length; j++)
            {
                DicomObject dicomobject1;
                if((dicomobject1 = query(afile[j], idicomobjectfilter)) != null)
                    vector.addElement(((Object) (DefDicomObjectFilter.createReturn(dicomobject, dicomobject1))));
            }

            return ((IMultiResponse) (new DefMultiResponse(vector.elements())));
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
        return ((IMultiResponse) (new SingleResponse(257)));
    }

    private DicomObject query(File file, IDicomObjectFilter idicomobjectfilter)
        throws IOException, DicomException
    {
        DicomObject dicomobject = new DicomObject();
        FileInputStream fileinputstream = new FileInputStream(file);
        try
        {
            dicomobject.read(((java.io.InputStream) (fileinputstream)));
        }
        finally
        {
            fileinputstream.close();
        }
        return idicomobjectfilter.accept(dicomobject) ? dicomobject : null;
    }
}
