// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MyTreeCellRenderer.java

package com.tiani.dicom.tools;

import java.awt.Component;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

class MyTreeCellRenderer extends DefaultTreeCellRenderer
{

    private static final Icon _icons[];

    MyTreeCellRenderer()
    {
    }

    public Component getTreeCellRendererComponent(JTree jtree, Object obj, boolean flag, boolean flag1, boolean flag2, int i, boolean flag3)
    {
        super.getTreeCellRendererComponent(jtree, obj, flag, flag1, flag2, i, flag3);
        DefaultMutableTreeNode defaultmutabletreenode = (DefaultMutableTreeNode)obj;
        ((JLabel)this).setIcon(_icons[defaultmutabletreenode.getLevel()]);
        return ((Component) (this));
    }

    static Class _mthclass$(String s)
    {
        try
        {
            return Class.forName(s);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            throw new NoClassDefFoundError(((Throwable) (classnotfoundexception)).getMessage());
        }
    }

    static 
    {
        _icons = (new Icon[] {
            new ImageIcon((com.tiani.dicom.tools.MyTreeCellRenderer.class).getResource("/com/tiani/dicom/icons/root.gif")), new ImageIcon((com.tiani.dicom.tools.MyTreeCellRenderer.class).getResource("/com/tiani/dicom/icons/patient.gif")), new ImageIcon((com.tiani.dicom.tools.MyTreeCellRenderer.class).getResource("/com/tiani/dicom/icons/study.gif")), new ImageIcon((com.tiani.dicom.tools.MyTreeCellRenderer.class).getResource("/com/tiani/dicom/icons/series.gif")), new ImageIcon((com.tiani.dicom.tools.MyTreeCellRenderer.class).getResource("/com/tiani/dicom/icons/image.gif"))
        });
    }
}
