// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JPropertiesTable.java

package com.tiani.dicom.ui;

import com.tiani.dicom.util.CheckParam;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Hashtable;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

// Referenced classes of package com.tiani.dicom.ui:
//            JSizeColumnsToFitTable, PropertiesTableModel

public class JPropertiesTable extends JSizeColumnsToFitTable
{

    private Hashtable _checks;

    public JPropertiesTable(String as[], Properties properties, Hashtable hashtable)
    {
        super(((javax.swing.table.TableModel) (new PropertiesTableModel(as, properties))));
        _checks = hashtable;
        ((Component)this).addMouseListener(((java.awt.event.MouseListener) (new MouseAdapter() {

            public void mouseClicked(MouseEvent mouseevent)
            {
                int i = rowAtPoint(mouseevent.getPoint());
                if(i != -1 && mouseevent.getClickCount() == 2)
                    update(i);
            }

        })));
    }

    private void update(int i)
    {
        String s = (String)((JTable)this).getValueAt(i, 0);
        CheckParam checkparam = (CheckParam)_checks.get(((Object) (s)));
        if(checkparam == null)
            return;
        String as[] = checkparam.getTags();
        if(as == null)
            return;
        if(!checkparam.isMulti())
        {
            Object obj = ((JTable)this).getValueAt(i, 1);
            Object obj2 = JOptionPane.showInputDialog(((Component) (null)), "Choose one", s, 3, ((javax.swing.Icon) (null)), ((Object []) (as)), obj);
            if(obj2 != null && obj2 != obj)
            {
                ((JTable)this).setValueAt(obj2, i, 1);
                ((AbstractTableModel) ((PropertiesTableModel)((JTable)this).getModel())).fireTableCellUpdated(i, 1);
            }
        } else
        {
            Object obj1 = JOptionPane.showInputDialog(((Component) (null)), "Choose one to add", s, 3, ((javax.swing.Icon) (null)), ((Object []) (as)), ((Object) (as[0])));
            if(obj1 != null)
            {
                String s1 = (String)obj1;
                String s2 = (String)((JTable)this).getValueAt(i, 1);
                if(s2.trim().length() > 0)
                    s1 = s1 + ',' + s2;
                ((JTable)this).setValueAt(((Object) (s1)), i, 1);
                ((AbstractTableModel) ((PropertiesTableModel)((JTable)this).getModel())).fireTableCellUpdated(i, 1);
            }
        }
    }

}
