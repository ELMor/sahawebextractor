// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JAutoScrollPane.java

package com.tiani.dicom.ui;

import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

public class JAutoScrollPane extends JScrollPane
{

    public JAutoScrollPane(JTextComponent jtextcomponent)
    {
        super(((java.awt.Component) (jtextcomponent)));
        final JViewport vp = ((JScrollPane)this).getViewport();
        jtextcomponent.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent documentevent)
            {
                SwingUtilities.invokeLater(new Runnable() {

                    public void run()
                    {
                        Dimension dimension = vp.getViewSize();
                        Rectangle rectangle = vp.getViewRect();
                        int i = dimension.height - rectangle.height;
                        if(i > rectangle.y)
                            vp.scrollRectToVisible(new Rectangle(rectangle.x, i, rectangle.width, rectangle.height));
                    }

                });
            }

            public void removeUpdate(DocumentEvent documentevent)
            {
            }

            public void changedUpdate(DocumentEvent documentevent)
            {
            }


        });
    }
}
