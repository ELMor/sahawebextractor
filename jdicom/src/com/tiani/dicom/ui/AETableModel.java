// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AETableModel.java

package com.tiani.dicom.ui;

import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;
import javax.swing.table.AbstractTableModel;

public class AETableModel extends AbstractTableModel
{

    private final AETable aetable;
    private String keys[];
    private static String HEADER[] = {
        "Title", "Host", "Port"
    };

    public AETableModel(AETable aetable1)
    {
        aetable = aetable1;
        update();
    }

    public void update()
    {
        keys = aetable.list();
        int i = getRowCount();
        ((AbstractTableModel)this).fireTableRowsInserted(0, i);
    }

    public String getColumnName(int i)
    {
        return HEADER[i];
    }

    public int getColumnCount()
    {
        return 3;
    }

    public int getRowCount()
    {
        return keys.length;
    }

    public Object getValueAt(int i, int j)
    {
        AET aet = aetable.lookup(keys[i]);
        switch(j)
        {
        case 0: // '\0'
            return ((Object) (aet.title));

        case 1: // '\001'
            return ((Object) (aet.host));

        case 2: // '\002'
            return ((Object) ("" + aet.port));
        }
        throw new IndexOutOfBoundsException();
    }

    public void setValueAt(Object obj, int i, int j)
    {
        String s = (String)obj;
        AET aet = aetable.removeAET(keys[i]);
        AET aet1;
        switch(j)
        {
        case 0: // '\0'
            aet1 = new AET(s, aet.host, aet.port);
            break;

        case 1: // '\001'
            aet1 = new AET(aet.title, s, aet.port);
            break;

        case 2: // '\002'
            aet1 = new AET(aet.title, aet.host, Integer.parseInt(s));
            break;

        default:
            throw new IndexOutOfBoundsException();
        }
        aetable.add(aet1);
        keys[i] = aet1.title;
    }

    public boolean isCellEditable(int i, int j)
    {
        return true;
    }

    public void deleteAET(int i)
    {
        aetable.removeAET(keys[i]);
        update();
    }

    public void addAET(AET aet)
    {
        aetable.add(aet);
        update();
    }

}
