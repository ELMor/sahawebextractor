// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PropertiesTableModel.java

package com.tiani.dicom.ui;

import java.util.Hashtable;
import java.util.Properties;
import javax.swing.table.AbstractTableModel;

public class PropertiesTableModel extends AbstractTableModel
{

    private static final String _HEADERS[] = {
        "Property", "Value"
    };
    private String _keys[];
    private Properties _data;

    public PropertiesTableModel()
    {
        this(((String []) (null)), ((Properties) (null)));
    }

    public PropertiesTableModel(String as[], Properties properties)
    {
        _keys = as == null ? new String[0] : as;
        _data = properties == null ? new Properties() : properties;
    }

    public String getColumnName(int i)
    {
        return _HEADERS[i];
    }

    public int getColumnCount()
    {
        return 2;
    }

    public int getRowCount()
    {
        return _keys.length;
    }

    public Object getValueAt(int i, int j)
    {
        if(i < 0 || i >= _keys.length)
            return ((Object) (null));
        switch(j)
        {
        case 0: // '\0'
            return ((Object) (_keys[i]));

        case 1: // '\001'
            return ((Object) (_data.getProperty(_keys[i])));
        }
        return ((Object) (null));
    }

    public boolean isCellEditable(int i, int j)
    {
        return j == 1;
    }

    public void setValueAt(Object obj, int i, int j)
    {
        if(isCellEditable(i, j))
            ((Hashtable) (_data)).put(((Object) (_keys[i])), obj);
    }

    public boolean put(String s, String s1)
    {
        for(int i = 0; i < _keys.length; i++)
            if(_keys[i].equals(((Object) (s))))
            {
                ((Hashtable) (_data)).put(((Object) (s)), ((Object) (s1)));
                ((AbstractTableModel)this).fireTableCellUpdated(i, 1);
                return true;
            }

        return false;
    }

}
