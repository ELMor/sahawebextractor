// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UserOptionTableModel.java

package com.tiani.dicom.ui;

import com.tiani.dicom.iod.DefCallbackUser;
import com.tiani.dicom.iod.UserOption;
import com.tiani.dicom.myassert.Assert;
import javax.swing.table.AbstractTableModel;

public class UserOptionTableModel extends AbstractTableModel
{

    private static final String _HEADERS[] = {
        "Y/N", "Option"
    };
    private static final String _CLASSES[] = {
        "java.lang.Boolean", "java.lang.String"
    };
    private UserOption _options[];
    private DefCallbackUser _callback;

    public UserOptionTableModel(UserOption auseroption[], DefCallbackUser defcallbackuser)
    {
        _options = auseroption;
        _callback = defcallbackuser;
    }

    public String getColumnName(int i)
    {
        return _HEADERS[i];
    }

    public Class getColumnClass(int i)
    {
        try
        {
            return Class.forName(_CLASSES[i]);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            return super.getColumnClass(i);
        }
    }

    public int getColumnCount()
    {
        return _HEADERS.length;
    }

    public int getRowCount()
    {
        return _options.length;
    }

    public boolean isCellEditable(int i, int j)
    {
        return j == 0;
    }

    public void setValueAt(Object obj, int i, int j)
    {
        Assert.equals(0L, j);
        String s = _options[i].option();
        _callback.put(s, ((Boolean)obj).booleanValue());
    }

    public Object getValueAt(int i, int j)
    {
        String s = _options[i].option();
        if(j == 0)
            return ((Object) (new Boolean(_callback.isTrue(s))));
        else
            return ((Object) (s));
    }

}
