// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DicomObjectTableRow.java

package com.tiani.dicom.ui;

import com.archimed.dicom.DDate;
import com.archimed.dicom.DDateRange;
import com.archimed.dicom.DDateTime;
import com.archimed.dicom.DDateTimeRange;
import com.archimed.dicom.DTime;
import com.archimed.dicom.DTimeRange;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.util.Tag;
import java.util.StringTokenizer;
import java.util.Vector;

public final class DicomObjectTableRow
{

    private Tag _tag;
    private DicomObject _dicomObject;
    private DicomObjectTableRow _parent;
    private int _itemIndex;
    private boolean _editable;
    private String _tagPrompt;
    private String _itemPrompt;
    private int _level;

    public DicomObjectTableRow(Tag tag, DicomObject dicomobject, DicomObjectTableRow dicomobjecttablerow, int i, boolean flag)
    {
        _parent = null;
        _itemIndex = -1;
        _itemPrompt = null;
        _level = 0;
        _tag = tag;
        _dicomObject = dicomobject;
        _parent = dicomobjecttablerow;
        _itemIndex = i;
        _editable = flag;
        _initPrompts();
    }

    public boolean isItem()
    {
        return _tag == null;
    }

    public boolean isTypeSQ()
    {
        return _tag != null && _tag.isTypeSQ();
    }

    public void setEditable(boolean flag)
    {
        _editable = flag;
    }

    public Tag getTag()
    {
        return _tag;
    }

    public int getLevel()
    {
        return _level;
    }

    public DicomObjectTableRow getParent()
    {
        return _parent;
    }

    public int getItemIndex()
    {
        return _itemIndex;
    }

    public void incItemIndex()
    {
        _itemPrompt = "ITEM " + (++_itemIndex + 1);
    }

    public void decItemIndex()
    {
        _itemPrompt = "ITEM " + (--_itemIndex + 1);
    }

    public Object getValueAt(int i)
        throws DicomException
    {
        if(_tag == null)
        {
            switch(i)
            {
            case 0: // '\0'
                return ((Object) (new Boolean(_itemIndex < _parent.getSize())));

            case 1: // '\001'
                return ((Object) (_tagPrompt + _itemPrompt));
            }
            return ((Object) (null));
        }
        switch(i)
        {
        case 0: // '\0'
            return ((Object) (new Boolean(getSize() >= 0)));

        case 1: // '\001'
            return ((Object) (_tagPrompt));

        case 2: // '\002'
            return ((Object) (_tag.getDescription()));

        case 3: // '\003'
            return ((Object) (_tag.getTypePrompt()));

        case 4: // '\004'
            return ((Object) (_sizeToString()));

        case 5: // '\005'
            return ((Object) (_valueToString()));
        }
        throw new IllegalArgumentException("no column #" + i);
    }

    public boolean isCellEditable(int i)
    {
        return _editable && (i == 0 || i == 5 && _tag != null && !_tag.isTypeSQ());
    }

    public int getSize()
    {
        return _dicomObject.getSize_ge(_tag.getGroup(), _tag.getElement());
    }

    public DicomObject getDicomObject()
    {
        return _dicomObject;
    }

    public Object getValue(int i)
    {
        return _dicomObject.get_ge(_tag.getGroup(), _tag.getElement(), i);
    }

    public void setValue(Object obj, int i)
        throws DicomException
    {
        _dicomObject.set_ge(_tag.getGroup(), _tag.getElement(), obj, i);
    }

    public void setMultiValue(String s)
        throws DicomException
    {
        Assert.isTrue(!_tag.isTypeSQ());
        StringTokenizer stringtokenizer = new StringTokenizer(s, "\\");
        setNull();
        for(; stringtokenizer.hasMoreTokens(); append(_stringToValue(stringtokenizer.nextToken())));
    }

    public void setNull()
        throws DicomException
    {
        _dicomObject.set_ge(_tag.getGroup(), _tag.getElement(), ((Object) (null)));
    }

    public void append(Object obj)
        throws DicomException
    {
        _dicomObject.append_ge(_tag.getGroup(), _tag.getElement(), obj);
    }

    public Vector deleteItem()
    {
        return _dicomObject.deleteItem_ge(_tag.getGroup(), _tag.getElement());
    }

    public Object deleteItem(int i)
    {
        return _dicomObject.deleteItem_ge(_tag.getGroup(), _tag.getElement(), i);
    }

    private void _initPrompts()
    {
        StringBuffer stringbuffer = new StringBuffer();
        for(DicomObjectTableRow dicomobjecttablerow = _parent; dicomobjecttablerow != null; dicomobjecttablerow = dicomobjecttablerow.getParent())
        {
            stringbuffer.append('>');
            _level++;
        }

        if(_tag != null)
            stringbuffer.append(_tag.toString());
        else
            _itemPrompt = "ITEM " + (_itemIndex + 1);
        _tagPrompt = stringbuffer.toString();
    }

    private Object _stringToValue(String s)
        throws DicomException
    {
        switch(_tag.getTypeCode())
        {
        case 11: // '\013'
            if(s.indexOf('-') != -1)
                return ((Object) (new DDateRange(s)));
            else
                return ((Object) (new DDate(s)));

        case 28: // '\034'
            if(s.indexOf('-') != -1)
                return ((Object) (new DDateTimeRange(s)));
            else
                return ((Object) (new DDateTime(s)));

        case 12: // '\f'
            if(s.indexOf('-') != -1)
                return ((Object) (new DTimeRange(s)));
            else
                return ((Object) (new DTime(s)));
        }
        return ((Object) (s));
    }

    private String _sizeToString()
        throws DicomException
    {
        int i = getSize();
        return i != -1 ? String.valueOf(i) : "-";
    }

    private String _valueToString()
        throws DicomException
    {
        if(_tag.isTypeSQ())
            return null;
        StringBuffer stringbuffer = new StringBuffer();
        int i = getSize();
        int j = _tag.getGroup();
        int k = _tag.getElement();
        for(int l = 0; l < i; l++)
        {
            if(l != 0)
                stringbuffer.append('\\');
            stringbuffer.append(_dicomObject.getS_ge(j, k, l));
        }

        return stringbuffer.toString();
    }
}
