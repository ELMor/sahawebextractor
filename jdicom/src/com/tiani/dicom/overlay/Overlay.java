// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Overlay.java

package com.tiani.dicom.overlay;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;

public final class Overlay
{

    private final int _rows;
    private final int _columns;
    private final int _originRow;
    private final int _originColumn;
    private final int _bitsAllocated;
    private final int _bitPosition;
    private final boolean _imbedded;
    private final int _bytePos;
    private final int _bitmask;
    private final byte _data[];

    public Overlay(DicomObject dicomobject, int i)
        throws DicomException
    {
        _rows = dicomobject.getI_ge(i, 16);
        _columns = dicomobject.getI_ge(i, 17);
        _originRow = dicomobject.getI_ge(i, 80, 0);
        _originColumn = dicomobject.getI_ge(i, 80, 1);
        _bitsAllocated = dicomobject.getI_ge(i, 256);
        _bitPosition = dicomobject.getI_ge(i, 258);
        byte abyte0[] = (byte[])dicomobject.get_ge(i, 12288);
        _data = (_imbedded = abyte0 == null) ? (byte[])dicomobject.get(1184) : abyte0;
        if(_bitsAllocated != 1 && _bitsAllocated != 8 && _bitsAllocated != 16)
            throw new DicomException("Overlay with " + _bitsAllocated + " bits allocated not supported");
        if(_imbedded && dicomobject.getI(461) > 1)
        {
            throw new DicomException("Overlay imbedded into mulit banded pixel data not supported");
        } else
        {
            _bytePos = _bitPosition / 8;
            _bitmask = 1 << _bitPosition % 8;
            return;
        }
    }

    public int getRows()
    {
        return _rows;
    }

    public int getColumns()
    {
        return _columns;
    }

    public int getOriginRow()
    {
        return _originRow;
    }

    public int getOriginColumn()
    {
        return _originColumn;
    }

    public boolean isOverlayAt(int i, int j)
    {
        if(i < 0 || i >= _rows || j < 0 || j >= _columns)
            return false;
        int k = i * _columns + j;
        switch(_bitsAllocated)
        {
        case 1: // '\001'
            return (_data[k / 8] & 1 << (k & 7)) != 0;

        case 8: // '\b'
            return (_data[k] & _bitmask) != 0;

        case 16: // '\020'
            return (_data[k * 2 + _bytePos] & _bitmask) != 0;
        }
        return false;
    }

    public static int[] listOverlayGroups(DicomObject dicomobject)
        throws DicomException
    {
        int ai[] = new int[16];
        int i = 0;
        int j = 0;
        for(int k = 24576; i < ai.length; k++)
        {
            if(dicomobject.getSize_ge(k, 16) > 0)
                ai[j++] = k;
            i++;
            k++;
        }

        int ai1[] = new int[j];
        System.arraycopy(((Object) (ai)), 0, ((Object) (ai1)), 0, j);
        return ai1;
    }

    public static int getFreeOverlayGroup(DicomObject dicomobject)
        throws DicomException
    {
        for(int i = 24576; i < 24832; i += 2)
            if(!((GroupList) (dicomobject)).containsGroup(i))
                return i;

        return -1;
    }
}
