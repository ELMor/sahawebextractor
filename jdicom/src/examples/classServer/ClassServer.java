// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClassServer.java

package examples.classServer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class ClassServer
    implements Runnable
{

    private ServerSocket server;
    private int port;

    protected ClassServer(int i)
        throws IOException
    {
        server = null;
        port = i;
        server = new ServerSocket(i);
        newListener();
    }

    public abstract byte[] getBytes(String s)
        throws IOException, ClassNotFoundException;

    public void run()
    {
        Socket socket;
        try
        {
            socket = server.accept();
        }
        catch(IOException ioexception)
        {
            System.out.println("Class Server died: " + ((Throwable) (ioexception)).getMessage());
            ((Throwable) (ioexception)).printStackTrace();
            return;
        }
        newListener();
        try
        {
            DataOutputStream dataoutputstream = new DataOutputStream(socket.getOutputStream());
            try
            {
                DataInputStream datainputstream = new DataInputStream(socket.getInputStream());
                String s = getPath(datainputstream);
                byte abyte0[] = getBytes(s);
                try
                {
                    dataoutputstream.writeBytes("HTTP/1.0 200 OK\r\n");
                    dataoutputstream.writeBytes("Content-Length: " + abyte0.length + "\r\n");
                    dataoutputstream.writeBytes("Content-Type: application/java\r\n\r\n");
                    ((FilterOutputStream) (dataoutputstream)).write(abyte0);
                    dataoutputstream.flush();
                }
                catch(IOException ioexception2)
                {
                    return;
                }
            }
            catch(Exception exception)
            {
                dataoutputstream.writeBytes("HTTP/1.0 400 " + ((Throwable) (exception)).getMessage() + "\r\n");
                dataoutputstream.writeBytes("Content-Type: text/html\r\n\r\n");
                dataoutputstream.flush();
            }
        }
        catch(IOException ioexception1)
        {
            System.out.println("error writing response: " + ((Throwable) (ioexception1)).getMessage());
            ((Throwable) (ioexception1)).printStackTrace();
        }
        finally
        {
            try
            {
                socket.close();
            }
            catch(IOException ioexception3) { }
        }
    }

    private void newListener()
    {
        (new Thread(((Runnable) (this)))).start();
    }

    private static String getPath(DataInputStream datainputstream)
        throws IOException
    {
        String s = datainputstream.readLine();
        String s1 = "";
        if(s.startsWith("GET /"))
        {
            s = s.substring(5, s.length() - 1).trim();
            int i = s.indexOf(".class ");
            if(i != -1)
                s1 = s.substring(0, i).replace('/', '.');
        }
        do
            s = datainputstream.readLine();
        while(s.length() != 0 && s.charAt(0) != '\r' && s.charAt(0) != '\n');
        if(s1.length() != 0)
            return s1;
        else
            throw new IOException("Malformed Header");
    }
}
