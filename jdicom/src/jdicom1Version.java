// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   jdicom1Version.java


public final class jdicom1Version
{

    public static int versionMajor = 1;
    public static int versionMinor = 7;
    public static int versionPatch = 35;
    public static String type = "beta";

    public jdicom1Version()
    {
    }

    public static String getVersion()
    {
        return "jdicom1 v" + versionMajor + "." + versionMinor + "." + versionPatch;
    }

}
