
package infedoind;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import notdata.DataEnfCaso;
import notdata.DataTab;
import notutil.Comunicador;
import sapp.StubSrvBD;

//import infproto.*;

public class DialInfEdoInd
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;

  final int modoESPERA = 1;
  //Servlet y modos consulta para recuperar datos de caso EDO (salvo protocolo)
  final String strSERVLET_INDIV_SELECT = "servlet/SrvIndivSelect";
  final int servletSELECT_EDO_INVIDIDUALIZADA = 0;
  final int servletSELECT_EDO_INVIDIDUALIZADA_SUCA = 1;
  CLista parametros = null, result = null;
  DataTab dataTab = null; //Datos de caso EDO (sin protocolo)
  String numCasoEdo = "";

  //Para recuperar datos de protocolo
  final String strSERVLET_PROT_CASO = "servlet/SrvProtCaso";
//  protected boolean NoSeEncontraronDatos = false;

  //Servlet de respuestas al protocolo
//  final String strSERVLET_RESP = "servlet/SrvRespEDO";
//  final int servletSELECT = 2;

  protected CLista vLineas = null;

  final String strLOGO_CCAA = "images/ccaa.gif";

  // estructuras de datos
  protected Vector vAgrupaciones;
  protected Vector vTotales;
  protected Vector vCasos;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // Para pedir mas paginas
  protected Vector ultSemana = new Vector();

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  // JRM
  //ERW erw1 = null;
  //ERWClient erwClient1 = null;
  //AppDataHandler dataHandler1 = null;

  CApp app = null;

  Integer regMostrados = new Integer(0);

  //Datos del caso que se usar�n en pag de cabecera EDO y tambi�n para traer datos protocolo
  String sCod1 = ""; //Nivel1 del caso (tabla EDOIND)
  String sCod2 = ""; //Nivel2 del caso
  String sEnfCie = ""; //Enf. CIE

  protected String dsProceso = ""; //descripcion de la enfermedad -> informe

  //Para traer las preguntas y rspuestas del protocolo del caso EDO
  protected CLista listaLineasYRespuestas = null; //Contiene 2 listas
  protected CLista listadatos = null; //lista con lineas
  protected CLista listaValores = null; //lista con respuestas

  /** Lista con las claves de los casos que hay que buscar */
  protected CLista casos = null;

  public DialInfEdoInd(CApp a, String numCas) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("infedoind.Res" + a.getIdioma());
    try {

      stub = new StubSrvBD();
      numCasoEdo = numCas;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public DialInfEdoInd(CApp a, String numCas, String p_dsProceso) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("infedoind.Res" + a.getIdioma());
    try {
      stub = new StubSrvBD();
      numCasoEdo = numCas;

      //DSR
      dsProceso = p_dsProceso;
      //DSR

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // ARG: Nuevo constructor
  public DialInfEdoInd(CApp a, CLista casos) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("infedoind.Res" + a.getIdioma());
    try {
      stub = new StubSrvBD();
      //JRM: Actualizamos la lista de casos de la que hay que obtener los valores.
      this.casos = casos;
      // JRM: Esto es provisional para ver si funciona con un caso.
      numCasoEdo = (String) casos.elementAt(0);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWEDOInd.erw");

    // JRM
    //erw1 = new ERW();
    //erw1.ReadTemplate(app.getCodeBase().toString() + "erw/ERWEDOInd.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // JRM
    //dataHandler1 = new AppDataHandler();
    //erw1.SetDataSource(dataHandler1);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // JRM
    //erwClient1 = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    //JRM
    //this.add(erwClient1.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    ( (CDialog)this).show();
    LlamadaInforme(false);
  }

  // todo el informe
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    ////#// System_out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();
        /*
                param.setFilter(lista.getFilter());
                // obtiene los datos del servidor
                //datosPar.iPagina++;
                param.addElement(datosPar);
                param.setFilter("");
                param.trimToSize();
                stub.setUrl(new URL(app.getURL() + strSERVLET));
                lista = (CLista) stub.doPost(erwCASOS_EDO, param);
                v = (Vector) lista.elementAt(0);
                vTotales = (Vector) lista.elementAt(1);
                for (int j=0; j<v.size(); j++)
                  vCasos.addElement(v.elementAt(j));
                // control de registros
                Integer tot = (Integer) lista.elementAt(1);
                this.setTotalRegistros(tot.toString());
             this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());
         */
        // repintado
        erwClient.refreshReport(true);

        //JRM
        //erwClient1.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

// Rellena los datos de las solapas
  void obtenerDatosPrimeraPagina() {
    CMessage msgBox = null;
    try {

      parametros = new CLista();
      // par�metros que se le pasan a DataEnfCaso: CD_ENFERMO, NM_CASO
//      DataEnfCaso dataEnf = new DataEnfCaso("", (String)hashNotifEDO.get("NM_EDO"));
      DataEnfCaso dataEnf = new DataEnfCaso("", numCasoEdo);
      parametros.addElement(dataEnf);

      parametros.setLogin(app.getLogin());

      parametros.setLortad(app.getParameter("LORTAD"));

      if (app.getIT_TRAMERO().equals("S")) {
        result = Comunicador.Communicate(this.getCApp(),
                                         stub,
                                         servletSELECT_EDO_INVIDIDUALIZADA_SUCA,
                                         strSERVLET_INDIV_SELECT,
                                         parametros);
      }
      else {
        result = Comunicador.Communicate(this.getCApp(),
                                         stub,
                                         servletSELECT_EDO_INVIDIDUALIZADA,
                                         strSERVLET_INDIV_SELECT,
                                         parametros);

        // comprueba que hay datos
      }
      if (result.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg1.Text"));
        msgBox.show();
        msgBox = null;
      }
      else {
        //tengo los datos de la Indiv
        dataTab = (DataTab) result.elementAt(0);
      }
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      e.printStackTrace();
      msgBox.show();
      msgBox = null;

    }

  } //fin de obtenerDatosPrimeraPagina

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  // genera el informe
  public boolean GenerarInformeCompleto() {
    return LlamadaInformeCompleto();
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();
    boolean elBoolean = true;
    int indice = 0; //Recorrido vector preguntas
    //Para hacer listado
    Vector vLineas = new Vector(); //vector de lineas que ir� al informe
    String sBloque = "1";
    String sSeccion = "1";
    DataRespCompInfedoind datRespAPreguntaActual = null;
    String sResp = "";
    boolean llegaListaPresYResp = true;
    boolean bHayLineas = true;

    // plantilla
    TemplateManager tm = null;
    //JRM
    //TemplateManager tm1 = null;

    modoOperacion = modoESPERA;
    Inicializar();
    obtenerDatosPrimeraPagina();

    this.setGenerandoInforme();

    try {

      // Si no hay datos del caso EDO no se saca informe
      if (dataTab == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg2.Text"));
        msgBox.show();
        msgBox = null;
        return false;
      }
      //Obtiene datos de la primera p�gina del caso EDO
      PrepararInforme();

      //Prepara para ir a por datos de protocolo
      listadatos = new CLista();
      listaValores = new CLista();

      llegaListaPresYResp = IraBuscarLineasYRespuestasCaso();
      //Si ha habido error al ir a por lineas no se saca informe
      if (llegaListaPresYResp == false) {
        //Nota: ya se ha sacado mensaje
        return false;
      }

      listaValores = (CLista) (listaLineasYRespuestas.elementAt(0));
      listadatos = (CLista) (listaLineasYRespuestas.elementAt(1));

      //________________________________________________________________

      // No hay lineas de protocolo
      if (listadatos == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg3.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        bHayLineas = false;
      }
      // No hay lineas de protocolo
      else if (listadatos.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg3.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        bHayLineas = false;
      }
      // Hay lineas de protocolo
      else {

        int iTamLista = listadatos.size();

        //recorremos la listadatos
        for (indice = 0; indice < iTamLista; indice++) {
          DataLayout datUnaLinea = (DataLayout) listadatos.elementAt(indice);

          //Cambio de bloque
          if (datUnaLinea.getTipoPreg().equals("X")) {

            if (datUnaLinea.getDesTexto().equals("CNE")) {
              sBloque = res.getString("msg4.Text");
            }
            else if (datUnaLinea.getDesTexto().equals("CA")) {
              sBloque = res.getString("msg5.Text");
            }
            else if (datUnaLinea.getDesTexto().equals("N1")) {
              sBloque = this.app.getNivel1() + " " + sCod1;
            }
            else if (datUnaLinea.getDesTexto().equals("N2")) {
              sBloque = this.app.getNivel2() + " " + sCod2;
            }
          }
          //Cambio de Secci�n
          else if (datUnaLinea.getTipoPreg().equals("D")) {
            sSeccion = datUnaLinea.getDesTexto();
            // System_out.println("Seccion " + datUnaLinea.getDesTexto());
          }
          //Lineas con preguntas  (No son cambios de seccion noi de bloque)
          else {

            //Se va a por la respuesta a la pregunta actual
            boolean encontrado = false;
            for (int i = 0; (i < listaValores.size()) && (encontrado == false);
                 i++) {
              DataRespCompInfedoind dat = (DataRespCompInfedoind) listaValores.
                  elementAt(i);
              //Si se encuentra la respuesta se sale
              if ( (datUnaLinea.getCodModelo().trim()).equals(dat.getCodModelo().
                  trim()) &&
                  (datUnaLinea.getNumLinea().trim()).equals(dat.getNumLinea().
                  trim()))
              //&&(datUnaLinea.getCodPregunta().trim()).equals(dat.getCodPregunta().trim()) )
              {
                encontrado = true;
                datRespAPreguntaActual = dat;
              }
            } //fin for

            //Si hay respuesta a la preg, se ve en la respuesta el tipo de pregunta, y se pone la respuesta
            if (encontrado == true) {
              //Preguntas de tipo car�cter, num�rica o fecha
              if (datRespAPreguntaActual.getTipoPregunta().equals("C") ||
                  datRespAPreguntaActual.getTipoPregunta().equals("N") ||
                  datRespAPreguntaActual.getTipoPregunta().equals("F")) {
                vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                    datUnaLinea.getDesTexto() + " : ",
                    datRespAPreguntaActual.getDesPregunta()));
              }
              //LISTA
              else if (datRespAPreguntaActual.getTipoPregunta().equals("L")) {
                vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                    datUnaLinea.getDesTexto() + " : ",
                    datRespAPreguntaActual.getValorLista()));
              }
              //Booleana
              else if (datRespAPreguntaActual.getTipoPregunta().equals("B")) {
                if (datRespAPreguntaActual.getDesPregunta().equals("S")) {
                  vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                      datUnaLinea.getDesTexto() + " : ",
                      res.getString("msg6.Text")));
                }
                else if (datRespAPreguntaActual.getDesPregunta().equals("N")) {
                  vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                      datUnaLinea.getDesTexto() + " : ",
                      res.getString("msg7.Text")));
                }
              }
            }
            //Si no hay respuesta
            else {
              vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                  datUnaLinea.getDesTexto() + " : ", ""));
            }
//___________________

          } //Fin Lineas que son Preguntas

        } //Fin Recorrido lineas (los DataLayout)

      } //else hay lineas

      //_______________________________________________________________________
      //AIC
      tm = erw.GetTemplateManager();

      //JRM
      //tm1 = erw1.GetTemplateManager();

      if (bHayLineas == true) {

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("BLOQUE = BLOQUE");
        retval.addElement("SECCION = SECCION");
        retval.addElement("PREGUNTA = PREGUNTA");
        retval.addElement("RESPUESTA = RESPUESTA");
        dataHandler.RegisterTable(vLineas, "sive_911", retval, null);

        //JRM
        //dataHandler1.RegisterTable(vLineas, "sive_911", retval, null);
      }
      //No hay lineas
      else {

        //AIC
        //tm = erw.GetTemplateManager();

        //Hace invisible lineas de deatalle
        tm.SetVisible(tm.GetDetailZone("SEC_00"), false);
        Object[][] grupos = tm.GetGroups("SEC_00");
        //Hace invisible cab. de primera agrupaci�n (bloques)
        tm.SetVisible(grupos[0][0], false);
        //Hace invisible cab. de segunda agrupaci�n (secciones)
        tm.SetVisible(grupos[1][0], false);

        //Se ponen dos lineas cualquiera para que funcione
        vLineas.addElement(new DataPregYResp("seccion", "bloque", " ", " "));
        vLineas.addElement(new DataPregYResp("seccion", "bloque2", " ", " "));

        Vector retval = new Vector();
        retval.addElement("BLOQUE = BLOQUE");
        retval.addElement("SECCION = SECCION");
        retval.addElement("PREGUNTA = PREGUNTA");
        retval.addElement("RESPUESTA = RESPUESTA");

        dataHandler.RegisterTable(vLineas, "sive_911", retval, null);

        //JRM
        //dataHandler1.RegisterTable(vLineas, "sive_911", retval, null);

      }

      erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

      //JRM
      //erwClient1.setInputProperties(erw1.GetTemplateManager(), erw1.getDATReader(true));

      // repintado
      //AIC
      this.pack();
      erwClient.prv_setActivePage(0);

      // JRM
      //erwClient1.prv_setActivePage(0);

      //erwClient.refreshReport(true);
      elBoolean = true;

//____________________

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
//_____________________

    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;
  }

  // ARG: Informe con todos los envios
  protected boolean LlamadaInformeCompleto() {
    CMessage msgBox;
    CLista param = new CLista();
    boolean elBoolean = true;
    int indice = 0; //Recorrido vector preguntas
    //Para hacer listado
    Vector vLineas = new Vector(); //vector de lineas que ir� al informe
    String sBloque = "1";
    String sSeccion = "1";
    DataRespCompInfedoind datRespAPreguntaActual = null;
    String sResp = "";
    boolean llegaListaPresYResp = true;
    boolean bHayLineas = true;

    // plantilla
    TemplateManager tm = null;
    // JRM
    //TemplateManager tm1 = null;

    modoOperacion = modoESPERA;
    Inicializar();
    obtenerDatosPrimeraPagina();

    this.setGenerandoInforme();

    try {
      // Si no hay datos del caso EDO no se saca informe
      if (dataTab == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg2.Text"));
        msgBox.show();
        msgBox = null;
        return false;
      }
      //Obtiene datos de la primera p�gina del caso EDO
      PrepararInforme();

      //Prepara para ir a por datos de protocolo
      listadatos = new CLista();
      listaValores = new CLista();

      llegaListaPresYResp = IraBuscarLineasYRespuestasCaso();
      //Si ha habido error al ir a por lineas no se saca informe
      if (llegaListaPresYResp == false) {
        //Nota: ya se ha sacado mensaje
        return false;
      }
      listaValores = (CLista) (listaLineasYRespuestas.elementAt(0));
      listadatos = (CLista) (listaLineasYRespuestas.elementAt(1));

      //________________________________________________________________

      // No hay lineas de protocolo
      if (listadatos == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg3.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        bHayLineas = false;
      }
      // No hay lineas de protocolo
      else if (listadatos.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg3.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        bHayLineas = false;
      }
      // Hay lineas de protocolo
      else {
        int iTamLista = listadatos.size();

        //recorremos la listadatos
        for (indice = 0; indice < iTamLista; indice++) {
          DataLayout datUnaLinea = (DataLayout) listadatos.elementAt(indice);

          //Cambio de bloque
          if (datUnaLinea.getTipoPreg().equals("X")) {

            if (datUnaLinea.getDesTexto().equals("CNE")) {
              sBloque = res.getString("msg4.Text");
            }
            else if (datUnaLinea.getDesTexto().equals("CA")) {
              sBloque = res.getString("msg5.Text");
            }
            else if (datUnaLinea.getDesTexto().equals("N1")) {
              sBloque = this.app.getNivel1() + " " + sCod1;
            }
            else if (datUnaLinea.getDesTexto().equals("N2")) {
              sBloque = this.app.getNivel2() + " " + sCod2;
            }
          }
          //Cambio de Secci�n
          else if (datUnaLinea.getTipoPreg().equals("D")) {
            sSeccion = datUnaLinea.getDesTexto();
            // System_out.println("Seccion " + datUnaLinea.getDesTexto());
          }
          //Lineas con preguntas  (No son cambios de seccion noi de bloque)
          else {

            //Se va a por la respuesta a la pregunta actual
            boolean encontrado = false;
            for (int i = 0; (i < listaValores.size()) && (encontrado == false);
                 i++) {
              DataRespCompInfedoind dat = (DataRespCompInfedoind) listaValores.
                  elementAt(i);
              //Si se encuentra la respuesta se sale
              if ( (datUnaLinea.getCodModelo().trim()).equals(dat.getCodModelo().
                  trim()) &&
                  (datUnaLinea.getNumLinea().trim()).equals(dat.getNumLinea().
                  trim()))
              //&&(datUnaLinea.getCodPregunta().trim()).equals(dat.getCodPregunta().trim()) )
              {
                encontrado = true;
                datRespAPreguntaActual = dat;
              }
            } //fin for

            //Si hay respuesta a la preg, se ve en la respuesta el tipo de pregunta, y se pone la respuesta
            if (encontrado == true) {
              //Preguntas de tipo car�cter, num�rica o fecha
              if (datRespAPreguntaActual.getTipoPregunta().equals("C") ||
                  datRespAPreguntaActual.getTipoPregunta().equals("N") ||
                  datRespAPreguntaActual.getTipoPregunta().equals("F")) {
                vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                    datUnaLinea.getDesTexto() + " : ",
                    datRespAPreguntaActual.getDesPregunta()));
              }
              //LISTA
              else if (datRespAPreguntaActual.getTipoPregunta().equals("L")) {
                vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                    datUnaLinea.getDesTexto() + " : ",
                    datRespAPreguntaActual.getValorLista()));
              }
              //Booleana
              else if (datRespAPreguntaActual.getTipoPregunta().equals("B")) {
                if (datRespAPreguntaActual.getDesPregunta().equals("S")) {
                  vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                      datUnaLinea.getDesTexto() + " : ",
                      res.getString("msg6.Text")));
                }
                else if (datRespAPreguntaActual.getDesPregunta().equals("N")) {
                  vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                      datUnaLinea.getDesTexto() + " : ",
                      res.getString("msg7.Text")));
                }
              }
            }
            //Si no hay respuesta
            else {
              vLineas.addElement(new DataPregYResp(sBloque, sSeccion,
                  datUnaLinea.getDesTexto() + " : ", ""));
            }
            //___________________
          } //Fin Lineas que son Preguntas
        } //Fin Recorrido lineas (los DataLayout)
      } //else hay lineas
      //_______________________________________________________________________
      tm = erw.GetTemplateManager();

      // JRM
      //tm1 = erw.GetTemplateManager();

      if (bHayLineas == true) {
        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("BLOQUE = BLOQUE");
        retval.addElement("SECCION = SECCION");
        retval.addElement("PREGUNTA = PREGUNTA");
        retval.addElement("RESPUESTA = RESPUESTA");
        dataHandler.RegisterTable(vLineas, "sive_911", retval, null);

        //JRM
        //dataHandler1.RegisterTable(vLineas, "sive_911", retval, null);

      }
      //No hay lineas
      else {
        //Hace invisible lineas de deatalle
        tm.SetVisible(tm.GetDetailZone("SEC_00"), false);
        Object[][] grupos = tm.GetGroups("SEC_00");
        //Hace invisible cab. de primera agrupaci�n (bloques)
        tm.SetVisible(grupos[0][0], false);
        //Hace invisible cab. de segunda agrupaci�n (secciones)
        tm.SetVisible(grupos[1][0], false);

        //Se ponen dos lineas cualquiera para que funcione
        vLineas.addElement(new DataPregYResp("seccion", "bloque", " ", " "));
        vLineas.addElement(new DataPregYResp("seccion", "bloque2", " ", " "));

        Vector retval = new Vector();
        retval.addElement("BLOQUE = BLOQUE");
        retval.addElement("SECCION = SECCION");
        retval.addElement("PREGUNTA = PREGUNTA");
        retval.addElement("RESPUESTA = RESPUESTA");
        dataHandler.RegisterTable(vLineas, "sive_911", retval, null);

        //JRM
        //dataHandler1.RegisterTable(vLineas, "sive_911", retval, null);

      }
      erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

      //JRM
      //erwClient1.setInputProperties(erw1.GetTemplateManager(), erw1.getDATReader(true));

      // repintado
      this.pack();
      erwClient.prv_setActivePage(0);

      //JRM
      //erwClient1.prv_setActivePage(0);

      //erwClient.refreshReport(true);
      elBoolean = true;

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
//_____________________

    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;
  }

  /*
   protected boolean IraBuscarLineasCaso(){
    CMessage msgBox = null;
    CLista ListaEntrada  = null;
    DataPetLineas datosEntrada = null;
     try{
       datosEntrada = new DataPetLineas(sEnfCie, app.getCA(), sCod1, sCod2,numCasoEdo, app.getNivel1(),app.getNivel2());
       ListaEntrada = new CLista();
       ListaEntrada.addElement(datosEntrada);
       ListaEntrada.setPerfil(app.getPerfil());
       ListaEntrada.setLogin(app.getLogin());
       stub.setUrl(new URL(app.getURL() + strSERVLET_PROT_CASO));
       vLineas = (CLista) stub.doPost(0, ListaEntrada);
       ListaEntrada = null;
       if (vLineas == null ){
          msgBox = new CMessage (this.app, CMessage.msgAVISO, "No se encontraron datos del protocolo");
          msgBox.show();
          msgBox=null;
          return false;
       }
       //controlar que el primer tipo debe ser descripcion
       else{
          if (vLineas.size() == 0)
             return false;
          else
             return true;
       }
     }catch(Exception e){
      e.printStackTrace();
      msgBox = new CMessage (this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      return false;
     }
   }
   */

  /*
   protected void IraBuscarDatosCaso(){
    CMessage msgBox = null;
    CLista ListaEntrada = null;
     try{
       DataResp datosEntrada = new DataResp(numCasoEdo);
       ListaEntrada = new CLista();
       ListaEntrada.addElement(datosEntrada);
       stub.setUrl(new URL(app.getURL() + strSERVLET_RESP));
       listaValores = (CLista) stub.doPost(servletSELECT, ListaEntrada);
       ListaEntrada = null;
     }catch(Exception e){
      e.printStackTrace();
      msgBox = new CMessage (this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
     }
   }
   */

  protected boolean IraBuscarLineasYRespuestasCaso() {
    CMessage msgBox = null;
    CLista ListaEntrada = null;
    DataPetLineas datosEntrada = null;

    try {
      datosEntrada = new DataPetLineas(sEnfCie, app.getCA(), sCod1, sCod2,
                                       numCasoEdo, app.getNivel1(),
                                       app.getNivel2());
      ListaEntrada = new CLista();
      ListaEntrada.addElement(datosEntrada);
      ListaEntrada.setPerfil(app.getPerfil());
      ListaEntrada.setLogin(app.getLogin());
      ListaEntrada.setIdioma(app.getIdioma());
      stub.setUrl(new URL(app.getURL() + strSERVLET_PROT_CASO));

      listaLineasYRespuestas = (CLista) stub.doPost(0, ListaEntrada);

      /*infedoind.SrvProtCaso srv = new infedoind.SrvProtCaso();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                              "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                              "pista_desa",
                              "pista_desa");
            listaLineasYRespuestas = srv.doDebug(0, ListaEntrada);
       */
      ListaEntrada = null;

      if (listaLineasYRespuestas == null) {
        msgBox = new CMessage(this.app, CMessage.msgERROR,
                              "No se encontraron datos");
        msgBox.show();
        msgBox = null;
        return false;
      }
      return true;

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      return false;
    }
  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    CMessage msgBox = null;
    String sBuffer;
    int iEtiqueta = 0;
    //Para recibir nulos y pasarlos a ""
    String sNom = "";
    String sApe2 = "";
    String sCodSex = "";
    String sFecNac = "";
    String sFecNot = "";
    String sCp = "";
    String sMun = "";
    String sCal = "";
    String sNumCal = "";
    String sPis = "";
    String sTel = "";

    String sCodZbs = "";
    String sDesZbs = "";

    String sFecIni = "";
    String sCol = "";
    String sCenDer = "";
    String sOtr = "";
    String sCla = "";
    String sFecRec = "";

    String sItAso = "";
    String sItDer = "";
    String sItDiaCli = "";
    String sItDiaMic = "";
    String sItDiaSer = "";

    //Para recibir fecha nac y calcular meses y a�os

    java.util.Date dFecNac = null;
    java.util.Date dFecNot = null;

    int anyoNac = 0;
    int anyoNot = 0;
    int mesNac = 0;
    int mesNot = 0;
    int anyos = 0;
    int meses = 0;
    Calendar cal = new GregorianCalendar();

    try {

      // plantilla
      TemplateManager tm = erw.GetTemplateManager();
      //JRM
      //TemplateManager tm1 = erw1.GetTemplateManager();

      // carga los logos
      tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
      tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

      //this.getCApp().getIT_FG_ENFERMO()
      // modificacion jlt
      sEnfCie = dataTab.getCD_ENFCIE();
      dsProceso = dataTab.getDS_PROCESO();
      tm.SetLabel("LABENF", sEnfCie + " " + dsProceso);

      sNom = dataTab.getDS_NOMBRE();
      if (sNom == null) {
        sNom = "";
      }
      sApe2 = dataTab.getDS_APE2();
      if (sApe2 == null) {
        sApe2 = "";
      }
      tm.SetLabel("LABAPE", dataTab.getDS_APE1() + " " + sApe2 + "," + " " +
                  sNom);

      sCodSex = dataTab.getCD_SEXO();
      if (sCodSex == null) {
        sCodSex = "";
      }
      tm.SetLabel("LABSEX", sCodSex);

      //***********************************
       //Edad, a�os, meses
      sFecNac = dataTab.getFC_NAC();
      if (sFecNac == null) {
        sFecNac = "";
      }
      tm.SetLabel("LABFECNAC", dataTab.getFC_NAC());

      //C�lculo de la edad y relleno de datos en elemento si hay fecha nacimiento
      if (!sFecNac.equals("")) {
        sFecNot = dataTab.getFC_FECNOTIF(); //OBLIGATORIO
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        dFecNac = formateador.parse(sFecNac);
        dFecNot = formateador.parse(sFecNot);
        //Se obtiene el a�o y mes de nacimiento
        cal.setTime(dFecNac);
        anyoNac = cal.get(Calendar.YEAR);
        mesNac = cal.get(Calendar.MONTH);
        //Se obtiene a�o y mes de notificaci�n
        cal.setTime(dFecNot);
        anyoNot = cal.get(Calendar.YEAR);
        mesNot = cal.get(Calendar.MONTH);
        //Se ve la diferencia entre fechas en a�os y meses
        if (mesNot > mesNac) {
          anyos = anyoNot - anyoNac;
          meses = mesNot - mesNac;
        }
        else if (mesNot == mesNac) {
          anyos = anyoNot - anyoNac;
          meses = 0;
        }
        else {
          anyos = (anyoNot - anyoNac) - 1;
          meses = (mesNot + 12) - mesNac;
        }

        //Se rellenan datos de edad y sexo del caso EDO
        // indicando la fecha en a�os o meses segun corresponda
        if (anyos <= 0) {
          //Se da la edad en meses
          tm.SetLabel("LABANO", "");
          tm.SetLabel("LABMES", Integer.toString(meses));
        }
        else {
          //Se da la edad en a�os
          tm.SetLabel("LABANO", Integer.toString(anyos));
          tm.SetLabel("LABMES", "");
        }

      }
      //No se tiene dato de la edad
      else {
        tm.SetLabel("LABANO", "");
        tm.SetLabel("LABMES", "");
      }

      sCp = dataTab.getCD_POSTAL_EDOIND();
      if (sCp == null) {
        sCp = "";

      }
      sMun = dataTab.getDS_MUN_EDOIND();
      if (sMun == null) {
        sMun = "";

      }
      sCal = dataTab.getDS_CALLE();
      if (sCal == null) {
        sCal = "";

      }
      sNumCal = dataTab.getDS_NMCALLE();
      if (sNumCal == null) {
        sNumCal = "";

      }
      sPis = dataTab.getDS_PISO_EDOIND();
      if (sPis == null) {
        sPis = "";

      }
      sTel = dataTab.getDS_TELEF();
      if (sTel == null) {
        sTel = "";

      }
      tm.SetLabel("LABCP", sCp);
      tm.SetLabel("LABMUN", sMun);
      tm.SetLabel("LABCAL", sCal);
      tm.SetLabel("LABNUM", sNumCal);
      tm.SetLabel("LABPIS", sPis);
      tm.SetLabel("LABTEL", sTel); //De tabla ENFERMO

      sCod1 = dataTab.getCD_NIVEL_1_EDOIND();
      if (sCod1 == null) {
        sCod1 = "";

      }
      sCod2 = dataTab.getCD_NIVEL_2_EDOIND();
      if (sCod2 == null) {
        sCod2 = "";

      }
      sCodZbs = dataTab.getCD_ZBS_EDOIND();
      if (sCodZbs == null) {
        sCodZbs = "";

      }
      sDesZbs = dataTab.getDS_ZBS_EDOIND();
      if (sDesZbs == null) {
        sDesZbs = "";

      }
      tm.SetLabel("LABZBS", sCod1 + " " + sCod2 + " "
                  + sCodZbs + "  " + sDesZbs);

      sFecIni = dataTab.getFC_INISNT();
      if (sFecIni == null) {
        sFecIni = "";

      }
      sCol = dataTab.getDS_COLECTIVO();
      if (sCol == null) {
        sCol = "";

      }
      sCenDer = dataTab.getDS_CENTRODER();
      if (sCenDer == null) {
        sCenDer = "";

      }
      sOtr = dataTab.getDS_DIAGOTROS();
      if (sOtr == null) {
        sOtr = "";

      }
      sCla = dataTab.getCD_CLASIFDIAG();
      if (sCla == null) {
        sCla = "";

      }
      sFecRec = dataTab.getFC_RECEP();
      if (sFecRec == null) {
        sFecRec = "";

        //Campos de los checkbox: Solo se pone algo si se marc� (hay 'S' en b.datos)
        //Los "N" se dejan en blanco
      }
      sItAso = dataTab.getIT_ASOCIADO();
      if ( (sItAso != null) && (sItAso.equals("S"))) {
        sItAso = "S";
      }
      else {
        sItAso = "";

      }
      sItDer = dataTab.getIT_DERIVADO();
      if ( (sItDer != null) && (sItDer.equals("S"))) {
        sItDer = "S";
      }
      else {
        sItDer = "";

      }
      sItDiaCli = dataTab.getIT_DIAGCLI();
      if ( (sItDiaCli != null) && (sItDiaCli.equals("S"))) {
        sItDiaCli = "S";
      }
      else {
        sItDiaCli = "";

      }
      sItDiaMic = dataTab.getIT_DIAGMICRO();
      if ( (sItDiaMic != null) && (sItDiaMic.equals("S"))) {
        sItDiaMic = "S";
      }
      else {
        sItDiaMic = "";

      }
      sItDiaSer = dataTab.getIT_DIAGSERO();
      if ( (sItDiaSer != null) && (sItDiaSer.equals("S"))) {
        sItDiaSer = "S";
      }
      else {
        sItDiaSer = "";

      }
      tm.SetLabel("LABFECINISIN", sFecIni);
      tm.SetLabel("LABASOOTRCAS", sItAso);

      tm.SetLabel("LABCOL", sCol);
      tm.SetLabel("LABDER", sItDer);
      tm.SetLabel("LABCEN", sCenDer);

      tm.SetLabel("LABCLI", sItDiaCli);
      tm.SetLabel("LABMIC", sItDiaMic);
      tm.SetLabel("LABSER", sItDiaSer);
      tm.SetLabel("LABOTR", sOtr);

      tm.SetLabel("LABCLA", sCla);
      tm.SetLabel("LABFECREC", sFecRec);

      //Ocultaci�n de datos de enfermo si usuario no tiene permiso para ver
      // datos completos del enfermo
      if (this.getCApp().getIT_FG_ENFERMO().equals("N")) {
        //Siglas en vez de nombre y apellidos
        tm.SetLabel("LAB007", "Iniciales enfermo:");
        tm.SetLabel("LABAPE", dataTab.getSIGLAS());
        //Ocultaci�n datos direccion y tel�fono
        tm.SetLabel("LAB009", "");
        tm.SetLabel("LABCP", "");
        tm.SetLabel("LAB010", "");
        tm.SetLabel("LABCAL", "");
        tm.SetLabel("LAB033", "");
        tm.SetLabel("LABNUM", "");
        tm.SetLabel("LAB035", "");
        tm.SetLabel("LABPIS", "");
        tm.SetLabel("LAB011", "");
        tm.SetLabel("LABTEL", "");
      }

      //**************
       //AIC

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }
} //CLASE
