package infedoind;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import infprotoedo.DataLineasM;
import sapp.DBServlet;

//return: listaDevolver con 2 listas: lista con resp edo
//                                    lista con layout

//si fallo: listaDevolver == null
//si no datos de layout:   listaDevolver: -> 2� lista == null
//si no datos en resp edo: listaDevolver: -> 1� lista == null

public class SrvProtCaso
    extends DBServlet {

  protected CLista listaDevolver = null;
  protected CLista listaDataLineasM = null; //lista para la primera query
  protected CLista listaModelos = null; //aprovecho DataLineasM para guardarlo
//  protected CLista listaDataPregunta = null; // lista para la segunda query (DataLayout)
  protected CLista listaDataLayout = null; //lista para la total

  protected CLista listaListas = null; //lista para cargar las Choices L que haya
  protected CLista listaListaValores = null; //ya cargadas

  protected CLista listaSalida = null; //intermedia
  protected CLista listaSalidaResp = null; //las respuestas (DataRespCompInfedoind)

  protected CLista doWork(int opmode, CLista param) throws Exception {

    //variable de control para la secuencialidad
    boolean b = true;

    // objetos de datos
    listaDataLineasM = new CLista();
    listaModelos = new CLista();
    listaDataLayout = new CLista();
//    listaDataPregunta = new CLista();
    listaListas = new CLista();
    listaListaValores = new CLista();
    listaSalida = new CLista();
    listaSalidaResp = new CLista();

    DataLineasM datosLineasM = null;
    //DataLayout datosLayout = null;    //total

    // campos primera consulta
    String sCD_MODELO = "";
    String sNM_LIN = "";
    Integer iNM_LIN = null;
    String sCD_TLINEA = "";
    String sDS_TEXTO = "";
    String sCA = "";
    String sN1 = "";
    String sN2 = "";

    //campos LAYOUT************
    String sCD_TPREG = "";
    //sDS_TEXTO = "";
    String sOBLIG = "";
    String sLONG = "";
    String sENT = "";
    String sDEC = "";
    //sCD_MODELO = "";
    //sNM_LIN = "";
    String sCD_PREGUNTA = "";
    String sIT_COND = "";
    Integer iNM_LIN_COND = null;
    String sNM_LIN_COND = "";
    String sCD_PREGUNTA_COND = "";
    String sDS_PREGUNTA_COND = "";
    String sCD_LISTA = "";
    //*************************

    String des = "";
    String desL = "";

    DataPetLineas datosEntrada = null; //entrada al Servlet ****************************
    DataRespCompInfedoind dataSalidaResp = null;

    //Variables booleanas que indican si un usuario con perfil 3 o 4 (ep area o distrito)
    //est� autorizado para ver protocolo con modelos de area, distrito
    boolean autorizadoNiv1 = false;
    boolean autorizadoNiv2 = false;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    String sQuery = "";
    int i = 0;
    con = openConnection();
    con.setAutoCommit(false); //aunque son selects

    try {

      //cargamos datosbase //es solo un registro
      datosEntrada = (DataPetLineas) param.firstElement();

      /*
//_________Consulta de modelos autorizados para ese usuario puede ver modelo de niv1 o de niv2
              String sBUSQUEDA_NIVEL_1 = "select CD_NIVEL_1 from SIVE_AUTORIZACIONES where  CD_USUARIO = ? and CD_NIVEL_1 = ?";
              String sBUSQUEDA_NIVEL_2 = "select CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ? and CD_NIVEL_1 = ? and CD_NIVEL_2 = ? ";
             switch(param.getPerfil() ) {
               //S. Centrales
               case 1:
                 autorizadoNiv1 = true;
                 autorizadoNiv2 = true;
                 break;
               //Com aut.
               case 2:
                 autorizadoNiv1 = true;
                 autorizadoNiv2 = true;
                 break;
               //Niv. 1
               case 3:
                 st = con.prepareStatement(sBUSQUEDA_NIVEL_1);
                 st.setString(1, param.getLogin().trim());
                 st.setString(2, datosEntrada.getNivelUNO().trim());
                 rs = st.executeQuery();
                 //Si est� ese nivel 1 entre sua autor.
                 if (rs.next() ) {
                 //Tendr� autorizacion en niv 1 y niv 2 indicados
                  autorizadoNiv1 = true;
                  autorizadoNiv2 = true;  //Pues est� autorizado en todos los distritos de sua area
                 }
                 rs.close();
                 rs = null;
                 st.close();
                 st = null;
                 break;
               //Niv. 2
               case 4:
                 st = con.prepareStatement(sBUSQUEDA_NIVEL_2);
                 st.setString(1, param.getLogin().trim());
                 st.setString(2, datosEntrada.getNivelUNO().trim());
                 st.setString(3, datosEntrada.getNivelDOS().trim());
                 rs = st.executeQuery();
                 //Si tiene autorizaciones
                 if (rs.next() ) {
                 //Tendr� autorizacion en niv 1 y niv 2 indicados
                  autorizadoNiv1 = true;
                  autorizadoNiv2 = true;
                 }
                 rs.close();
                 rs = null;
                 st.close();
                 st = null;
                 break;
             }
        // System_out.println("&& FIN AUTORIZACIONES**********");
       */

//_________Consulta de modelos autorizados para ese usuario puede ver modelo de niv1 o de niv2

      //String sBUSQUEDA_NIVEL_1 = "select CD_NIVEL_1 from SIVE_AUTORIZACIONES where  CD_USUARIO = ? and CD_NIVEL_1 = ?";
      //String sBUSQUEDA_NIVEL_2 = "select CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ? and CD_NIVEL_1 = ? and CD_NIVEL_2 = ? ";
      String sBUSQUEDA_NIVEL_1 = "select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where  CD_USUARIO = ? and CD_NIVEL_1 = ?";
      String sBUSQUEDA_NIVEL_2 = "select CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ? and CD_NIVEL_1 = ? and CD_NIVEL_2 = ? ";

      switch (param.getPerfil()) {
        //S. Centrales
        case 1:
          autorizadoNiv1 = true;
          autorizadoNiv2 = true;
          break;
          //Com aut.
        case 2:
          autorizadoNiv1 = true;
          autorizadoNiv2 = true;
          break;
          // Niv. 1 (perfil de epid de area)
        case 3:

          //Si caso est� dado en distrito se ve si el area del caso es el area del usuario
          //En ese caso hay autorizacion para ver modelo de area y distrito
          if (! (datosEntrada.getNivelUNO().trim().equals("")) &&
              ! (datosEntrada.getNivelDOS().trim().equals(""))) {
            st = con.prepareStatement(sBUSQUEDA_NIVEL_1);
            st.setString(1, param.getLogin().trim());
            st.setString(2, datosEntrada.getNivelUNO().trim());
            rs = st.executeQuery();
            //Si est� ese nivel 1 entre sua autor.
            if (rs.next()) {
              //Tendr� autorizacion en niv 1 y niv 2 indicados
              autorizadoNiv1 = true;
              autorizadoNiv2 = true; //Pues est� autorizado en todos los distritos de sua area
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
          }
          //Si caso est� dado en area se ve si el area del caso es el area del usuario
          //En ese caso hay autorizacion para ver modelo de area. Nunca de distrito (imposible haya uno con resptas)
          else if (! (datosEntrada.getNivelUNO().trim().equals("")) &&
                   (datosEntrada.getNivelDOS().trim().equals(""))) {
            st = con.prepareStatement(sBUSQUEDA_NIVEL_1);
            st.setString(1, param.getLogin().trim());
            st.setString(2, datosEntrada.getNivelUNO().trim());
            rs = st.executeQuery();
            //Si est� ese nivel 1 entre sua autor.
            if (rs.next()) {
              //Tendr� autorizacion en niv 1 indicados
              autorizadoNiv1 = true;

            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            autorizadoNiv2 = false;
          }
          //Si el caso est� dado en C Aut�noma no hay autorizci�n para ver modelos area y distrito

          else {
            autorizadoNiv1 = false;
            autorizadoNiv2 = false;
          }
          break;

          //Niv. 2
        case 4:

          //Si caso est� dado en distrito se ve si el area y distrito del caso son los del usuario
          //En ese caso hay autorizacion para ver modelo de area y distrito
          if (! (datosEntrada.getNivelUNO().trim().equals("")) &&
              ! (datosEntrada.getNivelDOS().trim().equals(""))) {
            st = con.prepareStatement(sBUSQUEDA_NIVEL_2);
            st.setString(1, param.getLogin().trim());
            st.setString(2, datosEntrada.getNivelUNO().trim());
            st.setString(3, datosEntrada.getNivelDOS().trim());
            rs = st.executeQuery();
            //Si tiene autorizaciones
            if (rs.next()) {
              //Tendr� autorizacion en niv 1 y niv 2 indicados
              autorizadoNiv1 = true;
              autorizadoNiv2 = true;
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
          }
          //Si caso est� dado en area se ve si el area del caso es el area del usuario
          //En ese caso hay autorizacion para ver modelo de area. Nunca de distrito (imposible haya uno con resptas)
          //Motivo : aunque estemos en diferente distrito, si los dos distritos pertenecen a un mismo area
          //la el modelo del area debe ser visible (ambos distritos usan el mismo)
          else if (! (datosEntrada.getNivelUNO().trim().equals("")) &&
                   (datosEntrada.getNivelDOS().trim().equals(""))) {
            st = con.prepareStatement(sBUSQUEDA_NIVEL_1);
            st.setString(1, param.getLogin().trim());
            st.setString(2, datosEntrada.getNivelUNO().trim());
            rs = st.executeQuery();
            //Si est� ese nivel 1 entre sua autor.
            if (rs.next()) {
              //Tendr� autorizacion en niv 1 indicado
              autorizadoNiv1 = true;

            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            autorizadoNiv2 = false;
          }
          //Si el caso est� dado en C Aut�noma no hay autorizci�n para ver modelos area y distrito
          else {
            autorizadoNiv1 = false;
            autorizadoNiv2 = false;
          }

          break;
      }
      // System_out.println("&& FIN AUTORIZACIONES**********");

      if (!datosEntrada.getNM_EDO().trim().equals("")) {

        //______________ Busca modelos con reptas____________________________________

        // lanza la query
        sQuery = " select "
            + "CD_MODELO, NM_LIN, CD_PREGUNTA, "
            + " DS_RESPUESTA "
            + "from SIVE_RESP_EDO "
            + "where CD_TSIVE = ? AND NM_EDO = ? order by CD_MODELO, NM_LIN";

        st = con.prepareStatement(sQuery);

        st.setString(1, "E");
        Integer iCasoSelect = new Integer(datosEntrada.getNM_EDO().trim());
        st.setInt(2, iCasoSelect.intValue());
        rs = st.executeQuery();

        // extrae los registros encontrados
        while (rs.next()) {

          Integer NumLinWhile = new Integer(rs.getInt("NM_LIN"));

          // a�ade un nodo
          dataSalidaResp = new DataRespCompInfedoind(datosEntrada.getNM_EDO().
              trim(),
              rs.getString("CD_MODELO"),
              NumLinWhile.toString(),
              rs.getString("CD_PREGUNTA"),
              null, null,
              rs.getString("DS_RESPUESTA"), null, null);

          listaSalida.addElement(dataSalidaResp);
          NumLinWhile = null;
        } //fin while
        rs.close();
        rs = null;
        st.close();
        st = null;

        iCasoSelect = null;

        //____________________ Busca pregunta de cada respuesta_________________
        //____________________ y valor elegido si es preg tipo  lista_________________

        //RECOMPONEMOS LA PARTE QUE NOS FALTA  (tipoPregunta)
        for (i = 0; i < listaSalida.size(); i++) {
          DataRespCompInfedoind datars = null;
          dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(i);
          sQuery = "select "
              + " CD_TPREG, CD_LISTA  "
              + " from SIVE_PREGUNTA where "
              + " CD_PREGUNTA = ? ";

          st = con.prepareStatement(sQuery);
          st.setString(1, dataSalidaResp.getCodPregunta()); //cd_pregunta
          rs = st.executeQuery();
          String cd_lista = "";

          while (rs.next()) { //un dato

            //modifico el dataSalidaResp
            listaSalida.removeElementAt(i);
            datars = new DataRespCompInfedoind(dataSalidaResp.getCaso(),
                                               dataSalidaResp.getCodModelo(),
                                               dataSalidaResp.getNumLinea(),
                                               dataSalidaResp.getCodPregunta(),
                                               null, null,
                                               dataSalidaResp.getDesPregunta(),
                                               null, rs.getString("CD_TPREG"));

            cd_lista = rs.getString("CD_LISTA");
            listaSalida.insertElementAt(datars, i);

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //el mismo dato
          if (datars.getTipoPregunta().equals("L")) {
            dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(i);
            sQuery = "select "
                + " DS_LISTAP, DSL_LISTAP  "
                + "from SIVE_LISTA_VALORES where "
                + " CD_LISTA = ? and CD_LISTAP = ? ";
            st = con.prepareStatement(sQuery);
            st.setString(1, cd_lista);
            st.setString(2, dataSalidaResp.getDesPregunta()); //cd_listap
            rs = st.executeQuery();

            String valor = "";
            while (rs.next()) {
              //modifico el dataSalidaResp
              listaSalida.removeElementAt(i);
              des = rs.getString("DS_LISTAP");
              desL = rs.getString("DSL_LISTAP");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desL != null)) {
                valor = desL;
              }
              else {
                valor = des;
              }
              listaSalida.insertElementAt
                  (new DataRespCompInfedoind(dataSalidaResp.getCaso(),
                                             dataSalidaResp.getCodModelo(),
                                             dataSalidaResp.getNumLinea(),
                                             dataSalidaResp.getCodPregunta(),
                                             null, null,
                                             dataSalidaResp.getDesPregunta(),
                                             valor,
                                             dataSalidaResp.getTipoPregunta()),
                   i);
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
          } //if si es lista

        } //for

      } //fin de if de caso informado

      //AIC
      if (listaSalida.size() == 0) {
        b = false;
        listaDataLayout = null;
      }

      //_____________________Se a�aden datos de nivel del modelo en cada respuesta________________________________

      //veo si esos modelos son obligatorios y su nivel
      for (i = 0; i < listaSalida.size(); i++) {
        dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(i);
        sQuery = "SELECT CD_NIVEL_1, CD_NIVEL_2, "
            + " CD_CA , IT_OK FROM SIVE_MODELO WHERE "
            + " CD_TSIVE = ? AND CD_MODELO = ?";

        st = con.prepareStatement(sQuery);
        st.setString(1, "E");
        st.setString(2, dataSalidaResp.getCodModelo());
        rs = st.executeQuery();
        String n1 = null;
        String n2 = null;
        String ca = null;
        String nivel = null; //"CNE"; "CA"; "N1", "N2"
        while (rs.next()) {

          //modifico el dataSalidaResp
          listaSalida.removeElementAt(i);
          n1 = rs.getString("CD_NIVEL_1");
          n2 = rs.getString("CD_NIVEL_2");
          ca = rs.getString("CD_CA");
          //"CNE"; "CA"; "N1", "N2"
          if (n1 == null && n2 == null && ca == null) {
            nivel = "CNE";
          }
          else if (n1 == null && n2 == null && ca != null) {
            nivel = "CA";
          }
          else if (n1 != null && n2 == null && ca != null) {
            nivel = "N1";
          }
          else if (n1 != null && n2 != null && ca != null) {
            nivel = "N2";

          }

          listaSalida.insertElementAt
              (new DataRespCompInfedoind(dataSalidaResp.getCaso(),
                                         dataSalidaResp.getCodModelo(),
                                         dataSalidaResp.getNumLinea(),
                                         dataSalidaResp.getCodPregunta(), nivel,
                                         rs.getString("IT_OK"),
                                         dataSalidaResp.getDesPregunta(),
                                         dataSalidaResp.getValorLista(),
                                         dataSalidaResp.getTipoPregunta()), i);
        } //while

      } //for
      //------------------------------------------------

//___________________ Ordena la lista de respuestas de forma que vayan modelos en orden CNE,CA,N1,N2 __________________________________

      //la salida de datos (igual a listaCopia)
      CLista listaSalidaResp = new CLista();
      for (i = 0; i < listaSalida.size(); i++) {
        dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(i);
        listaSalidaResp.addElement(dataSalidaResp);
      } //for

      //ordenamos la lista por niveles
      CLista listaCopia = new CLista();
      for (i = 0; i < listaSalida.size(); i++) {
        dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(i);
        listaCopia.addElement(dataSalidaResp);
      } //for
      listaSalida = new CLista();
      boolean bExiste = false;
      for (i = 0; i < listaCopia.size(); i++) {
        dataSalidaResp = (DataRespCompInfedoind) listaCopia.elementAt(i);
        if (dataSalidaResp.getNivel().trim().equals("CNE")) {
          listaSalida.addElement(dataSalidaResp);
          bExiste = true;
          break;
        }
      }
      if (!bExiste) {
        listaSalida.addElement(new DataRespCompInfedoind(null, null, null, null,
            "", null, null, null, null));
      }
      bExiste = false;
      for (i = 0; i < listaCopia.size(); i++) {
        dataSalidaResp = (DataRespCompInfedoind) listaCopia.elementAt(i);
        if (dataSalidaResp.getNivel().trim().equals("CA")) {
          listaSalida.addElement(dataSalidaResp);
          bExiste = true;
          break;
        }
      }
      if (!bExiste) {
        listaSalida.addElement(new DataRespCompInfedoind(null, null, null, null,
            "", null, null, null, null));
      }
      bExiste = false;
      for (i = 0; i < listaCopia.size(); i++) {
        dataSalidaResp = (DataRespCompInfedoind) listaCopia.elementAt(i);
        if (dataSalidaResp.getNivel().trim().equals("N1")) {
          listaSalida.addElement(dataSalidaResp);
          bExiste = true;
          break;
        }
      }
      if (!bExiste) {
        listaSalida.addElement(new DataRespCompInfedoind(null, null, null, null,
            "", null, null, null, null));
      }
      bExiste = false;
      for (i = 0; i < listaCopia.size(); i++) {
        dataSalidaResp = (DataRespCompInfedoind) listaCopia.elementAt(i);
        if (dataSalidaResp.getNivel().trim().equals("N2")) {
          listaSalida.addElement(dataSalidaResp);
          bExiste = true;
          break;
        }
      }
      if (!bExiste) {
        listaSalida.addElement(new DataRespCompInfedoind(null, null, null, null,
            "", null, null, null, null));
        //------------------------------------------------
      }
      listaCopia = null;

//_________OBTIENE LINEAS DE CADA UNO DE LOS 4 POSIBLES MODELOS ____________________________________________________________________
//_________Puede coger el que tiene resptas o ,si no hay, el activo (podria no haberlo tampoco)______
//

      //hacemos 4 consultas
      //CNE!!!!!!
      //!!!!!!!!!

      dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(0);
      if (dataSalidaResp.getNivel().trim().equals("CNE")) {

        sQuery = " select "
            + "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO "
            + "from SIVE_LINEASM "
            +
            "where CD_TSIVE = ? and  CD_MODELO = ? order by  CD_MODELO, NM_LIN";

        st = con.prepareStatement(sQuery);
        st.setString(1, "E");
        st.setString(2, dataSalidaResp.getCodModelo().trim());

      }
      else {
        sQuery = " select "
            + "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  "
            + "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 "
            + "from SIVE_MODELO a, SIVE_LINEASM b  "
            +
            "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) "
            + "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)"
            + "and (a.CD_ENFCIE = ?) "
            + "and ( "
            +
            "(a.CD_CA is null and a.CD_NIVEL_1 is null and a.CD_NIVEL_2 is null)  "
            + ") "
            + ") "
            + "order by b.CD_MODELO, b.NM_LIN";

        st = con.prepareStatement(sQuery);
        st.setString(1, "E");
        st.setString(2, "S");
        st.setString(3, datosEntrada.getCodEnfermedad().trim());
      } //else de cne

      rs = st.executeQuery();

      // extrae el registro encontrado y lo carga en listaDataLineasM
      while (rs.next()) {
        des = rs.getString("DS_TEXTO");
        desL = rs.getString("DSL_TEXTO");

        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
            && (desL != null)) {
          sDS_TEXTO = desL;
        }
        else {
          sDS_TEXTO = des;

        }
        sCD_MODELO = rs.getString("CD_MODELO");
        iNM_LIN = new Integer(rs.getInt("NM_LIN"));
        sNM_LIN = iNM_LIN.toString();
        sCD_TLINEA = rs.getString("CD_TLINEA");
        sCA = null;
        sN1 = null;
        sN2 = null;

        // a�ade un nodo
        listaDataLineasM.addElement(new DataLineasM
                                    (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                     sCA, sN1, sN2));
      } //while
      rs.close();
      rs = null;
      st.close();
      st = null;

      //CA!!!!!!!
      //!!!!!!!!!
      dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(1);
      if (dataSalidaResp.getNivel().trim().equals("CA")) {

        sQuery = " select " +
            "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
            "from SIVE_LINEASM " +
            "where CD_TSIVE = ? and  CD_MODELO = ? order by  CD_MODELO, NM_LIN";

        st = con.prepareStatement(sQuery);
        st.setString(1, "E");
        st.setString(2, dataSalidaResp.getCodModelo().trim());

      }
      else {
        sQuery = " select " +
            "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
            "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
            "from SIVE_MODELO a, SIVE_LINEASM b  " +
            "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
            "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
            "and (a.CD_ENFCIE = ?) " +
            "and ( " +
            "(a.CD_CA = ? and a.CD_NIVEL_1 is null and a.CD_NIVEL_2 is null)  " +
            ") " +
            ") " +
            "order by b.CD_MODELO, b.NM_LIN";

        st = con.prepareStatement(sQuery);
        st.setString(1, "E");
        st.setString(2, "S");
        st.setString(3, datosEntrada.getCodEnfermedad().trim());
        st.setString(4, datosEntrada.getComunidad().trim());
      } //else de cne

      rs = st.executeQuery();

      // extrae el registro encontrado y lo carga en listaDataLineasM
      while (rs.next()) {
        des = rs.getString("DS_TEXTO");
        desL = rs.getString("DSL_TEXTO");

        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
            && (desL != null)) {
          sDS_TEXTO = desL;
        }
        else {
          sDS_TEXTO = des;

        }
        sCD_MODELO = rs.getString("CD_MODELO");
        iNM_LIN = new Integer(rs.getInt("NM_LIN"));
        sNM_LIN = iNM_LIN.toString();
        sCD_TLINEA = rs.getString("CD_TLINEA");
        sCA = datosEntrada.getComunidad().trim();
        sN1 = null;
        sN2 = null;

        // a�ade un nodo
        listaDataLineasM.addElement(new DataLineasM
                                    (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                     sCA, sN1, sN2));
      } //while
      rs.close();
      rs = null;
      st.close();
      st = null;

      if (autorizadoNiv1 == true) {

        //N1!!!!!!!
        //!!!!!!!!!
        dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(2);
        if (dataSalidaResp.getNivel().trim().equals("N1")) {

          sQuery = " select " +
              "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
              "from SIVE_LINEASM " +
              "where CD_TSIVE = ? and  CD_MODELO = ? order by  CD_MODELO, NM_LIN";

          st = con.prepareStatement(sQuery);
          st.setString(1, "E");
          st.setString(2, dataSalidaResp.getCodModelo().trim());

        }
        else {
          sQuery = " select " +
              "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
              "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
              "from SIVE_MODELO a, SIVE_LINEASM b  " +
              "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
              "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
              "and (a.CD_ENFCIE = ?) " +
              "and ( " +
              "(a.CD_CA = ? and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 is null)  " +
              ") " +
              ") " +
              "order by b.CD_MODELO, b.NM_LIN";

          st = con.prepareStatement(sQuery);
          st.setString(1, "E");
          st.setString(2, "S");
          st.setString(3, datosEntrada.getCodEnfermedad().trim());
          st.setString(4, datosEntrada.getComunidad().trim());
          st.setString(5, datosEntrada.getNivelUNO().trim());
        } //else de cne

        rs = st.executeQuery();

        // extrae el registro encontrado y lo carga en listaDataLineasM
        while (rs.next()) {
          des = rs.getString("DS_TEXTO");
          desL = rs.getString("DSL_TEXTO");

          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
              && (desL != null)) {
            sDS_TEXTO = desL;
          }
          else {
            sDS_TEXTO = des;

          }
          sCD_MODELO = rs.getString("CD_MODELO");
          iNM_LIN = new Integer(rs.getInt("NM_LIN"));
          sNM_LIN = iNM_LIN.toString();
          sCD_TLINEA = rs.getString("CD_TLINEA");
          sCA = datosEntrada.getComunidad().trim();
          sN1 = datosEntrada.getNivelUNO().trim();
          sN2 = null;

          // a�ade un nodo
          listaDataLineasM.addElement(new DataLineasM
                                      (sCD_MODELO, sNM_LIN, sCD_TLINEA,
                                       sDS_TEXTO,
                                       sCA, sN1, sN2));
        } //while
        rs.close();
        rs = null;
        st.close();
        st = null;

      } //if niv1 aut

      //Solo oge las resptas de niv 2 si niv2 est� autorizado
      if (autorizadoNiv2 == true) {
        //N2!!!!!!!
        //!!!!!!!!!
        dataSalidaResp = (DataRespCompInfedoind) listaSalida.elementAt(3);
        if (dataSalidaResp.getNivel().trim().equals("N2")) {

          sQuery = " select " +
              "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
              "from SIVE_LINEASM " +
              "where CD_TSIVE = ? and  CD_MODELO = ? order by  CD_MODELO, NM_LIN";

          st = con.prepareStatement(sQuery);
          st.setString(1, "E");
          st.setString(2, dataSalidaResp.getCodModelo().trim());

        }
        else {
          sQuery = " select " +
              "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
              "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
              "from SIVE_MODELO a, SIVE_LINEASM b  " +
              "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
              "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
              "and (a.CD_ENFCIE = ?) " +
              "and ( " +
              "(a.CD_CA = ? and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 = ?)  " +
              ") " +
              ") " +
              "order by b.CD_MODELO, b.NM_LIN";

          st = con.prepareStatement(sQuery);
          st.setString(1, "E");
          st.setString(2, "S");
          st.setString(3, datosEntrada.getCodEnfermedad().trim());
          st.setString(4, datosEntrada.getComunidad().trim());
          st.setString(5, datosEntrada.getNivelUNO().trim());
          st.setString(6, datosEntrada.getNivelDOS().trim());
        } //else de cne

        rs = st.executeQuery();

        // extrae el registro encontrado y lo carga en listaDataLineasM
        while (rs.next()) {
          des = rs.getString("DS_TEXTO");
          desL = rs.getString("DSL_TEXTO");

          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
              && (desL != null)) {
            sDS_TEXTO = desL;
          }
          else {
            sDS_TEXTO = des;

          }
          sCD_MODELO = rs.getString("CD_MODELO");
          iNM_LIN = new Integer(rs.getInt("NM_LIN"));
          sNM_LIN = iNM_LIN.toString();
          sCD_TLINEA = rs.getString("CD_TLINEA");
          sCA = datosEntrada.getComunidad().trim();
          sN1 = datosEntrada.getNivelUNO().trim();
          sN2 = datosEntrada.getNivelDOS().trim();

          // a�ade un nodo
          listaDataLineasM.addElement(new DataLineasM
                                      (sCD_MODELO, sNM_LIN, sCD_TLINEA,
                                       sDS_TEXTO,
                                       sCA, sN1, sN2));
        } //while
        rs.close();
        rs = null;
        st.close();
        st = null;

      }

      if ( (listaDataLineasM.size() == 0) || !b) {
        b = false;
        listaDataLayout = null;
      }
      //TENEMOS !!!!!!!!!!!!!!!!!!!!!!!!!!!
      //lista de Salida con dataSalidaResp (respedo)
      // listaDataLineasM con los modelos
      if ( (listaSalida.size() == 0) || !b) {
        b = false;
        listaDataLayout = null;
      }

//Busca los modelos con lineas
      //NUEVA ETAPA********************************
      if (b) {
        EncontrarModelos(); // lo carga en Modelos, con DataLineasM

      } //fin de b

//___________________  Recompone la lista de salida final  (con el DataLayout)

      //NUEVA ETAPA************
      if (b) { //si sigue siendo true
        RecomponerLista();
      }

      listaDevolver = new CLista();
      listaSalidaResp.trimToSize();
      listaDevolver.addElement(listaSalidaResp);
      listaDevolver.addElement(listaDataLayout);
      listaDevolver.trimToSize();

      // valida la transacci�n
      con.commit();

      /*
       System_out.println("Usuario "+ param.getLogin());
       System_out.println("CA "+ datosEntrada.getComunidad());
       System_out.println("N1 "+ datosEntrada.getNivelUNO());
       System_out.println("N2 "+ datosEntrada.getNivelDOS());
       System_out.println("caso "+ datosEntrada.getNM_EDO());
       if (autorizadoNiv1 == true)
         System_out.println("Autorizado N1 ");
       if (autorizadoNiv2 == true)
         System_out.println("Autorizado N2 ");
       System_out.println(" tam listaDevolver ");
       if (listaDevolver != null)
        System_out.println(listaDevolver.size());
       System_out.println(" tam listaSalidaResp ");
       if (listaSalidaResp != null)
        System_out.println(listaSalidaResp.size());
       System_out.println(" tam listaDataLayout ");
       if (listaDataLayout != null)
        System_out.println(listaDataLayout.size());
       System_out.println(" tam listaModelos ");
       if (listaModelos != null)
        System_out.println(listaModelos.size());
       System_out.println(" tam listaSalida ");
       if (listaSalida != null)
        System_out.println(listaSalida.size());
       System_out.println(" tam listaDataLineasm ");
       if (listaDataLineasM != null)
        System_out.println(listaDataLineasM.size());
       */

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaDevolver = null;
      throw ex;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    return listaDevolver;

  } //fin doPost

//encontrar cd_modelo y en el orden.  (Modelos: 1, 3, 2)
  protected void EncontrarModelos() {
    String sAlm = "";

    for (int indice = 0; indice < listaDataLineasM.size(); indice++) {
      DataLineasM datosParaBusqueda = (DataLineasM) listaDataLineasM.elementAt(
          indice);
      if (!sAlm.equals(datosParaBusqueda.getCodModelo())) { //modelo diferente
        listaModelos.addElement(new DataLineasM
                                (datosParaBusqueda.getCodModelo()));
      }
      sAlm = datosParaBusqueda.getCodModelo();

    }
  }

  protected String ConsultaLista() {

    return ("select "
            + "CD_LISTAP, DS_LISTAP, DSL_LISTAP "
            + "from SIVE_LISTA_VALORES "
            + "where "
            + "CD_LISTA = ? ");
  }

  protected void RecomponerLista() {

    boolean b = true;
    //leemos una lineaM: si D, "D", null, null...add
    //                   si P, leemos...add

    int iListaLineasM = 0; //ppal
    int iListaPregunta = 0; //para movernos por la lista secundaria

    //tipos de registros
    String sTipoRegistro = "";
    String sAlmacen = "-1";

    for (iListaLineasM = 0; iListaLineasM < listaDataLineasM.size();
         iListaLineasM++) {
      if (b) {
        DataLineasM datosLinea = (DataLineasM) listaDataLineasM.elementAt(
            iListaLineasM);
        //si la primera no es descripcion, lista=null------------------------
        if (iListaLineasM == 0 && !datosLinea.getTipoLinea().equals("D")) {

          if (b) {
            b = false;
          }
        }
        //si es una descripcion, a a�adimos a listaDataLayout----------------
        if (datosLinea.getTipoLinea().equals("D")) {

          //vemos en que tipo de registro estamos
          if (datosLinea.getComunidad() == null &&
              datosLinea.getNivel1() == null &&
              datosLinea.getNivel2() == null) {
            sTipoRegistro = "CNE";
          }
          else if (datosLinea.getComunidad() != null &&
                   datosLinea.getNivel1() == null &&
                   datosLinea.getNivel2() == null) {
            sTipoRegistro = "CA";
          }
          else if (datosLinea.getComunidad() != null &&
                   datosLinea.getNivel1() != null &&
                   datosLinea.getNivel2() == null) {
            sTipoRegistro = "N1";
          }
          else if (datosLinea.getComunidad() != null &&
                   datosLinea.getNivel1() != null &&
                   datosLinea.getNivel2() != null) {
            sTipoRegistro = "N2";
          }

          //a�adimos una Descripcion de tipo X: CNE, CA, N1, N2
          //se a�ade cuando el registro es diferente del almacen
          if (!sAlmacen.trim().equals(sTipoRegistro.trim())) {

            listaDataLayout.addElement(new DataLayout
                                       ("X", sTipoRegistro.trim(), null, null, null, null,
                                        null, null,
                                        null, null, null, null, null, null, null));
            sAlmacen = sTipoRegistro;
          }

          //a�adimos D en tipoPregunta

          listaDataLayout.addElement(new DataLayout
                                     ("D",
                                      datosLinea.getDesTexto(), null, null, null, null,
                                      datosLinea.getCodModelo(),
                                      datosLinea.getNmLinea(),
                                      null, null, null, null, null, null, null));

        }

        //si es una pregunta--------------------------------------------------
        if (datosLinea.getTipoLinea().equals("P")) {

          //a�adimos a lista definitiva
          if (b) {

            listaDataLayout.addElement(new DataLayout
                                       ("P",
                                        datosLinea.getDesTexto(), null, null, null, null,
                                        datosLinea.getCodModelo(),
                                        datosLinea.getNmLinea(),
                                        null, null, null, null, null, null, null));

          } //fin de a�adir  (b)
        } //P ---------------------------------------------------------------------

      } //continuamos si es true!!
    } //fin for
    if (!b) {
      listaDataLayout = null;
    }
    else {
      for (int i = 0; i < listaDataLayout.size(); i++) {
        DataLayout dat = (DataLayout) (listaDataLayout.elementAt(i));
      }

    }

  }

} //fin de la clase
