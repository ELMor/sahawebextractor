//Estructura de datos para asociar cada pregunta con su respuesta

package infedoind;

import java.io.Serializable;

public class DataPregYResp
    implements Serializable {

  public String BLOQUE = "";
  public String SECCION = "";
  public String PREGUNTA = "";
  public String RESPUESTA = "";

  public DataPregYResp() {
  }

  public DataPregYResp(String sBlo, String sSec, String sPre, String sRes) {

    BLOQUE = sBlo;
    SECCION = sSec;
    PREGUNTA = sPre;
    RESPUESTA = sRes;

  }

}