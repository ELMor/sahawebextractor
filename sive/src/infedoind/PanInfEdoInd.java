//Title:        Your Product Name
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:  Your description

package infedoind;

import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CPanel;

public class PanInfEdoInd
    extends CPanel {
  XYLayout xYLayout1 = new XYLayout();
  Label lblCaso = new Label();
  TextField txtCaso = new TextField();

  public PanInfEdoInd(CApp a) {
    try {
      setApp(a);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    xYLayout1.setHeight(300);
    lblCaso.setText("Caso EDO");
    txtCaso.addActionListener(new PanInfEdoInd_txtCaso_actionAdapter(this));
    xYLayout1.setWidth(439);
    this.setLayout(xYLayout1);
    this.add(lblCaso, new XYConstraints(64, 56, 71, -1));
    this.add(txtCaso, new XYConstraints(139, 57, 146, -1));
  }

  void txtCaso_actionPerformed(ActionEvent e) {
    DialInfEdoInd dlgInforme = new DialInfEdoInd(app, txtCaso.getText().trim());
    dlgInforme.setEnabled(true);
    if (dlgInforme.GenerarInforme()) {
      dlgInforme.show();
    }
  }

  public void Inicializar() {
  }

}

class PanInfEdoInd_txtCaso_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanInfEdoInd adaptee;
  ActionEvent e = null;

  PanInfEdoInd_txtCaso_actionAdapter(PanInfEdoInd adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecución para servir el evento
  public void run() {
    adaptee.txtCaso_actionPerformed(e);
  }
}
