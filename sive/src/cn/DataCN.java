package cn;

import java.io.Serializable;

public class DataCN
    implements Serializable {
  protected String sCD_CENTRO = "";
  protected String sCD_PROV = "";
  protected String sCD_MUN = "";
  protected String sDS_CENTRO = "";
  protected String sDS_DIREC = "";
  protected String sDS_NUM = "";
  protected String sDS_PISO = "";
  protected String sCD_POSTAL = "";
  protected String sDS_TELEF = "";
  protected String sDS_FAX = "";
  protected String sCD_NIVASIS = "";
  protected String sIT_COBERTURA = "";
  protected String sFC_ALTA = "";
  protected String sCD_OPE = "";
  protected String sIT_BAJA = "";
  protected String sAN_DESDE = "";
  //protected String sAN_HASTA = "";
  protected String sSEM_DESDE = "";
  //protected String sSEM_HASTA = "";
  protected String sDesMun = "";
  protected String sDesProv = "";
  protected String sDesLProv = "";

  public DataCN(String codCentro) {
    sCD_CENTRO = codCentro;
  }

  public DataCN(String codCentro, String prov, String mun, String dsCen,
                String direccion, String dsNum, String dsPiso, String cdPostal,
                String telef, String fax,
                String nivAsis, String cober, String fAlta, String cdOperador,
                String esBaja,
                String anDesde, String semDesde) {

    sCD_CENTRO = codCentro;
    sCD_PROV = prov;
    sCD_MUN = mun;
    sDS_CENTRO = dsCen;
    sDS_DIREC = direccion;
    sDS_NUM = dsNum;
    sDS_PISO = dsPiso;
    sCD_POSTAL = cdPostal;
    sDS_TELEF = telef;
    sDS_FAX = fax;
    sCD_NIVASIS = nivAsis;
    sIT_COBERTURA = cober;
    sFC_ALTA = fAlta;
    sCD_OPE = cdOperador;
    sIT_BAJA = esBaja;
    sAN_DESDE = anDesde;
    //sAN_HASTA = anHasta;
    sSEM_DESDE = semDesde;
    //sSEM_HASTA = semHasta;
  }

  public String getAnDesde() {
    return sAN_DESDE;
  }

  /*public String getAnHasta() {
    return sAN_HASTA;
     }*/
  public String getSemDesde() {
    return sSEM_DESDE;
  }

  /*public String getSemHasta() {
    return sSEM_HASTA;
     }*/

  public String getCodCentro() {
    return sCD_CENTRO;
  }

  public String getProv() {
    return sCD_PROV;
  }

  public String getMun() {
    return sCD_MUN;
  }

  public String getCentroDesc() {
    return sDS_CENTRO;
  }

  public String getDireccion() {
    return sDS_DIREC;
  }

  public String getNum() {
    return sDS_NUM;
  }

  public String getPiso() {
    return sDS_PISO;
  }

  public String getCPostal() {
    return sCD_POSTAL;
  }

  public String getTelefono() {
    return sDS_TELEF;
  }

  public String getFax() {
    return sDS_FAX;
  }

  public String getNivAsis() {
    return sCD_NIVASIS;
  }

  public String getCobertura() {
    return sIT_COBERTURA;
  }

  public String getAlta() {
    return sFC_ALTA;
  }

  public String getCdOperador() {
    return sCD_OPE;
  }

  public String getBaja() {
    return sIT_BAJA;
  }

  public String getDesMun() {
    return sDesMun;
  }

  public void setDesMun(String a_desMun) {
    sDesMun = a_desMun;
  }

  public String getDesProv() {
    return sDesProv;
  }

  public void setDesProv(String a_desProv) {
    sDesProv = a_desProv;
  }

  public String getDesLProv() {
    return sDesLProv;
  }

  public void setDesLProv(String a_desLProv) {
    sDesLProv = sDesLProv;
  }

} //__________________________________ END CLASS
