package cn;

import java.util.ResourceBundle;

import capp.CApp;

public class cn
    extends CApp {

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("cn.Res" + this.getIdioma());
    CApp a = (CApp)this;
    setTitulo(res.getString("msg1.Text"));
    VerPanel("", new PanelBusca(a));
  }

}