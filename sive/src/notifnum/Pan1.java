package notifnum;

import java.util.Hashtable;

import java.awt.Dimension;
import java.awt.ScrollPane;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CPanel;

public class Pan1
    extends CPanel {

  //ancho del arbol
  public int iAncho = 0;

  ScrollPane panelScroll = new ScrollPane();

  public Pan2 pan2;

  protected Hashtable hashnotif = null;
  protected CCargadorImagen imgs = null;

  //final String imgNAME[] = {"images/view.gif",
  //  "images/view2.gif"};

  XYLayout xYLayout1 = new XYLayout();

  DialNum dialogo = null;
  int mod;
  boolean datosEncontrados = false;

  public Pan1(CApp a, Hashtable hashNotifEDO,
              DialNum dlg, int modos) {
    try {

      app = a;
      hashnotif = hashNotifEDO;
      dialogo = dlg;
      mod = modos;
      // formulario EDO
      pan2 = new Pan2(app, hashnotif, this, dialogo, mod);
      if (!pan2.hayDatos()) {
        datosEncontrados = true;
      }
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();

    }
  }

  void jbInit() throws Exception {
    // Sin relieve
    this.setBorde(false);

    // carga las im�genes
    //imgs = new CCargadorImagen(app, imgNAME);
    //imgs.CargaImagenes();

    //mlm
    //panelEDO
    this.setSize(new Dimension(390, 250));
    xYLayout1.setHeight(250);
    xYLayout1.setWidth(390); //el panel grande
    this.setLayout(xYLayout1);
    panelScroll.add(pan2); //panel_Informe

    this.add(panelScroll, new XYConstraints(0, 0, 390, 250)); //ponia 315

    this.doLayout();
  }

  public void Inicializar() {
  }

  public CLista devuelveDatos() {
    return pan2.devuelveDatos();
  }

  public boolean hayDatos() {
    return datosEncontrados;
  }
}
