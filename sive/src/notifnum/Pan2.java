package notifnum;

import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import sapp.StubSrvBD;

//Los datos de entrada: Enfermedad y niveles,
//hay que meterlos, en getEnfermedad y en IraBuscarDatos y
//Componentes(N1, N2)
//NOTA: espero que me pasen la descripcion de la Enfermedad tb

public class Pan2
    extends CPanel {

  public String CD_TSIVE = "";

  //private boolean bContacto = false; // Ser� distinto de false si
  // se trata de contactos de tuberculosis

  protected Hashtable hashnotif = null;

  public int iTam = 0;

  //srvProtocolo --> Recupera las respuestas de diferentes
  //                 tablas seg�n la opci�n
  final int servletALTA = 0;
  final int servletMODIFICACION = 1;

  protected Pan1 pan1 = null;

  protected StubSrvBD stub = null;
  protected CLista listadatos = null; //para el LAYOUT
  protected CLista listaValores = null;
  protected CLista listaInsertar = null;

  protected boolean NoSeEncontraronDatos = false;

  CApp applet = null;
  DialNum dialogo = null;

  XYLayout xYLayout1 = null;

  final String strSERVLET_COMPLETO = "servlet/SrvTraerNum";

  final int Ancho_Panel = 390; //antes 600
  final int Ancho_GroupBox = Ancho_Panel - 5;
  final int Ancho_Label_Caja = Ancho_GroupBox / 2;

  //Estados del panel
  final int ALTA = 0;
  final int MODIFICACION = 1;
  protected int iEstado = ALTA;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;

  protected Vector lblT = new Vector();
  protected Vector txt = new Vector();

  //clase escuchadora de perdida de foco e itemchange
  Foco_Completo objLostFocus = new Foco_Completo(this);

  int m;

  // configura los controles condicionados a InHabilitados
  public void Inicializar() {

  } //fin Inicializar

  public Pan2(CApp a, Hashtable hashnotif,
              Pan1 p, DialNum dlg, int mod) {
    try {

      setApp( (CApp) a);
      applet = a;
      dialogo = dlg;
      pan1 = p;
      this.hashnotif = hashnotif;
      m = mod;

      stub = new StubSrvBD();
      listadatos = new CLista();
      listaValores = new CLista();
      listaInsertar = new CLista();

      IraBuscarDatosLayout(); //carga listadatos y listaListaValores

      xYLayout1 = new XYLayout();
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception {
    //NO HAY LAYOUT
    if (listadatos == null) {
      xYLayout1.setHeight(250);
      xYLayout1.setWidth(370); //550
      this.setLayout(xYLayout1);
      this.doLayout(); //sino no sale!!

    }
    //EXISTE LAYOUT
    else {
      //layout
      //antes, pero lo dejo
      this.setLayout(xYLayout1);

      iTam = 0;
      iTam = Componentes(); //pinta el dialogo

      //Boton Reset------------------
      //CCargadorImagen imgs = null;
      //final String imgNAME[] = {"images/aceptar.gif"};

      //imgs = new CCargadorImagen(applet,imgNAME);
      //imgs.CargaImagenes();

      iTam = iTam + 5;
      //------------------------------

      xYLayout1.setHeight(iTam + 10 + 10 + 10);
      xYLayout1.setWidth(370); //550
      this.setLayout(xYLayout1);
      this.doLayout(); //sino no sale!!
    } // Del else
  }

//Tama�o de las cajas de texto (N, C)
//(Max = Ancho_Label_Caja) (Min = 35)
  protected int MaxMin(String Tipo, int longi, int ente, int deci) {
    int tam = 0;
    if (Tipo.equals("C")) {
      tam = (150 / 20) * longi;
      if (tam > Ancho_Label_Caja) {
        tam = Ancho_Label_Caja;
      }
      if (tam < 35) {
        tam = 35;
      }
    }
    else if (Tipo.equals("N")) {
      tam = (150 / 20) * (LongNumero(ente, deci) + 3);
      if (tam > Ancho_Label_Caja) {
        tam = Ancho_Label_Caja;
      }
      if (tam < 35) {
        tam = 35;
      }
    }
    return (tam);
  }

//Valor guardado en el vector para esa Choice y el indice selected
//return : "" o "H"

  protected void IraBuscarDatosLayout() {
    CMessage msgBox = null;
    CLista ListaEntrada = null;
    listadatos = null;
    listaValores = null;

    try {

      ListaEntrada = new CLista();
      ListaEntrada.addElement(hashnotif);

      CLista listaSalida = new CLista();

      ListaEntrada.setIdioma(this.app.getIdioma());
      ListaEntrada.setLogin(this.app.getLogin());
      ListaEntrada.setPerfil(this.app.getPerfil());
      stub.setUrl(new URL(app.getURL() + strSERVLET_COMPLETO));

      if (m == modoALTA) {
        listaSalida = (CLista) stub.doPost(servletALTA, ListaEntrada);
        //SrvTraerNum srv = new SrvTraerNum();
        //listaSalida = srv.doPrueba(servletALTA, ListaEntrada);
        //srv = null;

        /*
                SrvTraerNum srv = new SrvTraerNum();
                srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                               "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                               "dba_edo",
                               "manager");
                listaSalida = srv.doDebug(servletALTA, ListaEntrada);
         */

      }
      else if (m == modoMODIFICACION) {
        listaSalida = (CLista) stub.doPost(servletMODIFICACION, ListaEntrada);
        //SrvTraerNum srv = new SrvTraerNum();
        //listaSalida = srv.doPrueba(servletMODIFICACION, ListaEntrada);
        //srv = null;

        /*
                SrvTraerNum srv = new SrvTraerNum();
                srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                               "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                               "dba_edo",
                               "manager");
                listaSalida = srv.doDebug(servletMODIFICACION, ListaEntrada);
         */

      }

      ListaEntrada = null;

      //listaSalida: La de resp y la de layout
      //si null, sale por catch  (error)

      if ( (listaSalida != null) && (listaSalida.size() > 0)) {
        //listaValores =  (CLista) listaSalida.elementAt(0);
        listadatos = (CLista) listaSalida.elementAt(0);
        if ( (listadatos == null) || (listadatos.size() == 0)) {
          NoSeEncontraronDatos = true;
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

  protected int Componentes() {

    Label lblTexto;
    Label lblChoice;
    Label DesEtiqueta; //SobreTitulos: Cne, ca, nivel1, nivel2
    CEntero txtEtiqueta;

    int indice = 0;

    int iLabelTexto = 0;
    int iCaja = 0;

    int M = 1; //entre objetos
    int Alto = 30; // alto de los objetos

    int y = 0; //donde estamos
    int yUp = 0;
    int yDown = 0;

    CCargadorImagen imgs = null;
    final String imgNAME_BOTONES[] = {
        "images/refrescar.gif"};

    imgs = new CCargadorImagen(applet, imgNAME_BOTONES);
    imgs.CargaImagenes();

    //recorremos la listadatos
    int iTamLista = listadatos.size();
    for (indice = 0; indice < iTamLista; indice++) {
      Hashtable datUnaLinea = (Hashtable) listadatos.elementAt(indice);

      y = y + M;

      //label
      lblT.addElement(new Label());
      lblTexto = (Label) lblT.elementAt(iLabelTexto);
      lblTexto.setFont(new Font("", Font.PLAIN, 10));
      this.add(lblTexto,
               new XYConstraints(3, y, 75 + Ancho_Label_Caja, -1));
      lblTexto.setAlignment(Label.RIGHT);
      lblTexto.setText(datUnaLinea.get("DS_ENFEREDO") + " : ");
      iLabelTexto++; //contador = cajas

      //caja
      //datos int
      int ilon = 0;
      int idec = 0;
      int ient = 0;

      txt.addElement(new CEntero(7));

      //txtEtiqueta = (TextField)txt.elementAt(iCaja);
      txtEtiqueta = (CEntero) txt.elementAt(iCaja);

      this.add(txtEtiqueta,
               new XYConstraints(Ancho_Label_Caja + 85, y, 60, -1));

      txtEtiqueta.setText( (String) datUnaLinea.get("NM_CASOS"));

      //escuchador
      txtEtiqueta.addFocusListener(objLostFocus);
      iCaja++;

      //sAlmTipo = "C";
      y = y + Alto;

    } //listadatos

    return y;
  }

  public boolean hayDatos() {
    return NoSeEncontraronDatos;
  }

  public CLista devuelveDatos() {
    Hashtable datos = null;
    CLista listaSal = new CLista();
    int tamanio_Lista = txt.size();
    for (int contador = 0; contador < tamanio_Lista; contador++) {
      datos = new Hashtable();
      datos.put("ENFERMEDAD", ( (Label) lblT.elementAt(contador)).getText());
      datos.put("NUM_CASOS", ( (TextField) txt.elementAt(contador)).getText());
      // Metemos los dem�s datos del servlet.
      datos.put("CD_TVIGI",
                (String) ( (Hashtable) listadatos.elementAt(contador)).get(
          "CD_TVIGI"));
      datos.put("CD_ENFCIE",
                (String) ( (Hashtable) listadatos.elementAt(contador)).get(
          "CD_ENFCIE"));
      listaSal.addElement(datos);
      datos = null;
    }
    return listaSal;
  }

//Calcula la longitud permitida del numero (con formato)
  protected int LongNumero(int iEnt, int iDec) {

    int iLen = 0;
    int iResto = 0;
    int iDiv = 0;
    int iNumPtos = 0;

    if (iEnt == 0) { //N(0,?)
      iLen = 0;
    }
    else {
      iDiv = (int) (iEnt / 3);
      iResto = iEnt % 3;

      if (iResto == 0) {
        iNumPtos = iDiv - 1;
      }
      else {
        iNumPtos = iDiv;
      }
    }
    if (iDec == 0) {
      iLen = iEnt + iNumPtos;
    }
    else {
      iLen = iEnt + iNumPtos + 1 + iDec;

    }
    return (iLen);
  }

//PERDIDA DE FOCO**********

  public void objfocusLost(FocusEvent e) {
  }

//Ganar Foco
  public void objfocusGained(FocusEvent e) {
  }

//para el trozo de modelo, ordena por nm_lin

  // Devuelve true si el objeto 'contenedor' contiene la cadena especificada
  private boolean bContiene(String contenedor, String contenido) {
    return contenedor.indexOf(contenido) != -1;
  }

  public boolean getNoSeEncontraronDatos() {
    return (NoSeEncontraronDatos);
  }

} //FIN DE CLASE PPAL******************************************

class Foco_Completo
    implements FocusListener {
  Pan2 adaptee;
  FocusEvent e = null;

  Foco_Completo(Pan2 adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.objfocusLost(e);
  }

  public void focusGained(FocusEvent e) {
    adaptee.objfocusGained(e);
  }
}
