
package consconsenf;

import java.sql.SQLException;
import java.util.Enumeration;

import Registro.RegistroConsultas;
import capp.CLista;
import conspacenf.Data;
import conspacenf.SrvGen;
import sapp.DBServlet;

/**
 *  Esta clase est� en el servidor recibe una CLista
 *  recoge de ella el nombre de la clase de datos y los par�metros
 *  ejecuta la sentencia SQL
 *  rellena una CLISTA con objetos de datos
 *  y finaliza la conexi�n
 */
public class SrvConsEnf
    extends SrvGen {

  public void SrvConsEnf() {
    con = null;
  }

  protected CLista doWork(int opmode, CLista param) throws Exception {
    this.param = param;
    String descrip = null;
    Data hash = null;
    CLista result = null;
    this.param = param;
    Data paramData = null;
    String num_reg = null;
    Object o = null;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

    }
    if (param == null || param.size() < 1) {
      // no nos han pasado datos iniciales
      return null;
    }
    else {
      paramData = (Data) param.firstElement();
    }

    if (breal) {
      con = openConnection();
      /*
                if (aplicarLortad)
                {
         // Se obtiene la clave de acceso del usuario que se ha conectado
         passwd = sapp.Funciones.getPassword(con, st, rs, user);
         closeConnection(con);
         con=null;
         // Se abre una nueva conexion para el usuario
         con = openConnection(user, passwd);
                }
       */
    }

    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    try {
      ///establecemos las propiedades indicadas
      ///______________________________________
      con.setAutoCommit(true);

      if (opmode == DataConsEnf.modoENFDUPLICADOCOUNT) {
        StringBuffer strSQL = new StringBuffer();
        int iNumReg = paramData.prepareSQLSentencia(DataConsEnf.
            modoENFDUPLICADOCOUNT, strSQL, param);

        CLista count = realiza_SQL(DataConsEnf.modoENFDUPLICADOCOUNT, iNumReg,
                                   strSQL.toString(), param);
        DataConsEnf denferCount, denfermo;
        if (count != null && count.size() > 0) {
          denferCount = (DataConsEnf) count.firstElement();
          strSQL = new StringBuffer();
          iNumReg = paramData.prepareSQLSentencia(DataConsEnf.modoENFDUPLICADO,
                                                  strSQL, param);

          result = realiza_SQL(DataConsEnf.modoENFDUPLICADO, iNumReg,
                               strSQL.toString(), param);
          if (result != null && result.size() > 0) {
            for (Enumeration e = result.elements(); e.hasMoreElements(); ) {
              denfermo = (DataConsEnf) e.nextElement();
//                       denfermo.put(DataConsEnf.COUNT, denferCount.get(1));

// Correcci�n ARS 11-07-01, para que devuelva bien el n�mero de registros.
              denfermo.put(DataConsEnf.COUNT, denferCount.get(1).toString());
//                       denfermo.put(DataConsEnf.COUNT, denferCount.get(0)); //******* Cuenta est� en indice 0

              descrip = (String) traeDescripcion(
                  "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE CD_MUN = ? AND CD_PROV = ? ",
                  denfermo.get(DataConsEnf.CD_MUN),
                  denfermo.get(DataConsEnf.CD_PROV));
              if (descrip != null) {
                denfermo.put(DataConsEnf.DS_MUN, descrip);
              }
            }
          }
        }
      }
      else {
        StringBuffer strSQL = new StringBuffer();
        int iNumReg = paramData.prepareSQLSentencia(opmode, strSQL, param);

        result = realiza_SQL(opmode, iNumReg, strSQL.toString(), param);
      }

      CLista l = ( (CLista) result); // prueba
      ////# System_Out.println ("compreobamos");
    }
    catch (Exception exc) {
      String dd = exc.toString();
      //E //# System_Out.println("SrvConsEnf Error: "+ exc.toString());
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    //E //# System_Out.println("SrvConsEnf adios" + result);
    return result;
  }

  /**
   *  esta funci�n realiza fetch de los datos leidos
   * y los va metiendo en la estructura solicitada
   *  pagina seg�n el par�metro 0
   *
   * @param paramData es un clase Data que tiene los par�metros iniciales
   * @return devuelve una CLista con todos los datos
   */
  public CLista recogerDatos(Data paramData) {
    CLista result = new CLista();
    Data h;
    Object o;
    int num_datos = 0;
    int campo_filtro = 0;
    int i;

    ////# System_Out.println("Report: entramos en recoger datos");
    //recogemos el par�metro de paginaci�n
    String pag = (String) paramData.get(DataConsEnf.P_PAGINA);
    int ini_pagina = 0;
    int fin_pagina = 0;
    if (pag != null) {
      ini_pagina = Integer.parseInt(pag);
    }
    ini_pagina = (ini_pagina * DBServlet.maxSIZE);
    fin_pagina = ini_pagina + DBServlet.maxSIZE;

    try {
      for (i = 0; rs.next(); i++) {
        // control de tama�o
        if ( (i >= fin_pagina) && (! (paramData.bInformeCompleto))) { //DBServlet.maxSIZE) {
          result.setState(CLista.listaINCOMPLETA);
          campo_filtro = ( (Data) result.lastElement()).getCampoFiltro();
          o = ( (Data) result.lastElement()).get(campo_filtro);
          result.setFilter(o.toString());
          break;
        }
        else if (i < ini_pagina) {
          // no guardamos los datos
          continue;
        }

        // de la clase hija nos da los datos del servlet
        // copiamosla clase hija paraque conserve el filtrado
        h = (Data) paramData.getNewData();
        int num_cols = rs.getMetaData().getColumnCount();
        for (int j = 1; j <= num_cols; j++) {
          o = rs.getObject(j);
          if (o != null) { // si es null no lo metemos
            h.put(j, o);
          }
        }
        result.addElement(h);
      } //orf

      // vemos si est� llena la lista
      if (i < fin_pagina) {
        result.setState(CLista.listaLLENA);
      }

      ////# System_Out.println("Report:salimos bien");
      return result;
    }
    catch (SQLException exc) {
      String dd = exc.toString();
      exc.printStackTrace();
      return null;
    }
  }

  /**
   *  esta funci�n va leyendo los par�metros de Data de entrada
   *  y los prepara, no incluye paginaci�n
   */
  protected void fijarParametros(Data paramData, int a_modo) {
    int tipoDato = 0;
    Object dato = null;
    int i = 0;

    boolean is_like = (a_modo == servletSELECCION_X_CODIGO) ||
        (a_modo == servletSELECCION_X_DESCRIPCION);
////# System_Out.println("Report:: fijaParametros" );
    // ponemos los par�metros fijados por el usuario
    for (i = 1; i <= paramData.getNumParam(); i++) {
      try {
        dato = paramData.get(i);
        if (dato != null) {
          st.setObject(i, dato);
          ////# System_Out.println("Param" +i + ": " + dato);
          // ARG: Se a�aden parametros
          if (existeSiveEnfermo || existeSiveEdoind) {
            registroConsultas.insertarParametro(dato.toString());
          }
        }
        else {
          st.setNull(i, java.sql.Types.VARCHAR);
          // ARG: Se a�aden parametros
          if (existeSiveEnfermo || existeSiveEdoind) {
            registroConsultas.insertarParametro("null");
          }
        }
      }
      catch (Exception exc) {
        String dd = exc.toString();
        //E //# System_Out.println("Report: Fallo paramtero "+ exc.toString());
      }
    }
  }

  /** prueba del servlet */
  static public void main(String[] args) {
    try {
      CLista data = new CLista();
      DataConsEnf d = new DataConsEnf(DataConsEnf.DS_APE1);
      d.setNumParam(2); //11);   //DataConsEnf.P_NUM_C3);
      d.put(DataConsEnf.P_PAGINA, "0");
      d.put(DataConsEnf.P_CD_NIVEL_1, "%");
      d.put(DataConsEnf.P_CD_NIVEL_2, "%");

      data.addElement(d);

      SrvConsEnf srv = new SrvConsEnf();

      CLista dd = srv.doPrueba(DataConsEnf.modoENFDUPLICADOCOUNT, data);
      //String ss = "Lista: " + dd;
      ////# System_Out.println(ss);

      int i = 33;
    }
    catch (Exception exc) {
      String ss = "fallo " + exc.toString();
      //E //# System_Out.println("C3 Error " + ss);
    }
  }

} //________________________________________________ END_CLASS
