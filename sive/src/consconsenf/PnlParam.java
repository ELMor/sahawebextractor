package consconsenf;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo2.DataCat2;
import enfermo.comun;
import eqNot.DataEqNot;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import vCasIn.DataMun;
import zbs.DataZBS;
import zbs.DataZBS2;

public class PnlParam
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  protected StubSrvBD stubCliente = new StubSrvBD();

  public DataConsEnf paramC1 = null;

  protected int modoOperacion = modoNORMAL;

  protected PanelInforme informe;

  protected boolean bPermiso = false;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblArea = new Label();
  CCampoCodigo txtCodAre = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarAre = null;
  TextField txtDesAre = new TextField();
  Label lblDistrito = new Label();
  CCampoCodigo txtCodDis = new CCampoCodigo(); /*E*/
  TextField txtDesDis = new TextField();
  ButtonControl btnCtrlBuscarDis = null;
  ButtonControl btnLimpiar = null;
  ButtonControl btnInforme = null;

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  actionListener btnActionListener = new actionListener(this);

  public PnlParam(CApp a, boolean permiso) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("consconsenf.Res" + a.getIdioma());
      DataConsEnf d = new DataConsEnf(DataConsEnf.DS_FONOAPE1);
      informe = new PanelInforme(new StubSrvBD(), a, "erw/consenf.erw",
                                 "SIVE_CONSENF", d.getFieldVector(permiso), this);
      d = null;

      jbInit();
      informe.setEnabled(false);
      lblArea.setText(this.app.getNivel1() + ":");
      lblDistrito.setText(this.app.getNivel2() + ":");
      this.txtCodAre.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesAre.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDis.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDis.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        comun.imgLUPA,
        comun.imgLIMPIAR,
        comun.imgGENERAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(490);

    btnCtrlBuscarAre = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarDis = new ButtonControl(imgs.getImage(0));
    btnLimpiar = new ButtonControl(imgs.getImage(1));
    btnInforme = new ButtonControl(imgs.getImage(2));

    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    xYLayout.setWidth(596);

    // gestores de eventos
    btnCtrlBuscarAre.addActionListener(btnActionListener);
    btnCtrlBuscarDis.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtCodAre.addFocusListener(txtFocusAdapter);
    txtCodDis.addFocusListener(txtFocusAdapter);

    btnCtrlBuscarAre.setActionCommand("buscarAre");
    txtDesAre.setEditable(false);
    txtDesAre.setEnabled(false);
    btnCtrlBuscarDis.setActionCommand("buscarDis");
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtCodAre.setName("txtCodAre");
    txtCodAre.addKeyListener(new PnlParam_txtCodAre_keyAdapter(this));
    txtCodDis.setName("txtCodDis");
    txtCodDis.addKeyListener(new PnlParam_txtCodDis_keyAdapter(this));
    txtDesDis.setEditable(false);
    txtDesDis.setEnabled(false);

    lblArea.setText(res.getString("lblArea.Text"));
    this.setLayout(xYLayout);

    // ponemos la enfermedad como clave

    this.add(lblArea, new XYConstraints(19, 24, 97, -1));
    this.add(txtCodAre, new XYConstraints(136, 24, 77, -1));
    this.add(btnCtrlBuscarAre, new XYConstraints(218, 24, -1, -1));
    this.add(txtDesAre, new XYConstraints(247, 24, 287, -1));
    this.add(lblDistrito, new XYConstraints(19, 62, 99, -1));
    this.add(txtCodDis, new XYConstraints(136, 62, 77, -1));
    this.add(btnCtrlBuscarDis, new XYConstraints(218, 62, -1, -1));
    this.add(txtDesDis, new XYConstraints(247, 62, 287, -1));

    this.add(btnLimpiar, new XYConstraints(374, 120, -1, -1));
    this.add(btnInforme, new XYConstraints(460, 120, -1, -1));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  public synchronized void setPermiso(boolean bool) {
    bPermiso = bool;
  }

  public synchronized boolean getPermiso() {
    return bPermiso;
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodAre.setEnabled(true);
        btnCtrlBuscarAre.setEnabled(true);
        // control area
        if (!txtDesAre.getText().equals("")) {
          btnCtrlBuscarDis.setEnabled(true);
          txtCodDis.setEnabled(true);
        }
        else {
          btnCtrlBuscarDis.setEnabled(false);
          txtCodDis.setEnabled(false);
        }
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodAre.setEnabled(false);
        txtCodDis.setEnabled(false);
        btnCtrlBuscarAre.setEnabled(false);
        btnCtrlBuscarDis.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarAre_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg5.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodAre.setText(data.getCod());
      txtDesAre.setText(data.getDes());
      txtCodDis.setText("");
      txtDesDis.setText("");
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el distrito (Niv2)
  void btnCtrlbuscarDis_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg5.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDis.setText(data.getNiv2());
      txtDesDis.setText(data.getDes());
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtCodAre.setText("");
    txtDesAre.setText("");
    txtCodDis.setText("");
    txtDesDis.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC1 = new DataConsEnf(DataConsEnf.DS_FONOAPE1);

    if (isDataValid()) {
      if (!txtCodAre.getText().trim().equals("")) {
        paramC1.put(DataConsEnf.P_CD_NIVEL_1, txtCodAre.getText());
        //E //# System_Out.println("Param. 1");
        //E //# System_Out.println(txtCodAre.getText());
        paramC1.setNumParam(1);
        if (txtCodDis.getText().trim().equals("")) {
          //paramC1.put(DataConsEnf.P_CD_NIVEL_2 ,"%");
          //E //# System_Out.println("Param 2 nulo %");
        }
        else {
          paramC1.put(DataConsEnf.P_CD_NIVEL_2, txtCodDis.getText());
          //E //# System_Out.println("Param. 2");
          //E //# System_Out.println(txtCodDis.getText());
          paramC1.setNumParam(2);
        }
      }
      /*if (!txtCodDis.getText().trim().equals("")){
        paramC1.put(DataConsEnf.P_CD_NIVEL_2 ,txtCodDis.getText());
        //# System_Out.println("Param. 2");
        //# System_Out.println(txtCodDis.getText());
        paramC1.setNumParam(2);
        if (paramC1.get(DataConsEnf.P_CD_NIVEL_1) == null){
            paramC1.put(DataConsEnf.P_CD_NIVEL_1 ,"%");
            //# System_Out.println("Param 1 nulo %");
        }
        if (paramC1.get(DataConsEnf.P_CD_NIVEL_2) == null){
            paramC1.put(DataConsEnf.P_CD_NIVEL_2 ,"%");
            //# System_Out.println("Param 2 nulo %");
        }
             }
       */
      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      CLista param = new CLista();
      param.addElement(paramC1);
      informe.setListaParam(param);
      boolean hay = informe.GenerarInforme();
      if (hay) {
        informe.show();
      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg6.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;
    DataEqNot eqnot;
    DataCat2 prov;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtCodAre.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodAre.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtCodDis.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodAre.getText(), txtCodDis.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodAre")) {
            nivel1 = (DataNivel1) param.firstElement();
            txtCodAre.setText(nivel1.getCod());
            txtDesAre.setText(nivel1.getDes());

          }
          else if (txt.getName().equals("txtCodDis")) {
            nivel2 = (DataZBS2) param.firstElement();
            txtCodDis.setText(nivel2.getNiv2());
            txtDesDis.setText(nivel2.getDes());

          }

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg7.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // cambio en una caja de texto

  void txtCodAre_keyPressed(KeyEvent e) {
    txtCodDis.setText("");
    txtDesDis.setText("");
    txtDesAre.setText("");
    btnCtrlBuscarDis.setEnabled(false);
  }

  void txtCodDis_keyPressed(KeyEvent e) {
    txtDesDis.setText("");
  }

} // end clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  PnlParam adaptee = null;
  ActionEvent e = null;

  public actionListener(PnlParam adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarAre")) {
      adaptee.btnCtrlbuscarAre_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDis")) {
      adaptee.btnCtrlbuscarDis_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
} // end class

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PnlParam adaptee;
  FocusEvent event;

  focusAdapter(PnlParam adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
} // end class

////////////////////// Clases para listas

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
} // end class

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected PnlParam panel;

  public CListaZBS2(PnlParam p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodAre.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
} //end class

class PnlParam_txtCodAre_keyAdapter
    extends java.awt.event.KeyAdapter {
  PnlParam adaptee;

  PnlParam_txtCodAre_keyAdapter(PnlParam adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodAre_keyPressed(e);
  }
}

class PnlParam_txtCodDis_keyAdapter
    extends java.awt.event.KeyAdapter {
  PnlParam adaptee;

  PnlParam_txtCodDis_keyAdapter(PnlParam adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodDis_keyPressed(e);
  }
}
