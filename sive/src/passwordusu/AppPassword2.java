package passwordusu;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppPassword2
    extends CApp
    implements CInicializar {

  DiaPassword2 di = null;

  public AppPassword2() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Cambio de password");
    di = new DiaPassword2(this);
    di.show();
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        di.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        di.setEnabled(true);
        break;
    }
  }

}
