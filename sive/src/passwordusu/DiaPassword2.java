package passwordusu;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
//import centinelas.cliente.c_comuncliente.*;
import capp2.CApp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CTexto;
import cifrado.Cifrar;
import sapp2.Data;
import sapp2.Lista;

public class DiaPassword2
    extends CDialog
    implements CInicializar {

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  XYLayout xyLayout1 = new XYLayout();

  Label lblUsuario = new Label();
  Label lblPass = new Label();
  Label lblPassNueva = new Label();
  Label lblPassRepetir = new Label();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  CTexto txtUsuario = new CTexto(12);
  CTexto txtPass = new CTexto(8);
  CTexto txtPassNueva = new CTexto(8);
  CTexto txtPassRepetir = new CTexto(8);

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  public DiaPassword2(CApp a) {
    super(a);
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(400, 250);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    lblUsuario.setText("Usuario:");
    lblPass.setText("Password Actual:");
    lblPassNueva.setText("Password Nueva:");
    lblPassRepetir.setText("Confirmar Password Nueva:");

    txtUsuario.setBackground(new Color(255, 255, 150));
    txtPass.setBackground(new Color(255, 255, 150));
    txtPass.setEchoChar('*');
    txtPassNueva.setBackground(new Color(255, 255, 150));
    txtPassNueva.setEchoChar('*');
    txtPassRepetir.setBackground(new Color(255, 255, 150));
    txtPassRepetir.setEchoChar('*');

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(450);
    xyLayout1.setWidth(450);

    this.add(lblUsuario, new XYConstraints(MARGENIZQ, MARGENSUP, 170, ALTO));
    this.add(txtUsuario,
             new XYConstraints(MARGENIZQ + 170 + INTERHOR, MARGENSUP, 150, ALTO));
    this.add(lblPass,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 170,
                               ALTO));
    this.add(txtPass,
             new XYConstraints(MARGENIZQ + 170 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 150, ALTO));
    this.add(lblPassNueva,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERVERT,
                               170, ALTO));
    this.add(txtPassNueva,
             new XYConstraints(MARGENIZQ + 170 + INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 150, ALTO));
    this.add(lblPassRepetir,
             new XYConstraints(MARGENIZQ, MARGENSUP + 3 * ALTO + 3 * INTERVERT,
                               170, ALTO));
    this.add(txtPassRepetir,
             new XYConstraints(MARGENIZQ + 170 + INTERHOR,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT, 150, ALTO));

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 320 - 88 - 88,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT, 88, 29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 320 + INTERHOR - 88,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT, 88, 29));

    setTitle("Cambio de password");

    txtUsuario.setText(this.getApp().getParameter("COD_USUARIO"));
  } //end jbInit

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  } //end Inicializar

  Data recogerDatos() {
    Data dtDatos = new Data();
    dtDatos.put("COD_USUARIO", txtUsuario.getText().trim());
    dtDatos.put("COD_APLICACION", this.getApp().getParametro("COD_APLICACION"));
    String passAnt = Cifrar.cifraClave(txtPass.getText().trim());
//    dtDatos.put("PASSWORD",txtPass.getText().trim());
    dtDatos.put("PASSWORD", passAnt);
    String passNueva = Cifrar.cifraClave(txtPassNueva.getText().trim());
    dtDatos.put("PASSWORD_NUEVA", passNueva);
    return dtDatos;
  }

  /*  void vaciarCampos(int modoVaciado){
      switch (modoVaciado)
    }*/

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {
    final String servlet = "servlet/SrvPassword2";
    Lista lResultado = new Lista();
    Data dtDatos = new Data();
    Lista lDatos = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        Inicializar(CInicializar.ESPERA);
        dtDatos = recogerDatos();
        lDatos.addElement(dtDatos);
        try {
          this.getApp().getStub().setUrl(servlet);
          lResultado = (Lista)this.getApp().getStub().doPost(0, lDatos);
          //lResultado=BDatos.ejecutaSQL(false,this.getApp(),servlet,0,lDatos);
          this.getApp().showAdvise("Modificaci�n de password realizada");
          dispose();
        }
        catch (Exception ex) {
          this.getApp().trazaLog(ex);
//          vaciarCampos(1);
        }
        Inicializar(CInicializar.NORMAL);
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      dispose();
    }
  } //fin de btn_actionPerformed

  //comprueba: 1.campos rellenos; 2:pwnuevo=pwrepetir
  boolean isDataValid() {
    //compruebo todos los campos rellenos
    if ( (txtUsuario.getText().trim().equals(""))
        || (txtPass.getText().trim().equals(""))
        || (txtPassNueva.getText().trim().equals(""))
        || (txtPassRepetir.getText().trim().equals(""))) {
      this.getApp().showAdvise("Debe rellenar todos los campos previamente");
      return false;
    }
    else { //compruebo q password nuevo=repite passw
      if ( (! (txtPassNueva.getText().trim().equals(txtPassRepetir.getText().
          trim())))) {
        this.getApp().showAdvise(
            "Compruebe la nueva password. Validaci�n no correcta");
        return false;
      }
      else {
        return true;
      }
    } //end if campos rellenos
  } //end isDataValid()
}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaPassword2 adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaPassword2 adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
    /*    SincrEventos s=adaptee.getSincrEventos();
        if(s.bloquea(adaptee.modoOperacion,adaptee)){
          adaptee.btn_actionPerformed(e);
        }
        s.desbloquea(adaptee);*/
  }
}
