
package mantus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;

import capp.CLista;
//import enfermo.Fechas;
import comun.Fechas;
import sapp.DBServlet;

public class SrvMantUs
    extends DBServlet {

  // modos de operaci�n
  final int modoALTA = 0;
  final int modoMODIFICAR = 1;
  final int modoSELECCION = 2;
  final int modoBAJA = 3;
  final int modoOBTENER = 4;

  final int modoGENERAR_LOGIN = 8;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int k; //Para �ndice b�squeda columnas
    // objetos de datos
    CLista data = null;
    DataUsuario datUsuario = null;

    //Campos que se reciben/envian en la DataUsuario

    String sCodUsu; //C�digo de usuario
    String sCodComAut; //Com. aut�noma
    String sCodEquNot; //Equipo notificador
    String sCodTipUsu; // Tipo de usuario
    String sDesNom; //Nombre
    String sDesApe; //apellidos
    boolean bAutAlt; //Autorizaci�n para dar altas
    boolean bAutMod; //Autorizaci�n para hacer modificaciones
    boolean bAutBaj; //Autorizaci�n para dar bajas
    String sPer; //perfil
    boolean bEnf; //Flag indica si usuario puede ver datos del enfermo
    boolean bManCat; //Flag de autorizaci�n a mantenimiento de cat�logos
    boolean bManUsu; //Flag de autorizaci�n a mantenimiento de usuarios
    boolean bTraPro; //Flag de autorizaci�n de transmisi�n a Madrid e import protocolos
    boolean bDefPro; //Flag de autorizaci�n de definici�n de protocolos
    boolean bAla; //Flag de autorizaci�n de creaci�n de definiciones de alarmas
    boolean bVal; //Flag de autorizaci�n de validaci�n de notificaciones
    String sDesTel; //Tel�fonos de contacto
    String sDesDir; //Direcci�n de contacto
    String sDesCor; //Correo electr�nico
    String sDesAno; //Otras anotaciones
    String sFecAlt; //Fecha de alta
    String sFecBaj; //Fecha de baja
    boolean bBaj; //Marca de baja
    String sDesPas; //Password
    Vector vAmbUsu = new Vector(); //Ambito del usuario (varios String)
    String sDesEquNot = ""; //Descripci�n del equipo notificador
    boolean bresConf = false; //Resolucion de conflictos
    //QQ: Nuevos Flags:
    boolean bExport; //Autorizaci�n para dar altas
    boolean bGenAlauto; //Autorizaci�n para hacer modificaciones
    boolean bNotifs; //Autorizaci�n para dar bajas
    boolean bConsRes; //Autorizaci�n para dar altas
    String sCodApl = "";

    Enumeration enum;
    String sAmbUsu = "";
    //Strings para recoger y meter los booleanos en b.datos
    String sAutAlt, sAutMod, sAutBaj, sEnf, sManCat, sManUsu, sTraPro, sDefPro,
        sAla, sVal, sBaj;
    //QQ: Nuevos Flags, idem
    String sExport, sGenAlauto, sNotifs, sConsRes;

    //Date para recoger y meter fechas en b.datos
    java.sql.Date dFecAlt, dFecBaj;

    boolean bBajAnt = false; //Marca de baja anterior a una modificaci�n
    java.sql.Date dFecAltAnt = null; //Fecha �ltima alta de este usuario anterior a una modificaci�n
    java.sql.Date dFecBajAnt = null; //Fecha �ltima baja de este usuario anterior a una modificaci�n

    // nuevo item en tabla USUARIO
    final String sALTA = "insert into SIVE_USUARIO (CD_USUARIO, CD_CA, CD_E_NOTIF, DS_NOMBRE, DS_APELLIDOS, IT_AUTALTA, IT_AUTMOD,IT_AUTBAJA, IT_PERFILUSU, IT_FG_ENFERMO, IT_FG_MNTO, IT_FG_MNTO_USU, IT_FG_TCNE, IT_FG_PROTOS, IT_FG_ALARMAS, IT_FG_VALIDAR, DS_TELEF, DS_DIRECCION, DS_EMAIL, DS_ANOTACIONES, FC_ALTA, FC_BAJA,IT_BAJA, DS_PASSWORD, IT_FG_EXPORT, IT_FG_GENALAUTO, IT_FG_MNOTIFS, IT_FG_CONSREST) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    //borra ambitos de un usuario en tabla USU_AUTORIZACIONES
    final String sBAJA_AUTORIZACIONES =
        "delete from SIVE_AUTORIZACIONES where CD_USUARIO = ?";

    //Nuevo item en tabla USU_AUTORIZACIONES
    final String sALTA_AUTORIZACIONES = "insert into SIVE_AUTORIZACIONES(CD_CA, CD_NIVEL_1, CD_NIVEL_2, CD_SECUENCIAL, CD_USUARIO) values( ?, ?, ?, ?, ?)";

    // busca marca de baja (para saber si luego hay que actualizar fechas en una modificaci�n)
    final String sBUSQUEDA_MARCA_BAJA =
        "select FC_ALTA, FC_BAJA, IT_BAJA from SIVE_USUARIO where CD_USUARIO = ? ";

    // actualiza el item
    final String sACTUALIZA = "update SIVE_USUARIO SET  CD_CA = ?, CD_E_NOTIF = ?,  DS_NOMBRE = ?, DS_APELLIDOS = ?, IT_AUTALTA = ?, IT_AUTMOD = ?,IT_AUTBAJA = ?, IT_PERFILUSU = ?, IT_FG_ENFERMO = ?, IT_FG_MNTO = ?, IT_FG_MNTO_USU = ?, IT_FG_TCNE = ?, IT_FG_PROTOS = ?, IT_FG_ALARMAS = ?, IT_FG_VALIDAR = ?, DS_TELEF = ?, DS_DIRECCION = ?, DS_EMAIL = ?, DS_ANOTACIONES = ?, FC_ALTA = ?, FC_BAJA = ?,IT_BAJA = ? ,DS_PASSWORD = ?, IT_FG_EXPORT = ?, IT_FG_GENALAUTO = ?, IT_FG_MNOTIFS = ?, IT_FG_CONSREST = ? where CD_USUARIO = ? ";

    //Obtiene lista de items
    final String sLISTADO = "select CD_USUARIO, CD_CA, CD_E_NOTIF, DS_NOMBRE, DS_APELLIDOS, IT_AUTALTA, IT_AUTMOD,IT_AUTBAJA, IT_PERFILUSU, IT_FG_ENFERMO, IT_FG_MNTO, IT_FG_MNTO_USU, IT_FG_TCNE, IT_FG_PROTOS, IT_FG_ALARMAS, IT_FG_VALIDAR, DS_TELEF, DS_DIRECCION, DS_EMAIL, DS_ANOTACIONES, FC_ALTA, FC_BAJA, IT_BAJA, DS_PASSWORD, IT_FG_EXPORT, IT_FG_GENALAUTO, IT_FG_MNOTIFS, IT_FG_CONSREST from SIVE_USUARIO  where CD_USUARIO like ? and DS_NOMBRE like ? and  DS_APELLIDOS like ? and IT_PERFILUSU like ? order by CD_USUARIO";

    //Obtiene lista de items (en listas largas que no se transportan de una vez)
    final String sLISTADO2 = "select CD_USUARIO, CD_CA, CD_E_NOTIF, DS_NOMBRE, DS_APELLIDOS, IT_AUTALTA, IT_AUTMOD,IT_AUTBAJA, IT_PERFILUSU, IT_FG_ENFERMO, IT_FG_MNTO, IT_FG_MNTO_USU, IT_FG_TCNE, IT_FG_PROTOS, IT_FG_ALARMAS, IT_FG_VALIDAR, DS_TELEF, DS_DIRECCION, DS_EMAIL, DS_ANOTACIONES, FC_ALTA, FC_BAJA, IT_BAJA, DS_PASSWORD, IT_FG_EXPORT, IT_FG_GENALAUTO, IT_FG_MNOTIFS, IT_FG_CONSREST from SIVE_USUARIO  where CD_USUARIO like ? and DS_NOMBRE like ? and  DS_APELLIDOS like ? and IT_PERFILUSU like ? and CD_USUARIO > ? order by CD_USUARIO";

    //Obtiene la lista de autorizaciones de un usuario
    final String sLISTADO_AUTORIZACIONES =
        "select CD_NIVEL_1,CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ?";

    //Obtiene la descripci�n de un equipo notificador
    final String sBUSQUEDA_DES_EQU_NOT =
        "select DS_ENOTIF from SIVE_E_NOTIF where CD_E_NOTIF = ?";

    // busca un item  (modo OBTENER)
    final String sBUSQUEDA = "select CD_USUARIO, CD_CA, CD_E_NOTIF, DS_NOMBRE, DS_APELLIDOS, IT_AUTALTA, IT_AUTMOD,IT_AUTBAJA, IT_PERFILUSU, IT_FG_ENFERMO, IT_FG_MNTO, IT_FG_MNTO_USU, IT_FG_TCNE, IT_FG_PROTOS, IT_FG_ALARMAS, IT_FG_VALIDAR, DS_TELEF, DS_DIRECCION, DS_EMAIL, DS_ANOTACIONES, FC_ALTA, FC_BAJA, IT_BAJA, DS_PASSWORD, IT_FG_EXPORT, IT_FG_GENALAUTO, IT_FG_MNOTIFS, IT_FG_CONSREST from SIVE_USUARIO  where CD_USUARIO = ? order by CD_USUARIO";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    datUsuario = (DataUsuario) param.firstElement();

    try {

      // modos de operaci�n
      switch (opmode) {

        // alta
        case modoALTA:

////#// System_out.println("Alta");//**********************************
           // prepara la query
           st = con.prepareStatement(sALTA);
          k = 0;
          // cod usario
          k++;
          st.setString(1, datUsuario.getCodUsu().trim());

          // cod Com Autonoma
          k++;
          st.setString(k, datUsuario.getCodComAut().trim());

          // eq notificador
          k++;
          if (datUsuario.getCodEquNot().trim().length() > 0) {
            st.setString(k, datUsuario.getCodEquNot().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);

            //Nombre
          }
          k++;
          st.setString(k, datUsuario.getDesNom().trim());

          //Apellidos
          k++;
          if (datUsuario.getDesApe().trim().length() > 0) {
            st.setString(k, datUsuario.getDesApe().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);

            //Autorizaci�n altas
          }
          k++;
          if (datUsuario.getAutAlt() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

            //Autorizaci�n modificaciones
          }
          k++;
          if (datUsuario.getAutMod() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autorizaci�n bajas
          }
          k++;
          if (datUsuario.getAutBaj() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Perfil
          }
          k++;
          if (datUsuario.getPer().trim().length() > 0) {
            st.setString(k, datUsuario.getPer().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Autor. ver datos enfermo
          }
          k++;
          if (datUsuario.getEnf() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor mant. cat�logos
          }
          k++;
          if (datUsuario.getManCat() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor. mat. usuarios
          }
          k++;
          if (datUsuario.getManUsu() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor. transmisi�n protocolos
          }
          k++;
          if (datUsuario.getTraPro() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor. def. protocolos
          }
          k++;
          if (datUsuario.getDefPro() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Aut. definiciones de alarmas
          }
          k++;
          if (datUsuario.getAla() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor. validaci�n de notificaciones
          }
          k++;
          if (datUsuario.getVal() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Tel�fono
          }
          k++;
          if (datUsuario.getDesTel().trim().length() > 0) {
            st.setString(k, datUsuario.getDesTel().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Direcci�n
          }
          k++;
          if (datUsuario.getDesDir().trim().length() > 0) {
            st.setString(k, datUsuario.getDesDir().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Correo electr�nico
          }
          k++;
          if (datUsuario.getDesCor().trim().length() > 0) {
            st.setString(k, datUsuario.getDesCor().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Anotaciones
          }
          k++;
          if (datUsuario.getDesAno().trim().length() > 0) {
            st.setString(k, datUsuario.getDesAno().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);

            //Marca de baja como usuario
            //En fechas se pone fech actual en las dos

          }
          if (datUsuario.getBaj() == false) { //No est� como baja
            //Fecha de alta (fecha actual)
            k++;
            st.setDate(k, new java.sql.Date(new java.util.Date().getTime()));
            //Fecha de baja   (null)
            k++;
            //st.setDate(k, new java.sql.Date(new java.util.Date().getTime()));
            st.setNull(k, java.sql.Types.VARCHAR);
            //Marca de baja a false
            k++;
            st.setString(k, "N");
          }

          else {
            //Fecha de alta (null)
            k++;
//          st.setDate(k, new java.sql.Date(new java.util.Date().getTime()));          st.setNull(k, java.sql.Types.VARCHAR);
            //Fecha de baja ,(fecha actual)
            st.setNull(k, java.sql.Types.VARCHAR);
            k++;
            st.setDate(k, new java.sql.Date(new java.util.Date().getTime()));
            //Marca de baja a false
            k++;
            st.setString(k, "S");
          }

          // password
          k++;
          st.setString(k, datUsuario.getDesPas().trim());

          //***
           //QQ: Nuevos Flags de Autorizaciones
          k++;
          if (datUsuario.getExport() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

          }
          k++;
          if (datUsuario.getGenAlauto() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

          }
          k++;
          if (datUsuario.getManNotifs() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

          }
          k++;
          if (datUsuario.getConsRes() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

            //----.

            // lanza la query
          }
          st.executeUpdate();
          ////#// System_out.println("Mantus: "+ sALTA);
          st.close();

          //Hacemos baja (por si acaso) en tabla autorizaciones antes de insertar
          // lanza la query
          st = con.prepareStatement(sBAJA_AUTORIZACIONES);
          st.setString(1, datUsuario.getCodUsu().trim());
          st.executeUpdate();
          ////#// System_out.println("Mantus: " +sBAJA_AUTORIZACIONES);
          st.close();
          st = null;

          //Recorremos el vector de ambitos e insertamos una linea por cada ambito en tabla autorizaciones
          vAmbUsu = datUsuario.getAmbUsu();
          String ambito = null;
          String nivel2 = null;
          if (vAmbUsu != null) {
            enum = vAmbUsu.elements();
            int z = 0;
            while (enum.hasMoreElements()) {
              ambito = (String) (enum.nextElement()); // nivel1
              nivel2 = (String) (enum.nextElement()); // nivel2

              // prepara la query
              st = con.prepareStatement(sALTA_AUTORIZACIONES);
              // cod Com Aut�noma
              st.setString(1, datUsuario.getCodComAut().trim());
              // Cod nivel 1
              if (datUsuario.getPer().equals("3") && ambito != null) {
                //Codigo en perfil para CD_NIVEL_1***********************
                st.setString(2, ambito.trim());
              }
              else {
                st.setNull(2, java.sql.Types.VARCHAR);
              }
              // Cod nivel 2
              //if  (datUsuario.getPer().equals("4") && nivel2 != null ){
              //Codigo en perfil para CD_NIVEL_2***********************
              if (nivel2 != null) {
                st.setString(3, nivel2.trim());
              }
              else {
                st.setNull(3, java.sql.Types.VARCHAR);
              }
              //Cod secuencial
              z++;
              st.setInt(4, z); //????????????????????????????????????????Secuencial*******
              //Cod usuario
              st.setString(5, datUsuario.getCodUsu().trim());
              // lanza la query
              st.executeUpdate();
              ////#// System_out.println("Mantus: "+sALTA_AUTORIZACIONES);
              st.close();
              st = null;

            }
          }

          break;

          // listado
        case modoSELECCION:

////#// System_out.println("Entra sel");//*************************************
           // prepara la query
           if (param.getFilter().length() > 0) {
             st = con.prepareStatement(sLISTADO2);
           }
           else {
             st = con.prepareStatement(sLISTADO);
             // prepara la lista de resultados
           }
          data = new CLista();

          // filtros
          st.setString(1, datUsuario.getCodUsu().trim() + "%");
          st.setString(2, datUsuario.getDesNom().trim() + "%");
          st.setString(3, datUsuario.getDesApe().trim() + "%");
          st.setString(4, datUsuario.getPer().trim() + "%");

          // paginaci�n
          if (param.getFilter().length() > 0) {
            st.setString(5, param.getFilter().trim());
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              data.setFilter( ( (DataUsuario) data.lastElement()).getCodUsu());
              break;
            }

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            // obtiene los campos
            sCodUsu = rs.getString("CD_USUARIO");
            sCodComAut = rs.getString("CD_CA");
            sCodEquNot = rs.getString("CD_E_NOTIF");
            sDesNom = rs.getString("DS_NOMBRE");
            sDesApe = rs.getString("DS_APELLIDOS");

            sAutAlt = rs.getString("IT_AUTALTA");
            if (sAutAlt.equals("S")) {
              bAutAlt = true;
            }
            else {
              bAutAlt = false;

            }
            sAutMod = rs.getString("IT_AUTMOD");
            if (sAutMod.equals("S")) {
              bAutMod = true;
            }
            else {
              bAutMod = false;

            }
            sAutBaj = rs.getString("IT_AUTBAJA");
            if (sAutBaj.equals("S")) {
              bAutBaj = true;
            }
            else {
              bAutBaj = false;

              //QQ: Nuevos Flags
            }
            sExport = rs.getString("IT_FG_EXPORT");
            if (sExport.equals("S")) {
              bExport = true;
            }
            else {
              bExport = false;

            }
            sGenAlauto = rs.getString("IT_FG_GENALAUTO");
            if (sGenAlauto.equals("S")) {
              bGenAlauto = true;
            }
            else {
              bGenAlauto = false;

            }
            sNotifs = rs.getString("IT_FG_MNOTIFS");
            if (sNotifs.equals("S")) {
              bNotifs = true;
            }
            else {
              bNotifs = false;

            }
            sConsRes = rs.getString("IT_FG_CONSREST");
            if (sConsRes.equals("S")) {
              bConsRes = true;
            }
            else {
              bConsRes = false;
              //---.

            }
            sPer = rs.getString("IT_PERFILUSU");

            sEnf = rs.getString("IT_FG_ENFERMO");
            if (sEnf.equals("S")) {
              bEnf = true;
            }
            else {
              bEnf = false;

            }
            sManCat = rs.getString("IT_FG_MNTO");
            if (sManCat.equals("S")) {
              bManCat = true;
            }
            else {
              bManCat = false;

            }
            sManUsu = rs.getString("IT_FG_MNTO_USU");
            if (sManUsu.compareTo("S") == 0) {
              bManUsu = true;
            }
            else {
              bManUsu = false;

            }
            sTraPro = rs.getString("IT_FG_TCNE");
            if (sTraPro.equals("S")) {
              bTraPro = true;
            }
            else {
              bTraPro = false;

            }
            sDefPro = rs.getString("IT_FG_PROTOS");
            if (sDefPro.equals("S")) {
              bDefPro = true;
            }
            else {
              bDefPro = false;

            }
            sAla = rs.getString("IT_FG_ALARMAS");
            if (sAla.equals("S")) {
              bAla = true;
            }
            else {
              bAla = false;

            }
            sVal = rs.getString("IT_FG_VALIDAR");
            if (sVal.equals("S")) {
              bVal = true;
            }
            else {
              bVal = false;

            }
            sDesTel = rs.getString("DS_TELEF");
            sDesDir = rs.getString("DS_DIRECCION");
            sDesCor = rs.getString("DS_EMAIL");
            sDesAno = rs.getString("DS_ANOTACIONES");

            dFecAlt = rs.getDate("FC_ALTA");
            if (dFecAlt != null) {
              sFecAlt = Fechas.date2String(dFecAlt);
            }
            else {
              sFecAlt = "";

            }
            dFecBaj = rs.getDate("FC_BAJA");
            if (dFecBaj != null) {
              sFecBaj = Fechas.date2String(dFecBaj);
            }
            else {
              sFecBaj = "";

            }

            sBaj = rs.getString("IT_BAJA");
            if (sBaj.equals("S")) {
              bBaj = true;
            }
            else {
              bBaj = false;
            }

                /* Codigo bueno?? sustituido provisional pues no permite null en fechas
                       if (dFecAlt!= null)
                         sFecAlt = Fechas.date2String(dFecAlt);
                       else
                         sFecAlt = "";
                       if (dFecBaj!= null)
                          sFecBaj= Fechas.date2String(dFecBaj);
                       else
                         sFecBaj = "";
                       if (sBaj.equals("S"))
                          bBaj=true;
                       else bBaj=false;
             */

            sDesPas = rs.getString("DS_PASSWORD");
            vAmbUsu = new Vector();
            // a�ade un nodo
            data.addElement(new DataUsuario(sCodUsu, sCodComAut, sCodEquNot,
                                            sDesNom, sDesApe, bAutAlt, bAutMod,
                                            bAutBaj, sPer, bEnf, bManCat,
                                            bManUsu, bTraPro, bDefPro, bAla,
                                            bVal, sDesTel, sDesDir, sDesCor,
                                            sDesAno, sFecAlt, sFecBaj, bBaj,
                                            sDesPas, vAmbUsu, sDesEquNot,
                                            bresConf, bExport, bGenAlauto,
                                            bNotifs, bConsRes, sCodApl));
            i++;

          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          //Buscamos las autorizqaciones de cada  usuario y se los a�adimos a la lista

          enum = data.elements();
          while (enum.hasMoreElements()) { //Recorremos lista principal (usuarios)
            datUsuario = (DataUsuario) (enum.nextElement());

            st = con.prepareStatement(sLISTADO_AUTORIZACIONES);
            st.setString(1, datUsuario.getCodUsu().trim());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            String sCodNiv1, sCodNiv2;
            while (rs.next()) {
              // obtiene los campos
              sCodNiv1 = rs.getString("CD_NIVEL_1");
              sCodNiv2 = rs.getString("CD_NIVEL_2");
              // a�ade un nodo
              datUsuario.getAmbUsu().addElement(sCodNiv1);
              datUsuario.getAmbUsu().addElement(sCodNiv2);
////#// System_out.println("AUTORIZAION: " + sCodNiv1 + " "+ sCodNiv2);
            }

            rs.close();
            st.close();
            st = null;
            rs = null;

          }

////#// System_out.println("Fin autorizaciones   ?????????????");  //*******************

           /*
                   //Buscamos, en caso necesario la descripci�n del eq notificador en cada usuario
                   enum = data.elements();
                while (enum.hasMoreElements() ){    //Recorremos lista principal (usuarios)
                     datUsuario= (DataUsuario)(enum.nextElement());
                if (datUsuario.getCodEquNot()!= "") {  //Llega un blanco ,no un null
                       st = con.prepareStatement(sBUSQUEDA_DES_EQU_NOT);
                       st.setString(1, datUsuario.getCodUsu().trim() );
                       rs = st.executeQuery();
                       // extrae la p�gina requerida
                        while (rs.next()) {
                          sDesEquNot= rs.getString("DS_E_NOTIF");
                        }
                       datUsuario.setDesEquNot(sDesEquNot);
                       rs.close();
                       st.close();
                       rs = null;
                       st = null;
                     }//if
                     else {
                      sDesEquNot="";
                     }
//#// System_out.println("a� descrip" + datUsuario.getCodUsu());//*************************************
                     }//while
             */
            break;

          // modificaci�n
        case modoMODIFICAR:

          // lanza la query para ver si el usuario estaba de baja o no
          st = con.prepareStatement(sBUSQUEDA_MARCA_BAJA);
          st.setString(1, datUsuario.getCodUsu().trim()); //Nota: No lleva % Cojo solo uno
          rs = st.executeQuery();
          ////#// System_out.println("Mantus: " + sBUSQUEDA_MARCA_BAJA);

          while (rs.next()) {
            // obtiene los campos
            //Marca de baja
            String stBajAnt;
            stBajAnt = rs.getString("IT_BAJA");
            if (stBajAnt.equals("S")) {
              bBajAnt = true;
            }
            else {
              bBajAnt = false;
              //Fecha �ltima alta
            }
            dFecAltAnt = rs.getDate("FC_ALTA");
            //Fecha �ltima baja
            dFecBajAnt = rs.getDate("FC_BAJA");
          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          // prepara la query de actualizaci�n
          st = con.prepareStatement(sACTUALIZA);
          k = 0;
          // cod Com Autonoma
          k++;
          st.setString(k, datUsuario.getCodComAut().trim());
          // eq notificador
          k++;
          if (datUsuario.getCodEquNot().trim().length() > 0) {
            st.setString(k, datUsuario.getCodEquNot().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Nombre
          }
          k++;
          st.setString(k, datUsuario.getDesNom().trim());
          //Apellidos
          k++;
          if (datUsuario.getDesApe().trim().length() > 0) {
            st.setString(k, datUsuario.getDesApe().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Autorizaci�n altas
          }
          k++;
          if (datUsuario.getAutAlt() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autorizaci�n modificaciones
          }
          k++;
          if (datUsuario.getAutMod() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autorizaci�n bajas
          }
          k++;
          if (datUsuario.getAutBaj() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

            //Perfil
          }
          k++;
          if (datUsuario.getPer().trim().length() > 0) {
            st.setString(k, datUsuario.getPer().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Autor. ver datos enfermo
          }
          k++;
          if (datUsuario.getEnf() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor mant. cat�logos
          }
          k++;
          if (datUsuario.getManCat() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor. mat. usuarios
          }
          k++;
          if (datUsuario.getManUsu() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor. transmisi�n protocolos
          }
          k++;
          if (datUsuario.getTraPro() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor. def. protocolos
          }
          k++;
          if (datUsuario.getDefPro() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Aut. definiciones de alarmas
          }
          k++;
          if (datUsuario.getAla() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Autor. validaci�n de notificaciones
          }
          k++;
          if (datUsuario.getVal() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");
            //Tel�fono
          }
          k++;
          if (datUsuario.getDesTel().trim().length() > 0) {
            st.setString(k, datUsuario.getDesTel().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Direcci�n
          }
          k++;
          if (datUsuario.getDesDir().trim().length() > 0) {
            st.setString(k, datUsuario.getDesDir().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Correo electr�nico
          }
          k++;
          if (datUsuario.getDesCor().trim().length() > 0) {
            st.setString(k, datUsuario.getDesCor().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);
            //Anotaciones
          }
          k++;
          if (datUsuario.getDesAno().trim().length() > 0) {
            st.setString(k, datUsuario.getDesAno().trim());
          }
          else {
            st.setNull(k, java.sql.Types.VARCHAR);

          }

          if ( (bBajAnt == true) && (datUsuario.getBaj() == false)) { //Pasa a alta
            //Fecha de alta (fecha actual)
            k++;
            st.setDate(k, new java.sql.Date(new java.util.Date().getTime()));
            //Fecha de baja ,la que habia
            k++;
            if (dFecBajAnt != null) {
              st.setDate(k, dFecBajAnt); //???????????????????    VEr que pasa
            }
            else {
              st.setNull(k, java.sql.Types.VARCHAR);
              //Marca de baja a N
            }
            k++;
            st.setString(k, "N");
          }

          else if ( (bBajAnt == false) && (datUsuario.getBaj() == true)) { //Pasa a baja
            //Fecha de alta (la que habia)
            k++;
            if (dFecAltAnt != null) {
              st.setDate(k, dFecAltAnt); //???????????????????    VEr que pasa
            }
            else {
              st.setNull(k, java.sql.Types.VARCHAR);
            }
            k++;
            //Fecha de baja ,la actual
            st.setDate(k, new java.sql.Date(new java.util.Date().getTime()));
            //Marca de baja a S
            k++;
            st.setString(k, "S");
          }
          else { //No hay cambios
            //Fecha de alta (la que habia)
            k++;
            if (dFecAltAnt != null) {
              st.setDate(k, dFecAltAnt); //???????????????????    VEr que pasa
            }
            else {
              st.setNull(k, java.sql.Types.VARCHAR);
              //Fecha de baja ,la que habia
            }
            k++;
            if (dFecBajAnt != null) {
              st.setDate(k, dFecBajAnt); //???????????????????    VEr que pasa
            }
            else {
              st.setNull(k, java.sql.Types.VARCHAR);

              //Marca de baja (la que hab�a)
            }
            k++;
            if (bBajAnt == true) {
              st.setString(k, "S");
            }
            else {
              st.setString(k, "N");
            }
          }

          // password
          k++;
          st.setString(k, datUsuario.getDesPas().trim());

          //QQ: Nuevos Flags de Autorizaciones
          k++;
          if (datUsuario.getExport() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

          }
          k++;
          if (datUsuario.getGenAlauto() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

          }
          k++;
          if (datUsuario.getManNotifs() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

          }
          k++;
          if (datUsuario.getConsRes() == true) {
            st.setString(k, "S");
          }
          else {
            st.setString(k, "N");

            //----.

            //Cod Usuario
          }
          k++;
          st.setString(k, datUsuario.getCodUsu().trim());

          // lanza la query
          st.executeUpdate();
          ////#// System_out.println("Maqntus: "+sACTUALIZA);
          st.close();
          st = null;

          //Hacemos baja (por si acaso) en tabla autorizaciones antes de insertar
          // lanza la query
          st = con.prepareStatement(sBAJA_AUTORIZACIONES);
          st.setString(1, datUsuario.getCodUsu().trim());
          st.executeUpdate();
          ////#// System_out.println("Masntus: "+ sBAJA_AUTORIZACIONES);
          st.close();
          st = null;

          //Recorremos el vector de ambitos e insertamos una linea por cada ambitoen tabla autorizaciones

          vAmbUsu = datUsuario.getAmbUsu();
          if (vAmbUsu != null) {
            enum = vAmbUsu.elements();
            int j = 0;
            while (enum.hasMoreElements()) {
              ambito = (String) (enum.nextElement()); // nivel1
              nivel2 = (String) (enum.nextElement()); // nivel2

              // prepara la query
              st = con.prepareStatement(sALTA_AUTORIZACIONES);
              // cod Com Aut�noma
              st.setString(1, datUsuario.getCodComAut().trim());
              // Cod nivel 1
//            if  (datUsuario.getPer().equals("3") && ambito != null ){
              if (ambito != null) {
                //Codigo en perfil para CD_NIVEL_1***********************
                st.setString(2, ambito.trim());
              }
              else {
                st.setNull(2, java.sql.Types.VARCHAR);
              }
              // Cod nivel 2
//            if  (datUsuario.getPer().equals("4") && nivel2 != null) {
              if (nivel2 != null) {
                //Codigo en perfil para CD_NIVEL_2***********************
                st.setString(3, nivel2.trim());
              }
              else {
                st.setNull(3, java.sql.Types.VARCHAR);
              }
              //Cod secuencial
              j++;
              st.setInt(4, j); //????????????????????????????????????????
              //Cod usuario
              st.setString(5, datUsuario.getCodUsu().trim());

              // lanza la query
              st.executeUpdate();
              ////#// System_out.println("MAntus: " + sALTA_AUTORIZACIONES);
              st.close();
              st = null;
            }
          }

          break;

        case modoOBTENER:

          // prepara la query
          st = con.prepareStatement(sBUSQUEDA);

          ////#// System_out.println("prep obtener           ??????????????");  //****************

           // prepara la lista de resultados
          data = new CLista();

          // filtro
          st.setString(1, datUsuario.getCodUsu().trim());
          rs = st.executeQuery();
          ////#// System_out.println("Mantus: " + sBUSQUEDA);

          // extrae la p�gina requerida
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            // obtiene los campos
            sCodUsu = rs.getString("CD_USUARIO");
            sCodComAut = rs.getString("CD_CA");
            sCodEquNot = rs.getString("CD_E_NOTIF");
            sDesNom = rs.getString("DS_NOMBRE");
            sDesApe = rs.getString("DS_APELLIDOS");

            sAutAlt = rs.getString("IT_AUTALTA");
            if (sAutAlt.equals("S")) {
              bAutAlt = true;
            }
            else {
              bAutAlt = false;

            }
            sAutMod = rs.getString("IT_AUTMOD");
            if (sAutMod.equals("S")) {
              bAutMod = true;
            }
            else {
              bAutMod = false;

            }
            sAutBaj = rs.getString("IT_AUTBAJA");
            if (sAutBaj.equals("S")) {
              bAutBaj = true;
            }
            else {
              bAutBaj = false;

              //QQ: Nuevos Flags
            }
            sAutBaj = rs.getString("IT_FG_EXPORT");
            if (sAutBaj.equals("S")) {
              bExport = true;
            }
            else {
              bExport = false;

            }
            sAutBaj = rs.getString("IT_FG_GENALAUTO");
            if (sAutBaj.equals("S")) {
              bGenAlauto = true;
            }
            else {
              bGenAlauto = false;

            }
            sAutBaj = rs.getString("IT_FG_MNOTIFS");
            if (sAutBaj.equals("S")) {
              bNotifs = true;
            }
            else {
              bNotifs = false;

            }
            sAutBaj = rs.getString("IT_FG_CONSREST");
            if (sAutBaj.equals("S")) {
              bConsRes = true;
            }
            else {
              bConsRes = false;

              //---.

            }
            sPer = rs.getString("IT_PERFILUSU");

            sEnf = rs.getString("IT_FG_ENFERMO");
            if (sEnf.equals("S")) {
              bEnf = true;
            }
            else {
              bEnf = false;

            }
            sManCat = rs.getString("IT_FG_MNTO");
            if (sManCat.equals("S")) {
              bManCat = true;
            }
            else {
              bManCat = false;

            }
            sManUsu = rs.getString("IT_FG_MNTO_USU");
            if (sManUsu.compareTo("S") == 0) {
              bManUsu = true;
            }
            else {
              bManUsu = false;

            }
            sTraPro = rs.getString("IT_FG_TCNE");
            if (sTraPro.equals("S")) {
              bTraPro = true;
            }
            else {
              bTraPro = false;

            }
            sDefPro = rs.getString("IT_FG_PROTOS");
            if (sDefPro.equals("S")) {
              bDefPro = true;
            }
            else {
              bDefPro = false;

            }
            sAla = rs.getString("IT_FG_ALARMAS");
            if (sAla.equals("S")) {
              bAla = true;
            }
            else {
              bAla = false;

            }
            sVal = rs.getString("IT_FG_VALIDAR");
            if (sVal.equals("S")) {
              bVal = true;
            }
            else {
              bVal = false;

            }

            sDesTel = rs.getString("DS_TELEF");
            sDesDir = rs.getString("DS_DIRECCION");
            sDesCor = rs.getString("DS_EMAIL");
            sDesAno = rs.getString("DS_ANOTACIONES");

            dFecAlt = rs.getDate("FC_ALTA");
            if (dFecAlt != null) {
              sFecAlt = Fechas.date2String(dFecAlt);
            }
            else {
              sFecAlt = "";

            }
            dFecBaj = rs.getDate("FC_BAJA");
            if (dFecBaj != null) {
              sFecBaj = Fechas.date2String(dFecBaj);
            }
            else {
              sFecBaj = "";

            }
            sBaj = rs.getString("IT_BAJA");

            if (sBaj.equals("S")) {
              bBaj = true;
            }
            else {
              bBaj = false;
            }

            /*
                       dFecAlt= rs.getDate("FC_ALTA");
                       dFecBaj= rs.getDate("FC_BAJA");
                       sBaj= rs.getString("IT_BAJA");
                       if (sBaj.equals("S"))  {
                          sFecAlt = "";
                          sFecBaj= Fechas.date2String(dFecBaj);
                          bBaj=true;
                        }
                       else {
                         sFecAlt = Fechas.date2String(dFecAlt);
                         sFecBaj = "";
                         bBaj=false;
                        }
             */
                /* Codigo bueno?? sustituido provisional pues no permite null en fechas
                       if (dFecAlt!= null)
                         sFecAlt = Fechas.date2String(dFecAlt);
                       else
                         sFecAlt = "";
                       if (dFecBaj!= null)
                          sFecBaj= Fechas.date2String(dFecBaj);
                       else
                         sFecBaj = "";
                       if (sBaj.equals("S"))
                          bBaj=true;
                       else bBaj=false;
             */

            sDesPas = rs.getString("DS_PASSWORD");

            // a�ade un nodo
            data.addElement(new DataUsuario(sCodUsu, sCodComAut, sCodEquNot,
                                            sDesNom, sDesApe, bAutAlt, bAutMod,
                                            bAutBaj, sPer, bEnf, bManCat,
                                            bManUsu, bTraPro, bDefPro, bAla,
                                            bVal, sDesTel, sDesDir, sDesCor,
                                            sDesAno, sFecAlt, sFecBaj, bBaj,
                                            sDesPas, vAmbUsu, sDesEquNot,
                                            bresConf, bExport, bGenAlauto,
                                            bNotifs, bConsRes, sCodApl));

          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          //Buscamos las autorizqaciones de cada  usuario y se los a�adimos a la lista

          enum = data.elements();
          while (enum.hasMoreElements()) { //Recorremos lista principal (usuarios)
            datUsuario = (DataUsuario) (enum.nextElement());

            st = con.prepareStatement(sLISTADO_AUTORIZACIONES);
            st.setString(1, datUsuario.getCodUsu().trim());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              String sCodNiv1, sCodNiv2;
              // obtiene los campos
              sCodNiv1 = rs.getString("CD_NIVEL_1");
              sCodNiv2 = rs.getString("CD_NIVEL_2");
              // a�ade un nodo
              datUsuario.getAmbUsu().addElement(sCodNiv1);
              datUsuario.getAmbUsu().addElement(sCodNiv2);

            }

            rs.close();
            st.close();
            rs = null;
            st = null;
          }

          /*
                  //Buscamos, en caso necesario la descripci�n del eq notificador en cada usuario
                  enum = data.elements();
               while (enum.hasMoreElements() ){    //Recorremos lista principal (usuarios)
                    datUsuario= (DataUsuario)(enum.nextElement());
                    if (datUsuario.getCodEquNot()!= "") {
                      st = con.prepareStatement(sBUSQUEDA_DES_EQU_NOT);
                      st.setString(1, datUsuario.getCodUsu().trim() );
                      rs = st.executeQuery();
                      // extrae la p�gina requerida
                       while (rs.next()) {
                         sDesEquNot= rs.getString("DS_E_NOTIF");
                       }
                      datUsuario.setDesEquNot(sDesEquNot);
                      rs.close();
                      st.close();
                      rs = null;
                      st = null;
                    }//if
                    else {
                     sDesEquNot="";
                    }
                   }//while
           */

          break;

        case modoGENERAR_LOGIN:

          data = new CLista();
          sCodUsu = "login";
          sDesPas = "pwd";

          data.addElement(new DataUsuario(sCodUsu, "", "",
                                          "", "", false,
                                          false, false,
                                          "", false, false,
                                          false, false, false,
                                          false, false, "",
                                          "", "", "", "", "", false,
                                          sDesPas, null, "", false,
                                          //QQ: Flags adicionales
                                          false, false, false, false, ""
                                          ));

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();
      ////#// System_out.println("Acomete la transaccion");

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}