/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de usuarios.
 * @ADAPTACION DEL ANTIGUO MANTUS-PISTA AL UTILIZADO EN ICM99 PDP 14/04/2000
 * @version 1.0
 */

package mantus;

import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CCodigo;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CListaValores;
import capp2.CPanel;
import comun.constantes;
//import java.net.*;
import sapp2.Lista;
import sapp2.QueryTool;

public class PanelBusca
    extends CPanel
    implements CInicializar, CFiltro { //CPanel

  // C�digo de la aplicaci�n
  private String sCodAplicacion = null;

  // modos de operaci�n
  final int modoSELECCION_COD = constantes.sSELECCION_X_CODIGO;
  final int modoSELECCION_DES = constantes.sSELECCION_X_DESCRIPCION;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;

  PsApMantUsu PsApp = null;

  XYLayout xYLayout1 = new XYLayout();
  CCodigo txtCod = new CCodigo();
  TextField txtDes = new TextField();
  CListaMantenimiento clmMantenimiento = null;
  ButtonControl btnBuscar = new ButtonControl();
  CheckboxControl chckCod = new CheckboxControl();
  CheckboxControl chckDes = new CheckboxControl();
  CheckboxGroup chkboxGrupo = new CheckboxGroup();

  // Servlet
  private final String strSERVLET_SRVMNTUSU = "servlet/SrvSelUsu";

  // Comunicaci�n con el servlet
  private sapp.StubSrvBD stubCliente = null;

  //Panel1
  public Panel1 tPanel1;

  //Ambito usuario
  public Vector AmbUsu = new Vector();

  // Valores devueltos
  capp.CLista data = null;

  // filtro
  Label label1 = new Label();

  // constructor del panel PanMant
  public PanelBusca(CApp a) {

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {
      setApp(a);

      // C�digo de la aplicaci�n
      sCodAplicacion = this.app.getParametro("COD_APLICACION");

      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Alta Usuario",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar Usuario",
                                     true,
                                     true));
      // etiquetas
      vLabels.addElement(new CColumna("C�digo",
                                      "135",
                                      "COD_USUARIO"));

      vLabels.addElement(new CColumna("Nombre",
                                      "475",
                                      "DS_NOMBRE"));

      clmMantenimiento = new CListaMantenimiento(a,
                                                 vLabels,
                                                 vBotones,
                                                 this,
                                                 this,
                                                 250,
                                                 650);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Lista primeraPagina() {
    Lista l = new Lista();
    DialUsuario dusu = null;
    sapp2.Data d = null;
    DataUsuario du = null;

    for (int i = 0; i < data.size(); i++) {
      d = new sapp2.Data();
      du = (DataUsuario) data.elementAt(i);

      //COD USUARIO
      d.put("COD_USUARIO", du.getCodUsu());

      //NOMBRE
      d.put("DS_NOMBRE", du.getDesNom());

      //PERFIL
      d.put("IT_PERFIL_USU", du.getPer());

      //ANOTACIONES
      d.put("DS_ANOTACIONES", du.getDesAno());

      //DIRECCION
      d.put("DS_DIRECCION", du.getDesDir());

      //EMAIL
      d.put("DS_EMAIL", du.getDesCor());

      //TELEFONO
      d.put("DS_TELEF", du.getDesTel());

      //CODIGO DE EQUIPO NOTIFICADOR
      d.put("CD_E_NOTIF", du.getCodEquNot());

      /*//DESCRIPCION DE EQUIPO NOTIFICADOR
            d.put("DS_E_NOTIF", tPanel1.txtDesEquNot.getText());*/

     //FECHA ALTA
      d.put("FC_ALTA", du.getFecAlt());

      //FECHA BAJA
      d.put("DS_BAJA", du.getFecBaj());

      //CHECK DE BAJA
      boolean bBaj = du.getBaj();
      if (bBaj) {
        d.put("IT_BAJA", "S");
      }
      else {
        d.put("IT_BAJA", "N");
      }

      //AUTORIZACION ALTA
      boolean bAutAlt = du.getAutAlt();
      if (bAutAlt) {
        d.put("IT_AUTALTA", "S");
      }
      else {
        d.put("IT_AUTALTA", "N");
      }

      //AUTORIZACION BAJA
      boolean bAutBaj = du.getAutBaj();
      if (bAutBaj) {
        d.put("IT_AUTBAJA", "S");
      }
      else {
        d.put("IT_AUTBAJA", "N");
      }

      //AUTORIZACION MODIFICACION
      boolean bAutMod = du.getAutMod();
      if (bAutMod) {
        d.put("IT_AUTMOD", "S");
      }
      else {
        d.put("IT_AUTMOD", "N");
      }

      //VALIDACION AUTORIZACIONES
      boolean bVal = du.getVal();
      if (bVal) {
        d.put("IT_FG_VALIDAR", "S");
      }
      else {
        d.put("IT_FG_VALIDAR", "N");
      }

      //MANTENIMIENTO DE USUARIOS
      boolean bMantUsu = du.getManUsu();
      if (bMantUsu) {
        d.put("IT_FG_MNTO_USU", "S");
      }
      else {
        d.put("IT_FG_MNTO_USU", "N");
      }

      //MANTENIMIENTO DE NOTIFICADORES
      boolean bManNotifs = du.getManNotifs();
      if (bManNotifs) {
        d.put("IT_FG_MNOTIFS", "S");
      }
      else {
        d.put("IT_FG_MNOTIFS", "N");
      }

      //MANTENIMIENTO DE TABLAS DE CODIGO
      boolean bManCat = du.getManCat();
      if (bManCat) {
        d.put("IT_FG_MNTO", "S");
      }
      else {
        d.put("IT_FG_MNTO", "N");
      }

      //DISE�O DE PROTOCOLOS
      boolean bDefPro = du.getDefPro();
      if (bDefPro) {
        d.put("IT_FG_PROTOS", "S");
      }
      else {
        d.put("IT_FG_PROTOS", "N");
      }

      //MANTENIMIENTO DE INDICADORES
      boolean bAla = du.getAla();
      if (bAla) {
        d.put("IT_FG_ALARMAS", "S");
      }
      else {
        d.put("IT_FG_ALARMAS", "N");
      }

      //GENERACION DE ALARMAS AUTOMATICAS
      boolean bGenAlauto = du.getGenAlauto();
      if (bGenAlauto) {
        d.put("IT_FG_GENALAUTO", "S");
      }
      else {
        d.put("IT_FG_GENALAUTO", "N");
      }

      //EXPORTACION DE DATOS
      boolean bTraPro = du.getTraPro();
      if (bTraPro) {
        d.put("IT_FG_TCNE", "S");
      }
      else {
        d.put("IT_FG_TCNE", "N");
      }

      //ENVIOS A OTRAS INSTITUCIONES
      boolean bExport = du.getExport();
      if (bExport) {
        d.put("IT_FG_EXPORT", "S");
      }
      else {
        d.put("IT_FG_EXPORT", "N");
      }

      //CONSULTAS RESTRINGIDAS
      boolean bConsRes = du.getConsRes();
      if (bConsRes) {
        d.put("IT_FG_CONSREST", "S");
      }
      else {
        d.put("IT_FG_CONSREST", "N");
      }

      //CONFIDENCIALIDAD
      boolean bEnf = du.getEnf();
      if (bEnf) {
        d.put("IT_FG_ENFERMO", "S");
      }
      else {
        d.put("IT_FG_ENFERMO", "N");
      }

      l.addElement(d);
    }

    return l;
  }

  // Para poder heredar de CApp
  public Lista siguientePagina() {
    return new Lista();
  }

  // inicia el aspecto del panel PanMant
  public void jbInit() throws Exception {

    // Escuchadores de eventos

    PanMant_actionAdapter actionAdapter = new PanMant_actionAdapter(this);
    CListaMantItemAdapter itemAdapter = new CListaMantItemAdapter(this);

    chckCod.setLabel("C�digo");
    chckCod.setCheckboxGroup(chkboxGrupo);
    chckCod.addItemListener(itemAdapter);
    chckCod.setState(true);
    chckDes.setLabel("Nombre");
    chckDes.setCheckboxGroup(chkboxGrupo);
    chckDes.setState(false);
    chckDes.addItemListener(itemAdapter);

    xYLayout1.setWidth(725);
    xYLayout1.setHeight(380);

    txtCod.setName("codigo");
    txtDes.setName("descripcion");

    txtCod.setVisible(true);
    txtDes.setVisible(false);

    btnBuscar.setActionCommand("buscar");
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(actionAdapter);

    this.setLayout(xYLayout1);
    this.add(txtCod, new XYConstraints(103, 6, 312, -1));
    this.add(txtDes, new XYConstraints(103, 6, 312, -1));
    this.add(chckCod, new XYConstraints(30, 36, 130, -1));
    this.add(chckDes, new XYConstraints(212, 36, 130, -1));
    this.add(btnBuscar, new XYConstraints(520, 36, 79, -1));
    this.add(clmMantenimiento, new XYConstraints(7, 80, 710, 294));
    this.add(label1, new XYConstraints(37, 6, 55, -1));

    final String imgBUSCAR = "images/refrescar.gif";
    label1.setText("Usuario:");
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));

    // tool tips

    new CContextHelp("Obtener usuarios", btnBuscar);
  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  // gesti�n de los botones
  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("buscar")) {
      Inicializar(CInicializar.ESPERA);
      try {
        hazBusqueda();
      }
      catch (Exception exc) {
      }
      Inicializar(CInicializar.NORMAL);
    }
  }

  // operaciones de la botonera
  public void realizaOperacion(int j) {

    Lista lisPet = this.clmMantenimiento.getLista();
    Lista lisInd = new Lista();
    DialUsuario dm = null;
    sapp2.Data dPetMod = new sapp2.Data();
    Lista v1 = new Lista();
    Lista v2 = new Lista();
    DataUsuario dusu = null;
    CListaValores lupa = null;
    Vector vCod = null;
    Vector vCod1 = null;
    Vector vCod2 = null;
    QueryTool qt = null;
    QueryTool qt2 = null;

    String sTitle = "";
    int i = 0;

    switch (j) {

      case ALTA:

        vCod1 = new Vector();
        qt = new QueryTool();

        qt.putName("USUARIO");
        qt.putType("COD_USUARIO", QueryTool.STRING);
        qt.putType("NOMBRE", QueryTool.STRING);
        //qt.putType("PASSWORD", QueryTool.STRING);//PDP 28/04/2000

        sTitle = "Usuarios";

        String subQuery = "select CD_USUARIO from SIVE_USUARIO_PISTA";
        qt.putSubquery("COD_USUARIO not ", subQuery);

        vCod1.addElement("COD_USUARIO");
        vCod1.addElement("NOMBRE");
        //vCod1.addElement("PASSWORD");

        lupa = new CListaValores(this.getApp(),
                                 sTitle,
                                 qt,
                                 vCod1);
        lupa.show();

        // graba el item
        if (lupa.getSelected() != null) {

          // PDP: obtenci�n del pseudocapp2 de la p�gina
          PsApp = (PsApMantUsu)this.getApp().getAppletContext().getApplet(
              "PsApMantUsu");

          //PsApp = new PsApMantUsu();

          dPetMod = lupa.getSelected();
          DialUsuario dial = new DialUsuario(this.getApp(),
                                             DialUsuario.modoALTA, dPetMod,
                                             AmbUsu, PsApp);
          dial.show();
          // lo a�adimos al finald ela lista y repintamos la tabla
          if (dial.datoA�adido != null) {
            try {
              hazBusqueda();
            }
            catch (Exception exc) {
              // System_out.println(exc.getMessage());
            }
          }
          dial = null;
        }

        break;

      case MODIFICACION:

        // PDP: obtenci�n del pseudocapp2 de la p�gina
        PsApp = (PsApMantUsu)this.getApp().getAppletContext().getApplet(
            "PsApMantUsu");
        //PsApp = new PsApMantUsu();

        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {

          int ind = clmMantenimiento.getSelectedIndex();
          dusu = (DataUsuario) data.elementAt(ind);
          AmbUsu = dusu.getAmbUsu();

          dm = new DialUsuario(this.getApp(), 1, dPetMod, AmbUsu, PsApp);

          dm.show();
          if (dm.bAceptar()) {
            try {
              hazBusqueda();
            }
            catch (Exception exc) {
              // System_out.println(exc.getMessage());
            }
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un usuario en la tabla.");
        }

        break;
    }
  }

  void chkItemStateChanged(ItemEvent e) {
    if (e.getItem().equals("C�digo")) {
      txtCod.setVisible(true);
      txtCod.setText(txtDes.getText().trim().toUpperCase());
      txtDes.setVisible(false);
    }
    else {
      txtCod.setVisible(false);
      txtDes.setVisible(true);
      txtDes.setText(txtCod.getText().trim());
    }
    doLayout();
  }

  private void hazBusqueda() throws Exception {
    int modoLlamada = -1;
    DataUsuario du = new DataUsuario();

    data = new capp.CLista();

    if (chckCod.isChecked()) {
      modoLlamada = modoSELECCION_COD;
      du.setCodUsu(txtCod.getText().trim());
    }
    else {
      modoLlamada = modoSELECCION_DES;
      du.setDesNom(txtDes.getText().trim());
    }

    du.setCodApl(sCodAplicacion);

    data.addElement(du);

    stubCliente = new sapp.StubSrvBD();

    stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") +
                                        strSERVLET_SRVMNTUSU));
    data = (capp.CLista) stubCliente.doPost(modoLlamada, data);

    /*
         mantus.SrvSelUsu servlet = new mantus.SrvSelUsu();
         //Indica como conectarse a la b.datos
         servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
         data = (capp.CLista) servlet.doDebug(modoLlamada, data);*/

    if (data != null) {
      if (data.size() > 0) {
        clmMantenimiento.setPrimeraPagina(this.primeraPagina());
      }
      else {
        clmMantenimiento.vaciarPantalla();
        this.getApp().showAdvise("El usuario no existe");
      }
    }

  }
}

// botones
class PanMant_actionAdapter
    implements java.awt.event.ActionListener {
  PanelBusca adaptee;
  ActionEvent e;

  PanMant_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    adaptee.btn_actionPerformed(e);
  }
}

// control de cambio de buscar por c�digo/descripci�n
class CListaMantItemAdapter
    implements java.awt.event.ItemListener {
  PanelBusca adaptee;

  CListaMantItemAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkItemStateChanged(e);
  }
}
