
package mantus;

import java.io.Serializable;

public class DataIdentifUsuario
    implements Serializable {

  protected String sCodUsu; //C�digo de usuario
  protected String sDesNom; //Nombre
  protected String sDesApe; //apellidos
  protected String sPer; //perfil

  public DataIdentifUsuario() {
  }

  public DataIdentifUsuario(String codUsu, String desNom, String desApe,
                            String per) {
    sCodUsu = codUsu;
    sDesNom = desNom;
    sDesApe = desApe;
    sPer = per;

  }

  public String getCodUsu() {
    return sCodUsu;
  }

  public String getDesNom() {
    return sDesNom;
  }

  public String getDesApe() {
    return sDesApe;
  }

  public String getPer() {
    return sPer;
  }

}