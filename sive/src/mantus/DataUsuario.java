
package mantus;

import java.io.Serializable;
import java.util.Vector;

public class DataUsuario
    implements Serializable {

  protected String sCodUsu = ""; //C�digo de usuario
  protected String sCodComAut = ""; //Com. aut�noma
  protected String sCodEquNot = ""; //Equipo notificador
  protected String sDesNom = ""; //Nombre
  protected String sDesApe = ""; //apellidos
  protected boolean bAutAlt; //Autorizaci�n para dar altas
  protected boolean bAutMod; //Autorizaci�n para hacer modificaciones
  protected boolean bAutBaj; //Autorizaci�n para dar bajas
  protected String sPer = ""; //perfil
  protected boolean bEnf; //Flag indica si usuario puede ver datos del enfermo
  protected boolean bManCat; //Flag de autorizaci�n a mantenimiento de cat�logos
  protected boolean bManUsu; //Flag de autorizaci�n a mantenimiento de usuarios
  protected boolean bTraPro; //Flag de autorizaci�n de transmisi�n a Madrid e import protocolos
  protected boolean bDefPro; //Flag de autorizaci�n de definici�n de protocolos
  protected boolean bAla; //Flag de autorizaci�n de creaci�n de definiciones de alarmas
  protected boolean bVal; //Flag de autorizaci�n de validaci�n de notificaciones
  protected String sDesTel = ""; //Tel�fonos de contacto
  protected String sDesDir = ""; //Direcci�n de contacto
  protected String sDesCor = ""; //Correo electr�nico
  protected String sDesAno = ""; //Otras anotaciones
  protected String sFecAlt = ""; //Fecha de alta
  protected String sFecBaj = ""; //Fecha de baja
  protected boolean bBaj; //Marca de baja
  protected String sDesPas = ""; //Password del usuario
  protected Vector vAmbUsu = new Vector(); //Ambito del usuario
  protected String sDesEquNot = ""; //Descripci�n eq.notificador
  //QQ: Nuevos Flags:
  protected boolean bExport; //Flag Exportaciones ASCII
  protected boolean bGenAlauto; //Flag Generaci�n de Alarmas Autom�ticas
  protected boolean bManNotifs; //Flag de Mantenimiento de Centros y Equipos Notificadores
  protected boolean bConsRes; //Flag de Acceso a Consultas Restringidas
  protected boolean bResConf; //Flag de Resoluci�n de conflictos
  protected String sCodApl = "";

  public DataUsuario() {
  }

  public DataUsuario(

      String codUsu, String codComAut, String codEquNot,
      String desNom, String desApe, boolean autAlt, boolean autMod,
      boolean autBaj, String per, boolean enf, boolean manCat, boolean manUsu,
      boolean traPro, boolean defPro, boolean ala, boolean val, String desTel,
      String desDir, String desCor, String desAno, String fecAlt, String fecBaj,
      boolean baj, String desPas, Vector ambUsu, String desEquNot,
      boolean resConf,
      //QQ:Nuevos Flags
      boolean Export, boolean GenAlauto, boolean ManNotifs, boolean ConsRes,
      String CodApl) {

    sCodUsu = codUsu;
    sCodComAut = codComAut;
    sCodEquNot = codEquNot;
    sDesNom = desNom;
    sDesApe = desApe;
    bAutAlt = autAlt;
    bAutMod = autMod;
    bAutBaj = autBaj;
    sPer = per;
    bEnf = enf;
    bManCat = manCat;
    bManUsu = manUsu;
    bTraPro = traPro;
    bDefPro = defPro;
    bAla = ala;
    bVal = val;
    sDesTel = desTel;
    sDesDir = desDir;
    sDesCor = desCor;
    sDesAno = desAno;
    sFecAlt = fecAlt;
    sFecBaj = fecBaj;
    bBaj = baj;
    sDesPas = desPas;
    vAmbUsu = ambUsu;
    sDesEquNot = desEquNot;
    //QQ:Nuevos Flags
    bExport = Export;
    bGenAlauto = GenAlauto;
    bManNotifs = ManNotifs;
    bConsRes = ConsRes;
    sCodApl = CodApl;
  }

  public String getCodUsu() {
    return sCodUsu;
  }

  public String getCodComAut() {
    return sCodComAut;
  }

  public String getCodEquNot() {
    String s;

    s = sCodEquNot;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public String getDesNom() {
    return sDesNom;
  }

  public String getDesApe() {
    String s;

    s = sDesApe;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public boolean getAutAlt() {
    return bAutAlt;
  }

  public boolean getAutMod() {
    return bAutMod;
  }

  public boolean getAutBaj() {
    return bAutBaj;
  }

  public boolean getResConf() {
    return bResConf;
  }

  //---QQ:Nuevos Flags:

  public boolean getExport() {
    return bExport;
  }

  public boolean getGenAlauto() {
    return bGenAlauto;
  }

  public boolean getManNotifs() {
    return bManNotifs;
  }

  public boolean getConsRes() {
    return bConsRes;
  }

  //----.

  public String getPer() {
    String s;

    s = sPer;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public boolean getEnf() {
    return bEnf;
  }

  public boolean getManCat() {
    return bManCat;
  }

  public boolean getManUsu() {
    return bManUsu;
  }

  public boolean getTraPro() {
    return bTraPro;
  }

  public boolean getDefPro() {
    return bDefPro;
  }

  public boolean getAla() {
    return bAla;
  }

  public boolean getVal() {
    return bVal;
  }

  public String getDesTel() {
    String s;

    s = sDesTel;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public String getDesDir() {
    String s;

    s = sDesDir;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public String getDesCor() {
    String s;

    s = sDesCor;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public String getDesAno() {
    String s;

    s = sDesAno;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public String getFecAlt() {
    String s;

    s = sFecAlt;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public String getFecBaj() {
    String s;

    s = sFecBaj;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public boolean getBaj() {
    return bBaj;
  }

  public String getDesPas() {
    return sDesPas;
  }

  public Vector getAmbUsu() {
    return vAmbUsu;
  }

  public String getDesEquNot() {
    return sDesEquNot;
  }

  public String getCodApl() {
    return sCodApl;
  }

  public void setAmbUsu(Vector v) {
    vAmbUsu = v;
  }

  public void setDesEquNot(String desEquNot) {
    sDesEquNot = desEquNot;
    if (desEquNot == null) {
      sDesEquNot = "";
    }
  }

  public void setCodApl(String CodApl) {
    sCodApl = CodApl;
    if (CodApl == null) {
      sCodApl = "";
    }
  }

  public void setCodComAut(String CodAut) {
    sCodComAut = CodAut;
    if (CodAut == null) {
      sCodComAut = "";

    }
  }

  public void setDesNom(String desNom) {
    sDesNom = desNom;
    if (desNom == null) {
      sDesNom = "";
    }
  }

  public void setPer(String per) {
    sPer = per;
    if (per == null) {
      sPer = "";
    }
  }

  public void setEnf(boolean enf) {
    bEnf = enf;
  }

  public void setCodUsu(String codUsu) {
    sCodUsu = codUsu;
    if (codUsu == null) {
      sCodUsu = "";
    }
  }

}
