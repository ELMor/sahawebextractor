
package enfermotub;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Choice;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
//j2000
import comun.Common;
import comun.DataEnfermo;
import comun.constantes;
import sapp.StubSrvBD;
import suca.datasuca;
import suca.srvsuca;

public class DialOtrasDirecciones
    extends CDialog {

//__________________________________________________ MODOS
  //modos de operaci�n de la ventana
  public final int modoINICIO = 0; // solo puede apretar boton b�squeda
  ResourceBundle res;
  public final int modoMODIFICAR = 1; // modifica datos del enfermo
  public final int modoSELECCION = 2;
  public final int modoOBTENER = 4;
  public final int modoESPERA = 5; // para cuano se va a por datos

  public StubSrvBD stubCliente = new StubSrvBD();

  protected Pan_Enfermo pan_enfer = null;

  protected XYLayout xYLayout = new XYLayout();

  protected boolean sinBloquear = true;
  /** es la lista que tiene los datos del enfermo */
  protected CLista listaEnfermo = null;
  protected CLista listaEnfermoBK = null;

  protected boolean bCambioOtrasDirecciones = false;

  /** Lista que contiene todos los paises posibles, es fija */
  // LRG
//  protected CLista  listaPaises = null;
  /** Lista que contiene las regiones posibles de Espa�a */
  protected CLista listaCA = null;
  /** Lista que contiene las provincias 1*/
  protected CLista listaProvincias2 = null;
  /** Lista que contiene las provincias 2*/
  protected CLista listaProvincias3 = null;

  /** indica en que estado est� el frame */
  protected int modo = 0;
  protected int modoAnterior = 0;

  //Servlet
  protected final String strSERVLET_SUCA = "servlet/srvsuca";
  protected boolean bTramero = false;

  /// componentes VISUALES
  Label lblcalle2 = new Label();
  TextField txtCalle2 = new TextField();
  Label lblNumero = new Label();
  TextField txtNumero2 = new TextField();
  Label lblPiso2 = new Label();
  TextField txtPiso2 = new TextField();
  Label lblCP = new Label();
  TextField txtCP2 = new TextField();
  Label lblMunicipio2 = new Label();
  TextField txtMunicipio2 = new TextField();
  TextField txtMunicipio2L = new TextField();
  ButtonControl btnMunicipio2 = new ButtonControl();
//  Label lblPais2 = new Label();  LRG
  Label lblCA2 = new Label();
  Label lblProvincia2 = new Label();
//  Choice chPais3 = new Choice();  LRG
  Label lblProvincia3 = new Label();
  Label lblMunicipio3 = new Label();
  Label lblPiso3 = new Label();
  TextField txtCP3 = new TextField();
  TextField txtPiso3 = new TextField();
  Label lblCP1 = new Label();
  TextField txtNumero3 = new TextField();
  Label lblNumero1 = new Label();
  Label lblCA3 = new Label();
  TextField txtMunicipio3 = new TextField();
  TextField txtMunicipio3L = new TextField();
  ButtonControl btnMunicipio3 = new ButtonControl();
//  Label lblPais3 = new Label(); LRG
  Label lblcalle3 = new Label();
  TextField txtCalle3 = new TextField();
  Label lbltelefono2 = new Label();
  Label lblTelefono3 = new Label();
  TextField txtTelefono3 = new TextField();
  TextField txtTelefono2 = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  TextField txtObserva2 = new TextField();
  Label lblObserva3 = new Label();
  TextField txtObserva3 = new TextField();
//  Choice chPais2 = new Choice();   LRG
  Choice chCA2 = new Choice();
  Choice chProvincia2 = new Choice();
  Choice chCA3 = new Choice();
  Choice chProvincia3 = new Choice();
  GroupBox pnlDir2 = new GroupBox();
  GroupBox pnlDir3 = new GroupBox();

  //______________________________________________ GESTOR de EVENTOS
  DialOtrasDireccionesBtnActionListener btnActionListener = new
      DialOtrasDireccionesBtnActionListener(this);
  DialOtrasDireccionesTextAdapter textAdapter = new
      DialOtrasDireccionesTextAdapter(this);
  DialOtrasDireccionesChoiceItemListener chItemListener = new
      DialOtrasDireccionesChoiceItemListener(this);
  DialOtrasDireccionesTextFocusListener focusAdapter = new
      DialOtrasDireccionesTextFocusListener(this);

  Label lblObserva2 = new Label();

  //______________________________________________  CONSTRUCTOR
  public DialOtrasDirecciones(CApp app, Pan_Enfermo a_pan) {
    super(app);
    res = ResourceBundle.getBundle("enfermotub.Res" + app.getIdioma());
    setTitle(res.getString("msg18.Text"));
    pan_enfer = a_pan;

    // cogemos del panel los paises
//    listaPaises = pan_enfer.getListaPaises();  LRG
    listaCA = pan_enfer.getListaCA();
    bTramero = pan_enfer.btramero;

    try {

      //Se traen las listas de paises y de comunidades aut�nomas espa�olas
      while (listaCA == null) {
//          listaPaises = pan_enfer.getListaPaises();  LRG
        listaCA = pan_enfer.getListaCA();
        Thread.sleep(50);
      }
      //Se escriben los datos de las listas en los choices
//    comun.writeChoice(app, chPais2, listaPaises, true,"DS_PAIS");  LRG
//    comun.writeChoice(app, chPais3, listaPaises, true,"DS_PAIS");  LRG
      Common.writeChoice(app, chCA2, listaCA, true, "DS_CA");
      Common.writeChoice(app, chCA3, listaCA, true, "DS_CA");

      chProvincia2.add("  ");
      chProvincia3.add("  ");

      jbInit();
      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /** ete m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modo;
      modo = modoESPERA;
      Inicializar();
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    if (modo == modoESPERA) {
      modo = modoAnterior;
    }
    Inicializar();

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        Common.imgLUPA,
        Common.imgACEPTAR,
        Common.imgLIMPIAR,
        Common.imgCANCELAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    setTitle(res.getString("msg19.Text"));

    xYLayout.setHeight(434);
    xYLayout.setWidth(561);

    setSize(561, 403);

    setLayout(xYLayout);

    // ponemos la imagen de la lupa
    btnMunicipio2 = new ButtonControl(imgs.getImage(0));
    btnMunicipio3 = new ButtonControl(imgs.getImage(0));
    btnAceptar = new ButtonControl(imgs.getImage(1));
    btnCancelar = new ButtonControl(imgs.getImage(3));

    lblObserva3.setText(res.getString("lblObserva3.Text"));

    lblcalle2.setText(res.getString("lblcalle2.Text"));
    lblNumero.setText(res.getString("lblNumero.Text"));
    lblPiso2.setText(res.getString("lblPiso2.Text"));
    lblCP.setText(res.getString("lblCP.Text"));
    lblMunicipio2.setText(res.getString("lblMunicipio2.Text"));
    btnMunicipio2.setLabel("");
//  lblPais2.setText(res.getString("lblPais2.Text")); LRG
    lblCA2.setText(res.getString("lblCA2.Text"));
    lblProvincia2.setText(res.getString("lblProvincia2.Text"));
    lbltelefono2.setText(res.getString("lbltelefono2.Text"));
    lblTelefono3.setText(res.getString("lbltelefono2.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    lblProvincia3.setText(res.getString("lblProvincia2.Text"));
    lblMunicipio3.setText(res.getString("lblMunicipio2.Text"));
    lblPiso3.setText(res.getString("lblPiso2.Text"));
    lblCP1.setText(res.getString("lblCP.Text"));
    lblNumero1.setText(res.getString("lblNumero.Text"));
    lblCA3.setText(res.getString("lblCA2.Text"));
//  lblPais3.setText(res.getString("lblPais2.Text"));   LRG
    lblcalle3.setText(res.getString("lblcalle2.Text"));
    btnMunicipio3.setLabel("");

    pnlDir2.setLabel(res.getString("pnlDir2.Label"));

    pnlDir3.setLabel(res.getString("pnlDir3.Label"));
    lblObserva2.setText(res.getString("lblObserva3.Text"));

    /// fijamos nombre a los botones
    btnMunicipio2.setActionCommand("municipio2");
    btnMunicipio3.setActionCommand("municipio3");
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    // ponemos nombre a los choices
//  chPais2.setName("pais2");  LRG
    chCA2.setName("ccaa2");
    chProvincia2.setName("provincia2");
//  chPais3.setName("pais3");   LRG
    chCA3.setName("ccaa3");
    chProvincia3.setName("provincia3");

    // a�adimos el nombre de la cjaa de texto
    txtMunicipio2.setName("municipio2");
    txtMunicipio3.setName("municipio3");
    // deshabilitamos las descripciones
    txtMunicipio2L.setEditable(false);
    txtMunicipio2L.setEnabled(false);
    txtMunicipio3L.setEditable(false);
    txtMunicipio3L.setEnabled(false);

    //________ colocamos com,ponentes
    this.add(lbltelefono2, new XYConstraints(18, 46, 48, 20));
    this.add(txtTelefono2, new XYConstraints(72, 46, 107, 20));
    this.add(lblcalle2, new XYConstraints(18, 78, 49, 20));
    this.add(txtCalle2, new XYConstraints(72, 78, 219, 20));
    this.add(lblNumero, new XYConstraints(323, 78, 55, 20));
    this.add(txtNumero2, new XYConstraints(389, 78, 44, 20));
    this.add(lblPiso2, new XYConstraints(450, 78, 37, 20));
    this.add(txtPiso2, new XYConstraints(490, 78, 48, 20));
//  this.add(lblPais2, new XYConstraints(18, 106, 40, 20));  LRG
//    this.add(chPais2, new XYConstraints(72, 106, 106, 20));       LRG
//    this.add(lblCA2, new XYConstraints(195, 106, 28, 20));
//    this.add(chCA2, new XYConstraints(225, 106, 106, 20));
    this.add(lblCA2, new XYConstraints(18, 106, 28, 20));
    this.add(chCA2, new XYConstraints(72, 106, 106, 20));

//    this.add(lblProvincia2, new XYConstraints(345, 106, 61, 22));
//    this.add(chProvincia2, new XYConstraints(415, 106, 106, 20));
    this.add(lblProvincia2, new XYConstraints(195, 106, 61, 22));
    this.add(chProvincia2, new XYConstraints(260, 106, 106, 20));

    this.add(lblCP, new XYConstraints(18, 138, 45, 20));
    this.add(txtCP2, new XYConstraints(72, 138, 106, 20));
    this.add(lblMunicipio2, new XYConstraints(195, 138, 60, 20));
    this.add(txtMunicipio2, new XYConstraints(260, 138, 50, 20));
    this.add(btnMunicipio2, new XYConstraints(319, 136, -1, -1));
    this.add(txtMunicipio2L, new XYConstraints(356, 138, 184, 20));
    this.add(txtObserva2, new XYConstraints(117, 169, 422, 20));
    this.add(lblObserva2, new XYConstraints(18, 169, 85, 21));

    this.add(lblTelefono3, new XYConstraints(18, 224, 48, 20));
    this.add(txtTelefono3, new XYConstraints(72, 224, 107, 20));
    this.add(lblcalle3, new XYConstraints(18, 251, 49, 20));
    this.add(txtCalle3, new XYConstraints(72, 251, 217, 20));
    this.add(txtNumero3, new XYConstraints(392, 251, 44, 20));
    this.add(lblNumero1, new XYConstraints(326, 251, 55, 20));
    this.add(lblPiso3, new XYConstraints(453, 251, 33, 21));
    this.add(txtPiso3, new XYConstraints(490, 251, 48, 20));
//  this.add(lblPais3, new XYConstraints(18, 281, 40, 20));       LRG
//    this.add(chPais3, new XYConstraints(72, 281, 106, 20));       LRG
//    this.add(lblCA3, new XYConstraints(195, 281, 25, 22));
//    this.add(chCA3, new XYConstraints(225, 281, 106, 20));
    this.add(lblCA3, new XYConstraints(18, 281, 25, 22));
    this.add(chCA3, new XYConstraints(72, 281, 106, 20));

//    this.add(lblProvincia3, new XYConstraints(345, 283, 61, 22));
//    this.add(chProvincia3, new XYConstraints(415, 281, 106, 20));
    this.add(lblProvincia3, new XYConstraints(195, 283, 61, 22));
    this.add(chProvincia3, new XYConstraints(260, 281, 106, 20));

    this.add(txtCP3, new XYConstraints(72, 315, 106, 20));
    this.add(lblCP1, new XYConstraints(18, 315, 45, 20));
    this.add(lblMunicipio3, new XYConstraints(195, 315, 60, 20));
    this.add(txtMunicipio3, new XYConstraints(260, 315, 50, 20));
    this.add(btnMunicipio3, new XYConstraints(319, 315, -1, -1));
    this.add(txtMunicipio3L, new XYConstraints(356, 315, 184, 20));
    this.add(txtObserva3, new XYConstraints(117, 347, 422, 20));
    this.add(lblObserva3, new XYConstraints(18, 347, 85, 21));

    this.add(btnAceptar, new XYConstraints(390, 392, -1, -1));
    this.add(btnCancelar, new XYConstraints(470, 392, -1, -1));

    this.add(pnlDir2, new XYConstraints(7, 20, 548, 181));
    this.add(pnlDir3, new XYConstraints(7, 208, 548, 172));

    // gesti�n de eventos de botones
    btnMunicipio2.addActionListener(btnActionListener);
    btnMunicipio3.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    // gestion de evenetos de los Choices
    chCA2.addItemListener(chItemListener);
    chCA3.addItemListener(chItemListener);
    chProvincia2.addItemListener(chItemListener);
    chProvincia3.addItemListener(chItemListener);
//    chPais2.addItemListener(chItemListener);        LRG
//    chPais3.addItemListener(chItemListener);       LRG

    // gesti�n de eventos de las cajas de texto
    txtMunicipio2.addActionListener(focusAdapter);
    txtMunicipio3.addActionListener(focusAdapter);

    // modificaci�n de los datos de las cajas
    txtMunicipio2.addKeyListener(textAdapter);
    txtMunicipio3.addKeyListener(textAdapter);

    txtMunicipio2.addFocusListener(focusAdapter);
    txtMunicipio3.addFocusListener(focusAdapter);

    // se inicializan los botones a false o true
    this.modo = modoINICIO;
    Inicializar();

  }

  //____________________________________________________________
  /**
   *  esta funci�n fija los parametros para que se muestren los datos
   * Es llmada inicialmente desde el panel
   */
  public void setData(CLista a_dat) {
    listaEnfermo = a_dat;
    listaEnfermoBK = a_dat;
    rellenaPantalla();
    //_________ LRG

    //Se pone a false todo
    this.modo = modoESPERA;
    Inicializar();
    //Se pone a true todo lo necesario
    //(ser� seg�n �ndices rellenados en choices)
    this.modo = modoINICIO;
    Inicializar();

    //________ TRAZA_______________________________________________

    // System_out.println("******************************************");
    // System_out.println("DIALOTRASDIR listaEnfermo Tras  rellenaPantalla() :");
    if (listaEnfermo != null) {
      for (int cont = 0; cont < listaEnfermo.size(); cont++) {
        // System_out.println("    Elemento " + cont);
        Object elemLista = listaEnfermo.elementAt(cont);
        if ( (elemLista.getClass()) == (new DataEnfermo(" ").getClass())) {
          Hashtable laHash = (Hashtable) elemLista;
          Enumeration enum = laHash.keys();
          while (enum.hasMoreElements()) {
            String clave = (String) (enum.nextElement());
            Object valor = null;
            valor = laHash.get(clave);
            if (valor.getClass() == (new String()).getClass()) {
              // System_out.println("        Clave  "+clave+"                 " + (String)valor);
            }
            else {
              // System_out.println("        Clave  "+clave+"   No es un String  ");
            }

          } //While
        } //if
      } //for
    }
    //________ FIN TRAZA_______________________________________________

  }

  /**
   *  esta funci�n devuelvelos valores calculados
   */
  public CLista getData() {
    return listaEnfermo;
  }

  public boolean getCambioOtrasDirecciones() {
    return bCambioOtrasDirecciones;
  }

  //____________________________________________________________
  /**
   *  comprueba que los datos de la pantalla sean v�lidos
   *  comprueba que esten informados los campos obligatorios
   *  y que los campos sean de la longitud adecuada
   *  rellena la lista con esos datos
   * Si campos no v�lidos muestra mensaje y devuelve false
   * Si datos no han variado devuelve lista vac�a
   */
  protected boolean isDataValid(CLista a_lista) {
    CMessage msgBox;
    Hashtable hash = null;
    int indice;
    String campo = null;
    String dato = null;
    boolean campos_validos = true; // indica si son validos los campos o no
    Hashtable parameter = (Hashtable) (a_lista.firstElement());
    Hashtable datoInicial = (Hashtable) listaEnfermo.firstElement();
    String mensaje = null;
    boolean cambio = false;
    String desDato = "";

    // comprueba que esten informados los campos obligatorios, con longitud adecuada

    //Nota: Tal como se implementa aqu�, la variable cambio puede ponerse a true
    //aunque luego no salgamos del di�logo por haber una cadena de longitud excesiva
    //Eso no es problema. En siguiente vez que demos a aceptar se pone cambio a false
    // y se vuelve a comprobar todo

    //Telefonos
    dato = txtTelefono2.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 14) {
      parameter.put("DS_TELEF2", dato);
    }
    else if (dato != null && dato.length() > 14) {
      campos_validos = false;
      mensaje = res.getString("msg30.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_TELEF2"));

    dato = txtTelefono3.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 14) {
      parameter.put("DS_TELEF3", dato);
    }
    else if (dato != null && dato.length() > 14) {
      campos_validos = false;
      mensaje = res.getString("msg29.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_TELEF3"));

    //Calles
    dato = txtCalle2.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 40) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_DIREC2", dato);
    }
    else if (dato != null && dato.length() > 40) { // el campo no cumplelos requisitos
      campos_validos = false;
      mensaje = res.getString("msg20.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_DIREC2"));

    dato = txtCalle3.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 40) {
      parameter.put("DS_DIREC3", dato);
    }
    else if (dato != null && dato.length() > 40) {
      campos_validos = false;
      mensaje = res.getString("msg28.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_DIREC3"));

    //N�meros de calle
    dato = txtNumero2.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 5) {
      parameter.put("DS_NUM2", dato);
    }
    else if (dato != null && dato.length() > 5) {
      campos_validos = false;
      mensaje = res.getString("msg21.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_NUM2"));

    dato = txtNumero3.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 5) {
      parameter.put("DS_NUM3", dato);
    }
    else if (dato != null && dato.length() > 5) {
      campos_validos = false;
      mensaje = res.getString("msg26.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_NUM3"));

    //Pisos
    dato = txtPiso2.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 5) {
      parameter.put("DS_PISO2", dato);
    }
    else if (dato != null && dato.length() > 5) {
      campos_validos = false;
      mensaje = res.getString("msg22.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_PISO2"));

    dato = txtPiso3.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 5) {
      parameter.put("DS_PISO3", dato);
    }
    else if (dato != null && dato.length() > 5) {
      campos_validos = false;
      mensaje = res.getString("msg25.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_PISO3"));

//__________ Codigo LRG ________________________________

    //Com Aut�nomas
    indice = chCA2.getSelectedIndex();
    indice--;
    if (indice >= 0) {
      hash = (Hashtable) listaCA.elementAt(indice);
      dato = (String) hash.get("CD_CA");
      if (dato != null && dato.length() > 0) {
        //No necesario tener en cuenta cambios en CA: no va a b.datos (solo cambios prov)
//            cambio = cambio ||  comun.ha_cambiado(dato,  (String)datoInicial.get("CD_CA2"));
        parameter.put("CD_CA2", dato.toUpperCase());
      }
    }
    else {
      parameter.put("CD_CA2", "");
    }

    indice = chCA3.getSelectedIndex();
    indice--;
    if (indice >= 0) {
      hash = (Hashtable) listaCA.elementAt(indice);
      dato = (String) hash.get("CD_CA");
      if (dato != null && dato.length() > 0) {
        //No necesario tener en cuenta cambios en CA: no va a b.datos (solo cambios prov)
//            cambio = cambio ||  comun.ha_cambiado(dato,  (String)datoInicial.get("CD_CA2"));
        parameter.put("CD_CA3", dato.toUpperCase());
      }
    }
    else {
      parameter.put("CD_CA3", "");
    }
    //____________________

    //Provincias
    indice = chProvincia2.getSelectedIndex();
    indice--;
    if (indice >= 0) {
      hash = (Hashtable) listaProvincias2.elementAt(indice);
      dato = (String) hash.get("CD_PROV");
      desDato = (String) hash.get("DS_PROV"); //LRG
      if (dato != null && dato.length() > 0) {
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("CD_PROV2"));
        parameter.put("CD_PROV2", dato.toUpperCase());
        parameter.put("DS_PROV2", desDato.toUpperCase()); //LRG
      }
    }
    else {
      parameter.put("CD_PROV2", "");
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CD_PROV2")) &&
          (! ( ( (String) datoInicial.get("CD_PROV2")).equals("")))) {
        cambio = true;
      }
    }

    indice = chProvincia3.getSelectedIndex();
    indice--;
    if (indice >= 0) {
      hash = (Hashtable) listaProvincias3.elementAt(indice);
      dato = (String) hash.get("CD_PROV");
      desDato = (String) hash.get("DS_PROV"); //LRG
      if (dato != null && dato.length() > 0) {
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("CD_PROV3"));
        parameter.put("CD_PROV3", dato.toUpperCase());
        parameter.put("DS_PROV3", desDato.toUpperCase()); //LRG
      }
    }
    else {
      parameter.put("CD_PROV3", "");
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CD_PROV3")) &&
          (! ( ( (String) datoInicial.get("CD_PROV3")).equals("")))
          ) {
        cambio = true;
      }
    }

    //C�digos post
    dato = txtCP2.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 5) {
      parameter.put("CD_POST2", dato);
    }
    else if (dato != null && dato.length() > 5) {
      campos_validos = false;
      mensaje = res.getString("msg23.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("CD_POST2"));

    dato = txtCP3.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 5) {
      parameter.put("CD_POST3", dato);
    }
    else if (dato != null && dato.length() > 5) {
      campos_validos = false;
      mensaje = res.getString("msg23.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("CD_POST3"));

    //Municipios 2 y 3

    /*
        dato  = txtMunicipio2.getText().trim();
        if (dato != null && dato.length() >= 0 && dato.length() <= 3) {
             cambio = cambio ||  comun.ha_cambiado(dato,  (String)datoInicial.get("CD_MUNI2"));
             parameter.put("CD_MUNI2", dato.toUpperCase());
        }else if (dato != null && dato.length() > 3){
             campos_validos = false;
             mensaje = res.getString("msg24.Text");
        }
     */
    //LRG
    //Municipio: No necesario verificar longitud (desc viene de b.datos)
    dato = txtMunicipio2.getText().trim();
    desDato = txtMunicipio2L.getText().trim(); //LRG
    //Si se ha tra�do descripci�n
    if (desDato != null && desDato.length() > 0) {
      //Se a�ade c�d y desc mun  a la lista
      parameter.put("CD_MUNI2", dato.toUpperCase());
      parameter.put("DS_MUNI2", desDato.toUpperCase()); //LRG
    }
    else {
      parameter.put("CD_MUNI2", "");
      parameter.put("DS_MUNI2", ""); //LRG
    }
    //Se ve si ha cambiado c�digo
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("CD_MUNI2"));

    /*
        dato  = txtMunicipio3.getText().trim();
        if (dato != null && dato.length() >= 0 && dato.length() <= 3) {
             cambio = cambio ||  comun.ha_cambiado(dato,  (String)datoInicial.get("CD_MUNI3"));
             parameter.put("CD_MUNI3", dato.toUpperCase());
        }else if (dato != null && dato.length() > 3){
             campos_validos = false;
             mensaje = res.getString("msg27.Text");
        }
     */
    //LRG
    //Municipio: No necesario verificar longitud (desc viene de b.datos)
    dato = txtMunicipio3.getText().trim();
    desDato = txtMunicipio3L.getText().trim(); //LRG
    //Si se ha tra�do descripci�n
    if (desDato != null && desDato.length() > 0) {
      //Se a�ade c�d y desc mun  a la lista
      parameter.put("CD_MUNI3", dato.toUpperCase());
      parameter.put("DS_MUNI3", desDato.toUpperCase()); //LRG
    }
    else {
      parameter.put("CD_MUNI3", "");
      parameter.put("DS_MUNI3", ""); //LRG
    }
    //Se ve si ha cambiado c�digo
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("CD_MUNI3"));

    //Observaciones
    dato = txtObserva2.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 40) {
      parameter.put("DS_OBSERV2", dato);
    }
    else if (dato != null && dato.length() > 40) {
      campos_validos = false;
      mensaje = res.getString("msg31.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_OBSERV2"));

    dato = txtObserva3.getText().trim();
    if (dato != null && dato.length() >= 0 && dato.length() <= 40) {
      parameter.put("DS_OBSERV3", dato);
    }
    else if (dato != null && dato.length() > 40) {
      campos_validos = false;
      mensaje = res.getString("msg32.Text");
    }
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.get("DS_OBSERV3"));

    if (!cambio) {
      // como no hay cambio quitamos todos los elementos
      //La lista que se devolver� luego al panel ser� la misma que nos pas�
      a_lista.removeAllElements();
    }

    if (!campos_validos) {
      msgBox = new CMessage(app, CMessage.msgAVISO, mensaje);
      msgBox.show();
      msgBox = null;
    }

    return campos_validos;
  }

  //____________________________________________________________

  public void txtFocusMunicipio(int a_iNumMunicipio) {
    CLista data = new CLista();
    CLista result;
    String campo;
    TextField punteroText;
    CLista listaProvincias;
    int indice;

    if (a_iNumMunicipio == 2) {
      campo = txtMunicipio2.getText();
      indice = chProvincia2.getSelectedIndex();
      punteroText = txtMunicipio2L;
      listaProvincias = listaProvincias2;
    }
    else {
      campo = txtMunicipio3.getText();
      indice = chProvincia3.getSelectedIndex();
      punteroText = txtMunicipio3L;
      listaProvincias = listaProvincias3;
    }
    indice--; // quitamos el blanco de la choice

    if (campo != null && campo.trim().length() > 0) {
      DataEnfermo denfer = new DataEnfermo("CD_MUN");
      denfer.setFiltro(campo);
      if (indice >= 0 && indice < listaProvincias.size()) {
        campo = (String) ( (Hashtable) listaProvincias.elementAt(indice)).get(
            "CD_PROV");
      }
      if (campo != null && (campo.trim().length() > 0)) {
        denfer.put("CD_PROV", campo);
      }
      data.addElement(denfer);
      result = Common.traerDatos(app, stubCliente,
                                 constantes.strSERVLET_ENFERMO,
                                 SrvEnfermo.modoMUNICIPIO +
                                 SrvGeneral.servletOBTENER_X_CODIGO, data);

      if (result != null && result.size() > 0) {
        DataEnfermo dato = (DataEnfermo) result.firstElement();
        if (dato != null) {
          punteroText.setText( (String) dato.get("DS_MUN"));
        }
      }
      else {
        CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                       res.getString("msg33.Text"));
        msgBox.show();
        msgBox = null;
      }
    }
  }

  //____________________________________________________________
  /**
       *   esta funcion rellena toda la pantalla con los datos de la Lista de Enfermo
   */
  public void rellenaPantalla() {
    Hashtable hash = null;
    String desMun2 = null, desMun3 = null;
    String dato = null;

    if (listaEnfermo != null) {
      hash = (Hashtable) listaEnfermo.firstElement();
    }

    if (hash == null) {
      return;
    }

    dato = (String) hash.get("DS_DIREC2");
    if (dato != null) {
      txtCalle2.setText(dato);
    }
    else {
      txtCalle2.setText("");
    }
    dato = (String) hash.get("DS_NUM2");
    if (dato != null) {
      txtNumero2.setText(dato);
    }
    else {
      txtNumero2.setText("");
    }
    dato = (String) hash.get("DS_PISO2");
    if (dato != null) {
      txtPiso2.setText(dato);
    }
    else {
      txtPiso2.setText("");
    }
    dato = (String) hash.get("CD_POST2");
    if (dato != null) {
      txtCP2.setText(dato);
    }
    else {
      txtCP2.setText("");
    }
    dato = (String) hash.get("CD_MUNI2");
    if (dato != null) {
      txtMunicipio2.setText(dato);
    }
    else {
      txtMunicipio2.setText("");
    }
    dato = (String) hash.get("DS_MUNI2");
    if (dato != null) {
      txtMunicipio2L.setText(dato);
    }
    else {
      txtMunicipio2L.setText("");
    }

    dato = (String) hash.get("CD_POST3");
    if (dato != null) {
      txtCP3.setText(dato);
    }
    else {
      txtCP3.setText("");
    }
    dato = (String) hash.get("DS_PISO3");
    if (dato != null) {
      txtPiso3.setText(dato);
    }
    else {
      txtPiso3.setText("");
    }
    dato = (String) hash.get("DS_NUM3");
    if (dato != null) {
      txtNumero3.setText(dato);
    }
    else {
      txtNumero3.setText("");
    }
    dato = (String) hash.get("CD_MUNI3");
    if (dato != null) {
      txtMunicipio3.setText(dato);
    }
    else {
      txtMunicipio3.setText("");
    }
    dato = (String) hash.get("DS_MUNI3");
    if (dato != null) {
      txtMunicipio3L.setText(dato);
    }
    else {
      txtMunicipio3L.setText("");
    }

    dato = (String) hash.get("DS_DIREC3");
    if (dato != null) {
      txtCalle3.setText(dato);
    }
    else {
      txtCalle3.setText("");
    }
    dato = (String) hash.get("DS_TELEF3");
    if (dato != null) {
      txtTelefono3.setText(dato);
    }
    else {
      txtTelefono3.setText("");
    }
    dato = (String) hash.get("DS_TELEF2");
    if (dato != null) {
      txtTelefono2.setText(dato);
    }
    else {
      txtTelefono2.setText("");
    }
    dato = (String) hash.get("DS_OBSERV3");
    if (dato != null) {
      txtObserva3.setText(dato);
    }
    else {
      txtObserva3.setText("");
    }
    dato = (String) hash.get("DS_OBSERV2");
    if (dato != null) {
      txtObserva2.setText(dato);
    }
    else {
      txtObserva2.setText("");
    }

    // escribimos los choices antes
    int indice = Common.buscaDato(listaCA, "CD_CA", (String) hash.get("CD_CA2"));
    indice++;

    if (listaCA != null && indice >= 0 && indice <= listaCA.size()) {
      chCA2.select(indice);
    }
    /*
          //Si se ha encontrado la Com Aut el pa�s es Espa�a (LRG)
          if (indice > 0) {  // el pais es espa�a
              indice = comun.buscaDato ( listaPaises, "CD_PAIS", "ESP");
              indice++;
         if (listaPaises != null  && indice >= 0 && indice < listaPaises.size()){
                 chPais2.select(indice); // es el c�digo de Espa�a
              }
          }
     */

    //en la PROVINCIA a�adimos solo la correspondiente
    String des = (String) hash.get("DS_PROV2");
    String cod = (String) hash.get("CD_PROV2");
    if (des != null && cod != null) {
      chProvincia2.add(des);
      chProvincia2.select(1);
      listaProvincias2 = new CLista();
      DataEnfermo den = new DataEnfermo("CD_PROV");
      den.put("CD_PROV", cod);
      den.put("DS_PROV", des);
      listaProvincias2.addElement(den);
    }

    // los choice del 3
    indice = Common.buscaDato(listaCA, "CD_CA", (String) hash.get("CD_CA3"));
    indice++;
    if (listaCA != null && indice >= 0 && indice <= listaCA.size()) {
      chCA3.select(indice);
    }

    /*           LRG
          if (indice > 0) {  // el pais es espa�a
         indice = comun.buscaDato ( listaPaises, "CD_PAIS", "ESP"); //(LRG)
              indice++;
              if (listaCA != null  && indice >= 0 && indice < listaCA.size()){
                  chPais3.select(indice); // es el c�digo de Espa�a
              }
          }
     */

    //en la PROVINCIA a�adimos solo la correspondiente
    des = (String) hash.get("DS_PROV3");
    cod = (String) hash.get("CD_PROV3");
    if (des != null && cod != null) {
      chProvincia3.add(des);
      chProvincia3.select(1);
      listaProvincias3 = new CLista();
      DataEnfermo den = new DataEnfermo("CD_PROV");
      den.put("CD_PROV", cod);
      den.put("DS_PROV", des);
      listaProvincias3.addElement(den);
    }
  }

  //____________________________________________________________

  public void Inicializar() {
    switch (modo) {
      case modoINICIO: // es el mismo que el de modificar
      case modoMODIFICAR:

        //Si el pa�s2 es Espa�a se habilita la Choice de C Aut. (LRG)
//              if (chPais2.getSelectedItem().equalsIgnoreCase("espa�a")){
        chCA2.setEnabled(true);
//              }
        /*       LRG
                      //En caso contrario no se selecciona c Aut 2 ni provincia 2
                      else{
                         chCA2.select(0);
                         chProvincia2.select(0);
                      }
         */
        //Si hay provincia seleccionada (aunque sea de forma fijada por datos que llegan)
        //se habilita mujicipio (LRG)
        if (chProvincia2.getSelectedIndex() > 0) {
          chProvincia2.setEnabled(true);
          btnMunicipio2.setEnabled(true);
          txtMunicipio2.setEnabled(true);
        }
        //Si hay com aut seleccionada se habilita provincia
        else if (chCA2.getSelectedIndex() > 0) {
          chProvincia2.setEnabled(true);
        }
        //Si no hay com aut seleccionada tampoco hay provincia selecionada
        else {
          chProvincia2.select(0);
        }

        //Pa�s siempre selecionado
//              chPais2.setEnabled(true);       LRG

//              if (chPais3.getSelectedItem().equalsIgnoreCase("espa�a")){       LRG
        chCA3.setEnabled(true);
        /*        LRG
                      }
                      else{
                         chCA3.select(0);
                         chProvincia3.select(0);
                      }
         */
        if (chProvincia3.getSelectedIndex() > 0) {
          chProvincia3.setEnabled(true);
          btnMunicipio3.setEnabled(true);
          txtMunicipio3.setEnabled(true);
        }
        else if (chCA3.getSelectedIndex() > 0) {
          chProvincia3.setEnabled(true);
        }
        else {
          chProvincia3.select(0);
        }

//              chPais3.setEnabled(true);       LRG

        txtCalle2.setEnabled(true);
        txtNumero2.setEnabled(true);
        txtPiso2.setEnabled(true);
        txtCP2.setEnabled(true);
        txtCP3.setEnabled(true);
        txtPiso3.setEnabled(true);
        txtNumero3.setEnabled(true);
        txtCalle3.setEnabled(true);
        txtTelefono3.setEnabled(true);
        txtTelefono2.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtObserva2.setEnabled(true);
        txtObserva3.setEnabled(true);
        break;

      case modoESPERA:
        txtCalle2.setEnabled(false);
        txtNumero2.setEnabled(false);
        txtPiso2.setEnabled(false);
        txtCP2.setEnabled(false);
        txtMunicipio2.setEnabled(false);
        txtMunicipio2L.setEnabled(false);
        btnMunicipio2.setEnabled(false);
//              chPais3.setEnabled(false);       LRG
        txtCP3.setEnabled(false);
        txtPiso3.setEnabled(false);
        txtNumero3.setEnabled(false);
        txtMunicipio3.setEnabled(false);
        btnMunicipio3.setEnabled(false);
        txtCalle3.setEnabled(false);
        txtTelefono3.setEnabled(false);
        txtTelefono2.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        txtObserva2.setEnabled(false);
        txtObserva3.setEnabled(false);
//              chPais2.setEnabled(false);       LRG
        chCA2.setEnabled(false);
        chProvincia2.setEnabled(false);
        chCA3.setEnabled(false);
        chProvincia3.setEnabled(false);
        break;
    }

    this.doLayout();
  }

//_________________________________________________________________
//_________________________________________________________________

  /**
   *  esta clase recoge los eventos de pinchar en las lupas
   *  cuando se pincha a una lupa se llama a un servlet para pedir los datos
   *  se deshabilitan el resto de los botones lupa
   *  y se manda ejecutar la acci�n correspondiente
   */
// action listener de evento en bot�nes
  class DialOtrasDireccionesBtnActionListener
      implements ActionListener, Runnable {
    DialOtrasDirecciones adaptee = null;
    ActionEvent e = null;

    public DialOtrasDireccionesBtnActionListener(DialOtrasDirecciones adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void actionPerformed(ActionEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        // lanzamos el thread que tratar� el evento
        new Thread(this).start();
      }
    }

    // hilo de ejecuci�n para servir el evento
    public void run() {
      // deshabilita los escuchadores de cambios de c�digo
      String name = e.getActionCommand();
      String name2 = ( (Component) e.getSource()).getName();
      CLista lalista = null;
      String campo = null;

      Inicializar();

      /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
      if (name.equals("aceptar") || name2.equals("aceptar")) {
        if (listaEnfermo != null && listaEnfermo.size() > 0) {
          // creamos los nuevos datos
          // cogemos la hashtable con los datos del enfermo
          /// validamos los datos
          CLista tryLista = new CLista();
          tryLista.addElement(new DataEnfermo("CD_ENFERMO"));

          //M�todo isDataValid mete datos de pantalla en la lista provisional

          //Y Si no ha habido error en datos pantalla
          if (isDataValid(tryLista)) {

            //Si han cambiado los datos
            if (tryLista.size() > 0) {
              listaEnfermo = tryLista;
              pan_enfer.bCambioOtrasDirecciones = true; //LRG
              // System_out.println("DIALOTRASDIR    Se sustituye la lista de salida");
            }
            // si no han cambiado los datos
            else {

              //  listaEnfermo = null ;
              //LRG
              listaEnfermo = listaEnfermoBK; //LRG
              pan_enfer.bCambioOtrasDirecciones = false; //LRG
              // System_out.println("DIALOTRASDIR    No hay cambios: Lista de salida null ");
            }

            //________ TRAZA_______________________________________________

            // System_out.println("******************************************");
            // System_out.println("DIALOTRASDIR listaEnfermo Tras  isDataValid() :");
            if (listaEnfermo != null) {
              for (int cont = 0; cont < listaEnfermo.size(); cont++) {
                // System_out.println("    Elemento " + cont);
                Object elemLista = listaEnfermo.elementAt(cont);
                if ( (elemLista.getClass()) == (new DataEnfermo(" ").getClass())) {
                  Hashtable laHash = (Hashtable) elemLista;
                  Enumeration enum = laHash.keys();
                  while (enum.hasMoreElements()) {
                    String clave = (String) (enum.nextElement());
                    Object valor = null;
                    valor = laHash.get(clave);
                    if (valor.getClass() == (new String()).getClass()) {
                      // System_out.println("        Clave  "+clave+"                 " + (String)valor);
                    }
                    else {
                      // System_out.println("        Clave  "+clave+"   No es un String  ");
                    }

                  } //While
                } //if
              } //for
            }
            //________ FIN TRAZA_______________________________________________

            //Se cierra di�logo
            dispose();
          }
          //Si ha habido error no se cierra di�logo
          // y se recupera lista con datos anteriores
          //No hay que analizar si se han cambiado datos o no pus no salimos
          else {
            listaEnfermo = listaEnfermoBK;
            // System_out.println("    Datos no v�lidos: No se sale igual" );

          }
        }
      }
      else if (name.equals("cancelar") || name2.equals("cancelar")) {
        // nos vamos sin aceptar los cambios
        // le gravamos los datos pedidos
        listaEnfermo = listaEnfermoBK;
        pan_enfer.bCambioOtrasDirecciones = false; //LRG
        dispose();
      }
      else if (name.equals("municipio2") || name2.equals("municipio2")) {
        CListaEnfermedad lista = new CListaEnfermedad(adaptee.getCApp(),
            res.getString("msg17.Text"), adaptee.stubCliente,
            constantes.strSERVLET_ENFERMO,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletOBTENER_X_CODIGO,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletOBTENER_X_DESCRIPCION,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletSELECCION_X_CODIGO,
            SrvEnfermo.modoMUNICIPIO +
            SrvGeneral.servletSELECCION_X_DESCRIPCION,
                           "CD_MUN", "DS_MUN");
        // a�adimosla provincia para que filtre por provincias
        int indice = chProvincia2.getSelectedIndex();
        indice--; // le restamos uno por el campo vacio
        if (indice >= 0) {
          lalista = listaProvincias2;
          campo = (String) ( (Hashtable) lalista.elementAt(indice)).get(
              "CD_PROV");
        }
        if (campo != null && (campo.trim().length() > 0)) {
          lista.setParameter("CD_PROV", campo);
        }
        lista.buscarInicial();
        lista.show();
        DataEnfermo dato = (DataEnfermo) lista.getComponente();
        lista = null;
        if (dato != null) {
          txtMunicipio2.setText( (String) dato.get("CD_MUN"));
          txtMunicipio2L.setText( (String) dato.get("DS_MUN"));
        }
        modo = modoMODIFICAR;
      }
      else if (name.equals("municipio3") || name2.equals("municipio3")) {
        CListaEnfermedad lista = new CListaEnfermedad(adaptee.getCApp(),
            res.getString("msg17.Text"), adaptee.stubCliente,
            constantes.strSERVLET_ENFERMO,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletOBTENER_X_CODIGO,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletOBTENER_X_DESCRIPCION,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletSELECCION_X_CODIGO,
            SrvEnfermo.modoMUNICIPIO +
            SrvGeneral.servletSELECCION_X_DESCRIPCION,
                           "CD_MUN", "DS_MUN");
        // a�adimosla provincia para que filtre por provincias
        int indice = chProvincia3.getSelectedIndex();
        indice--; // le restamos uno por el campo vacio
        if (indice >= 0) {
          lalista = listaProvincias3;
          campo = (String) ( (Hashtable) lalista.elementAt(indice)).get(
              "CD_PROV");
        }
        if (campo != null && (campo.trim().length() > 0)) {
          lista.setParameter("CD_PROV", campo);
        }
        lista.buscarInicial();
        lista.show();
        DataEnfermo dato = (DataEnfermo) lista.getComponente();
        lista = null;
        if (dato != null) {
          txtMunicipio3.setText( (String) dato.get("CD_MUN"));
          txtMunicipio3L.setText( (String) dato.get("DS_MUN"));
        }
        modo = modoMODIFICAR;
      }
      else if (name.equals("vaciar") || name2.equals("vaciar")) {
//        vaciarPantalla();
        modo = modoMODIFICAR;
      }

      Inicializar();
      // desbloquea la recepci�n  de eventos
      adaptee.desbloquea();
    }
  }

  class DialOtrasDireccionesTextFocusListener
      implements java.awt.event.FocusListener, java.awt.event.ActionListener,
      Runnable {
    DialOtrasDirecciones adaptee;
    String name2 = null;
    protected StubSrvBD stub = new StubSrvBD();

    DialOtrasDireccionesTextFocusListener(DialOtrasDirecciones adaptee) {
      this.adaptee = adaptee;
    }

    public void focusGained(FocusEvent e) {
    }

    public void focusLost(FocusEvent e) {
      if (adaptee.bloquea()) {
        name2 = ( (Component) e.getSource()).getName();
        // lanzamos el thread que tratar� el evento
        new Thread(this).start();
      }
    }

    public void actionPerformed(ActionEvent e) {
      if (adaptee.bloquea()) {
        name2 = ( (Component) e.getSource()).getName();
        // lanzamos el thread que tratar� el evento
        new Thread(this).start();
      }
    }

    public void run() {
      CLista lalista = null;
      String campo = null;
      CLista result = null, data = new CLista();
      DataEnfermo denfer = null;
      try {

        if (name2.equals("municipio2")) { // muestra la pantalla de b�squeda
          adaptee.txtFocusMunicipio(2);
        }
        else if (name2.equals("municipio3")) {
          adaptee.txtFocusMunicipio(3);
        }
      }
      catch (Exception exc) {
        exc.printStackTrace();
      }
      adaptee.desbloquea();
    }

  } //_______________________________________________ END_CLASS

  /**
   *  Esta clase recoge el evento de modificar los valores del TextField
       *  cuando cambia el valor simplemente borra el TextField descriptivo asociado
   */
  class DialOtrasDireccionesTextAdapter
      extends java.awt.event.KeyAdapter {
    DialOtrasDirecciones adaptee;
    KeyEvent e = null;

    DialOtrasDireccionesTextAdapter(DialOtrasDirecciones adaptee) {
      this.adaptee = adaptee;
    }

    public void keyPressed(KeyEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        // lanzamos el thread que tratar� el evento
        //new Thread(this).start();
        run();
      }
    }

    public void run() {
      // quitamos los valores de los textfield de descripcion
      String name2 = ( (Component) e.getSource()).getName();

      if (name2.equals("municipio2")) {
        txtMunicipio2L.setText("");
      }
      else if (name2.equals("municipio3")) {
        txtMunicipio3L.setText("");
      }

      adaptee.desbloquea();
    }
  } //_________________________________________________ END CLASS BusEnfermoTextAdapter

// escuchador de los cambios en los itemListener  de los choices
  /**
   *  hay tres CListas que representan paises,comunidades y provincias
   *  cuando cambiamos el pais, se rellena la lista de comunidades
   *  y se vacia la lista de Provincias
   */
  class DialOtrasDireccionesChoiceItemListener
      implements java.awt.event.ItemListener, Runnable {
    DialOtrasDirecciones adaptee;
    ItemEvent e = null;
    int modoRun = 0;

    DialOtrasDireccionesChoiceItemListener(DialOtrasDirecciones adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void itemStateChanged(ItemEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        // lanzamos el thread que tratar� el evento
        //new Thread(this).start();
        run1();
      }
    }

    public void run() {
      CLista data = new CLista();
      CLista appList = null;
      int indice;
      modo = modoESPERA;
      Inicializar();
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      if (modoRun == 2) {
        if (listaCA != null) {
          indice = adaptee.chCA2.getSelectedIndex();
          indice = indice - 1;

        }
        else {
          indice = -1;
        }
        appList = null;
        data = new CLista();
        int modoLlamadaServlet;
        boolean bIdiomaPorDefecto = (adaptee.getCApp().getIdioma() ==
                                     adaptee.getCApp().idiomaPORDEFECTO);

        if (indice >= 0 && listaCA != null && indice < listaCA.size()) {
          //cargar provincias
          appList = listaCA;
          String codCA = (String) ( (datasuca) appList.elementAt(indice)).get(
              "CD_CA");

          listaProvincias2 = null;

          datasuca dat = new datasuca();
          dat.put("CD_CA", codCA);
          data.addElement(dat);

          // Distintas posibilidades de llamada
          modoLlamadaServlet = (bTramero ? srvsuca.modoPROVINCIAS :
                                srvsuca.modoPROVINCIAS_SIN_TRAMERO);
          if (!bTramero) {
            data.addElement(bIdiomaPorDefecto ? "S" : "N");
          }
          //********************************************************************

           listaProvincias2 = Common.traerDatos(adaptee.getCApp(), stubCliente,
                                                strSERVLET_SUCA,
                                                modoLlamadaServlet, data);

          if (chProvincia2.getItemCount() > 0) {
            chProvincia2.removeAll();

          }
          if (listaProvincias2.size() > 0) {
            chProvincia2.add(" ");
            Common.writeChoice(adaptee.getCApp(), chProvincia2,
                               listaProvincias2, false, "DS_PROV");
            if (listaProvincias2.size() == 1) {
              chProvincia2.select(1);
            }
          }
          // limpia el municipio
          txtMunicipio2.setText("");
          txtMunicipio2L.setText("");
        }
        //cargar provincias
        modo = modoMODIFICAR;

      }
      else if (modoRun == 3) {
        if (listaCA != null) {
          indice = adaptee.chCA3.getSelectedIndex();
          indice = indice - 1;
        }
        else {
          indice = -1;
        }
        appList = null;
        data = new CLista();
        int modoLlamadaServlet;
        boolean bIdiomaPorDefecto = (adaptee.getCApp().getIdioma() ==
                                     adaptee.getCApp().idiomaPORDEFECTO);

        if (indice >= 0 && listaCA != null && indice < listaCA.size()) {
          //cargar provincias
          appList = listaCA;
          String codCA = (String) ( (datasuca) appList.elementAt(indice)).get(
              "CD_CA");

          listaProvincias3 = null;

          datasuca dat = new datasuca();
          dat.put("CD_CA", codCA);
          data.addElement(dat);

          // Distintas posibilidades de llamada
          modoLlamadaServlet = (bTramero ? srvsuca.modoPROVINCIAS :
                                srvsuca.modoPROVINCIAS_SIN_TRAMERO);
          if (!bTramero) {
            data.addElement(bIdiomaPorDefecto ? "S" : "N");
          }
          //********************************************************************

           listaProvincias3 = Common.traerDatos(adaptee.getCApp(), stubCliente,
                                                strSERVLET_SUCA,
                                                modoLlamadaServlet, data);

          if (chProvincia3.getItemCount() > 0) {
            chProvincia3.removeAll();

          }
          if (listaProvincias3.size() > 0) {
            chProvincia3.add(" ");
            Common.writeChoice(adaptee.getCApp(), chProvincia3,
                               listaProvincias3, false, "DS_PROV");
            if (listaProvincias3.size() == 1) {
              chProvincia3.select(1);
            }
          }
          // limpia el municipio
          txtMunicipio3.setText("");
          txtMunicipio3L.setText("");
        }
        //cargar provincias
        modo = modoMODIFICAR;

      }
      adaptee.desbloquea();
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    // hilo de ejecuci�n para servir el evento
    public void run1() {
      // quitamos los valores de los textfield de descripcion
      String name2 = ( (Component) e.getSource()).getName();
      CLista data = new CLista();
      CLista appList = null;
      int indice;
//    String elpais=null;

      /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
      if (! ( (name2.equals("ccaa2") && adaptee.chCA2.getSelectedIndex() != 0) ||
             (name2.equals("ccaa3") && adaptee.chCA3.getSelectedIndex() != 0))) {
        // no se va a lanzar el thread
        Inicializar();
      }

      /*
          if (name2.equals("pais2")){
             // recogemos el valor seleccionado y le restamos uno del valor  nulo
             if (listaPaises != null) {
                 indice = chPais2.getSelectedIndex();
                 indice--;
                 elpais = chPais2.getSelectedItem();
             }else{
                 indice = -1;
             }
             // borramosla lista  y el choice de Provincias
             //adaptee.chcProvincia.removeAll();
             chProvincia2.select(0);
             chCA2.select(0);
             //borramos el municipio
             txtMunicipio2.setText("");
             txtMunicipio2L.setText("");
             modo = modoMODIFICAR;
             adaptee.desbloquea();
          }else  if (name2.equals("pais3")){
             // recogemos el valor seleccionado y le restamos uno del valor  nulo
             if (listaPaises != null) {
                 indice = chPais3.getSelectedIndex();
                 indice--;
                 elpais = chPais3.getSelectedItem();
             }else{
                 indice = -1;
             }
             // borramosla lista  y el choice de Provincias
             //adaptee.chcProvincia.removeAll();
             chProvincia3.select(0);
             chCA3.select(0);
             //borramos el municipio
             txtMunicipio3.setText("");
             txtMunicipio3L.setText("");
             modo = modoMODIFICAR;
             adaptee.desbloquea();
          }else
       */

      if (name2.equals("ccaa2")) {

        // recogemos el valor seleccionado y lew restamos uno delvalor nulo
        indice = adaptee.chCA2.getSelectedIndex();
        indice--;
        if (listaProvincias2 != null) {
          listaProvincias2 = null;
          adaptee.chProvincia2.removeAll();
          adaptee.chProvincia2.add("  ");
        }

        //borramos el municipio
        txtMunicipio2.setText("");
        txtMunicipio2L.setText("");

        // traemos los datos de las comunidades si es espa�a
        if (indice >= 0 && indice < listaCA.size()) {
          modoRun = 2;
          new Thread(this).start();
        }
        else {
          // eliminaos chProv
          if (listaProvincias2 != null) {
            adaptee.chProvincia2.removeAll();
            adaptee.chProvincia2.add("  ");
            listaProvincias2 = null;
          }
          modo = modoMODIFICAR;
          adaptee.desbloquea();
        }
      }
      else if (name2.equals("ccaa3")) {

        // recogemos el valor seleccionado y lew restamos uno delvalor nulo
        indice = adaptee.chCA3.getSelectedIndex();
        indice--;
        if (listaProvincias3 != null) {
          listaProvincias3 = null;
          adaptee.chProvincia3.removeAll();
          adaptee.chProvincia3.add("  ");
        }

        //borramos el municipio
        txtMunicipio3.setText("");
        txtMunicipio3L.setText("");

        // traemos los datos de las comunidades si es espa�a
        if (indice >= 0 && indice < listaCA.size()) {
          modoRun = 3;
          new Thread(this).start();
        }
        else {
          // eliminaos chProv
          if (listaProvincias3 != null) {
            adaptee.chProvincia3.removeAll();
            adaptee.chProvincia3.add("  ");
            listaProvincias3 = null;
          }
          modo = modoMODIFICAR;
          adaptee.desbloquea();
        }
      }
      else if (name2.equals("provincia2")) {
        // activamos municipio o no
        indice = chProvincia2.getSelectedIndex();
        indice--;
        //borramos el municipio
        txtMunicipio2.setText("");
        txtMunicipio2L.setText("");

        modo = modoMODIFICAR;
        adaptee.desbloquea();
      }
      else if (name2.equals("provincia3")) {
        // activamos municipio o no
        indice = chProvincia3.getSelectedIndex();
        indice--;
        //borramos el municipio
        txtMunicipio3.setText("");
        txtMunicipio3L.setText("");

        modo = modoMODIFICAR;
        adaptee.desbloquea();
      }

      Inicializar();
      // desbloquea la recepci�n  de eventos
      adaptee.desbloquea();

    }

  } //________________________________________________ END CLASS BusEnfermoTableAdapter

} ///___________________________________________________ END_CLASS
