package infprotoedo;

import java.util.Vector;

public class DataListaValores {
  protected String CodLista = "";
  protected String Cod = ""; //V
  protected String Des = ""; //varon
  protected Vector VALORES = null;

  public DataListaValores(String codlista, String cod, String des) {

    CodLista = codlista;
    Cod = cod;
    Des = des;
  } //fin construct

  public DataListaValores(String codlista, Vector valores) {

    CodLista = codlista;
    VALORES = valores;

  } //fin construct

  public String getCodLista() {
    return CodLista;
  }

  public String getCod() {
    return Cod;
  }

  public String getDes() {
    return Des;
  }

  public Vector getVALORES() {
    return VALORES;
  }

}
