package infprotoedo;

import java.io.Serializable;

//Estructura para la consulta a SIVE_RESP_EDO

public class DataResp
    implements Serializable {
  protected String Caso = "";
  protected String CodModelo = "";
  protected String Num = "";
  protected String CodPregunta = "";
  protected String Des = ""; //V
  protected String ValorLista = ""; //varon

  public DataResp(String caso, String codM, String num,
                  String pregunta, String des, String valorlista) {

    Caso = caso;
    CodModelo = codM;
    Num = num;
    CodPregunta = pregunta;
    Des = des;
    ValorLista = valorlista;

  } //fin construct

  public DataResp(String caso) {

    Caso = caso;
    CodModelo = null;
    Num = null;
    CodPregunta = null;
    Des = null;
    ValorLista = null;

  } //fin construct

  public String getCodModelo() {
    return CodModelo;
  }

  public String getCaso() {
    return Caso;
  }

  public String getNumLinea() {
    return Num;
  }

  public String getCodPregunta() {
    return CodPregunta;
  }

  public String getDesPregunta() {
    return Des;
  }

  public String getValorLista() {
    return ValorLista;
  }

}
