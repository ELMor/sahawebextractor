package infprotoedo;

import java.util.Vector;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;

import capp.CApp;
import capp.CLista;

/*Si se pasa como obligatorio, se pintara amarilo*/
public class CChoice
    extends Choice {

  //datos de entrada
  public String sOblig;
  public String sCodLista;
  public Vector vValores;
  public String sCondicionada;
  public String sCodM;
  public String sNum;
  public String sNumC;
  public String sDesC;
  public String sPregunta;

  protected boolean bCboCargada = false;

  protected Vector vCbo = new Vector(); //para los codigos de las listas

  protected CApp app = null;

  //constructor
  public CChoice(CApp a, String sOBLIGATORIO,
                 String sCDLISTA,
                 String sCONDICIONADA, String sCD_MODELO,
                 String sNM_LIN,
                 String sNM_LIN_COND, String sDS_PREG_COND,
                 String sCD_PREGUNTA, Vector vVALORES) {
    super();

    //parametros de entrada
    app = a;
    sOblig = sOBLIGATORIO;
    sCodLista = sCDLISTA;
    sCondicionada = sCONDICIONADA;
    sCodM = sCD_MODELO;
    sNum = sNM_LIN;
    sNumC = sNM_LIN_COND;
    sDesC = sDS_PREG_COND;
    sPregunta = sCD_PREGUNTA;
    vValores = vVALORES;

    //Color
    if (sOblig.equals("S")) {
      setBackground(new Color(255, 255, 150));

    }
    try {
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  } //end Constructor

//jbInit*****************************************
  private void jbInit() throws Exception {

    this.addItem(""); //uno vacio!!!!!!!!!
    vCbo.addElement("");

    Cargar_Choice();
  } //fin jbnit

  public String getCodLista() {
    return (sCodLista);
  }

  public Vector getVectorCodLista() {
    return (vCbo);
  }

  public String getObligatorio() {
    return (sOblig);
  }

  public String getCondicionada() {
    return (sCondicionada);
  }

  public String getCodM() {
    return (sCodM);
  }

  public String getNum() {
    return (sNum);
  }

  public String getNumC() {
    return (sNumC);
  }

  public String getDesC() {
    return (sDesC);
  }

  public String getCodPregunta() {
    return (sPregunta);
  }

  public Vector getvValores() {
    return (vValores);
  }

//recuperar si la choice esta cargada o no
  public String getCboCargada() {
    if (bCboCargada) {
      return ("S");
    }
    else {
      return ("N");
    }
  }

  protected void Cargar_Choice() {
    try {
      //por si acaso, la vacio
      if (this.getItemCount() > 1) {
        //vaciarla
        //vaciar el vector
        this.removeAll();
        vCbo.removeAllElements();

        //comenzar again
        this.addItem(""); //uno vacio!!!!!!!!!
        vCbo.addElement("");
      }

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      //BOOLEAN !!!!
      //!!!!!!!!!!!!
      if (sCodLista == null) {
        CLista lista = new CLista();
        lista.addElement(new DataValores("S", "Si"));
        lista.addElement(new DataValores("N", "No"));
        for (int indice = 0; indice < lista.size(); indice++) {
          DataValores datosSalida = (DataValores) lista.elementAt(indice);
          //a�adir los valores a la choice
          this.addItem(datosSalida.getDes());
          vCbo.addElement(datosSalida.getCod());
        }
      }
      //LISTAS!!!!!
      //!!!!!!!!!!!
      else {

        for (int indice = 0; indice < vValores.size(); indice++) {

          DataValores datosSalida = (DataValores) vValores.elementAt(indice);
          //a�adir los valores a la choice
          this.addItem(datosSalida.getDes());
          vCbo.addElement(datosSalida.getCod());
        }
      }

      bCboCargada = true;
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      this.doLayout();
      vCbo.trimToSize();

    }
    catch (Exception excep) {
      excep.printStackTrace();
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

  } //fin de cargarChoice

//Se llama desde modificacion, cuando se quiere volcar un dato
//en la choice
  public void CargarVector(String descripcion, String valor) {

    //BOOLEAN!!!!
    if (descripcion == null) {
      if (valor.equals("S")) {
        this.select("Si");
      }
      else {
        this.select("No");
      }
    }
    //LISTAS!!!!
    else {
      this.select(descripcion);
    }

  } //fin cargarVector
} //endclass
