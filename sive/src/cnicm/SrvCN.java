package cnicm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
//import eqNot.*;
import comun.DataEqNot;
import comun.DataNotifSem;
import sapp.DBServlet;

public class SrvCN
    extends DBServlet {

  //Modos de operaci�n del Servlet
  public static final int servletALTA = 0;
  public static final int servletMODIFICAR = 1;
  public static final int servletBAJA = 2;
  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;
  public static final int servletSELECCION_NIVASIS_X_CODIGO = 7;
  public static final int servletOBTENER_NIVASIS_X_CODIGO = 8;
  public static final int servletSELECCION_NIVASIS_X_DESCRIPCION = 9;
  public static final int servletOBTENER_NIVASIS_X_DESCRIPCION = 10;
  public static final int servletGENERA_COBERTURA = 11;
  public static final int servletOBTENER_X_TODO_CODIGO = 12;
  public static final int servletOBTENER_X_TODO_DESCRIPCION = 13;
  public static final int servletSELECCION_X_TODO_CODIGO = 14;
  public static final int servletSELECCION_X_TODO_DESCRIPCION = 15;

  // objetos JDBC
  protected Connection con = null;
  protected PreparedStatement st = null;
  protected String query = null;
  protected ResultSet rs = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    /* QQ: An�lisis de tiempos: */

    long lInicio = 0;
    long lFin = 0;
    long lIniConexion = 0;
    long lFinConexion = 0;
    long lIniSelect = 0;
    long lFinSelect = 0;
    long lIniProceso = 0;
    long lFinProceso = 0;

    int i = 1;
    boolean fisico = false;

    // objetos de datos
    CLista data = new CLista();
    CLista listaSemana = new CLista();
    CLista listaEquipos = new CLista();
    CLista listaMixta = new CLista();

    DataNotifSem datosSemana = null;
    DataNotifSem datosEquipos = null;

    DataCN datos = null;
    DataTA datosTA = null;
    DataCN datosCentroAuxi = null;
    DataEqNot datosEquipoAuxi = null;
    DataNotifSem datosMixtosAuxi = null;

    String semHasta = null;
    String anoHasta = null;
    String semDesde = null;
    String anoDesde = null;

    //Para elegir descripci�n
    String sDesNivAsis = "";
    String sDesNivAsisL = "";

    // establece la conexi�n con la base de datos
    lIniConexion = System.currentTimeMillis();
    con = openConnection();
    lFinConexion = System.currentTimeMillis();

    con.setAutoCommit(false);

    datos = (DataCN) param.firstElement();

    lIniProceso = System.currentTimeMillis(); //^^^^^^^^^^^^^^^^

    try {
      // modos de operaci�n
      switch (opmode) {

        // alta
        case servletALTA:

          // prepara la query
          query = "insert into SIVE_C_NOTIF " +
              "(CD_CENTRO,CD_PROV,CD_MUN,DS_CENTRO,DS_DIREC,DS_NUM,DS_PISO,CD_POSTAL,DS_TELEF,DS_FAX,CD_NIVASIS" +
              ",IT_COBERTURA,FC_ALTA,CD_OPE,FC_ULTACT,IT_BAJA)" +
              "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
          st = con.prepareStatement(query);
          // codigo
          //# // System_out.println(datos.getCodCentro().trim());
          st.setString(1, datos.getCodCentro().trim());
          // provincia
          //# // System_out.println(datos.getProv().trim());
          st.setString(2, datos.getProv().trim());
          // municipio
          //# // System_out.println(datos.getMun().trim());
          st.setString(3, datos.getMun().trim());
          // descripci�n del centro
          //# // System_out.println(datos.getCentroDesc().trim());
          st.setString(4, datos.getCentroDesc().trim());
          // direcci�n del centro
          //# // System_out.println(datos.getDireccion().trim());
          st.setString(5, datos.getDireccion().trim());
          // n�mero
          //# // System_out.println(datos.getNum().trim());
          st.setString(6, datos.getNum().trim());
          // piso
          //# // System_out.println(datos.getPiso().trim());
          st.setString(7, datos.getPiso().trim());
          // c�digo postal
          //# // System_out.println(datos.getCPostal().trim());
          st.setString(8, datos.getCPostal().trim());
          // tel�fono
          //# // System_out.println(datos.getTelefono().trim());
          st.setString(9, datos.getTelefono().trim());
          // fax
          //# // System_out.println(datos.getFax().trim());
          st.setString(10, datos.getFax().trim());
          // nivel asistencial
          //# // System_out.println(datos.getNivAsis().trim());
          st.setString(11, datos.getNivAsis().trim());
          // cobertura
          //# // System_out.println(datos.getCobertura().trim());
          st.setString(12, datos.getCobertura().trim());
          // fecha de alta
          //# // System_out.println("Date alta");
          st.setDate(13, (new java.sql.Date( (new java.util.Date()).getTime())));
          // c�digo del operador
          //# // System_out.println(datos.getCdOperador().trim());
          st.setString(14, datos.getCdOperador().trim());
          // fecha de la �ltima actualizaci�n
          //# // System_out.println("Date ult. act.");
          st.setDate(15, (new java.sql.Date( (new java.util.Date()).getTime())));
          // �es baja?
          //# // System_out.println(datos.getBaja().trim());
          st.setString(16, datos.getBaja().trim());
          // lanza la query
          //# // System_out.println("Lanzamos----------------->");
          st.executeUpdate();
          //# // System_out.println("<-------lanzada");
          st.close();
          //# // System_out.println("fin");
          data = new CLista();
          data = null;
          break;

          // baja:  Aqu� s�lo se borra f�sicamente
          //        si existe equipo notificador
        case servletBAJA: //CREO SOBRA ESTE MODO ^^^^^^^^^^^^^^

          // prepara la query
          query = "DELETE FROM  WHERE CD_CENTRO = ?";

          // lanza la query
          st = con.prepareStatement(query);
          st.setString(1, datos.getCodCentro().trim());
          st.executeUpdate();
          st.close();

          break;

          // b�squeda
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:
        case servletSELECCION_X_TODO_CODIGO:
        case servletSELECCION_X_TODO_DESCRIPCION:

          // prepara la query
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_X_CODIGO ||
                opmode == servletSELECCION_X_TODO_CODIGO) {
              query = "select * from SIVE_C_NOTIF where CD_CENTRO like ?" +
                  " and CD_PROV like ?  and CD_MUN like ? and DS_CENTRO like ? and CD_CENTRO > ? order by CD_CENTRO";
            }
            else {
              query = "select * from SIVE_C_NOTIF where DS_CENTRO like ?" +
                  "and CD_PROV like ? and CD_MUN like ? and DS_CENTRO like ? and CD_CENTRO > ? order by CD_CENTRO";
            }
          }
          else {
            if (opmode == servletSELECCION_X_CODIGO ||
                opmode == servletSELECCION_X_TODO_CODIGO) {
              query = "select * from SIVE_C_NOTIF where CD_CENTRO like ? " +
                  "and CD_PROV like ? and CD_MUN like ? and DS_CENTRO like ? order by CD_CENTRO";
            }
            else {
              query = "select * from SIVE_C_NOTIF where DS_CENTRO like ? " +
                  "and CD_PROV like ? and CD_MUN like ? and DS_CENTRO like ? order by CD_CENTRO";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          st.setString(1, datos.getCodCentro().trim() + "%");
          // provincia
          st.setString(2, datos.getProv().trim() + "%");
          // municipio
          st.setString(3, datos.getMun().trim() + "%");

          // descripci�n del centro
          st.setString(4, datos.getCentroDesc().trim() + "%");

          // paginaci�n
          if (param.getFilter().length() > 0) {
            st.setString(5, param.getFilter().trim());
          }
          rs = st.executeQuery();
          //# // System_out.println(")))))))))))))))))))))))XXXXXXXXXXX(((((((((((((");
          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              data.setFilter( ( (DataCN) data.lastElement()).getCodCentro());
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo rs.getString("")
            data.addElement(new DataCN(rs.getString("CD_CENTRO"),
                                       rs.getString("CD_PROV"),
                                       rs.getString("CD_MUN"),
                                       rs.getString("DS_CENTRO"),
                                       rs.getString("DS_DIREC"),
                                       rs.getString("DS_NUM"),
                                       rs.getString("DS_PISO"),
                                       rs.getString("CD_POSTAL"),
                                       rs.getString("DS_TELEF"),
                                       rs.getString("DS_FAX"),
                                       rs.getString("CD_NIVASIS"),
                                       rs.getString("IT_COBERTURA"),
                                       rs.getDate("FC_ALTA").toString(),
                                       rs.getString("CD_OPE"),
                                       rs.getString("IT_BAJA"), "", ""));
            i++;
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          if (opmode == servletSELECCION_X_TODO_CODIGO ||
              opmode == servletSELECCION_X_TODO_DESCRIPCION) {
            rellena_descripciones(data);
          }

          break;

          // obtenci�n
        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:
        case servletOBTENER_X_TODO_CODIGO:
        case servletOBTENER_X_TODO_DESCRIPCION:

          // prepara la query
          if (opmode == servletOBTENER_X_CODIGO ||
              opmode == servletOBTENER_X_TODO_CODIGO) {
            query = "select * from SIVE_C_NOTIF where CD_CENTRO = ? " +
                "and CD_PROV like ? and CD_MUN like ?  order by CD_CENTRO";
          }
          else {
            query = "select * from SIVE_C_NOTIF where DS_CENTRO = ? " +
                "and CD_PROV like ? and CD_MUN like ? order by CD_CENTRO";
            // prepara la lista de resultados
          }
          data = new CLista();
          //# // System_out.println("CN "+ datos.getCodCentro().trim()+ datos.getProv().trim()+ datos.getMun().trim() + query);
          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          st.setString(1, datos.getCodCentro().trim());
          // provincia
          st.setString(2, datos.getProv().trim() + "%");
          // municipio
          st.setString(3, datos.getMun().trim() + "%");

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              data.setFilter( ( (DataCN) data.lastElement()).getCodCentro());
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo rs.getString("")
            data.addElement(new DataCN(rs.getString("CD_CENTRO"),
                                       rs.getString("CD_PROV"),
                                       rs.getString("CD_MUN"),
                                       rs.getString("DS_CENTRO"),
                                       rs.getString("DS_DIREC"),
                                       rs.getString("DS_NUM"),
                                       rs.getString("DS_PISO"),
                                       rs.getString("CD_POSTAL"),
                                       rs.getString("DS_TELEF"),
                                       rs.getString("DS_FAX"),
                                       rs.getString("CD_NIVASIS"),
                                       rs.getString("IT_COBERTURA"),
                                       rs.getDate("FC_ALTA").toString(),
                                       rs.getString("CD_OPE"),
                                       rs.getString("IT_BAJA"), "", ""));

            /// a�adimos las descripciones

            i++;
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          if (opmode == servletOBTENER_X_TODO_CODIGO ||
              opmode == servletOBTENER_X_TODO_DESCRIPCION) {
            rellena_descripciones(data);
          }
          break;

          // modificaci�n
        case servletMODIFICAR:

          fisico = false;
          query =
              "select * from sive_semgen order by cd_anoepig desc, cd_semepig desc";
          // lanza la query
          st = con.prepareStatement(query);
          rs = st.executeQuery();
          if (rs.next()) {
            anoHasta = rs.getString("cd_anoepig");
            semHasta = rs.getString("cd_semepig");
            rs.close();
            st.close();
            rs = null;
            st = null;
          }
          else {
            rs.close();
            st.close();
            rs = null;
            st = null;
            query =
                "select * from sive_semana_epi order by cd_anoepi desc, cd_semepi desc";
            // lanza la query
            st = con.prepareStatement(query);
            rs = st.executeQuery();

            anoHasta = rs.getString("cd_anoepi");
            semHasta = rs.getString("cd_semepi");
            rs.close();
            st.close();
            rs = null;
            st = null;
          }

          if (datos.getBaja().compareTo("S") == 0) {

            // Se ve si centro tiene equipos o no

            query = "select * from SIVE_E_NOTIF WHERE CD_CENTRO=?";

            // lanza la query
            st = con.prepareStatement(query);
            st.setString(1, datos.getCodCentro().trim());
            rs = st.executeQuery();
            if (!rs.next()) {
              fisico = true;
            }
            //# // System_out.println("Despu�s");
            rs.close();
            st.close();
            rs = null;
            st = null;

            //# // System_out.println("SI����������������������������");
            if (fisico) {
              // como no hay equipos notificadores
              // haremos un borrado f�sico
              // prepara la query
              query = "DELETE FROM SIVE_C_NOTIF WHERE CD_CENTRO = ?";

              // lanza la query
              st = con.prepareStatement(query);
              st.setString(1, datos.getCodCentro().trim());
              st.executeUpdate();
              st.close();

            }
            else {
              // Tiene equipo notificador y hay que poner marca de baja en centro
              //y marca de baja en sus equipos

              // prepara la query
              //# // System_out.println("UPDATE����������������������������");
              query =
                  "UPDATE  SIVE_C_NOTIF SET CD_PROV=?, CD_MUN=?, DS_CENTRO=?, " +
                  "DS_DIREC=?, DS_NUM=?, DS_PISO=?, CD_POSTAL=?, DS_TELEF=?, DS_FAX=?, " +
                  "CD_NIVASIS=?, IT_COBERTURA=?, CD_OPE=?, FC_ULTACT=?, IT_BAJA=? WHERE CD_CENTRO=?";

              // lanza la query
              st = con.prepareStatement(query);
              // provincia
              st.setString(1, datos.getProv().trim());
              // municipio
              st.setString(2, datos.getMun().trim());
              // descripci�n centro
              st.setString(3, datos.getCentroDesc().trim());
              // direcci�n
              st.setString(4, datos.getDireccion().trim());
              // n�mero
              st.setString(5, datos.getNum().trim());
              // piso
              st.setString(6, datos.getPiso().trim());
              // c�digo postal
              st.setString(7, datos.getCPostal().trim());
              // tel�fono
              st.setString(8, datos.getTelefono().trim());
              // fax
              st.setString(9, datos.getFax().trim());
              // nivel asistencial
              st.setString(10, datos.getNivAsis().trim());
              // cobertura
              st.setString(11, datos.getCobertura().trim());
              // c�digo del operador
              st.setString(12, datos.getCdOperador().trim());
              // fecha de la �ltima actualizaci�n
              st.setDate(13, (new java.sql.Date( (new java.util.Date()).getTime())));
              // �Es baja?
              st.setString(14, datos.getBaja().trim());
              // c�digo del centro
              st.setString(15, datos.getCodCentro().trim());

              st.executeUpdate();
              st.close();
              //# // System_out.println("DELETE����������������������������");

              //__________________________
              // marca de baja en equipos

              query = "UPDATE  SIVE_E_NOTIF SET IT_BAJA=? WHERE CD_CENTRO=?";

              // lanza la query
              st = con.prepareStatement(query);
              // �Es baja?
              st.setString(1, datos.getBaja().trim());
              // c�digo del centro
              st.setString(2, datos.getCodCentro().trim());

              st.executeUpdate();
              st.close();

            }
          }

          else {
            // no es baja
            // prepara la query
            query =
                "UPDATE  SIVE_C_NOTIF SET CD_PROV=?, CD_MUN=?, DS_CENTRO=?, " +
                "DS_DIREC=?, DS_NUM=?, DS_PISO=?, CD_POSTAL=?, DS_TELEF=?, DS_FAX=?, " +
                "CD_NIVASIS=?, IT_COBERTURA=?, CD_OPE=?, FC_ULTACT=?, IT_BAJA=? WHERE CD_CENTRO=?";

            // lanza la query
            st = con.prepareStatement(query);
            // provincia
            st.setString(1, datos.getProv().trim());
            // municipio
            st.setString(2, datos.getMun().trim());
            // descripci�n centro
            st.setString(3, datos.getCentroDesc().trim());
            // direcci�n
            st.setString(4, datos.getDireccion().trim());
            // n�mero
            st.setString(5, datos.getNum().trim());
            // piso
            st.setString(6, datos.getPiso().trim());
            // c�digo postal
            st.setString(7, datos.getCPostal().trim());
            // tel�fono
            st.setString(8, datos.getTelefono().trim());
            // fax
            st.setString(9, datos.getFax().trim());
            // nivel asistencial
            st.setString(10, datos.getNivAsis().trim());
            // cobertura
            st.setString(11, datos.getCobertura().trim());
            // c�digo del operador
            st.setString(12, datos.getCdOperador().trim());
            // fecha de la �ltima actualizaci�n
            st.setDate(13, (new java.sql.Date( (new java.util.Date()).getTime())));
            // �Es baja?
            st.setString(14, datos.getBaja().trim());
            // c�digo del centro
            st.setString(15, datos.getCodCentro().trim());

            st.executeUpdate();
            st.close();

            st = null;
            rs = null;

            //# // System_out.println("�Cobertura?����������������������������");

            // COBERTURA
            if (!datos.getAnDesde().equals("")) {

              // incluido en cobertura
              if (datos.getCobertura().compareTo("S") == 0) {
                // primero se buscan todas las semanas del rango
                if (datos.getAnDesde().compareTo(anoHasta) == 0) {
                  query = "select * from sive_semana_epi where " +
                      "cd_anoepi=? and cd_semepi>=? and cd_semepi<=?";
                  // lanza la query
                  st = con.prepareStatement(query);
                  // a�o inicio
                  st.setString(1, datos.getAnDesde().trim());
                  // semana desde
                  st.setString(2, datos.getSemDesde().trim());
                  // semana hasta
                  st.setString(3, semHasta);
                }
                else {
                  query = "select * from sive_semana_epi " +
                      "where (cd_anoepi = ? and cd_semepi >= ?) or " +
                      "(cd_anoepi > ? and cd_anoepi < ?) or " +
                      "(cd_anoepi = ? and cd_semepi <= ?)";

                  // lanza la query
                  st = con.prepareStatement(query);

                  // a�o inicio
                  st.setString(1, datos.getAnDesde().trim());
                  st.setString(3, datos.getAnDesde().trim());
                  // a�o hasta
                  st.setString(4, anoHasta);
                  st.setString(5, anoHasta);
                  // semana desde
                  st.setString(2, datos.getSemDesde().trim());
                  // semana hasta
                  st.setString(6, semHasta);
                }
                //# // System_out.println("Lanzamos������������");
                rs = st.executeQuery();
                i = 0;
                //# // System_out.println("YA EST�������������");
                listaSemana = new CLista();

                while (rs.next()) {
                  // a�ade un nodo
                  listaSemana.addElement(new DataCN("", "", "", "", "", "", "",
                      "", "", "", "",
                      "", "", "", "", rs.getString("CD_ANOEPI"),
                      rs.getString("CD_SEMEPI")));
                  i++;
                  //# // System_out.println("Un dato������������");
                }
                rs.close();
                st.close();

                rs = null;
                st = null;

                // ahora los buscamos todos los equipos
                //# // System_out.println("Seleccionamos equipos ����������������������������");
                query = "select * from sive_e_notif " +
                    "where cd_centro=? ";

                // lanza la query
                st = con.prepareStatement(query);

                // c�digo del centro
                st.setString(1, datos.getCodCentro().trim());

                rs = st.executeQuery();
                //# // System_out.println("query ejecutada");
                i = 0;
                listaEquipos = new CLista();

                while (rs.next()) {
                  // a�ade un nodo
                  listaEquipos.addElement(new DataEqNot(rs.getString(
                      "CD_E_NOTIF"), "", "", "", "", "", "", "", "", "", "", 0,
                      "", "", false));
                  i++;
                }
                rs.close();
                st.close();

                // ahora combinamos las semanas con los equipos
                listaMixta = new CLista();

                if ( (listaEquipos != null) && (listaSemana != null)) {
                  //# // System_out.println("Combinamos ����������������������������");
                  for (int j = 0; j < listaEquipos.size(); j++) {
                    datosEquipoAuxi = (DataEqNot) listaEquipos.elementAt(j);
                    for (int k = 0; k < listaSemana.size(); k++) {
                      datosCentroAuxi = (DataCN) listaSemana.elementAt(k);
                      datosMixtosAuxi = new DataNotifSem(0, 0,
                          datos.getCdOperador(), datosEquipoAuxi.getEquipo(),
                          datosCentroAuxi.getSemDesde(),
                          datosCentroAuxi.getAnDesde());
                      listaMixta.addElement(datosMixtosAuxi);
                    }
                  }
                }
                // Ahora a�adimos todos los equipos que no tienen datos
                if (listaMixta != null) {
                  listaSemana = null;
                  listaSemana = new CLista();
                  //# // System_out.println("A�adimos los no existentes ����������������������������");
                  for (int j = 0; j < listaMixta.size(); j++) {
                    rs = null;
                    st = null;

                    datosMixtosAuxi = (DataNotifSem) listaMixta.elementAt(j);

                    query =
                        "select * from sive_semana_epi a, sive_notif_sem b " +
                        "where a.cd_semepi=b.cd_semepi and a.cd_anoepi=b.cd_anoepi " +
                        "and b.cd_e_notif =? and b.cd_semepi=? and b.cd_anoepi=?";

                    st = con.prepareStatement(query);

                    // c�digo del equipo
                    st.setString(1, datosMixtosAuxi.getCdEquipo().trim());
                    // c�digo de la semana
                    st.setString(2, datosMixtosAuxi.getSemana().trim());
                    // c�digo del a�o
                    st.setString(3, datosMixtosAuxi.getAnyo().trim());

                    rs = st.executeQuery();

                    if (!rs.next()) {
                      // a�ade un nodo
                      listaSemana.addElement(new DataNotifSem(0, 0,
                          datos.getCdOperador(),
                          datosMixtosAuxi.getCdEquipo().trim(),
                          datosMixtosAuxi.getSemana().trim(),
                          datosMixtosAuxi.getAnyo().trim()));
                    }
                    rs.close();
                    st.close();

                  }
                  // ahora a�adimos los registros definitivos
                  for (int j = 0; j < listaSemana.size(); j++) {
                    st = null;
                    //# // System_out.println("inser. definitivos ����������������������������");
                    datosMixtosAuxi = (DataNotifSem) listaSemana.elementAt(j);

                    query =
                        "insert into sive_notif_sem (cd_e_notif,cd_anoepi,cd_semepi,nm_nnotift" +
                        ",nm_ntotreal,cd_ope,fc_ultact) values (?,?,?,?,?,?,?)";

                    st = con.prepareStatement(query);

                    // c�digo del equipo
                    st.setString(1, datosMixtosAuxi.getCdEquipo().trim());
                    // c�digo del a�o
                    st.setString(2, datosMixtosAuxi.getAnyo().trim());
                    // c�digo de la semana
                    st.setString(3, datosMixtosAuxi.getSemana().trim());
                    //   notificaciones totales
                    st.setInt(4, 0);
                    // notificaciones reales
                    st.setInt(5, 0);
                    // c�digo del operador
                    st.setString(6, datos.getCdOperador().trim());
                    // fecha de actualizaci�n
                    st.setDate(7,
                               (new java.sql.Date( (new java.util.Date()).getTime())));

                    st.executeUpdate();

                    st.close();

                  }
                }

                // desincluido de la cobertura
              }
              else {

                // Ahora se eliminan los equipos de la cobertura
                query = "DELETE from sive_notif_sem where " +
                    "(nm_ntotreal=0 or nm_ntotreal is null) and ( (cd_anoepi=? and cd_semepi>=?) or (cd_anoepi>? and cd_anoepi<?) or (cd_anoepi=? and cd_semepi>=?)) and cd_e_notif in " +
                    "(select cd_e_notif from sive_e_notif where cd_centro=?)";

                // lanza la query
                st = con.prepareStatement(query);

                // periodo de semanas del a�o actual
                st.setString(1, anoHasta);
                st.setString(2, datos.getSemDesde());
                // periodo de semanas entre a�o selccionado-a�o actual
                st.setString(3, datos.getAnDesde());
                st.setString(4, anoHasta);
                // periodo de semanas del a�o selccionado
                st.setString(5, datos.getAnDesde());
                st.setString(6, datos.getSemDesde());
                // centro
                st.setString(7, datos.getCodCentro().trim());

                st.executeUpdate();
                st.close();
              }
            } // fin cobertura
          }
          break;

        case servletSELECCION_NIVASIS_X_CODIGO:
        case servletSELECCION_NIVASIS_X_DESCRIPCION:

          // prepara la query
          if (opmode == servletSELECCION_NIVASIS_X_CODIGO) {
            query = "select * from SIVE_NIV_ASIST where (CD_NIVASIS like ?)";
          }
          else {
            query = "select * from SIVE_NIV_ASIST where (DS_NIVASIS like ?)";
          }
          st = con.prepareStatement(query);

          // prepara la lista de resultados
          data = new CLista();
          st.setString(1, datos.getCodCentro() + "%");
          st.executeQuery();
          rs = st.getResultSet();
          // extrae la p�gina requerida
          i = 0;
          while (rs.next()) {

            //_______________________

            sDesNivAsis = rs.getString("DS_NIVASIS");

            // obtiene la descripcion auxiliar en funci�n del idioma
            if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
              sDesNivAsisL = rs.getString("DSL_NIVASIS");
              if (sDesNivAsisL != null) {
                sDesNivAsis = sDesNivAsisL;
              }
            }

            // a�ade un nodo
            data.addElement(new DataTA(rs.getString("CD_NIVASIS"),
                                       sDesNivAsis.trim()));
            //_______________________

            i++;
          }

          rs.close();
          st.close();

          break;

        case servletOBTENER_NIVASIS_X_CODIGO:
        case servletOBTENER_NIVASIS_X_DESCRIPCION:

          // prepara la query
          if (opmode == servletOBTENER_NIVASIS_X_CODIGO) {
            query = "select * from SIVE_NIV_ASIST where (CD_NIVASIS = ?)";
          }
          else {
            query = "select * from SIVE_NIV_ASIST where (DS_NIVASIS = ?)";

          }
          st = con.prepareStatement(query);

          // prepara la lista de resultados
          data = new CLista();
          st.setString(1, datos.getCodCentro());
          st.executeQuery();
          rs = st.getResultSet();
          // extrae la p�gina requerida
          i = 0;
          while (rs.next()) {

            //_______________________

            sDesNivAsis = rs.getString("DS_NIVASIS");

            // obtiene la descripcion auxiliar en funci�n del idioma
            if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
              sDesNivAsisL = rs.getString("DSL_NIVASIS");
              if (sDesNivAsisL != null) {
                sDesNivAsis = sDesNivAsisL;
              }
            }

            // a�ade un nodo
            data.addElement(new DataTA(rs.getString("CD_NIVASIS"),
                                       sDesNivAsis.trim()));
            //_______________________

            i++;
          }

          rs.close();
          st.close();

          break;

          // generaci�n de cobertura desde notificaciones EDO
        case servletGENERA_COBERTURA:
          anoDesde = "";
          semDesde = "";
          // primero seleccionamos la �ltima fecha generada
          query =
              "select * from sive_semgen order by cd_anoepig desc, cd_semepig desc";
          //# // System_out.println("1");
          st = con.prepareStatement(query);
          //# // System_out.println("2");
          rs = st.executeQuery();
          //# // System_out.println("3");
          if (rs.next()) {
            anoDesde = rs.getString("cd_anoepig");
            //# // System_out.println("3a");
            semDesde = rs.getString("cd_semepig");
            //# // System_out.println("3b");
          }
          else {
            anoDesde = "";
            semDesde = "";
          }
          rs.close();
          //# // System_out.println("3c");
          st.close();
          //# // System_out.println("3d");
          rs = null;
          //# // System_out.println("3e");
          st = null;
          //# // System_out.println("3f");

          // Despu�s generamos la nueva fecha
          anoHasta = datos.getAnDesde();
          //# // System_out.println("3g");
          semHasta = datos.getSemDesde();
          //# // System_out.println("3h");

          // comprueba que la semana informada supera a la semana generada
          if ( (anoHasta.compareTo(anoDesde) > 0) ||
              (anoHasta.equals(anoDesde) && semHasta.compareTo(semDesde) > 0)) {

            query = "delete from sive_semgen";
            st = con.prepareStatement(query);
            st.executeUpdate();
            st.close();
            st = null;

            query =
                "insert into sive_semgen (cd_anoepig,cd_semepig) values (?,?)";
            //# // System_out.println("4");
            st = con.prepareStatement(query);
            //# // System_out.println("5");
            st.setString(1, anoHasta);
            st.setString(2, semHasta);
            st.executeUpdate();
            //# // System_out.println("6");
            st.close();
            st = null;

            // primero se buscan todas las semanas del rango
            if (anoDesde.compareTo(anoHasta) == 0) {
              query = "select * from sive_semana_epi where " +
                  "cd_anoepi=? and cd_semepi>? and cd_semepi<=?";
              // lanza la query
              st = con.prepareStatement(query);
              //# // System_out.println("7");
              // a�o inicio
              st.setString(1, anoDesde.trim());
              // semana desde
              st.setString(2, semDesde.trim());
              // semana hasta
              st.setString(3, semHasta);
            }
            else {
              query = "select * from sive_semana_epi " +
                  "where (cd_anoepi = ? and cd_semepi > ?) or " +
                  "(cd_anoepi > ? and cd_anoepi < ?) or " +
                  "(cd_anoepi = ? and cd_semepi <= ?)";

              // lanza la query
              st = con.prepareStatement(query);
              //# // System_out.println("8");
              // a�o inicio
              st.setString(1, anoDesde.trim());
              st.setString(3, anoDesde.trim());
              // a�o hasta
              st.setString(4, anoHasta);
              st.setString(5, anoHasta);
              // semana desde
              st.setString(2, semDesde.trim());
              // semana hasta
              st.setString(6, semHasta);
            }
            //# // System_out.println("Lanzamos������������");
            rs = st.executeQuery();
            //# // System_out.println("9");
            i = 0;
            //# // System_out.println("YA EST�������������");
            listaSemana = new CLista();

            while (rs.next()) {
              // a�ade un nodo
              listaSemana.addElement(new DataCN("", "", "", "", "", "", "", "",
                                                "", "", "",
                                                "", "", "", "",
                                                rs.getString("CD_ANOEPI"),
                                                rs.getString("CD_SEMEPI")));
              i++;
              //# // System_out.println("Un dato������������");
            }
            rs.close();
            st.close();

            rs = null;
            st = null;

            // ahora los buscamos todos los equipos de centros en cobertura
            //# // System_out.println("Seleccionamos equipos ����������������������������");
            query = "select a.cd_e_notif,a.nm_notift,b.cd_centro from sive_e_notif a, sive_c_notif b " +
                "where a.cd_centro=b.cd_centro and b.it_cobertura='S' and b.it_baja='N'";

            // lanza la query
            st = con.prepareStatement(query);
            //# // System_out.println("10");
            rs = st.executeQuery();
            //# // System_out.println("11");

            //# // System_out.println("query ejecutada");
            i = 0;
            listaEquipos = new CLista();

            while (rs.next()) {
              // a�ade un nodo
              listaEquipos.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),
                  "", rs.getString("CD_CENTRO"), "", "", "",
                  "", "", "", "", "", rs.getInt("NM_NOTIFT"), "", "", false));
              i++;
            }
            rs.close();
            st.close();

            // ahora combinamos las semanas con los equipos
            listaMixta = new CLista();

            if ( (listaEquipos != null) && (listaSemana != null)) {
              //# // System_out.println("Combinamos ����������������������������");
              for (int j = 0; j < listaEquipos.size(); j++) {
                datosEquipoAuxi = (DataEqNot) listaEquipos.elementAt(j);
                for (int k = 0; k < listaSemana.size(); k++) {
                  datosCentroAuxi = (DataCN) listaSemana.elementAt(k);
                  datosMixtosAuxi = new DataNotifSem(0,
                      datosEquipoAuxi.getNotift(), datos.getCdOperador(),
                      datosEquipoAuxi.getEquipo(), datosCentroAuxi.getSemDesde(),
                      datosCentroAuxi.getAnDesde());
                  listaMixta.addElement(datosMixtosAuxi);
                }
              }
            }
            // Ahora a�adimos todos los equipos que no tienen datos
            if (listaMixta != null) {
              listaSemana = null;
              listaSemana = new CLista();
              //# // System_out.println("A�adimos los no existentes ����������������������������");
              for (int j = 0; j < listaMixta.size(); j++) {
                rs = null;
                st = null;

                datosMixtosAuxi = (DataNotifSem) listaMixta.elementAt(j);

                query = "select * from sive_semana_epi a, sive_notif_sem b " +
                    "where a.cd_semepi=b.cd_semepi and a.cd_anoepi=b.cd_anoepi " +
                    "and b.cd_e_notif =? and b.cd_semepi=? and b.cd_anoepi=?";
                //# // System_out.println("12");
                st = con.prepareStatement(query);
                //# // System_out.println("13");
                // c�digo del equipo
                st.setString(1, datosMixtosAuxi.getCdEquipo().trim());
                // c�digo de la semana
                st.setString(2, datosMixtosAuxi.getSemana().trim());
                // c�digo del a�o
                st.setString(3, datosMixtosAuxi.getAnyo().trim());

                rs = st.executeQuery();
                //# // System_out.println("14");

                if (!rs.next()) {
                  // a�ade un nodo
                  listaSemana.addElement(new DataNotifSem(0,
                      datosMixtosAuxi.getNotifTotal(), datos.getCdOperador(),
                      datosMixtosAuxi.getCdEquipo().trim(),
                      datosMixtosAuxi.getSemana().trim(),
                      datosMixtosAuxi.getAnyo().trim()));
                }
                rs.close();
                st.close();

              }
              // ahora a�adimos los registros definitivos
              for (int j = 0; j < listaSemana.size(); j++) {
                st = null;
                //# // System_out.println("inser. definitivos ����������������������������");
                datosMixtosAuxi = (DataNotifSem) listaSemana.elementAt(j);

                query =
                    "insert into sive_notif_sem (cd_e_notif,cd_anoepi,cd_semepi,nm_nnotift" +
                    ",nm_ntotreal,cd_ope,fc_ultact) values (?,?,?,?,?,?,?)";
                //# // System_out.println("15");
                st = con.prepareStatement(query);
                //# // System_out.println("16");
                // c�digo del equipo
                st.setString(1, datosMixtosAuxi.getCdEquipo().trim());
                // c�digo del a�o
                st.setString(2, datosMixtosAuxi.getAnyo().trim());
                // c�digo de la semana
                st.setString(3, datosMixtosAuxi.getSemana().trim());
                // notificaciones totales
                st.setInt(4, datosMixtosAuxi.getNotifTotal());
                // notificaciones reales
                st.setInt(5, 0);
                // c�digo del operador
                st.setString(6, datos.getCdOperador().trim());
                // fecha de actualizaci�n
                st.setDate(7,
                           (new java.sql.Date( (new java.util.Date()).getTime())));
                //# // System_out.println(datosMixtosAuxi.getNotifTotal());
                st.executeUpdate();
                //# // System_out.println("NT");
                st.close();
              }
            }
          }

          break;

      }
      con.commit();
    }
    catch (Exception exs) {
      con.rollback();
      if (exs.getMessage() != null) {
        // System_out.println(exs.getMessage() );
      }
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (data != null) {
      data.trimToSize();
    }
    lFinProceso = System.currentTimeMillis(); //^^^^^^^^^^^^
    // System_out.println("-TConex:"+( (lFinConexion-lIniConexion)/1000)+"-TProceso:"+(  (lFinProceso-lIniProceso)/1000 ) ) ;
    return data;
  }

  // rellena las descripciones si es necesario
  public void rellena_descripciones(CLista data) throws Exception {
    DataCN datCN = null;
    String codeMun, codeProv;
    String desMun, desProv;

    if (data == null) {
      return;
    }

    //nos traemos todas las descripciones de Municipios
    query =
        "select DS_MUN from SIVE_MUNICIPIO where CD_MUN = ? AND CD_PROV = ? ";
    //# // System_out.println("Lanzamos " +query);
    try {
      // lanza la query
      st = con.prepareStatement(query);
      for (int i = 0; i < data.size(); i++) {
        datCN = (DataCN) data.elementAt(i);
        codeMun = datCN.getMun();
        codeProv = datCN.getProv();
        st.setString(1, codeMun);
        st.setString(2, codeProv);
        rs = st.executeQuery();
        if (rs.next()) {
          desMun = rs.getString(1);
          if (desMun != null) {
            datCN.setDesMun(desMun);
          }
        }
        rs.close();
      } //orf
      st.close();
    }
    catch (Exception exc) {
      throw exc;
      //# // System_out.println("Error descripci�n provincia " + exc.toString());
    }

    //nos traemos todas las descripciones de PROVINCIAS
    query = "select DS_PROV, DSL_PROV from SIVE_PROVINCIA where CD_PROV = ? ";
    //# // System_out.println("Lanzamos " +query);
    try {
      // lanza la query
      st = con.prepareStatement(query);
      for (int i = 0; i < data.size(); i++) {
        datCN = (DataCN) data.elementAt(i);
        codeProv = datCN.getProv();
        st.setString(1, codeProv);
        rs = st.executeQuery();
        if (rs.next()) {
          desMun = rs.getString(1);
          if (desMun != null) {
            datCN.setDesProv(desMun);
            datCN.setDesLProv(rs.getString(2));
          }
        }
        rs.close();
      } // orf
      st.close();
    }
    catch (Exception exc) {
      throw exc;
      //# // System_out.println("Error trayendo descripci�n");
    }
  }

} //______________________________________ END CLASS
