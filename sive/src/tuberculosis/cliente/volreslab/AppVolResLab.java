/**
 * Descripci�n de paquete.
 */
package tuberculosis.cliente.volreslab;

//import sapp2.*;
import capp2.CApp;

//import tuberculosis.cliente.componentes.*; //PanAnoSemFecha
//import util_ficheros.*;//EditorFicheros

/**
 * Applet que realiza la petici�n de datos para exportaciones.
 *
 * @author JRM&AIC
 * @version  1.1 22/03/2000
 */
/*
 * Clase : tuberculosis.cliente.exportaciones.appExportaciones
 *   Autor    Fecha        Accion
 * JRM&AIC  22/03/2000   La implementa
 */

public class AppVolResLab
    extends CApp {
  final String Titulo = "Volcado de Resultados de Laboratorio";

  //constantes para localizaciones

  boolean isStandalone = false;

  PanelVolResLab miPanel = null;

  /**
   * Constructor de la clase.
   */
  /*
   * M�todo : AppVolResLab()
   *   Autor    Fecha        Accion
   * JRM&AIC  22/03/2000   La implementa
   */
  public AppVolResLab() {

  }

  /**
   * Inicializaci�n de applet
   */

  /*
   * M�todo : AppVolResLab.init()
   *   Autor    Fecha        Accion
   * JRM&AIC  22/03/2000   La implementa
   */
  public void init() {
    try {
      super.init();
      miPanel = new PanelVolResLab(this);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Inicializaci�n de controles de pantalla
   */
  /*
   * M�todo : AppVolResLab.jbInit()
   *   Autor    Fecha        Accion
   * JRM&AIC  22/03/2000   La implementa
   */
  private void jbInit() throws Exception {
    setTitulo(Titulo);

    VerPanel("", miPanel);

  }

//Get Applet information

  public String getAppletInfo() {
    return "Applet Information";
  }

//Get parameter info

  public String[][] getParameterInfo() {
    return null;
  }

}
