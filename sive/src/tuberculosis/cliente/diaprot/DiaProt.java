package tuberculosis.cliente.diaprot;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CPanel;
import comun.constantes;
import infproto.DataProtocolo;
// modificacion 30/06/2000 para obtener protocolo general
import tuberculosis.cliente.panotros.PanOtros;
import tuberculosis.cliente.pantubnotif.PanTablaNotif;

public class DiaProt
    extends CDialog {

  // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  //Aceptar == repetir query al volver
  //Cancelar == nada
  public int iOut = -1;

  final String imgNAME[] = {
      "images/aceptar.gif"};

  public int modoOperacion = constantes.modoALTA;

  ScrollPane panelScroll = new ScrollPane();
  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnSalir = new ButtonControl("Aceptar");

  PanProt pP = null;
  PanTablaNotif pNotif = null;
  // modificacion
  PanOtros pOtros = null;

  DataProtocolo datProt = null;
  //DataNotifTub datNotif = null;
  public String sNotif = "";
  Hashtable hs = null;
  PanelGeneral pProtocolo;

  public boolean bProtGen = false;

  // constructor
  public DiaProt(PanOtros panOtros, int modo, DataProtocolo data, boolean b) {

    super(panOtros.app);

    try {
      datProt = data;

      jbInit();
      pOtros = panOtros;
      //sNotif = (DatNotifTub) panNotif.getNotificador().getCD_E_NOTIF()
      modoOperacion = modo;
      Inicializar(modoOperacion);
      bProtGen = b;

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public DiaProt(PanTablaNotif panNotif, int modo, DataProtocolo data) {

    super(panNotif.capp);

    try {
      datProt = data;

      jbInit();
      pNotif = panNotif;
      //sNotif = (DatNotifTub) panNotif.getNotificador().getCD_E_NOTIF()
      modoOperacion = modo;
      Inicializar(modoOperacion);

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void jbInit() throws Exception {
    btnSalir.setActionCommand("cancelar");

    // carga las imagenes
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnSalir.setImage(imgs.getImage(0));

    xYLayout1.setWidth(745);
    xYLayout1.setHeight(440);

    this.setLayout(xYLayout1);
    this.setSize(745, 440);

    DialogOPactionAdapter actionAdapter = new DialogOPactionAdapter(this);
    this.setTitle("Protocolo");
    this.add(btnSalir, new XYConstraints( (785 - 80) / 2, 380, -1, -1));
    btnSalir.addActionListener(actionAdapter);
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar()

  {

    switch (modoOperacion) {

      case constantes.modoALTA:
      case constantes.modoMODIFICACION:
      case constantes.modoBAJA:
      case constantes.modoCONSULTA:
        btnSalir.setEnabled(true);
        if (pP != null) {
          pP.Inicializar();
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoESPERA:
        btnSalir.setEnabled(false);
        if (pP != null) {
          pP.Inicializar();
        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();

  }

  //De DialogoGeneral //PDP 16/03/2000
  public Hashtable getDatosBloqueo() {
    return new Hashtable();
  };

  /*
     // Para recuperar los datos del Notificador actualizado
     public DatCasMuesCS getComponente() {
    return pM.getComponente();
     }
   */

  public void addPanelProt() {
    int modo = modoOperacion;
    this.Inicializar(constantes.modoESPERA);

    pP = null;
    pP = new PanProt(this.app, datProt, this);

    this.add(pP, new XYConstraints(2, 2, 740, 425));
    pP.doLayout();

    this.Inicializar(modo);

  }

  void btnCancelar_actionPerformed() {
    //hay aue recoger cd_ope y fc_ultact
    dispose();
  }

} //fin clase

//PDP 16/03/2000
class PanelGeneral
    extends CPanel {
  public void Inicializar() {};
  public ScrollPane panelScroll = new ScrollPane();
  public ScrollPane panelScroll2 = new ScrollPane();
}

class DialogOPactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaProt adaptee;
  ActionEvent evt;

  DialogOPactionAdapter(DiaProt adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
} //fin class
