/**
 * Clase: DiaNotificaciones
 * Paquete: tuberculosis.cliente.diatubnotif
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT) / Emilio Postigo Riancho
 * Fecha Inicio: 07/10/1999 - 19/11/1999
 * Descripcion: Implementaci�n del panel de introducci�n de una notificaci�n.
 */

package tuberculosis.cliente.diatubnotif;

import java.util.Hashtable;
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import cnicm.DataCN;
import comun.Common;
import comun.Comunicador;

import comun.DataEntradaEDO; //import comun.CFechaSimple; import comun.CHora; import comun.CListaNiveles1; import comun.Comunicador; import comun.constantes; import comun.Data; import comun.DataAutoriza; import comun.DataCasIn; import comun.DataEnferedo; import comun.DataEnfermo; import comun.DataEntradaEDO; import comun.DataEqNot; import comun.DataGeneralCDDS; import comun.DataIndivEnfermedad; import comun.DataLongValid; import comun.DataMun; import comun.DataMunicipioEDO; import comun.DataNivel1; import comun.DataNotifEDO; import comun.DataNotifSem; import comun.DataRegistroEDO; import comun.DataValoresEDO; import comun.DataZBS; import comun.DataZBS2; import comun.DatoBase; import comun.datosParte; import comun.datosPreg; import comun.DialogoGeneral; import comun.DialogRegistroEDO; import comun.DialogSelDomi; import comun.DialogSelEnfermo; import comun.Fechas; import comun.IntContenedor; import comun.PanAnoSemFech; import comun.SrvDebugGeneral; import comun.SrvDlgBuscarRegistro; import comun.SrvEnfermedad; import comun.SrvEntradaEDO; import comun.SrvMaestroEDO; import comun.SrvMun; import comun.SrvMunicipioCont; import comun.SrvNivel1; import comun.SrvZBS2; import comun.UtilEDO; import comun.comun;
import comun.DataGeneralCDDS;
import comun.Fechas;
import comun.IntContenedor;
import comun.PanAnoSemFech;
import comun.UtilEDO;
import comun.constantes;
import panniveles.panelNiveles;
import panniveles.usaPanelNiveles;
import sapp.StubSrvBD;
import tuberculosis.datos.diatubnotif.ConTubNotif;
import tuberculosis.datos.diatubnotif.DatTubNotif;
import tuberculosis.datos.notiftub.DatNotifTub;

public class DiaNotificaciones
    extends CDialog
    implements usaPanelNiveles, IntContenedor {

  // Cola de eventos
  EventQueue eqClase = new EventQueue();

  // Estados posibles del di�logo
  protected final int modoESPERA = constantes.modoESPERA;
  protected final int modoALTA = constantes.modoALTA;
  protected final int modoMODIFICACION = constantes.modoMODIFICACION;
  protected final int modoBAJA = constantes.modoBAJA;
  protected final int modoCONSULTA = constantes.modoCONSULTA;
  protected final int modoLIMPIAR_FECHA = constantes.modoLIMPIAR_FECHAS;

  // C�digo de 'fuente EDO'
  private final String sFuenteEDO = constantes.FUENTE_EDO;

  // Mensaje en caso de bloqueo
  private final String DATOS_BLOQUEADOS =
      "Los datos han sido modificados. �Desea sobrescribirlos?";

  private final String CAMBIO_TEORICOS =
      "�Desea actualizar el n�mero de notificadores te�ricos?";

  // Variables utilizadas para gestionar los estados del di�logo
  protected int modoOperacion = 0;
  protected int modoAnterior = 0;

  // Para saber c�mo construir PanNiveles
  private boolean bAutorizaciones = false;

  // Para sincronizar eventos
  private boolean sinBloquear = true;

  // Conexi�n con servlets
  private StubSrvBD stubCliente;

  // Para controlar cambios de controles
  private String sTeoricosEQ = null;
  private String sNotificador = null;
  private int iIndiceFuente = -1;

  private String sNotTeoricos = null;
  private String sNotReales = null;

  private String sTeoricos = null;
  private String sReales = null;
  private String sTotalReales = null;

  private String sNotTeoricosInicio = null;
  private String sNotRealesInicio = null;
  private String sNotTotalRealesInicio = null;

  private String sNotTeoricosInicioMod = null;
  private String sNotRealesInicioMod = null;
  private String sNotTotalRealesInicioMod = null;

  private boolean bResumenSemanal = false;

  // Para almacenar los valores iniciales (Modificaciones y Bajas)
  private DatNotifTub dntInicio = null;
  // Tiene sentido cuando el contenedor est� en modo modificaci�n
  // y permite distinguirlo del modo alta
  private String sNM_EDO = null;
  // Para devolver valores
  private DatNotifTub dntRetorno = null;

  //private String sDescNotificador = null;
  //private String sCentro = null;

  // Hashtable encargada de almacenar los datos enviados por el contenedor
  private Hashtable hDatos = null;

  // Datos recuperados como par�metros
  private CLista clFuente = null;
  private String sAnoAlta = null;
  // Almacena en cada elemento un objeto DatNotifTub
  private Vector vNotificadoresActuales = null; // E ?????

  // Variables recuperadas del applet
  private String sCD_NIVEL_1 = null;
  private String sCD_NIVEL_2 = null;
  private String sDS_NIVEL_2 = null;

  // Operadores y fechas de actualizaci�n
  // Iniciales

  private String sCD_OPE_SEM_Inicio = null;
  private String sCD_OPE_EQ_Inicio = null;
  private String sCD_OPE_EDO_Inicio = null;
  private String sCD_OPE_EDOI_Inicio = null;

  private String sFC_ULTACT_SEM_Inicio = null;
  private String sFC_ULTACT_EQ_Inicio = null;
  private String sFC_ULTACT_EDO_Inicio = null;
  private String sFC_ULTACT_EDOI_Inicio = null;

  // Finales

  // Datos obtenidos de consultas
  //private String sCD_CENTRO = null;
  //private String sDS_CENTRO = null;
  //private String sCD_ZBS = null;
  private boolean bIT_COBERTURA = false;
  private DatTubNotif dtnRelacion = null;
  private DataEntradaEDO deeNotificador = null;

  // Datos usados de forma habitual
  private String sFechaRecepcion = null;
  private boolean bFechaRecepCorrecta = true;
  private boolean bFuenteEDO = false;

  // Indica si el nivel 2 es obligatorio
  private boolean bObligatorioNivel2 = false;

  // Contenedor de im�genes
  protected CCargadorImagen imgs = null;

  // Imagenes utilizadas
  final String imgNAME[] = {
      "images\\Magnify.gif",
      "images\\aceptar.gif",
      "images\\cancelar.gif"};

  // Layout del panel
  XYLayout lyXYLayout = new XYLayout();

  // Panel con �rea, Distrito y descripci�n de �ste
  panelNiveles panAreaDistrito;

  // Panel con a�o y semana epidenmiol�gicos
  PanAnoSemFech panAnoSemana;

  // Notificador
  Label lblNotificador = new Label();
  CCampoCodigo ccNotificador = new CCampoCodigo();
  ButtonControl btnNotificador = new ButtonControl();
  TextField txtDescNotificador = new TextField();

  // Fuente
  Label lblFuente = new Label();
  Choice choFuente = new Choice();

  // Del centro
  Label lblCentro = new Label();
  TextField txtCentro = new TextField();

  // Fecha Recepcion
  Label lblFechaRecep = new Label();
  //23-05-01 ARS Poner barras en fechas autom�ticas
//  CFechaSimple cfsFechaRecep = new CFechaSimple("N");
  fechas.CFecha cfsFechaRecep = new fechas.CFecha("N");

  // Boton de relacion: compara si existe una notificacion con los datos introducidos
  ButtonControl btnRelacion = new ButtonControl();

  // Resumen semanal
  Label lblResumenSem = new Label();
  Checkbox chkResumenSem = new Checkbox();
  Label lblNotTeoricos = new Label();
  TextField txtNotTeoricos = new TextField();
  Label lblNotReales = new Label();
  TextField txtNotReales = new TextField();
  Label lblTotalNotReales = new Label();
  TextField txtTotalNotReales = new TextField();
  Label lblCobertura = new Label();
  TextField txtCobertura = new TextField();

  // Declarante
  Label lblDeclarante = new Label();
  TextField txtDeclarante = new TextField();

  // Historia cl�nica
  Label lblHistoria = new Label();
  TextField txtHistoria = new TextField();

  // Botones Aceptar y Cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Objeto gestor de eventos en Aceptar y Cancelar
  DiaNotificaciones_ActionAdapter dnaaGestorEventos = new
      DiaNotificaciones_ActionAdapter(this);

  // Objeto gestor de eventos Ganancia/P�rdida de foco en las cajas de texto
  DiaNotificaciones_FocusAdapter dnfaGestorEventos = new
      DiaNotificaciones_FocusAdapter(this);

  // Objeto gestor de eventos Ganancia/P�rdida de foco en las cajas de texto
  // de notificaciones
  Notificadores_FocusAdapter dnfaGestorEventosNotificadores = new
      Notificadores_FocusAdapter(this);

  // Objeto gestor de eventos en las listas desplegables
  DiaNotificaciones_ChoiceItemListener dnchItemListener = new
      DiaNotificaciones_ChoiceItemListener(this);

  // Constructor
  public DiaNotificaciones(CApp a, boolean bAut) {
    super(a);
    try {
      // Para construir PanNiveles en un modo u otro
      bAutorizaciones = bAut;

      // Recuperacion de par�metros
      sCD_NIVEL_1 = a.getCD_NIVEL1_DEFECTO();
      sCD_NIVEL_2 = a.getCD_NIVEL2_DEFECTO();
      sDS_NIVEL_2 = a.getDS_NIVEL2_DEFECTO();
      // Inicializaci�n de los controles
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicializacion en la creacion de un objeto
  private void jbInit() throws Exception {

    // t�tulo
    this.setTitle("Notificador");

    // Dimensionado del di�logo
    this.setSize(570, 430);

    // Parametros del di�logo
    this.setLayout(lyXYLayout);
    lyXYLayout.setHeight(400);
    lyXYLayout.setWidth(570);
    this.setBackground(Color.lightGray);

    // Crea el objeto stub
    stubCliente = new StubSrvBD();

    // Carga de las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // Creaci�n del panel �rea de salud/Distrito/Descripci�n
    //if (modoOperacion == modoALTA){
    panAreaDistrito = new panelNiveles(this.app,
                                       this,
                                       panelNiveles.MODO_INICIO,
                                       constantes.TIPO_NIVEL_3,
                                       constantes.LINEAS_1,
                                       bAutorizaciones);
    /*}else{
     panAreaDistrito = new panelNiveles(this.app,
                                        this,
                                        panelNiveles.MODO_INICIO,
                                        constantes.TIPO_NIVEL_3,
                                        constantes.LINEAS_1,
                                        false);
         }*/

    this.add(panAreaDistrito, new XYConstraints(10, 0, 560, -1));

    // Se establece si son obligatorios o no
    // los dos niveles posibles
    panAreaDistrito.setNivelObligatorio(1, true);

    switch (this.app.getPerfil()) {
      case 4:
      case 5:
        panAreaDistrito.setNivelObligatorio(2, true);
        bObligatorioNivel2 = true;
        break;
      case 1:
      case 2:
      case 3:
        break;
    }

    // Se inicializan con los datos enviados como par�metros
    // del applet
    panAreaDistrito.setBuscaCDNivel1(sCD_NIVEL_1);
    panAreaDistrito.setCDNivel2(sCD_NIVEL_2);
    panAreaDistrito.setDSNivel2(sDS_NIVEL_2);

    // Creaci�n del panel con el a�o y la semana epidemiol�gicos
    panAnoSemana = new PanAnoSemFech(this, PanAnoSemFech.modoINI_MODIFICAR, false);
    panAnoSemana.setBorde(false);

    this.add(panAnoSemana, new XYConstraints(12, 40, -1, -1));

    // Notificador
    lblNotificador.setText("Notificador:");
    this.add(lblNotificador, new XYConstraints(15, 85, 70, -1));

    this.add(ccNotificador, new XYConstraints(85, 85, 49, -1));
    ccNotificador.setBackground(new Color(255, 255, 150));

    btnNotificador.setImage(imgs.getImage(0));
    this.add(btnNotificador, new XYConstraints(136, 85, -1, -1));

    txtDescNotificador.setEditable(false);
    this.add(txtDescNotificador, new XYConstraints(167, 85, 128, -1));

    // Eventos sobre controles 'Notificador'
    ccNotificador.setName("equipo");
    ccNotificador.addFocusListener(dnfaGestorEventos);

    btnNotificador.setActionCommand("equipo");
    btnNotificador.addActionListener(dnaaGestorEventos);

    // Del centro
    lblCentro.setText("Del Centro:");
    this.add(lblCentro, new XYConstraints(307, 85, 65, -1));
    txtCentro.setEditable(false);
    this.add(txtCentro, new XYConstraints(377, 85, 178, -1));

    // Fuente
    lblFuente.setText("Fuente:");
    this.add(lblFuente, new XYConstraints(15, 85 + 40, 40, -1));

    this.add(choFuente, new XYConstraints(85, 85 + 40, 210, -1));
    choFuente.setBackground(new Color(255, 255, 150));

    // Eventos sobre fuentes
    choFuente.setName("fuente");
    choFuente.addItemListener(dnchItemListener);

    // Fecha Recepcion
    lblFechaRecep.setText("Fecha Recepcion:");
    this.add(lblFechaRecep, new XYConstraints(15, 165, 100, -1));
    this.add(cfsFechaRecep, new XYConstraints(120, 165, 80, -1));
    cfsFechaRecep.setBackground(new Color(255, 255, 150));

    // Eventos sobre fecha de recepci�n
    cfsFechaRecep.setName("fecha_recepcion");
    cfsFechaRecep.addFocusListener(dnfaGestorEventos);

    // Boton de relacion: compara si existe una notificacion con los datos introducidos
    btnRelacion.setLabel("Relaci�n");
    btnRelacion.setImage(imgs.getImage(0));
    btnRelacion.setActionCommand("relacion");
    btnRelacion.addActionListener(dnaaGestorEventos);

    this.add(btnRelacion, new XYConstraints(202, 165, 93, -1));

    // Resumen semanal
    lblResumenSem.setText("Res. Semanal:");
    this.add(lblResumenSem, new XYConstraints(15, 190 + 15, 90, -1));
    this.add(chkResumenSem, new XYConstraints(105, 190 + 15, -1, -1));

    chkResumenSem.addItemListener(new
                                  DiaNotificaciones_chkResumenSem_itemAdapter(this));

    // Inicialmente invisible
    // Si chkResumenSem=ok => visible
    lblNotTeoricos.setText("Not. Teor.:");
    lblNotTeoricos.setVisible(false);
    this.add(lblNotTeoricos, new XYConstraints(15, 225 + 15 + 5, 60, -1));
    txtNotTeoricos.setVisible(false);
    this.add(txtNotTeoricos, new XYConstraints(85, 225 + 15 + 5, 35, -1));
    // Eventos
    txtNotTeoricos.setName("not_teoricos");
    txtNotTeoricos.addFocusListener(dnfaGestorEventosNotificadores);

    // Inicialmente invisible
    // Si chkResumenSem=ok => visible
    lblNotReales.setText("Not. Reales:");
    lblNotReales.setVisible(false);
    this.add(lblNotReales, new XYConstraints(132, 225 + 15 + 5, 70, -1));
    txtNotReales.setVisible(false);
    this.add(txtNotReales, new XYConstraints(207, 225 + 15 + 5, 35, -1));

    // Eventos
    txtNotReales.setName("not_reales");
    txtNotReales.addFocusListener(dnfaGestorEventosNotificadores);

    // Inicialmente invisible
    // Si chkResumenSem=ok => visible
    lblTotalNotReales.setText("Tot. Not. Reales:");
    lblTotalNotReales.setVisible(false);
    this.add(lblTotalNotReales,
             new XYConstraints(149 + 122, 225 + 15 + 5, 90, -1));
    txtTotalNotReales.setVisible(false);
    txtTotalNotReales.setEditable(false);
    this.add(txtTotalNotReales,
             new XYConstraints(307 + 65, 225 + 15 + 5, 35, -1));

    // Inicialmente invisible
    // Si chkResumenSem=ok => visible
    lblCobertura.setText("Cobertura:");
    lblCobertura.setVisible(false);
    this.add(lblCobertura, new XYConstraints(419, 225 + 15 + 5, 60, -1));
    txtCobertura.setVisible(false);
    txtCobertura.setEditable(false);
    this.add(txtCobertura, new XYConstraints(486, 225 + 15 + 5, 40, -1));

    // Declarante
    lblDeclarante.setText("Declarante:");
    this.add(lblDeclarante, new XYConstraints(15, 260 + 15 + 10, 68, -1));
    this.add(txtDeclarante, new XYConstraints(85, 260 + 15 + 10, 155 + 132, -1));
    txtDeclarante.setBackground(new Color(255, 255, 150));

    // Historia cl�nica
    lblHistoria.setText("N� Historia cl�nica:");
    this.add(lblHistoria, new XYConstraints(250 + 132, 260 + 25, 100, -1));
    this.add(txtHistoria, new XYConstraints(355 + 132, 260 + 25, 68, -1));

    // Botones Aceptar y Cancelar
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(imgs.getImage(1));
    this.add(btnAceptar, new XYConstraints(345, 310 + 30 + 10, 100, -1));
    btnAceptar.setActionCommand("aceptar");
    btnAceptar.addActionListener(dnaaGestorEventos);

    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(imgs.getImage(2));
    this.add(btnCancelar, new XYConstraints(455, 310 + 30 + 10, 100, -1));
    btnCancelar.setActionCommand("cancelar");
    btnCancelar.addActionListener(dnaaGestorEventos);

    // Se establecen los modos de funcionamiento iniciales
    modoOperacion = modoESPERA;
    modoAnterior = modoESPERA;

    // Para la fecha de recepcion
    cfsFechaRecep.setText(Fechas.date2String(new java.util.Date()));

    // Inicializaci�n
    Inicializar();
  } // Fin jbinit()

  // Sistema de bloqueo de eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    int modo = 0;
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;

      modo = ponerEnEspera();
      if (modo != modoESPERA) {
        modoAnterior = modo;
      }

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** Este m�todo desbloquea el sistema */
  protected synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // Implementacion del metodo abstracto de la clase CPanel
  public void Inicializar() {
    switch (modoOperacion) {
      case modoESPERA:
        panAreaDistrito.setModoEspera();
        panAnoSemana.setModoEspera();

        ccNotificador.setEnabled(false);
        btnNotificador.setEnabled(false);

        choFuente.setEnabled(false);

        cfsFechaRecep.setEnabled(false);
        btnRelacion.setEnabled(false);

        chkResumenSem.setEnabled(false);

        txtNotTeoricos.setEnabled(false);
        txtNotReales.setEnabled(false);
        txtTotalNotReales.setEnabled(false);
        txtCobertura.setEnabled(false);

        txtDeclarante.setEnabled(false);
        txtHistoria.setEnabled(false);

        break;
      case modoALTA:
        panAnoSemana.setModoNormal();
        panAreaDistrito.setModoNormal();

        ccNotificador.setEnabled(!panAnoSemana.getFechaNotificacion().equals("")
                                 && !panAreaDistrito.getCDNivel1().equals("")
                                 &&
                                 (!panAreaDistrito.getCDNivel2().equals("") || !bObligatorioNivel2));

        btnNotificador.setEnabled(ccNotificador.isEnabled());

        choFuente.setEnabled(!txtDescNotificador.getText().equals(""));
        btnRelacion.setEnabled(choFuente.isEnabled());

        cfsFechaRecep.setEnabled(choFuente.isEnabled());

        chkResumenSem.setVisible(bIT_COBERTURA && bFuenteEDO);

        chkResumenSem.setEnabled(chkResumenSem.isVisible());

        lblResumenSem.setVisible(chkResumenSem.isVisible());

        txtNotTeoricos.setVisible(chkResumenSem.isVisible() &&
                                  chkResumenSem.getState());
        txtNotReales.setVisible(txtNotTeoricos.isVisible());
        txtTotalNotReales.setVisible(txtNotTeoricos.isVisible());
        txtCobertura.setVisible(txtNotTeoricos.isVisible());

        lblNotTeoricos.setVisible(txtNotTeoricos.isVisible());
        lblNotReales.setVisible(txtNotTeoricos.isVisible());
        lblTotalNotReales.setVisible(txtNotTeoricos.isVisible());
        lblCobertura.setVisible(txtNotTeoricos.isVisible());

        txtNotTeoricos.setEnabled(true);
        txtNotReales.setEnabled(true);
        txtTotalNotReales.setEnabled(true);
        txtCobertura.setEnabled(true);

        txtDeclarante.setEnabled(choFuente.isEnabled());
        txtHistoria.setEnabled(txtDeclarante.isEnabled());

        break;
      case modoMODIFICACION:
        panAnoSemana.setModoDeshabilitado();
        panAreaDistrito.setModoDeshabilitado();

        ccNotificador.setEnabled(false);
        btnNotificador.setEnabled(false);

        choFuente.setEnabled(false);
        btnRelacion.setEnabled(false);

        cfsFechaRecep.setEnabled(false);

        chkResumenSem.setVisible(bIT_COBERTURA && bFuenteEDO);
        chkResumenSem.setEnabled(chkResumenSem.isVisible());

        lblResumenSem.setVisible(chkResumenSem.isVisible());

        txtNotTeoricos.setVisible(chkResumenSem.isVisible() &&
                                  chkResumenSem.getState());
        txtNotReales.setVisible(txtNotTeoricos.isVisible());
        txtTotalNotReales.setVisible(txtNotTeoricos.isVisible());
        txtCobertura.setVisible(txtNotTeoricos.isVisible());

        lblNotTeoricos.setVisible(txtNotTeoricos.isVisible());
        lblNotReales.setVisible(txtNotTeoricos.isVisible());
        lblTotalNotReales.setVisible(txtNotTeoricos.isVisible());
        lblCobertura.setVisible(txtNotTeoricos.isVisible());

        txtNotTeoricos.setEnabled(true);
        txtNotReales.setEnabled(true);
        txtTotalNotReales.setEnabled(true);
        txtCobertura.setEnabled(true);

        txtDeclarante.setEnabled(true);
        txtHistoria.setEnabled(true);
        break;
      case modoBAJA:
        panAnoSemana.setModoDeshabilitado();
        panAreaDistrito.setModoDeshabilitado();

        ccNotificador.setEnabled(false);
        btnNotificador.setEnabled(false);

        choFuente.setEnabled(false);
        btnRelacion.setEnabled(false);

        cfsFechaRecep.setEnabled(false);

        chkResumenSem.setVisible(bFuenteEDO);
        chkResumenSem.setEnabled(false);

        lblResumenSem.setVisible(chkResumenSem.isVisible());

        txtNotTeoricos.setVisible(chkResumenSem.isVisible() &&
                                  chkResumenSem.getState());
        txtNotReales.setVisible(txtNotTeoricos.isVisible());
        txtTotalNotReales.setVisible(txtNotTeoricos.isVisible());
        txtCobertura.setVisible(txtNotTeoricos.isVisible());

        lblNotTeoricos.setVisible(txtNotTeoricos.isVisible());
        lblNotReales.setVisible(txtNotTeoricos.isVisible());
        lblTotalNotReales.setVisible(txtNotTeoricos.isVisible());
        lblCobertura.setVisible(txtNotTeoricos.isVisible());

        txtNotTeoricos.setEnabled(false);
        txtNotReales.setEnabled(false);
        txtTotalNotReales.setEnabled(false);
        txtCobertura.setEnabled(false);

        txtDeclarante.setEnabled(false);
        txtHistoria.setEnabled(false);
        break;
      case modoCONSULTA:
        panAnoSemana.setModoDeshabilitado();
        panAreaDistrito.setModoDeshabilitado();

        ccNotificador.setEnabled(false);
        btnNotificador.setEnabled(false);

        choFuente.setEnabled(false);
        btnRelacion.setEnabled(false);

        cfsFechaRecep.setEnabled(false);

        chkResumenSem.setVisible(bFuenteEDO);
        chkResumenSem.setEnabled(false);

        lblResumenSem.setVisible(chkResumenSem.isVisible());

        txtNotTeoricos.setVisible(chkResumenSem.isVisible() &&
                                  chkResumenSem.getState());
        txtNotReales.setVisible(txtNotTeoricos.isVisible());
        txtTotalNotReales.setVisible(txtNotTeoricos.isVisible());
        txtCobertura.setVisible(txtNotTeoricos.isVisible());

        lblNotTeoricos.setVisible(txtNotTeoricos.isVisible());
        lblNotReales.setVisible(txtNotTeoricos.isVisible());
        lblTotalNotReales.setVisible(txtNotTeoricos.isVisible());
        lblCobertura.setVisible(txtNotTeoricos.isVisible());

        txtNotTeoricos.setEnabled(false);
        txtNotReales.setEnabled(false);
        txtTotalNotReales.setEnabled(false);
        txtCobertura.setEnabled(false);

        txtDeclarante.setEnabled(false);
        txtHistoria.setEnabled(false);

        btnAceptar.setEnabled(false);
        break;
    }
    // Se repinta
    this.validate();
  }

  // M�todos PRIVADOS

  // Para decidir si una excepci�n ha sido originada por un
  // bloqueo
  private CLista resolverBloqueo(Exception e, int modo, DatTubNotif dtn,
                                 Hashtable h) {
    CLista clValor = null;

    if (bContiene(e.getMessage(), constantes.PRE_BLOQUEO)) {
      if (Common.ShowPregunta(this.app, DATOS_BLOQUEADOS)) {
        try {
          clValor = hazAccion(modo, dtn, h);
        }
        catch (Exception exp) {
          Common.ShowWarning(this.app, exp.getMessage());
        }
      }
    }
    else {
      Common.ShowWarning(this.app, e.getMessage());
    }

    return clValor;
  }

  //
  private String getAnno() {
    return panAnoSemana.txtAno.getText();
  }

  /*  private String getSemana() {
      return panAnoSemana.txtCodSem.getText();
    }*/

  // Si la semana actual es v�lida y no est� generada, lanza el
  // proceso que se encarga de generar todas las semanas que falten
  // en sive_notif_sem hasta la semana seleccionada
  private void generadorDeCobertura() {
    CLista datos;
    try {
      datos = new CLista();
      DataCN datacn = new DataCN("", "", "", "", "", "", "",
                                 "", "", "", "", "S", "",
                                 this.app.getLogin(), "N",
                                 this.getAnno(), this.getSEMANA());
      //this.getAnno(), this.getSemana());
      datos.addElement(datacn);

      Comunicador.Communicate(this.app, stubCliente,
                              constantes.modoGENERACOBERTURA,
                              constantes.strSERVLET_CN, datos);

    }
    catch (Exception e) {
      ;
    }
  } // fin m�todo GeneradorDeCobertura()

  // Devuelve true si el objeto 'contenedor' contiene la cadena especificada
  private boolean bContiene(String contenedor, String contenido) {
    return contenedor.indexOf(contenido) != -1;
  }

  // Realiza las acciones correspondientes a las bajas
  private boolean hazBaja() {
    boolean bValor = false; // Valor de retorno

    Hashtable hDatosBaja = null;

    CLista clResultado = null;
    //CLista clDatos = null;

    DatTubNotif dtnParametros = null;

    // Objeto GLOBAL empleado para devolver datos al llamador del di�logo
    // Se carga con los valores recibidos y se modifica con
    // lo pertinente
    dntRetorno = dntInicio;

    // Para hacer llegar c�digo de equipo, a�o y semana...
    dtnParametros = new DatTubNotif();

    // Para saber qu� se debe pasar
    sTeoricos = null;
    sReales = null;
    sTotalReales = null;

    try {
      hDatosBaja = new Hashtable();
      hGestionResumenSemanalBajas();

      dtnParametros.setCodEquipo(ccNotificador.getText());
      dtnParametros.setAnno(panAnoSemana.txtAno.getText());
      dtnParametros.setSemana(getSEMANA());
      dtnParametros.setFechaNotif(panAnoSemana.txtFecSem.getText());
      dtnParametros.setFechaRecep(cfsFechaRecep.getText());

      // Para SIVE_NOTIF_EDOI
      dtnParametros.setCD_FUENTE(dntInicio.getCD_FUENTE());
      dtnParametros.setNM_EDO(sNM_EDO);
      dtnParametros.setCD_OPE_NOTIF_EDOI(dntInicio.getCD_OPE_EDOI());
      dtnParametros.setFC_ULTACT_NOTIF_EDOI(dntInicio.getFC_ULTACT_EDOI());
      dtnParametros.setIT_PRIMERO(dntInicio.getIT_PRIMERO());

      // Si procede...
      if (sTotalReales != null) {
        hDatosBaja.put(ConTubNotif.TOTAL_SEMANAS, sTotalReales);
        hDatosBaja.put(ConTubNotif.CD_OPE_SEMANAS, sCD_OPE_SEM_Inicio);
        hDatosBaja.put(ConTubNotif.FC_ULTACT_SEMANAS, sFC_ULTACT_SEM_Inicio);
      }

      dtnParametros.setCD_OPE_NOTIFEDO(sCD_OPE_EDO_Inicio);
      dtnParametros.setFC_ULTACT_NOTIFEDO(sFC_ULTACT_EDO_Inicio);
      dtnParametros.setResSem(chkResumenSem.getState() ? "S" : "N");

      clResultado = hazAccion(constantes.modoBAJA, dtnParametros, hDatosBaja);
    }
    catch (Exception e) {
      clResultado = resolverBloqueo(e, constantes.modoBAJA_SB, dtnParametros,
                                    hDatosBaja);
    }
    finally {
      if (clResultado != null) {
        // Se devuelve el total de reales tras la eliminaci�n de estos
        dntRetorno.setNM_NTOTREAL_SEM(String.valueOf(
            new Integer(sNotTotalRealesInicioMod).intValue()
            - new Integer(sNotRealesInicioMod).intValue()));

        // Aqu� hay que comprobar si el eliminado era el
        // primer notificador.
        cargaDatosBloqueo( (Hashtable) clResultado.firstElement());
        bValor = true;
      }
    }

    return bValor;
  }

  // Realiza las acciones correspondientes a las modificaciones
  private boolean hazModificacion() {
    boolean bValor = false; // Valor de retorno

    Hashtable hDatosModificacion = null;

    CLista clResultado = null;
    //CLista clDatos = null;

    DatTubNotif dtnParametros = null;

    // Esta es la condici�n que, de cumplirse, indica que los datos
    // son v�lidos
    if (txtDeclarante.getText().trim().equals("")) {
      Common.ShowWarning(this.app, "Faltan campos obligatorios");
      return false;
    }

    // Se ejecutan p�rdidas de foco cr�ticas
    if (sNotTeoricos == null) {
      sNotTeoricos = txtNotTeoricos.getText().trim();
    }
    txtNotTeoricos_focusLost();

    if (sNotReales == null) {
      sNotReales = txtNotReales.getText().trim();
    }
    txtNotReales_focusLost();

    // Objeto GLOBAL empleado para devolver datos al llamador del di�logo
    // Se carga con los valores recibidos y se modifica con
    // lo pertinente
    dntRetorno = dntInicio;

    // Para hacer llegar c�digo de equipo, a�o y semana
    dtnParametros = new DatTubNotif();

    // Para saber qu� se debe pasar
    sTeoricos = null;
    sReales = null;
    sTotalReales = null;

    try {
      hDatosModificacion = new Hashtable();
      hGestionResumenSemanalModificaciones();

      dtnParametros.setCodEquipo(ccNotificador.getText());
      dtnParametros.setAnno(panAnoSemana.txtAno.getText());
      dtnParametros.setSemana(getSEMANA());
      dtnParametros.setFechaNotif(panAnoSemana.txtFecSem.getText());
      dtnParametros.setFechaRecep(cfsFechaRecep.getText());

      dtnParametros.setNM_EDO(sNM_EDO);
      dtnParametros.setCD_FUENTE(dntInicio.getCD_FUENTE());

      dtnParametros.setDS_DECLARANTE(txtDeclarante.getText().trim());
      dtnParametros.setDS_HISTCLI(txtHistoria.getText().trim());

      // Si procede...
      if (sTeoricos != null) {
        hDatosModificacion.put(ConTubNotif.TEORICOS_SEMANAS, sTeoricos);
        hDatosModificacion.put(ConTubNotif.CD_OPE_SEMANAS, sCD_OPE_SEM_Inicio);
        hDatosModificacion.put(ConTubNotif.FC_ULTACT_SEMANAS,
                               sFC_ULTACT_SEM_Inicio);

        if (!sTeoricos.equals(sTeoricosEQ)) {
          if (Common.ShowPregunta(this.app, CAMBIO_TEORICOS)) {
            hDatosModificacion.put(ConTubNotif.TEORICOS_EQUIPOS, sTeoricos);
            hDatosModificacion.put(ConTubNotif.CD_OPE_EQUIPOS,
                                   sCD_OPE_EQ_Inicio);
            hDatosModificacion.put(ConTubNotif.FC_ULTACT_EQUIPOS,
                                   sFC_ULTACT_EQ_Inicio);

            sTeoricosEQ = sTeoricos;
          }
        }
      }

      dtnParametros.setNotifReales(sReales == null ? txtNotReales.getText() :
                                   sReales);

      if (sTotalReales != null) {
        hDatosModificacion.put(ConTubNotif.TOTAL_SEMANAS, sTotalReales);
        hDatosModificacion.put(ConTubNotif.CD_OPE_SEMANAS, sCD_OPE_SEM_Inicio);
        hDatosModificacion.put(ConTubNotif.FC_ULTACT_SEMANAS,
                               sFC_ULTACT_SEM_Inicio);
      }

      dtnParametros.setCD_OPE_NOTIFEDO(sCD_OPE_EDO_Inicio);
      dtnParametros.setFC_ULTACT_NOTIFEDO(sFC_ULTACT_EDO_Inicio);

      dtnParametros.setCD_OPE_NOTIF_EDOI(sCD_OPE_EDOI_Inicio);
      dtnParametros.setFC_ULTACT_NOTIF_EDOI(sFC_ULTACT_EDOI_Inicio);

      dtnParametros.setResSem(chkResumenSem.getState() ? "S" : "N");

      clResultado = hazAccion(constantes.modoMODIFICACION, dtnParametros,
                              hDatosModificacion);
    }
    catch (Exception e) {
      clResultado = resolverBloqueo(e, constantes.modoMODIFICACION_SB,
                                    dtnParametros, hDatosModificacion);
    }
    finally {
      if (clResultado != null) {
        cargaDatosBloqueo( (Hashtable) clResultado.elementAt(0));

        dntRetorno.setIT_RESSEM(chkResumenSem.getState() ? "S" : "N");
        dntRetorno.setNM_NNOTIFT_SEM(sTeoricos == null ? txtNotTeoricos.getText() :
                                     sTeoricos);
        dntRetorno.setNM_NNOTIFR(sReales == null ? txtNotReales.getText() :
                                 sReales);
        dntRetorno.setNM_NTOTREAL_SEM(sTotalReales == null ?
                                      txtTotalNotReales.getText() :
                                      sTotalReales);

        dntRetorno.setDS_DECLARANTE(txtDeclarante.getText().trim());
        dntRetorno.setDS_HISTCLI(txtHistoria.getText().trim());
        dntRetorno.setNM_NOTIFT_EQ(sTeoricosEQ);

        bValor = true;
      }
    }

    return bValor;
  }

  // Efect�a la llamada al servlet con el modo de operaci�n
  // y los datos necesarios
  private CLista hazAccion(int modo, DatTubNotif dtn, Hashtable h) throws
      Exception {
    CLista clDatos;
    CLista clResultado;

    clDatos = new CLista();

    clDatos.addElement(dtn);
    clDatos.addElement(h);

    clDatos.setLogin(this.app.getLogin());

    // perfil
    clDatos.setPerfil(app.getPerfil());

    // idioma
    clDatos.setIdioma(app.getIdioma());

    /*SrvTubNotif srv = new SrvTubNotif();
         clResultado = srv.doPrueba(modo, clDatos); */

    // modificacion momentanea 02062000

    clResultado = Comunicador.Communicate(this.app,
                                          stubCliente,
                                          modo,
                                          constantes.strSERVLET_TUBNOTIF,
                                          clDatos);

    return clResultado;
  }

  // Realiza las acciones correspondientes al alta
  private boolean hazAlta() {
    boolean bValor = false; // Valor de retorno

    boolean bExiste = false;

    Hashtable hDatosAlta = null;

    CLista clResultado = null;
    //CLista clDatos = null;

    DatTubNotif dtnDatos = null;

    // Objeto empleado para devolver datos al llamador del di�logo
    dntRetorno = null;

    // Sabremos, en el momento oprtuno, lo que se debe pasar
    // como par�metro gracias a estas variables
    sTeoricos = null;
    sReales = null;
    sTotalReales = null;

    // Esta es la condici�n que, de cumplirse, indica que los datos
    // son v�lidos
    if (txtDeclarante.getText().trim().equals("") || !bFechaRecepCorrecta ||
        choFuente.getSelectedIndex() == 0) {
      Common.ShowWarning(this.app, "Faltan campos obligatorios");
      return false;
    }

    // Se ejecutan p�rdidas de foco cr�ticas
    if (sNotTeoricos == null) {
      sNotTeoricos = txtNotTeoricos.getText().trim();
    }
    txtNotTeoricos_focusLost();

    if (sNotReales == null) {
      sNotReales = txtNotReales.getText().trim();
    }
    txtNotReales_focusLost();

    // Se crea un objeto DatNotifTub con los datos de que disponemos
    dntRetorno = new DatNotifTub();

    // Se cargan los valores de la clave (Salvo el n� de caso EDO)
    dntRetorno.setCD_E_NOTIF_EDOI(ccNotificador.getText().trim());
    dntRetorno.setCD_ANOEPI_EDOI(panAnoSemana.txtAno.getText());

    // Para asegurarnos de que se env�a 01, 02, etc.
    dntRetorno.setCD_SEMEPI(getSEMANA());

    dntRetorno.setFC_NOTIF(panAnoSemana.txtFecSem.getText());
    dntRetorno.setFC_RECEP_EDOI(cfsFechaRecep.getText());
    // De momento, no se deja pasar la fuente en este punto,
    // para que no haya conflicto de NOTIFEDO'S
    //dntRetorno.setCD_FUENTE( ((DataGeneralCDDS) clFuente.elementAt(choFuente.getSelectedIndex())).getCD() );

    // Se comprueba que no existe a�n
    for (int i = 0; i < vNotificadoresActuales.size() && !bExiste; i++) {
      if ( ( (DatNotifTub) vNotificadoresActuales.elementAt(i)).contiene(
          dntRetorno)) {
        bExiste = true;
      }
    }

    if (!bExiste) {
      // Ya que este notificador no est� presente en la tabla enviada
      // como par�metro, se procede a almacenar los datos pertinentes

      // Gesti�n de NOTIFEDO
      dtnDatos = new DatTubNotif();

      dtnDatos.setCodEquipo(ccNotificador.getText().trim());
      dtnDatos.setAnno(panAnoSemana.txtAno.getText());
      dtnDatos.setSemana(getSEMANA());
      dtnDatos.setFechaRecep(cfsFechaRecep.getText());
      dtnDatos.setFechaNotif(panAnoSemana.txtFecSem.getText());
      dtnDatos.setNotifReales(txtNotReales.getText().trim());
      dtnDatos.setResSem(chkResumenSem.getState() == true ? "S" : "N");
      dtnDatos.setCD_OPE_NOTIFEDO(sCD_OPE_EDO_Inicio);
      dtnDatos.setFC_ULTACT_NOTIFEDO(sFC_ULTACT_EDO_Inicio);

      dtnDatos.setFC_ULTACT_NOTIF_SEM(sFC_ULTACT_SEM_Inicio);
      dtnDatos.setCD_OPE_NOTIF_SEM(sCD_OPE_SEM_Inicio);

      // Permitir� al servlet saber si est� dando de alta o modificando
      // Si su contenido es distinto de '', podr� darse de alta en
      // SIVE_NOTIF_EDOI
      dtnDatos.setNM_EDO(sNM_EDO);
      if (!sNM_EDO.equals("")) { // Si es posible crear el registro en SIVE_NOTIF_EDOI ...
        dtnDatos.setCD_FUENTE( ( (DataGeneralCDDS) clFuente.elementAt(choFuente.
            getSelectedIndex() - 1)).getCD()); //*E
        dtnDatos.setDS_DECLARANTE(txtDeclarante.getText().trim());
        dtnDatos.setDS_HISTCLI(txtHistoria.getText().trim());
        dtnDatos.setIT_PRIMERO("N");
      }

      // Fin gesti�n NOTIFEDO
      hDatosAlta = new Hashtable();

      // La gesti�n de resumen semanal s�lo procede si la fuente es EDO
      if (bFuenteEDO) {
        // este dato s�lo tiene sentido aqu�
        dtnDatos.setNotifReales(txtNotReales.getText().trim());
        hGestionResumenSemanalAltas();

        // Si procede...
        if (sTeoricos != null) {
          hDatosAlta.put(ConTubNotif.TEORICOS_SEMANAS, sTeoricos);
          hDatosAlta.put(ConTubNotif.CD_OPE_SEMANAS, sCD_OPE_SEM_Inicio);
          hDatosAlta.put(ConTubNotif.FC_ULTACT_SEMANAS, sFC_ULTACT_SEM_Inicio);

          if (!sTeoricos.equals(sTeoricosEQ)) {
            if (Common.ShowPregunta(this.app, CAMBIO_TEORICOS)) {
              hDatosAlta.put(ConTubNotif.TEORICOS_EQUIPOS, sTeoricos);
              hDatosAlta.put(ConTubNotif.CD_OPE_EQUIPOS, sCD_OPE_EQ_Inicio);
              hDatosAlta.put(ConTubNotif.FC_ULTACT_EQUIPOS,
                             sFC_ULTACT_EQ_Inicio);

              sTeoricosEQ = sTeoricos;
            }
          }

        }

        dtnDatos.setNotifReales(sReales == null ? txtNotReales.getText() :
                                sReales);

        if (sTotalReales != null) {
          hDatosAlta.put(ConTubNotif.TOTAL_SEMANAS, sTotalReales);
          hDatosAlta.put(ConTubNotif.CD_OPE_SEMANAS, sCD_OPE_SEM_Inicio);
          hDatosAlta.put(ConTubNotif.FC_ULTACT_SEMANAS, sFC_ULTACT_SEM_Inicio);
        }
      }

      // Grabaci�n
      try {
        clResultado = hazAccion(constantes.modoALTA, dtnDatos, hDatosAlta);
      }
      catch (Exception e) {
        clResultado = resolverBloqueo(e, constantes.modoALTA_SB, dtnDatos,
                                      hDatosAlta);
      }
      finally {
        if (clResultado != null) {
          cargaDatosBloqueo( (Hashtable) clResultado.firstElement());

          dntRetorno.setCD_FUENTE( ( (DataGeneralCDDS) clFuente.elementAt(
              choFuente.getSelectedIndex() - 1)).getCD());

          // No forman parte de la clave y se cargan aqu�
          dntRetorno.setNM_EDO_EDOI(sNM_EDO);
          dntRetorno.setCD_E_NOTIF_EDOI(ccNotificador.getText());
          dntRetorno.setDS_CENTRO(deeNotificador.getDesCentro());

          dntRetorno.setCD_NIVEL_1(getCD_NIVEL_1_EQ());
          dntRetorno.setCD_NIVEL_2(getCD_NIVEL_2_EQ());
          dntRetorno.setCD_ZBS(getCD_NIVEL_ZBS_EQ());

          dntRetorno.setDS_DECLARANTE(txtDeclarante.getText().trim());
          dntRetorno.setDS_HISTCLI(txtHistoria.getText().trim());

          dntRetorno.setCD_CENTRO(deeNotificador.getCodCentro());
          dntRetorno.setDS_E_NOTIF(deeNotificador.getDes());
          dntRetorno.setIT_RESSEM(dtnDatos.getResSem());
          dntRetorno.setIT_PRIMERO("N");
          dntRetorno.setIT_COBERTURA(bIT_COBERTURA ? "S" : "N");
          dntRetorno.setNM_NNOTIFT_SEM(sTeoricos == null ?
                                       txtNotTeoricos.getText() : sTeoricos);
          dntRetorno.setNM_NNOTIFR(sReales == null ? txtNotReales.getText() :
                                   sReales);
          dntRetorno.setNM_NTOTREAL_SEM(sTotalReales == null ?
                                        txtTotalNotReales.getText() :
                                        sTotalReales);

          dntRetorno.setNM_NOTIFT_EQ(sTeoricosEQ);
          bValor = true;
        }
      }
      // Fin de grabaci�n

    }
    else {
      Common.ShowWarning(this.app, "El Notificador ya existe");
    }

    return bValor;
  }

  // Resumen semanal y bajas
  private void hGestionResumenSemanalBajas() {
    // Si son iguales al principio y al final
    if (bResumenSemanal) {
      // Se trata como si hubiera estado marcado y ahora no
      sReales = sNotRealesInicioMod;
      // Este valor ir� a SIVE_NOTIF_SEM
      sTotalReales = String.valueOf( (new Integer(sNotTotalRealesInicioMod).
                                      intValue() -
                                      new Integer(sReales).intValue()));
    }
  }

  // Resumen semanal y modificaciones
  private void hGestionResumenSemanalModificaciones() {
    int iCambiosTN = 0;

    iCambiosTN = iGestionResumenSemanal();

    // Si son iguales al principio y al final
    if (bResumenSemanal == chkResumenSem.getState()) {
      if (bResumenSemanal) {
        if (iCambiosTN != -1) { // S�lo han cambiado los datos relativos a n�meros
          //Si han cambiado los totales
          if (!sNotTotalRealesInicioMod.equals(txtTotalNotReales.getText().trim())) {
            sTotalReales = txtTotalNotReales.getText().trim();
          }

          // Si han cambiado los te�ricos
          if (!sNotTeoricosInicioMod.equals(txtNotTeoricos.getText().trim())) {
            sTeoricos = txtNotTeoricos.getText().trim();
          }

          // Si han cambiado los reales
          if (!sNotRealesInicioMod.equals(txtNotReales.getText().trim())) {
            sReales = txtNotReales.getText().trim();
          }
        }
      }
    }
    else { // Si son diferentes ...
      if (!bResumenSemanal) { // No estaba marcado y ahora s�
        if (iCambiosTN != -1) { // Si han cambiado los datos relativos a n�meros
          // Si han cambiado los totales
          if (!sNotTotalRealesInicioMod.equals(txtTotalNotReales.getText().trim())) {
            sTotalReales = txtTotalNotReales.getText().trim();
          }

          // Si han cambiado los te�ricos
          if (!sNotTeoricosInicioMod.equals(txtNotTeoricos.getText().trim())) {
            sTeoricos = txtNotTeoricos.getText().trim();
          }

          // Si han cambiado los reales
          if (!sNotRealesInicioMod.equals(txtNotReales.getText().trim())) {
            sReales = txtNotReales.getText().trim();
          }
        }
      }
      else { // Estaba marcado y ahora no
        sReales = sNotRealesInicioMod;
        // Este valor ir� a SIVE_NOTIF_SEM
        sTotalReales = String.valueOf( (new Integer(sNotTotalRealesInicioMod).
                                        intValue() -
                                        new Integer(sReales).intValue()));
        // Este valor ir� a SIVE_NOTIFEDO
        sReales = "0";
      }
    }
  }

  // Para gestionar las modificaciones en resumen semanal en altas
  private void hGestionResumenSemanalAltas() {
    int iCambiosTN = 0;

    iCambiosTN = iGestionResumenSemanal();

    // Si son iguales al principio y al final
    if (bResumenSemanal == chkResumenSem.getState()) {
      if (bResumenSemanal) {
        if (iCambiosTN != -1) { // S�lo han cambiado los datos relativos a n�meros
          //Si han cambiado los totales
          if (!sNotTotalRealesInicio.equals(txtTotalNotReales.getText().trim())) {
            sTotalReales = txtTotalNotReales.getText().trim();
          }

          // Si han cambiado los te�ricos
          if (!sNotTeoricosInicio.equals(txtNotTeoricos.getText().trim())) {
            sTeoricos = txtNotTeoricos.getText().trim();
          }

          // Si han cambiado los reales
          if (!sNotRealesInicio.equals(txtNotReales.getText().trim())) {
            sReales = txtNotReales.getText().trim();
          }

        }
      }
    }
    else { // Si son diferentes ...
      if (!bResumenSemanal) { // No estaba marcado y ahora s�
        if (iCambiosTN != -1) { // Si han cambiado los datos relativos a n�meros
          // Si han cambiado los totales
          if (!sNotTotalRealesInicio.equals(txtTotalNotReales.getText().trim())) {
            sTotalReales = txtTotalNotReales.getText().trim();
          }

          // Si han cambiado los te�ricos
          if (!sNotTeoricosInicio.equals(txtNotTeoricos.getText().trim())) {
            sTeoricos = txtNotTeoricos.getText().trim();
          }

          // Si han cambiado los reales
          if (!sNotRealesInicio.equals(txtNotReales.getText().trim())) {
            sReales = txtNotReales.getText().trim();
          }
        }
      }
      else { // Estaba marcado y ahora no
        sReales = sNotRealesInicio;
        // Este valor ir� a SIVE_NOTIF_SEM
        sTotalReales = String.valueOf( (new Integer(sNotTotalRealesInicio).
                                        intValue() -
                                        new Integer(sReales).intValue()));
        // Este valor ir� a SIVE_NOTIFEDO
        sReales = "0";
      }
    }
  }

  // -1: sin cambios, 1: datos nuevos, 2: cambios en los antiguos
  private int iGestionResumenSemanal() {

    int iValor = -1;

    if (sNotTeoricosInicio == null) { // no tocar nada

    }
    else { // es necesario efectuar comprobaciones
      if (sNotTeoricosInicio.equals("")) { // Datos nuevos
        iValor = 1;
      }
      else { // Comprobar los antiguos
        if (sNotTeoricosInicio.equals(txtNotTeoricos.getText().trim())
            && sNotRealesInicio.equals(txtNotReales.getText().trim())) {

        }
        else {
          iValor = 2;
        }
      }
    }

    return iValor;

  }

  // Este m�todo permite al componente llamador recuperar los
  // valores introducidos.
  public Object getComponente() {
    return dntRetorno;
  }

  // Para obtener la cobertura
  private String sPorcentaje(String sDe, String sSobre) {
    String sValor = "0";

    int iDe = new Integer(sDe).intValue();
    int iSobre = new Integer(sSobre).intValue();

    if (iSobre != 0) {
      sValor = String.valueOf(100 * iDe / iSobre);
    }

    return sValor;

  }

  private void limpiaFechaRecepcion() {
    sFechaRecepcion = "";
    cfsFechaRecep.setText(Fechas.date2String(new java.util.Date()));
    // Los datos propios de la relaci�n de notificaciones se
    // borran
    dtnRelacion = null;
    gestionDatosRelacion(null);
  }

  protected void lanzaEvento() {
    java.awt.AWTEvent e = null;
    try {
      e = eqClase.peekEvent();
    }
    catch (Exception exc) {
    }
    finally {
      e = null;
    }
  }

  //
  private void cargaDatosBloqueo(Hashtable hFuente) {
    if (hFuente.containsKey(ConTubNotif.NUEVO_PRIMERO)) {
      // Si se recibe este objeto, significa que se ha dado de baja un notificador
      // con categor�a de primer declarante y que se ha decidido qui�n le sustituye.
      // Los datos pertinentes, los de la clave salvo NM_EDO, se devuelven al panel.
      //dntRetorno = new DatNotifTub();

      DatTubNotif dtnTemp = (DatTubNotif) hFuente.get(ConTubNotif.NUEVO_PRIMERO);

      dntRetorno.setCD_E_NOTIF_EDOI(dtnTemp.getCodEquipo());
      dntRetorno.setCD_ANOEPI_EDOI(dtnTemp.getAnno());
      dntRetorno.setCD_SEMEPI(dtnTemp.getSemana());
      dntRetorno.setFC_RECEP_EDOI(dtnTemp.getFechaRecep());
      dntRetorno.setFC_NOTIF(dtnTemp.getFechaNotif());
      dntRetorno.setCD_FUENTE(dtnTemp.getCD_FUENTE());

      dntRetorno.setIT_PRIMERO("S");
    }

    // Se devuelven los datos originales...
    dntRetorno.setFC_ULTACT_EQ(sFC_ULTACT_EQ_Inicio);
    dntRetorno.setCD_OPE_EQ(sCD_OPE_EQ_Inicio);

    dntRetorno.setFC_ULTACT_SEM(sFC_ULTACT_SEM_Inicio);
    dntRetorno.setCD_OPE_SEM(sCD_OPE_SEM_Inicio);

    dntRetorno.setFC_ULTACT_EDOI(sFC_ULTACT_EDOI_Inicio);
    dntRetorno.setCD_OPE_EDOI(sCD_OPE_EDOI_Inicio);

    // ... salvo que haya cambios
    if (hFuente.containsKey(ConTubNotif.CD_OPE_SEMANAS)) {
      dntRetorno.setCD_OPE_SEM( (String) hFuente.get(ConTubNotif.CD_OPE_SEMANAS));
      dntRetorno.setFC_ULTACT_SEM( (String) hFuente.get(ConTubNotif.
          FC_ULTACT_SEMANAS));
    }

    if (hFuente.containsKey(ConTubNotif.CD_OPE_NOTIFEDO)) {
      dntRetorno.setCD_OPE_EDO( (String) hFuente.get(ConTubNotif.
          CD_OPE_NOTIFEDO));
      dntRetorno.setFC_ULTACT_EDO( (String) hFuente.get(ConTubNotif.
          FC_ULTACT_NOTIFEDO));
    }

    if (hFuente.containsKey(ConTubNotif.CD_OPE_EQUIPOS)) {
      dntRetorno.setCD_OPE_EQ( (String) hFuente.get(ConTubNotif.CD_OPE_EQUIPOS));
      dntRetorno.setFC_ULTACT_EQ( (String) hFuente.get(ConTubNotif.
          FC_ULTACT_EQUIPOS));
    }

    if (hFuente.containsKey(ConTubNotif.CD_OPE_NOTIF_EDOI)) {
      dntRetorno.setCD_OPE_EDOI( (String) hFuente.get(ConTubNotif.
          CD_OPE_NOTIF_EDOI));
      dntRetorno.setFC_ULTACT_EDOI( (String) hFuente.get(ConTubNotif.
          FC_ULTACT_NOTIF_EDOI));
    }

  }

  private void gestionDatosNotificador(DataEntradaEDO deeDatos) {
    deeNotificador = deeDatos;

    ccNotificador.setText(deeNotificador.sCod);
    txtDescNotificador.setText(deeNotificador.sDes);

    /*sCD_CENTRO = deeDatos.sCodCentro;
         sDS_CENTRO = deeDatos.getDesCentro();
         sCD_ZBS = deeDatos.getZBS();*/
    txtCentro.setText(deeNotificador.getCodCentro() + " " +
                      deeNotificador.getDesCentro());
    bIT_COBERTURA = deeNotificador.sDato7.equals("S");

    // Datos de control de bloqueo
    sCD_OPE_EQ_Inicio = deeNotificador.getCD_OPE();
    sFC_ULTACT_EQ_Inicio = deeNotificador.getFC_ULTACT();

    // Notificadores te�ricos
    sTeoricosEQ = deeNotificador.getNM_NOTIFT();
  }

  private void gestionDatosSemanas(DatTubNotif dtnDatos) {
    if (dtnDatos != null) {
      sCD_OPE_EDO_Inicio = "";
      sFC_ULTACT_EDO_Inicio = "";
      sCD_OPE_SEM_Inicio = dtnDatos.getCD_OPE_NOTIF_SEM();
      sFC_ULTACT_SEM_Inicio = dtnDatos.getFC_ULTACT_NOTIF_SEM();

      txtNotTeoricos.setText(dtnDatos.getNotifTeor());
      txtNotReales.setText("0");
      txtTotalNotReales.setText(dtnDatos.getTotNotifReales());

      txtCobertura.setText(sPorcentaje(txtTotalNotReales.getText(),
                                       txtNotTeoricos.getText()) + "%");

      // Para saber qu� hab�a al principio
      sNotTeoricosInicio = dtnDatos.getNotifTeor();
      sNotRealesInicio = "0";
      sNotTotalRealesInicio = dtnDatos.getTotNotifReales();

    }
  }

  private void gestionDatosRelacion(DatTubNotif dtnDatos) {
    if (dtnDatos != null) {
      panAnoSemana.txtFecSem.setText(dtnDatos.getFechaNotif());
      cfsFechaRecep.setText(dtnDatos.getFechaRecep());
      txtDeclarante.setText(dtnDatos.getDS_DECLARANTE());
      txtHistoria.setText(dtnDatos.getDS_HISTCLI());
      chkResumenSem.setState(dtnRelacion.sResSem.equals("S"));

      txtNotTeoricos.setText(dtnDatos.getNotifTeor());
      txtNotReales.setText(dtnDatos.getNotifReales());
      txtTotalNotReales.setText(dtnDatos.getTotNotifReales());

      txtCobertura.setText(sPorcentaje(txtTotalNotReales.getText(),
                                       txtNotTeoricos.getText()) + "%");

      // Para saber qu� hab�a al principio
      sNotTeoricosInicio = dtnDatos.getNotifTeor();
      sNotRealesInicio = dtnDatos.getNotifReales();
      sNotTotalRealesInicio = dtnDatos.getTotNotifReales();

      bResumenSemanal = dtnRelacion.sResSem.equals("S");

      // Datos de control de bloqueo
      sCD_OPE_SEM_Inicio = dtnDatos.getCD_OPE_NOTIF_SEM();
      sCD_OPE_EDO_Inicio = (dtnDatos.getCD_OPE_NOTIFEDO() == null ? "" :
                            dtnDatos.getCD_OPE_NOTIFEDO());

      sFC_ULTACT_SEM_Inicio = dtnDatos.getFC_ULTACT_NOTIF_SEM();
      sFC_ULTACT_EDO_Inicio = (dtnDatos.getFC_ULTACT_NOTIFEDO() == null ? "" :
                               dtnDatos.getFC_ULTACT_NOTIFEDO());

      // Se supone que los datos fueron validados al ser introducidos
      bFechaRecepCorrecta = true;

    }
    else {
      //cfsFechaRecep.setText("");
      //txtDeclarante.setText("");
      txtHistoria.setText("");

      chkResumenSem.setState(false);

      txtNotTeoricos.setText("0");
      txtNotReales.setText("0");
      txtTotalNotReales.setText("0");
      txtCobertura.setText("0");

      // Para saber qu� hab�a al principio
      sNotTeoricosInicio = null;
      sNotRealesInicio = null;
      sNotTotalRealesInicio = null;

      bResumenSemanal = false;

    }
  }

  // Siempre ser� obligatorio el �rea a introducir
  private String getCD_NIVEL_1_EQ() {
    return panAreaDistrito.getCDNivel1();
  }

  // Se recupera cn los datos del notificador
  private String getCD_NIVEL_2_EQ() {
    return deeNotificador.getNivel2();
  }

  // Se recupera cn los datos del notificador
  private String getCD_NIVEL_ZBS_EQ() {
    return deeNotificador.getZBS();
  }

  private String getCD_FUENTE() {
    String s = null;
    if (choFuente.getItemCount() > 0 && choFuente.getSelectedIndex() > 0) {
      s = (String) ( (DataGeneralCDDS) clFuente.elementAt(choFuente.
          getSelectedIndex() - 1)).getCD();
    }
    return s;
  }

  private String getCD_EQUIPO() {
    return ccNotificador.getText().trim();
  }

  private String getANO() {
    return panAnoSemana.txtAno.getText().trim();
  }

  private String getSEMANA() {
    String s_Semana_Formateada = "0" + panAnoSemana.txtCodSem.getText();
    if (s_Semana_Formateada.length() > 2) {
      s_Semana_Formateada = s_Semana_Formateada.substring(1);
    }

    return s_Semana_Formateada;
  }

  // M�todos est�ndard

  // Permite al contenedor proporcionar datos al di�logo
  public void rellena(Hashtable h) {
    hDatos = h;
    if (hDatos != null) {
      // Se enviar� al servlet a trav�s de un objeto
      // de tipo DatTubNotif
      if (hDatos.containsKey(constantes.CASOEDO)) {
        sNM_EDO = (String) hDatos.get(constantes.CASOEDO);
      }
      else {
        sNM_EDO = "";
      }
      if (hDatos.containsKey(constantes.FUENTESNOTIF)) {
        clFuente = (CLista) hDatos.get(constantes.FUENTESNOTIF);
        choFuente.removeAll();
        choFuente.add(""); //*E
        for (int i = 0; i < clFuente.size(); i++) {
          choFuente.add( (String) ( (DataGeneralCDDS) clFuente.elementAt(i)).
                        getCD()
                        + " " +
                        (String) ( (DataGeneralCDDS) clFuente.elementAt(i)).
                        getDS());
        }
      }
      if (hDatos.containsKey(constantes.ANO_ALTA_DESDE_RT)) {
        sAnoAlta = (String) hDatos.get(constantes.ANO_ALTA_DESDE_RT);
        // Se inicializa el a�o de alta del panel con el valor obtenido
        panAnoSemana.txtAno.setText(sAnoAlta);
      }
      if (hDatos.containsKey(constantes.VECTORNOTIFICADORES)) {
        vNotificadoresActuales = (Vector) ( (Vector) hDatos.get(constantes.
            VECTORNOTIFICADORES)).clone();
        if (hDatos.containsKey(constantes.VECTORNUEVOSNOTIFICADORES)) {
          Vector v = (Vector) hDatos.get(constantes.VECTORNUEVOSNOTIFICADORES);
          for (int i = 0; i < v.size(); i++) {
            vNotificadoresActuales.addElement( (DatNotifTub) v.elementAt(i));
          }
        }
      }
      if (hDatos.containsKey(constantes.DATOSNOTIFICADOR)) {
        dntInicio = (DatNotifTub) hDatos.get(constantes.DATOSNOTIFICADOR);
        panAreaDistrito.setCDNivel1(dntInicio.getCD_NIVEL_1());
        panAreaDistrito.setCDNivel2(dntInicio.getCD_NIVEL_2());
        panAnoSemana.txtAno.setText(dntInicio.getCD_ANOEPI_EDOI());
        panAnoSemana.txtCodSem.setText(dntInicio.getCD_SEMEPI());
        panAnoSemana.txtFecSem.setText(dntInicio.getFC_NOTIF());
        ccNotificador.setText(dntInicio.getCD_E_NOTIF_EDOI());
        txtDescNotificador.setText(dntInicio.getDS_E_NOTIF());
        txtCentro.setText(dntInicio.getCD_CENTRO() + " " +
                          dntInicio.getDS_CENTRO());
        cfsFechaRecep.setText(dntInicio.getFC_RECEP_EDOI());

        txtDeclarante.setText(dntInicio.getDS_DECLARANTE());
        txtHistoria.setText(dntInicio.getDS_HISTCLI());

        chkResumenSem.setState(dntInicio.getIT_RESSEM().equals("S"));

        // Si viene 'chequeado'
        if (chkResumenSem.getState()) {
          sNotRealesInicio = "";
          sNotTeoricosInicio = "";
          sNotTotalRealesInicio = "";
        }

        txtNotTeoricos.setText(dntInicio.getNM_NNOTIFT_SEM());
        txtNotReales.setText(dntInicio.getNM_NNOTIFR());
        txtTotalNotReales.setText(dntInicio.getNM_NTOTREAL_SEM());

        txtCobertura.setText(sPorcentaje(txtTotalNotReales.getText(),
                                         txtNotTeoricos.getText()) + "%");

        // Poner choice de fuentes en el modo adecuado
        for (int i = 0; i < clFuente.size(); i++) {
          if ( ( (String) ( (DataGeneralCDDS) clFuente.elementAt(i)).getCD()).
              equals(dntInicio.getCD_FUENTE())) {
            choFuente.select(i + 1);
            break;
          }
        }

        // Ver si es EDO
        bFuenteEDO = dntInicio.getCD_FUENTE().equals(sFuenteEDO);

        // invocar al m�todo de b�squeda de distrito
        panAreaDistrito.txtFocusNivel2();

        // Cargar datos usados en el bloqueo
        sCD_OPE_EDO_Inicio = dntInicio.getCD_OPE_EDO();
        sCD_OPE_SEM_Inicio = dntInicio.getCD_OPE_SEM();
        sCD_OPE_EQ_Inicio = dntInicio.getCD_OPE_EQ();
        sCD_OPE_EDOI_Inicio = dntInicio.getCD_OPE_EDOI();

        sFC_ULTACT_EDO_Inicio = dntInicio.getFC_ULTACT_EDO();
        sFC_ULTACT_SEM_Inicio = dntInicio.getFC_ULTACT_SEM();
        sFC_ULTACT_EQ_Inicio = dntInicio.getFC_ULTACT_EQ();
        sFC_ULTACT_EDOI_Inicio = dntInicio.getFC_ULTACT_EDOI();

        // Su centro tiene cobertura
        bIT_COBERTURA = dntInicio.getIT_COBERTURA().equals("S");

        // Resumen semanal
        bResumenSemanal = dntInicio.getIT_RESSEM().equals("S");

        // Para controlar cambios
        sNotTeoricosInicioMod = dntInicio.getNM_NNOTIFT_SEM();
        sNotRealesInicioMod = dntInicio.getNM_NNOTIFR();
        sNotTotalRealesInicioMod = dntInicio.getNM_NTOTREAL_SEM();

        // Te�ricos de Equipos
        sTeoricosEQ = dntInicio.getNM_NOTIFT_EQ();

        // Para controlar cambios
        //sNotTeoricos = dntInicio.getNM_NNOTIFT_SEM();
        //sNotReales = dntInicio.getNM_NNOTIFR();
      }
      else { // Estamos en un alta
        cfsFechaRecep.setText(Fechas.date2String(new java.util.Date()));
      }
    }
  }

  // Gesti�n de eventos

  // Manejador de cambio de estado de chkResumenSem
  protected void chkResumenSem_itemStateChanged(ItemEvent e) {
    if (chkResumenSem.getState()) {
      // Si chkResumenSem esta chequeado se ense�a el resto de componentes
      lblNotTeoricos.setVisible(true);
      txtNotTeoricos.setVisible(true);
      lblNotReales.setVisible(true);
      txtNotReales.setVisible(true);
      lblTotalNotReales.setVisible(true);
      txtTotalNotReales.setVisible(true);
      lblCobertura.setVisible(true);
      txtCobertura.setVisible(true);

      if (sNotTeoricosInicio == null) {
        sNotTeoricosInicio = "";
        sNotRealesInicio = "";
        sNotTotalRealesInicio = "";
      }

    }
    else {
      // Si chkResumenSem no esta chequeado no se ense�a el resto de componentes
      lblNotTeoricos.setVisible(false);
      txtNotTeoricos.setVisible(false);
      lblNotReales.setVisible(false);
      txtNotReales.setVisible(false);
      lblTotalNotReales.setVisible(false);
      txtTotalNotReales.setVisible(false);
      lblCobertura.setVisible(false);
      txtCobertura.setVisible(false);

      if (sNotTeoricosInicio.equals("")) {
        sNotTeoricosInicio = null;
        sNotRealesInicio = null;
        sNotTotalRealesInicio = null;
      }

    }
    // Se repinta
    this.validate();
  }

  // Botones
  protected void btnAceptar_actionPerformed() {

    boolean bSalir = false;

    switch (modoAnterior) { // ya que el panel est� en espera
      case modoALTA:
        bSalir = hazAlta();
        break;
      case modoMODIFICACION:
        bSalir = hazModificacion();
        break;
      case modoBAJA:
        bSalir = hazBaja();
        break;
    }

    if (bSalir) {
      dispose();
    }
  }

  protected void btnCancelar_actionPerformed() {
    dntRetorno = null; // Se vac�a
    dispose();
  }

  // iModo = 0, llamada desde bot�n (se muestra di�logo o mensaje)
  // iModo = 1, llamada desde la p�rdida de foco de la fecha de recepci�n
  // (no se muestra nada, pero se cargan datos si procede)

  protected void btnRelacion_actionPerformed(int iModo) {

    DiaRelaciones diaRelaciones = null;
    Comunicador comRelaciones = null;
    CLista clResultado = null;

    DatTubNotif data = new DatTubNotif();
    DatTubNotif dtnTemporal = null;

    CLista clLista = new CLista();

    comRelaciones = new Comunicador();

    data.sCodEquipo = getCD_EQUIPO();
    data.sAnno = getANO();
    data.sSemana = getSEMANA();
    data.setCD_FUENTE(getCD_FUENTE());

    clLista.addElement(data);

    // Nuevo
    generadorDeCobertura();

    try {
      clResultado = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            constantes.modoLEERDATOS,
                                            constantes.strSERVLET_TUBNOTIF,
                                            clLista);
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
      dtnRelacion = null; // E ????
      if (clResultado.size() > 0) {
        // En caso de que s�lo vengan datos relativos a las semanas
        if ( ( (DatTubNotif) clResultado.firstElement()).getResSem().equals("X")) {
          dtnRelacion = (DatTubNotif) clResultado.firstElement();
          gestionDatosSemanas(dtnRelacion);
          // no se encontraron registros
          if (iModo == 0) {
            Common.ShowWarning(this.app,
                               "No se encontraron notificaciones relacionadas");
          }
        }
        else {
          switch (iModo) {
            case 0:
              clResultado.trimToSize();
              diaRelaciones = new DiaRelaciones(this.app, clResultado,
                  "Relaci�n de Notificaciones coincidentes");
              diaRelaciones.show();
              dtnRelacion = (DatTubNotif) diaRelaciones.getComponente();
              if (dtnRelacion != null) { // E ??????
                gestionDatosRelacion(dtnRelacion);
              }
              break;
            case 1:
              for (int i = 0; i < clResultado.size(); i++) {
                dtnTemporal = (DatTubNotif) clResultado.elementAt(i);
                if (dtnTemporal.getFechaNotif().equals(panAnoSemana.
                    getFechaNotificacion())
                    && dtnTemporal.getFechaRecep().equals(sFechaRecepcion)
                    ) {
                  dtnRelacion = dtnTemporal;
                  break;
                }
              }
              if (dtnRelacion != null) {
                gestionDatosRelacion(dtnRelacion);
              }
              else {
                // si no se ha encontrado uno que coincida por completo,
                // al menos se cargan los datos de la semana relacionada.
                dtnRelacion = (DatTubNotif) clResultado.firstElement();
                gestionDatosSemanas(dtnRelacion);
              }
              break;
          }
        }
        // Para gestionar el cambio de fuente y limpiar o no
        // la caja de texto de la fecha de recepci�n
        iIndiceFuente = choFuente.getSelectedIndex();
      }
      else {
        // no se encontraron registros
        if (iModo == 0) {
          Common.ShowWarning(this.app,
                             "No se encontraron notificaciones relacionadas");
        }
      }
    }
  }

  protected void btnEquipos_actionPerformed() {
    DataEntradaEDO data;

    CListaEquipos lista = new CListaEquipos(this,
                                            "Equipos notificadores",
                                            stubCliente,
                                            constantes.strSERVLET_MAESTRO,
                                            constantes.sOBTENER_X_CODIGO,
                                            constantes.sOBTENER_X_DESCRIPCION,
                                            constantes.sSELECCION_X_CODIGO,
                                            constantes.sSELECCION_X_DESCRIPCION);

    if (lista.getHayDatos() == 1) {
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();

      if (data != null) {
        // Se inhabilita panel de �REA/DISTRITO
        panAreaDistrito.setEnabled(false);
        // Se limpia la caja de tecto de la fecha de recepci�n
        limpiaFechaRecepcion();
        // Se cargan las variables correspondientes
        // con la informaci�n obtenida
        gestionDatosNotificador(data);
      }
    }
  }

  // Listas desplegables
  protected void choFuente_Click() {
    bFuenteEDO = false;

    if (choFuente.getSelectedIndex() > 0) { //Si no est� el blanco seleccionado
      bFuenteEDO = (
          ( (DataGeneralCDDS) clFuente.elementAt(choFuente.getSelectedIndex() -
                                                 1)).getCD()
          ).equals(sFuenteEDO);
    }

    if (iIndiceFuente != choFuente.getSelectedIndex()) {
      limpiaFechaRecepcion();

      // Si la fecha es correcta, se lanza el evento de relaciones
      if (bFechaRecepCorrecta) {
        btnRelacion_actionPerformed(1);
      }
    }
  }

  // Cajas de texto
  protected void cfsFechaRecepcion_focusGained() {
    bFechaRecepCorrecta = false;
    iIndiceFuente = choFuente.getSelectedIndex();
    sFechaRecepcion = cfsFechaRecep.getText().trim();
    if (sFechaRecepcion.equals(Fechas.date2String(new java.util.Date()))) {
      sFechaRecepcion = "";
    }
  }

  protected void cfsFechaRecepcion_focusLost() {
    cfsFechaRecep.setText(cfsFechaRecep.getText().trim());

    bFechaRecepCorrecta = false;

    // Si no ha cambiado, nos vamos
    if (cfsFechaRecep.getText().equals(sFechaRecepcion)) {
      //return;
    }

    cfsFechaRecep.ValidarFecha();
    cfsFechaRecep.setText(cfsFechaRecep.getFecha()); //mlm: lo Cargo

    // La fecha no es v�lida
    if (cfsFechaRecep.getValid().equals("N")) {
      cfsFechaRecep.setText(sFechaRecepcion);
      //bFechaRecepCorrecta = false;
      return;
    }

    // La fecha est� vac�a (s�lo es posible hasta que se introduce un valor v�lido)
    if (cfsFechaRecep.getText().trim().equals("")) {
      //bFechaRecepCorrecta = false;
      return;
    }

    //Se comprueba que es MAYOR o IGUAL que la de notificaci�n
    if (UtilEDO.Compara_Fechas(panAnoSemana.getFechaNotificacion(),
                               cfsFechaRecep.getText()) == 1) {
      // No lo es
      Common.ShowWarning(this.app,
          "La fecha de recepci�n ha de ser mayor o igual que la de notificaci�n");
      cfsFechaRecep.setText(sFechaRecepcion);
    }
    else { // Lo es.
      // Ahora se comprueba si la fecha de recepci�n
      // es menor o igual que la del d�a actual
      if (UtilEDO.Compara_Fechas(cfsFechaRecep.getText(),
                                 Fechas.date2String(new java.util.Date())) == 1) {
        Common.ShowWarning(this.app,
            "La fecha de recepci�n ha de ser menor o igual que la actual");
        cfsFechaRecep.setText(sFechaRecepcion);
      }
    }
    // En cualquier caso, si el contenido final es correcto,
    // se habilitar� la siguiente opci�n
    if (!cfsFechaRecep.getText().equals("")) {
      bFechaRecepCorrecta = true;
      sFechaRecepcion = cfsFechaRecep.getText();
    }
  }

  protected void txtNotificador_focusGained() {
    sNotificador = ccNotificador.getText().trim();
    //sDescNotificador = txtDescNotificador.getText().trim();
    //sCentro = txtCentro.getText().trim();
  }

  protected void txtNotificador_focusLost() {
    CLista data = new CLista();
    CLista clNotificador = null;
    int modo = 0;

    ccNotificador.setText(ccNotificador.getText().trim());
    String campo = ccNotificador.getText();

    if (campo.equals("")) {
      if (!sNotificador.equals(null)) {
        ccNotificador.setText(sNotificador);
      }
      return;
    }

    if (!campo.equals(sNotificador)) {
      modo = ponerEnEspera();

      DataEntradaEDO deeNotificador = new DataEntradaEDO(ccNotificador.getText(),
          "",
          "",
          panAreaDistrito.getCDNivel1(),
          panAreaDistrito.getCDNivel2());

      data.addElement(deeNotificador);
      clNotificador = Common.traerDatos(app, stubCliente,
                                        constantes.strSERVLET_MAESTRO,
                                        constantes.sOBTENER_X_CODIGO, data);

      if (clNotificador != null && clNotificador.size() > 0) {
        deeNotificador = (DataEntradaEDO) clNotificador.firstElement();
        if (deeNotificador != null) {
          panAreaDistrito.setEnabled(false); // Se inhabilita panel de �REA/DISTRITO
          gestionDatosNotificador(deeNotificador);
          // Se limpia la caja de texto de la fecha de recepci�n
          limpiaFechaRecepcion();
        }
      }
      else {
        //txtDescNotificador.setText("");
        //txtCentro.setText("");
        Common.ShowWarning(this.app, "No se encontraron datos");
        ccNotificador.setText(sNotificador);
      }
      ponerModo(modo);
    }
  }

  // Gesti�n de eventos sobre las cajas de texto asociadas con
  // n� de notificadores

  // Ganancia de foco
  protected void txtNotTeoricos_focusGained() {
    sNotTeoricos = txtNotTeoricos.getText().trim();
  }

  protected void txtNotReales_focusGained() {
    sNotReales = txtNotReales.getText().trim();
  }

  // P�rdida de foco
  protected void txtNotTeoricos_focusLost() {
    String sNotTeoricosTemp = txtNotTeoricos.getText().trim().equals("") ? "0" :
        txtNotTeoricos.getText().trim();

        // Si son iguales salimos
    if (sNotTeoricos.equals(sNotTeoricosTemp)) {
      return;
    }

    int iTeoricos = new Integer(sNotTeoricosTemp).intValue();
    //int iReales = new Integer( txtNotReales.getText().trim() ).intValue();
    int iTotal = new Integer(txtTotalNotReales.getText().trim()).intValue();

    if (iTeoricos < iTotal) {
      Common.ShowWarning(this.app, "Valor incorrecto");
    }
    else {
      sNotTeoricos = sNotTeoricosTemp;
    }

    txtNotTeoricos.setText(sNotTeoricos);
    txtCobertura.setText(sPorcentaje(txtTotalNotReales.getText(),
                                     txtNotTeoricos.getText()) + "%");
  }

  protected void txtNotReales_focusLost() {
    String sNotRealesTemp = txtNotReales.getText().trim().equals("") ? "0" :
        txtNotReales.getText().trim();

        // Si son iguales salimos
    if (sNotReales.equals(sNotRealesTemp)) {
      return;
    }

    int iTeoricos = new Integer(txtNotTeoricos.getText().trim()).intValue();
    int iReales = new Integer(sNotRealesTemp).intValue();
    int iTotal = new Integer(txtTotalNotReales.getText().trim()).intValue();

    if (iTeoricos < iReales) {
      Common.ShowWarning(this.app, "Valor incorrecto");
    }
    else {
      iTotal = iTotal + iReales - new Integer(sNotReales).intValue();
      if (iTeoricos < iTotal) {
        Common.ShowWarning(this.app, "Valor incorrecto");
      }
      else {
        txtTotalNotReales.setText(String.valueOf(iTotal));
        sNotReales = sNotRealesTemp;
      }
    }

    txtNotReales.setText(sNotReales);
    txtCobertura.setText(sPorcentaje(txtTotalNotReales.getText(),
                                     txtNotTeoricos.getText()) + "%");
  }

  // Fin gesti�n de eventos

  // Exigido por la interfaz ...
  public void cambioNivelAntesInformado(int niv) {
    System.out.println("cambioNivelAntesInformado " + niv);
  }

  public int ponerEnEspera() {
    int modo = modoOperacion;
    if (modoOperacion != modoESPERA) {
      modoOperacion = modoESPERA;
      Inicializar();
    }
    //System.out.println("ponerEnEspera ");
    return modo;
  }

  public void ponerModo(int modo) {
    // Situaci�n especial
    if (modo == constantes.modoLIMPIAR_FECHAS) {
      limpiaFechaRecepcion();
    }
    else if (modoOperacion != modo) {
      modoOperacion = modo;
      Inicializar();
    }
    //System.out.println("ponerModo " + modo);
  }

  // Exigido por IntContenedor
  public CApp getMiCApp() {
    return this.app;
  }

} // endclass PanTuberNotificaciones

/******************** ESCUCHADORES DE EVENTOS *********************/

// Eventos sobre los botones
class DiaNotificaciones_ActionAdapter
    implements java.awt.event.ActionListener {
  DiaNotificaciones adaptee;

  DiaNotificaciones_ActionAdapter(DiaNotificaciones adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(java.awt.event.ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand().equals("aceptar")) {
        adaptee.btnAceptar_actionPerformed();
      }
      else if (e.getActionCommand().equals("cancelar")) {
        adaptee.btnCancelar_actionPerformed();
      }
      else if (e.getActionCommand().equals("equipo")) {
        adaptee.btnEquipos_actionPerformed();
      }
      else if (e.getActionCommand().equals("relacion")) {
        adaptee.btnRelacion_actionPerformed(0);
      }
      adaptee.desbloquea();
    }
  }
} // Fin DiaNotificacionesActionAdapter

// Eventos sobre el checkbox
class DiaNotificaciones_chkResumenSem_itemAdapter
    implements java.awt.event.ItemListener {
  DiaNotificaciones adaptee;

  public DiaNotificaciones_chkResumenSem_itemAdapter(DiaNotificaciones adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(java.awt.event.ItemEvent e) {
    if (adaptee.bloquea()) {
      adaptee.chkResumenSem_itemStateChanged(e);
      adaptee.desbloquea();
    }
  }
}

// P�rdida de foco en las cajas de texto
class DiaNotificaciones_FocusAdapter
    extends java.awt.event.FocusAdapter {

  DiaNotificaciones adaptee;
  FocusEvent e = null;
  String nombreTxt = null;

  public DiaNotificaciones_FocusAdapter(DiaNotificaciones adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    //adaptee.lanzaEvento(); // E
    //if (adaptee.bloquea()) {

    adaptee.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    this.e = e;
    nombreTxt = ( (Component) e.getSource()).getName();
    if (nombreTxt.equals("equipo")) {
      adaptee.txtNotificador_focusLost();
    }
    else if (nombreTxt.equals("fecha_recepcion")) {
      adaptee.cfsFechaRecepcion_focusLost();
    }
    adaptee.Inicializar();
    adaptee.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    //}
  }

  public void focusGained(FocusEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      nombreTxt = ( (Component) e.getSource()).getName();

      if (nombreTxt.equals("equipo")) {
        adaptee.txtNotificador_focusGained();
      }
      else if (nombreTxt.equals("fecha_recepcion")) {
        adaptee.cfsFechaRecepcion_focusGained();
      }
      adaptee.desbloquea();
    }
  }
}

// Eventos en las listas desplegables
class DiaNotificaciones_ChoiceItemListener
    implements java.awt.event.ItemListener {
  DiaNotificaciones adaptee;
  ItemEvent e = null;

  DiaNotificaciones_ChoiceItemListener(DiaNotificaciones adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      String nombre = ( (Component) e.getSource()).getName();

      if (nombre.equals("fuente")) {
        adaptee.choFuente_Click();
      }
      adaptee.desbloquea();
    }
  }
}

// Clase empleada para visualizar la lista de notificadores
class CListaEquipos
    extends CListaValores {

  protected DiaNotificaciones dialogo;

  public CListaEquipos(DiaNotificaciones dialogo,
                       String title,
                       StubSrvBD stub,
                       String servlet,
                       int obtener_x_codigo,
                       int obtener_x_descripcion,
                       int seleccion_x_codigo,
                       int seleccion_x_descripcion) {

    super(dialogo.getMiCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    this.dialogo = dialogo;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", "", dialogo.panAreaDistrito.getCDNivel1(),
                              dialogo.panAreaDistrito.getCDNivel2());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

// Gesti�n de las cajas de texto de los notificadores
class Notificadores_FocusAdapter
    extends java.awt.event.FocusAdapter {

  DiaNotificaciones adaptee;
  FocusEvent e = null;
  String nombreTxt = null;

  public Notificadores_FocusAdapter(DiaNotificaciones adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {

    adaptee.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    this.e = e;
    nombreTxt = ( (Component) e.getSource()).getName();
    if (nombreTxt.equals("not_teoricos")) {
      adaptee.txtNotTeoricos_focusLost();
    }
    else if (nombreTxt.equals("not_reales")) {
      adaptee.txtNotReales_focusLost();
    }

    adaptee.Inicializar();
    adaptee.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  public void focusGained(FocusEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      nombreTxt = ( (Component) e.getSource()).getName();

      if (nombreTxt.equals("not_teoricos")) {
        adaptee.txtNotTeoricos_focusGained();
      }
      else if (nombreTxt.equals("not_reales")) {
        adaptee.txtNotReales_focusGained();
      }
      adaptee.desbloquea();
    }
  }

}
//*********************
