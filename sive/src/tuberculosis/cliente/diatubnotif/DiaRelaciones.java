/**
 * Clase: DiaRelaciones
 * Paquete: tuberculosis.cliente.diatubnotif
 * Hereda: CDialog
 * Autor: Emilio Postigo Riancho
 * Fecha Inicio: 23/11/1999
 * Descripcion: Implementacion del diálogo que presenta notificaciones
               que cumplen determinadas condiciones.
 */

package tuberculosis.cliente.diatubnotif;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import jclass.bwt.JCActionEvent;
import jclass.util.JCUtilConverter;
import jclass.util.JCVector;
import tuberculosis.datos.diatubnotif.DatTubNotif;

public class DiaRelaciones
    extends CDialog {

  // modos de actualización de la lista
  final int modoADD = 20;
  //ResourceBundle res = ResourceBundle.getBundle("notentrada.Res"+ getCApp().getIdioma());
  final int modoMODIFY = 21;
  final int modoDELETE = 22;

  // constantes del dialog
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/declaracion2.gif",
      "images/declaracion.gif"};

  // parámetros
  CLista listaRet = new CLista();

  JCVector items = new JCVector();
  private CLista listaTbl = new CLista();
  String sResSem = "", sFechaNotif = "", sFechaRecep = "";
  String sNotifReales = "", sImage = "";
  int iOut = -1;

  // controles
  jclass.bwt.JCMultiColumnList tbl = new jclass.bwt.JCMultiColumnList();
  XYLayout xYLayout = new XYLayout();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Listeners
  DiaRelacionesActionAdapter actionAdapter = new DiaRelacionesActionAdapter(this);
  DiaRelacionesTblActionAdapter tblActionAdapter = new
      DiaRelacionesTblActionAdapter(this);

  String titulo = "";
  protected CCargadorImagen imgs = null;

  public DiaRelaciones(CApp a, String title) {
    super(a);
    titulo = title;
    try {
      jbInit();
      pack();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public DiaRelaciones(CApp a, CLista lista, String title) {

    this(a, title);

    listaRet = lista;
    RellenaLista(lista);
  }

  void jbInit() throws Exception {
    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout.setHeight(295);
    xYLayout.setWidth(480);
    setSize(480, 295);

    setTitle(titulo);

    setLayout(xYLayout);
    this.setBackground(Color.lightGray);

    tbl.getList().setBackground(Color.white);
    tbl.getList().setHighlightColor(Color.lightGray);
    tbl.setInsets(new Insets(5, 5, 5, 5));
    tbl.setColumnButtons(JCUtilConverter.toStringList(new String(
        "Fecha Notificación\nFecha Recepción\nR.\nNotif. Reales"), '\n'));
    tbl.setColumnResizePolicy(1);
    tbl.setColumnWidths(JCUtilConverter.toIntList(new String(
        "135\n135\n40\n110"), '\n'));
    tbl.setNumColumns(4);
    tbl.setScrollbarDisplay(3);
    tbl.setAutoSelect(true);
    this.add(tbl, new XYConstraints(15, 15, 450, 235));
    this.add(btnAceptar, new XYConstraints(283, 255, 80, -1));
    this.add(btnCancelar, new XYConstraints(380, 255, 80, -1));

    btnAceptar.setLabel("Aceptar");
    btnCancelar.setLabel("Cancelar");
    btnAceptar.setActionCommand("aceptar");
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setActionCommand("cancelar");
    btnCancelar.setImage(imgs.getImage(1));

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);
    tbl.addActionListener(tblActionAdapter);
  }

  public void RellenaLista(CLista lista) {
    Object componente;
    for (int i = 0; i < lista.size(); i++) {
      componente = lista.elementAt(i);
      addRow( (DatTubNotif) componente);
    }
  }

  //public void addRow(int modo, DataListaNotifEDO data, int iSel) {
  private void addRow(DatTubNotif data) {
    int iImage = 0;

    sResSem = data.getResSem().trim();
    sFechaNotif = data.getFechaNotif();
    sFechaRecep = data.getFechaRecep();
    sNotifReales = data.getNotifReales();

    JCVector row1 = new JCVector();

    if (sResSem.equals("S")) {
      iImage = 2;
    }
    else {
      iImage = 3;

    }
    row1.addElement(sFechaNotif);
    row1.addElement(sFechaRecep);
    row1.addElement(imgs.getImage(iImage));
    row1.addElement(sNotifReales);

    items.addElement(row1);
    listaTbl.addElement(data);
    tbl.setItems(items);
  }

  // Para devolver todos los valores actualmente cargados
  public CLista getLineas() {
    return listaTbl;
  }

  public Object getComponente() {
    Object o = null;

    if (iOut != -1) {
      o = listaTbl.elementAt(tbl.getSelectedIndex());

    }
    return o;
  }

  void tbl_actionPerformed() {
    btnAceptar_actionPerformed();
  }

  void btnAceptar_actionPerformed() {
    iOut = 0;
    dispose();
  }

  void btnCancelar_actionPerformed() {
    iOut = -1;
    dispose();
  }

}

// escuchador de los click en los botones
class DiaRelacionesActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaRelaciones adaptee;
  ActionEvent evt;

  DiaRelacionesActionAdapter(DiaRelaciones adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
}

// escuchador de los click en la tabla
class DiaRelacionesTblActionAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  DiaRelaciones adaptee;

  DiaRelacionesTblActionAdapter(DiaRelaciones adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.tbl_actionPerformed();
  }
}
