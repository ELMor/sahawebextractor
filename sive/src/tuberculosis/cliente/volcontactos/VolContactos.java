//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales
//              (incluidas preguntas del protocolo)
//              de una enfermedad a fichero ASCII.

package tuberculosis.cliente.volcontactos;

import java.util.ResourceBundle;

import capp.CApp;

public class VolContactos
    extends CApp {

  ResourceBundle res;

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("tuberculosis.cliente.volcontactos.Res" +
                                   this.getIdioma());
    setTitulo("Volcado de Contactos de Casos de Tuberculosis");
    CApp a = (CApp)this;
    PanelListaPreg listaPreg = new PanelListaPreg(a);

    listaPreg.setBorde(false);

    if (this.getTSive().equals("T")) {
      PanelConsVolCasosIndTub consVol = new PanelConsVolCasosIndTub(a,
          listaPreg);
      consVol.setBorde(false);

      VerPanel("Contactos de Casos de Tuberculosis", consVol, true);
      VerPanel(res.getString("msg20.Text"), listaPreg, true);
      VerPanel("Contactos de Casos de Tuberculosis");
    }

  }

}
