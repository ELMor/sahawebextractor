package tuberculosis.cliente.panconflicto;

import capp.CApp;

public class appPanconflicto
    extends CApp {

  public void init() {
    super.init();
    setTitulo("Resolución de conflictos en preguntas de protocolo");
  }

  public void start() {
    CApp a = (CApp)this;
    PanConflictos panConflictos = new PanConflictos(a);
    VerPanel("", panConflictos);
  }

}
