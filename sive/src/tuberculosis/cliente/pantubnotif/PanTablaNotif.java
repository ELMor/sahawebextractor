/**
 * Clase: PanTablaNotif
 * Paquete: tuberculosis.cliente.pantubnotif
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT) / Emilio Postigo Riancho
 * Fecha Inicio: 07/10/1999 / 18/11/1999
 * Descripcion: Implementacion del panel que permite visualizar y navegar
    entre los diferentes declarantes de un caso
 */

package tuberculosis.cliente.pantubnotif;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import comun.Common;
import comun.UtilEDO;
import comun.constantes;
//PDP 16/03/2000
import infproto.DataProtocolo;
import jclass.util.JCVector;
import tuberculosis.cliente.diaprot.DiaProt;
import tuberculosis.cliente.diaresul.DiaOtrosMuestra;
import tuberculosis.cliente.diaresul.DiaOtrosTratamientos;
import tuberculosis.cliente.diatuber.DialogTub;
import tuberculosis.cliente.diatubnotif.DiaNotificaciones;
import tuberculosis.datos.notiftub.DatNotifTub;
import tuberculosis.datos.panmues.DatCasMuesCS;
import tuberculosis.datos.pantrat.DatCasTratCS;

public class PanTablaNotif
    extends CPanel {

  XYLayout lyXYLayout = new XYLayout();

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior = 0;

  // Estados posibles del panel
  protected final int modoESPERA = constantes.modoESPERA;
  protected final int modoALTA = constantes.modoALTA;
  protected final int modoMODIFICACION = constantes.modoMODIFICACION;
  protected final int modoBAJA = constantes.modoBAJA;
  protected final int modoCONSULTA = constantes.modoCONSULTA;

  // Variable estado del panel
  protected int modoOperacion = 0;

  // Variables que almacenan los diferentes permisos del usuario
  private boolean bAlta = false;
  private boolean bBaja = false;
  private boolean bModificacion = false;

  // Controles para la navegacion entre los elementos de la tabla
  public ButtonControl btnAlta = null;
  public ButtonControl btnModificar = null;
  public ButtonControl btnBaja = null;
  public ButtonControl btnResul = null; //PDP 15/03/2000
  public ButtonControl btnEvol = null; //PDP 15/03/2000
  public ButtonControl btnProtocolo = null; //PDP 15/03/2000
  ButtonControl btnTodas = null;
  ButtonControl btnValidar = null;
  ButtonControl btnValidadas = null;
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  // Variable que almacena el n� de caso EDO o '' si no se dispone
  // de este valor
  private String sNM_EDO = "";

  //Datos utilizados para Resultados y Evoluciones
  DatCasMuesCS datNotifM = null;
  DatCasTratCS datNotifT = null;

  /*
     //Dato utilizado para el di�logo de protocolo
     Public String sNotif = "";
   */

  // Tabla que visualiza y permite navegar entre los notificadores
  public CTabla tabla = new CTabla();

  // Vector que almacena las hashtable's que llegan para rellenar
  // el panel
  private Vector vNotificadores = new Vector();

  // Vector que almacena las hashtable's que llegan para rellenar
  // el panel con los notificadores que se est�n dando de alta
  //private Vector vNuevosNotificadores = new Vector();
  // modificacion
  // hago publica esta variable para poder acceder desde dilogtub
  public Vector vNuevosNotificadores = new Vector();

  // modificacion 06/07/2000
  public Vector vNuevosNotificadoresaux = new Vector();

  // Cargadores de im�genes visibles en todo el m�dulo
  private CCargadorImagen imgs_declarantes = null;

  public CApp capp;

  DialogTub dlgb = null;
  Hashtable hsCompleta = new Hashtable();

  Vector vTotal = null;
  // Constructor
  public PanTablaNotif(DialogTub dlg, int modo, Hashtable has) {
    try {

      capp = dlg.getCApp();
      this.app = capp;
      hsCompleta = (Hashtable) has.clone();

      dlgb = dlg;

      bAlta = true; //app.getIT_AUTALTA().equals("S");
      bBaja = true; //app.getIT_AUTBAJA().equals("S");
      bModificacion = true; //app.getIT_AUTMOD().equals("S");

      // Tener en cuenta modo consulta

      modoOperacion = modo;
      jbInit(); //hay un Inicializar dentro!
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicializacion en la creacion de un objeto
  void jbInit() throws Exception {
    final String imgNAME_mantenimiento[] = {
        "images/alta2.gif",
        "images/modificacion2.gif",
        "images/baja2.gif"};

    final String imgNAME_tabla[] = {
        "images/primero.gif",
        "images/anterior.gif",
        "images/siguiente.gif",
        "images/ultimo.gif"};

    final String imgNAME_declarantes[] = {
        "images/declaracion2.gif"};

    // Carga de imagenes
    CCargadorImagen imgs_mantenimiento = new CCargadorImagen(app,
        imgNAME_mantenimiento);
    imgs_mantenimiento.CargaImagenes();
    CCargadorImagen imgs_tabla = new CCargadorImagen(app, imgNAME_tabla);
    imgs_tabla.CargaImagenes();

    imgs_declarantes = new CCargadorImagen(app, imgNAME_declarantes);
    imgs_declarantes.CargaImagenes();

    // Medidas del panel
    this.setSize(new Dimension(770, 315 + 100));
    lyXYLayout.setHeight(305 + 100);
    lyXYLayout.setWidth(770);
    this.setLayout(lyXYLayout);
    setBorde(false);

    btnPrimero = new ButtonControl(imgs_tabla.getImage(0));
    btnAnterior = new ButtonControl(imgs_tabla.getImage(1));
    btnSiguiente = new ButtonControl(imgs_tabla.getImage(2));
    btnUltimo = new ButtonControl(imgs_tabla.getImage(3));

    btnAlta = new ButtonControl(imgs_mantenimiento.getImage(0));
    btnModificar = new ButtonControl(imgs_mantenimiento.getImage(1));
    btnBaja = new ButtonControl(imgs_mantenimiento.getImage(2));
    btnResul = new ButtonControl();
    btnEvol = new ButtonControl();
    btnProtocolo = new ButtonControl();

    // Se establecen los parametros de la tabla
    PintarTabla();

    this.add(tabla, new XYConstraints(5, 15, 745, 280));

    this.add(btnAlta, new XYConstraints(10, 155 + 160, 25, 25));
    this.add(btnModificar, new XYConstraints(40, 155 + 160, 25, 25));
    this.add(btnBaja, new XYConstraints(70, 155 + 160, 25, 25));
    this.add(btnResul, new XYConstraints(130, 155 + 160, 80, 25));
    this.add(btnEvol, new XYConstraints(220, 155 + 160, 80, 25));
    this.add(btnProtocolo, new XYConstraints(310, 155 + 160, 80, 25));
    this.add(btnPrimero, new XYConstraints(505 + 105, 155 + 160, 25, 25));
    this.add(btnAnterior, new XYConstraints(545 + 105, 155 + 160, 25, 25));
    this.add(btnSiguiente, new XYConstraints(583 + 105, 155 + 160, 25, 25));
    this.add(btnUltimo, new XYConstraints(621 + 105, 155 + 160, 25, 25));

    // Gesti�n de eventos

    // Botones de la tabla
    btnTablaActionListener alBotonesTabla = new btnTablaActionListener(this);

    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");

    btnPrimero.addActionListener(alBotonesTabla);
    btnAnterior.addActionListener(alBotonesTabla);
    btnSiguiente.addActionListener(alBotonesTabla);
    btnUltimo.addActionListener(alBotonesTabla);

    // Botones A�ADIR, MODIFICAR, BORRAR, RESULTADO, EVOLUCION, PROTOCOLO
    btnAMBActionListener alBotonesAMB = new btnAMBActionListener(this);

    btnAlta.setActionCommand("alta");
    btnModificar.setActionCommand("modificar");
    btnBaja.setActionCommand("baja");
    btnResul.setActionCommand("resultado");
    btnEvol.setActionCommand("evolucion");
    btnProtocolo.setActionCommand("protocolo");
    btnResul.setLabel("Resultados");
    btnEvol.setLabel("Evoluciones");
    btnProtocolo.setLabel("Protocolo");

    btnAlta.addActionListener(alBotonesAMB);
    btnModificar.addActionListener(alBotonesAMB);
    btnBaja.addActionListener(alBotonesAMB);
    btnResul.addActionListener(alBotonesAMB);
    btnEvol.addActionListener(alBotonesAMB);
    btnProtocolo.addActionListener(alBotonesAMB);

    // Tabla de notificadores
    jcalTablaActionListener jcalTabla = new jcalTablaActionListener(this);

    tabla.addActionListener(jcalTabla);

    TablaItemListener tilTabla = new TablaItemListener(this);
    tabla.addItemListener(tilTabla);

    // Se habilitan/deshabilitan los controles pertinentes
    modoAnterior = modoOperacion;

    //Inicializar(modoOperacion);
    Inicializar();

    // Fin de inicializaci�n: se restaura el puntero del rat�n
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // modificacion 10/07/2000
  public void ChangedProtocolo() {

    // variable para controlar que cuando se grabe un protocolo y
    // se pulse grabar, si estamos en el panel del protocolo, se
    // grabe dicho protocolo

    //dlgb.bGrabarProt = false;

    if (dlgb.bChanging) {
      if (Common.ShowPregunta(this.app,
                              "El protocolo ha cambiado. �Desea grabarlo?")) {

        //dlgb.grabarProtocolo();
        dlgb.tabPanel.VerPanel("Protocolo Notificador");

        dlgb.bChanging = false;
      }
      else {
        dlgb.bChanging = false;
      }
    }
  }

  //
  protected boolean esNivel1Autorizado(String cd_nivel_1) {
    boolean bValor = false;

    Vector vNivel1 = this.app.getCD_NIVEL_1_AUTORIZACIONES();

    for (int i = 0; i < vNivel1.size() && bValor == false; i++) {
      bValor = ( (String) vNivel1.elementAt(i)).equals(cd_nivel_1);
    }

    return bValor;
  }

  //
  protected boolean esNivel2Autorizado(String cd_nivel_2) {
    boolean bValor = false;

    Vector vNivel2 = this.app.getCD_NIVEL_2_AUTORIZACIONES();

    for (int i = 0; i < vNivel2.size() && bValor == false; i++) {
      bValor = ( (String) vNivel2.elementAt(i)).equals(cd_nivel_2);
    }

    return bValor;
  }

  // Devuelve true si el usuario tiene permisos sobre la fila
  // seleccionada (false en caso contrario)
  protected boolean hayPermiso() {
    boolean bValor = true;

    int iPerfil = this.app.getPerfil();

    if (iPerfil == 3 || iPerfil == 4) {
      bValor = esNivel1Autorizado(getNivel1Gestion());
      if (bValor && iPerfil == 4) {
        bValor = esNivel2Autorizado(getNivel2Gestion());
      }
    }

    return bValor;
  }

  // Para recuperar el c�digo de nivel 1 del elemento seleccionado de la tabla
  protected String getNivel1Gestion() {
    String s = null;
    DatNotifTub dnt = getNotificador();

    s = dnt.getCD_NIVEL_1();

    return s;
  }

  // Para recuperar el c�digo de nivel 2 del elemento seleccionado de la tabla
  protected String getNivel2Gestion() {
    String s = null;

    DatNotifTub dnt = getNotificador();

    s = dnt.getCD_NIVEL_2();

    return s;
  }

  // Bloquear eventos
  /** este m�todo sirve para mantener una sincronizaci�n en los eventos */
  /* protected synchronized  boolean bloquea(){
      if (sinBloquear) {
          // no hay nadie bloqueando pasamos a bloquear
          sinBloquear = false;
          modoAnterior = modoOperacion;
          modoOperacion = modoESPERA;
          Inicializar(modoOperacion);
          setCursor(new Cursor(Cursor.WAIT_CURSOR));
          return true;
      }else {
          // ya est� bloqueado
          return false;
      }
   } */

  /** este m�todo desbloquea el sistema */
  /* public synchronized void desbloquea(){
      sinBloquear = true;
      modoOperacion  = modoAnterior;
      Inicializar(modoOperacion);
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
   } */

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
    if (tabla.countItems() <= 0) {
      btnModificar.setEnabled(false);
      btnBaja.setEnabled(false);
      btnResul.setEnabled(false);
      btnEvol.setEnabled(false);
      btnProtocolo.setEnabled(false);
    }
  }

  // Implementacion del metodo abstracto de CPanel
  public void Inicializar() {
    switch (modoOperacion) {
      case modoESPERA:
        tabla.setEnabled(false);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);
        btnResul.setEnabled(false);
        btnEvol.setEnabled(false);
        btnProtocolo.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
      case modoALTA:
        if (tabla.countItems() == 0) {
          tabla.setEnabled(true);
          btnAlta.setEnabled(true && bAlta);
          btnModificar.setEnabled(true && bModificacion);
          btnBaja.setEnabled(true && bBaja);
          btnResul.setEnabled(true);
          btnEvol.setEnabled(true);
          btnProtocolo.setEnabled(true);
          btnPrimero.setEnabled(true);
          btnAnterior.setEnabled(true);
          btnSiguiente.setEnabled(true);
          btnUltimo.setEnabled(true);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        else { //para v.0 no se puede borrar el notificador metido
          tabla.setEnabled(false);
          btnAlta.setEnabled(false);
          btnModificar.setEnabled(false);
          btnBaja.setEnabled(false);
          btnResul.setEnabled(false);
          btnEvol.setEnabled(false);
          btnProtocolo.setEnabled(false);
          btnPrimero.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        break;
      case modoMODIFICACION:
        tabla.setEnabled(true);
        btnAlta.setEnabled(true && bAlta);
        btnModificar.setEnabled(true && bModificacion);
        btnBaja.setEnabled(true && bBaja);
        btnResul.setEnabled(true);
        btnEvol.setEnabled(true);
        btnProtocolo.setEnabled(true);
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case modoBAJA:
      case modoCONSULTA:
        tabla.setEnabled(true);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(true);
        btnPrimero.setEnabled(true);
        btnBaja.setEnabled(false);
        btnResul.setEnabled(true);
        btnEvol.setEnabled(true);
        btnProtocolo.setEnabled(true);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  // Establece el numero de columnas de la tabla y las cabeceras de las mismas
  public void PintarTabla() {
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "69\n140\n50\n165\n60\n146\n48\n47"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(
        "A�o-Sem\nF.Notif-F.Recep\nEquipo\nCentro\nZonas\nDeclarante\nFuente\nPrim"),
        '\n'));
    tabla.setNumColumns(8);

  } //fin PintarTabla()

  // Implementaci�n del m�todo de rellenado
  public void rellena(Hashtable data) {
    DatNotifTub dntDatos = null;

    vNotificadores = new Vector();

    // Carga el vector de notificadores actuales
    if (data != null) {
      for (Enumeration e = data.keys(); e.hasMoreElements(); ) {
        dntDatos = (DatNotifTub) data.get( (String) e.nextElement());
        vNotificadores.addElement(dntDatos);
      }
    }

    if (vNotificadores.size() > 0) {
      sNM_EDO = (String) dntDatos.getNM_EDO();
    }
    else {
      sNM_EDO = "";
    }

    cargaTabla();

  }

  //
  private void actualizaNotificadores(DatNotifTub dnt) {
    DatNotifTub dntTemp = null;

    // En los viejos
    for (int i = 0; i < vNotificadores.size(); i++) {
      dntTemp = (DatNotifTub) vNotificadores.elementAt(i);
      // Si es el mismo equipo ...
      if (dntTemp.getCD_E_NOTIF_EDOI().equals(dnt.getCD_E_NOTIF_EDOI())) {
        dntTemp.setCD_OPE_EQ(dnt.getCD_OPE_EQ());
        dntTemp.setFC_ULTACT_EQ(dnt.getFC_ULTACT_EQ());

        if (dnt.getIT_RESSEM().equals("S") &&
            dnt.getCD_FUENTE().equals(constantes.FUENTE_EDO)) {
          dntTemp.setNM_NOTIFT_EQ(dnt.getNM_NOTIFT_EQ().toString());
        }

        // Si adem�s son iguales el a�o y la semana...
        if (dntTemp.getCD_ANOEPI_EDOI().equals(dnt.getCD_ANOEPI_EDOI())
            && dntTemp.getCD_SEMEPI().equals(dnt.getCD_SEMEPI())) {

          dntTemp.setCD_OPE_SEM(dnt.getCD_OPE_SEM());
          dntTemp.setFC_ULTACT_SEM(dnt.getFC_ULTACT_SEM());

          if (dnt.getIT_RESSEM().equals("S") &&
              dnt.getCD_FUENTE().equals(constantes.FUENTE_EDO)) {
            dntTemp.setNM_NNOTIFT_SEM(dnt.getNM_NNOTIFT_SEM());
            dntTemp.setNM_NTOTREAL_SEM(dnt.getNM_NTOTREAL_SEM());
          }
        }
      }
    }
    // En los nuevos
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      dntTemp = (DatNotifTub) vNuevosNotificadores.elementAt(i);
      // Si es el mismo equipo ...
      if (dntTemp.getCD_E_NOTIF_EDOI().equals(dnt.getCD_E_NOTIF_EDOI())) {
        dntTemp.setCD_OPE_EQ(dnt.getCD_OPE_EQ());
        dntTemp.setFC_ULTACT_EQ(dnt.getFC_ULTACT_EQ());

        dntTemp.setNM_NNOTIFT_SEM(dnt.getNM_NNOTIFT_SEM());
        // Si adem�s son iguales el a�o y la semana...
        if (dntTemp.getCD_ANOEPI_EDOI().equals(dnt.getCD_ANOEPI_EDOI())
            && dntTemp.getCD_SEMEPI().equals(dnt.getCD_SEMEPI())) {

          dntTemp.setCD_OPE_SEM(dnt.getCD_OPE_SEM());
          dntTemp.setFC_ULTACT_SEM(dnt.getFC_ULTACT_SEM());

          dntTemp.setNM_NTOTREAL_SEM(dnt.getNM_NTOTREAL_SEM());
        }
      }
    }

  }

  // Para establecer el nuevo primer notificador despu�s de una baja
  // Se chequean s�lo los campos de la clave
  private void establecePrimero(DatNotifTub dnt) {
    boolean bEncontrado = false;
    for (int i = 0; i < vNotificadores.size() && !bEncontrado; i++) {
      if ( ( (DatNotifTub) vNotificadores.elementAt(i)).contieneClave(dnt)) {
        ( (DatNotifTub) vNotificadores.elementAt(i)).setIT_PRIMERO("S");
        ( (DatNotifTub) vNotificadores.elementAt(i)).setCD_OPE_EDOI(dnt.
            getCD_OPE_EDOI());
        ( (DatNotifTub) vNotificadores.elementAt(i)).setFC_ULTACT_EDOI(dnt.
            getFC_ULTACT_EDOI());
        bEncontrado = true;
      }
    }

    for (int i = 0; i < vNuevosNotificadores.size() && !bEncontrado; i++) {
      if ( ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).contieneClave(dnt)) {
        ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).setIT_PRIMERO("S");
        ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).setCD_OPE_EDOI(dnt.
            getCD_OPE_EDOI());
        ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).setFC_ULTACT_EDOI(
            dnt.getFC_ULTACT_EDOI());
        bEncontrado = true;
      }
    }
  }

  // Para visualizar mensajes
  private void showMensaje(String sMensaje) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMensaje);
    msgBox.show();
    msgBox = null;
  }

  // Carga la tabla a partir de los datos de los dos
  // vectores de notificadores
  private void cargaTabla() {
    JCVector jcvTabla = new JCVector();
    JCVector jcvLinea = null;

    DatNotifTub dntDatos = null;

    tabla.clear();

    for (int i = 0; i < vNotificadores.size(); i++) {
      dntDatos = (DatNotifTub) vNotificadores.elementAt(i);

      jcvLinea = new JCVector();

      jcvLinea.addElement(dntDatos.getCD_ANOEPI_EDOI() + "-" +
                          dntDatos.getCD_SEMEPI());
      jcvLinea.addElement(dntDatos.getFC_NOTIF() + "-" +
                          dntDatos.getFC_RECEP_EDOI());
      jcvLinea.addElement(dntDatos.getCD_E_NOTIF_EDOI());
      jcvLinea.addElement(dntDatos.getDS_CENTRO());
      jcvLinea.addElement(dntDatos.getCD_NIVEL_1() + "-" +
                          dntDatos.getCD_NIVEL_2() + "-" + dntDatos.getCD_ZBS());
      jcvLinea.addElement(dntDatos.getDS_DECLARANTE());
      jcvLinea.addElement(dntDatos.getCD_FUENTE());
      // Icono si es el primero
      if (dntDatos.getIT_PRIMERO().equals("S")) {
        jcvLinea.addElement(imgs_declarantes.getImage(0));
      }
      jcvTabla.addElement(jcvLinea);
    }

    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      dntDatos = (DatNotifTub) vNuevosNotificadores.elementAt(i);

      jcvLinea = new JCVector();

      jcvLinea.addElement(dntDatos.getCD_ANOEPI_EDOI() + "-" +
                          dntDatos.getCD_SEMEPI());
      jcvLinea.addElement(dntDatos.getFC_NOTIF() + "-" +
                          dntDatos.getFC_RECEP_EDOI());
      jcvLinea.addElement(dntDatos.getCD_E_NOTIF_EDOI());
      jcvLinea.addElement(dntDatos.getDS_CENTRO());
      jcvLinea.addElement(dntDatos.getCD_NIVEL_1() + "-" +
                          dntDatos.getCD_NIVEL_2() + "-" + dntDatos.getCD_ZBS());
      jcvLinea.addElement(dntDatos.getDS_DECLARANTE());
      jcvLinea.addElement(dntDatos.getCD_FUENTE());
      // Icono si es el primero
      if (dntDatos.getIT_PRIMERO().equals("S")) {
        jcvLinea.addElement(imgs_declarantes.getImage(0));
      }
      jcvTabla.addElement(jcvLinea);
    }

    tabla.setItems(jcvTabla);
    iluminarITPrimero();
  }

  private boolean bNotificadorAutorizado() {
    return hayPermiso();
  }

  public Vector getVectorNotificadores() {
    vTotal = new Vector();
    for (int i = 0; i < vNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNotificadores.elementAt(i));
    }
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNuevosNotificadores.elementAt(i));
    }
    return (vTotal);
  }

  public String anoPrimero() {

    vTotal = new Vector();
    DatNotifTub dnt = null;

    for (int i = 0; i < vNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNotificadores.elementAt(i));
    }
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNuevosNotificadores.elementAt(i));
    }
    //busco el primero
    for (int i = 0; i < vTotal.size(); i++) {
      if ( ( ( (DatNotifTub) vTotal.elementAt(i)).getIT_PRIMERO().equals("S"))) {
        dnt = (DatNotifTub) vTotal.elementAt(i);
        return (dnt.getCD_ANOEPI_EDOI());
      }
    }
    return ("");
  }

  public String semanaPrimero() {

    vTotal = new Vector();
    DatNotifTub dnt = null;

    for (int i = 0; i < vNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNotificadores.elementAt(i));
    }
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNuevosNotificadores.elementAt(i));
    }
    //busco el primero
    for (int i = 0; i < vTotal.size(); i++) {
      if ( ( ( (DatNotifTub) vTotal.elementAt(i)).getIT_PRIMERO().equals("S"))) {
        dnt = (DatNotifTub) vTotal.elementAt(i);
        return (dnt.getCD_SEMEPI());
      }
    }
    return ("");
  }

  public String fnotifPrimero() {
    vTotal = new Vector();
    DatNotifTub dnt = null;

    for (int i = 0; i < vNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNotificadores.elementAt(i));
    }
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNuevosNotificadores.elementAt(i));
    }
    //busco el primero
    for (int i = 0; i < vTotal.size(); i++) {
      if ( ( ( (DatNotifTub) vTotal.elementAt(i)).getIT_PRIMERO().equals("S"))) {
        dnt = (DatNotifTub) vTotal.elementAt(i);
        return (dnt.getFC_NOTIF());
      }
    }
    return ("");
  }

  public String frecepPrimero() {
    vTotal = new Vector();
    DatNotifTub dnt = null;

    for (int i = 0; i < vNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNotificadores.elementAt(i));
    }
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNuevosNotificadores.elementAt(i));
    }
    //busco el primero
    for (int i = 0; i < vTotal.size(); i++) {
      if ( ( ( (DatNotifTub) vTotal.elementAt(i)).getIT_PRIMERO().equals("S"))) {
        dnt = (DatNotifTub) vTotal.elementAt(i);
        return (dnt.getFC_RECEP_EDOI());
      }
    }
    return ("");
  }

  private void iluminarITPrimero() {
    int iLugar = 0;
    vTotal = new Vector();
    DatNotifTub dnt = null;

    if (vNotificadores.size() == 0) {
      return;
    }

    for (int i = 0; i < vNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNotificadores.elementAt(i));
    }
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      vTotal.addElement( (DatNotifTub) vNuevosNotificadores.elementAt(i));
    }
    //busco el primero
    for (int i = 0; i < vTotal.size(); i++) {
      if ( ( ( (DatNotifTub) vTotal.elementAt(i)).getIT_PRIMERO().equals("S"))) {
        iLugar = i;
        dnt = (DatNotifTub) vTotal.elementAt(i);
        break;
      }
    }
    //Ilumino i
    tabla.select(iLugar);

    // Escribo i en el panel superior
    dlgb.pansup.setNotificador("Notificador: " +
                               dnt.getCD_ANOEPI_EDOI() + "-" + dnt.getCD_SEMEPI() +
                               " " +
                               dnt.getFC_NOTIF() + "-" + dnt.getFC_RECEP_EDOI() +
                               " " +
                               dnt.getCD_E_NOTIF_EDOI() + " " +
                               dnt.getDS_CENTRO() + " " +
                               dnt.getCD_NIVEL_1() + "-" + dnt.getCD_NIVEL_2() +
                               "-" + dnt.getCD_ZBS() + " " +
                               dnt.getDS_DECLARANTE() + " " +
                               dnt.getCD_FUENTE());

    // Ver si hay permisos o no para el 1er. notificador
    // Si procede, pasar a modo consulta el di�logo completo
    if (!bNotificadorAutorizado()) {

    }
  }

  // M�todos gestores de eventos
  // Tabla
  protected void btnTablaPrimero() {
    tabla.select(0);
    tabla.setTopRow(0);
  }

  protected void btnTablaAnterior() {
    int index = tabla.getSelectedIndex();
    if (index > 0) {
      index--;
      tabla.select(index);
      if (index - tabla.getTopRow() <= 0) {
        tabla.setTopRow(tabla.getTopRow() - 1);
      }
    }
    else {
      tabla.select(0);
      tabla.setTopRow(0);
    }
  }

  protected void btnTablaSiguiente() {
    int ultimo = tabla.countItems() - 1;
    int index = tabla.getSelectedIndex();
    if (index < ultimo && index >= 0) {
      index++;
      tabla.select(index);
      if (index - tabla.getTopRow() >= 4) {
        tabla.setTopRow(tabla.getTopRow() + 1);
      }
    }
    else {
      if (index != ultimo) {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  }

  protected void btnTablaUltimo() {
    int index = 0;
    int ultimo = tabla.countItems() - 1;
    if (ultimo >= 0) {
      index = ultimo;
      tabla.select(index);
      if (tabla.countItems() >= 5) {
        tabla.setTopRow(tabla.countItems() - 5);
      }
      else {
        tabla.setTopRow(0);
      }
    }
  }

  // Para obtener el notificador con fecha de notificaci�n
  // menor.
  private void ponPrimerNotificador() {
    String sFechaAntiguos = null;
    String sFechaNuevos = null;

    Vector vActual = null;
    String fFechaMenor = null;

    sFechaAntiguos = menorFecha(vNotificadores);
    sFechaNuevos = menorFecha(vNuevosNotificadores);

    if (UtilEDO.fecha1MayorqueFecha2(sFechaNuevos, sFechaAntiguos)) {
      fFechaMenor = sFechaAntiguos;
      vActual = vNotificadores;
    }
    else {
      fFechaMenor = sFechaNuevos;
      vActual = vNuevosNotificadores;
    }

    for (int i = 0; i < vActual.size(); i++) {
      if ( ( (DatNotifTub) vActual.elementAt(i)).getFC_NOTIF().equals(
          fFechaMenor)) {
        ( (DatNotifTub) vActual.elementAt(i)).setIT_PRIMERO("S");
        break;
      }
    }

  } // FIN ponPrimerNotificador ***************************

  private String menorFecha(Vector v) {
    Vector vFechas = v;
    String dMinima = "01/01/9000";

    for (int i = 0; i < vFechas.size(); i++) {
      if (UtilEDO.fecha1MayorqueFecha2(dMinima,
                                       ( (DatNotifTub) vFechas.elementAt(i)).
                                       getFC_NOTIF())) {
        dMinima = ( (DatNotifTub) vFechas.elementAt(i)).getFC_NOTIF();
      }
    }

    return dMinima;
  } // FIN menorFecha *************************************

  //

  // Actualiza los datos de bloqueo del notificador seleccionado
  public void setDatosBloqueoNotificador(String sCdOpe, String sFcUltAct) {
    Vector vCual = null;
    int iIndice = -1;

    // �En qu� vector est� el elemento pulsado?
    if (tabla.getSelectedIndex() <= vNotificadores.size() - 1) {
      // Est� en la 'antigua'
      vCual = vNotificadores;
      iIndice = tabla.getSelectedIndex();
    }
    else {
      // Est� en la 'nueva'
      vCual = vNuevosNotificadores;
      iIndice = tabla.getSelectedIndex() - vNotificadores.size();
    }

    ( (DatNotifTub) vCual.elementAt(iIndice)).setCD_OPE_EDOI(sCdOpe);
    ( (DatNotifTub) vCual.elementAt(iIndice)).setFC_ULTACT_EDOI(sFcUltAct);
  }

  public void setEdoNotificadores(int iNmEdo) {
    //for (int i = 0; i < vNotificadores.size(); i++) {
    //  ((DatNotifTub) vNotificadores.elementAt(i)).setNM_EDO_EDOI(String.valueOf(iNmEdo));
    //}
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).setNM_EDO_EDOI(String.
          valueOf(iNmEdo));
    }
  }

  public void setCdOpe_FcUltAct_Notificadores(String sCdOpe, String sFcUltAct) {
    //for (int i = 0; i < vNotificadores.size(); i++) {
    //  ((DatNotifTub) vNotificadores.elementAt(i)).setNM_EDO_EDOI(String.valueOf(iNmEdo));
    //}
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).setCD_OPE_EDOI(sCdOpe);
      ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).setFC_ULTACT_EDOI(
          sFcUltAct);
    }
  }

  public void setCdOpe_FcUltAct_EDOIND_Notificadores(String sCdOpe,
      String sFcUltAct) {
    for (int i = 0; i < vNotificadores.size(); i++) {
      ( (DatNotifTub) vNotificadores.elementAt(i)).setCD_OPE_EDOIND(sCdOpe);
      ( (DatNotifTub) vNotificadores.elementAt(i)).setFC_ULTACT_EDOIND(
          sFcUltAct);
    }
    for (int i = 0; i < vNuevosNotificadores.size(); i++) {
      ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).setCD_OPE_EDOIND(
          sCdOpe);
      ( (DatNotifTub) vNuevosNotificadores.elementAt(i)).setFC_ULTACT_EDOIND(
          sFcUltAct);
    }
  }

  public DatNotifTub getNotificador() {
    Vector vCual = null;
    int iIndice = -1;

    // �En qu� vector est� el elemento pulsado?
    if (tabla.getSelectedIndex() <= vNotificadores.size() - 1) {
      // Est� en la 'antigua'
      vCual = vNotificadores;
      iIndice = tabla.getSelectedIndex();
    }
    else {
      // Est� en la 'nueva'
      vCual = vNuevosNotificadores;
      iIndice = tabla.getSelectedIndex() - vNotificadores.size();
    }

    return (DatNotifTub) vCual.elementAt(iIndice);

  }

  // click en la Tabla
  protected void clickTabla() {

    //if(dlgb.pProtocolo != null)
    //  dlgb.pProtocolo.recuperarProtocolo();

    DatNotifTub dnt = getNotificador();
    // Escribo i en el panel superior
    dlgb.pansup.setNotificador("Notificador: " +
                               dnt.getCD_ANOEPI_EDOI() + "-" + dnt.getCD_SEMEPI() +
                               " " +
                               dnt.getFC_NOTIF() + "-" + dnt.getFC_RECEP_EDOI() +
                               " " +
                               dnt.getCD_E_NOTIF_EDOI() + " " +
                               dnt.getDS_CENTRO() + " " +
                               dnt.getCD_NIVEL_1() + "-" + dnt.getCD_NIVEL_2() +
                               "-" + dnt.getCD_ZBS() + " " +
                               dnt.getDS_DECLARANTE() + " " +
                               dnt.getCD_FUENTE());

    bBaja = bNotificadorAutorizado();
    Inicializar();
  }

  // modificacion 11/07/2000
  public void setNM_EDO(String s) {
    sNM_EDO = "";
    if (s != null) {
      sNM_EDO = s.trim();
    }
  }

  // Acciones A�ADIR, MODIFICAR, BAJA
  protected void btnAlta() {

    /*// Durante las pruebas
        DiaNotificaciones diaNotificaciones = new DiaNotificaciones(this.app);
        Hashtable hsEnvioADialogo = new Hashtable();
        CLista clFuentes = new CLista();
        // Datos de Fuentes
        DataGeneralCDDS dgTemp = new DataGeneralCDDS("1", "TUB.");
        clFuentes.addElement(dgTemp);
        dgTemp = new DataGeneralCDDS("2", "EDO");
        clFuentes.addElement(dgTemp);
        dgTemp = new DataGeneralCDDS("3", "Otras");
        clFuentes.addElement(dgTemp);
        hsEnvioADialogo.put(constantes.FUENTESNOTIF, clFuentes);
        // A�o de alta
        hsEnvioADialogo.put(constantes.ANO_ALTA_DESDE_RT, "1999");
        // Vector con notificadores actuales
        hsEnvioADialogo.put("VECTOR_NOTIFICADORES", vNotificadores);
        // Vector con los notificadores nuevos
        hsEnvioADialogo.put("VECTOR_NUEVOS_NOTIFICADORES", vNuevosNotificadores);
        // n� de caso EDO
        hsEnvioADialogo.put(constantes.CASOEDO, sNM_EDO);
    */

   int modo = modoOperacion;
    dlgb.Inicializar(constantes.modoESPERA);

    // Proceso en s�
    DiaNotificaciones diaNotificaciones = new DiaNotificaciones(this.getApp(), true);
    //ANO  -------
    hsCompleta.put(constantes.ANO_ALTA_DESDE_RT, this.dlgb.pansup.getAno());
    // Vector con notificadores actuales
    hsCompleta.put("VECTOR_NOTIFICADORES", vNotificadores);
    // Vector con los notificadores nuevos
    hsCompleta.put("VECTOR_NUEVOS_NOTIFICADORES", vNuevosNotificadores);
    // n� de caso EDO
    hsCompleta.put(constantes.CASOEDO, sNM_EDO);

    // E 17/01/2000
    // Se deben eliminar aquellas claves que proceden de otras opciones y no deben
    // ser utilizadas en altas
    if (hsCompleta.containsKey(constantes.DATOSNOTIFICADOR)) {
      hsCompleta.remove(constantes.DATOSNOTIFICADOR);
    }

    //-----------------------------------------------------
    diaNotificaciones.rellena(hsCompleta);
    diaNotificaciones.ponerModo(constantes.modoALTA);

    diaNotificaciones.show();

    // Ahora debe recargarse la tabla a partir de vNotificadores,
    // si se ha pulsado Aceptar en el di�logo
    DatNotifTub nuevaFila = (DatNotifTub) diaNotificaciones.getComponente();

    if (nuevaFila != null) {
      if (vNotificadores.size() == 0 && vNuevosNotificadores.size() == 0) {
        nuevaFila.setIT_PRIMERO("S");
      }
      else {
        nuevaFila.setIT_PRIMERO("N");
      }

      vNuevosNotificadores.addElement(nuevaFila);

      // modificacion 06/07/2000
      // sirve para gestionar los nuevos notificadores
      // en el panel_informe_completo
      vNuevosNotificadoresaux.addElement(nuevaFila);

      actualizaNotificadores(nuevaFila);
      cargaTabla();
      btnTablaUltimo(); //lo seleccionamos
      dlgb.pansup.setNotificador("Notificador: " +
                                 nuevaFila.getCD_ANOEPI_EDOI() + "-" +
                                 nuevaFila.getCD_SEMEPI() + "  " +
                                 nuevaFila.getFC_NOTIF() + "-" +
                                 nuevaFila.getFC_RECEP_EDOI() + "  " +
                                 nuevaFila.getCD_E_NOTIF_EDOI() + "  " +
                                 nuevaFila.getDS_CENTRO() + "  " +
                                 nuevaFila.getCD_ZBS() + "  " +
                                 nuevaFila.getDS_DECLARANTE() + "  " +
                                 nuevaFila.getCD_FUENTE());
    }

    diaNotificaciones = null;

    dlgb.Inicializar(modo);

    // tabPanel.InsertarPanel("Notificadores", panTabla);

  }

  protected void btnModificar() {
    Vector vCual = null;
    int iIndice = -1;

    int modo = modoOperacion;
    dlgb.Inicializar(constantes.modoESPERA);

    // Si no hay nada seleccionado, se ignora la pulsaci�n
    if (tabla.getSelectedIndex() < 0) {
      return;
    }

    // �En qu� vector est� el elemento pulsado?
    if (tabla.getSelectedIndex() <= vNotificadores.size() - 1) {
      // Est� en la 'antigua'
      vCual = vNotificadores;
      iIndice = tabla.getSelectedIndex();
    }
    else {
      // Est� en la 'nueva'
      vCual = vNuevosNotificadores;
      iIndice = tabla.getSelectedIndex() - vNotificadores.size();
    }

    // A�o de alta
    // se obtiene del vector

    // n� de caso EDO
    hsCompleta.put(constantes.CASOEDO, sNM_EDO);

    // Datos de ese notificador
    hsCompleta.put(constantes.DATOSNOTIFICADOR,
                   (DatNotifTub) vCual.elementAt(iIndice));

    // Proceso en s�
    DiaNotificaciones diaNotificaciones = new DiaNotificaciones(this.app, false);

    diaNotificaciones.rellena(hsCompleta);

    // Puede ser modo modificaci�n o consulta
    diaNotificaciones.ponerModo( (!bNotificadorAutorizado() ||
                                  modo == constantes.modoBAJA ||
                                  modo == constantes.modoCONSULTA) ?
                                constantes.modoCONSULTA :
                                constantes.modoMODIFICACION);

    diaNotificaciones.show();

    // Ahora debe recargarse la tabla a partir de vNotificadores,
    // si se ha pulsado Aceptar en el di�logo
    DatNotifTub nuevaFila = (DatNotifTub) diaNotificaciones.getComponente();

    if (nuevaFila != null) {
      vCual.setElementAt(nuevaFila, iIndice);
      actualizaNotificadores(nuevaFila);
      cargaTabla();
    }

    diaNotificaciones = null;

    dlgb.Inicializar(modo);
  }

  public void btnResul() {
    int modo = modoOperacion;
    int modoApertura = modo;

    if (!bNotificadorAutorizado()) {
      modoApertura = constantes.modoCONSULTA;
    }

    dlgb.Inicializar(constantes.modoESPERA);

    setDatosNotificadorM(getNotificador());

    DiaOtrosMuestra diaMuestras = new DiaOtrosMuestra(this, modoApertura,
        hsCompleta);

    diaMuestras.addPanelMuestras();

    diaMuestras.show();

    // Se actualizan los datos de bloqueo del notificador seleccionado
    datNotifM = diaMuestras.getComponente();
    setDatosBloqueoNotificador(datNotifM.getCD_OPE(), datNotifM.getFC_ULTACT());

    if (diaMuestras.iOut == -1) { //cancelar
      diaMuestras = null;
    }
    else { //aceptar
      diaMuestras = null;
    }

    dlgb.Inicializar(modo);

  }

  public void btnEvol() {
    int modo = modoOperacion;
    int modoApertura = modo;

    if (!bNotificadorAutorizado()) {
      modoApertura = constantes.modoCONSULTA;
    }

    dlgb.Inicializar(constantes.modoESPERA);

    setDatosNotificadorT(getNotificador());

    DiaOtrosTratamientos diaTratamientos = new DiaOtrosTratamientos(this,
        modoApertura, hsCompleta);

    diaTratamientos.addPanelTratamientos();

    diaTratamientos.show();

    // Se actualizan los datos de bloqueo del notificador seleccionado
    datNotifT = diaTratamientos.getComponente();
    setDatosBloqueoNotificador(datNotifT.getCD_OPE(), datNotifT.getFC_ULTACT());

    if (diaTratamientos.iOut == -1) { //cancelar
      diaTratamientos = null;
    }
    else { //aceptar
      diaTratamientos = null;
    }

    dlgb.Inicializar(modo);

  }

  public void btnProtocolo() {
    int modo = modoOperacion;
    dlgb.Inicializar(constantes.modoESPERA);

    DataProtocolo data = new DataProtocolo();
    //data.sCodigo = constantes.TUBERCULOSIS;
    // modificacion 12/02/2001
    data.sCodigo = this.app.getParametro("CD_TUBERCULOSIS");
    data.sDescripcion = "TUBERCULOSIS";

    // modificacion 09/06/2000
    // de esta manera conseguimos que aparezcan los protocolos
    // generados por usuarios de area y distrito
    String s1 = null;
    String s2 = null;
    //if(modoOperacion == modoALTA) {
    //s1 = this.pansup.panNiv.getCDNivel1();
    //s2 = this.pansup.panNiv.getCDNivel2();
    //} else {
    s1 = (String) getNivel1Gestion();
    s2 = (String) getNivel2Gestion();
    //}

    data.sNivel1 = s1;
    data.sNivel2 = s2;

    //data.sNivel1 = dlgb.panCabecera.txtCodNivel1.getText().trim();
    //data.sNivel2 = dlgb.panCabecera.txtCodNivel2.getText().trim();
    data.NumCaso = dlgb.sNM_EDO;
    data.sCa = app.getCA();
    //sNotif = (DataNotifTub) getNotificador().getCD_E_NOTIF();

    DiaProt diaProt = new DiaProt(this, modo, data);

    diaProt.addPanelProt();

    diaProt.show();

    if (diaProt.iOut == -1) { //cancelar
      diaProt = null;
    }
    else { //aceptar
      diaProt = null;
    }

    dlgb.Inicializar(modo);

  }

  public void setDatosNotificadorM(DatNotifTub dnt) {
    datNotifM = new DatCasMuesCS(dnt.getCD_E_NOTIF_EDOI(),
                                 dnt.getCD_ANOEPI_EDOI(),
                                 dnt.getCD_SEMEPI(),
                                 dnt.getNM_EDO(),
                                 dnt.getFC_NOTIF(),
                                 dnt.getFC_RECEP_EDOI(),
                                 dnt.getCD_FUENTE(),
                                 dnt.getDS_HISTCLI(),
                                 dnt.getDS_DECLARANTE(),
                                 dnt.getIT_PRIMERO(),
                                 dnt.getCD_OPE_EDOI(),
                                 dnt.getFC_ULTACT_EDOI());
  }

  public DatCasMuesCS getDatosNotificadorM() {
    return datNotifM;
  }

  public void setDatosNotificadorT(DatNotifTub dnt) {
    datNotifT = new DatCasTratCS(dnt.getCD_E_NOTIF_EDOI(),
                                 dnt.getCD_ANOEPI_EDOI(),
                                 dnt.getCD_SEMEPI(),
                                 dnt.getNM_EDO(),
                                 dnt.getFC_NOTIF(),
                                 dnt.getFC_RECEP_EDOI(),
                                 dnt.getCD_FUENTE(),
                                 dnt.getDS_HISTCLI(),
                                 dnt.getDS_DECLARANTE(),
                                 dnt.getIT_PRIMERO(),
                                 dnt.getCD_OPE_EDOI(),
                                 dnt.getFC_ULTACT_EDOI());
  }

  public DatCasTratCS getDatosNotificadorT() {
    return datNotifT;
  }

  public void setOpeFcNotificadores(DatNotifTub dnt) {
    if (dnt != null) {
      actualizaNotificadores(dnt);
    }
  }

  // Para eliminar notificadores
  protected void btnBaja() {
    boolean bEraPrimero = false;

    Vector vCual = null;
    int iIndice = -1;

    int modo = modoOperacion;
    dlgb.Inicializar(constantes.modoESPERA);

    // Si no hay nada seleccionado, se ignora la pulsaci�n
    if (tabla.getSelectedIndex() < 0) {
      return;
    }

    // Si s�lo hay uno, no se puede borrar
    if (tabla.countItems() <= 1) {
      showMensaje("Como m�nimo ha de existir un notificador");

    }
    else {

      // �En qu� vector est� el elemento pulsado?
      if (tabla.getSelectedIndex() <= vNotificadores.size() - 1) {
        // Est� en la 'antigua'
        vCual = vNotificadores;
        iIndice = tabla.getSelectedIndex();
      }
      else {
        // Est� en la 'nueva'
        vCual = vNuevosNotificadores;
        iIndice = tabla.getSelectedIndex() - vNotificadores.size();
      }

      // A�o de alta
      // se obtiene del vector

      // n� de caso EDO
      hsCompleta.put(constantes.CASOEDO, sNM_EDO);

      // Datos de ese notificador
      hsCompleta.put(constantes.DATOSNOTIFICADOR,
                     (DatNotifTub) vCual.elementAt(iIndice));

      // Proceso en s�
      DiaNotificaciones diaNotificaciones = new DiaNotificaciones(this.app, false);
      Hashtable hsEnvioADialogo = new Hashtable();

      diaNotificaciones.rellena(hsCompleta);
      diaNotificaciones.ponerModo(constantes.modoBAJA);

      diaNotificaciones.show();

      // Ahora debe borrarse el elemento de su vector y recargar
      // la tabla si se ha pulsado Aceptar en el di�logo
      DatNotifTub nuevaFila = (DatNotifTub) diaNotificaciones.getComponente();

      // El objeto devuelto contiene la clave del que pasar� a ser 1er. notificador
      // si el campo IT_PRIMERO es 'S'
      if (nuevaFila != null) {
        vCual.removeElementAt(iIndice);
        if (nuevaFila.getIT_PRIMERO().equals("S")) {
          // Ahora ha de buscarse cu�l es el nuevo primer notificador, teniendo
          // en cuenta que puede estar en cualquiera de los dos vectores
          establecePrimero(nuevaFila);
        }
        actualizaNotificadores(nuevaFila);
        cargaTabla();
      }

      diaNotificaciones = null;
    }

    dlgb.Inicializar(modo);
  } // FIN btnBaja ***********************************************************

  // Acciones sobre la tabla
  protected void tablaActionPerformed(jclass.bwt.JCActionEvent e) {
    btnModificar();
  }

} // endclass PanTuberIndivDeclarantes

// Gesti�n de eventos sobre los botones de la tabla
class btnTablaActionListener
    implements java.awt.event.ActionListener {
  PanTablaNotif adaptee;

  public btnTablaActionListener(PanTablaNotif ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    //adaptee.modoOperacion = adaptee.modoESPERA;
    //adaptee.Inicializar(adaptee.modoOperacion);

    if (e.getActionCommand() == "primero") {
      adaptee.btnTablaPrimero();
    }
    else if (e.getActionCommand() == "anterior") {
      adaptee.btnTablaAnterior();
    }
    else if (e.getActionCommand() == "siguiente") {
      adaptee.btnTablaSiguiente();
    }
    else {
      adaptee.btnTablaUltimo();
    }
    //adaptee.desbloquea();
  }

}

// Gesti�n de eventos sobre los botones A�ADIR, MODIFICAR, BORRAR
class btnAMBActionListener
    implements java.awt.event.ActionListener {
  PanTablaNotif adaptee;

  public btnAMBActionListener(PanTablaNotif ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    //adaptee.modoOperacion = adaptee.modoESPERA;
    //adaptee.Inicializar(adaptee.modoOperacion);

    if (e.getActionCommand() == "alta") {
      adaptee.btnAlta();
    }
    else if (e.getActionCommand() == "modificar") {
      adaptee.btnModificar();
    }
    else if (e.getActionCommand() == "baja") {
      adaptee.btnBaja();
    }
    else if (e.getActionCommand() == "resultado") {
      adaptee.btnResul();
    }
    else if (e.getActionCommand() == "evolucion") {
      adaptee.btnEvol();
    }
    else if (e.getActionCommand() == "protocolo") {
      adaptee.btnProtocolo();
    }
    //adaptee.desbloquea();
  }

}

class jcalTablaActionListener
    implements jclass.bwt.JCActionListener {
  PanTablaNotif adaptee;

  public jcalTablaActionListener(PanTablaNotif ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(jclass.bwt.JCActionEvent e) {
    //adaptee.modoOperacion = adaptee.modoESPERA;
    //adaptee.Inicializar(adaptee.modoOperacion );

    adaptee.tablaActionPerformed(e);

    //adaptee.desbloquea();
  }
}

// Eventos (click o barra espaciadora) sobre la tabla
class TablaItemListener
    implements jclass.bwt.JCItemListener {
  PanTablaNotif adaptee;

  public TablaItemListener(PanTablaNotif dcDialogo) {
    adaptee = dcDialogo;
  }

  public void itemStateChanged(jclass.bwt.JCItemEvent e) {
    adaptee.clickTabla();
  }
}
