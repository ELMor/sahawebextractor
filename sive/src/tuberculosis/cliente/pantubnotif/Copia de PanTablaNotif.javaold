/**
* Clase: PanTablaNotif
* Paquete: tuberculosis.cliente.pantubnotif
* Hereda: CPanel
* Autor: Jos� M� Torrecilla P�rez (JMT) / Emilio Postigo Riancho
* Fecha Inicio: 07/10/1999 / 18/11/1999
* Descripcion: Implementacion del panel que permite visualizar y navegar
    entre los diferentes declarantes de un caso
*/
package tuberculosis.cliente.pantubnotif;

import java.awt.*;
import java.awt.event.*;
import borland.jbcl.layout.*;
import borland.jbcl.control.*;

import jclass.bwt.*;
import jclass.util.*;

import capp.*;
import comun.*;

import tuberculosis.datos.notiftub.*;
import tuberculosis.cliente.diatubnotif.*;
import tuberculosis.cliente.diatuber.*;

import java.util.*;

public class PanTablaNotif extends CPanel{

  XYLayout lyXYLayout = new XYLayout();

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior = 0;

  // Estados posibles del panel
  protected final int modoESPERA = comun.constantes.modoESPERA;
  protected final int modoALTA = comun.constantes.modoALTA;
  protected final int modoMODIFICACION = comun.constantes.modoMODIFICACION;
  protected final int modoBAJA = comun.constantes.modoBAJA;
  protected final int modoCONSULTA = comun.constantes.modoCONSULTA;

  // Variable estado del panel
  protected int modoOperacion = 0;

  // Variables que almacenan los diferentes permisos del usuario
  private boolean bAlta = false;
  private boolean bBaja = false;
  private boolean bModificacion = false;

  // Controles para la navegacion entre los elementos de la tabla
  public ButtonControl btnAlta = null;
  public ButtonControl btnModificar = null;
  public ButtonControl btnBaja = null;
  ButtonControl btnTodas = null;
  ButtonControl btnValidar = null;
  ButtonControl btnValidadas = null;
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  // Tabla que visualiza y permite navegar entre los notificadores
  public CTabla tabla = new CTabla();

  // Vector que almacena las hashtable's que llegan para rellenar
  // el panel
  private Vector vNotificadores = null;

  // Cargadores de im�genes visibles en todo el m�dulo
  private CCargadorImagen imgs_declarantes = null;

  CApp capp;

  
  DialogTub dlgb = null;
  Hashtable hsCompleta = new Hashtable();


  // Constructor
  public PanTablaNotif(DialogTub dlg, int modo, Hashtable has) {
    try{

      capp = dlg.getCApp();
      this.app = capp;
      hsCompleta = (Hashtable)has.clone();
      dlgb = dlg;

      bAlta = capp.getIT_AUTALTA().equals("S");
      bBaja = capp.getIT_AUTBAJA().equals("S");
      bModificacion = capp.getIT_AUTMOD().equals("S");

      modoOperacion = modo;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicializacion en la creacion de un objeto
  void jbInit() throws Exception {
     final String imgNAME_mantenimiento[] = {
                            "images/alta2.gif",
                            "images/modificacion2.gif",
                            "images/baja2.gif"};

     final String imgNAME_tabla[] = {
                            "images/primero.gif",
                            "images/anterior.gif",
                            "images/siguiente.gif",
                            "images/ultimo.gif"};

     final String imgNAME_declarantes[] = {
                            "images/declaracion2.gif"};

    // Carga de imagenes
    CCargadorImagen imgs_mantenimiento = new CCargadorImagen(capp,imgNAME_mantenimiento);
    imgs_mantenimiento.CargaImagenes();
    CCargadorImagen imgs_tabla = new CCargadorImagen(capp,imgNAME_tabla);
    imgs_tabla.CargaImagenes();

    imgs_declarantes = new CCargadorImagen(capp,imgNAME_declarantes);
    imgs_declarantes.CargaImagenes();

    // Medidas del panel
    this.setSize(new Dimension(770, 315));
    lyXYLayout.setHeight(305);
    lyXYLayout.setWidth(770);
    this.setLayout(lyXYLayout);

    btnPrimero = new ButtonControl(imgs_tabla.getImage(0));
    btnAnterior = new ButtonControl(imgs_tabla.getImage(1));
    btnSiguiente = new ButtonControl(imgs_tabla.getImage(2));
    btnUltimo = new ButtonControl(imgs_tabla.getImage(3));

    btnAlta = new ButtonControl(imgs_mantenimiento.getImage(0));
    btnModificar = new ButtonControl(imgs_mantenimiento.getImage(1));
    btnBaja = new ButtonControl(imgs_mantenimiento.getImage(2));

    // Se establecen los parametros de la tabla
    PintarTabla();

    this.add(tabla, new XYConstraints(5, 15, 745, 120));

    this.add(btnAlta, new XYConstraints(10, 155, 25, 25));
    this.add(btnModificar, new XYConstraints(40, 155, 25, 25));
    this.add(btnBaja, new XYConstraints(70, 155, 25, 25));
    this.add(btnPrimero, new XYConstraints(505 + 105, 155, 25, 25));
    this.add(btnAnterior, new XYConstraints(545 + 105, 155, 25, 25));
    this.add(btnSiguiente, new XYConstraints(583 + 105, 155, 25, 25));
    this.add(btnUltimo, new XYConstraints(621 + 105, 155, 25, 25));

    // Gesti�n de eventos

    // Botones de la tabla
    btnTablaActionListener alBotonesTabla = new btnTablaActionListener(this);

    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");

    btnPrimero.addActionListener(alBotonesTabla);
    btnAnterior.addActionListener(alBotonesTabla);
    btnSiguiente.addActionListener(alBotonesTabla);
    btnUltimo.addActionListener(alBotonesTabla);

    // Botones A�ADIR, MODIFICAR, BORRAR
    btnAMBActionListener alBotonesAMB = new btnAMBActionListener(this);

    btnAlta.setActionCommand("alta");
    btnModificar.setActionCommand("modificar");
    btnBaja.setActionCommand("baja");

    btnAlta.addActionListener(alBotonesAMB);
    btnModificar.addActionListener(alBotonesAMB);
    btnBaja.addActionListener(alBotonesAMB);

    // Tabla de notificadores
    jcalTablaActionListener jcalTabla = new jcalTablaActionListener(this);

    tabla.addActionListener(jcalTabla);

    // Se habilitan/deshabilitan los controles pertinentes
    modoAnterior = modoOperacion;
    Inicializar(modoOperacion);
    // Fin de inicializaci�n: se restaura el puntero del rat�n
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // Bloquear eventos
  /** este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized  boolean bloquea(){
     if (sinBloquear) {
         // no hay nadie bloqueando pasamos a bloquear
         sinBloquear = false;
         modoAnterior = modoOperacion;
         modoOperacion = modoESPERA;
         Inicializar(modoOperacion);

         setCursor(new Cursor(Cursor.WAIT_CURSOR));
         return true;
     }else {
         // ya est� bloqueado
         return false;
     }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea(){
     sinBloquear = true;
     modoOperacion  = modoAnterior;
     Inicializar(modoOperacion);
     setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
    if (btnModificar.isEnabled() && tabla.countItems() <= 0 )
       btnModificar.setEnabled(false);
    if (btnBaja.isEnabled() && tabla.countItems() <= 0 )
       btnBaja.setEnabled(false);
  }
  // Implementacion del metodo abstracto de CPanel
  public void Inicializar() {
    switch (modoOperacion) {
      case modoESPERA:
        tabla.setEnabled(false);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
      case modoALTA:
        break;
      case modoMODIFICACION:
        tabla.setEnabled(true);
        btnAlta.setEnabled(true && bAlta);
        btnModificar.setEnabled(true && bModificacion);
        btnBaja.setEnabled(true && bBaja);
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case modoBAJA:
      case modoCONSULTA:
        tabla.setEnabled(false);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(true);
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  // Establece el numero de columnas de la tabla y las cabeceras de las mismas
  public void PintarTabla(){
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String("70\n125\n50\n170\n40\n170\n50\n50"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new String("A�o-Sem\nF.Notif-F.Recep\nEquipo\nCentro\nZonas\nDeclarante\nFuente\nPrim"), '\n'));
    tabla.setNumColumns(8);
  }//fin PintarTabla()

  // Implementaci�n del m�todo de rellenado
  public void rellena(Hashtable data) {
    DatNotifTub  dntDatos = null;
    // StringBuffer sbLinea = null;
    JCVector jcvTabla = new JCVector();
    JCVector jcvLinea = null;

    vNotificadores = new Vector();
    tabla.clear();

    for (Enumeration e = data.keys(); e.hasMoreElements(); ) {
      dntDatos = (DatNotifTub) data.get((String) e.nextElement());
      vNotificadores.addElement(dntDatos);

      jcvLinea = new JCVector();

      jcvLinea.addElement(dntDatos.getCD_ANOEPI_EDOI() + "-" + dntDatos.getCD_SEMEPI());
      jcvLinea.addElement(dntDatos.getFC_NOTIF() + "-" + dntDatos.getFC_RECEP_EDOI());
      jcvLinea.addElement(dntDatos.getCD_E_NOTIF_EDOI());
      jcvLinea.addElement(dntDatos.getDS_CENTRO());
      jcvLinea.addElement(dntDatos.getCD_ZBS());
      jcvLinea.addElement(dntDatos.getDS_DECLARANTE());
      jcvLinea.addElement(dntDatos.getCD_FUENTE());
      // Icono si es el primero sbLinea.append(dntDatos.getDS_DECLARANTE() + "|");
      if (dntDatos.getIT_PRIMERO().equals("S")) {
        jcvLinea.addElement(imgs_declarantes.getImage(0));
      }
      jcvTabla.addElement(jcvLinea);
    }
    tabla.setItems(jcvTabla);
  }

  // M�todos gestores de eventos
  // Tabla
  protected void btnTablaPrimero() {
    tabla.select(0);
    tabla.setTopRow(0);
  }

  protected void btnTablaAnterior() {
    int index = tabla.getSelectedIndex();
    if (index > 0){
      index--;
      tabla.select(index);
      if (index - tabla.getTopRow() <= 0){
        tabla.setTopRow(tabla.getTopRow() - 1);
      }
    } else {
      tabla.select(0);
      tabla.setTopRow(0);
    }
  }

  protected void btnTablaSiguiente() {
    int ultimo = tabla.countItems() -1;
    int index = tabla.getSelectedIndex();
    if (index < ultimo && index >= 0){
      index++;
      tabla.select(index);
      if (index - tabla.getTopRow() >=4){
        tabla.setTopRow(tabla.getTopRow() + 1);
      }
    } else {
      if (index != ultimo) {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    } 
  }

  protected void btnTablaUltimo() {
    int index = 0;
    int ultimo = tabla.countItems() - 1;
    if (ultimo >= 0){
      index = ultimo;
      tabla.select(index);
      if (tabla.countItems() >= 5){
        tabla.setTopRow(tabla.countItems() - 5);
      } else {
        tabla.setTopRow(0);
      }
    }
  }

  // Acciones A�ADIR, MODIFICAR, BAJA
  protected void btnAlta() {

    //le a�ado el a�o del panel superior
    hsCompleta.put (comun.constantes.ANO_ALTA_DESDE_RT, dlgb.getAnopanSup());

    DiaNotificaciones diaNotificaciones = new DiaNotificaciones(this.app);
    diaNotificaciones.rellena(hsCompleta);
    diaNotificaciones.ponerModo(constantes.modoALTA);
    diaNotificaciones.show();

  }

  protected void btnModificar() {

  }

  protected void btnBaja() {

  }

  // Acciones sobre la tabla
  protected void tablaActionPerformed(jclass.bwt.JCActionEvent e) {

  }

} // endclass PanTuberIndivDeclarantes

// Gesti�n de eventos sobre los botones de la tabla
class btnTablaActionListener implements java.awt.event.ActionListener {
  PanTablaNotif adaptee;

  public btnTablaActionListener(PanTablaNotif ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar(adaptee.modoOperacion);

    if (e.getActionCommand() == "primero") {
      adaptee.btnTablaPrimero();
    } else if (e.getActionCommand() == "anterior") {
      adaptee.btnTablaAnterior();
    } else if (e.getActionCommand() == "siguiente") {
      adaptee.btnTablaSiguiente();
    } else {
      adaptee.btnTablaUltimo();
    }
    adaptee.desbloquea();
  }

}

// Gesti�n de eventos sobre los botones A�ADIR, MODIFICAR, BORRAR
class btnAMBActionListener implements java.awt.event.ActionListener {
  PanTablaNotif adaptee;

  public btnAMBActionListener(PanTablaNotif ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar(adaptee.modoOperacion);

    if (e.getActionCommand() == "alta") {
      adaptee.btnAlta();
    } else if (e.getActionCommand() == "modificar") {
      adaptee.btnModificar();
    } else if (e.getActionCommand() == "baja") {
      adaptee.btnBaja();
    }
    adaptee.desbloquea();
  }

}

class jcalTablaActionListener implements jclass.bwt.JCActionListener {
  PanTablaNotif adaptee;

  public jcalTablaActionListener(PanTablaNotif ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(jclass.bwt.JCActionEvent e) {
    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar(adaptee.modoOperacion );

    adaptee.tablaActionPerformed(e);

    adaptee.desbloquea();
  }

}




