/**
 * Clase: AppMenuTub
 * Paquete: tuberculosis.cliente.menu
 * Fecha Inicio: 29/02/2000
 */

package tuberculosis.cliente.menu;

//import panelnivan.*;
import java.net.URL;
import java.util.StringTokenizer;

import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import capp2.CApp;
import capp2.CLogin;
import capp2.CMenuPanel;
import capp2.UMenu;
import capp2.UMenuItem;
import comun.constantes;
import sapp2.Data;
// Para zonas por defecto, a�o y perfil
import tuberculosis.datos.menu.ParametrosTubUsu;

public class AppMenuTub
    extends CApp
    implements ActionListener {
  // Servlets utilizados
  private final String strSERVLET_AUTORIZACIONES = constantes.
      strSERVLET_AUTORIZACIONES;
  private final String strSERVLET_MENU = constantes.strSERVLET_MENU;

  // Para que siempre exista una instancia de capp.CApp
  // y se mantenga el valor de sus variables est�ticas
  public static capp.CApp AppletEterno = null;

  // applets
  protected final int appletCASOS = 100;
  protected final int appletCONTACTOS = 105;
  protected final int appletENFERMOS_CONTACTOS = 110;

  protected final int appletCIERRE = 200;
  protected final int appletAPERTURA = 210;
  protected final int appletCIERRE_AUTOMATICO = 220;

  protected final int appletMAN_REGISTRO = 300;

  protected final int appletUSUARIOS = 310;
  protected final int appletPASSWORD = 320;

  protected final int appletCATALOGO_FUENTES = constantes.
      appletCATALOGO_FUENTES;
  protected final int appletCATALOGO_MOTIVOS_SALIDA = constantes.
      appletCATALOGO_MOTIVOS_SALIDA;
  protected final int appletCATALOGO_MOTIVOS_INICIO = constantes.
      appletCATALOGO_MOTIVOS_INICIO;
  protected final int appletCATALOGO_MUESTRAS = constantes.
      appletCATALOGO_MUESTRAS;
  protected final int appletCATALOGO_TECNICAS = constantes.
      appletCATALOGO_TECNICAS;
  protected final int appletCATALOGO_VALORES_MUESTRAS = constantes.
      appletCATALOGO_VALORES_MUESTRAS;
  protected final int appletCATALOGO_MICOBACTERIAS = constantes.
      appletCATALOGO_MICOBACTERIAS;
  protected final int appletCATALOGO_ESTUDIOS_RESISTENCIAS = constantes.
      appletCATALOGO_ESTUDIOS_RESISTENCIAS;

  protected final int appletCONFLICTOS = 400;

  protected final int appletPREGUNTAS_CONTACTOS = 500;
  protected final int appletMODELOS_CONTACTOS = 510;
  protected final int appletLISTAS_CONTACTOS = 520;

  protected final int appletPREGUNTAS_CASOS = 550;
  protected final int appletMODELOS_CASOS = 560;
  protected final int appletLISTAS_CASOS = 570;

  protected final int appletENVIO_CNE = 800;
  protected final int appletZONIFICACION = 600;
  protected final int appletGRUPOS = 700;

  protected final int appletVOLCADOS_CONTACTOS = 900;
  protected final int appletVOLCADOS_RESULTADOS = 910;
  protected final int appletVOLCADOS_EVOLUCIONES = 920;

  // p�ginas de ayuda
  protected final int ayuda_AD = 1000;
  protected final int ayuda_PP = 1001;

  // Di�logo para solicitar usuario/password
  private static CLogin dlg = null;

  // Par�metros para invocar al servidor HTML
//  private Data dtParametros = null;

  // Par�metros 'antiguos'
  private ParametrosTubUsu ptu = null;

  // Dialogo para obtener los valores por defecto
//  DlgMenuTub dlgMenu = null;

  // Objetos del men�
  CMenuPanel mPanel = new CMenuPanel();
  MenuBar mBar = new MenuBar();

  public void destroy() {
    if (dlg != null) {
      dlg.dispose();
      dlg = null;
    }

  }

  public void stop() {
    if (dlg != null) {
      dlg.dispose();
      dlg = null;
    }

  }

  public void init() {
    super.init();

    try {
      AppletEterno = new capp.CApp();

      if (dlg != null) {
        dlg.dispose();
        dlg = null;
      }

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    Data dtAut = null;

    // Solicita las acreditaciones del usuario

    dlg = new CLogin(this, strSERVLET_AUTORIZACIONES,
                     "Control de Acceso [" + getParameter("COD_APLICACION") +
                     "]");
    dlg.show();
    dtAut = dlg.getAutorizaciones();

    /*
         // PRUEBAS
         Data dt = new Data();
         dt.put("COD_USUARIO", "ADMON");
         dt.put("PASSWORD", cifrado.Cifrar.cifraClave("ADMON"));
         dt.put("COD_APLICACION", "RTBC");
         Lista cl = new Lista();
         cl.addElement(dt);
         tuberculosis.servidor.menu.SrvTubAut srv = new tuberculosis.servidor.menu.SrvTubAut();
         srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
         cl = srv.doDebug(1, cl);
         //
         dtAut = (Data) cl.elementAt(0);*/

    // modificacion jlt para recuperar el url_servlet y
    // el url_html
    String ur = new String();
    String ur2 = new String();
    String ur3 = new String();
    String ur4 = new String();
    int ind = 0;
    int indb = 0;
    ur = (String)this.getDocumentBase().toString();
    ind = ur.lastIndexOf("/");
    ur2 = (String) ur.substring(0, ind + 1);
    ur3 = (String) ur2.substring(0, ind);
    indb = ur3.lastIndexOf("/");
    ur4 = (String) ur3.substring(0, indb + 1);
    System.out.println("urmenu2" + ur2);
    System.out.println("urmenu4" + ur4);
    /////////////////

    setAutorizaciones(dtAut);

    // Si el usuario est� acreditado tiene acceso
    if (dtAut != null) {

      // Recupera los par�metros 'antiguos'
      ptu = (ParametrosTubUsu) dtAut.get("PARAMETROS_TUB_USU");

      // Prepara la estructura de par�metros para solicitar los applets

      // modificacion jlt para recuperar url_servlet
      if (getParametro("URL_SERVLET").equals("") ||
          getParametro("URL_SERVLET").equals("/")
          || getParametro("URL_SERVLET").equals(null)) {

        ptu.put("URL_SERVLET", ur4);
      }
      else {
        ptu.put("URL_SERVLET", getParametro("URL_SERVLET"));
      }
      //ptu.put("URL_SERVLET", getParametro("URL_SERVLET"));

      if (getParametro("URL_HTML").equals("") ||
          getParametro("URL_HTML").equals("/")
          || getParametro("URL_HTML").equals(null)) {

        ptu.put("URL_HTML", ur2);
      }
      else {
        ptu.put("URL_HTML", getParametro("URL_HTML"));
      }

      //ptu.put("URL_HTML", getParametro("URL_HTML"));

      ptu.put("FTP_SERVER", getParametro("FTP_SERVER"));

      ptu.put("IT_USU", parseAutorizaciones());
      ptu.put("COD_APLICACION", getParametro("COD_APLICACION"));
      ptu.put("CD_TUBERCULOSIS", getParametro("CD_TUBERCULOSIS"));

      mPanel.removeAll();
      mBar = new MenuBar();

      /* PDP 20/03/2000
             // Grupos de trabajo RTBC
             if(isAccesible("1")){
        UMenu m1 = new UMenu("Grupos","1",this);
        mBar.add(m1);
        // Comprueba si esta cerrada
        if(isAllowed("1")){
           UMenuItem mi11 = new UMenuItem("Grupos de trabajo", "11", 11, this, this);
          m1.add(mi11);
        } // Fin if(allowed "1")
             } // Fin if(accesible "1")
       */

      // Casos RTBC
      if (isAccesible("1")) {
        UMenu m1 = new UMenu("Casos", "1", this);
        mBar.add(m1);

        // Comprueba si esta cerrada
        if (isAllowed("1")) {
          UMenuItem mi11 = new UMenuItem("Casos de Tuberculosis", "11", 11, this, this);
          m1.add(mi11);

          UMenuItem mi12 = new UMenuItem("Contactos de Casos de Tuberculosis",
                                         "12", 12, this, this);
          m1.add(mi12);

          // Separador
          MenuItem msep13 = new MenuItem("-");
          m1.add(msep13);

          // Cierre
          UMenuItem mi14 = new UMenuItem("Cierre de Registros de Tuberculosis",
                                         "14", 14, this, this);
          m1.add(mi14);

          UMenuItem mi15 = new UMenuItem(
              "Apertura de Registros de Tuberculosis", "15", 15, this, this);
          m1.add(mi15);

          UMenuItem mi16 = new UMenuItem(
              "Cierre Autom�tico de Registros de Tuberculosis", "16", 16, this, this);
          m1.add(mi16);

          // Separador
          MenuItem msep17 = new MenuItem("-");
          m1.add(msep17);

          UMenuItem mi18 = new UMenuItem("Mantenimiento de Enfermos/Contactos",
                                         "18", 18, this, this);
          m1.add(mi18);

        } // Fin if(allowed "1")
      } // Fin if(accesible "1")

      // Resoluci�n de conflictos
      if (isAccesible("2")) {
        UMenu m2 = new UMenu("Conflictos", "2", this);
        mBar.add(m2);

        // Comprueba si esta cerrada
        if (isAllowed("2")) {
          UMenuItem mi21 = new UMenuItem("Resoluci�n de Conflictos", "21", 21, this, this);
          m2.add(mi21);
        } // Fin if(allowed "2")
      } // Fin if(accesible "2")

      // VOLCADOS
      if (isAccesible("3")) {
        UMenu m3 = new UMenu("Exportaciones", "3", this);
        mBar.add(m3);

        // Comprueba si esta cerrada
        if (isAllowed("3")) {
          UMenuItem mi31 = new UMenuItem("Volcado de Contactos", "31", 31, this, this);
          m3.add(mi31);

          UMenuItem mi32 = new UMenuItem("Volcado de Resultados", "32", 32, this, this);
          m3.add(mi32);

          UMenuItem mi33 = new UMenuItem("Volcado de Evoluciones", "33", 33, this, this);
          m3.add(mi33);
        } // Fin if(allowed "3")
      } // Fin if(accesible "3")

      // ENV�OS
      if (isAccesible("4")) {
        UMenu m4 = new UMenu("Env�os", "4", this);
        mBar.add(m4);

        // Comprueba si esta cerrada
        if (isAllowed("4")) {
          UMenuItem mi41 = new UMenuItem("Env�os al CNE", "41", 41, this, this);
          m4.add(mi41);
        } // Fin if(allowed "4")
      } // Fin if(accesible "4")

      // Parametrizacion
      if (isAccesible("5")) {
        UMenu m5 = new UMenu("Parametrizaci�n", "5", this);
        mBar.add(m5);

        if (isAllowed("5")) {

          UMenu m52 = new UMenu("Protocolos de Casos RTBC", "52", this);
          if (isAccesible("52")) {
            m5.add(m52);

            // Mantenimiento de Modelos
            UMenuItem mi521 = new UMenuItem("Mantenimiento de Modelos", "521",
                                            521, this, this);
            m52.add(mi521);

            // Mantenimiento de Preguntas
            UMenuItem mi522 = new UMenuItem("Mantenimiento de Preguntas", "522",
                                            522, this, this);
            m52.add(mi522);

            // Mantenimiento de Listas de valores
            UMenuItem mi523 = new UMenuItem("Mantenimiento de Listas", "523",
                                            523, this, this);
            m52.add(mi523);

          } // Fin if(accesible "52")

          UMenu m51 = new UMenu("Protocolos de Contactos RTBC", "51", this);
          if (isAccesible("51")) {
            m5.add(m51);

            // Mantenimiento de Modelos
            UMenuItem mi511 = new UMenuItem("Mantenimiento de Modelos", "511",
                                            511, this, this);
            m51.add(mi511);

            // Mantenimiento de Preguntas
            UMenuItem mi512 = new UMenuItem("Mantenimiento de Preguntas", "512",
                                            512, this, this);
            m51.add(mi512);

            // Mantenimiento de Listas de valores
            UMenuItem mi513 = new UMenuItem("Mantenimiento de Listas", "513",
                                            513, this, this);
            m51.add(mi513);

          } // Fin if(accesible "51")

        } // Fin if(allowed "5")
      } // Fin if(accesible "5")

      // Mantenimientos
      if (isAccesible("6")) {
        UMenu m6 = new UMenu("Administraci�n", "6", this);
        mBar.add(m6);

        // Comprueba si esta cerrada
        if (isAllowed("6")) {

          // Registro de Tuberculosis
          UMenuItem mi61 = new UMenuItem(
              "Mantenimiento de Registros de Tuberculosis", "61", 61, this, this);
          if (isAccesible("61")) {
            m6.add(mi61);

            // Separador
          }
          MenuItem msep62 = new MenuItem("-");
          m6.add(msep62);

          // Usuarios
          UMenuItem mi63 = new UMenuItem("Usuarios", "63", 63, this, this);
          if (isAccesible("63")) {
            m6.add(mi63);

            // Grupos de Trabajo
          }
          UMenuItem mi64 = new UMenuItem("Grupos de Trabajo", "64", 64, this, this);
          if (isAccesible("64")) {
            m6.add(mi64);

            // Separador
          }
          MenuItem msep65 = new MenuItem("-");
          m6.add(msep65);

          // Tablas de C�digos
          UMenu m66 = new UMenu("Tablas de c�digos", "66", this);
          if (isAccesible("66")) {
            m6.add(m66);

            // Tipo de fuentes de notificaci�n
            UMenuItem mi661 = new UMenuItem("Fuentes de Notificaci�n", "661",
                                            661, this, this);
            m66.add(mi661);

            // Tipo de motivos de salida del Registro de Tuberculosis
            UMenuItem mi662 = new UMenuItem(
                "Motivos de salida del Registro de Tuberculosis", "662", 662, this, this);
            m66.add(mi662);

            // Tipo de motivos de inicio/fin de tratamientos
            UMenuItem mi663 = new UMenuItem(
                "Motivos de inicio/fin de Tratamientos de Tuberculosis", "663",
                663, this, this);
            m66.add(mi663);

            // Tipo de muestras de laboratorio
            UMenuItem mi664 = new UMenuItem("Muestras de Laboratorio", "664",
                                            664, this, this);
            m66.add(mi664);

            // Tipo de t�cnicas
            UMenuItem mi665 = new UMenuItem("Tipos de T�cnicas", "665", 665, this, this);
            m66.add(mi665);

            // Tipo de valores de muestras
            UMenuItem mi666 = new UMenuItem("Valores de Muestras", "666", 666, this, this);
            m66.add(mi666);

            // Tipo de micobacteria
            UMenuItem mi667 = new UMenuItem("Tipos de Micobacteria", "667", 667, this, this);
            m66.add(mi667);

            // Tipo de estudios de resistencias
            UMenuItem mi668 = new UMenuItem("Tipos de Estudios de Resistencias",
                                            "668", 668, this, this);
            m66.add(mi668);

          } // Fin if(accesible "66")

        } // Fin if(allowed "6")
      } // Fin if(accesible "6")

      //Opciones
      if (isAccesible("7")) {
        UMenu m7 = new UMenu("General", "7", this);
        mBar.add(m7);

        if (isAllowed("7")) {
          UMenuItem mi71 = new UMenuItem("Seleccionar Zonificaci�n", "71", 71, this, this);
          m7.add(mi71);
        }
      }

      // Ayuda
      if (isAccesible("8")) {
        UMenu m8 = new UMenu("Ayuda", "8", this);
        mBar.add(m8);

        // Comprueba si esta cerrada
        if (isAllowed("8")) {

          // Sistema de Informaci�n RTBC
          UMenuItem mi81 = new UMenuItem("Sistema de Informaci�n RTBC", "81",
                                         81, this, this);
          if (isAccesible("81")) {
            m8.add(mi81);

            // Separador
            MenuItem msep3 = new MenuItem("-");
            m8.add(msep3);
          }

          // Acerca de ...
          UMenuItem mi83 = new UMenuItem("Acerca de ...", "83", 83, this, this);
          if (isAccesible("83")) {
            m8.add(mi83);

          }
        } // Fin if(allowed "8")
      } // Fin if(accesible "8")

      /*      // Crea el di�logo de valores por defecto (a�o) y lo muestra
            dlgMenu = new DlgMenuTub(this, constantes.modoMODIFICACION,dtAut.getString("FC_ACTUAL").substring(6));
            dlgMenu.show();
            // Completa los valores por defecto: A�o
            if(dlgMenu.getAceptar()) {
           dtParametros.put("CD_ANO", dlgMenu.getParametros().getString("CD_ANO"));
            }*/

    } // Fin if(dtAut!=null)

    // pinta el men�
    mPanel.setMenuBar(mBar);
    add("North", mPanel);
    validate();
  }

  public void actionPerformed(ActionEvent e) {
    URL url;
    String get;

    // modificacion jlt para recuperar el url_servlet
    String ur = new String();
    String ur2 = new String();
    String ur3 = new String();
    String ur4 = new String();
    int ind = 0;
    int indb = 0;
    ur = (String)this.getDocumentBase().toString();
    ind = ur.lastIndexOf("/");
    ur2 = (String) ur.substring(0, ind + 1);
    ur3 = (String) ur2.substring(0, ind);
    indb = ur3.lastIndexOf("/");
    ur4 = (String) ur3.substring(0, indb + 1);
    System.out.println("urmenu2" + ur2);
    System.out.println("urmenu3" + ur4);
    ////////////////////////

    if (e.getSource()instanceof UMenuItem) {
      UMenuItem mi = (UMenuItem) e.getSource();
      try {

        // Agrega el c�digo como par�metro
        //dtParametros.put("NM_APPLET",(new Integer(mi.getCode())).toString());

        // Recarga el applet
        switch (mi.getCode()) {
          case 41:
            showAdvise(
                "Conexi�n no disponible con el Centro Nacional de Epidemiolog�a");
            break;

            /*  PDP 20/03/2000
              //Cambio de password
                       case 78:
                DiaPassword2 dialPwd= new DiaPassword2(this);
                dialPwd.show();
                dialPwd = null;
                break;
             */

          case 81:
            ptu.put("ANYO_DEFECTO", AppletEterno.getANYO_DEFECTO());

            ptu.put("CD_NIVEL_1_DEFECTO", AppletEterno.getCD_NIVEL1_DEFECTO());
            ptu.put("CD_NIVEL_2_DEFECTO", AppletEterno.getCD_NIVEL2_DEFECTO());
            ptu.put("DS_NIVEL_1_DEFECTO", AppletEterno.getDS_NIVEL1_DEFECTO());
            ptu.put("DS_NIVEL_2_DEFECTO", AppletEterno.getDS_NIVEL2_DEFECTO());

            //url = new URL(getParameter("URL_SERVLET") + strSERVLET_MENU + ptu.toURL(String.valueOf(idApplet(mi.getCode()))));
            //url=new URL(getParameter("URL_SERVLET") + "zip/ayuda/tuberculosis"  +"/ayuda_pp.html");
            //Se obtiene la URL de la pag. html de ayuda
            if (getParameter("URL_HTML").equals("") ||
                getParameter("URL_HTML").equals("/")
                || getParameter("URL_HTML").equals(null)) {

              url = new URL(ur2 + "zip/ayuda/tuberculosis" + "/ayuda_pp.html");
            }
            else {
              url = new URL(getParameter("URL_HTML") + "zip/ayuda/tuberculosis" +
                            "/ayuda_pp.html");
            }

            //url=new URL(getParameter("URL_HTML") + "zip/ayuda/tuberculosis"  +"/ayuda_pp.html");
            System.out.println(url);
            getAppletContext().showDocument(url, "main");
            break;

          case 83:
            ptu.put("ANYO_DEFECTO", AppletEterno.getANYO_DEFECTO());

            ptu.put("CD_NIVEL_1_DEFECTO", AppletEterno.getCD_NIVEL1_DEFECTO());
            ptu.put("CD_NIVEL_2_DEFECTO", AppletEterno.getCD_NIVEL2_DEFECTO());
            ptu.put("DS_NIVEL_1_DEFECTO", AppletEterno.getDS_NIVEL1_DEFECTO());
            ptu.put("DS_NIVEL_2_DEFECTO", AppletEterno.getDS_NIVEL2_DEFECTO());

            //url = new URL(getParameter("URL_SERVLET") + strSERVLET_MENU + ptu.toURL(String.valueOf(idApplet(mi.getCode()))));
            //url=new URL(getParameter("URL_SERVLET") + "zip/ayuda/tuberculosis"  +"/ayuda_ad.html");

            if (getParameter("URL_HTML").equals("") ||
                getParameter("URL_HTML").equals("/")
                || getParameter("URL_HTML").equals(null)) {

              url = new URL(ur2 + "zip/ayuda/tuberculosis" + "/ayuda_AD.html");
            }
            else {
              url = new URL(getParameter("URL_HTML") + "zip/ayuda/tuberculosis" +
                            "/ayuda_AD.html");
            }

            //url=new URL(getParameter("URL_HTML") + "zip/ayuda/tuberculosis"  +"/ayuda_AD.html");
            System.out.println(url);
            getAppletContext().showDocument(url, "main");
            break;

            // Applet de trabajo
          default:
            ptu.put("ANYO_DEFECTO", AppletEterno.getANYO_DEFECTO());

            ptu.put("CD_NIVEL_1_DEFECTO", AppletEterno.getCD_NIVEL1_DEFECTO());
            ptu.put("CD_NIVEL_2_DEFECTO", AppletEterno.getCD_NIVEL2_DEFECTO());
            ptu.put("DS_NIVEL_1_DEFECTO", AppletEterno.getDS_NIVEL1_DEFECTO());
            ptu.put("DS_NIVEL_2_DEFECTO", AppletEterno.getDS_NIVEL2_DEFECTO());

            // modificacion jlt para recuperar url_servlet
            if (getParameter("URL_SERVLET").equals("") ||
                getParameter("URL_SERVLET").equals("/")
                || getParameter("URL_SERVLET").equals(null)) {

              url = new URL(ur4 + strSERVLET_MENU +
                            ptu.toURL(String.valueOf(idApplet(mi.getCode()))));
            }
            else {
              url = new URL(getParameter("URL_SERVLET") + strSERVLET_MENU +
                            ptu.toURL(String.valueOf(idApplet(mi.getCode()))));
            }

            //url = new URL(getParameter("URL_SERVLET") + strSERVLET_MENU + ptu.toURL(String.valueOf(idApplet(mi.getCode()))));
            System.out.println(url);
            getAppletContext().showDocument(url, "main");
            break;
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
        showError(ex.getMessage());
      } // Fin try .. catch

    } // if(instancia de UmenuItem)
  } // Fin actionPerformed()

  private int idApplet(int numeroMenu) {
    int iValor = -1;

    switch (numeroMenu) {
      /*
             case 11:
        iValor = appletGRUPOS;
        break;
       */

      case 11:
        iValor = appletCASOS;
        break;
      case 12:
        iValor = appletCONTACTOS;
        break;
      case 14:
        iValor = appletCIERRE;
        break;
      case 15:
        iValor = appletAPERTURA;
        break;
      case 16:
        iValor = appletCIERRE_AUTOMATICO;
        break;
      case 18:
        iValor = appletENFERMOS_CONTACTOS;
        break;
      case 21:
        iValor = appletCONFLICTOS;
        break;
      case 31:
        iValor = appletVOLCADOS_CONTACTOS;
        break;
      case 32:
        iValor = appletVOLCADOS_RESULTADOS;
        break;
      case 33:
        iValor = appletVOLCADOS_EVOLUCIONES;
        break;
      case 41:
        iValor = appletENVIO_CNE;
        break;
      case 511:
        iValor = appletMODELOS_CONTACTOS;
        break;
      case 512:
        iValor = appletPREGUNTAS_CONTACTOS;
        break;
      case 513:
        iValor = appletLISTAS_CONTACTOS;
        break;
      case 521:
        iValor = appletMODELOS_CASOS;
        break;
      case 522:
        iValor = appletPREGUNTAS_CASOS;
        break;
      case 523:
        iValor = appletLISTAS_CASOS;
        break;
      case 61:
        iValor = appletMAN_REGISTRO;
        break;
      case 63:
        iValor = appletUSUARIOS;
        break;
      case 64:
        iValor = appletGRUPOS;
        break;
      case 661:
        iValor = appletCATALOGO_FUENTES;
        break;
      case 662:
        iValor = appletCATALOGO_MOTIVOS_SALIDA;
        break;
      case 663:
        iValor = appletCATALOGO_MOTIVOS_INICIO;
        break;
      case 664:
        iValor = appletCATALOGO_MUESTRAS;
        break;
      case 665:
        iValor = appletCATALOGO_TECNICAS;
        break;
      case 666:
        iValor = appletCATALOGO_VALORES_MUESTRAS;
        break;
      case 667:
        iValor = appletCATALOGO_MICOBACTERIAS;
        break;
      case 668:
        iValor = appletCATALOGO_ESTUDIOS_RESISTENCIAS;
        break;
      case 71:
        iValor = appletZONIFICACION;
        break;
    }

    return iValor;
  }

  // Chapuza
  public void showStatus(String s) {
    //System.out.println(s);

    String sBuffer = null;
    StringTokenizer st = new StringTokenizer(s, "$");

    sBuffer = st.nextToken();
    AppletEterno.setCD_NIVEL1_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);

    sBuffer = st.nextToken();
    AppletEterno.setDS_NIVEL1_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);

    sBuffer = st.nextToken();
    AppletEterno.setCD_NIVEL2_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);

    sBuffer = st.nextToken();
    AppletEterno.setDS_NIVEL2_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);

    sBuffer = st.nextToken();
    AppletEterno.setANYO_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);

  }
  // Fin Chapuza

} // endclass AppMenu
