/**
 * Clase: PanCaso
 * Paquete: tuberculosis.cliente.pancaso
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 17/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberulosis
 * Descripcion: Implementacion del panel que interactua con los datos del
 *   caso. Tabla asociada: SIVE_EDOIND
 */

package tuberculosis.cliente.pancaso;

import java.util.Hashtable;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CDialog;
import capp.CLista;
import capp.CPanel;
import comun.Common;
import comun.UtilEDO;
import comun.constantes;
// modificacion
import tuberculosis.cliente.diatuber.DialogTub;
import tuberculosis.datos.notiftub.DatCasosTub;

public class PanCaso
    extends CPanel {

  // Modo de entrada al panel
  protected int modoOperacion = constantes.modoALTA;

  // Lista que contiene las posibles clasificaciones
  protected CLista LClasif = null;

  // Dialog que contiene a este panel: SOLOPANEL
  CDialog dlgCasos;

  DialogTub dlgtub;

  // Hashtable con los datos
  Hashtable hsCasos = null;

  // Escuchadores
  PanCasoChkItemAdapter chkItemAdapter = new PanCasoChkItemAdapter(this);
  PanCasoFocusAdapter txtFocusAdapter = new PanCasoFocusAdapter(this);

  /**************** Componentes del panel *********************/

  // Organizacion del panel
  XYLayout xYLayout = new XYLayout();

  // Panel Sintomas
  GroupBox pnlSintomas = new GroupBox();

  // Fecha Inicio Sintomas
  Label lblFechaIniSint = new Label();
  // Correcci�n, para poner barras autom�ticamente (23-05-01 ARS)
//  CFechaSimple CfechaIniSint = new CFechaSimple("S");
  fechas.CFecha CfechaIniSint = new fechas.CFecha("S");

  // Asociado otro caso
  Checkbox chkAsociadoCaso = new Checkbox();
  Label lblAsociadoCaso = new Label();
  TextField txtAsociado = new TextField();

  // Colectivo
  Label lblColectivo = new Label();
  TextField txtColectivo = new TextField();

  // Derivado
  Label lblDerivado = new Label();
  Checkbox chkDerivado = new Checkbox();

  // Centro
  Label lblCentro = new Label();
  TextField txtCentro = new TextField();

  // Panel Diagnostico
  GroupBox pnlDiagnostico = new GroupBox();

  // Clinico
  Label lblDiagClinico = new Label();
  Checkbox chkDgClinico = new Checkbox();

  // Serologico
  Label lblDiagSerol = new Label();
  Checkbox chkDgSerologico = new Checkbox();

  // Microbiologico
  Label lblDiagMicro = new Label();
  Checkbox chkDgMicrobio = new Checkbox();

  // Otros
  Label lblDgOtros = new Label();
  TextField txtDgOtros = new TextField();

  // Clasificacion
  Label lblClasificacion = new Label();
  Choice choClasificacion = new Choice();

  /*
    // Constructor
    public PanCaso(CDialog dlg, int modo, Hashtable hsEntrada) {
      try  {
        // CApp y CDialog
        setApp(dlg.getCApp());
        dlgCasos = dlg;
        // Listas de entrada
        this.hsCasos = hsEntrada;
        LClasif = (CLista) hsEntrada.get("CLASIFICACIONES");
        jbInit();
        modoOperacion = modo;
        Inicializar(modoOperacion);
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
   */

  // Constructor
  public PanCaso(DialogTub dlg, int modo, Hashtable hsEntrada) {
    try {
      // CApp y CDialog
      setApp(dlg.getCApp());
      dlgtub = dlg;

      // Listas de entrada
      this.hsCasos = hsEntrada;
      LClasif = (CLista) hsEntrada.get("CLASIFICACIONES");

      jbInit();

      modoOperacion = modo;
      Inicializar(modoOperacion);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    // Organizacion del panel
    this.setSize(new Dimension(745, 170));
    xYLayout.setHeight(282);
    xYLayout.setWidth(755);
    this.setLayout(xYLayout);
    setBorde(false);

    // Labeles
    lblFechaIniSint.setText("Fecha Inicio Sintomas:");
    lblAsociadoCaso.setText("Asociado otro caso?:");
    lblColectivo.setText("Colectivo:");
    lblDerivado.setText("Derivado?:");
    lblCentro.setText("Centro:");
    lblDiagClinico.setText("Diagnostico: Clinico:");
    lblDiagSerol.setText("Serologico:");
    lblDiagMicro.setText("Microbiologico:");
    lblDgOtros.setText("Otros:");
    lblClasificacion.setText("Clasificacion:");

    pnlSintomas.setLabel("Sintomas");
    pnlSintomas.setFont(new Font("Dialog", 3, 12));
    pnlSintomas.setForeground(new Color(153, 0, 0));
    CfechaIniSint.setBackground(Color.white);

    pnlDiagnostico.setLabel("Diagnostico");
    pnlDiagnostico.setFont(new Font("Dialog", 3, 12));
    pnlDiagnostico.setForeground(new Color(153, 0, 0));

    // Panel Sintomas
    this.add(lblFechaIniSint, new XYConstraints(60, 47, 128, -1));
    this.add(CfechaIniSint, new XYConstraints(204, 47, 95, -1));
    this.add(lblAsociadoCaso, new XYConstraints(309, 47, 125, -1));
    this.add(chkAsociadoCaso, new XYConstraints(434, 47, 23, -1));
    this.add(txtAsociado, new XYConstraints(465, 47, 166, -1));
    this.add(lblColectivo, new XYConstraints(60, 72, 58, -1));
    this.add(txtColectivo, new XYConstraints(204, 72, 325, -1));
    this.add(lblDerivado, new XYConstraints(60, 97, 67, -1));
    this.add(chkDerivado, new XYConstraints(127, 97, 27, -1));
    this.add(lblCentro, new XYConstraints(154, 97, 49, -1));
    this.add(txtCentro, new XYConstraints(204, 97, 325, -1));
    this.add(pnlSintomas, new XYConstraints(27, 33, 628, 100));

    // Panel Diagnosticos
    this.add(lblDiagClinico, new XYConstraints(60, 168, 123, -1));
    this.add(chkDgClinico, new XYConstraints(184, 168, 23, -1));
    this.add(lblDiagSerol, new XYConstraints(210, 168, 67, -1));
    this.add(chkDgSerologico, new XYConstraints(279, 168, 24, -1));
    this.add(lblDiagMicro, new XYConstraints(308, 168, 86, -1));
    this.add(chkDgMicrobio, new XYConstraints(395, 168, 24, -1));
    this.add(lblDgOtros, new XYConstraints(422, 168, 41, -1));
    this.add(txtDgOtros, new XYConstraints(465, 168, 166, -1));
    this.add(lblClasificacion, new XYConstraints(60, 193, 82, -1));
    this.add(choClasificacion, new XYConstraints(183, 193, 229, -1));
    this.add(pnlDiagnostico, new XYConstraints(27, 155, 630, 80));

    // Componentes invisibles a priori
    txtAsociado.setVisible(false);
    txtCentro.setVisible(false);
    lblCentro.setVisible(false);

    // Relleno lista de clasificaciones
    Common.writeChoiceDataGeneralCDDS(choClasificacion, LClasif, true, true, true);

    // Asociacion de escuchadores
    chkDerivado.setName("derivado");
    chkDerivado.addItemListener(chkItemAdapter);
    chkAsociadoCaso.setName("asociadocaso");
    chkAsociadoCaso.addItemListener(chkItemAdapter);
    CfechaIniSint.setName("CfechaIniSint");
    CfechaIniSint.addFocusListener(txtFocusAdapter);
    CfechaIniSint.requestFocus();

  }

  public void ChangedProtocolo() {

    // variable para controlar que cuando se grabe un protocolo y
    // se pulse grabar, si estamos en el panel del protocolo, se
    // grabe dicho protocolo

    //dlgtub.bGrabarProt = false;

    if (dlgtub.bChanging) {
      if (Common.ShowPregunta(this.app,
                              "El protocolo ha cambiado. �Desea grabarlo?")) {

        dlgtub.tabPanel.VerPanel("Protocolo Notificador");
        //dlgtub.grabarProtocolo();

        dlgtub.bChanging = false;
      }
      else {
        dlgtub.bChanging = false;
      }
    }
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar(modo)

  public void Inicializar() {
    switch (modoOperacion) {
      case constantes.modoESPERA:

        // Se deshabilitan todos los controles
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
      case constantes.modoALTA:
      case constantes.modoMODIFICACION:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case constantes.modoBAJA:
      case constantes.modoCONSULTA:
        enableControls(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  } // Fin Inicializar()

  // Habilita/Deshabilita los componentes segun el parametro
  private void enableControls(boolean en) {
    // Fecha Inicio Sintomas
    CfechaIniSint.setEnabled(en);

    // Asociado otro caso
    chkAsociadoCaso.setEnabled(en);
    txtAsociado.setEnabled(en);

    // Colectivo
    txtColectivo.setEnabled(en);

    // Derivado
    chkDerivado.setEnabled(en);
    // Centro
    txtCentro.setEnabled(en);

    //29/12/99: mlm - estos datos van ocultos----
    // Clinico
    chkDgClinico.setEnabled(en);
    // Serologico
    chkDgSerologico.setEnabled(en);
    // Microbiologico
    chkDgMicrobio.setEnabled(en);
    // Otros
    txtDgOtros.setEnabled(en);
    // Clasificacion
    choClasificacion.setEnabled(en); //------------
    Ocultar();
  } // Fin enableControls()

  private void Ocultar() {
    pnlDiagnostico.setVisible(false);

    // Clinico
    chkDgClinico.setVisible(false);
    // Serologico
    chkDgSerologico.setVisible(false);
    // Microbiologico
    chkDgMicrobio.setVisible(false);
    // Otros
    txtDgOtros.setVisible(false);
    // Clasificacion
    choClasificacion.setVisible(false);

    lblDiagClinico.setVisible(false);
    lblDiagSerol.setVisible(false);
    lblDiagMicro.setVisible(false);
    lblDgOtros.setVisible(false);
    lblClasificacion.setVisible(false);

  }

  // Recoge los datos que hay por los componentes y los introduce en un DatCasosTub
  public DatCasosTub recogerDatos(DatCasosTub resul) {

    resul.insert("FC_INISNT", this.getFechaIniSint());
    resul.insert("IT_ASOCIADO", this.getItAsociado());
    resul.insert("DS_ASOCIADO", this.txtAsociado.getText());
    resul.insert("DS_COLECTIVO", this.txtColectivo.getText());
    resul.insert("IT_DERIVADO", this.getItDerivado());
    resul.insert("DS_CENTRODER", this.txtCentro.getText());
    resul.insert("IT_DIAGCLI", this.getItDiagClin());
    resul.insert("IT_DIAGSERO", this.getItDiagSero());
    resul.insert("IT_DIAGMICRO", this.getItDiagMicro());
    resul.insert("DS_DIAGOTROS", this.txtDgOtros.getText());
    resul.insert("CD_CLASIFDIAG", this.getCodClasif());

    return resul;
  } // Fin recogerDatos()

  public void rellenarDatos(DatCasosTub data) {

    // Fecha Inicio Sintomas
    CfechaIniSint.setText(data.getFC_INISNT());
    CfechaIniSint.ValidarFecha();
    if (CfechaIniSint.getValid().equals("N")) {
      CfechaIniSint.setText("");

      // Asociado otro caso
    }
    chkAsociadoCaso.setState(data.getIT_ASOCIADO().equals("S") ? true : false);
    if (chkAsociadoCaso.getState()) {
      txtAsociado.setVisible(true);
      txtAsociado.setText(data.getDS_ASOCIADO());
    }

    // Colectivo
    txtColectivo.setText(data.getDS_COLECTIVO());

    // Derivado y Centro derivado
    chkDerivado.setState(data.getIT_DERIVADO().equals("S") ? true : false);
    if (chkDerivado.getState()) {
      lblCentro.setVisible(true);
      txtCentro.setVisible(true);
      txtCentro.setText(data.getDS_CENTRODER());
    }

    // Clinico
    chkDgClinico.setState(data.getIT_DIAGCLI().equals("S") ? true : false);

    // Serologico
    chkDgSerologico.setState(data.getIT_DIAGSERO().equals("S") ? true : false);

    // Microbiologico
    chkDgMicrobio.setState(data.getIT_DIAGMICRO().equals("S") ? true : false);

    // Otros
    txtDgOtros.setText(data.getDS_DIAGOTROS());

    // Clasificacion
    Common.selectChoiceElement(choClasificacion, LClasif, true,
                               data.getCD_CLASIFDIAG());

  } // Fin rellenarDatos()

  public boolean validarDatos() {
    // 1. PERDIDAS DE FOCO: No hay campos CD-Button-DS

    // 2. LONGITUDES

    // txtAsociado
    if (txtAsociado.getText().length() > 20) {
      Common.ShowError(this.getApp(),
          "La longitud del texto Asociado Otro Caso excede de 20 caracteres");
      txtAsociado.setText("");
      txtAsociado.requestFocus();
      return false;
    }

    // txtColectivo
    if (txtColectivo.getText().length() > 40) {
      Common.ShowError(this.getApp(),
          "La longitud del texto Colectivo excede de 40 caracteres");
      txtColectivo.setText("");
      txtColectivo.requestFocus();
      return false;
    }

    // txtCentro
    if (txtCentro.getText().length() > 40) {
      Common.ShowError(this.getApp(),
                       "La longitud del texto Centro excede de 40 caracteres");
      txtCentro.setText("");
      txtCentro.requestFocus();
      return false;
    }

    // txtDgOtros
    if (txtDgOtros.getText().length() > 20) {
      Common.ShowError(this.getApp(),
          "La longitud del texto Diagnostico Otros excede de 40 caracteres");
      txtDgOtros.setText("");
      txtDgOtros.requestFocus();
      return false;
    }

    // 3. DATOS NUMERICOS: No hay

    // 4. CAMPOS OBLIGATORIOS: No hay

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin  validarDatos()

  // Retorna la fecha de inicio de s�ntomas
  private String getFechaIniSint() {
    // Antes de recogerla, se actualiza
    ActCFecha(CfechaIniSint, CfechaIniSint.getText().trim());
    return CfechaIniSint.getText().trim();
  }

  // Retorna el Cod de la clasificacion seleccionada
  private String getCodClasif() {
    // Choice con primera entrada vacia, CD y DS
    return Common.getChoiceCDDataGeneralCDDS(
        choClasificacion, LClasif, true, true, true);
  } // Fin getCodClasif()

  // Retorna "S" o "N" segun el estado del check IT_ASOCIADO
  private String getItAsociado() {
    if (chkAsociadoCaso.getState()) {
      return "S";
    }
    return "N";
  } // Fin getItAsociado()

  // Retorna "S" o "N" segun el estado del check IT_DERIVADO
  private String getItDerivado() {
    if (chkDerivado.getState()) {
      return "S";
    }
    return "N";
  } // Fin getItDerivado()

  // Retorna "S" o "N" segun el estado del check IT_DIAGCLIN
  private String getItDiagClin() {
    if (chkDgClinico.getState()) {
      return "S";
    }
    return "N";
  } // Fin getItDiagClin()

  // Retorna "S" o "N" segun el estado del check IT_DIAGSERO
  private String getItDiagSero() {
    if (chkDgSerologico.getState()) {
      return "S";
    }
    return "N";
  } // Fin getItDiagSero()

  // Retorna "S" o "N" segun el estado del check IT_DIAGMICRO
  private String getItDiagMicro() {
    if (chkDgMicrobio.getState()) {
      return "S";
    }
    return "N";
  } // Fin getItDiagMicro()

  /*************** Funciones auxiliares ******************/

  // Actualiza y valida la fecha de un CFechaSimple
// ARS 23-05-01 Para poner barras
//  private void ActCFecha(CFechaSimple CFecha, String sFecha) {
  private void ActCFecha(fechas.CFecha CFecha, String sFecha) {
    CFecha.setText(sFecha);
    CFecha.ValidarFecha();
    CFecha.setText(CFecha.getFecha());
  }

  /******************* Manejadores ********************/

  // Manejador de TextField CfechaIniSint
  void CfechaIniSintFocusLost() {

    // Se valida y fija la fecha introducida
    ActCFecha(CfechaIniSint, CfechaIniSint.getText().trim());

    // Se comprueba que no sea mayor que la fecha actual
    // Si es mayor, se actualiza a la fecha actual
    if (UtilEDO.fecha1MayorqueFecha2(getFechaIniSint(), UtilEDO.getFechaAct())) {
      /*ShowWarning("Introduzca fecha de inicio de s�ntomas menor o igual a la fecha actual");*/
      ActCFecha(CfechaIniSint, UtilEDO.getFechaAct());
    }
  } // Fin CfechaIniSintFocusLost()

  // Manejador de Check AsociadoCaso
  void chkAsociadoCasoItemStateChanged(ItemEvent e) {
    if (chkAsociadoCaso.getState()) {
      txtAsociado.setVisible(true);
      validate(); // Redispone los componentes en un contenedor
    }
    else {
      txtAsociado.setVisible(false);
      txtAsociado.setText("");
    }
  } // Fin chkAsociadoCasoItemStateChanged()

  // Manejador de Check Derivado
  void chkDerivadoItemStateChanged(ItemEvent e) {
    if (chkDerivado.getState()) {
      lblCentro.setVisible(true);
      txtCentro.setVisible(true);
      validate(); // Redispone los componentes en un contenedor
    }
    else {
      lblCentro.setVisible(false);
      txtCentro.setVisible(false);
      txtCentro.setText("");
    }
  } // Fin chkDerivadoItemStateChanged()

} // Fin clase PanCaso

/******************* CLASES ESCUCHADORAS ********************/

// Escuchador itemStateChanged de checks
class PanCasoChkItemAdapter
    implements java.awt.event.ItemListener {
  PanCaso adaptee;

  PanCasoChkItemAdapter(PanCaso adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    String sName = ( (Component) e.getSource()).getName();

    // Segun el check presionado se hace una cosa u otra
    if (sName.equals("derivado")) {
      adaptee.chkDerivadoItemStateChanged(e);
    }
    else if (sName.equals("asociadocaso")) {
      adaptee.chkAsociadoCasoItemStateChanged(e);
    }
  }
} // Fin clase PanCasoChkItemAdapter

// Escuchador FocusLost de TextField
class PanCasoFocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanCaso adaptee;
  FocusEvent evt;

  PanCasoFocusAdapter(PanCaso adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {
  }

  public void run() {
    // ARS 23-05-01 corregido para poner barras autom�ticas
//    if(((CFechaSimple)evt.getSource()).getName().equals("CfechaIniSint")){
    if ( ( (fechas.CFecha) evt.getSource()).getName().equals("CfechaIniSint")) {
      adaptee.CfechaIniSintFocusLost();
    }
  }
} // Fin clase PanCasoFocusAdapter
