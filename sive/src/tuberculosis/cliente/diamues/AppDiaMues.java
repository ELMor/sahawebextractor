/**
 * Clase: AppDiaMues
 * Paquete: tuberculosis.cliente.diamues
 * Hereda: CApp
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 30/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Applet de prueba para visualizar el dialogo DiaMues
 */

package tuberculosis.cliente.diamues;

import capp.CApp;
import capp.CLista;
import comun.DataGeneralCDDS;
import tuberculosis.datos.panmues.DatCasMuesCS;
import tuberculosis.datos.panmues.DatCasMuesSC;

public class AppDiaMues
    extends CApp {

  CLista LMotTratamiento = new CLista();

  public void init() {
    setTitulo("Dialogo de actualizacion de Muestras");
    super.init();
  }

  public void start() {

    // Listas de catalogos rellenadas a pelo

    // Relleno a pelo de la lista de motivos de tratamiento
    CLista LMotSalida = new CLista();
    DataGeneralCDDS d1 = new DataGeneralCDDS("01", "Fallecimiento");
    DataGeneralCDDS d2 = new DataGeneralCDDS("02", "Inicio Sintomas");
    DataGeneralCDDS d3 = new DataGeneralCDDS("03", "Evolucion Lenta");
    DataGeneralCDDS d4 = new DataGeneralCDDS("04", "Cura");
    LMotTratamiento.addElement(d1);
    LMotTratamiento.addElement(d2);
    LMotTratamiento.addElement(d3);
    LMotTratamiento.addElement(d4);

    // Relleno de los datos de un notificador
    DatCasMuesCS caso = new DatCasMuesCS("H01A", "1999", "14", "1",
                                         "17/11/1999", "11/11/1999", "1", "",
                                         "D Mora", "N", "ADMON", "17/11/1999");
    // Relleno de los datos de una muestra
    DatCasMuesSC mues = new DatCasMuesSC("5", "31/12/1999",
                                         "", "", "",
                                         "", "", "",
                                         "", "", "", "", "");

    // Dialogo
    /*DiaMues dlg = new DiaMues(this,
      constantes.modoALTA,
      LMotTratamiento,
      caso,
      mues);
         dlg.show();
         dlg = null;  */

    return;
  }
} // endclass AppDiaMues