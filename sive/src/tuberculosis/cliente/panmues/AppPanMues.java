package tuberculosis.cliente.panmues;

import java.util.Hashtable;

import capp.CApp;
import capp.CLista;
import comun.DataGeneralCDDS;
import comun.constantes;
import tuberculosis.cliente.diaresul.DiaOtrosMuestra;
import tuberculosis.datos.panmues.DatCasMuesCS;

public class AppPanMues
    extends CApp {

  Hashtable hs = new Hashtable();
  DatCasMuesCS notif = null;

  public void init() {
    super.init();
    setTitulo("Hola: applet prueba PanMues");

    // Hash con listas de catalogos
    CLista listaTecnicasLaboratorio = new CLista();
    CLista listaMuestrasLaboratorio = new CLista();
    CLista listaValoresMuestra = new CLista();
    CLista listaTiposMicobacteria = new CLista();
    CLista listaEstudiosResistencia = new CLista();

    listaTecnicasLaboratorio.addElement(new DataGeneralCDDS("1", "An�lisis"));
    listaTecnicasLaboratorio.addElement(new DataGeneralCDDS("2",
        "Estudio Interno"));
    listaTecnicasLaboratorio.addElement(new DataGeneralCDDS("3",
        "Estudio Huesos"));

    listaMuestrasLaboratorio.addElement(new DataGeneralCDDS("1", "Sangre"));
    listaMuestrasLaboratorio.addElement(new DataGeneralCDDS("2", "Orina"));
    listaMuestrasLaboratorio.addElement(new DataGeneralCDDS("3", "Huesos"));

    listaValoresMuestra.addElement(new DataGeneralCDDS("1", "Positivo"));
    listaValoresMuestra.addElement(new DataGeneralCDDS("2", "Negativo"));

    listaTiposMicobacteria.addElement(new DataGeneralCDDS("A", "Tipo A"));
    listaTiposMicobacteria.addElement(new DataGeneralCDDS("B", "Tipo B"));

    listaEstudiosResistencia.addElement(new DataGeneralCDDS("1", "Agua"));
    listaEstudiosResistencia.addElement(new DataGeneralCDDS("2", "Ox�geno"));
    listaEstudiosResistencia.addElement(new DataGeneralCDDS("3", "Fuego"));
    listaEstudiosResistencia.addElement(new DataGeneralCDDS("4", "Proteinas"));

    hs.put("TIPO_TECNICALAB", listaTecnicasLaboratorio);
    hs.put("MUESTRA_LAB", listaMuestrasLaboratorio);
    hs.put("VALOR_MUESTRA", listaValoresMuestra);
    hs.put("TMICOBACTERIA", listaTiposMicobacteria);
    hs.put("ESTUDIORESIS", listaEstudiosResistencia);

  }

  public void start() {

    String Equipo = "H01A";
    String Ano = "1999";
    String Sem = "14";
    String Edo = "1";
    String FNotif = "17/11/1999";
    String FRecep = "11/11/1999";
    String Fuente = "1";
    String HistClin = "";
    String Decl = "D Mora";
    String FlagPrim = "N";
    String Ope = "D_1";
    String FUlt = "09/12/1999";

    notif = new DatCasMuesCS(Equipo, Ano, Sem, Edo, FNotif, FRecep,
                             Fuente, HistClin, Decl, FlagPrim, Ope, FUlt);

    DiaOtrosMuestra dlgOtrosM = null;
    // Creacion y visualizacion del panel de tratamientos
    PanMues pm = new PanMues(this, constantes.modoMODIFICACION, hs, notif,
                             dlgOtrosM);

    VerPanel("", pm);
  }

}
