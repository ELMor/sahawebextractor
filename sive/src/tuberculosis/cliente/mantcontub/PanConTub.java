/**
 * Clase: PanConTub
 * Paquete: tuberculosis.cliente.mantcontub
 * Hereda: CPanel
 * Autor: Pedro Antonio D�az Pardo (PDP)
 * Fecha Inicio: 25/01/2000
     * Analisis Funcional: Punto 2. Mantenimiento Contactos de Casos de Tuberculosis.
 * Descripcion: Implementacion del panel que permite visualizar los datos
     principales de los contactos de tuberculosis. Los registros mostrados dependen
    de los valores suministrados a los campos de filtrado.
    Ademas permite acceder a la pantalla de alta/modificacion/baja/cierre
    de los datos asociados a un determinado contacto de tuberculosis.
 */

package tuberculosis.cliente.mantcontub;

import java.net.URL;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import capp.USUButtonControl;
import comun.Common;
import comun.Comunicador;
import comun.Fechas;
import comun.constantes;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import sapp.StubSrvBD;
import tuberculosis.cliente.diaconcasotub.DiaConCasoTub;
import tuberculosis.cliente.diacontub.DiaConTub;
// import tuberculosis.servidor.mantcontub.*;
import tuberculosis.datos.mantcontub.DatConTub;
import tuberculosis.datos.mantregtub.DatMntRegTubCS;
import tuberculosis.datos.mantregtub.DatMntRegTubSC;

public class PanConTub
    extends CPanel {

  // Para saber si est� seleccionado un caso
  private boolean bOperacion = false;

  // Modos de entrada al panel
  private final int modoMANTENIMIENTO = constantes.modoMANTENIMIENTO;

  final int modoCERRAR = constantes.modoCERRAR;
  final int modoDESHACERCIERRE = constantes.modoDESHACERCIERRE;
  final int modoCASOSTUB = constantes.modoCASOSTUB;

  int modoEntrada = modoMANTENIMIENTO; // Por defecto

  // Modos de operacion del panel
  final int modoESPERA = 0;
  final int modoBUSCAR = 1;

  int modoOperacion = modoBUSCAR; // Por defecto

  //Flags de habilitado-deshabilitado botonera de mantenimiento
  boolean bFirst = false;
  boolean bSecond = false;
  boolean bAno = false;
  boolean bReg = false;

  // Keys Modo Usu
  String keyBtnAnadir = "";
  String keyBtnModificar = "";
  String keyBtnBorrar = "";

  String claveBtnAnadir = "20001";
  String claveBtnModificar = "20002";
  String claveBtnBorrar = "20003";

  // Flags de permisos de usuario (solo existe el permiso de modificacion)
  boolean bAlta = false;
  boolean bMod = false;
  boolean bBaja = false;
  boolean bConsulta = false;

  // Datos para la busqueda de registros
  private DatConTub dataBusqueda = null;

  // Lista de los parametros de busqueda
  public CLista listaBusquedaConTub = null;

  // Lista de Registros de contactos de Tuberculosis
  public CLista listaRegConTub = null;

  // Cat�logos utilizados por este panel o paneles llamados desde �ste
  public Hashtable catalogos = new Hashtable();

  //Datos Registro seleccionado
  public DatConTub RegSel = null;

  // Stub's
  protected StubSrvBD stubCliente = null;

  // Localizacion del servlets
//  private final String strSERVLET_CONTUB = "servlet/SrvMntCont";
  private final String strSERVLET_CONTUB = constantes.strSERVLET_CONTUB;
//  private final String strSERVLET_CATALOGOS_CASOSTUB = "servlet/SrvCatCasoTub";
  private final String strSERVLET_CATALOGOS_CASOSTUB = constantes.
      strSERVLET_CATALOGOS_CASOSTUB;

  private final String strSERVLET_CASOSTUB = constantes.strSERVLET_CASOSTUB;

  // Para gestionar cambios en cajas de texto
  protected String s_Ano = "";
  protected String s_Registro = "";
  protected String s_RegDia = "";

  // Bloqueo de RTBC
  private String CD_OPE_RTBC = null;
  private java.sql.Timestamp FC_ULTACT_RTBC = null;

  // �rea y distrito de Gesti�n del caso
  private String CD_NIVEL_1_GE = null;
  private String CD_NIVEL_2_GE = null;

  // Datos para conocer las 'coordenadas' de EDOIND para
  // el protocolo
  private String CD_NIVEL_1_EDOIND = null;
  private String CD_NIVEL_2_EDOIND = null;

  //Para enviar datos al di�logo
  //private String sAno = null;

  //Datos para el envio de informaci�n al di�logo
  public String AnoCon = null;

  //Nombre enfermo tuberculosis
  public String sNombre = null;

  //C�digo de enfermo
  public String sCodEnf = null;

  // Contenedores de im�genes
  protected CCargadorImagen imgs = null;
  protected CCargadorImagen imgs_mantenimiento = null;
  protected CCargadorImagen imgs_tabla = null;
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/refrescar.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/declaracion.gif",
      "images/declaracion2.gif"};

  final String imgNAME_mantenimiento[] = {
      "images/alta2.gif",
      "images/modificacion2.gif",
      "images/baja2.gif"};
  final String imgNAME_tabla[] = {
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif"};

  /* -------------------- CONTROLES ------------------------ */

  // Organizacion del panel
  private XYLayout lyXYLayout = new XYLayout();

  // A�o/Registro
  private Label lblAnoReg = new Label("A�o/Registro:");
  private TextField txtAno = new TextField();
  private boolean btxtAno = true;
  private Label lblBarra = new Label("/");
  private TextField txtReg = new TextField();
  private boolean btxtReg = true;

  // Lupa y Nombre enfermo
  private ButtonControl btnLupaEnfermo = new ButtonControl();
  private Label lblNomEnfermo = new Label("");
  private boolean bbtnLupaEnfermo = true;

  // Boton Buscar
  private ButtonControl btnBuscar = new ButtonControl();

  // Tabla que visualiza y permite navegar entre los registros de contactos de tuberculosis
  public CTabla tabla = new CTabla();

  // Controles para el mantenimiento y navegacion entre los registros de la tabla
  //private ButtonControl btnAlta = null;
  //private ButtonControl btnModificar = null;
  //private ButtonControl btnBaja = null;

  private USUButtonControl btnAlta = null;
  private USUButtonControl btnModificar = null;
  private USUButtonControl btnBaja = null;

  private ButtonControl btnPrimero = null;
  private ButtonControl btnAnterior = null;
  private ButtonControl btnSiguiente = null;
  private ButtonControl btnUltimo = null;

  // Botones de cierre y deshacer cierre del registro de tuberculosis
  private ButtonControl btnCerrar = new ButtonControl();
  private ButtonControl btnDeshacerCierre = new ButtonControl();

  // Escuchadores
  PanConTubFocusAdapter focusAdapter = null;
  PanConTubActionAdapter actionAdapter = null;
  PanConTubTablaDobleClick tablaDobleClickListener = null;
  //PanTablaItemListener tablaItemListener = null;

  // Interfaz con otros componentes

  // Niveles de gesti�n
  public void setCD_NIVEL_1_GE(String s) {
    CD_NIVEL_1_GE = s;
  }

  public void setCD_NIVEL_2_GE(String s) {
    CD_NIVEL_2_GE = s;
  }

  public String getCD_NIVEL_1_GE() {
    return CD_NIVEL_1_GE;
  }

  public String getCD_NIVEL_2_GE() {
    return CD_NIVEL_2_GE;
  }

  // Niveles de EDOIND para cargar el protocolo adecuado
  public void setCD_NIVEL_1_EDOIND(String s) {
    CD_NIVEL_1_EDOIND = s;
  }

  public void setCD_NIVEL_2_EDOIND(String s) {
    CD_NIVEL_2_EDOIND = s;
  }

  public String getCD_NIVEL_1_EDOIND() {
    return CD_NIVEL_1_EDOIND;
  }

  public String getCD_NIVEL_2_EDOIND() {
    return CD_NIVEL_2_EDOIND;
  }

  // Para que en las altas se disponga de datos de RTBC actualizados
  public void setCD_OPE_RTBC(String s) {
    CD_OPE_RTBC = s;
  }

  public void setFC_ULTACT_RTBC(java.sql.Timestamp ts) {
    FC_ULTACT_RTBC = ts;
  }

  public String getAnoCon() {
    AnoCon = txtAno.getText();
    //sAno = AnoCon.getCD_ANO();
    return AnoCon;
  }

  /* CONSTRUCTOR */
  public PanConTub(CApp a, int modoVent) {
    try {

      setApp(a);
      setBorde(false);

      modoEntrada = modoVent;
      stubCliente = new StubSrvBD(new URL(this.app.getURL() + strSERVLET_CONTUB));

      //Gesti�n claves de Usu
      keyBtnAnadir = claveBtnAnadir;
      keyBtnModificar = claveBtnModificar;
      keyBtnBorrar = claveBtnBorrar;

      jbInit();

      // Carga de los cat�logos desde la BD (Motivos de salida, Pa�ses y CAs)
      // Son cargados en la Hashtable denominada catalogos
      CargarListas();

      if (!this.app.getANYO_DEFECTO().equals("")) {
        txtAno.setText(a.getANYO_DEFECTO());

        //Inicializa modo de operaci�n
      }
      bFirst = true;
      Inicializar();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // Funcion de inicializacion de los controles del panel
  void jbInit() throws Exception {

    // Vars. para colocar los componentes
    int pX = 0; // Posicion en X
    int pY = 0; // Posicion en Y
    int longX = 0; // Longitud en X de un componente
    int longY = 0; // Longitud en Y de un componente

    // Obtencion del a�o actual
    Calendar cal = Calendar.getInstance();
    int iAnoActual;

    // Clase escuchadora de los eventos relacionados con el foco
    focusAdapter = new PanConTubFocusAdapter(this);
    actionAdapter = new PanConTubActionAdapter(this);
    tablaDobleClickListener = new PanConTubTablaDobleClick(this);
    //tablaItemListener = new PanTablaItemListener(this);

    // Medidas y organizacion del panel
    this.setSize(new Dimension(608, 304));
    lyXYLayout.setHeight(370);
    lyXYLayout.setWidth(680);
    this.setLayout(lyXYLayout);

    // Carga de las imagenes generales (lupa, buscar)
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // A�o/Registro del registro de tuberculosis. A�o actual por defecto
    pX = 15;
    longX = 90;
    pY = 15;
    longY = -1;
    this.add(lblAnoReg, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 40;
    txtAno.setBackground(new Color(255, 255, 150));
    this.add(txtAno, new XYConstraints(pX, pY, longX, longY));
    iAnoActual = cal.get(cal.YEAR);
    txtAno.setText(new Integer(iAnoActual).toString());
    txtAno.setName("Ano");
    txtAno.addFocusListener(focusAdapter);
//    txtAno.requestFocus();

    pX += longX + 5;
    longX = 5;
    this.add(lblBarra, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 50;
    //txtReg.setBackground(new Color(255, 255, 150));
    this.add(txtReg, new XYConstraints(pX, pY, longX, longY));
    txtReg.setName("Registro");
    txtReg.addFocusListener(focusAdapter); //<------  Lo necesito
//    txtReg.requestFocus();

    // Lupa de busqueda y Nombre del contacto
    pX += longX + 10;
    longX = -1;
    pY = 15;
    longY = -1;
    this.add(btnLupaEnfermo, new XYConstraints(pX, pY - 1, longX, longY));
    btnLupaEnfermo.setImage(imgs.getImage(0));
    btnLupaEnfermo.setActionCommand("LupaEnfermo");
    btnLupaEnfermo.addActionListener(actionAdapter);
    pX += longX + 40;
    longX = 225;
    this.add(lblNomEnfermo, new XYConstraints(pX, pY, longX, longY));

    //Boton de b�squeda
    btnBuscar.setImage(imgs.getImage(1));
    btnBuscar.setLabel("Buscar");
    btnBuscar.setActionCommand("Buscar");
    btnBuscar.addActionListener(actionAdapter);
    pX += longX + 80;
    longX = -1;
    this.add(btnBuscar, new XYConstraints(pX, pY, longX, longY));

    // Se establecen los parametros de la tabla
    PintarTabla();
    pX = 15;
    pY += 45;
    longX = 557 + 75;
    longY = 125;
    this.add(tabla, new XYConstraints(pX, pY, longX, longY));
    tabla.addActionListener(tablaDobleClickListener);
    //tabla.addItemListener(tablaItemListener);

    // Carga de las im�genes de mantenimiento
    imgs_mantenimiento = new CCargadorImagen(app, imgNAME_mantenimiento);
    imgs_mantenimiento.CargaImagenes();

    // Creacion de los botones de mantenimiento de los registros de la tabla
    //btnAlta =  new ButtonControl(imgs_mantenimiento.getImage(0));
    //btnModificar  = new ButtonControl(imgs_mantenimiento.getImage(1));
    //btnBaja = new ButtonControl(imgs_mantenimiento.getImage(2));

    btnAlta = new USUButtonControl(keyBtnAnadir, app);
    btnAlta.setImage(imgs_mantenimiento.getImage(0));
    btnModificar = new USUButtonControl(keyBtnModificar, app);
    btnModificar.setImage(imgs_mantenimiento.getImage(1));
    btnBaja = new USUButtonControl(keyBtnBorrar, app);
    btnBaja.setImage(imgs_mantenimiento.getImage(2));

    // Asignaci�n a variables booleanas
    bAlta = btnAlta.isEnabled();
    bBaja = btnBaja.isEnabled();
    bMod = btnModificar.isEnabled();
    bConsulta = ! (bAlta || bBaja || bMod);

    btnModificar.setActionCommand("Modificar");
    btnModificar.addActionListener(actionAdapter);
    btnAlta.setActionCommand("Alta");
    btnAlta.addActionListener(actionAdapter);
    btnBaja.setActionCommand("Baja");
    btnBaja.addActionListener(actionAdapter);

    pX = 15;
    longX = 25;
    pY += longY + 10;
    longY = -1;
    this.add(btnAlta, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnModificar, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnBaja, new XYConstraints(pX, pY, longX, longY));

    // Carga de las im�genes de navegacion
    imgs_tabla = new CCargadorImagen(app, imgNAME_tabla);
    imgs_tabla.CargaImagenes();

    // Creacion de los botones de navegacion por los registros de la tabla
    btnPrimero = new ButtonControl(imgs_tabla.getImage(0));
    btnPrimero.setActionCommand("Primero");
    btnPrimero.addActionListener(actionAdapter);
    btnAnterior = new ButtonControl(imgs_tabla.getImage(1));
    btnAnterior.setActionCommand("Anterior");
    btnAnterior.addActionListener(actionAdapter);
    btnSiguiente = new ButtonControl(imgs_tabla.getImage(2));
    btnSiguiente.setActionCommand("Siguiente");
    btnSiguiente.addActionListener(actionAdapter);
    btnUltimo = new ButtonControl(imgs_tabla.getImage(3));
    btnUltimo.setActionCommand("Ultimo");
    btnUltimo.addActionListener(actionAdapter);
    pX = 440 + 75;
    longX = 25;
    longY = 25;
    this.add(btnPrimero, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnAnterior, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnSiguiente, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnUltimo, new XYConstraints(pX, pY, longX, longY));

  } // Fin jbInit()

  // Establece el numero de columnas de la tabla y las cabeceras de las mismas
  public void PintarTabla() {
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "55\n150\n200\n150\n50"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("C�digo\nNombre\nApellidos\nFecha de Nacimiento\nC/R"), '\n'));
    tabla.setNumColumns(5);
    tabla.setRowHeight(20);
    tabla.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    tabla.setInsets(new Insets(5, 5, 5, 5));
  } // fin PintarTabla()

  public void Inicializar(int modo) {
    boolean bFirst = true;
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar(modo)

  // Implementacion del metodo abstracto de Cpanel
  public void Inicializar() {

    switch (modoOperacion) {
      case modoBUSCAR:

        // A�o / Registro
        txtAno.setEnabled(true);
        txtReg.setEnabled(true);
        //Lupa / Etiqueta
        btnLupaEnfermo.setEnabled(true);
        lblNomEnfermo.setEnabled(true);
        // Tabla y botones

        btnBuscar.setEnabled(bOperacion);

        if (tabla.getSelectedIndex() < 0) {
          btnAlta.setEnabled(bOperacion);
          btnModificar.setEnabled(false);
          btnBaja.setEnabled(false);
        }
        else {
          btnAlta.setEnabled(true);
          btnModificar.setEnabled(true);
          if (btnModificar.isEnabled() == false) {
            btnModificar.setEnabledForzado(bConsulta);
          }
          btnBaja.setEnabled(hayPermiso());
        }

        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        tabla.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case modoESPERA:

        // A�o / Registro
        txtAno.setEnabled(false);
        txtReg.setEnabled(false);
        //Lupa / Etiqueta
        btnLupaEnfermo.setEnabled(false);
        lblNomEnfermo.setEnabled(true);

        // Botones de modificaci�n/navegaci�n por la tabla

        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);

        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);

        // Deshabilitamos la tabla
        tabla.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

    // Repintado de la pantalla
    this.doLayout();

  } // Fin Inicializar()

  // Gesti�n de Autorizaciones
  //
  protected boolean esNivel1Autorizado(String cd_nivel_1) {
    boolean bValor = false;

    Vector vNivel1 = this.app.getCD_NIVEL_1_AUTORIZACIONES();

    for (int i = 0; i < vNivel1.size() && bValor == false; i++) {
      bValor = ( (String) vNivel1.elementAt(i)).equals(cd_nivel_1);
    }

    return bValor;
  }

  //
  protected boolean esNivel2Autorizado(String cd_nivel_2) {
    boolean bValor = false;

    Vector vNivel2 = this.app.getCD_NIVEL_2_AUTORIZACIONES();

    for (int i = 0; i < vNivel2.size() && bValor == false; i++) {
      bValor = ( (String) vNivel2.elementAt(i)).equals(cd_nivel_2);
    }

    return bValor;
  }

  // Devuelve true si el usuario tiene permisos sobre la fila
  // seleccionada (false en caso contrario)
  protected boolean hayPermiso() {
    boolean bValor = true;

    int iPerfil = this.app.getPerfil();

    if (iPerfil == 3 || iPerfil == 4) {
      bValor = esNivel1Autorizado(getCD_NIVEL_1_GE());
      if (bValor && iPerfil == 4) {
        bValor = esNivel2Autorizado(getCD_NIVEL_2_GE());
      }
    }

    return bValor;
  }

  // Ganancia de foco de Registro
  void txtRegFocusGained() {
    s_Registro = new String(txtReg.getText().trim());
    /*
         boolean sAno = false;
         boolean sReg = false;
         if (s_Ano == "") {
       bAno = false;
     }else{
       bAno = true;
         }
         Inicializar(modoBUSCAR);
         /*
          if ((sAno&&sReg) == true){
        btnBuscar.setEnabled(true);
          }else{
        btnBuscar.setEnabled(false);
          }
          if ((sAno||sReg) == true){
        btnBuscar.setEnabled(false);
          }
      */
  }

  // Ganancia de foco de A�o
  void txtAnoFocusGained() {
    s_Ano = new String(txtAno.getText().trim());
    /*
         if (s_Registro == "") {
       bReg = false;
     }else{
       bReg = true;
         }
         Inicializar(modoBUSCAR);
         if ((sAno&&sReg) == true){
       btnBuscar.setEnabled(true);
         }else{
       btnBuscar.setEnabled(false);
         }
         if ((sAno||sReg) == true){
       btnBuscar.setEnabled(false);
         }
     */
  }

  // P�rdida de foco de txtAno
  void txtAnoFocusLost() {
    //Se ajusta el contenido
    txtAno.setText(txtAno.getText().trim());

    if (txtAno.getText().equals("")) {
      txtAno.setText(s_Ano);
    }

    // Obtiene el foco
    boolean bFocus = false;

    // Comprobaci�n del valor de txtAno
    // El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo
    if (comprobarAno(txtAno.getText())) {
      bFocus = true;
    }
    else {
      Common.ShowWarning(this.app,
          "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
      txtAno.setText(s_Ano);
      bFocus = true;
    }

//    if(bFocus)
//      txtAno.requestFocus();

    lblNomEnfermo.setText("");

  } // Fin txtA�oFocusLost()

  // Perdida de foco de txtReg
  void txtRegFocusLost() {
    bOperacion = false;

    // Se ajusta el contenido del a�o y registro
    if (s_Ano != null) {
      if (s_Registro != null) {
        bSecond = true;
      }
    }

    // Anterior modo
    int modo = modoOperacion;

    // Seguir la comprobacion del campo txtReg
    boolean bSeguirCompr = true;

    // Obtiene el foco
    boolean bFocus = false;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtReg.getText().equals("")) {
      bSeguirCompr = false;
      //bbtnLupaEnfermo = true;
      Inicializar(modoBUSCAR);
    }

    // Existe texto en el txtReg: Comprobaci�n del valor de txtReg
    // El registro es un valor num�rico, <= 5 d�gitos y positivo
    if (bSeguirCompr) {
      try {
        Integer f = new Integer(txtReg.getText());
        int reg = f.intValue();
        if ( (reg < 0) || (reg > 99999)) {
          ShowWarning(
              "El registro es un valor num�rico, menor o igual que 5 d�gitos y positivo.");
          txtReg.setText("");
          bSeguirCompr = false;
          bFocus = true;
        }
      }
      catch (Exception e) {
        ShowWarning(
            "El registro es un valor num�rico, menor o igual que 5 d�gitos y positivo.");
        txtReg.setText("");
        //txtReg.requestFocus();
        bSeguirCompr = false;
        bFocus = true;
      }
    }
    // Se resetea la tabla (el numero registro puede ser diferente)
    if (!txtReg.getText().equals(s_Registro)) {
      ResetearTabla();
      lblNomEnfermo.setText("");
    }

    // Se resetea la tabla (el a�o puede ser diferente)
    if (!txtAno.getText().equals(s_Ano)) {
      ResetearTabla();
      lblNomEnfermo.setText("");
    }

    // Existe algo en txtReg y es correcto: deshabilitamos el resto de campos de filtro
    if (bSeguirCompr) {

      // Modo ESPERA
      //int modo = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar(modoOperacion);

      // Creacion y relleno de la lista con los parametros de busqueda
      CLista listaBusquedaCasosTub = new CLista();
      listaBusquedaCasosTub.addElement(CargarListaBusquedaCasos());

      // Creacion de la lista de registros de casos de tuberculosis, para que
      // acto seguido sea llamado el servlet
      CLista listaRegCasosTub = new CLista();

      // Llamada al servlet: Intento de obtencion de los registros de tuberculosis
      try {

        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_CASOSTUB));

        listaRegCasosTub = (CLista) stubCliente.doPost(0, listaBusquedaCasosTub);
        listaRegCasosTub.trimToSize();

        // Escritura de los datos obtenidos en la tabla
        if (listaRegCasosTub != null) {
          if (listaRegCasosTub.size() > 0) {
            escribeNombre( (DatMntRegTubSC) listaRegCasosTub.firstElement());

            setCD_OPE_RTBC( ( (DatMntRegTubSC) listaRegCasosTub.firstElement()).
                           getCD_OPE());
            setFC_ULTACT_RTBC(Fechas.string2Timestamp( ( (DatMntRegTubSC)
                listaRegCasosTub.firstElement()).getFC_ULTACT()));

            setCD_NIVEL_1_EDOIND( ( (DatMntRegTubSC) listaRegCasosTub.
                                   firstElement()).getCD_NIVEL_1_EDOIND());
            setCD_NIVEL_2_EDOIND( ( (DatMntRegTubSC) listaRegCasosTub.
                                   firstElement()).getCD_NIVEL_2_EDOIND());

            setCD_NIVEL_1_GE( ( (DatMntRegTubSC) listaRegCasosTub.firstElement()).
                             getCD_NIVEL_1_GE());
            setCD_NIVEL_2_GE( ( (DatMntRegTubSC) listaRegCasosTub.firstElement()).
                             getCD_NIVEL_2_GE());

            btnBuscarActionPerformed(false);
            bOperacion = true;
          }
          else {
            ShowWarning("No se encontraron registros de tuberculosis");
          }
        }
        else {
          ShowWarning(
              "Error al realizar la b�squeda de registros de tuberculosis");
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        ShowWarning(
            "Error al recuperar los datos sobre registros de tuberculosis");
      }
      /*
             // Llamada al servlet: Intento de obtencion de los registros de casos de tuberculosis
             try{
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_CONTUB));
        listaRegConTub = (CLista) stubCliente.doPost(1, listaBusquedaConTub);
        listaRegConTub.trimToSize();
        // Escritura de los datos obtenidos en la tabla
        if (listaRegConTub != null){
          if (listaRegConTub.size() > 0)
            EscribirTabla();
          else {
            //ShowWarning("No se encontraron contactos de tuberculosis");
            bFirst = true;
            lblNomEnfermo.setText("");
          }
        }
        // Escritura del nombre
        if (bFirst) {
           EscribirNombre();
        }
             } catch (Exception e){
        e.printStackTrace();
        ShowWarning ("No se encontraron contactos de tuberculosis");
             }
             // Reestablecemos el modo de operacion
             if (listaRegConTub != null) {
         if (listaRegConTub.size() > 0) {
           bFirst = false;
        }
             } */

      modoOperacion = modo;
      Inicializar(modoOperacion);

    }
    else {
      // ShowWarning("Debe cumplimentar el n�mero de registro");
    }

//    if(bFocus)
//      txtReg.requestFocus();

    // Reestablecer el mod anterior
    if (!txtReg.getText().equals("")) {
      Inicializar(modo);
    }

  } // Fin txtRegFocusLost()

  //aqui empieza el boton de b�squeda
  void btnBuscarActionPerformed(boolean bMensaje) {
    // Se ajusta el contenido
    txtReg.setText(txtReg.getText().trim());

    // Anterior modo
    int modo = modoOperacion;

    // Seguir la comprobacion del campo txtReg
    boolean bSeguirCompr = true;

    // Obtiene el foco
    boolean bFocus = false;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtReg.getText().equals("")) {
      bSeguirCompr = false;

      //bbtnLupaEnfermo = true;

      Inicializar(modoBUSCAR);
    }

    // Existe texto en el txtReg: Comprobaci�n del valor de txtReg
    // El registro es un valor num�rico, <= 5 d�gitos y positivo
    /*    if (bSeguirCompr) {
          try {
            Integer f = new Integer(txtReg.getText());
            int reg = f.intValue();
            if ((reg <0) || (reg > 99999)){
              ShowWarning("El registro es un valor num�rico, menor o igual que 5 d�gitos y positivo.");
              txtReg.setText("");
              bSeguirCompr = false;
              bFocus = true;
            }
          } catch (Exception e) {
            ShowWarning("El registro es un valor num�rico, menor o igual que 5 d�gitos y positivo.");
            txtReg.setText("");
//        txtReg.requestFocus();
            bSeguirCompr = false;
            bFocus = true;
          }
        }*/

    // Se resetea la tabla (el numero registro puede ser diferente)
    if (!txtReg.getText().equals(s_Registro)) {
      ResetearTabla();
      //lblNomEnfermo.setText("");
    }

    // Se resetea la tabla (el a�o puede ser diferente)
    if (!txtAno.getText().equals(s_Ano)) {
      ResetearTabla();
      //lblNomEnfermo.setText("");
    }

    // Existe algo en txtReg y es correcto: deshabilitamos el resto de campos de filtro
    if (bSeguirCompr) {

      // Modo ESPERA
      //int modo = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar(modoOperacion);

      // Creacion y relleno de la lista con los parametros de busqueda
      listaBusquedaConTub = new CLista();
      CargarListaBusqueda();

      // Creacion de la lista de registros de casos de tuberculosis, para que
      // acto seguido sea llamado el servlet
      listaRegConTub = new CLista();

      // Llamada al servlet: Intento de obtencion de los registros de casos de tuberculosis
      try {

        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_CONTUB));

        listaRegConTub = (CLista) stubCliente.doPost(1, listaBusquedaConTub);
        listaRegConTub.trimToSize();

        // Escritura de los datos obtenidos en la tabla

        if (listaRegConTub != null) {
          if (listaRegConTub.size() > 0) {
            EscribirTabla();
          }
          else {
            ShowWarning("No se encontraron contactos de tuberculosis");
            bFirst = true;
            //lblNomEnfermo.setText("");
          }
        }
        //EscribirNombre();
      }
      catch (Exception e) {
        e.printStackTrace();
        if (bMensaje) {
          ShowWarning("No se encontraron contactos de tuberculosis");
        }
      }

      // Reestablecemos el modo de operacion
      if (listaRegConTub != null) {
        if (listaRegConTub.size() > 0) {
          bFirst = false;
        }
      }
      modoOperacion = modo;
      Inicializar(modoOperacion);

      Inicializar(modoBUSCAR);
    }
    else {
      ShowWarning("Debe cumplimentar el n�mero de registro");
    }

  } // Fin btnBuscarActionPerformed()

  // Manejador del Boton de Enfermos
  protected void btnLupaEnfermoActionPerformed() {
    // Requiere el foco
    //btnLupaEnfermo.requestFocus();

    if (!comprobarAno(txtAno.getText())) {
      Common.ShowWarning(this.app,
          "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
      return;
    }

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    //Inicializar(modoOperacion);

    // Abrimos el dialogo del contactos para elegir los filtros de b�squeda
    DiaConTub dlg = new DiaConTub(this.getApp());
    dlg.setAno(txtAno.getText());
    //dlg.setReg(txtReg.getText());

    if (dlg.iERROR != 0) { //0: error
      dlg.show();

      // Vuelta del dialogo
    }
    if (dlg.getAno() != null) {
      txtAno.setText(dlg.getAno());
      bOperacion = true;
    }

    if (dlg.getReg() != null) {
      txtReg.setText(dlg.getReg());
    }

    if (dlg.getNom() != null) {
      lblNomEnfermo.setText(dlg.getNom());
    }

    if (dlg.getCD_OPE_RTBC() != null) {
      CD_OPE_RTBC = dlg.getCD_OPE_RTBC();
    }

    if (dlg.getFC_ULTACT_RTBC() != null) {
      FC_ULTACT_RTBC = dlg.getFC_ULTACT_RTBC();
    }

    if (dlg.getCD_NIVEL_1_EDOIND() != null) {
      setCD_NIVEL_1_EDOIND(dlg.getCD_NIVEL_1_EDOIND());
    }

    if (dlg.getCD_NIVEL_2_EDOIND() != null) {
      setCD_NIVEL_2_EDOIND(dlg.getCD_NIVEL_2_EDOIND());
    }

    if (dlg.getCD_NIVEL_1_GE() != null) {
      setCD_NIVEL_1_GE(dlg.getCD_NIVEL_1_GE());
    }

    if (dlg.getCD_NIVEL_2_GE() != null) {
      setCD_NIVEL_2_GE(dlg.getCD_NIVEL_2_GE());
    }

    dlg = null;

    // Se resetea la tabla (ha podido cambiar el codigo del enfermo)
    ResetearTabla();

    // Reestablecemos el modo de operacion
    modoOperacion = modo;
    Inicializar(modoOperacion);

  } // Fin btnLupaEnfermoActionPerformed()

  // Escribe la etiqueta
  private void escribeNombre(DatMntRegTubSC dmrt) {
    lblNomEnfermo.setText(dmrt.getDS_NOMBREENFERMO() +
                          " " + dmrt.getDS_APE1ENFERMO() +
                          " " + dmrt.getDS_APE2ENFERMO());
  }

  // Devuelve el c�digo del enfermo seleccionado en la tabla
  private DatConTub getContacto() {
    DatConTub dct = null;
    int i = tabla.getSelectedIndex();
    if (i >= 0) {
      dct = (DatConTub) listaRegConTub.elementAt(i);
    }
    return dct;
  }

  // Devuelve una instancia de DatMntRegTubCS
  // para buscar el caso de tuberculosis seleccionado
  private DatMntRegTubCS CargarListaBusquedaCasos() {
    String Ano = txtAno.getText().trim();
    String Reg = txtReg.getText().trim();

    return new DatMntRegTubCS(Ano, Reg, null, null, null, null, null);
  } // Fin CargarListaBusquedaCasos()

  // Rellena la lista de busqueda con los parametros apropiados del panel
  void CargarListaBusqueda() {
    String Ano = txtAno.getText().trim();
    String Reg = txtReg.getText().trim();

    // Creamos una estructura de datos de busqueda y la rellenamos
    dataBusqueda = new DatConTub(Ano, Reg, null, null, null, null, null, null);

    // A�adimos la estructura a la lista de datos de busqueda
    listaBusquedaConTub.addElement(dataBusqueda);
  } // Fin CargarListaBusqueda()

  private CLista CargarListas() {

    boolean b = false;

    CLista parametros = new CLista();
    CLista listaSalida = new CLista();
    try {
      listaSalida = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            0,
                                            strSERVLET_CATALOGOS_CASOSTUB,
                                            parametros);

      if (listaSalida != null) {
        if (listaSalida.size() > 0) {
          catalogos.put(constantes.CLASIFICACIONES,
                        (CLista) listaSalida.elementAt(0));
          catalogos.put(constantes.PAISES, (CLista) listaSalida.elementAt(1));
          catalogos.put(constantes.CA, (CLista) listaSalida.elementAt(2));
          catalogos.put(constantes.TRAMEROS, (CLista) listaSalida.elementAt(3));
          catalogos.put(constantes.NUMEROS, (CLista) listaSalida.elementAt(4));
          catalogos.put(constantes.CALIFICADORES,
                        (CLista) listaSalida.elementAt(5));
          catalogos.put(constantes.SEXO, (CLista) listaSalida.elementAt(6));
          catalogos.put(constantes.TDOC, (CLista) listaSalida.elementAt(7));
        }
        else {
          ShowWarning("No existen datos para rellenar los cat�logos.");
          b = true;
        }
      }
      else {
        ShowWarning("Error al recuperar los cat�logos.");
        b = true;
      }

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      ShowWarning("Error al recuperar los cat�logos.");
      b = true;
    }
    // Si no se pueden recuperar los cat�logos semuestra la p�gina HTML anterior
    if (b) {
      try {
        System.runFinalization();
        System.gc();
        app.getAppletContext().showDocument(new URL(app.getCodeBase(),
            "default.html"), "_self");
      }
      catch (Exception excepc) {
        ;
      }
    }

    return (listaSalida);
  }

  // Cada registro de tuberculosis que proviene del sevlet se
  // introduce en la tabla
  public void EscribirTabla() {
    // Fila (registro tuberculosis)
    JCVector row = null;
    // Matriz de filas (registros de tuberculosis
    JCVector items = new JCVector();

    // Reseteo de la tabla
    ResetearTabla();

    // Datos de una fila de la tabla
    DatConTub dataResult = null;

    for (int i = 0; i < listaRegConTub.size(); i++) {
      dataResult = (DatConTub) listaRegConTub.elementAt(i);

      // Nuevo JCVector
      row = new JCVector();

      // C�digo
      row.addElement(dataResult.getCD_ENFERMO().trim());

      // NOMBRE
      row.addElement(dataResult.getDS_NOMBRE().trim());

      // APELLIDOS
      row.addElement(dataResult.getDS_APE1().trim() + " " +
                     dataResult.getDS_APE2().trim());
      // Fecha de Nacimiento
      row.addElement(dataResult.getFC_NAC());

      // Calculada o real
      if (dataResult.getIT_CALC() != null) {
        String Ind = dataResult.getIT_CALC().trim();
        if (Ind == "S") {
          String IndC = "C";
          row.addElement(IndC);
        }
        else {
          String IndR = "R";
          row.addElement(IndR);
        }
      }
      //row.addElement(dataResult.getIT_CALC().trim());

      /*
             // Escribe el nombre completo del enfermo
             String Nombre = (dataResult.getDS_NOMBRE().trim()+" "+
                       dataResult.getDS_APE1().trim()+" "+
                       dataResult.getDS_APE2().trim());
             lblNomEnfermo.setText(Nombre);
       */

      // Se a�ade una fila a la matriz
      items.addElement(row);
    } // Fin for()

    // Se establecen los  items
    tabla.setItems(items);

    // Verifica que sea la �ltima trama
    if (listaRegConTub.getState() == CLista.listaINCOMPLETA) {
      tabla.addItem("M�s datos ...");

      // Se selecciona el primer registro de la tabla
    }
    tabla.select(0);
    tabla.setTopRow(0);

    // Se repinta la tabla
    tabla.repaint();
  } // Fin EscribirTabla()

  // Resetea la tabla: borrado y repintado
  public void ResetearTabla() {
    tabla.clear();
    tabla.repaint();
    btnModificar.setEnabled(false);
    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);

  } // Fin ResetearTabla()

  /*public void EscribirNombre() {
    RegSel = (DatConTub)listaRegConTub.elementAt(tabla.getSelectedIndex());
    String Nombre = (RegSel.getDS_NOMBRE().trim()+" "+
                    RegSel.getDS_APE1().trim()+" "+
                    RegSel.getDS_APE2().trim());
    lblNomEnfermo.setText(Nombre);
     } */

  public String getCodEnf() {
    RegSel = (DatConTub) listaRegConTub.elementAt(tabla.getSelectedIndex());
    return (RegSel.getCD_ENFERMO().trim());
  }

  /*
     // click en Tabla
     protected void clickTabla(){
    if (lblNomEnfermo.getText() != "") {
      EscribirNombre();
    }
     }
   */
  // Manejador del boton Primero
  void btnPrimeroActionPerformed() {
    if (tabla.countItems() > 0) {
      tabla.select(0);
      tabla.setTopRow(0);
    }
  } // Fin btnPrimeroActionPerformed()

  // Manejador del boton Anterior
  void btnAnteriorActionPerformed() {
    if (tabla.countItems() > 0) {
      int index = tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        tabla.select(index);
        if (index - tabla.getTopRow() <= 0) {
          tabla.setTopRow(tabla.getTopRow() - 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  } // Fin btnAnteriorActionPerformed()

  // Manejador del boton Siguiente
  void btnSiguienteActionPerformed() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;
      int index = tabla.getSelectedIndex();

      if (index >= 0 && index < ultimo) {
        index++;
        tabla.select(index);
        if (index - tabla.getTopRow() >= 4) {
          tabla.setTopRow(tabla.getTopRow() + 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  } // Fin btnSiguienteActionPerformed()

  // Manejador del boton Ultimo
  void btnUltimoActionPerformed() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;

      if (ultimo >= 0) {
        tabla.select(ultimo);
        if (tabla.countItems() >= 4) {
          tabla.setTopRow(tabla.countItems() - 4);
        }
        else {
          tabla.setTopRow(0);
        }
      }
    }
  } // Fin btnUltimoActionPerformed()

  void btnAltaActionPerformed() {
    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Para pasar datos de protocolo
    Hashtable hEdoind = new Hashtable();

    hEdoind.put("CD_NIVEL_1_EDOIND", getCD_NIVEL_1_EDOIND());
    hEdoind.put("CD_NIVEL_2_EDOIND", getCD_NIVEL_2_EDOIND());

    // modificacion 09/06/2000
    // recuperamos los valores GE para los niveles 1 y 2 para
    // poder visualizar los protocolos de area y distrito
    hEdoind.put("CD_NIVEL_1_GE", getCD_NIVEL_1_GE());
    hEdoind.put("CD_NIVEL_2_GE", getCD_NIVEL_2_GE());

    //modificacion 12/02/2001
    // le paso informaci�n del a�o y n� de rtbc para el caso de que
    // haya selecci�n de enfermos que pueda calcular el protocolo
    Hashtable hRTBC = new Hashtable();
    hRTBC.put("CD_ARTBC", (String) txtAno.getText());
    hRTBC.put("CD_NRTBC", (String) txtReg.getText());

    // Abrimos el dialogo del contactos para elegir los filtros de b�squeda
    //DiaConCasoTub dlg = new DiaConCasoTub(this, this.getApp(), catalogos, constantes.modoALTA, null, hEdoind);
    DiaConCasoTub dlg = new DiaConCasoTub(this, this.getApp(), catalogos,
                                          constantes.modoALTA, hRTBC, hEdoind);

    dlg.setAno(txtAno.getText());
    dlg.setReg(txtReg.getText());
    dlg.setNom(lblNomEnfermo.getText());

    // Se pasa informaci�n para gestionar el
    // bloqueo sobre RTBC
    dlg.setCD_OPE_RTBC(CD_OPE_RTBC);
    dlg.setFC_ULTACT_RTBC(FC_ULTACT_RTBC);

    if (dlg.iOut != 0) { //cancelar
      dlg.show();

      //btnBuscarActionPerformed();
      //EscribirTabla();
    }
    setCD_OPE_RTBC(dlg.getCD_OPE_RTBC());
    setFC_ULTACT_RTBC(dlg.getFC_ULTACT_RTBC());

    dlg = null;

    // Se vac�a la tabla
    ResetearTabla();

    // Se recarga la tabla
    btnBuscarActionPerformed(false);

    // Se reestablece el modo
    bSecond = true;
    Inicializar(modoBUSCAR);

  }

  /*
   void btnBajaActionPerformed(){
    // Modo espera
    int modo =  modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);
    // Creaci�n del dialogo
    MostrarCaso(constantes.modoBAJA);
    // Se vac�a la tabla
    ResetearTabla();
    // Se recarga la tabla
    btnBuscarActionPerformed(false);
    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
     }
   */

  void btnBajaActionPerformed() {
    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Para pasar los par�metros del caso
    DatConTub dct = getContacto();

    // Para pasar datos de protocolo
    Hashtable hEdoind = new Hashtable();

    hEdoind.put("CD_NIVEL_1_EDOIND", getCD_NIVEL_1_EDOIND());
    hEdoind.put("CD_NIVEL_2_EDOIND", getCD_NIVEL_2_EDOIND());

    // modificacion 09/06/2000
    // recuperamos los valores GE para los niveles 1 y 2 para
    // poder visualizar los protocolos de area y distrito
    hEdoind.put("CD_NIVEL_1_GE", getCD_NIVEL_1_GE());
    hEdoind.put("CD_NIVEL_2_GE", getCD_NIVEL_2_GE());

    // Creaci�n del dialogo de modificaci�n
    // Abrimos el dialogo del contactos para elegir los filtros de b�squeda
    DiaConCasoTub dlg = new DiaConCasoTub(this, this.getApp(), catalogos,
                                          constantes.modoBAJA, dct, hEdoind);

    dlg.setAno(txtAno.getText());
    dlg.setReg(txtReg.getText());
    dlg.setNom(lblNomEnfermo.getText());

    if (dlg.iOut != 0) { //cancelar
      dlg.show();

      //btnBuscarActionPerformed();
      //EscribirTabla();
    }
    setCD_OPE_RTBC(dlg.getCD_OPE_RTBC());
    setFC_ULTACT_RTBC(dlg.getFC_ULTACT_RTBC());

    dlg = null;

    // Se recarga la tabla
    btnBuscarActionPerformed(false);

    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
  }

  void btnModificarActionPerformed() {

    // Modo de apertura del di�logo
    int modoApertura = constantes.modoMODIFICACION;

    if (!hayPermiso() || bConsulta) {
      modoApertura = constantes.modoCONSULTA;
    }

    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Para pasar los par�metros del caso
    DatConTub dct = getContacto();

    // Para pasar datos de protocolo
    Hashtable hEdoind = new Hashtable();

    hEdoind.put("CD_NIVEL_1_EDOIND", getCD_NIVEL_1_EDOIND());
    hEdoind.put("CD_NIVEL_2_EDOIND", getCD_NIVEL_2_EDOIND());

    // modificacion 09/06/2000
    // recuperamos los valores GE para los niveles 1 y 2 para
    // poder visualizar los protocolos de area y distrito
    hEdoind.put("CD_NIVEL_1_GE", getCD_NIVEL_1_GE());
    hEdoind.put("CD_NIVEL_2_GE", getCD_NIVEL_2_GE());

    // Creaci�n del dialogo de modificaci�n
    // Abrimos el dialogo de contactos para elegir los filtros de b�squeda
    DiaConCasoTub dlg = new DiaConCasoTub(this, this.getApp(), catalogos,
                                          modoApertura, dct, hEdoind);

    dlg.setAno(txtAno.getText());
    dlg.setReg(txtReg.getText());
    dlg.setNom(lblNomEnfermo.getText());

    // modificacion 06/07/2000
    // Se pasa informaci�n para gestionar el
    // bloqueo sobre RTBC
    dlg.setCD_OPE_RTBC(CD_OPE_RTBC);
    dlg.setFC_ULTACT_RTBC(FC_ULTACT_RTBC);

    if (dlg.iOut != 0) { //cancelar
      dlg.show();

    }
    setCD_OPE_RTBC(dlg.getCD_OPE_RTBC());
    setFC_ULTACT_RTBC(dlg.getFC_ULTACT_RTBC());

    //btnBuscarActionPerformed();
    //EscribirTabla();

    dlg = null;

    // Se recarga la tabla
    btnBuscarActionPerformed(false);

    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
  } // Fin btnModificarActionPerformed()

  // Manejador del doble-click en un registro de la tabla
  void TablaDobleClickActionPerformed() {
    boolean bModificar = btnModificar.isEnabled();
    if (bModificar) {
      // Modo espera
      int modo = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar(modoOperacion);

      // Para pasar los par�metros del caso
      DatConTub dct = getContacto();

      // Para pasar datos de protocolo
      Hashtable hEdoind = new Hashtable();

      hEdoind.put("CD_NIVEL_1_EDOIND", getCD_NIVEL_1_EDOIND());
      hEdoind.put("CD_NIVEL_2_EDOIND", getCD_NIVEL_2_EDOIND());

      // recuperamos los valores GE para los niveles 1 y 2 para
      // poder visualizar los protocolos de area y distrito
      hEdoind.put("CD_NIVEL_1_GE", getCD_NIVEL_1_GE());
      hEdoind.put("CD_NIVEL_2_GE", getCD_NIVEL_2_GE());

      // Creaci�n del dialogo de modificaci�n
      // Abrimos el dialogo de contactos para elegir los filtros de b�squeda
      DiaConCasoTub dlg = new DiaConCasoTub(this, this.getApp(), catalogos,
                                            constantes.modoMODIFICACION, dct,
                                            hEdoind);

      dlg.setAno(txtAno.getText());
      dlg.setReg(txtReg.getText());
      dlg.setNom(lblNomEnfermo.getText());

      if (dlg.iOut != 0) { //cancelar
        dlg.show();

      }
      setCD_OPE_RTBC(dlg.getCD_OPE_RTBC());
      setFC_ULTACT_RTBC(dlg.getFC_ULTACT_RTBC());

      //btnBuscarActionPerformed();
      //EscribirTabla();

      dlg = null;

      // Se recarga la tabla
      btnBuscarActionPerformed(false);

      // Se reestablece el modo
      modoOperacion = modo;
      Inicializar(modoOperacion);
    }
  } // Fin TablaDobleClickActionPerformed()

  /*
     private void MostrarDialogoModificacion(int modoVentana){
    // Obtenci�n del registro seleccionado el a�o y el registro
    JCVector row = new JCVector();
    row = (JCVector) tabla.getSelectedItem();
    String anoreg = (String) row.getFirst();
    String pano = anoreg.substring(0,4);
    String preg = anoreg.substring(5,anoreg.length());
    // Creaci�n del dialogo de modificaci�n
    int modoOpe = 0;
    if ( !bAlta &&  !bBaja &&  !bMod)
      modoOpe = constantes.modoCONSULTA;
    else
      modoOpe = constantes.modoMODIFICACION;
    DatMntRegTubSC RegSel = (DatMntRegTubSC)listaRegConTub.elementAt(tabla.getSelectedIndex());
    String fentrada = RegSel.getFC_FECHAENTRADA();
    DiaMntRegTub dlg = new DiaMntRegTub(this.getApp(),
                modoVentana, pano, preg, catalogos,
                modoOpe,fentrada);
    if ( dlg.iERROR != 0)  //0: error
      dlg.show();
    // Vuelta del dialogo
    dlg = null;
    // Para ver s�lo los activos
    ResetearTabla();
    btnBuscarActionPerformed(false);
     } // Fin MostrarDialogo()
   */
  /*
     private void MostrarCaso(int modo){//alta, modif, borrar
    DatMntRegTubSC data = null;
    DatRT datosEntrada = null;
    if (modo !=  constantes.modoALTA){
       data = (DatMntRegTubSC)listaRegConTub.elementAt(tabla.getSelectedIndex());
      datosEntrada = new DatRT();
      datosEntrada.put ("CD_ARTBC", data.getCD_ANO());
      datosEntrada.put ("CD_NRTBC", data.getCD_REG());
      datosEntrada.put ("NM_EDO", data.getNM_EDO());
    } else { //alta (lo envio vacio, ya se encarga pSup de pintar a�o defecto)
        datosEntrada = new DatRT();
    }
    int modoOpe = 0;
    if ( !bAlta &&  !bBaja &&  !bMod)
      modoOpe = constantes.modoCONSULTA;
    else
      modoOpe = modo;
    //cerrado, solo en modo consulta
    if (modo !=  constantes.modoALTA &&
        data.getIT_CERRADO().trim().equals("S"))
      modoOpe = constantes.modoCONSULTA;
    DialogTub dlg = new DialogTub (this.app, catalogos,
              modoOpe, datosEntrada);
    dlg.show();
    if (dlg.iOut == -1) {//cancelar
        dlg = null;
        return;
    }
    else { //aceptar
      // Para ver s�lo los activos
      ResetearTabla();
      //alta ense�amos solo en insertado
      if ( modoOpe == constantes.modoALTA ){
        DatRT dataalta = (DatRT) dlg.getComponente();
        txtAno.setText(dataalta.getCD_ARTBC());
        txtReg.setText(dataalta.getCD_NRTBC());
        txtRegFocusLost();
        dlg = null;
        btnBuscarActionPerformed(true); //mata la lista etc
      }
      else  {
        dlg = null;
        btnBuscarActionPerformed(true);
      }
    }
     } // Fin MostrarCaso()
   */

  /*
     // Pasa a la pantalla de modificacion con el click de Cierre (off)
     void btnCerrarActionPerformed() {
    // Modo espera
    int modo =  modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);
    // Creaci�n del dialogo de modificaci�n
    MostrarDialogoModificacion(modoCERRAR);
    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
     } // Fin btnCerrarActionPerformed()
     // Pasa a la pantalla de modficacion con el click de cierre (on) + resto de campos
     void btnDeshacerCierreActionPerformed() {
    // Modo espera
    int modo =  modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);
    // Creaci�n del dialogo de modificaci�n
    MostrarDialogoModificacion(modoDESHACERCIERRE);
    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
     } // Fin btnDeshacerCierreActionPerformed()
   */
  /*************** FUNCIONES AUXILIARES ***********************/

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowWarning()

  // Llama a CMessage para mostrar el mensaje de error sMessage
  public void ShowError(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgERROR, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowError()

  private boolean comprobarAno(String sAno) {
    boolean bValor = true;
    int ano = 0;
    try {
      Integer f = new Integer(sAno);
      ano = f.intValue();
      if ( (ano < 1000) || (ano > 9999)) {
        //comun.ShowWarning(this.app, "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
        //txtAno.setText(sReserva);
        //bFocus = true;
        bValor = false;
      }
    }
    catch (Exception e) {
      //comun.ShowWarning(this.app, "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
      //txtAno.setText(sReserva);
      //bFocus = true;
      bValor = false;
    }
    return bValor;
  }

} // endclass PanConTub

/* --------------- ESCUCHADORES ------------------- */

// Botones
class PanConTubActionAdapter
    implements java.awt.event.ActionListener {
  PanConTub adaptee;
  ActionEvent evt;

  PanConTubActionAdapter(PanConTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    //new Thread(this).start();
    if (evt.getActionCommand().equals("LupaEnfermo")) {
      adaptee.btnLupaEnfermoActionPerformed();
    }
    else if (evt.getActionCommand().equals("Buscar")) {
      adaptee.btnBuscarActionPerformed(true);
    }
    else if (evt.getActionCommand().equals("Primero")) {
      adaptee.btnPrimeroActionPerformed();
    }
    else if (evt.getActionCommand().equals("Anterior")) {
      adaptee.btnAnteriorActionPerformed();
    }
    else if (evt.getActionCommand().equals("Siguiente")) {
      adaptee.btnSiguienteActionPerformed();
    }
    else if (evt.getActionCommand().equals("Ultimo")) {
      adaptee.btnUltimoActionPerformed();
    }
    else if (evt.getActionCommand().equals("Cerrar")) {
      //adaptee.btnCerrarActionPerformed();
    }
    else if (evt.getActionCommand().equals("Deshacer Cierre")) {
      //adaptee.btnDeshacerCierreActionPerformed();
    }
    else if (evt.getActionCommand().equals("Modificar")) {
      adaptee.btnModificarActionPerformed();
    }
    else if (evt.getActionCommand().equals("Alta")) {
      adaptee.btnAltaActionPerformed();
    }
    else if (evt.getActionCommand().equals("Baja")) {
      adaptee.btnBajaActionPerformed();
    }

  }

  /*  public void run(){
      if (evt.getActionCommand().equals("LupaEnfermo")){
        adaptee.btnLupaEnfermoActionPerformed();
      } else if (evt.getActionCommand().equals("Buscar")){
        adaptee.btnBuscarActionPerformed(true);
      } else if(evt.getActionCommand().equals("Primero")){
        adaptee.btnPrimeroActionPerformed();
      } else if(evt.getActionCommand().equals("Anterior")){
        adaptee.btnAnteriorActionPerformed();
      } else if(evt.getActionCommand().equals("Siguiente")){
        adaptee.btnSiguienteActionPerformed();
      } else if(evt.getActionCommand().equals("Ultimo")){
        adaptee.btnUltimoActionPerformed();
      } else if(evt.getActionCommand().equals("Cerrar")){
        //adaptee.btnCerrarActionPerformed();
      } else if(evt.getActionCommand().equals("Deshacer Cierre")){
        //adaptee.btnDeshacerCierreActionPerformed();
      } else if(evt.getActionCommand().equals("Modificar")){
        adaptee.btnModificarActionPerformed();
      } else if(evt.getActionCommand().equals("Alta")){
        adaptee.btnAltaActionPerformed();
      } else if(evt.getActionCommand().equals("Baja")){
        adaptee.btnBajaActionPerformed();
      }
    }*/
} // Fin clase PanConTubActionAdapter

// Perdidas de foco
class PanConTubFocusAdapter
    implements java.awt.event.FocusListener {
  PanConTub adaptee;
  FocusEvent evt;

  PanConTubFocusAdapter(PanConTub adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    //Thread th = new Thread(this);
    //th.start();
    if ( ( (TextField) evt.getSource()).getName().equals("Registro")) {
      adaptee.txtRegFocusLost();
    }
    if ( ( (TextField) evt.getSource()).getName().equals("Ano")) {
      adaptee.txtAnoFocusLost();
    }
  } // Fin focusLost()

  public void focusGained(FocusEvent e) {

    if ( ( (TextField) e.getSource()).getName().equals("Ano")) {
      adaptee.txtAnoFocusGained();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("Registro")) {
      adaptee.txtRegFocusGained();
    }
    /*
         } else if(((TextField)e.getSource()).getName().equals("FechaDesde")){
       adaptee.txtFechaDesdeFocusGained();
         } else if(((TextField)e.getSource()).getName().equals("FechaHasta")){
        adaptee.txtFechaHastaFocusGained();
         }
     */
  }

  // Implementacion de run()
  /*  public void run() {
      if(((TextField)evt.getSource()).getName().equals("Registro")){
         adaptee.txtRegFocusLost();
      }
      if(((TextField)evt.getSource()).getName().equals("Ano")){
         adaptee.txtAnoFocusLost();
      }
    } // Fin run()*/

} // Fin clase PanMntRegTubFocusAdapter

// Doble-click en un registro de la tabla
class PanConTubTablaDobleClick
    implements jclass.bwt.JCActionListener, Runnable {
  PanConTub adaptee;

  PanConTubTablaDobleClick(PanConTub adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.TablaDobleClickActionPerformed();
  }

} // Fin clase PanConTubTablaDobleClick
/*
// Eventos (click o barra espaciadora) sobre la tabla
 class PanTablaItemListener implements jclass.bwt.JCItemListener {
  PanConTub adaptee;
  PanTablaItemListener(PanConTub adaptee) {
    this.adaptee = adaptee;
  }
  public void itemStateChanged(jclass.bwt.JCItemEvent e) {
    adaptee.clickTabla();
  }
 } // Fin clase TablaItemListener
 */
