/**
 * Descripci�n de paquete.
 */
package tuberculosis.cliente.voltratamientos;

//import sapp2.*;
import capp2.CApp;

//import tuberculosis.cliente.componentes.*; //PanAnoSemFecha
//import util_ficheros.*;//EditorFicheros

/**
 * Applet que realiza la petici�n de datos para exportaciones.
 *
 * @author JRM&AIC
 * @version  1.1 22/03/2000
 */
/*
 * Clase : tuberculosis.cliente.exportaciones.appExportaciones
 *   Autor    Fecha        Accion
 * JRM&AIC  22/03/2000   La implementa
 */

public class AppVolTratamientos
    extends CApp {
  final String Titulo = "Volcado de Evoluciones de Tuberculosis";

  //constantes para localizaciones

  boolean isStandalone = false;

  PanelVolTratamientos miPanel = null;

  /**
   * Constructor de la clase.
   *
   * @author JRM&AIC
   * @version 1.1
   */
  /*
   * M�todo : appExportaciones()
   *   Autor    Fecha        Accion
   * JRM&AIC  22/03/2000   La implementa
   */
  public AppVolTratamientos() {

  }

  /**
   * Inicializaci�n de applet
   */

  /*
   * M�todo : appExportaciones.init()
   *   Autor    Fecha        Accion
   * JRM&AIC  22/03/2000   La implementa
   */
  public void init() {
    try {
      super.init();
      miPanel = new PanelVolTratamientos(this);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Inicializaci�n de controles de pantalla
   */
  /*
   * M�todo : appExportaciones.jbInit()
   *   Autor    Fecha        Accion
   * JRM&AIC  22/03/2000   La implementa
   */
  private void jbInit() throws Exception {
    setTitulo(Titulo);

    VerPanel("", miPanel);

  }

//Get Applet information

  public String getAppletInfo() {
    return "Applet Information";
  }

//Get parameter info

  public String[][] getParameterInfo() {
    return null;
  }

}
