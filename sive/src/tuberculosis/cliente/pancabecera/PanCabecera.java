/**
 * Clase: AppPanCabecera
 * Paquete: tuberculosis.cliente.pancabecera
 * Hereda: CApp
 * Autor: MLuisa
 * Fecha Inicio: 20/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: panel PanCabecera
 */

package tuberculosis.cliente.pancabecera;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import comun.CListaNiveles1;
import comun.Common;
import comun.Comunicador;
import comun.DataEnfermo;
import comun.DataEntradaEDO;
import comun.DataGeneralCDDS;
import comun.DataMunicipioEDO;
import comun.DataRegistroEDO;
import comun.DataValoresEDO;
import comun.DialogRegistroEDO;
import comun.DialogSelEnfermo;
import comun.Fechas;
import comun.UtilEDO;
import comun.constantes;
import sapp.StubSrvBD;
import suca.panelsuca;
import suca.zonificacionSanitaria;
import tuberculosis.cliente.diaconcasotub.DiaConCasoTub;
import tuberculosis.cliente.diatuber.DialogTub;
import tuberculosis.datos.mantcontub.DatConCasTub;
import tuberculosis.datos.notiftub.DatCasosTub;
import tuberculosis.datos.notiftub.DatRT;

public class PanCabecera
    extends CPanel
    implements zonificacionSanitaria {

  // Para saber qu� se est� tratando
  private boolean bEnfermo = false;

  //listas catalogos
  public CLista listaTipoDocumento = new CLista();
  public CLista listaSexo = new CLista();

  //Almacen Zonificacion
  public String sN1Bk = "";
  public String sN2Bk = "";

  // variabla para pasar informacion sobre codigo enfermo
  public String sStringCodEnf = null;

  //Antes de espera en suca Guardar el modo
  public int iAntesDeZona = 0;

  //control de activacion automatica de boton enfermo
  protected boolean bDeboLanzarEnfermo = true;

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;

  //CA-prov
  protected boolean bCA_Cargada = false;
  protected boolean bProv_Cargada = false;

  // A�o y num-registro del caso
  protected String sAnio = "";
  protected String sNumReg = "";

  //Guardar enfermo: (desde)
  // select,tstCodEnfermo_focuslost, tstCodEnfermo_focuslost,
  // btnlupa, btnEnfermo, btnCaso
  public String sStringBk = "";
  public String sEnfermedadBk = "";

  //fechas
  public String sFechaRecepcion = "";
  public String sFechaNotificacion = "";

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/enfermos.gif",
      "images/casos.gif"};

  /* MODOS DE OPERACI�N DEL PANEL */
  final int modoESPERA = constantes.modoESPERA;
  final int modoALTA = constantes.modoALTA;
  final int modoMODIFICACION = constantes.modoMODIFICACION;
  final int modoBAJA = constantes.modoBAJA;
  final int modoCONSULTA = constantes.modoCONSULTA;
  final int modoSOLODECLARANTES = constantes.modoSOLODECLARANTES;
  // modos de operaci�n del servlet SrvEntradaEDO
  final int servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ = 19;
  final int servletOBTENER_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 20;
  final int servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ = 21;
  final int servletSELECCION_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 22;
  final int servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ = 23;
  final int servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ = 24;
  final int servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ = 25;
  final int servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ = 26;
  final int servletSELECCION_ZBS_X_CODIGO = 11;
  final int servletOBTENER_ZBS_X_CODIGO = 12;
  final int servletSELECCION_ZBS_X_DESCRIPCION = 13;
  final int servletOBTENER_ZBS_X_DESCRIPCION = 14;
  // modos de operaci�n del servlet SrvMunicipio
  final int servletSELECCION_MUNICIPIO_X_CODIGO = 9;
  final int servletOBTENER_MUNICIPIO_X_CODIGO = 10;
  final int servletSELECCION_MUNICIPIO_X_DESCRIPCION = 11;
  final int servletOBTENER_MUNICIPIO_X_DESCRIPCION = 12;
  // modos de operaci�n del servlet SrvEnfermo
  final int modoDATOSENFERMO = 10;
  final int modoDATOSENFERMOTRAMERO = 320;

  /* CONSTANTES DEL PANEL */
  // constantes del panel
//  final String strSERVLET = "servlet/SrvEntradaEDO";
  final String strSERVLET = constantes.strSERVLET_NIV;

//  final String strSERVLET_MUNICIPIO = "servlet/SrvMunicipio";
  final String strSERVLET_MUNICIPIO = constantes.strSERVLET_MUNICIPIO;

//  final String strSERVLET_MUNICIPIO_CONT = "servlet/SrvMunicipioCont";
  final String strSERVLET_MUNICIPIO_CONT = constantes.strSERVLET_MUNICIPIO_CONT;

//  final String strSERVLET_ENFERMO = "servlet/SrvEnfermo";
  final String strSERVLET_ENFERMO = constantes.strSERVLET_ENFERMO;

  /* PAR�METROS DEL PANEL */

  protected StubSrvBD stubCliente = null;
  // modo de operaci�n del panel
  public int modoOperacion = modoALTA;

  // Lista que contiene los datos del enfermo
  protected CLista listaEnfermo = null;

  // sincronizaci�n
  protected boolean sinBloquear = true;
  // objeto que sirve de sincronizaci�n
  Object sincro = new Object();

  // indica si tiene permiso para ver los nombres o las siglas
  public boolean bFlagEnfermo = true; //si false, no puede ver normal

  // dialog que contiene a este panel
  DialogTub dlgTuber;
  DiaConCasoTub dlgContacto;

  //variable control
  //PARA NO DISPARAR 2 VECES LOSTFOCUS!!!!!!!
  boolean bFocoN1 = false;
  boolean bFocoN2 = false;
  boolean bFocoZBS = false;
  //boolean bFocoMun = false;

  boolean bSalir_del_dialogo = false;

  // variables booleanas que indican si el contenido de los campos
  // respectivos es correcto
  public boolean bNivel1Valid = false;
  public boolean bNivel2Valid = false;
  public boolean bNivelZBSValid = false;
  //public boolean bMunValid = false;

  // variable booleanas que indican si el nivel elegido se ha
  // seleccionado de la lista
  private boolean bNivel1SelDePopup = false;
  private boolean bNivel2SelDePopup = false;
  private boolean bNivelZBSSelDePopup = false;
  //private boolean bMunSelDePopup = false;

  /* LISTENERS */
  PanCabeceraactionAdapter actionAdapter = new PanCabeceraactionAdapter(this);
  PanCabecerafocusAdapter focusAdapter = new PanCabecerafocusAdapter(this);
  PanCabecerachItemListener chItemListener = new PanCabecerachItemListener(this);
  PanCabeceratextAdapter KeyAdapter = new PanCabeceratextAdapter(this);

  XYLayout xYLayout = new XYLayout();
  Panel panel1 = new Panel();
  Label lblCaso = new Label();
  Label lblNCaso = new Label();
  Label lFechaNac = new Label();
  // Corregido 23-05-01 ARS, pone barras autom�ticamente.
//  CFechaSimple txtFechaNac = new CFechaSimple("N");
//  CFechaSimple CfechaNotif = new CFechaSimple("S");
  fechas.CFecha txtFechaNac = new fechas.CFecha("N");
  fechas.CFecha CfechaNotif = new fechas.CFecha("S");
  Label lEdad = new Label();
  TextField txtEdad = new TextField();
  Label label8 = new Label();
  Label lTelefono = new Label();
  TextField txtTelefono = new TextField();
  public TextField txtCodEnfermo = new TextField();
  Label lCodEnfermo = new Label();
  TextField txtDesZBS = new TextField();
  TextField txtCodZBS = new TextField();
  public TextField txtCodNivel1 = new TextField();
  public TextField txtCodNivel2 = new TextField();
  ButtonControl btnZBS = new ButtonControl();
  Label lZBS = new Label();
  ButtonControl btnNivel1 = new ButtonControl();
  ButtonControl btnNivel2 = new ButtonControl();
  TextField txtDesNivel1 = new TextField();
  TextField txtDesNivel2 = new TextField();
  Label lNivel1 = new Label();
  Label lNivel2 = new Label();
  TextField txtApe1 = new TextField();
  TextField txtApe2 = new TextField();
  TextField txtNombre = new TextField();
  Label lApeNombre = new Label();
  Label lTipoDocumento = new Label();
  Choice choTipoDocumento = new Choice();
  Label lDocumento = new Label();
  TextField txtDocumento = new TextField();
  Choice choSexo = new Choice();
  Choice choEdad = new Choice();
  ButtonControl btnEnfermo = new ButtonControl();
  Label lTipoFechaNac = new Label();
  ButtonControl btnCaso = new ButtonControl();
  Label lFechaNotif = new Label();
  ButtonControl btnLupaEnfermo = new ButtonControl();

  // suca
  panelsuca panSuca = null;
  boolean bTramero = false;

  Hashtable hs = null;
  CApp capp = null;

  XYLayout xYLayout2 = new XYLayout();
  ScrollPane panelScroll = new ScrollPane();
  Panel panel = new Panel();

  // E 03/02/2000 --> Estudiar el uso �nico de DialogoGeneral
  public PanCabecera(DiaConCasoTub dlg, int modo, Hashtable hash,
                     boolean enfermo) {
    try {

      bEnfermo = enfermo;
      dlgContacto = dlg;

      setApp(dlgContacto.getCApp());
      capp = dlgContacto.getCApp();
      hs = (Hashtable) hash.clone();
      modoOperacion = modo;

      stubCliente = dlgContacto.stubCliente;

      boolean tramero = true;

      if (getApp().getIT_TRAMERO().equals("N")) {
        tramero = false;

        // visibilidad de datos sensibles
      }
      int modoSuca = panelsuca.modoINICIO;
      if (getApp().getIT_FG_ENFERMO().equals("N")) {
        modoSuca = panelsuca.modoCONFIDENCIAL;

        //------------ panel suca --------
      }
      CLista listaEnviarSuca = new CLista();
      if (tramero) {
        listaEnviarSuca.addElement(hs.get(constantes.PAISES)); //0
        listaEnviarSuca.addElement(hs.get(constantes.CA)); //1
        listaEnviarSuca.addElement(hs.get(constantes.TRAMEROS)); //2
        listaEnviarSuca.addElement(hs.get(constantes.NUMEROS)); //3
        listaEnviarSuca.addElement(hs.get(constantes.CALIFICADORES)); //4
      }
      else {
        listaEnviarSuca.addElement(hs.get(constantes.PAISES)); //0
        listaEnviarSuca.addElement(hs.get(constantes.CA)); //1
        listaEnviarSuca.addElement(null); //2
        listaEnviarSuca.addElement(null); //3
        listaEnviarSuca.addElement(null); //4
      }
      panSuca = new panelsuca(getApp(), tramero, modoSuca, false,
                              listaEnviarSuca, this);
      //panSuca = new panelsuca(getApp(), tramero, modoSuca, false, this);
      bTramero = panSuca.getModoTramero();
      //----------------------------------

      jbInit();
      // Ponemos obligatorios campos de suca:
      panSuca.setObligatorios();
      RellenaCombos();

      if (modo == modoALTA) {
        txtDocumento.setVisible(false); //cris
        lDocumento.setVisible(false); //cris
      }

      modoOperacion = modo;
      Inicializar(modoOperacion);
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    catch (Exception ex) {
      ex.printStackTrace();
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
  }

  public PanCabecera(DialogTub dlg, int modo, Hashtable hash, boolean enfermo) {
    try {

      bEnfermo = enfermo;
      dlgTuber = dlg;

      setApp(dlgTuber.getCApp());
      capp = dlgTuber.getCApp();
      hs = (Hashtable) hash.clone();
      modoOperacion = modo;

      stubCliente = dlgTuber.stubCliente;

      boolean tramero = true;

      if (getApp().getIT_TRAMERO().equals("N")) {
        tramero = false;

        // visibilidad de datos sensibles
      }
      int modoSuca = panelsuca.modoINICIO;
      if (getApp().getIT_FG_ENFERMO().equals("N")) {
        modoSuca = panelsuca.modoCONFIDENCIAL;

        //------------ panel suca --------
      }
      CLista listaEnviarSuca = new CLista();
      if (tramero) {
        listaEnviarSuca.addElement(hs.get(constantes.PAISES)); //0
        listaEnviarSuca.addElement(hs.get(constantes.CA)); //1
        listaEnviarSuca.addElement(hs.get(constantes.TRAMEROS)); //2
        listaEnviarSuca.addElement(hs.get(constantes.NUMEROS)); //3
        listaEnviarSuca.addElement(hs.get(constantes.CALIFICADORES)); //4
      }
      else {
        listaEnviarSuca.addElement(hs.get(constantes.PAISES)); //0
        listaEnviarSuca.addElement(hs.get(constantes.CA)); //1
        listaEnviarSuca.addElement(null); //2
        listaEnviarSuca.addElement(null); //3
        listaEnviarSuca.addElement(null); //4
      }
      panSuca = new panelsuca(getApp(), tramero, modoSuca, false,
                              listaEnviarSuca, this);
      //panSuca = new panelsuca(getApp(), tramero, modoSuca, false, this);
      bTramero = panSuca.getModoTramero();
      //----------------------------------

      jbInit();
      // Ponemos obligatorios campos de suca:
      panSuca.setObligatorios();
      RellenaCombos();

      if (modo == modoALTA) {
        txtDocumento.setVisible(false); //cris
        lDocumento.setVisible(false); //cris
      }

      modoOperacion = modo;
      Inicializar(modoOperacion);
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    catch (Exception ex) {
      ex.printStackTrace();
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
  }

  void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnLupaEnfermo.setImage(imgs.getImage(0));
    lblCaso.setText("");
    lblNCaso.setText("");
    btnNivel1.setImage(imgs.getImage(0));
    btnNivel2.setImage(imgs.getImage(0));
    btnZBS.setImage(imgs.getImage(0));
    btnEnfermo.setImage(imgs.getImage(1));
    btnCaso.setImage(imgs.getImage(2));

    //mlm
    this.setSize(new Dimension(745, 230 + 80));
    xYLayout.setHeight(365 + 80);
    xYLayout.setWidth(755);
    this.setLayout(xYLayout);

    xYLayout2.setHeight(400 + 80); //   320
    xYLayout2.setWidth(600);
    panelScroll.setSize(745, 20); //(735, 270);
    //panel.setBounds(new Rectangle(10, 10, 715, 315));
    panel.setSize(new Dimension(600, 400 + 80));
    panel.setLayout(xYLayout2);

    lTelefono.setText("Telefono:");
    txtCodEnfermo.setBackground(new Color(255, 255, 150));
    if (bEnfermo) {
      lCodEnfermo.setText("C�digo enfermo:");
    }
    else {
      lCodEnfermo.setText("C�digo contacto:");
    }
    txtDesZBS.setEditable(false);
    txtApe1.setBackground(new Color(255, 255, 150));
    lApeNombre.setText("Apellidos  y nombre:");
    lTipoDocumento.setText("Tipo Documento:");
    lDocumento.setText("Documento:");
    lZBS.setText(this.app.getNivel3() + ":");
    // Cambio de color de las cajas de �rea, distrito y ZBS. (29-03-01)
    txtCodNivel1.setBackground(new Color(255, 255, 150));
    txtCodNivel2.setBackground(new Color(255, 255, 150));
    txtCodZBS.setBackground(new Color(255, 255, 150));
    // -------
    txtDesNivel1.setEditable(false);
    txtDesNivel2.setEditable(false);

    lNivel1.setText(this.app.getNivel1() + ":");
    lNivel2.setText(this.app.getNivel2() + ":");
    lFechaNac.setText("Fecha Nacimiento:");
    lEdad.setText("Edad:");
    btnEnfermo.setLabel("Fichas");
    btnCaso.setLabel("Registros");
    lFechaNotif.setText("Fecha Notificaci�n:");
    label8.setText("Sexo:");

    this.add(panelScroll, new XYConstraints(0, 0, 745, 230 + 80));
    panelScroll.add(panel, new XYConstraints(0, 0, 600, 400 + 80));
    panel.add(txtCodEnfermo, new XYConstraints(135, 26, 86, -1));
    panel.add(btnLupaEnfermo, new XYConstraints(227, 24, -1, -1));
    panel.add(btnEnfermo, new XYConstraints(257, 24, 115, -1));
    panel.add(lCodEnfermo, new XYConstraints(15, 26, 98, -1));
    panel.add(btnCaso, new XYConstraints(402, 25, 115, -1));

    panel.add(lFechaNotif, new XYConstraints(15, 75, 0, 0));
    panel.add(CfechaNotif, new XYConstraints(135, 75, 0, 0));
    lFechaNotif.setVisible(false);
    CfechaNotif.setVisible(false);
    panel.add(lTipoDocumento, new XYConstraints(15, 75, 100, -1));
    panel.add(lDocumento, new XYConstraints(305 + 15, 75, 73, -1));
    panel.add(choTipoDocumento, new XYConstraints(120 + 15, 75, 162, -1));
    panel.add(txtDocumento, new XYConstraints(370 + 25, 75, 116, -1));
    panel.add(txtApe1, new XYConstraints(135, 104, 145, -1));
    panel.add(txtApe2, new XYConstraints(287, 104, 225, -1));
    panel.add(txtNombre, new XYConstraints(517, 104, 224, -1));
    panel.add(lFechaNac, new XYConstraints(15, 132, 113, -1));
    panel.add(txtFechaNac, new XYConstraints(135, 132, 85, -1));
    panel.add(lEdad, new XYConstraints(275, 132, 37, -1));
    panel.add(txtEdad, new XYConstraints(313, 132, 35, -1));
    panel.add(choEdad, new XYConstraints(353, 132, 81, -1));
    panel.add(lTipoFechaNac, new XYConstraints(225, 132, 44, -1));
    panel.add(choSexo, new XYConstraints(490, 132, 79, -1));
    panel.add(txtTelefono, new XYConstraints(645, 132, 96, -1));
    panel.add(label8, new XYConstraints(445, 132, 38, -1));
    panel.add(lTelefono, new XYConstraints(580, 132, 59, -1));
    panel.add(panSuca, new XYConstraints(13, 156, 596, 116));

    // Ahora la informaci�n sobre �rea, distrito y zona
    // va sobre una �nica l�nea
    panel.add(lNivel1, new XYConstraints(15, 275, 80, 19));
    panel.add(txtCodNivel1, new XYConstraints(100, 273, 66, -1));
    panel.add(btnNivel1, new XYConstraints(169, 273, -1, -1));
    panel.add(txtDesNivel1, new XYConstraints(170, 273, 152, -1));

    panel.add(lNivel2, new XYConstraints(205, 275, 42, 21)); // (15, 301, 92, 21)
    panel.add(txtCodNivel2, new XYConstraints(250, 273, 66, -1)); // (135, 301, 66, -1)
    panel.add(btnNivel2, new XYConstraints(319, 273, -1, -1)); // (215, 301, -1, -1)
    panel.add(txtDesNivel2, new XYConstraints(320, 273, 152, -1)); // (257, 301, 152, -1)

    panel.add(lZBS, new XYConstraints(355, 275, 75, -1)); // (15, 329, 92, -1)
    panel.add(txtCodZBS, new XYConstraints(430, 273, 64, -1)); // (135, 329, 66, -1)
    panel.add(btnZBS, new XYConstraints(499, 273, -1, -1)); // (215, 329, -1, -1)

    panel.add(txtDesZBS, new XYConstraints(530, 273, 152, -1)); // (257, 329, 152, -1)

    // Las descripciones de �rea y distrito ser�n invisibles
    txtDesNivel1.setVisible(false);
    txtDesNivel2.setVisible(false);

    //

    panel.add(lApeNombre, new XYConstraints(15, 104, 114, -1));
    panel.add(lblCaso, new XYConstraints(584, 45, 70, 21));
    panel.add(lblNCaso, new XYConstraints(671, 45, 70, 21));

    setBorde(false);

    btnLupaEnfermo.setActionCommand("enfermo");
    btnNivel1.setActionCommand("nivel1");
    btnNivel2.setActionCommand("nivel2");
    btnZBS.setActionCommand("zbs");
    btnEnfermo.setActionCommand("selenfermo");
    btnCaso.setActionCommand("datosenfermo");
    btnLupaEnfermo.addActionListener(actionAdapter);
    btnNivel1.addActionListener(actionAdapter);
    btnNivel2.addActionListener(actionAdapter);
    btnZBS.addActionListener(actionAdapter);
    btnEnfermo.addActionListener(actionAdapter);
    btnCaso.addActionListener(actionAdapter);

    txtApe1.setName("ape1");
    txtFechaNac.setName("fechanac");
    txtEdad.setName("edad");
    txtCodEnfermo.setName("enfermo");
    txtFechaNac.addFocusListener(focusAdapter);
    txtEdad.addFocusListener(focusAdapter);
    txtCodEnfermo.addFocusListener(focusAdapter);

    txtCodNivel1.setName("nivel1");
    txtCodNivel2.setName("nivel2");
    txtCodZBS.setName("zbs");
    txtDocumento.setName("documento");

    txtDocumento.addFocusListener(focusAdapter);

    txtCodNivel1.addKeyListener(KeyAdapter);
    txtCodNivel2.addKeyListener(KeyAdapter);
    txtCodZBS.addKeyListener(KeyAdapter);

    txtCodNivel1.addFocusListener(focusAdapter);
    txtCodNivel2.addFocusListener(focusAdapter);
    txtCodZBS.addFocusListener(focusAdapter);

    choTipoDocumento.setName("tipodoc");
    choSexo.setName("sexo");
    choEdad.setName("edad");

    choEdad.addItemListener(chItemListener);
    choTipoDocumento.addItemListener(chItemListener);
    choSexo.addItemListener(chItemListener);

    lDocumento.setVisible(false);
    txtDocumento.setVisible(false);
    CfechaNotif.setEnabled(false);
    CfechaNotif.setBackground(new Color(255, 255, 150));
  }

  // modificacion 10/07/2000
  public void ChangedProtocolo() {

    // variable para controlar que cuando se grabe un protocolo y
    // se pulse grabar, si estamos en el panel del protocolo, se
    // grabe dicho protocolo

    //dlgTuber.bGrabarProt = false;

    if (dlgTuber.bChanging) {
      if (Common.ShowPregunta(this.app,
                              "El protocolo ha cambiado. �Desea grabarlo?")) {

        //dlgTuber.grabarProtocolo();
        dlgTuber.tabPanel.VerPanel("Protocolo Notificador");

        dlgTuber.bChanging = false;
      }
      else {
        dlgTuber.bChanging = false;
      }
    }
  }

  public void RellenaCombos() {

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion);
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }
    listaTipoDocumento = (CLista) hs.get(constantes.TDOC);
    listaSexo = (CLista) hs.get(constantes.SEXO);

    // Tipo de Documento con blanco
    Common.writeChoiceDataGeneralCDDS(choTipoDocumento, listaTipoDocumento, true, false, true);
    // choEdad
    choEdad.addItem("A�os");
    choEdad.addItem("Meses");
    // Sexo con blanco
    Common.writeChoiceDataGeneralCDDS(choSexo, listaSexo, true, false, true);

    modoOperacion = modo;

    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion);
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }

  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();

    //confidencialidad -----------
    if (getApp().getIT_FG_ENFERMO().equals("N")) {
      setFlagEnfermo(false); //no LO VE
    }
    else if (getApp().getIT_FG_ENFERMO().equals("S")) {
      setFlagEnfermo(true); //lo ve
      //-----------------------------

      //perfil 5: no ve botones, ni cod enfermo
    }
    if (getApp().getPerfil() == 5 &&
        modoOperacion == modoALTA) {
      btnCaso.setVisible(false);
      btnEnfermo.setVisible(false);
      btnLupaEnfermo.setVisible(false);
      lCodEnfermo.setVisible(false);
      txtCodEnfermo.setVisible(false);
      bDeboLanzarEnfermo = false;
    }
    else if (getApp().getPerfil() == 5 &&
             modoOperacion != modoALTA) {
      btnCaso.setVisible(true);
      btnEnfermo.setVisible(true);
      btnLupaEnfermo.setVisible(true);
      lCodEnfermo.setVisible(true);
      txtCodEnfermo.setVisible(true);
    }
    //-------------------------------------

  } //fin Inicializar

  // incializa el panel en funci�n del valor de modoOperacion
  public void Inicializar() {

    btnEnfermo.setVisible(bEnfermo);
    btnCaso.setVisible(bEnfermo);
    lblCaso.setVisible(bEnfermo);

    switch (modoOperacion) {

      case modoESPERA:
        panSuca.setModoEspera();

        choEdad.setEnabled(false);
        choTipoDocumento.setEnabled(false);
        choSexo.setEnabled(false);
        btnLupaEnfermo.setEnabled(false);
        txtApe2.setEnabled(false);
        txtNombre.setEnabled(false);
        txtFechaNac.setEnabled(false);
        txtEdad.setEnabled(false);
        txtTelefono.setEnabled(false);

        if (txtDocumento.isVisible()) {
          txtDocumento.setEnabled(false);

        }
        txtApe1.setEnabled(false);
        txtCodZBS.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);
        btnZBS.setEnabled(false);
        btnNivel1.setEnabled(false);
        btnNivel2.setEnabled(false);
        txtCodEnfermo.setEnabled(false);
        btnEnfermo.setEnabled(false);
        btnCaso.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.doLayout();
        break;

      case modoALTA:

        panSuca.setModoNormal();

        txtCodEnfermo.setEnabled(true);
        //txtCodEnfermo.setBackground(Color.white);
        btnLupaEnfermo.setEnabled(true);
        choTipoDocumento.setEnabled(true);
        // cris
        if (txtDocumento.isVisible()) {
          txtDocumento.setEnabled(true);

        }
        txtApe1.setEnabled(true);
        txtApe2.setEnabled(true);
        txtNombre.setEnabled(true);
        choSexo.setEnabled(true);
        txtFechaNac.setEnabled(true);
        txtEdad.setEnabled(true);
        choEdad.setEnabled(true);
        txtTelefono.setEnabled(true);
        CfechaNotif.setEnabled(false);
        btnEnfermo.setEnabled(true);
        btnCaso.setEnabled(true);
        txtCodZBS.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        txtCodNivel2.setEnabled(true);
        btnZBS.setEnabled(true);
        btnNivel1.setEnabled(true);
        btnNivel2.setEnabled(true);
        txtCodZBS.setEnabled(true);
        btnZBS.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        btnNivel1.setEnabled(true);
        txtCodNivel2.setEnabled(true);
        btnNivel2.setEnabled(true);

        //CARGAR ENFERMO
        sStringBk = txtCodEnfermo.getText();
        sN1Bk = txtCodNivel1.getText().trim();
        sN2Bk = txtCodNivel2.getText().trim();

        InicializaNivel2();
        InicializaNivelZBS();

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case modoMODIFICACION:

        panSuca.setModoNormal();

        //DESACTIVO AUTOMATISMO
        bDeboLanzarEnfermo = false;

        txtCodEnfermo.setEnabled(false);
        txtCodEnfermo.setBackground(new Color(255, 255, 150));
        choTipoDocumento.setEnabled(true);
        if (txtDocumento.isVisible()) {
          txtDocumento.setEnabled(true);

        }
        txtApe1.setEnabled(true);
        txtApe2.setEnabled(true);
        txtNombre.setEnabled(true);
        choSexo.setEnabled(true);
        txtFechaNac.setEnabled(true);
        txtEdad.setEnabled(true);
        choEdad.setEnabled(true);
        txtTelefono.setEnabled(true);
        CfechaNotif.setEnabled(false);
        btnLupaEnfermo.setEnabled(false);
        btnEnfermo.setEnabled(false);
        btnCaso.setEnabled(false);
        txtCodZBS.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        txtCodNivel2.setEnabled(true);
        btnZBS.setEnabled(true);
        btnNivel1.setEnabled(true);
        btnNivel2.setEnabled(true);
        txtCodZBS.setEnabled(true);
        btnZBS.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        btnNivel1.setEnabled(true);
        txtCodNivel2.setEnabled(true);
        btnNivel2.setEnabled(true);
        //Inicializa niveles
        InicializaNivel2();
        InicializaNivelZBS();

        //Etiqueta con el Caso
        lblCaso.setText("N� Exped.:");
        if (bEnfermo) {
          lblNCaso.setText(dlgTuber.sNM_EDO);
        }
        else {
          //lblNCaso.setText(dlgContacto.sNM_EDO);
        }
        lblNCaso.setFont(new Font("Dialog", 1, 12));

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case modoBAJA:
      case modoSOLODECLARANTES:
      case modoCONSULTA:

        panSuca.setModoDeshabilitado();
        panSuca.doLayout();

        txtCodEnfermo.setEnabled(false);
        txtCodEnfermo.setBackground(new Color(255, 255, 150));
        btnLupaEnfermo.setEnabled(false);
        choTipoDocumento.setEnabled(false);
        if (txtDocumento.isVisible()) {
          txtDocumento.setEnabled(false);
        }
        txtApe1.setEnabled(false);
        txtApe2.setEnabled(false);
        txtNombre.setEnabled(false);
        choSexo.setEnabled(false);
        txtFechaNac.setEnabled(false);
        txtEdad.setEnabled(false);
        choEdad.setEnabled(false);
        txtTelefono.setEnabled(false);
        CfechaNotif.setEnabled(false);
        btnEnfermo.setEnabled(false);
        btnCaso.setEnabled(false);
        txtCodZBS.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);
        btnZBS.setEnabled(false);
        btnNivel1.setEnabled(false);
        btnNivel2.setEnabled(false);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }

    //aparte
    switch (modoOperacion) {
      case modoBAJA:

        //Etiqueta con el Caso
        if (bEnfermo) {
          lblCaso.setText("N� Exped.:");
          lblNCaso.setText(dlgTuber.sNM_EDO);
          lblNCaso.setFont(new Font("Dialog", 1, 12));
        }
        break;
    } //fin segundo switch

    switch (modoOperacion) {
      case modoMODIFICACION:
      case modoBAJA:
      case modoCONSULTA:
      case modoSOLODECLARANTES:
      case modoESPERA:

        if (!getFlagEnfermo()) { //conf
          txtApe1.setEnabled(false);
        }
        break;
      case modoALTA:

        //conf y ya esta informado el enfermo(modif)
        if (!getFlagEnfermo()
            && !txtCodEnfermo.getText().trim().equals("")) {
          txtApe1.setEnabled(false);
        }
        else {
          txtApe1.setEnabled(true);
        }
        break;
    } //----tercer switch

  } //fin Inicializar

  // actualiza el estado del nivel2, condicionado al nivel1
  protected void InicializaNivel2() {
    // E 21/01/2000
    if (txtDesNivel1.getText().trim().length() == 0) {
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      btnNivel2.setEnabled(false);
    }
    else {
      btnNivel2.setEnabled(true);
    }

  }

  // actualiza el estado de la ZBS, condicionado al nivel2
  protected void InicializaNivelZBS() {
    if (txtDesNivel2.getText().trim().length() == 0) {

      txtCodZBS.setText("");
      txtDesZBS.setText("");
      btnZBS.setEnabled(false);
    }
    else {
      btnZBS.setEnabled(true);
    }

  }

  // mantiene una sincronizaci�n en los eventos
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  // desbloquea el sistema
  public synchronized void desbloquea() {
    sinBloquear = true;
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // actualiza el flag de enfermo
  public void setFlagEnfermo(boolean bool) {
    bFlagEnfermo = bool;
  }

// recupera el flag de enfermo
//    (true si SIVE_USUARIO.IT_FG_ENFERMO = 'S')
//    (false en cualquier otro caso)
  public boolean getFlagEnfermo() {
    return bFlagEnfermo;
  }

// Restricci�n de confidencialidad
// si el usuario accede al modo modificaci�n y bFlagEnfermo = false:
// no puede ver el nombre / apellidos, se ver�n las iniciales
  public void Confidencialidad() {

    txtApe2.setVisible(false);
    txtNombre.setVisible(false);
    lApeNombre.setText("Iniciales:");
    if (!txtCodEnfermo.getText().equals("")) {
      if (bEnfermo) {
        txtApe1.setText(dlgTuber.datcasostub.getSIGLAS());
      }
      else {
        txtApe1.setText(dlgContacto.datconcastub.getSIGLAS());
      }
    }

    txtApe1.setVisible(true);
    txtApe1.setEnabled(false); //en Inicializar hay un refuerzo

    txtDocumento.setVisible(false);
    choTipoDocumento.setVisible(false);
    txtTelefono.setVisible(false);

    choSexo.setVisible(true);
    lDocumento.setVisible(false);
    lTipoDocumento.setVisible(false);
    lTelefono.setVisible(false);

    label8.setVisible(true);
    choSexo.setEnabled(false); //en Inicializar hay un refuerzo

  } //CONFIDENCIALIDAD********************************

  public void Visible(boolean bSi_No) {
    if (bSi_No) { //si (visible)
      txtApe2.setVisible(true);
      txtNombre.setVisible(true);
      txtApe1.setVisible(true);

      if (choTipoDocumento.getSelectedIndex() != 0
          && choTipoDocumento.getSelectedIndex() != -1) {
        txtDocumento.setVisible(true);
        lDocumento.setVisible(true);
      }
      //txtDocumento.setVisible (true);
      choTipoDocumento.setVisible(true);
      txtTelefono.setVisible(true);
      choSexo.setVisible(true);

    }
    else { //no (invisible)
      txtApe2.setVisible(false);
      txtNombre.setVisible(false);
      txtApe1.setVisible(false);

      txtDocumento.setVisible(false);
      lDocumento.setVisible(true);

      choTipoDocumento.setVisible(false);
      txtTelefono.setVisible(false);
      choSexo.setVisible(false);
    }
  } //fin de visible

  public void setCA(String sCD_CA) {
    panSuca.setCD_CA(sCD_CA);
  }

  public void setProvincia(String sCD_PROV) {
    panSuca.setCD_PROV(sCD_PROV); // escribe la provincia (c�digo + descripci�n)en choProvincia
  }

//MLMORA  (al menos debe  estar informado el ape1 para llamar a btnEn)
  void txApe1_focusLost() {
    if (txtApe1.getText().trim().equals("")) {
      btnEnfermo.setEnabled(false);
    }
    else {
      btnEnfermo.setEnabled(true);
    }
  }

  public void choEdad_itemStateChanged() {
    txtEdad_focusLost();
  }

  public void choTipoDocumento_itemStateChanged() {
    txtDocumento.setText("");
    if (choTipoDocumento.getSelectedIndex() > 0) {
      txtDocumento.setVisible(true);
      lDocumento.setVisible(true);
      validate();
    }
    else {
      txtDocumento.setVisible(false);
      lDocumento.setVisible(false);
    }
  }

// se ejecuta en respuesta al actionPerformed en btnNivel1
  void btnNivel1_actionPerformed() {

    DataEntradaEDO data;

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }

    if (bEnfermo) { //PDP 09/02/2000
      CListaNiveles1 lista = new CListaNiveles1(dlgTuber.getCApp(),
          "Selecci�n de Niveles Sanitarios",
          stubCliente,
          strSERVLET,
          servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ,
          servletOBTENER_NIV1_X_DESCRIPCION_TODAS_AUTORIZ,
          servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ,
          servletSELECCION_NIV1_X_DESCRIPCION_TODAS_AUTORIZ);

      lista.show();
      data = (DataEntradaEDO) lista.getComponente();

    }
    else {
      CListaNiveles1 lista = new CListaNiveles1(dlgContacto.getCApp(),
          "Selecci�n de Niveles Sanitarios",
          stubCliente,
          strSERVLET,
          servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ,
          servletOBTENER_NIV1_X_DESCRIPCION_TODAS_AUTORIZ,
          servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ,
          servletSELECCION_NIV1_X_DESCRIPCION_TODAS_AUTORIZ);

      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
    }

    if (data != null) {
      txtCodNivel1.setText(data.getCod());
      txtDesNivel1.setText(data.getDes());
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      txtCodZBS.setText("");
      txtDesZBS.setText("");
      bNivel1SelDePopup = true;
    }

    modoOperacion = modo;
    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }

    if (bNivel1SelDePopup) {
      bNivel1Valid = true;
      if (bEnfermo) {
        Parche_Protocolo();
      }
    }
  }

  // se ejecuta en respuesta al actionPerformed en btnNivel2
  void btnNivel2_actionPerformed() {
    DataEntradaEDO data;

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }
    // Se obliga a que antes se haya seleccionado un nivel1
    //if (bNivel1SelDePopup) {
    if (bEnfermo) { //PDP 09/02/2000
      CListaNiveles2Indiv lista = new CListaNiveles2Indiv(this,
          "Selecci�n de Niveles Sanitarios",
          dlgTuber.stubCliente,
          strSERVLET,
          servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ,
          servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ,
          servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ,
          servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
    }
    else {
      CListaNiveles2Indiv lista = new CListaNiveles2Indiv(this,
          "Selecci�n de Niveles Sanitarios",
          dlgContacto.stubCliente,
          strSERVLET,
          servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ,
          servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ,
          servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ,
          servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
    }
    if (data != null) {
      txtCodNivel2.setText(data.getCod());
      txtDesNivel2.setText(data.getDes());
      txtCodZBS.setText("");
      txtDesZBS.setText("");

      bNivel2SelDePopup = true;
    }

    modoOperacion = modo;
    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }
    if (bNivel2SelDePopup) {
      bNivel2Valid = true;
      if (bEnfermo) {
        Parche_Protocolo();
      }
    }
  }

  // se ejecuta en respuesta al actionPerformed en btnZBS
  void btnZBS_actionPerformed() {
    DataEntradaEDO data;

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }
    // Se obliga a que antes se haya seleccionado un nivel1
    //if (bNivel1SelDePopup) {
    if (bEnfermo) { //PDP 09/02/2000
      CListaNiveles2Indiv lista = new CListaNiveles2Indiv(this,
          "Selecci�n de Niveles Sanitarios",
          dlgTuber.stubCliente,
          strSERVLET,
          servletOBTENER_ZBS_X_CODIGO,
          servletOBTENER_ZBS_X_DESCRIPCION,
          servletSELECCION_ZBS_X_CODIGO,
          servletSELECCION_ZBS_X_DESCRIPCION);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
    }
    else {
      CListaNiveles2Indiv lista = new CListaNiveles2Indiv(this,
          "Selecci�n de Niveles Sanitarios",
          dlgContacto.stubCliente,
          strSERVLET,
          servletOBTENER_ZBS_X_CODIGO,
          servletOBTENER_ZBS_X_DESCRIPCION,
          servletSELECCION_ZBS_X_CODIGO,
          servletSELECCION_ZBS_X_DESCRIPCION);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
    }
    if (data != null) {
      txtCodZBS.setText(data.getCod());
      txtDesZBS.setText(data.getDes());

      bNivelZBSSelDePopup = true;
    }

    modoOperacion = modo;
    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }
    if (bNivelZBSSelDePopup) {
      bNivelZBSValid = true;
    }
  }

  // se ejecuta en respuesta al keyPressed en txtCodNivel1
  void txtCodNivel1_keyPressed() {

    txtDesNivel1.setText("");
    txtCodNivel2.setText("");
    txtDesNivel2.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");

    bNivel1SelDePopup = false;
    bNivel2SelDePopup = false;
    bNivelZBSSelDePopup = false;
    bNivel1Valid = false;
    bNivel2Valid = false;
    bNivelZBSValid = false;

    btnNivel2.setEnabled(false);
    btnZBS.setEnabled(false);

  }

  // se ejecuta en respuesta al keyPressed en txtCodNivel2
  void txtCodNivel2_keyPressed() {
    txtDesNivel2.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");

    bNivel2SelDePopup = false;
    bNivelZBSSelDePopup = false;
    bNivel2Valid = false;
    bNivelZBSValid = false;

    btnZBS.setEnabled(false);

  }

  // se ejecuta en respuesta al keyPressed en txtCodZBS
  void txtCodZBS_keyPressed() {
    txtDesZBS.setText("");
  }

  // se ejecuta en respuesta al focuslost en txtCodNivel1
  void txtCodNivel1_focusLost() {

    if (bSalir_del_dialogo) {
      return;
    }
    if (bFocoN1) {
      return;
    }

    if (bNivel1Valid) {
      return;
    }

    if (bNivel1SelDePopup) {
      return;
    }

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    if (bEnfermo) { //PDP 09/02/2000
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }

    if (txtCodNivel1.getText().trim().equals("")) {
      bFocoN1 = true;
      bFocoN1 = false;
      bNivel1Valid = false;
      bNivel2Valid = false;
      bNivelZBSValid = false;

      modoOperacion = modo;
      if (bEnfermo) { //PDP 09/02/2000
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }

      if (bEnfermo) {
        Parche_Protocolo();
      }
      return;
    }

    // Comprueba que datos introducidos son v�lidos si no se han
    // seleccionado de los popup
    CLista data = null;
    Object componente;

    if (!bNivel1SelDePopup) {

      try {
        data = CompruebaNivel(
            servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ,
            txtCodNivel1.getText().trim(), "", "");

        if (data.size() == 0) {

          bFocoN1 = true;
          Common.ShowWarning(capp, "Valor incorrecto");
          bFocoN1 = false;
          txtCodNivel1.setText("");
          bNivel1Valid = false;
          bNivel2Valid = false;
          bNivelZBSValid = false;

          modoOperacion = modo;
          if (bEnfermo) {
            dlgTuber.Inicializar(modoOperacion); //mlm nuevo
            Parche_Protocolo();
          }
          else {
            dlgContacto.Inicializar(modoOperacion);
          }
          return;
        }

        else {
          DataEntradaEDO dataEDO = new DataEntradaEDO();
          dataEDO = (DataEntradaEDO) data.elementAt(0);
          txtDesNivel1.setText(dataEDO.getDes());
        }

      }
      catch (Exception e) { //acceso a base de datos mal

        bFocoN1 = true;
        Common.ShowWarning(capp, e.getMessage());
        bFocoN1 = false;

        bNivel1Valid = false;
        bNivel2Valid = false;
        bNivelZBSValid = false;

        //reseteo
        txtCodNivel1.setText("");
        txtCodNivel1.select(txtCodNivel1.getText().length(),
                            txtCodNivel1.getText().length());
        txtCodNivel1.requestFocus();

        modoOperacion = modo;

        if (bEnfermo) {
          dlgTuber.Inicializar(modoOperacion); //mlm nuevo
          Parche_Protocolo();
        }
        else {
          dlgContacto.Inicializar(modoOperacion);
        }
        return;
      } //fin del try

      //si todo fue bien
      componente = data.elementAt(0);
      txtDesNivel1.setText( ( (DataEntradaEDO) componente).getDes());
      bNivel1SelDePopup = true;
    }

    modoOperacion = modo;
    if (bEnfermo) { //PDP 09/02/2000
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion); //mlm nuevo
    }

    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel1 el v�lido, y se pone su boolean a true
    bNivel1Valid = true;
    if (bEnfermo) {
      Parche_Protocolo();

    }
  } //fin focuslostNivel1

// se ejecuta en respuesta al focuslost en txtCodNivel2
  void txtCodNivel2_focusLost() {
    if (bSalir_del_dialogo) {
      return;
    }
    if (bFocoN2) {
      return;
    }

    if (bNivel2Valid) {
      return;
    }

    if (bNivel2SelDePopup) {
      return;
    }

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    if (bEnfermo) { //PDP 09/02/2000
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }

    CLista data = null;
    Object componente = null;

    //si lleno - y no se pulso popup
    if (txtCodNivel2.getText().trim().length() != 0
        && !bNivel2SelDePopup) {

      try {
        data = CompruebaNivel(
            servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ,
            txtCodNivel2.getText().trim(),
            txtCodNivel1.getText().trim(), "");

        if (data.size() == 0) {

          bFocoN2 = true;
          Common.ShowWarning(capp, "Valor incorrecto");
          bFocoN2 = false;
          txtCodNivel2.setText("");
          txtDesNivel2.setText("");
          bNivel2Valid = false;
          bNivelZBSValid = false;

          modoOperacion = modo;
          if (bEnfermo) { //PDP 09/02/2000
            dlgTuber.Inicializar(modoOperacion); //mlm nuevo
            Parche_Protocolo();
          }
          else {
            dlgContacto.Inicializar(modoOperacion);
          }
          return;
        }

        else {
          DataEntradaEDO dataEDO = new DataEntradaEDO();
          dataEDO = (DataEntradaEDO) data.elementAt(0);
          txtDesNivel2.setText(dataEDO.getDes());

          bNivel2SelDePopup = true;
        }

      }
      catch (Exception e) { //acceso a base de datos mal

        bFocoN2 = true;
        Common.ShowWarning(capp, e.getMessage());
        bFocoN2 = false;

        bNivel2Valid = false;
        bNivelZBSValid = false;

        //resetea
        txtCodNivel2.setText("");
        txtCodNivel2.select(txtCodNivel2.getText().length(),
                            txtCodNivel2.getText().length());
        txtCodNivel2.requestFocus();

        modoOperacion = modo;
        if (bEnfermo) { //PDP 09/02/2000
          dlgTuber.Inicializar(modoOperacion); //mlm nuevo
          Parche_Protocolo();
        }
        else {
          dlgContacto.Inicializar(modoOperacion); //mlm nuevo
        }
        return;
      } //fin del try

      modoOperacion = modo;
      if (bEnfermo) { //PDP 09/02/2000
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }
    } //fin del if

    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel2 es correcto, y se pone su boolean a true
    if (modoOperacion == modoESPERA) {
      //modoOperacion = modoNORMAL;
      modoOperacion = modo;
      if (bEnfermo) {
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }
    }

    bNivel2Valid = true;
    if (bEnfermo) {
      Parche_Protocolo();
    }
  } //fin de n2

  // se ejecuta en respuesta al focuslost en txtCodZBS
  void txtCodZBS_focusLost() {
    if (bSalir_del_dialogo) {
      return;
    }
    if (bFocoZBS) {
      return;
    }

    if (bNivelZBSValid) {
      return;
    }

    if (bNivelZBSSelDePopup) {
      return;
    }

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    if (bEnfermo) { //PDP 09/02/2000
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion); //mlm nuevo
    }
    CLista data = null;
    Object componente = null;

    //si lleno - y no se pulso popup
    if (txtCodZBS.getText().trim().length() != 0
        && !bNivelZBSSelDePopup) {

      try {
        data = CompruebaNivel(
            servletOBTENER_ZBS_X_CODIGO,
            txtCodZBS.getText().trim(),
            txtCodNivel1.getText().trim(),
            txtCodNivel2.getText().trim());

        if (data.size() == 0) {

          bFocoZBS = true;
          Common.ShowWarning(capp, "Valor incorrecto");
          bFocoZBS = false;
          txtCodZBS.setText("");
          txtDesZBS.setText("");
          bNivelZBSValid = false;

          modoOperacion = modo;
          if (bEnfermo) { //PDP 09/02/2000
            dlgTuber.Inicializar(modoOperacion); //mlm nuevo
          }
          else {
            dlgContacto.Inicializar(modoOperacion); //mlm nuevo
          }
          return;
        }
        else {
          DataEntradaEDO dataEDO = new DataEntradaEDO();
          dataEDO = (DataEntradaEDO) data.elementAt(0);
          txtDesZBS.setText(dataEDO.getDes());

          bNivelZBSSelDePopup = true;
        }

      }
      catch (Exception e) { //acceso a base de datos mal

        bFocoZBS = true;
        Common.ShowWarning(capp, e.getMessage());
        bFocoZBS = false;

        bNivelZBSValid = false;

        //resetea
        txtCodZBS.setText("");
        txtCodZBS.select(txtCodZBS.getText().length(),
                         txtCodZBS.getText().length());
        txtCodZBS.requestFocus();

        modoOperacion = modo;
        if (bEnfermo) { //PDP 09/02/2000
          dlgTuber.Inicializar(modoOperacion); //mlm nuevo
        }
        else {
          dlgContacto.Inicializar(modoOperacion); //mlm nuevo
        }
        return;

      } //fin del try

      modoOperacion = modo;
      if (bEnfermo) { //PDP 09/02/2000
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }

    }
    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel2 es correcto, y se pone su boolean a true
    bNivelZBSValid = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modo;
      if (bEnfermo) { //PDP 09/02/2000
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }
    }
  }

  boolean ValidarDatosSuca(panelsuca nombre) {

//1.Longitud
    if (nombre.getDS_DIREC().length() > 50) {
      Common.ShowError(this.app,
                       "La longitud del campo Via excede de 50 caracteres");
      nombre.setDS_DIREC("");
      nombre.requestFocus();
      return false;
    }
    if (nombre.getDS_NUM().length() > 6) {
      Common.ShowError(this.app,
                       "La longitud del campo N� excede de 6 caracteres");
      nombre.setDS_NUM("");
      nombre.requestFocus();
      return false;
    }
    if (nombre.getDS_PISO().length() > 4) {
      Common.ShowError(this.app,
                       "La longitud del campo Piso excede de 4 caracteres");
      nombre.setDS_PISO("");
      nombre.requestFocus();
      return false;
    }
    if (nombre.getCD_POSTAL().length() > 5) {
      Common.ShowError(this.app,
                       "La longitud del campo C. Postal excede de 5 caracteres");
      nombre.setCD_POSTAL("");
      nombre.requestFocus();
      return false;
    }
    // ************* 29-03-01 *************
    // Correcciones hechas para confirmar que los datos de
    // Municipio y provincia tienen datos.
    if (nombre.getCD_PROV().length() == 0) {
      Common.ShowError(this.app, "Seleccione una provincia");
      nombre.requestFocus();
      return false;
    }
    if (nombre.getCD_MUN().length() == 0) {
      Common.ShowError(this.app, "Seleccione un municipio");
      nombre.requestFocus();
      return false;
    }

    return true;
  } //fin clase

//return: false, es negativo o no numero
//return: true, es ok
  boolean isNum(String Numero) {
    try {
      Integer I = new Integer(Numero);
      int i = I.intValue();
      if (i < 0) {
        return false;
      }
    }
    catch (Exception e) {
      return false;
    }
    return true;
  }

  public boolean validarDatos() {

    boolean correcto = true;

    //1. Perdida de foco
    if (!txtCodNivel1.getText().equals("")) {
      if (txtDesNivel1.getText().equals("")) {
        txtCodNivel1.setText("");
        txtCodNivel1.requestFocus();
        return false;
      }
    }

    if (!txtCodNivel2.getText().equals("")) {
      if (txtDesNivel2.getText().equals("")) {
        txtCodNivel2.setText("");
        txtCodNivel2.requestFocus();
        return false;
      }
    }

    if (!txtCodZBS.getText().equals("")) {
      if (txtDesZBS.getText().equals("")) {
        txtCodZBS.setText("");
        txtCodZBS.requestFocus();
        return false;
      }
    }

    //2. Longitudes de los campos
    if (txtCodEnfermo.getText().length() > 6) {
      Common.ShowError(this.app,
          "La longitud del c�digo del enfermo excede de 6 caracteres");
      txtCodEnfermo.setText("");
      txtCodEnfermo.requestFocus();
      return false;
    }
    if (txtApe1.getText().length() > 20) {
      Common.ShowError(this.app,
          "La longitud del primer apellido excede de 20 caracteres");
      txtApe1.setText("");
      txtApe1.requestFocus();
      return false;
    }
    if (txtApe2.getText().length() > 20) {
      Common.ShowError(this.app,
          "La longitud del segundo apellido excede de 20 caracteres");
      txtApe2.setText("");
      txtApe2.requestFocus();
      return false;
    }
    if (txtNombre.getText().length() > 30) {
      Common.ShowError(this.app,
                       "La longitud del Nombre excede de 30 caracteres");
      txtNombre.setText("");
      txtNombre.requestFocus();
      return false;
    }
    if (txtTelefono.getText().length() > 14) {
      Common.ShowError(this.app,
                       "La longitud del tel�fono excede de 14 caracteres");
      txtTelefono.setText("");
      txtTelefono.requestFocus();
      return false;
    }

    //suca (numericos y longitudes)
    if (!ValidarDatosSuca(panSuca)) {
      return false;
    }

    //3. Valores numericos
    if (!txtTelefono.getText().equals("")) {
      boolean b = isNum(txtTelefono.getText());
      if (!b) {
        Common.ShowError(this.app,
                         "El campo Telefono debe ser un valor Entero Positivo");
        txtTelefono.setText("");
        txtTelefono.requestFocus();
        return false;
      }
    }

    //Campos obligatorios
    if (txtApe1.getText().trim().equals("")) {
      Common.ShowError(this.app, "Es necesario informar el apellido");
      txtApe1.requestFocus();
      return false;
    }

    // Confirmar que est�n rellenos, �rea, distrito, y ZBS.
    if (txtCodNivel1.getText().trim().equals("")) {
      Common.ShowError(this.app, "Introduzca el �rea");
      txtCodNivel1.requestFocus();
      return false;
    }

    if (txtCodNivel2.getText().trim().equals("")) {
      Common.ShowError(this.app, "Introduzca el distrito");
      txtCodNivel2.requestFocus();
      return false;
    }

    if (txtCodZBS.getText().trim().equals("")) {
      Common.ShowError(this.app, "Introduzca la Zona B�sica de Salud");
      txtCodZBS.requestFocus();
      return false;
    }

    return correcto;

  }

//////////////////////                         //////////////////////
//////////////////////FUNCIONES                 //////////////////////
/////////////////////                         //////////////////////

  /**
   *  esta funcion formatea una fecha  a cadena
   *
   * @param a_date es la fecha a formatear
   * @return es la cadena de textocon el formato formatter
   */
  private String date2StringCab(java.util.Date a_date) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    if (a_date == null) {
      return null;
    }

    try {
      return formatter.format(a_date);
    }
    catch (Exception exc) {
      return "";
    }

  }

  /**
   *  esta funcion formatea una fecha
   *
   * @param a_String es la cadena de texto a traducir a fecha
   * @return es la fecha que describ�a la cadena
   */
  private java.util.Date string2DateCab(String a_String) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    if (a_String == null || a_String.trim().length() == 0) {
      return null;
    }

    try {
      ParsePosition pos = new ParsePosition(0);
      return formatter.parse(a_String, pos);
    }
    catch (Exception exc) {
      return null;
    }
  }

  public void FechaNac_desde_BD(String sIT_CALC) {
    //si esta llena (esta validada):
    //label segun el it
    //calculo la edad --> num, a/m
    //edad, cmbo
    if (!txtFechaNac.getText().equals("")) {

      //label
      if (sIT_CALC.equals("S")) { //fue calculada
        lTipoFechaNac.setText("calc");
      }
      if (sIT_CALC.equals("N")) { //fue metida real
        lTipoFechaNac.setText("real");
      } //-------

      //edad-----
      java.util.Date dFecha = Fechas.string2Date(txtFechaNac.getText()); //date
      if (Fechas.edadTipo(dFecha)) { //true-->A�OS
        choEdad.select(0);
        txtEdad.setText( (new Integer(Fechas.edadAnios(dFecha))).toString());
      }
      if (!Fechas.edadTipo(dFecha)) { //false-->MESES
        choEdad.select(1);
        txtEdad.setText( (new Integer(Fechas.edadMeses(dFecha))).toString());
      } //----------

    } //fin no vacio

    //si vacia
    //Label oculta, cmbo a�os, edad vacio
    if (txtFechaNac.getText().equals("")) {
      lTipoFechaNac.setText("");
      choEdad.select(0);
      txtEdad.setText("");
    }

  } //fin funcion

// ******** M�todo de la interfaz de suca  (28-03-01) **********

  // b�squeda de la zonificaci�n sanitaria de un municipio
  public void setZonificacionSanitaria(String mun, String prov) {}

// *****************************

  public int Ape1_Y_QuizasOtros() {

//no se considera cdEnfermo
//1: al menos ape1 informado
//2: ape1 NO, pero algun otro que va en la query de Enfermo
//3: ape1 NO, NINGUN DATO de la query de Enfermo pero algun dato que no vale para la query
//4: sin contar: cdEnfermo, enfermedad y Pais, el resto, vacio
    int iResult = 0;

    if (!txtApe1.getText().trim().equals("")) {
      iResult = 1;
      return (iResult);
    }
    //Ape1 vacia
    //fecha y edad van juntos
    // suca
    if (!txtApe2.getText().trim().equals("")
        || !txtNombre.getText().trim().equals("")
        || !panSuca.getCD_MUN().trim().equals("")
        || !panSuca.getCDVIAL().trim().equals("")
        || !panSuca.getDS_DIREC().trim().equals("")
        || !txtFechaNac.getText().trim().equals("")
        || !txtDocumento.getText().trim().equals("")
        || !panSuca.getCD_PROV().trim().equals("")
        || !Common.getChoiceCDDataGeneralCDDS(choSexo,
                                              listaSexo,
                                              true,
                                              true,
                                              true).trim().equals("")
        || !Common.getChoiceCDDataGeneralCDDS(choTipoDocumento,
                                              listaTipoDocumento,
                                              true, true,
        true).trim().equals("")) {

      iResult = 2;
      return (iResult);
    }

    /*//LOS DATOS NECESARIOS PARA LA QUERY DE ENFERMO ESTAN DESINFORMADOS,
     //PERO BUSCO EL RESTO
    // suca
     if (!txtTelefono.getText().trim().equals("")
       || !panSuca.getDS_NUM().trim().equals("")
       || !panSuca.getDS_PISO().trim().equals("")
      || !panSuca.getCD_POSTAL().trim().equals("")
      || !txtCodNivel1.getText().trim().equals("")
      || !txtCodNivel2.getText().trim().equals("")
      || !txtCodZBS.getText().trim().equals("")
      || !panSuca.getCD_CA().trim().equals("")
      || !panSuca.getCD_PROV().trim().equals("")
    ){
    iResult  = 3;
    return(iResult);
     } */

   iResult = 4;
    return (iResult);

  }

  public void Reset_Cabecera() {

    txtCodEnfermo.setText("");
    txtDocumento.setText("");
    choTipoDocumento.select(0);

    //visible
    txtDocumento.setVisible(false);
    lDocumento.setVisible(false);

    txtApe1.setText("");
    txtApe2.setText("");
    txtNombre.setText("");
    txtTelefono.setText("");
    panSuca.setDS_DIREC("");
    panSuca.setDS_NUM("");
    panSuca.setDS_PISO("");
    panSuca.setCD_POSTAL("");

    txtCodNivel1.setText("");
    txtDesNivel1.setText("");
    txtCodNivel2.setText("");
    txtDesNivel2.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");
    InicializaNivel2();
    InicializaNivelZBS();

    choSexo.select(0);

    txtFechaNac.setText("");
    txtEdad.setText("");
    lTipoFechaNac.setText("");

    panSuca.setCD_MUN("");
    panSuca.setDS_MUN("");
    panSuca.setCD_CA("");
    panSuca.setCD_PROV("");

    //confidencialidad
    lApeNombre.setText("Apellidos  y nombre:"); //oportunidad de alta
    txtApe1.setEnabled(true);
    Visible(true);

  }

  public CLista CompruebaNivel(int iMode, String sCod, String sNivel1,
                               String sNivel2) throws Exception {

    CLista lista = null, data = null;
    data = new CLista();

    data.addElement(new DataEntradaEDO(sCod, "", sNivel1, sNivel2));

    data.setLogin(app.getLogin());
    data.setIdioma(app.getIdioma());
    data.setPerfil(app.getPerfil());
    data.setTSive(app.getTSive());

    data.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
    data.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

    lista = Comunicador.Communicate(getApp(),
                                    stubCliente,
                                    iMode,
                                    strSERVLET,
                                    data);

    lista.trimToSize();
    return lista;
  }

// rellena todos los controles que corresponden a datos del enfermo,
// a partir de la CLista listaEnfermo
//NO LLEVA BACKUP, va en lostfocus y en lupa
  void Pinta_Actualiza_DatosEnfermo(CLista listaEnfermo) {

    Hashtable hash = null;
    String dato = null;

    //desde el btn, sabemos llega lista con size1
    if (listaEnfermo != null) {
      hash = (Hashtable) listaEnfermo.firstElement();
    }
    if (hash == null) {
      return;
    } //*******************************************

    //TENEMOS LOS DATOS

    //confidencialidad ????
    if (!getFlagEnfermo()) { //confid
      Visible(false);

    }

    dato = (String) hash.get("DS_APE1");
    if (dato != null) {
      txtApe1.setText(dato);
    }
    else {
      txtApe1.setText("");

    }
    dato = (String) hash.get("DS_APE2");
    if (dato != null) {
      txtApe2.setText(dato);
    }
    else {
      txtApe2.setText("");

    }
    dato = (String) hash.get("DS_NOMBRE");
    if (dato != null) {
      txtNombre.setText(dato);
    }
    else {
      txtNombre.setText("");

    }
    dato = (hash.get("CD_ENFERMO")).toString();
    if (dato != null) {
      txtCodEnfermo.setText(dato);
    }
    else {
      txtCodEnfermo.setText("");

    }
    String sApe1 = "", sApe2 = "", sNombre = "";
    String sFonoApe1 = "", sFonoApe2 = "", sFonoNombre = "";

    sApe1 = (String) hash.get("DS_APE1");
    sApe2 = (String) hash.get("DS_APE2");
    sNombre = (String) hash.get("DS_NOMBRE");
    if (sApe2 == null) {
      sApe2 = ""; //COMUN no admite null
    }
    if (sNombre == null) {
      sNombre = "";

    }
    sFonoApe1 = Common.traduccionFonetica(sApe1);
    sFonoApe2 = Common.traduccionFonetica(sApe2);
    sFonoNombre = Common.traduccionFonetica(sNombre);

    dato = (String) hash.get("DS_TELEF");
    if (dato != null) {
      txtTelefono.setText(dato);
    }
    else {
      txtTelefono.setText("");
      //-----------------------------------------

      //FECHA NAC Y EDAD
    }
    String sIT_CALC = (String) hash.get("IT_CALC");
    String sFC_NAC = "";

    if ( (String) hash.get("FC_NAC") != null) {
      sFC_NAC = (String) hash.get("FC_NAC");
      sFC_NAC = sFC_NAC.trim();
      //controla el vacio
      txtFechaNac.setText(sFC_NAC);
      FechaNac_desde_BD(sIT_CALC); //no debe ser null IT_CALC
    }
    else { //resetear cajas
      txtFechaNac.setText("");
      FechaNac_desde_BD("");
    }
    //fin fecha--------

    // ponemos los choices
    //Hashtable hash;
    CLista clista;

    /*// enfermedad en set VIENE EN HASH (Jose Miguel)
          String sCodEnfer = "", sDesEnfer = "";
          if ((String)hash.get("CD_ENFCIE") != null)
     sCodEnfer = (String)hash.get("CD_ENFCIE");
          //setEnfermedad (sCodEnfer);  */
   //----------------------------------------------

   // tipo de documento
    String sCodTipoDoc = "", sDesTipoDoc = "";
    if ( (String) hash.get("CD_TDOC") != null) {
      sCodTipoDoc = (String) hash.get("CD_TDOC");
    }
    Common.selectChoiceElement(choTipoDocumento,
                               listaTipoDocumento,
                               true, sCodTipoDoc);

    if (choTipoDocumento.getSelectedIndex() != 0
        && choTipoDocumento.getSelectedIndex() != -1) {
      txtDocumento.setVisible(true);
      lDocumento.setVisible(true);

    }
    else {
      txtDocumento.setVisible(false);
      lDocumento.setVisible(false);

    } //----------------

    dato = (String) hash.get("DS_NDOC");
    if (dato != null) {
      txtDocumento.setText(dato);
    }
    else {
      txtDocumento.setText("");

      // sexo
    }
    String sCodSexo = "";
    if ( (String) hash.get("CD_SEXO") != null) {
      sCodSexo = (String) hash.get("CD_SEXO");
    }
    Common.selectChoiceElement(choSexo,
                               listaSexo,
                               true, sCodSexo);

    //SUCA -----------------------------------------------------------
    //panSuca.setModoDeshabilitado();

    // el pa�s es Espa�a
    // Comunidad Aut�noma
    String sCodCA = "";
    if ( (String) hash.get("CD_CA") != null) {
      sCodCA = (String) hash.get("CD_CA");
    }
    setCA(sCodCA);

    //prov
    if (!sCodCA.equals("")) {
      String sCodProv = "";
      if ( (String) hash.get("CD_PROV") != null) {
        sCodProv = (String) hash.get("CD_PROV");
      }
      setProvincia(sCodProv);
    }
    // el municipio
    dato = (String) hash.get("CD_MUN");
    if (dato != null) {
      panSuca.setCD_MUN(dato);
    }
    else {
      panSuca.setCD_MUN("");
    }
    dato = (String) hash.get("DS_MUN");
    if (dato != null) {
      panSuca.setDS_MUN(dato);
    }
    else {
      panSuca.setDS_MUN("");

      // los 4 nuevos
    }
    dato = (String) hash.get("CDTVIA");
    if (dato != null) {
      panSuca.setCDTVIA(dato);
    }
    else {
      panSuca.setCDTVIA("");

    }
    dato = (String) hash.get("CDVIAL");
    if (dato != null) {
      panSuca.setCDVIAL(dato);
    }
    else {
      panSuca.setCDVIAL("");

    }
    dato = (String) hash.get("CDTNUM");
    if (dato != null) {
      panSuca.setCDTNUM(dato);
    }
    else {
      panSuca.setCDTNUM("");

    }
    dato = (String) hash.get("DSCALNUM");
    if (dato != null) {
      panSuca.setDSCALNUM(dato);
    }
    else {
      panSuca.setDSCALNUM("");
      //los 4 ***

    }
    dato = (String) hash.get("DS_DIREC");
    if (dato != null) {
      panSuca.setDS_DIREC(dato);
    }
    else {
      panSuca.setDS_DIREC("");

    }
    dato = (String) hash.get("DS_NUM");
    if (dato != null) {
      panSuca.setDS_NUM(dato);
    }
    else {
      panSuca.setDS_NUM("");

    }
    dato = (String) hash.get("DS_PISO");
    if (dato != null) {
      panSuca.setDS_PISO(dato);
    }
    else {
      panSuca.setDS_PISO("");

    }
    dato = (String) hash.get("CD_POSTAL");
    if (dato != null) {
      panSuca.setCD_POSTAL(dato);
    }
    else {
      panSuca.setCD_POSTAL(dato);

      //niveles zonas ----

      //n1 y popup
    }
    dato = (String) hash.get("CD_NIVEL_1");
    if (dato != null) {
      txtCodNivel1.setText(dato);
      bNivel1Valid = true;
      bNivel1SelDePopup = true;
    }
    else {
      txtCodNivel1.setText("");
      //desc
    }
    dato = (String) hash.get("DS_NIVEL_1");
    if (dato != null) {
      txtDesNivel1.setText(dato);
    }
    else {
      txtDesNivel1.setText("");
      //---------

      //n2 y popup
    }
    dato = (String) hash.get("CD_NIVEL_2");
    if (dato != null) {
      txtCodNivel2.setText(dato);
      bNivel2Valid = true;
      bNivel2SelDePopup = true;
    }
    else {
      txtCodNivel2.setText("");
      //desc
    }
    dato = (String) hash.get("DS_NIVEL_2");
    if (dato != null) {
      txtDesNivel2.setText(dato);
    }
    else {
      txtDesNivel2.setText("");
      //---------

      //zbs y popup
    }
    dato = (String) hash.get("CD_ZBS");
    if (dato != null) {
      txtCodZBS.setText(dato);
      bNivelZBSValid = true;
      bNivelZBSSelDePopup = true;
    }
    else {
      txtCodZBS.setText("");
      //desc
    }
    dato = (String) hash.get("DS_ZBS");
    if (dato != null) {
      txtDesZBS.setText(dato);
    }
    else {
      txtDesZBS.setText("");
      //---------

    }

    this.doLayout();

    //cargamos la estructura de datos del enfermo
    String sFC_ULTACT = (String) hash.get("FC_ULTACT");

    //****************
    if (bEnfermo) { // PDP 10/02/2000
      dlgTuber.datcasostub.setCD_ENFERMO( (hash.get("CD_ENFERMO")).toString());
      dlgTuber.datcasostub.setCD_TDOC( (String) hash.get("CD_TDOC"));
      dlgTuber.datcasostub.setDS_APE1(sApe1);
      dlgTuber.datcasostub.setDS_APE2(sApe2);
      dlgTuber.datcasostub.setDS_NOMBRE(sNombre);
      dlgTuber.datcasostub.setDS_FONOAPE1(sFonoApe1);
      dlgTuber.datcasostub.setDS_FONOAPE2(sFonoApe2);
      dlgTuber.datcasostub.setDS_FONONOMBRE(sFonoNombre);
      dlgTuber.datcasostub.setIT_CALC(sIT_CALC);
      dlgTuber.datcasostub.setFC_NAC(sFC_NAC);
      dlgTuber.datcasostub.setCD_SEXO( (String) hash.get("CD_SEXO"));
      dlgTuber.datcasostub.setDS_TELEF( (String) hash.get("DS_TELEF"));
      dlgTuber.datcasostub.setCD_NIVEL_1( (String) hash.get("CD_NIVEL_1"));
      dlgTuber.datcasostub.setCD_NIVEL_2( (String) hash.get("CD_NIVEL_2"));
      dlgTuber.datcasostub.setCD_ZBS( (String) hash.get("CD_ZBS"));
      dlgTuber.datcasostub.setCD_OPE( (String) hash.get("CD_OPE"));
      dlgTuber.datcasostub.setFC_ULTACT(sFC_ULTACT);
      dlgTuber.datcasostub.setDS_NDOC( (String) hash.get("DS_NDOC"));
      dlgTuber.datcasostub.setSIGLAS( (String) hash.get("SIGLAS"));
      //flags de it_contacto y de it_enfermo
      dlgTuber.datcasostub.setIT_ENFERMO( (String) hash.get("IT_ENFERMO"));
      dlgTuber.datcasostub.setIT_CONTACTO( (String) hash.get("IT_CONTACTO"));
      //**********************
    }
    else {
      dlgContacto.datconcastub.setCD_ENFERMO(new Integer( (hash.get(
          "CD_ENFERMO")).toString()).intValue());
      dlgContacto.datconcastub.setCD_TDOC( (String) hash.get("CD_TDOC"));
      dlgContacto.datconcastub.setDS_APE1(sApe1);
      dlgContacto.datconcastub.setDS_APE2(sApe2);
      dlgContacto.datconcastub.setDS_NOMBRE(sNombre);
      dlgContacto.datconcastub.setDS_FONOAPE1(sFonoApe1);
      dlgContacto.datconcastub.setDS_FONOAPE2(sFonoApe2);
      dlgContacto.datconcastub.setDS_FONONOMBRE(sFonoNombre);
      dlgContacto.datconcastub.setIT_CALC(sIT_CALC);
      dlgContacto.datconcastub.setFC_NAC(Fechas.StringToSQLDate(sFC_NAC));
      dlgContacto.datconcastub.setCD_SEXO( (String) hash.get("CD_SEXO"));
      dlgContacto.datconcastub.setDS_TELEF( (String) hash.get("DS_TELEF"));
      dlgContacto.datconcastub.setCD_NIVEL_1( (String) hash.get("CD_NIVEL_1"));
      dlgContacto.datconcastub.setCD_NIVEL_2( (String) hash.get("CD_NIVEL_2"));
      dlgContacto.datconcastub.setCD_ZBS( (String) hash.get("CD_ZBS"));
      dlgContacto.datconcastub.setCD_OPE( (String) hash.get("CD_OPE"));
      dlgContacto.datconcastub.setFC_ULTACT(Fechas.string2Timestamp(sFC_ULTACT));
      dlgContacto.datconcastub.setDS_NDOC( (String) hash.get("DS_NDOC"));
      dlgContacto.datconcastub.setSIGLAS( (String) hash.get("SIGLAS"));
      //flags de it_contacto y de it_enfermo
      dlgContacto.datconcastub.setIT_ENFERMO( (String) hash.get("IT_ENFERMO"));
      dlgContacto.datconcastub.setIT_CONTACTO( (String) hash.get("IT_CONTACTO"));
      // modificacion 01/03/2001
      // Metemos el a�o y el n�mero de registro.
      dlgContacto.datconcastub.setCD_ANO_RTBC(sAnio);
      dlgContacto.datconcastub.setCD_REG_RTBC(sNumReg);

      // modificacion 01/03/2001
      sStringCodEnf = txtCodEnfermo.getText();
      dlgContacto.Inicializar(modoMODIFICACION);
      dlgContacto.addProtocolo();
    }
    //**********************

     //confidencialidad -----------
     if (!getFlagEnfermo()) { //confid
       Confidencialidad();
     }
     else {
       Visible(true);
     } //---------------------------

  }

  String cadenaBarra(int tipo) {
    String cad = "Buscando enfermos coincidentes ";
    switch (tipo) {
      case 0:
        break;
      case 1:
        cad = cad + "por documento...";
        break;
      case 2:
        cad = cad + "por nombre, municipio y via ...";
        break;
      case 3:
        cad = cad + "por nombre y fecha de nacimiento...";
        break;
    }
    return cad;
  }

//////////////////////                         //////////////////////
//////////////////////INTERFACES                 //////////////////////
/////////////////////                         //////////////////////

// SUCA
  public void vialInformado() {

    int modo = modoOperacion;

    //sin datos o sin permiso
    // suca
    if (txtApe1.getText().trim().equals("")
        || !bDeboLanzarEnfermo
        || panSuca.getDS_DIREC().trim().equals("")
        || panSuca.getCD_MUN().trim().equals("")
        || panSuca.getCD_PROV().trim().equals("")
        ) {
      return;
    }

    //con datos suficientes y con permiso de lanzar Automatismo
    int iBuscar = 0;
    iBuscar = Buscar_Ficha(2);
    if (iBuscar == -1 || iBuscar == 0) { //No hay datos
      return;
    }
    else if (iBuscar == 1) { //aceptar
      bDeboLanzarEnfermo = false;
      //se habra pintado el enfermo

      //------- CASO COINCIDENTE ----------
      //String sStringDespues = txtCodEnfermo.getText();//por si se vuelve a llenar
      sStringCodEnf = new String();
      sStringCodEnf = txtCodEnfermo.getText();
      int ibuscar = 0;
      ibuscar = Buscar_Registro();

      if (ibuscar == 0 || ibuscar == -1) {
        String n1antes = "";
        String n1desp = "";
        String n2antes = "";
        String n2desp = "";
        //Paneles_Lupa ( sStringBk, sStringDespues, n1antes, n1desp, n2antes, n2desp);//llama a inicializar en alta
        Paneles_Lupa(sStringBk, sStringCodEnf, n1antes, n1desp, n2antes, n2desp); //llama a inicializar en alta
      }
      else if (ibuscar == 1) { //acepta caso
        //ya estan los paneles y en modificacion
      }

      //BACKUPS ****
      sStringBk = txtCodEnfermo.getText();

    } //ACEPTAR
  }

  // b�squeda de la zonificaci�n sanitaria de un municipio
  public void setZonificacionSanitaria(String mun) {

    //Entra en modo ESPERA

    DataMunicipioEDO data = new DataMunicipioEDO(mun, "");

    if (mun.trim().equals("")) {
      txtCodNivel1.setText("");
      txtDesNivel1.setText("");
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      txtCodZBS.setText("");
      txtDesZBS.setText("");

    }
    else {

      try {
        CLista listadata = new CLista();
        listadata.addElement(data);
        listadata = Comunicador.Communicate(this.getApp(),
                                            stubCliente,
                                            0, //no lleva modo
                                            strSERVLET_MUNICIPIO_CONT,
                                            listadata);
        //listadata.size()==1
        data = (DataMunicipioEDO) listadata.firstElement();
      }
      catch (Exception e) {
        Common.ShowWarning(capp, "Error en continuaci�n datos Municipio");
        modoOperacion = iAntesDeZona;
        if (bEnfermo) { //PDP 04/02/2000
          dlgTuber.Inicializar(modoOperacion); //mlm nuevo
        }
        else {
          dlgContacto.Inicializar(modoOperacion);
        }

        return;
      }

      txtCodNivel1.setText(data.getCodNivel1());
      txtDesNivel1.setText(data.getDesNivel1());
      txtCodNivel2.setText(data.getCodNivel2());
      txtDesNivel2.setText(data.getDesNivel2());
      txtCodZBS.setText(data.getCodZBS());
      txtDesZBS.setText(data.getDesZBS());
      if (!txtDesNivel1.getText().trim().equals("")) {
        bNivel1Valid = true;
        bNivel1SelDePopup = true;
      }
      if (!txtDesNivel2.getText().trim().equals("")) {
        bNivel2Valid = true;
        bNivel2SelDePopup = true;
      }
      if (!txtDesZBS.getText().trim().equals("")) {
        bNivelZBSValid = true;
        bNivelZBSSelDePopup = true;
      }

    }

    modoOperacion = iAntesDeZona;
    if (bEnfermo) { //PDP 04/02/2000
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }

  }

  public void cambiaModoTramero(boolean b) {
    bTramero = b;
  }

  public int ponerEnEspera() {
    int m = modoOperacion;

    iAntesDeZona = m; //para saber en setZona...si alta o modif

    modoOperacion = modoESPERA;
    if (bEnfermo) { //PDP 04/02/2000
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }
    return m;
  }

  public void ponerModo(int modo) {
    modoOperacion = modo;
    if (bEnfermo) { //PDP 04/02/2000
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }

  }

//////////////////////                         //////////////////////
//////////////////////BOTON LUPA, EVENTOS(LOST FECNAC...)//////////////////////
/////////////////////                         //////////////////////

  void btnLupaEnfermo_actionPerformed() {

    int modo = modoOperacion;
    modoOperacion = modoESPERA;

    if (bEnfermo) {
      dlgTuber.Inicializar(modoOperacion);
    }
    else {
      dlgContacto.Inicializar(modoOperacion);
    }

    String sMetaModo = null;

    if (bEnfermo) {
      sMetaModo = constantes.sSoloEnfermos;
    }
    else {
      sMetaModo = constantes.sSoloContactos;
    }

    enfermotub.DialBusEnfermo dlgEnfermo = new enfermotub.DialBusEnfermo(this.
        getApp(), sMetaModo, true);
    dlgEnfermo.show();

    CLista listaEnfermo = dlgEnfermo.getListaDatosEnfermo();
    // se piden los datos y rellenamos la tabla
    if (listaEnfermo != null) {

      if (listaEnfermo.size() != 0) {
        //backup de niveles anterior si habia enfermo (antes)
        Pinta_Actualiza_DatosEnfermo(listaEnfermo);
        //(despues)
        String sStringDespues = txtCodEnfermo.getText();
        //---------- CASO COINCIDENTE----
        //<AIC>
        if (sMetaModo == constantes.sSoloEnfermos) { //</AIC>
          int ibuscar = 0;
          ibuscar = Buscar_Registro();
          if (ibuscar == 0 || ibuscar == -1) { //no toma caso. Abrir paneles caso+ protocolo
            //String n1antes=""; String n1desp=""; String n2antes=""; String n2desp = "";
            //Paneles_Lupa ( sStringBk, sStringDespues, n1antes, n1desp, n2antes, n2desp);//llama a inicializar en alta
            modoOperacion = modo;
            if (bEnfermo) {
              dlgTuber.Inicializar(modoOperacion);
            }
            else {
              dlgContacto.Inicializar(modoOperacion);
            }
            //////////////
          }
          else if (ibuscar == 1) { //acepta caso
            //ya estan los paneles y en modificacion
          }
          //------------------------
        }
        //<AIC>
        else {
          //modoOperacion = modo;
          modoOperacion = modoMODIFICACION;
          //dlgContacto.Inicializar(modoOperacion);
          // modificacion 01/03/2001
          dlgContacto.Inicializar(modoMODIFICACION);
        } //</AIC>
      } //size
      else { //cancelar  (size=0)
        //No se ha elegido nada
        modoOperacion = modo;
        if (bEnfermo) {
          dlgTuber.Inicializar(modoOperacion);
        }
        else {
          dlgContacto.Inicializar(modoOperacion);
        }
      }
    } //null
    else { //(=null)
      modoOperacion = modo;
      if (bEnfermo) {
        dlgTuber.Inicializar(modoOperacion);
      }
      else {
        dlgContacto.Inicializar(modoOperacion);
      }
    }

    sStringBk = txtCodEnfermo.getText(); //ACTUALIZO BACKUP de enfermo
    //en modif no hay lupa de enfermo. No procede sN1bk..
  } //FIN DE LUPA de enfermo

//PANELES --------------------
  protected void Paneles_Lupa(String sCodBk, String sCodDespues,
                              String n1antes, String n1despues,
                              String n2antes, String n2despues) {

    //Si el enfermo estaba vacio, ahora lleno
    //y con enfermedad (011)-->caso+proto
    //<AIC>
    if (dlgTuber != null) { //</AIC>
      if (sCodBk.equals("") &&
          !sCodDespues.equals("")) {
//                  dlgTuber.Inicializar(modoESPERA);
//                  dlgTuber.addCaso();
//                  dlgTuber.Inicializar(modoESPERA);
//                  dlgTuber.addProtocolo();  //ALTA (lupa activa solo en alta)
        dlgTuber.Inicializar(modoALTA);

      }

      //si el enfermo estaba lleno, ahora lleno de momento abro again
      //hacer solo si cambian niveles

      if (!sCodBk.equals("") &&
          !sCodDespues.equals("")) {
        //enfermos distintos
        if (!sCodBk.equals(sCodDespues)) {
//                if (dlgTuber.panCaso != null)
//                  dlgTuber.borrarCaso();
//                if (dlgTuber.pProtocolo != null )
//                  dlgTuber.borrarProtocolo();

//                  dlgTuber.Inicializar(modoESPERA);
//                  dlgTuber.addCaso();
//                  dlgTuber.Inicializar(modoESPERA);
//                  dlgTuber.addProtocolo();  //ALTA  (lupa activa solo en alta)
          dlgTuber.Inicializar(modoALTA);

        }
        //si igual tb
        else {
//                  if (dlgTuber.panCaso != null)
//                  dlgTuber.borrarCaso();
//                if (dlgTuber.pProtocolo != null )
//                  dlgTuber.borrarProtocolo();

//                  dlgTuber.Inicializar(modoESPERA);
//                  dlgTuber.addCaso();
//                  dlgTuber.Inicializar(modoESPERA);
//                  dlgTuber.addProtocolo();  //ALTA  (lupa activa solo en alta)
          dlgTuber.Inicializar(modoALTA);

        }
      }
    }
    //<AIC>
    else { //dlgTub = null, tenemos que hacer los cambios con dlgContacto
      if (sCodBk.equals("") &&
          !sCodDespues.equals("")) {
        dlgContacto.Inicializar(modoESPERA);
        //dlgContacto.addCaso();
        //dlgContacto.Inicializar(modoESPERA);

        dlgContacto.Inicializar(modoMODIFICACION);
        dlgContacto.addProtocolo(); //ALTA (lupa activa solo en alta)
        //dlgContacto.Inicializar(modoALTA);
        // modificacion 12/02/2001
        //dlgContacto.Inicializar(modoMODIFICACION);

      }
    }
    //</AIC>
    //-------------
  } //fin paneles_lupa

//FECHA NAC LOST FOCUS
  public void txtFechaNac_focusLost() {

    txtFechaNac.ValidarFecha(); //validamos

    //MAntengo, aunque sean iguales-----
    if (txtFechaNac.getValid().equals("S")) { //fecha ok, llena
      txtFechaNac.setText(txtFechaNac.getFecha()); //Cargo

      //fec nac <= dia Hoy
      int iLimite = 0;
      iLimite = UtilEDO.Compara_Fechas(UtilEDO.getFechaAct(),
                                       txtFechaNac.getText().trim());
      if (iLimite == 2) { //sin sentido!!
        txtFechaNac.setText("");
        //------------------
        //de cualquier modo
      }
      FechaNac_desde_BD("N"); //la metemos real
    }
    else { //ko, vacio
      txtFechaNac.setText(txtFechaNac.getFecha()); //Cargo, vacio

      FechaNac_desde_BD("N"); //la metemos real
    } //------------------------------------

    //AUTOMATISMO ENFERMO-CASO   ---------------------
    int modo = modoOperacion;

    //sin dato o sin permiso
    if (!bDeboLanzarEnfermo
        || txtFechaNac.getText().trim().equals("")
        || txtApe1.getText().trim().equals("")) {
      return;
    }

    //con datos suficientes y con permiso de lanzar Automatismo
    int iBuscar = 0;
    iBuscar = Buscar_Ficha(3);
    if (iBuscar == -1 || iBuscar == 0) { //No hay datos-cancelar
      return;
    }
    else if (iBuscar == 1) { //aceptar
      bDeboLanzarEnfermo = false;
      //se habra pintado el enfermo

      //------------CASO COINCIDENTE -------------
      //String sStringDespues = txtCodEnfermo.getText();//por si se vuelve a llenar
      sStringCodEnf = new String();
      sStringCodEnf = txtCodEnfermo.getText();
      int ibuscar = 0;
      ibuscar = Buscar_Registro();
      if (ibuscar == 0 || ibuscar == -1) {
        String n1antes = "";
        String n1desp = "";
        String n2antes = "";
        String n2desp = "";
        //Paneles_Lupa ( sStringBk, sStringDespues, n1antes, n1desp, n2antes, n2desp);//llama a inicializar en alta
        Paneles_Lupa(sStringBk, sStringCodEnf, n1antes, n1desp, n2antes, n2desp); //llama a inicializar en alta
      }
      else if (ibuscar == 1) { //acepta caso
        //ya estan los paneles y en modificacion
      }

      //BACKUPS ****
      sStringBk = txtCodEnfermo.getText();
      //Busqueda, solo en alta

    } //ACEPTAR

  }

//focuslost en txtEdad
  public void txtEdad_focusLost() {

    if (txtEdad.getText().equals("")) {
      //choEdad.select(0); //a�os
      return;
    }
    //edades backup
    //ACTUAL
    int valor = Integer.parseInt(txtEdad.getText());
    int tipoEdad = choEdad.getSelectedIndex();

    int valorBk = 0;
    int tipoEdadBk = 0;

    //calcular la edad a partir de la fecha
    //si coinciden, return   No coinciden, calcular la fecha y cambiar

    //calcular la edad a partir de la fecha
    //si coinciden, return   No coinciden, calcular la fecha y cambiar
    java.util.Date dFecha = string2DateCab(txtFechaNac.getText()); //date
    if (Fechas.edadTipo(dFecha)) { //true-->A�OS

      valorBk = Fechas.edadAnios(dFecha);
      tipoEdadBk = 0;
    }
    if (!Fechas.edadTipo(dFecha)) { //false-->MESES

      valorBk = Fechas.edadMeses(dFecha);
      tipoEdadBk = 1;
    } //----------
    if ( (valor == valorBk) &&
        (tipoEdad == tipoEdadBk)) { //IGUALES, es un lostfocus sin cambio
      return;
    }
    if ( (valor != valorBk) ||
        (tipoEdad != tipoEdadBk)) { //LOSTFOCUS VERDADERO  --> cambiar fecha y calc
      //a�os****
      if (choEdad.getSelectedIndex() == 0) {
        //calcualar la fecha  despues

        //////////////
        //String pepe = new String();
        //pepe = (String) CfechaNotif.getText();
        java.util.Date result = Fechas.restaTiempo(valor, false,
            new java.util.Date());
        txtFechaNac.setText(date2StringCab(result));
        lTipoFechaNac.setText("calc");

      }
      //meses
      if (choEdad.getSelectedIndex() == 1) {
        //calcualar la fecha  despues
        java.util.Date result = Fechas.restaTiempo(valor, true,
            new java.util.Date());
        txtFechaNac.setText(date2StringCab(result));
        lTipoFechaNac.setText("calc");

      }
    }

    //AUTOMATISMO ENFERMO-CASO   ---------------------
    int modo = modoOperacion;
    //sin dato o sin permiso
    if (!bDeboLanzarEnfermo
        || txtFechaNac.getText().trim().equals("")
        || txtApe1.getText().trim().equals("")) {
      return;
    }
    //con datos suficientes y con permiso de lanzar Automatismo
    int iBuscar = 0;
    iBuscar = Buscar_Ficha(3);
    if (iBuscar == -1 || iBuscar == 0) { //No hay datos-cancelar
      return;
    }
    else if (iBuscar == 1) { //aceptar
      bDeboLanzarEnfermo = false;
      //se habra pintado el enfermo

      //--------- CASO COINCIDENTE ----------------
      //String sStringDespues = txtCodEnfermo.getText();//por si se vuelve a llenar
      sStringCodEnf = new String();
      sStringCodEnf = txtCodEnfermo.getText();
      int ibuscar = 0;
      ibuscar = Buscar_Registro();
      if (ibuscar == 0 || ibuscar == -1) {
        String n1antes = "";
        String n1desp = "";
        String n2antes = "";
        String n2desp = "";
        //Paneles_Lupa ( sStringBk, sStringDespues, n1antes, n1desp, n2antes, n2desp);//llama a inicializar en alta
        Paneles_Lupa(sStringBk, sStringCodEnf, n1antes, n1desp, n2antes, n2desp); //llama a inicializar en alta

      }
      else if (ibuscar == 1) { //acepta caso
        //ya estan los paneles y en modificacion
      }

      //BACKUPS ****
      sStringBk = txtCodEnfermo.getText();
      //busqueda solo en alta

    } //ACEPTAR

  } //fin focuslost en txtEdad

  public void txtDocumento_focusLost() {
    int modo = modoOperacion;

    //System.out.println("bdebolanzarenfermo" +  bDeboLanzarEnfermo);
    //System.out.println("doc" +  txtDocumento.getText().trim());

    //sin dato o sin permiso
    if (!bDeboLanzarEnfermo
        || txtDocumento.getText().trim().equals("")) {
      return;
    }
    //con datos suficientes y con permiso de lanzar Automatismo
    int iBuscar = 0;
    iBuscar = Buscar_Ficha(1);
    if (iBuscar == -1 || iBuscar == 0) { //No hay datos
      return;
    }
    else if (iBuscar == 1) { //aceptar
      bDeboLanzarEnfermo = false;
      //se habra pintado el enfermo

      //------------- CASO COINCIDENTE ------------------
      //String sStringDespues = txtCodEnfermo.getText();//por si se vuelve a llenar
      // variable p�blica que env�a el codigo enfermo al di�logo
      sStringCodEnf = new String();
      sStringCodEnf = txtCodEnfermo.getText();
      int ibuscar = 0;
      ibuscar = Buscar_Registro();
      if (ibuscar == 0 || ibuscar == -1) {
        String n1antes = "";
        String n1desp = "";
        String n2antes = "";
        String n2desp = "";
        //Paneles_Lupa ( sStringBk, sStringDespues, n1antes, n1desp, n2antes, n2desp);//llama a inicializar en alta
        Paneles_Lupa(sStringBk, sStringCodEnf, n1antes, n1desp, n2antes, n2desp); //llama a inicializar en alta

      }
      else if (ibuscar == 1) { //acepta caso
        //ya estan los paneles y en modificacion
      }

      //BACKUPS ****
      sStringBk = txtCodEnfermo.getText();

    } //ACEPTAR

  } //fin lostfocus de documento

  public DatCasosTub recogerDatos(DatCasosTub datos) {

    //ALTA enfermo
    if (txtCodEnfermo.getText().trim().equals("")) {

      datos.setIT_ENFERMO("S");
      datos.setIT_CONTACTO("N");
      datos.setCD_ENFERMO(txtCodEnfermo.getText().trim());
      datos.setCD_PROV(panSuca.getCD_PROV());
      datos.setCD_PAIS(panSuca.getCD_PAIS());
      datos.setCD_MUN(panSuca.getCD_MUN());
      if (txtDocumento.getText().trim().equals("")) {
        datos.setCD_TDOC("");
      }
      else {
        datos.setCD_TDOC(getCodTipoDocumento());

      }
      datos.setDS_APE1(txtApe1.getText().trim());
      datos.setDS_APE2(txtApe2.getText().trim());
      datos.setDS_NOMBRE(txtNombre.getText().trim());
      datos.setDS_FONOAPE1(Common.traduccionFonetica(txtApe1.getText().trim()));
      datos.setDS_FONOAPE2(Common.traduccionFonetica(txtApe2.getText().trim()));
      datos.setDS_FONONOMBRE(Common.traduccionFonetica(txtNombre.getText().trim()));
      if (lTipoFechaNac.getText().trim().equals("real")) {
        datos.setIT_CALC("N");
      }
      else {
        datos.setIT_CALC("S");

      }
      datos.setFC_NAC(txtFechaNac.getText().trim());
      datos.setCD_SEXO(getCodSexo());

      datos.setCD_POSTAL(panSuca.getCD_POSTAL());
      datos.setDS_DIREC(panSuca.getDS_DIREC());
      datos.setDS_NUM(panSuca.getDS_NUM());
      datos.setDS_PISO(panSuca.getDS_PISO());
      datos.setDS_TELEF(txtTelefono.getText().trim());
      datos.setCD_NIVEL_1(txtCodNivel1.getText().trim());
      datos.setCD_NIVEL_2(txtCodNivel2.getText().trim());
      datos.setCD_ZBS(txtCodZBS.getText().trim());
      datos.setIT_REVISADO("N");

      datos.setDS_NDOC(txtDocumento.getText().trim());
      datos.setSIGLAS(Common.Calcular_Siglas(txtApe1.getText().trim(),
                                             txtApe2.getText().trim(),
                                             txtNombre.getText().trim()));

      datos.setCDVIAL(panSuca.getCDVIAL());
      datos.setCDTVIA(panSuca.getCDTVIA());
      datos.setCDTNUM(panSuca.getCDTNUM());
      datos.setDSCALNUM(panSuca.getDSCALNUM());
    }
    else { //modif de enfermo
      //meteremos un campo que indique si se ha modificado o no el enfermo
      if (Cambio_de_Ficha()) { //$$$$$$$$$$$$$$$lo que sea

      }

      //it_contacto no se toca
      datos.setIT_ENFERMO("S");

      //cdope y fcultact hay que modificarlo en datCasosTub del dialogo
      datos.setCD_ENFERMO(txtCodEnfermo.getText().trim());
      datos.setDS_NDOC(txtDocumento.getText().trim());
      if (txtDocumento.getText().trim().equals("")) {
        datos.setCD_TDOC("");
      }
      else {
        datos.setCD_TDOC(getCodTipoDocumento());

      }
      if (lTipoFechaNac.getText().trim().equals("real")) {
        datos.setIT_CALC("N");
      }
      else {
        datos.setIT_CALC("S");

      }
      datos.setFC_NAC(txtFechaNac.getText().trim());
      datos.setCD_SEXO(getCodSexo());
      datos.setDS_TELEF(txtTelefono.getText().trim());
      if (getFlagEnfermo()) { //true it=S lo ve todo
        datos.setDS_APE1(txtApe1.getText().trim());
        datos.setDS_APE2(txtApe2.getText().trim());
        datos.setDS_NOMBRE(txtNombre.getText().trim());
        datos.setDS_FONOAPE1(Common.traduccionFonetica(txtApe1.getText().trim()));
        datos.setDS_FONOAPE2(Common.traduccionFonetica(txtApe2.getText().trim()));
        datos.setDS_FONONOMBRE(Common.traduccionFonetica(txtNombre.getText().
            trim()));
        datos.setSIGLAS(Common.Calcular_Siglas(txtApe1.getText().trim(),
                                               txtApe2.getText().trim(),
                                               txtNombre.getText().trim()));
      }
      else {
        //si es confidencial, no puede tocar nada, ptt se le envia lo mismo que en datCasoTub de la select
      }
    }

    //CASO ***************
    //ALTA DE CASO
    if (dlgTuber.sNM_EDO.equals("")) {

      datos.setCD_ENFERMO_EDOIND(txtCodEnfermo.getText().trim());
      datos.setCD_ANOEPI(dlgTuber.panTabla.anoPrimero());
      datos.setCD_SEMEPI(dlgTuber.panTabla.semanaPrimero());
      datos.setFC_RECEP(dlgTuber.panTabla.frecepPrimero());
      datos.setFC_FECNOTIF(dlgTuber.panTabla.fnotifPrimero());

      datos.setCDVIAL_EDOIND(panSuca.getCDVIAL());
      datos.setCDTVIA_EDOIND(panSuca.getCDTVIA());
      datos.setCDTNUM_EDOIND(panSuca.getCDTNUM());
      datos.setDSCALNUM_EDOIND(panSuca.getDSCALNUM());

      datos.setCD_NIVEL_1_EDOIND(txtCodNivel1.getText().trim());
      datos.setCD_NIVEL_2_EDOIND(txtCodNivel2.getText().trim());
      datos.setCD_ZBS_EDOIND(txtCodZBS.getText().trim());

      datos.setCD_ANOURG(null);
      datos.setNM_ENVIOURGSEM(null);
      datos.setIT_PENVURG("N");
      datos.setCD_ANOOTRC(null);
      datos.setNM_ENVOTRC(null);

      datos.setCD_PROV_EDOIND(panSuca.getCD_PROV());
      datos.setCD_MUN_EDOIND(panSuca.getCD_MUN());

      datos.setDS_CALLE(panSuca.getDS_DIREC());
      datos.setDS_NMCALLE(panSuca.getDS_NUM());
      datos.setDS_PISO_EDOIND(panSuca.getDS_PISO());
      datos.setCD_POSTAL_EDOIND(panSuca.getCD_POSTAL());
      // modificacion 12/02/2001
      //datos.setCD_ENFCIE(constantes.TUBERCULOSIS);
      datos.setCD_ENFCIE(this.app.getParametro("CD_TUBERCULOSIS"));

    }
    else {
      //MODIF DE CASO

      // Ya veremos pq es necesario

      try {
        datos.put("NM_EDO", new Integer(dlgTuber.sNM_EDO));
        //System.out.println(datos.getNM_EDO());
      }
      catch (Exception x) {
        //System.out.println(datos.getNM_EDO());
      }

      datos.setCD_ENFERMO_EDOIND(txtCodEnfermo.getText().trim());
      datos.setCD_ANOEPI(dlgTuber.panTabla.anoPrimero());
      datos.setCD_SEMEPI(dlgTuber.panTabla.semanaPrimero());
      datos.setFC_RECEP(dlgTuber.panTabla.frecepPrimero());
      datos.setFC_FECNOTIF(dlgTuber.panTabla.fnotifPrimero());

      //CD_ANOURG, NM_ENVIOURGSEM , IT_PENVURG , CD_ANOOTRC, NM_ENVOTRC no modificar
      //cd_enfcie no se modifiar, no tocar
      datos.setCD_NIVEL_1_EDOIND(txtCodNivel1.getText().trim());
      datos.setCD_NIVEL_2_EDOIND(txtCodNivel2.getText().trim());
      datos.setCD_ZBS_EDOIND(txtCodZBS.getText().trim());

      datos.setCD_PROV_EDOIND(panSuca.getCD_PROV());
      datos.setCD_MUN_EDOIND(panSuca.getCD_MUN());

      datos.setDS_CALLE(panSuca.getDS_DIREC());
      datos.setDS_NMCALLE(panSuca.getDS_NUM());
      datos.setDS_PISO_EDOIND(panSuca.getDS_PISO());
      datos.setCD_POSTAL_EDOIND(panSuca.getCD_POSTAL());

      datos.setCDVIAL_EDOIND(panSuca.getCDVIAL());
      datos.setCDTVIA_EDOIND(panSuca.getCDTVIA());
      datos.setCDTNUM_EDOIND(panSuca.getCDTNUM());
      datos.setDSCALNUM_EDOIND(panSuca.getDSCALNUM());

    }

    return (datos);
  }

//Recoge los datos de un contacto  //PDP 07/02/2000
  public DatConCasTub recogerDatContacto(DatConCasTub datoscon) {

    //ALTA contacto
    if (txtCodEnfermo.getText().trim().equals("")) {

      datoscon.setIT_ENFERMO("N");
      datoscon.setIT_CONTACTO("S");

      datoscon.setCD_ENFERMO( -1);

      datoscon.setCD_PROV(panSuca.getCD_PROV());
      datoscon.setCD_PAIS(panSuca.getCD_PAIS());
      datoscon.setCD_MUN(panSuca.getCD_MUN());

      if (txtDocumento.getText().trim().equals("")) {
        datoscon.setCD_TDOC("");
      }
      else {
        datoscon.setCD_TDOC(getCodTipoDocumento());

      }
      datoscon.setDS_APE1(txtApe1.getText().trim());
      datoscon.setDS_APE2(txtApe2.getText().trim());
      datoscon.setDS_NOMBRE(txtNombre.getText().trim());
      datoscon.setDS_FONOAPE1(Common.traduccionFonetica(txtApe1.getText().trim()));
      datoscon.setDS_FONOAPE2(Common.traduccionFonetica(txtApe2.getText().trim()));
      datoscon.setDS_FONONOMBRE(Common.traduccionFonetica(txtNombre.getText().
          trim()));
      if (lTipoFechaNac.getText().trim().equals("real")) {
        datoscon.setIT_CALC("N");
      }
      else {
        datoscon.setIT_CALC("S");

      }
      datoscon.setFC_NAC(Fechas.StringToSQLDate(txtFechaNac.getText().trim()));
      datoscon.setCD_SEXO(getCodSexo());

      datoscon.setCD_POSTAL(panSuca.getCD_POSTAL());
      datoscon.setDS_DIREC(panSuca.getDS_DIREC());
      datoscon.setDS_NUM(panSuca.getDS_NUM());
      datoscon.setDS_PISO(panSuca.getDS_PISO());
      datoscon.setDS_TELEF(txtTelefono.getText().trim());
      datoscon.setCD_NIVEL_1(txtCodNivel1.getText().trim());
      datoscon.setCD_NIVEL_2(txtCodNivel2.getText().trim());
      datoscon.setCD_ZBS(txtCodZBS.getText().trim());
      datoscon.setIT_REVISADO("N");

      datoscon.setDS_NDOC(txtDocumento.getText().trim());
      datoscon.setSIGLAS(Common.Calcular_Siglas(txtApe1.getText().trim(),
                                                txtApe2.getText().trim(),
                                                txtNombre.getText().trim()));

      datoscon.setCDVIAL(panSuca.getCDVIAL());
      datoscon.setCDTVIA(panSuca.getCDTVIA());
      datoscon.setCDTNUM(panSuca.getCDTNUM());
      datoscon.setDSCALNUM(panSuca.getDSCALNUM());
    }
    else { //modif de enfermo
      //meteremos un campo que indique si se ha modificado o no el enfermo
      /* if (Cambio_de_Ficha()){ //$$$$$$$$$$$$$$$lo que sea
       }*/

      //cdope y fcultact hay que modificarlo en datCasosTub del dialogo
      datoscon.setCD_ENFERMO(new Integer(txtCodEnfermo.getText().trim()).
                             intValue());
      datoscon.setDS_NDOC(txtDocumento.getText().trim());

      datoscon.setCD_PROV(panSuca.getCD_PROV());
      datoscon.setCD_PAIS(panSuca.getCD_PAIS());
      datoscon.setCD_MUN(panSuca.getCD_MUN());

      if (txtDocumento.getText().trim().equals("")) {
        datoscon.setCD_TDOC("");
      }
      else {
        datoscon.setCD_TDOC(getCodTipoDocumento());

      }
      if (lTipoFechaNac.getText().trim().equals("real")) {
        datoscon.setIT_CALC("N");
      }
      else {
        datoscon.setIT_CALC("S");

      }
      datoscon.setFC_NAC(Fechas.StringToSQLDate(txtFechaNac.getText().trim()));
      datoscon.setCD_SEXO(getCodSexo());

      datoscon.setCD_POSTAL(panSuca.getCD_POSTAL());
      datoscon.setDS_DIREC(panSuca.getDS_DIREC());
      datoscon.setDS_NUM(panSuca.getDS_NUM());
      datoscon.setDS_PISO(panSuca.getDS_PISO());
      datoscon.setDS_TELEF(txtTelefono.getText().trim());
      datoscon.setCD_NIVEL_1(txtCodNivel1.getText().trim());
      datoscon.setCD_NIVEL_2(txtCodNivel2.getText().trim());
      datoscon.setCD_ZBS(txtCodZBS.getText().trim());

      if (getFlagEnfermo()) { //true it=S lo ve todo
        datoscon.setDS_APE1(txtApe1.getText().trim());
        datoscon.setDS_APE2(txtApe2.getText().trim());
        datoscon.setDS_NOMBRE(txtNombre.getText().trim());
        datoscon.setDS_FONOAPE1(Common.traduccionFonetica(txtApe1.getText().
            trim()));
        datoscon.setDS_FONOAPE2(Common.traduccionFonetica(txtApe2.getText().
            trim()));
        datoscon.setDS_FONONOMBRE(Common.traduccionFonetica(txtNombre.getText().
            trim()));
        datoscon.setSIGLAS(Common.Calcular_Siglas(txtApe1.getText().trim(),
                                                  txtApe2.getText().trim(),
                                                  txtNombre.getText().trim()));
      }
      else {
        //si es confidencial, no puede tocar nada, ptt se le envia lo mismo que en datCasoTub de la select
      }

      datoscon.setCDVIAL(panSuca.getCDVIAL());
      datoscon.setCDTVIA(panSuca.getCDTVIA());
      datoscon.setCDTNUM(panSuca.getCDTNUM());
      datoscon.setDSCALNUM(panSuca.getDSCALNUM());

    }

    return (datoscon);
  }

  protected String getCodSexo() {
    int indice = choSexo.getSelectedIndex();

    if (indice > 0) {
      DataGeneralCDDS dat = (DataGeneralCDDS) listaSexo.elementAt(indice - 1);
      return (dat.getCD());
    }
    return "";
  }

  protected String getCodTipoDocumento() {
    int indice = choTipoDocumento.getSelectedIndex();

    if (indice > 0) {
      DataGeneralCDDS dat = (DataGeneralCDDS) listaTipoDocumento.elementAt(
          indice - 1);
      return (dat.getCD());
    }
    return "";
  }

//////////////////////                         //////////////////////
//////////////////////BOTONES + RELLENA               //////////////////////
/////////////////////                         //////////////////////

//ENFERMO*****  BOTON GRANDE *****
  public void btnEnfermo_actionPerformed() {

    int modo = modoOperacion;
    int iBuscar = 0;

    //Al menos que este Ape1 informado???-> salen todos los enfermos
    int iDatosSuficientes = 0;
    iDatosSuficientes = Ape1_Y_QuizasOtros();
    if (iDatosSuficientes == 1 || iDatosSuficientes == 2) {
      //continuamos
    }
    else {
      Common.ShowWarning(capp, "No existen datos para realizar la B�squeda");
      return;
    }
    //-------------------------------------------------------------

    //Estudio de Autorizaciones-> de momento false (TODO) DENTRO DE BUSCAR
    iBuscar = Buscar_Ficha(0);
    if (iBuscar == -1) { //No hay datos
      Common.ShowWarning(capp,
                         "No existen datos con los criterios seleccionados");
      modoOperacion = modo; //ALTA
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else if (iBuscar == 0) { //cancelar
      modoOperacion = modo; //ALTA
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else if (iBuscar == 1) { //aceptar
      //hasta aqui tenemos el cd_enfermo

      //-----------CASO COINCIDENTE-------------
      String sStringDespues = txtCodEnfermo.getText();
      int ibuscar = 0;
      ibuscar = Buscar_Registro();
      //repite busqueda y trae de nuevo datos de enfermo
      if (ibuscar == 0 || ibuscar == -1) {
        //paneles detras va a modo ALTA
        String n1antes = "";
        String n1desp = "";
        String n2antes = "";
        String n2desp = "";
        Paneles_Lupa(sStringBk, sStringDespues, n1antes, n1desp, n2antes,
                     n2desp); //llama a inicializar en alta

      }
      else if (ibuscar == 1) { //pasar a MODIFICAR
        //ya estan los paneles y en modificacion
      }

      bDeboLanzarEnfermo = false;
    } //ACEPTAR

    sStringBk = txtCodEnfermo.getText();
    //boton no habilit en modif, ptt no procede sN1bk..

  } //fin BOTON enfermo

//!!!!!  CASO !!!!!!!
//!!!!!!!!!!!!!!!!!!!
//CD_ENFERMO, CD_ENFERMEDAD*******
  public void btnCaso_actionPerformed() {
    int modo = modoOperacion;

    //Datos obligatorios***
    if (txtCodEnfermo.getText().trim().equals("")) {
      Common.ShowWarning(capp, "Enfermo debe estar informado");
      return;
    } //OBLIGATORIOS******

    //nota: los paneles estan abiertos, pero sin datos

    int iBuscar = 0;
    iBuscar = Buscar_Registro();
    if (iBuscar == -1) { //no hay datos
      Common.ShowWarning(capp,
                         "No existen datos con los criterios seleccionados");
      modoOperacion = modo; //ALTA
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else if (iBuscar == 0) { //cancelar
      modoOperacion = modo; //ALTA
      dlgTuber.Inicializar(modoOperacion); //mlm nuevo
    }
    else if (iBuscar == 1) { //acepta uno
      bDeboLanzarEnfermo = false;
      //abre paneles e Inicializa a Modificar
    }
    //no procede sN1Bk porque en modif  no se buscan casos

  } //FIN BOTON DE CASO

// m�todo que rellena los controles del panel con los datos
// de la estructura datatab ( a parti de SELECT*****)
//MODIFICACION-CONSULTA******
//BACKUP AL FINAL
  public void rellenaDatos(DatCasosTub datatab) {

    Hashtable hash;
    CLista clista;

    String sCD_TDOC = "", sDS_TDOC = "", sDS_NDOC = "";
    String sDS_APE2 = "", sDS_NOMBRE = "";
    String sCD_SEXO = "", sDS_SEXO = "";
    String sIT_CALC = "", sFC_NAC = "";
    String sCD_PROV_EDOIND = "", sDS_PROV_EDOIND = "", sCD_CA = "", sDS_CA = "";
    String sDS_TELEF = "", sCD_POSTAL_EDOIND = "";
    String sCD_MUN_EDOIND = "", sDS_MUN_EDOIND = "";
    String sCD_NIVEL_1_EDOIND = "", sDS_NIVEL_1_EDOIND = "";
    String sCD_NIVEL_2_EDOIND = "", sDS_NIVEL_2_EDOIND = "";
    String sCD_ZBS_EDOIND = "", sDS_ZBS_EDOIND = "";
    String sDS_CALLE = "", sDS_NMCALLE = "", sDS_PISO_EDOIND = "";
    String sFC_FECNOTIF = "";
    String sFC_RECEP = "";
    String sCD_ENFCIE = "";
    String sDS_PROCESO = "";
    String sCDVIAL = "";
    String sCDTVIA = "";
    String sCDTNUM = "";
    String sDSCALNUM = "";

    //cargamos el panel
    sFechaRecepcion = datatab.getFC_RECEP();
    sFechaNotificacion = datatab.getFC_FECNOTIF();
    CfechaNotif.setText(sFechaNotificacion);
    txtCodEnfermo.setText(datatab.getCD_ENFERMO());

    //documento *****
    if ( (datatab.getCD_TDOC()) != null) {
      sCD_TDOC = datatab.getCD_TDOC();
    }
    Common.selectChoiceElement(choTipoDocumento,
                               listaTipoDocumento,
                               true, sCD_TDOC);

    //visible
    if (choTipoDocumento.getSelectedIndex() != 0
        && choTipoDocumento.getSelectedIndex() != -1) {
      txtDocumento.setVisible(true);
      lDocumento.setVisible(true);
    }
    else {
      txtDocumento.setVisible(false);
      lDocumento.setVisible(false);
    } //----------------------------------
    this.doLayout();

    if ( (datatab.getDS_NDOC()) != null) {
      sDS_NDOC = datatab.getDS_NDOC();
    }
    txtDocumento.setText(sDS_NDOC);

    //confidencialidad
    if (!getFlagEnfermo()) { //confid
      Visible(false);

    }

    txtApe1.setText(datatab.getDS_APE1());

    if ( (datatab.getDS_APE2()) != null) {
      sDS_APE2 = datatab.getDS_APE2();
    }
    txtApe2.setText(sDS_APE2);
    if ( (datatab.getDS_NOMBRE()) != null) {
      sDS_NOMBRE = datatab.getDS_NOMBRE();
    }
    txtNombre.setText(sDS_NOMBRE);

    // SEXO *****
    if ( (datatab.getCD_SEXO()) != null) {
      sCD_SEXO = datatab.getCD_SEXO();
    }
    Common.selectChoiceElement(choSexo,
                               listaSexo,
                               true, sCD_SEXO);

    //FECHA NACIMIENTO - it-calc
    if ( (datatab.getIT_CALC()) != null) {
      sIT_CALC = datatab.getIT_CALC();
      //fecha
    }
    if ( (datatab.getFC_NAC()) != null) {
      sFC_NAC = datatab.getFC_NAC();

    }
    txtFechaNac.setText(sFC_NAC);
    FechaNac_desde_BD(sIT_CALC);
    //---------------------------------

    // Tel�fono  ******
    if ( (datatab.getDS_TELEF()) != null) {
      sDS_TELEF = datatab.getDS_TELEF();

    }
    txtTelefono.setText(sDS_TELEF);

    // Rellenando datos al panel SUCA  -------------------------------------
    //panSuca.setModoDeshabilitado();

    // pa�s: por ahora, SIEMPRE ESPA�A
    // Comunidad Aut�noma
    if ( (datatab.getCD_CA()) != null) {
      sCD_CA = datatab.getCD_CA();
    }
    setCA(sCD_CA);

    // s�lo se rellena la lista de provincias si la Comunidad
    // Aut�noma se encuentra rellena
    if (!sCD_CA.equals("")) {

      if ( (datatab.getCD_PROV_EDOIND()) != null) {
        sCD_PROV_EDOIND = datatab.getCD_PROV_EDOIND();
      }
      setProvincia(sCD_PROV_EDOIND);
    }

    // C�digo de Municipio
    if ( (datatab.getCD_MUN_EDOIND()) != null) {
      sCD_MUN_EDOIND = datatab.getCD_MUN_EDOIND();
    }
    panSuca.setCD_MUN(sCD_MUN_EDOIND);

    // Descripci�n de Municipio
    if ( (datatab.getDS_MUN()) != null) {
      sDS_MUN_EDOIND = datatab.getDS_MUN();
    }
    panSuca.setDS_MUN(sDS_MUN_EDOIND);

    // c�digo de vial
    if ( (datatab.getCDVIAL()) != null) {
      sCDVIAL = datatab.getCDVIAL();
    }
    panSuca.setCDVIAL(sCDVIAL);

    // tipo de via
    if ( (datatab.getCDTVIA()) != null) {
      sCDTVIA = datatab.getCDTVIA();
    }
    panSuca.setCDTVIA(sCDTVIA);

    // tipo de numeraci�n
    if ( (datatab.getCDTNUM()) != null) {
      sCDTNUM = datatab.getCDTNUM();
    }
    panSuca.setCDTNUM(sCDTNUM);

    // tipo de calificador
    if ( (datatab.getDSCALNUM()) != null) {
      sDSCALNUM = datatab.getDSCALNUM();
    }
    panSuca.setDSCALNUM(sDSCALNUM);

    // C�digo Postal
    if ( (datatab.getCD_POSTAL_EDOIND()) != null) {
      sCD_POSTAL_EDOIND = datatab.getCD_POSTAL_EDOIND();
    }
    panSuca.setCD_POSTAL(sCD_POSTAL_EDOIND);

    // calle
    if (datatab.getDS_CALLE() != null) {
      sDS_CALLE = datatab.getDS_CALLE();
    }
    panSuca.setDS_DIREC(sDS_CALLE);

    // n�mero
    if (datatab.getDS_NMCALLE() != null) {
      sDS_NMCALLE = datatab.getDS_NMCALLE();
    }
    panSuca.setDS_NUM(sDS_NMCALLE);

    // piso
    if (datatab.getDS_PISO_EDOIND() != null) {
      sDS_PISO_EDOIND = datatab.getDS_PISO_EDOIND();
    }
    panSuca.setDS_PISO(sDS_PISO_EDOIND);

    //--------------------------------------------------------------------------

    // c�digo de nivel1
    if ( (datatab.getCD_NIVEL_1_EDOIND()) != null) {
      sCD_NIVEL_1_EDOIND = datatab.getCD_NIVEL_1_EDOIND();
    }
    txtCodNivel1.setText(sCD_NIVEL_1_EDOIND);
    if (!sCD_NIVEL_1_EDOIND.equals("")) {
      bNivel1Valid = true;
      bNivel1SelDePopup = true;
    }

    // descripci�n de nivel1
    if ( (datatab.getDS_NIVEL_1_EDOIND()) != null) {
      sDS_NIVEL_1_EDOIND = datatab.getDS_NIVEL_1_EDOIND();
    }
    txtDesNivel1.setText(sDS_NIVEL_1_EDOIND);

    // c�digo de nivel2
    if ( (datatab.getCD_NIVEL_2_EDOIND()) != null) {
      sCD_NIVEL_2_EDOIND = datatab.getCD_NIVEL_2_EDOIND();
    }
    txtCodNivel2.setText(sCD_NIVEL_2_EDOIND);
    if (!sCD_NIVEL_2_EDOIND.equals("")) {
      bNivel2Valid = true;
      bNivel2SelDePopup = true;
    }
    // descripci�n de nivel2
    if ( (datatab.getDS_NIVEL_2_EDOIND()) != null) {
      sDS_NIVEL_2_EDOIND = datatab.getDS_NIVEL_2_EDOIND();
    }
    txtDesNivel2.setText(sDS_NIVEL_2_EDOIND);

    // c�digo de ZBS
    if ( (datatab.getCD_ZBS_EDOIND()) != null) {
      sCD_ZBS_EDOIND = datatab.getCD_ZBS_EDOIND();
    }
    txtCodZBS.setText(sCD_ZBS_EDOIND);
    if (!sCD_ZBS_EDOIND.equals("")) {
      bNivelZBSValid = true;
      bNivelZBSSelDePopup = true;
    }
    // descripci�n de ZBS
    if ( (datatab.getDS_ZBS_EDOIND()) != null) {
      sDS_ZBS_EDOIND = datatab.getDS_ZBS_EDOIND();
    }
    txtDesZBS.setText(sDS_ZBS_EDOIND);

    //BACKUPs
    sStringBk = datatab.getCD_ENFERMO(); //CodEnfermo Backup !!!!!!
    sN1Bk = sCD_NIVEL_1_EDOIND;
    sN2Bk = sCD_NIVEL_2_EDOIND;

    //confidencialidad -------------
    if (!getFlagEnfermo()) { //confid
      Confidencialidad();
    }
    else {
      Visible(true);
    } //---------------------------

  } //FIN RELLENA CABECERA

//PDP 10/02/2000
  public void rellenaDatosCon(DatConCasTub datatabcon) {
    Hashtable hash;
    CLista clista;

    String sCD_TDOC = "", sDS_TDOC = "", sDS_NDOC = "";
    String sDS_APE2 = "", sDS_NOMBRE = "";
    String sCD_SEXO = "", sDS_SEXO = "";
    String sIT_CALC = "", sFC_NAC = "";
    String sCD_PROV = "", sDS_PROV = "", sCD_CA = "", sDS_CA = "";
    String sDS_TELEF = "", sCD_POSTAL = "";
    String sCD_MUN = "", sDS_MUN = "";
    String sCD_NIVEL_1 = "", sDS_NIVEL_1 = "";
    String sCD_NIVEL_2 = "", sDS_NIVEL_2 = "";
    String sCD_ZBS = "", sDS_ZBS = "";
    String sDS_CALLE = "", sDS_NMCALLE = "", sDS_PISO = "";
    //String sFC_FECNOTIF = "";
    //String sFC_RECEP = "";
    //String sCD_ENFCIE = "";
    //String sDS_PROCESO = "";
    String sCDVIAL = "";
    String sCDTVIA = "";
    String sCDTNUM = "";
    String sDSCALNUM = "";

    //cargamos el panel
    //sFechaRecepcion = datatabcon.getFC_RECEP();
    //sFechaNotificacion =  datatab.getFC_FECNOTIF();
    //CfechaNotif.setText(sFechaNotificacion);
    txtCodEnfermo.setText(String.valueOf(datatabcon.getCD_ENFERMO()));

    //documento *****
    if ( (datatabcon.getCD_TDOC()) != null) {
      sCD_TDOC = datatabcon.getCD_TDOC();
    }
    Common.selectChoiceElement(choTipoDocumento,
                               listaTipoDocumento,
                               true, sCD_TDOC);

    //visible
    if (choTipoDocumento.getSelectedIndex() != 0
        && choTipoDocumento.getSelectedIndex() != -1) {
      txtDocumento.setVisible(true);
      lDocumento.setVisible(true);
    }
    else {
      txtDocumento.setVisible(false);
      lDocumento.setVisible(false);
    } //----------------------------------
    this.doLayout();

    if ( (datatabcon.getDS_NDOC()) != null) {
      sDS_NDOC = datatabcon.getDS_NDOC();
    }
    txtDocumento.setText(sDS_NDOC);

    //confidencialidad
    if (!getFlagEnfermo()) { //confid
      Visible(false);

    }

    txtApe1.setText(datatabcon.getDS_APE1());

    if ( (datatabcon.getDS_APE2()) != null) {
      sDS_APE2 = datatabcon.getDS_APE2();
    }
    txtApe2.setText(sDS_APE2);
    if ( (datatabcon.getDS_NOMBRE()) != null) {
      sDS_NOMBRE = datatabcon.getDS_NOMBRE();
    }
    txtNombre.setText(sDS_NOMBRE);

    // SEXO *****
    if ( (datatabcon.getCD_SEXO()) != null) {
      sCD_SEXO = datatabcon.getCD_SEXO();
    }
    Common.selectChoiceElement(choSexo,
                               listaSexo,
                               true, sCD_SEXO);

    //FECHA NACIMIENTO - it-calc
    if ( (datatabcon.getIT_CALC()) != null) {
      sIT_CALC = datatabcon.getIT_CALC();
      //fecha
    }
    if ( (datatabcon.getFC_NAC()) != null) {
      sFC_NAC = Fechas.date2String(datatabcon.getFC_NAC());

    }
    txtFechaNac.setText(sFC_NAC);
    FechaNac_desde_BD(sIT_CALC);
    //---------------------------------

    // Tel�fono  ******
    if ( (datatabcon.getDS_TELEF()) != null) {
      sDS_TELEF = datatabcon.getDS_TELEF();

    }
    txtTelefono.setText(sDS_TELEF);

    // Rellenando datos al panel SUCA  -------------------------------------
    //panSuca.setModoDeshabilitado();

    // pa�s: por ahora, SIEMPRE ESPA�A
    // Comunidad Aut�noma
    /*
         if ((datatabcon.getCD_PROV()) != null)
      sCD_CA = datatabcon.getCD_CA();
         setCA(sCD_CA);
     */
    // s�lo se rellena la lista de provincias si la Comunidad
    // Aut�noma se encuentra rellena
    if (!sCD_CA.equals("")) {

      if ( (datatabcon.getCD_PROV()) != null) {
        sCD_PROV = datatabcon.getCD_PROV();
      }
      setProvincia(sCD_PROV);
    }

    // C�digo de Municipio
    if ( (datatabcon.getCD_MUN()) != null) {
      sCD_MUN = datatabcon.getCD_MUN();
    }
    panSuca.setCD_MUN(sCD_MUN);

    // Descripci�n de Municipio
    if ( (datatabcon.getDS_MUN()) != null) {
      sDS_MUN = datatabcon.getDS_MUN();
    }
    panSuca.setDS_MUN(sDS_MUN);

    // c�digo de vial
    if ( (datatabcon.getCDVIAL()) != null) {
      sCDVIAL = datatabcon.getCDVIAL();
    }
    panSuca.setCDVIAL(sCDVIAL);

    // tipo de via
    if ( (datatabcon.getCDTVIA()) != null) {
      sCDTVIA = datatabcon.getCDTVIA();
    }
    panSuca.setCDTVIA(sCDTVIA);

    // tipo de numeraci�n
    if ( (datatabcon.getCDTNUM()) != null) {
      sCDTNUM = datatabcon.getCDTNUM();
    }
    panSuca.setCDTNUM(sCDTNUM);

    // tipo de calificador
    if ( (datatabcon.getDSCALNUM()) != null) {
      sDSCALNUM = datatabcon.getDSCALNUM();
    }
    panSuca.setDSCALNUM(sDSCALNUM);

    // C�digo Postal
    if ( (datatabcon.getCD_POSTAL()) != null) {
      sCD_POSTAL = datatabcon.getCD_POSTAL();
    }
    panSuca.setCD_POSTAL(sCD_POSTAL);

    // calle
    if (datatabcon.getDS_DIREC() != null) {
      sDS_CALLE = datatabcon.getDS_DIREC();
    }
    panSuca.setDS_DIREC(sDS_CALLE);

    // n�mero
    if (datatabcon.getDS_NUM() != null) {
      sDS_NMCALLE = datatabcon.getDS_NUM();
    }
    panSuca.setDS_NUM(sDS_NMCALLE);

    // piso
    if (datatabcon.getDS_PISO() != null) {
      sDS_PISO = datatabcon.getDS_PISO();
    }
    panSuca.setDS_PISO(sDS_PISO);

    //--------------------------------------------------------------------------

    // c�digo de nivel1
    if ( (datatabcon.getCD_NIVEL_1()) != null) {
      sCD_NIVEL_1 = datatabcon.getCD_NIVEL_1();
    }
    txtCodNivel1.setText(sCD_NIVEL_1);
    if (!sCD_NIVEL_1.equals("")) {
      bNivel1Valid = true;
      bNivel1SelDePopup = true;
    }

    // descripci�n de nivel1
    if ( (datatabcon.getDS_NIVEL_1()) != null) {
      sDS_NIVEL_1 = datatabcon.getDS_NIVEL_1();
    }
    txtDesNivel1.setText(sDS_NIVEL_1);

    // c�digo de nivel2
    if ( (datatabcon.getCD_NIVEL_2()) != null) {
      sCD_NIVEL_2 = datatabcon.getCD_NIVEL_2();
    }
    txtCodNivel2.setText(sCD_NIVEL_2);
    if (!sCD_NIVEL_2.equals("")) {
      bNivel2Valid = true;
      bNivel2SelDePopup = true;
    }

    // descripci�n de nivel2
    if ( (datatabcon.getDS_NIVEL_2()) != null) {
      sDS_NIVEL_2 = datatabcon.getDS_NIVEL_2();
    }
    txtDesNivel2.setText(sDS_NIVEL_2);

    // c�digo de ZBS
    if ( (datatabcon.getCD_ZBS()) != null) {
      sCD_ZBS = datatabcon.getCD_ZBS();
    }
    txtCodZBS.setText(sCD_ZBS);
    if (!sCD_ZBS.equals("")) {
      bNivelZBSValid = true;
      bNivelZBSSelDePopup = true;
    }

    // descripci�n de ZBS
    if ( (datatabcon.getDS_ZBS()) != null) {
      sDS_ZBS = datatabcon.getDS_ZBS();
    }
    txtDesZBS.setText(sDS_ZBS);

    //BACKUPs
    sStringBk = String.valueOf(datatabcon.getCD_ENFERMO()); //CodEnfermo Backup !!!!!!
    sN1Bk = sCD_NIVEL_1;
    sN2Bk = sCD_NIVEL_2;

    //confidencialidad -------------
    if (!getFlagEnfermo()) { //confid
      Confidencialidad();
    }
    else {
      Visible(true);
    } //---------------------------

  } //FIN RELLENA CABECERA

/////////////////////////////////////////

  private boolean Cambio_de_Ficha() { //CAMBIO DE FICHA

    boolean bReturn = false;
    //hs era clone de la del dialogo pero mejor leemos del datcasostub

    //si era solo contacto, ahora es tb enfermo, ptt hay que modificar
    if (bEnfermo) { //PDP 10/02/2000
      if (dlgTuber.datcasostub.getIT_ENFERMO().trim().equals("N")) {
        return (true); //hemos cambiado la ficha
      }
    }
    else {
      if (dlgContacto.datconcastub.getIT_ENFERMO().trim().equals("N")) {
        return (true); //hemos cambiado la ficha
      }
    }

    if (bEnfermo) {
      //enfermo normal : no confindencial
      if (this.getFlagEnfermo()) {

        if (!dlgTuber.datcasostub.getDS_APE1().trim().equals(txtApe1.getText().
            trim())
            ||
            !dlgTuber.datcasostub.getDS_APE2().trim().equals(txtApe2.getText().trim())
            ||
            !dlgTuber.datcasostub.getDS_NOMBRE().trim().equals(txtNombre.getText().
            trim())
            ||
            !dlgTuber.datcasostub.getFC_NAC().trim().equals(txtFechaNac.getText().
            trim())
            ||
            !dlgTuber.datcasostub.getCD_SEXO().trim().equals(getCodSexo().trim())
            ||
            !dlgTuber.datcasostub.getDS_TELEF().trim().equals(txtTelefono.getText().
            trim())
            ||
            !dlgTuber.datcasostub.getDS_NDOC().trim().equals(txtDocumento.getText().
            trim())
            ||
            !dlgTuber.datcasostub.getCD_TDOC().trim().equals(getCodTipoDocumento().
            trim())
            ) {
          bReturn = true; //hemos cambiado enfermo
        }
        else {
          bReturn = false; //NO hemos cambiado enfermo

        }
      }
      //enfermo confidencial
      else {
        if (!dlgTuber.datcasostub.getFC_NAC().trim().equals(txtFechaNac.getText().
            trim())
            ||
            !dlgTuber.datcasostub.getCD_SEXO().trim().equals(getCodSexo().trim())
            ||
            !dlgTuber.datcasostub.getDS_TELEF().trim().equals(txtTelefono.getText().
            trim())
            ||
            !dlgTuber.datcasostub.getDS_NDOC().trim().equals(txtDocumento.getText().
            trim())
            ||
            !dlgTuber.datcasostub.getCD_TDOC().trim().equals(getCodTipoDocumento().
            trim())
            ) {
          bReturn = true; //hemos cambiado enfermo
        }
        else {
          bReturn = false; //NO hemos cambiado enfermo
        }
      }
    }
    else {
      //contacto normal : no confindencial  //PDP 10/02/2000
      if (this.getFlagEnfermo()) {

        if (!dlgContacto.datconcastub.getDS_APE1().trim().equals(txtApe1.
            getText().trim())
            ||
            !dlgContacto.datconcastub.getDS_APE2().trim().equals(txtApe2.getText().
            trim())
            ||
            !dlgContacto.datconcastub.getDS_NOMBRE().trim().equals(txtNombre.getText().
            trim())
            ||
            !dlgContacto.datconcastub.getFC_NAC().equals(Fechas.
            StringToSQLDate(txtFechaNac.getText().trim()))
            ||
            !dlgContacto.datconcastub.getCD_SEXO().trim().equals(getCodSexo().trim())
            ||
            !dlgContacto.datconcastub.getDS_TELEF().trim().equals(txtTelefono.
            getText().trim())
            ||
            !dlgContacto.datconcastub.getDS_NDOC().trim().equals(txtDocumento.
            getText().trim())
            ||
            !dlgContacto.datconcastub.getCD_TDOC().trim().equals(getCodTipoDocumento().
            trim())
            ) {
          bReturn = true; //hemos cambiado enfermo
        }
        else {
          bReturn = false; //NO hemos cambiado enfermo

        }
      }
      //enfermo confidencial
      else {
        if (!dlgContacto.datconcastub.getFC_NAC().equals(Fechas.StringToSQLDate(
            txtFechaNac.getText().trim()))
            ||
            !dlgContacto.datconcastub.getCD_SEXO().trim().equals(getCodSexo().trim())
            ||
            !dlgContacto.datconcastub.getDS_TELEF().trim().equals(txtTelefono.
            getText().trim())
            ||
            !dlgContacto.datconcastub.getDS_NDOC().trim().equals(txtDocumento.
            getText().trim())
            ||
            !dlgContacto.datconcastub.getCD_TDOC().trim().equals(getCodTipoDocumento().
            trim())
            ) {
          bReturn = true; //hemos cambiado enfermo
        }
        else {
          bReturn = false; //NO hemos cambiado enfermo
        }
      }
    }
    return (bReturn);

  } //fin funcion

//COD LOSTFOCUS
  void txtCodEnfermo_focusLost() {
    String sStringDespues = txtCodEnfermo.getText();

    if (sStringBk.equals(sStringDespues)) {
      return;
    }

    //si difiere del anterior  ***************
    CMessage msgBox;

    //ahora VACIO--> el de antes
    if (txtCodEnfermo.getText().trim().equals("")) {
      int modo = modoOperacion;
      if (bEnfermo) { //10/02/2000
        dlgTuber.Inicializar(modoESPERA); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoESPERA); //mlm nuevo
      }
      Reset_Cabecera();
      modoOperacion = modo;
      if (bEnfermo) { //10/02/2000
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }
      bDeboLanzarEnfermo = true; //NOTA!! comenzar
      sStringBk = txtCodEnfermo.getText().trim();

      return;
    } //*********************

    //COMPROBAR NUMERICO ****
    boolean bEnf = true;
    try {
      Integer iCodEnfermo = new Integer(txtCodEnfermo.getText());
      if (iCodEnfermo.intValue() < 0) {
        bEnf = false;
      }
      if (iCodEnfermo.intValue() > 999999) {
        bEnf = false;
      }
    }
    catch (Exception e) {
      bEnf = false;
    }

    if (!bEnf) {
      msgBox = new CMessage(this.app,
                            CMessage.msgAVISO, "C�digo de Enfermo no v�lido");
      msgBox.show();
      msgBox = null;
      txtCodEnfermo.selectAll();
      txtCodEnfermo.setText(sStringBk);
      return;
    }
    //****************************

     int modo = modoOperacion; //lo guardo
    modoOperacion = modoESPERA; //general
    if (bEnfermo) { //PDP 10/02/2000
      dlgTuber.Inicializar(modoOperacion); //nuevo
    }
    else {
      dlgContacto.Inicializar(modoOperacion); //nuevo
    }
    CLista data = new CLista();
    DataEnfermo hash = new DataEnfermo("CD_ENFERMO");
    hash.put("CD_ENFERMO", txtCodEnfermo.getText());

    data.addElement(hash);
    data.addElement(bEnfermo ? constantes.sSoloEnfermos :
                    constantes.sSoloContactos);

    try {
//System.out.println(">>>"+data);
      //segun tramero se llama a uno u otro
      if (app.getIT_TRAMERO().equals("N")) {
//SrvEnfermo srv = new SrvEnfermo();
//listaEnfermo = srv.doPrueba(modoDATOSENFERMO, data);
//srv = null;

        listaEnfermo = Comunicador.Communicate(this.getApp(),
                                               stubCliente,
                                               modoDATOSENFERMO,
                                               strSERVLET_ENFERMO,
                                               data);
      }
      else {

//SrvEnfermo srv = new SrvEnfermo();
//listaEnfermo = srv.doPrueba(modoDATOSENFERMOTRAMERO, data);
//srv = null;
        listaEnfermo = Comunicador.Communicate(this.getApp(),
                                               stubCliente,
                                               modoDATOSENFERMOTRAMERO,
                                               strSERVLET_ENFERMO,
                                               data);
      } //-------------------------------

      if (listaEnfermo != null) {
        //No existe el enfermo
        if (listaEnfermo.size() == 0) {
          Common.ShowWarning(capp, "No existe ese c�digo de enfermo");
          //sin dato o sin permiso
          if (txtApe1.getText().trim().equals("")
              || !bDeboLanzarEnfermo) {
            txtCodEnfermo.setText(sStringBk);
            modoOperacion = modo;
            if (bEnfermo) { // PDP 10/02/2000
              dlgTuber.Inicializar(modoOperacion); //nuevo
            }
            else {
              dlgContacto.Inicializar(modoOperacion); //nuevo
            }
            return;
          }
          if (!txtApe1.getText().trim().equals("")) {
            //Estoy en espera, con datos suficientes y con permiso de lanzar Automatismo
            int iBuscar = 0;
            iBuscar = Buscar_Ficha(0);

            if (iBuscar == -1) { //No hay datos
              txtCodEnfermo.setText(sStringBk);
              modoOperacion = modo; //ALTA
              if (bEnfermo) { // PDP 10/02/2000
                dlgTuber.Inicializar(modoOperacion); //nuevo
              }
              else {
                dlgContacto.Inicializar(modoOperacion); //nuevo
              }
              return;
            }
            else if (iBuscar == 0) { //cancelar
              txtCodEnfermo.setText(sStringBk);
              modoOperacion = modo; //ALTA
              if (bEnfermo) { // PDP 10/02/2000
                dlgTuber.Inicializar(modoOperacion); //nuevo
              }
              else {
                dlgContacto.Inicializar(modoOperacion); //nuevo
              }
              return;
            }
            else if (iBuscar == 1) { //aceptar
              bDeboLanzarEnfermo = false;
              //que continue con a parte de caso
            }
          } //-------

        } //fin No existe enfermo
        //Existe el enfermo
        else {
          bDeboLanzarEnfermo = false;
          Pinta_Actualiza_DatosEnfermo(listaEnfermo); //No se ha hecho backup del enfermo
        }

        //----------- CASO COINCIDENTE -------------
        sStringDespues = txtCodEnfermo.getText(); //por si se vuelve a llenar
        int ibuscar = 0;
        if (bEnfermo) { // PDP 10/02/2000
          ibuscar = Buscar_Registro();

          if (ibuscar == 0 || ibuscar == -1) {
            //paneles --- llaman a inicializar (ALTA (pCabecera))
            String n1antes = "";
            String n1desp = "";
            String n2antes = "";
            String n2desp = "";
            Paneles_Lupa(sStringBk, sStringDespues, n1antes, n1desp, n2antes,
                         n2desp); //llama a inicializar en alta

          }
          else if (ibuscar == 1) { //acepta caso
            //ya estan los paneles y en modificacion
          }

        }
        //---------------------------------------

      }
      else if (listaEnfermo == null) { //no debe darse
        txtCodEnfermo.setText(sStringBk);
        modoOperacion = modo;
        if (bEnfermo) { //PDP 10/02/2000
          dlgTuber.Inicializar(modoOperacion); //mlm nuevo
        }
        else {
          dlgContacto.Inicializar(modoOperacion); //mlm nuevo
        }
        return;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      txtCodEnfermo.setText(sStringBk);
      modoOperacion = modo;
      if (bEnfermo) { //PDP 10/02/2000
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }
    }

    //BACKUPS ****
    sStringBk = txtCodEnfermo.getText().trim();
    //en modificacion no se puede hacer lostfocus de cod enfermo, ptt no sN1Bk,sN2Bk

    if (!bEnfermo) {
      modoOperacion = modo;
      //dlgContacto.Inicializar(modoOperacion);//mlm nuevo
      // modificaci�n 01/03/2001
      dlgContacto.Inicializar(modoMODIFICACION);
    }
  } //FIN LOST COD ENFERMO

  void Parche_Protocolo() {

    int modo = modoOperacion;

    //PARCHE: protocolo -----
    if (!txtCodNivel1.getText().trim().equals(sN1Bk) ||
        !txtCodNivel2.getText().trim().equals(sN2Bk)) {

      if (modo == modoMODIFICACION &&
          !dlgTuber.YaSalioMsgEnferSinProtocolo) {

        dlgTuber.borrarProtocolo();
        dlgTuber.borrarOtros(); //PDP  07/03/2000
        dlgTuber.Inicializar(modoESPERA);
        dlgTuber.getBarra().setText("Cargando Protocolo...");
        dlgTuber.addProtocolo();
        dlgTuber.addOtros(); //PDP  07/03/2000
        dlgTuber.Inicializar(modo);
        dlgTuber.getBarra().setText("");
      }
      else if (modo == modoALTA
               && dlgTuber.pProtocolo != null) {
        dlgTuber.borrarProtocolo();
        dlgTuber.Inicializar(modoESPERA);
        dlgTuber.getBarra().setText("Cargando Protocolo...");
        dlgTuber.addProtocolo();
        dlgTuber.Inicializar(modo);
        dlgTuber.getBarra().setText("");

      }
      sN1Bk = txtCodNivel1.getText().trim();
      sN2Bk = txtCodNivel2.getText().trim();
    }
    //-----------------------
  }

//CONTROL DE PANELES----------------------
  /*protected void Paneles_txtCodEnfermo_focuslost
   (String sCodBk, String sCodDespues){
            //vacio--> lleno
            if (sCodBk.equals("") &&
                !sCodDespues.equals("")){
             dlgTuber.Inicializar(modoESPERA);
             dlgTuber.addCaso();
             dlgTuber.Inicializar(modoESPERA);
             dlgTuber.addProtocolo();  //ALTA
             dlgTuber.Inicializar(modoALTA);
            }//fin
            //lleno--> lleno y diferentes
            if (!sCodBk.equals(sCodDespues) &&
                !sCodBk.equals("") &&
                !sCodDespues.equals("")){
                if (dlgTuber.panCaso != null)
                  dlgTuber.borrarCaso();
                if (dlgTuber.pProtocolo != null )
                  dlgTuber.borrarProtocolo();
                 dlgTuber.Inicializar(modoESPERA);
                 dlgTuber.addCaso();
                 dlgTuber.Inicializar(modoESPERA);
                 dlgTuber.addProtocolo();  //ALTA
                 dlgTuber.Inicializar(modoALTA);
            }//fin de enfermo distintos -informados
            //----------------------------------------
   }//fin funcion */

  protected int Buscar_Ficha(int iTipo) {
//llamada normal 0 (data: todo);
//llamada desde documento: 1 (data: tipo, txtDocumento)
//llamada desde via: 2 (data: ape1, ape2, nombre, via, mun, prov)
//llamada desde fechanac 3: (data: ape1, ape2, nombre, fechanac)

//0:	sale por cancelar No selecciono ficha
//-1:	No hay datos, sacar mensaje si desde boton
//1:	acepta uno
//Entra en espera, pero no Inicializa al salir!!! (la salida siempre es ALTA)

    //System.out.println("entro en buscar_ficha");

    int iSalida = 0;
    if (getApp().getPerfil() == 5) {
      return (iSalida);
    }

    int modo = modoOperacion;

    if (modoOperacion != modoESPERA) {
      modoOperacion = modoESPERA;
      if (bEnfermo) { //PDP 08/02/2000
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion);
      }

    }

    //CONFIDENCIALIDAD --------------------------------------------
    //AUTORIZACIONES SOBRE EL DIALOGO: control (data: bFlag_Siglas)
    //si no esta autorizado, enviar en el data-> bFlag_Siglas =true
    boolean bFlag_Siglas = false;
    if (!getFlagEnfermo()) { //confid
      bFlag_Siglas = true;
    }
    //-------------------------------------------------------------

    DataValoresEDO dataVal = null;
    switch (iTipo) {
      case 0: // suca
        dataVal = new DataValoresEDO(txtApe1.getText().trim(),
                                     txtApe2.getText().trim(),
                                     txtNombre.getText().trim(),
                                     panSuca.getCD_MUN(),
                                     panSuca.getCD_PROV(),
                                     panSuca.getDS_DIREC(),
                                     txtFechaNac.getText().trim(),
                                     getCodSexo().trim(),
                                     getCodTipoDocumento().trim(),
                                     txtDocumento.getText().trim(),
                                     bFlag_Siglas);

        break;
      case 1:

        //String Tdoc = new String();
        //String Ndoc = new String();
        //Tdoc = getCodTipoDocumento().trim();
        //Ndoc = txtDocumento.getText().trim();
        //System.out.println("TIPOdocenbuscar" + Tdoc);
        //System.out.println("Ndocenbuscar" + Ndoc);

        dataVal = new DataValoresEDO("",
                                     "",
                                     "",
                                     "",
                                     "",
                                     "",
                                     "",
                                     "",
                                     getCodTipoDocumento().trim(),
                                     txtDocumento.getText().trim(),
                                     bFlag_Siglas);
        break;
      case 2: //ape1, ape2, nombre, via, mun, prov) // suca

        if (panSuca.getCDVIAL().trim().equals("")) {
          dataVal = new DataValoresEDO(txtApe1.getText().trim(),
                                       txtApe2.getText().trim(),
                                       txtNombre.getText().trim(),
                                       panSuca.getCD_MUN(),
                                       panSuca.getCD_PROV(),
                                       panSuca.getDS_DIREC(),
                                       "",
                                       "",
                                       "",
                                       "",
                                       bFlag_Siglas);
        }
        else {
          dataVal = new DataValoresEDO(txtApe1.getText().trim(),
                                       txtApe2.getText().trim(),
                                       txtNombre.getText().trim(),
                                       panSuca.getCD_MUN(),
                                       panSuca.getCD_PROV(),
                                       "",
                                       "",
                                       "",
                                       "",
                                       "",
                                       bFlag_Siglas);
          //para buscar por... ira informado
          dataVal.CDVIAL = panSuca.getCDVIAL();
        }
        break;
      case 3:
        dataVal = new DataValoresEDO(txtApe1.getText().trim(),
                                     txtApe2.getText().trim(),
                                     txtNombre.getText().trim(),
                                     "",
                                     "",
                                     "",
                                     txtFechaNac.getText().trim(),
                                     "",
                                     "",
                                     "",
                                     bFlag_Siglas);
        break;

    }

    if (bEnfermo) { // PDP 08/02/2000
      dlgTuber.getBarra().setText(cadenaBarra(iTipo));
    }
    else {
      dlgContacto.getBarra().setText(cadenaBarra(iTipo));
      dlgContacto.Inicializar(modoOperacion);
    }

    DialogSelEnfermo dlg = new DialogSelEnfermo(this.getApp(),
                                                dataVal);

// hay datos --> lo muestro
    if (dlg.hayResultados()) {
      //System.out.println("hay datos");
      if (bEnfermo) { // 08/02/2000
        dlgTuber.getBarra().setText("");
      }
      else {
        dlgContacto.getBarra().setText("");
      }
      // Repintamos, porque a veces falla. (ARS 16-05-01)
      dlg.repaint();
      dlg.show();

      //pulso OK!!!!
      //!!!!!!!!!!!!
      if (dlg.BotonSalida()) {

        iSalida = 1;
        txtCodEnfermo.setText(dlg.Resultados.sCodEnfermo);

        //confidencialidad
        if (!getFlagEnfermo()) { //confid
          Visible(false);

        }
        txtApe1.setText(dlg.Resultados.sApe1);
        txtApe2.setText(dlg.Resultados.sApe2);
        txtNombre.setText(dlg.Resultados.sNombre);

        //--------------------------------------
        //setTipoDocumento(dlg.Resultados.sTDoc);
        //documento *****
        String sCD_TDOC = "";
        if ( (dlg.Resultados.sTDoc) != null) {
          sCD_TDOC = dlg.Resultados.sTDoc;
        }
        if (!sCD_TDOC.equals("")) {
          Common.selectChoiceElement(choTipoDocumento,
                                     listaTipoDocumento,
                                     true, sCD_TDOC);

          //documento---
        }
        txtDocumento.setText(dlg.Resultados.sNDoc);

        if (choTipoDocumento.getSelectedIndex() != 0
            && choTipoDocumento.getSelectedIndex() != -1) {
          txtDocumento.setVisible(true);
          lDocumento.setVisible(true);
        }
        else {
          txtDocumento.setVisible(false);
          lDocumento.setVisible(false);
        }
        this.doLayout();
        //---------

        //setSexo(dlg.Resultados.sCodSexo);
        // SEXO *****
        String sCD_SEXO = "";
        if ( (dlg.Resultados.sCodSexo) != null) {
          sCD_SEXO = dlg.Resultados.sCodSexo;
        }
        if (!sCD_SEXO.equals("")) {
          Common.selectChoiceElement(choSexo,
                                     listaSexo,
                                     true, sCD_SEXO);

          //NECESITO SABER SI ES CALCULADA  ---
          //sFechaNac viene vacia o informada. Nunca null.
        }
        txtFechaNac.setText(dlg.Resultados.sFechaNac);
        FechaNac_desde_BD(dlg.Resultados.sItCalc);
        //------

        txtTelefono.setText(dlg.Resultados.sTelefono);

        //SUCA --------------------------------------
        //panSuca.setModoDeshabilitado();

        //pais espa�a
        //ca
        //(se llena la lista y el hash??)
        setCA(dlg.Resultados.sCodCA);
        if (!dlg.Resultados.sCodCA.equals("")) {
          setProvincia(dlg.Resultados.sCodProv);

        }
        panSuca.setCD_MUN(dlg.Resultados.sCodMun);
        panSuca.setDS_MUN(dlg.Resultados.sDesMun);

        panSuca.setCDTVIA(dlg.Resultados.CDTVIA);
        panSuca.setCDVIAL(dlg.Resultados.CDVIAL);
        panSuca.setCDTNUM(dlg.Resultados.CDTNUM);
        panSuca.setDSCALNUM(dlg.Resultados.DSCALNUM);

        panSuca.setDS_DIREC(dlg.Resultados.sDesCalle);
        panSuca.setCD_POSTAL(dlg.Resultados.sCPostal);
        panSuca.setDS_PISO(dlg.Resultados.sPiso);
        panSuca.setDS_NUM(dlg.Resultados.sNumero);

        txtCodNivel1.setText(dlg.Resultados.sCodNivel1);
        txtDesNivel1.setText(dlg.Resultados.sDesNivel1);
        if (!txtDesNivel1.getText().trim().equals("")) {
          bNivel1Valid = true;
          bNivel1SelDePopup = true;
        }
        txtCodNivel2.setText(dlg.Resultados.sCodNivel2);
        txtDesNivel2.setText(dlg.Resultados.sDesNivel2);
        if (!txtDesNivel2.getText().trim().equals("")) {
          bNivel2Valid = true;
          bNivel2SelDePopup = true;
        }
        txtCodZBS.setText(dlg.Resultados.sCodZBS);
        txtDesZBS.setText(dlg.Resultados.sDesZBS);
        if (!txtDesZBS.getText().trim().equals("")) {
          bNivelZBSValid = true;
          bNivelZBSSelDePopup = true;
        }

        //cargamos la estructura de datos del enfermo
        String sA1 = "", sFonoA1 = "";
        String sA2 = "", sFonoA2 = "";
        String sN = "", sFonoN = "";
        sA1 = dlg.Resultados.sApe1;
        sA2 = dlg.Resultados.sApe2;
        sN = dlg.Resultados.sNombre;
        sFonoA1 = Common.traduccionFonetica(sA1);
        sFonoA2 = Common.traduccionFonetica(sA2);
        sFonoN = Common.traduccionFonetica(sN);

        /*  dataEnfermoBD = new DataEnfermoBD (
               dlg.Resultados.sCodEnfermo,dlg.Resultados.sTDoc,
               sA1, sA2, sN, sFonoA1, sFonoA2, sFonoN,
               dlg.Resultados.sItCalc, dlg.Resultados.sFechaNac,
               dlg.Resultados.sCodSexo, dlg.Resultados.sTelefono,
               dlg.Resultados.sCodNivel1, dlg.Resultados.sCodNivel2,
               dlg.Resultados.sCodZBS, "",
               "", dlg.Resultados.sNDoc,
               dlg.Resultados.sSiglas); */
        //****************
        if (bEnfermo) { //PDP 09/02/2000
          dlgTuber.datcasostub.setCD_ENFERMO(dlg.Resultados.sCodEnfermo);
          dlgTuber.datcasostub.setCD_TDOC(dlg.Resultados.sTDoc);
          dlgTuber.datcasostub.setDS_APE1(sA1);
          dlgTuber.datcasostub.setDS_APE2(sA2);
          dlgTuber.datcasostub.setDS_NOMBRE(sN);
          dlgTuber.datcasostub.setDS_FONOAPE1(sFonoA1);
          dlgTuber.datcasostub.setDS_FONOAPE2(sFonoA2);
          dlgTuber.datcasostub.setDS_FONONOMBRE(sFonoN);
          dlgTuber.datcasostub.setIT_CALC(dlg.Resultados.sItCalc);
          dlgTuber.datcasostub.setFC_NAC(dlg.Resultados.sFechaNac);
          dlgTuber.datcasostub.setCD_SEXO(dlg.Resultados.sCodSexo);
          dlgTuber.datcasostub.setDS_TELEF(dlg.Resultados.sTelefono);
          dlgTuber.datcasostub.setCD_NIVEL_1(dlg.Resultados.sCodNivel1);
          dlgTuber.datcasostub.setCD_NIVEL_2(dlg.Resultados.sCodNivel2);
          dlgTuber.datcasostub.setCD_ZBS(dlg.Resultados.sCodZBS);
          dlgTuber.datcasostub.setCD_OPE(dlg.Resultados.CD_OPE);
          dlgTuber.datcasostub.setFC_ULTACT(dlg.Resultados.FC_ULTACT);
          dlgTuber.datcasostub.setDS_NDOC(dlg.Resultados.sNDoc);
          dlgTuber.datcasostub.setSIGLAS(dlg.Resultados.sSiglas);
          //flags de it_contacto y de it_enfermo
          dlgTuber.datcasostub.setIT_ENFERMO(dlg.Resultados.IT_ENFERMO);
          dlgTuber.datcasostub.setIT_CONTACTO(dlg.Resultados.IT_CONTACTO);
          //**********************
        }
        else {
          dlgContacto.datconcastub.setCD_ENFERMO(new Integer(dlg.Resultados.
              sCodEnfermo).intValue());
          dlgContacto.datconcastub.setCD_TDOC(dlg.Resultados.sTDoc);
          dlgContacto.datconcastub.setDS_APE1(sA1);
          dlgContacto.datconcastub.setDS_APE2(sA2);
          dlgContacto.datconcastub.setDS_NOMBRE(sN);
          dlgContacto.datconcastub.setDS_FONOAPE1(sFonoA1);
          dlgContacto.datconcastub.setDS_FONOAPE2(sFonoA2);
          dlgContacto.datconcastub.setDS_FONONOMBRE(sFonoN);
          dlgContacto.datconcastub.setIT_CALC(dlg.Resultados.sItCalc);
          dlgContacto.datconcastub.setFC_NAC(Fechas.StringToSQLDate(dlg.
              Resultados.sFechaNac));
          dlgContacto.datconcastub.setCD_SEXO(dlg.Resultados.sCodSexo);
          dlgContacto.datconcastub.setDS_TELEF(dlg.Resultados.sTelefono);
          dlgContacto.datconcastub.setCD_NIVEL_1(dlg.Resultados.sCodNivel1);
          dlgContacto.datconcastub.setCD_NIVEL_2(dlg.Resultados.sCodNivel2);
          dlgContacto.datconcastub.setCD_ZBS(dlg.Resultados.sCodZBS);
          dlgContacto.datconcastub.setCD_OPE(dlg.Resultados.CD_OPE);
          dlgContacto.datconcastub.setFC_ULTACT(Fechas.string2Timestamp(dlg.
              Resultados.FC_ULTACT));
          dlgContacto.datconcastub.setDS_NDOC(dlg.Resultados.sNDoc);
          dlgContacto.datconcastub.setSIGLAS(dlg.Resultados.sSiglas);
          //flags de it_contacto y de it_enfermo
          dlgContacto.datconcastub.setIT_ENFERMO(dlg.Resultados.IT_ENFERMO);
          dlgContacto.datconcastub.setIT_CONTACTO(dlg.Resultados.IT_CONTACTO);
          // modificacion 28/02/2001
          // Metemos el a�o y el n�mero de registro.
          dlgContacto.datconcastub.setCD_ANO_RTBC(sAnio);
          dlgContacto.datconcastub.setCD_REG_RTBC(sNumReg);
          // Metemos CD_OPE y FC_ULTACT
        }

        //FIN DE CARGAR DATA
        //confidencialidad
        if (!getFlagEnfermo()) { //confid
          Confidencialidad();
        }
        else {
          Visible(true);
        } //---------------------------

      }
      //Abrio, lo vio y CANCELAR!!!
      //!!!!!!!!!!!
      else {
        //nada
        iSalida = 0;
        modoOperacion = modo;
        if (bEnfermo) { //PDP 09/02/2000
          dlgTuber.Inicializar(modoOperacion); //mlm nuevo
        }
        else {
          dlgContacto.Inicializar(modoOperacion); //mlm nuevo
        }
      }

    } //NO HAY DATOS !!!!!
    else {
      //System.out.println("NO hay datos");
      if (bEnfermo) { //PDP 09/02/2000
        dlgTuber.getBarra().setText("No se han encontrado fichas coincidentes");
        iSalida = -1;
        modoOperacion = modo;
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.getBarra().setText(
            "No se han encontrado fichas coincidentes");
        iSalida = -1;
        modoOperacion = modo;
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }
    }

    //DE CUALQUIER MODO:
    dlg = null;

    return (iSalida);
  }

  public void cargaAnioNumReg(String anio, String numreg) {
    sAnio = anio;
    sNumReg = numreg;
  }

//0: sale por cancelar  ALTA
//-1: no hay datos, sacar mensaje si se hace desde boton caso. ALTA
//1: aceptar y con datos
//llama a rellenadatos, y abre los paneles. MODIFICACION
//Esta funcion se queda en espera!!!!, poner detras
//de quien la llame, modo e inicializar (NO EN MODIFICACION; QUE YA LO HACE)
  protected int Buscar_Registro() {

    int result = 0;

    if (getApp().getPerfil() == 5) {
      return (result);
    }

    int modo = modoOperacion;
    if (modoOperacion != modoESPERA) {
      modoOperacion = modoESPERA;
      if (bEnfermo) { //PDP 10/02/2000
        dlgTuber.Inicializar(modoOperacion); //mlm nuevo
      }
      else {
        dlgContacto.Inicializar(modoOperacion); //mlm nuevo
      }
    }

    //Cuando se llega aqui, siempre esta informado el enfermo
    //datos del enfermo en la base de datos: dlgTuber.datcasostub
    // NO SE PUEDE SABER SI EL PANEL HA SIDO CONSTRUIDO CON UN
    // DialogTub (lo que nos permitir�a seguir) o con un DiaConCasoTub.
    // salvo comprobando si las referencias a los di�logos apuntan a algo o no.
    //<AIC>
    if (dlgTuber != null) { //</AIC>
      DataRegistroEDO dataCaso = new DataRegistroEDO(
          txtCodEnfermo.getText().trim(),
          dlgTuber.datcasostub.getDS_APE1(),
          dlgTuber.datcasostub.getDS_APE2(),
          dlgTuber.datcasostub.getDS_NOMBRE(),
          dlgTuber.datcasostub.getSIGLAS(),
          0,
          "TUBERCULOSIS",
          // modificacion 12/02/2001
          //constantes.TUBERCULOSIS,
          this.app.getParametro("CD_TUBERCULOSIS"),
          false,
          false,
          getFlagEnfermo(),
          txtFechaNac.getText().trim(),
          CfechaNotif.getText().trim(),
          "", "", "",
          "", "", "", "", "", "", "", "", "");
      dlgTuber.getBarra().setText("Buscando registros coincidentes...");

      DialogRegistroEDO dlg = new DialogRegistroEDO(this.getApp(), dataCaso);

      // ahora se recoge el flag: si vuelve a true, es que se ha
      // cerrado con Aceptar y hay datos
      if (dlg.Resultados.bHayDatos) {
        dlgTuber.getBarra().setText("");
        dlg.show();

        // si hay datos, y adem�s se han seleccionado y aceptado,
        // tenemos un CASO EXISTENTE -> pasar a modo MODIFICAR
        if (dlg.Resultados.bEsOK) {
          //si igual datos, No Solapas, pero si modificacion
          //si diferentes datos, cerrar y abrir , modificacion
          //PERO, las solapas se abren con NUEVO SELECT , ptt
          //SOLO modificacion

          //simula modificacion
          //dlgTuber.modoOperacionBk =  modoMODIFICACION;
          dlgTuber.modoOperacion = modoMODIFICACION;
          modo = modoMODIFICACION;

          //CASO
          //recuperar DATOS DEL REGISTRO
          dlgTuber.Inicializar(modoESPERA);
          dlgTuber.sANO = dlg.Resultados.sCD_ARTBC;
          dlgTuber.sREG = dlg.Resultados.sCD_NRTBC;
          dlgTuber.sNM_EDO = (new Integer(dlg.Resultados.iCodCaso)).toString();
          DatRT datos = new DatRT();
          datos.insert("CD_ARTBC", dlgTuber.sANO);
          datos.insert("CD_NRTBC", dlgTuber.sREG);
          datos.insert("NM_EDO", dlgTuber.sNM_EDO);
          try {
            dlgTuber.RellenaDatosTodosPaneles(datos); //se encarga de abrir paneles
          }
          catch (Exception ex) {
            ex.printStackTrace();
            dlgTuber.bError = true;
          }
          dlgTuber.Inicializar(modoMODIFICACION); //
          result = 1; //modo modificacion
        } //ACEPTAR

        //CANCELAR
        else {
          //nada
          result = 0;

        }

      }
      else {
        dlgTuber.getBarra().setText("No existen registros coincidentes");
        result = -1;

      }

      dlg = null;

      //modoOperacion = modo; //reseteara pCabecera!!!! (ver si txtCodEnfermo amarillo)
      //Inicializar();
    }
    else if (dlgContacto != null) {
      // modificacion 09/02/2001 para poner el dialogo de
      // contacto en modificacion
      //dlgContacto.modoOperacion = modoMODIFICACION;
      modo = modoMODIFICACION;
      //dlgContacto.Inicializar(modoMODIFICACION);
      //result = 1; //modo modificacion
    }
    return (result);

  } //fin Buscar_Registro

}

// FOCO*******************************************************************
class PanCabecerafocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanCabecera adaptee;
  FocusEvent evt;

  PanCabecerafocusAdapter(PanCabecera adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("fechanac")) {
      adaptee.txtFechaNac_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("edad")) {
      adaptee.txtEdad_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("ape1")) {
      adaptee.txApe1_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("nivel1")) {
      adaptee.txtCodNivel1_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("nivel2")) {
      adaptee.txtCodNivel2_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("zbs")) {
      adaptee.txtCodZBS_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("enfermo")) {
      adaptee.txtCodEnfermo_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("documento")) {
      adaptee.txtDocumento_focusLost();
    }
  }

} //fin class

// escuchador para el pulsado de los botones
class PanCabeceraactionAdapter
    implements java.awt.event.ActionListener {
  PanCabecera adaptee;
  ActionEvent evt;

  PanCabeceraactionAdapter(PanCabecera adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    if (evt.getActionCommand().equals("enfermo")) {
      adaptee.btnLupaEnfermo_actionPerformed();
    }
    else if (evt.getActionCommand().equals("nivel1")) {
      adaptee.btnNivel1_actionPerformed();
    }
    else if (evt.getActionCommand().equals("nivel2")) {
      adaptee.btnNivel2_actionPerformed();
    }
    else if (evt.getActionCommand().equals("zbs")) {
      adaptee.btnZBS_actionPerformed();
    }
    else if (evt.getActionCommand().equals("selenfermo")) {
      adaptee.btnEnfermo_actionPerformed();
    }
    else if (evt.getActionCommand().equals("datosenfermo")) {
      adaptee.btnCaso_actionPerformed();

    }
  }

}

// escuchador de los cambios en los itemListener  de los choices
/**
 *  hay tres CListas que representan paises,comunidades y provincias
 *  cuando cambiamos el pais, se rellena la lista de comunidades
 *  y se vacia la lista de Provincias
 */
class PanCabecerachItemListener
    implements java.awt.event.ItemListener, Runnable {
  PanCabecera adaptee;
  ItemEvent e = null;

  PanCabecerachItemListener(PanCabecera adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(ItemEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    // quitamos los valores de los textfield de descripcion
    String name = ( (Component) e.getSource()).getName();

    if (name.equals("tipodoc")) {
      adaptee.choTipoDocumento_itemStateChanged();
    }
    else if (name.equals("edad")) {
      adaptee.choEdad_itemStateChanged();
    }
    // desbloquea la recepci�n  de eventos
    adaptee.desbloquea();
  }
}

// escuchador para las cajas de texto
class PanCabeceratextAdapter
    extends java.awt.event.KeyAdapter {
  PanCabecera adaptee;

  PanCabeceratextAdapter(PanCabecera adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    if ( ( (TextField) e.getSource()).getName().equals("nivel1")) {
      adaptee.txtCodNivel1_keyPressed();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("nivel2")) {
      adaptee.txtCodNivel2_keyPressed();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("zbs")) {
      adaptee.txtCodZBS_keyPressed();
    }
  }
}

// PRIMERA TRAMA DE LAS LISTAS/////////////////////////////////
// clase para obtener y seleccionar niveles2
class CListaNiveles2Indiv
    extends CListaValores {

  protected PanCabecera panel;

  public CListaNiveles2Indiv(PanCabecera p,
                             String title,
                             StubSrvBD stub,
                             String servlet,
                             int obtener_x_codigo,
                             int obtener_x_descricpcion,
                             int seleccion_x_codigo,
                             int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;

    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "",
                              panel.txtCodNivel1.getText(),
                              panel.txtCodNivel2.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

// clase para obtener y seleccionar zbs
class CListaNivelesZBSIndiv
    extends CListaValores {

  protected PanCabecera panel;

  public CListaNivelesZBSIndiv(PanCabecera p,
                               String title,
                               StubSrvBD stub,
                               String servlet,
                               int obtener_x_codigo,
                               int obtener_x_descricpcion,
                               int seleccion_x_codigo,
                               int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.txtCodNivel1.getText(),
                              panel.txtCodNivel2.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}
