/**
 * Clase: PanConCasTub
 * Paquete: tuberculosis.cliente.PanConCasTub
 * Hereda: CPanel
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 28/01/2000
 * Analisis Funcional: Mantenimiento Contactos Tuberculosis.
 * Descripcion: Implementacion del dialogo que permite visualizar los datos
    principales de los contactos de enfermos de tuberculosis. Los registros
    mostrados dependen de los valores suministrados a los campos de filtrado.
    Ademas permite acceder a la pantalla de modificacion/cierre/deshacer cierre
    de los datos asociados a un determinado registro de tuberculosis.
 */

package tuberculosis.cliente.diacontub;

import java.net.URL;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import comun.Common;
import comun.DataEnfermo;
import comun.Fechas;
import comun.constantes;
import enfermotub.DialBusEnfermo;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import sapp.StubSrvBD;
import tuberculosis.datos.mantregtub.DatMntRegTubCS;
import tuberculosis.datos.mantregtub.DatMntRegTubSC;

//import tuberculosis.servidor.mantregtub.*;

public class PanConCasTub
    extends CPanel {

  //public int iERROR = -1;  //0: error

  // Modos de entrada al panel

  final int modoMANTENIMIENTO = constantes.modoMANTENIMIENTO;

//  final int modoCERRAR = constantes.modoCERRAR;
//  final int modoDESHACERCIERRE = constantes.modoDESHACERCIERRE;
  final int modoCASOSTUB = constantes.modoCASOSTUB;

  int modoEntrada = modoMANTENIMIENTO; // Por defecto

  // Modos de operacion del panel
  final int modoESPERA = 0;
  final int modoBUSCAR = 1;
  int modoOperacion = modoBUSCAR; // Por defecto

  // Flags de permisos de usuario (solo existe el permiso de modificacion)
  boolean bAlta = false;
  boolean bMod = false;
  boolean bBaja = false;

  // Modo de representacion del nombre del enfermo (segun flag confidencial)
  boolean bNombreCompleto = true;

  //Prime vez que se entra en el panel
  boolean bFirst = false;

  // Datos para la busqueda de registros
  private DatMntRegTubCS dataBusqueda = null;

  //Datos para el envio de informaci�n al di�logo

  public DatMntRegTubSC RegSel = null;
  public String datEnv = null;

  // Lista de los parametros de busqueda
  public CLista listaBusquedaRegTub = null;

  // Lista de Registros de Tuberculosis/o CASOS
  public CLista listaRegTubs = null;

  // Stub's
  protected StubSrvBD stubCliente = null;

  // Localizacion del servlet
  private final String strSERVLET_CASOSTUB = constantes.strSERVLET_CASOSTUB;

  // Para gestionar cambios en cajas de texto
  protected String s_Cod_Enfermo = "";
  protected String s_Fecha_Desde = "";
  protected String s_Fecha_Hasta = "";
  protected String s_Ano = "";
  protected String s_Registro = "";
  protected String sAno = "";
  // Contenedores de im�genes
  protected CCargadorImagen imgs = null;
  protected CCargadorImagen imgs_mantenimiento = null;
  protected CCargadorImagen imgs_tabla = null;
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/refrescar.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/declaracion.gif",
      "images/declaracion2.gif"};

  final String imgNAME_mantenimiento[] = {
      "images/alta2.gif",
      "images/modificacion2.gif",
      "images/baja2.gif"};
  final String imgNAME_tabla[] = {
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif"};

  /* -------------------- CONTROLES ------------------------ */

  // Organizacion del panel
  private XYLayout lyXYLayout = new XYLayout();

  // A�o/Registro
  private Label lblAnoReg = new Label("A�o/Registro:");
  private TextField txtAno = new TextField();
  private boolean btxtAno = true;
  private Label lblBarra = new Label("/");
  private TextField txtReg = new TextField();
  private boolean btxtReg = true;

  // Codigo contacto
  private Label lblCodContacto = new Label("Cod. Enfermo:");
  private TextField txtCodContacto = new TextField();
  private boolean btxtCodContacto = true;
  private ButtonControl btnLupaContacto = new ButtonControl();
  private boolean bbtnLupaContacto = true;

  // Fecha entrada Desde-Hasta
  private Label lblFechaEnt = new Label("Fecha Entrada");
  private Label lblDesde = new Label("Desde:");
  // ARS 22-05-01; Cambio para poner barra autom�ticamente
//  private CFechaSimple txtFechaDesde = new CFechaSimple("N");
  private fechas.CFecha txtFechaDesde = new fechas.CFecha("N");
  private boolean btxtFechaDesde = true;
  private Label lblHasta = new Label("Hasta:");
  // ARS 22-05-01; Cambio para poner barra autom�ticamente
//  private CFechaSimple txtFechaHasta = new CFechaSimple("N");
  private fechas.CFecha txtFechaHasta = new fechas.CFecha("N");
  private boolean btxtFechaHasta = false;

  // Boton Buscar
  private ButtonControl btnBuscar = new ButtonControl();

  // Tabla que visualiza y permite navegar entre los registros tuberculosis
  public CTabla tabla = new CTabla();

  // Controles para el mantenimiento y navegacion entre los registros de la tabla
  private ButtonControl btnAlta = null;
  private ButtonControl btnModificar = null;
  private ButtonControl btnBaja = null;
  private ButtonControl btnPrimero = null;
  private ButtonControl btnAnterior = null;
  private ButtonControl btnSiguiente = null;
  private ButtonControl btnUltimo = null;

  // Botones de cierre y deshacer cierre del registro de tuberculosis
  private ButtonControl btnCerrar = new ButtonControl();
  private ButtonControl btnDeshacerCierre = new ButtonControl();

  // Escuchadores
  PanConCasTubFocusAdapter focusAdapter = null;
  PanConCasTubActionAdapter actionAdapter = null;
  // modificacion
  PanConCasTubTablaDobleClick tablaDobleClickListener = null;

  /* CONSTRUCTOR */
  private DiaConTub dlg = null;

  public PanConCasTub(DiaConTub dialogo) {
    try {
      dlg = dialogo;

      this.app = dlg.getCApp();

      setApp(dlg.getCApp());
      setBorde(false);

      stubCliente = new StubSrvBD(new URL(this.app.getURL() +
                                          strSERVLET_CASOSTUB));

      jbInit();

      switch (modoEntrada) {
        case modoMANTENIMIENTO:
          /*
           btnCerrar.setVisible(false);
           btnDeshacerCierre.setVisible(false);
           /*
            btnAlta.setVisible(false);
            btnBaja.setVisible(false);
            btnModificar.setVisible(true);
            break;
        */
      }

      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // Funcion de inicializacion de los controles del panel
  void jbInit() throws Exception {

    // Vars. para colocar los componentes
    int pX = 0; // Posicion en X
    int pY = 0; // Posicion en Y
    int longX = 0; // Longitud en X de un componente
    int longY = 0; // Longitud en Y de un componente

    // Clase escuchadora de los eventos relacionados con el foco
    focusAdapter = new PanConCasTubFocusAdapter(this);
    actionAdapter = new PanConCasTubActionAdapter(this);
    // modificacion
    tablaDobleClickListener = new PanConCasTubTablaDobleClick(this);

    // Medidas y organizacion del panel
    this.setSize(new Dimension(680, 370));
    lyXYLayout.setHeight(370);
    lyXYLayout.setWidth(680);
    this.setLayout(lyXYLayout);

    // Carga de las imagenes generales (lupa, buscar)
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // A�o/Registro del registro de tuberculosis. A�o actual por defecto
    pX = 15;
    longX = 90;
    pY = 15;
    longY = -1;
    this.add(lblAnoReg, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 40;
    txtAno.setBackground(new Color(255, 255, 150));
    this.add(txtAno, new XYConstraints(pX, pY, longX, longY));
    txtAno.setName("Ano");
    txtAno.addFocusListener(focusAdapter);
    txtAno.requestFocus();

    pX += longX + 5;
    longX = 5;
    this.add(lblBarra, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 50;
    this.add(txtReg, new XYConstraints(pX, pY, longX, longY));
    txtReg.setName("Registro");
    txtReg.addFocusListener(focusAdapter);
    txtReg.requestFocus();

    // Codigo del Contacto
    pX = 15;
    longX = 80;
    pY += 30;
    longY = -1;
    this.add(lblCodContacto, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 20;
    longX = 60;
    this.add(txtCodContacto, new XYConstraints(pX, pY, longX, longY));
    btnLupaContacto.setImage(imgs.getImage(0));
    pX += longX + 10;
    longX = -1;
    this.add(btnLupaContacto, new XYConstraints(pX, pY, longX, longY));
    txtCodContacto.setName("CodEnfermo");
    txtCodContacto.addFocusListener(focusAdapter);
    btnLupaContacto.setActionCommand("LupaContacto");
    btnLupaContacto.addActionListener(actionAdapter);

    // Fecha Entrada:  Desde-Hasta
    pX = 15;
    longX = 95;
    pY += 30;
    longY = -1;
    this.add(lblFechaEnt, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 39;
    this.add(lblDesde, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 75;
    this.add(txtFechaDesde, new XYConstraints(pX, pY, longX, longY));
    txtFechaDesde.setName("FechaDesde");
    txtFechaDesde.addFocusListener(focusAdapter);
    pX += longX + 20;
    longX = 35;
    this.add(lblHasta, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 75;
    this.add(txtFechaHasta, new XYConstraints(pX, pY, longX, longY));
    txtFechaHasta.setName("FechaHasta");
    txtFechaHasta.addFocusListener(focusAdapter);

    // Boton de busqueda de registros
    btnBuscar.setImage(imgs.getImage(1));
    btnBuscar.setLabel("Buscar");
    btnBuscar.setActionCommand("Buscar");
    btnBuscar.addActionListener(actionAdapter);
    pX += longX + 100 + 75;
    longX = -1;
    this.add(btnBuscar, new XYConstraints(pX, pY, longX, longY));

    // Se establecen los parametros de la tabla
    PintarTabla();
    pX = 15;
    pY += 45;
    longX = 557 + 75;
    longY = 125;
    this.add(tabla, new XYConstraints(pX, pY, longX, longY));
    //modificacion
    tabla.addActionListener(tablaDobleClickListener);

    // Carga de las im�genes de navegacion
    imgs_tabla = new CCargadorImagen(app, imgNAME_tabla);
    imgs_tabla.CargaImagenes();

    // Creacion de los botones de navegacion por los registros de la tabla
    btnPrimero = new ButtonControl(imgs_tabla.getImage(0));
    btnPrimero.setActionCommand("Primero");
    btnPrimero.addActionListener(actionAdapter);
    btnAnterior = new ButtonControl(imgs_tabla.getImage(1));
    btnAnterior.setActionCommand("Anterior");
    btnAnterior.addActionListener(actionAdapter);
    btnSiguiente = new ButtonControl(imgs_tabla.getImage(2));
    btnSiguiente.setActionCommand("Siguiente");
    btnSiguiente.addActionListener(actionAdapter);
    btnUltimo = new ButtonControl(imgs_tabla.getImage(3));
    btnUltimo.setActionCommand("Ultimo");
    btnUltimo.addActionListener(actionAdapter);
    pX = 440 + 75;
    longX = 25;
    pY += longY + 10;
    longY = 25;
    this.add(btnPrimero, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnAnterior, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnSiguiente, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnUltimo, new XYConstraints(pX, pY, longX, longY));

    // Botones de cerrar o deshacer cierre del registro
    btnCerrar = new ButtonControl(imgs.getImage(4));
    btnCerrar.setLabel("Cerrar");
    btnCerrar.setActionCommand("Cerrar");
    btnCerrar.addActionListener(actionAdapter);
    btnDeshacerCierre = new ButtonControl(imgs.getImage(5));
    btnDeshacerCierre.setLabel("Deshacer Cierre");
    btnDeshacerCierre.setActionCommand("Deshacer Cierre");
    btnDeshacerCierre.addActionListener(actionAdapter);

  } // Fin jbInit()

  // Establece el numero de columnas de la tabla y las cabeceras de las mismas
  public void PintarTabla() {
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "100\n100\n350\n55"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("Registro\nF.Entrada\nEnfermo\nCerrado"), '\n'));
    tabla.setNumColumns(4);
    tabla.setRowHeight(20);
    tabla.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    tabla.setInsets(new Insets(5, 5, 5, 5));
  } // fin PintarTabla()

  private CLista CargarListas() {

    boolean b = false;

    CLista parametros = new CLista();
    CLista listaSalida = new CLista();
    /*
      try{
        if (modoEntrada == modoCASOSTUB)
           listaSalida = Comunicador.Communicate(this.app,
                            stubCliente,
                            0 ,
                            strSERVLET_CATALOGOS_CASOSTUB,
                            parametros);
        else
          listaSalida = Comunicador.Communicate(this.app,
                            stubCliente,
                            0 ,
                            strSERVLET_CATALOGOS,
                            parametros);
        if (listaSalida != null){
          if (listaSalida.size() > 0) {
            if (modoEntrada == modoCASOSTUB){
         catalogos.put(constantes.CLASIFICACIONES, (CLista)listaSalida.elementAt(0));
         catalogos.put(constantes.PAISES, (CLista)listaSalida.elementAt(1));
              catalogos.put(constantes.CA, (CLista)listaSalida.elementAt(2));
         catalogos.put(constantes.TRAMEROS, (CLista)listaSalida.elementAt(3));
         catalogos.put(constantes.NUMEROS, (CLista)listaSalida.elementAt(4));
         catalogos.put(constantes.CALIFICADORES, (CLista)listaSalida.elementAt(5));
              catalogos.put(constantes.SEXO, (CLista)listaSalida.elementAt(6));
              catalogos.put(constantes.TDOC, (CLista)listaSalida.elementAt(7));
         catalogos.put(constantes.FUENTESNOTIF, (CLista)listaSalida.elementAt(8));
         catalogos.put(constantes.MOTIVOS_TRATAMIENTO, (CLista)listaSalida.elementAt(9));
              // Para muestras (resultados de laboratorio)
         catalogos.put(constantes.TIPO_TECNICALAB, (CLista) listaSalida.elementAt(10));
         catalogos.put(constantes.MUESTRA_LAB, (CLista) listaSalida.elementAt(11));
         catalogos.put(constantes.VALOR_MUESTRA, (CLista) listaSalida.elementAt(12));
         catalogos.put(constantes.TMICOBACTERIA, (CLista) listaSalida.elementAt(13));
         catalogos.put(constantes.ESTUDIORESIS, (CLista) listaSalida.elementAt(14));
            }else{
         catalogos.put("MOTIVOS_SALIDA", (CLista)listaSalida.elementAt(0));
         catalogos.put(constantes.PAISES, (CLista)listaSalida.elementAt(1));
              catalogos.put(constantes.CA, (CLista)listaSalida.elementAt(2));
              catalogos.put("SEXOS", (CLista)listaSalida.elementAt(3));
            }
            // Si se est� dando de alta, debe mostrarse el di�logo de RT
            // que necesita estos datos
            if (modoEntrada == modoCASOSTUB) {
              parametros = new CLista();
              listaSalida = new CLista();
              listaSalida = Comunicador.Communicate(this.app,
                            stubCliente,
                            0 ,
                            strSERVLET_CATALOGOS,
                            parametros);
              if (listaSalida != null){
                if (listaSalida.size() > 0) {
         catalogos.put("MOTIVOS_SALIDA", (CLista)listaSalida.elementAt(0));
                  catalogos.put("SEXOS", (CLista)listaSalida.elementAt(3));
                }
              }
            }
          }
          else {
            ShowWarning("No existen datos para rellenar los cat�logos.");
            b = true;
          }
        }
        else{
          ShowWarning("Error al recuperar los cat�logos.");
          b = true;
        }
      } catch (Exception excepc){
        excepc.printStackTrace();
        ShowWarning ("Error al recuperar los cat�logos.");
        b = true;
      }
      // Si no se pueden recuperar los cat�logos semuestra la p�gina HTML anterior
      if (b){
        try{
          System.runFinalization();
          System.gc();
          app.getAppletContext().showDocument( new URL(app.getCodeBase(), "default.html"),"_self");
        } catch(Exception excepc){;}
      }
     */
    return (listaSalida);

  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar(modo)

  // Implementacion del metodo abstracto de Cpanel
  public void Inicializar() {
    //boolean bFechaHasta;

    switch (modoOperacion) {
      case modoBUSCAR:

        // modificacion
        txtAno.setEnabled(btxtAno);
        txtAno.setEditable(btxtAno);
        txtReg.setEnabled(btxtReg);
        txtReg.setEditable(btxtReg);
        txtCodContacto.setEnabled(btxtCodContacto);
        txtCodContacto.setEditable(btxtCodContacto);
        btnLupaContacto.setEnabled(bbtnLupaContacto);
        txtFechaDesde.setEnabled(btxtFechaDesde);
        txtFechaDesde.setEditable(btxtFechaDesde);
        txtFechaHasta.setEnabled(btxtFechaHasta);
        txtFechaHasta.setEditable(btxtFechaHasta);

        // A�o / Registro
//        txtAno.setEnabled(true);
//        txtReg.setEnabled(true);
        //Lupa / Etiqueta
//        btnLupaContacto.setEnabled(true);

        // Tabla y botones
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        tabla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case modoESPERA:

        // A�o / Registro
        txtAno.setEnabled(false);
        txtReg.setEnabled(false);
        //Lupa / Etiqueta
        btnLupaContacto.setEnabled(false);

        // Botones de modificaci�n/navegaci�n por la tabla
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);

        // Deshabilitamos la tabla
        tabla.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

    // Repintado de la pantalla
    this.doLayout();
  } // Fin Inicializar()

  //Recibe el a�o del panel princiapl
  public void setAno(String sAno) {
    txtAno.setText(sAno);
  }

  //Recibe el registro del panel princiapl
  public void setReg(String sReg) {
    txtReg.setText(sReg);
  }

  // Ganancia de foco de A�o
  void txtAnoFocusGained() {
    s_Ano = new String(txtAno.getText().trim());
  }

  // Ganancia de foco de Registro
  void txtRegFocusGained() {
    s_Registro = new String(txtReg.getText().trim());
    if (s_Registro == "") {
      bFirst = true;
    }
  }

  // P�rdida de foco de txtAno
  void txtAnoFocusLost() {
    // Se ajusta el contenido
    txtAno.setText(txtAno.getText().trim());

    if (txtAno.getText().equals("")) {
      txtAno.setText(s_Ano);
    }

    // Obtiene el foco
    boolean bFocus = false;

    // Comprobaci�n del valor de txtAno
    // El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo
    try {
      Integer f = new Integer(txtAno.getText());
      int ano = f.intValue();
      if ( (ano < 1000) || (ano > 9999)) {
        Common.ShowWarning(this.app,
            "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
        txtAno.setText(s_Ano);
        bFocus = true;
      }
    }
    catch (Exception e) {
      Common.ShowWarning(this.app,
          "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
      txtAno.setText(s_Ano);
      bFocus = true;
    }

    // Se resetea la tabla (el numero de ano puede ser diferente)
    if (!txtAno.getText().equals(s_Ano)) {
      ResetearTabla();
    }

    // Se resetea la tabla (el numero registro puede ser diferente)
    if (!txtReg.getText().equals(s_Registro)) {
      ResetearTabla();
    }

    if (bFocus) {
      txtAno.requestFocus();
    }
  } // Fin txtA�oFocusLost()

  // Perdida de foco de txtReg
  void txtRegFocusLost() {
    // Se ajusta el contenido
    txtReg.setText(txtReg.getText().trim());

    // Anterior modo
    int modo = modoOperacion;

    // Seguir la comprobacion del campo txtReg
    boolean bSeguirCompr = true;

    // Obtiene el foco
    boolean bFocus = false;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtReg.getText().equals("")) {
      bSeguirCompr = false;

      btxtCodContacto = true;
      bbtnLupaContacto = true;
      btxtFechaDesde = true;
      if (txtFechaDesde.getText().equals("")) {
        btxtFechaHasta = false;
      }
      else {
        btxtFechaHasta = true;

      }
      Inicializar(modoBUSCAR);
      txtCodContacto.requestFocus();
    }

    // Existe texto en el txtReg: Comprobaci�n del valor de txtReg
    // El registro es un valor num�rico, <= 5 d�gitos y positivo
    if (bSeguirCompr) {
      try {
        Integer f = new Integer(txtReg.getText());
        int reg = f.intValue();
        if ( (reg < 0) || (reg > 99999)) {
          ShowWarning(
              "El registro es un valor num�rico, menor o igual que 5 d�gitos y positivo.");
          txtReg.setText("");
          bSeguirCompr = false;
          bFocus = true;
        }
      }
      catch (Exception e) {
        ShowWarning(
            "El registro es un valor num�rico, menor o igual que 5 d�gitos y positivo.");
        txtReg.setText("");
        txtReg.requestFocus();
        bSeguirCompr = false;
        bFocus = true;
      }
    }

    // Se resetea la tabla (el numero registro puede ser diferente)
    if (!txtReg.getText().equals(s_Registro)) {
      ResetearTabla();
    }

    // Existe algo en txtReg y es correcto: deshabilitamos el resto de campos de filtro
    if (bSeguirCompr) {
      txtCodContacto.setText("");
      btxtCodContacto = false;
      bbtnLupaContacto = false;
      txtFechaDesde.setText("");
      btxtFechaDesde = false;
      txtFechaHasta.setText("");
      btxtFechaHasta = false;

      btnBuscar.requestFocus();
    }

    Inicializar(modoBUSCAR);

    if (bFocus) {
      txtReg.requestFocus();

      // Reestablecer el mod oanterior
    }
    Inicializar(modo);
  } // Fin txtRegFocusLost()

  // Perdida de foco de txtCodContacto
  void txtCodContactoFocusLost() {
    // Se ajusta el contenido
    txtCodContacto.setText(txtCodContacto.getText().trim());

    // Seguir la comprobacion del campo txtReg
    boolean bSeguirCompr = true;

    // Obtiene el foco
    boolean bFocus = false;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtCodContacto.getText().equals("")) {
      bSeguirCompr = false;

      // Existe texto en el txtCodEfermo: Comprobaci�n del valor de txtCodContacto
      // El registro es un valor num�rico, <= 6 d�gitos y positivo
    }
    if (bSeguirCompr) {
      try {
        Integer f = new Integer(txtCodContacto.getText());
        int reg = f.intValue();
        if ( (reg < 0) || (reg > 999999)) {
          ShowWarning("El c�digo de enfermo es un valor num�rico, menor o igual que 6 d�gitos y positivo.");
          txtCodContacto.setText("");
          bSeguirCompr = false;
          bFocus = true;
        }
      }
      catch (Exception e) {
        ShowWarning("El c�digo de enfermo es un valor num�rico, menor o igual que 6 d�gitos y positivo.");
        txtCodContacto.setText("");
        txtCodContacto.requestFocus();
        bSeguirCompr = false;
        bFocus = true;
      }
    }

    // Se resetea la tabla (el a�o puede ser diferente)
    if (!txtCodContacto.getText().equals(s_Cod_Enfermo)) {
      ResetearTabla();
    }

    if (bFocus) {
      txtCodContacto.requestFocus();

    }
  } // Fin txtCodContactoFocusLost()

  // Perdida de foco de txtFechaDesde
  void txtFechaDesdeFocusLost() {
    // Se ajusta el contenido
    txtFechaDesde.setText(txtFechaDesde.getText().trim());

    // Anterior modo
    int modo = modoOperacion;

    // Seguir la comprobacion del campo txtFechaDesde
    boolean bSeguirCompr = true;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtFechaDesde.getText().equals("")) {
      bSeguirCompr = false;
      txtFechaHasta.setText("");
      btxtFechaHasta = false;
      Inicializar(modoBUSCAR);
      btnBuscar.requestFocus();
    }

    // Comprobacion de formato valido de la fecha
    if (bSeguirCompr) {
      // Validacion de la fecha
      txtFechaDesde.ValidarFecha();

      // Carga de la fecha
      txtFechaDesde.setText(txtFechaDesde.getFecha());

      // Si la fecha es valida se habilita la Fecha Hasta
      if (txtFechaDesde.getValid().equals("S")) {
        btxtFechaHasta = true;
        Inicializar(modoBUSCAR);
        //txtFechaHasta.requestFocus();
      }
      else {
        txtFechaHasta.setText("");
        btxtFechaHasta = false;
        Inicializar(modoBUSCAR);
        //btnBuscar.requestFocus();
      }
    }

    // Transferencia del foco
    //txtFechaDesde.transferFocus();

    // Se resetea la tabla (el a�o puede ser diferente)
    if (!txtFechaDesde.getText().equals(s_Fecha_Desde)) {
      ResetearTabla();
    }

    // Reestablecer el modo
    Inicializar(modo);
  } // Fin txtFechaDesdeFocusLost()

  // Perdida de foco de txtFechaHasta
  void txtFechaHastaFocusLost() {
    // Se ajusta el contenido
    txtFechaHasta.setText(txtFechaHasta.getText().trim());

    // Seguir la comprobacion del campo txtFechaDesde
    boolean bSeguirCompr = true;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtFechaHasta.getText().equals("")) {
      bSeguirCompr = false;

      // Comprobacion de formato valido de la fecha
    }
    if (bSeguirCompr) {

      // Validacion de la fecha
      txtFechaHasta.ValidarFecha();

      // Carga de la fecha
      txtFechaHasta.setText(txtFechaHasta.getFecha());
    }

    // Se resetea la tabla (el a�o puede ser diferente)
    if (txtFechaHasta.getText().equals(s_Fecha_Hasta)) {
      ResetearTabla();
    }

  } // Fin txtFechaHastaFocusLost()

  // Manejador del Boton de Contactos
  protected void btnLupaContactoActionPerformed() {

    // Requiere el foco
    btnLupaContacto.requestFocus();

    CLista listaDeEnfermos = new CLista();
    Object o = new Object();
    DataEnfermo miEnfermo = new DataEnfermo("CD_ENFERMO");

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Abrimos el dialogo del enfermo para elegir el codigo de uno de ellos
    // y colocamos en el TextField el codigo del enfermo elegido
    DialBusEnfermo dial = new enfermotub.DialBusEnfermo(this.app,
        constantes.sSoloEnfermos, true);
    dial.show();
    listaDeEnfermos = (CLista) dial.getListaDatosEnfermo();
    if (listaDeEnfermos != null) {
      if (listaDeEnfermos.size() > 0) {
        miEnfermo = (DataEnfermo) listaDeEnfermos.firstElement();
        o = miEnfermo.get("CD_ENFERMO");
        txtCodContacto.setText(o.toString());
      }
    }

    // Eliminamos el dialogo del enfermo
    dial.dispose();
    dial = null;

    // Se resetea la tabla (ha podido cambiar el codigo del enfermo)
    ResetearTabla();

    // Reestablecemos el modo de operacion
    modoOperacion = modo;
    Inicializar(modoOperacion);

  } // Fin btnLupaContactoActionPerformed()

  // Manejador del Boton Buscar
  protected void btnBuscarActionPerformed(boolean bMensaje) {

    // Primero se requiere el foco
    btnBuscar.requestFocus();

    // Modo ESPERA
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Se resetea la tabla (el numero de ano puede ser diferente)
    if (!txtAno.getText().equals(s_Ano)) {
      ResetearTabla();
    }

    // Se resetea la tabla (el numero registro puede ser diferente)

    if (!txtReg.getText().equals(s_Registro)) {
      ResetearTabla();
    }
    if (bFirst) {
      ResetearTabla();
      bFirst = false;
    }

    // Creacion y relleno de la lista con los parametros de busqueda
    listaBusquedaRegTub = new CLista();
    CargarListaBusqueda();

    // Creacion de la lista de registros de tuberculosis, para que
    // acto seguido sea llamado el servlet
    listaRegTubs = new CLista();

    // Llamada al servlet: Intento de obtencion de los registros de tuberculosis
    try {

      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_CASOSTUB));

      listaRegTubs = (CLista) stubCliente.doPost(0, listaBusquedaRegTub);
      listaRegTubs.trimToSize();

      // Escritura de los datos obtenidos en la tabla
      if (listaRegTubs != null) {
        if (listaRegTubs.size() > 0) {
          EscribirTabla();
        }
        else {
          if (bMensaje) {
            ShowWarning("No se encontraron registros de tuberculosis");
          }
        }
      }
      else {
        ShowWarning(
            "Error al realizar la b�squeda de registros de tuberculosis");
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      ShowWarning(
          "Error al recuperar los datos sobre registros de tuberculosis");
    }

    // Reestablecemos el modo de operacion
    modoOperacion = modo;
    Inicializar(modoOperacion);

  } // Fin btnBuscarActionPerformed()

  // Rellena la lista de busqueda con los parametros apropiados del panel
  void CargarListaBusqueda() {
    String Ano = txtAno.getText().trim();
    String Reg = txtReg.getText().trim();
    String CodEnfermo = txtCodContacto.getText().trim();
    String FDesde = txtFechaDesde.getText().trim();
    String FHasta = txtFechaHasta.getText().trim();
    //String FCerrado = "";
    //String FConfidencial = "";

    /*
         // Se establece FCerrado
         if(modoEntrada == modoMANTENIMIENTO ||
      modoEntrada == modoCERRAR)
      FCerrado = "N";
         else if (modoEntrada == modoDESHACERCIERRE)
      FCerrado = "S";
         else if (modoEntrada == modoCASOSTUB)
      FCerrado = "";
     */

    /*
         // Se establece FConfidencial
         bNombreCompleto = (this.getApp().getIT_FG_ENFERMO()).equals("S");
         if(bNombreCompleto)
      FConfidencial = "N";
         else
      FConfidencial = "S";
     */
    // Creamos una estructura de datos de busqueda y la rellenamos
    //dataBusqueda = new DatMntRegTubCS(Ano,Reg,CodEnfermo,FDesde,FHasta,FCerrado,FConfidencial);
    dataBusqueda = new DatMntRegTubCS(Ano, Reg, CodEnfermo, FDesde, FHasta, null, null);
    // A�adimos la estructura a la lista de datos de busqueda
    listaBusquedaRegTub.addElement(dataBusqueda);
  } // Fin CargarListaBusqueda()

  // Cada registro de tuberculosis que proviene del sevlet se
  // introduce en la tabla
  public void EscribirTabla() {
    // Fila (registro tuberculosis)
    JCVector row = null;
    // Matriz de filas (registros de tuberculosis
    JCVector items = new JCVector();

    // Reseteo de la tabla
    ResetearTabla();

    // Datos de una fila de la tabla
    DatMntRegTubSC dataResult = null;

    for (int i = 0; i < listaRegTubs.size(); i++) {
      dataResult = (DatMntRegTubSC) listaRegTubs.elementAt(i);

      // Nuevo JCVector
      row = new JCVector();

      // A�o/Registro
      row.addElement(dataResult.getCD_ANO().trim() +
                     "/" +
                     dataResult.getCD_REG().trim());

      // F.Entrada
      row.addElement(dataResult.getFC_FECHAENTRADA().trim());

      // Enfermo (en funcion del flag de confidencialidad)
      if (bNombreCompleto) {
        row.addElement(dataResult.getCD_CODENFERMO().trim() + " - " +
                       dataResult.getDS_APE1ENFERMO().trim() + " " +
                       dataResult.getDS_APE2ENFERMO().trim() + " " +
                       dataResult.getDS_NOMBREENFERMO().trim());
      }
      else {
        row.addElement(dataResult.getCD_CODENFERMO().trim() + " - " +
                       dataResult.getDS_SIGLAS().trim());

      }

      if (dataResult.getIT_CERRADO().trim().equals("S")) {
        row.addElement(imgs.getImage(2));
      }
      else if (dataResult.getIT_CERRADO().trim().equals("N")) {
        row.addElement(imgs.getImage(3));

        // Se a�ade una fila a la matriz
      }
      items.addElement(row);
    } // Fin for()

    // Se establecen los  items
    tabla.setItems(items);

    // Verifica que sea la �ltima trama
    if (listaRegTubs.getState() == CLista.listaINCOMPLETA) {
      tabla.addItem("M�s datos ...");

      // Se selecciona el primer registro de la tabla
    }
    tabla.select(0);
    tabla.setTopRow(0);

    // Se repinta la tabla
    tabla.repaint();
  } // Fin EscribirTabla()

  /*
     //Encapsula Datos
     public void EncapsularDatos(){
     // Obtenci�n del registro seleccionado el a�o y el registro
     RegSel = (DatMntRegTubSC)listaRegTubs.elementAt(tabla.getSelectedIndex());
     String Ano = RegSel.getCD_ANO();
     String Reg = RegSel.getCD_REG();
     String Nom = RegSel.getDS_NOMBREENFERMO()+" "+
                  RegSel.getDS_APE1ENFERMO()+" "+
                  RegSel.getDS_APE2ENFERMO();
     }
   */

  // Resetea la tabla: borrado y repintado
  public void ResetearTabla() {

    tabla.clear();
    tabla.repaint();
    //btnModificar.setEnabled(false);
    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);
  } // Fin ResetearTabla()

  // Manejador del boton Primero
  void btnPrimeroActionPerformed() {
    if (tabla.countItems() > 0) {
      tabla.select(0);
      tabla.setTopRow(0);
    }
  } // Fin btnPrimeroActionPerformed()

  // Manejador del boton Anterior
  void btnAnteriorActionPerformed() {
    if (tabla.countItems() > 0) {
      int index = tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        tabla.select(index);
        if (index - tabla.getTopRow() <= 0) {
          tabla.setTopRow(tabla.getTopRow() - 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  } // Fin btnAnteriorActionPerformed()

  // Manejador del boton Siguiente
  void btnSiguienteActionPerformed() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;
      int index = tabla.getSelectedIndex();

      if (index >= 0 && index < ultimo) {
        index++;
        tabla.select(index);
        if (index - tabla.getTopRow() >= 4) {
          tabla.setTopRow(tabla.getTopRow() + 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  } // Fin btnSiguienteActionPerformed()

  // Manejador del boton Ultimo
  void btnUltimoActionPerformed() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;

      if (ultimo >= 0) {
        tabla.select(ultimo);
        if (tabla.countItems() >= 4) {
          tabla.setTopRow(tabla.countItems() - 4);
        }
        else {
          tabla.setTopRow(0);
        }
      }
    }
  } // Fin btnUltimoActionPerformed()

  // Manejador del doble-click en un registro de la tabla
  void TablaDobleClickActionPerformed() {

    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Si presionamos al "Mas Datos" se trae otro bloque de datos
    if ( (tabla.getSelectedIndex() == (tabla.countItems() - 1)) &&
        (listaRegTubs.getState() == CLista.listaINCOMPLETA)) {
      try {
        String sFiltro = listaRegTubs.getFilter();
        listaBusquedaRegTub.setFilter(sFiltro);
        CLista listaParcial = null;

        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_CASOSTUB));
        listaParcial = (CLista) stubCliente.doPost(0, listaBusquedaRegTub);

        listaRegTubs.appendData(listaParcial);
        EscribirTabla();

        // Se reestablece el modo
        modoOperacion = modo;
        Inicializar(modoOperacion);

      }
      catch (Exception e) {
        e.printStackTrace();
        ShowWarning(
            "Error al recuperar los datos sobre registros de tuberculosis");
      }

    }
    else {

      dlg.RegSel = (DatMntRegTubSC) listaRegTubs.elementAt(tabla.
          getSelectedIndex());

      if (dlg.RegSel != null) {
        dlg.sAno = dlg.RegSel.getCD_ANO();
        dlg.sReg = dlg.RegSel.getCD_REG();
        dlg.sNombre = dlg.RegSel.getDS_NOMBREENFERMO() + " " +
            dlg.RegSel.getDS_APE1ENFERMO() + " " +
            dlg.RegSel.getDS_APE2ENFERMO();
        dlg.CD_OPE_RTBC = dlg.RegSel.getCD_OPE();
        dlg.FC_ULTACT_RTBC = Fechas.string2Timestamp(dlg.RegSel.getFC_ULTACT());

        dlg.setCD_NIVEL_1_EDOIND(dlg.RegSel.getCD_NIVEL_1_EDOIND());
        dlg.setCD_NIVEL_2_EDOIND(dlg.RegSel.getCD_NIVEL_2_EDOIND());

        dlg.setCD_NIVEL_1_GE(dlg.RegSel.getCD_NIVEL_1_GE());
        dlg.setCD_NIVEL_2_GE(dlg.RegSel.getCD_NIVEL_2_GE());
      }

      dlg.dispose();

    }

  } // Fin TablaDobleClickActionPerformed()

  /*************** FUNCIONES AUXILIARES ***********************/

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowWarning()

  // Llama a CMessage para mostrar el mensaje de error sMessage
  public void ShowError(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgERROR, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowError()

} // endclass PanConCasTub

/* --------------- ESCUCHADORES ------------------- */

// Botones
class PanConCasTubActionAdapter
    implements java.awt.event.ActionListener {
  PanConCasTub adaptee;
  ActionEvent evt;

  PanConCasTubActionAdapter(PanConCasTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    if (evt.getActionCommand().equals("LupaContacto")) {
      adaptee.btnLupaContactoActionPerformed();
    }
    else if (evt.getActionCommand().equals("Buscar")) {
      adaptee.btnBuscarActionPerformed(true);
    }
    else if (evt.getActionCommand().equals("Primero")) {
      adaptee.btnPrimeroActionPerformed();
    }
    else if (evt.getActionCommand().equals("Anterior")) {
      adaptee.btnAnteriorActionPerformed();
    }
    else if (evt.getActionCommand().equals("Siguiente")) {
      adaptee.btnSiguienteActionPerformed();
    }
    else if (evt.getActionCommand().equals("Ultimo")) {
      adaptee.btnUltimoActionPerformed();
    }
  }

} // Fin clase PanConCasTubActionAdapter

// Perdidas de foco
class PanConCasTubFocusAdapter
    implements java.awt.event.FocusListener {
  PanConCasTub adaptee;
  FocusEvent evt;

  PanConCasTubFocusAdapter(PanConCasTub adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    if ( ( (TextField) evt.getSource()).getName().equals("Ano")) {
      adaptee.txtAnoFocusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("Registro")) {
      adaptee.txtRegFocusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("CodContacto")) {
      adaptee.txtCodContactoFocusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("FechaDesde")) {
      adaptee.txtFechaDesdeFocusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("FechaHasta")) {
      adaptee.txtFechaHastaFocusLost();
    }
  } // Fin focusLost()

  public void focusGained(FocusEvent e) {

    if ( ( (TextField) e.getSource()).getName().equals("Ano")) {
      adaptee.txtAnoFocusGained();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("Registro")) {
      adaptee.txtRegFocusGained();
      /*
           } else if(((TextField)e.getSource()).getName().equals("CodEnfermo")){
        adaptee.txtCodContactoFocusGained();
           } else if(((TextField)e.getSource()).getName().equals("FechaDesde")){
         adaptee.txtFechaDesdeFocusGained();
           } else if(((TextField)e.getSource()).getName().equals("FechaHasta")){
          adaptee.txtFechaHastaFocusGained();
       */
    }

  } // Fin run()

} // Fin clase PanConCasTubFocusAdapter

// Doble-click en un registro de la tabla
class PanConCasTubTablaDobleClick
    implements jclass.bwt.JCActionListener {
  PanConCasTub adaptee;

  PanConCasTubTablaDobleClick(PanConCasTub adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    adaptee.TablaDobleClickActionPerformed();
  }

} // Fin clase PanConCasTubTablaDobleClick
