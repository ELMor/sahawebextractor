package tuberculosis.cliente.panotros;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CCargadorImagen;
import capp.CDialog;
import comun.constantes;
import tuberculosis.cliente.pantotalmues.PanTotMues;
import tuberculosis.datos.panmues.DatCasMuesCS;

public class DiaTotMues
    extends CDialog {

  // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  //Aceptar == repetir query al volver
  //Cancelar == nada
  public int iOut = -1;
  public String nmCaso = "";

  final String imgNAME[] = {
      "images/aceptar.gif"};

  public int modoOperacion = constantes.modoALTA;

  ScrollPane panelScroll = new ScrollPane();
  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnSalir = new ButtonControl("Aceptar");

  PanTotMues pM = null;
  DatCasMuesCS datNotif = null;
  Hashtable hs = null;

  // constructor
  public DiaTotMues(PanOtros panOtros, int modo, String Caso) {

    super(panOtros.app);

    try {

      hs = panOtros.hash;
      nmCaso = Caso;

      jbInit();
      // E 27/01/2000
      modoOperacion = modo;
      Inicializar(modoOperacion);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void jbInit() throws Exception {
    btnSalir.setActionCommand("cancelar");

    // carga las imagenes
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnSalir.setImage(imgs.getImage(0));

    xYLayout1.setWidth(785);
    xYLayout1.setHeight(265);

    this.setLayout(xYLayout1);
    this.setSize(785, 265);

    DialogTotMactionAdapter actionAdapter = new DialogTotMactionAdapter(this);
    this.setTitle("Resultados de Laboratorio");
    this.add(btnSalir, new XYConstraints( (785 - 80) / 2, 203, -1, -1));
    btnSalir.addActionListener(actionAdapter);
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar()

  {

    switch (modoOperacion) {

      case constantes.modoALTA:
      case constantes.modoMODIFICACION:
      case constantes.modoBAJA:
      case constantes.modoCONSULTA:
        btnSalir.setEnabled(true);
        if (pM != null) {
          pM.Inicializar(modoOperacion);
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoESPERA:
        btnSalir.setEnabled(false);
        if (pM != null) {
          pM.Inicializar(modoOperacion);
        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();

  }

  public void addPanelMuestras() {
    int modo = modoOperacion;
    this.Inicializar(constantes.modoESPERA);

    pM = null;
    pM = new PanTotMues(this.app,
                        modo,
                        hs, this, nmCaso);

    this.add(pM, new XYConstraints(2, 2, 780, 200));
    pM.doLayout();

    this.Inicializar(modo);

  }

  void btnCancelar_actionPerformed() {
    //hay aue recoger cd_ope y fc_ultact
    dispose();
  }

} //fin clase

class DialogTotMactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaTotMues adaptee;
  ActionEvent evt;

  DialogTotMactionAdapter(DiaTotMues adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
} //fin class
