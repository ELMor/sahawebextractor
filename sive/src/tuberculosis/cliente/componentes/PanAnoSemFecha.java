//______________________________________________________________

/**
 * Clase: PanAnoSemFecha
 * Paquete: c_componentes
 * Autor: Luis Rivera (LRG)
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: xx/xx/xxxx
 * Descripcion: Implementacion del panel de A�o, Semana
 *   epidemiol�gica y Fecha Notificaci�n.
 * Modificaciones:
 *   19/10/1999 (JMT) Adici�n de 2 constructores que permiten especificar
 *     si la disposici�n de los componentes es vertical u horizontal
 * 01/2000 Readaptada para pasar de EDO a Centinelas:
 *
 *
 **/

package tuberculosis.cliente.componentes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CMessage;
import capp2.CPanel;
//PAquete de EDO
import obj.CFechaSimple;
//Paquetes nuevos
import tuberculosis.cliente.fechas.conversorfechas;

//import centinelas.cliente.c_comuncliente.*;

public class PanAnoSemFecha
    extends CPanel
    implements CInicializar {

  //modos de inicializacion

  public static final int modoVACIO = -1; //Inicia si a�o,semana ni fecha
  public static final int modoSIN_SEMANA = 0; //Inicia con a�o actual pero sin semana actual
  //Al cambiar a�o tampoco pondr� autom�ticamente la semana
  public static final int modoINICIO_SEMANA = 1; //Inicia con primera semana a�o actual
  public static final int modoSEMANA_ACTUAL = 2; //Inicia con semana actual

  public static final int modoINI_ANO = 8; //Inicia con el a�o que  se le indique
  public static final int modoINI_MODIFICAR = 9; //Inicia con a�o,semana que se indique en el constructor
  // Sirve para entrar en modo de modificaci�n de datos ya existenntes
  public static final int modoINI_CON_FECHA = 9; //Inicia con a�o,semana,fecha que se indique en el constructor
  // Sirve para entrar en modo de modificaci�n de datos ya existentes

  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;
  //Variables
  int modoOperacion;
  int modoContenedor; // Para guardar y recuperar el modo del contenedor de este panel

  protected ContPanAnoSemFecha pnlContenedor;

  //objeto para conversi�n de num semana a fecha y viceversa
  public conversorfechas convDesde = null;

  //Variables y objetos para recuperar datos si cambios no tienen �xito
  //Siempre almacenan datos v�lidos
  public conversorfechas convDesdeBk = null;
  public String sAnoBk = "";
  public String sCodSemBk = "";
  public String sFecSemBk = "";

  //Datos v�lidos anteriores. Se usan para poder informar de cambios
  public String sAnoBkAntiguo = "";
  public String sCodSemBkAntiguo = "";
  public String sFecSemBkAntiguo = "";
  //Para ver si ha habido cambios entre  antiguos y nuevos datos v�lidos
  protected boolean bCambioCodAno = false;
  protected boolean bCambioCodSem = false;
  protected boolean bCambioFecSem = false;

  Enumeration enum;

  int tipIni;
  boolean bFechaProtegida;

  XYLayout xYLayout1 = new XYLayout();
  public TextField txtAno = new TextField();
  public TextField txtCodSem = new TextField();
  // Cambio para barra autom�tica: ARS 22-05-01
  public CFechaSimple txtFecSem = new CFechaSimple("S");
//  public fechas.CFecha txtFecSem = new fechas.CFecha("S");

  Label lblAno = new Label();
  Label lblSem = new Label();
  Label lblFecSem = new Label();

  CApp miCApp = null;

  //_________________________________________________________________________

  //Constructor que no recibe par�metros de a�o,sem, fecha
  //Luego, si es necesario, busca datos de fecha actual
  public PanAnoSemFecha(ContPanAnoSemFecha pnl, int iModo, boolean bFechaProt) {
    //Si iModo es modoINICIO_SEMANA (1) pone inicialmente fecha de fin de primera sem epid. del a�o actual
    //Si iModo es modoSIN_SEMANA (0) no pone inicialmente semana, pero s� a�o actual
    //Si iModo es modoVACIO no pone nada inicialmente

    //Si iModo tiene otro valor se pone inicialmente la fecha de hoy
    //Si bFechaProt es true la caja de fecha no la puede tocar el usuario
    try {
      pnlContenedor = pnl;
      tipIni = iModo;
      bFechaProtegida = bFechaProt;
      miCApp = pnlContenedor.getMiCApp();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //_________________________________________________________________________

// Constructor que pone inicialmente solo el a�o
  public PanAnoSemFecha(ContPanAnoSemFecha pnl, boolean bFechaProt, String ano) {

    try {
      pnlContenedor = pnl;
      tipIni = modoINI_ANO; //Constante indica forma de inicializaci�n
      bFechaProtegida = bFechaProt;
      miCApp = pnlContenedor.getMiCApp();
      jbInit(); //No inicializar� las fechas
      //Se inicializan cajas de texto y conversor
      txtAno.setText(ano);
      sAnoBk = ano;
      convDesde = new conversorfechas(ano, miCApp);

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //_________________________________________________________________________

// Constructor usado para modificar datos. Parte con a�o y sem iniciales
  public PanAnoSemFecha(ContPanAnoSemFecha pnl, boolean bFechaProt, String ano,
                        String codSem) {

    //Pone inicialmente el a�o y cod semana indicados
    try {
      pnlContenedor = pnl;
      tipIni = modoINI_MODIFICAR; //Constante indica forma de inicializaci�n
      bFechaProtegida = bFechaProt;
      miCApp = pnlContenedor.getMiCApp();
      jbInit(); //No inicializar� las fechas
      //Se inicializan cajas de texto y conversor
      txtAno.setText(ano);
      sAnoBk = ano;
      convDesde = new conversorfechas(ano, miCApp);
      txtCodSem.setText(codSem);
      sCodSemBk = codSem;
      //Se va a por la fecha de esa semana
      txtCodSem_focusLost(null);

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

//_______________________________________________________

// Constructor usado para modificar datos. Parte con a�o,sem y fecha iniciales
  public PanAnoSemFecha(ContPanAnoSemFecha pnl, boolean bFechaProt, String ano,
                        String codSem, String fecSem) {

    //Pone inicialmente el a�o y cod semana y fecha indicados
    try {
      pnlContenedor = pnl;
      tipIni = modoINI_CON_FECHA; //Constante indica forma de inicializaci�n
      bFechaProtegida = bFechaProt;
      miCApp = pnlContenedor.getMiCApp();
      jbInit(); //No inicializar� las fechas
      //Se inicializan cajas de texto y conversor
      txtAno.setText(ano);
      sAnoBk = ano;
      convDesde = new conversorfechas(ano, miCApp);
      txtCodSem.setText(codSem);
      sCodSemBk = codSem;
      txtFecSem.setText(fecSem);
      sFecSemBk = fecSem;

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

//_______________________________________________________________________

  //Devuelve el c�digo de semana v�lido actualmente
  // (puede no coincidir temporalmente con contenido de caja de texto
  public String getCodSem() {
    //Si c�digo est� entre "01" y "09" menor que "10" necesario a�adirle el "0"
    if (sCodSemBk.length() == 1) {
      return ("0" + sCodSemBk);
    }
    else {
      return sCodSemBk;
    }
  }

  //______________________________________________________________________

  //Devuelve el c�digo de a�o v�lido actualmente
  // (puede no coincidir temporalmente con contenido de caja de texto
  public String getCodAno() {
    return sAnoBk;
  }

  //Devuelve la fecha v�lida actualmente
  // (puede no coincidir temporalmente con contenido de caja de texto
  public String getFecSem() {
    return sFecSemBk;
  }

  //Indica si hay cambio de a�o
  public boolean hayCambioCodAno() {
    return bCambioCodAno;
  }

  //Indica si hay cambio de semana
  public boolean hayCambioCodSem() {
    return bCambioCodSem;
  }

  //Indica si hay cambio de fecha de semana
  public boolean hayCambioFecSem() {
    return bCambioFecSem;
  }

//_______________________________________________________________________

  void jbInit() throws Exception {
    this.setSize(new Dimension(390, 30));
    xYLayout1.setHeight(30);
    xYLayout1.setWidth(390);

    txtFecSem.addFocusListener(new PanAnoSemFecha_txtFecSem_focusAdapter(this));
    txtCodSem.addFocusListener(new PanAnoSemFecha_txtCodSem_focusAdapter(this));
    txtAno.addFocusListener(new PanAnoSemFecha_txtAno_focusAdapter(this));

    lblFecSem.setText("Fecha Notificaci�n:");
    lblSem.setText("Semana:");
    lblAno.setText("A�o:");
    this.setLayout(xYLayout1);

    this.add(lblAno, new XYConstraints(3, 2, 34, -1));
    this.add(txtAno, new XYConstraints(36, 2, 77, -1));
    this.add(lblSem, new XYConstraints(143, 2, 51, -1));
    this.add(txtCodSem, new XYConstraints(206, 2, 43, -1));
    this.add(txtFecSem, new XYConstraints(260, 2, 97, -1));

    txtAno.setForeground(Color.black);
    txtAno.setBackground(new Color(255, 255, 150));

    txtCodSem.setForeground(Color.black);
    txtCodSem.setBackground(new Color(255, 255, 150));

    if (bFechaProtegida) {
      txtFecSem.setEditable(false);
    }
    else {
      txtFecSem.setForeground(Color.black);
      txtFecSem.setBackground(new Color(255, 255, 150));
    }

    //Inicialza fechas si es necesario
    if ( (tipIni != modoINI_MODIFICAR) && (tipIni != modoINI_CON_FECHA)
        && (tipIni != modoVACIO) && (tipIni != modoINI_ANO)) {
      inicializarFechas();

    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  //_________________________________________________________________________

  // JMT: Constructor que incluye un parametro de disposicion de los componentes
  public PanAnoSemFecha(ContPanAnoSemFecha pnl, int iModo, boolean bFechaProt,
                        boolean horizontal) {
    //Si iModo es 1 pone inicialmente fecha de fin de primera sem epid. del a�o actual
    //Si iModo tiene otro valor se pone inicialmente la fecha de hoy
    //Si bFechaProt es true la caja de fecha no la puede tocar el usuario
    try {
      pnlContenedor = pnl;
      tipIni = iModo;
      bFechaProtegida = bFechaProt;

      miCApp = pnlContenedor.getMiCApp();

      jbInit(horizontal);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //_________________________________________________________________________

  //  Constructor usado para modificar datos. Parte con datos iniciales___
  // JMT: Constructor que incluye un parametro de disposicion de los componentes
  public PanAnoSemFecha(ContPanAnoSemFecha pnl, boolean bFechaProt, String ano,
                        String codSem, boolean horizontal) {

    //Pone inicialmente el a�o y cod semana indicados

    try {
      pnlContenedor = pnl;
      tipIni = modoINI_MODIFICAR; //Constante indica forma de inicializaci�n
      bFechaProtegida = bFechaProt;
      miCApp = pnlContenedor.getMiCApp();

      jbInit(horizontal); //No inicializar� las fechas

      //Se inicializan cajas de texto y conversor
      txtAno.setText(ano);
      sAnoBk = ano;

      convDesde = new conversorfechas(ano, miCApp);
      txtCodSem.setText(codSem);
      sCodSemBk = codSem;
      //Se va a por la fecha de esa semana
      //txtCodSem_focusLost(null);

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //_________________________________________________________________________

  // Dispone los componentes en hor. o vertical segun el parametro
  void jbInit(boolean horizontal) throws Exception {
    // Vars. para colocar los componentes
    int pX = 0, pY = 0;
    int longX = 0, longY = 0;
    int dimX, dimY;

    // Medidas y organizacion del panel
    if (horizontal) {
      dimX = 515;
      dimY = 65;
    }
    else {
      dimX = 225;
      dimY = 125;
    }
    this.setSize(new Dimension(dimX, dimY));
    xYLayout1.setHeight(dimY);
    xYLayout1.setWidth(dimX);
    this.setLayout(xYLayout1);

    if (horizontal) { // Horizontal
      ColocaAno(15, 15, horizontal);
      ColocaSemana(105, 15);
      ColocaFechaNot(305, 15);
    }
    else { // Vertical
      ColocaAno(15, 15, horizontal);
      ColocaSemana(15, 45);
      ColocaFechaNot(15, 75);
    }

    this.modoOperacion = modoNORMAL;
    Inicializar();
  } // Fin jbInit()

  // Coloca los componentes relacionados con el a�o epidemiologico
  void ColocaAno(int pX, int pY, boolean hor) {
    // A�o epidemiologico
    int longX, longY;

    longY = -1;
    if (hor) {
      longX = 25;
    }
    else {
      longX = 117;

    }
    this.add(lblAno, new XYConstraints(pX, pY, longX, longY));
    if (hor) {
      pX += longX + 5;
    }
    else {
      pX += longX + 28;
    }
    longX = 40;
    txtAno = new TextField();
    this.add(txtAno, new XYConstraints(pX, pY, longX, longY));

    // Longitud total de los comps. hor = 70, vert = 190

    txtAno.addFocusListener(new PanAnoSemFecha_txtAno_focusAdapter(this));
    txtAno.setForeground(Color.black);
    txtAno.setBackground(new Color(255, 255, 150));
  } // Fin ColocaAno()

  // Coloca los componentes relacionados con la semana epidemiologica
  void ColocaSemana(int pX, int pY) {
    // Semana epidemiologica
    int longX, longY;
    longX = 140;
    longY = -1;
    this.add(lblSem, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5; /*longX = 28;*/
    longX = 40;
    txtCodSem = new TextField();
    this.add(txtCodSem, new XYConstraints(pX, pY, longX, longY));

    // Longitud total de los comps. 185

    txtCodSem.addFocusListener(new PanAnoSemFecha_txtCodSem_focusAdapter(this));
    txtCodSem.setForeground(Color.black);
    txtCodSem.setBackground(new Color(255, 255, 150));

    //Se va a por la fecha de esa semana
    txtCodSem_focusLost(null);
  } // Fin ColocaSemana()

  // Coloca los componentes relacionados con la fecha de notificacion
  void ColocaFechaNot(int pX, int pY) {
    // Fecha notificacion
    int longX, longY;
    longX = 105;
    longY = -1;
    this.add(lblFecSem, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 75;
    // Cambio para barrado autom�tico ARS 22-05-01
    txtFecSem = new CFechaSimple("N");
    this.add(txtFecSem, new XYConstraints(pX, pY, longX, longY));

    txtFecSem.addFocusListener(new PanAnoSemFecha_txtFecSem_focusAdapter(this));
    if (bFechaProtegida) {
      txtFecSem.setEditable(false);
    }
    else {
      txtFecSem.setForeground(Color.black);
      txtFecSem.setBackground(new Color(255, 255, 150));
    }

    if ( (tipIni != modoINI_MODIFICAR) && (tipIni != modoINI_CON_FECHA)
        && (tipIni != modoVACIO)) {
      inicializarFechas();

    }
  } // Fin ColocaFechaNot()

  // Fin JMT

//________________________________________________________________________

  //No hace nada : Est� por compatibilidad, por ser un CPanel
  public void Inicializar() {
  }

//________________________________________________________________________

  // configura los controles seg�n el modo de operaci�n en este panel
  public void Inicializate(int modo) {

    switch (modo) {
      case CInicializar.NORMAL:
        txtAno.setEnabled(true);
        txtCodSem.setEnabled(true);
        if (!bFechaProtegida) {
          txtFecSem.setEnabled(true);
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case CInicializar.ESPERA:
        txtAno.setEnabled(false);
        txtCodSem.setEnabled(false);
        if (!bFechaProtegida) {
          txtFecSem.setEnabled(false);
        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

  }

  //_________________________________________________________________________

  //Orden de Inicializar en modo Espera o Normal el contenedor
  public void Inicializar(int modo) {
    if (this.pnlContenedor != null) {
      this.pnlContenedor.Inicializar(modo);
    }
  }

  //_________________________________________________________________________

  //M�todo llamado solo en el constructor de este panel
  void inicializarFechas() {
    String sFecActual;
    java.util.Date fecActual = null;
    String sFecIni;
    String sAnoDesde;
    int resul;
    CMessage msgBox;

    try {

      //Obtiene la fecha actual
      fecActual = new java.util.Date();

      //Pasa la fecha actual de Date a String
      SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
      sFecActual = formater.format(fecActual);

      txtFecSem.setText(sFecActual.trim());

      //Si la fecha no ha cambiado no hace nada
      if (txtFecSem.getText().equals(sFecSemBk)) {
        return;
      }

      //valida el dato
      txtFecSem.ValidarFecha();
      //Si no es v�lido recupera el antiguo
      if (txtFecSem.getValid().equals("N")) {
        txtFecSem.setText(sFecSemBk);
        return;
      }
      //Si es v�lido le da el formato adecuado
      else if (txtFecSem.getValid().equals("S")) {
        txtFecSem.setText(txtFecSem.getFecha());

      }

      //Se trae el a�o de fecha actual
      sFecIni = txtFecSem.getText();
      convDesde = conversorfechas.getConvDeFecha(sFecIni, miCApp);

      // si a�o no es v�lido recupera todos los datos y muestra mensaje
      if (convDesde == null) {
        convDesde = convDesdeBk;
        txtAno.setText(sAnoBk);
        txtCodSem.setText(sCodSemBk);
        txtFecSem.setText(sFecSemBk);
        msgBox = new CMessage(miCApp, CMessage.msgAVISO,
                              "No hay datos de ese a�o epidemiol�gico");
        msgBox.show();
        msgBox = null;
      }
      else {
        // si es v�lido rellena campo de a�o y guarda datos del a�o para posible recuperaci�n
        //NO se vac�an las cajas de semana
        convDesdeBk = convDesde;
        sAnoDesde = convDesde.getAno();
        txtAno.setText(sAnoDesde);
        sAnoBk = sAnoDesde;

      }
      //si no hay un a�o v�lido todav�a no hace nada y sale
      if (sAnoBk == "") {
        return;
      }

      /// Vamos a por la semana y fecha

      //Si tipo de inicial. es modoSIN_SEMANA deja vac�as cajas de semana y fecha
      if (tipIni == modoSIN_SEMANA) {
        txtCodSem.setText("");
        sCodSemBk = "";
        txtFecSem.setText("");
        sFecSemBk = "";
      }
      //Si tipo de inicial. es 1 pone primer dia y semana del a�o desde
      else if (tipIni == modoINICIO_SEMANA) {
        txtCodSem.setText("1");
        sCodSemBk = txtCodSem.getText();
        txtFecSem.setText(convDesde.getFecFinPriSem());
        sFecSemBk = txtFecSem.getText();
      }

      else {
        //Si se inicializa el panel con otro valor se pone la fecha de hoy

        //indeptemente de si he traido a�o o sigue el que habia, busca semana:
        resul = convDesde.getNumSem(txtFecSem.getText());

        //Si esa semana no es v�lida recupera los datos anteriores de semana y muestra un mensaje
        if (resul == -1) {
          txtCodSem.setText(sCodSemBk);
          txtFecSem.setText(sFecSemBk);
          msgBox = new CMessage(miCApp, CMessage.msgAVISO,
              "Esa fecha no pertenece al a�o epidemiol�gico indicado");
          msgBox.show();
          msgBox = null;
        }
        else {
          txtCodSem.setText(Integer.toString(resul));
          sCodSemBk = txtCodSem.getText();
          sFecSemBk = txtFecSem.getText();
        }
      }
      // envia la excepci�n
    }
    catch (Exception e) {
      //#System.Out.println(e.getMessage() );
      e.printStackTrace();
    }
  }

  //_________________________________________________________________________

  //Cambio de semana externo (se supone es semana existente en SIVE_SEMEPI_RMC
  public void setCodSem(String codSem) {

    //Se pone el c�digo de semana sin el "0", si lo ten�a
    txtCodSem.setText(Integer.toString(Integer.parseInt(codSem)));

    guardarDatosValidos();
    //Ejecuta acciones correspondientes a p�rdida foco
    semanaPierdeFoco();
    //Se registran cambios
    registrarCambiosEnDatosValidos();
    //Ejecuta m�todo del interfaz
    // (pues aunque no haya p�rdida de foco puede haber cambiado semana
    pnlContenedor.alPerderFocoSemana();
  }

  //_________________________________________________________________________

  void txtCodSem_focusLost(FocusEvent evt) {
    guardarDatosValidos();
    //Ejecuta acciones correspondientes a p�rdida foco
    semanaPierdeFoco();
    //Se registran cambios
    registrarCambiosEnDatosValidos();
    //Ejecuta m�todo del interfaz
    pnlContenedor.alPerderFocoSemana();
  }

  //_________________________________________________________________________

  public void semanaPierdeFoco() {

//#System.Out.println("evento en codSem");
    int numero;
    String resul;
    CMessage msgBox;
    try {

      txtCodSem.setText(txtCodSem.getText().trim());

      //si no ha cambiado el valor de la caja de texto no hace nada
      if (txtCodSem.getText().equals(sCodSemBk)) {
        return;
      }

      //chequea cod semana.Debe ser entero entre 1 y 53
      //Si no es ocrrecto o no hay a�o v�lido recupera el valor antiguo y ya est�
      if (
          (sAnoBk == "") || (ChequearEntero(txtCodSem.getText(), 0, 54, 2) == false)
          ) {
        txtCodSem.setText(sCodSemBk);
        return;
      }

      this.modoOperacion = modoESPERA;
      Inicializar();
      pnlContenedor.Inicializar(CInicializar.ESPERA);

      //Si hay a�o valido
      numero = Integer.parseInt(txtCodSem.getText());
      resul = convDesde.getFec(numero);
      //Si no es num semana v�lido recupera los datos anteriores y muestra mensaje
      if (resul.equals("")) {
        txtCodSem.setText(sCodSemBk);
        txtFecSem.setText(sFecSemBk);
        msgBox = new CMessage(miCApp, CMessage.msgAVISO,
            "Esa semana no pertenece al a�o epidemiol�gico indicado");
        msgBox.show();
        msgBox = null;

      }
      else {
        //si es semana v�lida actualiza la fecha de fin de sem y guarda los datos de reserva
        txtFecSem.setText(resul);
        sCodSemBk = txtCodSem.getText();
        sFecSemBk = resul;

      }

      // envia la excepci�n
    }
    catch (Exception e) {
      //#System.Out.println(e.getMessage() );
      e.printStackTrace();

    }

    this.modoOperacion = modoNORMAL;
    Inicializar();
    pnlContenedor.Inicializar(CInicializar.NORMAL);

    //#System.Out.println(txtFecSem.getText());
  }

  //_________________________________________________________________________

  public void txtFecSem_focusLost(FocusEvent evt) {

    guardarDatosValidos();
    //Ejecuta acciones correspondientes a p�rdida foco
    fechaPierdeFoco();
    //Se registran cambios
    registrarCambiosEnDatosValidos();
    //Ejecuta m�todo del interfaz
    pnlContenedor.alPerderFocoFecha();

  }

  void fechaPierdeFoco() {

    java.util.Date dFecha;
    Calendar calen = new GregorianCalendar();
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    int ano;

    String sFecIni;
    String sAnoDesde; //A�o que se obttiene a partir de fecha indicada por usuario
    int resul;
    CMessage msgBox;

    try {

      txtFecSem.setText(txtFecSem.getText().trim());

      //Si la fecha no ha cambiado no hace nada
      if (txtFecSem.getText().equals(sFecSemBk)) {
        return;
      }

      //valida el dato
      txtFecSem.ValidarFecha();
      //Si no es v�lido recupera el antiguo
      if (txtFecSem.getValid().equals("N")) {
        txtFecSem.setText(sFecSemBk);
        return;
      }
      //Si es v�lido le da el formato adecuado
      else if (txtFecSem.getValid().equals("S")) {
        txtFecSem.setText(txtFecSem.getFecha());
      }

      //Pasa el string de fecha a Date
      dFecha = formater.parse(txtFecSem.getText());
      //obtiene el a�o cronol�gico correspondiente a esa fecha
      calen.setTime(dFecha);
      ano = calen.get(Calendar.YEAR);

      // trae fechas del a�o epidemiol�gico de b.datos si es necesario
      //Si el a�o cronol�gico ha cambiado en fecha
      //o fecha es de �ltimos de diciembre
      //o fecha es de principios de enero
      if ( (! (Integer.toString(ano).equals(sAnoBk))) ||
          ( (calen.get(Calendar.MONTH) == Calendar.DECEMBER) &&
           (calen.get(Calendar.DAY_OF_MONTH) > 23)) ||
          ( (calen.get(Calendar.MONTH) == Calendar.JANUARY) &&
           (calen.get(Calendar.DAY_OF_MONTH) < 9))
          ) {

        //Se va a por el a�o epidemiol�gico  (podr� coincidir con el cronol�gico o no )
        this.modoOperacion = modoESPERA;
        Inicializar();
        pnlContenedor.Inicializar(CInicializar.ESPERA); //^^^^
        sFecIni = txtFecSem.getText();
        convDesde = conversorfechas.getConvDeFecha(sFecIni, miCApp);

        this.modoOperacion = modoNORMAL;
        Inicializar();
        pnlContenedor.Inicializar(CInicializar.NORMAL);

        // si a�o no es v�lido recupera todos los datos y muestra mensaje
        if (convDesde == null) {
          convDesde = convDesdeBk;
          txtAno.setText(sAnoBk);
          txtCodSem.setText(sCodSemBk);
          txtFecSem.setText(sFecSemBk);
          msgBox = new CMessage(miCApp, CMessage.msgAVISO,
                                "No hay datos de ese a�o epidemiol�gico");
          msgBox.show();
          msgBox = null;
        }
        // si es v�lido rellena campo de a�o y guarda datos  para posible recuperaci�n
        //NO se vac�an las cajas de semana
        else {
          convDesdeBk = convDesde;
          sAnoDesde = convDesde.getAno();
          txtAno.setText(sAnoDesde);
          sAnoBk = sAnoDesde;
        }
      }

      //si no hay un a�o v�lido todav�a no hace nada y sale
      if (sAnoBk == "") {
        return;
      }

      //indeptemente de si he traido a�o o sigue el que habia, busca semana:
      resul = convDesde.getNumSem(txtFecSem.getText());

      //Si esa semana no es v�lida recupera los datos anteriores de semana y muestra un mensaje
      if (resul == -1) {
        txtCodSem.setText(sCodSemBk);
        txtFecSem.setText(sFecSemBk);
        msgBox = new CMessage(miCApp, CMessage.msgAVISO,
            "Esa fecha no pertenece al a�o epidemiol�gico indicado");
        msgBox.show();
        msgBox = null;
      }
      else {
        //Si semana es v�lida guarda los datos y muestra su c�d de semana
        txtCodSem.setText(Integer.toString(resul));
        sCodSemBk = txtCodSem.getText();
        sFecSemBk = txtFecSem.getText();
      }

      // envia la excepci�n
    }
    catch (Exception e) {
      //#System.Out.println(e.getMessage() );
      e.printStackTrace();
    }

  }

  // Inhabilita todos los objetos y pone el cursor en modo de espera. */
 public void setModoEspera() {
   modoOperacion = modoESPERA;
   Inicializar();
 }

  // Restaura el estado anterior del panel y pone el cursor en modo normal. */
 public void setModoNormal() {
   modoOperacion = modoNORMAL;
   Inicializar();
 }

  //___________________________________________________________________

  //guarda datos v�lidos para luego ver si hay cambios
  void guardarDatosValidos() {

    sAnoBkAntiguo = sAnoBk;
    sCodSemBkAntiguo = sCodSemBk;
    sFecSemBkAntiguo = sFecSemBk;
  }

  //_________________________________________________________________________

  //Se registran los cambios en los nuevos datos v�lidos en las var booleanas
  void registrarCambiosEnDatosValidos() {

    if (sAnoBk.equals(sAnoBkAntiguo)) {
      bCambioCodAno = false;
    }
    else {
      bCambioCodAno = true;

    }
    if (sCodSemBkAntiguo.equals(sCodSemBk)) {
      bCambioCodSem = false;
    }
    else {
      bCambioCodSem = true;

    }
    if (sFecSemBkAntiguo.equals(sFecSemBk)) {
      bCambioFecSem = false;
    }
    else {
      bCambioFecSem = true;
    }
  }

  //_________________________________________________________________________

  void txtAno_focusLost(FocusEvent e) {

    guardarDatosValidos();
    //Ejecuta acciones correspondientes a p�rida foco
    anoPierdeFoco();
    //Se registran cambios
    registrarCambiosEnDatosValidos();
    //Ejecuta m�todo del interfaz
    pnlContenedor.alPerderFocoAno();

  }

  void anoPierdeFoco() {
    CMessage msgBox;
    String sAnoDesde;

    try {

      txtAno.setText(txtAno.getText().trim());

      //Si el a�o no ha cambiado no hace nada
      if (txtAno.getText().equals(sAnoBk)) {
        return;
      }

      //chequea el a�o.Debe ser entero entre 0 y 9999
      //Si no es ocrrecto recupera el valor antiguo y ya est�
      if (ChequearEntero(txtAno.getText(), 0, 9999, 2) == false) {
        txtAno.setText(sAnoBk);
        return;
      }

      this.modoOperacion = modoESPERA;
      Inicializar();
      pnlContenedor.Inicializar(CInicializar.ESPERA);

      //objeto para conversi�n de num semana a fecha y viceversa
      sAnoDesde = txtAno.getText();
      convDesde = new conversorfechas(sAnoDesde, miCApp);

      //recupera datos y muestra mensaje si a�o no es v�lido
      if (convDesde.anoValido() == false) {
        convDesde = convDesdeBk;

        txtAno.setText(sAnoBk);
        txtCodSem.setText(sCodSemBk);
        txtFecSem.setText(sFecSemBk);
        msgBox = new CMessage(miCApp, CMessage.msgAVISO,
                              "No hay datos de ese a�o epidemiol�gico");
        msgBox.show();
        msgBox = null;
      }
      // si es v�lido vacia cajas de semana y guarda datos del a�o para posible recuperaci�n
      else {

        convDesdeBk = convDesde;
        sAnoBk = txtAno.getText();

        //Si tipo de inicial. es modoSIN_SEMANA deja vac�as cajas de semana y fecha
        if (tipIni == modoSIN_SEMANA) {
          txtCodSem.setText("");
          sCodSemBk = "";
          txtFecSem.setText("");
          sFecSemBk = "";
        }
        else {
          //Primer dia y semana del a�o Desde
          txtCodSem.setText("1");
          sCodSemBk = txtCodSem.getText();
          txtFecSem.setText(convDesde.getFecFinPriSem());
          sFecSemBk = txtFecSem.getText();
        }
      } //Fin caso a�o v�lido

      // envia la excepci�n
    }
    catch (Exception ex) {
      //#System.Out.println(ex.getMessage() );
      ex.printStackTrace();
    }

    this.modoOperacion = modoNORMAL;
    Inicializar();
    pnlContenedor.Inicializar(CInicializar.NORMAL);

  }

  //_________________________________________________________________________

//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
  boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
    String cChar = "";
    String sString = sDat;
    int uTipo = uFlag;
    boolean b = true;

    //si chequeamos un string que admita vacio.
    if (uTipo == 1) {
      if (sString.equals("")) {
        b = true;
      }
    }
    //si chequeamos un string que no admita vacio.
    if (uTipo == 2) {
      if (sString.equals("")) {
        b = false;
      }
    }
    //comprobar es un numero
    for (int i = 0; i < sString.length(); i++) {
      cChar = sString.substring(i, i + 1);
      try {
        Integer IChar = new Integer(cChar);
      }
      catch (Exception e) { // si no es un numero
        b = false;
      }

    } //fin del for

    //comprobar valor
    if (b) {
      Integer Imin = new Integer(sString);
      Integer Imax = new Integer(sString);

      if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
        b = false;
      }
    }

    return (b);
  } //fin de ChequearEntero

  //_________________________________________________________________________

  public boolean isDataValid() {

    boolean b = false;
    //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
    if ( (!sAnoBk.equals("")) &&
        (!sCodSemBk.equals("")) &&
        (!sFecSemBk.equals("")) &&
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos v�lidos
        (txtAno.getText().trim().equals(sAnoBk)) &&
        (txtCodSem.getText().trim().equals(sCodSemBk)) &&
        (txtFecSem.getText().trim().equals(sFecSemBk))

        ) {
      return true;
    }
    else {
      return false;
    }

  }

  //_________________________________________________________________________

  //Devuelve fecha de �ltima semana
  public int getNumUltSem() {
    return (convDesde.getNumUltSem());
  }

} //clase

//___________ CLASES DE EVENTOS_______________________________________________
//_________________________________________________________________________

class PanAnoSemFecha_txtCodSem_focusAdapter
    extends java.awt.event.FocusAdapter {
  PanAnoSemFecha adaptee;

  PanAnoSemFecha_txtCodSem_focusAdapter(PanAnoSemFecha adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtCodSem_focusLost(e);
  }
}

class PanAnoSemFecha_txtFecSem_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  FocusEvent e = null;
  PanAnoSemFecha adaptee;

  PanAnoSemFecha_txtFecSem_focusAdapter(PanAnoSemFecha adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtFecSem_focusLost(e);
  }
}

class PanAnoSemFecha_txtAno_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanAnoSemFecha adaptee;
  FocusEvent e = null;

  PanAnoSemFecha_txtAno_focusAdapter(PanAnoSemFecha adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtAno_focusLost(e);
  }
}
