package tuberculosis.cliente.mantus;

import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CCodigo;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPanel;
import comun.constantes;
//import java.net.*;
import sapp2.Data;
import sapp2.Lista;
import tuberculosis.datos.mantus.DataUsuario;

/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de usuarios.
 */

public class PanMantUsuTub
    extends CPanel
    implements CInicializar, CFiltro { //CPanel

  // C�digo de la aplicaci�n
  private String sCodAplicacion = null;

  // modos de operaci�n
  final int modoSELECCION_COD = constantes.sSELECCION_X_CODIGO;
  final int modoSELECCION_DES = constantes.sSELECCION_X_DESCRIPCION;

  final public int MODIFICACION = 1;

  XYLayout xYLayout1 = new XYLayout();
  CCodigo txtCod = new CCodigo();
  TextField txtDes = new TextField();
  CListaMantenimiento clmMantenimiento = null;
  ButtonControl btnBuscar = new ButtonControl();
  CheckboxControl chckCod = new CheckboxControl();
  CheckboxControl chckDes = new CheckboxControl();
  CheckboxGroup chkboxGrupo = new CheckboxGroup();

  // Servlet
  private final String strSERVLET_SELUSUTUB = constantes.strSERVLET_SELUSUTUB;

  // Comunicaci�n con el servlet
  private sapp.StubSrvBD stubCliente = null;

  //Ambito usuario
  public Vector AmbUsu = new Vector();

  // Valores devueltos
  capp.CLista data = null;

  // filtro
  Label label1 = new Label();

  // constructor del panel PanMant
  public PanMantUsuTub(CApp a) {

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {
      setApp(a);

      // C�digo de la aplicaci�n
      sCodAplicacion = this.app.getParametro("COD_APLICACION");

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar petici�n",
                                     true,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("C�digo",
                                      "135",
                                      "COD_USUARIO"));

      vLabels.addElement(new CColumna("Nombre",
                                      "475",
                                      "DS_NOMBRE"));

      clmMantenimiento = new CListaMantenimiento(a,
                                                 vLabels,
                                                 vBotones,
                                                 this,
                                                 this,
                                                 250,
                                                 650);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Lista primeraPagina() {
    Lista l = new Lista();
    DiaMantUsuTub dusu = null;
    Data d = null;
    DataUsuario du = null;

    for (int i = 0; i < data.size(); i++) {
      d = new Data();
      du = (DataUsuario) data.elementAt(i);

      d.put("COD_USUARIO", du.getCodUsu());
      d.put("DS_NOMBRE", du.getDesNom());
      d.put("IT_PERFIL_USU", du.getPer());
      boolean bEnf = du.getEnf();
      if (bEnf) {
        d.put("IT_FG_ENFERMO", "S");
      }
      else {
        d.put("IT_FG_ENFERMO", "N");
      }
      /*
             Vector vAmbUsu = du.getAmbUsu();
             d.put("NIVELES", vAmbUsu);
       */
      /*
             Vector vAmbUsu = du.getAmbUsu();
             Vector Niv1 = new Vector();
             Vector Niv2 = new Vector();
             int k = vAmbUsu.size();
             if (k > 0){
             int j = 0;
         while (j< k/2 + 1){
            Niv1.addElement(vAmbUsu.elementAt(j));
            Niv2.addElement(vAmbUsu.elementAt(j + 1));
            j+=2;
         }
         d.put("NIV1", Niv1);
         d.put("NIV2", Niv2);
             }
       */
      l.addElement(d);
    }

    return l;
  }

  // Para poder heredar de CApp
  public Lista siguientePagina() {
    return new Lista();
  }

  // inicia el aspecto del panel PanMant
  public void jbInit() throws Exception {

    // Escuchadores de eventos

    PanMant_actionAdapter actionAdapter = new PanMant_actionAdapter(this);
    CListaMantItemAdapter itemAdapter = new CListaMantItemAdapter(this);

    chckCod.setLabel("C�digo");
    chckCod.setCheckboxGroup(chkboxGrupo);
    chckCod.addItemListener(itemAdapter);
    chckCod.setState(true);
    chckDes.setLabel("Nombre");
    chckDes.setCheckboxGroup(chkboxGrupo);
    chckDes.setState(false);
    chckDes.addItemListener(itemAdapter);

    xYLayout1.setWidth(725);
    xYLayout1.setHeight(380);

    txtCod.setName("codigo");
    txtDes.setName("descripcion");

    txtCod.setVisible(true);
    txtDes.setVisible(false);

    btnBuscar.setActionCommand("buscar");
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(actionAdapter);

    this.setLayout(xYLayout1);
    this.add(txtCod, new XYConstraints(103, 6, 312, -1));
    this.add(txtDes, new XYConstraints(103, 6, 312, -1));
    this.add(chckCod, new XYConstraints(30, 36, 130, -1));
    this.add(chckDes, new XYConstraints(212, 36, 130, -1));
    this.add(btnBuscar, new XYConstraints(548, 36, 79, -1));
    this.add(clmMantenimiento, new XYConstraints(7, 80, 710, 294));
    this.add(label1, new XYConstraints(37, 6, 55, -1));

    final String imgBUSCAR = "images/refrescar.gif";
    label1.setText("Usuario:");
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));

    // tool tips

    new CContextHelp("Obtener usuarios", btnBuscar);
  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  // gesti�n de los botones
  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("buscar")) {
      Inicializar(CInicializar.ESPERA);
      try {
        hazBusqueda();
      }
      catch (Exception exc) {
      }
      Inicializar(CInicializar.NORMAL);
    }
  }

  // operaciones de la botonera
  public void realizaOperacion(int MODIFICACION) {

    Lista lisPet = this.clmMantenimiento.getLista();
    DiaMantUsuTub dm = null;
    Data dPetMod = new Data();
    Lista v1 = new Lista();
    Lista v2 = new Lista();
    DataUsuario dusu = null;

    //switch (j) {
    //case MODIFICACION:

    dPetMod = clmMantenimiento.getSelected();
    if (dPetMod != null) {

      int ind = clmMantenimiento.getSelectedIndex();
      dusu = (DataUsuario) data.elementAt(ind);
      AmbUsu = dusu.getAmbUsu();

      dm = new DiaMantUsuTub(this.getApp(), 1, dPetMod, AmbUsu);

      dm.show();
      if (dm.bAceptar()) {
        try {
          hazBusqueda();
        }
        catch (Exception exc) {
          System.out.println(exc.getMessage());
        }
      }
    }
    else {
      this.getApp().showAdvise("Debe seleccionar un proveedor en la tabla.");
    }

    //    break;
    //}
  }

  void chkItemStateChanged(ItemEvent e) {
    if (e.getItem().equals("C�digo")) {
      txtCod.setVisible(true);
      txtCod.setText(txtDes.getText().trim().toUpperCase());
      txtDes.setVisible(false);
    }
    else {
      txtCod.setVisible(false);
      txtDes.setVisible(true);
      txtDes.setText(txtCod.getText().trim());
    }
    doLayout();
  }

  private void hazBusqueda() throws Exception {
    int modoLlamada = -1;
    DataUsuario du = new DataUsuario();

    data = new capp.CLista();

    if (chckCod.isChecked()) {
      modoLlamada = modoSELECCION_COD;
      du.setCodUsu(txtCod.getText().trim());
    }
    else {
      modoLlamada = modoSELECCION_DES;
      du.setDesNom(txtDes.getText().trim());
    }

    du.setCodApl(sCodAplicacion);

    data.addElement(du);

    stubCliente = new sapp.StubSrvBD();

    stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") +
                                        strSERVLET_SELUSUTUB));
    data = (capp.CLista) stubCliente.doPost(modoLlamada, data);

    /*
         tuberculosis.servidor.mantus.SrvSelUsuTub servlet = new tuberculosis.servidor.mantus.SrvSelUsuTub();
         //Indica como conectarse a la b.datos
         servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
         data = (capp.CLista) servlet.doDebug(modoLlamada, data);*/

    if (data != null) {
      if (data.size() > 0) {
        clmMantenimiento.setPrimeraPagina(this.primeraPagina());
      }
      else {
        clmMantenimiento.vaciarPantalla();
        this.getApp().showAdvise("El usuario no existe");
      }
    }

  }
}

// botones
class PanMant_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanMantUsuTub adaptee;
  ActionEvent e;

  PanMant_actionAdapter(PanMantUsuTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

// control de cambio de buscar por c�digo/descripci�n
class CListaMantItemAdapter
    implements java.awt.event.ItemListener {
  PanMantUsuTub adaptee;

  CListaMantItemAdapter(PanMantUsuTub adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkItemStateChanged(e);
  }
}
