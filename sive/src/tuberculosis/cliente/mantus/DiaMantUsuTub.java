package tuberculosis.cliente.mantus;

import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CMessage;
import comun.DataEnfermo;
import comun.constantes;
//import tuberculosis.servidor.mantus.*;
import enfermotub.SrvEnfermo;
import sapp2.Lista;
import tuberculosis.datos.mantus.DataUsuario;

public class DiaMantUsuTub
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final static public int MODIFICACION = 1;

  //modo de entrada en el Servlet
  final int modoMOD = constantes.modoMODIFICACION;

  //Variables para los items del choice (dependen del idioma)
  String strSERVICIOS_CENTRALES = "Servicios Centrales";
  String strEPIDEMIOLOGO_CA = "Epidemi�logo de C.A.";
  String strEPIDEMIOLOGO_NIVEL1 = "Epidemi�logo de Area";
  String strEPIDEMIOLOGO_NIVEL2 = "Epidemi�logo de Distrito";
  String strFUENTE_NOTIFICADORA = "Notificador";

  // componentes
  XYLayout xyLyt = new XYLayout();

  Label lblUsuario = new Label();
  Label lblNombre = new Label();
  TextField txtUsu = new TextField();
  TextField txtNom = new TextField();
  ButtonControl btnGrabar = new ButtonControl();
  ButtonControl btnSalir = new ButtonControl();
  Choice chkPerfil = new Choice();
  Label lblAmbUsu = new Label();
  java.awt.List lstAmbUsu = new java.awt.List();
  Label lblPerfil = new Label();
  Checkbox chkbxEnf = new Checkbox();
  Label lblEnf = new Label();

  // Servlet
  private final String strSERVLET_MODUSUTUB = constantes.strSERVLET_SELUSUTUB;

  // Comunicaci�n con el servlet
  private sapp.StubSrvBD stubCliente = null;

  // modo de apertura de la ventana
  public int modoApertura;

  public int modoOperacion;

  public sapp2.Data dtSel = null;

  // Valores devueltos
  capp.CLista vLista = null;
  capp.CLista v = null;
  capp.CLista parametros = null;
  capp.CLista result = null;

  // control aceptar/cancelar
  private boolean bAceptar = false;

  // lista de �mbito de usuario
  boolean lstAmbUsuEnabled = false;

  //par�metros
  protected capp.CLista listaNiv1 = null; //Lista de c�digos nivel 1
  protected capp.CLista listaNiv2 = null; //Lista de c�digos nivel 2
  protected capp.CLista listaNiv = null; //Lista de c�digos nivel 2
  protected String strCode = null; // es el nombre del campo code de la lista seleccionada
  public Vector vAmbUsu = new Vector(); //Strings elegidos del ambito
  protected Vector vAmbUsuSel = new Vector(); //Strings elegidos del ambito

  // Datos varios
  private Lista lsPerfil = new Lista();

  /**
   * Constructor
   */
  public DiaMantUsuTub(CApp a,
                       int modo,
                       sapp2.Data dtReg, Vector AmbUsuSel) {
    super(a);

    modoApertura = modo;

    vAmbUsuSel = AmbUsuSel;

    dtSel = dtReg;

    new Thread(new LeerNiveles(this)).start();

    /*
         trasSeleccionarPerfil(perSel);
         seleccionarAmbitos();
     */
    try {

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    //Inicializar(CInicializar.NORMAL);
  }

  void jbInit() throws Exception {
    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";

    DiaMantUsuTub_actionAdapter actionAdapter = new DiaMantUsuTub_actionAdapter(this);
    DiaMantUsuTubItemListener lstItemListener = new DiaMantUsuTubItemListener(this);
    xyLyt.setHeight(291);
    xyLyt.setWidth(559);
    this.setLayout(xyLyt);
    this.setSize(559, 291);

    // etiquetas
    lblUsuario.setText("Usuario:");
    lblPerfil.setText("Perfil:");
    chkbxEnf.addItemListener(new DiaMantUsuTub_chkbxEnf_itemAdapter(this));
    lblNombre.setText("Nombre:");
    lblAmbUsu.setText("Ambito de usuario:");
    lstAmbUsu.setName("lstAmbUsu");
    lstAmbUsu.setMultipleMode(true);
    lblEnf.setText("Visualizar datos identificactivos de enfermos");
    btnGrabar.setLabel("Aceptar");
    btnSalir.setLabel("Cancelar");

    btnGrabar.setActionCommand("Grabar");
    btnSalir.setActionCommand("Salir");

    btnGrabar.addActionListener(actionAdapter);
    btnSalir.addActionListener(actionAdapter);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);

    this.getApp().getLibImagenes().CargaImagenes();
    btnGrabar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnSalir.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    chkPerfil.addItemListener(new DiaMantUsuTub_chkPerfil_itemAdapter(this));
    lstAmbUsu.addItemListener(lstItemListener);

    chkPerfil.addItem(strSERVICIOS_CENTRALES);
    chkPerfil.addItem(strEPIDEMIOLOGO_CA);
    chkPerfil.addItem(strEPIDEMIOLOGO_NIVEL1);
    chkPerfil.addItem(strEPIDEMIOLOGO_NIVEL2);
    chkPerfil.addItem("");

    //chkPerfil.addItem(strFUENTE_NOTIFICADORA);

    // inicializaci�n en funci�n del modo de apertura
    switch (modoApertura) {
      case MODIFICACION:

        rellenaDatos();

        setTitle("Modificaci�n de un usuario");

        Inicializar(CInicializar.NORMAL);

        break;
    }

    chkPerfil.setBackground(new Color(255, 255, 150));
    txtNom.setBackground(new Color(255, 255, 150));
    // pinta los componentes
    this.add(lblUsuario, new XYConstraints(10, 17, 53, 21));
    this.add(txtUsu, new XYConstraints(129, 17, 101, -1));
    this.add(lblNombre, new XYConstraints(244, 17, 57, 21));
    this.add(txtNom, new XYConstraints(302, 16, 230, -1));
    this.add(chkPerfil, new XYConstraints(130, 57, 198, 21));
    this.add(lblAmbUsu, new XYConstraints(10, 96, 104, 21));
    this.add(lstAmbUsu, new XYConstraints(130, 91, 198, 60));
    this.add(lblPerfil, new XYConstraints(10, 57, 64, 21));
    this.add(chkbxEnf, new XYConstraints(10, 160, -1, -1));
    this.add(lblEnf, new XYConstraints(44, 160, 255, 21));
    this.add(btnGrabar, new XYConstraints(374, 210, -1, -1));
    this.add(btnSalir, new XYConstraints(465, 210, -1, -1));

  }

  // rellena los datos
  private void rellenaDatos() {

    String sUsu = dtSel.getString("COD_USUARIO");
    String sNom = dtSel.getString("DS_NOMBRE");
    String sPer = dtSel.getString("IT_PERFIL_USU");
    String sEnf = dtSel.getString("IT_FG_ENFERMO");

    txtUsu.setText(sUsu);
    txtNom.setText(sNom);
    if (sEnf == "S") {
      chkbxEnf.setState(true);
    }

    if (sPer.equals("1")) {
      chkPerfil.select(0);
    }
    else if (sPer.equals("2")) {
      chkPerfil.select(1);
    }
    else if (sPer.equals("3")) {
      chkPerfil.select(2);
    }
    else if (sPer.equals("4")) {
      chkPerfil.select(3);
    }
    else if (sPer.equals("")) {
      chkPerfil.select(4);
    }

    //String perSel = dtSel.getString("IT_PERFIL_USU");
    trasSeleccionarPerfil();
    vAmbUsu = vAmbUsuSel;
    seleccionarAmbitos();
  }

  private void RecogeDatos() {
    DataUsuario du = new DataUsuario();
    String Usu = txtUsu.getText();
    du.setCodUsu(Usu);
    String Nom = txtNom.getText();
    du.setDesNom(Nom);
    if (chkPerfil.getSelectedIndex() == 0) {
      du.setPer("1");
    }
    else if (chkPerfil.getSelectedIndex() == 1) {
      du.setPer("2");
    }
    else if (chkPerfil.getSelectedIndex() == 2) {
      du.setPer("3");
    }
    else if (chkPerfil.getSelectedIndex() == 3) {
      du.setPer("4");
    }

    du.setEnf(chkbxEnf.getState());
    int h = vAmbUsu.size();
    ponerAmbitosEnVector();

    //if (vAmbUsu.size()!=null){
    du.setAmbUsu(vAmbUsu);
//      }
    du.setCodComAut(app.getParametro("CA"));
    du.setCodApl(app.getParametro("COD_APLICACION"));
    vLista = new capp.CLista();
    vLista.addElement(du);
  }

  public void Inicializar() {
  }

  public boolean bAceptar() {
    return bAceptar;
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {

    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        switch (modoApertura) {

          case MODIFICACION:
            this.setEnabled(true);
            txtUsu.setEnabled(false);
            txtNom.setEnabled(false);
            lstAmbUsu.setEnabled(lstAmbUsuEnabled);
            if (lstAmbUsuEnabled) {
              lstAmbUsu.setBackground(Color.white);
            }
            else {
              lstAmbUsu.setBackground(Color.gray);
            }
            break;
        }
        break;
    }
  }

  public synchronized void setListaNivel1(capp.CLista a_lista) {
    listaNiv1 = a_lista;
  }

  public synchronized capp.CLista getListaNivel1() {
    return listaNiv1;
  }

  public synchronized void setListaNivel2(capp.CLista a_lista) {
    listaNiv2 = a_lista;
  }

  public synchronized capp.CLista getListaNivel2() {
    return listaNiv2;
  }

  /** cambios en la lista  */
  void lsAmbUsu_itemStateChanged(ItemEvent evt) {
  }

  // gestion de los botones
  void btn_actionPerformed(ActionEvent e) {
    int modoLlamada = modoMOD;
    v = new capp.CLista();
    boolean bExisteElemento = false;
    RecogeDatos();
    v = vLista;

    // grabar
    if (e.getActionCommand().equals("Grabar")) {
      if (isDataValid()) {
        switch (modoApertura) {
          // modificaci�n de una notificaci�n
          case DiaMantUsuTub.MODIFICACION:

            Inicializar(CInicializar.ESPERA);
            try {

              //v.addElement(vLista);

              stubCliente = new sapp.StubSrvBD();

              stubCliente.setUrl(new java.net.URL(app.getParametro(
                  "URL_SERVLET") + strSERVLET_MODUSUTUB));
              stubCliente.doPost(modoLlamada, v);

              /*
                               tuberculosis.servidor.mantus.SrvSelUsuTub servlet = new tuberculosis.servidor.mantus.SrvSelUsuTub();
                               //Indica como conectarse a la b.datos
                   servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                   "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                         "sive_desa",
                                         "sive_desa");
                               servlet.doDebug(modoLlamada, v);*/

              bAceptar = true;
              dispose();
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              bAceptar = false;
            }
            Inicializar(CInicializar.NORMAL);

            break;

        }
      }
      // salir
    }
    else if (e.getActionCommand().equals("Salir")) {
      bAceptar = false;
      dispose();
    }

  }

  /** cambios en el choice de Perfiles */
  void chkPerfil_itemStateChanged(ItemEvent e) {
    trasSeleccionarPerfil();
    vAmbUsu = vAmbUsuSel;
    seleccionarAmbitos();
  }

  void trasSeleccionarPerfil() {

    if (chkPerfil.getSelectedIndex() == 2) {
      // comprobamos que los datos de la listaNivel1 esten listos
      /*
               try {
         while(listaNiv1 == null) {
            Thread.sleep(500);
         }
               }catch (Exception exc){
         //#System.Out.println ("Fallo esperando la lista Nivel 1");
               }*/

      lstAmbUsuEnabled = true;
      Inicializar(CInicializar.NORMAL);

      vAmbUsu.removeAllElements();
      writeListaNiveles(listaNiv1, lstAmbUsu, "DS_NIVEL_1", "DSL_NIVEL_1",
                        "CD_NIVEL_1", "");

      //Dejamos como lista en uso lista de niveles 1  Necesario pues funcion seleccionarAmbitos solo usa listaNiv
      strCode = "CD_NIVEL_1";
      listaNiv = listaNiv1;
    }
    else if (chkPerfil.getSelectedIndex() == 3) {
      // comprobamos que los datos de la listaNivel2 esten listos
      /*
               try {
         while(listaNiv2 == null) {
            Thread.sleep(500);
         }
               }catch (Exception exc){
         //#System.Out.println ("Fallo esperando la lista Nivel 2");
               } */

      lstAmbUsuEnabled = true;
      Inicializar(CInicializar.NORMAL);

      vAmbUsu.removeAllElements();
      writeListaNiveles(listaNiv2, lstAmbUsu, "DS_NIVEL_2", "DSL_NIVEL_2",
                        "CD_NIVEL_1", "CD_NIVEL_2");

      //Dejamos como lista en uso lista de niveles 1
      strCode = "CD_NIVEL_2";
      listaNiv = listaNiv2;
    }
    else if (chkPerfil.getSelectedIndex() == 0 ||
             chkPerfil.getSelectedIndex() == 1 ||
             chkPerfil.getSelectedIndex() == 4) {
      lstAmbUsuEnabled = false;
      Inicializar(CInicializar.NORMAL);

      if (vAmbUsu != null) {
        vAmbUsu.removeAllElements();
        lstAmbUsu.removeAll();
      }

      strCode = null;
      listaNiv = null;
    }
  }

  // a�ade los datos de CLista  lista al control l
  protected void writeListaNiveles(capp.CLista lista, java.awt.List l,
                                   String des, String desL,
                                   String cod1, String cod2) {
    DataEnfermo datEnfermo;
    CMessage msgBox;
    capp.CLista data;

    // vacia el contenido
    l.removeAll();

    data = lista;

    // agrega lso items
    if (data.size() > 0) {

      // vuelca la lista, dependiendo de su tipo
      for (int j = 0; j < data.size(); ++j) {
        if (l.getName() == "lstAmbUsu") {
          datEnfermo = (DataEnfermo) data.elementAt(j);

          String strDesL = (String) datEnfermo.get(desL);
          if ( (strDesL != null) && (strDesL.length() > 0)) {
            l.add(strDesL);
          }
          else
          if (cod2.equals("")) {
            l.add( (String) datEnfermo.get(cod1) + " - " +
                  (String) datEnfermo.get(des));
          }
          else {
            l.add( (String) datEnfermo.get(cod1) + "-" +
                  (String) datEnfermo.get(cod2) + " - " +
                  (String) datEnfermo.get(des));
          }
        }
      }

      // opci�n m�s datos
      if (data.getState() == capp.CLista.listaINCOMPLETA) {
        //l.add(res.getString("msg30.Text"));
      }

      // mensaje de lista vacia
    }
    else {
      // msgBox = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg29.Text"));
      // msgBox.show();
      // msgBox = null;
    }
  }

  //Selecciona en el control lstAmbUsu todos los items cuyo c�digo corresponda con alguno
  //del vector de Strings vAmbUsu . La comparaci�n se busca en la listaAmbUsu pues es la que tiene
  //los codigos
  //Se supone que partimos de lstAmbUsu sin ninguno seleccionado
  void seleccionarAmbitos() {

    Enumeration enum;
    String listaNivel1 = null, listaNivel2 = null;
    String vAmbUsuNivel1 = null, vAmbUsuNivel2 = null;
    boolean encontrado = false;
    DataEnfermo denf = null;
    //vAmbUsuSel = dtSel.getString("NIVELES");
    //vAmbUsu.addElement(vAmbUsuSel);
    if ( (vAmbUsu != null) && (listaNiv != null)) {
      enum = vAmbUsu.elements();
      while (enum.hasMoreElements()) {
        vAmbUsuNivel1 = (String) (enum.nextElement());
        vAmbUsuNivel2 = (String) (enum.nextElement());

        ////#System.Out.println("Fijamos ambito "+vAmbUsuNivel1+vAmbUsuNivel2);
        encontrado = false;
        if (vAmbUsuNivel2 != null) {
          for (int j = 0; (j < listaNiv.size()) && (encontrado == false); j++) {
            denf = (DataEnfermo) (listaNiv.elementAt(j));
            listaNivel1 = (String) denf.get("CD_NIVEL_1");
            listaNivel2 = (String) denf.get("CD_NIVEL_2");
            if (vAmbUsuNivel1.equals(listaNivel1) &&
                vAmbUsuNivel2.equals(listaNivel2)) {
              lstAmbUsu.select(j);
              encontrado = true;
            }
          }
        }
        else if (vAmbUsuNivel1 != null) {
          for (int j = 0; (j < listaNiv.size()) && (encontrado == false); j++) {
            denf = (DataEnfermo) (listaNiv.elementAt(j));
            listaNivel1 = (String) denf.get("CD_NIVEL_1");
            if (vAmbUsuNivel1.equals(listaNivel1)) {
              lstAmbUsu.select(j);
              encontrado = true;
            }
          }
        }
      } //while
    } //if
  }

  //Al usar esta funcion suponemos que lst contiene todos los elem de la lista
  //Por tanto para indicar seleccionados basta volcar la lst al vector
  void ponerAmbitosEnVector() {
    String codigo = null, nivel2 = null;
    DataEnfermo denf = null;
    if (chkPerfil.getSelectedIndex() == 2 || chkPerfil.getSelectedIndex() == 3) {
      int[] indices = lstAmbUsu.getSelectedIndexes();
      if ( (vAmbUsu != null) && (listaNiv != null)) {
        vAmbUsu.removeAllElements();

        // rellenamos los c�digo de esos indices
        for (int j = 0; (j < indices.length); j++) {
          denf = ( (DataEnfermo) (listaNiv.elementAt(indices[j])));
          codigo = (String) denf.get("CD_NIVEL_1");
          nivel2 = (String) denf.get("CD_NIVEL_2");
          ////#System.Out.println("indice: " + indices[j] + codigo + nivel2);
          vAmbUsu.addElement(codigo); // nivel1
          vAmbUsu.addElement(nivel2); // nivel2
        }
      }
    }
    else {
      vAmbUsu = null;
    }

  }

  public boolean isDataValid() {
    String sMsg = "";
    int Per = chkPerfil.getSelectedIndex();
    if (Per == 2 || Per == 3) {
      int[] indSel = lstAmbUsu.getSelectedIndexes();

      if (indSel.length == 0) {
        sMsg = "Debe seleccionar al menos un �mbito de usuario";
        this.getApp().showAdvise(sMsg);
      }
      else {
        sMsg = "";
      }

      if (sMsg.equals("")) {
        return true;
      }
      else {
        return false;
      }
    }
    else if (Per == 0 || Per == 1) {
      return true;
    }
    sMsg = "Debe seleccionar al menos un perfil";
    this.getApp().showAdvise(sMsg);
    return false;
  }

  void chkbxEnf_itemStateChanged(ItemEvent e) {

  }
}

/**
 *  esta clase se lanza como un thread para traer los datos
 */
class LeerNiveles
    implements Runnable {
  protected DiaMantUsuTub adaptee = null;
  // Comunicaci�n con el servlet
  public sapp.StubSrvBD stubCliente = null;

  /**
   *  se inicia la lista no puede ser null
   */
  public LeerNiveles(DiaMantUsuTub app) {
    adaptee = app;
    run();
  }

  public void run() {

    capp.CLista parametros = null;
    capp.CLista result = null;
    stubCliente = new sapp.StubSrvBD();
    /// lista de Nivel1
    try {
      parametros = new capp.CLista();
      result = new capp.CLista();
      parametros.addElement(new DataEnfermo("CD_NIVEL_1"));

      URL u = new URL(adaptee.getApp().getParametro("URL_SERVLET") +
                      constantes.strSERVLET_ENFERMO);
      stubCliente.setUrl(u);

      /*
                 enfermo.SrvEnfermo servlet = new enfermo.SrvEnfermo();
                 //Indica como conectarse a la b.datos
                 servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
           result = (capp.CLista) servlet.doDebug(SrvEnfermo.modoNivel1, parametros);
       */

      result = (capp.CLista) stubCliente.doPost(SrvEnfermo.modoNivel1,
                                                parametros);

      adaptee.setListaNivel1(result);

    }
    catch (Exception exc) {
      //#System.Out.println("Error trayendo Nivel 1");
    }

    /// lista de Nivel2
    try {
      parametros = new capp.CLista();
      result = new capp.CLista();
      parametros.addElement(new DataEnfermo("CD_NIVEL_2"));

      URL u = new URL(adaptee.getApp().getParametro("URL_SERVLET") +
                      constantes.strSERVLET_ENFERMO);
      stubCliente.setUrl(u);

      result = (capp.CLista) stubCliente.doPost(SrvEnfermo.modoNivel2,
                                                parametros);

      //result = (capp.CLista) comun.traerDatos(adaptee.getApp(), stubCliente, constantes.strSERVLET_ENFERMO, SrvEnfermo.modoNivel1, parametros);
      adaptee.setListaNivel2(result);

    }
    catch (Exception exc) {
      //#System.Out.println("Error trayendo Nivel 2");
    }
  }

} // ENd Class

// gestor para los clics de los botones
class DiaMantUsuTub_actionAdapter
    implements java.awt.event.ActionListener {
  DiaMantUsuTub adaptee;
  ActionEvent e;

  DiaMantUsuTub_actionAdapter(DiaMantUsuTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    //Thread th = new Thread(this);
    //th.start();
    adaptee.btn_actionPerformed(e);
  }

  //public void run() {
  //  adaptee.btn_actionPerformed(e);
  //}
}

class DiaMantUsuTub_chkPerfil_itemAdapter
    implements java.awt.event.ItemListener {
  DiaMantUsuTub adaptee;

  DiaMantUsuTub_chkPerfil_itemAdapter(DiaMantUsuTub adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkPerfil_itemStateChanged(e);
  }
}

class DiaMantUsuTub_chkbxEnf_itemAdapter
    implements java.awt.event.ItemListener {
  DiaMantUsuTub adaptee;

  DiaMantUsuTub_chkbxEnf_itemAdapter(DiaMantUsuTub adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkbxEnf_itemStateChanged(e);
  }
}

// operaci�nes sobre las listas
class DiaMantUsuTubItemListener
    implements java.awt.event.ItemListener {
  DiaMantUsuTub adaptee = null;
  ItemEvent e = null;

  public DiaMantUsuTubItemListener(DiaMantUsuTub adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(ItemEvent e) {
    adaptee.lsAmbUsu_itemStateChanged(e);

  }
}
