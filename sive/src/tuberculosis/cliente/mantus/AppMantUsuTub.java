package tuberculosis.cliente.mantus;

import capp2.CApp;

public class AppMantUsuTub
    extends CApp {

  public AppMantUsuTub() {
    super();
  }

  public void init() {
    super.init();
    setTitulo("Mantenimiento de usuarios");
    PanMantUsuTub pnlUsuariosTub = new PanMantUsuTub( (CApp)this); ;
    pnlUsuariosTub.setBorde(false);

    VerPanel("", pnlUsuariosTub);
  }
}
