/**
 * Clase: DatMntRegTubSC
 * Paquete: SP_Tuberculosis.datos.mantregtub
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 28/10/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos resultado
 *   que envia el servlet SrvMntRegTub hacia el cliente.
 *   Implementa la estructura de comnicaci�n del servidor al cliente.
 * Modificaci�n: 19/01/2000
  Se a�aden los atributos CD_NIVEL_1_GE  y CD_NIVEL_2_GE
 */

package tuberculosis.datos.mantregtub;

import java.io.Serializable;

public class DatMntRegTubSC
    implements Serializable {

  // Datos de la busqueda
  private String CD_ANO = "";
  private String CD_REG = "";
  private String FC_FECHAENTRADA = "";
  private String CD_CODENFERMO = "";
  private String DS_APE1ENFERMO = "";
  private String DS_APE2ENFERMO = "";
  private String DS_NOMBREENFERMO = "";
  private String DS_SIGLAS = "";
  private String IT_CERRADO = "";
  private String NM_EDO = "";

  // E 19/01/2000
  private String CD_NIVEL_1_GE = "";
  private String CD_NIVEL_2_GE = "";

  // E 21/02/2000
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  // E 22/02/2000
  private String CD_NIVEL_1_EDOIND = "";
  private String CD_NIVEL_2_EDOIND = "";

  // Constructor 1�
  public DatMntRegTubSC(String Ano, String Reg, String FEntrada,
                        String CodEnfermo, String Ape1Enfermo,
                        String Ape2Enfermo, String NombreEnfermo,
                        String Siglas, String it_cerrado,
                        String nm_edo, String nivel1, String nivel2) {

    if (Ano != null) {
      CD_ANO = Ano;
    }
    else {
      CD_ANO = "";
    }
    if (Reg != null) {
      CD_REG = Reg;
    }
    else {
      CD_REG = "";
    }
    if (FEntrada != null) {
      FC_FECHAENTRADA = FEntrada;
    }
    else {
      FC_FECHAENTRADA = "";
    }
    if (CodEnfermo != null) {
      CD_CODENFERMO = CodEnfermo;
    }
    else {
      CD_CODENFERMO = "";
    }
    if (Ape1Enfermo != null) {
      DS_APE1ENFERMO = Ape1Enfermo;
    }
    else {
      DS_APE1ENFERMO = "";
    }
    if (Ape2Enfermo != null) {
      DS_APE2ENFERMO = Ape2Enfermo;
    }
    else {
      DS_APE2ENFERMO = "";
    }
    if (NombreEnfermo != null) {
      DS_NOMBREENFERMO = NombreEnfermo;
    }
    else {
      DS_NOMBREENFERMO = "";
    }
    if (Siglas != null) {
      DS_SIGLAS = Siglas;
    }
    else {
      DS_SIGLAS = "";
    }
    if (it_cerrado != null) {
      IT_CERRADO = it_cerrado;
    }
    else {
      IT_CERRADO = "";
    }
    if (nm_edo != null) {
      NM_EDO = nm_edo;
    }
    else {
      NM_EDO = "";

    }
    if (nivel1 != null) {
      CD_NIVEL_1_GE = new String(nivel1);
    }
    if (nivel2 != null) {
      CD_NIVEL_2_GE = new String(nivel2);

    }
  }

  // Constructor 2�
  public DatMntRegTubSC(String Ano, String Reg, String FEntrada,
                        String CodEnfermo, String Ape1Enfermo,
                        String Ape2Enfermo, String NombreEnfermo,
                        String Siglas, String it_cerrado,
                        String nm_edo) {

    if (Ano != null) {
      CD_ANO = Ano;
    }
    else {
      CD_ANO = "";
    }
    if (Reg != null) {
      CD_REG = Reg;
    }
    else {
      CD_REG = "";
    }
    if (FEntrada != null) {
      FC_FECHAENTRADA = FEntrada;
    }
    else {
      FC_FECHAENTRADA = "";
    }
    if (CodEnfermo != null) {
      CD_CODENFERMO = CodEnfermo;
    }
    else {
      CD_CODENFERMO = "";
    }
    if (Ape1Enfermo != null) {
      DS_APE1ENFERMO = Ape1Enfermo;
    }
    else {
      DS_APE1ENFERMO = "";
    }
    if (Ape2Enfermo != null) {
      DS_APE2ENFERMO = Ape2Enfermo;
    }
    else {
      DS_APE2ENFERMO = "";
    }
    if (NombreEnfermo != null) {
      DS_NOMBREENFERMO = NombreEnfermo;
    }
    else {
      DS_NOMBREENFERMO = "";
    }
    if (Siglas != null) {
      DS_SIGLAS = Siglas;
    }
    else {
      DS_SIGLAS = "";
    }
    if (it_cerrado != null) {
      IT_CERRADO = it_cerrado;
    }
    else {
      IT_CERRADO = "";
    }
    if (nm_edo != null) {
      NM_EDO = nm_edo;
    }
    else {
      NM_EDO = "";
    }
  }

  // Constructor 3�
  public DatMntRegTubSC(String Ano, String Reg, String FEntrada,
                        String CodEnfermo, String Ape1Enfermo,
                        String Ape2Enfermo, String NombreEnfermo,
                        String Siglas, String it_cerrado,
                        String nm_edo, String nivel1, String nivel2,
                        String cd_ope, String fc_ultact, String nivel1_edoind,
                        String nivel2_edoind) {

    if (Ano != null) {
      CD_ANO = Ano;
    }
    else {
      CD_ANO = "";
    }
    if (Reg != null) {
      CD_REG = Reg;
    }
    else {
      CD_REG = "";
    }
    if (FEntrada != null) {
      FC_FECHAENTRADA = FEntrada;
    }
    else {
      FC_FECHAENTRADA = "";
    }
    if (CodEnfermo != null) {
      CD_CODENFERMO = CodEnfermo;
    }
    else {
      CD_CODENFERMO = "";
    }
    if (Ape1Enfermo != null) {
      DS_APE1ENFERMO = Ape1Enfermo;
    }
    else {
      DS_APE1ENFERMO = "";
    }
    if (Ape2Enfermo != null) {
      DS_APE2ENFERMO = Ape2Enfermo;
    }
    else {
      DS_APE2ENFERMO = "";
    }
    if (NombreEnfermo != null) {
      DS_NOMBREENFERMO = NombreEnfermo;
    }
    else {
      DS_NOMBREENFERMO = "";
    }
    if (Siglas != null) {
      DS_SIGLAS = Siglas;
    }
    else {
      DS_SIGLAS = "";
    }
    if (it_cerrado != null) {
      IT_CERRADO = it_cerrado;
    }
    else {
      IT_CERRADO = "";
    }
    if (nm_edo != null) {
      NM_EDO = nm_edo;
    }
    else {
      NM_EDO = "";

    }
    if (nivel1 != null) {
      CD_NIVEL_1_GE = new String(nivel1);
    }
    if (nivel2 != null) {
      CD_NIVEL_2_GE = new String(nivel2);

    }
    if (cd_ope != null) {
      CD_OPE = new String(cd_ope);
    }
    if (fc_ultact != null) {
      FC_ULTACT = new String(fc_ultact);

    }
    if (nivel1_edoind != null) {
      CD_NIVEL_1_EDOIND = new String(nivel1_edoind);
    }
    if (nivel2_edoind != null) {
      CD_NIVEL_2_EDOIND = new String(nivel2_edoind);
    }
  }

  public String getCD_ANO() {
    return CD_ANO;
  } // Fin getCD_ANO()

  public String getCD_REG() {
    return CD_REG;
  } // Fin getCD_REG()

  public String getFC_FECHAENTRADA() {
    return FC_FECHAENTRADA;
  } // Fin getFC_FECHAENTRADA()

  public String getCD_CODENFERMO() {
    return CD_CODENFERMO;
  } // Fin getCD_CODENFERMO()

  public String getDS_APE1ENFERMO() {
    return DS_APE1ENFERMO;
  } // Fin getDS_APE1ENFERMO()

  public String getDS_APE2ENFERMO() {
    return DS_APE2ENFERMO;
  } // Fin getDS_APE2ENFERMO()

  public String getDS_NOMBREENFERMO() {
    return DS_NOMBREENFERMO;
  } // Fin getDS_NOMBREENFERMO()

  public String getDS_SIGLAS() {
    return DS_SIGLAS;
  } // Fin getDS_SIGLAS()

  public String getIT_CERRADO() {
    return IT_CERRADO;
  }

  public String getNM_EDO() {
    return NM_EDO;
  }

  public String getCD_NIVEL_1_GE() {
    return CD_NIVEL_1_GE;
  }

  public String getCD_NIVEL_2_GE() {
    return CD_NIVEL_2_GE;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public String getCD_NIVEL_1_EDOIND() {
    return CD_NIVEL_1_EDOIND;
  }

  public String getCD_NIVEL_2_EDOIND() {
    return CD_NIVEL_2_EDOIND;
  }

} // endclass DatMntRegTubSC
