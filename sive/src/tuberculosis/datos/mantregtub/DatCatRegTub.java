/**
 * Clase: DatCatRegTub
 * Paquete: SP_Tuberculosis.datos.mantregtub
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 11/11/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Estructura de datos que contiene un c�digo y una
 *   descripci�n. Sirve para  enviar informaci�n de los cat�logos
 *   ya que la mayor�a de ellos vienen identificados con un c�digo
 *   y una descripci�n.
 */

package tuberculosis.datos.mantregtub;

import java.io.Serializable;

public class DatCatRegTub
    implements Serializable {

  // Datos de la busqueda
  private String CD = ""; // C�digo
  private String DS = ""; // Descripci�n

  // Constructor
  public DatCatRegTub(String Cod, String Desc) {

    if (Cod != null) {
      CD = Cod;
    }
    else {
      CD = "";
    }
    if (Desc != null) {
      DS = Desc;
    }
    else {
      DS = "";
    }
  }

  public String getCD() {
    return CD;
  } // Fin getCD()

  public String getDS() {
    return DS;
  } // Fin getDS()

} // endclass DatCatRegTub
