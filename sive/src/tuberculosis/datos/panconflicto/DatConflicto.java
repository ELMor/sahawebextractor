// Extiende la clase DatTub

package tuberculosis.datos.panconflicto;

import java.util.Hashtable;

import comun.Fechas;
import tuberculosis.datos.notiftub.DatTub;

public class DatConflicto
    extends DatTub {

  /** constructor */
  public DatConflicto() {
    super();
  }

  /** constructor a partir de Hashtable */
  public DatConflicto(Hashtable h) {
    super(h);
  }

  /** Asignación de valores */
  public void setNM_EDO_MRE(int i) {
    introduce("NM_EDO_MRE", String.valueOf(i));
  }

  public void setCD_TSIVE_MRE(String s) {
    introduce("CD_TSIVE_MRE", s);
  }

  public void setCD_MODELO_MRE(String s) {
    introduce("CD_MODELO_MRE", s);
  }

  public void setNM_LIN_MRE(int i) {
    introduce("NM_LIN_MRE", String.valueOf(i));
  }

  public void setCD_PREGUNTA_MRE(String s) {
    introduce("CD_PREGUNTA_MRE", s);
  }

  public void setCD_ARTBC_RT(String s) {
    introduce("CD_ARTBC_RT", s);
  }

  public void setCD_NRTBC_RT(String s) {
    introduce("CD_NRTBC_RT", s);
  }

  public void setCD_NIVEL_1_GE_RT(String s) {
    introduce("CD_NIVEL_1_GE_RT", s);
  }

  public void setCD_NIVEL_2_GE_RT(String s) {
    introduce("CD_NIVEL_2_GE_RT", s);
  }

  public void setFC_INIRTBC_RT(java.sql.Date d) {
    introduce("FC_INIRTBC_RT", Fechas.date2String(d));
  }

  public void setDS_TEXTO_LA(String s) {
    introduce("DS_TEXTO_LA", s);
  }

  /** Lectura de valores */
  public int getNM_EDO_MRE() {
    int r = 0;
    String s = saca("NM_EDO_MRE");
    try {
      r = new Integer(s).intValue();
    }
    catch (Exception e) {
    }
    return r;
  }

  public String getCD_TSIVE_MRE() {
    return saca("CD_TSIVE_MRE");
  }

  public String getCD_MODELO_MRE() {
    return saca("CD_MODELO_MRE");
  }

  public int getNM_LIN_MRE() {
    int r = 0;
    String s = saca("NM_LIN_MRE");
    try {
      r = new Integer(s).intValue();
    }
    catch (Exception e) {
    }
    return r;
  }

  public String getCD_PREGUNTA_MRE() {
    return saca("CD_PREGUNTA_MRE");
  }

  public String getCD_ARTBC_RT() {
    return saca("CD_ARTBC_RT");
  }

  public String getCD_NRTBC_RT() {
    return saca("CD_NRTBC_RT");
  }

  public String getCD_NIVEL_1_GE_RT() {
    return saca("CD_NIVEL_1_GE_RT");
  }

  public String getCD_NIVEL_2_GE_RT() {
    return saca("CD_NIVEL_2_GE_RT");
  }

  public java.sql.Date getFC_INIRTBC_RT() {
    return new java.sql.Date(Fechas.string2Date(saca("FC_INIRTBC_RT")).getTime());
  }

  public String getDS_TEXTO_LA() {
    return saca("DS_TEXTO_LA");
  }

}