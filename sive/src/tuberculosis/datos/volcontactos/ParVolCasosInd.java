// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   ParVolCasosInd.java

package tuberculosis.datos.volcontactos;

import java.io.Serializable;

public class ParVolCasosInd
    implements Serializable {

  public String anoDesde;
  public String semDesde;
  public String anoHasta;
  public String semHasta;
  public String enfermedad;
  public String zbs;
  public String area;
  public String distrito;
  public String provincia;
  public String municipio;
  public int numPagina;

  public ParVolCasosInd() {
    anoDesde = new String();
    semDesde = new String();
    anoHasta = new String();
    semHasta = new String();
    enfermedad = new String();
    zbs = new String();
    area = new String();
    distrito = new String();
    provincia = new String();
    municipio = new String();
    numPagina = 0;
  }

  public ParVolCasosInd(String anoD, String semD, String anoH, String semH,
                        String enf) {
    anoDesde = new String();
    semDesde = new String();
    anoHasta = new String();
    semHasta = new String();
    enfermedad = new String();
    zbs = new String();
    area = new String();
    distrito = new String();
    provincia = new String();
    municipio = new String();
    numPagina = 0;
    anoDesde = anoD;
    semDesde = semD;
    anoHasta = anoH;
    semHasta = semH;
    enfermedad = enf;
  }
}
