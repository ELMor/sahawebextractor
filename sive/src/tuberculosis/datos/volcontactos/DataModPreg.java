// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   DataModPreg.java

package tuberculosis.datos.volcontactos;

import java.io.Serializable;

public class DataModPreg
    implements Serializable {

  public String codMod;
  public String codPreg;
  public String desMod;
  public String desPreg;
  public String tSive;
  public int modo;

  public DataModPreg() {
  }

  public DataModPreg(String cMod, String dMod, String cPreg, String dPreg,
                     String tS) {
    codMod = cMod;
    desMod = dMod;
    codPreg = cPreg;
    desPreg = dPreg;
    tSive = tS;
  }
}
