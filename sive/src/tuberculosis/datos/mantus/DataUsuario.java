package tuberculosis.datos.mantus;

import java.io.Serializable;
import java.util.Vector;

public class DataUsuario
    implements Serializable {

  protected String sCodUsu = ""; //C�digo de usuario
  protected String sDesNom = ""; //Nombre
  protected String sCodComAut = ""; //Com. aut�noma
  protected String sPer = ""; //perfil
  protected boolean bEnf; //Flag indica si usuario puede ver datos del enfermo
  protected Vector vAmbUsu = new Vector(); //Ambito del usuario
  protected String sCodApl = "";

  /*
    protected String sCodEquNot = ""; //Equipo notificador
    protected String sDesNom = "";    //Nombre
    protected String sDesApe = "";    //apellidos
    protected boolean bAutAlt;   //Autorizaci�n para dar altas
    protected boolean bAutMod;   //Autorizaci�n para hacer modificaciones
    protected boolean bAutBaj;   //Autorizaci�n para dar bajas
       protected boolean bManCat;   //Flag de autorizaci�n a mantenimiento de cat�logos
       protected boolean bManUsu;   //Flag de autorizaci�n a mantenimiento de usuarios
    protected boolean bTraPro;   //Flag de autorizaci�n de transmisi�n a Madrid e import protocolos
       protected boolean bDefPro;   //Flag de autorizaci�n de definici�n de protocolos
    protected boolean bAla;      //Flag de autorizaci�n de creaci�n de definiciones de alarmas
    protected boolean bVal;      //Flag de autorizaci�n de validaci�n de notificaciones
    protected String sDesTel = "";    //Tel�fonos de contacto
    protected String sDesDir = "";    //Direcci�n de contacto
    protected String sDesCor = "";    //Correo electr�nico
    protected String sDesAno = "";    //Otras anotaciones
    protected String sFecAlt = "";    //Fecha de alta
    protected String sFecBaj = "";     //Fecha de baja
    protected boolean bBaj;      //Marca de baja
    protected String sDesPas = "";    //Password del usuario
    protected String sDesEquNot = "";  //Descripci�n eq.notificador
    //QQ: Nuevos Flags:
    protected boolean bExport;     //Flag Exportaciones ASCII
    protected boolean bGenAlauto;  //Flag Generaci�n de Alarmas Autom�ticas
    protected boolean bManNotifs;  //Flag de Mantenimiento de Centros y Equipos Notificadores
    protected boolean bConsRes;    //Flag de Acceso a Consultas Restringidas
    protected boolean bResConf;
   */

  // Constructor por defecto
  public DataUsuario() {

  }

  //Constructor con par�metros
  public DataUsuario(String codUsu,
                     String desNom,
                     String codComAut,
                     String per, boolean enf,
                     Vector ambUsu,
                     String CodApl) {

    sCodUsu = codUsu;
    sDesNom = desNom;
    sCodComAut = codComAut;
    sPer = per;
    bEnf = enf;
    vAmbUsu = ambUsu;
    sCodApl = CodApl;
  }

  public String getCodUsu() {
    return sCodUsu;
  }

  public String getDesNom() {
    return sDesNom;
  }

  public String getCodApl() {
    return sCodApl;
  }

  public String getCodComAut() {
    return sCodComAut;
  }

  public String getPer() {
    String s;

    s = sPer;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public boolean getEnf() {
    return bEnf;
  }

  public Vector getAmbUsu() {
    return vAmbUsu;
  }

  // SET'S
  public void setAmbUsu(Vector v) {
    vAmbUsu = v;
  }

  public void setCodUsu(String codUsu) {
    sCodUsu = codUsu;
    if (codUsu == null) {
      sCodUsu = "";
    }
  }

  public void setDesNom(String desNom) {
    sDesNom = desNom;
    if (sDesNom == null) {
      sDesNom = "";
    }
  }

  public void setCodApl(String CodApl) {
    sCodApl = CodApl;
    if (CodApl == null) {
      sCodApl = "";
    }
  }

  public void setPer(String per) {
    sPer = per;
    if (per == null) {
      sPer = "";
    }
  }

  public void setEnf(boolean enf) {
    bEnf = enf;
  }

  public void setCodComAut(String CodAut) {
    sCodComAut = CodAut;
  }

}
