/**
 * Clase: DatTubNotif
 * Paquete: tuberculosis.datos.diatubnotif
 * Hereda:
 * Autor: Emilio Postigo Riancho
 * Fecha Inicio: 23/11/1999
 * Descripcion: Implementacion de la estructura de datos empleada en la
  clase DiaRelaciones, en el paquete tuberculosis.cliente.diatubnotif
 */

package tuberculosis.datos.diatubnotif;

import java.io.Serializable;

public class DatTubNotif
    implements Serializable {

  public String sResSem = "", sFechaNotif = "", sFechaRecep = "";
  public String sNotifReales = "";
  public String sNotifTeor = "", sTotNotifReales = "";
  public String sCodEquipo = "", sAnno = "";
  public String sSemana = "";

  public String CD_OPE_NOTIF_SEM = "";
  public String FC_ULTACT_NOTIF_SEM = "";
  public String CD_OPE_NOTIFEDO = "";
  public String FC_ULTACT_NOTIFEDO = "";

  public String IT_VALIDADA = "";
  public String FC_FECVALID = "";

  public String CD_NIVEL_1 = "";
  public String CD_NIVEL_2 = "";

  private String sDS_DECLARANTE = null;
  private String sDS_HISTCLI = null;

  private String sCD_FUENTE = null;
  private String sNM_EDO = null;

  private String CD_OPE_NOTIF_EDOI = "";
  private String FC_ULTACT_NOTIF_EDOI = "";

  private String sIT_PRIMERO = null;

  // Constructores
  public DatTubNotif() {}

  // Este constructor se eliminar� en cuanto sea posible
  /*public DatTubNotif(String ressem,
                     String fechanotif,
                     String fecharecep,
                     String notifreales,
                     String notifTeoricos,
                     String totalReales,
                     String codeq, String anno, String sem,
                     String cd_ope_notif_sem,
                     String fc_ultact_notif_sem,
                     String cd_ope_notifedo,
                     String fc_ultact_notifedo,
                     String it_validada,
                     String fc_validacion,
                     String cd_nivel_1,
                     String cd_nivel_2) {
    sResSem = ressem;
    sFechaNotif = fechanotif;
    sFechaRecep = fecharecep;
    sNotifReales = notifreales;
    sNotifTeor = notifTeoricos;
    sTotNotifReales = totalReales;
    sCodEquipo = codeq;
    sAnno = anno;
    sSemana = sem;
    CD_OPE_NOTIF_SEM= cd_ope_notif_sem;
    FC_ULTACT_NOTIF_SEM= fc_ultact_notif_sem;
    CD_OPE_NOTIFEDO= cd_ope_notifedo;
    FC_ULTACT_NOTIFEDO = fc_ultact_notifedo;
    IT_VALIDADA=it_validada;
    FC_FECVALID=fc_validacion;
    CD_NIVEL_1 = cd_nivel_1;
    CD_NIVEL_2 = cd_nivel_2;
     } */

  // Constructor nuevo
  public DatTubNotif(String ressem,
                     String fechanotif,
                     String fecharecep,
                     String notifreales,
                     String notifTeoricos,
                     String totalReales,
                     String codeq, String anno, String sem,
                     String cd_ope_notif_sem,
                     String fc_ultact_notif_sem,
                     String cd_ope_notifedo,
                     String fc_ultact_notifedo,
                     String it_validada,
                     String fc_validacion,
                     String cd_nivel_1,
                     String cd_nivel_2,
                     String ds_declarante,
                     String ds_histcli,
                     String cd_fuente) {

    setResSem(ressem);
    setFechaNotif(fechanotif);
    setFechaRecep(fecharecep);
    setNotifReales(notifreales);
    setNotifTeor(notifTeoricos);
    setTotNotifReales(totalReales);
    setCodEquipo(codeq);
    setAnno(anno);
    setSemana(sem);
    setCD_OPE_NOTIF_SEM(cd_ope_notif_sem);
    setFC_ULTACT_NOTIF_SEM(fc_ultact_notif_sem);
    setCD_OPE_NOTIFEDO(cd_ope_notifedo);
    setFC_ULTACT_NOTIFEDO(fc_ultact_notifedo);

    setIT_VALIDADA(it_validada);
    setFC_FECVALID(fc_validacion);

    setCD_NIVEL_1(cd_nivel_1);
    setCD_NIVEL_2(cd_nivel_2);

    setDS_DECLARANTE(ds_declarante);
    setDS_HISTCLI(ds_histcli);
    setCD_FUENTE(cd_fuente);
  }

  // M�todos de lectura
  public String getIT_PRIMERO() {
    return sIT_PRIMERO;
  }

  public String getNM_EDO() {
    return sNM_EDO;
  }

  public String getResSem() {
    return sResSem;
  }

  public String getFechaNotif() {
    return sFechaNotif;
  }

  public String getFechaRecep() {
    return sFechaRecep;
  }

  public String getNotifReales() {
    return sNotifReales;
  }

  public String getNotifTeor() {
    return sNotifTeor;
  }

  public String getTotNotifReales() {
    return sTotNotifReales;
  }

  public String getCodEquipo() {
    return sCodEquipo;
  }

  public String getAnno() {
    return sAnno;
  }

  public String getSemana() {
    return sSemana;
  }

  public String getCD_OPE_NOTIF_SEM() {
    return CD_OPE_NOTIF_SEM;
  }

  public String getFC_ULTACT_NOTIF_SEM() {
    return FC_ULTACT_NOTIF_SEM;
  }

  public String getCD_OPE_NOTIFEDO() {
    return CD_OPE_NOTIFEDO;
  }

  public String getFC_ULTACT_NOTIFEDO() {
    return FC_ULTACT_NOTIFEDO;
  }

  public String getCD_OPE_NOTIF_EDOI() {
    return CD_OPE_NOTIF_EDOI;
  }

  public String getFC_ULTACT_NOTIF_EDOI() {
    return FC_ULTACT_NOTIF_EDOI;
  }

  public String getIT_VALIDADA() {
    return IT_VALIDADA;
  }

  public String getFC_FECVALID() {
    return FC_FECVALID;
  }

  public String getCD_NIVEL_1() {
    return CD_NIVEL_1;
  }

  public String getCD_NIVEL_2() {
    return CD_NIVEL_2;
  }

  public String getDS_DECLARANTE() {
    return sDS_DECLARANTE;
  }

  public String getDS_HISTCLI() {
    return sDS_HISTCLI;
  }

  public String getCD_FUENTE() {
    return sCD_FUENTE;
  }

  // M�todos de escritura
  public void setResSem(String s) {
    sResSem = "";
    if (s != null) {
      sResSem = s.trim();
    }
  }

  public void setFechaNotif(String s) {
    sFechaNotif = "";
    if (s != null) {
      sFechaNotif = s.trim();
    }
  }

  public void setFechaRecep(String s) {
    sFechaRecep = "";
    if (s != null) {
      sFechaRecep = s.trim();
    }
  }

  public void setNotifReales(String s) {
    sNotifReales = "0";
    if (s != null && !s.equals("")) {
      sNotifReales = s.trim();
    }
  }

  public void setNotifTeor(String s) {
    sNotifTeor = "0";
    if (s != null && !s.equals("")) {
      sNotifTeor = s.trim();
    }
  }

  public void setTotNotifReales(String s) {
    sTotNotifReales = "0";
    if (s != null && !s.equals("")) {
      sTotNotifReales = s.trim();
    }
  }

  public void setCodEquipo(String s) {
    sCodEquipo = "";
    if (s != null) {
      sCodEquipo = s.trim();
    }
  }

  public void setAnno(String s) {
    sAnno = "";
    if (s != null) {
      sAnno = s.trim();
    }
  }

  public void setSemana(String s) {
    sSemana = "";
    if (s != null) {
      sSemana = s.trim();
    }
  }

  public void setCD_OPE_NOTIF_SEM(String s) {
    CD_OPE_NOTIF_SEM = "";
    if (s != null) {
      CD_OPE_NOTIF_SEM = s.trim();
    }
  }

  public void setFC_ULTACT_NOTIF_SEM(String s) {
    FC_ULTACT_NOTIF_SEM = "";
    if (s != null) {
      FC_ULTACT_NOTIF_SEM = s.trim();
    }
  }

  public void setCD_OPE_NOTIFEDO(String s) {
    CD_OPE_NOTIFEDO = "";
    if (s != null) {
      CD_OPE_NOTIFEDO = s.trim();
    }
  }

  public void setFC_ULTACT_NOTIFEDO(String s) {
    FC_ULTACT_NOTIFEDO = "";
    if (s != null) {
      FC_ULTACT_NOTIFEDO = s.trim();
    }
  }

  public void setCD_OPE_NOTIF_EDOI(String s) {
    CD_OPE_NOTIF_EDOI = "";
    if (s != null) {
      CD_OPE_NOTIF_EDOI = s.trim();
    }
  }

  public void setFC_ULTACT_NOTIF_EDOI(String s) {
    FC_ULTACT_NOTIF_EDOI = "";
    if (s != null) {
      FC_ULTACT_NOTIF_EDOI = s.trim();
    }
  }

  public void setIT_VALIDADA(String s) {
    IT_VALIDADA = "";
    if (s != null) {
      IT_VALIDADA = s.trim();
    }
  }

  public void setFC_FECVALID(String s) {
    FC_FECVALID = "";
    if (s != null) {
      FC_FECVALID = s.trim();
    }
  }

  public void setCD_NIVEL_1(String s) {
    CD_NIVEL_1 = "";
    if (s != null) {
      CD_NIVEL_1 = s.trim();
    }
  }

  public void setCD_NIVEL_2(String s) {
    CD_NIVEL_2 = "";
    if (s != null) {
      CD_NIVEL_2 = s.trim();
    }
  }

  public void setDS_DECLARANTE(String s) {
    sDS_DECLARANTE = "";
    if (s != null) {
      sDS_DECLARANTE = s.trim();
    }
  }

  public void setDS_HISTCLI(String s) {
    sDS_HISTCLI = "";
    if (s != null) {
      sDS_HISTCLI = s.trim();
    }
  }

  public void setCD_FUENTE(String s) {
    sCD_FUENTE = "";
    if (s != null) {
      sCD_FUENTE = s.trim();
    }
  }

  public void setNM_EDO(String s) {
    sNM_EDO = "";
    if (s != null) {
      sNM_EDO = s.trim();
    }
  }

  public void setIT_PRIMERO(String s) {
    sIT_PRIMERO = "";
    if (s != null) {
      sIT_PRIMERO = s.trim();
    }
  }
}
