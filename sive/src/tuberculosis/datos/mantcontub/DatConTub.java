/*
 * Clase: DatConTubCS
 * Paquete: tuberculosis.datos.mantcontub
 * Hereda:
 * Autor: Pedro Antonio D�az Pardo (PDP)
 * Fecha Inicio: 25/01/2000
 * Analisis Funcional: Mantenimiento Contactos de enfermos de Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos necesarios
     *   para realizar una busqueda de contactos de enfermos de tuberculosis a traves
 *   del servlet SrvConTub, para el panel PanConTub.
 *   Implementa la estructura de comunicaci�n del cliente al servidor.
 */

package tuberculosis.datos.mantcontub;

import java.sql.Date;

import comun.DatoBase;

public class DatConTub
    extends DatoBase {

  // Datos de la busqueda
  private static String key_CD_ANO = "key_CD_ANO";
  private static String key_CD_REG = "key_CD_REG";
  private static String key_CD_ENFERMO = "key_CD_ENFERMO";
  private static String key_DS_NOMBRE = "key_DS_NOMBRE";
  private static String key_DS_APE1 = "key_DS_APE1";
  private static String key_DS_APE2 = "key_DS_APE2";
  private static String key_FC_NAC = "key_FC_NAC";
  private static String key_IT_CALC = "key_IT_CALC";

  public DatConTub() {
    super();
  }

  public DatConTub(String Ano,
                   String Reg,
                   String CEnfermo,
                   String Nombre,
                   String Ape1,
                   String Ape2,
                   java.sql.Date FNac,
                   String ICalc) { // Constructor
    super();
    setCD_ANO(Ano);
    setCD_REG(Reg);
    setCD_ENFERMO(CEnfermo);
    setDS_NOMBRE(Nombre);
    setDS_APE1(Ape1);
    setDS_APE2(Ape2);
    setFC_NAC(FNac);
    setIT_CALC(ICalc);
  }

  //M�todos para  introducir
  public void setCD_ANO(String Ano) {
    introduce(key_CD_ANO, Ano);
  }

  public void setCD_REG(String Reg) {
    introduce(key_CD_REG, Reg);
  }

  public void setCD_ENFERMO(String CEnfermo) {
    introduce(key_CD_ENFERMO, CEnfermo);
  }

  public void setDS_NOMBRE(String Nombre) {
    introduce(key_DS_NOMBRE, Nombre);
  }

  public void setDS_APE1(String Ape1) {
    introduce(key_DS_APE1, Ape1);
  }

  public void setDS_APE2(String Ape2) {
    introduce(key_DS_APE2, Ape2);
  }

  public void setFC_NAC(java.sql.Date FNac) {
    introduce(key_FC_NAC, FNac);
  }

  public void setIT_CALC(String ICalc) {
    introduce(key_IT_CALC, ICalc);
  }

  //M�todos para recuperar

  public String getCD_ANO() {
    return (String) saca(key_CD_ANO);
  }

  public String getCD_REG() {
    return (String) saca(key_CD_REG);
  }

  public String getCD_ENFERMO() {
    return (String) saca(key_CD_ENFERMO);
  }

  public String getDS_NOMBRE() {
    return (String) saca(key_DS_NOMBRE);
  }

  public String getDS_APE1() {
    return (String) saca(key_DS_APE1);
  }

  public String getDS_APE2() {
    return (String) saca(key_DS_APE2);
  }

  public java.sql.Date getFC_NAC() {
    return (Date) sacaDate(key_FC_NAC);
  }

  public String getIT_CALC() {
    return (String) saca(key_IT_CALC);
  }

} // endclass DatConTubCS