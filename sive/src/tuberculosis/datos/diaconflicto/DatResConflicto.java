// Extiende la clase DatTub

package tuberculosis.datos.diaconflicto;

import java.util.Hashtable;

import comun.Fechas;
import tuberculosis.datos.notiftub.DatTub;

public class DatResConflicto
    extends DatTub {

  /** constructor */
  public DatResConflicto() {
    super();
  }

  /** constructor a partir de Hashtable */
  public DatResConflicto(Hashtable h) {
    super(h);
  }

  /** Asignaci�n de valores */
  public void setNM_EDO_MRE(int i) {
    introduce("NM_EDO_MRE", String.valueOf(i));
  }

  public void setCD_TSIVE_MRE(String s) {
    introduce("CD_TSIVE_MRE", s);
  }

  public void setCD_MODELO_MRE(String s) {
    introduce("CD_MODELO_MRE", s);
  }

  public void setNM_LIN_MRE(int i) {
    introduce("NM_LIN_MRE", String.valueOf(i));
  }

  public void setCD_PREGUNTA_MRE(String s) {
    introduce("CD_PREGUNTA_MRE", s);
  }

  public void setDS_DECLARANTE_SNE(String s) {
    introduce("DS_DECLARANTE_SNE", s);
  }

  public void setCD_E_NOTIF_MRE(String s) {
    introduce("CD_E_NOTIF_MRE", s);
  }

  public void setCD_ANOEPI_MRE(String s) {
    introduce("CD_ANOEPI_MRE", s);
  }

  public void setCD_SEMEPI_MRE(String s) {
    introduce("CD_SEMEPI_MRE", s);
  }

  public void setFC_FECNOTIF_MRE(java.sql.Date d) {
    introduce("FC_FECNOTIF_MRE", Fechas.date2String(d));
  }

  public void setFC_RECEP_MRE(java.sql.Date d) {
    introduce("FC_RECEP_MRE", Fechas.date2String(d));
  }

  public void setFC_ASIGN_MRE(java.sql.Date d) {
    introduce("FC_ASIGN_MRE", Fechas.date2String(d));
  }

  public void setCD_NIVEL_1_EQ(String s) {
    introduce("CD_NIVEL_1_EQ", s);
  }

  public void setCD_NIVEL_2_EQ(String s) {
    introduce("CD_NIVEL_2_EQ", s);
  }

  public void setCD_FUENTE_MRE(String s) {
    introduce("CD_FUENTE_MRE", s);
  }

  public void setDS_RESPUESTA_MRE(String s) {
    introduce("DS_RESPUESTA_MRE", s);
  }

  // Para almacenar los datos de bloqueo del caso
  // asociado
  public void setCD_OPE_EDOIND(String s) {
    introduce("CD_OPE_EDOIND", s);
  }

  public void setFC_ULTACT_EDOIND(java.sql.Timestamp ts) {
    introduce("FC_ULTACT_EDOIND", Fechas.timestamp2String(ts));
  }

  // Para establecer si est� consolidado o no
  public void setIT_CONSOLIDADO(boolean b) {
    introduce("IT_CONSOLIDADO", String.valueOf(b));
  }

  /** Lectura de valores */
  public int getNM_EDO_MRE() {
    int r = 0;
    String s = saca("NM_EDO_MRE");
    try {
      r = new Integer(s).intValue();
    }
    catch (Exception e) {
    }
    return r;
  }

  public String getCD_TSIVE_MRE() {
    return saca("CD_TSIVE_MRE");
  }

  public String getCD_MODELO_MRE() {
    return saca("CD_MODELO_MRE");
  }

  public int getNM_LIN_MRE() {
    int r = 0;
    String s = saca("NM_LIN_MRE");
    try {
      r = new Integer(s).intValue();
    }
    catch (Exception e) {
    }
    return r;
  }

  public String getCD_PREGUNTA_MRE() {
    return saca("CD_PREGUNTA_MRE");
  }

  public String getDS_DECLARANTE_SNE() {
    return saca("DS_DECLARANTE_SNE");
  }

  public String getCD_E_NOTIF_MRE() {
    return saca("CD_E_NOTIF_MRE");
  }

  public String getCD_ANOEPI_MRE() {
    return saca("CD_ANOEPI_MRE");
  }

  public String getCD_SEMEPI_MRE() {
    return saca("CD_SEMEPI_MRE");
  }

  public java.sql.Date getFC_FECNOTIF_MRE() {
    return new java.sql.Date(Fechas.string2Date(saca("FC_FECNOTIF_MRE")).
                             getTime());
  }

  public java.sql.Date getFC_RECEP_MRE() {
    return new java.sql.Date(Fechas.string2Date(saca("FC_RECEP_MRE")).getTime());
  }

  public java.sql.Date getFC_ASIGN_MRE() {
    return new java.sql.Date(Fechas.string2Date(saca("FC_ASIGN_MRE")).getTime());
  }

  public String getCD_NIVEL_1_EQ() {
    return saca("CD_NIVEL_1_EQ");
  }

  public String getCD_NIVEL_2_EQ() {
    return saca("CD_NIVEL_2_EQ");
  }

  public String getCD_FUENTE_MRE() {
    return saca("CD_FUENTE_MRE");
  }

  public String getDS_RESPUESTA_MRE() {
    return saca("DS_RESPUESTA_MRE");
  }

  // Para recuperar los datos de bloqueo del caso
  // asociado
  public String getCD_OPE_EDOIND() {
    return saca("CD_OPE_EDOIND");
  }

  public java.sql.Timestamp getFC_ULTACT_EDOIND() {
    return Fechas.string2Timestamp(saca("FC_ULTACT_EDOIND"));
  }

  // Para saber si est� consolidado o no
  public boolean getIT_CONSOLIDADO() {
    String s = saca("IT_CONSOLIDADO");
    boolean b = false;

    if (!s.equals("")) {
      b = new Boolean(s).booleanValue();
    }

    return b;
  }

}