/**
 * @autor Pedro Antonio D�az Pardo (PDP)
 * @version 1.0
 *
 * Recupera las l�neas de detalle de una petici�n
 * Tabla:    SIVE_ENFERMO
 * Entrada:  un Data con CD_NRTBC Y CD_ARTBC
 * Salida:   una Lista con un Data para cada una de las filas de
 *           que correspondan a esa petici�n.
 *       CD_ENFERMO
 *       CD_E_NOTIF
 * Tabla:    SIVE_CASOCONTAC
 * Entrada:  CD_ENFERMO
 * Salida:   DS_NOMBRE
 *           DS_APE1
 *           DS_APE2
 *           FC_NAC
 *           IT_CALC
 *
 */

package tuberculosis.servidor.mantcontub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;
import tuberculosis.datos.mantcontub.DatConTub;

public class SrvMntCont
    extends DBServlet {

  final protected int servletCONTUB = 1;

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatConTub dataS = null;
    boolean bFiltro = false;
    int iValorFiltro = 0;

    // Querys
    String sQuery = "";
    String sDatosEnfermo = "";

    // Lleva la cuenta del numero de registros a�adidos para el tramado
    int iNRegs = 1;

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // Recuperacion de los datos de busqueda
      dataS = (DatConTub) param.firstElement();

      switch (opmode) {

        case servletCONTUB:

          // Preparacion del patron de la query
          sQuery = "SELECT c.CD_ARTBC, c.CD_NRTBC, " +
              " e.CD_ENFERMO, e.DS_NOMBRE, " +
              " e.DS_APE1, e.DS_APE2, e.FC_NAC, " +
              " e.IT_CALC " +
              " FROM SIVE_CASOCONTAC c, SIVE_ENFERMO e" +
              " WHERE c.CD_ENFERMO = e.CD_ENFERMO ";

          // Terminacion de la query en funcion de los parametros de busqueda que
          //  vienen en dataEntrada
          // Siguiente registro a leer
          if (param.getFilter().length() > 0) {
            bFiltro = true;
            iValorFiltro = (new Integer(param.getFilter().trim())).intValue();
          }

          sQuery = sQuery + TerminarPreparacionQuery(dataS);

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciamos la query con los valores de los datos de entrada
          InstanciarQuery(dataS, st, bFiltro, iValorFiltro);

          // Ejecucion de la query, obteniendo un ResultSet
          rs = st.executeQuery();

          // Procesamiento de cada registro obtenido de la BD
          while (rs.next()) {
            // Si iNRegs es > que el maximo numero de registros por peticion
            //  se sale del bucle dejando la lista en estado INCOMPLETA
            if (iNRegs > DBServlet.maxSIZE) {
              listaSalida.setState(CLista.listaINCOMPLETA);
              listaSalida.setFilter( ( (DatConTub) listaSalida.lastElement()).
                                    getCD_REG());
              break;
            }

            // Recuperacion del a�o, registro, CodEnfermo, Ape1Enfermo, Ape2Enfermo, It_calc
            //  a partir del ResultSet de la query realizada sobre la BD
            String Ano = rs.getString("CD_ARTBC");
            String Reg = rs.getString("CD_NRTBC");
            int iCEnfermo = rs.getInt("CD_ENFERMO");
            String CEnfermo = (new Integer(iCEnfermo)).toString();
            String Nombre = rs.getString("DS_NOMBRE");
            if (Nombre == null) {
              Nombre = "";
            }
            String Ape1 = rs.getString("DS_APE1");
            String Ape2 = rs.getString("DS_APE2");
            if (Ape1 == null) {
              Ape1 = "";
            }
            if (Ape2 == null) {
              Ape2 = "";
            }
            java.sql.Date FNac = rs.getDate("FC_NAC");
            String ICalc = rs.getString("IT_CALC");

            // Relleno del registro de salida correspondiente a esta iteracion por
            //  los registros devueltos por la query

            dataS = new DatConTub(Ano, Reg, CEnfermo, Nombre,
                                  Ape1, Ape2, FNac, ICalc);

            // Adicion del registro obtenido a la lista resultado de salida hacia cliente
            listaSalida.addElement(dataS);

            // Se aumenta el numero de registros a�adidos
            iNRegs++;
          } // Fin for each registro

          // Cierre del ResultSet y del PreparedStatement
          rs.close();
          rs = null;
          st.close();
          st = null;
          break;
      } //fin switch

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }
    finally {
      closeConnection(con);
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Termina la preparacion de la query
  protected String TerminarPreparacionQuery(DatConTub dataS) {

    // Registros de un a�o determinado (obligatorio)
    String sQ = " AND c.CD_ARTBC = ? ";

    // Un determinado numero de registro (unico)
    if (dataS.getCD_REG().trim().length() > 0) {
      sQ = sQ + " AND c.CD_NRTBC = ? ";
      return (sQ);
    }

    return sQ;
  } // Fin TerminarPreparacionQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void InstanciarQuery(DatConTub dataS,
                                 PreparedStatement st, boolean bFiltro,
                                 int iValorFiltro) throws Exception {

    // Contador de posicion del parametro
    int iParN = 1;

    // A�o (obligatorio)
    st.setString(iParN, dataS.getCD_ANO().trim());

    iParN++;

    // Registro (unico)
    String Reg = dataS.getCD_REG().trim();
    if (Reg.length() > 0) {
      st.setString(iParN, Reg);
      return;
    }

  } // Fin InstanciarQuery()

  /*
   } // Fin SrvMntRegTub
   */

  static public void main(String args[]) {

    SrvMntCont srv = new SrvMntCont();
    CLista listaSalida = new CLista();
    DatConTub hs = new DatConTub();
    CLista resultado = null;

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    hs.setCD_ANO("2000");
    hs.setCD_REG("40");
    hs.setCD_ENFERMO("");
    hs.setDS_NOMBRE("");
    hs.setDS_APE1("");
    hs.setDS_APE2("");
    hs.setFC_NAC(null);
    hs.setIT_CALC("");

    listaSalida.addElement(hs);
    resultado = srv.doDebug(1, listaSalida);
    resultado = null;

  }
}
