/**
 * Clase: SrvCatRegTub
 * Paquete: SP_Tuberculosis.servidor.mantregtub
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 10/11/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Implementacion del servlet que recupera de la BD los
 *   cat�logos que ser�n utilizados tanto en el panel de mantenimiento
 *   como en los di�logos y paneles que de �l se desprenden.
 * 12/11/99 (JMT): recupera lista de sexos
 */

package tuberculosis.servidor.mantregtub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;
import suca.datasuca;
import tuberculosis.datos.mantregtub.DatCatRegTub;

public class SrvCatRegTub
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista(); // Lista con las 3 siguientes listas
    CLista listaMotivosSalida = new CLista();
    CLista listaPaises = new CLista();
    CLista listaCA = new CLista();
    CLista listaSexos = new CLista();

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que el commit
    //  se haga autom�ticamente sobre cada actualizaci�n
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // No hay que recuperar ning�n dato de entrada

      /************* CLISTA DE MOTIVOS DE SALIDA **************/

      // Preparaci�n de la query completa (motivos de salida)
      sQuery = "SELECT CD_MOTSALRTBC, DS_MOTSALRTBC " +
          "FROM SIVE_MOTIVOS_SALRTBC ORDER BY CD_MOTSALRTBC";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DatCatRegTub dataMotSalida = null;
      while (rs.next()) {
        dataMotSalida = new DatCatRegTub(rs.getString("CD_MOTSALRTBC"),
                                         rs.getString("DS_MOTSALRTBC"));
        listaMotivosSalida.addElement(dataMotSalida);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE MOTIVOS DE SALIDA **************/

      /************* CLISTA DE PAISES **************/

      // Preparaci�n de la query completa (paises)
      sQuery = "select CD_PAIS, DS_PAIS from SIVE_PAISES";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      datasuca dataPais = null;
      while (rs.next()) {
        dataPais = new datasuca();
        dataPais.put("CD_PAIS", rs.getString("CD_PAIS"));
        dataPais.put("DS_PAIS", rs.getString("DS_PAIS"));
        listaPaises.addElement(dataPais);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE PAISES **************/

      /************* CLISTA DE COMUNIDADES AUTONOMAS SIVE **************/

      /*  // Preparaci�n de la query completa (CAs)
        sQuery = "select CD_CA, DS_CA, DSL_CA from SIVE_COM_AUT";
        // Preparaci�n de la sentencia SQL
        st = con.prepareStatement(sQuery);
        // Ejecuci�n de la query obteniendo un resultset
        rs = st.executeQuery();
        // Procesamiento de cada registro obtenido de la BD
        String ds = "";
        String dsl= "";
        String sDes = "";
        datasuca dataComunidad = null;
        while (rs.next()) {
          ds = rs.getString("DS_CA");
          dsl = rs.getString("DSL_CA");
          if ((param.getIdioma()!=CApp.idiomaPORDEFECTO) && (dsl!=null))
            sDes = dsl;
          else
            sDes = ds;
          dataComunidad = new datasuca();
          dataComunidad.put("CD_CA", rs.getString("CD_CA"));
          dataComunidad.put("DS_CA", sDes);
          listaCA.addElement(dataComunidad);
        }
        // Cierre del ResultSet y del PreparedStatement
        rs.close();
        rs = null;
        st.close();
        st = null;*/

      /************* FIN CLISTA DE COMUNIDADES AUTONOMAS SIVE **************/

      /************* CLISTA DE COMUNIDADES AUTONOMAS SUCA **************/

      // Preparaci�n de la query completa (CAs)
      sQuery = " select CDCOMU, DSCOMU from SUCA_AUTONOMIAS ";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      datasuca dataComunidad = null;
      while (rs.next()) {
        dataComunidad = new datasuca();

        dataComunidad.put("CD_CA", rs.getString("CDCOMU"));
        dataComunidad.put("DS_CA", rs.getString("DSCOMU"));

        listaCA.addElement(dataComunidad);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE COMUNIDADES AUTONOMAS SUCA **************/

      /************* CLISTA DE SEXOS **************/

      // Preparaci�n de la query completa (motivos de salida)
      sQuery = "SELECT CD_SEXO, DS_SEXO, DSL_SEXO " +
          "FROM SIVE_SEXO ORDER BY CD_SEXO";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DatCatRegTub dataSexo = null;
      while (rs.next()) {
        dataSexo = new DatCatRegTub(rs.getString("CD_SEXO"),
                                    rs.getString("DS_SEXO"));
        listaSexos.addElement(dataSexo);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE SEXOS **************/

      // Preparaci�n de la lista resultado a devolver al cliente
      listaSalida.addElement(listaMotivosSalida);
      listaSalida.addElement(listaPaises);
      listaSalida.addElement(listaCA);
      listaSalida.addElement(listaSexos);

      // Validaci�n de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexi�n con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

} // Fin clase SrvCatRegTub
