/**
 * Clase: SrvSelMues
 * Paquete: tuberculosis.servidor.pantotalmues
 * Hereda: DBServlet
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 10/03/2000
 * Descripcion: Implementacion del servlet que recupera de la BD los datos
 * a mostrar en la tabla del PanTotMues muestras todos los notificadores.
 */

package tuberculosis.servidor.pantotalmues;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import capp.CLista;
import sapp.DBServlet;
import tuberculosis.datos.pantotalmues.DatTotMuesCS;
import tuberculosis.datos.pantotalmues.DatTotMuesSC;

public class SrvSelRes
    extends DBServlet {

  final protected int servletSELMUES = 1;

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();

    DatTotMuesCS dataEntrada = null;
    DatTotMuesSC dataSalida = null;

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // Recuperacion de los datos de busqueda
      dataEntrada = (DatTotMuesCS) param.firstElement();

      switch (opmode) {
        case servletSELMUES:

          // Preparacion del patron de la query
          sQuery =
              "SELECT CD_E_NOTIF, NM_RESLAB, CD_ANOEPI, FC_MUESTRA, CD_SEMEPI, CD_TTECLAB, " +
              "CD_MUESTRA, CD_VMUESTRA, CD_TMICOB, IT_RESISTENTE " +
              "FROM SIVE_RESLAB " +
              "WHERE NM_EDO = ?";

          // Ordenamiento
          sQuery = sQuery + " ORDER BY CD_ANOEPI ASC";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciamos la query con los valores de los datos de entrada
          InstanciarQuery(dataEntrada, st);

          // Ejecucion de la query, obteniendo un ResultSet
          rs = st.executeQuery();

          // Procesamiento de cada registro obtenido de la BD
          while (rs.next()) {

            // Recuperacion del CD_E_NOTIF, NM_RESLAB, FC_MUESTRA, CD_TTECLAB, CD_MUESTRA,
            //  CD_VMUESTRA,CD_TMICOB,IT_RESISTENTE
            //  a partir del ResultSet de la query realizada sobre la BD
            int iNMues = rs.getInt("NM_RESLAB");
            String NMues = (new Integer(iNMues)).toString();
            String CodNotif = rs.getString("CD_E_NOTIF");
            String CodAno = rs.getString("CD_ANOEPI");
            String FMues = UtilDateToString(rs.getDate("FC_MUESTRA"));
            String CodSem = rs.getString("CD_SEMEPI");
            String CodTLab = rs.getString("CD_TTECLAB");
            String DescTLab = getDescTLab(con, CodTLab);
            String CodMues = rs.getString("CD_MUESTRA");
            String DescMues = getDescMues(con, CodMues);
            String CodVMues = rs.getString("CD_VMUESTRA");
            String DescVMues = getDescVMues(con, CodVMues);
            String CodTMicob = rs.getString("CD_TMICOB");
            String DescTMicob = getDescTMicob(con, CodTMicob);
            String FlagResis = rs.getString("IT_RESISTENTE"); ;
            String Ope = "";
            String FUlt = "";

            // Relleno del registro de salida correspondiente a esta iteracion por
            //  los registros devueltos por la query
            dataSalida = new DatTotMuesSC(CodNotif, NMues, CodAno, FMues,
                                          CodSem, CodTLab, DescTLab,
                                          CodMues, DescMues, CodVMues,
                                          DescVMues,
                                          CodTMicob, DescTMicob, FlagResis, Ope,
                                          FUlt);

            // Adicion del registro obtenido a la lista resultado de salida hacia cliente
            listaSalida.addElement(dataSalida);
          } // Fin for each registro

          // Cierre del ResultSet y del PreparedStatement
          rs.close();
          rs = null;
          st.close();
          st = null;

          break;
      } //fin switch

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }
    finally {
      closeConnection(con);
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void InstanciarQuery(DatTotMuesCS dataEntrada,
                                 PreparedStatement st) throws Exception {

    // Contador de posicion del parametro
    int iParN = 1;

    /*
         // A�o epidemiologico
         st.setString(iParN++, dataEntrada.getCD_ANOEPI());
     */

    // Numero de EDO
    String nmEdo = dataEntrada.getNM_EDO().trim();
    st.setInt(iParN, (new Integer(nmEdo)).intValue());

    /*
         // Semana epidemiologica
         st.setString(iParN++, dataEntrada.getCD_SEMEPI());
         // Codigo del equipo notificador
         st.setString(iParN++, dataEntrada.getCD_E_NOTIF());
         // Fuente
         st.setString(iParN++, dataEntrada.getCD_FUENTE());
         // Fecha recepcion
         String FRecep = dataEntrada.getFC_RECEP().trim();
         if (FRecep.length() > 0) {
      java.sql.Date fRecep = StringToSQLDate(FRecep);
      st.setDate(iParN++, fRecep);
         }
         // Fecha notificacion
         String FNotif = dataEntrada.getFC_FECNOTIF().trim();
         if (FNotif.length() > 0) {
      java.sql.Date fNotif = StringToSQLDate(FNotif);
      st.setDate(iParN++, fNotif);
         }
     */

  } // Fin InstanciarQuery()

  // Conversor de fecha String a java.sql.Date
  protected java.sql.Date StringToSQLDate(String sFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    java.util.Date uFecha = formater.parse(sFecha);
    java.sql.Date sqlFec = new java.sql.Date(uFecha.getTime());
    return sqlFec;
  } // Fin StringToSQLDate()

  // Conversor de fecha java.util.Date a String
  protected String UtilDateToString(java.util.Date uFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    String sFecha = "";
    if (uFecha == null) {
      sFecha = "";
    }
    else {
      sFecha = formater.format(uFecha);

    }
    return (sFecha);
  } // Fin UtilDateToString()

  // Obtiene la descr. de una tecnica de laboratorio
  String getDescTLab(Connection c, String CodTLab) throws Exception {
    if (CodTLab == null || CodTLab.equals("")) {
      return "";
    }

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Query
    String Query =
        "select DS_TTECLAB from SIVE_TIPO_TECNICALAB where CD_TTECLAB = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);

      // CodTLab
      st.setString(1, CodTLab.trim());

      // Ejecucion de la query
      rs = st.executeQuery();

      // Almacenamos la DS_TTECLAB obtenida
      if (rs.next()) {
        res = rs.getString("DS_TTECLAB");

        // Cierre del ResultSet y del PreparedStatement
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }

    return res;
  } // Fin getDescTLab()

  // Obtiene la descr. de una muestra
  String getDescMues(Connection c, String CodMues) throws Exception {
    if (CodMues == null || CodMues.equals("")) {
      return "";
    }

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Query
    String Query =
        "select DS_MUESTRA from SIVE_MUESTRA_LAB where CD_MUESTRA = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);

      // CodMues
      st.setString(1, CodMues.trim());

      // Ejecucion de la query
      rs = st.executeQuery();

      // Almacenamos la DS_MUESTRA obtenida
      if (rs.next()) {
        res = rs.getString("DS_MUESTRA");

        // Cierre del ResultSet y del PreparedStatement
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }

    return res;
  } // Fin getDescMues()

  // Obtiene la descr. de una muestra
  String getDescVMues(Connection c, String CodVMues) throws Exception {
    if (CodVMues == null || CodVMues.equals("")) {
      return "";
    }

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Query
    String Query =
        "select DS_VMUESTRA from SIVE_VALOR_MUESTRA where CD_VMUESTRA = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);

      // CodVMues
      st.setString(1, CodVMues.trim());

      // Ejecucion de la query
      rs = st.executeQuery();

      // Almacenamos la DS_VMUESTRA obtenida
      if (rs.next()) {
        res = rs.getString("DS_VMUESTRA");

        // Cierre del ResultSet y del PreparedStatement
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }

    return res;
  } // Fin getDescVMues()

  // Obtiene la descr. de una muestra
  String getDescTMicob(Connection c, String CodTMicob) throws Exception {
    if (CodTMicob == null || CodTMicob.equals("")) {
      return "";
    }

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Query
    String Query =
        "select DS_TMICOB from SIVE_TMICOBACTERIA where CD_TMICOB = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);

      // CodTMicob
      st.setString(1, CodTMicob.trim());

      // Ejecucion de la query
      rs = st.executeQuery();

      // Almacenamos la DS_TMICOB obtenida
      if (rs.next()) {
        res = rs.getString("DS_TMICOB");

        // Cierre del ResultSet y del PreparedStatement
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }

    return res;
  } // Fin getDescTMicob()

  static public void main(String args[]) {

    SrvSelRes srv = new SrvSelRes();
    CLista listaSalida = new CLista();
    //DatTotMuesCS hs = null;
    DatTotMuesCS hs = new DatTotMuesCS(null, null, null, null, null, null, null, null, null, null, null, null);
    CLista resultado = null;

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    //hs.setCD_ANOEPI("1999");
    hs.setNM_EDO("252");

    listaSalida.addElement(hs);
    resultado = srv.doDebug(1, listaSalida);
    resultado = null;

  }

} // Fin SrvSelRes
