/**
 * Clase: SrvCatCasoTub
 * Paquete: tuberculosis.servidor.mantcasotub
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 19/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Implementacion del servlet que recupera de la BD los
 *   cat�logos que ser�n utilizados tanto en el panel de mantenimiento
 *   de casos como en los di�logos y paneles que de �l se desprenden.
 */

package tuberculosis.servidor.mantcasotub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import comun.DataGeneralCDDS;
import sapp.DBServlet;
import suca.datasuca;

public class SrvCatCasoTub
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista(); // Lista que contendra las siguientes listas
    CLista listaClasificaciones = new CLista();
    CLista listaPaises = new CLista();
    CLista listaCA = new CLista();
    CLista listaTrameros = new CLista();
    CLista listaNumeracion = new CLista();
    CLista listaCalificadores = new CLista();
    CLista listaSexos = new CLista();
    CLista listaTDoc = new CLista();
    CLista listaFuentesNotif = new CLista();
    CLista listaMotivosTrat = new CLista();
    CLista listaTecnicasLaboratorio = new CLista();
    CLista listaMuestrasLaboratorio = new CLista();
    CLista listaValoresMuestra = new CLista();
    CLista listaTiposMicobacteria = new CLista();
    CLista listaEstudiosResistencia = new CLista();

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que el commit
    //  se haga autom�ticamente sobre cada actualizaci�n
    con = openConnection();
    con.setAutoCommit(false);

    // Vars. auxiliares
    String ds = "";
    String dsl = "";
    String sDes = "";
    String dsc = "";
    String dsds = "";
    String dsa = "";
    String dstv = null;

    try {
      // No hay que recuperar ning�n dato de entrada

      /************* CLISTA DE CLASIFICACIONES **************/

      // Preparaci�n de la query completa (clasificaciones)
      sQuery = "SELECT CD_CLASIFDIAG, DS_CLASIFDIAG, DSL_CLASIFDIAG " +
          "FROM SIVE_CLASIF_EDO ORDER BY CD_CLASIFDIAG";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      //ds = ""; dsl= ""; sDes = "";
      DataGeneralCDDS dataClasif = null;
      while (rs.next()) {
        ds = rs.getString("DS_CLASIFDIAG");
        dsl = rs.getString("DSL_CLASIFDIAG");
        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (dsl != null)) {
          sDes = dsl;
        }
        else {
          sDes = ds;

        }
        dataClasif = new DataGeneralCDDS(rs.getString("CD_CLASIFDIAG"), sDes);
        listaClasificaciones.addElement(dataClasif);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE CLASIFICACIONES **************/

      /************* CLISTA DE PAISES **************/

      // Preparaci�n de la query completa (paises)
      sQuery = "select CD_PAIS, DS_PAIS from SIVE_PAISES";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      datasuca dataPais = null;
      while (rs.next()) {
        dataPais = new datasuca();
        dataPais.put("CD_PAIS", rs.getString("CD_PAIS"));
        dataPais.put("DS_PAIS", rs.getString("DS_PAIS"));
        listaPaises.addElement(dataPais);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE PAISES **************/

      /************* CLISTA DE COMUNIDADES AUTONOMAS SUCA **************/

      // Preparaci�n de la query completa (CAs)
      sQuery = " select CDCOMU, DSCOMU from SUCA_AUTONOMIAS ";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      datasuca dataComunidad = null;
      while (rs.next()) {
        dataComunidad = new datasuca();

        dataComunidad.put("CD_CA", rs.getString("CDCOMU"));
        dataComunidad.put("DS_CA", rs.getString("DSCOMU"));

        listaCA.addElement(dataComunidad);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE COMUNIDADES AUTONOMAS SUCA **************/

      /************* CLISTA DE COMUNIDADES AUTONOMAS SIVE **************/

      /*      // Preparaci�n de la query completa (CAs)
            sQuery = "select CD_CA, DS_CA, DSL_CA from SIVE_COM_AUT";
            // Preparaci�n de la sentencia SQL
            st = con.prepareStatement(sQuery);
            // Ejecuci�n de la query obteniendo un resultset
            //System.out.println(sQuery);
            rs = st.executeQuery();
            // Procesamiento de cada registro obtenido de la BD
            //ds = ""; dsl= ""; sDes = "";
            datasuca dataComunidad = null;
            while (rs.next()) {
              ds = rs.getString("DS_CA");
              dsl = rs.getString("DSL_CA");
              if ((param.getIdioma()!=CApp.idiomaPORDEFECTO) && (dsl!=null))
                sDes = dsl;
              else
                sDes = ds;
              dataComunidad = new datasuca();
              dataComunidad.put("CD_CA", rs.getString("CD_CA"));
              dataComunidad.put("DS_CA", sDes);
              listaCA.addElement(dataComunidad);
            }
            // Cierre del ResultSet y del PreparedStatement
            rs.close();
            rs = null;
            st.close();
            st = null; */

      /************* FIN CLISTA DE COMUNIDADES AUTONOMAS SIVE **************/

      /************* CLISTA DE TRAMEROS **************/

      // Preparaci�n de la query completa (Trameros)
      sQuery = "select CDTVIA, DSTVIA, DSTVABR from SUCA_TIPO_VIAL";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      datasuca dataTramero = null;
      while (rs.next()) {
        dataTramero = new datasuca();
        dataTramero.put("CDTVIA", rs.getString("CDTVIA"));
        dataTramero.put("DSTVIA", rs.getString("DSTVIA"));
        dstv = rs.getString("DSTVABR");
        if (dstv != null) {
          dataTramero.put("DSTVABR", dstv);

        }
        listaTrameros.addElement(dataTramero);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE TRAMEROS **************/

      /************* CLISTA DE NUMEROS **************/

      // Preparaci�n de la query completa (CAs)
      sQuery = "select CDTNUM, DSTNUM, DSABREV from SUCA_TIPO_NUMERACION";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      datasuca dataNumeracion = null;
      while (rs.next()) {
        dataNumeracion = new datasuca();
        dataNumeracion.put("CDTNUM", rs.getString("CDTNUM"));
        dataNumeracion.put("DSTNUM", rs.getString("DSTNUM"));
        dstv = rs.getString("DSABREV");
        if (dstv != null) {
          dataNumeracion.put("DSABREV", dstv);

        }
        listaNumeracion.addElement(dataNumeracion);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE NUMEROS **************/

      /************* CLISTA DE CLASIFICADORES **************/

      // Preparaci�n de la query completa (CAs)
      sQuery = "select DSCALNUM, DSDSCALNUM, DSABREV from SUCA_CALIFICADOR ";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      //dsc = ""; dsds = ""; dsa = "";
      datasuca dataCalificacion = null;
      while (rs.next()) {
        dataCalificacion = new datasuca();
        dsc = rs.getString("DSCALNUM");
        dsds = rs.getString("DSDSCALNUM");
        dsa = rs.getString("DSABREV");

        dataCalificacion.put("DSCALNUM", dsc);
        if (dsds != null) {
          dataCalificacion.put("DSDSCALNUM", dsds);
        }
        if (dsds != null) {
          dataCalificacion.put("DSABREV", dsa);

        }
        listaCalificadores.addElement(dataCalificacion);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE CLASIFICADORES **************/

      /************* CLISTA DE SEXOS **************/

      // Preparaci�n de la query completa (motivos de salida)
      sQuery = "SELECT CD_SEXO, DS_SEXO, DSL_SEXO " +
          "FROM SIVE_SEXO ORDER BY CD_SEXO";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DataGeneralCDDS dataSexo = null;
      while (rs.next()) {
        dataSexo = new DataGeneralCDDS(rs.getString("CD_SEXO"),
                                       rs.getString("DS_SEXO"));
        listaSexos.addElement(dataSexo);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE SEXOS **************/

      /************* CLISTA DE TIPOSDOC **************/

      // Preparaci�n de la query completa (motivos de salida)
      sQuery = "SELECT CD_TDOC, DS_TIPODOC, DSL_TIPODOC " +
          "FROM SIVE_T_DOC ORDER BY CD_TDOC";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      //ds = ""; dsl= ""; sDes = "";
      DataGeneralCDDS dataTDoc = null;
      while (rs.next()) {
        ds = rs.getString("DS_TIPODOC");
        dsl = rs.getString("DSL_TIPODOC");
        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (dsl != null)) {
          sDes = dsl;
        }
        else {
          sDes = ds;

        }
        dataTDoc = new DataGeneralCDDS(rs.getString("CD_TDOC"), sDes);
        listaTDoc.addElement(dataTDoc);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE TIPOSDOC **************/

      /************* CLISTA DE FUENTES NOTIFICADORAS **************/

      // Preparaci�n de la query completa (fuentes notificadoras)
      sQuery = "SELECT CD_FUENTE, DS_FUENTE " +
          "FROM SIVE_FUENTES_NOTIF ORDER BY CD_FUENTE";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DataGeneralCDDS dataFuente = null;
      while (rs.next()) {
        dataFuente = new DataGeneralCDDS(rs.getString("CD_FUENTE"),
                                         rs.getString("DS_FUENTE"));
        listaFuentesNotif.addElement(dataFuente);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE FUENTES NOTIFICADORAS **************/

      /************* CLISTA DE MOTIVOS TRATAMIENTO **************/

      // Preparaci�n de la query completa (motivos tratamiento)
      sQuery = "SELECT CD_MOTRAT, DS_MOTRAT " +
          "FROM SIVE_MOTIVO_TRAT ORDER BY CD_MOTRAT";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DataGeneralCDDS dataMotTrat = null;
      while (rs.next()) {
        dataMotTrat = new DataGeneralCDDS(rs.getString("CD_MOTRAT"),
                                          rs.getString("DS_MOTRAT"));
        listaMotivosTrat.addElement(dataMotTrat);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE MOTIVOS TRATAMIENTO **************/

      /************* CLISTA DE TECNICAS LABORATORIO **************/

      // Preparaci�n de la query completa (tecnicas laboratorio)
      sQuery = "SELECT CD_TTECLAB, DS_TTECLAB " +
          "FROM SIVE_TIPO_TECNICALAB ORDER BY CD_TTECLAB";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DataGeneralCDDS dataTLab = null;
      while (rs.next()) {
        dataTLab = new DataGeneralCDDS(rs.getString("CD_TTECLAB"),
                                       rs.getString("DS_TTECLAB"));
        listaTecnicasLaboratorio.addElement(dataTLab);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE TECNICAS LABORATORIO **************/

      /************* CLISTA DE MUESTRAS LABORATORIO **************/

      // Preparaci�n de la query completa (muestras laboratorio)
      sQuery = "SELECT CD_MUESTRA, DS_MUESTRA " +
          "FROM SIVE_MUESTRA_LAB ORDER BY CD_MUESTRA";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DataGeneralCDDS dataMuesLab = null;
      while (rs.next()) {
        dataMuesLab = new DataGeneralCDDS(rs.getString("CD_MUESTRA"),
                                          rs.getString("DS_MUESTRA"));
        listaMuestrasLaboratorio.addElement(dataMuesLab);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE MUESTRAS LABORATORIO **************/

      /************* CLISTA DE VALORES MUESTRA **************/

      // Preparaci�n de la query completa (valores muestras)
      sQuery = "SELECT CD_VMUESTRA, DS_VMUESTRA " +
          "FROM SIVE_VALOR_MUESTRA ORDER BY CD_VMUESTRA";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DataGeneralCDDS dataVMues = null;
      while (rs.next()) {
        dataVMues = new DataGeneralCDDS(rs.getString("CD_VMUESTRA"),
                                        rs.getString("DS_VMUESTRA"));
        listaValoresMuestra.addElement(dataVMues);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE VALORES MUESTRA **************/

      /************* CLISTA DE TIPOS MICOBACTERIA **************/

      // Preparaci�n de la query completa (tipos micobacterias)
      sQuery = "SELECT CD_TMICOB, DS_TMICOB " +
          "FROM SIVE_TMICOBACTERIA ORDER BY CD_TMICOB";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DataGeneralCDDS dataTMico = null;
      while (rs.next()) {
        dataTMico = new DataGeneralCDDS(rs.getString("CD_TMICOB"),
                                        rs.getString("DS_TMICOB"));
        listaTiposMicobacteria.addElement(dataTMico);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE TIPOS MICOBACTERIA **************/
      /************* CLISTA DE ESTUDIOS RESISTENCIAS **************/

      // Preparaci�n de la query completa (estudios resistencias)
      sQuery = "SELECT CD_ESTREST, DS_ESTREST " +
          "FROM SIVE_ESTUDIORESIS ORDER BY CD_ESTREST";

      // Preparaci�n de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Ejecuci�n de la query obteniendo un resultset
      //System.out.println(sQuery);
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      DataGeneralCDDS dataEstResis = null;
      while (rs.next()) {
        dataEstResis = new DataGeneralCDDS(rs.getString("CD_ESTREST"),
                                           rs.getString("DS_ESTREST"));
        listaEstudiosResistencia.addElement(dataEstResis);
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      /************* FIN CLISTA DE ESTUDIOS RESISTENCIAS **************/

      // Preparaci�n de la lista resultado a devolver al cliente
      listaSalida.addElement(listaClasificaciones);
      listaSalida.addElement(listaPaises);
      listaSalida.addElement(listaCA);
      listaSalida.addElement(listaTrameros);
      listaSalida.addElement(listaNumeracion);
      listaSalida.addElement(listaCalificadores);
      listaSalida.addElement(listaSexos);
      listaSalida.addElement(listaTDoc);
      listaSalida.addElement(listaFuentesNotif);
      listaSalida.addElement(listaMotivosTrat);
      listaSalida.addElement(listaTecnicasLaboratorio);
      listaSalida.addElement(listaMuestrasLaboratorio);
      listaSalida.addElement(listaValoresMuestra);
      listaSalida.addElement(listaTiposMicobacteria);
      listaSalida.addElement(listaEstudiosResistencia);

      // Validaci�n de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexi�n con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

} // Fin clase SrvCatCasoTub
