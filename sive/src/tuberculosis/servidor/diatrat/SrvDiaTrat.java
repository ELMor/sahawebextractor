/**
 * Clase: SrvDiaTrat
 * Paquete: tuberculosis.servidor.diatrat
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 23/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Implementacion del servlet que permite dar de alta,
 *   baja, modificar un tratamiento.
 */

package tuberculosis.servidor.diatrat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import capp.CLista;
import comun.Fechas;
import comun.constantes;
import sapp.DBServlet;
import tuberculosis.datos.pantrat.DatCasTratCS;
import tuberculosis.datos.pantrat.DatCasTratSC;

public class SrvDiaTrat
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatCasTratCS notif = null;
    DatCasTratSC trat = null;
    DatCasTratSC result = null;
    java.util.Date dFechaUltact = null;
    SimpleDateFormat formUltact = null;
    String sOpe;
    String sFechaUltact = null;

    // Querys
    String sQuery = "";
    // Indicador de bloqueo
    boolean bBloqueo = true;

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    // Datos del tratamiento actual
    notif = (DatCasTratCS) param.elementAt(0);
    trat = (DatCasTratSC) param.elementAt(1);

    try {

      switch (opmode) {
        case constantes.modoALTA_SB:
          bBloqueo = false;
        case constantes.modoALTA:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado la notif no se da de alta el trat
          if (bBloqueo) {
            if (notificacionModificada(con, notif)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_NOTIF_EDOI));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "INSERT INTO SIVE_TRATAMIENTOS " +
              " (NM_TRATRTBC,CD_E_NOTIF,CD_ANOEPI,CD_SEMEPI, FC_RECEP, FC_FECNOTIF, " +
              "  NM_EDO, CD_FUENTE, FC_INITRAT, CD_MOTRATINI,FC_FINTRAT, CD_MOTRATFIN, DS_OBSERV) " +
              " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

          // Buscamos en la tabla de secuenciadores el siguiente numero de tratamiento
          int iNextTrat = sapp.Funciones.SecGeneral(con, st, rs, "NM_TRATRTBC");

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarInsertQuery(notif, trat, st, iNextTrat);

          // Ejecucion del insert
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = trat.getCD_OPE();

          if (bBloqueo) { //si es true es con bloqueo
            modificarNotificacion(con, notif, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarNotificacion_SB(con, notif, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos el sintoma modificado
          result = new DatCasTratSC( (new Integer(iNextTrat)).toString(),
                                    trat.getFC_INITRAT(), trat.getCD_MOTRATINI(),
                                    trat.getDS_MOTRATINI(),
                                    trat.getFC_FINTRAT(), trat.getCD_MOTRATFIN(),
                                    trat.getDS_MOTRATFIN(),
                                    trat.getDS_OBSERV(), sOpe, sFechaUltact);

          listaSalida.addElement(result);
          break;

        case constantes.modoMODIFICACION_SB:
          bBloqueo = false;
        case constantes.modoMODIFICACION:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado la notificacion no se modif el trat
          if (bBloqueo) {
            if (notificacionModificada(con, notif)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_NOTIF_EDOI));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "UPDATE SIVE_TRATAMIENTOS " +
              "SET FC_INITRAT = ?,CD_MOTRATINI = ?," +
              "FC_FINTRAT = ?,CD_MOTRATFIN = ?,DS_OBSERV = ? " +
              "WHERE NM_TRATRTBC = ? AND CD_E_NOTIF = ? AND " +
              "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
              "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
              "NM_EDO = ? AND CD_FUENTE = ?";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarUpdateQuery(notif, trat, st);

          // Ejecucion del update
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = trat.getCD_OPE();

          if (bBloqueo) { //si es true es con bloqueo
            modificarNotificacion(con, notif, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarNotificacion_SB(con, notif, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos el nuevo tratamiento
          result = new DatCasTratSC(trat.getNM_TRATRTBC(),
                                    trat.getFC_INITRAT(), trat.getCD_MOTRATINI(),
                                    trat.getDS_MOTRATINI(),
                                    trat.getFC_FINTRAT(), trat.getCD_MOTRATFIN(),
                                    trat.getDS_MOTRATFIN(),
                                    trat.getDS_OBSERV(), sOpe, sFechaUltact);
          listaSalida.addElement(result);

          break;
        case constantes.modoBAJA_SB:
          bBloqueo = false;
        case constantes.modoBAJA:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado la notificacion no se da de baja el trat
          if (bBloqueo) {
            if (notificacionModificada(con, notif)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_NOTIF_EDOI));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "DELETE FROM SIVE_TRATAMIENTOS " +
              "WHERE NM_TRATRTBC = ? AND CD_E_NOTIF = ? AND " +
              "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
              "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
              "NM_EDO = ? AND CD_FUENTE = ?";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarDeleteQuery(notif, trat, st);

          // Ejecucion del delete
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = trat.getCD_OPE();

          if (bBloqueo) { //si es true es con bloqueo
            modificarNotificacion(con, notif, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarNotificacion_SB(con, notif, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos el nuevo tratamiento
          result = new DatCasTratSC(trat.getNM_TRATRTBC(),
                                    trat.getFC_INITRAT(), trat.getCD_MOTRATINI(),
                                    trat.getDS_MOTRATINI(),
                                    trat.getFC_FINTRAT(), trat.getCD_MOTRATFIN(),
                                    trat.getDS_MOTRATFIN(),
                                    trat.getDS_OBSERV(), sOpe, sFechaUltact);
          listaSalida.addElement(result);

          break;

      }

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarInsertQuery(DatCasTratCS notif,
                                       DatCasTratSC trat,
                                       PreparedStatement st,
                                       int nextTrat) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    // Numero de tratamiento SECUENCIADOR !!!
    st.setInt(iParN++, nextTrat);

    /************ Campos de notif *****************/

    // Codigo del equipo notificador
    st.setString(iParN++, notif.getCD_E_NOTIF());

    // A�o epidemiologico
    st.setString(iParN++, notif.getCD_ANOEPI());

    // Semana epidemiologica
    st.setString(iParN++, notif.getCD_SEMEPI());

    // Fecha recepcion
    String FRecep = notif.getFC_RECEP().trim();
    if (FRecep.length() > 0) {
      java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
      st.setDate(iParN++, fRecep);
    }

    // Fecha notificacion
    String FNotif = notif.getFC_FECNOTIF().trim();
    if (FNotif.length() > 0) {
      java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
      st.setDate(iParN++, fNotif);
    }

    // Numero de EDO
    String nmEdo = notif.getNM_EDO().trim();
    st.setInt(iParN++, (new Integer(nmEdo)).intValue());

    // Fuente
    st.setString(iParN++, notif.getCD_FUENTE());

    /************ Campos de trat *****************/

    // Fecha Inicio Tratamiento
    String FIni = trat.getFC_INITRAT().trim();
    if (FIni.length() > 0) {
      java.sql.Date fIni = Fechas.StringToSQLDate(FIni);
      st.setDate(iParN++, fIni);
    }
    else {
      st.setNull(iParN++, java.sql.Types.DATE);

      // Motivo Tratamiento Inicio
    }
    String sMotIni = trat.getCD_MOTRATINI();
    if (!sMotIni.equals("")) {
      st.setString(iParN++, sMotIni);
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Fecha FinTratamiento
    }
    String FFin = trat.getFC_FINTRAT().trim();
    if (FFin.length() > 0) {
      java.sql.Date fFin = Fechas.StringToSQLDate(FFin);
      st.setDate(iParN++, fFin);
    }
    else {
      st.setNull(iParN++, java.sql.Types.DATE);

      // Motivo Tratamiento Fin
    }
    String sMotFin = trat.getCD_MOTRATFIN();
    if (!sMotFin.equals("")) {
      st.setString(iParN++, sMotFin);
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Observaciones
    }
    String sObserv = trat.getDS_OBSERV();
    if (!sObserv.equals("")) {
      st.setString(iParN++, sObserv);
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

    }
  } // Fin instanciarInsertQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarUpdateQuery(DatCasTratCS notif,
                                       DatCasTratSC trat,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    /************** Par�metros del SET ***************/

    // Fecha Inicio Tratamiento
    String FIni = trat.getFC_INITRAT().trim();
    if (FIni.length() > 0) {
      java.sql.Date fIni = Fechas.StringToSQLDate(FIni);
      st.setDate(iParN++, fIni);
    }
    else {
      st.setNull(iParN++, java.sql.Types.DATE);

      // Motivo Tratamiento Inicio
    }
    String sMotIni = trat.getCD_MOTRATINI();
    if (!sMotIni.equals("")) {
      st.setString(iParN++, sMotIni);
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Fecha FinTratamiento
    }
    String FFin = trat.getFC_FINTRAT().trim();
    if (FFin.length() > 0) {
      java.sql.Date fFin = Fechas.StringToSQLDate(FFin);
      st.setDate(iParN++, fFin);
    }
    else {
      st.setNull(iParN++, java.sql.Types.DATE);

      // Motivo Tratamiento Fin
    }
    String sMotFin = trat.getCD_MOTRATFIN();
    if (!sMotFin.equals("")) {
      st.setString(iParN++, sMotFin);
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Observaciones
    }
    String sObserv = trat.getDS_OBSERV();
    if (!sObserv.equals("")) {
      st.setString(iParN++, sObserv);
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      /************** Par�metros del WHERE ****************/

      // Numero de tratamiento SECUENCIADOR !!!
    }
    String iNumTrat = trat.getNM_TRATRTBC().trim();
    st.setInt(iParN++, (new Integer(iNumTrat)).intValue());

    // Codigo del equipo notificador
    st.setString(iParN++, notif.getCD_E_NOTIF());

    // A�o epidemiologico
    st.setString(iParN++, notif.getCD_ANOEPI());

    // Semana epidemiologica
    st.setString(iParN++, notif.getCD_SEMEPI());

    // Fecha recepcion
    String FRecep = notif.getFC_RECEP().trim();
    if (FRecep.length() > 0) {
      java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
      st.setDate(iParN++, fRecep);
    }

    // Fecha notificacion
    String FNotif = notif.getFC_FECNOTIF().trim();
    if (FNotif.length() > 0) {
      java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
      st.setDate(iParN++, fNotif);
    }

    // Numero de EDO
    String nmEdo = notif.getNM_EDO().trim();
    st.setInt(iParN++, (new Integer(nmEdo)).intValue());

    // Fuente
    st.setString(iParN++, notif.getCD_FUENTE());
  } // Fin instanciarUpdateQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarDeleteQuery(DatCasTratCS notif,
                                       DatCasTratSC trat,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    // Numero de tratamiento SECUENCIADOR !!!
    String iNumTrat = trat.getNM_TRATRTBC().trim();
    st.setInt(iParN++, (new Integer(iNumTrat)).intValue());

    // Codigo del equipo notificador
    st.setString(iParN++, notif.getCD_E_NOTIF());

    // A�o epidemiologico
    st.setString(iParN++, notif.getCD_ANOEPI());

    // Semana epidemiologica
    st.setString(iParN++, notif.getCD_SEMEPI());

    // Fecha recepcion
    String FRecep = notif.getFC_RECEP().trim();
    if (FRecep.length() > 0) {
      java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
      st.setDate(iParN++, fRecep);
    }

    // Fecha notificacion
    String FNotif = notif.getFC_FECNOTIF().trim();
    if (FNotif.length() > 0) {
      java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
      st.setDate(iParN++, fNotif);
    }

    // Numero de EDO
    String nmEdo = notif.getNM_EDO().trim();
    st.setInt(iParN++, (new Integer(nmEdo)).intValue());

    // Fuente
    st.setString(iParN++, notif.getCD_FUENTE());

  } // Fin instanciarDeleteQuery()

  // Comprueba el CD_OPE y FC_ULTACT de la notificacion
  //  de la BD con la notificacion del cliente
  boolean notificacionModificada(Connection c, DatCasTratCS notif) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery;
    String ope, fultact;

    try {
      sQuery = "SELECT CD_OPE,FC_ULTACT FROM SIVE_NOTIF_EDOI " +
          "WHERE CD_E_NOTIF = ? AND " +
          "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
          "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
          "NM_EDO = ? AND CD_FUENTE = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // Codigo del equipo notificador
      st.setString(iParN++, notif.getCD_E_NOTIF());

      // A�o epidemiologico
      st.setString(iParN++, notif.getCD_ANOEPI());

      // Semana epidemiologica
      st.setString(iParN++, notif.getCD_SEMEPI());

      // Fecha recepcion
      String FRecep = notif.getFC_RECEP().trim();
      if (FRecep.length() > 0) {
        java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
        st.setDate(iParN++, fRecep);
      }

      // Fecha notificacion
      String FNotif = notif.getFC_FECNOTIF().trim();
      if (FNotif.length() > 0) {
        java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
        st.setDate(iParN++, fNotif);
      }

      // Numero de EDO
      String nmEdo = notif.getNM_EDO().trim();
      st.setInt(iParN++, (new Integer(nmEdo)).intValue());

      // Fuente
      st.setString(iParN++, notif.getCD_FUENTE());

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion de la query
      rs = st.executeQuery();

      // Obtenemos CD_OPE y FC_ULTACT
      if (rs.next()) {
        ope = rs.getString("CD_OPE");
        fultact = Fechas.timestamp2String(rs.getTimestamp("FC_ULTACT"));
      }
      else {
        return true;
      }

      // Cierre del PreparedStatement y del ResultSet
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      return true;
    }

    if (!ope.equals(notif.getCD_OPE()) || !fultact.equals(notif.getFC_ULTACT())) {
      return true;
    }
    else {
      return false;
    }

  } // Fin notificacionModificada()

  // Modifica el CD_OPE y FC_ULTACT de la notificacion
  void modificarNotificacion(Connection c, DatCasTratCS notif, String sOpe,
                             String sFechaUltact) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_NOTIF_EDOI SET CD_OPE = ?,FC_ULTACT = ? " +
          "WHERE CD_E_NOTIF = ? AND " +
          "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
          "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
          "NM_EDO = ? AND CD_FUENTE = ? AND CD_OPE = ? AND FC_ULTACT = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // Parte del SET
      // CD_OPE NUEVO
      st.setString(iParN++, sOpe);
      // FC_ULTACT NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact);

      // Codigo del equipo notificador
      st.setString(iParN++, notif.getCD_E_NOTIF());

      // A�o epidemiologico
      st.setString(iParN++, notif.getCD_ANOEPI());

      // Semana epidemiologica
      st.setString(iParN++, notif.getCD_SEMEPI());

      // Fecha recepcion
      String FRecep = notif.getFC_RECEP().trim();
      if (FRecep.length() > 0) {
        java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
        st.setDate(iParN++, fRecep);
      }

      // Fecha notificacion
      String FNotif = notif.getFC_FECNOTIF().trim();
      if (FNotif.length() > 0) {
        java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
        st.setDate(iParN++, fNotif);
      }

      // Numero de EDO
      String nmEdo = notif.getNM_EDO().trim();
      st.setInt(iParN++, (new Integer(nmEdo)).intValue());

      // Fuente
      st.setString(iParN++, notif.getCD_FUENTE());

      // CD_OPE ANTIGUO
      st.setString(iParN++, notif.getCD_OPE());
      // FC_ULTACT ANTIGUO
      st.setTimestamp(iParN++, Fechas.string2Timestamp(notif.getFC_ULTACT()));

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion del update
      int iActualizados = st.executeUpdate();

      //PREGUNTAR si ha modificado algo (ver de nuevo el bloqueo)
      if (iActualizados == 0) {
        throw (new Exception(constantes.PRE_BLOQUEO +
                             constantes.SIVE_NOTIF_EDOI));
      }

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarNotificacion()

  // Modifica el CD_OPE y FC_ULTACT de la notificacion sin bloqueo
  void modificarNotificacion_SB(Connection c, DatCasTratCS notif, String sOpe,
                                String sFechaUltact) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_NOTIF_EDOI SET CD_OPE = ?,FC_ULTACT = ? " +
          "WHERE CD_E_NOTIF = ? AND " +
          "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
          "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
          "NM_EDO = ? AND CD_FUENTE = ? ";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // Parte del SET
      // CD_OPE NUEVO
      st.setString(iParN++, sOpe);
      // FC_ULTACT NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact);

      // Codigo del equipo notificador
      st.setString(iParN++, notif.getCD_E_NOTIF());

      // A�o epidemiologico
      st.setString(iParN++, notif.getCD_ANOEPI());

      // Semana epidemiologica
      st.setString(iParN++, notif.getCD_SEMEPI());

      // Fecha recepcion
      String FRecep = notif.getFC_RECEP().trim();
      if (FRecep.length() > 0) {
        java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
        st.setDate(iParN++, fRecep);
      }

      // Fecha notificacion
      String FNotif = notif.getFC_FECNOTIF().trim();
      if (FNotif.length() > 0) {
        java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
        st.setDate(iParN++, fNotif);
      }

      // Numero de EDO
      String nmEdo = notif.getNM_EDO().trim();
      st.setInt(iParN++, (new Integer(nmEdo)).intValue());

      // Fuente
      st.setString(iParN++, notif.getCD_FUENTE());

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion del update
      st.executeUpdate();

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarNotificacion_SB()

} // Fin SrvDiaTrat
