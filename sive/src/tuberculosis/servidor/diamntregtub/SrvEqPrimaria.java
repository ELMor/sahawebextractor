/**
 * Clase: SrvEqPrimaria
 * Paquete: tuberculosis.servidor.diamntregtub
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 10/11/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Equipos de Primaria: con CD_NIVASIS=P
 */

package tuberculosis.servidor.diamntregtub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import comun.constantes;
import sapp.DBServlet;
import tuberculosis.datos.diamntregtub.DatEqPrim;

public class SrvEqPrimaria
    extends DBServlet {

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "";
    String sFiltro = "";
    String sAutorizacion1 = "";
    String sAutorizacion2 = "";
    Vector vN1 = null;
    Vector vN2 = null;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // objetos de datos
    CLista listaSalida = new CLista();
    DatEqPrim ent = null;
    DatEqPrim dataSalida = null;
    DatEqPrim dataLinea = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    String des = "";
    String desL = "";

    try {

      switch (opmode) {

        case constantes.sOBTENER_X_CODIGO:
        case constantes.sOBTENER_X_DESCRIPCION:

          ent = (DatEqPrim) param.firstElement();
          vN1 = (Vector) ent.getVN1();
          vN2 = (Vector) ent.getVN2();

          //respecto a Nivel1 y 2 ------------------
          if (!ent.getCD_NIVEL_1().equals("")) {
            sFiltro = " AND CD_NIVEL_1 = ?";
          }
          if (!ent.getCD_NIVEL_2().equals("")) {
            sFiltro = sFiltro + " AND CD_NIVEL_2 = ?";

          }
          if (ent.getCD_NIVEL_1().equals("") && ent.getCD_NIVEL_2().equals("")) {
            switch (param.getPerfil()) {
              case CApp.perfilNIVEL1:
                sAutorizacion1 = " AND CD_NIVEL_1 IN (";
                for (i = 0; i < vN1.size(); i++) {
                  sAutorizacion1 = sAutorizacion1 + " ?" + ",";
                }
                sAutorizacion1 = sAutorizacion1.substring(0,
                    sAutorizacion1.length() - 1) + ")";
                break;

              case CApp.perfilNIVEL2:
                for (i = 0; i < vN1.size(); i++) {
                  sAutorizacion2 = sAutorizacion2 +
                      " (CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?) OR";
                }
                sAutorizacion2 = " AND ( " +
                    sAutorizacion2.substring(0, sAutorizacion2.length() - 2) +
                    ")";
                break;
            }
          }

          //-----------------------------------------

          // query
          if (opmode == constantes.sOBTENER_X_CODIGO) {
            sQuery = "select a.CD_E_NOTIF, a.DS_E_NOTIF, a.CD_NIVEL_1, " +
                " a.CD_NIVEL_2, " +
                " a.CD_CENTRO from SIVE_E_NOTIF  a, SIVE_C_NOTIF b" +
                " where a.CD_CENTRO = b.CD_CENTRO AND a.CD_E_NOTIF = ?";
          }
          else {
            sQuery = "select a.CD_E_NOTIF, a.DS_E_NOTIF, a.CD_NIVEL_1, " +
                " a.CD_NIVEL_2, " +
                " a.CD_CENTRO from SIVE_E_NOTIF  a, SIVE_C_NOTIF b" +
                " where a.CD_CENTRO = b.CD_CENTRO AND a.DS_E_NOTIF = ?";

            //query
          }
          sQuery = sQuery + sFiltro + sAutorizacion1 + sAutorizacion2;
          sQuery = sQuery +
              " AND a.IT_BAJA = ? AND b.CD_NIVASIS = ? order by CD_E_NOTIF";

          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCD_E_NOTIF().trim()); //sera des o cod
          iValor++;

          //respecto a nivel 1 y 2 ---------------
          if (!ent.getCD_NIVEL_1().equals("")) {
            st.setString(iValor, ent.getCD_NIVEL_1().trim());
            iValor++;
          }
          if (!ent.getCD_NIVEL_2().equals("")) {
            st.setString(iValor, ent.getCD_NIVEL_2().trim());
            iValor++;
          }
          if (ent.getCD_NIVEL_1().equals("") && ent.getCD_NIVEL_2().equals("")) {
            switch (param.getPerfil()) {
              case CApp.perfilNIVEL1:
                for (i = 0; i < vN1.size(); i++) {
                  st.setString(iValor, (String) vN1.elementAt(i));
                  iValor++;
                }
                break;

              case CApp.perfilNIVEL2:
                for (i = 0; i < vN1.size(); i++) {
                  st.setString(iValor, (String) vN1.elementAt(i));
                  iValor++;
                  st.setString(iValor, (String) vN2.elementAt(i));
                  iValor++;
                }
                break;
            }
          }

          //---------------------------------------------

          //it_baja
          st.setString(iValor, "N");
          iValor++;
          //nivel asistencial P  : centros de Primaria
          st.setString(iValor, "P");
          iValor++;

          break;

          //TRAMAR ****
        case constantes.sSELECCION_X_CODIGO:
        case constantes.sSELECCION_X_DESCRIPCION:

          ent = (DatEqPrim) param.firstElement();
          vN1 = (Vector) ent.getVN1();
          vN2 = (Vector) ent.getVN2();

          //respecto a Nivel1 y 2 ------------------
          if (!ent.getCD_NIVEL_1().equals("")) {
            sFiltro = " AND CD_NIVEL_1 = ?";
          }
          if (!ent.getCD_NIVEL_2().equals("")) {
            sFiltro = sFiltro + " AND CD_NIVEL_2 = ?";

          }
          if (ent.getCD_NIVEL_1().equals("") && ent.getCD_NIVEL_2().equals("")) {
            switch (param.getPerfil()) {
              case CApp.perfilNIVEL1:
                sAutorizacion1 = " AND CD_NIVEL_1 IN (";
                for (i = 0; i < vN1.size(); i++) {
                  sAutorizacion1 = sAutorizacion1 + " ?" + ",";
                }
                sAutorizacion1 = sAutorizacion1.substring(0,
                    sAutorizacion1.length() - 1) + ")";
                break;

              case CApp.perfilNIVEL2:
                for (i = 0; i < vN1.size(); i++) {
                  sAutorizacion2 = sAutorizacion2 +
                      " (CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?) OR";
                }
                sAutorizacion2 = " AND ( " +
                    sAutorizacion2.substring(0, sAutorizacion2.length() - 2) +
                    ")";
                break;
            }
          }

          //-----------------------------------------

          // query
          //tramar
          if (param.getFilter().length() > 0) {

            if (opmode == constantes.sSELECCION_X_CODIGO) {
              sQuery = "select a.CD_E_NOTIF, a.DS_E_NOTIF, a.CD_NIVEL_1, " +
                  " a.CD_NIVEL_2, " +
                  " a.CD_CENTRO from SIVE_E_NOTIF  a, SIVE_C_NOTIF b" +
                  " where a.CD_CENTRO = b.CD_CENTRO  and a.CD_E_NOTIF like ? and a.CD_E_NOTIF > ? ";

            }
            else {
              sQuery = "select a.CD_E_NOTIF, a.DS_E_NOTIF, a.CD_NIVEL_1, " +
                  " a.CD_NIVEL_2, " +
                  " a.CD_CENTRO from SIVE_E_NOTIF  a, SIVE_C_NOTIF b" +
                  " where a.CD_CENTRO = b.CD_CENTRO  and a.DS_E_NOTIF like ? and a.DS_E_NOTIF > ? ";
            }
          }
          else {
            if (opmode == constantes.sSELECCION_X_CODIGO) {
              sQuery = "select a.CD_E_NOTIF, a.DS_E_NOTIF, a.CD_NIVEL_1, " +
                  " a.CD_NIVEL_2, " +
                  " a.CD_CENTRO from SIVE_E_NOTIF  a, SIVE_C_NOTIF b" +
                  " where a.CD_CENTRO = b.CD_CENTRO  and a.CD_E_NOTIF like ? ";

            }
            else {
              sQuery = "select a.CD_E_NOTIF, a.DS_E_NOTIF, a.CD_NIVEL_1, " +
                  " a.CD_NIVEL_2, " +
                  " a.CD_CENTRO from SIVE_E_NOTIF  a, SIVE_C_NOTIF b" +
                  " where a.CD_CENTRO = b.CD_CENTRO  and a.DS_E_NOTIF like ? ";
            }
          }

          sQuery = sQuery + sFiltro + sAutorizacion1 + sAutorizacion2;
          sQuery = sQuery + " AND a.IT_BAJA = ? AND b.CD_NIVASIS = ? ";

          //order by
          if (opmode == constantes.sSELECCION_X_CODIGO) {
            sQuery = sQuery + " order by CD_E_NOTIF";
          }
          else {
            sQuery = sQuery + " order by DS_E_NOTIF";

            //pregunta
          }
          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCD_E_NOTIF().trim() + "%");
          iValor++;

          //trama
          if (param.getFilter().length() > 0) {
            st.setString(iValor, param.getFilter().trim());
            iValor++;
          }

          //respecto a nivel 1 y 2 ---------------
          if (!ent.getCD_NIVEL_1().equals("")) {
            st.setString(iValor, ent.getCD_NIVEL_1().trim());
            iValor++;
          }
          if (!ent.getCD_NIVEL_2().equals("")) {
            st.setString(iValor, ent.getCD_NIVEL_2().trim());
            iValor++;
          }
          if (ent.getCD_NIVEL_1().equals("") && ent.getCD_NIVEL_2().equals("")) {
            switch (param.getPerfil()) {
              case CApp.perfilNIVEL1:
                for (i = 0; i < vN1.size(); i++) {
                  st.setString(iValor, (String) vN1.elementAt(i));
                  iValor++;
                }
                break;

              case CApp.perfilNIVEL2:
                for (i = 0; i < vN1.size(); i++) {
                  st.setString(iValor, (String) vN1.elementAt(i));
                  iValor++;
                  st.setString(iValor, (String) vN2.elementAt(i));
                  iValor++;
                }
                break;
            }
          }

          //---------------------------------------------

          //it_baja
          st.setString(iValor, "N");
          iValor++;
          //nivel asistencial P: Primaria
          st.setString(iValor, "P");
          iValor++;
          break;

      }

      //lanzar query

      rs = st.executeQuery();

      while (rs.next()) {

        //tramar ---
        if ( (opmode == constantes.sSELECCION_X_CODIGO) ||
            (opmode == constantes.sSELECCION_X_DESCRIPCION)) {

          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            listaSalida.setState(CLista.listaINCOMPLETA);

            if (opmode == constantes.sSELECCION_X_CODIGO) {
              listaSalida.setFilter( ( (DatEqPrim) listaSalida.lastElement()).
                                    getCD_E_NOTIF());
            }
            else {
              listaSalida.setFilter( ( (DatEqPrim) listaSalida.lastElement()).
                                    getDS_E_NOTIF());

            }
            break;
          }

          // control de estado
          if (listaSalida.getState() == CLista.listaVACIA) {
            listaSalida.setState(CLista.listaLLENA);
          }
        } //tramar ------

        dataSalida = new DatEqPrim(rs.getString("CD_NIVEL_1"),
                                   rs.getString("CD_NIVEL_2"),
                                   rs.getString("CD_E_NOTIF"),
                                   rs.getString("DS_E_NOTIF"), null, null);

        // a�ade un nodo
        listaSalida.addElement(dataSalida);
        i++;
      }

      rs.close();
      rs = null;
      st.close();
      st = null;

      // valida la transacci�n

      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    listaSalida.trimToSize();

    return listaSalida;
  }

}
