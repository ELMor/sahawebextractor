package tuberculosis.servidor.mantautom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import capp.CLista;
import comun.Fechas;
import comun.constantes;
import sapp.DBServlet;
import tuberculosis.datos.mantautom.DataAuto;

//Interesa ver que no nos han cambiado el RT,
//pero nosotros actualizamos el caso+RT
public class SrvAutoCierre
    extends DBServlet {

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 0;

    // objetos de datos
    CLista listaSalida = new CLista();
    CLista listaSalidaDefinitiva = new CLista();

    //querys
    String sQuery = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    java.sql.Date maxIniDate = null;
    java.sql.Date minIniDate = null;
    java.sql.Date maxFinDate = null;
    java.sql.Date minFinDate = null;

    String maxini = "";
    String minini = "";
    String maxfin = "";
    String minfin = "";

    boolean bBloqueo = true;

    java.util.Date dFechaUltact = new java.util.Date();
    SimpleDateFormat formActual = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
        new Locale("es", "ES"));
    String sFechaUltact = formUltact.format(dFechaUltact);
    java.util.Date dFechaA = new java.util.Date();
    java.sql.Date sqlFecA = new java.sql.Date(dFechaA.getTime());

    try {

      switch (opmode) {

        case constantes.modoSELECT:

          //tratamientos1
          sQuery = "SELECT NM_EDO, MAX(FC_FINTRAT), MAX(FC_INITRAT), MIN(FC_FINTRAT), MIN(FC_INITRAT)  " +
              //"FROM SIVE_TRATAMIENTOS GROUP BY NM_EDO , "+
              //" CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI,FC_RECEP , "+
              //" FC_FECNOTIF, CD_FUENTE "+
              " FROM SIVE_TRATAMIENTOS GROUP BY NM_EDO " +
              " HAVING MIN( FC_FINTRAT) IS NULL  " +
              " AND  (TRUNC(MONTHS_BETWEEN(SYSDATE,  MAX(FC_INITRAT))) / 12 ) > 2" +
              " ORDER BY NM_EDO";

          st = con.prepareStatement(sQuery);
          rs = st.executeQuery();

          while (rs.next()) {
            DataAuto datos = new DataAuto();
            datos.NM_EDO = new Integer(rs.getInt("NM_EDO")).toString();

            maxIniDate = rs.getDate("MAX(FC_INITRAT)");
            minIniDate = rs.getDate("MIN(FC_INITRAT)");
            maxFinDate = rs.getDate("MAX(FC_FINTRAT)");
            minFinDate = rs.getDate("MIN(FC_FINTRAT)");

            if (maxIniDate != null) {
              maxini = formater.format(maxIniDate);
            }
            else {
              maxini = "";
            }

            if (minIniDate != null) {
              minini = formater.format(minIniDate);
            }
            else {
              minini = "";
            }

            if (maxFinDate != null) {
              maxfin = formater.format(maxFinDate);
            }
            else {
              maxfin = "";
            }

            if (minFinDate != null) {
              minfin = formater.format(minFinDate);
            }
            else {
              minfin = "";
            }

            datos.MAX_FC_INITRAT = maxini;
            datos.MIN_FC_INITRAT = minini;
            datos.MAX_FC_FINTRAT = maxfin;
            datos.MIN_FC_FINTRAT = minfin;
            listaSalida.addElement(datos);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //tratamientos2
          sQuery = "SELECT NM_EDO, MAX(FC_FINTRAT), MAX(FC_INITRAT), MIN(FC_FINTRAT), MIN(FC_INITRAT)  " +
              //" FROM SIVE_TRATAMIENTOS GROUP BY NM_EDO, "+
              //" CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI,FC_RECEP , "+
              //" FC_FECNOTIF, CD_FUENTE "+
              " FROM SIVE_TRATAMIENTOS GROUP BY NM_EDO " +
              " HAVING MIN(FC_INITRAT) IS NOT NULL AND MIN(FC_FINTRAT) IS NOT NULL " +
              " AND  (TRUNC(MONTHS_BETWEEN(SYSDATE,  MAX(FC_FINTRAT))) / 12 ) > 1 " +
              " ORDER BY NM_EDO ";

          st = con.prepareStatement(sQuery);
          rs = st.executeQuery();

          while (rs.next()) {
            DataAuto datos = new DataAuto();
            datos.NM_EDO = new Integer(rs.getInt("NM_EDO")).toString();
                /*          maxini = formater.format (rs.getDate("MAX(FC_INITRAT)"));
                      minini = formater.format (rs.getDate("MIN(FC_INITRAT)"));
                      maxfin = formater.format (rs.getDate("MAX(FC_FINTRAT)"));
                      minfin = formater.format (rs.getDate("MIN(FC_FINTRAT)"));
                      if ( maxini== null) maxini="";
                      datos.MAX_FC_INITRAT =  maxini;
                      if ( minini== null) minini="";
                      datos.MIN_FC_INITRAT =  minini;
                      if ( maxfin== null) maxfin="";
                      datos.MAX_FC_FINTRAT =  maxfin;
                      if ( minfin== null) minfin="";
                      datos.MIN_FC_FINTRAT =  minfin;*/
            maxIniDate = rs.getDate("MAX(FC_INITRAT)");
            minIniDate = rs.getDate("MIN(FC_INITRAT)");
            maxFinDate = rs.getDate("MAX(FC_FINTRAT)");
            minFinDate = rs.getDate("MIN(FC_FINTRAT)");

            if (maxIniDate != null) {
              maxini = formater.format(maxIniDate);
            }
            else {
              maxini = "";
            }

            if (minIniDate != null) {
              minini = formater.format(minIniDate);
            }
            else {
              minini = "";
            }

            if (maxFinDate != null) {
              maxfin = formater.format(maxFinDate);
            }
            else {
              maxfin = "";
            }

            if (minFinDate != null) {
              minfin = formater.format(minFinDate);
            }
            else {
              minfin = "";
            }

            datos.MAX_FC_INITRAT = maxini;
            datos.MIN_FC_INITRAT = minini;
            datos.MAX_FC_FINTRAT = maxfin;
            datos.MIN_FC_FINTRAT = minfin;

            listaSalida.addElement(datos);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //tratamientos3  - pero puede venir tb de otra anterior
          sQuery = "SELECT NM_EDO, MAX(FC_FINTRAT), MAX(FC_INITRAT), MIN(FC_FINTRAT), MIN(FC_INITRAT) " +
              //" FROM SIVE_TRATAMIENTOS GROUP BY , "+
              //" CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI,FC_RECEP , "+
              //" FC_FECNOTIF, CD_FUENTE "+
              " FROM SIVE_TRATAMIENTOS GROUP BY NM_EDO " +
              " HAVING " + //MIN(FC_INITRAT) IS NOT NULL  AND  "+ (ahora trat no es obligatorio)
              " (TRUNC(MONTHS_BETWEEN(SYSDATE, MAX(FC_FINTRAT))) / 12 ) > 1 " +
              " AND (TRUNC(MONTHS_BETWEEN(SYSDATE, MAX(FC_INITRAT))) / 12 ) > 2 " +
              " ORDER BY NM_EDO ";

          st = con.prepareStatement(sQuery);
          rs = st.executeQuery();

          CLista listaNueva = new CLista();
          while (rs.next()) {
            DataAuto datos = new DataAuto();
            datos.NM_EDO = new Integer(rs.getInt("NM_EDO")).toString();

                /*          maxini = formater.format (rs.getDate("MAX(FC_INITRAT)"));
                      minini = formater.format (rs.getDate("MIN(FC_INITRAT)"));
                      maxfin = formater.format (rs.getDate("MAX(FC_FINTRAT)"));
                      minfin = formater.format (rs.getDate("MIN(FC_FINTRAT)"));
                      if ( maxini== null) maxini="";
                      datos.MAX_FC_INITRAT =  maxini;
                      if ( minini== null) minini="";
                      datos.MIN_FC_INITRAT =  minini;
                      if ( maxfin== null) maxfin="";
                      datos.MAX_FC_FINTRAT =  maxfin;
                      if ( minfin== null) minfin="";
                      datos.MIN_FC_FINTRAT =  minfin;  */

            maxIniDate = rs.getDate("MAX(FC_INITRAT)");
            minIniDate = rs.getDate("MIN(FC_INITRAT)");
            maxFinDate = rs.getDate("MAX(FC_FINTRAT)");
            minFinDate = rs.getDate("MIN(FC_FINTRAT)");

            if (maxIniDate != null) {
              maxini = formater.format(maxIniDate);
            }
            else {
              maxini = "";
            }

            if (minIniDate != null) {
              minini = formater.format(minIniDate);
            }
            else {
              minini = "";
            }

            if (maxFinDate != null) {
              maxfin = formater.format(maxFinDate);
            }
            else {
              maxfin = "";
            }

            if (minFinDate != null) {
              minfin = formater.format(minFinDate);
            }
            else {
              minfin = "";
            }

            datos.MAX_FC_INITRAT = maxini;
            datos.MIN_FC_INITRAT = minini;
            datos.MAX_FC_FINTRAT = maxfin;
            datos.MIN_FC_FINTRAT = minfin;

            listaNueva.addElement(datos);

          } //while
          rs.close();
          rs = null;
          st.close();
          st = null;

          //quitar los repetidos  ------------------------------------
          //listaSalida tiene lo antiguo
          //listaNueva  lo que aporta la nueva query

          CLista listaNuevaLimpia = new CLista();

          boolean bEstaenAntigua = false;
          for (int j = 0; j < listaNueva.size(); j++) {
            DataAuto dataNueva = (DataAuto) listaNueva.elementAt(j);
            bEstaenAntigua = false;
            for (int k = 0; k < listaSalida.size(); k++) {
              if (!bEstaenAntigua) {
                DataAuto dataSalida = (DataAuto) listaSalida.elementAt(k);
                if (dataSalida.getNM_EDO().equals(dataNueva.getNM_EDO())) {
                  bEstaenAntigua = true;
                }
              }
            }
            if (!bEstaenAntigua) {
              listaNuevaLimpia.addElement(dataNueva);

            }
          }

          //----------------------------------------------------------

          listaSalida.appendData(listaNuevaLimpia); //agregamos lo nuevo

          //ir a por el registro Y DEJAR SOLO LOS ABTOS
          for (int j = 0; j < listaSalida.size(); j++) {

            DataAuto datRT = (DataAuto) listaSalida.elementAt(j);
            sQuery = "SELECT a.CD_ARTBC, a.CD_NRTBC, a.FC_SALRTBC, " +
                " a.CD_OPE, a.FC_ULTACT " +
                " FROM SIVE_REGISTROTBC a " +
                " WHERE a.NM_EDO = ?";
            st = con.prepareStatement(sQuery);

            st.setInt(1, (new Integer(datRT.getNM_EDO().trim())).intValue());
            rs = st.executeQuery();

            String fc_salida = "";
            java.util.Date dFecha = new java.util.Date();
            java.sql.Date sqlFec = new java.sql.Date(dFecha.getTime());
            java.sql.Timestamp fecRecu = null;

            while (rs.next()) {
              sqlFec = rs.getDate("FC_SALRTBC");
              if (sqlFec != null) { //cerrado
                fc_salida = formater.format(rs.getDate("FC_SALRTBC"));
              }
              else {
                fc_salida = null;

              }
              if (fc_salida == null) { //abto
                datRT.CD_ARTBC = rs.getString("CD_ARTBC");
                datRT.CD_NRTBC = rs.getString("CD_NRTBC");
                datRT.CD_OPE_RT = rs.getString("CD_OPE");
                fecRecu = rs.getTimestamp("FC_ULTACT");
                datRT.FC_ULTACT_RT = Fechas.timestamp2String(fecRecu);
                dFecha = new java.util.Date();
                datRT.FC_ACTUAL = formater.format(dFecha);

                listaSalidaDefinitiva.addElement(datRT);
              }
            }
          }

          break;

        case constantes.modoBORRARMASIVO_SB:
          bBloqueo = false;
        case constantes.modoBORRARMASIVO:
          for (int j = 0; j < param.size(); j++) {

            DataAuto datosborrar = (DataAuto) param.elementAt(j);
            if (bBloqueo) {
              if (RegistroModificado(con, datosborrar)) {
                throw (new Exception(constantes.PRE_BLOQUEO +
                                     constantes.SIVE_REGISTROTBC));
              }
            }

            sQuery = "UPDATE SIVE_EDOIND SET CD_OPE = ?, FC_ULTACT = ? " +
                "WHERE NM_EDO = ?";

            st = con.prepareStatement(sQuery);
            instanciarUpdate(datosborrar, param.getLogin(), st, sFechaUltact);
            st.executeUpdate();
            st.close();
            st = null;

            if (bBloqueo) { //con bloqueo
              modificarRT(con, datosborrar, param.getLogin(), sFechaUltact,
                          sqlFecA);
            }
            else { //sin bloqueo
              modificarRT_SB(con, datosborrar, param.getLogin(), sFechaUltact,
                             sqlFecA);

            }
            listaSalidaDefinitiva = new CLista(); //vacia

          }
          break;

        case constantes.modoBORRARUNO_SB:
          bBloqueo = false;
        case constantes.modoBORRARUNO:

          DataAuto datosborrar = (DataAuto) param.firstElement();
          //Interesa ver que no nos han cambiado el RT,
          //pero nosotros actualizamos el caso+RT
          if (bBloqueo) {
            if (RegistroModificado(con, datosborrar)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_REGISTROTBC));
            }
          }

          sQuery = "UPDATE SIVE_EDOIND SET CD_OPE = ?, FC_ULTACT = ? " +
              "WHERE NM_EDO = ?";

          st = con.prepareStatement(sQuery);
          instanciarUpdate(datosborrar, param.getLogin(), st, sFechaUltact);
          st.executeUpdate();
          st.close();
          st = null;

          if (bBloqueo) { //con bloqueo
            modificarRT(con, datosborrar, param.getLogin(), sFechaUltact,
                        sqlFecA);
          }
          else { //sin bloqueo
            modificarRT_SB(con, datosborrar, param.getLogin(), sFechaUltact,
                           sqlFecA);
          }
          listaSalidaDefinitiva = new CLista(); //vacia

          break;
      }

      //valida la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalidaDefinitiva = null;
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return listaSalidaDefinitiva;

  } //fin doWork

  void modificarRT(Connection c,
                   DataAuto datosborrar, String cd_openuevo,
                   String sFechaUltact,
                   java.sql.Date sqlFec) throws Exception {

    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_REGISTROTBC SET CD_OPE = ?, FC_ULTACT = ?, " +
          "FC_SALRTBC = ?, CD_MOTSALRTBC = ? " +
          "WHERE CD_ARTBC = ? AND CD_NRTBC = ? " +
          "AND CD_OPE = ? AND FC_ULTACT = ?";

      st = c.prepareStatement(sQuery);
      int iParN = 1;

      st.setString(iParN++, cd_openuevo); // CD_OPE NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact); // FC_ULTACT NUEVO
      st.setTimestamp(iParN++, fUltact);
      st.setDate(iParN++, sqlFec); //fechasalida
      st.setString(iParN++, constantes.sMotivoCierreAutomatico); //motivo salida

      //WHERE ( CD_ARTBC, CD_NRTBC, CD_OPE, FC_ULTACT)
      st.setString(iParN++, datosborrar.getCD_ARTBC());
      st.setString(iParN++, datosborrar.getCD_NRTBC());
      st.setString(iParN++, datosborrar.getCD_OPE_RT()); // CD_OPE ANTIGUO
      st.setTimestamp(iParN++,
                      Fechas.string2Timestamp(datosborrar.getFC_ULTACT_RT())); // FC_ULTACT ANTIGUO

      int iActualizados = st.executeUpdate();

      //PREGUNTAR si ha modificado algo (ver de nuevo el bloqueo)
      if (iActualizados == 0) {
        throw (new Exception(constantes.PRE_BLOQUEO +
                             constantes.SIVE_REGISTROTBC));
      }

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarBrote()

  void modificarRT_SB(Connection c,
                      DataAuto datosborrar, String cd_openuevo,
                      String sFechaUltact,
                      java.sql.Date sqlFec) throws Exception {

    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_REGISTROTBC SET CD_OPE = ?, FC_ULTACT = ?, " +
          "FC_SALRTBC = ?, CD_MOTSALRTBC = ? " +
          "WHERE CD_ARTBC = ? AND CD_NRTBC = ? ";

      st = c.prepareStatement(sQuery);
      int iParN = 1;

      st.setString(iParN++, cd_openuevo); // CD_OPE NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact); // FC_ULTACT NUEVO
      st.setDate(iParN++, sqlFec); //fechasalida
      st.setString(iParN++, constantes.sMotivoCierreAutomatico); //motivo salida
      //WHERE ( CD_ARTBC, CD_NRTBC
      st.setString(iParN++, datosborrar.getCD_ARTBC());
      st.setString(iParN++, datosborrar.getCD_NRTBC());

      st.executeUpdate();

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  }

// Comprueba el CD_OPE y FC_ULTACT del registro de la BD con el registro del cliente
  boolean RegistroModificado(Connection c, DataAuto datosborrar) throws
      Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery;
    String ope, fultact;

    try {
      sQuery = "SELECT CD_OPE,FC_ULTACT FROM SIVE_REGISTROTBC " +
          "WHERE CD_ARTBC = ? AND CD_NRTBC = ? ";

      st = c.prepareStatement(sQuery);
      int iParN = 1;
      st.setString(iParN++, datosborrar.getCD_ARTBC());
      st.setString(iParN++, datosborrar.getCD_NRTBC());
      rs = st.executeQuery();

      if (rs.next()) {
        ope = rs.getString("CD_OPE");
        fultact = Fechas.timestamp2String(rs.getTimestamp("FC_ULTACT"));
      }
      else {
        return true;
      }

      // Cierre del PreparedStatement y del ResultSet
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      return true;
    }

    if (!ope.equals(datosborrar.getCD_OPE_RT()) ||
        !fultact.equals(datosborrar.getFC_ULTACT_RT())) {
      return true;
    }
    else {
      return false;
    }

  } //RegistroModificado()

  protected void instanciarUpdate(DataAuto datosborrar, String cd_openuevo,
                                  PreparedStatement st,
                                  String sFechaUltact) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    st.setString(iParN++, cd_openuevo);

    java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
    st.setTimestamp(iParN++, fUltact);

    String iEDO = datosborrar.getNM_EDO().trim();
    st.setInt(iParN++, (new Integer(iEDO)).intValue());

  } // Fin instanciarUpdate

} //fin de la clase
