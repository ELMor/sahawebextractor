/**
 * Clase: SrvTubApp
 * Paquete: tuberculosis.servidor.menu
 * Hereda: HttpServlet
 * Fecha Inicio: 29/02/2000
 */

package tuberculosis.servidor.menu;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import comun.constantes;
import tuberculosis.datos.menu.ParametrosTubUsu;

public class SrvTubApp
    extends HttpServlet {

  // applets
  protected final int appletCASOS = 100;
  protected final int appletCONTACTOS = 105;
  protected final int appletENFERMOS_CONTACTOS = 110;

  protected final int appletCIERRE = 200;
  protected final int appletAPERTURA = 210;
  protected final int appletCIERRE_AUTOMATICO = 220;

  protected final int appletMAN_REGISTRO = 300;

  protected final int appletUSUARIOS = 310;
  protected final int appletPASSWORD = 320;

  protected final int appletCATALOGO_FUENTES = constantes.
      appletCATALOGO_FUENTES;
  protected final int appletCATALOGO_MOTIVOS_SALIDA = constantes.
      appletCATALOGO_MOTIVOS_SALIDA;
  protected final int appletCATALOGO_MOTIVOS_INICIO = constantes.
      appletCATALOGO_MOTIVOS_INICIO;
  protected final int appletCATALOGO_MUESTRAS = constantes.
      appletCATALOGO_MUESTRAS;
  protected final int appletCATALOGO_TECNICAS = constantes.
      appletCATALOGO_TECNICAS;
  protected final int appletCATALOGO_VALORES_MUESTRAS = constantes.
      appletCATALOGO_VALORES_MUESTRAS;
  protected final int appletCATALOGO_MICOBACTERIAS = constantes.
      appletCATALOGO_MICOBACTERIAS;
  protected final int appletCATALOGO_ESTUDIOS_RESISTENCIAS = constantes.
      appletCATALOGO_ESTUDIOS_RESISTENCIAS;

  protected final int appletCONFLICTOS = 400;

  protected final int appletPREGUNTAS_CONTACTOS = 500;
  protected final int appletMODELOS_CONTACTOS = 510;
  protected final int appletLISTAS_CONTACTOS = 520;

  protected final int appletPREGUNTAS_CASOS = 550;
  protected final int appletMODELOS_CASOS = 560;
  protected final int appletLISTAS_CASOS = 570;

  protected final int appletENVIO_CNE = 800;
  protected final int appletZONIFICACION = 600;
  protected final int appletGRUPOS = 700;

  protected final int appletVOLCADOS_CONTACTOS = 900;
  protected final int appletVOLCADOS_RESULTADOS = 910;
  protected final int appletVOLCADOS_EVOLUCIONES = 920;

  //QQ Ayudas:
  protected final int ayudaPP = 37;
  protected final int ayudaAcerca = 38;

  // servicio
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    // streams
    //ObjectInputStream in = null;
    ServletOutputStream out;

    // par�metros del applet
    int iCat = -1;
    int iCatAux = -1;
    int iCatPral = -1;
    String sWidth = "";
    String sHeight = "";
    String sCatalogoW = ""; //QQ Se establcen m�s abajo.
    String sCatalogoH = "";
    String sConsW = "";
    String sConsH = "";
    String sVolcW = "";
    String sVolcH = "";
    String sClass = "";
    String sLogin = "", sRef = "";
    String sAyuda = "tuberculosis/ayuda_pp";

    final String sArchiveDefault = "capp.zip, sapp.zip";

    String sArchive = "";
    String sTSIVE = constantes.CD_T_SIVE_EDO;

    // comunicaciones servlet-applet
    try {

      // lee el modo de operaci�n
      //in = new ObjectInputStream(request.getInputStream());

      // lee los par�metros
      //hash = (sesion) in.readObject();

      // par�metros recibidos
      ParametrosTubUsu hash = new ParametrosTubUsu();
      loadTable(hash, request);

      // abre el stream de salida
      response.setContentType("text/html");
      out = response.getOutputStream();

      // cabecera
      out.println("<HTML><HEAD>");
      out.println("<TITLE>ICM_99</TITLE>");
      out.println("<BASE HREF=\"" + hash.getString("URL_HTML", true) + "\">");
      out.println("</HEAD><BODY BGCOLOR=\"#FFFFFF\">");

      // determina el tama�o y el nombre de la clase
      sCatalogoW = "680"; //Tama�o para applets de mantenimiento.
      sCatalogoH = "325";
      sConsW = "640"; // Tama�o para applets de Consultas
      sConsH = "600";
      sVolcW = "502"; //Tama�o para applets de volcados (Exportaciones)
      sVolcH = "230";

      switch (new Integer(hash.getString("APPLET", true)).intValue()) {
        case appletVOLCADOS_EVOLUCIONES:
          sClass =
              "tuberculosis.cliente.voltratamientos.AppVolTratamientos.class";
          sWidth = "615";
          sHeight = "510";
          sAyuda = "tuberculosis/ayuda_expor";
          sArchive = sArchiveDefault + ", capp2.zip, sapp2.zip, obj.zip" +
              ", tuberculosis.cliente.componentes.zip, tuberculosis.cliente.fechas.zip" +
              ", tuberculosis.cliente.voltratamientos.zip, tuberculosis.datos.fechas.zip" +
              ", util_ficheros.zip";

          break;
        case appletVOLCADOS_RESULTADOS:
          sClass = "tuberculosis.cliente.volreslab.AppVolResLab.class";
          sWidth = "615";
          sHeight = "510";
          sAyuda = "tuberculosis/ayuda_expor";
          sArchive = sArchiveDefault + ", capp2.zip, sapp2.zip, obj.zip" +
              ", tuberculosis.cliente.componentes.zip, tuberculosis.cliente.fechas.zip" +
              ", tuberculosis.cliente.volreslab.zip, tuberculosis.datos.fechas.zip" +
              ", util_ficheros.zip";

          break;
        case appletVOLCADOS_CONTACTOS:
          sTSIVE = constantes.CD_T_SIVE_CONTACTOS;
          sClass = "tuberculosis.cliente.volcontactos.VolContactos.class";
          sWidth = "615";
          sHeight = "510";
          sAyuda = "tuberculosis/ayuda_expor";
          sArchive = sArchiveDefault + ", fechas.zip, obj.zip, utilidades.zip" +
              ", tuberculosis.cliente.volcontactos.zip, tuberculosis.datos.volcontactos.zip";

          break;
        case appletGRUPOS:
          sClass = "usu2.AppUSU.class";
          sWidth = "640";
          sHeight = "550";
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", capp2.zip, sapp2.zip, usu2.zip";

          break;
        case appletZONIFICACION:
          sClass = "tuberculosis.cliente.zonificacion.AppZonTub.class";
          sWidth = "450";
          sHeight = "250";
          sAyuda = "tuberculosis/ayuda_gener";
          sArchive = sArchiveDefault +
              ", comun.zip, fechas.zip, panniveles.zip, " +
              "sapp.zip, tuberculosis.cliente.zonificacion.zip";

          break;
        case appletUSUARIOS:
          sClass = "tuberculosis.cliente.mantus.AppMantUsuTub.class";
          sWidth = "675";
          sHeight = "350";
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", capp2.zip, comun.zip, sapp2.zip" +
              ", tuberculosis.cliente.mantus.zip, tuberculosis.datos.mantus.zip";

          break;
        case appletPASSWORD:

          /*sTSIVE = "E";
                     sClass = "passwordusu.AppPassword2.class";
                     sWidth = sCatalogoW;
                     sHeight = sCatalogoH;
                     sAyuda = "";
                     sArchive = sArchiveDefault;*/

          break;
        case appletENVIO_CNE:
          sClass = "envios.AppEnviosCNE.class";
          sWidth = sVolcW;
          sHeight = sVolcH;
          sAyuda = "tuberculosis/ayuda_envio";
          sArchive = sArchiveDefault + ", comun.zip, envios.zip";

          break;
        case appletCASOS:
          sClass = "tuberculosis.cliente.mantregtub.AppPanMntCasosTub.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_casos";
          sArchive = sArchiveDefault + ", tuberculosis.util.zip" +
              ", tuberculosis.cliente.pantotalmues.zip, tuberculosis.cliente.pantotaltrat.zip" +
              ", tuberculosis.datos.pantotalmues.zip, tuberculosis.datos.pantotaltrat.zip" +
              ", tuberculosis.cliente.diaresul.zip";

          break;

        case appletCONTACTOS:
          sTSIVE = constantes.CD_T_SIVE_CONTACTOS;
          sClass = "tuberculosis.cliente.mantcontub.AppConTub.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_casos";
          sArchive = sArchiveDefault + ", tuberculosis.util.zip";

          break;

          /*case appletENFERMOS_CONTACTOS:
            sClass = "enfermotub.ApPruEnfermo.class";
            sWidth = sConsW;
            sHeight = sConsH;
            sAyuda = "tuberculosis/ayuda_casos";
            sArchive = sArchiveDefault + ", catalogo.zip, comun.zip, enfermotub.zip, suca.zip";
            break; */

          // modificacion 08/02/2001
        case appletENFERMOS_CONTACTOS:
          sClass = "enfermotub.ApPruEnfermo.class";
          sWidth = sConsW;
          sHeight = sConsH;
          sAyuda = "tuberculosis/ayuda_casos";
          sArchive = sArchiveDefault +
              ", catalogo.zip, comun.zip, enfermotub.zip, tuberculosis.util.zip";

          break;

        case appletCIERRE:
          sClass = "tuberculosis.cliente.mantregtub.AppPanMntCierre.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_casos";
          sArchive = sArchiveDefault + ", tuberculosis.util.zip";

          break;

        case appletAPERTURA:
          sClass = "tuberculosis.cliente.mantregtub.AppPanMntDeshacer.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_casos";
          sArchive = sArchiveDefault + ", tuberculosis.util.zip";

          break;

        case appletCIERRE_AUTOMATICO:
          sClass = "tuberculosis.cliente.mantautom.appcomenzarauto.class";
          sWidth = "648";
          sHeight = "250";
          sAyuda = "tuberculosis/ayuda_casos";
          sArchive = sArchiveDefault +
              ", comun.zip, tuberculosis.cliente.mantautom.zip," +
              "tuberculosis.datos.mantautom.zip";

          break;

        case appletMAN_REGISTRO:
          sClass = "tuberculosis.cliente.mantregtub.AppPanMntRegTub.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", tuberculosis.util.zip";

          break;

        case appletCATALOGO_FUENTES:
          iCat = appletCATALOGO_FUENTES;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", catalogo.zip";

          break;

        case appletCATALOGO_MOTIVOS_SALIDA:
          iCat = appletCATALOGO_MOTIVOS_SALIDA;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", catalogo.zip";

          break;

        case appletCATALOGO_MOTIVOS_INICIO:
          iCat = appletCATALOGO_MOTIVOS_INICIO;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", catalogo.zip";

          break;

        case appletCATALOGO_MUESTRAS:
          iCat = appletCATALOGO_MUESTRAS;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", catalogo.zip";

          break;

        case appletCATALOGO_TECNICAS:
          iCat = appletCATALOGO_TECNICAS;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", catalogo.zip";

          break;

        case appletCATALOGO_VALORES_MUESTRAS:
          iCat = appletCATALOGO_VALORES_MUESTRAS;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", catalogo.zip";

          break;

        case appletCATALOGO_MICOBACTERIAS:
          iCat = appletCATALOGO_MICOBACTERIAS;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", catalogo.zip";

          break;

        case appletCATALOGO_ESTUDIOS_RESISTENCIAS:
          iCat = appletCATALOGO_ESTUDIOS_RESISTENCIAS;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_admin";
          sArchive = sArchiveDefault + ", catalogo.zip";

          break;

        case appletCONFLICTOS:
          sClass = "tuberculosis.cliente.panconflicto.appPanconflicto.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_confl";
          sArchive = sArchiveDefault +
              ", comun.zip, panniveles.zip, tuberculosis.cliente.diaconflicto.zip" +
              ", tuberculosis.cliente.panconflicto.zip, tuberculosis.datos.diaconflicto.zip" +
              ", tuberculosis.datos.notiftub.zip, tuberculosis.datos.panconflicto.zip";

          break;

        case appletPREGUNTAS_CONTACTOS:
          sTSIVE = constantes.CD_T_SIVE_CONTACTOS;
          sClass = "preguntas.MantDePreguntas.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_param";
          sArchive = sArchiveDefault +
              ", catalogo.zip, listas.zip, preguntas.zip";

          break;

        case appletMODELOS_CONTACTOS:
          sTSIVE = constantes.CD_T_SIVE_CONTACTOS;
          sClass = "confprot.ApConfProt.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_param";
          sArchive = sArchiveDefault + ", cargamodelo.zip, catalogo.zip, comun.zip, confprot.zip, enfcentinela.zip, listas.zip, preguntas.zip";

          break;

        case appletLISTAS_CONTACTOS:
          sTSIVE = constantes.CD_T_SIVE_CONTACTOS;
          sClass = "listas.mantlis.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_param";
          sArchive = sArchiveDefault + ", listas.zip, catalogo.zip";

          break;

        case appletPREGUNTAS_CASOS:

          //sTSIVE = constantes.CD_T_SIVE_CONTACTOS;
          sClass = "preguntas.MantDePreguntas.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_param";
          sArchive = sArchiveDefault +
              ", catalogo.zip, listas.zip, preguntas.zip";

          break;

        case appletMODELOS_CASOS:

          //sTSIVE = constantes.CD_T_SIVE_CONTACTOS;
          sClass = "confprot.ApConfProt.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_param";
          sArchive = sArchiveDefault + ", cargamodelo.zip, catalogo.zip, comun.zip, confprot.zip, enfcentinela.zip, listas.zip, preguntas.zip";

          break;

        case appletLISTAS_CASOS:

          //sTSIVE = constantes.CD_T_SIVE_CONTACTOS;
          sClass = "listas.mantlis.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "tuberculosis/ayuda_param";
          sArchive = sArchiveDefault + ", listas.zip, catalogo.zip";

          break;

      }

      out.println("<TABLE WIDTH=\"100%\" HEIGHT=\"100%\" BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"\"0>");
      out.println("<TR><TD ALIGN=\"CENTER\" VALIGN=\"MIDDLE\">");

      // applet
      out.println("<APPLET");
      out.println(" MAYSCRIPT");

      // ZIP
      if (sArchive.equals("")) {
        //out.println(" CODEBASE = \"applet/\"");
        out.println(" CODEBASE = \".\"");
      }
      else {
        out.println(" CODEBASE = \"zip/\"");
        out.println(" ARCHIVE = \"" + sArchive + "\"");
      }
      out.println(" CODE     = \"" + sClass + "\"");
      out.println(" NAME     = \"ICM_99" + hash.getString("APPLET", true) +
                  "\"");
      out.println(" WIDTH    = \"" + sWidth + "\"");
      out.println(" HEIGHT   = \"" + sHeight + "\"");
      out.println(" HSPACE   = \"0\"");
      out.println(" VSPACE   = \"0\"");
      out.println(" ALIGN   = \"CENTER\"");
      out.println(">");

      out.println("<PARAM NAME=\"IDIOMA\" VALUE=\"" +
                  hash.getInteger("IDIOMA", true).toString() + "\">");
      out.println("<PARAM NAME=\"LOGIN\" VALUE=\"" + hash.getString("LOGIN", true) +
                  "\">");
      out.println("<PARAM NAME=\"URL_SERVLET\" VALUE=\"" +
                  hash.getString("URL_SERVLET", true) + "\">");
      out.println("<PARAM NAME=\"URL_HTML\" VALUE=\"" +
                  hash.getString("URL_HTML", true) + "\">");
      out.println("<PARAM NAME=\"FTP_SERVER\" VALUE=\"" +
                  hash.getString("FTP_SERVER", true) + "\">");
      out.println("<PARAM NAME=\"PERFIL\" VALUE=\"" + hash.getString("PERFIL", true) +
                  "\">");
      out.println("<PARAM NAME=\"CA\" VALUE=\"" + hash.getString("CA", true) +
                  "\">");
      //*E out.println("<PARAM NAME=\"DS_EMAIL\" VALUE=\"" + hash.getString("DS_EMAIL", false) + "\">");
      out.println("<PARAM NAME=\"NIVEL1\" VALUE=\"" + hash.getString("NIVEL1", true) +
                  "\">");
      out.println("<PARAM NAME=\"NIVEL2\" VALUE=\"" + hash.getString("NIVEL2", true) +
                  "\">");
      out.println("<PARAM NAME=\"NIVEL3\" VALUE=\"" + hash.getString("NIVEL3", true) +
                  "\">");
      out.println("<PARAM NAME=\"TSIVE\" VALUE=\"" + sTSIVE + "\">");
      //out.println("<PARAM NAME=\"IDIOMA_LOCAL\" VALUE=\"" + hash.getString("IDIOMA_LOCAL", false) + "\">");
      //*E out.println("<PARAM NAME=\"IT_AUTALTA\" VALUE=\"" + hash.getString("IT_AUTALTA", true) + "\">");
       //*E out.println("<PARAM NAME=\"IT_AUTBAJA\" VALUE=\"" + hash.getString("IT_AUTBAJA", true) + "\">");
        //*E out.println("<PARAM NAME=\"IT_AUTMOD\" VALUE=\"" + hash.getString("IT_AUTMOD", true) + "\">");
      out.println("<PARAM NAME=\"IT_FG_ENFERMO\" VALUE=\"" +
                  hash.getString("IT_FG_ENFERMO", true) + "\">");
      //out.println("<PARAM NAME=\"IT_FG_VALIDAR\" VALUE=\"" + hash.getString("IT_FG_VALIDAR", true) + "\">");
      //*E out.println("<PARAM NAME=\"IT_MODULO_3\" VALUE=\"" + hash.getString("IT_MODULO_3", true) + "\">");
      out.println("<PARAM NAME=\"IT_TRAMERO\" VALUE=\"" +
                  hash.getString("IT_TRAMERO", true) + "\">");

      out.println("<PARAM NAME=\"CATALOGO\" VALUE=\"" + Integer.toString(iCat) +
                  "\">");
      out.println("<PARAM NAME=\"CATAUX\" VALUE=\"" + Integer.toString(iCatAux) +
                  "\">");
      out.println("<PARAM NAME=\"CATPRAL\" VALUE=\"" +
                  Integer.toString(iCatPral) + "\">");
      out.println("<PARAM NAME=\"AYUDA\" VALUE=\"" + "ayuda/" + sAyuda +
                  ".html\">");

      //*E out.println("<PARAM NAME=\"CD_E_NOTIF\" VALUE=\"" + hash.getString("CD_E_NOTIF", false) + "\">");
       //*E out.println("<PARAM NAME=\"DS_E_NOTIF\" VALUE=\"" + hash.getString("DS_E_NOTIF", false) + "\">");
        //*E out.println("<PARAM NAME=\"CD_NIVEL_1_EQ\" VALUE=\"" + hash.getString("CD_NIVEL_1_EQ", false) + "\">");
         //*E out.println("<PARAM NAME=\"CD_NIVEL_2_EQ\" VALUE=\"" + hash.getString("CD_NIVEL_2_EQ", false) + "\">");
          //*E out.println("<PARAM NAME=\"DS_NIVEL_1_EQ\" VALUE=\"" + hash.getString("DS_NIVEL_1_EQ", false) + "\">");
           //*E out.println("<PARAM NAME=\"DS_NIVEL_2_EQ\" VALUE=\"" + hash.getString("DS_NIVEL_2_EQ", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_1_AUTORIZACIONES\" VALUE=\"" +
                  hash.getString("CD_NIVEL_1_AUTORIZACIONES", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_2_AUTORIZACIONES\" VALUE=\"" +
                  hash.getString("CD_NIVEL_2_AUTORIZACIONES", false) + "\">");
      out.println("<PARAM NAME=\"FC_ACTUAL\" VALUE=\"" +
                  hash.getString("FC_ACTUAL", true) + "\">");

      out.println("<PARAM NAME=\"CD_NIVEL1_DEFECTO\" VALUE=\"" +
                  hash.getString("CD_NIVEL_1_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL2_DEFECTO\" VALUE=\"" +
                  hash.getString("CD_NIVEL_2_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL1_DEFECTO\" VALUE=\"" +
                  hash.getString("DS_NIVEL_1_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL2_DEFECTO\" VALUE=\"" +
                  hash.getString("DS_NIVEL_2_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"ANYO_DEFECTO\" VALUE=\"" +
                  hash.getString("ANYO_DEFECTO", false) + "\">");

      //*E out.println("<PARAM NAME=\"IT_FG_RESOLVER_CONFLICTOS\" VALUE=\"" + hash.getString("IT_FG_RESOLVER_CONFLICTOS", false) + "\">");
       //out.println("<PARAM NAME=\"IT_FG_EXPORT\" VALUE=\"" + hash.getString("IT_FG_EXPORT", false) + "\">");

      out.println("<PARAM NAME=\"IT_USU\" VALUE=\"" + hash.getString("IT_USU", false) +
                  "\">");
      out.println("<PARAM NAME=\"COD_APLICACION\" VALUE=\"" +
                  hash.getString("COD_APLICACION", false) + "\">");
      out.println("<PARAM NAME=\"CD_TUBERCULOSIS\" VALUE=\"" +
                  hash.getString("CD_TUBERCULOSIS", false) + "\">");

      out.println("</APPLET>");
      out.println("</TD></TR>");
      out.println("</TABLE></BODY></HTML>");
      out.flush();
      out.close();

      // envia la excepci�n
    }
    catch (Exception e) {
      e.printStackTrace();

      // abre el stream de salida
      response.setContentType("text/html");
      out = response.getOutputStream();
      out.println("<HTML><HEAD>");
      out.println("<TITLE>ICM_99</TITLE>");
      out.println("</HEAD><BODY BGCOLOR=\"#FFFFFF\">");
      out.println(
          "<STRONG><FONT SIZE=2>Petici�n no autorizada.</FONT></STRONG></BODY></HTML>");
      out.flush();
      out.close();
    }
  }

  // CARGA LA TABLA CON LOS DATOS DEL OBJETO REQUEST
  public void loadTable(ParametrosTubUsu param, HttpServletRequest request) {

    param.put("APPLET", request.getParameter("APPLET"));
    param.put("IDIOMA", new Integer(request.getParameter("IDIOMA")));
    param.put("LOGIN", request.getParameter("LOGIN"));
    param.put("URL_SERVLET", request.getParameter("URL_SERVLET"));
    param.put("FTP_SERVER", request.getParameter("FTP_SERVER"));
    //*E param.put("DS_EMAIL", request.getParameter("DS_EMAIL"));
    param.put("URL_HTML", request.getParameter("URL_HTML"));
    param.put("PERFIL", request.getParameter("PERFIL"));
    param.put("CA", request.getParameter("CA"));
    param.put("NIVEL1", request.getParameter("NIVEL1"));
    param.put("NIVEL2", request.getParameter("NIVEL2"));
    param.put("NIVEL3", request.getParameter("NIVEL3"));
    param.put("TSIVE", request.getParameter("TSIVE"));
    //param.put("IDIOMA_LOCAL", request.getParameter("IDIOMA_LOCAL"));
    //*E param.put("IT_AUTALTA", request.getParameter("IT_AUTALTA"));
     //*E param.put("IT_AUTBAJA", request.getParameter("IT_AUTBAJA"));
      //*E param.put("IT_AUTMOD", request.getParameter("IT_AUTMOD"));
    param.put("IT_FG_ENFERMO", request.getParameter("IT_FG_ENFERMO"));
    //param.put("IT_FG_VALIDAR", request.getParameter("IT_FG_VALIDAR"));
    //*E param.put("IT_MODULO_3", request.getParameter("IT_MODULO_3"));
    param.put("IT_TRAMERO", request.getParameter("IT_TRAMERO"));
    //*E param.put("IT_FG_MNTO", request.getParameter("IT_FG_MNTO"));
     //*E param.put("IT_FG_MNTO_USU", request.getParameter("IT_FG_MNTO_USU"));
      //*E param.put("IT_FG_TCNE", request.getParameter("IT_FG_TCNE"));
       //*E param.put("IT_FG_PROTOS", request.getParameter("IT_FG_PROTOS"));
        //param.put("IT_FG_ALARMAS", request.getParameter("IT_FG_ALARMAS"));
        //param.put("IT_FG_EXPORT", request.getParameter("IT_FG_EXPORT"));
        //param.put("IT_FG_GENALAUTO", request.getParameter("IT_FG_GENALAUTO"));
        //param.put("IT_FG_MNOTIFS", request.getParameter("IT_FG_MNOTIFS"));
        //param.put("IT_FG_CONSRES", request.getParameter("IT_FG_CONSRES"));
        //*E param.put("CD_E_NOTIF", request.getParameter("CD_E_NOTIF"));
         //*E param.put("DS_E_NOTIF", request.getParameter("DS_E_NOTIF"));
          //*E param.put("CD_NIVEL_1_EQ", request.getParameter("CD_NIVEL_1_EQ"));
           //*E param.put("CD_NIVEL_2_EQ", request.getParameter("CD_NIVEL_2_EQ"));
            //*E param.put("DS_NIVEL_1_EQ", request.getParameter("DS_NIVEL_1_EQ"));
             //*E param.put("DS_NIVEL_2_EQ", request.getParameter("DS_NIVEL_2_EQ"));
    param.put("CD_NIVEL_1_AUTORIZACIONES",
              request.getParameter("CD_NIVEL_1_AUTORIZACIONES"));
    param.put("CD_NIVEL_2_AUTORIZACIONES",
              request.getParameter("CD_NIVEL_2_AUTORIZACIONES"));
    param.put("FC_ACTUAL", request.getParameter("FC_ACTUAL"));
    param.put("CD_NIVEL_1_DEFECTO", request.getParameter("CD_NIVEL_1_DEFECTO"));
    param.put("CD_NIVEL_2_DEFECTO", request.getParameter("CD_NIVEL_2_DEFECTO"));
    param.put("DS_NIVEL_1_DEFECTO", request.getParameter("DS_NIVEL_1_DEFECTO"));
    param.put("DS_NIVEL_2_DEFECTO", request.getParameter("DS_NIVEL_2_DEFECTO"));
    param.put("ANYO_DEFECTO", request.getParameter("ANYO_DEFECTO"));
    //*E param.put("IT_FG_RESOLVER_CONFLICTOS", request.getParameter("IT_FG_RESOLVER_CONFLICTOS"));
    param.put("IT_USU", request.getParameter("IT_USU"));
    param.put("COD_APLICACION", request.getParameter("COD_APLICACION"));
    param.put("CD_TUBERCULOSIS", request.getParameter("CD_TUBERCULOSIS"));
  }
}
