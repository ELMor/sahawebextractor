//AUTOR Luis Rivera
//Lista (Vector) en el cual se pueden agrupar sus elementos
//Los elementos de la ListaAgrupable deben implementar la interfaz Agrupable
// es , decir, deben ser agrupables segun algun criterio

//Al agrupar la ListaAgrupable lo que haremos es subdividirla en otras ListasAgrupables

package componentes;

public class ListaAgrupable
    extends ListaOrdenable { //Extieneden de Lista ,Vector

  public ListaAgrupable() {
  }

  public ListaAgrupable agrupa() {
    return agrupa(this);
  }

  //Recibe como parm. una ListaAgrupable, es decir un Vector cuyos elementos queremos
  // agrupar seg�n cierto criterio
  //Devuelve una ListaAgrupable cuyos elementos ser�n tambi�n de clase ListaAgrupable.
  //Es decir, subdivide el Vector inicial en otros Vectores cuyos elementos pertenecen al mismo grupo

  //NOTA: Se supone que ListaAgrupable parametro ven�a ordenada, es decir, con elemntos
  //de un mismo grupo seguidos. El m�todo se limita a separar los grupos
  public ListaAgrupable agrupa(ListaAgrupable param) {
    ListaAgrupable lisDev = new ListaAgrupable(); //Representa lista a devolver
    ListaAgrupable lisGrupo = new ListaAgrupable(); //Representa un grupo
    int act; //Indice de elem actual
    int sig; //Indice de elem siguiente
    if (param == null) {
      return null;
    }
    //Lista par�metro vacia
    //Se devuelve vac�a
    if (param.size() == 0) {
      return param;
    }
    //Lista par�metro tiene un solo elemento
    //Se devuelve un solo grupo
    if (param.size() == 1) {
      lisGrupo.addElement(param.firstElement());
      lisDev.addElement(lisGrupo);
      return lisDev;
    }
    else {
      //Se inicializan variables que recorren la lista par�metro
      act = 0;
      sig = 1;
      //Se crea el primer grupo y se a�ade primer elem
      lisGrupo.addElement(param.firstElement());

      //Bucle for : act  Debe recorrer valores entre 0 y size-2. (< param.size()-1)
      //De esta forma sig recorre valores entre 1 y size-1
      for (act = 0; act < param.size() - 1; ) {
        //Si elem actual y el sigte son del mismo grupo se meten en lisGrupo
        if ( ( (Agrupable) (param.elementAt(act))).mismoGrupoQue( (Agrupable) (
            param.elementAt(sig)))) {
          lisGrupo.addElement(param.elementAt(sig));
        }
        //Si elem actual y sig. son de distinto grupo
        else {
          //Se a�ade grupo anterior a la lista a devolver
          lisDev.addElement(lisGrupo);
          //Se crea un nuevo grupo y en �l se mete el elem sig
          lisGrupo = new ListaAgrupable();
          lisGrupo.addElement(param.elementAt(sig));
        }
        act++;
        sig++;
      } //for
      //Se a�ade el �ltimo grupo
      lisDev.addElement(lisGrupo);

      return lisDev;
    } //else

  } //agrupa

} //CLASE