package nivel1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//
import java.util.Enumeration;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvNivel1
    extends DBServlet {

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = new CLista();
    DataNivel1 nivel1 = null;

    //Para manejo de descripciones
    String sDes = "";
    String sDesL = "";

// // System_out.println("^^^^^^^Las 20:15");

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    nivel1 = (DataNivel1) param.firstElement();

    //////////////// modificacion jlt 18/12/2001
    String strAreas = "";
    Vector areas = null;
    areas = param.getVN1();
    // Recorremos el vector
    String strAux = "";
    boolean hayArea = false;

    if (areas != null &&
        !areas.equals("")) {

      hayArea = true;

      Enumeration enum;
      try {
        enum = areas.elements();
      }
      catch (Exception e) {
        enum = null;
      }

      while (enum.hasMoreElements()) {
        strAux = strAux + enum.nextElement();
        if (enum.hasMoreElements()) {
          strAux = strAux + ",";
        }
      }

      if ( (strAux.trim().equals("")) || (strAux.trim().equals("N"))) {
        strAreas = "";
      }
      else {
        strAreas = " and CD_NIVEL_1 in (" + strAux + ")";

      }
    }
    ///////////////////////////////////

    // modos de operaci�n
    switch (opmode) {

      // alta de nivel 1
      case servletALTA:

        // prepara la query
        query = "insert into SIVE_NIVEL1_S (CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3) values (?, ?, ?, ?, ?, ?)";
        st = con.prepareStatement(query);

        // codigo
        st.setString(1, nivel1.getCod().trim());
        // descripci�n
        st.setString(2, nivel1.getDes().trim());
        // descripci�n local
        if (nivel1.getDesL().trim().length() > 0) {
          st.setString(3, nivel1.getDesL().trim());
        }
        else {
          st.setNull(3, java.sql.Types.VARCHAR);

          // buz�n de email 1
        }
        if (nivel1.getEMail1().trim().length() > 0) {
          st.setString(4, nivel1.getEMail1().trim());
        }
        else {
          st.setNull(4, java.sql.Types.VARCHAR);

          // buz�n de email 2
        }
        if (nivel1.getEMail2().trim().length() > 0) {
          st.setString(5, nivel1.getEMail2().trim());
        }
        else {
          st.setNull(5, java.sql.Types.VARCHAR);

          // buz�n de email 3
        }
        if (nivel1.getEMail3().trim().length() > 0) {
          st.setString(6, nivel1.getEMail3().trim());
        }
        else {
          st.setNull(6, java.sql.Types.VARCHAR);

          // lanza la query
        }
        st.executeUpdate();
        st.close();

        break;

        // baja de enfermedad CIE
      case servletBAJA:

        // prepara la query
        query = "DELETE FROM SIVE_NIVEL1_S WHERE CD_NIVEL_1 = ?";

        // lanza la query
        st = con.prepareStatement(query);
        st.setString(1, nivel1.getCod().trim());
        st.executeUpdate();
        st.close();

        break;

        // b�squeda de niveles 1
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        // prepara la query
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_X_CODIGO) {

            //query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where CD_NIVEL_1 like ? and CD_NIVEL_1 > ? order by CD_NIVEL_1";
            query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where CD_NIVEL_1 like ? and CD_NIVEL_1 > ? ";
          }
          else {

            //query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where DS_NIVEL_1 like ? and DS_NIVEL_1 > ? order by DS_NIVEL_1";
            query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where upper(DS_NIVEL_1) like upper(?) and upper(DS_NIVEL_1) > upper(?) ";
          }
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO) {

            //query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where CD_NIVEL_1 like ? order by CD_NIVEL_1";
            query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where CD_NIVEL_1 like ? ";
          }
          else {

            //query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where DS_NIVEL_1 like ? order by DS_NIVEL_1";
            query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where upper(DS_NIVEL_1) like upper(?) ";
          }
        }

        // prepara la lista de resultados
        data = new CLista();

        // modificacion jlt
        if (hayArea) {
          query = query + strAreas;
        }
        query = query + " order by CD_NIVEL_1";

        // lanza la query
        st = con.prepareStatement(query);
        // filtro
        if (opmode == servletSELECCION_X_CODIGO) {
          st.setString(1, nivel1.getCod().trim() + "%");
        }
        else {
          st.setString(1, "%" + nivel1.getCod().trim() + "%");
          // paginaci�n
        }
        if (param.getFilter().length() > 0) {
          st.setString(2, param.getFilter().trim());
        }
        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataNivel1) data.lastElement()).getCod());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          //______________________________________

          sDes = rs.getString("DS_NIVEL_1");
          sDesL = rs.getString("DSL_NIVEL_1");

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }

          // a�ade un nodo
          data.addElement(new DataNivel1(rs.getString("CD_NIVEL_1"), sDes,
                                         sDesL, rs.getString("DS_EMAIL_1"),
                                         rs.getString("DS_EMAIL_2"),
                                         rs.getString("DS_EMAIL_3")));
          //______________________________________

          i++;
        }

        rs.close();
        st.close();

        break;

        // modificaci�n de nivel 1
      case servletMODIFICAR:

        // prepara la query
        query = "UPDATE SIVE_NIVEL1_S SET DS_NIVEL_1=?, DSL_NIVEL_1=?, DS_EMAIL_1=?, DS_EMAIL_2=?, DS_EMAIL_3=? WHERE CD_NIVEL_1=?";

        // lanza la query
        st = con.prepareStatement(query);
        st.setString(1, nivel1.getDes().trim());
        // descripci�n local
        if (nivel1.getDesL().trim().length() > 0) {
          st.setString(2, nivel1.getDesL().trim());
        }
        else {
          st.setNull(2, java.sql.Types.VARCHAR);
          // buz�n de email 1
        }
        if (nivel1.getEMail1().trim().length() > 0) {
          st.setString(3, nivel1.getEMail1().trim());
        }
        else {
          st.setNull(3, java.sql.Types.VARCHAR);
          // buz�n de email 2
        }
        if (nivel1.getEMail2().trim().length() > 0) {
          st.setString(4, nivel1.getEMail2().trim());
        }
        else {
          st.setNull(4, java.sql.Types.VARCHAR);
          // buz�n de email 3
        }
        if (nivel1.getEMail3().trim().length() > 0) {
          st.setString(5, nivel1.getEMail3().trim());
        }
        else {
          st.setNull(5, java.sql.Types.VARCHAR);
          // codigo
        }
        st.setString(6, nivel1.getCod().trim());
        st.executeUpdate();
        st.close();

        break;

      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:

        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO) {

          //query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where CD_NIVEL_1 = ? order by CD_NIVEL_1";
          query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where CD_NIVEL_1 = ? ";
        }
        else {

          //query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where DS_NIVEL_1 = ? order by DS_NIVEL_1";
          query = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1, DS_EMAIL_1, DS_EMAIL_2, DS_EMAIL_3 from SIVE_NIVEL1_S where DS_NIVEL_1 = ? ";

        }
        if (hayArea) {
          query = query + strAreas;
        }
        query = query + " order by CD_NIVEL_1";

        // lanza la query
        st = con.prepareStatement(query);
        st.setString(1, nivel1.getCod().trim());
        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {

          //____________________________________

          sDes = rs.getString("DS_NIVEL_1");
          sDesL = rs.getString("DSL_NIVEL_1");

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }
          // a�ade un nodo
          data.addElement(new DataNivel1(rs.getString("CD_NIVEL_1"), sDes,
                                         sDesL, rs.getString("DS_EMAIL_1"),
                                         rs.getString("DS_EMAIL_2"),
                                         rs.getString("DS_EMAIL_3")));

          //____________________________
        }
        rs.close();
        st.close();

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }
}
