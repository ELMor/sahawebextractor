package panelnivan;

import capp.CApp;

public class AppletNivAn
    extends CApp {
  public void init() {
    super.init();
  }

  public void start() {
    setTitulo("Selección zonificación sanitaria");
    CApp a = (CApp)this;

    // muestra el panel
    VerPanel("Selección zonificación sanitaria",
             new Pan_NivelesAnyo(this,
                                 a.getANYO_DEFECTO(),
                                 a.getCD_NIVEL1_DEFECTO(),
                                 a.getDS_NIVEL1_DEFECTO(),
                                 a.getCD_NIVEL2_DEFECTO(),
                                 a.getDS_NIVEL2_DEFECTO()),
             false);

    // muestra el panel que llama al diálogo
    /*VerPanel("Selección zonificación sanitaria",
             new panelNivAn( this ));
     */
    VerPanel("Selección zonificación sanitaria");
  }
}