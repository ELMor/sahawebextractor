package descarga;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

public class SrvDescarga
    extends DBServlet {

  String sLinea = "";

  // modos de operaci�n
  final int servletGEN_FICH_DE_TABLA = 1;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    // contador
    int i = 1;

    // objetos de datos
    CLista data = null;
    DataPet cat = null;

    //Elemento que indica tabla
    int iNumTabla;

    // Campo recogido en una selecci�n
    String sCam = null;

    //Para recoger campos enteros
    int iCam = 0;
    /*
        String sTab = null;
        String sCod = null;
        String sDes = null;
        String sDesL = null;
     */

    try {

      //Inicializacion Vector con las posibles consultas
      Vector vectorConsultas = new Vector();
      //Necesario insetar en este orden. Si se cambian los indices, cambiar orden de insercion tambien
      vectorConsultas.insertElementAt(
          " select CD_PAIS, DS_PAIS from SIVE_PAISES ", DataPet.tablaPAISES);
      vectorConsultas.insertElementAt(" select CD_CA, DS_CA from SIVE_COM_AUT ",
                                      DataPet.tablaCCAA);
      vectorConsultas.insertElementAt(
          " select CD_PROV, CD_CA, DS_PROV from SIVE_PROVINCIA ",
          DataPet.tablaPROVINCIA);
      vectorConsultas.insertElementAt(" select CD_PROV, CD_MUN, CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, DS_MUN from SIVE_MUNICIPIO ",
                                      DataPet.tablaMUNICIPIO);

      vectorConsultas.insertElementAt(
          " select CD_NIVEL_1, DS_NIVEL_1 from SIVE_NIVEL1_S ",
          DataPet.tablaNIVEL1_S);
      vectorConsultas.insertElementAt(
          " select CD_NIVEL_1, CD_NIVEL_2, DS_NIVEL_2 from SIVE_NIVEL2_S ",
          DataPet.tablaNIVEL2_S);
      vectorConsultas.insertElementAt(
          " select CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, DS_ZBS from SIVE_ZONA_BASICA ",
          DataPet.tablaZONA_BASICA);

      vectorConsultas.insertElementAt(
          " select CD_ENFCIE, DS_ENFERCIE from SIVE_ENFER_CIE ",
          DataPet.tablaENFER_CIE);
      vectorConsultas.insertElementAt(
          " select CD_ENFEREDO, DS_ENFEREDO from SIVE_EDO_CNE",
          DataPet.tablaEDO_CNE);
      vectorConsultas.insertElementAt(
          " select CD_TSIVE, DS_TSIVE from SIVE_TSIVE ", DataPet.tablaTSIVE);
      vectorConsultas.insertElementAt(
          " select CD_TLINEA, DS_TLINEA from SIVE_TLINEA ", DataPet.tablaTLINEA);
      vectorConsultas.insertElementAt(
          " select CD_TPREG, DS_TPREG from SIVE_TPREGUNTA ",
          DataPet.tablaTPREGUNTA);
      vectorConsultas.insertElementAt(
          " select CD_TVIGI, DS_TVIGI from SIVE_T_VIGILANCIA ",
          DataPet.tablaT_VIGILANCIA);
      vectorConsultas.insertElementAt(
          " select CD_MOTBAJA, DS_MOTBAJA from SIVE_MOTIVO_BAJA ",
          DataPet.tablaMOTIVO_BAJA);
      vectorConsultas.insertElementAt(
          " select CD_NIVASIS, DS_NIVASIS from SIVE_NIV_ASIST ",
          DataPet.tablaNIV_ASIST);
      vectorConsultas.insertElementAt(
          " select CD_SEXO, DS_SEXO from SIVE_SEXO ", DataPet.tablaSEXO);
      //vectorConsultas.insertElementAt(" select CD_TASIST, DS_TASIST from SIVE_TASISTENCIA " , DataPet.tablaTASISTENCIA);
      //vectorConsultas.insertElementAt(" select CD_ESPEC, DS_ESPEC from SIVE_ESPECIALIDAD " , DataPet.tablaESPECIALIDAD);

      vectorConsultas.insertElementAt(" select CD_NIVEL_1, CD_NIVEL_2, CD_ZBS,CD_ANOEPI,CD_SEXO, NM_EDAD, NM_POBLACION from SIVE_POBLACION_NS ",
                                      DataPet.tablaPOBLACION_NS);
      vectorConsultas.insertElementAt(" select CD_PROV, CD_MUN, CD_ANOEPI,CD_SEXO, NM_EDAD, NM_POBLACION from SIVE_POBLACION_NG ",
                                      DataPet.tablaPOBLACION_NG);

      vectorConsultas.insertElementAt(
          " select CD_GRUPO, DS_GRUPO from SIVE_GRUPO_BROTE ",
          DataPet.tablaGRUPOBROTE);
      vectorConsultas.insertElementAt(
          " select CD_TNOTIF, DS_TNOTIF from SIVE_T_NOTIFICADOR ",
          DataPet.tablaTNOTIFICADOR);
      vectorConsultas.insertElementAt(
          " select CD_TRANSMIS, DS_TRANSMIS from SIVE_MEC_TRANS ",
          DataPet.tablaMTRANSMISION);
      vectorConsultas.insertElementAt(
          " select CD_TIPOCOL, DS_TIPOCOL from SIVE_TIPOCOL ",
          DataPet.tablaTCOLECTIVO);
      vectorConsultas.insertElementAt(
          " select CD_SITALERBRO, DS_SITALERBRO from SIVE_T_SIT_ALERTA ",
          DataPet.tablaSALERTA);
      vectorConsultas.insertElementAt(
          " select CD_GRUPO, CD_TBROTE, DS_TBROTE from SIVE_TIPO_BROTE ",
          DataPet.tablaTBROTE);

      // establece la conexi�n con la base de datos
      con = openConnection();
      con.setAutoCommit(true);

      // obtiene los par�metros enviados desde el servidor
      cat = (DataPet) param.firstElement();
      iNumTabla = cat.getNumTabla();

      // modos de operaci�n
      switch (opmode) {

        // extrae un item del cat�logo por su c�digo
        case servletGEN_FICH_DE_TABLA:

          // prepara la query
          query = (String) (vectorConsultas.elementAt(iNumTabla));

          // lanza la query
          st = con.prepareStatement(query);
          rs = st.executeQuery();

          data = new CLista();

          // extrae el item encontrado
          while (rs.next()) {

            //Se recoge una linea del fichero tomando los valores de
            switch (iNumTabla) {

              case (DataPet.tablaPAISES):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_PAIS");
                anadir(sCam, 3);
                sCam = rs.getString("DS_PAIS");
                anadir(sCam, 40);
                break;

              case (DataPet.tablaCCAA):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_CA");
                anadir(sCam, 2);
                sCam = rs.getString("DS_CA");
                anadir(sCam, 50);

                break;

              case (DataPet.tablaPROVINCIA):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_PROV");
                anadir(sCam, 2);
                sCam = rs.getString("CD_CA");
                anadir(sCam, 2);
                sCam = rs.getString("DS_PROV");
                anadir(sCam, 20);

                break;

              case (DataPet.tablaMUNICIPIO):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_PROV");
                anadir(sCam, 2);
                sCam = rs.getString("CD_MUN");
                anadir(sCam, 3);
                sCam = rs.getString("CD_NIVEL_1");
                if (sCam == null) {
                  sCam = "";
                }
                anadir(sCam, 2);
                sCam = rs.getString("CD_NIVEL_2");
                if (sCam == null) {
                  sCam = "";
                }
                anadir(sCam, 2);
                sCam = rs.getString("CD_ZBS");
                if (sCam == null) {
                  sCam = "";
                }
                anadir(sCam, 2);
                sCam = rs.getString("DS_MUN");
                anadir(sCam, 50);
                break;

              case (DataPet.tablaNIVEL1_S):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_NIVEL_1");
                anadir(sCam, 2);
                sCam = rs.getString("DS_NIVEL_1");
                anadir(sCam, 30);

                break;

              case (DataPet.tablaNIVEL2_S):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_NIVEL_1");
                anadir(sCam, 2);
                sCam = rs.getString("CD_NIVEL_2");
                anadir(sCam, 2);

                sCam = rs.getString("DS_NIVEL_2");
                anadir(sCam, 30);

                break;

              case (DataPet.tablaZONA_BASICA):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_NIVEL_1");
                anadir(sCam, 2);
                sCam = rs.getString("CD_NIVEL_2");
                anadir(sCam, 2);
                sCam = rs.getString("CD_ZBS");
                anadir(sCam, 2);
                sCam = rs.getString("DS_ZBS");
                anadir(sCam, 30);

                break;

              case (DataPet.tablaENFER_CIE):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_ENFCIE");
                anadir(sCam, 6);
                sCam = rs.getString("DS_ENFERCIE");
                anadir(sCam, 40);
                /*
                              sCam = rs.getString("DSL_ENFERCIE");
                              if (sCam == null)
                                sCam = "";
                              anadir(sCam,40);
                              sCam = rs.getString("DSI_ENFERCIE");
                              if (sCam == null)
                                sCam = "";
                              anadir(sCam,40);
                 */
                break;

              case (DataPet.tablaEDO_CNE):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_ENFEREDO");
                anadir(sCam, 3);
                sCam = rs.getString("DS_ENFEREDO");
                anadir(sCam, 40);
                /*
                              sCam = rs.getString("DSL_ENFEREDO");
                              if (sCam == null)
                                sCam = "";
                              anadir(sCam,40);
                 */
                break;

                //____________________________________________________

              case (DataPet.tablaTSIVE):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_TSIVE");
                anadir(sCam, 1);
                sCam = rs.getString("DS_TSIVE");
                anadir(sCam, 30);
                break;

              case (DataPet.tablaTLINEA):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_TLINEA");
                anadir(sCam, 1);
                sCam = rs.getString("DS_TLINEA");
                anadir(sCam, 30);
                break;

              case (DataPet.tablaTPREGUNTA):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_TPREG");
                anadir(sCam, 1);
                sCam = rs.getString("DS_TPREG");
                anadir(sCam, 30);
                break;

              case (DataPet.tablaT_VIGILANCIA):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_TVIGI");
                anadir(sCam, 1);
                sCam = rs.getString("DS_TVIGI");
                anadir(sCam, 30);
                break;

              case (DataPet.tablaMOTIVO_BAJA):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_MOTBAJA");
                anadir(sCam, 2);
                sCam = rs.getString("DS_MOTBAJA");
                anadir(sCam, 40);
                break;

              case (DataPet.tablaNIV_ASIST):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_NIVASIS");
                anadir(sCam, 1);
                sCam = rs.getString("DS_NIVASIS");
                anadir(sCam, 15);
                break;

              case (DataPet.tablaSEXO):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_SEXO");
                anadir(sCam, 1);
                sCam = rs.getString("DS_SEXO");
                anadir(sCam, 12);
                break;

                /*            case(DataPet.tablaTASISTENCIA):
                              sLinea = "";
                              // obtiene los campos
                              sCam = rs.getString("CD_TASIST");
                              anadir(sCam,1);
                              sCam = rs.getString("DS_TASIST");
                              anadir(sCam,15);
                              break;
                            case(DataPet.tablaESPECIALIDAD):
                              sLinea = "";
                              // obtiene los campos
                              sCam = rs.getString("CD_ESPEC");
                              anadir(sCam,1);
                              sCam = rs.getString("DS_ESPEC");
                              anadir(sCam,15);
                              break;*/

              case (DataPet.tablaPOBLACION_NS):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_NIVEL_1");
                anadir(sCam, 2);
                sCam = rs.getString("CD_NIVEL_2");
                anadir(sCam, 2);
                sCam = rs.getString("CD_ZBS");
                anadir(sCam, 2);
                sCam = rs.getString("CD_ANOEPI");
                anadir(sCam, 4);
                sCam = rs.getString("CD_SEXO");
                anadir(sCam, 1);
                iCam = rs.getInt("NM_EDAD");
                sCam = Integer.toString(iCam);
                anadir(sCam, 3); //********************
                iCam = rs.getInt("NM_POBLACION");
                sCam = Integer.toString(iCam);
                anadir(sCam, 8); //********************
                break;

              case (DataPet.tablaPOBLACION_NG):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_PROV");
                anadir(sCam, 2);
                sCam = rs.getString("CD_MUN");
                anadir(sCam, 3);
                sCam = rs.getString("CD_ANOEPI");
                anadir(sCam, 4);
                sCam = rs.getString("CD_SEXO");
                anadir(sCam, 1);
                iCam = rs.getInt("NM_EDAD");
                sCam = Integer.toString(iCam);
                anadir(sCam, 3); //********************
                iCam = rs.getInt("NM_POBLACION");
                sCam = Integer.toString(iCam);
                anadir(sCam, 8); //********************
                break;

                // brotes
              case (DataPet.tablaGRUPOBROTE):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_GRUPO");
                anadir(sCam, 1);
                sCam = rs.getString("DS_GRUPO");
                anadir(sCam, 25);
                break;
              case (DataPet.tablaTNOTIFICADOR):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_TNOTIF");
                anadir(sCam, 1);
                sCam = rs.getString("DS_TNOTIF");
                anadir(sCam, 25);
                break;
              case (DataPet.tablaMTRANSMISION):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_TRANSMIS");
                anadir(sCam, 4);
                sCam = rs.getString("DS_TRANSMIS");
                anadir(sCam, 40);
                break;
              case (DataPet.tablaTCOLECTIVO):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_TIPOCOL");
                anadir(sCam, 2);
                sCam = rs.getString("DS_TIPOCOL");
                anadir(sCam, 40);
                break;
              case (DataPet.tablaSALERTA):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_SITALERBRO");
                anadir(sCam, 1);
                sCam = rs.getString("DS_SITALERBRO");
                anadir(sCam, 40);
                break;
              case (DataPet.tablaTBROTE):
                sLinea = "";
                // obtiene los campos
                sCam = rs.getString("CD_GRUPO");
                anadir(sCam, 1);
                sCam = rs.getString("CD_TBROTE");
                anadir(sCam, 2);
                sCam = rs.getString("DS_TBROTE");
                anadir(sCam, 40);
                break;
            }

            // a�ade un nodo (una linea) a la lista de lineas
            data.addElement(sLinea);

          }
          rs.close();
          st.close();

          break;

      }

      // cierra la conexion y acaba el procedimiento doWork
      closeConnection(con);

      if (data != null) {
        data.trimToSize();

        //fall� la transacci�n
      }
    }
    catch (Exception ex) {
      //   con.rollback();
      throw ex;
    }
    return data;

  }

  /*
// Recoge los datos y los a�ade a una  linea en tablas que solo tienen cod y desc
   void recogerDatosLinea(ResultSet rs,String campo,int longMaxCod, int longMaxDes) {
      String sCam = null;
      sLinea = "";
      // obtiene los campos
      sCam = rs.getString("CD_" + campo);
      anadir(sCam,longMaxCod);
      sCam = rs.getString("DS_" + campo);
      anadir(sCam,longMaxDes);
   }
   */

  //____________________________________________________________________________

  //A�ade al String sLinea (usado para cada linea del fichero) el String
  //indicado en parametro cual
  //Si cual no llega a ocupar longMax caracteres, estos caracteres se ponen como blancos
  void anadir(String cual, int longMax) {
    sLinea = sLinea + cual;
    for (int cont = 0; cont < (longMax - (cual.length())); cont++) {
      sLinea = sLinea + " ";
    }
  }

} //CLASE