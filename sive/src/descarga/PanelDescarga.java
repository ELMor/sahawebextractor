//Title:        Your Product Name
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:  Your description

package descarga;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CFileName;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import sapp.StubSrvBD;

//________________________________________________________________

public class PanelDescarga
    extends CPanel {

  //Variable y ctes para modo pantalla
  int modoOperacion;
  ResourceBundle res;
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variable y ctes. para modos de consulta al servlet
  int modoServlet;
  final int servletGEN_FICH_DE_TABLA = 1;
  //Localizacion del servlet
  final String strSERVLET = "servlet/SrvDescarga";

  //rutas imagenes
  final String imgNAME[] = {
      "images/salvar.gif"};

  protected CLista listaFich = new CLista();
  protected StubSrvBD stubCliente = new StubSrvBD();

//______________________________________________________

  BorderLayout borderLayout1 = new BorderLayout();
  StatusBar statusBar = new StatusBar();
  Panel pnl = new Panel();
  XYLayout xYLayout1 = new XYLayout();
  Label lblTabla = new Label();
  Choice chTabla = new Choice();
  CFileName panFile;
  ButtonControl btnDescarga = new ButtonControl();
  PanelDescargaActionListener btnActionListener = new
      PanelDescargaActionListener(this);

  protected CCargadorImagen imgs = null;

  public PanelDescarga(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("descarga.Res" + app.getIdioma());
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(400, 245));
    statusBar.setBevelInner(BevelPanel.LOWERED);
    statusBar.setBevelOuter(BevelPanel.LOWERED);
    panFile = new CFileName(this.app);

    statusBar.setText("");
    lblTabla.setText(res.getString("lblTabla.Text"));
    chTabla.addItemListener(new PanelDescarga_chTabla_itemAdapter(this));
    btnDescarga.setLabel(res.getString("btnDescarga.Label"));
    pnl.setLayout(xYLayout1);
    this.setLayout(borderLayout1);

    this.add(statusBar, BorderLayout.SOUTH);
    this.add(pnl, BorderLayout.CENTER);

    pnl.add(lblTabla, new XYConstraints(26, 27, -1, -1));
    pnl.add(chTabla, new XYConstraints(93, 27, 223, -1));
    pnl.add(panFile, new XYConstraints(18, 80, -1, -1));
    pnl.add(btnDescarga, new XYConstraints(353, 146, -1, -1));
    btnDescarga.addActionListener(btnActionListener);

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnDescarga.setImage(imgs.getImage(0));

    inicializarChoice();
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  void inicializarChoice() {
    chTabla.addItem(" ");

    chTabla.insert(res.getString("msg2.Text"), DataPet.tablaPAISES + 1);
    chTabla.insert(res.getString("msg3.Text"), DataPet.tablaCCAA + 1);
    chTabla.insert(res.getString("msg4.Text"), DataPet.tablaPROVINCIA + 1);
    chTabla.insert(res.getString("msg5.Text"), DataPet.tablaMUNICIPIO + 1);

    chTabla.insert(res.getString("msg6.Text"), DataPet.tablaNIVEL1_S + 1);
    chTabla.insert(res.getString("msg7.Text"), DataPet.tablaNIVEL2_S + 1);
    chTabla.insert(res.getString("msg8.Text"), DataPet.tablaZONA_BASICA + 1);

    chTabla.insert(res.getString("msg9.Text"), DataPet.tablaENFER_CIE + 1);
    chTabla.insert(res.getString("msg10.Text"), DataPet.tablaEDO_CNE + 1);

    chTabla.insert(res.getString("msg11.Text"), DataPet.tablaTSIVE + 1);
    chTabla.insert(res.getString("msg12.Text"), DataPet.tablaTLINEA + 1);
    chTabla.insert(res.getString("msg13.Text"), DataPet.tablaTPREGUNTA + 1);
    chTabla.insert(res.getString("msg14.Text"), DataPet.tablaT_VIGILANCIA + 1);
    chTabla.insert(res.getString("msg15.Text"), DataPet.tablaMOTIVO_BAJA + 1);
    chTabla.insert(res.getString("msg16.Text"), DataPet.tablaNIV_ASIST + 1);
    chTabla.insert(res.getString("msg17.Text"), DataPet.tablaSEXO + 1);

    chTabla.insert(res.getString("msg18.Text"), DataPet.tablaGRUPOBROTE + 1);
    chTabla.insert(res.getString("msg19.Text"), DataPet.tablaTNOTIFICADOR + 1);
    chTabla.insert(res.getString("msg22.Text"), DataPet.tablaMTRANSMISION + 1);
    chTabla.insert(res.getString("msg23.Text"), DataPet.tablaTCOLECTIVO + 1);
    chTabla.insert(res.getString("msg24.Text"), DataPet.tablaSALERTA + 1);
    chTabla.insert(res.getString("msg25.Text"), DataPet.tablaTBROTE + 1);

    chTabla.insert(res.getString("msg20.Text"), DataPet.tablaPOBLACION_NS + 1);
    chTabla.insert(res.getString("msg21.Text"), DataPet.tablaPOBLACION_NG + 1);
  }

  //________________________________________________________________

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        btnDescarga.setEnabled(true);
        InicializarPanFile();
        panFile.setEnabled(true);
        chTabla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnDescarga.setEnabled(false);
        InicializarPanFile();
        panFile.setEnabled(false);
        chTabla.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    doLayout();
  }

//________________________________________________________________

  void InicializarPanFile() {

    if (chTabla.getSelectedIndex() == 0) {
      panFile.txtFile.setEditable(false);
      panFile.setEnabled(false);
      btnDescarga.setEnabled(false);
    }
    else {
      panFile.txtFile.setEditable(true);
      panFile.setEnabled(true);
      btnDescarga.setEnabled(true);
    }

  }

//________________________________________________________________

  void btnDescarga_actionPerformed(ActionEvent evt) {

    CLista data;
    int j = 0;
    String sLinea;
    File miFichero = null;
    FileWriter fStream = null;
    CMessage msgBox;

//       if (isDataValid() == true)   {

    if (1 == 1) {
      try {

        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataPet(chTabla.getSelectedIndex() - 1));

//# // System_out.println("A�adido Elemento*******");

        statusBar.setText(res.getString("statusBar.Text"));

        // apunta al servlet principal
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

        // obtiene la lista
        listaFich = (CLista) stubCliente.doPost(servletGEN_FICH_DE_TABLA, data);

//# // System_out.println("Obtenida lista*******");

        if (listaFich.size() > 0) {
          miFichero = new File(this.panFile.txtFile.getText());
          fStream = new FileWriter(miFichero);

          for (j = 0; j < listaFich.size(); j++) {
            sLinea = (String) listaFich.elementAt(j);
            fStream.write(sLinea);
            fStream.write("\r");
            fStream.write("\n");
          }
          fStream.close();
          fStream = null;
          miFichero = null;
          statusBar.setText(res.getString("statusBar.Text1"));
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg21.Text"));
          msgBox.show();
          msgBox = null;
          statusBar.setText("");
        } //else

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        statusBar.setText("");
      }

    } //if isDataValid
    this.modoOperacion = modoNORMAL;
    Inicializar();

  } //Fin de funcion

  void chTabla_itemStateChanged(ItemEvent e) {
    InicializarPanFile();
  }

} //FIN DE CLASE

//________________________________________________________________
//________________________________________________________________

// action listener para los botones
class PanelDescargaActionListener
    implements ActionListener, Runnable {
  PanelDescarga adaptee = null;
  ActionEvent e = null;

  public PanelDescargaActionListener(PanelDescarga adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.btnDescarga_actionPerformed(e);
  }
}

class PanelDescarga_chTabla_itemAdapter
    implements java.awt.event.ItemListener {
  PanelDescarga adaptee;

  PanelDescarga_chTabla_itemAdapter(PanelDescarga adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chTabla_itemStateChanged(e);
  }
}
