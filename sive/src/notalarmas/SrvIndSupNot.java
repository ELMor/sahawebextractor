//Title:        Notificacion de alarmas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Durante la notificacion de los casos que se han dado de una enfermedad,
//muestra los indicadores para los que se ha superado el umbral.

//07/03/2001
// Cambio: Se ha reconstruido partiendo del anterior para adecuarlo al cambio en
//         la visibilidad que se da a los distintos perfiles.

package notalarmas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

//import jdbcpool.*; // $$$ modo debug

public class SrvIndSupNot
    extends DBServlet {

  // modos de operaci�n
  final int servletIND_SUP = 1;

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    Integer casos;
    Vector alarmDistrito = new Vector();
    Vector alarmArea = new Vector();
    Vector alarmCA = new Vector();
    Vector alarmCN = new Vector();
    parNotifAlar parCons = null;

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      //Conexi�n con la base de datos
    }
    Connection con = null;

    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    parCons = (parNotifAlar) param.firstElement();

    switch (opmode) {
      case servletIND_SUP: {
        try {
          //// System_out.print("SrvIndSupNot--********* Perfil:  ");
          //// System_out.println(param.getPerfil());
          switch (param.getPerfil()) {
            case 4: //nivel2

              //// System_out.print("Pasa por nivel 2, Perfil:  ");
              //// System_out.println(param.getPerfil());
              casos = obtenerCasosDistrito(con, parCons.ano(), parCons.semana(),
                                           parCons.enfermedad(),
                                           parCons.cAutonoma(),
                                           parCons.nivel1(), parCons.nivel2());

              alarmDistrito = comprobarIndicadoresDistrito(con, casos,
                  parCons.ano(),
                  parCons.semana(), parCons.enfermedad(),
                  parCons.cAutonoma(), parCons.nivel1(),
                  parCons.nivel2());

              //pasamos a la parte de Area

            case 3: //nivel1

              //// System_out.print("Pasa por nivel 1, Perfil:  ");
              //// System_out.println(param.getPerfil());
              casos = obtenerCasosArea(con, parCons.ano(), parCons.semana(),
                                       parCons.enfermedad(), parCons.cAutonoma(),
                                       parCons.nivel1());

              alarmArea = comprobarIndicadoresArea(con, casos, parCons.ano(),
                  parCons.semana(), parCons.enfermedad(),
                  parCons.cAutonoma(), parCons.nivel1());

              //pasamos a la parte de Comunidad Aut�noma.

            case 2: //Comunidad Aut�noma
              //// System_out.print("Pasa por CA, Perfil:  ");
              //// System_out.println(param.getPerfil());
            case 1: //Centro Notificador

              //// System_out.print("Pasa por CN, Perfil:  ");
              //// System_out.println(param.getPerfil());
              casos = obtenerCasosCA(con, parCons.ano(), parCons.semana(),
                                     parCons.enfermedad(),
                                     parCons.cAutonoma());

              if (param.getPerfil() >= 2) {
                //// System_out.print("Pasa por CA, Perfil:  ");
                //// System_out.println(param.getPerfil());
                alarmCA = comprobarIndicadoresCA(con, casos, parCons.ano(),
                                                 parCons.semana(),
                                                 parCons.enfermedad(),
                                                 parCons.cAutonoma());
              }

              alarmCN = comprobarIndicadoresCN(con, casos, parCons.ano(),
                                               parCons.semana(),
                                               parCons.enfermedad());
          }

          Vector resultado = alarmDistrito;
          for (int i = 0; i < alarmArea.size(); i++) {
            resultado.addElement(alarmArea.elementAt(i));
          }
          //// System_out.print(" Tamano de Indicadores CA : ");
          //// System_out.println(alarmCA.size());
          for (int i = 0; i < alarmCA.size(); i++) {
            resultado.addElement(alarmCA.elementAt(i));
          }
          for (int i = 0; i < alarmCN.size(); i++) {
            resultado.addElement(alarmCN.elementAt(i));
          }

          Vector dataCabecera = obtenerCabecera(con, param, parCons.enfermedad(),
                                                parCons.nivel1(),
                                                parCons.nivel2());

          CLista data = new CLista();
          data.addElement(resultado);
          data.addElement(dataCabecera);
          data.trimToSize();

          // Se borra de la tabla de registro de sesion de usuario
          registroConsultas.borrarSesion();
          closeConnection(con);

          return data;
        }
        catch (Exception e) {
          e.printStackTrace();
        }

      }
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    closeConnection(con);

    return null;
  }

  /**  obtenerCasosDistrito
   * Obtiene los casos de la enfermedad indicada para el distrito, la semana y el a�o
   * indicados por los par�metros
   * @con Conexion con la base de datos
   * @anno A�o para el que se est�n consultando los casos.
   * @semana Semana para la que se est�n consultado los casos.
   * @enfermedad C�digo de la enfermedad de la que queremos obtener los casos.
   * @cAutonoma C�digo de la Comunidad Aut�noma
   * @nivel1 C�digo de Area
   * @nivel2 C�digo de Distrito
   */
  protected Integer obtenerCasosDistrito(Connection con, String anno,
                                         String semana,
                                         String enfermedad, String cAutonoma,
                                         String nivel1, String nivel2) throws
      Exception {
    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    Integer resultado = null;

    /*query = "select count(NM_EDO) from SIVE_EDOIND edo "
               + " where CD_ANOEPI = ? and CD_SEMEPI = ? "
               + " and CD_ENFCIE = ?  and CD_NIVEL_1 in(select distinct "
               + " CD_NIVEL_1 FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV) and "
               + " CD_NIVEL_2 in (select distinct CD_NIVEL_2 "
               + " FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV) "
               + " and CD_PROV in (select distinct "
               + " muni.CD_PROV FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV)"
               + " and edo.CD_NIVEL_1 = ? "
               + " and edo.CD_NIVEL_2 = ? ";
     */
    query = " select count(NM_EDO) from SIVE_EDOIND edo "
        + " where CD_ANOEPI = ? and CD_SEMEPI = ? "
        + " and CD_ENFCIE = ? and CD_NIVEL_1 = ? "
        + " and CD_NIVEL_2 = ? and "
        + " CD_PROV in (select distinct CD_PROV from SIVE_PROVINCIA "
        + " where CD_CA = ?) ";

    st = con.prepareStatement(query);

    st.setString(1, anno);
    st.setString(2, semana);
    st.setString(3, enfermedad);
    //AIC
    st.setString(4, nivel1);
    st.setString(5, nivel2);
    st.setString(6, cAutonoma);
    //st.setString(4, cAutonoma);
    //st.setString(5, cAutonoma);
    //st.setString(6, cAutonoma);
    //st.setString(7, nivel1);
    //st.setString(8, nivel2);

    // ARG: Se a�aden parametros
    registroConsultas.insertarParametro(anno);
    registroConsultas.insertarParametro(semana);
    registroConsultas.insertarParametro(enfermedad);
    registroConsultas.insertarParametro(nivel1);
    registroConsultas.insertarParametro(nivel2);
    registroConsultas.insertarParametro(cAutonoma);

    rs = st.executeQuery();

    // ARG: Registro LORTAD
    registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                "", "SrvIndSupNot", true);

    if (rs.next()) {
      resultado = new Integer(rs.getInt(1));
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;

  }

  /**  obtenerCasosArea
       * Obtiene los casos de la enfermedad indicada para el �rea, la semana y el a�o
   * indicados por los par�metros
   * @con Conexion con la base de datos
   * @anno A�o para el que se est�n consultando los casos.
   * @semana Semana para la que se est�n consultado los casos.
   * @enfermedad C�digo de la enfermedad de la que queremos obtener los casos.
   * @cAutonoma C�digo de la Comunidad Aut�noma
   * @nivel1 C�digo de Area
   */
  protected Integer obtenerCasosArea(Connection con, String anno, String semana,
                                     String enfermedad, String cAutonoma,
                                     String nivel1) throws Exception {
    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    Integer resultado = null;

    //AIC
    /*query = "select count(NM_EDO) from SIVE_EDOIND edo "
               + " where CD_ANOEPI = ? and CD_SEMEPI = ? "
               + " and CD_ENFCIE = ?  and CD_NIVEL_1 in(select distinct "
               + " CD_NIVEL_1 FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV) and "
               + " CD_NIVEL_2 in (select distinct CD_NIVEL_2 "
               + " FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV) "
               + " and CD_PROV in (select distinct "
               + " muni.CD_PROV FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV)"
               + " and edo.CD_NIVEL_1 = ? ";  */
    query = " select count(NM_EDO) from SIVE_EDOIND edo "
        + " where CD_ANOEPI = ? and CD_SEMEPI = ? "
        + " and CD_ENFCIE = ? and CD_NIVEL_1 = ? "
        + " and CD_PROV in (select distinct "
        + " CD_PROV from SIVE_PROVINCIA "
        + " where CD_CA = ?) ";

    st = con.prepareStatement(query);

    st.setString(1, anno);
    st.setString(2, semana);
    st.setString(3, enfermedad);
    //AIC
    st.setString(4, nivel1);
    st.setString(5, cAutonoma);
    //st.setString(4, cAutonoma);
    //st.setString(5, cAutonoma);
    //st.setString(6, cAutonoma);
    //st.setString(7, nivel1);

    // ARG: Se a�aden parametros
    registroConsultas.insertarParametro(anno);
    registroConsultas.insertarParametro(semana);
    registroConsultas.insertarParametro(enfermedad);
    registroConsultas.insertarParametro(nivel1);
    registroConsultas.insertarParametro(cAutonoma);

    rs = st.executeQuery();

    // ARG: Registro LORTAD
    registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                "", "SrvIndSupNot", true);

    if (rs.next()) {
      resultado = new Integer(rs.getInt(1));
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;
  }

  /**  obtenerCasosArea
   * Obtiene los casos de la enfermedad indicada para la Comunidad Aut�noma,
   * la semana y el a�o indicados por los par�metros
   * @con Conexion con la base de datos
   * @anno A�o para el que se est�n consultando los casos.
   * @semana Semana para la que se est�n consultado los casos.
   * @enfermedad C�digo de la enfermedad de la que queremos obtener los casos.
   * @cAutonoma C�digo de la Comunidad Aut�noma
   */
  protected Integer obtenerCasosCA(Connection con, String anno, String semana,
                                   String enfermedad, String cAutonoma) throws
      Exception {
    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    Integer resultado = null;

    //AIC
    /*query = "select count(NM_EDO) from SIVE_EDOIND edo "
               + " where CD_ANOEPI = ? and CD_SEMEPI = ? "
               + " and CD_ENFCIE = ?  and CD_NIVEL_1 in(select distinct "
               + " CD_NIVEL_1 FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV) and "
               + " CD_NIVEL_2 in (select distinct CD_NIVEL_2 "
               + " FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV) "
               + " and CD_PROV in (select distinct "
               + " muni.CD_PROV FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV)";
     */
    query = " select count(NM_EDO) from SIVE_EDOIND edo "
        + " where CD_ANOEPI = ? and CD_SEMEPI = ? "
        + " and CD_ENFCIE = ?  "
        + " and CD_PROV in (select distinct "
        + " CD_PROV from SIVE_PROVINCIA pro where CD_CA = ?) ";

    st = con.prepareStatement(query);

    st.setString(1, anno);
    st.setString(2, semana);
    st.setString(3, enfermedad);
    st.setString(4, cAutonoma);
    //AIC
    //st.setString(5, cAutonoma);
    //st.setString(6, cAutonoma);

    // ARG: Se a�aden parametros
    registroConsultas.insertarParametro(anno);
    registroConsultas.insertarParametro(semana);
    registroConsultas.insertarParametro(enfermedad);
    registroConsultas.insertarParametro(cAutonoma);

    rs = st.executeQuery();

    // ARG: Registro LORTAD
    registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                "", "SrvIndSupNot", true);

    if (rs.next()) {
      resultado = new Integer(rs.getInt(1));
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;
  }

  /**  obtenerValorAlarma
   * Devuelve el valor de casos de la alarma correspondiente al indicador y la
   * fecha indicada.
   * @codIndicador C�digo del indicador
   * @anno A�o del que se consulta la alarma.
   * @semana Semana de la que se consulta la alarma.
   */
  protected Double obtenerValorAlarma(Connection con, String codIndicador,
                                      String anno, String semana) throws
      Exception {
    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    Double resultado = null;

    // Consulta para comparar los valores
    query = " select NM_VALOR "
        + " from SIVE_ALARMA "
        + " where CD_INDALAR = ? and CD_ANOEPI = ? and CD_SEMEPI = ? ";
    st = con.prepareStatement(query);
    st.setString(1, codIndicador);
    st.setString(2, anno);
    st.setString(3, semana);

    rs = st.executeQuery();

    if (rs.next()) {
      resultado = new Double(rs.getString(1));
    }

    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;

  }

  /**  comprobarIndicadoresDistrito
       * Devuelve un vector con los indicadores para los que se ha superado el umbral
   * de casos permitidos, para indicadores de un Distrito.
   * indicados por los par�metros
   * @con Conexion con la base de datos
   * @anno A�o para el que se est�n consultando los casos.
   * @semana Semana para la que se est�n consultado los casos.
   * @enfermedad C�digo de la enfermedad de la que queremos obtener los casos.
   * @cAutonoma C�digo de la Comunidad Aut�noma
   * @nivel1 C�digo de Area
   * @nivel2 C�digo de Distrito
   */
  protected Vector comprobarIndicadoresDistrito(Connection con, Integer casos,
                                                String anno,
                                                String semana,
                                                String enfermedad,
                                                String cAutonoma, String nivel1,
                                                String nivel2) throws Exception {

    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    Vector resultado = new Vector();

    /*     query = " select distinct i.CD_INDALAR, i.DS_INDALAR, j.NM_COEF "
               + " from SIVE_INDICADOR i, SIVE_IND_ANO j, SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where i.CD_INDALAR = j.CD_INDALAR "
               + " and i.IT_ACTIVO = ? and i.CD_ENFCIE = ? "
               + " and j.CD_ANOEPI = ? "
               + " and pro.CD_CA = ? "
            //   + " and muni.CD_PROV = pro.CD_PROV "
         + " and (i.CD_NIVEL_1 = ?) " // and (i.CD_NIVEL_1 = muni.CD_NIVEL_1)  "
         + " and (i.CD_NIVEL_2 = ?) " // and (i.CD_NIVEL_2 = muni.CD_NIVEL_2) "
               + " order by i.CD_INDALAR "; */
    query = " select distinct i.CD_INDALAR, i.DS_INDALAR, j.NM_COEF "
        + " from SIVE_INDICADOR i, SIVE_IND_ANO j, SIVE_PROVINCIA pro "
        + " where i.CD_INDALAR = j.CD_INDALAR "
        + " and i.IT_ACTIVO = ? and i.CD_ENFCIE = ? "
        + " and j.CD_ANOEPI = ? "
        + " and pro.CD_CA = ? "
        + " and (i.CD_NIVEL_1 = ?) "
        + " and (i.CD_NIVEL_2 = ?) "
        + " order by i.CD_INDALAR ";

    st = con.prepareStatement(query);

    st.setString(1, "S");
    st.setString(2, enfermedad);
    st.setString(3, anno);
    st.setString(4, cAutonoma);
    st.setString(5, nivel1);
    st.setString(6, nivel2);

    rs = st.executeQuery();

    String cdIndicador = "";
    String dsIndicador = "";
    Double coeficiente = null;
    Double valorAlarma = null;
    Double umbral = null;
    while (rs.next()) {
      cdIndicador = rs.getString(1);
      dsIndicador = rs.getString(2);
      //la descripcion puede ser nula
      if (dsIndicador == null) {
        dsIndicador = "";
      }
      coeficiente = new Double(rs.getString(3));

      valorAlarma = obtenerValorAlarma(con, cdIndicador, anno, semana);

      if (valorAlarma != null) {
        umbral = new Double(coeficiente.doubleValue() * valorAlarma.doubleValue());

        if (casos.doubleValue() > umbral.doubleValue()) {
          resultado.addElement(
              new datosNotifAlar(cdIndicador,
                                 dsIndicador,
                                 coeficiente.doubleValue(),
                                 valorAlarma.doubleValue(),
                                 umbral.doubleValue()));
        }
      }

    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;
  }

  /**  comprobarIndicadoresArea
       * Devuelve un vector con los indicadores para los que se ha superado el umbral
   * de casos permitidos, para indicadores de un Area.
   * indicados por los par�metros
   * @con Conexion con la base de datos
   * @anno A�o para el que se est�n consultando los casos.
   * @semana Semana para la que se est�n consultado los casos.
   * @enfermedad C�digo de la enfermedad de la que queremos obtener los casos.
   * @cAutonoma C�digo de la Comunidad Aut�noma
   * @nivel1 C�digo de Area
   */
  protected Vector comprobarIndicadoresArea(Connection con, Integer casos,
                                            String anno,
                                            String semana, String enfermedad,
                                            String cAutonoma, String nivel1) throws
      Exception {

    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    Vector resultado = new Vector();

    /*     query = " select distinct i.CD_INDALAR, i.DS_INDALAR, j.NM_COEF "
               + " from SIVE_INDICADOR i, SIVE_IND_ANO j, SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where i.CD_INDALAR = j.CD_INDALAR "
               + " and i.IT_ACTIVO = ? and i.CD_ENFCIE = ? "
               + " and j.CD_ANOEPI = ? "
               + " and pro.CD_CA = ? "
              // + " and muni.CD_PROV = pro.CD_PROV "
         + " and (i.CD_NIVEL_1 = ?) " // and (i.CD_NIVEL_1 = muni.CD_NIVEL_1)  "
               + " and (i.CD_NIVEL_2 is null) "
               + " order by i.CD_INDALAR ";*/

    query = " select distinct i.CD_INDALAR, i.DS_INDALAR, j.NM_COEF "
        + " from SIVE_INDICADOR i, SIVE_IND_ANO j, SIVE_PROVINCIA pro "
        + " where i.CD_INDALAR = j.CD_INDALAR "
        + " and i.IT_ACTIVO = ? and i.CD_ENFCIE = ? "
        + " and j.CD_ANOEPI = ? "
        + " and pro.CD_CA = ? "
        + " and (i.CD_NIVEL_1 = ?) "
        + " and (i.CD_NIVEL_2 is null) "
        + " order by i.CD_INDALAR ";

    st = con.prepareStatement(query);

    st.setString(1, "S");
    st.setString(2, enfermedad);
    st.setString(3, anno);
    st.setString(4, cAutonoma);
    st.setString(5, nivel1);

    rs = st.executeQuery();

    String cdIndicador = "";
    String dsIndicador = "";
    Double coeficiente = null;
    Double valorAlarma = null;
    Double umbral = null;
    while (rs.next()) {
      cdIndicador = rs.getString(1);
      dsIndicador = rs.getString(2);
      //la descripcion puede ser nula
      if (dsIndicador == null) {
        dsIndicador = "";
      }
      coeficiente = new Double(rs.getString(3));

      valorAlarma = obtenerValorAlarma(con, cdIndicador, anno, semana);

      if (valorAlarma != null) {
        umbral = new Double(coeficiente.doubleValue() * valorAlarma.doubleValue());

        if (casos.doubleValue() > umbral.doubleValue()) {
          resultado.addElement(
              new datosNotifAlar(cdIndicador,
                                 dsIndicador,
                                 coeficiente.doubleValue(),
                                 valorAlarma.doubleValue(),
                                 umbral.doubleValue()));
        }
      }

    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;
  }

  /**  comprobarIndicadoresCA
       * Devuelve un vector con los indicadores para los que se ha superado el umbral
   * de casos permitidos, para indicadores de una Comunidad Aut�noma.
   * indicados por los par�metros
   * @con Conexion con la base de datos
   * @anno A�o para el que se est�n consultando los casos.
   * @semana Semana para la que se est�n consultado los casos.
   * @enfermedad C�digo de la enfermedad de la que queremos obtener los casos.
   * @cAutonoma C�digo de la Comunidad Aut�noma
   */
  protected Vector comprobarIndicadoresCA(Connection con, Integer casos,
                                          String anno,
                                          String semana, String enfermedad,
                                          String cAutonoma) throws Exception {

    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    Vector resultado = new Vector();

    query = " select distinct i.CD_INDALAR, i.DS_INDALAR, j.NM_COEF "
        + " from SIVE_INDICADOR i, SIVE_IND_ANO j "
        + " where i.CD_INDALAR = j.CD_INDALAR "
        + " and i.IT_ACTIVO = ? and i.CD_ENFCIE = ? "
        + " and j.CD_ANOEPI = ? "
        + " and (i.CD_INDALAR like('CC--%')) "
        + " and (i.CD_NIVEL_1 is null) "
        + " and (i.CD_NIVEL_2 is null) "
        + " order by i.CD_INDALAR ";
    st = con.prepareStatement(query);

    st.setString(1, "S");
    st.setString(2, enfermedad);
    st.setString(3, anno);

    rs = st.executeQuery();

    String cdIndicador = "";
    String dsIndicador = "";
    Double coeficiente = null;
    Double valorAlarma = null;
    Double umbral = null;
    while (rs.next()) {
      cdIndicador = rs.getString(1);
      dsIndicador = rs.getString(2);
      //la descripcion puede ser nula
      if (dsIndicador == null) {
        dsIndicador = "";
      }
      coeficiente = new Double(rs.getString(3));

      valorAlarma = obtenerValorAlarma(con, cdIndicador, anno, semana);

      if (valorAlarma != null) {
        umbral = new Double(coeficiente.doubleValue() * valorAlarma.doubleValue());

        if (casos.doubleValue() > umbral.doubleValue()) {
          resultado.addElement(
              new datosNotifAlar(cdIndicador,
                                 dsIndicador,
                                 coeficiente.doubleValue(),
                                 valorAlarma.doubleValue(),
                                 umbral.doubleValue()));
        }
      }

    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;
  }

  /**  obtenerCabecera
   * Devuelve un vector con las descripciones de la enfermedad, �rea y distrito
   * que se le pasan como par�metros.
   * indicados por los par�metros
   * @con Conexion con la base de datos
   * @enfermedad C�digo de la enfermedad de la que queremos obtener los casos.
   * @nivel1 C�digo de Area
   * @nivel2 C�digo de Distrito
   */
  protected Vector obtenerCabecera(Connection con, CLista param,
                                   String enfermedad,
                                   String nivel1, String nivel2) throws
      Exception {
    // Recupero los datos necesarios para la cabecera

    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    String sDesPro = "";
    String sDesProL = "";
    String sDesNiv1 = "";
    String sDesNiv1L = "";
    String sDesNiv2 = "";
    String sDesNiv2L = "";

    Vector resultado = new Vector();

    query = " select DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS "
        + " where CD_ENFCIE = ? ";
    st = con.prepareStatement(query);

    st.setString(1, enfermedad);

    rs = st.executeQuery();
    if (rs.next()) {
      sDesPro = rs.getString("DS_PROCESO");
      sDesProL = rs.getString("DSL_PROCESO");
      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesProL != null)) {
        resultado.addElement(sDesProL);
      }
      else {
        resultado.addElement(sDesPro);
      }

    }
    else {
      resultado.addElement("");
    }

    rs.close();
    rs = null;
    st.close();
    st = null;

    if (!nivel1.equals("")) {
      query = " select DS_NIVEL_1,DSL_NIVEL_1 from SIVE_NIVEL1_S "
          + " where CD_NIVEL_1 = ? ";
      st = con.prepareStatement(query);

      st.setString(1, nivel1);

      rs = st.executeQuery();

      if (rs.next()) {
        sDesNiv1 = rs.getString("DS_NIVEL_1");
        sDesNiv1L = rs.getString("DSL_NIVEL_1");

        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesNiv1L != null)) {
          resultado.addElement(sDesNiv1L);
        }
        else {
          resultado.addElement(sDesNiv1);
        }
      }

      else {
        resultado.addElement("");
      }

      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    else {
      resultado.addElement("");
    }

    if (!nivel2.equals("")) {
      query = " select DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S "
          + " where CD_NIVEL_2 = ? ";
      st = con.prepareStatement(query);

      st.setString(1, nivel2);

      rs = st.executeQuery();
      if (rs.next()) {
        sDesNiv2 = rs.getString("DS_NIVEL_2");
        sDesNiv2L = rs.getString("DSL_NIVEL_2");

        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesNiv2L != null)) {
          resultado.addElement(sDesNiv2L);
        }
        else {
          resultado.addElement(sDesNiv2);
        }
      }
      else {
        resultado.addElement("");
      }

      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    else {
      resultado.addElement("");
    }

    return resultado;
  }

  protected Vector comprobarIndicadoresCN(Connection con, Integer casos,
                                          String anno,
                                          String semana, String enfermedad) throws
      Exception {

    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    Vector resultado = new Vector();

    query = " select distinct i.CD_INDALAR, i.DS_INDALAR, j.NM_COEF "
        + " from SIVE_INDICADOR i, SIVE_IND_ANO j "
        + " where i.CD_INDALAR = j.CD_INDALAR "
        + " and i.IT_ACTIVO = ? and i.CD_ENFCIE = ? "
        + " and j.CD_ANOEPI = ? "
        + " and (i.CD_INDALAR not like('CC--%')) "
        + " and (i.CD_NIVEL_1 is null) "
        + " and (i.CD_NIVEL_2 is null) "
        + " order by i.CD_INDALAR ";
    st = con.prepareStatement(query);

    st.setString(1, "S");
    st.setString(2, enfermedad);
    st.setString(3, anno);

    rs = st.executeQuery();

    String cdIndicador = "";
    String dsIndicador = "";
    Double coeficiente = null;
    Double valorAlarma = null;
    Double umbral = null;
    while (rs.next()) {
      cdIndicador = rs.getString(1);
      dsIndicador = rs.getString(2);
      //la descripcion puede ser nula
      if (dsIndicador == null) {
        dsIndicador = "";
      }
      coeficiente = new Double(rs.getString(3));

      valorAlarma = obtenerValorAlarma(con, cdIndicador, anno, semana);

      if (valorAlarma != null) {
        umbral = new Double(coeficiente.doubleValue() * valorAlarma.doubleValue());

        if (casos.doubleValue() > umbral.doubleValue()) {
          resultado.addElement(
              new datosNotifAlar(cdIndicador,
                                 dsIndicador,
                                 coeficiente.doubleValue(),
                                 valorAlarma.doubleValue(),
                                 umbral.doubleValue()));
        }
      }

    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;
  }

  /*    //AIC
    //// System_out.println("SrvIndSupNot : entrando en el servlet");
    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int iEstado;
    int j=0;
    Vector resConsulta = new Vector();
    int totalCasos = 0;
    Integer casosEnf = null;
    String sCondPerfil = "";
    //Descripciones
    String sDesPro="";
    String sDesProL="";
    String sDesNiv1="";
    String sDesNiv1L="";
    String sDesNiv2="";
    String sDesNiv2L="";
    // objetos de datos
    CLista data = new CLista();
    datosNotifAlar ind = null;
    parNotifAlar parCons = null;
    //auxiliares
    Vector codInd = new Vector();
    Vector infInd = new Vector();
    Vector datCab = new Vector();
    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);
    parCons = (parNotifAlar) param.firstElement();
    //aic
    switch (param.getPerfil()) {
      case 1:
      case 2: // ccaa
            //aic
            //sCondPerfil =  " and (edo.CD_NIVEL_1 = muni.CD_NIVEL_1)"
            //             + " and (edo.CD_NIVEL_2 = muni.CD_NIVEL_2)";
            //sCondPerfil = sCondPerfil;
            break;
      case 3: // niv 1
            sCondPerfil = //sCondPerfil +
                    " and edo.CD_NIVEL_1 = ? ";
            break;
      case 4: // niv 2
            sCondPerfil = //sCondPerfil +
                    " and edo.CD_NIVEL_1 = ? " +
                    " and edo.CD_NIVEL_2 = ? ";
            break;
    }
   // sCondPerfil = "and CD_NIVEL_1 = ? " +
   //                 "and CD_NIVEL_2 = ? ";
   //
            //aic
            //*System_out.print("SrvIndSupNot : ");
     //        System_out.print(sCondPerfil);
     //        System_out.print(" ");
     //        System_out.println(opmode);
     // modos de operaci�n
     switch (opmode) {
       //Consulta de los indicadores superados
       //cuando se realiza una notificacion
       case servletIND_SUP:
         try {
         //Consulta para obtener los casos existentes
         //de la enfermedad
        //AIC
        query = "select count(NM_EDO) from SIVE_EDOIND edo "
                 + " where CD_ANOEPI = ? and CD_SEMEPI = ? "
                 + " and CD_ENFCIE = ?  and CD_NIVEL_1 in(select distinct "
                 + " CD_NIVEL_1 FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
                 + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV) and "
                 + " CD_NIVEL_2 in (select distinct CD_NIVEL_2 "
                 + " FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
                 + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV) "
                 + " and CD_PROV in (select distinct "
        + " muni.CD_PROV FROM SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
                 + " where pro.CD_CA = ? AND muni.CD_PROV = pro.CD_PROV)"
                 + sCondPerfil;
         //query = " select count(NM_EDO)"
         //      + " from SIVE_EDOIND edo, SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
         //      + " where CD_ANOEPI = ? and CD_SEMEPI = ? and CD_ENFCIE = ? "
         //      + " and pro.CD_CA = ? "
         //      + " and muni.CD_PROV = pro.CD_PROV "
         //      and edo.CD_NIVEL_1 = muni.CD_NIVEL_1 "
         //      + " and edo.CD_NIVEL_2 = muni.CD_NIVEL_2 "
         //      + sCondPerfil;
         //aic
         // System_out.println("SrvIndSupNot : " + query + "  " + parCons.cAutonoma);
         st = con.prepareStatement(query);
         st.setString(1, parCons.ano());
         st.setString(2, parCons.semana());
         st.setString(3, parCons.enfermedad());
         st.setString(4, parCons.cAutonoma());
         st.setString(5, parCons.cAutonoma());
         st.setString(6, parCons.cAutonoma());
         switch (param.getPerfil()) {
           case 1:
           case 2: // ccaa
             //aic
             //st.setNull(4, Types.VARCHAR);
             //st.setNull(5, Types.VARCHAR);
             break;
           case 3: // niv 1
             st.setString(7, parCons.nivel1());
             //aic
             //st.setNull(5, Types.VARCHAR);
             break;
           case 4: // niv 2
             st.setString(7, parCons.nivel1());
             st.setString(8, parCons.nivel2());
             break;
         }
         rs = st.executeQuery();
                     //aic
             //// System_out.print("SrvIndSupNot Primera Q :");
             //// System_out.print(query);
             //// System_out.print("  ");
             //// System_out.print(parCons.ano());
             //// System_out.print(" ");
             //// System_out.print(parCons.semana());
             //// System_out.print(param.getPerfil());
             //// System_out.print("  ");
             //// System_out.print(parCons.nivel2());
             //// System_out.print("  ");
             //// System_out.println(parCons.enfermedad());
         if (rs.next()){
           casosEnf = new Integer (rs.getInt(1));
         }
         rs.close();
         rs = null;
         st.close();
         st = null;
         //aic
         sCondPerfil = "" ;
         switch (param.getPerfil())
         {
           case 1:
           case 2: // ccaa
                 //aic
                 //sCondPerfil = "and CD_NIVEL_1 is null ";
                 //sCondPerfil = sCondPerfil;
        sCondPerfil =  " and (i.CD_NIVEL_1 = muni.CD_NIVEL_1 or i.CD_NIVEL_1 is null)"
        + " and (i.CD_NIVEL_2 = muni.CD_NIVEL_2 or i.CD_NIVEL_2 is null)";
                 break;
           case 3: // niv 1
        sCondPerfil = " and (i.CD_NIVEL_1 = ?) and (i.CD_NIVEL_1 = muni.CD_NIVEL_1) ";
                 break;
           case 4: // niv 2
        sCondPerfil = " and (i.CD_NIVEL_1 = ?) and (i.CD_NIVEL_1 = muni.CD_NIVEL_1)  " +
        " and (i.CD_NIVEL_2 = ?) and (i.CD_NIVEL_2 = muni.CD_NIVEL_2) ";
                 break;
         }
         // Consulta para seleccionar los indicadores
         // que se han podido superar
         query = " select distinct i.CD_INDALAR, i.DS_INDALAR, j.NM_COEF "
               + " from SIVE_INDICADOR i, SIVE_IND_ANO j, SIVE_MUNICIPIO muni, SIVE_PROVINCIA pro "
               + " where i.CD_INDALAR = j.CD_INDALAR "
               + " and i.IT_ACTIVO = ? and i.CD_ENFCIE = ? "
               + " and j.CD_ANOEPI = ? "
               + " and pro.CD_CA = ? "
               + " and muni.CD_PROV = pro.CD_PROV "
               + sCondPerfil
               + " order by i.CD_INDALAR ";
         st = con.prepareStatement(query);
         //aic
         // System_out.println("SrvIndSupNot : " + query + "  " + parCons.cAutonoma);
         st.setString(1, "S");
         st.setString(2, parCons.enfermedad());
         st.setString(3, parCons.ano());
         st.setString(4, parCons.cAutonoma());
         switch (param.getPerfil()) {
           case 1:
           case 2: // ccaa
             //aic
             //st.setNull(4, Types.VARCHAR);
             //st.setNull(5, Types.VARCHAR);
             break;
           case 3: // niv 1
             st.setString(5, parCons.nivel1());
             //st.setNull(5, Types.VARCHAR);
             break;
           case 4: // niv 2
             st.setString(5, parCons.nivel1());
             st.setString(6, parCons.nivel2());
             //st.setNull(5, Types.VARCHAR);
             //st.setString(6, parCons.nivel2());
             //st.setNull(7, Types.VARCHAR);
             break;
         }
         rs = st.executeQuery();
         String cd = "";
         String ds = "";
         Double coef = null;
         int n = 0;
         Vector aux = new Vector();
         while (rs.next()){
           cd = rs.getString(1);
           ds = rs.getString(2);
           //la descripcion puede ser nula
           if (ds==null)
             ds = "";
           coef = new Double (rs.getString(3));
           //codInd.addElement(cd);
           codInd.insertElementAt(cd, n);
           //n = codInd.indexOf(cd);
           aux.addElement(ds);
           aux.addElement(coef);
           infInd.insertElementAt(aux, n);
           aux = new Vector();
           n++;
           //aic
           //// System_out.println("SrvIndSupNot : obtiene indicadores");
         }
         rs.close();
         rs = null;
         st.close();
         st = null;
         // Consulta para comparar los valores
         for (int s=0; s < codInd.size(); s++){
           query = " select NM_VALOR "
                 + " from SIVE_ALARMA "
        + " where CD_INDALAR = ? and CD_ANOEPI = ? and CD_SEMEPI = ? ";
           st = con.prepareStatement(query);
           st.setString(1, (String) codInd.elementAt(s));
           st.setString(2, parCons.ano());
           st.setString(3, parCons.semana());
           rs = st.executeQuery();
           Double valor = null;
           Double umbral = null;
           Double cf = null;
           Vector inf = new Vector();
           //AIC
           //// System_out.println("SrvIndSupNot : entrando en los resultados");
           if (rs.next()){
             valor = new Double (rs.getString(1));
             inf = (Vector) infInd.elementAt(s);
             cf = (Double) inf.elementAt(1);
             umbral = new Double (cf.doubleValue()*valor.doubleValue());
             //aic
             //// System_out.print("SrvIndSupNot :");
             //// System_out.print(casosEnf.doubleValue());
             //// System_out.print(" > ");
             //// System_out.println(umbral.doubleValue());
             if (casosEnf.doubleValue()>umbral.doubleValue()){
               resConsulta.addElement(
                   new datosNotifAlar ((String) codInd.elementAt(s),
                                       (String) inf.elementAt(0),
                                       cf.doubleValue(),
                                       valor.doubleValue(),
                                       umbral.doubleValue()));
             }
             valor = null;
             umbral = null;
             coef = null;
             inf = new Vector();
           }
           rs.close();
           rs = null;
           st.close();
           st = null;
         }// for para comparar valores
         // Recupero los datos necesarios para la cabecera
         query = " select DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS "
               + " where CD_ENFCIE = ? ";
         st = con.prepareStatement(query);
         st.setString(1, parCons.enfermedad());
         rs = st.executeQuery();
         if (rs.next()){
           sDesPro= rs.getString("DS_PROCESO");
           sDesProL= rs.getString("DSL_PROCESO");
           if ((param.getIdioma() != CApp.idiomaPORDEFECTO)
                 && (sDesProL != null))
             datCab.addElement(sDesProL);
           else
             datCab.addElement(sDesPro);
         }
         else{
           datCab.addElement("");
         }
         rs.close();
         rs = null;
         st.close();
         st = null;
         if (!parCons.nivel1().equals("")){
           query = " select DS_NIVEL_1,DSL_NIVEL_1 from SIVE_NIVEL1_S "
               + " where CD_NIVEL_1 = ? ";
           st = con.prepareStatement(query);
           st.setString(1, parCons.nivel1());
           rs = st.executeQuery();
           if (rs.next()){
             sDesNiv1= rs.getString("DS_NIVEL_1");
             sDesNiv1L= rs.getString("DSL_NIVEL_1");
             if ((param.getIdioma() != CApp.idiomaPORDEFECTO)
                 && (sDesNiv1L != null))
               datCab.addElement(sDesNiv1L);
             else
               datCab.addElement(sDesNiv1);
           }
           else{
             datCab.addElement("");
           }
           rs.close();
           rs = null;
           st.close();
           st = null;
         } else {
           datCab.addElement("");
         }
         if (!parCons.nivel2().equals("")){
           query = " select DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S "
               + " where CD_NIVEL_2 = ? ";
           st = con.prepareStatement(query);
           st.setString(1, parCons.nivel2());
           rs = st.executeQuery();
           if (rs.next()){
             sDesNiv2= rs.getString("DS_NIVEL_2");
             sDesNiv2L= rs.getString("DSL_NIVEL_2");
             if ((param.getIdioma() != CApp.idiomaPORDEFECTO)
                 && (sDesNiv2L != null))
               datCab.addElement(sDesNiv2L);
             else
               datCab.addElement(sDesNiv2);
           }
           else{
             datCab.addElement("");
           }
           rs.close();
           rs = null;
           st.close();
           st = null;
         } else {
           datCab.addElement("");
         }
         } catch (Exception e) {
           e.printStackTrace();
         }
         data.addElement(resConsulta);
         data.addElement(datCab);
         break;
     } // fin del switch para los modos de consulta
     // cierra la conexion y acaba el procedimiento doWork
     closeConnection(con);
     data.trimToSize();
     return data;
      }   */

// Para hacer pruebecillas
       /*public CLista doPrueba(int operacion, CLista parametros) throws Exception {
       jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                              "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                              "sive_desa",
                              "sive_desa");
       Connection con = null;
       con = openConnection();
       return doWork(operacion, parametros);
     }*/

}
