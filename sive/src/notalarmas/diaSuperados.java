//Title:        Notificacion de alarmas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Durante la notificacion de los casos que se han dado de una enfermedad,
//muestra los indicadores para los que se ha superado el umbral.

package notalarmas;

import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.LabelControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CTabla;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;

public class diaSuperados
    extends CDialog {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  ResourceBundle res;
  final public int modoESPERA = 1;

  // lista
  public CLista lista = new CLista();

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/aceptar.gif"};

  // indica ela fila seleccionada en la tabla
  public int m_itemSelecc = -1;
  int numCol;
  String tamCol;
  String nomCol;

  XYLayout xYLayout1 = new XYLayout();
  public CTabla tabla = new CTabla();

  //diaSuperados_tabla_actionAdapter tablaActionListener = new diaSuperados_tabla_actionAdapter(this);
  diaSuperadosItemListener multlstItemListener = new diaSuperadosItemListener(this);
  ButtonControl btnSalir = new ButtonControl();
  LabelControl labelArea = new LabelControl();
  LabelControl labelEnf = new LabelControl();
  LabelControl labelDist = new LabelControl();
  LabelControl labelSem = new LabelControl();

  // constructor
  public diaSuperados(CApp a) {
    super(a);
    res = ResourceBundle.getBundle("notalarmas.Res" + a.getIdioma());

    try {
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa el aspecto del panel
  public void jbInit() throws Exception {
    xYLayout1.setWidth(625);
    xYLayout1.setHeight(340);
    this.setLayout(xYLayout1);

    this.setSize(new Dimension(620, 370));
    this.setTitle(res.getString("msg1.Text"));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnSalir.setLabel(res.getString("btnSalir.Label"));
    labelArea.setText(res.getString("labelArea.Text"));
    labelEnf.setText(res.getString("labelEnf.Text"));
    labelDist.setText(res.getString("labelDist.Text"));
    labelSem.setText(res.getString("labelSem.Text"));
    btnSalir.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnSalir_actionPerformed(e);
      }
    });
    btnSalir.setImage(imgs.getImage(0));
    //btnSalir.addActionListener(btnNormalActionListener);

    this.add(tabla, new XYConstraints(12, 80, 600, 200));
    this.add(btnSalir, new XYConstraints(500, 300, -1, -1));
    this.add(labelArea, new XYConstraints(35, 15, -1, -1));
    this.add(labelEnf, new XYConstraints(35, 45, -1, -1));
    this.add(labelDist, new XYConstraints(323, 15, -1, -1));
    this.add(labelSem, new XYConstraints(323, 45, -1, -1));

    // configuraci�n de la tabla
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnButtonsStrings")), '\n'));
    //tabla.setBackground(Color.white);
    tabla.getList().setHighlightColor(Color.lightGray);

    tabla.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "295\n95\n95\n95"), '\n'));
    //tabla.setColumnLabelsStrings(jclass.util.JCUtilConverter.toStringList(new String("Indicador\nCoeficiente\nValor\nUmbral"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla1.ColumnButtonsStrings")), '\n'));
    tabla.setNumColumns(4);
    tabla.setAutoSelect(true);
    tabla.setColumnAlignment(1, 2);
    tabla.setColumnAlignment(2, 2);
    tabla.setColumnAlignment(3, 2);

    //tabla.addActionListener(tablaActionListener);
    tabla.addItemListener(multlstItemListener);

    Inicializar();
  }

  public void Inicializar() {
    switch (modoOperacion) {

      case modoNORMAL:

        // modo modificaci�n y baja
        btnSalir.setEnabled(true);
        tabla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnSalir.setEnabled(false);
        tabla.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // limpia la pantalla
  void vaciarPantalla() {
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);
    }
  }

  // doble click en la tabla
  // Se muestra informacion acerca de la pregunta
  //  modelo/FechAlta/FechBaja/Linea/Pregunta
  public void tabla_actionPerformed() {
  }

  void tabla_itemStateChanged(JCItemEvent evt) {

    m_itemSelecc = tabla.getSelectedIndex();

  }

  public void ponerCabecera(parNotifAlar par) {
    // antes se habran obtenido las descripciones
    // de la enfermedad, el area y el distrito
    labelArea.setText(labelArea.getText() + par.nivel1());
    labelEnf.setText(labelEnf.getText() + par.enfermedad());
    labelDist.setText(labelDist.getText() + par.nivel2());
    labelSem.setText(labelSem.getText() + par.ano() + "-" + par.semana());
  }

  // pinta la tabla
  public void rellenarTabla(Vector lista) {
    datosNotifAlar dato;
    tabla.clear();
    for (int i = 0; i < lista.size(); i++) {
      dato = (datosNotifAlar) lista.elementAt(i);
      tabla.addItem(setLinea(dato), '&');

    }
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(datosNotifAlar o) {
    String aux = o.cd_ind() + ": " + o.ds_ind();
    return new String(aux + '&' + o.coef() + '&'
                      + o.valor + '&' + o.umbral);
  }

  void btnSalir_actionPerformed(ActionEvent e) {
    this.dispose();
  }

} //fin clase

    /******************************************************************************/

//Tambi�n implementa Runnable por si usuario pulsa m�s..
class diaSuperadosItemListener
    implements JCItemListener, Runnable {
  diaSuperados adaptee = null;
  JCItemEvent e = null;

  public diaSuperadosItemListener(diaSuperados adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      this.e = e;
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

//    if (((JCMultiColumnList)e.getSource()).getName() == "tabla")
    adaptee.tabla_itemStateChanged(e);
  }
}
