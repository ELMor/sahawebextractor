package genanocent;

import java.util.ResourceBundle;

import com.borland.jbcl.layout.XYLayout;
import capp.CApp;

public class GenAnoCent
    extends CApp {
  XYLayout xYLayout1 = new XYLayout();
  ResourceBundle res;
  boolean isStandalone = false;
  Pan_GenAno tPan_Param = null;
//Get a parameter value

  public String getParameter(String key, String def) {
    return isStandalone ? System.getProperty(key, def) :
        (getParameter(key) != null ? getParameter(key) : def);
  }

  //Construct the applet

  public void start() {
    res = ResourceBundle.getBundle("genanocent.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    tPan_Param = new Pan_GenAno(a);
    tPan_Param.Inicializar();
    VerPanel(res.getString("msg1.Text"), tPan_Param, true);
  }

  private void jbInit() throws Exception {
    xYLayout1.setWidth(400);
    xYLayout1.setHeight(300);
    this.setLayout(xYLayout1);
  }

//Get Applet information

  public String getAppletInfo() {
    return "Applet Information";
  }

//Get parameter info

  public String[][] getParameterInfo() {
    return null;
  }
}
