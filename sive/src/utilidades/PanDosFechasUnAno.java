package utilidades;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.FocusEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import fechas.conversorfechas;
import obj.CFechaSimple;

public class PanDosFechasUnAno
    extends Panel {

  public static final int modoINICIO_SEMANA = 1; //Usarlo con constructor en el que no se indican fechas como par�metros
  //Este panel pone las semanas primera y actual del a�o en curso
  public static final int modoSEMANA_ACTUAL = 2; //No usarlo
  public static final int modoINI_MODIFICAR = 9; //Usarlo para crear panel indic�ndole las fechas de inicializaci�n
  // en los par�metros del constructor
  public static final int modoVACIO = -1; //Inicia si a�o,semana ni fecha
  //Al cambiar a�o no pondr� la semana

  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variables
  int modoOperacion;

  protected CLista listaFich = new CLista();
  protected CPanel pnlContenedor;
  ResourceBundle res;

  //objeto para conversi�n de num semana a fecha y viceversa
  public conversorfechas conv = null;

  //Variables y objetos para recuperar datos si cambios no tienen �xito
  //P�blicas para validarlas si es necesario
  public conversorfechas convBk = null;
  public String sAnoBk = "";
  public String sCodSemDesdeBk = "";
  public String sFecSemDesdeBk = "";
  public String sCodSemHastaBk = "";
  public String sFecSemHastaBk = "";
  Enumeration enum;
  int tipIni;
  boolean bFechaProtegida;

  //Datos del d�a de hoy
  String sAnoActual = "";
  public String fechaActual = ""; //fecha de hoy
  String sCodSemActual = "";

  //Strings dependen del idioma
  String sSEM_DESDE = "";
  String sSEM_HASTA = "";
  String sANO = "";
  String sNO_HAY_DATOS = "";
  String sESA_FECHA_NO_PERTENECE = "";
  String sESA_SEMANA_NO_PERTENECE = "";

  XYLayout xYLayout1 = new XYLayout();
  public TextField txtAno = new TextField();
  public TextField txtCodSemDesde = new TextField();
  public CFechaSimple txtFecSemDesde = new CFechaSimple("S");
  public TextField txtCodSemHasta = new TextField();
  public CFechaSimple txtFecSemHasta = new CFechaSimple("S");

  Label lblAno = new Label();
  Label lblSemDesde = new Label();
  Label lblSemHasta = new Label();
  protected String sConsulta = null;

  public PanDosFechasUnAno(CPanel pnl, int iModo, boolean bFechaProt) {
    //Si iModo es 1 pone inicialmente fecha de fin de primera sem epid. del a�o actual
    //Si iModo tiene otro valor se pone inicialmente la fecha de hoy
    //Si bFechaProt es true la caja de fecha no la puede tocar el usuario
    try {
      pnlContenedor = pnl;
      res = ResourceBundle.getBundle("utilidades.Res" + pnl.getApp().getIdioma());
      sSEM_DESDE = res.getString("lblSemDesde.Text");
      sSEM_HASTA = res.getString("lblSemHasta.Text");
      sANO = res.getString("lblAno.Text");
      sNO_HAY_DATOS = res.getString("msg.1");
      sESA_FECHA_NO_PERTENECE = res.getString("msg.2");
      sESA_SEMANA_NO_PERTENECE = res.getString("msg.3");

      //DSR: Detecci�n del par�metro IT_FCCONS; No inicializa los controles de
      //las fechas si el par�metro es N
      sConsulta = pnlContenedor.getApp().getParameter("IT_FCCONS", "");
      if (sConsulta.equals("N")) {
        tipIni = modoVACIO;
      }
      else {
        tipIni = iModo;
      }

      bFechaProtegida = bFechaProt;
      jbInit();

      // JRM
      if (txtCodSemDesde.getText().trim() != "") {
        txtCodSemDesde.setText(rellenoCeros(txtCodSemDesde.getText(), 2));
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

//_____________  Constructor usado para modificar datos. Parte con datos iniciales___

  public PanDosFechasUnAno(CPanel pnl, boolean bFechaProt, String ano,
                           String codSemDesde) {

    //Pone inicialmente el a�o y cod semana indicados

    try {
      pnlContenedor = pnl;

      res = ResourceBundle.getBundle("utilidades.Res" + pnl.getApp().getIdioma());
      sSEM_DESDE = res.getString("lblSemDesde.Text");
      sSEM_HASTA = res.getString("lblSemHasta.Text");
      sANO = res.getString("lblAno.Text");
      sNO_HAY_DATOS = res.getString("msg.1");
      sESA_FECHA_NO_PERTENECE = res.getString("msg.2");
      sESA_SEMANA_NO_PERTENECE = res.getString("msg.3");

      tipIni = modoINI_MODIFICAR; //Constante indica forma de inicializaci�n
      bFechaProtegida = bFechaProt;
      jbInit(); //No inicializar� las fechas
      //Se inicializan cajas de texto y conversor
      txtAno.setText(ano);
      sAnoBk = ano;
      conv = new conversorfechas(ano, pnlContenedor.getApp());

      txtCodSemDesde.setText(codSemDesde);

      sCodSemDesdeBk = codSemDesde;
      //Se va a por la fecha de esa semana
      txtCodSemDesde_focusLost(null);

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

//_______________________________________________________________________

  void jbInit() throws Exception {

    this.setSize(new Dimension(390, 30));
    xYLayout1.setHeight(61);
    xYLayout1.setWidth(434);

    txtFecSemDesde.addFocusListener(new PanFechas_txtFecSemDesde_focusAdapter(this));
    txtCodSemDesde.addFocusListener(new PanFechas_txtCodSemDesde_focusAdapter(this));
    txtFecSemHasta.addFocusListener(new PanFechas_txtFecSemHasta_focusAdapter(this));
    txtCodSemHasta.addFocusListener(new PanFechas_txtCodSemHasta_focusAdapter(this));

    txtAno.addFocusListener(new PanDosFechasUnAno_txtAno_focusAdapter(this));

    lblSemDesde.setText(sSEM_DESDE);
    lblSemHasta.setText(sSEM_HASTA);
    lblAno.setText(sANO);
    this.setLayout(xYLayout1);

    this.add(lblAno, new XYConstraints(3, 2, 34, -1));
    this.add(txtAno, new XYConstraints(36, 2, 77, -1));
    this.add(lblSemDesde, new XYConstraints(143, 2, 102, -1));
    this.add(txtCodSemDesde, new XYConstraints(249, 2, 43, -1));
    this.add(txtFecSemDesde, new XYConstraints(312, 2, 97, -1));
    this.add(txtCodSemHasta, new XYConstraints(249, 36, 43, -1));
    this.add(txtFecSemHasta, new XYConstraints(312, 36, 97, -1));
    this.add(lblSemHasta, new XYConstraints(143, 36, 97, -1));

    txtAno.setForeground(Color.black);
    txtAno.setBackground(new Color(255, 255, 150));

    txtCodSemDesde.setForeground(Color.black);
    txtCodSemDesde.setBackground(new Color(255, 255, 150));
    txtCodSemHasta.setForeground(Color.black);
    txtCodSemHasta.setBackground(new Color(255, 255, 150));

    if (bFechaProtegida) {
      txtFecSemDesde.setEditable(false);
      txtFecSemHasta.setEditable(false);
    }
    else {
      txtFecSemDesde.setForeground(Color.black);
      txtFecSemDesde.setBackground(new Color(255, 255, 150));
      txtFecSemHasta.setForeground(Color.black);
      txtFecSemHasta.setBackground(new Color(255, 255, 150));

    }

    //Inicialza fechas si no estamos en modo MODIFICAR
    if ( (tipIni != modoINI_MODIFICAR) && (tipIni != modoVACIO)) {
      inicializarFechas();

//    inicializarFechas();

    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtAno.setEnabled(true);
        txtCodSemDesde.setEnabled(true);
        txtCodSemHasta.setEnabled(true);
        if (!bFechaProtegida) {
          txtFecSemDesde.setEnabled(true);
          txtFecSemHasta.setEnabled(true);
        }
        pnlContenedor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        pnlContenedor.setEnabled(true);
        break;

      case modoESPERA:
        txtAno.setEnabled(false);
        txtCodSemDesde.setEnabled(false);
        txtCodSemHasta.setEnabled(false);
        if (!bFechaProtegida) {
          txtFecSemDesde.setEnabled(false);
          txtCodSemHasta.setEnabled(false);
        }
        pnlContenedor.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pnlContenedor.setEnabled(false);
        break;
    }
  }

  void inicializarFechas() {

    String sFecActual;
    java.util.Date fecActual = null;
    String sFecIni;
    String sAnoHasta;
    int resul;
    CMessage msgBox;

    try {

      //Obtiene la fecha actual
      fecActual = new java.util.Date();

      //Pasa la fecha actual de Date a String
      SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
      sFecActual = formater.format(fecActual);
      //Se almacena tambi�n en var. global para posible recuperacion
      fechaActual = sFecActual;
      txtFecSemHasta.setText(sFecActual.trim());

      //Si la fecha no ha cambiado no hace nada
      if (txtFecSemHasta.getText().equals(sFecSemHastaBk)) {
        return;
      }

      //valida el dato
      txtFecSemHasta.ValidarFecha();
      //Si no es v�lido recupera el antiguo
      if (txtFecSemHasta.getValid().equals("N")) {
        txtFecSemHasta.setText(sFecSemHastaBk);
        return;
      }
      //Si es v�lido le da el formato adecuado
      else if (txtFecSemHasta.getValid().equals("S")) {
        txtFecSemHasta.setText(txtFecSemHasta.getFecha());
      }

      this.modoOperacion = modoESPERA;
      Inicializar();

      sFecIni = txtFecSemHasta.getText();
      conv = conversorfechas.getConvDeFecha(sFecIni, pnlContenedor.getApp());

      this.modoOperacion = modoNORMAL;
      Inicializar();

      // si a�o no es v�lido recupera todos los datos y muestra mensaje
      if (conv == null) {
        conv = convBk;
        txtAno.setText(sAnoBk);
        txtCodSemHasta.setText(sCodSemHastaBk);
        txtFecSemHasta.setText(sFecSemHastaBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              sNO_HAY_DATOS);
        msgBox.show();
        msgBox = null;
      }
      // si es v�lido rellena campo de a�o y guarda datos del a�o para posible recuperaci�n
      //NO se vac�an las cajas de semana
      else {
        sAnoActual = conv.getAno();
        convBk = conv;
        sAnoHasta = conv.getAno();
        txtAno.setText(sAnoHasta);
        sAnoBk = sAnoHasta;

      }
      //si no hay un a�o v�lido todav�a no hace nada y sale
      if (sAnoBk == "") {
        return;
      }

      //Si tipo de inicial. es 1
      //Va a por la semana y fecha de semDesde:
      // pone primer dia y semana del a�o
      if (tipIni == 1) {
        txtCodSemDesde.setText("1");
        sCodSemDesdeBk = txtCodSemDesde.getText();
        txtFecSemDesde.setText(conv.getFecFinPriSem());
        sFecSemDesdeBk = txtFecSemDesde.getText();
      }

      /// Va a por la semana y fecha de semHasta
      //Se pone la fecha de hoy . Solo falta traer c�digo de semana

      //indeptemente de si he traido a�o o sigue el que habia, busca semana:
      resul = conv.getNumSem(txtFecSemHasta.getText());

      //Si esa semana no es v�lida recupera los datos anteriores de semana y muestra un mensaje
      if (resul == -1) {
        txtCodSemHasta.setText(sCodSemHastaBk);
        txtFecSemHasta.setText(sFecSemHastaBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              sESA_FECHA_NO_PERTENECE);
        msgBox.show();
        msgBox = null;
      }
      else {
        txtCodSemHasta.setText(Integer.toString(resul));
        sCodSemHastaBk = txtCodSemHasta.getText();
        sFecSemHastaBk = txtFecSemHasta.getText();
        //Guarda el cod de sem actual (ahora) para posible recuperacion
        sCodSemActual = txtCodSemHasta.getText();
      }

      // envia la excepci�n
    }
    catch (Exception e) {
      //#// System_out.println(e.getMessage() );
      e.printStackTrace();
    }

  }

  void txtCodSemDesde_focusLost(FocusEvent evt) {

//#// System_out.println("evento en codSemDesde");//*******************
     int numero;
    String resul;
    CMessage msgBox;
    try {

      txtCodSemDesde.setText(txtCodSemDesde.getText().trim());

      //si no ha cambiado el valor de la caja de texto no hace nada
      if (txtCodSemDesde.getText().equals(sCodSemDesdeBk)) {
        txtCodSemDesde.setText(rellenoCeros(txtCodSemDesde.getText(), 2));
        return;
      }

      //chequea cod semana.Debe ser entero entre 1 y 53
      //Si no es ocrrecto o no hay a�o v�lido recupera el valor antiguo y ya est�
      if (
          (sAnoBk == "") || (ChequearEntero(txtCodSemDesde.getText(), 0, 54, 2) == false)
          ) {
        txtCodSemDesde.setText(rellenoCeros(sCodSemDesdeBk, 2));
        return;
      }

      //Si hay a�o valido
      numero = Integer.parseInt(txtCodSemDesde.getText());
      resul = conv.getFec(numero);
      //Si no es num semana v�lido recupera los datos anteriores y muestra mensaje
      if (resul.equals("")) {
        txtCodSemDesde.setText(rellenoCeros(sCodSemDesdeBk, 2));
        txtFecSemDesde.setText(sFecSemDesdeBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              sESA_SEMANA_NO_PERTENECE);
        msgBox.show();
        msgBox = null;

      }
      //si es semana v�lida actualiza la fecha de fin de sem y guarda los datos de reserva
      else {
        txtFecSemDesde.setText(resul);
        sCodSemDesdeBk = txtCodSemDesde.getText();
        txtCodSemDesde.setText(rellenoCeros(txtCodSemDesde.getText(), 2));
        sFecSemDesdeBk = resul;

      }

      // envia la excepci�n
    }
    catch (Exception e) {
      //#// System_out.println(e.getMessage() );
      e.printStackTrace();

    }
    //#// System_out.println(txtFecSemDesde.getText());//*******************
  }

  void txtFecSemDesde_focusLost(FocusEvent evt) {
//#// System_out.println("evento lostfocus en FecSemDesde");//*******************
//#// System_out.println("valor SCodSemDesdeBk *********" + sCodSemDesdeBk);//*******************

      java.util.Date dFecha;
    Calendar calen = new GregorianCalendar();
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    int ano;

    String sFecIni;
    String sAnoDesde; //A�o que se obttiene a partir de fecha indicada por usuario
    int resul;
    CMessage msgBox;

    try {

      txtFecSemDesde.setText(txtFecSemDesde.getText().trim());

      //Si la fecha no ha cambiado no hace nada
      if (txtFecSemDesde.getText().equals(sFecSemDesdeBk)) {
        return;
      }

      //valida el dato
      txtFecSemDesde.ValidarFecha();
      //Si no es v�lido recupera el antiguo
      if (txtFecSemDesde.getValid().equals("N")) {
        txtFecSemDesde.setText(sFecSemDesdeBk);
        return;
      }
      //Si es v�lido le da el formato adecuado
      else if (txtFecSemDesde.getValid().equals("S")) {
        txtFecSemDesde.setText(txtFecSemDesde.getFecha());
      }

      //Pasa el string de fecha a Date
      dFecha = formater.parse(txtFecSemDesde.getText());
      //obtiene el a�o cronol�gico correspondiente a esa fecha
      calen.setTime(dFecha);
      ano = calen.get(Calendar.YEAR);

      // trae fechas del a�o epidemiol�gico de b.datos si es necesario
      if (
          //Si el a�o cronol�gico ha cambiado en fecha
          (! (Integer.toString(ano).equals(sAnoBk))) ||
          //o fecha es de �ltimos de diciembre
          ( (calen.get(Calendar.MONTH) == Calendar.DECEMBER) &&
           (calen.get(Calendar.DAY_OF_MONTH) > 23)) ||
          //o fecha es de principios de enero
          ( (calen.get(Calendar.MONTH) == Calendar.JANUARY) &&
           (calen.get(Calendar.DAY_OF_MONTH) < 9))
          ) {

        //Se va a por el a�o epidemiol�gico  (podr� coincidir con el cronol�gico o no )

        this.modoOperacion = modoESPERA;
        Inicializar();

        sFecIni = txtFecSemDesde.getText();
        conv = conversorfechas.getConvDeFecha(sFecIni, pnlContenedor.getApp());

        this.modoOperacion = modoNORMAL;
        Inicializar();

        // si a�o no es v�lido recupera todos los datos y muestra mensaje
        if (conv == null) {
          conv = convBk;
          txtAno.setText(sAnoBk);
          txtCodSemDesde.setText(sCodSemDesdeBk);
          txtFecSemDesde.setText(sFecSemDesdeBk);
          msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                                sNO_HAY_DATOS);
          msgBox.show();
          msgBox = null;
        }
        // si es v�lido rellena campo de a�o y guarda datos  para posible recuperaci�n
        //NO se vac�an las cajas de semana
        else {
          convBk = conv;
          sAnoDesde = conv.getAno();
          txtAno.setText(sAnoDesde);
          sAnoBk = sAnoDesde;
        }
      }

//#// System_out.println("Vamos a por la semana"); //********************

       //si no hay un a�o v�lido todav�a no hace nada y sale
       if (sAnoBk == "") {
         return;
       }

      //indeptemente de si he traido a�o o sigue el que habia, busca semana:
      resul = conv.getNumSem(txtFecSemDesde.getText());

      //Si esa semana no es v�lida recupera los datos anteriores de semana y muestra un mensaje
      if (resul == -1) {
        txtCodSemDesde.setText(sCodSemDesdeBk);
        txtFecSemDesde.setText(sFecSemDesdeBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              sESA_FECHA_NO_PERTENECE);
        msgBox.show();
        msgBox = null;
      }
      else {
        //Si semana es v�lida guarda los datos y muestra su c�d de semana
        txtCodSemDesde.setText(Integer.toString(resul));
        sCodSemDesdeBk = txtCodSemDesde.getText();
        sFecSemDesdeBk = txtFecSemDesde.getText();
      }
      // envia la excepci�n
    }
    catch (Exception e) {
      //#// System_out.println(e.getMessage() );
      e.printStackTrace();
    }
  }

//_____________________ HASTA__________________________

  void txtCodSemHasta_focusLost(FocusEvent evt) {

//#// System_out.println("evento en codSemHasta");//*******************
     int numero;
    String resul;
    CMessage msgBox;
    try {

      txtCodSemHasta.setText(txtCodSemHasta.getText().trim());

      //si no ha cambiado el valor de la caja de texto no hace nada
      if (txtCodSemHasta.getText().equals(sCodSemHastaBk)) {
        txtCodSemHasta.setText(rellenoCeros(txtCodSemHasta.getText(), 2));
        return;
      }

      //chequea cod semana.Debe ser entero entre 1 y 53
      //Si no es ocrrecto o no hay a�o v�lido recupera el valor antiguo y ya est�
      if (
          (sAnoBk == "") || (ChequearEntero(txtCodSemHasta.getText(), 0, 54, 2) == false)
          ) {
        txtCodSemHasta.setText(rellenoCeros(sCodSemHastaBk, 2));
        return;
      }

      //Si hay a�o valido
      numero = Integer.parseInt(txtCodSemHasta.getText());
      resul = conv.getFec(numero);
      //Si no es num semana v�lido recupera los datos anteriores y muestra mensaje
      if (resul.equals("")) {
        txtCodSemHasta.setText(rellenoCeros(sCodSemHastaBk, 2));
        txtFecSemHasta.setText(sFecSemHastaBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              sESA_SEMANA_NO_PERTENECE);
        msgBox.show();
        msgBox = null;

      }
      //si es semana v�lida actualiza la fecha de fin de sem y guarda los datos de reserva
      else {
        txtFecSemHasta.setText(resul);
        sCodSemHastaBk = txtCodSemHasta.getText();
        txtCodSemHasta.setText(rellenoCeros(txtCodSemHasta.getText(), 2));
        sFecSemHastaBk = resul;
      }

      // envia la excepci�n
    }
    catch (Exception e) {
      //#// System_out.println(e.getMessage() );
      e.printStackTrace();

    }
    //#// System_out.println(txtFecSemHasta.getText());//*******************
  }

  void txtFecSemHasta_focusLost(FocusEvent evt) {
//#// System_out.println("evento lostfocus en FecSemHasta");//*******************
//#// System_out.println("valor SCodSemHastaBk *********" + sCodSemHastaBk);//*******************

      java.util.Date dFecha;
    Calendar calen = new GregorianCalendar();
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    int ano;

    String sFecIni;
    String sAnoHasta; //A�o que se obttiene a partir de fecha indicada por usuario
    int resul;
    CMessage msgBox;

    try {

      txtFecSemHasta.setText(txtFecSemHasta.getText().trim());

      //Si la fecha no ha cambiado no hace nada
      if (txtFecSemHasta.getText().equals(sFecSemHastaBk)) {
        return;
      }

      //valida el dato
      txtFecSemHasta.ValidarFecha();
      //Si no es v�lido recupera el antiguo
      if (txtFecSemHasta.getValid().equals("N")) {
        txtFecSemHasta.setText(sFecSemHastaBk);
        return;
      }
      //Si es v�lido le da el formato adecuado
      else if (txtFecSemHasta.getValid().equals("S")) {
        txtFecSemHasta.setText(txtFecSemHasta.getFecha());
      }

      //Pasa el string de fecha a Date
      dFecha = formater.parse(txtFecSemHasta.getText());
      //obtiene el a�o cronol�gico correspondiente a esa fecha
      calen.setTime(dFecha);
      ano = calen.get(Calendar.YEAR);

      // trae fechas del a�o epidemiol�gico de b.datos si es necesario
      if (
          //Si el a�o cronol�gico ha cambiado en fecha
          (! (Integer.toString(ano).equals(sAnoBk))) ||
          //o fecha es de �ltimos de diciembre
          ( (calen.get(Calendar.MONTH) == Calendar.DECEMBER) &&
           (calen.get(Calendar.DAY_OF_MONTH) > 23)) ||
          //o fecha es de principios de enero
          ( (calen.get(Calendar.MONTH) == Calendar.JANUARY) &&
           (calen.get(Calendar.DAY_OF_MONTH) < 9))
          ) {

        //Se va a por el a�o epidemiol�gico  (podr� coincidir con el cronol�gico o no )

        this.modoOperacion = modoESPERA;
        Inicializar();

        sFecIni = txtFecSemHasta.getText();
        conv = conversorfechas.getConvDeFecha(sFecIni, pnlContenedor.getApp());

        this.modoOperacion = modoNORMAL;
        Inicializar();

        // si a�o no es v�lido recupera todos los datos y muestra mensaje
        if (conv == null) {
          conv = convBk;
          txtAno.setText(sAnoBk);
          txtCodSemHasta.setText(sCodSemHastaBk);
          txtFecSemHasta.setText(sFecSemHastaBk);
          msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                                sNO_HAY_DATOS);
          msgBox.show();
          msgBox = null;
        }
        // si es v�lido rellena campo de a�o y guarda datos  para posible recuperaci�n
        //NO se vac�an las cajas de semana
        else {
          convBk = conv;
          sAnoHasta = conv.getAno();
          txtAno.setText(sAnoHasta);
          sAnoBk = sAnoHasta;
        }
      }

//#// System_out.println("Vamos a por la semana"); //********************

       //si no hay un a�o v�lido todav�a no hace nada y sale
       if (sAnoBk == "") {
         return;
       }

      //indeptemente de si he traido a�o o sigue el que habia, busca semana:
      resul = conv.getNumSem(txtFecSemHasta.getText());

      //Si esa semana no es v�lida recupera los datos anteriores de semana y muestra un mensaje
      if (resul == -1) {
        txtCodSemHasta.setText(sCodSemHastaBk);
        txtFecSemHasta.setText(sFecSemHastaBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              sESA_FECHA_NO_PERTENECE);
        msgBox.show();
        msgBox = null;
      }
      else {
        //Si semana es v�lida guarda los datos y muestra su c�d de semana
        txtCodSemHasta.setText(Integer.toString(resul));
        sCodSemHastaBk = txtCodSemHasta.getText();
        sFecSemHastaBk = txtFecSemHasta.getText();
      }
      // envia la excepci�n
    }
    catch (Exception e) {
      //#// System_out.println(e.getMessage() );
      e.printStackTrace();
    }
  }

  void txtAno_focusLost(FocusEvent e) {

    CMessage msgBox;

    String sAnoDesde;
//#// System_out.println("evento en Ano");//*******************

    try {

      txtAno.setText(txtAno.getText().trim());

      //Si el a�o no ha cambiado no hace nada
      if (txtAno.getText().equals(sAnoBk)) {
        return;
      }

      //chequea el a�o.Debe ser entero entre 0 y 9999
      //Si no es ocrrecto recupera el valor antiguo y ya est�
      if (ChequearEntero(txtAno.getText(), 0, 9999, 2) == false) {
        txtAno.setText(sAnoBk);
        return;
      }

      this.modoOperacion = modoESPERA;
      Inicializar();

      //objeto para conversi�n de num semana a fecha y viceversa
      sAnoDesde = txtAno.getText();
      conv = new conversorfechas(sAnoDesde, pnlContenedor.getApp());

      //recupera datos y muestra mensaje si a�o no es v�lido
      if (conv.anoValido() == false) {
        conv = convBk;
//#// System_out.println("sAnoBk  "+ sAnoBk); //*****************

        txtAno.setText(sAnoBk);
        txtCodSemDesde.setText(sCodSemDesdeBk);
        txtFecSemDesde.setText(sFecSemDesdeBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              sNO_HAY_DATOS);
        msgBox.show();
        msgBox = null;
      }
      // si es v�lido vacia cajas de semana y guarda datos del a�o para posible recuperaci�n
      else {

        convBk = conv;
        sAnoBk = txtAno.getText();
        //Semana Desde: Primer dia y semana del a�o Desde
        txtCodSemDesde.setText("1");
        sCodSemDesdeBk = txtCodSemDesde.getText();
        txtFecSemDesde.setText(conv.getFecFinPriSem());
        sFecSemDesdeBk = txtFecSemDesde.getText();

        //Semana Hasta
        //Si a�o es el actual ser� la semana actual
        if (conv.getAno().equals(sAnoActual)) {
          txtCodSemHasta.setText(sCodSemActual);
          txtFecSemHasta.setText(fechaActual);
        }
        //Si es un a�o anterior ser� la ultima semana
        else {
          txtCodSemHasta.setText(Integer.toString(conv.getNumUltSem()));
          txtFecSemHasta.setText(conv.getFec(conv.getNumUltSem()));
        }
        sCodSemHastaBk = txtCodSemHasta.getText();
        sFecSemHastaBk = txtFecSemHasta.getText();

        // JRM
        if (txtCodSemDesde.getText().trim() != "") {
          txtCodSemDesde.setText(rellenoCeros(txtCodSemDesde.getText(), 2));

        }
      }

      // envia la excepci�n
    }
    catch (Exception ex) {
      //#// System_out.println(ex.getMessage() );
      ex.printStackTrace();
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString.equals("")) {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString.equals("")) {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     //comprobar valor
     if (b) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

//#// System_out.println("Devuelve b **********" + b);
     return (b);
   } //fin de ChequearEntero

  public boolean isDataValid() {

    boolean b = false;
    if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!sAnoBk.equals("")) &&
        (!sCodSemDesdeBk.equals("")) &&
        (!sFecSemDesdeBk.equals("")) &&
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos v�lidos
        (txtAno.getText().trim().equals(sAnoBk)) &&
        (txtCodSemDesde.getText().trim().equals(sCodSemDesdeBk)) &&
        (txtFecSemDesde.getText().trim().equals(sFecSemDesdeBk))

        ) {
      return true;
    }
    else {
      return false;
    }

  }

  //Devuelve fecha de �ltima semana
  public int getNumUltSem() {
    return (conv.getNumUltSem());
  }

  /**
   * Mete ceros a la izquierda de Cadena hasta alcanzar el tama�o especificado en Maximo.
   * @param     cadena: Cadena a rellenar con ceros
   * @param     maximo: Maximo longitud de la cadena despues de a�adir ceros
   */
  String rellenoCeros(String cadena, int maximo) {
    int l;
    int i;
    String cadAux = "";

    l = cadena.length();
    if (l < maximo) {
      for (i = 1; i <= maximo - l; i++) {
        cadAux = cadAux + '0';
      }
      cadAux = cadAux + cadena;

      return cadAux;
    }
    else {
      return cadena;
    }
  }

} //clase

class PanFechas_txtCodSemDesde_focusAdapter
    extends java.awt.event.FocusAdapter {
  PanDosFechasUnAno adaptee;

  PanFechas_txtCodSemDesde_focusAdapter(PanDosFechasUnAno adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtCodSemDesde_focusLost(e);
  }
}

class PanFechas_txtFecSemDesde_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  FocusEvent e = null;
  PanDosFechasUnAno adaptee;

  PanFechas_txtFecSemDesde_focusAdapter(PanDosFechasUnAno adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtFecSemDesde_focusLost(e);
  }
}

//____________________________________________________

class PanFechas_txtCodSemHasta_focusAdapter
    extends java.awt.event.FocusAdapter {
  PanDosFechasUnAno adaptee;

  PanFechas_txtCodSemHasta_focusAdapter(PanDosFechasUnAno adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtCodSemHasta_focusLost(e);
  }
}

class PanFechas_txtFecSemHasta_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  FocusEvent e = null;
  PanDosFechasUnAno adaptee;

  PanFechas_txtFecSemHasta_focusAdapter(PanDosFechasUnAno adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtFecSemHasta_focusLost(e);
  }
}

class PanDosFechasUnAno_txtAno_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanDosFechasUnAno adaptee;
  FocusEvent e = null;

  PanDosFechasUnAno_txtAno_focusAdapter(PanDosFechasUnAno adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtAno_focusLost(e);
  }
}
