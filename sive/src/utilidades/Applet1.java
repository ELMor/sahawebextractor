package utilidades;

import com.borland.jbcl.layout.XYLayout;
import capp.CApp;

//import javax.swing.UIManager;
public class Applet1
    extends CApp {
  XYLayout xYLayout1 = new XYLayout();
  boolean isStandalone = false;

  PanAnoSemFech panel = new PanAnoSemFech( (IntContenedor)this, 1, true);

  //Construct the applet

  public Applet1() {
  }

//Initialize the applet

  public void init() {
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //static {
  //  try {
  //    //UIManager.setLookAndFeel(new javax.swing.plaf.metal.MetalLookAndFeel());
  //    //UIManager.setLookAndFeel(new com.sun.java.swing.plaf.motif.MotifLookAndFeel());
  //    UIManager.setLookAndFeel(new com.sun.java.swing.plaf.windows.WindowsLookAndFeel());
  //  }
  //  catch (Exception e) {}
  //}
//Component initialization

  private void jbInit() throws Exception {
    xYLayout1.setWidth(656);
    xYLayout1.setHeight(108);
    this.setLayout(xYLayout1);
  }

//Get Applet information

  public String getAppletInfo() {
    return "Applet Information";
  }

//Get parameter info

  public String[][] getParameterInfo() {
    return null;
  }
}
