
package infedonoma;

import java.io.Serializable;

public class DataEDOnoMA
    implements Serializable {

  public String sAnoDesde = "";
  public String sSemDesde = "";
  public String sAnoHasta = "";
  public String sSemHasta = "";
  public String sCdEnfermedad = "";
  public String sDsEnfermedad = "";
  public String sCdArea = "";
  public String sDsArea = "";
  public boolean porMunicipio = true;
  public boolean bInformeCompleto = false;

  public DataEDOnoMA(String anoDesde, String semDesde, String anoHasta,
                     String semHasta, String cdEnf, String dsEnf, String cdArea,
                     String dsArea) {

    sAnoDesde = anoDesde;
    sSemDesde = semDesde;
    sAnoHasta = anoHasta;
    sSemHasta = semHasta;
    sCdEnfermedad = cdEnf;
    sDsEnfermedad = dsEnf;
    sCdArea = cdArea;
    sDsArea = dsArea;
  }

  public DataEDOnoMA() {
    sAnoDesde = "";
    sSemDesde = "";
    sAnoHasta = "";
    sSemHasta = "";
    sCdEnfermedad = "";
    sDsEnfermedad = "";
    sCdArea = "";
    sDsArea = "";
    porMunicipio = true;
    bInformeCompleto = false;
  }

}
