package infedonoma;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class Pan_InfEDOnoMA
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvInfEDOnoMA";
  final String strLOGO_CCAA = "images/ccaa.gif";

  final int erwCASOS_EDO = 1;
  public Param_C9_2_1 paramC1 = new Param_C9_2_1();
  public DataEDOnoMA datosPar = null;

  // estructuras de datos
  protected Vector vAgrupaciones;
  protected Vector vTotales;
  protected Vector vCasos;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // Para pedir mas paginas
  protected Vector ultSemana = new Vector();

  // report
  ERW erwMun = null;
  ERW erwArea = null;

  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;
  Integer regMostrados = new Integer(0);

  CApp app = null;

  public Pan_InfEDOnoMA(CApp a) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("infedonoma.Res" + a.getIdioma());

    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erwMun = new ERW();
    erwMun.ReadTemplate(app.getCodeBase().toString() + "erw/ERWEDOnoMA.erw");
    erwArea = new ERW();
    erwArea.ReadTemplate(app.getCodeBase().toString() + "erw/ERWEDOnoMAmun.erw");

    // data
    dataHandler = new AppDataHandler();
    erwMun.SetDataSource(dataHandler);
    erwArea.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // todo el informe
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox = null;
    CLista param = new CLista();
    Vector v;

    //E //#// System_out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();
        param.setFilter(lista.getFilter());
        // obtiene los datos del servidor
        //datosPar.iPagina++;
        param.addElement(datosPar);
        param.setFilter("");
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));

        // ARG: Se introduce el login
        param.setLogin(app.getLogin());

        // ARG: Se introduce la lortad
        param.setLortad(app.getParameter("LORTAD"));

        lista = (CLista) stub.doPost(erwCASOS_EDO, param);

        v = (Vector) lista.elementAt(0);
        vTotales = (Vector) lista.elementAt(1);

        for (int j = 0; j < v.size(); j++) {
          vCasos.addElement(v.elementAt(j));

          // control de registros
        }
        Integer tot = (Integer) lista.elementAt(1);
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox = null;
    CLista param = new CLista();
    boolean elBoolean = true;

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();
      //E //#// System_out.println("A");
      // obtiene los datos del servidor
      paramC1.iPagina = 0;
      //E //#// System_out.println("B");
      // �todos los datos?
      datosPar.bInformeCompleto = conTodos;
      //E //#// System_out.println("C");
      //param.addElement(paramC1);
      param.addElement(datosPar);
      //E //#// System_out.println("D");
      param.trimToSize();
      //E //#// System_out.println("E");
      stub.setUrl(new URL(app.getURL() + strSERVLET));
      //E //#// System_out.println("F");
      lista = new CLista();
      //E //#// System_out.println("G");
      lista.setState(CLista.listaVACIA);

      // ARG: Se introduce el login
      param.setLogin(app.getLogin());

      // ARG: Se introduce la lortad
      param.setLortad(app.getParameter("LORTAD"));

      //E //#// System_out.println("Antes");
      lista = (CLista) stub.doPost(erwCASOS_EDO, param);

      /*      infedonoma.SrvInfEDOnoMA srv = new infedonoma.SrvInfEDOnoMA();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                   "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
                                   "pista",
                                   "loteb98");
            lista = srv.doDebug(erwCASOS_EDO, param);
       */
      //E //#// System_out.println("Despu�s");
      // control de registros
      if (lista == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg7.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        elBoolean = false;
      }
      else {
        vCasos = (Vector) lista.elementAt(0);
        Integer tot = (Integer) lista.elementAt(1);

        //_________________
        /*
                 for(int j=0; j<vCasos.size();j++){
         System_out.println("Enfermo "+   ((Param_C9_2_1)(vCasos.elementAt(j))).CD_ENFERMO);
             System_out.println("Zbs "+  ((Param_C9_2_1)(vCasos.elementAt(j))).DS_ZBS);
                 }
         */
        //_________________

        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("CD_NIVEL_1 = CD_NIVEL_1");
        retval.addElement("CD_NIVEL_2 = CD_NIVEL_2");
        retval.addElement("DS_NIVEL_1 = DS_NIVEL_1");
        retval.addElement("DS_NIVEL_2 = DS_NIVEL_2");
        retval.addElement("DS_ENFCIE = DS_ENFCIE");
        retval.addElement("DS_SEMANA = DS_SEMANA");
        retval.addElement("CD_ENFERMO = CD_ENFERMO");
        retval.addElement("DS_ENFERMO = DS_ENFERMO");
        retval.addElement("CD_PROV = CD_PROV");
        retval.addElement("DS_PROV = DS_PROV");
        retval.addElement("CD_MUN = CD_MUN");
        retval.addElement("DS_MUN = DS_MUN");
        retval.addElement("CD_ZBS = CD_ZBS");
        retval.addElement("DS_ZBS = DS_ZBS");
        retval.addElement("CD_E_NOTIF = CD_E_NOTIF");
        retval.addElement("DS_E_NOTIF = DS_E_NOTIF");
        retval.addElement("CD_C_NOTIF = CD_C_NOTIF");
        retval.addElement("DS_C_NOTIF = DS_C_NOTIF");

        dataHandler.RegisterTable(vCasos, "SIVE_C9_2_1", retval, null);
        if (datosPar.porMunicipio) {
          erwClient.setInputProperties(erwMun.GetTemplateManager(),
                                       erwMun.getDATReader(true));
        }
        else {
          erwClient.setInputProperties(erwArea.GetTemplateManager(),
                                       erwArea.getDATReader(true));

          // repintado
          //AIC
        }
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        elBoolean = true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;
  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    String criterio;
    int iEtiqueta = 0;

    String sEtiqueta[] = {
        "CRITERIO1",
        "CRITERIO2",
        "CRITERIO3"};

    TemplateManager tm = null;

    // plantilla
    if (datosPar.porMunicipio) {
      tm = erwMun.GetTemplateManager();
    }
    else {
      tm = erwArea.GetTemplateManager();

      // carga los logos
    }
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    // carga los citerios
    tm.SetLabel("LAB005",
                res.getString("msg8.Text") + datosPar.sAnoDesde +
                res.getString("msg9.Text") + datosPar.sSemDesde);
    tm.SetLabel("LAB007",
                res.getString("msg10.Text") + datosPar.sAnoHasta +
                res.getString("msg11.Text") + datosPar.sSemHasta);
    tm.SetLabel("LAB008", res.getString("msg12.Text") + datosPar.sDsEnfermedad);
    tm.SetLabel("LAB001", res.getString("msg13.Text") + datosPar.sDsArea);
    if (datosPar.porMunicipio) {
      tm.SetLabel("LAB014", res.getString("msg14.Text"));
      criterio = res.getString("msg15.Text");
    }
    else {
      tm.SetLabel("LAB014", res.getString("msg16.Text"));
      criterio = res.getString("msg17.Text");
    }

    tm.SetLabel(sEtiqueta[iEtiqueta],
                res.getString("msg18.Text") + datosPar.sAnoDesde
                + EPanel.SEPARADOR_ANO_SEM + datosPar.sSemDesde
                + res.getString("msg19.Text") + datosPar.sAnoHasta +
                EPanel.SEPARADOR_ANO_SEM + datosPar.sSemHasta);
    iEtiqueta++;

    if (!datosPar.sDsEnfermedad.equals("")) {
      tm.SetLabel(sEtiqueta[iEtiqueta],
                  res.getString("msg20.Text") + datosPar.sCdEnfermedad + " "
                  + datosPar.sDsEnfermedad);
      iEtiqueta++;
    }
    if ( (!datosPar.sDsArea.equals("")) && (datosPar.porMunicipio)) {
      tm.SetLabel(sEtiqueta[iEtiqueta],
                  res.getString("msg21.Text") + datosPar.sCdArea + " "
                  + datosPar.sDsArea);
      iEtiqueta++;
    }

    if (iEtiqueta < 3) {
      for (int b = iEtiqueta; b < 3; b++) {
        tm.SetLabel(sEtiqueta[b], "");
      }
    }
  }
}
