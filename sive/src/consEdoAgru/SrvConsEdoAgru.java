package consEdoAgru;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

// LORTAD
import Registro.RegistroConsultas;
import capp.CLista;
import sapp.DBServlet;

public class SrvConsEdoAgru
    extends DBServlet {

  // informes
  final int erwCASOS_EDO_AGRU = 1;
  final int erwCASOS_EDO_AGRU_NO_EQ_NI_CEN = 2;
  final int erwCASOS_EDO_AGRU_EQ = 3;
  final int erwCASOS_EDO_AGRU_CEN = 4;
  final int erwCASOS_EDO_AGRU_EQ_Y_CEN = 5;

  Vector registros = new Vector();

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // parametros de la consulta
    }
    ParamC2 parCons = (ParamC2) param.firstElement();

    String filtroPeriodo = "";

    if (parCons.anoDesde.equals(parCons.anoHasta)) {
      filtroPeriodo = " (CD_ANOEPI = ? and CD_SEMEPI >= ? and CD_SEMEPI <= ?) ";
    }
    else {
      filtroPeriodo = " ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?)) ";
    }

    // Numero de par�metros empleados en filtroPeriodoA
    int numParamFiltroA = 0;
    String filtroPeriodoA = "";
    if (parCons.anoDesde.equals(parCons.anoHasta)) {
      filtroPeriodoA =
          " (a.CD_ANOEPI = ? and a.CD_SEMEPI >= ? and a.CD_SEMEPI <= ?) ";
      numParamFiltroA = 3;
    }
    else {
      filtroPeriodoA = " ((a.CD_ANOEPI = ? and a.CD_SEMEPI >= ?) or (a.CD_ANOEPI > ? and a.CD_ANOEPI < ?) or (a.CD_ANOEPI = ? and a.CD_SEMEPI <= ?)) ";
      numParamFiltroA = 6;
    }

    // control
    int i = 1;

    // Para las tasas (obtener la poblacion)
    String sMunicipios = null;

    // buffers
    String sMun = new String();
    int iNum;
    DataC2 dat = null;
    String anos[] = new String[parCons.agrupacion];
    String sems[] = new String[parCons.agrupacion];
    int casos[] = new int[parCons.agrupacion];

    Vector guiaSemana = new Vector();

    // objetos de datos
    CLista data = new CLista();
    // Los siguientes vectores se envian en las tres primeras
    // componentes de la CLista

    Vector totalCasos = new Vector();
    Vector totalLineas = new Vector();

    //Vector resultado
    registros = new Vector();

    // establece la conexi�n con la base de datos
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
     }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    //parCons.enfermedad = obtenerCodCIE(con, parCons);
    // modos de operaci�n
    switch (opmode) {
      // Veo si se han seleccionado o no, equipo y centro notificador
      case erwCASOS_EDO_AGRU_NO_EQ_NI_CEN:

        // No se han seleccionado ninguno de los dos

        //// System_out.println("srvConsEdoAgru: erwCASOS_EDO_AGRU_NO_EQ_NI_CEN");

        query =
            "select CD_ANOEPI, CD_SEMEPI, sum(NM_CASOS) from SIVE_RESUMEN_EDOS ";

        String filtro = new String();
        // Condiciones para la enfermedad y para el periodo
        // que van a estar siempre independientemente de los
        // criterios de seleccion
        filtro = filtro + " where CD_ENFCIE = ? and "
            + filtroPeriodo;

        if (!parCons.area.equals("")) {
          filtro = filtro + " and CD_NIVEL_1 = ? ";
        }
        if (!parCons.distrito.equals("")) {
          filtro = filtro + " and CD_NIVEL_2 = ? ";
        }
        if (!parCons.zbs.equals("")) {
          filtro = filtro + " and CD_ZBS = ? ";
        }
        if (!parCons.provincia.equals("")) {
          filtro = filtro + " and CD_PROV = ? ";
        }
        if (!parCons.municipio.equals("")) {
          filtro = filtro + " and CD_MUN = ? ";
        }

        // a�ado el filtro a la consulta
        query = query + filtro + " group by cd_anoepi, cd_semepi"
            + " order by cd_anoepi, cd_semepi";

        // lanza la query
        st = con.prepareStatement(query);

        // st = ponerParamConsulta(st, parCons);
        ponerParamConsultaJRM(st, parCons);

        // Ejecuto la sentencia

        rs = st.executeQuery();

        int iAgru = 0;
        int iEstado;

        // Agrupacion y paginacion
        /*
           registros por pagina = agrupacion * lineas por pagina
           primer registro de la pagina P = P*agru*lineas pagina
         */

        guiaSemana = obtenerGuiaSemana(con, parCons);

        iAgru = 0;
        i = 1;
        iEstado = CLista.listaVACIA;

        // Voy a la pagina que solicita
        int iSem = parCons.numPagina * DBServlet.maxSIZE * parCons.agrupacion;
        boolean resultSetSituado = false;

        // Veo si hay que situar el resultSet en la nueva pagina
        if ( (iSem == 0) || (parCons.bInformeCompleto)) {
          resultSetSituado = true;
        }
        else {
          resultSetSituado = false;

        }
        boolean leer = true;
        String sSem = new String();
        String sAno = new String();
        String guia[] = new String[2];
        int iCasos = 0;

        while (iSem < guiaSemana.size()) {
          // control de tama�o
          if ( (i > DBServlet.maxSIZE * parCons.agrupacion) &&
              (! (parCons.bInformeCompleto))) {
            data.setState(CLista.listaINCOMPLETA);
            //data.setFilter( ((DataC1)registros.lastElement()).DS_PROCESO );

            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);

            // obtengo una nueva semana
          }
          guia = (String[]) guiaSemana.elementAt(iSem);

          // Veo si debo tomar otro registro del resultSet
          if (leer) {
            if (resultSetSituado) {
              if (rs.next()) {
                sAno = rs.getString("CD_ANOEPI");
                sSem = rs.getString("CD_SEMEPI");
                iCasos = rs.getInt(3);
                sMun = "no"; //rs.getString("CD_MUN");
              }
              else {
                // ya no hay mas registros en el resultado
                sAno = new String();
                sSem = new String();
                iCasos = 0;
              }
            }
            else { // viene de mas datos
              Vector temp = new Vector();
              temp = this.situarResultSet(guia, rs, parCons.criterio);
              sAno = (String) temp.elementAt(0);
              sSem = (String) temp.elementAt(1);
              Integer iTemp = (Integer) temp.elementAt(2);
              iCasos = iTemp.intValue();
              sMun = "no"; //(String) temp.elementAt(3);
              resultSetSituado = true;
            }
          }

          if (guia[0].equals(sAno) && guia[1].equals(sSem)) {
            anos[iAgru] = sAno;
            sems[iAgru] = sSem;
            casos[iAgru] = iCasos;
            leer = true;
          }
          else {
            anos[iAgru] = guia[0];
            sems[iAgru] = guia[1];
            casos[iAgru] = 0;
            leer = false;
          }

          if (iAgru == parCons.agrupacion - 1) {
            // Se han obtenido las semanas de una agrupacion y
            // se rellena un dato
            registros.addElement(agruparSemanas(iAgru, anos, sems, casos,
                                                erwCASOS_EDO_AGRU_EQ));
            iAgru = 0;
            anos = new String[parCons.agrupacion];
            sems = new String[parCons.agrupacion];
            casos = new int[parCons.agrupacion];

          }
          else {
            iAgru++;

          }
          i++;
          iSem++;

        }
        if (iAgru != 0) {
          // ultima agrupacion que no esta completa
          registros.addElement(agruparSemanas(iAgru - 1, anos,
                                              sems, casos,
                                              erwCASOS_EDO_AGRU_EQ));

        }
        rs.close();
        rs = null;
        if (st != null) {
          st.close();
        }
        st = null;

        //// System_out.println("SrvConsEdoAgru: voy a calcular las tasas");

        // Para calcular las tasas/////////////////////////////////

        Vector tasasAno = new Vector();
        Boolean hay = new Boolean(false);

        if (parCons.tasas) {
          tasasAno = calcularTasas(parCons, con, st, rs);
          //tasasAno = ( anos, poblaciones)

          //// System_out.println("SrvConsEdoAgru: vuelvo de calcular las tasas: longitud de registros= " +  registros.size());
          //// System_out.println("SrvConsEdoAgru: resultado " + tasasAno.toString());

          //Recorro el vector de agrupaciones con los casos
          DataC2 auxC2 = null;
          Vector tanos = new Vector();
          Vector pobs = new Vector();

          hay = (Boolean) tasasAno.firstElement();

          if (hay.booleanValue()) {
            tanos = (Vector) tasasAno.elementAt(1);
            pobs = (Vector) tasasAno.elementAt(2);
            for (int cont = 0; cont < registros.size(); cont++) {
              Float tasas = new Float(0.0);
              auxC2 = (DataC2) registros.elementAt(cont);
              String aD = auxC2.DESDE.substring(0, 4);
              String aH = auxC2.HASTA.substring(0, 4);
              Long pobla = null;
              // modificacion jlt 18/09/01

              //int ind = tanos.indexOf(aD);
              pobla = (Long) pobs.elementAt(0);

              /*if (aD.equals(aH)){
                int ind = tanos.indexOf(aD);
                pobla = (Long)pobs.elementAt(ind);
                                 }
                                 else{
                // Si en la agupacion intervienen dos a�os
                // calculo la media de la poblacion
                int indD = tanos.indexOf(aD);
                int indH = tanos.indexOf(aH);
                pobla = new Long((((Long)pobs.elementAt(indD)).longValue() + ((Long)pobs.elementAt(indH)).longValue())/ (new Long(2)).longValue());
                                 }*/

              // (100000xcasos)/poblacion
              Long aux = new Long( (new Integer(100000)).longValue() *
                                  (new Integer(auxC2.CASOS)).longValue());
              tasas = new Float(aux.doubleValue() / pobla.doubleValue());
              auxC2.TASAS = tasas.floatValue();
              /*
                                 // System_out.println("SrvConsEdoAgru: agrupacion " + auxC2.DESDE + " "
                                 + auxC2.HASTA + " poblacion " +  pobla
                                 + " tasas " + tasas);
               */
            }
          }
          else { //no hay datos de poblacion
            //for (int cont=0; cont<registros.size(); cont++){
            //  auxC2.TASAS = (new Float(0.0)).floatValue();
            //}
          }
        }

        // Para calcular las tasas/////////////////////////////////

        // Total final de numero de casos
        PreparedStatement st1 = null;

        query = "select sum(NM_CASOS) from SIVE_RESUMEN_EDOS " + filtro;
        st1 = con.prepareStatement(query);

        //st = ponerParamConsulta(st, parCons);
        ponerParamConsultaJRM(st1, parCons);

        rs = st1.executeQuery();
        if (rs.next()) {
          totalCasos.addElement(new Integer(rs.getInt(1)));
        }
        rs.close();
        rs = null;
        st1.close();
        st1 = null;

        // Total lineas (agrupaciones)
        query = "select count(*) from SIVE_RESUMEN_EDOS " + filtro;
        st = con.prepareStatement(query);
        //st = ponerParamConsulta(st, parCons);
        ponerParamConsultaJRM(st, parCons);

        rs = st.executeQuery();
        if (rs.next()) {
          totalLineas.addElement(new Integer(rs.getInt(1) / parCons.agrupacion));
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        // prepara los datos
        registros.trimToSize();
        data.addElement(registros);
        totalCasos.trimToSize();
        data.addElement(totalCasos);
        totalLineas.trimToSize();
        data.addElement(totalLineas);
        data.addElement(hay);
        data.trimToSize();
        break;

      case erwCASOS_EDO_AGRU_EQ:

        // Se ha seleccionado el equipo pero no el centro

        //// System_out.println("srvConsEdoAgru: erwCASOS_EDO_AGRU_EQ");

        String tipoEnf = new String();
        tipoEnf = averiguaDeclaracion(con, parCons.enfermedad);

        guiaSemana = obtenerGuiaSemana(con, parCons);

        String filtroInd = new String();
        String filtroNum = new String();

//E            //# // System_out.println("Se ha seleccionado el equipo pero no el centro");

        // Ver si es una enfermedad de declaracion individual
        // o de declaracion numerica
        String strSelect = null;
        if (tipoEnf.equals("I")) {
          // Declaracion individual
          String queryInd = new String();
          queryInd = "select a.CD_ANOEPI, a.CD_SEMEPI,count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          filtroInd = " where b.CD_ENFCIE = ? and "
              + " a.CD_E_NOTIF = ? and "
              + " a.NM_EDO = b.NM_EDO and a.CD_FUENTE = 'E' and "
              + filtroPeriodoA + " and "
              //" ((a.CD_ANOEPI = ? and a.CD_SEMEPI >= ?) or (a.CD_ANOEPI > ? and a.CD_ANOEPI < ?) or (a.CD_ANOEPI = ? and a.CD_SEMEPI <= ?)) "
              + " group by a.CD_ANOEPI, a.CD_SEMEPI "
              + " order by a.cd_anoepi, a.cd_semepi";
          queryInd = queryInd + filtroInd;
          st = con.prepareStatement(queryInd);
          if (numParamFiltroA == 6) {
            st.setString(1, parCons.enfermedad);
            st.setString(2, parCons.eqNotif);
            st.setString(3, parCons.anoDesde);
            st.setString(4, parCons.semDesde);
            st.setString(5, parCons.anoDesde);
            st.setString(6, parCons.anoHasta);
            st.setString(7, parCons.anoHasta);
            st.setString(8, parCons.semHasta);
            registroConsultas.insertarParametro(parCons.enfermedad);
            registroConsultas.insertarParametro(parCons.eqNotif);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.semDesde);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.anoHasta);
            registroConsultas.insertarParametro(parCons.anoHasta);
            registroConsultas.insertarParametro(parCons.semHasta);
          }
          else {
            st.setString(1, parCons.enfermedad);
            st.setString(2, parCons.eqNotif);
            st.setString(3, parCons.anoDesde);
            st.setString(4, parCons.semDesde);
            st.setString(5, parCons.semHasta);
            registroConsultas.insertarParametro(parCons.enfermedad);
            registroConsultas.insertarParametro(parCons.eqNotif);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.semDesde);
            registroConsultas.insertarParametro(parCons.semHasta);
          }
          strSelect = queryInd;
        }
        else {
          // Declaracion numerica
          String queryNum = new String();
          queryNum = "select CD_ANOEPI, CD_SEMEPI, NM_CASOS from SIVE_EDONUM ";
          filtroNum = " where CD_ENFCIE = ? and CD_E_NOTIF = ? and "
              + " ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?)) "
              + " order by CD_ANOEPI, CD_SEMEPI";
          queryNum = queryNum + filtroNum;
          st = con.prepareStatement(queryNum);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);
        }

        // Ejecuto la sentencia
        if (tipoEnf.equals("I")) {
          registroConsultas.registrar("SIVE_EDOIND",
                                      strSelect,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvConsEdoAgru",
                                      true);

        }
        rs = st.executeQuery();

        // Agrupacion y paginacion
        /*
           registros por pagina = agrupacion * lineas por pagina
           primer registro de la pagina P = P*agru*lineas pagina
         */
        iAgru = 0;
        i = 1;
        iEstado = CLista.listaVACIA;

        // Voy a la pagina que solicita
        iSem = parCons.numPagina * DBServlet.maxSIZE * parCons.agrupacion;
//E            //# // System_out.println("iSem=" + iSem);
        resultSetSituado = false;

        // Veo si hay que situar el resultSet en la nueva pagina
        if ( (iSem == 0) || (parCons.bInformeCompleto)) {
          resultSetSituado = true;
        }
        else {
          resultSetSituado = false;

        }
        leer = true;
        sSem = new String();
        sAno = new String();
        guia = new String[2];
        iCasos = 0;
        //String guiaSemArray[] = new String[guiaSemana.size()][2];
        //guiaSemana.copyInto(guiaSemArray);

        while (iSem < guiaSemana.size()) {
          // control de tama�o
          if ( (i > DBServlet.maxSIZE * parCons.agrupacion) &&
              (! (parCons.bInformeCompleto))) {
            data.setState(CLista.listaINCOMPLETA);
            //data.setFilter( ((DataC1)registros.lastElement()).DS_PROCESO );

            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          guia = (String[]) guiaSemana.elementAt(iSem);

          // Veo si debo tomar otro registro del resultSet
          if (leer) {
            if (resultSetSituado) {
              if (rs.next()) {
                sAno = rs.getString("CD_ANOEPI");
                sSem = rs.getString("CD_SEMEPI");
                iCasos = rs.getInt(3);
//E                    //# // System_out.println("He leido el siguiente " + sAno +"  "+ sSem +"  "+ iCasos);
              }
              else {
                // ya no hay mas registros en el resultado
//E                    //# // System_out.println("ya no hay mas registros en el resultado");
                sAno = new String();
                sSem = new String();
                iCasos = 0;
              }
            }
            else { // viene de mas datos
//E                  //# // System_out.println("RESULTSET NO SITUADO");
              Vector temp = new Vector();
              temp = this.situarResultSet(guia, rs, parCons.criterio);
              sAno = (String) temp.elementAt(0);
              sSem = (String) temp.elementAt(1);
              Integer iTemp = (Integer) temp.elementAt(2);
              iCasos = iTemp.intValue();
              resultSetSituado = true;
            }
          }
          if (guia[0].equals(sAno) && guia[1].equals(sSem)) {
//E                //# // System_out.println("Son iguales");
            anos[iAgru] = sAno;
            sems[iAgru] = sSem;
            casos[iAgru] = iCasos;
            leer = true;
          }
          else {
            anos[iAgru] = guia[0];
            sems[iAgru] = guia[1];
            casos[iAgru] = 0;
            leer = false;
          }

          if (iAgru == parCons.agrupacion - 1) {
            // Se han obtenido las semanas de una agrupacion y
            // se rellena un dato
//E                //# // System_out.println("antes de las semanas de una agrupacion iAgru=" + iAgru);
            registros.addElement(agruparSemanas(iAgru, anos, sems, casos,
                                                erwCASOS_EDO_AGRU_EQ));
            iAgru = 0;
            anos = new String[parCons.agrupacion];
            sems = new String[parCons.agrupacion];
            casos = new int[parCons.agrupacion];

          }
          else {
            iAgru++;

          }
          i++;
          iSem++;

        }
        if (iAgru != 0) {
          // ultima agrupacion que no esta completa
//E                  //# // System_out.println("ultima agrupacion incompleta agrupacion iAgru=" + iAgru);
          registros.addElement(agruparSemanas(iAgru - 1, anos,
                                              sems, casos,
                                              erwCASOS_EDO_AGRU_EQ));

        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        hay = new Boolean(false);

        // Para calcular las tasas/////////////////////////////////
        // En este caso no hay tasas

        // Total final de numero de casos y de lineas (agrupaciones)
        if (tipoEnf.equals("I")) {
          // Total de casos
          query = "select count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          query = query + filtroInd.substring(0, filtroInd.indexOf("group"));
          st = con.prepareStatement(query);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);

          rs = st.executeQuery();
          if (rs.next()) {
            totalCasos.addElement(new Integer(rs.getInt(1)));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          // Total de lineas (agrupaciones)
          query = "select a.CD_ANOEPI, a.CD_SEMEPI,count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          query = query + filtroInd;
//E              //# // System_out.println(query);
          st = con.prepareStatement(query);

          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);

          registroConsultas.insertarParametro(parCons.enfermedad);
          registroConsultas.insertarParametro(parCons.eqNotif);
          registroConsultas.insertarParametro(parCons.anoDesde);
          registroConsultas.insertarParametro(parCons.semDesde);
          registroConsultas.insertarParametro(parCons.anoDesde);
          registroConsultas.insertarParametro(parCons.anoHasta);
          registroConsultas.insertarParametro(parCons.anoHasta);
          registroConsultas.insertarParametro(parCons.semHasta);

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvConsEdoAgru",
                                      true);

          rs = st.executeQuery();
          if (rs.next()) {
            totalLineas.addElement(new Integer(rs.getInt(3) /
                                               parCons.agrupacion));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        }
        else {
          // Total casos
          query = "select sum(NM_CASOS) from SIVE_EDONUM ";
          query = query + filtroNum;
          st = con.prepareStatement(query);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);

          rs = st.executeQuery();
          if (rs.next()) {
            totalCasos.addElement(new Integer(rs.getInt(1)));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          // Total lineas (agrupaciones)
          query = "select count(*) from SIVE_EDONUM ";
          query = query + filtroNum;
//E              //# // System_out.println(query);
          st = con.prepareStatement(query);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);

          rs = st.executeQuery();
          if (rs.next()) {
            totalLineas.addElement(new Integer(rs.getInt(1) /
                                               parCons.agrupacion));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

        }

        // prepara los datos
        registros.trimToSize();
        data.addElement(registros);
        totalCasos.trimToSize();
        data.addElement(totalCasos);
        totalLineas.trimToSize();
        data.addElement(totalLineas);
        data.addElement(hay);
        data.trimToSize();
        break;

      case erwCASOS_EDO_AGRU_CEN:

        // Se ha seleccionado el centro pero no el equipo

//E            //# // System_out.println("C2: Se ha seleccionado el centro pero no el equipo");

        //// System_out.println("srvConsEdoAgru: erwCASOS_EDO_AGRU_CEN");

        tipoEnf = averiguaDeclaracion(con, parCons.enfermedad);

        filtroInd = new String();
        filtroNum = new String();

        // Ver si es una enfermedad de declaracion individual
        // o de declaracion numerica
        strSelect = null;
        if (tipoEnf.equals("I")) {
          // Declaracion individual
          String queryInd = new String();
          queryInd = "select a.CD_ANOEPI, a.CD_SEMEPI,count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          filtroInd = " where b.CD_ENFCIE = ? and "
              + " a.NM_EDO = b.NM_EDO and a.CD_FUENTE = 'E' and "
              + filtroPeriodoA + " and"
              //" ((a.CD_ANOEPI = ? and a.CD_SEMEPI >= ?) or (a.CD_ANOEPI > ? and a.CD_ANOEPI < ?) or (a.CD_ANOEPI = ? and a.CD_SEMEPI <= ?)) and "
              +
              " a.CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_CENTRO = ?)"
              + " group by a.CD_ANOEPI, a.CD_SEMEPI ";
          queryInd = queryInd + filtroInd +
              " order by a.cd_anoepi, a.cd_semepi";
          st = con.prepareStatement(queryInd);
          if (numParamFiltroA == 6) {
            st.setString(1, parCons.enfermedad);
            st.setString(2, parCons.anoDesde);
            st.setString(3, parCons.semDesde);
            st.setString(4, parCons.anoDesde);
            st.setString(5, parCons.anoHasta);
            st.setString(6, parCons.anoHasta);
            st.setString(7, parCons.semHasta);
            st.setString(8, parCons.cenNotif);

            registroConsultas.insertarParametro(parCons.enfermedad);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.semDesde);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.anoHasta);
            registroConsultas.insertarParametro(parCons.anoHasta);
            registroConsultas.insertarParametro(parCons.semHasta);
            registroConsultas.insertarParametro(parCons.cenNotif);
          }
          else {
            st.setString(1, parCons.enfermedad);
            st.setString(2, parCons.anoDesde);
            st.setString(3, parCons.semDesde);
            st.setString(4, parCons.semHasta);
            st.setString(5, parCons.cenNotif);

            registroConsultas.insertarParametro(parCons.enfermedad);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.semDesde);
            registroConsultas.insertarParametro(parCons.semHasta);
            registroConsultas.insertarParametro(parCons.cenNotif);
          }
          strSelect = queryInd;
//E              //# // System_out.println(queryInd);
        }
        else {
          // Declaracion numerica
          String queryNum = new String();
          queryNum = "select CD_ANOEPI, CD_SEMEPI, NM_CASOS from SIVE_EDONUM ";
          filtroNum = " where CD_ENFCIE = ? and  "
              + " ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?)) and "
              +
              " CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_CENTRO = ?)";
          queryNum = queryNum + filtroNum + " order by cd_anoepi, cd_semepi";
          st = con.prepareStatement(queryNum);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.anoDesde);
          st.setString(3, parCons.semDesde);
          st.setString(4, parCons.anoDesde);
          st.setString(5, parCons.anoHasta);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.semHasta);
          st.setString(8, parCons.cenNotif);
//E              //# // System_out.println(queryNum);
        }

        // Ejecuto la sentencia
        if (tipoEnf.equals("I")) {
          registroConsultas.registrar("SIVE_EDOIND",
                                      strSelect,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvConsEdoAgru",
                                      true);

        }
        rs = st.executeQuery();

        // Agrupacion y paginacion
        /*
           registros por pagina = agrupacion * lineas por pagina
           primer registro de la pagina P = P*agru*lineas pagina
         */
        iAgru = 0;
        i = 1;
        iEstado = CLista.listaVACIA;

        // Voy a la pagina que solicita
        iSem = parCons.numPagina * DBServlet.maxSIZE * parCons.agrupacion;
//E            //# // System_out.println("iSem=" + iSem);
        resultSetSituado = false;

        // Veo si hay que situar el resultSet en la nueva pagina
        if ( (iSem == 0) || (parCons.bInformeCompleto)) {
          resultSetSituado = true;
        }
        else {
          resultSetSituado = false;

        }
        leer = true;
        sSem = new String();
        sAno = new String();
        guia = new String[2];
        iCasos = 0;

        guiaSemana = obtenerGuiaSemana(con, parCons);

        while (iSem < guiaSemana.size()) {
          // control de tama�o
          if ( (i > DBServlet.maxSIZE * parCons.agrupacion) &&
              (! (parCons.bInformeCompleto))) {
            data.setState(CLista.listaINCOMPLETA);
            //data.setFilter( ((DataC1)registros.lastElement()).DS_PROCESO );

            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          guia = (String[]) guiaSemana.elementAt(iSem);

          // Veo si debo tomar otro registro del resultSet
          if (leer) {
            if (resultSetSituado) {
              if (rs.next()) {
                sAno = rs.getString("CD_ANOEPI");
                sSem = rs.getString("CD_SEMEPI");
                iCasos = rs.getInt(3);
//E                    //# // System_out.println("He leido el siguiente " + sAno +"  "+ sSem +"  "+ iCasos);
              }
              else {
                // ya no hay mas registros en el resultado
//E                    //# // System_out.println("ya no hay mas registros en el resultado");
                sAno = new String();
                sSem = new String();
                iCasos = 0;
              }
            }
            else { // viene de mas datos
//E                  //# // System_out.println("RESULTSET NO SITUADO");
              Vector temp = new Vector();
              temp = this.situarResultSet(guia, rs, parCons.criterio);
              sAno = (String) temp.elementAt(0);
              sSem = (String) temp.elementAt(1);
              Integer iTemp = (Integer) temp.elementAt(2);
              iCasos = iTemp.intValue();
              resultSetSituado = true;
//E                  //# // System_out.println("RESULTSET SITUADO");
            }
          }

          if (guia[0].equals(sAno) && guia[1].equals(sSem)) {
//E                //# // System_out.println("Son iguales");
            anos[iAgru] = sAno;
            sems[iAgru] = sSem;
            casos[iAgru] = iCasos;
            leer = true;
          }
          else {
            anos[iAgru] = guia[0];
            sems[iAgru] = guia[1];
            casos[iAgru] = 0;
            leer = false;
          }

          if (iAgru == parCons.agrupacion - 1) {
            // Se han obtenido las semanas de una agrupacion y
            // se rellena un dato
//E                //# // System_out.println("antes de las semanas de una agrupacion iAgru=" + iAgru);
            registros.addElement(agruparSemanas(iAgru, anos, sems, casos,
                                                erwCASOS_EDO_AGRU_EQ));
            iAgru = 0;
            anos = new String[parCons.agrupacion];
            sems = new String[parCons.agrupacion];
            casos = new int[parCons.agrupacion];

          }
          else {
            iAgru++;

          }
          i++;
          iSem++;

        }
        if (iAgru != 0) {
          // ultima agrupacion que no esta completa
//E                  //# // System_out.println("ultima agrupacion incompleta agrupacion iAgru=" + iAgru);
          registros.addElement(agruparSemanas(iAgru - 1, anos,
                                              sems, casos,
                                              erwCASOS_EDO_AGRU_EQ));

        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        hay = new Boolean(false);
        // Para calcular las tasas/////////////////////////////////
        // en este caso no hay tasas

        // Total final de numero de casos y de lineas (agrupaciones)
        if (tipoEnf.equals("I")) {
          // Total de casos
          query = "select count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          query = query + filtroInd.substring(0, filtroInd.indexOf("group"));
//E              //# // System_out.println(query);
          st = con.prepareStatement(query);
          if (numParamFiltroA == 6) {
            st.setString(1, parCons.enfermedad);
            st.setString(2, parCons.anoDesde);
            st.setString(3, parCons.semDesde);
            st.setString(4, parCons.anoDesde);
            st.setString(5, parCons.anoHasta);
            st.setString(6, parCons.anoHasta);
            st.setString(7, parCons.semHasta);
            st.setString(8, parCons.cenNotif);

            registroConsultas.insertarParametro(parCons.enfermedad);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.semDesde);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.anoHasta);
            registroConsultas.insertarParametro(parCons.anoHasta);
            registroConsultas.insertarParametro(parCons.semHasta);
            registroConsultas.insertarParametro(parCons.cenNotif);
          }
          else {
            st.setString(1, parCons.enfermedad);
            st.setString(2, parCons.anoDesde);
            st.setString(3, parCons.semDesde);
            st.setString(4, parCons.semHasta);
            st.setString(5, parCons.cenNotif);

            registroConsultas.insertarParametro(parCons.enfermedad);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.semDesde);
            registroConsultas.insertarParametro(parCons.semHasta);
            registroConsultas.insertarParametro(parCons.cenNotif);
          }

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvConsEdoAgru",
                                      true);

          rs = st.executeQuery();
          if (rs.next()) {
            totalCasos.addElement(new Integer(rs.getInt(1)));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          // Total de lineas (agrupaciones)
          query = "select a.CD_ANOEPI, a.CD_SEMEPI,count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          query = query + filtroInd;
//E              //# // System_out.println(query);
          st = con.prepareStatement(query);
          if (numParamFiltroA == 6) {
            st.setString(1, parCons.enfermedad);
            st.setString(2, parCons.anoDesde);
            st.setString(3, parCons.semDesde);
            st.setString(4, parCons.anoDesde);
            st.setString(5, parCons.anoHasta);
            st.setString(6, parCons.anoHasta);
            st.setString(7, parCons.semHasta);
            st.setString(8, parCons.cenNotif);

            registroConsultas.insertarParametro(parCons.enfermedad);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.semDesde);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.anoHasta);
            registroConsultas.insertarParametro(parCons.anoHasta);
            registroConsultas.insertarParametro(parCons.semHasta);
            registroConsultas.insertarParametro(parCons.cenNotif);
          }
          else {
            st.setString(1, parCons.enfermedad);
            st.setString(2, parCons.anoDesde);
            st.setString(3, parCons.semDesde);
            st.setString(4, parCons.semHasta);
            st.setString(5, parCons.cenNotif);

            registroConsultas.insertarParametro(parCons.enfermedad);
            registroConsultas.insertarParametro(parCons.anoDesde);
            registroConsultas.insertarParametro(parCons.semDesde);
            registroConsultas.insertarParametro(parCons.semHasta);
            registroConsultas.insertarParametro(parCons.cenNotif);
          }

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvConsEdoAgru",
                                      true);

          rs = st.executeQuery();
          if (rs.next()) {
            totalLineas.addElement(new Integer(rs.getInt(3) /
                                               parCons.agrupacion));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        }
        else {
          // Total casos
          query = "select sum(NM_CASOS) from SIVE_EDONUM ";
          query = query + filtroNum;
          st = con.prepareStatement(query);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.anoDesde);
          st.setString(3, parCons.semDesde);
          st.setString(4, parCons.anoDesde);
          st.setString(5, parCons.anoHasta);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.semHasta);
          st.setString(8, parCons.cenNotif);
          rs = st.executeQuery();
          if (rs.next()) {
            totalCasos.addElement(new Integer(rs.getInt(1)));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          // Total lineas (agrupaciones)
          query = "select count(*) from SIVE_EDONUM ";
          query = query + filtroNum;
//E              //# // System_out.println(query);
          st = con.prepareStatement(query);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.anoDesde);
          st.setString(3, parCons.semDesde);
          st.setString(4, parCons.anoDesde);
          st.setString(5, parCons.anoHasta);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.semHasta);
          st.setString(8, parCons.cenNotif);
          rs = st.executeQuery();
          if (rs.next()) {
            totalLineas.addElement(new Integer(rs.getInt(1) /
                                               parCons.agrupacion));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

        }

        // prepara los datos
        registros.trimToSize();
        data.addElement(registros);
        totalCasos.trimToSize();
        data.addElement(totalCasos);
        totalLineas.trimToSize();
        data.addElement(totalLineas);
        data.addElement(hay);
        data.trimToSize();

        break;
      case erwCASOS_EDO_AGRU_EQ_Y_CEN:

        // Seleccion del centro y del equipo

        //// System_out.println("srvConsEdoAgru: erwCASOS_EDO_AGRU_EQ_Y_CEN");

        tipoEnf = averiguaDeclaracion(con, parCons.enfermedad);

        filtroInd = new String();
        filtroNum = new String();

        // Ver si es una enfermedad de declaracion individual
        // o de declaracion numerica
        strSelect = null;
        if (tipoEnf.equals("I")) {
          // Declaracion individual
          String queryInd = new String();
          queryInd = "select a.CD_ANOEPI, a.CD_SEMEPI,count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          filtroInd = " where b.CD_ENFCIE = ? and "
              + " a.CD_E_NOTIF = ? and "
              + " a.NM_EDO = b.NM_EDO and a.CD_FUENTE = 'E' and "
              + filtroPeriodoA + " and "
              //+ " ((a.CD_ANOEPI = ? and a.CD_SEMEPI >= ?) or (a.CD_ANOEPI > ? and a.CD_ANOEPI < ?) or (a.CD_ANOEPI = ? and a.CD_SEMEPI <= ?)) and "
              +
              " a.CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_CENTRO = ?)"
              + " group by a.CD_ANOEPI, a.CD_SEMEPI ";
          queryInd = queryInd + filtroInd +
              " order by a.cd_anoepi, a.cd_semepi";
          st = con.prepareStatement(queryInd);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);
          st.setString(9, parCons.cenNotif);

          registroConsultas.insertarParametro(parCons.enfermedad);
          registroConsultas.insertarParametro(parCons.eqNotif);
          registroConsultas.insertarParametro(parCons.anoDesde);
          registroConsultas.insertarParametro(parCons.semDesde);
          registroConsultas.insertarParametro(parCons.anoDesde);
          registroConsultas.insertarParametro(parCons.anoHasta);
          registroConsultas.insertarParametro(parCons.anoHasta);
          registroConsultas.insertarParametro(parCons.semHasta);
          registroConsultas.insertarParametro(parCons.cenNotif);
          strSelect = queryInd;
        }
        else {
          // Declaracion numerica
          String queryNum = new String();
          queryNum = "select CD_ANOEPI, CD_SEMEPI, NM_CASOS from SIVE_EDONUM ";
          filtroNum = " where CD_ENFCIE = ? and CD_E_NOTIF = ? and "
              + " ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?)) and "
              +
              " CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_CENTRO = ?)";
          queryNum = queryNum + filtroNum + " order by cd_anoepi, cd_semepi";
          st = con.prepareStatement(queryNum);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);
          st.setString(9, parCons.cenNotif);
        }

        // Ejecuto la sentencia
        if (tipoEnf.equals("I")) {
          registroConsultas.registrar("SIVE_EDOIND",
                                      strSelect,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvConsEdoAgru",
                                      true);

        }
        rs = st.executeQuery();

        // Agrupacion y paginacion
        /*
           registros por pagina = agrupacion * lineas por pagina
           primer registro de la pagina P = P*agru*lineas pagina
         */
        iAgru = 0;
        i = 1;
        iEstado = CLista.listaVACIA;

        // Voy a la pagina que solicita
        iSem = parCons.numPagina * DBServlet.maxSIZE * parCons.agrupacion;
//E            //# // System_out.println("iSem=" + iSem);
        resultSetSituado = false;

        // Veo si hay que situar el resultSet en la nueva pagina
        if ( (iSem == 0) || (parCons.bInformeCompleto)) {
          resultSetSituado = true;
        }
        else {
          resultSetSituado = false;

        }
        leer = true;
        sSem = new String();
        sAno = new String();
        guia = new String[2];
        iCasos = 0;

        guiaSemana = obtenerGuiaSemana(con, parCons);

        while (iSem < guiaSemana.size()) {
          // control de tama�o
          if ( (i > DBServlet.maxSIZE * parCons.agrupacion) &&
              (! (parCons.bInformeCompleto))) {
            data.setState(CLista.listaINCOMPLETA);
            //data.setFilter( ((DataC1)registros.lastElement()).DS_PROCESO );

            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);

          }
          guia = (String[]) guiaSemana.elementAt(iSem);

          // Veo si debo tomar otro registro del resultSet
          if (leer) {
            if (resultSetSituado) {
              if (rs.next()) {
                sAno = rs.getString("CD_ANOEPI");
                sSem = rs.getString("CD_SEMEPI");
                iCasos = rs.getInt(3);
//E                    //# // System_out.println("He leido el siguiente " + sAno +"  "+ sSem +"  "+ iCasos);
              }
              else {
                // ya no hay mas registros en el resultado
//E                    //# // System_out.println("ya no hay mas registros en el resultado");
                sAno = new String();
                sSem = new String();
                iCasos = 0;
              }
            }
            else { // viene de mas datos
//E                  //# // System_out.println("RESULTSET NO SITUADO");
              Vector temp = new Vector();
              temp = this.situarResultSet(guia, rs, parCons.criterio);
              sAno = (String) temp.elementAt(0);
              sSem = (String) temp.elementAt(1);
              Integer iTemp = (Integer) temp.elementAt(2);
              iCasos = iTemp.intValue();
              resultSetSituado = true;
            }
          }

          if (guia[0].equals(sAno) && guia[1].equals(sSem)) {
//E                //# // System_out.println("Son iguales");
            anos[iAgru] = sAno;
            sems[iAgru] = sSem;
            casos[iAgru] = iCasos;
            leer = true;
          }
          else {
            anos[iAgru] = guia[0];
            sems[iAgru] = guia[1];
            casos[iAgru] = 0;
            leer = false;
          }

          if (iAgru == parCons.agrupacion - 1) {
            // Se han obtenido las semanas de una agrupacion y
            // se rellena un dato
//E                //# // System_out.println("antes de las semanas de una agrupacion iAgru=" + iAgru);
            registros.addElement(agruparSemanas(iAgru, anos, sems, casos,
                                                erwCASOS_EDO_AGRU_EQ));
            iAgru = 0;
            anos = new String[parCons.agrupacion];
            sems = new String[parCons.agrupacion];
            casos = new int[parCons.agrupacion];

          }
          else {
            iAgru++;

          }
          i++;
          iSem++;

        }
        if (iAgru != 0) {
          // ultima agrupacion que no esta completa
//E                  //# // System_out.println("ultima agrupacion incompleta agrupacion iAgru=" + iAgru);
          registros.addElement(agruparSemanas(iAgru - 1, anos,
                                              sems, casos,
                                              erwCASOS_EDO_AGRU_EQ));

        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        hay = new Boolean(false);

        // Para calcular las tasas/////////////////////////////////
        // en este caso no hay tasas

        // Total final de numero de casos y de lineas (agrupaciones)
        if (tipoEnf.equals("I")) {
          // Total de casos
          query = "select count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          query = query + filtroInd.substring(0, filtroInd.indexOf("group"));
//E              //# // System_out.println(query);
          st = con.prepareStatement(query);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);
          st.setString(9, parCons.cenNotif);

          registroConsultas.insertarParametro(parCons.enfermedad);
          registroConsultas.insertarParametro(parCons.eqNotif);
          registroConsultas.insertarParametro(parCons.anoDesde);
          registroConsultas.insertarParametro(parCons.semDesde);
          registroConsultas.insertarParametro(parCons.anoDesde);
          registroConsultas.insertarParametro(parCons.anoHasta);
          registroConsultas.insertarParametro(parCons.anoHasta);
          registroConsultas.insertarParametro(parCons.semHasta);
          registroConsultas.insertarParametro(parCons.cenNotif);

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvConsEdoAgru",
                                      true);

          rs = st.executeQuery();
          if (rs.next()) {
            totalCasos.addElement(new Integer(rs.getInt(1)));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          // Total de lineas (agrupaciones)
          query = "select a.CD_ANOEPI, a.CD_SEMEPI,count(a.NM_EDO) "
              + " from SIVE_NOTIF_EDOI a, SIVE_EDOIND b ";
          query = query + filtroInd;
//E              //# // System_out.println(query);
          st = con.prepareStatement(query);

          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);
          st.setString(9, parCons.cenNotif);

          registroConsultas.insertarParametro(parCons.enfermedad);
          registroConsultas.insertarParametro(parCons.eqNotif);
          registroConsultas.insertarParametro(parCons.anoDesde);
          registroConsultas.insertarParametro(parCons.semDesde);
          registroConsultas.insertarParametro(parCons.anoDesde);
          registroConsultas.insertarParametro(parCons.anoHasta);
          registroConsultas.insertarParametro(parCons.anoHasta);
          registroConsultas.insertarParametro(parCons.semHasta);
          registroConsultas.insertarParametro(parCons.cenNotif);

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvConsEdoAgru",
                                      true);

          rs = st.executeQuery();
          if (rs.next()) {
            totalLineas.addElement(new Integer(rs.getInt(3) /
                                               parCons.agrupacion));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        }
        else {
          // Total casos
          query = "select sum(NM_CASOS) from SIVE_EDONUM ";
          query = query + filtroNum;
          st = con.prepareStatement(query);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);
          st.setString(9, parCons.cenNotif);
          rs = st.executeQuery();
          if (rs.next()) {
            totalCasos.addElement(new Integer(rs.getInt(1)));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          // Total lineas (agrupaciones)
          query = "select count(*) from SIVE_EDONUM ";
          query = query + filtroNum;
//E              //# // System_out.println(query);
          st = con.prepareStatement(query);
          st.setString(1, parCons.enfermedad);
          st.setString(2, parCons.eqNotif);
          st.setString(3, parCons.anoDesde);
          st.setString(4, parCons.semDesde);
          st.setString(5, parCons.anoDesde);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.anoHasta);
          st.setString(8, parCons.semHasta);
          st.setString(9, parCons.cenNotif);
          rs = st.executeQuery();
          if (rs.next()) {
            totalLineas.addElement(new Integer(rs.getInt(1) /
                                               parCons.agrupacion));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

        }

        // prepara los datos
        registros.trimToSize();
        data.addElement(registros);
        totalCasos.trimToSize();
        data.addElement(totalCasos);
        totalLineas.trimToSize();
        data.addElement(totalLineas);
        data.addElement(hay);
        data.trimToSize();

        break;

    } // fin del switch (opMode)

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return data;
  }

  private Vector calcularTasas(ParamC2 parCons, Connection con,
                               PreparedStatement st,
                               ResultSet rs) throws Exception {

    Vector tasas = new Vector();
    Vector auxA = new Vector(); //Vector de a�os
    Vector auxP = new Vector(); //Vector de poblacion
    Boolean hay = new Boolean(false);

    String query = "";

    // modificacion jlt 17/09/01
    // se calcula la poblacion a partir del a�o maximo que sea
    // menor o igual que el a�o desde

    if (parCons.tasas) {
      String anos = "";
      /*if (parCons.anoDesde.equals(parCons.anoHasta)){
        anos = " CD_ANOEPI = ? ";
             }
             else{
        anos = " CD_ANOEPI >= ? and CD_ANOEPI <= ? ";
             }*/

      /////////////////
      anos = " CD_ANOEPI IN (SELECT MAX(CD_ANOEPI) FROM " +
          " SIVE_POBLACION_NS WHERE CD_ANOEPI <= ? ) ";
      /////////////////

      /*
             // de SIVE_POBLACION_NG
             if (!parCons.provincia.equals("")){
        if (parCons.municipio.equals("")) {//para toda la provincia
           query = " select sum(NM_POBLACION), CD_ANOEPI from SIVE_POBLACION_NG "
                        + " where CD_PROV = ? and "
                        + anos;
        }
        else { //para ese municipio
           query = " select sum(NM_POBLACION), CD_ANOEPI from SIVE_POBLACION_NG "
                        + " where CD_PROV = ? and CD_MUN = ? and "
                        + anos;
        }
             }
             // de SIVE_POBLACION_NS
             else{ */
      if (parCons.area.equals("")) { //para toda la comunidad
        query = " select sum(NM_POBLACION), CD_ANOEPI from SIVE_POBLACION_NS "
            + " where "
            + anos;
      }
      else if (parCons.distrito.equals("")) { //para un area
        query = " select sum(NM_POBLACION), CD_ANOEPI from SIVE_POBLACION_NS "
            + " where CD_NIVEL_1 = ? and "
            + anos;
      }
      else if (parCons.zbs.equals("")) { //para un distrito
        query = " select sum(NM_POBLACION), CD_ANOEPI from SIVE_POBLACION_NS "
            + " where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and "
            + anos;
      }
      else { //para una zbs
        query = " select sum(NM_POBLACION), CD_ANOEPI from SIVE_POBLACION_NS "
            + " where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and CD_ZBS = ? and "
            + anos;
      }
      //}

      query = query + " group by CD_ANOEPI ";

      //// System_out.println("SrvConsEdoAgru: " + query);
      // lanza la query
      st = con.prepareStatement(query);

      int iPar = 1;
      // de SIVE_POBLACION_NG
      /*
             if (!parCons.provincia.equals("")){
        if (parCons.municipio.equals("")) {//para toda la provincia
          st.setString(iPar, parCons.provincia);
          iPar++;
        }
        else { //para ese municipio
          st.setString(iPar, parCons.provincia);
          iPar++;
          st.setString(iPar, parCons.municipio);
          iPar++;
        }
             }
             // de SIVE_POBLACION_NS
             else{ */
      if (parCons.area.equals("")) { //para toda la comunidad
      }
      else if (parCons.distrito.equals("")) { //para un area
        st.setString(iPar, parCons.area);
        iPar++;
      }
      else if (parCons.zbs.equals("")) { //para un distrito
        st.setString(iPar, parCons.area);
        iPar++;
        st.setString(iPar, parCons.distrito);
        iPar++;
      }
      else { //para una zbs
        st.setString(iPar, parCons.area);
        iPar++;
        st.setString(iPar, parCons.distrito);
        iPar++;
        st.setString(iPar, parCons.zbs);
        iPar++;
      }
      //}

      // modificacion jlt 18/09/01
      /*if (parCons.anoDesde.equals(parCons.anoHasta)){
        st.setString(iPar, parCons.anoHasta);
        iPar++;
             }
             else{
        st.setString(iPar, parCons.anoDesde);
        iPar++;
        st.setString(iPar, parCons.anoHasta);
        iPar++;
             } */

      st.setString(iPar, parCons.anoDesde);
      iPar++;

      rs = st.executeQuery();
      long pob = 0;
      String ano = "";
      int i = 0;
      while (rs.next()) {
        pob = rs.getLong(1);
        // modificacion jlt 18/09/01
        //ano = rs.getString(2);
        ano = (String) parCons.anoDesde;

        //// System_out.println("SrvConsEdoAgru: poblacion " + pob + " ano " +  ano);

        auxP.insertElementAt(new Long(pob), i);
        //auxP.addElement(new Long(pob));
        auxA.insertElementAt(ano, i);
        //aux.addElement(ano);
        //tasas.addElement(aux);
        i++;
        hay = new Boolean(true);
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

      // calculo las tasas

    }
    tasas.addElement(hay);
    tasas.addElement(auxA);
    tasas.addElement(auxP);

    return tasas;

  } //fin de CalcularTasas

  private String averiguaDeclaracion(Connection con, String enf) throws
      Exception {
    String queryIni = "select CD_TVIGI from SIVE_ENFEREDO where CD_ENFCIE=? ";
    PreparedStatement st = con.prepareStatement(queryIni);
    st.setString(1, enf);
//E    //# // System_out.println(queryIni);
    ResultSet rs = st.executeQuery();
    if (rs.next()) {
      return rs.getString("CD_TVIGI");
    }
    return null;
  }

  private PreparedStatement ponerParamConsulta(PreparedStatement st,
                                               ParamC2 parCons) throws
      Exception {
    int iParam = 1;

    //enfermedad

    st.setString(iParam, parCons.enfermedad);
    iParam++;

    //periodo
    if (parCons.anoDesde.equals(parCons.anoHasta)) {
      st.setString(iParam, parCons.anoDesde);
      iParam++;
      st.setString(iParam, parCons.semDesde);
      iParam++;
      st.setString(iParam, parCons.semHasta);
      iParam++;
    }
    else {
      st.setString(iParam, parCons.anoDesde);
      iParam++;
      st.setString(iParam, parCons.semDesde);
      iParam++;
      st.setString(iParam, parCons.anoDesde);
      iParam++;
      st.setString(iParam, parCons.anoHasta);
      iParam++;
      st.setString(iParam, parCons.anoHasta);
      iParam++;
      st.setString(iParam, parCons.semHasta);
      iParam++;
    }

    // Parametros de los criterios de seleccion
    if (!parCons.area.equals("")) {
      st.setString(iParam, parCons.area);
      iParam++;
    }
    if (!parCons.distrito.equals("")) {
      st.setString(iParam, parCons.distrito);
      iParam++;
    }
    if (!parCons.zbs.equals("")) {
      st.setString(iParam, parCons.zbs);
      iParam++;
    }
    if (!parCons.provincia.equals("")) {
      st.setString(iParam, parCons.provincia);
      iParam++;
    }
    if (!parCons.municipio.equals("")) {
      st.setString(iParam, parCons.municipio);
      iParam++;
    }

    return st;

  }

  // JRM: Intenta corregir error cuando se hace st.close
  private void ponerParamConsultaJRM(PreparedStatement st,
                                     ParamC2 parCons) throws Exception {
    int iParam = 1;

    //enfermedad

    st.setString(iParam, parCons.enfermedad);
    iParam++;

    //periodo
    if (parCons.anoDesde.equals(parCons.anoHasta)) {
      st.setString(iParam, parCons.anoDesde);
      iParam++;
      st.setString(iParam, parCons.semDesde);
      iParam++;
      st.setString(iParam, parCons.semHasta);
      iParam++;
    }
    else {
      st.setString(iParam, parCons.anoDesde);
      iParam++;
      st.setString(iParam, parCons.semDesde);
      iParam++;
      st.setString(iParam, parCons.anoDesde);
      iParam++;
      st.setString(iParam, parCons.anoHasta);
      iParam++;
      st.setString(iParam, parCons.anoHasta);
      iParam++;
      st.setString(iParam, parCons.semHasta);
      iParam++;
    }

    // Parametros de los criterios de seleccion
    if (!parCons.area.equals("")) {
      st.setString(iParam, parCons.area);
      iParam++;
    }
    if (!parCons.distrito.equals("")) {
      st.setString(iParam, parCons.distrito);
      iParam++;
    }
    if (!parCons.zbs.equals("")) {
      st.setString(iParam, parCons.zbs);
      iParam++;
    }
    if (!parCons.provincia.equals("")) {
      st.setString(iParam, parCons.provincia);
      iParam++;
    }
    if (!parCons.municipio.equals("")) {
      st.setString(iParam, parCons.municipio);
      iParam++;
    }
  }

  private Vector situarResultSet(String[] semana, ResultSet rs, int tipo) throws
      Exception {
    Vector reg = new Vector();
    Integer i;
    String semRes;
    String anoRes;
//E    //# // System_out.println("Debo SITUAR EL RESULTSET en:" + semana[0] + "  " + semana[1]);

    while (rs.next()) {

//E      //# // System_out.println("Entro en el whike desituarResultSet");

      reg = new Vector();
      reg.addElement(rs.getString("CD_ANOEPI"));
      anoRes = (String) reg.elementAt(0);
      reg.addElement(rs.getString("CD_SEMEPI"));
      semRes = (String) reg.elementAt(1);
      i = new Integer(rs.getInt(3));
      reg.addElement(i);
      //if (tipo == erwCASOS_EDO_AGRU_NO_EQ_NI_CEN)
      //  reg.addElement(rs.getString("CD_MUN"));

//E      //# // System_out.println("RESULTSET " + reg.elementAt(0).toString() + "  " + reg.elementAt(1).toString() + "  "  + reg.elementAt(2).toString() );

      if ( (anoRes.compareTo(semana[0]) == 0) &&
          (semRes.compareTo(semana[1]) >= 0)) {
        //(semana[1].compareTo((String)reg.elementAt(1))<=0)) {
//E         //# // System_out.println("He SITUADO (ano y sem) el resultSet en " + reg.elementAt(0).toString() + "  " + reg.elementAt(1).toString() + "  "  + reg.elementAt(2).toString() );
        return reg;
      }
      if (anoRes.compareTo(semana[0]) > 0) {
        //(semana[1].compareTo((String)reg.elementAt(1))<=0)) {
//E         //# // System_out.println("He SITUADO (ano) el resultSet en " + reg.elementAt(0).toString() + "  " + reg.elementAt(1).toString() + "  "  + reg.elementAt(2).toString() );
        return reg;
      }
    }
    // No ha encontrado nada en el resultSet
    // No se si se puede dar este caso, pero por si acaso
    reg.addElement(new String());
    reg.addElement(new String());
    reg.addElement(new Integer(0));
//E    //# // System_out.println("No ha encontrado nada en el RESULTSET" );
    return reg;
  }

  private Vector obtenerGuiaSemana(Connection con, ParamC2 parCons) throws
      Exception {
    Vector vec = new Vector();
    String sem[];
    String queryGuia = new String();
    PreparedStatement st;
    if (parCons.anoDesde.equals(parCons.anoHasta)) {
      queryGuia = "select CD_ANOEPI, CD_SEMEPI from SIVE_SEMANA_EPI "
          + " where CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? "
          + " order by CD_ANOEPI, CD_SEMEPI ";
      st = con.prepareStatement(queryGuia);
      st.setString(1, parCons.semDesde);
      st.setString(2, parCons.semHasta);
      st.setString(3, parCons.anoDesde);
    }
    else {
      queryGuia = "select CD_ANOEPI, CD_SEMEPI from SIVE_SEMANA_EPI "
          + " where ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?)) "
          + " order by CD_ANOEPI, CD_SEMEPI ";
      st = con.prepareStatement(queryGuia);
      st.setString(1, parCons.anoDesde);
      st.setString(2, parCons.semDesde);
      st.setString(3, parCons.anoDesde);
      st.setString(4, parCons.anoHasta);
      st.setString(5, parCons.anoHasta);
      st.setString(6, parCons.semHasta);
    }

//E    //# // System_out.println(queryGuia);
//E    //# // System_out.println("AnoHasta=" +  parCons.anoHasta);
//E    //# // System_out.println("SemHasta=" +  parCons.semHasta);

    ResultSet rs = st.executeQuery();
    while (rs.next()) {
      sem = new String[2];
      sem[0] = rs.getString("CD_ANOEPI");
      sem[1] = rs.getString("CD_SEMEPI");
      vec.addElement(sem);
    }
//E    //# // System_out.println("Obtener semanas : " + vec.size());
    return vec;
  }

  private DataC2 agruparSemanas(int iAgru, String[] anos, String[] sems,
                                int[] casos, int criterio) {
    String desde = anos[0] + "    " + sems[0];
    String hasta = anos[iAgru] + "    " + sems[iAgru];
    int numCasos = 0;
    for (int j = 0; j <= iAgru; j++) {
      Integer intCasos = new Integer(casos[j]);
      numCasos = numCasos + intCasos.intValue();
    }
    return new DataC2(desde, hasta, numCasos);

    /*
         switch (criterio){
      case erwCASOS_EDO_AGRU_NO_EQ_NI_CEN:
        break;
      case erwCASOS_EDO_AGRU_EQ:
        break;
      case erwCASOS_EDO_AGRU_CEN:
        break;
      case erwCASOS_EDO_AGRU_EQ_Y_CEN:
        break;
         }
     */

  }

}
