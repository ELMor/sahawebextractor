package bipcuadro;

import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import bidata.DataBrote;
import bidial.DialogInvesInfor;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CMessage;
import capp.CPanel;
import obj.CFechaSimple;
import obj.CHora;

public class PanelBICuadro
    extends CPanel {

  //para guardar el estado anterior
  private String PI = "";
  ResourceBundle res;
  private String CC = "";

  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  protected int modoOperacion = modoALTA;

  // dialog que contiene a este panel
  DialogInvesInfor dlgB;

  Panel panel = new Panel();

  // controles
  XYLayout xYLayout = new XYLayout();
  XYLayout xYLayout2 = new XYLayout();

  Label lFIS = new Label();
  Label lFISUC = new Label();
  Label lPI = new Label();
  Label lFISPC = new Label();
  Label lMinPI = new Label();
  Label lDCC = new Label();

  CheckboxGroup checkGroupPI = new CheckboxGroup();
  CheckboxGroup checkGroupDCC = new CheckboxGroup();

  CFechaSimple fechaPC = new CFechaSimple("N");
  CFechaSimple fechaUC = new CFechaSimple("N");
  CHora horaPC = new CHora("N");
  CHora horaUC = new CHora("N");

  focusAdapter focusAdap = null;
  focusAdapterHora focusAdapHora = null;
  itemListener item = null;

  TextField txtMinPI = new TextField();
  TextField txtMaxPI = new TextField();
  Label lMaxPI = new Label();
  TextField txtMedPI = new TextField();
  Label lMedPI = new Label();
  CheckboxControl checkDPI = new CheckboxControl();
  CheckboxControl checkHPI = new CheckboxControl();
  TextField txtMinDCC = new TextField();
  Label lMinDCC = new Label();
  CheckboxControl checkDDCC = new CheckboxControl();
  TextField txtMedDCC = new TextField();
  Label lMedDCC = new Label();
  CheckboxControl checkHDCC = new CheckboxControl();
  TextField txtMaxDCC = new TextField();
  Label lMaxDCC = new Label();

  // constructor
  public PanelBICuadro(DialogInvesInfor dlg,
                       int modo,
                       Hashtable hsCompleta) {

    try {
      res = ResourceBundle.getBundle("bipcuadro.Res" + dlg.getCApp().getIdioma());
      setApp(dlg.getCApp());
      dlgB = dlg;

      jbInit();

      //por defecto HORAS
      checkHPI.setChecked(true);
      checkDPI.setChecked(false);
      checkHDCC.setChecked(true);
      checkDDCC.setChecked(false);

      PI = "H";
      CC = "H";

      modoOperacion = modo;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(270); //(280);
    xYLayout.setWidth(735); //(745);

    xYLayout2.setHeight(460);
    xYLayout2.setWidth(600);

    panel.setSize(new Dimension(735, 270));
    panel.setLayout(xYLayout2);

    fechaPC.setName("fechaPC");
    fechaUC.setName("fechaUC");
    horaPC.setName("horaPC");
    horaUC.setName("horaUC");
    checkHPI.setName("horasPI");
    checkHDCC.setName("horasCC");
    checkDPI.setName("diasPI");
    checkDDCC.setName("diasCC");

    focusAdap = new focusAdapter(this);
    focusAdapHora = new focusAdapterHora(this);
    item = new itemListener(this);

    checkHPI.addItemListener(item);
    checkHDCC.addItemListener(item);
    checkDPI.addItemListener(item);
    checkDDCC.addItemListener(item);

    fechaPC.addFocusListener(focusAdap);
    horaPC.addFocusListener(focusAdapHora);
    fechaUC.addFocusListener(focusAdap);
    horaUC.addFocusListener(focusAdapHora);

    lFIS.setText(res.getString("lFIS.Text"));
    lFISUC.setForeground(Color.black);
    lFISUC.setFont(new Font("Dialog", 0, 12));
    lFISUC.setText(res.getString("lFISUC.Text"));
    lPI.setForeground(Color.black);
    lPI.setFont(new Font("Dialog", 0, 12));
    lPI.setText(res.getString("lPI.Text"));
    lMinPI.setForeground(Color.black);
    lMinPI.setFont(new Font("Dialog", 0, 12));
    lMinPI.setText(res.getString("lMinPI.Text"));
    lMaxPI.setForeground(Color.black);
    lMaxPI.setFont(new Font("Dialog", 0, 12));
    lMaxPI.setText(res.getString("lMaxPI.Text"));
    lMedPI.setForeground(Color.black);
    lMedPI.setFont(new Font("Dialog", 0, 12));
    lMedPI.setText(res.getString("lMedPI.Text"));

    checkDPI.setCheckboxGroup(checkGroupPI);
    checkDPI.setLabel(res.getString("checkDPI.Label"));
    checkHPI.setCheckboxGroup(checkGroupPI);
    checkHPI.setLabel(res.getString("checkHPI.Label"));
    lDCC.setForeground(Color.black);
    lDCC.setFont(new Font("Dialog", 0, 12));
    lDCC.setText(res.getString("lDCC.Text"));
    lMinDCC.setForeground(Color.black);
    lMinDCC.setFont(new Font("Dialog", 0, 12));
    lMinDCC.setText(res.getString("lMinPI.Text"));
    checkDDCC.setCheckboxGroup(checkGroupDCC);
    checkDDCC.setLabel(res.getString("checkDPI.Label"));

    lMedDCC.setForeground(Color.black);
    lMedDCC.setFont(new Font("Dialog", 0, 12));
    lMedDCC.setText(res.getString("lMedPI.Text"));
    checkHDCC.setCheckboxGroup(checkGroupDCC);
    checkHDCC.setLabel(res.getString("checkHPI.Label"));
    lMaxDCC.setForeground(Color.black);
    lMaxDCC.setFont(new Font("Dialog", 0, 12));
    lMaxDCC.setText(res.getString("lMaxDCC.Text"));
    lFISPC.setText(res.getString("lFISPC.Text"));

    this.setLayout(xYLayout);

    this.add(panel, new XYConstraints(0, 0, 735, 270)); //735,280
    panel.add(lFIS, new XYConstraints(9, 20, 132, -1));
    panel.add(lFISPC, new XYConstraints(9, 50, 80, -1));
    panel.add(fechaPC, new XYConstraints(100, 50, 100, -1));
    panel.add(horaPC, new XYConstraints(205, 50, 60, -1));

    panel.add(lFISUC, new XYConstraints(300, 50, -1, -1));
    panel.add(fechaUC, new XYConstraints(400, 50, 100, -1));
    panel.add(horaUC, new XYConstraints(505, 50, 60, -1));

    panel.add(lPI, new XYConstraints(9, 85, 147, -1));
    panel.add(lMinPI, new XYConstraints(9, 115, 58, -1));
    panel.add(txtMinPI, new XYConstraints(71, 115, 50, -1));
    panel.add(lMaxPI, new XYConstraints(146, 115, 58, -1));
    panel.add(txtMaxPI, new XYConstraints(208, 115, 50, -1));
    panel.add(lMedPI, new XYConstraints(283, 115, 58, -1));
    panel.add(txtMedPI, new XYConstraints(345, 115, 50, -1));
    panel.add(checkDPI, new XYConstraints(455, 115, -1, -1));
    panel.add(checkHPI, new XYConstraints(540, 115, -1, -1));
    panel.add(lDCC, new XYConstraints(9, 150, 221, -1));
    panel.add(lMinDCC, new XYConstraints(9, 180, 58, -1));
    panel.add(txtMinDCC, new XYConstraints(71, 180, 50, -1));
    panel.add(lMaxDCC, new XYConstraints(146, 180, 58, -1));
    panel.add(txtMaxDCC, new XYConstraints(208, 180, 50, -1));
    panel.add(lMedDCC, new XYConstraints(283, 180, 58, -1));
    panel.add(txtMedDCC, new XYConstraints(345, 180, 50, -1));
    panel.add(checkDDCC, new XYConstraints(455, 180, -1, -1));
    panel.add(checkHDCC, new XYConstraints(540, 180, -1, -1));

    setBorde(false);

  }

  public DataBrote recogerDatos(DataBrote resul) {

    //si fecha informada, enviar tb la hora
    resul.insert("FC_ISINPRIMC_F", fechaPC.getText().trim());
    if (!fechaPC.getText().trim().equals("")) {
      resul.insert("FC_ISINPRIMC_H", horaPC.getText().trim());
    }
    resul.insert("FC_FSINPRIMC_F", fechaUC.getText().trim());
    if (!fechaUC.getText().trim().equals("")) {
      resul.insert("FC_FSINPRIMC_H", horaUC.getText().trim());

    }

    if (checkDPI.isChecked()) {
      resul.insert("IT_PERIN", "D");
      resul.insert("NM_PERINMIN", DiasAHoras(txtMinPI.getText().trim()));
      resul.insert("NM_PERINMAX", DiasAHoras(txtMaxPI.getText().trim()));
      resul.insert("NM_PERINMED", DiasAHoras(txtMedPI.getText().trim()));
    }
    else {
      resul.insert("IT_PERIN", "H");
      resul.insert("NM_PERINMIN", txtMinPI.getText().trim());
      resul.insert("NM_PERINMAX", txtMaxPI.getText().trim());
      resul.insert("NM_PERINMED", txtMedPI.getText().trim());
    }

    if (checkDDCC.isChecked()) {
      resul.insert("IT_DCUAC", "D");
      resul.insert("NM_DCUACMIN", DiasAHoras(txtMinDCC.getText().trim()));
      resul.insert("NM_DCUACMAX", DiasAHoras(txtMaxDCC.getText().trim()));
      resul.insert("NM_DCUACMED", DiasAHoras(txtMedDCC.getText().trim()));
    }
    else {
      resul.insert("IT_DCUAC", "H");
      resul.insert("NM_DCUACMIN", txtMinDCC.getText().trim());
      resul.insert("NM_DCUACMAX", txtMaxDCC.getText().trim());
      resul.insert("NM_DCUACMED", txtMedDCC.getText().trim());
    }
    return resul;

  }

//return: false, es negativo o no numero
//return: true, es ok
  boolean isNum(String Numero) {
    try {
      Integer I = new Integer(Numero);
      int i = I.intValue();
      if (i < 0) {
        return false;
      }
    }
    catch (Exception e) {
      return false;
    }
    return true;
  }

  private void ShowError(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgERROR, sMessage);
    msgBox.show();
    msgBox = null;
  }

  public boolean validarDatos() {

    boolean correcto = true;
    /*
        //1. perdidas de foco
        //Cambio de orden: primero Numericos - pasar a horas - long
        //2 Valores numericos
        if (!txtMaxPI.getText().equals("")){
          boolean b = isNum(txtMaxPI.getText());
          if (!b){
             ShowError("El campo Periodo de Incubaci�n M�ximo debe ser un valor Entero Positivo");
             txtMaxPI.setText("");
             txtMaxPI.requestFocus();
            return false;
          }
        }
        if (!txtMedPI.getText().equals("")){
          boolean b = isNum(txtMedPI.getText());
          if (!b){
             ShowError("El campo Periodo de Incubaci�n Mediana debe ser un valor Entero Positivo");
             txtMedPI.setText("");
             txtMedPI.requestFocus();
            return false;
          }
        }
        if (!txtMinPI.getText().equals("")){
          boolean b = isNum(txtMinPI.getText());
          if (!b){
             ShowError("El campo Periodo de Incubaci�n M�nimo debe ser un valor Entero Positivo");
             txtMinPI.setText("");
             txtMinPI.requestFocus();
            return false;
          }
        }
        if (!txtMaxDCC.getText().equals("")){
          boolean b = isNum(txtMaxDCC.getText());
          if (!b){
             ShowError("El campo Duraci�n Cuadro Cl�nico M�ximo debe ser un valor Entero Positivo");
             txtMaxDCC.setText("");
             txtMaxDCC.requestFocus();
            return false;
          }
        }
        if (!txtMedDCC.getText().equals("")){
          boolean b = isNum(txtMedDCC.getText());
          if (!b){
             ShowError("El campo Duraci�n Cuadro Cl�nico Mediana debe ser un valor Entero Positivo");
             txtMedDCC.setText("");
             txtMedDCC.requestFocus();
            return false;
          }
        }
        if (!txtMinDCC.getText().equals("")){
          boolean b = isNum(txtMinDCC.getText());
          if (!b){
             ShowError("El campo Duraci�n Cuadro Cl�nico M�nimo debe ser un valor Entero Positivo");
             txtMinDCC.setText("");
             txtMinDCC.requestFocus();
            return false;
          }
        }
        //3 Longitud
        //pasar TODO A HORAS, como se guardara en BD  *************
              //P INCUBACION
        if (checkDPI.isChecked()){
          txtMaxPI.setText(DiasAHoras(txtMaxPI.getText().trim()));
          txtMinPI.setText(DiasAHoras(txtMinPI.getText().trim()));
          txtMedPI.setText(DiasAHoras(txtMedPI.getText().trim()));
          checkHPI.setChecked(true);
          checkDPI.setChecked(false);
        }
           //D CUADRO
        if (checkDDCC.isChecked()){
          txtMaxDCC.setText(DiasAHoras(txtMaxDCC.getText().trim()));
          txtMinDCC.setText(DiasAHoras(txtMinDCC.getText().trim()));
          txtMedDCC.setText(DiasAHoras(txtMedDCC.getText().trim()));
          checkHDCC.setChecked(true);
          checkDDCC.setChecked(false);
        }
        //******
          if (txtMaxPI.getText().length() > 6){
          ShowError("El campo M�ximo Periodo de Incubaci�n excede de 999999 horas");
           txtMaxPI.setText("");
           this .requestFocus();
           txtMaxPI.requestFocus();
           return false;
         }
         if (txtMedPI.getText().length()>6){
          ShowError("El campo Mediana Periodo de Incubaci�n excede de 999999 horas");
           txtMedPI.setText("");
           this .requestFocus();
           txtMedPI.requestFocus();
           return false;
         }
         if (txtMinPI.getText().length()>6){
          ShowError("El campo M�nimo Periodo de Incubaci�n excede de 999999 horas");
           txtMinPI.setText("");
           this .requestFocus();
           txtMinPI.requestFocus();
           return false;
         }
         if (txtMaxDCC.getText().length()>6){
          ShowError("El campo M�ximo Duraci�n Cuadro Cl�nico excede de 999999 horas");
           txtMaxDCC.setText("");
           this .requestFocus();
           txtMaxDCC.requestFocus();
           return false;
         }
         if (txtMedDCC.getText().length()>6){
          ShowError("El campo Media Duraci�n Cuadro Cl�nico excede de 999999 horas");
           txtMedDCC.setText("");
           this .requestFocus();
           txtMedDCC.requestFocus();
           return false;
         }
         if (txtMinDCC.getText().length()>6){
          ShowError("El campo M�nimo Duraci�n Cuadro Cl�nico excede de 999999 horas");
           txtMinDCC.setText("");
           this .requestFocus();
           txtMinDCC.requestFocus();
           return false;
         }
         //4. Obligatorios
      */

     if (!ComprobarMaximos(0, "PI")) {
       return false;
     }
    if (!ComprobarMaximos(0, "CC")) {
      return false;
    }
    return correcto;

  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICACION:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:

        enableControls(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  private void enableControls(boolean en) {

    fechaPC.setEnabled(en);
    fechaUC.setEnabled(en);
    horaPC.setEnabled(en);
    horaUC.setEnabled(en);
    txtMaxPI.setEnabled(en);
    txtMedPI.setEnabled(en);
    txtMinPI.setEnabled(en);
    txtMinDCC.setEnabled(en);
    txtMaxDCC.setEnabled(en);
    txtMedDCC.setEnabled(en);
    checkDDCC.setEnabled(en);
    checkDPI.setEnabled(en);
    checkHDCC.setEnabled(en);
    checkHPI.setEnabled(en);

  }

  public void rellena(DataBrote data) {

    String fcFIPC = ""; //pareja, fecha-hora
    if (data.getFC_ISINPRIMC_F() != null) {
      fcFIPC = data.getFC_ISINPRIMC_F();
    }
    fechaPC.setText(fcFIPC);
    if (data.getFC_ISINPRIMC_H() != null) {
      fcFIPC = data.getFC_ISINPRIMC_H();
    }
    horaPC.setText(fcFIPC);

    String fcFIUC = "";
    if (data.getFC_FSINPRIMC_F() != null) {
      fcFIUC = data.getFC_FSINPRIMC_F();
    }
    fechaUC.setText(fcFIUC);
    if (data.getFC_FSINPRIMC_H() != null) {
      fcFIUC = data.getFC_FSINPRIMC_H();
    }
    horaUC.setText(fcFIUC);

    //PERIODO DE INCUBACION
    String it = "";
    String nmPI = "";
    if (data.getIT_PERIN() != null) {
      it = data.getIT_PERIN();
    }
    if (it.equals("H")) {
      checkHPI.setChecked(true);
      checkDPI.setChecked(false);

      if (data.getNM_PERINMIN() != null) {
        nmPI = data.getNM_PERINMIN();
      }
      txtMinPI.setText(nmPI);
      if (data.getNM_PERINMAX() != null) {
        nmPI = data.getNM_PERINMAX();
      }
      txtMaxPI.setText(nmPI);
      if (data.getNM_PERINMED() != null) {
        nmPI = data.getNM_PERINMED();
      }
      txtMedPI.setText(nmPI);
    }
    else if (it.equals("D")) {
      checkHPI.setChecked(false);
      checkDPI.setChecked(true);

      if (data.getNM_PERINMIN() != null) {
        nmPI = data.getNM_PERINMIN();
      }
      txtMinPI.setText(HorasADias(nmPI));
      if (data.getNM_PERINMAX() != null) {
        nmPI = data.getNM_PERINMAX();
      }
      txtMaxPI.setText(HorasADias(nmPI));
      if (data.getNM_PERINMED() != null) {
        nmPI = data.getNM_PERINMED();
      }
      txtMedPI.setText(HorasADias(nmPI));
    }

    //DURACION CUADRO CLINICO
    String nmDCC = "";
    if (data.getIT_DCUAC() != null) {
      it = data.getIT_DCUAC();
    }
    if (it.equals("H")) {
      checkHDCC.setChecked(true);
      checkDDCC.setChecked(false);

      if (data.getNM_DCUACMIN() != null) {
        nmDCC = data.getNM_DCUACMIN();
      }
      txtMinDCC.setText(nmDCC);
      if (data.getNM_DCUACMAX() != null) {
        nmDCC = data.getNM_DCUACMAX();
      }
      txtMaxDCC.setText(nmDCC);
      if (data.getNM_DCUACMED() != null) {
        nmDCC = data.getNM_DCUACMED();
      }
      txtMedDCC.setText(nmDCC);
    }
    else if (it.equals("D")) {
      checkHDCC.setChecked(false);
      checkDDCC.setChecked(true);

      if (data.getNM_DCUACMIN() != null) {
        nmDCC = data.getNM_DCUACMIN();
      }
      txtMinDCC.setText(HorasADias(nmDCC));
      if (data.getNM_DCUACMAX() != null) {
        nmDCC = data.getNM_DCUACMAX();
      }
      txtMaxDCC.setText(HorasADias(nmDCC));
      if (data.getNM_DCUACMED() != null) {
        nmDCC = data.getNM_DCUACMED();
      }
      txtMedDCC.setText(HorasADias(nmDCC));
    }

    //backup
    if (checkHDCC.isChecked()) {
      CC = "H";
    }
    else {
      CC = "D";
    }
    if (checkHPI.isChecked()) {
      PI = "H";
    }
    else {
      PI = "D";

      /*//P Incubacion
         //se guarda siempre en horas
         //Se presenta en horas si max < 96 h En dias si max > 96 h
         String it = "";
         String nmPI = "";
         if (data.getIT_PERIN() != null) {
      it = data.getIT_PERIN();//sera H
      if (data.getNM_PERINMAX() != null) nmPI = data.getNM_PERINMAX();
      Integer I = new Integer(nmPI);
      if (I.intValue() > 96){ //presentar en dias TODO
          checkHPI.setChecked(false);
          checkDPI.setChecked(true);
          txtMaxPI.setText(HorasADias(nmPI));
          if (data.getNM_PERINMED() != null) {
            nmPI = data.getNM_PERINMED();
            txtMedPI.setText(HorasADias(nmPI));
          }
          if (data.getNM_PERINMIN() != null) {
            nmPI = data.getNM_PERINMIN();
            txtMinPI.setText(HorasADias(nmPI));
          }
      }
      else{ //presentar en horas
          if (data.getNM_PERINMAX() != null) {
            nmPI = data.getNM_PERINMAX();
            txtMaxPI.setText(nmPI);
          }
          if (data.getNM_PERINMED() != null) {
            nmPI = data.getNM_PERINMED();
            txtMedPI.setText(nmPI);
          }
          if (data.getNM_PERINMIN() != null) {
            nmPI = data.getNM_PERINMIN();
            txtMinPI.setText(nmPI);
          }
      }
         }//it_perin
         //D Cuadro
         //se guarda siempre en horas
         //Se presenta en horas si max < 96 h En dias si max > 96 h
         if (data.getIT_DCUAC() != null) {
      it = data.getIT_DCUAC();//sera H
      if (data.getNM_DCUACMAX() != null) nmPI = data.getNM_DCUACMAX();
      Integer I = new Integer(nmPI);
      if (I.intValue() > 96){ //presentar en dias TODO
          checkHDCC.setChecked(false);
          checkDDCC.setChecked(true);
          txtMaxDCC.setText(HorasADias(nmPI));
          if (data.getNM_DCUACMED() != null) {
            nmPI = data.getNM_DCUACMED();
            txtMedDCC.setText(HorasADias(nmPI));
          }
          if (data.getNM_DCUACMIN() != null) {
            nmPI = data.getNM_DCUACMIN();
            txtMinDCC.setText(HorasADias(nmPI));
          }
      }
      else{ //presentar en horas
          if (data.getNM_DCUACMAX() != null) {
            nmPI = data.getNM_DCUACMAX();
            txtMaxDCC.setText(nmPI);
          }
          if (data.getNM_DCUACMED() != null) {
            nmPI = data.getNM_DCUACMED();
            txtMedDCC.setText(nmPI);
          }
          if (data.getNM_DCUACMIN() != null) {
            nmPI = data.getNM_DCUACMIN();
            txtMinDCC.setText(nmPI);
          }
      }
         }//IT_DCUAC    */

    }
  } //fin

//PERIODO INCUBACION
  void fechaPC_focusLost() {
    fechaPC.ValidarFecha();
    if (fechaPC.getValid().equals("N")) {
      fechaPC.setText(fechaPC.getFecha()); //vacia
      horaPC.setText("");
    }
    else if (fechaPC.getValid().equals("S")) {
      fechaPC.setText(fechaPC.getFecha());
      if (horaPC.getText().equals("")) {
        horaPC.setText("00:00");
      }
    }
  }

  void horaPC_focusLost() {
    horaPC.ValidarFecha();
    if (horaPC.getValid().equals("N")) {
      if (fechaPC.getText().equals("")) {
        horaPC.setText("");
      }
      else {
        horaPC.setText("00:00");
      }
    }
    else if (horaPC.getValid().equals("S")) {
      horaPC.setText(horaPC.getFecha());
    }
  }

//DURACION CUADRO CLINICO
  void fechaUC_focusLost() {
    fechaUC.ValidarFecha();
    if (fechaUC.getValid().equals("N")) {
      fechaUC.setText(fechaUC.getFecha()); //vacia
      horaUC.setText("");
    }
    else if (fechaUC.getValid().equals("S")) {
      fechaUC.setText(fechaUC.getFecha());
      if (horaUC.getText().equals("")) {
        horaUC.setText("00:00");
      }
    }
  }

  void horaUC_focusLost() {
    horaUC.ValidarFecha();
    if (horaUC.getValid().equals("N")) {
      if (fechaUC.getText().equals("")) {
        horaUC.setText("");
      }
      else {
        horaUC.setText("00:00");
      }
    }
    else if (horaUC.getValid().equals("S")) {
      horaUC.setText(horaUC.getFecha());
    }
  }

// Comprueba que todos Numericos positivos y su longitud
//-1: es con el anterior de backup
//0: con el actual
  boolean ComprobarMaximos(int ActualoAnterior, String PICC) {

    if (PICC.equals("PI")) {

      if (!txtMaxPI.getText().equals("")) {
        boolean b = isNum(txtMaxPI.getText());
        if (!b) {
          ShowError(res.getString("msg1.Text"));
          txtMaxPI.setText("");
          txtMaxPI.requestFocus();
          return false;
        }
      }

      if (!txtMedPI.getText().equals("")) {
        boolean b = isNum(txtMedPI.getText());
        if (!b) {
          ShowError(res.getString("msg2.Text"));
          txtMedPI.setText("");
          txtMedPI.requestFocus();
          return false;
        }
      }

      if (!txtMinPI.getText().equals("")) {
        boolean b = isNum(txtMinPI.getText());
        if (!b) {
          ShowError(res.getString("msg3.Text"));
          txtMinPI.setText("");
          txtMinPI.requestFocus();
          return false;
        }
      }
    }
    else {
      if (!txtMaxDCC.getText().equals("")) {
        boolean b = isNum(txtMaxDCC.getText());
        if (!b) {
          ShowError(res.getString("msg4.Text"));
          txtMaxDCC.setText("");
          txtMaxDCC.requestFocus();
          return false;
        }
      }

      if (!txtMedDCC.getText().equals("")) {
        boolean b = isNum(txtMedDCC.getText());
        if (!b) {
          ShowError(res.getString("msg5.Text"));
          txtMedDCC.setText("");
          txtMedDCC.requestFocus();
          return false;
        }
      }

      if (!txtMinDCC.getText().equals("")) {
        boolean b = isNum(txtMinDCC.getText());
        if (!b) {
          ShowError(res.getString("msg6.Text"));
          txtMinDCC.setText("");
          txtMinDCC.requestFocus();
          return false;
        }
      }
    }

    boolean consultardias = true;
//P Incubacion ******************
    if (PICC.equals("PI")) {

      //-------------------------------------------------------
      if (ActualoAnterior == -1) { //ie al reves de lo que este
        if (checkHPI.isChecked()) {
          consultardias = true;
        }
        else {
          consultardias = false;
        }
      }
      else { //normal
        if (checkHPI.isChecked()) {
          consultardias = false;
        }
        else {
          consultardias = true;
        }
      } //-----------------------------------------------------

      //si vacio, estan bien
      if (txtMaxPI.getText().trim().equals("") ||
          txtMinPI.getText().trim().equals("") ||
          txtMedPI.getText().trim().equals("")) {
        return true;
      }

      if (consultardias) { //dias
        if ( (new Integer(txtMaxPI.getText()).intValue()) > 41666) {
          ShowError(res.getString("msg7.Text"));
          txtMaxPI.requestFocus();
          return false;
        }
        if ( (new Integer(txtMinPI.getText()).intValue()) > 41666) {
          ShowError(res.getString("msg8.Text"));
          txtMinPI.requestFocus();
          return false;
        }
        if ( (new Integer(txtMedPI.getText()).intValue()) > 41666) {
          ShowError(res.getString("msg9.Text"));
          txtMedPI.requestFocus();
          return false;
        }
      }
      else { //horas
        if ( (new Integer(txtMaxPI.getText()).intValue()) > 999999) {
          ShowError(res.getString("msg10.Text"));
          txtMaxPI.requestFocus();
          return false;
        }
        if ( (new Integer(txtMinPI.getText()).intValue()) > 999999) {
          ShowError(res.getString("msg11.Text"));
          txtMinPI.requestFocus();
          return false;
        }
        if ( (new Integer(txtMedPI.getText()).intValue()) > 999999) {
          ShowError(res.getString("msg12.Text"));
          txtMedPI.requestFocus();
          return false;
        }
      }
      //PI  ****************************
    }
//D Cuadro Clinico ******************
    if (PICC.equals("CC")) {

      //-------------------------------------------------------
      if (ActualoAnterior == -1) { //ie al reves de lo que este
        if (checkHDCC.isChecked()) {
          consultardias = true;
        }
        else {
          consultardias = false;
        }
      }
      else { //normal
        if (checkHDCC.isChecked()) {
          consultardias = false;
        }
        else {
          consultardias = true;
        }
      } //-----------------------------------------------------

      //si vacio, estan bien
      if (txtMaxDCC.getText().trim().equals("") ||
          txtMinDCC.getText().trim().equals("") ||
          txtMedDCC.getText().trim().equals("")) {
        return true;
      }

      if (consultardias) { //dias
        if ( (new Integer(txtMaxDCC.getText()).intValue()) > 41666) {
          ShowError(res.getString("msg13.Text"));
          txtMaxDCC.requestFocus();
          return false;
        }
        if ( (new Integer(txtMinDCC.getText()).intValue()) > 41666) {
          ShowError(res.getString("msg14.Text"));
          txtMinDCC.requestFocus();
          return false;
        }
        if ( (new Integer(txtMedDCC.getText()).intValue()) > 41666) {
          ShowError(res.getString("msg15.Text"));
          txtMedDCC.requestFocus();
          return false;
        }
      }
      else { //horas
        if ( (new Integer(txtMaxDCC.getText()).intValue()) > 999999) {
          ShowError(res.getString("msg16.Text"));
          txtMaxDCC.requestFocus();
          return false;
        }
        if ( (new Integer(txtMinDCC.getText()).intValue()) > 999999) {
          ShowError(res.getString("msg17.Text"));
          txtMinDCC.requestFocus();
          return false;
        }
        if ( (new Integer(txtMedDCC.getText()).intValue()) > 999999) {
          ShowError(res.getString("msg18.Text"));
          txtMedDCC.requestFocus();
          return false;
        }
      }
      //CC  ****************************
    }
    return true;
  }

//se invoca cuando se seleciona horas en PI
  void PIHoras_itemStateChanged(ItemEvent e) {

    if (!ComprobarMaximos( -1, "PI")) { //estaban MAL los dias
      checkDPI.setChecked(true); //volvemos a dias
      //backup: igual
    }
    else { //estaban BIEN los dias
      txtMaxPI.setText(DiasAHoras(txtMaxPI.getText().trim()));
      txtMedPI.setText(DiasAHoras(txtMedPI.getText().trim()));
      txtMinPI.setText(DiasAHoras(txtMinPI.getText().trim()));
      //backup: ahora estamos en horas
      PI = "H";
    }

  }

//se invoca cuando se seleciona dias en PI
  void PIDias_itemStateChanged(ItemEvent e) {
    if (!ComprobarMaximos( -1, "PI")) { //estaban MAL las horas
      checkHPI.setChecked(true); //volvemos a horas
      //backup: igual
    }
    else { //estaban BIEN las horas
      txtMaxPI.setText(HorasADias(txtMaxPI.getText().trim()));
      txtMedPI.setText(HorasADias(txtMedPI.getText().trim()));
      txtMinPI.setText(HorasADias(txtMinPI.getText().trim()));
      //backup: ahora estamos en dias
      PI = "D";
    }

  };

//se invoca cuando se seleciona horas en CC
  void CCHoras_itemStateChanged(ItemEvent e) {
    if (!ComprobarMaximos( -1, "CC")) { //estaban MAL los dias
      checkDDCC.setChecked(true); //volvemos a dias
      //backup: igual
    }
    else { //estaban BIEN los dias
      txtMaxDCC.setText(DiasAHoras(txtMaxDCC.getText().trim()));
      txtMedDCC.setText(DiasAHoras(txtMedDCC.getText().trim()));
      txtMinDCC.setText(DiasAHoras(txtMinDCC.getText().trim()));
      //backup: ahora estamos en horas
      CC = "H";
    }
  }

//se invoca cuando se seleciona dias en CC
  void CCDias_itemStateChanged(ItemEvent e) {
    if (!ComprobarMaximos( -1, "CC")) { //estaban MAL las horas
      checkHDCC.setChecked(true); //volvemos a horas
      //backup: igual
    }
    else { //estaban BIEN las horas
      txtMaxDCC.setText(HorasADias(txtMaxDCC.getText().trim()));
      txtMedDCC.setText(HorasADias(txtMedDCC.getText().trim()));
      txtMinDCC.setText(HorasADias(txtMinDCC.getText().trim()));
      //backup: ahora estamos en dias
      CC = "D";
    }

  };

//En el data el periodo de incubacion y la duracion del cuadro
//clinico se guarda en horas
  private String HorasADias(String h) {
    if (h.trim().equals("")) {
      return ("");
    }

    Integer aux = new Integer(h);
    aux = new Integer(aux.intValue() / 24);
    return aux.toString();
  }

  private String DiasAHoras(String d) {
    if (d.trim().equals("")) {
      return ("");
    }
    Integer aux = new Integer(d);
    aux = new Integer(aux.intValue() * 24);
    return aux.toString();

  }

} //clase

class focusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanelBICuadro adaptee;
  FocusEvent evt;

  focusAdapter(PanelBICuadro adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {
    if ( ( (CFechaSimple) evt.getSource()).getName().equals("fechaPC")) {
      adaptee.fechaPC_focusLost();
    }
    else if ( ( (CFechaSimple) evt.getSource()).getName().equals("fechaUC")) {
      adaptee.fechaUC_focusLost();
    }
  }

}

class focusAdapterHora
    implements java.awt.event.FocusListener, Runnable {
  PanelBICuadro adaptee;
  FocusEvent evt;

  focusAdapterHora(PanelBICuadro adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {

    if ( ( (CHora) evt.getSource()).getName().equals("horaPC")) {
      adaptee.horaPC_focusLost();
    }
    else if ( ( (CHora) evt.getSource()).getName().equals("horaUC")) {
      adaptee.horaUC_focusLost();
    }
  }
}

class itemListener
    implements java.awt.event.ItemListener, Runnable {
  PanelBICuadro adaptee;
  ItemEvent e = null;

  itemListener(PanelBICuadro adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    String name = ( (Component) e.getSource()).getName();

    if (name.equals("horasPI")) {
      adaptee.PIHoras_itemStateChanged(e);
    }
    else if (name.equals("horasCC")) {
      adaptee.CCHoras_itemStateChanged(e);
    }
    else if (name.equals("diasPI")) {
      adaptee.PIDias_itemStateChanged(e);
    }
    else if (name.equals("diasCC")) {
      adaptee.CCDias_itemStateChanged(e);
    }

  }
} //class
