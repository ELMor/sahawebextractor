/**
 * Clase: DatSintCS
 * Paquete: brotes.datos.pansint
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 09/12/1999
 * Analisis Funcional: Punto 2. Mantenimiento Sintomatologia.
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   a un BROTE, necesarios para buscar los SINTOMAS asociados.
 */

package brotes.datos.pansint;

import java.io.Serializable;

public class DatSintCS
    implements Serializable {

  // Datos de la busqueda (SIVE_BROTES)
  private String CD_ANO = "";
  private String NM_ALERBRO = "";
  private String DS_ALERBRO = "";
  private String CD_GRUPO = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  public DatSintCS(String Ano,
                   String AlerBro,
                   String DescBro,
                   String Grupo,
                   String Ope,
                   String FUlt) { // Constructor

    if (Ano != null) {
      CD_ANO = Ano;
    }
    else {
      CD_ANO = "";
    }
    if (AlerBro != null) {
      NM_ALERBRO = AlerBro;
    }
    else {
      NM_ALERBRO = "";
    }
    if (DescBro != null) {
      DS_ALERBRO = DescBro;
    }
    else {
      DS_ALERBRO = "";
    }
    if (Grupo != null) {
      CD_GRUPO = Grupo;
    }
    else {
      CD_GRUPO = "";
    }
    if (Ope != null) {
      CD_OPE = Ope;
    }
    else {
      CD_OPE = "";
    }
    if (FUlt != null) {
      FC_ULTACT = FUlt;
    }
    else {
      FC_ULTACT = "";
    }
  }

  public String getCD_ANO() {
    return CD_ANO;
  }

  public String getNM_ALERBRO() {
    return NM_ALERBRO;
  }

  public String getDS_ALERBRO() {
    return DS_ALERBRO;
  }

  public String getCD_GRUPO() {
    return CD_GRUPO;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public void setCD_OPE(String ope) {
    CD_OPE = ope;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public void setFC_ULTACT(String fultact) {
    FC_ULTACT = fultact;
  }

} // endclass DatSintCS
