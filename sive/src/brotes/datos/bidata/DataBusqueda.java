package brotes.datos.bidata;

import java.io.Serializable;
import java.util.Vector;

public class DataBusqueda
    implements Serializable {
  final int modoMODIFICACION = 4;
  final int modoCONSULTA = 6;

  public String CD_ANO = "";
  public String NM_ALERBRO = "";
  public String DS_ALERTA = "";
  public String IT_VALIDADA = "";
  public String CD_GRUPO = "";
  public String DS_GRUPO = "";
  public String CD_SITALERBRO = "";
  public String CD_NIVEL_1 = "";
  public String CD_NIVEL_2 = "";
  public String DS_NIVEL_1 = "";
  public String DS_NIVEL_2 = "";
  public String CD_E_NOTIF = "";
  public String DS_E_NOTIF = "";
  public String CD_CENTRO = "";
  public String DS_CENTRO = "";
  public String FC_FECHAHORA = "";
  public String FC_FECHAHORA_DESDE = ""; //sin 00:00
  public String FC_FECHAHORA_HASTA = ""; //sin 00:00
  public String FC_ALERBRO = "";
  public int modoOperacion = 0; //  modoMODIFICACION = 4;  modoCONSULTA = 6

  public Vector VN1 = null;
  public Vector VN2 = null;

  public DataBusqueda() {}

  public DataBusqueda(String cd_ano,
                      String nm_alerbro,
                      String it_validada,
                      String ds_alerta,
                      String cd_grupo,
                      String ds_grupo,
                      String cd_sitalerbro,
                      String cd_nivel_1, String ds_nivel_1,
                      String cd_nivel_2, String ds_nivel_2,
                      String cd_e_notif,
                      String ds_e_notif,
                      String cd_centro,
                      String ds_centro,
                      String fc_fechahora,
                      String fc_fechahora_desde,
                      String fc_fechahora_hasta,
                      String fc_alerbro,
                      int modo,
                      Vector vn1, Vector vn2) {
    CD_ANO = cd_ano;
    NM_ALERBRO = nm_alerbro;
    IT_VALIDADA = it_validada;
    DS_ALERTA = ds_alerta;
    CD_GRUPO = cd_grupo;
    DS_GRUPO = ds_grupo;
    CD_SITALERBRO = cd_sitalerbro;
    CD_NIVEL_1 = cd_nivel_1;
    DS_NIVEL_1 = ds_nivel_1;
    CD_NIVEL_2 = cd_nivel_2;
    DS_NIVEL_2 = ds_nivel_2;
    CD_E_NOTIF = cd_e_notif;
    DS_E_NOTIF = ds_e_notif;
    CD_CENTRO = cd_centro;
    DS_CENTRO = ds_centro;
    FC_FECHAHORA = fc_fechahora;
    FC_FECHAHORA_DESDE = fc_fechahora_desde;
    FC_FECHAHORA_HASTA = fc_fechahora_hasta;
    FC_ALERBRO = fc_alerbro;
    modoOperacion = modo;
    VN1 = vn1;
    VN2 = vn2;
  }

  public String getCD_ANO() {
    return CD_ANO;
  }

  public String getNM_ALERBRO() {
    return NM_ALERBRO;
  }

  public String getIT_VALIDADA() {
    return IT_VALIDADA;
  }

  public String getDS_ALERTA() {
    return DS_ALERTA;
  }

  public String getCD_GRUPO() {
    return CD_GRUPO;
  }

  public String getDS_GRUPO() {
    return DS_GRUPO;
  }

  public String getCD_SITALERBRO() {
    return CD_SITALERBRO;
  }

  public String getCD_NIVEL_1() {
    return CD_NIVEL_1;
  }

  public String getDS_NIVEL_1() {
    return DS_NIVEL_1;
  }

  public String getCD_NIVEL_2() {
    return CD_NIVEL_2;
  }

  public String getDS_NIVEL_2() {
    return DS_NIVEL_2;
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getDS_E_NOTIF() {
    return DS_E_NOTIF;
  }

  public String getCD_CENTRO() {
    return CD_CENTRO;
  }

  public String getDS_CENTRO() {
    return DS_CENTRO;
  }

  public String getFC_FECHAHORA() {
    return FC_FECHAHORA;
  }

  public String getFC_FECHAHORA_DESDE() {
    return FC_FECHAHORA_DESDE;
  }

  public String getFC_FECHAHORA_HASTA() {
    return FC_FECHAHORA_HASTA;
  }

  public String getFC_ALERBRO() {
    return FC_ALERBRO;
  }

  public int getmodoOperacion() {
    return modoOperacion;
  }

  public Vector getVN1() {
    return VN1;
  }

  public Vector getVN2() {
    return VN2;
  }
}