package brotes.datos.bidata;

import java.io.Serializable;

public class DataCDDS
    implements Serializable {

  private String CDpadre = "";
  private String CD = "";
  private String DS = "";

  public DataCDDS(String cod, String des) {
    CD = cod;
    DS = des;
  }

  public DataCDDS(String codpadre, String cod, String des) {
    CDpadre = codpadre;
    CD = cod;
    DS = des;
  }

  public String getCDpadre() {
    return CDpadre;
  }

  public String getCD() {
    return CD;
  }

  public String getDS() {
    return DS;
  }
}