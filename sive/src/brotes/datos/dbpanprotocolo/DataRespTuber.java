// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   DataRespTuber.java

package brotes.datos.dbpanprotocolo;

import java.io.Serializable;
import java.sql.Timestamp;

import comun.Fechas;

public class DataRespTuber
    implements Serializable {

  private String NmEdo;
  private String CdTsive;
  private String CdModelo;
  private String NmLinea;
  private String CdPregunta;
  private String DsRespuesta;
  private String ValorLista;
  private String sCodEq;
  private String sCdOpe;
  private String sFcUltAct;

  public DataRespTuber() {
    NmEdo = null;
    CdTsive = "E";
    CdModelo = null;
    NmLinea = null;
    CdPregunta = null;
    DsRespuesta = null;
    ValorLista = null;
    sCodEq = null;
    sCdOpe = null;
    sFcUltAct = null;
  }

  public DataRespTuber(int caso, String modelo, int linea, String pregunta,
                       String respuesta, String valorlista, String equipo) {
    NmEdo = null;
    CdTsive = "E";
    CdModelo = null;
    NmLinea = null;
    CdPregunta = null;
    DsRespuesta = null;
    ValorLista = null;
    sCodEq = null;
    sCdOpe = null;
    sFcUltAct = null;
    setNM_EDO(caso);
    setCD_MODELO(modelo);
    setNM_LINEA(linea);
    setCD_PREGUNTA(pregunta);
    setDS_RESPUESTA(respuesta);
    ValorLista = valorlista;
    setCD_E_NOTIF(equipo);
  }

  public int getNM_EDO() {
    return (new Integer(NmEdo)).intValue();
  }

  public String getCD_TSIVE() {
    String s = "";
    if (CdTsive != null) {
      s = CdTsive;
    }
    return s;
  }

  public String getCD_MODELO() {
    String s = "";
    if (CdModelo != null) {
      s = CdModelo;
    }
    return s;
  }

  public int getNM_LINEA() {
    return (new Integer(NmLinea)).intValue();
  }

  public String getCD_PREGUNTA() {
    String s = "";
    if (CdPregunta != null) {
      s = CdPregunta;
    }
    return s;
  }

  public String getDS_RESPUESTA() {
    return DsRespuesta;
  }

  public String getCodEq() {
    String s = "";
    if (sCodEq != null) {
      s = sCodEq;
    }
    return s;
  }

  public Timestamp getFC_ULTACT_EDOIND() {
    Timestamp ts = new Timestamp(0L);
    if (sFcUltAct != null && !sFcUltAct.equals("")) {
      ts = Fechas.string2Timestamp(sFcUltAct);
    }
    return ts;
  }

  public String getCD_OPE_EDOIND() {
    String s = "";
    if (sCdOpe != null) {
      s = sCdOpe;
    }
    return s;
  }

  public void setNM_EDO(int i) {
    NmEdo = String.valueOf(i);
  }

  public void setCD_TSIVE(String s) {
    CdTsive = "";
    if (s != null) {
      CdTsive = s.trim();
    }
  }

  public void setCD_MODELO(String s) {
    CdModelo = "";
    if (s != null) {
      CdModelo = s.trim();
    }
  }

  public void setNM_LINEA(int i) {
    NmLinea = String.valueOf(i);
  }

  public void setCD_PREGUNTA(String s) {
    CdPregunta = "";
    if (s != null) {
      CdPregunta = s.trim();
    }
  }

  public void setDS_RESPUESTA(String s) {
    DsRespuesta = "";
    if (s != null) {
      DsRespuesta = s.trim();
    }
  }

  public void setCD_E_NOTIF(String s) {
    sCodEq = "";
    if (s != null) {
      sCodEq = s.trim();
    }
  }

  public void setFC_ULTACT_EDOIND(Timestamp ts) {
    sFcUltAct = "";
    if (ts != null) {
      sFcUltAct = Fechas.timestamp2String(ts);
    }
  }

  public void setCD_OPE_EDOIND(String s) {
    sCdOpe = "";
    if (s != null) {
      sCdOpe = s.trim();
    }
  }
}
