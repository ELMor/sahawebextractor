package brotes.datos.confprot2;

import java.io.Serializable;

public class DataLineaItemBrote
    implements Serializable {
  String sCD_TSIVE;
  String sCD_MODELO;
  int iNM_LIN;
  String sCD_PREGUNTA;
  String sCD_PREGUNTA_PC;
  String sIT_OBLIGATORIO;
  String sIT_CONDP;
  int iNM_LIN_PC;
  String sDS_VPREGUNTA_PC;

  public DataLineaItemBrote(String CD_TSIVE, String CD_MODELO, int NM_LIN,
                            String CD_PREGUNTA, String CD_PREGUNTA_PC,
                            String IT_OBLIGATORIO,
                            String IT_CONDP, int NM_LIN_PC,
                            String DS_VPREGUNTA_PC) {

    sCD_TSIVE = CD_TSIVE;
    sCD_MODELO = CD_MODELO;
    iNM_LIN = NM_LIN;
    sCD_PREGUNTA = CD_PREGUNTA;
    sCD_PREGUNTA_PC = CD_PREGUNTA_PC;
    sIT_OBLIGATORIO = IT_OBLIGATORIO;
    sIT_CONDP = IT_CONDP;
    iNM_LIN_PC = NM_LIN_PC;
    sDS_VPREGUNTA_PC = DS_VPREGUNTA_PC;
  }

  public String getCD_TSIVE() {
    return sCD_TSIVE;
  }

  public String getCD_MODELO() {
    return sCD_MODELO;
  }

  public int getNM_LIN() {
    return iNM_LIN;
  }

  public String getCD_PREGUNTA() {
    return sCD_PREGUNTA;
  }

  public String getCD_PREGUNTA_PC() {
    return sCD_PREGUNTA_PC;
  }

  public String getIT_OBLIGATORIO() {
    return sIT_OBLIGATORIO;
  }

  public String getIT_CONDP() {
    return sIT_CONDP;
  }

  public int getNM_LIN_PC() {
    return iNM_LIN_PC;
  }

  public String getDS_VPREGUNTA_PC() {
    return sDS_VPREGUNTA_PC;
  }

}
