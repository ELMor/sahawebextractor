package brotes.datos.confprot2;

import java.io.Serializable;

import capp.CLista;

public class DataMantLineasModBrote
    implements Serializable {
  public String sCodModelo = "";
  public String sDescModelo = "";
  public String sProceso = "";
  public String sTSive = null;
  protected CLista lineasModelo = null;

  public DataMantLineasModBrote() {
  }

  public DataMantLineasModBrote(String CodModelo) {
    sCodModelo = CodModelo;
    sDescModelo = null;
    sProceso = null;
  }

  public DataMantLineasModBrote(String CodModelo,
                                String DescModelo,
                                String Proceso) {
    sCodModelo = CodModelo;
    sDescModelo = DescModelo;
    sProceso = Proceso;
  }

  public DataMantLineasModBrote(String CodModelo,
                                String TSive) {
    sCodModelo = CodModelo;
    sDescModelo = null;
    sProceso = null;
    sTSive = TSive;
  }

  public CLista getLineas() {
    return lineasModelo;
  }

  public void setLineas(CLista lineas) {
    lineasModelo = lineas;
  }
}
