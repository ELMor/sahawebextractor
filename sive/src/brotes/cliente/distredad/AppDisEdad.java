package brotes.cliente.distredad;

import capp2.CApp;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class AppDisEdad
    extends CApp {

  PanDisEdad pan = null;
  public AppDisEdad() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    // query tool
    QueryTool qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", "1999");
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", "1");
    qtBloqueo.putOperator("NM_ALERBRO", "=");
    Lista l = new Lista();
    l.addElement(qtBloqueo);

    Lista vResultado = null;
    try {
      this.getStub().setUrl("servlet/SrvQueryTool");
      vResultado = (Lista)this.getStub().doPost(1, l);
    }
    catch (Exception ex) {}
    ;

    Data miData = (Data) vResultado.elementAt(0);

    Data datos = new Data();
    datos.put("NM_ALERBRO", "1");
    datos.put("CD_ANO", "1997");
    datos.put("CD_GRUPO", "");
    datos.put("DS_BROTE", "Botulismo");
    datos.put("CD_OPE", miData.getString("CD_OPE"));
    datos.put("FC_ULTACT", "03/03/2000 12:33:36"); //miData.getString("FC_ULTACT"));

    setTitulo("Distribución por grupos de edad y sexo");
    PanDisEdad pan = null;
    pan = new PanDisEdad(this, pan.MODIFICACION, datos);
    pan.show();
  }
}
