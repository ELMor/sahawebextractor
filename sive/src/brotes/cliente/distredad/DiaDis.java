package brotes.cliente.distredad;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CPnlCodigo;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * Di�logo a trav�s del cu�l se insertan/modifican/borran datos
 * en tablas de mantenimiento de distribuci�n por grupos de edad y sexo.
 * @autor PDP
 * @version 1.0
 */

public class DiaDis
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  public Data dtDev = null;
  //
  public Data dtDevuelto = new Data();
  //
  public Data dtReg = null;
  //
  Data dtBrote = null;
  //
  public Data dtRegSubT = new Data();
  //
  public Lista lTable = new Lista();
  //

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xYLayout1 = new XYLayout();
  Label lblGrupos = new Label();
  CPnlCodigo pnlGrupos = null;
  Label lblRangos = new Label();
  CPnlCodigo pnlRangos = null;

  //EXP
  Label lblExpuestos = new Label();
  Label lblVEXP = new Label();
  CEntero txtVEXP = new CEntero(4);
  Label lblMEXP = new Label();
  CEntero txtMEXP = new CEntero(4);
  Label lblNCEXP = new Label();
  CEntero txtNCEXP = new CEntero(4);
  //ENF
  Label lblEnfermos = new Label();
  Label lblVENF = new Label();
  CEntero txtVENF = new CEntero(4);
  Label lblMENF = new Label();
  CEntero txtMENF = new CEntero(4);
  Label lblNCENF = new Label();
  CEntero txtNCENF = new CEntero(4);
  //HOS
  Label lblHospitalizados = new Label();
  Label lblVHOS = new Label();
  CEntero txtVHOS = new CEntero(4);
  Label lblMHOS = new Label();
  CEntero txtMHOS = new CEntero(4);
  Label lblNCHOS = new Label();
  CEntero txtNCHOS = new CEntero(4);
  //DEF
  Label lblDefunciones = new Label();
  Label lblVDEF = new Label();
  CEntero txtVDEF = new CEntero(4);
  Label lblMDEF = new Label();
  CEntero txtMDEF = new CEntero(4);
  Label lblNCDEF = new Label();
  CEntero txtNCDEF = new CEntero(4);

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...

  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  // constructor del di�logo DiaDis
  public DiaDis(CApp a, int modoop, Data dtBr, Data dt, Lista lTab) {

    super(a);
    //QueryTool qt1 = null;
    QueryTool qt = null;

    try {
      modoOperacion = modoop;
      dtBrote = dtBr;
      dtDev = dt;
      lTable = lTab;

      // configura el componente de grupos
      qt = new QueryTool();
      qt.putName("SIVE_GEDAD");
      qt.putType("CD_GEDAD", QueryTool.STRING);
      qt.putType("DS_GEDAD", QueryTool.STRING);
      // panel de tipo de GrupoEdad
      pnlGrupos = new CPnlCodigo(a, //applet
                                 this,
                                 qt,
                                 "CD_GEDAD",
                                 "DS_GEDAD",
                                 true,
                                 "Grupos de edad",
                                 "Grupos de edad"); //,this);

      // configura el componente de rangos
      /*
             qt = new QueryTool();
             qt.putName("SIVE_DISTR_GEDAD");
             qt.putType("CD_GEDAD", QueryTool.STRING);
             qt.putType("DS_DIST_EDAD", QueryTool.STRING);
             qt.putType("DIST_EDAD_I", QueryTool.INTEGER);
             qt.putType("DIST_EDAD_F", QueryTool.INTEGER);
             qt.putType("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F", QueryTool.STRING);
           qt.addOrderField("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
       */
      // panel de tipo de GrupoEdad
      pnlRangos = new CPnlCodigo(a, //applet
                                 this,
                                 null,
          "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
          "DS_DIST_EDAD",
          true,
          "Rangos de edad",
          "Rangos de edad");

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(675, 390);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    lblGrupos.setText("Grupos de Edad:");
    lblRangos.setText("Rangos de Edad:");
    lblExpuestos.setText("Expuestos");
    lblVEXP.setText("Varones");
    lblMEXP.setText("Mujeres");
    lblNCEXP.setText("Nc");
    lblEnfermos.setText("Enfermos");
    lblVENF.setText("Varones");
    lblMENF.setText("Mujeres");
    lblNCENF.setText("Nc");
    lblHospitalizados.setText("Hospitalizados");
    lblVHOS.setText("Varones");
    lblMHOS.setText("Mujeres");
    lblNCHOS.setText("Nc");
    lblDefunciones.setText("Defunciones");
    lblVDEF.setText("Varones");
    lblMDEF.setText("Mujeres");
    lblNCDEF.setText("Nc");

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    txtVEXP.setBackground(new Color(255, 255, 150));
    txtMEXP.setBackground(new Color(255, 255, 150));
    txtNCEXP.setBackground(new Color(255, 255, 150));
    txtVENF.setBackground(new Color(255, 255, 150));
    txtMENF.setBackground(new Color(255, 255, 150));
    txtNCENF.setBackground(new Color(255, 255, 150));
    txtVHOS.setBackground(new Color(255, 255, 150));
    txtMHOS.setBackground(new Color(255, 255, 150));
    txtNCHOS.setBackground(new Color(255, 255, 150));
    txtVDEF.setBackground(new Color(255, 255, 150));
    txtMDEF.setBackground(new Color(255, 255, 150));
    txtNCDEF.setBackground(new Color(255, 255, 150));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xYLayout1.setHeight(390);
    xYLayout1.setWidth(675);

    this.add(lblGrupos, new XYConstraints(39, 25, 97, -1));
    this.add(pnlGrupos, new XYConstraints(146, 21, 182, -1));
    this.add(lblRangos, new XYConstraints(342, 25, 97, -1));
    this.add(pnlRangos, new XYConstraints(452, 21, 182, -1));
    this.add(lblExpuestos, new XYConstraints(40, 65, 85, -1));
    this.add(lblVEXP, new XYConstraints(60, 87, 85, -1));
    this.add(txtVEXP, new XYConstraints(150, 87, 85, -1));
    this.add(lblMEXP, new XYConstraints(260, 87, 85, -1));
    this.add(txtMEXP, new XYConstraints(350, 87, 85, -1));
    this.add(lblNCEXP, new XYConstraints(460, 87, 85, -1));
    this.add(txtNCEXP, new XYConstraints(550, 87, 85, -1));
    this.add(lblEnfermos, new XYConstraints(40, 125, 85, -1));
    this.add(lblVENF, new XYConstraints(60, 146, 85, -1));
    this.add(txtVENF, new XYConstraints(150, 146, 85, -1));
    this.add(lblMENF, new XYConstraints(260, 146, 85, -1));
    this.add(txtMENF, new XYConstraints(350, 146, 85, -1));
    this.add(lblNCENF, new XYConstraints(460, 146, 85, -1));
    this.add(txtNCENF, new XYConstraints(550, 146, 85, -1));
    this.add(lblHospitalizados, new XYConstraints(40, 185, 85, -1));
    this.add(lblVHOS, new XYConstraints(60, 206, 85, -1));
    this.add(txtVHOS, new XYConstraints(150, 206, 85, -1));
    this.add(lblMHOS, new XYConstraints(260, 206, 85, -1));
    this.add(txtMHOS, new XYConstraints(350, 206, 85, -1));
    this.add(lblNCHOS, new XYConstraints(460, 206, 85, -1));
    this.add(txtNCHOS, new XYConstraints(550, 206, 85, -1));
    this.add(lblDefunciones, new XYConstraints(40, 245, 85, -1));
    this.add(lblVDEF, new XYConstraints(60, 266, 85, -1));
    this.add(txtVDEF, new XYConstraints(150, 266, 85, -1));
    this.add(lblMDEF, new XYConstraints(260, 266, 85, -1));
    this.add(txtMDEF, new XYConstraints(350, 266, 85, -1));
    this.add(lblNCDEF, new XYConstraints(460, 266, 85, -1));
    this.add(txtNCDEF, new XYConstraints(550, 266, 85, -1));
    this.add(btnAceptar, new XYConstraints(454, 310, 89, -1));
    this.add(btnCancelar, new XYConstraints(548, 310, 89, -1));

    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    // ponemos el t�tulo

    switch (modoOperacion) {
      case modoALTA:
        setTitle("Alta de una nueva distribuci�n por grupos de edad y sexo");
        break;
      case modoMODIFICACION:
        this.setTitle(
            "Modificaci�n de una distribuci�n por grupos de edad y sexo");
        break;
      case modoBAJA:
        this.setTitle("Baja de una distribuci�n por grupos de edad y sexo");
        break;

    }
  }

  // Pone los campos de texto en el estado que les corresponda
  // segun el modo.

  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        if (ExisteGrupo()) {
          pnlGrupos.setEnabled(false);
          pnlRangos.setEnabled(true);
          Data dtT = (Data) lTable.elementAt(0);
          pnlGrupos.setCodigo(dtT);

          QueryTool qt1 = new QueryTool();
          qt1.putName("SIVE_DISTR_GEDAD");
          qt1.putType("CD_GEDAD", QueryTool.STRING);
          qt1.putType("DS_DIST_EDAD", QueryTool.STRING);
          qt1.putType("DIST_EDAD_I", QueryTool.INTEGER);
          qt1.putType("DIST_EDAD_F", QueryTool.INTEGER);
          qt1.putType("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
                      QueryTool.STRING);
          qt1.putWhereType("CD_GEDAD", QueryTool.STRING);
          qt1.putWhereValue("CD_GEDAD", dtT.getString("CD_GEDAD"));
          qt1.putOperator("CD_GEDAD", "=");
          qt1.addOrderField(
              "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
          pnlRangos.setQueryTool(qt1);

        }
        else {
          pnlGrupos.setEnabled(true);
          pnlRangos.setEnabled(true);
          txtVEXP.setEnabled(true);
          txtMEXP.setEnabled(true);
          txtNCEXP.setEnabled(true);
          txtVENF.setEnabled(true);
          txtMENF.setEnabled(true);
          txtNCENF.setEnabled(true);
          txtVHOS.setEnabled(true);
          txtMHOS.setEnabled(true);
          txtNCHOS.setEnabled(true);
          txtVDEF.setEnabled(true);
          txtMDEF.setEnabled(true);
          txtNCDEF.setEnabled(true);
        }

        // Iniciamos a ceros.
        txtVEXP.setText("0");
        txtMEXP.setText("0");
        txtNCEXP.setText("0");
        txtVENF.setText("0");
        txtMENF.setText("0");
        txtNCENF.setText("0");
        txtVHOS.setText("0");
        txtMHOS.setText("0");
        txtNCHOS.setText("0");
        txtVDEF.setText("0");
        txtMDEF.setText("0");
        txtNCDEF.setText("0");
        break;

      case modoMODIFICACION:
        pnlGrupos.setEnabled(false);
        pnlRangos.setEnabled(false);
        txtVEXP.setEnabled(true);
        txtMEXP.setEnabled(true);
        txtNCEXP.setEnabled(true);
        txtVENF.setEnabled(true);
        txtMENF.setEnabled(true);
        txtNCENF.setEnabled(true);
        txtVHOS.setEnabled(true);
        txtMHOS.setEnabled(true);
        txtNCHOS.setEnabled(true);
        txtVDEF.setEnabled(true);
        txtMDEF.setEnabled(true);
        txtNCDEF.setEnabled(true);

        // datos panel principal
        pnlGrupos.setCodigo(dtDev);
        pnlRangos.setCodigo(dtDev);
        txtVEXP.setText(dtDev.getString("NM_V_EXP"));
        txtMEXP.setText(dtDev.getString("NM_M_EXP"));
        txtNCEXP.setText(dtDev.getString("NM_NC_EXP"));
        txtVENF.setText(dtDev.getString("NM_V_ENF"));
        txtMENF.setText(dtDev.getString("NM_M_ENF"));
        txtNCENF.setText(dtDev.getString("NM_NC_ENF"));
        txtVHOS.setText(dtDev.getString("NM_V_HOS"));
        txtMHOS.setText(dtDev.getString("NM_M_HOS"));
        txtNCHOS.setText(dtDev.getString("NM_NC_HOS"));
        txtVDEF.setText(dtDev.getString("NM_V_DEF"));
        txtMDEF.setText(dtDev.getString("NM_M_DEF"));
        txtNCDEF.setText(dtDev.getString("NM_NC_DEF"));

        break;

        // s�lo habilitado aceptar y cancelar
      case modoBAJA:
        pnlGrupos.setEnabled(false);
        pnlRangos.setEnabled(false);
        txtVEXP.setEnabled(false);
        txtMEXP.setEnabled(false);
        txtNCEXP.setEnabled(false);
        txtVENF.setEnabled(false);
        txtMENF.setEnabled(false);
        txtNCENF.setEnabled(false);
        txtVHOS.setEnabled(false);
        txtMHOS.setEnabled(false);
        txtNCHOS.setEnabled(false);
        txtVDEF.setEnabled(false);
        txtMDEF.setEnabled(false);
        txtNCDEF.setEnabled(false);

        // datos panel principal
        pnlGrupos.setCodigo(dtDev);
        pnlRangos.setCodigo(dtDev);
        txtVEXP.setText(dtDev.getString("NM_V_EXP"));
        txtMEXP.setText(dtDev.getString("NM_M_EXP"));
        txtNCEXP.setText(dtDev.getString("NM_NC_EXP"));
        txtVENF.setText(dtDev.getString("NM_V_ENF"));
        txtMENF.setText(dtDev.getString("NM_M_ENF"));
        txtNCENF.setText(dtDev.getString("NM_NC_ENF"));
        txtVHOS.setText(dtDev.getString("NM_V_HOS"));
        txtMHOS.setText(dtDev.getString("NM_M_HOS"));
        txtNCHOS.setText(dtDev.getString("NM_NC_HOS"));
        txtVDEF.setText(dtDev.getString("NM_V_DEF"));
        txtMDEF.setText(dtDev.getString("NM_M_DEF"));
        txtNCDEF.setText(dtDev.getString("NM_NC_DEF"));

        break;
    }
  }

  // Pone el di�logo en modo de espera

  public void Inicializar(int modo) {
    switch (modo) {

      // componente producto accediendo a base de datos
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            if (!ExisteGrupo()) {
              if (pnlGrupos.getDatos() != null) {
                pnlRangos.setEnabled(true);
                pnlGrupos.setEnabled(false);
                Data dtGedad = new Data();
                dtGedad.put("CD_GEDAD",
                            pnlGrupos.getDatos().getString("CD_GEDAD"));

                QueryTool qt1 = new QueryTool();
                qt1.putName("SIVE_DISTR_GEDAD");
                qt1.putType("CD_GEDAD", QueryTool.STRING);
                qt1.putType("DS_DIST_EDAD", QueryTool.STRING);
                qt1.putType("DIST_EDAD_I", QueryTool.INTEGER);
                qt1.putType("DIST_EDAD_F", QueryTool.INTEGER);
                qt1.putType(
                    "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
                    QueryTool.STRING);
                qt1.putWhereType("CD_GEDAD", QueryTool.STRING);
                qt1.putWhereValue("CD_GEDAD", dtGedad.getString("CD_GEDAD"));
                qt1.putOperator("CD_GEDAD", "=");
                qt1.addOrderField(
                    "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
                pnlRangos.setQueryTool(qt1);
              }

              if (pnlRangos.getDatos() == null && pnlGrupos.getDatos() != null) {
                pnlGrupos.setEnabled(true);
              }

              if (pnlRangos.getDatos() == null && pnlGrupos.getDatos() == null) {
                pnlGrupos.setEnabled(true);
                pnlRangos.setEnabled(false);
              }
            }
            else {
              pnlRangos.setEnabled(true);
              pnlGrupos.setEnabled(false);
            }
            break;

          case modoMODIFICACION:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  private boolean ExisteGrupo() {
    boolean bExiste = false;
    for (int j = 0; j < lTable.size() - 1; j++) {
      Data dtTab = (Data) lTable.elementAt(j);
      String sDS_DIST_EDAD = dtTab.getString(
          "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
      if (!sDS_DIST_EDAD.equals("")) {
        bExiste = true;
      }
    }

    if (bExiste) {
      return true;
    }
    else {
      return false;
    }

  }

  private boolean isDataValid() {

    String sMsg = null;
    boolean bCtrl1 = true;
    boolean bCtrl2 = true;
    boolean bCtrl3 = true;
    boolean bCtrl4 = true;
    boolean bCtrl5 = true;
    boolean bCtrl6 = true;
    boolean bCtrl7 = true;
    boolean bCtrl8 = true;
    boolean bCtrl9 = true;
    boolean bCtrl10 = true;
    boolean bCtrl11 = true;
    boolean bCtrl12 = true;
    boolean bCtrl13 = true;
    boolean bCtrl14 = true;

    if (txtVEXP.getText().trim().length() == 0) {
      bCtrl1 = false;
      sMsg = "Debe cumplimentar el n�mero de varones expuestos";
    }

    if (txtMEXP.getText().length() == 0) {
      bCtrl2 = false;
      sMsg = "Debe cumplimentar el n�mero de mujeres expuestas";
    }

    if (txtNCEXP.getText().length() == 0) {
      bCtrl3 = false;
      sMsg = "Debe cumplimentar el n�mero de no conocidos expuestos";
    }

    if (txtVENF.getText().length() == 0) {
      bCtrl4 = false;
      sMsg = "Debe cumplimentar el n�mero de varones enfermos";
    }

    if (txtMENF.getText().length() == 0) {
      bCtrl5 = false;
      sMsg = "Debe cumplimentar el n�mero de mujeres enfermas";
    }

    if (txtNCENF.getText().length() == 0) {
      bCtrl6 = false;
      sMsg =
          "Debe cumplimentar el n�mero de personas enfermas con edad desconocida";
    }

    if (txtVHOS.getText().length() == 0) {
      bCtrl7 = false;
      sMsg = "Debe cumplimentar el n�mero de varones hospitalizados";
    }

    if (txtMHOS.getText().length() == 0) {
      bCtrl8 = false;
      sMsg = "Debe cumplimentar el n�mero de mujeres hospitalizadas";
    }

    if (txtNCHOS.getText().length() == 0) {
      bCtrl9 = false;
      sMsg =
          "Debe cumplimentar el n�mero de personas hospitalizadas con edad desconocida";
    }

    if (txtVDEF.getText().length() == 0) {
      bCtrl10 = false;
      sMsg = "Debe cumplimentar el n�mero de defunciones en varones";
    }

    if (txtMDEF.getText().length() == 0) {
      bCtrl11 = false;
      sMsg = "Debe cumplimentar el n�mero de defunciones en mujeres";
    }

    if (txtNCDEF.getText().length() == 0) {
      bCtrl12 = false;
      sMsg = "Debe cumplimentar el n�mero de defunciones con edad desconocida";
    }

    if (pnlGrupos.getDatos() == null) {
      bCtrl13 = false;
      sMsg = "Debe cumplimentar el c�digo de grupos de enfermos";
    }

    if (pnlRangos.getDatos() == null) {
      bCtrl14 = false;
      sMsg = "Debe cumplimentar los rangos para el grupo de enfermos";
    }

    if ( (bCtrl1 && bCtrl2 && bCtrl3 && bCtrl4 && bCtrl5 && bCtrl6 && bCtrl6 &&
          bCtrl7 && bCtrl8 && bCtrl9 && bCtrl10 && bCtrl1 && bCtrl2 && bCtrl3 &&
          bCtrl4) == false) {
      sMsg = "Debe cumplimentar los datos obligatorios";
    }

    if (sMsg == null) {
      return true;
    }
    else {
      this.getApp().showAdvise(sMsg);
      return false;
    }

  }

  // Comprobaci�n de la existencia de un registro con ya repetido
  private boolean ComprobarRepeticion() {
    boolean bRepe = false;
    for (int u = 0; u < lTable.size() - 1; u++) {
      Data dtTable = (Data) lTable.elementAt(u);
      String sDS_DIST_EDADTab = dtTable.getString(
          "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
      String sDS_DIST_EDADAlt = pnlRangos.getDatos().getString(
          "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
      if (sDS_DIST_EDADTab.equals(sDS_DIST_EDADAlt)) {
        bRepe = true;
      }
    }

    if (bRepe) {
      this.getApp().showAdvise("Ya existe este rango de edad, seleccione otro.");

      //pnlGrupos.limpiarDatos();
      pnlRangos.limpiarDatos();

      return false;
    }
    else {
      return true;
    }
    /*
         Lista p = new Lista();
         Lista p1= new Lista();
         //Comprobaci�n de que los registros no est�n repetidos
      try{
        QueryTool2 qt = new QueryTool2();
        // tabla de familias de productos
        qt.putName("SIVE_BROTES_DISTGEDAD");
        // campos que se escriben
        qt.putType("NM_DISTGEDAD",QueryTool.INTEGER);
        qt.putWhereType("CD_GEDAD", QueryTool.STRING);
         qt.putWhereValue("CD_GEDAD",pnlGrupos.getDatos().getString("CD_GEDAD"));
        qt.putOperator("CD_GEDAD","=");
        qt.putWhereType("DIST_EDAD_I", QueryTool.INTEGER);
         qt.putWhereValue("DIST_EDAD_I",pnlGrupos.getDatos().getString("DIST_EDAD_I"));
        qt.putOperator("DIST_EDAD_I","=");
        qt.putWhereType("DIST_EDAD_F", QueryTool.INTEGER);
         qt.putWhereValue("DIST_EDAD_F",pnlGrupos.getDatos().getString("DIST_EDAD_F"));
        qt.putOperator("DIST_EDAD_F","=");
        qt.putWhereType("CD_ANO", QueryTool.STRING);
        qt.putWhereValue("CD_ANO",dtBrote.getString("CD_ANO"));
        qt.putOperator("CD_ANO","=");
        qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
        qt.putWhereValue("NM_ALERBRO",dtBrote.getString("NM_ALERBRO"));
        qt.putOperator("NM_ALERBRO","=");
        p.addElement(qt);
        this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        p1 = (Lista) this.getApp().getStub().doPost(1, p);
        }catch (Exception ex) {
         ex.printStackTrace();
         this.getApp().trazaLog(ex);
        }
        if ((p1.size()==0)&&(RepeticionNC())){
           return true;
        }else{
           this.getApp().showAdvise("Ya existe un registro con este grupo de edad, seleccione otro.");
           return false;
        }
     */
  }

  /*
     private boolean RepeticionNC(){
    Lista p = new Lista();
    Lista p1= new Lista();
    Lista lis = new Lista();
    String sDS_DIST_EDAD = "";
    String sCD_GEDAD="";
    String sInCD_GEDAD = pnlGrupos.getDatos().getString("CD_GEDAD");
    boolean bNC=true;
    Data fila = null;
    fila = new Data();
    //Comprobaci�n de que los registros NC no est�n repetidos
      try{
       QueryTool2 qt = new QueryTool2();
       // tabla de familias de productos
       qt.putName("SIVE_BROTES_DISTGEDAD");
       // campos que se escriben
       qt.putType("CD_GEDAD",QueryTool.STRING);
       qt.putWhereType("IT_EDAD", QueryTool.STRING);
       qt.putWhereValue("IT_EDAD","N");
       qt.putOperator("IT_EDAD","=");
       p.addElement(qt);
       this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
       p1 = (Lista) this.getApp().getStub().doPost(1, p);
       }catch (Exception ex) {
        ex.printStackTrace();
        this.getApp().trazaLog(ex);
       }
       System.out.println(p1);
       if (pnlGrupos.getDatos().getString("DS_DIST_EDAD").equals("NC")){
        if (p1.size()>0){
         fila = (Data)p1.firstElement();
         sCD_GEDAD = (String) fila.getString("CD_GEDAD");
         bNC=false;
        }
       }
       if ((sCD_GEDAD.equals(sInCD_GEDAD))){
          bNC=true;
       }
       if (sCD_GEDAD.equals("")){
          bNC=true;
       }
       if (bNC){
          return true;
       }else{
          return false;
       }
     }*/

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    Data dtReg = null;
    Lista lSubTot = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          case 0:

            // ----- NUEVO -----------
            Inicializar(CInicializar.ESPERA);
            if (ComprobarRepeticion()) {
              dtReg = new Data();
              dtRegSubT = new Data();
              dtReg.put("CD_ANO", dtBrote.get("CD_ANO"));
              dtReg.put("NM_ALERBRO", dtBrote.get("NM_ALERBRO"));
              if (pnlRangos.getDatos().getString("DS_DIST_EDAD").equals("NC")) {
                dtReg.put("IT_EDAD", "N");
              }
              else {
                dtReg.put("IT_EDAD", "S");
              }
              dtReg.put("CD_GEDAD", pnlGrupos.getDatos().getString("CD_GEDAD"));
              dtReg.put("DS_GEDAD", pnlGrupos.getDatos().getString("DS_GEDAD"));
              dtReg.put("DIST_EDAD_I",
                        pnlRangos.getDatos().getString("DIST_EDAD_I"));
              dtReg.put("DIST_EDAD_F",
                        pnlRangos.getDatos().getString("DIST_EDAD_F"));
              dtReg.put("DS_DIST_EDAD",
                        pnlRangos.getDatos().getString("DS_DIST_EDAD"));
              dtReg.put("NM_V_EXP", txtVEXP.getText());
              dtReg.put("NM_M_EXP", txtMEXP.getText());
              dtReg.put("NM_NC_EXP", txtNCEXP.getText());
              dtReg.put("NM_V_ENF", txtVENF.getText());
              dtReg.put("NM_M_ENF", txtMENF.getText());
              dtReg.put("NM_NC_ENF", txtNCENF.getText());
              dtReg.put("NM_V_HOS", txtVHOS.getText());
              dtReg.put("NM_M_HOS", txtMHOS.getText());
              dtReg.put("NM_NC_HOS", txtNCHOS.getText());
              dtReg.put("NM_V_DEF", txtVDEF.getText());
              dtReg.put("NM_M_DEF", txtMDEF.getText());
              dtReg.put("NM_NC_DEF", txtNCDEF.getText());
              lSubTot.addElement(dtReg);
              dtRegSubT = SubTotales(lSubTot);

              bAceptar = true;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);

            break;
          case 1:

            // ---- MODIFICAR ----
            Inicializar(CInicializar.ESPERA);
            dtReg = new Data();
            dtRegSubT = new Data();
            dtReg.put("CD_ANO", dtBrote.get("CD_ANO"));
            dtReg.put("NM_ALERBRO", dtBrote.get("NM_ALERBRO"));
            if (pnlRangos.getDatos().getString("DS_DIST_EDAD").equals("NC")) {
              dtReg.put("IT_EDAD", "N");
            }
            else {
              dtReg.put("IT_EDAD", "S");
            }
            dtReg.put("CD_GEDAD", pnlGrupos.getDatos().getString("CD_GEDAD"));
            dtReg.put("DS_GEDAD", pnlGrupos.getDatos().getString("DS_GEDAD"));
            dtReg.put("DIST_EDAD_I",
                      pnlRangos.getDatos().getString("DIST_EDAD_I"));
            dtReg.put("DIST_EDAD_F",
                      pnlRangos.getDatos().getString("DIST_EDAD_F"));
            dtReg.put("DS_DIST_EDAD",
                      pnlRangos.getDatos().getString("DS_DIST_EDAD"));
            dtReg.put("NM_V_EXP", txtVEXP.getText());
            dtReg.put("NM_M_EXP", txtMEXP.getText());
            dtReg.put("NM_NC_EXP", txtNCEXP.getText());
            dtReg.put("NM_V_ENF", txtVENF.getText());
            dtReg.put("NM_M_ENF", txtMENF.getText());
            dtReg.put("NM_NC_ENF", txtNCENF.getText());
            dtReg.put("NM_V_HOS", txtVHOS.getText());
            dtReg.put("NM_M_HOS", txtMHOS.getText());
            dtReg.put("NM_NC_HOS", txtNCHOS.getText());
            dtReg.put("NM_V_DEF", txtVDEF.getText());
            dtReg.put("NM_M_DEF", txtMDEF.getText());
            dtReg.put("NM_NC_DEF", txtNCDEF.getText());

            lSubTot.addElement(dtReg);
            dtRegSubT = SubTotales(lSubTot);

            bAceptar = true;
            dispose();
            Inicializar(CInicializar.NORMAL);

            break;
          case 2:
            Inicializar(CInicializar.ESPERA);
            dtReg = new Data();
            dtRegSubT = new Data();
            dtReg.put("CD_ANO", dtBrote.get("CD_ANO"));
            dtReg.put("NM_ALERBRO", dtBrote.get("NM_ALERBRO"));
            if (pnlRangos.getDatos().getString("DS_DIST_EDAD").equals("NC")) {
              dtReg.put("IT_EDAD", "N");
            }
            else {
              dtReg.put("IT_EDAD", "S");
            }
            dtReg.put("CD_GEDAD", pnlGrupos.getDatos().getString("CD_GEDAD"));
            dtReg.put("DS_GEDAD", pnlGrupos.getDatos().getString("DS_GEDAD"));
            dtReg.put("DIST_EDAD_I",
                      pnlRangos.getDatos().getString("DIST_EDAD_I"));
            dtReg.put("DIST_EDAD_F",
                      pnlRangos.getDatos().getString("DIST_EDAD_F"));
            dtReg.put("DS_DIST_EDAD",
                      pnlRangos.getDatos().getString("DS_DIST_EDAD"));
            dtReg.put("NM_V_EXP", txtVEXP.getText());
            dtReg.put("NM_M_EXP", txtMEXP.getText());
            dtReg.put("NM_NC_EXP", txtNCEXP.getText());
            dtReg.put("NM_V_ENF", txtVENF.getText());
            dtReg.put("NM_M_ENF", txtMENF.getText());
            dtReg.put("NM_NC_ENF", txtNCENF.getText());
            dtReg.put("NM_V_HOS", txtVHOS.getText());
            dtReg.put("NM_M_HOS", txtMHOS.getText());
            dtReg.put("NM_NC_HOS", txtNCHOS.getText());
            dtReg.put("NM_V_DEF", txtVDEF.getText());
            dtReg.put("NM_M_DEF", txtMDEF.getText());
            dtReg.put("NM_NC_DEF", txtNCDEF.getText());
            dtReg.put("NM_DISTGEDAD", dtDev.getString("NM_DISTGEDAD"));
            lSubTot.addElement(dtReg);
            dtRegSubT = SubTotales(lSubTot);

            bAceptar = true;
            dispose();
            Inicializar(CInicializar.NORMAL);
            break;
        }
      }
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  }

  //Calculo de subtotales
  public Data SubTotales(Lista lis) {
    int vExp = 0;
    int mExp = 0;
    int ncExp = 0;
    int tExp = 0;
    int vEnf = 0;
    int mEnf = 0;
    int ncEnf = 0;
    int tEnf = 0;
    int vHos = 0;
    int mHos = 0;
    int ncHos = 0;
    int tHos = 0;
    int vDef = 0;
    int mDef = 0;
    int ncDef = 0;
    int tDef = 0;
    String total = "Total";
    String sNM_V_EXP = "";
    String sDS_DIST_EDAD = "";
    int iTotal = 0;

    Data fila = null;

    for (int i = 0; i < lis.size(); i++) {
      fila = (Data) lis.elementAt(i);
      iTotal = lis.size();

      vExp += Integer.parseInt(fila.getString("NM_V_EXP"));
      mExp += Integer.parseInt(fila.getString("NM_M_EXP"));
      ncExp += Integer.parseInt(fila.getString("NM_NC_EXP"));

      tExp += vExp + mExp + ncExp;
      vEnf += Integer.parseInt(fila.getString("NM_V_ENF"));
      mEnf += Integer.parseInt(fila.getString("NM_M_ENF"));
      ncEnf += Integer.parseInt(fila.getString("NM_NC_ENF"));

      tEnf += vEnf + mEnf + ncEnf;
      vHos += Integer.parseInt(fila.getString("NM_V_HOS"));
      mHos += Integer.parseInt(fila.getString("NM_M_HOS"));
      ncHos += Integer.parseInt(fila.getString("NM_NC_HOS"));

      tHos += vHos + mHos + ncHos;
      vDef += Integer.parseInt(fila.getString("NM_V_DEF"));
      mDef += Integer.parseInt(fila.getString("NM_M_DEF"));
      ncDef += Integer.parseInt(fila.getString("NM_NC_DEF"));

      tDef += vDef + mDef + ncDef;
      //}//fin if
    } //fin for

    fila = new Data();

    if (modoOperacion == 0) {
      fila.put("TIPO_OPERACION", "A");
    }
    else if (modoOperacion == 1) {

      String OpeAnt = dtDev.getString("TIPO_OPERACION");
      if (OpeAnt.equals("A")) {
        fila.put("TIPO_OPERACION", "A");
      }
      else {
        fila.put("NM_DISTGEDAD", dtDev.getString("NM_DISTGEDAD"));
        fila.put("TIPO_OPERACION", "M");
      }

    }
    else if (modoOperacion == 2) {

      String OpeAntBaj = dtDev.getString("TIPO_OPERACION");
      if (OpeAntBaj.equals("M")) {
        fila.put("TIPO_OPERACION", "M");
      }
      else {
        fila.put("TIPO_OPERACION", "B");
      }
    }

    if (pnlRangos.getDatos().getString("DS_DIST_EDAD").equals("NC")) {
      fila.put("IT_EDAD", "N");
    }
    else {
      fila.put("IT_EDAD", "S");
    }

    fila.put("NM_V_EXP", (new Integer(vExp)).toString());
    fila.put("NM_M_EXP", (new Integer(mExp)).toString());
    fila.put("NM_NC_EXP", (new Integer(ncExp)).toString());
    fila.put("NM_V_EXP + NM_M_EXP + NM_NC_EXP", (new Integer(tExp)).toString());
    fila.put("NM_V_ENF", (new Integer(vEnf)).toString());
    fila.put("NM_M_ENF", (new Integer(mEnf)).toString());
    fila.put("NM_NC_ENF", (new Integer(ncEnf)).toString());
    fila.put("NM_V_ENF + NM_M_ENF + NM_NC_ENF", (new Integer(tEnf)).toString());
    fila.put("NM_V_HOS", (new Integer(vHos)).toString());
    fila.put("NM_M_HOS", (new Integer(mHos)).toString());
    fila.put("NM_NC_HOS", (new Integer(ncHos)).toString());
    fila.put("NM_V_HOS + NM_M_HOS + NM_NC_HOS", (new Integer(tHos)).toString());
    fila.put("NM_V_DEF", (new Integer(vDef)).toString());
    fila.put("NM_M_DEF", (new Integer(mDef)).toString());
    fila.put("NM_NC_DEF", (new Integer(ncDef)).toString());
    fila.put("NM_V_DEF + NM_M_DEF + NM_NC_DEF", (new Integer(tDef)).toString());
    fila.put("CD_ANO", dtBrote.get("CD_ANO"));
    fila.put("NM_ALERBRO", dtBrote.get("NM_ALERBRO"));
    fila.put("DS_DIST_EDAD", pnlRangos.getDatos().getString("DS_DIST_EDAD"));
    fila.put("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
             pnlRangos.
             getDatos().getString("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F"));
    fila.put("CD_GEDAD", pnlRangos.getDatos().getString("CD_GEDAD"));
    fila.put("DS_GEDAD", pnlGrupos.getDatos().getString("DS_GEDAD"));
    fila.put("DIST_EDAD_I", pnlRangos.getDatos().getString("DIST_EDAD_I"));
    fila.put("DIST_EDAD_F", pnlRangos.getDatos().getString("DIST_EDAD_F"));

    lis.addElement(fila);
    return fila;
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // Devuelve los datos seleccionados en el di�logo.
  public Data devuelveData() {
    return dtRegSubT;
  }

// botones de aceptar y cancelar.

  class DialogActionAdapter
      implements java.awt.event.ActionListener, Runnable {
    DiaDis adaptee;
    ActionEvent e;

    DialogActionAdapter(DiaDis adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
      this.e = e;
      Thread th = new Thread(this);
      th.start();
    }

    public void run() {
      adaptee.btn_actionPerformed(e);
    }
  }

}
