package brotes.cliente.factriesgo;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import comun.Common;
import jclass.bwt.BWTEnum;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

//import brotes.cliente.c_componentes.*;

public class DiaGestFactR
    extends CDialog
    implements CInicializar, CFiltro {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // modo de operaci�n
  public int modoOperacion = 0;
  public int modoBloqueo = modoNORMAL;

  // datos que le llegan al di�logo
  Data dtDev = null;

  // query para rellenar la lista
  QueryTool2 qtFactR = null;
  QueryTool qtAdic = null;

  // datos devueltos por el servlet
  public Data dtcab = null;

  // datos de cabecera enviados al servlet
  public Data dtCabecera = null;

  //Lista donde se encuentran todos los registros q se muestran,
  //esten o no en BD.
  Lista lClm = new Lista();
  //Lista para guardar elementos a dar de baja.
  Lista lClmBaja = new Lista();
  //Lista para accesos a BD
  Lista lBd = new Lista();

  // lista para recoger los datos de la tabla
  public Lista vDevol = null;
  Lista vDevolfinal = null;

  // lista para el filtro
  Lista vFiltro = null;

  // lista para recoger los datos devueltos de la B.D.
  public Lista vDevolucion = null;

  // lista para recoger los datos del detalle
  public Lista vDetalle = null;

  int indicetabla = 0;

  //variables para controlar el bloqueo
  QueryTool qtBloqueo = null;
  Data dtBloqueo = null;

  // filtro de b�squeda
  private Data dFiltro = null;

  // tabla
  CListaMantenimiento clmFactR = null;

  //Dialogo que rellena los datos de la devoluci�n
  DiaDetFactR diaDet = null;

  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnGrabar = new ButtonControl();
  ButtonControl btnSalir = new ButtonControl();
  Label lblBrote = new Label();
  Label lblCodBrote = new Label();
  Label lblDesBrote = new Label();

  final String servletFactR = "servlet/SrvFactR";

  DiaGestFactR_btn_actionAdapter actionAdapterBtn = new
      DiaGestFactR_btn_actionAdapter(this);

  // constructor de DiaGestFactR
  public DiaGestFactR(CApp a, int modo, Data datos) {

    super(a);
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {

      modoOperacion = modo;
      dtDev = datos;

      // configuraci�n de botones
      vBotones.addElement(new CBoton("",
                                     "images/alta.gif",
                                     "Nuevo brote",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion.gif",
                                     "Modificar brote",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja.gif",
                                     "Borrar brote",
                                     false,
                                     true));

      // configuraci�n de etiquetas
      vLabels.addElement(new CColumna("F.Rieg.",
                                      "84",
                                      "F_RIESGO"));

      vLabels.addElement(new CColumna("ExpEnf.",
                                      "60",
                                      "NM_EXPENF"));

      vLabels.addElement(new CColumna("ExpNEnf.",
                                      "60",
                                      "NM_EXPNOENF"));

      vLabels.addElement(new CColumna("Total",
                                      "50",
                                      "TOTALEXP"));

      vLabels.addElement(new CColumna("T.Atq.%",
                                      "55",
                                      "TASAEXP"));

      vLabels.addElement(new CColumna("NExpEnf.",
                                      "60",
                                      "NM_NOEXPENF"));

      vLabels.addElement(new CColumna("NExpNEnf.",
                                      "60",
                                      "NM_NMNOEXPNOENF"));

      vLabels.addElement(new CColumna("Total",
                                      "50",
                                      "TOTALNOEXP"));

      vLabels.addElement(new CColumna("T.Atq.%",
                                      "59",
                                      "TASANOEXP"));

      vLabels.addElement(new CColumna("D.T.(%)",
                                      "45",
                                      "DIFTASAS"));

      vLabels.addElement(new CColumna("OR(IC)",
                                      "104",
                                      "OR"));

      int[] ajustar = new int[11];
      ajustar[0] = BWTEnum.TOPLEFT;
      ajustar[1] = BWTEnum.TOPRIGHT;
      ajustar[2] = BWTEnum.TOPRIGHT;
      ajustar[3] = BWTEnum.TOPRIGHT;
      ajustar[4] = BWTEnum.TOPRIGHT;
      ajustar[5] = BWTEnum.TOPRIGHT;
      ajustar[6] = BWTEnum.TOPRIGHT;
      ajustar[7] = BWTEnum.TOPRIGHT;
      ajustar[8] = BWTEnum.TOPRIGHT;
      ajustar[9] = BWTEnum.TOPRIGHT;
      ajustar[10] = BWTEnum.TOPRIGHT;

      clmFactR = new capp2.CListaMantenimiento(a,
                                               vLabels,
                                               vBotones,
                                               ajustar,
                                               this,
                                               this,
                                               290,
                                               725);

      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa el aspecto del dialogo
  public void jbInit() throws Exception {

    // carga las im�genes
    final String ImgACEPTAR = "images/aceptar.gif";
    final String ImgCANCELAR = "images/cancelar.gif";

    this.getApp().getLibImagenes().put(ImgACEPTAR);
    this.getApp().getLibImagenes().put(ImgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnGrabar.setImage(this.getApp().getLibImagenes().get(ImgACEPTAR));
    btnSalir.setImage(this.getApp().getLibImagenes().get(ImgCANCELAR));

    xYLayout1.setWidth(740);
    xYLayout1.setHeight(420);
    this.setLayout(xYLayout1);
    this.setSize(740, 420);

    btnGrabar.setLabel("Aceptar");
    btnGrabar.addActionListener(actionAdapterBtn);
    btnGrabar.setActionCommand("grabar");
    btnSalir.setLabel("Cancelar");
    btnSalir.addActionListener(actionAdapterBtn);
    btnSalir.setActionCommand("salir");
    lblBrote.setText("Brote:");

    this.add(lblBrote, new XYConstraints(15, 10, 50, 22));
    this.add(lblCodBrote, new XYConstraints(65, 10, 50, 22));
    this.add(lblDesBrote, new XYConstraints(120, 10, 190, 22));
    this.add(clmFactR, new XYConstraints(5, 40, 725, 290));
    this.add(btnGrabar, new XYConstraints(519, 345, 88, 29));
    this.add(btnSalir, new XYConstraints(630, 345, 88, 29));

    this.setTitle("Brotes: Factores de Riesgo");

    // instancio los objetos  para acceder a b.d.
    vDetalle = new Lista();

    // relleno los datos del brote
    verDatos();

    // relleno la lista
    if (modoOperacion == MODIFICACION ||
        modoOperacion == BAJA ||
        modoOperacion == CONSULTA) {
      clmFactR.setPrimeraPagina(this.primeraPagina());

    }
  } // fin jbinit

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    switch (modo) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        switch (modoOperacion) {
          case ALTA:
            this.setEnabled(true);
            btnSalir.setLabel("Cancelar");
            break;

          case MODIFICACION:
            this.setEnabled(true);
            btnSalir.setLabel("Cancelar");
            break;

          case BAJA:
          case CONSULTA:
            this.setEnabled(false);
            btnGrabar.setVisible(false);
            btnSalir.setLabel("Salir");
            btnSalir.setEnabled(true);
            break;
        }

        break;
    } //fin 2 switch

  }

  private void verDatos() {

    String sBrote = null;
    sBrote = dtDev.getString("CD_ANO") + "/" + dtDev.getString("NM_ALERBRO");
    lblCodBrote.setText(sBrote);

    lblDesBrote.setText(dtDev.getString("DS_BROTE"));

  }

  // operaciones de la botonera
  public void realizaOperacion(int j) {
    DiaDetFactR di = null;
    Data dtDiaDetFactR = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        dtDiaDetFactR.put("CD_GRUPO", dtDev.getString("CD_GRUPO"));
        dtDiaDetFactR.put("CD_ANO", dtDev.getString("CD_ANO"));
        dtDiaDetFactR.put("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
        di = new DiaDetFactR(this.getApp(), ALTA, dtDiaDetFactR, this);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar) {
          Data dtDevuelto = di.dtDevolver();
          lClm.addElement(dtDevuelto);
          //lClm=calcularDatos(lClm);
          lClm = anadirCampo(lClm);
          clmFactR.setPrimeraPagina(lClm);
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dtDiaDetFactR = clmFactR.getSelected();
        //si existe alguna fila seleccionada
        if (dtDiaDetFactR != null) {
          int ind = clmFactR.getSelectedIndex();
          di = new DiaDetFactR(this.getApp(), MODIFICACION, dtDiaDetFactR, this);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar) {
            Data dtDevuelto = di.dtDevolver();
            lClm.removeElementAt(ind);
            lClm.addElement(dtDevuelto);
            //lClm=calcularDatos(lClm);
            lClm = anadirCampo(lClm);
            clmFactR.setPrimeraPagina(lClm);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dtDiaDetFactR = clmFactR.getSelected();
        if (dtDiaDetFactR != null) {
          int ind = clmFactR.getSelectedIndex();
          di = new DiaDetFactR(this.getApp(), BAJA, dtDiaDetFactR, this);
          di.show();
          if (di.bAceptar) {
            Data dtDevuelto = di.dtDevolver();
            lClm.removeElementAt(ind);
            if (dtDevuelto.getString("TIPO_OPERACION").equals("B")) {
              lClmBaja.addElement(dtDiaDetFactR);
            }
            clmFactR.setPrimeraPagina(lClm);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }
  }

  /*  public void realizaOperacion(int j) {
      Data dt = null;
      switch (j) {
        // bot�n de alta
        case 0:
          vDevol = clmFactR.getLista();
          dt = new Data();
          dt.put("CD_ANO", dtDev.getString("CD_ANO"));
          dt.put("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
          diaDet = new DiaDetFactR(this.getApp(),2,dt , this);
          diaDet.show();
          if(diaDet.bAceptar){
//          diaDet.dtSalida.put("IT_OPERACION", "A");
//          vDevol.addElement((Data) diaDet.dtSalida);
            vDevol =anadirCampo(vDevol);
            vDevol=anadirTipoOperacion(vDevol);
            vDevol=calcularDatos(vDevol);
            clmFactR.setPrimeraPagina(vDevol);
            // a�ado el detalle a la lista que luego accedera a la B.D.
//          vDetalle.addElement((Data) diaDet.dtSalida);
            }
          break;
        // bot�n de modificaci�n
        case 1:
          // verifico que se seleccione un producto
          if (clmFactR.getSelected() == null) {
       this.getApp().showAdvise("Debe seleccionar un registro de la tabla");
              break;
          } else {
              indicetabla = clmFactR.m_itemSelecc;
              vDevol = clmFactR.getLista();
              dt = new Data();
              dt = (Data) clmFactR.getSelected();
              diaDet = new DiaDetFactR(this.getApp(),3,dt, this);
              diaDet.show();
//        }
              if(diaDet.bAceptar){
                // lo a�ado a la tabla
//              diaDet.dtSalida.put("IT_OPERACION", "M");
                vDevol =anadirCampo(vDevol);
                vDevol=calcularDatos(vDevol);
                //MODIFICAR
//              vDevol.setElementAt( (Data)diaDet.dtSalida, indicetabla);
                clmFactR.setPrimeraPagina(vDevol);
                // a�ado el detalle a la lista que luego accedera a la B.D.
//              vDetalle.addElement((Data) diaDet.dtSalida);
              }
          }
          break;
        // bot�n de baja
        case 2:
          // verifico que se seleccione un producto
          if (clmFactR.getSelected() == null) {
       this.getApp().showAdvise("Debe seleccionar un registro de la tabla");
              break;
          } else {
              indicetabla = clmFactR.m_itemSelecc;
              vDevol = clmFactR.getLista();
              dt = new Data();
              dt = (Data) clmFactR.getSelected();
              diaDet = new DiaDetFactR(this.getApp(),4,dt, this);
              diaDet.show();
//        }
              if (diaDet.bAceptar) {
//              diaDet.dtSalida.put("IT_OPERACION", "B");
                vDevol.removeElementAt(indicetabla);
                clmFactR.vaciarPantalla();
                clmFactR.setPrimeraPagina(vDevol);
                // a�ado el detalle a la lista que luego accedera a la B.D.
//              vDetalle.addElement((Data) diaDet.dtSalida);
                }
          }
          break;
      }
    }*/

  // solicita la primera trama de datos
  public Lista primeraPagina() {

    Lista vFactR = new Lista();
    vFiltro = new Lista();
    Data dtAdic = new Data();
    final String servlet = "servlet/SrvQueryTool";

    qtFactR = new QueryTool2();
    qtFactR.putName("SIVE_FACTOR_RIESGO");
    qtFactR.putType("NM_FACRI", QueryTool.INTEGER);
    qtFactR.putType("DS_FACRI", QueryTool.STRING);
    qtFactR.putType("CD_ANO", QueryTool.STRING);
    qtFactR.putType("NM_ALERBRO", QueryTool.INTEGER);

    //datos adicionales
    qtAdic = new QueryTool();
    qtAdic.putName("SIVE_VALOR_FACRI");
    qtAdic.putType("DS_VFACRI", QueryTool.STRING);
    qtAdic.putType("NM_EXPENF", QueryTool.INTEGER);
    qtAdic.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qtAdic.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qtAdic.putType("NM_NMNOEXPNOENF", QueryTool.INTEGER);
    qtAdic.putType("IT_CONFIRMADO", QueryTool.STRING);

    dtAdic.put("NM_FACRI", QueryTool.INTEGER);

    qtFactR.addQueryTool(qtAdic);
    qtFactR.addColumnsQueryTool(dtAdic);

    // filtro de a�o y numero de brote
    qtFactR.putWhereType("CD_ANO", QueryTool.STRING);
    qtFactR.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtFactR.putOperator("CD_ANO", "=");

    qtFactR.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtFactR.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtFactR.putOperator("NM_ALERBRO", "=");

    qtFactR.addOrderField("NM_FACRI");

    Inicializar(CInicializar.ESPERA);

    //realizo la consulta al servlet
    try {
      /*      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
            //filtro
            vFiltro.addElement(qtFactR);
            // trama
            vFiltro.setTrama("NM_FACRI","");
            vDevol = (Lista) this.getApp().getStub().doPost(8, vFiltro);*/

      vFiltro.addElement(qtFactR);
      vDevol = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, vFiltro);
      if (vDevol.size() == 0) {
//          this.getApp().showAdvise("No hay datos con esta entrada");
        vDevol = null;
      }
      else {
        vDevolfinal = (Lista) calcularDatos(vDevol);
        vDevolfinal = anadirCampo(vDevolfinal);
        lClm = vDevolfinal;
      }

      //errores
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // la lista debe estar creada
    if (vDevol == null) {
      vDevol = new Lista();
      vDevolfinal = new Lista();
    }

    Inicializar(CInicializar.NORMAL);

    return vDevolfinal;

  }

  //concatena c�digo y descripci�n de t_implicacion
  Lista anadirCampo(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      ( (Data) lCampo.elementAt(i)).put("F_RIESGO",
                                        dCampo.getString("DS_FACRI") + " - " +
                                        dCampo.getString("DS_VFACRI"));
    } //end for
    return lCampo;
  } //end anadirCampo

  //Tipo Operacion=BD
  Lista anadirTipoOperacion(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      ( (Data) lCampo.elementAt(i)).put("TIPO_OPERACION", "BD");
    } //end for
    return lCampo;
  } //end anadirPrincipal

  public Lista siguientePagina() {
    Lista l = new Lista();
    return l;
  } //end siguiente_pagina

  /*    // lista con las devoluciones
      Lista vDevol = null;
      // lista para el filtro
      vFiltro = new Lista();
      Inicializar(CInicializar.ESPERA);
      // realiza la consulta al servlet
      try {
//      Data devol = new Data();
        this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        // fija el filtro
        vFiltro.addElement(qtFactR);
        // fija la trama
        vFiltro.setTrama(this.clmFactR.getLista().getTrama());
        vDevol = (Lista) this.getApp().getStub().doPost(8, vFiltro);
        vDevolfinal = (Lista) calcularDatos(vDevol);
      // error en el servlet
      } catch (Exception ex) {
        this.getApp().trazaLog(ex);
        this.getApp().showError(ex.getMessage());
      }
      // la lista debe estar creada
//    if (vDevol == null)
      if (vDevol.size() == 0) {
        vDevol = new Lista();
        vDevolfinal = new Lista();
      }
      Inicializar(CInicializar.NORMAL);
//   Lista vDevol = null;
      return vDevolfinal;*/

  public Lista calcularDatos(Lista lt) {

    Lista lista = null;
    Data dtlista = null;
    int indice = 0;
    int total1 = 0;
    int total2 = 0;
    int totalenf = 0;
    double tasa1 = 0;
    double tasa2 = 0;
    double diftasa = 0;
    double expenf = 0;
    double expnenf = 0;
    double nexpenf = 0;
    double nexpnenf = 0;
    double or = 0;
    lista = lt;
    indice = lista.size();
    for (int i = 0; i < indice; i++) {
      dtlista = (Data) lista.elementAt(i);
      expenf = Integer.parseInt( (String) dtlista.getString("NM_EXPENF"));
      expnenf = Integer.parseInt( (String) dtlista.getString("NM_EXPNOENF"));
      nexpenf = Integer.parseInt( (String) dtlista.getString("NM_NOEXPENF"));
      nexpnenf = Integer.parseInt( (String) dtlista.getString("NM_NMNOEXPNOENF"));
      total1 = (int) (expenf + expnenf); // total de expuestos
      total2 = (int) (nexpenf + nexpnenf);
      totalenf = (int) (expenf + nexpenf); // total de enfermos
      tasa1 = ( (expenf / (expenf + expnenf)) * 100);
      tasa2 = ( (nexpenf / (nexpenf + nexpnenf)) * 100);
      if (expenf + expnenf == 0) {
        tasa1 = 0;
      }
      if (nexpenf + nexpnenf == 0) {
        tasa2 = 0;
      }
      diftasa = (tasa1 - tasa2);
      dtlista.put("TOTALEXP", Integer.toString(total1));
      dtlista.put("TOTALNOEXP", Integer.toString(total2));
      int ind1 = Double.toString(tasa1).indexOf(".");
      dtlista.put("TASAEXP", Double.toString(tasa1).substring(0, (ind1 + 2)));
      int ind2 = Double.toString(tasa2).indexOf(".");
      dtlista.put("TASANOEXP", Double.toString(tasa2).substring(0, (ind2 + 2)));
      int ind3 = Double.toString(diftasa).indexOf(".");
      dtlista.put("DIFTASAS", Double.toString(diftasa).substring(0, (ind3 + 2)));
      dtlista.put("OR", intervaloConfianza(expenf, nexpenf, expnenf, nexpnenf,
                                           (expenf + nexpenf),
                                           (expnenf + nexpnenf),
                                           (expenf + expnenf),
                                           (nexpenf + nexpnenf),
                                           (expenf + nexpenf + expnenf +
                                            nexpnenf),
                                           2)); // 14-05-01, 2 decimales (ARS)
      dtlista.put("TOTALENF", Integer.toString(totalenf));

    }
    return lista;

  }

  private String intervaloConfianza(double a, double b, double c, double d,
                                    double M1, double M0, double N1, double N0,
                                    double T, int decimales) {
    String salida = "";
    double rv = 0.0;
    double Z = 1.96;
    double varianza = 0.0;
    double E = 0.0;
    double X = 0.0;
    double LCI = 0.0;
    double LCS = 0.0;
    int indice = 0;
    int indice_E = 0;

    rv = (a * d) / (b * c);
    varianza = (M1 * N1 * M0 * N0) / (T * T * (T - 1));
    E = M1 * N1 / T;
    // ARS (20-04-01)
    X = java.lang.Math.sqrt( ( (a - E) * (a - E)) / varianza);

    LCI = java.lang.Math.pow(rv, (1 - Z / X));
    LCS = java.lang.Math.pow(rv, (1 + Z / X));

    indice = Double.toString(LCI).indexOf(".");
    indice_E = Double.toString(LCI).indexOf("E");
    if (LCI != 0.0) {
      salida = Double.toString(LCI).substring(0, 1 + indice + decimales);
    }
    else {
      salida = Double.toString(LCI);
    }
    if (indice_E != -1) {
      salida += Double.toString(LCI).substring(indice_E);

    }
    indice = Double.toString(LCS).indexOf(".");
    indice_E = Double.toString(LCS).indexOf("E");
    if (LCS != 0.0) {
      salida += "-" + Double.toString(LCS).substring(0, 1 + indice + decimales);
    }
    else {
      salida += "-" + Double.toString(LCS);

    }
    if (indice_E != -1) {
      salida += Double.toString(LCS).substring(indice_E);
      // Cambio nuevo: 20-04-01. Para ponerlo en otro formato.
      // Otro cambio: 14-05-01. Ponerle variable "decimales"
    }
    salida = cogeDecimal(rv, decimales, '.') + "(" + salida + ")";
    // Cambio nuevo: 20-04-01. Si cualquiera de los dos l�mites sale indeterminado
    // se pone indeterminado y santas pascuas.
    if ( (salida.indexOf("N") != -1) || (salida.indexOf("I") != -1)) {
      salida = "Indeterminado";
      /*    // Quitamos Na e Inf, �pero dejamos 0.0!.
          int indiceNa = salida.indexOf("N");
          int indice0_0 = salida.indexOf("0.0");
          if (indiceNa==-1)
            indiceNa = salida.indexOf("I");
          if (indiceNa!=-1) {
            if (indice0_0!=-1) {
              if (indice0_0>indiceNa)
                salida = "Indef." + salida.substring(indiceNa+1);
              else if (indice0_0<indiceNa)
                salida = salida.substring(0);
            } else {
                salida = "Indef." + salida.substring(indiceNa+1);
            }
          }
          //***************
           indiceNa = salida.indexOf("N");
           if (indiceNa==-1)
             indiceNa = salida.lastIndexOf("I");
           if (indiceNa!=-1) {
                 salida = salida.substring(0, indiceNa)+"Indef.";
           }*/

    }
    return salida;
  }

  //M�todo que consulta al  servlet
  public Lista accesoBd() {
    // nombre del servlet
    final String servlet = "servlet/SrvGestFactR";

//    this.app.getParameter("COD_USUARIO")
    Lista vParam = new Lista();

    // realiza la consulta al servlet
    try {

      this.getApp().getStub().setUrl(servlet);
      vParam.setParameter("CD_OPE", this.app.getParameter("LOGIN"));

      // le a�ado las lineas de detalle
//      if (modoOperacion == BAJA)
//        vDetalle =  clmFactR.getLista();
      vParam.addElement(vDetalle);
      /*      // accedo al servlet dependiendo del modoOperacion
            if ((String) dtDev.get("IT_OPERACION") == "A") {
           vDevolucion = (Lista) this.getApp().getStub().doPost(2, vParam);
            } else if ((String) dtDev.get("IT_OPERACION") == "M") {
           vDevolucion = (Lista) this.getApp().getStub().doPost(3, vParam);
            } else if ((String) dtDev.get("IT_OPERACION") == "B") {
           vDevolucion = (Lista) this.getApp().getStub().doPost(4, vParam);
            }
       */

      preparaBloqueo();
      vDevolucion = (Lista)this.getApp().getStub().doPost(10003, vParam,
          qtBloqueo, dtBloqueo, this.getApp());

      // actualizo la informaci�n de cd_ope y fc_ultact del
      // data de entrada  si todo va bien
      dtDev.put("CD_OPE", ( (Data) vDevolucion.elementAt(0)).getString("CD_OPE"));
      dtDev.put("FC_ULTACT",
                ( (Data) vDevolucion.elementAt(0)).getString("FC_ULTACT"));

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // la lista debe estar creada
    if (vDevolucion == null) {
      vDevolucion = new Lista();

    }
    return vDevolucion;
  }

  /**************************************/

  //botones
  void btnSalir_actionPerformed() {
    dispose();

  }

  private String cogeDecimal(double dent, int num_dec, char decimal) {
    String salida = "";
    int pos_coma = 0;
    if (!Double.isNaN(dent)) {
      String cadena = String.valueOf(dent);
      for (int a = 0; a < cadena.length(); a++) {
        if (cadena.charAt(a) == decimal) {
          pos_coma = a;
        }
      }
      if ( (num_dec > 0) && (pos_coma + num_dec < cadena.length())) {
        salida = cadena.substring(0, pos_coma + num_dec + 1);
      }
      else if (num_dec == 0) {
        salida = cadena.substring(0, pos_coma);
      }
      else {
        salida = cadena;
      }
    }
    else {
      salida = "-";
    }
    return salida;
  }

  private void preparaBloqueo() {

    // parametros que se leen
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    // filtro
    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtDev.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtDev.getString("FC_ULTACT"));

  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  void btnGrabar_actionPerformed() {
    Inicializar(CInicializar.ESPERA);
    Lista vResultado = new Lista();
    //recorro la lista q tengo para altas y modificaciones
    for (int i = 0; i < lClm.size(); i++) {
      Data dtClm = (Data) lClm.elementAt(i);

      //Se realiza un alta
      if (dtClm.getString("TIPO_OPERACION").equals("A")) {
        QueryTool qtIns = new QueryTool();
        qtIns = realizarInsertFactor(dtClm);
        Data dtInsert = new Data();
        dtInsert.put("10003", qtIns);
        lBd.addElement(dtInsert);

        QueryTool qtInsV = new QueryTool();
        qtInsV = realizarInsertValor(dtClm);
        Data dtInsertV = new Data();
        dtInsertV.put("10003", qtInsV);
        lBd.addElement(dtInsertV);

      } //end if ALTA

      if (dtClm.getString("TIPO_OPERACION").equals("M")) {
        /*            QueryTool qtUpd=new QueryTool();
                    qtUpd=realizarUpdateFactor(dtClm);
                    Data dtUpdate = new Data();
                    dtUpdate.put("10006",qtUpd);
                    lBd.addElement(dtUpdate);*/

        QueryTool qtUpdV = new QueryTool();
        qtUpdV = realizarUpdateValor(dtClm);
        Data dtUpdateV = new Data();
        dtUpdateV.put("10006", qtUpdV);
        lBd.addElement(dtUpdateV);
      }
    } //end for
    //lista para bajas
    for (int i = 0; i < lClmBaja.size(); i++) {
      Data dtClmBaja = (Data) lClmBaja.elementAt(i);

      QueryTool qtDelV = new QueryTool();
      qtDelV = realizarDeleteValor(dtClmBaja);
      Data dtDelV = new Data();
      dtDelV.put("10007", qtDelV);
      lBd.addElement(dtDelV);

      QueryTool qtDel = new QueryTool();
      qtDel = realizarDeleteFactor(dtClmBaja);
      Data dtDel = new Data();
      dtDel.put("10007", qtDel);
      lBd.addElement(dtDel);

    } //end for lBajas

    //si se ha modificado algo:se inserta para ese brote el nuevo
    //cd_ope y fc_ultact
    if (lBd.size() != 0) {
      Data dtDatBrot = new Data();
      dtDatBrot.put("CD_OPE", dtDev.getString("CD_OPE"));
      dtDatBrot.put("FC_ULTACT", "");

      QueryTool qtUpdBro = new QueryTool();
      qtUpdBro = realizarUpdateBrote(dtDatBrot);
      Data dtUpdBro = new Data();
      dtUpdBro.put("10006", qtUpdBro);
      lBd.addElement(dtUpdBro);
//    }

      preparaBloqueo();
      try {
        //BDatos.ejecutaSQL(false,this.getApp(),servletFactR,0,lBd);
        /*            SrvFactR servlet=new SrvFactR();
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                    vResultado = (Lista) servlet.doDebug(0,lBd);*/
//            vResultado = (Lista)this.getApp().getStub().doPost(0,lBd);

        this.getApp().getStub().setUrl(servletFactR);
        vResultado = (Lista)this.getApp().getStub().doPost(10000,
            lBd,
            qtBloqueo,
            dtBloqueo,
            getApp());

        //obtener el nuevo cd_ope y fc_ultact de la base de datos.
        int tamano = vResultado.size();
        String fecha = null;
        for (int i = 0; i < vResultado.size(); i++) {
          fecha = null;
          fecha = ( (Lista) vResultado.elementAt(i)).getFC_ULTACT();
        }
        dtDev.put("CD_OPE", dtDev.getString("CD_OPE"));
        dtDev.put("FC_ULTACT", fecha);
        dispose();
      }
      catch (Exception ex) {
        if (Common.ShowPregunta(this.getApp(), "Los datos han sido modificados por otro usuario. �Sigue queriendo realizar esta actualizaci�n?")) {
          try {
            ModosOperacNormal();
            this.getApp().getStub().setUrl(servletFactR);
            vResultado = (Lista)this.getApp().getStub().doPost(0, lBd);
            dispose();
          }
          catch (Exception exc) {
            this.getApp().trazaLog(exc);
            this.getApp().showError(ex.getMessage());
            dispose();
          } //end 2.catch
        }
        else {
          dispose();
        } //end if comun.showpregunta
      } //end 1. catch
    }
    else {
      dispose();
    } //end if se han realizado cambios
    Inicializar(CInicializar.NORMAL);
  } //end btnAceptar

  QueryTool realizarInsertFactor(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    qtIns.putName("SIVE_FACTOR_RIESGO");

    qtIns.putType("DS_FACRI", QueryTool.STRING);
    qtIns.putValue("DS_FACRI", dtUpd.getString("DS_FACRI"));
    qtIns.putType("CD_ANO", QueryTool.STRING);
    qtIns.putValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtIns.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtIns.putValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtIns.putType("NM_FACRI", QueryTool.INTEGER);
    qtIns.putValue("NM_FACRI", "");

    return qtIns;
  } //end realizarInserFactor

  QueryTool realizarInsertValor(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    qtIns.putName("SIVE_VALOR_FACRI");

    qtIns.putType("DS_VFACRI", QueryTool.STRING);
    qtIns.putValue("DS_VFACRI", dtUpd.getString("DS_VFACRI"));

    qtIns.putType("NM_EXPENF", QueryTool.INTEGER);
    qtIns.putValue("NM_EXPENF", dtUpd.getString("NM_EXPENF"));

    qtIns.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qtIns.putValue("NM_EXPNOENF", dtUpd.getString("NM_EXPNOENF"));

    qtIns.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qtIns.putValue("NM_NOEXPENF", dtUpd.getString("NM_NOEXPENF"));

    qtIns.putType("NM_NMNOEXPNOENF", QueryTool.INTEGER);
    qtIns.putValue("NM_NMNOEXPNOENF", dtUpd.getString("NM_NMNOEXPNOENF"));

    qtIns.putType("IT_CONFIRMADO", QueryTool.STRING);
    qtIns.putValue("IT_CONFIRMADO", dtUpd.getString("IT_CONFIRMADO"));

    qtIns.putType("NM_FACRI", QueryTool.INTEGER);
    qtIns.putValue("NM_FACRI", "");

    return qtIns;
  } //end realizarInserValor

  QueryTool realizarUpdateFactor(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_FACTOR_RIESGO");

    qtModif.putType("DS_FACRI", QueryTool.STRING);
    qtModif.putValue("DS_FACRI", dtUpd.getString("DS_FACRI"));

    qtModif.putWhereType("NM_FACRI", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_FACRI", dtUpd.getString("NM_FACRI"));
    qtModif.putOperator("NM_FACRI", "=");

    return qtModif;
  }

  QueryTool realizarUpdateValor(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_VALOR_FACRI");

    qtModif.putType("DS_VFACRI", QueryTool.STRING);
    qtModif.putValue("DS_VFACRI", dtUpd.getString("DS_VFACRI"));

    qtModif.putType("NM_EXPENF", QueryTool.INTEGER);
    qtModif.putValue("NM_EXPENF", dtUpd.getString("NM_EXPENF"));

    qtModif.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qtModif.putValue("NM_EXPNOENF", dtUpd.getString("NM_EXPNOENF"));

    qtModif.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qtModif.putValue("NM_NOEXPENF", dtUpd.getString("NM_NOEXPENF"));

    qtModif.putType("NM_NMNOEXPNOENF", QueryTool.INTEGER);
    qtModif.putValue("NM_NMNOEXPNOENF", dtUpd.getString("NM_NMNOEXPNOENF"));

    qtModif.putType("IT_CONFIRMADO", QueryTool.STRING);
    qtModif.putValue("IT_CONFIRMADO", dtUpd.getString("IT_CONFIRMADO"));

    qtModif.putWhereType("NM_FACRI", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_FACRI", dtUpd.getString("NM_FACRI"));
    qtModif.putOperator("NM_FACRI", "=");

    return qtModif;
  }

  QueryTool realizarUpdateBrote(Data dtUpd) {
    QueryTool qtUpd = new QueryTool();
    qtUpd.putName("SIVE_BROTES");
    qtUpd.putType("CD_OPE", QueryTool.STRING);
    qtUpd.putValue("CD_OPE", dtUpd.getString("CD_OPE"));
    qtUpd.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtUpd.putValue("FC_ULTACT", dtUpd.getString("FC_ULTACT"));

    // filtro
    qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");

    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");

    return qtUpd;
  }

  QueryTool realizarDeleteFactor(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_FACTOR_RIESGO");

    qtModif.putWhereType("NM_FACRI", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_FACRI", dtUpd.getString("NM_FACRI"));
    qtModif.putOperator("NM_FACRI", "=");
    return qtModif;
  }

  QueryTool realizarDeleteValor(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_VALOR_FACRI");

    qtModif.putWhereType("NM_FACRI", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_FACRI", dtUpd.getString("NM_FACRI"));
    qtModif.putOperator("NM_FACRI", "=");
    return qtModif;
  }

  public void ModosOperacNormal() {
    for (int i = 0; i < lClm.size(); i++) {
      Data dtClm = (Data) lClm.elementAt(i);

      //Se realiza un alta
      if (dtClm.getString("TIPO_OPERACION").equals("A")) {
        QueryTool qtIns = new QueryTool();
        qtIns = realizarInsertFactor(dtClm);
        Data dtInsert = new Data();
        dtInsert.put("3", qtIns);
        lBd.addElement(dtInsert);

        QueryTool qtInsV = new QueryTool();
        qtInsV = realizarInsertValor(dtClm);
        Data dtInsertV = new Data();
        dtInsertV.put("3", qtInsV);
        lBd.addElement(dtInsertV);

      } //end if ALTA

      if (dtClm.getString("TIPO_OPERACION").equals("M")) {
        /*            QueryTool qtUpd=new QueryTool();
                    qtUpd=realizarUpdateFactor(dtClm);
                    Data dtUpdate = new Data();
                    dtUpdate.put("4",qtUpd);
                    lBd.addElement(dtUpdate);*/

        QueryTool qtUpdV = new QueryTool();
        qtUpdV = realizarUpdateValor(dtClm);
        Data dtUpdateV = new Data();
        dtUpdateV.put("4", qtUpdV);
        lBd.addElement(dtUpdateV);
      }
    } //end for
    //lista para bajas
    for (int i = 0; i < lClmBaja.size(); i++) {
      Data dtClmBaja = (Data) lClmBaja.elementAt(i);

      QueryTool qtDelV = new QueryTool();
      qtDelV = realizarDeleteValor(dtClmBaja);
      Data dtDelV = new Data();
      dtDelV.put("5", qtDelV);
      lBd.addElement(dtDelV);

      QueryTool qtDel = new QueryTool();
      qtDel = realizarDeleteFactor(dtClmBaja);
      Data dtDel = new Data();
      dtDel.put("5", qtDel);
      lBd.addElement(dtDel);

    } //end for lBajas

  } //end ModosOperacNormal
} // fin clase

/****************************************/
class DiaGestFactR_btn_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaGestFactR adaptee;
  ActionEvent evt;

  DiaGestFactR_btn_actionAdapter(DiaGestFactR adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (evt.getActionCommand().equals("grabar")) {
        adaptee.btnGrabar_actionPerformed();
      }
      if (evt.getActionCommand().equals("salir")) {
        adaptee.btnSalir_actionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
}
