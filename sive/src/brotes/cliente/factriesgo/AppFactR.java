package brotes.cliente.factriesgo;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

//import javax.swing.UIManager;
public class AppFactR
    extends CApp
    implements CInicializar {

  DiaGestFactR d = null;
//  DiaDetFactR d = null;

  public AppFactR() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    // query tool
    QueryTool qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", "2000");
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", "1");
    qtBloqueo.putOperator("NM_ALERBRO", "=");
    Lista l = new Lista();
    l.addElement(qtBloqueo);

    Lista vResultado = null;
    try {
      this.getStub().setUrl("servlet/SrvQueryTool");
      vResultado = (Lista)this.getStub().doPost(1, l);
    }
    catch (Exception ex) {}
    ;

    Data miData = (Data) vResultado.elementAt(0);

    String Ano = "2000";
    String NumBrote = "1";
    String Grupo = "0";
    String DescBrote = "Informe Brote 1";
    String Ope = "D_1";
    String FUlt = "03/01/2000 12:33:36";
    Data datos = new Data();
    datos.put("CD_ANO", Ano);
    datos.put("NM_ALERBRO", NumBrote);
    datos.put("DS_BROTE", DescBrote);
    datos.put("CD_GRUPO", Grupo);
    datos.put("CD_OPE", miData.getString("CD_OPE"));
    datos.put("FC_ULTACT", miData.getString("FC_ULTACT"));

    //    setTitulo("Tasa de ataque por factor de riesgo");
//    pan = new PanSelDevPr(this);
//    VerPanel("", pan);
//    d = new DiaGestFactR(this);
//    datos.put("CD_ANO", "1997");
//    datos.put("NM_ALERBRO", "1");
    d = new DiaGestFactR(this, d.MODIFICACION, datos);
    d.show();
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        d.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        d.setEnabled(true);
        break;
    }
  }
}
