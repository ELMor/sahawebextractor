
package brotes.cliente.factriesgo;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import com.borland.jbcl.layout.*;
import com.borland.jbcl.control.*;
import java.applet.*;
import jclass.bwt.*;
import java.net.*;
import sapp2.*;
import capp2.*;
import brotes.cliente.c_comuncliente.*;
import brotes.cliente.c_componentes.*;

public class DiaDetFactR extends  CDialog implements CInicializar{

  // modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // producto devuelto
//  Data dtDev = null;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // datos para panel superior
  public Data dtdatos = null;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  //escuchador de botones
  DiaDetFactRActionAdapter actionAdapter = new DiaDetFactRActionAdapter(this) ;

  // modo de operaci�n
  public int modoOperacion = 0;
  public int modoBloqueo= modoNORMAL;

  XYLayout xYLayout1 = new XYLayout();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  LabelControl lblFactor = new LabelControl();
  CTexto txtFactor = new CTexto(40);
  CheckboxGroup cbg = new CheckboxGroup();
  Checkbox cb1 = new Checkbox("Sospechoso", true,cbg);
  Checkbox cb2 = new Checkbox("Confirmado", false, cbg);
  LabelControl lblValor = new LabelControl();
  CTexto txtValor = new CTexto(40);
  LabelControl lblExpEnf = new LabelControl();
  CEntero txtExpEnf = new CEntero(6);
  LabelControl lblExpNEnf = new LabelControl();
  CEntero txtExpNEnf = new CEntero(6);
  LabelControl lblNExpEnf = new LabelControl();
  CEntero txtNExpEnf = new CEntero(6);
  LabelControl lblNExpNEnf = new LabelControl();
  CEntero txtNExpNEnf = new CEntero(6);

  // di�logo de gesti�n
  DiaGestFactR diaGest = null;

  public Data dtDev=null;
  public Data dtDevuelto=null;
  public Lista lEntra=null;
  public Data dtDatFact=new Data();
  public  Data dtRecoger=new Data();

  // constructor del di�logo DiaDetFactR
  public DiaDetFactR(CApp a, int modo, Data dt, DiaGestFactR dial) {

    super(a);
    try  {
      modoOperacion = modo;
      dtDev = dt;
      diaGest = dial;

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa el aspecto del dialogo
  public void jbInit() throws Exception{

    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";

    this.setLayout(xYLayout1);
    this.setSize(550, 250);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));
    btnCancelar.addActionListener(actionAdapter);

    cb1.addItemListener(new DiaDetFactR_cb_itemAdapter(this));
    cb2.addItemListener(new DiaDetFactR_cb_itemAdapter(this));
    cb1.setName("Check");
    cb2.setName("Check");

    lblFactor.setText("Factor de riesgo:");
    lblValor.setText("Valor:");
    lblExpEnf.setText("Expuestos enfermos:");
    lblExpNEnf.setText("Expuestos no enfermos:");
    lblNExpEnf.setText("No Expuestos enfermos:");
    lblNExpNEnf.setText("No Expuestos no enfermos:");

    txtFactor.setBackground(new Color(255, 255, 150));
    txtValor.setBackground(new Color(255, 255, 150));
    txtExpEnf.setBackground(new Color(255, 255, 150));
    txtExpNEnf.setBackground(new Color(255, 255, 150));
    txtNExpEnf.setBackground(new Color(255, 255, 150));
    txtNExpNEnf.setBackground(new Color(255, 255, 150));

    this.add(lblFactor, new XYConstraints(5, 5, 100, -1));
    this.add(txtFactor, new XYConstraints(110, 5, 200, 22));
    this.add(cb1, new XYConstraints(150, 35, -1, -1));
    this.add(cb2, new XYConstraints(275, 35, -1, -1));
    this.add(lblValor, new XYConstraints(5, 65, 100, -1));
    this.add(txtValor, new XYConstraints(110, 65, 200, 22));
    this.add(lblExpEnf, new XYConstraints(5, 95, 140, -1));
    this.add(txtExpEnf, new XYConstraints(175, 95, 80, 22));
    this.add(lblExpNEnf, new XYConstraints(275, 95, 140, -1));
    this.add(txtExpNEnf, new XYConstraints(445, 95, 80, 22));
    this.add(lblNExpEnf, new XYConstraints(5, 125, 140, -1));
    this.add(txtNExpEnf, new XYConstraints(175, 125, 80, 22));
    this.add(lblNExpNEnf, new XYConstraints(275, 125, 160, -1));
    this.add(txtNExpNEnf, new XYConstraints(445, 125, 80, -1));
    this.add(btnAceptar, new XYConstraints(370, 170, -1, -1));
    this.add(btnCancelar, new XYConstraints(460, 170, -1, -1));

    this.setTitle("Brotes: Factores de Riesgo");

    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s del applet
    verDatos();

  }  // fin jbinit

  public void verDatos() {
    switch (modoOperacion) {
          case ALTA:
            this.setEnabled(true);
            break;

          case MODIFICACION:
            this.setEnabled(true);
            txtFactor.setEnabled(false);

            txtFactor.setText((String)dtDev.get("DS_FACRI"));
            txtValor.setText((String)dtDev.get("DS_VFACRI"));
            txtExpEnf.setText((String)dtDev.get("NM_EXPENF"));
            txtExpNEnf.setText((String)dtDev.get("NM_EXPNOENF"));
            txtNExpEnf.setText((String)dtDev.get("NM_NOEXPENF"));
            txtNExpNEnf.setText((String)dtDev.get("NM_NMNOEXPNOENF"));
            if (dtDev.get("IT_CONFIRMADO").equals("S")){
              cb1.setState(false);//sospechoso
              cb2.setState(true);//confirmado
            }else{
              cb1.setState(true);//sospechoso
              cb2.setState(false);//confirmado
            }
            break;

          // s�lo habilitado salir y grabar
          case BAJA:
          case CONSULTA:
            this.setEnabled(false);

            txtFactor.setText((String)dtDev.get("DS_FACRI"));
            txtValor.setText((String)dtDev.get("DS_VFACRI"));
            txtExpEnf.setText((String)dtDev.get("NM_EXPENF"));
            txtExpNEnf.setText((String)dtDev.get("NM_EXPNOENF"));
            txtNExpEnf.setText((String)dtDev.get("NM_NOEXPENF"));
            txtNExpNEnf.setText((String)dtDev.get("NM_NMNOEXPNOENF"));
            if (dtDev.get("IT_CONFIRMADO").equals("S")){
              cb1.setState(false);//sospechoso
              cb2.setState(true);//confirmado
            }else{
              cb1.setState(true);//sospechoso
              cb2.setState(false);//confirmado
            }

            btnCancelar.setEnabled(true);
            btnAceptar.setEnabled(true);
            break;
        }

    }

  public void Inicializar(int modo) {
    switch (modo) {

      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case ALTA:
            this.setEnabled(true);
            break;

          case MODIFICACION:
            this.setEnabled(true);
            txtFactor.setEnabled(false);
            break;

          // s�lo habilitado salir y grabar
          case BAJA:
          case CONSULTA:
            this.setEnabled(false);
            btnCancelar.setEnabled(true);
            btnAceptar.setEnabled(true);
            break;
        }
        break;
    }

  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos(){
    return scr;
  } // Fin getSincrEventos()


  void calcularTotalEnf() {
    Lista vTotal = null;
    Lista vFiltro = null;
    Data dtTotal = null;
    QueryTool qtTotal = null;
    boolean bTotal = false;

    qtTotal = new QueryTool();
    qtTotal.putName("SIVE_BROTES");
    qtTotal.putType("NM_EXPUESTOS", QueryTool.INTEGER);
    qtTotal.putType("NM_ENFERMOS", QueryTool.INTEGER);

    // filtro de a�o y numero de brote
    qtTotal.putWhereType("CD_ANO", QueryTool.STRING);
    qtTotal.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtTotal.putOperator("CD_ANO", "=");

    qtTotal.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtTotal.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtTotal.putOperator("NM_ALERBRO", "=");


    //realizo la consulta al servlet
    try {
      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);

      //filtro
      vFiltro = new Lista();
      vFiltro.addElement(qtTotal);

      vTotal = new Lista();
      vTotal = (Lista) this.getApp().getStub().doPost(1, vFiltro);


      if(vTotal.size() != 0) {
        dtTotal = new Data();
        dtTotal = (Data) vTotal.elementAt(0);
        int exptot = 0;
        int enftot = 0;
        int exp = 0;
        int enf = 0;
        if(((String)dtTotal.getString("NM_EXPUESTOS")).equals(""))
          exptot=0;
        else
          exptot = Integer.parseInt((String)dtTotal.getString("NM_EXPUESTOS"));
        if(((String)dtTotal.getString("NM_ENFERMOS")).equals(""))
          enftot=0;
        else
          enftot = Integer.parseInt((String)dtTotal.getString("NM_ENFERMOS"));
        //valores existentes
        exp = Integer.parseInt((String)dtRecoger.getString("TOTALEXP"));
        enf =Integer.parseInt((String)dtRecoger.getString("TOTALENF"));
        if( exptot != exp )
          this.getApp().showAdvise("El numero total de expuestos :" + exptot + " es distinto al numero de expuestos introducidos");

        if( enftot != enf )
          this.getApp().showAdvise("El numero total de enfermos :" + enftot + " es distinto al numero de enfermos introducidos");

        bTotal = true;
      }


      //errores
      } catch (Exception ex) {
        this.getApp().trazaLog(ex);
        this.getApp().showError(ex.getMessage());
      }

      // la lista debe estar creada
      if (vTotal == null)
        vTotal = new Lista();

//      return bTotal;

  }

  public Data recogerDatos(){
    dtRecoger.put("DS_FACRI", (String)txtFactor.getText().trim());
    dtRecoger.put("DS_VFACRI", (String)txtValor.getText().trim());
    dtRecoger.put("NM_EXPENF", txtExpEnf.getText().trim());
    dtRecoger.put("NM_EXPNOENF", txtExpNEnf.getText().trim());
    dtRecoger.put("NM_NOEXPENF", txtNExpEnf.getText().trim());
    dtRecoger.put("NM_NMNOEXPNOENF", txtNExpNEnf.getText().trim());
    dtRecoger.put("CD_ANO",dtDev.getString("CD_ANO"));
    dtRecoger.put("NM_ALERBRO",dtDev.getString("NM_ALERBRO"));
    if (cb1.getState()){ //sospechoso
      dtRecoger.put("IT_CONFIRMADO","N");
    }else{//confirmado
      dtRecoger.put("IT_CONFIRMADO","S");
    }

    switch (modoOperacion){
      case ALTA:
        dtRecoger.put("TIPO_OPERACION","A");
        dtRecoger.put("NM_FACRI","");
        break;
      case MODIFICACION:
        if (dtDev.getString("TIPO_OPERACION").equals("A")){
          dtRecoger.put("TIPO_OPERACION","A");
          dtRecoger.put("NM_FACRI","");
        }else{//si es modif de BD o de otra modificacion
          dtRecoger.put("TIPO_OPERACION","M");
          dtRecoger.put("NM_FACRI",dtDev.getString("NM_FACRI"));
        }
        break;
      case BAJA:
        if (dtDev.getString("TIPO_OPERACION").equals("A")){
          dtRecoger.put("TIPO_OPERACION","E");//Eliminar
        }else{//si es dar de baja una de bd o una modificada
          dtRecoger.put("TIPO_OPERACION","B");
        }
        dtRecoger.put("NM_FACRI",dtDev.getString("NM_FACRI"));
        break;
      case CONSULTA:
          dtRecoger.put("TIPO_OPERACION","C");
          dtRecoger.put("NM_FACRI",dtDev.getString("NM_FACRI"));
          break;
     }//end switch

    Lista lt1 = new Lista();
    Lista lt2 = new Lista();
    lt2.addElement(dtRecoger);
    lt1 = diaGest.calcularDatos(lt2);
    dtRecoger = (Data) lt1.elementAt(0);

/*    if ((modoOperacion != BAJA) &&
        (modoOperacion != CONSULTA))
      calcularTotalEnf();*/
    return dtRecoger;
  }//end recoger_Datos

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  public Data dtDevolver(){
    return dtDevuelto;
  }

  public boolean validarDatos(){
    boolean b = false;
    if ((txtExpEnf.getText().length()==0) ||
       (txtExpNEnf.getText().length()==0) ||
       (txtNExpEnf.getText().length()==0) ||
       (txtNExpNEnf.getText().length()==0) ||
       (txtFactor.getText().length()==0) ||
       (txtValor.getText().length()==0)){
          this.getApp().showAdvise("Debe completar todos los campos");
          b=false;
     }else{
          b=true;
     }
    return b;
  }//end validarDatos

  void btnAceptarActionPerformed(){
      if (validarDatos()){
        dtDevuelto=recogerDatos();
        bAceptar=true;
        dispose();
      }
  }

  void btnCancelarActionPerformed(){
    bAceptar=false;
    dispose();
  }

  // salir/grabar
/*  void btn_actionPerformed(ActionEvent e) {

    if (e.getActionCommand().equals("grabar")) {
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      // ajusta la ventana al modo que tenga
      switch (modoOperacion) {

        // se da de baja
        case BAJA:
          bAceptar = true;
          dispose();
          break;

        case MODIFICACION:
        case ALTA:
//          if(calcularTotales())
          bAceptar = true;
          // grabo los datos del detalle que utilizar�
          // el di�logo de gesti�n
          dtRecoger.put("DS_FACRI", (String)txtFactor.getText());
          dtRecoger.put("DS_VFACRI", (String)txtValor.getText());
          dtRecoger.put("NM_EXPENF", txtExpEnf.getText());
          dtRecoger.put("NM_EXPNOENF", txtExpNEnf.getText());
          dtRecoger.put("NM_NOEXPENF", txtNExpEnf.getText());
          dtRecoger.put("NM_NMNOEXPNOENF", txtNExpNEnf.getText());

          Lista lt1 = new Lista();
          Lista lt2 = new Lista();
          lt2.addElement((Data) dtRecoger);
          lt1 = diaGest.calcularDatos(lt2);
          dtRecoger = (Data) lt1.elementAt(0);

          if(modoOperacion != BAJA)
            calcularTotalEnf();
          recogerDatos();

          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          dispose();


          break;
      }  // fin switch


    } else if (e.getActionCommand().equals("salir")) {
      bAceptar = false;
      dispose();
    }

  }*/


  void cb_itemStateChanged() {
//    dtRecoger.put("DS_SOSP_CONF", (String) e2.getItem());
  }


  // comprobaci�n de datos v�lidos
/*  private boolean isDataValid() {
    boolean bResultado = false;
*/
/*    if (panProdu.getProducto() == null) {
        this.getApp().showAdvise("Falta introducir el producto");
     } else if (txtCantidad.getText().trim().length() == 0) {
           this.getApp().showAdvise("Falta introducir una cantidad");
       } else {
              bResultado = true;

       }
*/
/*
    if (panProdu.getProducto()==  null) {
      this.getApp().showAdvise("Falta introducir el producto");
      } else if (txtCantidad.getText().trim().length() == 0) {
          this.getApp().showAdvise("Falta la cantidad");
          } else if (txtLote.getText().trim().length() == 0 && bVacuna) {
              this.getApp().showAdvise("Falta el lote");
              } else if (txtFechaCad.getText().trim().length() == 0 && bVacuna) {
                  this.getApp().showAdvise("Falta la fecha de caducidad");
                  } else {
                      bResultado = true;
                  }

     return bResultado;
  }

*/
 } // fin clase

/*// escuchador de los botones grabar,salir
class DiaDetFactR_btn_actionAdapter implements java.awt.event.ActionListener{
  DiaDetFactR adaptee;


  DiaDetFactR_btn_actionAdapter(DiaDetFactR adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btn_actionPerformed(e);
  }
} */

class DiaDetFactRActionAdapter implements java.awt.event.ActionListener, Runnable{
  DiaDetFactR adaptee;
  ActionEvent evt;

  DiaDetFactRActionAdapter(DiaDetFactR adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run(){
    SincrEventos s = adaptee.getSincrEventos();
    if(s.bloquea(adaptee.modoBloqueo,adaptee)){
      if(evt.getActionCommand().equals("Aceptar")){
        adaptee.btnAceptarActionPerformed();
        //adaptee.btnAreaActionPerformed();
      }
      if(evt.getActionCommand().equals("Cancelar")){
        adaptee.btnCancelarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaDetFactRActionAdapter



class DiaDetFactR_cb_itemAdapter implements java.awt.event.ItemListener, Runnable {
  DiaDetFactR adaptee2;
  ItemEvent evt;

  DiaDetFactR_cb_itemAdapter(DiaDetFactR adaptee2) {
    this.adaptee2 = adaptee2;
  }

  public void itemStateChanged(ItemEvent e2) {
    this.evt = e2;
    new Thread(this).start();

  }
  public void run() {
    SincrEventos s = adaptee2.getSincrEventos();
    if(s.bloquea(adaptee2.modoBloqueo,adaptee2)){
      if(((Checkbox)evt.getSource()).getName().equals("Check")){
        adaptee2.cb_itemStateChanged();
      }
      s.desbloquea(adaptee2);
    }
  }

}


