package brotes.cliente.dapanotros;

import java.util.Hashtable;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextArea;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.diaalerta.DiaAlerta;
import brotes.cliente.suca.Suca;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;

public class DAPanOtros
    extends CPanel
    implements DatoRecogible, CInicializar {

  final int ESPERA = 4;
  final int NORMAL = 5;
  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  /****************** Componentes del panel ********************/

  //suca
  Suca pansuca = null;
  Hashtable listas = null;

  // Alerta incial
  Data alerta = null;

  //Listas de Datas de entrada a Suca para convertirlas en CLista
  Lista listaPaises = null;
  Lista listaCA = null;

  // controles
  XYLayout xYLayout = new XYLayout();
  Label lColectivo = new Label();
  CTexto txtColectivo = new CTexto(50);

  CApp applet = null;
  DiaAlerta dlg = null;
  Label lAli = new Label();
  TextArea textAreaAli = new TextArea("", 0, 0, TextArea.SCROLLBARS_NONE);
  Label lSint = new Label();
  TextArea textAreaSin = new TextArea("", 0, 0, TextArea.SCROLLBARS_NONE);
  Label lObser = new Label();
  TextArea textAreaObser = new TextArea("", 0, 0, TextArea.SCROLLBARS_NONE);
  Label lTelCol = new Label();
  CTexto txtTelCol = new CTexto(14);

  SincrEventos scr = new SincrEventos();

  // constructor
  public DAPanOtros(CApp a,
                    DiaAlerta d,
                    Data al,
                    int modo,
                    Hashtable hsCompleta) {
    super(a);
    applet = a;
    dlg = d;
    alerta = al;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    listas = hsCompleta;
    try {
      // Recuperacion de listas
      listaPaises = (Lista) hsCompleta.get("PAISES");
      listaCA = (Lista) hsCompleta.get("CA");

      jbInit();
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(429);
    xYLayout.setWidth(725);

    //Panel Suca
    pansuca = new Suca(applet, this, scr, modoEntrada, listas);

    lColectivo.setText("Colectivo afectado");
    lAli.setForeground(Color.black);
    lAli.setFont(new Font("Dialog", 0, 12));
    lAli.setText("Alimentos sospechosos");
    textAreaAli.setBackground(Color.white);
    lSint.setForeground(Color.black);
    lSint.setFont(new Font("Dialog", 0, 12));
    lSint.setText("Sintomas");
    textAreaSin.setBackground(Color.white);
    lObser.setForeground(Color.black);
    lObser.setFont(new Font("Dialog", 0, 12));
    lObser.setText("Observaciones");
    textAreaObser.setBackground(Color.white);
    lTelCol.setText("Telefono");
    this.setLayout(xYLayout);

    this.add(lColectivo, new XYConstraints(9, 5, 125, -1));

    this.add(txtColectivo, new XYConstraints(136, 5, 275, -1));
    this.add(pansuca, new XYConstraints(8, 33, 596, 116));

    this.add(lSint, new XYConstraints(10, 145, -1, -1));
    this.add(textAreaSin, new XYConstraints(10, 169, 670, 43));
    this.add(lAli, new XYConstraints(10, 217, -1, -1));
    this.add(textAreaAli, new XYConstraints(10, 242, 670, 43));
    this.add(lObser, new XYConstraints(10, 290, -1, -1));
    this.add(textAreaObser, new XYConstraints(10, 315, 670, 43));

    this.add(lTelCol, new XYConstraints(480, 7, 63, -1));
    this.add(txtTelCol, new XYConstraints(549, 5, 130, -1));

  }

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Inicializar()

  public void InicializarDesdeFuera(int mod) {
    modoOperacion = mod;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case CInicializar.ESPERA:
        enableControls(false);
        pansuca.InicializarDesdeFuera(CInicializar.ESPERA);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        switch (modoEntrada) {
          case ALTA:
            enableControls(true);
            pansuca.InicializarDesdeFuera(modoOperacion);
            break;
          case MODIFICACION:
            int sit = new Integer(alerta.getString("CD_SITALERBRO")).intValue();
            switch (sit) {
              case 0:
                enableControls(true);
                pansuca.setModoEntrada(modoEntrada);
                pansuca.InicializarDesdeFuera(modoOperacion);
                break;
              case 1:
              case 2:
              case 3:
              case 4:
                enableControls(false);
                pansuca.setModoEntrada(CONSULTA);
                pansuca.InicializarDesdeFuera(modoOperacion);
                break;
            }
            break;

          case BAJA:
          case CONSULTA:
            enableControls(false);
            pansuca.InicializarDesdeFuera(modoOperacion);
            break;
        } // Fin switch modoEntrada
        break;
    } // Fin switch modoOperacion
  }

  private void enableControls(boolean en) {

    txtColectivo.setEnabled(en);
    txtTelCol.setEnabled(en);
    textAreaAli.setEnabled(en);
    textAreaSin.setEnabled(en);
    textAreaObser.setEnabled(en);
  }

  public void recogerDatos(Data dtDev) {

    dtDev.put("DS_NOMCOL", txtColectivo.getText().trim());
    dtDev.put("DS_TELCOL", txtTelCol.getText().trim());

    // Recogida de datos de sucaNot
    Data dtsuca = new Data();
    pansuca.recogerDatos(dtsuca);
    dtDev.put("CD_PROV", (String) dtsuca.getString("PROV"));
    dtDev.put("CD_MUN", (String) dtsuca.getString("MUN"));
    dtDev.put("DS_DIRCOL", (String) dtsuca.getString("VIA"));
    dtDev.put("DS_NMCALLE", (String) dtsuca.getString("NUM"));
    dtDev.put("DS_PISOCOL", (String) dtsuca.getString("PISO"));
    dtDev.put("CD_POSTALCOL", (String) dtsuca.getString("CODPOSTAL"));

    dtDev.put("DS_ALISOSP", textAreaAli.getText().trim());
    dtDev.put("DS_SINTOMAS", textAreaSin.getText().trim());
    dtDev.put("DS_OBSERV", textAreaObser.getText().trim());
  }

  public boolean validarDatos() {
    //suca  (long, Num)
    if (!pansuca.validarDatos()) {
      return false;
    }

    if (textAreaAli.getText().length() > 1000) {
      this.getApp().showAdvise(
          "La longitud del texto de Alimentos sospechosos excede de 1000 caracteres");
      textAreaAli.setText("");
      textAreaAli.requestFocus();
      return false;
    }

    if (textAreaSin.getText().length() > 1000) {
      this.getApp().showAdvise(
          "La longitud del texto de Sintomas excede de 1000 caracteres");
      textAreaSin.setText("");
      textAreaSin.requestFocus();
      return false;
    }

    if (textAreaObser.getText().length() > 1000) {
      this.getApp().showAdvise(
          "La longitud del texto de Observaciones excede de 1000 caracteres");
      textAreaObser.setText("");
      textAreaObser.requestFocus();
      return false;
    }

    return true;
  }

  public void rellenarDatos(Data dtReg) {

    txtColectivo.setText(dtReg.getString("DS_NOMCOL"));
    txtTelCol.setText(dtReg.getString("DS_TELCOL"));

    // Relleno de pansuca
    Data dtSuca = new Data();
    dtSuca.put("CA", dtReg.getString("CD_CA"));
    dtSuca.put("PROV", dtReg.getString("CD_PROV"));
    dtSuca.put("MUN", dtReg.getString("CD_MUN"));
    dtSuca.put("VIA", dtReg.getString("DS_DIRCOL"));
    dtSuca.put("NUM", dtReg.getString("DS_NMCALLE"));
    dtSuca.put("PISO", dtReg.getString("DS_PISOCOL"));
    dtSuca.put("CODPOSTAL", dtReg.getString("CD_POSTALCOL"));
    pansuca.rellenarDatos(dtSuca);

    textAreaAli.setText(dtReg.getString("DS_ALISOSP"));
    textAreaObser.setText(dtReg.getString("DS_OBSERV"));
    textAreaSin.setText(dtReg.getString("DS_SINTOMAS"));
  } //fin

  /**************** Funciones de acceso a vars. del panel *******/
  public void cambiarAlerta(Data newAlerta) {
    alerta = newAlerta;
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    modoEntrada = m;
  }

} //clase
