// Modificado el 02/12/1999 para hacer p�blico el m�todo
// lanzado al perderse el foco en Nivel 2

package brotes.cliente.pannivelesbro;

import java.util.Vector;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CCargadorImagen;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CPanel;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

public class PanNivelesBro
    extends CPanel
    implements CInicializar {

  /*************** Datos propios del panel ****************/

  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;
  final int ESPERA = 4;
  final int NORMAL = 5;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Sincronizador de Eventos
  private SincrEventos scr = null;
  //Panel
  CInicializar pan = null;

  //Estados de los componenetes
  boolean bNivel1 = false;
  boolean bNivel2 = false;
  boolean bNivel4 = false;
  boolean bValidos = false;

  // Para controlar el cambio de las cajas de texto
  private String strCDNivel1 = "";
  private String strCDNivel2 = "";
  private String strCDNivel4 = "";

  //applet
  CApp applet = null;

  // Controles
  TextField txtNivel1 = null;
  TextField txtNivel2 = null;
  TextField txtNivel4 = null;

  TextField txtNivel1L = null;
  TextField txtNivel2L = null;
  TextField txtNivel4L = null;

  /*
     String antantTxtNivel1L = new String();
     String antantTxtNivel2L = new String();
     String antantTxtNivel4L = new String();
     String antTxtNivel1L = new String();
     String antTxtNivel2L = new String();
     String antTxtNivel4L = new String();
   */

  ButtonControl btnNivel1 = null;
  ButtonControl btnNivel2 = null;
  ButtonControl btnNivel4 = null;

  Label lblNivel1 = null;
  Label lblNivel2 = null;
  Label lblNivel4 = null;

  // Gestores de eventos
  PanelNivelesBtnActionListener btnActionListener = new
      PanelNivelesBtnActionListener(this);
  //PanelNivelesTextListener textListener = new PanelNivelesTextListener(this);
  PanelNivelesKeyListener keyReleased = new PanelNivelesKeyListener(this);
  PanelNivelesTextFocusListener txtFocusListener = new
      PanelNivelesTextFocusListener(this);
  XYLayout xyFondo = new XYLayout();

  // M�todos de lectura

  public String getCDNivel1() {
    return txtNivel1.getText().trim();
  }

  public String getCDNivel2() {
    String strValor = null;
    strValor = txtNivel2.getText().trim();
    return strValor;
  }

  public String getCDNivel4() {
    String strValor = null;
    strValor = txtNivel4.getText().trim();
    return strValor;
  }

  public String getDSNivel1() {
    return txtNivel1L.getText().trim();
  }

  public String getDSNivel2() {
    String strValor = null;
    strValor = txtNivel2L.getText().trim();
    return strValor;
  }

  public String getDSNivel4() {
    String strValor = null;
    strValor = txtNivel4L.getText().trim();
    return strValor;
  }

  public void setCDNivel1(String strValor) {
    txtNivel1.setText(strValor.trim());
  }

  /*
     // E Para cargar el nivel 1 y activar los controles
     // pertinentes
     public void setBuscaCDNivel1(String strValor) {
    sinBloquear = false;
    txtNivel1.setText(strValor.trim());
    txtNivel2.setEnabled(true);
    btnNivel2.setEnabled(true);
    modoOperacion = MODO_N1;
    Inicializar();
    sinBloquear = true;
     }
   */

  public void setCDNivel2(String strValor) {
    txtNivel2.setText(strValor.trim());
  }

  public void setCDNivel4(String strValor) {
    txtNivel4.setText(strValor.trim());
  }

  public void setDSNivel1(String strValor) {
    txtNivel1L.setText(strValor.trim());
  }

  public void setDSNivel2(String strValor) {
    txtNivel2L.setText(strValor.trim());
  }

  public void setDSNivel4(String strValor) {
    txtNivel4L.setText(strValor.trim());
  }

  //****************************************************************
   // Gesti�n del estado habilitado/deshabilitado de los componentes
   public void Inicializar(int modo) {
     modoOperacion = modo;
     if (pan != null) {
       pan.Inicializar(modoOperacion);
     }
   } // Fin Inicializar()

  public void InicializarDesdeFuera(int mod) {
    modoOperacion = mod;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      // modo espera
      case CInicializar.ESPERA:
        txtNivel1.setEnabled(false);
        btnNivel1.setEnabled(false);
        txtNivel2.setEnabled(false);
        btnNivel2.setEnabled(false);
        txtNivel4.setEnabled(false);
        btnNivel4.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
            if (!bNivel1) {
              txtNivel1.setEnabled(true);
              btnNivel1.setEnabled(true);
              txtNivel2.setEnabled(false);
              btnNivel2.setEnabled(false);
              txtNivel4.setEnabled(false);
              btnNivel4.setEnabled(false);
            }
            else {
              txtNivel1.setEnabled(true);
              btnNivel1.setEnabled(true);
              txtNivel2.setEnabled(true);
              btnNivel2.setEnabled(true);
              txtNivel4.setEnabled(false);
              btnNivel4.setEnabled(false);
            }
            if (bNivel2) {
              txtNivel1.setEnabled(true);
              btnNivel1.setEnabled(true);
              txtNivel2.setEnabled(true);
              btnNivel2.setEnabled(true);
              txtNivel4.setEnabled(true);
              btnNivel4.setEnabled(true);
            }

            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;

          case MODIFICACION:
            if (!bNivel1) {
              txtNivel1.setEnabled(true);
              btnNivel1.setEnabled(true);
              txtNivel2.setEnabled(false);
              btnNivel2.setEnabled(false);
              txtNivel4.setEnabled(false);
              btnNivel4.setEnabled(false);
            }
            else {
              txtNivel1.setEnabled(true);
              btnNivel1.setEnabled(true);
              txtNivel2.setEnabled(true);
              btnNivel2.setEnabled(true);
              txtNivel4.setEnabled(false);
              btnNivel4.setEnabled(false);
            }
            if (bNivel2) {
              txtNivel1.setEnabled(true);
              btnNivel1.setEnabled(true);
              txtNivel2.setEnabled(true);
              btnNivel2.setEnabled(true);
              txtNivel4.setEnabled(true);
              btnNivel4.setEnabled(true);
            }
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;

          case BAJA:
            txtNivel1.setEnabled(false);
            btnNivel1.setEnabled(false);
            txtNivel2.setEnabled(false);
            btnNivel2.setEnabled(false);
            txtNivel4.setEnabled(false);
            btnNivel4.setEnabled(false);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;

          case CONSULTA:
            txtNivel1.setEnabled(false);
            btnNivel1.setEnabled(false);
            txtNivel2.setEnabled(false);
            btnNivel2.setEnabled(false);
            txtNivel4.setEnabled(false);
            btnNivel4.setEnabled(false);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;
        }
    }
  }

  public PanNivelesBro(CApp a, CInicializar p, int modo, SincrEventos sin) {
    applet = a;

    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    pan = p;
    scr = sin;
    try {
      jbInit();
      Inicializar();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        "images/Magnify.gif"};

    imgs = new CCargadorImagen(applet, imgNAME);
    imgs.CargaImagenes();

    this.setSize(new Dimension(510, 94));
    this.setBorde(false);
    this.setLayout(xyFondo);

    txtNivel1 = new TextField();
    txtNivel1.addFocusListener(txtFocusListener);
    //txtNivel1.addTextListener(textListener);
    txtNivel1.addKeyListener(keyReleased);
    txtNivel1.setName("nivel1");

    btnNivel1 = new ButtonControl();
    btnNivel1.addActionListener(btnActionListener);
    btnNivel1.setActionCommand("nivel1");
    btnNivel1.setImage(imgs.getImage(0));

    txtNivel1L = new TextField();
    txtNivel1L.setEditable(false);
    txtNivel1L.setEnabled(false);
    //txtNivel1L.addTextListener(textDesListener);
    txtNivel1L.setName("desNivel1");

    lblNivel1 = new Label();
    lblNivel1.setText("Area");

    this.add(lblNivel1, new XYConstraints(4, 6, 80, 19));
    this.add(txtNivel1, new XYConstraints(85, 6, 38, 22));
    this.add(btnNivel1, new XYConstraints(125, 6, -1, -1));
    this.add(txtNivel1L, new XYConstraints(155, 6, 1, 1));
    txtNivel1L.setVisible(false);

    txtNivel2 = new TextField();
    txtNivel2.addFocusListener(txtFocusListener);
    //txtNivel2.addTextListener(textListener);
    txtNivel2.addKeyListener(keyReleased);
    txtNivel2.setName("nivel2");

    btnNivel2 = new ButtonControl();
    btnNivel2.addActionListener(btnActionListener);
    btnNivel2.setActionCommand("nivel2");
    btnNivel2.setImage(imgs.getImage(0));

    txtNivel2L = new TextField();
    txtNivel2L.setEditable(false);
    txtNivel2L.setEnabled(false);
    //txtNivel2L.addTextListener(textDesListener);
    txtNivel2L.setName("desNivel2");

    lblNivel2 = new Label();
    lblNivel2.setText("Distrito");

    this.add(lblNivel2, new XYConstraints(202, 6, 38, 21));
    this.add(txtNivel2, new XYConstraints(245, 6, 38, 22));
    this.add(btnNivel2, new XYConstraints(285, 6, 26, 26)); // E 26, 26

    this.add(txtNivel2L, new XYConstraints(315, 6, 1, 1));

    txtNivel4 = new TextField();
    txtNivel4.addFocusListener(txtFocusListener);
    //txtNivel4.addTextListener(textListener);
    txtNivel4.setName("nivel4");
    txtNivel4.setEnabled(false);

    btnNivel4 = new ButtonControl();
    btnNivel4.addActionListener(btnActionListener);
    btnNivel4.setActionCommand("nivel4");
    btnNivel4.setImage(imgs.getImage(0));

    lblNivel4 = new Label();
    lblNivel4.setText("Zona");

    txtNivel4L = new TextField();
    txtNivel4L.setEditable(false);
    txtNivel4L.setEnabled(false);
    //txtNivel4L.addTextListener(textDesListener);
    txtNivel4L.setName("desNivel4");

    this.add(lblNivel4, new XYConstraints(376, 6, 28, 21));
    this.add(txtNivel4, new XYConstraints(405, 6, 38, 22));
    this.add(btnNivel4, new XYConstraints(445, 6, -1, -1));
    this.add(txtNivel4L, new XYConstraints(475, 6, 175, 22));

    xyFondo.setHeight(40);
    xyFondo.setWidth(655);
  }

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

//**************************************************************************
//          M�todos asociados a la gesti�n de eventos
//**************************************************************************

    // Operaciones realizadas al pulsar en las cajas de texto.
    protected void txtValueChangeNivel1() {
      txtNivel2.setText("");
      txtNivel4.setText("");
      txtNivel4L.setText("");
      bNivel1 = false;
      bNivel2 = false;
      Inicializar();
    }

  protected void txtValueChangeNivel2() {
    txtNivel4.setText("");
    txtNivel4L.setText("");
    bNivel1 = true;
    bNivel2 = false;
    Inicializar();
  }

  // Nivel 1
  protected void btnActionNivel1() {
    CListaValores clv = null;
    QueryTool qt = null;
    Vector v = null;
    Data dtNiv1 = null;
    String sNiv1 = txtNivel1.getText().trim();
    String sNiv2 = txtNivel2.getText().trim();
    String sNiv4 = txtNivel4.getText().trim();

    // configura el lector de centros
    qt = new QueryTool();

    // centros de vacunacion
    qt.putName("SIVE_NIVEL1_S");

    // campos que se leen
    qt.putType("CD_NIVEL_1", QueryTool.STRING);
    qt.putType("DS_NIVEL_1", QueryTool.STRING);
    qt.putType("DSL_NIVEL_1", QueryTool.STRING);
    qt.putType("DS_EMAIL_1", QueryTool.STRING);
    qt.putType("DS_EMAIL_2", QueryTool.STRING);
    qt.putType("DS_EMAIL_3", QueryTool.STRING);

    // campos que se muestran en la lista de valores
    v = new Vector();
    v.addElement("CD_NIVEL_1");
    v.addElement("DS_NIVEL_1");

    // lista de valores
    clv = new CListaValores(applet,
                            "Selecci�n de Area",
                            qt,
                            v);
    clv.show();

    // recupera el centro seleccionado
    if (clv.getSelected() != null) {
      //borrado del contenido de las cajas
      txtNivel1.setText("");
      txtNivel2.setText("");
      txtNivel4.setText("");
      txtNivel4L.setText("");

      dtNiv1 = new Data();
      dtNiv1 = clv.getSelected();
      String sCDNiv1 = dtNiv1.getString("CD_NIVEL_1");
      String sDSNiv1 = dtNiv1.getString("DS_NIVEL_1");
      txtNivel1.setText(sCDNiv1);
      txtNivel1L.setText(sDSNiv1);
      bNivel1 = true;
      bNivel2 = false;
      Inicializar();

    }

    clv = null;

  }

  public void txtFocusNivel1() {

    QueryTool qt1 = null;
    Lista vNiv1 = null;
    //Data dtNiv1 = null;
    // lista para el filtro
    Lista vFiltro = null;

    String sCD_NIVEL_1 = txtNivel1.getText().trim();

    if (!sCD_NIVEL_1.equals("")) {

      // configura el lector de niveles
      qt1 = new QueryTool();
      vNiv1 = new Lista();
      vFiltro = new Lista();

      // centros de vacunacion
      qt1.putName("SIVE_NIVEL1_S");

      // campos que se escriben
      qt1.putType("DS_NIVEL_1", QueryTool.STRING);

      qt1.putWhereType("CD_NIVEL_1", QueryTool.STRING);
      qt1.putWhereValue("CD_NIVEL_1", sCD_NIVEL_1);
      qt1.putOperator("CD_NIVEL_1", "=");

      // realiza la consulta al servlet
      try {

        applet.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        vFiltro.addElement(qt1);
        vNiv1 = (Lista) applet.getStub().doPost(1, vFiltro);

        // muestra incidencia
        if (vNiv1.size() == 0) {
          applet.showAdvise("El Area introducida no existe.");
          bNivel1 = false;
        }
        else {
          bNivel1 = true;
        }

        // error en el servlet
      }
      catch (Exception ex) {
        applet.trazaLog(ex);
        applet.showError(ex.getMessage());
      }

      //Inicializar(CInicializar.NORMAL);
      //Inicializar();
    }
    else {
      applet.showAdvise("Por favor, introduzca el �rea.");
    }
  }

  public void setModoEntrada(int m) {
    modoEntrada = m;
  } // Fin setModoEntrada

  //********** Nivel 2
   protected void btnActionNivel2() {
     CListaValores clv2 = null;
     QueryTool qt2 = null;
     Vector v2 = null;
     Data dtNiv2 = null;

     // configura el lector de centros
     qt2 = new QueryTool();

     // centros de vacunacion
     qt2.putName("SIVE_NIVEL2_S");

     // campos que se leen
     qt2.putType("CD_NIVEL_2", QueryTool.STRING);
     qt2.putType("DS_NIVEL_2", QueryTool.STRING);
     qt2.putType("DSL_NIVEL_2", QueryTool.STRING);
     qt2.putType("DS_EMAIL_1", QueryTool.STRING);
     qt2.putType("DS_EMAIL_2", QueryTool.STRING);
     qt2.putType("DS_EMAIL_3", QueryTool.STRING);

     // filtro de �reas
     qt2.putWhereType("CD_NIVEL_1", QueryTool.STRING);
     qt2.putWhereValue("CD_NIVEL_1", txtNivel1.getText());
     qt2.putOperator("CD_NIVEL_1", "=");

     // campos que se muestran en la lista de valores
     v2 = new Vector();
     v2.addElement("CD_NIVEL_2");
     v2.addElement("DS_NIVEL_2");

     // lista de valores
     clv2 = new CListaValores(applet,
                              "Selecci�n de Distrito",
                              qt2,
                              v2);
     clv2.show();

     // recupera el centro seleccionado
     if (clv2.getSelected() != null) {
       txtNivel2.setText("");
       txtNivel4.setText("");
       txtNivel4L.setText("");

       dtNiv2 = new Data();
       dtNiv2 = clv2.getSelected();
       String sCDNiv2 = dtNiv2.getString("CD_NIVEL_2");
       String sDSNiv2 = dtNiv2.getString("DS_NIVEL_2");
       txtNivel2.setText(sCDNiv2);
       txtNivel2L.setText(sDSNiv2);
       bNivel2 = true;
       Inicializar();
     }

     clv2 = null;
   }

  public void txtFocusNivel2() {

    QueryTool qt2 = null;
    Lista vNiv2 = null;
    // lista para el filtro
    Lista vFiltro = new Lista();

    String sCD_NIVEL_2 = txtNivel2.getText().trim();

    if (!sCD_NIVEL_2.equals("")) {

      //Inicializar(CInicializar.ESPERA);

      // configura el lector de centros
      qt2 = new QueryTool();
      vNiv2 = new Lista();
      vFiltro = new Lista();

      // centros de vacunacion
      qt2.putName("SIVE_NIVEL2_S");

      // campos que se leen
      qt2.putType("DS_NIVEL_2", QueryTool.STRING);

      // filtro de �reas
      qt2.putWhereType("CD_NIVEL_2", QueryTool.STRING);
      qt2.putWhereValue("CD_NIVEL_2", sCD_NIVEL_2);
      qt2.putOperator("CD_NIVEL_2", "=");

      // realiza la consulta al servlet
      try {

        applet.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        vFiltro.addElement(qt2);
        vNiv2 = (Lista) applet.getStub().doPost(1, vFiltro);

        // muestra incidencia
        if (vNiv2.size() == 0) {
          applet.showAdvise("El Distrito introducido no existe.");
          bNivel2 = false;
        }
        else {
          bNivel2 = true;
        }

        // error en el servlet
      }
      catch (Exception ex) {
        applet.trazaLog(ex);
        applet.showError(ex.getMessage());
      }

      //Inicializar(CInicializar.NORMAL);
      //Inicializar();
    }
    else {
      applet.showAdvise("Pro favor, introduzca el distrito.");
    }
  }

  // Nivel 4
  protected void btnActionNivel4() {
    CListaValores clv4 = null;
    QueryTool qt4 = null;
    Vector v4 = null;
    Data dtNiv4 = null;

    // configura el lector de centros
    qt4 = new QueryTool();

    // centros de vacunacion
    qt4.putName("SIVE_ZONA_BASICA");

    // campos que se leen
    qt4.putType("CD_ZBS", QueryTool.STRING);
    qt4.putType("DS_ZBS", QueryTool.STRING);
    qt4.putType("DSL_ZBS", QueryTool.STRING);

    // filtro de �reas
    qt4.putWhereType("CD_NIVEL_1", QueryTool.STRING);
    qt4.putWhereValue("CD_NIVEL_1", txtNivel1.getText());
    qt4.putOperator("CD_NIVEL_1", "=");

    // filtro de zonas
    qt4.putWhereType("CD_NIVEL_2", QueryTool.STRING);
    qt4.putWhereValue("CD_NIVEL_2", txtNivel2.getText());
    qt4.putOperator("CD_NIVEL_2", "=");

    // campos que se muestran en la lista de valores
    v4 = new Vector();
    v4.addElement("CD_ZBS");
    v4.addElement("DS_ZBS");

    // lista de valores
    clv4 = new CListaValores(applet,
                             "Selecci�n de Zona B�sica",
                             qt4,
                             v4);
    clv4.show();

    // recupera el centro seleccionado
    if (clv4.getSelected() != null) {
      dtNiv4 = new Data();
      dtNiv4 = clv4.getSelected();
      String sCDNiv4 = dtNiv4.getString("CD_ZBS");
      String sDSNiv4 = dtNiv4.getString("DS_ZBS");
      txtNivel4.setText(sCDNiv4);
      txtNivel4L.setText(sDSNiv4);
    }

    clv4 = null;
    bNivel2 = true;
    Inicializar();
  }

  protected void txtFocusNivel4() {
    CListaValores clv4 = null;
    QueryTool qt4 = null;
    Lista vNiv4 = null;
    Data dtNiv4 = null;
    // lista para el filtro
    Lista vFiltro = new Lista();
    String sCD_ZBS = txtNivel4.getText().trim();

    if (!sCD_ZBS.equals("")) {

      //Inicializar(CInicializar.ESPERA);
      // configura el lector de centros
      qt4 = new QueryTool();
      vNiv4 = new Lista();
      vFiltro = new Lista();

      // centros de vacunacion
      qt4.putName("SIVE_ZONA_BASICA");

      // campos que se leen
      qt4.putType("DS_ZBS", QueryTool.STRING);

      // filtro de �reas
      qt4.putWhereType("CD_NIVEL_1", QueryTool.STRING);
      qt4.putWhereValue("CD_NIVEL_1", txtNivel1.getText());
      qt4.putOperator("CD_NIVEL_1", "=");

      // filtro de distrito
      qt4.putWhereType("CD_NIVEL_2", QueryTool.STRING);
      qt4.putWhereValue("CD_NIVEL_2", txtNivel2.getText());
      qt4.putOperator("CD_NIVEL_2", "=");

      // filtro de zonas
      qt4.putWhereType("CD_ZBS", QueryTool.STRING);
      qt4.putWhereValue("CD_ZBS", txtNivel4.getText());
      qt4.putOperator("CD_ZBS", "=");

      // realiza la consulta al servlet
      try {

        applet.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        vFiltro.addElement(qt4);
        vNiv4 = (Lista) applet.getStub().doPost(1, vFiltro);

        // muestra incidencia
        if (vNiv4.size() == 0) {

          applet.showAdvise("No existe Zona B�sica para el c�digo introducido.");
          txtNivel4L.setText("");
          bNivel4 = false;
        }
        else {
          dtNiv4 = new Data();
          dtNiv4 = (Data) vNiv4.elementAt(0);
          String sDS_ZBS = dtNiv4.getString("DS_ZBS");
          txtNivel4L.setText(sDS_ZBS);
          bNivel4 = true;
        }

        // error en el servlet
      }
      catch (Exception ex) {
        applet.trazaLog(ex);
        applet.showError(ex.getMessage());
      }

      //Inicializar(CInicializar.NORMAL);
      //Inicializar();
    }
    else {
      applet.showAdvise("Pro favor, introduzca la zona.");
    }
  }

} // FIN CLASE panelNiveles

//**************************************************************************
//          Clases para la gesti�n de eventos
//**************************************************************************

// Para comunicar al los contenedores de este panel, que implementan
// el interfaz usaPanelNiveles, que ha habido un cambio en alguno de los niveles
  /*
   class PanelNivelesDesTextListener implements java.awt.event.TextListener {
    PanNivelesBro adaptee = null;
    TextEvent e = null;
    PanelNivelesDesTextListener(PanNivelesBro adaptee) {
      this.adaptee = adaptee;
    }
    public void textValueChanged(java.awt.event.TextEvent e) {
    }
   }
   */

  class PanelNivelesKeyListener
      implements java.awt.event.KeyListener, Runnable {
    PanNivelesBro adaptee = null;
    KeyEvent evt = null;

    PanelNivelesKeyListener(PanNivelesBro adaptee) {
      this.adaptee = adaptee;
    }

    public void keyPressed(java.awt.event.KeyEvent e) {}

    public void keyTyped(java.awt.event.KeyEvent e) {}

    //public void textValueChanged(java.awt.event.KeyEvent e) {
    public void keyReleased(java.awt.event.KeyEvent e) {
      this.evt = e;
      new Thread(this).start();
    }

    public void run() {
      //SincrEventos s = adaptee.getSincrEventos();
      //if(s.bloquea(adaptee.modoOperacion,adaptee)){
      String name2 = ( (Component) evt.getSource()).getName();
      if (name2.equals("nivel1")) {
        adaptee.txtValueChangeNivel1();
      }
      else if (name2.equals("nivel2")) {
        adaptee.txtValueChangeNivel2();
      }
      //  s.desbloquea(adaptee);
      //}
    }

  }

// P�rdida de foco en las cajas de texto
class PanelNivelesTextFocusListener
    implements java.awt.event.FocusListener, Runnable {
  PanNivelesBro adaptee;
  FocusEvent evt = null;

  PanelNivelesTextFocusListener(PanNivelesBro adaptee) {
    this.adaptee = adaptee;
  }

  public void focusGained(FocusEvent e) {}

  /*
     public void focusGained(FocusEvent e) {
     this.evt = e;
     new Thread(this).start();
     }
     public void run(){
   //SincrEventos s = adaptee.getSincrEventos();
   //if(s.bloquea(adaptee.modoOperacion,adaptee)){
     String name2 = ((Component)evt.getSource()).getName();
     if (name2.equals("nivel1")){
       adaptee.txtGotFocusNivel1();
     }else if (name2.equals("nivel2")){
       adaptee.txtGotFocusNivel2();
     }
    // s.desbloquea(adaptee);
   //}
     }
   */

  public void focusLost(FocusEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {

      String name3 = ( (Component) evt.getSource()).getName();
      if (name3.equals("nivel1")) {
        adaptee.txtFocusNivel1();
      }
      else if (name3.equals("nivel2")) {
        adaptee.txtFocusNivel2();
      }
      else if (name3.equals("nivel4")) {
        adaptee.txtFocusNivel4();
      }
      s.desbloquea(adaptee);
    }
  }

} //_______________________________________________ END_CLASS

// Eventos sobre los botones
class PanelNivelesBtnActionListener
    implements ActionListener, Runnable {
  PanNivelesBro adaptee = null;
  ActionEvent evt = null;

  public PanelNivelesBtnActionListener(PanNivelesBro adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {

      String name = evt.getActionCommand();
      //Lista lalista= null;
      //String campo = null;

      if (name.equals("nivel1")) {
        adaptee.btnActionNivel1();
      }
      else if (name.equals("nivel2")) {
        adaptee.btnActionNivel2();
      }
      else if (name.equals("nivel4")) {
        adaptee.btnActionNivel4();
      }
      s.desbloquea(adaptee);
    }
  }

} //__________________________________________________ END_CLASS
