package brotes.cliente.zonificacion;

import java.util.Calendar;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CCargadorImagen;
import capp.CMessage;
import capp.CPanel;
import fechas.conversorfechas;
import panniveles.panelNiveles;
import panniveles.usaPanelNiveles;

public class Pan_NivelesAnyo
    extends CPanel
    implements usaPanelNiveles {

  boolean esAceptado = false;
  boolean bAnyoValid = false;
  int modoOperacion;
  int ultimo;

  final int modoNORMAL = 0;
  final int modoESPERA = 2;

  panelNiveles niveles;
  XYLayout xYLayout1 = new XYLayout();
  Label lblAnyo = new Label();
  TextField txtAnyo = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Para controlar el cambio de a�o
  private String sAnyo = "";

  public Pan_NivelesAnyo(capp.CApp a, String elAnyo, String cdArea,
                         String dsArea, String cdDist, String dsDist) {
    this.app = a;
    niveles = new panelNiveles(a,
                               this,
                               panelNiveles.MODO_INICIO,
                               panelNiveles.TIPO_NIVEL_1,
                               panelNiveles.LINEAS_3,
                               true, true);
    try {
      if (elAnyo.length() == 0) {
        Calendar cal = Calendar.getInstance();
        ultimo = cal.get(cal.YEAR);
      }
      else {
        ultimo = Integer.parseInt(elAnyo);
      }
      niveles.setCDNivel1(cdArea);
//      niveles.setCDNivel2(cdDist);
      niveles.setDSNivel1(dsArea);
//      niveles.setDSNivel2(dsDist);
      jbInit();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    setBorde(false);
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        "images/Aceptar.gif",
        "images/Cancelar.gif"};

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setSize(450, 200);
    xYLayout1.setHeight(200);
    xYLayout1.setWidth(450);

    this.setLayout(xYLayout1);
    lblAnyo.setText("A�o");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new Pan_NivelesAnyo_btnAceptar_actionAdapter(this));
    btnCancelar.setLabel("Cancelar");
    btnCancelar.addActionListener(new Pan_NivelesAnyo_btnCancelar_actionAdapter(this));
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    this.add(niveles, new XYConstraints( -40, 5, -1, 33));
    this.add(lblAnyo, new XYConstraints(3, 47, -1, -1));
    this.add(txtAnyo, new XYConstraints(63, 47, 66, -1));
//    this.add(lblAnyo, new XYConstraints(8, 69, -1, -1));
//    this.add(txtAnyo, new XYConstraints(63, 69, 66, -1));
    this.add(btnAceptar, new XYConstraints(230, 90, -1, -1));
    this.add(btnCancelar, new XYConstraints(313, 90, -1, -1));

    lblAnyo.setAlignment(java.awt.Label.RIGHT);

    txtAnyo.setText(Integer.toString(ultimo));
    txtAnyo.addFocusListener(new Pan_NivelesAnyo_txtAnyo_focusAdapter(this));
  }

  void txtAnyo_focusGained() {
    sAnyo = txtAnyo.getText().trim();
  }

  void txtAnyo_focusLost() {
    //if (txtAnyo.getText().compareTo(Integer.toString(ultimo)) != 0){
    String sDato = "";
    sDato = txtAnyo.getText().trim();

    if (sDato.equals(sAnyo)) {
      return;
    }

    modoOperacion = modoESPERA;
    Inicializar();

    // Se comprueba que el a�o est� relleno y con formato AAAA
    if (sDato.length() == 0) {
      bAnyoValid = false;
      txtAnyo.setText(Integer.toString(ultimo));
      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }
    if (sDato.length() != 4) {
      bAnyoValid = false;
      txtAnyo.setText(Integer.toString(ultimo));
      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // comprueba que el a�o introducido sea correcto
    int iAnno = 0;
    try {
      iAnno = new Integer(sDato).intValue();

      // se comprueba que no sea un entero negativo
      if (iAnno < 0) {
        bAnyoValid = false;
        txtAnyo.setText(Integer.toString(ultimo));
        modoOperacion = modoNORMAL;
        Inicializar();

        return;
      }

    }
    catch (java.lang.NumberFormatException e) {
      bAnyoValid = false;
      txtAnyo.setText(Integer.toString(ultimo));
      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // se comprueba que el a�o se encuentre dentro de los a�os generados

    conversorfechas conv = new conversorfechas(sDato, app);
    if (!conv.anoValido()) {
      bAnyoValid = false;
      txtAnyo.setText(Integer.toString(ultimo));

      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // si no ha salido por ninguna de las condiciones anteriores,
    // el a�o es correcto y se pone su boolean a true
    bAnyoValid = true;
    ultimo = Integer.parseInt(txtAnyo.getText());
    if (!esAceptado) { //si no hemos pulsado aceptar
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    //}
  }

  private void ShowWarning(String sMessage) {
    CMessage msgBox;

    msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:

        txtAnyo.setEnabled(true);
        niveles.setModoNormal();
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        txtAnyo.setEnabled(false);
        niveles.setModoEspera();
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    this.doLayout();
  }

  boolean isDataValid() {
    boolean miValor = true;
    txtAnyo_focusLost();

    if (txtAnyo.getText().trim().equals("")) {
      ShowWarning("Introduzca el a�o.");
      txtAnyo.setText(Integer.toString(ultimo));
      txtAnyo.select(txtAnyo.getText().length(), txtAnyo.getText().length());
      txtAnyo.requestFocus();
      miValor = false;
    }
    if ( (!txtAnyo.getText().trim().equals("")) && (!bAnyoValid)) {
      ShowWarning("Introduzca un a�o v�lido.");
      txtAnyo.setText(Integer.toString(ultimo));
      txtAnyo.select(txtAnyo.getText().length(), txtAnyo.getText().length());
      txtAnyo.requestFocus();
      miValor = false;
    }
    return miValor;
  }

  void btnAceptar_actionPerformed(ActionEvent e) {
    modoOperacion = modoESPERA;
    Inicializar();

    this.esAceptado = this.isDataValid();

    if (esAceptado) {
      // alteramos el valor de las variables del sistema
      this.app.setANYO_DEFECTO(this.txtAnyo.getText());

      this.app.setCD_NIVEL1_DEFECTO(this.niveles.getCDNivel1());
      this.app.setDS_NIVEL1_DEFECTO(this.niveles.getDSNivel1());
//      this.app.setCD_NIVEL2_DEFECTO(this.niveles.getCDNivel2());
//      this.app.setDS_NIVEL2_DEFECTO(this.niveles.getDSNivel2());

      // Esta chapuza es necesaria hasta que se vea la forma
      // de que las variables est�ticas se comporten como tales
      java.applet.Applet P = (java.applet.Applet) app.getAppletContext().
          getApplet("menu");

      P.showStatus( (this.niveles.getCDNivel1().equals("") ? "#" :
                     this.niveles.getCDNivel1()) +
                   "$" +
                   (this.niveles.getDSNivel1().equals("") ? "#" :
                    this.niveles.getDSNivel1()) +
                   "$" +
          /*(this.niveles.getCDNivel2().equals("") ? "#" : this.niveles.getCDNivel2()) +
                             "$" +
           (this.niveles.getDSNivel2().equals("") ? "#" : this.niveles.getDSNivel2()) +
                             "$" +*/
          (this.txtAnyo.getText().equals("") ? "#" : this.txtAnyo.getText()) +
          "$");
      // Fin Chapuza

    }
    modoOperacion = modoNORMAL;
    Inicializar();
    cerrarPagina();
  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    this.esAceptado = false;
    this.txtAnyo.setText(Integer.toString(ultimo));
    cerrarPagina();
  }

  private void cerrarPagina() {
    try {
      this.app.getAppletContext().showDocument(new java.net.URL(app.getCodeBase(),
          "default.html"), "_self");
    }
    catch (Exception ex) {}

  }

  boolean esOK() {
    return esAceptado;
  }

  String getCodArea() {
    return this.niveles.getCDNivel1();
  }

  String getDescArea() {
    return this.niveles.getDSNivel1();
  }

  /*  String getCodDistrito(){
      return this.niveles.getCDNivel2();
    }
    String getDescDistrito(){
      return this.niveles.getDSNivel2();
    }*/

  //Para el interfaz usaPanelNiveles
  public void cambioNivelAntesInformado(int nivel) {}

  public int ponerEnEspera() {
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();
    return modo;
  }

  public void ponerModo(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

}

class Pan_NivelesAnyo_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_NivelesAnyo adaptee;
  ActionEvent e;

  Pan_NivelesAnyo_btnAceptar_actionAdapter(Pan_NivelesAnyo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnAceptar_actionPerformed(e);
  }
}

class Pan_NivelesAnyo_btnCancelar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_NivelesAnyo adaptee;
  ActionEvent e;

  Pan_NivelesAnyo_btnCancelar_actionAdapter(Pan_NivelesAnyo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnCancelar_actionPerformed(e);
  }
}

class Pan_NivelesAnyo_txtAnyo_focusAdapter
    extends java.awt.event.FocusAdapter {
  Pan_NivelesAnyo adaptee;
  FocusEvent e;

  Pan_NivelesAnyo_txtAnyo_focusAdapter(Pan_NivelesAnyo adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    adaptee.txtAnyo_focusLost();
  }

  public void focusGained(FocusEvent e) {
    this.e = e;
    adaptee.txtAnyo_focusGained();
  }

}
