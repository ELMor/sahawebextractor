package brotes.cliente.c_fechas;

import java.awt.Color;
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

/* Personalizacion de TextField:
  Entrada:  sOBLIGATORIO  campo obligatorio:  S, N
  Si Obligatorio, color amarillo (pero no dejamos el cursor estatico!)
    No deja escribir mas de dd/mm/aaaa (No deja letras)
    Si erronea, msg y toma el foco.*/

public class CFecha
    extends TextField {

  //datos de entrada
  public String sOblig;

  final int modoOK = 0;
  final int modoFechaKO = 1;

  protected int modo = modoOK;
  protected String sValidaSN = "N";
  protected String sFechaAlmacen = "";

  //constructor
  public CFecha(String sOBLIGATORIO) {
    super();

    //parametros de entrada
    sOblig = sOBLIGATORIO;

    //Color
    if (sOblig.equals("S")) {
      setBackground(new Color(255, 255, 150));

    }
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  } //end Constructor

//jbInit*****************************************
  private void jbInit() throws Exception {

    //ESCRIBIR
    this.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(KeyEvent e) {
        txt_keyReleased(e);
      }
    });

    this.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        txt_keyPressed(e);
      }
    });

    //CHANGE
    this.addTextListener(new java.awt.event.TextListener() {
      public void textValueChanged(TextEvent e) {
        txt_textValueChanged(e);
      }
    });

  } //fin jbnit

//Fecha: lee y escribe '/' si toca
  void AutoEscribeFormatoFecha(KeyEvent e) throws Exception {
    String sFecha = getText();

    if (sFecha.length() == 2 &&
        e.getKeyCode() != java.awt.event.KeyEvent.VK_BACK_SPACE) {
      setText(sFecha.substring(0, 2) + "/");
      setText(getText().substring(0, 3));
    }
    if (sFecha.length() == 5 &&
        e.getKeyCode() != java.awt.event.KeyEvent.VK_BACK_SPACE) {
      setText(sFecha.substring(0, 5) + "/");
      setText(getText().substring(0, 6));
    }
  }

//FECHA: true si existen, false en otro caso
  boolean ExistenDosBarras() {

    boolean b = true;

    if (getText().indexOf("/", 0) == -1) { //No existe ni una barra
      b = false;
    }
    else {
      int x1 = getText().indexOf("/", 0);
      if (getText().indexOf("/", x1 + 1) == -1) { //No existe la segunda
        b = false;
      }
    }
    return (b);
  }

//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString == "") {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString == "") {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     //comprobar longitud
     if (b) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

     return (b);
   } //fin de ChequearEntero

//uOrden = 1: dia/mes/a�o � uOrden = 2: mes/dia/a�o  (01/02/97)(01/02/8)
  boolean ChequearFecha(String sCad, int uOrden) {

    boolean b = true;
    String sCadena = "";
    int i = 0;
    String j1, j2, aa, bb, cc, i1 = "";
    int x1, x2 = 0;
    String dd = "";
    String mm = "";
    String yy = "";

    String ch = "";
    String tt = "";

    int bisiesto = 0;
    int alerta = 0;

    int total = 0;
    int resto4 = 0;
    int resto100 = 0;
    int resto400 = 0;

    sCadena = sCad;

    //sCad = ""; se controla por campo obligatorio
    if (!sCad.equals("")) {
      //SEPARAMOS LOS DATOS
      x1 = sCadena.indexOf("/", 0);
      x2 = sCadena.indexOf("/", x1 + 1);
      aa = sCadena.substring(0, x1);
      bb = sCadena.substring(x1 + 1, x2);
      cc = sCadena.substring(x2 + 1, sCadena.length());

      //deletepot del a�o.
      for (i = 0; i < cc.length(); ++i) {
        if ( (ch = cc.substring(i, i + 1)) != ".") {
          tt = tt + ch;
        }
        if ( (ch = cc.substring(i, i + 1)) == ".") {
          tt = tt;
        }
      }
      //FORMATO dia/mes/a�o � mes/dia/a�o
      if (uOrden == 1) {
        dd = aa;
        mm = bb;
        yy = tt;
      }
      else if (uOrden == 2) {
        dd = bb;
        mm = aa;
        yy = tt;
      }
      else {
        //NO DEBE DARSE UN ORDEN DISTINTO DE 1 o 2
        b = false;
      }

      //COMPROBAR QUE SON NUMEROS y su rango.
      if ( (ChequearEntero(dd, 1, 31, 2) != true) ||
          (ChequearEntero(mm, 1, 12, 2) != true) ||
          (ChequearEntero(tt, 0, 10000, 2) != true)) {
        b = false;
      }

      //RESPECTO A LOS MESES Y DIAS
      //desechamos largos y 0. FORMATEAMOS A DOS CIFRAS, y
      if (mm.length() > 2 || dd.length() > 2
          || dd.equals("00") || mm.equals("00")) {
        b = false;
      }
      if (mm.length() == 1) {
        mm = "0" + mm;
      }
      if (dd.length() == 1) {
        dd = "0" + dd;
      }

      //descomponer los dias
      j1 = dd.substring(0, 1);
      j2 = dd.substring(1, 2);

      //dentro de cada mes
      if (mm.equals("02")) {
        if (j1.equals("3")) {
          b = false;
        }
        else if (j1.equals("2")
                 && j2.equals("9")) {
          alerta = 1;
        }
      }
      else if (mm.equals("04") || mm.equals("06") || mm.equals("09") ||
               mm.equals("11")) {
        if (j1.equals("3") && !j2.equals("0")) {
          b = false;
        }
      }
      //sacar el a�o con 4 cifras=total.
      if (yy.length() > 4 || yy.length() == 3 || yy.length() == 0) {
        b = false;
      }
      else if (yy.length() == 2) {
        //leo el 1� caracter
        i1 = yy.substring(0, 1);

        if (i1.equals("7") || i1.equals("8") || i1.equals("9")) {
          yy = "19" + yy;
        }
        else {
          yy = "20" + yy;
        }
      } //fin elseif

      else if (yy.length() == 1) {
        yy = "200" + yy;
      }
      //a�o

      Integer Itotal = new Integer(yy);
      total = Itotal.intValue();

      resto4 = total % 4;
      resto100 = total % 100;
      resto400 = total % 400;

      //ESTUDIO DE BISIESTO
      //divisible entre 4
      if (resto4 == 0) {
        //no divis. entre 100
        if (resto100 != 0) {
          bisiesto = 1;
        }
        //si div. entre 100
        else {
          //div. entre 400
          if (resto400 == 0) {
            bisiesto = 1;
          }
        }
      } //fin if

      //A�O BISIESTO
      if (alerta == 1 && bisiesto == 0) {
        b = false;
      }

      //FIN DE FUNCION
      if (!b) { //si mal
        modo = modoFechaKO;
      }
      else {
        modo = modoOK;
      }

    } //fin del else

    return (b);
  } //fin funcion

//uOrden = 1: dia/mes/a�o � uOrden = 2: mes/dia/a�o  (01/02/97)(01/02/8)
  String FormatearFecha(String sCad, int uOrden) {

    boolean b = true;
    String sCadena = "";
    int i = 0;
    String j1, j2, aa, bb, cc, i1 = "";
    int x1, x2 = 0;
    String dd = "";
    String mm = "";
    String yy = "";

    String ch = "";
    String tt = "";

    int bisiesto = 0;
    int alerta = 0;

    int total = 0;
    int resto4 = 0;
    int resto100 = 0;
    int resto400 = 0;

    sCadena = sCad;

    //sCad = ""; se controla por campo obligatorio
    if (!sCad.equals("")) {
      //SEPARAMOS LOS DATOS
      x1 = sCadena.indexOf("/", 0);
      x2 = sCadena.indexOf("/", x1 + 1);
      aa = sCadena.substring(0, x1);
      bb = sCadena.substring(x1 + 1, x2);
      cc = sCadena.substring(x2 + 1, sCadena.length());

      //deletepot del a�o.
      for (i = 0; i < cc.length(); ++i) {
        if ( (ch = cc.substring(i, i + 1)) != ".") {
          tt = tt + ch;
        }
        if ( (ch = cc.substring(i, i + 1)) == ".") {
          tt = tt;
        }
      }
      //FORMATO dia/mes/a�o � mes/dia/a�o
      if (uOrden == 1) {
        dd = aa;
        mm = bb;
        yy = tt;
      }
      else if (uOrden == 2) {
        dd = bb;
        mm = aa;
        yy = tt;
      }
      else {
        //NO DEBE DARSE UN ORDEN DISTINTO DE 1 o 2
        b = false;
      }

      //COMPROBAR QUE SON NUMEROS y su rango.
      if ( (ChequearEntero(dd, 1, 31, 2) != true) ||
          (ChequearEntero(mm, 1, 12, 2) != true) ||
          (ChequearEntero(tt, 0, 10000, 2) != true)) {
        b = false;
      }

      //RESPECTO A LOS MESES Y DIAS
      //desechamos largos y 0. FORMATEAMOS A DOS CIFRAS, y
      if (mm.length() > 2 || dd.length() > 2
          || dd.equals("00") || mm.equals("00")) {
        b = false;
      }
      if (mm.length() == 1) {
        mm = "0" + mm;
      }
      if (dd.length() == 1) {
        dd = "0" + dd;
      }

      //descomponer los dias
      j1 = dd.substring(0, 1);
      j2 = dd.substring(1, 2);

      //dentro de cada mes
      if (mm.equals("02")) {
        if (j1.equals("3")) {
          b = false;
        }
        else if (j1.equals("2")
                 && j2.equals("9")) {
          alerta = 1;
        }
      }
      else if (mm.equals("04") || mm.equals("06") || mm.equals("09") ||
               mm.equals("11")) {
        if (j1.equals("3") && !j2.equals("0")) {
          b = false;
        }
      }
      //sacar el a�o con 4 cifras=total.
      if (yy.length() > 4 || yy.length() == 3 || yy.length() == 0) {
        b = false;
      }
      else if (yy.length() == 2) {
        //leo el 1� caracter
        i1 = yy.substring(0, 1);

        if (i1.equals("7") || i1.equals("8") || i1.equals("9")) {
          yy = "19" + yy;
        }
        else {
          yy = "20" + yy;
        }
      } //fin elseif

      else if (yy.length() == 1) {
        yy = "200" + yy;
      }
      //a�o

      Integer Itotal = new Integer(yy);
      total = Itotal.intValue();

      resto4 = total % 4;
      resto100 = total % 100;
      resto400 = total % 400;

      //ESTUDIO DE BISIESTO
      //divisible entre 4
      if (resto4 == 0) {
        //no divis. entre 100
        if (resto100 != 0) {
          bisiesto = 1;
        }
        //si div. entre 100
        else {
          //div. entre 400
          if (resto400 == 0) {
            bisiesto = 1;
          }
        }
      } //fin if

      //A�O BISIESTO
      if (alerta == 1 && bisiesto == 0) {
        b = false;
      }

      //FIN DE FUNCION
      if (!b) { //si mal
        modo = modoFechaKO;
      }
      else {
        modo = modoOK;
        sCadena = dd + "/" + mm + "/" + yy;
      }

    } //fin del else

    return (sCadena);
  } //fin funcion

//Fecha:    No deja escribir mas de lo permitido.  ( release)
  public void txt_keyReleased(KeyEvent e) {

    if (getText().length() > 10) {
      setText(getText().substring(0, 10));
      select(getText().length() + 1, getText().length() + 1);
    }

    if (getText().length() < 7 || !ExistenDosBarras()) {
      modo = modoFechaKO;
      sFechaAlmacen = "";

    }
    if (getText().length() >= 7 && ExistenDosBarras()) {
      if (ChequearFecha(getText(), 1)) { //true
        sFechaAlmacen = FormatearFecha(getText(), 1);
      }
      if (!ChequearFecha(getText(), 1)) {
        sFechaAlmacen = "";
      }
    }

    if (modo == modoOK) {
      sValidaSN = "S";
      sFechaAlmacen = FormatearFecha(getText(), 1);
    }
    if (modo == modoFechaKO) {
      sValidaSN = "N";
      sFechaAlmacen = "";
    }

  } //fin Release

//Fecha:    AutoFormatea
  public void txt_keyPressed(KeyEvent e) {

    try {
      //fecha

      if (getText().length() < 10) {
        AutoEscribeFormatoFecha(e);
        select(getText().length() + 1, getText().length() + 1);
      }

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //fin Pressed

  public void txt_textValueChanged(TextEvent e) {

    boolean bBorrarUltimo = false;

    if (!getText().equals("")) {
      String s = getText().substring(getText().length() - 1, getText().length());

      //Fecha: solo numeros excepto en pos: 3 y 6----
      if (getText().length() != 3 && getText().length() != 6) {
        if (!ChequearEntero(s, 0, 9, 1)) { //No N
          bBorrarUltimo = true;
        }
      }

      if (bBorrarUltimo) {
        setText(getText().substring(0, getText().length() - 1));
        select(getText().length() + 1, getText().length() + 1);
      }

    } // vacio

  } //fin change

  public String getValid() {

    if (getText().equals("") && sOblig.equals("S")) {
      sValidaSN = "N";
    }
    if (getText().equals("") && !sOblig.equals("S")) {
      sValidaSN = "S";

    }
    return (sValidaSN);
  }

  public String getFecha() {
    return (sFechaAlmacen);
  }
} //endclass
