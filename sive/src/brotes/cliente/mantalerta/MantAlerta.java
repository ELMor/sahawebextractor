package brotes.cliente.mantalerta;

import java.util.Calendar;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.ChoiceCDDS;
import brotes.cliente.c_comuncliente.SincrEventos;
//import brotes.servidor.alerta.*;
import brotes.cliente.diaalerta.DiaAlerta;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CEntero;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CListaValores;
import capp2.CPanel;
import capp2.CTexto;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class MantAlerta
    extends CPanel
    implements CInicializar, CFiltro {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 20;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Botones
  MantAlertaActionAdapter actionAdapter = null;

  // P�rdidas de foco
  MantAlertaFocusAdapter focusAdapter = null;

  //Cambio en los campos de texto
  MantAlertaTextAdapter textAdapter = null;

  //Cambio en los campos de opci�n (choice)
  MantAlertaItemListener itemListener = null;

  //listas que se pasan a las siguientes pantallas
  protected Lista listaGrupos = new Lista();
  protected Lista listaSituacion = new Lista();
  protected Lista listaTNotificador = new Lista();
  protected Lista listaPaises = new Lista();
  protected Lista listaCA = new Lista();
  protected Lista listaCENTRO = new Lista();

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();

  //Botones
  ButtonControl btnBuscar = new ButtonControl();
  ButtonControl btnBuscAlertas = new ButtonControl();

  //Lista de alertas
  CListaMantenimiento clmMantAlerta = null;

  //labels
  Label lblArea = new Label();
  Label lblAno = new Label();
  Label lblCodigo = new Label();
  Label lblDescripcion = new Label();
  Label lblGrupo = new Label();
  Label lblSituacion = new Label();
  Label lblFNotifDesde = new Label();
  Label lblFNotifHasta = new Label();

  //campos
  CTexto txtArea = new CTexto(2);
  CEntero txtAno = new CEntero(4);
  CEntero txtCodigo = new CEntero(7);
  CTexto txtDescripcion = new CTexto(50);
  ChoiceCDDS chGrupo = null;
  ChoiceCDDS chSituacion = null;
  fechas.CFecha txtFNotifDesde = new fechas.CFecha("N");
  fechas.CFecha txtFNotifHasta = new fechas.CFecha("N");

  CApp applet = null;

  //hash q se le pasa a las siguientes pantallas.
  protected Hashtable hs = new Hashtable();

  final String servlet = "servlet/SrvQueryTool";
  final String srvAlert = "servlet/SrvAlerta";
  //constructor
  public MantAlerta(CApp a) {
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    setApp(a);
    applet = a;
    try {
      Lista listaOut = Cargar_Listas();
      montarHash();
      //Configuro los combos de Grupo y Situaci�n.
      chGrupo = new ChoiceCDDS(true, true, true, "CD_GRUPO", "DS_GRUPO",
                               listaGrupos);
      chSituacion = new ChoiceCDDS(true, true, true, "CD_SITALERBRO",
                                   "DS_SITALERBRO", listaSituacion);
      // configura la lista de alertas
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nueva alerta",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar alerta",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "Borrar alerta",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("A�o-C�digo",
                                      "100",
                                      "ANO_CODIGO"));

      vLabels.addElement(new CColumna("Descripci�n",
                                      "175",
                                      "DS_ALERTA"));

      vLabels.addElement(new CColumna("Area",
                                      "70",
                                      "CD_NIVEL_1"));

      vLabels.addElement(new CColumna("F.Notificaci�n",
                                      "125",
                                      "FC_FECHAHORA"));

      vLabels.addElement(new CColumna("Grupo",
                                      "65",
                                      "CD_GRUPO"));

      vLabels.addElement(new CColumna("Sit.",
                                      "65",
                                      "CD_SITALERBRO"));

      clmMantAlerta = new CListaMantenimiento(a,
                                              vLabels,
                                              vBotones,
                                              this,
                                              this,
                                              250,
                                              640);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  public void montarHash() {
    hs.put("GRUPOS", (Lista) listaGrupos);
    hs.put("SITUACIONES", (Lista) listaSituacion);
    hs.put("TNOTIFICADOR", (Lista) listaTNotificador);
    hs.put("PAISES", (Lista) listaPaises);
    hs.put("CA", (Lista) listaCA);
  } //end montarHash

  // sQuery = "SELECT CD_GRUPO, DS_GRUPO, DSL_GRUPO "+
  // " FROM SIVE_GRUPO_BROTE ORDER BY CD_GRUPO";
  QueryTool realizarSelGrupos() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_GRUPO_BROTE");
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("DS_GRUPO", QueryTool.STRING);
    qt.putType("DSL_GRUPO", QueryTool.STRING);
    qt.addOrderField("CD_GRUPO");
    return qt;
  }

// sQuery = "SELECT CD_SITALERBRO, DS_SITALERBRO, DSL_SITALERBRO "+
// " FROM SIVE_T_SIT_ALERTA ORDER BY CD_SITALERBRO";
  QueryTool realizarSelSituacion() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_T_SIT_ALERTA");
    qt.putType("CD_SITALERBRO", QueryTool.STRING);
    qt.putType("DS_SITALERBRO", QueryTool.STRING);
    qt.putType("DSL_SITALERBRO", QueryTool.STRING);
    qt.addOrderField("CD_SITALERBRO");
    return qt;
  }

// sQuery = "SELECT  CD_TNOTIF,  DS_TNOTIF,  DSL_TNOTIF "+
// " FROM SIVE_T_NOTIFICADOR ORDER BY CD_TNOTIF";
  QueryTool realizarSelNotif() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_T_NOTIFICADOR");
    qt.putType("CD_TNOTIF", QueryTool.STRING);
    qt.putType("DS_TNOTIF", QueryTool.STRING);
    qt.putType("DSL_TNOTIF", QueryTool.STRING);
    qt.addOrderField("CD_TNOTIF");
    return qt;
  }

// sQuery = "select CD_PAIS, DS_PAIS from SIVE_PAISES";
  QueryTool realizarSelPais() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PAISES");
    qt.putType("CD_PAIS", QueryTool.STRING);
    qt.putType("DS_PAIS", QueryTool.STRING);
    return qt;
  }

// sQuery = "select CD_CA, DS_CA, DSL_CA from SIVE_COM_AUT";
  QueryTool realizarSelCa() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_COM_AUT");
    qt.putType("CD_CA", QueryTool.STRING);
    qt.putType("DS_CA", QueryTool.STRING);
    qt.putType("DSL_CA", QueryTool.STRING);
    qt.addOrderField("CD_CA");
    return qt;
  }

  protected Lista Cargar_Listas() {
    Lista listaSalida = new Lista();
    Lista lCarga = new Lista();

    //recupera los grupos
    QueryTool qtSel = new QueryTool();
    qtSel = realizarSelGrupos();
    Data dtSelGrup = new Data();
    dtSelGrup.put("1", qtSel);
    lCarga.addElement(dtSelGrup);

    //recupera las situaciones
    QueryTool qtSelSit = new QueryTool();
    qtSelSit = realizarSelSituacion();
    Data dtSelSit = new Data();
    dtSelSit.put("1", qtSelSit);
    lCarga.addElement(dtSelSit);

    //recupera los TNotificador
    QueryTool qtSelNotif = new QueryTool();
    qtSelNotif = realizarSelNotif();
    Data dtSelNotif = new Data();
    dtSelNotif.put("1", qtSelNotif);
    lCarga.addElement(dtSelNotif);

    //recupera los Paises
    QueryTool qtSelPais = new QueryTool();
    qtSelPais = realizarSelPais();
    Data dtSelPais = new Data();
    dtSelPais.put("1", qtSelPais);
    lCarga.addElement(dtSelPais);

    //recupera las CA
    QueryTool qtSelCa = new QueryTool();
    qtSelCa = realizarSelCa();
    Data dtSelCa = new Data();
    dtSelCa.put("1", qtSelCa);
    lCarga.addElement(dtSelCa);

    try {
      app.getStub().setUrl(srvAlert);
//    this.getApp().getStub().setUrl();
      listaSalida = (Lista)this.getApp().getStub().doPost(0, lCarga);

      if (listaSalida != null) {
        if (listaSalida.size() > 0) {
          listaGrupos = (Lista) listaSalida.elementAt(0);
          listaSituacion = (Lista) listaSalida.elementAt(1);
          listaTNotificador = (Lista) listaSalida.elementAt(2);
          listaPaises = (Lista) listaSalida.elementAt(3);
          listaCA = (Lista) listaSalida.elementAt(4);
        }
        else {
          //ShowWarning("No existen datos para rellenar las listas");
        }
      }
      else {
        //ShowWarning("Error al recuperar las listas");
      }

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      //ShowWarning ("Error al recuperar las listas");
    }
    return listaSalida;
  }

  void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";
    final String imgBUSCALERTA = "images/browser.gif";

    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().put(imgBUSCALERTA);
    this.getApp().getLibImagenes().CargaImagenes();

    xyLayout.setHeight(440);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    // Escuchadores: botones y p�rdidas de foco
    actionAdapter = new MantAlertaActionAdapter(this);
    focusAdapter = new MantAlertaFocusAdapter(this);
    textAdapter = new MantAlertaTextAdapter(this);
    itemListener = new MantAlertaItemListener(this);

    //campo obligatorio
    txtAno.setBackground(new Color(255, 255, 150));

    lblArea.setText("�rea:");
    lblAno.setText("A�o:");
    lblCodigo.setText("C�digo:");
    lblDescripcion.setText("Descripci�n:");
    lblGrupo.setText("Grupo:");
    lblSituacion.setText("Situaci�n:");
    lblFNotifDesde.setText("Fecha Notificaci�n desde:");
    lblFNotifHasta.setText("Hasta:");

    int iYearAct = 0;
    Calendar cal = Calendar.getInstance();
    iYearAct = cal.get(cal.YEAR);
    if (! (this.getApp().getParametro("ANYO_DEFECTO").equals(""))) {
      txtAno.setText(this.getApp().getParametro("ANYO_DEFECTO"));
    }
    else {
      txtAno.setText(new Integer(iYearAct).toString());

    }
    txtArea.addFocusListener(focusAdapter);
    txtArea.setText(this.getApp().getParametro("CD_NIVEL1_DEFECTO"));
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(actionAdapter);
    btnBuscAlertas.setImage(this.getApp().getLibImagenes().get(imgBUSCALERTA));
    btnBuscAlertas.addActionListener(actionAdapter);
    txtAno.addTextListener(textAdapter);
    txtArea.addTextListener(textAdapter);
    txtCodigo.addTextListener(textAdapter);
    txtDescripcion.addTextListener(textAdapter);
    chGrupo.addItemListener(itemListener);
    chSituacion.addItemListener(itemListener);
    txtFNotifDesde.addTextListener(textAdapter);
    txtFNotifHasta.addTextListener(textAdapter);

    btnBuscar.setActionCommand("Buscar");
    btnBuscAlertas.setActionCommand("BuscarAlertas");
    txtArea.setName("Area");
    txtAno.setName("Ano");
    txtCodigo.setName("Codigo");
    txtDescripcion.setName("Descripcion");
    chGrupo.setName("Grupo");
    chSituacion.setName("Situacion");
    txtFNotifDesde.setName("FechaNotificacionDesde");
    txtFNotifHasta.setName("FechaNotificacionHasta");

    chGrupo.writeData();
    chSituacion.writeData();

    // a�ade los componentes
    this.add(lblArea, new XYConstraints(MARGENIZQ, MARGENSUP, 40, ALTO));
    this.add(txtArea,
             new XYConstraints(MARGENIZQ + 40 + INTERHOR, MARGENSUP, 50, ALTO));
    this.add(btnBuscAlertas,
             new XYConstraints(MARGENIZQ + 40 + 50 + 2 * INTERHOR, MARGENSUP,
                               25, -1));
    this.add(lblAno,
             new XYConstraints(MARGENIZQ + 40 + 50 + 25 + 3 * INTERHOR, MARGENSUP,
                               40, ALTO));
    this.add(txtAno,
             new XYConstraints(MARGENIZQ + 50 + 2 * 40 + 25 + 4 * INTERHOR,
                               MARGENSUP, 50, ALTO));
    this.add(lblCodigo,
             new XYConstraints(MARGENIZQ + 2 * 50 + 2 * 40 + 25 + 5 * INTERHOR,
                               MARGENSUP, 55, ALTO));
    this.add(txtCodigo,
             new XYConstraints(MARGENIZQ + 2 * 50 + 2 * 40 + 55 + 25 +
                               6 * INTERHOR, MARGENSUP, 60, ALTO));
    this.add(lblDescripcion,
             new XYConstraints(MARGENIZQ + 40 + 3 * 50 + 55 + 60 + 25 +
                               7 * INTERHOR, MARGENSUP, 70, ALTO));
    this.add(txtDescripcion,
             new XYConstraints(MARGENIZQ + 40 + 3 * 50 + 55 + 60 + 25 + 70 +
                               8 * INTERHOR, MARGENSUP, 130, ALTO));

    this.add(lblGrupo,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 40,
                               ALTO));
    this.add(chGrupo,
             new XYConstraints(MARGENIZQ + 40 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 198, ALTO));

    this.add(lblSituacion,
             new XYConstraints(MARGENIZQ + 45 + 198 + 2 * INTERHOR - 8,
                               MARGENSUP + ALTO + INTERVERT, 65, ALTO));
    this.add(chSituacion,
             new XYConstraints(MARGENIZQ + 2 * 50 + 2 * 40 + 55 + 25 +
                               6 * INTERHOR, MARGENSUP + ALTO + INTERVERT, 250,
                               ALTO));

    this.add(lblFNotifDesde,
             new XYConstraints(MARGENIZQ,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT + 5, 145,
                               ALTO));
    this.add(txtFNotifDesde,
             new XYConstraints(MARGENIZQ + 40 + INTERHOR + 198 - 100,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT + 5, 100,
                               ALTO));
    this.add(lblFNotifHasta,
             new XYConstraints(MARGENIZQ + 45 + 198 + 2 * INTERHOR - 8,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT + 5, 35,
                               ALTO));
    this.add(txtFNotifHasta,
             new XYConstraints(MARGENIZQ + 2 * 50 + 2 * 40 + 55 + 25 +
                               6 * INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT + 5, 100,
                               ALTO));

    this.add(btnBuscar,
             new XYConstraints(535, MARGENSUP + 3 * ALTO + 3 * INTERVERT, 80,
                               25));
    this.add(clmMantAlerta,
             new XYConstraints(MARGENIZQ - 10,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT, 650, 230));

  } //end jbInit

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        clmMantAlerta.setEnabled(true);
    }
  } //end Inicializar

  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
//    Inicializar(CInicializar.ESPERA);
    if (isDataValid()) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_ALERTA_BROTES");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_ANO", QueryTool2.STRING);
        qt.putType("NM_ALERBRO", QueryTool2.INTEGER);
        qt.putType("DS_ALERTA", QueryTool2.STRING);
        qt.putType("IT_VALIDADA", QueryTool2.STRING);
        qt.putType("CD_GRUPO", QueryTool2.STRING);
        qt.putType("CD_SITALERBRO", QueryTool2.STRING);
        qt.putType("CD_NIVEL_1", QueryTool2.STRING);
        qt.putType("CD_NIVEL_2", QueryTool2.STRING);
        qt.putType("CD_E_NOTIF", QueryTool2.STRING);
        qt.putType("CD_NIVEL_1_EX", QueryTool2.STRING);
        qt.putType("CD_NIVEL_2_EX", QueryTool2.STRING);
        qt.putType("FC_FECHAHORA", QueryTool2.TIMESTAMP);
        qt.putType("FC_ALERBRO", QueryTool2.DATE);

        if (! (txtAno.getText().equals(""))) {
          qt = obtenerAno(qt);
        }

        if (! (txtArea.getText().equals(""))) {
          qt = obtenerArea(qt);
        }

        if (! (txtCodigo.getText().equals(""))) {
          qt = obtenerCodigo(qt);
        }

        if (! (txtDescripcion.getText().equals(""))) {
          qt = obtenerDescripcion(qt);
        }

        if (! (txtFNotifDesde.getText().equals(""))) {
          qt = obtenerFNotifDesde(qt);
        }

        if (! (txtFNotifHasta.getText().equals(""))) {
          qt = obtenerFNotifHasta(qt);
        }

        if (! (chGrupo.getChoiceCD().equals(""))) {
          qt = obtenerGrupo(qt);
        }

        if (! (chSituacion.getChoiceCD().equals(""))) {
          qt = obtenerSituacion(qt);
        }

        //si usuario es de Area-> limitado a sus �reas.
        if (this.getApp().getParametro("PERFIL").equals("3")) {
          qt = anadirSubquery(qt);
        }
        //"ORDER BY CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC";
        qt.addOrderField("CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC");

        p.addElement(qt);
        app.getStub().setUrl(servlet);
        p1 = (Lista) app.getStub().doPost(1, p);
        if (p1.size() == 0) {
          this.getApp().showAdvise(
              "No existen elementos con los criterios especificados");
        }
        else {
          p1 = anadirCampo(p1);
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //end isDataValid
    return p1;
  } //end primera pagina

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
//    Inicializar(CInicializar.ESPERA);
    if (isDataValid()) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_ALERTA_BROTES");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_ANO", QueryTool2.STRING);
        qt.putType("NM_ALERBRO", QueryTool2.INTEGER);
        qt.putType("DS_ALERTA", QueryTool2.STRING);
        qt.putType("IT_VALIDADA", QueryTool2.STRING);
        qt.putType("CD_GRUPO", QueryTool2.STRING);
        qt.putType("CD_SITALERBRO", QueryTool2.STRING);
        qt.putType("CD_NIVEL_1", QueryTool2.STRING);
        qt.putType("CD_NIVEL_2", QueryTool2.STRING);
        qt.putType("CD_E_NOTIF", QueryTool2.STRING);
        qt.putType("CD_NIVEL_1_EX", QueryTool2.STRING);
        qt.putType("CD_NIVEL_2_EX", QueryTool2.STRING);
        qt.putType("FC_FECHAHORA", QueryTool2.TIMESTAMP);
        qt.putType("FC_ALERBRO", QueryTool2.DATE);

        if (txtAno.getText().equals("")) {
          qt = obtenerAno(qt);
        }

        if (txtArea.getText().equals("")) {
          qt = obtenerArea(qt);
        }

        if (txtCodigo.getText().equals("")) {
          qt = obtenerCodigo(qt);
        }

        if (txtDescripcion.getText().equals("")) {
          qt = obtenerDescripcion(qt);
        }

        if (txtFNotifDesde.getText().equals("")) {
          qt = obtenerFNotifDesde(qt);
        }

        if (txtFNotifHasta.getText().equals("")) {
          qt = obtenerFNotifHasta(qt);
        }

        if (chGrupo.getChoiceCD().equals("")) {
          qt = obtenerGrupo(qt);
        }

        if (chSituacion.getChoiceCD().equals("")) {
          qt = obtenerSituacion(qt);
        }

        //si usuario es de Area-> limitado a sus �reas.
        if (this.getApp().getParametro("PERFIL").equals("3")) {
          qt = anadirSubquery(qt);
        }
        //"ORDER BY CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC";
        qt.addOrderField("CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC");

        p.addElement(qt);
        app.getStub().setUrl(servlet);
        p1 = (Lista) app.getStub().doPost(1, p);
        if (p1.size() == 0) {
          this.getApp().showAdvise(
              "No existen elementos con los criterios especificados");
        }
        else {
          p1 = anadirCampo(p1);
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //end isdatavalid
    return p1;
  } //end siguiente pagina

  public Data busquedaDatos(Data dt) {
    Data envDatos = null;
    Lista p = new Lista();
    Lista p1 = new Lista();
    Lista e = new Lista();
    Lista s = new Lista();
    try {
      QueryTool2 qt = new QueryTool2();

      // Nombre de la tabla (FROM)
      qt.putName("SIVE_ALERTA_BROTES");

      // Campos que se quieren leer (SELECT)
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("CD_LEXPROV", QueryTool.STRING);
      qt.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
      qt.putType("DS_ALERTA", QueryTool.STRING);
      qt.putType("CD_GRUPO", QueryTool.STRING);
      qt.putType("CD_E_NOTIF", QueryTool.STRING);
      qt.putType("CD_NIVEL_1_EX", QueryTool.STRING);
      qt.putType("CD_NIVEL_2_EX", QueryTool.STRING);
      qt.putType("CD_NIVEL_1", QueryTool.STRING);
      qt.putType("CD_ZBS_EX", QueryTool.STRING);
      qt.putType("CD_NIVEL_2", QueryTool.STRING);
      qt.putType("NM_EXPUESTOS", QueryTool.INTEGER);
      qt.putType("NM_ENFERMOS", QueryTool.INTEGER);
      qt.putType("NM_INGHOSP", QueryTool.INTEGER);
      qt.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
      qt.putType("FC_INISINTOMAS", QueryTool.TIMESTAMP);
      qt.putType("DS_LEXP", QueryTool.STRING);
      qt.putType("CD_LEXMUN", QueryTool.STRING);
      qt.putType("DS_LEXPDIREC", QueryTool.STRING);
      qt.putType("CD_LEXPPOST", QueryTool.STRING);
      qt.putType("DS_LNMCALLE", QueryTool.STRING);
      qt.putType("DS_LEXPPISO", QueryTool.STRING);
      qt.putType("DS_LEXPTELEF", QueryTool.STRING);
      qt.putType("CD_SITALERBRO", QueryTool.STRING);
      qt.putType("FC_ALERBRO", QueryTool.DATE);
      qt.putType("IT_VALIDADA", QueryTool.STRING);
      qt.putType("FC_FECVALID", QueryTool.DATE);
      qt.putType("CD_OPE", QueryTool.STRING);
      qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dt.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", dt.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      //Datos adicionales de alerta de la tabla SIVE_ALERTA_ADIC
      QueryTool qtAdic1 = new QueryTool();
      qtAdic1.putName("SIVE_ALERTA_ADIC");

      qtAdic1.putType("CD_MINPROV", QueryTool.STRING);
      qtAdic1.putType("CD_TNOTIF", QueryTool.STRING);
      qtAdic1.putType("DS_NOTIFINST", QueryTool.STRING);
      qtAdic1.putType("DS_NOTIFICADOR", QueryTool.STRING);
      qtAdic1.putType("CD_NOTIFPROV", QueryTool.STRING);
      qtAdic1.putType("CD_NOTIFMUN", QueryTool.STRING);
      qtAdic1.putType("DS_NOTIFDIREC", QueryTool.STRING);
      qtAdic1.putType("DS_NNMCALLE", QueryTool.STRING);
      qtAdic1.putType("DS_NOTIFPISO", QueryTool.STRING);
      qtAdic1.putType("CD_NOTIFPOSTAL", QueryTool.STRING);
      qtAdic1.putType("DS_NOTIFTELEF", QueryTool.STRING);
      qtAdic1.putType("DS_MINFPER", QueryTool.STRING);
      qtAdic1.putType("CD_MINMUN", QueryTool.STRING);
      qtAdic1.putType("DS_MINFDIREC", QueryTool.STRING);
      qtAdic1.putType("DS_MNMCALLE", QueryTool.STRING);
      qtAdic1.putType("DS_MINFPISO", QueryTool.STRING);
      qtAdic1.putType("CD_MINFPOSTAL", QueryTool.STRING);
      qtAdic1.putType("DS_MINFTELEF", QueryTool.STRING);
      qtAdic1.putType("DS_OBSERV", QueryTool.STRING);
      qtAdic1.putType("DS_ALISOSP", QueryTool.STRING);
      qtAdic1.putType("DS_SINTOMAS", QueryTool.STRING);

      Data dtAdic1 = new Data();
      dtAdic1.put("CD_ANO", QueryTool.STRING);
      dtAdic1.put("NM_ALERBRO", QueryTool.INTEGER);
      qt.addQueryTool(qtAdic1);
      qt.addColumnsQueryTool(dtAdic1);

      //Datos adicionales de alerta de la tabla SIVE_ALERTA_COLEC
      QueryTool qtAdic2 = new QueryTool();
      qtAdic2.putName("SIVE_ALERTA_COLEC");

      qtAdic2.putType("CD_PROV", QueryTool.STRING);
      qtAdic2.putType("CD_MUN", QueryTool.STRING);
      qtAdic2.putType("DS_NOMCOL", QueryTool.STRING);
      qtAdic2.putType("DS_DIRCOL", QueryTool.STRING);
      qtAdic2.putType("DS_NMCALLE", QueryTool.STRING);
      qtAdic2.putType("DS_PISOCOL", QueryTool.STRING);
      qtAdic2.putType("CD_POSTALCOL", QueryTool.STRING);
      qtAdic2.putType("DS_TELCOL", QueryTool.STRING);

      Data dtAdic2 = new Data();
      dtAdic2.put("CD_ANO", QueryTool.STRING);
      dtAdic2.put("NM_ALERBRO", QueryTool.INTEGER);
      qt.addQueryTool(qtAdic2);
      qt.addColumnsQueryTool(dtAdic2);

      p.addElement(qt);
      app.getStub().setUrl(servlet);
      p1 = (Lista) app.getStub().doPost(1, p);

      envDatos = (Data) p1.elementAt(0);

      QueryTool otraQt = new QueryTool();

      otraQt.putName("SIVE_ZONA_BASICA");
      otraQt.putType("DS_ZBS", QueryTool.STRING);

      otraQt.putWhereType("CD_NIVEL_1", QueryTool.STRING);
      otraQt.putWhereValue("CD_NIVEL_1", envDatos.getString("CD_NIVEL_1_EX"));
      otraQt.putOperator("CD_NIVEL_1", "=");
      otraQt.putWhereType("CD_NIVEL_2", QueryTool.STRING);
      otraQt.putWhereValue("CD_NIVEL_2", envDatos.getString("CD_NIVEL_2_EX"));
      otraQt.putOperator("CD_NIVEL_2", "=");
      otraQt.putWhereType("CD_ZBS", QueryTool.STRING);
      otraQt.putWhereValue("CD_ZBS", envDatos.getString("CD_ZBS_EX"));
      otraQt.putOperator("CD_ZBS", "=");

      e.addElement(otraQt);
      app.getStub().setUrl(servlet);
      s = (Lista) app.getStub().doPost(1, e);

      // Correcci�n 26-04-01 (ARS)
      Data dtTmp = null;
      if (s.size() > 0) {
        dtTmp = (Data) s.elementAt(0);
      }
      else {
        dtTmp = new Data();

        // Ahora meto el nuevo dato en el Data ese.
      }
      envDatos.put("DS_ZBS_EX", dtTmp.getString("DS_ZBS"));

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }

    return envDatos;
  }

  public void realizaOperacion(int j) {
    DiaAlerta di = null;
    Data envDatos = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        di = new DiaAlerta(this.getApp(), ALTA, envDatos, hs);
        di.show();
        // a�adir el nuevo elem a la lista
//        if (di.bAceptar) {
        clmMantAlerta.setPrimeraPagina(primeraPagina());
//        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        Data dMantAlertaModif = clmMantAlerta.getSelected();
        //si existe alguna fila seleccionada
        if (dMantAlertaModif != null) {
          int ind = clmMantAlerta.getSelectedIndex();
          envDatos = busquedaDatos(dMantAlertaModif);
          envDatos.put("CD_CA", applet.getParametro("CA"));
          di = new DiaAlerta(this.getApp(), MODIFICACION, envDatos, hs);
          di.show();
          //Tratamiento de bAceptar para Salir
//          if (di.bAceptar){
          clmMantAlerta.setPrimeraPagina(primeraPagina());
//          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        Data dMantAlertaBaja = clmMantAlerta.getSelected();
        if (dMantAlertaBaja != null) {
          if (!dMantAlertaBaja.getString("CD_SITALERBRO").equals("0")) {
            this.getApp().showError(
                "Solo se puede dar de baja alertas en estado 0");
          }
          else {
            int ind = clmMantAlerta.getSelectedIndex();
            envDatos = busquedaDatos(dMantAlertaBaja);
            envDatos.put("CD_CA", applet.getParametro("CA"));
            di = new DiaAlerta(this.getApp(), BAJA, envDatos, hs);
            di.show();
//          if (di.bAceptar){
            clmMantAlerta.getLista().removeElementAt(ind);
            clmMantAlerta.setPrimeraPagina(primeraPagina());
//          }
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;
        // bot�n de modificaci�n
      case CONSULTA:
        Data dMantAlertaConsulta = clmMantAlerta.getSelected();

        //si existe alguna fila seleccionada
        if (dMantAlertaConsulta != null) {
          int ind = clmMantAlerta.getSelectedIndex();
          envDatos = busquedaDatos(dMantAlertaConsulta);
          di = new DiaAlerta(this.getApp(), CONSULTA, envDatos, hs);
          di.show();
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

    }
  } //end realizaOperacion

  void btnBuscarActionPerformed() {
    clmMantAlerta.setPrimeraPagina(this.primeraPagina());
  } //end btnBuscar

  void btnBuscAlertasActionPerformed() {
    seleccionarAreas();
  }

  QueryTool2 anadirSubquery(QueryTool2 qt) {
    String subquery = "";
    Vector vSubquery = new Vector();

    String autoriza = this.getApp().getParametro("CD_NIVEL_1_AUTORIZACIONES");
    //si tiene autorizaciones para alg�n �rea.
    if (! (autoriza.equals(""))) {
      Data dtAutoriza = new Data();
      StringTokenizer stAutor = new StringTokenizer(autoriza, ",", false);
      for (; stAutor.hasMoreTokens(); ) {
        String autorizacion = stAutor.nextToken();
        Data dtAutVect = new Data();
        subquery += "?,";
        dtAutVect.put(new Integer(QueryTool2.STRING), autorizacion);
        vSubquery.addElement(dtAutVect);
      } //end for
      subquery = subquery.substring(0, subquery.length() - 1); // Quita la ultima coma
      // Establecemos la subquery y sus (tipos,valores)
      qt.putSubquery("CD_NIVEL_1", subquery);
      qt.putVectorSubquery("CD_NIVEL_1", vSubquery);
    } //end si existen autorizaciones.
    return qt;
  } //end anadirSubquery

  void seleccionarAreas() {
    CListaValores clv = null;
    Vector vCod = null;
    QueryTool2 qt = new QueryTool2(); ;
    // "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 IN (?,...)";

    qt.putName("SIVE_NIVEL1_S");
    qt.putType("CD_NIVEL_1", QueryTool2.STRING);
    qt.putType("DS_NIVEL_1", QueryTool2.STRING);
    if (this.getApp().getParametro("PERFIL").equals("3")) {
      qt = anadirSubquery(qt);
    }

    // campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement("CD_NIVEL_1");
    vCod.addElement("DS_NIVEL_1");

    // lista de valores
    clv = new CListaValores(this.getApp(),
                            "Alertas",
                            qt,
                            vCod);
    clv.show();

    // recupera el centro seleccionado
    if (clv.getSelected() != null) {
      txtArea.setText( ( (String) clv.getSelected().get("CD_NIVEL_1")));
    }

    clv = null;
  } //end seleccionarAreas

  QueryTool2 obtenerAno(QueryTool2 qt) {
    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");
    return qt;
  }

  QueryTool2 obtenerArea(QueryTool2 qt) {
    qt.putWhereType("CD_NIVEL_1", QueryTool.STRING);
    qt.putWhereValue("CD_NIVEL_1", txtArea.getText().trim());
    qt.putOperator("CD_NIVEL_1", "=");
    return qt;
  }

  QueryTool2 obtenerCodigo(QueryTool2 qt) {
    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCodigo.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");
    return qt;
  }

  QueryTool2 obtenerDescripcion(QueryTool2 qt) {
    qt.putWhereType("DS_ALERTA", QueryTool.STRING);
    qt.putWhereValue("DS_ALERTA", txtDescripcion.getText().trim());
    qt.putOperator("DS_ALERTA", "=");
    return qt;
  }

  QueryTool2 obtenerFNotifDesde(QueryTool2 qt) {
    String fechaDesde = txtFNotifDesde.getText().trim() + " 00:00:00";
    qt.putWhereType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qt.putWhereValue("FC_FECHAHORA", fechaDesde);
    qt.putOperator("FC_FECHAHORA", ">");
    return qt;
  }

  QueryTool2 obtenerFNotifHasta(QueryTool2 qt) {
    String fechaHasta = txtFNotifHasta.getText().trim() + " 00:00:00";
    qt.putWhereType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qt.putWhereValue("FC_FECHAHORA", fechaHasta);
    qt.putOperator("FC_FECHAHORA", "<");
    return qt;
  }

  QueryTool2 obtenerGrupo(QueryTool2 qt) {
    qt.putWhereType("CD_GRUPO", QueryTool.STRING);
    qt.putWhereValue("CD_GRUPO", chGrupo.getChoiceCD());
    qt.putOperator("CD_GRUPO", "=");
    return qt;
  }

  QueryTool2 obtenerSituacion(QueryTool2 qt) {
    qt.putWhereType("CD_SITALERBRO", QueryTool.STRING);
    qt.putWhereValue("CD_SITALERBRO", chSituacion.getChoiceCD());
    qt.putOperator("CD_SITALERBRO", "=");
    return qt;
  }

  boolean isDataValid() {
    boolean b = true;
    if (txtFNotifDesde.getText().trim().length() != 0) {
      txtFNotifDesde.ValidarFecha();
      if (txtFNotifDesde.getValid().equals("N")) {
        this.getApp().showError("Fecha Desde incorrecta");
        b = false;
      }
    }
    if (b == true) {
      if (txtFNotifHasta.getText().trim().length() != 0) {
        txtFNotifHasta.ValidarFecha();
        if (txtFNotifHasta.getValid().equals("N")) {
          this.getApp().showError("Fecha Hasta incorrecta");
          b = false;
        }
      }
    }

    if (b == true) {
      if (txtAno.getText().length() == 0) {
        this.getApp().showError("Debe introducir un a�o");
        b = false;
      }
      else {
        b = true;
      }
    }
    return b;
  }

  //concatena a�o y c�digo de brote
  Lista anadirCampo(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = new Data();
      dCampo = (Data) lCampo.elementAt(i);
      String s = new String();
      s = dCampo.getString("CD_ANO") + "-" + dCampo.getString("NM_ALERBRO");
      ( (Data) lCampo.elementAt(i)).put("ANO_CODIGO", s);
    }
    return lCampo;
  }

  boolean buscarAreaGlobal(String cdArea) {
    boolean resul = false;
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool2 qt = new QueryTool2(); ;
    try {
      qt.putName("SIVE_NIVEL1_S");
      qt.putType("CD_NIVEL_1", QueryTool2.STRING);
      qt.putWhereType("CD_NIVEL_1", QueryTool.STRING);
      qt.putWhereValue("CD_NIVEL_1", cdArea);
      qt.putOperator("CD_NIVEL_1", "=");
      p.addElement(qt);
      app.getStub().setUrl(servlet);
      p1 = (Lista) app.getStub().doPost(1, p);
      if (p1.size() == 0) {
        resul = false;
      }
      else {
        resul = true;
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return resul;
  } //end buscarAreaGlobal

  boolean buscarAreaUsuario(String cdArea) {
    boolean resul = false;
    boolean salir = false;
    String autoriza = this.getApp().getParametro("CD_NIVEL_1_AUTORIZACIONES");
    //si tiene autorizaciones para alg�n �rea.
    if (! (autoriza.equals(""))) {
      Data dtAutoriza = new Data();
      StringTokenizer stAutor = new StringTokenizer(autoriza, ",", false);
      for (; stAutor.hasMoreTokens() && salir == false; ) {
        String autorizacion = stAutor.nextToken();
        //si el �rea introducida es una de las autorizadas
        if (cdArea.equals(autorizacion)) {
          salir = true;
          resul = true;
        }
      } //end for
    } //end if tiene alguna autorizaci�n
    return resul;
  } //end buscarAreaUsuario

  //al perder el foco se valida q si se ha introducido 1 �rea y
  //usuario=p3, tiene autorizaci�n para la misma.
  void txtAreaFocusLost() {
    String area = txtArea.getText().trim();
    if (area.length() != 0) {
      if ( (this.getApp().getParametro("PERFIL")).equals("3")) {
        boolean b = buscarAreaUsuario(area);
        if (b == false) {
          this.getApp().showAdvise("�rea no existe o usuario no autorizado");
          txtArea.setText("");
        } //end b==false
      }
      else { //si es otro usuario->comprobar con �reas de la bd
        boolean b = buscarAreaGlobal(area);
        if (b == false) {
          this.getApp().showAdvise("�rea no existe");
          txtArea.setText("");
        } //end b==false
      } //end perfil=3
    } //end if area.length
  } //end txtArea_focuslost

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  void txtAnoTextValueChanged() {
    clmMantAlerta.vaciarPantalla();
  }

  void txtAreaTextValueChanged() {
    clmMantAlerta.vaciarPantalla();
  }

  void txtCodigoTextValueChanged() {
    clmMantAlerta.vaciarPantalla();
  }

  void txtDescripcionTextValueChanged() {
    clmMantAlerta.vaciarPantalla();
  }

  void txtFNotifDesdeTextValueChanged() {
    clmMantAlerta.vaciarPantalla();
  }

  void txtFNotifHastaTextValueChanged() {
    clmMantAlerta.vaciarPantalla();
  }

  void chGrupoItemStateChanged() {
    clmMantAlerta.vaciarPantalla();
  }

  void chSituacionItemStateChanged() {
    clmMantAlerta.vaciarPantalla();
  }

} //end class

// Botones
class MantAlertaActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  MantAlerta adaptee;
  ActionEvent evt;

  MantAlertaActionAdapter(MantAlerta adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("BuscarAlertas")) {
        adaptee.btnBuscAlertasActionPerformed();
        //adaptee.btnAreaActionPerformed();
      }
      if (evt.getActionCommand().equals("Buscar")) {
        adaptee.btnBuscarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  MantAlertaActionAdapter

// P�rdidas de foco
class MantAlertaFocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  MantAlerta adaptee;
  FocusEvent evt;

  MantAlertaFocusAdapter(MantAlerta adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("Area")) {
        adaptee.txtAreaFocusLost();
        //adaptee.txtAreaFocusLost();
      }
      s.desbloquea(adaptee);
    }
  }
} // Fin clase MantAlertaFocusAdapter

class MantAlertaTextAdapter
    implements java.awt.event.TextListener, Runnable {
  MantAlerta adaptee;
  TextEvent evt;

  MantAlertaTextAdapter(MantAlerta adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("Ano")) {
        adaptee.txtAnoTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals("Area")) {
        adaptee.txtAreaTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals("Descripcion")) {
        adaptee.txtDescripcionTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals("Codigo")) {
        adaptee.txtCodigoTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals(
          "FechaNotificacionDesde")) {
        adaptee.txtFNotifDesdeTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals(
          "FechaNotificacionHasta")) {
        adaptee.txtFNotifHastaTextValueChanged();
      }
      s.desbloquea(adaptee);
    }
  }

}

class MantAlertaItemListener
    implements java.awt.event.ItemListener, Runnable {
  MantAlerta adaptee;
  ItemEvent evt;

  MantAlertaItemListener(MantAlerta adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (Choice) evt.getSource()).getName().equals("Grupo")) {
        adaptee.chGrupoItemStateChanged();
      }
      if ( ( (Choice) evt.getSource()).getName().equals("Situacion")) {
        adaptee.chSituacionItemStateChanged();
      }
      s.desbloquea(adaptee);
    }
  }
}
