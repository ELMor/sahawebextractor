package brotes.cliente.agcausales;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CPnlCodigo;
import sapp2.Data;
import sapp2.QueryTool;

public class DiaAgCausal
    extends CDialog
    implements CInicializar {

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // Botones de aceptar y cancelar

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  private int modoOperacion = 0;
  private boolean bAceptar = false;

  DiaAgCausalActionAdapter actionAdapter = null;
  Label lblInfAdic = new Label();
  Label lblObserv = new Label();

  // Parte encargada del CPanel C�digo
  CPnlCodigo pnlAgente = null;
  QueryTool qtTemp = new QueryTool();
  // Final del CPanel C�digo...

  Data dtEnt = new Data();
  Data dtSal = new Data();

  // String de cach�.
  String txtAlimCache = "";
  TextArea txtObserv = new TextArea();
  TextArea txtInfAdic = new TextArea();
  Checkbox chkInfAdic = new Checkbox();
  Checkbox chkConfirmado = new Checkbox();
  Checkbox chkSospechoso = new Checkbox();
  CheckboxGroup chkGrupoConfirma = new CheckboxGroup();
  Label lblAgente = new Label();

  // Constructor
  // CApp sera luego el panel del que procede.

  public DiaAgCausal(CApp a, int modo, Data entrada) {

    super(a);
    try {

      // Inicializacion
      modoOperacion = modo;
      dtEnt = entrada;
      Vector vDatSubQuery = new Vector();
      Data dtSubQuery = new Data();

      qtTemp.putName("SIVE_AG_CAUSALES");
      qtTemp.putType("CD_AGENTC", QueryTool.STRING);
      qtTemp.putType("DS_AGENTC", QueryTool.STRING);
      qtTemp.putType("IT_INFADIC", QueryTool.STRING);
      qtTemp.putType("IT_REPE", QueryTool.STRING);
      qtTemp.putType("CD_GRUPO", QueryTool.STRING);

      qtTemp.putWhereType("CD_GRUPO", QueryTool.STRING);
      qtTemp.putWhereValue("CD_GRUPO", dtEnt.getString("CD_GRUPO"));
      qtTemp.putOperator("CD_GRUPO", "=");

      String subConsulta =
          "select CD_AGENTC from SIVE_AG_CAUSALES where IT_REPE='N' " +
          "and CD_GRUPO=? and CD_AGENTC in (select CD_AGENTC from SIVE_AGCAUSAL_BROTE " +
          "where CD_GRUPO=? and NM_ALERBRO = ?)";
      // realiza la subconsulta
      qtTemp.putSubquery("CD_AGENTC not", subConsulta);
      dtSubQuery = new Data();
      dtSubQuery.put(new Integer(QueryTool.STRING), dtEnt.getString("CD_GRUPO"));
      vDatSubQuery.addElement(dtSubQuery);
      dtSubQuery = new Data();
      dtSubQuery.put(new Integer(QueryTool.STRING), dtEnt.getString("CD_GRUPO"));
      vDatSubQuery.addElement(dtSubQuery);
      dtSubQuery = new Data();
      dtSubQuery.put(new Integer(QueryTool.INTEGER),
                     dtEnt.getString("NM_ALERBRO"));
      vDatSubQuery.addElement(dtSubQuery);

      qtTemp.putVectorSubquery("CD_AGENTC", vDatSubQuery);

      pnlAgente = new CPnlCodigo(a, this, qtTemp, "CD_AGENTC",
                                 "DS_AGENTC", true, "Tipos de alimentos",
                                 "Tipos de alimentos");

      this.setTitle("Brotes: Agentes Causales");

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // Organizacion del panel
    this.setSize(new Dimension(580, 350));
    xYLayout.setHeight(350);
    xYLayout.setWidth(580);
    this.setLayout(xYLayout);

    // Im�genes

    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";
    final String imgLUPA = "images/magnify.gif";
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().put(imgLUPA);
    this.getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaAgCausalActionAdapter(this);

    // Boton de Aceptar
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));

    lblInfAdic.setText("Informacion Adicional: ");
    lblObserv.setText("Observaciones: ");
    chkInfAdic.addItemListener(new DiaAgCausal_chkInfAdic_itemAdapter(this));
    chkConfirmado.setLabel("Confirmado");
    chkSospechoso.setLabel("Sospechoso");
    chkGrupoConfirma.setSelectedCheckbox(chkConfirmado);
    lblAgente.setText("Agente:");
    chkConfirmado.setCheckboxGroup(chkGrupoConfirma);
    chkSospechoso.setCheckboxGroup(chkGrupoConfirma);

    btnCancelar.setLabel("Cancelar");
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    this.add(pnlAgente, new XYConstraints(132, 15, 435, 32));
    this.add(lblInfAdic, new XYConstraints(29, 57, 126, 19));
    this.add(lblObserv, new XYConstraints(29, 158, 126, 19));
    this.add(btnAceptar, new XYConstraints(383, 284, 80, -1));
    this.add(btnCancelar, new XYConstraints(469, 284, 80, -1));
    this.add(txtObserv, new XYConstraints(169, 158, 379, 67));
    this.add(txtInfAdic, new XYConstraints(169, 87, 379, 67));
    this.add(chkInfAdic, new XYConstraints(169, 58, 43, 21));
    this.add(chkSospechoso, new XYConstraints(169, 271, -1, 22));
    this.add(chkConfirmado, new XYConstraints(169, 239, -1, 22));
    this.add(lblAgente, new XYConstraints(29, 22, 74, 24));

    verDatos();
  } // Fin jbInit()

  public void Inicializar(int i) {

    switch (i) {

      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {

          case ALTA:
            this.setEnabled(true);
            chkInfAdic.setEnabled(false);
            chkInfAdic.setState(false);
            txtInfAdic.setEnabled(false);
            txtInfAdic.setVisible(false);
            if (pnlAgente.getDatos() != null) {
              if (pnlAgente.getDatos().getString("IT_INFADIC").equals("S")) {
                chkInfAdic.setEnabled(true);
                chkInfAdic.setState(true);
              }
              else {
                chkInfAdic.setEnabled(false);
                chkInfAdic.setState(false);
              }
            }
            if (pnlAgente.hayCambios()) {
              chkInfAdic.setState(false);
            }
            if (chkInfAdic.getState()) {
              txtInfAdic.setVisible(true);
              txtInfAdic.setEnabled(true);
            }
            else {
              txtInfAdic.setVisible(false);
              txtInfAdic.setEnabled(false);
              txtInfAdic.setText("");
            }
            doLayout();
            break;

          case MODIFICACION:
            this.setEnabled(true);
            pnlAgente.setEnabled(false);
            if (pnlAgente.getDatos().getString("IT_INFADIC").equals("S")) {
              chkInfAdic.setEnabled(true);
              chkInfAdic.setState(true);
            }
            else {
              chkInfAdic.setEnabled(false);
              chkInfAdic.setState(false);
            }
            if (chkInfAdic.getState()) {
              txtInfAdic.setVisible(true);
              txtInfAdic.setEnabled(true);
            }
            else {
              txtInfAdic.setVisible(false);
              txtInfAdic.setEnabled(false);
              txtInfAdic.setText("");
            }
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;

            // s�lo habilitado salir y grabar
          case BAJA:
          case CONSULTA:
            this.setEnabled(false);
            if (chkInfAdic.getState()) {
              txtInfAdic.setVisible(true);
              txtInfAdic.setEnabled(false);
            }
            else {
              txtInfAdic.setVisible(false);
              txtInfAdic.setEnabled(false);
            }
            btnAceptar.setEnabled(true);
            if (modoOperacion == CONSULTA) {
              btnAceptar.setEnabled(false);
            }
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  } // Fin Inicializar()

  public boolean validarDatos() {
    boolean estado = false;
    if (pnlAgente.getDatos() != null) {
      estado = true;
    }
    else {
      this.getApp().showAdvise("Debe completar los campos obligatorios");
    }
    return estado;
  } // Fin validarDatos()

  public boolean bAceptar() {
    return bAceptar;
  }

  public Data dtResultado() {
    return dtSal;
  }

  public void verDatos() {
    switch (modoOperacion) {
      case ALTA:
        this.setEnabled(true);
        chkInfAdic.setEnabled(false);
        chkInfAdic.setState(false);
        txtInfAdic.setEnabled(false);
        txtInfAdic.setVisible(false);
        break;

      case MODIFICACION:
        this.setEnabled(true);
        pnlAgente.setCodigo(dtEnt);
        pnlAgente.setEnabled(false);
        if (pnlAgente.getDatos().getString("IT_INFADIC").equals("S")) {
          if (dtEnt.getString("DS_MASAGCAUSAL").length() > 0) {
            chkInfAdic.setState(true);
            chkInfAdic.setEnabled(true);
            txtInfAdic.setVisible(true);
            txtInfAdic.setEnabled(true);
            txtInfAdic.setText(dtEnt.getString("DS_MASAGCAUSAL"));
          }
          else {
            chkInfAdic.setState(false);
            chkInfAdic.setEnabled(true);
            txtInfAdic.setVisible(false);
            txtInfAdic.setEnabled(false);
          }
        }
        else {
          chkInfAdic.setState(false);
          chkInfAdic.setEnabled(false);
          txtInfAdic.setVisible(false);
          txtInfAdic.setEnabled(false);
        }
        txtObserv.setText(dtEnt.getString("DS_OBSERV"));
        if (dtEnt.getString("IT_CONFIRMADO").equals("S")) {
          chkConfirmado.setState(true);
        }
        else if (dtEnt.getString("IT_CONFIRMADO").equals("N")) {
          chkSospechoso.setState(true);
        }
        break;

      case BAJA:
      case CONSULTA:
        this.setEnabled(false);
        btnAceptar.setEnabled(true);
        if (modoOperacion == CONSULTA) {
          btnAceptar.setEnabled(false);
        }
        btnCancelar.setEnabled(true);
        pnlAgente.setCodigo(dtEnt);
        if (dtEnt.getString("IT_INFADIC").equals("S")) {
          if (dtEnt.getString("DS_MASAGCAUSAL").length() > 0) {
            chkInfAdic.setState(true);
            chkInfAdic.setEnabled(false);
            txtInfAdic.setVisible(true);
            txtInfAdic.setEnabled(false);
            txtInfAdic.setText(dtEnt.getString("DS_MASAGCAUSAL"));
          }
        }
        else {
          chkInfAdic.setState(false);
          txtInfAdic.setVisible(false);
          txtInfAdic.setEnabled(false);
        }
        txtObserv.setText(dtEnt.getString("DS_OBSERV"));
        if (dtEnt.getString("IT_CONFIRMADO").equals("S")) {
          chkConfirmado.setState(true);
        }
        else if (dtEnt.getString("IT_CONFIRMADO").equals("N")) {
          chkSospechoso.setState(true);
        }
        break;
    }

  } // Fin rellenarDatos()

  /******************** Manejadores *************************/

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    dtSal = new Data();

    if (e.getActionCommand().equals("Aceptar")) {

      if (validarDatos()) {

        switch (modoOperacion) {

          // ----- NUEVO -----------

          case 0:
            Inicializar(CInicializar.ESPERA);
            dtSal.put("TIPO_OPERACION", "A");
            dtSal.put("CD_AGENTC", pnlAgente.getDatos().getString("CD_AGENTC"));
            dtSal.put("DS_AGENTC", pnlAgente.getDatos().getString("DS_AGENTC"));
            dtSal.put("IT_INFADIC", pnlAgente.getDatos().getString("IT_INFADIC"));
            dtSal.put("IT_REPE", pnlAgente.getDatos().getString("IT_REPE"));
            if (chkInfAdic.getState()) {
              dtSal.put("DS_MASAGCAUSAL", txtInfAdic.getText().trim());
            }
            else {
              dtSal.put("DS_MASAGCAUSAL", "");
            }
            dtSal.put("CD_GRUPO", pnlAgente.getDatos().getString("CD_GRUPO"));
            if (chkConfirmado.getState()) {
              dtSal.put("IT_CONFIRMADO", "S");
            }
            else if (chkSospechoso.getState()) {
              dtSal.put("IT_CONFIRMADO", "N");
            }
            else {
              dtSal.put("IT_CONFIRMADO", "");
            }
            dtSal.put("DS_OBSERV", txtObserv.getText().trim());
            bAceptar = true;
            dispose();
            Inicializar(CInicializar.NORMAL);
            break;
          case 1:

            // ---- MODIFICAR ----
            Inicializar(CInicializar.ESPERA);
            String OpeAnt = dtEnt.getString("TIPO_OPERACION");
            if (OpeAnt.equals("A")) {
              dtSal.put("TIPO_OPERACION", "A");
            }
            else {
              dtSal.put("TIPO_OPERACION", "M");
            }

//              dtSal.put("TIPO_OPERACION","M");
            dtSal.put("CD_AGENTC", dtEnt.getString("CD_AGENTC"));
            dtSal.put("DS_AGENTC", dtEnt.getString("DS_AGENTC"));
            dtSal.put("IT_INFADIC", pnlAgente.getDatos().getString("IT_INFADIC"));
            if (chkInfAdic.getState()) {
              dtSal.put("DS_MASAGCAUSAL", txtInfAdic.getText().trim());
            }
            else {
              dtSal.put("DS_MASAGCAUSAL", "");
            }
            dtSal.put("CD_GRUPO", dtEnt.getString("CD_GRUPO"));
            if (chkConfirmado.getState()) {
              dtSal.put("IT_CONFIRMADO", "S");
              dtSal.put("DS_MASAGCAUSAL", txtInfAdic.getText().trim());
            }
            else if (chkSospechoso.getState()) {
              dtSal.put("IT_CONFIRMADO", "N");
            }
            else {
              dtSal.put("IT_CONFIRMADO", "");
            }
            dtSal.put("DS_OBSERV", txtObserv.getText().trim());
            dtSal.put("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
            dtSal.put("NM_SECAGB", dtEnt.getString("NM_SECAGB"));
            bAceptar = true;
            dispose();
            Inicializar(CInicializar.NORMAL);
            break;
          case 2:

            // ---- BORRAR -------
            OpeAnt = dtEnt.getString("TIPO_OPERACION");
            Inicializar(CInicializar.ESPERA);
//              dtSal.put("TIPO_OPERACION","B");
            dtSal.put("NM_SECAGB", dtEnt.getString("NM_SECAGB"));
            if (OpeAnt.equals("A")) {
              dtSal = null;
            }
            else {
              dtSal.put("TIPO_OPERACION", "B");
            }
            bAceptar = true;
            dispose();
            Inicializar(CInicializar.NORMAL);
            break;
        }
      }
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      dispose();
    }
  }

  void chkInfAdic_itemStateChanged(ItemEvent e) {

    if (chkInfAdic.getState()) {
      txtInfAdic.setVisible(true);
      txtInfAdic.setEnabled(true);
    }
    else {
      txtInfAdic.setVisible(false);
      txtInfAdic.setEnabled(false);
      txtInfAdic.setText("");
    }
    doLayout();
  }
}

/******************* ESCUCHADORES **********************/

// Botones
class DiaAgCausalActionAdapter
    implements java.awt.event.ActionListener, Runnable {

  DiaAgCausal adaptee;
  ActionEvent evt;

  DiaAgCausalActionAdapter(DiaAgCausal adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btn_actionPerformed(evt);
  }
}

class DiaAgCausal_chkInfAdic_itemAdapter
    implements java.awt.event.ItemListener {
  DiaAgCausal adaptee;

  DiaAgCausal_chkInfAdic_itemAdapter(DiaAgCausal adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkInfAdic_itemStateChanged(e);
  }
}
