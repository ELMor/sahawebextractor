/*
 Clase para representar una columna de una tabla que puede ser de texto o una imagen
 */

package brotes.cliente.agcausales;

import capp2.CColumna;

public class CColumnaImagen
    extends CColumna {

  boolean esImagen; //Dice si esta columna se representa con una imagen o no

  public CColumnaImagen(String label,
                        String size,
                        String field,
                        boolean isImage) {
    super(label, size, field);
    esImagen = isImage;
  }

  public boolean isImage() {
    return esImagen;
  }

} //CLASE