/**
 * Clase: CListaMantenimientoExt
 * Paquete: agcausales
 * Autor: Luis Rivera (LRG)
 * Modificado para agcausales, por ARS. (�ngel Rodr�guez S�nchez)
 * y para permitir im�genes en las columnas.
 * Fecha Inicio: xx/xx/xxxx
 * Descripcion: Permite habilitar o deshabilitar los
 *   UButtonControl seg�n el elemento seleccionado usando
 * m�todo de la interfaz ContCListaMantenimientoExt
 *
 *
 **/

package brotes.cliente.agcausales;

import java.util.Vector;

import java.awt.Image;

import capp2.CApp;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import jclass.util.JCVector;
import sapp2.Data;
import sapp2.Lista;

//import java.awt.Image;
//import java.util.Vector;

public class CListaMantenimientoExt
    extends CListaMantenimiento {

  //Objeto que configura botones.Normalmente contenedor
  protected ContCListaMantenimientoExt configBotones;

  // constructor
  public CListaMantenimientoExt(CApp a,
                                Vector labels,
                                Vector botones,
                                CInicializar ci,
                                CFiltro cf,
                                ContCListaMantenimientoExt cext) {

    super(a, labels, botones, ci, cf);
    //Fija la altura de las filas a un valor que permita albergar la imagen
    tabla.setRowHeight(22);
    configBotones = cext;
    configBotones.cambiarBotones(vBotones);
  }

  // constructor con tama�o indicado
  public CListaMantenimientoExt(CApp a,
                                Vector labels,
                                Vector botones,
                                CInicializar ci,
                                CFiltro cf,
                                ContCListaMantenimientoExt cext,
                                int ancho,
                                int alto) {

    super(a, labels, botones, ci, cf, ancho, alto);
    //Fija la altura de las filas a un valor que permita albergar la imagen
    tabla.setRowHeight(22);
    configBotones = cext;
    configBotones.cambiarBotones(vBotones);
  }

  // establece el �ndice seleccionado
//  public void tabla_itemStateChanged(JCItemEvent evt) {
//    super.tabla_itemStateChanged(evt);
//    configBotones.cambiarBotones(vBotones);
//  }

  // constructor : Mismo que el del padre a�adiendo altura max de imagenes
  public CListaMantenimientoExt(CApp a,
                                Vector labels,
                                Vector botones,
                                CInicializar ci,
                                CFiltro cf,
                                ContCListaMantenimientoExt cext,
                                int altura,
                                int anchura,
                                int alturaMaxImagen) {
    super(a, labels, botones, ci, cf, altura, anchura);
    //Fija la altura de las filas a un valor que permita albergar la imagen
    tabla.setRowHeight(alturaMaxImagen);
    configBotones = cext;
    configBotones.cambiarBotones(vBotones);
  }

  // pinta la primera trama en la tabla
  public void setPrimeraPagina(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumnaImagen lbl = null;
    JCVector jcvTabla = new JCVector();
    vSnapShot = v;

    // escribe las l�neas en la tabla
    tabla.clear();
    if (vSnapShot.size() > 0) {
      for (int j = 0; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);

        JCVector jcvLinea = new JCVector();

        //Busca datos de cada linea en un elemento del vector vSnapShot
        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumnaImagen) vLabels.elementAt(i);

          if (lbl.isImage()) {

            //Si la columna representa una imagen
            //En el campo correspondiente de la Hash debe ir una imagen, que es lo que se recoge
            jcvLinea.addElement( (Image) dt.get(lbl.getField()));
          }
          else {
            jcvLinea.addElement(dt.get(lbl.getField()));
          }
        } //for de cada linea

        jcvTabla.addElement(jcvLinea);

      } //For recorre lista datos
      tabla.setItems(jcvTabla);

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(0);
    } //For recorre lista datos
  }

  // pinta las siguientes tramas
  public void setSiguientePagina(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumnaImagen lbl = null;
    JCVector jcvTabla = new JCVector();

    int iLast = tabla.countItems() - 1;
    vSnapShot.addElements(v);

    // escribe las l�neas en la tabla
    tabla.deleteItem(iLast);
    if (vSnapShot.size() >= iLast) {
      for (int j = iLast; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);

        JCVector jcvLinea = new JCVector();

        //Busca datos de cada linea en un elemento del vector vSnapShot
        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumnaImagen) vLabels.elementAt(i);

          if (lbl.isImage()) {

            //Si la columna representa una imagen
            //En el campo correspondiente de la Hash debe ir una imagen, que es lo que se recoge
            jcvLinea.addElement( (Image) dt.get(lbl.getField()));
          }
          else {
            jcvLinea.addElement(dt.get(lbl.getField()));
          }
        } //for de cada linea

        jcvTabla.addElement(jcvLinea);

      } //for lista recorre datos
      tabla.setItems(jcvTabla);

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(iLast);
    } //if
  }

}
/*Clase que permite mostrar columnas con im�genes en la CListaMantenimiento
 Forma de uso: Como la CListaMantenimiento pero:
  Al a�adir una columna que sea de tipo imagen se pone
     datPregunta.put("CONTROL",this.getApp().getLibImagenes().get(imgAceptar));
 */
