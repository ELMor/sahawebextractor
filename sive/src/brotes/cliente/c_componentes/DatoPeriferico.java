// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   DatoPeriferico.java

package brotes.cliente.c_componentes;

import sapp2.Data;

public interface DatoPeriferico {

  public abstract void rellenarDatos(Data data);

  public abstract void recogerDatos(Data data);

  public abstract boolean validarDatos();
}
