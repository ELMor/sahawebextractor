/**
 * Clase: C
 * Paquete: c
 * Hereda:
 * Autor: LRG
 * Fecha Inicio: 12/01/2000
 * Descripcion: Permite ejecutar m�todo en contenedor de este panel (llmadao desde aqu�) tras el
 * tratamiento de eventos en este panel
 */

package brotes.cliente.c_componentes;

import java.util.Vector;

import brotes.cliente.c_comuncliente.BDatos;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPnlCodigo;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

public class CPnlCodigoExt
    extends CPnlCodigo {

//Contenedor que implementa interfaz
  ContCPnlCodigoExt contenedorExt = null;

  boolean otraDesc = false;

  /**
   * Constructor
   */
  public CPnlCodigoExt(CApp applet,
                       CInicializar contenedor,
                       QueryTool querytool,
                       String codigo,
                       String descripcion,
                       boolean obligatorio,
                       String titulo,
                       String tooltip,
                       ContCPnlCodigoExt contenedorEx) {

    super(applet,
          contenedor,
          querytool,
          codigo,
          descripcion,
          obligatorio,
          titulo,
          tooltip);

    contenedorExt = contenedorEx;

  }

  /**
   * Constructor para poner otra desc en lista valores
   */
  public CPnlCodigoExt(CApp applet,
                       CInicializar contenedor,
                       QueryTool querytool,
                       String codigo,
                       String descripcion,
                       boolean obligatorio,
                       String titulo,
                       String tooltip,
                       ContCPnlCodigoExt contenedorEx,
                       boolean otraDescrip) {

    super(applet,
          contenedor,
          querytool,
          codigo,
          descripcion,
          obligatorio,
          titulo,
          tooltip);

    contenedorExt = contenedorEx;
    otraDesc = otraDescrip;

  }

  /*
    protected void btnLupa_actionPerformed() {
      super.btnLupa_actionPerformed();
      contenedorExt.trasEventoEnCPnlCodigoExt();
    }
    protected void txtCod_focusLost() {
      super.txtCod_focusLost();
      contenedorExt.trasEventoEnCPnlCodigoExt();
    }
   */

  /**
   * Muestra la lista de valores para buscar registros
   */
  protected void btnLupa_actionPerformed() {
    CListaValoresExt clv = null;
    Vector vCod = null;
    QueryTool qt = null;

    this.modoOperacion = CInicializar.ESPERA;
    Inicializar();

    // campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement(sCodigo);
    vCod.addElement(sDescripcion);

    // Para evitar que el filtro de la lupa permanezca
    qt = (QueryTool) queryTool.clone();

    if (otraDesc == false) {
      // lista de valores
      clv = new CListaValoresExt(this.getApp(),
                                 titLisVal,
                                 qt,
                                 vCod);
    }

    else {
      // lista de valores
      clv = new CListaValoresExt(this.getApp(),
                                 titLisVal,
                                 qt,
                                 vCod,
                                 contenedorExt,
                                 true);
    }

    clv.show();

    // recupera el centro seleccionado
    if (clv.getSelected() != null) {
      setCodigo(clv.getSelected());
    }

    clv = null;

    //Llama m�todo del interfaz  ^^^^^^^^^^^^
    contenedorExt.trasEventoEnCPnlCodigoExt();

    this.modoOperacion = CInicializar.NORMAL;
    Inicializar();

  }

  /**
   * Descarga el registro cuyo c�digo sea establecido
   * Sobreescrito para acceso a b.datos con interfaz com�n de centinelas
   */
  protected void txtCod_focusLost() {
    Lista v = null;
    Data dt = null;
    boolean bTraer = true;
    QueryTool qt = null;

    // determina que haya un c�digo v�lido
    if (this.txtCod.getText().length() == 0) {
      this.modoOperacion = CInicializar.ESPERA;
      Inicializar();
      limpiarDatos();
      this.modoOperacion = CInicializar.NORMAL;
      Inicializar();
      bTraer = false;
    }

    // determina que haya cambiado el c�digo
    if (this.dtGeneral != null) {
      if (this.dtGeneral.get(sCodigo) != null) {
        if (txtCod.getText().equals( (String)this.dtGeneral.get(sCodigo))) {
          bTraer = false;
        }
      }
    }

    // solicita el registro
    if (bTraer) {

      this.modoOperacion = CInicializar.ESPERA;
      Inicializar();

      // accede a la base de datos
      try {

        //Para evitar que el filtro de codigo permanezca
        qt = (QueryTool) queryTool.clone();

        // filtro de c�digo
        qt.putWhereType(sCodigo, QueryTool.STRING);
        qt.putWhereValue(sCodigo, txtCod.getText());
        qt.putOperator(sCodigo, "=");

        v = new Lista();
        v.addElement(qt);

        /*
                // consulta el servidor
                this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
                v = (Lista) this.getApp().getStub().doPost(1, v);
         */

        v = (Lista) (BDatos.ejecutaSQL(true, app, StubSrvBD.SRV_QUERY_TOOL, 1,
                                       v));

        // presenta el registro
        if (v.size() > 0) {
          setCodigo( (Data) v.elementAt(0));

          // no hay
        }
        else {
          limpiarDatos();
          this.getApp().showAdvise("El c�digo " + txtCod.getText() +
                                   " no existe.");
        }

      }
      catch (Exception e) {
        this.getApp().trazaLog(e);
        this.getApp().showError(e.getMessage());
      }

      //Llama m�todo del interfaz ^^^^^^
      contenedorExt.trasEventoEnCPnlCodigoExt();

      this.modoOperacion = CInicializar.NORMAL;
      Inicializar();
    }

  }

}