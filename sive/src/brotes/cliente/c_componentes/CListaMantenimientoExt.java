/**
 * Clase: CListaMantenimientoExt
 * Paquete: c_componentes
 * Autor: Luis Rivera (LRG)
 * Fecha Inicio: xx/xx/xxxx
 * Descripcion: Permite habilitar o deshabilitar los
 *   UButtonControl seg�n el elemento seleccionado usando
 * m�todo del interfaz ContCListaMantenimientoExt
 *
 *
 **/

package brotes.cliente.c_componentes;

import java.util.Vector;

import capp2.CApp;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import jclass.bwt.JCItemEvent;

public class CListaMantenimientoExt
    extends CListaMantenimiento {

  //Objeto que configura botones.Normalmente contenedor
  protected ContCListaMantenimientoExt configBotones;

  // constructor
  public CListaMantenimientoExt(CApp a,
                                Vector labels,
                                Vector botones,
                                CInicializar ci,
                                CFiltro cf,
                                ContCListaMantenimientoExt cext) {

    super(a, labels, botones, ci, cf);
    configBotones = cext;

  }

  // establece el �ndice seleccionado
  public void tabla_itemStateChanged(JCItemEvent evt) {
    super.tabla_itemStateChanged(evt);
    configBotones.cambiarBotones(vBotones);
  }

}