package brotes.cliente.c_componentes;

import java.util.Vector;

import java.awt.Color;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CContextHelp;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CPanel;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

public class CPnlCodigoExtAli
    extends CPanel {

  public static final int LONG_COD = 60;
  public static final int LONG_DES = 132;
  public static final int LONG_SEP = 2;
  public static final int TAM_BOT_LUPA = 23;
  public static final int ALT_COD_DES = 21;
  public static final int LONG_ETI = 105;
  public static final int LONG_ETI2 = 75;
  public static final int LONG_TXT2 = 140;
  //para los codigos que son numero y secuenciador
  public static final int LONG_COD_NUM = 65;
  public static final int LONG_COD_ANO = 50;
  //Tama�o del componente
  public int width = LONG_SEP + LONG_COD + LONG_SEP + TAM_BOT_LUPA + LONG_SEP +
      LONG_DES + LONG_SEP;
  public int height = LONG_SEP + TAM_BOT_LUPA + LONG_SEP;

  XYLayout xyLayout = new XYLayout();
  protected CCodigo txtCod = new CCodigo();
  TextField txtDes = new TextField();
  ButtonControl btnLupa = new ButtonControl();

  // contenedor
  protected CInicializar ciContenedor = null;

  // Par�metros para realizar la consulta a la base de datos
  protected QueryTool queryTool = null;
  protected String sCodigo = null;
  protected String sDescripcion = null;

  // registro mostrado
  protected Data dtGeneral = null;

  // modo de operaci�n
  protected int modoOperacion = CInicializar.NORMAL;

  // indicador de si se trata de un dato obligatorio
  protected boolean cdObligatorio = false;

  //titulo de la lista de valores
  protected String titLisVal = null;

  // tool tip del bot�n lupa
  protected String sToolTip = null;

  // backup para detectar cambios
  protected Data dtBackUp = null;

  /**
   * Constructor
   */
  public CPnlCodigoExtAli(CApp applet,
                          CInicializar contenedor,
                          QueryTool querytool,
                          String codigo,
                          String descripcion,
                          boolean obligatorio,
                          String titulo,
                          String tooltip) {

    super(applet);
    try {
      ciContenedor = contenedor;
      queryTool = querytool;
      cdObligatorio = obligatorio;
      titLisVal = titulo;
      sCodigo = codigo;
      sDescripcion = descripcion;
      sToolTip = tooltip;
      jbInit();
      // Cargamos los posibles valores que pudiera haber por par�metro
      cargaCenvac_Alm();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public CPnlCodigoExtAli(CApp applet,
                          CInicializar contenedor,
                          QueryTool querytool,
                          String codigo,
                          String descripcion,
                          boolean obligatorio,
                          String titulo,
                          String tooltip,
                          int anchura,
                          int altura) {

    super(applet);
    try {
      width = anchura;
      height = altura;
      ciContenedor = contenedor;
      queryTool = querytool;
      cdObligatorio = obligatorio;
      titLisVal = titulo;
      sCodigo = codigo;
      sDescripcion = descripcion;
      sToolTip = tooltip;
      jbInit();
      // Cargamos los posibles valores que pudiera haber por par�metro
      cargaCenvac_Alm();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Dibuja el componente
   */
  void jbInit() throws Exception {
    final String imgLUPA = "images/magnify.gif";

    // tama�o del componente
//    width = LONG_SEP+LONG_COD+LONG_SEP+TAM_BOT_LUPA+LONG_SEP+LONG_DES+LONG_SEP;
//    height = LONG_SEP+TAM_BOT_LUPA+LONG_SEP;

    xyLayout.setWidth(width);
    xyLayout.setHeight(height);
    txtCod.setText("");
    this.setLayout(xyLayout);
    this.setSize(width, height);

    // bot�n
    this.getApp().getLibImagenes().put(imgLUPA);
    this.getApp().getLibImagenes().CargaImagenes();
    btnLupa.setImage(this.getApp().getLibImagenes().get(imgLUPA));
    if (sToolTip.length() > 0) {
      new CContextHelp(sToolTip, btnLupa);
    }

    if (cdObligatorio) {
      txtCod.setBackground(new Color(255, 255, 150));

    }
    txtDes.setEnabled(false);
    txtDes.setEditable(false);

    // pinta el componente
    this.add(txtCod,
             new XYConstraints(LONG_SEP, LONG_SEP, LONG_COD, ALT_COD_DES));
    this.add(btnLupa,
             new XYConstraints(LONG_SEP + LONG_COD + LONG_SEP, LONG_SEP,
                               TAM_BOT_LUPA, TAM_BOT_LUPA));
    this.add(txtDes,
             new XYConstraints(LONG_SEP + LONG_COD + LONG_SEP + TAM_BOT_LUPA +
                               LONG_SEP, LONG_SEP, LONG_DES, ALT_COD_DES));

    txtCod.addFocusListener(new CPnlCodigoExtAli_focusAdapter(this));
    btnLupa.addActionListener(new CPnlCodigoExtAli_actionAdapter(this));
  }

  //Inicializar en modo Espera o Normal
  public void Inicializar() {
    if (this.ciContenedor != null) {
      this.ciContenedor.Inicializar(this.modoOperacion);
    }
  }

  /**
   * Descarga el registro cuyo c�digo sea establecido
   */
  protected void txtCod_focusLost() {
    Lista v = null;
    Data dt = null;
    boolean bTraer = true;
    QueryTool qt = null;

    // determina que haya un c�digo v�lido
    if (this.txtCod.getText().length() == 0) {
      this.modoOperacion = CInicializar.ESPERA;
      Inicializar();
      limpiarDatos();
      this.modoOperacion = CInicializar.NORMAL;
      Inicializar();
      bTraer = false;
    }

    // determina que haya cambiado el c�digo
    if (this.dtGeneral != null) {
      if (this.dtGeneral.get(sCodigo) != null) {
        if (txtCod.getText().equals( (String)this.dtGeneral.get(sCodigo))) {
          bTraer = false;
        }
      }
    }

    // solicita el registro
    if (bTraer) {

      this.modoOperacion = CInicializar.ESPERA;
      Inicializar();

      // accede a la base de datos
      try {

        //Para evitar que el filtro de codigo permanezca
        qt = (QueryTool) queryTool.clone();

        // filtro de c�digo
        qt.putWhereType(sCodigo, QueryTool.STRING);
        qt.putWhereValue(sCodigo, txtCod.getText());
        qt.putOperator(sCodigo, "=");

        v = new Lista();
        v.addElement(qt);

        // consulta el servidor
        this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        v = (Lista)this.getApp().getStub().doPost(1, v);

        // presenta el registro
        if (v.size() > 0) {
          setCodigo( (Data) v.elementAt(0));

          // no hay
        }
        else {
          limpiarDatos();
          this.getApp().showAdvise("El c�digo " + txtCod.getText() +
                                   " no existe.");
        }

      }
      catch (Exception e) {
        this.getApp().trazaLog(e);
        this.getApp().showError(e.getMessage());
      }

      this.modoOperacion = CInicializar.NORMAL;
      Inicializar();
    }
  }

  /**
   * Muestra la lista de valores para buscar registros
   */
  protected void btnLupa_actionPerformed() {
    CListaValores clv = null;
    Vector vCod = null;
    QueryTool qt = null;

    this.modoOperacion = CInicializar.ESPERA;
    Inicializar();

    // campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement(sCodigo);
    vCod.addElement(sDescripcion);

    // Para evitar que el filtro de la lupa permanezca
    qt = (QueryTool) queryTool.clone();

    // lista de valores
    clv = new CListaValores(this.getApp(),
                            titLisVal,
                            qt,
                            vCod);
    clv.show();

    // recupera el centro seleccionado
    if (clv.getSelected() != null) {
      setCodigo(clv.getSelected());
    }

    clv = null;

    this.modoOperacion = CInicializar.NORMAL;
    Inicializar();
  }

  /**
   * Graba el registro indicado en el componente
   * @param dtGen registro
   */
  public void setCodigo(Data dtGen) {
    dtGeneral = dtGen;
    txtCod.setText( (String) dtGen.get(sCodigo));
    txtDes.setText( (String) dtGen.get(sDescripcion));
  }

  /**
   * Realiza un reset del componente
   */
  public void limpiarDatos() {
    dtGeneral = null;
    txtCod.setText("");
    txtDes.setText("");
  }

  /**
   * Realiza un backup del componente
   */
  public void backupDatos() {
    dtBackUp = dtGeneral;
  }

  /**
   * Recupera el registro descargado de la base de datos
   * @return Data
   */
  public Data getDatos() {
    return dtGeneral;
  }

  /**
   * Devuelve si hay cambios entre el backup y el registro actual
   * @return boolean
   */
  public boolean hayCambios() {
    boolean bCtrl = true;

    if (dtGeneral != null) {
      if (dtBackUp != null) {
        if (dtBackUp.getString(sCodigo).equals(dtGeneral.getString(sCodigo))) {

          // no hay cambio -> el c�digo es el mismo
          bCtrl = false;
        }
      }
    }
    else if (dtBackUp == null) {
      // no hay cambio -> sigue vacio
      bCtrl = false;
    }

    return bCtrl;
  }

  /**
   * Recupera la QueryTool que se esta utilizando
   * @return queryTool
   */
  public QueryTool getQueryTool() {
    return queryTool;
  }

  /**
   * Graba la QueryTool que se va a utilizar
   * @param qt  QueryTool
   */
  public void setQueryTool(QueryTool qt) {
    queryTool = qt;
  }

  private void cargaCenvac_Alm() {

    String sCodigoInterno = "";

    QueryTool qt = new QueryTool();
    Lista v = null;
    boolean bBuscar = false;

    if (sCodigo.equals("CD_CENTROVAC")) {
      // Inicia con centro de vacunaci�n
      sCodigoInterno = this.getApp().getParameter("CD_CENTROVAC");
      if ( (sCodigoInterno != null) && (sCodigoInterno.length() > 0)) {
        bBuscar = true;

        //CRIS
        //Para centros de vacunaci�n
      }
      if (this.getApp().getParametro("COD_APLICACION").equals("SIPC")) {
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        btnLupa.setEnabled(false);
      }

    }

    if (sCodigo.equals("CD_ALMACEN")) {
      // Inicia con almac�n.
      sCodigoInterno = this.getApp().getParameter("CD_ALMACEN");
      if ( (sCodigoInterno != null) && (sCodigoInterno.length() > 0)) {
        bBuscar = true;
      }
    }

    // Ahora se realiza la consulta a la BBDD.

    if (bBuscar) {

//      this.modoOperacion = CInicializar.ESPERA;
//      Inicializar();

      try {

        //Para evitar que el filtro de codigo permanezca
        if ( (queryTool != null) && (queryTool.getName().length() > 0)) {
          qt = (QueryTool) queryTool.clone();
        }
        else {
          if (sCodigo.equals("CD_CENTROVAC")) {
            qt.putName("SIP_CENTRO_VACUNACION");
            // campos que se leen
            qt.putType("CD_CENTROVAC", QueryTool.STRING);
            qt.putType("DS_CENTROVAC", QueryTool.STRING);
            qt.putType("CDPAIS", QueryTool.STRING);
            qt.putType("CDCOMU", QueryTool.STRING);
            qt.putType("CDPROV", QueryTool.STRING);
            qt.putType("CDMUNI", QueryTool.STRING);
            qt.putType("DS_RESPONSABLE", QueryTool.STRING);
            qt.putType("DS_TELEF", QueryTool.STRING);
            qt.putType("NM_SEMDIST", QueryTool.INTEGER);
            qt.putType("CD_DIADIST", QueryTool.STRING);

            // filtro de �reas
            qt.putWhereType("CDAREA_SALUD", QueryTool.STRING);
            qt.putWhereValue("CDAREA_SALUD",
                             this.getApp().getParametro("CDAREA_SALUD"));
            qt.putOperator("CDAREA_SALUD", "=");
          }

          if (sCodigo.equals("CD_ALMACEN")) {
            qt.putName("SIP_ALMACEN");
            qt.putType("CD_ALMACEN", QueryTool.STRING);
            qt.putType("DS_ALMACEN", QueryTool.STRING);
            qt.putType("IT_INTEXT", QueryTool.STRING);

            // Que est� de alta.
            qt.putWhereType("IT_BAJA", QueryTool.STRING);
            qt.putWhereValue("IT_BAJA", "N");
            qt.putOperator("IT_BAJA", "=");
          }
        } // del else

        // filtro de c�digo
        qt.putWhereType(sCodigo, QueryTool.STRING);
        qt.putWhereValue(sCodigo, sCodigoInterno);
        qt.putOperator(sCodigo, "=");

        v = new Lista();
        v.addElement(qt);

        // consulta el servidor
        this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        v = (Lista)this.getApp().getStub().doPost(1, v);

        // presenta el registro
        if (v.size() > 0) {
          setCodigo( (Data) v.elementAt(0));

          // no hay
        }
        else {
          limpiarDatos();
        }

      }
      catch (Exception e) {
        this.getApp().trazaLog(e);
        this.getApp().showError(e.getMessage());
      }

      qt = null;
//      this.modoOperacion = CInicializar.NORMAL;
//      Inicializar();
    } // del if
  }
}

// p�dida de foco del c�digo de producto
class CPnlCodigoExtAli_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  CPnlCodigoExtAli adaptee;

  CPnlCodigoExtAli_focusAdapter(CPnlCodigoExtAli adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.txtCod_focusLost();
  }
}

// bot�n lupa de productos
class CPnlCodigoExtAli_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  CPnlCodigoExtAli adaptee;

  CPnlCodigoExtAli_actionAdapter(CPnlCodigoExtAli adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btnLupa_actionPerformed();
  }
}
