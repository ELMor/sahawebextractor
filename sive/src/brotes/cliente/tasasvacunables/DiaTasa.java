package brotes.cliente.tasasvacunables;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CPnlCodigo;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * Di�logo a trav�s del cu�l se insertan/modifican/borran datos
 * en tablas de mantenimiento de tasas vacunables.
 * @autor PDP
 * @version 1.0
 */

public class DiaTasa
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  public Data dtDev = null;
  //
  public Data dtDevuelto = new Data();
  //
  public Data dtReg = new Data();
  //
  public Data dtRegSubT = new Data();
  //
  //public Lista lAlt = new Lista(); //<---Alta de forma masiva
  //
  public Lista lTable = new Lista();

  public Data dtBrote = null;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xYLayout1 = new XYLayout();
  Label lblGrupos = new Label();
  CPnlCodigo pnlGrupos = null;
  Label lblRangos = new Label();
  CPnlCodigo pnlRangos = null;
  Label lblVE = new Label();
  CEntero txtVE = new CEntero(4);
  Label lblVNE = new Label();
  CEntero txtVNE = new CEntero(4);
  Label lblNVE = new Label();
  CEntero txtNVE = new CEntero(4);
  Label lblNVNE = new Label();
  CEntero txtNVNE = new CEntero(4);
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...

  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  // constructor del di�logo DiaInvBrotes
  public DiaTasa(CApp a, int modoop, Data dtBr, Data dt, Lista lTab) {

    super(a);
    //QueryTool qt1 = null;
    QueryTool qt = null;

    try {
      modoOperacion = modoop;
      dtBrote = dtBr;
      dtDev = dt;
      lTable = lTab;

      // configura el componente de grupos
      qt = new QueryTool();
      qt.putName("SIVE_GEDAD");
      qt.putType("CD_GEDAD", QueryTool.STRING);
      qt.putType("DS_GEDAD", QueryTool.STRING);
      // panel de tipo de GrupoEdad
      pnlGrupos = new CPnlCodigo(a, //applet
                                 this,
                                 qt,
                                 "CD_GEDAD",
                                 "DS_GEDAD",
                                 true,
                                 "Grupos de edad",
                                 "Grupos de edad"); //,this);

      /*
             //Rango seleccionado
             String sCD_GEDADSel = pnlGrupos.getDatos().getString("CD_GEDAD");
       */

      // configura el componente de rangos
      /*
             qt = new QueryTool();
             qt.putName("SIVE_DISTR_GEDAD");
             qt.putType("CD_GEDAD", QueryTool.STRING);
             qt.putType("DS_DIST_EDAD", QueryTool.STRING);
             qt.putType("DIST_EDAD_I", QueryTool.INTEGER);
             qt.putType("DIST_EDAD_F", QueryTool.INTEGER);
             qt.putType("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F", QueryTool.STRING);
             qt.putWhereType("CD_GEDAD", QueryTool.STRING);
             qt.putWhereValue("CD_GEDAD", dtGedadSel.getString("CD_GEDAD"));
             qt.putOperator("CD_GEDAD","=");
           qt.addOrderField("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
       */

      // panel de tipo de Rangos
      pnlRangos = new CPnlCodigo(a, //applet
                                 this,
                                 null,
          "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
          "DS_DIST_EDAD",
          true,
          "Rangos de edad",
          "Rangos de edad");

      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(483, 240);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    lblGrupos.setText("Grupos de Edad:");
    lblRangos.setText("Rangos de Edad:");
    lblVE.setText("Vacunados Enfermos:");
    lblVNE.setText("Vacunados No Enfermos:");
    lblNVE.setText("No Vacunados Enfermos:");
    lblNVNE.setText("No Vacunados No Enfermos:");

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    txtVE.setBackground(new Color(255, 255, 150));
    txtVNE.setBackground(new Color(255, 255, 150));
    txtNVE.setBackground(new Color(255, 255, 150));
    txtNVNE.setBackground(new Color(255, 255, 150));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xYLayout1.setHeight(240);
    xYLayout1.setWidth(483);

    this.add(lblGrupos, new XYConstraints(21, 25, 97, -1));
    this.add(pnlGrupos, new XYConstraints(128, 21, 320, -1));
    this.add(lblRangos, new XYConstraints(21, 63, 97, -1));
    this.add(pnlRangos, new XYConstraints(128, 59, 320, -1));
    this.add(lblVE, new XYConstraints(21, 100, 125, -1));
    this.add(txtVE, new XYConstraints(170, 100, 56, -1));
    this.add(lblVNE, new XYConstraints(235, 100, 145, -1));
    this.add(txtVNE, new XYConstraints(409, 100, 56, -1));
    this.add(lblNVE, new XYConstraints(21, 135, 144, -1));
    this.add(txtNVE, new XYConstraints(170, 135, 56, -1));
    this.add(lblNVNE, new XYConstraints(236, 135, 166, -1));
    this.add(txtNVNE, new XYConstraints(409, 135, 56, -1));
    this.add(btnAceptar, new XYConstraints(283, 178, 89, -1));
    this.add(btnCancelar, new XYConstraints(377, 178, 89, -1));

    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    // ponemos el t�tulo

    switch (modoOperacion) {
      case modoALTA:
        setTitle("Alta de una nueva tasa de enfermedad vacunable");
        break;
      case modoMODIFICACION:
        this.setTitle("Modificaci�n de tasa de enfermedad vacunable");
        break;
      case modoBAJA:
        this.setTitle("Baja de tasa de enfermedad vacunable");
        break;

    }
  }

  // Pone los campos de texto en el estado que les corresponda
  // segun el modo.

  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        if (ExisteGrupo()) {
          pnlGrupos.setEnabled(false);
          pnlRangos.setEnabled(true);
          Data dtT = (Data) lTable.elementAt(0);
          pnlGrupos.setCodigo(dtT);

          QueryTool qt1 = new QueryTool();
          qt1.putName("SIVE_DISTR_GEDAD");
          qt1.putType("CD_GEDAD", QueryTool.STRING);
          qt1.putType("DS_DIST_EDAD", QueryTool.STRING);
          qt1.putType("DIST_EDAD_I", QueryTool.INTEGER);
          qt1.putType("DIST_EDAD_F", QueryTool.INTEGER);
          qt1.putType("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
                      QueryTool.STRING);
          qt1.putWhereType("CD_GEDAD", QueryTool.STRING);
          qt1.putWhereValue("CD_GEDAD", dtT.getString("CD_GEDAD"));
          qt1.putOperator("CD_GEDAD", "=");
          qt1.addOrderField(
              "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
          pnlRangos.setQueryTool(qt1);

        }
        else {
          pnlGrupos.setEnabled(true);
          pnlRangos.setEnabled(false);
        }
        txtVE.setEnabled(true);
        txtVNE.setEnabled(true);
        txtNVE.setEnabled(true);
        txtNVNE.setEnabled(true);
        break;

      case modoMODIFICACION:
        pnlGrupos.setEnabled(false);
        pnlRangos.setEnabled(false);
        txtVE.setEnabled(true);
        txtVNE.setEnabled(true);
        txtNVE.setEnabled(true);
        txtNVNE.setEnabled(true);

        // datos panel principal
        pnlGrupos.setCodigo(dtDev);
        pnlRangos.setCodigo(dtDev);
        txtVE.setText(dtDev.getString("NM_VACENF"));
        txtVNE.setText(dtDev.getString("NM_VACNOENF"));
        txtNVE.setText(dtDev.getString("NM_NOVACENF"));
        txtNVNE.setText(dtDev.getString("NM_NOVACNOENF"));

        break;

        // s�lo habilitado aceptar y cancelar
      case modoBAJA:
        pnlGrupos.setEnabled(false);
        pnlRangos.setEnabled(false);
        txtVE.setEnabled(false);
        txtVNE.setEnabled(false);
        txtNVE.setEnabled(false);
        txtNVNE.setEnabled(false);

        // datos panel principal
        pnlGrupos.setCodigo(dtDev);
        pnlRangos.setCodigo(dtDev);
        txtVE.setText(dtDev.getString("NM_VACENF"));
        txtVNE.setText(dtDev.getString("NM_VACNOENF"));
        txtNVE.setText(dtDev.getString("NM_NOVACENF"));
        txtNVNE.setText(dtDev.getString("NM_NOVACNOENF"));
        break;
    }
  }

  private boolean ExisteGrupo() {
    boolean bExiste = false;
    for (int j = 0; j < lTable.size() - 1; j++) {
      Data dtTab = (Data) lTable.elementAt(j);
      String sDS_DIST_EDAD = dtTab.getString(
          "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
      if (!sDS_DIST_EDAD.equals("")) {
        bExiste = true;
      }
    }

    if (bExiste) {
      return true;
    }
    else {
      return false;
    }

  }

  // Pone el di�logo en modo de espera

  public void Inicializar(int modo) {
    switch (modo) {

      // componente producto accediendo a base de datos
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            if (!ExisteGrupo()) {
              if (pnlGrupos.getDatos() != null) {
                pnlRangos.setEnabled(true);
                pnlGrupos.setEnabled(false);
                Data dtGedad = new Data();
                dtGedad.put("CD_GEDAD",
                            pnlGrupos.getDatos().getString("CD_GEDAD"));
                QueryTool qt1 = new QueryTool();
                qt1.putName("SIVE_DISTR_GEDAD");
                qt1.putType("CD_GEDAD", QueryTool.STRING);
                qt1.putType("DS_DIST_EDAD", QueryTool.STRING);
                qt1.putType("DIST_EDAD_I", QueryTool.INTEGER);
                qt1.putType("DIST_EDAD_F", QueryTool.INTEGER);
                qt1.putType(
                    "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
                    QueryTool.STRING);
                qt1.putWhereType("CD_GEDAD", QueryTool.STRING);
                qt1.putWhereValue("CD_GEDAD", dtGedad.getString("CD_GEDAD"));
                qt1.putOperator("CD_GEDAD", "=");
                qt1.addOrderField(
                    "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
                pnlRangos.setQueryTool(qt1);
              }

              if (pnlRangos.getDatos() == null && pnlGrupos.getDatos() != null) {
                pnlGrupos.setEnabled(true);
              }

              if (pnlRangos.getDatos() == null && pnlGrupos.getDatos() == null) {
                pnlGrupos.setEnabled(true);
                pnlRangos.setEnabled(false);
              }

            }
            else {
              pnlGrupos.setEnabled(false);
              pnlRangos.setEnabled(true);
            }

            break;

          case modoMODIFICACION:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  private boolean isDataValid() {

    String sMsg = null;
    boolean bCtrl1 = true;
    boolean bCtrl2 = true;
    boolean bCtrl3 = true;
    boolean bCtrl4 = true;
    boolean bCtrl5 = true;
    boolean bCtrl6 = true;

    if (txtVE.getText().trim().length() == 0) {
      bCtrl1 = false;
      sMsg = "Debe cumplimentar el n�mero de vacunados enfermos";
    }

    if (txtVNE.getText().length() == 0) {
      bCtrl2 = false;
      sMsg = "Debe cumplimentar el n�mero de vacunados no enfermos";
    }

    if (txtNVE.getText().length() == 0) {
      bCtrl3 = false;
      sMsg = "Debe cumplimentar el n�mero de no vacunados enfermos";
    }

    if (txtNVNE.getText().length() == 0) {
      bCtrl4 = false;
      sMsg = "Debe cumplimentar el n�mero de no vacunados no enfermos";
    }

    if (pnlGrupos.getDatos() == null) {
      bCtrl5 = false;
      sMsg = "Debe cumplimentar el c�digo de grupos de enfermos";
    }

    if (pnlRangos.getDatos() == null) {
      bCtrl6 = false;
      sMsg = "Debe cumplimentar el rango de edad para grupos de enfermos";
    }

    if ( (bCtrl1 && bCtrl2 && bCtrl3 && bCtrl4 && bCtrl5 && bCtrl6) == false) {
      sMsg = "Debe cumplimentar los datos obligatorios";
    }

    if (sMsg == null) {
      return true;
    }
    else {
      this.getApp().showAdvise(sMsg);
      return false;
    }
  }

  // Comprobaci�n de la existencia de un registro con ya repetido
  private boolean ComprobarRepeticion() {
    boolean bRepe = false;
    for (int u = 0; u < lTable.size() - 1; u++) {
      Data dtTable = (Data) lTable.elementAt(u);
      String sDS_DIST_EDADTab = dtTable.getString(
          "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
      String sDS_DIST_EDADAlt = pnlRangos.getDatos().getString(
          "CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F");
      if (sDS_DIST_EDADTab.equals(sDS_DIST_EDADAlt)) {
        bRepe = true;
      }
    }

    if (bRepe) {
      this.getApp().showAdvise("Ya existe este rango de edad, seleccione otro.");

      //pnlGrupos.limpiarDatos();
      pnlRangos.limpiarDatos();

      return false;
    }
    else {
      return true;
    }

    /*
         Lista p = new Lista();
         Lista p1= new Lista();
      try{
        QueryTool qt = new QueryTool();
        // tabla de familias de productos
        qt.putName("SIVE_TATAQ_ENFVAC");
        // campos que se escriben
        qt.putType("NM_TENFVAC",QueryTool.INTEGER);
        qt.putWhereType("CD_GEDAD", QueryTool.STRING);
         qt.putWhereValue("CD_GEDAD",pnlGrupos.getDatos().getString("CD_GEDAD"));
        qt.putOperator("CD_GEDAD","=");
        qt.putWhereType("CD_ANO", QueryTool.STRING);
        qt.putWhereValue("CD_ANO",dtBrote.getString("CD_ANO"));
        qt.putOperator("CD_ANO","=");
        qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
        qt.putWhereValue("NM_ALERBRO",dtBrote.getString("NM_ALERBRO"));
        qt.putOperator("NM_ALERBRO","=");
        p.addElement(qt);
        this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        p1 = (Lista) this.getApp().getStub().doPost(1, p);
        }catch (Exception ex) {
         ex.printStackTrace();
         this.getApp().trazaLog(ex);
        }
        if (p1.size()==0) {
           return true;
        }else{
         this.getApp().showAdvise("Ya existe este grupo de edad, seleccione otro.");
           return false;
        }
     */
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    Data dtTemp = null;
    Lista lSubTot = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          // ----- NUEVO -----------

          case 0:

            Inicializar(CInicializar.ESPERA);

            if (ComprobarRepeticion()) {

              // Valores de los campos
              dtReg = new Data();
              dtRegSubT = new Data();
              //lAlt = new Lista(); // <----Alta de forma masiva
              dtReg.put("CD_ANO", dtBrote.get("CD_ANO"));
              dtReg.put("NM_ALERBRO", dtBrote.get("NM_ALERBRO"));
              dtReg.put("IT_EDAD", "S");
              dtReg.put("CD_GEDAD", pnlGrupos.getDatos().getString("CD_GEDAD"));
              dtReg.put("DS_GEDAD", pnlGrupos.getDatos().getString("DS_GEDAD"));
              dtReg.put("NM_VACENF", txtVE.getText());
              dtReg.put("NM_VACNOENF", txtVNE.getText());
              dtReg.put("NM_NOVACENF", txtNVE.getText());
              dtReg.put("NM_NOVACNOENF", txtNVNE.getText());
              dtReg.put("TIPO_OPERACION", "A");
              lSubTot.addElement(dtReg);
              dtRegSubT = SubTotales(lSubTot);
              //lAlt = SubTotalesAlta(lSubTot); // <----Alta de forma masiva
              bAceptar = true;
              dispose();
            }

            Inicializar(CInicializar.NORMAL);

            break;
          case 1:

            Inicializar(CInicializar.ESPERA);

            // Valores de los campos
            dtReg = new Data();
            dtRegSubT = new Data();
            dtReg.put("CD_ANO", dtBrote.get("CD_ANO"));
            dtReg.put("NM_ALERBRO", dtBrote.get("NM_ALERBRO"));
            dtReg.put("IT_EDAD", "S");
            dtReg.put("CD_GEDAD", pnlGrupos.getDatos().getString("CD_GEDAD"));
            dtReg.put("DS_GEDAD", pnlGrupos.getDatos().getString("DS_GEDAD"));
            dtReg.put("DIST_EDAD_I",
                      pnlGrupos.getDatos().getString("DIST_EDAD_I"));
            dtReg.put("DIST_EDAD_F",
                      pnlGrupos.getDatos().getString("DIST_EDAD_F"));
            dtReg.put("DS_DIST_EDAD",
                      pnlGrupos.getDatos().getString("DS_DIST_EDAD"));
            dtReg.put("NM_VACENF", txtVE.getText());
            dtReg.put("NM_VACNOENF", txtVNE.getText());
            dtReg.put("NM_NOVACENF", txtNVE.getText());
            dtReg.put("NM_NOVACNOENF", txtNVNE.getText());
            //dtReg.put("TIPO_OPERACION","M");
            lSubTot.addElement(dtReg);
            dtRegSubT = SubTotales(lSubTot);
            bAceptar = true;
            dispose();

            Inicializar(CInicializar.NORMAL);

            break;
          case 2:

            Inicializar(CInicializar.ESPERA);

            // Valores de los campos
            dtReg = new Data();
            dtRegSubT = new Data();
            dtReg.put("CD_ANO", dtBrote.get("CD_ANO"));
            dtReg.put("NM_ALERBRO", dtBrote.get("NM_ALERBRO"));
            dtReg.put("IT_EDAD", "S");
            dtReg.put("CD_GEDAD", pnlGrupos.getDatos().getString("CD_GEDAD"));
            dtReg.put("DS_GEDAD", pnlGrupos.getDatos().getString("DS_GEDAD"));
            dtReg.put("DIST_EDAD_I",
                      pnlGrupos.getDatos().getString("DIST_EDAD_I"));
            dtReg.put("DIST_EDAD_F",
                      pnlGrupos.getDatos().getString("DIST_EDAD_F"));
            dtReg.put("DS_DIST_EDAD",
                      pnlGrupos.getDatos().getString("DS_DIST_EDAD"));
            dtReg.put("NM_VACENF", txtVE.getText());
            dtReg.put("NM_VACNOENF", txtVNE.getText());
            dtReg.put("NM_NOVACENF", txtNVE.getText());
            dtReg.put("NM_NOVACNOENF", txtNVNE.getText());
            //dtReg.put("TIPO_OPERACION","B");
            lSubTot.addElement(dtReg);
            dtRegSubT = SubTotales(lSubTot);
            bAceptar = true;
            dispose();

            Inicializar(CInicializar.NORMAL);

            break;
        }
      }
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  }

  public Data SubTotales(Lista lisSubTot) {
    int vEnf = 0;
    int vnEnf = 0;
    int tvEnf = 0;
    double ttvEnf = 0;
    int ittvEnf = 0;
    int nvEnf = 0;
    int nvnEnf = 0;
    int tnvEnf = 0;
    double ttnvEnf = 0;
    int ittnvEnf = 0;
    double efVac = 0;
    int iefVac = 0;
    String total = "Total";
    String sNM_VACENF = "";
    String sNM_VACNOENF = "";
    String sNM_NOVACENF = "";
    String sNM_NOVACNOENF = "";
    String sDS_DIST_EDAD = "";
    int iTotal = 0;
    Data fila = new Data();

    iTotal = lisSubTot.size();
    fila = (Data) lisSubTot.elementAt(0);
    sDS_DIST_EDAD = (String) fila.getString("DS_DIST_EDAD");

    //N�mero de vacunados enfermos
    sNM_VACENF = (String) fila.getString("NM_VACENF");
    if (sNM_VACENF.equals("")) {
      sNM_VACENF = "";
    }
    else {
      vEnf += Integer.parseInt(fila.getString("NM_VACENF"));
    }

    //N�mero de vacunados no enfermos
    sNM_VACNOENF = (String) fila.getString("NM_VACNOENF");
    if (sNM_VACNOENF.equals("")) {
      sNM_VACNOENF = "";
    }
    else {
      vnEnf += Integer.parseInt(fila.getString("NM_VACNOENF"));
    }

    //N�mero total de vacunados
    tvEnf = vEnf + vnEnf;

    //Tasa de vacunados
    if (tvEnf == 0) {
      tvEnf = 0;
    }
    else {
      ttvEnf = 100 * (new Integer(vEnf)).doubleValue() /
          (new Integer(tvEnf)).doubleValue();
    }

    //N�mero de no vacunados enfermos
    sNM_NOVACENF = (String) fila.getString("NM_NOVACENF");
    if (sNM_NOVACENF.equals("")) {
      sNM_NOVACENF = "";
    }
    else {
      nvEnf += Integer.parseInt(fila.getString("NM_NOVACENF"));
    }

    //N�mero de no vacunados no enfermos
    sNM_NOVACNOENF = (String) fila.getString("NM_NOVACNOENF");
    if (sNM_NOVACNOENF.equals("")) {
      sNM_NOVACNOENF = "";
    }
    else {
      nvnEnf += Integer.parseInt(fila.getString("NM_NOVACNOENF"));
    }

    tnvEnf = nvEnf + nvnEnf;

    //Tasa de no vacunados
    if (tnvEnf == 0) {
      tnvEnf = 0;
    }
    else {
      ttnvEnf = 100 * (new Integer(nvEnf)).doubleValue() /
          (new Integer(tnvEnf)).doubleValue();
    }

    //Efectividad de la vacuna

    if ( (nvEnf == 0) && (tvEnf == 0)) {
      efVac = 0;
    }
    else {
      efVac = (1 - (ttvEnf / ttnvEnf)) * 100;
    }

    fila = new Data();
    fila.put("DS_DIST_EDAD", sDS_DIST_EDAD);
    fila.put("NM_VACENF", (new Integer(vEnf)).toString());
    fila.put("NM_VACNOENF", (new Integer(vnEnf)).toString());
    fila.put("NM_VACENF + NM_VACNOENF", (new Integer(tvEnf)).toString());
    fila.put("(NM_VACENF/(NM_VACENF + NM_VACNOENF))*100",
             cogeDecimal(ttvEnf, 1, '.')); //Tasa
    fila.put("NM_NOVACENF", (new Integer(nvEnf)).toString());
    fila.put("NM_NOVACNOENF", (new Integer(nvnEnf)).toString());
    fila.put("NM_NOVACENF + NM_NOVACNOENF", (new Integer(tnvEnf)).toString());
    fila.put("(NM_NOVACENF/(NM_NOVACENF + NM_NOVACNOENF))*100",
             cogeDecimal(ttnvEnf, 1, '.')); //Tasa
    fila.put("(1-(NM_VACENF*(NM_NOVACENF + NM_NOVACNOENF))/(NM_NOVACENF*(NM_VACENF + NM_VACNOENF)))*100",
             cogeDecimal(efVac, 1, '.'));
    fila.put("IT_EDAD", "S");

    if (modoOperacion == 0) {
      fila.put("TIPO_OPERACION", "A");
    }
    else if (modoOperacion == 1) {
      String OpeAnt = dtDev.getString("TIPO_OPERACION");
      if (OpeAnt.equals("A")) {
        fila.put("TIPO_OPERACION", "A");
      }
      else {
        fila.put("NM_TENFVAC", dtDev.getString("NM_TENFVAC"));
        fila.put("TIPO_OPERACION", "M");
      }

    }
    else if (modoOperacion == 2) {

      fila.put("TIPO_OPERACION", "B");
      fila.put("NM_TENFVAC", dtDev.getString("NM_TENFVAC"));
    }

    fila.put("CD_ANO", dtBrote.get("CD_ANO"));
    fila.put("NM_ALERBRO", dtBrote.get("NM_ALERBRO"));
    fila.put("CD_GEDAD", pnlGrupos.getDatos().getString("CD_GEDAD"));
    fila.put("DS_GEDAD", pnlGrupos.getDatos().getString("DS_GEDAD"));
    fila.put("DIST_EDAD_I", pnlRangos.getDatos().getString("DIST_EDAD_I"));
    fila.put("DIST_EDAD_F", pnlRangos.getDatos().getString("DIST_EDAD_F"));
    fila.put("DS_DIST_EDAD", pnlRangos.getDatos().getString("DS_DIST_EDAD"));
    fila.put("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
             pnlRangos.
             getDatos().getString("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F"));

    return fila;
  }

  /*   //Funci�n utilizada para dar de alta de forma masiva des el di�logo
     public Lista SubTotalesAlta(Lista lisSubTot){
    int vEnf = 0;
    int vnEnf = 0;
    int tvEnf = 0;
    double ttvEnf = 0;
    int ittvEnf = 0;
    int nvEnf = 0;
    int nvnEnf = 0;
    int tnvEnf = 0;
    double ttnvEnf = 0;
    int ittnvEnf = 0;
    double efVac = 0;
    int iefVac = 0;
    String total ="Total";
    String sNM_VACENF="";
    String sNM_VACNOENF="";
    String sNM_NOVACENF="";
    String sNM_NOVACNOENF="";
    String sDS_DIST_EDAD="";
    int iTotal= 0;
    Data fila = new Data();
      iTotal = lisSubTot.size();
      fila = (Data)lisSubTot.elementAt(0);
      sDS_DIST_EDAD = (String) fila.getString("DS_DIST_EDAD");
      //N�mero de vacunados enfermos
      sNM_VACENF = (String) fila.getString("NM_VACENF");
      if (sNM_VACENF.equals("")){
      sNM_VACENF="";
      }else{
      vEnf += Integer.parseInt( fila.getString("NM_VACENF"));
      }
      //N�mero de vacunados no enfermos
      sNM_VACNOENF = (String) fila.getString("NM_VACNOENF");
      if (sNM_VACNOENF.equals("")){
      sNM_VACNOENF="";
      }else{
      vnEnf += Integer.parseInt( fila.getString("NM_VACNOENF"));
      }
      //N�mero total de vacunados
       tvEnf = vEnf + vnEnf;
      //Tasa de vacunados
      if (tvEnf==0){
      tvEnf=0;
      }else{
          ttvEnf = ((double)vEnf/(double)tvEnf)*100;
          ittvEnf = (int) ttvEnf;
      }
      //N�mero de no vacunados enfermos
      sNM_NOVACENF = (String) fila.getString("NM_NOVACENF");
      if (sNM_NOVACENF.equals("")){
      sNM_NOVACENF="";
      }else{
      nvEnf += Integer.parseInt( fila.getString("NM_NOVACENF"));
      }
      //N�mero de no vacunados no enfermos
      sNM_NOVACNOENF = (String) fila.getString("NM_NOVACNOENF");
      if (sNM_NOVACNOENF.equals("")){
      sNM_NOVACNOENF ="";
      }else{
      nvnEnf += Integer.parseInt( fila.getString("NM_NOVACNOENF"));
      }
      tnvEnf = nvEnf + nvnEnf;
      //Tasa de no vacunados
      if (tnvEnf==0){
      tnvEnf=0;
      }else{
          ttnvEnf = ((double)nvEnf/(double)tnvEnf)*100;
          ittnvEnf = (int) ttnvEnf;
      }
      //Efectividad de la vacuna
      if ((nvEnf&tvEnf)==0){
      efVac=0;
      }else{
          efVac = (1-((double)(ttvEnf)/(double)(ttnvEnf)))*100;
          iefVac = (int) efVac;
      }
      Lista lDist = null;
      Lista lfila = null;
      Data dtDist = null;
      lDist = TraerDistribuciones();
      lfila = new Lista();
      for (int i=0; i<lDist.size(); i++){
          dtDist = (Data) lDist.elementAt(i);
          fila = new Data();
          fila.put("DS_DIST_EDAD", sDS_DIST_EDAD);
          fila.put("NM_VACENF", (new Integer(vEnf)).toString());
          fila.put("NM_VACNOENF", (new Integer(vnEnf)).toString());
          fila.put("NM_VACENF + NM_VACNOENF", (new Integer(tvEnf)).toString());
          fila.put("(NM_VACENF/(NM_VACENF + NM_VACNOENF))*100", (new Integer(ittvEnf)).toString()); //Tasa
          fila.put("NM_NOVACENF", (new Integer(nvEnf)).toString());
          fila.put("NM_NOVACNOENF", (new Integer(nvnEnf)).toString());
       fila.put("NM_NOVACENF + NM_NOVACNOENF", (new Integer(tnvEnf)).toString());
          fila.put("(NM_NOVACENF/(NM_NOVACENF + NM_NOVACNOENF))*100", (new Integer(ittnvEnf)).toString()); //Tasa
          fila.put("(1-(NM_VACENF*(NM_NOVACENF + NM_NOVACNOENF))/(NM_NOVACENF*(NM_VACENF + NM_VACNOENF)))*100", (new Integer(iefVac)).toString());
          fila.put("IT_EDAD","S");
          fila.put("TIPO_OPERACION","A");
          fila.put("CD_ANO",dtBrote.get("CD_ANO"));
          fila.put("NM_ALERBRO",dtBrote.get("NM_ALERBRO"));
          fila.put("CD_GEDAD",dtDist.getString("CD_GEDAD"));
          fila.put("DIST_EDAD_I",dtDist.getString("DIST_EDAD_I"));
          fila.put("DIST_EDAD_F",dtDist.getString("DIST_EDAD_F"));
          fila.put("DS_DIST_EDAD", dtDist.getString("DS_DIST_EDAD"));
          fila.put("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",dtDist.getString("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F"));
          lfila.addElement(fila);
          //lfila = new Lista();
      }
       return lfila;
     }*/

  private String cogeDecimal(double dent, int num_dec, char decimal) {
    String salida = "";
    int pos_coma = 0;
    if (!Double.isNaN(dent)) {
      String cadena = String.valueOf(dent);
      for (int a = 0; a < cadena.length(); a++) {
        if (cadena.charAt(a) == decimal) {
          pos_coma = a;
        }
      }
      if ( (num_dec > 0) && (pos_coma + num_dec < cadena.length())) {
        salida = cadena.substring(0, pos_coma + num_dec + 1);
      }
      else if (num_dec == 0) {
        salida = cadena.substring(0, pos_coma);
      }
      else {
        salida = cadena;
      }
    }
    else {
      salida = "";
    }
    return salida;
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // Devuelve los datos seleccionados en el di�logo.
  public Data devuelveData() {
    return dtRegSubT;
  }

  /*
     // Devuelve los datos correspondientes a un alta.
     public Lista devuelveDataAlta() {
    return lAlt;
     }
   */

  /* // <---- Alta de forma masiva
     Lista TraerDistribuciones(){
    Lista p = new Lista();
    Lista p1= new Lista();
    final String servlet = "servlet/SrvQueryTool";
        try{
           QueryTool qt = new QueryTool();
           qt.putName("SIVE_DISTR_GEDAD");
           qt.putType("CD_GEDAD", QueryTool.STRING);
           qt.putType("DIST_EDAD_I", QueryTool.INTEGER);
           qt.putType("DIST_EDAD_F", QueryTool.INTEGER);
           qt.putType("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F", QueryTool.STRING);
           qt.putType("DIST_EDAD_I || '-' || DIST_EDAD_F", QueryTool.STRING);
           qt.putType("DS_DIST_EDAD", QueryTool.STRING);
           qt.putWhereType("CD_GEDAD", QueryTool.STRING);
       qt.putWhereValue("CD_GEDAD",pnlGrupos.getDatos().getString("CD_GEDAD"));
           qt.putOperator("CD_GEDAD","=");
           p.addElement(qt);
          this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
          p1 = (Lista) this.getApp().getStub().doPost(1, p);
          //p1= BDatos.ejecutaSQL(true,this.getApp(),servlet,1,p);
          if (p1.size()==0) {
           this.getApp().showAdvise("No existen datos");
          }
        }catch (Exception ex) {
         ex.printStackTrace();
         this.getApp().trazaLog(ex);
        }
        return p1;
     }*/

// botones de aceptar y cancelar.

  class DialogActionAdapter
      implements java.awt.event.ActionListener, Runnable {
    DiaTasa adaptee;
    ActionEvent e;

    DialogActionAdapter(DiaTasa adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
      this.e = e;
      Thread th = new Thread(this);
      th.start();
    }

    public void run() {
      adaptee.btn_actionPerformed(e);
    }
  }

}
