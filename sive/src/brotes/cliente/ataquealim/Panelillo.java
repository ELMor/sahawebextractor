package brotes.cliente.ataquealim;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CInicializar;
import capp2.CPanel;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * Panelillo para probar el paquete ...
 */
public class Panelillo
    extends CPanel { //CPanel

  ButtonControl btnBuscar = new ButtonControl();

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // constructor del panel PanMant
  public Panelillo(CApp a) {

    try {
      setApp(a);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel PanMant
  public void jbInit() throws Exception {

    // Organizacion del panel
    this.setSize(new Dimension(100, 100));
    xYLayout.setHeight(100);
    xYLayout.setWidth(100);
    this.setLayout(xYLayout);

    // Escuchadores de eventos

    PanMant_actionAdapter actionAdapter = new PanMant_actionAdapter(this);

    btnBuscar.setActionCommand("buscar");
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(actionAdapter);

    final String imgBUSCAR = "images/refrescar.gif";
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));

    // tool tips

    new CContextHelp("Obtener almacenes", btnBuscar);
    this.add(btnBuscar, new XYConstraints(5, 5, -1, -1));
  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  // gesti�n de los botones
  void btn_actionPerformed(ActionEvent e) {

    QueryTool qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", "1997");
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", "2");
    qtBloqueo.putOperator("NM_ALERBRO", "=");
    Lista l = new Lista();
    l.addElement(qtBloqueo);

    Lista vResultado = null;
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      vResultado = (Lista)this.getApp().getStub().doPost(1, l);
    }
    catch (Exception ex) {}
    ;

    String Ope = ( (Data) vResultado.elementAt(0)).getString("CD_OPE");
    String FUlt = ( (Data) vResultado.elementAt(0)).getString("FC_ULTACT");
//  String Ope = "D_1";
//  String FUlt = "03/03/2000 12:33:36";

    Data miData = (Data) vResultado.elementAt(0);
    Data pepito = new Data();
    pepito.put("CD_ANO", "1997");
    pepito.put("NM_ALERBRO", "2");
    pepito.put("DS_BROTE", "Brote de prueba");
    pepito.put("CD_OPE", Ope); //miData.getString("CD_OPE"));
    pepito.put("FC_ULTACT", FUlt); //miData.getString("FC_ULTACT"));

    if (e.getActionCommand().equals("buscar")) {
      DiaAtaqAlim dm = null;
      dm = new DiaAtaqAlim(this.getApp(), 1, pepito);
      dm.show();
    }
  }

}

// botones de centro, almac�n y buscar
class PanMant_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Panelillo adaptee;
  ActionEvent e;

  PanMant_actionAdapter(Panelillo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
