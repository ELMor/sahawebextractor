package brotes.cliente.exportbrotesperiodo;

import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CTabla;
import sapp2.Lista;

/**
 * Di�logo a trav�s del cu�l se muestra una lista de los ficheros creados
 */

public class DiaMsgAviso
    extends CDialog {

  XYLayout xYLayout1 = new XYLayout();

  CTabla tbl = new CTabla();
  ButtonControl btnAceptar = new ButtonControl();

  Lista vError = new Lista();

  // Escuchadores de eventos...

  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  // constructor del di�logo DiaMant
  public DiaMsgAviso(CApp a, int modo, Lista lErrores) {

    super(a);

    try {
      this.setTitle("Aviso");
      vError = lErrores;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {

    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(230, 300);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));

    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        "Ficheros creados:", '\n'));
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String("160"),
        '\n'));
    tbl.setNumColumns(1);

    // a�adimos los errores..
    for (int i = 0; i < vError.size(); i++) {
      tbl.addItem(vError.elementAt(i));
    }

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);

    xYLayout1.setHeight(300);
    xYLayout1.setWidth(230);

    this.add(btnAceptar, new XYConstraints(78, 223, -1, -1));
    this.add(tbl, new XYConstraints(24, 23, 180, 180));
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    if (e.getActionCommand().equals("Aceptar")) {
      this.dispose();
    }
  }
}

// botones de aceptar y cancelar.

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMsgAviso adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMsgAviso adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
