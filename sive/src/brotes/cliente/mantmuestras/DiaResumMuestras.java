package brotes.cliente.mantmuestras;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
//import centinelas.datos.c_mantPC.*;
//import centinelas.datos.centbasicos.*;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
//import CListaMantenimiento.*;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import comun.Common;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

//import brotes.servidor.mantmuestras.*;

public class DiaResumMuestras
    extends CDialog
    implements CInicializar, CFiltro {

  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 20;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  // parametros pasados al di�logo
  public Data dtDev = null;

  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOp = 0;
  public int modoBloqueo = modoNORMAL;

  private Lista lClm = new Lista();
  private Lista lBaja = new Lista();
  private Lista lBd = new Lista();
  private Lista lSec = new Lista();

  int secuenciadorMuestra = 0;
  int secuenciadorMicrotox = 0;

  final String srvTrans = "servlet/SrvTransaccion";

  XYLayout xyLayout1 = new XYLayout();

  CListaMantenimiento clmMuestras = null;

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  Label lblBrote = new Label();
  Label lblDatosBrote = new Label();

  public DiaResumMuestras(CApp a, int modoop, Data dt) {
    super(a);
    modoOp = modoop;
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qt = new QueryTool();

    dtDev = dt;
    // longitud de los campos de texto y �rea...
    try {
      vBotones.addElement(new CBoton("",
                                     "images/alta2.gif",
                                     "Nueva muestra de brote",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion2.gif",
                                     "Modificar muestra de brote",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja2.gif",
                                     "Borrar muestra de brote",
                                     false,
                                     true));

      // etiquetas
      //DS_MUESTRA de sive_origen_muestra
      vLabels.addElement(new CColumna("Origen",
                                      "175",
                                      "ORIGEN"));

      //DS_RMUESTRA de sive_muestras_brote
      vLabels.addElement(new CColumna("Muestra",
                                      "165",
                                      "DS_RMUESTRA"));

      vLabels.addElement(new CColumna("N� Muest.",
                                      "60",
                                      "NM_MUESBROTE"));

      vLabels.addElement(new CColumna("N� Posit.",
                                      "60",
                                      "NM_POSITBROTE"));

      //DS_AGENTET de sive_agente_toxico
      vLabels.addElement(new CColumna("Agentes/M./Toxinas",
                                      "120",
                                      "DS_AGENTET"));

      //DS_RESCUANTI de sive_microtox_muestras
      vLabels.addElement(new CColumna("Rdo Cuantitativo",
                                      "130",
                                      "DS_RESCUANTI"));

      clmMuestras = new CListaMantenimiento(a,
                                            vLabels,
                                            vBotones,
                                            this,
                                            this,
                                            260,
                                            750);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    this.setLayout(xyLayout1);
    this.setSize(775, 410);

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    lblBrote.setText("Brote:");
    lblDatosBrote.setText(dtDev.getString("CD_ANO") + "/" +
                          dtDev.getString("NM_ALERBRO") + " - " +
                          dtDev.getString("DS_BROTE"));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(775);
    xyLayout1.setWidth(410);

    this.add(lblBrote, new XYConstraints(MARGENIZQ, MARGENSUP, 80, ALTO));
    this.add(lblDatosBrote,
             new XYConstraints(MARGENIZQ + 80 + INTERHOR, MARGENSUP, 300, ALTO));
    this.add(clmMuestras,
             new XYConstraints(MARGENIZQ - 10, MARGENSUP + ALTO + 2 * INTERVERT,
                               760, 230));
    this.add(btnAceptar, new XYConstraints(555, 320, 88, 29));
    this.add(btnCancelar, new XYConstraints(655, 320, 88, 29));

    /*    this.add(clmMuestras, new XYConstraints(15, 15, 760, 290));
        this.add(btnAceptar, new XYConstraints(575, 320, 88, 29));
        this.add(btnCancelar, new XYConstraints(675,320, 88, 29));*/

    verDatos();
    // ponemos el t�tulo

    this.setTitle("Brotes: Resumen de Muestras");

  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOp) {
          case ALTA:
          case MODIFICACION:
            this.setEnabled(true);
            break;
          case BAJA:
          case CONSULTA:
            clmMuestras.setEnabled(false);
            btnAceptar.setVisible(false);
            btnCancelar.setLabel("Salir");
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  //Me situa la modificaci�n en la lista
  int situarModifEnLista(Data dtModif) {
    String itBd = dtModif.getString("IT_BD");
    String cdTabMues = new String();
    int cdTablaMues = 0;
    int posicion = 0;

    //No pertenece a la BD, solo se encuentra en lClm
    if (itBd.equals("N")) {
      //se coloca el indicativo de q este registro es para dar de alta
      dtModif.put("IT_OPERACION", "0");

      //se obtiene el origen y primero se coloca por origen
      String cdRecib = dtModif.getString("CD_MUESTRA");
      int cdRecibido = Integer.parseInt(cdRecib);
      boolean salir = false;
      //lo meto en la lista lClm
      for (int i = 0; i < lClm.size() && salir == false; i++) {
        String cdTab = ( (Data) lClm.elementAt(i)).getString("CD_MUESTRA");
        int cdTabla = Integer.parseInt(cdTab);
        if (cdTabla > cdRecibido) {
          lClm.insertElementAt(dtModif, i);
          posicion = i;
          salir = true;
        }
      } //end for lClm

      //si no va antes de los origenes existentes-> se comprueba
      //en que posici�n debe ir y se coloca
      if (salir == false) {
        boolean salir2 = false;

        //lo meto en la lista lClm: recorro la cListaMtmto->
        //por cada elemento saco su origen->cdTabla. Cuando
        //encuentre 1 elemento cuyo cd_muestra sea superior en
        //1 al q se intenta introducir, se introduce delante.
        for (int i = 0; i < lClm.size() && salir2 == false; i++) {
          String cdTab1 = ( (Data) lClm.elementAt(i)).getString("CD_MUESTRA");
          int cdTabla1 = Integer.parseInt(cdTab1);
          if (cdTabla1 > cdRecibido) {
            lClm.insertElementAt(dtModif, i);
            salir2 = true;
          }
        } //end for lClm
        //si no hemos encontrado ->comprobamos si es el final de la lista
        if (salir2 == false) {
          lClm.addElement(dtModif);
          posicion = lClm.size() - 1;
        }

        /*        lClm.addElement(dtModif);
          posicion=lClm.size()-1;*/
      }

    }
    else { //si pertenece a la bd

      //obtengo el origen y codigo de la muestra q se desea colocar
      String cdRecib = dtModif.getString("CD_MUESTRA");
      int cdRecibido = Integer.parseInt(cdRecib);
      String cdRecibMues = dtModif.getString("NM_MUESTRA");
      int cdRecibidoMues = Integer.parseInt(cdRecibMues);

      boolean salir = false;
      String cdTab = new String();
      int cdTabla = 0;

      //recorro lClm y busco el origen correspondiente
      int i = 0;
      for (i = 0; i < lClm.size() && salir == false; i++) {
        cdTab = ( (Data) lClm.elementAt(i)).getString("CD_MUESTRA");
        cdTabla = Integer.parseInt(cdTab);

        //si hemos entrado en el origen q nos pertenece
        if (cdTabla == cdRecibido) {

          //accedo al primer elemento de ese origen y si pertenece
          //a la BD obtengo su nm_muestra y si no le doy un valor
          //m�ximo
          String nombre = ( (Data) lClm.elementAt(i)).getString("DS_RMUESTRA");
          if ( ( (Data) lClm.elementAt(i)).getString("IT_BD").equals("S")) {
            cdTabMues = ( (Data) lClm.elementAt(i)).getString("NM_MUESTRA");
            cdTablaMues = Integer.parseInt(cdTabMues);
          }
          else {
            cdTabMues = "0";
            cdTablaMues = 0;
          }

          boolean fuera = false;

          //mientras estemos en nuestro origen y el c�digo de
          //la muestra q leemos sea menor al nuestro
          //y la lista no se acabe
          for (; cdTabla == cdRecibido && cdTablaMues != cdRecibidoMues
               && i < lClm.size() - 1; ) {
            i++;
            //obtenemos el origen de la muestra
            cdTab = ( (Data) lClm.elementAt(i)).getString("CD_MUESTRA");
            cdTabla = Integer.parseInt(cdTab);
            if ( ( (Data) lClm.elementAt(i)).getString("IT_BD").equals("S")) {
              cdTabMues = ( (Data) lClm.elementAt(i)).getString("NM_MUESTRA");
              cdTablaMues = Integer.parseInt(cdTabMues);
            }
            else {
              cdTabMues = "0";
              cdTablaMues = 0;
            }
          } //end for
          //si se ha llegado al final de la lista
          if (i == lClm.size() - 1) {
            //si el �ltimo elemento es de distinto origen-> se
            //coloca previo a �l
            if (cdTabla != cdRecibido) {
              lClm.insertElementAt(dtModif, i);
              posicion = i;
            }
            else { //si no se coloca en el �ltimo lugar de la lista
              lClm.addElement(dtModif);
              posicion = lClm.size() - 1;
            }
            //si sale del bucle pq llega a otro origen o pq
            //el siguiente nm_muestra es mayor

          }
          else {
            //si los c�digos son iguales busco el �ltimo lugar
            if (cdTablaMues == cdRecibidoMues) {
              //mientras estemos en el mismo origen y mismo
              //nm_muestra
              for (; cdTabla == cdRecibido && cdTablaMues == cdRecibidoMues
                   && i < lClm.size() - 1; ) {
                i++;
                //obtenemos el origen de la muestra
                cdTab = ( (Data) lClm.elementAt(i)).getString("CD_MUESTRA");
                cdTabla = Integer.parseInt(cdTab);
                if ( ( (Data) lClm.elementAt(i)).getString("IT_BD").equals("S")) {
                  cdTabMues = ( (Data) lClm.elementAt(i)).getString(
                      "NM_MUESTRA");
                  cdTablaMues = Integer.parseInt(cdTabMues);
                }
                else {
                  cdTabMues = "0";
                  cdTablaMues = 0;
                }
              } //end for
              //si se ha llegado al final de la lista
              if (i == lClm.size() - 1) {
                //si el �ltimo elemento es de distinto origen-> se
                //coloca previo a �l
                if (cdTabla != cdRecibido) {
                  lClm.insertElementAt(dtModif, i);
                  posicion = i;
                }
                else { //si no se coloca en el �ltimo lugar de la lista
                  lClm.addElement(dtModif);
                  posicion = lClm.size() - 1;
                }
                //si ha salido pq ha llegado a un origen distinto
                //o a una nm_muestra distinta
              }
              else {
                lClm.insertElementAt(dtModif, i);
                posicion = i;
              }
            }
            else { //si las muestras no son iguales, luego se ha
              //llegado a otro origen
              lClm.insertElementAt(dtModif, i);
              posicion = i;
            }
          }
          salir = true;
        } //end if hemos encontrado el origen nuestro
      } //end for lClm

      //si no se ha encontrado el origen nuestro
      if (salir == false) {
        boolean salir2 = false;

        //lo meto en la lista lClm: recorro la cListaMtmto->
        //por cada elemento saco su origen->cdTabla. Cuando
        //encuentre 1 elemento cuyo cd_muestra sea superior en
        //1 al q se intenta introducir, se introduce delante.
        for (int j = 0; j < lClm.size() && salir2 == false; j++) {
          String cdTab1 = ( (Data) lClm.elementAt(j)).getString("CD_MUESTRA");
          int cdTabla1 = Integer.parseInt(cdTab1);
          if (cdTabla1 > cdRecibido) {
            lClm.insertElementAt(dtModif, j);
            salir2 = true;
          }
        } //end for lClm
        //si no hemos encontrado -> se introduce al final
        if (salir2 == false) {
          lClm.addElement(dtModif);
          posicion = lClm.size() - 1;
        }

        /*        int m=0;
                boolean salir1=false;
                lClm.addElement(dtModif);
                posicion=lClm.size()-1;  */
      }

      /*        for (m=0;m<lClm.size()&& salir1==false;m++){
                String cdPru=((Data)lClm.elementAt(m)).getString("CD_MUESTRA");
                int cdPruNum=Integer.parseInt(cdPru);
                if (cdTabla==cdPruNum){
                  salir1=true;
                }
              }//end for localizar ultimo origen
              boolean salir2=false;
              for (m=m-1;m<lClm.size() && salir2==false;m++){
                //ordena por nm_muestra dentro del �ltimo origen
                cdTabMues=((Data)lClm.elementAt(m)).getString("NM_MUESTRA");
                cdTablaMues=Integer.parseInt(cdTabMues);
                if (cdRecibidoMues<cdTablaMues){
                  lClm.insertElementAt(dtModif,m);
                  posicion=m;
                  salir2=true;
                }
//          for(;cdRecibidoMues<cdTablaMues && m>limite;m++){}
//          if (m==limite){
//           lClm.insertElementAt(dtModif,m);
        //        }else{
          //        lClm.insertElementAt(dtModif,m+1);
            //    }
              //  salir2=true;
              } //end for
              if (salir2==false){
                lClm.addElement(dtModif);
                posicion=lClm.size()-1;
              }//end if salir1
            }//end if salir=false*/
    } //end if-else pertenece a bd
    return posicion;
  }

  //funci�n q genera la query y el data necesarios para el bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtDev.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtDev.getString("FC_ULTACT"));
  } //end prepara bloqueo

  Lista modificarHermanos(Data dtModif, Data dtAnterior) {
    Lista lModif = new Lista();
    String nmMues = dtAnterior.getString("NM_MUESTRA");
    //por si habia sido modificado alg�n elemento antes
    for (int i = 0; i < lClm.size(); i++) {
      ( (Data) lClm.elementAt(i)).put("ELIMINAR", "N");
    }
    for (int i = 0; i < lClm.size(); i++) {
      if ( ( (Data) lClm.elementAt(i)).getString("NM_MUESTRA").equals(nmMues)) {
        ( (Data) lClm.elementAt(i)).put("CD_MUESTRA",
                                        dtModif.getString("CD_MUESTRA"));
        ( (Data) lClm.elementAt(i)).put("DS_MUESTRA",
                                        dtModif.getString("DS_MUESTRA"));
        ( (Data) lClm.elementAt(i)).put("DS_RMUESTRA",
                                        dtModif.getString("DS_RMUESTRA"));
        ( (Data) lClm.elementAt(i)).put("NM_MUESBROTE",
                                        dtModif.getString("NM_MUESBROTE"));
        ( (Data) lClm.elementAt(i)).put("NM_POSITBROTE",
                                        dtModif.getString("NM_POSITBROTE"));
        String posicion = Integer.toString(i);
        ( (Data) lClm.elementAt(i)).put("ELIMINAR", "S");
        lModif.addElement( (Data) lClm.elementAt(i));
//        lClm.removeElementAt(i);
      }
    }
    return lModif;
  }

  //Obligatorio rellenar todos los campos
  public void realizaOperacion(int j) {
    boolean op = false;
    DiaMantMuestras di = null;
    Data dMant = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:

        //nos llevamos:CD_ANO,NM_ALERBRO,CD_GRUPO
        dMant = dtDev;
        dMant.put("IT_BD", "N");
        dMant.put("ELIMINAR", "N");
        di = new DiaMantMuestras(this.getApp(), ALTA, dMant);
        di.show();
        // a�adir el nuevo elem a la lista-> como �ltimo elemento de
        // su origen.
        if (di.bAceptar) {

          boolean escribir = false;
          Lista lAcep = new Lista();
          lAcep = di.blDevuelto();

          //se comprueba q no exista un elemento de = origen con igual descripcion
          escribir = comprobarRepeticion( (Data) lAcep.elementAt(0));

          //si se permite el alta
          if (escribir == true) {

            //recorro la lista q me devuelve y por cada elemento...
            for (int m = 0; m < lAcep.size(); m++) {
              //obtengo el origen->cdRecibido
              String cdRecib = ( (Data) lAcep.elementAt(m)).getString(
                  "CD_MUESTRA");
              int cdRecibido = Integer.parseInt(cdRecib);
              boolean salir = false;

              //lo meto en la lista lClm: recorro la cListaMtmto->
              //por cada elemento saco su origen->cdTabla. Cuando
              //encuentre 1 elemento cuyo cd_muestra sea superior en
              //1 al q se intenta introducir, se introduce delante.
              for (int i = 0; i < lClm.size() && salir == false; i++) {
                String cdTab = ( (Data) lClm.elementAt(i)).getString(
                    "CD_MUESTRA");
                int cdTabla = Integer.parseInt(cdTab);
                if (cdTabla > cdRecibido) {
                  lClm.insertElementAt( (Data) lAcep.elementAt(m), i);
                  salir = true;
                }
              } //end for lClm
              //si no hemos encontrado -> se introduce al final
              if (salir == false) {
                lClm.addElement( (Data) lAcep.elementAt(m));
              }
            } //end for lAcep
            lClm = anadirCodDescripcion(lClm);
            lClm = generarFlags(lClm);
            clmMuestras.setPrimeraPaginaMuestra(lClm);
          } //end escribir=true
        } //end bAceptar
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        Lista lModif = null;
        dMant = clmMuestras.getSelected();
        //si existe alguna fila seleccionada
        if (dMant != null) {
          int ind = clmMuestras.getSelectedIndex();
          di = new DiaMantMuestras(this.getApp(), MODIFICACION, dMant);
          di.show();
          //Tratamiento de bAceptar para Cancelar
          if (di.bAceptar) {
            //recojo los datos q nos devuelve el dialogo llamado
            Lista lAcepModif = null;
            Lista lHerm = new Lista();
            lAcepModif = di.blDevuelto();

//            boolean escribir=comprobarRepeticion((Data)lAcepModif.elementAt(0));
//            if (escribir==true){

            //recojo el primer elemento de la lista devuelta
            Data dtModif = (Data) lAcepModif.elementAt(0);

            //compruebo si la lista incluye m�s agentes(hermanos)
            //adem�s de el ya recogido. Los introduzco en ->lHerm
            for (int t = 1; t < lAcepModif.size(); t++) {
              lHerm.addElement( (Data) lAcepModif.elementAt(t));
            }

            //borro de la lista el elemento modificado para insertarlo
            //con los nuevos valores.
            lClm.removeElementAt(ind);

            //compruebo si existian elementos en la lista q deben
            //ser modificados porque poseen datos comunes con los q
            //se han traido
            lModif = modificarHermanos(dtModif, dMant);

            //si hay elementos modificables en la lista
            if (lModif != null) {
              //elimina todos las modificaciones
              for (int i = 0; i < lClm.size(); i++) {
                if ( ( (Data) lClm.elementAt(i)).getString("ELIMINAR").equals(
                    "S")) {
                  lClm.removeElementAt(i);
                  i = i - 1;
                }
              } //end for

              //AQUI:lModif contiene los elementos de la lista q deben
              //ser modificados. Ahora lo q se hace es introducir en
              //la misma lista los elementos q se traen del otro dialogo
              for (int t = 0; t < lHerm.size(); t++) {
                lModif.addElement( (Data) lHerm.elementAt(t));
              }

              //se le a�ade el primer elemento q se trae.
              lModif.addElement(dtModif);

              //lModif -> todos los elementos a modificar.
              for (int i = 0; i < lModif.size(); i++) {
                //se colocan en lClm en el lugar correspondiente,
                //uno por uno.
                situarModifEnLista( (Data) lModif.elementAt(i));
                lClm = anadirCodDescripcion(lClm);
                lClm = generarFlags(lClm);
              }
            } //end if modificar hermanos
            clmMuestras.setPrimeraPaginaMuestra(lClm);
//            }//end escribir
          } //end lAceptar
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dMant = clmMuestras.getSelected();
        if (dMant != null) {
          int ind = clmMuestras.getSelectedIndex();
          di = new DiaMantMuestras(this.getApp(), BAJA, dMant);
          di.show();
          if (di.bAceptar) {
            Lista lAcepBaja = new Lista();
            lAcepBaja = di.blDevuelto();
            Data dtBaja = (Data) lAcepBaja.elementAt(0);
            if (dtBaja.getString("IT_BD").equals("S")) {
              if (! (dtBaja.getString("IT_OPERACION").equals("4"))) {
                    /*            if ((!(dtBaja.getString("IT_OPERACION").equals("0")))
                     && (!(dtBaja.getString("IT_OPERACION").equals("4")))){*/
                lBaja.addElement(dtBaja);
              }
            }
            lClm.removeElementAt(ind);
            if (lClm.size() != 0) {
              generarFlags(lClm);
              clmMuestras.setPrimeraPaginaMuestra(lClm);
            }
            else {
              clmMuestras.setPrimeraPaginaMuestra(lClm);
            }
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }
  }

  //Genera los 2 flags para cada data q indican q columnas deben ser escritas
  Lista generarFlags(Lista lFlag) {
    Lista lGenFlag = new Lista();
    lGenFlag = lFlag;
    String sOrigen = new String();
    String sMuestra = new String();
    sOrigen = ( (Data) lGenFlag.elementAt(0)).getString("CD_MUESTRA");
    sMuestra = ( (Data) lGenFlag.elementAt(0)).getString("NM_MUESTRA");
    ( (Data) lGenFlag.elementAt(0)).put("IT_ORIGEN", "S");
    ( (Data) lGenFlag.elementAt(0)).put("IT_MUESTRA", "S");
    for (int i = 1; i < lGenFlag.size(); i++) {
      if ( ( (Data) lGenFlag.elementAt(i)).getString("CD_MUESTRA").equals(
          sOrigen)) {
        ( (Data) lGenFlag.elementAt(i)).put("IT_ORIGEN", "N");
        if ( ( (Data) lGenFlag.elementAt(i)).getString("NM_MUESTRA").equals(
            sMuestra)) {
          ( (Data) lGenFlag.elementAt(i)).put("IT_MUESTRA", "N");
        }
        else {
          ( (Data) lGenFlag.elementAt(i)).put("IT_MUESTRA", "S");
          sMuestra = ( (Data) lGenFlag.elementAt(i)).getString("NM_MUESTRA");
        }
      }
      else {
        ( (Data) lGenFlag.elementAt(i)).put("IT_ORIGEN", "S");
        sOrigen = ( (Data) lGenFlag.elementAt(i)).getString("CD_MUESTRA");
        if ( ( (Data) lGenFlag.elementAt(i)).getString("NM_MUESTRA").equals(
            sMuestra)) {
          ( (Data) lGenFlag.elementAt(i)).put("IT_MUESTRA", "N");
        }
        else {
          ( (Data) lGenFlag.elementAt(i)).put("IT_MUESTRA", "S");
          sMuestra = ( (Data) lGenFlag.elementAt(i)).getString("NM_MUESTRA");
        }
      }
    }
    return lGenFlag;
  }

  //concatena a�o y c�digo de brote
  Lista anadirCodDescripcion(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      ( (Data) lCampo.elementAt(i)).put("ORIGEN",
                                        dCampo.getString("CD_MUESTRA") + " - " +
                                        dCampo.getString("DS_MUESTRA"));
    } //end for

    return lCampo;
  } //end anadirPrincipal

  Lista anadirFlag(Lista lFlag) {
    for (int i = 0; i < lFlag.size(); i++) {
      ( (Data) lFlag.elementAt(i)).put("IT_BD", "S");
      ( (Data) lFlag.elementAt(i)).put("ELIMINAR", "N");
      ( (Data) lFlag.elementAt(i)).put("IT_OPERACION", "3");
    }
    return lFlag;
  }

  public Lista obtenerMuestras() {
    final String servlet1 = "servlet/SrvMantMuestras";
    Data dtMues = new Data();
    Lista lMues = new Lista();
    Lista lResulMuestra = new Lista();
    Inicializar(CInicializar.ESPERA);
    dtMues.put("CD_ANO", dtDev.getString("CD_ANO"));
    dtMues.put("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    dtMues.put("CD_GRUPO", dtDev.getString("CD_GRUPO"));
    lMues.addElement(dtMues);
    try {

      /*          SrvMantMuestras servlet3=new SrvMantMuestras();
           servlet3.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
                  lResulMuestra = (Lista) servlet3.doDebug(1,lMues); */

      lResulMuestra = BDatos.ejecutaSQL(false, this.getApp(), servlet1, 1,
                                        lMues);
      if (lResulMuestra.size() != 0) {
        lResulMuestra = generarFlags(lResulMuestra);
        lResulMuestra = anadirFlag(lResulMuestra);
        lResulMuestra = anadirCodDescripcion(lResulMuestra);
        lClm = lResulMuestra;
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    Inicializar(CInicializar.NORMAL);
    return lResulMuestra;
  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  public Lista primeraPagina() {
    Lista l = new Lista();
    return l;
  }

  public Lista siguientePagina() {
    Lista l = new Lista();
    return l;
  }

  QueryTool realizarUpdateMuestras(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_MUESTRAS_BROTE");
    qtModif.putType("CD_MUESTRA", QueryTool.STRING);
    qtModif.putValue("CD_MUESTRA", dtUpd.getString("CD_MUESTRA"));
    qtModif.putType("DS_RMUESTRA", QueryTool.STRING);
    qtModif.putValue("DS_RMUESTRA", dtUpd.getString("DS_RMUESTRA"));
    qtModif.putType("NM_MUESBROTE", QueryTool.INTEGER);
    qtModif.putValue("NM_MUESBROTE", dtUpd.getString("NM_MUESBROTE"));
    qtModif.putType("NM_POSITBROTE", QueryTool.INTEGER);
    qtModif.putValue("NM_POSITBROTE", dtUpd.getString("NM_POSITBROTE"));

    qtModif.putWhereType("NM_MUESTRA", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_MUESTRA", dtUpd.getString("NM_MUESTRA"));
    qtModif.putOperator("NM_MUESTRA", "=");

    return qtModif;
  }

  QueryTool realizarSelectSecuenciadores() {
    QueryTool qtSel = new QueryTool();
    qtSel.putName("SIVE_SEC_GENERAL");
    qtSel.putType("NM_MUESTRA", QueryTool.INTEGER);
    qtSel.putType("NM_MICROTOX", QueryTool.INTEGER);

    return qtSel;
  } //end realizarSelectSecuenciadores

  QueryTool realizarUpdateMicrotox(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_MICROTOX_MUESTRAS");
    qtModif.putType("CD_AGENTET", QueryTool.STRING);
    qtModif.putValue("CD_AGENTET", dtUpd.getString("CD_AGENTET"));
    qtModif.putType("DS_RESCUANTI", QueryTool.STRING);
    qtModif.putValue("DS_RESCUANTI", dtUpd.getString("DS_RESCUANTI"));

    qtModif.putWhereType("NM_MICROTOX", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_MICROTOX", dtUpd.getString("NM_MICROTOX"));
    qtModif.putOperator("NM_MICROTOX", "=");

    return qtModif;
  }

  QueryTool realizarDeleteMuestra(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_MUESTRAS_BROTE");

    qtModif.putWhereType("NM_MUESTRA", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_MUESTRA", dtUpd.getString("NM_MUESTRA"));
    qtModif.putOperator("NM_MUESTRA", "=");
    return qtModif;
  }

  QueryTool realizarDeleteMicrotox(Data dtUpd) {

    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_MICROTOX_MUESTRAS");

    qtModif.putWhereType("NM_MICROTOX", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_MICROTOX", dtUpd.getString("NM_MICROTOX"));
    qtModif.putOperator("NM_MICROTOX", "=");
    return qtModif;
  }

  String obtenerSecuenciadorMuestra() {
    final String servlet = "servlet/SrvQueryTool";
    String sec = new String();
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_SEC_GENERAL");
    qt.putType("NM_MUESTRA", QueryTool.INTEGER);
    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      String secuencial = ( (Data) p1.elementAt(0)).getString("NM_MUESTRA");
      secuenciadorMuestra = Integer.parseInt(secuencial);
//        secuenciadorMuestra ++;
      sec = Integer.toString(secuenciadorMuestra);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    return sec;
  }

  String obtenerSecuenciadorMicrotox() {
    final String servlet = "servlet/SrvQueryTool";
    String sec = new String();
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_SEC_GENERAL");
    qt.putType("NM_MICROTOX", QueryTool.INTEGER);
    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      String secuencial = ( (Data) p1.elementAt(0)).getString("NM_MICROTOX");
      secuenciadorMicrotox = Integer.parseInt(secuencial);
//        secuenciadorMicrotox ++;
      sec = Integer.toString(secuenciadorMicrotox);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    return sec;
  }

  QueryTool realizarUpdateBrote(Data dtUpd) {
    QueryTool qtUpd = new QueryTool();
//      String nmMues=dtUpd.getString("NM_MUESTRA");
    qtUpd.putName("SIVE_BROTES");
    qtUpd.putType("CD_OPE", QueryTool.STRING);
    qtUpd.putValue("CD_OPE", dtUpd.getString("CD_OPE"));
    qtUpd.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtUpd.putValue("FC_ULTACT", dtUpd.getString("FC_ULTACT"));

    // filtro
    qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");

    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");

    return qtUpd;
  }

  QueryTool realizarInsertMuestra(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    String nmMues = dtUpd.getString("NM_MUESTRA");
    qtIns.putName("SIVE_MUESTRAS_BROTE");
    qtIns.putType("NM_MUESTRA", QueryTool.INTEGER);
    qtIns.putValue("NM_MUESTRA", dtUpd.getString("NM_MUESTRA"));
    qtIns.putType("CD_MUESTRA", QueryTool.STRING);
    qtIns.putValue("CD_MUESTRA", dtUpd.getString("CD_MUESTRA"));
    qtIns.putType("CD_ANO", QueryTool.STRING);
    qtIns.putValue("CD_ANO", dtUpd.getString("CD_ANO"));
    qtIns.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtIns.putValue("NM_ALERBRO", dtUpd.getString("NM_ALERBRO"));
    qtIns.putType("DS_RMUESTRA", QueryTool.STRING);
    qtIns.putValue("DS_RMUESTRA", dtUpd.getString("DS_RMUESTRA"));
    qtIns.putType("NM_MUESBROTE", QueryTool.INTEGER);
    qtIns.putValue("NM_MUESBROTE", dtUpd.getString("NM_MUESBROTE"));
    qtIns.putType("NM_POSITBROTE", QueryTool.INTEGER);
    qtIns.putValue("NM_POSITBROTE", dtUpd.getString("NM_POSITBROTE"));

    return qtIns;
  }

  QueryTool realizarInsertMicrotox(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    qtIns.putName("SIVE_MICROTOX_MUESTRAS");
    qtIns.putType("NM_MICROTOX", QueryTool.INTEGER);
    qtIns.putValue("NM_MICROTOX", dtUpd.getString("NM_MICROTOX"));
    qtIns.putType("NM_MUESTRA", QueryTool.INTEGER);
    qtIns.putValue("NM_MUESTRA", dtUpd.getString("NM_MUESTRA"));
    qtIns.putType("CD_GRUPO", QueryTool.STRING);
    qtIns.putValue("CD_GRUPO", dtUpd.getString("CD_GRUPO"));
    qtIns.putType("CD_AGENTET", QueryTool.STRING);
    qtIns.putValue("CD_AGENTET", dtUpd.getString("CD_AGENTET"));
    qtIns.putType("DS_MICROTOX", QueryTool.STRING);
    qtIns.putValue("DS_MICROTOX", dtUpd.getString("DS_MICROTOX"));
    qtIns.putType("DS_RESCUANTI", QueryTool.STRING);
    qtIns.putValue("DS_RESCUANTI", dtUpd.getString("DS_RESCUANTI"));

    return qtIns;
  }

  /*   if (e.getActionCommand().equals("Aceptar")) {
     Lista lPrueba=new Lista();
        for (int j=0;j<lBaja.size();j++){
          Data dtBaja= (Data)lBaja.elementAt(j);
          Inicializar(CInicializar.ESPERA);
          if (dtBaja.getString("IT_OPERACION").equals("2")){
            this.getApp().trazaLog("dtBaja " + dtBaja);
            QueryTool qtDel1=new QueryTool();
            qtDel1=realizarDeleteMicrotox(dtBaja);
            Data dtDelete1 = new Data();
            dtDelete1.put("5",qtDel1); // DO_DELETE
            lPrueba.addElement(dtDelete1);
            QueryTool qtDel=new QueryTool();
            qtDel=realizarDeleteMuestra(dtBaja);
            Data dtDelete = new Data();
            dtDelete.put("5",qtDel); // DO_DELETE
            lPrueba.addElement(dtDelete);
          }//end if op=2
        }//end for lBaja
        //se ejecutan todas las operaciones
          try{
            BDatos.ejecutaSQL(false,this.getApp(),srvTrans,0,lPrueba);
            //si todo normal aumentar los secuenciadores
            bAceptar=true;
            dispose();
          } catch (Exception ex) {
            this.getApp().trazaLog(ex);
            this.getApp().showError(ex.getMessage());
            bAceptar=false;
            dispose();
          }
        Inicializar(CInicializar.NORMAL);
     }//end if aceptar
   }//end btnact*/
  //Incrementa el secuenciador de sive_muestras_brote
  void incrementarSecuenciadores() {
    Lista vPetic = new Lista();
    Lista vFiltro = new Lista();
    final String servlet = "servlet/SrvQueryTool";
    Inicializar(CInicializar.ESPERA);
    try {
      //Relleno los par�metros
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_SEC_GENERAL");
      qt.putType("NM_MUESTRA", QueryTool.INTEGER);
      qt.putValue("NM_MUESTRA", Integer.toString(secuenciadorMuestra));
      qt.putType("NM_MICROTOX", QueryTool.INTEGER);
      qt.putValue("NM_MICROTOX", Integer.toString(secuenciadorMicrotox));

      this.getApp().getStub().setUrl(servlet);
      vFiltro.addElement(qt);
      vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 4, vFiltro);

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
      bAceptar = false;
      dispose();
    }
    Inicializar(CInicializar.NORMAL);
  }

  boolean comprobarRepeticion(Data dtRepet) {
    boolean b = false;
    boolean encontrado = false;
    String cdOrigen = dtRepet.getString("CD_MUESTRA");
    String dsMuestra = dtRepet.getString("DS_RMUESTRA");
    for (int i = 0; i < lClm.size() && encontrado == false; i++) {
      if ( ( ( (Data) lClm.elementAt(i)).getString("CD_MUESTRA").equals(
          cdOrigen))
          &&
          ( ( (Data) lClm.elementAt(i)).getString("DS_RMUESTRA").equals(dsMuestra))) {
        this.getApp().showAdvise(
            "Muestra ya introducida. Debe realizar una modificaci�n de la misma");
        encontrado = true;
        b = false;
      }
    }
    if (encontrado == false) {
      b = true;
    }
    return b;
  }

  Lista actualizarBd(String secMuestra, String secMicrotox) {
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();
    Lista vResultado = new Lista();
    boolean existeAlta = false;
    Lista lAltaMuestras = new Lista();

    //recorro lClm y seg�n su it_operaci�n realizo la operaci�n q toca
    //Obtengo los valores actuales de los secuenciadores
    for (int i = 0; i < lClm.size(); i++) {
      Data dtClm = (Data) lClm.elementAt(i);

      //Se realiza un alta
      if (dtClm.getString("IT_OPERACION").equals("0")) {
        if (lAltaMuestras.size() == 0) {
          secuenciadorMuestra++;
          secMuestra = Integer.toString(secuenciadorMuestra);
          secuenciadorMicrotox++;
          secMicrotox = Integer.toString(secuenciadorMicrotox);
          String nmMues = dtClm.getString("NM_MUESTRA");
          dtClm.put("NM_MUESTRA", secMuestra);
          dtClm.put("NM_MICROTOX", secMicrotox);
          Data dtAlta = new Data();
          dtAlta = dtClm;
          dtAlta.put("NM_MUESANTER", nmMues);

          QueryTool qtIns = new QueryTool();
          qtIns = realizarInsertMuestra(dtClm);
          Data dtInsert = new Data();
          dtInsert.put("3", qtIns);
          lBd.addElement(dtInsert);
          lAltaMuestras.addElement(dtAlta);
        }
        else { //si ya ha habido alg�n alta compruebo si ha sido de un padre mio
          boolean exit = false;
          for (int x = 0; x < lAltaMuestras.size() && exit == false; x++) {
            if ( ( (Data) lAltaMuestras.elementAt(x)).getString("NM_MUESANTER").
                equals(dtClm.getString("NM_MUESTRA"))) {
              secMuestra = ( (Data) lAltaMuestras.elementAt(x)).getString(
                  "NM_MUESTRA");
              secuenciadorMicrotox++;
              secMicrotox = Integer.toString(secuenciadorMicrotox);
              dtClm.put("NM_MUESTRA", secMuestra);
              dtClm.put("NM_MICROTOX", secMicrotox);
              exit = true;
            }
          } //end for
          //si no ha sido dado de alta ning�n hermano suyo
          if (exit == false) {
            secuenciadorMuestra++;
            secMuestra = Integer.toString(secuenciadorMuestra);
            secuenciadorMicrotox++;
            secMicrotox = Integer.toString(secuenciadorMicrotox);
            String nmMues = dtClm.getString("NM_MUESTRA");
            dtClm.put("NM_MUESTRA", secMuestra);
            dtClm.put("NM_MICROTOX", secMicrotox);
            Data dtAlta = new Data();
            dtAlta = dtClm;
            dtAlta.put("NM_MUESANTER", nmMues);

            QueryTool qtIns = new QueryTool();
            qtIns = realizarInsertMuestra(dtClm);
            Data dtInsert = new Data();
            dtInsert.put("3", qtIns);
            lBd.addElement(dtInsert);
            lAltaMuestras.addElement(dtAlta);
          } //end if exit==false
        } //end if lAltaMuest

        QueryTool qtIns1 = new QueryTool();
        qtIns1 = realizarInsertMicrotox(dtClm);
        Data dtInsert1 = new Data();
        dtInsert1.put("3", qtIns1);
        lBd.addElement(dtInsert1);

        existeAlta = true;
      } //end if op=0

      //alta en una modificaci�n, solo rellenar sive_microtox
      if (dtClm.getString("IT_OPERACION").equals("4")) {
        secuenciadorMicrotox++;
        secMicrotox = Integer.toString(secuenciadorMicrotox);
        dtClm.put("NM_MICROTOX", secMicrotox);
        QueryTool qtIn = new QueryTool();
        qtIn = realizarInsertMicrotox(dtClm);
        Data dtIn = new Data();
        dtIn.put("3", qtIn);
        lBd.addElement(dtIn);
        existeAlta = true;
      } //end if op=4

      //Se realiza la modificaci�n:update
      if (dtClm.getString("IT_OPERACION").equals("1")) {
        QueryTool qtUpdMu = realizarUpdateMuestras(dtClm);
        Data dtUpdate = new Data();
        dtUpdate.put("4", qtUpdMu);
        lBd.addElement(dtUpdate);

        QueryTool qtUpd = realizarUpdateMicrotox(dtClm);
        Data dtUpdate1 = new Data();
        dtUpdate1.put("4", qtUpd);
        lBd.addElement(dtUpdate1);
      } //end if op=1
    } //end for

    //Se realiza la baja
    for (int j = 0; j < lBaja.size(); j++) {
      Data dtBaja = (Data) lBaja.elementAt(j);
      if (dtBaja.getString("IT_OPERACION").equals("2")) {
        QueryTool qtDel1 = new QueryTool();
        qtDel1 = realizarDeleteMicrotox(dtBaja);
        Data dtDelete1 = new Data();
        dtDelete1.put("5", qtDel1); // DO_DELETE
        lBd.addElement(dtDelete1);

        //solo se borra en microtox si no queda ning�n registro en muestras
        boolean encontrado = false;
        for (int n = 0; n < lClm.size() && encontrado == false; n++) {
          if ( ( (Data) lClm.elementAt(n)).getString("NM_MUESTRA").equals(
              dtBaja.getString("NM_MUESTRA"))) {
            encontrado = true;
          }
        }
        if (encontrado == false) {
          for (int s = 0; s < lBaja.size() && encontrado == false; s++) {
            if (s != j) {
              if ( ( (Data) lBaja.elementAt(s)).getString("NM_MUESTRA").equals(
                  dtBaja.getString("NM_MUESTRA"))) {
                encontrado = true;
              }
            }
          }
          if (encontrado == false) {
            QueryTool qtDel = new QueryTool();
            qtDel = realizarDeleteMuestra(dtBaja);
            Data dtDelete = new Data();
            dtDelete.put("5", qtDel); // DO_DELETE
            lBd.addElement(dtDelete);
          }
        }
      } //end if op=2
    } //end for lBaja
    //si se ha modificado algo:se inserta para ese brote el nuevo
    //cd_ope y fc_ultact
    if (lBd.size() != 0) {
      Data dtDatBrot = new Data();
      dtDatBrot.put("CD_OPE", this.getApp().getParametro("login"));
      dtDatBrot.put("FC_ULTACT", "");

      QueryTool qtUpdBro = new QueryTool();
      qtUpdBro = realizarUpdateBrote(dtDatBrot);
      Data dtUpdBro = new Data();
      dtUpdBro.put("4", qtUpdBro);
      lBd.addElement(dtUpdBro);
//    }

      //preparaBloqueo();
      try {
        //BDatos.ejecutaSQL(false,this.getApp(),srvTrans,0,lBd);
        /*          SrvTransaccion servlet=new SrvTransaccion();
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                    lBd = (Lista) servlet.doDebug(0,lBd);*/
        this.getApp().getStub().setUrl(srvTrans);
        vResultado = (Lista)this.getApp().getStub().doPost(0, lBd);

        /*          vResultado = (Lista)this.getApp().getStub().doPost(10000,
                                                                 lBd,
                                                                 qtBloqueo,
                                                                 dtBloqueo,
                                                                   getApp());*/
        //si ha existido alg�n alta aumentar los secuenciadores
        if (existeAlta == true) {
          incrementarSecuenciadores();
        }

      }
      catch (Exception exc) {
        this.getApp().trazaLog(exc);
        this.getApp().showError(exc.getMessage());
        bAceptar = false;
        dispose();
      }
    } //end lBd.size!=0
    return vResultado;
  } //end actualizarBd

  void btn_actionPerformed(ActionEvent e) {
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();
    Lista vResultado = new Lista();
    String secMuestra = new String();
    String secMicrotox = new String();
    boolean existeAlta = false;
    Lista lAltaMuestras = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      //busco los secuenciadores y bloqueo:
      Inicializar(CInicializar.ESPERA);

      //preparo el bloqueo
      preparaBloqueo();

      //prepara la selecci�n de los secuenciadores
      QueryTool qtSel = new QueryTool();
      qtSel = realizarSelectSecuenciadores();
      Data dtSel = new Data();
      dtSel.put("1", qtSel);
      lSec.addElement(dtSel);

      //prepara la modificaci�n de cd_ope y fc_ultact en sive_brotes
      Data dtDatBrot1 = new Data();
      dtDatBrot1.put("CD_OPE", this.getApp().getParametro("login"));
      dtDatBrot1.put("FC_ULTACT", "");

      QueryTool qtUpdBro1 = new QueryTool();
      qtUpdBro1 = realizarUpdateBrote(dtDatBrot1);
      Data dtUpdBro1 = new Data();
      dtUpdBro1.put("10006", qtUpdBro1);
      lSec.addElement(dtUpdBro1);
      try {
        this.getApp().getStub().setUrl(srvTrans);

        /*      SrvTransaccion servlet4=new SrvTransaccion();
              servlet4.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
              vResultado = (Lista) servlet4.doDebug(0,lSec);*/
        vResultado = (Lista)this.getApp().getStub().doPost(10000,
            lSec,
            qtBloqueo,
            dtBloqueo,
            getApp());
        Lista lResultado = (Lista) vResultado.elementAt(0);
        Data dtResultado = (Data) lResultado.elementAt(0);
        secMuestra = dtResultado.getString("NM_MUESTRA");
        secuenciadorMuestra = Integer.parseInt(secMuestra);
        secMicrotox = dtResultado.getString("NM_MICROTOX");
        secuenciadorMicrotox = Integer.parseInt(secMicrotox);

        Lista vResul = actualizarBd(secMuestra, secMicrotox);
        //si se ha realizado alguna modificaci�n
        if (lBd.size() != 0) {
          //obtener el nuevo cd_ope y fc_ultact de la base de datos.
          int tamano = vResul.size();
          String fecha = null;
          for (int i = 0; i < vResul.size(); i++) {
            fecha = null;
            fecha = ( (Lista) vResul.elementAt(i)).getFC_ULTACT();
          }
          dtDev.put("CD_OPE", this.getApp().getParametro("login"));
          dtDev.put("FC_ULTACT", fecha);
          bAceptar = true;
          dispose();
        }
        else {
          //no se ha realizado ninguna modificaci�n luego...
          bAceptar = false;
          dispose();
        } //end else si lBd!=0
      }
      catch (Exception ex) {
        if (Common.ShowPregunta(this.getApp(), "Los datos han sido modificados por otro usuario. �Sigue queriendo realizar esta actualizaci�n?")) {
          try {
            //se obtienen los secuenciadores sin mirar el bloqueo
            ModosOperacNormalSec();
            this.getApp().getStub().setUrl(srvTrans);
            vResultado = (Lista)this.getApp().getStub().doPost(0, lSec);

            Lista lResultado = (Lista) vResultado.elementAt(0);
            Data dtResultado = (Data) lResultado.elementAt(0);
            secMuestra = dtResultado.getString("NM_MUESTRA");
            secuenciadorMuestra = Integer.parseInt(secMuestra);
            secMicrotox = dtResultado.getString("NM_MICROTOX");
            secuenciadorMicrotox = Integer.parseInt(secMicrotox);

            //realizo operaciones de actualizaci�n
            Lista vResultado2 = actualizarBd(secMuestra, secMicrotox);

            //si se ha realizado alguna modificaci�n
            if (lBd.size() != 0) {
              //si todas las actualizaciones se han realizado correctamente
              //obtener el nuevo cd_ope y fc_ultact de la base de datos.
              int tamano = vResultado2.size();
              String fecha = null;
              for (int i = 0; i < vResultado2.size(); i++) {
                fecha = null;
                fecha = ( (Lista) vResultado2.elementAt(i)).getFC_ULTACT();
              }
              dtDev.put("CD_OPE", this.getApp().getParametro("login"));
              dtDev.put("FC_ULTACT", fecha);
              bAceptar = true;
              dispose();
            }
            else {
              //no se ha realizado ninguna modificaci�n luego...
              bAceptar = false;
              dispose();
            }
          }
          catch (Exception exc) {
            this.getApp().trazaLog(exc);
            this.getApp().showError(ex.getMessage());
            bAceptar = false;
            dispose();
          } //end 2.catch
        } //end if comun.showpregunta
      } //end 1. catch

      Inicializar(CInicializar.NORMAL);

    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  void ModosOperacNormalSec() {
    //prepara la selecci�n de los secuenciadores
    QueryTool qtSel = new QueryTool();
    qtSel = realizarSelectSecuenciadores();
    Data dtSel = new Data();
    dtSel.put("1", qtSel);
    lSec.addElement(dtSel);

    //prepara la modificaci�n de cd_ope y fc_ultact en sive_brotes
    Data dtDatBrot1 = new Data();
    dtDatBrot1.put("CD_OPE", this.getApp().getParametro("login"));
    dtDatBrot1.put("FC_ULTACT", "");

    QueryTool qtUpdBro1 = new QueryTool();
    qtUpdBro1 = realizarUpdateBrote(dtDatBrot1);
    Data dtUpdBro1 = new Data();
    dtUpdBro1.put("4", qtUpdBro1);
    lSec.addElement(dtUpdBro1);
  } //end ModosOperacionNormalSec

  public boolean ModosOperacNormal() {
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();
    Lista vResultado = new Lista();
    String secMuestra = new String();
    String secMicrotox = new String();
    boolean existAlta = false;
    Lista lAltaMuestras = new Lista();

    lBd = null;
    lBd = new Lista();
    secMuestra = obtenerSecuenciadorMuestra();
    secMicrotox = obtenerSecuenciadorMicrotox();
    Inicializar(CInicializar.ESPERA);
    //recorro lClm y seg�n su it_operaci�n realizo la operaci�n q toca
    //Obtengo los valores actuales de los secuenciadores
    for (int i = 0; i < lClm.size(); i++) {
      Data dtClm = (Data) lClm.elementAt(i);

      //Se realiza un alta
      if (dtClm.getString("IT_OPERACION").equals("0")) {
        if (lAltaMuestras.size() == 0) {
          secuenciadorMuestra++;
          secMuestra = Integer.toString(secuenciadorMuestra);
          secuenciadorMicrotox++;
          secMicrotox = Integer.toString(secuenciadorMicrotox);
          String nmMues = dtClm.getString("NM_MUESTRA");
          dtClm.put("NM_MUESTRA", secMuestra);
          dtClm.put("NM_MICROTOX", secMicrotox);
          Data dtAlta = new Data();
          dtAlta = dtClm;
          dtAlta.put("NM_MUESANTER", nmMues);

          QueryTool qtIns = new QueryTool();
          qtIns = realizarInsertMuestra(dtClm);
          Data dtInsert = new Data();
          dtInsert.put("3", qtIns);
          lBd.addElement(dtInsert);
          lAltaMuestras.addElement(dtAlta);
        }
        else { //si ya ha habido alg�n alta compruebo si ha sido de un padre mio
          boolean exit = false;
          for (int x = 0; x < lAltaMuestras.size() && exit == false; x++) {
            if ( ( (Data) lAltaMuestras.elementAt(x)).getString("NM_MUESANTER").
                equals(dtClm.getString("NM_MUESTRA"))) {
              secMuestra = ( (Data) lAltaMuestras.elementAt(x)).getString(
                  "NM_MUESTRA");
              secuenciadorMicrotox++;
              secMicrotox = Integer.toString(secuenciadorMicrotox);
              dtClm.put("NM_MUESTRA", secMuestra);
              dtClm.put("NM_MICROTOX", secMicrotox);
              exit = true;
            }
          } //end for
          //si no ha sido dado de alta ning�n hermano suyo
          if (exit == false) {
            secuenciadorMuestra++;
            secMuestra = Integer.toString(secuenciadorMuestra);
            secuenciadorMicrotox++;
            secMicrotox = Integer.toString(secuenciadorMicrotox);
            String nmMues = dtClm.getString("NM_MUESTRA");
            dtClm.put("NM_MUESTRA", secMuestra);
            dtClm.put("NM_MICROTOX", secMicrotox);
            Data dtAlta = new Data();
            dtAlta = dtClm;
            dtAlta.put("NM_MUESANTER", nmMues);

            QueryTool qtIns = new QueryTool();
            qtIns = realizarInsertMuestra(dtClm);
            Data dtInsert = new Data();
            dtInsert.put("3", qtIns);
            lBd.addElement(dtInsert);
            lAltaMuestras.addElement(dtAlta);
          } //end if exit==false
        } //end if lAltaMuest

        QueryTool qtIns1 = new QueryTool();
        qtIns1 = realizarInsertMicrotox(dtClm);
        Data dtInsert1 = new Data();
        dtInsert1.put("3", qtIns1);
        lBd.addElement(dtInsert1);

        existAlta = true;
      } //end if op=0

      //alta en una modificaci�n, solo rellenar sive_microtox
      if (dtClm.getString("IT_OPERACION").equals("4")) {
        secuenciadorMicrotox++;
        secMicrotox = Integer.toString(secuenciadorMicrotox);
        dtClm.put("NM_MICROTOX", secMicrotox);
        QueryTool qtIn = new QueryTool();
        qtIn = realizarInsertMicrotox(dtClm);
        Data dtIn = new Data();
        dtIn.put("3", qtIn);
        lBd.addElement(dtIn);
        existAlta = true;
      } //end if op=4

      //Se realiza la modificaci�n:update
      if (dtClm.getString("IT_OPERACION").equals("1")) {
        QueryTool qtUpdMu = realizarUpdateMuestras(dtClm);
        Data dtUpdate = new Data();
        dtUpdate.put("4", qtUpdMu);
        lBd.addElement(dtUpdate);

        QueryTool qtUpd = realizarUpdateMicrotox(dtClm);
        Data dtUpdate1 = new Data();
        dtUpdate1.put("4", qtUpd);
        lBd.addElement(dtUpdate1);
      } //end if op=1
    } //end for

    //Se realiza la baja
    for (int j = 0; j < lBaja.size(); j++) {
      Data dtBaja = (Data) lBaja.elementAt(j);
      if (dtBaja.getString("IT_OPERACION").equals("2")) {
        QueryTool qtDel1 = new QueryTool();
        qtDel1 = realizarDeleteMicrotox(dtBaja);
        Data dtDelete1 = new Data();
        dtDelete1.put("5", qtDel1); // DO_DELETE
        lBd.addElement(dtDelete1);

        //solo se borra en microtox si no queda ning�n registro en muestras
        boolean encontrado = false;
        for (int n = 0; n < lClm.size() && encontrado == false; n++) {
          if ( ( (Data) lClm.elementAt(n)).getString("NM_MUESTRA").equals(
              dtBaja.getString("NM_MUESTRA"))) {
            encontrado = true;
          }
        }
        if (encontrado == false) {
          for (int s = 0; s < lBaja.size() && encontrado == false; s++) {
            if (s != j) {
              if ( ( (Data) lBaja.elementAt(s)).getString("NM_MUESTRA").equals(
                  dtBaja.getString("NM_MUESTRA"))) {
                encontrado = true;
              }
            }
          }
          if (encontrado == false) {
            QueryTool qtDel = new QueryTool();
            qtDel = realizarDeleteMuestra(dtBaja);
            Data dtDelete = new Data();
            dtDelete.put("5", qtDel); // DO_DELETE
            lBd.addElement(dtDelete);
          }
        }
      } //end if op=2
    } //end for lBaja
    return existAlta;
  } //end  funci�n ModosOperacNormal

  public boolean bAceptar() {
    return bAceptar;
  }

  public void verDatos() {
    Inicializar(CInicializar.ESPERA);
    clmMuestras.setPrimeraPaginaMuestra(obtenerMuestras());
    Inicializar(CInicializar.NORMAL);
  }
}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaResumMuestras adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaResumMuestras adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (e.getActionCommand().equals("Aceptar")) {
        adaptee.btn_actionPerformed(e);
        //adaptee.btnAreaActionPerformed();
      }
      if (e.getActionCommand().equals("Cancelar")) {
        adaptee.btn_actionPerformed(e);
      }
      s.desbloquea(adaptee);
    }
  }
}
