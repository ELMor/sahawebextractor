package brotes.cliente.mantmuestras;

import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
//import centinelas.datos.c_mantPC.*;
import brotes.cliente.c_componentes.CPnlCodigoExt;
import brotes.cliente.c_componentes.ContCPnlCodigoExt;
import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaMantMuestras
    extends CDialog
    implements CInicializar, ContCPnlCodigoExt {

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int TAM_BOT_LUPA = 24;

  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // parametros pasados del di�logo padre
  public Data dtDev = null;
  //par�metros devueltos al di�logo padre
  public Lista lDevuelto = new Lista();

//  private boolean tieneDatos=false;
//  private String interv=new String();

  public boolean bAceptar = false;
//  public int borrPto=0;
//  boolean noMensaje=false;

  // modo de operaci�n
  public int modoOperacion = 0;
  public int modoBloqueo = modoNORMAL;

  //vble para saber si han sido introducidos agentes
  private boolean agIntroducido = false;
  private Lista lAgente = new Lista();
  XYLayout xyLayout1 = new XYLayout();

  Label lblOrigen = new Label();
  Label lblMuestra = new Label();
  Label lblMuesAnal = new Label();
  Label lblMuesPosit = new Label();
  Label lblAgente = new Label();
  Label lblResul = new Label();

  CTexto txtMuestra = new CTexto(40);
  CTexto txtResultado = new CTexto(30);
  CEntero txtMuesAnal = new CEntero(5);
  CEntero txtMuesPosit = new CEntero(5);
  CPnlCodigoExt pnlOrigen = null;
  CPnlCodigoExt pnlAgente = null;

  //bot�n Lupa de las descripciones de las muestras
  ButtonControl btnLupa = new ButtonControl();

  GroupBox gbAgente = new GroupBox();

  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnNuevoAg = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter1 actionAdapter = new DialogActionAdapter1(this);

  public DiaMantMuestras(CApp a, int modoop, Data dt) {
    super(a);
    modoOperacion = modoop;
    dtDev = dt;
    //configuro el panel de origen
    QueryTool qtOrigen = new QueryTool();
    qtOrigen.putName("SIVE_ORIGEN_MUESTRA");
    qtOrigen.putType("CD_MUESTRA", QueryTool.STRING);
    qtOrigen.putType("DS_MUESTRA", QueryTool.STRING);

    pnlOrigen = new CPnlCodigoExt(a,
                                  this,
                                  qtOrigen,
                                  "CD_MUESTRA",
                                  "DS_MUESTRA",
                                  true,
                                  "Origen muestra",
                                  "Origen muestra",
                                  this);
    //configuro el panel de agentes
    QueryTool qtAgente = new QueryTool();
    qtAgente.putName("SIVE_AGENTE_TOXICO");
    qtAgente.putType("CD_AGENTET", QueryTool.STRING);
    qtAgente.putType("DS_AGENTET", QueryTool.STRING);

    qtAgente.putWhereType("CD_GRUPO", QueryTool.STRING);
    qtAgente.putWhereValue("CD_GRUPO", dtDev.getString("CD_GRUPO"));
    qtAgente.putOperator("CD_GRUPO", "=");

    //panel de origen
    pnlAgente = new CPnlCodigoExt(a,
                                  this,
                                  qtAgente,
                                  "CD_AGENTET",
                                  "DS_AGENTET",
                                  true,
                                  "Agente t�xico",
                                  "Agente t�xico",
                                  this);

    dtDev = dt;
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        pnlOrigen.backupDatos();
        pnlAgente.backupDatos();
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case ALTA:
            this.setEnabled(true);
            break;
          case MODIFICACION:
            this.setEnabled(true);
            pnlOrigen.setEnabled(false);
            break;
          case BAJA:
          case CONSULTA:
            this.setEnabled(false);
            break;
        }
    }
  }

  boolean comprobarAgente() {
    boolean b = false;
    switch (modoOperacion) {
      case ALTA:
        if ( (pnlAgente.getDatos() == null) &&
            (agIntroducido == false)) {
          this.getApp().showAdvise("Debe introducir un agente como m�nimo");
          b = false;
        }
        else {
          if (pnlAgente.getDatos() != null) {
            introducirAgente();
          }
          b = true;
        }
        break;
      case MODIFICACION:
        if ( (pnlAgente.getDatos() == null) &&
            (agIntroducido == false)) {
          this.getApp().showAdvise("El campo de agente es obligatorio");
          b = false;
        }
        else {
          if (pnlAgente.getDatos() != null) {
            introducirAgente();
          }
          b = true;
        }
        break;
      case BAJA:
        if (pnlAgente.getDatos() == null) {
          this.getApp().showAdvise("El campo de agente es obligatorio");
          b = false;
        }
        else {
          b = true;
        }
        break;
    }
    return b;
  }

  private boolean isDataValid() {
    if ( (pnlOrigen.getDatos() == null) ||
        (txtMuestra.getText().trim().equals("")) ||
        (txtMuesAnal.getText().trim().equals("")) ||
        (txtMuesPosit.getText().trim().equals(""))) {
      this.getApp().showAdvise(
          "Debe rellenar todos los campos obligatoriamente");
      return false;
    }
    else {
      //compruebo q n�muestras positivas no sea mayor q el de analizadas
      int muesAnal = Integer.parseInt(txtMuesAnal.getText());
      int muesPosit = Integer.parseInt(txtMuesPosit.getText());
      if (muesPosit > muesAnal) {
        this.getApp().showAdvise(
            "El n� de muestras positivas no puede ser superior al de analizadas");
        return false;
      }
      else {
        boolean b = comprobarAgente();
        return b;
      }
    } //fin primer if
  }

  void jbInit() throws Exception {
    final String imgLUPA = "images/magnify.gif";
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(640, 400);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgLUPA);
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();
    btnLupa.setImage(this.getApp().getLibImagenes().get(imgLUPA));

    // marcar como campos obligatorios de relleno

    lblOrigen.setText("Origen:");
    lblMuestra.setText("Muestra:");
    lblMuesAnal.setText("N� muestras analizadas:");
    lblMuesPosit.setText("N� muestras positivas:");
    lblAgente.setText("Agente/Microorganismo/Toxinas:");
    lblResul.setText("Resultado cuantitativo:");
    gbAgente.setLabel("Datos de agentes t�xicos");

    //todos los campos son obligatorios
    txtMuestra.setBackground(new Color(255, 255, 150));
    txtMuesAnal.setBackground(new Color(255, 255, 150));
    txtMuesPosit.setBackground(new Color(255, 255, 150));

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
//    btnAceptar.addActionListener(new DiaMantPoblacMed_btnAceptar_actionAdapter(this));
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
//    btnCancelar.addActionListener(new DiaMantPoblacMed_btnCancelar_actionAdapter(this));
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    btnNuevoAg.setActionCommand("Nuevo Agente");
    btnNuevoAg.setLabel("Nuevo Agente");
    btnNuevoAg.addActionListener(new DiaMantMuestras_btnNuevoAg_actionAdapter(this));
    btnLupa.setActionCommand("Lupa");
    btnLupa.addActionListener(new DiaMantMuestras_btnLupa_actionAdapter(this));

//    btnBuscar.setLabel("Buscar");
//    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(400);
    xyLayout1.setWidth(450);

    this.add(lblOrigen, new XYConstraints(MARGENIZQ, MARGENSUP, 150, ALTO));
    this.add(pnlOrigen,
             new XYConstraints(MARGENIZQ + 150 + INTERHOR - 5, MARGENSUP, 400,
                               ALTO + 10));
    this.add(lblMuestra,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT + 10,
                               150, ALTO));
    this.add(txtMuestra,
             new XYConstraints(MARGENIZQ + 150 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT + 10, 150, ALTO));
    this.add(btnLupa,
             new XYConstraints(MARGENIZQ + 150 + 2 * INTERHOR + 150,
                               MARGENSUP + ALTO + INTERVERT + 10, TAM_BOT_LUPA,
                               TAM_BOT_LUPA));
    this.add(lblMuesAnal,
             new XYConstraints(MARGENIZQ,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT + 10, 150,
                               ALTO));
    this.add(txtMuesAnal,
             new XYConstraints(MARGENIZQ + 150 + INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT + 10, 60,
                               ALTO));
    this.add(lblMuesPosit,
             new XYConstraints(MARGENIZQ,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT + 10, 150,
                               ALTO));
    this.add(txtMuesPosit,
             new XYConstraints(MARGENIZQ + 150 + INTERHOR,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT + 10, 60,
                               ALTO));
    this.add(lblAgente,
             new XYConstraints(MARGENIZQ + 10,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT + 10 + ALTO,
                               180, ALTO));
    this.add(pnlAgente,
             new XYConstraints(MARGENIZQ + 180 + INTERHOR + 10,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT + 10 + ALTO,
                               320, ALTO + 10));
    this.add(lblResul,
             new XYConstraints(MARGENIZQ + 10,
                               MARGENSUP + 5 * ALTO + 5 * INTERVERT + 10 + 10 +
                               ALTO, 180, ALTO));
    this.add(txtResultado,
             new XYConstraints(MARGENIZQ + 180 + INTERHOR + 10 + 5,
                               MARGENSUP + 5 * ALTO + 5 * INTERVERT + 10 + 10 +
                               ALTO, 150, ALTO));
    this.add(btnNuevoAg,
             new XYConstraints(MARGENIZQ + 2 * 10 + 350 + INTERHOR + 10 + 180 -
                               88 - 10 - 45,
                               MARGENSUP + 6 * ALTO + 6 * INTERVERT + 10 + 10 +
                               ALTO, 88, 29));
    this.add(gbAgente,
             new XYConstraints(MARGENIZQ - 5,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT + 10,
                               2 * 10 + 350 + INTERHOR + 10 + 180 - 88 - 10 -
                               30 + 88 + 10, 10 + 2 * ALTO + 88 + 2 * INTERVERT));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 2 * 10 + 350 + INTERHOR + 10 + 180 -
                               88 - 10 - 30,
                               MARGENSUP + 6 * ALTO + 6 * INTERVERT + 10 + 10 +
                               ALTO + 29 + 15 + 3 * INTERVERT, 88, 29));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 2 * 10 + 350 + INTERHOR + 10 + 180 -
                               88 - 10 - 30 - 88 - INTERHOR,
                               MARGENSUP + 6 * ALTO + 6 * INTERVERT + 10 + 10 +
                               ALTO + 29 + 15 + 3 * INTERVERT, 88, 29));

    verDatos();

    setTitle("Brotes: Resumen de Muestras");
    /*switch (modoOperacion) {
      case ALTA:setTitle("ALTA");break;
      case MODIFICACION:this.setTitle("MODIFICACION");break;
      case BAJA:this.setTitle("BAJA");break;
      case CONSULTA:this.setTitle("CONSULTA");break;
         }*/
  }

  public void rellenarDatos() {
    pnlOrigen.setCodigo(dtDev);
    txtMuestra.setText(dtDev.getString("DS_RMUESTRA"));
    txtMuesAnal.setText(dtDev.getString("NM_MUESBROTE"));
    txtMuesPosit.setText(dtDev.getString("NM_POSITBROTE"));
    pnlAgente.setCodigo(dtDev);
    txtResultado.setText(dtDev.getString("DS_RESCUANTI"));
  }

  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return ("");
  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  //rellena dtDevuelto, para la pantalla padre
  void devolverCampos(String nmMues, String itOper, String nmMicrotox) {
    //it_oper=0(alta) ; =1(modificacion) ; =2(baja) ; =3(nada) ; =4(alta en modif)
    for (int i = 0; i < lAgente.size(); i++) {
      Data dtDevuelto = new Data();
      dtDevuelto.put("NM_MUESTRA", nmMues);
      dtDevuelto.put("CD_MUESTRA",
                     ( (Data) pnlOrigen.getDatos()).getString("CD_MUESTRA"));
      dtDevuelto.put("CD_ANO", dtDev.getString("CD_ANO"));
      dtDevuelto.put("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
      dtDevuelto.put("DS_RMUESTRA", txtMuestra.getText().trim());
      dtDevuelto.put("NM_MUESBROTE", txtMuesAnal.getText().trim());
      dtDevuelto.put("NM_POSITBROTE", txtMuesPosit.getText().trim());
      dtDevuelto.put("DS_MUESTRA",
                     ( (Data) pnlOrigen.getDatos()).getString("DS_MUESTRA"));
      dtDevuelto.put("CD_GRUPO", dtDev.getString("CD_GRUPO"));
      Data dtAg = (Data) lAgente.elementAt(i);
      dtDevuelto.put("CD_AGENTET", dtAg.getString("CD_AGENTET"));
      dtDevuelto.put("DS_AGENTET", dtAg.getString("DS_AGENTET"));
      dtDevuelto.put("DS_RESCUANTI", dtAg.getString("DS_RESCUANTI"));
      dtDevuelto.put("IT_BD", dtDev.getString("IT_BD"));
      if (itOper.equals("1")) {
        if (i == 0) {
          dtDevuelto.put("IT_OPERACION", itOper);
        }
        else {
          dtDevuelto.put("IT_OPERACION", "4");
        }
      }
      else {
        dtDevuelto.put("IT_OPERACION", itOper);
      }
      dtDevuelto.put("NM_MICROTOX", nmMicrotox);
      dtDevuelto.put("ELIMINAR", dtDev.getString("ELIMINAR"));

      if (itOper.equals("0")) {
        String prim = Integer.toString(i);
        dtDevuelto.put("IT_POSICION", prim);
      }
      lDevuelto.addElement(dtDevuelto);
    }
    //recorrer la lista lAgente y meterla en lDevuelto
  }

  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        String nmMuestra = new String();
        String nmMicrotox = new String();
        String itOperacion = new String();
        switch (modoOperacion) {
          case 0:
            String m = txtMuestra.getText().trim();
            String mP = txtMuesPosit.getText().trim();
            String mA = txtMuesAnal.getText().trim();
            String or = ( (Data) pnlOrigen.getDatos()).getString("CD_MUESTRA");
            nmMuestra = "" + m + "" + mP + "" + mA + "" + or;
            itOperacion = "0";
            nmMicrotox = "0000000";

            break;
          case 1:
            nmMuestra = dtDev.getString("NM_MUESTRA");
            nmMicrotox = dtDev.getString("NM_MICROTOX");
            if (dtDev.getString("IT_OPERACION").equals("4")) {
              itOperacion = "4";
            }
            else {
              itOperacion = "1";
            }

//            introducirAgente();
            break;
          case 2:
            nmMuestra = dtDev.getString("NM_MUESTRA");
            nmMicrotox = dtDev.getString("NM_MICROTOX");
            if (dtDev.getString("IT_OPERACION").equals("4")) {
              itOperacion = "4";
            }
            else {
              itOperacion = "2";
            }
            introducirAgente();
            break;
        } //end switch
        devolverCampos(nmMuestra, itOperacion, nmMicrotox);
        bAceptar = true;
        dispose();
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  public boolean bAceptar() {
    return bAceptar;
  }

  public Lista blDevuelto() {
    return lDevuelto;
  }

  public void verDatos() {
    switch (modoOperacion) {
      case ALTA:
        btnNuevoAg.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        break;

      case MODIFICACION:

        //doy por supuesto q puedes rellenar todos los campos
        pnlOrigen.setEnabled(false);
        btnNuevoAg.setLabel("Modif.Agente");
        rellenarDatos();
        break;

      case BAJA:
      case CONSULTA:

        //solo habilitados los botones de aceptar y cancelar
        btnNuevoAg.setEnabled(false);
        rellenarDatos();
        this.setEnabled(false);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        break;
    }

  }

  void introducirAgente() {
    Data dtAgente = new Data();
    dtAgente.put("CD_AGENTET",
                 ( (Data) pnlAgente.getDatos()).getString("CD_AGENTET"));
    dtAgente.put("DS_AGENTET",
                 ( (Data) pnlAgente.getDatos()).getString("DS_AGENTET"));
    dtAgente.put("DS_RESCUANTI", txtResultado.getText().trim());
    lAgente.addElement(dtAgente);
  }

  void btnNuevoAg_actionPerformed(ActionEvent e) {
    if (btnNuevoAg.getLabel().equals("Modif.Agente")) {
      introducirAgente();
      pnlAgente.limpiarDatos();
      txtResultado.setText("");
      agIntroducido = true;
      btnNuevoAg.setLabel("A�adir Agente");
    }
    else {
//    if ((pnlAgente.getDatos()==null) || (txtResultado.getText().trim().equals(""))){
//modificaci�n: resultado cuantitativo es opcional:
      if (pnlAgente.getDatos() == null) {
        this.getApp().showAdvise("El campo de agente t�xico es obligatorio");
      }
      else {
        introducirAgente();
        agIntroducido = true;
        pnlAgente.limpiarDatos();
        txtResultado.setText("");
      }
    }
  }

  void btnLupa_actionPerformed(ActionEvent e) {
    CListaValores clv = null;
    Vector vCod = null;
    QueryTool qt = null;

//    this.modoOperacion = CInicializar.ESPERA;
    Inicializar(CInicializar.ESPERA);

    QueryTool qtMues = new QueryTool();
    qtMues.putName("SIVE_MUESTRAS_BROTE");
    qtMues.putType("NM_MUESTRA", QueryTool.INTEGER);
    qtMues.putType("DS_RMUESTRA", QueryTool.STRING);

    // campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement("NM_MUESTRA");
    vCod.addElement("DS_RMUESTRA");

    // Para evitar que el filtro de la lupa permanezca
    qt = (QueryTool) qtMues.clone();

    // lista de valores
    clv = new CListaValores(this.getApp(),
                            "Muestras previamente introducidas",
                            qt,
                            vCod);
    clv.show();

    // recupera el centro seleccionado
    if (clv.getSelected() != null) {
      txtMuestra.setText( (clv.getSelected()).getString("DS_RMUESTRA"));
    }

    clv = null;

//    this.modoOperacion = CInicializar.NORMAL;
    Inicializar(CInicializar.NORMAL);

  }
}

class DialogActionAdapter1
    implements java.awt.event.ActionListener, Runnable {
  DiaMantMuestras adaptee;
  ActionEvent e;

  DialogActionAdapter1(DiaMantMuestras adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (e.getActionCommand().equals("Aceptar")) {
        adaptee.btn_actionPerformed(e);
        //adaptee.btnAreaActionPerformed();
      }
      if (e.getActionCommand().equals("Cancelar")) {
        adaptee.btn_actionPerformed(e);
      }
      s.desbloquea(adaptee);
    }
  }
}

class DiaMantMuestras_btnNuevoAg_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantMuestras adaptee;
  ActionEvent e;

  DiaMantMuestras_btnNuevoAg_actionAdapter(DiaMantMuestras adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (e.getActionCommand().equals("Nuevo Agente")) {
        adaptee.btnNuevoAg_actionPerformed(e);
        //adaptee.btnAreaActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
}

class DiaMantMuestras_btnLupa_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantMuestras adaptee;
  ActionEvent e;

  DiaMantMuestras_btnLupa_actionAdapter(DiaMantMuestras adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (e.getActionCommand().equals("Lupa")) {
        adaptee.btnLupa_actionPerformed(e);
      }
      s.desbloquea(adaptee);
    }
  }
}
