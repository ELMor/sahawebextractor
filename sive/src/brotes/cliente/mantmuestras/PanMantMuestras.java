//Title:        Mantenimiento de las muestras del brote
//Version:
//Copyright:    Copyright (c) 1998
//Author:       MTR
//Company:
//Description:

package brotes.cliente.mantmuestras;

import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CEntero;
import capp2.CPanel;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class PanMantMuestras
    extends CPanel {
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();

  Label lblAno = new Label();
  Label lblAlerbro = new Label();
  Label lblGrupo = new Label();

  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  CEntero txtAlerbro = new CEntero(7);
  CCodigo txtAno = new CCodigo(4);
  CCodigo txtGrupo = new CCodigo(1);

  ButtonControl btnBuscar = new ButtonControl();

  public PanMantMuestras(CApp a) {
    setApp(a);
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgBuscar = "images/refrescar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgBuscar);
    this.getApp().getLibImagenes().CargaImagenes();

    this.setLayout(xyLayout);
    this.setSize(300, 200);

    btnBuscar.setActionCommand("Buscar");
    btnBuscar.setLabel("Buscar");
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBuscar));

    // A�adimos los escuchadores...

    btnBuscar.addActionListener(new PanMantMuestras_btnBuscar_actionAdapter(this));

    lblAno.setText("Cd_Ano:");
    lblAlerbro.setText("Nm_Alerbro:");
    lblGrupo.setText("Grupo:");

    txtAno.setText("2000");
    txtAlerbro.setText("53");
    txtGrupo.setText("0");

    xyLayout.setHeight(450);
    xyLayout.setWidth(450);

    this.add(lblAno, new XYConstraints(MARGENIZQ, MARGENSUP, 70, ALTO));
    this.add(txtAno,
             new XYConstraints(MARGENIZQ + 70 + INTERVERT, MARGENSUP, 70, ALTO));
    this.add(lblAlerbro,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERHOR, 70, ALTO));
    this.add(txtAlerbro,
             new XYConstraints(MARGENIZQ + 70 + INTERVERT,
                               MARGENSUP + ALTO + INTERHOR, 70, ALTO));
    this.add(lblGrupo,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERHOR,
                               70, ALTO));
    this.add(txtGrupo,
             new XYConstraints(MARGENIZQ + 70 + INTERVERT,
                               MARGENSUP + 2 * ALTO + 2 * INTERHOR, 70, ALTO));
    this.add(btnBuscar,
             new XYConstraints(MARGENIZQ, MARGENSUP + 3 * ALTO + 3 * INTERHOR,
                               88, 29));
  }

  public void Inicializar() {
  }

  void btnBuscar_actionPerformed(ActionEvent e) {
    int modoOp = 1;
    Data dtMuestras = new Data();

    // query tool
    QueryTool qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", "1997");
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", "2");
    qtBloqueo.putOperator("NM_ALERBRO", "=");
    Lista l = new Lista();
    l.addElement(qtBloqueo);

    Lista vResultado = null;
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      vResultado = (Lista)this.getApp().getStub().doPost(1, l);
    }
    catch (Exception ex) {}
    ;

    Data miData = (Data) vResultado.elementAt(0);

    dtMuestras.put("CD_ANO", txtAno.getText().trim());
    dtMuestras.put("NM_ALERBRO", txtAlerbro.getText().trim());
//    dtMuestras.put("CD_ANO","1997");
//    dtMuestras.put("NM_ALERBRO","1");
    dtMuestras.put("CD_GRUPO", txtGrupo.getText().trim());
    dtMuestras.put("CD_OPE", miData.getString("CD_OPE"));
    dtMuestras.put("FC_ULTACT", miData.getString("FC_ULTACT"));
    DiaResumMuestras di = new DiaResumMuestras(this.getApp(), modoOp,
                                               dtMuestras);
    di.show();
  }
}

class PanMantMuestras_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener {
  PanMantMuestras adaptee;

  PanMantMuestras_btnBuscar_actionAdapter(PanMantMuestras adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnBuscar_actionPerformed(e);
  }
}
