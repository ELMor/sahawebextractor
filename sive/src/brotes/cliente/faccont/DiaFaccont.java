package brotes.cliente.faccont;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CPnlCodigoExt;
import brotes.cliente.c_componentes.ContCPnlCodigoExt;
import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CTextArea;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaFaccont
    extends CDialog
    implements CInicializar, ContCPnlCodigoExt {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 20;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  //acciones de botones
  DiaFaccontActionAdapter actionAdapter = new DiaFaccontActionAdapter(this);
  //Cambio en los campos de opci�n (choice)
  DiaFaccontItemListener itemListener = new DiaFaccontItemListener(this);

  //elementos de la pantalla
  Label lblFactor = new Label();
  Label lblInfAdic = new Label();
  Label lblObservaciones = new Label();
  Label lblPrincipal = new Label();
  CPnlCodigoExt pnlFactor = null;
  CTextArea textAreaObserv = new CTextArea(500);
  CTextArea textAreaInfAdicional = new CTextArea(30);
  Checkbox chInfAdic = new Checkbox();
  Checkbox chPrincipal = new Checkbox();
  // Botones de aceptar y cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // modo de operaci�n
  public int modoOperacion = 0;
  public int modoBloqueo = modoNORMAL;

  //imagenes a insertar en la tabla
  final String imgCANCELAR = "images/cancelar.gif";
  final String imgACEPTAR = "images/aceptar.gif";

  public Data dtEntra = null;
  public Data dtDevuelto = null;
  public Lista lEntra = null;
  public boolean bAceptar = false;
  public Data dtDatFact = new Data();
  XYLayout xyLayout = new XYLayout();

  final String servlet = "servlet/SrvQueryTool";

  public DiaFaccont(CApp a, int modo, Data dataBusq) {
    super(a);
    modoOperacion = modo;
    dtEntra = dataBusq;
    setTitle("Brotes: Factores Contribuyentes");
    try {
      QueryTool qtFactContr = new QueryTool();
      qtFactContr.putName("SIVE_FACT_CONTRIB");
      qtFactContr.putType("CD_FACCONT", QueryTool.STRING);
      qtFactContr.putType("DS_FACCON", QueryTool.STRING);
      qtFactContr.putType("IT_MASFACCONT", QueryTool.STRING);
      qtFactContr.putType("IT_REPE", QueryTool.STRING);

      qtFactContr.putWhereType("CD_GRUPO", QueryTool.STRING);
      qtFactContr.putWhereValue("CD_GRUPO", dtEntra.getString("CD_GRUPO"));
      qtFactContr.putOperator("CD_GRUPO", "=");
      //panel de punto centinela
      pnlFactor = new CPnlCodigoExt(a,
                                    this,
                                    qtFactContr,
                                    "CD_FACCONT",
                                    "DS_FACCON",
                                    true,
                                    "Factores contribuyentes",
                                    "Factores contribuyentes",
                                    this);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  public DiaFaccont(CApp a, int modo, Data dataBusq, Lista lFactores) {
    super(a);
    modoOperacion = modo;
    dtEntra = dataBusq;
    lEntra = lFactores;
    setTitle("Brotes: Factores Contribuyentes");
    try {
      QueryTool qtFactContr = new QueryTool();
      qtFactContr.putName("SIVE_FACT_CONTRIB");
      qtFactContr.putType("CD_FACCONT", QueryTool.STRING);
      qtFactContr.putType("DS_FACCON", QueryTool.STRING);
      qtFactContr.putType("IT_MASFACCONT", QueryTool.STRING);
      qtFactContr.putType("IT_REPE", QueryTool.STRING);

      qtFactContr.putWhereType("CD_GRUPO", QueryTool.STRING);
      qtFactContr.putWhereValue("CD_GRUPO", dtEntra.getString("CD_GRUPO"));
      qtFactContr.putOperator("CD_GRUPO", "=");
      //panel de punto centinela
      pnlFactor = new CPnlCodigoExt(a,
                                    this,
                                    qtFactContr,
                                    "CD_FACCONT",
                                    "DS_FACCON",
                                    true,
                                    "Factores contribuyentes",
                                    "Factores contribuyentes",
                                    this);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {

    // carga la imagen
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));
    btnCancelar.addActionListener(actionAdapter);
    chInfAdic.addItemListener(itemListener);
    chInfAdic.setName("Informacion");

    this.setSize(450, 300);
    xyLayout.setHeight(300);
    xyLayout.setWidth(450);
    this.setLayout(xyLayout);

    lblFactor.setText("Factor:");
    lblInfAdic.setText("Informaci�n Adicional:");
    lblObservaciones.setText("Observaciones:");
    lblPrincipal.setText("Principal:");

    // a�ade los componentes
    this.add(lblFactor, new XYConstraints(MARGENIZQ, MARGENSUP, 70, ALTO));
    this.add(pnlFactor,
             new XYConstraints(MARGENIZQ + 70 + INTERHOR - DESFPANEL, MARGENSUP,
                               TAMPANEL, 34));

    this.add(lblInfAdic,
             new XYConstraints(MARGENIZQ, MARGENSUP + 34 + INTERVERT, 125, ALTO));
    this.add(chInfAdic,
             new XYConstraints(MARGENIZQ + 125 + INTERHOR,
                               MARGENSUP + 34 + INTERVERT - 5, 30, 34));
    this.add(textAreaInfAdicional,
             new XYConstraints(MARGENIZQ + 125 + 2 * INTERHOR + 30,
                               MARGENSUP + 34 + INTERVERT,
                               280 - INTERHOR - 25 - 30, 55));

    this.add(lblObservaciones,
             new XYConstraints(MARGENIZQ, MARGENSUP + 34 + 2 * INTERVERT + 55,
                               100, ALTO));
    this.add(textAreaObserv,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR,
                               MARGENSUP + 34 + 2 * INTERVERT + 55, 280, 55));

    this.add(lblPrincipal,
             new XYConstraints(MARGENIZQ,
                               MARGENSUP + 34 + 3 * INTERVERT + 2 * 55, 65,
                               ALTO));
    this.add(chPrincipal,
             new XYConstraints(MARGENIZQ + 65 + INTERHOR,
                               MARGENSUP + 34 + 3 * INTERVERT + 2 * 55 - 5, 30,
                               34));

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR + 280 - 2 * 80 -
                               INTERVERT,
                               MARGENSUP + 34 + 4 * INTERVERT + 2 * 55 + 34, 80,
                               30));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR + 280 - 80,
                               MARGENSUP + 34 + 4 * INTERVERT + 2 * 55 + 34, 80,
                               30));

    switch (modoOperacion) {
      case ALTA:
        chInfAdic.setEnabled(false);
        textAreaInfAdicional.setVisible(false);
        break;
      case MODIFICACION:
        pnlFactor.setEnabled(false);
        rellenarDatos(dtEntra);
        break;
      case BAJA:
      case CONSULTA:
        rellenarDatos(dtEntra);
        break;
    }
    Inicializar(CInicializar.NORMAL);
  } //end jbinit

  public void Inicializar() {}

  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if ( (modoOperacion == BAJA) || (modoOperacion == CONSULTA)) {
          this.setEnabled(false);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
        }
        else {
          if (modoOperacion == MODIFICACION) {
            this.setEnabled(true);
            pnlFactor.setEnabled(false);
            if (dtEntra.getString("IT_MASFACCONT").equals("S")) {
              chInfAdic.setEnabled(true);
              if ( (dtEntra.getString("DS_MASFACCON").length() != 0) ||
                  (chInfAdic.getState())) {
                textAreaInfAdicional.setEnabled(true);
              }
            }
            else {
              chInfAdic.setEnabled(false);
            }
          }
          else {
            this.setEnabled(true);
            if ( (dtDatFact != null) &&
                (dtDatFact.getString("IT_MASFACCONT").equals("S"))) {
              chInfAdic.setEnabled(true);
            }
            else {
              chInfAdic.setEnabled(false);
            } //end
          } //end
        } //end
        break;
    } //end switch
  }

  public void rellenarDatos(Data dt) {
    if (modoOperacion == ALTA) {
      textAreaInfAdicional.setVisible(false);
    }
    else {
      Data dtPanel = new Data();
      dtPanel.put("CD_FACCONT", dt.getString("CD_FACCONT"));
      dtPanel.put("DS_FACCON", dt.getString("DS_FACCON"));
      pnlFactor.setCodigo(dtPanel);
      pnlFactor.setEnabled(false);

      if (dt.getString("IT_MASFACCONT").equals("S")) {
        chInfAdic.setEnabled(true);
        if (dt.getString("DS_MASFACCON").length() != 0) {
          chInfAdic.setState(true);
          textAreaInfAdicional.setVisible(true);
          textAreaInfAdicional.setText(dt.getString("DS_MASFACCON"));
        }
        else {
          textAreaInfAdicional.setVisible(false);
        }
      }
      else {
        chInfAdic.setEnabled(false);
        chInfAdic.setState(false);
        textAreaInfAdicional.setVisible(false);
      }

      textAreaObserv.setText(dt.getString("DS_OBSERV"));
      if (dt.getString("IT_PRINCIPAL").equals("S")) {
        chPrincipal.setState(true);
      }
      else {
        chPrincipal.setEnabled(false);
      }
    } //end else=false
  }

  public Data recogerDatos() {
    Data dtRecoger = new Data();
    Data dtDatosFactor = pnlFactor.getDatos();
    if (dtDatosFactor != null) {
      dtRecoger.put("CD_FACCONT", dtDatosFactor.getString("CD_FACCONT"));
      dtRecoger.put("DS_FACCON", dtDatosFactor.getString("DS_FACCON"));
    }
    if (chInfAdic.getState()) {
      dtRecoger.put("DS_MASFACCON", textAreaInfAdicional.getText().trim());
    }
    dtRecoger.put("DS_OBSERV", textAreaObserv.getText().trim());
    if (chPrincipal.getState()) {
      dtRecoger.put("IT_PRINCIPAL", "S");
    }
    else {
      dtRecoger.put("IT_PRINCIPAL", "N");
    }
    dtRecoger.put("CD_GRUPO", dtEntra.getString("CD_GRUPO"));
    dtRecoger.put("CD_ANO", dtEntra.getString("CD_ANO"));
    dtRecoger.put("NM_ALERBRO", dtEntra.getString("NM_ALERBRO"));
    switch (modoOperacion) {
      case ALTA:
        dtRecoger.put("TIPO_OPERACION", "A");
        dtRecoger.put("IT_MASFACCONT", dtDatFact.getString("IT_MASFACCONT"));
        break;
      case MODIFICACION:
        if (dtEntra.getString("TIPO_OPERACION").equals("A")) {
          dtRecoger.put("TIPO_OPERACION", "A");
        }
        else { //si es modif de BD o de otra modificacion
          dtRecoger.put("TIPO_OPERACION", "M");
          dtRecoger.put("NM_FACB", dtEntra.getString("NM_FACB"));
        }
        dtRecoger.put("IT_MASFACCONT", dtEntra.getString("IT_MASFACCONT"));
        break;
      case BAJA:
        if (dtEntra.getString("TIPO_OPERACION").equals("A")) {
          dtRecoger.put("TIPO_OPERACION", "E"); //Eliminar
        }
        else { //si es dar de baja una de bd o una modificada
          dtRecoger.put("TIPO_OPERACION", "B");
        }
        break;
    } //end switch
    return dtRecoger;
  }

  public boolean validarDatos() {
    boolean b = false;
    Data dtFac = pnlFactor.getDatos();
    if (dtFac == null) {
      this.getApp().showAdvise("El campo Factor es obligatorio");
      b = false;
    }
    else {
      b = true;
    }
    return b;
  }

  /********************Interface ContCPnlCodigoExt********************/
  //busca los datos del factor elegido
  public void trasEventoEnCPnlCodigoExt() {
//      Inicializar(CInicializar.NORMAL);
    dtDatFact = pnlFactor.getDatos();
    chInfAdic.setState(false);
    textAreaInfAdicional.setVisible(false);
    textAreaInfAdicional.setText("");
    if ( (dtDatFact != null) &&
        (dtDatFact.getString("IT_MASFACCONT").equals("S"))) {
      chInfAdic.setEnabled(true);
    }
    else {
      chInfAdic.setEnabled(false);
    }
    //    if (
//    QueryTool qt=new QueryTool();

  }

  public String getDescCompleta(Data dat) {
    String s = new String();
    return s;
  }

  /********************************************************************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  boolean comprobarNoMas3Principal() {
    boolean b = true;
    int cont = 0;
    if (chPrincipal.getState()) {
      for (int i = 0; i < lEntra.size(); i++) {
        String itPrincipal = ( (Data) lEntra.elementAt(i)).getString(
            "IT_PRINCIPAL");
        if (itPrincipal.equals("S")) {
          cont = cont + 1;
        }
      } //end for
      // Comprobamos que se repiten s�lo en el caso:
      // * De que estemos en alta.
      // * Y que haya cambiado el IT_ principal de N a S (modificaci�n).

      if ( (modoOperacion == 0) ||
          (dtEntra.getString("IT_PRINCIPAL").equals("N")) &&
          (chPrincipal.getState())) {
        if (cont > 2) {
          b = false;
          this.getApp().showAdvise(
              "No se admiten m�s de 3 factores principales por brote");
          chPrincipal.setState(false);
        }
      }
    } //end if
    return b;
  } //end comprobarNomas...

  void btnAceptarActionPerformed() {

    //controlar it_repe con lEntra
    boolean salir = false;
    boolean comprobarRepes = false;
    boolean hacerAlta = true;
    Data dtRepe = pnlFactor.getDatos();
    String sCdFac = dtRepe.getString("CD_FACCONT");
    // ponemos hacerAlta a 'v' o 'f' , seg�n estemos
    // en modo alta o no.
    if (modoOperacion == 2) {
      comprobarRepes = false;
    }
    else {
      comprobarRepes = true;

      //Si no se puede repetir comprobamos si existia antes en la lista
    }
    if (dtRepe.getString("IT_REPE").equals("N")) {
      for (int i = 0; i < lEntra.size() && salir == false; i++) {
        String sEnt = ( (Data) lEntra.elementAt(i)).getString("CD_FACCONT");
        if (sCdFac.equals(sEnt)) {
          salir = true;
        }
      } //end for
      //si se ha encontrado uno igual
      if (salir == true) {
        this.getApp().showAdvise(
            "No se admiten factores repetidos con ese c�digo");
        pnlFactor.limpiarDatos();
        hacerAlta = false;
      }
    } //end if comprobar si repetidos.
    //comprobar q no hay m�s de 3 factores principales por brote
    if (comprobarRepes) {
      hacerAlta = comprobarNoMas3Principal();
    }
    //si se puede hacer el alta
    if (hacerAlta) {
      //Validamos los datos
      if (validarDatos()) {
        dtDevuelto = recogerDatos();
        bAceptar = true;
        dispose();
      }
    } //end if hacerAlta
  }

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  public Data dtDevolver() {
    return dtDevuelto;
  }

  void btnCancelarActionPerformed() {
    bAceptar = false;
    dispose();
  }

  void chInfAdicItemStateChanged() {
    if (chInfAdic.getState()) {
      textAreaInfAdicional.setVisible(true);
    }
    else {
      textAreaInfAdicional.setText("");
      textAreaInfAdicional.setVisible(false);
    }
//    Inicializar(CInicializar.NORMAL);
    this.doLayout();
  }

} //end class

class DiaFaccontActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaFaccont adaptee;
  ActionEvent evt;

  DiaFaccontActionAdapter(DiaFaccont adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
        //adaptee.btnAreaActionPerformed();
      }
      if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaFaccontActionAdapter

class DiaFaccontItemListener
    implements java.awt.event.ItemListener, Runnable {
  DiaFaccont adaptee;
  ItemEvent evt;

  DiaFaccontItemListener(DiaFaccont adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if ( ( (Checkbox) evt.getSource()).getName().equals("Informacion")) {
        adaptee.chInfAdicItemStateChanged();
      }
      s.desbloquea(adaptee);
    }
  }
}
