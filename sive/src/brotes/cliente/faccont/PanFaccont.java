package brotes.cliente.faccont;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CColumnaImagen;
import brotes.cliente.c_componentes.CListaMantConImagenes;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CBoton;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import comun.constantes;
import comun.Common;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

//import brotes.servidor.faccont.*;

public class PanFaccont
    extends CDialog
    implements CInicializar, CFiltro {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 20;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Botones
  FaccontActionAdapter actionAdapter = new FaccontActionAdapter(this);

  // modo de operaci�n
  public int modoOperacion = 0;
  public int modoBloqueo = modoNORMAL;

  public Data dtDev = null;
  public Lista lClm = new Lista();
  public Lista lClmBaja = new Lista();
  public Lista lBd = new Lista();

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  //imagenes a insertar en la tabla
  final String imgCANCELAR = "images/cancelar.gif";
  final String imgACEPTAR = "images/aceptar.gif";

  final String servletFaccont = "servlet/SrvFaccont";

  XYLayout xyLayout = new XYLayout();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  CListaMantConImagenes clmFaccont = null;

  Label lblBrote = new Label();
  Label lblDatosBrote = new Label();

  public PanFaccont(CApp a, int modo, Data dataBusq) {
    super(a);
    modoOperacion = modo;
    dtDev = dataBusq;
    setTitle("Brotes: Factores Contribuyentes");
    try {
      //Configuro la CListaMantenimento
      Vector vBotones = new Vector();
      Vector vLabels = new Vector();
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nuevo factor contribuyente",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar factor contribuyente",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "Borrar factor contribuyente",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumnaImagen("Descripci�n Factor",
                                            "265",
                                            "DS_FACCON",
                                            false));

      vLabels.addElement(new CColumnaImagen("Observaciones",
                                            "235",
                                            "DS_MASFACCON",
                                            false));

      vLabels.addElement(new CColumnaImagen("Principal",
                                            "100",
                                            "PRINCIPAL",
                                            true));

      clmFaccont = new CListaMantConImagenes(a,
                                             vLabels,
                                             vBotones,
                                             this,
                                             this,
                                             250,
                                             640,
                                             22);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {

    // carga la imagen
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));
    btnCancelar.addActionListener(actionAdapter);

    this.setSize(680, 385);
    xyLayout.setHeight(385);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    lblBrote.setText("Brote:");
    lblDatosBrote.setText(dtDev.getString("CD_ANO") + "/" +
                          dtDev.getString("NM_ALERBRO") + " - " +
                          dtDev.getString("DS_BROTE"));

    // a�ade los componentes
    this.add(lblBrote, new XYConstraints(MARGENIZQ, MARGENSUP, 80, ALTO));
    this.add(lblDatosBrote,
             new XYConstraints(MARGENIZQ + 80 + INTERHOR, MARGENSUP, 300, ALTO));
    this.add(clmFaccont,
             new XYConstraints(MARGENIZQ - 10, MARGENSUP + ALTO + 2 * INTERVERT,
                               650, 230));
    this.add(btnAceptar,
             new XYConstraints(535 - 80 - INTERVERT,
                               MARGENSUP + ALTO + 4 * INTERVERT + 230, 80, 30));
    this.add(btnCancelar,
             new XYConstraints(535, MARGENSUP + ALTO + 4 * INTERVERT + 230, 80,
                               30));

    //Se a�aden a la lista los factores ya existentes.
    Inicializar(CInicializar.ESPERA);
    clmFaccont.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);

  } //end jbinit

  public void Inicializar() {}

  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if ( (modoOperacion == BAJA) || (modoOperacion == CONSULTA)) {
          this.setEnabled(false);
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
          clmFaccont.setEnabled(false);
        }
        else {
          clmFaccont.setEnabled(true);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
        }
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";

    try {
      Data fila = null;
      fila = new Data();
      QueryTool2 qt = new QueryTool2();
      qt.putName("SIVE_BROTES_FACCONT");

      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("CD_GRUPO", QueryTool.STRING);
      qt.putType("CD_FACCONT", QueryTool.STRING);
      qt.putType("NM_FACB", QueryTool.INTEGER);
      qt.putType("DS_MASFACCON", QueryTool.STRING);
      qt.putType("DS_OBSERV", QueryTool.STRING);
      qt.putType("IT_PRINCIPAL", QueryTool.STRING);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic1 = new QueryTool();
      qtAdic1.putName("SIVE_FACT_CONTRIB");

      /*      sQuery =  "SELECT fb.CD_ANO, fb.NM_ALERBRO, fb.CD_GRUPO, "+
           "fb.CD_FACCONT, f.DS_FACCON, f.IT_MASFACCONT, f.IT_REPE, "+
           "fb.NM_FACB, fb.DS_MASFACCON, fb.DS_OBSERV, fb.IT_PRINCIPAL "+
                      "FROM SIVE_FACT_CONTRIB f, SIVE_BROTES_FACCONT fb "+
                      "WHERE fb.CD_FACCONT = f.CD_FACCONT "+
                        "AND fb.CD_GRUPO = f.CD_GRUPO "+
                        "AND fb.CD_ANO = ? "+
                        "AND fb.NM_ALERBRO = ? ";
       */
      qtAdic1.putType("DS_FACCON", QueryTool.STRING);
      qtAdic1.putType("IT_MASFACCONT", QueryTool.STRING);
      qtAdic1.putType("IT_REPE", QueryTool.STRING);

      Data dtAdic1 = new Data();
      dtAdic1.put("CD_GRUPO", QueryTool.STRING);
      dtAdic1.put("CD_FACCONT", QueryTool.STRING);
      qt.addQueryTool(qtAdic1);
      qt.addColumnsQueryTool(dtAdic1);

      qt.addOrderField("NM_FACB");

      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() == 0) {
//            this.getApp().showAdvise("No hay datos con esta entrada");
      }
      else {
        p1 = anadirTipoOperacion(p1);
        p1 = anadirPrincipal(p1);
        lClm = p1;
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();

    }
    return p1;
  } //end primerapagina

  //Tipo Operacion=BD
  Lista anadirTipoOperacion(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      ( (Data) lCampo.elementAt(i)).put("TIPO_OPERACION", "BD");
    } //end for
    return lCampo;
  } //end anadirPrincipal

  //concatena a�o y c�digo de brote
  Lista anadirPrincipal(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      if (dCampo.getString("IT_PRINCIPAL").equals("S")) {
        ( (Data) lCampo.elementAt(i)).put("PRINCIPAL",
                                          this.getApp().
                                          getLibImagenes().get(imgACEPTAR));
      }
      else {
        ( (Data) lCampo.elementAt(i)).put("PRINCIPAL",
                                          this.getApp().
                                          getLibImagenes().get(imgCANCELAR));
      }
    } //end for

    return lCampo;
  } //end anadirPrincipal

  public Lista siguientePagina() {
    Lista l = new Lista();
    return l;
  }

  public void realizaOperacion(int j) {
    DiaFaccont di = null;
    Data dtFaccont = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        dtFaccont.put("CD_GRUPO", dtDev.getString("CD_GRUPO"));
        dtFaccont.put("CD_ANO", dtDev.getString("CD_ANO"));
        dtFaccont.put("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
//        di = new DiaFaccont(this.getApp(), ALTA, dtFaccont);
        di = new DiaFaccont(this.getApp(), ALTA, dtFaccont, lClm);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar) {
          Data dtDevuelto = di.dtDevolver();
          lClm.addElement(dtDevuelto);
          lClm = anadirPrincipal(lClm);
          clmFaccont.setPrimeraPagina(lClm);
          //clmFaccont.setPrimeraPagina(primeraPagina());
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dtFaccont = clmFaccont.getSelected();
        //si existe alguna fila seleccionada
        if (dtFaccont != null) {
          int ind = clmFaccont.getSelectedIndex();
          di = new DiaFaccont(this.getApp(), MODIFICACION, dtFaccont, lClm);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar) {
            Data dtDevuelto = di.dtDevolver();
            lClm.removeElementAt(ind);
            lClm.addElement(dtDevuelto);
            lClm = anadirPrincipal(lClm);
            clmFaccont.setPrimeraPagina(lClm);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dtFaccont = clmFaccont.getSelected();
        if (dtFaccont != null) {
          int ind = clmFaccont.getSelectedIndex();
          di = new DiaFaccont(this.getApp(), BAJA, dtFaccont);
          di.show();
          if (di.bAceptar) {
            Data dtDevuelto = di.dtDevolver();
            lClm.removeElementAt(ind);
            if (dtDevuelto.getString("TIPO_OPERACION").equals("B")) {
              lClmBaja.addElement(dtFaccont);
            }
            clmFaccont.setPrimeraPagina(lClm);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }
  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  //funci�n q genera la query y el data necesarios para el bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtDev.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtDev.getString("FC_ULTACT"));
  } //end prepara bloqueo

  void btnAceptarActionPerformed() {
    Inicializar(CInicializar.ESPERA);
    Lista vResultado = new Lista();
    //recorro la lista q tengo para altas y modificaciones
    for (int i = 0; i < lClm.size(); i++) {
      Data dtClm = (Data) lClm.elementAt(i);

      //Se realiza un alta
      if (dtClm.getString("TIPO_OPERACION").equals("A")) {
        QueryTool qtIns = new QueryTool();
        qtIns = realizarInsert(dtClm);
        Data dtInsert = new Data();
        dtInsert.put("10003", qtIns);
        lBd.addElement(dtInsert);
      } //end if ALTA

      if (dtClm.getString("TIPO_OPERACION").equals("M")) {
        QueryTool qtUpd = new QueryTool();
        qtUpd = realizarUpdate(dtClm);
        Data dtUpdate = new Data();
        dtUpdate.put("10006", qtUpd);
        lBd.addElement(dtUpdate);
      }
    } //end for
    //lista para bajas
    for (int i = 0; i < lClmBaja.size(); i++) {
      Data dtClmBaja = (Data) lClmBaja.elementAt(i);
      QueryTool qtDel = new QueryTool();
      qtDel = realizarDelete(dtClmBaja);
      Data dtDel = new Data();
      dtDel.put("10007", qtDel);
      lBd.addElement(dtDel);
    } //end for lBajas

    //si se ha modificado algo:se inserta para ese brote el nuevo
    //cd_ope y fc_ultact
    if (lBd.size() != 0) {
      Data dtDatBrot = new Data();
      dtDatBrot.put("CD_OPE", dtDev.getString("CD_OPE"));
      dtDatBrot.put("FC_ULTACT", "");

      QueryTool qtUpdBro = new QueryTool();
      qtUpdBro = realizarUpdateBrote(dtDatBrot);
      Data dtUpdBro = new Data();
      dtUpdBro.put("10006", qtUpdBro);
      lBd.addElement(dtUpdBro);
//    }

      preparaBloqueo();
      try {
        //BDatos.ejecutaSQL(false,this.getApp(),servletFaccont,0,lBd);
        /*            SrvFaccont servlet=new SrvFaccont();
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                    vResultado = (Lista) servlet.doDebug(0,lBd);*/
//            vResultado = (Lista)this.getApp().getStub().doPost(0,lBd);

        this.getApp().getStub().setUrl(servletFaccont);
        vResultado = (Lista)this.getApp().getStub().doPost(10000,
            lBd,
            qtBloqueo,
            dtBloqueo,
            getApp());

        //obtener el nuevo cd_ope y fc_ultact de la base de datos.
        int tamano = vResultado.size();
        String fecha = null;
        for (int i = 0; i < vResultado.size(); i++) {
          fecha = null;
          fecha = ( (Lista) vResultado.elementAt(i)).getFC_ULTACT();
        }
        dtDev.put("CD_OPE", dtDev.getString("CD_OPE"));
        dtDev.put("FC_ULTACT", fecha);
        dispose();
      }
      catch (Exception ex) {
        if (Common.ShowPregunta(this.getApp(), "Los datos han sido modificados por otro usuario. �Sigue queriendo realizar esta actualizaci�n?")) {
          try {
            ModosOperacNormal();
            this.getApp().getStub().setUrl(servletFaccont);
            vResultado = (Lista)this.getApp().getStub().doPost(0, lBd);
            dispose();
          }
          catch (Exception exc) {
            this.getApp().trazaLog(exc);
            this.getApp().showError(ex.getMessage());
            dispose();
          } //end 2.catch
        }
        else {
          dispose();
        } //end if comun.showpregunta
      } //end 1. catch
    }
    else {
      dispose();
    } //end if se han realizado cambios
    Inicializar(CInicializar.NORMAL);
  } //end btnAceptar

  //sin el bloqueo
  public void ModosOperacNormal() {
    for (int i = 0; i < lClm.size(); i++) {
      Data dtClm = (Data) lClm.elementAt(i);

      //Se realiza un alta
      if (dtClm.getString("TIPO_OPERACION").equals("A")) {
        QueryTool qtIns = new QueryTool();
        qtIns = realizarInsert(dtClm);
        Data dtInsert = new Data();
        dtInsert.put("3", qtIns);
        lBd.addElement(dtInsert);
      } //end if ALTA
      if (dtClm.getString("TIPO_OPERACION").equals("M")) {
        QueryTool qtUpd = new QueryTool();
        qtUpd = realizarUpdate(dtClm);
        Data dtUpdate = new Data();
        dtUpdate.put("4", qtUpd);
        lBd.addElement(dtUpdate);
      }
    } //end for
    //lista para bajas
    for (int i = 0; i < lClmBaja.size(); i++) {
      Data dtClmBaja = (Data) lClmBaja.elementAt(i);
      QueryTool qtDel = new QueryTool();
      qtDel = realizarDelete(dtClmBaja);
      Data dtDel = new Data();
      dtDel.put("5", qtDel);
      lBd.addElement(dtDel);
    } //end for lBajas
  } //end modosOperacNormal

  QueryTool realizarDelete(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_BROTES_FACCONT");

    qtModif.putWhereType("NM_FACB", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_FACB", dtUpd.getString("NM_FACB"));
    qtModif.putOperator("NM_FACB", "=");
    return qtModif;
  }

  QueryTool realizarInsert(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    qtIns.putName("SIVE_BROTES_FACCONT");
    qtIns.putType("CD_ANO", QueryTool.INTEGER);
    qtIns.putValue("CD_ANO", dtUpd.getString("CD_ANO"));
    qtIns.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtIns.putValue("NM_ALERBRO", dtUpd.getString("NM_ALERBRO"));
    qtIns.putType("CD_GRUPO", QueryTool.STRING);
    qtIns.putValue("CD_GRUPO", dtUpd.getString("CD_GRUPO"));
    qtIns.putType("CD_FACCONT", QueryTool.STRING);
    qtIns.putValue("CD_FACCONT", dtUpd.getString("CD_FACCONT"));
    qtIns.putType("DS_MASFACCON", QueryTool.STRING);
    qtIns.putValue("DS_MASFACCON", dtUpd.getString("DS_MASFACCON"));
    qtIns.putType("DS_OBSERV", QueryTool.STRING);
    qtIns.putValue("DS_OBSERV", dtUpd.getString("DS_OBSERV"));
    qtIns.putType("IT_PRINCIPAL", QueryTool.STRING);
    qtIns.putValue("IT_PRINCIPAL", dtUpd.getString("IT_PRINCIPAL"));
    qtIns.putType("NM_FACB", QueryTool.STRING);
    qtIns.putValue("NM_FACB", "");

    return qtIns;
  }

  QueryTool realizarUpdate(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_BROTES_FACCONT");
    qtModif.putType("DS_MASFACCON", QueryTool.STRING);
    qtModif.putValue("DS_MASFACCON", dtUpd.getString("DS_MASFACCON"));
    qtModif.putType("DS_OBSERV", QueryTool.STRING);
    qtModif.putValue("DS_OBSERV", dtUpd.getString("DS_OBSERV"));
    qtModif.putType("IT_PRINCIPAL", QueryTool.STRING);
    qtModif.putValue("IT_PRINCIPAL", dtUpd.getString("IT_PRINCIPAL"));

    qtModif.putWhereType("NM_FACB", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_FACB", dtUpd.getString("NM_FACB"));
    qtModif.putOperator("NM_FACB", "=");

    return qtModif;
  }

  QueryTool realizarUpdateBrote(Data dtUpd) {
    QueryTool qtUpd = new QueryTool();
    qtUpd.putName("SIVE_BROTES");
    qtUpd.putType("CD_OPE", QueryTool.STRING);
    qtUpd.putValue("CD_OPE", dtUpd.getString("CD_OPE"));
    qtUpd.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtUpd.putValue("FC_ULTACT", dtUpd.getString("FC_ULTACT"));

    // filtro
    qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");

    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");

    return qtUpd;
  }

  void btnCancelarActionPerformed() {
    dispose();
  }

} //end class

class FaccontActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanFaccont adaptee;
  ActionEvent evt;

  FaccontActionAdapter(PanFaccont adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
        //adaptee.btnAreaActionPerformed();
      }
      if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  FaccontActionAdapter
