package brotes.cliente.alibrote;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CHora;
import brotes.cliente.c_componentes.CPnlCodigoExtAli;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CTexto;
import comun.constantes;
import comun.Common;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaAlibrote
    extends CDialog
    implements CInicializar {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 22;
  final int DESFPANEL = 5;
  final int TAMPANEL = 230;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  //acciones de botones
  DiaAlibroteActionAdapter actionAdapter = new DiaAlibroteActionAdapter(this);

  //elementos de la pantalla
  // Descripcion del alimento
  private Label lblDescAli = new Label("Alimento:");
  private CTexto txtDescAli = new CTexto(40);

  // Tipo de Ali
  private Label lblTAli = new Label("Tipo:");
  private CPnlCodigoExtAli txtTAli = null;

  // Tipo de Imp
  private Label lblTImp = new Label("Sospechoso/Confirmado:");
  private CPnlCodigoExtAli txtTImp = null;

  // Marca
  private Label lblMarca = new Label("Marca:");
  private CTexto txtMarca = new CTexto(30);

  // Fabricante
  private Label lblFab = new Label("Fabricante:");
  private CTexto txtFab = new CTexto(40);

  // Lote
  private Label lblLote = new Label("Lote:");
  private CTexto txtLote = new CTexto(15);

  // Metodo comercializacion
  private Label lblMCom = new Label("Metodo comercializacion:");
  private CPnlCodigoExtAli txtMCom = null;

  // Tratamiento previo a la preparacion final
  private Label lblTPrev = new Label("Trat. previo preparacion:");
  private CPnlCodigoExtAli txtTPrev = null;

  // Forma de servir o ingerir el alimento
  private Label lblFIng = new Label("Forma de servir/ingerir:");
  private CPnlCodigoExtAli txtFIng = null;

  // Lugar contaminaci�n alimento
  private Label lblLCont = new Label("Lugar contaminaci�n:");
  private CPnlCodigoExtAli txtLCont = null;

  // Pais contaminaci�n alimento
  private Label lblPCont = new Label("Pa�s:");
  private CPnlCodigoExtAli txtPCont = null;

  // Lugar donde se prepar�
  private Label lblLPrep = new Label("Lugar de preparaci�n:");
  private CPnlCodigoExtAli txtLPrep = null;

  // Pais donde se prepar�
  private Label lblPPrep = new Label("Pa�s:");
  private CPnlCodigoExtAli txtPPrep = null;

  // Nombre establecimiento
  private Label lblNEstPrep = new Label("Nombre establecimiento:");
  private CTexto txtNEstPrep = new CTexto(40);

  // Direcci�n establecimiento
  private Label lblDEstPrep = new Label("Direcci�n:");
  private CTexto txtDEstPrep = new CTexto(40);

  // CP
  private Label lblCPEstPrep = new Label("CP:");
  private CTexto txtCPEstPrep = new CTexto(5);

  // Municipio
  private Label lblMPrep = new Label("Municipio:");
  private CPnlCodigoExtAli txtMPrep = null;

  // Tel�fono
  private Label lblTEstPrep = new Label("Tel�fono:");
  private CTexto txtTEstPrep = new CTexto(14);

  // Fecha y hora de preparaci�n
  private Label lblFHPrep = new Label("Fecha y hora de preparaci�n:");
  private fechas.CFecha txtFechaPrep = new fechas.CFecha("N");
  private CHora txtHoraPrep = new CHora("N");

  // Lugar donde se consumi�
  private Label lblLCons = new Label("Lugar de consumici�n:");
  private CPnlCodigoExtAli txtLCons = null;

  // Pais donde se consumi�
  private Label lblPCons = new Label("Pa�s:");
  private CPnlCodigoExtAli txtPCons = null;

  // Nombre establecimiento
  private Label lblNEstCons = new Label("Nombre establecimiento:");
  private CTexto txtNEstCons = new CTexto(40);

  // Direcci�n establecimiento
  private Label lblDEstCons = new Label("Direcci�n:");
  private CTexto txtDEstCons = new CTexto(40);

  // CP
  private Label lblCPEstCons = new Label("CP:");
  private CTexto txtCPEstCons = new CTexto(5);

  // Municipio
  private Label lblMCons = new Label("Municipio:");
  private CPnlCodigoExtAli txtMCons = null;

  // Tel�fono
  private Label lblTEstCons = new Label("Tel�fono:");
  private CTexto txtTEstCons = new CTexto(14);

  // Medio de transporte
  private Label lblMTrans = new Label("Medio de transporte:");
  private CPnlCodigoExtAli txtMTrans = null;

  // De pa�s
  private Label lblPDe = new Label("De pa�s:");
  private CPnlCodigoExtAli txtPDe = null;

  // A pa�s
  private Label lblPA = new Label("A pa�s:");
  private CPnlCodigoExtAli txtPA = null;

  // Botones de aceptar y cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // modo de operaci�n
  public int modoOperacion = 0;
  public int modoBloqueo = modoNORMAL;

  //imagenes a insertar en la tabla
  final String imgCANCELAR = "images/cancelar.gif";
  final String imgACEPTAR = "images/aceptar.gif";

  public Data dtEntra = null;
  public Lista lEntra = null;
  public Data dtDevuelto = null;
  public boolean bAceptar = false;
//  public Data dtDatFact=new Data();
  XYLayout xyLayout = new XYLayout();

  final String servlet = "servlet/SrvQueryTool";

  public DiaAlibrote(CApp a, int modo, Data dataBusq) {
    super(a);
    modoOperacion = modo;
    dtEntra = dataBusq;
    setTitle("Brotes: Alimentos");

    try {
      QueryTool qtTipo = configurarQtTipo();
      QueryTool qtSospConf = configurarQtSospConf();
      QueryTool qtMetComer = configurarQtMetComer();
      QueryTool qtTratPrevio = configurarQtTratPrevio();
      QueryTool qtFormaServir = configurarQtFormaServir();
      QueryTool qtLugarCont = configurarQtLugarCont();
      QueryTool qtPaisCont = configurarQtPaisCont();
      QueryTool qtLugarPrep = configurarQtLugarPrep();
      QueryTool qtPaisPrep = configurarQtPaisPrep();
      QueryTool qtMunicPrep = configurarQtMunicPrep();
      QueryTool qtLugarCons = configurarQtLugarCons();
      QueryTool qtPaisCons = configurarQtPaisCons();
      QueryTool qtMunicCons = configurarQtMunicCons();
      QueryTool qtMedTrans = configurarQtMedTrans();
      QueryTool qtDePais = configurarQtDePais();
      QueryTool qtAPais = configurarQtAPais();

      txtTAli = new CPnlCodigoExtAli(a,
                                     this,
                                     qtTipo,
                                     "CD_TALIMENTO",
                                     "DS_TALIMENTO",
                                     true,
                                     "Tipo de alimentos",
                                     "Tipo de alimentos",
                                     220,
                                     25);

      txtTImp = new CPnlCodigoExtAli(a,
                                     this,
                                     qtSospConf,
                                     "CD_TIMPLICACION",
                                     "DS_TIMPLICACION",
                                     false,
                                     "Sospechoso/confirmado",
                                     "Sospechoso/confirmado",
                                     220,
                                     25);

      txtMCom = new CPnlCodigoExtAli(a,
                                     this,
                                     qtMetComer,
                                     "CD_MCOMERCALI",
                                     "DS_MCOMERCALI",
                                     false,
                                     "M�todo comercializaci�n",
                                     "M�todo comercializaci�n",
                                     220,
                                     25);

      txtTPrev = new CPnlCodigoExtAli(a,
                                      this,
                                      qtTratPrevio,
                                      "CD_TRATPREVALI",
                                      "DS_TRATPREVALI",
                                      false,
                                      "Tratamiento previo",
                                      "Tratamiento previo",
                                      220,
                                      25);

      txtFIng = new CPnlCodigoExtAli(a,
                                     this,
                                     qtFormaServir,
                                     "CD_FINGERIRALI",
                                     "DS_FINGERIRALI",
                                     false,
                                     "Forma de servir",
                                     "Forma de servir",
                                     220,
                                     25);

      txtLCont = new CPnlCodigoExtAli(a,
                                      this,
                                      qtLugarCont,
                                      "CD_LCONTAMIALI",
                                      "DS_LCONTAMIALI",
                                      false,
                                      "Lugar contaminaci�n",
                                      "Lugar contaminaci�n",
                                      220,
                                      25);

      txtPCont = new CPnlCodigoExtAli(a,
                                      this,
                                      qtPaisCont,
                                      "CD_PAIS",
                                      "DS_PAIS",
                                      false,
                                      "Pais contaminaci�n",
                                      "Pais contaminaci�n",
                                      220,
                                      25);

      txtLPrep = new CPnlCodigoExtAli(a,
                                      this,
                                      qtLugarPrep,
                                      "CD_LPREPALI",
                                      "DS_LPREPALI",
                                      false,
                                      "Lugar preparaci�n",
                                      "Lugar preparaci�n",
                                      220,
                                      25);

      txtPPrep = new CPnlCodigoExtAli(a,
                                      this,
                                      qtPaisPrep,
                                      "CD_PAIS",
                                      "DS_PAIS",
                                      false,
                                      "Pa�s",
                                      "Pa�s",
                                      220,
                                      25);

      txtMPrep = new CPnlCodigoExtAli(a,
                                      this,
                                      qtMunicPrep,
                                      "CD_MUN",
                                      "DS_MUN",
                                      false,
                                      "Municipio",
                                      "Municipio",
                                      220,
                                      25);

      txtLCons = new CPnlCodigoExtAli(a,
                                      this,
                                      qtLugarCons,
                                      "CD_CONSUMOALI",
                                      "DS_CONSUMOALI",
                                      false,
                                      "Lugar consumici�n",
                                      "Lugar consumici�n",
                                      220,
                                      25);

      txtPCons = new CPnlCodigoExtAli(a,
                                      this,
                                      qtPaisCons,
                                      "CD_PAIS",
                                      "DS_PAIS",
                                      false,
                                      "Pa�s",
                                      "Pa�s",
                                      220,
                                      25);

      txtMCons = new CPnlCodigoExtAli(a,
                                      this,
                                      qtMunicCons,
                                      "CD_MUN",
                                      "DS_MUN",
                                      false,
                                      "Municipio",
                                      "Municipio",
                                      220,
                                      25);

      txtMTrans = new CPnlCodigoExtAli(a,
                                       this,
                                       qtMedTrans,
                                       "CD_MTVIAJEALI",
                                       "DS_MTVIAJEALI",
                                       false,
                                       "Medio transporte",
                                       "Medio transporte",
                                       220,
                                       25);

      txtPDe = new CPnlCodigoExtAli(a,
                                    this,
                                    qtDePais,
                                    "CD_PAIS",
                                    "DS_PAIS",
                                    false,
                                    "Pa�s",
                                    "Pa�s",
                                    220,
                                    25);

      txtPA = new CPnlCodigoExtAli(a,
                                   this,
                                   qtAPais,
                                   "CD_PAIS",
                                   "DS_PAIS",
                                   false,
                                   "Pa�s",
                                   "Pa�s",
                                   220,
                                   25);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

  } //end 1. constructor

  public DiaAlibrote(CApp a, int modo, Data dataBusq, Lista lBusq) {
    super(a);
    modoOperacion = modo;
    dtEntra = dataBusq;
    lEntra = lBusq;
    setTitle("Brotes: Alimentos");

    try {
      QueryTool qtTipo = configurarQtTipo();
      QueryTool qtSospConf = configurarQtSospConf();
      QueryTool qtMetComer = configurarQtMetComer();
      QueryTool qtTratPrevio = configurarQtTratPrevio();
      QueryTool qtFormaServir = configurarQtFormaServir();
      QueryTool qtLugarCont = configurarQtLugarCont();
      QueryTool qtPaisCont = configurarQtPaisCont();
      QueryTool qtLugarPrep = configurarQtLugarPrep();
      QueryTool qtPaisPrep = configurarQtPaisPrep();
      QueryTool qtMunicPrep = configurarQtMunicPrep();
      QueryTool qtLugarCons = configurarQtLugarCons();
      QueryTool qtPaisCons = configurarQtPaisCons();
      QueryTool qtMunicCons = configurarQtMunicCons();
      QueryTool qtMedTrans = configurarQtMedTrans();
      QueryTool qtDePais = configurarQtDePais();
      QueryTool qtAPais = configurarQtAPais();

      txtTAli = new CPnlCodigoExtAli(a,
                                     this,
                                     qtTipo,
                                     "CD_TALIMENTO",
                                     "DS_TALIMENTO",
                                     true,
                                     "Tipo de alimentos",
                                     "Tipo de alimentos",
                                     220,
                                     25);

      txtTImp = new CPnlCodigoExtAli(a,
                                     this,
                                     qtSospConf,
                                     "CD_TIMPLICACION",
                                     "DS_TIMPLICACION",
                                     false,
                                     "Sospechoso/confirmado",
                                     "Sospechoso/confirmado",
                                     220,
                                     25);

      txtMCom = new CPnlCodigoExtAli(a,
                                     this,
                                     qtMetComer,
                                     "CD_MCOMERCALI",
                                     "DS_MCOMERCALI",
                                     false,
                                     "M�todo comercializaci�n",
                                     "M�todo comercializaci�n",
                                     220,
                                     25);

      txtTPrev = new CPnlCodigoExtAli(a,
                                      this,
                                      qtTratPrevio,
                                      "CD_TRATPREVALI",
                                      "DS_TRATPREVALI",
                                      false,
                                      "Tratamiento previo",
                                      "Tratamiento previo",
                                      220,
                                      25);

      txtFIng = new CPnlCodigoExtAli(a,
                                     this,
                                     qtFormaServir,
                                     "CD_FINGERIRALI",
                                     "DS_FINGERIRALI",
                                     false,
                                     "Forma de servir",
                                     "Forma de servir",
                                     220,
                                     25);

      txtLCont = new CPnlCodigoExtAli(a,
                                      this,
                                      qtLugarCont,
                                      "CD_LCONTAMIALI",
                                      "DS_LCONTAMIALI",
                                      false,
                                      "Lugar contaminaci�n",
                                      "Lugar contaminaci�n",
                                      220,
                                      25);

      txtPCont = new CPnlCodigoExtAli(a,
                                      this,
                                      qtPaisCont,
                                      "CD_PAIS",
                                      "DS_PAIS",
                                      false,
                                      "Pais contaminaci�n",
                                      "Pais contaminaci�n",
                                      220,
                                      25);

      txtLPrep = new CPnlCodigoExtAli(a,
                                      this,
                                      qtLugarPrep,
                                      "CD_LPREPALI",
                                      "DS_LPREPALI",
                                      false,
                                      "Lugar preparaci�n",
                                      "Lugar preparaci�n",
                                      220,
                                      25);

      txtPPrep = new CPnlCodigoExtAli(a,
                                      this,
                                      qtPaisPrep,
                                      "CD_PAIS",
                                      "DS_PAIS",
                                      false,
                                      "Pa�s",
                                      "Pa�s",
                                      220,
                                      25);

      txtMPrep = new CPnlCodigoExtAli(a,
                                      this,
                                      qtMunicPrep,
                                      "CD_MUN",
                                      "DS_MUN",
                                      false,
                                      "Municipio",
                                      "Municipio",
                                      220,
                                      25);

      txtLCons = new CPnlCodigoExtAli(a,
                                      this,
                                      qtLugarCons,
                                      "CD_CONSUMOALI",
                                      "DS_CONSUMOALI",
                                      false,
                                      "Lugar consumici�n",
                                      "Lugar consumici�n",
                                      220,
                                      25);

      txtPCons = new CPnlCodigoExtAli(a,
                                      this,
                                      qtPaisCons,
                                      "CD_PAIS",
                                      "DS_PAIS",
                                      false,
                                      "Pa�s",
                                      "Pa�s",
                                      220,
                                      25);

      txtMCons = new CPnlCodigoExtAli(a,
                                      this,
                                      qtMunicCons,
                                      "CD_MUN",
                                      "DS_MUN",
                                      false,
                                      "Municipio",
                                      "Municipio",
                                      220,
                                      25);

      txtMTrans = new CPnlCodigoExtAli(a,
                                       this,
                                       qtMedTrans,
                                       "CD_MTVIAJEALI",
                                       "DS_MTVIAJEALI",
                                       false,
                                       "Medio transporte",
                                       "Medio transporte",
                                       220,
                                       25);

      txtPDe = new CPnlCodigoExtAli(a,
                                    this,
                                    qtDePais,
                                    "CD_PAIS",
                                    "DS_PAIS",
                                    false,
                                    "Pa�s",
                                    "Pa�s",
                                    220,
                                    25);

      txtPA = new CPnlCodigoExtAli(a,
                                   this,
                                   qtAPais,
                                   "CD_PAIS",
                                   "DS_PAIS",
                                   false,
                                   "Pa�s",
                                   "Pa�s",
                                   220,
                                   25);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

  } //end 2.constructor

  void jbInit() throws Exception {

    // carga la imagen
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));
    btnCancelar.addActionListener(actionAdapter);

    this.setSize(700, 580);
    xyLayout.setHeight(580);
    xyLayout.setWidth(700);
    this.setLayout(xyLayout);

    txtDescAli.setBackground(new Color(255, 255, 150));

    // a�ade los componentes
    this.add(lblDescAli, new XYConstraints(15, 15, 55, -1));
    this.add(txtDescAli, new XYConstraints(80, 15, 180, ALTO));

    this.add(lblTAli, new XYConstraints(275, 15, 30, -1));
    this.add(txtTAli, new XYConstraints(315, 15, TAMPANEL, -1));

    // Tipo de Implicacion
    this.add(lblTImp, new XYConstraints(15, 45, 143, -1));
    this.add(txtTImp, new XYConstraints(165, 45, TAMPANEL, -1));

    // Marca
    this.add(lblMarca, new XYConstraints(425, 45, 75, -1));
    this.add(txtMarca, new XYConstraints(505, 45, 150, ALTO));

    // Metodo de Comercializacion
    this.add(lblMCom, new XYConstraints(15, 75, 140, -1));
    this.add(txtMCom, new XYConstraints(165, 75, TAMPANEL, -1));

    // Fabricante
    this.add(lblFab, new XYConstraints(425, 75, 70, -1));
    this.add(txtFab, new XYConstraints(505, 75, 150, ALTO));

    // Tratamiento Previo
    this.add(lblTPrev, new XYConstraints(15, 105, 140, -1));
    this.add(txtTPrev, new XYConstraints(165, 105, TAMPANEL, -1));

    // Lote
    this.add(lblLote, new XYConstraints(425, 105, 70, -1));
    this.add(txtLote, new XYConstraints(505, 105, 150, ALTO));

    // Forma de servir o ingerir el alimento
    this.add(lblFIng, new XYConstraints(15, 135, 140, -1));
    this.add(txtFIng, new XYConstraints(165, 135, TAMPANEL, -1));

    // Lugar contaminacion alimento
    this.add(lblLCont, new XYConstraints(15, 165, 140, -1));
    this.add(txtLCont, new XYConstraints(165, 165, TAMPANEL, -1));

    // Pais contaminacion alimento
    this.add(lblPCont, new XYConstraints(425, 165, 28, -1));
    this.add(txtPCont, new XYConstraints(463, 165, TAMPANEL, -1));

    // Lugar donde se preparo
    this.add(lblLPrep, new XYConstraints(15, 215, 140, -1));
    this.add(txtLPrep, new XYConstraints(165, 215, TAMPANEL, -1));

    // Pais donde se preparo
    this.add(lblPPrep, new XYConstraints(425, 215, 28, -1));
    this.add(txtPPrep, new XYConstraints(463, 215, TAMPANEL, -1));

    // Nombre Establecimiento donde se preparo
    this.add(lblNEstPrep, new XYConstraints(15, 245, 140, -1));
    this.add(txtNEstPrep, new XYConstraints(165, 245, 200, ALTO));

    // Direccion Establecimiento donde se preparo
    this.add(lblDEstPrep, new XYConstraints(390, 245, 70, -1));
    this.add(txtDEstPrep, new XYConstraints(463, 245, 205, ALTO));

    // Municipio donde se preparo
    this.add(lblMPrep, new XYConstraints(15, 275, 135, -1));
    this.add(txtMPrep, new XYConstraints(160, 275, TAMPANEL, -1));

    // CP donde se preparo
    this.add(lblCPEstPrep, new XYConstraints(425, 275, 25, -1));
    this.add(txtCPEstPrep, new XYConstraints(463, 275, 50, ALTO));

    // Telefono donde se preparo
    this.add(lblTEstPrep, new XYConstraints(15, 305, 60, -1));
    this.add(txtTEstPrep, new XYConstraints(85, 305, 145, ALTO));

    // Fecha y hora de preparacion
    this.add(lblFHPrep, new XYConstraints(240, 305, 170, -1));
    this.add(txtFechaPrep, new XYConstraints(420, 305, 90, ALTO));
    this.add(txtHoraPrep, new XYConstraints(515, 305, 80, ALTO));

    // Lugar donde se consumio
    this.add(lblLCons, new XYConstraints(15, 355, 140, -1));
    this.add(txtLCons, new XYConstraints(165, 355, TAMPANEL, -1));

    // Pais donde se consumio
    this.add(lblPCons, new XYConstraints(425, 355, 28, -1));
    this.add(txtPCons, new XYConstraints(463, 355, TAMPANEL, -1));

    // Nombre Establecimiento donde se consumio
    this.add(lblNEstCons, new XYConstraints(15, 385, 140, -1));
    this.add(txtNEstCons, new XYConstraints(165, 385, 200, ALTO));

    // Direccion Establecimiento donde se consumio
    this.add(lblDEstCons, new XYConstraints(380, 385, 70, -1));
    this.add(txtDEstCons, new XYConstraints(463, 385, 200, ALTO));

    // Municipio donde se consumio
    this.add(lblMCons, new XYConstraints(15, 415, 135, -1));
    this.add(txtMCons, new XYConstraints(160, 415, TAMPANEL, -1));

    // CP donde se consumio
    this.add(lblCPEstCons, new XYConstraints(425, 415, 25, -1));
    this.add(txtCPEstCons, new XYConstraints(463, 415, 50, ALTO));

    // Telefono donde se consumio
    this.add(lblTEstCons, new XYConstraints(15, 445, 60, -1));
    this.add(txtTEstCons, new XYConstraints(85, 445, 145, ALTO));

    // Si se consumio en viaje: Medio de transporte
    this.add(lblMTrans, new XYConstraints(245, 445, 120, -1));
    this.add(txtMTrans, new XYConstraints(375, 445, TAMPANEL, -1));

    // De Pais
    this.add(lblPDe, new XYConstraints(15, 475, 50, -1));
    this.add(txtPDe, new XYConstraints(75, 475, TAMPANEL, -1));

    // A Pais
    this.add(lblPA, new XYConstraints(320, 475, 50, -1));
    this.add(txtPA, new XYConstraints(375, 475, TAMPANEL, -1));

    // Boton de Aceptar
    this.add(btnAceptar, new XYConstraints(500, 520, 80, -1));
    if (modoOperacion == constantes.modoCONSULTA) {
      btnAceptar.setEnabled(false);
    }
    else {
      btnAceptar.setEnabled(true);
    }

    // Boton de Cancelar
    this.add(btnCancelar, new XYConstraints(590, 520, 80, -1));

    switch (modoOperacion) {
      case MODIFICACION:
      case BAJA:
      case CONSULTA:
        rellenarDatos(dtEntra);
        break;
    }
    Inicializar(CInicializar.NORMAL);
  } //end jbinit

  public void Inicializar() {}

  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if ( (modoOperacion == BAJA) || (modoOperacion == CONSULTA)) {
          this.setEnabled(false);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
        }
        else {
          if ( (modoOperacion == MODIFICACION) || (modoOperacion == ALTA)) {
            this.setEnabled(true);
          }
        }
        break;
    } //end switch
  } //end inicializar

  public void rellenarDatos(Data dt) {
    //Descripci�m
    txtDescAli.setText(dt.getString("DS_ALI"));

    //tipo alimento
    if (dt.getString("CD_TALIMENTO").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_TALIMENTO", dt.getString("CD_TALIMENTO"));
      dtAli.put("DS_TALIMENTO", dt.getString("DS_TALIMENTO"));
      txtTAli.setCodigo(dtAli);
    }

    // Tipo de Implicacion
    if (dt.getString("CD_TIMPLICACION").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_TIMPLICACION", dt.getString("CD_TIMPLICACION"));
      dtAli.put("DS_TIMPLICACION", dt.getString("DS_TIMPLICACION"));
      txtTImp.setCodigo(dtAli);
    }

    // Marca
    txtMarca.setText(dt.getString("DS_NOMCOMER"));

    // Metodo de Comercializacion
    if (dt.getString("CD_MCOMERCALI").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_MCOMERCALI", dt.getString("CD_MCOMERCALI"));
      dtAli.put("DS_MCOMERCALI", dt.getString("DS_MCOMERCALI"));
      txtMCom.setCodigo(dtAli);
    }

    // Fabricante
    txtFab.setText(dt.getString("DS_FABRICAN"));

    // Tratamiento Previo
    if (dt.getString("CD_TRATPREVALI").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_TRATPREVALI", dt.getString("CD_TRATPREVALI"));
      dtAli.put("DS_TRATPREVALI", dt.getString("DS_TRATPREVALI"));
      txtTPrev.setCodigo(dtAli);
    }

    // Lote
    txtLote.setText(dt.getString("DS_LOTE"));

    // Forma de servir o ingerir el alimento
    if (dt.getString("CD_FINGERIRALI").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_FINGERIRALI", dt.getString("CD_FINGERIRALI"));
      dtAli.put("DS_FINGERIRALI", dt.getString("DS_FINGERIRALI"));
      txtFIng.setCodigo(dtAli);
    }

    // Lugar contaminacion alimento
    if (dt.getString("CD_LCONTAMIALI").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_LCONTAMIALI", dt.getString("CD_LCONTAMIALI"));
      dtAli.put("DS_LCONTAMIALI", dt.getString("DS_LCONTAMIALI"));
      txtLCont.setCodigo(dtAli);
    }

    // Pais contaminacion alimento
    if (dt.getString("CD_PAIS_LCONA").length() != 0) {
      String dsPais = obtenerPaisContaminacion();
      Data dtAli = new Data();
      dtAli.put("CD_PAIS", dt.getString("CD_PAIS_LCONA"));
      dtAli.put("DS_PAIS", dsPais);
      txtPCont.setCodigo(dtAli);
    }

    // Lugar donde se preparo
    if (dt.getString("CD_LPREPALI").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_LPREPALI", dt.getString("CD_LPREPALI"));
      dtAli.put("DS_LPREPALI", dt.getString("DS_LPREPALI"));
      txtLPrep.setCodigo(dtAli);
    }

    // Pais donde se preparo
    if (dt.getString("CD_PAIS_LPA").length() != 0) {
      String dsPais = obtenerPaisPrepAlimento();
      Data dtAli = new Data();
      dtAli.put("CD_PAIS", dt.getString("CD_PAIS_LPA"));
      dtAli.put("DS_PAIS", dsPais);
      txtPPrep.setCodigo(dtAli);
    }

    // Nombre Establecimiento donde se preparo
    txtNEstPrep.setText(dt.getString("DS_NOMESTAB"));

    // Direccion Establecimiento donde se preparo
    txtDEstPrep.setText(dt.getString("DS_DIRESTAB"));

    // Municipio donde se preparo
    if (dt.getString("CD_MUN_LPREP").length() != 0) {
      String dsMun = obtenerMunicPreparacion();
      Data dtAli = new Data();
      dtAli.put("CD_MUN", dt.getString("CD_MUN_LPREP"));
      dtAli.put("DS_MUN", dsMun);
      txtMPrep.setCodigo(dtAli);
    }

    // CP donde se preparo
    txtCPEstPrep.setText(dt.getString("CD_POSESTAB"));

    // Telefono donde se preparo
    txtTEstPrep.setText(dt.getString("DS_TELEF_LPREP"));

    // Fecha y hora de preparacion
    // Fecha/Hora de la notificaci�n
    String f = ( (String) dt.getString("FC_PREPARACION")).trim();
    if (!f.equals("")) {
      txtFechaPrep.setText(f.substring(0, f.indexOf(' ', 0)));
      txtHoraPrep.setText( (f.substring(f.indexOf(' ', 0) + 1)).substring(0, 5));
    }

    // Lugar donde se consumio
    if (dt.getString("CD_CONSUMOALI").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_CONSUMOALI", dt.getString("CD_CONSUMOALI"));
      dtAli.put("DS_CONSUMOALI", dt.getString("DS_CONSUMOALI"));
      txtLCons.setCodigo(dtAli);
    }

    // Pais donde se consumio
    if (dt.getString("CD_PAIS_LCON").length() != 0) {
      String dsPais = obtenerPaisConsumicion();
      Data dtAli = new Data();
      dtAli.put("CD_PAIS", dt.getString("CD_PAIS_LCON"));
      dtAli.put("DS_PAIS", dsPais);
      txtPCons.setCodigo(dtAli);
    }

    // Nombre Establecimiento donde se consumio
    txtNEstCons.setText(dt.getString("DS_ESTAB_LCON"));

    // Direccion Establecimiento donde se consumio
    txtDEstCons.setText(dt.getString("DS_DIRESTAB_LCON"));

    // Municipio donde se consumio
    if (dt.getString("CD_MUN_LCON").length() != 0) {
      String dsMun = obtenerMunicConsumicion();
      Data dtAli = new Data();
      dtAli.put("CD_MUN", dt.getString("CD_MUN_LCON"));
      dtAli.put("DS_MUN", dsMun);
      txtMCons.setCodigo(dtAli);
    }

    // CP donde se consumio
    txtCPEstCons.setText(dt.getString("CD_POSESTAB_LCON"));

    // Telefono donde se consumio
    txtTEstCons.setText(dt.getString("DS_TELEF_LCON"));

    // Si se consumio en viaje: Medio de transporte
    if (dt.getString("CD_MTVIAJEALI").length() != 0) {
      Data dtAli = new Data();
      dtAli.put("CD_MTVIAJEALI", dt.getString("CD_MTVIAJEALI"));
      dtAli.put("DS_MTVIAJEALI", dt.getString("DS_MTVIAJEALI"));
      txtMTrans.setCodigo(dtAli);
    }

    // De Pais
    if (dt.getString("CD_PAIS_DE").length() != 0) {
      String dsPais = obtenerPaisDe();
      Data dtAli = new Data();
      dtAli.put("CD_PAIS", dt.getString("CD_PAIS_DE"));
      dtAli.put("DS_PAIS", dsPais);
      txtPDe.setCodigo(dtAli);
    }

    // A Pais
    if (dt.getString("CD_PAIS_A").length() != 0) {
      String dsPais = obtenerPaisA();
      Data dtAli = new Data();
      dtAli.put("CD_PAIS", dt.getString("CD_PAIS_A"));
      dtAli.put("DS_PAIS", dsPais);
      txtPA.setCodigo(dtAli);
    }
  } //end rellenarDatos

  public String obtenerMunicPreparacion() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    String salir = new String();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";
    try {
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_MUNICIPIO");
      qt.putType("DS_MUN", QueryTool.STRING);
      qt.putWhereType("CD_MUN", QueryTool.STRING);
      qt.putWhereValue("CD_MUN", dtEntra.getString("CD_MUN_LPREP"));
      qt.putOperator("CD_MUN", "=");
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        salir = ( (Data) p1.elementAt(0)).getString("DS_MUN");
      }
      else {
        salir = "";
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
    Inicializar(CInicializar.NORMAL);
    return salir;
  }

  public String obtenerMunicConsumicion() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    String salir = new String();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";
    try {
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_MUNICIPIO");
      qt.putType("DS_MUN", QueryTool.STRING);
      qt.putWhereType("CD_MUN", QueryTool.STRING);
      qt.putWhereValue("CD_MUN", dtEntra.getString("CD_MUN_LCON"));
      qt.putOperator("CD_MUN", "=");
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        salir = ( (Data) p1.elementAt(0)).getString("DS_MUN");
      }
      else {
        salir = "";
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
    Inicializar(CInicializar.NORMAL);
    return salir;
  }

  public String obtenerPaisContaminacion() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    String salir = new String();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";
    try {
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_PAISES");
      qt.putType("DS_PAIS", QueryTool.STRING);
      qt.putWhereType("CD_PAIS", QueryTool.STRING);
      qt.putWhereValue("CD_PAIS", dtEntra.getString("CD_PAIS_LCONA"));
      qt.putOperator("CD_PAIS", "=");
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        salir = ( (Data) p1.elementAt(0)).getString("DS_PAIS");
      }
      else {
        salir = "";
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
    Inicializar(CInicializar.NORMAL);
    return salir;
  }

  public String obtenerPaisPrepAlimento() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    String salir = new String();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";
    try {
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_PAISES");
      qt.putType("DS_PAIS", QueryTool.STRING);
      qt.putWhereType("CD_PAIS", QueryTool.STRING);
      qt.putWhereValue("CD_PAIS", dtEntra.getString("CD_PAIS_LPA"));
      qt.putOperator("CD_PAIS", "=");
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        salir = ( (Data) p1.elementAt(0)).getString("DS_PAIS");
      }
      else {
        salir = "";
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
    Inicializar(CInicializar.NORMAL);
    return salir;
  }

  public String obtenerPaisConsumicion() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    String salir = new String();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";
    try {
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_PAISES");
      qt.putType("DS_PAIS", QueryTool.STRING);
      qt.putWhereType("CD_PAIS", QueryTool.STRING);
      qt.putWhereValue("CD_PAIS", dtEntra.getString("CD_PAIS_LCON"));
      qt.putOperator("CD_PAIS", "=");
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        salir = ( (Data) p1.elementAt(0)).getString("DS_PAIS");
      }
      else {
        salir = "";
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
    Inicializar(CInicializar.NORMAL);
    return salir;
  }

  public String obtenerPaisDe() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    String salir = new String();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";
    try {
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_PAISES");
      qt.putType("DS_PAIS", QueryTool.STRING);
      qt.putWhereType("CD_PAIS", QueryTool.STRING);
      qt.putWhereValue("CD_PAIS", dtEntra.getString("CD_PAIS_DE"));
      qt.putOperator("CD_PAIS", "=");
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        salir = ( (Data) p1.elementAt(0)).getString("DS_PAIS");
      }
      else {
        salir = "";
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
    Inicializar(CInicializar.NORMAL);
    return salir;
  }

  public String obtenerPaisA() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    String salir = new String();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";
    try {
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_PAISES");
      qt.putType("DS_PAIS", QueryTool.STRING);
      qt.putWhereType("CD_PAIS", QueryTool.STRING);
      qt.putWhereValue("CD_PAIS", dtEntra.getString("CD_PAIS_A"));
      qt.putOperator("CD_PAIS", "=");
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        salir = ( (Data) p1.elementAt(0)).getString("DS_PAIS");
      }
      else {
        salir = "";
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
    Inicializar(CInicializar.NORMAL);
    return salir;
  }

  public Data recogerDatos() {
    Data dtRecoger = new Data();

    dtRecoger.put("CD_ANO", dtEntra.getString("CD_ANO"));
    dtRecoger.put("NM_ALERBRO", dtEntra.getString("NM_ALERBRO"));
    dtRecoger.put("DS_ALI", txtDescAli.getText());

    //tipo alimento
    Data dttxtAli = txtTAli.getDatos();
    if (dttxtAli != null) {
      dtRecoger.put("CD_TALIMENTO", dttxtAli.getString("CD_TALIMENTO"));
      dtRecoger.put("DS_TALIMENTO", dttxtAli.getString("DS_TALIMENTO"));
    }

    // Tipo de Implicacion
    Data dttxtTImp = txtTImp.getDatos();
    if (dttxtTImp != null) {
      dtRecoger.put("CD_TIMPLICACION", dttxtTImp.getString("CD_TIMPLICACION"));
      dtRecoger.put("DS_TIMPLICACION", dttxtTImp.getString("DS_TIMPLICACION"));
    }

    // Marca
    dtRecoger.put("DS_NOMCOMER", txtMarca.getText());

    // Metodo de Comercializacion
    Data dttxtMCom = txtMCom.getDatos();
    if (dttxtMCom != null) {
      dtRecoger.put("CD_MCOMERCALI", dttxtMCom.getString("CD_MCOMERCALI"));
      dtRecoger.put("DS_MCOMERCALI", dttxtMCom.getString("DS_MCOMERCALI"));
    }

    // Fabricante
    dtRecoger.put("DS_FABRICAN", txtFab.getText());

    // Tratamiento Previo
    Data dttxtTPrev = txtTPrev.getDatos();
    if (dttxtTPrev != null) {
      dtRecoger.put("CD_TRATPREVALI", dttxtTPrev.getString("CD_TRATPREVALI"));
      dtRecoger.put("DS_TRATPREVALI", dttxtTPrev.getString("DS_TRATPREVALI"));
    }

    // Lote
    dtRecoger.put("DS_LOTE", txtLote.getText());

    // Forma de servir o ingerir el alimento
    Data dttxtFIng = txtFIng.getDatos();
    if (dttxtFIng != null) {
      dtRecoger.put("CD_FINGERIRALI", dttxtFIng.getString("CD_FINGERIRALI"));
      dtRecoger.put("DS_FINGERIRALI", dttxtFIng.getString("DS_FINGERIRALI"));
    }

    // Lugar contaminacion alimento
    Data dttxtLCont = txtLCont.getDatos();
    if (dttxtLCont != null) {
      dtRecoger.put("CD_LCONTAMIALI", dttxtLCont.getString("CD_LCONTAMIALI"));
      dtRecoger.put("DS_LCONTAMIALI", dttxtLCont.getString("DS_LCONTAMIALI"));
    }

    // Pais contaminacion alimento
    Data dttxtPCont = txtPCont.getDatos();
    if (dttxtPCont != null) {
      dtRecoger.put("CD_PAIS_LCONA", dttxtPCont.getString("CD_PAIS"));
//      dtRecoger.put("DS_PAIS",dttxtPCont.getString("DS_PAIS"));
    }

    // Lugar donde se preparo
    Data dttxtLPrep = txtLPrep.getDatos();
    if (dttxtLPrep != null) {
      dtRecoger.put("CD_LPREPALI", dttxtLPrep.getString("CD_LPREPALI"));
      dtRecoger.put("DS_LPREPALI", dttxtLPrep.getString("DS_LPREPALI"));
    }

    // Pais donde se preparo
    Data dttxtPPrep = txtPPrep.getDatos();
    if (dttxtPPrep != null) {
      dtRecoger.put("CD_PAIS_LPA", dttxtPPrep.getString("CD_PAIS"));
//      dtRecoger.put("DS_PAIS",dttxtPPrep.getString("DS_PAIS"));
    }

    // Nombre Establecimiento donde se preparo
    dtRecoger.put("DS_NOMESTAB", txtNEstPrep.getText());

    // Direccion Establecimiento donde se preparo
    dtRecoger.put("DS_DIRESTAB", txtDEstPrep.getText());

    // Municipio donde se preparo
    Data dttxtMPrep = txtMPrep.getDatos();
    if (dttxtMPrep != null) {
      dtRecoger.put("CD_MUN_LPREP", dttxtMPrep.getString("CD_MUN"));
//      dtRecoger.put("DS_MUN",dttxtMPrep.getString("DS_MUN"));
    }

    // CP donde se preparo
    dtRecoger.put("CD_POSESTAB", txtCPEstPrep.getText());

    // Telefono donde se preparo
    dtRecoger.put("DS_TELEF_LPREP", txtTEstPrep.getText());

    // Fecha y hora de preparacion
    String fExp = "";
    if (!txtFechaPrep.getText().equals("")) {
      fExp = txtFechaPrep.getText().trim();
      if (!txtHoraPrep.getText().equals("")) {
        fExp += " " + txtHoraPrep.getText().trim() + ":00:00";
      }
      else {
        fExp += " 00:00:00";
      }
    }
    else {
      fExp = "";
    }
    dtRecoger.put("FC_PREPARACION", fExp);

    // Lugar donde se consumio
    Data dttxtLCons = txtLCons.getDatos();
    if (dttxtLCons != null) {
      dtRecoger.put("CD_CONSUMOALI", dttxtLCons.getString("CD_CONSUMOALI"));
      dtRecoger.put("DS_CONSUMOALI", dttxtLCons.getString("DS_CONSUMOALI"));
    }

    // Pais donde se consumio
    Data dttxtPCons = txtPCons.getDatos();
    if (dttxtPCons != null) {
      dtRecoger.put("CD_PAIS_LCON", dttxtPCons.getString("CD_PAIS"));
//      dtRecoger.put("DS_PAIS",dttxtPCons.getString("DS_PAIS"));
    }

    // Nombre Establecimiento donde se consumio
    dtRecoger.put("DS_ESTAB_LCON", txtNEstCons.getText());

    // Direccion Establecimiento donde se consumio
    dtRecoger.put("DS_DIRESTAB_LCON", txtDEstCons.getText());

    // Municipio donde se consumio
    Data dttxtMCons = txtMCons.getDatos();
    if (dttxtMCons != null) {
      dtRecoger.put("CD_MUN_LCON", dttxtMCons.getString("CD_MUN"));
//      dtRecoger.put("DS_MUN",dttxtMCons.getString("DS_MUN"));
    }

    // CP donde se consumio
    dtRecoger.put("CD_POSESTAB_LCON", txtCPEstCons.getText());

    // Telefono donde se consumio
    dtRecoger.put("DS_TELEF_LCON", txtTEstCons.getText());

    // Si se consumio en viaje: Medio de transporte
    Data dttxtMTrans = txtMTrans.getDatos();
    if (dttxtMTrans != null) {
      dtRecoger.put("CD_MTVIAJEALI", dttxtMTrans.getString("CD_MTVIAJEALI"));
      dtRecoger.put("DS_MTVIAJEALI", dttxtMTrans.getString("DS_MTVIAJEALI"));
    }

    // De Pais
    Data dttxtPDe = txtPDe.getDatos();
    if (dttxtPDe != null) {
      dtRecoger.put("CD_PAIS_DE", dttxtPDe.getString("CD_PAIS"));
//      dtRecoger.put("DS_PAIS",dttxtPDe.getString("DS_PAIS"));
    }

    // A Pais
    Data dttxtPA = txtPA.getDatos();
    if (dttxtPA != null) {
      dtRecoger.put("CD_PAIS_A", dttxtPA.getString("CD_PAIS"));
//      dtRecoger.put("DS_PAIS",dttxtPA.getString("DS_PAIS"));
    }

    switch (modoOperacion) {
      case ALTA:
        dtRecoger.put("TIPO_OPERACION", "A");
        dtRecoger.put("IT_CALCTASA", "M");
        break;
      case MODIFICACION:
        if (dtEntra.getString("TIPO_OPERACION").equals("A")) {
          dtRecoger.put("TIPO_OPERACION", "A");
          dtRecoger.put("IT_CALCTASA", "M");
        }
        else { //si es modif de BD o de otra modificacion
          dtRecoger.put("TIPO_OPERACION", "M");
          dtRecoger.put("NM_ALIBROTE", dtEntra.getString("NM_ALIBROTE"));
          dtRecoger.put("IT_CALCTASA", dtEntra.getString("IT_CALCTASA"));
          dtRecoger.put("NM_EXPENF", dtEntra.getString("NM_EXPENF"));
          dtRecoger.put("NM_EXPNOENF", dtEntra.getString("NM_EXPNOENF"));
          dtRecoger.put("NM_NOEXPENF", dtEntra.getString("NM_NOEXPENF"));
          dtRecoger.put("NM_NOEXPNOENF", dtEntra.getString("NM_NOEXPNOENF"));
        }
        break;
      case BAJA:
        if (dtEntra.getString("TIPO_OPERACION").equals("A")) {
          dtRecoger.put("TIPO_OPERACION", "E"); //Eliminar
        }
        else { //si es dar de baja una de bd o una modificada
          dtRecoger.put("TIPO_OPERACION", "B");
          dtRecoger.put("NM_ALIBROTE", dtEntra.getString("NM_ALIBROTE"));
          dtRecoger.put("IT_CALCTASA", dtEntra.getString("IT_CALCTASA"));
        }
        break;
      case CONSULTA:
        dtRecoger.put("TIPO_OPERACION", "C"); //Eliminar
        break;
    } //end switch
    return dtRecoger;
  } //end recogerDatos

  public boolean validarDatos() {
    boolean b = true;
    //compruebo q campos obligatorios est�n rellenos.
    Data dtTipo = txtTAli.getDatos();
    if (dtTipo == null) {
      this.getApp().showAdvise("El campo Tipo es obligatorio");
      b = false;
    }
    else {
      if (txtDescAli.getText().length() == 0) {
        this.getApp().showAdvise("El campo Alimento es obligatorio");
        b = false;
      }
      else {
        b = true;
      }
    }
    //si por ahora es v�lido compruebo la fecha
    if (b) {
      if (txtFechaPrep.getText().trim().length() != 0) {
        txtFechaPrep.ValidarFecha();
        if (txtFechaPrep.getValid().equals("N")) {
          this.getApp().showError("Fecha incorrecta");
          b = false;
        }
        else {
          if (txtHoraPrep.getText().trim().length() == 0) {
            this.getApp().showError(
                "Debe completar el campo fecha/hora con la hora");
            b = false;
          }
          else { //si esta rellena la fecha y hora valido la hora
            if (txtHoraPrep.getText().trim().length() != 0) {
              txtHoraPrep.ValidarFecha();
              if (txtHoraPrep.getValid().equals("N")) {
                this.getApp().showError("Hora incorrecta");
                b = false;
              }
            }
          }
        } //end else de fecha getvalid
      }
      else { //si fecha vacia se comprueba q hora tb.
        if (txtHoraPrep.getText().trim().length() != 0) {
          this.getApp().showError(
              "Debe completar el campo fecha/hora con la fecha");
          b = false;
        }
      } //end else si fecha vacia
    } //si b

    //si se han hecho las validaciones de datos y todos son correctos->
    //se valida q la descripci�n introducida del alimento no exista ya
    //en la BD.
    if (b) {
      //solo se comprueba si es modif o alta
      if (modoOperacion == ALTA) {
        boolean encontrado = buscarDescripcion(txtDescAli.getText().trim());
        if (encontrado) {
          this.getApp().showError(
              "La descripci�n del alimento que se quiere introducir ya existe");
          txtDescAli.setText("");
          b = false;
        }
      } //end if modoOp=BAJA
      if (modoOperacion == MODIFICACION) {
        //solo se realiza la comprobaci�n si se ha modificado el campo descripci�n
        if (! (dtEntra.getString("DS_ALI").equals(txtDescAli.getText().trim()))) {
          boolean encontrado = buscarDescripcion(txtDescAli.getText().trim());
          if (encontrado) {
            this.getApp().showError(
                "La descripci�n del alimento que se quiere introducir ya existe");
            txtDescAli.setText("");
            b = false;
          }
        } //end if campo descrip modif
      } //end if modoOp=MODIF
    } //end if b
    return b;
  }

  boolean buscarDescripcion(String desc) {
    //recorro la lista de alimentosq actualmente hay para ese brote->
    //no lo hago en la BD, pq puede q existan alimentos cuyos valores
    //de bd han sido modif pero todavia no se ha llevado a cabo la modif
    boolean salir = false;
    for (int i = 0; i < lEntra.size() && salir == false; i++) {
      //si existe 1 descripc q sea =
      if ( ( (Data) lEntra.elementAt(i)).getString("DS_ALI").equals(desc)) {
        salir = true;
      } //end if
    } //end for

    return salir;
  } //end  buscarDescripcion

  /*  boolean buscarDescripcion(String desc){
    Lista p=new Lista();
    Lista p1=new Lista();
    boolean encontrado=false;
    final String servQT = "servlet/SrvQueryTool";
    QueryTool qtDesc = new QueryTool();
    qtDesc.putName("SIVE_ALI_BROTE");
    qtDesc.putType("DS_ALI", QueryTool.STRING);
    // filtro de a�o y numero de brote
    qtDesc.putWhereType("DS_ALI", QueryTool.STRING);
    qtDesc.putWhereValue("DS_ALI", desc);
    qtDesc.putOperator("DS_ALI", "=");
    qtDesc.putWhereType("CD_ANO", QueryTool.STRING);
    qtDesc.putWhereValue("CD_ANO", dtEntra.getString("CD_ANO"));
    qtDesc.putOperator("CD_ANO", "=");
    qtDesc.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtDesc.putWhereValue("NM_ALERBRO",dtEntra.getString("NM_ALERBRO"));
    qtDesc.putOperator("NM_ALERBRO", "=");
    try{
      p.addElement(qtDesc);
      p1= BDatos.ejecutaSQL(false ,this.getApp(),servQT,1,p);
      //si no devuelve ning�n valor-> no encuentra
      if (p1.size()==0) {
        encontrado=false;
      }else{//si tiene alg�n valor-> existe una descripci�n =
        encontrado=true;
      }
    } catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
     return encontrado;
     }//end buscarDescripcion*/

  QueryTool configurarQtTipo() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_TALIMENTO");
    qt.putType("CD_TALIMENTO", QueryTool.STRING);
    qt.putType("DS_TALIMENTO", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtSospConf() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_T_IMPLICACION");
    qt.putType("CD_TIMPLICACION", QueryTool.STRING);
    qt.putType("DS_TIMPLICACION", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtMetComer() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MCOMERC_ALI");
    qt.putType("CD_MCOMERCALI", QueryTool.STRING);
    qt.putType("DS_MCOMERCALI", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtTratPrevio() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_TRAT_PREVIO_ALI");
    qt.putType("CD_TRATPREVALI", QueryTool.STRING);
    qt.putType("DS_TRATPREVALI", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtFormaServir() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_F_INGERIR_ALI");
    qt.putType("CD_FINGERIRALI", QueryTool.STRING);
    qt.putType("DS_FINGERIRALI", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtLugarCont() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_LCONTAMI_ALI");
    qt.putType("CD_LCONTAMIALI", QueryTool.STRING);
    qt.putType("DS_LCONTAMIALI", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtPaisCont() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PAISES");
    qt.putType("CD_PAIS", QueryTool.STRING);
    qt.putType("DS_PAIS", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtLugarPrep() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_LPREP_ALI");
    qt.putType("CD_LPREPALI", QueryTool.STRING);
    qt.putType("DS_LPREPALI", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtPaisPrep() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PAISES");
    qt.putType("CD_PAIS", QueryTool.STRING);
    qt.putType("DS_PAIS", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtMunicPrep() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MUNICIPIO");
    qt.putType("CD_MUN", QueryTool.STRING);
    qt.putType("DS_MUN", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtLugarCons() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_LCONSUMO_ALI");
    qt.putType("CD_CONSUMOALI", QueryTool.STRING);
    qt.putType("DS_CONSUMOALI", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtPaisCons() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PAISES");
    qt.putType("CD_PAIS", QueryTool.STRING);
    qt.putType("DS_PAIS", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtMunicCons() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MUNICIPIO");
    qt.putType("CD_MUN", QueryTool.STRING);
    qt.putType("DS_MUN", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtMedTrans() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MTVIAJE_ALI");
    qt.putType("CD_MTVIAJEALI", QueryTool.STRING);
    qt.putType("DS_MTVIAJEALI", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtDePais() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PAISES");
    qt.putType("CD_PAIS", QueryTool.STRING);
    qt.putType("DS_PAIS", QueryTool.STRING);
    return qt;
  }

  QueryTool configurarQtAPais() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PAISES");
    qt.putType("CD_PAIS", QueryTool.STRING);
    qt.putType("DS_PAIS", QueryTool.STRING);
    return qt;
  }

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  public Data dtDevolver() {
    return dtDevuelto;
  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  void btnAceptarActionPerformed() {
    if (validarDatos()) {
      dtDevuelto = recogerDatos();
      switch (modoOperacion) {
        case BAJA:
          if (Common.ShowPregunta(this.getApp(), "�Desea dejar el alimento como sospechoso y no confirmado en el brote, en vez de darlo de baja?")) {
            dtDevuelto = recogerDatosBorradoSospechoso();
          }
          else { //se borra el registro
            //si es un elemento de la bd
            if (! (dtEntra.getString("TIPO_OPERACION").equals("A"))) {
              //si se ha usado en tasas de ataque
              if (dtEntra.getString("NM_EXPENF").length() != 0) {
                this.getApp().showAdvise(
                    "El elemento que va a ser eliminado se ha usado en tasas de ataque");
              }
            }
          }
          break;
      }
      bAceptar = true;
      dispose();
    } //end if validarDatos
  } //end btnaceptar

  Data recogerDatosBorradoSospechoso() {
    Data dtRecoger = new Data();

    dtRecoger.put("CD_ANO", dtEntra.getString("CD_ANO"));
    dtRecoger.put("NM_ALERBRO", dtEntra.getString("NM_ALERBRO"));
    dtRecoger.put("DS_ALI", txtDescAli.getText());

    //tipo alimento
    Data dttxtAli = txtTAli.getDatos();
    if (dttxtAli != null) {
      dtRecoger.put("CD_TALIMENTO", dttxtAli.getString("CD_TALIMENTO"));
      dtRecoger.put("DS_TALIMENTO", dttxtAli.getString("DS_TALIMENTO"));
    }

    dtRecoger.put("CD_TIMPLICACION", "02");
    dtRecoger.put("DS_TIMPLICACION", "Sospechoso");

    dtRecoger.put("DS_NOMCOMER", "");

    dtRecoger.put("CD_MCOMERCALI", "");
    dtRecoger.put("DS_MCOMERCALI", "");

    dtRecoger.put("DS_FABRICAN", "");

    dtRecoger.put("CD_TRATPREVALI", "");
    dtRecoger.put("DS_TRATPREVALI", "");

    dtRecoger.put("DS_LOTE", "");

    dtRecoger.put("CD_FINGERIRALI", "");
    dtRecoger.put("DS_FINGERIRALI", "");

    dtRecoger.put("CD_LCONTAMIALI", "");
    dtRecoger.put("DS_LCONTAMIALI", "");

    dtRecoger.put("CD_PAIS_LCONA", "");

    dtRecoger.put("CD_LPREPALI", "");
    dtRecoger.put("DS_LPREPALI", "");

    dtRecoger.put("CD_PAIS_LPA", "");

    dtRecoger.put("DS_NOMESTAB", "");

    dtRecoger.put("DS_DIRESTAB", "");

    dtRecoger.put("CD_MUN_LPREP", "");

    dtRecoger.put("CD_POSESTAB", "");

    dtRecoger.put("DS_TELEF_LPREP", "");

    dtRecoger.put("FC_PREPARACION", "");

    dtRecoger.put("CD_CONSUMOALI", "");
    dtRecoger.put("DS_CONSUMOALI", "");

    dtRecoger.put("CD_PAIS_LCON", "");

    dtRecoger.put("DS_ESTAB_LCON", "");

    dtRecoger.put("DS_DIRESTAB_LCON", "");

    dtRecoger.put("CD_MUN_LCON", "");

    dtRecoger.put("CD_POSESTAB_LCON", "");

    dtRecoger.put("DS_TELEF_LCON", "");

    dtRecoger.put("CD_MTVIAJEALI", "");
    dtRecoger.put("DS_MTVIAJEALI", "");

    dtRecoger.put("CD_PAIS_DE", "");

    dtRecoger.put("CD_PAIS_A", "");

    if (dtEntra.getString("TIPO_OPERACION").equals("A")) {
      dtRecoger.put("TIPO_OPERACION", "A");
      dtRecoger.put("IT_CALCTASA", "M");
    }
    else { //si es modif de BD o de otra modificacion
      dtRecoger.put("TIPO_OPERACION", "M");
      dtRecoger.put("NM_ALIBROTE", dtEntra.getString("NM_ALIBROTE"));
      dtRecoger.put("IT_CALCTASA", dtEntra.getString("IT_CALCTASA"));
      dtRecoger.put("NM_EXPENF", dtEntra.getString("NM_EXPENF"));
      dtRecoger.put("NM_EXPNOENF", dtEntra.getString("NM_EXPNOENF"));
      dtRecoger.put("NM_NOEXPENF", dtEntra.getString("NM_NOEXPENF"));
      dtRecoger.put("NM_NOEXPNOENF", dtEntra.getString("NM_NOEXPNOENF"));
    }
    return dtRecoger;
  }

  void btnCancelarActionPerformed() {
    bAceptar = false;
    dispose();
  }

} //end class

class DiaAlibroteActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaAlibrote adaptee;
  ActionEvent evt;

  DiaAlibroteActionAdapter(DiaAlibrote adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
        //adaptee.btnAreaActionPerformed();
      }
      if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaAlibroteActionAdapter
