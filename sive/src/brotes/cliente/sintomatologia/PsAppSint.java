// 8/05/2000 (PDP) Applet que extiende de CApp utilizado
//  en el enganche de capp.CApp y capp2.CApp

package brotes.cliente.sintomatologia;

//import capp2.CApp;
import capp.CApp;

public class PsAppSint
    extends CApp {

  public PsAppSint() {
    super();
  }

  public void init() {
    super.init();
    setTitulo("PSAPPSINT");
    this.setName("PsAppSint");
  }
}
