package brotes.cliente.mantcatalog;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CDecimal;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

//import gesvac.cliente.sipproducto.PanProdu;

/**
 * Di�logo a trav�s del cu�l se insertan/modifican/borran datos
 * en tablas de mantenimiento de vacunaci�n.
 * @autor ARS
 * @version 1.0
 */

public class DiaMant
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  public Data dtDev = null;
  // par�metros configuraci�n de ventana.
  private Data dtParam = null;
  private boolean bExtra = false;
  private boolean bNumerico = false;
  private String nomCampoCod = "";
  private String nomCampoDesc = "";
  private String nomCampoExt = "";
  private String nomCampoNum = "";
  private String nomTabla = "";
  private int tipoCampoCod = 0;
  private int tipoCampoDesc = 0;
  private int tipoCampoExt = 0;
  private int tipoCampoNum = 0;
  //
  public Data dtDevuelto = new Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xYLayout1 = new XYLayout();

  Label lblCod = new Label();
  Label lblDes = new Label();
  Label label1 = new Label();
  CCodigo txtCodigo = null;
  CTexto txtDescripcion = null;
  Checkbox chkPres = new Checkbox();
  CDecimal txtDivisa = null;
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...

  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  // constructor del di�logo DiaMant
  public DiaMant(CApp a, int modoop, Data dt, Data parametros) {

    super(a);

    try {
      modoOperacion = modoop;
      dtDev = dt;
      dtParam = parametros;
      // �tiene tercer campo el di�logo?
      bExtra = Boolean.valueOf(dtParam.getString("bCheckExtra")).booleanValue();
      bNumerico = Boolean.valueOf(dtParam.getString("bNumExtra")).booleanValue();
      // longitud de los campos de texto...
      txtCodigo = new CCodigo(Integer.parseInt(dtParam.getString("nLongCod")));
      txtDescripcion = new CTexto(Integer.parseInt(dtParam.getString(
          "nLongDesc")));

      // Cogemos los par�metros de la tabla
      nomTabla = dtParam.getString("sNomTabla");
      nomCampoCod = dtParam.getString("sNomCampoCod");
      nomCampoDesc = dtParam.getString("sNomCampoDes");
      if (bExtra) {
        nomCampoExt = dtParam.getString("sNomCheckExtra");
        tipoCampoExt = Integer.parseInt(dtParam.getString("sTipoCheckExtra"));
      }
      if (bNumerico) {
        nomCampoNum = dtParam.getString("sNomNumExtra");
        tipoCampoNum = Integer.parseInt(dtParam.getString("sTipoNumExtra"));
        txtDivisa = new CDecimal(Integer.parseInt(dtParam.getString(
            "nLongNumExtraEnt")),
                                 Integer.parseInt(dtParam.getString(
            "nLongNumExtraDec")));
      }
      tipoCampoCod = Integer.parseInt(dtParam.getString("sTipoCampoCod"));
      tipoCampoDesc = Integer.parseInt(dtParam.getString("sTipoCampoDes"));

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(400, 200);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    txtCodigo.setBackground(new Color(255, 255, 150));
    txtCodigo.setText("");
    lblDes.setText("Descripci�n");
    lblCod.setText("C�digo");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    label1.setText("Factor de conversion");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    chkPres.setLabel("Tiene mas informacion");
    txtDescripcion.setBackground(new Color(255, 255, 150));
    txtDescripcion.setText("");
    if (bNumerico) {
      txtDivisa.setBackground(new Color(255, 255, 150));
      txtDivisa.setText("");
    }

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xYLayout1.setHeight(199);
    xYLayout1.setWidth(397);

    this.add(lblCod, new XYConstraints(21, 11, 62, -1));
    this.add(txtCodigo, new XYConstraints(129, 11, 142, -1));
    this.add(lblDes, new XYConstraints(21, 41, 76, -1));
    this.add(txtDescripcion, new XYConstraints(129, 41, 246, -1));
    this.add(chkPres, new XYConstraints(23, 71, 201, 24));
    if (bNumerico) {
      this.add(txtDivisa, new XYConstraints(146, 71, 124, -1));
      this.add(label1, new XYConstraints(23, 71, 123, -1));
    }
    this.add(btnAceptar, new XYConstraints(187, 119, 89, -1));
    this.add(btnCancelar, new XYConstraints(281, 119, 89, -1));
    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    // Hacemos o no, visibles los campos del di�logo necesarios
    chkPres.setVisible(bExtra);
    if (bNumerico) {
      txtDivisa.setVisible(bNumerico);
      label1.setVisible(bNumerico);
    }
    // ponemos el t�tulo

    switch (modoOperacion) {
      case modoALTA:
        setTitle(dtParam.getString("sTituloDialogoAlta"));
        break;
      case modoMODIFICACION:
        this.setTitle(dtParam.getString("sTituloDialogoModif"));
        break;
      case modoBAJA:
        this.setTitle(dtParam.getString("sTituloDialogoBaja"));
        break;
    }
  }

  // Pone los campos de texto en el estado que les corresponda
  // segun el modo.

  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        txtCodigo.setEnabled(true);
        txtDescripcion.setEnabled(true);
        if (bExtra) {
          chkPres.setEnabled(true);
        }
        if (bNumerico) {
          txtDivisa.setEnabled(true);
        }
        break;

      case modoMODIFICACION:
        txtCodigo.setEnabled(false);
        txtDescripcion.setEnabled(true);
        // datos panel inferior
        txtCodigo.setText(dtDev.getString(nomCampoCod));
        txtDescripcion.setText(dtDev.getString(nomCampoDesc));
        if (bExtra) {
          chkPres.setEnabled(true);
          if (dtDev.getString(nomCampoExt).equals("S")) {
            chkPres.setState(true);
          }
          else if (dtDev.getString(nomCampoExt).equals("N")) {
            chkPres.setState(false);
          }
        }
        if (bNumerico) {
          txtDivisa.setText(dtDev.getString(nomCampoNum));
          txtDivisa.setEnabled(true);
        }

        break;

        // s�lo habilitado aceptar y cancelar
      case modoBAJA:
        txtCodigo.setText(dtDev.getString(nomCampoCod));
        txtDescripcion.setText(dtDev.getString(nomCampoDesc));
        if (bExtra) {
          chkPres.setEnabled(false);
          if (dtDev.getString(nomCampoExt).equals("S")) {
            chkPres.setState(true);
          }
          else if (dtDev.getString(nomCampoExt).equals("N")) {
            chkPres.setState(false);
          }
        }
        if (bNumerico) {
          txtDivisa.setEnabled(false);
          txtDivisa.setText(dtDev.getString(nomCampoNum));
        }
        txtCodigo.setEnabled(false);
        txtDescripcion.setEnabled(false);
        break;
    }
  }

  // Pone el di�logo en modo de espera

  public void Inicializar(int modo) {
    switch (modo) {

      // componente producto accediendo a base de datos
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            break;

          case modoMODIFICACION:
            txtCodigo.setEnabled(false);
            txtDescripcion.setEnabled(true);
            if (bExtra) {
              chkPres.setEnabled(true);
            }
            if (bNumerico) {
              txtDivisa.setEnabled(true);
            }
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  private boolean isDataValid() {
    if ( (txtCodigo.getText().trim().length() == 0) ||
        (txtDescripcion.getText().trim().length() == 0) ||
        ( (bNumerico) && (txtDivisa.getText().trim().length() == 0))) {
      getApp().showAdvise("Debe completar todos los campos obligatorios");
      return false;
    }
    else {
      return true;
    }
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    final String servlet = "servlet/SrvQueryTool";
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();
    boolean bTieneGrupo = false;

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          // ----- NUEVO -----------

          case 0:
            Inicializar(CInicializar.ESPERA);

            // realiza la consulta al servlet
            try {
              QueryTool qt = new QueryTool();

              // tabla de familias de productos
              qt.putName(nomTabla);

              // campos que se escriben
              qt.putType(nomCampoCod, tipoCampoCod);
              qt.putType(nomCampoDesc, tipoCampoDesc);
              // Valores de los campos
              qt.putValue(nomCampoCod, txtCodigo.getText().trim());
              qt.putValue(nomCampoDesc, txtDescripcion.getText().trim());
              // Valores de Data a devolver
              dtDevuelto.put(nomCampoCod, txtCodigo.getText().trim());
              dtDevuelto.put(nomCampoDesc, txtDescripcion.getText().trim());

              if (bExtra) {
                qt.putType(nomCampoExt, tipoCampoExt);
                if (chkPres.getState()) {
                  qt.putValue(nomCampoExt, "S");
                  dtDevuelto.put(nomCampoExt, "S");
                }
                else {
                  qt.putValue(nomCampoExt, "N");
                  dtDevuelto.put(nomCampoExt, "N");
                }
              }

              if (bNumerico) {
                qt.putType(nomCampoNum, tipoCampoNum);
                qt.putValue(nomCampoNum, txtDivisa.getText().trim());
                dtDevuelto.put(nomCampoNum, txtDivisa.getText().trim());
              }

              vFiltro.addElement(qt);
              this.getApp().getStub().setUrl(servlet);
              vPetic = (Lista)this.getApp().getStub().doPost(3, vFiltro);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
//                    this.getApp().trazaLog(ex);
              String sMsg = "";
              if (ex.toString().indexOf("ORA-00001") != -1) {
                sMsg = "Clave repetida; ";
              }
              if (ex.toString().indexOf("ORA-20002") != -1) {
                sMsg += "Registro padre no encontrado; ";
              }
              if (ex.toString().indexOf("ORA-01401") != -1) {
                sMsg += "Valor demasiado largo para columna; ";
              }
              if (ex.toString().indexOf("ORA-01400") != -1) {
                sMsg += "Insertando valor a nulo; ";
              }
              if (ex.toString().indexOf("ORA-02291") != -1) {
                sMsg = "Clave padre no encontrada.";
              }
              this.getApp().showError(sMsg);
              bAceptar = false;
            }
            Inicializar(CInicializar.NORMAL);
            break;
          case 1:

            // ---- MODIFICAR ----
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qt = new QueryTool();

              // tabla de familias de productos
              qt.putName(nomTabla);

              // campos que se leen
              qt.putType(nomCampoDesc, tipoCampoDesc);
              qt.putValue(nomCampoDesc, txtDescripcion.getText().trim());
              dtDevuelto.put(nomCampoDesc, txtDescripcion.getText().trim());

              qt.putWhereType(nomCampoCod, tipoCampoCod);
              qt.putWhereValue(nomCampoCod, dtDev.getString(nomCampoCod));
              dtDevuelto.put(nomCampoCod, dtDev.getString(nomCampoCod));

              if (bExtra) {
                qt.putType(nomCampoExt, tipoCampoExt);

                if (chkPres.getState()) {
                  qt.putValue(nomCampoExt, "S");
                  dtDevuelto.put(nomCampoExt, "S");
                }
                else {
                  qt.putValue(nomCampoExt, "N");
                  dtDevuelto.put(nomCampoExt, "N");
                }
              }
              if (bNumerico) {
                qt.putType(nomCampoNum, tipoCampoNum);
                qt.putValue(nomCampoNum, txtDivisa.getText().trim());
                dtDevuelto.put(nomCampoNum, txtDivisa.getText().trim());
              }

              qt.putOperator(nomCampoCod, "=");

              vFiltro.addElement(qt);

              // debug
              /*                    SrvQueryTool srv = new SrvQueryTool();
                                  // par�metros jdbc
                   srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                   "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                         "sip_desa",
                                         "sip_desa");
                                  srv.doDebug(4, vFiltro);*/

              this.getApp().getStub().setUrl(servlet);
              vPetic = (Lista)this.getApp().getStub().doPost(4, vFiltro);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
//                    this.getApp().trazaLog(ex);
              String sMsg = "";
              if (ex.toString().indexOf("ORA-00001") != -1) {
                sMsg = "Clave repetida; ";
              }
              if (ex.toString().indexOf("ORA-20002") != -1) {
                sMsg += "Registro padre no encontrado; ";
              }
              if (ex.toString().indexOf("ORA-01401") != -1) {
                sMsg += "Valor demasiado largo para columna; ";
              }
              if (ex.toString().indexOf("ORA-01400") != -1) {
                sMsg += "Insertando valor a nulo; ";
              }
              if (ex.toString().indexOf("ORA-02291") != -1) {
                sMsg = "Clave padre no encontrada.";
              }
              this.getApp().showError(sMsg);
            }
            Inicializar(CInicializar.NORMAL);
            break;
          case 2:

            // ---- BORRAR -------
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qt = new QueryTool();

              // Ponemos la siguiente comprobaci�n, para averiguar
              // si una familia tiene grupos de productos...

              if (nomTabla.equals("SIP_FAMILIA_PROD")) {
                qt.putName("SIP_GRUPO_PROD");
                qt.putType("CD_GPROD", QueryTool.STRING);
                qt.putWhereType("CD_FAMPROD", QueryTool.STRING);
                qt.putWhereValue("CD_FAMPROD", dtDev.getString(nomCampoCod));
                qt.putOperator("CD_FAMPROD", "=");
                vFiltro.addElement(qt);

                this.getApp().getStub().setUrl(servlet);
                vPetic = (Lista)this.getApp().getStub().doPost(1, vFiltro);
                if (vPetic.size() > 0) {
                  bTieneGrupo = true;

                }
                vPetic = null;
                vPetic = new Lista();
                vFiltro = null;
                vFiltro = new Lista();
                qt = null;
                qt = new QueryTool();
              }
              // Ahora en el caso de que tuviera grupos de productos, no se borra.

              if (!bTieneGrupo) {

                // tabla de familias de productos
                qt.putName(nomTabla);

                // campos que se seleccionan
                qt.putWhereType(nomCampoCod, tipoCampoCod);
                qt.putWhereValue(nomCampoCod, dtDev.getString(nomCampoCod));
                qt.putOperator(nomCampoCod, "=");

                vFiltro.addElement(qt);

                this.getApp().getStub().setUrl(servlet);
                vPetic = (Lista)this.getApp().getStub().doPost(5, vFiltro);
                bAceptar = true;
                dispose();
              }
              else {
                this.getApp().showAdvise(
                    "Esta familia de productos contiene grupos.");
              }
              // error en el servlet
            }
            catch (Exception ex) {
//                    this.getApp().trazaLog(ex);
              String sMsg = "";
              if (ex.toString().indexOf("ORA-00001") != -1) {
                sMsg = "Clave repetida; ";
              }
              if (ex.toString().indexOf("ORA-20002") != -1) {
                sMsg += "Registro padre no encontrado; ";
              }
              if (ex.toString().indexOf("ORA-01401") != -1) {
                sMsg += "Valor demasiado largo para columna; ";
              }
              if (ex.toString().indexOf("ORA-01400") != -1) {
                sMsg += "Insertando valor a nulo; ";
              }
              if (ex.toString().indexOf("ORA-02291") != -1) {
                sMsg = "Clave padre no encontrada.";
              }
              if (ex.toString().indexOf("ORA-02292") != -1) {
                sMsg =
                    "Registro hijo encontrado. No se puede borrar este elemento";
              }
              if (ex.toString().indexOf("ORA-04088") != -1) {
                sMsg = "No se puede borrar este elemento";
              }
              this.getApp().showError(sMsg);
            }
            Inicializar(CInicializar.NORMAL);
            break;
        }
      }
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // Devuelve los datos seleccionados en el di�logo.
  public Data devuelveData() {
    return dtDevuelto;
  }

// botones de aceptar y cancelar.

  class DialogActionAdapter
      implements java.awt.event.ActionListener, Runnable {
    DiaMant adaptee;
    ActionEvent e;

    DialogActionAdapter(DiaMant adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
      this.e = e;
      Thread th = new Thread(this);
      th.start();
    }

    public void run() {
      adaptee.btn_actionPerformed(e);
    }
  }

}
