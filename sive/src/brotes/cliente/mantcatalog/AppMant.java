package brotes.cliente.mantcatalog;

import capp2.CApp;
import capp2.CInicializar;

/* Implemento CInicializar, de manera que existan dos modos de espera y modo
 normal, de comportamiento*/
public class AppMant
    extends CApp { //implements CInicializar {

  PanMant pan = null;

//creo una instancia del tipo de panel padre que se va a incluir en este applet

  public void start() {
    int tmp = Integer.parseInt(getParameter("TBL_MANT"));
    switch (tmp) {
      case 10:
        setTitulo("Mantenimiento de Origenes de Muestras");
        break;
      case 20:
        setTitulo("Mantenimiento de Sintomas");
        break;
      case 30:
        setTitulo("Mantenimiento de Tipos de Alimentos");
        break;
      case 40:
        setTitulo("Mantenimiento de Tipos Implicaciones");
        break;
      case 50:
        setTitulo("Mantenimiento de Métodos de Comercialización");
        break;
      case 60:
        setTitulo("Mantenimiento de Tratamientos Previos a la Preparación");
        break;
      case 70:
        setTitulo("Mantenimiento de Formas de ingerir");
        break;
      case 80:
        setTitulo("Mantenimiento de Lugares de Contaminación");
        break;
      case 90:
        setTitulo("Mantenimiento de Lugares de Preparación");
        break;
      case 100:
        setTitulo("Mantenimiento de Lugares de Consumición");
        break;
      case 110:
        setTitulo("Mantenimiento de Métodos de Transporte");
        break;
      case 120:
        setTitulo("Mantenimiento de grupos de brotes");
        break;
//<PARAM NAME="IT_USU" VALUE="45, ,44, ,992, ,43, ,991, ,42, ,499, ,232, ,990, ,41, ,498, ,231, ,497, ,496, ,410, ,54, ,495, ,4911, ,494, ,4910, ,52, ,493, ,51, ,492, ,491, ,13, ,12, ,11, ,215, ,214, ,5, ,213, ,4, ,23, ,212, ,3, ,22, ,211, ,2, ,21, ,1, ,132, ,49, ,223, ,131, ,48, ,222, ,47, ,221, ,46, ,46, ">

    }
  }

  //public AppMant(){}???
  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {

    PanMant pan = new PanMant(this, Integer.parseInt(getParameter("TBL_MANT")));
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }

}
