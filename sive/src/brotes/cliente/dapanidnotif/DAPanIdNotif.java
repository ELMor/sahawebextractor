package brotes.cliente.dapanidnotif;

import java.util.Hashtable;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.ChoiceCDDS;
import brotes.cliente.c_componentes.DatoPeriferico;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.diaalerta.DiaAlerta;
import brotes.cliente.suca.Suca;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;

public class DAPanIdNotif
    extends CPanel
    implements CInicializar, DatoPeriferico {

  /*************** Datos propios del panel ****************/

  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;
  final int ESPERA = 4;
  final int NORMAL = 5;

  //suca
  Suca sucaNot = null;
  Suca sucaCon = null;
  Lista listaSuca = null;
  Hashtable listas = null;

  // Alerta de entrada
  Data alerta = null;

  //Listas de Datas de entrada a Suca para convertirlas en CLista
  Lista listaPaises = null;
  Lista listaCA = null;

  //tipo notificador
  Lista listaTNotif = null;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  /****************** Componentes del panel ********************/

  XYLayout xYLayout = new XYLayout();
  Label lTNotif = new Label();

  CTexto txtInst = new CTexto(50);
  ChoiceCDDS choTNotif = null;
  Label lPerNot = new Label();
  Label lInsti = new Label();
  CTexto txtPerNot = new CTexto(50);
  Label lTelNot = new Label();
  CTexto txtTelNot = new CTexto(14);
  CTexto txtTelCon = new CTexto(14);
  Label lTelCon = new Label();
  CTexto txtPerCon = new CTexto(50);
  Label lPerCon = new Label();
  XYLayout xYLayout1 = new XYLayout();

  CApp applet = null;
  DiaAlerta dlg = null;

  XYLayout xYLayout4 = new XYLayout();

  SincrEventos scr = new SincrEventos();

  // constructor
  public DAPanIdNotif(CApp a,
                      DiaAlerta d,
                      Data al,
                      int modo,
                      Hashtable hsCompleta) {

    super(a);
    applet = a;
    dlg = d;
    alerta = al;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    listas = hsCompleta;

    try {

      // Recuperacion de listas
      listaPaises = (Lista) hsCompleta.get("PAISES");
      listaCA = (Lista) hsCompleta.get("CA");
      listaTNotif = (Lista) hsCompleta.get("TNOTIFICADOR");

      jbInit();
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    boolean nulo, bCD, bDS;
    String sCD = "";
    String sDS = "";
    Color cObligatorio = new Color(255, 255, 150);

    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(270);
    xYLayout.setWidth(725);

    // Paneles suca
    sucaNot = new Suca(applet, this, scr, modoEntrada, listas);
    sucaCon = new Suca(applet, this, scr, modoEntrada, listas);

    nulo = true;
    bCD = false;
    bDS = true;
    sCD = "CD_TNOTIF";
    sDS = "DS_TNOTIF";
    lTNotif.setText("Tipo Notificador");
    choTNotif = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, listaTNotif);
    choTNotif.setBackground(cObligatorio);
    choTNotif.writeData();

    lPerNot.setForeground(Color.black);
    lPerNot.setFont(new Font("Dialog", 0, 12));
    lPerNot.setText("Persona que notifica");
    lTelNot.setForeground(Color.black);
    lTelNot.setFont(new Font("Dialog", 0, 12));
    lTelNot.setText("Telefono");
    lTelCon.setForeground(Color.black);
    lTelCon.setFont(new Font("Dialog", 0, 12));
    lTelCon.setText("Telefono");
    txtPerCon.setForeground(Color.black);
    txtPerCon.setFont(new Font("Dialog", 0, 12));
    lPerCon.setForeground(Color.black);
    lPerCon.setFont(new Font("Dialog", 0, 12));
    lPerCon.setText("T�cnico de contacto");
    lInsti.setForeground(Color.black);
    lInsti.setFont(new Font("Dialog", 0, 12));
    lInsti.setText("Institucion");
    txtPerNot.setForeground(Color.black);
    txtPerNot.setFont(new Font("Dialog", 0, 12));
    this.setLayout(xYLayout);

    this.add(lTNotif, new XYConstraints(13, 10, -1, -1));
    this.add(choTNotif, new XYConstraints(140, 10, 135, -1));
    this.add(lInsti, new XYConstraints(283, 10, 64, -1));
    this.add(txtInst, new XYConstraints(349, 10, 346, -1));

    this.add(lPerNot, new XYConstraints(13, 45, -1, -1));
    this.add(txtPerNot, new XYConstraints(140, 45, 362, -1));
    this.add(lTelNot, new XYConstraints(521, 45, -1, -1));
    this.add(txtTelNot, new XYConstraints(590, 45, 104, -1));

    this.add(sucaNot, new XYConstraints(10, 80, 596, 116));

    this.add(lPerCon, new XYConstraints(13, 205, -1, -1));
    this.add(txtPerCon, new XYConstraints(140, 205, 362, -1));
    this.add(lTelCon, new XYConstraints(521, 205, -1, -1));
    this.add(txtTelCon, new XYConstraints(590, 205, 104, -1));

    this.add(sucaCon, new XYConstraints(10, 245, 596, 116));
  }

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Inicializar()

  public void InicializarDesdeFuera(int mod) {
    modoOperacion = mod;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      // modo espera
      case CInicializar.ESPERA:
        sucaNot.InicializarDesdeFuera(CInicializar.ESPERA);
        sucaCon.InicializarDesdeFuera(CInicializar.ESPERA);
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        switch (modoEntrada) {
          case ALTA:
            enableControls(true);
            sucaNot.InicializarDesdeFuera(modoOperacion);
            sucaCon.InicializarDesdeFuera(modoOperacion);
            break;
          case MODIFICACION:
            int sit = new Integer(alerta.getString("CD_SITALERBRO")).intValue();
            switch (sit) {
              case 0:
                enableControls(true);
                sucaNot.setModoEntrada(modoEntrada);
                sucaNot.InicializarDesdeFuera(modoOperacion);
                sucaCon.setModoEntrada(modoEntrada);
                sucaCon.InicializarDesdeFuera(modoOperacion);
                break;
              case 1:
              case 2:
              case 3:
              case 4:
                enableControls(false);
                sucaNot.setModoEntrada(CONSULTA);
                sucaNot.InicializarDesdeFuera(modoOperacion);
                sucaCon.setModoEntrada(CONSULTA);
                sucaCon.InicializarDesdeFuera(modoOperacion);
                break;
            }
            break;

          case BAJA:
          case CONSULTA:
            enableControls(false);
            sucaNot.InicializarDesdeFuera(modoOperacion);
            sucaCon.InicializarDesdeFuera(modoOperacion);
            break;
        } // Fin switch modoEntrada
        break;
    } // Fin switch modoOperacion
  }

  public void enableControls(boolean state) {
    choTNotif.setEnabled(state);
    txtInst.setEnabled(state);
    txtPerCon.setEnabled(state);
    txtPerNot.setEnabled(state);
    txtTelCon.setEnabled(state);
    txtTelNot.setEnabled(state);
  }

  public void recogerDatos(Data dtDev) {
    dtDev.put("CD_TNOTIF", choTNotif.getChoiceCD());

    dtDev.put("DS_NOTIFINST", txtInst.getText().trim());
    dtDev.put("DS_NOTIFICADOR", txtPerNot.getText().trim());
    dtDev.put("DS_NOTIFTELEF", txtTelNot.getText().trim());

    // Recogida de datos de sucaNot
    Data dtNot = new Data();
    sucaNot.recogerDatos(dtNot);
    dtDev.put("CD_NOTIFCA", (String) dtNot.getString("CA"));
    dtDev.put("CD_NOTIFPROV", (String) dtNot.getString("PROV"));
    dtDev.put("CD_NOTIFMUN", (String) dtNot.getString("MUN"));
    dtDev.put("DS_NOTIFMUN", (String) dtNot.getString("DSMUN"));
    dtDev.put("DS_NOTIFDIREC", (String) dtNot.getString("VIA"));
    dtDev.put("DS_NNMCALLE", (String) dtNot.getString("NUM"));
    dtDev.put("DS_NOTIFPISO", (String) dtNot.getString("PISO"));
    dtDev.put("CD_NOTIFPOSTAL", (String) dtNot.getString("CODPOSTAL"));

    dtDev.put("DS_MINFPER", txtPerCon.getText().trim());
    dtDev.put("DS_MINFTELEF", txtTelCon.getText().trim());
    // Recogida de datos de sucaCon
    Data dtCon = new Data();
    sucaCon.recogerDatos(dtCon);
    dtDev.put("CD_MINCA", (String) dtCon.getString("CA"));
    dtDev.put("CD_MINPROV", (String) dtCon.getString("PROV"));
    dtDev.put("CD_MINMUN", (String) dtCon.getString("MUN"));
    dtDev.put("DS_MINMUN", (String) dtCon.getString("DSMUN"));
    dtDev.put("DS_MINFDIREC", (String) dtCon.getString("VIA"));
    dtDev.put("DS_MNMCALLE", (String) dtCon.getString("NUM"));
    dtDev.put("DS_MINFPISO", (String) dtCon.getString("PISO"));
    dtDev.put("CD_MINFPOSTAL", (String) dtCon.getString("CODPOSTAL"));
  }

  public boolean validarDatos() {

    if (!sucaNot.validarDatos()) {
      return false;
    }
    if (!sucaCon.validarDatos()) {
      return false;
    }

    // Tipo de Notificador es obligatorio
    if (choTNotif.getChoiceCD().equals("")) {
      this.getApp().showError("El tipo de notificador es un campo obligatorio");
      choTNotif.requestFocus();
      return false;
    }

    return true;
  }

  public void rellenarDatos(Data dtReg) {

    String cdTipoNotif = "";
    String dsTipoNotif = "";
    String instiNotif = "";
    String dsNotif = "";
    String telNotif = "";

    String cdCANotif = "";
    String dsCANotif = "";
    String cdProvNotif = "";
    String dsProvNotif = "";
    String cdMunNotif = "";
    String dsMunNotif = "";

    String dsDirecNotif = "";
    String numNotif = "";
    String pisoNotif = "";
    String cdPostalNotif = "";

    String dsContac = "";
    String telContac = "";

    String cdCAContac = "";
    String dsCAContac = "";
    String cdProvContac = "";
    String dsProvContac = "";
    String cdMunContac = "";
    String dsMunContac = "";

    String dsDirecContac = "";
    String numContac = "";
    String pisoContac = "";
    String cdPostalContac = "";

    choTNotif.selectChoiceElement(dtReg.getString("CD_TNOTIF"));

    instiNotif = dtReg.getString("DS_NOTIFINST");
    txtInst.setText(instiNotif);

    dsNotif = dtReg.getString("DS_NOTIFICADOR");
    txtPerNot.setText(dsNotif);

    telNotif = dtReg.getString("DS_NOTIFTELEF");
    txtTelNot.setText(telNotif);

    // Relleno de sucaNot
    Data dtNot = new Data();
    dtNot.put("CA", dtReg.getString("CD_CA"));
    dtNot.put("PROV", dtReg.getString("CD_NOTIFPROV"));
    dtNot.put("MUN", dtReg.getString("CD_NOTIFMUN"));
    dtNot.put("VIA", dtReg.getString("DS_NOTIFDIREC"));
    dtNot.put("NUM", dtReg.getString("DS_NNMCALLE"));
    dtNot.put("PISO", dtReg.getString("DS_NOTIFPISO"));
    dtNot.put("CODPOSTAL", dtReg.getString("CD_NOTIFPOSTAL"));
    sucaNot.rellenarDatos(dtNot);

    //mas informacion
    dsContac = dtReg.getString("DS_MINFPER");
    txtPerCon.setText(dsContac);

    telContac = dtReg.getString("DS_MINFTELEF");
    txtTelCon.setText(telContac);

    // Relleno de sucaCon
    Data dtCon = new Data();
    dtCon.put("CA", dtReg.getString("CD_CA"));
    dtCon.put("PROV", dtReg.getString("CD_MINPROV"));
    dtCon.put("MUN", dtReg.getString("CD_MINMUN"));
    dtCon.put("VIA", dtReg.getString("DS_MINFDIREC"));
    dtCon.put("NUM", dtReg.getString("DS_MNMCALLE"));
    dtCon.put("PISO", dtReg.getString("DS_MINFPISO"));
    dtCon.put("CODPOSTAL", dtReg.getString("CD_MINFPOSTAL"));
    sucaCon.rellenarDatos(dtCon);

  } //fin

  /**************** Funciones de acceso a vars. del panel *******/
  public void cambiarAlerta(Data newAlerta) {
    alerta = newAlerta;
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    modoEntrada = m;
  }

} // Fin class DAPanIdNotif
