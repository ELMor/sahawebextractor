package brotes.cliente.diaalerta;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import sapp2.Data;

public class PanDiaAlerta
    extends CPanel
    implements CInicializar {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  Hashtable listas = null;

  /*************** Constantes ****************/

  // Modo de operación y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  /****************** Componentes del panel ********************/

  // Organización del panel
  private XYLayout xYLayout1 = new XYLayout();

  // Botones
  ButtonControl btnAlta = null;
  ButtonControl btnMod = null;
  ButtonControl btnBaja = null;
  ButtonControl btnConsulta = null;

  // Escuchadores (inicializados a null)

  // Botones
  PanDiaAlertaActionAdapter actionAdapter = null;

  // Constructor
  // @param a: CApp
  // @param modo: modo de entrada al diálogo (constantes.*)
  // @param hash: listas de catálogos
  public PanDiaAlerta(CApp a, int modo, Hashtable hash) {

    super(a);
    applet = a;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    listas = hash;

    try {
      // Inicialización
      jbInit();
      Inicializar(modoOperacion);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del panel superior de alertas
  public void jbInit() throws Exception {
    // Escuchadores: botones
    actionAdapter = new PanDiaAlertaActionAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(250, 250));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(250);
    xYLayout1.setHeight(250);

    btnAlta = new ButtonControl();
    btnAlta.setLabel("Alta");
    btnAlta.setActionCommand("Alta");
    btnAlta.addActionListener(actionAdapter);

    btnMod = new ButtonControl();
    btnMod.setLabel("Modificacion");
    btnMod.setActionCommand("Modificacion");
    btnMod.addActionListener(actionAdapter);

    btnBaja = new ButtonControl();
    btnBaja.setLabel("Baja");
    btnBaja.setActionCommand("Baja");
    btnBaja.addActionListener(actionAdapter);

    btnConsulta = new ButtonControl();
    btnConsulta.setLabel("Consulta");
    btnConsulta.setActionCommand("Consulta");
    btnConsulta.addActionListener(actionAdapter);

    this.add(btnAlta, new XYConstraints(15, 15, 70, -1));
    this.add(btnMod, new XYConstraints(15, 45, 70, -1));
    this.add(btnBaja, new XYConstraints(15, 75, 70, -1));
    this.add(btnConsulta, new XYConstraints(15, 105, 70, -1));
  } // Fin jbInit()

  // Gestión del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar()

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
          case MODIFICACION:
            break;
          case BAJA:
          case CONSULTA:
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  } // Fin Inicializar()

  public Data prepararAlerta() {
    Data dtResul = new Data();

    dtResul.put("CD_ANO", "2000");
    dtResul.put("NM_ALERBRO", "15");
    dtResul.put("FC_FECHAHORA", "10/02/2000 12:12:00");
    dtResul.put("DS_ALERTA", "Alertilla1");
    dtResul.put("CD_GRUPO", "0");
    dtResul.put("CD_NIVEL_1", "1");
    dtResul.put("CD_SITALERBRO", "1");
    dtResul.put("FC_ALERBRO", "11/05/2000");
    dtResul.put("CD_OPE", "CHEMA");
    dtResul.put("FC_ULTACT", "12/05/2000 11:01:06");
    dtResul.put("CD_TNOTIF", "0");
    dtResul.put("DS_TELCOL", "");

    dtResul.put("CD_MINPROV", "28");
    dtResul.put("CD_TNOTIF", "2");
    dtResul.put("DS_NOTIFINST", "Inst");
    dtResul.put("DS_MINFPER", "P2");
    dtResul.put("CD_MINMUN", "100");
    dtResul.put("DS_MINFDIREC", "V2");
    dtResul.put("DS_MNMCALLE", "N2");
    dtResul.put("DS_MINFPISO", "P2");
    dtResul.put("CD_MINFPOSTAL", "C2");
    dtResul.put("DS_MINFTELEF", "T2");

    return dtResul;
  }

} // Fin class DAPanSup

/******************* ESCUCHADORES **********************/

// Botones
class PanDiaAlertaActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanDiaAlerta adaptee;
  ActionEvent evt;

  PanDiaAlertaActionAdapter(PanDiaAlerta adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    DiaAlerta dlg = null;

    if (evt.getActionCommand().equals("Alta")) {
      System.out.println("Alta");
      dlg = new DiaAlerta(adaptee.applet, 0, new Data(), adaptee.listas); // 0 -> ALTA
      dlg.show();
    }
    else if (evt.getActionCommand().equals("Modificacion")) {
      System.out.println("Modificacion");
      dlg = new DiaAlerta(adaptee.applet, 1, adaptee.prepararAlerta(),
                          adaptee.listas); // 1 -> MODIFICACION
      dlg.show();
    }
    else if (evt.getActionCommand().equals("Baja")) {
      System.out.println("Baja");
      dlg = new DiaAlerta(adaptee.applet, 2, adaptee.prepararAlerta(),
                          adaptee.listas); // 2 -> BAJA
      dlg.show();
    }
    else if (evt.getActionCommand().equals("Consulta")) {
      System.out.println("Consulta");
      dlg = new DiaAlerta(adaptee.applet, 3, adaptee.prepararAlerta(),
                          adaptee.listas); // 3 -> CONSULTA
      dlg.show();
    }
  }
} // endclass  PanDiaAlertaActionAdapter
