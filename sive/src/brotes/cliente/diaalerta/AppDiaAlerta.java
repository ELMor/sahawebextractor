/* Applet de prueba DiaAlerta */

package brotes.cliente.diaalerta;

import java.util.Hashtable;

import capp2.CApp;
import sapp2.Data;
import sapp2.Lista;

public class AppDiaAlerta
    extends CApp {

  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  PanDiaAlerta pan = null;
  Lista lGrupo = null;
  Lista lSit = null;
  Lista listaNot = null;
  Lista listaPa = null;
  Lista listaCA = null;

  public void init() {
    super.init();
    setTitulo("Panel Prueba DiaAlerta");
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    PrepararListas();
    Hashtable hash = new Hashtable();
    hash.put("GRUPOS", lGrupo);
    hash.put("SITUACIONES", lSit);
    hash.put("TNOTIFICADOR", listaNot);
    hash.put("PAISES", listaPa);
    hash.put("CA", listaCA);

    pan = new PanDiaAlerta(this, ALTA, hash);
    VerPanel("", pan);
  }

  private void PrepararListas() {
    // Grupos
    lGrupo = new Lista();
    Data d1 = new Data();
    d1.put("CD", "0");
    d1.put("DS", "TIA");
    Data d2 = new Data();
    d2.put("CD", "1");
    d2.put("DS", "Vacunables");
    lGrupo.addElement(d1);
    lGrupo.addElement(d2);

    // Situaciones
    lSit = new Lista();
    Data d4 = new Data();
    d4.put("CD", "0");
    d4.put("DS", "Alerta Notif");
    Data d5 = new Data();
    d5.put("CD", "1");
    d5.put("DS", "No se confirma como brote");
    lSit.addElement(d4);
    lSit.addElement(d5);

    // Tipos de Notificadores
    listaNot = new Lista();
    Data dt = null;
    dt = new Data();
    dt.put("CD", "0");
    dt.put("DS", "Colegio");
    listaNot.addElement(dt);
    dt = new Data();
    dt.put("CD", "1");
    dt.put("DS", "Trabajo");
    listaNot.addElement(dt);
    dt = new Data();
    dt.put("CD", "2");
    dt.put("DS", "Parque Recreativo");
    listaNot.addElement(dt);

    // Paises
    listaPa = new Lista();
    Data dato_ST = null;
    dato_ST = new Data();
    dato_ST.put("CD", "ESP");
    dato_ST.put("DS", "ESPA�A");
    listaPa.addElement(dato_ST);
    dato_ST = new Data();
    dato_ST.put("CD", "ALE");
    dato_ST.put("DS", "ALEMANIA");
    listaPa.addElement(dato_ST);
    dato_ST = new Data();
    dato_ST.put("CD", "BOL");
    dato_ST.put("DS", "Bolivia");
    listaPa.addElement(dato_ST);

    listaCA = new Lista();
    Data datoCA = null;
    datoCA = new Data();
    datoCA.put("CD", "02");
    datoCA.put("DS", "Aragon");
    listaCA.addElement(datoCA);
    datoCA = new Data();
    datoCA.put("CD", "13");
    datoCA.put("DS", "Madrid");
    listaCA.addElement(datoCA);
    datoCA = new Data();
    datoCA.put("CD", "01");
    datoCA.put("DS", "Andalucia");
    listaCA.addElement(datoCA);

  }

}
