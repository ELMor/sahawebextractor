package brotes.cliente.dapanhosp;

import capp2.CApp;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class AppHosBro
    extends CApp {

  DAPanHosp pan = null;

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    // query tool
    QueryTool qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", "2000");
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", "4");
    qtBloqueo.putOperator("NM_ALERBRO", "=");
    Lista l = new Lista();
    l.addElement(qtBloqueo);

    Lista vResultado = null;
    try {
      this.getStub().setUrl("servlet/SrvQueryTool");
      vResultado = (Lista)this.getStub().doPost(1, l);
    }
    catch (Exception ex) {}
    ;

    Data miData = (Data) vResultado.elementAt(0);

    String Ano = "1999";
    String NumBrote = "1";
    String Grupo = "3";
    String DescBrote = "Informe Brote 1";
    String Ope = "D_1";
    String FUlt = "03/03/2000 12:33:36";
    Data datos = new Data();
    datos.put("CD_ANO", Ano);
    datos.put("NM_ALERBRO", NumBrote);
    datos.put("DS_ALERTA", DescBrote);
    datos.put("CD_GRUPO", Grupo);
    datos.put("CD_OPE", miData.getString("CD_OPE"));
    datos.put("FC_ULTACT", FUlt); //miData.getString("FC_ULTACT"));

    setTitulo("Applet prueba tasasvacunables");
    pan = new DAPanHosp(this, pan.MODIFICACION, datos);
    pan.show();
  }
}
/*
 package brotes.cliente.dapanhosp;
 import java.awt.*;
 import java.awt.event.*;
 import capp2.*;
 import sapp2.*;
 import com.borland.jbcl.layout.*;
 import com.borland.jbcl.control.*;
 public class AppHosBro extends CApp {
  DAPanHosp pan=null;
  public AppHosBro() {
  }
  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception {
    //setTitulo("Obtenci�n de la Tasa de ataque de enfermedades vacunables");
    Data datos = new Data();
    datos.put("NM_ALERBRO", "1");
    datos.put("CD_ANO", "1999");
    datos.put("CD_GRUPO", "3");
    datos.put("DS_ALERTA", "Botulismo");
    setTitulo("Mantenimiento de Hospitales");
    DAPanHosp pan = null;
    pan = new DAPanHosp(this, pan.MODIFICACION, datos);
    pan.show();
  }
 }*/
