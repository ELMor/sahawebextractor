package brotes.cliente.mantbif;

import java.util.Calendar;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.ChoiceCDDS;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
//import brotes.servidor.alerta.*;
//import brotes.servidor.mantbif.*;
import brotes.cliente.diabif.DiaBIF;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CEntero;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CListaValores;
import capp2.CPanel;
import capp2.CTexto;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class MantBIF
    extends CPanel
    implements CInicializar, CFiltro {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 20;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

//  protected StubSrvBD stubCliente;

  // Botones
  MantBIFActionAdapter actionAdapter = null;

  // P�rdidas de foco
  MantBIFFocusAdapter focusAdapter = null;

  //Cambio en los campos de texto
  MantBIFTextAdapter textAdapter = null;

  //Cambio en los campos de opci�n (choice)
  MantBIFItemListener itemListener = null;

  //listas que se pasan a las siguientes pantallas
  protected Lista listaGrupos = new Lista();
  protected Lista listaSituacion = new Lista();
  protected Lista listaTNotificador = new Lista();
  protected Lista listaPaises = new Lista();
  protected Lista listaCA = new Lista();
  protected Lista listaCENTRO = new Lista();
  protected Lista listaTBrotes = new Lista();
  protected Lista listaMecTrans = new Lista();
  protected Lista listaTColectivo = new Lista();

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();

  //Botones
  ButtonControl btnBuscar = new ButtonControl();
  ButtonControl btnBuscInfFinal = new ButtonControl();

  //Lista de alertas
  CListaMantenimiento clmMantBIF = null;

  CApp applet = null;

  //labels
  Label lblArea = new Label();
  Label lblAno = new Label();
  Label lblCodigo = new Label();
  Label lblDescripcion = new Label();
  Label lblGrupo = new Label();
  Label lblSituacion = new Label();
  Label lblFNotifDesde = new Label();
  Label lblFNotifHasta = new Label();

  //campos
  CTexto txtArea = new CTexto(2);
  CEntero txtAno = new CEntero(4);
  CEntero txtCodigo = new CEntero(7);
  CTexto txtDescripcion = new CTexto(50);
  ChoiceCDDS chGrupo = null;
  ChoiceCDDS chSituacion = null;
  fechas.CFecha txtFNotifDesde = new fechas.CFecha("N");
  fechas.CFecha txtFNotifHasta = new fechas.CFecha("N");

  //hash q se le pasa a las siguientes pantallas.
  protected Hashtable hs = new Hashtable();

  final String strSERVLETMantbif = "servlet/SrvMantBIF";
  final String servlet = "servlet/SrvQueryTool";
  final String srvTrans = "servlet/SrvTransaccion";

  //constructor
  public MantBIF(CApp a) {
//    stubCliente=new StubSrvBD();
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    setApp(a);
    applet = a;
    try {
      Lista listaOut = Cargar_Listas();
      montarHash();
      ValidarListaSituacion();
      //Configuro los combos de Grupo y Situaci�n.
      chGrupo = new ChoiceCDDS(true, true, true, "CD_GRUPO", "DS_GRUPO",
                               listaGrupos);
      chSituacion = new ChoiceCDDS(true, true, true, "CD_SITALERBRO",
                                   "DS_SITALERBRO", listaSituacion);
      // configura la lista de alertas
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nuevo informe final",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar informe final",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "Borrar informe final",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("A�o-C�digo",
                                      "100",
                                      "ANO_CODIGO"));

      vLabels.addElement(new CColumna("Descripci�n",
                                      "175",
                                      "DS_BROTE"));

      vLabels.addElement(new CColumna("Area",
                                      "70",
                                      "CD_NIVEL_1"));

      vLabels.addElement(new CColumna("F.Notificaci�n",
                                      "125",
                                      "FC_FECHAHORA"));

      vLabels.addElement(new CColumna("Grupo",
                                      "65",
                                      "CD_GRUPO"));

      vLabels.addElement(new CColumna("Sit.",
                                      "65",
                                      "CD_SITALERBRO"));

      clmMantBIF = new CListaMantenimiento(a,
                                           vLabels,
                                           vBotones,
                                           this,
                                           this,
                                           250,
                                           640);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  //recorre listaSituacion y quita los valores:0,1,2
  void ValidarListaSituacion() {
    Lista lTemp = new Lista(); ;
    for (int i = 0; i < listaSituacion.size(); i++) {
      String sSit = ( (Data) listaSituacion.elementAt(i)).getString(
          "CD_SITALERBRO");
      if ( (! (sSit.equals("0"))) &&
          (! (sSit.equals("1"))) &&
          (! (sSit.equals("2")))) {
        lTemp.addElement( (Data) listaSituacion.elementAt(i));
      } //end if
    } //end for
    listaSituacion.removeAllElements();
    listaSituacion = null;
    listaSituacion = lTemp;
    lTemp = null;
  } //end validarlistasituacion

  public void montarHash() {
    hs.put("GRUPOS", (Lista) listaGrupos);
    hs.put("SITUACION", (Lista) listaSituacion);
    hs.put("TNOTIFICADOR", (Lista) listaTNotificador);
    hs.put("PAISES", (Lista) listaPaises);
    hs.put("CA", (Lista) listaCA);
    hs.put("TBROTES", (Lista) listaTBrotes);
    hs.put("MECTRANS", (Lista) listaMecTrans);
    hs.put("TCOLECTIVO", (Lista) listaTColectivo);

  } //end montarHash

  // sQuery = "SELECT CD_GRUPO, DS_GRUPO, DSL_GRUPO "+
  // " FROM SIVE_GRUPO_BROTE ORDER BY CD_GRUPO";
  QueryTool realizarSelGrupos() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_GRUPO_BROTE");
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("DS_GRUPO", QueryTool.STRING);
    qt.putType("DSL_GRUPO", QueryTool.STRING);
    qt.addOrderField("CD_GRUPO");
    return qt;
  }

// sQuery = "SELECT CD_SITALERBRO, DS_SITALERBRO, DSL_SITALERBRO "+
// " FROM SIVE_T_SIT_ALERTA ORDER BY CD_SITALERBRO";
  QueryTool realizarSelSituacion() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_T_SIT_ALERTA");
    qt.putType("CD_SITALERBRO", QueryTool.STRING);
    qt.putType("DS_SITALERBRO", QueryTool.STRING);
    qt.putType("DSL_SITALERBRO", QueryTool.STRING);
    qt.addOrderField("CD_SITALERBRO");
    return qt;
  }

  QueryTool realizarSelTBrote() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_TIPO_BROTE");
    // Cambios ARS 12-07-01 para recuperar el c�digo de grupo.
    qt.putType("CD_GRUPO", QueryTool.STRING);
    // Fin de cambios
    qt.putType("CD_TBROTE", QueryTool.STRING);
    qt.putType("DS_TBROTE", QueryTool.STRING);
    qt.putType("DSL_TBROTE", QueryTool.STRING);
    qt.addOrderField("CD_TBROTE");
    return qt;
  }

  QueryTool realizarSelMecTrans() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MEC_TRANS");
    qt.putType("CD_TRANSMIS", QueryTool.STRING);
    qt.putType("DS_TRANSMIS", QueryTool.STRING);
    qt.putType("DSL_TRANSMIS", QueryTool.STRING);
    qt.addOrderField("CD_TRANSMIS");
    return qt;
  }

  QueryTool realizarTCol() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_TIPOCOL");
    qt.putType("CD_TIPOCOL", QueryTool.STRING);
    qt.putType("DS_TIPOCOL", QueryTool.STRING);
    qt.putType("DSL_TIPOCOL", QueryTool.STRING);
    qt.addOrderField("CD_TIPOCOL");
    return qt;
  }

// sQuery = "SELECT  CD_TNOTIF,  DS_TNOTIF,  DSL_TNOTIF "+
// " FROM SIVE_T_NOTIFICADOR ORDER BY CD_TNOTIF";
  QueryTool realizarSelNotif() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_T_NOTIFICADOR");
    qt.putType("CD_TNOTIF", QueryTool.STRING);
    qt.putType("DS_TNOTIF", QueryTool.STRING);
    qt.putType("DSL_TNOTIF", QueryTool.STRING);
    qt.addOrderField("CD_TNOTIF");
    return qt;
  }

// sQuery = "select CD_PAIS, DS_PAIS from SIVE_PAISES";
  QueryTool realizarSelPais() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PAISES");
    qt.putType("CD_PAIS", QueryTool.STRING);
    qt.putType("DS_PAIS", QueryTool.STRING);
    return qt;
  }

// sQuery = "select CD_CA, DS_CA, DSL_CA from SIVE_COM_AUT";
  QueryTool realizarSelCa() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_COM_AUT");
    qt.putType("CD_CA", QueryTool.STRING);
    qt.putType("DS_CA", QueryTool.STRING);
    qt.putType("DSL_CA", QueryTool.STRING);
    qt.addOrderField("CD_CA");
    return qt;
  }

  protected Lista Cargar_Listas() {
    Lista listaSalida = new Lista();
    Lista lCarga = new Lista();

    //recupera los grupos
    QueryTool qtSel = new QueryTool();
    qtSel = realizarSelGrupos();
    Data dtSelGrup = new Data();
    dtSelGrup.put("1", qtSel);
    lCarga.addElement(dtSelGrup);

    //recupera las situaciones
    QueryTool qtSelSit = new QueryTool();
    qtSelSit = realizarSelSituacion();
    Data dtSelSit = new Data();
    dtSelSit.put("1", qtSelSit);
    lCarga.addElement(dtSelSit);

    //recupera los TNotificador
    QueryTool qtSelNotif = new QueryTool();
    qtSelNotif = realizarSelNotif();
    Data dtSelNotif = new Data();
    dtSelNotif.put("1", qtSelNotif);
    lCarga.addElement(dtSelNotif);

    //recupera los Paises
    QueryTool qtSelPais = new QueryTool();
    qtSelPais = realizarSelPais();
    Data dtSelPais = new Data();
    dtSelPais.put("1", qtSelPais);
    lCarga.addElement(dtSelPais);

    //recupera las CA
    QueryTool qtSelCa = new QueryTool();
    qtSelCa = realizarSelCa();
    Data dtSelCa = new Data();
    dtSelCa.put("1", qtSelCa);
    lCarga.addElement(dtSelCa);

    //recupera los tipos de brotes
    QueryTool qtSelTBrote = new QueryTool();
    qtSelTBrote = realizarSelTBrote();
    Data dtSelTBrote = new Data();
    dtSelTBrote.put("1", qtSelTBrote);
    lCarga.addElement(dtSelTBrote);

    //recupera los mecanismos probables de transmision
    QueryTool qtSelMecTrans = new QueryTool();
    qtSelMecTrans = realizarSelMecTrans();
    Data dtSelMecTrans = new Data();
    dtSelMecTrans.put("1", qtSelMecTrans);
    lCarga.addElement(dtSelMecTrans);

    //recupera los tipos de colectivo
    QueryTool qtTCol = new QueryTool();
    qtTCol = realizarTCol();
    Data dtTCol = new Data();
    dtTCol.put("1", qtTCol);
    lCarga.addElement(dtTCol);

    try {
      app.getStub().setUrl(srvTrans);
//    this.getApp().getStub().setUrl();
      listaSalida = (Lista)this.getApp().getStub().doPost(0, lCarga);

      if (listaSalida != null) {
        if (listaSalida.size() > 0) {
          listaGrupos = (Lista) listaSalida.elementAt(0);
          listaSituacion = (Lista) listaSalida.elementAt(1);
          listaTNotificador = (Lista) listaSalida.elementAt(2);
          listaPaises = (Lista) listaSalida.elementAt(3);
          listaCA = (Lista) listaSalida.elementAt(4);
          listaTBrotes = (Lista) listaSalida.elementAt(5);
          listaMecTrans = (Lista) listaSalida.elementAt(6);
          listaTColectivo = (Lista) listaSalida.elementAt(7);
        }
        else {
          //ShowWarning("No existen datos para rellenar las listas");
        }
      }
      else {
        //ShowWarning("Error al recuperar las listas");
      }

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      //ShowWarning ("Error al recuperar las listas");
    }
    return listaSalida;
  }

  void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";
    final String imgBUSCINFFINAL = "images/browser.gif";

    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().put(imgBUSCINFFINAL);
    this.getApp().getLibImagenes().CargaImagenes();

    xyLayout.setHeight(440);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    // Escuchadores: botones y p�rdidas de foco
    actionAdapter = new MantBIFActionAdapter(this);
    focusAdapter = new MantBIFFocusAdapter(this);
    textAdapter = new MantBIFTextAdapter(this);
    itemListener = new MantBIFItemListener(this);

    //campo obligatorio
    txtAno.setBackground(new Color(255, 255, 150));

    lblArea.setText("�rea:");
    lblAno.setText("A�o:");
    lblCodigo.setText("C�digo:");
    lblDescripcion.setText("Descripci�n:");
    lblGrupo.setText("Grupo:");
    lblSituacion.setText("Situaci�n:");
    lblFNotifDesde.setText("Fecha Notificaci�n desde:");
    lblFNotifHasta.setText("Hasta:");

    int iYearAct = 0;
    Calendar cal = Calendar.getInstance();
    iYearAct = cal.get(cal.YEAR);
    if (! (this.getApp().getParametro("ANYO_DEFECTO").equals(""))) {
      txtAno.setText(this.getApp().getParametro("ANYO_DEFECTO"));
    }
    else {
      txtAno.setText(new Integer(iYearAct).toString());

    }
    txtArea.addFocusListener(focusAdapter);
    txtArea.setText(getApp().getParametro("CD_NIVEL1_DEFECTO"));
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(actionAdapter);
    btnBuscInfFinal.setImage(this.getApp().getLibImagenes().get(imgBUSCINFFINAL));
    btnBuscInfFinal.addActionListener(actionAdapter);
    txtAno.addTextListener(textAdapter);
    txtArea.addTextListener(textAdapter);
    txtCodigo.addTextListener(textAdapter);
    txtDescripcion.addTextListener(textAdapter);
    chGrupo.addItemListener(itemListener);
    chSituacion.addItemListener(itemListener);
    txtFNotifDesde.addTextListener(textAdapter);
    txtFNotifHasta.addTextListener(textAdapter);

    btnBuscar.setActionCommand("Buscar");
    btnBuscInfFinal.setActionCommand("BuscarInfFinal");
    txtArea.setName("Area");
    txtAno.setName("Ano");
    txtCodigo.setName("Codigo");
    txtDescripcion.setName("Descripcion");
    chGrupo.setName("Grupo");
    chSituacion.setName("Situacion");
    txtFNotifDesde.setName("FechaNotificacionDesde");
    txtFNotifHasta.setName("FechaNotificacionHasta");

    chGrupo.writeData();
    chSituacion.writeData();

    // a�ade los componentes
    this.add(lblArea, new XYConstraints(MARGENIZQ, MARGENSUP, 40, ALTO));
    this.add(txtArea,
             new XYConstraints(MARGENIZQ + 40 + INTERHOR, MARGENSUP, 50, ALTO));
    this.add(btnBuscInfFinal,
             new XYConstraints(MARGENIZQ + 40 + 50 + 2 * INTERHOR, MARGENSUP,
                               25, -1));
    this.add(lblAno,
             new XYConstraints(MARGENIZQ + 40 + 50 + 25 + 3 * INTERHOR, MARGENSUP,
                               40, ALTO));
    this.add(txtAno,
             new XYConstraints(MARGENIZQ + 50 + 2 * 40 + 25 + 4 * INTERHOR,
                               MARGENSUP, 50, ALTO));
    this.add(lblCodigo,
             new XYConstraints(MARGENIZQ + 2 * 50 + 2 * 40 + 25 + 5 * INTERHOR,
                               MARGENSUP, 55, ALTO));
    this.add(txtCodigo,
             new XYConstraints(MARGENIZQ + 2 * 50 + 2 * 40 + 55 + 25 +
                               6 * INTERHOR, MARGENSUP, 60, ALTO));
    this.add(lblDescripcion,
             new XYConstraints(MARGENIZQ + 40 + 3 * 50 + 55 + 60 + 25 +
                               7 * INTERHOR, MARGENSUP, 70, ALTO));
    this.add(txtDescripcion,
             new XYConstraints(MARGENIZQ + 40 + 3 * 50 + 55 + 60 + 25 + 70 +
                               8 * INTERHOR, MARGENSUP, 130, ALTO));

    this.add(lblGrupo,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 40,
                               ALTO));
    this.add(chGrupo,
             new XYConstraints(MARGENIZQ + 40 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 198, ALTO));

    this.add(lblSituacion,
             new XYConstraints(MARGENIZQ + 45 + 198 + 2 * INTERHOR - 8,
                               MARGENSUP + ALTO + INTERVERT, 65, ALTO));
    this.add(chSituacion,
             new XYConstraints(MARGENIZQ + 2 * 50 + 2 * 40 + 55 + 25 +
                               6 * INTERHOR, MARGENSUP + ALTO + INTERVERT, 250,
                               ALTO));

    this.add(lblFNotifDesde,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERVERT,
                               145, ALTO));
    this.add(txtFNotifDesde,
             new XYConstraints(MARGENIZQ + 40 + INTERHOR + 198 - 100,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 100, ALTO));
    this.add(lblFNotifHasta,
             new XYConstraints(MARGENIZQ + 45 + 198 + 2 * INTERHOR - 8,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 35, ALTO));
    this.add(txtFNotifHasta,
             new XYConstraints(MARGENIZQ + 2 * 50 + 2 * 40 + 55 + 25 +
                               6 * INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 100, ALTO));

    this.add(btnBuscar,
             new XYConstraints(535, MARGENSUP + 3 * ALTO + 3 * INTERVERT, 80,
                               ALTO));
    this.add(clmMantBIF,
             new XYConstraints(MARGENIZQ - 10,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT, 650, 230));

  } //end jbInit

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        clmMantBIF.setEnabled(true);
    }
  } //end Inicializar

  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Data datos = new Data();
    if (isDataValid()) {
      try {
        datos.put("CD_USUARIO", this.getApp().getParametro("LOGIN"));
        datos.put("CD_ANO", txtAno.getText().trim());
        datos.put("NM_ALERBRO", txtCodigo.getText().trim());
        datos.put("DS_BROTE", txtDescripcion.getText().trim());
        String fechaDesde = null;
        if (txtFNotifDesde.getText().trim().length() != 0) {
          fechaDesde = txtFNotifDesde.getText().trim() + " 00:00:00";
        }
        else {
          fechaDesde = "";
        }
        datos.put("FC_FECHAHORADESDE", fechaDesde);
        String fechaHasta = null;
        if (txtFNotifHasta.getText().trim().length() != 0) {
          fechaHasta = txtFNotifHasta.getText().trim() + " 00:00:00";
        }
        else {
          fechaHasta = "";
        }
        datos.put("FC_FECHAHORAHASTA", fechaHasta);
        datos.put("CD_GRUPO", chGrupo.getChoiceCD());
        datos.put("CD_NIVEL_1", txtArea.getText().trim());
        datos.put("CD_SITALERBRO", chSituacion.getChoiceCD());
        this.getApp().getStub().setUrl(strSERVLETMantbif);
        p.addElement(datos);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), strSERVLETMantbif, 0, p);
        /*      SrvMantBIF serv=new SrvMantBIF();
              serv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
              p1 = (Lista) serv.doDebug(0,p);*/
        if (p1.size() == 0) {
          this.getApp().showAdvise(
              "No existen elementos con los criterios especificados");
        }
        else {
          p1 = anadirCampo(p1);
        }
        /*            SrvModPregCent servlet = new SrvModPregCent();
             //Indica como conectarse a la b.datos
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                             "sive_desa",
                             "sive_desa");
              data = (CLista) servlet.doDebug(this.modoVOLCADO, data);
         */
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //end isdatavalid
    return p1;

    /*    Lista p = new Lista();
         Lista p1= new Lista();
      try{
        QueryTool2 qt = new QueryTool2();
        // Nombre de la tabla (FROM)
        qt.putName("SIVE_BROTES");
        // Campos que se quieren leer (SELECT)
        qt.putType("CD_ANO",QueryTool2.STRING);
        qt.putType("NM_ALERBRO",QueryTool2.INTEGER);
        qt.putType("DS_BROTE",QueryTool2.STRING);
        qt.putType("CD_GRUPO",QueryTool2.STRING);
        qt.putType("FC_FECHAHORA",QueryTool2.TIMESTAMP);
//        qt.putType("CD_SITALERBRO",QueryTool2.STRING);
//        qt.putType("CD_NIVEL_1",QueryTool2.STRING);
        if (!(txtAno.getText().equals(""))){
          qt=obtenerAno(qt);
        }
        if (!(txtArea.getText().equals(""))){
          qt=obtenerArea(qt);
        }
        if (!(txtCodigo.getText().equals(""))){
          qt=obtenerCodigo(qt);
        }
        if (!(txtDescripcion.getText().equals(""))){
          qt=obtenerDescripcion(qt);
        }
        if (!(txtFNotifDesde.getText().equals(""))){
          qt=obtenerFNotifDesde(qt);
        }
        if (!(txtFNotifHasta.getText().equals(""))){
          qt=obtenerFNotifHasta(qt);
        }
        if (!(chGrupo.getChoiceCD().equals(""))){
          qt=obtenerGrupo(qt);
        }
        if (!(chSituacion.getChoiceCD().equals(""))){
          qt=obtenerSituacion(qt);
        }
        //si usuario es de Area-> limitado a sus �reas.
        if (this.getApp().getParametro("PERFIL").equals("3")){
          qt=anadirSubquery(qt);
        }
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_ALERTA_BROTES");
        qtAdic1.putType("CD_SITALERBRO",QueryTool.STRING);
        qtAdic1.putType("CD_NIVEL_1",QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_ANO", QueryTool.STRING);
        dtAdic1.put("NM_ALERBRO", QueryTool.INTEGER);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);
        //"ORDER BY CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC";
        qt.addOrderField("CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC");
        p.addElement(qt);
        app.getStub().setUrl(servlet);
        p1 = (Lista) app.getStub().doPost(1,p);
            SrvQueryTool serv = new SrvQueryTool();
           //Indica como conectarse a la b.datos
             serv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
            p1 = (Lista) serv.doDebug(1,p);
        if (p1.size()==0) {
            this.getApp().showAdvise("No existen elementos con los criterios especificados");
          }else{
            p1=anadirCampo(p1);
          }
      } catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
         return p1;*/
  } //end primera pagina

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Data datos = new Data();
    if (isDataValid()) {
      try {
        datos.put("CD_USUARIO", this.getApp().getParametro("LOGIN"));
        datos.put("CD_ANO", txtAno.getText().trim());
        datos.put("NM_ALERBRO", txtCodigo.getText().trim());
        datos.put("DS_BROTE", txtDescripcion.getText().trim());
        datos.put("FC_FECHAHORADESDE", txtFNotifDesde.getText().trim());
        datos.put("FC_FECHAHORAHASTA", txtFNotifHasta.getText().trim());
        datos.put("CD_GRUPO", chGrupo.getChoiceCD());
        datos.put("CD_NIVEL_1", txtArea.getText().trim());
        datos.put("CD_SITALERBRO", chSituacion.getChoiceCD());
        this.getApp().getStub().setUrl(strSERVLETMantbif);
        p.addElement(datos);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), strSERVLETMantbif, 0, p);
        /*      SrvMantBIF serv=new SrvMantBIF();
              serv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
              p1 = (Lista) serv.doDebug(0,p);*/
        if (p1.size() == 0) {
          this.getApp().showAdvise(
              "No existen elementos con los criterios especificados");
        }
        else {
          p1 = anadirCampo(p1);
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //end isdatavalid
    /*    stubCliente.setUrl(new URL(app.getURL() + strSERVLETMantbif));
         p1=(Lista)this.stubCliente.doPost(0,datos);*/
    return p1;

    /*    Lista p = new Lista();
        Lista p1= new Lista();
//    Inicializar(CInicializar.ESPERA);
          try{
            QueryTool2 qt = new QueryTool2();
            // Nombre de la tabla (FROM)
            qt.putName("SIVE_BROTES");
            // Campos que se quieren leer (SELECT)
            qt.putType("CD_ANO",QueryTool2.STRING);
            qt.putType("NM_ALERBRO",QueryTool2.INTEGER);
            qt.putType("DS_BROTE",QueryTool2.STRING);
            qt.putType("CD_GRUPO",QueryTool2.STRING);
            qt.putType("FC_FECHAHORA",QueryTool2.TIMESTAMP);
//        qt.putType("CD_SITALERBRO",QueryTool2.STRING);
//        qt.putType("CD_NIVEL_1",QueryTool2.STRING);
            if (!(txtAno.getText().equals(""))){
              qt=obtenerAno(qt);
            }
            if (!(txtArea.getText().equals(""))){
              qt=obtenerArea(qt);
            }
            if (!(txtCodigo.getText().equals(""))){
              qt=obtenerCodigo(qt);
            }
            if (!(txtDescripcion.getText().equals(""))){
              qt=obtenerDescripcion(qt);
            }
            if (!(txtFNotifDesde.getText().equals(""))){
              qt=obtenerFNotifDesde(qt);
            }
            if (!(txtFNotifHasta.getText().equals(""))){
              qt=obtenerFNotifHasta(qt);
            }
            if (!(chGrupo.getChoiceCD().equals(""))){
              qt=obtenerGrupo(qt);
            }
            if (!(chSituacion.getChoiceCD().equals(""))){
              qt=obtenerSituacion(qt);
            }
            //si usuario es de Area-> limitado a sus �reas.
            if (this.getApp().getParametro("PERFIL").equals("3")){
              qt=anadirSubquery(qt);
            }
            QueryTool qtAdic1 = new QueryTool();
            qtAdic1.putName("SIVE_ALERTA_BROTES");
            qtAdic1.putType("CD_SITALERBRO",QueryTool.STRING);
            qtAdic1.putType("CD_NIVEL_1",QueryTool.STRING);
            Data dtAdic1 = new Data();
            dtAdic1.put("CD_ANO", QueryTool.STRING);
            dtAdic1.put("NM_ALERBRO", QueryTool.INTEGER);
            qt.addQueryTool(qtAdic1);
            qt.addColumnsQueryTool(dtAdic1);
            //"ORDER BY CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC";
         qt.addOrderField("CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC");
            p.addElement(qt);
            app.getStub().setUrl(servlet);
            p1 = (Lista) app.getStub().doPost(1,p);
            if (p1.size()==0) {
                this.getApp().showAdvise("No existen elementos con los criterios especificados");
              }else{
                p1=anadirCampo(p1);
              }
          } catch (Exception ex) {
            this.getApp().trazaLog(ex);
          }
        return p1;*/
  } //end siguiente pagina

  public Data busquedaDatos(Data dt) {
    Data envDatos = null;
    Lista p = new Lista();
    Lista p1 = new Lista();

    Data datosAlerta = null;
    Lista pA = new Lista();
    Lista p1A = new Lista();

    try {
      QueryTool2 qt = new QueryTool2();

      // Nombre de la tabla (FROM)
      qt.putName("SIVE_BROTES");

      // Campos que se quieren leer (SELECT)
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("CD_TIPOCOL", QueryTool.STRING);
      qt.putType("CD_TNOTIF", QueryTool.STRING);
      qt.putType("CD_TRANSMIS", QueryTool.STRING);
      qt.putType("CD_GRUPO", QueryTool.STRING);
      qt.putType("CD_TBROTE", QueryTool.STRING);
      qt.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
      qt.putType("DS_BROTE", QueryTool.STRING);
      qt.putType("DS_NOMCOL", QueryTool.STRING);
      qt.putType("DS_DIRCOL", QueryTool.STRING);
      qt.putType("CD_POSTALCOL", QueryTool.STRING);
      qt.putType("CD_PROVCOL", QueryTool.STRING);
      qt.putType("CD_MUNCOL", QueryTool.STRING);
      qt.putType("DS_NMCALLE", QueryTool.STRING);
      qt.putType("DS_PISOCOL", QueryTool.STRING);
      qt.putType("DS_TELCOL", QueryTool.STRING);
      qt.putType("CD_NIVEL_1_LCA", QueryTool.STRING);
      qt.putType("CD_NIVEL_2_LCA", QueryTool.STRING);
      qt.putType("CD_ZBS_LCA", QueryTool.STRING);
      qt.putType("CD_NIVEL_1_LE", QueryTool.STRING);
      qt.putType("CD_NIVEL_2_LE", QueryTool.STRING);
      qt.putType("CD_ZBS_LE", QueryTool.STRING);
      qt.putType("NM_MANIPUL", QueryTool.INTEGER);
      qt.putType("IT_RESCALC", QueryTool.STRING);
      qt.putType("NM_EXPUESTOS", QueryTool.INTEGER);
      qt.putType("NM_ENFERMOS", QueryTool.INTEGER);
      qt.putType("NM_INGHOSP", QueryTool.INTEGER);
      qt.putType("NM_DEFUNCION", QueryTool.INTEGER);
      qt.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
      qt.putType("FC_ISINPRIMC", QueryTool.TIMESTAMP);
      qt.putType("FC_FSINPRIMC", QueryTool.TIMESTAMP);
      qt.putType("NM_PERINMIN", QueryTool.INTEGER);
      qt.putType("NM_PERINMAX", QueryTool.INTEGER);
      qt.putType("NM_PERINMED", QueryTool.REAL);
      qt.putType("NM_DCUACMIN", QueryTool.INTEGER);
      qt.putType("NM_DCUACMAX", QueryTool.INTEGER);
      qt.putType("NM_DCUACMED", QueryTool.REAL);
      qt.putType("NM_VACNENF", QueryTool.INTEGER);
      qt.putType("NM_VACENF", QueryTool.INTEGER);
      qt.putType("NM_NVACNENF", QueryTool.INTEGER);
      qt.putType("NM_NVACENF", QueryTool.INTEGER);
      qt.putType("DS_OBSERV", QueryTool.STRING);
      qt.putType("IT_PERIN", QueryTool.STRING);
      qt.putType("IT_DCUAC", QueryTool.STRING);
      qt.putType("CD_OPE", QueryTool.STRING);
      qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dt.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", dt.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      p.addElement(qt);
      app.getStub().setUrl(servlet);
      p1 = (Lista) app.getStub().doPost(1, p);

      // Tambien traemos CD_SITALERBRO y FC_ALERBRO de SIVE_ALERTA_BROTES

      QueryTool qtA = new QueryTool();

      // Nombre de la tabla (FROM)
      qtA.putName("SIVE_ALERTA_BROTES");

      qtA.putType("CD_SITALERBRO", QueryTool.STRING);
      qtA.putType("FC_ALERBRO", QueryTool.DATE);
      qtA.putType("CD_NIVEL_1", QueryTool.STRING);

      qtA.putWhereType("CD_ANO", QueryTool.STRING);
      qtA.putWhereValue("CD_ANO", dt.getString("CD_ANO"));
      qtA.putOperator("CD_ANO", "=");

      qtA.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qtA.putWhereValue("NM_ALERBRO", dt.getString("NM_ALERBRO"));
      qtA.putOperator("NM_ALERBRO", "=");

      pA.addElement(qtA);
      app.getStub().setUrl(servlet);
      p1A = (Lista) app.getStub().doPost(1, pA);

      envDatos = (Data) p1.elementAt(0);
      datosAlerta = (Data) p1A.elementAt(0);

      envDatos.put("CD_SITALERBRO", datosAlerta.get("CD_SITALERBRO"));
      envDatos.put("FC_ALERBRO", datosAlerta.get("FC_ALERBRO"));
      envDatos.put("CD_NIVEL_1", datosAlerta.get("CD_NIVEL_1"));

      // ***********************************************************
      // Traemos las descripciones de la Zona B�sica de Salud ******
      // ***********************************************************

      QueryTool otraQt = new QueryTool();
      Lista e = new Lista();
      Lista s = new Lista();

      otraQt.putName("SIVE_ZONA_BASICA");
      otraQt.putType("DS_ZBS", QueryTool.STRING);

      otraQt.putWhereType("CD_NIVEL_1", QueryTool.STRING);
      otraQt.putWhereValue("CD_NIVEL_1", envDatos.getString("CD_NIVEL_1_LCA"));
      otraQt.putOperator("CD_NIVEL_1", "=");
      otraQt.putWhereType("CD_NIVEL_2", QueryTool.STRING);
      otraQt.putWhereValue("CD_NIVEL_2", envDatos.getString("CD_NIVEL_2_LCA"));
      otraQt.putOperator("CD_NIVEL_2", "=");
      otraQt.putWhereType("CD_ZBS", QueryTool.STRING);
      otraQt.putWhereValue("CD_ZBS", envDatos.getString("CD_ZBS_LCA"));
      otraQt.putOperator("CD_ZBS", "=");

      e.addElement(otraQt);
      app.getStub().setUrl(servlet);
      s = (Lista) app.getStub().doPost(1, e);

      Data dtTmp = null;

      // 30-03-01 (ARS) Si hay dato, lo recogemos, si no, no hagas nada.
      if ( (s != null) && (s.size() > 0)) {
        dtTmp = (Data) s.elementAt(0);
      }
      else {
        dtTmp = new Data();

        // Ahora meto el nuevo dato en el Data ese.
      }
      envDatos.put("DS_ZBS_LCA", dtTmp.getString("DS_ZBS"));

      // Descripciones de la Zona B�sica de Salud ******
      // Para otro caso.

      otraQt = null;
      e = null;
      s = null;

      otraQt = new QueryTool();
      e = new Lista();
      s = new Lista();

      otraQt.putName("SIVE_ZONA_BASICA");
      otraQt.putType("DS_ZBS", QueryTool.STRING);

      otraQt.putWhereType("CD_NIVEL_1", QueryTool.STRING);
      otraQt.putWhereValue("CD_NIVEL_1", envDatos.getString("CD_NIVEL_1_LE"));
      otraQt.putOperator("CD_NIVEL_1", "=");
      otraQt.putWhereType("CD_NIVEL_2", QueryTool.STRING);
      otraQt.putWhereValue("CD_NIVEL_2", envDatos.getString("CD_NIVEL_2_LE"));
      otraQt.putOperator("CD_NIVEL_2", "=");
      otraQt.putWhereType("CD_ZBS", QueryTool.STRING);
      otraQt.putWhereValue("CD_ZBS", envDatos.getString("CD_ZBS_LE"));
      otraQt.putOperator("CD_ZBS", "=");

      e.addElement(otraQt);
      app.getStub().setUrl(servlet);
      s = (Lista) app.getStub().doPost(1, e);

      dtTmp = null;
      // 30-03-01 (ARS) Si hay dato, lo recogemos, si no, no hagas nada.
      if ( (s != null) && (s.size() > 0)) {
        dtTmp = (Data) s.elementAt(0);
      }
      else {
        dtTmp = new Data();

        // Ahora meto el nuevo dato en el Data ese.
      }
      envDatos.put("DS_ZBS_LE", dtTmp.getString("DS_ZBS"));

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }

    return envDatos;
  }

  public void realizaOperacion(int j) {
    DiaBIF di = null;
    Data envDatos = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        di = new DiaBIF(this.getApp(), ALTA, envDatos, hs);
        di.show();
        // a�adir el nuevo elem a la lista
//        if (di.bAceptar) {
        clmMantBIF.setPrimeraPagina(primeraPagina());
//        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        Data dMantBIFModif = clmMantBIF.getSelected();
        //si existe alguna fila seleccionada
        if (dMantBIFModif != null) {
          int ind = clmMantBIF.getSelectedIndex();
          envDatos = busquedaDatos(dMantBIFModif);
          envDatos.put("CD_CA", applet.getParametro("CA"));
          di = new DiaBIF(this.getApp(), MODIFICACION, envDatos, hs);
          di.show();
          // Tratamiento de bAceptar para Salir
//          if (di.bAceptar){
          clmMantBIF.setPrimeraPagina(primeraPagina());
//          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        Data dMantBIFBaja = clmMantBIF.getSelected();
        if (dMantBIFBaja != null) {
          if (!dMantBIFBaja.getString("CD_SITALERBRO").equals("3")) {
            this.getApp().showError(
                "Solo se puede dar de baja brotes en estado 3");
          }
          else {
            int ind = clmMantBIF.getSelectedIndex();
            envDatos = busquedaDatos(dMantBIFBaja);
            di = new DiaBIF(this.getApp(), BAJA, envDatos, hs);
            di.show();
//          if (di.bAceptar){
            clmMantBIF.getLista().removeElementAt(ind);
            clmMantBIF.setPrimeraPagina(primeraPagina());
//          }
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;
    }
  } //end realizaOperacion

  void btnBuscarActionPerformed() {
    clmMantBIF.setPrimeraPagina(this.primeraPagina());
  } //end btnBuscar

  void btnBuscInfFinalActionPerformed() {
    seleccionarAreas();
  }

  QueryTool2 anadirSubquery(QueryTool2 qt) {
    String subquery = "";
    Vector vSubquery = new Vector();

    String autoriza = this.getApp().getParametro("CD_NIVEL_1_AUTORIZACIONES");
    //si tiene autorizaciones para alg�n �rea.
    if (! (autoriza.equals(""))) {
      Data dtAutoriza = new Data();
      StringTokenizer stAutor = new StringTokenizer(autoriza, ",", false);
      for (; stAutor.hasMoreTokens(); ) {
        String autorizacion = stAutor.nextToken();
        Data dtAutVect = new Data();
        subquery += "?,";
        dtAutVect.put(new Integer(QueryTool2.STRING), autorizacion);
        vSubquery.addElement(dtAutVect);
      } //end for
      subquery = subquery.substring(0, subquery.length() - 1); // Quita la ultima coma
      // Establecemos la subquery y sus (tipos,valores)
      qt.putSubquery("CD_NIVEL_1", subquery);
      qt.putVectorSubquery("CD_NIVEL_1", vSubquery);
    } //end si existen autorizaciones.
    return qt;
  } //end anadirSubquery

  void seleccionarAreas() {
    CListaValores clv = null;
    Vector vCod = null;
    QueryTool2 qt = new QueryTool2(); ;
    // "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 IN (?,...)";

    qt.putName("SIVE_NIVEL1_S");
    qt.putType("CD_NIVEL_1", QueryTool2.STRING);
    qt.putType("DS_NIVEL_1", QueryTool2.STRING);
    /*    if (this.getApp().getParametro("PERFIL").equals("3")){
            qt=anadirSubquery(qt);
        }*/

    // campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement("CD_NIVEL_1");
    vCod.addElement("DS_NIVEL_1");

    // lista de valores
    clv = new CListaValores(this.getApp(),
                            "Alertas",
                            qt,
                            vCod);
    clv.show();

    // recupera el centro seleccionado
    if (clv.getSelected() != null) {
      txtArea.setText( ( (String) clv.getSelected().get("CD_NIVEL_1")));
    }

    clv = null;
  } //end seleccionarAreas

  QueryTool2 obtenerAno(QueryTool2 qt) {
    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");
    return qt;
  }

  /*  QueryTool2 obtenerArea(QueryTool2 qt){
      qt.putWhereType("CD_NIVEL_1",QueryTool.STRING);
      qt.putWhereValue("CD_NIVEL_1",txtArea.getText().trim());
      qt.putOperator("CD_NIVEL_1","=");
      return qt;
    }*/

  QueryTool2 obtenerCodigo(QueryTool2 qt) {
    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCodigo.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");
    return qt;
  }

  QueryTool2 obtenerDescripcion(QueryTool2 qt) {
    qt.putWhereType("DS_ALERTA", QueryTool.STRING);
    qt.putWhereValue("DS_ALERTA", txtDescripcion.getText().trim());
    qt.putOperator("DS_ALERTA", "=");
    return qt;
  }

  QueryTool2 obtenerFNotifDesde(QueryTool2 qt) {
    String fechaDesde = txtFNotifDesde.getText().trim() + " 00:00:00";
    qt.putWhereType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qt.putWhereValue("FC_FECHAHORA", fechaDesde);
    qt.putOperator("FC_FECHAHORA", ">");
    return qt;
  }

  QueryTool2 obtenerFNotifHasta(QueryTool2 qt) {
    String fechaHasta = txtFNotifHasta.getText().trim() + " 00:00:00";
    qt.putWhereType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qt.putWhereValue("FC_FECHAHORA", fechaHasta);
    qt.putOperator("FC_FECHAHORA", "<");
    return qt;
  }

  QueryTool2 obtenerGrupo(QueryTool2 qt) {
    qt.putWhereType("CD_GRUPO", QueryTool.STRING);
    qt.putWhereValue("CD_GRUPO", chGrupo.getChoiceCD());
    qt.putOperator("CD_GRUPO", "=");
    return qt;
  }

  /*  QueryTool2 obtenerSituacion(QueryTool2 qt){
      qt.putWhereType("CD_SITALERBRO",QueryTool.STRING);
      qt.putWhereValue("CD_SITALERBRO",chSituacion.getChoiceCD());
      qt.putOperator("CD_SITALERBRO","=");
      return qt;
    }*/

  boolean isDataValid() {
    boolean b = true;
    if (txtFNotifDesde.getText().trim().length() != 0) {
      txtFNotifDesde.ValidarFecha();
      if (txtFNotifDesde.getValid().equals("N")) {
        this.getApp().showError("Fecha Desde incorrecta");
        b = false;
      }
    }
    if (b == true) {
      if (txtFNotifHasta.getText().trim().length() != 0) {
        txtFNotifHasta.ValidarFecha();
        if (txtFNotifHasta.getValid().equals("N")) {
          this.getApp().showError("Fecha Hasta incorrecta");
          b = false;
        }
      }
    }

    if (b == true) {
      if (txtAno.getText().length() == 0) {
        this.getApp().showError("Debe introducir un a�o");
        b = false;
      }
      else {
        b = true;
      }
    }
    return b;
  }

  //concatena a�o y c�digo de brote
  Lista anadirCampo(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = new Data();
      dCampo = (Data) lCampo.elementAt(i);
      String s = new String();
      s = dCampo.getString("CD_ANO") + "-" + dCampo.getString("NM_ALERBRO");
      ( (Data) lCampo.elementAt(i)).put("ANO_CODIGO", s);
    }
    return lCampo;
  }

  boolean buscarAreaGlobal(String cdArea) {
    boolean resul = false;
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool2 qt = new QueryTool2(); ;
    try {
      qt.putName("SIVE_NIVEL1_S");
      qt.putType("CD_NIVEL_1", QueryTool2.STRING);
      qt.putWhereType("CD_NIVEL_1", QueryTool.STRING);
      qt.putWhereValue("CD_NIVEL_1", cdArea);
      qt.putOperator("CD_NIVEL_1", "=");
      p.addElement(qt);
      app.getStub().setUrl(servlet);
      p1 = (Lista) app.getStub().doPost(1, p);
      if (p1.size() == 0) {
        resul = false;
      }
      else {
        resul = true;
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return resul;
  } //end buscarAreaGlobal

  /*  boolean buscarAreaUsuario(String cdArea){
      boolean resul=false;
      boolean salir=false;
      String autoriza=this.getApp().getParametro("CD_NIVEL_1_AUTORIZACIONES");
      //si tiene autorizaciones para alg�n �rea.
      if (!(autoriza.equals(""))){
        Data dtAutoriza=new Data();
        StringTokenizer stAutor=new StringTokenizer(autoriza,",",false);
        for(;stAutor.hasMoreTokens()&& salir==false;){
          String autorizacion= stAutor.nextToken();
          //si el �rea introducida es una de las autorizadas
          if (cdArea.equals(autorizacion)){
                  salir=true;
                  resul=true;
          }
        }//end for
      }//end if tiene alguna autorizaci�n
      return resul;
    }//end buscarAreaUsuario*/

  //al perder el foco se valida q area introducida exista en BD
  void txtAreaFocusLost() {
    String area = txtArea.getText().trim();
    if (area.length() != 0) {
      boolean b = buscarAreaGlobal(area);
      if (b == false) {
        this.getApp().showAdvise("�rea no existe");
        txtArea.setText("");
      } //end b==false
    } //end if area.length
  } //end txtArea_focuslost

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  void txtAnoTextValueChanged() {
    clmMantBIF.vaciarPantalla();
  }

  void txtAreaTextValueChanged() {
    clmMantBIF.vaciarPantalla();
  }

  void txtCodigoTextValueChanged() {
    clmMantBIF.vaciarPantalla();
  }

  void txtDescripcionTextValueChanged() {
    clmMantBIF.vaciarPantalla();
  }

  void txtFNotifDesdeTextValueChanged() {
    clmMantBIF.vaciarPantalla();
  }

  void txtFNotifHastaTextValueChanged() {
    clmMantBIF.vaciarPantalla();
  }

  void chGrupoItemStateChanged() {
    clmMantBIF.vaciarPantalla();
  }

  void chSituacionItemStateChanged() {
    clmMantBIF.vaciarPantalla();
  }

} //end class

// Botones
class MantBIFActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  MantBIF adaptee;
  ActionEvent evt;

  MantBIFActionAdapter(MantBIF adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("BuscarInfFinal")) {
        adaptee.btnBuscInfFinalActionPerformed();
        //adaptee.btnAreaActionPerformed();
      }
      if (evt.getActionCommand().equals("Buscar")) {
        adaptee.btnBuscarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  MantBIFActionAdapter

// P�rdidas de foco
class MantBIFFocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  MantBIF adaptee;
  FocusEvent evt;

  MantBIFFocusAdapter(MantBIF adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("Area")) {
        adaptee.txtAreaFocusLost();
        //adaptee.txtAreaFocusLost();
      }
      s.desbloquea(adaptee);
    }
  }
} // Fin clase MantBIFFocusAdapter

class MantBIFTextAdapter
    implements java.awt.event.TextListener, Runnable {
  MantBIF adaptee;
  TextEvent evt;

  MantBIFTextAdapter(MantBIF adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("Ano")) {
        adaptee.txtAnoTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals("Area")) {
        adaptee.txtAreaTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals("Descripcion")) {
        adaptee.txtDescripcionTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals("Codigo")) {
        adaptee.txtCodigoTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals(
          "FechaNotificacionDesde")) {
        adaptee.txtFNotifDesdeTextValueChanged();
      }
      if ( ( (TextField) evt.getSource()).getName().equals(
          "FechaNotificacionHasta")) {
        adaptee.txtFNotifHastaTextValueChanged();
      }
      s.desbloquea(adaptee);
    }
  }

}

class MantBIFItemListener
    implements java.awt.event.ItemListener, Runnable {
  MantBIF adaptee;
  ItemEvent evt;

  MantBIFItemListener(MantBIF adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (Choice) evt.getSource()).getName().equals("Grupo")) {
        adaptee.chGrupoItemStateChanged();
      }
      if ( ( (Choice) evt.getSource()).getName().equals("Situacion")) {
        adaptee.chSituacionItemStateChanged();
      }
      s.desbloquea(adaptee);
    }
  }
}
