package brotes.cliente.mantbif;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantBIF
    extends CApp
    implements CInicializar {
  MantBIF pan = null;

  public AppMantBIF() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento de Informes finales");
    pan = new MantBIF(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }
}
