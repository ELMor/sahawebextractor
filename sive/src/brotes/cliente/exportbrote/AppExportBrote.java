package brotes.cliente.exportbrote;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppExportBrote
    extends CApp
    implements CInicializar {

  PanExportBrote pan = null;

  public AppExportBrote() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Volcado a fichero de la información general del brote");
    pan = new PanExportBrote(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(false);
        break;
    }
  }
}
