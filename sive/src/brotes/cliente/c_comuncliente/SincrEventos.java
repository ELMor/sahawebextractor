package brotes.cliente.c_comuncliente;

import capp2.CInicializar;

public class SincrEventos {

  public SincrEventos() {
  }

  private boolean sinBloquear = true;
  private int modoAnterior;

  // Bloqueo MUTEX mediante la variable "sinBloquear"
  public synchronized boolean bloquea(int mAnt, CInicializar adaptee) {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = mAnt;
      adaptee.Inicializar(CInicializar.ESPERA);

      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  } // Fin bloquea()

  // Este m�todo desbloquea el sistema
  public synchronized void desbloquea(CInicializar CIni) {
    sinBloquear = true;
    CIni.Inicializar(modoAnterior);
  } // Fin desbloquea()

} // endclass SincrEventos
