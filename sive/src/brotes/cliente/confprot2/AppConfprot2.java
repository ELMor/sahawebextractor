package brotes.cliente.confprot2;

import java.util.ResourceBundle;

import capp.CApp;

public class AppConfprot2
    extends CApp {

  CMantmtoModBrote panel;
  ResourceBundle res;
  String sEtCodigo = "";
  String sEtModelo = "";
  String sEtOperativo = "";

  public AppConfprot2() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    ResourceBundle res = ResourceBundle.getBundle(
        "brotes.cliente.confprot2.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    sEtCodigo = res.getString("msg2.Text");
    sEtModelo = res.getString("msg3.Text");
    sEtOperativo = res.getString("msg4.Text");

    panel = new CMantmtoModBrote(this);
    panel.setBorde(false);

    VerPanel("", panel);
  }

  public void stop() {
    panel = null;
    System.gc();
  }
}
