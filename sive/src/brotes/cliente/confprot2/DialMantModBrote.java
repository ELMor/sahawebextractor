package brotes.cliente.confprot2;

//import brotes.servidor.confprot2.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.datos.confprot2.DataBroteBasico;
import brotes.datos.confprot2.DataMantLineasModBrote;
import brotes.datos.confprot2.DataModeloBrote;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import cargamodelo.volcarRepositorio;
/*            SrvModPregCent servlet = new SrvModPregCent();
           //Indica como conectarse a la b.datos
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
            data = (CLista) servlet.doDebug(this.modoVOLCADO, data);
 */
import comun.DataEnferedo;
import comun.DataEntradaEDO;
import sapp.StubSrvBD;

public class DialMantModBrote
    extends CDialog {

  //Modos de operaci�n de la ventana
  public static final int modoALTA = 0;
  ResourceBundle res = ResourceBundle.getBundle("brotes.cliente.confprot2.Res" +
                                                this.app.getIdioma());
  public static final int modoMODIFICAR = 1;
  public static final int modoESPERA = 2;
  public static final int modoBAJA = 3;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;

  //Para la lista de valores de 'Partir del modelo'
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSEL_DESCBROTE = 2;
  final int servletSEL_DESCBROTELIM = 3;

  //
  final static int servletOBTENER_X_CODIGO_MODELOS = 202;
  final static int servletOBTENER_X_DESCRIPCION_MODELOS = 203;
  final static int servletSELECCION_X_CODIGO_MODELOS = 200;
  final static int servletSELECCION_X_DESCRIPCION_MODELOS = 201;

  final int servletNO_OPERATIVO = 10;
  final int servletDES_MOD = 0;

  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  final int servletCONSULTAR_RESPUESTAS = 2;
  final int servletCONSULTAR_LINEAS = 4;

  //Constantes del panel
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/repositorio.gif"};

  final String strSERVLETMod = "servlet/SrvGesModBrote";
  final String strSERVLETModelo = "servlet/SrvLineasModBrote";
  final String strSERVLETEnf = "servlet/SrvEnfermedad";
  final String strSERVLETNiv = "servlet/SrvEntradaEDO";
  final String strSERVLET_BROTES = "servlet/SrvObtenerBrotes";
  final String strSERVLETdescarga = "servlet/SrvDesMod";
  //Para centinelas
  final String strSERVLET_ENF_CENTI = "servlet/SrvEnfCenti";

  protected int modoOperacion;
  protected int iYearAct;

  protected StubSrvBD stubCliente;
  protected StubSrvBD stubClienteRepositorio;

  protected boolean bAceptar = false;
  protected boolean fallo = false;

  // control
  protected String sModeloConLineas = "N";
  protected String sModeloConRespuestas = "N";

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  public DataModeloBrote datModelo;

  // gestores de eventos
  botonesActionListener btnActionListener = new botonesActionListener(this);
  itemListener choiceItemListener = new itemListener(this);
  keyAdapter txtKeyListener = new keyAdapter(this);
  focusAdapter txtFocusListener = new focusAdapter(this);

  XYLayout xYLayout = new XYLayout();
  Label lblModelo = new Label();
  Label lblPartirMod = new Label();
  Label lblOp = new Label();
  Label lblDescLocal = new Label();
  CCampoCodigo modelo = new CCampoCodigo();
  TextField modeloDesc = new TextField();
  TextField txtDescL = new TextField();
  CCampoCodigo txtCodPartirMod = new CCampoCodigo();
  Choice choiceOp = new Choice();

  //Campos q identifican el brote al que se le asigna el modelo
  Label lblAno = new Label();
  TextField txtAno = new TextField();
  TextField txtCod = new TextField();
  ButtonControl btnClave = new ButtonControl();
  TextField txtDesc = new TextField();

//  Label lblBrote = new Label();
  //  CCampoCodigo txtCodBrote = new CCampoCodigo();
//  TextField txtDesBrote = new TextField();
//  ButtonControl btnBrote = new ButtonControl();

  //  Choice choGrupo = new Choice();
  TextField fecha = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnPartirMod = new ButtonControl();
  TextField txtDesPartirMod = new TextField();
  Label lblFecBaja = new Label();
  Label lblModeloDesc = new Label();
  Panel pnl = new Panel();
  Panel pnl2 = new Panel();
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  StatusBar statusBar1 = new StatusBar();
  StatusBar statusBar2 = new StatusBar();
  SimpleDateFormat formato1 = new SimpleDateFormat("dd/MM/yyyy");
//  Label lblPartirFichero = new Label();
//  TextField txtPartirFichero = new TextField();
//  ButtonControl btnFichero = new ButtonControl();
  ButtonControl btnRepositorio = new ButtonControl();

  public DialMantModBrote(CApp a,
                          int modo,
                          DataModeloBrote dat) {
    super(a);

    try {

      Calendar cal = Calendar.getInstance();
      iYearAct = cal.get(Calendar.YEAR);
//      bTuberculosis = !a.getParametro("CD_TUBERCULOSIS").equals("");

      stubCliente = new StubSrvBD();
      modoOperacion = modo;
      datModelo = dat;
      jbInit();

      Inicializar();
      txtDesPartirMod.setEditable(false);
      txtDesPartirMod.setEnabled(false);
      btnPartirMod.setFocusAware(false);
      modelo.requestFocus();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /*  private String getCodEnfGrupo (){
     String sAfectado = "";
        sAfectado = getGrupo();
      return ( sAfectado);
    }*/

  // validaciones
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;
    boolean b = true;

    if ( (this.modelo.getText().length() == 0) ||
        (this.modeloDesc.getText().length() == 0) ||
        (txtAno.getText().length() == 0) ||
        (txtCod.getText().length() == 0) ||
        (txtDesc.getText().length() == 0)) {
      b = false;
    }
    if (!b) {
      msg = res.getString("msg5.Text");
    }

    if (b) {
      b = true;
      if ( (modelo.getText().length() > 8) &&
          (modoOperacion == modoALTA)) {
        b = false;
        msg = res.getString("msg6.Text");
        modelo.selectAll();
      }
      if (modeloDesc.getText().length() > 60) {
        b = false;
        msg = res.getString("msg6.Text");
        modeloDesc.selectAll();
      }
      if (txtDescL.getText().length() > 60) {
        b = false;
        msg = res.getString("msg6.Text");
        txtDescL.selectAll();
      }
    }
    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    //Se ajusta la variable que se analiza en panel otra vez por si se cerrase dialogo con aspa !!
    bAceptar = b;
    return b;
  }

  // inicializador
  void jbInit() throws Exception {
    CLista data;
    CLista param;

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnPartirMod.setImage(imgs.getImage(0));
    btnClave.setImage(imgs.getImage(0));
//    btnFichero.setImage(imgs.getImage(0));
    btnRepositorio.setLabel(res.getString("btnRepositorio.Label"));
    btnRepositorio.addActionListener(new
        DialMantModBrote_btnRepositorio_actionAdapter(this));
    btnRepositorio.setImage(imgs.getImage(3));
    btnAceptar.setImage(imgs.getImage(1));
    btnCancelar.setImage(imgs.getImage(2));

    xYLayout.setHeight(470);
    xYLayout.setWidth(500);
    this.setLayout(borderLayout1);
    //this.setSize(new Dimension(400, 470));
    pnl.setLayout(xYLayout);
    txtAno.addTextListener(new DialMantModBrote_txtAno_textAdapter(this));

    btnCancelar.setLabel(res.getString("btnCancelar.Label"));

    lblModelo.setText(res.getString("lblModelo.Text"));
    lblAno.setText("A�o-C�digo Brote:");
    lblPartirMod.setText(res.getString("lblCopia.Text")); //Partir del modelo
    lblOp.setText(res.getString("lblOp.Text")); //Operativo?
    lblDescLocal.setText(res.getString("lblDescLocal.Text"));
    if (! (this.app.getParametro("ANYO_DEFECTO").equals(""))) {
      txtAno.setText(this.app.getParametro("ANYO_DEFECTO"));
    }
    else {
      txtAno.setText(new Integer(iYearAct).toString());

    }

    modelo.setForeground(Color.black);
    modelo.setBackground(new Color(255, 255, 150));
    modeloDesc.setBackground(new Color(255, 255, 150));
    txtAno.setBackground(new Color(255, 255, 150));
    txtCod.setBackground(new Color(255, 255, 150));

    modelo.setName("modelo");
    txtCodPartirMod.setName("partir");
    txtAno.setName("ano");
    txtCod.setName("codigo");
//    txtAno.addFocusListener(new DialMantModBrote_txtAno_focusAdapter(this));
    txtAno.addKeyListener(new DialMantModBrote_txtAno_keyAdapter(this));
    txtCodPartirMod.addKeyListener(new
                                   DialMantModBrote_txtCodPartirMod_keyAdapter(this));
    txtCod.addKeyListener(new DialMantModBrote_txtCod_keyAdapter(this));

    // p�rdida del foco
    txtCod.addFocusListener(txtFocusListener);

    // cambio de c�digo
    txtCod.addKeyListener(txtKeyListener);

    // p�rdida del foco
    txtAno.addFocusListener(txtFocusListener);

    // cambio de c�digo
    txtAno.addKeyListener(txtKeyListener);

    // p�rdida del foco
    txtCodPartirMod.addFocusListener(txtFocusListener);

    // cambio de c�digo
    txtCodPartirMod.addKeyListener(txtKeyListener);

    // botones
    btnClave.addActionListener(btnActionListener);
    btnPartirMod.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    lblFecBaja.setText(res.getString("lblFecBaja.Text"));
    fecha.setEditable(false);
    txtDesc.setEditable(false);
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    lblModeloDesc.setText(res.getString("lblModeloDesc.Text"));
    statusBar1.setBevelOuter(BevelPanel.LOWERED);
    statusBar1.setBevelInner(BevelPanel.LOWERED);
    statusBar2.setBevelOuter(BevelPanel.LOWERED);
//    lblPartirFichero.setText(res.getString("lblPartirFichero.Text"));
//    btnFichero.addActionListener(new DialMantModBrote_btnFichero_actionAdapter(this));
//    txtPartirFichero.addKeyListener(new DialMantModBrote_txtPartirFichero_keyAdapter(this));
    statusBar2.setBevelInner(BevelPanel.LOWERED);
    pnl2.setLayout(borderLayout2);

    btnClave.setActionCommand("alta");
    btnPartirMod.setActionCommand("partir");
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    txtDesPartirMod.setEditable(false);

    pnl.add(modelo, new XYConstraints(128, 8, 93, -1));
    pnl.add(modeloDesc, new XYConstraints(128, 37, 326, -1));
    pnl.add(lblDescLocal, new XYConstraints(8, 67, 113, -1));
    pnl.add(txtDescL, new XYConstraints(128, 66, 300, -1));

    pnl.add(lblAno, new XYConstraints(8, 98, 108, -1));
    pnl.add(txtAno, new XYConstraints(128, 98, 57, -1));
    pnl.add(txtCod, new XYConstraints(188, 98, 63, -1));
    pnl.add(btnClave, new XYConstraints(257, 98, -1, -1));
    pnl.add(txtDesc, new XYConstraints(289, 98, 167, -1));
//    pnl.add(choGrupo, new XYConstraints(128, 98, -1, -1));
//    pnl.add(txtCodBrote, new XYConstraints(128, 98, 93, -1));
//    pnl.add(btnBrote, new XYConstraints(224, 98, -1, -1));
//    pnl.add(txtDesBrote, new XYConstraints(267, 98, 187, -1));
//    pnl.add(lblBrote, new XYConstraints(8, 98, 82, -1));

    pnl.add(txtCodPartirMod, new XYConstraints(128, 196, 93, -1));
    pnl.add(btnPartirMod, new XYConstraints(224, 196, -1, -1));
    pnl.add(txtDesPartirMod, new XYConstraints(267, 196, 187, -1));

    pnl.add(choiceOp, new XYConstraints(128, 258, 46, 23));
    pnl.add(fecha, new XYConstraints(363, 258, 92, -1));
    pnl.add(btnAceptar, new XYConstraints(287, 297, 80, 26));
    pnl.add(btnCancelar, new XYConstraints(374, 297, 80, 26));
    pnl.add(lblModelo, new XYConstraints(8, 8, 54, -1));

    pnl.add(lblPartirMod, new XYConstraints(8, 196, 105, -1));
    pnl.add(lblOp, new XYConstraints(8, 258, 80, -1));
    pnl.add(lblFecBaja, new XYConstraints(261, 258, 88, -1));
    pnl.add(lblModeloDesc, new XYConstraints(8, 37, 102, -1));
//    pnl.add(lblPartirFichero, new XYConstraints(8, 228, -1, -1));
//    pnl.add(txtPartirFichero, new XYConstraints(128, 228, 283, -1));
//    pnl.add(btnFichero, new XYConstraints(414, 228, -1, -1));
    if (modoOperacion == modoMODIFICAR) {
      pnl.add(btnRepositorio, new XYConstraints(8, 297, -1, -1));
    }

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDescL.setVisible(false);
      lblDescLocal.setVisible(false);
    }
    else {
      lblDescLocal.setText(res.getString("lblDescLocal.Text") +
                           app.getIdiomaLocal() + "):");
    }

    //nombre
    txtAno.setName("ano");

    choiceOp.addItem(res.getString("msg7.Text"));
    choiceOp.addItem(res.getString("msg8.Text"));
    choiceOp.addItemListener(choiceItemListener);

    setSize(466, 400);
    this.add(pnl, BorderLayout.CENTER);
    pnl2.add(statusBar1, BorderLayout.CENTER);
    pnl2.add(statusBar2, BorderLayout.WEST);
    this.add(pnl2, BorderLayout.SOUTH);

    // datos iniciales
    if (datModelo != null) {
      modelo.setText(datModelo.getCD_Modelo());
      modeloDesc.setText(datModelo.getDS_Modelo());
      txtDescL.setText(datModelo.getDSL_Modelo());
      txtAno.setText(datModelo.getCD_Ano());
      txtCod.setText( (new Integer(datModelo.getNM_Alerbro())).toString());
      String sDescBrote = buscarDescripcionBrote(datModelo.getCD_Ano(),
                                                 txtCod.getText());
      txtDesc.setText(sDescBrote);

      if (datModelo.getIT_OK().compareTo("S") == 0) {
        choiceOp.select(res.getString("msg7.Text"));
      }
      else {
        choiceOp.select(res.getString("msg8.Text"));
      }

      //Si el modelo est� de baja se pone la fecha de baja
      //(Nota puede estar no operativo aun sin haber sido dado de baja
      if (! (datModelo.getBaja().equals(""))) {
        /*formato1=new SimpleDateFormat("dd/MM/yyyy");
                 formato2=new SimpleDateFormat("yyyy-MM-dd");
                 Date miFecha = null;
                 miFecha = formato2.parse(datModelo.getBaja()); */
        fecha.setText(datModelo.getBaja());
      }

      param = new CLista();
      param.setTSive(this.app.getTSive());
      param.addElement(new DataMantLineasModBrote(datModelo.getCD_Modelo(),
                                                  app.getTSive()));

      // determina si el modelo tiene l�neas
      param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
      param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
      stubCliente.setUrl(new URL(app.getURL() + strSERVLETModelo));

      /*    SrvLineasModBrote servlet = new SrvLineasModBrote();
                 //Indica como conectarse a la b.datos
            servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
            data = (CLista) servlet.doDebug(servletCONSULTAR_LINEAS, param);*/

      data = (CLista) stubCliente.doPost(servletCONSULTAR_LINEAS, param);

      /*
            SrvModelo srv= new SrvModelo();
            data= srv.doDebug(servletCONSULTAR_LINEAS, param);
       */

      if (data.size() != 0) {
        sModeloConLineas = (String) data.firstElement();
      }
      else {
        sModeloConLineas = "S"; // bloquea el modelo

      }
      data = null;

      statusBar2.setText(res.getString("statusBar2.Text") + sModeloConLineas);

      // determina si el modelo tiene respuestas
      param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
      param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
      stubCliente.setUrl(new URL(app.getURL() + strSERVLETModelo));

      /*           //Indica como conectarse a la b.datos
            servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
           data = (CLista) servlet.doDebug(servletCONSULTAR_RESPUESTAS, param);*/

      data = (CLista) stubCliente.doPost(servletCONSULTAR_RESPUESTAS, param);

      /*
           SrvModelo srvlet= new SrvModelo();
           data= srvlet.doDebug(servletCONSULTAR_RESPUESTAS, param);
       */

      if (data.size() != 0) {
        sModeloConRespuestas = (String) data.firstElement();
      }
      else {
        sModeloConRespuestas = "S"; // bloquea el modelo

      }
      data = null;
      param = null;

      statusBar1.setText(res.getString("statusBar1.Text") +
                         sModeloConRespuestas);
    } //end datos iniciales

    // Adaptaci�n para Tuberculosis
    /*    if (bTuberculosis) {
          enfermedad.setText(this.app.getParametro("CD_TUBERCULOSIS"));
        }*/

    Inicializar();
  }

  String buscarDescripcionBrote(String ano, String aler) {
    CMessage mensaje = null;
    CLista lDesc = new CLista();
    CLista datDesc = null;
    String sDescDev = null;
    lDesc.addElement(ano);
    lDesc.addElement(aler);
    try {

      stubCliente.setUrl(new URL(app.getURL() + strSERVLET_BROTES));

      /*      SrvObtenerBrotes servlet = new SrvObtenerBrotes();
                 //Indica como conectarse a la b.datos
            servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
            datDesc = (CLista) servlet.doDebug(servletSEL_DESCBROTE, lDesc);*/

      datDesc = (CLista) stubCliente.doPost(servletSEL_DESCBROTE, lDesc);
      sDescDev = (String) datDesc.elementAt(0);
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    return sDescDev;
  } //end buscarDescripcionBrote

  CLista buscarDescripcionBroteLim(String ano, String aler, String usu) {
    CMessage mensaje = null;
    CLista lDesc = new CLista();
    CLista datDesc = null;
    String sDescDev = null;
    lDesc.addElement(ano);
    lDesc.addElement(aler);
    lDesc.addElement(usu);
    lDesc.addElement(this.app.getParametro("ca"));
    try {

      stubCliente.setUrl(new URL(app.getURL() + strSERVLET_BROTES));

      /*      SrvObtenerBrotes servlet = new SrvObtenerBrotes();
                 //Indica como conectarse a la b.datos
            servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
           datDesc = (CLista) servlet.doDebug(servletSEL_DESCBROTELIM, lDesc);*/

      datDesc = (CLista) stubCliente.doPost(servletSEL_DESCBROTELIM, lDesc);
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
    return datDesc;
  } //end buscarDescripcionBrote

  //Funci�n q se ejecuta para consultar datos de brotes
  void btnClave_actionPerformed() {

    /*    int modo = modoOperacion;
        modoOperacion = modoESPERA;*/

    String titulo = "";
    titulo = "Brotes disponibles";
    DialBuscarBrote dlg = new DialBuscarBrote(this.app,
                                              titulo,
                                              txtAno.getText().trim());
    if (!dlg.bError) { //sin error y con datos

      //puede ser que se de un error en la recuperacion de la
      //lista de alertas y brotes
      dlg.show();
    }
    else { //con error  o sin datos
      if (dlg.hayDatos != 0) { //con error y no sin datos
        String msgError = "Error recuperando la lista";
        CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
        mensaje.show();
      }
    }

    //si sali con el bot�n Aceptar en el dialogo
    if (dlg.iOut != -1) {
      CLista listaAlta = dlg.getComponente();
      if (listaAlta.size() > 0) {
        //relleno los paneles
        DataBroteBasico data = (DataBroteBasico) listaAlta.elementAt(0);
        txtAno.setText(data.getCD_Ano());
        txtCod.setText(data.getNM_Alerbro());
        txtDesc.setText(data.getDS_BROTE());
        //se guarda el grupo al que pertenece el brote?? -> cd_enfcie
        //grupo = data.getCD_GRUPO();
        //this.dlgB.RellenaCheck(data);
      }
    }
    //si sale por cancelar, nada
    dlg = null;
    /*    modoOperacion = modo;
        dlgB.Inicializar(modoOperacion);*/
  } //end btnClave

  //funci�n q comprueba si lo introducido en un campo es un valor num�rico
  public boolean isNum(TextField txt) {
    boolean b = true; //Es Numerico
    try {
      Integer f = new Integer(txt.getText());
      int i = f.intValue();
      if (i < 0) {
        b = false;
      }
    }
    catch (Exception e) {
      b = false;
    }
    return b;
  } //end isNum

//funci�n para validar q el a�o introducido es correcto
  public boolean validarAno() {
    //1. perdidas de foco
    //2. longitudes
    if (txtAno.getText().length() > 4) {
      String msgError =
          "La longitud del campo a�o del Brote excede de 4 caracteres";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtAno.setText("");
      txtAno.requestFocus();
      return false;
    }
    if (txtAno.getText().length() < 4) {
      String msgError =
          "La longitud del campo a�o del Brote es menor de 4 caracteres";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtAno.setText("");
      txtAno.requestFocus();
      return false;
    }
    if (!isNum(txtAno)) {
      String msgError = "El a�o debe ser un valor Entero y Positivo";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtAno.requestFocus();
      return false;
    }
    if (txtAno.getText().equals("")) {
      String msgError = "Es necesario introducir el a�o del brote";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtAno.requestFocus();
      return false;
    }
    return true;
  } //end validarAno

//funci�n para validar los datos de a�o,c�digo y descripci�n de brote
  public boolean validarDatos(int modo) {
    boolean correcto = true;

    //1. perdidas de foco
    //2. longitudes
    if (txtAno.getText().length() > 4) {
      String msgError =
          "La longitud del campo a�o del Brote excede de 4 caracteres";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtAno.setText("");
      txtAno.requestFocus();
      return false;
    }
    if (txtAno.getText().length() < 4) {
      String msgError =
          "La longitud del campo a�o del Brote es menor de 4 caracteres";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtAno.setText("");
      txtAno.requestFocus();
      return false;
    }

    if (txtCod.getText().length() > 7) {
      String msgError =
          "La longitud del campo C�digo del Brote excede de 7 caracteres";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtCod.setText("");
      txtCod.requestFocus();
      return false;
    }
    if (txtDesc.getText().length() > 50) {
      String msgError =
          "La longitud de la Descripci�n del Brote excede de 50 caracteres";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtDesc.setText("");
      txtDesc.requestFocus();
      return false;
    }
    //3. numericos
    //en alta, puedo escribir el a�o
    if (modo == modoALTA) {
      if (!isNum(txtAno)) {
        String msgError = "El a�o debe ser un valor Entero y Positivo";
        CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
        mensaje.show();
        txtAno.requestFocus();
        return false;
      }
    }
    //4. obligatorios //no saltara ninguno
    //en alta
    if (txtCod.getText().equals("")) {
      String msgError =
          "Es necesario seleccionar una Alerta o Brote Disponible";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtCod.requestFocus();
      return false;
    }
    if (txtAno.getText().equals("")) {
      String msgError = "Es necesario introducir el a�o del brote";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtAno.requestFocus();
      return false;
    }
    if (txtDesc.getText().equals("")) {
      String msgError = "Es necesario introducir la descripci�n del brote";
      CMessage mensaje = new CMessage(this.app, CMessage.msgERROR, msgError);
      mensaje.show();
      txtDesc.requestFocus();
      return false;
    }
    return true;
  }

  public void Inicializar() {

    btnRepositorio.setEnabled(false); // opci�n no sisponible

    switch (modoOperacion) {

      case modoALTA:
        setTitle(res.getString("msg9.Text"));
        modelo.setEnabled(true);
        modeloDesc.setEnabled(true);
        txtCodPartirMod.setEnabled(true);
        txtAno.setEnabled(true);
        txtCod.setEnabled(true);
        btnClave.setEnabled(true);
        btnClave.setEnabled(true);
        btnPartirMod.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnAceptar.setEnabled(true);
        if (txtAno.getText().equals("")) {
          btnClave.setEnabled(false);
          txtCod.setEnabled(false);
        }

        // se crea como no operativo
        choiceOp.setEnabled(false);
        choiceOp.select(1);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:
        setTitle(res.getString("msg10.Text"));
        modelo.setEnabled(false);
        modeloDesc.setEnabled(true);
        txtCodPartirMod.setVisible(false);
        txtDesPartirMod.setVisible(false);
        btnPartirMod.setVisible(false);
//      txtPartirFichero.setVisible(false);
//      btnFichero.setVisible(false);
//      lblPartirFichero.setVisible(false);
        lblPartirMod.setVisible(false);
        choiceOp.setEnabled(true);
        fecha.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnAceptar.setEnabled(true);
        // modelo con respuestas u operativo
        if ( (sModeloConRespuestas.equals("S")) ||
            (datModelo.getIT_OK().equals("S"))) {
          txtAno.setEnabled(false);
          txtCod.setEnabled(false);
          btnClave.setEnabled(false);
        }
        else {
          txtAno.setEnabled(true);
          txtCod.setEnabled(true);
          btnClave.setEnabled(true);
        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        modelo.setEnabled(false);
        modeloDesc.setEnabled(false);
        txtCodPartirMod.setEnabled(false);
        txtAno.setEnabled(false);
        txtCod.setEnabled(false);
        btnClave.setEnabled(false);
        choiceOp.setEnabled(false);
        fecha.setEnabled(false);
        btnCancelar.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnClave.setEnabled(false);
        btnPartirMod.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoBAJA:
        setTitle(res.getString("msg11.Text"));
        modelo.setEnabled(false);
        modeloDesc.setEnabled(false);
        txtDescL.setEnabled(false);
        txtCodPartirMod.setVisible(false);
        txtDesPartirMod.setVisible(false);
        txtAno.setEnabled(false);
        txtCod.setEnabled(false);
        btnClave.setEnabled(false);
//      btnClave.setVisible(false);
        btnPartirMod.setVisible(false);
        lblPartirMod.setVisible(false);
        choiceOp.setEnabled(false);
        fecha.setEnabled(false);
        btnCancelar.setEnabled(true);
        btnAceptar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
    this.doLayout();
  }

  //dado un cod, situar la Choice en ese registro
  //recordar que es obligatorio pero lleva un blanco
  public void selectGrupo(String cd) {
    /*    if ( cd.equals(""))
          choGrupo.select(0);
        else{
          String cdChoice = "";
          for (int i = 1; i < 3; i++){
//        cdChoice =  choGrupo.getItem(i);
            cdChoice = cdChoice.substring(0, 2);
            if (cdChoice.equals(cd.trim())){
//          choGrupo.select (i);
              break;
            }
          }
        }*/
  }

  // gesti�n del campo fecha de baja
  void choiceOp_itemStateChanged(ItemEvent e) {
    Date today = new Date();
    String dia;
    int mes;
    int anyo;
    String cad;
    //Si es operativo
    if (choiceOp.getSelectedItem().compareTo(res.getString("msg8.Text")) != 0) {
      fecha.setText("");
    }
    //Si no est� operativo
    else {
      formato1 = new SimpleDateFormat("dd/MM/yyyy");
      fecha.setText(formato1.format(today));
    }
  }

  // limpia la ventana
  protected void borraTodo() {
    modeloDesc.setText("");
    txtDescL.setText("");
//    choGrupo.select(0);
    txtCodPartirMod.setText("");
    txtDesPartirMod.setText("");
    choiceOp.select(res.getString("msg7.Text"));
    fecha.setText("");
    Inicializar();
  }

  // lista de valores de modelos
  void btnPartirMod_actionPerformed(ActionEvent e) {

    DataModeloBrote data;
    CMessage mensaje = null;
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaModelo lista = new CListaModelo(app,
                                            res.getString("msg14.Text"),
                                            stubCliente,
                                            strSERVLETMod,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      // ARS 20-06-01: L�nea puesta para obtener datos.
      lista.btnSearch_actionPerformed();
      // ----- fin 20-06-01
      lista.show();
      data = (DataModeloBrote) lista.getComponente();
      if (data != null) {
        txtDesPartirMod.setText(data.getDS_Modelo());
        txtCodPartirMod.setText(data.getCD_Modelo());
//      txtPartirFichero.setText("");
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
    modoOperacion = modo;
    Inicializar();
  }

// a�adir
  void Anadir() {
    CLista datos = null;
    CMessage mensaje = null;
    CLista lr = null;
    String miCod = "";
    Date dFec = null;

    if (!isDataValid()) {
      return;
    }
    else {

      int miModo = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar();

      String cad = new String("");
      String tramI = new String("");

      //comprobacion modeloalta <> partir
      if (modelo.getText().compareTo(txtCodPartirMod.getText()) != 0) {

        try {
          if (this.choiceOp.getSelectedItem() == res.getString("msg8.Text")) {
            tramI = "N";
          }
          else {
            tramI = "S";
          }
          lr = new CLista();

          datos = new CLista();
          datos.setLogin(this.app.getLogin());
          datos.setFilter("");
          datos.setTSive(this.app.getTSive());
//parte donde se elige el sufijo q se debe a�adir al cod_modelo
          switch (this.app.getCA().length()) {
            case 0:
              miCod = "_00";
              break;
            case 1:
              miCod = "_0" + this.app.getCA();
              break;
            case 2:
              miCod = "_" + this.app.getCA();
              break;
          }
//params:tSive,cdmod,dsmod,dslmodelo,cdano,nmalerbro,itok,partir,deBaja,cdenfcie
          int nmAler = Integer.parseInt(txtCod.getText().trim());
          datModelo = new DataModeloBrote(this.app.getTSive(),
                                          modelo.getText().toUpperCase() +
                                          miCod,
                                          this.modeloDesc.getText(),
                                          "", txtAno.getText().trim(),
                                          nmAler, tramI,
                                          txtCodPartirMod.getText(),
                                          "", null, this.app.getParametro("ca"));

          datos.addElement(datModelo);
//        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
//        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
          stubCliente.setUrl(new URL(app.getURL() + strSERVLETMod));

          /*        //QUITAR
                      SrvGesModBrote servlet = new SrvGesModBrote();
                     //Indica como conectarse a la b.datos
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                     "sive_desa",
                                     "sive_desa");
                      lr = (CLista) servlet.doDebug(modoALTA,datos);*/

          lr = (CLista)this.stubCliente.doPost(modoALTA, datos);

          /*
                  lr= (new SrvGesMod()).doDebug(modoALTA,datos);
           */

          datos = null;
          /*        if (txtPartirFichero.getText().length() > 0){
                  partirFichero partFich = new partirFichero( this.app,
                          txtPartirFichero.getText().trim(),
                          this.modelo.getText().trim()+miCod);
                  }*/
          dispose();
        }
        catch (Exception excepc) {
          excepc.printStackTrace();
          mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
          mensaje.show();
        }
      }
      else { //ni idea
        mensaje = new CMessage(this.app, CMessage.msgERROR,
                               res.getString("msg15.Text"));
        mensaje.show();
      }
    } //else data valid

    modoOperacion = modoALTA;
    Inicializar();
  }

  void Modificar() {
    CLista datos = null;
    CMessage mensaje = null;
    CLista lrm = null;
    String cad = new String("");
    String tramI = new String("");

    Date dFec = null;
    String sFec = "";

    if (isDataValid()) {
      try {
        if (this.choiceOp.getSelectedItem().compareTo(res.getString("msg8.Text")) ==
            0) {
          tramI = "N";
          //Convierte la fecha de baja de formato pantalla a formato de b.datos
          /*formato1=new SimpleDateFormat("dd/MM/yyyy");
                     formato2=new SimpleDateFormat("yyyy-MM-dd");*/
          //Pasa a tipo Date
          //dFec = formato1.parse(fecha.getText());
          //Pasa el Date a String con el formato que viene de b.datos
          //sFec = formato2.format(dFec);

          //si antes ha sido operativo y luego deja de serlo,
          //tendr� una fecha de baja, en caso contrario, no.
          sFec = fecha.getText();
        }
        else {
          tramI = "S";
        }
        //if (area.getText()
        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        lrm = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.setPerfil(this.app.getPerfil());

        int nmAler = Integer.parseInt(txtCod.getText().trim());
        datModelo = new DataModeloBrote(this.app.getTSive(),
                                        modelo.getText(),
                                        this.modeloDesc.getText(),
                                        txtDescL.getText(),
                                        txtAno.getText().trim(),
                                        nmAler, tramI, "",
                                        sFec, null, this.app.getParametro("ca"));

        datos.addElement(datModelo);
        datos.setTSive(this.app.getTSive());
        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLETMod));
        /*            SrvGesModBrote servlet = new SrvGesModBrote();
                   //Indica como conectarse a la b.datos
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                    lrm = (CLista) servlet.doDebug(servletMODIFICAR,datos);*/
        lrm = (CLista)this.stubCliente.doPost(servletMODIFICAR, datos);
        datos = null;
        dispose();
      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        mensaje.show();

      }
      modoOperacion = modoMODIFICAR;
      Inicializar();
    }
  }

  void Borrar() {
    CLista datos = null;
    CMessage mensaje = null;
    CLista lrm = null;
    String cad = new String("");
    String tramI = new String("");
    int modo = -1;

    try {
      //a�adir cd_enfcie al final
      /*        datModelo =new DataModeloBrote(this.app.getTSive(),
                        modelo.getText(), null, null, null,
                        0,  null, null, null);*/

      // CASO1:el modelo tiene respuestas
      if (sModeloConRespuestas.equals("S")) {
        mensaje = new CMessage(app, CMessage.msgPREGUNTA,
                               res.getString("msg16.Text"));
        mensaje.show();

        // pone el nodo como no operativo
        if ( (mensaje.getResponse()) == true) {
          modo = servletNO_OPERATIVO;
        }
        //CASO2:el modelo tiene l�neas y no respuestas
      }
      else if (sModeloConLineas.equals("S")) {
        mensaje = new CMessage(app, CMessage.msgPREGUNTA,
                               res.getString("msg17.Text"));
        mensaje.show();

        // borra el modelo y sus l�neas
        if ( (mensaje.getResponse()) == true) {
          modo = servletBAJA;
        }
        //CASO3:el modelo no tiene l�neas ni respuestas
      }
      else {
        mensaje = new CMessage(app, CMessage.msgPREGUNTA,
                               res.getString("msg18.Text"));
        mensaje.show();

        // borra el modelo
        if ( (mensaje.getResponse()) == true) {
          modo = servletBAJA;
        }
      }

      //if (area.getText()
      if (modo != -1) {
        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        lrm = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.addElement(datModelo);
        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLETMod));
        lrm = (CLista)this.stubCliente.doPost(modo, datos);
        datos = null;
        dispose();
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
    modoOperacion = modoBAJA;
    Inicializar();
  }

  // cambio en partir
  void txtCodPartirMod_keyPressed(KeyEvent e) {
    int iLong = txtCodPartirMod.getText().length();
    int iMaxLonCodPartir = 11;
    char c = e.getKeyChar();

    if ( (int) c == 8 || (int) c == 9 || (int) c == 127) {
      return;
    }
    else {
      if (iLong >= iMaxLonCodPartir) {
        e.consume(); // se supera la longitud m�xima
      } //end else if
    } //end else
    //si cambia el c�digo se vacia el campo descripci�n
    txtDesPartirMod.setText("");
  }

  void txtCodPartirMod_focusLost() {
    CLista datos = null;
    CLista lr = null;
    DataModeloBrote data;
    CMessage msgBox;
    int modo = modoOperacion;

    if (txtCodPartirMod.getText().length() > 0) {
      try {
        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.setPerfil(this.app.getPerfil());
        //cd_enfcie=null
        datos.addElement(new DataModeloBrote(this.app.getTSive(),
                                             txtCodPartirMod.getText(), null, null, null,
                                             0, null, null, null, null,
                                             this.app.getParametro("ca")));

        stubCliente.setUrl(new URL(app.getURL() + strSERVLETMod));
        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
        lr = (CLista)this.stubCliente.doPost(servletOBTENER_X_CODIGO, datos);
        datos = null;
        if (lr.size() > 0) {
          data = (DataModeloBrote) lr.firstElement();
          txtDesPartirMod.setText(data.getDS_Modelo());
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg19.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception erl) {
        erl.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, erl.getMessage());
        msgBox.show();
        msgBox = null;
      }
      modoOperacion = modo;
      Inicializar();
    }
  }

  // cambio en a�o de brote
  void txtAno_keyPressed(KeyEvent e) {
    //hasta q no se introduce algo en a�o no se permite introd el resto
    if (txtAno.getText().equals("")) {
      btnClave.setEnabled(false);
      txtCod.setEnabled(false);
    }
    else {
      btnClave.setEnabled(true);
      txtCod.setEnabled(true);
    }
    char c = e.getKeyChar();
    int iLong = txtAno.getText().length();
    int iMaxLongAno = 4;

    if ( (int) c == 8 || (int) c == 9 || (int) c == 127) {
      return;
    }
    else {
      if (!Character.isDigit(c)) {
        e.consume(); // letra
      }
      else if (iLong >= iMaxLongAno) {
        e.consume(); // se supera la longitud m�xima
      } //end else if
    } //end else
    //si cambia el a�o: se vacian c�d y descripci�n y se deshabilita
    //hasta q se introduzca un nuevo a�o.
    txtDesc.setText("");
    txtCod.setText("");
    txtCod.setEnabled(false);
  }

  //si el a�o pierde el foco y ya ha sido introducido un c�digo
  //obtener la descripci�n del brote correspondiente.
  void txtAno_focusLost() {
    CLista desc = null;
    if ( (txtCod.getText().length() != 0) && (txtAno.getText().length() != 0)) {
      desc = buscarDescripcionBroteLim(txtAno.getText(), txtCod.getText(),
                                       this.app.getLogin());
      //si se obtiene una descripci�n.
      if (desc.size() != 0) {
        txtDesc.setText( (String) desc.elementAt(0));
        fallo = false;
      }
      else {
        fallo = true;
        CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                       res.getString("msg19.Text"));
        msgBox.show();
        msgBox = null;
      }
    } //end txtCod.getText().length()!=0
  } //end txtAno_focusLost

  // cambio en c�digo de brote  (nm_alerbro)
  void txtCod_keyPressed(KeyEvent event) {
    char c = event.getKeyChar();
    int iLong = txtCod.getText().length();
    int iMaxDigCod = 7;

    if ( (int) c == 8 || (int) c == 9 || (int) c == 127) {
      return;
    }
    else {
      if (!Character.isDigit(c)) {
        event.consume(); // letra
      }
      else if (iLong >= iMaxDigCod) {
        event.consume(); // se supera la longitud m�xima
      } //end else if
    } //end else
    //si hay un cambio en c�digo se vacia la descripci�n.
    txtDesc.setText("");
  } //end txtCod_keypressed

  //si el c�digo de brote pierde el foco y ya ha sido introducido uno
  //obtener la descripci�n del brote correspondiente.
  void txtCod_focusLost() {
    CLista desc = null;
    if ( (txtCod.getText().length() != 0) && (txtAno.getText().length() != 0)) {
      desc = buscarDescripcionBroteLim(txtAno.getText(), txtCod.getText(),
                                       this.app.getLogin());
      //si se obtiene una descripci�n.
      if (desc.size() != 0) {
        txtDesc.setText( (String) desc.elementAt(0));
        fallo = false;
      }
      else {
        //si ha fallado en a�o q no falle aqu� tambi�n.
        if (fallo) {
          fallo = false;
        }
        else {
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                         res.getString("msg19.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
    } //end txtAno.getText().length()!=0
  } //end txtCod_focusLost

  // cambio en la caja del modelo
  void modelo_keyPressed(KeyEvent e) {
    borraTodo();
  }

  /*  void txtPartirFichero_keyPressed(KeyEvent e) {
      txtCodPartirMod.setText("");
      txtDesPartirMod.setText("");
    }*/

  /*  void btnFichero_actionPerformed(ActionEvent e) {
      int modoMio = modoOperacion;
      CMessage msgBox = null;
      modoOperacion = modoESPERA;
      Inicializar();
      try{
        FileDialog dialog = new FileDialog(app.getFrame(), res.getString("msg20.Text"), FileDialog.LOAD);
        dialog.show();
        if (dialog.getFile() != null){
//        txtPartirFichero.setText(dialog.getDirectory() + dialog.getFile());
          txtCodPartirMod.setText("");
        }
      }catch (Exception er){
        msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
        msgBox.show();
        msgBox = null;
      }
      modoOperacion = modoMio;
      Inicializar();
    }*/

  void btnRepositorio_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    CLista data;
    CLista elBloque;
    CLista listaFich;
    int j = 0;
    int miModo = modoOperacion;
    int numTok; //Num tokens quedan por recorrer de una linea
    String sLinea;
    // stub's

    try {
      this.modoOperacion = modoESPERA;
      Inicializar();
      data = new CLista();
      listaFich = new CLista();
      //a�adir cd_enfcie al final
//      data.addElement(new DataModeloBrote(this.app.getTSive(), modelo.getText(),null,null,null,0,null,null,null,this.app.getParametro("ca")) );
      // llamamos al servlet de descarga
      data.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
      data.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
      stubClienteRepositorio = new StubSrvBD();
      stubClienteRepositorio.setUrl(new URL(app.getURL() + strSERVLETdescarga));
      // obtiene la lista con los registros a a�adir
      listaFich = (CLista) stubClienteRepositorio.doPost(servletDES_MOD, data);

      if (listaFich.size() > 0) {
        // insertamos en el repositorio
        volcarRepositorio volcador = new volcarRepositorio(this.app,
            listaFich,
            this.app.getFTP_SERVER());
      }
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg21.Text"));
        msgBox.show();
        msgBox = null;
      }
      // error al procesar
    }
    catch (Exception enc) {
      enc.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, enc.toString());
      msgBox.show();
      msgBox = null;
    }

    this.modoOperacion = miModo;
    Inicializar();

  }

  /*Al perder el foco el a�o, valida si es correcto y en caso
     afirmativo pone habilitados el resto de los campos*/
  void txtAno_focusLost(FocusEvent e) {
    if (validarAno()) {
      txtCod.setEnabled(true);
      btnClave.setEnabled(true);
    }
    else {
      txtCod.setEnabled(false);
      btnClave.setEnabled(false);
    }
  } //end AnofocusLost

  void txtAno_textValueChanged(TextEvent e) {
    txtCod.setText("");
    txtDesc.setText("");
    if (txtAno.getText().equals("")) {
      txtCod.setEnabled(false);
      btnClave.setEnabled(false);
    }
    else {
      txtCod.setEnabled(true);
      btnClave.setEnabled(true);
    }
  }

}

// gestor evento botones
class botonesActionListener
    implements ActionListener, Runnable {
  DialMantModBrote adaptee = null;
  ResourceBundle res = ResourceBundle.getBundle("brotes.cliente.confprot2.Res0");
  ActionEvent e = null;

  public botonesActionListener(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand() == "partir") {
      adaptee.btnPartirMod_actionPerformed(e);
    }

    if (e.getActionCommand() == "alta") {
      adaptee.btnClave_actionPerformed();
    }

    else if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case DialMantModBrote.modoALTA:
          adaptee.Anadir();
          break;
        case DialMantModBrote.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case DialMantModBrote.modoBAJA:
          adaptee.Borrar();
          break;
      }

    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }

  }
}

// selecci�n de la choice de modelo operativo
class itemListener
    implements java.awt.event.ItemListener {
  DialMantModBrote adaptee;

  itemListener(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.choiceOp_itemStateChanged(e);
  }
}

// cambio del texto en una caja de c�digo
class keyAdapter
    extends java.awt.event.KeyAdapter {
  DialMantModBrote adaptee;

  keyAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    if (txt.getName().equals("partir")) {
      adaptee.txtCodPartirMod_keyPressed(e);
    }
    else if (txt.getName().equals("modelo")) {
      adaptee.modelo_keyPressed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DialMantModBrote adaptee;
  FocusEvent evt;

  focusAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    TextField txt = (TextField) evt.getSource();
    if (txt.getName().equals("partir")) {
      adaptee.txtCodPartirMod_focusLost();
    }
    if (txt.getName().equals("ano")) {
      adaptee.txtAno_focusLost();
    }
    if (txt.getName().equals("codigo")) {
      adaptee.txtCod_focusLost();
    }
  }
}

// listas de valores
class CListaModelo
    extends CListaValores {
  public CListaModelo(CApp a,
                      String title,
                      StubSrvBD stub, String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descripcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a, title, stub, servlet, obtener_x_codigo, obtener_x_descripcion,
          seleccion_x_codigo, seleccion_x_descripcion);

    //btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    //cd_enfcie a null
    return new DataModeloBrote(this.app.getTSive(), s, null, null, null, 0, null, null, null, null,
                               this.app.getParametro("ca"));
  }

  public String getCodigo(Object o) {
    return ( ( (DataModeloBrote) o).getCD_Modelo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataModeloBrote) o).getDS_Modelo());
  }

}

// lista de valores  enf para aplicaci�n  EDO
class CListaEnfCie
    extends CListaValores {

  public CListaEnfCie(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descripcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEnferedo(s, s); // El servlet se encarga de tomar la cadena
    // oportuna.
  }

  public String getCodigo(Object o) {
    return ( ( (DataEnferedo) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnferedo) o).getDes());
  }

}

// lista de valores
class CListaNiv1
    extends CListaValores {

  public CListaNiv1(CApp a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

// lista de valores
class CListaNiv2
    extends CListaValores {

  DialMantModBrote dial;

  public CListaNiv2(DialMantModBrote a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(a.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    dial = a;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    String d = s;
    return d;
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

/*class DialMantModBrote_txtPartirFichero_keyAdapter extends java.awt.event.KeyAdapter {
  DialMantModBrote adaptee;
  DialMantModBrote_txtPartirFichero_keyAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }
  public void keyPressed(KeyEvent e) {
    adaptee.txtPartirFichero_keyPressed(e);
  }
 } */

class DialMantModBrote_txtCodPartirMod_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialMantModBrote adaptee;

  DialMantModBrote_txtCodPartirMod_keyAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodPartirMod_keyPressed(e);
  }
}

class DialMantModBrote_txtAno_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialMantModBrote adaptee;

  DialMantModBrote_txtAno_keyAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtAno_keyPressed(e);
  }
}

class DialMantModBrote_txtCod_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialMantModBrote adaptee;

  DialMantModBrote_txtCod_keyAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCod_keyPressed(e);
  }
}

/*class DialMantModBrote_btnFichero_actionAdapter implements java.awt.event.ActionListener{
  DialMantModBrote adaptee;
  DialMantModBrote_btnFichero_actionAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.btnFichero_actionPerformed(e);
  }
 } */

class DialMantModBrote_btnRepositorio_actionAdapter
    implements java.awt.event.ActionListener {
  DialMantModBrote adaptee;

  DialMantModBrote_btnRepositorio_actionAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnRepositorio_actionPerformed(e);
  }
}

class DialMantModBrote_txtAno_focusAdapter
    extends java.awt.event.FocusAdapter {
  DialMantModBrote adaptee;

  DialMantModBrote_txtAno_focusAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtAno_focusLost(e);
  }
}

class DialMantModBrote_txtAno_textAdapter
    implements java.awt.event.TextListener {
  DialMantModBrote adaptee;

  DialMantModBrote_txtAno_textAdapter(DialMantModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    adaptee.txtAno_textValueChanged(e);
  }
}
