package brotes.cliente.confprot2;

import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CDialog;

public class DialTextoPregBrote
    extends CDialog
    implements ActionListener {
  XYLayout xYLayout1 = new XYLayout();
  ResourceBundle res = ResourceBundle.getBundle("brotes.cliente.confprot2.Res" +
                                                this.app.getIdioma());
  ButtonControl btnAceptar = new ButtonControl();
  CheckboxGroup checkboxGroup1 = new CheckboxGroup();
  Checkbox chTexto = new Checkbox();
  Checkbox chPregunta = new Checkbox();
  boolean aceptado = false;

  public DialTextoPregBrote(Image img, CApp app) {
    super(app);
    try {
      jbInit();
      btnAceptar.setImage(img);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    xYLayout1.setHeight(90);
    setTitle(res.getString("Insertar_bloque_o"));
    chTexto.setLabel(res.getString("chTexto.Label"));
    chTexto.setCheckboxGroup(checkboxGroup1);
    chTexto.setState(true);
    chPregunta.setLabel(res.getString("chPregunta.Label"));
    chPregunta.setCheckboxGroup(checkboxGroup1);
    xYLayout1.setWidth(250);
    setSize(250, 90);
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    this.setLayout(xYLayout1);
    this.add(btnAceptar, new XYConstraints(174, 34, -1, -1));
    this.add(chTexto, new XYConstraints(3, 5, 112, 18));
    this.add(chPregunta, new XYConstraints(127, 5, 119, 16));
    btnAceptar.addActionListener(this);
  }

  public void actionPerformed(ActionEvent e) {
    dispose();
    //# System.Out.println(e.getID());
  }

  public boolean bInsertarTexto() {
    return chTexto.getState();
  }
}
