package brotes.cliente.confprot2;

import java.net.URL;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
//import brotes.servidor.confprot2.*;
import brotes.datos.bidata.DatAlBro;
import brotes.datos.bidata.DataAlerta;
import brotes.datos.bidata.DataBrote;
import brotes.datos.confprot2.DataBroteBasico;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import sapp.StubSrvBD;

public class DialBuscarBrote
    extends CDialog {

  public boolean bError = false;
  public int hayDatos = -1;

  // modos de operaci�n del servlet
  final int servletSEL_ALTA_INV_BROTE = 1;
  final String strSERVLET_BROTES = "servlet/SrvObtenerBrotes";

  //para ver si Aceptar=0 � Cancelar=-1;
  public int iOut = -1;

  // modos de actualizaci�n de la lista
  final int modoADD = 20;
  final int modoMODIFY = 21;
  final int modoDELETE = 22;

  // modo de operaci�n
  final int modoNORMAL = 0;
  final int modoESPERA = 1;
  final int modoINFORME = 600;
  final int modoINVESTIGACION = 601;
  protected int modoVentana = modoINFORME;
  protected int modoOperacion = modoNORMAL;
  //imagenes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/declaracion2.gif",
      "images/declaracion.gif"};
  protected CCargadorImagen imgs = null;

  // par�metros
  String cd_ano = new String();
  String titulo = "";

  //otros
  //para el servlet
  StubSrvBD stubCliente;

  //para la tabla
  DataAlerta alerta = new DataAlerta();
  DataBrote brote = new DataBrote();
  JCVector items = new JCVector();
  CLista listaTbl = new CLista();
  String ano_nm = new String();
  String fc_fechahora_f = new String();
  String fc_fechahora_h = new String();
  String ds_brote = new String();
  String cd_sitalerbro = new String();

  //para el tramado
  CLista listaBD = new CLista();
  DataAlerta ultAlerta = new DataAlerta();
  DatAlBro ultBrote = new DatAlBro();

  ///????
  CLista listaRet = new CLista();

  // controles
  jclass.bwt.JCMultiColumnList tbl = new jclass.bwt.JCMultiColumnList();
  XYLayout xYLayout = new XYLayout();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  Panel panel = new Panel();
  XYLayout xYLayout2 = new XYLayout();

  // Listeners
  DialBuscarBroteactionAdapter actionAdapter = new DialBuscarBroteactionAdapter(this);
  DialBuscarBroteTblactionAdapter tblActionAdapter = new
      DialBuscarBroteTblactionAdapter(this);

  //constructor
  public DialBuscarBrote(CApp a, String title, String ano) {
    super(a);
    titulo = title;
    cd_ano = ano;
    stubCliente = new StubSrvBD();
//    modoVentana = dlg.modoVentana;

    try {
      bError = !BuscarBrotes(cd_ano);
      jbInit();
      pack();
      RellenaLista(listaBD);
      //bError = false;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      bError = true;
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void jbInit() throws Exception {
    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout.setHeight(300);
    xYLayout.setWidth(385);
    setSize(385, 300);

    setTitle(titulo);

    setLayout(xYLayout);

    xYLayout2.setHeight(300);
    xYLayout2.setWidth(385);
    panel.setSize(385, 300);
    panel.setLayout(xYLayout2);

    this.setBackground(Color.lightGray);

    tbl.getList().setBackground(Color.white);
    tbl.getList().setHighlightColor(Color.lightGray);
    tbl.setInsets(new Insets(5, 5, 5, 5));
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "85\n248"), '\n'));
    tbl.setColumnLabelsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("A�o-C�digo\nDescripci�n"), '\n'));
    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("A�o-C�digo\nDescripci�n"), '\n'));
    tbl.setNumColumns(3);
    tbl.setColumnResizePolicy(1);
    /*
         tbl.setColumnButtons(JCUtilConverter.toStringList(new String("Fecha Notificacion\nFecha Recepcion\nR.\nNotif. Reales"), '\n'));
         tbl.setColumnWidths(JCUtilConverter.toIntList(new String("135\n135\n40\n115"), '\n'));
         tbl.setNumColumns(4);
     */
    tbl.setScrollbarDisplay(3);
    tbl.setAutoSelect(true);
    this.add(panel, new XYConstraints(0, 0, 385, 300));
    panel.add(tbl, new XYConstraints(10, 20, 362, 220));
    panel.add(btnAceptar, new XYConstraints(185, 251, 80, -1));
    panel.add(btnCancelar, new XYConstraints(287, 251, 80, -1));

    btnAceptar.setLabel("Aceptar");
    btnCancelar.setLabel("Cancelar");
    btnAceptar.setActionCommand("aceptar");
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setActionCommand("cancelar");
    btnCancelar.setImage(imgs.getImage(1));

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);
    tbl.addActionListener(tblActionAdapter);

  }

  // habilita/deshabilita las opciones seg�n sea necesario
  void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:

        tbl.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        tbl.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  public void RellenaLista(CLista lista) {
    DataBroteBasico brote;
    tbl.clear();
    items = new JCVector();
    for (int i = 0; i < lista.size(); i++) {
      try {
        brote = (DataBroteBasico) lista.elementAt(i);
        addRowBrote(brote);
      }
      catch (ClassCastException ex) {
        ex.printStackTrace();
        bError = true;
      }
    }
    // verifica que sea la �ltima trama
    if (lista.getState() == CLista.listaINCOMPLETA) {
      tbl.addItem("M�s datos...");

    }
  }

  public void addRowBrote(DataBroteBasico data) {
    int iImage = 0;
    ano_nm = data.getCD_Ano().trim() + "-" + data.getNM_Alerbro().trim();
    ds_brote = data.getDS_BROTE().trim();

    JCVector row1 = new JCVector();
    row1.addElement(ano_nm);
    row1.addElement(ds_brote);

    items.addElement(row1);
    listaTbl.addElement(data);
    tbl.setItems(items);
  }

  public void addRowBro(DatAlBro data) {
    int iImage = 0;

    ano_nm = data.getCD_ANO().trim() + "-" + data.getNM_ALERBRO().trim();
    fc_fechahora_f = data.getFC_FECHAHORA_F().trim();
    fc_fechahora_h = data.getFC_FECHAHORA_H().trim();
    ds_brote = data.getDS_BROTE().trim();
    cd_sitalerbro = data.getCD_SITALERBRO().trim();

    JCVector row1 = new JCVector();
    row1.addElement(ano_nm);
    row1.addElement(ds_brote);
    row1.addElement(fc_fechahora_f + " " + fc_fechahora_h);
    row1.addElement(cd_sitalerbro);

    items.addElement(row1);
    listaTbl.addElement(data);
    tbl.setItems(items);
  }

  /*
   * Devuelve el dato de la fila seleccionada en la tabla
   * Es una CLista porque si es una alerta se devuelve un DataAlerta
   * pero si es un brote se devuelve un DataAlerta y un DataBrote
   */
  public CLista getComponente() {
    Object o = null;

    if (iOut != -1) {
      o = listaTbl.elementAt(tbl.getSelectedIndex());

      /*    DataAlerta al = null;
          DatAlBro alBro = null;
          DataBrote bro = null;*/
    }
    DataBroteBasico brBasic = null;
    CLista res = new CLista();

    try {
      brBasic = (DataBroteBasico) o;
      res.addElement(brBasic);
    }
    catch (ClassCastException ex) {
      ex.printStackTrace();
      bError = true;
    }
    return res;
  }

  void tbl_actionPerformed() {
    CMessage msgBox = null;
    CLista data = null;
    CLista resul = null;
    int iLast;
    Object componente;

    // lista incompleta
    if ( (listaBD.getState() == CLista.listaINCOMPLETA) &&
        (tbl.getSelectedIndex() == (tbl.countItems() - 1))) {

      modoOperacion = modoESPERA;
      Inicializar();

      CLista listaBDAnt = listaBD;
      iLast = tbl.countItems() - 1;

      try {
        BuscarBrotes(cd_ano);
      }
      catch (Exception e) {
        ShowWarning("Error recuperando la lista");
      }

      if (listaBD.size() > 0) {
        listaBDAnt.appendData(listaBD);
        listaBDAnt.setState(listaBD.getState());
      }
      else {
        listaBDAnt.setState(CLista.listaLLENA);
      }
      listaBD = listaBDAnt;

      RellenaLista(listaBD);

      modoOperacion = modoNORMAL;
      Inicializar();

      // item seleccionado
    }
    else {
      btnAceptar_actionPerformed();
    }
  }

  void btnAceptar_actionPerformed() {
    iOut = 0;
    dispose();
  }

  void btnCancelar_actionPerformed() {
    iOut = -1;
    dispose();
  }

  boolean BuscarBrotes(String ano) throws Exception {
    //try {
    CLista result = new CLista();
    CLista param = new CLista();
    //como par�metros de entrada se pasa el a�o y el usuario
    param.addElement(ano);
    param.addElement(this.app.getLogin());
    param.addElement(this.app.getParametro("ca"));
//      datos.setLogin(this.app.getLogin());
    stubCliente.setUrl(new URL(app.getURL() + strSERVLET_BROTES));
    result = (CLista) stubCliente.doPost(servletSEL_ALTA_INV_BROTE, param);
    /*            SrvObtenerBrotes servlet = new SrvObtenerBrotes();
               //Indica como conectarse a la b.datos
                 servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                               "sive_desa",
                               "sive_desa");
         result = (CLista) servlet.doDebug(servletSEL_ALTA_INV_BROTE, param);*/

    // comprueba que hay datos
    if (result.size() == 0) {
      ShowWarning("No se encontraron datos.");
      hayDatos = 0;
      listaBD = new CLista();
      return false;
    }
    else {
      hayDatos = 1;
      CLista lis = new CLista();
      for (int i = 0; i < result.size(); i++) {
        listaBD.addElement( (DataBroteBasico) result.elementAt(i));
      } //end for

      // verifica que sea la �ltima trama
      if (result.getState() == CLista.listaINCOMPLETA) {
        listaBD.setState(CLista.listaINCOMPLETA);

      }
      return true;

    }
    /*      CLista result = new CLista();
          CLista parametros = new CLista();
          // par�metros que se le pasan a DataAlerta: CD_ANO
          // que es modo? de momento le mando un 0
         DataBusqueda data = new DataBusqueda(ano, "", "", "", "", "","","","",
                                                "", "", "", "", "", "","",
                                                "", "", "", 0,
         (Vector)this.app.getCD_NIVEL_1_AUTORIZACIONES(),
         (Vector)this.app.getCD_NIVEL_2_AUTORIZACIONES() );
          parametros.addElement(data);
          parametros.addElement(ultAlerta);
          parametros.addElement(ultBrote);
          SrvBIAlt srv = new SrvBIAlt();
          // perfil
          parametros.setPerfil(app.getPerfil());
          // login
          parametros.setLogin(app.getLogin());
          // idioma
          parametros.setIdioma(app.getIdioma());
          result = srv.doDebug(1,  parametros);
          //devuelve una lista con alertas y alertabrotes ordenadas por
          //fecha (decreciente)
          //si no se traen todos los datos, entonces devuelve tambien
          //los ultimos alerta y brotes (si corresponde)
          // comprueba que hay datos
          if (result.size() == 0) {
            ShowWarning ("No se encontraron datos.");
            hayDatos = 0;
            listaBD = new CLista();
            return false;
          }
          else {
            hayDatos = 1;
            CLista lis = new CLista();
            listaBD = (CLista) result.elementAt(0);
            if (result.size()== 2)
              ultAlerta = (DataAlerta) result.elementAt(1);
            else if (result.size()== 3){
              ultAlerta = (DataAlerta) result.elementAt(1);
              ultBrote = (DatAlBro) result.elementAt(2);
            }
            //tengo los datos de todos los paneles
            // verifica que sea la �ltima trama
            if (result.getState() == CLista.listaINCOMPLETA)
              listaBD.setState(CLista.listaINCOMPLETA);
            return true;
          }
        /*
         } catch (Exception e) {
           ShowWarning("Error recuperando la lista de alertas y brotes");
           throw (e);
         }*/

  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

}

// escuchador de los click en los botones
class DialBuscarBroteactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialBuscarBrote adaptee;
  ActionEvent evt;

  DialBuscarBroteactionAdapter(DialBuscarBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
}

// escuchador de los click en la tabla
class DialBuscarBroteTblactionAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  DialBuscarBrote adaptee;

  DialBuscarBroteTblactionAdapter(DialBuscarBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.tbl_actionPerformed();
  }
}
