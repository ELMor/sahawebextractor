/**
 * Clase: SrvBrAut
 * Paquete: brotes.servidor.menu
 * Hereda: AppServlet
 * Fecha Inicio: 29/02/2000
 */

package brotes.servidor.menu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

// Para recuperar las autorizaciones 'antiguas'
import brotes.datos.menu.ParametrosBrUsu;
import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;

public class SrvBrAut
    extends DBServlet {

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    /** PDP 03/05/2000
     * Usuario para la conexi�n JDBC
     */
    //Variables para primera conexi�n que valida el password
    //Nota: No confundir con la conexi�n habitual a la base de datos
    //que usaria el mismo userName y userPwd para todos los usuarios
    String userNameEntrada = "";
    String userPwdEntrada = "";
    Connection conEntrada = null;

    //Valida la conexi�n con JDBC  PDP 03/05/2000
    // Par�metros
    Data dtLogin2 = (Data) vParametros.elementAt(0);
    userNameEntrada = dtLogin2.getString("COD_USUARIO");
    userPwdEntrada = dtLogin2.getString("PASSWORD");
    // Control de error
    boolean bError1 = false;
    String sMsg1 = null;

    // abre la conexi�n de este usuario
    try {
      Class.forName(driverName);
      conEntrada = DriverManager.getConnection(dbUrl, userNameEntrada,
                                               userPwdEntrada);
      conEntrada.close();
    }
    catch (Exception e) {

      trazaLog(e);
      bError1 = true;
      sMsg1 = "Usuario o password incorrecto";
    }
    // Devuelve el error recogido
    if (bError1) {
      throw new Exception(sMsg1);
    }

    // Control de error
    boolean bError = false;
    String sMsg = null;

    // Buffers
    Lista snapShot = null;
    Lista vResultado = new Lista();
    Data dt = null;
    Data dtAutorizaciones = new Data();
    Vector vDatSubQuery = new Vector();
    int i;

    // Query tool
    QueryTool qt = null;

    // Recupera par�metros 'antiguas'
    ParametrosBrUsu ptu = new ParametrosBrUsu();

    try {

      // Par�metros
      Data dtLogin = (Data) vParametros.elementAt(0);

      con.setAutoCommit(true);

      // Comprueba que la aplicaci�n est� dada de alta
      // select ESTADO from APLICACION where COD_APLICACION = BRO
      qt = new QueryTool();
      qt.putName("APLICACION");
      qt.putType("ESTADO", QueryTool.STRING);

      // filtro de la aplicaci�n
      qt.putWhereType("COD_APLICACION", QueryTool.STRING);
      qt.putWhereValue("COD_APLICACION", dtLogin.getString("COD_APLICACION"));
      qt.putOperator("COD_APLICACION", "=");

      snapShot = (Lista) qt.doSelect(con);

      // Aplicaci�n inexistente
      if (snapShot.size() == 0) {
        sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
            "] no existe";
        bError = true;

        // Aplicaci�n existe
      }
      else {
        // Chequea el estado de la aplicaci�n
        dt = (Data) snapShot.elementAt(0);

        // Aplicaci�n abierta
        if (dt.getString("ESTADO").equals("")) {

          // Comprueba la acreditaci�n del usuario

          // select COD_USUARIO,COD_GRUPO from V_APLIC_USU_GRU
          // where COD_APLICACION=? and COD_USUARIO=? and PASSWORD=?
          // and FC_BAJA is null

          qt = new QueryTool();
          qt.putName("V_APLIC_USU_GRU");
          qt.putType("COD_USUARIO", QueryTool.STRING);
          qt.putType("COD_GRUPO", QueryTool.INTEGER);

          // Filtro con las acreditaciones
          qt.putWhereType("COD_APLICACION", QueryTool.STRING);
          qt.putWhereValue("COD_APLICACION", dtLogin.getString("COD_APLICACION"));
          qt.putOperator("COD_APLICACION", "=");

          qt.putWhereType("COD_USUARIO", QueryTool.STRING);
          qt.putWhereValue("COD_USUARIO", dtLogin.getString("COD_USUARIO"));
          qt.putOperator("COD_USUARIO", "=");

          /*qt.putWhereType("PASSWORD", QueryTool.STRING);
               qt.putWhereValue("PASSWORD", dtLogin.getString("PASSWORD"));
                     qt.putOperator("PASSWORD", "=");*/

          qt.putWhereType("FC_BAJA", QueryTool.VOID);
          qt.putWhereValue("FC_BAJA", "");
          qt.putOperator("FC_BAJA", "is null");

          snapShot = (Lista) qt.doSelect(con);

          // Usuario-Grupo para la Aplicacion no encontrado
          if (snapShot.size() == 0) {
            sMsg = "Acceso denegado";
            bError = true;

            // Usuario-Grupo para la Aplicacion encontrado
          }
          else {

            // Graba el grupo
            dt = (Data) snapShot.elementAt(0);
            dtLogin.put("COD_GRUPO", dt.getString("COD_GRUPO"));
            /*
                          // Lee el perfil del usuario
                          //select CD_TUSU from SIVE_USUARIO where CD_USUARIO = ?
                          qt = new QueryTool();
                          qt.putName("SIVE_USUARIO");
                          qt.putType("CD_TUSU", QueryTool.STRING);
                          qt.putWhereType("CD_USUARIO", QueryTool.STRING);
                 qt.putWhereValue("CD_USUARIO", dtLogin.getString("COD_USUARIO"));
                          qt.putOperator("CD_USUARIO", "=");
                          snapShot = (Lista) qt.doSelect(con);
                          // Perfil de usuario no encontrado
                          if (snapShot.size() == 0) {
                            sMsg = "Perfil de usuario no reconocido";
                            bError = true;
                          // Perfil de usuario encontrado
                          } else {
                            // Graba el perfil
                            dt = (Data) snapShot.elementAt(0);
                            dtLogin.put("CD_TUSU", dt.getString("CD_TUSU"));
             */
            // Lee las autorizaciones del usuario
            // select COD_ACCION,SW_ESTADO from ACCION where COD_APLICACION=BRO
            // and COD_ACCION in (select COD_ACCION from GRUPAUTO where COD_APLICACION=? and COD_GRUPO=?)
            qt = new QueryTool();
            qt.putName("ACCION");
            qt.putType("COD_ACCION", QueryTool.INTEGER);
            qt.putType("SW_ESTADO", QueryTool.STRING);
            qt.putWhereType("COD_APLICACION", QueryTool.STRING);
            qt.putWhereValue("COD_APLICACION",
                             dtLogin.getString("COD_APLICACION"));
            qt.putOperator("COD_APLICACION", "=");

            // Realiza el cruce con GRUPAUTO
            qt.putSubquery("COD_ACCION",
                "select COD_ACCION from GRUPAUTO where COD_APLICACION=? and COD_GRUPO=?");
            dt = new Data();
            dt.put(new Integer(QueryTool.STRING),
                   dtLogin.getString("COD_APLICACION"));
            vDatSubQuery.addElement(dt);
            dt = new Data();
            dt.put(new Integer(QueryTool.INTEGER),
                   dtLogin.getString("COD_GRUPO"));
            vDatSubQuery.addElement(dt);
            qt.putVectorSubquery("COD_ACCION", vDatSubQuery);

            snapShot = qt.doSelect(con);

            // Transforma el snapShot en una estructura Data
            String accion = null;
            String estado = null;
            for (i = 0; i < snapShot.size(); i++) {
              dt = (Data) snapShot.elementAt(i);

              accion = dt.getString("COD_ACCION");
              estado = dt.getString("SW_ESTADO");
              dtAutorizaciones.put(accion, estado);

              // Establece IT_AUTALTA
              if (accion.equals("20001")) {
                if (estado.equals("")) {
                  ptu.put("IT_AUTALTA", "S");
                }
                else {
                  ptu.put("IT_AUTALTA", "N");
                }
              }

              // Establece IT_AUTMOD
              if (accion.equals("20002")) {
                if (estado.equals("")) {
                  ptu.put("IT_AUTMOD", "S");
                }
                else {
                  ptu.put("IT_AUTMOD", "N");
                }
              }

              // Establece IT_AUTBAJA
              if (accion.equals("20003")) {
                if (estado.equals("")) {
                  ptu.put("IT_AUTBAJA", "S");
                }
                else {
                  ptu.put("IT_AUTBAJA", "N");
                }
              }
            }

            // Agrega el par�metro FC_ACTUAL
            dtAutorizaciones.put("FC_ACTUAL", Format.fechaActual());

            // Agrega el par�metro LOGIN
            ptu.put("LOGIN", dtLogin.getString("COD_USUARIO"));

            // TSive
            ptu.put("TSIVE", "B");

            java.util.Date dFecha_Actual = new java.util.Date();
            java.text.SimpleDateFormat Format = new java.text.SimpleDateFormat(
                "dd/MM/yyyy");
            String sFecha_Actual = Format.format(dFecha_Actual); //string

            ptu.put("FC_ACTUAL", sFecha_Actual);
            ptu.put("ANYO_DEFECTO", sFecha_Actual.substring(6, 10));

            // Obtiene los datos que obten�a AutServlet para
            // ser pasados a cada applet
            qt = new QueryTool();
            qt.putName("SIVE_USUARIO_BROTE");

            qt.putType("IT_PERFILUSU", QueryTool.STRING);
            qt.putType("IT_FG_VALIDAR", QueryTool.STRING);

            //*E qt.putType("IT_FG_MNTO", QueryTool.STRING);
             //*E qt.putType("IT_FG_MNTO_USU", QueryTool.STRING);
              //*E qt.putType("IT_FG_TCNE", QueryTool.STRING);
               //*E qt.putType("IT_FG_PROTOS", QueryTool.STRING);
                //hash.put("IT_FG_ALARMAS", rs.getString("IT_FG_ALARMAS"));
                //*E qt.putType("IT_AUTALTA", QueryTool.STRING);
                 //*E qt.putType("IT_AUTBAJA", QueryTool.STRING);
                  //*E qt.putType("IT_AUTMOD", QueryTool.STRING);

                   //hash.put("IT_FG_VALIDAR", rs.getString("IT_FG_VALIDAR"));
                   //qt.putType("FC_ACTUAL", QueryTool.STRING);
                   //hash.put("IT_FG_EXPORT", rs.getString("IT_FG_EXPORT"));
                   //hash.put("IT_FG_GENALAUTO", rs.getString("IT_FG_GENALAUTO"));
                   //hash.put("IT_FG_MNOTIFS", rs.getString("IT_FG_MNOTIFS"));
                   //hash.put("IT_FG_CONSRES", rs.getString("IT_FG_CONSREST"));
                   //*E qt.putType("IT_FG_RESCONF", QueryTool.STRING);
                    //*E qt.putType("DS_EMAIL", QueryTool.STRING);

                     // Filtro con las acreditaciones
            qt.putWhereType("CD_USUARIO", QueryTool.STRING);
            qt.putWhereValue("CD_USUARIO", dtLogin.getString("COD_USUARIO"));
            qt.putOperator("CD_USUARIO", "=");

            snapShot = (Lista) qt.doSelect(con);

            dt = (Data) snapShot.elementAt(0);

            ptu.put("PERFIL", dt.getString("IT_PERFILUSU"));
            ptu.put("IT_FG_VALIDAR", dt.getString("IT_FG_VALIDAR"));

            //*E ptu.put("IT_FG_MNTO", dt.getString("IT_FG_MNTO"));
             //*E ptu.put("IT_FG_MNTO_USU", dt.getString("IT_FG_MNTO_USU"));

              //*E ptu.put("IT_FG_TCNE", dt.getString("IT_FG_TCNE"));
               //*E ptu.put("IT_FG_PROTOS", dt.getString("IT_FG_PROTOS"));
                //*E ptu.put("IT_AUTALTA", dt.getString("IT_AUTALTA"));
                 //*E ptu.put("IT_AUTBAJA", dt.getString("IT_AUTBAJA"));
                  //*E ptu.put("IT_AUTMOD", dt.getString("IT_AUTMOD"));

                   //*E ptu.put("IT_FG_RESOLVER_CONFLICTOS", dt.getString("IT_FG_RESCONF"));
                    //*E ptu.put("DS_EMAIL", dt.getString("DS_EMAIL"));

                     // Obtiene los datos que antes suministraba
                     // AutServlet
            qt = new QueryTool();
            qt.putName("SIVE_CA_PARAMETROS_BR");

            qt.putType("CD_CA", QueryTool.STRING);
            qt.putType("DS_NIVEL_1", QueryTool.STRING);
            qt.putType("DS_NIVEL_2", QueryTool.STRING);
            qt.putType("IT_TRAMERO", QueryTool.STRING);

            //*E qt.putType("URL", QueryTool.STRING);
             //*E qt.putType("DS_URL_CNE", QueryTool.STRING);

              //qt.putType("FTP_SERVER", QueryTool.STRING);
              //*E qt.putType("DSL_NIVEL_1", QueryTool.STRING);

               //qt.putType("NIVEL1", QueryTool.STRING);
               //*E qt.putType("DSL_NIVEL_2", QueryTool.STRING);

                //qt.putType("NIVEL2", QueryTool.STRING);
                //*E qt.putType("DSL_NIVEL_3", QueryTool.STRING);

            qt.putType("DS_NIVEL_3", QueryTool.STRING);
            //qt.putType("NIVEL3", QueryTool.STRING);
            //*E qt.putType("DSL_IDLOCAL", QueryTool.STRING);

             //qt.putType("IDIOMA_LOCAL", QueryTool.STRING);
             //*E qt.putType("IT_MODULO_3", QueryTool.STRING);

            snapShot = (Lista) qt.doSelect(con);

            dt = (Data) snapShot.elementAt(0);

            ptu.put("CD_CA", dt.getString("CD_CA"));
            ptu.put("CA", dt.getString("CD_CA"));
            //ptu.put("DS_NIVEL_1", dt.getString("DS_NIVEL_1"));
            //ptu.put("DS_NIVEL_2", dt.getString("DS_NIVEL_2"));
            ptu.put("IT_TRAMERO", dt.getString("IT_TRAMERO"));

            //ptu.put("URL_SERVLET", dt.getString("URL"));
            //ptu.put("URL_HTML", dt.getString("URL")); // A�adido

            //*E ptu.put("FTP_SERVER", dt.getString("DS_URL_CNE"));
             //*E ptu.put("DSL_NIVEL_1", dt.getString("DSL_NIVEL_1"));

            ptu.put("NIVEL1", dt.getString("DS_NIVEL_1"));
            //*E ptu.put("DSL_NIVEL_2", dt.getString("DSL_NIVEL_2"));

            ptu.put("NIVEL2", dt.getString("DS_NIVEL_2"));
            //*E ptu.put("DSL_NIVEL_3", dt.getString("DSL_NIVEL_3"));

            ptu.put("DS_NIVEL_3", dt.getString("DS_NIVEL_3"));
            ptu.put("NIVEL3", dt.getString("DS_NIVEL_3"));

            ptu.put("IDIOMA", new Integer("0"));
            //ptu.put("IDIOMA", new Integer(dt.getString("DSL_IDLOCAL")));
            //ptu.put("IDIOMA_LOCAL", dt.getString("DSL_IDLOCAL"));

            //*E ptu.put("IT_MODULO_3", dt.getString("IT_MODULO_3"));

             // Detecta si hay que obtener autorizaciones
            if (ptu.getString("PERFIL", true).equals("3")
                || ptu.getString("PERFIL", true).equals("4")) {

              StringBuffer sbCD_NIVEL_1 = new StringBuffer("");
              StringBuffer sbCD_NIVEL_2 = new StringBuffer("");

              String sN1 = null;
              String sN2 = null;

              // Obtiene autorizaciones
              qt = new QueryTool();
              qt.putName("SIVE_AUTORIZACIONES_BROTE");
              qt.putType("CD_USUARIO", QueryTool.STRING);
              qt.putType("CD_NIVEL_1", QueryTool.STRING);
              qt.putType("CD_NIVEL_2", QueryTool.STRING);

              // Filtro con las acreditaciones
              qt.putWhereType("CD_USUARIO", QueryTool.STRING);
              qt.putWhereValue("CD_USUARIO", dtLogin.getString("COD_USUARIO"));
              qt.putOperator("CD_USUARIO", "=");

              snapShot = (Lista) qt.doSelect(con);

              for (int j = 0; j < snapShot.size(); j++) {
                dt = (Data) snapShot.elementAt(j);
                sN1 = dt.getString("CD_NIVEL_1");
                if (sN1 != null) {
                  sbCD_NIVEL_1.append(sN1 + ",");
                  sN2 = dt.getString("CD_NIVEL_2");
                  if (sN2 != null) {
                    if (ptu.getString("PERFIL", true).equals("4")) { // Pon�a 4
                      sbCD_NIVEL_2.append(sN2 + ",");
                    }
                  }
                }
              }

              ptu.put("CD_NIVEL_1_AUTORIZACIONES", sbCD_NIVEL_1.toString());
              ptu.put("CD_NIVEL_2_AUTORIZACIONES", sbCD_NIVEL_2.toString());
            }
            // Agrega estos datos a lo ya recuperado
            dtAutorizaciones.put("PARAMETROS_BRO_USU", ptu);

            // agrega la estructura Data con las autorizaciones
            vResultado.addElement(dtAutorizaciones);

            //   }

          } // if(Usuario-Grupo para la Aplicacion encontrado)

          // Aplicaci�n cerrada por aver�a
        }
        else if (dt.getString("ESTADO").equals("T")) {
          sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
              "] est� cerrada por aver�a";
          bError = true;
          // Aplicaci�n cerrada bajo petici�n de usuario
        }
        else if (dt.getString("ESTADO").equals("V")) {
          sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
              "] est� cerrada por petici�n de usuario";
          bError = true;
          // Aplicaci�n cerrada por backup
        }
        else if (dt.getString("ESTADO").equals("B")) {
          sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
              "] est� cerrada por BACKUP";
          bError = true;
          // Aplicaci�n cerrada
        }
        else {
          sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
              "] est� cerrada";
          bError = true;
        }
      } // if(Aplicacion existe)

      vResultado.setParameter("FC_ACTUAL", Format.fechaActual());

    }
    catch (SQLException e1) {
      // Traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error al comprobar las acreditaciones";
    }
    catch (Exception e2) {
      trazaLog(e2);
      bError = true;
      sMsg = "Error inesperado";
    } // Fin try .. catch

    // Devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }

    return vResultado;
  } // Fin doWork()

} // endclass SrvBrAut
