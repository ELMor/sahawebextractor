/**
 * Clase: SrvAlerta
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 11/05/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Alertas
 * Descripcion: Servlet para dar de alta/baja/modificar 1 alerta
 *   Es una r�plica de SrvTransaccion con alguna funcionalidad
 *   espec�fica de las alertas a�adida
 */

package brotes.servidor.alerta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
//import java.sql.SQLException;
import java.util.Enumeration;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;

public class SrvAlerta
    extends DBServlet {

  final protected int servletDO_SELECT = 1;
  final protected int servletDO_GETPAGE = 2;
  final protected int servletDO_INSERT = 3;
  final protected int servletDO_UPDATE = 4;
  final protected int servletDO_DELETE = 5;
  final protected int servletDO_INSERT_BLO = 10003;
  final protected int servletDO_UPDATE_BLO = 10006;
  final protected int servletDO_DELETE_BLO = 10007;
  final protected int servletDO_GETPAGE_DESC = 8;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // Vector con el resultado, Data con la QueryTool, opmode, QueryTool
    Lista vResulset = new Lista();
    Data dtQT = null;
    int modoOp;
    QueryTool queryTool = null;
    Enumeration eKey = null;
    Enumeration eValue = null;

    // Siguiente secuenciador en un alta
    int sigNM = 0;

    try {
      con.setAutoCommit(false);

      for (int i = 0; i < vParametros.size(); i++) {
        dtQT = (Data) vParametros.elementAt(i);
        eKey = dtQT.keys();
        eValue = dtQT.elements();

        modoOp = Integer.parseInt( (String) eKey.nextElement());
        queryTool = (QueryTool) eValue.nextElement();

        switch (modoOp) {
          case servletDO_SELECT:

            vResulset.addElement(queryTool.doSelect(con));
            break;

          case servletDO_GETPAGE:
            vResulset.addElement(queryTool.doGetPage(vParametros.getCampoTrama(),
                vParametros.getValorTrama(),
                ">",
                "",
                con));
            break;

          case servletDO_GETPAGE_DESC:
            vResulset.addElement(queryTool.doGetPage(vParametros.getCampoTrama(),
                vParametros.getValorTrama(),
                "<",
                "DESC",
                con));
            break;

          case servletDO_INSERT:
          case servletDO_INSERT_BLO:
            if (queryTool.getName().equals("SIVE_ALERTA_BROTES")) {
              // Obtenci�n del secuenciador para la alerta
              sigNM = obtenerSecuenciadorAlerta(queryTool);

              // Establece en la QT FC_ALERBRO
              establecerFC_ALERBRO(queryTool);
            }

            // Establece en la QT el secuenciador de la alerta
            establecerSecuenciadorAlerta(queryTool, sigNM);

            vResulset.addElement(queryTool.doInsert(con));
            vResulset.addElement(new Integer(sigNM));
            break;

          case servletDO_UPDATE_BLO:
          case servletDO_UPDATE:
            if (queryTool.getName().equals("SIVE_ALERTA_BROTES")) {
              if ( ( (Data) queryTool.getDatValue()).get("FC_ALERBRO").equals(
                  "CHANGE")) {

                // Establece en la QT FC_ALERBRO
                establecerFC_ALERBRO(queryTool);
              }
            }

            vResulset.addElement(queryTool.doUpdate(con));
            break;

          case servletDO_DELETE_BLO:
          case servletDO_DELETE:
            vResulset.addElement(queryTool.doDelete(con));
            break;
        }
      } // Fin for

      // Si todo ha ido bien se hace el commit
      con.commit();
    }
    catch (SQLException e1) {
      // rollback
      con.rollback();

      // traza el error
      trazaLog(e1);
      bError = true;

      // selecciona el mensaje de error
      switch (opmode) {
        case servletDO_SELECT:
        case servletDO_GETPAGE:
          sMsg = "Error al leer " + queryTool.getName();
          break;

        case servletDO_INSERT:
        case servletDO_INSERT_BLO:
          sMsg = "Error al insertar " + queryTool.getName();
          break;

        case servletDO_UPDATE:
        case servletDO_UPDATE_BLO:
          sMsg = "Error al actualizar " + queryTool.getName();
          break;

        case servletDO_DELETE:
        case servletDO_DELETE_BLO:
          sMsg = "Error al borrar " + queryTool.getName();
          break;
      }
    }
    catch (Exception e2) {
      // rollback
      con.rollback();

      // traza el error
      trazaLog(e2);
      bError = true;

      // selecciona el mensaje de error
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }

    return vResulset;
  }

  private int obtenerSecuenciadorAlerta(QueryTool qt) throws SQLException {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String ano = null;
    int sigNM = 0;

    // Obtenci�n del a�o y del secuenciador en ese a�o
    ano = (String) ( (Data) qt.getDatValue()).get("CD_ANO");
    sigNM = SecAnual(con, st, rs, "CD_ANO", ano, "NM_ALERBRO");

    return sigNM;
  } // obtenerSecuenciadorAlerta()

  // Establece en la querytool el valor del secuenciador de la
  //  nueva, ie, NM_ALERBRO
  private void establecerSecuenciadorAlerta(QueryTool qt, int sigNM) {
    // Establecimiento de ese secuenciador en la QueryTool
    ( (Data) qt.getDatValue()).put("NM_ALERBRO", sigNM + "");
  } // Fin establecerSecuenciadorAlerta()

  // Establece en la querytool el valor de FC_ALERBRO
  private void establecerFC_ALERBRO(QueryTool qt) {
    ( (Data) qt.getDatValue()).put("FC_ALERBRO", Format.fechaActual());
  } // Fin establecerSecuenciadorAlerta()

  /**
   * Inserta/Actualiza en la tabla sive_sec_anual
   * Devuelve el Secuenciador ya incrementado
   * Copia de sapp.Funciones  (JMT - 11/05/2000)
   */
  public static int SecAnual(Connection conec,
                             PreparedStatement stat,
                             ResultSet rset,
                             String sNombreColumAno,
                             String sValorAno,
                             String sNombreColum) throws SQLException {

    int iNM = 0;

    String sQuery = "SELECT * " +
        " FROM SIVE_SEC_ANUAL WHERE " + sNombreColumAno + " = ?";

    stat = conec.prepareStatement(sQuery);
    stat.setString(1, sValorAno);
    rset = stat.executeQuery();

    boolean bAnoExiste = false;
    if (rset.next()) {
      if (rset == null) {
        //insertar: todos a cero excepto el mio que va a 1
        bAnoExiste = false;
      }
      else {
        //update: solo al campo en cuestion
        bAnoExiste = true;
        iNM = rset.getInt(sNombreColum);
      }
    }

    if (bAnoExiste) {

      rset.close();
      rset = null;
      stat.close();
      stat = null;

      sQuery = "UPDATE SIVE_SEC_ANUAL SET " + sNombreColum + " = ? " +
          " WHERE " + sNombreColumAno + " = ?";

      stat = conec.prepareStatement(sQuery);
      iNM++;
      stat.setInt(1, iNM);
      stat.setString(2, sValorAno);

      stat.executeUpdate();
      stat.close();
      stat = null;

    } //existe el a�o

    else if (!bAnoExiste) {
      //Insert de todo a 0 excepto de XXX a 1
      iNM = 1;
      int i = 0;
      ResultSetMetaData rsmd = rset.getMetaData(); //no lo cerre antes
      int numCols = rsmd.getColumnCount();

      sQuery = "INSERT INTO SIVE_SEC_ANUAL " +
          "( " + sNombreColumAno + ", " + sNombreColum;

      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColumAno) &&
            !rsmd.getColumnLabel(i).equals(sNombreColum)) {
          sQuery = sQuery + ", " + rsmd.getColumnLabel(i);
        }
      }
      sQuery = sQuery + ") ";
      sQuery = sQuery + " values (?, ?";

      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColumAno) &&
            !rsmd.getColumnLabel(i).equals(sNombreColum)) {
          sQuery = sQuery + ", " + "?";
        }
      }
      sQuery = sQuery + " ) ";

      int iValor = 1;

      stat = conec.prepareStatement(sQuery);
      stat.setString(iValor, sValorAno);
      iValor++;
      stat.setInt(iValor, 1);
      iValor++;
      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColumAno) &&
            !rsmd.getColumnLabel(i).equals(sNombreColum)) {
          stat.setInt(iValor, 0);
          iValor++;
        }
      }

      stat.executeUpdate();
      stat.close();
      stat = null;

    } //no existe el a�o

    return (iNM);
  }

} // Fin SrvAlerta
