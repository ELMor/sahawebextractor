/* Servlet para la recuperaci�n de la BD de los datos a mostrar en la
 * tabla del PanBusca mostrando a todos los usuarios.
 * Es un Copy&Paste del servlet SrvSelUsuTub desarrollado para tuberculosis
 *      Fecha           Autor             Accion
 *    11/04/2000        JRM               La escribe
 */
package brotes.servidor.mantus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;

// Paquete de datos.
import brotes.datos.mantus.DataUsuario;
import capp.CLista;
import comun.constantes;
import sapp.DBServlet;

/**
 *  Obtenci�n de datos de usuario
 */
public class SrvSelUsuBro
    extends DBServlet {

  // modos de operaci�n
  static final int modoSELECCION_COD = constantes.sSELECCION_X_CODIGO;
  static final int modoSELECCION_DES = constantes.sSELECCION_X_DESCRIPCION;
  static final int modoMOD = constantes.modoMODIFICACION;

  /**
   * Funcionalidad del servlet
   */
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    //Para �ndice b�squeda columnas
    int k;
    // objetos de datos
    CLista data = null;
    DataUsuario datUsuario = null;

    //Campos que se reciben/envian en la DataUsuario
    //C�digo de usuario
    String sCodUsu = "";
    //Com. aut�noma
    String sCodComAut = "";
    //Nombre
    String sDesNom = "";
    //perfil
    String sPer = "";
    //Ambito del usuario (varios String)
    Vector vAmbUsu = new Vector();
    //Flag indica si usuario puede ver datos del enfermo
    boolean bValidar = false;

    Enumeration enum;
    String sAmbUsu = "";
    String sValidar = "";
    String sCodApl = "";

    // Querys
    String sQueryDel = "";
    String sQueryDel1 = "";
    String sQuery = "";
    String sQuery1 = "";

    //Date para recoger y meter fechas en b.datos
    java.sql.Date dFecAlt,
        dFecBaj;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      datUsuario = (DataUsuario) param.firstElement();

      // modos de operaci�n
      switch (opmode) {

        // listado selecci�n por c�digo
        case modoSELECCION_COD:

          //Borrado previo de autorizaciones que no est�n autorizados en USUARIO_GRUPO
          sQueryDel = "delete SIVE_AUTORIZACIONES_BROTE where CD_USUARIO not in (select COD_USUARIO CD_USUARIO from USUARIO_GRUPO where COD_APLICACION = ?)";
          // prepara la query
          st = con.prepareStatement(sQueryDel);
          // filtros
          st.setString(1, datUsuario.getCodApl());

          st.executeUpdate();

          st.close();
          st = null;

          //Borrado previo de usuarios que no est�n autorizados en USUARIO_GRUPO
          sQueryDel1 = " delete SIVE_USUARIO_BROTE where " +
              " CD_USUARIO not in (select COD_USUARIO CD_USUARIO from USUARIO_GRUPO where COD_APLICACION = ?) ";

          // prepara la query
          st = con.prepareStatement(sQueryDel1);
          // filtros
          st.setString(1, datUsuario.getCodApl());

          st.executeUpdate();

          st.close();
          st = null;

          //Selecci�n de la lista de usuarios por codigo
          String sUsu = datUsuario.getCodUsu();

          if (sUsu.equals("")) {
            sQuery = "select a.COD_USUARIO, a.NOMBRE, b.COD_GRUPO" +
                " from USUARIO a, USUARIO_GRUPO b where a.COD_USUARIO=b.COD_USUARIO" +
                " and b.COD_APLICACION = ? order by a.COD_USUARIO";
          }
          else {
            sQuery = "select a.COD_USUARIO, a.NOMBRE, b.COD_GRUPO" +
                " from USUARIO a, USUARIO_GRUPO b where a.COD_USUARIO=b.COD_USUARIO" +
                " and b.COD_USUARIO = ? and b.COD_APLICACION = ? order by a.COD_USUARIO";
          }

          // prepara la query
          st = con.prepareStatement(sQuery);

          // prepara la lista de resultados
          data = new CLista();

          // filtros
          if (sUsu.equals("")) {
            st.setString(1, datUsuario.getCodApl()); //Lo debe coger del applet.
          }
          else {
            st.setString(1, datUsuario.getCodUsu());
            st.setString(2, datUsuario.getCodApl()); //Lo debe coger del applet.
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // obtiene los campos
            sCodUsu = rs.getString("COD_USUARIO");
            sDesNom = rs.getString("NOMBRE");
            sValidar = getIT_FG_VALIDAR(con, sCodUsu);
            sPer = getPER(con, sCodUsu);

            if (sValidar == null) {
              sValidar = "";
            }
            if (sPer == null) {
              sPer = "";

              // El usuario permite validar
            }
            bValidar = sValidar.equals("S");

            vAmbUsu = getNiv(con, sCodUsu);

            // a�ade un nodo
            data.addElement(new DataUsuario(sCodUsu,
                                            sDesNom,
                                            sCodComAut,
                                            sPer,
                                            bValidar,
                                            vAmbUsu,
                                            sCodApl)
                            );
          }

          rs.close();
          st.close();

          rs = null;
          st = null;

          break;

          // listado selecci�n por nombre
        case modoSELECCION_DES:

          //Borrado previo de autorizaciones que no est�n autorizados en USUARIO_GRUPO
          sQueryDel = "delete SIVE_AUTORIZACIONES_BROTE where CD_USUARIO not in (select COD_USUARIO CD_USUARIO from USUARIO_GRUPO where COD_APLICACION = ?)";
          // prepara la query
          st = con.prepareStatement(sQueryDel);
          // filtros
          st.setString(1, datUsuario.getCodApl());

          st.executeUpdate();

          st.close();
          st = null;

          //Borrado previo de usuarios que no est�n autorizados en USUARIO_GRUPO
          sQueryDel1 = " delete SIVE_USUARIO_BROTE where " +
              " CD_USUARIO not in (select COD_USUARIO CD_USUARIO from USUARIO_GRUPO where COD_APLICACION = ?)";

          // prepara la query
          st = con.prepareStatement(sQueryDel1);
          // filtros
          st.setString(1, datUsuario.getCodApl());

          st.close();
          st = null;

          //Selecci�n de usuarios por nombre
          String sNom = datUsuario.getDesNom();

          if (sNom.equals("")) {
            sQuery = "select a.COD_USUARIO, a.NOMBRE, b.COD_GRUPO" +
                " from USUARIO a, USUARIO_GRUPO b where a.COD_USUARIO=b.COD_USUARIO" +
                " and b.COD_APLICACION = ? order by a.NOMBRE";
          }
          else {
            sQuery = "select a.COD_USUARIO, a.NOMBRE, b.COD_GRUPO" +
                " from USUARIO a, USUARIO_GRUPO b where a.COD_USUARIO=b.COD_USUARIO" +
                " and a.NOMBRE = ? and b.COD_APLICACION = ? order by a.NOMBRE";
          }

          // prepara la query
          st = con.prepareStatement(sQuery);

          // prepara la lista de resultados
          data = new CLista();

          // filtros
          if (sNom.equals("")) {
            st.setString(1, datUsuario.getCodApl()); //Lo debe coger del applet.
          }
          else {
            st.setString(1, datUsuario.getDesNom());
            st.setString(2, datUsuario.getCodApl()); //Lo debe coger del applet.
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // obtiene los campos
            sCodUsu = rs.getString("COD_USUARIO");
            sDesNom = rs.getString("NOMBRE");
            sValidar = getIT_FG_VALIDAR(con, sCodUsu);
            sPer = getPER(con, sCodUsu);

            if (sValidar == null) {
              sValidar = "";
            }
            if (sPer == null) {
              sPer = "";

            }
            bValidar = sValidar.equals("S");

            getNiv(con, sCodUsu);

            vAmbUsu = getNiv(con, sCodUsu);

            // a�ade un nodo
            data.addElement(new DataUsuario(sCodUsu,
                                            sDesNom,
                                            sCodComAut,
                                            sPer,
                                            bValidar,
                                            vAmbUsu,
                                            sCodApl)
                            );
          }

          rs.close();
          st.close();

          rs = null;
          st = null;

          break;

        case modoMOD:

          //Comprobaci�n de CD_USUARIO en SIVE_USUARIO_BROTE
          sQuery =
              "select CD_USUARIO from SIVE_USUARIO_BROTE where CD_USUARIO = ?";

          // prepara la query
          st = con.prepareStatement(sQuery);

          // prepara la lista de resultados
          data = new CLista();

          // filtros
          st.setString(1, datUsuario.getCodUsu());

          rs = st.executeQuery();

          if (rs.next()) { //modifica el registro existente
            sQuery1 = "update SIVE_USUARIO_BROTE SET IT_PERFILUSU = ?, IT_FG_VALIDAR = ? where CD_USUARIO = ? and CD_CA = ?";

            // prepara la query
            st = con.prepareStatement(sQuery1);

            // prepara la lista de resultados
            data = new CLista();

            // filtros
            st.setString(1, datUsuario.getPer());

            bValidar = datUsuario.getValidar();
            if (bValidar) {
              st.setString(2, "S");
            }
            else {
              st.setString(2, "N");
            }

            st.setString(3, datUsuario.getCodUsu());
            st.setString(4, datUsuario.getCodComAut());

            st.executeUpdate();
          }
          else {
            //hace el insert
            sQuery1 = "insert into SIVE_USUARIO_BROTE (CD_USUARIO, CD_CA, IT_PERFILUSU, IT_FG_VALIDAR) values (?, ?, ?, ?)";

            // prepara la query
            st = con.prepareStatement(sQuery1);

            // prepara la lista de resultados
            data = new CLista();

            // filtros
            st.setString(1, datUsuario.getCodUsu());
            st.setString(2, datUsuario.getCodComAut());
            st.setString(3, datUsuario.getPer());
            bValidar = datUsuario.getValidar();

            if (bValidar) {
              st.setString(4, "S");
            }
            else {
              st.setString(4, "N");
            }

            st.executeUpdate();
          }

          rs.close();
          st.close();

          rs = null;
          st = null;
          //Se actualiza Sive_actualizaciones, primero se borran todos los registros
          //y a continucaci�n se insertan los nuevos

          vAmbUsu = datUsuario.getAmbUsu();
          //if (vAmbUsu.size()>0){
          //borra y actualiza
          ActualizarAmbitos(con, vAmbUsu, datUsuario);
          //}
          break;
      } //fin switch

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      data = null;
      throw ex;
    }
    finally {
      closeConnection(con);
    }

    // Cierre de la conexion con la BD
    //closeConnection(con);

    return data;
  } // Fin doWork()

  // Obtiene el IT_FG_VALIDAR de SIVE_USUARIO_BROTE
  String getIT_FG_VALIDAR(Connection c, String CodUsu) throws Exception {
    if (CodUsu == null || CodUsu.equals("")) {
      return "";
    }

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Query
    String Query =
        "select IT_FG_VALIDAR from SIVE_USUARIO_BROTE where CD_USUARIO = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);

      // CodTLab
      st.setString(1, CodUsu.trim());

      // Ejecucion de la query
      rs = st.executeQuery();

      // Almacenamos la IT_FG_VALIDAR obtenida
      if (rs.next()) {
        res = rs.getString("IT_FG_VALIDAR");

        // Cierre del ResultSet y del PreparedStatement
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      c.rollback();
      throw ex;
    }

    return res;
  } // Fin getIT_FG_VALIDAR()

  // Obtiene el IT_PERFILUSU de SIVE_USUARIO_BROTE
  String getPER(Connection c, String CodUsu) throws Exception {
    if (CodUsu == null || CodUsu.equals("")) {
      return "";
    }

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Query
    String Query =
        "select IT_PERFILUSU from SIVE_USUARIO_BROTE where CD_USUARIO = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);

      // CodTLab
      st.setString(1, CodUsu.trim());

      // Ejecucion de la query
      rs = st.executeQuery();

      // Almacenamos la IT_PERFILUSU obtenida
      if (rs.next()) {
        res = rs.getString("IT_PERFILUSU");

        // Cierre del ResultSet y del PreparedStatement
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }

    return res;
  } // Fin getPER()

  Vector getNiv(Connection c, String CodUsu) throws Exception {
    //if(CodUsu == null || CodUsu.equals("")) return "";
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    Vector vUsu = null;
    // Query
    String Query = "select CD_NIVEL_1,CD_NIVEL_2 from SIVE_AUTORIZACIONES_BROTE where CD_USUARIO = ?";

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);
      st.setString(1, CodUsu.trim());

      rs = st.executeQuery();

      // extrae la p�gina requerida
      String sCodNiv1, sCodNiv2;

      //DataUsuario datUsuario = new DataUsuario();
      vUsu = new Vector();

      while (rs.next()) {
        // obtiene los campos
        sCodNiv1 = rs.getString("CD_NIVEL_1");
        sCodNiv2 = rs.getString("CD_NIVEL_2");

        // a�ade un nodo
        vUsu.addElement(sCodNiv1);
        vUsu.addElement(sCodNiv2);
      }

      rs.close();
      st.close();

      st = null;
      rs = null;
    }
    catch (Exception ex) {
      throw ex;
    }
    return vUsu;
  }

  void ActualizarAmbitos(Connection c, Vector AmbUsu, DataUsuario datUsuarioA) throws
      Exception {
    //DataUsuario datUsuario = new DataUsuario();
    //Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    Vector vAmbUsu = new Vector();
    c.setAutoCommit(false);
    //BORRADO PREVIOD DE SIVE_AUTORIZACIONES_BROTE
    String sQueryDel =
        "delete from SIVE_AUTORIZACIONES_BROTE where CD_USUARIO = ?";

    try {
      // prepara la query
      st = c.prepareStatement(sQueryDel);

      // filtros
      st.setString(1, datUsuarioA.getCodUsu());

      st.executeUpdate();

      st.close();
      st = null;

      if (AmbUsu != null) {
        //Nuevo item en tabla SIVE_AUTORIZACIONES_BROTE
        String sQueryA = "insert into SIVE_AUTORIZACIONES_BROTE (CD_CA, CD_NIVEL_1, CD_NIVEL_2, CD_SECUENCIAL, CD_USUARIO) values( ?, ?, ?, ?, ?)";

        //Recorremos el vector de ambitos e insertamos una linea por cada ambito en tabla autorizaciones
        Enumeration enum;
        vAmbUsu = AmbUsu;
        //vAmbUsu= datUsuario.getAmbUsu();
        String ambito = null;
        String nivel2 = null;

        enum = vAmbUsu.elements();
        int z = 0;
        while (enum.hasMoreElements()) {
          ambito = (String) (enum.nextElement()); // nivel1
          nivel2 = (String) (enum.nextElement()); // nivel2

          // prepara la query
          st = c.prepareStatement(sQueryA);

          // cod Com Aut�noma
          st.setString(1, datUsuarioA.getCodComAut().trim());

          //Codigo en perfil para CD_NIVEL_1 CD_NIVEL_2*********************
          if (datUsuarioA.getPer().equals("3")) {
            st.setString(2, ambito.trim());
            st.setNull(3, java.sql.Types.VARCHAR);
          }
          else if (datUsuarioA.getPer().equals("4")) {
            st.setString(2, ambito.trim());
            st.setString(3, nivel2.trim());
          }

          //Cod secuencial
          z++;
          st.setInt(4, z); //????????????????????????????????????????Secuencial*******
          //Cod usuario
          st.setString(5, datUsuarioA.getCodUsu().trim());

          // lanza la query
          st.executeUpdate();

          st.close();
          st = null;
        } // fin while
      } // fin if

    }
    catch (Exception ex) {
      throw ex;
    }

  }

  static public void main(String args[]) {
    /*
         SrvSelUsuBro srv = new SrvSelUsuBro();
         CLista listaSalida = new CLista();
         DataUsuario hs = new DataUsuario();
         //DataUsuario hs = new DataUsuario(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
         CLista resultado = null;
         // par�metros jdbc
         srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
         //hs.setCodUsu("");
         hs.setCodUsu("CA_1");
         //hs.setDesNom("Pedro Antonio Diaz");  //si entro por nombre
         //hs.setPer("2");
         //hs.setEnf(true);
         hs.setCodApl("BROTE");
         listaSalida.addElement(hs);
         resultado = srv.doDebug(constantes.sSELECCION_X_CODIGO, listaSalida);
         resultado = null;
     */
  }

} // Fin SrvSelUsu
