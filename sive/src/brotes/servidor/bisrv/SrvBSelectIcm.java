package brotes.servidor.bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import brotes.datos.bidata.DataAlerta;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvBSelectIcm
    extends DBServlet {

  final int servletSELECT = 0;
  final int servletSELECT_HOSP = 1;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  private String fecha_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    return f.substring(0, f.indexOf(' ', 0));
  }

  private String hora_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    f = f.substring(f.indexOf(' ', 0) + 1); //descartar ss
    f = f.substring(0, 5);
    return f;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    DataAlerta dataEntrada = null;
    CLista listaSalida = new CLista();
    DataAlerta dataSalida = new DataAlerta();

    //querys
    String sQuery = "";

    //fechas
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //STRING
    String desMunicipio = null;
    String codCadef = null;
    String desNivel1 = null;
    String desNivel2 = null;
    String desZB = null;
    String desNivel1_normal = null;
    String desNivel2_normal = null;
    String desequipo = null;
    String codcentro = null;
    String descentro = null;

    boolean hayRegistro = false;

    try {

      switch (opmode) {

        //con SUCA cambia la seleccion de descripciones de pais, ca, prov y mun
        case servletSELECT:

          dataEntrada = (DataAlerta) param.firstElement();

//!!!!!!!!!!!!!!!!!!!!!!
//!!!!BROTES!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!

          System.out.println("SrvBSelectIcm: antes Pregunta_Select_Brotes()");
          System.out.println("SrvBSelectIcm: " + Pregunta_Select_Brotes());
          System.out.println("SrvBSelectIcm: a�o " +
                             dataEntrada.getCD_ANO().trim());
          System.out.println("SrvBSelectIcm: nm_alerbro " +
                             dataEntrada.getNM_ALERBRO().trim());

          sQuery = Pregunta_Select_Brotes();
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2,
                    (new Integer(dataEntrada.getNM_ALERBRO().trim())).intValue());

          rs = st.executeQuery();

          //NORMALES
          String FC_ALERBRO = "";
          java.util.Date dFC_ALERBRO;
          String FC_FECVALID = "";
          java.util.Date dFC_FECVALID;
          //TS
          java.sql.Timestamp dFC_FECHAHORA = null;
          String FC_FECHAHORA_F = "";
          String FC_FECHAHORA_H = "";
          java.sql.Timestamp dFC_EXPOSICION = null;
          String FC_EXPOSICION_F = "";
          String FC_EXPOSICION_H = "";
          java.sql.Timestamp dFC_INISINTOMAS = null;
          String FC_INISINTOMAS_F = "";
          String FC_INISINTOMAS_H = "";
          //ULTACT
          fecRecu = null;
          sfecRecu = "";

          // extrae el registro encontrado
          while (rs.next()) {

            hayRegistro = true;

            //campos criticos:-----------------------------------
            FC_ALERBRO = formater.format(rs.getDate("FC_ALERBRO"));
            dFC_FECVALID = rs.getDate("FC_FECVALID");
            if (dFC_FECVALID == null) {
              FC_FECVALID = null;
            }
            else {
              FC_FECVALID = formater.format(dFC_FECVALID);

              //TS
            }
            dFC_FECHAHORA = rs.getTimestamp("FC_FECHAHORA");
            if (dFC_FECHAHORA == null) {
              FC_FECHAHORA_F = null;
              FC_FECHAHORA_H = null;
            }
            else {
              FC_FECHAHORA_F = fecha_de_fecha(timestamp_a_cadena(dFC_FECHAHORA));
              FC_FECHAHORA_H = hora_de_fecha(timestamp_a_cadena(dFC_FECHAHORA));
            }

            dFC_EXPOSICION = rs.getTimestamp("FC_EXPOSICION");
            if (dFC_EXPOSICION == null) {
              FC_EXPOSICION_F = null;
              FC_EXPOSICION_H = null;
            }
            else {
              FC_EXPOSICION_F = fecha_de_fecha(timestamp_a_cadena(
                  dFC_EXPOSICION));
              FC_EXPOSICION_H = hora_de_fecha(timestamp_a_cadena(dFC_EXPOSICION));
            }

            dFC_INISINTOMAS = rs.getTimestamp("FC_INISINTOMAS");
            if (dFC_INISINTOMAS == null) {
              FC_INISINTOMAS_F = null;
              FC_INISINTOMAS_H = null;
            }
            else {
              FC_INISINTOMAS_F = fecha_de_fecha(timestamp_a_cadena(
                  dFC_INISINTOMAS));
              FC_INISINTOMAS_H = hora_de_fecha(timestamp_a_cadena(
                  dFC_INISINTOMAS));
            }

            fecRecu = rs.getTimestamp("FC_ULTACT");
            sfecRecu = timestamp_a_cadena(fecRecu);
            //-----------------------------------------------------

            dataSalida.insert( (String) "CD_ANO", rs.getString("CD_ANO"));
            dataSalida.insert( (String) "NM_ALERBRO",
                              (new Integer(rs.getString("NM_ALERBRO"))).
                              toString().trim());
            dataSalida.insert( (String) "CD_LEXPROV", rs.getString("CD_LEXPROV"));
            dataSalida.insert( (String) "CD_LEXMUN", rs.getString("CD_LEXMUN"));

            dataSalida.insert( (String) "FC_FECHAHORA_F", FC_FECHAHORA_F);
            dataSalida.insert( (String) "FC_FECHAHORA_H", FC_FECHAHORA_H);

            dataSalida.insert( (String) "CD_NIVEL_1_EX",
                              rs.getString("CD_NIVEL_1_EX"));
            dataSalida.insert( (String) "CD_NIVEL_2_EX",
                              rs.getString("CD_NIVEL_2_EX"));
            dataSalida.insert( (String) "CD_ZBS_EX", rs.getString("CD_ZBS_EX"));
            dataSalida.insert( (String) "CD_NIVEL_1", rs.getString("CD_NIVEL_1"));
            dataSalida.insert( (String) "CD_NIVEL_2", rs.getString("CD_NIVEL_2"));
            dataSalida.insert( (String) "CD_E_NOTIF", rs.getString("CD_E_NOTIF"));
            dataSalida.insert( (String) "DS_ALERTA", rs.getString("DS_ALERTA"));
            dataSalida.insert( (String) "CD_GRUPO", rs.getString("CD_GRUPO"));
            dataSalida.insert( (String) "NM_EXPUESTOS",
                              rs.getString("NM_EXPUESTOS"));
            dataSalida.insert( (String) "NM_ENFERMOS",
                              rs.getString("NM_ENFERMOS"));
            dataSalida.insert( (String) "NM_INGHOSP", rs.getString("NM_INGHOSP"));

            dataSalida.insert( (String) "FC_EXPOSICION_F", FC_EXPOSICION_F);
            dataSalida.insert( (String) "FC_EXPOSICION_H", FC_EXPOSICION_H);
            dataSalida.insert( (String) "FC_INISINTOMAS_F", FC_INISINTOMAS_F);
            dataSalida.insert( (String) "FC_INISINTOMAS_H", FC_INISINTOMAS_H);

            dataSalida.insert( (String) "DS_LEXP", rs.getString("DS_LEXP"));
            dataSalida.insert( (String) "DS_LEXPDIREC",
                              rs.getString("DS_LEXPDIREC"));
            dataSalida.insert( (String) "DS_LEXPPISO",
                              rs.getString("DS_LEXPPISO"));
            dataSalida.insert( (String) "CD_LEXPPOST",
                              rs.getString("CD_LEXPPOST"));
            dataSalida.insert( (String) "DS_LNMCALLE",
                              rs.getString("DS_LNMCALLE"));
            dataSalida.insert( (String) "DS_LEXPTELEF",
                              rs.getString("DS_LEXPTELEF"));
            dataSalida.insert( (String) "CD_SITALERBRO",
                              rs.getString("CD_SITALERBRO"));
            dataSalida.insert( (String) "FC_ALERBRO", FC_ALERBRO);
            dataSalida.insert( (String) "IT_VALIDADA",
                              rs.getString("IT_VALIDADA"));
            dataSalida.insert( (String) "FC_FECVALID", FC_FECVALID);
            dataSalida.insert( (String) "CD_OPE", rs.getString("CD_OPE"));
            dataSalida.insert( (String) "FC_ULTACT", sfecRecu);

          } //fin del while

          rs.close();
          rs = null;
          st.close();
          st = null;

          System.out.println(
              "SrvBSelectIcm: despues de Pregunta_Select_Brotes ");

          if (!hayRegistro) {
            listaSalida = new CLista();
            break;
          }

          //completamos las descripciones

          // MUN y CA
          if (dataSalida.getCD_LEXPROV() != null &&
              dataSalida.getCD_LEXMUN() != null) {

            System.out.println(
                "SrvBSelectIcm: antes de Pregunta_Select_Municipio");

            sQuery = Pregunta_Select_Municipio();

            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_LEXPROV().trim());
            st.setString(2, dataSalida.getCD_LEXMUN().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              desMunicipio = rs.getString("DS_MUN");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues de Pregunta_Select_Municipio");
          }

          // recupera el c�digo de la comunidad aut�noma
          if (dataSalida.getCD_LEXPROV() != null) {
            sQuery = "select CD_CA from SIVE_PROVINCIA where CD_PROV = ?";

            System.out.println(
                "SrvBSelectIcm: antes de recupera el c�digo de la comunidad aut�noma ");

            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_LEXPROV().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              codCadef = rs.getString("CD_CA");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues de recupera el c�digo de la comunidad aut�noma ");

          }

          //fin de mun, prov, ca,,,,,,

          //nivel 1
          if (dataSalida.getCD_NIVEL_1_EX() != null) {
            System.out.println(
                "SrvBSelectIcm: antes de Pregunta_Select_Nivel1 ");
            sQuery = Pregunta_Select_Nivel1();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_EX().trim());
            rs = st.executeQuery();
            String desN1 = "";
            String desLN1 = "";

            while (rs.next()) {
              desN1 = rs.getString("DS_NIVEL_1");
              desLN1 = rs.getString("DSL_NIVEL_1");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN1 != null)) {
                desNivel1 = desLN1;
              }
              else {
                desNivel1 = desN1;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues de Pregunta_Select_Nivel1 ");
          }

          //nivel2
          if (dataSalida.getCD_NIVEL_1_EX() != null &&
              dataSalida.getCD_NIVEL_2_EX() != null) {

            System.out.println(
                "SrvBSelectIcm: despues de Pregunta_Select_Nivel2 ");
            sQuery = Pregunta_Select_Nivel2();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_EX().trim());
            st.setString(2, dataSalida.getCD_NIVEL_2_EX().trim());
            rs = st.executeQuery();
            String desN2 = "";
            String desLN2 = "";

            while (rs.next()) {
              desN2 = rs.getString("DS_NIVEL_2");
              desLN2 = rs.getString("DSL_NIVEL_2");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN2 != null)) {
                desNivel2 = desLN2;
              }
              else {
                desNivel2 = desN2;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues de Pregunta_Select_Nivel2 ");
          }

          //Zona Basica
          if (dataSalida.getCD_NIVEL_1_EX() != null &&
              dataSalida.getCD_NIVEL_2_EX() != null &&
              dataSalida.getCD_ZBS_EX() != null) {

            System.out.println(
                "SrvBSelectIcm: antes de Pregunta_Select_ZonaBasica ");
            sQuery = Pregunta_Select_ZonaBasica();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_EX().trim());
            st.setString(2, dataSalida.getCD_NIVEL_2_EX().trim());
            st.setString(3, dataSalida.getCD_ZBS_EX().trim());
            rs = st.executeQuery();
            String desZona = "";
            String desLZona = "";

            while (rs.next()) {
              desZona = rs.getString("DS_ZBS");
              desLZona = rs.getString("DSL_ZBS");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (rs.getString("DSL_ZBS") != null)) {
                desZB = desLZona;
              }
              else {
                desZB = desZona;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues de Pregunta_Select_ZonaBasica ");
          }

          //nivel 1 normal
          if (dataSalida.getCD_NIVEL_1() != null) {
            sQuery = Pregunta_Select_Nivel1();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1().trim());
            rs = st.executeQuery();
            String desN1 = "";
            String desLN1 = "";

            while (rs.next()) {
              desN1 = rs.getString("DS_NIVEL_1");
              desLN1 = rs.getString("DSL_NIVEL_1");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN1 != null)) {
                desNivel1_normal = desLN1;
              }
              else {
                desNivel1_normal = desN1;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues de Pregunta_Select_Nivel1 normal");

          }

          //nivel2 normal
          if (dataSalida.getCD_NIVEL_1() != null &&
              dataSalida.getCD_NIVEL_2() != null) {
            sQuery = Pregunta_Select_Nivel2();
            System.out.println("SrvBSelectIcm:  Pregunta_Select_Nivel2 " +
                               sQuery);
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1().trim());
            st.setString(2, dataSalida.getCD_NIVEL_2().trim());
            rs = st.executeQuery();
            String desN2 = "";
            String desLN2 = "";

            while (rs.next()) {
              desN2 = rs.getString("DS_NIVEL_2");
              System.out.println("SrvBSelectIcm:  despues DS_NIVEL_2 ");
              desLN2 = rs.getString("DSL_NIVEL_2");
              System.out.println("SrvBSelectIcm:  despues DSL_NIVEL_2 ");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN2 != null)) {
                desNivel2_normal = desLN2;
              }
              else {
                desNivel2_normal = desN2;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues de Pregunta_Select_Nivel2 normal");
          }

          //equipo
          if (dataSalida.getCD_E_NOTIF() != null) {
            sQuery = Pregunta_Select_Equipo_Notificador();
            System.out.println(
                "SrvBSelectIcm: Pregunta_Select_Equipo_Notificador " + sQuery);
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_E_NOTIF().trim());
            rs = st.executeQuery();

            while (rs.next()) {
              desequipo = rs.getString("DS_E_NOTIF");
              codcentro = rs.getString("CD_CENTRO");
              descentro = rs.getString("DS_CENTRO");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues de Pregunta_Select_Equipo_notificador normal");
          }

          //**********************

           //RECONSTRUIMOS DATASALIDA
           dataSalida.insert( (String) "DS_LEXMUN", desMunicipio);
          dataSalida.insert( (String) "CD_LEXCA", codCadef);
          dataSalida.insert( (String) "DS_NIVEL_1_EX", desNivel1);
          dataSalida.insert( (String) "DS_NIVEL_2_EX", desNivel2);
          dataSalida.insert( (String) "DS_ZBS_EX", desZB);
          dataSalida.insert( (String) "DS_NIVEL_1", desNivel1_normal);
          dataSalida.insert( (String) "DS_NIVEL_2", desNivel2_normal);
          dataSalida.insert( (String) "DS_E_NOTIF", desequipo);
          dataSalida.insert( (String) "CD_CENTRO", codcentro);
          dataSalida.insert( (String) "DS_CENTRO", descentro);

          //-------------------------------------------------------
//!!!!!!!!!!!!!!!!!!!!!!
//!!!!ADIC!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!
          sQuery = Pregunta_Select_Adic();
          System.out.println("SrvBSelectIcm: Pregunta_Select_Adic " + sQuery);
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2,
                    (new Integer(dataEntrada.getNM_ALERBRO().trim())).intValue());

          rs = st.executeQuery();

          while (rs.next()) {
            dataSalida.insert( (String) "CD_MINPROV", rs.getString("CD_MINPROV"));
            dataSalida.insert( (String) "CD_MINMUN", rs.getString("CD_MINMUN"));
            dataSalida.insert( (String) "CD_TNOTIF", rs.getString("CD_TNOTIF"));
            dataSalida.insert( (String) "DS_NOTIFINST",
                              rs.getString("DS_NOTIFINST"));
            dataSalida.insert( (String) "CD_NOTIFPROV",
                              rs.getString("CD_NOTIFPROV"));
            dataSalida.insert( (String) "DS_NOTIFICADOR",
                              rs.getString("DS_NOTIFICADOR"));
            dataSalida.insert( (String) "DS_NOTIFDIREC",
                              rs.getString("DS_NOTIFDIREC"));

            dataSalida.insert( (String) "DS_NOTIFPISO",
                              rs.getString("DS_NOTIFPISO"));

            dataSalida.insert( (String) "CD_NOTIFMUN",
                              rs.getString("CD_NOTIFMUN"));
            dataSalida.insert( (String) "CD_NOTIFPOSTAL",
                              rs.getString("CD_NOTIFPOSTAL"));
            dataSalida.insert( (String) "DS_NNMCALLE",
                              rs.getString("DS_NNMCALLE"));
            dataSalida.insert( (String) "DS_NOTIFTELEF",
                              rs.getString("DS_NOTIFTELEF"));
            dataSalida.insert( (String) "DS_MINFPER", rs.getString("DS_MINFPER"));
            dataSalida.insert( (String) "DS_MINFDIREC",
                              rs.getString("DS_MINFDIREC"));

            dataSalida.insert( (String) "DS_MINFPISO",
                              rs.getString("DS_MINFPISO"));

            dataSalida.insert( (String) "DS_MINFPOSTAL",
                              rs.getString("CD_MINFPOSTAL"));
            dataSalida.insert( (String) "DS_MINFTELEF",
                              rs.getString("DS_MINFTELEF"));
            dataSalida.insert( (String) "DS_MNMCALLE",
                              rs.getString("DS_MNMCALLE"));
            dataSalida.insert( (String) "DS_OBSERV", rs.getString("DS_OBSERV"));
            dataSalida.insert( (String) "DS_ALISOSP", rs.getString("DS_ALISOSP"));
            dataSalida.insert( (String) "DS_SINTOMAS",
                              rs.getString("DS_SINTOMAS"));

          } //fin del while
          rs.close();
          rs = null;
          st.close();
          st = null;

          System.out.println("SrvBSelectIcm: DESPUES Pregunta_Select_Adic ");

          //DESCRIPCIONES
          desMunicipio = null;
          codCadef = null;

          // MUN y CA
          if (dataSalida.getCD_MINPROV() != null &&
              dataSalida.getCD_MINMUN() != null) {

            sQuery = Pregunta_Select_Municipio();
            System.out.println("SrvBSelectIcm: Pregunta_Select_Municipio " +
                               sQuery);
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_MINPROV().trim());
            st.setString(2, dataSalida.getCD_MINMUN().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              desMunicipio = rs.getString("DS_MUN");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println(
                "SrvBSelectIcm: despues Pregunta_Select_Municipio ");

          }

          // recupera el c�digo de la comunidad aut�noma
          if (dataSalida.getCD_MINPROV() != null) {
            sQuery = "select CD_CA from SIVE_PROVINCIA where CD_PROV = ?";
            System.out.println("SrvBSelectIcm: Pregunta CA " + sQuery);
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_MINPROV().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              codCadef = rs.getString("CD_CA");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            System.out.println("SrvBSelectIcm: despues Pregunta CA ");
          }

          //fin de mun, prov, ca,,,,,,

          //resconstruir
          dataSalida.insert( (String) "CD_MINCA", codCadef);
          dataSalida.insert( (String) "DS_MINMUN", desMunicipio);

          //CONTINUAMOS CON LOS OTROS
          desMunicipio = null;
          codCadef = null;

          // MUN y CA  $$$
          if (dataSalida.getCD_NOTIFPROV() != null &&
              dataSalida.getCD_NOTIFMUN() != null) {

            sQuery = Pregunta_Select_Municipio();

            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NOTIFPROV().trim());
            st.setString(2, dataSalida.getCD_NOTIFMUN().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              desMunicipio = rs.getString("DS_MUN");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

          }

          // recupera el c�digo de la comunidad aut�noma
          if (dataSalida.getCD_NOTIFPROV() != null) {
            sQuery = "select CD_CA from SIVE_PROVINCIA where CD_PROV = ?";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NOTIFPROV().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              codCadef = rs.getString("CD_CA");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
          }

          //fin de mun, prov, ca,,,,,,

          //resconstruir
          dataSalida.insert( (String) "CD_NOTIFCA", codCadef);
          dataSalida.insert( (String) "DS_NOTIFMUN", desMunicipio);

//!!!!!!!!!!!!!!!!!!!!!!
//!!!!COLEC!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!
          sQuery = Pregunta_Select_Colec();
          System.out.println("SrvBSelectIcm: Pregunta_Select_Colec " + sQuery);
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2,
                    (new Integer(dataEntrada.getNM_ALERBRO().trim())).intValue());

          rs = st.executeQuery();

          while (rs.next()) {
            dataSalida.insert( (String) "CD_PROV", rs.getString("CD_PROV"));
            dataSalida.insert( (String) "CD_MUN", rs.getString("CD_MUN"));
            dataSalida.insert( (String) "DS_NOMCOL", rs.getString("DS_NOMCOL"));
            dataSalida.insert( (String) "DS_DIRCOL", rs.getString("DS_DIRCOL"));
            dataSalida.insert( (String) "DS_PISOCOL", rs.getString("DS_PISOCOL"));
            dataSalida.insert( (String) "DS_NMCALLE", rs.getString("DS_NMCALLE"));
            dataSalida.insert( (String) "CD_POSTALCOL",
                              rs.getString("CD_POSTALCOL"));
            dataSalida.insert( (String) "DS_TELCOL", rs.getString("DS_TELCOL"));
          } //fin del while
          rs.close();
          rs = null;
          st.close();
          st = null;
          System.out.println("SrvBSelectIcm: despues Pregunta_Select_Colec ");

          //DESCRIPCIONES
          desMunicipio = null;
          codCadef = null;

          // MUN y CA
          if (dataSalida.getCD_PROV() != null &&
              dataSalida.getCD_MUN() != null) {

            sQuery = Pregunta_Select_Municipio();

            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_PROV().trim());
            st.setString(2, dataSalida.getCD_MUN().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              desMunicipio = rs.getString("DS_MUN");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

          }

          // recupera el c�digo de la comunidad aut�noma
          if (dataSalida.getCD_PROV() != null) {
            sQuery = "select CD_CA from SIVE_PROVINCIA where CD_PROV = ?";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_PROV().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              codCadef = rs.getString("CD_CA");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
          }

          //fin de mun, prov, ca,,,,,,

          //resconstruir
          dataSalida.insert( (String) "CD_CA", codCadef);
          dataSalida.insert( (String) "DS_MUN", desMunicipio);

          //*****//*****//******//******//*******//********//*******

          //*****//*****//******//******//*******//********//*******

          listaSalida.addElement(dataSalida);

          break;

        case servletSELECT_HOSP:

          //entrada: un DataAlerta con CD_ANO y NM_ALERBRO
          //salida: una lista de DataAlertas (uno para cada hospital
          //        de la alerta)
          dataEntrada = (DataAlerta) param.firstElement();

          System.out.println("SrvBSelectIcm: antes Pregunta_Select_Hosp()");
          System.out.println("SrvBSelectIcm: " + Pregunta_Select_Hosp());
          System.out.println("SrvBSelectIcm: a�o " +
                             dataEntrada.getCD_ANO().trim());
          System.out.println("SrvBSelectIcm: nm_alerbro " +
                             dataEntrada.getNM_ALERBRO().trim());

          sQuery = Pregunta_Select_Hosp();
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2,
                    (new Integer(dataEntrada.getNM_ALERBRO().trim())).intValue());

          rs = st.executeQuery();

          // extrae el registro encontrado
          while (rs.next()) {
            dataSalida = new DataAlerta();
            dataSalida.insert( (String) "CD_ANO", dataEntrada.getCD_ANO().trim());
            dataSalida.insert( (String) "NM_ALERBRO",
                              dataEntrada.getNM_ALERBRO().trim());
            dataSalida.insert( (String) "NM_ALERHOSP",
                              (new Integer(rs.getString("NM_ALERHOSP"))).
                              toString().trim());
            dataSalida.insert( (String) "DS_HOSPITAL",
                              rs.getString("DS_HOSPITAL"));
            dataSalida.insert( (String) "FC_INGHOSP",
                              formater.format(rs.getDate("FC_INGHOSP")));
            dataSalida.insert( (String) "DS_OBSERV_HOSP",
                              rs.getString("DS_OBSERV"));
            listaSalida.addElement(dataSalida);
          } //fin del while

          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

  public String Pregunta_Select_Hosp() {
    return ("select "
            + " NM_ALERHOSP, DS_HOSPITAL,  "
            + " FC_INGHOSP, DS_OBSERV "
            + " from SIVE_HOSPITAL_ALERT where CD_ANO = ? AND NM_ALERBRO = ? ");

  }

  public String Pregunta_Select_Adic() {
    return ("select "
            + " CD_ANO, NM_ALERBRO, "
            + " CD_MINPROV, CD_MINMUN,  "
            + " CD_TNOTIF, DS_NOTIFINST, "
            + " CD_NOTIFPROV, DS_NOTIFICADOR, DS_NOTIFDIREC, "
            + " CD_NOTIFMUN, CD_NOTIFPOSTAL, DS_NNMCALLE, "
            + " DS_NOTIFTELEF, DS_MINFPER, DS_MINFDIREC, CD_MINFPOSTAL,  "
            + " DS_MINFTELEF,  DS_MNMCALLE, DS_OBSERV, "
            + " DS_ALISOSP,  DS_SINTOMAS, "
            + " DS_NOTIFPISO, DS_MINFPISO "
            + " from SIVE_ALERTA_ADIC where CD_ANO = ? AND NM_ALERBRO = ? ");

  }

  public String Pregunta_Select_Colec() {
    return ("select "
            + " CD_ANO, NM_ALERBRO, "
            + " CD_PROV, CD_MUN,  "
            + " DS_NOMCOL, DS_DIRCOL, "
            + " DS_NMCALLE, CD_POSTALCOL, DS_TELCOL, "
            + " DS_PISOCOL "
            + " from SIVE_ALERTA_COLEC where CD_ANO = ? AND NM_ALERBRO = ? ");

  }

  public String Pregunta_Select_Brotes() {

    return ("select "
            + " CD_ANO,  NM_ALERBRO, "
            + " CD_LEXPROV, CD_LEXMUN,  "
            + " FC_FECHAHORA, CD_NIVEL_1_EX, CD_NIVEL_2_EX,  CD_ZBS_EX,"
            + " CD_NIVEL_1, CD_NIVEL_2, CD_E_NOTIF, "
            + " DS_ALERTA, CD_GRUPO, NM_EXPUESTOS, NM_ENFERMOS, NM_INGHOSP, "
            + " FC_EXPOSICION, FC_INISINTOMAS, DS_LEXP,  "
            + " DS_LEXPDIREC,  CD_LEXPPOST, DS_LNMCALLE, "
            + " DS_LEXPTELEF,  CD_SITALERBRO, FC_ALERBRO, "
            + " IT_VALIDADA,  FC_FECVALID, CD_OPE, "
            + " FC_ULTACT, DS_LEXPPISO "
            + " from SIVE_ALERTA_BROTES where CD_ANO = ? AND NM_ALERBRO = ? ");
  }

  public String Pregunta_Select_Municipio() {
    return ("select DS_MUN "
            + " FROM SIVE_MUNICIPIO WHERE CD_PROV = ? AND CD_MUN = ? ");
  }

  public String Pregunta_Select_Nivel1() {
    return ("select "
            + " DS_NIVEL_1, DSL_NIVEL_1 "
            + " FROM  SIVE_NIVEL1_S  WHERE CD_NIVEL_1 = ? ");
  }

  public String Pregunta_Select_Nivel2() {
    return ("select "
            + " DS_NIVEL_2, DSL_NIVEL_2 "
            + " FROM  SIVE_NIVEL2_S  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?");
  }

  public String Pregunta_Select_ZonaBasica() {
    return ("select "
            + " DS_ZBS, DSL_ZBS "
            + " FROM  SIVE_ZONA_BASICA"
            + "  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND CD_ZBS = ? ");

  }

  public String Pregunta_Select_Equipo_Notificador() {
    return ("select a.DS_E_NOTIF, a.CD_CENTRO, b.DS_CENTRO "
            + " from SIVE_E_NOTIF a, SIVE_C_NOTIF b "
            + " where  a.CD_E_NOTIF= ? and a.CD_CENTRO = b.CD_CENTRO ");

  }
} //fin de la clase
