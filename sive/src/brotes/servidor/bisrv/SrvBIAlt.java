package brotes.servidor.bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Vector;

import brotes.datos.bidata.DatAlBro;
import brotes.datos.bidata.DataAlerta;
import brotes.datos.bidata.DataBusqueda;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvBIAlt
    extends DBServlet {

  final int servletSEL_ALTA_INF_BROTE = 0;
  final int servletSEL_ALTA_INV_BROTE = 1;

  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString().trim();

    String res = aux.substring(11); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  private String fecha_mas_hora(String f, String h) {
    f = f.trim();
    h = h.trim();
    //return f + " " + h.substring(0, h.lastIndexOf(':'));
    return f + " " + h + ":00";
  }

  private String fecha_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    return f.substring(0, f.indexOf(' ', 0));
  }

  private String hora_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    return f.substring(f.indexOf(' ', 0) + 1);
  }

  // E 28/01/2000
  /*public CLista doDebug(int opmode, CLista param) throws Exception {
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
      Connection con = openConnection();
      return doWork(opmode, param);
     } */

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 0;

    // objetos de datos
    CLista listaSalida = new CLista();
    DataBusqueda dataSalida = null;
    DataBusqueda dataEntrada = null;

    CLista listaAlertas = new CLista();
    CLista listaBrotes = new CLista();

    DataAlerta alerta = new DataAlerta();
    DatAlBro brote = new DatAlBro();

    DataAlerta ultAlerta = new DataAlerta();
    DatAlBro ultBrote = new DatAlBro();

    //querys
    String sQuery = "";
    int iValor = 1;
    int iTramar = 1;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //modos
    final int modoMODIFICACION = 4;
    final int modoCONSULTA = 6;

    String ds = "";
    String dsl = "";
    String sDes = "";

    try {

      switch (opmode) {
        case servletSEL_ALTA_INF_BROTE:
        case servletSEL_ALTA_INV_BROTE:

          //entrada: (DataBusqueda [, ultAlerta anterior trama, ultBrote anterior trama])

          dataEntrada = (DataBusqueda) param.firstElement();
          if (param.size() > 1) {
            ultAlerta = (DataAlerta) param.elementAt(1);
            ultBrote = (DatAlBro) param.elementAt(2);
          }

          //1. Para las alertas
          query = " select NM_ALERBRO, FC_FECHAHORA, "
              + " CD_NIVEL_1, CD_NIVEL_2, "
              + " CD_NIVEL_1_EX, CD_NIVEL_2_EX, "
              + " DS_ALERTA, FC_ALERBRO "
              + " from SIVE_ALERTA_BROTES  "
              + " where CD_ANO = ? and CD_SITALERBRO = ? ";

          if (ultAlerta.containsKey("NM_ALERBRO")
              && ultAlerta.containsKey("FC_FECHAHORA_F")
              && ultAlerta.containsKey("FC_FECHAHORA_H")) { //trama
            query = query + " and NM_ALERBRO > ? and FC_FECHAHORA > ? ";

          }
          query = query +
              " order by FC_FECHAHORA  DESC, CD_ANO  DESC,  NM_ALERBRO DESC";

          st = con.prepareStatement(query);
          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setString(2, "0");
          if (ultAlerta.containsKey("NM_ALERBRO")
              && ultAlerta.containsKey("FC_FECHAHORA_F")
              && ultAlerta.containsKey("FC_FECHAHORA_H")) {
            st.setInt(3,
                      (new Integer(ultAlerta.getNM_ALERBRO().trim())).intValue());
            if (ultAlerta.getFC_FECHAHORA_H().trim().length() > 0) {
              st.setTimestamp(4,
                              cadena_a_timestamp(fecha_mas_hora(ultAlerta.getFC_FECHAHORA_F().
                  trim(),
                  ultAlerta.getFC_FECHAHORA_H().trim())));
            }
            else {
              st.setTimestamp(4,
                              cadena_a_timestamp(fecha_mas_hora(ultAlerta.getFC_FECHAHORA_F().
                  trim(),
                  "00:00:00")));
            }
          }
          rs = st.executeQuery();

          boolean bSaltarReg = false;

          //fechaHora
          java.sql.Timestamp dFC_FECHAHORA = null;
          String FC_FECHAHORA_F = "";
          String FC_FECHAHORA_H = "";

          //modo
          int modo = 0;

          // autorizaciones
          String cd_nivel_1 = "";
          String cd_nivel_2 = "";
          String cd_nivel_1_ex = "";
          String cd_nivel_2_ex = "";

          //enteros
          int iN = 0;
          Integer IN = new Integer(iN);
          String sN = IN.toString();

          while (rs.next()) {

            //autoriz
            cd_nivel_1 = rs.getString("CD_NIVEL_1");
            cd_nivel_2 = rs.getString("CD_NIVEL_2");
            cd_nivel_1_ex = rs.getString("CD_NIVEL_1_EX");
            cd_nivel_2_ex = rs.getString("CD_NIVEL_2_EX");

            bSaltarReg = false;
            int iAut = autorizaciones(param, cd_nivel_1, cd_nivel_2,
                                      "", "",
                                      cd_nivel_1_ex, cd_nivel_2_ex);

            if (iAut == -1 || iAut == 2) {
              bSaltarReg = true; //no autorizado o error
            }
            else if (iAut == 0) {
              modo = modoMODIFICACION;
            }
            else if (iAut == 1) {
              modo = modoCONSULTA;

            }
            if (!bSaltarReg) {

              //enteros
              iN = rs.getInt("NM_ALERBRO");
              IN = new Integer(iN);
              sN = IN.toString();

              String FC_ALERBRO = formater.format(rs.getDate("FC_ALERBRO"));

              //fechahora
              dFC_FECHAHORA = rs.getTimestamp(2); //("FC_FECHAHORA");

              // fechaHora no puede ser nulo !!!!
              if (dFC_FECHAHORA == null) {
                FC_FECHAHORA_F = null;
                FC_FECHAHORA_H = null;
              }
              else {
                FC_FECHAHORA_F = fecha_de_fecha(timestamp_a_cadena(
                    dFC_FECHAHORA));
                FC_FECHAHORA_H = hora_de_fecha(timestamp_a_cadena(dFC_FECHAHORA));
              }

              alerta = new DataAlerta();
              alerta.insert( (String) "CD_ANO", dataEntrada.getCD_ANO().trim());
              alerta.insert( (String) "NM_ALERBRO", sN);
              alerta.insert( (String) "FC_FECHAHORA_H", FC_FECHAHORA_H);
              alerta.insert( (String) "FC_FECHAHORA_F", FC_FECHAHORA_F);
              alerta.insert( (String) "CD_SITALERBRO", "0");
              alerta.insert( (String) "FC_ALERBRO", FC_ALERBRO);
              alerta.insert( (String) "DS_ALERTA", rs.getString("DS_ALERTA"));

              listaAlertas.addElement(alerta);
            }

          } //while rs.next()

          rs.close();
          rs = null;
          st.close();
          st = null;

          /*or(int j= 0; j<listaAlertas.size(); j++){
            impHash((Hashtable)listaAlertas.elementAt(j));
           }*/

          if (opmode == servletSEL_ALTA_INF_BROTE) { //continuo

            //2. Para los brotes
            query = " select b.NM_ALERBRO, b.FC_FECHAHORA, "
                + " a.CD_NIVEL_1, a.CD_NIVEL_2, "
                + " b.CD_NIVEL_1_LE, b.CD_NIVEL_2_LE, "
                + " b.CD_NIVEL_1_LCA, b.CD_NIVEL_2_LCA, "
                + " FC_ALERBRO, "

                + " FC_FSINPRIMC, FC_ISINPRIMC, "
                + " NM_PERINMIN, NM_PERINMAX, NM_PERINMED, "
                + " b.NM_ENFERMOS, b.FC_EXPOSICION, b.NM_INGHOSP, "
                + " NM_DEFUNCION, b.NM_EXPUESTOS, "
                + " CD_TRANSMIS, NM_NVACENF, "
                + " b.CD_OPE, b.FC_ULTACT, "
                + " NM_DCUACMIN, NM_DCUACMED, NM_DCUACMAX, "
                + " NM_NVACNENF, NM_VACNENF, NM_VACENF, "
                + " DS_OBSERV, CD_TIPOCOL, DS_NOMCOL, DS_DIRCOL, "
                + " CD_POSTALCOL, CD_PROVCOL,  "
                + " DS_BROTE, b.CD_GRUPO, NM_MANIPUL, "
                + " CD_ZBS_LE,  CD_TNOTIF, IT_RESCALC, "
                + " CD_MUNCOL, DS_TELCOL, "
                + " CD_ZBS_LCA, "
                + " IT_PERIN, IT_DCUAC, DS_NMCALLE, CD_TBROTE "

                + " from SIVE_ALERTA_BROTES a, SIVE_BROTES b "
                + " where b.CD_ANO = ? and a.CD_SITALERBRO = ? "
                + " and a.CD_ANO = b.CD_ANO and a.NM_ALERBRO = b.NM_ALERBRO ";
            if (ultBrote.containsKey("NM_ALERBRO")
                && ultBrote.containsKey("FC_FECHAHORA_F")
                && ultBrote.containsKey("FC_FECHAHORA_H")) { //trama
              query = query + " and b.NM_ALERBRO > ? and b.FC_FECHAHORA > ? ";

            }
            query = query +
                " order by b.FC_FECHAHORA DESC, b.CD_ANO DESC, b.NM_ALERBRO DESC"; ;
            st = con.prepareStatement(query);
            st.setString(1, dataEntrada.getCD_ANO().trim());
            st.setString(2, "2");
            if (ultBrote.containsKey("NM_ALERBRO")
                && ultBrote.containsKey("FC_FECHA_F")
                && ultBrote.containsKey("FC_FECHA_H")) {
              st.setInt(3,
                        (new Integer(ultBrote.getNM_ALERBRO().trim())).intValue());
              if (ultBrote.getFC_FECHAHORA_H().trim().length() > 0) {
                st.setTimestamp(4,
                                cadena_a_timestamp(fecha_mas_hora(ultBrote.getFC_FECHAHORA_F().
                    trim(),
                    ultBrote.getFC_FECHAHORA_H().trim())));
              }
              else {
                st.setTimestamp(4,
                                cadena_a_timestamp(fecha_mas_hora(ultBrote.getFC_FECHAHORA_F().
                    trim(),
                    "00:00:00")));
              }
            }

            rs = st.executeQuery();

            bSaltarReg = false;

            //modo
            modo = 0;

            // autorizaciones
            String cd_nivel_1_lca = "";
            String cd_nivel_2_lca = "";

            //enteros
            iN = 0;
            IN = new Integer(iN);
            sN = IN.toString();

            while (rs.next()) {

              //autoriz
              cd_nivel_1 = rs.getString(3); //("CD_NIVEL_1");
              cd_nivel_2 = rs.getString(4); //("CD_NIVEL_2");
              cd_nivel_1_ex = rs.getString(5); //("CD_NIVEL_1_LE");
              cd_nivel_2_ex = rs.getString(6); //("CD_NIVEL_2_LE");
              cd_nivel_1_lca = rs.getString(7); //("CD_NIVEL_1_LCA");
              cd_nivel_2_lca = rs.getString(8); //("CD_NIVEL_2_LCA");

              bSaltarReg = false;
              int iAut = autorizaciones(param, cd_nivel_1, cd_nivel_2,
                                        cd_nivel_1_lca, cd_nivel_2_lca,
                                        cd_nivel_1_ex, cd_nivel_2_ex);

              if (iAut == -1 || iAut == 2) {
                bSaltarReg = true; //no autorizado o error
              }
              else if (iAut == 0) {
                modo = modoMODIFICACION;
              }
              else if (iAut == 1) {
                modo =
                    modoCONSULTA;

              }
              if (!bSaltarReg) {

                //enteros
                iN = rs.getInt(1); //("NM_ALERBRO");
                IN = new Integer(iN);
                sN = IN.toString();

                //fechahora
                dFC_FECHAHORA = rs.getTimestamp(2); //("FC_FECHAHORA");

                // fechaHora no puede ser nulo !!!!
                if (dFC_FECHAHORA == null) {
                  FC_FECHAHORA_F = null;
                  FC_FECHAHORA_H = null;
                }
                else {
                  FC_FECHAHORA_F = fecha_de_fecha(timestamp_a_cadena(
                      dFC_FECHAHORA));
                  FC_FECHAHORA_H = hora_de_fecha(timestamp_a_cadena(
                      dFC_FECHAHORA));
                }

                String FC_ALERBRO = formater.format(rs.getDate("FC_ALERBRO"));
                java.sql.Timestamp dFC_EXPOSICION = null;
                String FC_EXPOSICION_F = "";
                String FC_EXPOSICION_H = "";
                java.sql.Timestamp dFC_FSINPRIMC = null;
                String FC_FSINPRIMC_F = "";
                String FC_FSINPRIMC_H = "";
                java.sql.Timestamp dFC_ISINPRIMC = null;
                String FC_ISINPRIMC_F = "";
                String FC_ISINPRIMC_H = "";
                //ULTACT
                fecRecu = null;
                sfecRecu = "";

                //??
                dFC_FSINPRIMC = rs.getTimestamp("FC_FSINPRIMC");
                if (dFC_FSINPRIMC == null) {
                  FC_FSINPRIMC_F = null;
                  FC_FSINPRIMC_H = null;
                }
                else {
                  FC_FSINPRIMC_F = fecha_de_fecha(timestamp_a_cadena(
                      dFC_FSINPRIMC));
                  FC_FSINPRIMC_H = hora_de_fecha(timestamp_a_cadena(
                      dFC_FSINPRIMC));
                }

                dFC_ISINPRIMC = rs.getTimestamp("FC_ISINPRIMC");
                if (dFC_ISINPRIMC == null) {
                  FC_ISINPRIMC_F = null;
                  FC_ISINPRIMC_H = null;
                }
                else {
                  FC_ISINPRIMC_F = fecha_de_fecha(timestamp_a_cadena(
                      dFC_ISINPRIMC));
                  FC_ISINPRIMC_H = hora_de_fecha(timestamp_a_cadena(
                      dFC_ISINPRIMC));
                }

                dFC_EXPOSICION = rs.getTimestamp(16);
                if (dFC_EXPOSICION == null) {
                  FC_EXPOSICION_F = null;
                  FC_EXPOSICION_H = null;
                }
                else {
                  FC_EXPOSICION_F = fecha_de_fecha(timestamp_a_cadena(
                      dFC_EXPOSICION));
                  FC_EXPOSICION_H = hora_de_fecha(timestamp_a_cadena(
                      dFC_EXPOSICION));
                }

                fecRecu = rs.getTimestamp(23);
                sfecRecu = timestamp_a_cadena(fecRecu);

                brote = new DatAlBro();
                brote.insert( (String) "CD_ANO", dataEntrada.getCD_ANO().trim());
                brote.insert( (String) "NM_ALERBRO", sN);
                brote.insert( (String) "FC_FECHAHORA_F", FC_FECHAHORA_F);
                brote.insert( (String) "FC_FECHAHORA_H", FC_FECHAHORA_H);
                brote.insert( (String) "CD_SITALERBRO", "2");
                brote.insert( (String) "FC_ALERBRO", FC_ALERBRO);

                brote.insert( (String) "FC_FSINPRIMC_F", FC_FSINPRIMC_F);
                brote.insert( (String) "FC_FSINPRIMC_H", FC_FSINPRIMC_H);
                brote.insert( (String) "FC_ISINPRIMC_F", FC_ISINPRIMC_F);
                brote.insert( (String) "FC_ISINPRIMC_H", FC_ISINPRIMC_H);

                brote.insert( (String) "NM_PERINMIN",
                             rs.getString("NM_PERINMIN"));
                brote.insert( (String) "NM_PERINMAX",
                             rs.getString("NM_PERINMAX"));
                brote.insert( (String) "NM_PERINMED",
                             rs.getString("NM_PERINMED"));

                brote.insert( (String) "NM_ENFERMOS", rs.getString(15));
                brote.insert( (String) "FC_EXPOSICION_F", FC_EXPOSICION_F);
                brote.insert( (String) "FC_EXPOSICION_H", FC_EXPOSICION_H);
                brote.insert( (String) "NM_INGHOSP", rs.getString(17));
                brote.insert( (String) "NM_DEFUNCION",
                             rs.getString("NM_DEFUNCION"));
                brote.insert( (String) "NM_EXPUESTOS", rs.getString(19));

                brote.insert( (String) "CD_TRANSMIS",
                             rs.getString("CD_TRANSMIS"));

                brote.insert( (String) "NM_NVACENF", rs.getString("NM_NVACENF"));

                brote.insert( (String) "CD_OPE", rs.getString(22));
                brote.insert( (String) "FC_ULTACT", sfecRecu);

                brote.insert( (String) "NM_DCUACMIN",
                             rs.getString("NM_DCUACMIN"));
                brote.insert( (String) "NM_DCUACMED",
                             rs.getString("NM_DCUACMED"));
                brote.insert( (String) "NM_DCUACMAX",
                             rs.getString("NM_DCUACMAX"));

                brote.insert( (String) "NM_NVACNENF",
                             rs.getString("NM_NVACNENF"));
                brote.insert( (String) "NM_VACNENF", rs.getString("NM_VACNENF"));
                brote.insert( (String) "NM_VACENF", rs.getString("NM_VACENF"));

                brote.insert( (String) "DS_OBSERV", rs.getString("DS_OBSERV"));

                brote.insert( (String) "CD_TIPOCOL", rs.getString("CD_TIPOCOL"));
                brote.insert( (String) "DS_NOMCOL", rs.getString("DS_NOMCOL"));
                brote.insert( (String) "DS_DIRCOL", rs.getString("DS_DIRCOL"));
                brote.insert( (String) "CD_POSTALCOL",
                             rs.getString("CD_POSTALCOL"));
                brote.insert( (String) "CD_PROVCOL", rs.getString("CD_PROVCOL"));

                String auxDsBrote = rs.getString("DS_BROTE");
                brote.insert( (String) "DS_BROTE", auxDsBrote);
                brote.insert( (String) "CD_GRUPO", rs.getString(37));

                brote.insert( (String) "NM_MANIPUL", rs.getString("NM_MANIPUL"));

                brote.insert( (String) "CD_ZBS_LE", rs.getString("CD_ZBS_LE"));
                brote.insert( (String) "CD_NIVEL_2_LE", cd_nivel_2_ex);

                brote.insert( (String) "CD_TNOTIF", rs.getString("CD_TNOTIF"));

                brote.insert( (String) "IT_RESCALC", rs.getString("IT_RESCALC"));

                brote.insert( (String) "CD_MUNCOL", rs.getString("CD_MUNCOL"));
                brote.insert( (String) "CD_NIVEL_1_LCA", cd_nivel_1_lca);
                brote.insert( (String) "DS_TELCOL", rs.getString("DS_TELCOL"));
                brote.insert( (String) "CD_NIVEL_1_LE", cd_nivel_1_ex);
                brote.insert( (String) "CD_NIVEL_2_LCA", cd_nivel_2_lca);
                brote.insert( (String) "CD_ZBS_LCA", rs.getString("CD_ZBS_LCA"));

                brote.insert( (String) "DS_NMCALLE", rs.getString("DS_NMCALLE"));
                brote.insert( (String) "CD_TBROTE", rs.getString("CD_TBROTE"));
                brote.insert( (String) "IT_PERIN", rs.getString("IT_PERIN"));
                brote.insert( (String) "IT_DCUAC", rs.getString("IT_DCUAC"));

                //??

                listaBrotes.addElement(brote);
              }

            } //while rs.next()

            rs.close();
            rs = null;
            st.close();
            st = null;

            //Para cada uno de los brotes de la lista hay que
            //completar las descripciones

            String desMunicipio = null;

            String codCadef = null;

            String desNivel1LCA = null;
            String desNivel2LCA = null;
            String desZBSLCA = null;

            String desNivel1LE = null;
            String desNivel2LE = null;
            String desZBSLE = null;

            String desNivel1_normal = null;
            String desNivel2_normal = null;

            for (int j = 0; j < listaBrotes.size(); j++) {
              brote = (DatAlBro) listaBrotes.elementAt(j);

              // Municipio
              if (brote.getCD_PROVCOL() != null &&
                  brote.getCD_MUNCOL() != null) {

                sQuery = " select DS_MUN "
                    + " FROM SIVE_MUNICIPIO "
                    + " WHERE CD_PROV = ? AND CD_MUN = ? ";

                st = con.prepareStatement(sQuery);
                st.setString(1, brote.getCD_PROVCOL().trim());
                st.setString(2, brote.getCD_MUNCOL().trim());
                rs = st.executeQuery();
                while (rs.next()) {
                  desMunicipio = rs.getString("DS_MUN");
                }
                rs.close();
                rs = null;
                st.close();
                st = null;
              }

              // c�digo de la comunidad aut�noma
              if (brote.getCD_PROVCOL() != null) {
                sQuery = "select CD_CA from SIVE_PROVINCIA where CD_PROV = ?";

                st = con.prepareStatement(sQuery);
                st.setString(1, brote.getCD_PROVCOL().trim());
                rs = st.executeQuery();
                while (rs.next()) {
                  codCadef = rs.getString("CD_CA");
                }
                rs.close();
                rs = null;
                st.close();
                st = null;

              }

              //nivel 1 LCA
              if (brote.getCD_NIVEL_1_LCA() != null) {
                sQuery = "select "
                    + " DS_NIVEL_1, DSL_NIVEL_1 "
                    + " FROM  SIVE_NIVEL1_S  WHERE CD_NIVEL_1 = ? ";
                st = con.prepareStatement(sQuery);
                st.setString(1, brote.getCD_NIVEL_1_LCA().trim());
                rs = st.executeQuery();
                String desN1 = "";
                String desLN1 = "";

                while (rs.next()) {
                  desN1 = rs.getString("DS_NIVEL_1");
                  desLN1 = rs.getString("DSL_NIVEL_1");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (desLN1 != null)) {
                    desNivel1LCA = desLN1;
                  }
                  else {
                    desNivel1LCA = desN1;
                  }
                }
                rs.close();
                rs = null;
                st.close();
                st = null;
              }

              //nivel2 LCA
              if (brote.getCD_NIVEL_1_LCA() != null &&
                  brote.getCD_NIVEL_2_LCA() != null) {

                sQuery = "select "
                    + " DS_NIVEL_2, DSL_NIVEL_2 "
                    +
                    " FROM  SIVE_NIVEL2_S  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?";
                st = con.prepareStatement(sQuery);
                st.setString(1, brote.getCD_NIVEL_1_LCA().trim());
                st.setString(2, brote.getCD_NIVEL_2_LCA().trim());
                rs = st.executeQuery();
                String desN2 = "";
                String desLN2 = "";

                while (rs.next()) {
                  desN2 = rs.getString("DS_NIVEL_2");
                  desLN2 = rs.getString("DSL_NIVEL_2");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (desLN2 != null)) {
                    desNivel2LCA = desLN2;
                  }
                  else {
                    desNivel2LCA = desN2;
                  }
                }
                rs.close();
                rs = null;
                st.close();
                st = null;
              }

              //Zona Basica LCA
              if (brote.getCD_NIVEL_1_LCA() != null &&
                  brote.getCD_NIVEL_2_LCA() != null &&
                  brote.getCD_ZBS_LCA() != null) {

                sQuery = "select "
                    + " DS_ZBS, DSL_ZBS "
                    + " FROM  SIVE_ZONA_BASICA"
                    +
                    "  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND CD_ZBS = ? ";
                st = con.prepareStatement(sQuery);
                st.setString(1, brote.getCD_NIVEL_1_LCA().trim());
                st.setString(2, brote.getCD_NIVEL_2_LCA().trim());
                st.setString(3, brote.getCD_ZBS_LCA().trim());
                rs = st.executeQuery();
                String desZona = "";
                String desLZona = "";

                while (rs.next()) {
                  desZona = rs.getString("DS_ZBS");
                  desLZona = rs.getString("DSL_ZBS");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (rs.getString("DSL_ZBS") != null)) {
                    desZBSLCA = desLZona;
                  }
                  else {
                    desZBSLCA = desZona;
                  }
                }
                rs.close();
                rs = null;
                st.close();
                st = null;
              }

              //nivel 1 LE
              if (brote.getCD_NIVEL_1_LE() != null) {
                sQuery = "select "
                    + " DS_NIVEL_1, DSL_NIVEL_1 "
                    + " FROM  SIVE_NIVEL1_S  WHERE CD_NIVEL_1 = ? ";
                st = con.prepareStatement(sQuery);
                st.setString(1, brote.getCD_NIVEL_1_LE().trim());
                rs = st.executeQuery();
                String desN1 = "";
                String desLN1 = "";

                while (rs.next()) {
                  desN1 = rs.getString("DS_NIVEL_1");
                  desLN1 = rs.getString("DSL_NIVEL_1");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (desLN1 != null)) {
                    desNivel1LE = desLN1;
                  }
                  else {
                    desNivel1LE = desN1;
                  }
                }
                rs.close();
                rs = null;
                st.close();
                st = null;
              }

              //nivel2 LE
              if (brote.getCD_NIVEL_1_LE() != null &&
                  brote.getCD_NIVEL_2_LE() != null) {

                sQuery = "select "
                    + " DS_NIVEL_2, DSL_NIVEL_2 "
                    +
                    " FROM  SIVE_NIVEL2_S  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?";
                st = con.prepareStatement(sQuery);
                st.setString(1, brote.getCD_NIVEL_1_LE().trim());
                st.setString(2, brote.getCD_NIVEL_2_LE().trim());
                rs = st.executeQuery();
                String desN2 = "";
                String desLN2 = "";

                while (rs.next()) {
                  desN2 = rs.getString("DS_NIVEL_2");
                  desLN2 = rs.getString("DSL_NIVEL_2");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (desLN2 != null)) {
                    desNivel2LE = desLN2;
                  }
                  else {
                    desNivel2LE = desN2;
                  }
                }
                rs.close();
                rs = null;
                st.close();
                st = null;
              }

              //Zona Basica LE
              if (brote.getCD_NIVEL_1_LE() != null &&
                  brote.getCD_NIVEL_2_LE() != null &&
                  brote.getCD_ZBS_LE() != null) {

                sQuery = "select "
                    + " DS_ZBS, DSL_ZBS "
                    + " FROM  SIVE_ZONA_BASICA"
                    +
                    "  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND CD_ZBS = ? ";
                st = con.prepareStatement(sQuery);
                st.setString(1, brote.getCD_NIVEL_1_LE().trim());
                st.setString(2, brote.getCD_NIVEL_2_LE().trim());
                st.setString(3, brote.getCD_ZBS_LE().trim());
                rs = st.executeQuery();
                String desZona = "";
                String desLZona = "";

                while (rs.next()) {
                  desZona = rs.getString("DS_ZBS");
                  desLZona = rs.getString("DSL_ZBS");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (rs.getString("DSL_ZBS") != null)) {
                    desZBSLE = desLZona;
                  }
                  else {
                    desZBSLE = desZona;
                  }
                }
                rs.close();
                rs = null;
                st.close();
                st = null;
              }

              //Se insertan las descripciones
              brote.insert( (String) "DS_MUNCOL", desMunicipio);
              brote.insert( (String) "CD_CACOL", codCadef);
              brote.insert( (String) "DS_NIVEL_1_LCA", desNivel1LCA);
              brote.insert( (String) "DS_NIVEL_2_LCA", desNivel2LCA);
              brote.insert( (String) "DS_ZBS_LCA", desZBSLCA);
              brote.insert( (String) "DS_NIVEL_1_LE", desNivel1LE);
              brote.insert( (String) "DS_NIVEL_2_LE", desNivel2LE);
              brote.insert( (String) "DS_ZBS_LE", desZBSLE);

              listaBrotes.setElementAt(brote, j);

            } //fin del for para completar las descripciones

            /*or(int j= 0; j<listaBrotes.size(); j++){
              impHash((Hashtable)listaBrotes.elementAt(j));
             }  */

          } //mlm separando la parte de brotes

          //3. Ahora hay que combinar las dos listas de forma que
          // quede una ordenada de forma decreciente de nm_alerbro y fechaHora

          int iAlerta = 0;
          int iBrote = 0;
          boolean bSeguir = true;
          boolean bBrote = false;
          boolean bAlerta = false;
          String sResComp = "";
          //en las listas ya estan los elementos de la siguiente trama,
          //es decir, no hace falta saltar ninguno
          Hashtable ultAler = new Hashtable();
          Hashtable ultBro = new Hashtable();

          CLista listaAux = new CLista();
          if (listaAlertas.size() == 0) {
            if (listaBrotes.size() == 0) {
              listaSalida = new CLista();
            }
            else {
              if (listaBrotes.size() > DBServlet.maxSIZE) {
                listaSalida.setState(CLista.listaINCOMPLETA);

                for (int j = 0; j < DBServlet.maxSIZE; j++) {
                  listaAux.addElement( (DatAlBro) listaBrotes.elementAt(j));
                }
                listaSalida.addElement(listaAux);
              }
              else {
                listaSalida.addElement(listaBrotes);
              }
              if (listaSalida.getState() == CLista.listaINCOMPLETA) {
                //para tramar
                DataAlerta auxD = new DataAlerta();
                auxD.put("CD_SITALERBRO", "0");
                AnyadeHashLista(listaSalida, auxD);
                AnyadeHashLista(listaSalida,
                                (DatAlBro) listaBrotes.elementAt(DBServlet.
                    maxSIZE - 1));
              }
            }
          }
          else {
            if (listaBrotes.size() == 0) {
              if (listaAlertas.size() > DBServlet.maxSIZE) {
                listaSalida.setState(CLista.listaINCOMPLETA);

                for (int j = 0; j < DBServlet.maxSIZE; j++) {
                  listaAux.addElement( (DataAlerta) listaAlertas.elementAt(j));
                }
                listaSalida.addElement(listaAux);
              }
              else {
                listaSalida.addElement(listaAlertas);
              }
              if (listaSalida.getState() == CLista.listaINCOMPLETA) {
                //para tramar
                AnyadeHashLista(listaSalida,
                                (DataAlerta) listaAlertas.
                                elementAt(DBServlet.maxSIZE - 1));
                DatAlBro auxD = new DatAlBro();
                auxD.put("CD_SITALERBRO", "2");
                AnyadeHashLista(listaSalida, auxD);
              }
            }
            else {

              Hashtable aler = (Hashtable) listaAlertas.elementAt(iAlerta);
              Hashtable bro = (Hashtable) listaBrotes.elementAt(iBrote);
              iAlerta++;
              iBrote++;

              CLista listaMezclada = new CLista();
              while (bSeguir) {
                sResComp = compararNMAlerbroFechaHora(aler, bro);
                if (sResComp.equals("b")) {
                  //se a�ade el brote y se toma uno nuevo
                  AnyadeHashLista(listaMezclada, bro);
                  //listaMezclada.addElement(brote);
                  ultBro = bro;
                  bBrote = true;
                  bAlerta = false;
                  if (iBrote < listaBrotes.size()) {
                    bro = (Hashtable) listaBrotes.elementAt(iBrote);
                    iBrote++;
                  }
                  else if (iAlerta < listaAlertas.size()) {
                    bro = (Hashtable) listaAlertas.elementAt(iAlerta);
                    iAlerta++;
                  }
                  else {
                    bSeguir = false;
                  }
                }
                else if (sResComp.equals("a")) {
                  //se a�ade la alerta y se toma una nueva
                  AnyadeHashLista(listaMezclada, aler);
                  //listaMezclada.addElement(alerta);
                  ultAler = aler;
                  bBrote = false;
                  bAlerta = true;
                  if (iAlerta < listaAlertas.size()) {
                    aler = (Hashtable) listaAlertas.elementAt(iAlerta);
                    iAlerta++;
                  }
                  else if (iBrote < listaBrotes.size()) {
                    aler = (Hashtable) listaBrotes.elementAt(iBrote);
                    iBrote++;
                  }
                  else {
                    bSeguir = false;
                  }
                }

                if (listaMezclada.size() == DBServlet.maxSIZE) {
                  listaSalida.setState(CLista.listaINCOMPLETA);
                  //listaSalida.setFilter(((DataBusqueda)listaSalida.lastElement()).getNM_ALERBRO());
                  bSeguir = false;
                }
              } //while bSalir

              if (bBrote) {
                AnyadeHashLista(listaMezclada, alerta);
              }
              else if (bAlerta) {
                AnyadeHashLista(listaMezclada, brote);

              }
              listaSalida.addElement(listaMezclada);

              if (listaSalida.getState() == CLista.listaINCOMPLETA) {
                //para tramar
                AnyadeHashLista(listaSalida, ultAler);
                AnyadeHashLista(listaSalida, ultBro);
              }
            }
          }

          break;

      } //fin switch

      //valida la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    listaSalida.trimToSize();
    return listaSalida;

  } //fin doWork

  private void AnyadeHashLista(CLista l, Hashtable brote) {
    String sit = (String) brote.get("CD_SITALERBRO");

    if (sit.equals("2")) { //brote
      DatAlBro bro = new DatAlBro();

      if ( (String) brote.get("CD_ANO") == null) {
        l.addElement(bro);
      }
      else {
        bro.insert( (String) "CD_ANO", ( (String) brote.get("CD_ANO")).trim());
        bro.insert( (String) "NM_ALERBRO",
                   ( (String) brote.get("NM_ALERBRO")).trim());
        bro.insert( (String) "FC_FECHAHORA_F",
                   ( (String) brote.get("FC_FECHAHORA_F")).trim());
        bro.insert( (String) "FC_FECHAHORA_H",
                   ( (String) brote.get("FC_FECHAHORA_H")).trim());
        bro.insert( (String) "CD_SITALERBRO",
                   ( (String) brote.get("CD_SITALERBRO")).trim());
        bro.insert( (String) "FC_ALERBRO",
                   ( (String) brote.get("FC_ALERBRO")).trim());

        if (brote.get("FC_FSINPRIMC_F") != null) {
          bro.insert( (String) "FC_FSINPRIMC_F",
                     ( (String) brote.get("FC_FSINPRIMC_F")).trim());
        }
        if (brote.get("FC_FSINPRIMC_H") != null) {
          bro.insert( (String) "FC_FSINPRIMC_H",
                     ( (String) brote.get("FC_FSINPRIMC_H")).trim());
        }
        if (brote.get("FC_ISINPRIMC_F") != null) {
          bro.insert( (String) "FC_ISINPRIMC_F",
                     ( (String) brote.get("FC_ISINPRIMC_F")).trim());
        }
        if (brote.get("FC_ISINPRIMC_H") != null) {
          bro.insert( (String) "FC_ISINPRIMC_H",
                     ( (String) brote.get("FC_ISINPRIMC_H")).trim());

        }
        if (brote.get("NM_PERINMIN") != null) {
          bro.insert( (String) "NM_PERINMIN",
                     ( (String) brote.get("NM_PERINMIN")).trim());
        }
        if (brote.get("NM_PERINMAX") != null) {
          bro.insert( (String) "NM_PERINMAX",
                     ( (String) brote.get("NM_PERINMAX")).trim());
        }
        if (brote.get("NM_PERINMED") != null) {
          bro.insert( (String) "NM_PERINMED",
                     ( (String) brote.get("NM_PERINMED")).trim());

        }
        if (brote.get("NM_ENFERMOS") != null) {
          bro.insert( (String) "NM_ENFERMOS",
                     ( (String) brote.get("NM_ENFERMOS")).trim());
        }
        if (brote.get("FC_EXPOSICION_F") != null) {
          bro.insert( (String) "FC_EXPOSICION_F",
                     ( (String) brote.get("FC_EXPOSICION_F")).trim());
        }
        if (brote.get("FC_EXPOSICION_H") != null) {
          bro.insert( (String) "FC_EXPOSICION_H",
                     ( (String) brote.get("FC_EXPOSICION_H")).trim());
        }
        if (brote.get("NM_INGHOSP") != null) {
          bro.insert( (String) "NM_INGHOSP",
                     ( (String) brote.get("NM_INGHOSP")).trim());
        }
        if (brote.get("NM_DEFUNCION") != null) {
          bro.insert( (String) "NM_DEFUNCION",
                     ( (String) brote.get("NM_DEFUNCION")).trim());
        }
        if (brote.get("NM_EXPUESTOS") != null) {
          bro.insert( (String) "NM_EXPUESTOS",
                     ( (String) brote.get("NM_EXPUESTOS")).trim());

        }
        if (brote.get("CD_TRANSMIS") != null) {
          bro.insert( (String) "CD_TRANSMIS",
                     ( (String) brote.get("CD_TRANSMIS")).trim());

        }
        if (brote.get("CD_TRANSMIS") != null) {
          bro.insert( (String) "NM_NVACENF",
                     ( (String) brote.get("NM_NVACENF")).trim());

        }
        if (brote.get("CD_OPE") != null) {
          bro.insert( (String) "CD_OPE", ( (String) brote.get("CD_OPE")).trim());
        }
        if (brote.get("FC_ULTACT") != null) {
          bro.insert( (String) "FC_ULTACT",
                     ( (String) brote.get("FC_ULTACT")).trim());

        }
        if (brote.get("NM_DCUACMIN") != null) {
          bro.insert( (String) "NM_DCUACMIN",
                     ( (String) brote.get("NM_DCUACMIN")).trim());
        }
        if (brote.get("NM_DCUACMED") != null) {
          bro.insert( (String) "NM_DCUACMED",
                     ( (String) brote.get("NM_DCUACMED")).trim());
        }
        if (brote.get("NM_DCUACMAX") != null) {
          bro.insert( (String) "NM_DCUACMAX",
                     ( (String) brote.get("NM_DCUACMAX")).trim());

        }
        if (brote.get("NM_NVACNENF") != null) {
          bro.insert( (String) "NM_NVACNENF",
                     ( (String) brote.get("NM_NVACNENF")).trim());
        }
        if (brote.get("NM_VACNENF") != null) {
          bro.insert( (String) "NM_VACNENF",
                     ( (String) brote.get("NM_VACNENF")).trim());
        }
        if (brote.get("NM_VACENF") != null) {
          bro.insert( (String) "NM_VACENF",
                     ( (String) brote.get("NM_VACENF")).trim());

        }
        if (brote.get("DS_OBSERV") != null) {
          bro.insert( (String) "DS_OBSERV",
                     ( (String) brote.get("DS_OBSERV")).trim());

        }
        if (brote.get("CD_TIPOCOL") != null) {
          bro.insert( (String) "CD_TIPOCOL",
                     ( (String) brote.get("CD_TIPOCOL")).trim());
        }
        if (brote.get("DS_NOMCOL") != null) {
          bro.insert( (String) "DS_NOMCOL",
                     ( (String) brote.get("DS_NOMCOL")).trim());
        }
        if (brote.get("DS_DIRCOL") != null) {
          bro.insert( (String) "DS_DIRCOL",
                     ( (String) brote.get("DS_DIRCOL")).trim());
        }
        if (brote.get("CD_POSTALCOL") != null) {
          bro.insert( (String) "CD_POSTALCOL",
                     ( (String) brote.get("CD_POSTALCOL")).trim());
        }
        if (brote.get("CD_PROVCOL") != null) {
          bro.insert( (String) "CD_PROVCOL",
                     ( (String) brote.get("CD_PROVCOL")).trim());

        }
        if (brote.get("DS_BROTE") != null) {
          bro.insert( (String) "DS_BROTE",
                     ( (String) brote.get("DS_BROTE")).trim());
        }
        if (brote.get("CD_GRUPO") != null) {
          bro.insert( (String) "CD_GRUPO",
                     ( (String) brote.get("CD_GRUPO")).trim());

        }

        if (brote.get("NM_MANIPUL") != null) {
          bro.insert( (String) "NM_MANIPUL",
                     ( (String) brote.get("NM_MANIPUL")).trim());

        }
        if (brote.get("CD_ZBS_LE") != null) {
          bro.insert( (String) "CD_ZBS_LE",
                     ( (String) brote.get("CD_ZBS_LE")).trim());
        }
        if (brote.get("CD_NIVEL_2_LE") != null) {
          bro.insert( (String) "CD_NIVEL_2_LE",
                     ( (String) brote.get("CD_NIVEL_2_LE")).trim());

        }
        if (brote.get("CD_TNOTIF") != null) {
          bro.insert( (String) "CD_TNOTIF",
                     ( (String) brote.get("CD_TNOTIF")).trim());

        }
        if (brote.get("IT_RESCALC") != null) {
          bro.insert( (String) "IT_RESCALC",
                     ( (String) brote.get("IT_RESCALC")).trim());

        }
        if (brote.get("CD_MUNCOL") != null) {
          bro.insert( (String) "CD_MUNCOL",
                     ( (String) brote.get("CD_MUNCOL")).trim());
        }
        if (brote.get("CD_NIVEL_1_LCA") != null) {
          bro.insert( (String) "CD_NIVEL_1_LCA",
                     ( (String) brote.get("CD_NIVEL_1_LCA")).trim());
        }
        if (brote.get("DS_TELCOL") != null) {
          bro.insert( (String) "DS_TELCOL",
                     ( (String) brote.get("DS_TELCOL")).trim());
        }
        if (brote.get("CD_NIVEL_1_LE") != null) {
          bro.insert( (String) "CD_NIVEL_1_LE",
                     ( (String) brote.get("CD_NIVEL_1_LE")).trim());
        }
        if (brote.get("CD_NIVEL_2_LCA") != null) {
          bro.insert( (String) "CD_NIVEL_2_LCA",
                     ( (String) brote.get("CD_NIVEL_2_LCA")).trim());
        }
        if (brote.get("CD_ZBS_LCA") != null) {
          bro.insert( (String) "CD_ZBS_LCA",
                     ( (String) brote.get("CD_ZBS_LCA")).trim());

        }
        if (brote.get("DS_NMCALLE") != null) {
          bro.insert( (String) "DS_NMCALLE",
                     ( (String) brote.get("DS_NMCALLE")).trim());
        }
        if (brote.get("CD_TBROTE") != null) {
          bro.insert( (String) "CD_TBROTE",
                     ( (String) brote.get("CD_TBROTE")).trim());
        }
        if (brote.get("IT_PERIN") != null) {
          bro.insert( (String) "IT_PERIN",
                     ( (String) brote.get("IT_PERIN")).trim());
        }
        if (brote.get("IT_DCUAC") != null) {
          bro.insert( (String) "IT_DCUAC",
                     ( (String) brote.get("IT_DCUAC")).trim());

        }
        if (brote.get("DS_MUNCOL") != null) {
          bro.insert( (String) "DS_MUNCOL",
                     ( (String) brote.get("DS_MUNCOL")).trim());
        }
        if (brote.get("CD_CACOL") != null) {
          bro.insert( (String) "CD_CACOL",
                     ( (String) brote.get("CD_CACOL")).trim());
        }
        if (brote.get("DS_NIVEL_1_LCA") != null) {
          bro.insert( (String) "DS_NIVEL_1_LCA",
                     ( (String) brote.get("DS_NIVEL_1_LCA")).trim());
        }
        if (brote.get("DS_NIVEL_2_LCA") != null) {
          bro.insert( (String) "DS_NIVEL_2_LCA",
                     ( (String) brote.get("DS_NIVEL_2_LCA")).trim());
        }
        if (brote.get("DS_ZBS_LCA") != null) {
          bro.insert( (String) "DS_ZBS_LCA",
                     ( (String) brote.get("DS_ZBS_LCA")).trim());
        }
        if (brote.get("DS_NIVEL_1_LE") != null) {
          bro.insert( (String) "DS_NIVEL_1_LE",
                     ( (String) brote.get("DS_NIVEL_1_LE")).trim());
        }
        if (brote.get("DS_NIVEL_2_LE") != null) {
          bro.insert( (String) "DS_NIVEL_2_LE",
                     ( (String) brote.get("DS_NIVEL_2_LE")).trim());
        }
        if (brote.get("DS_ZBS_LE") != null) {
          bro.insert( (String) "DS_ZBS_LE",
                     ( (String) brote.get("DS_ZBS_LE")).trim());

        }
        l.addElement(bro);
      }
    }
    else if (sit.equals("0")) { //alerta
      DataAlerta bro = new DataAlerta();
      if ( (String) brote.get("CD_ANO") == null) {
        l.addElement(bro);
      }
      else {
        bro.insert( (String) "FC_FECHAHORA_F",
                   (String) brote.get("FC_FECHAHORA_F"));
        bro.insert( (String) "FC_FECHAHORA_H",
                   (String) brote.get("FC_FECHAHORA_H"));
        bro.insert( (String) "CD_ANO",
                   (String) brote.get("CD_ANO"));
        bro.insert( (String) "NM_ALERBRO",
                   (String) brote.get("NM_ALERBRO"));
        bro.insert( (String) "CD_SITALERBRO",
                   (String) brote.get("CD_SITALERBRO"));
        bro.insert( (String) "FC_ALERBRO",
                   (String) brote.get("FC_ALERBRO"));
        bro.insert( (String) "DS_ALERTA",
                   (String) brote.get("DS_ALERTA"));
        l.addElement(bro);
      }
    }

  }

  private void impHash(Hashtable alerta) {
    System.out.println("HASH : " + (String) alerta.get("FC_FECHAHORA_F") + " "
                       + (String) alerta.get("FC_FECHAHORA_H") + " "
                       + (String) alerta.get("CD_ANO") + " "
                       + (String) alerta.get("NM_ALERBRO") + " "
                       + (String) alerta.get("CD_SITALERBRO") + " ");
  }

  private String compararNMAlerbroFechaHora(Hashtable alerta,
                                            Hashtable brote) {
    //ordenar por fechaHora, a�o, alerbro
    String sFHFA = (String) alerta.get("FC_FECHAHORA_F");
    String sFHHA = (String) alerta.get("FC_FECHAHORA_H");
    String sA = sFHFA.substring(6) + sFHFA.substring(3, 5) +
        sFHFA.substring(0, 2)
        + sFHHA.substring(0, 2) + sFHHA.substring(3);
    sA = sA + (String) alerta.get("CD_ANO") + (String) alerta.get("NM_ALERBRO");

    String sFHFB = (String) brote.get("FC_FECHAHORA_F");
    String sFHHB = (String) brote.get("FC_FECHAHORA_H");
    String sB = sFHFB.substring(6) + sFHFB.substring(3, 5) +
        sFHFB.substring(0, 2)
        + sFHHB.substring(0, 2) + sFHHB.substring(3);
    sB = sB + (String) brote.get("CD_ANO") + (String) brote.get("NM_ALERBRO");

    // 0 si son iguales
    //<0 si sA<sB
    //>0 si sA>sB
    if (sA.compareTo(sB) < 0) {
      return "b";
    }
    else if (sA.compareTo(sB) > 0) {
      return "a";
    }
    return "";
  }

  //return: 0:modoMODIFICAR
  //        1:modoCONSULTA
  //        2:modoNADA (no esta autorizado)
  //        -1:ERROR
  public int autorizaciones(CLista listaentrada,
                            String n1_alerta, String n2_alerta,
                            String n1_lca, String n2_lca,
                            String n1_le, String n2_le) {
    try {

      DataBusqueda datosentrada = (DataBusqueda) listaentrada.firstElement();

      Vector vN1 = (Vector) datosentrada.getVN1();
      Vector vN2 = (Vector) datosentrada.getVN2();

      Integer IPerfil = new Integer(listaentrada.getPerfil());
      String sPerfil = IPerfil.toString();

      String N1 = n1_alerta;
      String N2 = n2_alerta;
      if (N2 == null) {
        N2 = "";

      }
      String N1_LE = n1_le;
      String N2_LE = n2_le;
      if (N1_LE == null) {
        N1_LE = "";
      }
      if (N2_LE == null) {
        N2_LE = "";

      }
      String N1_LCA = n1_lca;
      String N2_LCA = n2_lca;
      if (N1_LCA == null) {
        N1_LCA = "";
      }
      if (N2_LCA == null) {
        N2_LCA = "";

        //Epidemiologo N1: autorizado a vector
      }
      if (sPerfil.equals("3")) {

        //normal: si pertenece al conjunto de autoriz,
        //lo modif independiente de le y lca
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1)) {
            return (0);
          }
        } //for
        //-------- normal -----

        //Si llega hasta aqui, puede que pueda modificarla por
        //l expuestos: si pertenece al conjunto de autoriz:
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1_LE)) {
            return (0);
          }
        } //for
        //-------- expuestos -----

        //Si llega hasta aqui, puede que pueda modificarla por
        //localizacion: si pertenece al conjunto de autoriz:
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1_LCA)) {
            return (0);
          }
        } //for
        //-------- localizacion ------

        //si llega hasta aqui, es que no se ha encontrado ni uno ni otro:
        return (2); //NO AUTORIZADO.
      } //fin Epidemiologo N1

      //Epidemiologo N2: autorizado a vectores
      if (sPerfil.equals("4")) {
        //normal -----------------------------------------------------
        //area, distrito  parejas (deben ser de igual tama�o)
        if (!N1.equals("") && !N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1)) {
              if (n2.trim().equals(N2)) {
                return (0);
              }
            }
          }
        }

        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1.equals("") && N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1)) {
              return (0);
            }
          }
        }
        //-----------------------------------------------------

        //Si llega hasta aqui, puede que pueda modificar
        //l expuestos: se trata igual, si Si: 1
        //area, distrito  parejas (deben ser de igual tama�o)
        if (!N1_LE.equals("") && !N2_LE.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1_LE)) {
              if (n2.trim().equals(N2_LE)) {
                return (0);
              }
            }
          }
        }
        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1_LE.equals("") && N2_LE.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1_LE)) {
              return (0);
            }
          }
        }
        //expuestos----------------------------------------

        //Si llega hasta aqui, puede que pueda modificar
        //l ocalizacion: se trata igual, si Si: 1
        //area, distrito  parejas (deben ser de igual tama�o)
        if (!N1_LCA.equals("") && !N2_LCA.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1_LCA)) {
              if (n2.trim().equals(N2_LCA)) {
                return (0);
              }
            }
          }
        }
        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1_LCA.equals("") && N2_LCA.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1_LCA)) {
              return (0);
            }
          }
        }
        //LOCALIZACION----------------------------------------
        //si llega hasta aqui, es que no se ha encontrado ni uno ni otro:
        return (2); //NO AUTORIZADO.
      } //fin Epidemiologo N2

      //perfil 2: CA: //permiso de modificar a todo
      if (sPerfil.equals("2")) {
        return (0);
      }

      //perfil 5: solo vera n1 n2 permitidos ptt podra modificarlos
      //En Informes de Brotes, ni siquiera entrara.
      if (sPerfil.equals("5")) {
        return (0);
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      return ( -1);
    }

    return (2);
  } //autorizaciones

} //fin de la clase
/*
 Cris, en los parametros de entrada se mete lo que salga de rs.getString, por tanto pueden ser nulos...excepto n1_alerta
//return: 0:modoMODIFICAR
  //        1:modoCONSULTA
  //        2:modoNADA (no esta autorizado)
  //        -1:ERROR
  public int autorizaciones(CLista listaentrada,
                              String n1_alerta, String n2_alerta,
                              String n1_lca, String  n2_lca,
                              String n1_le,  String n2_le){
  try {
    DataBusqueda datosentrada = (DataBusqueda) listaentrada.firstElement();
    Vector vN1 = (Vector)datosentrada.getVN1();
    Vector vN2 = (Vector)datosentrada.getVN2();
    Integer IPerfil = new Integer (listaentrada.getPerfil());
    String sPerfil = IPerfil.toString();
    String N1 = n1_alerta;
    String N2 = n2_alerta;
    if (N2 == null)  N2 = "";
    String N1_LE = n1_le;
    String N2_LE = n2_le;
    if (N1_LE == null)  N1_LE = "";
    if (N2_LE == null)  N2_LE = "";
    String N1_LCA = n1_lca;
    String N2_LCA = n2_lca;
    if (N1_LCA == null)  N1_LCA = "";
    if (N2_LCA == null)  N2_LCA = "";
    //Epidemiologo N1: autorizado a vector
    if (sPerfil.equals("3")) {
      //normal: si pertenece al conjunto de autoriz,
      //lo modif independiente de le y lca
      for (int i=0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1)){
             return (0);
          }
      }//for
      //-------- normal -----
      //Si llega hasta aqui, puede que pueda modificarla por
      //l expuestos: si pertenece al conjunto de autoriz:
      for (int i=0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1_LE)){
             return(0);
          }
      }//for
      //-------- expuestos -----
       //Si llega hasta aqui, puede que pueda modificarla por
      //localizacion: si pertenece al conjunto de autoriz:
      for (int i=0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1_LCA)){
             return(0);
          }
      }//for
      //-------- localizacion ------
      //si llega hasta aqui, es que no se ha encontrado ni uno ni otro:
      return (2); //NO AUTORIZADO.
    }//fin Epidemiologo N1
    //Epidemiologo N2: autorizado a vectores
    if (sPerfil.equals("4")) {
       //normal -----------------------------------------------------
      //area, distrito  parejas (deben ser de igual tama�o)
       if (!N1.equals("") && !N2.equals("")){
          for (int i=0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1)){
               if (n2.trim().equals(N2))
                return(0);
            }
          }
       }
       //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
       if (!N1.equals("") && N2.equals("")){
          for (int i=0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1))
              return(0);
            }
       }
       //-----------------------------------------------------
       //Si llega hasta aqui, puede que pueda modificar
       //l expuestos: se trata igual, si Si: 1
       //area, distrito  parejas (deben ser de igual tama�o)
       if (!N1_LE.equals("") && !N2_LE.equals("")){
          for (int i=0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1_LE)){
               if (n2.trim().equals(N2_LE))
                return(0);
            }
          }
       }
       //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
       if (!N1_LE.equals("") && N2_LE.equals("")){
          for (int i=0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1_LE))
              return(0);
            }
       }
       //expuestos----------------------------------------
       //Si llega hasta aqui, puede que pueda modificar
       //l ocalizacion: se trata igual, si Si: 1
       //area, distrito  parejas (deben ser de igual tama�o)
       if (!N1_LCA.equals("") && !N2_LCA.equals("")){
          for (int i=0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1_LCA)){
               if (n2.trim().equals(N2_LCA))
                return(0);
            }
          }
       }
       //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
       if (!N1_LCA.equals("") && N2_LCA.equals("")){
          for (int i=0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1_LCA))
              return(0);
            }
       }
       //LOCALIZACION----------------------------------------
      //si llega hasta aqui, es que no se ha encontrado ni uno ni otro:
      return (2); //NO AUTORIZADO.
    }//fin Epidemiologo N2
    //perfil 2: CA: //permiso de modificar a todo
    if (sPerfil.equals("2")){
       return(0);
    }
    //perfil 5: solo vera n1 n2 permitidos ptt podra modificarlos
    //En Informes de Brotes, ni siquiera entrara.
    if (sPerfil.equals("5")){
       return(0);
    }
   }
   catch (Exception e) {
      e.printStackTrace();
      return(-1);
   }
   return(2);
 } //autorizaciones
 */
