package brotes.servidor.bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import brotes.datos.bidata.DataCDDS;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;
import suca.datasuca;

//import jdbcpool.*;//quitar

public class SrvCombosIcm
    extends DBServlet {

  final int servletCOMBOS = 0;

  /*public CLista doDebug(int opmode, CLista param) throws Exception {
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
      Connection con = openConnection();
      return doWork(opmode, param);
     } */

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 0;

    // objetos de datos
    CLista listaSalida = new CLista(); //lista con las 2 listas
    CLista listaGrupos = new CLista();
    CLista listaSituacion = new CLista();
    CLista listaTNotificador = new CLista();
    CLista listaPaises = new CLista();
    CLista listaCA = new CLista();
    CLista listaCENTRO = new CLista();
    DataCDDS data = null;

    //querys
    String sQuery = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    String ds = "";
    String dsl = "";
    String sDes = "";

    try {

      switch (opmode) {
        case servletCOMBOS:

          sQuery = "SELECT CD_GRUPO, DS_GRUPO, DSL_GRUPO " +
              " FROM SIVE_GRUPO_BROTE ORDER BY CD_GRUPO";

          st = con.prepareStatement(sQuery);
          rs = st.executeQuery();

          while (rs.next()) {
            ds = rs.getString("DS_GRUPO");
            dsl = rs.getString("DSL_GRUPO");
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (dsl != null)) {
              sDes = dsl;
            }
            else {
              sDes = ds;

            }
            data = new DataCDDS(rs.getString("CD_GRUPO"), sDes);
            listaGrupos.addElement(data);
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          sQuery = "SELECT CD_SITALERBRO, DS_SITALERBRO, DSL_SITALERBRO " +
              " FROM SIVE_T_SIT_ALERTA ORDER BY CD_SITALERBRO";

          st = con.prepareStatement(sQuery);
          rs = st.executeQuery();

          while (rs.next()) {
            ds = rs.getString("DS_SITALERBRO");
            dsl = rs.getString("DSL_SITALERBRO");
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (dsl != null)) {
              sDes = dsl;
            }
            else {
              sDes = ds;

            }
            data = new DataCDDS(rs.getString("CD_SITALERBRO"), sDes);
            listaSituacion.addElement(data);
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          sQuery = "SELECT  CD_TNOTIF,  DS_TNOTIF,  DSL_TNOTIF " +
              " FROM SIVE_T_NOTIFICADOR ORDER BY CD_TNOTIF";

          st = con.prepareStatement(sQuery);
          rs = st.executeQuery();

          while (rs.next()) {
            ds = rs.getString("DS_TNOTIF");
            dsl = rs.getString("DSL_TNOTIF");
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (dsl != null)) {
              sDes = dsl;
            }
            else {
              sDes = ds;

            }
            data = new DataCDDS(rs.getString("CD_TNOTIF"), sDes);
            listaTNotificador.addElement(data);
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          //pais
          sQuery = "select CD_PAIS, DS_PAIS from SIVE_PAISES";

          st = con.prepareStatement(sQuery);
          rs = st.executeQuery();

          datasuca dataPais = null;

          while (rs.next()) {
            dataPais = new datasuca();
            dataPais.put("CD_PAIS", rs.getString("CD_PAIS"));
            dataPais.put("DS_PAIS", rs.getString("DS_PAIS"));
            listaPaises.addElement(dataPais);
          }

          rs.close();
          rs = null;
          st.close();
          st = null;
          //CA
          sQuery = "select CD_CA, DS_CA, DSL_CA from SIVE_COM_AUT";

          st = con.prepareStatement(sQuery);
          rs = st.executeQuery();

          datasuca dataComunidad = null;
          while (rs.next()) {
            ds = rs.getString("DS_CA");
            dsl = rs.getString("DSL_CA");
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (dsl != null)) {
              sDes = dsl;
            }
            else {
              sDes = ds;

            }
            dataComunidad = new datasuca();
            dataComunidad.put("CD_CA", rs.getString("CD_CA"));
            dataComunidad.put("DS_CA", sDes);
            listaCA.addElement(dataComunidad);
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          //si el perfil es 5, pedira tb el cd_centro, ds_centro,
          //pasando el cd_e_notif con un DataCDDS en param
          if (param.getPerfil() == 5) {

            sQuery = "select b.CD_CENTRO, b.DS_CENTRO "
                + " from SIVE_C_NOTIF b, SIVE_E_NOTIF a "
                + " where (a.CD_E_NOTIF = ?  and "
                + " a.CD_CENTRO = b.CD_CENTRO )";

            DataCDDS dataEntrada = (DataCDDS) param.firstElement();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataEntrada.getCD());
            rs = st.executeQuery();

            while (rs.next()) {
              data = new DataCDDS(rs.getString("CD_CENTRO"),
                                  rs.getString("DS_CENTRO"));
              listaCENTRO.addElement(data);
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
          }

          /////////////////////////////////////////

          //listas
          listaSalida.addElement(listaGrupos);
          listaSalida.addElement(listaSituacion);
          listaSalida.addElement(listaTNotificador);
          listaSalida.addElement(listaPaises);
          listaSalida.addElement(listaCA);
          if (param.getPerfil() == 5) {
            listaSalida.addElement(listaCENTRO);
          }
          break;

      } //fin switch

      //valida la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return listaSalida;

  } //fin doWork

} //fin de la clase
