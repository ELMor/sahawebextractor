package brotes.servidor.tasasvacunables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

/**
 * @autor PDP
 * @version 1.0
 *
 * Actualiza NM_TENFVAC en SIVE_TATAQ_ENFVAC
 * Tabla:    SIVE_TATAQ_ENFVAC
 * Entrada:  Le suma una al NM_TENFVAC
 *
 *
 * Inserta un registro en la SIVE_TATAQ_ENFVAC
 * Entrada:  NM_TENFVAC
 *           CD_ANO
 *           NM_ALERBRO
 *           IT_EDAD
 *           CD_GEDAD
 *           DIST_EDAD_I
 *           DIST_EDAD_F
 *           NM_VACENF
 *           NM_VACNOENF
 *           NM_NOVACENF
 *           NM_NOVACNOENF
 */
public class SrvMntTas
    extends DBServlet {

  final protected int servletALTATASA = 1;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Lista de entrada;
    Lista lsEntrada = new Lista();
    lsEntrada = vParametros;

    // regularización
    Data dtReg = null;

    String sQuery = null;

    // vector con el resultado
    Lista snapShot = new Lista();

    // filtro
    Data dtFiltro = null;

    switch (opmode) {
      case servletALTATASA:
        try {
          // inserta el registro en SIVE_TATAQ_ENFVAC
          con.setAutoCommit(false);
          for (int i = 0; i < lsEntrada.size(); i++) {
            dtReg = (Data) lsEntrada.elementAt(i);
            //ResultSet rs = null;

            // Incrementamos el secuenciador

            sQuery =
                "select NM_TENFVAC from SIVE_TATAQ_ENFVAC order by NM_TENFVAC desc ";

            st = con.prepareStatement(sQuery);
            rs = st.executeQuery();
            String secuencial = null;
            if (rs.next()) {
              secuencial = rs.getString("NM_TENFVAC");
            }
            int secuenciador = Integer.parseInt(secuencial);
            secuenciador++;

            // cierres

            rs.close();
            rs = null;
            st.close();
            st = null;

            // Ahora insertamos nuestro registro....

            sQuery = "insert into SIVE_TATAQ_ENFVAC (NM_TENFVAC," +
                "CD_ANO," +
                "NM_ALERBRO," +
                "IT_EDAD," +
                "CD_GEDAD," +
                "DIST_EDAD_I," +
                "DIST_EDAD_F," +
                "NM_VACENF," +
                "NM_VACNOENF," +
                "NM_NOVACENF," +
                "NM_NOVACNOENF)" +
                "values (?,?,?,?,?,?,?,?,?,?,?)";
            st = con.prepareStatement(sQuery);
            st.setInt(1, secuenciador);
            st.setInt(2, Integer.parseInt(dtReg.getString("CD_ANO")));
            st.setInt(3, Integer.parseInt(dtReg.getString("NM_ALERBRO")));
            st.setString(4, "S");
            st.setString(5, dtReg.getString("CD_GEDAD"));
            st.setInt(6, Integer.parseInt(dtReg.getString("DIST_EDAD_I")));
            st.setInt(7, Integer.parseInt(dtReg.getString("DIST_EDAD_F")));
            st.setInt(8, Integer.parseInt(dtReg.getString("NM_VACENF")));
            st.setInt(9, Integer.parseInt(dtReg.getString("NM_VACNOENF")));
            st.setInt(10, Integer.parseInt(dtReg.getString("NM_NOVACENF")));
            st.setInt(11, Integer.parseInt(dtReg.getString("NM_NOVACNOENF")));

            st.executeUpdate();

            // cierre

            st.close();
            st = null;
          } // del for
          con.commit();

        }
        catch (SQLException e1) {

          // traza el error
          trazaLog(e1);
          bError = true;
          sMsg = "Error al insertar";

        }
        catch (Exception e2) {
          // traza el error
          trazaLog(e2);
          bError = true;

          // selecciona el mensaje de error
          sMsg = "Error inesperado";
        }

        break;
    } // cierro el switch

    // devuelve el error recogido
    if (bError) {
      con.rollback();
      throw new Exception(sMsg);

    }

    // valida la transacción
    con.commit();
    trazaLog(snapShot);
    return snapShot;
  } // cierra el catch

  public static void main(String args[]) {
    SrvMntTas srv = new SrvMntTas();
    Lista vParam = new Lista();
    Data filtro = new Data();
    Data dtReg = new Data();

    // parámetros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    dtReg.put("CD_ANO", "1999");
    dtReg.put("NM_ALERBRO", "2");
    dtReg.put("IT_EDAD", "S");
    dtReg.put("CD_GEDAD", "002");
    dtReg.put("DIST_EDAD_I", "0");
    dtReg.put("DIST_EDAD_F", "8");
    dtReg.put("NM_VACENF", "50");
    dtReg.put("NM_VACNOENF", "25");
    dtReg.put("NM_NOVACENF", "30");
    dtReg.put("NM_NOVACNOENF", "20");

    vParam.addElement(dtReg);

    srv.doDebug(1, vParam);
    srv = null;

  }

}