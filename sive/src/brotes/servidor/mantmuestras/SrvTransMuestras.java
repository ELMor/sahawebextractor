package brotes.servidor.mantmuestras;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Enumeration;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class SrvTransMuestras
    extends DBServlet {

  final protected int servletDO_SELECT = 1;
  final protected int servletDO_GETPAGE = 2;
  final protected int servletDO_INSERT = 3;
  final protected int servletDO_UPDATE = 4;
  final protected int servletDO_DELETE = 5;
  final protected int servletDO_INSERT_BD = 9;
  final protected int servletDO_INSERT_BLO = 10003;
  final protected int servletDO_INSERT_BD_BLO = 10009;
  final protected int servletDO_UPDATE_BLO = 10006;
  final protected int servletDO_DELETE_BLO = 10007;
  final protected int servletDO_GETPAGE_DESC = 8;
  //valor q adquiere nmMuestra y q se introduce a sive_microtos
  String nmMuestra = new String();

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // Vector con el resultado, Data con la QueryTool, opmode, QueryTool
    Lista vResulset = new Lista();
    Data dtQT = null;
    int modoOp;
    QueryTool queryTool = null;
    Enumeration eKey = null;
    Enumeration eValue = null;

    // Siguiente secuenciador en un alta
    int sigNM = 0;
    //siguiente secuenciador en un alta de microtox
    int sigNMMicrotox = 0;
    int sigNMMicrotoxBd = 0;

    try {
      con.setAutoCommit(false);

      for (int i = 0; i < vParametros.size(); i++) {
        dtQT = (Data) vParametros.elementAt(i);
        eKey = dtQT.keys();
        eValue = dtQT.elements();

        modoOp = Integer.parseInt( (String) eKey.nextElement());
        queryTool = (QueryTool) eValue.nextElement();

        switch (modoOp) {
          case servletDO_SELECT:

            vResulset.addElement(queryTool.doSelect(con));
            break;

          case servletDO_GETPAGE:
            vResulset.addElement(queryTool.doGetPage(vParametros.getCampoTrama(),
                vParametros.getValorTrama(),
                ">",
                "",
                con));
            break;

          case servletDO_GETPAGE_DESC:
            vResulset.addElement(queryTool.doGetPage(vParametros.getCampoTrama(),
                vParametros.getValorTrama(),
                "<",
                "DESC",
                con));
            break;

            //insert para elementos que pertenecen a la BD
          case servletDO_INSERT_BD:
          case servletDO_INSERT_BD_BLO:
            sigNMMicrotoxBd = obtenerSecuenciadorMicrotox(queryTool);
            establecerSecuenciadorMicrotoxBd(queryTool, sigNMMicrotoxBd);

            vResulset.addElement(queryTool.doInsert(con));
            break;

          case servletDO_INSERT:
          case servletDO_INSERT_BLO:

            //para sive_muestras_brote
            if (queryTool.getName().equals("SIVE_MUESTRAS_BROTE")) {
              sigNM = obtenerSecuenciador(queryTool);
              establecerSecuenciador(queryTool, sigNM);
            }

            //si no pertenece a BD -> establecer secuenciador
//            if (!(((Data)queryTool.getDatValue()).getString("IT_BD").equals("S"))){
            //para sive_microtox_muestras
            if (queryTool.getName().equals("SIVE_MICROTOX_MUESTRAS")) {
              sigNMMicrotox = obtenerSecuenciadorMicrotox(queryTool);
              establecerSecuenciadorMicrotox(queryTool, sigNMMicrotox);
            }

//            }

            vResulset.addElement(queryTool.doInsert(con));
            break;

          case servletDO_UPDATE_BLO:
          case servletDO_UPDATE:
            vResulset.addElement(queryTool.doUpdate(con));
            break;

          case servletDO_DELETE_BLO:
          case servletDO_DELETE:
            vResulset.addElement(queryTool.doDelete(con));
            break;
        }
      } // Fin for

      // Si todo ha ido bien se hace el commit
      con.commit();
    }
    catch (SQLException e1) {
      // rollback
      con.rollback();

      // traza el error
      trazaLog(e1);
      bError = true;

      // selecciona el mensaje de error
      switch (opmode) {
        case servletDO_SELECT:
        case servletDO_GETPAGE:
          sMsg = "Error al leer " + queryTool.getName();
          break;

        case servletDO_INSERT:
        case servletDO_INSERT_BLO:
          sMsg = "Error al insertar " + queryTool.getName();
          break;

        case servletDO_UPDATE:
        case servletDO_UPDATE_BLO:
          sMsg = "Error al actualizar " + queryTool.getName();
          break;

        case servletDO_DELETE:
        case servletDO_DELETE_BLO:
          sMsg = "Error al borrar " + queryTool.getName();
          break;
      }
    }
    catch (Exception e2) {
      // rollback
      con.rollback();

      // traza el error
      trazaLog(e2);
      bError = true;

      // selecciona el mensaje de error
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }

    return vResulset;
  }

  // Establece en la querytool el valor del nuevo secuenciador
  private void establecerSecuenciador(QueryTool qt, int sigNM) {
    // Establecimiento de ese secuenciador en la QueryTool
    ( (Data) qt.getDatValue()).put("NM_MUESTRA", sigNM + "");
    nmMuestra = sigNM + "";
  } // Fin establecerSecuenciadorAlerta()

  private int obtenerSecuenciador(QueryTool qt) throws SQLException {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    int sigNM = 0;

    sigNM = SecGeneral(con, st, rs, "NM_MUESTRA");

    return sigNM;
  } // obtenerSecuenciadorAlerta()

  private void establecerSecuenciadorMicrotoxBd(QueryTool qt, int sigNM) {
    // Establecimiento de ese secuenciador en la QueryTool
    ( (Data) qt.getDatValue()).put("NM_MICROTOX", sigNM + "");
  } // Fin establecerSecuenciadorAlerta()

  // Establece en la querytool el valor del nuevo secuenciador
  private void establecerSecuenciadorMicrotox(QueryTool qt, int sigNMicrotox) throws
      SQLException {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    int sigNMMuestra = 0;
    //Busco el nm_muestra �ltimo en SecGenarl,q es el q hay q insertar
    sigNMMuestra = SecGeneralMicrotox(con, st, rs, "NM_MUESTRA");

    ( (Data) qt.getDatValue()).put("NM_MUESTRA", sigNMMuestra + "");
    ( (Data) qt.getDatValue()).put("NM_MICROTOX", sigNMicrotox + "");
  } // Fin establecerSecuenciadorAlerta()

  private int obtenerSecuenciadorMicrotox(QueryTool qt) throws SQLException {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    int sigNMMicrotox = 0;

    sigNMMicrotox = SecGeneral(con, st, rs, "NM_MICROTOX");

    return sigNMMicrotox;
  } // obtenerSecuenciadorAlerta()

  public static int SecGeneralMicrotox(Connection conec,
                                       PreparedStatement stat,
                                       ResultSet rset,
                                       String sNombreColum) throws SQLException {

    int iNM = 0;

    String sQuery = "SELECT * " +
        " FROM SIVE_SEC_GENERAL ";

    stat = conec.prepareStatement(sQuery);
    rset = stat.executeQuery();

    if (rset.next()) {
      iNM = rset.getInt(sNombreColum);
    }
    return iNM;
  } //end SecGeneralMicrotox

  /**
   * Inserta/Actualiza en la tabla sive_sec_general
   * Devuelve el Secuenciador ya incrementado
   * copia de sapp/Funciones
   */
  public static int SecGeneral(Connection conec,
                               PreparedStatement stat,
                               ResultSet rset,
                               String sNombreColum) throws SQLException {

    int iNM = 0;

    String sQuery = "SELECT * " +
        " FROM SIVE_SEC_GENERAL ";

    stat = conec.prepareStatement(sQuery);
    rset = stat.executeQuery();

    boolean bExisteRegistro = false;
    if (rset.next()) {
      if (rset == null) {
        //insertar: todos a cero excepto el mio que va a 1
        bExisteRegistro = false;
      }
      else {
        //update: solo al campo en cuestion
        bExisteRegistro = true;
        iNM = rset.getInt(sNombreColum);
      }
    }

    if (bExisteRegistro) {

      rset.close();
      rset = null;
      stat.close();
      stat = null;

      sQuery = "UPDATE SIVE_SEC_GENERAL SET " + sNombreColum + " = ? ";

      stat = conec.prepareStatement(sQuery);
      iNM++;
      stat.setInt(1, iNM);

      stat.executeUpdate();
      stat.close();
      stat = null;

    } //existe el registro

    else if (!bExisteRegistro) {
      //Insert de todo a 0 excepto de XXX a 1
      iNM = 1;
      int i = 0;
      ResultSetMetaData rsmd = rset.getMetaData(); //no lo cerre antes
      int numCols = rsmd.getColumnCount();

      sQuery = "INSERT INTO SIVE_SEC_GENERAL " +
          "( " + sNombreColum;

      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColum)) {
          sQuery = sQuery + ", " + rsmd.getColumnLabel(i);
        }
      }
      sQuery = sQuery + ") ";
      sQuery = sQuery + " values (?";

      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColum)) {
          sQuery = sQuery + ", " + "?";
        }
      }
      sQuery = sQuery + " ) ";

      int iValor = 1;

      stat = conec.prepareStatement(sQuery);
      stat.setInt(iValor, 1);
      iValor++;
      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColum)) {
          stat.setInt(iValor, 0);
          iValor++;
        }
      }

      stat.executeUpdate();
      stat.close();
      stat = null;

    } //no existe el registro

    return (iNM);
  }
} //end class
