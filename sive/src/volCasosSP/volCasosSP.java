//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales a fichero ASCII sin preguntas.

package volCasosSP;

import java.util.ResourceBundle;

import capp.CApp;

public class volCasosSP
    extends CApp {

  ResourceBundle res;
  //public volCasosInd() {
  //}

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("volCasosSP.Res" + this.getIdioma());
    setTitulo(res.getString("msg12.Text"));
    CApp a = (CApp)this;
    PanelConsVolCasosSP consVol = new PanelConsVolCasosSP(a);
    VerPanel(res.getString("msg13.Text"), consVol, true);
  }

}
