//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package volCasosInd;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import sapp.StubSrvBD;

public class DialogoMasPreg
    extends CDialog { //CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res = ResourceBundle.getBundle("volCasosInd.Res" +
                                                app.getIdioma());
  final int modoESPERA = 2;

  final int MODELO = 10;
  final int PREGUNTA = 11;
  final int MOD_Y_PREG = 12;

  //rutas imagenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  protected StubSrvBD stubCliente = new StubSrvBD();

  protected int modoOperacion = modoNORMAL;

  protected boolean resultado = false;
  protected DataModPreg fila;

  // stub's
  final String strSERVLETModYPreg = "servlet/SrvModPreg";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  CCargadorImagen imgs = null;

  String tSive = new String();

  XYLayout xYLayout = new XYLayout();

  Label lblPregunta = new Label();
  TextField txtCodPreg = new TextField();
  ButtonControl btnCtrlBuscarPreg = new ButtonControl();
  TextField txtDesPreg = new TextField();
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnAgnadir = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  focusAdapterPMP txtFocusAdapter = new focusAdapterPMP(this);
  textAdapterPMP txtTextAdapter = new textAdapterPMP(this);
  actionListenerPMP btnActionListener = new actionListenerPMP(this);

  DialogoMasPregtxt_keyAdapter txtKeyAdapter = new DialogoMasPregtxt_keyAdapter(this);

  CheckboxGroup checkboxAgrupar = new CheckboxGroup();
  Label lblModelo = new Label();
  TextField txtModelo = new TextField();
  ButtonControl btnCtrlBuscarMod = new ButtonControl();
  TextField txtDesMod = new TextField();

  public DialogoMasPreg(CApp a) {

    super(a);
    try {

      tSive = a.getTSive();
      //setApp(a);
      jbInit();
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    this.setTitle(res.getString("msg3.Text"));

    imgs = new CCargadorImagen(getCApp(), imgNAME);
    imgs.CargaImagenes();

    btnCtrlBuscarMod.setImage(imgs.getImage(0));
    btnCtrlBuscarPreg.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));
    btnAgnadir.setImage(imgs.getImage(2));
    btnCancelar.setImage(imgs.getImage(3));
    btnLimpiar.setSize(btnCancelar.getSize());

    this.setSize(new Dimension(569, 200));
    xYLayout.setHeight(301);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnAgnadir.setLabel(res.getString("btnAgnadir.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    // gestores de eventos

    btnCtrlBuscarPreg.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnAgnadir.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);
    btnCtrlBuscarMod.addActionListener(btnActionListener);

    txtCodPreg.addTextListener(txtTextAdapter);
    txtCodPreg.addFocusListener(txtFocusAdapter);
    txtModelo.addTextListener(txtTextAdapter);
    txtModelo.addFocusListener(txtFocusAdapter);

    txtModelo.addKeyListener(txtKeyAdapter);
    txtCodPreg.addKeyListener(txtKeyAdapter);

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnCtrlBuscarPreg.setActionCommand("buscarPregunta");
    btnCtrlBuscarMod.setActionCommand("buscarModelo");
    btnAgnadir.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");
    btnCancelar.setActionCommand("cancelar");

    txtCodPreg.setName("txtCodPreg");
    txtCodPreg.setBackground(new Color(255, 255, 150));
    txtModelo.setName("txtModelo");
    txtDesPreg.setEditable(false);
    txtDesMod.setEditable(false);

    lblPregunta.setText(res.getString("lblPregunta.Text"));
    lblModelo.setText(res.getString("lblModelo.Text"));

    xYLayout.setWidth(596);
    xYLayout.setHeight(200); ;
    this.setLayout(xYLayout);

    this.add(lblModelo, new XYConstraints(26, 42, 55, -1));
    this.add(txtModelo, new XYConstraints(143, 42, 77, -1));
    this.add(btnCtrlBuscarMod, new XYConstraints(225, 42, -1, -1));
    this.add(txtDesMod, new XYConstraints(254, 42, 287, -1));
    this.add(lblPregunta, new XYConstraints(26, 82, 101, -1));
    this.add(txtCodPreg, new XYConstraints(143, 82, 77, -1));
    this.add(btnCtrlBuscarPreg, new XYConstraints(225, 82, -1, -1));
    this.add(txtDesPreg, new XYConstraints(254, 82, 287, -1));
    this.add(btnLimpiar, new XYConstraints(275, 125, -1, -1));
    this.add(btnAgnadir, new XYConstraints(375, 125, -1, -1));
    this.add(btnCancelar, new XYConstraints(475, 125, -1, -1));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de la descripcion de la pregunta
    if (txtDesPreg.getText().length() == 0) {
      bDatosCompletos = false;

    }

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodPreg.setEnabled(true);
        txtDesPreg.setEnabled(true);
        txtModelo.setEnabled(true);
        txtDesMod.setEnabled(true);

        btnCtrlBuscarPreg.setEnabled(true);
        btnCtrlBuscarMod.setEnabled(true);

        btnLimpiar.setEnabled(true);
        btnAgnadir.setEnabled(true);
        btnCancelar.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodPreg.setEnabled(false);
        txtDesPreg.setEnabled(false);
        txtModelo.setEnabled(false);
        txtDesMod.setEnabled(false);

        btnCtrlBuscarPreg.setEnabled(false);
        btnCtrlBuscarMod.setEnabled(false);

        btnLimpiar.setEnabled(false);
        btnAgnadir.setEnabled(false);
        btnCancelar.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //Busca los modelos
  void btnCtrlbuscarMod_actionPerformed(ActionEvent evt) {
    DataModPreg data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaMod lista = new CListaMod(getCApp(),
                                      res.getString("msg5.Text"),
                                      stubCliente,
                                      strSERVLETModYPreg,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION,
                                      tSive, MODELO);

      lista.show();
      data = (DataModPreg) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.getCApp(), CMessage.msgERROR,
                             excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtModelo.removeTextListener(txtTextAdapter);
      txtModelo.setText(data.codMod);
      txtDesMod.setText(data.desMod);
      txtModelo.addTextListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los modelos
  void btnCtrlbuscarPreg_actionPerformed(ActionEvent evt) {
    DataModPreg data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();

    int mod;
    if (txtModelo.getText().equals("")) {
      mod = PREGUNTA;
    }
    else {
      mod = MOD_Y_PREG;

    }
    try {
      CListaPreg lista = new CListaPreg(getCApp(),
                                        res.getString("msg6.Text"),
                                        stubCliente,
                                        strSERVLETModYPreg,
                                        servletOBTENER_X_CODIGO,
                                        servletOBTENER_X_DESCRIPCION,
                                        servletSELECCION_X_CODIGO,
                                        servletSELECCION_X_DESCRIPCION,
                                        tSive, mod, txtModelo.getText());
      lista.show();
      data = (DataModPreg) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.getCApp(), CMessage.msgERROR,
                             excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodPreg.removeTextListener(txtTextAdapter);
      txtCodPreg.setText(data.codPreg);
      txtDesPreg.setText(data.desPreg);
      txtCodPreg.addTextListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtModelo.setText("");
    txtDesMod.setText("");
    txtCodPreg.setText("");
    txtDesPreg.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCancelar_actionPerformed(ActionEvent evt) {
    this.resultado = false;
    this.dispose();
  }

  void btnAgnadir_actionPerformed(ActionEvent evt) {

    CMessage msgBox;

    if (isDataValid()) {
      this.resultado = true;
      this.fila = new DataModPreg(txtModelo.getText(),
                                  txtDesMod.getText(),
                                  txtCodPreg.getText(),
                                  txtDesPreg.getText(),
                                  this.tSive);
    }
    else {
      msgBox = new CMessage(this.getCApp(), CMessage.msgADVERTENCIA,
                            res.getString("msg7.Text"));
      msgBox.show();
      msgBox = null;
    }

    dispose();
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    DataModPreg dModPreg;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtModelo"))
        && (txtModelo.getText().length() > 0)) {
      //#// System_out.println("Pierde el foco el textModelo");
      param = new CLista();
      DataModPreg aux = new DataModPreg(txtModelo.getText(), "",
                                        "", "", tSive);
      aux.modo = MODELO;
      param.addElement(aux);
      strServlet = strSERVLETModYPreg;
      modoServlet = servletOBTENER_X_CODIGO;
    }

    // sera distinto si se ha puesto antes un modelo o no
    // Por lo menos parece que se van a usar servlets distintos
    else if ( (txt.getName().equals("txtCodPreg"))
             && (txtCodPreg.getText().length() > 0)) {
      if (!txtModelo.getText().equals("")) {
        // Servlet que devuelva la pregunta dado un modelo
        //#// System_out.println("Pierde el foco el textCodPreg");
        param = new CLista();
        DataModPreg aux = new DataModPreg(txtModelo.getText(), "",
                                          txtCodPreg.getText(), "", tSive);
        aux.modo = MOD_Y_PREG;
        param.addElement(aux);
        strServlet = strSERVLETModYPreg;
        modoServlet = servletOBTENER_X_CODIGO;
      }
      else {
        // Servlet que devuelva la pregunta sim un modelo concreto
        //#// System_out.println("Pierde el foco el textCodPreg");
        param = new CLista();
        DataModPreg aux = new DataModPreg("", "",
                                          txtCodPreg.getText(), "", tSive);
        aux.modo = PREGUNTA;
        param.addElement(aux);
        strServlet = strSERVLETModYPreg;
        modoServlet = servletOBTENER_X_CODIGO;
      }
    }
    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(getCApp().getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtModelo")) {
            //#// System_out.println("realiza el cast y rellena los datos del modulo");
            dModPreg = (DataModPreg) param.firstElement();
            txtModelo.removeTextListener(txtTextAdapter);
            txtModelo.setText(dModPreg.codMod);
            txtDesMod.setText(dModPreg.desMod);
            txtModelo.addTextListener(txtTextAdapter);
          }
          else if (txt.getName().equals("txtCodPreg")) {
            //#// System_out.println("realiza el cast y rellena los datos de preg");
            dModPreg = (DataModPreg) param.firstElement();
            txtCodPreg.removeTextListener(txtTextAdapter);
            txtCodPreg.setText(dModPreg.codPreg);
            txtDesPreg.setText(dModPreg.desPreg);
            txtCodPreg.addTextListener(txtTextAdapter);
          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.getCApp(), CMessage.msgAVISO,
                             res.getString("msg8.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.getCApp(), CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  /*
    // cambio en una caja de texto
    void textValueChanged(TextEvent e) {
      TextField txt = (TextField) e.getSource();
      // gestion de datos
      if ( (txt.getName().equals("txtModelo"))
           && (txtDesMod.getText().length() > 0)) {
        txtModelo.setText("");
        txtDesMod.setText("");
      }
      else if ( (txt.getName().equals("txtCodPreg"))
                && (txtDesPreg.getText().length() > 0)) {
        txtCodPreg.setText("");
        txtDesPreg.setText("");
      }
      Inicializar();
    }
   */

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtModelo")) &&
        (txtDesMod.getText().length() > 0)) {
      txtDesMod.setText("");
    }
    else if ( (txt.getName().equals("txtCodPreg")) &&
             (txtDesPreg.getText().length() > 0)) {
      txtDesPreg.setText("");
    }

    Inicializar();
  }

  public boolean resultado() {
    return resultado;
  }

  public DataModPreg obtenerPregunta() {
    return this.fila;
  }

} //clase

// action listener para los botones

class actionListenerPMP
    implements ActionListener, Runnable {
  DialogoMasPreg adaptee = null;
  ActionEvent e = null;

  public actionListenerPMP(DialogoMasPreg adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarModelo")) {
      adaptee.btnCtrlbuscarMod_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarPregunta")) {
      adaptee.btnCtrlbuscarPreg_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnAgnadir_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapterPMP
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DialogoMasPreg adaptee;
  FocusEvent event;

  focusAdapterPMP(DialogoMasPreg adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// cambio en una caja de codigos
class textAdapterPMP
    implements java.awt.event.TextListener {
  DialogoMasPreg adaptee;

  textAdapterPMP(DialogoMasPreg adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    //  adaptee.textValueChanged(e);
  }
}

////////////////////// Clases para listas

// lista de valores
class CListaMod
    extends CListaValores {

  String tSive;
  int modo;

  public CListaMod(CApp a,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion,
                   String tS, int mod) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    tSive = tS;
    modo = mod;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    DataModPreg aux = new DataModPreg(s, s, "", "", tSive);
    aux.modo = modo;
    return aux;
  }

  public String getCodigo(Object o) {
    return ( ( (DataModPreg) o).codMod);
  }

  public String getDescripcion(Object o) {
    return ( ( (DataModPreg) o).desMod);
  }
}

class CListaPreg
    extends CListaValores {

  String tSive;
  int modo;
  String codMod;
  public CListaPreg(CApp a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion,
                    String tS, int mod, String cdMod) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    tSive = tS;
    modo = mod;
    codMod = cdMod;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    DataModPreg aux = new DataModPreg(codMod, "", s, s, tSive);
    aux.modo = modo;
    return aux;
  }

  public String getCodigo(Object o) {
    return ( ( (DataModPreg) o).codPreg);
  }

  public String getDescripcion(Object o) {
    return ( ( (DataModPreg) o).desPreg);
  }
}

class DialogoMasPregtxt_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialogoMasPreg adaptee;

  DialogoMasPregtxt_keyAdapter(DialogoMasPreg adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
