//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package volCasosInd;

import java.io.Serializable;

public class ParVolCasosInd
    implements Serializable {

  public String anoDesde = new String();
  public String semDesde = new String();
  public String anoHasta = new String();
  public String semHasta = new String();

  public String enfermedad = new String();

  public String zbs = new String();
  public String area = new String();
  public String distrito = new String();
  public String provincia = new String();
  public String municipio = new String();

  public int numPagina = 0;

  public ParVolCasosInd() {
  }

  public ParVolCasosInd(String anoD, String semD,
                        String anoH, String semH,
                        String enf) {
    anoDesde = anoD;
    semDesde = semD;
    anoHasta = anoH;
    semHasta = semH;

    enfermedad = enf;
  }
}
