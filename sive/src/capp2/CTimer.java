package capp2;

/**
 * @autor IC
 * @version 1.0
 *
 * Clase que se comporta como un temporizador. Cada periodo mseg dispara el método
 * TimerEnd del interfaz CTimerListener.
 *
 */
public class CTimer
    extends Thread {
  CTimerListener timerListener;
  String nombre;
  long timer;

  public CTimer(String s, long mseg, CTimerListener tl) {
    super(s);

    this.timerListener = tl;
    this.nombre = s;
    this.timer = mseg;
  } //end Timer

  public void Start() {
    start();
  } //end Start

  public void End() {
    timerListener = null;
    stop();
  } //end stop

  public void run() {
    try {
      Thread.sleep(timer);

      //Informar terminación
      if (timerListener != null) {
        timerListener.TimerEnd(this);
      } //endif

    }
    catch (Exception e) {
      timerListener.TimerError(this, e);
    } //endtry
  } //end run

} //endclass Timer
