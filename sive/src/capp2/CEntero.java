package capp2;

/**
 * @autor IC
 * @version 1.0
 *
 * Caja de texto que admite una entrada del tipo #######
 * La longitud m�xima se informa en el constructor.
 *
 * LRG Cambio permite accion cursores dcha e izqda y otros eventos
 */
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class CEntero
    extends TextField
    implements KeyListener {
  private int iMaxDig;
  private int iLong;
  private boolean bControl;

  public CEntero(int iE) {
    iMaxDig = iE;
    iLong = 0;
    addKeyListener(this);
  }

  public void keyPressed(KeyEvent event) {
    char c = event.getKeyChar();
    iLong = getText().length();

    //C�digo de la tecla asociada a este evento (tecla pulsada)
    int iKey = event.getKeyCode();

    if ( (int) c == 8 || (int) c == 9 || (int) c == 127) {
      return;
    }

    //_____ LRG
    //Si se pulsan ciertas teclas de control (entre ellas las de cursores) se admite el evento
    //Si algunos de estos eventos no se admitesen se producir�an efectos extra�os
    else if ( (iKey == KeyEvent.VK_LEFT)
             || (iKey == KeyEvent.VK_RIGHT)
             || (iKey == KeyEvent.VK_UP)
             || (iKey == KeyEvent.VK_DOWN)
             || (iKey == KeyEvent.VK_INSERT)
             || (iKey == KeyEvent.VK_CLEAR)
             || (iKey == KeyEvent.VK_DELETE)
             || (iKey == KeyEvent.VK_HOME)
             || (iKey == KeyEvent.VK_END)
             || (iKey == KeyEvent.VK_PAGE_UP)
             || (iKey == KeyEvent.VK_PAGE_DOWN)
             || (iKey == KeyEvent.VK_F1)
             || (iKey == KeyEvent.VK_F2)
             || (iKey == KeyEvent.VK_F3)
             || (iKey == KeyEvent.VK_F4)
             || (iKey == KeyEvent.VK_F5)
             || (iKey == KeyEvent.VK_F6)
             || (iKey == KeyEvent.VK_F7)
             || (iKey == KeyEvent.VK_F8)
             || (iKey == KeyEvent.VK_F9)
             || (iKey == KeyEvent.VK_F10)
             || (iKey == KeyEvent.VK_F11)
             || (iKey == KeyEvent.VK_F12)
             ) {
      return;
    }
    //__________

    //Del resto de teclas:
    else {
      //Si tecla pulsada no representa un d�gito se consume el evento
      // (Como si no hubiera evento)
      if (!Character.isDigit(c)) {
        event.consume(); // letra
      }
      //Si tecla pulsada representa un d�gito pero long es excesiva se consume el evento
      // (Como si no hubiera evento)
      else if (iLong >= iMaxDig) {
        event.consume();
      }
    }
  }

  public void keyTyped(KeyEvent evt) {
    /*
         iLong = getText().length();
         int iKey = evt.getKeyCode();
         if(bControl)  // tabulador
        return;
         if(!Character.isDigit(evt.getKeyChar())) {
        evt.consume(); // letra
         } else if(iLong >= iMaxDig) {
        evt.consume(); // se supera la longitud m�xima
         }
     */
  }

  public void keyReleased(KeyEvent evt) {
  }

}
