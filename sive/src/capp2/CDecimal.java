package capp2;

/**
 * @autor IC
 * @version 1.0
 *
 * Caja de texto que admite una entrada del tipo #######.###
     * La longitud de la parte entera y de la decimal se informan en el constructor.
 *
 */
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class CDecimal
    extends java.awt.TextField
    implements KeyListener {
  private int iMaxDig;
  private int iEnt;
  private int iDec;
  private int iLong;
  private char cDecimal;
  private boolean bDecimal = false;

  public CDecimal(int iE, int iD) {
    iMaxDig = iE + 1 + iD;
    iEnt = iE;
    iDec = iD;
    iLong = 0;

    //{{REGISTER_LISTENERS
    this.addKeyListener(this);
    //}}
  }

  public void keyPressed(KeyEvent event) {
    char c = event.getKeyChar();

    iLong = getText().length();
    if ( (int) c == 8 || (int) c == 9 || (int) c == 127) {
      return;
    }
    else {
      if (!Character.isDigit(c) && c != '.') {
        event.consume();
      }
      else if (iLong >= iMaxDig) {
        event.consume();
      }
      else if (c == '.') {
        if (bDecimal) {
          event.consume();
        }
        else {
          cDecimal = c;
          bDecimal = true;
        }
      }
      else if (Character.isDigit(c)) {
        if (bDecimal) {
          if ( (iLong - getText().indexOf(cDecimal)) > iDec) {
            event.consume();
          }
        }
        else {
          if (iLong >= iEnt) {
            event.consume();
          }
        }
      }
    }
  }

  public void keyTyped(KeyEvent e) {
  }

  public void keyReleased(KeyEvent event) {
    char c = event.getKeyChar();

    if (getText().indexOf(cDecimal) < 0 && c != cDecimal) {
      bDecimal = false;
    }
  }

  /**
   * Devuelve el valor introducido con el formato ######,###
   *
   * @return valor introducido.
   */
  public String getValorConComa() {
    String num = this.getText();

    if (num.indexOf(".") > -1) {
      num.replace('.', ',');
    }
    else if (num.indexOf("'") > -1) {
      num.replace('\'', ',');
    }
    return num;
  }

}