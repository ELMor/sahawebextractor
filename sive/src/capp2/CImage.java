package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Panel que presenta una im�gen. La im�gen puede haberse cargado o se carga con ayuda
 * de la librer�a de im�genes del applet.
 *
 */
import java.awt.Graphics;
import java.awt.Image;

public class CImage
    extends CPanel {
  protected Image image = null;
  protected int width;
  protected int height;

  public CImage(CApp a, String file, int w, int h) {
    super(a);
    width = w;
    height = h;
    this.setSize(width, height);
    this.getApp().getLibImagenes().put(file);
    this.getApp().getLibImagenes().CargaImagenes();
    image = this.getApp().getLibImagenes().get(file);
  }

  public CImage(CApp a, Image i, int w, int h) {
    super(a);
    width = w;
    height = h;
    this.setSize(width, height);
    image = i;
  }

  public void Inicializar() {
  }

  public void paint(Graphics g) {
    Graphics g2;

    if (image != null) {
      g2 = g.create();
      g2.clipRect(0, 0, width, height);
      g2.drawImage(image, 0, 0, width, height, this);
      g2.dispose();
    }
  }

  public Image getImage() {
    return (image);
  }

  // actualizaciones
  public void update(Graphics g) {
    paint(g);
  }
}
