package capp2;

import com.borland.jbcl.control.ButtonControl;

/**
 * Botón con identificación para la gestión de USU.
 * Sobreescribe el método setEnable para encapsular las autorizaciones
 * sobre las acciones de la aplicación.
 * Nota: si no se indica una clave, no se tiene en cuenta USU
 *
 * @autor LSR
 * @version 1.0
 */
public class UButtonControl
    extends ButtonControl {
  private String key = null;
  private CApp app = null;

  public UButtonControl(String k, CApp a) {
    super();
    key = k;
    app = a;
    if (!app.isAllowed(key)) {
      super.setEnabled(false);
    }
  }

  public void setEnabled(boolean enabled) {
    if (app.isAllowed(key)) {
      super.setEnabled(enabled);
    }
    else {
      super.setEnabled(false);
    }
  }
}
