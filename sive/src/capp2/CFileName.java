package capp2;

//import capp.*;
import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;

public class CFileName
    extends CPanel {

  XYLayout xYLayout1 = new XYLayout();
//  ResourceBundle res;
  Label lbl = new Label();
  public TextField txtFile = new TextField();
  ButtonControl btn = new ButtonControl();
  protected CCargadorImagen imgs = null;
  final String imgBROWSER = "images/browser.gif";
  final String fileDEFAULT = "c:\\data.txt";
  protected int fileMODE;

  //Atibuto para inicializar en modo de elecci�n fichero o directorio
  int modoEleccion = 1;
  //Constantes para indicar  modo de elecci�n fichero o directorio
  public final static int ELIGE_FICHERO = 1;
  public final static int ELIGE_DIRECTORIO = 2;

  final String dirDEFAULT = "c:\\";

  String sNombre = "data.txt";
  String sDirectorio = dirDEFAULT;

  public CFileName(CApp a) {
    try {
      setApp(a);
//      res = ResourceBundle.getBundle("capp.Res" + this.app.getIdioma());
      fileMODE = FileDialog.SAVE; // por defecto es de modo grabado
      modoEleccion = ELIGE_FICHERO;
      jbInit();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public CFileName(CApp a, int modo) {
    try {
      setApp(a);
//      res = ResourceBundle.getBundle("capp.Res" + this.app.getIdioma());
      fileMODE = modo;
      modoEleccion = ELIGE_FICHERO;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public CFileName(CApp a, int modo, int fichODirectorio) {
    try {
      setApp(a);
//      res = ResourceBundle.getBundle("capp.Res" + this.app.getIdioma());
      fileMODE = modo;
      modoEleccion = fichODirectorio;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void hazEnabled(boolean booleano) {
    btn.setEnabled(booleano);
    this.setEnabled(booleano);
  }

  public String getNombre() {
    return sNombre;
  }

  public String getDirectorio() {
    return sDirectorio;
  }

  public void Inicializar() {
  }

  void jbInit() throws Exception {
    imgs = new CCargadorImagen(app);
    imgs.setImage(imgBROWSER);
    imgs.CargaImagenes();
    xYLayout1.setHeight(48);
    txtFile.setText(fileDEFAULT);

    if (modoEleccion == ELIGE_DIRECTORIO) {
      txtFile.setText(dirDEFAULT);

    }
    txtFile.setBackground(new Color(255, 255, 150));
    lbl.setText("Nombre del fichero:");
    btn.setImage(imgs.getImage(0));
    btn.addActionListener(new CFileName_btn_actionAdapter(this));
    xYLayout1.setWidth(432);
    this.setLayout(xYLayout1);
    this.add(lbl, new XYConstraints(13, 11, 116, -1));
    this.add(txtFile, new XYConstraints(129, 11, 259, -1));
    this.add(btn, new XYConstraints(398, 11, 24, 24));
    this.setBorde(false);
  }

  void btn_actionPerformed(ActionEvent e) {
    if (modoEleccion == ELIGE_FICHERO) {
      FileDialog dialog = new FileDialog(app.getFrame(), "Nombre del fichero",
                                         fileMODE);
      dialog.show();
      if (dialog.getFile() != null) {
        txtFile.setText(dialog.getDirectory() + dialog.getFile());
        sNombre = dialog.getFile();
        sDirectorio = dialog.getDirectory();
      }
    } //if modoELECCION

    else {

      FileDialog dialog = new FileDialog(app.getFrame(), "Nombre del fichero",
                                         fileMODE);
      dialog.show();
      if (dialog.getDirectory() != null) {
        txtFile.setText(dialog.getDirectory());
        sNombre = "";
        sDirectorio = dialog.getDirectory();
      }
    } //if modoELECCION
  }

  /**
   * Cambia el t�tulo de la etiqueta del panel
   *
   */
  /*
   * M�todo : setTitulo(String titulo)
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  public void setTitulo(String titulo) {
    lbl.setText(titulo);
  }

}

class CFileName_btn_actionAdapter
    implements java.awt.event.ActionListener {
  CFileName adaptee;

  CFileName_btn_actionAdapter(CFileName adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btn_actionPerformed(e);
  }
}
