package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Panel que incorpora un t�tulo y dos botones:
 *
 *	SALIR: que carga la p�gina default.html en el frame del applet.
 *	AYUDA: que carga la p�gina que indica el par�metro del applet AYUDA en
 *		 una nueva ventana.
 *
 */
import java.net.URL;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;

public class CTitulo
    extends CPanel {

  final String imgSALIR = "images/salir.gif";
  final String imgAYUDA = "images/ayuda.gif";

  BorderLayout borderLayout = new BorderLayout();
  BorderLayout borderLayoutB = new BorderLayout();
  Label lblTitulo = new Label();
  ButtonControl btnCerrar = new ButtonControl();
  ButtonControl btnAyuda = new ButtonControl();
  Panel pnlButton = new Panel();

  public CTitulo(CApp a) {
    try {
      setApp(a);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void Inicializar() {
  }

  public void setTitulo(String title) {
    lblTitulo.setText(" " + title);
    this.doLayout();
  }

  void jbInit() throws Exception {

    // carga las im�genes
    app.getLibImagenes().put(imgSALIR);
    app.getLibImagenes().put(imgAYUDA);
    app.getLibImagenes().CargaImagenes();

    setLayout(borderLayout);
    setForeground(SystemColor.activeCaptionText);
    setBackground(SystemColor.activeCaption);
    lblTitulo.setFont(new Font("Dialog", 1, 12));
    btnCerrar.addActionListener(new CTitulo_btnCerrar_actionAdapter(this));
    btnCerrar.setImage(app.getLibImagenes().get(imgSALIR));
    new CContextHelp("Salir", btnCerrar);

    btnAyuda.addActionListener(new CTitulo_btnAyuda_actionAdapter(this));
    btnAyuda.setImage(app.getLibImagenes().get(imgAYUDA));
    new CContextHelp("Ayuda", btnAyuda);

    pnlButton.setLayout(borderLayoutB);
    pnlButton.add(btnCerrar, BorderLayout.EAST);
    pnlButton.add(btnAyuda, BorderLayout.WEST);
    add(lblTitulo, BorderLayout.CENTER);
    add(pnlButton, BorderLayout.EAST);
  }

  void btnCerrar_actionPerformed(ActionEvent e) {
    try {
      System.runFinalization();
      System.gc();
      app.getAppletContext().showDocument(new URL(app.getCodeBase(),
                                                  "default.html"), "_self");
    }
    catch (Exception ex) {}
  }

  void btnAyuda_actionPerformed(ActionEvent e) {
    try {
//      app.getAppletContext().showDocument( new URL(app.getCodeBase(), app.getParameter("AYUDA")),"_blank");
      //Se obtiene la URL de la pag. html de ayuda
      URL url = new URL(app.getParametro("URL_HTML") + "zip/" +
                        app.getParameter("AYUDA"));
      //Se carga la p�gina en el frame "_blank" (nueva ventana)
      app.getAppletContext().showDocument(url, "_blank");

    }
    catch (Exception ex) {}
  }

}

class CTitulo_btnCerrar_actionAdapter
    implements java.awt.event.ActionListener {
  CTitulo adaptee;

  CTitulo_btnCerrar_actionAdapter(CTitulo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnCerrar_actionPerformed(e);
  }
}

class CTitulo_btnAyuda_actionAdapter
    implements java.awt.event.ActionListener {
  CTitulo adaptee;

  CTitulo_btnAyuda_actionAdapter(CTitulo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAyuda_actionPerformed(e);
  }
}
