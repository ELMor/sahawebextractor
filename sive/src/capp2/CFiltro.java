
package capp2;

import sapp2.Lista;

/**
 * @autor LSR
 * @version 1.0
 *
 * Interfaz que deben soportar los paneles filtro
 * asociados a una CListaMantenimiento.
 *
 */
public interface CFiltro {
  public Lista siguientePagina();

  public void realizaOperacion(int j);
}
