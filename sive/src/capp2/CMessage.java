package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Di�logo para mostrar un mensaje al usuario.
 *
 *	msgERROR: Muestra un mensaje de error. S�lo aparece el bot�n aceptar.
 *	msgAVISO: Muestra un mensaje de aviso. S�lo aparece el bot�n aceptar.
 *	msgADVERTENCIA: Muestra una advertencia. Permite la elecci�n entre aceptar y cancelar
 *	msgPREGUNTA: Muestra una pregunta con las opciones s�/no.
 *
 */
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;

public class CMessage
    extends Dialog {

  // propiedades
  protected CApp app = null;
  protected boolean response = false;

  // tama�o predefinido (s�lo en altura)
  final int msgHEIGHT = 130;
  protected int msgWIDTH = 300;

  // componentes
  ButtonControl btn1 = new ButtonControl();
  ButtonControl btn2 = new ButtonControl();
  Label lbl = new Label();
  Panel pnl = new Panel();
  XYLayout xyLyt = new XYLayout();

  // constantes de configuraci�n
  final static public int msgERROR = 0;
  final static public int msgADVERTENCIA = 1;
  final static public int msgPREGUNTA = 2;
  final static public int msgAVISO = 3;

  // constantes de respuesta
  final static public boolean msgSI = true;
  final static public boolean msgNO = false;
  final static public boolean msgACEPTAR = true;
  final static public boolean msgCANCELAR = false;

  public CMessage(CApp a, int type, String message) {
    super(a.getParentFrame(), "", true);

    final String imgERROR = "images/msg0.gif";
    final String imgADVERTENCIA = "images/msg1.gif";
    final String imgPREGUNTA = "images/msg2.gif";
    final String imgAVISO = "images/msg3.gif";
    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";

    String title = "";
    String filename = "";
    CImage image = null;

    app = a;

    // colores y tipo de letra
    this.setLayout(xyLyt);
    this.setBackground(Color.lightGray);
    this.setForeground(Color.black);
    pnl.setBackground(Color.lightGray);
    pnl.setForeground(Color.black);
    lbl.setFont(new Font("Dialog", 1, 12));

    // im�genes
    app.getLibImagenes().put(imgERROR);
    app.getLibImagenes().put(imgADVERTENCIA);
    app.getLibImagenes().put(imgPREGUNTA);
    app.getLibImagenes().put(imgAVISO);
    app.getLibImagenes().put(imgACEPTAR);
    app.getLibImagenes().put(imgCANCELAR);
    app.getLibImagenes().CargaImagenes();

    // selecciona el tipo de ventana
    try {
      switch (type) {
        case msgERROR:
          title = "Error";
          btn1.setLabel("Aceptar");
          btn1.setImage(app.getLibImagenes().get(imgACEPTAR));
          btn2.setVisible(false);
          filename = imgERROR;
          break;
        case msgAVISO:
          title = "Aviso";
          btn1.setLabel("Aceptar");
          btn1.setImage(app.getLibImagenes().get(imgACEPTAR));
          btn2.setVisible(false);
          filename = imgAVISO;
          break;
        case msgPREGUNTA:
          title = "Pregunta";
          btn1.setLabel("S�");
          btn1.setImage(app.getLibImagenes().get(imgACEPTAR));
          btn2.setLabel("No");
          btn2.setImage(app.getLibImagenes().get(imgCANCELAR));
          filename = imgPREGUNTA;
          break;
        case msgADVERTENCIA:
          title = "Advertencia";
          btn1.setLabel("Aceptar");
          btn1.setImage(app.getLibImagenes().get(imgACEPTAR));
          btn2.setLabel("Cancelar");
          btn2.setImage(app.getLibImagenes().get(imgCANCELAR));
          filename = imgADVERTENCIA;
          break;
      }
    }
    catch (Exception e) {
      dispose();
    }
    setTitle(title);

    Graphics g = app.getGraphics();
    g.setFont(new Font("Dialog", 1, 12));

    if (message == null) {
      message = " ";

    }
    try {
      msgWIDTH = g.getFontMetrics().stringWidth(message);
      lbl.setText(message);
    }
    catch (Exception e) {
    }

    // tama�o del di�logo
    this.setSize(msgWIDTH + 70, msgHEIGHT);
    setResizable(false);

    // imagen
    image = new CImage(app, app.getLibImagenes().get(filename), 40, 40);
    this.add(image, new XYConstraints(5, 5, 45, 45));

    // etiqueta
    this.add(lbl, new XYConstraints(50, 20, -1, -1));

    // botones
    pnl.setLayout(new FlowLayout());
    pnl.add(btn1);
    pnl.add(btn2);
    this.add(pnl, new XYConstraints(0, 50, msgWIDTH, -1));

    /*
         // instala los escuchadores
        this.addWindowListener(new java.awt.event.WindowAdapter() {
          public void windowClosed(WindowEvent e) {
            mi_windowClosed(e);
          }
          public void windowClosing(WindowEvent e) {
            mi_windowClosing(e);
          }
        });
        btn1.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
            btn_actionPerformed(e, true);
          }
        });
        btn2.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
            btn_actionPerformed(e, false);
          }
        });
     */

    // instala los escuchadores
    this.addWindowListener(new MiWindowAdapter(this));

    btn1.setActionCommand("btn1");
    btn1.addActionListener(new MiActionAdapter(this));

    btn2.setActionCommand("btn2");
    btn2.addActionListener(new MiActionAdapter(this));

  } //end CMessage

  public void setWidth(int width) {
    setSize(width, msgHEIGHT);
  } //end CMessage

  public void show() {
    if (app != null) {
      app.getParentFrame().setEnabled(false);
      Dimension dimCapp = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension dimCmsg = getSize();
      setLocation( (dimCapp.width - dimCmsg.width) / 2,
                  (dimCapp.height - dimCmsg.height) / 2);
    } //endif
    super.show();
  } //end show

  void mi_windowClosed(WindowEvent e) {
    if (app != null) {
      app.getParentFrame().setEnabled(true); // Siempre que se cierra
    } //endif
  } //end mi_windowClosed

  void mi_windowClosing(WindowEvent e) {
    dispose(); // Se cierra con la cruz
  } //end mi_windowClosing

  void btn_actionPerformed(ActionEvent e, boolean r) {
    response = r;
    dispose();
  }

  public boolean getResponse() {
    return (response);
  }
}

//__________________

// Cierre de Ventana
class MiWindowAdapter
    extends WindowAdapter {
  CMessage adaptee;

  MiWindowAdapter(CMessage adaptee) {
    this.adaptee = adaptee;
  }

  public void windowClosed(WindowEvent e) {
    adaptee.mi_windowClosed(e);
  }

  public void windowClosing(WindowEvent e) {
    adaptee.mi_windowClosing(e);
  }

} // endclass  MiWindowAdapter

// Botones
class MiActionAdapter
    implements ActionListener {
  CMessage adaptee;

  MiActionAdapter(CMessage adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("btn1")) {
      adaptee.btn_actionPerformed(e, true);
    }
    else if (e.getActionCommand().equals("btn2")) {
      adaptee.btn_actionPerformed(e, false);
    }
  }
} // endclass  MiActionAdapter
