package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Configuración de un elemento de la botonera.
 *
 */
public class CBoton {
  private String sImage = null;
  private String sToolTip = null;
  private String sName = null;
  private boolean bDblClick = false;
  private boolean bSelRequired = false;

  public CBoton(String name,
                String image,
                String tooltip,
                boolean dblclick,
                boolean selrequired) {
    sImage = image;
    sToolTip = tooltip;
    sName = name;
    bDblClick = dblclick;
    bSelRequired = selrequired;
  }

  public boolean getDblClick() {
    return bDblClick;
  }

  public boolean getRequired() {
    return bSelRequired;
  }

  public String getImage() {
    return sImage;
  }

  public String getToolTip() {
    return sToolTip;
  }

  public String getName() {
    return sName;
  }

}
