package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Configuración de una columna de una tabla.
 *
 */
public class CColumna {
  private String sLabel = null;
  private String sSize = null;
  private String sField = null;

  public CColumna(String label,
                  String size,
                  String field) {
    sLabel = label;
    sSize = size;
    sField = field;
  }

  public String getLabel() {
    return sLabel;
  }

  public String getSize() {
    return sSize;
  }

  public String getField() {
    return sField;
  }

}
