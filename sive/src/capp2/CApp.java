package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Applet con las siguientes caracter�sticas:
 *
 *	Incorpora la barra de t�tulo.
 *	Incorpora un stub de comunicaciones con los servlets.
 *	Incorpora una librer�a de im�genes.
 *
 */
import java.applet.Applet;
import java.util.Enumeration;
import java.util.StringTokenizer;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;

import sapp2.Data;
import sapp2.StubSrvBD;

public class CApp
    extends Applet {
  private BorderLayout borderLayout = new BorderLayout();
  private Frame elPadre = null;
  private CTabPanel tabPanel = new CTabPanel();
  private CLibImagenes libImgs = null;
  private CTitulo pnlTitulo = null;
  private StubSrvBD stubSrv = null;
  private boolean debugMode = true;
  private Data usuIDs = null;
  protected int iIdioma;

  public void init() {
    StringTokenizer token = null;

    elPadre = getParentFrame();
    try {
      jbInit();
      // modificacion jlt para recuperar el url_servlet
      String ur = new String();
      String ur2 = new String();
      String ur3 = new String();
      String ur4 = new String();
      int ind = 0;
      int indb = 0;
      ur = (String)this.getDocumentBase().toString();
      ind = ur.lastIndexOf("/");
      ur2 = (String) ur.substring(0, ind + 1);
      ur3 = (String) ur.substring(0, ind);
      // System_out.println("urcapp" + ur2);
      indb = ur3.lastIndexOf("/");
      ur4 = (String) ur3.substring(0, indb + 1);
      // System_out.println("urcapp" + ur4);

      if (this.getParametro("URL_SERVLET").equals("") ||
          this.getParametro("URL_SERVLET").equals("/")
          || this.getParametro("URL_SERVLET").equals(null)) {

        stubSrv = new StubSrvBD(ur4);
      }
      else {
        stubSrv = new StubSrvBD(this.getParametro("URL_SERVLET"));
      }

      ////////////
      //stubSrv = new StubSrvBD(this.getParametro("URL_SERVLET"));
      libImgs = new CLibImagenes( (Applet)this);
      // idioma
      iIdioma = 0;
      try {
        iIdioma = Integer.parseInt(this.getParametro("idioma"));
      }
      catch (Exception exp) {
      }

      // lee los ids autorizados para el usuario
      usuIDs = new Data();
      token = new StringTokenizer(getParametro("IT_USU") + " ", ",");
      while (token.hasMoreElements()) {
        usuIDs.put(token.nextElement().toString().trim(),
                   token.nextElement().toString().trim());
      }

      // determina si est� en modo debug
      if (this.getParametro("DEBUGMODE").equals("OFF")) {
        debugMode = false;

      }
    }
    catch (Exception e) {
      trazaLog(e);
    }
  } //end init

  // libera todos los componentes
  public void destroy() {
    Component[] comps = this.getComponents();
    for (int i = 0; i < this.getComponentCount(); i++) {
      comps[i] = null;
    }
  }

  // devuelve el frame
  public Frame getFrame() {
    return elPadre;
  }

  //Component initialization
  private void jbInit() throws Exception {
    this.setLayout(borderLayout);
    add("Center", tabPanel);
  } //end jbInit

  final public synchronized void VerPanel(String nombre, CPanel p) {
    VerPanel(nombre, p, false);
  } //end VerPanel

  final public synchronized void VerPanel(String nombre, CPanel p,
                                          boolean modoTab) {
    p.setApp(this);
    if (!modoTab) { // Solo el panel
      // Guardar panel anterio
      if (tabPanel.getNumSolapas() == 1) {
        // Hay un panel solo y se guarda el actual
        CPanel pant = PanelActual();
        if (pant != null) {
          p.panelAnt = PanelActual();
          p.nombrePanelAnt = NombrePanelActual();
        } //endif
      }
      else {
        // Antes habia solapas y se pone anterior a null
        p.panelAnt = null;
        p.nombrePanelAnt = null;
      } //endif
      tabPanel.BorrarSolapa();
      tabPanel.InsertarPanel(nombre, p);
      tabPanel.validate();
    }
    else { // ver con solapas
      tabPanel.InsertarPanel(nombre, p);
      tabPanel.validate();
    } //endif
  } //end VerPanel

  final public void VerPanel(String nombre) {
    tabPanel.VerPanel(nombre);
  } //end BorrarPanel

  final public void BorrarPanel(String nombre) {
    tabPanel.BorrarSolapa(nombre);
  } //end BorrarPanel

  final public String NombrePanelActual() {
    return tabPanel.getSolapaActual();
  } //end NombrePanelActual

  final public CPanel PanelActual() {
    return tabPanel.getPanelActual();
  } //end NombrePanelActual

  final public void VerPanelAnterior() {
    tabPanel.VerPanelAnterior();
  } //end VerPanelAnterior

  final public void addPanelSuperior(Container panel) {
    add("North", panel);
  } //end addPanelSuperior

  final public void addPanelInferior(Container panel) {
    add("South", panel);
  } //end addPanelInferior

  final public void addPanelLateral(Container panel) {
    add("West", panel);
  } //end addPanelLateral

  public Frame getParentFrame() {
    if (elPadre == null) {
      Component p = this;
      while ( ( (p = p.getParent()) != null) && ! (p instanceof Frame)) {
        ;
      }
      elPadre = (Frame) p;
    } //endif

    return elPadre;
  } //end getParentFrame

  /**
   * Devuelve una referencia al stub
   *
   * @return stub
   */
  public StubSrvBD getStub() {
    return stubSrv;
  }

  /**
   * Devuelve una referencia a la librer�a de im�genes
   *
   * @return librer�a
   */
  public CLibImagenes getLibImagenes() {
    return libImgs;
  }

  // recupera el idioma
  public int getIdioma() {
    return iIdioma;
  }

  /**
   * Presenta la barra con el t�tulo. El bot�n de cierre carga la
   * la p�gina default.html, y el de ayuda carga la p�gina indicada
   * en el par�metro AYUDA.
   *
   * @param title T�tulo
   */
  public void setTitulo(String title) {
    if (pnlTitulo == null) {
      pnlTitulo = new CTitulo(this);
      add("North", pnlTitulo);
    }
    pnlTitulo.setTitulo(title);
  }

  /**
   * Muestra un ventana de error
   *
   * @param s Mensaje para mostrar
   */
  public void showError(String s) {
    CMessage box = new CMessage(this, CMessage.msgERROR, s);
    box.show();
    box = null;
  }

  /**
   * Muestra una ventana de aviso
   *
   * @param s Mensaje para trazar
   */
  public void showAdvise(String s) {
    CMessage box = new CMessage(this, CMessage.msgAVISO, s);
    box.show();
    box = null;
  }

  /**
   * Muestra una ventana de confirmaci�n
   *
   * @param s Mensaje para trazar
   * @return respuesta
   */
  public boolean showConfirm(String s) {
    CMessage box = new CMessage(this, CMessage.msgADVERTENCIA, s);
    box.show();
    boolean b = box.getResponse();
    box = null;
    return b;
  }

  /**
   * Muestra una ventana de pregunta
   *
   * @param s Mensaje para trazar
   * @return respuesta
   */
  public boolean showQuestion(String s) {
    CMessage box = new CMessage(this, CMessage.msgPREGUNTA, s);
    box.show();
    boolean b = box.getResponse();
    box = null;
    return b;
  }

  /**
   * Traza en el log el mensaje leido
   *
   * @param msg Mensaje para trazar
   */
  public void trazaLog(Object msg) {
//    if (debugMode)
//      System_out.println( msg.toString() );
  }

  /**
   * Lee el par�metro requerido. Si no existe devuelve un string vacio
   *
   * @param nombre nombre del par�metro
   * @return par�metros
   */
  public String getParametro(String name) {
    String param = getParameter(name);
    if (param == null) {
      param = "";
    }
    return param;
  }

  /**
   * Devuelve verdadero/falso dependiendo de si el c�digo de la
   * acci�n est� permitido (existe y no est� cerrada) o no lo est�
   *
   * Nota: si no se indica una clave, no se tiene en cuenta USU
   * @param key id de la acci�n
   * @return indicador
   */
  public boolean isAllowed(String key) {
    boolean allowed = false;
    String name = null;

    if (key.length() > 0) {
      for (Enumeration enum = usuIDs.keys(); enum.hasMoreElements(); ) {
        name = (String) enum.nextElement();
        if (key.equals(name)) {
          if (usuIDs.getString(name).length() == 0) {
            allowed = true;
          }
          break;
        }
      }
    }
    else {
      allowed = true;
    }

    return allowed;
  }

  /**
   * Indica si la clave existe en las claves autorizadas del usuario
   *
   * @param key id de la acci�n
   * @return indicador
   */
  public boolean isAccesible(String key) {
    boolean accesible = false;
    String name = null;

    for (Enumeration enum = usuIDs.keys(); enum.hasMoreElements(); ) {
      name = (String) enum.nextElement();
      if (key.equals(name)) {
        accesible = true;
        break;
      }
    }

    return accesible;
  }

  /**
   * Graba las autorizaciones
   *
   * @param aut Autorizaciones
   */
  public void setAutorizaciones(Data dt) {
    usuIDs = dt;
  }

  /**
   * Transforma la estructura de autorizaciones USU de la forma
   *  c�digp1,estado1,c�digo2,estado2 ...
   *
   * @return valor
   */
  public String parseAutorizaciones() {
    String name = null;
    String parse = "";

    for (Enumeration enum = usuIDs.keys(); enum.hasMoreElements(); ) {

      name = (String) enum.nextElement();

      try {
        Integer.parseInt(name);

        if (parse.length() > 0) {
          parse += ",";

        }
        parse += name + ",";

        if (usuIDs.getString(name).length() > 0) {

          //parse += name + usuIDs.getString(name);
          parse += usuIDs.getString(name);
        }
        else {

          //parse += name + " ";
          parse += " ";

          //CRISTINA: Atencion al "arreglo"
          //He detectado que cuando se prueba en los navegadores
          //la ultima accion que va en el parametro IT_USU no la tiene
          //en cuenta (concretamente me pasaba cuando la ultima accion
          //era la 990 y no me habilitaba el boton de alta).
          //Una solucion provisional es repetir la ultima accion. Cuando
          //haya mas tiempo mirare mejor que es lo que pasa.
        }
        if (!enum.hasMoreElements()) { //es la ultima
          if (parse.length() > 0) {
            parse += "," + name + ",";

          }
          if (usuIDs.getString(name).length() > 0) {

            //parse += name + usuIDs.getString(name);
            parse += usuIDs.getString(name);
          }
          else {

            //parse += name + " ";
            parse += " ";

          }
        }

      }
      catch (Exception e) {}
    }

    return parse;
  }

} //endclass CApp
