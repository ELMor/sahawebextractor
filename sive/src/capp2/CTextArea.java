
package capp2;

import java.awt.TextArea;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @autor LSR
 * @version 1.0
 *
 * Area de texto que admite una longitud m�xima de datos.
 * La longitud m�xima se informa en el constructor.
 *
 */
public class CTextArea
    extends TextArea
    implements KeyListener {
  private int iMaxLng;
  private int iLong;
  private boolean bControl;

  public CTextArea(int iS) {
    iMaxLng = iS;
    iLong = 0;

    //{{REGISTER_LISTENERS
    this.addKeyListener(this);
    //}}
  }

  public void keyPressed(KeyEvent event) {
    char c = event.getKeyChar();

    if ( (int) c == 8 || (int) c == 9 || (int) c == 127) {
      bControl = true;
    }
    else {
      bControl = false;
    }
  }

  public void keyTyped(KeyEvent e) {
    iLong = getText().length();

    if (bControl) { // tabulador
      return;
    }
    if (iLong >= iMaxLng) {
      e.consume(); // se supera la longitud m�xima
    }
  }

  public void keyReleased(KeyEvent evt) {
  }

}