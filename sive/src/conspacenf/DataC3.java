package conspacenf;

import java.util.Date;
import java.util.Vector;

import capp.CLista;
import enfermo.Fechas;
import sapp.DBServlet;

/** relaci�n de pacientes de una enfermedad
 *
 *  filtros:  desde a�o y semana
 *            hasta a�o y semana
 *            enfermedad
 * (opcional) equipo notificador
 *            centro notificador
 *            municipio
 *            distrito
 *            �rea  // por defecto Comunidad de Madrid
 *            ZBS
 *
 * resultados:enfermo (nombre, apellidos)
 *            sexo
 *            edad
 *            Semana epidemiol�gica
 *            Notificacion
 *            Mun
 *            clasificacion diagn�stico
 *            Fecha de inicio S�ntoma ?
 */
public class DataC3
    extends Data {

  /** campos que va a tener */
  public static final int COUNT = 0;
  public static final int DS_NOMBRE = 1;
  public static final int DS_APE1 = 2;
  public static final int DS_APE2 = 3;
  public static final int CD_SEXO = 4;
  public static final int FC_NAC = 5;
  public static final int CD_SEMEPI = 6;
  public static final int CD_PROV = 7;
  public static final int CD_MUN = 8;
  public static final int CD_CLASIFDIAG = 9;
  public static final int NM_EDO = 10;
  public static final int SIGLAS = 11;
  public static final int CD_ANOEPI = 12;
  public static final int FC_INISNT = 13;
  public static final int CD_E_NOTIF = 14;
  public static final int FECHA = 15;
  public static final int DS_MUN = 16;
  public static final int DS_E_NOTIF = 17;
  public static final int DS_CLASIFDIAG = 18;
  public static final int PERMISO = 19;
  // ARG: Para calcular la edad
  public static final int FC_FECNOTIF = 20;

  /** parametros que tiene */
  // n�mero de par�metros de esta sentencia
  public static final int P_NUM_C3 = 5;

  // posici�n de los diferentes par�metrso en la select
  public static final int P_PAGINA = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_CD_ENFCIE = 1;
  public static final int P_DESDE_CD_ANOEPI1 = 2;
  public static final int P_DESDE_CD_SEMEPI2 = 3;
  public static final int P_DESDE_CD_ANOEPI3 = 4;
  public static final int P_HASTA_CD_ANOEPI4 = 5;
  public static final int P_HASTA_CD_ANOEPI5 = 6;
  public static final int P_HASTA_CD_SEMEPI6 = 7;
  public static final int P_CD_E_NOTIF = 8;
  public static final int P_CD_C_NOTIF = 9;
  public static final int P_CD_PROV = 10;
  public static final int P_CD_MUN = 11;
  public static final int P_CD_NIVEL_1 = 12;
  public static final int P_CD_NIVEL_2 = 13;
  public static final int P_CD_ZBS = 14;
  public static final int P_PERMISO = 15;

  public static final int P_NM_EDO = 1;

  /** modos que tiene */
  public static final int modoJOIN_ENFERMO_EDOIND = 1;
  public static final int modoNOTIFEDOI = 2;
  public static final int modoE_NOTIF = 3;
  public static final int modoDATOS_EDOIND = 4;
  public static final int modoCOUNT = 5;
  public static final int modoCOUNTJOIN_ENFERMO_EDOIND = 6;

  /** sewntencias SQL con la que se obtienen los datos */
  //AIC
  /*  public static final String sqlJOIN_ENFERMO_EDOIND = "SELECT "+
       " a.DS_NOMBRE, a.DS_APE1, a.DS_APE2, a.CD_SEXO, a.FC_NAC, b.CD_SEMEPI,"+
               " b.CD_PROV, b.CD_MUN, b.CD_CLASIFDIAG, b.NM_EDO, a.SIGLAS, b.CD_ANOEPI, b.FC_INISNT "+
               " FROM SIVE_ENFERMO a, SIVE_EDOIND b " +
               " WHERE a.CD_ENFERMO = b.CD_ENFERMO " +
               " AND b.CD_ENFCIE = ? AND ";*/

  /*  public static final String sqlJOIN_ENFERMO_EDOIND = "SELECT "+
       " a.DS_NOMBRE, a.DS_APE1, a.DS_APE2, c.DS_SEXO, a.FC_NAC, b.CD_SEMEPI,"+
                 " b.CD_PROV, b.CD_MUN, b.CD_CLASIFDIAG, b.NM_EDO, a.SIGLAS, b.CD_ANOEPI, b.FC_INISNT "+
                 " FROM SIVE_ENFERMO a, SIVE_EDOIND b, " +
                 " SIVE_SEXO c " +
                 " WHERE a.CD_ENFERMO = b.CD_ENFERMO " +
                 " AND c.CD_SEXO (+)= a.CD_SEXO " +
                 " AND b.CD_ENFCIE = ? AND ";
   */
  // modificacion para ordenar por ape1 ape2, nombre
  public static final String sqlJOIN_ENFERMO_EDOIND = "SELECT " +
      " a.DS_APE1, a.DS_APE2, ', ' || a.DS_NOMBRE, c.DS_SEXO, a.FC_NAC, b.CD_SEMEPI," +
      " b.CD_PROV, b.CD_MUN, b.CD_CLASIFDIAG, b.NM_EDO, a.SIGLAS, b.CD_ANOEPI, b.FC_INISNT " +
      " FROM SIVE_ENFERMO a, SIVE_EDOIND b, " +
      " SIVE_SEXO c " +
      " WHERE a.CD_ENFERMO = b.CD_ENFERMO " +
      " AND c.CD_SEXO (+)= a.CD_SEXO " +
      " AND b.CD_ENFCIE = ? AND ";

  public static final String sqlNOTIFEDOI = "SELECT " +
      " NM_EDO FROM SIVE_NOTIF_EDOI WHERE IT_PRIMERO = 'S' AND CD_FUENTE = 'E' AND CD_E_NOTIF LIKE "; //?";

  public static final String sqlE_NOTIF = "SELECT " +
      " CD_E_NOTIF FROM SIVE_E_NOTIF WHERE CD_CENTRO LIKE "; //?";

  //public static final String sql_ORDER = " ORDER BY DS_APE1, DS_NOMBRE, CD_ANOEPI, CD_SEMEPI";
  public static final String sql_ORDER =
      " ORDER BY DS_APE1, DS_APE2, DS_NOMBRE, CD_ANOEPI, CD_SEMEPI";

  public static final String sql_COUNT = "SELECT COUNT(*)" +
      " FROM SIVE_ENFERMO a, SIVE_EDOIND b " +
      " WHERE a.CD_ENFERMO = b.CD_ENFERMO " +
      " AND b.CD_ENFCIE = ? AND ";

  public static final String sqlDATOS_EDOIND = "SELECT " +
      " CD_SEMEPI, CD_MUN, CD_CLASIFDIAG  FROM SIVE_EDOIND WHERE NM_EDO =?";

  public static String filtroNoIguales =
      " ( ( b.CD_ANOEPI = ? AND b.CD_SEMEPI >= ? ) OR " +
      " ( b.CD_ANOEPI > ? AND b.CD_ANOEPI < ? ) OR " +
      " ( b.CD_ANOEPI = ? AND b.CD_SEMEPI <= ? ) ) ";

  public static String filtroIguales =
      " ( b.CD_ANOEPI = ? AND b.CD_SEMEPI >= ? AND " +
      "  b.CD_ANOEPI = ? AND b.CD_ANOEPI = ?  AND " +
      "  b.CD_ANOEPI = ? AND b.CD_SEMEPI <= ?  ) ";

  public DataC3(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[20];
    // ARG: En la ultima posicion esta la fecha de notificacion
    arrayDatos = new Object[21];
  }

  public Object getNewData() {
    return new DataC3(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "NM_EDO";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  /** esta funci�n devuelve la sentencia a ejecutar */
  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    Data param_item = (Data) ini_param.firstElement();
    String hasta_ano = (String) param_item.get(DataC3.P_HASTA_CD_ANOEPI5);
    String desde_ano = (String) param_item.get(DataC3.P_DESDE_CD_ANOEPI1);

    switch (opmode) {
      case modoJOIN_ENFERMO_EDOIND:
        if (hasta_ano.equals(desde_ano)) {
          strSQL.append(sqlJOIN_ENFERMO_EDOIND + filtroIguales);
        }
        else {
          strSQL.append(sqlJOIN_ENFERMO_EDOIND + filtroNoIguales);
        }
        break;
      case modoNOTIFEDOI:
        strSQL.append(sqlNOTIFEDOI);
        break;
      case modoE_NOTIF:
        strSQL.append(sqlE_NOTIF);
        break;
      case modoDATOS_EDOIND:
        strSQL.append(sqlDATOS_EDOIND);
        break;
      case modoCOUNT:
        if (hasta_ano.equals(desde_ano)) {
          strSQL.append(sql_COUNT + filtroIguales);
        }
        else {
          strSQL.append(sql_COUNT + filtroNoIguales);
        }
        break;

    }

    // indicamos le n�merod e registros que queremos
    return DBServlet.maxSIZE; // por defecto
  }

  public String getENFERMO() {
    StringBuffer result = new StringBuffer();

    if (arrayDatos.length > CD_ANOEPI) {
      if (arrayDatos[DataC3.PERMISO] != null) { // tiene permiso para ver los nombres
        if (arrayDatos[DS_NOMBRE] != null) {
          result.append(arrayDatos[DS_NOMBRE]);
        }
        result.append(" ");
        if (arrayDatos[DS_APE1] != null) {
          result.append(arrayDatos[DS_APE1]);
        }
        result.append(" ");
        if (arrayDatos[DS_APE2] != null) {
          result.append(arrayDatos[DS_APE2]);
        }
      }
      else {
        if (arrayDatos[SIGLAS] != null) {
          result.append(arrayDatos[SIGLAS]);
        }
        result.append(" ");
      }
    }
    else {
      result.append(" ");
    }

    return result.toString();
  }

  public String getSEXO() {
    if (arrayDatos.length > CD_SEXO) {
      return (String) arrayDatos[CD_SEXO];
    }
    else {
      return " ";
    }
  }

  // recuperamos el c�digo edo como un String
  public String getEDO() {
    if (arrayDatos.length > NM_EDO) {
      //return (String) arrayDatos[NM_EDO];
      return ( (java.math.BigDecimal) arrayDatos[NM_EDO]).toString();
    }
    else {
      return " ";

    }

  }

  public String getEDAD() {
    int i;

    if (arrayDatos.length > FC_NAC) {
      // i = Fechas.edadAnios((Date) arrayDatos[FC_NAC]);
      // ARG: Se calcula la edad con respecto a la fecha de notificacion
      i = Fechas.edadAniosConRef( (Date) arrayDatos[FC_NAC],
                                 (Date) arrayDatos[FC_FECNOTIF]);

      if (i > 0) {
        return Integer.toString(i);
      }
      else {

        // return Integer.toString(Fechas.edadMeses((Date) arrayDatos[FC_NAC])) + "(m)";
        // ARG: Se calcula la edad con respecto a la fecha de notificacion
        return Integer.toString(Fechas.edadMesesConRef( (Date) arrayDatos[
            FC_NAC], (Date) arrayDatos[FC_FECNOTIF])) + "(m)";
      }
    }
    else {
      return " ";
    }
  }

  public String getSEM_EPI() {
    if (arrayDatos.length > CD_SEMEPI) {
      return (String) arrayDatos[CD_SEMEPI];
    }
    else {
      return " ";
    }
  }

  public String getANO_EPI() {
    if (arrayDatos.length > CD_ANOEPI) {
      return ( (String) arrayDatos[CD_ANOEPI]) + "-" +
          ( (String) arrayDatos[CD_SEMEPI]);
    }
    else {
      return " ";
    }
  }

  public String getNOTIFICADOR() {
    if (arrayDatos.length > CD_E_NOTIF) {
      return (String) arrayDatos[CD_E_NOTIF];
    }
    else {
      return " ";
    }
  }

  public String getCD_MUNICIPIO() {
    if (arrayDatos.length > DS_MUN) {
      return (String) arrayDatos[DS_MUN];
    }
    else {
      return " ";
    }
  }

  public String getCLAS() {
    if (arrayDatos.length > DS_CLASIFDIAG) {
      return (String) arrayDatos[DS_CLASIFDIAG];
    }
    else {
      return " ";
    }
  }

  public String getFECHA() {
    if (arrayDatos.length >= FC_INISNT) {
      //# // System_out.println( (String) arrayDatos[FC_INISNT] );
      return (String) arrayDatos[FC_INISNT];
    }
    else {
      return " ";
    }
  }

  /** indica al report como leer los datos */
  public Vector getFieldVector() {
    Vector retval = new Vector();

    retval.addElement("ENFERMO  = getENFERMO");
    retval.addElement("SEXO  = getSEXO");
    retval.addElement("EDAD  = getEDAD");
    retval.addElement("SEM_EPI  = getANO_EPI");
    retval.addElement("ANO_EPI  = getANO_EPI");
    retval.addElement("NOTIFICADOR  = getNOTIFICADOR");
    retval.addElement("CD_MUNICIPIO  = getCD_MUNICIPIO");
    retval.addElement("CLAS  = getCLAS");
    retval.addElement("FECHA  = getFECHA");

    return retval;
  }

  // espec�fico para la consulta conspacenf
  public Vector getFieldVectorCP() {
    Vector retval = new Vector();

    retval.addElement("ENFERMO  = getENFERMO");
    // modificacion para recuperar el codigo edo
    retval.addElement("EDO  = getEDO");
    retval.addElement("SEXO  = getSEXO");
    retval.addElement("EDAD  = getEDAD");
    retval.addElement("SEM_EPI  = getANO_EPI");
    retval.addElement("ANO_EPI  = getANO_EPI");
    retval.addElement("NOTIFICADOR  = getNOTIFICADOR");
    retval.addElement("CD_MUNICIPIO  = getCD_MUNICIPIO");
    retval.addElement("CLAS  = getCLAS");
    retval.addElement("FECHA  = getFECHA");

    return retval;
  }

} //____________________________________ END_CLASS