package conspacenf;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

//import java.text.SimpleDateFormat;

public class PanelInforme
    extends EPanel {

  PnlParamC3 pan;
  ResourceBundle res;

  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvC3";

  final int MODO_SERVLET = DataC3.modoJOIN_ENFERMO_EDOIND;
  final int MODO_SERVLETCOUNT = DataC3.modoCOUNTJOIN_ENFERMO_EDOIND;

  /** guarda el n�mero de registros */
  protected String num_reg = "0";

  // estructuras de datos
  protected Vector vCasos = null;

  /** esta es la lista que recoge los datos */
  protected CLista lista;

  /** esta es la lista que contiene los par�metros */
  protected CLista param = null;

  /** n�mero de p�gina por el que se va */
  int num_pag = 0;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // vector y nombre de la tabla y fichero
  String m_file = null;
  String m_table = null;
  Vector m_column = null;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;

  public PanelInforme(StubSrvBD s, CApp a, String a_file, String a_nombreTabla,
                      Vector a_column, PnlParamC3 miPanel) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("conspacenf.Res" + app.getIdioma());
    pan = miPanel;
    m_file = a_file;
    m_table = a_nombreTabla;
    m_column = a_column;
    vCasos = new Vector();

    try {
      stub = s;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(getCApp().getCodeBase().toString() + m_file);

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    ( (CDialog)this).show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;

    // incrementamos el n�mero de p�gina
    num_pag++;

    //// System_out.println("Mas Datos" + lista);
    if (lista != null && lista.getState() == CLista.listaINCOMPLETA) {
      //// System_out.println ( "Lista Estado " +lista.getState());
      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        // obtiene los datos del servidor
        param.setIdioma(app.getIdioma());
        ( (DataC3) param.firstElement()).put(DataC3.P_PAGINA,
                                             Integer.toString(num_pag));

        // ARG: Se introduce el login
        param.setLogin(app.getLogin());
        // ARG: Se introduce la lortad
        param.setLortad(app.getParameter("LORTAD"));

        stub.setUrl(new URL(getCApp().getURL() + strSERVLET));
        lista = (CLista) stub.doPost(MODO_SERVLET, param);

        // ponemos las etiquetas del report
        fijarEtiqueas();

        if (lista != null) {
          for (int j = 0; j < lista.size(); j++) {
            vCasos.addElement(lista.elementAt(j));
          }

          // control de registros
          this.setTotalRegistros(num_reg);
          this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

          // repintado
          //// System_out.println("Se refresca ");
          erwClient.refreshReport(true);
        }
        else {
          this.setTotalRegistros("0");
          this.setRegistrosMostrados("0");
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(getCApp(), CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  protected void fijarEtiqueas() {
    String dato = null;
    StringBuffer linea = null;
    int iEtiqueta = 0;
    CMessage msgBox;

    String sEtiqueta[] = {
        "CRITERIO1",
        "CRITERIO2",
        "CRITERIO3",
        "CRITERIO4",
        "CRITERIO5"};

    // cambiamos las etiquetas
    DataC3 datac3 = (DataC3) param.firstElement();
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000",
                   getCApp().getCodeBase().toString() + "images/ccaa.gif");
    tm.SetImageURL("IMA001",
                   getCApp().getCodeBase().toString() + "images/ccaa.gif");
    /*
        linea = new StringBuffer();
        dato  = (String) datac3.get(DataC3.P_DESDE_CD_ANOEPI1);
        if (dato != null){
            linea.append(EPanel.PERIODO);
            linea.append(dato);
        }
        dato  = (String) datac3.get(DataC3.P_DESDE_CD_SEMEPI2);
        if (dato != null){
            linea.append(EPanel.SEPARADOR_ANO_SEM + dato);
        }
        //tm.SetLabel("LAB005", linea.toString());  // DESDE
        dato  = (String) datac3.get(DataC3.P_HASTA_CD_ANOEPI4);
        if (dato != null){
            linea.append(EPanel.HASTA);
            linea.append(dato);
        }
        dato  = (String) datac3.get(DataC3.P_HASTA_CD_SEMEPI6);
        if (dato != null){
            linea.append(EPanel.SEPARADOR_ANO_SEM + dato);
        }
        //tm.SetLabel("LAB007", linea.toString()); // HASTA
        dato  = (String) datac3.get(DataC3.P_CD_ENFCIE);
        if (dato != null){
            linea.append(EPanel.ENFERMEDAD);
            linea.append(dato);
        }
        dato  = (String) datac3.get(DataC3.P_CD_C_NOTIF);
        if (dato != null && !dato.equals("'%'") ){
            linea.append(EPanel.CENTRO);
            linea.append(dato.substring(1, dato.length()-2));
        }
        dato  = (String) datac3.get(DataC3.P_CD_E_NOTIF);
        if (dato != null && !dato.equals("'%'") ){
            linea.append(EPanel.EQUIPO);
            linea.append(dato.substring(1, dato.length()-2));
        }
        dato  = (String) datac3.get(DataC3.P_CD_MUN);
        if (dato != null){
            linea.append(EPanel.MUNICIPIO);
            linea.append(dato);
        }
        dato  = (String) datac3.get(DataC3.P_CD_PROV);
        if (dato != null){
            linea.append(EPanel.PROVINCIA);
            linea.append(dato);
        }
        dato  = (String) datac3.get(DataC3.P_CD_NIVEL_1);
        if (dato != null){
            linea.append(EPanel.AREA);
            linea.append(dato);
        }
        dato  = (String) datac3.get(DataC3.P_CD_NIVEL_2);
        if (dato != null){
            linea.append(EPanel.DISTRITO);
            linea.append(dato);
        }
        dato  = (String) datac3.get(DataC3.P_CD_ZBS);
        if (dato != null){
            linea.append(EPanel.ZBS);
            linea.append(dato);
        }
        //tm.SetLabel("LAB010", linea.toString()); // ZBS
        tm.SetLabel("CRITERIO", linea.toString());
     */

    try {

      // carga los citerios
      dato = (String) datac3.get(DataC3.P_DESDE_CD_ANOEPI1);
      if (dato != null) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg3.Text") + " " +
                    (String) datac3.get(DataC3.P_DESDE_CD_ANOEPI1)
                    + " " + res.getString("msg4.Text") + " " +
                    (String) datac3.get(DataC3.P_DESDE_CD_SEMEPI2) + ", "
                    + res.getString("msg5.Text") + " " +
                    (String) datac3.get(DataC3.P_HASTA_CD_ANOEPI4) + " "
                    + res.getString("msg6.Text") +
                    (String) datac3.get(DataC3.P_HASTA_CD_SEMEPI6));
        iEtiqueta++;
      }

      dato = (String) datac3.get(DataC3.P_CD_ENFCIE);

      if (dato != null) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg7.Text") + " " + dato + " " +
                    pan.txtEnfermedadL.getText());
        iEtiqueta++;
      }

      dato = (String) datac3.get(DataC3.P_CD_PROV);
      if (dato != null) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg8.Text") + " " + dato + " " +
                    pan.txtDesPro.getText());
        dato = (String) datac3.get(DataC3.P_CD_MUN);
        if (dato != null) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + ", " +
                      res.getString("msg9.Text") + dato + " " +
                      pan.txtDesMun.getText());
        }
        iEtiqueta++;
      }

      //nivel1
      dato = (String) datac3.get(DataC3.P_CD_NIVEL_1);
      if (dato != null) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    app.getNivel1() + ": " + dato + " " + pan.txtDesAre.getText());
        dato = (String) datac3.get(DataC3.P_CD_NIVEL_2);
        //nivel2
        if (dato != null) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + ", " + app.getNivel2() +
                      ": " + dato + " " + pan.txtDesDis.getText());
          dato = (String) datac3.get(DataC3.P_CD_ZBS);
          //ZBS
          if (dato != null) {
            tm.SetLabel(sEtiqueta[iEtiqueta],
                        tm.GetLabel(sEtiqueta[iEtiqueta]) + ", " +
                        res.getString("msg10.Text") + ": " + dato + " " +
                        pan.txtDesZBS.getText());
          }
        }
        iEtiqueta++;
      }

      //CNot
      dato = (String) datac3.get(DataC3.P_CD_C_NOTIF);
      if (dato != null && !dato.equals("'%'")) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg11.Text") +
                    dato.substring(1, dato.length() - 2)
                    + " " + pan.txtDesCenNot.getText());
        //Eq not
        dato = (String) datac3.get(DataC3.P_CD_E_NOTIF);
        if (dato != null && !dato.equals("'%'")) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + " " +
                      res.getString("msg12.Text") +
                      dato.substring(1, dato.length() - 2)
                      + " " + pan.txtDesEquNot.getText());
        }
        iEtiqueta++;
      }

      // oculta criterios no informados
      for (int i = iEtiqueta; i < 5; i++) {
        tm.SetLabel(sEtiqueta[i], "");
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    String dato = null;

    // lo ponemo al inico de la p�gina y  borramos los datos que hab�a
    num_pag = 0;
    vCasos.setSize(0);

    //// System_out.println("GenerarInforme " );
    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      // obtiene los datos del servidor
      DataC3 data = (DataC3) param.firstElement();
      data.put(DataC3.P_PAGINA, Integer.toString(num_pag));
      data.bInformeCompleto = conTodos;
      param.setIdioma(app.getIdioma());

      // ARG: Se introduce el login
      param.setLogin(app.getLogin());
      // ARG: Se introduce la lortad
      param.setLortad(app.getParameter("LORTAD"));

      param.trimToSize();
      stub.setUrl(new URL(getCApp().getURL() + strSERVLET));
//      lista = (CLista)stub.doPost(MODO_SERVLETCOUNT, param);

      SrvC3 srv = new SrvC3();
      srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                             "pista",
                             "loteb98");
      lista = srv.doDebug(MODO_SERVLETCOUNT, param);

      if (lista != null && lista.size() > 0) {
        vCasos = (Vector) lista;
        // ponemos las etiquetas del report
        fijarEtiqueas();

        num_reg = (String) ( (DataC3) lista.firstElement()).get(DataC3.COUNT);

        this.setTotalRegistros(num_reg);
        this.setRegistrosMostrados(Integer.toString(lista.size()));
        //// System_out.println("Lista "+ num_reg + lista.getState());

        // establece las matrices de datos
        dataHandler.RegisterTable(vCasos, m_table, m_column, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        erwClient.setPrinterResolution(false, 300.0);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;

      }
      else {
        vCasos.setSize(0);
        msgBox = new CMessage(getCApp(), CMessage.msgAVISO,
                              res.getString("msg13.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(getCApp(), CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  public void MostrarGrafica() {
    TemplateManager tm = erw.GetTemplateManager();

    if (lista != null && lista.getState() == CLista.listaLLENA) {

      // oculta una de las secciones
      if (tm.IsVisible("RP_FTR")) {
        tm.SetVisible("PG_HDR", true);
        tm.SetVisible("DTL_00", true);
        tm.SetVisible("RP_FTR", false);
      }
      else {
        tm.SetVisible("PG_HDR", false);
        tm.SetVisible("DTL_00", false);
        tm.SetVisible("RP_FTR", true);
      }

      // repinta el grafico
      erwClient.refreshReport(true);

    }
  }

  public void setListaParam(CLista a_param) {
    param = a_param;
  }

  public CLista getListaParam() {
    return param;
  }

} //___________________________________ END_CLASS
