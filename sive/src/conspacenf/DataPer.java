package conspacenf;

import capp.CLista;

public class DataPer
    extends Data {

  /** campos que va a tener */
  public static final int COUNT = 0;
  public static final int IT_FG_ENFERMO = 1;

  /** parametros que tiene */
  // n�mero de par�metros de esta sentencia
  public static final int P_PAGINA = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_CD_USUARIO = 1;

  /** modos que tiene */
  public static final int modoPERMISO = 1;

  /** sewntencias SQL con la que se obtienen los datos */
  //public static final String sqlPERMISO = "SELECT "+
  //              " IT_FG_ENFERMO FROM SIVE_USUARIO WHERE CD_USUARIO = ?";

  public static final String sqlPERMISO = "SELECT " +
      " IT_FG_ENFERMO FROM SIVE_USUARIO_PISTA WHERE CD_USUARIO = ?";

  public DataPer(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[16];
  }

  public Object getNewData() {
    return new DataPer(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "CD_USUARIO";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  /** esta funci�n devuelve la sentencia a ejecutar */
  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    strSQL.append(sqlPERMISO);
    return 1;
  }

  public boolean getPERMISO() {
    String si = new String("S");
    return (si.equals( (String) get(DataPer.IT_FG_ENFERMO)));
  }

} //____________________________________ END_CLASS