
package conspacenf;

import capp.CApp;
import capp.CListaValores;
import sapp.StubSrvBD;

/**
 *  esta clase activa el servlet, y se enlaza con la clase CListaValores
 *  responsable de mostrar los valores y que el usuario seleccione los
 *  los valores pedidos
 */
public class CListaData
    extends CListaValores {
  /** nombredel campo calve */
  int codigo = 1;
  /** nombre del campo descripci�n */
  int descripcion = 2;
  /** componente que se va a enviar */
  Data hash = null;

  public CListaData(CApp a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descripcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion,
                    int a_codigo,
                    int a_descripcion,
                    Data a_data) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    codigo = a_codigo;
    descripcion = a_descripcion;
    hash = a_data;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String filtro) {
    if (filtro != null) {
      hash.setFiltro(filtro);
    }
    return hash;
  }

  public String getCodigo(Object o) {
    return (String) ( (Data) o).get(codigo);
  }

  public String getDescripcion(Object o) {
    return (String) ( (Data) o).get(descripcion);
  }

  /**
   *  a�ade un parametro a la lista
   *  por si el servlet necesita m�s de un parametro
   */
  public void setParameter(int campo, String dato) {
    if (campo > 0 && dato != null && dato.trim().length() > 0) {
      hash.put(campo, dato);
    }
  }
} //____________________________________ END_CLASE}