package conspacenf;

//______________________________________________ IMPORT
import java.io.Serializable;

import capp.CLista;

/** esta clase contiene los datos de la pantalla de enfermo
 *  sirve para intercambiar con el servlet
 */
public abstract class Data
    implements Serializable {

  /** tipos de datos en la base de datos */
  public static final int VARCHAR = 0;
  public static final int DATE = 1;
  public static final int INT = 2;
  public static final int LONG = 3;
  public static final int FLOAT = 4;
  public static final int DOUBLE = 5;
  public boolean bInformeCompleto = false;

  /** array donde se guardan los datos */
  protected Object[] arrayDatos = null;

  /** n�mero de par�metros que tiene */
  protected int numParam = 0;

  /** es el campo para filtrar */
  protected int campo_filtro = 22222;

  /** es el dato para filtrar */
  protected String filtro = null;

  public Data(int a_campo_filtro) {
    campo_filtro = a_campo_filtro;
  }

  public void setFiltro(Object a_filtro) {
    filtro = (String) a_filtro;
  }

  public Object getFiltro() {
    return filtro;
  }

  public void setCampoFiltro(int a_filtro) {
    campo_filtro = a_filtro;
  }

  public int getCampoFiltro() {
    return campo_filtro;
  }

  /** esta funci�n devuelve el tipo de dato del servlet */
  public abstract Object getNewData();

  /** devuelve el c�digo de un modod e funcionamiento */
  public abstract String getCode(int a_modo);

  /** devuelve la descripci�n de un modo de funcionamiento */
  public abstract String getDes(int a_modo);

  /** indica si ya lleva where la select */
  public abstract boolean getWhere(int a_modo);

  /** esta funci�n devuelve la sentencia a ejecutar
   *
       * @param opmode, es un entero que indica el modso de operaci�n (sentencia SQL)
   * @param strSQL, es el StringBuffer en el que se devolver� la sentencia SQL
   * @param ini_param, son los datos iniciales
   * @return es el n�mero de datos que se espera obtener
   */
  public abstract int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                          CLista ini_param);

  /** funci�n gen�rica que guarda datos*/
  public void put(int a_clave, Object a_data) {
    if (arrayDatos.length > a_clave) {
      arrayDatos[a_clave] = a_data;
    }
  }

  public Object get(int a_clave) {
    if (arrayDatos.length > a_clave) {
      return arrayDatos[a_clave];
    }
    else {
      return null;
    }
  }

  /** devuelve el tama�o del vector
   *  como el valor 0 no se usa al size se le quita 1
   */
  public int size() {
    return arrayDatos.length - 1;
  }

  public int getNumParam() {
    return numParam;
  }

  public void setNumParam(int a_numParam) {
    numParam = a_numParam;
  }

} //________________________________________________  END_CLASS
