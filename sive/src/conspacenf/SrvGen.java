
package conspacenf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Registro.RegistroConsultas;
import capp.CLista;
import sapp.DBServlet;

/**
 *  Esta clase est� en el servidor recibe una CLista
 *  recoge de ella el nombre de la clase de datos y los par�metros
 *  ejecuta la sentencia SQL
 *  rellena una CLISTA con objetos de datos
 *  y finaliza la conexi�n
 */
public class SrvGen
    extends DBServlet {

  // modos de operaci�n
  public static final int servletALTA = 0;
  public static final int servletMODIFICAR = 1;
  public static final int servletBAJA = 2;
  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;

  /** conexi�n que se mantiene cada vez en el servidor */
  protected Connection con = null;

  /** statement de la sentencia SQL */
  protected PreparedStatement st = null;

  /** resultados de la query */
  protected ResultSet rs = null;

  /** sentencia sql que se est� ejecutando */
  protected StringBuffer strSQL = null;

  /** son los datos de los parametros */
  protected CLista param = null;

  /**  indica el n�mero de datos que hay que traer */
  protected int iNumReg = 0;

  /** indic asi es prueba o real */
  public boolean breal = true;

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  protected boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  protected RegistroConsultas registroConsultas = null;
  // Flags para indicar si hay que aplicar la LORTAD
  protected boolean existeSiveEnfermo;
  protected boolean existeSiveEdoind;
  protected String user = "";
  protected String passwd = "";
  protected boolean lortad;

  public SrvGen() {
    con = null;
  }

  protected CLista doWork(int opmode, CLista param) throws Exception {
    CLista result = null;
    this.param = param;
    Data paramData = null;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      //// System_out.println("ReportC3: bienvenido al servlet Report");

    }
    if (param == null || param.size() < 1) {
      // no nos han pasado datos iniciales
      return null;
    }
    else {
      paramData = (Data) param.firstElement();
    }

    // establece la conexi�n con la base de datos
    if (breal) {
      con = openConnection();
      /*
                if (aplicarLortad)
                {
         // Se obtiene la clave de acceso del usuario que se ha conectado
         passwd = sapp.Funciones.getPassword(con, st, rs, user);
         closeConnection(con);
         con=null;
         // Se abre una nueva conexion para el usuario
         con = openConnection(user, passwd);
                }
       */
    }

    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    try {
      ///establecemos las propiedades indicadas
      ///______________________________________
      con.setAutoCommit(true);

      // prepara la sentencia con sus par�metros
      StringBuffer strSQL = new StringBuffer();
      int iNumReg = paramData.prepareSQLSentencia(opmode, strSQL, param);
      strSQL.append(SrvGen.ponFiltro(opmode, paramData.getCode(opmode),
                                     paramData.getDes(opmode),
                                     paramData.getWhere(opmode), param));

      //// System_out.println("ReportC3: "+ strSQL.toString());
      result = realiza_SQL(opmode, iNumReg, strSQL.toString(), param);
    }
    catch (Exception exc) {
      String dd = exc.toString();
//E        // System_out.println("SrvC3Error: "+ exc.toString());
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    //// System_out.println("ReportC3: adios al servlet Report");
    return result;
  }

  //____________________________________________________________________

  protected CLista doPrueba(int opmode, CLista param) throws Exception {
    breal = false;
    // establece la conexi�n con la base de datos
    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
    con = DriverManager.getConnection("jdbc:odbc:pista_oracle", "pista_trg",
                                      "loteb98");

    return doWork(opmode, param);
  }

  // funcionalidad del servlet
  /**
   *  sevlet generico recibe los parametros de entrada en la CLista
   *  y devuelve una CLista con los resultados
   *
   * @param opmode es el modo en que se quiere que funcione la select
   * @param iNumReg es el numero de registros que se quieren leer
   * @param strSQL es la sentencia SQL a ejecutar
   * @param a_param es una Clista con lo par�metros
   * @return devuelve una CLista con los datos obtenidos
   */
  protected CLista realiza_SQL(int opmode, int iNumReg, String strSQL,
                               CLista a_param) throws Exception {
    // objetos JDBC

    // objetos de datos
    CLista result_data = null;
    /** parametros de entrada */
    CLista paramLista = a_param;
    Data paramData = null;
    int iValor = 1;

    if (paramLista != null && paramLista.size() >= 1) {
      paramData = (Data) paramLista.firstElement();
    }
    else {
//E        // System_out.println("REport: no hjan pasado par�metros a la select");
      return null;
    }

    st = con.prepareStatement(strSQL);

    existeSiveEnfermo = false;
    existeSiveEdoind = false;
    // ARG: Se comprueba si se accede a las tablas SIVE_ENFERMO o SIVE_EDOIND
    if ( (strSQL.toUpperCase()).indexOf("SIVE_ENFERMO") != -1) {
      existeSiveEnfermo = true;
    }
    if ( (strSQL.toUpperCase()).indexOf("SIVE_EDOIND") != -1) {
      existeSiveEdoind = true;

      // System_out.println("QUERY**** " + strSQL.toString() );

    }
    try {
      fijarParametros(paramData, opmode);

      // lanza la query
      if (iNumReg > 0) {
        // lo que se ha lanzado es una select
        rs = st.executeQuery();

        if (existeSiveEnfermo && existeSiveEdoind) {
          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_EDOIND", strSQL, "CD_ENFERMO",
                                      "", "SrvGen", false);
          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_ENFERMO", strSQL, "CD_ENFERMO",
                                      "", "SrvGen", true);
        }
        else if (existeSiveEnfermo) {
          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_ENFERMO", strSQL, "CD_ENFERMO",
                                      "", "SrvGen", true);
        }
        else if (existeSiveEdoind) {
          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_EDOIND", strSQL, "CD_ENFERMO",
                                      "", "SrvGen", true);
        }

        result_data = recogerDatos(paramData);
        rs.close();
        rs = null;
      }
      else {
        // se est� modificando la base de datos
        st.executeUpdate();
      }
    }
    catch (Exception exc) {
      String dd = exc.toString();
//E       // System_out.println("SrvGenError: "+ exc.toString());
    }

    // cerramos el cursor
    st.close();
    st = null;

    if (result_data != null) {
      result_data.trimToSize();
    }

    return result_data;
  }

  /**
   *  esta funci�n devuelve el Data que contieen los par�metros iniciales
   */
  public Data getIniParam() {
    if (param != null && param.size() > 0) {
      return (Data) param.firstElement();
    }
    else {
      return null;
    }
  }

  /**
   *  esta funci�n va leyendo los par�metros de Data de entrada
   *  y los prepara
   */
  protected void fijarParametros(Data paramData, int a_modo) {
    int tipoDato = 0;
    Object dato = null;
    int i = 0;

    boolean is_like = (a_modo == servletSELECCION_X_CODIGO) ||
        (a_modo == servletSELECCION_X_DESCRIPCION);
//// System_out.println("Report:: fijaParametros" );
    // ponemos los par�metros fijados por el usuario
    for (i = 1; i <= paramData.getNumParam(); i++) {
      try {
        dato = paramData.get(i);
        if (dato != null) {
          st.setObject(i, dato);
          // ARG: Se a�aden parametros
          if (existeSiveEnfermo || existeSiveEdoind) {
            registroConsultas.insertarParametro(dato.toString());
          }
        }
        else {
          st.setNull(i, java.sql.Types.VARCHAR);
          // ARG: Se a�aden parametros
          if (existeSiveEnfermo || existeSiveEdoind) {
            registroConsultas.insertarParametro("null");
          }
        }
      }
      catch (Exception exc) {
        String dd = exc.toString();
//E             // System_out.println("Report: Fallo paramtero "+ exc.toString());
      }
    }

    // a�adimos le filtro, se lee del Data
    String filtro = (String) paramData.getFiltro();
    if (filtro != null && filtro.trim().length() > 0) {
//// System_out.println("Report::: filtro" + filtro);
      try {
        if (is_like) {
//                 st.setObject(++i, dato);
//// System_out.println("Report::: islike" + filtro);
          st.setString(++i, (String) filtro);
          // ARG: Se a�aden parametros
          if (existeSiveEnfermo || existeSiveEdoind) {
            registroConsultas.insertarParametro(" " + (String) filtro);
          }
        }
        else {
//// System_out.println("Report::: islike" + "'"+dato+"%'");
          st.setString(++i, "'" + filtro + "%'");
          // ARG: Se a�aden parametros
          if (existeSiveEnfermo || existeSiveEdoind) {
            registroConsultas.insertarParametro(" '" + filtro + "%'");
          }
        }
      }
      catch (Exception exc) {
        String dd = exc.toString();
//E             // System_out.println("Report: Fallo paramtero "+ exc.toString());
      }
    }

    // a�adimos la paginaci�n
    if (param.getFilter().length() > 0) {
      try {
        st.setObject(++i, param.getFilter().trim());
      }
      catch (Exception exc) {
        String dd = exc.toString();
//E             // System_out.println("Report: Fallo paramtero "+ exc.toString());
      }
    }
  }

  /**
   *  esta funci�n realiza fetch de los datos leidos
   * y los va metiendo en la estructura solicitada
   *
   * @param paramData es un clase Data que tiene los par�metros iniciales
   * @return devuelve una CLista con todos los datos
   */
  public CLista recogerDatos(Data paramData) {
    CLista v = new CLista();
    Data h;
    Object o = null;
    String token;
    int num_datos = 0;
    int campo_filtro = 0;
    int i;

//       // System_out.println("Report: entramos en recoger datos");
    try {
      for (i = 0; rs.next(); i++) {
        // control de tama�o
        if (i > iNumReg) { //DBServlet.maxSIZE) {
          v.setState(CLista.listaINCOMPLETA);
          campo_filtro = ( (Data) v.lastElement()).getCampoFiltro();
          o = ( (Data) v.lastElement()).get(campo_filtro);
          v.setFilter(o.toString());
          break;
        }

        // de la clase hija nos da los datos del servlet
        // copiamosla clase hija paraque conserve el filtrado
        int num_cols = rs.getMetaData().getColumnCount();
        h = (Data) paramData.getNewData();
        for (int j = 1; j <= num_cols; j++) {
          o = rs.getObject(j);
          if (o != null) { // si es null no lo metemos
            h.put(j, o);
          }
        }

        v.addElement(h);
      }

      // vemos si est� llena la lista
      if (i <= iNumReg) {
        v.setState(CLista.listaLLENA);
      }

//            // System_out.println("Report:salimos bien");
      return v;
    }
    catch (SQLException exc) {
      String dd = exc.toString();
      exc.printStackTrace();
      return null;
    }
  }

  /**
   *  esta funci�n comprueba si hay un dato de filtrado
   *  si lo hay a�ade la clausula WHERE , ORDEER con par�metros ?
   *  @param a_mode, es el modo que se ha pedido
   *  @param a_code, es el campo c�digo por el que se quiere filtrar
   *  @param a_des,  ers el campo descripci�n
   *  @param a_where es un booleano que indica si se a�ade WHERE O AND
   *  @param param, es la CLista que tieen los par�metros
   */
  public static String ponFiltro(int a_mode, String a_code, String a_des,
                                 boolean a_where, CLista param) {
    Data parameter = (Data) (param.firstElement());
//      Object o = null;
//      String clave= null;
    StringBuffer resultSQL = new StringBuffer();
    boolean hay_parametros = false;
    String campo = null;
    boolean is_like = (a_mode == servletSELECCION_X_CODIGO) ||
        (a_mode == servletSELECCION_X_DESCRIPCION);

    if ( (a_mode == servletOBTENER_X_CODIGO) ||
        (a_mode == servletSELECCION_X_CODIGO)) {
      campo = a_code;
    }
    else if ( (a_mode == servletOBTENER_X_DESCRIPCION) ||
             (a_mode == servletSELECCION_X_DESCRIPCION)) {
      campo = a_des;
    }

    // a�adimos le filtro, se lee del Data
    String filtro = (String) parameter.getFiltro();
    if (filtro != null && filtro.trim().length() > 0) {
      if (hay_parametros) { // si ya hay parametrosa�adimo el AND
        resultSQL.append(" AND ");
      }
      if (is_like) {
        resultSQL.append(campo + " LIKE ? ");
      }
      else {
        resultSQL.append(campo + " = ? ");
      }

      hay_parametros = true;
    }

    // a�adimos la paginaci�n
    if (param.getFilter().length() > 0) {
      if (hay_parametros) { // si ya hay parametrosa�adimo el AND
        resultSQL.append(" AND ");
      }
      resultSQL.append(a_code + " > ? ");
      hay_parametros = true;
    }

    resultSQL.append(" order by " + a_code);

    // a�adimos la clausula WHERE o el AND
    if (a_where && hay_parametros) {
      return " WHERE " + resultSQL.toString();
    }
    if (hay_parametros) {
      return " AND " + resultSQL.toString();
    }

    return resultSQL.toString();
  }

  /**
   *  esta funci�n ejecuta la sentencia select con el filtro dado
   *  y devuelve el dato que le han pedido
       * @param a_strSQL es la sen tencia SQL SELECT DS_MUN FROM SIVE_MUNICIPIO WHERE
   * @param a_filter es el campo que lleva el dato para filtrar
   */
  public Object traeDescripcion(String a_strSQL, Object a_filter) {
    Object result = null;
    // si no hay filtro  no traemos nada
    if (a_filter == null) {
      return result;
    }

    try {
      st = con.prepareStatement(a_strSQL);

      // System_out.println("QUERY**** " + a_strSQL );

      if (a_filter != null) {
        st.setObject(1, a_filter);
      }
      else {
        st.setNull(1, java.sql.Types.VARCHAR);
      }
      rs = st.executeQuery();
      //ponemos el par�metro
      if (rs.next()) {
        result = rs.getObject(1);
      }
      rs.close();
      st.close();
    }
    catch (Exception exc) {
      String dd = exc.toString();
      result = null;
//E        // System_out.println("Report: fallamos en " + a_strSQL);
    }

    return result;
  }

  public Object traeDescripcion(String a_strSQL, Object a_filter,
                                Object a_filter2) {
    Object result = null;
    // si no hay filtro  no traemos nada
    if (a_filter == null) {
      return result;
    }

    try {
      st = con.prepareStatement(a_strSQL);

      // System_out.println("QUERY**** " + a_strSQL );

      if (a_filter != null) {
        st.setObject(1, a_filter);
      }
      else {
        st.setNull(1, java.sql.Types.VARCHAR);
      }

      if (a_filter2 != null) {
        st.setObject(2, a_filter2);
      }
      else {
        st.setNull(2, java.sql.Types.VARCHAR);
      }

      rs = st.executeQuery();
      //ponemos el par�metro
      if (rs.next()) {
        result = rs.getObject(1);
      }
      rs.close();
      st.close();
    }
    catch (Exception exc) {
      String dd = exc.toString();
      result = null;
//E        // System_out.println("Report: fallamos en " + a_strSQL);
    }

    return result;
  }

  /**
   *  esta funci�n ejecuta la sentencia select con el filtro dado
   *  y devuelve el dato que le han pedido
       * @param a_strSQL es la sen tencia SQL SELECT DS_MUN FROM SIVE_MUNICIPIO WHERE
   * @param campo2, es un StringBuffer con el resultado del campo2
   * @param a_filter es el campo que lleva el dato para filtrar
   */
  public Object traeDescripcion(String a_strSQL, StringBuffer campo2,
                                Object a_filter) {
    Object result = null;
    // si no hay filtro  no traemos nada
    if (a_filter == null || campo2 == null) {
      return result;
    }

    try {
      st = con.prepareStatement(a_strSQL);

      // System_out.println("QUERY**** " + a_strSQL );

      st.setObject(1, a_filter);
      rs = st.executeQuery();
      //ponemos el par�metro
      if (rs.next()) {
        result = rs.getObject(1);
        campo2.append(rs.getString(2));
      }
      rs.close();
      st.close();
    }
    catch (Exception exc) {
      String dd = exc.toString();
      result = null;
//E        // System_out.println("Report: fallamos en " + a_strSQL);
    }

    return result;
  }

  /** prueba del servlet */
  static public void main(String[] args) {
    try {
      CLista data = new CLista();
      DataPer d = new DataPer(DataPer.IT_FG_ENFERMO);
      d.setNumParam(1);
      d.put(1, "2222"); // ponemos el c�digo del enfermo
      data.addElement(d);

      SrvGen srv = new SrvGen();

      CLista dd = srv.doPrueba(DataPer.modoPERMISO, data);
      String ss = "Lista: " + dd;
//E     // System_out.println(ss);

      int i = 33;
    }
    catch (Exception exc) {
      String dd = exc.toString();
//E       // System_out.println("fallo " + exc.toString());
    }
  }

} //________________________________________________ END_CLASS
