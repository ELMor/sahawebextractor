
package conspacenf;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;

import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

/**
 *  Esta clase est� en el servidor recibe una CLista
 *  recoge de ella el nombre de la clase de datos y los par�metros
 *  ejecuta la sentencia SQL
 *  rellena una CLISTA con objetos de datos
 *  y finaliza la conexi�n
 */
public class SrvC3
    extends SrvGen {

  public void SrvC3() {
    con = null;
  }

  protected CLista doWork(int opmode, CLista param) throws Exception {
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy"); // E
    this.param = param;
    String descrip = null;
    Data hash = null;
    CLista result = null;
    this.param = param;
    Data paramData = null;
    String num_reg = null;
    Object o = null;

    String strFecha = null;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

//E      // System_out.println("ReportC3:2 bienvenido al servlet Report");

    }
    if (param == null || param.size() < 1) {
      // no nos han pasado datos iniciales
      return null;
    }
    else {
      paramData = (Data) param.firstElement();
    }

    if (breal) {
      con = openConnection();
      /*
                if (aplicarLortad)
                {
         // Se obtiene la clave de acceso del usuario que se ha conectado
         passwd = sapp.Funciones.getPassword(con, st, rs, user);
         closeConnection(con);
         con=null;
         // Se abre una nueva conexion para el usuario
         con = openConnection(user, passwd);
                }
       */
    }

    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    try {
      ///establecemos las propiedades indicadas
      ///______________________________________
      con.setAutoCommit(true);

      if (opmode == DataC3.modoCOUNTJOIN_ENFERMO_EDOIND) {
        num_reg = num_reg( (DataC3) paramData);
        //// System_out.println("Report: num_reg = " + num_reg);
      }

      if (opmode == DataC3.modoJOIN_ENFERMO_EDOIND ||
          opmode == DataC3.modoCOUNTJOIN_ENFERMO_EDOIND) {
        // hay que componer la sentencia SQL seg�n los campos
        StringBuffer strSQL = new StringBuffer();
        int iNumReg = paramData.prepareSQLSentencia(DataC3.
            modoJOIN_ENFERMO_EDOIND, strSQL, param);
        rellenar_param_opcionales(strSQL, (DataC3) paramData);
        // comprobamos si hay subconsultas
        if (paramData.get(DataC3.P_CD_E_NOTIF) != null) {
          strSQL.append(" AND b.NM_EDO IN ( ");
          paramData.prepareSQLSentencia(DataC3.modoNOTIFEDOI, strSQL, param);
          strSQL.append(paramData.get(DataC3.P_CD_E_NOTIF));
          if (paramData.get(DataC3.P_CD_C_NOTIF) != null) {
            strSQL.append(" AND CD_E_NOTIF IN ( ");
            paramData.prepareSQLSentencia(DataC3.modoE_NOTIF, strSQL, param);
            strSQL.append(paramData.get(DataC3.P_CD_C_NOTIF));
            strSQL.append(")");
          }
          strSQL.append(")");
        }
        strSQL.append(DataC3.sql_ORDER);
        //// System_out.println("ReportC3: "+ strSQL.toString());

        result = realiza_SQL(opmode, iNumReg, strSQL.toString(), param);
        //// System_out.println("realizada primera SQL "+ result);
        //  rellenamos la lista con la descripci�n de los municipios
        for (Enumeration e = result.elements(); e.hasMoreElements(); ) {
          hash = (Data) e.nextElement();
          // nos traemos el resto de datos
          hash.put(DataC3.PERMISO, paramData.get(DataC3.P_PERMISO));
          //// System_out.println("Report: numreg " + num_reg);
          hash.put(DataC3.COUNT, num_reg);

          StringBuffer itPrimero = new StringBuffer();
          descrip = (String) traeDescripcion("SELECT CD_E_NOTIF, IT_PRIMERO FROM SIVE_NOTIF_EDOI WHERE NM_EDO =? AND CD_FUENTE = 'E' ",
                                             itPrimero, hash.get(DataC3.NM_EDO));
          String si = "S";

          // se quita la etiqueta (P)
          hash.put(DataC3.CD_E_NOTIF, descrip);

          /*                 if (si.equals(itPrimero.toString()))
                              hash.put(DataC3.CD_E_NOTIF, "(P)  " + descrip);
                           else
                              hash.put(DataC3.CD_E_NOTIF, descrip);
           */
          //// System_out.println("Cualo 1");

          descrip = (String) traeDescripcion(
              "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE CD_MUN = ? AND CD_PROV = ? ",
              hash.get(DataC3.CD_MUN), hash.get(DataC3.CD_PROV));
          if (descrip != null) {
            hash.put(DataC3.DS_MUN, descrip);

            //// System_out.println("Cualo 2");
          }
          descrip = (String) traeDescripcion(
              "SELECT DS_CLASIFDIAG FROM SIVE_CLASIF_EDO WHERE CD_CLASIFDIAG= ?",
              hash.get(DataC3.CD_CLASIFDIAG));
          if (descrip != null) {
            hash.put(DataC3.DS_CLASIFDIAG, descrip);
            //___________________ LRivera
            // obtiene la descripcion de CLASIFDIAG  en funci�n del idioma
          }
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            descrip = (String) traeDescripcion(
                "SELECT DSL_CLASIFDIAG FROM SIVE_CLASIF_EDO WHERE CD_CLASIFDIAG= ?",
                hash.get(DataC3.CD_CLASIFDIAG));
            if (descrip != null) {
              hash.put(DataC3.DS_CLASIFDIAG, descrip);
            }
          }

          // ARG: Para calcular la edad
          /*                 descrip = (String) (traeDescripcion("SELECT FC_FECNOTIF, IT_PRIMERO FROM SIVE_NOTIF_EDOI WHERE NM_EDO =? AND CD_FUENTE = 'E' ", itPrimero, hash.get(DataC3.NM_EDO))).toString().substring(0,11);
               if (descrip != null) hash.put(DataC3.FC_FECNOTIF, descrip.substring(8, 10) + "/"
               + descrip.substring(5, 7) + "/"
               + descrip.substring(0, 4));
           */

          Object fechaNotif = (Object) (traeDescripcion("SELECT FC_FECNOTIF, IT_PRIMERO FROM SIVE_NOTIF_EDOI WHERE NM_EDO =? AND CD_FUENTE = 'E' ",
              itPrimero, hash.get(DataC3.NM_EDO)));
          if (fechaNotif != null) {
            hash.put(DataC3.FC_FECNOTIF, fechaNotif);

            // fecha
          }
          if (hash.get(DataC3.FC_INISNT) == null) {
            hash.put(DataC3.FC_INISNT, "");
            //// System_out.println("Era nulo...");
          }
          else {
            //// System_out.println("SrvC3: paso por aqu�");
            strFecha = new String( (hash.get(DataC3.FC_INISNT)).toString().
                                  substring(0, 11));
            //// System_out.println(strFecha);
            hash.put(DataC3.FC_INISNT, strFecha.substring(8, 10) + "/"
                     + strFecha.substring(5, 7) + "/"
                     + strFecha.substring(0, 4)
                     );
          }
        } //orf
        //// System_out.println("Report: Ende del for");
      }
      else if (opmode == DataC3.modoCOUNT) {
        // hay que componer la sentencia SQL seg�n los campos
        StringBuffer strSQL = new StringBuffer();
        int iNumReg = paramData.prepareSQLSentencia(opmode, strSQL, param);
        // comprobamos si hay subconsultas
        if (paramData.getNumParam() >= DataC3.P_CD_E_NOTIF &&
            paramData.get(DataC3.P_CD_E_NOTIF) != null) {
          strSQL.append(" AND b.NM_EDO IN ( ");
          paramData.prepareSQLSentencia(DataC3.modoNOTIFEDOI, strSQL, param);
          if (paramData.getNumParam() >= DataC3.P_CD_C_NOTIF &&
              paramData.get(DataC3.P_CD_C_NOTIF) != null) {
            strSQL.append(" AND CD_E_NOTIF IN ( ");
            paramData.prepareSQLSentencia(DataC3.modoE_NOTIF, strSQL, param);
            strSQL.append(")");
          }
          strSQL.append(")");
        }
        strSQL.append(DataC3.sql_ORDER);
        //// System_out.println("ReportC3: "+ strSQL.toString());

        result = realiza_SQL(opmode, iNumReg, strSQL.toString(), param);
        //// System_out.println("realizada primera SQL "+ result);
      }

      CLista l = ( (CLista) result); // prueba
      //// System_out.println ("compreobamos");
    }
    catch (Exception exc) {
      String dd = exc.toString();
      //// System_out.println("SrvC3 Error: "+ exc.toString());
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    //// System_out.println("SrvC3 adios" + result);
    return result;
  }

  public String num_reg(DataC3 paramData) {
    try {
      StringBuffer strSQL = new StringBuffer();
      int iNumReg = paramData.prepareSQLSentencia(DataC3.modoCOUNT, strSQL,
                                                  param);
      // comprobamos si hay subconsultas
//            if (paramData.getNumParam() >= DataC3.P_CD_E_NOTIF && paramData.get(DataC3.P_CD_E_NOTIF) != null){
      if (paramData.get(DataC3.P_CD_E_NOTIF) != null) {
        strSQL.append(" AND b.NM_EDO IN ( ");
        paramData.prepareSQLSentencia(DataC3.modoNOTIFEDOI, strSQL, param);
        strSQL.append(paramData.get(DataC3.P_CD_E_NOTIF));
//                if (paramData.getNumParam() >= DataC3.P_CD_C_NOTIF && paramData.get(DataC3.P_CD_C_NOTIF) != null){
        if (paramData.get(DataC3.P_CD_C_NOTIF) != null) {
          strSQL.append(" AND CD_E_NOTIF IN ( ");
          paramData.prepareSQLSentencia(DataC3.modoE_NOTIF, strSQL, param);
          strSQL.append(paramData.get(DataC3.P_CD_C_NOTIF));
          strSQL.append(")");
        }
        strSQL.append(")");
      }
      strSQL.append(DataC3.sql_ORDER);
      //// System_out.println("ReportC3: "+ strSQL.toString());

      String inum_pag = (String) ( (DataC3) param.firstElement()).get(DataC3.
          COUNT);
      ( (DataC3) param.firstElement()).put(DataC3.COUNT, (Object)new String("0"));
      String ss = strSQL.toString();
      CLista result = realiza_SQL(DataC3.modoCOUNT, iNumReg, strSQL.toString(),
                                  param);
      ( (DataC3) param.firstElement()).put(DataC3.COUNT, (Object) inum_pag);

      if (result != null && result.size() > 0) {
        Object o = ( (DataC3) result.firstElement()).get(1);
        //// System_out.println("Report Double: "+ o.toString());
        return o.toString();
      }

    }
    catch (Exception exc) {
      String ss = exc.toString();
//E         // System_out.println("ReportC3: SELECT COunt "+ exc.toString());
    }

    return new String("0");

  }

  /** esta funci�n rellena los like si hacen falta */
  public void rellenar_param_opcionales(StringBuffer strSQL, DataC3 a_data) {
//               " AND b.CD_MUN LIKE ? AND b.CD_NIVEL_1 LIKE ? AND b.CD_NIVEL_2 LIKE ? AND b.CD_ZBS LIKE ?";
    String dato = (String) a_data.get(DataC3.P_CD_PROV);
    if (dato != null && dato.trim().length() >= 0) {
      strSQL.append(" AND b.CD_PROV LIKE '" + dato + "%'");
    }

    dato = (String) a_data.get(DataC3.P_CD_MUN);
    if (dato != null && dato.trim().length() >= 0) {
      strSQL.append(" AND b.CD_MUN LIKE '" + dato + "%'");
    }

    dato = (String) a_data.get(DataC3.P_CD_NIVEL_1);
    if (dato != null && dato.trim().length() >= 0) {
      strSQL.append(" AND b.CD_NIVEL_1 LIKE '" + dato + "%'");
    }

    dato = (String) a_data.get(DataC3.P_CD_NIVEL_2);
    if (dato != null && dato.trim().length() >= 0) {
      strSQL.append(" AND b.CD_NIVEL_2 LIKE '" + dato + "%'");
    }

    dato = (String) a_data.get(DataC3.P_CD_ZBS);
    if (dato != null && dato.trim().length() >= 0) {
      strSQL.append(" AND b.CD_ZBS LIKE '" + dato + "%'");
    }
  }

  /**
   *  esta funci�n realiza fetch de los datos leidos
   * y los va metiendo en la estructura solicitada
   *  pagina seg�n el par�metro 0
   *
   * @param paramData es un clase Data que tiene los par�metros iniciales
   * @return devuelve una CLista con todos los datos
   */
  public CLista recogerDatos(Data paramData) {
    CLista result = new CLista();
    Data h;
    Object o;
    int num_datos = 0;
    int campo_filtro = 0;
    int i;

    //// System_out.println("Report: entramos en recoger datos");
    //recogemos el par�metro de paginaci�n
    String pag = (String) paramData.get(DataC3.P_PAGINA);
    int ini_pagina = 0;
    int fin_pagina = 0;
    if (pag != null) {
      ini_pagina = Integer.parseInt(pag);
    }
    ini_pagina = (ini_pagina * DBServlet.maxSIZE);
    fin_pagina = ini_pagina + DBServlet.maxSIZE;

    try {
      for (i = 0; rs.next(); i++) {
        // control de tama�o
        if ( (i >= fin_pagina) && (! (paramData.bInformeCompleto))) { //DBServlet.maxSIZE) {
          result.setState(CLista.listaINCOMPLETA);
          campo_filtro = ( (Data) result.lastElement()).getCampoFiltro();
          o = ( (Data) result.lastElement()).get(campo_filtro);
          result.setFilter(o.toString());
          break;
        }
        else if (i < ini_pagina) {
          // no guardamos los datos
          continue;
        }

        // de la clase hija nos da los datos del servlet
        // copiamosla clase hija paraque conserve el filtrado
        h = (Data) paramData.getNewData();
        int num_cols = rs.getMetaData().getColumnCount();
        for (int j = 1; j <= num_cols; j++) {
          o = rs.getObject(j);
          if (o != null) { // si es null no lo metemos
            h.put(j, o);
          }
        }
        result.addElement(h);
      } //orf

      // vemos si est� llena la lista
      if (i < fin_pagina) {
        result.setState(CLista.listaLLENA);
      }

      //// System_out.println("Report:salimos bien");
      return result;
    }
    catch (SQLException exc) {
      String dd = exc.toString();
      exc.printStackTrace();
      return null;
    }
  }

  /**
   *  esta funci�n va leyendo los par�metros de Data de entrada
   *  y los prepara, no incluye paginaci�n
   */
  protected void fijarParametros(Data paramData, int a_modo) {
    int tipoDato = 0;
    Object dato = null;
    int i = 0;

    boolean is_like = (a_modo == servletSELECCION_X_CODIGO) ||
        (a_modo == servletSELECCION_X_DESCRIPCION);
//// System_out.println("Report:: fijaParametros" );
    // ponemos los par�metros fijados por el usuario
    for (i = 1; i <= paramData.getNumParam(); i++) {
      try {
        dato = paramData.get(i);
        if (dato != null) {
          st.setObject(i, dato);
          //// System_out.println("Param" +i + ": " + dato);
          // ARG: Se a�aden parametros
          if (existeSiveEnfermo || existeSiveEdoind) {
            registroConsultas.insertarParametro(dato.toString());
          }
        }
        else {
          st.setNull(i, java.sql.Types.VARCHAR);
          // ARG: Se a�aden parametros
          if (existeSiveEnfermo || existeSiveEdoind) {
            registroConsultas.insertarParametro("null");
          }
        }
      }
      catch (Exception exc) {
        String dd = exc.toString();
//E             // System_out.println("Report: Fallo paramtero "+ exc.toString());
      }
    }
  }

  /** prueba del servlet */
  static public void main(String[] args) {
    try {
      CLista data = new CLista();
      DataC3 d = new DataC3(DataC3.DS_APE1);
      d.setNumParam(7); //11);   //DataC3.P_NUM_C3);
      d.put(DataC3.P_PAGINA, "0");
      d.put(DataC3.P_DESDE_CD_ANOEPI1, "1999");
      d.put(DataC3.P_DESDE_CD_SEMEPI2, "01");
      d.put(DataC3.P_DESDE_CD_ANOEPI3, "1999");
      d.put(DataC3.P_HASTA_CD_ANOEPI4, "1999");
      d.put(DataC3.P_HASTA_CD_ANOEPI5, "1999");
      d.put(DataC3.P_HASTA_CD_SEMEPI6, "14");
      d.put(DataC3.P_CD_ENFCIE, "0-0-0");
      /*     d.put(DataC3.P_CD_C_NOTIF, "'2%'");
           d.put(DataC3.P_CD_E_NOTIF, "'%'");
           d.put(DataC3.P_CD_PROV, "'%'");
           d.put(DataC3.P_CD_MUN, "'%'");
           d.put(DataC3.P_CD_NIVEL_1, "'%'");
           d.put(DataC3.P_CD_NIVEL_2, "'%'");
           d.put(DataC3.P_CD_ZBS, "'%'");
       */data.addElement(d);

      SrvC3 srv = new SrvC3();

      CLista dd = srv.doPrueba(DataC3.modoCOUNTJOIN_ENFERMO_EDOIND, data);
      //String ss = "Lista: " + dd;
      //// System_out.println(ss);

      int i = 33;
    }
    catch (Exception exc) {
      String ss = "fallo " + exc.toString();
//E       // System_out.println("C3 Error " + ss);
    }
  }

} //________________________________________________ END_CLASS
