package pannivelesedo;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import nivel1.DataNivel1;
import notdata.DataEntradaEDO;
import sapp.StubSrvBD;
import zbs.DataZBS;
import zbs.DataZBS2;

public class panelNiveles
    extends CPanel {

  private final int servletOBTENER_X_CODIGO = 3;
  private final int servletOBTENER_X_DESCRIPCION = 4;
  private final int servletSELECCION_X_CODIGO = 5;
  private final int servletSELECCION_X_DESCRIPCION = 6;

  private final int servletSELECCION_NIV2_X_CODIGO = 7;
  private final int servletOBTENER_NIV2_X_CODIGO = 8;
  private final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  private final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  // ARG: Selecci�n de distrito/Zona b�sica de salud
  final int servletSELECCION_NIV2_X_CODIGO_CON_ZBS = 30;
  final int servletOBTENER_NIV2_X_CODIGO_CON_ZBS = 31;
  final int servletSELECCION_NIV2_X_DESCRIPCION_CON_ZBS = 32;
  final int servletOBTENER_NIV2_X_DESCRIPCION_CON_ZBS = 33;

  // servlets para la b�squeda de niveles
  private final String strSERVLETNivel1 = "servlet/SrvNivel1";
  private final String strSERVLETNivel2 = "servlet/SrvZBS2";
  private final String strSERVLETZona = "servlet/SrvMun";

  private final String strSERVLETAut = "servlet/SrvEntradaEDO";

  /**
   * atributo utilizado para la captura de los textos en el idioma
   * correspondiente del usuario
   */
  ResourceBundle res;

  // Flag que indica si hay que introducir la ZBS junto con el distrito.
  boolean CON_ZBS;

  //conexion del panel con el panel que lo contiene
  usaPanelNiveles contenedor = null;
  int modoContenedor = -1;

  // conexi�n con los servlets
  protected StubSrvBD stubCliente = new StubSrvBD();

  // sincronizaci�n de eventos
  private boolean sinBloquear = true;

  // Informa de si el componente fue inhabilitado exteriormente.
  private boolean bHabilitado = true;

  // Modos de operaci�n del panel
  public static final int MODO_INICIO = 0;
  public static final int MODO_ESPERA = 1001;
  public static final int MODO_N1 = 1002;
  public static final int MODO_N2 = 1004;
  public static final int MODO_N3 = 1006;

  private int modoAnterior = 0;
  int modoOperacion = 0;

  // Indica si se debe tener en cuenta el perfil y el login del usuario
  // a la hora de acceder a los niveles 1 y 2.
  private boolean bAutorizacion = false;

  // Para saber qu� controles se visualizan
  public static final int TIPO_NIVEL_1 = 1;
  public static final int TIPO_NIVEL_2 = 2;
  public static final int TIPO_NIVEL_3 = 3;

  private int tipoOperacion = TIPO_NIVEL_3;

  // Para saber su distribuci�n
  public static final int LINEAS_1 = 1;
  public static final int LINEAS_2 = 2;
  public static final int LINEAS_3 = 3;

  //No puede darse el caso de TIPO_NIVEL_3 y COLUMNAS_2
  public static final int COLUMNAS_2 = 12; //una linea con dos columnas

  private int lineas = 0;

  // Para controlar el cambio de las cajas de texto
  private String strCDNivel1 = "";
  private String strCDNivel2 = "";
  private String strCDNivel3 = "";

  // Controles
  TextField txtNivel1 = null;
  TextField txtNivel2 = null;
  TextField txtNivel3 = null;

  TextField txtNivel1L = null;
  TextField txtNivel2L = null;
  TextField txtNivel3L = null;

  String antantTxtNivel1L = new String();
  String antantTxtNivel2L = new String();
  String antantTxtNivel3L = new String();

  String antTxtNivel1L = new String();
  String antTxtNivel2L = new String();
  String antTxtNivel3L = new String();

  ButtonControl btnNivel1 = null;
  ButtonControl btnNivel2 = null;
  ButtonControl btnNivel3 = null;

  Label lblNivel1 = null;
  Label lblNivel2 = null;
  Label lblNivel3 = null;

  // Gestores de eventos
  PanelNivelesBtnActionListener btnActionListener = new
      PanelNivelesBtnActionListener(this);
  PanelNivelesTextListener textListener = new PanelNivelesTextListener(this);
  PanelNivelesDesTextListener textDesListener = new PanelNivelesDesTextListener(this);
  PanelNivelesTextFocusListener txtFocusListener = new
      PanelNivelesTextFocusListener(this);
  XYLayout xyFondo = new XYLayout();

  // Interfaz
  /** Inhabilita todos los objetos y pone el cursor en modo de espera. */
  public void setModoEspera() {
    setModoDeshabilitado();
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
  }

  /** Inhabilita todos los objetos. */
  public void setModoDeshabilitado() {
    if (bHabilitado) {
      int modoTmp = modoOperacion;
      modoOperacion = MODO_ESPERA;
      Inicializar();
      modoOperacion = modoTmp;
      bHabilitado = false;
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  public void setModoOperacion(int m) {
    modoOperacion = m;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  /** Restaura el estado anterior del panel y pone el cursor en modo normal. */
  public void setModoNormal() {
    bHabilitado = true;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // M�todos de lectura

  public String getCDNivel1() {
    return txtNivel1.getText().trim();
  }

  public String getCDNivel2() {
    String strValor = null;
    if (tipoOperacion >= TIPO_NIVEL_2) {
      strValor = txtNivel2.getText().trim();
    }
    return strValor;
  }

  public String getCDNivel3() {
    String strValor = null;
    if (tipoOperacion == TIPO_NIVEL_3) {
      strValor = txtNivel3.getText().trim();
    }
    return strValor;
  }

  public String getDSNivel1() {
    return txtNivel1L.getText().trim();
  }

  public String getDSNivel2() {
    String strValor = null;
    if (tipoOperacion >= TIPO_NIVEL_2) {
      strValor = txtNivel2L.getText().trim();
    }
    return strValor;
  }

  public String getDSNivel3() {
    String strValor = null;
    if (tipoOperacion == TIPO_NIVEL_3) {
      strValor = txtNivel3L.getText().trim();
    }
    return strValor;
  }

  // Escritura
  public void setNivelObligatorio(int iNivel, boolean bValor) {
    switch (iNivel) {
      case 1:
        txtNivel1.setBackground(bValor ? new Color(255, 255, 150) :
                                new Color(255, 255, 250));
        break;
      case 2:
        if (tipoOperacion >= TIPO_NIVEL_2) {
          txtNivel2.setBackground(bValor ? new Color(255, 255, 150) :
                                  new Color(255, 255, 250));
        }
        break;
      case 3:
        if (tipoOperacion == TIPO_NIVEL_3) {
          txtNivel2.setBackground(bValor ? new Color(255, 255, 150) :
                                  new Color(255, 255, 250));
        }
        break;
    }
    repaint();
  }

  public void setCDNivel1(String strValor) {
    sinBloquear = false;
    txtNivel1.setText(strValor.trim());
    sinBloquear = true;
  }

  public void setCDNivel2(String strValor) {
    sinBloquear = false;
    if (tipoOperacion >= TIPO_NIVEL_2) {
      txtNivel2.setText(strValor.trim());
    }
    sinBloquear = true;
  }

  public void setCDNivel3(String strValor) {
    sinBloquear = false;
    if (tipoOperacion == TIPO_NIVEL_3) {
      txtNivel3.setText(strValor.trim());
    }
    sinBloquear = true;
  }

  public void setDSNivel1(String strValor) {
    txtNivel1L.setText(strValor.trim());
  }

  public void setDSNivel2(String strValor) {
    if (tipoOperacion >= TIPO_NIVEL_2) {
      txtNivel2L.setText(strValor.trim());
    }
  }

  public void setDSNivel3(String strValor) {
    if (tipoOperacion == TIPO_NIVEL_3) {
      txtNivel3L.setText(strValor.trim());
    }
  }

  //****************************************************************

   public void Inicializar() {
     switch (modoOperacion) {
       case MODO_INICIO:
         txtNivel1.setEnabled(true);
         btnNivel1.setEnabled(true);
         lblNivel1.setEnabled(true);
         if (tipoOperacion >= TIPO_NIVEL_2) {
           txtNivel2.setEnabled(false);
           btnNivel2.setEnabled(false);
           lblNivel2.setEnabled(true);
           if (tipoOperacion == TIPO_NIVEL_3) {
             txtNivel3.setEnabled(false);
             btnNivel3.setEnabled(false);
             lblNivel3.setEnabled(true);
           }
         }

         break;
       case MODO_N1:
         txtNivel1.setEnabled(true);
         btnNivel1.setEnabled(true);
         lblNivel1.setEnabled(true);
         if (tipoOperacion >= TIPO_NIVEL_2) {
           txtNivel2.setEnabled(true);
           btnNivel2.setEnabled(true);
           lblNivel2.setEnabled(true);
           if (tipoOperacion == TIPO_NIVEL_3) {
             txtNivel3.setEnabled(false);
             btnNivel3.setEnabled(false);
             lblNivel3.setEnabled(true);
           }
         }
         break;
       case MODO_N2:
       case MODO_N3:
         txtNivel1.setEnabled(true);
         btnNivel1.setEnabled(true);
         lblNivel1.setEnabled(true);
         if (tipoOperacion >= TIPO_NIVEL_2) {
           txtNivel2.setEnabled(true);
           btnNivel2.setEnabled(true);
           lblNivel2.setEnabled(true);
           if (tipoOperacion == TIPO_NIVEL_3) {
             txtNivel3.setEnabled(true);
             btnNivel3.setEnabled(true);
             lblNivel3.setEnabled(true);
           }
         }
         break;
       case MODO_ESPERA:
         txtNivel1.setEnabled(false);
         btnNivel1.setEnabled(false);
         lblNivel1.setEnabled(false);
         if (tipoOperacion >= TIPO_NIVEL_2) {
           txtNivel2.setEnabled(false);
           btnNivel2.setEnabled(false);
           lblNivel2.setEnabled(false);
           if (tipoOperacion == TIPO_NIVEL_3) {
             txtNivel3.setEnabled(false);
             btnNivel3.setEnabled(false);
             lblNivel3.setEnabled(false);
           }
         }

         break;
     }
   }

  public panelNiveles(CApp a, usaPanelNiveles usuario, int modoOperacion,
                      int tipoOperacion, int lineas) {
    setApp(a);

    this.contenedor = usuario;

    // ARG: Inicializamos el flag CON_ZBS
    if (getApp().getParameter("ORIGEN").equals("M")) {
      CON_ZBS = false;
    }
    else {
      CON_ZBS = true;

    }
    res = ResourceBundle.getBundle("pannivelesedo.Res" + app.getIdioma());

    this.modoOperacion = modoOperacion;
    this.tipoOperacion = tipoOperacion;
    this.lineas = lineas;

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public panelNiveles(CApp a, usaPanelNiveles usuario, int modoOperacion,
                      int tipoOperacion, int lineas, boolean bAutorizacion) {
    setApp(a);
    this.contenedor = usuario;

    // ARG: Inicializamos el flag CON_ZBS
    if (getApp().getParameter("ORIGEN").equals("M")) {
      CON_ZBS = false;
    }
    else {
      CON_ZBS = true;

//    res = ResourceBundle.getBundle("pannivelesedo.Res" + getApp().getIdioma());
    }
    res = ResourceBundle.getBundle("pannivelesedo.Res" + app.getIdioma());

    this.modoOperacion = modoOperacion;
    this.tipoOperacion = tipoOperacion;
    this.lineas = lineas;
    this.bAutorizacion = bAutorizacion;

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        "images/Magnify.gif"};

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setSize(new Dimension(449, 94));
    this.setBorde(false);
    this.setLayout(xyFondo);

    txtNivel1 = new TextField();
    txtNivel1.addFocusListener(txtFocusListener);
    txtNivel1.addTextListener(textListener);
    txtNivel1.setName("nivel1");

    btnNivel1 = new ButtonControl();
    btnNivel1.addActionListener(btnActionListener);
    btnNivel1.setActionCommand("nivel1");
    btnNivel1.setImage(imgs.getImage(0));

    txtNivel1L = new TextField();
    txtNivel1L.setEditable(false);
    txtNivel1L.setEnabled(false);
    txtNivel1L.addTextListener(textDesListener);
    txtNivel1L.setName("desNivel1");

    lblNivel1 = new Label();
    lblNivel1.setText(app.getNivel1());

    if (lineas == LINEAS_3) {
      this.add(lblNivel1, new XYConstraints(4, 6, 92, 19));
      this.add(txtNivel1, new XYConstraints(103, 6, 66, 22));
      this.add(btnNivel1, new XYConstraints(179, 6, -1, -1));
      this.add(txtNivel1L, new XYConstraints(217, 6, 216, 22));
    }
    else if (lineas == LINEAS_2) {
      this.add(lblNivel1, new XYConstraints(4, 6, 92, 19));
      this.add(txtNivel1, new XYConstraints(95, 6, 38, 22));
      this.add(btnNivel1, new XYConstraints(138, 6, -1, -1));
      this.add(txtNivel1L, new XYConstraints(170, 6, 195, 22));
    }
    else if (lineas == LINEAS_1) {
      this.add(lblNivel1, new XYConstraints(4, 6, 80, 19));
      this.add(txtNivel1, new XYConstraints(85, 6, 38, 22));
      this.add(btnNivel1, new XYConstraints(125, 6, -1, -1));
      this.add(txtNivel1L, new XYConstraints(155, 6, 1, 1));
      txtNivel1L.setVisible(false);
    }
    else if (lineas == COLUMNAS_2) {
      this.add(lblNivel1, new XYConstraints(4, 6, 92, 19));
      this.add(txtNivel1, new XYConstraints(100, 6, 38, 22));
      this.add(btnNivel1, new XYConstraints(140, 6, -1, -1));
      this.add(txtNivel1L, new XYConstraints(175, 6, 1, 1));
      txtNivel1L.setVisible(false);
    }

    if (tipoOperacion >= TIPO_NIVEL_2) {

      txtNivel2 = new TextField();
      txtNivel2.addFocusListener(txtFocusListener);
      txtNivel2.addTextListener(textListener);
      txtNivel2.setName("nivel2");

      btnNivel2 = new ButtonControl();
      btnNivel2.addActionListener(btnActionListener);
      btnNivel2.setActionCommand("nivel2");
      btnNivel2.setImage(imgs.getImage(0));

      txtNivel2L = new TextField();
      txtNivel2L.setEditable(false);
      txtNivel2L.setEnabled(false);
      txtNivel2L.addTextListener(textDesListener);
      txtNivel2L.setName("desNivel2");

      lblNivel2 = new Label();
      lblNivel2.setText(app.getNivel2());

      if (lineas == LINEAS_3) {
        this.add(lblNivel2, new XYConstraints(4, 35, 92, 21));
        this.add(txtNivel2, new XYConstraints(103, 35, 66, 22));
        this.add(btnNivel2, new XYConstraints(179, 35, -1, -1));
        this.add(txtNivel2L, new XYConstraints(217, 35, 216, 22));
      }
      else if (lineas == LINEAS_2) {
        this.add(lblNivel2, new XYConstraints(300 + 75 + 4, 6, 92, 21));
        this.add(txtNivel2, new XYConstraints(300 + 75 + 95, 6, 38, 22));
        this.add(btnNivel2, new XYConstraints(300 + 75 + 138, 6, -1, -1));
        this.add(txtNivel2L, new XYConstraints(300 + 75 + 170, 6, 195, 22));
      }
      else if (lineas == LINEAS_1) {
        this.add(lblNivel2, new XYConstraints(160, 6, 80, 21));
        this.add(txtNivel2, new XYConstraints(245, 6, 38, 22));
        this.add(btnNivel2, new XYConstraints(285, 6, -1, -1));
        this.add(txtNivel2L, new XYConstraints(315, 6, 1, 1));
        txtNivel2L.setVisible(false);
      }
      else if (lineas == COLUMNAS_2) {
        this.add(lblNivel2, new XYConstraints(180, 6, 92, 21));
        this.add(txtNivel2, new XYConstraints(275, 6, 38, 22));
        this.add(btnNivel2, new XYConstraints(315, 6, -1, -1));
        this.add(txtNivel2L, new XYConstraints(345, 6, 195, 22));
      }

      if (tipoOperacion == TIPO_NIVEL_3) {

        txtNivel3 = new TextField();
        txtNivel3.addFocusListener(txtFocusListener);
        txtNivel3.addTextListener(textListener);
        txtNivel3.setName("nivel3");
        txtNivel3.setEnabled(false);

        btnNivel3 = new ButtonControl();
        btnNivel3.addActionListener(btnActionListener);
        btnNivel3.setActionCommand("nivel3");
        btnNivel3.setImage(imgs.getImage(0));

        lblNivel3 = new Label();
        lblNivel3.setText(app.getNivel3());

        txtNivel3L = new TextField();
        txtNivel3L.setEditable(false);
        txtNivel3L.setEnabled(false);
        txtNivel3L.addTextListener(textDesListener);
        txtNivel3L.setName("desNivel3");

        if (lineas == LINEAS_3) {
          this.add(lblNivel3, new XYConstraints(4, 64, 92, 21));
          this.add(txtNivel3, new XYConstraints(103, 64, 66, 22));
          this.add(btnNivel3, new XYConstraints(179, 64, -1, -1));
          this.add(txtNivel3L, new XYConstraints(217, 64, 217, 22));
        }
        else if (lineas == LINEAS_2) {
          this.add(lblNivel3, new XYConstraints(4, 35, 92, 21));
          this.add(txtNivel3, new XYConstraints(95, 35, 38, 22));
          this.add(btnNivel3, new XYConstraints(138, 35, -1, -1));
          this.add(txtNivel3L, new XYConstraints(170, 35, 195, 22));
        }
        else if (lineas == LINEAS_1) {
          this.add(lblNivel3, new XYConstraints(320, 6, 80, 21));
          this.add(txtNivel3, new XYConstraints(405, 6, 38, 22));
          this.add(btnNivel3, new XYConstraints(445, 6, -1, -1));
          this.add(txtNivel3L, new XYConstraints(475, 6, 175, 22));
        }
      }
    }

    if (lineas == LINEAS_3) {
      xyFondo.setHeight(91);
      xyFondo.setWidth(438);
    }
    else if (lineas == LINEAS_2) {
      xyFondo.setHeight(70);
      xyFondo.setWidth(755);
    }
    else if (lineas == LINEAS_1) {
      xyFondo.setHeight(40);
      xyFondo.setWidth(655);
    }
    else if (lineas == COLUMNAS_2) {
      xyFondo.setHeight(40);
      xyFondo.setWidth(550);
    }

    Inicializar();

  }

  // Sistema de bloqueo de eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = MODO_ESPERA;

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      return false;
    }
  }

  /** Este m�todo desbloquea el sistema */
  protected synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == MODO_ESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    if (modoOperacion == MODO_ESPERA) {
      modoContenedor = contenedor.ponerEnEspera();
    }
    else if (modoContenedor != -1) {
      contenedor.ponerModo(modoContenedor);

    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // Borrado de las cajas de texto
  private void borraNivel2() {
    if (tipoOperacion >= TIPO_NIVEL_2) {
      txtNivel2.setText("");
      txtValueChangeNivel2();
    }
  }

  private void borraNivel3() {
    if (tipoOperacion == TIPO_NIVEL_3) {
      txtNivel3.setText("");
      txtValueChangeNivel3();
    }
  }

//**************************************************************************
//          M�todos asociados a la gesti�n de eventos
//**************************************************************************

    // Para el interfaz "usaPanelNiveles"
    // informado ---> informado : resetear el suca
    // vacio ---> informado : nada
    // informado ---> vacio : nada

    protected void txtDesValueChangeNivel1() {
      //// System_out.println("  txtDesValueChangeNivel1 antes:" + antTxtNivel1L + " despues:" + txtNivel1L.getText());

      if (!antantTxtNivel1L.equals(new String())) { //antes informado
        if (!txtNivel1L.getText().equals(new String())) { //ahora informado
          contenedor.cambioNivelAntesInformado(1);
        }
      }
      antantTxtNivel1L = antTxtNivel1L;
      antTxtNivel1L = txtNivel1L.getText().trim();
    }

  protected void txtDesValueChangeNivel2() {

    //// System_out.println("  txtDesValueChangeNivel2 antes:" + antTxtNivel2L + " despues:" + txtNivel2L.getText());
    if (!antantTxtNivel2L.equals(new String())) { //antes informado
      if (!txtNivel2L.getText().equals(new String())) { //ahora informado
        contenedor.cambioNivelAntesInformado(2);
      }
    }
    antantTxtNivel2L = antTxtNivel2L;
    antTxtNivel2L = txtNivel2L.getText().trim();
  }

  protected void txtDesValueChangeNivel3() {
    //// System_out.println("  txtDesValueChangeNivel3 antes:" + antTxtNivel3L + " despues:" + txtNivel3L.getText());
    if (!antantTxtNivel3L.equals(new String())) { //antes informado
      if (!txtNivel3L.getText().equals(new String())) { //ahora informado
        contenedor.cambioNivelAntesInformado(3);
      }
    }
    antantTxtNivel3L = antTxtNivel3L;
    antTxtNivel3L = txtNivel3L.getText().trim();
  }

  // Operaciones realizadas al pulsar en las cajas de texto.
  protected void txtValueChangeNivel1() {
    if (!bHabilitado) {
      return;
    }
    if (txtNivel1.getText().trim().equals("")) {
      strCDNivel1 = "";
    }
    txtNivel1L.setText("");
    if (tipoOperacion >= TIPO_NIVEL_2) {
      txtNivel2.setText("");
      txtNivel2L.setText("");
      if (tipoOperacion == TIPO_NIVEL_3) {
        txtNivel3.setText("");
        txtNivel3L.setText("");
      }
    }
    modoOperacion = MODO_INICIO;
  }

  protected void txtValueChangeNivel2() {
    if (!bHabilitado) {
      return;
    }
    if (txtNivel1L.getText().equals("")) {
      return;
    }
    if (tipoOperacion >= TIPO_NIVEL_2) {
      if (txtNivel2.getText().trim().equals("")) {
        strCDNivel2 = "";
      }
      txtNivel2L.setText("");
      if (tipoOperacion == TIPO_NIVEL_3) {
        txtNivel3.setText("");
        txtNivel3L.setText("");
      }
    }
    modoOperacion = MODO_N1;
  }

  protected void txtValueChangeNivel3() {
    if (!bHabilitado) {
      return;
    }
    if (txtNivel2L.getText().equals("")) {
      return;
    }
    if (tipoOperacion == TIPO_NIVEL_3) {
      if (txtNivel3.getText().trim().equals("")) {
        strCDNivel3 = "";
      }
      txtNivel3L.setText("");
    }
  }

  // Nivel 1
  protected void btnActionNivel1() {
    DataNivel1 dataDN1 = null;
    DataEntradaEDO dataDEE = null;
    CMessage mensaje = null;

    try {
      if (bAutorizacion) {
        CListaNiveles1 lista = new CListaNiveles1(app,
                                                  "Selecci�n de " +
                                                  app.getNivel1(), stubCliente,
                                                  strSERVLETAut,
                                                  servletOBTENER_X_CODIGO,
                                                  servletOBTENER_X_DESCRIPCION,
                                                  servletSELECCION_X_CODIGO,
            servletSELECCION_X_DESCRIPCION);
        lista.show();
        dataDEE = (DataEntradaEDO) lista.getComponente();
      }
      else {
        CListaNivel1 lista = new CListaNivel1(app,
                                              "Selecci�n de " + app.getNivel1(),
                                              stubCliente,
                                              strSERVLETNivel1,
                                              servletOBTENER_X_CODIGO,
                                              servletOBTENER_X_DESCRIPCION,
                                              servletSELECCION_X_CODIGO,
                                              servletSELECCION_X_DESCRIPCION);
        lista.show();
        dataDN1 = (DataNivel1) lista.getComponente();
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (bAutorizacion) {
      if (dataDEE != null) {
        txtNivel1.setText(dataDEE.getCod());
        txtNivel1L.setText(dataDEE.getDes());
        //borraNivel2();
        modoOperacion = MODO_N1;
      }
      else {
        txtNivel1.setText("");
        txtNivel1L.setText("");
      }
    }
    else {
      if (dataDN1 != null) {
        txtNivel1.setText(dataDN1.getCod());
        txtNivel1L.setText(dataDN1.getDes());
        //borraNivel2();
        modoOperacion = MODO_N1;
      }
      else {
        txtNivel1.setText("");
        txtNivel1L.setText("");
      }
    }
  }

  protected void txtFocusNivel1() {
    DataNivel1 dataDN1 = null;
    DataEntradaEDO dataDEE = null;

    CLista param = null;
    CMessage msg;

    if (!strCDNivel1.equals(txtNivel1.getText())) {

      if (txtNivel1.getText().trim().length() > 0) {
        param = new CLista();
        if (bAutorizacion) {
          param.setPerfil(app.getPerfil());
          param.setLogin(app.getLogin());
          param.addElement(new DataEntradaEDO(txtNivel1.getText()));
        }
        else {
          param.addElement(new DataNivel1(txtNivel1.getText()));
        }
      }

      if (param != null) {
        try {
          if (bAutorizacion) {
            stubCliente.setUrl(new URL(app.getURL() + strSERVLETAut));
          }
          else {
            stubCliente.setUrl(new URL(app.getURL() + strSERVLETNivel1));
          }
          param.setPerfil(app.getPerfil()); //LRG
          param.setLogin(app.getLogin());

          // JRM: Para que funcione correctamente notservlet
          param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
          param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

          param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO, param);

//          notservlets.SrvEntradaEDO srv = new notservlets.SrvEntradaEDO();
//          srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
//                                 "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
//                                 "dba_edo",
//                                 "manager");
//          param = srv.doDebug(servletOBTENER_X_CODIGO, param);

          if (param.size() > 0) {
            if (bAutorizacion) {
              dataDEE = (DataEntradaEDO) param.firstElement();
              txtNivel1L.setText(dataDEE.getDes());
            }
            else {
              dataDN1 = (DataNivel1) param.firstElement();
              txtNivel1L.setText(dataDN1.getDes());
            }
            borraNivel2();
            modoOperacion = MODO_N1;
          }
          else {
            msg = new CMessage(this.app, CMessage.msgAVISO,
                               res.getString("NoDatos"));
            msg.show();
            msg = null;
            txtNivel1.setText("");
            txtNivel1L.setText("");
          }
        }
        catch (Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }
      }
    }
  }

  protected void txtGotFocusNivel1() {
    strCDNivel1 = txtNivel1.getText().trim();
  }

  protected void txtGotFocusNivel2() {
    strCDNivel2 = txtNivel2.getText().trim();
  }

  protected void txtGotFocusNivel3() {
    strCDNivel3 = txtNivel3.getText().trim();
  }

  //********** Nivel 2
   protected void btnActionNivel2() {
     DataZBS2 dataDN2 = null;
     DataEntradaEDO dataDEE = null;
     CMessage msgBox = null;

     try {
       if (bAutorizacion) {
         CListaNiveles2 lista;
         // ARG: Petici�n s�lo con distrito.
         if (!CON_ZBS) {
           lista = new CListaNiveles2(this,
                                      "Selecci�n de " + app.getNivel2(),
                                      stubCliente,
                                      strSERVLETAut,
                                      servletOBTENER_NIV2_X_CODIGO,
                                      servletOBTENER_NIV2_X_DESCRIPCION,
                                      servletSELECCION_NIV2_X_CODIGO,
                                      servletSELECCION_NIV2_X_DESCRIPCION);
         }
         // ARG: Petici�n con distrito/zona b�sica de salud
         else {
           lista = new CListaNiveles2(this,
                                      "Selecci�n de " + app.getNivel2(),
                                      stubCliente,
                                      strSERVLETAut,
                                      servletOBTENER_NIV2_X_CODIGO_CON_ZBS,
                                      servletOBTENER_NIV2_X_DESCRIPCION_CON_ZBS,
                                      servletSELECCION_NIV2_X_CODIGO_CON_ZBS,
               servletSELECCION_NIV2_X_DESCRIPCION_CON_ZBS);
         }

         lista.show();
         dataDEE = (DataEntradaEDO) lista.getComponente();
       }
       else {
         CListaNivel2 lista;
         // ARG: SPetici�n s�lo con distrito.
         if (!CON_ZBS) {
           lista = new CListaNivel2(this,
                                    "Selecci�n de " + app.getNivel2(),
                                    stubCliente,
                                    strSERVLETNivel2,
                                    servletOBTENER_NIV2_X_CODIGO,
                                    servletOBTENER_NIV2_X_DESCRIPCION,
                                    servletSELECCION_NIV2_X_CODIGO,
                                    servletSELECCION_NIV2_X_DESCRIPCION);
         }
         // ARG: Petici�n con distrito/zona b�sica de salud
         else {
           lista = new CListaNivel2(this,
                                    "Selecci�n de " + app.getNivel2(),
                                    stubCliente,
                                    strSERVLETNivel2,
                                    servletOBTENER_NIV2_X_CODIGO_CON_ZBS,
                                    servletOBTENER_NIV2_X_DESCRIPCION_CON_ZBS,
                                    servletSELECCION_NIV2_X_CODIGO_CON_ZBS,
                                    servletSELECCION_NIV2_X_DESCRIPCION_CON_ZBS);
         }
         lista.show();
         dataDN2 = (DataZBS2) lista.getComponente();
       }
     }
     catch (Exception er) {
       er.printStackTrace();
       msgBox = new CMessage(getApp(), CMessage.msgERROR, er.getMessage());
       msgBox.show();
       msgBox = null;
     }

     if (bAutorizacion) {
       if (dataDEE != null) {
         txtNivel2.setText(dataDEE.getCod());
         txtNivel2L.setText(dataDEE.getDes());
         //borraNivel3();
         modoOperacion = MODO_N2;
       }
       else {
         txtNivel2.setText("");
         txtNivel2L.setText("");
       }

     }
     else {
       if (dataDN2 != null) {
         txtNivel2.setText(dataDN2.getNiv2());
         txtNivel2L.setText(dataDN2.getDes());
         //borraNivel3();
         modoOperacion = MODO_N2;
       }
       else {
         txtNivel2.setText("");
         txtNivel2L.setText("");
       }
     }
   }

  protected void txtFocusNivel2() {
    DataZBS2 dataDN2 = null;
    DataEntradaEDO dataDEE = null;
    CLista param = null;
    CMessage msg;

    String Distrito = txtNivel2.getText();

    // El distrito cuando tiene zona b�sica de salud es de la forma XXYY
    // donde XX es el distrito e YY la zona b�sica de salud
    if ( (CON_ZBS) && (Distrito.length() >= 1) && (Distrito.length() < 4)) {
      msg = new CMessage(this.app, CMessage.msgAVISO,
                         res.getString("SelecDZBS.Text"));
      msg.show();
      msg = null;
      return;
    }

    if (!strCDNivel2.equals(txtNivel2.getText().trim())) {
      if (txtNivel2.getText().length() > 0) {
        param = new CLista();
        if (bAutorizacion) {
          param.setPerfil(app.getPerfil());
          param.setLogin(app.getLogin());
          param.addElement(new DataEntradaEDO(txtNivel2.getText(), "",
                                              txtNivel1.getText()));
        }
        else {
          param.addElement(new DataZBS2(txtNivel1.getText(), txtNivel2.getText(),
                                        "", ""));
        }
      }

      if (param != null) {
        try {
          if (bAutorizacion) {
            stubCliente.setUrl(new URL(app.getURL() + strSERVLETAut));
          }
          else {
            stubCliente.setUrl(new URL(app.getURL() + strSERVLETNivel2));
          }
          param.setPerfil(app.getPerfil()); //LRG
          param.setLogin(app.getLogin());

          // JRM: Para que funcione correctamente notservlet
          param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
          param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

          // ARG: Si no hay que introducir ZBS
          if (!CON_ZBS) {
            param = (CLista) stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO,
                                                param);
          }
          // ARG: Si hay que introducir ZBS
          else {
            param = (CLista) stubCliente.doPost(
                servletOBTENER_NIV2_X_CODIGO_CON_ZBS, param);
          }
          /*
               notservlets.SrvEntradaEDO srv = new notservlets.SrvEntradaEDO();
                     srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                                 "dba_edo",
                                 "manager");
               param = srv.doDebug(servletOBTENER_NIV2_X_CODIGO_CON_ZBS, param);
           */

          if (param.size() > 0) {
            if (bAutorizacion) {
              dataDEE = (DataEntradaEDO) param.firstElement();
              txtNivel2.setText(dataDEE.getCod());
              txtNivel2L.setText(dataDEE.getDes());
            }
            else {
              dataDN2 = (DataZBS2) param.firstElement();
              txtNivel2.setText(dataDN2.getNiv2());
              txtNivel2L.setText(dataDN2.getDes());
            }
            borraNivel3();
            modoOperacion = MODO_N2;
          }
          else {
            msg = new CMessage(this.app, CMessage.msgAVISO,
                               res.getString("NoDatos"));
            msg.show();
            msg = null;
            txtNivel2.setText("");
            txtNivel2L.setText("");
          }
        }
        catch (Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }
      }
    }
  }

  // Nivel 3
  protected void btnActionNivel3() {
    DataZBS data = null;
    CMessage msgBox = null;
    CListaNivel3 lista = null;

    try {
      lista = new CListaNivel3(this, "Selecci�n de " + app.getNivel3(),
                               stubCliente, strSERVLETZona,
                               servletOBTENER_NIV2_X_CODIGO,
                               servletOBTENER_NIV2_X_DESCRIPCION,
                               servletSELECCION_NIV2_X_CODIGO,
                               servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtNivel3.setText(data.getCod());
      txtNivel3L.setText(data.getDes());
      modoOperacion = MODO_N3;
    }
    else {
      txtNivel3.setText("");
      txtNivel3L.setText("");
    }
  }

  protected void txtFocusNivel3() {
    DataZBS zbs;
    CLista param = null;
    CMessage msg;

    if (!strCDNivel3.equals(txtNivel3.getText().trim())) {
      modoOperacion = MODO_N2;

      if (txtNivel3.getText().length() > 0) {
        param = new CLista();
        param.addElement(new DataZBS(txtNivel3.getText(), "", "",
                                     txtNivel1.getText(), txtNivel2.getText()));
      }
      if (param != null) {
        try {
          stubCliente.setUrl(new URL(app.getURL() + strSERVLETZona));

          param.setPerfil(app.getPerfil()); //LRG
          param.setLogin(app.getLogin());

          param = (CLista) stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO,
                                              param);

          if (param.size() > 0) {
            zbs = (DataZBS) param.firstElement();
            txtNivel3.setText(zbs.getCod());
            txtNivel3L.setText(zbs.getDes());
            modoOperacion = MODO_N3;
          }
          else {
            msg = new CMessage(this.app, CMessage.msgAVISO,
                               res.getString("NoDatos"));
            msg.show();
            msg = null;
            txtNivel3.setText("");
            txtNivel3L.setText("");
          }
        }
        catch (Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }
      }
    }
  }

} // FIN CLASE panelNiveles

//**************************************************************************
//          Clases para la gesti�n de eventos
//**************************************************************************

// Para comunicar al los contenedores de este panel, que implementan
// el interfaz usaPanelNiveles, que ha habido un cambio en alguno de los niveles
  class PanelNivelesDesTextListener
      implements java.awt.event.TextListener {
    panelNiveles adaptee = null;
    TextEvent e = null;

    PanelNivelesDesTextListener(panelNiveles adaptee) {
      this.adaptee = adaptee;
    }

    public void textValueChanged(java.awt.event.TextEvent e) {
      String name2 = ( (Component) e.getSource()).getName();

      if (name2.equals("desNivel1")) {
        adaptee.txtDesValueChangeNivel1();
      }
      else if (name2.equals("desNivel2")) {
        adaptee.txtDesValueChangeNivel2();
      }
      else if (name2.equals("desNivel3")) {
        adaptee.txtDesValueChangeNivel3();
      }

      adaptee.desbloquea();
    }

  }

class PanelNivelesTextListener
    implements java.awt.event.TextListener {
  panelNiveles adaptee = null;
  TextEvent e = null;

  PanelNivelesTextListener(panelNiveles adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(java.awt.event.TextEvent e) {
    String name2 = ( (Component) e.getSource()).getName();

    if (name2.equals("nivel1")) {
      adaptee.txtValueChangeNivel1();
    }
    else if (name2.equals("nivel2")) {
      adaptee.txtValueChangeNivel2();
    }
    else if (name2.equals("nivel3")) {
      adaptee.txtValueChangeNivel3();
    }

    adaptee.desbloquea();
  }

}

// P�rdida de foco en las cajas de texto
class PanelNivelesTextFocusListener
    implements java.awt.event.FocusListener, Runnable {
  panelNiveles adaptee;
  String name2 = null;
  protected StubSrvBD stub = new StubSrvBD();

  PanelNivelesTextFocusListener(panelNiveles adaptee) {
    this.adaptee = adaptee;
  }

  public void focusGained(FocusEvent e) {
    name2 = ( (Component) e.getSource()).getName();
    if (name2.equals("nivel1")) {
      adaptee.txtGotFocusNivel1();
    }
    else if (name2.equals("nivel2")) {
      adaptee.txtGotFocusNivel2();
    }
    else if (name2.equals("nivel3")) {
      adaptee.txtGotFocusNivel3();
    }
  }

  public void focusLost(FocusEvent e) {
    if (adaptee.bloquea()) {
      name2 = ( (Component) e.getSource()).getName();
      new Thread(this).start();
    }
  }

  public void run() {
    CLista lalista = null;
    String campo = null;
    CLista result = null, data = new CLista();

    try {

      adaptee.Inicializar();
      if (adaptee.modoOperacion == adaptee.MODO_ESPERA) {
        adaptee.modoContenedor = adaptee.contenedor.ponerEnEspera();
      }
      else if (adaptee.modoContenedor != -1) {
        adaptee.contenedor.ponerModo(adaptee.modoContenedor);
      }
      if (name2.equals("nivel1")) {
        adaptee.txtFocusNivel1();
      }
      else if (name2.equals("nivel2")) {
        adaptee.txtFocusNivel2();
      }
      else if (name2.equals("nivel3")) {
        adaptee.txtFocusNivel3();
      }

    }
    catch (Exception exc) {
      exc.printStackTrace();
    }
    adaptee.desbloquea();
  }

} //_______________________________________________ END_CLASS

// Eventos sobre los botones
class PanelNivelesBtnActionListener
    implements ActionListener, Runnable {
  panelNiveles adaptee = null;
  ActionEvent e = null;

  public PanelNivelesBtnActionListener(panelNiveles adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      Thread th = new Thread(this);
      th.start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    String name = e.getActionCommand();
    CLista lalista = null;
    String campo = null;

    adaptee.Inicializar();
    if (adaptee.modoOperacion == adaptee.MODO_ESPERA) {
      adaptee.modoContenedor = adaptee.contenedor.ponerEnEspera();
    }
    else if (adaptee.modoContenedor != -1) {
      adaptee.contenedor.ponerModo(adaptee.modoContenedor);

    }
    if (name.equals("nivel1")) {
      adaptee.btnActionNivel1();
    }
    else if (name.equals("nivel2")) {
      adaptee.btnActionNivel2();
    }
    else if (name.equals("nivel3")) {
      adaptee.btnActionNivel3();
    }

    adaptee.desbloquea();
  }

} //__________________________________________________ END_CLASS

class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a, String title, StubSrvBD stub, String servlet,
                      int obtener_x_codigo, int obtener_x_descricpcion,
                      int seleccion_x_codigo, int seleccion_x_descripcion) {

    super(a, title, stub, servlet, obtener_x_codigo, obtener_x_descricpcion,
          seleccion_x_codigo, seleccion_x_descripcion);

    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

class CListaNivel2
    extends CListaValores {

  protected panelNiveles panel;

  public CListaNivel2(panelNiveles p, String title, StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo, int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {

    super(p.getApp(), title, stub, servlet, obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo, seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtNivel1.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

class CListaNivel3
    extends CListaValores {

  protected panelNiveles panel;

  public CListaNivel3(panelNiveles p, String title, StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo, int obtener_x_descricpcion,
                      int seleccion_x_codigo, int seleccion_x_descripcion) {

    super(p.getApp(), title, stub, servlet, obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo, seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtNivel1.getText(),
                       panel.txtNivel2.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

// Clases empleadas para acceder con bAutorizaciones = true
class CListaNiveles1
    extends CListaValores {

  public CListaNiveles1(CApp a,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descripcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();

  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

class CListaNiveles2
    extends CListaValores {

  protected panelNiveles panel;

  public CListaNiveles2(panelNiveles p,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.txtNivel1.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}
