package notlimpiar;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
//
import eqNot.DataEqNot;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import nivel1.DataNivel1;
import notutil.Comunicador;
import notutil.UtilEDO;
import sapp.StubSrvBD;

public class PanelLimpiar
    extends CPanel {

  protected CCargadorImagen imgs = null;
  ResourceBundle res;

  //Modos de operaci�n de la ventana
  final int modoALTA = 0;
  final int modoESPERA = 2;

  // para no disparar dos veces el focuslost
  boolean bFoco1 = false;

  // variables booleanas que indican si el contenido de los campos
  // respectivos es correcto
  protected boolean bNivel1Valid = false;

  private boolean bNivel1SelDePopup = false;

  final String strSERVLET_NOT_BORRAR = "servlet/SrvNotBorrar";

  /////////////
  final String strSERVLET_Nivel1 = "servlet/SrvNivel1";

  final int servletOBTENER_X_N1_CODIGO = 3;
  final int servletOBTENER_X_N1_DESCRIPCION = 4;
  final int servletSELECCION_X_N1_CODIGO = 5;
  final int servletSELECCION_X_N1_DESCRIPCION = 6;

  final String strSERVLET_Equipo = "servlet/SrvEqLimp";

  final int servletOBTENER_X_CODIGO = 7;
  final int servletOBTENER_X_DESCRIPCION = 8;
  final int servletSELECCION_X_CODIGO = 9;
  final int servletSELECCION_X_DESCRIPCION = 10;

  /////////////

  // modos de operaci�n del servlet
  final int servletBORRAR_NO_RS = 0;
  final int servletSELECT_SI_RS = 1;
  final int servletBORRAR_SI_RS = 2;

  XYLayout xYLayout1 = new XYLayout();
  Label lblCero = new Label();
  ButtonControl btnBorrarNoRS = new ButtonControl();
  Label lblNoRS = new Label();
  ButtonControl btnBuscar = new ButtonControl();

  PanelLimpiar_btn_actionAdapter btn_actionAdapter = null;
  focusAdapter _focusAdapter = null;
  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);
  focusAdapter txtFocusAdapter = new focusAdapter(this);
  BevelPanel bevelPanel1 = new BevelPanel();
  BevelPanel bevelPanel2 = new BevelPanel();
  Label lblSiRS = new Label();

  /////////////
  // modificacion jlt 22/11/2001
  // introducimos campos de filtrado
  // area, ano, semana, equipo notificador
  Label lblNivel1 = new Label();
  TextField txtNivel1 = new TextField();
  ButtonControl btnNivel1 = new ButtonControl();

  TextField txtDesNivel1 = new TextField();

  Label lblAnno = new Label();
  TextField txtAnno = new TextField();

  Label lblSemana = new Label();
  TextField txtSemana = new TextField();

  Label lblNotif = new Label();
  CCampoCodigo txtEquipo = new CCampoCodigo();
  ButtonControl btnEquipo = new ButtonControl();

  TextField txtDesEquipo = new TextField();

  ///////////////////

  ButtonControl btnBorrar = null;
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  public CTabla tabla = new CTabla();

  // Stub's
  protected StubSrvBD stubCliente = new StubSrvBD();

  // par�metros
  protected int modoOperacion = modoALTA;
  protected int modoAnt = modoALTA;

  public CLista listaNotificaciones = null;

  public PanelLimpiar(CApp a) {
    try {
      this.app = (CApp) a;
      res = ResourceBundle.getBundle("notlimpiar.Res" + a.getIdioma());
      listaNotificaciones = new CLista();
      //stubCliente = new StubSrvBD(new URL(this.app.getURL() + strSERVLET_NOT_BORRAR));
      //stubClientenivel1 = new StubSrvBD(new URL(this.app.getURL() + strSERVLET_NOT_BORRAR));

      jbInit();

      // inicializamos las variables seleccionadas
      this.txtNivel1.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesNivel1.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtAnno.setText(this.app.getANYO_DEFECTO());

    }
    catch (Exception e) {
      ;
    }
  }

  void jbInit() throws Exception {

    final String imgNAME[] = {
        "images/refrescar.gif",
        "images/baja2.gif",
        "images/primero.gif",
        "images/anterior.gif",
        "images/siguiente.gif",
        "images/ultimo.gif",
        "images/Magnify.gif"};

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout1.setHeight(350); //278
    xYLayout1.setWidth(671);
    this.setLayout(xYLayout1);

    lblCero.setFont(new Font("Dialog", 1, 12));
    lblCero.setText(res.getString("lblCero.Text"));
    lblNoRS.setText(res.getString("lblNoRS.Text"));

    /////////////
    lblNivel1.setText(res.getString("lblNivel1.Text"));
    lblAnno.setText(res.getString("lblAnno.Text"));
    lblSemana.setText(res.getString("lblSemana.Text"));
    lblNotif.setText(res.getString("lblNotif.Text"));

    txtNivel1.setBackground(new Color(255, 255, 150));
    txtAnno.setBackground(new Color(255, 255, 150));
    /////////////

    btnBuscar = new ButtonControl(imgs.getImage(0));
    btnBorrar = new ButtonControl(imgs.getImage(1));
    btnBorrarNoRS = new ButtonControl(imgs.getImage(1));
    btnPrimero = new ButtonControl(imgs.getImage(2));
    btnAnterior = new ButtonControl(imgs.getImage(3));
    btnSiguiente = new ButtonControl(imgs.getImage(4));
    btnUltimo = new ButtonControl(imgs.getImage(5));

    ////////////
    btnEquipo = new ButtonControl(imgs.getImage(6));
    btnNivel1 = new ButtonControl(imgs.getImage(6));
    ////////////

    bevelPanel2.setBevelOuter(BevelPanel.LOWERED);
    lblSiRS.setText(res.getString("lblSiRS.Text"));
    bevelPanel2.setBevelInner(BevelPanel.FLAT);

    btnBuscar.setActionCommand("buscar");
    btnBorrarNoRS.setActionCommand("borrarNoRS");
    btnBorrar.setActionCommand("borrar");
    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");

    ////////////////
    btnNivel1.setActionCommand("nivel1");
    btnEquipo.setActionCommand("equipo");

    ////////////////

    btn_actionAdapter = new PanelLimpiar_btn_actionAdapter(this);
    txtNivel1.addActionListener(new PanelLimpiar_txtNivel1_actionAdapter(this));

    txtNivel1.addFocusListener(txtFocusAdapter);
    txtEquipo.addFocusListener(txtFocusAdapter);

    ///////////
    txtNivel1.setName("nivel1");
    txtEquipo.setName("equipo");
    ///////////

    btnBorrarNoRS.addActionListener(btn_actionAdapter);
    btnBuscar.addActionListener(btn_actionAdapter);

    btnBuscar.setLabel(res.getString("btnBuscar.Label"));
    btnBorrarNoRS.setLabel(res.getString("btnBorrarNoRS.Label"));

    btnBuscar.addActionListener(btn_actionAdapter);
    btnBorrar.addActionListener(btn_actionAdapter);
    btnPrimero.addActionListener(btn_actionAdapter);
    btnAnterior.addActionListener(btn_actionAdapter);
    btnSiguiente.addActionListener(btn_actionAdapter);
    btnUltimo.addActionListener(btn_actionAdapter);

    ///////////////
    btnNivel1.addActionListener(btn_actionAdapter);
    btnEquipo.addActionListener(btn_actionAdapter);
    //////////////

    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "104\n53\n60\n137\n189\n100"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnButtonsStrings")), '\n'));
    tabla.setNumColumns(6);

    bevelPanel1.setBevelOuter(BevelPanel.LOWERED);
    bevelPanel1.setBevelInner(BevelPanel.FLAT);

    tabla.addActionListener(new PanelLimpiar_tabla_dobleClick(this));

    ////////////
    txtDesNivel1.setEditable(false);
    txtDesEquipo.setEditable(false);
    txtDesNivel1.setEnabled(false);
    txtDesEquipo.setEnabled(false);
    ////////////

    this.add(lblCero, new XYConstraints(9, 8, 301, -1));
    this.add(btnBuscar, new XYConstraints(579, 129, -1, -1));
    this.add(tabla, new XYConstraints(6, 164, 659, 124));
    //this.add(btnBorrarNoRS, new XYConstraints(393, 53, -1, -1));
    this.add(btnBorrar, new XYConstraints(9, 299, 25, 25));
    this.add(btnPrimero, new XYConstraints(507, 299, 25, 25));
    this.add(btnAnterior, new XYConstraints(547, 299, 25, 25));
    this.add(btnSiguiente, new XYConstraints(585, 299, 25, 25));
    this.add(btnUltimo, new XYConstraints(623, 299, 25, 25));
    this.add(bevelPanel2, new XYConstraints(7, 36, 660, 4));
    this.add(lblSiRS, new XYConstraints(9, 41, 357, 29));
    //this.add(lblNoRS, new XYConstraints(11, 55, 351, -1));
    //this.add(bevelPanel1, new XYConstraints(11, 88, 656, 3));

    //////////////////////
    this.add(lblNivel1, new XYConstraints(9, 70, 50, -1));
    this.add(txtNivel1, new XYConstraints(89, 70, 50, -1));
    this.add(btnNivel1, new XYConstraints(150, 70, -1, -1));
    this.add(txtDesNivel1, new XYConstraints(191, 70, 330, -1));
    this.add(lblAnno, new XYConstraints(9, 100, 50, -1));
    this.add(txtAnno, new XYConstraints(90, 100, 50, -1));
    this.add(lblSemana, new XYConstraints(196, 100, 142, -1));
    this.add(txtSemana, new XYConstraints(338, 100, 27, -1));
    this.add(lblNotif, new XYConstraints(9, 133, 68, -1));
    this.add(txtEquipo, new XYConstraints(90, 133, 50, -1));
    this.add(btnEquipo, new XYConstraints(150, 133, -1, -1));
    this.add(txtDesEquipo, new XYConstraints(192, 133, 330, -1));
    //////////////////////

    /*this.add(lblCero, new XYConstraints(9, 8, 301, -1));
         this.add(btnBorrarNoRS, new XYConstraints(393, 53, -1, -1));
         this.add(btnBuscar, new XYConstraints(579, 102, -1, -1));
         this.add(tabla, new XYConstraints(6, 148, 659, 124));
         this.add(lblNoRS, new XYConstraints(11, 55, 351, -1));
         this.add(btnBorrar, new XYConstraints(9, 283, 25, 25));
         this.add(btnPrimero, new XYConstraints(507, 283, 25, 25));
         this.add(btnAnterior, new XYConstraints(547, 283, 25, 25));
         this.add(btnSiguiente, new XYConstraints(585, 283, 25, 25));
         this.add(btnUltimo, new XYConstraints(623, 283, 25, 25));
         this.add(bevelPanel1, new XYConstraints(11, 88, 656, 3));
         this.add(bevelPanel2, new XYConstraints(7, 38, 660, 4));
         this.add(lblSiRS, new XYConstraints(15, 102, 357, 29));*/

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    modoOperacion = modoALTA;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:
        btnBorrarNoRS.setEnabled(true);
        btnBuscar.setEnabled(true);
        ////////
        txtAnno.setEnabled(true);
        txtNivel1.setEnabled(true);
        txtSemana.setEnabled(true);
        txtEquipo.setEnabled(true);
        btnNivel1.setEnabled(true);
        btnEquipo.setEnabled(true);
        ////////
        if (tabla.countItems() > 0) {
          btnBorrar.setEnabled(true);
          btnPrimero.setEnabled(true);
          btnAnterior.setEnabled(true);
          btnSiguiente.setEnabled(true);
          btnUltimo.setEnabled(true);

        }
        else {
          btnBorrar.setEnabled(false);
          btnPrimero.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);
        }
        tabla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.doLayout();
        break;

      case modoESPERA:
        btnBorrarNoRS.setEnabled(false);
        btnBorrar.setEnabled(false);
        btnBuscar.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        tabla.setEnabled(false);
        ////////
        txtAnno.setEnabled(false);
        txtNivel1.setEnabled(false);
        txtSemana.setEnabled(false);
        txtEquipo.setEnabled(false);
        btnNivel1.setEnabled(false);
        btnEquipo.setEnabled(false);
        ////////
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.doLayout();
        break;

    }
    this.doLayout();
  }

  public void btnBorrar_actionPerformed() {

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();
    try {

      int iSel = tabla.getSelectedIndex();
      if (iSel != BWTEnum.NOTFOUND) {
        // se solicita la confirmaci�n de la baja
        CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                       res.getString("msg2.Text"));
        msgBox.show();

        if (msgBox.getResponse()) {

          DataNotificaciones dataEntrada = new DataNotificaciones();
          dataEntrada = (DataNotificaciones) listaNotificaciones.elementAt(iSel);
          CLista listaEntrada = new CLista();
          listaEntrada.addElement(dataEntrada);
          CLista result = new CLista();
          result = Comunicador.Communicate(this.app,
                                           stubCliente,
                                           servletBORRAR_SI_RS,
                                           strSERVLET_NOT_BORRAR,
                                           listaEntrada);

          ShowWarning(res.getString("msg3.Text"));
          listaNotificaciones.removeElementAt(iSel);
          writeListaNotificaciones();

          ///---

        }
      }
      else {
        ShowWarning(res.getString("msg4.Text"));
      }

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      ShowWarning(res.getString("msg5.Text"));
    }

    modoOperacion = modo;
    Inicializar();

  } //fin de borrar

  void btnBorrarNoRS_actionPerformed() {
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    // se solicita la confirmaci�n de la baja
    CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                   res.getString("msg2.Text"));
    msgBox.show();

    if (msgBox.getResponse()) {
      CLista parametros = new CLista();
      parametros.addElement(new DataNotificaciones());

      CLista result = new CLista();

      try {

        result = Comunicador.Communicate(this.app,
                                         stubCliente,
                                         servletBORRAR_NO_RS,
                                         strSERVLET_NOT_BORRAR,
                                         parametros);

        //si mal: null
        //si bien:  String -- n� de notifedo borradas
        //          String -- n� de notif_sem borradas
        if (result != null) {
          String notifedo = (String) result.elementAt(0);
          String notifSem = (String) result.elementAt(1);

          ShowWarning(res.getString("msg6.Text") + notifedo +
                      res.getString("msg7.Text") + notifSem +
                      res.getString("msg8.Text"));
        }
        else {
          ShowWarning(res.getString("msg9.Text"));
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
        ShowWarning(res.getString("msg9.Text"));
      }
    } //fin de SI

    modoOperacion = modo;
    Inicializar();
  }

// Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

/////////////////////

  // m�todo que se ejecuta en respuesta al focuslost en txtNivel1
  void txtNivel1_focusLost() {

    // datos de envio
    DataNivel1 nivel1;

    DataEqNot eqnot;
    ;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    if (this.txtNivel1.getText().length() > 0) {

      // gestion de datos

      param = new CLista();
      param.setIdioma(app.getIdioma());
      //// modificacion jlt
      param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
      param.addElement(new DataNivel1(txtNivel1.getText()));
      strServlet = strSERVLET_Nivel1;
      modoServlet = servletOBTENER_X_N1_CODIGO;

      // busca el item
      if (param != null) {

        try {
          // consulta en modo espera
          modoOperacion = modoESPERA;
          Inicializar();

          stubCliente.setUrl(new URL(app.getURL() + strServlet));
          param = (CLista) stubCliente.doPost(modoServlet, param);

          /*nivel1.SrvNivel1 srv = new nivel1.SrvNivel1();
                srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                  "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                  "sive_desa",
                                  "sive_desa");
                param = srv.doDebug(modoServlet, param);*/

          // rellena los datos
          if (param.size() > 0) {

            nivel1 = (DataNivel1) param.firstElement();
            txtNivel1.removeKeyListener(txtKeyAdapter);
            txtNivel1.setText(nivel1.getCod());
            txtDesNivel1.setText(nivel1.getDes());
            txtNivel1.addKeyListener(txtKeyAdapter);
          }
          // no hay datos
          else {
            msg = new CMessage(this.app, CMessage.msgAVISO,
                               res.getString("msg22.Text"));
            msg.show();
            msg = null;
          }

        }
        catch (Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }

        // consulta en modo normal
        modoOperacion = modoALTA;
        Inicializar();
      }

    } // if del txtnivel1

  }

  void txtEquipo_focusLost() {

    // datos de envio
    DataEqNot data = null;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    if (this.txtEquipo.getText().length() > 0) {

      // gestion de datos

      param = new CLista();
      param.setIdioma(app.getIdioma());
      //param.addElement(new DataNivel1(txtNivel1.getText()));
      strServlet = strSERVLET_Equipo;
      modoServlet = servletOBTENER_X_CODIGO;
      data = new DataEqNot(this.txtEquipo.getText(), "", "",
                           this.txtNivel1.getText(), "", "", "", "", "", "",
                           "", 0, this.app.getLogin(), "", false);
      param.addElement( (DataEqNot) data);

      // busca el item
      if (param != null) {

        try {
          // consulta en modo espera
          modoOperacion = modoESPERA;
          Inicializar();

          stubCliente.setUrl(new URL(app.getURL() + strServlet));
          param = (CLista) stubCliente.doPost(modoServlet, param);

          /* SrvEqLimp srv = new SrvEqLimp();
                srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
                                "pista",
                                "loteb98");
                param = srv.doDebug(modoServlet, param);
           */

          // rellena los datos
          if (param.size() > 0) {

            data = (DataEqNot) param.firstElement();
            txtEquipo.removeKeyListener(txtKeyAdapter);
            txtEquipo.setText(data.getEquipo());
            txtDesEquipo.setText(data.getDesEquipo());
            txtEquipo.addKeyListener(txtKeyAdapter);
          }
          // no hay datos
          else {
            msg = new CMessage(this.app, CMessage.msgAVISO,
                               res.getString("msg22.Text"));
            msg.show();
            msg = null;
          }

        }
        catch (Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }

        // consulta en modo normal
        modoOperacion = modoALTA;
        Inicializar();
      }

    } // if del txtequipo

  }

////////////////////////
  public void btnNivel1_actionPerformed() {
    //Busca los niveles1 (area de salud)
    //void btnCtrlbuscarAre_actionPerformed(ActionEvent evt){

    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg19.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLET_Nivel1,
                                            servletOBTENER_X_N1_CODIGO,
                                            servletOBTENER_X_N1_DESCRIPCION,
                                            servletSELECCION_X_N1_CODIGO,
                                            servletSELECCION_X_N1_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception ex) {
      ex.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtNivel1.removeKeyListener(txtKeyAdapter);
      txtNivel1.setText(data.getCod());
      txtDesNivel1.setText(data.getDes());
      txtNivel1.addKeyListener(txtKeyAdapter);

//      txtCodAre.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoALTA;
    Inicializar();
  }

  void btnEquipo_actionPerformed() {

    DataEqNot data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();
      CListaEquipos lista = null;

      lista = new CListaEquipos(this,
                                res.getString("msg20.Text"),
                                stubCliente,
                                strSERVLET_Equipo,
                                servletOBTENER_X_CODIGO,
                                servletOBTENER_X_DESCRIPCION,
                                servletSELECCION_X_CODIGO,
                                servletSELECCION_X_DESCRIPCION);

      lista.show();
      data = (DataEqNot) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtEquipo.removeKeyListener(txtKeyAdapter);
      txtEquipo.setText(data.getEquipo());
      txtDesEquipo.setText(data.getDesEquipo());
      txtEquipo.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoALTA;
    Inicializar();
  }

  public boolean isDataValid() {
    boolean bDatos = true;

    if ( (this.txtNivel1.getText().length() == 0) ||
        (this.txtAnno.getText().length() == 0)) {

      bDatos = false;

    }
    return bDatos;
  }

////////////////////////

  void btnBuscar_actionPerformed() {

    int modo = modoOperacion;
    CMessage msgBox;
    String anno = "";
    String area = "";
    String semana = "";
    String equipo = "";

    DataNotificaciones data = null;

    /////////////////
    if (isDataValid()) {

      if (!this.txtAnno.getText().equals("")) {
        anno = this.txtAnno.getText();

      }
      if (!this.txtNivel1.getText().equals("")) {
        area = this.txtNivel1.getText();

      }
      if (!this.txtSemana.getText().equals("")) {
        semana = this.txtSemana.getText();

      }
      if (!this.txtEquipo.getText().equals("")) {
        equipo = this.txtEquipo.getText();

        // rellenamos el data con la informaci�n
      }
      data = new DataNotificaciones(equipo, area, "", "", anno, semana,
                                    "", "", "", "", "", "", this.app.getLogin(),
                                    "");

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();

      CLista parametros = new CLista();
      listaNotificaciones = new CLista();

      parametros.addElement( (DataNotificaciones) data);

      try {

        listaNotificaciones = Comunicador.Communicate(this.app,
            stubCliente,
            servletSELECT_SI_RS,
            strSERVLET_NOT_BORRAR,
            parametros);

        if (listaNotificaciones != null) {
          if (listaNotificaciones.size() > 0) {
            writeListaNotificaciones();
          }
          else {
            ShowWarning(res.getString("msg10.Text"));
          }
        }
        else {
          ShowWarning(res.getString("msg11.Text"));
        }

      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        ShowWarning(res.getString("msg12.Text"));
      }

      modoOperacion = modo;
      Inicializar();

      //modoOperacion = modoNORMAL;
      //Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg23.Text"));
      msgBox.show();
      msgBox = null;
    }
    /////////////////

  }

  public void writeListaNotificaciones() {

    DataNotificaciones data = new DataNotificaciones();
    String cadLinea = "";
    JCVector row1 = new JCVector();
    JCVector items = new JCVector();
    tabla.clear();

    String cd_e_notif = "";
    String cd_nivel_1 = "";
    String cd_nivel_2 = "";
    String cd_zbs = "";
    String cd_anoepi = "";
    String cd_semepi = "";
    String fc_recep = "";
    String fc_notif = "";
    String cd_ope = "";
    String fc_ultact = "";
    String cd_centro = "";
    String nm_nnotift = "";
    String nm_ntotreal = "";
    String nm_nnotifr = "";

    // vuelca la lista
    for (int j = 0; j < listaNotificaciones.size(); j++) {
      data = (DataNotificaciones) listaNotificaciones.elementAt(j);
      row1 = new JCVector();

      if (data.getCD_E_NOTIF() == null) {
        cd_e_notif = "";
      }
      else {
        cd_e_notif = data.getCD_E_NOTIF().trim();

      }
      if (data.getCD_NIVEL_1() == null) {
        cd_nivel_1 = "";
      }
      else {
        cd_nivel_1 = data.getCD_NIVEL_1().trim();
      }
      if (data.getCD_NIVEL_2() == null) {
        cd_nivel_2 = "";
      }
      else {
        cd_nivel_2 = data.getCD_NIVEL_2().trim();
      }
      if (data.getCD_ZBS() == null) {
        cd_zbs = "";
      }
      else {
        cd_zbs = data.getCD_ZBS();
      }
      if (data.getCD_ANOEPI() == null) {
        cd_anoepi = "";
      }
      else {
        cd_anoepi = data.getCD_ANOEPI().trim();
      }
      if (data.getCD_SEMEPI() == null) {
        cd_semepi = "";
      }
      else {
        cd_semepi = data.getCD_SEMEPI().trim();
      }
      if (data.getCD_OPE_NOTIFEDO() == null) {
        cd_ope = "";
      }
      else {
        cd_ope = data.getCD_OPE_NOTIFEDO().trim();
      }
      if (data.getFC_ULTACT_NOTIFEDO() == null) {
        fc_ultact = "";
      }
      else {
        fc_ultact = data.getFC_ULTACT_NOTIFEDO().trim();
      }
      if (data.getFC_RECEP() == null) {
        fc_recep = "";
      }
      else {
        fc_recep = data.getFC_RECEP().trim();
      }
      if (data.getFC_FECNOTIF() == null) {
        fc_notif = "";
      }
      else {
        fc_notif = data.getFC_FECNOTIF().trim();
      }
      if (data.getCD_CENTRO() == null) {
        cd_centro = "";
      }
      else {
        cd_centro = data.getCD_CENTRO().trim();
      }
      if (data.getNM_NNOTIFT() == null) {
        nm_nnotift = "";
      }
      else {
        nm_nnotift = data.getNM_NNOTIFT().trim();
      }
      if (data.getNM_NTOTREAL() == null) {
        nm_ntotreal = "";
      }
      else {
        nm_ntotreal = data.getNM_NTOTREAL().trim();
      }
      if (data.getNM_NNOTIFR() == null) {
        nm_nnotifr = "";
      }
      else {
        nm_nnotifr = data.getNM_NNOTIFR().trim();

      }

      UtilEDO ceros = new UtilEDO();

      row1.addElement(cd_e_notif + "  " + cd_nivel_1 + "-" + cd_nivel_2 + "-" +
                      cd_zbs);
      row1.addElement(cd_centro);
      row1.addElement(cd_anoepi + "-" + cd_semepi);
      row1.addElement(fc_recep + "-" + fc_notif);
      row1.addElement(fc_ultact + "-" + cd_ope);
      row1.addElement(ceros.Formatear_Numeros(3, nm_nnotift) + "-" +
                      ceros.Formatear_Numeros(3, nm_nnotifr) + "-" +
                      ceros.Formatear_Numeros(3, nm_ntotreal));
      items.addElement(row1);

    }
    tabla.setItems(items);
    tabla.repaint();

  }

  void txtNivel1_actionPerformed(ActionEvent e) {

  }

/////////////
  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {

    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("nivel1")) &&
        (txtDesNivel1.getText().length() > 0)) {
      txtDesNivel1.setText("");
      txtEquipo.setText("");
      txtDesEquipo.setText("");
    }
    else if ( (txt.getName().equals("equipo")) &&
             (txtDesEquipo.getText().length() > 0)) {
      txtDesEquipo.setText("");

    }

    Inicializar();
  }
////////////

} // END CLASS

////////////
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_n1_codigo,
                      int obtener_x_n1_descricpcion,
                      int seleccion_x_n1_codigo,
                      int seleccion_x_n1_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_n1_codigo,
          obtener_x_n1_descricpcion,
          seleccion_x_n1_codigo,
          seleccion_x_n1_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

class CListaEquipos
    extends CListaValores {

  protected PanelLimpiar panel;
  private String sComunidad = "M";

  private String fechaNotificacion = "";

  /**
   * Acceso a los datos de los equipos notificadores.
   * @return Una lista con los datos de los equipos. Es de la clase base.
   */
  public CLista getLista() {
    return lista;
  }

  public CListaEquipos(PanelLimpiar p,
                       String title,
                       StubSrvBD stub,
                       String servlet,
                       int obtener_x_codigo,
                       int obtener_x_descripcion,
                       int seleccion_x_codigo,
                       int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
//     this.sComunidad = sComunidad;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {

    return new DataEqNot(s, "", "", panel.txtNivel1.getText(), "", "", "", "",
                         "", "",
                         "", 0, this.app.getLogin(), "", false);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEqNot) o).getEquipo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEqNot) o).getDesEquipo());
  }
}

////////////

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanelLimpiar adaptee;
  FocusEvent evt;

  focusAdapter(PanelLimpiar adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("nivel1")) {
      adaptee.txtNivel1_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("equipo")) {
      adaptee.txtEquipo_focusLost();
    }

  }
}

class PanelLimpiar_btn_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelLimpiar adaptee;
  ActionEvent e;

  PanelLimpiar_btn_actionAdapter(PanelLimpiar adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    int index = 0;

    if (e.getActionCommand() == "borrarNoRS") {
      adaptee.btnBorrarNoRS_actionPerformed();
    }
    else if (e.getActionCommand() == "buscar") { // lista principal
      adaptee.btnBuscar_actionPerformed();
    }
    else if (e.getActionCommand() == "nivel1") {
      adaptee.btnNivel1_actionPerformed();
    }
    else if (e.getActionCommand() == "equipo") {
      adaptee.btnEquipo_actionPerformed();
    }
    else if (e.getActionCommand() == "borrar") {
      adaptee.btnBorrar_actionPerformed();
    }
    else if (e.getActionCommand() == "primero") {
      if (adaptee.tabla.countItems() > 0) {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "anterior")
             && (adaptee.tabla.countItems() > 0)) {
      index = adaptee.tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        adaptee.tabla.select(index);
        if (index - adaptee.tabla.getTopRow() <= 0) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() - 1);
        }
      }
      else {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "siguiente")
             && ( (adaptee.tabla.countItems() > 0))) {
      int ultimo = adaptee.tabla.countItems() - 1;
      index = adaptee.tabla.getSelectedIndex();
      if (index < ultimo && index >= 0) {
        index++;
        adaptee.tabla.select(index);
        if (index - adaptee.tabla.getTopRow() >= 4) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() + 1);
        }
      }
      else {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "ultimo")
             && (adaptee.tabla.countItems() > 0)) {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (ultimo >= 0) {
        index = ultimo;
        adaptee.tabla.select(index);
        if (adaptee.tabla.countItems() >= 4) {
          adaptee.tabla.setTopRow(adaptee.tabla.countItems() - 4);
        }
        else {
          adaptee.tabla.setTopRow(0);
        }
      }
    }
  }
}

// escuchador de los click en la tabla
class PanelLimpiar_tabla_dobleClick
    implements jclass.bwt.JCActionListener, Runnable {
  PanelLimpiar adaptee;
  JCActionEvent e;

  PanelLimpiar_tabla_dobleClick(PanelLimpiar adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btnBorrar_actionPerformed();

  }
}

class PanelLimpiar_txtNivel1_actionAdapter
    implements java.awt.event.ActionListener {
  PanelLimpiar adaptee;

  PanelLimpiar_txtNivel1_actionAdapter(PanelLimpiar adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.txtNivel1_actionPerformed(e);
  }
}

//clase para controlar los eventos en las cajas
class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanelLimpiar adaptee;

  txt_keyAdapter(PanelLimpiar adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
