package notlimpiar;

import java.io.Serializable;

public class DataNotificaciones
    implements Serializable {

  public String CD_E_NOTIF = "";
  public String CD_NIVEL_1 = "";
  public String CD_NIVEL_2 = "";
  public String CD_ZBS = "";
  public String CD_ANOEPI = "";
  public String CD_SEMEPI = "";
  public String CD_CENTRO = "";
  public String NM_NNOTIFT = "";
  public String NM_NTOTREAL = "";
  public String NM_NNOTIFR = "";
  public String FC_RECEP = "";
  public String FC_FECNOTIF = "";
  public String CD_OPE_NOTIFEDO = "";
  public String FC_ULTACT_NOTIFEDO = "";

  public DataNotificaciones(String cd_e_notif,
                            String cd_nivel_1, String cd_nivel_2,
                            String cd_zbs, String cd_anoepi,
                            String cd_semepi, String cd_centro,
                            String nm_nnotift,
                            String nm_ntotreal, String nm_nnotifr,
                            String fc_recep, String fc_fecnotif,
                            String cd_ope_notifedo, String fc_ultact_notifedo) {
    CD_E_NOTIF = cd_e_notif;
    CD_NIVEL_1 = cd_nivel_1;
    CD_NIVEL_2 = cd_nivel_2;
    CD_ZBS = cd_zbs;
    CD_ANOEPI = cd_anoepi;
    CD_SEMEPI = cd_semepi;
    CD_CENTRO = cd_centro;
    NM_NNOTIFT = nm_nnotift;
    NM_NTOTREAL = nm_ntotreal;
    NM_NNOTIFR = nm_nnotifr;
    FC_RECEP = fc_recep;
    FC_FECNOTIF = fc_fecnotif;
    CD_OPE_NOTIFEDO = cd_ope_notifedo;
    FC_ULTACT_NOTIFEDO = fc_ultact_notifedo;

  }

  public DataNotificaciones() {}

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getCD_NIVEL_1() {
    return CD_NIVEL_1;
  }

  public String getCD_NIVEL_2() {
    return CD_NIVEL_2;
  }

  public String getCD_ZBS() {
    return CD_ZBS;
  }

  public String getCD_ANOEPI() {
    return CD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return CD_SEMEPI;
  }

  public String getCD_CENTRO() {
    return CD_CENTRO;
  }

  public String getNM_NNOTIFT() {
    return NM_NNOTIFT;
  }

  public String getNM_NTOTREAL() {
    return NM_NTOTREAL;
  }

  public String getNM_NNOTIFR() {
    return NM_NNOTIFR;
  }

  public String getFC_RECEP() {
    return FC_RECEP;
  }

  public String getFC_FECNOTIF() {
    return FC_FECNOTIF;
  }

  public String getCD_OPE_NOTIFEDO() {
    return CD_OPE_NOTIFEDO;
  }

  public String getFC_ULTACT_NOTIFEDO() {
    return FC_ULTACT_NOTIFEDO;
  }

}
