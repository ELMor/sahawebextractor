//Title:        Gr�fico comparativo de una enfermedad
//Version:
//Copyright:    Copyright (c) 1999
//Author:       Norsistemas
//Company:      NorSistemas
//Description:

package GrafComEnf;

import capp.CApp;

public class LanzaGrafCom
    extends CApp {

  public LanzaGrafCom() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    setTitulo("Gr�fico comparativo de una enfermedad");
    CApp a = (CApp)this;
    panGrafCom panelCons = new panGrafCom(a);
    VerPanel("Gr�fico comparativo de una enfermedad", panelCons, false);
    VerPanel("Gr�fico comparativo de una enfermedad");
  }
}
