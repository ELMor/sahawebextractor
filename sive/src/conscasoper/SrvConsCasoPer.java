package conscasoper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

// LORTAD
import Registro.RegistroConsultas;
import capp.CLista;
import sapp.DBServlet;

public class SrvConsCasoPer
    extends DBServlet {

  // informes
  final int erwCASOS_EDO_PERIODO = 1;

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    String where = null;
    ResultSet rs = null;

    // control
    int i;
    int iEstado = CLista.listaVACIA;
    String sCodigos = null;
    String sTipo = null;

    // parametros de consulta
    DataC13 paramC1;
    int maxNumEnf = 0;
    DataInfC13 miRegistro = null;

    // buffers
    String sCod;
    int iNum;
    DataC13 dat = null;
    DataC13 c1;
    String strCD_ENFCIE = null;

    //  Hashtable casosEnfermedades = null; // E

    // subqueries
    String queryEq = null;
    String queryCN = null;
    String queryEnf = null;
    String queryEDO = null;
    String queryEDOind = null;

    // objetos de datos
    CLista data = new CLista();
    Vector agrup = new Vector();
    Vector registros = new Vector();
    Vector totales = new Vector();
    Vector vectorCentros = new Vector();
    Vector vectorEquipos = new Vector();
    Vector vectorEnfermedades = new Vector(); // E
//    Vector vectorCasosEnfermedades = null; // E

    String cadenaEquipos = null;
    String codAux = null;

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    user = param.getLogin();
    lortad = param.getLortad();

    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
     }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    paramC1 = (DataC13) param.firstElement();
    maxNumEnf = paramC1.iNumEnf;
    maxNumEnf--;

    // modos de operaci�n
    switch (opmode) {

      // consulta sobre resumen edo
      case erwCASOS_EDO_PERIODO:

        // E Recuperar c�digos de enfermedad
        int ku = 0;
        String strIN = "";
        for (ku = 0; ku < paramC1.listaEnfermedades.size(); ku++) {
          vectorEnfermedades.addElement( (String) ( (Hashtable) paramC1.
              listaEnfermedades.elementAt(ku)).get("CD_ENFCIE"));
        }

        strIN = new String(
            " and NM_EDO in (select NM_EDO from SIVE_EDOIND where CD_ENFCIE in(");
        for (ku = 0; ku < vectorEnfermedades.size() - 1; ku++) {
          strIN = new String(strIN + "'" +
                             (String) vectorEnfermedades.elementAt(ku) + "'" +
                             ",");
        }
        strIN = new String(strIN + "'" +
                           (String) vectorEnfermedades.elementAt(ku) + "'" +
                           ")");

        if (paramC1.sProvincia.trim().length() > 0) {
          if (paramC1.sMunicipio.trim().length() > 0) {
            strIN = new String(strIN + " and CD_PROV = '"
                               + paramC1.sProvincia.trim() + "' and CD_MUN = '"
                               + paramC1.sMunicipio.trim() + "')");
          }
          else {
            strIN = new String(strIN + " and CD_PROV = '"
                               + paramC1.sProvincia.trim() + "')");
          }
        }
        else {
          strIN = new String(strIN + ")");
        }

        // Ahora los equipos que tratan, al menos, una de esas enfermedades.
        queryEq =
            "select distinct(cd_e_notif), ds_e_notif, cd_centro from sive_e_notif "
            + "where cd_e_notif in(";
        queryEq += "select cd_e_notif from sive_notif_edoi ";
        if (paramC1.sAnyoDesde.equals(paramC1.sAnyoHasta)) {
          queryEq += " where (CD_FUENTE = 'E' and CD_ANOEPI = ? and CD_SEMEPI >= ? and CD_SEMEPI <= ?)";
        }
        else {
          queryEq +=
              " where ((CD_FUENTE = 'E' and CD_ANOEPI = ? and CD_SEMEPI >= ?) or "
              + "(CD_FUENTE = 'E' and CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_FUENTE = 'E' and CD_ANOEPI = ? and CD_SEMEPI <= ?))";
        }

        queryEq = new String(queryEq + strIN); // E

        if (paramC1.sNivel1.trim().length() > 0) {
          queryEq += "and cd_nivel_1=? ";

          if (paramC1.sNivel2.trim().length() > 0) {
            queryEq += "and cd_nivel_2=? ";

            if (paramC1.sZBS.trim().length() > 0) {
              queryEq += "and cd_zbs=? ";
            }
          }
        }
        queryEq += ")";

        st = con.prepareStatement(queryEq);

        // parametros
        int iParam = 1;

        // parametros temporales
        if (paramC1.sAnyoHasta.equals(paramC1.sAnyoDesde)) {
          registroConsultas.insertarParametro(paramC1.sAnyoDesde);
          st.setString(iParam, paramC1.sAnyoDesde);
          iParam++;

          registroConsultas.insertarParametro(paramC1.sSemDesde);
          st.setString(iParam, paramC1.sSemDesde);
          iParam++;

          registroConsultas.insertarParametro(paramC1.sSemHasta);
          st.setString(iParam, paramC1.sSemHasta);
          iParam++;
        }
        else {
          registroConsultas.insertarParametro(paramC1.sAnyoDesde);
          st.setString(iParam, paramC1.sAnyoDesde);
          iParam++;

          registroConsultas.insertarParametro(paramC1.sSemDesde);
          st.setString(iParam, paramC1.sSemDesde);
          iParam++;

          registroConsultas.insertarParametro(paramC1.sAnyoDesde);
          st.setString(iParam, paramC1.sAnyoDesde);
          iParam++;

          registroConsultas.insertarParametro(paramC1.sAnyoHasta);
          st.setString(iParam, paramC1.sAnyoHasta);
          iParam++;

          registroConsultas.insertarParametro(paramC1.sAnyoHasta);
          st.setString(iParam, paramC1.sAnyoHasta);
          iParam++;

          registroConsultas.insertarParametro(paramC1.sSemHasta);
          st.setString(iParam, paramC1.sSemHasta);
          iParam++;
        }

        if (paramC1.sNivel1.trim().length() > 0) {

          registroConsultas.insertarParametro(paramC1.sNivel1);
          st.setString(iParam, paramC1.sNivel1);
          iParam++;
          if (paramC1.sNivel2.trim().length() > 0) {

            registroConsultas.insertarParametro(paramC1.sNivel2);
            st.setString(iParam, paramC1.sNivel2);
            iParam++;
            if (paramC1.sZBS.trim().length() > 0) {

              registroConsultas.insertarParametro(paramC1.sZBS);
              st.setString(iParam, paramC1.sZBS);
              iParam++;
            }
          }
        }

        // JRM (28/06/01): Aplicaci�n LORTAD
        registroConsultas.registrar("SIVE_EDOIND",
                                    queryEq,
                                    "C�d. Enf",
                                    "",
                                    "SrvConsCasoPer",
                                    true);
        // ejecutamos la consulta
        rs = st.executeQuery();

        cadenaEquipos = "(";

        while (rs.next()) {
          codAux = rs.getString("cd_e_notif");
          cadenaEquipos += codAux + ",";
          miRegistro = new DataInfC13(rs.getString("cd_centro"),
                                      "", codAux, rs.getString("ds_e_notif"));
          vectorEquipos.addElement(miRegistro);
          miRegistro = null;
          codAux = null;
        }

        if (vectorEquipos.size() > 0) {
          cadenaEquipos = cadenaEquipos.substring(1, cadenaEquipos.length() - 1);
          cadenaEquipos += ")";
        }

        rs.close();
        st.close();
        rs = null;
        st = null;

        if (vectorEquipos.size() > 0) {
          // ahora los centros
          for (int j = 0; j < vectorEquipos.size(); j++) {
            miRegistro = new DataInfC13();
            miRegistro = (DataInfC13) vectorEquipos.elementAt(j);

            queryCN = "select ds_centro from sive_c_notif where ";
            queryCN += "cd_centro=?";

            st = con.prepareStatement(queryCN);

            st.setString(1, miRegistro.CD_CENTRO);

            rs = st.executeQuery();

            // guardamos los datos en el vector de centros
            if (rs.next()) {
              miRegistro.DS_CENTRO = rs.getString("DS_CENTRO");
              vectorCentros.addElement(miRegistro);
            }

            rs.close();
            st.close();
            rs = null;
            st = null;
            miRegistro = null;
          }

          // ahora los casos
          // preparo la cadena con los c�digos de enfermedad: ('10','15')
          strIN = new String(" and CD_ENFCIE in(");
          for (ku = 0; ku < vectorEnfermedades.size() - 1; ku++) {
            strIN = new String(strIN + "'" +
                               (String) vectorEnfermedades.elementAt(ku) + "'" +
                               ",");
          }
          strIN = new String(strIN + "'" +
                             (String) vectorEnfermedades.elementAt(ku) + "'" +
                             ")");

          for (int j = 0; j < vectorCentros.size(); j++) {
            iEstado = -1;

            miRegistro = new DataInfC13();
            miRegistro = (DataInfC13) vectorCentros.elementAt(j);

            query = " select count(sne.nm_edo) cuenta, se.CD_ENFCIE "
                + " from SIVE_NOTIF_EDOI sne, SIVE_EDOIND se "
                + " where sne.NM_EDO = se.NM_EDO and sne.CD_FUENTE = 'E' ";

            if (paramC1.sAnyoDesde.equals(paramC1.sAnyoHasta)) {
              query +=
                  " and (sne.CD_ANOEPI = ? and sne.CD_SEMEPI >= ? and sne.CD_SEMEPI <= ?)";
            }
            else {
              query += " and ((sne.CD_ANOEPI = ? and sne.CD_SEMEPI >= ?) or "
                  + "(sne.CD_ANOEPI > ? and sne.CD_ANOEPI < ?) or (sne.CD_ANOEPI = ? and sne.CD_SEMEPI <= ?))";
            }
            query += " and cd_e_notif= ? " + strIN;

            query += " group by CD_ENFCIE order by CD_ENFCIE";

            // System_out.println(query);

            st = con.prepareStatement(query);

            // parametros
            iParam = 1;

            // parametros temporales
            if (paramC1.sAnyoHasta.equals(paramC1.sAnyoDesde)) {
              registroConsultas.insertarParametro(paramC1.sAnyoDesde);
              st.setString(iParam, paramC1.sAnyoDesde);
              iParam++;

              registroConsultas.insertarParametro(paramC1.sSemDesde);
              st.setString(iParam, paramC1.sSemDesde);
              iParam++;

              registroConsultas.insertarParametro(paramC1.sSemHasta);
              st.setString(iParam, paramC1.sSemHasta);
              iParam++;
            }
            else {

              registroConsultas.insertarParametro(paramC1.sAnyoDesde);
              st.setString(iParam, paramC1.sAnyoDesde);
              iParam++;

              registroConsultas.insertarParametro(paramC1.sSemDesde);
              st.setString(iParam, paramC1.sSemDesde);
              iParam++;

              registroConsultas.insertarParametro(paramC1.sAnyoDesde);
              st.setString(iParam, paramC1.sAnyoDesde);
              iParam++;

              registroConsultas.insertarParametro(paramC1.sAnyoHasta);
              st.setString(iParam, paramC1.sAnyoHasta);
              iParam++;

              registroConsultas.insertarParametro(paramC1.sAnyoHasta);
              st.setString(iParam, paramC1.sAnyoHasta);
              iParam++;

              registroConsultas.insertarParametro(paramC1.sSemHasta);
              st.setString(iParam, paramC1.sSemHasta);
              iParam++;
            }

            // valor del equipo
            registroConsultas.insertarParametro(miRegistro.CD_E_NOTIF);
            st.setString(iParam, miRegistro.CD_E_NOTIF);
            iParam++;

            // JRM (28/06/01): Aplicaci�n LORTAD
            registroConsultas.registrar("SIVE_EDOIND",
                                        query,
                                        "C�d. Enf",
                                        "",
                                        "SrvConsCasoPer",
                                        true);

            rs = st.executeQuery();

            // guardamos los datos

            while (rs.next()) {
              iEstado = CLista.listaLLENA;

              strCD_ENFCIE = new String(rs.getString("CD_ENFCIE"));

              miRegistro.ponDato(vectorEnfermedades.indexOf(strCD_ENFCIE) + 1,
                                 strCD_ENFCIE,
                                 "",
                                 rs.getInt("CUENTA"));
            }

            for (ku = 1; ku <= vectorEnfermedades.size(); ku++) {
              miRegistro.ponCDDato(ku,
                                   (String) vectorEnfermedades.elementAt(ku - 1));
            }

            if (iEstado == CLista.listaLLENA) {
              registros.addElement(miRegistro);
            }

            rs.close();
            st.close();
            rs = null;
            st = null;
            miRegistro = null;
          }

          // totales
          query = "";
          totales = new Vector();
          totales.addElement(new Integer(registros.size()));
        }
        else {
          //totales = new Vector();
          // System_out.println("Salida sin datos");
        }

        // System_out.println("Salida3");
        break;
    }
    // E System_out.println("Salida4");
    data = new CLista();
    // prepara los datos
    registros.trimToSize();
    data.addElement(registros);
    totales.trimToSize();
    data.addElement(totales);
    data.setState(iEstado);
    data.trimToSize();
//    data.setState = CLista.listaLLENA;

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return data;
  }
}
