package conscasoper;

import java.io.Serializable;

public class DataInfC13
    implements Serializable {
  public String CD_CENTRO = "";
  public String DS_CENTRO = "";
  public String CD_E_NOTIF = "";
  public String DS_E_NOTIF = "";
  public String CD_ENFCIE1 = "";
  public String DS_ENFCIE1 = "";
  public int SUMA1 = 0;
  public String CD_ENFCIE2 = "";
  public String DS_ENFCIE2 = "";
  public int SUMA2 = 0;
  public String CD_ENFCIE3 = "";
  public String DS_ENFCIE3 = "";
  public int SUMA3 = 0;
  public String CD_ENFCIE4 = "";
  public String DS_ENFCIE4 = "";
  public int SUMA4 = 0;
  public String CD_ENFCIE5 = "";
  public String DS_ENFCIE5 = "";
  public int SUMA5 = 0;
  public String CD_ENFCIE6 = "";
  public String DS_ENFCIE6 = "";
  public int SUMA6 = 0;
  public String CD_ENFCIE7 = "";
  public String DS_ENFCIE7 = "";
  public int SUMA7 = 0;
  public String CD_ENFCIE8 = "";
  public String DS_ENFCIE8 = "";
  public int SUMA8 = 0;
  public String CD_ENFCIE9 = "";
  public String DS_ENFCIE9 = "";
  public int SUMA9 = 0;
  public String CD_ENFCIE10 = "";
  public String DS_ENFCIE10 = "";
  public int SUMA10 = 0;
  public String CD_ENFCIE11 = "";
  public String DS_ENFCIE11 = "";
  public int SUMA11 = 0;
  public String CD_ENFCIE12 = "";
  public String DS_ENFCIE12 = "";
  public int SUMA12 = 0;

  public DataInfC13(String centro, String dscentro, String equipo,
                    String dsequipo) {

    CD_CENTRO = centro;
    DS_CENTRO = dscentro;
    CD_E_NOTIF = equipo;
    DS_E_NOTIF = dsequipo;
  }

  public DataInfC13() {
  }

  public String getCDDato(int posicion) {
    String strValor = null;
    switch (posicion) {
      case 1:
        strValor = CD_ENFCIE1;
        break;
      case 2:
        strValor = CD_ENFCIE2;
        break;
      case 3:
        strValor = CD_ENFCIE3;
        break;
      case 4:
        strValor = CD_ENFCIE4;
        break;
      case 5:
        strValor = CD_ENFCIE5;
        break;
      case 6:
        strValor = CD_ENFCIE6;
        break;
      case 7:
        strValor = CD_ENFCIE7;
        break;
      case 8:
        strValor = CD_ENFCIE8;
        break;
      case 9:
        strValor = CD_ENFCIE9;
        break;
      case 10:
        strValor = CD_ENFCIE10;
        break;
      case 11:
        strValor = CD_ENFCIE11;
        break;
      case 12:
        strValor = CD_ENFCIE12;
        break;
    }
    return strValor;
  }

  public void ponDato(int posicion, String cd, String ds, int suma) {
    switch (posicion) {
      case 1:
        CD_ENFCIE1 = cd;
        DS_ENFCIE1 = ds;
        SUMA1 = suma;
        break;
      case 2:
        CD_ENFCIE2 = cd;
        DS_ENFCIE2 = ds;
        SUMA2 = suma;
        break;
      case 3:
        CD_ENFCIE3 = cd;
        DS_ENFCIE3 = ds;
        SUMA3 = suma;
        break;
      case 4:
        CD_ENFCIE4 = cd;
        DS_ENFCIE4 = ds;
        SUMA4 = suma;
        break;
      case 5:
        CD_ENFCIE5 = cd;
        DS_ENFCIE5 = ds;
        SUMA5 = suma;
        break;
      case 6:
        CD_ENFCIE6 = cd;
        DS_ENFCIE6 = ds;
        SUMA6 = suma;
        break;
      case 7:
        CD_ENFCIE7 = cd;
        DS_ENFCIE7 = ds;
        SUMA7 = suma;
        break;
      case 8:
        CD_ENFCIE8 = cd;
        DS_ENFCIE8 = ds;
        SUMA8 = suma;
        break;
      case 9:
        CD_ENFCIE9 = cd;
        DS_ENFCIE9 = ds;
        SUMA9 = suma;
        break;
      case 10:
        CD_ENFCIE10 = cd;
        DS_ENFCIE10 = ds;
        SUMA10 = suma;
        break;
      case 11:
        CD_ENFCIE11 = cd;
        DS_ENFCIE11 = ds;
        SUMA11 = suma;
        break;
      case 12:
        CD_ENFCIE12 = cd;
        DS_ENFCIE12 = ds;
        SUMA12 = suma;
        break;
    }
  }

  public void ponCDDato(int posicion, String cd) {
    switch (posicion) {
      case 1:
        CD_ENFCIE1 = cd;
        break;
      case 2:
        CD_ENFCIE2 = cd;
        break;
      case 3:
        CD_ENFCIE3 = cd;
        break;
      case 4:
        CD_ENFCIE4 = cd;
        break;
      case 5:
        CD_ENFCIE5 = cd;
        break;
      case 6:
        CD_ENFCIE6 = cd;
        break;
      case 7:
        CD_ENFCIE7 = cd;
        break;
      case 8:
        CD_ENFCIE8 = cd;
        break;
      case 9:
        CD_ENFCIE9 = cd;
        break;
      case 10:
        CD_ENFCIE10 = cd;
        break;
      case 11:
        CD_ENFCIE11 = cd;
        break;
      case 12:
        CD_ENFCIE12 = cd;
        break;
    }
  }

}