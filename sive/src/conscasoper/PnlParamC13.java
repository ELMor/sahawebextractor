package conscasoper;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaADZE;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo2.DataCat2;
import enfermo.comun;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import vCasIn.DataMun;
import zbs.DataZBS;
import zbs.DataZBS2;

public class PnlParamC13
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  //final String strSERVLETenf = "servlet/SrvEnfVigi";

  protected StubSrvBD stubCliente = new StubSrvBD();

  public DataC13 paramC1 = null;

  protected int modoOperacion = modoNORMAL;

  protected PanelInforme informe;

  //protected boolean bPermiso = false;

  // E
  protected CLista listaEnfermedades = new CLista();

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETCentro = "servlet/SrvCN";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;
  final int servletOBTENERindiv_X_CODIGO = 7;
  final int servletOBTENERindiv_X_DESCRIPCION = 8;
  final int servletSELECCIONindiv_X_CODIGO = 9;
  final int servletSELECCIONindiv_X_DESCRIPCION = 10;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblProvincia = new Label();
  CCampoCodigo txtCodPro = new CCampoCodigo();
  ButtonControl btnCtrlBuscarPro = null;
  TextField txtDesPro = new TextField();
  Label lblMunicipio = new Label();
  CCampoCodigo txtCodMun = new CCampoCodigo();
  ButtonControl btnCtrlBuscarMun = null;
  TextField txtDesMun = new TextField();
  Label lblArea = new Label();
  CCampoCodigo txtCodAre = new CCampoCodigo();
  ButtonControl btnCtrlBuscarAre = null;
  TextField txtDesAre = new TextField();
  Label lblDistrito = new Label();
  CCampoCodigo txtCodDis = new CCampoCodigo();
  TextField txtDesDis = new TextField();
  Label lblZonaBasica = new Label();
  CCampoCodigo txtCodZBS = new CCampoCodigo();
  ButtonControl btnCtrlBuscarDis = null;
  ButtonControl btnCtrlBuscarZBS = null;
  TextField txtDesZBS = new TextField();

  PanFechas fechasDesde;
  PanFechas fechasHasta;

  ButtonControl btnADZE = null; // E

  ButtonControl btnLimpiar = null;
  ButtonControl btnInforme = null;

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  txt_keyAdapter txtTextAdapter = new txt_keyAdapter(this);
  actionListener btnActionListener = new actionListener(this);
  Label lblNumEnf = new Label();
  TextField txtNumEnf = new TextField();

  public PnlParamC13(CApp a) {
    try {

      this.app = a;
      res = ResourceBundle.getBundle("conscasoper.Res" + app.getIdioma());
      DataC13 d = new DataC13();
      informe = new PanelInforme(this.app, this);
      d = null;
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);

      // lanzamos un thread que trae los datos del choice
      //new Thread(new LeerPermiso(this)).start();

      jbInit();
      informe.setEnabled(false);
      lblArea.setText(this.app.getNivel1() + ":");
      lblDistrito.setText(this.app.getNivel2() + ":");
      this.txtCodAre.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesAre.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDis.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDis.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        comun.imgLUPA,
        comun.imgLIMPIAR,
        comun.imgGENERAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(490);

    btnCtrlBuscarPro = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarMun = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarAre = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarDis = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarZBS = new ButtonControl(imgs.getImage(0));

    btnADZE = new ButtonControl(imgs.getImage(0)); // E

    btnLimpiar = new ButtonControl(imgs.getImage(1));
    btnInforme = new ButtonControl(imgs.getImage(2));

    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    lblNumEnf.setText(res.getString("lblNumEnf.Text"));
    txtNumEnf.setBackground(new Color(255, 255, 150));

    txtNumEnf.setEditable(false); // E

    xYLayout.setWidth(596);

    // gestores de eventos
    btnCtrlBuscarPro.addActionListener(btnActionListener);
    btnCtrlBuscarMun.addActionListener(btnActionListener);
    btnCtrlBuscarAre.addActionListener(btnActionListener);
    btnCtrlBuscarDis.addActionListener(btnActionListener);
    btnCtrlBuscarZBS.addActionListener(btnActionListener);

    btnADZE.addActionListener(btnActionListener); // E

    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtCodPro.addKeyListener(txtTextAdapter);
    txtCodMun.addKeyListener(txtTextAdapter);
    txtCodAre.addKeyListener(txtTextAdapter);
    txtCodDis.addKeyListener(txtTextAdapter);
    txtCodZBS.addKeyListener(txtTextAdapter);

    txtCodPro.addFocusListener(txtFocusAdapter);
    txtCodMun.addFocusListener(txtFocusAdapter);
    txtCodAre.addFocusListener(txtFocusAdapter);
    txtCodDis.addFocusListener(txtFocusAdapter);
    txtCodZBS.addFocusListener(txtFocusAdapter);

    btnCtrlBuscarPro.setActionCommand("buscarPro");
    txtDesPro.setEditable(false);
    txtDesPro.setEnabled(false);
    btnCtrlBuscarMun.setActionCommand("buscarMun");
    txtDesMun.setEditable(false);
    txtDesMun.setEnabled(false);
    btnCtrlBuscarAre.setActionCommand("buscarAre");
    txtDesAre.setEditable(false);
    txtDesAre.setEnabled(false);
    btnCtrlBuscarDis.setActionCommand("buscarDis");
    btnCtrlBuscarZBS.setActionCommand("buscarZBS");

    btnADZE.setActionCommand("SeleccionarEnfermedades"); // E

    txtDesZBS.setEditable(false);
    txtDesZBS.setEnabled(false);
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtCodPro.setName("txtCodPro");
    txtCodMun.setName("txtCodMun");
    txtCodAre.setName("txtCodAre");
    txtCodDis.setName("txtCodDis");
    txtDesDis.setEditable(false);
    txtDesDis.setEnabled(false);
    txtCodZBS.setName("txtCodZBS");

    lblHasta.setText(res.getString("lblHasta.Text"));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    lblZonaBasica.setText(res.getString("lblZonaBasica.Text"));
    lblArea.setText(res.getString("lblArea.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));
    this.setLayout(xYLayout);

    // ponemos la enfermedad como clave

    this.add(lblDesde, new XYConstraints(26, 5, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 5, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 35, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 35, -1, -1));

    this.add(lblProvincia, new XYConstraints(24, 95, 64, -1));
    this.add(txtCodPro, new XYConstraints(141, 95, 77, -1));
    this.add(btnCtrlBuscarPro, new XYConstraints(223, 95, -1, -1));
    this.add(txtDesPro, new XYConstraints(252, 95, 287, -1));

    this.add(lblMunicipio, new XYConstraints(24, 125, 66, -1));
    this.add(txtCodMun, new XYConstraints(141, 125, 77, -1));
    this.add(btnCtrlBuscarMun, new XYConstraints(223, 125, -1, -1));
    this.add(txtDesMun, new XYConstraints(252, 125, 287, -1));
    this.add(lblArea, new XYConstraints(24, 155, 97, -1));
    this.add(txtCodAre, new XYConstraints(141, 155, 77, -1));
    this.add(btnCtrlBuscarAre, new XYConstraints(223, 155, -1, -1));
    this.add(txtDesAre, new XYConstraints(252, 155, 287, -1));

    this.add(lblDistrito, new XYConstraints(24, 185, 99, -1));
    this.add(txtCodDis, new XYConstraints(141, 185, 77, -1));
    this.add(btnCtrlBuscarDis, new XYConstraints(223, 185, -1, -1));
    this.add(txtDesDis, new XYConstraints(252, 185, 287, -1));
    this.add(lblZonaBasica, new XYConstraints(24, 215, 77, -1));
    this.add(txtCodZBS, new XYConstraints(141, 215, 77, -1));
    this.add(btnCtrlBuscarZBS, new XYConstraints(223, 215, -1, -1));
    this.add(txtDesZBS, new XYConstraints(252, 215, 287, -1));

    this.add(btnADZE, new XYConstraints(223, 247, -1, -1)); // E

    this.add(btnLimpiar, new XYConstraints(387, 305, -1, -1));
    this.add(btnInforme, new XYConstraints(473, 305, -1, -1));
    this.add(lblNumEnf, new XYConstraints(24, 247, 107, -1));
    this.add(txtNumEnf, new XYConstraints(141, 247, 77, -1));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  /*public synchronized void setPermiso(boolean bool) {
      bPermiso = bool;
     }
     public synchronized boolean getPermiso(){
      return bPermiso;
     } */

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0) ||
        (txtNumEnf.getText().length() == 0)) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodPro.setEnabled(true);
        txtCodAre.setEnabled(true);
        txtNumEnf.setEnabled(true);
        btnCtrlBuscarPro.setEnabled(true);

        // control municipio
        if (!txtDesPro.getText().equals("")) {
          btnCtrlBuscarMun.setEnabled(true);
          txtCodMun.setEnabled(true);
        }
        else {
          btnCtrlBuscarMun.setEnabled(false);
          txtCodMun.setEnabled(false);
        }

        btnCtrlBuscarAre.setEnabled(true);
        // control area
        if (!txtDesAre.getText().equals("")) {
          btnCtrlBuscarDis.setEnabled(true);
          txtCodDis.setEnabled(true);
        }
        else {
          btnCtrlBuscarDis.setEnabled(false);
          txtCodDis.setEnabled(false);
        }

        // control distrito
        if (!txtDesDis.getText().equals("")) {
          btnCtrlBuscarZBS.setEnabled(true);
          txtCodZBS.setEnabled(true);
        }
        else {
          btnCtrlBuscarZBS.setEnabled(false);
          txtCodZBS.setEnabled(false);
        }

        btnADZE.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodPro.setEnabled(false);
        txtCodMun.setEnabled(false);
        txtCodAre.setEnabled(false);
        txtCodDis.setEnabled(false);
        txtCodZBS.setEnabled(false);
        txtNumEnf.setEnabled(false);
        btnCtrlBuscarPro.setEnabled(false);
        btnCtrlBuscarMun.setEnabled(false);
        btnCtrlBuscarAre.setEnabled(false);
        btnCtrlBuscarDis.setEnabled(false);
        btnCtrlBuscarZBS.setEnabled(false);
        btnADZE.setEnabled(false); // E
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //busca la provincia
  void btnCtrlbuscarPro_actionPerformed(ActionEvent evt) {
    DataCat2 data = null;
    DataCat2 datProv = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      catalogo2.CListaCat2 lista = new catalogo2.CListaCat2(app,
          res.getString("msg13.Text"),
          stubCliente,
          strSERVLETProv,
          servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletOBTENER_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA);
      lista.show();
      datProv = (DataCat2) lista.getComponente();
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datProv != null) {
      txtCodPro.removeKeyListener(txtTextAdapter);
      txtCodPro.setText(datProv.getCod());
      txtDesPro.setText(datProv.getDes());
      txtCodMun.setText("");
      txtDesMun.setText("");
      txtCodPro.addKeyListener(txtTextAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el municipio
  void btnCtrlbuscarMun_actionPerformed(ActionEvent evt) {
    DataMun data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaMun lista = new CListaMun(this,
                                      res.getString("msg14.Text"),
                                      stubCliente,
                                      strSERVLETMun,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataMun) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodMun.removeKeyListener(txtTextAdapter);
      txtCodMun.setText(data.getMunicipio());
      txtDesMun.setText(data.getDescMun());
      txtCodMun.addKeyListener(txtTextAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarAre_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg15.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodAre.removeKeyListener(txtTextAdapter);
      txtCodAre.setText(data.getCod());
      txtDesAre.setText(data.getDes());
      txtCodDis.setText("");
      txtDesDis.setText("");
      txtCodZBS.setText("");
      txtDesZBS.setText("");

      txtCodAre.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el distrito (Niv2)
  void btnCtrlbuscarDis_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg15.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDis.removeKeyListener(txtTextAdapter);
      txtCodDis.setText(data.getNiv2());
      txtDesDis.setText(data.getDes());
      txtCodZBS.setText("");
      txtDesZBS.setText("");

      txtCodDis.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnADZE_actionPerformed(ActionEvent evt) {
    final int ENFERMEDAD = 15;

    CListaADZE clEnfermedades = null;

    modoOperacion = modoESPERA;
    Inicializar();

    clEnfermedades = new CListaADZE(app, ENFERMEDAD, 12, listaEnfermedades);
    clEnfermedades.show();

    if (clEnfermedades.resultado()) {
      listaEnfermedades = clEnfermedades.listaRes();
      txtNumEnf.setText(String.valueOf(listaEnfermedades.size()));
    }

    modoOperacion = modoNORMAL;
    Inicializar();

    return;
  }

  void btnCtrlbuscarZBS_actionPerformed(ActionEvent evt) {
    DataZBS data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZona lista = new CListaZona(this,
                                        res.getString("msg16.Text"),
                                        stubCliente,
                                        strSERVLETZona,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (data != null) {
      txtCodZBS.removeKeyListener(txtTextAdapter);
      txtCodZBS.setText(data.getCod());
      txtDesZBS.setText(data.getDes());
      txtCodZBS.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtCodAre.setText("");
    txtDesAre.setText("");
    txtCodDis.setText("");
    txtDesDis.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");
    txtCodPro.setText("");
    txtDesPro.setText("");
    txtCodMun.setText("");
    txtDesMun.setText("");
    txtNumEnf.setText(""); // E
    listaEnfermedades = null; // E
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC1 = new DataC13();

    if (isDataValid()) {
      try {
        if (!txtCodPro.getText().equals("")) {
          paramC1.sProvincia = txtCodPro.getText();
        }
        if (!txtCodMun.getText().equals("")) {
          paramC1.sMunicipio = txtCodMun.getText();
        }
        if (!txtCodAre.getText().equals("")) {
          paramC1.sNivel1 = txtCodAre.getText();
        }
        if (!txtCodDis.getText().equals("")) {
          paramC1.sNivel2 = txtCodDis.getText();
        }
        if (!txtCodZBS.getText().equals("")) {
          paramC1.sZBS = txtCodZBS.getText();
        }

        // parametros obligatorios
        paramC1.sAnyoDesde = fechasDesde.txtAno.getText();
        paramC1.sSemDesde = fechasDesde.txtCodSem.getText();
        paramC1.sAnyoHasta = fechasHasta.txtAno.getText();
        paramC1.sSemHasta = fechasHasta.txtCodSem.getText();
        paramC1.iNumEnf = Integer.parseInt(txtNumEnf.getText().trim());

        paramC1.listaEnfermedades = listaEnfermedades;

        // habilita el panel
        modoOperacion = modoESPERA;
        Inicializar();
        informe.setEnabled(true);
        CLista param = new CLista();
        param.addElement(paramC1);
        //informe.setListaParam(param);
        informe.paramC1 = paramC1;
        boolean hay = informe.GenerarInforme();
        if (hay) {
           ( (CDialog) informe).show();
        }
        modoOperacion = modoNORMAL;
        Inicializar();
      }
      catch (Exception erf) {
        CMessage msgBox2 = new CMessage(this.app, CMessage.msgERROR,
                                        erf.getMessage());
        msgBox2.show();
        msgBox2 = null;
      }
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg17.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;
    //DataEqNot eqnot;
    //DataCN cnnot;
    DataCat2 prov;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtCodAre.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodAre.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtCodDis.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodAre.getText(), txtCodDis.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtCodZBS.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS(txtCodZBS.getText(), "", "",
                                   txtCodAre.getText(), txtCodDis.getText()));
      strServlet = strSERVLETZona;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtCodPro.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCat2(txtCodPro.getText()));
      strServlet = strSERVLETProv;
      modoServlet = servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA;

    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtCodMun.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataMun(txtCodPro.getText(), txtCodMun.getText(), "",
                                   "", "", ""));
      strServlet = strSERVLETMun;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodAre")) {
            nivel1 = (DataNivel1) param.firstElement();
            txtCodAre.removeKeyListener(txtTextAdapter);
            txtCodAre.setText(nivel1.getCod());
            txtDesAre.setText(nivel1.getDes());
            txtCodAre.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodDis")) {
            nivel2 = (DataZBS2) param.firstElement();
            txtCodDis.removeKeyListener(txtTextAdapter);
            txtCodDis.setText(nivel2.getNiv2());
            txtDesDis.setText(nivel2.getDes());
            txtCodDis.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodZBS")) {
            zbs = (DataZBS) param.firstElement();
            txtCodZBS.removeKeyListener(txtTextAdapter);
            txtCodZBS.setText(zbs.getCod());
            txtDesZBS.setText(zbs.getDes());
            txtCodZBS.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodPro")) {
            prov = (DataCat2) param.firstElement();
            txtCodPro.removeKeyListener(txtTextAdapter);
            txtCodPro.setText(prov.getCod());
            txtDesPro.setText(prov.getDes());
            txtCodPro.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodMun")) {
            mun = (DataMun) param.firstElement();
            txtCodMun.removeKeyListener(txtTextAdapter);
            txtCodMun.setText(mun.getMunicipio());
            txtDesMun.setText(mun.getDescMun());
            txtCodMun.addKeyListener(txtTextAdapter);

          }

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg18.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtDesAre.getText().length() > 0)) {
//      txtCodAre.setText("");
      txtCodDis.setText("");
      txtCodZBS.setText("");
      txtDesAre.setText("");
      txtDesDis.setText("");
      txtDesZBS.setText("");
    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtDesDis.getText().length() > 0)) {
//      txtCodDis.setText("");
      txtCodZBS.setText("");
      txtDesDis.setText("");
      txtDesZBS.setText("");
    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtDesZBS.getText().length() > 0)) {
//      txtCodZBS.setText("");
      txtDesZBS.setText("");
    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtDesPro.getText().length() > 0)) {
//      txtCodPro.setText("");
      txtCodMun.setText("");
      txtDesPro.setText("");
      txtDesMun.setText("");
    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtDesMun.getText().length() > 0)) {
//      txtCodMun.setText("");
      txtDesMun.setText("");
    }

    Inicializar();
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  PnlParamC13 adaptee = null;
  ActionEvent e = null;

  public actionListener(PnlParamC13 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarPro")) {
      adaptee.btnCtrlbuscarPro_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarMun")) {
      adaptee.btnCtrlbuscarMun_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarAre")) {
      adaptee.btnCtrlbuscarAre_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDis")) {
      adaptee.btnCtrlbuscarDis_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarZBS")) {
      adaptee.btnCtrlbuscarZBS_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("SeleccionarEnfermedades")) {
      adaptee.btnADZE_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PnlParamC13 adaptee;
  FocusEvent event;

  focusAdapter(PnlParamC13 adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

/*
// cambio en una caja de codigos
 class textAdapter implements java.awt.event.TextListener {
  PnlParamC3 adaptee;
  textAdapter(PnlParamC3 adaptee) {
    this.adaptee = adaptee;
  }
  public void textValueChanged(TextEvent e) {
    adaptee.textValueChanged(e);
  }
 }
 */
////////////////////// Clases para listas

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected PnlParamC13 panel;

  public CListaZBS2(PnlParamC13 p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodAre.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaZona
    extends CListaValores {

  protected PnlParamC13 panel;

  public CListaZona(PnlParamC13 p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtCodAre.getText(),
                       panel.txtCodDis.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

class CListaMun
    extends CListaValores {

  protected PnlParamC13 panel;

  public CListaMun(PnlParamC13 p,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataMun(panel.txtCodPro.getText(), s, "", "", "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataMun) o).getMunicipio());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMun) o).getDescMun());
  }
}

/**
 *  esta clase se lanza como un thread para traer los datos
 class LeerPermiso implements Runnable{
  protected PnlParamC13 adaptee = null;
  /** para la comunicacion con el servlet */
 /*public StubSrvBD stubCliente = new StubSrvBD();
    public LeerPermiso (PnlParamC13 app  ) {
     adaptee = app;
    }
    public void run(){
   try {
     CLista parametros = new CLista();
     CLista result = null;
    DataPer d = new DataPer(DataPer.IT_FG_ENFERMO);
    d.setNumParam(1);
    d.put(1, adaptee.getApp().getLogin());  // ponemos el c�digo del enfermo
    parametros.addElement(d);
    result = comun.traerDatos(adaptee.getApp(), stubCliente, comun.strSERVLET_GEN, DataPer.modoPERMISO, parametros);
    if ( result != null && result.size() > 0 ){
       DataPer p = (DataPer) result.firstElement();
       adaptee.setPermiso (p.getPERMISO());
       //System_out.println("fijamos " +p.getPERMISO());
    }else{
       adaptee.setPermiso (false);
    }
  adaptee.setPermiso (false);
   }catch (Exception exc) {
     adaptee.setPermiso (false);
   }
    }
  } // ENd Class
  */

 class txt_keyAdapter
     extends java.awt.event.KeyAdapter {
   PnlParamC13 adaptee;

   txt_keyAdapter(PnlParamC13 adaptee) {
     this.adaptee = adaptee;
   }

   public void keyPressed(KeyEvent e) {
     adaptee.txt_keyPressed(e);
   }
 }
