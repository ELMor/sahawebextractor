package conscasoper;

import java.io.Serializable;

import capp.CLista; // E

public class DataC13
    implements Serializable {
  public String sAnyoDesde = "";
  public String sSemDesde = "";
  public String sAnyoHasta = "";
  public String sSemHasta = "";
  public String sProvincia = "";
  public String sMunicipio = "";
  public String sNivel1 = "";
  public String sNivel2 = "";
  public String sZBS = "";
  public int iNumEnf = 0;

  public CLista listaEnfermedades = null; // E

  public int iPagina = 0;
  public boolean bInformeCompleto = false;

  public DataC13(String anodes, String semdes, String anohas, String semhas,
                 String prov, String mun, String niv1, String niv2,
                 String zbs, int numenf) {

    sAnyoDesde = anodes;
    sSemDesde = semdes;
    sAnyoHasta = anohas;
    sSemHasta = semhas;
    sProvincia = prov;
    sMunicipio = mun;
    sNivel1 = niv1;
    sNivel2 = niv2;
    sZBS = zbs;
    iNumEnf = numenf;
  }

  public DataC13() {
  }
}
