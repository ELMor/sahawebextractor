//Title:        Your Product Name
//Version:
//Copyright:    CopyrtxtFecEnvight (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:  Your description

package envressem;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Event;
import java.awt.FileDialog;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CFileName;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import envotrcom.DialCarta;
import fechas.CFecha;
import infedoind.DialInfEdoInd;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;
import sapp.StubSrvBD;

public class PanEnvUrg
    extends CPanel {

  //a�o epidemiol�gico
  public String sAnoBk = "";
  //Para obtenci�n de Strings de fichero
  ResourceBundle res;
  String strNuevo;

  //�ltimo a�o epid.
  public String sUltAnoBk = "";

  //modos de consulta al servlet
  final int servletOBTENER_ULTIMO_ANO_Y_ENVIOS = 1;
  final int servletOBTENER_UN_ANO_Y_ENVIOS = 2;
  final int servletSELECCION_EDOIND_URG = 3;
  final int servletGENFICH_EDOIND_URG = 4;

  //rutas imagenes
  final String imgNAME[] = {
      "images/salvar.gif",
      "images/grafico.gif",
      "images/grafico.gif"};

  final String dirDEFAULT = "c:\\";

  // indica la fila seleccionada en la tabla
  public int m_itemSelecc = -1;

  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variables
  int modoOperacion;
  int modoServlet;

  // lista
  public CLista lista = new CLista();

  //Vector que guarda parejas fecha,n�mero de los envios de un a�o
  Vector vEnvios;

  protected CLista listaFich = new CLista();
  protected StubSrvBD stubCliente = new StubSrvBD();
  protected StubSrvBD stubCliente2 = new StubSrvBD();

  XYLayout xYLayout1 = new XYLayout();
  XYLayout xYLayoutPrueba = new XYLayout();

  ButtonControl btnGenFic = new ButtonControl();
  CTabla tabla = new CTabla();
  CFileName panFile;
  String sAnoActual = "";

  ButtonControl btnCarta = new ButtonControl();
  ButtonControl btnListado = new ButtonControl();

  PanEnvUrg_tabla_actionAdapter tablaActionListener = new
      PanEnvUrg_tabla_actionAdapter(this);
  PanEnvUrgItemListener multlstItemListener = new PanEnvUrgItemListener(this);
  PanEnvUrgActionListener btnActionListener = new PanEnvUrgActionListener(this);

  protected CCargadorImagen imgs = null;

  StatusBar statusBar = new StatusBar();
  Panel pnl = new Panel();
  Label lblAno = new Label();
  Choice choiceEnv = new Choice();
  Label lblFecha = new Label();

  // ARG: Se en vez de TextField usamos CFecha
  //TextField txtFecEnv = new TextField();
  CFecha txtFecEnv = new CFecha("S");

  Label lblEnvio = new Label();
  TextField txtAno = new TextField();

  Label lblDir = new Label();
  TextField txtDir = new TextField();

  public PanEnvUrg(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("envressem.Res" + app.getIdioma());
      //String de fichero
      strNuevo = res.getString("msg4.Text");

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

//    panFile = new CFileName(this.app);
    panFile = new CFileName(this.app, FileDialog.SAVE,
                            CFileName.ELIGE_DIRECTORIO);

    //   this.setSize(new Dimension(569, 400)); //????????????

    xYLayoutPrueba.setHeight(362); //???????????    362
    xYLayoutPrueba.setWidth(660); //?????????????  678
    setLayout(xYLayoutPrueba); //????????????

    //    xYLayout1.setHeight(260);  //??????????????
//    xYLayout1.setWidth(450);   //????????????

//__________

    //CAraga imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    imgs.CargaImagenes();
    btnGenFic.setImage(imgs.getImage(0));
    btnListado.setImage(imgs.getImage(1));
    btnCarta.setImage(imgs.getImage(2));

    //Escuchadores
    btnGenFic.addActionListener(btnActionListener);
    btnListado.addActionListener(btnActionListener);
    btnCarta.addActionListener(btnActionListener);
    tabla.addActionListener(tablaActionListener);
    tabla.addItemListener(multlstItemListener);

//_____________

    btnGenFic.setActionCommand("GenFic");
    btnListado.setActionCommand("GenListado");
    btnCarta.setActionCommand("GenCarta");

    btnCarta.setLabel(res.getString("btnCarta.Label"));
    btnGenFic.setLabel(res.getString("btnGenFic.Label"));
    btnListado.setLabel(res.getString("btnListado.Label"));

//    pnl.setLayout(xYLayout1);

//    this.add(panFile, new XYConstraints(22, 229, -1, -1));

    this.add(btnGenFic, new XYConstraints(468, 286, -1, 26));
    this.add(btnCarta, new XYConstraints(212, 286, -1, 26));
    this.add(btnListado, new XYConstraints(340, 286, -1, 26));
    this.add(tabla, new XYConstraints(12, 87, -1, -1));
    this.add(lblAno, new XYConstraints(19, 32, 31, -1));
    this.add(choiceEnv, new XYConstraints(248, 32, 99, -1));
    this.add(lblFecha, new XYConstraints(356, 32, 92, -1));
    this.add(txtFecEnv, new XYConstraints(455, 32, 96, -1));
    this.add(lblEnvio, new XYConstraints(197, 32, 40, -1));
    this.add(txtAno, new XYConstraints(71, 32, 80, -1));

    this.add(lblDir, new XYConstraints(22, 229, 124, -1));
    this.add(txtDir, new XYConstraints(152, 229, 267, -1));

    // configuraci�n de la tabla
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(res.
        getString("tabla.ColumnButtonsStrings"), '\n'));
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(
        "175\n210\n75\n100", '\n'));
    tabla.setNumColumns(4);

    choiceEnv.addItemListener(new PanEnvUrg_choiceEnv_itemAdapter(this));
    txtAno.addFocusListener(new PanEnvUrg_txtAno_focusAdapter(this));
    lblEnvio.setText(res.getString("lblEnvio.Text"));
    lblFecha.setText(res.getString("lblFecha.Text"));
    lblAno.setText(res.getString("lblAno.Text"));
    lblDir.setText(res.getString("lblDir.Text"));

//    add(pnl, BorderLayout.CENTER);//?????????
//    add(statusBar, BorderLayout.SOUTH); //???????????

    txtDir.setText(dirDEFAULT);

    //Trae el �ltimo a�o y sus env�os de la b. datos
    traerUltimoAno();

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // limpia la pantalla
  void vaciarPantalla() {
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);
    }
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {

      case modoNORMAL:
        choiceEnv.setEnabled(true);

        //Inicializaci�n botones depende de elecci�n en choice
        //Si no hay elecci�n, botones desactivados
        if (choiceEnv.getSelectedItem().equals("")) {
          btnGenFic.setEnabled(false);
          btnCarta.setEnabled(false);
          btnListado.setEnabled(false);
        }
        //Si es envio nuevo, bot�n de carta desactivado
        // (Antes hay que grabar el env�o)
        else if (choiceEnv.getSelectedItem().equals(strNuevo)) {
          btnGenFic.setEnabled(true);
          btnCarta.setEnabled(false);
          //Resto de botones activos solo si tabla tiene elementos
          if (tabla.countItems() > 0) {
            btnListado.setEnabled(true);
          }
          else {
            btnListado.setEnabled(false);
          }
        }
        //Si es env�o antiguo boton de carta si activo
        else {
          btnGenFic.setEnabled(true);
          btnCarta.setEnabled(true);
          //Resto de botones activos solo si tabla tiene elementos
          if (tabla.countItems() > 0) {
            btnListado.setEnabled(true);
          }
          else {
            btnListado.setEnabled(false);
          }
        }

//        InicializarPanFile();
//        panFile.setEnabled(true);
        InicializarTxtDir();
        txtAno.setEnabled(true);
        tabla.setEnabled(true);
        txtFecEnv.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        choiceEnv.setEnabled(false);
        btnGenFic.setEnabled(false);
        btnCarta.setEnabled(false);
        btnListado.setEnabled(false);
//        InicializarPanFile();
        InicializarTxtDir();

//        panFile.setEnabled(false);
        txtAno.setEnabled(false);
        tabla.setEnabled(false);
        txtFecEnv.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    doLayout();

  }

  /*
    void InicializarPanFile() {  //?????????????????????????????
    //Si hay casos para generar fichero
      if (tabla.countItems() > 0) {
        panFile.txtFile.setEditable(false);
        panFile.setEnabled(true);
     }
      else {
        panFile.txtFile.setEditable(false);
        panFile.setEnabled(false);
     }
    }
   */

  void InicializarTxtDir() {
    if (tabla.countItems() > 0) {

      txtDir.setEditable(true);
      txtDir.setEnabled(true);
    }
    else {
      txtDir.setEditable(false);
      txtDir.setEnabled(false);
    }
  }

  public void setCurrent(int i) {
    Event ev = null;

    if (i >= tabla.countItems()) {
      i = tabla.countItems() - 1;

    }
    if (i > 0) {
      tabla.select(i);
      m_itemSelecc = i;
    }
  }

  void btnListado_actionPerformed(ActionEvent evt) {

    DialInfEdoInd dlgInforme = null;
    int indice = tabla.getSelectedIndex();
    CMessage msgBox = null;
    CLista casos;

    try {

      this.modoOperacion = modoESPERA;
      Inicializar();

      casos = null;
      casos = new CLista();

      // ARG: De monento solo se mete en la lista la clave actual.
      //casos.addElement(Integer.toString(((DataNotInd)(lista.elementAt(indice))).getNumEdo()));

      //Si hay fila elegida

      if (indice != BWTEnum.NOTFOUND) {
        //ARG: Ahora se le pasa una lista de claves.
        //dlgInforme = new DialInfEdoInd(app,casos);
        dlgInforme = new DialInfEdoInd(app,
                                       Integer.toString( ( (DataNotInd) (lista.
            elementAt(indice))).getNumEdo()));

        dlgInforme.setEnabled(true);
        if (dlgInforme.GenerarInformeCompleto()) {
          dlgInforme.show();
        }
        setCurrent(indice);
      }

      /*
             Hilo Hilo1 = new Hilo(app,Integer.toString (((DataNotInd)(lista.elementAt(indice))).getNumEdo()));
             Hilo Hilo2 = new Hilo(app,Integer.toString (((DataNotInd)(lista.elementAt(indice))).getNumEdo()));
             Hilo1.run();
             Hilo2.run();
       */
      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

//___________________ CARTA_________
  void btnCarta_actionPerformed(ActionEvent evt) {

    DialCarta dlgCarta = null;
    CMessage msgBox = null;

    try {

      this.modoOperacion = modoESPERA;
      Inicializar();

      //Si hay env�o elegido
      if (isDataValidParaCarta()) {
        //Si se ha elegido nuevo se pone el sigte numero de secuencia
        if (choiceEnv.getSelectedItem().equals(strNuevo)) {
          dlgCarta = new DialCarta(app, DialCarta.CARTA_ENV_URG, sAnoBk,
                                   Integer.toString( (vEnvios.size()) + 1));
        }
        else {
          dlgCarta = new DialCarta(app, DialCarta.CARTA_ENV_URG, sAnoBk,
                                   choiceEnv.getSelectedItem());
        }
        dlgCarta.setEnabled(true);
        if (dlgCarta.GenerarInforme()) {
          dlgCarta.show();
        }
      }

      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

//__________________________________

  void btnGenFic_actionPerformed(ActionEvent evt) {

    DataAnoEnvUrg datAnoEnvUrg;
    CLista data;
    CLista data2;
    int j = 0;
    String sLinea;
    File miFichero = null;
    FileWriter fStream = null;
    CMessage msgBox;
    CLista resEdoInd = null;
    File miFicheroCab = null;
    FileWriter fStreamCab = null;

    String sCodComAutDefinitivo = "";
    boolean bHayDatos = false;
    NumyFecEnv envio;
    Vector vEnv = new Vector(); //Vector de env�os uso para petici�n de num env�o al Servlet

    CLista listaErrores = null;
    CLista listaCatalogos = new CLista();
    DialErrores dlg = null;

    try {
      txtFecEnv.ValidarFecha();
      if ( (isDataValid() == true) && (txtFecEnv.getValid().equals("S"))) {

        this.modoOperacion = modoESPERA;
        Inicializar();

        //____________ Generaci�n fichero de casos urgentes______________________________

        data2 = null;
        data2 = new CLista();
        //Se preparan los datos de petici�n
        if (choiceEnv.getSelectedItem().equals(strNuevo)) {
          // ARG: Ahora se pasa la fecha de envio
          envio = new NumyFecEnv( -1, txtFecEnv.getText().trim());
          //envio = new NumyFecEnv(-1);
          //# // System_out.println("El nuevo******************************");
        }
        else {
          envio = new NumyFecEnv(Integer.parseInt(choiceEnv.getSelectedItem()));

        }
        vEnv.addElement(envio);

        data2.addElement(new DataAnoEnvUrg(txtAno.getText().trim(), true, vEnv));
        ( (DataAnoEnvUrg) (data2.firstElement())).setComAut(app.getCA());
        ( (DataAnoEnvUrg) (data2.firstElement())).setTSive(app.getTSive());

        //# // System_out.println("Antes de apuntar******************************");
        // apunta al servlet principal
        stubCliente2.setUrl(new URL(app.getURL() + "servlet/SrvEnvUrg"));

        // ARG: Se introduce el login
        data2.setLogin(app.getLogin());

        // ARG: Se introduce la lortad
        data2.setLortad(app.getParameter("LORTAD"));

        // obtiene la lista de resultados
        resEdoInd = (CLista) stubCliente2.doPost(servletGENFICH_EDOIND_URG,
                                                 data2);

        /*
                  envressem.SrvEnvUrg srv = new envressem.SrvEnvUrg();
                  srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                               "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                               "dba_edo",
                               "manager");
                  resEdoInd = srv.doDebug(servletGENFICH_EDOIND_URG, data2);
         */

        //Extrae las lineas del fichero de los resultados
        listaFich = ( (DataResInd) (resEdoInd.firstElement())).resumen;

        // 14-03-01  *********************************************************
        // ARS: Quitamos este c�digo para que devuelva bien el nombre del
        // fichero
            /*          //Formatea el nombre de com aut para que vaya al nombre del fichero
                  sCodComAutDefinitivo = app.getCA();
                  if (sCodComAutDefinitivo.length() ==1)
                    sCodComAutDefinitivo = "0" + sCodComAutDefinitivo ;
         */

        // ARS: En Madrid el c�digo de la Comunidad Aut�noma es 12 y para
        // epidemiolog�a es 13 por eso debemos hacer este cambio.
        sCodComAutDefinitivo = app.getCA();
        if (
            (sCodComAutDefinitivo.equals("12")) &&
            (app.getParameter("ORIGEN").equals("M"))
            ) {
          sCodComAutDefinitivo = "13";
        }
        else {
          if (sCodComAutDefinitivo.length() == 1) {
            sCodComAutDefinitivo = "0" + sCodComAutDefinitivo;
          }
        }
        // *********************************************************

        //Se crea el fichero
        /*
                  miFichero = new File(
                   this.panFile.getDirectorio()+ "\\" + "U" + "_"
                     + (txtAno.getText().trim()).substring(2,4) + "00" +
                     sCodComAutDefinitivo + ".txt");
         */
        miFichero = new File(
            this.txtDir.getText() + "\\" + "U" + "_"
            + (txtAno.getText().trim()).substring(2, 4) + "00" +
            sCodComAutDefinitivo + ".txt");

        fStream = new FileWriter(miFichero);

        //Se a�ade cabecera del fichero
        fStream.write("[cabecera]");
        fStream.write("\r");
        fStream.write("\n");

        fStream.write("A�o= " + txtAno.getText().trim());
        fStream.write("\r");
        fStream.write("\n");

        fStream.write("Usuario= " + app.getLogin());
        fStream.write("\r");
        fStream.write("\n");

        java.util.Date hoyAhora = new Date();
        SimpleDateFormat formaterFecha = new SimpleDateFormat("dd/MM/yyyy");
        fStream.write("Fecha= " + formaterFecha.format(hoyAhora));
        fStream.write("\r");
        fStream.write("\n");

        SimpleDateFormat formater = new SimpleDateFormat("h:mm a");
        fStream.write("Hora=    " + formater.format(hoyAhora));
        fStream.write("\r");
        fStream.write("\n");

        //Se extrae la lista de catalogos y se a�ade al fichero
        listaCatalogos = ( (DataResInd) (resEdoInd.firstElement())).catalogos;
        //Escribe lineas de catalogos en ficehro
        if (listaCatalogos.size() > 0) {
          for (j = 0; j < listaCatalogos.size(); j++) {
            sLinea = (String) listaCatalogos.elementAt(j);
            fStream.write(sLinea);
            fStream.write("\r");
            fStream.write("\n");
          }
        }

        fStream.write("[datos]");
        fStream.write("\r");
        fStream.write("\n");

        //Se a�aden datos del fichero
        if (listaFich.size() > 0) {
          bHayDatos = true;
          for (j = 0; j < listaFich.size(); j++) {
            sLinea = (String) listaFich.elementAt(j);
            fStream.write(sLinea);
            fStream.write("\r");
            fStream.write("\n");
          } //for
        } //if
        /*
                   else {
             msgBox = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg5.Text"));
                      msgBox.show();
                      msgBox = null;
                   }
         */

        //SE liberan recursos
        fStream.close();
        fStream = null;
        miFichero = null;

        String rutaFich = this.txtDir.getText() + "\\" + "U" + "_"
            + (txtAno.getText().trim()).substring(2, 4) + "00" +
            sCodComAutDefinitivo + ".txt";

        //Mensaje al usuario
        //Si no hay datos en ninguna semana
        if (bHayDatos == false) {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg6.Text") + rutaFich +
                                res.getString("msg7.Text"));
        }
        //Si hay datos
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg8.Text") + rutaFich);
        }
        msgBox.show();
        msgBox = null;

        //Se ven los errores de generaci�n de fichero
        listaErrores = ( (DataResInd) (resEdoInd.firstElement())).errores;
        if (listaErrores.size() > 0) {
          dlg = new DialErrores(this.app, listaErrores);
          ( (CDialog) (dlg)).show();
          dlg = null;
        }

        //Se ajusta la pantalla de cliente a los cambios ya produciidos en b. datos

        //Fecha de hoy para el envio
        /*
                 java.util.Date dFecHoy = new java.util.Date();
             SimpleDateFormat superFormateador = new SimpleDateFormat("dd/MM/yyyy");
         */
        // ARG: Solo se a�ade un nuevo envio si hay envios urgentes y es un nuevo envio.
        if ( (bHayDatos)
            && (choiceEnv.getSelectedItem().equals(strNuevo))) {
          //A�ade nuevo env�o a lista de env�os. Se a�ade el primero (va en orden descend)
          // ARG: Se tiene que guardar la fecha que se ha introducido, no la actual
          //txtFecEnv.setText(formaterFecha.format(hoyAhora));
          //vEnvios.insertElementAt(new NumyFecEnv((vEnvios.size())+ 1, formaterFecha.format(hoyAhora)), 0) ;
          vEnvios.insertElementAt(new NumyFecEnv( (vEnvios.size()) + 1,
                                                 txtFecEnv.getText()), 0);

          choiceEnv.removeAll();
          //A�ade el caso de NO SELECCION (el seleccionado por defecto)
          choiceEnv.addItem("");
          //A�ade la opci�n nuevo envio
          choiceEnv.addItem(strNuevo);

          //Reestructura el choice
          for (int cont = 0; cont < vEnvios.size(); cont++) {
            NumyFecEnv elem = (NumyFecEnv) (vEnvios.elementAt(cont));
            choiceEnv.addItem(Integer.toString(elem.getNumEnv()));
          }
          //Selecciona el que se acaba de a�adir a lista de envios
          choiceEnv.select(2);
        }

      } //if  is Data Valid

      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  boolean isDataValidParaCarta() {
    //Comprobaciones a�o en cualquier caso
    if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!sAnoBk.equals("")) &&
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos a�o v�lidos
        (txtAno.getText().trim().equals(sAnoBk)) &&
        //y que se ha seleccionado alg�n env�o en choice
        (! (choiceEnv.getSelectedItem().equals("")))
        ) {
      return true;
    }
    else {
      return false;
    }
  }

  boolean isDataValid() {

    //Comprueba que si hay que generar fichero est� el nombre del fichero en caja de texto
    if (txtDir.getText().equals("")) {
      return false;
    }

    //Comprobaciones a�o en cualquier caso
    else if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!sAnoBk.equals("")) &&
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos a�o v�lidos
        (txtAno.getText().trim().equals(sAnoBk)) &&
        //y que se ha seleccionado alg�n env�o en choice
        (! (choiceEnv.getSelectedItem().equals("")))

        ) {
      return true;
    }

    else {
      return false;
    }
  }

  void traerUltimoAno() {

    CMessage msgBox = null;
    CLista data = null;
    Object componente = null;

    // procesa
    try {

      modoOperacion = modoESPERA;
      Inicializar();

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());
      // perfil
      data.setPerfil(app.getPerfil());
      // login
      data.setLogin(app.getLogin());

      // ARG: Se introduce la lortad
      data.setLortad(app.getParameter("LORTAD"));

      data.addElement(new DataAnoEnvUrg(""));

      stubCliente.setUrl(new URL(app.getURL() + "servlet/SrvEnvUrg"));

      lista = (CLista) stubCliente.doPost(servletOBTENER_ULTIMO_ANO_Y_ENVIOS,
                                          data);
      /*
             envressem.SrvEnvUrg srv = new envressem.SrvEnvUrg();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                             "dba_edo",
                             "manager");
             lista = srv.doDebug(servletOBTENER_ULTIMO_ANO_Y_ENVIOS, data);
       */
      data = null;

      // comprueba que hay datos
      if (lista.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg14.Text"));
        msgBox.show();
        msgBox = null;
        //Recupera el a�o que hab�a
        txtAno.setText(sAnoBk);
      }
      else {

        // vacia la lista de envios y la tabla
        tabla.clear();
        choiceEnv.removeAll();

        DataAnoEnvUrg datAnoEnvUrg = (DataAnoEnvUrg) (lista.firstElement());

        //A�ade el caso de NO SELECCION (e� seleccionado por defecto)
        choiceEnv.addItem("");
        //A�ade la opci�n nuevo envio
        choiceEnv.addItem(strNuevo);
        //A�ade los env�os efectuados en ese a�o
        if (datAnoEnvUrg.getHayAno() == true) {
          //Guarda el a�o
          txtAno.setText(datAnoEnvUrg.getCodAnoUrg());

          vEnvios = datAnoEnvUrg.getEnvios();
          for (int j = 0; j < vEnvios.size(); j++) {
            NumyFecEnv elem = (NumyFecEnv) (vEnvios.elementAt(j));
            choiceEnv.addItem(Integer.toString(elem.getNumEnv()));
          }
          //Guarda el datoo del a�o para posterior posible recuperaci�n
          sAnoBk = txtAno.getText();
          sUltAnoBk = txtAno.getText();
        }
        //Si no hay a�o epid.
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg11.Text"));
          msgBox.show();
          msgBox = null;
          //Recupera el a�o que hab�a
          txtAno.setText(sAnoBk);

        }

      }

      // error al procesar
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoNORMAL;
    Inicializar();

  }

  void txtAno_focusLost(FocusEvent e) {

    CMessage msgBox = null;
    CLista data = null;
    Object componente = null;

    // procesa
    try {

      txtAno.setText(txtAno.getText().trim());

      //Si el a�o no ha cambiado no hace nada
      if (txtAno.getText().equals(sAnoBk)) {
        return;
      }

      //chequea el a�o.Debe ser entero entre 0 y 9999
      //Si no es ocrrecto recupera el valor antiguo y ya est�
      if (ChequearEntero(txtAno.getText(), 0, 9999, 2) == false) {
        txtAno.setText(sAnoBk);
        return;
      }

      modoOperacion = modoESPERA;
      Inicializar();

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());
      // perfil
      data.setPerfil(app.getPerfil());
      // login
      data.setLogin(app.getLogin());

      // ARG: Se introduce la lortad
      data.setLortad(app.getParameter("LORTAD"));

      data.addElement(new DataAnoEnvUrg(txtAno.getText().trim()));

      stubCliente.setUrl(new URL(app.getURL() + "servlet/SrvEnvUrg"));
      lista = (CLista) stubCliente.doPost(servletOBTENER_UN_ANO_Y_ENVIOS, data);

      data = null;

      // comprueba que hay datos
      if (lista.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg12.Text"));
        msgBox.show();
        msgBox = null;
        //Recupera el a�o que hab�a
        txtAno.setText(sAnoBk);
      }
      else {

        // vacia la lista de envios y la tabla
        tabla.clear();
        choiceEnv.removeAll();

        DataAnoEnvUrg datAnoEnvUrg = (DataAnoEnvUrg) (lista.firstElement());

        //A�ade el caso de NO SELECCION (e� seleccionado por defecto)
        choiceEnv.addItem("");

        //A�ade la opci�n nuevo envio s�lo para el �ltimo a�o
        if (txtAno.getText().equals(sUltAnoBk)) {
          choiceEnv.addItem(strNuevo);

          //A�ade los env�os efectuados en ese a�o
        }
        if (datAnoEnvUrg.getHayAno() == true) {
          vEnvios = datAnoEnvUrg.getEnvios();
          for (int j = 0; j < vEnvios.size(); j++) {
            NumyFecEnv elem = (NumyFecEnv) (vEnvios.elementAt(j));
            choiceEnv.addItem(Integer.toString(elem.getNumEnv()));
          }
          //Guarda el datoo del a�o para posterior posible recuperaci�n
          sAnoBk = txtAno.getText();

        }
        //Si no hay a�o epid.
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg11.Text"));
          msgBox.show();
          msgBox = null;
          //Recupera el a�o que hab�a
          txtAno.setText(sAnoBk);

        }

        /*
                // verifica que sea la �ltima trama
                if (lista.getState() == CLista.listaINCOMPLETA)
                  tabla.addItem(res.getString("msg13.Text"));
         */
      }

      // error al procesar
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoNORMAL;
    Inicializar();

  }

//Funci�n �til para validar a�o
//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString.equals("")) {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString.equals("")) {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     //comprobar valor
     if (b) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

//# // System_out.println("Devuelve b **********" + b);
     return (b);
   } //fin de ChequearEntero

  void choiceEnv_itemStateChanged(ItemEvent e) {

    CMessage msgBox = null;
    CLista data = null;
    Object componente = null;
    NumyFecEnv envio;
    Vector vEnv = new Vector(); // Vetor env�os para uso local. Petici�n de un env�o al Servlet

    /*
        //No hace nada si no hay a�o o no hay env�o seleccionado
        if  ( (choiceEnv.getSelectedItem().equals("") ) ||  (txtAno.getText().equals("")) )
          return;
     */

    //No hace nada si no hay a�o  seleccionado
    if ( (txtAno.getText().equals(""))) {
      return;
    }

    //Si no  selecciona env�o resetea campo de fecha env�o y tabla y se sale
    if (choiceEnv.getSelectedItem().equals("")) {
      txtFecEnv.setText("");
      // vacia la tabla
      tabla.clear();
      InicializarTxtDir();
      return;
    }

    // procesa
    try {

      //Si  hay env�o nuevo seleccionado resetea campo de fecha env�o
      // ARG: Ahora se introduce la fecha actual.
      if (choiceEnv.getSelectedItem().equals(strNuevo)) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date fecHoy = new java.util.Date();
        txtFecEnv.setText(formatter.format(fecHoy));
        txtFecEnv.setValid("S");
        //txtFecEnv.setText("");
      }

      //Cambia campo de fecha env�o
      else {
        //Si es el �ltimo a�o busca la fecha en vector teniendo en cuenta que choice tiene "" y res.getString("msg4.Text")
        if (txtAno.getText().equals(sUltAnoBk)) {
          NumyFecEnv elemento = (NumyFecEnv) (vEnvios.elementAt(choiceEnv.
              getSelectedIndex() - 2));
          txtFecEnv.setText(elemento.getFecEnv());
        }

        //Si no es el �ltimo a�o busca la fecha en vector teniemdo en cuenta que choice tiene ""
        else {
          NumyFecEnv elemento = (NumyFecEnv) (vEnvios.elementAt(choiceEnv.
              getSelectedIndex() - 1));
          txtFecEnv.setText(elemento.getFecEnv());
        }
      }

      // vacia la tabla
      tabla.clear();

      modoOperacion = modoESPERA;
      Inicializar();

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());
      // perfil
      data.setPerfil(app.getPerfil());
      // login
      data.setLogin(app.getLogin());

      // ARG: Se introduce la lortad
      data.setLortad(app.getParameter("LORTAD"));

      //Se preparan los datos de petici�n
      if (choiceEnv.getSelectedItem().equals(res.getString("msg4.Text"))) {
        envio = new NumyFecEnv( -1);
      }
      else {
        envio = new NumyFecEnv(Integer.parseInt(choiceEnv.getSelectedItem()));
      }
      vEnv.addElement(envio);
      data.addElement(new DataAnoEnvUrg(txtAno.getText().trim(), true, vEnv));
      ( (DataAnoEnvUrg) (data.firstElement())).setComAut(app.getCA());
      ( (DataAnoEnvUrg) (data.firstElement())).setTSive(app.getTSive());

      //Envio al servlet
      stubCliente.setUrl(new URL(app.getURL() + "servlet/SrvEnvUrg"));

      lista = (CLista) stubCliente.doPost(servletSELECCION_EDOIND_URG, data);

      /*
             envressem.SrvEnvUrg srv = new envressem.SrvEnvUrg();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                             "dba_edo",
                             "manager");
             lista = srv.doDebug(servletSELECCION_EDOIND_URG, data);
       */

      data = null;

      // comprueba que hay datos
      if (lista.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg14.Text"));
        msgBox.show();
        msgBox = null;
      }
      else {
        // escribe las l�neas en la tabla
        for (int j = 0; j < lista.size(); j++) {
          componente = lista.elementAt(j);
          tabla.addItem(setLinea(componente), '&');
        }
        /*
                // verifica que sea la �ltima trama
                if (lista.getState() == CLista.listaINCOMPLETA)
                  tabla.addItem(res.getString("msg13.Text"));
         */
      }

      // error al procesar
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // formatea el componente para ser insertado en una fila de la tabla
  public String setLinea(Object o) {
    DataNotInd dat = (DataNotInd) o;
    String sLinea = dat.getProceso() + '&' + //******
        dat.getNombre() + ' ' + dat.getApe1() + ' ' + dat.getApe2() + '&' + //********
        dat.getCodSem() + '&' +
        dat.getFecRec();
    return sLinea;
  }

  // doble click en la tabla
  public void tabla_actionPerformed() {

    DialInfEdoInd dlgInforme = null;
    int indice = tabla.getSelectedIndex();
    CMessage msgBox = null;

    try {

      this.modoOperacion = modoESPERA;
      Inicializar();

      //Si hay fila elegida
      if (indice != BWTEnum.NOTFOUND) {
        dlgInforme = new DialInfEdoInd(app,
                                       Integer.toString( ( (DataNotInd) (lista.
            elementAt(indice))).getNumEdo()));
        dlgInforme.setEnabled(true);
        if (dlgInforme.GenerarInforme()) {
          dlgInforme.show();
        }
        setCurrent(indice);
      }

      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  void tabla_itemStateChanged(JCItemEvent evt) {
    m_itemSelecc = tabla.getSelectedIndex();

  }

} //CLASE

// action listener para los botones
class PanEnvUrgActionListener
    implements ActionListener, Runnable {
  PanEnvUrg adaptee = null;
  ActionEvent e = null;

  public PanEnvUrgActionListener(PanEnvUrg adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if (e.getActionCommand().equals("GenFic")) {
      adaptee.btnGenFic_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("GenListado")) {
      adaptee.btnListado_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("GenCarta")) {
      adaptee.btnCarta_actionPerformed(e);
    }
  }
}

class PanEnvUrg_txtAno_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanEnvUrg adaptee;
  FocusEvent e;

  PanEnvUrg_txtAno_focusAdapter(PanEnvUrg adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtAno_focusLost(e);
  }
}

class PanEnvUrg_choiceEnv_itemAdapter
    implements java.awt.event.ItemListener, Runnable {
  PanEnvUrg adaptee;
  ItemEvent e;

  PanEnvUrg_choiceEnv_itemAdapter(PanEnvUrg adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    adaptee.choiceEnv_itemStateChanged(e);
  }
}

// escuchador de los doble click en la tabla
class PanEnvUrg_tabla_actionAdapter
    implements jclass.bwt.JCActionListener {
  PanEnvUrg adaptee;

  PanEnvUrg_tabla_actionAdapter(PanEnvUrg adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    GestionTabla th = new GestionTabla(adaptee);
    th.run();
  }
}

// hilo de ejecuci�n para la gesti�n del doble click en la tabla
class GestionTabla
    implements Runnable {
  PanEnvUrg adaptee;

  public GestionTabla(PanEnvUrg l) {
    adaptee = l;
    Thread th = new Thread();
    th.run();
  }

  // obtiene la nueva trama de la lista
  public void run() {
    adaptee.tabla_actionPerformed();
  }
}

//Gesti�n de los click
//Tambi�n implementa Runnable por si usuario pulsa m�s..
class PanEnvUrgItemListener
    implements JCItemListener, Runnable {
  PanEnvUrg adaptee = null;
  JCItemEvent e = null;

  public PanEnvUrgItemListener(PanEnvUrg adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      this.e = e;
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.tabla_itemStateChanged(e);
  }
}
/*
 class Hilo extends Thread
 {
 DialInfEdoInd dlgInforme;
 CApp app;
 String num;
 Hilo(CApp app, String num)
 {
  this.app = app;
  this.num = num;
  this.yield();
 }
 public void run()
 {
        //ARG: Ahora se le pasa una lista de claves.
        //dlgInforme = new DialInfEdoInd(app,casos);
        dlgInforme = new DialInfEdoInd(app,num) ;
        dlgInforme.setEnabled(true);
        if (dlgInforme.GenerarInformeCompleto() )
           dlgInforme.show();
 }
 }
 */
