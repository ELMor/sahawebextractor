
package envressem;

import java.io.Serializable;

public class DataProd
    implements Serializable {

  protected String sCodEnf = "";
  protected String sCodTVi = "";

  protected String sCodNiv1 = "";
  protected String sCodNiv2 = "";
  protected String sCodZbs = "";
  protected String sCodPro = "";
  protected String sCodMun = "";

  protected String sCodAno = "";
  protected String sCodSem = "";

  //DAtos de tabla SIVE_EDONUM o de tabla SIVE_EDOIND
  protected int iNumCas = 0;

  public DataProd(String codEnf, String codTVi,
                  String codNiv1, String codNiv2, String codZbs, String codPro,
                  String codMun,
                  String codAno, String codSem) {

    sCodEnf = codEnf;
    sCodTVi = codTVi;
    sCodNiv1 = codNiv1;
    sCodNiv2 = codNiv2;
    sCodZbs = codZbs;
    sCodPro = codPro;
    sCodMun = codMun;
    sCodAno = codAno;
    sCodSem = codSem;
  }

  public String getCodEnf() {
    return sCodEnf;
  }

  public String getCodTVi() {
    return sCodTVi;
  }

  public String getCodNiv1() {
    return sCodNiv1;
  }

  public String getCodNiv2() {
    return sCodNiv2;
  }

  public String getCodZbs() {
    return sCodZbs;
  }

  public String getCodPro() {
    return sCodPro;
  }

  public String getCodMun() {
    return sCodMun;
  }

  public String getCodAno() {
    return sCodAno;
  }

  public String getCodSem() {
    return sCodSem;
  }

  public int getNumCas() {
    return iNumCas;
  }

  public void setNumCas(int numCas) {
    iNumCas = numCas;
  }

}
