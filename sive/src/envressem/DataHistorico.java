// Clase para representar el estado de los resumenes EDO.
package envressem;

import java.io.Serializable;

public class DataHistorico
    implements Serializable {

  public String sCodPet = "";
  public String sSemIni = "";
  public String sSemFin = "";
  public String sFechaIni = "";
  public String sFechaFin = "";
  public String sEstado = "";
  public String sDesError = "";

  public DataHistorico() {
  }

  public DataHistorico(String codPet, String semIni, String semFin,
                       String fechaIni, String fechaFin, String estado,
                       String desError) {

    sCodPet = codPet;
    sSemIni = semIni;
    sSemFin = semFin;
    sFechaIni = fechaIni;
    sFechaFin = fechaFin;
    sEstado = estado;
    sDesError = desError;

  }

}