package envressem;

import java.util.ResourceBundle;

import capp.CApp;

public class envressem
    extends CApp {

  ResourceBundle res;

  public envressem() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("envressem.Res" + this.getIdioma());
    setTitulo(res.getString("msg2.Text"));
    CApp a = (CApp)this;
    // abrimos el panel con el modo oportuno
    VerPanel("", new PanResSem(a));
  }

}
