//Servlet usado para env�os urgentes

package envressem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Vector;

import Registro.RegistroConsultas;
import capp.CLista;
import enfermo.Fechas;
import sapp.DBServlet;

public class SrvEnvUrg
    extends DBServlet {

  final int servletOBTENER_ULTIMO_ANO_Y_ENVIOS = 1;
  final int servletOBTENER_UN_ANO_Y_ENVIOS = 2;
  final int servletSELECCION_EDOIND_URG = 3;
  final int servletGENFICH_EDOIND_URG = 4;

  public final static int NO_HAY_EDAD = 0;
  public final static int EDAD_EN_MESES = 1;
  public final static int EDAD_EN_A�OS = 2;

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int k; //Para �ndice b�squeda columnas

    // objetos de datos
    //SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    CLista data = null; //lista de resultado al cliente (lineas, es decir, Strings)
    DataAnoEnvUrg datAnoUrg = null;
    String sLinea = null;
    Enumeration enum; //Para recorrer listas

    //Par�metros recibidos para generar fichero
    String sSep = "|"; //Caracter separador entre datos de una l�nea
    String sAno = "";
    String sSem = "";

    //_____________________ Querys y par�metors nuevos usados en generacion y envio RES SEMANAL_______________________________
    String sCodPro = "";
    String sCodMun = "";

    //_____________________ Querys y par�metors a�adidos usados en envios (urgente)_______________________________

    final String sBUSQUEDA_ULTIMO_ANO =
        "select MAX(CD_ANOEPI) as MAXANOEPI from SIVE_SEMANA_EPI ";
    final String sBUSQUEDA_UN_ANO =
        "select * from SIVE_SEMANA_EPI where CD_ANOEPI = ? ";
    final String sLISTADO_ENVIOS_UN_ANO = "select NM_ENVIOURGSEM, FC_ENVIOURGSEM from SIVE_ENVURG where CD_ANOURG = ? order by NM_ENVIOURGSEM desc";

    //Para recoger campos version y OK de tabla catalogos y meterlos a lineas fichero
    final String sBUSQUEDA_CATALOGOS =
        "select * from SIVE_CATALOGOS where CD_TABLA = ? ";

    //Seleccion edos urgentes no enviados a�n
    final String sLISTADO_LOS_EDOS_URG_NUEVO =
        " select e.NM_EDO, e.CD_ANOEPI, n.CD_SEMEPI, p.DS_PROCESO, " +
        " e.CD_CLASIFDIAG, e.CD_PROV, e.CD_MUN, e.CD_ENFERMO, n.FC_RECEP, e.CD_ENFCIE, e.FC_FECNOTIF " +
        " from SIVE_NOTIF_EDOI n, SIVE_EDOIND e, SIVE_PROCESOS p where " +
        " e.CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where IT_INDURG = 'S' and IT_BAJA ='N' )"
        + " and n.NM_EDO = e.NM_EDO and e.CD_ENFCIE = p.CD_ENFCIE and NM_ENVIOURGSEM is null order by p.DS_PROCESO";
    //final String sLISTADO_LOS_EDOS_URG_NUEVO = " select * from SIVE_EDOIND where " +
    //" CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where IT_INDURG = 'S' and IT_BAJA ='N' )"
    //+ " and NM_ENVIOURGSEM is null order by FC_RECEP DESC";

    //Seleccion edos urgentes ya enviados antes
    // ARG: Ahora se hace un join con SIVE_NOTIF_EDOI
    final String sLISTADO_LOS_EDOS_URG_YA_ENVIADO =
        "select e.NM_EDO, e.CD_ANOEPI, n.CD_SEMEPI, p.DS_PROCESO, " +
        " e.CD_CLASIFDIAG, e.CD_PROV, e.CD_MUN, e.CD_ENFERMO, n.FC_RECEP, e.CD_ENFCIE, e.FC_FECNOTIF " +
        " from SIVE_NOTIF_EDOI n, SIVE_EDOIND e, SIVE_PROCESOS p where " +
        " e.CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where IT_INDURG = 'S' and IT_BAJA ='N' )" +
        " and n.NM_EDO = e.NM_EDO and e.CD_ENFCIE = p.CD_ENFCIE and e.NM_ENVIOURGSEM = ? and e.CD_ANOURG = ?  " +
        " order by p.DS_PROCESO";

    //final String sLISTADO_LOS_EDOS_URG_YA_ENVIADO = " select * from SIVE_EDOIND where " +
    //" CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where IT_INDURG = 'S' and IT_BAJA ='N' )"
    //+ " and NM_ENVIOURGSEM = ? and CD_ANOURG = ?  order by FC_RECEP DESC";

    final String sBUSQUEDA_ENFEREDO =
        "select CD_ENFERE from SIVE_ENFEREDO where CD_ENFCIE = ?";

    final String sLISTADO_ENFERMO = "select SIGLAS, FC_NAC, CD_SEXO, DS_NOMBRE, DS_APE1, DS_APE2 from SIVE_ENFERMO where CD_ENFERMO = ? ";
//     final String sLISTADO_MODELOS = "select CD_MODELO from SIVE_MODELO where CD_TSIVE = 'E' and CD_NIVEL_1 is null and CD_NIVEL_2 is null and CD_CA is null and CD_ENFCIE = ? ";
    final String sLISTADO_MODELOS = "select CD_MODELO from SIVE_MODELO where CD_TSIVE = 'E' and CD_NIVEL_1 is null and CD_NIVEL_2 is null and CD_CA is null and CD_MODELO in " +
        " (select CD_MODELO from SIVE_RESP_EDO where NM_EDO = ?)";
    // ARG: Para obtener el proceso
    final String sLISTADO_PROCESO =
        "select DS_PROCESO from SIVE_PROCESOS where CD_ENFCIE = ? ";

    final String sLISTADO_LINEAS =
        "select NM_LIN from SIVE_LINEA_ITEM where CD_MODELO = ? order by NM_LIN";
    final String sLISTADO_RESPUESTAS = "select DS_RESPUESTA from SIVE_RESP_EDO where NM_EDO = ? and CD_MODELO = ?  and NM_LIN = ? ";

    final String sOBTENER_NUM_ENVIO =
        " select MAX(NM_ENVIOURGSEM) AS MAXNM from SIVE_ENVURG where CD_ANOURG =  ?";
    //Para actualizar el num de envio
    final String sMODIFICACION_LOS_EDOS_URG =
        "UPDATE SIVE_EDOIND SET CD_ANOURG = ?, NM_ENVIOURGSEM =? where "
        + " NM_EDO = ?";
    //Para crear un env�o en tabla de env�os
    final String sALTA_ENVURG = "INSERT INTO SIVE_ENVURG ( CD_ANOURG, NM_ENVIOURGSEM, FC_ENVIOURGSEM ) VALUES (?,?,?)";

    //Envios de un a�o (tablas SIVE_ANOEPI y SIVE_ENVURG)
    boolean bHayAno = false;
    int iNumEnv; //N�mero d3e envio
    java.sql.Date dFecEnv = null; //Fecha env�o
    String sFecEnv = ""; //Fecha envio
    Vector vEnvios = null;

    //Para seleccion de tabla CATALOGOS
    String sCodTab = "";
    String sCodVer = "";
    String sItOk = "";

    //Variables para envio ind. urgente
    int cuentaNoNulos = 0;
    int iMaxEnv = 0;

    String sComAut = "";
    String sTSive = "";

    DataNotInd datoNotInd = null;
    int iNumEdo;
    String sTipCas = "";
    int iCodEnfermo;
    String sCodEnfCie = "";
    java.sql.Date dFecNot = null;
    java.sql.Date dFecRec = null; //Fecha de recepci�n
    String sFecRec = "";

    String sSig = ""; //*****
    String sCodEnfEdo = ""; //***************
    String sNombre = "";
    String sApe1 = "";
    String sApe2 = "";
    String sProceso = "";

    java.sql.Date dFecNac = null;
    String sCodSex = "";
    // Para c�lculo de edad del enfermo
    java.util.Date laFecha; //Fecha de la que extraeremos a�os , meses
    String fechaNac = "";
    String fechaNot = "";
    int anyoNac = 0;
    int anyoNot = 0;
    int mesNac = 0;
    int mesNot = 0;
    int anyos = 0;
    int meses = 0;
    Calendar cal = new GregorianCalendar();
    String laEdad = "";

    String sCodMod = "";
    int iNumLin = 0;
    String sDesRes = "";

    java.util.Date dFecha = null;
    java.sql.Date sqlFec = null;

    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      //___________________________________________________________________

//# // System_out.println("Entra en Servlet BIS*********************");
      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    //Extrae par�metros de consulta
    datAnoUrg = (DataAnoEnvUrg) param.firstElement();

    // prepara la lista de resultados
    data = new CLista();

    // modos de operaci�n
    switch (opmode) {

      case servletOBTENER_ULTIMO_ANO_Y_ENVIOS:

//# // System_out.println("Entra en obtener ultimo *********************");
        //______________________  Obtener �ltimo a�o ___________________________

        // prepara la query
        st = con.prepareStatement(sBUSQUEDA_ULTIMO_ANO);
//# // System_out.println("prep busqueda ultimo a�o *********************");
        rs = st.executeQuery();

        // extrae la p�gina requerida  Exactamente como en el select normalmente
        while (rs.next()) {

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          //A�o m�ximo. Siempre distinto de null si tabla a�os no vac�a, pues todos los a�os son distintos de null
          //sAno= rs.getString("MAX(CD_ANOEPI)");
          sAno = rs.getString("MAXANOEPI");

          if (sAno == null) {
            bHayAno = false;
          }
          else {
            bHayAno = true;
          }
        }
        rs.close();
        st.close();
        rs = null;
        st = null;

//# // System_out.println("OBTIENE ULT A�O *********************");
//        data.addElement(new DataConvFec(datConvFec.getAno(),vFec, bHayFec ));

        //______________________  Obtener sus env�os ___________________________

        //Si se ha recogido max(ano) , es decir, siempre que haya alg�n a�o en b. datos
        if (bHayAno == true) {

          // prepara la query
          st = con.prepareStatement(sLISTADO_ENVIOS_UN_ANO);
//# // System_out.println("prep envios un a�o *********************");
          // filtro
          st.setString(1, sAno);

          rs = st.executeQuery();

          // obtiene los campos
          vEnvios = new Vector();

          // extrae la p�gina requerida  Exactamente como en el select normalmente
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            //N�mero d3e envio
            iNumEnv = rs.getInt("NM_ENVIOURGSEM");
            //Fecha
            dFecEnv = rs.getDate("FC_ENVIOURGSEM");
            sFecEnv = Fechas.date2String(dFecEnv);
            vEnvios.addElement(new NumyFecEnv(iNumEnv, sFecEnv));
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          // a�ade un nodo
          data.addElement(new DataAnoEnvUrg(sAno, true, vEnvios));
//# // System_out.println("OBTENIDOS ENVIOS *********************");

        } // if hay a�o

        else {
          // a�ade un nodo indicando que no hay a�o
          data.addElement(new DataAnoEnvUrg(sAno, false, vEnvios));
        }

        break;

      case servletOBTENER_UN_ANO_Y_ENVIOS:

//# // System_out.println("Entra en obtener un a�o *********************");
        //______________________  Obtener un a�o ___________________________

        //Se recogen par�metros
        sAno = datAnoUrg.getCodAnoUrg().trim();

        // prepara la query
        st = con.prepareStatement(sBUSQUEDA_UN_ANO);
//# // System_out.println("prep busqueda un a�o *********************");
        st.setString(1, datAnoUrg.getCodAnoUrg().trim());

        rs = st.executeQuery();

        // extrae la p�gina requerida  Exactamente como en el select normalmente
        while (rs.next()) {

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          //A�o m�ximo. Siempre distinto de null si tabla a�os no vac�a, pues todos los a�os son distintos de null
          sAno = rs.getString("CD_ANOEPI");
          bHayAno = true;
        }
        rs.close();
        st.close();
        rs = null;
        st = null;

//# // System_out.println("COMPRUEBA A�O *********************");
        //______________________  Obtener sus env�os ___________________________

        //Si se ha recogido max(ano) , es decir, siempre que haya alg�n a�o en b. datos
        if (bHayAno == true) {

          // prepara la query
          st = con.prepareStatement(sLISTADO_ENVIOS_UN_ANO);
//# // System_out.println("prep envios un a�o *********************");
          // filtro
          st.setString(1, sAno);

          rs = st.executeQuery();

          // obtiene los campos
          vEnvios = new Vector();

          // extrae la p�gina requerida  Exactamente como en el select normalmente
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            //N�mero d3e envio
            iNumEnv = rs.getInt("NM_ENVIOURGSEM");
            //Fecha
            dFecEnv = rs.getDate("FC_ENVIOURGSEM");
            sFecEnv = Fechas.date2String(dFecEnv);
//# // System_out.println("Fecha*************************** " + sFecEnv);
            vEnvios.addElement(new NumyFecEnv(iNumEnv, sFecEnv));
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

//# // System_out.println("OBTIENE ENVIOS *********************");

          // a�ade un nodo
          data.addElement(new DataAnoEnvUrg(sAno, true, vEnvios));

        } // if hay a�o

        else {
          // a�ade un nodo indicando que no hay a�o
          data.addElement(new DataAnoEnvUrg(sAno, false, vEnvios));
        }

        break;

        //======================== Modo envio edos para tabla ===================================

      case servletSELECCION_EDOIND_URG:

//# // System_out.println("Entra en obtener edos urg *********************");

        //______________________  Consulta casos edo ind urgentes___________________________

        //Caso de nuevo env�o
        if ( ( (NumyFecEnv) (datAnoUrg.getEnvios().firstElement())).getNumEnv() ==
            -1) {
          // prepara la query
          st = con.prepareStatement(sLISTADO_LOS_EDOS_URG_NUEVO);

          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_EDOIND",
                                      sLISTADO_LOS_EDOS_URG_NUEVO, "CD_ENFERMO",
                                      "", "SrvEnvUrg", true);

//# // System_out.println("prep edos urg nuevo *********************");
        }
        //Caso de env�o yq hecho anteriormente
        else {
          // prepara la query
          st = con.prepareStatement(sLISTADO_LOS_EDOS_URG_YA_ENVIADO);
//# // System_out.println("prep edos urg ya enviado *********************");
          //N�mero de env�o
          st.setInt(1,
                    ( (NumyFecEnv) (datAnoUrg.getEnvios().firstElement())).getNumEnv());

          // ARG: Se a�aden parametros
          registroConsultas.insertarParametro(String.valueOf( ( (NumyFecEnv) (
              datAnoUrg.getEnvios().firstElement())).getNumEnv()));

          //A�o del env�o anterior
          st.setString(2, datAnoUrg.getCodAnoUrg().trim());

          // ARG: Se a�aden parametros
          registroConsultas.insertarParametro(datAnoUrg.getCodAnoUrg().trim());

          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_EDOIND",
                                      sLISTADO_LOS_EDOS_URG_YA_ENVIADO,
                                      "CD_ENFERMO",
                                      "", "SrvEnvUrg", true);
        }

//# // System_out.println("Prep los edos nuevo*********************");

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          // obtiene los campos

          iNumEdo = rs.getInt("NM_EDO");
          sAno = rs.getString("CD_ANOEPI");
          sSem = rs.getString("CD_SEMEPI");
          sTipCas = rs.getString("CD_CLASIFDIAG");
          if (sTipCas == null) {
            sTipCas = "";
          }
          sCodPro = rs.getString("CD_PROV");
          if (sCodPro == null) {
            sCodPro = "";
          }
          sCodMun = rs.getString("CD_MUN");
          if (sCodMun == null) {
            sCodMun = "";
          }
          iCodEnfermo = rs.getInt("CD_ENFERMO");
          dFecRec = rs.getDate("FC_RECEP");
          sFecRec = Fechas.date2String(dFecRec);
          sCodEnfCie = rs.getString("CD_ENFCIE");
          dFecNot = rs.getDate("FC_FECNOTIF");
          sProceso = rs.getString("DS_PROCESO");
          if (sProceso == null) {
            sProceso = "";

            // a�ade un nodo
          }
          data.addElement(new DataNotInd(iNumEdo, sCodPro, sCodMun, iCodEnfermo,
                                         sCodEnfCie, dFecNot, sTipCas, sAno,
                                         sSem));
          ( (DataNotInd) (data.lastElement())).setFecRec(sFecRec);
          // ARG: A�ade la descripci�n del proceso.
          ( (DataNotInd) (data.lastElement())).setProceso(sProceso);

        }
        rs.close();
        st.close();
        rs = null;
        st = null;

        //Bucle para recoger datos de ENFEDO y SIGLAS
        for (int cont = 0; cont < data.size(); cont++) {
          datoNotInd = (DataNotInd) (data.elementAt(cont));

          //_______________ Busca ENFEDO______________________________

          // prepara la query
          st = con.prepareStatement(sBUSQUEDA_ENFEREDO);
//# // System_out.println("prep envios un a�o *********************");
          // filtro
          st.setString(1, datoNotInd.getCodEnfCie());

          rs = st.executeQuery();

          // extrae la p�gina requerida  Exactamente como en el select normalmente
          while (rs.next()) {

            //N�mero d3e envio
            sCodEnfEdo = rs.getString("CD_ENFERE");
          }
          ( (DataNotInd) (data.elementAt(cont))).setCodEnfEdo(sCodEnfEdo);

          rs.close();
          st.close();
          rs = null;
          st = null;

          //_______________ Busca Siglas______________________________

          st = con.prepareStatement(sLISTADO_ENFERMO);
          st.setInt(1, datoNotInd.getCodEnfermo());

          // ARG: Se a�aden parametros
          registroConsultas.insertarParametro(String.valueOf(datoNotInd.
              getCodEnfermo()));

          rs = st.executeQuery();

          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_ENFERMO", sLISTADO_ENFERMO,
                                      "CD_ENFERMO",
                                      String.valueOf(datoNotInd.getCodEnfermo()),
                                      "SrvEnvUrg", true);

          // extrae la p�gina requerida
          while (rs.next()) {
            // obtiene los campos
            sSig = rs.getString("SIGLAS");
            dFecNac = rs.getDate("FC_NAC");
            sCodSex = rs.getString("CD_SEXO");
            if (sCodSex == null) {
              sCodSex = "";
            }
            sNombre = rs.getString("DS_NOMBRE");
            if (sNombre == null) {
              sNombre = "";
            }
            sApe1 = rs.getString("DS_APE1");
            if (sApe1 == null) {
              sApe1 = "";
            }
            sApe2 = rs.getString("DS_APE2");
            if (sApe2 == null) {
              sApe2 = "";

            }
          }

          datoNotInd.setSiglas(sSig);
          datoNotInd.setNombre(sNombre);
          datoNotInd.setApe1(sApe1);
          datoNotInd.setApe2(sApe2);

          //C�lculo de la edad y relleno de datos en elemento

          rs.close();
          st.close();
          st = null;
          rs = null;

        } //For

        break;

        //======================== Modo ENV�O DEL FICHero ===================================
        // listado

      case servletGENFICH_EDOIND_URG:

//# // System_out.println("%%Entra en  gen fichero *********************");

        //Se crean listas que se van a enviar
        data.addElement(new DataResInd());
        ( (DataResInd) (data.firstElement())).resumen = new CLista();
        ( (DataResInd) (data.firstElement())).errores = new CLista();
        ( (DataResInd) (data.firstElement())).catalogos = new CLista();

        //prepara la lista auxiliar
        CLista dataAux = new CLista();

        //Recogida Par�mettros enviados para el envio semanal y el urgente
        sSep = datAnoUrg.getSep();
        sComAut = datAnoUrg.getComAut();
        sTSive = datAnoUrg.getTSive();

//# // System_out.println("%%Recogidos paramentros*********************");

        try {

          //Se va a modificar la b. datos al final
          con.setAutoCommit(false);

//______Se a�aden lineas con datos de tablas CATALOGOS_____________________

          //____  Varias Consultas en cat�logos, una para cada c�digo de tabla
          //Si es necesario Se a�aden las correspondientes tres lineas de fichero en cada caso

          for (int contad = 0; contad < 5; contad++) {

            // prepara la query
            st = con.prepareStatement(sBUSQUEDA_CATALOGOS);

            //Codigo Tabla
            if (contad == 0) {
              sCodTab = "SIVE_SEXO";
            }
            else if (contad == 1) {
              sCodTab = "SIVE_CLASIF_EDO";
            }
            else if (contad == 2) {
              sCodTab = "SIVE_PROVINCIA";
            }
            else if (contad == 3) {
              sCodTab = "SIVE_COM_AUT";
            }
            else if (contad == 4) {
              sCodTab = "SIVE_MUNICIPIO";

            }
            st.setString(1, sCodTab);

            rs = st.executeQuery();

            // extrae la p�gina requerida (como m�ximo ser� una en cada consulta
            while (rs.next()) {

              // obtiene los campos
              sCodVer = rs.getString("CD_VERSION");
              sItOk = rs.getString("IT_OK");

              // a�ade linea indica tabla
              sLinea = "[" + sCodTab + "]";
              ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                  sLinea);

              // a�ade linea indica version
              sLinea = "version=";
              sLinea = sLinea + sCodVer;
              ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                  sLinea);

              // a�ade linea indica OK
              sLinea = "OK=";
              sLinea = sLinea + sItOk;
              ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                  sLinea);
            }
            rs.close();
            st.close();
            rs = null;
            st = null;

          } //Fin for

          //______________________  Consulta casos edo ind urgentes___________________________

          //Caso de nuevo env�o
          if ( ( (NumyFecEnv) (datAnoUrg.getEnvios().firstElement())).getNumEnv() ==
              -1) {
            // prepara la query
            st = con.prepareStatement(sLISTADO_LOS_EDOS_URG_NUEVO);

            // ARG: Registro LORTAD
            registroConsultas.registrar("SIVE_EDOIND",
                                        sLISTADO_LOS_EDOS_URG_NUEVO,
                                        "CD_ENFERMO",
                                        "", "SrvEnvUrg", true);

//# // System_out.println("%%prep edos urg nuevo para gen fich*********************");
          }
          //Caso de env�o ya hecho anteriormente
          else {
            // prepara la query
            st = con.prepareStatement(sLISTADO_LOS_EDOS_URG_YA_ENVIADO);
//# // System_out.println("%%prep edos urg enviado para genfich *********************");
            //N�mero de env�o
            st.setInt(1,
                      ( (NumyFecEnv) (datAnoUrg.getEnvios().firstElement())).getNumEnv());

            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(String.valueOf( ( (NumyFecEnv) (
                datAnoUrg.getEnvios().firstElement())).getNumEnv()));

            //A�o del env�o anterior
            st.setString(2, datAnoUrg.getCodAnoUrg().trim());

            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(datAnoUrg.getCodAnoUrg().trim());

            // ARG: Registro LORTAD
            registroConsultas.registrar("SIVE_EDOIND",
                                        sLISTADO_LOS_EDOS_URG_YA_ENVIADO,
                                        "CD_ENFERMO",
                                        "", "SrvEnvUrg", true);
          }

//# // System_out.println("%%Prep los edos*********************");

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // obtiene los campos

            //Tanto Strings obligatorios como no obligatorios se obtienen igual
            //En cliente se convertir�n los Strings nulos a cadenas""

            iNumEdo = rs.getInt("NM_EDO");
            sAno = rs.getString("CD_ANOEPI");
            sSem = rs.getString("CD_SEMEPI");
            sTipCas = rs.getString("CD_CLASIFDIAG");
            if (sTipCas == null) {
              sTipCas = "";
            }
            sCodPro = rs.getString("CD_PROV");
            if (sCodPro == null) {
              sCodPro = "";
            }
            sCodMun = rs.getString("CD_MUN");
            if (sCodMun == null) {
              sCodMun = "";
            }
            iCodEnfermo = rs.getInt("CD_ENFERMO");
            dFecRec = rs.getDate("FC_RECEP"); //*******************
            sFecRec = Fechas.date2String(dFecRec);

            sCodEnfCie = rs.getString("CD_ENFCIE");
            dFecNot = rs.getDate("FC_FECNOTIF");
            sProceso = rs.getString("DS_PROCESO");
            if (sProceso == null) {
              sProceso = "";

              // a�ade un nodo
            }
            dataAux.addElement(new DataNotInd(iNumEdo, sCodPro, sCodMun,
                                              iCodEnfermo, sCodEnfCie, dFecNot,
                                              sTipCas, sAno, sSem));
            ( (DataNotInd) (dataAux.lastElement())).setFecRec(sFecRec);
            // ARG: A�ade la descripci�n del proceso.
            ( (DataNotInd) (dataAux.lastElement())).setProceso(sProceso);

          }
          rs.close();
          st.close();
          rs = null;
          st = null;
//# // System_out.println("%%OBTIENE EDOS URG PARA FICHERO*********************");

          //Caso de nuevo env�o
          if ( ( (NumyFecEnv) (datAnoUrg.getEnvios().firstElement())).getNumEnv() ==
              -1) {

            //______  Consulta en  SIVE_EDOIND para ver el MAX(NM:ENVIOURGSEM___________________________

            st = con.prepareStatement(sOBTENER_NUM_ENVIO);

            st.setString(1, datAnoUrg.getCodAnoUrg().trim());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              //iMaxEnv = rs.getInt("MAX(NM_ENVIOURGSEM)");
              iMaxEnv = rs.getInt("MAXNM");
              //# // System_out.println("%%Cog Max************" + iMaxEnv);
            }

            rs.close();
            st.close();
            st = null;
            rs = null;
//# // System_out.println("%% OBTIENE MAX *********************");

            //______  Alta en tabla ENVURG ___________________________

            if (dataAux.elements().hasMoreElements()) {
              // lanza la query
              st = con.prepareStatement(sALTA_ENVURG);
              //A�o al que corresponde el evvio
              st.setString(1, datAnoUrg.getCodAnoUrg().trim());
              //Valor de num envio que se actualiza
              int iEntrada = iMaxEnv + 1;
              st.setInt(2, iEntrada);
              // codigo buscado

              // ARG: Se guarda la fecha elegida, no la actual
              SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
              dFecha = formater.parse( ( (NumyFecEnv) (datAnoUrg.getEnvios().
                  firstElement())).getFecEnv());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(3, sqlFec);
              //java.util.Date fecHoy = new java.util.Date();
              //st.setDate(3, new java.sql.Date(fecHoy.getTime()) ) ;

              st.executeUpdate();
              st.close();
              st = null;
            }
          }

          //______________________  rCONSULTA EN SIVE_ENFEREDO___________________________
          enum = dataAux.elements();
          while (enum.hasMoreElements()) { //Recorremos lista
            DataNotInd datNotInd = (DataNotInd) (enum.nextElement());
            //# // System_out.println("%%extrae elem de lista  *********************");

            // prepara la query
            st = con.prepareStatement(sBUSQUEDA_ENFEREDO);
//# // System_out.println("prep envios un a�o *********************");
            // filtro
            st.setString(1, datNotInd.getCodEnfCie());

            rs = st.executeQuery();

            // extrae la p�gina requerida  Exactamente como en el select normalmente
            while (rs.next()) {

              //N�mero d3e envio
              sCodEnfEdo = rs.getString("CD_ENFERE");
            }
            datNotInd.setCodEnfEdo(sCodEnfEdo);

            rs.close();
            st.close();
            rs = null;
            st = null;

// // System_out.println("%%OBTENIDO ENFEDO *********************");

            //______________________  Consulta en SIVE_ENFERMO___________________________

            st = con.prepareStatement(sLISTADO_ENFERMO);
////# // System_out.println("Prep Enfermo*********************");
            st.setInt(1, datNotInd.getCodEnfermo());

            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(String.valueOf(datNotInd.
                getCodEnfermo()));

            rs = st.executeQuery();

            // ARG: Registro LORTAD
            registroConsultas.registrar("SIVE_ENFERMO", sLISTADO_ENFERMO,
                                        "CD_ENFERMO",
                                        String.valueOf(datNotInd.getCodEnfermo()),
                                        "SrvEnvUrg", true);

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              sSig = rs.getString("SIGLAS");
              dFecNac = rs.getDate("FC_NAC");
              sCodSex = rs.getString("CD_SEXO");
              if (sCodSex == null) {
                sCodSex = "";
              }
              sNombre = rs.getString("DS_NOMBRE");
              if (sNombre == null) {
                sNombre = "";
              }
              sApe1 = rs.getString("DS_APE1");
              if (sApe1 == null) {
                sApe1 = "";
              }
              sApe2 = rs.getString("DS_APE2");
              if (sApe2 == null) {
                sApe2 = "";
              }
            }

            datNotInd.setSiglas(sSig);
            datNotInd.setNombre(sNombre);
            datNotInd.setApe1(sApe1);
            datNotInd.setApe2(sApe2);

            //C�lculo de la edad y relleno de datos en elemento

            if (dFecNac != null) {
              //   SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
              //     laFecha = formateador.parse(fechaNac);

              //Se obtiene el a�o y mes de nacimiento
              cal.setTime(dFecNac);
              anyoNac = cal.get(Calendar.YEAR);
              mesNac = cal.get(Calendar.MONTH);
              //Se obtiene a�o y mes de notificaci�n
              cal.setTime(datNotInd.getFecNot());
              anyoNot = cal.get(Calendar.YEAR);
              mesNot = cal.get(Calendar.MONTH);
              //Se ve la diferencia entre fechas en a�os y meses
              if (mesNot > mesNac) {
                anyos = anyoNot - anyoNac;
                meses = mesNot - mesNac;
              }
              else if (mesNot == mesNac) {
                anyos = anyoNot - anyoNac;
                meses = 0;
              }
              else {
                anyos = (anyoNot - anyoNac) - 1;
                meses = (mesNot + 12) - mesNac;
              }

              //Se rellenan datos de edad y sexo del caso EDO
              // indicando la fecha en a�os o meses segun corresponda
              if (anyos <= 0) {
                //Se da la edad en meses
                laEdad = Integer.toString(meses);
                datNotInd.setEdadySexo(laEdad, EDAD_EN_MESES, sCodSex);
              }
              else {
                //Se da la edad en a�os
                laEdad = Integer.toString(anyos);
                datNotInd.setEdadySexo(laEdad, EDAD_EN_A�OS, sCodSex);
              }
            }
            //No se tiene dato de la edad
            else {
              laEdad = "";
              datNotInd.setEdadySexo(laEdad, NO_HAY_EDAD, sCodSex);
            }

            rs.close();
            st.close();
            st = null;
            rs = null;
//# // System_out.println("%%OBTIENE DATOS ENFERMO*********************");

            //Caso de nuevo env�o
            if ( ( (NumyFecEnv) (datAnoUrg.getEnvios().firstElement())).
                getNumEnv() == -1) {

              //______________________  Actualiza secuencia de envio en tabla EDOIND para los urgentes ___________________________

              // lanza la query
              st = con.prepareStatement(sMODIFICACION_LOS_EDOS_URG);
////# // System_out.println("Mod urg *********************");
              //A�o env�o
              st.setString(1, datAnoUrg.getCodAnoUrg().trim());
              //Valor de num envio que se actualiza
              st.setInt(2, iMaxEnv + 1);
              // codigo buscado
              st.setInt(3, datNotInd.getNumEdo());
////# // System_out.println("Pone MaxEnv *********************"+ iMaxEnv + 1);
              st.executeUpdate();
              st.close();
              st = null;

//# // System_out.println("%%MODIFICA EDOIND*********************");
            } // if era nuevo envio

            //______________________  Consulta en SIVE_MODELO___________________________

            int numModelos = 0; //N�mero de modelos que cumplen la query LISTADO_MODELO
            //Si no hay error debe ser solo uno
            st = con.prepareStatement(sLISTADO_MODELOS);

            st.setInt(1, datNotInd.getNumEdo());

            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              sCodMod = rs.getString("CD_MODELO");
              datNotInd.setCodMod(sCodMod);
              numModelos++;
            }
            rs.close();
            st.close();
            st = null;
            rs = null;

//# // System_out.println("%%OBTIENE DATOS MODELO*********************");
            ////# // System_out.println("Fin modelo  *********************");

            //Si hay un solo modelo todo OK
            if (numModelos == 1) {

              //Si es en modo urgente a�o y semana son distintos en cada caso
              if (opmode == servletGENFICH_EDOIND_URG) {
                sAno = datNotInd.getCodAno();
                sSem = datNotInd.getCodSem();
              }

// ARS: 14-03-01. Cambio en los registros del fichero que se va a generar
// para que en vez de incluir el c�digo de enfermedad CIE, incluya el c�digo
// de enfermedad EDO
              /*           sLinea =   Integer.toString(datNotInd.getNumEdo()) + sSep + sComAut + sSep
                   + datNotInd.getCodEnfCie() + sSep  + datNotInd.getCodMun() +  sSep
                   + datNotInd.getCodPro() +sSep + sAno + sSep + sSem + sSep
                          + datNotInd.getEda() + sSep + datNotInd.getTipEda() + sSep + datNotInd.getSex() + sSep  + sTipCas + sSep
                           +sCodMod +sSep;*/

              sLinea = Integer.toString(datNotInd.getNumEdo()) + sSep + sComAut +
                  sSep
                  + datNotInd.getCodEnfEdo() + sSep + datNotInd.getCodMun() +
                  sSep
                  + datNotInd.getCodPro() + sSep + sAno + sSep + sSem + sSep
                  + datNotInd.getEda() + sSep + datNotInd.getTipEda() + sSep +
                  datNotInd.getSex() + sSep + sTipCas + sSep
                  + sCodMod + sSep;

              //______________________  Consulta en  SIVE_LINEA_ITEM___________________________

              st = con.prepareStatement(sLISTADO_LINEAS);
////# // System_out.println("Prep lineas*********************");
              st.setString(1, sCodMod);

              rs = st.executeQuery();

              // extrae la p�gina requerida
              while (rs.next()) {
                // obtiene los campos
                iNumLin = rs.getInt("NM_LIN");
                datNotInd.vLineas.addElement(new DataLinea(iNumLin));

              }
              rs.close();
              st.close();
              st = null;
              rs = null;
////# // System_out.println("OBTIENE LINEAS*********************");

              //______________________  Consulta en  SIVE_RESP_EDO para cada linea ___________________________

              for (int j = 0; j < datNotInd.vLineas.size(); j++) {

                st = con.prepareStatement(sLISTADO_RESPUESTAS);
////# // System_out.println("Prep respuestas*********************");
                st.setInt(1, datNotInd.getNumEdo());
                st.setString(2, datNotInd.getCodMod());
                st.setInt(3,
                          ( (DataLinea) (datNotInd.vLineas.elementAt(j))).getNumLin());

                rs = st.executeQuery();

                // extrae la p�gina requerida
                while (rs.next()) {
                  // obtiene los campos
                  sDesRes = rs.getString("DS_RESPUESTA");

                  //A�ade a la linea del fichero  la respta de la pregunta
                  sLinea = sLinea + sDesRes;
                }
                //Se a�ade separador independientemente de si se ha encontrado respta a la pregunta o no
                sLinea = sLinea + sSep;

                rs.close();
                st.close();
                st = null;
                rs = null;
////# // System_out.println("OBTIENE RESPUESTAS*********************");

              } //For recorre lineas

//# // System_out.println("%%OBTENIDAS LINEAS Y RESPUESTAS*********************");

              // a�ade una linea
              ( ( (DataResInd) (data.firstElement())).resumen).addElement(
                  sLinea);
//# // System_out.println("%% A�ADIDA linea *********************");

            }

            //Si no hay un solo modelo ( hay 0 o m�as)
            else {

              //if hay  m�s de un modelo ERROR
              if (numModelos > 1) {

                ( ( (DataResInd) (data.firstElement())).errores).addElement(
                    new DataError(datNotInd.getNumEdo(), datNotInd.getCodEnfEdo(),
                                  datNotInd.getFecRec(),
                                  Fechas.date2String(datNotInd.getFecNot()),
                                  "M�s de un modelo con resptas. para ese caso"));
              }
              // (numModelos = 0) No se meten datos del modelo
              else {

// ARS: 14-03-01. Cambio en los registros del fichero que se va a generar
// para que en vez de incluir el c�digo de enfermedad CIE, incluya el c�digo
// de enfermedad EDO
                /*           sLinea =  Integer.toString(datNotInd.getNumEdo()) + sSep +  sComAut + sSep
                     + datNotInd.getCodEnfCie() + sSep  + datNotInd.getCodMun() +  sSep
                     + datNotInd.getCodPro() +sSep + sAno + sSep + sSem + sSep
                            + datNotInd.getEda() + sSep + datNotInd.getTipEda() + sSep + datNotInd.getSex() + sSep  + sTipCas + sSep;*/

                sLinea = Integer.toString(datNotInd.getNumEdo()) + sSep +
                    sComAut + sSep
                    + datNotInd.getCodEnfEdo() + sSep + datNotInd.getCodMun() +
                    sSep
                    + datNotInd.getCodPro() + sSep + sAno + sSep + sSem + sSep
                    + datNotInd.getEda() + sSep + datNotInd.getTipEda() + sSep +
                    datNotInd.getSex() + sSep + sTipCas + sSep;

                // a�ade una linea
                ( ( (DataResInd) (data.firstElement())).resumen).addElement(
                    sLinea);
              }
            } //else hay error
          } //while recorrido de vector

          if (opmode == servletGENFICH_EDOIND_URG) {

            // valida la transacci�n de una semana
            con.commit();
          }
        }
        catch (Exception ex) {
          if (opmode == servletGENFICH_EDOIND_URG) {
            con.rollback();
          }
          throw ex;

        }

        break;

    } //fin switch

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
