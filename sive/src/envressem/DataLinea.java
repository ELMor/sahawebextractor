//Clase para representar una l�nea de un modelo

package envressem;

import java.io.Serializable;

public class DataLinea
    implements Serializable {

  int iNumLin;

  public DataLinea(int numLin) {
    iNumLin = numLin;
  }

  public int getNumLin() {
    return iNumLin;
  }

}