//Clase de datos de PETICION para generaci�n re�mnes num�ricos
// generaci�n fichero res. num,  generaci�n fichero individuales
// y generaci�n fichero de brotes

package envressem;

import java.io.Serializable;

public class DataFich
    implements Serializable {

//Usadas para generar ficheros
  protected String sSep = "|"; //Caracter separador entre datos de una l�nea

  protected String sSemDesde;
  protected String sSemHasta;
  protected String sOrigen; //Indica si el origen es Madrid ("M") o C. y L. ("C")

  //Usadas para generar resumen de una semana
  protected String sAno; //Un a�o solo
  protected String sSem; //Para generar de semana en semana
  protected boolean bEsPrimera = false; //Indica si es la �ltima semana o no para
  //saber si debe a�adir datos de tabla CATALOGOS

  protected String sComAut;
  protected String sTSive;

  public DataFich() {
  }

  //Para generaci�n de ficheros res numerico(todo a la vez)
  public DataFich(String sep, String ano, String semDesde, String semHasta) {
    sSep = sep;
    sAno = ano;
    sSemDesde = semDesde;
    sSemHasta = semHasta;
  }

  public DataFich(String sep, String ano, String semDesde, String semHasta,
                  String origen) {
    sSep = sep;
    sAno = ano;
    sSemDesde = semDesde;
    sSemHasta = semHasta;
    sOrigen = origen;
  }

  //Para generaci�n de resumenes num y gen ficheros not indiv (semana a semana)
  public DataFich(String sep, String ano, String sem) {

    sSep = sep;
    sAno = ano;
    sSem = sem;
  }

  //Para generaci�n de ficheros de brotes (un a�o completo siempre)
  public DataFich(String sep, String ano) {

    sSep = sep;
    sAno = ano;
  }

  public String getSep() {
    return sSep;
  }

  public String getAno() {
    return sAno;
  }

  //Para generaci�n de ficheros res numerico(todo a la vez)

  public String getSemDesde() {
    return sSemDesde;
  }

  public String getSemHasta() {
    return sSemHasta;
  }

  //Para generaci�n de resumenes num y gen ficheros not indiv (semana a semana)
  public String getSem() {
    return sSem;
  }

  // ARS 07-02-01 Para poder excluir las que no son de Madrid.
  public void setOrigen(String origen) {
    sOrigen = origen;
  }

  public String getOrigen() {
    return sOrigen;
  }

  public boolean getEsPrimera() {
    return bEsPrimera;
  }

  public void setEsPrimera(boolean esPri) {
    bEsPrimera = esPri;
  }

//Para indicar com aut y TSive en ficheros not indiv
  public void setTSive(String tSive) {
    sTSive = tSive;
  }

  public void setComAut(String comAut) {
    sComAut = comAut;
  }

  public String getTSive() {
    return sTSive;
  }

  public String getComAut() {
    return sComAut;
  }

}
