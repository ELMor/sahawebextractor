//Clase para representar todos los env�os urgentes de un a�o
//Se usa tanto para petici�n como para respuesta

package envressem;

import java.io.Serializable;
import java.util.Vector;

public class DataAnoEnvUrg
    implements Serializable {

  protected String sCodAnoUrg = ""; //C�digo del a�o
  protected boolean bHayAno = false; //Dice si existe el a�o epidemiol�gico o no
  protected Vector vEnvios = new Vector(); //Vector de env�os (fecha y n�mero)

  //Datos para generac��n fichero
//Usadas para generar ficheros
  protected String sSep = "|"; //Caracter separador entre datos de una l�nea
  protected String sComAut;
  protected String sTSive;

  //Constructor para petici�n
  public DataAnoEnvUrg(String codAnoUrg) {
    sCodAnoUrg = codAnoUrg;
  }

  public DataAnoEnvUrg(String codAnoUrg, boolean hayAno, Vector envios) {
    sCodAnoUrg = codAnoUrg;
    bHayAno = hayAno;
    vEnvios = envios;
  }

  public String getCodAnoUrg() {
    return sCodAnoUrg;
  }

  public boolean getHayAno() {
    return bHayAno;
  }

  public Vector getEnvios() {
    return vEnvios;
  }

//Para indicar com aut y TSive cuando haya que generar fichero

  public String getSep() {
    return sSep;
  }

  public void setTSive(String tSive) {
    sTSive = tSive;
  }

  public void setComAut(String comAut) {
    sComAut = comAut;
  }

  public String getTSive() {
    return sTSive;
  }

  public String getComAut() {
    return sComAut;
  }

}