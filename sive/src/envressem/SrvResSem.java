package envressem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import Registro.RegistroConsultas;
import capp.CLista;
import enfermo.Fechas;
import sapp.DBServlet;

public class SrvResSem
    extends DBServlet {

  final int servletGENFICH_RES_NUM = 2;
  final int servletACTUALIZAR_RES_NUM_UNA_SEM = 3;
  final int servletGENFICH_EDOIND = 4;
  final int servlet_MOSTRAR_HISTORICO = 5;

  public final static int NO_HAY_EDAD = 0;
  public final static int EDAD_EN_MESES = 1;
  public final static int EDAD_EN_A�OS = 2;

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  String sLinea = null; //Linea de un fichero

  // ARG: Para usar fechas en las selects
  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    String aux = ts.toString();
    String res = aux.substring(0, 4); //a�o    yyyy
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy
    return res;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int k; //Para �ndice b�squeda columnas

    // objetos de datos
    //SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    CLista data = null; //lista de resultado al cliente (lineas, es decir, Strings)
    DataFich datFich = null;

    Enumeration enum; //Para recorrer listas
    CLista dataEnf = null; //lista de objetos de datos provisionales (depende de cada caso el tipo)
    CLista dataGeo = null;
    CLista dataSem = null; //lista de semanas
    CLista dataProd = null; //Producto cartesiano de tres listas

    //Par�metros recibidos para generar fichero
    String sSep = "|"; //Caracter separador entre datos de una l�nea
    //Si queremos todas las semanas de una vez
    String sSemDesde = "";
    String sSemHasta = "";
    //Si se va semana a semana
    String sAno = "";
    String sSem = "";
    // Indica si el origen es Madrid o C. y L.
    String sOrigen = "";
    boolean bEsPrimera = false; //Dice si es la �ltima semana

    //_____________________ Querys y par�metors nuevos usados en generacion y envio RES SEMANAL_______________________________

    //Parametros usados en listado enfermedades
    String sCodEnf = "";
    String sCodTVi = "";
    //Par�metros usados en listado geografico
    String sCodNiv1 = "";
    String sCodNiv2 = "";
    String sCodZbs = "";
    String sCodPro = "";
    String sCodMun = "";
    //Param en lista semanas
    String sCodSem = "";

    int iNumCas = 0;
    String sNumCas = "";
    String sCodAno = "";

    //Para seleccion de tabla CATALOGOS
    String sCodTab = "";
    String sCodVer = "";
    String sItOk = "";

    //boolean todoOK = true;
    //Vector vSemFallidas = new Vector();  //Vector de Strings de c�digos de semana
    //para las semanas en las que ha fallado la transacci�n

    final String sBUSQUEDA_ANO_ACTUAL =
        "select FC_INIEPI,FC_FINEPI from SIVE_ANO_EPI where CD_ANOEPI = ? ";

    final String sBORRADO_UNA_SEMANA_RESUMEN_EDOS =
        " delete from SIVE_RESUMEN_EDOS where CD_ANOEPI = ? and CD_SEMEPI = ? ";
    final String sALTA_RESUMEN_EDOS = " insert into SIVE_RESUMEN_EDOS ( CD_ENFCIE,CD_ANOEPI,  CD_SEMEPI,CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, CD_PROV, CD_MUN,  NM_CASOS ) values (?,?,?,?,?,?,?,?,?)";

    final String sLISTADO_ENFEREDO =
        " select CD_ENFCIE, CD_TVIGI from SIVE_ENFEREDO where IT_BAJA = 'N' ";
    final String sLISTADO_GEOGRAFICO = "select  distinct a.CD_NIVEL_1, a.CD_NIVEL_2, a.CD_ZBS, b.CD_PROV, b.CD_MUN from SIVE_E_NOTIF a, SIVE_C_NOTIF b where a.CD_CENTRO = b.CD_CENTRO ";
    final String sLISTADO_SEMANA_EPI = "select CD_SEMEPI from SIVE_SEMANA_EPI where CD_ANOEPI = ? and CD_SEMEPI >= ? and CD_SEMEPI <= ? ";

    /* final String sLISTADO_EDO_NUM = "select SUM(NM_CASOS) from SIVE_EDONUM  where  CD_ENFCIE = ? and  CD_ANOEPI = ?  and CD_SEMEPI = ? " +
     "and CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and CD_ZBS = ? " +
     "and CD_CENTRO in (select CD_CENTRO from SIVE_C_NOTIF where CD_PROV = ? and CD_MUN = ?))";
     */
    //DSR: quita caracter final a sLISTADO_EDO_NUM; no se rellenan par�metros; se monta la sql
    String sLISTADO_EDO_NUM = "";

    /* final String sLISTADO_EDO_IND = "select COUNT(a.NM_EDO) from SIVE_EDOIND a, SIVE_NOTIF_EDOI b where a.CD_ENFCIE = ? and a.CD_ANOEPI = ?  and a.CD_SEMEPI = ? " +
     "and b.CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and CD_ZBS = ? " +
     "and CD_CENTRO in (select CD_CENTRO from SIVE_C_NOTIF where CD_PROV = ? and CD_MUN = ?)) and a.NM_EDO = b.NM_EDO and b.CD_FUENTE = 'E' ";
     */
    //DSR:  quita caracter final a sLISTADO_EDO_IND; no se rellenan par�metros; se monta la sql
    String sLISTADO_EDO_IND = "";

    //Para generar el fichero
    final String sLISTADO_RESUMEN_EDOS =
        "select CD_ANOEPI, CD_ENFCIE, CD_SEMEPI, CD_PROV, SUM(NM_CASOS)" +
        " from SIVE_RESUMEN_EDOS" +
        " where CD_ANOEPI = ? and" +
        " CD_SEMEPI >= ? and" +
        " CD_SEMEPI <= ?" +
        " group by CD_ANOEPI, CD_ENFCIE, CD_SEMEPI, CD_PROV";

    //Para recoger campos version y OK de tabla catalogos
    final String sBUSQUEDA_CATALOGOS =
        "select * from SIVE_CATALOGOS where CD_TABLA = ? ";

    //_____________________ Querys y par�metors a�adidos usados en envios EDOIND (semanal )_______________________________

    final String sLISTADO_LOS_EDOS = " select * from SIVE_EDOIND where " +
        " CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where (CD_TVIGI = 'I' or CD_TVIGI = 'A') and IT_INDSEM = 'S' ) "
        + " and CD_ANOEPI = ? and CD_SEMEPI = ? order by CD_PROV ";

    final String sBUSQUEDA_ENFEREDO =
        "select CD_ENFERE from SIVE_ENFEREDO where CD_ENFCIE = ?";
    final String sLISTADO_ENFERMO =
        "select FC_NAC, CD_SEXO from SIVE_ENFERMO where CD_ENFERMO = ? ";
    final String sLISTADO_MODELOS = "select CD_MODELO from SIVE_MODELO where CD_TSIVE = 'E' and CD_NIVEL_1 is null and CD_NIVEL_2 is null and CD_CA is null and CD_MODELO in " +
        " (select CD_MODELO from SIVE_RESP_EDO where NM_EDO = ?)";
    final String sLISTADO_LINEAS = "select NM_LIN from SIVE_LINEA_ITEM where CD_MODELO = ? and CD_TSIVE='E' order by NM_LIN";
    final String sLISTADO_RESPUESTAS = "select DS_RESPUESTA from SIVE_RESP_EDO where NM_EDO = ? and CD_MODELO = ?  and NM_LIN = ? ";

    // Query con el estado de los Resumenes EDO
    /*final String sHISTORICO = "select pre.NM_PETICION, CD_SEMEPI_INI || '/' || CD_ANOEPI_INI as FC_EP_INI," +
         " CD_SEMEPI_FIN || '/' || CD_ANOEPI_FIN as FC_EP_FIN," +
         " TO_CHAR(FC_INICIO,'DD/MM/YYYY') as FC_INICIO, " +
                             " TO_CHAR(FC_FIN,'DD/MM/YYYY') as FC_FIN," +
                             " decode(IT_ESTADO, 'E', 'ERRORES'," +
                             "                   'T', 'FINALIZADA'," +
                             "                   'P','PENDIENTE'," +
                             "                   ' ') as Estado, DS_ERROR" +
         " from   SIVE_PARAM_RES_EDO pre, SIVE_PARAM_RESEDO_ERRORES pree" +
         " where  pree.NM_PETICION (+)= pre.NM_PETICION and " +
                             "        pre.CD_ANOEPI_INI = ? " +
                             " order by FC_INICIO desc";
     */

    // ARG: Sin usar TO_CHAR ni decode ni ||
    final String sHISTORICO = "select pre.NM_PETICION, CD_SEMEPI_INI " + MAS +
        " '/' " + MAS + " CD_ANOEPI_INI as FC_EP_INI," +
        " CD_SEMEPI_FIN " + MAS + " '/' " + MAS +
        " CD_ANOEPI_FIN as FC_EP_FIN," +
        " FC_INICIO, FC_FIN," +
        " IT_ESTADO as Estado, DS_ERROR" +
        " from   SIVE_PARAM_RES_EDO pre, SIVE_PARAM_RESEDO_ERRORES pree" +
        " where  pree.NM_PETICION (+)= pre.NM_PETICION and " +
        "        pre.CD_ANOEPI_INI = ? " +
        " order by FC_INICIO desc";

    /*     final String sHISTORICO = "select pre.NM_PETICION, CD_SEMEPI_INI || '/' || CD_ANOEPI_INI as FC_EP_INI," +
         " CD_SEMEPI_FIN || '/' || CD_ANOEPI_FIN as FC_EP_FIN," +
                                  " FC_INICIO, FC_FIN, " +
                                  " decode(IT_ESTADO, 'E', 'ERRORES'," +
                                  "                   'T', 'FINALIZADA'," +
                                  "                   'P','PENDIENTE'," +
         "                   ' ') as Estado, DS_ERROR" +
         " from   SIVE_PARAM_RES_EDO pre, SIVE_PARAM_RESEDO_ERRORES pree" +
         " where  pree.NM_PETICION (+)= pre.NM_PETICION and " +
                                  "        pre.CD_ANOEPI_INI = ? " +
                                  " order by FC_INICIO desc";
     */

    String sComAut = "";
    String sTSive = "";

    int iNumEdo;
    String sTipCas = "";
    int iCodEnfermo;
    String sCodEnfCie = "";
    java.sql.Date dFecNot = null;
    java.sql.Date dFecRec = null; //Fecha de recepci�n
    String sFecRec = "";

    String sCodEnfEdo = "";

    java.sql.Date dFecNac = null;
    String sCodSex = "";
    // Para c�lculo de edad del enfermo
    java.util.Date laFecha; //Fecha de la que extraeremos a�os , meses
    String fechaNac = "";
    String fechaNot = "";
    int anyoNac = 0;
    int anyoNot = 0;
    int mesNac = 0;
    int mesNot = 0;
    int anyos = 0;
    int meses = 0;
    Calendar cal = new GregorianCalendar();
    String laEdad = "";

    String sCodMod = "";
    int iNumLin = 0;

    String sDesRes = "";

    //Variables para envio ind. urgente
    int cuentaNoNulos = 0;
    int iMaxEnv = 0;

    //Variables para mostrar Historico
    String sCodPet = "";
    String sSemIni = "";
    String sSemFin = "";
    String sFechaIni = "";
    String sFechaFin = "";
    String sEstado = "";
    String sDesError = "";

    String sEstadoAux = "";
    String sOrigenDatos = "";

    String user = "";
    String passwd = "";
    boolean lortad;

    //___________________________________________________________________

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    //datFich = (DataFich) param.firstElement();
    // prepara la lista de resultados
    data = new CLista();

    // modos de operaci�n
    switch (opmode) {

      //======================== Modo gen fich edo ind===================================

      //Peticiones para EDOIND
      case servletGENFICH_EDOIND:

//# // System_out.println("Entra en servlet ressem*********************");
        datFich = (DataFich) param.firstElement();
        //Se crean listas que se van a enviar
        data.addElement(new DataResInd());
        ( (DataResInd) (data.firstElement())).resumen = new CLista();
        ( (DataResInd) (data.firstElement())).errores = new CLista();
        ( (DataResInd) (data.firstElement())).catalogos = new CLista();

        //prepara la lista auxiliar
        CLista dataAux = new CLista();

        //Recogida Par�mettros enviados para el envio semanal
        if (opmode == servletGENFICH_EDOIND) {
          sSem = datFich.getSem();
          sAno = datFich.getAno();
          bEsPrimera = datFich.getEsPrimera();
        }

        //Recogida Par�mettros enviados para el envio semanal y el urgente
        sSep = datFich.getSep();
        sComAut = datFich.getComAut();
        sTSive = datFich.getTSive();
        sOrigenDatos = datFich.getOrigen();

//# // System_out.println("Recogidos paramentros*********************");

        try {

//______Si es la primera semana se a�aden lineas con datos de tabla CATALOGOS_____________________

          //____  Varias Consultas en cat�logos, una para cada c�digo de tabla
          //Si es necesario Se a�aden las correspondientes tres lineas de fichero en cada caso

          if (bEsPrimera == true) {

            for (int contad = 0; contad < 5; contad++) {

              // prepara la query
              st = con.prepareStatement(sBUSQUEDA_CATALOGOS);

              //Codigo Tabla
              if (contad == 0) {
                sCodTab = "SIVE_SEXO";
              }
              else if (contad == 1) {
                sCodTab = "SIVE_CLASIF_EDO";
              }
              else if (contad == 2) {
                sCodTab = "SIVE_PROVINCIA";
              }
              else if (contad == 3) {
                sCodTab = "SIVE_COM_AUT";
              }
              else if (contad == 4) {
                sCodTab = "SIVE_MUNICIPIO";

              }
              st.setString(1, sCodTab);

              rs = st.executeQuery();

              // extrae la p�gina requerida (como m�ximo ser� una en cada consulta
              while (rs.next()) {

                // obtiene los campos
                sCodVer = rs.getString("CD_VERSION");
                sItOk = rs.getString("IT_OK");

                // a�ade linea indica tabla
                sLinea = "[" + sCodTab + "]";
                ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                    sLinea);

                // a�ade linea indica version
                sLinea = "version=";
                sLinea = sLinea + sCodVer;
                ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                    sLinea);

                // a�ade linea indica OK
                sLinea = "OK=";
                sLinea = sLinea + sItOk;
                ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                    sLinea);
              }
              rs.close();
              st.close();
              rs = null;
              st = null;

            } //Fin for
          } //Fin if esPrimera

          //Modos que no son el env�o de catalogos de EDOIND
          else {

            //______________________  Consulta casos edo ind para res semanal___________________________

            if (opmode == servletGENFICH_EDOIND) {

              //No se va a modificar la b. datos
              con.setAutoCommit(true);

              // prepara la query
              st = con.prepareStatement(sLISTADO_LOS_EDOS);

//# // System_out.println("Prep los edos*********************");
              st.setString(1, sAno);
              st.setString(2, sSem);

              // ARG: Se a�aden parametros
              registroConsultas.insertarParametro(sAno);
              // ARG: Se a�aden parametros
              registroConsultas.insertarParametro(sSem);

              rs = st.executeQuery();

              // ARG: Registro LORTAD
              registroConsultas.registrar("SIVE_EDOIND", sLISTADO_LOS_EDOS,
                                          "CD_ENFERMO",
                                          "", "SrvResSem", true);

              // extrae la p�gina requerida
              while (rs.next()) {

                // obtiene los campos

                //Tanto Strings obligatorios como no obligatorios se obtienen igual
                //En cliente se convertir�n los Strings nulos a cadenas""

                iNumEdo = rs.getInt("NM_EDO");

                sTipCas = rs.getString("CD_CLASIFDIAG");
                if (sTipCas == null) {
                  sTipCas = "";

                }
                sCodPro = rs.getString("CD_PROV");
                if (sCodPro == null) {
                  sCodPro = "";
                }
                sCodMun = rs.getString("CD_MUN");
                if (sCodMun == null) {
                  sCodMun = "";
                }
                iCodEnfermo = rs.getInt("CD_ENFERMO");
                dFecRec = rs.getDate("FC_RECEP");
                sFecRec = Fechas.date2String(dFecRec);

                sCodEnfCie = rs.getString("CD_ENFCIE");
                dFecNot = rs.getDate("FC_FECNOTIF");
                // a�ade un nodo
                dataAux.addElement(new DataNotInd(iNumEdo, sCodPro, sCodMun,
                                                  iCodEnfermo, sCodEnfCie,
                                                  dFecNot, sTipCas));
                ( (DataNotInd) (dataAux.lastElement())).setFecRec(sFecRec);
              }

              rs.close();
              st.close();
              rs = null;
              st = null;
//# // System_out.println("Fin losedos  *********************");
//# // System_out.println("dataAux  *********************" + dataAux.toString());
            }

            //______________________  recorrido lista___________________________

            enum = dataAux.elements();
            while (enum.hasMoreElements()) { //Recorremos lista
              DataNotInd datNotInd = (DataNotInd) (enum.nextElement());
              //# // System_out.println("extrae elem de lista  *********************");

              // prepara la query
              st = con.prepareStatement(sBUSQUEDA_ENFEREDO);
//# // System_out.println("prep envios un a�o *********************");
              // filtro
              st.setString(1, datNotInd.getCodEnfCie());

              rs = st.executeQuery();

              // extrae la p�gina requerida  Exactamente como en el select normalmente
              while (rs.next()) {

                //N�mero d3e envio
                sCodEnfEdo = rs.getString("CD_ENFERE");
              }
              datNotInd.setCodEnfEdo(sCodEnfEdo);

              rs.close();
              st.close();
              rs = null;
              st = null;

// // System_out.println("%%OBTENIDO ENFEDO *********************");

              //______________________  Consulta en SIVE_ENFERMO___________________________

              st = con.prepareStatement(sLISTADO_ENFERMO);
////# // System_out.println("Prep Enfermo*********************");
              st.setInt(1, datNotInd.getCodEnfermo());

              // ARG: Se a�aden parametros
              registroConsultas.insertarParametro(String.valueOf(datNotInd.
                  getCodEnfermo()));

              rs = st.executeQuery();

              // ARG: Registro LORTAD
              registroConsultas.registrar("SIVE_ENFERMO", sLISTADO_ENFERMO,
                                          "CD_ENFERMO",
                                          String.valueOf(datNotInd.
                  getCodEnfermo()), "SrvResSem", true);

              // extrae la p�gina requerida
              while (rs.next()) {
                // obtiene los campos
                dFecNac = rs.getDate("FC_NAC");
                sCodSex = rs.getString("CD_SEXO");
                if (sCodSex == null) {
                  sCodSex = "";
                }
              }

              //C�lculo de la edad y relleno de datos en elemento

              if (dFecNac != null) {
                //   SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
                //     laFecha = formateador.parse(fechaNac);

                //Se obtiene el a�o y mes de nacimiento
                cal.setTime(dFecNac);
                anyoNac = cal.get(Calendar.YEAR);
                mesNac = cal.get(Calendar.MONTH);
                //Se obtiene a�o y mes de notificaci�n
                cal.setTime(datNotInd.getFecNot());
                anyoNot = cal.get(Calendar.YEAR);
                mesNot = cal.get(Calendar.MONTH);
                //Se ve la diferencia entre fechas en a�os y meses
                if (mesNot > mesNac) {
                  anyos = anyoNot - anyoNac;
                  meses = mesNot - mesNac;
                }
                else if (mesNot == mesNac) {
                  anyos = anyoNot - anyoNac;
                  meses = 0;
                }
                else {
                  anyos = (anyoNot - anyoNac) - 1;
                  meses = (mesNot + 12) - mesNac;
                }

                //Se rellenan datos de edad y sexo del caso EDO
                // indicando la fecha en a�os o meses segun corresponda
                if (anyos <= 0) {
                  //Se da la edad en meses
                  laEdad = Integer.toString(meses);
                  datNotInd.setEdadySexo(laEdad, EDAD_EN_MESES, sCodSex);
                }
                else {
                  //Se da la edad en a�os
                  laEdad = Integer.toString(anyos);
                  datNotInd.setEdadySexo(laEdad, EDAD_EN_A�OS, sCodSex);
                }
              }
              //No se tiene dato de la edad
              else {
                laEdad = "";
                datNotInd.setEdadySexo(laEdad, NO_HAY_EDAD, sCodSex);
              }

              rs.close();
              st.close();
              st = null;
              rs = null;
////# // System_out.println("Fin enfermo  *********************");

              //______________________  Consulta en SIVE_MODELO___________________________

              int numModelos = 0; //N�mero de modelos que cumplen la query LISTADO_MODELO
              //Si no hay error debe ser solo uno
              st = con.prepareStatement(sLISTADO_MODELOS);
////# // System_out.println("Prep modelo*********************");
              st.setInt(1, datNotInd.getNumEdo());

              rs = st.executeQuery();

              // extrae la p�gina requerida
              while (rs.next()) {
                // obtiene los campos
                sCodMod = rs.getString("CD_MODELO");
                datNotInd.setCodMod(sCodMod);
                numModelos++;
              }
              rs.close();
              st.close();
              st = null;
              rs = null;
//# // System_out.println("Fin modelo  *********************");
              //Si hay un solo modelo todo OK
              if (numModelos == 1) {

                /*
                     sLinea =  Integer.toString(datNotInd.getNumEdo()) + sSep +  sComAut + sSep
                     + datNotInd.getCodEnfCie() + sSep  + datNotInd.getCodMun() +  sSep
                     + datNotInd.getCodPro() +sSep + sAno + sSep + sSem + sSep
                            + datNotInd.getEda() + sSep + datNotInd.getTipEda() + sSep + datNotInd.getSex() + sSep  + sTipCas + sSep
                             +sCodMod +sSep;
                 */

                // ARS 07-01-2001. Se generan las l�neas si los

                // datos son exclus�vamente de Magerit.

                if ( (sOrigenDatos.equals("M")) &&
                    (!datNotInd.getCodPro().equals("28"))) {

                  sLinea = "";

                }
                else {

                  sLinea = Integer.toString(datNotInd.getNumEdo()) + sSep +
                      sComAut + sSep
                      + datNotInd.getCodEnfEdo() + sSep + datNotInd.getCodMun() +
                      sSep
                      + datNotInd.getCodPro() + sSep + sAno + sSep + sSem +
                      sSep
                      + datNotInd.getEda() + sSep + datNotInd.getTipEda() +
                      sSep + datNotInd.getSex() + sSep + sTipCas + sSep
                      + sCodMod + sSep;
                }

                //______________________  Consulta en  SIVE_LINEA_ITEM___________________________

                st = con.prepareStatement(sLISTADO_LINEAS);
////# // System_out.println("Prep lineas*********************");
                st.setString(1, sCodMod);

                rs = st.executeQuery();

                // extrae la p�gina requerida
                while (rs.next()) {
                  // obtiene los campos
                  iNumLin = rs.getInt("NM_LIN");
                  datNotInd.vLineas.addElement(new DataLinea(iNumLin));

                }
                rs.close();
                st.close();
                st = null;
                rs = null;
////# // System_out.println(" Fin lineas *********************");

                //______________________  Consulta en  SIVE_RESP_EDO para cada linea ___________________________

                for (int j = 0; j < datNotInd.vLineas.size(); j++) {

                  st = con.prepareStatement(sLISTADO_RESPUESTAS);
////# // System_out.println("Prep respuestas*********************");
                  st.setInt(1, datNotInd.getNumEdo());
                  st.setString(2, datNotInd.getCodMod());
                  st.setInt(3,
                            ( (DataLinea) (datNotInd.vLineas.elementAt(j))).getNumLin());

                  rs = st.executeQuery();

                  // extrae la p�gina requerida
                  while (rs.next()) {
                    // obtiene los campos
                    sDesRes = rs.getString("DS_RESPUESTA");

                    //A�ade a la linea del fichero  la respta de la pregunta
                    sLinea = sLinea + sDesRes;
                  }
                  //Se a�ade separador independientemente de si se ha encontrado respta a la pregunta o no
                  sLinea = sLinea + sSep;

                  rs.close();
                  st.close();
                  st = null;
                  rs = null;
////# // System_out.println(" Fin respuestas *********************");

                } //For recorre lineas

                // ARS 01-07-2001 a�ade una linea
                if ( (sLinea != null) && (sLinea.length() > 0)) {
                  ( ( (DataResInd) (data.firstElement())).resumen).addElement(
                      sLinea);
                }

//# // System_out.println(" A�adida linea *********************");

              } //if hay un solo modelo

//______

              //Si no hay un solo modelo
              else {

                //if hay  m�s de un modelo ,se informa del error
                if (numModelos > 1) {

                  ( ( (DataResInd) (data.firstElement())).errores).addElement(
                      new DataError(datNotInd.getNumEdo(),
                                    datNotInd.getCodEnfEdo(),
                                    datNotInd.getFecRec(),
                                    Fechas.date2String(datNotInd.getFecNot()),
                      "M�s de un modelo con resptas. para ese caso"));
                }
                // No hay modelos para enfermedad (numModelos = 0).
                else {
                  /*
                       sLinea =  Integer.toString(datNotInd.getNumEdo()) + sSep +  sComAut + sSep
                       + datNotInd.getCodEnfCie() + sSep  + datNotInd.getCodMun() +  sSep
                       + datNotInd.getCodPro() +sSep + sAno + sSep + sSem + sSep
                              + datNotInd.getEda() + sSep + datNotInd.getTipEda() + sSep + datNotInd.getSex() + sSep  + sTipCas + sSep;
                   */
                  // ARS 01-07-2001. Se generan las l�neas si los

                  // datos son exclus�vamente de Magerit.

                  if ( (sOrigenDatos.equals("M")) &&
                      (!datNotInd.getCodPro().equals("28"))) {
                    sLinea = "";
                  }
                  else {
                    sLinea = Integer.toString(datNotInd.getNumEdo()) + sSep +
                        sComAut + sSep
                        + datNotInd.getCodEnfEdo() + sSep + datNotInd.getCodMun() +
                        sSep
                        + datNotInd.getCodPro() + sSep + sAno + sSep + sSem +
                        sSep
                        + datNotInd.getEda() + sSep + datNotInd.getTipEda() +
                        sSep + datNotInd.getSex() + sSep + sTipCas + sSep;
                  }
                  // ARS 01-07-2001 a�ade una linea
                  if ( (sLinea != null) && (sLinea.length() > 0)) {
                    ( ( (DataResInd) (data.firstElement())).resumen).addElement(
                        sLinea);
                  }
                }

              } //else hay error

            } //while recorrido de vector

          } //Else (No es la primera petici�n para fich indiv )

//____________________________________________________________________________

        }
        catch (Exception ex) {
          throw ex;
        }

        break;

        //____________________________________________________________________
        //____________________________________________________________________

        // listado
        // Resumen EDO: Ahora se generar� en el servidor de base de datos
        // El servlet solo inserta un registro en la tabla SIVE_PARAM_RES_EDO

      case servletACTUALIZAR_RES_NUM_UNA_SEM:
        InsParamResumen hilo = new InsParamResumen(param, con);
        try {
          hilo.run();
          data.addElement("OK");

        }
        catch (Exception ex) {
          data.addElement("ERROR");
          throw ex;
        }

        break;

// ARG
        /*        try {
             //______________________  Consulta enfermedad___________________________
                //prepara la lista auxiliar
                dataEnf = new CLista();
                  // prepara la query
                st = con.prepareStatement(sLISTADO_ENFEREDO);
                //// System_out.println("2) va a ajecutar...:" + st);
                rs = st.executeQuery();
                // extrae la p�gina requerida
                while (rs.next()) {
                  // obtiene los campos
                  //Tanto Strings obligatorios como no obligatorios se obtienen igual
                  //En cliente se convertir�n los Strings nulos a cadenas""
                   sCodEnf= rs.getString("CD_ENFCIE");
                   sCodTVi = rs.getString("CD_TVIGI");
                 // a�ade un nodo
                  dataEnf.addElement(new DataEnf(sCodEnf, sCodTVi));
                  //// System_out.println("3)(sCodEnf,sCodTVi):" + sCodEnf + ", " + sCodTVi);
                }
                rs.close();
                st.close();
                rs = null;
                st = null;
             //______________________  Consulta geog.___________________________
                //prepara la lista auxiliar
                dataGeo = new CLista();
                  // prepara la query
                st = con.prepareStatement(sLISTADO_GEOGRAFICO);
                //// System_out.println("4) va a ajecutar...:" + st);
                rs = st.executeQuery();
                // extrae la p�gina requerida
                while (rs.next()) {
                      // obtiene los campos
                      sCodNiv1= rs.getString("CD_NIVEL_1");
                      sCodNiv2= rs.getString("CD_NIVEL_2");
                      sCodZbs= rs.getString("CD_ZBS");
                      sCodPro = rs.getString("CD_PROV");
                      sCodMun = rs.getString("CD_MUN");
                 // a�ade un nodo
             dataGeo.addElement( new DataGeo(sCodNiv1,sCodNiv2,sCodZbs,sCodPro,sCodMun));
                 //// System_out.println("5)(sCodNiv1,sCodNiv2,sCodZbs,sCodPro,sCodMun):" +
                 //sCodNiv1 + ", " + sCodNiv2 + ", " + sCodZbs + ", " + sCodPro + ", " + sCodMun);
                }
                rs.close();
                st.close();
                rs = null;
                st = null;
             //______________________  Formaci�n vector ___________________________
             dataProd = new CLista();
                for (int ci=0; ci< dataEnf.size(); ci++) {
                  for (int ck=0; ck< dataGeo.size(); ck++) {
                      dataProd.addElement ( new DataProd(
                      ((DataEnf)(dataEnf.elementAt(ci))).getCodEnf(),
                      ((DataEnf)(dataEnf.elementAt(ci))).getCodTVi(),
                      ((DataGeo)(dataGeo.elementAt(ck))).getCodNiv1(),
                      ((DataGeo)(dataGeo.elementAt(ck))).getCodNiv2(),
                      ((DataGeo)(dataGeo.elementAt(ck))).getCodZbs(),
                      ((DataGeo)(dataGeo.elementAt(ck))).getCodPro(),
                      ((DataGeo)(dataGeo.elementAt(ck))).getCodMun(),
                      sAno,
                      sSem ) ) ;  //La semana que estamos tratando
         */
        /* // System_out.println("6)(getCodEnf,getCodTVi,getCodNiv1,getCodNiv2,getCodZbs,getCodPro,getCodMun,sAno,sSem):" +
                       ((DataEnf)(dataEnf.elementAt(ci))).getCodEnf() + ", " +
                       ((DataEnf)(dataEnf.elementAt(ci))).getCodTVi() + ", " +
                       ((DataGeo)(dataGeo.elementAt(ck))).getCodNiv1() + ", " +
                       ((DataGeo)(dataGeo.elementAt(ck))).getCodNiv2() + ", " +
                       ((DataGeo)(dataGeo.elementAt(ck))).getCodZbs() + ", " +
                       ((DataGeo)(dataGeo.elementAt(ck))).getCodPro() + ", " +
                       ((DataGeo)(dataGeo.elementAt(ck))).getCodMun() + ", " +
                       sAno + ", " +
                       sSem); */
// ARG
        /*          }
                }
             //______________________  Consulta num casos___________________________
               enum = dataProd.elements();
               while (enum.hasMoreElements() ){    //Recorremos lista
                  DataProd datProd = (DataProd)(enum.nextElement());
                  //DSR: Adici�n de Tipos de Vigilancias (A y X)
                  if (datProd.getCodTVi().equals("N") ||
                      datProd.getCodTVi().equals("A") ||
                      datProd.getCodTVi().equals("X")){
                    //DSR:montamos la query en lugar de rellenar par�metros
             sLISTADO_EDO_NUM = " select SUM(NM_CASOS) from SIVE_EDONUM " +
                      " where  CD_ENFCIE = " + datProd.getCodEnf().trim() +
                      " and  CD_ANOEPI = " + datProd.getCodAno().trim() +
                      " and CD_SEMEPI = " + datProd.getCodSem().trim() +
             " and CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF " +
                      " where CD_NIVEL_1 = " + datProd.getCodNiv1().trim() +
                      " and CD_NIVEL_2 = " + datProd.getCodNiv2().trim() +
                      " and CD_ZBS = " + datProd.getCodZbs().trim() +
             " and CD_CENTRO in (select CD_CENTRO from SIVE_C_NOTIF " +
                      " where CD_PROV = " + datProd.getCodPro().trim() +
                      " and CD_MUN = " + datProd.getCodMun().trim() + "))";
                    st = con.prepareStatement(sLISTADO_EDO_NUM);
         */
        /*
                     st.setString(1, datProd.getCodEnf().trim());
                     st.setString(2, datProd.getCodAno().trim());
                     st.setString(3, datProd.getCodSem().trim());
                     st.setString(4, datProd.getCodNiv1().trim());
                     st.setString(5, datProd.getCodNiv2().trim());
                     st.setString(6, datProd.getCodZbs().trim());
                     st.setString(7, datProd.getCodPro().trim());
                     st.setString(8, datProd.getCodMun().trim());
         */

        /*// System_out.println("7) va a ajecutar...: " +
                    "1)" + datProd.getCodEnf().trim() + ", " +
                    "2)" + datProd.getCodAno().trim() + ", " +
                    "3)" + datProd.getCodSem().trim() + ", " +
                    "4)" + datProd.getCodNiv1().trim() + ", " +
                    "5)" + datProd.getCodNiv2().trim() + ", " +
                    "6)" + datProd.getCodZbs().trim() + ", " +
                    "7)" + datProd.getCodPro().trim() + ", " +
                    "8)" + datProd.getCodMun().trim());
        */
// ARG
       /*            rs = st.executeQuery();
                   // extrae la p�gina requerida
                   while (rs.next()) {
                     // obtiene los campos
                     iNumCas= rs.getInt("SUM(NM_CASOS)");
                     datProd.setNumCas(iNumCas);
                     //// System_out.println("TOTAL_DENTRO_7:" + iNumCas);
                   }
                   rs.close();
                   st.close();
                   st = null;
                   rs = null;
                } //if numerica
               //Caso de edo ind
               else {
        */
       /* final String sLISTADO_EDO_IND = "select COUNT(a.NM_EDO) from SIVE_EDOIND a, SIVE_NOTIF_EDOI b where a.CD_ENFCIE = ? and a.CD_ANOEPI = ?  and a.CD_SEMEPI = ? " +
                 "and b.CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and CD_ZBS = ? " +
                 "and CD_CENTRO in (select CD_CENTRO from SIVE_C_NOTIF where CD_PROV = ? and CD_MUN = ?)) and a.NM_EDO = b.NM_EDO and b.CD_FUENTE = 'E' ";
        */
// ARG
       /*          //DSR:montamos la query en lugar de rellenar par�metros
                 sLISTADO_EDO_IND = " select COUNT(a.NM_EDO) " +
                   " from SIVE_EDOIND a, SIVE_NOTIF_EDOI b " +
                   " where a.CD_ENFCIE = " + datProd.getCodEnf().trim() +
                   " and a.CD_ANOEPI = " + datProd.getCodAno().trim() +
                   " and a.CD_SEMEPI = " + datProd.getCodSem().trim() +
            " and b.CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF " +
                   " where CD_NIVEL_1 = " + datProd.getCodNiv1().trim() +
                   " and CD_NIVEL_2 = " + datProd.getCodNiv2().trim() +
                   " and CD_ZBS = " + datProd.getCodZbs().trim() +
                   " and CD_CENTRO in (select CD_CENTRO " +
            " from SIVE_C_NOTIF where CD_PROV = " + datProd.getCodPro().trim() +
                   " and CD_MUN = " + datProd.getCodMun().trim() + " )) " +
                   " and a.NM_EDO = b.NM_EDO and b.CD_FUENTE = 'E' ";
                 st = con.prepareStatement(sLISTADO_EDO_IND);
        */
       /* st.setString(1, datProd.getCodEnf().trim() );
                  st.setString(2, datProd.getCodAno().trim() );
                  st.setString(3, datProd.getCodSem().trim() );
                  st.setString(4, datProd.getCodNiv1().trim() );
                  st.setString(5, datProd.getCodNiv2().trim() );
                  st.setString(6, datProd.getCodZbs().trim() );
                  st.setString(7, datProd.getCodPro().trim() );
                  st.setString(8, datProd.getCodMun().trim() );
        */

       /*// System_out.println("8) va a ajecutar...: " +
                 "1)" + datProd.getCodEnf().trim() + ", " +
                 "2)" + datProd.getCodAno().trim() + ", " +
                 "3)" + datProd.getCodSem().trim() + ", " +
                 "4)" + datProd.getCodNiv1().trim() + ", " +
                 "5)" + datProd.getCodNiv2().trim() + ", " +
                 "6)" + datProd.getCodZbs().trim() + ", " +
                 "7)" + datProd.getCodPro().trim() + ", " +
                 "8)" + datProd.getCodMun().trim());
       */
// ARG
      /*          rs = st.executeQuery();
                // extrae la p�gina requerida
                while (rs.next()) {
                    // obtiene los campos
                    iNumCas= rs.getInt("COUNT(a.NM_EDO)");
                    datProd.setNumCas(iNumCas);
                    //// System_out.println("TOTAL_DENTRO_8:" + iNumCas);
                }
                rs.close();
                st.close();
                st = null;
                rs = null;
              }
            }   //while recorrido de vector
           //______________________ Baja de registros referentes a una semana en SIVE_RESUMEN_EDOS___________________________
              // prepara la query
              st = con.prepareStatement(sBORRADO_UNA_SEMANA_RESUMEN_EDOS);
              // c�digo a�o desde
              st.setString(1,sAno);
              // c�digo semana
              st.setString(2, sSem  );  //Semana estamos recorriendo
              // lanza la query
              //// System_out.println("9) va a ajecutar...:" + st);
              st.executeUpdate();
              st.close();
              st = null;
           //______________________ Alta___________________________
              enum = dataProd.elements();
              while (enum.hasMoreElements() ){    //Recorremos lista
                DataProd datProd = (DataProd)(enum.nextElement());
                // prepara la query
                st = con.prepareStatement(sALTA_RESUMEN_EDOS);
                st.setString(1, datProd.getCodEnf().trim() );
                st.setString(2, datProd.getCodAno().trim() );
                st.setString(3, datProd.getCodSem().trim() );
                st.setString(4, datProd.getCodNiv1().trim() );
                st.setString(5, datProd.getCodNiv2().trim() );
                st.setString(6, datProd.getCodZbs().trim() );
                st.setString(7, datProd.getCodPro().trim() );
                st.setString(8, datProd.getCodMun().trim() );
                st.setInt(9, datProd.getNumCas());
       */
      // lanza la query
      /* // System_out.println("10) va a ajecutar...: " +
                 "1)" + datProd.getCodEnf().trim() + ", " +
                 "2)" + datProd.getCodAno().trim() + ", " +
                 "3)" + datProd.getCodSem().trim() + ", " +
                 "4)" + datProd.getCodNiv1().trim() + ", " +
                 "5)" + datProd.getCodNiv2().trim() + ", " +
                 "6)" + datProd.getCodZbs().trim() + ", " +
                 "7)" + datProd.getCodPro().trim() + ", " +
                 "8)" + datProd.getCodMun().trim() + ", " +
                 "9)" + datProd.getNumCas());
       */

// ARG
      /*
                st.executeUpdate();
                st.close();
                st = null;
              }
            // valida la transacci�n de una semana
            con.commit(); //??????????????????????????????????????????????
             //fall� la transacci�n
            } catch (Exception ex) {
              con.rollback(); //?????????????????????????????????????????
              throw ex;
            }
       */

      case servlet_MOSTRAR_HISTORICO:

        con.setAutoCommit(false);

        try {
          String annoInicio = (String) param.firstElement();
          // Prepara la Query
          st = con.prepareStatement(sHISTORICO);
          st.setString(1, annoInicio);

          // Ejecuta la consulta
          rs = st.executeQuery();

          // ARG: Para leer fecha de la query
          java.sql.Timestamp fecRecu = null;

          while (rs.next()) {
            sCodPet = rs.getString("NM_PETICION");
            sSemIni = rs.getString("FC_EP_INI");
            sSemFin = rs.getString("FC_EP_FIN");

            // ARG: Obtenemos las fechas de inicio y fin
            //sFechaIni = rs.getString("FC_INICIO");
            //sFechaFin = rs.getString("FC_FIN");
            fecRecu = rs.getTimestamp("FC_INICIO");
            sFechaIni = timestamp_a_cadena(fecRecu);
            fecRecu = rs.getTimestamp("FC_FIN");
            sFechaFin = timestamp_a_cadena(fecRecu);

            // ARG: Dependiendo del valor de ESTADO se rellena sEstado
            sEstadoAux = rs.getString("ESTADO");
            if (sEstadoAux.equals("E")) {
              sEstado = "ERRORES";
            }
            else if (sEstadoAux.equals("T")) {
              sEstado = "FINALIZADA";
            }
            else if (sEstadoAux.equals("P")) {
              sEstado = "PENDIENTE";
            }
            else {
              sEstado = " ";

            }

            if (rs.getString("DS_ERROR") == null) {
              sDesError = "";
            }
            else {
              sDesError = rs.getString("DS_ERROR").replace('\n', ' ');
            }

            data.addElement(new DataHistorico(sCodPet, sSemIni, sSemFin,
                                              sFechaIni, sFechaFin, sEstado,
                                              sDesError));
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          con.commit();

        }
        catch (Exception ex) {
          con.rollback();
          throw ex;
        }

        break;

      case servletGENFICH_RES_NUM:

        datFich = (DataFich) param.firstElement();
        con.setAutoCommit(true);

        //Recoge par�metros de cliente
        sSemDesde = datFich.getSemDesde();
        sSemHasta = datFich.getSemHasta();
        sAno = datFich.getAno();
        sSep = datFich.getSep();
        //sOrigen = datFich.getOrigen();

        //Se crean listas que se van a enviar
        data.addElement(new DataResInd());
        ( (DataResInd) (data.firstElement())).resumen = new CLista();
        ( (DataResInd) (data.firstElement())).errores = new CLista();
        ( (DataResInd) (data.firstElement())).catalogos = new CLista();

        //____ Dos Consulta en cat�logos, para tablas SIVE_PROVINCIA y SIVE_EDO_CNE ___________________________
        //Si es necesario Se a�aden las correspondientes tres lineas de fichero en cada caso

        for (int contad = 0; contad < 2; contad++) {

          // prepara la query
          st = con.prepareStatement(sBUSQUEDA_CATALOGOS);

          //Codigo Tabla
          if (contad == 0) {
            sCodTab = "SIVE_PROVINCIA";
          }
          else {
            sCodTab = "SIVE_EDO_CNE";

          }
          st.setString(1, sCodTab);

          rs = st.executeQuery();

          // extrae la p�gina requerida (como m�ximo ser� una en cada consulta
          while (rs.next()) {

            // obtiene los campos
            sCodVer = rs.getString("CD_VERSION");
            sItOk = rs.getString("IT_OK");

            // a�ade linea indica tabla
            sLinea = "[" + sCodTab + "]";
            ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                sLinea);

            // a�ade linea indica version
            sLinea = "version=";
            anadir(sCodVer, 5);
            ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                sLinea);

            // a�ade linea indica OK
            sLinea = "OK=";
            anadir(sItOk, 1);
            ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                sLinea);
          }
          rs.close();
          st.close();
          rs = null;
          st = null;

        } //Fin for

// System_out.println("Los catalogos ********" +  ( (DataResInd) (data.firstElement()) ).catalogos ) ;

        /*
             //______________________ Primera consulta___________________________
                  // prepara la query
                st = con.prepareStatement(sLISTADO_RESUMEN_EDOS);
                // c�digo a�o desde
                st.setString(1,sAno);
                // c�digo semana desde
                st.setString(2, sSemDesde);
                // c�digo sem hasta
                st.setString(3,sSemHasta);
                rs = st.executeQuery();
                // extrae la p�gina requerida
                while (rs.next()) {
                      // obtiene los campos
                      sCodAno= rs.getString("CD_ANOEPI");
                      sCodSem= rs.getString("CD_SEMEPI");
                      sCodPro = rs.getString("CD_PROV");
                      sCodEnf= rs.getString("CD_ENFCIE");
                      iNumCas = rs.getInt("SUM(NM_CASOS)");
                      sNumCas = Integer.toString(iNumCas);
                      sLinea = "";
                      anadir(sCodAno,4);
                      anadir(sCodSem,2);
                      anadir(sCodPro,2);
                      anadir(sCodEnf,2);
                      anadir(sNumCas,6);
                  // a�ade un nodo
             ( ( (DataResInd) (data.firstElement()) ).resumen ) .addElement(sLinea);
                }
                rs.close();
                st.close();
                rs = null;
                st = null;
         */

        //prepara la lista auxiliar
        dataEnf = new CLista();

        st = con.prepareStatement(sLISTADO_RESUMEN_EDOS);

        // c�digo a�o desde
        st.setString(1, sAno);
        // c�digo semana desde
        st.setString(2, sSemDesde);
        // c�digo sem hasta
        st.setString(3, sSemHasta);

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          // obtiene los campos
          sCodAno = rs.getString("CD_ANOEPI");
          sCodSem = rs.getString("CD_SEMEPI");
          sCodPro = rs.getString("CD_PROV");
          sCodEnf = rs.getString("CD_ENFCIE");
          iNumCas = rs.getInt("SUM(NM_CASOS)");
          sNumCas = Integer.toString(iNumCas);

          dataEnf.addElement(new DataResNum(sCodAno, sCodSem, sCodPro,
                                            sCodEnf, sNumCas));

        }
        rs.close();
        st.close();
        rs = null;
        st = null;

        //___________________________________________

        enum = dataEnf.elements();
        while (enum.hasMoreElements()) { //Recorremos lista
          DataResNum datResNum = (DataResNum) (enum.nextElement());
          //# // System_out.println("extrae elem de lista  *********************");

          //Busca codigo EDO

          // prepara la query
          st = con.prepareStatement(sBUSQUEDA_ENFEREDO);
          // filtro
          st.setString(1, datResNum.sCodEnf);
          rs = st.executeQuery();
          // extrae la p�gina requerida  Exactamente como en el select normalmente
          while (rs.next()) {
            //N�mero d3e envio
            sCodEnfEdo = rs.getString("CD_ENFERE");
          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          //A�ade linea
          sLinea = "";
          anadir(datResNum.sCodAno, 4);
          anadir(datResNum.sCodSem, 2);
          anadir(datResNum.sCodPro, 2);
          anadir(sCodEnfEdo, 3);
          anadir(datResNum.sNumCas, 6);

          // a�ade un nodo
          ( ( (DataResInd) (data.firstElement())).resumen).addElement(sLinea);

        } //While

    } //fin switch

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  } //doWork

  //____________________________________________________________________________

  //A�ade al String sLinea (usado para cada linea del fichero) el String
  //indicado en parametro cual
  //Si cual no llega a ocupar longMax caracteres, estos caracteres se ponen como blancos
  void anadir(String cual, int longMax) {
    sLinea = sLinea + cual;
    for (int cont = 0; cont < (longMax - (cual.length())); cont++) {
      sLinea = sLinea + " ";
    }
  }

} // SrvResSem

/**
 * Ejecuci�n de insert en SIVE_PARAM_RES_EDO sin que tenga que esperar el
 * usuario.
 *
 * @author  JRM
 * @version 16/01/01
 */
class InsResumen
    extends Thread {
  /** Insercion en tabla */
  public void run() {

  }
} // InsResumen

/**
 * Ejecuci�n de insert en SIVE_PARAM_RES_EDO sin que tenga que esperar el
 * usuario.
 *
 * @author  JRM
 * @version 16/01/01
 */
class InsParamResumen
    extends Thread {
  /** Par�metros del applet */
  CLista param;
  /** Conexi�n a la base de datos */
  Connection con;

  /** Recogemos los datos del applet */
  public InsParamResumen(CLista param, Connection con) {
    this.param = param;
    this.con = con;
  }

  /** Insercion en tabla */
  public void run() {
    DataFich datFich;
    String sSem,
        sAno;
    PreparedStatement st = null;

    datFich = (DataFich) param.firstElement();
    sSem = datFich.getSem();
    sAno = datFich.getAno();

    try {
      con.setAutoCommit(true);
      // Prepara la Query
      st = con.prepareStatement("insert into SIVE_PARAM_RES_EDO" +
          " (CD_ANOEPI_INI, CD_SEMEPI_INI, CD_ANOEPI_FIN, CD_SEMEPI_FIN)" +
          " values ('" + sAno + "', '01', '" + sAno + "', '" + sSem + "')");
      // Ejecuta la inserci�n
      st.executeUpdate();
      st.close();
      st = null;
    }
    catch (Exception ex) {
      ;
      //throw ex;
    }
  }
} // InsParamResumen
