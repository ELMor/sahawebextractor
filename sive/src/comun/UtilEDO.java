//viene del paquete de notutil de EDO
package comun;

import java.util.Calendar;

public class UtilEDO {

  public UtilEDO() {
  }

  //Formatear Numeros
  public static String Formatear_Numeros(int iLong, String dato) {
    int leng = dato.length();
    String ceros = "";
    for (int i = leng; i < iLong; i++) {
      ceros = "0" + ceros;
    }
    return (ceros + dato);
  }

  // m�todo que retorna la fecha actual con formato dd/mm/yyyy
  public static String getFechaAct() {

    Calendar cal = Calendar.getInstance();

    String sDayAct = (new Integer(cal.get(cal.DAY_OF_MONTH)).toString());
    if (sDayAct.length() == 1) {
      sDayAct = "0" + sDayAct;

    }
    String sMonthAct = (new Integer(cal.get(cal.MONTH) + 1).toString());
    if (sMonthAct.length() == 1) {
      sMonthAct = "0" + sMonthAct;

    }
    String sYearAct = (new Integer(cal.get(cal.YEAR))).toString();

    // retorna la fecha actual con formato dd/mm/yyyy
    return (sDayAct + "/" + sMonthAct + "/" + sYearAct);

  }

  // m�todo que retorna el a�o en curso
  public static String getAnnoAct() {
    Calendar cal = Calendar.getInstance();
    return ( (new Integer(cal.get(cal.YEAR))).toString());
  }

  // comparaci�n de fechas (con formato dd/mm/yyyy)
  //1 mayor que 2: true
  //2 mayor que 1 o iguales: false
  public static boolean fecha1MayorqueFecha2(String fecha1, String fecha2) {

    // se supone que las fecha presentan el formato dd/mm/yyyy
    String sFecha1 = fecha1.substring(6, 10) + fecha1.substring(3, 5) +
        fecha1.substring(0, 2);
    String sFecha2 = fecha2.substring(6, 10) + fecha2.substring(3, 5) +
        fecha2.substring(0, 2);

    int iFecha1 = new Integer(sFecha1).intValue();
    int iFecha2 = new Integer(sFecha2).intValue();

    if (iFecha1 > iFecha2) {
      return true;
    }
    return false;
  }

  public static int Compara_Fechas(String Fecha1, String Fecha2) {
//Formato fechas dd/mm/aa e informadas!!!
//0: si iguales
//1: si Fecha1 mayor
//2: si Fecha2 mayor
    int iresult = 0;
    String Fec1 = "";
    String Fec2 = "";
    int x1 = 0, x2 = 0;
    String dd = "", mm = "", aa = "";

    String sCadena = "";
    sCadena = Fecha1;
    x1 = sCadena.indexOf("/", 0);
    x2 = sCadena.indexOf("/", x1 + 1);
    dd = sCadena.substring(0, x1);
    mm = sCadena.substring(x1 + 1, x2);
    aa = sCadena.substring(x2 + 1, sCadena.length());
    Fec1 = aa + mm + dd;
    sCadena = Fecha2;
    x1 = sCadena.indexOf("/", 0);
    x2 = sCadena.indexOf("/", x1 + 1);
    dd = sCadena.substring(0, x1);
    mm = sCadena.substring(x1 + 1, x2);
    aa = sCadena.substring(x2 + 1, sCadena.length());
    Fec2 = aa + mm + dd;

    int iCompara = 0;
    iCompara = Fec1.compareTo(Fec2);
    if (iCompara == 0) {
      iresult = 0;
    }
    else if (iCompara < 0) {
      iresult = 2;
    }
    else if (iCompara > 0) {
      iresult = 1;

    }
    return (iresult);
  } //fin Compara_Fechas

  //Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   public static boolean ChequearEntero(String sDat, int uMin, int uMax,
                                        int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString == "") {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString == "") {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     //comprobar longitud
     if (b) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

     return (b);
   } //fin de ChequearEntero

  // 18/11/99 (JMT)
  /**
   *  Comprueba si una cadena representa un numero o no
   *  @param sDat: String a comprobar
   */
  public static boolean isNum(String sDat) {

    int i;

    // Se prueba a convertir en entero para ver si salta la excepcion
    try {
      Integer f = new Integer(sDat);
      i = f.intValue();
    }
    catch (Exception e) {
      return false; ;
    }

    return true;
  } // Fin isNum()

}