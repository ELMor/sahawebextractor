/**
 * Clase: SrvDebugGeneral
 * Paquete: comun
 * Hereda: DBServlet
 * Autor: Emilio Postigo Riancho
 * Fecha Inicio: 26/11/1999
 * Descripcion: Clase abstracta para obtener, en las clases derivadas, la capacidad
               de ejecutar el servlet en local.
 */
package comun;

import java.sql.Connection;

import capp.CLista;
import jdbcpool.JDCConnectionDriver;
import sapp.DBServlet;

public abstract class SrvDebugGeneral
    extends DBServlet {
  // Para emplear en el modo debug
  protected Connection con = null;

  // Obliga a ....
  protected abstract CLista doWork(int i, CLista p) throws Exception;

  protected abstract CLista doDebug() throws Exception;

  // Modo debug
  public CLista doPrueba(int opmode, CLista param) throws Exception {
    jdcdriver = new JDCConnectionDriver("oracle.jdbc.driver.OracleDriver",
        "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
        "sive_desa",
        "sive_desa");
    con = openConnection();
    return doWork(opmode, param);
  }

}