package comun;

import java.io.Serializable;

public class DataEnferedo
    implements Serializable {
  protected String sCod = "";
  protected String sDes = "";

  public DataEnferedo() {
  }

  public DataEnferedo(String cod) {
    sCod = cod;
    sDes = null;
  }

  public DataEnferedo(String cod, String des) {

    sCod = cod;
    sDes = des;
  } //fin construct

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }
}
