//parecido a datacasoeo de notdata
package comun;

import java.io.Serializable;

public class DataRegistroEDO
    implements Serializable {
  public String sCodEnfermo = "";
  public String sApe1 = "";
  public String sApe2 = "";
  public String sNombre = "";
  public String sSiglas = "";
  public int iCodCaso = 0;
  public String sDesEnfermedad = "";
  public String sCodEnfermedad = "";
  public boolean bHayDatos = false;
  public boolean bEsOK = false;
  public boolean bConfidencial = false;
  public String sFechaNac = "";
  public String sFechaNotif = "";
  public String sCodClasif = "";
  public String sDesClasif = "";
  public String sFechaIni = "";
  //QQ
  public String sCodCentro = "";
  public String sDesCentro = "";
  public String sCodEquipo = "";
  public String sDesEquipo = "";
  public String sCodN1 = "";
  public String sCodN2 = "";
  //mlm
  public String sCD_ARTBC = "";
  public String sCD_NRTBC = "";
  public String sFC_SALRTBC = "";

  public DataRegistroEDO() {}

  public DataRegistroEDO(String CodEnfermo,
                         String Ape1,
                         String Ape2,
                         String Nombre,
                         String Siglas,
                         int CodCaso,
                         String DesEnfermedad,
                         String CodEnfermedad,
                         boolean HayDatos,
                         boolean EsOK, boolean Confidencial,
                         String FechaNac,
                         String FechaNotif,
                         String CodClasif,
                         String DesClasif,
                         String FechaIni,
                         //QQ
                         String CodCentro,
                         String DesCentro,
                         String CodEquipo,
                         String DesEquipo,
                         String CodN1,
                         String CodN2,
                         String sANORT, String sNRT, String sFSALRT
                         ) {
    sCodEnfermo = CodEnfermo;
    sApe1 = Ape1;
    sApe2 = Ape2;
    sNombre = Nombre;
    sSiglas = Siglas;
    iCodCaso = CodCaso;
    sDesEnfermedad = DesEnfermedad;
    sCodEnfermedad = CodEnfermedad;
    bHayDatos = HayDatos;
    bEsOK = EsOK;
    bConfidencial = Confidencial;
    sFechaNac = FechaNac;
    sFechaNotif = FechaNotif;
    sCodClasif = CodClasif;
    sDesClasif = DesClasif;
    sFechaIni = FechaIni;
    //QQ
    sCodCentro = CodCentro;
    sDesCentro = DesCentro;
    sCodEquipo = CodEquipo;
    sDesEquipo = DesEquipo;
    sCodN1 = CodN1;
    sCodN2 = CodN2;
    sANORT = sCD_ARTBC;
    sNRT = sCD_NRTBC;
    sFSALRT = sFC_SALRTBC;

  }
}
