//viene del paquete de notutil de EDO
package comun;

import capp.CApp;
import capp.CListaValores;
import sapp.StubSrvBD;

// lista de valores
public class CListaNiveles1
    extends CListaValores {

  public CListaNiveles1(CApp a,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descripcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();

  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}
