//viene de notdata
package comun;

public class DataValoresEDO {

  public String sApe1 = "";
  public String sApe2 = "";
  public String sNombre = "";
  public boolean bFlag_Siglas = false;
  public String sSiglas = "";

  public String sCodCA = "";
  public String sDesCA = "";
  public String sCodProv = "";
  public String sDesProv = "";
  public String sCodMun = "";
  public String sDesMun = "";
  public String sCodCalle = "";
  public String sDesCalle = "";
  public String sCPostal = "";
  public String sPiso = "";
  public String sNumero = "";

  public String sTDoc = "";
  public String sDesTDoc = "";
  public String sNDoc = "";

  public String sFechaNac = "";
  public String sItCalc = "";

  public String sTelefono = "";
  public String sCodSexo = "";
  public String sDesSexo = "";

  public String sCodEnfermo = "";
  public String sCodNivel1 = "";
  public String sDesNivel1 = "";
  public String sCodNivel2 = "";
  public String sDesNivel2 = "";
  public String sCodZBS = "";
  public String sDesZBS = "";

  public String CDTVIA = "";
  public String CDVIAL = "";
  public String CDTNUM = "";
  public String DSCALNUM = "";
  public String CD_OPE = "";
  public String FC_ULTACT = "";
  public String IT_ENFERMO = "";
  public String IT_CONTACTO = "";

  /*//OLD
    public DataValoresEDO(  String Ape1,
                         String Ape2,
                         String Nombre,
                         String CodCA,
                         String CodProv,
                         String CodMun,
                         String CodCalle,
                         String DesCalle,
                         String FechaNac,
                         String Piso,
                         String Numero,
                         String TDoc,
                         String NDoc)
    {
   sApe1 = Ape1;
   sApe2 = Ape2;
   sNombre = Nombre;
   sCodCA = CodCA;
   sCodProv = CodProv;
   sCodMun = CodMun;
   sCodCalle = CodCalle;
   sDesCalle = DesCalle;
   sFechaNac = FechaNac;
   sPiso = Piso;
   sNumero = Numero;
   sTDoc = TDoc;
   sNDoc = NDoc;
    } */

 //constructor para boton enfermo desde EDOIndiv  (CONSULTA)
 public DataValoresEDO(String APE1, String APE2, String NOMBRE,
                       String CODMUN,
                       String CODPROV,
                       String DESCALLE,
                       String FECHANAC,
                       String CODSEXO,
                       String TDOC,
                       String NDOC,
                       boolean bFLAG_CONFIDENCIAL) {
   sApe1 = APE1;
   sApe2 = APE2;
   sNombre = NOMBRE;
   sCodMun = CODMUN;
   sCodProv = CODPROV;
   sDesCalle = DESCALLE;
   sFechaNac = FECHANAC;
   sCodSexo = CODSEXO;
   sTDoc = TDOC;
   sNDoc = NDOC;
   bFlag_Siglas = bFLAG_CONFIDENCIAL;
 }

  //constructor para la SALIDA de la consulta
  public DataValoresEDO(

      String APE1, String APE2, String NOMBRE,
      boolean bFLAG_CONFIDENCIAL, String SIGLAS,
      String CODCA,
      String DESCA,
      String CODPROV,
      String DESPROV,
      String CODMUN,
      String DESMUN,
      String CODCALLE,
      String DESCALLE,
      String CPOSTAL,
      String PISO,
      String NUMERO,
      String TDOC,
      String DESTDOC,
      String NDOC,
      String FECHANAC,
      String ITCALC,
      String TELEFONO,
      String CODSEXO,
      String DESSEXO,
      String CODENFERMO,
      String CODNIVEL1,
      String DESNIVEL1,
      String CODNIVEL2,
      String DESNIVEL2,
      String CODZBS,
      String DESZBS) {

    sApe1 = APE1;
    sApe2 = APE2;
    sNombre = NOMBRE;
    bFlag_Siglas = bFLAG_CONFIDENCIAL;
    sSiglas = SIGLAS;

    sCodCA = CODCA;
    sDesCA = DESCA;
    sCodProv = CODPROV;
    sDesProv = DESPROV;
    sCodMun = CODMUN;
    sDesMun = DESMUN;
    sCodCalle = CODCALLE;
    sDesCalle = DESCALLE;
    sCPostal = CPOSTAL;
    sPiso = PISO;
    sNumero = NUMERO;

    sTDoc = TDOC;
    sDesTDoc = DESTDOC;
    sNDoc = NDOC;

    sFechaNac = FECHANAC;
    sItCalc = ITCALC;

    sTelefono = TELEFONO;
    sCodSexo = CODSEXO;
    sDesSexo = DESSEXO;

    sCodEnfermo = CODENFERMO;
    sCodNivel1 = CODNIVEL1;
    sDesNivel1 = DESNIVEL1;
    sCodNivel2 = CODNIVEL2;
    sDesNivel2 = DESNIVEL2;
    sCodZBS = CODZBS;
    sDesZBS = DESZBS;

  }

  //EN BLANCO
  public DataValoresEDO() {
  }

} //fin clase
