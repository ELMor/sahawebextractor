package comun;

import java.io.Serializable;
import java.util.Hashtable;

public abstract class DatoBase
    implements Serializable {

  private Hashtable h;

  protected DatoBase() {
    h = new Hashtable();
  }

  private void borra(String k) {
    if (h.containsKey(k)) {
      h.remove(k);
    }
  }

  protected void introduce(String k, Object o) {
    if (o == null) {
      borra(k);
    }
    else {
      if (o.getClass().getName().equals("java.lang.String")) {
        h.put(k, ( (String) o).trim());
      }
      else if (o.getClass().getName().equals("java.sql.Timestamp")) {
        introduceTimestamp(k, o);
      }
      else if (o.getClass().getName().equals("java.sql.Date")) {
        introduceDate(k, o);
      }
      else if (o.getClass().getName().equals("java.util.Date")) {
        introduceDate(k, o);
      }
      else {
        h.put(k, o);
      }
    }
  }

  private void introduceTimestamp(String k, Object o) {
    if (o == null) {
      borra(k);
    }
    else {
//      String s = Fechas.timestamp2String((java.sql.Timestamp) o);
      //LRG Se quita nombre paquete:  javac o navegador puede creer es la clase
      String s = Fechas.timestamp2String( (java.sql.Timestamp) o);
      h.put(k, ( (String) s).trim());
    }
  }

  private void introduceDate(String k, Object o) {
    String s = null;
    if (o == null) {
      borra(k);
    }
    else {
      if (o.getClass().getName().equals("java.sql.Date")) {
        //LRG Se quita nombre paquete:  javac o navegador puede creer es la clase
//         s = Fechas.date2String((java.sql.Date) o);
        s = Fechas.date2String( (java.sql.Date) o);
      }
      else {
        //LRG Se quita nombre paquete:  javac o navegador puede creer es la clase
//         s = Fechas.date2String((java.util.Date) o);
        s = Fechas.date2String( (java.util.Date) o);
      }
      h.put(k, s);
    }
  }

  protected Object saca(String k) {
    Object o = null;

    try {
      o = h.get(k);
    }
    catch (Exception e) {
    }

    return o;
  }

  protected java.sql.Date sacaDate(String k) {
    java.sql.Date d = null;

    try {
      String s = (String) h.get(k);
//      d = new java.sql.Date ( Fechas.string2Date(s).getTime() );
      d = new java.sql.Date(Fechas.string2Date(s).getTime());
    }
    catch (Exception e) {
    }

    return d;
  }

  protected java.sql.Timestamp sacaTimestamp(String k) {
    java.sql.Timestamp ts = null;

    try {
      String s = (String) h.get(k);
//      ts = Fechas.string2Timestamp(s);
      ts = Fechas.string2Timestamp(s);
    }
    catch (Exception e) {
    }

    return ts;
  }

}