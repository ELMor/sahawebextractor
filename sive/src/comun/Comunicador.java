package comun;

import java.net.URL;

import capp.CApp;
import capp.CLista;
import sapp.StubSrvBD;

// objeto comunicador
public class Comunicador {

  public Comunicador() {
  }

  public static final CLista Communicate(CApp app,
                                         StubSrvBD stubCliente,
                                         int iMode,
                                         String servlet,
                                         CLista data) throws Exception {

    CLista lista = null;
    lista = new CLista();

    // perfil
    data.setPerfil(app.getPerfil());
    // login
    data.setLogin(app.getLogin());
    // idioma
    data.setIdioma(app.getIdioma());

    // ARG: Lortad
    data.setLortad(app.getParameter("LORTAD"));

    // filter
    //data.setFilter("");

    stubCliente.setUrl(new URL(app.getURL() + servlet));
    lista = (CLista) stubCliente.doPost(iMode, data);

    lista.trimToSize();
    data = null;
    return lista;
  }
}
