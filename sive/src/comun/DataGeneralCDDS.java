package comun;

import java.io.Serializable;

public class DataGeneralCDDS
    implements Serializable {

  private String CDpadre = "";
  private String CD = "";
  private String DS = "";

  public DataGeneralCDDS(String cod, String des) {
    CD = cod;
    DS = des;
  }

  public DataGeneralCDDS(String codpadre, String cod, String des) {
    CDpadre = codpadre;
    CD = cod;
    DS = des;
  }

  public String getCDpadre() {
    return CDpadre;
  }

  public String getCD() {
    return CD;
  }

  public String getDS() {
    return DS;
  }
}