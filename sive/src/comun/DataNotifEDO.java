package comun;

import java.io.Serializable;

public class DataNotifEDO
    implements Serializable {

  protected String sFLAG_ALTA = "";
  protected String sFLAG_BAJA = "";
  protected String sFLAG_MOD = "";
  protected String sFLAG_ENFERMO = "";
  protected String sFLAG_VALIDAR = "";

  public DataNotifEDO() {
  }

  public DataNotifEDO(String sfAlta, String sfBaja, String sfMod,
                      String sfEnfer, String sfVal) {
    sFLAG_ALTA = sfAlta;
    sFLAG_BAJA = sfBaja;
    sFLAG_MOD = sfMod;
    sFLAG_ENFERMO = sfEnfer;
    sFLAG_VALIDAR = sfVal;
  }

  public String getFLAG_ALTA() {
    return sFLAG_ALTA;
  }

  public String getFLAG_BAJA() {
    return sFLAG_BAJA;
  }

  public String getFLAG_MOD() {
    return sFLAG_MOD;
  }

  public String getFLAG_ENFERMO() {
    return sFLAG_ENFERMO;
  }

  public String getFLAG_VALIDAR() {
    return sFLAG_VALIDAR;
  }
}
