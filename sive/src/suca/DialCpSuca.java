package suca;

import java.util.Enumeration;
import java.util.Hashtable;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import capp.CTabla;
//Im�genes
import comun.Common;
import jclass.bwt.JCActionEvent;
import sapp.StubSrvBD;

public class DialCpSuca
    extends CDialog {

//__________________________________________________ MODOS
  //modos de operaci�n de la ventana
  public final int modoINICIO = 0;
  public final int modoESPERA = 1;

  /** indica en que estado est� el frame */
  protected int modoOperacion = 0;
  protected int modoAnterior = 0;

  /** esta es la lista que contiene los datos que se muestran en la tabla */
  CLista listaCp = null;

  /** esta lista son los datos del enfermos seleccionado */
  CLista listaCpSeleccionado = null;

  /** Cp seleccionado */
  CLista listaDatosBusqueda = null;

  String cdvial = "";
  String cdmuni = "";
  String cdprov = "";
  String cdtpnum = "";
  String dscalnum = "";
  String dsnmportal = "";

  /** Para saber si hay datos tras una b�squeda */
  protected boolean bHayDatos = false;

  /** Cadena para el mensaje 'no hay datos' */
  private final String strNoHayDatos = "No se encontraron datos";

// SINCRONIZACION
  /** esta variable se pone a true cuando no se inhiben los eventos
   *  y a false se rechazan todos los eventos
   */
  protected boolean sinBloquear = true;

  public StubSrvBD stubCliente = new StubSrvBD();

//  protected panelsuca pnlSuca = null;
//__________________________________________________ COMP GRAFICOS

  XYLayout xYLayout = new XYLayout();
  CTabla tablaCP = new CTabla();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  GroupBox pnlEnfermedad = new GroupBox();

  //______________________________________________ GESTOR de EVENTOS
  BtnCpActionListener btnActionListener = new BtnCpActionListener(this);
  CpTableAdapter tableAdapter = new CpTableAdapter(this);

  //______________________________________________ CONSTRUCTOR
  public DialCpSuca(CApp app) {
    super(app);
    setTitle("Di�logo de c�d. postales");

    try {
      listaCp = new CLista();
      listaCp.setState(CLista.listaNOVALIDA);

      jbInit();

      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /** este m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = modoESPERA;
      //Inicializar();
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        Common.imgLUPA,
        Common.imgBUSCAR,
        Common.imgLIMPIAR,
        Common.imgACEPTAR,
        Common.imgCANCELAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // fijamos las dimensiones del di�logo
    setSize(300, 250);
    xYLayout.setHeight(275);
    xYLayout.setWidth(209);

    setLayout(xYLayout);

    // Im�genes de los botones
    btnAceptar.setImage(imgs.getImage(3));
    btnCancelar.setImage(imgs.getImage(4));

    tablaCP.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("C�digo Postal"), '\n'));
    tablaCP.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "160"), '\n'));
    tablaCP.setNumColumns(1);

    btnCancelar.setLabel("Cancelar");
    btnAceptar.setLabel("Aceptar");

    // a�adimos el nombre de los botones
    btnCancelar.setActionCommand("btnCancelar");
    btnAceptar.setActionCommand("btnAceptar");

    // a�adimos al panel todos los componentes
    this.add(tablaCP, new XYConstraints(15, 20, 180, 178));
    this.add(btnCancelar, new XYConstraints(113, 211, -1, -1));
    this.add(btnAceptar, new XYConstraints(36, 211, -1, -1));

    // gesti�n de eventos de botones
    btnCancelar.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);

    //gesti�n de eventos de la tabla
    tablaCP.addActionListener(tableAdapter);

    // se inicializan los botones a false o true
    this.modoOperacion = modoINICIO;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoINICIO:
        break;
      case modoESPERA:
        break;
    }
  }

  /**
   *  Esta funci�n devuelve el idioma
   */
  public int getIdioma() {
    return app.getIdioma();
  }

  public CLista getListaDatosCp() {
    return listaCpSeleccionado;
  }

  public void setListaDatosCp(CLista lista) {
    listaCpSeleccionado = lista;
  }

  protected void lanza_busqueda() {
    // rellenamos la hashtable solo con los datos que tengan valores utiles
    datasuca datosCp = new datasuca();

    String filtro = null;
    int indice = 0;
    CLista result = null;

    bHayDatos = true;

    listaDatosBusqueda = new CLista();
    listaDatosBusqueda.setIdioma(getIdioma());

    datosCp.put(panelsuca.CD_PROV, cdprov);
    datosCp.put(panelsuca.CD_MUN, cdmuni);
    datosCp.put("CDVIAL", cdvial);

    datosCp.put("CDTPNUM", cdtpnum);
    datosCp.put("DSCALNUM", dscalnum);
    datosCp.put("DSNMPORTAL", dsnmportal);

    listaDatosBusqueda.addElement(datosCp);

    listaCp = Common.traerDatos(app, stubCliente, panelsuca.strSERVLET_SUCA,
                                srvsuca.modoOBTENER_CDPOSTAL,
                                listaDatosBusqueda);

    if (listaCp == null) {
      CMessage msgBox = new CMessage(app, CMessage.msgAVISO, strNoHayDatos);
      msgBox.show();
      msgBox = null;
      bHayDatos = false;
    }
    else {
      escribirTabla(listaCp);
    }
  }

  // fue void
  boolean tabla_actionPerformed(JCActionEvent e) {
    int indice = tablaCP.getSelectedIndex();
    Hashtable h;
    Object code = null;
    String descrip = null;
    int indLista = -1;

    if (indice < 0) {
      return false;
    }

    if (indice == listaCp.size()) {
      /// se ha pinchado el elemento M�s...
      listaDatosBusqueda.setFilter(listaCp.getFilter());
      CLista result = Common.traerDatos(app, stubCliente,
                                        panelsuca.strSERVLET_SUCA,
                                        srvsuca.modoOBTENER_CDPOSTAL,
                                        listaDatosBusqueda);
      listaCp.appendData(result);
      escribirTabla(listaCp);
    }
    else {
      /// se buscan los datos del cp pinchado y se vuelve
      h = (Hashtable) (listaCp.elementAt(indice));

      datasuca dCp = new datasuca();

      dCp.put("CDPOSTAL", h.get("CDPOSTAL"));

      listaCpSeleccionado = new CLista();
      listaCpSeleccionado.setIdioma(app.getIdioma());
      listaCpSeleccionado.addElement(dCp);
    }
    return true;
  }

  // rellena la tabla
  void escribirTabla(CLista lista) {
    datasuca datUnaLinea;
    StringBuffer datLineaEscribir = new StringBuffer();
    String dato = null;
    int tam;

    //Borramos la tabla que hab�a
    tam = tablaCP.countItems();
    if (tam > 0) {
      tablaCP.deleteItems(0, tam - 1);
    }

    if (lista == null || (lista.size() == 0)) {
      CMessage msgBox = new CMessage(app, CMessage.msgAVISO, strNoHayDatos);
      msgBox.show();
      msgBox = null;
      bHayDatos = false;
      return;
    }

    Enumeration enum = lista.elements();

    while (enum.hasMoreElements()) {
      //Recogemos los datos de una l�nea
      datUnaLinea = (datasuca) (enum.nextElement());
      //ponemos a cero el buffer
      datLineaEscribir.setLength(0);

      Object o = (datUnaLinea.get("CDPOSTAL"));
      if (o != null) {
        datLineaEscribir.append(o.toString());
      }
      //A�adimos la l�nea a la tabla
      tablaCP.addItem(datLineaEscribir.toString(), '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)
    }

    // miramos si hay que a�adir Mas ...
    if (lista.getState() == CLista.listaINCOMPLETA) {
      //  como la lista est� incompleta ponemos lo de m�s
      tablaCP.addItem("M�s & ... &&", '&');
    }
  }

  /**
   *  esta funci�n fija los parametros para que se muestren los datos
   */
  public void setData(CLista a_dat) {

  }

  /**
   *  esta funci�n devuelve los valores calculados
   */
  public CLista getData() {
    return listaCpSeleccionado;
  }
}

/**
 *  esta clase recoge los eventos de pinchar en las lupas
 *  cuando se pincha a una lupa se llama a un servlet para pedir los datos
 *  se deshabilitan el resto de los botones lupa
 *  y se manda ejecutar la acci�n correspondiente
 */
// action listener de evento en bot�nes
class BtnCpActionListener
    implements ActionListener, Runnable {
  DialCpSuca adaptee = null;
  ActionEvent e = null;

  public BtnCpActionListener(DialCpSuca adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    String name = e.getActionCommand();
    String name2 = ( (Component) e.getSource()).getName();
    CLista lalista = null;
    String campo = null;

    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar();

    /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
    if (name.equals("btnAceptar")) {
      if (adaptee.tabla_actionPerformed(null)) {
        adaptee.dispose();
      }
    }
    else if (name.equals("btnCancelar")) {
      ////#// System_out.println("cancelar");
      /// nos salimos y borramos todo
      // le grabamos los datos pedidos
      adaptee.listaCpSeleccionado = null;
      adaptee.dispose();
    }
    adaptee.Inicializar();
    // desbloquea la recepci�n  de eventos
    adaptee.desbloquea();
  }
}

// escuchador de los click en la tabla
class CpTableAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  DialCpSuca adaptee;
  JCActionEvent e;

  CpTableAdapter(DialCpSuca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar();

    ////#// System_out.println("Evento de la tabla " + e.toString());
    adaptee.tabla_actionPerformed(e);
    adaptee.dispose();
    adaptee.desbloquea();
  }
}
//________________________________________________ END CLASS
