//Title:        Componente suca
//Version:
//Copyright:    Copyright (c) 1999
//Author:       NorSistemas
//Company:      NorSistemas
//Description:  Your description

package suca;

import java.net.URL;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
//import enfermo.comun;
import comun.Common;
import comun.constantes;
import sapp.StubSrvBD;

public class panelsuca
    extends CPanel {
  // Claves y descripciones para los elementos guardados en datasuca's
  protected static final String CD_PAIS = "CD_PAIS";
  protected static final String DS_PAIS = "DS_PAIS";
  protected static final String CD_CA = "CD_CA";
  protected static final String DS_CA = "DS_CA";
  protected static final String CD_PROV = "CD_PROV";
  protected static final String DS_PROV = "DS_PROV";
  protected static final String CD_MUN = "CD_MUN";
  protected static final String DS_MUN = "DS_MUN";

  // SINCRONIZACION
  private boolean sinBloquear = true;

  private int modoAnterior;

  private boolean bCtrlLimpiar = false;

  //Servlet
//  protected static final String strSERVLET_SUCA = "servlet/srvsuca";
  protected static final String strSERVLET_SUCA = constantes.strSERVLET_SUCA;

  // Conexi�n con el servlet
  private StubSrvBD stubCliente;

  // Variables empleadas para seleccionar el conjunto de controles que ha de mostrarse.
  private boolean bTramero = false;
  private boolean bModoTramero = false;

  // El panel est� habilitado/deshabilitado desde 'fuera'
  private boolean bHabilitado = true;

  // Variable empleada para saber si el desplegable de pa�ses debe estar activo o no.
  private boolean bSeleccionPais = true;

  // Variable empleada para saber si se a�aden los controles dependientes.
  private boolean bConfidencial = false;

  // Variable de zonificaci�n sanitaria.
  private zonificacionSanitaria zs = null;

  // Estado del contenedor de panelsuca.
  private int modoContenedor = 0;

  /** Lista que contiene las listas de las tablas maestras */
  CLista listaTotal = null;

  /** Lista que contiene todos los paises posibles */
  private CLista listaPaises = null;

  /** Lista que contiene las regiones posibles */
  private CLista listaCA = null;

  /** Lista que contiene las provincias */
  private CLista listaProvincias = null;

  /** Lista que contiene las vias */
  private CLista listaTrameros = null;

  /** Lista que contiene los numeros */
  private CLista listaNumeros = null;

  /** Lista que contiene los calificadores */
  private CLista listaCalificadores = null;

  /** Cadena para el mensaje 'hay m�s de 50 ocurrencias' */
  private final String strMasDe50 =
      "Se han encontrado m�s de 50 calles. Haga un filtrado m�s selectivo.";

  // contenedor de im�genes
  private CCargadorImagen imgs = null;

  // im�genes
  private final String imgNAME[] = {
      "images/Magnify.gif"};

  // modos de operaci�n de la ventana
  public static final int modoINICIO = 0;

  protected static final int modoESPERA = 1001;
  private static final int modoCONSULTA = 1002;
  public static final int modoCONFIDENCIAL = 1003;

  private static final int modoNOPAIS = 1004;
  private static final int modoPAIS = 1005;

  private static final int modoNOCA = 1006;
  private static final int modoCA = 1007;

  private static final int modoNOPROVINCIAS = 1008;
  private static final int modoPROVINCIAS = 1009;

  private static final int modoMUNICIPIO = 1010;

  protected int modoOperacion = modoINICIO;

  private XYLayout xyLayoutPanel = new XYLayout();
  private Label lblPais = new Label();
  private Choice choPaises = new Choice();
  private Label lblCA = new Label();
  private Choice choCA = new Choice();
  private Label lblProvincia = new Label();
  private Choice choProvincias = new Choice();
  private Label lblMunicipio = new Label();
  private CCampoCodigo txtCodMun = new CCampoCodigo();
  private ButtonControl btnMunicipio = new ButtonControl();
  private TextField txtDescMunicipio = new TextField();

  // Controles empleados en caso de bSuca = true
  private Label lblVial = new Label();
  private Choice choTrameros = new Choice();
  private Label lblNombreVia = new Label();
  private TextField txtVia = new TextField();
  private ButtonControl btnVia = new ButtonControl();
  private Label lblNum = new Label();
  private Choice choNumeros = new Choice();
  private Label lblPortal = new Label();
  private TextField txtPortal = new TextField();
  private ButtonControl btnPortal = new ButtonControl();
  private Label lblCalificador = new Label();
  private Choice choCalificador = new Choice();

  private Label lblCodPostal = new Label();
  private TextField txtCodPostal = new TextField();
  private ButtonControl btnCodPostal = new ButtonControl();

  // Controles empleados en caso de bSuca = false
  private Label lblNombre2Via = new Label();
  private TextField txt2Via = new TextField();
  private Label lblNo = new Label();
  private TextField txtNo = new TextField();
  private Label lblPiso = new Label();
  private TextField txtPiso = new TextField();
  private Label lbl2CodPostal = new Label();
  private TextField txt2CodPostal = new TextField();

  //  ESCUCHADOR PULSAR BOTONES:
  private Panel_SucabtnActionListener btnActionListener = new
      Panel_SucabtnActionListener(this);
  //Esc p�rdida de foco
  private Panel_SucaFocusAdapter focusListener = new Panel_SucaFocusAdapter(this);
  //Escuchador cambio en una caja de texto
  private Panel_SucaTextListener txtTextListener = new Panel_SucaTextListener(this);
  //Selecci�n en una lista desplegable
  private Panel_SucaChoiceItemListener chItemListener = new
      Panel_SucaChoiceItemListener(this);

  private String strCodMunAux = null;
  private String strTxtViaAux = null;
  private String strTxtPortalAux = null;
  private String cdvial = "";
  private boolean bIdiomaPorDefecto = true;

  // ARG: (5/6/02)
  private boolean bCodigoPostalActualizado = false;

  //***************************************
   //   Estado del contenedor de panelsuca
   //***************************************

    private void ponerEnEsperaContenedor() {
      if (zs != null) {
        modoContenedor = zs.ponerEnEspera();
      }
    }

  private void ponerModoContenedor() {
    if (zs != null) {
      zs.ponerModo(modoContenedor);
    }
  }

  //***************************
   //   Interfaz
   //***************************

    // Estado del panel.
    public boolean getModoTramero() {
      return bTramero;
    }

  public void setmodoOperacion(int modo) {
    modoOperacion = modo;
  }

  /** Inhabilita todos los objetos y pone el cursor en modo de espera. */
  public void setModoEspera() {
    //#// System_out.println(" setModoEspera ");
    setModoDeshabilitado();
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    //#// System_out.println(" setCursor(new Cursor(Cursor.WAIT_CURSOR)) ");
  }

  /** Inhabilita todos los objetos. */
  public void setModoDeshabilitado() {
    //#// System_out.println("setModoDeshabilitado");
    if (bHabilitado) {
      int modoTmp = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar();
      modoOperacion = modoTmp;
      bHabilitado = false;
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    //#// System_out.println("setCursor(new Cursor(Cursor.DEFAULT_CURSOR))");
  }

  /** Restaura el estado anterior del panel y pone el cursor en modo normal. */
  public void setModoNormal() {
    /** modoOperacion = modoAnterior; */
    //#// System_out.println("setModoNormal");
    bHabilitado = true;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    //#// System_out.println("setCursor(new Cursor(Cursor.DEFAULT_CURSOR))");
    // bHabilitado = true;
  }

  // M�todos de lectura.

  /** Obtiene el c�digo del pa�s activo */
  public String getCD_PAIS() {
    return getDatoLista(choPaises, CD_PAIS, listaPaises, "");
  }

  /** Obtiene el c�digo de la comunidad aut�noma activa */
  public String getCD_CA() {
    return getDatoLista(choCA, CD_CA, listaCA, "");
  }

  /** Obtiene el c�digo de la provincia activa */
  public String getCD_PROV() {
    return getDatoLista(choProvincias, CD_PROV, listaProvincias, "");
  }

  /** Obtiene el c�digo del municipio */
  public String getCD_MUN() {
    return txtCodMun.getText().trim();
  }

  /** Obtiene la descripci�n del municipio */
  public String getDS_MUN() {
    return txtDescMunicipio.getText().trim();
  }

  /** Obtiene el nombre de la v�a */
  public String getDS_DIREC() {
    String strValor;

    if (bTramero) {
      strValor = txtVia.getText().trim();
    }
    else {
      strValor = txt2Via.getText().trim();
    }
    return strValor;
  }

  /** Obtiene el piso en modo no tramero */
  public String getDS_PISO() {
    String strValor = "";

    if (!bTramero) {
      strValor = txtPiso.getText().trim();
    }
    return strValor;
  }

  /** Obtiene el n�. del portal en modo tramero */
  public String getDS_NUM() {
    String strValor = "";

    if (bTramero) {
      strValor = txtPortal.getText().trim();
    }
    else {
      strValor = txtNo.getText().trim();
    }
    return strValor;
  }

  /** Obtiene el c�digo postal */
  public String getCD_POSTAL() {
    String strValor;

    if (bTramero) {
      strValor = txtCodPostal.getText().trim();
    }
    else {
      strValor = txt2CodPostal.getText().trim();
    }
    return strValor;
  }

  /** Obtiene el c�digo del vial activo */
  public String getCDTVIA() {
    String strValor = "";

    if (bTramero) {
      strValor = getDatoLista(choTrameros, "CDTVIA", listaTrameros, "");
    }
    return strValor;
  }

  /** Obtiene el c�digo del calificador activo */
  public String getDSCALNUM() {
    String strValor = "";

    if (bTramero) {
      strValor = getDatoLista(choCalificador, "DSCALNUM", listaCalificadores,
                              "");
    }
    return strValor;
  }

  /** Obtiene el c�digo del tipo de numeraci�n activo */
  public String getCDTNUM() {
    String strValor = "";

    if (bTramero) {
      strValor = getDatoLista(choNumeros, "CDTNUM", listaNumeros, "");
    }
    return strValor;
  }

  /** Obtiene el valor actual de la variable cdvial */
  public String getCDVIAL() {
    String strValor = "";

    if ( (bTramero) && (cdvial != null)) {
      strValor = cdvial;
    }
    return strValor;
  }

  public CLista getListaPaises() {
    return listaPaises;
  }

  public CLista getListaCA() {
    return listaCA;
  }

  public CLista getListaProvincias() {
    return listaProvincias;
  }

  public CLista getListaTrameros() {
    return listaTrameros;
  }

  public CLista getListaNumeros() {
    return listaNumeros;
  }

  public CLista getListaCalificadores() {
    return listaCalificadores;
  }

  //*************************************
   // M�todos de escritura
   //*************************************

    /** Establece un pa�s en el choice correspondiente a partir de su c�digo */
    public void setCD_PAIS(String strValor) {
      choPaises.select(getIndiceDeClave(listaPaises, strValor, CD_PAIS) + 1);
    }

  /** Establece una comunidad aut�noma en el choice correspondiente a partir
     de su c�digo */
  public void setCD_CA(String strValor) {
    if ( (getIndiceDeClave(listaCA, strValor, CD_CA) + 1) !=
        choCA.getSelectedIndex()) {
      choCA.select(getIndiceDeClave(listaCA, strValor, CD_CA) + 1);
      choCA_Click(); // E 17/02/2000
    }
    else {
      ctrlLimpiar();
    }
  }

  /** Establece una provincia en el choice correspondiente a partir de su
     c�digo */
  public void setCD_PROV(String strValor) {
    try {
      if (choCA.getSelectedIndex() > 0) {
        choProvincias.select(getIndiceDeClave(listaProvincias, strValor,
                                              CD_PROV) + 1);
        choProvincia_Click();
      }
    }
    catch (Exception e) {
      // System_out.println(e.getMessage());
    }
  }

  /** Establece el valor de la caja de texto c�digo de municipio */
  public void setCD_MUN(String strValor) {
    txtCodMun.setText(strValor);
    if (!strValor.trim().equals("")) {
      modoOperacion = modoMUNICIPIO;
      Inicializar();
      ctrlLimpiar();
    }
  }

  /** Establece el valor de la caja de texto descripci�n del municipio */
  public void setDS_MUN(String strValor) {
    txtDescMunicipio.setText(strValor.trim());
  }

  /** Establece el valor de la caja de texto nombre de un vial */
  public void setDS_DIREC(String strValor) {
    if (bTramero) {
      txtVia.setText(strValor);
    }
    else {
      txt2Via.setText(strValor);
    }
  }

  /** Establece el valor de la caja de texto n�mero de portal,
     en modo no tramero */
  public void setDS_PISO(String strValor) {
    if (!bTramero) {
      txtPiso.setText(strValor);
    }
  }

  /** Establece el valor de la caja de texto n�mero de portal,
     en modo tramero */
  public void setDS_NUM(String strValor) {
    if (bTramero) {
      txtPortal.setText(strValor);
    }
    else {
      txtNo.setText(strValor);
    }
  }

  /** Establece el valor de la caja de texto c�digo postal */
  public void setCD_POSTAL(String strValor) {
    if (bTramero) {
      txtCodPostal.setText(strValor);
    }
    else {
      txt2CodPostal.setText(strValor);
    }
  }

  /** Establece el flag bCodigoPostalActualizado */
  public void setCodigoPostalActualizado(boolean actualizado) {
    this.bCodigoPostalActualizado = actualizado;
  }

  /** Devuelve el flag bCodigoPostalActualizado */
  public boolean getCodigoPostalActualizado() {
    return bCodigoPostalActualizado;
  }

  /** Establece un vial en el choice correspondiente a partir de su
     c�digo, en modo tramero */
  public void setCDTVIA(String strValor) {
    if (bTramero) {
      // choTrameros.select(getNombreTramero(strValor));
      // En este caso concreto, se visualiza el c�digo de la entidad.
      choTrameros.select(strValor);
    }
  }

  /** Establece un calificador en el choice correspondiente a partir de su
     c�digo, en modo tramero */
  public void setDSCALNUM(String strValor) {
    if (bTramero) {
      //choCalificador.select(getNombreCalificador(strValor));
      choCalificador.select(getIndiceDeClave(listaCalificadores, strValor,
                                             "DSCALNUM") + 1);
    }
  }

  /** Establece un tipo de numeraci�n en el choice correspondiente a partir
     de su c�digo */
  public void setCDTNUM(String strValor) {
    if (bTramero) {
      //choNumeros.select(getNombreTipoNumero(strValor));
      choNumeros.select(getIndiceDeClave(listaNumeros, strValor, "CDTNUM") + 1);
    }
  }

  /** Establece el valor de la variable cdvial */
  public void setCDVIAL(String strValor) {
    cdvial = new String(strValor);
  }

  //**************************
   //   Constructores
   //**************************

    //Constructor 1: Carga las listas a trav�s de una llamada al servlet.
    public panelsuca(CApp a, boolean bTramero, int modoOperacion,
                     boolean bSeleccionPais, zonificacionSanitaria zs) {
      CLista listaAux = new CLista();
      int modoLlamadaServlet;

      setApp(a);

      try {
        this.bTramero = bTramero;
        this.bModoTramero = bTramero;
        this.modoOperacion = modoOperacion;
        this.bSeleccionPais = bSeleccionPais;

        if (this.modoOperacion == modoCONFIDENCIAL) {
          this.modoOperacion = modoINICIO;
          bConfidencial = true;
        }

        stubCliente = new StubSrvBD();
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_SUCA));

        modoLlamadaServlet = (bTramero ? srvsuca.modoLISTAMAESTRAS :
                              srvsuca.modoLISTAMAESTRAS_SIN_TRAMERO);
        bIdiomaPorDefecto = (a.getIdioma() == a.idiomaPORDEFECTO);

        listaAux.addElement(bIdiomaPorDefecto ? "S" : "N");
        listaTotal = (CLista) stubCliente.doPost(modoLlamadaServlet, listaAux);

        jbInit();
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      this.zs = zs;
    }

  //Constructor 2: Carga las listas a trav�s del par�metro datosListas.
  public panelsuca(CApp a, boolean bTramero, int modoOperacion,
                   boolean bSeleccionPais, CLista datosListas,
                   zonificacionSanitaria zs) {

    setApp(a);

    try {
      this.bTramero = bTramero;
      this.bModoTramero = bTramero;
      this.modoOperacion = modoOperacion;
      this.bSeleccionPais = bSeleccionPais;

      if (this.modoOperacion == modoCONFIDENCIAL) {
        this.modoOperacion = modoINICIO;
        bConfidencial = true;
      }

      stubCliente = new StubSrvBD();
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET_SUCA));

      bIdiomaPorDefecto = (a.getIdioma() == a.idiomaPORDEFECTO);
      listaTotal = datosListas;

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    // �ltima asignaci�n para no interferir con jbinit
    this.zs = zs;
  }

  //Fin de constructores

  private void jbInit() throws Exception {

    // Carga las listas a partir de listaTotal
    listaPaises = (CLista) listaTotal.elementAt(0);
    listaCA = (CLista) listaTotal.elementAt(1);

    listaTrameros = (CLista) listaTotal.elementAt(2);
    listaNumeros = (CLista) listaTotal.elementAt(3);
    listaCalificadores = (CLista) listaTotal.elementAt(4);

    xyLayoutPanel.setHeight(116);
    xyLayoutPanel.setWidth(596);

    lblPais.setText("Pais");
    lblCA.setText("C.A.");
    lblProvincia.setText("Provincia");
    lblMunicipio.setText("Municipio");
    lblNombreVia.setText("Nombre");
    lblNum.setText("Num.");
    lblPortal.setText("Portal");
    lblCalificador.setText("Calificador");
    lblCodPostal.setText("C. Postal");
    lbl2CodPostal.setText("C. Postal");
    lblNombre2Via.setText("Via");
    lblNo.setText("N�.");
    lblPiso.setText("Piso");
    lblVial.setText("Vial");

    txtDescMunicipio.setEditable(false);
    txtDescMunicipio.setEnabled(false);

    this.setLayout(xyLayoutPanel);
    this.add(lblPais, new XYConstraints(3, 3, 32, 19));
    this.add(choPaises, new XYConstraints(59, 3, 181, 24));
    this.add(lblCA, new XYConstraints(249, 3, 28, 19));
    this.add(choCA, new XYConstraints(308, 3, 278, 24));
    this.add(lblProvincia, new XYConstraints(3, 32, 54, 19));

    this.add(choProvincias, new XYConstraints(59, 32, 181, 24));
    this.add(lblMunicipio, new XYConstraints(249, 34, 55, 19));
    this.add(txtCodMun, new XYConstraints(308, 32, 49, 24));
    this.add(btnMunicipio, new XYConstraints(362, 30, -1, -1));
    this.add(txtDescMunicipio, new XYConstraints(400, 32, 186, 24));

    // Comienza la inserci�n de los controles dependientes
    if (!bConfidencial) {
      // Controles para el modo tramero
      this.add(lblVial, new XYConstraints(3, 61, 39, 19));
      this.add(choTrameros, new XYConstraints(3, 82, 67, 23));
      this.add(lblNombreVia, new XYConstraints(75, 62, 48, 18));
      this.add(txtVia, new XYConstraints(75, 82, 135, 19));
      this.add(btnVia, new XYConstraints(214, 82, -1, -1));
      this.add(lblNum, new XYConstraints(249, 61, 38, 21));
      this.add(choNumeros, new XYConstraints(249, 82, 55, 23));
      this.add(lblPortal, new XYConstraints(311, 62, 37, 18));
      this.add(txtPortal, new XYConstraints(312, 82, 37, 19));
      this.add(btnPortal, new XYConstraints(345, 81, -1, -1));
      this.add(lblCalificador, new XYConstraints(391, 61, 61, 20));
      this.add(choCalificador, new XYConstraints(391, 82, 80, 23));
      this.add(lblCodPostal, new XYConstraints(476, 61, 54, 19));
      this.add(txtCodPostal, new XYConstraints(476, 82, 80, 19));
      this.add(btnCodPostal, new XYConstraints(562, 81, -1, -1));

      // Controles para el modo no tramero
      this.add(lblNombre2Via, new XYConstraints(3, 61, 43, 19));
      this.add(txt2Via, new XYConstraints(3, 83, 370, 24));
      this.add(lblNo, new XYConstraints(384, 61, 25, 19));
      this.add(txtNo, new XYConstraints(381, 83, 42, 24));
      this.add(lblPiso, new XYConstraints(433, 61, 38, 19));
      this.add(txtPiso, new XYConstraints(431, 83, 75, 24));
      this.add(lbl2CodPostal, new XYConstraints(518, 61, 54, 19));
      this.add(txt2CodPostal, new XYConstraints(518, 83, 68, 24));
    }

    //Fin pintado de controles dependientes

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnMunicipio.setImage(imgs.getImage(0));
    btnVia.setImage(imgs.getImage(0));
    btnPortal.setImage(imgs.getImage(0));
    btnCodPostal.setImage(imgs.getImage(0));

    // Panel sin borde
    this.setBorde(false);

    //Establece escuchadores, comandos y nombres
    btnMunicipio.addActionListener(btnActionListener);
    btnVia.addActionListener(btnActionListener);
    btnPortal.addActionListener(btnActionListener);
    btnCodPostal.addActionListener(btnActionListener);

    txtCodMun.addTextListener(txtTextListener);
    txtCodMun.addFocusListener(focusListener);

    txtVia.addTextListener(txtTextListener);
    txtVia.addFocusListener(focusListener);

    txtPortal.addTextListener(txtTextListener);
    txtPortal.addFocusListener(focusListener);

    txt2Via.addFocusListener(focusListener);

    txtCodPostal.addFocusListener(focusListener);

    choPaises.addItemListener(chItemListener);
    choCA.addItemListener(chItemListener);
    choProvincias.addItemListener(chItemListener);
    choTrameros.addItemListener(chItemListener);

    btnMunicipio.setActionCommand("buscarMunicipio");
    btnVia.setActionCommand("buscarVia");
    btnPortal.setActionCommand("buscarPortal");
    btnCodPostal.setActionCommand("buscarCodPostal");

    txtCodMun.setName("txtCodMun");
    txtVia.setName("txtVia");
    txtPortal.setName("txtPortal");
    txt2Via.setName("txt2Via");
    txtCodPostal.setName("txtCodPostal");

    choPaises.setName("choPais");
    choCA.setName("choCA");
    choProvincias.setName("choProvincia");
    choTrameros.setName("choTramero");

    // Se rellenan los choices
    Common.writeChoice(app, choTrameros, listaTrameros, true, "CDTVIA");
    Common.writeChoice(app, choNumeros, listaNumeros, true, "DSABREV");
    Common.writeChoice(app, choCalificador, listaCalificadores, true,
                       "DSDSCALNUM");
    // Primero el de pa�ses
    Common.writeChoice(app, choPaises, listaPaises, true, DS_PAIS);
    // Se muestra Espa�a como pa�s por defecto en el choice de pa�ses.
    setCD_PAIS("ESP");
    // Se rellena el de comunidades aut�nomas
    Common.writeChoice(app, choCA, listaCA, true, DS_CA);
    // Se selecciona la CA del usuario
    setCD_CA(this.app.getCA());

    // Se asigna modo
    modoOperacion = (listaProvincias.size() == 1 ? modoPROVINCIAS :
                     modoNOPROVINCIAS);
    modoAnterior = modoOperacion;

    Inicializar();
  }

  public void paint(Graphics g) {
    super.paint(g);
    ctrlVisibles();
  }

//**************************************************************
//**************************************************************
//                        M�todos
//**************************************************************
//**************************************************************

      private void cargarProvincias(int indexCA) {
        CLista appList = null;
        CLista data = new CLista();
        int modoLlamadaServlet;

        if (indexCA >= 0) {
          appList = listaCA;
          String codCA = (String) ( (datasuca) appList.elementAt(indexCA)).get(
              CD_CA);

          listaProvincias = null;

          datasuca dat = new datasuca();
          dat.put(CD_CA, codCA);
          data.addElement(dat);

          // Distintas posibilidades de llamada
          modoLlamadaServlet = (bTramero ? srvsuca.modoPROVINCIAS :
                                srvsuca.modoPROVINCIAS_SIN_TRAMERO);
          if (!bTramero) {
            data.addElement(bIdiomaPorDefecto ? "S" : "N");
          }

          listaProvincias = Common.traerDatos(app, stubCliente, strSERVLET_SUCA,
                                              modoLlamadaServlet, data);

          if (choProvincias.getItemCount() > 0) {
            choProvincias.removeAll();

          }
          if (listaProvincias.size() > 0) {
            choProvincias.add(" ");
            Common.writeChoice(getApp(), choProvincias, listaProvincias, false,
                               DS_PROV);
            if (listaProvincias.size() == 1) {
              choProvincias.select(1);
              modoOperacion = modoPROVINCIAS;
            }
            else {
              modoOperacion = modoNOPROVINCIAS;
            }
          }
        }
      }

  private String getDatoLista(Choice choice, String dato, CLista lista,
                              String valorDefecto) {
    String strTemp = valorDefecto;
    int indice = choice.getSelectedIndex();
    indice--;
    if (indice >= 0) {
      strTemp = (String) ( (datasuca) lista.elementAt(indice)).get(dato);
      if (strTemp == null) {
        strTemp = valorDefecto;
      }
    }
    return strTemp;
  }

// M�todo que obtiene la descripci�n de un PA�S a partir de su c�digo.
  private String getNombrePais(String strPais) {
    return getDatoDeClave(listaPaises, strPais, CD_PAIS, DS_PAIS);
  }

// M�todo que obtiene la descripci�n de una CA a partir de su c�digo.
  private String getNombreCA(String strCA) {
    return getDatoDeClave(listaCA, strCA, CD_CA, DS_CA);
  }

// M�todo que obtiene la descripci�n de una Provincia a partir de su c�digo.
  private String getNombreProvincia(String strProvincia) {
    return getDatoDeClave(listaProvincias, strProvincia, CD_PROV, DS_PROV);
  }

// M�todo que obtiene la descripci�n de un Vial a partir de su c�digo.
  private String getNombreTramero(String strTramero) {
    return getDatoDeClave(listaTrameros, strTramero, "CDTVIA", "DSTVIA");
  }

// M�todo que obtiene la descripci�n de un Calificador a partir de su c�digo.
  private String getNombreCalificador(String strCalificador) {
    return getDatoDeClave(listaCalificadores, strCalificador, "DSCALNUM",
                          "DSDSCALNUM");
  }

// M�todo que obtiene la descripci�n de un Tipo de N�mero a partir de su c�digo.
  private String getNombreTipoNumero(String strTipoNumero) {
    return getDatoDeClave(listaNumeros, strTipoNumero, "CDTNUM", "DSABREV");
  }

// M�todo que obtiene, de una lista, una descripci�n a partir de un c�digo.
  private String getDatoDeClave(CLista lista, String strCod,
                                String strNombreCod, String strNombreDes) {
    String strDescripcion = " ";

    for (int i = 0; i < lista.size(); i++) {
      if ( ( (datasuca) lista.elementAt(i)).get(strNombreCod).equals(strCod)) {
        strDescripcion = (String) ( (datasuca) lista.elementAt(i)).get(
            strNombreDes);
        break;
      }
    }
    return strDescripcion;
  }

// M�todo que obtiene, de una lista, el �ndice de un elemento a partir de un c�digo.
  private int getIndiceDeClave(CLista lista, String strCod, String strNombreCod) {
    int indiceClave = -1;

    for (int i = 0; i < lista.size(); i++) {
      if ( ( (datasuca) lista.elementAt(i)).get(strNombreCod) != null) {
        if ( ( (datasuca) lista.elementAt(i)).get(strNombreCod).equals(strCod)) {
          indiceClave = i;
          break;
        }
      }
    }
    return indiceClave;
  }

// Sistema de bloqueo de eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = modoESPERA;
      //
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** Este m�todo desbloquea el sistema */
  protected synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

// Iniciar controles seg�n el modo de operaci�n.
  public void Inicializar() {
    if (!bCtrlLimpiar) {
      if (bHabilitado) {
        switch (modoOperacion) {
          case modoPAIS:
            choPaises.setEnabled(true);
            choCA.setEnabled(true);
            choProvincias.setEnabled(false);
            txtCodMun.setEnabled(false);
            btnMunicipio.setEnabled(false);
            ctrlActivados(false);
            break;
          case modoNOPAIS:
            choPaises.setEnabled(bSeleccionPais);
            choCA.setEnabled(false);
            choProvincias.setEnabled(false);
            txtCodMun.setEnabled(false);
            btnMunicipio.setEnabled(false);
            ctrlActivados(true);
            break;
          case modoCA:
            choPaises.setEnabled(bSeleccionPais);
            choCA.setEnabled(true);
            choProvincias.setEnabled(true);
            txtCodMun.setEnabled(false);
            btnMunicipio.setEnabled(false);
            break;
          case modoNOCA:
            choPaises.setEnabled(bSeleccionPais);
            choCA.setEnabled(true);
            choProvincias.setEnabled(false);
            txtCodMun.setEnabled(false);
            btnMunicipio.setEnabled(false);
            break;
          case modoINICIO:
            choPaises.setEnabled(bSeleccionPais);
            choCA.setEnabled(true);
            choProvincias.setEnabled(false);
            txtCodMun.setEnabled(false);
            btnMunicipio.setEnabled(false);
            ctrlActivados(false);
            break;
          case modoCONSULTA:
          case modoESPERA:
            choPaises.setEnabled(false);
            choCA.setEnabled(false);
            choProvincias.setEnabled(false);
            txtCodMun.setEnabled(false);
            btnMunicipio.setEnabled(false);
            ctrlActivados(false);
            break;
          case modoPROVINCIAS:
            choPaises.setEnabled(bSeleccionPais);
            choCA.setEnabled(true);
            choProvincias.setEnabled(true);
            txtCodMun.setEnabled(true);
            btnMunicipio.setEnabled(true);
            ctrlActivados(false);
            break;
          case modoNOPROVINCIAS:
            choPaises.setEnabled(bSeleccionPais);
            choCA.setEnabled(true);
            choProvincias.setEnabled(true);
            txtCodMun.setEnabled(false);
            btnMunicipio.setEnabled(false);
            ctrlActivados(false);
            break;
          case modoMUNICIPIO:
            choPaises.setEnabled(bSeleccionPais);
            choCA.setEnabled(true);
            choProvincias.setEnabled(true);
            txtCodMun.setEnabled(true);
            btnMunicipio.setEnabled(true);
            ctrlActivados(true);
            break;
        }
      }
    }
  }

// Activar/desactivar los controles dependientes
  private void ctrlActivados(boolean bValor) {
    if (!bConfidencial) {
      if (bTramero) {
        choTrameros.setEnabled(bValor);
        txtVia.setEnabled(bValor);
        btnVia.setEnabled(bValor);
        choNumeros.setEnabled(bValor);
        txtPortal.setEnabled(bValor);
        btnPortal.setEnabled(bValor && !cdvial.equals(""));
        choCalificador.setEnabled(bValor);
        txtCodPostal.setEnabled(bValor);
        btnCodPostal.setEnabled(bValor);
      }
      else {
        txt2Via.setEnabled(bValor);
        txtNo.setEnabled(bValor);
        txtPiso.setEnabled(bValor);
        txt2CodPostal.setEnabled(bValor);
      }
    }
  }

// Vaciar los controles dependientes
  private void ctrlLimpiar() {
    if (!bCtrlLimpiar) {

      bCtrlLimpiar = true;

      if (modoOperacion != modoMUNICIPIO) {
        txtCodMun.setText("");
        txtDescMunicipio.setText("");
      }
      if (bTramero) {
        if (choTrameros.getItemCount() > 0) {
          choTrameros.select(0);
        }
        txtVia.setText("");
        if (choNumeros.getItemCount() > 0) {
          choNumeros.select(0);
        }
        txtPortal.setText("");
        if (choCalificador.getItemCount() > 0) {
          choCalificador.select(0);
        }
        txtCodPostal.setText("");
      }
      else {
        txt2Via.setText("");
        txtNo.setText("");
        txtPiso.setText("");
        txt2CodPostal.setText("");
      }
      bCtrlLimpiar = false;
    }
  }

// Limpia los datos de municipio en adelante
  public void setLimpiar() {
    boolean bTmp = bHabilitado;

    bHabilitado = false;

    txtCodMun.setText("");
    txtDescMunicipio.setText("");

    if (!bConfidencial) {
      if (bTramero) {
        if (choTrameros.getItemCount() > 0) {
          choTrameros.select(0);
        }
        txtVia.setText("");
        if (choNumeros.getItemCount() > 0) {
          choNumeros.select(0);
        }
        txtPortal.setText("");
        if (choCalificador.getItemCount() > 0) {
          choCalificador.select(0);
        }
        txtCodPostal.setText("");
      }
      else {
        txt2Via.setText("");
        txtNo.setText("");
        txtPiso.setText("");
        txt2CodPostal.setText("");
      }
    }

    bHabilitado = bTmp;
  }

// Hacer visibles/invisibles los controles dependientes
  private void ctrlVisibles() {

    lblVial.setVisible(bTramero);
    choTrameros.setVisible(bTramero);
    lblNombreVia.setVisible(bTramero);
    txtVia.setVisible(bTramero);
    btnVia.setVisible(bTramero);
    lblNum.setVisible(bTramero);
    choNumeros.setVisible(bTramero);
    lblPortal.setVisible(bTramero);
    txtPortal.setVisible(bTramero);
    btnPortal.setVisible(bTramero);
    lblCalificador.setVisible(bTramero);
    choCalificador.setVisible(bTramero);
    lblCodPostal.setVisible(bTramero);
    txtCodPostal.setVisible(bTramero);
    btnCodPostal.setVisible(bTramero);

    btnVia.setBounds(215, 82, 26, 27);
    btnPortal.setBounds(355, 81, 26, 27);
    btnCodPostal.setBounds(562, 81, 26, 27);

    lblNombre2Via.setVisible(!bTramero);
    txt2Via.setVisible(!bTramero);
    lblNo.setVisible(!bTramero);
    txtNo.setVisible(!bTramero);
    lblPiso.setVisible(!bTramero);
    txtPiso.setVisible(!bTramero);
    lbl2CodPostal.setVisible(!bTramero);
    txt2CodPostal.setVisible(!bTramero);
  }

  private void ctrlLimpiarTrameros() {
    if (choNumeros.getItemCount() > 0) {
      choNumeros.select(0);
    }
    if (choCalificador.getItemCount() > 0) {
      choCalificador.select(0);
    }
    txtPortal.setText("");
    txtCodPostal.setText("");
  }

// ARG: Metodo que pone color de fondo obligatorio (amarillo) los campos de Provincia y Municipio
  public void setObligatorios() {
    txtCodMun.setBackground(new Color(255, 255, 150));
    choProvincias.setBackground(new Color(255, 255, 150));
  }

//**************************************************************
//**************************************************************
// M�todos de gesti�n de eventos                               *
//**************************************************************
//**************************************************************

      protected void btnCtrlbuscarPortal_actionPerformed() {
        DialPortalSuca dlgPortal = null;
        CLista listaPortalAux = null;
        datasuca elPortal = null;

        modoOperacion = modoESPERA;
        Inicializar();
        ponerEnEsperaContenedor();

        dlgPortal = new DialPortalSuca(app);
        dlgPortal.modoLlamada = srvsuca.modoPORTAL_I;

        dlgPortal.cdvial = getCDVIAL();
        dlgPortal.cdmuni = txtCodMun.getText();

        dlgPortal.cdtpnum = getCDTNUM();
        dlgPortal.dscalnum = getDSCALNUM();
        dlgPortal.dsnumportal = getDS_NUM();

        //para el cdpostal
        dlgPortal.prov = getCD_PROV();

        dlgPortal.lanza_busqueda();

        if (dlgPortal.bHayDatos) {
          dlgPortal.show();

          listaPortalAux = dlgPortal.getListaDatosPortal();

          if (listaPortalAux != null) {
            elPortal = (datasuca) listaPortalAux.firstElement();
            choNumeros.select(getIndiceDeClave(listaNumeros,
                                               (String) elPortal.get("DSTNUM"),
                                               "DSTNUM") + 1);
            txtPortal.setText( (String) elPortal.get("DSNMPORTAL"));
            choCalificador.select(getIndiceDeClave(listaCalificadores,
                (String) elPortal.get("DSDSCALNUM"), "DSDSCALNUM") + 1);
            txtCodPostal.setText( (String) elPortal.get("CDPOSTAL"));
            setCodigoPostalActualizado(true);
          }
        }

        Inicializar();
        ponerModoContenedor();
      }

  protected void btnCtrlbuscarVial_actionPerformed() {
    DialVialSuca dlgVial = null;
    CLista listaVialAux = null;
    datasuca elVial = null;

    modoOperacion = modoESPERA;
    Inicializar();
    ponerEnEsperaContenedor();

    dlgVial = new DialVialSuca(app);
    dlgVial.cdtvia = getCDTVIA();
    dlgVial.cdmuni = txtCodMun.getText();
    dlgVial.dsvianorm = txtVia.getText();

    dlgVial.lanza_busqueda();

    if (dlgVial.bHayDatos) {
      if (dlgVial.bMas50Viales) {
        CMessage msgBox = new CMessage(app, CMessage.msgAVISO, strMasDe50);
        msgBox.show();
        msgBox = null;
      }
//    else {

      dlgVial.show();

      listaVialAux = dlgVial.getListaDatosVial();

      if (listaVialAux != null) {
        elVial = (datasuca) listaVialAux.firstElement();
        choTrameros.select( (String) elVial.get("CDTVIA"));
        txtVia.setText( (String) elVial.get("DSVIAOFIC"));
        cdvial = (String) elVial.get("CDVIAL");
        ctrlLimpiarTrameros();
        // informa de que se ha escrito un vial
        if (zs != null) {
          zs.vialInformado();
        }
//      }
      }
    }

    Inicializar();
    ponerModoContenedor();
  }

  protected void btnCtrlbuscarCodPostal_actionPerformed() {
    DialCpSuca dlgCp = null;
    CMessage msgBox = null;
    CLista listaCp = null;
    String cadena = "";
    datasuca elCp = null;

    modoOperacion = modoESPERA;
    Inicializar();
    ponerEnEsperaContenedor();

    dlgCp = new DialCpSuca(app);

    dlgCp.cdvial = cdvial;
    dlgCp.cdmuni = txtCodMun.getText();
    dlgCp.cdprov = getCD_PROV();

    dlgCp.cdtpnum = getCDTNUM();
    dlgCp.dscalnum = getDatoLista(choCalificador, "DSCALNUM",
                                  listaCalificadores, "0");

    dlgCp.dsnmportal = txtPortal.getText().trim();

    dlgCp.lanza_busqueda();

    if (dlgCp.bHayDatos) {
      dlgCp.show();

      listaCp = dlgCp.getListaDatosCp();

      if (listaCp != null) {
        elCp = (datasuca) listaCp.firstElement();
        txtCodPostal.setText( (String) elCp.get("CDPOSTAL"));
        setCodigoPostalActualizado(true);
      }
    }
    Inicializar();
    ponerModoContenedor();
  }

  protected void txtFocusPortal() {

    DialPortalSuca dlgPortal = null;
    datasuca elPortal = null;
    CLista listaPortalAux = null;

    txtPortal.setText(txtPortal.getText().trim());

    if (txtPortal.getText().equals("")) {
      return;
    }

    if (!txtPortal.getText().equals(strTxtPortalAux)) {
      modoOperacion = modoESPERA;
      Inicializar();
      ponerEnEsperaContenedor();

      try {
        dlgPortal = new DialPortalSuca(app);
        dlgPortal.modoLlamada = srvsuca.modoPORTAL_II;

        dlgPortal.cdvial = getCDVIAL();
        dlgPortal.cdmuni = txtCodMun.getText();

        dlgPortal.cdtpnum = getCDTNUM();
        dlgPortal.dscalnum = getDSCALNUM();
        dlgPortal.dsnumportal = getDS_NUM();

        // JRM: Lo a�ade porque faltaba.
        //para el cdpostal
        dlgPortal.prov = getCD_PROV();

        dlgPortal.lanza_busqueda();

        if (dlgPortal.listaPortal.size() > 0) {
          if (dlgPortal.listaPortal.size() > 1) {
            dlgPortal.show();
            listaPortalAux = dlgPortal.getListaDatosPortal();
          }
          else {
            listaPortalAux = dlgPortal.listaPortal;
          }
          if (listaPortalAux != null) {
            elPortal = (datasuca) listaPortalAux.firstElement();
            choNumeros.select(getIndiceDeClave(listaNumeros,
                                               (String) elPortal.get("DSTNUM"),
                                               "DSTNUM") + 1);
            txtPortal.setText( (String) elPortal.get("DSNMPORTAL"));
            choCalificador.select(getIndiceDeClave(listaCalificadores,
                (String) elPortal.get("DSDSCALNUM"), "DSDSCALNUM") + 1);
            txtCodPostal.setText( (String) elPortal.get("CDPOSTAL"));
            setCodigoPostalActualizado(true);
          }
        }
      }
      catch (Exception e) {
      }

      Inicializar();
      ponerModoContenedor();

    }

  }

  protected void txtFocusVia() {

    DialVialSuca dlgVial = null;
    datasuca elVial = null;
    CLista listaVialAux = null;

    txtVia.setText(txtVia.getText().trim());

    if (txtVia.getText().equals("")) {
      return;
    }

    if (!txtVia.getText().equals(strTxtViaAux)) {
      modoOperacion = modoESPERA;
      Inicializar();
      ponerEnEsperaContenedor();

      dlgVial = new DialVialSuca(app);
      dlgVial.cdtvia = getCDTVIA();
      dlgVial.cdmuni = txtCodMun.getText();
      dlgVial.dsvianorm = txtVia.getText();

      dlgVial.lanza_busqueda();

      if (dlgVial.listaVial.size() > 0) {
        if (dlgVial.listaVial.size() > 1) {
          // ARG: Si hay mas de 50 viales se muestra un mensaje
          if (dlgVial.bMas50Viales) {
            CMessage msgBox = new CMessage(app, CMessage.msgAVISO, strMasDe50);
            msgBox.show();
            msgBox = null;
          }

          dlgVial.show();
          listaVialAux = dlgVial.getListaDatosVial();
        }
        else {
          listaVialAux = dlgVial.listaVial;
        }
        if (listaVialAux != null) {
          elVial = (datasuca) listaVialAux.firstElement();
          choTrameros.select( (String) elVial.get("CDTVIA"));
          txtVia.setText( (String) elVial.get("DSVIAOFIC"));
          cdvial = (String) elVial.get("CDVIAL");
        }
      }

      // informa de que se ha escrito un vial
      if ( (txtVia.getText().length() > 0) &&
          (zs != null)) {
        zs.vialInformado();
      }

      Inicializar();
      ponerModoContenedor();

    }
  }

  protected void desactivaTpnumPortalCalifPostal(boolean bValor) {
    //deshabilito para que no se pueda hacer nada
    //hasta que se  pierda el foco en el vial y se compruebe
    choNumeros.setEnabled(bValor);
    txtPortal.setEnabled(bValor);
    btnPortal.setEnabled(bValor);
    choCalificador.setEnabled(bValor);
    txtCodPostal.setEnabled(bValor);
    btnCodPostal.setEnabled(bValor);
    //el cdpostal no lo deshabilito porque en la situacion
    //inicial, cuando solo se ha elegido el municipio,
    //esta habilitado
  }

  protected void txtValueChangeVia() {
    if (bHabilitado) {
      ctrlLimpiarTrameros();
      if (txtVia.getText().trim().equals("")) {
        cdvial = "";
      }
    }
  }

  protected void choPais_Click() {
    String nombrePais = choPaises.getSelectedItem();

    choCA.select(0);
    if (choProvincias.getItemCount() > 0) {
      choProvincias.removeAll();

    }
    if (nombrePais.equalsIgnoreCase("ESPA�A")) {
      bTramero = bModoTramero;
      modoOperacion = modoPAIS;
    }
    else {
      bTramero = false;
      modoOperacion = modoNOPAIS;
    }
    if (zs != null) {
      zs.cambiaModoTramero(bTramero);
    }
    ctrlVisibles();
    ctrlLimpiar();
  }

  protected void choCA_Click() {
    int indexCA = choCA.getSelectedIndex();

    ctrlLimpiar();

    if (indexCA == 0) {
      if (choProvincias.getItemCount() > 0) {
        choProvincias.removeAll();
      }
      modoOperacion = modoNOCA;
      Inicializar();
    }
    else {
      ponerEnEsperaContenedor();
      cargarProvincias(indexCA - 1);
      ponerModoContenedor();
    }

  }

  protected void choProvincia_Click() {
    if (choProvincias.getSelectedIndex() != 0) {
      modoOperacion = modoPROVINCIAS;
    }
    else {
      modoOperacion = modoNOPROVINCIAS;
    }
    ctrlLimpiar();
  }

  protected void choTramero_Click() {
    if (bHabilitado) {
      ctrlLimpiarTrameros();
    }
  }

  protected void btnCtrlbuscarMunicipio_actionPerformed() {
    String campo = null;
    ponerEnEsperaContenedor();
    CListaMunicipio listaMunicipio = new CListaMunicipio(
        app,
        "Selecci�n del municipio",
        stubCliente,
        strSERVLET_SUCA,
        srvsuca.servletMUN_OBTENER_X_CODIGO,
        srvsuca.servletMUN_OBTENER_X_DESCRIPCION,
        srvsuca.servletMUN_SELECCION_X_CODIGO,
        srvsuca.servletMUN_SELECCION_X_DESCRIPCION,
        CD_MUN, DS_MUN,
        bTramero ? "S" : "N"
        );

    // a�adimos la provincia para que filtre por provincias
    campo = getCD_PROV(); //getDatoLista(choProvincias, "CDPROV", listaProvincias, "");

    if (campo != null && (campo.trim().length() > 0)) {
      listaMunicipio.setParameter(CD_PROV, campo);
    }

    // A�adimos los dos par�metros restantes
    listaMunicipio.setParameter(CD_MUN, "");
    listaMunicipio.setParameter(DS_MUN, "");

    listaMunicipio.btnSearch_actionPerformed();
    listaMunicipio.show();
    datasuca dato = (datasuca) listaMunicipio.getComponente();
    listaMunicipio = null;
    if (dato != null) {
      txtDescMunicipio.setText( (String) dato.get(DS_MUN));
      txtCodMun.setText( (String) dato.get(CD_MUN));
      if (zs != null) {
        // System_out.println("@@@suca->panelsuca-> getCD_PROV() = " + getCD_PROV());
        zs.setZonificacionSanitaria(txtCodMun.getText(), getCD_PROV());
      }
      modoOperacion = modoMUNICIPIO;
      ctrlLimpiar();
    }
    ponerModoContenedor();
  }

  protected void txtValueChangePortal() {
    if (bHabilitado) {
      //strTxtPortalAux = txtPortal.getText();
    }
  }

  protected void txtValueChangeCodMun() {
    if (bHabilitado) {
      if ( (txtCodMun.getText().equals(""))) {
        ctrlLimpiar();

        modoOperacion = modoPROVINCIAS;
        txtDescMunicipio.setText("");
        strCodMunAux = "";
        ctrlActivados(false);
      }
    }
  }

  protected void txtFocusCodMun() {

    CLista data = new CLista();
    CLista listaMunicipio;

    txtCodMun.setText(txtCodMun.getText().trim());
    String campo = txtCodMun.getText();

    if (campo.equals("")) {
      return;
    }

    if (!campo.equals(strCodMunAux)) {
      Inicializar();
      ponerEnEsperaContenedor();

      datasuca datosMunicipio = new datasuca();
      datosMunicipio.put(CD_MUN, campo);
      datosMunicipio.put(CD_PROV, getCD_PROV());
      datosMunicipio.put("TRAMERO", bTramero ? "S" : "N");

      data.addElement(datosMunicipio);
      listaMunicipio = Common.traerDatos(app, stubCliente, strSERVLET_SUCA,
                                         srvsuca.servletMUN_OBTENER_X_CODIGO,
                                         data);

      if (listaMunicipio != null && listaMunicipio.size() > 0) {
        datasuca dato = (datasuca) listaMunicipio.firstElement();
        if (dato != null) {
          txtCodMun.setText( (String) dato.get(CD_MUN));
          txtDescMunicipio.setText( (String) dato.get(DS_MUN));
          if (zs != null) {
            // System_out.println("@@@suca->panelsuca-> getCD_PROV() = " + getCD_PROV());
            zs.setZonificacionSanitaria(txtCodMun.getText(), getCD_PROV());
          }
          modoOperacion = modoMUNICIPIO;
          ctrlLimpiar();
        }
        else {
          CMessage msgBox = new CMessage(app, CMessage.msgAVISO, "No hay datos");
          txtCodMun.setText("");
          msgBox.show();
          msgBox = null;
          modoOperacion = modoPROVINCIAS;
        }
      }
      else {
        CMessage msgBox = new CMessage(app, CMessage.msgAVISO, "No hay datos");
        txtCodMun.setText("");
        msgBox.show();
        msgBox = null;
        modoOperacion = modoPROVINCIAS;
      }

      Inicializar();
      ponerModoContenedor();
    }
  }

  protected void txtGotFocusPortal() {
    strTxtPortalAux = txtPortal.getText();
  }

  protected void txtGotFocusCodMun() {
    strCodMunAux = txtCodMun.getText();
  }

  protected void txtGotFocusTxtVia() {
    strTxtViaAux = txtVia.getText();
  }

  protected void txtFocusVia2() {
    // informa de que se ha escrito un vial2
    if ( (txt2Via.getText().length() > 0) &&
        (zs != null)) {
      zs.vialInformado();
    }
  }

  /* Permite que se resalten las cajas de texto que no cumplen cierte condicion
     Usada en validaci�n campos desde paquete enfermo*/
  public void hacerSelectAll(String clave) {

    if (clave.equals("DS_DIREC")) {
      if (bTramero) {
        txtVia.selectAll();
      }
      else {
        txt2Via.selectAll();
      }
    }

    else if (clave.equals("DS_NUM")) {
      if (bTramero) {
        txtPortal.selectAll();
      }
      else {
        txtNo.selectAll();
      }
    }

    else if (clave.equals("DS_PISO")) {
      if (!bTramero) {
        txtPiso.selectAll();
      }
    }
    else if (clave.equals("CD_POSTAL")) {
      if (bTramero) {
        txtCodPostal.selectAll();
      }
      else {
        txt2CodPostal.selectAll();
      }
    }

  } //Metodo

} //CLASE

//**********************************************************
//Clase encargada de gestionar los eventos sobre los botones
//**********************************************************

  class Panel_SucabtnActionListener
      implements ActionListener {
    panelsuca adaptee = null;
    ActionEvent e = null;

    public Panel_SucabtnActionListener(panelsuca adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void actionPerformed(ActionEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        //new Thread(this).start();
        adaptee.modoOperacion = adaptee.modoESPERA;
        adaptee.Inicializar();

        if (e.getActionCommand().equals("buscarMunicipio")) {
          adaptee.btnCtrlbuscarMunicipio_actionPerformed();
        }
        else if (e.getActionCommand().equals("buscarVia")) {
          adaptee.btnCtrlbuscarVial_actionPerformed();
        }
        else if (e.getActionCommand().equals("buscarPortal")) {
          adaptee.btnCtrlbuscarPortal_actionPerformed();
        }
        else if (e.getActionCommand().equals("buscarCodPostal")) {
          adaptee.btnCtrlbuscarCodPostal_actionPerformed();
        }

        adaptee.desbloquea();

      }
    }

    /*  // hilo de ejecuci�n para servir el evento
      public void run() {
        adaptee.modoOperacion = adaptee.modoESPERA;
        adaptee.Inicializar();
        if (e.getActionCommand().equals("buscarMunicipio")) {
          adaptee.btnCtrlbuscarMunicipio_actionPerformed();
        }
        else if (e.getActionCommand().equals("buscarVia")) {
          adaptee.btnCtrlbuscarVial_actionPerformed();
        }
        else if (e.getActionCommand().equals("buscarPortal")) {
          adaptee.btnCtrlbuscarPortal_actionPerformed();
        }
        else if (e.getActionCommand().equals("buscarCodPostal")) {
          adaptee.btnCtrlbuscarCodPostal_actionPerformed();
        }
        adaptee.desbloquea();
      }*/
  }

//******************************************
// p�rdida del foco de una caja de c�digo
//******************************************
  class Panel_SucaFocusAdapter
      extends java.awt.event.FocusAdapter {
    panelsuca adaptee;
    FocusEvent e = null;
    String nombreTxt = "";

    Panel_SucaFocusAdapter(panelsuca adaptee) {
      this.adaptee = adaptee;
    }

    public void focusLost(FocusEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        nombreTxt = ( (Component) e.getSource()).getName();
        //new Thread(this).start();
        if (nombreTxt.equals("txtCodMun")) {
          adaptee.txtFocusCodMun();
        }
        else if (nombreTxt.equals("txtVia")) {
          adaptee.txtFocusVia();
        }
        else if (nombreTxt.equals("txtPortal")) {
          adaptee.txtFocusPortal();
        }
        else if (nombreTxt.equals("txt2Via")) {
          adaptee.txtFocusVia2();
        }
        else if (nombreTxt.equals("txtCodPostal")) {
          //adaptee.txtFocusCodPostal();
        }
        adaptee.desbloquea();
      }
    }

    public void focusGained(FocusEvent e) {
      nombreTxt = ( (Component) e.getSource()).getName();
      if (nombreTxt.equals("txtCodMun")) {
        adaptee.txtGotFocusCodMun();
      }
      else if (nombreTxt.equals("txtVia")) {
        adaptee.txtGotFocusTxtVia();
      }
      else if (nombreTxt.equals("txtPortal")) {
        adaptee.txtGotFocusPortal();
      }
    }

    /*  public void run() {
        if (nombreTxt.equals("txtCodMun")){
          adaptee.txtFocusCodMun();
        }else if (nombreTxt.equals("txtVia")){
          adaptee.txtFocusVia();
        }else if (nombreTxt.equals("txtPortal")){
          adaptee.txtFocusPortal();
        }else if (nombreTxt.equals("txt2Via")){
          adaptee.txtFocusVia2();
        } else if (nombreTxt.equals("txtCodPostal")) {
          //adaptee.txtFocusCodPostal();
        }
        adaptee.desbloquea();
      }*/
  }

//***********************************
// Cambios en las cajas de texto
//***********************************
  class Panel_SucaTextListener
      implements java.awt.event.TextListener {
    panelsuca adaptee;
    TextEvent e = null;
    String nombreTxt = "";

    public Panel_SucaTextListener(panelsuca adaptee) {
      this.adaptee = adaptee;
    }

    public void textValueChanged(java.awt.event.TextEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        //nombreTxt = ((Component)e.getSource()).getName();
        //new Thread(this).start();
        nombreTxt = ( (Component) e.getSource()).getName();
        if (nombreTxt.equals("txtCodMun")) {
          adaptee.txtValueChangeCodMun();
          adaptee.desbloquea();
        }
        else if (nombreTxt.equals("txtVia")) {
          adaptee.txtValueChangeVia();
          adaptee.desbloquea();
          adaptee.desactivaTpnumPortalCalifPostal(false);
        }
        else if (nombreTxt.equals("txtPortal")) {
          adaptee.txtValueChangePortal();
          adaptee.desbloquea();
        }
      }
    }

    /*  public void run () {
        nombreTxt = ((Component)e.getSource()).getName();
        if (nombreTxt.equals("txtCodMun")){
          adaptee.txtValueChangeCodMun();
          adaptee.desbloquea();
        } else if (nombreTxt.equals("txtVia")){
          adaptee.txtValueChangeVia();
          adaptee.desbloquea();
          adaptee.desactivaTpnumPortalCalifPostal(false);
        } else if (nombreTxt.equals("txtPortal")){
          adaptee.txtValueChangePortal();
          adaptee.desbloquea();
        }
      }  */

  }

//***********************************
//Eventos en las listas desplegables
//***********************************
  class Panel_SucaChoiceItemListener
      implements java.awt.event.ItemListener, Runnable {
    panelsuca adaptee;
    ItemEvent e = null;

    Panel_SucaChoiceItemListener(panelsuca adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void itemStateChanged(ItemEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        new Thread(this).start();

        //     String nombreChoice = ((Component)e.getSource()).getName();

        /*    adaptee.modoOperacion = adaptee.modoESPERA;
            adaptee.Inicializar();
            if (nombreChoice.equals("choPais")) {
              adaptee.choPais_Click();
            } else if (nombreChoice.equals("choCA")) {
              adaptee.choCA_Click();
            } else if (nombreChoice.equals("choProvincia")) {
              adaptee.choProvincia_Click();
            } else if (nombreChoice.equals("choTramero")) {
              adaptee.choTramero_Click();
            }
            adaptee.desbloquea();*/
      }
    }

    // hilo de ejecuci�n para servir el evento
    public void run() {
      String nombreChoice = ( (Component) e.getSource()).getName();

      adaptee.modoOperacion = adaptee.modoESPERA;
      adaptee.Inicializar();

      if (nombreChoice.equals("choPais")) {
        adaptee.choPais_Click();
      }
      else if (nombreChoice.equals("choCA")) {
        adaptee.choCA_Click();
      }
      else if (nombreChoice.equals("choProvincia")) {
        adaptee.choProvincia_Click();
      }
      else if (nombreChoice.equals("choTramero")) {
        adaptee.choTramero_Click();
      }
      adaptee.desbloquea();
    }
  }

///************************************

 class CListaMunicipio
     extends CListaValores {
   /** componente que se va a enviar */
   datasuca hash = null;

   // Variables
   String codigo = null;
   String descripcion = null;

   public CListaMunicipio(CApp a,
                          String title,
                          StubSrvBD stub,
                          String servlet,
                          int obtener_x_codigo,
                          int obtener_x_descripcion,
                          int seleccion_x_codigo,
                          int seleccion_x_descripcion,
                          String a_codigo,
                          String a_descripcion,
                          String a_tramero
                          ) {
     super(a,
           title,
           stub,
           servlet,
           obtener_x_codigo,
           obtener_x_descripcion,
           seleccion_x_codigo,
           seleccion_x_descripcion);

     hash = new datasuca();
     codigo = a_codigo;
     descripcion = a_descripcion;

     hash.put("TRAMERO", a_tramero);

   }

   public Object setComponente(String filtro) {
     if (filtro != null) {
       hash.put(panelsuca.CD_MUN, filtro);
       hash.put(panelsuca.DS_MUN, filtro);
     }
     return hash;
   }

   public String getCodigo(Object o) {
     return (String) ( (datasuca) o).get(codigo);
   }

   public String getDescripcion(Object o) {
     return (String) ( (datasuca) o).get(descripcion);
   }

   /**
    *  a�ade un parametro a la lista
    *  por si el servlet necesita m�s de un parametro
    */

   public void setParameter(String campo, String dato) {
     hash.put(campo, dato);
   }

 } //____________________________________ END_CLASE}
