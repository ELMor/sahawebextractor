//Title:        Grafico Comparativo Historico
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina
//Company:      Norsistemas S.A.
//Description:

package grafcomphist;

import java.io.Serializable;

public class dataConsGrafHis
    implements Serializable {

  public String ano = "";
  public String sem = "";

  public String cd_enf = "";
  public String ds_enf = "";
  public int casos = 0;
  public float umbral_inf = (float) 0.0;
  public float umbral_sup = (float) 0.0;
  public float media = (float) 0.0;
  public float razon = (float) 0.0;

  public dataConsGrafHis(String a, String s, String cd, int c) {
    ano = a;
    sem = s;
    cd_enf = cd;
    casos = c;
  }

  public dataConsGrafHis() {
  }

}
