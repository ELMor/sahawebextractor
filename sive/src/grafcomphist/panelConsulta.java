//Title:        Grafico Comparativo Historico
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina
//Company:      Norsistemas S.A.
//Description:

package grafcomphist;

import java.net.URL;
import java.util.Vector;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaADZE;
import capp.CMessage;
import capp.CPanel;
import eapp.DataGraf;
import eapp.GPanel;
import eapp.PanelChart;
import sapp.StubSrvBD;
import utilidades.PanFechas;

public class panelConsulta
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/grafico.gif"};

  protected CCargadorImagen imgs = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  protected int modoOperacion = modoNORMAL;

  protected panelGrafico grafico = null;
  protected parConsGrafHis parCons = null;

  // stub's
  final String strSERVLETGrafHis = "servlet/SrvGrafCompHist";

  //Modos de operaci�n del Servlet
  final int GRAFICO_HISTORICO = 0;

  // modos de la lista
  final int AREA = 12;
  final int DISTRITO = 13;
  final int ZBS = 14;
  final int ENFERMEDAD = 15;
  //AIC
  final int ENFERMEDADTODAS = 16;

  //maximo de enfermedades
  final int COL_ENFERMEDAD = 20;

  XYLayout xYLayout = new XYLayout();

  PanFechas panFechas;

  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  actionListener btnActionListener = new actionListener(this);

  public panelConsulta(CApp a) {
    try {

      setApp(a);
      grafico = new panelGrafico();
      panFechas = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, false);
      jbInit();
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(538);
    btnLimpiar.setLabel("Limpiar");
    btnInforme.setLabel("Obtener");

    // gestores de eventos
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    xYLayout.setWidth(596);
    this.setLayout(xYLayout);

    this.add(panFechas, new XYConstraints(25, 25, -1, -1));
    this.add(btnLimpiar, new XYConstraints(390, 250, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 250, -1, -1));

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnLimpiar.setImage(imgs.getImage(1));
    btnInforme.setImage(imgs.getImage(2));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (panFechas.txtAno.getText().length() == 0) ||
        (panFechas.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    panFechas.txtAno.setText("");
    panFechas.txtCodSem.setText("");
    panFechas.txtFecSem.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    parCons = new parConsGrafHis();
    CLista listaParametros = new CLista();
    CLista listaResultados = new CLista();

    if (isDataValid()) {
      parCons.anoH = panFechas.txtAno.getText();
      if (panFechas.txtCodSem.getText().length() == 1) {
        parCons.semH = "0" + panFechas.txtCodSem.getText();
      }
      else {
        parCons.semH = panFechas.txtCodSem.getText();

      }
      parCons.ultEnf = "";

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();

      // Aparece un dialogo con una lista para que seleccione
      // las enfermedades que se van a mostrar en el grafico
      CListaADZE diaElegir = null;
      //diaElegir = new CListaADZE(app,ENFERMEDAD,COL_ENFERMEDAD,new CLista());
      diaElegir = new CListaADZE(app, ENFERMEDADTODAS, COL_ENFERMEDAD,
                                 new CLista());
      diaElegir.show();

      if (diaElegir.resultado()) {
        parCons.lista = diaElegir.listaRes();

        listaParametros = new CLista();

        listaParametros.setIdioma(getApp().getIdioma());
        listaParametros.setPerfil(getApp().getPerfil());
        listaParametros.setLogin(getApp().getLogin());

        listaParametros.addElement(parCons);

        try {
          stubCliente.setUrl(new URL(app.getURL() + strSERVLETGrafHis));

          listaResultados = (CLista) stubCliente.doPost(GRAFICO_HISTORICO,
              listaParametros);

          /*
                     SrvGrafCompHist servlet = new SrvGrafCompHist();
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                                 "dba_edo",
                                 "manager");
               listaResultados = (CLista) servlet.doDebug(GRAFICO_HISTORICO, listaParametros);
           */

          if (listaResultados != null) {
            pintaGrafico( (Vector) listaResultados.firstElement(),
                         (parConsGrafHis) listaResultados.elementAt(1));
          }
          else {
            msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                  "No hay datos que visualizar.");
            msgBox.show();
            msgBox = null;
          }
        }
        catch (Exception e) {
          e.printStackTrace();
          // System_out.println(e.getMessage());
        }
      }
      //informe.mostrar();
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            "Faltan campos obligatorios.");
      msgBox.show();
      msgBox = null;
    }
  }

  // se pinta el gr�fico
  private void pintaGrafico(Vector puntos, parConsGrafHis datos) {

    String strCriterio1 = null;
    String strCriterio2 = null;
    String strCriterio3 = null;

    double rawData[][] = null;

    String series_Y[] = null; //{"1", "2","3","4","5","6","7","8","9","10"};
    String series_X[] = {
        "Sobrepasa l�mites hist�ricos", "Raz�n"};

    series_Y = new String[puntos.size()];
    rawData = new double[3][puntos.size()];

    float razon = (float) 0.0;
    float inf = (float) 0.0;
    float sup = (float) 0.0;

    /*
         double rawData[][] = {
      { 1,   2,   3,   4,   5,   6,   7,    8,    9,    10 },
         { 0.001, 0.01, 0.1, 1.0, 10.0, 100.0 , 1000.0, 10000.0, 100000.0, 1000000.0 },
       { 0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 10.0, 5.0 }
         };
     */

    for (int i = 0; i < puntos.size(); i++) {
      series_Y[i] = ( (dataConsGrafHis) puntos.elementAt(i)).ds_enf + " " +
          ( (dataConsGrafHis) puntos.elementAt(i)).casos;
      rawData[0][i] = i + 1;

      //atencion a los umbrales

      razon = ( (dataConsGrafHis) puntos.elementAt(i)).razon;
      inf = ( (dataConsGrafHis) puntos.elementAt(i)).umbral_inf;
      sup = ( (dataConsGrafHis) puntos.elementAt(i)).umbral_sup;
      if (razon == (float) 0.0) {
        razon = (float) 0.001;

        // en rawData[1][i] valor mas alto
        // en rawData[2][i] valor mas bajo

      }
      if (inf <= 0) {
        rawData[2][i] = razon; // 0.001;//razon;
        rawData[1][i] = razon; //razon;//0.001;
      }
      else if (razon > inf) {
        rawData[2][i] = inf; //razon-inf; //inf;
        rawData[1][i] = razon; //inf; //razon;
      }
      else {
        rawData[2][i] = razon; // 0.001;//razon;
        rawData[1][i] = razon; //razon;//0.001;
      }

      if (razon > sup) {
        rawData[2][i] = sup; //razon-sup;//sup;
        rawData[1][i] = razon; //sup;//razon;
      }
      else {
        rawData[2][i] = razon; // 0.001;//razon;
        rawData[1][i] = razon; //razon;//0.001;
      }

      //rawData[1][i] = ((dataConsGrafHis) puntos.elementAt(i)).razon;
      //rawData[2][i] = ((dataConsGrafHis) puntos.elementAt(i)).razon + (double) 1;
    }

    DataGraf data = new DataGraf("Leyenda", rawData, series_X, series_Y);

    GPanel panel = new GPanel(this.app, PanelChart.BARRAS_INVERTIDAS_LOG);
    panel.chart.setDatosLabels(data, series_Y);

    //strCriterio1 = new String(" S.N.E.D.O. desde semana " + datos.semD
    // modificacion jlt 24/10/01 quitamos el t�tulo de S.N.E.D.O.
    strCriterio1 = new String(" Desde semana " + datos.semD
                              + " a�o " + datos.anoD + ", hasta semana " +
                              datos.semH
                              + " a�o " + datos.anoH + ".");

    strCriterio2 = new String(
        " Raz�n de casos actuales con los hist�ricos de los �ltimos cinco a�os.");

    strCriterio3 = new String();

    panel.chart.setCriterios(strCriterio3, strCriterio2, strCriterio1);

    panel.chart.setTitulo("GR�FICO COMPARATIVO HIST�RICO ");
    panel.chart.setTituloEje("", "Raz�n");

    //panel.chart.setOrigenY((double) 1);
    //panel.chart.chart.getChartArea().getYAxis(0).setOriginIsDefault(true);
    //panel.chart.chart.getChartArea().getYAxis(0).setMinIsDefault(true);
    //panel.chart.chart.getChartArea().getYAxis(0).setMaxIsDefault(true);

    //pruebas
    // ARG: Esto provocaba un error al compilar
    //panel.chart.chart.getChartArea().getXAxis(0).setPlacementIsDefault(true);
    //panel.chart.chart.getChartArea().getYAxis(0).setPlacementIsDefault(true);

    panel.show();
  }

} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  panelConsulta adaptee = null;
  ActionEvent e = null;

  public actionListener(panelConsulta adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}
////////////////////// Clases para listas
