package eapp2;

import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Dimension;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp2.CApp;
import capp2.CDialog;

public abstract class DiaGeneralInforme
    extends CDialog {

  //Panel con mensajes al usuario
  ControlPanel ctrlPanel;

  //Booleano indica si en esta consulta hay tramado (siempreTodo= false)
  //o se envia siempre informe completo (siempreTodo= true)
  public boolean siempreTodo = true;

  //Strings que van al di�logo
  protected String sREGISTROS_TOTALES = "";
  protected String sREGISTROS_MOSTRADOS = "";
  protected String sCONSULTANDO = "";

  // report
  protected ERW erw = null;
  protected ERWClient erwClient = null;
  protected AppDataHandler dataHandler = null;

  // plantilla
  protected TemplateManager tm = null;

  //____________________________________________________

  //Di�logo de informe recibe:
  //boolean conTodoAlavez : true si no se trama
  public DiaGeneralInforme(CApp a, boolean conTodoALaVez) {
    super(a);
    try {
      siempreTodo = conTodoALaVez;
      sREGISTROS_TOTALES = "Registros totales:";
      sREGISTROS_MOSTRADOS = "Registros mostrados:";
      sCONSULTANDO = "Consultando ...";
      ctrlPanel = new ControlPanel(this);
      jbInit();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(800, 550));
    this.add(ctrlPanel, BorderLayout.NORTH);
  }

  // METODOS PARA MANEJO DEL ENTERPRISESOFT____________________________
  //Normalmente no se sobreescibir�n.
  //Simplemente se llaman desde clases hijas

  protected void inicializaReport(CApp app, String rutaFicheroERW) throws
      Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + rutaFicheroERW);

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // plantilla
    tm = erw.GetTemplateManager();

  }

  //Pone texto a una etiqueta
  protected void setTexto(String identEtiqueta, String texto) {
    tm.SetLabel(identEtiqueta, texto);
  }

  //Pone imagen a una etiqueta
  protected void setImagen(String identEtiqueta, String imagen) {
    tm.SetImageURL(identEtiqueta, imagen);

  }

  //Pasa al ERW el vector con los datos vDatos ,l vector vEstructura y el nombre de la tabla de datos
  //El vector vEstructura  establece la relaci�n entre los atributos de los elementos de vDatos
  //y las colmunas de la tabla nombreTabla, ya dada de alta en la b.datos y el informe
  public void registraTabla(Vector vDatos, String nombreTabla,
                            Vector vEstructura) throws Exception {
    dataHandler.RegisterTable(vDatos, nombreTabla, vEstructura, null);
    erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));
  }

  //Metodo equivalente al registraTabla, pero que al final no inicializa propiedades
  //Se usa cuando hay que registra m�s de una tabla (para regitrar todas las tablas salvo la �ltima)
  public void registraUnaTabla(Vector vDatos, String nombreTabla,
                               Vector vEstructura) throws Exception {
    dataHandler.RegisterTable(vDatos, nombreTabla, vEstructura, null);
  }

  // repintado
  public void refrescaReport() {
    erwClient.refreshReport(true);
  }

  //Oculta una seccion y todos los grupos que tenga
  public void ocultaSeccion(String idSec) {
    //Hace invisible lineas de deatalle de seccion idSec
    tm.SetVisible(tm.GetDetailZone(idSec), false);
    //Hace invisible cabeceras y pies de grupos de seccion idSec
    Object[][] grupos = tm.GetGroups(idSec);
    for (int i = 0; i < grupos.length; i++) {
      for (int j = 0; j < grupos[i].length; j++) {
        //Hace invisible cab. y pie
        tm.SetVisible(grupos[i][j], false);
      }
    }
  }

  //Oculta cabecera de un grupo (pertenece a una seccion)
  //Hace invisible cabecera de grupo numGrupo perteneciente a seccion idSec
  public void ocultaCabGrupo(String idSec, int numGrupo) {
    Object[][] grupos = tm.GetGroups(idSec);
    tm.SetVisible(grupos[numGrupo][0], false);
  }

  //Oculta pie de un grupo (pertenece a una seccion)
  //Hace invisible pie de grupo numGrupo perteneciente a seccion idSec
  public void ocultaPieGrupo(String idSec, int numGrupo) {
    Object[][] grupos = tm.GetGroups(idSec);
    tm.SetVisible(grupos[numGrupo][1], false);
  }

  // METODOS PARA GENERACION INFORME Y GRAFICA____________________________

  // M�tod GenerarInforme.
  //Comienza la generaci�n del informe (envi� todo o una trama dependiendo de variable siempreTodo)
  public boolean GenerarInforme() {
    if (siempreTodo) {
      return LlamadaInforme(true);
    }
    else {
      return LlamadaInforme(false);
    }
  }

  //M�todo que a�ade datos al informe.
  //Si par�metro est� a true deber� a�adir todos los datos (informe completo)
  //Si par�metro est� a false a�ade una trama
  protected abstract boolean LlamadaInforme(boolean conTodos);

  //M�todo para recoger siguiente trama si se est� tramando
  //Si no se trama, no se llamar� nunca
  //Si se trama, se deber� sobreescribir en clases hijas
  public void MasDatos() {}

  // mostrar grafica
  //Si hay que mostrar graf., se deber� sobreescribir en clases hijas
  public void MostrarGrafica() {}

  // METODOS PARA MOSTRAR MENSAJES ____________________________

  //Indica la usuario el total de registros
  public void setTotalRegistros(String t) {
    ctrlPanel.setStatus1(sREGISTROS_TOTALES + t);
  }

  //Indica la usuario el total de registros mostrados (solo si se trama)
  public void setRegistrosMostrados(String t) {
    if (!siempreTodo) {
      ctrlPanel.setStatus2(sREGISTROS_MOSTRADOS + t);
    }
    else {
      ctrlPanel.setStatus2("");
    }
  }

  public void setLimpiarStatus2() {
    ctrlPanel.setStatus2("");
  }

  public void setLimpiarStatus() {
    ctrlPanel.setStatus1("");
    ctrlPanel.setStatus2("");
  }

  //Indica al usuario que se est� consultando la B.Datos
  public void setGenerandoInforme() {
    ctrlPanel.setStatus1("");
    ctrlPanel.setStatus2(sCONSULTANDO);
  }
}
