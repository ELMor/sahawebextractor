package eapp2;

import java.net.URL;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CImage;
import capp2.CApp;
import jclass.bwt.BWTEnum;
import jclass.chart.ChartDataView;
import jclass.chart.JCAxis;
import jclass.chart.JCAxisTitle;
import jclass.chart.JCChart;

public class PanelChart
    extends Panel {

  final String strLOGO_CCAA = "images/ccaa.gif";

  // tipos de gr�ficas
  final static public int CURVA_EPIDEMICA = 0;
  final static public int BARRAS = 1;
  final static public int BARRAS_INVERTIDAS = 2;
  final static public int BARRAS_INVERTIDAS_LOG = 3;

  XYLayout xYLayout = new XYLayout();
  jclass.bwt.JCSeparator linea1 = new jclass.bwt.JCSeparator();
  Label lblTitulo1 = new Label();
  Label lblTitulo2 = new Label();
  Label lblTitulo3 = new Label();
  Label lblFecha = new Label();
  CImage image = null;
  Label lblCriterio1 = new Label();
  Label lblCriterio2 = new Label();
  Label lblCriterio3 = new Label();
  public jclass.chart.JCChart chart = new jclass.chart.JCChart();
  CApp app;
  protected int iTipo;

  public PanelChart(CApp a, int tipo) {
    try {
      iTipo = tipo;
      app = a;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setLayout(xYLayout);
    this.setSize(new Dimension(800, 524));
    lblTitulo1.setFont(new Font("Dialog", 1, 14));
    lblTitulo1.setText(
        "SISTEMA DE INFORMACI�N PARA LA PREVENCI�N Y PROTECCI�N DE LA SALUD");
    lblTitulo2.setFont(new Font("Dialog", 1, 14));
    lblTitulo2.setText("GESTI�N DE VACUNAS");
    lblFecha.setFont(new Font("Dialog", 1, 12));
    lblFecha.setText("Fecha y hora:");
    this.add(linea1, new XYConstraints(5, 75, 790, -1));
    this.add(lblTitulo1, new XYConstraints(87, 8, 430, 20));
    this.add(lblTitulo2, new XYConstraints(87, 23, 362, 21));
    this.add(lblTitulo3, new XYConstraints(87, 49, 610, 22));
    this.add(lblFecha, new XYConstraints(652, 5, 115, 16));
    // fecha y hora
    java.util.Date dFecha_Actual = new java.util.Date();
    java.text.SimpleDateFormat Format = new java.text.SimpleDateFormat(
        "dd/MM/yyyy HH:mm:ss");
    String sFecha_Actual = Format.format(dFecha_Actual);
    lblFecha.setText(sFecha_Actual);

    // logo de la comunidad aut�noma
    image = new CImage(new URL(app.getCodeBase(), strLOGO_CCAA), 67, 67);
    this.add(image, new XYConstraints(5, 5, 67, 67));
    this.add(lblCriterio1, new XYConstraints(13, 82, 771, -1));
    this.add(lblCriterio2, new XYConstraints(13, 105, 771, -1));
    this.add(lblCriterio3, new XYConstraints(13, 127, 771, 27));
    this.add(chart, new XYConstraints(12, 159, 776, 338));
    this.setBackground(Color.white);

    // configuraci�n de la gr�fica
    chart.setAllowUserChanges(false);
    chart.setBorderType(BWTEnum.SHADOW_ETCHED_IN);
    chart.setBorderWidth(2);
    chart.setBorderWidth(2);

    //prepara el tipo inicial del Control
    switch (iTipo) {
      case PanelChart.CURVA_EPIDEMICA:
        setTipo(JCChart.PLOT);
        break;
      case PanelChart.BARRAS:
        setTipo(JCChart.BAR);
        break;
      case PanelChart.BARRAS_INVERTIDAS:
        setTipo(JCChart.BAR);
        setInvertido(true);
        break;
      case PanelChart.BARRAS_INVERTIDAS_LOG:

        //setTipo(JCChart.STACKING_BAR);
        setTipo(JCChart.BAR);
        setInvertido(true);
        setLogaritmicoY(true);
        //setOrigen(1);
        setOrigenY(1);
        setOverLap(100);

        break;
    }
  }

  public int getTipo() {
    return iTipo;
  }

  public void setTipo(int t) {
    chart.getDataView(0).setChartType(t);
  }

  public void setDatosLabels(DataGraf data, String[] labels) {

    chart.getDataView(0).setDataSource(data);

    chart.getChartArea().getXAxis(0).setPlacement(JCAxis.ORIGIN);
    chart.getChartArea().getYAxis(0).setPlacement(JCAxis.ORIGIN);

    chart.getChartArea().getXAxis(0).setAnnotationMethod(JCAxis.POINT_LABELS);

    ChartDataView cd = chart.getDataView(0);

    for (int i = 0; i < labels.length; i++) {
      cd.setPointLabel(i, labels[i]);
    }

  }

  public void setDatos(DataGraf data) {
    chart.getChartArea().getXAxis(0).setAnnotationMethod(JCAxis.VALUE);

    chart.getDataView(0).setDataSource(data);
    chart.getChartArea().getXAxis(0).setPlacement(JCAxis.ORIGIN);
    chart.getChartArea().getYAxis(0).setPlacement(JCAxis.ORIGIN);
  }

  public void setInvertido(boolean b) {
    chart.getDataView(0).setIsInverted(b);
  }

  public void setLeyenda(boolean b) {
    chart.getLegend().setIsShowing(b);
  }

  public void setGrids(boolean b) {
    chart.getChartArea().getXAxis(0).setGridIsShowing(b);
    chart.getChartArea().getYAxis(0).setGridIsShowing(b);
  }

  public void setLogaritmico(boolean b) {
    chart.getChartArea().getXAxis(0).setIsLogarithmic(b);
  }

  public void setOrigen(double o) {
    chart.getChartArea().getXAxis(0).setOrigin(o);
  }

  public void setLogaritmicoY(boolean b) {
    chart.getChartArea().getYAxis(0).setIsLogarithmic(b);
  }

  public void setOrigenY(double o) {
    chart.getChartArea().getYAxis(0).setOrigin(o);
  }

  // graba el t�tulo
  public void setTitulo(String t) {
    if (t == null) {
      t = new String("");
    }
    lblTitulo3.setText(t);
  }

  // graba los criterios
  public void setCriterios(String c1, String c2, String c3) {
    if (c1 == null) {
      c1 = new String("");
    }
    if (c2 == null) {
      c2 = new String("");
    }
    if (c3 == null) {
      c3 = new String("");
    }
    lblCriterio3.setText(c1);
    lblCriterio2.setText(c2);
    lblCriterio1.setText(c3);
  }

  // titulos
  public void setTituloEje(String tx, String ty) {
    if (tx == null) {
      tx = new String("");
    }
    if (ty == null) {
      ty = new String("");
    }
    chart.getChartArea().getXAxis(0).setTitle(new JCAxisTitle(tx));
    chart.getChartArea().getYAxis(0).setTitle(new JCAxisTitle(ty));
  }

  // 3D
  public void set3D(boolean b) {
    if (b) {
      chart.getChartArea().setDepth(10);
    }
    else {
      chart.getChartArea().setDepth(0);
    }
  }

  public void setOverLap(int i) {
    chart.getDataView(0).getBarChartFormat().setClusterOverlap(i);

  }

}
