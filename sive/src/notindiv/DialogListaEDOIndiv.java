// JRM: Hace obligatorio zona de riesgo y provincia y municipio en m�todo
// isDataValid()
package notindiv;

// modificacion para debug
import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import capp.CTabPanel;
import cartas.Mail;
import enfermo.comun;
import infedoind.DialInfEdoInd;
// JRM: Que no enga�e, se importa simplemente para hacer una consulta sencilla.
import informes.DataInforme;
import infprotoedo.DataProtocolo;
import infprotoedo.PanelEDO;
import notalarmas.miraIndicadores;
import notalarmas.parNotifAlar;
import notdata.DataBorrarIndiv;
import notdata.DataEnfCaso;
import notdata.DataEnfermoBD;
import notdata.DataIndivEnfermedad;
import notdata.DataListaEDOIndiv;
import notdata.DataLongValid;
import notdata.DataOpeFc;
import notdata.DataTab;
import notutil.Comunicador;
import notutil.UtilEDO;
import sapp.StubSrvBD;

public class DialogListaEDOIndiv
    extends CDialog {

  //Ya hubo msg en bajas
  boolean bYaHuboMsg = false;
  ResourceBundle res;

  //Ya hubo msg en protocolo
  public boolean YaSalioMsgEnferSinProtocolo = false;

  //lista Global para control bloqueo
  public CLista listaOpeUltactIndiv = new CLista();

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/grafico.gif"};
  //un modo de panelmaestroedo
  final int modoCABECERAYBOTONES = 15;

  // modos de operaci�n del dialog

  // modoESPERA: controles deshabilitados y cursor en espera hasta
  //              que se finaliza una transacci�n
  // modoALTA: modo para dar de alta una EDO Individualizada
  //          (controles habilitados para introducci�n de datos).
  // modoMODIFICACION: modo para modificar una EDO Individualizada
  //          (controles que se permiten modificar, habilitados).
  // modoBAJA: modo para dar de baja una EDO Individualizada
  //          (todos los controles deshabilitados, s�lo se
  //           permite aceptar / cancelar).
  // modoCONSULTA: modo para la consulta de una EDO Individualizada
  //          (todos los controles deshabilitados, s�lo se permite
  //          aceptar???)
  // modoSOLODECLARANTES: modo por el que s�lo se permite modificar
  //          los declarantes (restricciones).
  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;
  final int modoSOLODECLARANTES = 7;

  // modos de operaci�n del servlet
  final int servletOBTENER_DETALLE_CABECERA = 8;
  final int servletOBTENER_DATOS_EDOIND = 22;
  final int servletSELECT_EDO_INVIDIDUALIZADA = 0;
  final int servletSELECT_EDO_INVIDIDUALIZADA_SUCA = 1;

  // modos de operaci�n del servlet SrvIndivIM
  final int servletSIN_CD_ENFERMO_INSERT_INSERT_INSERT = 0;
  final int servletCON_CD_ENFERMO_SIN_NM_EDO_NADA_INSERT_INSERT = 1;
  final int servletCON_CD_ENFERMO_CON_NM_EDO_NADA_MODIF_BUCLE = 2;
  final int servletCON_CD_ENFERMO_SIN_NM_EDO_MODIF_INSERT_INSERT = 3;
  final int servletCON_CD_ENFERMO_CON_NM_EDO_MODIF_MODIF_BUCLE = 4;

  // modos de operaci�n del servleet SrvIndivB
  final int servletBORRAR_INDIVIDUALIZADA = 0;
  // modos de operaci�n del servlet SrvBloqueo
  final int servletBLOQUEO_SELECT = 0;
  // constantes del dialog
  final String strSERVLET_BLOQUEO = "servlet/SrvBloqueo";
  final String strSERVLET_EDOIND = "servlet/SrvEDOIndiv";
  final String strSERVLET_INDIV_SELECT = "servlet/SrvIndivSelect";
  final String strSERVLET_INDIV_IM = "servlet/SrvIndivIM";
  final String strSERVLET_INDIV_B = "servlet/SrvIndivB";

  // par�metros
  CMessage msgBox = null;
  CLista lista;
  CLista parametros = null, result = null;
  StubSrvBD stubCliente;
  public int iOut = -1; // Modo de salida del dialog (Aceptar=0, Cancelar=-1)

  String sExpediente = "";
  PanelEDOIndivCabecera pCabecera = null;
  PanelEDOIndivCaso pCaso = null;
  PanelEDOIndivDeclarantes pDeclarantes = null;
  public PanelEDO pProtocolo = null;
  CApp capp;
  DataProtocolo dataProtocol;
  // modo de operaci�n del dialog
  public int modoOperacion = modoALTA;
  public int modoOperacionBk = modoALTA;

  public boolean bModificacionArtificial = false; //desde alta****//mlm creo no sirve

  // Hastable para almacenar los datos de las solapas
  protected Hashtable hashIndiv = null; //mlm creo no sirve
  // Hastable NotifEDO
  protected Hashtable hashNotifEDO = null;
  // flag que se pone a true una vez aceptados los datos de las
  // 3 primeras solapas
  protected boolean bCabeceraAceptada = false; //mlm creo no sirve

  // datos con las listas
  protected CLista listaPanelCabecera = null;
  protected CLista listaPanelCaso = null;

  // controles
  XYLayout xYLayout = new XYLayout();
  XYLayout layoutBotones = new XYLayout();
  ButtonControl btnListado = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  Panel panelBotones = new Panel();
  StatusBar barra = new StatusBar();

  //TabsetPanel tabsetPanel1 = new TabsetPanel();
  public CTabPanel tabPanel = new CTabPanel();

  // listeners
  DialogListaEDOIndivactionAdapter actionAdapter = new
      DialogListaEDOIndivactionAdapter(this);
//  Scrollbar scrollbar1 = new Scrollbar();

  // constructor
  public DialogListaEDOIndiv(CApp a, String title, StubSrvBD stub,
                             String servlet, int modo,
                             DataListaEDOIndiv data,
                             Hashtable hashNotifEDO) {
    super(a);
    try {
      capp = a;
      res = ResourceBundle.getBundle("notindiv.Res" + a.getIdioma());
      stubCliente = stub;
      this.hashNotifEDO = hashNotifEDO;
      modoOperacion = modo;
      modoOperacionBk = modo;
      YaSalioMsgEnferSinProtocolo = false;
      jbInit();
      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));
    btnListado.setImage(imgs.getImage(2));
    // t�tulo
    this.setTitle(res.getString("DialogListaEDOIndiv.Title"));

    // layout - controles
    xYLayout.setHeight(450);
    xYLayout.setWidth(780);
    setSize(780, 450);
    setLayout(xYLayout);
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnListado.setLabel(res.getString("btnListado.Label"));

    layoutBotones.setHeight(44);
    layoutBotones.setWidth(755);
    barra.setBevelOuter(BevelPanel.LOWERED);
    barra.setBevelInner(BevelPanel.LOWERED);
    panelBotones.setLayout(layoutBotones);

    /*
        panelBotones.add(barra,new XYConstraints(5, 10, 440, -1));
        panelBotones.add(btnListado, new XYConstraints(455, 10, 90, -1));
        panelBotones.add(btnAceptar, new XYConstraints(555, 10, 90, -1));
        panelBotones.add(btnCancelar, new XYConstraints(655, 10, 90, -1));
     */
    panelBotones.add(barra, new XYConstraints(5, 10, 460, 26));
    panelBotones.add(btnListado, new XYConstraints(470, 10, 85, 26));
    panelBotones.add(btnAceptar, new XYConstraints(565, 10, 85, 26));
    panelBotones.add(btnCancelar, new XYConstraints(660, 10, 85, 26));

    add(panelBotones, new XYConstraints(15, 406, 755, 44));
    //cris
    //add(btnAceptar, new XYConstraints(200, 415, 100, -1));
    //add(btnCancelar, new XYConstraints(450, 415, 100, -1));
    add(tabPanel, new XYConstraints(10, 15, 755, 390));
//    pCabecera.add(scrollbar1, null);

    //btnAceptar.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
    //btnCancelar.setImageURL(new URL(app.getCodeBase(), imgCANCELAR));

    btnListado.setActionCommand("GenListado");
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    btnListado.addActionListener(actionAdapter);
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    hashIndiv = new Hashtable();

    pDeclarantes = new PanelEDOIndivDeclarantes(this, modoOperacion,
                                                hashNotifEDO);
    pCabecera = new PanelEDOIndivCabecera(this, modoOperacion, hashNotifEDO);
    tabPanel.addMouseListener(new DialogListaEDOIndiv_tabPanel_mouseAdapter(this));

    // orden definitivo
    // (el panel del caso no se a�ade en modo alta hasta no tener
    // c�digo de enfermo + enfermedad)
    tabPanel.InsertarPanel("Cabecera", pCabecera);
    tabPanel.InsertarPanel("Declarantes", pDeclarantes);
    tabPanel.VerPanel("Cabecera");

    int modo = modoOperacion;
    if ( (modoOperacion == modoMODIFICACION)
        || (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      modoOperacion = modoESPERA;
      Inicializar();
      RellenaDatos();
    }

    modoOperacion = modo;
    Inicializar();
  }

  public StatusBar getBarra() {
    return barra;
  }

  public int autorizaciones_solo_declarantes() {
    try {
      //return: 0:modoMODIFICAR 1:modoSOLODECLARANTES -1:ERROR
      Vector vN1 = (Vector) hashNotifEDO.get("CD_NIVEL_1_AUTORIZACIONES");
      Vector vN2 = (Vector) hashNotifEDO.get("CD_NIVEL_2_AUTORIZACIONES");
      String sPerfil = (String) hashNotifEDO.get("PERFIL");
      String N1 = "";
      String N2 = "";

      //equipo solo modificara SUS notificaciones ptt no solo declar.
      //Epidemiologo CA: autorizado a todo, ptt no modo solo declar.
      if (sPerfil.equals("5") || sPerfil.equals("2")) {
        return (0);
      }
      //Epidemiologo N1: autorizado a vector
      if (sPerfil.equals("3")) {
        //caso: pertenece al conjunto de autoriz
        N1 = pCabecera.txtCodNivel1.getText().trim();
        if (!N1.equals("")) { // si vacio, continua
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1)) {
              return (0);
            }
          }
        }
        //1� declarante
        for (int i = 0; i < pDeclarantes.clistaTbl.size(); i++) {
          DataTab data = (DataTab) pDeclarantes.clistaTbl.elementAt(i);
          if (data.getIT_PRIMERO().equals("S")) {
            N1 = data.getCD_NIVEL_1_E_NOTIF();
          }
        }
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1)) {
            return (0);
          }
        }

        //si llego hasta aqui: SOLODECLARANTES
        return (1);
      } //fin Epidemiologo N1

      //Epidemiologo N2: autorizado a vectores
      if (sPerfil.equals("4")) {
        //caso:
        N1 = pCabecera.txtCodNivel1.getText().trim();
        N2 = pCabecera.txtCodNivel2.getText().trim();
        if (N1.equals("") && N2.equals("")) {
          //continuo
        }
        //caso-area, distrito  parejas (deben ser de igual tama�o)
        if (!N1.equals("") && !N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1)) {
              if (n2.trim().equals(N2)) {
                return (0);
              }
            }
          }
        }
        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1.equals("") && N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1)) {
              return (0);
            }
          }
        }
        //1� declarante siempre lleva area - distrito:
        for (int i = 0; i < pDeclarantes.clistaTbl.size(); i++) {
          DataTab data = (DataTab) pDeclarantes.clistaTbl.elementAt(i);
          if (data.getIT_PRIMERO().equals("S")) {
            N1 = data.getCD_NIVEL_1_E_NOTIF();
            N2 = data.getCD_NIVEL_2_E_NOTIF();
          }
        }

        //caso-area, distrito  parejas (deben ser de igual tama�o)
        if (!N1.equals("") && !N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1)) {
              if (n2.trim().equals(N2)) {
                return (0);
              }
            }
          }
        }
        //si llego hasta aqui: SOLODECLARANTES
        return (1);
      } //fin Epidemiologo N2
    }
    catch (Exception e) {
      e.printStackTrace();
      return ( -1);
    }

    return (1);
  } //autorizaciones

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  void Inicializar() {

    //AQUI ESTAN RELLENOS LOS DATOS DE PCABECERA
    //CONTROL DE SOLO_DECLARANTES
    if (modoOperacion == modoMODIFICACION) {

      int iPasarA = autorizaciones_solo_declarantes();
      if (iPasarA == -1) {
        btnCancelar_actionPerformed();
        return;
      }
      else if (iPasarA == 0) {
        modoOperacion = modoMODIFICACION;
      }
      else if (iPasarA == 1) {
        modoOperacion = modoSOLODECLARANTES;
      }
    }
    //---------------------------

    switch (modoOperacion) {

      case modoESPERA:
        enableControls(false);

        if (pCabecera != null) {
          pCabecera.Inicializar(modoOperacion);
        }
        if (pCaso != null) {
          pCaso.Inicializar(modoOperacion);
        }
        if (pDeclarantes != null) {
          pDeclarantes.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.WAIT_CURSOR);

        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
        enableControls(true);
        btnListado.setVisible(false); //************** Riverita
        btnAceptar.setLabel(res.getString("btnAceptar.Label"));
        btnCancelar.setLabel(res.getString("btnCancelar.Label"));
        if (pCabecera != null) {
          pCabecera.Inicializar(modoOperacion);
        }
        if (pCaso != null) {
          pCaso.Inicializar(modoOperacion);
        }
        if (pDeclarantes != null) {
          pDeclarantes.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case modoMODIFICACION:
        enableControls(true);
        btnAceptar.setLabel(res.getString("btnAceptar.Label"));
        btnCancelar.setLabel(res.getString("btnCancelar.Label"));
        if (pCabecera != null) {
          pCabecera.Inicializar(modoOperacion);
        }
        if (pCaso != null) {
          pCaso.Inicializar(modoOperacion);
        }
        if (pDeclarantes != null) {
          pDeclarantes.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA: // baja de la Edoind
      case modoCONSULTA:
      case modoSOLODECLARANTES:
        enableControls(true);
        btnAceptar.setLabel(res.getString("btnAceptarB.Label"));
        btnCancelar.setLabel(res.getString("btnCancelar.Label"));
        if (pCabecera != null) {
          pCabecera.Inicializar(modoOperacion);
        }
        if (pCaso != null) {
          pCaso.Inicializar(modoOperacion);
        }
        if (pDeclarantes != null) {
          pDeclarantes.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        if (modoOperacion == modoCONSULTA) {
          btnAceptar.setEnabled(false);

        }
        break;
    }
    //RESETEAMOS LA VARIABLE
    pCabecera.bSalir_del_dialogo = false;
    doLayout();
  }

  private void enableControls(boolean en) {
    btnAceptar.setEnabled(en);
    btnCancelar.setEnabled(en);
    btnListado.setEnabled(en);
    //pnl.setEnabled(en);
  }

// Rellena los datos de las solapas
  void RellenaDatos() {

    try {

      parametros = new CLista();
      // par�metros que se le pasan a DataEnfCaso: CD_ENFERMO, NM_CASO
      DataEnfCaso dataEnf = new DataEnfCaso("",
                                            (String) hashNotifEDO.get("NM_EDO"));

      parametros.addElement(dataEnf);

      parametros.setLogin(app.getLogin());
      parametros.setLortad(app.getParameter("LORTAD"));

      if (pCabecera.bTramero) {
        result = Comunicador.Communicate(this.getCApp(),
                                         stubCliente,
                                         servletSELECT_EDO_INVIDIDUALIZADA_SUCA,
                                         strSERVLET_INDIV_SELECT,
                                         parametros);
      }
      else {
        result = Comunicador.Communicate(this.getCApp(),
                                         stubCliente,
                                         servletSELECT_EDO_INVIDIDUALIZADA,
                                         strSERVLET_INDIV_SELECT,
                                         parametros);

        // comprueba que hay datos
      }
      if (result.size() == 0) {
        CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                       res.getString("msg2.Text"));
        msgBox.show();
        msgBox = null;
      }
      else {
        //tengo los datos de la Indiv
        RellenaPaneles(result);
      }
    }
    catch (Exception e) {
      ShowWarning(res.getString("msg3.Text"));
      dispose();
    }

  } //fin de rellenadatos

  public void RellenaPaneles(CLista clista) {

    DataTab datatab = new DataTab();
    datatab = (DataTab) clista.elementAt(0);

    hashNotifEDO.put("CD_ENFCIE", datatab.getCD_ENFCIE());

    pCabecera.RellenaCabecera(datatab);
    pDeclarantes.RellenaDeclarantes(clista);

    //tengo cargado DataEnfermodb y tengo datatab para Edoind
    //*****cargo las listas para el bloqueo (ENFERMO, EDOIND)
    Cargar_Lista_Bloqueo_en_RellenaPaneles(datatab);

    //SOLO SE ENTRA DESDE MODIFICACION_BAJA_CONSULTA
    //pintamos Caso-Protocolo
    if (pCaso != null) {
      borrarCaso();
    }
    if (pProtocolo != null) {
      borrarProtocolo();
    }
    addCaso();
    pCaso.RellenaCaso(datatab); //rellena el caso
    addProtocolo();

  }

  // m�todo que comprueba que los datos exitentes son suficientes
  // y v�lidos para visualizar el protocolo. Se necesitan:
  // nivel1, nivel2, c�d. y desc. enfermedad y n� caso
  public boolean isDataValidForProtocol() {

    // se comprueba el c�digo y la descripci�n de la enfermedad
    switch (modoOperacionBk) {

      // los datos se han recogido del choice choEnfermedad
      case modoALTA:

        if (pCabecera.getCodEnfermedad() == null) {
          return false;
        }
        if (pCabecera.getCodEnfermedad().equals("")) {
          return false;
        }
        if (pCabecera.getDesEnfermedad() == null) {
          return false;
        }
        if (pCabecera.getDesEnfermedad().equals("")) {
          return false;
        }

        break;

        // los datos vienen del maestro - detalle o de modificacionartificial
      case modoMODIFICACION:
      case modoBAJA:
        if ( ( (String) hashNotifEDO.get("NM_EDO") == null) ||
            ( (String) hashNotifEDO.get("NM_EDO")).equals("")) {
          return false;
        }

        if ( (String) hashNotifEDO.get("CD_ENFCIE") == null) {
          return false;
        }
        if ( ( (String) hashNotifEDO.get("CD_ENFCIE")).equals("")) {
          return false;
        }

        if ( (String) hashNotifEDO.get("DS_PROCESO") == null) {
          return false;
        }
        if ( ( (String) hashNotifEDO.get("DS_PROCESO")).equals("")) {
          return false;
        }

        break;
    }

    // si todos est�n rellenos, se retorna true
    return true;
  }

//SOLAPA CASO
  public void addCaso() {

    tabPanel.BorrarSolapa("Caso");
    pCaso = null;
    pCaso = new PanelEDOIndivCaso(this, modoOperacion, hashNotifEDO);

    tabPanel.InsertarPanel("Caso", pCaso);
    pCaso.doLayout();
    tabPanel.VerPanel("Cabecera");

  }

//CASO
  public void borrarCaso() {

    tabPanel.BorrarSolapa("Caso");
    tabPanel.VerPanel("Cabecera");

  }

//PROTOCOLO
  public void borrarProtocolo() {

    tabPanel.BorrarSolapa("Protocolo");
    tabPanel.VerPanel("Cabecera");

  }

//SOLAPA PROTOCOLO
  public void addProtocolo() {

    if (isDataValidForProtocol()) {

      DataProtocolo data = new DataProtocolo();
      switch (modoOperacionBk) {
        case modoALTA:
          data.sCodigo = pCabecera.getCodEnfermedad();
          data.sDescripcion = pCabecera.getDesEnfermedad();
          break;

          // los datos vienen del maestro - detalle o de modificacionartificial
        case modoMODIFICACION:
        case modoBAJA:
          data.sCodigo = (String) hashNotifEDO.get("CD_ENFCIE");
          data.sDescripcion = (String) hashNotifEDO.get("DS_PROCESO");
          break;
      }

      //data.sNivel1 = (String)hashNotifEDO.get("CD_NIVEL_1");
      //data.sNivel2 = (String)hashNotifEDO.get("CD_NIVEL_2_EQ");

      data.sNivel1 = pCabecera.txtCodNivel1.getText().trim();
      data.sNivel2 = pCabecera.txtCodNivel2.getText().trim();
      data.NumCaso = (String) hashNotifEDO.get("NM_EDO");
      data.sCa = app.getCA();

      // borramos la solapa por si acaso exist�a; as� actualizamos
      tabPanel.BorrarSolapa("Protocolo");
      pProtocolo = null;
      pProtocolo = new PanelEDO(capp, data, this); //MIO!!!!!!!

      if (!pProtocolo.pnl.getNoSeEncontraronDatos()) {
        YaSalioMsgEnferSinProtocolo = false;
        tabPanel.InsertarPanel("Protocolo", pProtocolo);
        pProtocolo.doLayout();
        tabPanel.VerPanel("Cabecera");
      }
      else {
        if (!YaSalioMsgEnferSinProtocolo) {
          ShowWarning(res.getString("msg4.Text"));

        }
        YaSalioMsgEnferSinProtocolo = true;
        pProtocolo = null;
      }

    }
    else {
      ShowWarning(res.getString("msg5.Text"));
    }
  } //ADDPROTOCOLO***********************************************

//
  public boolean Control_Perdida_Foco() {

    //CONTROL DE PERDIDA DE FOCO***************************
    //NOta: se ejecuta perdida de foco, pero tb al dar a salir
    //tenemos una variable que lo controla

    //NO SE CONTROLA EL MUNICIPIO!!!!

    if (!pCabecera.txtCodNivel1.getText().trim().equals("") &&
        !pCabecera.bNivel1Valid) {
      return (false);
    }

    if (!pCabecera.txtCodNivel2.getText().trim().equals("") &&
        !pCabecera.bNivel2Valid) {
      return (false);
    }
    if (!pCabecera.txtCodZBS.getText().trim().equals("") &&
        !pCabecera.bNivelZBSValid) {
      return (false);
    }

    return (true);

  }

//DATOS VALIDOS
  public boolean Indiv_Long_Valid() {

    CMessage msgBox = null;
    CLista lista = new CLista();
    lista = isLongValid();

    if (lista != null) { //ha ido MAL
      DataLongValid datos = null;
      datos = (DataLongValid) lista.firstElement();

      String msg = res.getString("msg6.Text") + datos.getnombre() +
          res.getString("msg7.Text") + datos.getlimite() +
          res.getString("msg8.Text");
      msgBox = new CMessage(this.app, CMessage.msgERROR, msg);
      msgBox.show();
      msgBox = null;

      if (datos.getpanel_nombre().equals("pCaso")) {
        tabPanel.VerPanel("Caso");
        pCaso.requestFocus();
      }
      else if (datos.getpanel_nombre().equals("pCabecera")) {
        tabPanel.VerPanel("Cabecera");
        pCabecera.requestFocus();
      }

      TextField txtAfectado;
      txtAfectado = (TextField) datos.gettxt();

      if (!datos.getnombre().equals("c�digo postal") &&
          !datos.getnombre().equals("v�a") &&
          !datos.getnombre().equals("n�mero") &&
          !datos.getnombre().equals("piso")) {
        txtAfectado.requestFocus();
        txtAfectado.select(0,
                           txtAfectado.getText().length());
      }

      return (false);
    }
    return (true);
  } //Indiv_Long_Valid

  public boolean Permiso_Baja_Indiv() {

    Vector vN1 = (Vector) hashNotifEDO.get("CD_NIVEL_1_AUTORIZACIONES");
    Vector vN2 = (Vector) hashNotifEDO.get("CD_NIVEL_2_AUTORIZACIONES");
    String sPerfil = (String) hashNotifEDO.get("PERFIL");
    String sm = "";
    boolean encontrado = false;
    bYaHuboMsg = false;

    if (sPerfil.equals("5")) {
      if (pDeclarantes.clistaTbl.size() > 1) {
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        ShowWarning(res.getString("msg9.Text"));
        bYaHuboMsg = true;
        return false; //existe otro declarante
      }
    } //5 -----
    else if (sPerfil.equals("2")) {
      sm = res.getString("msg10.Text") +
          res.getString("msg11.Text");
      msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
      msgBox.show();
      bYaHuboMsg = true;

      //NO
      if (!msgBox.getResponse()) {
        msgBox = null;
        return false; //lo anulamos
      }
      else { //SI //continuar
        msgBox = null;
        return true;
      }
    } // 2 --------

    else if (sPerfil.equals("3")) {

      //cada componente de la lista debe pertenecer a mis autorizaciones
      for (int i = 0; i < pDeclarantes.clistaTbl.size(); i++) {
        encontrado = false;
        DataTab dataTabla = (DataTab) pDeclarantes.clistaTbl.elementAt(i);
        String tabla = dataTabla.getCD_NIVEL_1_E_NOTIF();
        for (int j = 0; j < vN1.size(); j++) {
          String sdataGlobo = (String) vN1.elementAt(j);
          if (sdataGlobo.equals(dataTabla.getCD_NIVEL_1_E_NOTIF())) {
            encontrado = true;
          }
        } //for aut
        if (!encontrado) {
          return false; //no esta ese n1 en autorizaciones
        }

      } //for tabla

      //llego aqui: borrara
      sm = res.getString("msg10.Text") +
          res.getString("msg12.Text");
      msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
      msgBox.show();
      bYaHuboMsg = true;
      //NO
      if (!msgBox.getResponse()) {
        msgBox = null;
        return false; //lo anulamos
      }
      else { //SI //continuar
        msgBox = null;
        return true;
      }
    }
    else if (sPerfil.equals("3")) {

      //cada componente de la lista debe pertenecer a mis autorizaciones
      for (int i = 0; i < pDeclarantes.clistaTbl.size(); i++) {
        encontrado = false;
        DataTab dataTabla = (DataTab) pDeclarantes.clistaTbl.elementAt(i);
        String tabla = dataTabla.getCD_NIVEL_1_E_NOTIF();
        for (int j = 0; j < vN1.size(); j++) {
          String sdataGlobo = (String) vN1.elementAt(j);
          if (sdataGlobo.equals(dataTabla.getCD_NIVEL_1_E_NOTIF())) {
            encontrado = true;
          }
        } //for aut
        if (!encontrado) {
          return false; //no esta ese n1 en autorizaciones
        }

      } //for tabla

      //llego aqui: borrara
      sm = res.getString("msg10.Text") +
          res.getString("msg13.Text");
      msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
      msgBox.show();
      bYaHuboMsg = true;
      //NO
      if (!msgBox.getResponse()) {
        msgBox = null;
        return false; //lo anulamos
      }
      else { //SI //continuar
        msgBox = null;
        return true;
      }
    } // 3 --------
    else if (sPerfil.equals("4")) {

      //cada componente de la lista debe pertenecer a mis autorizaciones
      for (int i = 0; i < pDeclarantes.clistaTbl.size(); i++) {
        encontrado = false;
        DataTab dataTabla = (DataTab) pDeclarantes.clistaTbl.elementAt(i);
        String tabla1 = dataTabla.getCD_NIVEL_1_E_NOTIF();
        String tabla2 = dataTabla.getCD_NIVEL_2_E_NOTIF();

        for (int j = 0; j < vN1.size(); j++) {
          String sdataGlobo1 = (String) vN1.elementAt(j);
          String sdataGlobo2 = (String) vN2.elementAt(j);
          if (sdataGlobo1.equals(tabla1)) {
            if (sdataGlobo2.equals(tabla2)) {
              encontrado = true;
            }
          }
        } //for aut
        if (!encontrado) {
          return false; //no esta ese n1-n2 en autorizaciones
        }

      } //for tabla
      //llego aqui: borrara
      sm = res.getString("msg10.Text") +
          res.getString("msg14.Text");
      msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
      msgBox.show();
      bYaHuboMsg = true;
      //NO
      if (!msgBox.getResponse()) {
        msgBox = null;
        return false; //lo anulamos
      }
      else { //SI //continuar
        msgBox = null;
        return true;
      }
    } //3 --------
    else if (sPerfil.equals("4")) {

      //cada componente de la lista debe pertenecer a mis autorizaciones
      for (int i = 0; i < pDeclarantes.clistaTbl.size(); i++) {
        encontrado = false;
        DataTab dataTabla = (DataTab) pDeclarantes.clistaTbl.elementAt(i);
        String tabla1 = dataTabla.getCD_NIVEL_1_E_NOTIF();
        String tabla2 = dataTabla.getCD_NIVEL_2_E_NOTIF();

        for (int j = 0; j < vN1.size(); j++) {
          String sdataGlobo1 = (String) vN1.elementAt(j);
          String sdataGlobo2 = (String) vN2.elementAt(j);
          if (sdataGlobo1.equals(tabla1)) {
            if (sdataGlobo2.equals(tabla2)) {
              encontrado = true;
            }
          }
        } //for aut
        if (!encontrado) {
          return false; //no esta ese n1-n2 en autorizaciones
        }

      } //for tabla
      //llego aqui: borrara
      sm = res.getString("msg10.Text") +
          res.getString("msg15.Text");
      msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
      msgBox.show();
      bYaHuboMsg = true;
      //NO
      if (!msgBox.getResponse()) {
        msgBox = null;
        return false; //lo anulamos
      }
      else { //SI //continuar
        msgBox = null;
        return true;
      }
    } // 4 ---------------

    return true;
  } //permisos

//ALERTAS/////////////////////////////////////////////////////////////////////////
//Comprobar si la enfermedad seleccionada es individual urgente
  private boolean enfermedadUrgente() {
    String cdEnf = pCabecera.getCodEnfermedad();
    CLista listaEnf = pCabecera.getListaEnfermedad();
    DataIndivEnfermedad enf = null;

    for (int i = 0; i < listaEnf.size(); i++) {
      enf = (DataIndivEnfermedad) listaEnf.elementAt(i);
      if (cdEnf.equals(enf.CD_ENFCIE)) {
        if (enf.IT_INDURG.equals("S")) {
          return true;
        }
        else {
          return false;
        }
      }
    }
    return false;
  }

//Comprobar si la enfermedad seleccionada es de tipo A
  public String enfermedadTipoA() {
    String cdEnf = pCabecera.getCodEnfermedad();
    CLista listaEnf = pCabecera.getListaEnfermedad();
    DataIndivEnfermedad enf = null;

    for (int i = 0; i < listaEnf.size(); i++) {
      enf = (DataIndivEnfermedad) listaEnf.elementAt(i);
      if (cdEnf.equals(enf.CD_ENFCIE)) {
        if (enf.CD_TVIGI.equals("A")) {
          return "A";
        }
        else if (enf.CD_TVIGI.equals("I")) {
          return "I";
        }
      }
    }
    return "I";
  }

  void btnAceptar_actionPerformed() {

    CMessage msgBox = null;
    modoOperacionBk = modoOperacion; //BACKUP=COMO ENTRA EN ACEPTAR!!!
    Inicializar(modoESPERA);

    // ARG: Se comprueba si las fechas son validas
    if (!isFechasValid() && (modoOperacionBk == modoALTA ||
                             modoOperacionBk == modoMODIFICACION ||
                             modoOperacionBk == modoSOLODECLARANTES)) {
      Inicializar(modoOperacionBk);
      return;
    }

    //ENFER URGENTE -----------
    if (enfermedadUrgente() && modoOperacionBk == modoALTA) {
      String sm = res.getString("msg16.Text");
      msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
      msgBox.show();
      //NO
      if (!msgBox.getResponse()) {
        msgBox = null;
        modoOperacion = modoOperacionBk;
        Inicializar(modoOperacion);
        return;
      }
      else { //SI //continuar
        msgBox = null;
        // resto del c�digo de btnAceptar_actionPerformed, continua
      }
    } //-------------------

    //BAJA DE LA INDIV (borra Indiv y resp edo)
    if (modoOperacionBk == modoBAJA) {
      //autorizaciones----
      boolean bPermiso = Permiso_Baja_Indiv();
      if (!bPermiso) {
        if (!bYaHuboMsg) {
          ShowWarning(res.getString("msg17.Text"));

        }
        Inicializar(modoBAJA);
        btnCancelar_actionPerformed();
        return;
      }
      //------------------
      borrarEDOInd(); //cierra la ventana o se inicializa si falla
      return;
    }
    //MODIFICACION
    else if (modoOperacionBk == modoMODIFICACION ||
             modoOperacionBk == modoSOLODECLARANTES) {
      //modificacion de la edoIndv  --------------------------------
      if (!Control_Perdida_Foco()) {
        Inicializar(modoOperacionBk);
        return;
      }
      if (!Indiv_Long_Valid()) {
        Inicializar(modoOperacionBk);
        return;
      }
      if (!isDataValid()) { //datos obligatorios
        Inicializar(modoOperacionBk);
        return;
      }
      //modificar Indiv y protocolo si procede
      modificarEDOInd();
      return;

    }
    //ALTA
    else if (modoOperacionBk == modoALTA) {

      //alta de la edoIndv  --------------------------------
      if (!Control_Perdida_Foco()) {
        Inicializar(modoOperacionBk);
        return;
      }
      if (!Indiv_Long_Valid()) {
        Inicializar(modoOperacionBk);
        return;
      }
      if (!isDataValid()) { //datos obligatorios
        Inicializar(modoOperacionBk);
        return;
      }

      //alta de enfermo (CASO 1)-> si pesta�a protocolo abta
      //modificacion de enfermo (CASO 4)
      //saldra cambiando el modoOperacionBk si bien, a MODIFICAR
      insertarEDOInd();
      return;

    } //fin alta

  } //fin funcion ACEPTAR

//___________________________________________

  void btnListado_actionPerformed(ActionEvent evt) {

    DialInfEdoInd dlgInforme = null;
    CMessage msgBox = null;

    try {

      modoOperacionBk = modoOperacion; //BACKUP=COMO ENTRA EN ACEPTAR!!!
      Inicializar(modoESPERA);

      //dlgInforme = new DialInfEdoInd(app,(String)hashNotifEDO.get("NM_EDO") ) ;

      //DSR: Constructor de 3 par�metros
      dlgInforme = new DialInfEdoInd(app, (String) hashNotifEDO.get("NM_EDO"),
                                     (String) hashNotifEDO.get("DS_PROCESO").
                                     toString().substring(8));
      //DSR: Constructor de 3 par�metros

      dlgInforme.setEnabled(true);

      if (dlgInforme.GenerarInforme()) {
        dlgInforme.show();

        // error al procesar
      }
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    Inicializar(modoOperacionBk);
  }

//___________________________________________

//Control para Mov de enfemo
  private boolean Cambio_de_Enfermo() {

    boolean bReturn = false;

    //si el enfermo NO esta informado, es un alta
    if (pCabecera.txtCodEnfermo.getText().trim().equals("")) {
      return (bReturn);
    }

    //Si esta informado, estamos modificand
    DataEnfermoBD dataEnfermoBD = (DataEnfermoBD) pCabecera.getEnfermoBD();

    //confidencialidad
    //enfermo normal
    if (pCabecera.getFlagEnfermo()) {

      if (!dataEnfermoBD.getDS_APE1().trim().equals(pCabecera.txtApe1.getText().
          trim())
          ||
          !dataEnfermoBD.getDS_APE2().trim().equals(pCabecera.txtApe2.getText().
          trim())
          ||
          !dataEnfermoBD.getDS_NOMBRE().trim().equals(pCabecera.txtNombre.getText().
          trim())
          ||
          !dataEnfermoBD.getFC_NAC().trim().equals(pCabecera.txtFechaNac.getText().
          trim())
          ||
          !dataEnfermoBD.getCD_SEXO().trim().equals(pCabecera.getCodSexo().trim())
          ||
          !dataEnfermoBD.getDS_TELEF().trim().equals(pCabecera.txtTelefono.getText().
          trim())
          ||
          !dataEnfermoBD.getDS_NDOC().trim().equals(pCabecera.txtDocumento.getText().
          trim())
          ||
          !dataEnfermoBD.getCD_TDOC().trim().equals(pCabecera.getCodTipoDocumento().
          trim())
          ) {
        bReturn = true; //hemos cambiado enfermo
      }
      else {
        bReturn = false; //NO hemos cambiado enfermo

      }
    }
    //enfermo confidencial
    else {
      if (!dataEnfermoBD.getFC_NAC().trim().equals(pCabecera.txtFechaNac.
          getText().trim())
          ||
          !dataEnfermoBD.getCD_SEXO().trim().equals(pCabecera.getCodSexo().trim())
          ||
          !dataEnfermoBD.getDS_TELEF().trim().equals(pCabecera.txtTelefono.getText().
          trim())
          ||
          !dataEnfermoBD.getDS_NDOC().trim().equals(pCabecera.txtDocumento.getText().
          trim())
          ||
          !dataEnfermoBD.getCD_TDOC().trim().equals(pCabecera.getCodTipoDocumento().
          trim())
          ) {
        bReturn = true; //hemos cambiado enfermo
      }
      else {
        bReturn = false; //NO hemos cambiado enfermo
      }
    }

    return (bReturn);

  } //fin funcion

//0:  Enfermo existe. No se modifica
//1:  Enfermo no existe. Alta de enfermo
//2:  Enfermo Existe. Se modifica
  public DataEnfermoBD Actualizar_DataEnfermoBD_en_insertarEDOInd(int
      iRespecto_a_Enfermo,
      DataApesFonosCalcSiglas datosOtros,
      String sFechaInsertada) {

    DataEnfermoBD dataEnfermoBD = null;

    String n1 = "", n2 = "", zbs = "";
    if (iRespecto_a_Enfermo == 1) { //alta, enfermo absorbe datos caso
      n1 = pCabecera.txtCodNivel1.getText().trim();
      n2 = pCabecera.txtCodNivel2.getText().trim();
      zbs = pCabecera.txtCodZBS.getText().trim();
    }
    else if (iRespecto_a_Enfermo == 2) { //modif, no se cambian
      n1 = pCabecera.getEnfermoBD().getCD_NIVEL_1();
      n2 = pCabecera.getEnfermoBD().getCD_NIVEL_2();
      zbs = pCabecera.getEnfermoBD().getCD_ZBS();
    }
    if (iRespecto_a_Enfermo == 1 || iRespecto_a_Enfermo == 2) {
      dataEnfermoBD = new DataEnfermoBD(
          pCabecera.txtCodEnfermo.getText().trim(),
          pCabecera.getCodTipoDocumento(),
          datosOtros.getAPE1(), datosOtros.getAPE2(), datosOtros.getNOMBRE(),
          datosOtros.getAPE1T(), datosOtros.getAPE2T(), datosOtros.getNOMBRET(),
          datosOtros.getITCALC(),
          pCabecera.getFechaNac(),
          pCabecera.getCodSexo(),
          pCabecera.txtTelefono.getText().trim(),
          n1, n2, zbs,
          this.getCApp().getLogin(),
          sFechaInsertada,
          pCabecera.txtDocumento.getText().trim(),
          datosOtros.getSIGLAS());
      pCabecera.Cargar_DataEnfermoBD(dataEnfermoBD);
    }

    //0: dataEnfermo es el que esta en pCabecera
    if (iRespecto_a_Enfermo == 0) {
      dataEnfermoBD = (DataEnfermoBD) pCabecera.getEnfermoBD();
    }

    return (dataEnfermoBD);
  } //fin

  private void Cargar_Lista_Bloqueo_en_RellenaPaneles(DataTab datos) {

    //cargar listaopeultactIndiv
    DataOpeFc datosEnfermo;
    Vector vEnfermo = new Vector();
    DataOpeFc datavEnfermo = new DataOpeFc("CD_ENFERMO",
                                           datos.getCD_ENFERMO(), "N");
    vEnfermo.addElement(datavEnfermo);

    datosEnfermo = new DataOpeFc(datos.getCD_OPE(),
                                 datos.getFC_ULTACT(),
                                 "SIVE_ENFERMO", vEnfermo);

    if (listaOpeUltactIndiv.size() > 0) {
      listaOpeUltactIndiv.removeAllElements();

    }
    listaOpeUltactIndiv.addElement(datosEnfermo);
    //edoind
    DataOpeFc datosEdoind;
    Vector vEdoind = new Vector();
    DataOpeFc datavEdoind = new DataOpeFc("NM_EDO",
                                          datos.getNM_EDO().trim(), "N");
    vEdoind.addElement(datavEdoind);

    datosEdoind = new DataOpeFc(datos.getCD_OPE_EDOIND(),
                                datos.getFC_ULTACT_EDOIND(),
                                "SIVE_EDOIND", vEdoind);
    listaOpeUltactIndiv.addElement(datosEdoind);

  } //fin

  private void Cargar_Lista_Bloqueo_en_insertarEDOInd(DataEnfermoBD datos) {

    listaOpeUltactIndiv = new CLista();
    //cargar listaopeultactIndiv
    DataOpeFc datosEnfermo;
    Vector vEnfermo = new Vector();
    DataOpeFc datavEnfermo = new DataOpeFc("CD_ENFERMO",
                                           datos.getCD_ENFERMO(), "N");
    vEnfermo.addElement(datavEnfermo);

    datosEnfermo = new DataOpeFc(datos.getCD_OPE(),
                                 datos.getFC_ULTACT(),
                                 "SIVE_ENFERMO", vEnfermo);

    listaOpeUltactIndiv.addElement(datosEnfermo);

  } //fin

  private void Actualizar_o_Cargar_Lista_Bloqueo_en_insertarEDOInd(
      DataEnfermoBD datos,
      String sFechaInsertada) {

    //DataEnfermo tiene -o bien lo antiguo o lo nuevo- en este punto

    //cargar listaopeultactIndiv
    listaOpeUltactIndiv = new CLista();

    DataOpeFc datosEnfermo;
    Vector vEnfermo = new Vector();
    DataOpeFc datavEnfermo = new DataOpeFc("CD_ENFERMO",
                                           datos.getCD_ENFERMO(), "N");
    vEnfermo.addElement(datavEnfermo);

    datosEnfermo = new DataOpeFc(datos.getCD_OPE(),
                                 datos.getFC_ULTACT(),
                                 "SIVE_ENFERMO", vEnfermo);

    listaOpeUltactIndiv.addElement(datosEnfermo);
    //edoind  lo que acabo de meter
    DataOpeFc datosEdoind;
    Vector vEdoind = new Vector();
    DataOpeFc datavEdoind = new DataOpeFc("NM_EDO",
                                          (String) hashNotifEDO.get("NM_EDO"),
                                          "N");
    vEdoind.addElement(datavEdoind);

    datosEdoind = new DataOpeFc(this.getCApp().getLogin(),
                                sFechaInsertada,
                                "SIVE_EDOIND", vEdoind);
    listaOpeUltactIndiv.addElement(datosEdoind);

  } //fin

  private DataTab Estructura_Quizas_Enfermo_AltaCaso(DataTab datos,
      DataApesFonosCalcSiglas datosOtros) {

    //para la comprobacion del numero de individualizadas
    //con el numero de numericas para las enfermedades A
    datos.insert("CD_TVIGI", pCabecera.getTVigiEnfermedad());

    datos.insert("CD_ENFERMO", pCabecera.txtCodEnfermo.getText().trim());
    datos.insert("DS_APE1", datosOtros.getAPE1());
    datos.insert("DS_APE2", datosOtros.getAPE2());
    datos.insert("DS_NOMBRE", datosOtros.getNOMBRE());
    datos.insert("DS_FONOAPE1", datosOtros.getAPE1T());
    datos.insert("DS_FONOAPE2", datosOtros.getAPE2T());
    datos.insert("DS_FONONOMBRE", datosOtros.getNOMBRET());
    datos.insert("IT_CALC", datosOtros.getITCALC());
    datos.insert("FC_NAC", pCabecera.getFechaNac());
    datos.insert("CD_SEXO", pCabecera.getCodSexo());
    datos.insert("DS_TELEF", pCabecera.txtTelefono.getText().trim());
    datos.insert("CD_OPE", this.getCApp().getLogin());
    datos.insert("FC_ULTACT", UtilEDO.getFechaAct());
    if (pCabecera.txtDocumento.getText().trim().equals("")) {
      datos.insert("CD_TDOC", "");
    }
    else {
      datos.insert("CD_TDOC", pCabecera.getCodTipoDocumento());
    }
    datos.insert("DS_NDOC", pCabecera.txtDocumento.getText().trim());
    datos.insert("SIGLAS", datosOtros.getSIGLAS());
    //resto de campos no se actualizan en la query del servlet

    //suca
    datos.insert("CDVIAL", pCabecera.panSuca.getCDVIAL());
    datos.insert("CDTVIA", pCabecera.panSuca.getCDTVIA());
    datos.insert("CDTNUM", pCabecera.panSuca.getCDTNUM());
    datos.insert("DSCALNUM", pCabecera.panSuca.getDSCALNUM());
    datos.insert("CD_POSTAL", pCabecera.panSuca.getCD_POSTAL());
    datos.insert("DS_DIREC", pCabecera.panSuca.getDS_DIREC());
    datos.insert("DS_NUM", pCabecera.panSuca.getDS_NUM());
    datos.insert("DS_PISO", pCabecera.panSuca.getDS_PISO());
    datos.insert("CD_MUN", pCabecera.panSuca.getCD_MUN());
    // datos edind
    datos.insert("CD_MUN_EDOIND", pCabecera.panSuca.getCD_MUN());
    datos.insert("DS_CALLE", pCabecera.panSuca.getDS_DIREC());
    datos.insert("DS_NMCALLE", pCabecera.panSuca.getDS_NUM());
    datos.insert("DS_PISO_EDOIND", pCabecera.panSuca.getDS_PISO());
    datos.insert("CD_POSTAL_EDOIND", pCabecera.panSuca.getCD_POSTAL());

    //CASO
    datos.insert("NM_EDO", (String) hashNotifEDO.get("NM_EDO"));
    datos.insert("CD_ANOEPI", (String) hashNotifEDO.get("CD_ANOEPI"));
    datos.insert("CD_SEMEPI", (String) hashNotifEDO.get("CD_SEMEPI"));
    datos.insert("CD_ANOURG", "");
    datos.insert("NM_ENVIOURGSEM", "");

    datos.insert("CD_CLASIFDIAG", pCaso.getCodClasif());
    datos.insert("DS_CLASIFDIAG", pCaso.getDesClasif());

    datos.insert("IT_PENVURG", "");
    datos.insert("CD_PROV_EDOIND", pCabecera.getCodProvincia());
    datos.insert("CD_CA_EDOIND", pCabecera.getCodCA());
    datos.insert("CD_MUN_EDOIND", pCabecera.panSuca.getCD_MUN());
    datos.insert("CD_NIVEL_1_EDOIND", pCabecera.txtCodNivel1.getText().trim());
    datos.insert("DS_NIVEL_1_EDOIND", pCabecera.txtDesNivel1.getText().trim());
    datos.insert("CD_NIVEL_2_EDOIND", pCabecera.txtCodNivel2.getText().trim());
    datos.insert("DS_NIVEL_2_EDOIND", pCabecera.txtDesNivel2.getText().trim());
    datos.insert("CD_ZBS_EDOIND", pCabecera.txtCodZBS.getText().trim());
    datos.insert("DS_ZBS_EDOIND", pCabecera.txtDesZBS.getText().trim());
    datos.insert("FC_RECEP", (String) hashNotifEDO.get("FC_RECEP"));
    datos.insert("CD_OPE_EDOIND", this.getCApp().getLogin());
    datos.insert("FC_ULTACT_EDOIND", UtilEDO.getFechaAct());
    datos.insert("CD_ANOOTRC", "");
    datos.insert("NM_ENVOTRC", "");
    datos.insert("CD_ENFCIE", pCabecera.getCodEnfermedad());
    datos.insert("DS_PROCESO", pCabecera.getDesEnfermedad());
    datos.insert("FC_FECNOTIF", (String) hashNotifEDO.get("FC_FECNOTIF"));

    if (pCaso != null) {
      datos.insert("IT_DERIVADO", pCaso.getItDerivado());
      datos.insert("DS_CENTRODER", pCaso.txtCentro.getText().trim());
      datos.insert("IT_DIAGCLI", pCaso.getItDiagClin());
      datos.insert("FC_INISNT", pCaso.CfechaIniSint.getText().trim());
      datos.insert("DS_COLECTIVO", pCaso.txtColectivo.getText().trim());
      datos.insert("IT_ASOCIADO", pCaso.getItAsociado());
      datos.insert("IT_DIAGMICRO", pCaso.getItDiagMicro());
      datos.insert("DS_ASOCIADO", pCaso.txtAsociado.getText().trim());
      datos.insert("IT_DIAGSERO", pCaso.getItDiagSero());
      datos.insert("DS_DIAGOTROS", pCaso.txtDgOtros.getText().trim());
    }
    else {
      datos.insert("IT_DERIVADO", "");
      datos.insert("DS_CENTRODER", "");
      datos.insert("IT_DIAGCLI", "");
      datos.insert("FC_INISNT", "");
      datos.insert("DS_COLECTIVO", "");
      datos.insert("IT_ASOCIADO", "");
      datos.insert("IT_DIAGMICRO", "");
      datos.insert("DS_ASOCIADO", "");
      datos.insert("IT_DIAGSERO", "");
      datos.insert("DS_DIAGOTROS", "");
    }

    datos.insert("CD_E_NOTIF", pDeclarantes.getCodEquipo(0)); //(String)hashNotifEDO.get("CD_E_NOTIF"),
    datos.insert("CD_ANOEPI_NOTIF_EDOI", pDeclarantes.getAnno(0)); //(String)hashNotifEDO.get("CD_ANOEPI"),
    datos.insert("CD_SEMEPI_NOTIF_EDOI", pDeclarantes.getSemana(0)); //(String)hashNotifEDO.get("CD_SEMEPI"),
    datos.insert("FC_RECEP_NOTIF_EDOI", pDeclarantes.getFechaRecep(0)); //(String)hashNotifEDO.get("FC_RECEP"),
    datos.insert("FC_FECNOTIF_NOTIF_EDOI", pDeclarantes.getFechaNotif(0)); //(String)hashNotifEDO.get("FC_FECNOTIF"),
    datos.insert("DS_DECLARANTE", pDeclarantes.getDeclarante(0));
    datos.insert("IT_PRIMERO", pDeclarantes.getItPrimero(0));
    datos.insert("CD_OPE_NOTIF_EDOI", pDeclarantes.getCdOpe(0));
    datos.insert("FC_ULTACT_NOTIF_EDOI", pDeclarantes.getFcUltAct(0));

    return (datos);

  } //fin

  private DataTab Estructura_AltaEnfermo_AltaCaso(DataTab datos,
                                                  DataApesFonosCalcSiglas
                                                  datosOtros) {

    //para la comprobacion del numero de individualizadas
    //con el numero de numericas para las enfermedades A
    datos.insert("CD_TVIGI", pCabecera.getTVigiEnfermedad());

    datos.insert("CD_ENFERMO", pCabecera.txtCodEnfermo.getText().trim());
    datos.insert("CD_PROV", pCabecera.getCodProvincia());
    datos.insert("CD_PAIS", pCabecera.getCodPais());

    if (pCabecera.txtDocumento.getText().trim().equals("")) {
      datos.insert("CD_TDOC", "");
    }
    else {
      datos.insert("CD_TDOC", pCabecera.getCodTipoDocumento());
      //suca
    }
    datos.insert("CDVIAL", pCabecera.panSuca.getCDVIAL());
    datos.insert("CDTVIA", pCabecera.panSuca.getCDTVIA());
    datos.insert("CDTNUM", pCabecera.panSuca.getCDTNUM());
    datos.insert("DSCALNUM", pCabecera.panSuca.getDSCALNUM());
    datos.insert("CD_POSTAL", pCabecera.panSuca.getCD_POSTAL());
    datos.insert("DS_DIREC", pCabecera.panSuca.getDS_DIREC());
    datos.insert("DS_NUM", pCabecera.panSuca.getDS_NUM());
    datos.insert("DS_PISO", pCabecera.panSuca.getDS_PISO());
    datos.insert("CD_MUN", pCabecera.panSuca.getCD_MUN());
    // datos edind
    datos.insert("CD_MUN_EDOIND", pCabecera.panSuca.getCD_MUN());
    datos.insert("DS_CALLE", pCabecera.panSuca.getDS_DIREC());
    datos.insert("DS_NMCALLE", pCabecera.panSuca.getDS_NUM());
    datos.insert("DS_PISO_EDOIND", pCabecera.panSuca.getDS_PISO());
    datos.insert("CD_POSTAL_EDOIND", pCabecera.panSuca.getCD_POSTAL());

    datos.insert("DS_APE1", datosOtros.getAPE1());
    datos.insert("DS_APE2", datosOtros.getAPE2());
    datos.insert("DS_NOMBRE", datosOtros.getNOMBRE());
    datos.insert("DS_FONOAPE1", datosOtros.getAPE1T());
    datos.insert("DS_FONOAPE2", datosOtros.getAPE2T());
    datos.insert("DS_FONONOMBRE", datosOtros.getNOMBRET());
    datos.insert("IT_CALC", datosOtros.getITCALC());
    datos.insert("FC_NAC", pCabecera.getFechaNac());
    datos.insert("CD_SEXO", pCabecera.getCodSexo());
    datos.insert("DS_SEXO", pCabecera.getDesSexo());

    datos.insert("DS_TELEF", pCabecera.txtTelefono.getText().trim());
    datos.insert("CD_NIVEL_1", pCabecera.txtCodNivel1.getText().trim());
    datos.insert("CD_NIVEL_2", pCabecera.txtCodNivel2.getText().trim());
    datos.insert("CD_ZBS", pCabecera.txtCodZBS.getText().trim());
    datos.insert("CD_OPE", this.getCApp().getLogin());
    datos.insert("FC_ULTACT", UtilEDO.getFechaAct());
    datos.insert("IT_REVISADO", "N"); //ALTA DE ENFERMO
    datos.insert("CD_MODBAJA", ""); //sCdMotBaja,
    datos.insert("DS_MODBAJA", ""); //sDsMotBaja,
    datos.insert("FC_BAJA", ""); //sFechaBaja,
    datos.insert("DS_NDOC", pCabecera.txtDocumento.getText().trim());
    datos.insert("SIGLAS", datosOtros.getSIGLAS());

    //caso
    datos.insert("NM_EDO", ""); // a vacio (String)hashNotifEDO.get("NM_EDO"),
    datos.insert("CD_ANOEPI", (String) hashNotifEDO.get("CD_ANOEPI"));
    datos.insert("CD_SEMEPI", (String) hashNotifEDO.get("CD_SEMEPI"));
    datos.insert("CD_ANOURG", "");
    datos.insert("NM_ENVIOURGSEM", "");
    //sin cd_enfermo, la pesta�a de pCaso esta desinformada *****
    datos.insert("CD_CLASIFDIAG", ""); //pCaso.getCodClasif());
    datos.insert("DS_CLASIFDIAG", ""); //pCaso.getDesClasif());
    datos.insert("IT_PENVURG", "");
    datos.insert("CD_PROV_EDOIND", pCabecera.getCodProvincia());
    datos.insert("CD_CA_EDOIND", pCabecera.getCodCA());
    datos.insert("CD_NIVEL_1_EDOIND", pCabecera.txtCodNivel1.getText().trim());
    datos.insert("DS_NIVEL_1_EDOIND", pCabecera.txtDesNivel1.getText().trim());
    datos.insert("CD_NIVEL_2_EDOIND", pCabecera.txtCodNivel2.getText().trim());
    datos.insert("DS_NIVEL_2_EDOIND", pCabecera.txtDesNivel2.getText().trim());
    datos.insert("CD_ZBS_EDOIND", pCabecera.txtCodZBS.getText().trim());
    datos.insert("DS_ZBS_EDOIND", pCabecera.txtDesZBS.getText().trim());
    datos.insert("FC_RECEP", (String) hashNotifEDO.get("FC_RECEP"));
    datos.insert("CD_OPE_EDOIND", this.getCApp().getLogin());
    datos.insert("FC_ULTACT_EDOIND", UtilEDO.getFechaAct());
    datos.insert("CD_ANOOTRC", "");
    datos.insert("NM_ENVOTRC", "");
    datos.insert("CD_ENFCIE", pCabecera.getCodEnfermedad());
    datos.insert("DS_PROCESO", pCabecera.getDesEnfermedad());
    datos.insert("FC_FECNOTIF", (String) hashNotifEDO.get("FC_FECNOTIF"));

    //sin cd_enfermo, la pesta�a de pCaso esta desinformada *********
    datos.insert("IT_DERIVADO", ""); //pCaso.getItDerivado());
    datos.insert("DS_CENTRODER", ""); //pCaso.txtCentro.getText().trim());
    datos.insert("IT_DIAGCLI", ""); //pCaso.getItDiagClin());
    datos.insert("FC_INISNT", ""); //pCaso.CfechaIniSint.getText().trim());
    datos.insert("DS_COLECTIVO", ""); //pCaso.txtColectivo.getText().trim());
    datos.insert("IT_ASOCIADO", ""); //pCaso.getItAsociado());
    datos.insert("IT_DIAGMICRO", ""); //pCaso.getItDiagMicro());
    datos.insert("DS_ASOCIADO", ""); //pCaso.txtAsociado.getText().trim());
    datos.insert("IT_DIAGSERO", ""); //pCaso.getItDiagSero());
    datos.insert("DS_DIAGOTROS", ""); //pCaso.txtDgOtros.getText().trim());

    datos.insert("CD_E_NOTIF", pDeclarantes.getCodEquipo(0)); //(String)hashNotifEDO.get("CD_E_NOTIF"),
    datos.insert("CD_ANOEPI_NOTIF_EDOI", pDeclarantes.getAnno(0)); //(String)hashNotifEDO.get("CD_ANOEPI"),
    datos.insert("CD_SEMEPI_NOTIF_EDOI", pDeclarantes.getSemana(0)); //(String)hashNotifEDO.get("CD_SEMEPI"),
    datos.insert("FC_RECEP_NOTIF_EDOI", pDeclarantes.getFechaRecep(0)); //(String)hashNotifEDO.get("FC_RECEP"),
    datos.insert("FC_FECNOTIF_NOTIF_EDOI", pDeclarantes.getFechaNotif(0)); //(String)hashNotifEDO.get("FC_FECNOTIF"),
    datos.insert("DS_DECLARANTE", pDeclarantes.getDeclarante(0));
    datos.insert("IT_PRIMERO", pDeclarantes.getItPrimero(0));
    datos.insert("CD_OPE_NOTIF_EDOI", pDeclarantes.getCdOpe(0));
    datos.insert("FC_ULTACT_NOTIF_EDOI", pDeclarantes.getFcUltAct(0));

    return (datos);
  } //fin

  private DataTab Estructura_Quizas_Enfermo_ModifCaso(DataTab datos,
      DataApesFonosCalcSiglas datosOtros) {

    datos.insert("CD_ENFERMO", pCabecera.txtCodEnfermo.getText().trim());
    datos.insert("DS_APE1", datosOtros.getAPE1());
    datos.insert("DS_APE2", datosOtros.getAPE2());
    datos.insert("DS_NOMBRE", datosOtros.getNOMBRE());
    datos.insert("DS_FONOAPE1", datosOtros.getAPE1T());
    datos.insert("DS_FONOAPE2", datosOtros.getAPE2T());
    datos.insert("DS_FONONOMBRE", datosOtros.getNOMBRET());
    datos.insert("IT_CALC", datosOtros.getITCALC());
    datos.insert("FC_NAC", pCabecera.getFechaNac());
    datos.insert("CD_SEXO", pCabecera.getCodSexo());

    datos.insert("DS_TELEF", pCabecera.txtTelefono.getText().trim());
    datos.insert("CD_OPE", this.getCApp().getLogin());
    datos.insert("FC_ULTACT", UtilEDO.getFechaAct());
    datos.insert("DS_NDOC", pCabecera.txtDocumento.getText().trim());
    if (pCabecera.txtDocumento.getText().trim().equals("")) {
      datos.insert("CD_TDOC", "");
    }
    else {
      datos.insert("CD_TDOC", pCabecera.getCodTipoDocumento());

    }
    datos.insert("SIGLAS", datosOtros.getSIGLAS());

    //suca
    datos.insert("CDVIAL", pCabecera.panSuca.getCDVIAL());
    datos.insert("CDTVIA", pCabecera.panSuca.getCDTVIA());
    datos.insert("CDTNUM", pCabecera.panSuca.getCDTNUM());
    datos.insert("DSCALNUM", pCabecera.panSuca.getDSCALNUM());
    datos.insert("CD_POSTAL", pCabecera.panSuca.getCD_POSTAL());
    datos.insert("DS_DIREC", pCabecera.panSuca.getDS_DIREC());
    datos.insert("DS_NUM", pCabecera.panSuca.getDS_NUM());
    datos.insert("DS_PISO", pCabecera.panSuca.getDS_PISO());
    datos.insert("CD_MUN", pCabecera.panSuca.getCD_MUN());
    // datos edind
    datos.insert("CD_MUN_EDOIND", pCabecera.panSuca.getCD_MUN());
    datos.insert("DS_CALLE", pCabecera.panSuca.getDS_DIREC());
    datos.insert("DS_NMCALLE", pCabecera.panSuca.getDS_NUM());
    datos.insert("DS_PISO_EDOIND", pCabecera.panSuca.getDS_PISO());
    datos.insert("CD_POSTAL_EDOIND", pCabecera.panSuca.getCD_POSTAL());

    //caso
    datos.insert("NM_EDO", (String) hashNotifEDO.get("NM_EDO"));
    datos.insert("CD_ANOEPI", (String) hashNotifEDO.get("CD_ANOEPI"));
    datos.insert("CD_SEMEPI", (String) hashNotifEDO.get("CD_SEMEPI"));
    datos.insert("CD_ANOURG", "");
    datos.insert("NM_ENVIOURGSEM", "");
    datos.insert("CD_CLASIFDIAG", pCaso.getCodClasif());
    datos.insert("DS_CLASIFDIAG", pCaso.getDesClasif());
    datos.insert("IT_PENVURG", "");
    datos.insert("CD_PROV_EDOIND", pCabecera.getCodProvincia());
    datos.insert("CD_CA_EDOIND", pCabecera.getCodCA());
    datos.insert("CD_NIVEL_1_EDOIND", pCabecera.txtCodNivel1.getText().trim());
    datos.insert("DS_NIVEL_1_EDOIND", pCabecera.txtDesNivel1.getText().trim());
    datos.insert("CD_NIVEL_2_EDOIND", pCabecera.txtCodNivel2.getText().trim());
    datos.insert("DS_NIVEL_2_EDOIND", pCabecera.txtDesNivel2.getText().trim());
    datos.insert("CD_ZBS_EDOIND", pCabecera.txtCodZBS.getText().trim());
    datos.insert("DS_ZBS_EDOIND", pCabecera.txtDesZBS.getText().trim());

    datos.insert("FC_RECEP", pCabecera.sFechaRecepcion);
    datos.insert("CD_OPE_EDOIND", this.getCApp().getLogin());
    datos.insert("FC_ULTACT_EDOIND", UtilEDO.getFechaAct());
    datos.insert("CD_ANOOTRC", "");
    datos.insert("NM_ENVOTRC", "");

    datos.insert("CD_ENFCIE", (String) hashNotifEDO.get("CD_ENFCIE")); //pCabecera.getCodEnfermedad(),
    datos.insert("DS_PROCESO", (String) hashNotifEDO.get("DS_PROCESO")); //pCabecera.getDesEnfermedad(),
    datos.insert("FC_FECNOTIF", pCabecera.sFechaNotificacion);
    datos.insert("IT_DERIVADO", pCaso.getItDerivado());
    datos.insert("DS_CENTRODER", pCaso.txtCentro.getText().trim());
    datos.insert("IT_DIAGCLI", pCaso.getItDiagClin());
    datos.insert("FC_INISNT", pCaso.CfechaIniSint.getText().trim());
    datos.insert("DS_COLECTIVO", pCaso.txtColectivo.getText().trim());
    datos.insert("IT_ASOCIADO", pCaso.getItAsociado());
    datos.insert("IT_DIAGMICRO", pCaso.getItDiagMicro());
    datos.insert("DS_ASOCIADO", pCaso.txtAsociado.getText().trim());
    datos.insert("IT_DIAGSERO", pCaso.getItDiagSero());
    datos.insert("DS_DIAGOTROS", pCaso.txtDgOtros.getText().trim());

    datos.insert("CD_E_NOTIF", pDeclarantes.getCodEquipo(0)); //(String)hashNotifEDO.get("CD_E_NOTIF"),
    datos.insert("CD_ANOEPI_NOTIF_EDOI", pDeclarantes.getAnno(0)); //(String)hashNotifEDO.get("CD_ANOEPI"),
    datos.insert("CD_SEMEPI_NOTIF_EDOI", pDeclarantes.getSemana(0)); //(String)hashNotifEDO.get("CD_SEMEPI"),
    datos.insert("FC_RECEP_NOTIF_EDOI", pDeclarantes.getFechaRecep(0)); //(String)hashNotifEDO.get("FC_RECEP"),
    datos.insert("FC_FECNOTIF_NOTIF_EDOI", pDeclarantes.getFechaNotif(0)); //(String)hashNotifEDO.get("FC_FECNOTIF"),
    datos.insert("DS_DECLARANTE", pDeclarantes.getDeclarante(0));
    datos.insert("IT_PRIMERO", pDeclarantes.getItPrimero(0));
    datos.insert("CD_OPE_NOTIF_EDOI", pDeclarantes.getCdOpe(0));
    datos.insert("FC_ULTACT_NOTIF_EDOI", pDeclarantes.getFcUltAct(0));

    return (datos);

  } //fin

//ALTA en edoind (nuevo caso, con enfermo o sin enfermo)
  private void insertarEDOInd() {

    //ENTRAMOS EN ESPERA
    //modoOperacionBk tiene modoALTA
    //SALDREMOS EN modoMODIFICAR si bien y modoALTA si mal
    //debemos INICIALIZAR antes de salir

    int modoSrv = 0;
    parametros = new CLista();
    DataTab datatab;

    //0: inicicial 1: alta de enfermo 2: modificacion de enfermo
    int iCargarDataEnfermodb = 0;

    String sFecha_Actual = "";

    //confidencialidad
    if (!pCabecera.getFlagEnfermo()) { //confid
      pCabecera.Visible(false);

    }
    String sApe1 = "", sApe2 = "", sNombre = "", sSiglas = "";
    String sApe1T = "", sApe2T = "", sNombreT = "", sItCalc = "";

    //es un alta!!!!
    String sFechaNotif = (String) hashNotifEDO.get("FC_FECNOTIF");
    String sFechaRecep = (String) hashNotifEDO.get("FC_RECEP");

    //enfermo confidencial y lo vamos a modificar (ie cdenfermo informado)
    if (!pCabecera.getFlagEnfermo()
        && !pCabecera.txtCodEnfermo.getText().trim().equals("")) {

      DataEnfermoBD dataEnfermoBD = (DataEnfermoBD) pCabecera.getEnfermoBD();
      //ape1 va siglas y no se modifica (de la BD directamente)
      sApe1 = dataEnfermoBD.getDS_APE1();
      sApe2 = dataEnfermoBD.getDS_APE2();
      sNombre = dataEnfermoBD.getDS_NOMBRE();
      sApe1T = dataEnfermoBD.getDS_FONOAPE1();
      sApe2T = dataEnfermoBD.getDS_FONOAPE2();
      sNombreT = dataEnfermoBD.getDS_FONONOMBRE();
      sSiglas = dataEnfermoBD.getSIGLAS();
    }

    //enfermo NO confidencial, estara TODO HABILITADO (en alta)
    else {

      //fonetica
      sApe1 = pCabecera.txtApe1.getText().trim();
      sApe2 = pCabecera.txtApe2.getText().trim();
      sNombre = pCabecera.txtNombre.getText().trim();
      sApe1T = comun.traduccionFonetica(pCabecera.txtApe1.getText().trim());
      sApe2T = comun.traduccionFonetica(pCabecera.txtApe2.getText().trim());
      sNombreT = comun.traduccionFonetica(pCabecera.txtNombre.getText().trim());

      //siglas
      sSiglas = Calcular_Siglas(pCabecera.getApe1(),
                                pCabecera.getApe2(),
                                pCabecera.getNombre());
    }

    if (pCabecera.lTipoFechaNac.getText().trim().equals("real")) {
      sItCalc = "N";
    }
    else {
      sItCalc = "S";

    }
    String msg = "";

    //lo guardamos en una estructura
    DataApesFonosCalcSiglas datosApesFonosCalcSiglas = new
        DataApesFonosCalcSiglas
        (sApe1, sApe2, sNombre, sApe1T, sApe2T, sNombreT, sItCalc, sSiglas);

    //********************************************************************************************************
     //caso 1: CD_ENFERMO no informado: nuevo enfermo, nuevo caso, posible varias notif_edoi
     //ESTRUCTURA DE LA LISTA:
     //primer registro :
     //datos de enfermo (Excepto Cod max +1), datos de caso (Excepto nm_edo max +1), datos declarante
     //Nota: hay un bucle pero solo se inserta un declarante)
     //listaSalida --> DataEnfCaso con CodEnf, y Num_Edo (strings)
     //Si lista vacia, no hace nada: listaSalida.size() == 0
     //enfermo absorbe datos del caso
     ///////servletSIN_CD_ENFERMO_INSERT_INSERT_INSERT = 0; /////
     //************************************************************************************************************

    if (pCabecera.txtCodEnfermo.getText().trim().equals("")) {

      iCargarDataEnfermodb = 1; //alta de enfermo

      //EL enfermo absorbe los datos del caso
      datatab = new DataTab();
      datatab.insert("IT_MOV_ENFERMO", "N"); //NO VALE PARA NADA EN EL ALTA

      datatab = Estructura_AltaEnfermo_AltaCaso(datatab,
                                                datosApesFonosCalcSiglas);

      modoSrv = servletSIN_CD_ENFERMO_INSERT_INSERT_INSERT;
      msg = res.getString("msg18.Text");
    }
    //************************************************************************************************************
     //caso 4:(PARALELO AL 2) CD_ENFERMo informado y SUS DATOS
     //           NM_EDO no informado
     //          , nuevo caso, UNA notif_edoi nueva
     //ESTRUCTURAenfermo  DE LA LISTA:
     //primer registro :
     //(incluye Cod enfermo Y SUS DATOS), datos de caso (Excepto nm_edo max +1), datos del declarante
     //listaSalida --> DataEnfCaso con CodEnf, y Num_Edo (strings)
     //Nota: hay un bucle pero solo se inserta un declarante)
     //Si lista vacia, no puede ser (todas tablas tienen campos obligatorios), no hace nada
     /////servletCON_CD_ENFERMO_SIN_NM_EDO_MODIF_INSERT_INSERT = 3; ////
     //************************************************************************************************************

      else {

        datatab = new DataTab();
        //enfermo lo modificamos???
        boolean bMover = false;
        bMover = Cambio_de_Enfermo();

        if (bMover) {
          datatab.insert("IT_MOV_ENFERMO", "S");
          iCargarDataEnfermodb = 2; //modif enfermo
        }
        else {
          datatab.insert("IT_MOV_ENFERMO", "N");
          iCargarDataEnfermodb = 0; //nada
        }

        datatab = Estructura_Quizas_Enfermo_AltaCaso(datatab,
            datosApesFonosCalcSiglas);

        modoSrv = servletCON_CD_ENFERMO_SIN_NM_EDO_MODIF_INSERT_INSERT;

        if (bMover) {
          msg = res.getString("msg19.Text");
        }
        else {
          msg = res.getString("msg20.Text");

        }

      } //fin del if/else

    //CONTROL de BLOQUEO -------------------------------------------
    if (iCargarDataEnfermodb == 2) { //vamos a modif enfermo existente
      Cargar_Lista_Bloqueo_en_insertarEDOInd( (DataEnfermoBD) pCabecera.
                                             getEnfermoBD());
      //la lista de bloqueo s�lo lleva enfermo
      if (!ComprobarOpeFc_Alta_Indiv()) {
        listaOpeUltactIndiv = new CLista();
        bModificacionArtificial = false;
        //confidencialidad -------------
        if (!pCabecera.getFlagEnfermo()) { //confid
          pCabecera.Confidencialidad();
        }
        else {
          pCabecera.Visible(true);
        } //---------------------------

        Inicializar(modoOperacionBk); //contiene ALTA
        return;
      }
    }
    //--------------------------------------------------------------

    parametros.addElement(datatab);

    try {
      DataEnfCaso dataEnf;
      // modificacion momentanea

      parametros.setLogin(app.getLogin());
      parametros.setLortad(app.getParameter("LORTAD"));

      result = Comunicador.Communicate(this.getCApp(),
                                       stubCliente,
                                       modoSrv,
                                       strSERVLET_INDIV_IM,
                                       parametros);

      /*
             SrvIndivIM srv = new SrvIndivIM();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.0.13:1521:SIVE",
                             "dba_edo",
                             "manager");
             result = srv.doDebug(modoSrv, parametros);
             srv = null;
       */

      //lista.size = 0  : Partes > casos , no dejar
      //lista = null    : catch (excepcion)
      //lista con datos : OK
      if (result.size() == 0) {
        ShowWarning(res.getString("msg21.Text"));
        iOut = -1;
        listaOpeUltactIndiv = new CLista();
        bModificacionArtificial = false;
        //la listaBloqueo aun no se ha tocado
        //confidencialidad -------------
        if (!pCabecera.getFlagEnfermo()) { //confid
          pCabecera.Confidencialidad();
        }
        else {
          pCabecera.Visible(true);
        } //---------------------------nota: si el alta y va mal, se vuelva a ver
        Inicializar(modoOperacionBk); //contiene ALTA
        return;
      }
      else if (result.size() != 0) {
        dataEnf = (DataEnfCaso) result.firstElement();

        sFecha_Actual = dataEnf.getFC_ULTACT();
        hashNotifEDO.put("NM_EDO", dataEnf.getNM_EDO()); //IMPORTANTE!!!
        hashNotifEDO.put("CD_ENFERMO", dataEnf.getCD_ENFERMO());
        hashNotifEDO.put("CD_ENFCIE", pCabecera.getCodEnfermedad());
        hashNotifEDO.put("DS_PROCESO", pCabecera.getDesEnfermedad());

        //PONEMOS EL ENFERMO
        pCabecera.txtCodEnfermo.setText(dataEnf.getCD_ENFERMO());
        pCabecera.sStringBk = dataEnf.getCD_ENFERMO(); //backup del enfermo
        pCabecera.txtCodEnfermo.setBackground(new Color(255, 255, 150));
      }

      // si todo ha ido bien, se muestra un mensaje
      //14/09/99 quitar msg (Enrique)
      //$$ShowWarning(msg + ".Estado: Modificaci�n");
      // el alta se ha efectuado correctamente ->

      bModificacionArtificial = true;

      //FECHAS QUE USARE AL MODIFICAR!!!!
      pCabecera.sFechaRecepcion = (String) hashNotifEDO.get("FC_RECEP");
      pCabecera.sFechaNotificacion = (String) hashNotifEDO.get("FC_FECNOTIF");

      // SE PASA MODO MODIFICACI�N en todo el dialog
      modoOperacionBk = modoMODIFICACION;

      iOut = 0;
      if (!bCabeceraAceptada) {
        bCabeceraAceptada = true; //????

      }

    }
    catch (Exception e) { //SUPONEMOS PUEDE FALLAR EN EL SERVLET
      iOut = -1;
      e.printStackTrace();
      ShowWarning(res.getString("msg22.Text"));
      listaOpeUltactIndiv = new CLista();
      bModificacionArtificial = false;
      //la listaBloqueo aun no se ha tocado
      //confidencialidad -------------
      if (!pCabecera.getFlagEnfermo()) { //confid
        pCabecera.Confidencialidad();
      }
      else {
        pCabecera.Visible(true);
      } //---------------------------nota: si el alta y va mal, se vuelva a ver
      Inicializar(modoOperacionBk); //contiene ALTA
      return;
    }

    //A PARTIR DE AQUI, SUPONEMOS NO EXISTE ERROR

    // si la enfermedad es urgente env�a un email
    if (enfermedadUrgente()) {
      if (! (this.getCApp().getDS_EMAIL().equals("")) &&
          ! (pCabecera.txtCodNivel1.getText().trim().equals(""))) {

        Mail miMail = new Mail();
        miMail.notificacionUrgente(this.getCApp(),
                                   (String) hashNotifEDO.get("NM_EDO"),
                                   pCabecera.txtCodNivel1.getText().trim(),
                                   (String) hashNotifEDO.get("CD_E_NOTIF"),
                                   (String) hashNotifEDO.get("DS_E_NOTIF"),
                                   pCabecera.getCodEnfermedad(),
                                   pCabecera.getDesEnfermedad(),
                                   this.getCApp().getDS_EMAIL());
      }
    }

    DataEnfermoBD dataEnfermoBD = Actualizar_DataEnfermoBD_en_insertarEDOInd(
        iCargarDataEnfermodb,
        datosApesFonosCalcSiglas,
        sFecha_Actual);

    //confidencialidad -------------
    if (!pCabecera.getFlagEnfermo()) { //confid
      pCabecera.Confidencialidad();
    }
    else {
      pCabecera.Visible(true);
    } //---------------------------nota: si el alta va mal, no se recupera

    //tengo cargado DataEnfermodb y tengo pCabecera para Edoind
    //*****cargo las listas para el bloqueo (ENFERMO, EDOIND)
     Actualizar_o_Cargar_Lista_Bloqueo_en_insertarEDOInd(dataEnfermoBD,
         sFecha_Actual);

     //INDICADORES-----------
    miraIndicadores mira = new miraIndicadores(capp);
    // La zonificacion que se usa para comprobar las alarmas
    // esta en funcion del perfil del usuario
    String perfil = (String) hashNotifEDO.get("PERFIL");
    String n1 = "";
    String n2 = "";
    String ca = "";
    // Corregido para que coga los campos del panel (ARS 03-05-01)
    if (perfil.equals("2")) {
      //se comprueban las alarmas de la comunidad autonoma
      ca = app.getCA();
    }
    else if (perfil.equals("3")) {
      ca = app.getCA();
      n1 = pCabecera.txtCodNivel1.getText().trim();
//        n1 = (String)hashNotifEDO.get("CD_NIVEL_1");
    }
    else if (perfil.equals("4")) {
      ca = app.getCA();
      n1 = pCabecera.txtCodNivel1.getText().trim();
      n2 = pCabecera.txtCodNivel2.getText().trim();
//        n1 = (String)hashNotifEDO.get("CD_NIVEL_1");
//        n2 = (String)hashNotifEDO.get("CD_NIVEL_2");
    }

    parNotifAlar par = new parNotifAlar(pCabecera.getCodEnfermedad().trim(),
                                        (String) hashNotifEDO.get("CD_ANOEPI"),
                                        (String) hashNotifEDO.get("CD_SEMEPI"),
                                        n1, n2, ca);

    if (modoOperacion != modoESPERA) {
      Inicializar(modoESPERA);

    }
    barra.setText(res.getString("barra.Text"));
    mira.compruebaIndSuperados(par);
    barra.setText("");
    //----------------------

    //continuo con el protocolo
    //SOLAPAS**** si cd_enfermo se da de alta,
    //no existen solapas de caso -protocolo
    //las abrire ahora, ptt no tengo que grabar el procolo
    if (modoSrv == servletSIN_CD_ENFERMO_INSERT_INSERT_INSERT) {

      if (modoOperacion != modoESPERA) {
        Inicializar(modoESPERA);
      }
      addCaso();
      if (modoOperacion != modoESPERA) {
        Inicializar(modoESPERA);

      }
      addProtocolo(); //MODIFICACION ARTIFICIAL
      Inicializar(modoOperacionBk); //a modificacion

    }
    else {
      //si existia el enfermo y enfermedad, existian las solapas
      //pero al darse el alta, la solapa de PROTOCOLO hay llamar al getnmedo
      //para que absorba el NM_EDO nuevo!!!

      if (modoOperacion != modoESPERA) {
        Inicializar(modoESPERA);

        //EXISTIA PROTOCOLO PARA LA ENFERMEDAD
      }
      if (pProtocolo != null) {
        //actualizar el panelInforme con nm_edo
        pProtocolo.pnl.Cargar_nm_edo( (String) hashNotifEDO.get("NM_EDO"));
        //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
        boolean b = pProtocolo.pnl.ValidarObligatorios();
        if (b) {
          pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo
          //dispose(); //Enrique quiere que se quede abto
          // estaba en la pesta�a de protocolo, continuara alli
        }
        Inicializar(modoOperacionBk);
      }
      //NO EXISTIA PROTOCOLO PARA LA ENFERMEDAD
      else {
        Inicializar(modoOperacionBk);
      }

    }
  } //FIN INSERTAR EDOIND*****************************************

//true si bien
//false si mal y se debe hacer return en MODIFICAR edoind
  public boolean ComprobarOpeFc_modif_Indiv() {

    String sm = "";

    //llamada al servlet de bloqueo
    CLista listaVuelta = new CLista();
    try {

      listaVuelta = Comunicador.Communicate(this.getCApp(),
                                            stubCliente,
                                            servletBLOQUEO_SELECT, //modo
                                            strSERVLET_BLOQUEO, //servlet
                                            listaOpeUltactIndiv);

    }
    catch (Exception e) {
      ShowWarning(res.getString("msg23.Text"));
      //restaurar valores al inicio de funcion:
      //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
      return (false);
    }

    //listaVuelta debe tener tama�o 1//
    DataOpeFc dataVuelta = null;
    for (int i = 0; i < listaVuelta.size(); i++) {
      dataVuelta = (DataOpeFc) listaVuelta.elementAt(i);
    } //---va a ser un solo dato

    if (!dataVuelta.getOK_KO()) { //false, ie EXISTE CAMBIO

      if (dataVuelta.getBORRADA()) { //borrada

        //se trata de edoind
        if (dataVuelta.getTABLA().trim().equals("SIVE_ENFERMO")) {
          sm = res.getString("msg24.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          msgBox = null;
          //DE MOMENTO NO HACEMOS NADA
          return (false);
        }
        //se trata de otra tabla
        else if (dataVuelta.getTABLA().trim().equals("SIVE_EDOIND")) {
          sm = res.getString("msg25.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          msgBox = null;
          //DE MOMENTO NO HACEMOS NADA
          return (false);
        }
      }
      else { //Cambiada pero no borrada
        sm = res.getString("msg26.Text") +
            dataVuelta.getTABLA() + ")" +
            res.getString("msg27.Text");
        msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
        msgBox.show();

        //SI //continuar

        if (!msgBox.getResponse()) { //NO
          //restaurar valores al inicio de funcion:
          //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
          msgBox = null;
          return (false);
        }

      } //Cambiada pero no borrada

    } //EXISTE CAMBIO

    return (true);
  } //fin de ComprobarOpeFc

//true si bien
//false si mal
  public boolean ComprobarOpeFc_Alta_Indiv() {
    //solo controla enfermo
    String sm = "";

    //llamada al servlet de bloqueo
    CLista listaVuelta = new CLista();
    try {

      listaVuelta = Comunicador.Communicate(this.getCApp(),
                                            stubCliente,
                                            servletBLOQUEO_SELECT, //modo
                                            strSERVLET_BLOQUEO, //servlet
                                            listaOpeUltactIndiv);

    }
    catch (Exception e) {
      ShowWarning(res.getString("msg23.Text"));
      //restaurar valores al inicio de funcion:
      //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
      return (false);
    }

    //listaVuelta debe tener tama�o 1//
    DataOpeFc dataVuelta = null;
    for (int i = 0; i < listaVuelta.size(); i++) {
      dataVuelta = (DataOpeFc) listaVuelta.elementAt(i);
    } //---va a ser un solo dato

    if (!dataVuelta.getOK_KO()) { //false, ie EXISTE CAMBIO

      if (dataVuelta.getBORRADA()) { //borrada
        //se trata de enfermo
        if (dataVuelta.getTABLA().trim().equals("SIVE_ENFERMO")) {
          sm = res.getString("msg24.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          msgBox = null;
          //DE MOMENTO NO HACEMOS NADA
          return (false);
        }

      }
      else { //Cambiada pero no borrada
        sm = res.getString("msg28.Text");
        msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
        msgBox.show();

        //SI //continuar

        if (!msgBox.getResponse()) { //NO
          //restaurar valores al inicio de funcion:
          //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
          msgBox = null;
          return (false);
        }

      } //Cambiada pero no borrada

    } //EXISTE CAMBIO

    return (true);
  } //fin de ComprobarOpeFc

//true si bien
//false si mal y se debe hacer return en BORRAR edoind
  public boolean ComprobarOpeFc_borrar_Indiv() {

    String sm = "";
    //lista a enviar: listaOpeUltactTabla +  listaOpeUltactIndiv
    CLista listaEnviar = new CLista();
    CLista listaReservada = new CLista();
    listaReservada = (CLista) hashNotifEDO.get("listaOpeUltactTabla");
    DataOpeFc dataProvisional = null;

    //NOTA:!!!quito e_notif (no procede en el borrado)//3->2
    for (int i = 1; i < listaReservada.size(); i++) {
      dataProvisional = (DataOpeFc) listaReservada.elementAt(i);
      listaEnviar.addElement(dataProvisional);
    } //listaEnviar cargada con la listaOpeUltactTabla excepto e_notif

    String IT_COBERTURA = (String) hashNotifEDO.get("IT_COBERTURA");
    if (IT_COBERTURA.equals("N")) {
      listaEnviar.removeElementAt(0); //quito notif_sem
    }

    //le a�adimos listaOpeUltactIndiv
    for (int i = 0; i < listaOpeUltactIndiv.size(); i++) {
      dataProvisional = (DataOpeFc) listaOpeUltactIndiv.elementAt(i);
      listaEnviar.addElement(dataProvisional);
    }
    //---------------------------------------------------------

    //llamada al servlet de bloqueo
    CLista listaVuelta = new CLista();
    try {

      listaVuelta = Comunicador.Communicate(this.getCApp(),
                                            stubCliente,
                                            servletBLOQUEO_SELECT, //modo
                                            strSERVLET_BLOQUEO, //servlet
                                            listaEnviar);

    }
    catch (Exception e) {
      ShowWarning(res.getString("msg23.Text"));
      //restaurar valores al inicio de funcion:
      //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
      return (false);
    }

    //listaVuelta debe tener tama�o 1//
    DataOpeFc dataVuelta = null;
    for (int i = 0; i < listaVuelta.size(); i++) {
      dataVuelta = (DataOpeFc) listaVuelta.elementAt(i);
    } //---va a ser un solo dato

    if (!dataVuelta.getOK_KO()) { //false, ie EXISTE CAMBIO

      if (dataVuelta.getBORRADA()) { //borrada

        //se trata de edoind
        if (dataVuelta.getTABLA().trim().equals("SIVE_ENFERMO")) {
          sm = res.getString("msg24.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          msgBox = null;
          //DE MOMENTO NO HACEMOS NADA
          return (false);
        }
        //edoindiv
        else if (dataVuelta.getTABLA().trim().equals("SIVE_EDOIND")) {
          sm = res.getString("msg25.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          msgBox = null;
          //DE MOMENTO NO HACEMOS NADA
          return (false);
        }
        else if (dataVuelta.getTABLA().trim().equals("SIVE_E_NOTIF")) {
          sm = res.getString("msg29.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          msgBox = null;
          //DE MOMENTO NO HACEMOS NADA
          return (false);
        }
        else if (dataVuelta.getTABLA().trim().equals("SIVE_NOTIF_SEM")) {
          sm = res.getString("msg30.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          msgBox = null;
          //DE MOMENTO NO HACEMOS NADA
          return (false);
        }
        else if (dataVuelta.getTABLA().trim().equals("SIVE_NOTIFEDO")) {
          sm = res.getString("msg31.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          msgBox = null;
          //DE MOMENTO NO HACEMOS NADA
          return (false);
        }
      }
      else { //Cambiada pero no borrada
        sm = res.getString("msg26.Text") +
            dataVuelta.getTABLA() + ")" +
            res.getString("msg32.Text");
        msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
        msgBox.show();

        //SI //continuar

        if (!msgBox.getResponse()) { //NO
          //restaurar valores al inicio de funcion:
          //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
          msgBox = null;
          return (false);
        }

      } //Cambiada pero no borrada

    } //EXISTE CAMBIO

    return (true);
  } //fin de ComprobarOpeFc

  public void ActListaBloqueo_Modif(CLista listaReserva,
                                    boolean bActualizar,
                                    String sFechaInsertada) {
    int iTamLista = 0;
    iTamLista = listaOpeUltactIndiv.size();

    //ACTUALIZAMOS listaOpeUltactIndiv con los datos que hay en la tabla!!!
    //para que PUEDA seguir MODIFICANDO
    DataOpeFc dataReservaEnfermo = null;
    DataOpeFc dataReservaEdoind = null;

    dataReservaEnfermo = (DataOpeFc) listaReserva.elementAt(0);
    dataReservaEdoind = (DataOpeFc) listaReserva.elementAt(1);

    if (!bActualizar) { //volver a valores antiguos
      if (listaOpeUltactIndiv.size() > 0) {
        listaOpeUltactIndiv.removeAllElements();
      }
      listaOpeUltactIndiv.addElement(dataReservaEnfermo);
      listaOpeUltactIndiv.addElement(dataReservaEdoind);
    }
    else {

      if (iTamLista == 1) { //modifique solo edoind
        if (listaOpeUltactIndiv.size() > 0) {
          listaOpeUltactIndiv.removeAllElements();
          //primer data, enfermo ANTIGUO
        }
        listaOpeUltactIndiv.addElement(dataReservaEnfermo);
        //segundo data, edoind NUEVO
        DataOpeFc datosedoind;
        Vector vedoind = new Vector();
        DataOpeFc datavedoind = null;
        datavedoind = new DataOpeFc("NM_EDO",
                                    (String) hashNotifEDO.get("NM_EDO"), "N");
        vedoind.addElement(datavedoind);

        datavedoind = new DataOpeFc(this.getCApp().getLogin(),
                                    sFechaInsertada,
                                    "SIVE_EDOIND", vedoind);

        listaOpeUltactIndiv.addElement(datavedoind);
      }

      else if (iTamLista == 2) { //modifico enfermo + edoind
        if (listaOpeUltactIndiv.size() > 0) {
          listaOpeUltactIndiv.removeAllElements();
          //primer data, enfermo NUEVO
        }
        DataOpeFc datosenfermo;
        Vector venfermo = new Vector();
        DataOpeFc datavenfermo = new DataOpeFc("CD_ENFERMO",
                                               pCabecera.txtCodEnfermo.getText().
                                               trim(), "N");
        venfermo.addElement(datavenfermo);

        datosenfermo = new DataOpeFc(this.getCApp().getLogin(),
                                     sFechaInsertada,
                                     "SIVE_ENFERMO", venfermo);

        listaOpeUltactIndiv.addElement(datosenfermo);
        //segundo data, edoind NUEVO
        DataOpeFc datosedoind;
        Vector vedoind = new Vector();
        DataOpeFc datavedoind = null;
        datavedoind = new DataOpeFc("NM_EDO",
                                    (String) hashNotifEDO.get("NM_EDO"), "N");
        vedoind.addElement(datavedoind);

        datavedoind = new DataOpeFc(this.getCApp().getLogin(),
                                    sFechaInsertada,
                                    "SIVE_EDOIND", vedoind);

        listaOpeUltactIndiv.addElement(datavedoind);
      }
    }

  } //fin ActListaBloqueo

//MODIFICAR EDOIND**************
  private void modificarEDOInd() {

    //Reserva de la listaOpeUltactIndiv
    CLista listaReservada = new CLista(); //lleva enfermo, edoind
    DataOpeFc dataProvisional = null;
    for (int i = 0; i < listaOpeUltactIndiv.size(); i++) {
      dataProvisional = (DataOpeFc) listaOpeUltactIndiv.elementAt(i);
      listaReservada.addElement(dataProvisional);
    } //----------------------------------

    //Si esta informado, estamos modificand
    DataEnfermoBD dataEnfermoBD = (DataEnfermoBD) pCabecera.getEnfermoBD();

    String sFecha_Actual = "";

    int iCargarDataEnfermodb = 0;

    //se entra por espera
    //se debe inicilaizar al salir (modobk o modificar)
    parametros = new CLista();

    String sSiglas = "";
    String sApe1 = "", sApe2 = "", sNombre = "";
    String sApe1T = "", sApe2T = "", sNombreT = "";
    String sItCalc = "";

    if (pCabecera.lTipoFechaNac.getText().trim().equals("real")) {
      sItCalc = "N";
    }
    else {
      sItCalc = "S";

      //enfermo NO confidencial
    }
    if (pCabecera.getFlagEnfermo()) {

      //fonetica
      sApe1 = pCabecera.txtApe1.getText().trim();
      sApe2 = pCabecera.txtApe2.getText().trim();
      sNombre = pCabecera.txtNombre.getText().trim();
      sApe1T = comun.traduccionFonetica(pCabecera.txtApe1.getText().trim());
      sApe2T = comun.traduccionFonetica(pCabecera.txtApe2.getText().trim());
      sNombreT = comun.traduccionFonetica(pCabecera.txtNombre.getText().trim());

      //siglas
      sSiglas = Calcular_Siglas(pCabecera.getApe1(),
                                pCabecera.getApe2(),
                                pCabecera.getNombre());
    }

    //enfermo confidencial
    else {
      //CAMBIA****
      //ape1 va siglas y no se modifica (de la BD directamente)
      sApe1 = dataEnfermoBD.getDS_APE1();
      sApe2 = dataEnfermoBD.getDS_APE2();
      sNombre = dataEnfermoBD.getDS_NOMBRE();
      sApe1T = dataEnfermoBD.getDS_FONOAPE1();
      sApe2T = dataEnfermoBD.getDS_FONOAPE2();
      sNombreT = dataEnfermoBD.getDS_FONONOMBRE();
      sSiglas = dataEnfermoBD.getSIGLAS();

    } //CONFIDENCIALIDAD********************  (AUN NO SE LLAMA A MODO MODIF ENFERMO)

    //lo guardamos en una estructura
    DataApesFonosCalcSiglas datosApesFonosCalcSiglas = new
        DataApesFonosCalcSiglas
        (sApe1, sApe2, sNombre, sApe1T, sApe2T, sNombreT, sItCalc, sSiglas);

    //MODIFICACION------
    DataTab datatab = new DataTab();

    //enfermo lo modificamos???
    boolean bMover = false;
    bMover = Cambio_de_Enfermo();
    if (bMover) {
      datatab.insert("IT_MOV_ENFERMO", "S");
      iCargarDataEnfermodb = 2;
    }
    else {
      datatab.insert("IT_MOV_ENFERMO", "N");
      iCargarDataEnfermodb = 0;
      listaOpeUltactIndiv.removeElementAt(0); //quito comprobar enfermo
    }

    //CONTROL BLOQUEO
    if (!ComprobarOpeFc_modif_Indiv()) {
      ActListaBloqueo_Modif(listaReservada, false, ""); //carga la reserva
      Inicializar(modoMODIFICACION);
      return;
    } //---------------------------

    datatab = Estructura_Quizas_Enfermo_ModifCaso(datatab,
                                                  datosApesFonosCalcSiglas);

    parametros.addElement(datatab);

    // en el primer elemento de par�metros enviamos el declarante1
    // se comprueba si existen m�s, y, en este caso, se env�an
    if (pDeclarantes.tbl.countItems() > 1) {

      for (int i = 1; i < pDeclarantes.tbl.countItems(); i++) {

        datatab = new DataTab();

        datatab.insert("CD_E_NOTIF", pDeclarantes.getCodEquipo(i));
        datatab.insert("CD_ANOEPI_NOTIF_EDOI", pDeclarantes.getAnno(i));
        datatab.insert("CD_SEMEPI_NOTIF_EDOI", pDeclarantes.getSemana(i));
        datatab.insert("FC_RECEP_NOTIF_EDOI", pDeclarantes.getFechaRecep(i));

        datatab.insert("DS_DECLARANTE", pDeclarantes.getDeclarante(i));
        datatab.insert("IT_PRIMERO", pDeclarantes.getItPrimero(i));
        datatab.insert("CD_OPE_NOTIF_EDOI", pDeclarantes.getCdOpe(i));
        datatab.insert("FC_ULTACT_NOTIF_EDOI", pDeclarantes.getFcUltAct(i));
        datatab.insert("FC_FECNOTIF_NOTIF_EDOI", pDeclarantes.getFechaNotif(i));

        parametros.addElement(datatab);
      }

    }
    //************************************************************************************************************
     //caso 5:(PARALELO AL 3) CD_ENFERMo informado Y SUS DATOS.
     //           NM_EDO  informado
     //         , modifico edoind , borro sus notif_edoi y la vuelvo a cargar
     //ESTRUCTURAenfermo  DE LA LISTA:
     //primer registro :
     //(incluye Cod  de enfermo Y SUS DATOS), datos de caso (incluye nm_edo ), datos del  declarante
     //siguientes registros:
     //datos de los siguientes declarantes
     //listaSalida --> DataEnfCaso con CodEnf, y Num_Edo (strings)

     //Si lista vacia, no puede ser (todas tablas tienen campos obligatorios), no hace nada
     /////servletCON_CD_ENFERMO_CON_NM_EDO_MODIF_MODIF_BUCLE = 4; /////
     //************************************************************************************************************

      try {

        CLista resultado = new CLista();
        parametros.setLogin(app.getLogin());
        parametros.setLortad(app.getParameter("LORTAD"));

        resultado = Comunicador.Communicate(this.getCApp(),
                                            stubCliente,
            servletCON_CD_ENFERMO_CON_NM_EDO_MODIF_MODIF_BUCLE,
            strSERVLET_INDIV_IM,
            parametros);

        DataEnfCaso dataEnf = (DataEnfCaso) resultado.firstElement();
        sFecha_Actual = dataEnf.getFC_ULTACT();

        // la modificaci�n se ha efectuado correctamente
        iOut = 0;

        // si todo ha ido bien, se muestra un mensaje
        String msg = "";
        if (bMover) {
          msg = res.getString("msg33.Text");
        }
        else {
          msg = res.getString("msg34.Text");
          //14/09/99 quitar msg Enrique
          //$$ShowWarning(msg);

        }
        if (!bCabeceraAceptada) {
          bCabeceraAceptada = true;
        }
        bModificacionArtificial = false;

        //ACTUALIZAMOS el dataEnfermodb******************
        String n1 = "", n2 = "", zbs = "";
        if (iCargarDataEnfermodb == 2) { //modif, pero estos no se cambian
          n1 = dataEnfermoBD.getCD_NIVEL_1();
          n2 = dataEnfermoBD.getCD_NIVEL_2();
          zbs = dataEnfermoBD.getCD_ZBS();
        }
        if (iCargarDataEnfermodb == 2) {
          dataEnfermoBD = new DataEnfermoBD(
              pCabecera.txtCodEnfermo.getText().trim(),
              pCabecera.getCodTipoDocumento(),
              sApe1, sApe2, sNombre,
              sApe1T, sApe2T, sNombreT,
              sItCalc,
              pCabecera.getFechaNac(),
              pCabecera.getCodSexo(),
              pCabecera.txtTelefono.getText().trim(),
              n1, n2, zbs,
              this.getCApp().getLogin(),
              sFecha_Actual,
              pCabecera.txtDocumento.getText().trim(),
              sSiglas);

          pCabecera.Cargar_DataEnfermoBD(dataEnfermoBD);
        }
        //*******************dataenfermobd***********************

         ActListaBloqueo_Modif(listaReservada, true, sFecha_Actual); //carga lo de BD

      }
      catch (Exception e) {
        iOut = -1;
        ShowWarning(res.getString("msg35.Text"));
        ActListaBloqueo_Modif(listaReservada, false, ""); //carga la reserva
        bModificacionArtificial = false;
        Inicializar(modoOperacionBk); //sigue en modificar
        return;
      }

    //continuo con el protocolo

    if (modoOperacion != modoESPERA) {
      Inicializar(modoESPERA);

      //EXISTIA PROTOCOLO PARA LA ENFERMEDAD
    }
    if (pProtocolo != null) {
      //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
      boolean b = pProtocolo.pnl.ValidarObligatorios();
      if (b) {
        pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo
        //dispose(); //Enrique quiere que se quede abto
        // estaba en la pesta�a de protocolo, continuara alli
      }
      Inicializar(modoOperacionBk);
    }
    //NO EXISTIA PROTOCOLO PARA LA ENFERMEDAD
    else {
      Inicializar(modoOperacionBk);
    }

  } //MODIFICAR *******************************************************

  // m�todo para borrar la EDO Individualizada
  // se borra al mismo tiempo EDO Ind. + datos protocolo (si tiene)
  private void borrarEDOInd() {

    //se entra en espera
    //DEBEMOS SALIR Y LLAMAR A INICIALIZAR EN EL MODO ADECUADO
    DataBorrarIndiv dataBorrar = new DataBorrarIndiv(
        (String) hashNotifEDO.get("NM_EDO"),
        pCabecera.txtCodEnfermo.getText().trim(),
        this.getCApp().getLogin());

    parametros = new CLista();
    parametros.addElement(dataBorrar);

    if (!ComprobarOpeFc_borrar_Indiv()) {
      Inicializar(modoOperacionBk); //debe ser modoBAJA
      return;
    }

    try {

      parametros.setLogin(app.getLogin());
      parametros.setLortad(app.getParameter("LORTAD"));

      Comunicador.Communicate(this.getCApp(),
                              stubCliente,
                              servletBORRAR_INDIVIDUALIZADA,
                              strSERVLET_INDIV_B,
                              parametros);

      // la baja se ha efectuado correctamente
      // si todo ha ido bien, se muestra un mensaje
      //cerramos el dialogo
      iOut = 0;
      //14/09/99 quitar msg Enrique
      //$$ShowWarning("EDO Individualizada borrada");
      dispose();
      //NO INICIALIZAMOS. NO HACE FALTA!!!!

    }
    catch (Exception e) {
      // la baja no se ha efectuado
      iOut = -1;
      ShowWarning(res.getString("msg36.Text"));
      Inicializar(modoOperacionBk); //debe ser modoBAJA

    }
  }

  // se ejecuta en respuesta al actionPerformed sobre btnCancelar
  void btnCancelar_actionPerformed() {

    pCabecera.bSalir_del_dialogo = true; //para que no se lanzen lostfocus
    // puede que se salga por cancelar desde el protocolo, pero
    // haber dado el alta / baja / m}}      od. correctamente
    //iOut = -1;
    dispose();
  }

  // se comprueba que, antes de dar alta o modificaci�n,
  // todos los datos est�n rellenos y son correctos
  // los datos que son obligatorios son:
  // solapa "Cabecera": NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI,
  //    FC_RECEP, FC_FECNOTIF, DS_DECLARANTE, CD_OPE, FC_ULTACT
  //    para la tabla SIVE_NOTIF_EDOI
  // NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, CD_ENFERMO, CD_OPE,
  //    FC_ULTACT, CD_ENFCIE para la tabla SIVE_EDOIND

  // JRM: Hace obligatorio zona de riesgo y provincia y municipio
  public boolean isDataValid() {

    try {
      // Ya tenemos los datos CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI,
      //    FC_RECEP, FC_FECNOTIF, CD_OPE, FC_ULTACT del maestro/detalle

      // se comprueba que existe por los menos un declarante
      if (pDeclarantes.tbl.countItems() == 0) {
        ShowWarning(res.getString("msg37.Text"));
        pDeclarantes.requestFocus();
        pDeclarantes.txtDeclarante.select(0,
                                          pDeclarantes.txtDeclarante.getText().length());
        return false;
      }

      // Apellido1
      if (pCabecera.txtApe1.getText().trim().equals("")) {
        ShowWarning(res.getString("msg38.Text"));
        pCabecera.requestFocus();
        pCabecera.txtApe1.select(0, pCabecera.txtApe1.getText().length());
        return false;
      }
      // se comprueba que se ha introducido la enfermedad
      if ( (pCabecera.getCodEnfermedad() == null) ||
          (pCabecera.getCodEnfermedad().equals(""))) {
        ShowWarning(res.getString("msg39.Text"));
        pCabecera.requestFocus();
        return false;
      }

      //JRM: Actualizamos zona de riesgo en Madrid
      if (pCabecera.panSuca.getModoTramero()) {
        // Si el usuario no ha introducido zona de salud.
        if ( ( (!pCabecera.bNivel1Valid) &&
              (!pCabecera.bNivel2Valid) &&
              (!pCabecera.bNivelZBSValid))
            ) {
          ShowWarning(res.getString("msg103.Text"));
          actZonaRiesgo();
        }
      }

      // JRM: Hacemos obligatorio la zona de riesgo
      if ( ( (!pCabecera.bNivel1Valid) ||
            (!pCabecera.bNivel2Valid) ||
            (!pCabecera.bNivelZBSValid)) ||
          ( (pCabecera.txtCodNivel1.getText().trim().equals("")) ||
           (pCabecera.txtCodNivel2.getText().trim().equals("")) ||
           (pCabecera.txtCodZBS.getText().trim().equals("")))
          ) {
        ShowWarning(res.getString("msg100.Text"));
        return false;
      }

      // JRM: Hacemos que la provincia sea obligatoria
      if (pCabecera.panSuca.getCD_PROV().trim().equals("")) {
        ShowWarning(res.getString("msg101.Text"));
        return false;

      }

      // JRM: Hacemos que el municipio sea obligatoria
      if (pCabecera.panSuca.getCD_MUN().trim().equals("")) {
        ShowWarning(res.getString("msg102.Text"));
        return false;

      }

      // se comprueba que la fecha de recpeci�n es correcta
      if ( ( ( (String) hashNotifEDO.get("FC_RECEP")).trim().equals("")) ||
          ( (String) hashNotifEDO.get("FC_RECEP") == null)) {
        ShowWarning(res.getString("msg40.Text"));
        return false;
      }

      // se comprueba que la fecha de notificaci�n es correcta
      if ( ( ( (String) hashNotifEDO.get("FC_FECNOTIF")).trim().equals("")) ||
          ( (String) hashNotifEDO.get("FC_FECNOTIF") == null)) {
        ShowWarning(res.getString("msg41.Text"));
        return false;
      }

      // se comprueba que el a�o est� relleno
      if ( ( ( (String) hashNotifEDO.get("CD_ANOEPI")).trim().equals("")) ||
          ( (String) hashNotifEDO.get("CD_ANOEPI") == null)) {
        ShowWarning(res.getString("msg42.Text"));
        return false;
      }

      // se comprueba que la semana est� rellena
      if ( ( ( (String) hashNotifEDO.get("CD_SEMEPI")).trim().equals("")) ||
          ( (String) hashNotifEDO.get("CD_SEMEPI") == null)) {
        ShowWarning(res.getString("msg43.Text"));
        return false;
      }

      // se diferencia entre el modo ALTA y el modo MODIFICACI�N
      // modo MODIFICACI�N: deben existir NM_EDO ,CD_ENFERMO
      // modo ALTA: CD_ENFERMO se puede haber obtenido
      //            si no existe, lanzar query para obtenerlo
      //            NM_EDO no existe, lanzar query para obtenerlo
      switch (modoOperacionBk) {
        case modoMODIFICACION:
          if ( ( ( (String) hashNotifEDO.get("NM_EDO")).trim().equals("")) ||
              ( (String) hashNotifEDO.get("NM_EDO") == null)) {
            ShowWarning(res.getString("msg44.Text"));
            return false;
          }

          if ( (pCabecera.txtCodEnfermo.getText().trim().equals("")) ||
              (pCabecera.txtCodEnfermo.getText() == null)) {
            ShowWarning(res.getString("msg45.Text"));
            return false;
          }

          break;

        case modoALTA:
          break;

      }
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  } //FIN DE ISDATAVALID***********************************************************

// ARG: Comprueba si las fechas tienen formato correcto
  public boolean isFechasValid() {
    // Cabecera -> Fecha de nacimiento
    pCabecera.txtFechaNac.ValidarFecha();
    if (!pCabecera.txtFechaNac.getValid().equals("S")) {
      return false;
    }
    // Caso -> Fecha inicio sintoma
    if (pCaso != null) {
      pCaso.CfechaIniSint.ValidarFecha();
      if (!pCaso.CfechaIniSint.getValid().equals("S")) {
        return false;
      }
    }
    return true;
  }

// retorna el modo de salida
  public int getModoBk() {
    return modoOperacionBk;
  }

  // retorna una clista con un elemento de tipo DataListaEDOIndiv
  // (se recupera desde PanelListaEDOIndiv para modificar / a�adir
  // la lista de EDO Individualizadas).
  public Object getComponente() {
    Object o = null;

    if (iOut != -1) {
      CLista lista = new CLista();
      String sSiglas = Calcular_Siglas(pCabecera.getApe1(),
                                       pCabecera.getApe2(),
                                       pCabecera.getNombre());

      // si se ha llegado aqu�, es que el ape1 existe, y por tanto,
      // Calcular_Siglas(...) nunca devolver� un null
      lista.addElement(new DataListaEDOIndiv(
          (String) hashNotifEDO.get("CD_E_NOTIF"),
          (String) hashNotifEDO.get("CD_ANOEPI"),
          (String) hashNotifEDO.get("CD_SEMEPI"),
          (String) hashNotifEDO.get("FC_RECEP"),
          (String) hashNotifEDO.get("FC_FECNOTIF"),
          (String) hashNotifEDO.get("NM_EDO"),
          pCabecera.getCodEnfermedad(),
          pCabecera.getDesEnfermedad(),
          pCabecera.getCodEnfermo(),
          sSiglas,
          pCabecera.getFechaNac()));

      o = lista.elementAt(0);

    }

    return o;
  }

  // actualiza listaPaises
  public CLista getListaPanelCabecera() {
    return listaPanelCabecera;
  }

  // actualiza el contenido de las listas del servet
  public void setListaPanelCabecera(CLista list) {
    listaPanelCabecera = list;
  }

  // actualiza listaPaises
  public CLista getListaPanelCaso() {
    return listaPanelCaso;
  }

  // actualiza el contenido de las listas del servet
  public void setListaPanelCaso(CLista list) {
    listaPanelCaso = list;
  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  void tabPanel_mouseClicked(MouseEvent e) {

    String sSolapa = tabPanel.getSolapaActual();

    if ( (sSolapa.equals("Cabecera")) || (sSolapa.equals("Caso"))
        || (sSolapa.equals("Declarantes"))) {
      btnAceptar.setEnabled(!bCabeceraAceptada);
    }
    else if (sSolapa.equals("Protocolo")) {
      btnAceptar.setEnabled(bCabeceraAceptada);
    }
  }

//RETURN CLista = null: todo esta correcto
//RETURN CLista.size() = 1, con DataLongValid(panel, txt, limite): ERROR en ese campo
  public CLista isLongValid() {
    CLista lista = null;
    lista = new CLista();

    if (pCabecera.txtCodEnfermo.getText().trim().length() > 6) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             pCabecera.txtCodEnfermo, "6",
                                             "c�digo del enfermo");
      lista.addElement(data);
      return (lista);
    }

    if (pCabecera.txtApe1.getText().trim().length() > 20) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             pCabecera.txtApe1, "20",
                                             res.getString("msg46.Text"));
      lista.addElement(data);
      return (lista);
    }
    if (pCabecera.txtApe2.getText().trim().length() > 30) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             pCabecera.txtApe2, "30",
                                             res.getString("msg47.Text"));
      lista.addElement(data);
      return (lista);
    }
    if (pCabecera.txtNombre.getText().trim().length() > 30) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             pCabecera.txtNombre, "30",
                                             res.getString("msg48.Text"));
      lista.addElement(data);
      return (lista);
    }
    if (pCabecera.txtTelefono.getText().trim().length() > 14) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             pCabecera.txtTelefono, "14",
                                             res.getString("msg49.Text"));
      lista.addElement(data);
      return (lista);
    }
// suca
    if (pCabecera.panSuca.getCD_POSTAL().length() > 5) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             new TextField(), "5",
                                             res.getString("msg50.Text"));
      lista.addElement(data);
      return (lista);
    }
    if (pCabecera.panSuca.getDS_DIREC().length() > 40) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             new TextField(), "40",
                                             res.getString("msg51.Text"));
      lista.addElement(data);
      return (lista);
    }
    if (pCabecera.panSuca.getDS_NUM().length() > 5) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             new TextField(), "5",
                                             res.getString("msg52.Text"));
      lista.addElement(data);
      return (lista);
    }
    if (pCabecera.panSuca.getDS_PISO().length() > 5) {
      DataLongValid data = new DataLongValid("pCabecera", pCabecera,
                                             new TextField(), "5",
                                             res.getString("msg53.Text"));
      lista.addElement(data);
      return (lista);
    }

//mas
//creo que los de los choices no se deben validar CDTVIA, CDVIAL, CDTNUM, DSCALNUM
//tampoco pais, ca, provincia y municipio

    //puede que pcaso no este abta!!!
    if (pCaso != null) {
      if (pCaso.txtAsociado.getText().trim().length() > 20) {
        DataLongValid data = new DataLongValid("pCaso", pCaso,
                                               pCaso.txtAsociado, "20",
                                               res.getString("msg54.Text"));
        lista.addElement(data);
        return (lista);
      }

      if (pCaso.txtColectivo.getText().trim().length() > 40) {
        DataLongValid data = new DataLongValid("pCaso", pCaso,
                                               pCaso.txtColectivo, "40",
                                               res.getString("msg55.Text"));
        lista.addElement(data);
        return (lista);
      }

      if (pCaso.txtCentro.getText().trim().length() > 40) {
        DataLongValid data = new DataLongValid("pCaso", pCaso,
                                               pCaso.txtCentro, "40",
                                               res.getString("msg56.Text"));
        lista.addElement(data);
        return (lista);
      }
      if (pCaso.txtDgOtros.getText().trim().length() > 40) {
        DataLongValid data = new DataLongValid("pCaso", pCaso,
                                               pCaso.txtDgOtros, "40",
                                               res.getString("msg57.Text"));
        lista.addElement(data);
        return (lista);
      }
    } //fin de pCaso abto
    lista = null;
    return (lista);

  } //fin isLongValid

//Return: null porque algun campo ES DE LONG CERO, SIENDO OBLIGATORIO
//PREVIAMENTE, LLAMAR A ISLONGVALID()
//NO ENVIAR NULOS
  public String Calcular_Siglas(String ape1,
                                String ape2,
                                String nombre) {
    String Siglas = "";
    //Apellido1 OBLIGATORIO
    if (ape1.length() == 0) {
      Siglas = null;
      return (Siglas);
    }
    else {

      Siglas = ape1.substring(0, 1);

      //APELLIDO2
      if (ape2.length() > 0) {
        Siglas = Siglas + ape2.substring(0, 1);
      }

      //NOMBRE
      if (nombre.length() > 0) {
        Siglas = Siglas + nombre.substring(0, 1);
      }
      return (Siglas.toUpperCase());
    } //else

  } //fin de funcion

  /**
   * Actualiza la zona de riesgo cuando estamos con el control de SUCA.
   * Utiliza el servlet que usabamos en la gneraci�n de los informes para
   * hacer la consulta que es lo m�s c�modo y m�s r�pido de implementar.
   *
   * @author  JRM
   * @version 13/01/2001
   */
  public void actZonaRiesgo() {
    if (pCabecera.panSuca.getModoTramero()) {
      String strSelect;
      CLista param = new CLista();
      CLista lista = null;
      StubSrvBD stub = new StubSrvBD();
      Vector vDatos;
      DataInforme datosZona;
      // Cuando el panel SUCA no da DSCALNUM es que este es 0.
      String DSCALNUM = "0";

      if (!pCabecera.panSuca.getDSCALNUM().trim().equals("")) {
        DSCALNUM = pCabecera.panSuca.getDSCALNUM();
      }

      strSelect = "SELECT substr(zo.CDZONA,1,2) as CD_AREA, " +
          "substr(zo.CDZONA,3,2) as CD_DIS, " +
          "substr(zo.CDZONA,5,2) as CD_ZONA " +
          "FROM SUCA_PORTAL_ZONA zo " +
          "WHERE zo.CDMUNI     = '" + pCabecera.panSuca.getCD_MUN() + "' and " +
          "      zo.CDVIAL     = '" + pCabecera.panSuca.getCDVIAL() + "' and " +
          "      zo.CDTPNUM    = '" + pCabecera.panSuca.getCDTNUM() + "' and " +
          "      zo.DSNMPORTAL = '" + pCabecera.panSuca.getDS_NUM() + "' and " +
          "      zo.DSCALNUM   = '" + DSCALNUM + "' and " +
          "      zo.CDTPZONA   = '2' ";

      param.addElement(strSelect);
      param.addElement(new Integer(3));

      try {
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + "servlet/SrvInformes"));
        lista = (CLista) stub.doPost(0, param);

//        SrvInformes srv = new SrvInformes();
//        srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
//                             "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
//                             "dba_edo",
//                             "manager");
//        lista = srv.doDebug(0, param);

        // Si no hay datos no actualizamos nada de pantalla.
        if (lista.size() != 0) {
          // El vector que estamos recogiendo es una lista de objetos
          // DataInforme, nos quedamos con el primer elemento que es el
          // que nos interesa
          vDatos = (Vector) lista.elementAt(0);
          if (vDatos.size() != 0) {
            datosZona = (DataInforme) vDatos.elementAt(0);
            // Escribir datos y buscar descripciones
            pCabecera.txtCodNivel1.setText(datosZona.C1);
            pCabecera.txtCodNivel1_focusLost();
            pCabecera.txtCodNivel2.setText(datosZona.C2);
            pCabecera.txtCodNivel2_focusLost();
            pCabecera.txtCodZBS.setText(datosZona.C3);
            pCabecera.txtCodZBS_focusLost();
            /*
                         pCabecera.bNivel1Valid    = true;
                         pCabecera.bNivel2Valid    = true;
                         pCabecera.bNivelZBSValid  = true;
             */
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        CMessage msgBox = new CMessage(this.app, CMessage.msgERROR,
                                       e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  } // actZonaRiesgo

} //FIN CLASE

class DialogListaEDOIndivactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialogListaEDOIndiv adaptee;
  ActionEvent evt;

  DialogListaEDOIndivactionAdapter(DialogListaEDOIndiv adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("GenListado")) {
      adaptee.btnListado_actionPerformed(evt);
    }
  }
} //fin class

class DialogListaEDOIndiv_tabPanel_mouseAdapter
    extends java.awt.event.MouseAdapter {
  DialogListaEDOIndiv adaptee;

  DialogListaEDOIndiv_tabPanel_mouseAdapter(DialogListaEDOIndiv adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseClicked(MouseEvent e) {
    adaptee.tabPanel_mouseClicked(e);
  }

  public void mousePressed(MouseEvent e) {
    adaptee.tabPanel_mouseClicked(e);
  }
}

class DataApesFonosCalcSiglas {

  public String APE1 = "";
  public String APE2 = "";
  public String NOMBRE = "";
  public String APE1T = "";
  public String APE2T = "";
  public String NOMBRET = "";
  public String ITCALC = "";
  public String SIGLAS = "";

  public DataApesFonosCalcSiglas(String ape1, String ape2, String nombre,
                                 String ape1T, String ape2T, String nombreT,
                                 String itcalc, String siglas) {
    APE1 = ape1;
    APE2 = ape2;
    NOMBRE = nombre;
    APE1T = ape1T;
    APE2T = ape2T;
    NOMBRET = nombreT;
    ITCALC = itcalc;
    SIGLAS = siglas;

  }

  public String getAPE1() {
    return APE1;
  }

  public String getAPE2() {
    return APE2;
  }

  public String getNOMBRE() {
    return NOMBRE;
  }

  public String getAPE1T() {
    return APE1T;
  }

  public String getAPE2T() {
    return APE2T;
  }

  public String getNOMBRET() {
    return NOMBRET;
  }

  public String getITCALC() {
    return ITCALC;
  }

  public String getSIGLAS() {
    return SIGLAS;
  }
}
