package obj;

//AIC
import java.util.Calendar;

import java.awt.Color;
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

/*Si se pasa como obligatorio, se pintara amarilo*/
public class CFechaSimple
    extends TextField {

  //datos de entrada
  public String sOblig;

  protected String sValidaSN = "N";
  protected String sFechaAlmacen = ""; //si fechaOK, almacena la fecha
  protected String sStringAntes = "";
  protected String sStringDespues = "";
  protected int iDia = 0;
  protected int iMes = 0;
  protected int iAnyo = 0;

  //constructor
  public CFechaSimple(String sOBLIGATORIO) {
    super();

    //parametros de entrada
    sOblig = sOBLIGATORIO;

    //Color
    if (sOblig.equals("S")) {
      setBackground(new Color(255, 255, 150));

    }
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  } //end Constructor

//jbInit*****************************************
  private void jbInit() throws Exception {

    //ESCRIBIR
    this.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        txt_keyPressed(e);
      }
    });

    //CHANGE
    this.addTextListener(new java.awt.event.TextListener() {
      public void textValueChanged(TextEvent e) {
        txt_textValueChanged(e);
      }
    });

  } //fin jbnit

//-------------------------------------------------------------------------
//uOrden = 1: dia/mes/a�o � uOrden = 2: mes/dia/a�o  (01/02/97)(01/02/8)
  protected boolean ChequearFecha(String sCad, int uOrden) {

    boolean b = true;
    String sCadena = "";
    int i = 0;
    String j1, j2, aa, bb, cc, i1 = "";
    int x1, x2 = 0;
    String dd = "";
    String mm = "";
    String yy = "";

    String ch = "";
    String tt = "";

    int bisiesto = 0;
    int alerta = 0;

    int total = 0;
    int resto4 = 0;
    int resto100 = 0;
    int resto400 = 0;

    sCadena = sCad;

    try {

      if (sCad.equals("")) {
        b = false;
      }

      if (!sCad.equals("")) {
        //SEPARAMOS LOS DATOS
        x1 = sCadena.indexOf("/", 0);
        x2 = sCadena.indexOf("/", x1 + 1);
        if (x1 == -1 || x2 == -1) {
          b = false; //no se encontraron las barras
        }
        else {
          aa = sCadena.substring(0, x1);
          bb = sCadena.substring(x1 + 1, x2);
          cc = sCadena.substring(x2 + 1, sCadena.length());

          if (aa.equals("") || bb.equals("") || cc.equals("")) {
            b = false; //algun bloque falta
          }
          else {

            //deletepot del a�o.
            for (i = 0; i < cc.length(); ++i) {
              if ( (ch = cc.substring(i, i + 1)) != ".") {
                tt = tt + ch;
              }
              if ( (ch = cc.substring(i, i + 1)) == ".") {
                tt = tt;
              }
            }
            //FORMATO dia/mes/a�o � mes/dia/a�o
            if (uOrden == 1) {
              dd = aa;
              mm = bb;
              yy = tt;
            }
            else if (uOrden == 2) {
              dd = bb;
              mm = aa;
              yy = tt;
            }
            else {
              //NO DEBE DARSE UN ORDEN DISTINTO DE 1 o 2
              b = false;
            }

            //COMPROBAR QUE SON NUMEROS y su rango.
            if ( (ChequearEntero(dd, 1, 31, 2) != true) ||
                (ChequearEntero(mm, 1, 12, 2) != true) ||
                (ChequearEntero(tt, 0, 10000, 2) != true)) {
              b = false;
            }

            //RESPECTO A LOS MESES Y DIAS
            //desechamos largos y 0. FORMATEAMOS A DOS CIFRAS, y
            if (mm.length() > 2 || dd.length() > 2
                || dd.equals("00") || mm.equals("00")) {
              b = false;
            }
            if (mm.length() == 1) {
              mm = "0" + mm;
            }
            if (dd.length() == 1) {
              dd = "0" + dd;
            }

            //descomponer los dias
            j1 = dd.substring(0, 1);
            j2 = dd.substring(1, 2);

            //dentro de cada mes
            if (mm.equals("02")) {
              if (j1.equals("3")) {
                b = false;
              }
              else if (j1.equals("2")
                       && j2.equals("9")) {
                alerta = 1;
              }
            }
            else if (mm.equals("04") || mm.equals("06") || mm.equals("09") ||
                     mm.equals("11")) {
              if (j1.equals("3") && !j2.equals("0")) {
                b = false;
              }
            }
            //sacar el a�o con 4 cifras=total.
            if (yy.length() > 4 || yy.length() == 3 || yy.length() == 0) {
              b = false;
            }
            else if (yy.length() == 2) {
              //leo el 1� caracter
              i1 = yy.substring(0, 1);

              if (i1.equals("7") || i1.equals("8") || i1.equals("9")) {
                yy = "19" + yy;
              }
              else {
                yy = "20" + yy;
              }
            } //fin elseif

            else if (yy.length() == 1) {
              yy = "200" + yy;
            }

            //a�o

            Integer Itotal = new Integer(yy);
            total = Itotal.intValue();

            resto4 = total % 4;
            resto100 = total % 100;
            resto400 = total % 400;

            //ESTUDIO DE BISIESTO
            //divisible entre 4
            if (resto4 == 0) {
              //no divis. entre 100
              if (resto100 != 0) {
                bisiesto = 1;
              }
              //si div. entre 100
              else {
                //div. entre 400
                if (resto400 == 0) {
                  bisiesto = 1;
                }
              }
            } //fin if

            //A�O BISIESTO
            if (alerta == 1 && bisiesto == 0) {
              b = false;
            }

          } //fin else aa, bb, cc
        } //fin del else (no barras)
      } //fin de scad

      //FIN DE FUNCION
      if (!b) { //si mal
        sFechaAlmacen = "";
      }
      else {
        sFechaAlmacen = dd + "/" + mm + "/" + yy;
        //AIC
        iDia = Integer.parseInt(dd);
        iMes = Integer.parseInt(mm);
        iAnyo = Integer.parseInt(yy);

      }

    }
    catch (Exception e) {
      e.printStackTrace();
      b = false;
      sFechaAlmacen = "";
    }
    return (b);
  } //fin funcion--------------------------------------

  /* Salida:  -1  Esta borrando  o es igual
              i   El lugar donde esta a�adiendo (0,...)
      Ej: XBC --> XB0C : 2*/
  protected int CalcularIndice() {
    //lee las cadenas y descubre la diferencia
    int iReturn = -2;
    String sA, sD = "";

    //borrando
    if (sStringAntes.length() >= sStringDespues.length()) {
      iReturn = -1;
    }
    else {
      //intro del primero
      if (sStringAntes.length() == 0) {
        iReturn = 0;
      }
      else {
        for (int i = 0; i < sStringAntes.length(); i++) {
          sA = sStringAntes.substring(i, i + 1);
          sD = sStringDespues.substring(i, i + 1);
          if (sA.compareTo(sD) != 0) { //no son iguales
            iReturn = i;
            break;
          }
          if (iReturn == -2) { //a�ade al final
            iReturn = sStringAntes.length();
          }
        }
      }
    }
    return (iReturn);
  }

  /* Borra caracter (indice, indice +1)*/
  protected void BorrarCaracter(int indice) {

    String sDelante = getText().substring(0, indice);
    String sDetras = getText().substring(indice + 1, getText().length());

    setText(sDelante + sDetras);
    select(indice, indice);
  }

//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   protected boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString == "") {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString == "") {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     //comprobar longitud
     if (b) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

     return (b);
   } //fin de ChequearEntero

  protected boolean Valid_Dato_Intermedio(int indice, String s) {

    boolean b = true;
    int Len = getText().length() - 1; //sin el dato nuevo

    //BLOQUE 1
    if (indice < 2) {

      if (!ChequearEntero(s, 0, 9, 1)) { //No N
        b = false;
        //bloque inacabado y longantes=2 (XXX)
      }
      if ( (getText().indexOf("/", 0) == -1) && (Len == 2) && b) {
        b = false;
        //bloque inacabado y longantes<2 no se dara!! (X)-->a�ado, no modifico

        //bloque acabado y longbloqueantes = 2 (XXX/)
      }
      if ( (getText().indexOf("/", 0) != -1) &&
          (getText().substring(0, getText().indexOf("/", 0)).length() == 3) &&
          b) {
        b = false;

        //bloque acabado y longbloqueantes < 2 (XX/) true
      }
    }

    //SEPARADORES
    if (indice == 2 || indice == 5 && b) {

      if (!s.equals("/")) {
        b = false;
      }
      else {
        b = true;
      }
    }
    //BLOQUE 2. Existe una barra y el ind > '/' (X/XXX)
    //puede haber pasado por separador!--> b (X/XXX
    if ( (getText().indexOf("/", 0) != -1) &&
        indice > getText().indexOf("/", 0) && b) {

      int x1 = getText().indexOf("/", 0); //pos primera barra

      if (!ChequearEntero(s, 0, 9, 1)) { //No N
        b = false;

        //bloque inacabado y longantes=2 (X/XXX)
      }
      if ( (getText().indexOf("/", x1 + 1) == -1)
          && (getText().substring(x1 + 1, getText().length()).length() == 3)
          && b) {
        b = false;
        //bloque acabado y longbloqueantes=2 (X/XXX/)
      }
      if ( (getText().indexOf("/", x1 + 1) != -1) && b &&
          indice < getText().indexOf("/", x1 + 1)) {

        int x2 = getText().indexOf("/", x1 + 1);
        if (getText().substring(x1 + 1, x2).length() == 3) {
          b = false;
        }
      }
    } //fin bloque 2

    //BLOQUE 3. Existe dos barras y el indice > 2� barra
    if (getText().indexOf("/", 0) != -1) { //1 barra
      int x1 = getText().indexOf("/", 0);
      if (getText().indexOf("/", x1 + 1) != -1 &&
          indice > getText().indexOf("/", x1 + 1)) {

        if (!ChequearEntero(s, 0, 9, 1)) { //No N
          b = false;

        }
        int x2 = getText().indexOf("/", x1 + 1);
        if (getText().substring(x2 + 1, getText().length()).length() == 5) {

          b = false;
        }
      }
    } //fin bloque 3

    return (b);
  }

  protected void txt_keyPressed(KeyEvent e) {
    //capturo la fecha de antes
    sStringAntes = getText();

  } //fin Pressed

  protected void txt_textValueChanged(TextEvent e) {

    String s = "";

    //capturo la fecha de despues
    sStringDespues = getText();

    //capturo el indice
    int ind = CalcularIndice();

    //a�adiendo
    if ( (ind != -1) && (ind == getText().length() - 1)) {
      //capturo el caracter que se ha introducido
      s = getText().substring(ind, ind + 1);

      if ( (ind == 2 || ind == 5) && !s.equals("/")) {
        BorrarCaracter(ind);

      }
      if (ind != 2 && ind != 5 && ind < 9) {
        if (!ChequearEntero(s, 0, 9, 1)) { //No N
          BorrarCaracter(ind);
        }
      }
      if (ind > 9) {
        BorrarCaracter(ind);
      }
    } //--------------------------

    //incluyendo en medio
    if ( (ind != -1) && (ind < getText().length() - 1)) {
      s = getText().substring(ind, ind + 1);
      if (!Valid_Dato_Intermedio(ind, s)) {
        BorrarCaracter(ind);
      }
    } //--------------------------
  } //fin change

//Chequea la fecha que hay en la caja
  public void ValidarFecha() {

    //OK
    if (ChequearFecha(getText(), 1)) {
      sValidaSN = "S";

      //KO
    }
    else {
      sValidaSN = "N";
    }
  }

//Proporciona si el valor es valido
  public String getValid() {
    return (sValidaSN);
  }

//Proporciona la fecha
  public String getFecha() {
    return (sFechaAlmacen);
  }

  /*AIC
   M�todo public String annadirDias(int numDias);
   Devuelve un String que representa la fecha resultante de a�adir numDias a la
   fecha almacenada por el componente.
   */
  public String annadirDias(int numDias) {
    try {
      ValidarFecha();
      Calendar calendario = Calendar.getInstance();
      calendario.set(iAnyo, iMes, iDia);
      calendario.add(Calendar.DATE, numDias);
      int dia = calendario.get(Calendar.DATE);
      int mes = calendario.get(Calendar.MONTH);
      int anyo = calendario.get(Calendar.YEAR);

      if (mes == 0) {
        mes = 12;
        anyo = anyo - 1;
      }
      ;

      String sDia = String.valueOf(dia);
      String sMes = String.valueOf(mes);
      String sAnyo = String.valueOf(anyo);

      sDia = sDia.length() == 1 ? "0" + sDia : sDia;
      sMes = sMes.length() == 1 ? "0" + sMes : sMes;

      return (sDia + "/" + sMes + "/" + sAnyo);
    }
    catch (Exception e) {
      e.printStackTrace();
      return ("");
    }

  }

} //endclass
