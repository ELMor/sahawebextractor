//Title:        Generacion de alarmas automaticas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Al inicio del a�o epidemiologico, o por peticion del administrador
//se generaran/actualizaran las alarmas automaticas de cada
//enfermedad a nivel de Area y a nivel de la Comunidad.

package genAlaAuto;

import java.io.Serializable;
import java.util.Vector;

public class ParGenAlaAuto
    implements Serializable {

  public String anoEpi = new String();
  public int tipo = 0;
  public Vector enfermedad = new Vector();

  public String sCodNiv1 = "";
  public String sCodNiv2 = "";

  public ParGenAlaAuto() {
  }

  public ParGenAlaAuto(String ano, String cod1, String cod2) {
    anoEpi = ano;
    sCodNiv1 = cod1;
    sCodNiv2 = cod2;
  }

  public ParGenAlaAuto(String ano, int t, String cod1, String cod2) {
    anoEpi = ano;
    tipo = t;
    sCodNiv1 = cod1;
    sCodNiv2 = cod2;

  }

  public ParGenAlaAuto(String ano, Vector e, int t, String cod1, String cod2) {
    anoEpi = ano;
    enfermedad = e;
    tipo = t;
    sCodNiv1 = cod1;
    sCodNiv2 = cod2;

  }

}
