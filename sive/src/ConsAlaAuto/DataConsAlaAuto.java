//Title:        Consulta de alarmas automaticas de una semana epidemilogica.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Se presentara la relacion de enfermedades
//notificadas en la semana elegida con el numero
//de casos y los indicadoes superados con su umbral.

package ConsAlaAuto;

import java.io.Serializable;

public class DataConsAlaAuto
    implements Serializable {

  public String DS_ENFERMEDAD = new String();
  public String NM_CASOS = new String();
  public String DS_INDICADORES = new String();

  public DataConsAlaAuto() {
  }

  public DataConsAlaAuto(String e, String c, String i) {
    DS_ENFERMEDAD = e;
    NM_CASOS = c;
    DS_INDICADORES = i;
  }
}
