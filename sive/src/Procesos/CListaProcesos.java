
package Procesos;

import capp.CApp;
import capp.CListaMantenimiento;
import catalogo.DataCat;
import sapp.StubSrvBD;

public class CListaProcesos
    extends CListaMantenimiento {

  protected int iProcesos;
  protected String sTitulo;

  public CListaProcesos(Procesos a) {
    super( (CApp) a,
          2,
          "100\n462",
          a.sEtCod + a.sLabel,
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO + a.iProcesos,
          a.servletSELECCION_X_DESCRIPCION + a.iProcesos);

    iProcesos = a.iProcesos;
    sTitulo = a.sLabel;
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    DataCat cat;

    modoOperacion = modoESPERA;
    Inicializar();
    PanelPro panel = new PanelPro(this.app, PanelPro.modoALTA, iProcesos,
                                  sTitulo);
    panel.show();

    if ( (panel.bAceptar)) {
      cat = new DataCat(panel.txtCod.getText().toUpperCase(),
                        panel.txtDes.getText(),
                        panel.txtDesL.getText());
      lista.addElement(cat);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    DataCat cat;

    modoOperacion = modoESPERA;
    Inicializar();
    PanelPro panel = new PanelPro(this.app, PanelPro.modoMODIFICAR, iProcesos,
                                  sTitulo);

    // rellena los datos
    cat = (DataCat) lista.elementAt(i);
    panel.txtCod.setText(cat.getCod());
    panel.txtDes.setText(cat.getDes());
    panel.txtDesL.setText(cat.getDesL());

    panel.show();

    if ( (panel.bAceptar)) {
      cat = new DataCat(panel.txtCod.getText(),
                        panel.txtDes.getText(),
                        panel.txtDesL.getText());
      lista.removeElementAt(i);
      lista.insertElementAt(cat, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {
    DataCat cat;

    modoOperacion = modoESPERA;
    Inicializar();
    PanelPro panel = new PanelPro(this.app, PanelPro.modoBAJA, iProcesos,
                                  sTitulo);

    // rellena los datos
    cat = (DataCat) lista.elementAt(i);
    panel.txtCod.setText(cat.getCod());
    panel.txtDes.setText(cat.getDes());
    panel.txtDesL.setText(cat.getDesL());

    panel.show();

    if (panel.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataCat(s);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataCat cat = (DataCat) o;

    return cat.getCod() + "&" + cat.getDes();
  }

}