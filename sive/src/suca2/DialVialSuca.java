package suca2;

import java.util.Enumeration;
import java.util.Hashtable;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CTabla;
import jclass.bwt.JCActionEvent;
import sapp2.Lista;

public class DialVialSuca
    extends CDialog {

//__________________________________________________ MODOS
  //modos de operaci�n de la ventana
  public final int modoINICIO = 0;
  public final int modoESPERA = 1;

  /** indica en que estado est� el frame */
  protected int modoOperacion = 0;
  protected int modoAnterior = 0;

  /** esta es la lista que contiene los datos que se muestran en la tabla */
  Lista listaVial = null;

  /** esta lista son los datos del enfermos seleccionado */
  Lista listaVialSeleccionado = null;

  /** Vial seleccionado */
  Lista listaDatosBusqueda = null;

  //protected boolean bPerNom = true;

  /** Par�metros de carga de la tabla */
  protected String cdtvia = "";
  protected String cdmuni = "";
  protected String dsvianorm = "";

  /** Permite saber si ha habido datos tras una b�squeda */
  protected boolean bHayDatos = false;

      /** Para mostrar un mensaje indicando que se han encontrado mas de 50 viales **/
  protected boolean bMas50Viales = false;

  /** Cadena para el mensaje 'no hay datos' */
  private final String strNoHayDatos =
      "No hay v�as que respondan a esas caracter�sticas";

  // SINCRONIZACION
  /** esta variable se pone a true cuando no se inhiben los eventos
   *  y a false se rechazan todos los eventos
   */
  protected boolean sinBloquear = true;

  //protected panelsuca pnlSuca = null;

//__________________________________________________ COMP GRAFICOS
//  BevelPanel pnl = new BevelPanel();
  XYLayout xYLayout = new XYLayout();
  CTabla tablaVial = new CTabla();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  GroupBox pnlEnfermedad = new GroupBox();

  //______________________________________________ GESTOR de EVENTOS
  BtnVialActionListener btnActionListener = new BtnVialActionListener(this);
  VialTableAdapter tableAdapter = new VialTableAdapter(this);

  //______________________________________________ CONSTRUCTOR
  public DialVialSuca(CApp app) {
    super(app);
    setTitle("Di�logo de direcciones normalizadas");

    try {
      Lista parametros = null, result = null;
      jbInit();
      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /** este m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = modoESPERA;
      //Iniciar();
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  void jbInit() throws Exception {
    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();

    // fijamos las dimensiones del di�logo
    setSize(600, 250);
    xYLayout.setHeight(275);
    xYLayout.setWidth(600);

    setLayout(xYLayout);

    // Im�genes de los botones
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    tablaVial.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("Tipo vial\nVial"), '\n'));
    tablaVial.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "60\n490"), '\n'));
    tablaVial.setNumColumns(3);

    btnCancelar.setLabel("Cancelar");
    btnAceptar.setLabel("Aceptar");

    // a�adimos el nombre de los botones
    btnCancelar.setActionCommand("btnCancelar");
    btnAceptar.setActionCommand("btnAceptar");

    // a�adimos al panel todos los componentes
    // primero a�adimos las listas y despu�s el resto de componentes
    // panel enfermedad
    this.add(tablaVial, new XYConstraints(15, 20, 570, 178));
    this.add(btnCancelar, new XYConstraints(504, 210, -1, -1));
    this.add(btnAceptar, new XYConstraints(427, 210, -1, -1));

    // gesti�n de eventos de botones
    btnCancelar.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);

    //gesti�n de eventos de la tabla
    tablaVial.addActionListener(tableAdapter);

    // se inician los botones a false o true
    this.modoOperacion = modoINICIO;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoINICIO:
        break;
      case modoESPERA:
        break;
    }
  }

  public Lista getListaDatosVial() {
    return listaVialSeleccionado;
  }

  public void setListaDatosVial(Lista lista) {
    listaVialSeleccionado = lista;
  }

  protected void lanza_busqueda() {
    // rellenamos la hashtable solo con los datos que tengan valores utiles
    datasuca datosVial = new datasuca();

    bHayDatos = true;

    String filtro = null;
    int indice = 0;
    Lista result = null;

    listaDatosBusqueda = new Lista();

    // miramos si queremos ver los enfermos de baja
    // dEnfer.setMostrarBajas(chkbMostrarBaja.getState());

    datosVial.put("CD_MUN", cdmuni);
    datosVial.put("DSVIANORM", dsvianorm);
    datosVial.put("CDTVIA", cdtvia);

    listaDatosBusqueda.addElement(datosVial);

    try {
      this.getApp().getStub().setUrl("servlet/srvsuca2");
      listaVial = (Lista)this.getApp().getStub().doPost(srvsuca2.
          modoOBTENER_VIAL, listaDatosBusqueda);
      /*                   // debug
                          srvsuca2 srv = new srvsuca2();
                          // par�metros jdbc
           srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
           listaVial = (Lista)srv.doDebug(srvsuca2.modoOBTENER_VIAL, listaDatosBusqueda);*/
    }
    catch (Exception ex) {
      this.app.showError(ex.getMessage());
    }

    if (listaVial.size() == 0) {
      this.app.showAdvise(strNoHayDatos);
      bHayDatos = false;
    }
    else {
      escribirTabla(listaVial);
    }
  }

  /**
   *
   */
  boolean tabla_actionPerformed(JCActionEvent e) {
    int indice = tablaVial.getSelectedIndex();
    Hashtable h;
    Object code = null;
    String descrip = null;
    int indLista = -1;

    if (indice < 0) {
      return false;
    }

    if ( (indice == listaVial.size()) &&
        (listaVial.getEstado() == Lista.INCOMPLETA)) {
      /// se ha pinchado el elemento M�s...
      listaDatosBusqueda.setTrama(listaVial.getTrama());

      try {
        this.getApp().getStub().setUrl("servlet/srvsuca2");
        listaVial.addElements( (Lista)this.getApp().getStub().doPost(srvsuca2.
            modoOBTENER_VIAL, listaDatosBusqueda));
      }
      catch (Exception ex) {
        this.app.showError(ex.getMessage());
      }

      escribirTabla(listaVial);
    }
    else {
      /// se buscan los datos del vial pinchado y se vuelve
      h = (Hashtable) (listaVial.elementAt(indice));

      datasuca dVial = new datasuca();

      dVial.put("CDTVIA", h.get("CDTVIA"));
      dVial.put("DSVIAOFIC", h.get("DSVIAOFIC"));
      dVial.put("CDVIAL", h.get("CDVIAL"));

      listaVialSeleccionado = new Lista();
      listaVialSeleccionado.addElement(dVial);
    }
    return true;
  }

  // rellena la tabla
  void escribirTabla(Lista lista) {
    datasuca datUnaLinea;
    StringBuffer datLineaEscribir = new StringBuffer();
    String dato = null;
    int tam;

    //Borramos la tabla que hab�a
    tam = tablaVial.countItems();
    if (tam > 0) {
      tablaVial.deleteItems(0, tam - 1);
    }

    if (lista == null || (lista.size() == 0)) {
      this.getApp().showAdvise(strNoHayDatos);
      bHayDatos = false;
    }

    Enumeration enum = lista.elements();

    while (enum.hasMoreElements()) {
      //Recogemos los datos de una l�nea
      datUnaLinea = (datasuca) (enum.nextElement());
      //ponemos a cero el buffer
      datLineaEscribir.setLength(0);

      Object o = (datUnaLinea.get("CDTVIA"));
      if (o != null) {
        datLineaEscribir.append(o.toString());
      }
      datLineaEscribir.append("&");

      //Aqu� se a�ade el resto de los datos
      dato = (String) datUnaLinea.get("DSVIAOFIC");
      if (dato != null) {
        datLineaEscribir.append(dato);
      }

      datLineaEscribir.append("&");

      dato = (String) datUnaLinea.get("CDVIAL");
      if (dato != null) {
        datLineaEscribir.append(dato);
      }

      datLineaEscribir.append(" ");

      //A�adimos la l�nea a la tabla
      tablaVial.addItem(datLineaEscribir.toString(), '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)

    }

    // aviso de hay m�s datos
    tam = tablaVial.countItems();
    if (tam > 50) {
      bMas50Viales = true;
    }
  }

  /**
   *  esta funci�n fija los parametros para que se muestren los datos
   */
  public void setData(Lista a_dat) {

  }

  /**
   *  esta funci�n devuelve los valores calculados
   */
  public Lista getData() {
    return listaVialSeleccionado;
  }
}

/**
 *  esta clase recoge los eventos de pinchar en las lupas
 *  cuando se pincha a una lupa se llama a un servlet para pedir los datos
 *  se deshabilitan el resto de los botones lupa
 *  y se manda ejecutar la acci�n correspondiente
 */
// action listener de evento en bot�nes
class BtnVialActionListener
    implements ActionListener, Runnable {
  DialVialSuca adaptee = null;
  ActionEvent e = null;

  public BtnVialActionListener(DialVialSuca adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    // deshabilita los escuchadores de cambios de c�digo
    String name = e.getActionCommand();
    String name2 = ( (Component) e.getSource()).getName();
    Lista lalista = null;
    String campo = null;

    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar();

    /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
    if (name.equals("btnAceptar")) {
      if (adaptee.tabla_actionPerformed(null)) {
        adaptee.dispose();
      }
    }
    else if (name.equals("btnCancelar")) {
      /// nos salimos y borramos todo
      // le grabamos los datos pedidos
      adaptee.listaVialSeleccionado = null;
      adaptee.dispose();
    }
    adaptee.Inicializar();
    // desbloquea la recepci�n  de eventos
    adaptee.desbloquea();
  }
}

// escuchador de los click en la tabla
class VialTableAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  DialVialSuca adaptee;
  JCActionEvent e;

  VialTableAdapter(DialVialSuca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar();

    ////#// System_out.println("Evento de la tabla " + e.toString());
    adaptee.tabla_actionPerformed(e);
    adaptee.dispose();
    adaptee.desbloquea();
  }
}
//________________________________________________ END CLASS
