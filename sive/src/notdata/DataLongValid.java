package notdata;

import java.awt.TextField;

import capp.CPanel;

public class DataLongValid {

  public String panel_nombre = "";
  public CPanel panel = null;
  public TextField txt = null;
  public String limite = "";
  public String nombre = "";

  public DataLongValid(String nombre_panel, CPanel Cp_panel,
                       TextField nombre_txt,
                       String limite_campo, String nombre_campo) {
    panel_nombre = nombre_panel;
    panel = Cp_panel;
    txt = nombre_txt;
    limite = limite_campo;
    nombre = nombre_campo;
  }

  public String getpanel_nombre() {
    return panel_nombre;
  }

  public CPanel getpanel() {
    return panel;
  }

  public TextField gettxt() {
    return txt;
  }

  public String getlimite() {
    return limite;
  }

  public String getnombre() {
    return nombre;
  }

}