package notdata;

import java.io.Serializable;

public class DataListaNotifEDO
    implements Serializable {

  public String sResSem = "", sFechaNotif = "", sFechaRecep = "";
  public String sNotifReales = "";
  public String sNotifTeor = "", sTotNotifReales = "";
  public String sCodEquipo = "", sAnno = "";
  public String sSemana = "";

  public String CD_OPE_NOTIF_SEM = "";
  public String FC_ULTACT_NOTIF_SEM = "";
  public String CD_OPE_NOTIFEDO = "";
  public String FC_ULTACT_NOTIFEDO = "";

  public String IT_VALIDADA = "";
  public String FC_FECVALID = "";

  public String CD_NIVEL_1 = "";
  public String CD_NIVEL_2 = "";

  public DataListaNotifEDO() {}

  public DataListaNotifEDO(String ressem,
                           String fechanotif,
                           String fecharecep,
                           String notifreales,
                           String notifTeoricos,
                           String totalReales,
                           String codeq, String anno, String sem,
                           String cd_ope_notif_sem,
                           String fc_ultact_notif_sem,
                           String cd_ope_notifedo,
                           String fc_ultact_notifedo,
                           String it_validada,
                           String fc_validacion,
                           String cd_nivel_1,
                           String cd_nivel_2) {
    sResSem = ressem;
    sFechaNotif = fechanotif;
    sFechaRecep = fecharecep;
    sNotifReales = notifreales;
    sNotifTeor = notifTeoricos;
    sTotNotifReales = totalReales;
    sCodEquipo = codeq;
    sAnno = anno;
    sSemana = sem;
    CD_OPE_NOTIF_SEM = cd_ope_notif_sem;
    FC_ULTACT_NOTIF_SEM = fc_ultact_notif_sem;
    CD_OPE_NOTIFEDO = cd_ope_notifedo;
    FC_ULTACT_NOTIFEDO = fc_ultact_notifedo;

    IT_VALIDADA = it_validada;
    FC_FECVALID = fc_validacion;

    CD_NIVEL_1 = cd_nivel_1;
    CD_NIVEL_2 = cd_nivel_2;
  }

  public String getResSem() {
    return sResSem;
  }

  public String getFechaNotif() {
    return sFechaNotif;
  }

  public String getFechaRecep() {
    return sFechaRecep;
  }

  public String getNotifReales() {
    return sNotifReales;
  }

  public String getNotifTeor() {
    return sNotifTeor;
  }

  public String getTotNotifReales() {
    return sTotNotifReales;
  }

  public String getCodEquipo() {
    return sCodEquipo;
  }

  public String getAnno() {
    return sAnno;
  }

  public String getSemana() {
    return sSemana;
  }

  public String getCD_OPE_NOTIF_SEM() {
    return CD_OPE_NOTIF_SEM;
  }

  public String getFC_ULTACT_NOTIF_SEM() {
    return FC_ULTACT_NOTIF_SEM;
  }

  public String getCD_OPE_NOTIFEDO() {
    return CD_OPE_NOTIFEDO;
  }

  public String getFC_ULTACT_NOTIFEDO() {
    return FC_ULTACT_NOTIFEDO;
  }

  public String getIT_VALIDADA() {
    return IT_VALIDADA;
  }

  public String getFC_FECVALID() {
    return FC_FECVALID;
  }

  public String getCD_NIVEL_1() {
    return CD_NIVEL_1;
  }

  public String getCD_NIVEL_2() {
    return CD_NIVEL_2;
  }
}
