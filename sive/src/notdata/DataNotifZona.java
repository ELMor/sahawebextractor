package notdata;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

//Estructura para SIVE_NOTIF_EDOI + ZONA E_NOTIF
public class DataNotifZona
    implements Serializable {

//notif_edoi
  public String NM_EDO = null;
  public String CD_ANOEPI_NOTIF_EDOI = null;
  public String CD_SEMEPI_NOTIF_EDOI = null;
  public String FC_RECEP_NOTIF_EDOI = null;
  public String FC_FECNOTIF_NOTIF_EDOI = null;
  public String CD_E_NOTIF = null;
  public String DS_DECLARANTE = null;
  public String IT_PRIMERO = null;
  public String CD_OPE_NOTIF_EDOI = null;
  public String FC_ULTACT_NOTIF_EDOI = null;

  public String CD_NIVEL_1_E_NOTIF = null;
  public String CD_NIVEL_2_E_NOTIF = null;
  public String CD_ZBS_E_NOTIF = null;
  public String CD_CENTRO_E_NOTIF = null;
  public String DS_CENTRO_E_NOTIF = null;

  public DataNotifZona() {
  }

//constructor para notif_edoi
  public DataNotifZona(String nm_edo,
                       String cd_e_notif,
                       String cd_anoepi_notif_edoi,
                       String cd_semepi_notif_edoi,
                       String fc_recep_notif_edoi,
                       String ds_declarante,
                       String it_primero,
                       String cd_ope_notif_edoi,
                       String fc_ultact_notif_edoi,
                       String fc_fecnotif_notif_edoi,
                       String cd_nivel_1_e_notif,
                       String cd_nivel_2_e_notif,
                       String cd_zbs_e_notif,
                       String cd_centro_e_notif,
                       String ds_centro_e_notif) {

    NM_EDO = nm_edo;
    CD_E_NOTIF = cd_e_notif;
    CD_ANOEPI_NOTIF_EDOI = cd_anoepi_notif_edoi;
    CD_SEMEPI_NOTIF_EDOI = cd_semepi_notif_edoi;
    FC_RECEP_NOTIF_EDOI = fc_recep_notif_edoi;
    FC_FECNOTIF_NOTIF_EDOI = fc_fecnotif_notif_edoi;
    DS_DECLARANTE = ds_declarante;
    IT_PRIMERO = it_primero;
    CD_OPE_NOTIF_EDOI = cd_ope_notif_edoi;
    FC_ULTACT_NOTIF_EDOI = fc_ultact_notif_edoi;

    CD_NIVEL_1_E_NOTIF = cd_nivel_1_e_notif;
    CD_NIVEL_2_E_NOTIF = cd_nivel_2_e_notif;
    CD_ZBS_E_NOTIF = cd_zbs_e_notif;
    CD_CENTRO_E_NOTIF = cd_centro_e_notif;
    DS_CENTRO_E_NOTIF = ds_centro_e_notif;
  }

  public String getNM_EDO() {
    return NM_EDO;
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getDS_DECLARANTE() {
    return DS_DECLARANTE;
  }

  public String getIT_PRIMERO() {
    return IT_PRIMERO;
  }

  public String getCD_OPE_NOTIF_EDOI() {
    return CD_OPE_NOTIF_EDOI;
  }

  public String getFC_ULTACT_NOTIF_EDOI() {
    return FC_ULTACT_NOTIF_EDOI;
  }

  public String getCD_ANOEPI_NOTIF_EDOI() {
    return CD_ANOEPI_NOTIF_EDOI;
  }

  public String getCD_SEMEPI_NOTIF_EDOI() {
    return CD_SEMEPI_NOTIF_EDOI;
  }

  public String getFC_RECEP_NOTIF_EDOI() {
    return FC_RECEP_NOTIF_EDOI;
  }

  public String getFC_FECNOTIF_NOTIF_EDOI() {
    return FC_FECNOTIF_NOTIF_EDOI;
  }

  public String getCD_NIVEL_1_E_NOTIF() {
    return CD_NIVEL_1_E_NOTIF;
  }

  public String getCD_NIVEL_2_E_NOTIF() {
    return CD_NIVEL_2_E_NOTIF;
  }

  public String getCD_ZBS_E_NOTIF() {
    return CD_ZBS_E_NOTIF;
  }

  public String getCD_CENTRO_E_NOTIF() {
    return CD_CENTRO_E_NOTIF;
  }

  public String getDS_CENTRO_E_NOTIF() {
    return DS_CENTRO_E_NOTIF;
  }

  private void writeObject(ObjectOutputStream s) throws IOException {
    s.defaultWriteObject();
  }

  private void readObject(ObjectInputStream s) throws IOException,
      ClassNotFoundException {
    s.defaultReadObject();
  }

} //FIN DE LA CLASE
