package notdata;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class DataEdoind
    implements Serializable {

//edoind
  public String NM_EDO = null;
  public String CD_ANOEPI = null;
  public String CD_SEMEPI = null;
  public String CD_ANOURG = null;
  public String NM_ENVIOURGSEM = null;
  public String CD_CLASIFDIAG = null;
  public String IT_PENVURG = null;
  public String CD_ENFERMO = null;
  public String CD_PROV_EDOIND = null;
  public String CD_CA_EDOIND = null;
  public String CD_MUN_EDOIND = null;
  public String CD_NIVEL_1_EDOIND = null;
  public String CD_NIVEL_2_EDOIND = null;
  public String CD_ZBS_EDOIND = null;
  public String FC_RECEP = null;
  public String DS_CALLE = null;
  public String DS_NMCALLE = null;
  public String DS_PISO_EDOIND = null;
  public String CD_POSTAL_EDOIND = null;
  public String CD_OPE_EDOIND = null;
  public String FC_ULTACT_EDOIND = null;
  public String CD_ANOOTRC = null;
  public String NM_ENVOTRC = null;
  public String CD_ENFCIE = null;
  public String FC_FECNOTIF = null;
  public String IT_DERIVADO = null;
  public String DS_CENTRODER = null;
  public String IT_DIAGCLI = null;
  public String FC_INISNT = null;
  public String DS_COLECTIVO = null; //30
  public String IT_ASOCIADO = null;
  public String IT_DIAGMICRO = null;
  public String DS_ASOCIADO = null;
//public String CD_VIA_EDOIND = null;
//public String CD_VIAL_EDOIND = null; //residuo
  public String IT_DIAGSERO = null;
  public String DS_DIAGOTROS = null; //37
// SUCA
  public String CDVIAL = null;
  public String CDTVIA = null;
  public String CDTNUM = null;
  public String DSCALNUM = null;

//descripciones: edoind
//public String DS_VIAL_EDOIND = null;
//public String DS_VIALITE_EDOIND = null;
  public String DS_PROV_EDOIND = null;
  public String DS_MUN_EDOIND = null;
  public String DS_NIVEL_1_EDOIND = null;
  public String DS_NIVEL_2_EDOIND = null;
  public String DS_ZBS_EDOIND = null;

//constructor para EDOIND
  public DataEdoind(String nm_edo,
                    String cd_anoepi, String cd_semepi,
                    String cd_anourg,
                    String nm_enviourgsem, String cd_clasifdiag,
                    String it_penvurg, String cd_enfermo,
                    String cd_prov_edoind, String ds_prov_edoind,
                    String cd_ca_edoind,
                    String cd_mun_edoind, String ds_mun_edoind,
                    String cd_nivel_1_edoind, String ds_nivel_1_edoind,
                    String cd_nivel_2_edoind, String ds_nivel_2_edoind,
                    String cd_zbs_edoind, String ds_zbs_edoind,
                    String fc_recep,
                    String ds_calle, String ds_nmcalle,
                    String ds_piso_edoind, String cd_postal_edoind,
                    String cd_ope_edoind, String fc_ultact_edoind,
                    String cd_anootrc, String nm_envotrc,
                    String cd_enfcie,
                    String fc_fecnotif,
                    String it_derivado, String ds_centroder,
                    String it_diagcli, String fc_inisnt,
                    String ds_colectivo, String it_asociado,
                    String it_diagmicro, String ds_asociado,
                    String it_diagsero,
                    String ds_diagotros,
                    String cdvial, String cdtvia,
                    String cdtnum, String dscalnum) {

    NM_EDO = nm_edo;
    CD_ANOEPI = cd_anoepi;
    CD_SEMEPI = cd_semepi;
    CD_ANOURG = cd_anourg;
    NM_ENVIOURGSEM = nm_enviourgsem;
    CD_CLASIFDIAG = cd_clasifdiag;
    IT_PENVURG = it_penvurg;
    CD_ENFERMO = cd_enfermo;
    CD_PROV_EDOIND = cd_prov_edoind;
    DS_PROV_EDOIND = ds_prov_edoind;
    CD_CA_EDOIND = cd_ca_edoind;
    CD_MUN_EDOIND = cd_mun_edoind;
    DS_MUN_EDOIND = ds_mun_edoind;
    CD_NIVEL_1_EDOIND = cd_nivel_1_edoind;
    DS_NIVEL_1_EDOIND = ds_nivel_1_edoind;
    CD_NIVEL_2_EDOIND = cd_nivel_2_edoind;
    DS_NIVEL_2_EDOIND = ds_nivel_2_edoind;
    CD_ZBS_EDOIND = cd_zbs_edoind;
    DS_ZBS_EDOIND = ds_zbs_edoind;
    FC_RECEP = fc_recep;
    DS_CALLE = ds_calle;
    DS_NMCALLE = ds_nmcalle;
    DS_PISO_EDOIND = ds_piso_edoind;
    CD_POSTAL_EDOIND = cd_postal_edoind;
    CD_OPE_EDOIND = cd_ope_edoind;
    FC_ULTACT_EDOIND = fc_ultact_edoind;
    CD_ANOOTRC = cd_anootrc;
    NM_ENVOTRC = nm_envotrc;
    CD_ENFCIE = cd_enfcie;
    FC_FECNOTIF = fc_fecnotif;
    IT_DERIVADO = it_derivado;
    DS_CENTRODER = ds_centroder;
    IT_DIAGCLI = it_diagcli;
    FC_INISNT = fc_inisnt;
    DS_COLECTIVO = ds_colectivo;
    IT_ASOCIADO = it_asociado;
    IT_DIAGMICRO = it_diagmicro;
    DS_ASOCIADO = ds_asociado;
    IT_DIAGSERO = it_diagsero;
    DS_DIAGOTROS = ds_diagotros;
    CDVIAL = cdvial;
    CDTVIA = cdtvia;
    CDTNUM = cdtnum;
    DSCALNUM = dscalnum;
  }

  public String getCD_ENFERMO() {
    return CD_ENFERMO;
  }

  public String getNM_EDO() {
    return NM_EDO;
  }

  public String getCD_ANOEPI() {
    return CD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return CD_SEMEPI;
  }

  public String getCD_ANOURG() {
    return CD_ANOURG;
  }

  public String getNM_ENVIOURGSEM() {
    return NM_ENVIOURGSEM;
  }

  public String getCD_CLASIFDIAG() {
    return CD_CLASIFDIAG;
  }

  public String getIT_PENVURG() {
    return IT_PENVURG;
  }

  public String getCD_PROV_EDOIND() {
    return CD_PROV_EDOIND;
  }

  public String getCD_MUN_EDOIND() {
    return CD_MUN_EDOIND;
  }

  public String getCD_NIVEL_1_EDOIND() {
    return CD_NIVEL_1_EDOIND;
  }

  public String getCD_NIVEL_2_EDOIND() {
    return CD_NIVEL_2_EDOIND;
  }

  public String getCD_ZBS_EDOIND() {
    return CD_ZBS_EDOIND;
  } //14

  public String getFC_RECEP() {
    return FC_RECEP;
  }

  public String getDS_CALLE() {
    return DS_CALLE;
  }

  public String getDS_NMCALLE() {
    return DS_NMCALLE;
  }

  public String getDS_PISO_EDOIND() {
    return DS_PISO_EDOIND;
  }

  public String getCD_POSTAL_EDOIND() {
    return CD_POSTAL_EDOIND;
  }

  public String getCD_OPE_EDOIND() {
    return CD_OPE_EDOIND;
  }

  public String getFC_ULTACT_EDOIND() {
    return FC_ULTACT_EDOIND;
  }

  public String getCD_ANOOTRC() {
    return CD_ANOOTRC;
  }

  public String getNM_ENVOTRC() {
    return NM_ENVOTRC;
  }

  public String getCD_ENFCIE() {
    return CD_ENFCIE;
  }

  public String getFC_FECNOTIF() {
    return FC_FECNOTIF;
  } //11

  public String getIT_DERIVADO() {
    return IT_DERIVADO;
  }

  public String getDS_CENTRODER() {
    return DS_CENTRODER;
  }

  public String getIT_DIAGCLI() {
    return IT_DIAGCLI;
  }

  public String getFC_INISNT() {
    return FC_INISNT;
  }

  public String getDS_COLECTIVO() {
    return DS_COLECTIVO;
  }

  public String getIT_ASOCIADO() {
    return IT_ASOCIADO;
  }

  public String getIT_DIAGMICRO() {
    return IT_DIAGMICRO;
  }

  public String getDS_ASOCIADO() {
    return DS_ASOCIADO;
  }

  public String getIT_DIAGSERO() {
    return IT_DIAGSERO;
  }

  public String getDS_DIAGOTROS() {
    return DS_DIAGOTROS;
  }

  public String getCDVIAL() {
    return CDVIAL;
  }

  public String getCDTVIA() {
    return CDTVIA;
  }

  public String getCDTNUM() {
    return CDTNUM;
  }

  public String getDSCALNUM() {
    return DSCALNUM;
  }

//public String getCD_VIA_EDOIND () { return CD_VIA_EDOIND ;}
//public String getCD_VIAL_EDOIND () { return CD_VIAL_EDOIND ;}//12

//public String getDS_VIAL_EDOIND () { return  DS_VIAL_EDOIND;}
//public String getDS_VIALITE_EDOIND () { return DS_VIALITE_EDOIND;}

  public String getDS_PROV_EDOIND() {
    return DS_PROV_EDOIND;
  }

  public String getDS_MUN_EDOIND() {
    return DS_MUN_EDOIND;
  }

  public String getDS_NIVEL_1_EDOIND() {
    return DS_NIVEL_1_EDOIND;
  }

  public String getDS_NIVEL_2_EDOIND() {
    return DS_NIVEL_2_EDOIND;
  }

  public String getDS_ZBS_EDOIND() {
    return DS_ZBS_EDOIND;
  }

  //otros
  public String getCD_CA_EDOIND() {
    return CD_CA_EDOIND;
  }

  private void writeObject(ObjectOutputStream s) throws IOException {
    s.defaultWriteObject();
  }

  private void readObject(ObjectInputStream s) throws IOException,
      ClassNotFoundException {
    s.defaultReadObject();
  }

} //FIN DE LA CLASE
