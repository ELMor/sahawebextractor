package notdata;

import java.io.Serializable;
import java.util.Vector;

//guardamos los datos de ope y fc_ultact del
//equipo notificador, notif_sem, notifedo, etc

//se usa tb para la respuesta del servlet

//VUELTA:
//  si podemos modificar sin problemas
//  listaVuelta -> DataOpeFc (... true)
//  si NO podemos modificar
//  listaVuelta -> DataOpeFc (... "TABLA AFECTADA", false, true)
//  si la TABLA ha sido borrada
//  listaVuelta -> DataOpeFc (..."TABLA AFECTADA", false, false)

public class DataOpeFc
    implements Serializable {

  public String CD_OPE = "";
  public String FC_ULTACT = "";
  public String TABLA = "";
  public Vector vCAMPOS = new Vector();
  public boolean OK_KO = false;
  public String CAMPOBD = "";
  public String VALOR = "";
  public String TIPO = "";
  public boolean BORRADA = false;

  public DataOpeFc() {}

  //entrada
  public DataOpeFc(String cd_ope,
                   String fc_ultact,
                   String tabla,
                   Vector vcampos) {
    CD_OPE = cd_ope;
    FC_ULTACT = fc_ultact;
    TABLA = tabla;
    vCAMPOS = vcampos;

  }

  //salida
  public DataOpeFc(String tabla, boolean ok_ko, boolean borrada) {

    TABLA = tabla;
    OK_KO = ok_ko;
    BORRADA = borrada;
  }

  //PARA SOPORTAR EL VECTOR
  public DataOpeFc(String campobd, String valor, String tipo) {

    CAMPOBD = campobd;
    VALOR = valor;
    TIPO = tipo;
  }

  //metodos
  public String getCD_OPE() {
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public String getTABLA() {
    return TABLA;
  }

  public Vector getvCAMPOS() {
    return vCAMPOS;
  }

  public boolean getOK_KO() {
    return OK_KO;
  }

  public boolean getBORRADA() {
    return BORRADA;
  }

  public String getCAMPOBD() {
    return CAMPOBD;
  }

  public String getVALOR() {
    return VALOR;
  }

  public String getTIPO() {
    return TIPO;
  }
} //fin class