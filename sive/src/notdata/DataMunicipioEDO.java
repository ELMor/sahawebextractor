package notdata;

import java.io.Serializable;

public class DataMunicipioEDO
    implements Serializable {

  public String sCodMun = "";
  public String sDesMun = "";
  public String sCodNivel1 = "";
  public String sDesNivel1 = "";
  public String sCodNivel2 = "";
  public String sDesNivel2 = "";
  public String sCodZBS = "";
  public String sDesZBS = "";
  public String sCodCa = "";
  public String sDesCa = "";
  public String sCodProv = "";
  public String sDesProv = "";

  public DataMunicipioEDO() {
  }

  public DataMunicipioEDO(String codmun, String codprov) {
    sCodMun = codmun;
    sCodProv = codprov;
  }

  public DataMunicipioEDO(String codmun, String desmun,
                          String codprov, String desprov,
                          String codnivel1, String desnivel1,
                          String codnivel2, String desnivel2,
                          String codzbs, String deszbs,
                          String cdca) {

    sCodMun = codmun;
    sDesMun = desmun;
    sCodProv = codprov;
    sDesProv = desprov;
    sCodNivel1 = codnivel1;
    sDesNivel1 = desnivel1;
    sCodNivel2 = codnivel2;
    sDesNivel2 = desnivel2;
    sCodZBS = codzbs;
    sDesZBS = deszbs;
    sCodCa = cdca;

  }

  public String getCodMun() {
    return sCodMun;
  }

  public String getDesMun() {
    return sDesMun;
  }

  public String getCodProv() {
    return sCodProv;
  }

  public String getCodNivel1() {
    return sCodNivel1;
  }

  public String getDesNivel1() {
    return sDesNivel1;
  }

  public String getCodNivel2() {
    return sCodNivel2;
  }

  public String getDesNivel2() {
    return sDesNivel2;
  }

  public String getCodZBS() {
    return sCodZBS;
  }

  public String getDesZBS() {
    return sDesZBS;
  }

  public String getDesProv() {
    return sDesProv;
  }

}