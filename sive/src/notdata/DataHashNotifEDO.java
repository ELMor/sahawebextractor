
package notdata;

import java.util.Hashtable;

// hashtable para guardar datos comunes al m�dulo notificaciones EDO
public class DataHashNotifEDO
    extends Hashtable {

  public DataHashNotifEDO() {
  }

  public synchronized Object get(Object key) {
    return this.get(key);
  }

  public synchronized Object put(Object key, Object value) {
    return this.put(key, value);
  }
}