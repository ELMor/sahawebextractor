package notdata;

import java.io.Serializable;

//soporta todos los datos para dar de alta una edo Numerica COMPLETA
//Tablas: notifedo, edoNum
//Partimos de que esta dada de alta en notif_sem
public class DataNumerica
    implements Serializable {

  public String IT_SOLO_ACTUALIZAR_NOTIFEDO = "";
  public String IT_SOLO_ALTA_NOTIFEDO = "";

//notif_sem
  public String IT_ACTUALIZAR_NOTIF_SEM = "";
  public String NM_NNOTIFT_ANTIGUA = "";
  public String NM_NNOTIFT_NUEVA = "";
  public String NM_NTOTREAL = "";

//notifedo
  public String CD_E_NOTIF = "";
  public String CD_ANOEPI = "";
  public String CD_SEMEPI = "";
  public String FC_RECEP = "";
  public String NM_NNOTIFR = "";
  public String FC_FECNOTIF = "";
  public String IT_RESSEM = "";
  public String CD_OPE = "";
  public String FC_ULTACT = "";
  public String IT_VALIDADA = "";
  public String FC_FECVALID = "";

//si esta dada de alta la semana,
//en CD_ OPE y FC_ ULTACT iran los datos para cargar notifedo

//edoNum
  public String CD_ENFCIE = "";
  public String NM_CASOS = "";

  public DataNumerica(
      String it_solo_actualizar_notifedo,
      String it_solo_alta_notifedo,
      String it_actualizar_notif_sem,
      String cd_e_notif, String cd_anoepi, String cd_semepi,
      String fc_recep, String nm_nnotifr,
      String fc_fecnotif, String it_ressem,
      String cd_ope, String fc_ultact,
      String it_validada, String fc_fecvalid,
      String cd_enfcie, String nm_casos,
      String nm_nnotift_antigua,
      String nm_nnotift_nueva,
      String nm_ntotreal) {

    IT_SOLO_ACTUALIZAR_NOTIFEDO = it_solo_actualizar_notifedo;
    IT_SOLO_ALTA_NOTIFEDO = it_solo_alta_notifedo;
    IT_ACTUALIZAR_NOTIF_SEM = it_actualizar_notif_sem;

    NM_NNOTIFT_ANTIGUA = nm_nnotift_antigua;
    NM_NNOTIFT_NUEVA = nm_nnotift_nueva;
    NM_NTOTREAL = nm_ntotreal;

    CD_E_NOTIF = cd_e_notif;
    CD_ANOEPI = cd_anoepi;
    CD_SEMEPI = cd_semepi;
    FC_RECEP = fc_recep;
    NM_NNOTIFR = nm_nnotifr;
    FC_FECNOTIF = fc_fecnotif;
    IT_RESSEM = it_ressem;
    CD_OPE = cd_ope;
    FC_ULTACT = fc_ultact;
    IT_VALIDADA = it_validada;
    FC_FECVALID = fc_fecvalid;

    CD_ENFCIE = cd_enfcie;
    NM_CASOS = nm_casos;
  }

  public String getIT_SOLO_ALTA_NOTIFEDO() {
    return IT_SOLO_ALTA_NOTIFEDO;
  }

  public String getIT_SOLO_ACTUALIZAR_NOTIFEDO() {
    return IT_SOLO_ACTUALIZAR_NOTIFEDO;
  }

  public String getIT_ACTUALIZAR_NOTIF_SEM() {
    return IT_ACTUALIZAR_NOTIF_SEM;
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getCD_ANOEPI() {
    return CD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return CD_SEMEPI;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public String getIT_VALIDADA() {
    return IT_VALIDADA;
  }

  public String getFC_FECVALID() {
    return FC_FECVALID;
  }

  public String getFC_RECEP() {
    return FC_RECEP;
  }

  public String getNM_NNOTIFR() {
    return NM_NNOTIFR;
  }

  public String getFC_FECNOTIF() {
    return FC_FECNOTIF;
  }

  public String getIT_RESSEM() {
    return IT_RESSEM;
  }

  public String getCD_ENFCIE() {
    return CD_ENFCIE;
  }

  public String getNM_CASOS() {
    return NM_CASOS;
  }

  public String getNM_NNOTIFT_ANTIGUA() {
    return NM_NNOTIFT_ANTIGUA;
  }

  public String getNM_NNOTIFT_NUEVA() {
    return NM_NNOTIFT_NUEVA;
  }

  public String getNM_NTOTREAL() {
    return NM_NTOTREAL;
  }
} //fin Class
