
package eqNot;

import java.io.Serializable;

public class DataNotifSem
    implements Serializable {

  protected int iNM_TOTREAL = 0;
  protected int iNM_NOTIFT = 0;
  protected String sCD_OPE = "";
  protected String sCD_E_NOTIF = "";
  protected String sCD_SEMEPI = "";
  protected String sCD_ANOEPI = "";

  public DataNotifSem(int numTot, int numNotif, String cdOpe, String equipo,
                      String semana, String anyo) {

    iNM_TOTREAL = numTot;
    iNM_NOTIFT = numNotif;
    sCD_OPE = cdOpe;
    sCD_E_NOTIF = equipo;
    sCD_SEMEPI = semana;
    sCD_ANOEPI = anyo;
  }

  public int getNotifTotal() {
    return iNM_NOTIFT;
  }

  public int getNotifReal() {
    return iNM_TOTREAL;
  }

  public String getCdOperador() {
    return sCD_OPE;
  }

  public String getCdEquipo() {
    return sCD_E_NOTIF;
  }

  public String getSemana() {
    return sCD_SEMEPI;
  }

  public String getAnyo() {
    return sCD_ANOEPI;
  }

}
