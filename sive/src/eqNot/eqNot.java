package eqNot;

import java.util.ResourceBundle;

import capp.CApp;

public class eqNot
    extends CApp {
  ResourceBundle res;

  public void init() {
    super.init();

  }

  public void start() {
//    CApp a = (CApp) this;
    res = ResourceBundle.getBundle("eqNot.Res" + this.getIdioma());
    setTitulo(res.getString("msg14.Text"));
    VerPanel("", new Pan_CN(this));
  }

}