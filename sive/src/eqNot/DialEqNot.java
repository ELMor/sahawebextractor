
package eqNot;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Event;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import cn.DataCN;
//import enfermo.SrvEnfermo;
import enfermo.comun;
import fechas.conversorfechas;
import mantus.DataUsuario;
import notdata.DataEntradaEDO;
import notutil.CListaNiveles1;
import sapp.StubSrvBD;
import utilidades.PanFechasEnDialogo;
import zbs.DataZBS;
import zbs.DataZBS2;

public class DialEqNot
    extends CDialog {

  //Modos de operaci�n de la ventana
  public static final int modoALTA = 0;
  ResourceBundle res;
  public static final int modoMODIFICAR = 1;
  public static final int modoESPERA = 2;
  public boolean fisico = false;

  //Modos de operaci�n del Servlet generales
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //Modos de operaci�n del servlet de autorizaciones (sive para autor. y descipciones)
  final String strSERVLETPerfil = "servlet/SrvAutorizaciones";
  final int servletOBTENER_MAX_SEMANA = 30;
  final int servlet_TRAER_DESCRIPCIONES = 50;
  final int servlet_TRAER_AUTORIZACIONES = 51;
  final int servlet_TRAER_DESC_Y_AUT = 52;

  //Modos op servlet Entrada EDO
  final String strSERVLET_NIVELES = "servlet/SrvEntradaEDO";
  // Consultas de servlet EntradaEdo `paq notificaciones)
  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletRESTRICCION_NIVEL2 = 11;

  final int servletSELECCION_ZBS_X_CODIGO = 11;
  final int servletOBTENER_ZBS_X_CODIGO = 12;
  final int servletSELECCION_ZBS_X_DESCRIPCION = 13;
  final int servletOBTENER_ZBS_X_DESCRIPCION = 14;

  // constantes del panel
  final String imgLUPA = "images/Magnify.gif";
  final String imgALTA = "images/alta.gif";
  final String imgMODIFICACION = "images/modificacion.gif";
  final String imgBORRAR = "images/baja.gif";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETCentro = "servlet/SrvCN";
  final String strSERVLETZona = "servlet/SrvZBS";

  // listas de resultados
  protected CLista listaResultadoCentro = null;
  protected CLista listaResultadoEquipo = null;
  protected CLista listaResultadoZona = null;

  protected String zonaActual = null;
  protected String centroActual = null;
  protected String nivel1Actual = null;
  protected String nivel2Actual = null;
  protected String anioActual = null;
  protected String semanaActual = null;

  public DataEqNot datoA�adido = null;
  protected int m_opcion;
  // Informaci�n del usuario
  public DataUsuario usuario = null;
  public DataAutoriza perfil_Usuario = null;
  public String perfil = "";
  public String perfil_nivel1 = "";
  public String perfil_nivel2 = "";
  public CLista lRes = null;

  protected boolean cobertura = false;
  /** true si la primera vez a�io tiene letra */
//  protected boolean anioLetra = false;

  final int listaCentro = 0;
  final int listaEquipo = 1;
  final int listaZona = 2;

  protected int xl = 126;
  protected int yl = 7;

  protected int listaActual = listaCentro;

  // bot�n pulsado
  protected String componente;

  // par�metros
  protected int modoOperacion = modoALTA;
  protected int modoAnt = modoALTA;
  protected CLista lista = null;
  protected StubSrvBD stubCentro = null;
  protected StubSrvBD stubEquipo = null;
  protected StubSrvBD stubZona = null;
  protected StubSrvBD stubUsuario = null;
  protected StubSrvBD stubPerfil = null;
  //Niveles 1 y 2
  protected StubSrvBD stubCliente = null;

  protected String sCodBk = null;
  XYLayout xYLayout1 = new XYLayout();
  CCampoCodigo txtCentro = new CCampoCodigo();
  Label lblCentro = new Label();
  ButtonControl btnCentro = new ButtonControl();
  TextField txtCentroDesc = new TextField();
  TextField txtEquipoDesc = new TextField();
  Label lblEquipo = new Label();
  CCampoCodigo txtEquipo = new CCampoCodigo();
  Label lblSemana = new Label();
  Label lblAnyo = new Label();
  Label lblTelefono = new Label();
  TextField txtTelefono = new TextField();
  Label lblResponsable = new Label();
  TextField txtNombre = new TextField();
  Label lblNombre = new Label();
  TextField txtFax = new TextField();
  Label lblFax = new Label();
  TextField txtNotif = new TextField();
  Label lblNotif = new Label();
  Checkbox checkBaja = new Checkbox();
  //AIC
  PanFechasEnDialogo fechaBaja; //Lo inicializaremos cuando tengamos datos

  ButtonControl btnModificar = new ButtonControl();
  ButtonControl btnAlta = new ButtonControl();
  Label lblZona = new Label();
  ButtonControl btnZona = new ButtonControl();
  TextField txtZona = new TextField();
  TextField txtDescZona = new TextField();

  PanFechasEnDialogo panFec; //Se inicializa cuando se tenga a�o y semana

//_______________________________________________________________________

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:

        txtCentro.setEnabled(true);
        txtCentroDesc.setEnabled(false);
        txtCentroDesc.setEditable(false);
        txtCod1.setEnabled(true);
        btnSearchNiv1.setEnabled(true);
        btnCentro.setEnabled(true);
        btnCentro.setSelected(false);
        txtEquipo.setEnabled(true);
        txtEquipoDesc.setEnabled(true);
//        txtAnyo.setEditable(true);
//        txtSemana.setEditable(true);
//        txtAnyo.setEnabled(true);
//        txtSemana.setEnabled(true);

        if (perfil.compareTo("5") == 0) {
          btnZona.setEnabled(false);
        }
        else {
          btnZona.setEnabled(true);

        }
        btnZona.setSelected(false);
        txtNombre.setEnabled(true);
        txtTelefono.setEnabled(true);
        txtFax.setEnabled(true);
        txtNotif.setEnabled(true);
        checkBaja.setEnabled(false);
        //AIC
        fechaBaja.setVisible(false);
        //fechaBaja.setEditable(false);

        checkBaja.setState(false);
        //boton de aceptar solo habilitado en usuarios con perfil adecuado
        if ( (perfil.compareTo("1") == 0) ||
            (perfil.compareTo("2") == 0) ||
            ( (perfil.compareTo("3") == 0) && (perfil_nivel1 != null)) ||
            ( (perfil.compareTo("4") == 0) && (perfil_nivel2 != null))) {
          btnAlta.setEnabled(true);
        }
        else {
          btnAlta.setEnabled(false);
        }
        btnModificar.setEnabled(true);

        InicializaNiveles();

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:
        btnCentro.setEnabled(false);
        // no se puede cambiar el centro
        txtCentro.setEnabled(false);
        btnCentro.setSelected(false);
        txtCentroDesc.setEnabled(false);
        txtCentroDesc.setEditable(false);
        txtCod1.setEnabled(true);
        btnSearchNiv1.setEnabled(true);
        txtEquipoDesc.setEnabled(true);
        txtEquipo.setEnabled(false);
//          txtAnyo.setBackground(new Color(255, 255, 150));
//          txtSemana.setBackground(new Color(255, 255, 150));
//          txtAnyo.setEditable(true);
//          txtSemana.setEditable(true);
//          txtAnyo.setEnabled(true);
//          txtSemana.setEnabled(true);

        if (perfil.compareTo("5") == 0) {
          btnZona.setEnabled(false);
        }
        else {
          btnZona.setEnabled(true);
        }
        txtNombre.setEnabled(true);
        txtTelefono.setEnabled(true);
        txtFax.setEnabled(true);
        txtNotif.setEnabled(true);

        checkBaja.setEnabled(true);
        //AIC
        fechaBaja.setVisible(checkBaja.getState());
        //fechaBaja.setEditable(true);

        btnAlta.setEnabled(true);
        //boton de aceptar solo habilitado en usuarios con perfil adecuado
        if ( (perfil.compareTo("1") == 0) ||
            (perfil.compareTo("2") == 0) ||
            ( (perfil.compareTo("3") == 0) && (perfil_nivel1 != null)) ||
            ( (perfil.compareTo("4") == 0) && (perfil_nivel2 != null))) {
          btnAlta.setEnabled(true);
        }
        else {
          btnAlta.setEnabled(false);
        }
        btnModificar.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        InicializaNiveles();
        break;

      case modoESPERA:
        btnCentro.setEnabled(false);
        btnCentro.setSelected(false);
        txtEquipo.setEnabled(false);
        txtCentro.setEnabled(false);
        txtCentroDesc.setEnabled(false);
        txtDescZona.setEnabled(false);
        txtEquipoDesc.setEnabled(false);
//          txtAnyo.setEnabled(false);
//          txtSemana.setEnabled(false);
        txtZona.setEnabled(false);
        btnZona.setEnabled(false);
        btnZona.setSelected(false);
        txtNombre.setEnabled(false);
        txtTelefono.setEnabled(false);
        txtFax.setEnabled(false);
        txtNotif.setEnabled(false);
        checkBaja.setEnabled(false);
        //AIC
        fechaBaja.setVisible(false);
        //fechaBaja.setEditable(false);

        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        txtCod1.setEnabled(false);
        btnSearchNiv1.setEnabled(false);
        txtCod2.setEnabled(false);
        btnSearchNiv2.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

//_______________________________________________________________________

  // actualiza el estado de selecci�n del nivel 2
  public void InicializaNiveles() {

    //Si no hay niv 1 v�lido N2 y ZBS desactivados
    if (txtDes1.getText().length() == 0) {
      txtDes2.setText("");
      txtCod2.setText("");
      txtCod2.setEnabled(false);
      btnSearchNiv2.setEnabled(false);
      txtDescZona.setText("");
      txtZona.setText("");
      txtZona.setEnabled(false);
      btnZona.setEnabled(false);
    }
    //Si hay niv1 v�lido controles N2 activos
    else {
      txtCod2.setEnabled(true);
      btnSearchNiv2.setEnabled(true);
      //Si no hay niv 2 v�lido controles zona no activos
      if (txtDes2.getText().length() == 0) {
        txtDescZona.setText("");
        txtZona.setText("");
        txtZona.setEnabled(false);
        btnZona.setEnabled(false);
      }
      //Si tambi�n hayy niv 2 v�lido controles zona activos
      else {
        txtZona.setEnabled(true);
        btnZona.setEnabled(true);
      }
    }
  }

//_______________________________________________________________________

  // generador de cobertura para los equipos
  public void Genera_Cobertura_Equipo(String SemInicio, String elAnyo) {
    DataEqNot equipoN = null;
    DataAutoriza datos = null;
    CLista data = null;
    CMessage msgBox = null;

    try {
      data = new CLista();
      data.addElement(new DataAutoriza(SemInicio, "", elAnyo, "", ""));
      data.setFilter("");
      // idioma
      data.setIdioma(this.app.getIdioma());

      // apunta al servlet principal
      lRes = (CLista)this.stubPerfil.doPost(servletOBTENER_MAX_SEMANA, data);
    }
    catch (Exception erf) {
      msgBox = new CMessage(this.app, CMessage.msgERROR,
                            res.getString("msg2.Text"));
      msgBox.show();
    }
  }

//_______________________________________________________________________
  // contructor
  public DialEqNot(CApp a, PanelBusca pBusca, String titulo, int a_modo,
                   DataEqNot eqNotSel) {
    super(a);
    res = ResourceBundle.getBundle("eqNot.Res" + app.getIdioma());
    CMessage msgBox = null;
    CLista data = null;
    CLista lRes = null;

    try {
      setTitle(titulo);
      lista = new CLista();
      jbInit(eqNotSel);
      stubCentro = new StubSrvBD(new URL(this.app.getURL() + strSERVLETCentro));
      stubEquipo = new StubSrvBD(new URL(this.app.getURL() + strSERVLETEquipo));
      stubZona = new StubSrvBD(new URL(this.app.getURL() + strSERVLETZona));
      stubPerfil = new StubSrvBD(new URL(this.app.getURL() + strSERVLETPerfil));
      stubCliente = new StubSrvBD();
      this.doLayout();
      listaResultadoCentro = new CLista();
      listaResultadoCentro.setState(CLista.listaNOVALIDA);
      listaResultadoEquipo = new CLista();
      listaResultadoEquipo.setState(CLista.listaNOVALIDA);
      listaResultadoZona = new CLista();
      listaResultadoZona.setState(CLista.listaNOVALIDA);

      perfil = Integer.toString(app.getPerfil());
      btnCentro.setFocusAware(false);
      btnSearchNiv1.setFocusAware(false);
      btnSearchNiv2.setFocusAware(false);
      btnZona.setFocusAware(false);
      this.txtCentro.requestFocus();
      //Habr� consulta al servlet Si estamos en modo MODIFICAR
      //  (Se traen descripciones de N1,N2)
      if (a_modo == modoMODIFICAR) {

        // Ahora las autorizaciones y las descripciones de N1,N2,ZBS
        data = new CLista();
        lRes = new CLista();

        DataAutoriza datAut = new DataAutoriza(this.app.getLogin(), "", "", "",
                                               "");

        //En caso de estar en modo modificacion ( eqNotSel no es null)
        //Mete campos del equipo seleccionado para que se hallen sus descripciones en sigte consulta
        if (eqNotSel != null) {
          datAut.setCodNiv1(eqNotSel.getNiv1());
          datAut.setCodNiv2(eqNotSel.getNiv2());
          datAut.setCodZbs(eqNotSel.getZBS());

          txtCod1.setText(eqNotSel.getNiv1());
          nivel1Actual = eqNotSel.getNiv1();
          txtCod2.setText(eqNotSel.getNiv2());
          nivel2Actual = eqNotSel.getNiv2();
          txtZona.setText(eqNotSel.getZBS());
          zonaActual = eqNotSel.getZBS();

          // actualiza los datos de la cobertura
          cobertura = eqNotSel.estaEnCobertura();
          //# // System_out.println(cobertura);
        }
        data.addElement(datAut);

        data.setFilter("");
        data.setIdioma(this.app.getIdioma());
        data.setPerfil(app.getPerfil());
        data.setLogin(app.getLogin());

        // apunta al servlet de autorizaciones para recoger descripciones
        lRes = (CLista)this.stubPerfil.doPost(servletOBTENER_X_CODIGO, data);

        if (lRes.size() > 0) {
          // el item existe
          perfil_Usuario = (DataAutoriza) lRes.firstElement();

          //descripciones
          txtDes1.setText(perfil_Usuario.getDesNiv1());
          txtDes2.setText(perfil_Usuario.getDesNiv2());
          txtDescZona.setText(perfil_Usuario.getDesZbs());

        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg1.Text"));
          msgBox.show();
          msgBox = null;
        }
      } //if consulta a la b. datos

      if (eqNotSel != null) {
        rellenaPantalla(eqNotSel);
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
    modoOperacion = a_modo;
    m_opcion = a_modo;
    Inicializar();
  }

//_______________________________________________________________________

  /** para la sincronizaci�n */
  protected boolean sinBloquear = true;
  Label lblNiv1 = new Label();
  TextField txtCod1 = new TextField();
  ButtonControl btnSearchNiv1 = new ButtonControl();
  TextField txtDes1 = new TextField();
  Label lblNiv2 = new Label();
  TextField txtCod2 = new TextField();
  ButtonControl btnSearchNiv2 = new ButtonControl();
  TextField txtDes2 = new TextField();

//_______________________________________________________________________

  /** ete m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoOperacion = modoESPERA;
      Inicializar();
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

//_______________________________________________________________________

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = m_opcion;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

//_______________________________________________________________________

  // aspecto de la ventana
  //Se le pasa el eq seleccionado para que se pueda cree un PanFechas con datos recogodos (sem,a�o)

  private void jbInit(DataEqNot eqNotSel) throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        comun.imgLUPA,
        comun.imgACEPTAR,
        comun.imgCANCELAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    setSize(552, 479);
    xYLayout1.setHeight(479); //447);
    xYLayout1.setWidth(552); //406);
    this.setLayout(xYLayout1);

    btnCentro.setImage(imgs.getImage(0));
    btnSearchNiv1.setImage(imgs.getImage(0));
    btnSearchNiv2.setImage(imgs.getImage(0));

    //btnEquipo.setImage(imgs.getImage(0));
    btnZona.setImage(imgs.getImage(0));
    btnAlta.setImage(imgs.getImage(1));
    btnModificar.setImage(imgs.getImage(2));

    lblNiv1.setText(this.app.getNivel1() + ":");
    lblNiv2.setText(this.app.getNivel2() + ":");

    btnCentro.addActionListener(new DialEqNot_btnCentro_actionAdapter(this));
    btnCentro.setEnabled(true);
    lblEquipo.setText(res.getString("lblEquipo.Text"));
    //btnEquipo.setEnabled(true);
    lblSemana.setText(res.getString("lblSemana.Text"));
    lblTelefono.setText(res.getString("lblTelefono.Text"));
    lblFax.setText(res.getString("lblFax.Text"));
    lblNotif.setText(res.getString("lblNotif.Text"));
    checkBaja.setLabel(res.getString("checkBaja.Label"));
    btnModificar.setLabel(res.getString("btnModificar.Label"));
    btnAlta.setLabel(res.getString("btnAlta.Label"));
    btnAlta.addActionListener(new DialEqNot_btnAlta_actionAdapter(this));
    lblZona.setText(res.getString("lblZona.Text"));
    btnZona.addActionListener(new DialEqNot_btnZona_actionAdapter(this));
    btnZona.setEnabled(true);
    lblResponsable.setFont(new Font("Dialog", 1, 12));
    lblResponsable.setText(res.getString("lblResponsable.Text"));
    lblNombre.setText(res.getString("lblNombre.Text"));
    lblAnyo.setText(res.getString("lblAnyo.Text"));
    DialEqNot_txtCentro_actionAdapter btnCentroListener = new
        DialEqNot_txtCentro_actionAdapter(this);
    txtCentro.addActionListener(btnCentroListener);
    txtCentro.addKeyListener(new DialEqNot_txtCentro_keyAdapter(this));
    txtCentro.addFocusListener(btnCentroListener);
    DialEqNot_txtZona_actionAdapter btnZonaListener = new
        DialEqNot_txtZona_actionAdapter(this);
    txtZona.addKeyListener(new DialEqNot_txtZona_keyAdapter(this));
    txtZona.addActionListener(btnZonaListener);
    txtZona.addFocusListener(btnZonaListener);
    lblCentro.setText(res.getString("lblCentro.Text"));

    /// fijamos los campos obligatorios
    txtCentro.setBackground(new Color(255, 255, 150));
    txtEquipoDesc.setBackground(new Color(255, 255, 150));
    txtEquipo.setBackground(new Color(255, 255, 150));
    txtCod1.setBackground(new Color(255, 255, 150));
    txtCod2.setBackground(new Color(255, 255, 150));
    txtZona.setBackground(new Color(255, 255, 150));

    //// a�adimos los componentes
    this.add(lblEquipo, new XYConstraints(34, 30, 54, -1));
    this.add(txtEquipo, new XYConstraints(177, 30, 86, -1));
    this.add(txtEquipoDesc, new XYConstraints(312, 30, 201, -1));
    this.add(lblCentro, new XYConstraints(34, 61, -1, -1));
    this.add(txtCentro, new XYConstraints(177, 61, 86, -1));
    this.add(btnCentro, new XYConstraints(274, 61, -1, -1));
    this.add(txtCentroDesc, new XYConstraints(312, 61, 201, -1));

    this.add(lblNiv1, new XYConstraints(34, 93, -1, -1));
    this.add(txtCod1, new XYConstraints(177, 93, 86, -1));
    this.add(btnSearchNiv1, new XYConstraints(274, 93, -1, -1));
    this.add(txtDes1, new XYConstraints(312, 93, 201, -1));

    this.add(lblNiv2, new XYConstraints(34, 126, -1, -1));
    this.add(txtCod2, new XYConstraints(177, 126, 86, -1));
    this.add(btnSearchNiv2, new XYConstraints(274, 126, -1, -1));
    this.add(txtDes2, new XYConstraints(312, 126, 201, -1));

    this.add(lblZona, new XYConstraints(34, 159, -1, -1));
    this.add(txtZona, new XYConstraints(177, 159, 86, -1));
    this.add(btnZona, new XYConstraints(274, 159, -1, -1));
    this.add(txtDescZona, new XYConstraints(312, 159, 201, -1));

    //Panel fechas con ano,sem del eq selecciondo
    if (eqNotSel != null) {
      panFec = new PanFechasEnDialogo(this, false, eqNotSel.getAnyo(),
                                      eqNotSel.getSemana());
    }
    //O en caso de que no haya  ninguno sleccionado
    else {
      panFec = new PanFechasEnDialogo(this,
                                      PanFechasEnDialogo.modoSEMANA_ACTUAL, false);
    }
    this.add(panFec, new XYConstraints(31, 210, -1, -1));

    this.add(lblResponsable, new XYConstraints(31, 251, 90, -1));
    this.add(txtNombre, new XYConstraints(91, 288, 333, -1));
    this.add(lblNombre, new XYConstraints(31, 288, 54, -1));
    this.add(lblTelefono, new XYConstraints(31, 320, 54, -1));
    this.add(txtTelefono, new XYConstraints(91, 320, 106, -1));
    this.add(txtFax, new XYConstraints(319, 320, 103, -1));
    this.add(lblFax, new XYConstraints(255, 320, 54, -1));
    this.add(txtNotif, new XYConstraints(111, 352, 86, -1));
    this.add(lblNotif, new XYConstraints(31, 352, 79, -1));

    this.add(checkBaja, new XYConstraints(32, 386, -1, -1));
    //AIC
    //this.add(fechaBaja,new XYConstraints(210, 386,80,-1));
    if ( (eqNotSel != null) && (eqNotSel.getBaja().compareTo("S") == 0)) {
      fechaBaja = new PanFechasEnDialogo(this, false, eqNotSel.getAnyoBaja(),
                                         eqNotSel.getSemanaBaja());
    }
    else {
      fechaBaja = new PanFechasEnDialogo(this,
                                         PanFechasEnDialogo.modoSEMANA_ACTUAL, false);
    }

    this.add(fechaBaja, new XYConstraints(121, 386, -1, -1));

    this.add(btnAlta, new XYConstraints(329, 427, 80, 26));
    this.add(btnModificar, new XYConstraints(433, 427, 80, 26));
    txtDescZona.setEnabled(false);
    txtCentroDesc.setEnabled(false);
    txtEquipoDesc.setEnabled(false);

    centroActual = "";
    zonaActual = "";
    nivel1Actual = "";
    nivel2Actual = "";
    anioActual = "";
    semanaActual = "";
    txtCod2.addKeyListener(new DialEqNot_txtCod2_keyAdapter(this));
    txtCod1.addKeyListener(new DialEqNot_txtCod1_keyAdapter(this));
    txtCod2.addFocusListener(new DialEqNot_txtCod2_focusAdapter(this));
    txtCod1.addFocusListener(new DialEqNot_txtCod1_focusAdapter(this));
    btnSearchNiv2.addActionListener(new DialEqNot_btnSearchNiv2_actionAdapter(this));
    btnSearchNiv1.addActionListener(new DialEqNot_btnSearchNiv1_actionAdapter(this));
    btnModificar.addActionListener(new DialEqNot_btnModificar_actionAdapter(this));
    //AIC
    checkBaja.addItemListener(new DialEqNot_checkBaja_itemListener(this));

    txtDes1.setEditable(false);
    txtDes2.setEditable(false);
    txtDescZona.setEditable(false);
    txtDes1.setEnabled(false);
    txtDes2.setEnabled(false);
    txtDescZona.setEnabled(false);

    // establece el modo de operaci�n
    modoOperacion = modoALTA;
    Inicializar();

  }

  protected void obetnerAnoSemanaActual(StringBuffer anio, StringBuffer semana) {
    //Obtiene la fecha actual
    java.util.Date fecActual = new java.util.Date();

    try {
      //Pasa la fecha actual de Date a String
      SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
      String sFecActual = formater.format(fecActual);
      conversorfechas convDesde = conversorfechas.getConvDeFecha(sFecActual,
          app);
      semana.append(convDesde.getNumSem(sFecActual));
      anio.append(convDesde.getAno());
    }
    catch (Exception exc) {
      anio.append("0");
      semana.append("0");
    }
  }

//_______________________________________________________________________

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;
    //boolean b = true;

    if ( (txtEquipoDesc.getText().length() > 0) &&
        (txtCentro.getText().length() > 0) &&
        (txtEquipo.getText().length() > 0) &&
        (txtZona.getText().length() > 0)) {

      b = true;
      centroActual = txtCentro.getText().toUpperCase();
      String cadena = txtZona.getText().toUpperCase();
      int pos1 = cadena.indexOf('-');
      int pos2 = cadena.indexOf('-', pos1 + 1);
      if ( (pos1 != -1) && (pos2 != -1)) {
        // prepara los par�metros de env�o
        nivel1Actual = cadena.substring(0, pos1);
        nivel2Actual = cadena.substring(pos1 + 1, pos2);
        zonaActual = cadena.substring(pos2 + 1, cadena.length());
      }
      if (!cobertura) {
        StringBuffer anio = new StringBuffer(), sem = new StringBuffer();
        obetnerAnoSemanaActual(anio, sem);
        anioActual = anio.toString();
        semanaActual = sem.toString();
      }

      if (nivel1Actual == null || nivel1Actual.trim().length() == 0 ||
          nivel2Actual == null || nivel2Actual.trim().length() == 0 ||
          zonaActual == null || zonaActual.trim().length() == 0) {
        b = false;
        msg = res.getString("msg3.Text");
      }
      if (txtCentro.getText().length() > 6) {
        b = false;
        msg = res.getString("msg4.text");
        txtCentro.selectAll();
      }
      if (txtEquipo.getText().length() > 6) {
        b = false;
        msg = res.getString("msg4.text");
        txtEquipo.selectAll();
      }
      if (txtEquipoDesc.getText().length() > 50) {
        b = false;
        msg = res.getString("msg4.text");
        txtEquipoDesc.selectAll();
      }
      if (txtFax.getText().length() > 9) {
        b = false;
        msg = res.getString("msg4.text");
        txtFax.selectAll();
      }
      if (txtNombre.getText().length() > 40) {
        b = false;
        msg = res.getString("msg4.text");
        txtNombre.selectAll();
      }
      if (txtNotif.getText().length() > 3) {
        b = false;
        msg = res.getString("msg4.text");
        txtNotif.selectAll();
      }
      if (txtTelefono.getText().length() > 14) {
        b = false;
        msg = res.getString("msg4.text");
        txtTelefono.selectAll();
      }
      if ( (txtZona.getText().length()) > 8) {
        b = false;
        msg = res.getString("msg4.text");
        txtZona.selectAll();
      }
      if (ChequearEntero(txtNotif.getText(), 1) == false) {
        b = false;
        msg = res.getString("msg5.Text");
        txtNotif.selectAll();
      }

      if (panFec.isDataValid() == false) {
        b = false;
        msg = res.getString("msg6.Text");
      }

      // advertencia de requerir datos
    }
    else {
      msg = res.getString("msg7.Text");
    }

    //AIC
    int iAnyoAlta = Integer.parseInt(panFec.txtAno.getText());
    int iAnyoBaja = Integer.parseInt(fechaBaja.txtAno.getText());
    int iSemAlta = Integer.parseInt(panFec.txtCodSem.getText());
    int iSemBaja = Integer.parseInt(fechaBaja.txtCodSem.getText());
    if ( (iAnyoBaja < iAnyoAlta) ||
        ( (iAnyoBaja == iAnyoAlta) && (iSemBaja < iSemAlta))) {
      b = false;
      msg = res.getString("msg21.Text");
    }

    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }

    return b;
  }

//___________________________________________________________

//Entradas: 	Cadena a chequear, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   protected boolean ChequearEntero(String sDat, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que no admita vacio
     if (uTipo == 2) {
       if (sString.equals("")) {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     return (b);
   } //fin de ChequearEntero

  //_______________________________ Func a�adidaas____________________________

  void txtCod1_focusLost(FocusEvent e) {
    // datos de envio
    DataEntradaEDO data;
    CLista param = null;
    CMessage msg;
    String sDes1;
    int modo = modoOperacion;

    if (txtCod1.getText().length() > 0) {

//# // System_out.println("Tras entrar ************" +txtCod2.getText() );

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

//# // System_out.println("Tras inicializarr ************" +txtCod2.getText() );
      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataEntradaEDO(txtCod1.getText().toUpperCase(), "",
                                            ""));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();
//# // System_out.println("Tras incar otr vez ************" +txtCod2.getText() );
        param.setIdioma(app.getIdioma());
        param.setPerfil(app.getPerfil());
        param.setLogin(app.getLogin());
        //AIC: A�adimos la informaci�n de �reas para que funcionen las p�rdidas
        // de foco.
        param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIVELES));
        param = (CLista) stubCliente.doPost(servletOBTENER_NIV1_X_CODIGO, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataEntradaEDO) param.elementAt(0);

          txtCod1.setText(data.getCod());
          sDes1 = data.getDes();
          txtDes1.setText(sDes1);
          nivel1Actual = data.getCod();

//# // System_out.println("Tras traer datos************" +txtCod2.getText() );
          InicializaNiveles();
//# // System_out.println("incic nivelesr ************" +txtCod2.getText() );

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg8.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modo;
    Inicializar();
//# // System_out.println("Tras inicializar final ************" +txtCod2.getText() );

  }

//_______________________________________________________________________

  void txtCod2_focusLost(FocusEvent e) {

    // datos de envio
    DataEntradaEDO data;
    CLista param = null;
    CMessage msg;
    String sDes2;
    int modo = modoOperacion;

    if (txtCod2.getText().length() > 0) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataEntradaEDO(txtCod2.getText(), "",
                                            txtCod1.getText()));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        param.setIdioma(app.getIdioma());
        param.setPerfil(app.getPerfil());
        param.setLogin(app.getLogin());
        //AIC: A�adimos la informaci�n de �reas para que funcionen las p�rdidas
        // de foco.
        param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIVELES));
        param = (CLista) stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataEntradaEDO) param.elementAt(0);

          txtCod2.setText(data.getCod());
          sDes2 = data.getDes();

//REPASAR  Ver si interesa comparar DesL con "" o con null
//          if ((this.app.getIdioma() != CApp.idiomaPORDEFECTO) && (data.getDesL() !=null))
//          sDes2 = data.getDesL();

          txtDes2.setText(sDes2);

          nivel2Actual = data.getCod();

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg8.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modo;
    Inicializar();

  }

//_______________________________________________________________________

  void txtCod1_keyPressed(KeyEvent e) {
    txtDes1.setText("");
    InicializaNiveles();
  }

//_______________________________________________________________________

  void txtCod2_keyPressed(KeyEvent e) {
    txtDes2.setText("");
    InicializaNiveles();
  }

  protected void btnCentro_actionPerformed(ActionEvent e) {
    DataCN data = null;
    CMessage msgBox = null;
    int modo = modoOperacion;

    try {
      CListaCN lista = new CListaCN(this.app,
                                    res.getString("msg9.Text"),
                                    stubCentro,
                                    strSERVLETCentro,
                                    servletOBTENER_X_CODIGO,
                                    servletOBTENER_X_DESCRIPCION,
                                    servletSELECCION_X_CODIGO,
                                    servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataCN) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCentro.setText(data.getCodCentro());
      txtCentroDesc.setText(data.getCentroDesc());
      if (data.getCobertura().compareTo("S") == 0) {
        cobertura = true;
      }
      else {
        cobertura = false;
      }
    }
  }

  public void setComponente(String valor) {
    componente = valor;
  }

  public String getComponente() {
    return componente;
  }

  public void borraTodo() {
    txtCentro.setText("");
    txtCentroDesc.setText("");
    txtDescZona.setText("");
    txtEquipoDesc.setText("");
    txtFax.setText("");
    txtNombre.setText("");
    txtNotif.setText("");
    txtTelefono.setText("");
    txtZona.setText("");
  }

  void txtCentro_keyPressed(KeyEvent e) {
    txtCentroDesc.setText("");
    listaResultadoCentro.setState(CLista.listaNOVALIDA);
  }

  void txtCentro_actionPerformed(ActionEvent e) {
    DataCN data = null;
    CMessage msgBox = null;
    int modo = modoOperacion;
    CLista data1, result1;
    String strDesCentro = null, strCentro = null;

    try {
      strDesCentro = txtCentroDesc.getText();
      strCentro = txtCentro.getText();
      if (strCentro != null && strCentro.trim().length() > 0 &&
          (strDesCentro == null || strDesCentro.trim().length() == 0)) {
        data1 = new CLista();
        data1.setIdioma(app.getIdioma());
        DataCN datCN = new DataCN(strCentro);
        data1.addElement(datCN);
        URL u = new URL(app.getURL() + strSERVLETCentro);
        stubCentro.setUrl(u);
        result1 = (CLista) stubCentro.doPost(servletSELECCION_X_CODIGO, data1);
        if (result1 != null && result1.size() > 0) {
          data = (DataCN) result1.firstElement();
          strDesCentro = data.getCentroDesc();
          if (data.getCobertura().compareTo("S") == 0) {
            cobertura = true;
          }
          else {
            cobertura = false;
          }
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg10.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (strDesCentro != null) {
      txtCentroDesc.setText(strDesCentro);
    }
  }

  void txtZona_keyPressed(KeyEvent e) {
    txtDescZona.setText("");
    listaResultadoZona.setState(CLista.listaNOVALIDA);
  }

  protected boolean realiza_actionPerformed(ActionEvent evt) {
    if (m_opcion == modoALTA) {
      return btnAlta_actionPerformed(evt);
    }
    else if (m_opcion == modoMODIFICAR) {
      return btnModificar_actionPerformed(evt);
    }
    else {
      return false;
    }
  }

  void checkBaja_ItemStateChanged(ItemEvent e) {
    boolean estado = e.getStateChange() == e.SELECTED;
    //new CMessage(this.app,CMessage.msgERROR,String.valueOf(estado)).show();
    fechaBaja.setVisible(estado);
    //fechaBaja.setEditable(estado);
    fechaBaja.invalidate();
    this.validate();
  }

  protected boolean btnAlta_actionPerformed(ActionEvent e) {
    CLista datos = null;
    CLista ln1 = null;
    CMessage mensaje = null;
    String esBaja = null;
    String esCobertura = null;
    String elCodigo = "";
    DataEqNot elDato = null;
    String elNivel = null;
    int notificadores = 0;
    boolean result = false;

    ln1 = new CLista();
    modoAnt = modoOperacion;

    if (isDataValid()) {
      if (checkBaja.getState()) {
        esBaja = "S";
      }
      else {
        esBaja = "N";
      }

      if (txtNotif.getText().length() == 0) {
        notificadores = 0;
      }
      else {
        notificadores = Integer.parseInt(txtNotif.getText());
      }
      if (panFec.txtCodSem.getText().length() == 1) {
        panFec.txtCodSem.setText("0" + panFec.txtCodSem.getText());
      }
      // Aqu� insertar�amos la informaci�n
      try {
        //   if((modoOperacion==modoALTA))
        if ( (m_opcion == modoALTA)) {

          modoOperacion = modoESPERA;
          Inicializar();
          datos = new CLista();
          datos.setLogin(this.app.getLogin());
          datos.setFilter("");
          if (txtEquipo.getText().length() == 1) {
            elCodigo = "0" + txtEquipo.getText();
          }
          else {
            elCodigo = txtEquipo.getText();

          }
          elDato = new DataEqNot(elCodigo, txtEquipoDesc.getText(),
                                 txtCentro.getText(), nivel1Actual,
                                 nivel2Actual,
                                 panFec.txtCodSem.getText(),
                                 panFec.txtAno.getText(), zonaActual,
                                 txtNombre.getText(),
                                 txtTelefono.getText(), txtFax.getText(),
                                 notificadores,
                                 this.app.getLogin(), esBaja, cobertura /*AIC*/,
                                 fechaBaja.txtAno.getText(),
                                 fechaBaja.txtCodSem.getText());
          elDato.setDesCentro(txtCentroDesc.getText());
          datos.addElement(elDato);

          ln1 = (CLista)this.stubEquipo.doPost(modoALTA, datos);

          datos = null;
          borraTodo();
          txtEquipo.setText("");
          datoA�adido = elDato;
          result = true;
        }

      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        mensaje.show();
        modoOperacion = modoALTA;
      }
    }
    modoOperacion = modoAnt;
    Inicializar();
    return result;
  }

  void btnZona_actionPerformed(ActionEvent e) {
    DataEntradaEDO data;
    CMessage msgBox;
    int srvObXCod = 0;
    int srvObXDes = 0;
    int srvSelXCod = 0;
    int srvSelXDes = 0;

    modoAnt = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();

    try {

      CListaNivelesZBSIndiv lista = new CListaNivelesZBSIndiv(this,
          res.getString("msg11.Text"),
          stubCliente,
          strSERVLET_NIVELES,
          servletOBTENER_ZBS_X_CODIGO,
          servletOBTENER_ZBS_X_DESCRIPCION,
          servletSELECCION_ZBS_X_CODIGO,
          servletSELECCION_ZBS_X_DESCRIPCION);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();

      if (data != null) {
        txtZona.setText(data.getCod());
        txtDescZona.setText(data.getDes());
        zonaActual = data.getCod();
      }

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoAnt;
    Inicializar();

  }

  protected boolean btnModificar_actionPerformed(ActionEvent e) {
    CLista datos = null;
    CLista ln1 = null;
    CMessage mensaje = null;
    String esBaja = null;
    String esCobertura = null;
    DataEqNot elDato = null;
    String elNivel = null;
    boolean result = false;

    ln1 = new CLista();
    modoAnt = modoOperacion;
    if (isDataValid()) {
      modoOperacion = modoESPERA;
      Inicializar();
      if (checkBaja.getState()) {
        esBaja = "S";
      }
      else {
        esBaja = "N";
      }

      // Aqu� insertar�amos la informaci�n
      try {

        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.setFilter("");
        datoA�adido = new DataEqNot(txtEquipo.getText().toUpperCase(),
                                    txtEquipoDesc.getText(), centroActual,
                                    nivel1Actual,
                                    nivel2Actual, panFec.txtCodSem.getText(),
                                    panFec.txtAno.getText(), zonaActual,
                                    txtNombre.getText(),
                                    txtTelefono.getText(), txtFax.getText(),
                                    Integer.parseInt(txtNotif.getText()),
                                    this.app.getLogin(), esBaja,
                                    cobertura /*AIC*/, fechaBaja.txtAno.getText(),
                                    fechaBaja.txtCodSem.getText());
        datoA�adido.setDesCentro(txtCentroDesc.getText());
        datos.addElement(datoA�adido);
        ln1 = (CLista)this.stubEquipo.doPost(modoMODIFICAR, datos);
        datos = null;
        listaResultadoEquipo.setState(CLista.listaNOVALIDA);
        result = true;
        if (ln1.getFilter().compareTo("fisico") == 0) {
          fisico = true;
          (new CMessage(this.app, CMessage.msgAVISO,
                        "El notificador ha sido eliminado")).show();
        }
        else {
          fisico = false;

          //AIC
          DataEqNot resultado;

          try {
            resultado = (DataEqNot) ln1.firstElement();
          }
          catch (Exception ex) {
            resultado = null;
          }

          if (resultado != null && resultado.getBaja().equals("N") &&
              esBaja.equals("S")) {
            (new CMessage(this.app, CMessage.msgAVISO,
                          "El notificador no puede ser eliminado " +
                          "porque tiene notificaciones asociadas")).show();
            result = false;
          }
          //Fin - AIC

        }
      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        mensaje.show();
        modoOperacion = modoALTA;
      }
    }
    modoOperacion = modoMODIFICAR;
    Inicializar();
    return result;
  }

  void btnEquipo_actionPerformed(ActionEvent e) {
    DataEqNot data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaEqNot3 lista = new CListaEqNot3(this,
                                            res.getString("msg12.Text"),
                                            stubEquipo,
                                            strSERVLETEquipo,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataEqNot) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        txtEquipo.setText(data.getEquipo());
        txtEquipoDesc.setText(data.getDesEquipo());
        txtCentro.setText(data.getCentro());
        txtFax.setText(data.getFax());
        txtNombre.setText(data.getResponsable());
        txtNotif.setText(new Integer(data.getNotift()).toString());
        txtTelefono.setText(data.getTelefono());
        txtZona.setText(data.getNiv1() + "-" + data.getNiv2() + "-" +
                        data.getZBS());

        modoOperacion = modo;
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modo;
    }
    Inicializar();

  }

  void rellenaPantalla(DataEqNot data) {
    CMessage msgBox;

    if (data != null) {

      //Ya se han rellenado antes datos de N1,N2,ZBS
      //Y tambi�n los de a�o, sem
      txtEquipo.setText(data.getEquipo());
      txtEquipoDesc.setText(data.getDesEquipo());
//      txtAnyo.setText(data.getAnyo());
      txtCentro.setText(data.getCentro());
      txtCentroDesc.setText(data.getDesCentro());
      txtFax.setText(data.getFax());
      txtNombre.setText(data.getResponsable());
      txtNotif.setText(new Integer(data.getNotift()).toString());
//      txtSemana.setText(data.getSemana());
      txtTelefono.setText(data.getTelefono());
//      txtZona.setText(data.getNiv1()+"-"+data.getNiv2()+"-"+data.getZBS());
      if (data.getBaja().compareTo("S") == 0) {
        checkBaja.setState(true);
        //AIC
        fechaBaja.setVisible(true);
        //fechaBaja.setEditable(true);
        fechaBaja.invalidate();
        this.validate();
      }
      else {
        checkBaja.setState(false);
        //AIC
        fechaBaja.setVisible(false);
        fechaBaja.invalidate();
        this.validate();
      }

    }
  }

  void txtEquipo_keyPressed(KeyEvent e) {
    borraTodo();
    modoOperacion = modoALTA;
    Inicializar();
  }

  void txtZona_actionPerformed(ActionEvent e) {

    // datos de envio
    DataEntradaEDO data;
    CLista param = null;
    CMessage msg;
    String sDes2;
    int modo = modoOperacion;

    if (txtZona.getText().length() > 0) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataEntradaEDO(txtZona.getText(), "",
                                            txtCod1.getText(), txtCod2.getText()));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        param.setIdioma(app.getIdioma());
        param.setPerfil(app.getPerfil());
        param.setLogin(app.getLogin());
        //AIC: A�adimos la informaci�n de �reas para que funcionen las p�rdidas
        // de foco.
        param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIVELES));
        param = (CLista) stubCliente.doPost(servletOBTENER_ZBS_X_CODIGO, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataEntradaEDO) param.elementAt(0);

          txtZona.setText(data.getCod());
          sDes2 = data.getDes();

//REPASAR  Ver si interesa comparar DesL con "" o con null
//          if ((this.app.getIdioma() != CApp.idiomaPORDEFECTO) && (data.getDesL() !=null))
//          sDes2 = data.getDesL();

          txtDescZona.setText(sDes2);
          zonaActual = data.getCod();
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg8.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {

        //msg = new CMessage(this.app, CMessage.msgERROR, "ES AQU�");
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modo;
    Inicializar();

  }

  void txtEquipo_actionPerformed(ActionEvent e) {
    /*CMessage msgBox = null;
         DataEqNot datcn = null;
         CLista data = null;
         CLista lRes=null;
         lRes=new CLista();
         modoAnt=modoOperacion;
         listaResultadoEquipo.setState(CLista.listaNOVALIDA);
         try {
      // modo espera
      this.modoOperacion = modoESPERA;
      Inicializar();
      // prepara los par�metros de env�o
      data = new CLista();
      data.addElement(new DataEqNot(txtEquipo.getText(),"","","","","","","","","","",0,this.app.getLogin(),""));
      data.setFilter("");
      // idioma
      data.setIdioma( this.listaResultadoEquipo.getIdioma() );
      // apunta al servlet principal
      lRes = (CLista)this.stubEquipo.doPost(modoSELECCION, data);
      if (lRes.size() > 0) {
        // el item existe
        //this.modoOperacion = modoMODIFICAR;
        datcn = (DataEqNot) lRes.firstElement();
        // rellena los datos
        borraTodo();
        txtEquipo.setText(datcn.getEquipo());
        txtCentro.setText(datcn.getCentro());
        txtFax.setText(datcn.getFax());
        txtNombre.setText(datcn.getResponsable());
        txtNotif.setText(Integer.toString(datcn.getNotift()));
        txtSemana.setText(datcn.getSemana());
        txtTelefono.setText(datcn.getTelefono());
         txtZona.setText(datcn.getNiv1()+"-"+datcn.getNiv2()+"-"+datcn.getZBS());
        if(datcn.getBaja().compareTo("S")==0){
          checkBaja.setState(true);
        }else{
          checkBaja.setState(false);
        }
        txtZona_actionPerformed(null);
        txtCentro_actionPerformed(null);
        modoOperacion=modoMODIFICAR;
      }else {
        // el item no existe
        this.modoOperacion = modoALTA;
         msgBox = new CMessage(this.app, CMessage.msgAVISO, "No se encontr� Equipo.");
        msgBox.show();
        msgBox = null;
      }
         // error en el proceso
         } catch (Exception esr) {
      this.modoOperacion = modoALTA;
      esr.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, esr.getMessage());
      msgBox.show();
      msgBox = null;
         }
         Inicializar();*/
  }

  void btnSearchNiv2_actionPerformed(ActionEvent e) {
    /*
        DataZBS2 data;
        CMessage msgBox;
        int modo = modoOperacion;
        try{
        // apunta al servlet auxiliar
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIV2));
        modoOperacion = modoESPERA;
        Inicializar();
        CListaZBS2 lista = new CListaZBS2(this,
                                          "Selecci�n de "+app.getNivel2(),
                                          stubCliente,
                                          strSERVLET_NIV2,
                                          servletOBTENER_NIV2_X_CODIGO,
                                          servletOBTENER_NIV2_X_DESCRIPCION,
                                          servletSELECCION_NIV2_X_CODIGO,
                                          servletSELECCION_NIV2_X_DESCRIPCION);
        lista.show();
        data = (DataZBS2) lista.getComponente();
        modoOperacion = modo;
        if (data != null) {
          txtDes2.setText(data.getDes());
          txtCod2.setText(data.getNiv2());
        }
      }catch (Exception er){
          er.printStackTrace();
          msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
          msgBox.show();
          msgBox = null;
      }
      Inicializar();
     */

    CMessage msgBox;
    DataEntradaEDO data;
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    try {
      CListaNiveles2 lista = new CListaNiveles2(this,
                                                res.getString("msg13.Text"),
                                                stubCliente,
                                                strSERVLET_NIVELES,
                                                servletOBTENER_NIV2_X_CODIGO,
          servletOBTENER_NIV2_X_DESCRIPCION,
          servletSELECCION_NIV2_X_CODIGO,
          servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();

      if (data != null) { //hay datos
        txtCod2.setText(data.getCod());
        txtDes2.setText(data.getDes());

        nivel2Actual = data.getCod();

        //inicializa otras cajas de texto
        txtZona.setText("");
        txtDescZona.setText("");

      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;

    }

    modoOperacion = modo;
    Inicializar();

  }

  void btnSearchNiv1_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    DataEntradaEDO data;

    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    try {

      CListaNiveles1 lista = new CListaNiveles1(app,
                                                res.getString("msg13.Text"),
                                                stubCliente,
                                                strSERVLET_NIVELES,
                                                servletOBTENER_NIV1_X_CODIGO,
          servletOBTENER_NIV1_X_DESCRIPCION,
          servletSELECCION_NIV1_X_CODIGO,
          servletSELECCION_NIV1_X_DESCRIPCION);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();

      if (data != null) { //existen datos OK
        txtCod1.setText(data.getCod());
        txtDes1.setText(data.getDes());

        nivel1Actual = data.getCod(); //Para comprobaciones

        //inicializa otras cajas de texto
        txtCod2.setText("");
        txtDes2.setText("");
        txtZona.setText("");
        txtDescZona.setText("");

      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;

    }

    modoOperacion = modo;
    Inicializar();

    /*
        DataCat data;
        CMessage msgBox;
        int modo = modoOperacion;
        try{
        // apunta al servlet auxiliar
        modoOperacion = modoESPERA;
        Inicializar();
        CListaCat lista = new CListaCat(app,
                                        "Selecci�n de "+app.getNivel1(),
                                        stubCliente,
                                        strSERVLET_NIV1,
         servletOBTENER_X_CODIGO+Catalogo.catNIVEL1,
         servletOBTENER_X_DESCRIPCION+Catalogo.catNIVEL1,
         servletSELECCION_X_CODIGO+Catalogo.catNIVEL1,
         servletSELECCION_X_DESCRIPCION+Catalogo.catNIVEL1);
        lista.show();
        data = (DataCat) lista.getComponente();
        modoOperacion = modo;
        if (data != null) {
          txtCod1.setText(data.getCod());
          txtDes1.setText(data.getDes());
         //Han cambiado datos de nivel1. Se inicializa el nivel 2
         InicializaNiveles();
        }
      }catch (Exception er){
          er.printStackTrace();
          msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
          msgBox.show();
          msgBox = null;
      }
      Inicializar();
     */
  }

  /*
    void txtEquipo_focusLost(FocusEvent e) {
    }
    void txtCentro_focusLost(FocusEvent e) {
      // datos de envio
      DataCN data;
      CLista param = null;
      CMessage msg;
      int modo = modoOperacion;
      if (txtCentro.getText().length() > 0) {
      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();
        try {
          param = new CLista();
          param.addElement(new DataCN(txtCentro.getText().toUpperCase()));
          stubCliente.setUrl(new URL(app.getURL() + strSERVLETCentro));
          param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO , param);
          // rellena los datos
          if (param.size() > 0) {
            data = (DataCN) param.elementAt(0);
//# // System_out.println("Elemento en 0 ************* " +  data);
            txtCentro.setText(data.getCodCentro());
            txtCentroDesc.setText(data.getCentroDesc());
          }
          // no hay datos
          else {
//# // System_out.println("No hay datos ************* " );
            msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
            msg.show();
            msg = null;
          }
        } catch(Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }
      // modo normal
      modoOperacion = modo;
      Inicializar();
      }  //if
    }
   */

} //CLASE

//____________________________ Fin clase ppal _________________________
//______________________________________________________________________________

class DialEqNot_btnCentro_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialEqNot adaptee;
  ActionEvent e;

  DialEqNot_btnCentro_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnCentro_actionPerformed(e);
    adaptee.desbloquea();
  }
}

class DialEqNot_txtCentro_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialEqNot adaptee;

  DialEqNot_txtCentro_keyAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCentro_keyPressed(e);
  }
}

//Clase que escucha TODOS LOS EVENTOS de la caja de texto de centro
class DialEqNot_txtCentro_actionAdapter
    implements java.awt.event.FocusListener, java.awt.event.ActionListener,
    Runnable {
  DialEqNot adaptee;
  Event e;

  DialEqNot_txtCentro_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusGained(FocusEvent e) {
  }

  public void focusLost(FocusEvent e) {
    if (adaptee.bloquea()) {
      //this.e = (Event)e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      //this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.txtCentro_actionPerformed(null);
    adaptee.desbloquea();
  }
}

class DialEqNot_txtZona_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialEqNot adaptee;

  DialEqNot_txtZona_keyAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtZona_keyPressed(e);
  }
}

class DialEqNot_btnAlta_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialEqNot adaptee;
  ActionEvent e;

  DialEqNot_btnAlta_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    //adaptee.btnAlta_actionPerformed(e);
    if (adaptee.realiza_actionPerformed(e)) {
      adaptee.dispose();
    }
    adaptee.desbloquea();
  }
}

class DialEqNot_btnZona_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialEqNot adaptee;
  ActionEvent e;

  DialEqNot_btnZona_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnZona_actionPerformed(e);
    adaptee.desbloquea();
  }
}

class DialEqNot_btnModificar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialEqNot adaptee;
  ActionEvent e;

  DialEqNot_btnModificar_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    //  adaptee.btnModificar_actionPerformed(e);
    adaptee.dispose();
    adaptee.desbloquea();
  }
}

class DialEqNot_btnEquipo_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialEqNot adaptee;
  ActionEvent e;

  DialEqNot_btnEquipo_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnEquipo_actionPerformed(e);
    adaptee.desbloquea();
  }
}

class DialEqNot_txtEquipo_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialEqNot adaptee;

  DialEqNot_txtEquipo_keyAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtEquipo_keyPressed(e);
  }
}

class DialEqNot_txtZona_actionAdapter
    implements java.awt.event.FocusListener, java.awt.event.ActionListener,
    Runnable {
  DialEqNot adaptee;
  Event e;

  DialEqNot_txtZona_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusGained(FocusEvent e) {
  }

  public void focusLost(FocusEvent e) {
    if (adaptee.bloquea()) {
      //this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      //this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.txtZona_actionPerformed(null);
    adaptee.desbloquea();
  }
}

//?????????????????????????????????????????????????????????????????????????

class DialEqNot_txtEquipo_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialEqNot adaptee;
  ActionEvent e;

  DialEqNot_txtEquipo_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.txtEquipo_actionPerformed(e);
    adaptee.desbloquea();
  }
}

// lista de valores
class CListaEqNot3
    extends CListaValores {

  protected DialEqNot panel;

  public CListaEqNot3(DialEqNot p,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(p.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEqNot(s, "", panel.txtCentro.getText(), "", "", "", "", "",
                         "", "",
                         "", 0, this.app.getLogin(), "", false, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataEqNot) o).getEquipo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEqNot) o).getDesEquipo());
  }
}

// lista de valores
class CListaZBS3
    extends CListaValores {

  protected DialEqNot panel;

  public CListaZBS3(DialEqNot p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {

    super(p.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataAutoriza(this.app.getLogin(), panel.txtCod1.getText(), s,
                            panel.txtCod2.getText(), "");
  }

  public String getCodigo(Object o) {
    //return ( ((DataZBS)o).getCod1()+"-"+((DataZBS)o).getCod2()+"-"+((DataZBS)o).getCod() );
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

// lista de valores
class CListaCN
    extends CListaValores {

  public CListaCN(CApp a,
                  String title,
                  StubSrvBD stub,
                  String servlet,
                  int obtener_x_codigo,
                  int obtener_x_descricpcion,
                  int seleccion_x_codigo,
                  int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataCN(s, "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                      "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataCN) o).getCodCentro());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCN) o).getCentroDesc());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected DialEqNot panel;

  public CListaZBS2(DialEqNot p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCod1.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

class DialEqNot_txtCod1_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DialEqNot adaptee;
  FocusEvent e = null;

  DialEqNot_txtCod1_focusAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.txtCod1_focusLost(e);
  }
}

class DialEqNot_txtCod2_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DialEqNot adaptee;
  FocusEvent e = null;

  DialEqNot_txtCod2_focusAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {

    adaptee.txtCod2_focusLost(e);
  }
}

class DialEqNot_txtCod1_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialEqNot adaptee;

  DialEqNot_txtCod1_keyAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCod1_keyPressed(e);
  }
}

class DialEqNot_txtCod2_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialEqNot adaptee;

  DialEqNot_txtCod2_keyAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCod2_keyPressed(e);
  }
}

class DialEqNot_btnSearchNiv2_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialEqNot adaptee;
  ActionEvent e = null;

  DialEqNot_btnSearchNiv2_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btnSearchNiv2_actionPerformed(e);
  }
}

class DialEqNot_btnSearchNiv1_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialEqNot adaptee;
  ActionEvent e = null;

  DialEqNot_btnSearchNiv1_actionAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {

    adaptee.btnSearchNiv1_actionPerformed(e);
  }
}

/*
 class DialEqNot_txtEquipo_focusAdapter extends java.awt.event.FocusAdapter {
  DialEqNot adaptee;
  DialEqNot_txtEquipo_focusAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }
  public void focusLost(FocusEvent e) {
    adaptee.txtEquipo_focusLost(e);
  }
 }
 class DialEqNot_txtCentro_focusAdapter extends java.awt.event.FocusAdapter {
  DialEqNot adaptee;
  DialEqNot_txtCentro_focusAdapter(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }
  public void focusLost(FocusEvent e) {
    adaptee.txtCentro_focusLost(e);
  }
 }
 */

class DialEqNot_checkBaja_itemListener
    implements java.awt.event.ItemListener {
  DialEqNot adaptee;
  ItemEvent e = null;

  DialEqNot_checkBaja_itemListener(DialEqNot adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.e = e;
    adaptee.checkBaja_ItemStateChanged(e);
  }
}

class CListaNiveles2
    extends CListaValores {

  protected DialEqNot panel;

  public CListaNiveles2(DialEqNot p,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(p.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.txtCod1.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

// clase para obtener y seleccionar zbs
class CListaNivelesZBSIndiv
    extends CListaValores {

  protected DialEqNot panel;

  public CListaNivelesZBSIndiv(DialEqNot p,
                               String title,
                               StubSrvBD stub,
                               String servlet,
                               int obtener_x_codigo,
                               int obtener_x_descricpcion,
                               int seleccion_x_codigo,
                               int seleccion_x_descripcion) {
    super(p.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.txtCod1.getText(),
                              panel.txtCod2.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}
