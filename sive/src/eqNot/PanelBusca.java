
package eqNot;

import capp.CListaMantenimiento;
import sapp.StubSrvBD;

public class PanelBusca
    extends CListaMantenimiento {

  protected int iCatalogo;
  protected String sTitulo;
  public String elCentro = "";

  public PanelBusca(eqNot a) {
    super(a,
          4,
          "70\n210\n70\n210",
          a.res.getString("msg18.Text"),
          new StubSrvBD(),
          SrvEqNot.name,
//          SrvEqNot.servletSELECCION_X_CODIGO,
//          SrvEqNot.servletSELECCION_X_DESCRIPCION);
          SrvEqNot.servletSELECCION_TODO_X_CODIGO,
          SrvEqNot.servletSELECCION_TODO_X_DESCRIPCION);

//^^L Rivera : Ahora los modos del servlet var�an seg�n est� determinado el centro o no

    ConfigModo(true, true, false);
    this.setBorde(false);

  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    modoOperacion = modoESPERA;
    Inicializar();

    DialEqNot dial = new DialEqNot(app, this,
                                   ( (eqNot) app).res.getString("msg19.Text"),
                                   DialEqNot.modoALTA, null);
    dial.show();
    if (dial.datoA�adido != null) {
      lista.addElement(dial.datoA�adido);
      RellenaTabla();
    }
    dial = null;

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    modoOperacion = modoESPERA;
    Inicializar();

    int index = tabla.getSelectedIndex();

    DataEqNot dato = null;
    if (lista != null && lista.size() > index) {
      dato = (DataEqNot) lista.elementAt(index);
    }
    DialEqNot dial = new DialEqNot(app, this,
                                   ( (eqNot) app).res.getString("msg20.Text"),
                                   DialEqNot.modoMODIFICAR, dato);
    dial.show();
    // modificamos los datos
    if (dial.datoA�adido != null) {
      lista.removeElementAt(index);
      if (!dial.fisico) {
        lista.insertElementAt(dial.datoA�adido, index);
      }
      RellenaTabla();
    }
    dial = null;
    // reescribimos la tabla

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {
    // no tiene sentido
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataEqNot(s, "", elCentro, "", "", "", "", "", "", "", "", 0, "",
                         "", false, "", "");
  }

  /** para la sincronizaci�n */
  protected boolean sinBloquear = true;

  /** este m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
//     setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataEqNot cat = (DataEqNot) o;

    return cat.getEquipo() + "&" + cat.getDesEquipo() + "&" + cat.getCentro() +
        "&" + cat.getDesCentro();
  }

} //________________________________  END_CLASS