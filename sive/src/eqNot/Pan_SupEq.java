package eqNot;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import cn.DataCN;
import sapp.StubSrvBD;

public class Pan_SupEq
    extends CPanel {
  // modos de operación de la ventana
  final static int modoALTA = 0;
  ResourceBundle res;
  final static int modoMODIFICAR = 1;
  final static int modoBAJA = 2;
  final static int modoESPERA = 5;

  // modos de operación del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // imágenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  final String strSERVLET = "servlet/SrvCN";

  // parámetros
  protected int modoOperacion = modoALTA;
  protected int modoOperacionBk = modoALTA;
  protected StubSrvBD stubCliente = null;

  XYLayout xYLayout1 = new XYLayout();
  Label label1 = new Label();
  CCampoCodigo txtCentro = new CCampoCodigo();
  ButtonControl btnCentro = new ButtonControl();
  TextField txtDesCentro = new TextField();

  boolean check = false;

  public Pan_SupEq(eqNot a) {
    try {
      this.app = a;
      res = ResourceBundle.getBundle("eqNot.Res" + app.getIdioma());
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));
      jbInit();
      setBorde(false);
      txtDesCentro.setEnabled(false);
      txtDesCentro.setEditable(false);
      btnCentro.setFocusAware(false);
      Inicializar();
      txtCentro.requestFocus();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    xYLayout1.setHeight(34);
    label1.setText(res.getString("label1.Text"));
    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnCentro.setActionCommand("nivel1");
    btnCentro.setImage(imgs.getImage(0));

    txtDesCentro.setEditable(false);
    xYLayout1.setWidth(400);
    this.setLayout(xYLayout1);
    this.add(label1, new XYConstraints(10, 3, -1, -1));
    this.add(txtCentro, new XYConstraints(71, 3, 90, -1));
    this.add(btnCentro, new XYConstraints(167, 3, -1, -1));
    this.add(txtDesCentro, new XYConstraints(206, 3, 215, -1));
  }

  public void Inicializar() {
    switch (modoOperacion) {

      case modoALTA:
        txtDesCentro.setEnabled(true);
        txtCentro.setEnabled(true);
        btnCentro.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtDesCentro.setEnabled(false);
        txtCentro.setEnabled(false);
        btnCentro.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  void btnCentro_actionPerformed(ActionEvent e) {
    DataCN data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {
      check = true;

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      CListaCNPanSup lista = new CListaCNPanSup(app,
                                                res.getString("msg16.Text"),
                                                stubCliente,
                                                strSERVLET,
                                                servletOBTENER_X_CODIGO,
                                                servletOBTENER_X_DESCRIPCION,
                                                servletSELECCION_X_CODIGO,
                                                servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataCN) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        txtCentro.setText(data.getCodCentro());
        txtDesCentro.setText(data.getCentroDesc());

      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    Inicializar();

  }

  void txtCentro_keyPressed(KeyEvent e) {
    txtDesCentro.setText("");
  }

  void txtCentro_focusLost(FocusEvent e) {
    DataCN data;
    CLista param = null;
    CMessage msg;
    String sDes1;
    int modo = modoOperacion;

    if (txtCentro.getText().length() > 0) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataCN(txtCentro.getText().toUpperCase(), "", "",
                                    "",
                                    "", "", "", "", "", "", "", "", "", "", "",
                                    "", ""));

        //check = true;

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataCN) param.elementAt(0);

          txtCentro.setText(data.getCodCentro());
          sDes1 = data.getCentroDesc();
          txtDesCentro.setText(sDes1);

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg17.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modo;
    Inicializar();

  }

}

// lista de valores
class CListaCNPanSup
    extends CListaValores {

  public CListaCNPanSup(CApp p,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(p,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();

  }

  public Object setComponente(String s) {
    return new DataCN(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataCN) o).getCodCentro());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCN) o).getCentroDesc());
  }
}
