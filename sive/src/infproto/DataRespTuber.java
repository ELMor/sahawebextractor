/*
 * Creado: 04/01/2000 / Emilio Postigo Riancho
          Se crea, a partir de la estructura de datos DataResp, la
          estructura DataRespTuber, con los siguientes cambios:
          - Se a�ade el atributo sCodEq, necesario para efectuar el
            tratamiento de conflictos en tuberculosis, as� como un
            constructor que lo tiene en cuenta, m�s los m�todos set y
            get correspondientes.
          - Se a�aden los atributos sCdOpe y sFcUltAct para poder gestionar
            bloqueos sobre la tabla SIVE_EDOIND, as� como sus m�todos set
            y get correspondientes.
 */

package infproto;

import java.io.Serializable;

import comun.Fechas;
import comun.constantes;

public class DataRespTuber
    implements Serializable, Ordenable, Cloneable {

  private String NmEdo = null;
  private String CdTsive = constantes.CD_T_SIVE_EDO;
  private String CdModelo = null;
  private String NmLinea = null;
  private String CdPregunta = null;
  private String DsRespuesta = null; // V ??
  private String ValorLista = null; // var�n ??

  private String sCodEq = null;
  private String sCdOpe = null;
  private String sFcUltAct = null;

  public DataRespTuber() {

  }

  public DataRespTuber(int caso, String modelo, int linea,
                       String pregunta, String respuesta, String valorlista,
                       String equipo) {

    setNM_EDO(caso);
    setCD_MODELO(modelo);
    setNM_LINEA(linea);
    setCD_PREGUNTA(pregunta);
    setDS_RESPUESTA(respuesta);
    ValorLista = valorlista;
    setCD_E_NOTIF(equipo);

  } //fin constructor

  // M�todos de lectura
  public int getNM_EDO() {
    return new Integer(NmEdo).intValue();
  }

  public String getCD_TSIVE() {
    String s = "";
    if (CdTsive != null) {
      s = CdTsive;
    }
    return s;
  }

  public String getCD_MODELO() {
    String s = "";
    if (CdModelo != null) {
      s = CdModelo;
    }
    return s;
  }

  public int getNM_LINEA() {
    return new Integer(NmLinea).intValue();
  }

  public String getCD_PREGUNTA() {
    String s = "";
    if (CdPregunta != null) {
      s = CdPregunta;
    }
    return s;
  }

  public String getDS_RESPUESTA() {
    return DsRespuesta;
  }

  public String getCodEq() {
    String s = "";
    if (sCodEq != null) {
      s = sCodEq;
    }
    return s;
  }

  public java.sql.Timestamp getFC_ULTACT_EDOIND() {
    java.sql.Timestamp ts = new java.sql.Timestamp(0);
    if (sFcUltAct != null && !sFcUltAct.equals("")) {
      ts = Fechas.string2Timestamp(sFcUltAct);
    }
    return ts;
  }

  public String getCD_OPE_EDOIND() {
    String s = "";
    if (sCdOpe != null) {
      s = sCdOpe;
    }
    return s;
  }

  // M�todos de asignaci�n
  public void setNM_EDO(int i) {
    NmEdo = String.valueOf(i);
  }

  public void setCD_TSIVE(String s) {
    CdTsive = "";
    if (s != null) {
      CdTsive = s.trim();
    }
  }

  public void setCD_MODELO(String s) {
    CdModelo = "";
    if (s != null) {
      CdModelo = s.trim();
    }
  }

  public void setNM_LINEA(int i) {
    NmLinea = String.valueOf(i);
  }

  public void setCD_PREGUNTA(String s) {
    CdPregunta = "";
    if (s != null) {
      CdPregunta = s.trim();
    }
  }

  public void setDS_RESPUESTA(String s) {
    DsRespuesta = "";
    if (s != null) {
      DsRespuesta = s.trim();
    }
  }

  public void setCD_E_NOTIF(String s) {
    sCodEq = "";
    if (s != null) {
      sCodEq = s.trim();
    }
  }

  public void setFC_ULTACT_EDOIND(java.sql.Timestamp ts) {
    sFcUltAct = "";
    if (ts != null) {
      sFcUltAct = Fechas.timestamp2String(ts);
    }
  }

  public void setCD_OPE_EDOIND(String s) {
    sCdOpe = "";
    if (s != null) {
      sCdOpe = s.trim();
    }
  }

  /////  clonacion
  public Object clone() {

    DataRespTuber clonado = new DataRespTuber();
    clonado.NmEdo = new String(this.NmEdo);
    clonado.CdTsive = new String(this.CdTsive);
    clonado.CdModelo = new String(this.CdModelo);
    clonado.NmLinea = new String(this.NmLinea);
    clonado.CdPregunta = new String(this.CdPregunta);
    clonado.DsRespuesta = new String(this.DsRespuesta);
    //clonado.ValorLista= new String(this.ValorLista);
    clonado.sCodEq = new String(this.sCodEq);
    //clonado.sCdOpe= new String(this.sCdOpe);
    //clonado.sFcUltAct= new String(this.sFcUltAct);
    return clonado;

  } //fin constructor

  //************************************************

   // modificacion 19/06/2000
   // para poder ordenar vectores.Se utiliza en SrvRespTuber

   //Dice si este elemento es menor que el pasado como par�metro
   public boolean menorQue(Ordenable ord) {
     //Ser� menor si no es mayor y no es igual
     if (this.mayorQue(ord)) {
       return false;
     }
     if (this.igualQue(ord)) {
       return false;
     }
     else {
       return true;
     }
   }

  //Dice si este elemento es mayor que el pasado como par�metro
  public boolean mayorQue(Ordenable ord) {

    if (this.getCD_PREGUNTA().compareTo( ( (DataRespTuber) ord).getCD_PREGUNTA()) >
        0) {
      return true;
    }
    else {
      return false;
    }
  }

  //Dice si este elemento es igual que el pasado como par�metro
  public boolean igualQue(Ordenable ord) {
    if (this.getCD_PREGUNTA().compareTo( ( (DataRespTuber) ord).getCD_PREGUNTA()) ==
        0) {
      return true;
    }
    else {
      return false;
    }

  }

}
