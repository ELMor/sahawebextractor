/*
 * Creado: 20/02/2000 / Emilio Postigo Riancho
 */

package infproto;

import java.io.Serializable;

import comun.Fechas;

public class DataRespTuberCon
    implements Serializable {

  private String Ano_RTBC = null;
  private String Reg_RTBC = null;
  private int Contacto = -1;

  private String CdTsive = null;
  private String CdModelo = null;
  private String NmLinea = null;
  private String CdPregunta = null;
  private String DsRespuesta = null; // V ??
  private String ValorLista = null; // var�n ??

  private String sCodEq = null;
  private String sCdOpe = null;
  private String sFcUltAct = null;

  public DataRespTuberCon() {

  }

  public DataRespTuberCon(String cdTSive, String ano_RTBC, String reg_RTBC,
                          int contacto, String modelo, int linea,
                          String pregunta, String respuesta, String valorlista,
                          String equipo) {

    setCD_TSIVE(cdTSive);
    setANO_RTBC(ano_RTBC);
    setREG_RTBC(reg_RTBC);
    setCONTACTO(contacto);

    setCD_MODELO(modelo);
    setNM_LINEA(linea);
    setCD_PREGUNTA(pregunta);
    setDS_RESPUESTA(respuesta);
    ValorLista = valorlista;
    setCD_E_NOTIF(equipo);

  } //fin constructor

  // M�todos de lectura
  public String getANO_RTBC() {
    String s = "";
    if (Ano_RTBC != null) {
      s = Ano_RTBC;
    }
    return s;
  }

  public String getREG_RTBC() {
    String s = "";
    if (Reg_RTBC != null) {
      s = Reg_RTBC;
    }
    return s;
  }

  public int getCONTACTO() {
    return Contacto;
  }

  public String getCD_TSIVE() {
    String s = "";
    if (CdTsive != null) {
      s = CdTsive;
    }
    return s;
  }

  public String getCD_MODELO() {
    String s = "";
    if (CdModelo != null) {
      s = CdModelo;
    }
    return s;
  }

  public int getNM_LINEA() {
    return new Integer(NmLinea).intValue();
  }

  public String getCD_PREGUNTA() {
    String s = "";
    if (CdPregunta != null) {
      s = CdPregunta;
    }
    return s;
  }

  public String getDS_RESPUESTA() {
    String s = "";
    if (DsRespuesta != null) {
      s = DsRespuesta;
    }
    return s;
  }

  public String getCodEq() {
    String s = "";
    if (sCodEq != null) {
      s = sCodEq;
    }
    return s;
  }

  public java.sql.Timestamp getFC_ULTACT_EDOIND() {
    java.sql.Timestamp ts = new java.sql.Timestamp(0);
    if (sFcUltAct != null && !sFcUltAct.equals("")) {
      ts = Fechas.string2Timestamp(sFcUltAct);
    }
    return ts;
  }

  public String getCD_OPE_EDOIND() {
    String s = "";
    if (sCdOpe != null) {
      s = sCdOpe;
    }
    return s;
  }

  // M�todos de asignaci�n
  public void setANO_RTBC(String s) {
    Ano_RTBC = "";
    if (s != null) {
      Ano_RTBC = s.trim();
    }
  }

  public void setREG_RTBC(String s) {
    Reg_RTBC = "";
    if (s != null) {
      Reg_RTBC = s.trim();
    }
  }

  public void setCONTACTO(int contacto) {
    Contacto = contacto;
  }

  public void setCD_TSIVE(String s) {
    CdTsive = "";
    if (s != null) {
      CdTsive = s.trim();
    }
  }

  public void setCD_MODELO(String s) {
    CdModelo = "";
    if (s != null) {
      CdModelo = s.trim();
    }
  }

  public void setNM_LINEA(int i) {
    NmLinea = String.valueOf(i);
  }

  public void setCD_PREGUNTA(String s) {
    CdPregunta = "";
    if (s != null) {
      CdPregunta = s.trim();
    }
  }

  public void setDS_RESPUESTA(String s) {
    DsRespuesta = "";
    if (s != null) {
      DsRespuesta = s.trim();
    }
  }

  public void setCD_E_NOTIF(String s) {
    sCodEq = "";
    if (s != null) {
      sCodEq = s.trim();
    }
  }

  public void setFC_ULTACT_EDOIND(java.sql.Timestamp ts) {
    sFcUltAct = "";
    if (ts != null) {
      sFcUltAct = Fechas.timestamp2String(ts);
    }
  }

  public void setCD_OPE_EDOIND(String s) {
    sCdOpe = "";
    if (s != null) {
      sCdOpe = s.trim();
    }
  }

  //************************************************

}