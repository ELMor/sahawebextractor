//LUIS RIVERA
//Ordenable . Interfaz que deben implementar los elementos de una lista
// que sea de clase ListaOrdenable para que se pueda efectuar la ordenación

//NOTA: Ejemplo uso : Paquete gesvac.cliente.gv_mantpres
//Clases DlgInformeDetPedidos y DataInfDetPedidos
package infproto;

public interface Ordenable {

  //Dice si este elemento es mayor que el pasado como parámetro
  boolean mayorQue(Ordenable ord);

  //Dice si este elemento es igual al pasado como parámetro
  boolean igualQue(Ordenable ord);

  //Dice si este elemento es menor que el pasado como parámetro
  boolean menorQue(Ordenable ord);
}
