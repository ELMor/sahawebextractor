/*
 * Clase: SrvRespTuberCon
 * Paquete: infproto
 * Hereda: SrvDebugGeneral
 * Autor: Emilio Postigo Riancho
 * Fecha Inicio: 20/02/2000
 * Descripci�n: Servlet encargado de efectuar el mantenimiento de
               protocolos de contactos de Tuberculosis.
 */

package infproto;

//import tuberculosis.datos.notiftub.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Vector;

import capp.CLista;
import comun.SrvDebugGeneral;
import comun.constantes;

public class SrvRespTuberCon
    extends SrvDebugGeneral {

  // Modos de funcionamiento del servlet
  private final int modoALTA = constantes.modoALTA;
  private final int modoALTA_SB = constantes.modoALTA_SB;
  private final int modoMODIFICACION = constantes.modoMODIFICACION;
  private final int modoMODIFICACION_SB = constantes.modoMODIFICACION_SB;

  // modo para vaciar protocolo
  private final int modoVACIARPROT = constantes.modoVACIARPROT;

  // objetos JDBC
  Connection conexion = null;

  // Vector para almacenar las resp. de los datos de protocolo
  Vector vRespuestasProtocolo = null;

  // Fecha de actualizaci�n
  java.sql.Date dFechaAsignacion = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // Lista de valores de salida
    CLista clRetorno = null;

    // Valores de entrada
    CLista cListaEntrada = null;

    // Para saber si se bloquea o no
    boolean bBloqueo = true;

    if (super.con != null) {
      conexion = super.con; // Se est� en modo debug
    }
    else {
      conexion = openConnection(); // Modo real
    }

    conexion.setAutoCommit(false);

    dFechaAsignacion = new java.sql.Date(new java.util.Date().getTime());

    cListaEntrada = param;

    try {
      cargaVectorRespuestas(cListaEntrada);
      switch (opmode) {
        case modoALTA_SB:
          bBloqueo = false;
        case modoALTA:
          clRetorno = insertDatos(cListaEntrada);
          break;
        case modoMODIFICACION_SB:
          bBloqueo = false;
        case modoMODIFICACION:
          clRetorno = updateDatos(cListaEntrada);
          break;
        case modoVACIARPROT:
          clRetorno = deleteresp(cListaEntrada);
          break;
      }
      conexion.commit();
    }
    catch (Exception e) {
      conexion.rollback();
      // System_out.println(e.getMessage());
      throw e;
    }
    finally {
      closeConnection(conexion);
    }

    return clRetorno;
  }

  private CLista deleteresp(CLista p) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    String sCaso = null;
    sCaso = (String) ( (DataResp) ( (Vector) p.firstElement()).firstElement()).
        getCaso();
    int iCaso = (new Integer(sCaso).intValue());

    // Se borran  todos las respuestas del contacto
    sQuery = " delete SIVE_RESPCONTACTO where CD_ENFERMO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);

    int c = st.executeUpdate();
    st = null;

    return p;
  }

  // m�todos privados
  private void deleteDatos(String ano, String reg, int enfermo) throws
      Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " delete SIVE_RESPCONTACTO where CD_ENFERMO = ? and " +
        " CD_ARTBC = ? and CD_NRTBC = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, enfermo);
    st.setString(2, ano);
    st.setString(3, reg);

    st.executeUpdate();

  }

  // Actualizaci�n de un protocolo existente
  private CLista updateDatos(CLista p) throws Exception {

    // Se borran los datos antiguos
    deleteDatos(getAno_RTBC(), getReg_RTBC(), getContacto());

    // Se insertan los datos actuales
    insertDatos(p);

    return p;
  }

  // Inserci�n de la respuesta a una pregunta
  private void insertDatosPreguntaProtocolo(DataRespTuberCon drt) throws
      Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " insert into SIVE_RESPCONTACTO " +
        " (CD_TSIVE, CD_MODELO, NM_LIN, CD_ENFERMO, CD_PREGUNTA, " +
        " DS_RESPCONTAC, CD_ARTBC, CD_NRTBC) " +
        " values " +
        " (?, ?, ?, ?, ?, ?, ?, ?) ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, drt.getCD_TSIVE());
    st.setString(2, drt.getCD_MODELO());
    st.setInt(3, drt.getNM_LINEA());
    st.setInt(4, drt.getCONTACTO());
    st.setString(5, drt.getCD_PREGUNTA());
    st.setString(6, drt.getDS_RESPUESTA());
    st.setString(7, drt.getANO_RTBC());
    st.setString(8, drt.getREG_RTBC());

    st.executeUpdate();
  }

  private void deleteProtocoloNoOperativo(DataRespTuberCon drt) throws
      Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " delete from SIVE_RESPCONTACTO where " +
        "  CD_TSIVE = ? and CD_ENFERMO = ? and " +
        " CD_ARTBC = ? and CD_NRTBC = ? and " +
        " CD_MODELO not in (select CD_MODELO from SIVE_MODELO " +
        " where CD_TSIVE = ? and IT_OK = ? ) ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, drt.getCD_TSIVE());
    st.setInt(2, drt.getCONTACTO());
    st.setString(3, drt.getANO_RTBC());
    st.setString(4, drt.getREG_RTBC());
    st.setString(5, drt.getCD_TSIVE());
    st.setString(6, "S");

    int c = st.executeUpdate();
    trazaLog("registros borrados c " + c);

  }

  // Inserci�n de un nuevo protocolo (SIVE_RESPCONTACTO)
  private CLista insertDatos(CLista p) throws Exception {
    DataRespTuberCon dr = null;

    if (vRespuestasProtocolo.size() > 0) {
      for (int i = 0; i < vRespuestasProtocolo.size(); i++) {
        dr = (DataRespTuberCon) vRespuestasProtocolo.elementAt(i);
        // SIVE_RESPCONTACTO
        insertDatosPreguntaProtocolo(dr);

        // modificacion 27/07/2000
        // se borran aquellas respuestas que sean de modelos que no est�n
        // ya operativos, de cualquier tipo de usuario
        deleteProtocoloNoOperativo(dr);
      }
    }
    return p;
  }

  // Carga vector de respuestas
  private void cargaVectorRespuestas(CLista p) throws Exception {
    if (p != null) {
      if (p.size() > 0) {
        vRespuestasProtocolo = (Vector) p.elementAt(0);
      }
      else {
        throw new Exception("No hay respuestas");
      }
    }
    else {
      throw new Exception("No hay respuestas");
    }
  }

  // Obtiene A�o del RTBC
  private String getAno_RTBC() {
    String s = null;
    DataRespTuberCon dr = null;

    try {
      dr = (DataRespTuberCon) vRespuestasProtocolo.elementAt(0);
      s = dr.getANO_RTBC();
    }
    catch (Exception e) {

    }
    return s;
  }

  // Obtiene Registro del RTBC
  private String getReg_RTBC() {
    String s = null;
    DataRespTuberCon dr = null;

    try {
      dr = (DataRespTuberCon) vRespuestasProtocolo.elementAt(0);
      s = dr.getREG_RTBC();
    }
    catch (Exception e) {

    }
    return s;
  }

  // Obtiene Contacto del RTBC
  private int getContacto() {
    int i = -1;
    DataRespTuberCon dr = null;

    try {
      dr = (DataRespTuberCon) vRespuestasProtocolo.elementAt(0);
      i = dr.getCONTACTO();
    }
    catch (Exception e) {

    }
    return i;
  }

  // Bloqueos. Para actualizar SIVE_REGISTROTBC y SIVE_ENFERMO
  private CLista updateCaso(CLista p, boolean bloqueo) throws Exception {
    java.sql.Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());

    CLista cLista = p;
    /*    DatNotifTub dnt = null;
        PreparedStatement st = null;
        String sQuery = null;
        String sQueyBloqueo = null;
        CLista cLista = p;
        dnt = (DatNotifTub) ((Hashtable) cLista.elementAt(1)).get(constantes.DATOSNOTIFICADOR);
        sQuery = " update SIVE_EDOIND set CD_OPE = ?, FC_ULTACT = ? " +
                 " where " +
                 " NM_EDO = ? ";
        if (bloqueo) {
          sQueyBloqueo = " and CD_OPE = ? and FC_ULTACT = ? ";
        } else {
          sQueyBloqueo = "";
        }
        st = conexion.prepareStatement(sQuery + sQueyBloqueo);
        // Datos
        st.setString(1, cLista.getLogin());
        st.setTimestamp(2, ts);
        // Clave
        st.setInt(3, new Integer(dnt.getNM_EDO()).intValue() );
        // Bloqueo
        if (bloqueo) {
          st.setString(4, dnt.getCD_OPE_EDOIND());
         st.setTimestamp(5, Fechas.string2Timestamp(dnt.getFC_ULTACT_EDOIND()));
        }
        int iActualizados = st.executeUpdate();
        if (bloqueo) {
          if (iActualizados == 0) {
         throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_EDOIND);
          }
        }
        dnt.setCD_OPE_EDOIND(cLista.getLogin());
        dnt.setFC_ULTACT_EDOIND(Fechas.timestamp2String(ts));
        Hashtable h = new Hashtable();
        h.put(constantes.DATOSNOTIFICADOR, dnt);
        cLista.setElementAt(h, 1);*/

    return cLista;
  }

  // Para modo debug
  protected CLista doDebug() throws Exception {

    return new CLista();
  }

  /** prueba del servlet */
  public static void main(String[] args) {

  }

}
