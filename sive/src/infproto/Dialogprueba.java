package infproto;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CMessage;
import capp.CTabPanel;
import comun.DialogoGeneral;
import sapp.StubSrvBD;

public class Dialogprueba
    extends DialogoGeneral {

  //Ya hubo msg en protocolo
  public boolean YaSalioMsgEnferSinProtocolo = false;

  //Error en la construccion del dialogo
  public boolean bError = false;

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  protected StubSrvBD stubCliente = null;

  // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  //Aceptar == repetir query al volver
  //Cancelar == nada
  public int iOut = -1;

  //paneles
  public Panelprueba pProtocolo = null;

  //
  CApp capp;

  // modo de operaci�n del dialog
  public int modoOperacion = modoALTA;

  // controles
  XYLayout xYLayout = new XYLayout();
  XYLayout layoutBotones = new XYLayout();
  XYLayout layoutSuperior = new XYLayout();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  Panel panelBotones = new Panel();

  public CTabPanel tabPanel = new CTabPanel();

  // listeners
  DialogIactionAdapter actionAdapter = new DialogIactionAdapter(this);
  DialogIchkitemAdapter chkItemAdapter = new DialogIchkitemAdapter(this);

  //variables globales de entrada
  public Hashtable hashListas_completa = null;

  // constructor
  public Dialogprueba(CApp a,
                      Hashtable hashListas,
                      int modo) {
    super(a);
    try {
      capp = a;
      modoOperacion = modo;

      stubCliente = new StubSrvBD();
      hashListas_completa = (Hashtable) hashListas.clone();

      //paneles

      jbInit();
      pack();

      if ( (modoOperacion == modoMODIFICACION)
          || (modoOperacion == modoBAJA)
          || (modoOperacion == modoCONSULTA)) {

        Inicializar(modoESPERA);

        if (pProtocolo != null) {
          borrarProtocolo("Protocolo");
        }
        addProtocolo("Protocolo"); //esta en espera
      }

      modoOperacion = modo;
      Inicializar(modo);

    }
    catch (Exception ex) {
      ex.printStackTrace();
      bError = true;
    }
  }

  void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    // t�tulo

    // layout - controles
    xYLayout.setHeight(450);
    xYLayout.setWidth(780);
    setSize(780, 450);
    setLayout(xYLayout);

    if ( (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      btnAceptar.setLabel("Aceptar");
    }
    else {
      btnAceptar.setLabel("Grabar");

    }
    btnCancelar.setLabel("Salir");

    layoutBotones.setHeight(44);
    layoutBotones.setWidth(755);

    panelBotones.setLayout(layoutBotones);
    panelBotones.add(btnAceptar, new XYConstraints(530, 10, 100, -1)); //520
    panelBotones.add(btnCancelar, new XYConstraints(645, 10, 100, -1)); //635

    add(tabPanel, new XYConstraints(10, 120 - 20, 755, 286)); //10, 106, 755, 300
    add(panelBotones, new XYConstraints(10, 409 - 20, 755, 44 + 30)); //10, 409, 755, 44

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();

  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);

        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.WAIT_CURSOR);

        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);

        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICACION:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);

        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);

        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

        }
        if (modoOperacion == modoCONSULTA) {
          btnAceptar.setEnabled(false);

        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }

    //botones
    if ( (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      btnAceptar.setLabel("Aceptar");
    }
    else {
      btnAceptar.setLabel("Grabar");

    }
    btnCancelar.setLabel("Cancelar");

    doLayout();
  }

  //PROTOCOLO
  public void borrarProtocolo(String sPantallaMostrar) {

    //tabPanel.BorrarSolapa("Protocolo");
    //tabPanel.VerPanel(sPantallaMostrar);

  }

//SOLAPA PROTOCOLO (solo se abre en modif)
  public void addProtocolo(String sPantallaMostrar) {

    DataProtocolo data = new DataProtocolo();
    //data.sCodigo = "011";
    //modificacion 12/02/2001
    data.sCodigo = this.app.getParametro("CD_TUBERCULOSIS");

    data.sDescripcion = "Tuberculosis";
    data.sNivel1 = "1";
    data.sNivel2 = "1";
    data.NumCaso = "9999";
    data.sCa = app.getCA();

    // borramos la solapa por si acaso exist�a; as� actualizamos
    if (pProtocolo != null) {
      tabPanel.BorrarSolapa("Protocolo");
    }
    pProtocolo = null;
    pProtocolo = new Panelprueba(capp, data, this); //MIO!!!!!!!

    if (!pProtocolo.pnl.getNoSeEncontraronDatos()) {
      YaSalioMsgEnferSinProtocolo = false;
      tabPanel.InsertarPanel("Protocolo", pProtocolo);
      pProtocolo.doLayout();
      tabPanel.VerPanel(sPantallaMostrar);

    }
    else {
      if (!YaSalioMsgEnferSinProtocolo) {
        ShowWarning("El XX no tiene un protocolo definido.");

      }
      YaSalioMsgEnferSinProtocolo = true;
      pProtocolo = null;
    }

  } //ADDPROTOCOLO***********************************************

  void btnAceptar_actionPerformed() { //alta, modif, baja

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();

    if (modo == modoBAJA) {
      borrarXX();
      return;
    }
    else if (modo == modoMODIFICACION) {

      modificarXX();
      return;
    }
    else if (modo == modoALTA) {

      insertarXX();
      return;

    } //fin alta

  } //fin funcion ACEPTAR *****************************************

  private void borrarXX() {
    //va dentro del servlet
  }

  public void modificarXX() {

    //continuamos con el protocolo
    if (pProtocolo != null) {
      //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
      boolean b = pProtocolo.pnl.ValidarObligatorios();
      if (b) {
        pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo
        //dispose(); //Enrique quiere que se quede abto
        // estaba en la pesta�a de protocolo, continuara alli
      }
    }
    modoOperacion = modoMODIFICACION;
    Inicializar();
    //-----------------------------

  }

  private void insertarXX() {

    //continuamos con el protocolo
    if (pProtocolo != null) {
      //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
      boolean b = pProtocolo.pnl.ValidarObligatorios();
      if (b) {
        pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo
        //dispose(); //Enrique quiere que se quede abto
        // estaba en la pesta�a de protocolo, continuara alli
      }
    }
    modoOperacion = modoMODIFICACION;
    Inicializar();
    //-----------------------------

  } //fin de insertar_XX

  void btnCancelar_actionPerformed() {
    dispose();
  }

  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

} //FIN CLASE

class DialogIactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Dialogprueba adaptee;
  ActionEvent evt;

  DialogIactionAdapter(Dialogprueba adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
} //fin class

// listener
class DialogIchkitemAdapter
    implements java.awt.event.ItemListener {
  Dialogprueba adaptee;
  DialogIchkitemAdapter(Dialogprueba adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    //adaptee.chkEstado_itemStateChanged(e);
  }
}
