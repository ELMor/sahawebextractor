package infproto;

import java.io.Serializable;

//soporta los datos para hacer la query base
public class DataEnfEntCaso
    implements Serializable {

  protected String sCodEnf = "";
  protected String sCA = "";
  protected String sN1 = "";
  protected String sN2 = "";
  protected String sNM_EDO = "";

  protected String sCD_ARTBC = "";
  protected String sCD_NRTBC = "";
  protected int iContacto = -1;

  public DataEnfEntCaso(String CodEnf,
                        String sComunidad,
                        String Nivel1,
                        String Nivel2,
                        String Expediente) {

    sCodEnf = CodEnf;
    sCA = sComunidad;
    sN1 = Nivel1;
    sN2 = Nivel2;
    sNM_EDO = Expediente;
  }

  public DataEnfEntCaso(String CodEnf,
                        String sComunidad,
                        String Nivel1,
                        String Nivel2,
                        String Expediente,
                        String Ano_RTBC,
                        String Reg_RTBC,
                        int Contacto) {

    sCodEnf = CodEnf;
    sCA = sComunidad;
    sN1 = Nivel1;
    sN2 = Nivel2;
    sNM_EDO = Expediente;
    sCD_ARTBC = Ano_RTBC;
    sCD_NRTBC = Reg_RTBC;
    iContacto = Contacto;
  }

  public String getCodEnfermedad() {
    return sCodEnf;
  }

  public String getComunidad() {
    return sCA;
  }

  public String getNivelUNO() {
    return sN1;
  }

  public String getNivelDOS() {
    return sN2;
  }

  public String getNM_EDO() {
    return sNM_EDO;
  }

  public String getCD_ARTBC() {
    return sCD_ARTBC;
  }

  public String getCD_NRTBC() {
    return sCD_NRTBC;
  }

  public int getContacto() {
    return iContacto;
  }

}
