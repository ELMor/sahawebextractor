package bidial;

import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.event.ActionEvent;

import bidata.DatAlBro;
import bidata.DataAlerta;
import bidata.DataBrote;
import bidata.DataBusqueda;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import notutil.Comunicador;
import sapp.StubSrvBD;

/*
 * Entrada = titulo, a�o
 * Salida:
 *    lista tama�o 0, iOut=-1 : cancelar
 *    lista tama�o 1, iOut=0  : seleccion de una alerta
 *    lista tama�o 2, iOut=0  : seleccion de un brote
 *    iOut=-1 : error
 */

public class DiaBIAlt
    extends CDialog {

  public boolean bError = false;
  ResourceBundle res;
  public int hayDatos = -1;

  // modos de operaci�n del servlet
  final int servletSEL_ALTA_INF_BROTE = 0;
  final String strSERVLET_ALTA_INF_BROTE = "servlet/SrvBIAlt";

  //para ver si Aceptar=0 � Cancelar=-1;
  public int iOut = -1;

  // modos de actualizaci�n de la lista
  final int modoADD = 20;
  final int modoMODIFY = 21;
  final int modoDELETE = 22;

  // modo de operaci�n
  final int modoNORMAL = 0;
  final int modoESPERA = 1;
  protected int modoOperacion = modoNORMAL;

  //imagenes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/declaracion2.gif",
      "images/declaracion.gif"};
  protected CCargadorImagen imgs = null;

  // par�metros
  String cd_ano = new String();
  String titulo = "";

  //otros
  //para el servlet
  StubSrvBD stubCliente;

  //para la tabla
  DataAlerta alerta = new DataAlerta();
  DataBrote brote = new DataBrote();
  JCVector items = new JCVector();
  CLista listaTbl = new CLista();
  String ano_nm = new String();
  String fc_fechahora_f = new String();
  String fc_fechahora_h = new String();
  String ds_alerta = new String();
  String ds_brote = new String();
  String cd_sitalerbro = new String();

  //para el tramado
  CLista listaBD = new CLista();
  DataAlerta ultAlerta = new DataAlerta();
  DatAlBro ultBrote = new DatAlBro();

  ///????
  CLista listaRet = new CLista();

  // controles
  jclass.bwt.JCMultiColumnList tbl = new jclass.bwt.JCMultiColumnList();
  XYLayout xYLayout = new XYLayout();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  Panel panel = new Panel();
  XYLayout xYLayout2 = new XYLayout();

  // Listeners
  DiaBIAltactionAdapter actionAdapter = new DiaBIAltactionAdapter(this);
  DiaBIAltTblactionAdapter tblActionAdapter = new DiaBIAltTblactionAdapter(this);

  public DiaBIAlt(CApp a, String title, String ano) {
    super(a);
    titulo = title;
    cd_ano = ano;
    stubCliente = new StubSrvBD();

    try {
      res = ResourceBundle.getBundle("bidial.Res" + a.getIdioma());
      bError = !BuscarAlertasBrotes(cd_ano);
      jbInit();
      pack();
      RellenaLista(listaBD);
      //bError = false;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      bError = true;
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void jbInit() throws Exception {
    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout.setHeight(300);
    xYLayout.setWidth(557);
    setSize(557, 300);

    setTitle(titulo);

    setLayout(xYLayout);

    xYLayout2.setHeight(300);
    xYLayout2.setWidth(557);
    panel.setSize(557, 300);
    panel.setLayout(xYLayout2);

    this.setBackground(Color.lightGray);

    tbl.getList().setBackground(Color.white);
    tbl.getList().setHighlightColor(Color.lightGray);
    tbl.setInsets(new Insets(5, 5, 5, 5));
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "85\n248\n150\n25\n-998"), '\n'));
    tbl.setColumnLabelsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tbl.ColumnLabelsStrings1")), '\n'));
    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tbl.ColumnButtonsStrings1")), '\n'));
    tbl.setNumColumns(5);
    tbl.setColumnResizePolicy(1);
    /*
         tbl.setColumnButtons(JCUtilConverter.toStringList(new String("Fecha Notificacion\nFecha Recepcion\nR.\nNotif. Reales"), '\n'));
         tbl.setColumnWidths(JCUtilConverter.toIntList(new String("135\n135\n40\n115"), '\n'));
         tbl.setNumColumns(4);
     */
    tbl.setScrollbarDisplay(3);
    tbl.setAutoSelect(true);
    this.add(panel, new XYConstraints(0, 0, 557, 300));
    panel.add(tbl, new XYConstraints(10, 20, 536, 220));
    panel.add(btnAceptar, new XYConstraints(350, 251, 80, -1));
    panel.add(btnCancelar, new XYConstraints(452, 251, 80, -1));

    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label1"));
    btnAceptar.setActionCommand("aceptar");
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setActionCommand("cancelar");
    btnCancelar.setImage(imgs.getImage(1));

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);
    tbl.addActionListener(tblActionAdapter);

  }

  // habilita/deshabilita las opciones seg�n sea necesario
  void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:

        tbl.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        tbl.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  public void RellenaLista(CLista lista) {
    DataAlerta aler;
    DatAlBro brot;
    for (int i = 0; i < lista.size(); i++) {
      try {
        aler = (DataAlerta) lista.elementAt(i);
        addRowAler(aler);
      }
      catch (ClassCastException ex) {
        brot = (DatAlBro) lista.elementAt(i);
        addRowBro(brot);
      }
    }
    // verifica que sea la �ltima trama
    if (lista.getState() == CLista.listaINCOMPLETA) {
      tbl.addItem("M�s datos...");

    }
  }

  public void addRowAler(DataAlerta data) {
    int iImage = 0;

    ano_nm = data.getCD_ANO().trim() + "-" + data.getNM_ALERBRO().trim();
    fc_fechahora_f = data.getFC_FECHAHORA_F().trim();
    fc_fechahora_h = data.getFC_FECHAHORA_H().trim();
    ds_alerta = data.getDS_ALERTA().trim();
    cd_sitalerbro = data.getCD_SITALERBRO().trim();

    JCVector row1 = new JCVector();
    row1.addElement(ano_nm);
    row1.addElement(ds_alerta);
    row1.addElement(fc_fechahora_f + " " + fc_fechahora_h);
    row1.addElement(cd_sitalerbro);

    items.addElement(row1);
    listaTbl.addElement(data);
    tbl.setItems(items);
  }

  public void addRowBro(DatAlBro data) {
    int iImage = 0;

    ano_nm = data.getCD_ANO().trim() + "-" + data.getNM_ALERBRO().trim();
    fc_fechahora_f = data.getFC_FECHAHORA_F().trim();
    fc_fechahora_h = data.getFC_FECHAHORA_H().trim();
    ds_brote = data.getDS_BROTE().trim();
    cd_sitalerbro = data.getCD_SITALERBRO().trim();

    JCVector row1 = new JCVector();
    row1.addElement(ano_nm);
    row1.addElement(ds_brote);
    row1.addElement(fc_fechahora_f + " " + fc_fechahora_h);
    row1.addElement(cd_sitalerbro);

    items.addElement(row1);
    listaTbl.addElement(data);
    tbl.setItems(items);
  }

  /*
   * Devuelve el dato de la fila seleccionada en la tabla
   * Es una CLista porque si es una alerta se devuelve un DataAlerta
   * pero si es un brote se devuelve un DataAlerta y un DataBrote
   */
  public CLista getComponente() {
    Object o = null;

    if (iOut != -1) {
      o = listaTbl.elementAt(tbl.getSelectedIndex());

    }
    DataAlerta al = null;
    DatAlBro alBro = null;
    DataBrote bro = null;
    CLista res = new CLista();

    try {
      al = (DataAlerta) o;
      res.addElement(al);
      return res;
    }
    catch (ClassCastException ex) {
      alBro = (DatAlBro) o;
      al = new DataAlerta();
      al.insert( (String) "CD_ANO", alBro.getCD_ANO().trim());
      al.insert( (String) "NM_ALERBRO", alBro.getNM_ALERBRO().trim());
      al.insert( (String) "CD_SITALERBRO", alBro.getCD_SITALERBRO().trim());
      al.insert( (String) "FC_ALERBRO", alBro.getFC_ALERBRO().trim());

      bro = new DataBrote();
      bro.insert( (String) "CD_ANO", alBro.getCD_ANO().trim());
      bro.insert( (String) "NM_ALERBRO", alBro.getNM_ALERBRO().trim());
      bro.insert( (String) "FC_FECHAHORA_F", alBro.getFC_FECHAHORA_F().trim());
      bro.insert( (String) "FC_FECHAHORA_H", alBro.getFC_FECHAHORA_H().trim());

      if (alBro.getFC_FSINPRIMC_F() != null) {
        bro.insert( (String) "FC_FSINPRIMC_F", alBro.getFC_FSINPRIMC_F().trim());
      }
      if (alBro.getFC_FSINPRIMC_H() != null) {
        bro.insert( (String) "FC_FSINPRIMC_H", alBro.getFC_FSINPRIMC_H().trim());
      }
      if (alBro.getFC_ISINPRIMC_F() != null) {
        bro.insert( (String) "FC_ISINPRIMC_F", alBro.getFC_ISINPRIMC_F().trim());
      }
      if (alBro.getFC_ISINPRIMC_H() != null) {
        bro.insert( (String) "FC_ISINPRIMC_H", alBro.getFC_ISINPRIMC_H().trim());

      }
      if (alBro.getNM_PERINMIN() != null) {
        bro.insert( (String) "NM_PERINMIN", alBro.getNM_PERINMIN().trim());
      }
      if (alBro.getNM_PERINMAX() != null) {
        bro.insert( (String) "NM_PERINMAX", alBro.getNM_PERINMAX().trim());
      }
      if (alBro.getNM_PERINMED() != null) {
        bro.insert( (String) "NM_PERINMED", alBro.getNM_PERINMED().trim());

      }
      if (alBro.getNM_ENFERMOS() != null) {
        bro.insert( (String) "NM_ENFERMOS", alBro.getNM_ENFERMOS().trim());
      }
      if (alBro.getFC_EXPOSICION_F() != null) {
        bro.insert( (String) "FC_EXPOSICION_F", alBro.getFC_EXPOSICION_F().trim());
      }
      if (alBro.getFC_EXPOSICION_H() != null) {
        bro.insert( (String) "FC_EXPOSICION_H", alBro.getFC_EXPOSICION_H().trim());
      }
      if (alBro.getNM_INGHOSP() != null) {
        bro.insert( (String) "NM_INGHOSP", alBro.getNM_INGHOSP().trim());
      }
      if (alBro.getNM_DEFUNCION() != null) {
        bro.insert( (String) "NM_DEFUNCION", alBro.getNM_DEFUNCION().trim());
      }
      if (alBro.getNM_EXPUESTOS() != null) {
        bro.insert( (String) "NM_EXPUESTOS", alBro.getNM_EXPUESTOS().trim());

      }
      if (alBro.getCD_TRANSMIS() != null) {
        bro.insert( (String) "CD_TRANSMIS", alBro.getCD_TRANSMIS().trim());

      }
      if (alBro.getNM_NVACENF() != null) {
        bro.insert( (String) "NM_NVACENF", alBro.getNM_NVACENF().trim());

      }
      if (alBro.getCD_OPE() != null) {
        bro.insert( (String) "CD_OPE", alBro.getCD_OPE().trim());
      }
      if (alBro.getFC_ULTACT() != null) {
        bro.insert( (String) "FC_ULTACT", alBro.getFC_ULTACT().trim());

      }
      if (alBro.getNM_DCUACMIN() != null) {
        bro.insert( (String) "NM_DCUACMIN", alBro.getNM_DCUACMIN().trim());
      }
      if (alBro.getNM_DCUACMED() != null) {
        bro.insert( (String) "NM_DCUACMED", alBro.getNM_DCUACMED().trim());
      }
      if (alBro.getNM_DCUACMAX() != null) {
        bro.insert( (String) "NM_DCUACMAX", alBro.getNM_DCUACMAX().trim());

      }
      if (alBro.getNM_NVACNENF() != null) {
        bro.insert( (String) "NM_NVACNENF", alBro.getNM_NVACNENF().trim());
      }
      if (alBro.getNM_VACNENF() != null) {
        bro.insert( (String) "NM_VACNENF", alBro.getNM_VACNENF().trim());
      }
      if (alBro.getNM_VACENF() != null) {
        bro.insert( (String) "NM_VACENF", alBro.getNM_VACENF().trim());

      }
      if (alBro.getDS_OBSERV() != null) {
        bro.insert( (String) "DS_OBSERV", alBro.getDS_OBSERV().trim());

      }
      if (alBro.getCD_TIPOCOL() != null) {
        bro.insert( (String) "CD_TIPOCOL", alBro.getCD_TIPOCOL().trim());
      }
      if (alBro.getDS_NOMCOL() != null) {
        bro.insert( (String) "DS_NOMCOL", alBro.getDS_NOMCOL().trim());
      }
      if (alBro.getDS_DIRCOL() != null) {
        bro.insert( (String) "DS_DIRCOL", alBro.getDS_DIRCOL().trim());
      }
      if (alBro.getCD_POSTALCOL() != null) {
        bro.insert( (String) "CD_POSTALCOL", alBro.getCD_POSTALCOL().trim());
      }
      if (alBro.getCD_PROVCOL() != null) {
        bro.insert( (String) "CD_PROVCOL", alBro.getCD_PROVCOL().trim());

      }
      bro.insert( (String) "DS_BROTE", alBro.getDS_BROTE().trim());
      bro.insert( (String) "CD_GRUPO", alBro.getCD_GRUPO().trim());

      if (alBro.getNM_MANIPUL() != null) {
        bro.insert( (String) "NM_MANIPUL", alBro.getNM_MANIPUL().trim());

      }
      if (alBro.getCD_ZBS_LE() != null) {
        bro.insert( (String) "CD_ZBS_LE", alBro.getCD_ZBS_LE().trim());
      }
      if (alBro.getCD_NIVEL_2_LE() != null) {
        bro.insert( (String) "CD_NIVEL_2_LE", alBro.getCD_NIVEL_2_LE().trim());

      }
      bro.insert( (String) "CD_TNOTIF", alBro.getCD_TNOTIF().trim());

      if (alBro.getIT_RESCALC() != null) {
        bro.insert( (String) "IT_RESCALC", alBro.getIT_RESCALC().trim());

      }
      if (alBro.getCD_MUNCOL() != null) {
        bro.insert( (String) "CD_MUNCOL", alBro.getCD_MUNCOL().trim());
      }
      if (alBro.getCD_NIVEL_1_LCA() != null) {
        bro.insert( (String) "CD_NIVEL_1_LCA", alBro.getCD_NIVEL_1_LCA().trim());
      }
      if (alBro.getDS_TELCOL() != null) {
        bro.insert( (String) "DS_TELCOL", alBro.getDS_TELCOL().trim());
      }
      if (alBro.getCD_NIVEL_1_LE() != null) {
        bro.insert( (String) "CD_NIVEL_1_LE", alBro.getCD_NIVEL_1_LE().trim());
      }
      if (alBro.getCD_NIVEL_2_LCA() != null) {
        bro.insert( (String) "CD_NIVEL_2_LCA", alBro.getCD_NIVEL_2_LCA().trim());
      }
      if (alBro.getCD_ZBS_LCA() != null) {
        bro.insert( (String) "CD_ZBS_LCA", alBro.getCD_ZBS_LCA().trim());

      }
      if (alBro.getDS_NMCALLE() != null) {
        bro.insert( (String) "DS_NMCALLE", alBro.getDS_NMCALLE().trim());
      }
      if (alBro.getCD_TBROTE() != null) {
        bro.insert( (String) "CD_TBROTE", alBro.getCD_TBROTE().trim());
      }
      if (alBro.getIT_PERIN() != null) {
        bro.insert( (String) "IT_PERIN", alBro.getIT_PERIN().trim());
      }
      if (alBro.getIT_DCUAC() != null) {
        bro.insert( (String) "IT_DCUAC", alBro.getIT_DCUAC().trim());

      }
      if (alBro.getDS_MUNCOL() != null) {
        bro.insert( (String) "DS_MUNCOL", alBro.getDS_MUNCOL().trim());
      }
      if (alBro.getCD_CACOL() != null) {
        bro.insert( (String) "CD_CACOL", alBro.getCD_CACOL().trim());
      }
      if (alBro.getDS_NIVEL_1_LCA() != null) {
        bro.insert( (String) "DS_NIVEL_1_LCA", alBro.getDS_NIVEL_1_LCA().trim());
      }
      if (alBro.getDS_NIVEL_2_LCA() != null) {
        bro.insert( (String) "DS_NIVEL_2_LCA", alBro.getDS_NIVEL_2_LCA().trim());
      }
      if (alBro.getDS_ZBS_LCA() != null) {
        bro.insert( (String) "DS_ZBS_LCA", alBro.getDS_ZBS_LCA().trim());
      }
      if (alBro.getDS_NIVEL_1_LE() != null) {
        bro.insert( (String) "DS_NIVEL_1_LE", alBro.getDS_NIVEL_1_LE().trim());
      }
      if (alBro.getDS_NIVEL_2_LE() != null) {
        bro.insert( (String) "DS_NIVEL_2_LE", alBro.getDS_NIVEL_2_LE().trim());
      }
      if (alBro.getDS_ZBS_LE() != null) {
        bro.insert( (String) "DS_ZBS_LE", alBro.getDS_ZBS_LE().trim());

      }
      res.addElement(al);
      res.addElement(bro);
      return res;
    }

  }

  void tbl_actionPerformed() {
    CMessage msgBox = null;
    CLista data = null;
    CLista resul = null;
    int iLast;
    Object componente;

    // lista incompleta
    if ( (listaBD.getState() == CLista.listaINCOMPLETA) &&
        (tbl.getSelectedIndex() == (tbl.countItems() - 1))) {

      modoOperacion = modoESPERA;
      Inicializar();

      CLista listaBDAnt = new CLista();
      iLast = tbl.countItems() - 1;

      try {
        BuscarAlertasBrotes(cd_ano);
      }
      catch (Exception e) {
        ShowWarning(res.getString("msg14.Text"));
      }

      listaBDAnt.appendData(listaBD);
      listaBDAnt.setState(listaBD.getState());
      listaBD = listaBDAnt;

      RellenaLista(listaBD);

      modoOperacion = modoNORMAL;
      Inicializar();

      // item seleccionado
    }
    else {
      btnAceptar_actionPerformed();
    }
  }

  void btnAceptar_actionPerformed() {
    iOut = 0;
    dispose();
  }

  void btnCancelar_actionPerformed() {
    iOut = -1;
    dispose();
  }

  boolean BuscarAlertasBrotes(String ano) throws Exception {
    //try {

    CLista result = new CLista();
    CLista parametros = new CLista();

    // par�metros que se le pasan a DataAlerta: CD_ANO
    // que es modo? de momento le mando un 0
    DataBusqueda data = new DataBusqueda(ano, "", "", "", "", "", "", "", "",
                                         "", "", "", "", "", "", "",
                                         "", "", "", 0,
                                         (Vector)this.app.
                                         getCD_NIVEL_1_AUTORIZACIONES(),
                                         (Vector)this.app.
                                         getCD_NIVEL_2_AUTORIZACIONES());
    parametros.addElement(data);
    parametros.addElement(ultAlerta);
    parametros.addElement(ultBrote);

    //sin tramero NOTA
    result = Comunicador.Communicate(app,
                                     stubCliente,
                                     servletSEL_ALTA_INF_BROTE,
                                     strSERVLET_ALTA_INF_BROTE,
                                     parametros);

    //devuelve una lista con alertas y alertabrotes ordenadas por
    //fecha (decreciente)
    //si no se traen todos los datos, entonces devuelve tambien
    //los ultimos alerta y brotes (si corresponde)

    // comprueba que hay datos
    if (result.size() == 0) {
      ShowWarning(res.getString("msg15.Text"));
      hayDatos = 0;
      return false;

    }
    else {

      hayDatos = 1;
      CLista lis = new CLista();
      listaBD = (CLista) result.elementAt(0);

      if (result.size() == 2) {
        ultAlerta = (DataAlerta) result.elementAt(1);
      }
      else if (result.size() == 3) {
        ultAlerta = (DataAlerta) result.elementAt(1);
        ultBrote = (DatAlBro) result.elementAt(2);
      }
      //tengo los datos de todos los paneles

      // verifica que sea la �ltima trama
      if (result.getState() == CLista.listaINCOMPLETA) {
        listaBD.setState(CLista.listaINCOMPLETA);

      }
      return true;

    }
    /*
         } catch (Exception e) {
      ShowWarning("Error recuperando la lista de alertas y brotes");
      throw (e);
         }*/

  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

}

// escuchador de los click en los botones
class DiaBIAltactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaBIAlt adaptee;
  ActionEvent evt;

  DiaBIAltactionAdapter(DiaBIAlt adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
}

// escuchador de los click en la tabla
class DiaBIAltTblactionAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  DiaBIAlt adaptee;

  DiaBIAltTblactionAdapter(DiaBIAlt adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.tbl_actionPerformed();
  }
}
