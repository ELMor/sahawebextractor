package centinelas.cliente.c_impetiquetas;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CInicializar;
import centinelas.cliente.c_componentes.CListaMantSelMultiple;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DiaImpEtiquetas
    extends CDialog
    implements CInicializar {

  XYLayout xyLayout = new XYLayout();

  CListaMantSelMultiple clmPuntos = null;
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 20;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  //variable que dice si se ha realizado la operaci�n correctamente
  //o no
  boolean bAceptar = false;

  //lista que se devuelve a la pantalla anterior
  Lista lDev = null;

  //escuchadores
  btnAceptActionAdapter actionAdapter = new btnAceptActionAdapter(this);

  public DiaImpEtiquetas(CApp a) {
    super(a);
    Vector vLabels = new Vector();
    try {
      vLabels.addElement(new CColumna("Punto Centinela",
                                      "200",
                                      "PUNTO_CENTINELA"));

      vLabels.addElement(new CColumna("M�dico",
                                      "310",
                                      "MEDICO_CENTINELA"));

      vLabels.addElement(new CColumna("Dado de baja",
                                      "90",
                                      "IT_BAJA"));

      clmPuntos = new CListaMantSelMultiple(a,
                                            vLabels,
                                            this,
                                            250,
                                            640);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";
    final String imgCancelar = "images/cancelar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    this.setTitle("Selecci�n manual de puntos centinelas");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.addActionListener(actionAdapter);

    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    btnCancelar.addActionListener(actionAdapter);

    this.setSize(670, 335);
    xyLayout.setHeight(335);
    xyLayout.setWidth(670);
    this.setLayout(xyLayout);

    this.add(clmPuntos, new XYConstraints(MARGENIZQ, MARGENSUP, 630, 240));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 630 - 88, MARGENSUP + 240, 88, 29));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 630 - 2 * 88 - INTERVERT,
                               MARGENSUP + 240, 88, 29));

    rellenarTabla();

  } //end jbinit

  public boolean bAcepta() {
    return bAceptar;
  }

  public Lista listaDev() {
    return lDev;
  }

  void rellenarTabla() {
    Inicializar(CInicializar.ESPERA);
    clmPuntos.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = "servlet/SrvQueryTool";

    Inicializar(CInicializar.ESPERA);

    QueryTool2 qt = new QueryTool2();

    qt.putName("SIVE_MCENTINELA");
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("DS_NOMBRE", QueryTool.STRING);
    qt.putType("DS_APE1", QueryTool.STRING);
    qt.putType("DS_APE2", QueryTool.STRING);
    qt.putType("CD_E_NOTIF", QueryTool.STRING);
    qt.putType("CD_MASIST", QueryTool.STRING);
    qt.putType("DS_DNI", QueryTool.STRING);
    qt.putType("DS_TELEF", QueryTool.STRING);
    qt.putType("DS_FAX", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.STRING);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("CD_PCENTI", QueryTool.STRING);

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_PCENTINELA");
    qtAdic.putType("DS_PCENTI", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_PCENTI", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);

    QueryTool qtAdic1 = new QueryTool();
    qtAdic1.putName("SIVE_E_NOTIF");
    qtAdic1.putType("CD_CENTRO", QueryTool.STRING);
    Data dtAdic1 = new Data();
    dtAdic1.put("CD_E_NOTIF", QueryTool.STRING);
    qt.addQueryTool(qtAdic1);
    qt.addColumnsQueryTool(dtAdic1);

    QueryTool qtAdic2 = new QueryTool();
    qtAdic2.putName("SIVE_C_NOTIF");
    qtAdic2.putType("DS_DIREC", QueryTool.STRING);
    qtAdic2.putType("DS_NUM", QueryTool.STRING);
    qtAdic2.putType("DS_PISO", QueryTool.STRING);
    qtAdic2.putType("CD_POSTAL", QueryTool.STRING);
    qtAdic2.putType("CD_MUN", QueryTool.STRING);
    qtAdic2.putType("CD_PROV", QueryTool.STRING);
    Data dtAdic2 = new Data();
    dtAdic2.put("CD_CENTRO", QueryTool.STRING);
    qt.addQueryTool(qtAdic2);
    qt.addColumnsQueryTool(dtAdic2);

    QueryTool qtAdic3 = new QueryTool();
    qtAdic3.putName("SIVE_MUNICIPIO");
    qtAdic3.putType("DS_MUN", QueryTool.STRING);
    Data dtAdic3 = new Data();
    dtAdic3.put("CD_PROV", QueryTool.STRING);
    dtAdic3.put("CD_MUN", QueryTool.STRING);
    qt.addQueryTool(qtAdic3);
    qt.addColumnsQueryTool(dtAdic3);

    QueryTool qtAdic4 = new QueryTool();
    qtAdic4.putName("SIVE_PROVINCIA");
    qtAdic4.putType("DS_PROV", QueryTool.STRING);
    Data dtAdic4 = new Data();
    dtAdic4.put("CD_PROV", QueryTool.STRING);
    qt.addQueryTool(qtAdic4);
    qt.addColumnsQueryTool(dtAdic4);

    qt.addOrderField("CD_PCENTI");

    /*      QueryTool2 qt= new QueryTool2();
          qt.putName("SIVE_MCENTINELA");
          qt.putType("CD_MEDCEN",QueryTool.STRING);
          qt.putType("DS_NOMBRE",QueryTool.STRING);
          qt.putType("DS_APE1",QueryTool.STRING);
          qt.putType("DS_APE2",QueryTool.STRING);
          qt.putType("IT_BAJA",QueryTool.STRING);
          qt.putType("CD_PCENTI",QueryTool.STRING);
          qt.putType("CD_E_NOTIF",QueryTool.STRING);
          QueryTool qtAdic = new QueryTool();
          qtAdic.putName("SIVE_PCENTINELA");
          qtAdic.putType("DS_PCENTI", QueryTool.STRING);
          Data dtAdic = new Data();
          dtAdic.put("CD_PCENTI", QueryTool.STRING);
          qt.addQueryTool(qtAdic);
          qt.addColumnsQueryTool(dtAdic);
          QueryTool qtAdic1 = new QueryTool();
          qtAdic1.putName("SIVE_E_NOTIF");
          qtAdic1.putType("CD_CENTRO", QueryTool.STRING);
          Data dtAdic1 = new Data();
          dtAdic1.put("CD_E_NOTIF", QueryTool.STRING);
          qt.addQueryTool(qtAdic1);
          qt.addColumnsQueryTool(dtAdic1);
          QueryTool qtAdic2 = new QueryTool();
          qtAdic2.putName("SIVE_C_NOTIF");
          qtAdic2.putType("DS_DIREC", QueryTool.STRING);
          qtAdic2.putType("DS_NUM", QueryTool.STRING);
          qtAdic2.putType("DS_PISO", QueryTool.STRING);
          qtAdic2.putType("CD_POSTAL", QueryTool.STRING);
          qtAdic2.putType("CD_MUN", QueryTool.STRING);
          qtAdic2.putType("CD_PROV", QueryTool.STRING);
          Data dtAdic2 = new Data();
          dtAdic2.put("CD_CENTRO", QueryTool.STRING);
          qt.addQueryTool(qtAdic2);
          qt.addColumnsQueryTool(dtAdic2);
          QueryTool qtAdic3 = new QueryTool();
          qtAdic3.putName("SIVE_MUNICIPIO");
          qtAdic3.putType("DS_MUN", QueryTool.STRING);
          Data dtAdic3 = new Data();
          dtAdic3.put("CD_PROV", QueryTool.STRING);
          dtAdic3.put("CD_MUN", QueryTool.STRING);
          qt.addQueryTool(qtAdic3);
          qt.addColumnsQueryTool(dtAdic3);
          QueryTool qtAdic4 = new QueryTool();
          qtAdic4.putName("SIVE_PROVINCIA");
          qtAdic4.putType("DS_PROV", QueryTool.STRING);
          Data dtAdic4 = new Data();
          dtAdic4.put("CD_PROV", QueryTool.STRING);
          qt.addQueryTool(qtAdic4);
          qt.addColumnsQueryTool(dtAdic4);
          qt.addOrderField("CD_PCENTI");*/

    p.addElement(qt);

    try {
      /*          SrvQueryTool servlet1=new SrvQueryTool();
                servlet1.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
                p1 = (Lista) servlet1.doDebug(1,p);*/
      this.getApp().getStub().setUrl(servlet);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);

      if (p1.size() == 0) {
        this.getApp().showAdvise("No existen puntos centinelas");
        btnAceptar.setEnabled(false);
      }
      else {
        p1 = anadirCampos(p1);
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public Lista siguientePagina() {
    Lista p = new Lista();
    return p;
  }

  Lista anadirCampos(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      String s = "Dr. " + dCampo.getString("DS_NOMBRE") + " " +
          dCampo.getString("DS_APE1");
      String p = dCampo.getString("CD_PCENTI") + " - " +
          dCampo.getString("DS_PCENTI");
      String direc = null;
      if (dCampo.getString("DS_NUM").equals("")) {
        direc = dCampo.getString("DS_DIREC") + " " + dCampo.getString("DS_PISO");
      }
      else {
        direc = dCampo.getString("DS_DIREC") + " N�" +
            dCampo.getString("DS_NUM") + " " + dCampo.getString("DS_PISO");
      }
      ( (Data) lCampo.elementAt(i)).put("MEDICO_CENTINELA", s);
      ( (Data) lCampo.elementAt(i)).put("PUNTO_CENTINELA", p);
      ( (Data) lCampo.elementAt(i)).put("DIRECCION", direc);
    }
    return lCampo;
  }

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("Aceptar")) {
      lDev = clmPuntos.getSelected();
      bAceptar = true;
      dispose();

    }
    if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }

  }
}

class btnAceptActionAdapter
    implements java.awt.event.ActionListener {
  DiaImpEtiquetas adaptee;
  ActionEvent e;

  btnAceptActionAdapter(DiaImpEtiquetas adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if ( (e.getActionCommand().equals("Aceptar")) ||
        (e.getActionCommand().equals("Cancelar"))) {
      adaptee.btn_actionPerformed(e);
    }
  }
}
