package centinelas.cliente.c_impetiquetas;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

//import centinelas.servidor.listados.*;

public class AppImpEtiquetas
    extends CApp
    implements CInicializar {

  PanImpEtiquetas pan = null;
  public AppImpEtiquetas() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Impresión de Etiquetas");
    pan = new PanImpEtiquetas(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }

}
