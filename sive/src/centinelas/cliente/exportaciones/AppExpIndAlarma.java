//Applet principal de indicadores de alarma de la RMC.
package centinelas.cliente.exportaciones;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppExpIndAlarma
    extends CApp
    implements CInicializar {

  PanExpIndAlarma pan = null;

  public AppExpIndAlarma() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Volcado a fichero de indicadores de alarma de la RMC");
    pan = new PanExpIndAlarma(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(false);
        break;
    }
  }
}