/**
 * Clase: ChoiceCDDS
 * Paquete: centinelas.cliente.c_componentes
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 17/01/2000
 * Analisis Funcional:
 * Descripcion: Choice que trabaja con una lista de Datas compuestos
 *   de C�digo y Descripcion. Ofrece funciones tales como la escritura
 *   de los Datas en el choice, seleccionar uno, obtener el que est� selecionado,...
 */

package centinelas.cliente.c_componentes;

import java.awt.Choice;

import sapp2.Data;
import sapp2.Lista;

public class ChoiceCDDS
    extends Choice {

  // Indicadores de la combinaci�n de CD - DS
  boolean bCD = false;
  boolean bDS = true;

  // Indicador de valor vac�o
  boolean a_null = false;

  // Clave para obtener los CD y DS de los Datas
  String sCD = "CD";
  String sDS = "DS";

  // Lista con los datos CD-DS
  Lista a_lista = null;

  public ChoiceCDDS() {

  }

  public ChoiceCDDS(boolean anull, boolean boolCD, boolean boolDS,
                    String stCD, String stDS, Lista l) {
    a_null = anull;
    bCD = boolCD;
    bDS = boolDS;
    sCD = stCD;
    sDS = stDS;
    a_lista = l;
  }

  // Se escriben los Datas pasados en una Lista en el Choice
  public void writeData() {
    // Insertamos la opci�n vacia y se selecciona esta
    if (a_null) {
      add("  ");
      select(0);
    }

    if (a_lista != null) {
      Data data;

      // Si bCD y bDS son falsos se termina
      if (!bCD && !bDS) {
        return;
      }

      // Si CD y DS son true se separan mediante un guion
      String guion = "";
      if (bCD && bDS) {
        guion = " - ";

        // Insertamos el resto de la lista
      }
      for (int j = 0; j < a_lista.size(); j++) {
        String cod = "", desc = "";

        if (bCD) {
          cod = (String) ( (Data) a_lista.elementAt(j)).getString(sCD);
          if (cod == null) {
            cod = "";
          }
        }

        if (bDS) {
          desc = (String) ( (Data) a_lista.elementAt(j)).getString(sDS);
          if (desc == null) {
            desc = "";
          }
        }

        add(cod + guion + desc);
      } // Fin for
    } // Fin if
  } // Fin writeData()

  // Se recupera el Codigo del elemento seleccionado
  public String getChoiceCD() {
    if (a_lista != null) {
      Data data;

      // Si CD y DS son falsos se termina
      if (!bCD && !bDS) {
        return "";
      }

      // Puede haber alguno CD-DS seleccionado
      if (a_null) {
        if (getSelectedIndex() > 0) {
          return ( (Data) a_lista.elementAt(getSelectedIndex() - 1)).getString(
              sCD).trim();
        }
        else {
          return "";
        }
      }
      else {
        return ( (Data) a_lista.elementAt(getSelectedIndex())).getString(sCD).
            trim();
      }
    }
    else {
      return "";
    }
  } // Fin getCD()

  // Se recupera la Descripcion del elemento seleccionado
  public String getChoiceDS() {
    if (a_lista != null) {
      Data data;

      // Si CD y DS son falsos se termina
      if (!bCD && !bDS) {
        return "";
      }

      // Puede haber CD-DS seleccionado
      if (a_null) {
        if (getSelectedIndex() > 0) {
          return ( (Data) a_lista.elementAt(getSelectedIndex() - 1)).getString(
              sDS).trim();
        }
        else {
          return "";
        }
      }
      else {
        return ( (Data) a_lista.elementAt(getSelectedIndex())).getString(sDS).
            trim();
      }
    } // Fin if (a_lista != null)
    else {
      return "";
    }
  } // Fin getDS()

  // Se selecciona un elemento de un choice segun un codigo
  public void selectChoiceElement(String cod) {
    if (cod.equals("")) {
      if (a_null) {
        select(0);
      }
    }
    else {
      for (int i = 0; i < a_lista.size(); i++) {
        Data data = (Data) a_lista.elementAt(i);
        if (data.getString(sCD).trim().equals(cod.trim())) {
          if (a_null) {
            select(i + 1);
          }
          else {
            select(i);
          }
          break;
        }
      }
    }
  } // Fin selectChoiceElement()

  // Devuelve la lista con la que trabaja el Choice
  public Lista getLista() {
    return a_lista;
  } // Fin getLista()

  // Establece la lista con la trabaja el Choice
  public void setLista(Lista l) {
    a_lista = l;
  } // Fin setLista()

}