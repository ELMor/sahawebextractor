package centinelas.cliente.c_componentes;

/**
 * @autor LSR
 * @version 1.0
 *
 * Lista de valores auxiliar para localizar registros en una tabla.
 *
 *
 */
import java.util.Vector;

import java.awt.Cursor;

import capp2.CApp;
import capp2.CListaValores;
import centinelas.cliente.c_comuncliente.BDatos;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

public class CListaValoresExt
    extends CListaValores {

  ContCPnlCodigoExt contenedor = null;
  boolean otraDesc = false;

  // constructor
  public CListaValoresExt(CApp a,
                          String title,
                          QueryTool qt,
                          Vector v) {

    super(a,
          title,
          qt,
          v);
  }

  // constructor
  public CListaValoresExt(CApp a,
                          String title,
                          QueryTool qt,
                          Vector v,
                          ContCPnlCodigoExt cont,
                          boolean otraDescrip) {

    super(a,
          title,
          qt,
          v);
    contenedor = cont;
    otraDesc = otraDescrip;
  }

  /*
    // constructor
    public CListaValores(CApp a,
                         String title,
                         QueryTool qt,
                         Vector v) {
      super(a.getParentFrame(), "", true);
      // par�metros
      setTitle(title);
      app = a;
      // configuraci�n
      qtConf = qt;
      vTabla = v;
      // trae la primera trama
      this.btnSearch_actionPerformed("LIKE");
      try {
        jbInit();
      } catch (Exception e) {e.printStackTrace();}
    }
   */

  // bot�n busqueda
  public void btnSearch_actionPerformed(String sOp) {
    String sFiltro = null;
    String sCampoFiltro = null;
    String sCampoCodigo = null;
    Lista vParam = null;
    Data dFila = null;

    // procesa
    try {

      modoOperacion = modoESPERA;
      Inicializar();

      // vacia la tabla
      tbl.clear();

      // actualiza la configuraci�n
      if (optSearch1.getState()) {
        sFiltro = txtCod.getText().trim();
        sCampoFiltro = (String) vTabla.elementAt(0);
        sCampoCodigo = sCampoFiltro;
        qtConf.putWhereType(sCampoFiltro, QueryTool.STRING);
        qtConf.putOperator(sCampoFiltro, sOp);
        if (sOp.equals("LIKE")) {
          sFiltro = sFiltro + "%";
        }
        qtConf.putWhereValue(sCampoFiltro, sFiltro);
        sFiltro = "%";
        sCampoFiltro = (String) vTabla.elementAt(1);
        qtConf.putWhereType(sCampoFiltro, QueryTool.STRING);
        qtConf.putOperator(sCampoFiltro, "LIKE");
        qtConf.putWhereValue(sCampoFiltro, sFiltro);
      }
      else {
        sFiltro = txtDes.getText().trim();
        sCampoFiltro = (String) vTabla.elementAt(1);
        qtConf.putWhereType(sCampoFiltro, QueryTool.STRING);
        qtConf.putOperator(sCampoFiltro, sOp);
        if (sOp.equals("LIKE")) {
          sFiltro = sFiltro + "%";
        }
        qtConf.putWhereValue(sCampoFiltro, sFiltro);
        sFiltro = "%";
        sCampoFiltro = (String) vTabla.elementAt(0);
        sCampoCodigo = sCampoFiltro;
        qtConf.putWhereType(sCampoFiltro, QueryTool.STRING);
        qtConf.putOperator(sCampoFiltro, "LIKE");
        qtConf.putWhereValue(sCampoFiltro, sFiltro);
      }

      // obtiene y pinta la lista nueva
      vParam = new Lista();
      vParam.addElement(qtConf);
      vParam.setTrama(sCampoCodigo, "");

      /*
            app.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
            vSnapShot = (Lista) app.getStub().doPost(2, vParam);
       */
      vSnapShot = (Lista) (BDatos.ejecutaSQL(true, app,
                                             StubSrvBD.SRV_QUERY_TOOL, 2,
                                             vParam));

      vParam = null;

      // comprueba que hay datos
      if (vSnapShot.size() == 0) {
        app.showAdvise("No se encontraron datos con los criterios informados.");
      }
      else {

        // escribe las l�neas en la tabla
        for (int j = 0; j < vSnapShot.size(); j++) {
          dFila = (Data) vSnapShot.elementAt(j);

          if (otraDesc == false) {
            tbl.addItem(dFila.get(vTabla.elementAt(0)) + "&" +
                        dFila.get(vTabla.elementAt(1)), '&');
          }
          //Le pasa Data al panel y este le indica descripcion
          else {
            tbl.addItem(dFila.get(vTabla.elementAt(0)) + "&" +
                        contenedor.getDescCompleta(dFila), '&');
          }
        }

        // verifica que sea la �ltima trama
        if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
          tbl.addItem("M�s datos ...");
        }
      }

      // error al procesar
    }
    catch (Exception e) {
      app.trazaLog(e);
      app.showError(e.getMessage());
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // habilita/deshabilita las opciones seg�n sea necesario
  void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        txtCod.setEnabled(true);
        txtDes.setEnabled(true);
        btnSearch1.setEnabled(true);
        btnSearch2.setEnabled(true);
        optSearch1.setEnabled(true);
        optSearch2.setEnabled(true);
        tbl.setEnabled(true);
        btnOk.setEnabled(true);
        btnCancel.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        btnSearch1.setEnabled(false);
        btnSearch2.setEnabled(false);
        optSearch1.setEnabled(false);
        optSearch2.setEnabled(false);
        tbl.setEnabled(false);
        btnOk.setEnabled(false);
        btnCancel.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // seleccionar la lista
  protected void tbl_actionPerformed() {
    int iLast = 0;
    Lista vParam = null;
    Data dFila;

    // lista incompleta -> trae la siguiente trama
    if ( (vSnapShot.getEstado() == Lista.INCOMPLETA) &&
        (tbl.getSelectedIndex() == (tbl.countItems() - 1))) {

      // procesa
      try {

        modoOperacion = modoESPERA;
        Inicializar();

        iLast = tbl.countItems() - 1;

        // obtiene y pinta la lista nueva
        vParam = new Lista();

        // alamcena los par�metros para recuperar las tramas
        vParam.setTrama(vSnapShot.getTrama());
        vParam.addElement(qtConf);
        /*
                app.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
                vSnapShot.addElements((Lista) app.getStub().doPost(2, vParam));
         */

        vSnapShot.addElements( (Lista)
                              (BDatos.ejecutaSQL(true, app,
                                                 StubSrvBD.SRV_QUERY_TOOL, 2,
                                                 vParam)));

        vParam = null;

        // escribe las l�neas en la tabla
        tbl.deleteItem(iLast);
        for (int j = iLast; j < vSnapShot.size(); j++) {
          dFila = (Data) vSnapShot.elementAt(j);

          if (otraDesc == false) {
            tbl.addItem(dFila.get(vTabla.elementAt(0)) + "&" +
                        dFila.get(vTabla.elementAt(1)), '&');
          }
          else {
            tbl.addItem(dFila.get(vTabla.elementAt(0)) + "&" +
                        contenedor.getDescCompleta(dFila), '&');

          }
        }

        // verifica que sea la �ltima trama
        if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
          tbl.addItem("M�s datos ...");

          // error al procesar
        }
      }
      catch (Exception e) {
        app.trazaLog(e);
        app.showError(e.getMessage());
      }

      modoOperacion = modoNORMAL;
      Inicializar();

      // item seleccionado
    }
    else {
      this.quit();
    }
  }

} //CLASE
