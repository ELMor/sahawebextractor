//Applet para mantenimiento de poblaci�n del m�dico notificador

package centinelas.cliente.c_mantpoblacmed;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantPoblacMed
    extends CApp
    implements CInicializar {
  PanMantPoblacMed pan = null;

  public AppMantPoblacMed() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento poblaci�n m�dico notificador ");
    pan = new PanMantPoblacMed(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }
}
