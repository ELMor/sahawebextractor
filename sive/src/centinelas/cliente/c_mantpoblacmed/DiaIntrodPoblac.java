package centinelas.cliente.c_mantpoblacmed;

import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import sapp2.Data;

public class DiaIntrodPoblac
    extends CDialog {
  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  public boolean bAcept = false;
  public String devPoblac = null;

  XYLayout xyLayout1 = new XYLayout();

  Data dtDev = new Data();

  Label lblPoblac = new Label();
  CEntero txtPoblac = null;

  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter3 actionAdapter = new DialogActionAdapter3(this);

  public DiaIntrodPoblac(CApp a, Data dtAnt) {
    super(a);
    dtDev = dtAnt;
    txtPoblac = new CEntero(10);
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private boolean isDataValid() {
    if (txtPoblac.getText().trim().equals("")) {
      this.getApp().showAdvise("Debe rellenar el campo visualizado");
      return false;
    }
    else {
      return true;
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(250, 120);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    lblPoblac.setText("Poblaci�n:");

    txtPoblac.setBackground(new Color(255, 255, 150));

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(450);
    xyLayout1.setWidth(450);

    this.add(lblPoblac, new XYConstraints(MARGENIZQ, MARGENSUP, 100, ALTO));
    this.add(txtPoblac,
             new XYConstraints(MARGENIZQ + INTERHOR + 100, MARGENSUP, 90, ALTO));

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 100 + 90 - 2 * 88,
                               MARGENSUP + ALTO + INTERVERT, 88, 29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + INTERHOR + 100 + 90 - 88,
                               MARGENSUP + ALTO + INTERVERT, 88, 29));

    setTitle("Modificar poblaci�n");
    txtPoblac.setText(dtDev.getString("NM_POBLAC"));
    txtPoblac.selectAll();
  }

  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        devPoblac = txtPoblac.getText().trim();
        bAcept = true;
        dispose();
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAcept = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  public boolean bAcept() {
    return bAcept;
  }

  public String bdevPoblac() {
    return devPoblac;
  }
}

class DialogActionAdapter3
    implements java.awt.event.ActionListener, Runnable {
  DiaIntrodPoblac adaptee;
  ActionEvent e;

  DialogActionAdapter3(DiaIntrodPoblac adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
