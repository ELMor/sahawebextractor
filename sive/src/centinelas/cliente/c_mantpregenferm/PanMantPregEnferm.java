package centinelas.cliente.c_mantpregenferm;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CListaValores;
import capp2.CPanel;
import centinelas.cliente.c_componentes.CColumnaImagen;
import centinelas.cliente.c_componentes.CListaMantConImagenes;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class PanMantPregEnferm
    extends CPanel
    implements CInicializar, CFiltro {
  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  /*
    final public int ALTA = 0;
    final public int MODIFICACION = 1;
    final public int BAJA = 2;
   */

  //Solo un modo
  final public int MODIFICACION = 0;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  // Nombre del servlet
  final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
//  final String srvTrans = "servlet/SrvTransaccion";
  final String srvTrans = nombreservlets.strSERVLET_TRANSACCION;

  final String imgCancelar = "images/cancelar.gif";
  final String imgAceptar = "images/aceptar.gif";

  //C�digo y descripcion de la enfermedad asociada a las preguntas fijas (gripe)
  String codEnfCie = "";
  String desEnfCie = "";
  //Lista de las preguntas fijas
  Lista listaPreguntas = new Lista();
  boolean hayPregFijas = false; //Dice si hay preguntas fijas
  boolean hayCambios = false; //Dice si ha habido cambios respecto a datos iniciales

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();
  ButtonControl btnCambEnferm = new ButtonControl();
  CListaMantenimiento clmMantPregEnf = null;
  Label lblEnfCent = new Label();
  Label txtEnf = new Label();
  ButtonControl btnGrabar = new ButtonControl();

  //______________________________________________________

  public PanMantPregEnferm(CApp a) {

    setApp(a);

    //configuro CListMantenimiento
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {

      //panel de punto centinela
      // configura la lista de inspecciones
      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar pregunta enfermedad",
                                     true,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumnaImagen("Descripci�n Corta",
                                            "150",
                                            "DS_CORTA", false));

      vLabels.addElement(new CColumnaImagen("Descripci�n larga",
                                            "290",
                                            "DS_LARGA", false));

      vLabels.addElement(new CColumnaImagen("C�digo pregunta",
                                            "110",
                                            "CD_PREGUNTA", false));

      //Esta es una imagen: �ltimo parametro a true
      vLabels.addElement(new CColumnaImagen("Control",
                                            "70",
                                            "CONTROL", true));

      clmMantPregEnf = new CListaMantConImagenes(a,
                                                 vLabels,
                                                 vBotones,
                                                 this,
                                                 this,
                                                 236,
                                                 640,
                                                 22);

      jbInit();
      this.getApp().showAdvise("La modificaci�n de los datos en este m�dulo puede afectar gravemente la explotaci�n de datos");
    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().showError("Error al traer datos");
    }
  }

  //______________________________________________________

  String obtenerEnfermedad() {
    String s = "";
    return s;
  }

  //______________________________________________________

  void jbInit() throws Exception {

    xyLayout.setHeight(381);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    String sEnf = obtenerEnfermedad();
    txtEnf.setText(sEnf);

    lblEnfCent.setText("Enfermedad: ");
    btnCambEnferm.setLabel("Cambiar enfermedad");
    btnGrabar.setLabel("Grabar");

    btnCambEnferm.addActionListener(new PanMantPregEnferm_btn_actionAdapter(this));
    btnGrabar.addActionListener(new PanMantPregEnferm_btn_actionAdapter(this));

    btnGrabar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    //Action command para distinguir eventos
    btnGrabar.setActionCommand("btnGrabar");
    btnCambEnferm.setActionCommand("btnCambEnferm");

    this.add(lblEnfCent, new XYConstraints(MARGENIZQ, MARGENSUP, 250, ALTO));
//    this.add(txtEnf, new XYConstraints(MARGENIZQ+INTERHOR+75,MARGENSUP, 400, ALTO));
    this.add(btnCambEnferm,
             new XYConstraints(MARGENIZQ - 5 + 650 - 175, MARGENSUP, 150, 29));
    this.add(clmMantPregEnf,
             new XYConstraints(MARGENIZQ - 10,
                               MARGENSUP + ALTO + 29 + 2 * INTERVERT - 20, 650,
                               250));
    this.add(btnGrabar,
             new XYConstraints(MARGENIZQ - 5 + 650 - 175 + 70,
                               MARGENSUP + ALTO + 29 + 2 * INTERVERT + 250 - 20,
                               -1, -1));

    QueryTool2 qtEnfCenti = new QueryTool2();
    //Obtiene el proceso asociado a la enfermedad (gripe) con preguntas fijas
    //Hace:  select CD_ENFCIE,DS_PROCESO from SIVE_PROCESOS where  CD_ENFCIE in
    //(select CD_ENFCIE from SIVE_PREG_FIJA)
    qtEnfCenti.putName("SIVE_PROCESOS");
    qtEnfCenti.putType("CD_ENFCIE", QueryTool.STRING);
    qtEnfCenti.putType("DS_PROCESO", QueryTool.STRING);
    qtEnfCenti.putSubquery("CD_ENFCIE", "select CD_ENFCIE from SIVE_PREG_FIJA");

    //Se recogen todos los registros de tabla SIVE_PREG_FIJA
    QueryTool2 qtPregFija = new QueryTool2();
    qtPregFija.putName("SIVE_PREG_FIJA");
    qtPregFija.putType("DS_CORTA", QueryTool.STRING);
    qtPregFija.putType("CD_ENFCIE", QueryTool.STRING);
    qtPregFija.putType("CD_TSIVE", QueryTool.STRING);
    qtPregFija.putType("CD_PREGUNTA", QueryTool.STRING);
    qtPregFija.putType("DS_LARGA", QueryTool.STRING);
    // Datos adicionales: descripci�n de pregunta
    //select DS_PREGUNTA from SIVE_PREGUNTA where CD_PREGUNTA= ? (pregunta de lista ya recogida)
    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_PREGUNTA");
    qtAdic.putType("DS_PREGUNTA", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_PREGUNTA", QueryTool.STRING);
    qtPregFija.addQueryTool(qtAdic);
    qtPregFija.addColumnsQueryTool(dtAdic);
    // Lista de QueryTools con sus modos de operacion
    Lista lQuery = new Lista();

    Data dtEnfCenti = new Data();
    dtEnfCenti.put("1", qtEnfCenti); //select
    lQuery.addElement(dtEnfCenti);

    Data dtPregFija = new Data();
    dtPregFija.put("1", qtPregFija); //select
    lQuery.addElement(dtPregFija);

    Lista listaConListas = BDatos.ejecutaSQL(false, app, srvTrans, 1, lQuery);

    //Lista donde llega la descripcion de enfermedad. En el primer elemento llega la desc
    Lista listaEnfCenti = (Lista) listaConListas.firstElement();
    //Lista donde llegan las preguntas fijas
    listaPreguntas = (Lista) listaConListas.elementAt(1);

    //Si aun no se han a�adido preguntas fijas
    if ( (listaEnfCenti.size() == 0) || (listaPreguntas.size() == 0)) {
      app.showError("No existen preguntas fijas en el sistema");

    }
    //Si hay preguntas fijas
    else {
      hayPregFijas = true;
      //Se guarda el cod y desc de enfermedad asociada a preg fijas
      Data elem = (Data) (listaEnfCenti.firstElement());
      codEnfCie = (String) (elem.getString("CD_ENFCIE"));
      desEnfCie = (String) (elem.getString("DS_PROCESO"));
      lblEnfCent.setText(lblEnfCent.getText() + desEnfCie);

      for (int j = 0; j < listaPreguntas.size(); j++) {
        Data datPregunta = (Data) listaPreguntas.elementAt(j);
        if (datPregunta.getString("DS_LARGA").equals(datPregunta.getString(
            "DS_PREGUNTA"))) {
          datPregunta.put("CONTROL",
                          this.getApp().getLibImagenes().get(imgAceptar));
        }
        else {
          datPregunta.put("CONTROL",
                          this.getApp().getLibImagenes().get(imgCancelar));
        }
      }
      clmMantPregEnf.setPrimeraPagina(listaPreguntas);
    }

    //Inicializacion pantalla
    Inicializar(CInicializar.NORMAL);

  }

  //______________________________________________________

  public void Inicializar() {
  }

  //______________________________________________________

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        if (hayPregFijas) {
          this.setEnabled(true);
        }
        else {
          this.setEnabled(false);
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    }
  }

  //______________________________________________________

  //Por compatibilidad : Nos traeremos toda la lista de una vez
  public Lista siguientePagina() {
    return null;
  }

  //______________________________________________________

  public void realizaOperacion(int j) {

    DiaMantPregEnferm di = null;
    Data dMantPregEnferm = new Data();

    switch (j) {

      // bot�n de modificaci�n
      case MODIFICACION:
        dMantPregEnferm = clmMantPregEnf.getSelected();
        //si existe alguna fila seleccionada
        if (dMantPregEnferm != null) {
          int ind = clmMantPregEnf.getSelectedIndex();
          di = new DiaMantPregEnferm(this.getApp(), MODIFICACION,
                                     dMantPregEnferm);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar) {
            clmMantPregEnf.setPrimeraPagina(listaPreguntas);
            hayCambios = true;
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

    }

  }

  //______________________________________________________

  //Cambio de enfermedad asociada a las preguntas fijas
  void btnCambEnferm_actionPerformed(ActionEvent e) {

    CListaValores clv = null;
    Vector vCod = null;
    QueryTool qt = null;

    // campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement("CD_ENFCIE");
    vCod.addElement("DS_PROCESO");

    qt = new QueryTool();
    qt.putName("SIVE_PROCESOS");
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("DS_PROCESO", QueryTool.STRING);
    qt.putSubquery("CD_ENFCIE", "select CD_ENFCIE from SIVE_ENF_CENTI");

    // lista de valores
    clv = new CListaValores(this.getApp(),
                            "Enfermedades Centinelas",
                            qt,
                            vCod);
    clv.show();

    // recupera la enfermedad seleccionada
    if (clv.getSelected() != null) {
      for (int k = 0; k < listaPreguntas.size(); k++) {
        Data elem = (Data) listaPreguntas.elementAt(k);
        elem.put("CD_ENFCIE", clv.getSelected().get("CD_ENFCIE"));
      }

      codEnfCie = clv.getSelected().getString("CD_ENFCIE");
      desEnfCie = clv.getSelected().getString("DS_PROCESO");
      lblEnfCent.setText("Enfermedad: " + desEnfCie);
    }
    clv = null;

  }

  void btnGrabar_actionPerformed(ActionEvent e) {

    try {

      Inicializar(CInicializar.ESPERA);

      // Lista de QueryTools con sus modos de operacion
      Lista lQuery = new Lista();

      for (int j = 0; j < listaPreguntas.size(); j++) {

        Data datPregFija = (Data) (listaPreguntas.elementAt(j));

        //QuetryTool que actualiza registro de SIVE_PREG_FIJA
        //Hace update SIVE_PREG_FIJA set CD_ENFCIE = ? CD_PREGUNTA = ? DS_LARGA = ?
        // where DS_CORTA= ? and CD_TSIVE= ?
        QueryTool2 qtPreguntas = new QueryTool2();

        qtPreguntas.putName("SIVE_PREG_FIJA");
        qtPreguntas.putType("CD_ENFCIE", QueryTool.STRING);
        qtPreguntas.putType("CD_PREGUNTA", QueryTool.STRING);
        qtPreguntas.putType("DS_LARGA", QueryTool.STRING);

        qtPreguntas.putValue("CD_ENFCIE", datPregFija.getString("CD_ENFCIE"));
        qtPreguntas.putValue("CD_PREGUNTA", datPregFija.getString("CD_PREGUNTA"));
        qtPreguntas.putValue("DS_LARGA", datPregFija.getString("DS_LARGA"));

        // Parte del where
        qtPreguntas.putWhereType("DS_CORTA", QueryTool.STRING);
        qtPreguntas.putWhereValue("DS_CORTA", datPregFija.getString("DS_CORTA"));
        qtPreguntas.putOperator("DS_CORTA", "=");

        // Parte del where
        qtPreguntas.putWhereType("CD_TSIVE", QueryTool.STRING);
        qtPreguntas.putWhereValue("CD_TSIVE", "C");
        qtPreguntas.putOperator("CD_TSIVE", "=");

        Data dtPregFija = new Data();
        dtPregFija.put("4", qtPreguntas); //update
        lQuery.addElement(dtPregFija);
      }

      Lista listaUpdate = BDatos.ejecutaSQL(false, app, srvTrans, 1, lQuery);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().showError("Error al traer datos");
    }
    Inicializar(CInicializar.NORMAL);
  }

} //CLASE

//______________________________________________________

class PanMantPregEnferm_btn_actionAdapter
    implements java.awt.event.ActionListener {
  PanMantPregEnferm adaptee;

  PanMantPregEnferm_btn_actionAdapter(PanMantPregEnferm adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if ( ( (ButtonControl) (e.getSource())).getActionCommand().equals(
        "btnGrabar")) {
      adaptee.btnGrabar_actionPerformed(e);
    }
    else if ( ( (ButtonControl) e.getSource()).getActionCommand().equals(
        "btnCambEnferm")) {
      adaptee.btnCambEnferm_actionPerformed(e);
    }
  }
}
