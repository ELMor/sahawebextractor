//Title:        Mantenimiento de grupos de edades para denominadores
//Version:
//Copyright:    Copyright (c) 1998
//Author:       MTR
//Company:
//Description:  Panel de mantenimiento de grupos de edad

package centinelas.cliente.c_mantgrupedad;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPanel;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class PanMantGrupEdad
    extends CPanel
    implements CInicializar, CFiltro, ContCPnlCodigoExt {
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();
  CPnlCodigoExt pnlGrupEdad = null;
  ButtonControl btnBuscar = new ButtonControl();
  CListaMantenimiento clmMantGrupEdad = null;
  Label lblGrupEdad = new Label();

  public PanMantGrupEdad(CApp a) {
    //configuro CListaMantenimiento y CPnlCodigoExt
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qtGrupEdad = new QueryTool();

    setApp(a);
    qtGrupEdad.putName("SIVE_GRUPO_EDAD_RMC");
    qtGrupEdad.putType("CD_GEDAD", QueryTool.STRING);
    qtGrupEdad.putType("DS_GEDAD", QueryTool.STRING);

    try {

      //panel de punto centinela
      pnlGrupEdad = new CPnlCodigoExt(a,
                                      this,
                                      qtGrupEdad,
                                      "CD_GEDAD",
                                      "DS_GEDAD",
                                      false,
                                      "Grupos de edades para denominadores",
                                      "Grupos de edades para denominadores",
                                      this);

      // configura la lista de inspecciones
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nuevo grupo de edad para denominadores",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
          "Modificar grupo de edad para denominadores",
          true,
          true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "grupo de edad para denominadores",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("C�digo grupo de edad",
                                      "230",
                                      "CD_GEDAD"));

      vLabels.addElement(new CColumna(
          "Descripci�n grupo de edad para denominadores",
          "370",
          "DS_GEDAD"));

      clmMantGrupEdad = new CListaMantenimiento(a,
                                                vLabels,
                                                vBotones,
                                                this,
                                                this,
                                                250,
                                                640);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";

    // carga la imagen
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();

    xyLayout.setHeight(385);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    lblGrupEdad.setText("Grupos de Edad:");
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(new PanMantGrupEdad_btnBuscar_actionAdapter(this));

    // a�ade los componentes
    this.add(lblGrupEdad,
             new XYConstraints(3 * CPnlCodigoExt.LONG_SEP, MARGENSUP, -1, ALTO));
    this.add(pnlGrupEdad,
             new XYConstraints(CPnlCodigoExt.LONG_SEP * 2 +
                               CPnlCodigoExt.LONG_ETI + 5, MARGENSUP, TAMPANEL,
                               34));
    this.add(btnBuscar,
             new XYConstraints(535, MARGENSUP + 34 + INTERVERT, 80, 24));
    this.add(clmMantGrupEdad,
             new XYConstraints(CPnlCodigoExt.LONG_SEP,
                               MARGENSUP + 34 + 3 * INTERVERT + ALTO, 650, 230));
  }

  public String getDescCompleta(Data dat) {
    return ("");
  }

  public void trasEventoEnCPnlCodigoExt() {}

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pnlGrupEdad.backupDatos();
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        pnlGrupEdad.setEnabled(true);
        clmMantGrupEdad.setEnabled(true);
        lblGrupEdad.setEnabled(true);
        btnBuscar.setEnabled(true);
    }
  }

  //solicita la primera trama de datos. 2 casos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //1� CASO: no se realiza ning�n filtro: QueryTool2 sin where
    if (pnlGrupEdad.getDatos() == null) {
      try {
        QueryTool qt = new QueryTool();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_GRUPO_EDAD_RMC");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_GEDAD", QueryTool.STRING);
        qt.putType("DS_GEDAD", QueryTool.STRING);

        qt.addOrderField("CD_GEDAD");

        p.addElement(qt);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          this.getApp().showAdvise("No se poseen elementos");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        ex.printStackTrace();
      }
    } //Fin del primer caso

    //2� CASO: se realiza el filtro de grupo_edad
    if (pnlGrupEdad.getDatos() != null) {
      QueryTool qt = new QueryTool();
      try {
        // Nombre de la tabla (FROM)
        qt.putName("SIVE_GRUPO_EDAD_RMC");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_GEDAD", QueryTool.STRING);
        qt.putType("DS_GEDAD", QueryTool.STRING);

        qt.putWhereType("CD_GEDAD", QueryTool.STRING);
        qt.putWhereValue("CD_GEDAD",
                         ( (Data) pnlGrupEdad.getDatos()).get("CD_GEDAD"));
        qt.putOperator("CD_GEDAD", "=");

        qt.addOrderField("CD_GEDAD");

        p.addElement(qt);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          pnlGrupEdad.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }

    } //Fin del segundo caso
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //1� CASO: no se realiza ning�n filtro: QueryTool2 sin where
    if (pnlGrupEdad.getDatos() == null) {
      try {
        QueryTool qt = new QueryTool();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_GRUPO_EDAD_RMC");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_GEDAD", QueryTool.STRING);
        qt.putType("DS_GEDAD", QueryTool.STRING);

        qt.addOrderField("CD_GEDAD");

        p.addElement(qt);
        p.setTrama(this.clmMantGrupEdad.getLista().getTrama());
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          this.getApp().showAdvise("No se poseen elementos");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del primer caso

    //2� CASO: se realiza el filtro de Enfermedad
    if (pnlGrupEdad.getDatos() != null) {
      QueryTool qt = new QueryTool();
      try {
        // Nombre de la tabla (FROM)
        qt.putName("SIVE_GRUPO_EDAD_RMC");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_GEDAD", QueryTool.STRING);
        qt.putType("DS_GEDAD", QueryTool.STRING);

        qt.putWhereType("CD_GEDAD", QueryTool.STRING);
        qt.putWhereValue("CD_GEDAD",
                         ( (Data) pnlGrupEdad.getDatos()).get("CD_GEDAD"));
        qt.putOperator("CD_GEDAD", "=");

        qt.addOrderField("CD_GEDAD");

        p.addElement(qt);
        p.setTrama(this.clmMantGrupEdad.getLista().getTrama());
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          pnlGrupEdad.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }

    } //Fin del segundo caso
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public void realizaOperacion(int j) {
    DiaMantGrupEdad di = null;
    DiaMantRangEdad diRang = null;
    Data dMantGrupEdad = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        di = new DiaMantGrupEdad(this.getApp(), ALTA, dMantGrupEdad);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar) {
          clmMantGrupEdad.setPrimeraPagina(primeraPagina());
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantGrupEdad = clmMantGrupEdad.getSelected();
        //si existe alguna fila seleccionada
        if (dMantGrupEdad != null) {
          int ind = clmMantGrupEdad.getSelectedIndex();
          diRang = new DiaMantRangEdad(this.getApp(), MODIFICACION,
                                       dMantGrupEdad);
          diRang.show();
          //Tratamiento de bAceptar para Salir
          if (diRang.bAceptar) {
            clmMantGrupEdad.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dMantGrupEdad = clmMantGrupEdad.getSelected();
        if (dMantGrupEdad != null) {
          int ind = clmMantGrupEdad.getSelectedIndex();
          diRang = new DiaMantRangEdad(this.getApp(), BAJA, dMantGrupEdad);
          diRang.show();
          if (diRang.bAceptar) {
            clmMantGrupEdad.getLista().removeElementAt(ind);
            clmMantGrupEdad.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;
    }
  }

  void btnBuscar_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    clmMantGrupEdad.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }
}

class PanMantGrupEdad_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener {
  PanMantGrupEdad adaptee;

  PanMantGrupEdad_btnBuscar_actionAdapter(PanMantGrupEdad adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnBuscar_actionPerformed(e);
  }

}
