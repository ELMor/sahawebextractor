//Di�logo con los datos de un rango de edad
package centinelas.cliente.c_mantgrupedad;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CTexto;
import centinelas.cliente.c_comuncliente.nombreservlets;
import sapp2.Data;
import sapp2.Lista;

public class DiaMantRang1
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // parametros pasados al di�logo
  public Data dtDev = null;
  //par�metros devueltos
  public Data dtDevuelto = new Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblDesc = new Label();
  Label lblLimInf = new Label();
  Label lblLimSup = new Label();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  CTexto txtDesc = new CTexto(30);
  CEntero txtLimInf = new CEntero(3);
  CEntero txtLimSup = new CEntero(3);

  // Escuchadores de eventos...
  DialogActionAdapter2 actionAdapter = new DialogActionAdapter2(this);

  public DiaMantRang1(CApp a, int modoop, Data dt) {
    super(a);
    modoOperacion = modoop;
    dtDev = dt;

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(395, 195);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    txtDesc.setBackground(new Color(255, 255, 150));
    txtLimInf.setBackground(new Color(255, 255, 150));
    txtLimSup.setBackground(new Color(255, 255, 150));
    txtDesc.setText("");
    txtLimInf.setText("");
    txtLimSup.setText("");
    lblDesc.setText("Descripci�n:");
    lblLimInf.setText("L�mite Inferior:");
    lblLimSup.setText("L�mite superior:");

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(450);
    xyLayout1.setWidth(450);

    this.add(lblDesc, new XYConstraints(MARGENIZQ, MARGENSUP, 94, ALTO));
    this.add(txtDesc,
             new XYConstraints(MARGENIZQ + 94 + INTERVERT, MARGENSUP, 240, ALTO));

    this.add(lblLimInf,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 94,
                               ALTO));
    this.add(txtLimInf,
             new XYConstraints(MARGENIZQ + 94 + INTERVERT,
                               MARGENSUP + ALTO + INTERVERT, 50, ALTO));

    this.add(lblLimSup,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERVERT,
                               94, ALTO));
    this.add(txtLimSup,
             new XYConstraints(MARGENIZQ + 94 + INTERVERT,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 50, ALTO));

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 94 + 240 - 2 * 88,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT, 88, 29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 94 + INTERVERT + 240 - 88,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT, 88, 29));

    verDatos();

    switch (modoOperacion) {
      case modoALTA:
        this.setTitle("ALTA");
        break;
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            txtLimInf.setEnabled(false);
            break;

          case modoMODIFICACION:
            this.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  public void rellenarDatos() {
    txtDesc.setText(dtDev.getString("DS_DIST_EDAD"));
    txtLimInf.setText(dtDev.getString("DIST_EDAD_I"));
    txtLimSup.setText(dtDev.getString("DIST_EDAD_F"));
  }

  //Prepara la pantalla para el modo de operaci�n a realizar
  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        this.setEnabled(true);
        break;

      case modoMODIFICACION:
        rellenarDatos();
        break;

      case modoBAJA:
        txtDesc.setEnabled(false);
        txtLimInf.setEnabled(false);
        txtLimSup.setEnabled(false);
        rellenarDatos();
        break;
    }
  }

  //Obligatorio rellenar todos los campos correctamente
  private boolean isDataValid() {
    if ( (txtDesc.getText().equals("")) || (txtLimInf.getText().equals("")) ||
        (txtLimSup.getText().equals(""))) {
      this.getApp().showAdvise("Debe completar todos los campos");
      return false;
    }
    else {
      return true;
    }
  }

  boolean isRangoValid() {
    if (isDataValid()) {
      int limInf = Integer.parseInt(txtLimInf.getText().trim());
      int limSup = Integer.parseInt(txtLimSup.getText().trim());
      if (limSup < limInf) {
        return false;
      }
      else {
        return true;
      }
    }
    else {
      return false;
    }
  }

  private boolean hayCambios() {
    if ( (! (txtDesc.getText().trim().equals(dtDev.getString("DS_DIST_EDAD"))))
        || (! (txtLimInf.getText().trim().equals(dtDev.getString("DIST_EDAD_I"))))
        || (! (txtLimSup.getText().trim().equals(dtDev.getString("DIST_EDAD_F"))))) {
      return true;
    }
    else {
      return false;
    }
  }

  void devolverCampos(String bd, String op) {
    dtDevuelto.put("CD_GEDAD", dtDev.getString("CD_GEDAD"));
    dtDevuelto.put("DIST_EDAD_I", txtLimInf.getText().trim());
    dtDevuelto.put("DIST_EDAD_F", txtLimSup.getText().trim());
    dtDevuelto.put("DS_DIST_EDAD", txtDesc.getText().trim());
    dtDevuelto.put("IT_BD", bd);
    dtDevuelto.put("IT_OPERACION", op);
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();
    String itBd = new String();
    String itOper = new String();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        if (isRangoValid()) {
          switch (modoOperacion) {
            case ALTA:
              itBd = "N";
              itOper = "0";
              break;
            case MODIFICACION:
              itBd = dtDev.getString("IT_BD");
              if (itBd.equals("S")) {
                itOper = "1";
              }
              else {
                itOper = "0";
              }
              break;
            case BAJA:
              itBd = dtDev.getString("IT_BD");
              if (itBd.equals("S")) {
                itOper = "2";
              }
              else {
                itOper = "0";
              }
              break;
          } //end switch
          devolverCampos(itBd, itOper);
          bAceptar = true;
          dispose();

        }
        else { //si no es rango valido
          this.getApp().showAdvise("El rango introducido es incorrecto");
        }
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  public Data blDevuelto() {
    return dtDevuelto;
  }

}

class DialogActionAdapter2
    implements java.awt.event.ActionListener, Runnable {
  DiaMantRang1 adaptee;
  ActionEvent e;

  DialogActionAdapter2(DiaMantRang1 adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
