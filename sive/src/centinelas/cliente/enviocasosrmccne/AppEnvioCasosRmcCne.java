//Applet principal de env�o de casos

package centinelas.cliente.enviocasosrmccne;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppEnvioCasosRmcCne
    extends CApp
    implements CInicializar {
  PanEnvioCasosRmcCne pan = null;

  public AppEnvioCasosRmcCne() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Env�o de casos RMC al Centro Nacional de Epidemiolog�a");
    pan = new PanEnvioCasosRmcCne(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(false);
        break;
    }
  }
}
