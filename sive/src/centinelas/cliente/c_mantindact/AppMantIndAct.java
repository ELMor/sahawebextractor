//Applet de mantenimiento de �ndices de actividad de los m�dicos notificadores

package centinelas.cliente.c_mantindact;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantIndAct
    extends CApp
    implements CInicializar {
  PanMantIndAct pan = null;

  public AppMantIndAct() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo(
        "Mantenimiento de �ndices de actividad asistencial del m�dico notificador");
    pan = new PanMantIndAct(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }
}
