//Di�logo que representa un �ndices de actividad de un m�dico notificador
package centinelas.cliente.c_mantindact;

import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDecimal;
import capp2.CDialog;
import capp2.CInicializar;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaIndAct2
    extends CDialog
    implements CInicializar, ContCPnlCodigoExt {

  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  public Data dtDev = null;

  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;
  public Lista lIndAct = null;
  public boolean bOp = false;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  XYLayout xyLayout1 = new XYLayout();

  Label lblValor = new Label();
  Label lblIndAct = new Label();
  CDecimal cValor = null;

  CPnlCodigoExt pnlIndAct = null;

  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter2 actionAdapter = new DialogActionAdapter2(this);

  public DiaIndAct2(CApp a, int modoop, Data dt, Lista lInd, boolean op) {
    super(a);
    modoOperacion = modoop;
    QueryTool qt = new QueryTool();

    dtDev = dt;
    cValor = new CDecimal(5, 1);
    lIndAct = lInd;
    bOp = op;

    try {
      qt = configurarPanel();

      //paneles
      pnlIndAct = new CPnlCodigoExt(a,
                                    this,
                                    qt,
                                    "CD_INDRMC",
                                    "DS_INDRMC",
                                    true,
                                    "�ndices de Actividad Asistencial",
                                    "�ndices de Actividad Asistencial",
                                    this);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  QueryTool configurarPanel() {
    Lista lResul = null;
    QueryTool qt = new QueryTool();
    Lista lQuery = new Lista();
    String subquery = "";
    Vector vSubquery = new Vector();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    qt.putName("SIVE_INDACTMC");
    qt.putType("CD_INDRMC", QueryTool.STRING);
    qt.putType("DS_INDRMC", QueryTool.STRING);

    if (bOp == true) {
      // Creaci�n de la subquery
      for (int i = 0; i < lIndAct.size(); i++) {
        Data dtCodInd = new Data();

        subquery += "?,";
        dtCodInd.put(new Integer(QueryTool.STRING),
                     ( (Data) lIndAct.elementAt(i)).getString("CD_INDRMC"));
        vSubquery.addElement(dtCodInd);
      }
      subquery = subquery.substring(0, subquery.length() - 1); // Quita la ultima coma

      // Establecemos la subquery y sus (tipos,valores)
      qt.putSubquery("CD_INDRMC NOT", subquery);
      qt.putVectorSubquery("CD_INDRMC NOT", vSubquery);
    }
    return qt;
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            break;

          case modoMODIFICACION:
            lblValor.setEnabled(true);
            cValor.setEnabled(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  private boolean isDataValid() {
    if ( (pnlIndAct.getDatos() == null) || (cValor.getText().equals(""))) {
      this.getApp().showAdvise("Debe rellenar todos los campos");
      return false;
    }
    else {
      return true;
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(480, 165);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    lblIndAct.setText("Indices de Actividad:");
    lblValor.setText("Valor:");

    cValor.setBackground(new Color(255, 255, 150));
    pnlIndAct.addMouseListener(new DiaIndAct2_pnlIndAct_mouseAdapter(this));

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addMouseListener(new DiaIndAct2_btnAceptar_mouseAdapter(this));
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.addMouseListener(new DiaIndAct2_btnCancelar_mouseAdapter(this));
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(450);
    xyLayout1.setWidth(325);

    this.add(lblIndAct, new XYConstraints(MARGENIZQ, MARGENSUP, 125, ALTO));
    this.add(pnlIndAct,
             new XYConstraints(MARGENIZQ + 125 + INTERHOR - DESFPANEL,
                               MARGENSUP, TAMPANEL, 34));

    this.add(lblValor,
             new XYConstraints(MARGENIZQ, MARGENSUP + 34 + INTERVERT, 95, ALTO));
    this.add(cValor,
             new XYConstraints(MARGENIZQ + 125 + INTERHOR,
                               MARGENSUP + 34 + INTERVERT, 60, ALTO));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 125 + TAMPANEL - 2 * 88 - 15,
                               MARGENSUP + 34 + 2 * INTERVERT + ALTO, 88, 29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 125 + INTERHOR + TAMPANEL - 88 - 15,
                               MARGENSUP + 34 + 2 * INTERVERT + ALTO, 88, 29));

    verDatos();

    // ponemos el t�tulo
    switch (modoOperacion) {
      case modoALTA:
        setTitle("ALTA");
        break;
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  public Data recogerDatos() {
    Data rec = new Data();
    rec.put("CD_INDRMC", pnlIndAct.getDatos().getString("CD_INDRMC"));
    rec.put("NM_VINDRMC", cValor.getValorConComa());
    return rec;
  }

  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return "";
  }

  public void rellenarDatos() {
    pnlIndAct.setCodigo(dtDev);
    cValor.setText(dtDev.getString("NM_VINDRMC"));
  }

  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        this.setEnabled(true);
        break;

      case modoMODIFICACION:
        this.setEnabled(true);
        pnlIndAct.setEnabled(false);
        rellenarDatos();
        break;

      case modoBAJA:
        this.setEnabled(false);
        rellenarDatos();
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        break;
    }
  }

  void btn_actionPerformed(ActionEvent e) {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          // ----- ALTA -----------

          case 0:

            //Inicio el alta
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qtAlta = new QueryTool();
              qtAlta.putName("SIVE_INDACTMC_MEDCEN");
              qtAlta.putType("CD_INDRMC", QueryTool.STRING);
              qtAlta.putValue("CD_INDRMC",
                              pnlIndAct.getDatos().getString("CD_INDRMC"));
              qtAlta.putType("NM_VINDRMC", QueryTool.REAL);
              qtAlta.putValue("NM_VINDRMC", cValor.getValorConComa());
              qtAlta.putType("CD_PCENTI", QueryTool.STRING);
              qtAlta.putValue("CD_PCENTI", dtDev.getString("CD_PCENTI"));
              qtAlta.putType("CD_MEDCEN", QueryTool.STRING);
              qtAlta.putValue("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));

              // realiza la consulta al servlet
              this.getApp().getStub().setUrl(servlet);
              vFiltro.addElement(qtAlta);
              vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 3,
                                         vFiltro);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;

          case 1:

            // ---- MODIFICAR ----
            Data dtRecDat = new Data();
            dtRecDat = recogerDatos();
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qtModif = new QueryTool();
              qtModif.putName("SIVE_INDACTMC_MEDCEN");
              qtModif.putType("NM_VINDRMC", QueryTool.REAL);
              qtModif.putValue("NM_VINDRMC", dtRecDat.getString("NM_VINDRMC"));

              qtModif.putWhereType("CD_PCENTI", QueryTool.STRING);
              qtModif.putWhereValue("CD_PCENTI", dtDev.getString("CD_PCENTI"));
              qtModif.putOperator("CD_PCENTI", "=");

              qtModif.putWhereType("CD_MEDCEN", QueryTool.STRING);
              qtModif.putWhereValue("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));
              qtModif.putOperator("CD_MEDCEN", "=");

              qtModif.putWhereType("CD_INDRMC", QueryTool.STRING);
              qtModif.putWhereValue("CD_INDRMC", dtDev.getString("CD_INDRMC"));
              qtModif.putOperator("CD_INDRMC", "=");

              // realiza la consulta al servlet
              this.getApp().getStub().setUrl(servlet);
              vFiltro.addElement(qtModif);
              vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 4,
                                         vFiltro);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;

          case 2:

            // ---- BORRAR -------
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qtBorr = new QueryTool();

              // tabla de familias de productos
              qtBorr.putName("SIVE_INDACTMC_MEDCEN");

              // campos que se seleccionan
              qtBorr.putWhereType("CD_PCENTI", QueryTool.STRING);
              qtBorr.putWhereValue("CD_PCENTI", dtDev.getString("CD_PCENTI"));
              qtBorr.putOperator("CD_PCENTI", "=");

              qtBorr.putWhereType("CD_MEDCEN", QueryTool.STRING);
              qtBorr.putWhereValue("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));
              qtBorr.putOperator("CD_MEDCEN", "=");

              qtBorr.putWhereType("CD_INDRMC", QueryTool.STRING);
              qtBorr.putWhereValue("CD_INDRMC", dtDev.getString("CD_INDRMC"));
              qtBorr.putOperator("CD_INDRMC", "=");

              vFiltro.addElement(qtBorr);
              this.getApp().getStub().setUrl(servlet);
              vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 5,
                                         vFiltro);
              bAceptar = true;
              dispose();
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;
        } //end if switch
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  public boolean bAceptar() {
    return bAceptar;
  }

  void pnlIndAct_mouseEntered(MouseEvent e) {
    // perdemos y ganamos el foco, para que funcione la lupa
    pnlIndAct.transferFocus();
    pnlIndAct.requestFocus();
  }

  void btnCancelar_mouseEntered(MouseEvent e) {
    // 3/4 de lo mismo, para cancelar
    btnCancelar.transferFocus();
    btnCancelar.requestFocus();
  }

  void btnAceptar_mouseEntered(MouseEvent e) {
    // Idem, para aceptar
    btnAceptar.transferFocus();
    btnAceptar.requestFocus();
  }
}

class DialogActionAdapter2
    implements java.awt.event.ActionListener, Runnable {
  DiaIndAct2 adaptee;
  ActionEvent e;

  DialogActionAdapter2(DiaIndAct2 adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

class DiaIndAct2_pnlIndAct_mouseAdapter
    extends java.awt.event.MouseAdapter {
  DiaIndAct2 adaptee;

  DiaIndAct2_pnlIndAct_mouseAdapter(DiaIndAct2 adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseEntered(MouseEvent e) {
    adaptee.pnlIndAct_mouseEntered(e);
  }
}

class DiaIndAct2_btnCancelar_mouseAdapter
    extends java.awt.event.MouseAdapter {
  DiaIndAct2 adaptee;

  DiaIndAct2_btnCancelar_mouseAdapter(DiaIndAct2 adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseEntered(MouseEvent e) {
    adaptee.btnCancelar_mouseEntered(e);
  }
}

class DiaIndAct2_btnAceptar_mouseAdapter
    extends java.awt.event.MouseAdapter {
  DiaIndAct2 adaptee;

  DiaIndAct2_btnAceptar_mouseAdapter(DiaIndAct2 adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseEntered(MouseEvent e) {
    adaptee.btnAceptar_mouseEntered(e);
  }
}
