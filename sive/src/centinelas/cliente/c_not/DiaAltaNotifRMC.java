/**
 * Clase: DiaAltaNotifRMC
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 17/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Di�logo de alta de una NotificacionRMC
   Permite dar de alta un registro de notificaci�n para el punto, m�dico,
   a�o, semana y enfermedad indicados (enfermedad est� fuera de cobertura con esos par�metros)
 */

package centinelas.cliente.c_not;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CInicializar;
import centinelas.cliente.c_componentes.ChoiceCDDS;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.SincrEventos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.EnfCent;
import centinelas.datos.centbasicos.MedCent;
import centinelas.datos.centbasicos.NotifRMC;
import centinelas.datos.centbasicos.PuntoCent;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DiaAltaNotifRMC
    extends CDialog
    implements CInicializar /*, CFiltro*/ {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  String ano = null;
  String sem = null;
  PuntoCent pCent = null;
  MedCent mCent = null;
  Lista lEnf = null;

  //Variable booleana que indiaca si hay enfermedades con estos criterios
  //Si se pone a false no se mostrar� el di�logo
  boolean hayEnfermedades = true;

  // Vars. intermedias
  Lista lEnfVis = null;

  // Parametros de salida
  private boolean OK = false;
  private NotifRMC NotifRMCAlta = null;
  private String sDescEnf = null;

  /*************** Constantes ****************/

  // Modo de operaci�n
  public int modoOperacion = CInicializar.NORMAL;

  // Imagenes a cargar
  final String imgAceptar = "images/aceptar.gif";
  final String imgCancelar = "images/cancelar.gif";

  // Nombre del servlet
  final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // Choice con enfermedades
  private ChoiceCDDS choEnf = null;

  // Botones de salida
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaAltaNotifRMCActionAdapter actionAdapter = null;

  // Constructor
  // @param a: CApp
  // @param ano: String con el a�o
  // @param pCent: PuntoCent
  public DiaAltaNotifRMC(CApp a, String anyo, String semana,
                         PuntoCent punto, MedCent med, Lista listaEnf) {

    super(a);
    setTitle("Alta Notificaci�n RMC");
    applet = a;
    ano = anyo;
    sem = semana;
    pCent = punto;
    mCent = med;
    lEnf = listaEnf;

    try {
      // Obtenci�n de la lista de enfermedades a visualizar
      lEnfVis = obtenerEnfermedades();

      // Creaci�n del choice de elecci�n de la enfermedad
      //  (a_null, !bCD, bDS, ...): representa la descripci�n y permite dejarlo a vac�o
      choEnf = new ChoiceCDDS(true, true, true, "CD_ENFCIE", "DS_PROCESO",
                              lEnfVis);

      // Inicializaci�n
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del dialogo DiaAltaNotifRMC
  public void jbInit() throws Exception {
    // Carga las imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().put(constantes.imgCANCELAR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaAltaNotifRMCActionAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(280, 120));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(280);
    xYLayout1.setHeight(120);

    // Adici�n de componentes al di�logo
    choEnf.writeData();
    this.add(choEnf, new XYConstraints(15, 15, 250, -1));
    this.add(btnAceptar, new XYConstraints(90, 60, 80, -1));
    this.add(btnCancelar, new XYConstraints(185, 60, 80, -1));

    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgACEPTAR));
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);

    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgCANCELAR));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);

    // Tool Tips
    new CContextHelp("Aceptar", btnAceptar);
    new CContextHelp("Cancelar", btnCancelar);
  }

  public void Inicializar() {}

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    switch (modo) {
      // Modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    } // Fin switch
  } // Fin Inicializar()

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  // Obtenci�n de la lista de enfermedades a visualizar
  private Lista obtenerEnfermedades() {
    Lista lResul = null;
    QueryTool2 qt = null;
    Lista lQuery = new Lista();
    String subquery = "";
    Vector vSubquery = new Vector();

    // Query que obtiene todas la enfermedades centinela (con Desc)
    qt = EnfCent.getAllEnfCent();

    if (lEnf != null && lEnf.size() != 0) {
      // Creaci�n de la subquery
      for (int i = 0; i < lEnf.size(); i++) {
        Data dtCodEnf = new Data();

        subquery += "?,";
        dtCodEnf.put(new Integer(QueryTool.STRING),
                     ( (Data) lEnf.elementAt(i)).getString("CD_ENFCIE"));
        vSubquery.addElement(dtCodEnf);
      }
      subquery = subquery.substring(0, subquery.length() - 1); // Quita la ultima coma

      // Establecemos la subquery y sus (tipos,valores)
      qt.putSubquery("CD_ENFCIE NOT", subquery);
      qt.putVectorSubquery("CD_ENFCIE NOT", vSubquery);
    }

    // Ejecuci�n de la query
    lQuery.addElement(qt);
    Inicializar(CInicializar.ESPERA);
    try {
      this.getApp().getStub().setUrl(servlet);
      lResul = BDatos.ejecutaSQL(true, applet, servlet, 1, lQuery); // 1: DO_SELECT

      /*// SOLO DESARROLLO
            SrvQueryTool srv = new SrvQueryTool();
          lResul = BDatos.execSQLDebug(applet,srv,1,lQuery); // 1: DO_SELECT*/

     // Lista vacia
      if (lResul.size() == 0) {
        this.getApp().showAdvise("No hay enfermedades con esos criterios.");
        hayEnfermedades = false;

      }

      // Error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // La lista debe estar creada
    if (lResul == null) {
      lResul = new Lista();

      // Vuelta al estado normal
    }
    Inicializar(CInicializar.NORMAL);

    return lResul;
  } // Fin obtenerEnfermedades()

  /******************** Funciones de mantenimiento ****************/

  // Recoge los datos existentes en los componentes del di�logo
  private NotifRMC recogerDatos() {
    NotifRMC nResul = new NotifRMC();

    nResul.put("CD_ENFCIE", choEnf.getChoiceCD());
    nResul.put("CD_PCENTI", pCent.getCD_PCENTI());
    nResul.put("CD_MEDCEN", mCent.getCD_MEDCEN());
    nResul.put("CD_ANOEPI", ano);
    nResul.put("CD_SEMEPI", sem);
    nResul.put("NM_CASOSDEC", "0");
    nResul.put("FC_RECEP", QueryTool.FECHA_ACTUAL);
    nResul.put("IT_COBERTURA", "N");
    nResul.put("CD_OPE", applet.getParameter("COD_USUARIO"));
    nResul.put("FC_ULTACT", "");

    return nResul;
  } // Fin recogerDatos()

  // Rellena los componentes del di�logo con los datos existentes en la estructura
  // private void rellenarDatos(NotifRMC n){}

  // Dev. true si los datos de los componentes del di�logo son v�lidos
  boolean validarDatos() {
    boolean valido = false;

    String sel = choEnf.getChoiceCD();
    if (sel != null && !sel.equals("")) {
      valido = true; ;

    }
    return valido;
  } // Fin validarDatos()

  // Dev. true si son iguales ambas estructuras
  // boolean compararCampos(NotifRMC n1, NotifRMC n2){}

  /******************** Acceso a vars. salida *********************/

  //  Devuelve true si se sale mediante el bot�n de Aceptar
  public boolean getAceptar() {
    return OK;
  } // Fin getAceptar()

  // Devuelve el NotifRMC dado de alta , o null si no se ha dado de alta ninguno
  public NotifRMC getNotifRMC() {
    return NotifRMCAlta;
  } // Fin getNotifRMC()

  // Devuelve la descripci�n de la dada de alta
  public String getDescEnf() {
    return sDescEnf;
  } // Fin getDescEnf()

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {
    QueryTool2 qt = null;
    Lista lQuery = new Lista();

    // Si son v�lidos los datos del di�logo se recogen en un NotifRMC
    if (validarDatos()) {
      NotifRMCAlta = recogerDatos();
      sDescEnf = choEnf.getChoiceDS();
      Lista lResul;

      // QueryTool
      qt = NotifRMC.getInsertOneNotifRMC(NotifRMCAlta);
      lQuery.addElement(qt);

      // Transacci�n con el servidor
      try {
        lResul =
            BDatos.ejecutaSQL(false, applet, servlet, 3, lQuery); // 3: DO_INSERT*/
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        this.getApp().showError(ex.getMessage());
        OK = false;
      }

      // Se ha dado de alta una NotifRMC
      OK = true;

      // Desaparece el dialogo
      dispose();
    }

  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    // Desaparece el dialogo
    dispose();
  } // Fin btnCancelarActionPerformed()

} // endclass DiaAltaNotifRMC

/******************* ESCUCHADORES **********************/

// Botones
class DiaAltaNotifRMCActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaAltaNotifRMC adaptee;
  ActionEvent evt;

  DiaAltaNotifRMCActionAdapter(DiaAltaNotifRMC adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaAltaNotifRMCActionAdapter
