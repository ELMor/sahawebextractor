/* Clase: AppNot
 * Autor: Luis Rivera
 * Fecha Inicio: 13/01/2000
 * Descripcion: Applet principal de notificaciones
 * Modificaciones:
 *
 **/

package centinelas.cliente.c_not;

import capp2.CApp;

public class AppNot
    extends CApp { //implements CInicializar {
//creo una instancia del tipo de panel padre que se va a incluir en este applet

  PanPpal pan = null;

  public void init() {
    super.init();
    setTitulo("Mantenimiento Notificaciones de casos Red de M�dicos Centinelas");
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    PanPpal pan = new PanPpal(this);
    VerPanel("", pan);
  }

}
