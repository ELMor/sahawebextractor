/**
 * Clase: DiaCasoCent
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 20/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Di�logo de alta/mod/baja/consulta de un CasoCent
 */

package centinelas.cliente.c_not;

import java.util.Date;
import java.util.Enumeration;

import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CTexto;
import centinelas.cliente.c_componentes.ChoiceCDDS;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.SincrEventos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.cliente.c_protocolo.Panelprueba;
import centinelas.datos.c_protocolo.DataProtocolo;
import centinelas.datos.centbasicos.CasoCent;
import centinelas.datos.centbasicos.NotifRMC;
import centinelas.datos.comun.Fechas;
import comun.constantes;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

//Servlet de disparo de alarmas.
//import disparaAlarma.*;

public class DiaCasoCent
    extends CDialog
    implements CInicializar /*, CFiltro*/ {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  String eCent = null; // Desc. Enfermedad Centinela
  NotifRMC nRMC = null; // NotifRMC
  CasoCent cCent = null; // CasoCent
  Lista lSexo = null; // Lista Sexos

  // Vars. intermedias
  int ordenCaso;
  int totalCasos;

  boolean hayProtocolo = false; //Dice si hay datos de protocolo para esta enfermedad

  // Parametros de salida
  private boolean OK = false;
  private CasoCent cCentSal = null;

  //Param para obtener respuestas del caso
  String sNumCaso = "";

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Imagenes a cargar
  final String imgAceptar = "images/aceptar.gif";
  final String imgCancelar = "images/cancelar.gif";

  // Nombre del servlet
  final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
//  final String srvTrans = "servlet/SrvTransaccion";

  final String srvTrans = nombreservlets.strSERVLET_TRANSACCION;

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // Enfermedad
  Label lblEnf = null;
  Label lblDsEnf = null;

  // Numero de parte
  Label nParte = null;

  // N�mero de paciente
  Label lblNPac = null;
  CTexto txtNPac = null;

  // Edad
  Label lblEdad = null;
  CEntero txtEdad = null;
  Choice choEdades = null;

  // Fecha Nacimiento
  Label lblFNac = null;
  // Correcci�n para poner barras autom�ticamente (ARS 22-05-01)
  //CFecha txtFNac = null;
  fechas.CFecha txtFNac = null;
  Label lblFNacCalc = null;

  // Sexo
  Label lblSexo = null;
  ChoiceCDDS choSexo = null;

  // Botones de salida
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();

  //Etiqueta
  Label lblNoHayProt = null;
  //Para protocolo
  public Panelprueba pProtocolo = null;
  DataProtocolo datProtocolo = null;

  // Escuchadores
  DiaCasoCentActionAdapter actionAdapter = null;
  DiaCasoCentFocusAdapter focusListener = null;
  DiaCasoCentKeyPressedAdapter keyListener = null;
  DiaCasoCentItemAdapter itemListener = null;

//___________________________________________________________________

  // Constructor
  // @param a: CApp
  // @param modo: modo de entrada al di�logo (constantes.*)
  // @param enf: Descripci�n de la enfermedad
  // @param enf: NotifRMC
  // @param c: CasoCent inicial (solo mod/baja/consulta) Nunca puede ser null.
  // @param oCaso: orden del caso (solo mod/baja/consulta)
  // @param tCasos: total casos de la NotifRMC
  // @param listaSexo: Lista Datas con CD y DS de los sexos
  public DiaCasoCent(CApp a, int modo, String enf, NotifRMC n, CasoCent c,
                     int oCaso, int tCasos, Lista listaSexo) {

    super(a);
    applet = a;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    eCent = enf;
    nRMC = n;
    cCent = c;
    lSexo = listaSexo;

    try {

      //Calculo del n�mero de  caso si es necesario: (que se haga solo una vez)
      switch (modoEntrada) {
        case constantes.modoALTA:
          sNumCaso = Integer.toString(Integer.parseInt(obtenerMaxNM_ORDENCasos()) +
                                      1);
          break;
        case constantes.modoMODIFICACION:
          sNumCaso = cCent.getNM_ORDEN();
          break;
        case constantes.modoBAJA:
          sNumCaso = cCent.getNM_ORDEN();
          break;
        case constantes.modoCONSULTA:
          sNumCaso = cCent.getNM_ORDEN();
          break;
      } // Fin switch

      // Iniciaci�n
      jbInit();

      switch (modoEntrada) {
        case constantes.modoALTA:
          setTitle("Alta Caso Centinela");
          txtNPac.setText("" + sNumCaso); //LRG

          int nextCaso;
          if (tCasos == -1 || tCasos == 0) {
            nextCaso = 1;
          }
          else {
            nextCaso = tCasos + 1;
          }
          ordenCaso = nextCaso;
          totalCasos = ordenCaso;
          break;
        case constantes.modoMODIFICACION:
          setTitle("Modificaci�n Caso Centinela");
          ordenCaso = oCaso;
          totalCasos = tCasos;
          rellenarDatos(cCent);
          break;
        case constantes.modoBAJA:
          setTitle("Baja Caso Centinela");
          ordenCaso = oCaso;
          totalCasos = tCasos;
          rellenarDatos(cCent);
          break;
        case constantes.modoCONSULTA:
          setTitle("Consulta Caso Centinela");
          ordenCaso = oCaso;
          totalCasos = tCasos;
          rellenarDatos(cCent);
          break;
      } // Fin switch

      // Relleno del numero de parte
      nParte.setText("Parte " + ordenCaso + " de " + totalCasos);

      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

//___________________________________________________________________

  // Inicia el aspecto del dialogo DiaCasoCent
  public void jbInit() throws Exception {
    // Carga las imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().put(constantes.imgCANCELAR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaCasoCentActionAdapter(this);
    focusListener = new DiaCasoCentFocusAdapter(this);
    keyListener = new DiaCasoCentKeyPressedAdapter(this);
    itemListener = new DiaCasoCentItemAdapter(this);

    // Enfermedad
    lblEnf = new Label("Enfermedad:");
    lblDsEnf = new Label(eCent);

    // Numero de parte (se rellena una vez calculado el orden y el total de parte)
    nParte = new Label();

    // N�mero de paciente
    lblNPac = new Label("Paciente n�:");
    txtNPac = new CTexto(2);
    txtNPac.setEnabled(false);
    txtNPac.setEditable(false);

    // Edad
    lblEdad = new Label("Edad:");
    txtEdad = new CEntero(3);
    txtEdad.addFocusListener(focusListener);
    txtEdad.addKeyListener(keyListener);
    txtEdad.setName("txtEdad");
    choEdades = new Choice();
    choEdades.setName("choEdades");
    choEdades.addItemListener(itemListener);
    choEdades.addItem("A�os");
    choEdades.addItem("Meses");
    choEdades.select("A�os");

    // Fecha Nacimiento
    lblFNac = new Label("Fecha Nacimiento:");
    // Correcci�n para poner barras autom�ticamente (ARS 22-05-01)
//    txtFNac = new CFecha("N");
    txtFNac = new fechas.CFecha("N");
    txtFNac.setName("txtFNac");
    txtFNac.addFocusListener(focusListener);
    txtFNac.addKeyListener(keyListener);
    lblFNacCalc = new Label("");

    // Sexo
    lblSexo = new Label("Sexo:");
    choSexo = new ChoiceCDDS(true, false, true, "CD_SEXO", "DS_SEXO", lSexo);
    choSexo.writeData();

    // Bot�n Aceptar
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setName("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgACEPTAR));
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);
    btnAceptar.addMouseListener(new DiaCasoCent_mouseAdapter(this));

    // Bot�n Cancelar
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setName("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgCANCELAR));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
    btnCancelar.addMouseListener(new DiaCasoCent_mouseAdapter(this));

    //Creaci�n de panel protocolo
    datProtocolo = new DataProtocolo(nRMC.getCD_ENFCIE(), eCent,
                                     null, null, "13",
                                     sNumCaso, nRMC);

    pProtocolo = new Panelprueba(app, datProtocolo, this);

    hayProtocolo = ! (pProtocolo.pnl.getNoSeEncontraronDatos());

    // Organizacion del panel en funci�n de si hay protocolo o no
    if (hayProtocolo) {
      this.setSize(new Dimension(750, 500));
      this.setLayout(xYLayout1);
      xYLayout1.setWidth(750);
      xYLayout1.setHeight(500);
    }
    else {
      this.setSize(new Dimension(550, 260));
      this.setLayout(xYLayout1);
      xYLayout1.setWidth(550);
      xYLayout1.setHeight(260);

    }

    // Adici�n de componentes al di�logo
    this.add(lblEnf, new XYConstraints(15, 15, 75, -1));
    this.add(lblDsEnf, new XYConstraints(100, 15, 250, -1));
    this.add(nParte, new XYConstraints(430, 15, 80, -1));
    this.add(lblNPac, new XYConstraints(15, 45, 75, -1));
    this.add(txtNPac, new XYConstraints(100, 45, 55, -1));
    this.add(lblEdad, new XYConstraints(170, 45, 35, -1));
    this.add(txtEdad, new XYConstraints(215, 45, 35, -1));
    this.add(choEdades, new XYConstraints(260, 45, 80, -1));
    this.add(lblFNac, new XYConstraints(15, 75, 110, -1));
    this.add(txtFNac, new XYConstraints(135, 75, 80, -1));
    this.add(lblFNacCalc, new XYConstraints(225, 75, 30, -1));
    this.add(lblSexo, new XYConstraints(15, 105, 35, -1));
    this.add(choSexo, new XYConstraints(60, 105, 80, -1));

    if (hayProtocolo) {
//      this.add(pProtocolo, new XYConstraints(15,150 , 740, 345));
      this.add(pProtocolo, new XYConstraints(15, 150, 740, 250));
      this.add(btnAceptar, new XYConstraints(532 + 15, 400 + 15, 80, -1));
      this.add(btnCancelar, new XYConstraints(627 + 15, 400 + 15, 80, -1));
    }
    else {
      lblNoHayProt = new Label();
      lblNoHayProt.setText("No hay protocolo asociado a esta enfermedad");
      this.add(lblNoHayProt, new XYConstraints(15, 135, 350, -1));
      this.add(btnAceptar, new XYConstraints(332, 180, 80, -1));
      this.add(btnCancelar, new XYConstraints(427, 180, 80, -1));
    }
    // Tool Tips
    new CContextHelp("Aceptar", btnAceptar);
    new CContextHelp("Cancelar", btnCancelar);

  }

//___________________________________________________________________

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  //Puede ser llamada desde un componente
  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar()

//___________________________________________________________________

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        enableControls(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        if ( (pProtocolo != null) && hayProtocolo) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.WAIT_CURSOR);
        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case constantes.modoALTA:
          case constantes.modoMODIFICACION:
            enableControls(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            if ( (pProtocolo != null) && hayProtocolo) {
              pProtocolo.pnl.habilitarCampos();
            }
            break;

          case constantes.modoBAJA:
          case constantes.modoCONSULTA:
            enableControls(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);

            if ( (pProtocolo != null) && hayProtocolo) {
              pProtocolo.pnl.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

            }
            if (modoOperacion == constantes.modoCONSULTA) {
              btnAceptar.setEnabled(false);
            }
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch

  }

//___________________________________________________________________

  public void enableControls(boolean state) {
    txtEdad.setEnabled(state);
    choEdades.setEnabled(state);
    txtFNac.setEnabled(state);
    choSexo.setEnabled(state);
  } // Fin enableControls()

//___________________________________________________________________

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

//___________________________________________________________________

  public void modificarXX() {

    //continuamos con el protocolo
    if (pProtocolo != null && hayProtocolo) {
      //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
      boolean b = pProtocolo.pnl.ValidarObligatorios();
      if (b) {
        pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo
        //dispose(); //Enrique quiere que se quede abto
        // estaba en la pesta�a de protocolo, continuara alli
      }
    }

  }

//___________________________________________________________________

  private void insertarXX() {

    //continuamos con el protocolo
    if (pProtocolo != null && hayProtocolo) {
      //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
      boolean b = pProtocolo.pnl.ValidarObligatorios();
      if (b) {
        pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo
        //dispose(); //Enrique quiere que se quede abto
        // estaba en la pesta�a de protocolo, continuara alli
      }
    }
//  Inicializar();
    //-----------------------------

  } //fin de insertar_XX

//___________________________________________________________________

  /******************** Funciones espec�ficas *********************/

  private String obtenerMaxNM_ORDENCasos() {
    Lista lQuery = new Lista();
    Lista lResul = null;
    QueryTool2 qt = CasoCent.getMaxNM_ORDEN(nRMC.getCD_ENFCIE(),
                                            nRMC.getCD_PCENTI(),
                                            nRMC.getCD_MEDCEN(),
                                            nRMC.getCD_ANOEPI(),
                                            nRMC.getCD_SEMEPI());

    // Ejecuci�n de la query
    lQuery.addElement(qt);

    try {
      this.getApp().getStub().setUrl(servlet);
      /*lResul =  BDatos.execSQL(applet,servlet,1,lQuery); // 1: DO_SELECT*/

      /*
            // SOLO DESARROLLO
            SrvQueryTool srv = new SrvQueryTool();
            lResul = BDatos.execSQLDebug(applet,srv,1,lQuery); // 1: DO_SELECT
       */

      lResul = BDatos.ejecutaSQL(true, applet, servlet, 1, lQuery); // 1: DO_SELECT

      // Error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // Lista vacia
    if (lResul.size() == 0) {
      return (new Integer(0)).toString();
    }
    else {
      return ( (Data) lResul.firstElement()).getString("NM_ORDEN");
    }
  } // Fin obtenerMaxNM_ORDENCasos()

  /******************** Funciones de mantenimiento ****************/

  // Recoge los datos existentes en los componentes del di�logo
  private CasoCent recogerDatos() {
    CasoCent nResul = new CasoCent();

    nResul.put("CD_ENFCIE", nRMC.getCD_ENFCIE());
    nResul.put("CD_PCENTI", nRMC.getCD_PCENTI());
    nResul.put("CD_MEDCEN", nRMC.getCD_MEDCEN());
    nResul.put("CD_ANOEPI", nRMC.getCD_ANOEPI());
    nResul.put("CD_SEMEPI", nRMC.getCD_SEMEPI());
    nResul.put("NM_ORDEN", txtNPac.getText());
    nResul.put("CD_SEXO", choSexo.getChoiceCD());
    nResul.put("FC_NAC", txtFNac.getText());
    if (lblFNacCalc.getText().equals("Real")) {
      nResul.put("IT_CALC", "N");
    }
    else {
      nResul.put("IT_CALC", "S");
    }
    nResul.put("CD_OPE", getApp().getParameter("COD_USUARIO"));
    nResul.put("FC_ULTACT", "");

    return nResul;
  } // Fin recogerDatos()

  // Rellena los componentes del di�logo con los datos existentes en la estructura
  private void rellenarDatos(CasoCent c) {
    // N� Paciente
    txtNPac.setText(c.getNM_ORDEN());

    // C�lculo de la edad
    String edad = "";
    if (!c.getFC_NAC().equals("")) {
      edad += Fechas.edadAnios(Format.string2Date(c.getFC_NAC()));
    }
    txtEdad.setText(edad);
    choEdades.select("A�os");

    // Fecha de nacimiento
    txtFNac.setText(c.getFC_NAC());
    if (c.getIT_CALC().equals("S")) {
      lblFNacCalc.setText("Calc");
    }
    else if (c.getIT_CALC().equals("N")) {
      lblFNacCalc.setText("Real");

      // Sexo
    }
    choSexo.selectChoiceElement(c.getCD_SEXO());
  } // Fin rellenarDatos()

  // Dev. true si los datos de los componentes del di�logo son v�lidos
  boolean validarDatos() {

    // 1. PERDIDAS DE FOCO: Campos CD-Button-DS (No hay)
    // 2. LONGITUDES: No hay
    // 3. DATOS NUMERICOS: No hay
    // 4. CAMPOS OBLIGATORIOS: No hay
    //Solo hay datos del panel protocolo
    //(ARS 11-05-01) �Qu� pasa si ocurre que el protocolo
    // no tiene datos obligatorios y est� vac�o?
    // Pues que no graba. Esto no queda bonito ...
    // (yo creo que lleva as� desde los Reyes Godos, pero ..)
    // p-al caso, que hay que cambiarlo.
    //Devuelto de ValidarObligatorio:
    /*
      //true : si listo para insertar
      //true:   si modifica borrando todos los datos.
      //false:  faltan campos obligatorios
      //false:  si es un alta, pulsaria aceptar, pero no lleva datos
     */
    boolean valorRetorno = true;

    if (pProtocolo != null && hayProtocolo) {
      //listadatos
      valorRetorno = pProtocolo.pnl.ValidarObligatorios();
    }
    // Si llega aqui todo ha ido bien
    return valorRetorno;
  } // Fin validarDatos()

  // Dev. true si son iguales ambas estructuras
  boolean compararCampos(CasoCent n1, CasoCent n2) {
    if (n1 == null || n2 == null) {
      return false;
    }

    if (!n1.getCD_ENFCIE().equals(n2.getCD_ENFCIE())) {
      return false;
    }

    if (!n1.getCD_PCENTI().equals(n2.getCD_PCENTI())) {
      return false;
    }

    if (!n1.getCD_MEDCEN().equals(n2.getCD_MEDCEN())) {
      return false;
    }

    if (!n1.getCD_ANOEPI().equals(n2.getCD_ANOEPI())) {
      return false;
    }

    if (!n1.getCD_SEMEPI().equals(n2.getCD_SEMEPI())) {
      return false;
    }

    if (!n1.getNM_ORDEN().equals(n2.getNM_ORDEN())) {
      return false;
    }

    if (!n1.getCD_SEXO().equals(n2.getCD_SEXO())) {
      return false;
    }

    if (!n1.getFC_NAC().equals(n2.getFC_NAC())) {
      return false;
    }

    if (!n1.getIT_CALC().equals(n2.getIT_CALC())) {
      return false;
    }

    if (!n1.getCD_OPE().equals(n2.getCD_OPE())) {
      return false;
    }

    if (!n1.getFC_ULTACT().equals(n2.getFC_ULTACT())) {
      return false;
    }

    return true;
  } // Fin compararCampos()

  //Devuelve true si no se han cambiado las respuestas
  //No implementado aun
  boolean compararRespuestas() {
    return false;
  }

  /******************** M�todos para el bloqueo *******************/

  // Devuelve una Lista cuyo primer elemento es la QueryTool del bloqueo
  //  y su segundo elemento es el Data del bloqueo
  private Lista prepararBloqueo() {
    Lista vResult = new Lista();
    QueryTool qtBloqueo = new QueryTool();
    Data dtBloqueo = new Data();

    // Tabla  a bloquear
    qtBloqueo.putName("SIVE_NOTIF_RMC");

    // Campos CD_OPE y FC_ULTACT
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Filtro del registro a bloquear
    qtBloqueo.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ENFCIE", nRMC.getCD_ENFCIE());
    qtBloqueo.putOperator("CD_ENFCIE", "=");
    qtBloqueo.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_PCENTI", nRMC.getCD_PCENTI());
    qtBloqueo.putOperator("CD_PCENTI", "=");
    qtBloqueo.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_MEDCEN", nRMC.getCD_MEDCEN());
    qtBloqueo.putOperator("CD_MEDCEN", "=");
    qtBloqueo.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANOEPI", nRMC.getCD_ANOEPI());
    qtBloqueo.putOperator("CD_ANOEPI", "=");
    qtBloqueo.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_SEMEPI", nRMC.getCD_SEMEPI());
    qtBloqueo.putOperator("CD_SEMEPI", "=");

    // Valores del bloqueo
    dtBloqueo.put("CD_OPE", nRMC.getCD_OPE());
    dtBloqueo.put("FC_ULTACT", nRMC.getFC_ULTACT());

    // Devolucion
    vResult.addElement(qtBloqueo);
    vResult.addElement(dtBloqueo);
    return vResult;
  } // Fin prepararBloqueo()

  /******************** Acceso a vars. salida *********************/

  //  Devuelve true si se sale mediante el bot�n de Aceptar
  public boolean getAceptar() {
    return OK;
  } // Fin getAceptar()

  // Devuelve el CasoCent tratado, o null si ninguno se ha tratado
  public CasoCent getCasoCent() {
    return cCentSal;
  } // Fin getCasoCent()

  // Devuelve la NotifRMC tratada, o null si ninguna se ha tratado
  public NotifRMC getNotifRMC() {
    return nRMC;
  } // Fin getCasoCent()

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {
    QueryTool2 qt = null;
    QueryTool2 qtInsert = null;
    Data dtInsert = null;
    QueryTool2 qtUpdate = null;
    Data dtUpdate = null;
    QueryTool2 qtDelete = null;
    Data dtDelete = null;
    QueryTool2 qtUpdateNRMC = null;
    Data dtUpdateNRMC = null;
    Lista lQuery = new Lista();
    Lista lBloqueo = null;
    Lista lResult = null;

    QueryTool2 qtDeleteResp = null; //Para resptas
    Data dtDeleteResp = null;
    boolean OKRespuestas = true; //Operaciones sobre resptas bien

    switch (modoEntrada) {
      case constantes.modoALTA:
        if (validarDatos()) {
          // Recogida de datos en un CasoCent
          cCentSal = recogerDatos();

          /****************************************************/
          /****************************************************/
          /******** AQU� VAN LOS AVISOS DE LAS ALARMAS ********/
          // Metemos los par�metros en un data
          Data dtParametrosAlarma = new Data();
          dtParametrosAlarma.put("CD_ANOEPI", nRMC.getCD_ANOEPI());
          dtParametrosAlarma.put("CD_SEMEPI", nRMC.getCD_SEMEPI());
          dtParametrosAlarma.put("CD_ENFCIE", nRMC.getCD_ENFCIE());
          dtParametrosAlarma.put("NUM_CASOS", Integer.toString(totalCasos));
          dtParametrosAlarma.put("IT_TIPO", "T");
          // El data en una lista (el viejo truco)
          Lista lParametrosAlarma = new Lista();
          lParametrosAlarma.addElement(dtParametrosAlarma);
          // La lista devuelta por el servlet
          Lista lDatosAlarma = new Lista();
          // Lista para mostrar mensajes
          Lista lMensajes = new Lista();

          /*          // debug
                    SrvDisparaAlarma srv = new SrvDisparaAlarma();
                    // par�metros jdbc
                    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                     "sive_desa",
                                     "sive_desa");
                    lDatosAlarma = srv.doDebug(0, lParametrosAlarma);
                    // Si existen indices superados, pues avisamos.*/
          try {
            this.getApp().getStub().setUrl("servlet/SrvDisparaAlarma");
            lDatosAlarma = (Lista)this.getApp().getStub().doPost(0,
                lParametrosAlarma);
          }
          catch (Exception e) {
            this.getApp().trazaLog(e);
            this.getApp().showError(e.getMessage());
          }

          if (lDatosAlarma.size() > 0) {
            String Msj = "";
            Data dtTmp = null;
            for (int a = 0; a < lDatosAlarma.size(); a++) {
              dtTmp = (Data) lDatosAlarma.elementAt(a);
              Msj = dtTmp.getString("CD_INDALAR") + ": " +
                  dtTmp.getString("DS_INDALAR") + '&' +
                  dtTmp.getString("NM_COEF") + '&' +
                  dtTmp.getString("NM_VALOR") + '&' +
                  dtTmp.getString("NUM_UMBRAL");
              lMensajes.addElement(Msj);
            }
            Data dtCabecera = new Data(); // Datos que aparecer�n en la cabecera
            // del di�logo
            dtCabecera.put("ENFERMEDAD", eCent);
            dtCabecera.put("SEMANA",
                           nRMC.getCD_ANOEPI() + "-" + nRMC.getCD_SEMEPI());
            DiaMsgAviso dma = new DiaMsgAviso(this.getApp(), 0, lMensajes,
                                              dtCabecera);
            dma.show();
            dma = null;
            dtTmp = null;
            Msj = null;
          }
          // Lo destruimos todo y no dejamos ni rastro...
          lDatosAlarma = null;
          lParametrosAlarma = null;
          dtParametrosAlarma = null;

          /******** FINAL DE LOS AVISOS DE LAS ALARMAS ********/
          /****************************************************/
          /****************************************************/
          // NotifRMC modificada
          NotifRMC nRMCMod = new NotifRMC();
          String sKey;
          Enumeration e2 = nRMC.elements();
          for (Enumeration e1 = nRMC.keys(); e1.hasMoreElements(); ) {
            sKey = (String) e1.nextElement();
            // NM_CASOSDEC, FC_RECEP y el resto
            if (sKey.equals("NM_CASOSDEC")) {
              nRMCMod.put("NM_CASOSDEC", "" + totalCasos);
              e2.nextElement();
            }
            else if (sKey.equals("FC_RECEP")) {
              if (nRMC.getFC_RECEP().equals("")) {
                nRMCMod.put("FC_RECEP", Format.fechaActual());
                e2.nextElement();
              }
              else {
                nRMCMod.put("FC_RECEP", nRMC.getFC_RECEP());
                e2.nextElement();
              }
              // Actualizar CD_OPE; ARS 12-03-01
            }
            else if (sKey.equals("CD_OPE")) {
              nRMCMod.put("CD_OPE", this.getApp().getParameter("COD_USUARIO"));
              e2.nextElement();
            }
            else {
              nRMCMod.put(sKey, e2.nextElement());
            }
          }
          // Lista de QueryTools con sus modos de operacion
          qtUpdateNRMC = NotifRMC.getUpdateOneNotifRMC(nRMCMod);
          dtUpdateNRMC = new Data();
          dtUpdateNRMC.put("4", qtUpdateNRMC); // DO_UPDATE
          lQuery.addElement(dtUpdateNRMC);
          qtInsert = CasoCent.getInsertOneCasoCent(cCentSal);
          dtInsert = new Data();
          dtInsert.put("3", qtInsert); // DO_INSERT
          lQuery.addElement(dtInsert);

          // Preparacion del bloqueo (QueryTool + Data)
          lBloqueo = prepararBloqueo();

          try {

            // Acceso al servidor con bloqueo sobre nRMC
            lResult =
                BDatos.execSQLBloqueo(applet, srvTrans, 10000, lQuery,
                                      (QueryTool) lBloqueo.elementAt(0),
                                      (Data) lBloqueo.elementAt(1)); // Transaccion

            // Actualizacion del CD_OPE y de la FC_ULTACT de nRMC
            nRMCMod.setCD_OPE(getApp().getParameter("COD_USUARIO"));
            nRMCMod.setFC_ULTACT( ( (Lista) lResult.firstElement()).
                                 getFC_ULTACT());
            nRMC = nRMCMod;

            // Se ha dado de alta un CasoCent
            OK = true;

            // Desaparece el dialogo
            // dispose();
          }
          catch (Exception e) {
            e.printStackTrace();
            OK = false;
          } // Fin try .. catch

          //Si se ha insertado caso y hay protocolo inserta respuestas
          if (OK && hayProtocolo) {
            try {
              //Se insertan respuestas
              insertarXX();
              OKRespuestas = true;
            }
            catch (Exception e) {
              e.printStackTrace();
              OKRespuestas = false;
            } // Fin try .. catch
          }

          //Cierra di�logo si todo ha ido bien
          //Caso bien y (no hay protocolo o resptas bien)
          if (OK && ( (!hayProtocolo) || OKRespuestas)) {
            dispose();

          }

        } // validarDatos() ya muestra mensajes de error

        break;

      case constantes.modoMODIFICACION:
        if (validarDatos()) {

          // Recogida de datos en un CasoCent
          cCentSal = recogerDatos();

          //Si no han cambiado datos, cierra el di�logo y mantiene OK como estaba
          if (compararCampos(cCentSal, cCent) && compararRespuestas()) {
            //Nota: flag OK queda como estaba: si antes se hab�a cambiado caso
            //pero hubo fallo en resptas
            dispose();
          }
          else {
            // NotifRMC modificada:
            // ARS 12-03-01. Esto se hace para que actualice
            // el CD_OPE cuando se actualiza SIVE_NOTIF_RMC.
            NotifRMC nRMCMod = new NotifRMC();
            String sKey;
            Enumeration e2 = nRMC.elements();
            for (Enumeration e1 = nRMC.keys(); e1.hasMoreElements(); ) {
              sKey = (String) e1.nextElement();
              if (sKey.equals("CD_OPE")) {
                nRMCMod.put("CD_OPE", this.getApp().getParameter("COD_USUARIO"));
                e2.nextElement();
              }
              else {
                nRMCMod.put(sKey, e2.nextElement());
              }
            }

            // Lista de QueryTools con sus modos de operacion
            qtUpdateNRMC = NotifRMC.getUpdateOneNotifRMC(nRMC);
            dtUpdateNRMC = new Data();
            dtUpdateNRMC.put("4", qtUpdateNRMC); // DO_UPDATE
            lQuery.addElement(dtUpdateNRMC);
            qtUpdate = CasoCent.getUpdateOneCasoCent(cCentSal);
            dtUpdate = new Data();
            dtUpdate.put("4", qtUpdate); // DO_UPDATE
            lQuery.addElement(dtUpdate);

            // Preparacion del bloqueo (QueryTool + Data)
            lBloqueo = prepararBloqueo();

            try {
              this.getApp().getStub().setUrl(servlet);
              lResult =
                  BDatos.execSQLBloqueo(applet, srvTrans, 10000, lQuery,
                                        (QueryTool) lBloqueo.elementAt(0),
                                        (Data) lBloqueo.elementAt(1)); // 4: DO_UPDATE

              nRMC = nRMCMod;

              // Actualizacion del CD_OPE y de la FC_ULTACT de nRMC
              nRMC.setCD_OPE(getApp().getParameter("COD_USUARIO"));
              nRMC.setFC_ULTACT( ( (Lista) lResult.firstElement()).getFC_ULTACT());

              // Se ha modificado un CasoCent
              OK = true;

              // Desaparece el dialogo
//              dispose();
            }
            catch (Exception e) {
              e.printStackTrace();
              OK = false;

            } // Fin try .. catch
          } // Fin compararCampos()

          //Si se ha insertado caso inserta respuestas
          if (OK) {
            //Respuestas
            try {
              //Se insertan las respuestas
              modificarXX();
              OKRespuestas = true;
            }
            catch (Exception e) {
              e.printStackTrace();
              OKRespuestas = false;
            } // Fin try .. catch
          }

          //Cierra di�logo si todo ha ido bien
          //Caso bien y (no hay protocolo o resptas bien)
          if (OK && ( (!hayProtocolo) || OKRespuestas)) {
            dispose();

          }
        } // validarDatos() ya muestra mensajes de error

        break;

      case constantes.modoBAJA:

        // Recogida de datos en un CasoCent
        cCentSal = recogerDatos();

        // NotifRMC modificada
        NotifRMC nRMCMod = new NotifRMC();
        String sKey;
        Enumeration e2 = nRMC.elements();
        for (Enumeration e1 = nRMC.keys(); e1.hasMoreElements(); ) {
          sKey = (String) e1.nextElement();

          if (sKey.equals("NM_CASOSDEC")) {
            nRMCMod.put("NM_CASOSDEC", "" + (totalCasos - 1));
            e2.nextElement();
          }
          else if (sKey.equals("CD_OPE")) {
            nRMCMod.put("CD_OPE", this.getApp().getParameter("COD_USUARIO"));
            e2.nextElement();
          }
          else {
            nRMCMod.put(sKey, e2.nextElement());
          }
        }

//        System.out.println("REgistro not " + nRMC);
//        System.out.println("REgistro not modificado" + nRMCMod);

        // Lista de QueryTools con sus modos de operacion
        qtUpdateNRMC = NotifRMC.getUpdateOneNotifRMC(nRMCMod);
        dtUpdateNRMC = new Data();
        dtUpdateNRMC.put("4", qtUpdateNRMC); // DO_UPDATE
        lQuery.addElement(dtUpdateNRMC);

        //Borra primero resptas del caso
        qtDeleteResp = CasoCent.getDeleteRespCentiOfOneCasoCent(cCentSal);
        dtDeleteResp = new Data();
        dtDeleteResp.put("5", qtDeleteResp); // DO_DELETE de resptas
        lQuery.addElement(dtDeleteResp);

        //Borra el caso
        qtDelete = CasoCent.getDeleteOneCasoCent(cCentSal);
        dtDelete = new Data();
        dtDelete.put("5", qtDelete); // DO_DELETE
        lQuery.addElement(dtDelete);

        // Preparacion del bloqueo (QueryTool + Data)
        lBloqueo = prepararBloqueo();

        try {

          this.getApp().getStub().setUrl(srvTrans);

          lResult =
              BDatos.execSQLBloqueo(applet, srvTrans, 10000, lQuery,
                                    (QueryTool) lBloqueo.elementAt(0),
                                    (Data) lBloqueo.elementAt(1)); // Transaccion

          /*
                   //  SOLO DESARROLLO
                    SrvTransaccion srv= new SrvTransaccion();
                    //Indica como conectarse a la b.datos
                    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                     "sive_desa",
                                     "sive_desa");
                    lResult= srv.doDebug(0,lQuery);
           */
          // Actualizacion del CD_OPE y de la FC_ULTACT de nRMC
          nRMCMod.setCD_OPE(getApp().getParameter("COD_USUARIO"));
          nRMCMod.setFC_ULTACT( ( (Lista) lResult.firstElement()).getFC_ULTACT());
          nRMC = nRMCMod;

          // Se ha dado de baja un CasoCent
          OK = true;

          // Desaparece el dialogo
          dispose();
        }
        catch (Exception e) {
          OK = false;
          e.printStackTrace();
        } // Fin try .. catch

        break;

      case constantes.modoCONSULTA:

        // Desaparece el di�logo
        dispose();
        break;
    } // Fin switch
  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    // Desaparece el dialogo
    dispose();
  } // Fin btnCancelarActionPerformed()

  void setTxtEdad(String anoMes, String fNac) {
    if (anoMes.equals("A�os")) {
      txtEdad.setText("" + Fechas.edadAnios(Format.string2Date(fNac)));
    }
    else if (anoMes.equals("Meses")) {
      txtEdad.setText("" + Fechas.edadMeses(Format.string2Date(fNac)));
    }
  }

  void setTxtFNac(String anoMes, String edad) {
    Date d = null;
    String fecha = "";

    // Cambios 24-04-01 (ARS) Resulta que se debe calcular de la fecha de notificaci�n
    // -------------------------------------------------------------------------------
    String fechaNotif = "";
    try {
      centinelas.cliente.c_fechas.conversorfechas cf = new centinelas.cliente.
          c_fechas.conversorfechas(nRMC.getCD_ANOEPI(), this.getApp());
      fechaNotif = cf.getFec(Integer.parseInt(nRMC.getCD_SEMEPI()));
      Date fechaNaci = Fechas.string2Date(fechaNotif);
      if (anoMes.equals("A�os")) {

//        d = Fechas.restaTiempo(Integer.parseInt(edad),false,new Date());
        d = Fechas.restaTiempo(Integer.parseInt(edad), false, fechaNaci);
      }
      else if (anoMes.equals("Meses")) {

//        d = Fechas.restaTiempo(Integer.parseInt(edad),true,new Date());
        d = Fechas.restaTiempo(Integer.parseInt(edad), true, fechaNaci);
        //Al poner fecha calculada a partir de edad,
        //esta se expresa de forma que siempre sea el d�a 15
//      fecha = Fechas.getFecha(15, Fechas.getDia(d), Fechas.getMes(d), Fechas.getAno(d));
        // modificacion jlt  03/09/01
        // la fecha definitiva de nacimiento es a partir de
        // la fecha de notificaci�n, pero con d�a 15
        ///
      }
      fecha = Fechas.getFecha(15, Fechas.getMes(d), Fechas.getAno(d));
      /////
      // Correcci�n (ARS) 24-04-01
      // mofifiacion jlt fecha = Fechas.date2String(d);
//     txtFNac.setText(Fechas.date2String(d));
      txtFNac.setText(fecha);
    }
    catch (Exception fechaCajca) {
      this.getApp().trazaLog("Error gordo por: " + fechaCajca);
    }
  }

  void txtEdadFocusLost() {
    if (!txtEdad.getText().equals("")) {
      if (lblFNacCalc.getText().equals("Calc")) {
        // Se establece txtFNac
        setTxtFNac(choEdades.getSelectedItem(), txtEdad.getText());
      }
    }
    else {
      if (!txtFNac.getText().equals("")) {
        // Se establece txtEdad
        setTxtEdad(choEdades.getSelectedItem(), txtFNac.getText());
      }
      else {
        lblFNacCalc.setText("");
      }
    }
  } // Fin txtEdadFocusLost()

  void txtFNacFocusLost() {
    if (!txtFNac.getText().equals("")) {
      txtFNac.ValidarFecha();
      if (txtFNac.getValid().equals("S")) {
        if (lblFNacCalc.getText().equals("Real")) {
          // Se establece txtEdad
          setTxtEdad(choEdades.getSelectedItem(), txtFNac.getText());
        }
      }
      else {
        txtFNac.setText("");
      }
    }
    else {
      if (!txtEdad.getText().equals("")) {
        // Se establece txtFNac
        setTxtFNac(choEdades.getSelectedItem(), txtEdad.getText());
      }
      else {
        lblFNacCalc.setText("");
      }

    }
  } // Fin txtFNacFocusLost()

  void txtEdadKeyPressed(KeyEvent e) {
    //calcOreal = "Calc";
    if (e.getKeyCode() != KeyEvent.VK_TAB) {
      lblFNacCalc.setText("Calc");
    }
    else {
      txtEdadFocusLost();
    }
  } // Fin txtEdadKeyPressed()

  void txtFNacKeyPressed(KeyEvent e) {
    //calcOreal = "Real";
    if (e.getKeyCode() != KeyEvent.VK_TAB) {
      lblFNacCalc.setText("Real");
    }
    else {
      txtFNacFocusLost();
    }
  } // Fin txtFNacKeyPressed()

  void choEdadesItemStateChanged() {
    if (lblFNacCalc.getText().equals("Real")) {
      txtFNacFocusLost();
    }
    else if (lblFNacCalc.getText().equals("Calc")) {
      txtEdadFocusLost();
    }
  } // Fin choEdadesItemStateChanged()

  void btnAjustaFoco(MouseEvent e) {

    if (e.getComponent().getName().equals("Aceptar")) {
      btnAceptar.transferFocus();
      btnAceptar.requestFocus();
    }

    if (e.getComponent().getName().equals("Cancelar")) {
//      btnCancelar.transferFocus();
      btnCancelar.requestFocus();
    }
  }

} // endclass DiaCasoCent

/******************* ESCUCHADORES **********************/

// Botones
class DiaCasoCentActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaCasoCent adaptee;
  ActionEvent evt;

  DiaCasoCentActionAdapter(DiaCasoCent adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaCasoCentActionAdapter

// P�rdidas de foco
class DiaCasoCentFocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DiaCasoCent adaptee;
  FocusEvent evt;

  DiaCasoCentFocusAdapter(DiaCasoCent adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("txtEdad")) {
        adaptee.txtEdadFocusLost();
      }
      else if ( ( (TextField) evt.getSource()).getName().equals("txtFNac")) {
        adaptee.txtFNacFocusLost();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass DiaCasoCentFocusAdapter

// Tecla presionada
class DiaCasoCentKeyPressedAdapter
    extends java.awt.event.KeyAdapter
    implements Runnable {
  DiaCasoCent adaptee;
  KeyEvent evt;

  DiaCasoCentKeyPressedAdapter(DiaCasoCent adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("txtEdad")) {
        adaptee.txtEdadKeyPressed(evt);
      }
      else if ( ( (TextField) evt.getSource()).getName().equals("txtFNac")) {
        adaptee.txtFNacKeyPressed(evt);
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass DiaCasoCentKeyPressedAdapter

// Cambio de selecci�n en un Choice
class DiaCasoCentItemAdapter
    implements Runnable, java.awt.event.ItemListener {
  DiaCasoCent adaptee;
  ItemEvent evt;

  DiaCasoCentItemAdapter(DiaCasoCent adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (Choice) evt.getSource()).getName().equals("choEdades")) {
        adaptee.choEdadesItemStateChanged();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass DiaCasoCentItemAdapter

// Para corregir el doble 'clic' de los botones.
class DiaCasoCent_mouseAdapter
    extends java.awt.event.MouseAdapter {
  DiaCasoCent adaptee;

  DiaCasoCent_mouseAdapter(DiaCasoCent adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseEntered(MouseEvent e) {
    adaptee.btnAjustaFoco(e);
  }
}
