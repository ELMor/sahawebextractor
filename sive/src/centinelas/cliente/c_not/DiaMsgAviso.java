package centinelas.cliente.c_not;

import java.awt.Color;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.LabelControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CTabla;
import sapp2.Data;
import sapp2.Lista;

/**
 * Di�logo a trav�s del cu�l se muestra una lista de los ficheros creados
 */

public class DiaMsgAviso
    extends CDialog {

  XYLayout xYLayout1 = new XYLayout();

  CTabla tbl = new CTabla();
  ButtonControl btnAceptar = new ButtonControl();

  LabelControl labelEnf = new LabelControl();
  LabelControl labelSem = new LabelControl();

  Lista vError = new Lista();
  Data cabecera = new Data();

  // Escuchadores de eventos...

  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  // constructor del di�logo DiaMant
  public DiaMsgAviso(CApp a, int modo, Lista lErrores, Data cabecera) {

    super(a);
    this.cabecera = cabecera;

    try {
      this.setTitle("Indicadores superados con la notificaci�n");
      vError = lErrores;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {

    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(620, 370);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    labelEnf.setText("Enfermedad: " + cabecera.getString("ENFERMEDAD"));
    labelSem.setText("Semana epidemiol�gica: " + cabecera.getString("SEMANA"));

    this.add(labelEnf, new XYConstraints(35, 45, -1, -1));
    this.add(labelSem, new XYConstraints(323, 45, -1, -1));

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Salir");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));

    tbl.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        "Indicador\nCoeficiente\nValor\nUmbral", '\n'));
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "295\n95\n95\n95"), '\n'));
    tbl.getList().setHighlightColor(Color.lightGray);
    tbl.setNumColumns(4);
    tbl.setColumnAlignment(1, 2);
    tbl.setColumnAlignment(2, 2);
    tbl.setColumnAlignment(3, 2);

    /*
        tabla.setAutoSelect(true);
        tabla.addItemListener(multlstItemListener);
     */

    // a�adimos los errores..
    for (int i = 0; i < vError.size(); i++) {
      tbl.addItem( (String) vError.elementAt(i), '&');
    }

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);

    xYLayout1.setWidth(625);
    xYLayout1.setHeight(340);

    this.add(btnAceptar, new XYConstraints(500, 300, -1, -1));
    this.add(tbl, new XYConstraints(12, 80, 600, 200));
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    if (e.getActionCommand().equals("Aceptar")) {
      this.dispose();
    }
  }
}

// botones de aceptar y cancelar.

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMsgAviso adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMsgAviso adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
