package centinelas.cliente.c_mantcartasavisos;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFileName;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPnlCodigo;
import centinelas.cliente.c_creaciondbf.DBFCartasEtiq;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

// Esta l�nea se debe comentar cuando quede corregido.
//import centinelas.servidor.c_mantcartas.*;

public class DiaMantCartasAvisos
    extends CDialog
    implements CInicializar, CFiltro {

  // modos de operaci�n de la ventana
  final public int modoMODIFICACION = 0;
  // Servlet de avisos
  final protected int servletCARTETIQ_AVISOS = 1;

  // parametros pasados al di�logo
  public Data dtDev = null;

  //par�metros devueltos por el di�logo
  public Data dtDevuelto = new Data();

  // gesti�n aceptar
  public boolean bAceptar = false;

  // vectores CLista.
  Vector vLabels = new Vector();
  Vector vBotones = new Vector();

  Lista lServ = null;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblCentinela = new Label();
  CCodigo txtPtoCentinela = null;
  TextField txtDescPlantilla = null;
  Checkbox chkImprimir = new Checkbox();
  CListaMantenimiento clmPlantillas = null;
  CPnlCodigo pnlModelo = null;
  CFileName pnlBuscarFichero = null;

  //Applet
  CApp applet = null;

  Label lblImprimir = new Label();
  ButtonControl btnAceptar = new ButtonControl();
  Label lblModelo = new Label();
  Label lblObservaciones = new Label();
  TextField txtObservaciones = new TextField();
  ButtonControl btnCancelar = new ButtonControl();

  //__________________________________________________________________

  //constructor del di�logo DiaMantCartasAvisos
  public DiaMantCartasAvisos(CApp a, int modoop, Data dt) {
    super(a);
    applet = a;
    dtDev = dt;
    modoOperacion = modoop;
    // Consulta para los modelos.
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PLANTILLA_RMC");
    qt.putType("CD_PLANTILLA", QueryTool.STRING);
    qt.putType("DS_PLANTILLA", QueryTool.STRING);
    //Se crean comps
    txtPtoCentinela = new CCodigo(6);
    txtDescPlantilla = new TextField(40);
    // Panel de modelos
    pnlModelo = new CPnlCodigo(a,
                               this,
                               qt,
                               "CD_PLANTILLA",
                               "DS_PLANTILLA",
                               true,
                               "Modelos carta",
                               "Modelos carta");

    // Panel del fichero.
    pnlBuscarFichero = new CFileName(a);
    // Lista mantenimiento.
    // Sin botones
    // etiquetas
    vLabels.addElement(new CColumna("Modelo",
                                    "70",
                                    "CD_PLANTILLA"));

    vLabels.addElement(new CColumna("Fecha",
                                    "80",
                                    "FC_AVISO"));

    vLabels.addElement(new CColumna("Observaciones",
                                    "389",
                                    "DS_OBSERV"));
    clmPlantillas = new CListaMantenimiento(a,
                                            vLabels,
                                            vBotones,
                                            this,
                                            this,
                                            240,
                                            578);

    try {

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  //__________________________________________________________________

  void jbInit() throws Exception {
    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";

    xyLayout1.setHeight(540);
    xyLayout1.setWidth(635);
    this.setLayout(xyLayout1);
    this.setSize(635, 540);

    // carga las imagenes
    applet.getLibImagenes().put(imgACEPTAR);
    applet.getLibImagenes().put(imgCANCELAR);
    applet.getLibImagenes().CargaImagenes();

    txtPtoCentinela.setText("");
    txtDescPlantilla.setText("");
    chkImprimir.addItemListener(new DiaMantCartasAvisos_chkImprimir_itemAdapter(this));
    lblCentinela.setText("Punto centinela:");
    lblImprimir.setText("�Desea imprimir?");
    lblModelo.setText("Modelo:");
    lblObservaciones.setText("Observaciones:");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new
                                 DiaMantCartasAvisos_btnAceptar_actionAdapter(this));
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnCancelar.setLabel("Cancelar");
    btnCancelar.addActionListener(new
                                  DiaMantCartasAvisos_btnCancelar_actionAdapter(this));
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    this.add(lblCentinela, new XYConstraints(32, 14, 92, -1));
    this.add(txtPtoCentinela, new XYConstraints(134, 14, 73, -1));
    this.add(clmPlantillas, new XYConstraints(27, 53, 570, 207));
    this.add(txtDescPlantilla, new XYConstraints(209, 14, 254, -1));
    this.add(chkImprimir, new XYConstraints(144, 269, 23, -1));
    this.add(lblImprimir, new XYConstraints(32, 269, 109, -1));
    this.add(pnlModelo, new XYConstraints(97, 307, 498, 33));
    this.add(lblObservaciones, new XYConstraints(31, 359, 91, 28));
    this.add(txtObservaciones, new XYConstraints(126, 361, 468, 26));
    this.add(pnlBuscarFichero, new XYConstraints(18, 398, 549, 41));
    this.add(lblModelo, new XYConstraints(32, 311, 50, -1));
    this.add(btnAceptar, new XYConstraints(401, 458, -1, -1));
    this.add(btnCancelar, new XYConstraints(495, 458, -1, -1));

    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    // ponemos el t�tulo
    switch (modoOperacion) {
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
    }
  }

  public void rellenarDatos() {
    String pcentinela = dtDev.getString("CD_PCENTI");
    String medico = dtDev.getString("DS_MEDICO");
    medico = medico.substring(0, medico.indexOf("-")).trim();
    //Introducimos los datos en la pantalla
    txtPtoCentinela.setText(pcentinela);
    txtDescPlantilla.setText(buscaDescripcionCentinela(pcentinela));
    clmPlantillas.setPrimeraPagina(this.obtenDatosLista(pcentinela, medico));
    chkImprimir.setState(false);
    pnlBuscarFichero.txtFile.setText("c:\\datos.dbf");
  }

  // Pone los distintos campos en el estado que les corresponda
  // segun el modo.
  public void verDatos() {
    switch (modoOperacion) {
      case modoMODIFICACION:
        txtObservaciones.setEnabled(false);
        pnlModelo.setEnabled(false);
        pnlBuscarFichero.setEnabled(false);
        txtPtoCentinela.setVisible(true);
        txtPtoCentinela.setEnabled(false);
        txtDescPlantilla.setVisible(true);
        txtDescPlantilla.setEnabled(false);
        chkImprimir.setState(false);
        //Se rellenan los datos en la pantalla
        rellenarDatos();
        break;
    }
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoMODIFICACION:
            this.setEnabled(true);
            txtPtoCentinela.setVisible(true);
            txtPtoCentinela.setEnabled(false);
            txtDescPlantilla.setVisible(true);
            txtDescPlantilla.setEnabled(false);
            chkImprimir.setEnabled(true);
            break;
        }
        break;
    }
  }

  public void realizaOperacion() {}

  public void realizaOperacion(int i) {}

  public Lista siguientePagina() {
    Lista l = new Lista();
    return l;
  }

  //__________________________________________________________________
  private String buscaDescripcionCentinela(String cod_centi) {
    String descripcion = "";
    // nombre del servlet
    final String servlet = "servlet/SrvQueryTool";
    Lista vFiltro = new Lista();
    Lista vReg = new Lista();

    QueryTool qt = new QueryTool();
    qt.putName("SIVE_PCENTINELA");
    qt.putType("DS_PCENTI", QueryTool.STRING);
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", cod_centi);
    qt.putOperator("CD_PCENTI", "=");
    // realiza la consulta al servlet
    try {

      this.getApp().getStub().setUrl(servlet);
      vFiltro.addElement(qt);
      vReg = (Lista)this.getApp().getStub().doPost(1, vFiltro);

      // muestra incidencia
      if (vReg.size() == 0) {
        this.getApp().showAdvise("Punto Centinela sin descripci�n");
      }

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    if (vReg.size() > 0) {
      Data dtTmp = new Data();
      dtTmp = (Data) vReg.elementAt(0);
      descripcion = dtTmp.getString("DS_PCENTI");
    }
    else {
      descripcion = "";
    }
    return descripcion;
  }

  //__________________________________________________________________

  private Lista obtenDatosLista(String cd_pcenti, String cd_medcen) {

    // nombre del servlet
    final String servlet = "servlet/SrvQueryTool";
    Lista vFiltro = new Lista();
    Lista vReg = new Lista();

    QueryTool qt = new QueryTool();
    qt.putName("SIVE_AVISOS");
    qt.putType("DS_OBSERV", QueryTool.STRING);
    qt.putType("FC_AVISO", QueryTool.DATE);
    qt.putType("CD_PLANTILLA", QueryTool.STRING);
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", cd_pcenti);
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", cd_medcen);
    qt.putOperator("CD_MEDCEN", "=");

    // realiza la consulta al servlet

    try {

      this.getApp().getStub().setUrl(servlet);
      vFiltro.addElement(qt);
      vReg = (Lista)this.getApp().getStub().doPost(1, vFiltro);

      // muestra incidencia
      if (vReg.size() == 0) {
        this.getApp().showAdvise("No se encontraron datos de avisos.");
      }

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    return vReg;
  }

  //__________________________________________________________________

  Lista buscarPuntos() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = "servlet/SrvQueryTool";

    Inicializar(CInicializar.ESPERA);

    //Hallamos los m�dicos asociados a ese pto:order by desc
    QueryTool2 qt = new QueryTool2();

    qt.putName("SIVE_MCENTINELA");
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("DS_NOMBRE", QueryTool.STRING);
    qt.putType("DS_APE1", QueryTool.STRING);
    qt.putType("DS_APE2", QueryTool.STRING);
    qt.putType("CD_E_NOTIF", QueryTool.STRING);
    qt.putType("CD_MASIST", QueryTool.STRING);
    qt.putType("DS_DNI", QueryTool.STRING);
    qt.putType("DS_TELEF", QueryTool.STRING);
    qt.putType("DS_FAX", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("CD_PCENTI", QueryTool.STRING);

    // El G�ear.
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "N");
    qt.putOperator("IT_BAJA", "=");

    // Cambio 21-05-01 (ARS) Que sea el m�dico seleccionado.
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));
    qt.putOperator("CD_MEDCEN", "=");

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_PCENTINELA");
    qtAdic.putType("DS_PCENTI", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_PCENTI", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);

    QueryTool qtAdic1 = new QueryTool();
    qtAdic1.putName("SIVE_E_NOTIF");
    qtAdic1.putType("CD_CENTRO", QueryTool.STRING);
    Data dtAdic1 = new Data();
    dtAdic1.put("CD_E_NOTIF", QueryTool.STRING);
    qt.addQueryTool(qtAdic1);
    qt.addColumnsQueryTool(dtAdic1);

    QueryTool qtAdic2 = new QueryTool();
    qtAdic2.putName("SIVE_C_NOTIF");
    qtAdic2.putType("DS_DIREC", QueryTool.STRING);
    qtAdic2.putType("DS_NUM", QueryTool.STRING);
    qtAdic2.putType("DS_PISO", QueryTool.STRING);
    qtAdic2.putType("CD_POSTAL", QueryTool.STRING);
    qtAdic2.putType("CD_MUN", QueryTool.STRING);
    qtAdic2.putType("CD_PROV", QueryTool.STRING);
    Data dtAdic2 = new Data();
    dtAdic2.put("CD_CENTRO", QueryTool.STRING);
    qt.addQueryTool(qtAdic2);
    qt.addColumnsQueryTool(dtAdic2);

    QueryTool qtAdic3 = new QueryTool();
    qtAdic3.putName("SIVE_MUNICIPIO");
    qtAdic3.putType("DS_MUN", QueryTool.STRING);
    Data dtAdic3 = new Data();
    dtAdic3.put("CD_PROV", QueryTool.STRING);
    dtAdic3.put("CD_MUN", QueryTool.STRING);
    qt.addQueryTool(qtAdic3);
    qt.addColumnsQueryTool(dtAdic3);

    QueryTool qtAdic4 = new QueryTool();
    qtAdic4.putName("SIVE_PROVINCIA");
    qtAdic4.putType("DS_PROV", QueryTool.STRING);
    Data dtAdic4 = new Data();
    dtAdic4.put("CD_PROV", QueryTool.STRING);
    qt.addQueryTool(qtAdic4);
    qt.addColumnsQueryTool(dtAdic4);

    qt.addOrderField("CD_PCENTI");

    p.addElement(qt);

    try {
      this.getApp().getStub().setUrl(servlet);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  //__________________________________________________________________
  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  public Data devuelveData() {
    return dtDevuelto;
  }

  void chkImprimir_itemStateChanged(ItemEvent e) {
    if (!chkImprimir.getState()) {
      pnlModelo.setEnabled(false);
      txtObservaciones.setEnabled(false);
      pnlBuscarFichero.setEnabled(false);
    }
    else {
      pnlModelo.setEnabled(true);
      txtObservaciones.setEnabled(true);
      pnlBuscarFichero.setEnabled(true);
    }
  }

  void crearDBF() {
    try {
      String localiz = this.pnlBuscarFichero.txtFile.getText();
      DBFCartasEtiq genDBF = new DBFCartasEtiq(lServ, localiz);
      //dbfCar= genDBFCar.getDBF();
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
  }

  private boolean datosValidos() {
    boolean resultado = true;
    String msg = "";
    if (chkImprimir.getState()) {
      if (pnlBuscarFichero.getNombre().equals("")) {
        resultado = false;
        msg = "Debe seleccionar un fichero";
      }
      if (pnlModelo.getDatos() == null) {
        resultado = false;
        msg = "Debe seleccionar un modelo";
      }
    }
    if (msg.length() > 0) {
      this.getApp().showError(msg);
    }
    return resultado;
  }

  void btnAceptar_actionPerformed(ActionEvent e) {

    Data dtDevuelto = new Data();
    if (datosValidos()) {
      if (chkImprimir.getState()) {
        try {
          Data dtParam = new Data();
          lServ = buscarPuntos();

          dtParam.put("CD_PLANTILLA", pnlModelo.getDatos().get("CD_PLANTILLA"));
          dtParam.put("DS_OBSERV", txtObservaciones.getText().trim());
          dtParam.put("LISTA_PUNTOS", lServ);
          dtParam.put("RUTA_FICH", this.pnlBuscarFichero.txtFile.getText());

          Lista lParam = new Lista();
          lParam.addElement(dtParam);

          crearDBF();

          this.getApp().getStub().setUrl("servlet/SrvCartasEtiq");
          Lista lResult = (Lista)this.getApp().getStub().doPost(
              servletCARTETIQ_AVISOS, lParam);

          this.getApp().showAdvise("Exportaci�n de fichero finalizada");

        }
        catch (Exception ex) {
          this.getApp().trazaLog(ex);
          this.getApp().showError(ex.getMessage());
        }
      } // Del clic de imprimir.
      bAceptar = true;
      dispose();
    } // Del datosValidos.
  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    bAceptar = false;
    dispose();
  }

}

class DiaMantCartasAvisos_chkImprimir_itemAdapter
    implements java.awt.event.ItemListener {
  DiaMantCartasAvisos adaptee;

  DiaMantCartasAvisos_chkImprimir_itemAdapter(DiaMantCartasAvisos adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkImprimir_itemStateChanged(e);
  }
}

class DiaMantCartasAvisos_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  DiaMantCartasAvisos adaptee;

  DiaMantCartasAvisos_btnAceptar_actionAdapter(DiaMantCartasAvisos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}

class DiaMantCartasAvisos_btnCancelar_actionAdapter
    implements java.awt.event.ActionListener {
  DiaMantCartasAvisos adaptee;

  DiaMantCartasAvisos_btnCancelar_actionAdapter(DiaMantCartasAvisos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnCancelar_actionPerformed(e);
  }
}
