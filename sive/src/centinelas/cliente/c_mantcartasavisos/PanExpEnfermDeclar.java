/*
 Panel  de enfermedades declaradas (casos num�ricos) por los puntos centinelas.
   Permite elegir per�odo del cual se extraer�n las enfermedades y ruta en la cual se
   grabar� el fichero generado. Tras recibir los datos del servidor, crea el fichero
   correspondiente en la ruta indicada.
 */
package centinelas.cliente.c_mantcartasavisos;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import centinelas.cliente.c_componentes.ContPanAnoSemFecha;
import centinelas.cliente.c_componentes.PanAnoSemFecha;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import sapp2.Data;
import sapp2.Lista;
import util_ficheros.DataCampos;
import util_ficheros.EditorFichero;

public class PanExpEnfermDeclar
    extends CPanel
    implements CInicializar, ContPanAnoSemFecha {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  XYLayout xyLayout = new XYLayout();

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

//constantes tama�o campos
  final public int tamCDENFCIE = 6;
  final public int tamCDPCENTI = 6;
  final public int tamCDMEDCEN = 6;
  final public int tamCDANOEPI = 4;
  final public int tamCDSEMEPI = 2;
  final public int tamNMCASOSDEC = 2;

  //componentes q conforman el panel
  Label lDesde = new Label();
  Label lHasta = new Label();
  PanAnoSemFecha panDesde = null;
  PanAnoSemFecha panHasta = null;
  ButtonControl btnAceptar = new ButtonControl();
  capp2.CFileName panFichero;

  //vector de DataCampos con nbCampo y su longitud
  Vector vCampos = new Vector();
  CApp apl;

  public PanExpEnfermDeclar(CApp a) {
    super(a);
    apl = a;
    try {
      /*Configuro el panel de a�o-sem-fecha de Desde y Hasta
             Si no se tiene un a�o seleccionado-> se inicializa a vacio
             Si se tiene un a�os seleccionado-> se inicializa con ese a�o*/
      if (app.getParametro("CD_ANO").equals("")) {
        panDesde = new PanAnoSemFecha(this, PanAnoSemFecha.modoVACIO, false);
      }
      else {
        panDesde = new PanAnoSemFecha(this, false, app.getParametro("CD_ANO"), false);

      }
      if (app.getParametro("CD_ANO").equals("")) {
        panHasta = new PanAnoSemFecha(this, PanAnoSemFecha.modoVACIO, false);
      }
      else {
        panHasta = new PanAnoSemFecha(this, false, app.getParametro("CD_ANO"), false);

      }
      panFichero = new capp2.CFileName(a);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  //rellena el vector de DataCampos con los nombres de los campos y su longitud
  void rellenarVector() {
    DataCampos dtCdPcenti = new DataCampos("CD_PCENTI", tamCDPCENTI);
    DataCampos dtCdMedcen = new DataCampos("CD_MEDCEN", tamCDMEDCEN);
    DataCampos dtCdAnoepi = new DataCampos("CD_ANOEPI", tamCDANOEPI);
    DataCampos dtCdSemepi = new DataCampos("CD_SEMEPI", tamCDSEMEPI);
    DataCampos dtCdEnf = new DataCampos("CD_ENFCIE", tamCDENFCIE);
    DataCampos dtNmCasos = new DataCampos("NM_CASOSDEC", tamNMCASOSDEC);
    vCampos.addElement(dtCdPcenti);
    vCampos.addElement(dtCdMedcen);
    vCampos.addElement(dtCdAnoepi);
    vCampos.addElement(dtCdSemepi);
    vCampos.addElement(dtCdEnf);
    vCampos.addElement(dtNmCasos);
  }

  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";

    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new
                                 PanExpEnfermDeclar_btnAceptar_actionAdapter(this));

    xyLayout.setHeight(220);
    xyLayout.setWidth(530);
    this.setLayout(xyLayout);

    lDesde.setText("Desde:");
    lHasta.setText("Hasta:");

    this.add(lDesde, new XYConstraints(MARGENIZQ, MARGENSUP, 110, ALTO));
    this.add(panDesde,
             new XYConstraints(MARGENIZQ + 110 + INTERHOR, MARGENSUP, 515, 34));
    this.add(lHasta,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 110,
                               ALTO));
    this.add(panHasta,
             new XYConstraints(MARGENIZQ + 110 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 515, 34));
    this.add(panFichero,
             new XYConstraints(MARGENIZQ - 13,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 515, 38));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 70 + 515 - 2 * 88 - 20,
                               MARGENSUP + 3 * ALTO + 4 * INTERVERT, 88, 29));

    //configuro el vector vCampos:DataCampos
    rellenarVector();
  } //end jbinit

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        panDesde.Inicializate(CInicializar.ESPERA);
        panHasta.Inicializate(CInicializar.ESPERA);
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        panDesde.Inicializate(CInicializar.NORMAL);
        panHasta.Inicializate(CInicializar.NORMAL);
        this.setEnabled(true);
    }
  }

  void btnAceptar_actionPerformed(ActionEvent e) {
//    final String servlet = "servlet/SrvExportaciones";
    final String servlet = nombreservlets.strSERVLET_EXPORTACIONES;
    Lista lResultado = null;
    Data dtDatos = new Data();
    Lista lDatos = new Lista();
    if (isDataValid()) {
      //1� llamar al servlet y mandar ejecutar la operacion->dev lista
      Inicializar(CInicializar.ESPERA);
      dtDatos = recogerDatos();
      lDatos.addElement(dtDatos);
      try {
        lResultado = BDatos.ejecutaSQL(true, this.getApp(), servlet, 1, lDatos);
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        ex.printStackTrace();
      }
      Inicializar(CInicializar.NORMAL);
      //mandar escribir el fichero
      String localiz = this.panFichero.txtFile.getText();
      EditorFichero edFic = new EditorFichero();
      edFic.escribirFichero(vCampos, apl, lResultado, localiz);

    } //end if isDataValid

  }

  Data recogerDatos() {
    Data dtRecoger = new Data();
    dtRecoger.put("CD_ANOEPIINI", panDesde.getCodAno());
    dtRecoger.put("CD_SEMEPIINI", panDesde.getCodSem());
    dtRecoger.put("CD_ANOEPIFIN", panHasta.getCodAno());
    dtRecoger.put("CD_SEMEPIFIN", panHasta.getCodSem());

    return dtRecoger;
  }

  //comprobar q se encuentran rellenos los campos
  boolean isDataValid() {
    if ( (panDesde.getCodAno().equals("")) ||
        (panDesde.getCodSem().equals("")) ||
        (panDesde.getFecSem().equals("")) ||
        (panHasta.getCodAno().equals("")) ||
        (panHasta.getCodSem().equals("")) ||
        (panHasta.getFecSem().equals("")) ||
        (panFichero.txtFile.getText().length() == 0)) {
      this.getApp().showAdvise(
          "Se deben rellenar todos los campos obligatoriamente");
      return false;
    }
    else {
      boolean b = comprobarRangoFechas();
      if (b) {
        return true;
      }
      else {
        return false;
      }
    }
  }

  //comprueba que la fecha l�mite hasta sea mayor que la de desde
  boolean comprobarRangoFechas() {
    boolean b;
    int sAnoDesde = Integer.parseInt(panDesde.getCodAno());
    int sSemDesde = Integer.parseInt(panDesde.getCodSem());
    int sAnoHasta = Integer.parseInt(panHasta.getCodAno());
    int sSemHasta = Integer.parseInt(panHasta.getCodSem());
    if (sAnoHasta > sAnoDesde) {
      b = true;
    }
    else {
      if (sAnoHasta < sAnoDesde) {
        this.getApp().showAdvise(
            "El a�o l�mite superior es menor que el inferior");
        b = false;
      }
      else {
        if (sSemHasta < sSemDesde) {
          this.getApp().showAdvise(
              "La semana l�mite superior es menor que la inferior para un mismo a�o");
          b = false;
        }
        else {
          b = true;
        }
      }
    }
    return b;
  }

//_____IMPLEMENTACION INTERFAZ CONTPANANOSEMFECHA________________________________________________________

  /* Para obtener el applet*/
  public CApp getMiCApp() {
    return this.app;
  };

  /* M�todos ser�n llamados desde el PanAnoSemFecha despu�s de que el propio PanAnoSemFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAno() {}

  public void alPerderFocoSemana() {}

  public void alPerderFocoFecha() {}

//_____________________________________________________________

}

class PanExpEnfermDeclar_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  PanExpEnfermDeclar adaptee;

  PanExpEnfermDeclar_btnAceptar_actionAdapter(PanExpEnfermDeclar adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}
