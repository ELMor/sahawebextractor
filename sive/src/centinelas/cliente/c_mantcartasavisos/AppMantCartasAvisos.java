package centinelas.cliente.c_mantcartasavisos;

import capp2.CApp;

public class AppMantCartasAvisos
    extends CApp {

  public AppMantCartasAvisos() {
    super();
  }

  public void init() {
    super.init();
    setTitulo("Mantenimiento de env�os de cartas de avisos");
    VerPanel("", new PanMantCartasAvisos(this));
  }
}