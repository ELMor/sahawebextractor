package centinelas.cliente.c_mantPC;

import java.applet.Applet;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import com.borland.jbcl.control.DecoratedFrame;
import com.borland.jbcl.layout.XYLayout;

//import javax.swing.UIManager;
public class Appletprueba
    extends Applet {
  XYLayout xYLayout1 = new XYLayout();
  boolean isStandalone = false;
//Get a parameter value

  public String getParameter(String key, String def) {
    return isStandalone ? System.getProperty(key, def) :
        (getParameter(key) != null ? getParameter(key) : def);
  }

  //Construct the applet

  public Appletprueba() {
  }

//Initialize the applet

  public void init() {
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //static {
  //  try {
  //    //UIManager.setLookAndFeel(new javax.swing.plaf.metal.MetalLookAndFeel());
  //    //UIManager.setLookAndFeel(new com.sun.java.swing.plaf.motif.MotifLookAndFeel());
  //    UIManager.setLookAndFeel(new com.sun.java.swing.plaf.windows.WindowsLookAndFeel());
  //  }
  //  catch (Exception e) {}
  //}
//Component initialization

  private void jbInit() throws Exception {
    xYLayout1.setWidth(400);
    xYLayout1.setHeight(300);
    this.setLayout(xYLayout1);
  }

//Get Applet information

  public String getAppletInfo() {
    return "Applet Information";
  }

//Get parameter info

  public String[][] getParameterInfo() {
    return null;
  }

//Main method

  public static void main(String[] args) {
    Appletprueba applet = new Appletprueba();
    applet.isStandalone = true;
    DecoratedFrame frame = new DecoratedFrame();
    frame.setTitle("Applet Frame");
    frame.add(applet, BorderLayout.CENTER);
    applet.init();
    applet.start();
    frame.setSize(400, 320);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    frame.setLocation( (d.width - frame.getSize().width) / 2,
                      (d.height - frame.getSize().height) / 2);
    frame.setVisible(true);
  }
}
