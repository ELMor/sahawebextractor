//Di�logo con los datos de un punto centinela

package centinelas.cliente.c_mantPC;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CMessage;
import capp2.CTexto;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.c_mantPC.PuntoCentExt;
import centinelas.datos.centbasicos.PuntoCent;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DiaMantpc
    extends CDialog
    implements CInicializar, CFiltro, ContCPnlCodigoExt {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // parametros pasados al di�logo
  public PuntoCentExt dtDev = null;

  //par�metros devueltos por el di�logo
  public PuntoCentExt dtDevuelto = new PuntoCentExt();

  // gesti�n salir/grabar
  public boolean bAceptar = false;
  //borrPto=0 (Delete); =1 (marca de baja); =2 (no eliminar)
  public int borrPto;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblPuntoCent = new Label();
  Label lblConglo = new Label();
  Label lblTipo = new Label();
  Label lblVigilaGrip = new Label();

  Checkbox chVigilaGrip = new Checkbox();

  CPnlCodigoExt pnlTipoPunt = null;
  CPnlCodigoExt pnlConglo = null;

  CCodigo txtCodPto = null;
  CTexto cDescPto = null;

  CListaMantenimiento clmMedNotif = null;

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  //constructor del di�logo DiaMantpc
  public DiaMantpc(CApp a, int modoop, PuntoCentExt dt) {
    super(a);
    modoOperacion = modoop;
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qtTipo = new QueryTool();
    QueryTool qtConglo = new QueryTool();

    dtDev = dt;
    // longitud de los campos de texto y �rea...
    txtCodPto = new CCodigo(6);
    cDescPto = new CTexto(40);
    try {
      //configurar el componente del pnlTipoPunt
      qtTipo.putName("SIVE_TPCENTINELA");
      qtTipo.putType("CD_TPCENTI", QueryTool.STRING);
      qtTipo.putType("DS_TPCENTI", QueryTool.STRING);

      //configura el componente de pnlConglomerado
      qtConglo.putName("SIVE_CONGLOM");
      qtConglo.putType("CD_CONGLO", QueryTool.STRING);
      qtConglo.putType("DS_CONGLO", QueryTool.STRING);

      // panel de tipo de punto centinela
      pnlTipoPunt = new CPnlCodigoExt(a,
                                      this,
                                      qtTipo,
                                      "CD_TPCENTI",
                                      "DS_TPCENTI",
                                      true,
                                      "Especialidades de puntos centinelas",
                                      "Especialidades de puntos centinelas",
                                      this);

      // panel de conglomerados
      pnlConglo = new CPnlCodigoExt(a,
                                    this,
                                    qtConglo,
                                    "CD_CONGLO",
                                    "DS_CONGLO",
                                    true,
                                    "Conglomerados",
                                    "Conglomerados",
                                    this);

      // configura el hist�rico de medicos notificadores de punto centinela
      // en este clm no existen botones
      // etiquetas
      vLabels.addElement(new CColumna("Baja",
                                      "45",
                                      "IT_BAJA"));

      vLabels.addElement(new CColumna("C�digo m�dico",
                                      "95",
                                      "CD_MEDCEN"));

      vLabels.addElement(new CColumna("Apellido",
                                      "175",
                                      "DS_APE1"));

      vLabels.addElement(new CColumna("Nombre",
                                      "175",
                                      "DS_NOMBRE"));

      vLabels.addElement(new CColumna("Fecha baja",
                                      "110",
                                      "FC_BAJA"));

      clmMedNotif = new CListaMantenimiento(a,
                                            vLabels,
                                            vBotones,
                                            this,
                                            this,
                                            250,
                                            640);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(680, 465);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    txtCodPto.setBackground(new Color(255, 255, 150));
    txtCodPto.setText("");
    cDescPto.setBackground(new Color(255, 255, 150));
    cDescPto.setText("");
    lblPuntoCent.setText("Punto Centinela:");
    lblConglo.setText("Conglomerado:");
    lblTipo.setText("Especialidad:");
    lblVigilaGrip.setText("Vigila Gripe:");
    clmMedNotif.setEnabled(true);

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(900);
    xyLayout1.setWidth(675);

    this.add(lblPuntoCent, new XYConstraints(MARGENIZQ, MARGENSUP, 90, ALTO));
    this.add(txtCodPto,
             new XYConstraints(MARGENIZQ + 90 + INTERHOR, MARGENSUP, 80, ALTO));
    this.add(cDescPto,
             new XYConstraints(MARGENIZQ + 90 + 2 * INTERHOR + 80, MARGENSUP,
                               218, ALTO));

    //localizaci�n de paneles
    this.add(lblConglo,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 85, -1));
    this.add(pnlConglo,
             new XYConstraints(MARGENIZQ + 90 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, TAMPANEL, 34));
    this.add(lblTipo,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERVERT,
                               75, -1));
    this.add(pnlTipoPunt,
             new XYConstraints(MARGENIZQ + 90 + INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, TAMPANEL,
                               34));
    //vigila gripe
    this.add(lblVigilaGrip,
             new XYConstraints(MARGENIZQ, MARGENSUP + 3 * ALTO + 3 * INTERVERT,
                               65, ALTO));
    this.add(chVigilaGrip,
             new XYConstraints(MARGENIZQ + 90 + INTERHOR,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT, 30, 34));

    this.add(clmMedNotif,
             new XYConstraints(MARGENIZQ - DESFPANEL,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT, 650, 225));
    this.add(btnAceptar,
             new XYConstraints(552 - 88 - INTERVERT,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT + 230, 88,
                               29));
    this.add(btnCancelar,
             new XYConstraints(552, MARGENSUP + 4 * ALTO + 4 * INTERVERT + 230,
                               88, 29));

    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    // ponemos el t�tulo
    switch (modoOperacion) {
      case modoALTA:
        setTitle("ALTA");
        break;
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  public PuntoCentExt recogerDatos() {
    Data rec = new Data();
    //en alta no se introduce el cd_pcenti:secuenciador
    rec.put("CD_PCENTI", txtCodPto.getText());
    rec.put("DS_PCENTI", cDescPto.getText());
    rec.put("CD_CONGLO", pnlConglo.getDatos().getString("CD_CONGLO"));
    rec.put("DS_CONGLO", pnlConglo.getDatos().getString("DS_CONGLO"));
    rec.put("CD_TPCENTI", pnlTipoPunt.getDatos().getString("CD_TPCENTI"));
    rec.put("DS_TPCENTI", pnlTipoPunt.getDatos().getString("DS_TPCENTI"));
    if (chVigilaGrip.getState()) {
      rec.put("IT_GRIPE", "S");
    }
    else {
      rec.put("IT_GRIPE", "N");
    }
    rec.put("CD_OPE", this.getApp().getParametro("COD_USUARIO"));
    rec.put("FC_ULTACT", "12/12/1999");
    rec.put("IT_BAJA", dtDev.getString("IT_BAJA"));

    //creo el objeto PuntoCentExt
    PuntoCentExt pce = new PuntoCentExt(rec);
    return pce;
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    Inicializar(CInicializar.ESPERA);

    //Hallamos los m�dicos asociados a ese pto:order by desc
    QueryTool qtMed = new QueryTool();

    qtMed.putName("SIVE_MCENTINELA");
    qtMed.putType("CD_PCENTI", QueryTool.STRING);
    qtMed.putType("IT_BAJA", QueryTool.STRING);
    qtMed.putType("CD_MEDCEN", QueryTool.STRING);
    qtMed.putType("DS_APE1", QueryTool.STRING);
    qtMed.putType("DS_APE2", QueryTool.STRING);
    qtMed.putType("DS_NOMBRE", QueryTool.STRING);
    qtMed.putType("FC_BAJA", QueryTool.DATE);

    qtMed.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtMed.putWhereValue("CD_PCENTI", dtDev.getString("CD_PCENTI"));
    qtMed.putOperator("CD_PCENTI", "=");

    qtMed.addOrderField("FC_BAJA desc");

    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtMed);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      borrPto = 0;
      if (p1.size() == 0) {
        //Se elimina fisicamente por no tener regs a su cargo de m�dicos
        borrPto = 0;
      }
      else {
        for (int i = 0; i < p1.size(); i++) {
          if ( ( (Data) p1.elementAt(i)).getString("IT_BAJA").equals("N")) {
            ( (Data) p1.elementAt(i)).put("FC_BAJA", "");
            borrPto = 2;
          }
        }
        if (borrPto == 0) {
          borrPto = 1;
        }
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    try {
      //Realizamos una query para ver m�dicos asociados a ese pto
      QueryTool qtMed = new QueryTool();

      qtMed.putName("SIVE_MCENTINELA");
      qtMed.putType("CD_PCENTI", QueryTool.STRING);
      qtMed.putType("IT_BAJA", QueryTool.STRING);
      qtMed.putType("CD_MEDCEN", QueryTool.STRING);
      qtMed.putType("DS_APE1", QueryTool.STRING);
      qtMed.putType("DS_APE2", QueryTool.STRING);
      qtMed.putType("DS_NOMBRE", QueryTool.STRING);
      qtMed.putType("FC_BAJA", QueryTool.DATE);

      qtMed.putWhereType("CD_PCENTI", QueryTool.STRING);
      qtMed.putWhereValue("CD_PCENTI", dtDev.getString("CD_PCENTI"));
      qtMed.putOperator("CD_PCENTI", "=");

      qtMed.addOrderField("FC_BAJA desc");

      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtMed);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);

      if (p1.size() == 0) {
        this.getApp().showAdvise(
            "Este punto centinela no posee m�dicos notificadores");
      }
      else {
        for (int i = 0; i < p1.size(); i++) {
          if ( ( (Data) p1.elementAt(i)).getString("IT_BAJA").equals("N")) {
            ( (Data) p1.elementAt(i)).put("FC_BAJA", "");
          }
        }
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public void rellenarDatos() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //Introducimos los datos en la pantalla
    txtCodPto.setText(dtDev.getString("CD_PCENTI"));
    cDescPto.setText(dtDev.getString("DS_PCENTI"));
    pnlConglo.setCodigo(dtDev);
    pnlTipoPunt.setCodigo(dtDev);
    if (dtDev.getString("IT_GRIPE").equals("S")) {
      chVigilaGrip.setState(true);
    }
    else {
      chVigilaGrip.setState(false);
    }
    //rellenamos la lista de m�dicos notificadores
    Inicializar(CInicializar.ESPERA);
    clmMedNotif.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  // Pone los distintos campos en el estado que les corresponda
  // segun el modo.
  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        txtCodPto.setVisible(false);
        cDescPto.setEnabled(true);
        break;

      case modoMODIFICACION:
        txtCodPto.setVisible(true);
        txtCodPto.setEnabled(false);
        //Se rellenan los datos en la pantalla
        rellenarDatos();
        break;

      case modoBAJA:

        //solo habilitados los botones de aceptar y cancelar
        txtCodPto.setVisible(true);
        chVigilaGrip.setEnabled(false);
        txtCodPto.setEnabled(false);
        cDescPto.setEnabled(false);
        pnlTipoPunt.setEnabled(false);
        pnlConglo.setEnabled(false);
        rellenarDatos();
        break;
    }
  }

  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return ("");
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            break;

          case modoMODIFICACION:

            //lo �nico q no se puede modificar es el c�d, si la descr
            txtCodPto.setEnabled(false);
            cDescPto.setEnabled(true);
            pnlConglo.setEnabled(true);
            pnlTipoPunt.setEnabled(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            chVigilaGrip.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  //Obligatorio rellenar todos los campos
  private boolean isDataValid() {
    if ( (pnlTipoPunt.getDatos() == null) || (pnlConglo.getDatos() == null)
        || (cDescPto.getText().trim().length() == 0)) {
      this.getApp().showAdvise(
          "Debe completar todos los campos obligatoriamente");
      return false;
    }
    else {
      return true;
    }
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();
//   final String servlet1="servlet/SrvMantPCentinela";
    final String servlet1 = nombreservlets.strSERVLET_MANT_P_CENTINELA;

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          // ----- ALTA -----------

          case 0:

            //Inicio el alta
            Inicializar(CInicializar.ESPERA);
            try {
              //Relleno los par�metros
              PuntoCentExt altaPc = new PuntoCentExt();
              altaPc = recogerDatos();
              altaPc.put("FC_ALTA", Format.fechaActual());

              // realiza la consulta al servlet
              this.getApp().getStub().setUrl(servlet1);
              vFiltro.addElement(altaPc);
              vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet1, 3,
                                         vFiltro);
              dtDevuelto = altaPc;
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;

          case 1:

            // ---- MODIFICAR ----
            Inicializar(CInicializar.ESPERA);
            try {
              PuntoCentExt modifPc = new PuntoCentExt();
              modifPc = recogerDatos();
              modifPc.put("FC_ALTA", dtDev.getString("FC_ALTA"));
              QueryTool2 qtModif = new QueryTool2();
              qtModif = PuntoCent.getUpdateOnePuntoCent(modifPc);
              vFiltro.addElement(qtModif);
              vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 4,
                                         vFiltro);
              dtDevuelto = modifPc;
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;
          case 2:

            // ---- BORRAR -------
            //3casos
            CMessage msgBox = new CMessage(app, CMessage.msgPREGUNTA,
                "\u00BFEst\u00E1 seguro de que desea eliminar este registro?");
            msgBox.show();

            // el usuario confirma la operaci�n
            if (msgBox.getResponse()) {

              msgBox = null;

              Inicializar(CInicializar.ESPERA);
              try {
                PuntoCentExt bajaPc = new PuntoCentExt();
                bajaPc = recogerDatos();
                //No tiene m�dicos asociados: Delete
                QueryTool2 qtBaja = new QueryTool2();
                if (dtDev.getString("IT_BAJA").equals("S")) {
                  this.getApp().showAdvise("Elemento ya dado de baja");
                  bAceptar = false;
                }
                else {
                  if (borrPto == 0) {
                    qtBaja = PuntoCent.getDeleteOnePuntoCent(bajaPc);
                    vFiltro.addElement(qtBaja);
                    vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 5,
                                               vFiltro);
                    bAceptar = true;
                  }
                  //Tiene m�dicos asociados pero todos de baja: por marca (it_baja)
                  if (borrPto == 1) {
                    bajaPc.put("IT_BAJA", "S");
                    bajaPc.put("FC_ALTA", dtDev.getString("FC_ALTA"));
                    qtBaja = PuntoCent.getUpdateOnePuntoCent(bajaPc);
                    vFiltro.addElement(qtBaja);
                    vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 4,
                                               vFiltro);
                    dtDevuelto = bajaPc;
                    bAceptar = true;
                  }
                  //Tiene m�dicos asociados y alguno no est� dado de baja: no modificar
                  if (borrPto == 2) {
                    this.getApp().showAdvise(
                        "No se puede realizar la baja. Posee m�dico dado de alta");
                    bAceptar = false;
                  }
                }
                dispose();
                // error en el servlet
              }
              catch (Exception ex) {
                this.getApp().trazaLog(ex);
                this.getApp().showError(ex.getMessage());
                bAceptar = false;
                dispose();
              }
              Inicializar(CInicializar.NORMAL);
            }
            msgBox = null;
            break;
        } //end if switch
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  //Devuelve si el borrado se hace f�sico, por marca o no se hace
  public int borrPto() {
    return borrPto;
  }

  public PuntoCentExt devuelveData() {
    return dtDevuelto;
  }

  public void realizaOperacion(int j) {}

}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantpc adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMantpc adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
