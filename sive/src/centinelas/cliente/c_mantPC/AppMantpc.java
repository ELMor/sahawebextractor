//Applet de mantenimiento de puntos centinelas

package centinelas.cliente.c_mantPC;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantpc
    extends CApp
    implements CInicializar {

  PanMantpc pan = null;
  public AppMantpc() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento de puntos centinelas");
    pan = new PanMantpc(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }

}
