package centinelas.cliente.c_creaciondbf;

import java.io.File;

import sapp2.Data;
import sapp2.Lista;
import xBaseJ.CharField;
import xBaseJ.DBF;
import xBaseJ.Field;

public class DBFCartasEtiq {

  //DBF que se devuelve al SrvCartasEtiq
  DBF DBDev = null;

  //Crea un directorio para DBFs (si no existe ya ) y en �l crea un fichero DBF
  // con nombre prefijado
  public DBFCartasEtiq(Lista vParam) throws Exception {

    String dirDBF = "dbfs";
    String rutaFich = "";

    //Crea dir
    String pathDefecto = System.getProperty("user.dir");
    System.out.println(pathDefecto);
    File miDir = new File(pathDefecto + File.separator + dirDBF);
    miDir.mkdir();

    //Creo un nuevo fichero DBF con nombre ? pasado desde cliente
    rutaFich = pathDefecto + File.separator + dirDBF + File.separator +
        "volcadoBd.dbf";
    creaDBF(vParam, rutaFich);

  }

  //Crea un fichero DBF en la ruta indicada por rutaFich
  //(rutaFich incluye directorios(ya existentes) y nombre fichero DBF que se crear�)
  public DBFCartasEtiq(Lista vParam, String rutaFich) throws Exception {

    creaDBF(vParam, rutaFich);
  }

  public void creaDBF(Lista vParam, String rutaFich) throws Exception {
    try {

      //Creo un nuevo fichero DBF en ruta rutaFich
//            DBF DBEtiq= new DBF(rutaFich,true);
      DBF DBEtiq = new DBF(rutaFich, xBaseJ.DBF.DBASEIII, true);

      //Creo los campos que va a contener el fichero DBF anterior
      //y los introduzco en un array
      Field[] nuevField = new Field[21];
      CharField cdMedCen = new CharField("CD_MEDCEN", 6);
      CharField cdPCenti = new CharField("CD_PCENTI", 6);
      CharField dsPCenti = new CharField("DS_PCENTI", 40);
      CharField dsApe1 = new CharField("DS_APE1", 20);
      CharField dsApe2 = new CharField("DS_APE2", 20);
      CharField dsNombre = new CharField("DS_NOMBRE", 20);
      CharField dsDirec = new CharField("DS_DIREC", 40);
      CharField dsNum = new CharField("DS_NUM", 5);
      CharField dsPiso = new CharField("DS_PISO", 5);
      CharField cdPostal = new CharField("CD_POSTAL", 5);
      CharField cdMun = new CharField("CD_MUN", 3);
      CharField dsMun = new CharField("DS_MUN", 50);
      CharField cdProv = new CharField("CD_PROV", 2);
      CharField dsProv = new CharField("DS_PROV", 20);
      CharField dsDni = new CharField("DS_DNI", 10);
      CharField dsTelef = new CharField("DS_TELEF", 14);
      CharField dsFax = new CharField("DS_FAX", 9);
      CharField itBaja = new CharField("IT_BAJA", 1);
      CharField cdENotif = new CharField("CD_E_NOTIF", 6);
      CharField cdMasist = new CharField("CD_MASIST", 1);
      CharField cdCentro = new CharField("CD_CENTRO", 6);

      nuevField[0] = cdMedCen;
      nuevField[1] = cdPCenti;
      nuevField[2] = dsPCenti;
      nuevField[3] = dsApe1;
      nuevField[4] = dsApe2;
      nuevField[5] = dsNombre;
      nuevField[6] = dsDirec;
      nuevField[7] = dsNum;
      nuevField[8] = dsPiso;
      nuevField[9] = cdPostal;
      nuevField[10] = cdMun;
      nuevField[11] = dsMun;
      nuevField[12] = cdProv;
      nuevField[13] = dsProv;
      nuevField[14] = dsDni;
      nuevField[15] = dsTelef;
      nuevField[16] = dsFax;
      nuevField[17] = itBaja;
      nuevField[18] = cdENotif;
      nuevField[19] = cdMasist;
      nuevField[20] = cdCentro;

      //introduzco el array de campos en el fic DBF
      DBEtiq.addField(nuevField);

      //introduzco los datos de la lista que paso como
      //par�metro del constructor dentro de cada campo, conform�ndose
      //as� un registro, y luego lo escribo en el DBD
      for (int i = 0; i < vParam.size(); i++) {
        nuevField[0].put( ( (Data) vParam.elementAt(i)).getString("CD_MEDCEN"));
        nuevField[1].put( ( (Data) vParam.elementAt(i)).getString("CD_PCENTI"));
        nuevField[2].put( ( (Data) vParam.elementAt(i)).getString("DS_PCENTI"));
        nuevField[3].put( ( (Data) vParam.elementAt(i)).getString("DS_APE1"));
        nuevField[4].put( ( (Data) vParam.elementAt(i)).getString("DS_APE2"));
        nuevField[5].put( ( (Data) vParam.elementAt(i)).getString("DS_NOMBRE"));
        nuevField[6].put( ( (Data) vParam.elementAt(i)).getString("DS_DIREC"));
        nuevField[7].put( ( (Data) vParam.elementAt(i)).getString("DS_NUM"));
        nuevField[8].put( ( (Data) vParam.elementAt(i)).getString("DS_PISO"));
        nuevField[9].put( ( (Data) vParam.elementAt(i)).getString("CD_POSTAL"));
        nuevField[10].put( ( (Data) vParam.elementAt(i)).getString("CD_MUN"));
        nuevField[11].put( ( (Data) vParam.elementAt(i)).getString("DS_MUN"));
        nuevField[12].put( ( (Data) vParam.elementAt(i)).getString("CD_PROV"));
        nuevField[13].put( ( (Data) vParam.elementAt(i)).getString("DS_PROV"));
        nuevField[14].put( ( (Data) vParam.elementAt(i)).getString("DS_DNI"));
        nuevField[15].put( ( (Data) vParam.elementAt(i)).getString("DS_TELEF"));
        nuevField[16].put( ( (Data) vParam.elementAt(i)).getString("DS_FAX"));
        nuevField[17].put( ( (Data) vParam.elementAt(i)).getString("IT_BAJA"));
        nuevField[18].put( ( (Data) vParam.elementAt(i)).getString("CD_E_NOTIF"));
        nuevField[19].put( ( (Data) vParam.elementAt(i)).getString("CD_MASIST"));
        nuevField[20].put( ( (Data) vParam.elementAt(i)).getString("CD_CENTRO"));

        DBEtiq.write();
      } //end for

      DBDev = DBEtiq;
      DBEtiq.close();
      DBEtiq = null;
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      throw e;
    }
  }

  public DBF getDBF() {
    System.out.println("SALIDA DEL DBF" + DBDev);
    return DBDev;
  }
}
