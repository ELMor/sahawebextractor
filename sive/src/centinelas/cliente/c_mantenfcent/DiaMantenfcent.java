//Di�logo que representa los datos de una enfermedad centinela

package centinelas.cliente.c_mantenfcent;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CMessage;
import capp2.CPnlCodigo;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaMantenfcent
    extends CDialog
    implements CInicializar {
  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // parametros pasados al di�logo
  public Data dtDev = null;

  //par�metros devueltos por el di�logo
  public Data dtDevuelto = new Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;
  public boolean bBorrLista = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblEnf = new Label();
  Label lblVigRut = new Label();
  Label lblSemIni = new Label();
  Label lblSemFin = new Label();
  Label lblNotifCNE = new Label();

  CPnlCodigo pnlMantEnfCent = null;

  Checkbox chVigRut = new Checkbox();
  Checkbox chNotCNE = new Checkbox();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  CEntero txtSemIni = new CEntero(2);
  CEntero txtSemFin = new CEntero(2);
  GroupBox gbSemVig = new GroupBox();

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);
  PopupMenu popupMenu1 = new PopupMenu();

  public DiaMantenfcent(CApp a, int modoop, Data dt) {
    super(a);
    modoOperacion = modoop;
    QueryTool qtEnfCent = new QueryTool();
    dtDev = dt;

    try {
      //configurar el componente del pnlMantEnfCent
      qtEnfCent.putName("SIVE_PROCESOS");
      qtEnfCent.putType("CD_ENFCIE", QueryTool.STRING);
      qtEnfCent.putType("DS_PROCESO", QueryTool.STRING);

      // panel de mant de enfermedades, obligatorio para altas
      pnlMantEnfCent = new CPnlCodigo(a,
                                      this,
                                      qtEnfCent,
                                      "CD_ENFCIE",
                                      "DS_PROCESO",
                                      true,
                                      "Enfermedades",
                                      "Enfermedades");
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(480, 280);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    txtSemIni.setText("");
    txtSemFin.setText("");
    lblEnf.setText("Procesos:");
    lblSemIni.setText("Semana inicio:");
    lblSemFin.setText("Semana fin");
    gbSemVig.setLabel("Periodo vigilancia rutinaria");
    lblVigRut.setText("Vigilancia rutinaria:");
    lblNotifCNE.setText("�Se notifica al CNE?:");
    pnlMantEnfCent.addMouseListener(new
                                    DiaMantenfcent_pnlMantEnfCent_mouseAdapter(this));
    chVigRut.addItemListener(new DiaMantenfcent_chVigRut_itemAdapter(this));

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(900);
    xyLayout1.setWidth(675);

    this.add(lblEnf, new XYConstraints(MARGENIZQ, MARGENSUP, 80, ALTO));
    this.add(pnlMantEnfCent,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR - DESFPANEL,
                               MARGENSUP, TAMPANEL, 34));
    this.add(lblVigRut,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + 2 * INTERVERT, 105,
                               ALTO));
    this.add(chVigRut,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR,
                               MARGENSUP + ALTO + 2 * INTERVERT, 30, 34));

    this.add(lblSemIni,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 5 -
                               45 - INTERHOR - 78 - 10 - 8,
                               MARGENSUP + ALTO + 3 * INTERVERT + 10, 78, ALTO));
    this.add(txtSemIni,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 5 -
                               45 - 10 - 5,
                               MARGENSUP + ALTO + 3 * INTERVERT + 10, 45, ALTO));
    this.add(lblSemFin,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 5 -
                               45 - INTERHOR - 78 - 10 - 8,
                               MARGENSUP + 2 * ALTO + 4 * INTERVERT + 10, 78,
                               ALTO));
    this.add(txtSemFin,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 5 -
                               45 - 10 - 5,
                               MARGENSUP + 2 * ALTO + 4 * INTERVERT + 10, 45,
                               ALTO));

    this.add(lblNotifCNE,
             new XYConstraints(MARGENIZQ, MARGENSUP + 3 * ALTO + 7 * INTERVERT,
                               120, ALTO));
    this.add(chNotCNE,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR,
                               MARGENSUP + 3 * ALTO + 7 * INTERVERT, 30, 34));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 15 -
                               45 - INTERHOR - 78 - 15 + 15 + 78 + 45 + 15 -
                               2 * 88 - 3, MARGENSUP + 4 * ALTO + 9 * INTERVERT,
                               88, 29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 15 -
                               45 - INTERHOR - 78 - 15 + 15 + 78 + INTERHOR +
                               45 + 15 - 88 - 3,
                               MARGENSUP + 4 * ALTO + 9 * INTERVERT, 88, 29));

    this.add(gbSemVig,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 15 -
                               45 - INTERHOR - 98 - 5 - 3,
                               MARGENSUP + ALTO + 2 * INTERVERT,
                               15 + 78 + INTERHOR + 45 + 30,
                               15 + 2 * ALTO + INTERVERT + 20));

    /*    this.add(lblEnf, new XYConstraints(39, CPnlCodigoExt.LONG_ETI/2 + 4, 80, -1));
        this.add(pnlMantEnfCent, new XYConstraints(CPnlCodigoExt.LONG_SEP*15+CPnlCodigoExt.LONG_ETI, CPnlCodigoExt.LONG_ETI/2, 651, 34));
        this.add(lblVigRut, new XYConstraints(39, CPnlCodigoExt.LONG_ETI/2+ 74, 115, -1));
        this.add(chVigRut, new XYConstraints(CPnlCodigoExt.LONG_SEP*14+CPnlCodigoExt.LONG_ETI, CPnlCodigoExt.LONG_ETI/2 + 70, 30, 34));
        this.add(lblSemIni, new XYConstraints(300, CPnlCodigoExt.LONG_ETI/2 + 64, 78, 34));
        this.add(txtSemIni, new XYConstraints(395, CPnlCodigoExt.LONG_ETI/2 + 70, 45, 22));
        this.add(lblSemFin, new XYConstraints(300, CPnlCodigoExt.LONG_ETI/2 + 104, 75, 34));
        this.add(txtSemFin, new XYConstraints(395, CPnlCodigoExt.LONG_ETI/2 + 110, 45, 22));
        this.add(lblNotifCNE,new XYConstraints(39,CPnlCodigoExt.LONG_ETI/2 + 184 ,120,-1));
        this.add(chNotCNE, new XYConstraints(CPnlCodigoExt.LONG_SEP*14+CPnlCodigoExt.LONG_ETI, CPnlCodigoExt.LONG_ETI/2+180, 30, 34));
        this.add(btnAceptar, new XYConstraints(300, CPnlCodigoExt.LONG_ETI/2 + 220, 88, 29));
        this.add(btnCancelar, new XYConstraints(400, CPnlCodigoExt.LONG_ETI/2 + 220, 88, 29));*/

    verDatos();

    // ponemos el t�tulo
    switch (modoOperacion) {
      case modoALTA:
        setTitle("ALTA");
        break;
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            if (chVigRut.getState() == true) {
              txtSemIni.setEnabled(true);
              txtSemFin.setEnabled(true);
            }
            else {
              txtSemIni.setEnabled(false);
              txtSemFin.setEnabled(false);
            }
            break;

          case modoMODIFICACION:
            chVigRut.setEnabled(true);
            if (chVigRut.getState() == true) {
              txtSemIni.setEnabled(true);
              txtSemFin.setEnabled(true);
            }
            else {
              txtSemIni.setEnabled(false);
              txtSemFin.setEnabled(false);
            }
            chNotCNE.setEnabled(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  public Data recogerDatos() {
    Data rec = new Data();
    rec.put("CD_ENFCIE", pnlMantEnfCent.getDatos().getString("CD_ENFCIE"));
    if (chNotCNE.getState()) {
      rec.put("IT_ENVCNE", "S");
    }
    else {
      rec.put("IT_ENVCNE", "N");
    }
    if (chVigRut.getState()) {
      rec.put("IT_RUTINA", "S");
    }
    else {
      rec.put("IT_RUTINA", "N");
    }
    rec.put("CD_SEMINIP", txtSemIni.getText());
    rec.put("CD_SEMFINP", txtSemFin.getText());
    rec.put("CD_OPE", this.getApp().getParametro("COD_USUARIO"));
    rec.put("FC_ULTACT", "12/12/1999");

    return rec;
  }

  public void rellenarDatos(int modoop) {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //Introducimos los datos en la pantalla
    pnlMantEnfCent.setCodigo(dtDev);
    if ( (dtDev.getString("IT_RUTINA").equals("S")) &&
        (modoop == modoMODIFICACION)) {
      chVigRut.setState(true);
      lblSemIni.setEnabled(true);
      lblSemFin.setEnabled(true);
      txtSemIni.setEnabled(true);
      txtSemFin.setEnabled(true);
      txtSemIni.setText(dtDev.getString("CD_SEMINIP"));
      txtSemFin.setText(dtDev.getString("CD_SEMFINP"));
    }
    else {
      if (modoop == modoBAJA) {
        txtSemIni.setText(dtDev.getString("CD_SEMINIP"));
        txtSemFin.setText(dtDev.getString("CD_SEMFINP"));
      }
      chVigRut.setState(false);
      txtSemIni.setEnabled(false);
      txtSemFin.setEnabled(false);
    }
    if ( (dtDev.getString("IT_RUTINA").equals("S")) && (modoop == modoBAJA)) {
      chVigRut.setState(true);
    }
    if (dtDev.getString("IT_ENVCNE").equals("S")) {
      chNotCNE.setState(true);
    }
    else {
      chNotCNE.setState(false);
    }
  }

  //Prepara la pantalla para el modo de operaci�n a realizar
  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        txtSemIni.setEnabled(false);
        txtSemFin.setEnabled(false);
        break;

      case modoMODIFICACION:
        pnlMantEnfCent.setEnabled(false);
        //Se rellenan los datos en la pantalla
        rellenarDatos(modoMODIFICACION);
        break;

      case modoBAJA:

        //solo habilitados los botones de aceptar y cancelar
        chVigRut.setEnabled(false);
        chNotCNE.setEnabled(false);
        pnlMantEnfCent.setEnabled(false);
        rellenarDatos(modoBAJA);
        txtSemFin.setEnabled(false);
        break;
    }
  }

  //Obligatorio rellenar todos los campos correctamente
  private boolean isDataValid() {
    boolean b = false;
    boolean b1 = false;
    boolean b2 = false;
    boolean blanco1 = false;
    boolean blanco2 = false;
    if ( (pnlMantEnfCent.getDatos() == null)) {
      this.getApp().showAdvise("Debe seleccionar la enfermedad deseada");
      b = false;
    }
    else {
      if (chVigRut.getState() == true) {
        if ( (txtSemIni.getText().equals("")) && (txtSemFin.getText().equals(""))) {
          if (modoOperacion != 2) {
            this.getApp().showAdvise(
                "La vigilancia se realiza durante todo el a�o");
          }
          b = true;
        }
        else {
          if (txtSemIni.getText().equals("")) {
            b1 = true;
            blanco1 = true;
          }
          else {
            int semini = Integer.parseInt(txtSemIni.getText().trim());
            if ( (semini < 1) || (semini > 53)) {
              this.getApp().showAdvise(
                  "C�digo de semana de inicio incorrecto. Rango entre 1-53");
              b1 = false;
            }
            else {
              b1 = true;
            } //fin else2
          } //fin else

          if (txtSemFin.getText().equals("")) {
            b2 = true;
            blanco2 = true;
          }
          else {
            int semfin = Integer.parseInt(txtSemFin.getText().trim());
            if ( (semfin < 1) || (semfin > 53)) {
              this.getApp().showAdvise(
                  "C�digo de semana de fin incorrecto. Rango entre 1-53");
              b2 = false;
            }
            else {
              b2 = true;
            } //fin else2
          } //fin else
          if (b1 && b2) {
            b = true;
          }
          else {
            b = false;
          }
          if ( (blanco1 && !blanco2) || (!blanco1 && blanco2)) {
            b = false;
            this.getApp().showAdvise("Rango incompleto");
          }

        } //fin else1
      }
      else {
        b = true;
      } //fin ifch
    } //fin else
    return b;
  }

  private int buscarNotificaciones() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista p1 = new Lista();
    int i = 0;

    try {
      Inicializar(CInicializar.ESPERA);
      QueryTool qtNotif = new QueryTool();
      qtNotif.putName("SIVE_NOTIF_RMC");
      qtNotif.putType("CD_ENFCIE", QueryTool.STRING);

      qtNotif.putWhereType("CD_ENFCIE", QueryTool.STRING);
      qtNotif.putWhereValue("CD_ENFCIE",
                            pnlMantEnfCent.getDatos().getString("CD_ENFCIE"));
      qtNotif.putOperator("CD_ENFCIE", "=");

      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtNotif);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      i = p1.size();
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
      bAceptar = false;
      dispose();
    }
    return i;
  }

  void consultaTama�os() {
    String cdEnf = pnlMantEnfCent.getDatos().getString("CD_ENFCIE");
    String semini = txtSemIni.getText().trim();
    String semfin = txtSemFin.getText().trim();
    String ope = this.getApp().getParametro("COD_USUARIO");
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          // ----- ALTA -----------

          case 0:

            //Inicio el alta
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qtAlta = new QueryTool();
              consultaTama�os();
              qtAlta.putName("SIVE_ENF_CENTI");
              qtAlta.putType("CD_ENFCIE", QueryTool.STRING);
              qtAlta.putValue("CD_ENFCIE",
                              pnlMantEnfCent.getDatos().getString("CD_ENFCIE"));
              qtAlta.putType("IT_ENVCNE", QueryTool.STRING);
              if (chNotCNE.getState()) {
                qtAlta.putValue("IT_ENVCNE", "S");
              }
              else {
                qtAlta.putValue("IT_ENVCNE", "N");
              }
              qtAlta.putType("IT_RUTINA", QueryTool.STRING);
              if (chVigRut.getState()) {
                qtAlta.putValue("IT_RUTINA", "S");
              }
              else {
                qtAlta.putValue("IT_RUTINA", "N");
              }
              qtAlta.putType("CD_SEMINIP", QueryTool.STRING);
              qtAlta.putValue("CD_SEMINIP", txtSemIni.getText().trim());
              qtAlta.putType("CD_SEMFINP", QueryTool.STRING);
              qtAlta.putValue("CD_SEMFINP", txtSemFin.getText().trim());
              qtAlta.putType("CD_OPE", QueryTool.STRING);
              qtAlta.putValue("CD_OPE",
                              this.getApp().getParametro("COD_USUARIO"));
              qtAlta.putType("FC_ULTACT", QueryTool.TIMESTAMP);
              qtAlta.putValue("FC_ULTACT", "12/12/1999");
              qtAlta.putType("FC_ALTA", QueryTool.DATE);
              qtAlta.putValue("FC_ALTA", Format.fechaActual());
              qtAlta.putType("FC_BAJA", QueryTool.DATE);
              qtAlta.putValue("FC_BAJA", dtDev.getString("FC_BAJA"));

              // realiza la consulta al servlet
              this.getApp().getStub().setUrl(servlet);
              vFiltro.addElement(qtAlta);
              vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 3,
                                         vFiltro);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;

          case 1:

            // ---- MODIFICAR ----
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qtModif = new QueryTool();
              qtModif.putName("SIVE_ENF_CENTI");
              qtModif.putType("CD_ENFCIE", QueryTool.STRING);
              qtModif.putValue("CD_ENFCIE", dtDev.getString("CD_ENFCIE"));
              qtModif.putType("IT_ENVCNE", QueryTool.STRING);
              if (chNotCNE.getState()) {
                qtModif.putValue("IT_ENVCNE", "S");
              }
              else {
                qtModif.putValue("IT_ENVCNE", "N");
              }
              qtModif.putType("IT_RUTINA", QueryTool.STRING);
              if (chVigRut.getState()) {
                qtModif.putValue("IT_RUTINA", "S");
                qtModif.putType("CD_SEMINIP", QueryTool.STRING);
                qtModif.putValue("CD_SEMINIP", txtSemIni.getText().trim());
                qtModif.putType("CD_SEMFINP", QueryTool.STRING);
                qtModif.putValue("CD_SEMFINP", txtSemFin.getText().trim());
              }
              else {
                qtModif.putValue("IT_RUTINA", "N");
                qtModif.putType("CD_SEMINIP", QueryTool.STRING);
                qtModif.putValue("CD_SEMINIP", "");
                qtModif.putType("CD_SEMFINP", QueryTool.STRING);
                qtModif.putValue("CD_SEMFINP", "");
              }

              qtModif.putWhereType("CD_ENFCIE", QueryTool.STRING);
              qtModif.putWhereValue("CD_ENFCIE", dtDev.getString("CD_ENFCIE"));
              qtModif.putOperator("CD_ENFCIE", "=");

              // realiza la consulta al servlet
              this.getApp().getStub().setUrl(servlet);
              vFiltro.addElement(qtModif);
              vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 4,
                                         vFiltro);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;

          case 2:

            /**Busco en la tabla sive_notif_rmc:
              si existen notificaciones-> la baja es delete
              si no existen notificaciones-> la baja es por marca:
                                            fc_baja la del sistema**/
            // ---- BORRAR -------
            CMessage msgBox = new CMessage(app, CMessage.msgPREGUNTA,
                "�Est� seguro de que desea eliminar este registro?");
            msgBox.show();

            // el usuario confirma la operaci�n
            if (msgBox.getResponse()) {

              msgBox = null;

              if (dtDev.getString("FC_BAJA").equals("")) {
                int not = buscarNotificaciones();
                if (not == 0) {
                  try {
                    //No tiene notificaciones pendientes:borrado por delete
                    QueryTool qtBorr = new QueryTool();

                    // tabla de familias de productos
                    qtBorr.putName("SIVE_ENF_CENTI");

                    // campos que se seleccionan
                    qtBorr.putWhereType("CD_ENFCIE", QueryTool.STRING);
                    qtBorr.putWhereValue("CD_ENFCIE",
                                         pnlMantEnfCent.getDatos().
                                         getString("CD_ENFCIE"));
                    qtBorr.putOperator("CD_ENFCIE", "=");

                    vFiltro.addElement(qtBorr);
                    this.getApp().getStub().setUrl(servlet);
                    vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 5,
                                               vFiltro);
                    bAceptar = true;
                    bBorrLista = true;
                    dispose();
                  }
                  catch (Exception ex) {
                    this.getApp().trazaLog(ex);
                    this.getApp().showError(ex.getMessage());
                    bAceptar = false;
                    dispose();
                  }
                }
                else {
                  try {
                    //Tiene notifs pendientes:borrado por marca
                    QueryTool qtBorr = new QueryTool();

                    qtBorr.putName("SIVE_ENF_CENTI");

                    qtBorr.putType("FC_BAJA", QueryTool.DATE);
                    qtBorr.putValue("FC_BAJA", Format.fechaActual());

                    qtBorr.putWhereType("CD_ENFCIE", QueryTool.STRING);
                    qtBorr.putWhereValue("CD_ENFCIE",
                                         dtDev.getString("CD_ENFCIE"));
                    qtBorr.putOperator("CD_ENFCIE", "=");

                    // realiza la consulta al servlet
                    this.getApp().getStub().setUrl(servlet);
                    vFiltro.addElement(qtBorr);
                    vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 4,
                                               vFiltro);
                    bAceptar = true;
                    dispose();
                  }
                  catch (Exception ex) {
                    this.getApp().trazaLog(ex);
                    this.getApp().showError(ex.getMessage());
                    bAceptar = false;
                    bBorrLista = false;
                    dispose();
                  }
                }
                Inicializar(CInicializar.NORMAL);
              }
              else {
                this.getApp().showAdvise("Enfermedad previamente dada de baja");
                bAceptar = false;
                dispose();
              }
            } // De la pantallita de Si/No
            msgBox = null;
            break;
        } //end if switch
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  public boolean bBorrLista() {
    return bBorrLista;
  }

  void chVigRut_itemStateChanged(ItemEvent e) {
    if (chVigRut.getState() == true) {
      txtSemIni.setEnabled(true);
      txtSemFin.setEnabled(true);
      lblSemIni.setEnabled(true);
      lblSemFin.setEnabled(true);
    }
    else {
      txtSemIni.setEnabled(false);
      txtSemFin.setEnabled(false);
      txtSemIni.setText("");
      txtSemFin.setText("");
    }
  }

  void pnlMantEnfCent_mouseEntered(MouseEvent e) {
    pnlMantEnfCent.transferFocus();
    pnlMantEnfCent.requestFocus();
  }

}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantenfcent adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMantenfcent adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

class DiaMantenfcent_chVigRut_itemAdapter
    implements java.awt.event.ItemListener {
  DiaMantenfcent adaptee;

  DiaMantenfcent_chVigRut_itemAdapter(DiaMantenfcent adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chVigRut_itemStateChanged(e);
  }
}

class DiaMantenfcent_pnlMantEnfCent_mouseAdapter
    extends java.awt.event.MouseAdapter {
  DiaMantenfcent adaptee;

  DiaMantenfcent_pnlMantEnfCent_mouseAdapter(DiaMantenfcent adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseEntered(MouseEvent e) {
    adaptee.pnlMantEnfCent_mouseEntered(e);
  }
}
