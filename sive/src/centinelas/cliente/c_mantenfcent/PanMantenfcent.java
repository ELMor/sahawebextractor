//Title:        Mantenimiento de Enfermedades Centinelas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       MTR
//Company:
//Description:

//Panel inicial que permite buscar y seleccionar enfermedades para poder
//modificarlas ,borrarlas o a�adir otras nuevas

package centinelas.cliente.c_mantenfcent;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPanel;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class PanMantenfcent
    extends CPanel
    implements CInicializar, CFiltro, ContCPnlCodigoExt {
  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();
  CPnlCodigoExt pnlEnfCent = null;
  ButtonControl btnBuscar = new ButtonControl();
  CListaMantenimiento clmMantEnfCent = null;
  Label lblEnfCent = new Label();

  public PanMantenfcent(CApp a) {
    //configuro CListaMantenimiento y CPnlCodigoExt
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qtEnfCenti = new QueryTool();

    setApp(a);
    qtEnfCenti.putName("SIVE_PROCESOS");
    qtEnfCenti.putType("CD_ENFCIE", QueryTool.STRING);
    qtEnfCenti.putType("DS_PROCESO", QueryTool.STRING);
    qtEnfCenti.putSubquery("CD_ENFCIE", "select CD_ENFCIE from SIVE_ENF_CENTI");
    try {

      //panel de punto centinela
      pnlEnfCent = new CPnlCodigoExt(a,
                                     this,
                                     qtEnfCenti,
                                     "CD_ENFCIE",
                                     "DS_PROCESO",
                                     false,
                                     "Enfermedades centinelas",
                                     "Enfermedades centinelas",
                                     this);

      // configura la lista de inspecciones
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nueva enfermedad centinela",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar enfermedad centinela",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "Borrar enfermedad centinela",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("C�digo enfermedad",
                                      "140",
                                      "CD_ENFCIE"));

      vLabels.addElement(new CColumna("Descripci�n enfermedad",
                                      "250",
                                      "DS_PROCESO"));

      vLabels.addElement(new CColumna("Vigilanc. Rutinaria",
                                      "110",
                                      "IT_RUTINA"));

      vLabels.addElement(new CColumna("Fecha_baja",
                                      "100",
                                      "FC_BAJA"));

      clmMantEnfCent = new CListaMantenimiento(a,
                                               vLabels,
                                               vBotones,
                                               this,
                                               this,
                                               250,
                                               640);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";

    // carga la imagen
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();

    xyLayout.setHeight(385);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    lblEnfCent.setText("Enfermedad:");
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(new PanMantenfcent_btnBuscar_actionAdapter(this));

    // a�ade los componentes
    this.add(lblEnfCent,
             new XYConstraints(3 * CPnlCodigoExt.LONG_SEP, MARGENSUP, -1, ALTO));
    this.add(pnlEnfCent,
             new XYConstraints(CPnlCodigoExt.LONG_SEP * 2 +
                               CPnlCodigoExt.LONG_ETI, MARGENSUP, TAMPANEL, 34));
    this.add(btnBuscar,
             new XYConstraints(535, MARGENSUP + 34 + INTERVERT, 80, 24));
    this.add(clmMantEnfCent,
             new XYConstraints(CPnlCodigoExt.LONG_SEP,
                               MARGENSUP + 34 + 3 * INTERVERT + ALTO, 650, 230));

  }

  public String getDescCompleta(Data dat) {
    return ("");
  }

  public void trasEventoEnCPnlCodigoExt() {}

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pnlEnfCent.backupDatos();
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        pnlEnfCent.setEnabled(true);
        clmMantEnfCent.setEnabled(true);
        lblEnfCent.setEnabled(true);
        btnBuscar.setEnabled(true);
    }
  }

  //solicita la primera trama de datos. 2 casos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //1� CASO: no se realiza ning�n filtro: QueryTool2 sin where
    if (pnlEnfCent.getDatos() == null) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_ENF_CENTI");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_ENFCIE", QueryTool.STRING);
        qt.putType("IT_ENVCNE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_RUTINA", QueryTool.STRING);
        qt.putType("CD_SEMINIP", QueryTool.STRING);
        qt.putType("CD_SEMFINP", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_BAJA", QueryTool.DATE);
        qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

        // Datos adicionales:descripci�n de enfermedad centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_PROCESOS");
        qtAdic.putType("DS_PROCESO", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_ENFCIE", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        qt.addOrderField("CD_ENFCIE");

        p.addElement(qt);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          this.getApp().showAdvise("No se poseen elementos");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        ex.printStackTrace();

      }
    } //Fin del primer caso

    //2� CASO: se realiza el filtro de Enfermedad
    if (pnlEnfCent.getDatos() != null) {
      QueryTool2 qt = new QueryTool2();
      try {
        // Nombre de la tabla (FROM)
        qt.putName("SIVE_ENF_CENTI");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_ENFCIE", QueryTool.STRING);
        qt.putType("IT_ENVCNE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_RUTINA", QueryTool.STRING);
        qt.putType("CD_SEMINIP", QueryTool.STRING);
        qt.putType("CD_SEMFINP", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_BAJA", QueryTool.DATE);
        qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

        qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
        qt.putWhereValue("CD_ENFCIE",
                         ( (Data) pnlEnfCent.getDatos()).get("CD_ENFCIE"));
        qt.putOperator("CD_ENFCIE", "=");

        // Datos adicionales:descripci�n de enfermedad centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_PROCESOS");
        qtAdic.putType("DS_PROCESO", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_ENFCIE", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        qt.addOrderField("CD_ENFCIE");

        p.addElement(qt);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          pnlEnfCent.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }

    } //Fin del segundo caso
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //1� CASO: no se realiza ning�n filtro: QueryTool2 sin where
    if (pnlEnfCent.getDatos() == null) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_ENF_CENTI");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_ENFCIE", QueryTool.STRING);
        qt.putType("IT_ENVCNE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_RUTINA", QueryTool.STRING);
        qt.putType("CD_SEMINIP", QueryTool.STRING);
        qt.putType("CD_SEMFINP", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_BAJA", QueryTool.DATE);
        qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

        // Datos adicionales:descripci�n de enfermedad centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_PROCESOS");
        qtAdic.putType("DS_PROCESO", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_ENFCIE", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        qt.addOrderField("CD_ENFCIE");

        p.addElement(qt);
        p.setTrama(this.clmMantEnfCent.getLista().getTrama());
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          this.getApp().showAdvise("No se poseen elementos");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del primer caso

    //2� CASO: se realiza el filtro de Enfermedad
    if (pnlEnfCent.getDatos() != null) {
      QueryTool2 qt = new QueryTool2();
      try {
        // Nombre de la tabla (FROM)
        qt.putName("SIVE_ENF_CENTI");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_ENFCIE", QueryTool.STRING);
        qt.putType("IT_ENVCNE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_RUTINA", QueryTool.STRING);
        qt.putType("CD_SEMINIP", QueryTool.STRING);
        qt.putType("CD_SEMFINP", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_BAJA", QueryTool.DATE);
        qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

        qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
        qt.putWhereValue("CD_ENFCIE",
                         ( (Data) pnlEnfCent.getDatos()).get("CD_ENFCIE"));
        qt.putOperator("CD_ENFCIE", "=");

        // Datos adicionales:descripci�n de enfermedad centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_PROCESOS");
        qtAdic.putType("DS_PROCESO", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_ENFCIE", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        qt.addOrderField("CD_ENFCIE");

        p.addElement(qt);
        p.setTrama(this.clmMantEnfCent.getLista().getTrama());
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          pnlEnfCent.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }

    } //Fin del segundo caso
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public void realizaOperacion(int j) {
    DiaMantenfcent di = null;
    Data dMantEnfCent = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        di = new DiaMantenfcent(this.getApp(), ALTA, dMantEnfCent);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar) {
          clmMantEnfCent.setPrimeraPagina(primeraPagina());
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantEnfCent = clmMantEnfCent.getSelected();
        //si existe alguna fila seleccionada
        if (dMantEnfCent != null) {
          int ind = clmMantEnfCent.getSelectedIndex();
          di = new DiaMantenfcent(this.getApp(), MODIFICACION, dMantEnfCent);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar) {
            clmMantEnfCent.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dMantEnfCent = clmMantEnfCent.getSelected();
        if (dMantEnfCent != null) {
          int ind = clmMantEnfCent.getSelectedIndex();
          di = new DiaMantenfcent(this.getApp(), BAJA, dMantEnfCent);
          di.show();
          if (di.bAceptar) {
            clmMantEnfCent.getLista().removeElementAt(ind);
            clmMantEnfCent.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }
  }

  void btnBuscar_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    clmMantEnfCent.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }
}

class PanMantenfcent_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener {
  PanMantenfcent adaptee;

  PanMantenfcent_btnBuscar_actionAdapter(PanMantenfcent adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnBuscar_actionPerformed(e);
  }
}
