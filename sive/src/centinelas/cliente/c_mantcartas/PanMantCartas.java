
package centinelas.cliente.c_mantcartas;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPanel;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

/**
 * Panel a trav�s del que se realiza el mantenimiento de los puntos centinelas
 *
 * @autor MTR.
 * @version 1.0
 */

public class PanMantCartas
    extends CPanel
    implements CInicializar, CFiltro {
  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();
  ButtonControl btnBuscar = new ButtonControl();
  CListaMantenimiento clmMantCartas = null;

  Checkbox chActivos = new Checkbox();
  Checkbox chDesactivos = new Checkbox();
  Checkbox chTodos = new Checkbox();
  CheckboxGroup chkGrp1 = new CheckboxGroup();

  //_________________________________________________________

  public PanMantCartas(CApp a) {
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qtTipo = new QueryTool();
    QueryTool qtPuntoCent = new QueryTool();

    try {
      setApp(a);

      // configura la lista de inspecciones
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nuevo punto centinela",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar punto centinela",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "Borrar punto centinela",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("C�d. modelo",
                                      "90",
                                      "CD_PLANTILLA"));

      vLabels.addElement(new CColumna("Descripci�n modelo",
                                      "440",
                                      "DS_PLANTILLA"));

      vLabels.addElement(new CColumna("Baja",
                                      "70",
                                      "IT_BAJA"));

      clmMantCartas = new CListaMantenimiento(a,
                                              vLabels,
                                              vBotones,
                                              this,
                                              this,
                                              250,
                                              640);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //_________________________________________________________

  void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";

    // carga la imagen
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();

    xyLayout.setHeight(420);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(new PanMantCartas_btnBuscar_actionAdapter(this));

    // a�ade los componentes

    this.add(chActivos, new XYConstraints(MARGENIZQ, MARGENSUP, -1, ALTO));
    this.add(chDesactivos,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + 3, -1, ALTO));
    this.add(chTodos,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * 3, -1,
                               ALTO));

    this.add(btnBuscar,
             new XYConstraints(540, MARGENSUP + 2 * 3 + INTERVERT + 3 * ALTO,
                               80, 24));
    this.add(clmMantCartas,
             new XYConstraints(MARGENIZQ - DESFPANEL,
                               MARGENSUP + 2 * 3 + 3 * INTERVERT + 4 * ALTO,
                               650, 230));

    chActivos.setLabel("Activos");
    chActivos.setCheckboxGroup(chkGrp1);
    chActivos.addItemListener(new PanMantCartas_chActivos_itemAdapter(this));
    chDesactivos.setLabel("Desactivos");
    chDesactivos.setCheckboxGroup(chkGrp1);
    chDesactivos.addItemListener(new PanMantCartas_chDesactivos_itemAdapter(this));
    chTodos.setLabel("Todos");
    chTodos.setCheckboxGroup(chkGrp1);
    chTodos.addItemListener(new PanMantCartas_chTodos_itemAdapter(this));
    chTodos.setState(true);

  }

  //_________________________________________________________

  public String getDescCompleta(Data dat) {
    return ("");
  }

  //_________________________________________________________

  public void Inicializar() {
  }

  //_________________________________________________________

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        clmMantCartas.setEnabled(true);
        chActivos.setEnabled(true);
        chDesactivos.setEnabled(true);
        chTodos.setEnabled(true);
        btnBuscar.setEnabled(true);
    }
  }

  //_________________________________________________________

  private QueryTool2 obtenerActivos(QueryTool2 qt) {
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "N");
    qt.putOperator("IT_BAJA", "=");
    return qt;
  }

  private QueryTool2 obtenerDesactivos(QueryTool2 qt) {
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "S");
    qt.putOperator("IT_BAJA", "=");
    return qt;
  }

  //_________________________________________________________

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    QueryTool2 qt = new QueryTool2();

    try {
      qt.putName("SIVE_PLANTILLA_RMC");
      // Campos que se quieren leer (SELECT)
      qt.putType("CD_PLANTILLA", QueryTool.STRING);
      qt.putType("DS_PLANTILLA", QueryTool.STRING);
      qt.putType("IT_BAJA", QueryTool.STRING);
      qt.putType("CD_OPE", QueryTool.STRING);
      qt.putType("FC_ULTACT", QueryTool.DATE);
      qt.putType("DS_MOTIVBAJA", QueryTool.STRING);

      if (chActivos.getState()) {
        qt = obtenerActivos(qt);
      }
      if (chDesactivos.getState()) {
        qt = obtenerDesactivos(qt);
      }

      qt.addOrderField("CD_PLANTILLA");

      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() == 0) {
        this.getApp().showAdvise("No se poseen elementos");
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }

    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  //_________________________________________________________

  // solicita la siguiente trama de datos
  // es igual q el primeraPagina pero con una sentencia +
  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    try {
      QueryTool2 qt = new QueryTool2();

      qt.putName("SIVE_PLANTILLA_RMC");
      // Campos que se quieren leer (SELECT)
      qt.putType("CD_PLANTILLA", QueryTool.STRING);
      qt.putType("DS_PLANTILLA", QueryTool.STRING);
      qt.putType("IT_BAJA", QueryTool.STRING);
      qt.putType("CD_OPE", QueryTool.STRING);
      qt.putType("FC_ULTACT", QueryTool.DATE);
      qt.putType("DS_MOTIVBAJA", QueryTool.STRING);

      if (chActivos.getState()) {
        qt = obtenerActivos(qt);
      }
      if (chDesactivos.getState()) {
        qt = obtenerDesactivos(qt);
      }

      qt.addOrderField("CD_PLANTILLA");

      p.addElement(qt);
      p.setTrama(this.clmMantCartas.getLista().getTrama());
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() == 0) {
        this.getApp().showAdvise("No se poseen elementos");
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }

    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  //_________________________________________________________
  // operaciones de la botonera
  //mirar en PanPetCen.
  //pasarlo como Data
  public void realizaOperacion(int j) {

    Lista lismanpc = this.clmMantCartas.getLista();
    DiaMantCartas di = null;
    //Este Data debe pasarse como par�metro al constructor de PuntoCentExt
    Data dMantCartas = new Data();
    switch (j) {
      // bot�n de alta:data vacio
      case ALTA:

        di = new DiaMantCartas(this.getApp(), ALTA, dMantCartas);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar()) {
          clmMantCartas.setPrimeraPagina(primeraPagina());
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantCartas = clmMantCartas.getSelected();
        //si existe alguna fila seleccionada
        if (dMantCartas != null) {
          int ind = clmMantCartas.getSelectedIndex();
          di = new DiaMantCartas(this.getApp(), MODIFICACION, dMantCartas);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar()) {
            clmMantCartas.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dMantCartas = clmMantCartas.getSelected();
        if (dMantCartas != null) {
          int ind = clmMantCartas.getSelectedIndex();
          di = new DiaMantCartas(this.getApp(), BAJA, dMantCartas);
          di.show();
          if (di.bAceptar()) {
            if (di.borrPto() == 0) {
              clmMantCartas.getLista().removeElementAt(ind);
              clmMantCartas.setPrimeraPagina(clmMantCartas.getLista());
            }
            if (di.borrPto() == 1) {
              clmMantCartas.setPrimeraPagina(primeraPagina());
            }
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }

  }

  //_________________________________________________________

  void btnBuscar_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    clmMantCartas.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  void chActivos_itemStateChanged(ItemEvent e) {
    if (chActivos.getState()) {
      chDesactivos.setState(false);
      chTodos.setState(false);
    }
  }

  void chDesactivos_itemStateChanged(ItemEvent e) {
    if (chDesactivos.getState()) {
      chActivos.setState(false);
      chTodos.setState(false);
    }
  }

  void chTodos_itemStateChanged(ItemEvent e) {
    if (chTodos.getState()) {
      chActivos.setState(false);
      chDesactivos.setState(false);
    }
  }

}

class PanMantCartas_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener {
  PanMantCartas adaptee;

  PanMantCartas_btnBuscar_actionAdapter(PanMantCartas adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnBuscar_actionPerformed(e);
  }
}

class PanMantCartas_chActivos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantCartas adaptee;

  PanMantCartas_chActivos_itemAdapter(PanMantCartas adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chActivos_itemStateChanged(e);
  }
}

class PanMantCartas_chDesactivos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantCartas adaptee;

  PanMantCartas_chDesactivos_itemAdapter(PanMantCartas adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chDesactivos_itemStateChanged(e);
  }
}

class PanMantCartas_chTodos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantCartas adaptee;

  PanMantCartas_chTodos_itemAdapter(PanMantCartas adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chTodos_itemStateChanged(e);
  }
}
