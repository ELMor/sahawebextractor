/**
 * Clase: DlgMenu
 * Paquete: centinelas.cliente.c_menu
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 14/02/2000
 * Analisis Funcional: Gesti�n de men�s
 * Descripcion: Di�logo de recogida de parametros
 *  generales al m�dulo de centinelas.
 */

package centinelas.cliente.c_menu;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import centinelas.cliente.c_comuncliente.SincrEventos;
import comun.constantes;
import sapp2.Data;

public class DlgMenu
    extends CDialog
    implements CInicializar {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;

  // Vars. intermedias

  // Parametros de salida
  private boolean OK = false;
  private Data dtParametros = new Data();

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Imagenes a cargar
  final String imgAceptar = "images/aceptar.gif";
  final String imgCancelar = "images/cancelar.gif";

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // A�o
  Label lblAno = null;
  CEntero ceAno = null;

  // Botones de salida
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DlgMenuActionAdapter actionAdapter = null;

  // Variable del a�o
  String anyito = "";

  // Constructor
  // @param a: CApp
  // @param modo: modo de entrada al di�logo
  public DlgMenu(CApp a, int modo, String anoActual) {

    super(a);
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    applet = a;

    // Titulo
    setTitle("Parametros [Centinelas]");

    try {
      // Iniciaci�n
      jbInit();

      // Establecemos el valor inicial del a�o
//      ceAno.setText(applet.getParameter("FC_ACTUAL").substring(6));
      ceAno.setText(anoActual);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del dialogo DiaAltaNotifRMC
  public void jbInit() throws Exception {
    // Carga las imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().put(constantes.imgCANCELAR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DlgMenuActionAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(250, 150));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(250);
    xYLayout1.setHeight(150);

    // A�o
    lblAno = new Label("A�o:");
    ceAno = new CEntero(4);

    // Bot�n Aceptar
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgACEPTAR));
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);

    // Bot�n Cancelar
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgCANCELAR));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);

    // Adici�n de componentes al di�logo
    this.add(lblAno, new XYConstraints(55, 25, 35, -1));
    this.add(ceAno, new XYConstraints(105, 25, 89, -1));
    this.add(btnAceptar, new XYConstraints(55, 80, 80, -1));
    this.add(btnCancelar, new XYConstraints(145, 80, 80, -1));

    // Tool Tips
    new CContextHelp("Aceptar", btnAceptar);
    new CContextHelp("Cancelar", btnCancelar);
  }

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Iniciar()

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        ceAno.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case constantes.modoALTA:
          case constantes.modoMODIFICACION:
            ceAno.setEnabled(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case constantes.modoBAJA:
          case constantes.modoCONSULTA:
            ceAno.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /******************** Acceso a vars. salida *********************/

  // Devuelve true si se sale mediante el bot�n de Aceptar
  public boolean getAceptar() {
    return OK;
  } // Fin getAceptar()

  public Data getParametros() {
    return dtParametros;
  } // Fin getParametros()

  /******************** Funcs. Auxiliares ************************/

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {

    if (ceAno.getText().trim().length() != 0) {
      dtParametros.put("CD_ANO", ceAno.getText());
      OK = true;
    }
    else {
      OK = false;

      // Desaparece el dialogo
    }
    dispose();
  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    // Se sale por cancelar
    OK = false;
    // Desaparece el dialogo
    dispose();
  } // Fin btnCancelarActionPerformed()

} // endclass DlgMenu

/******************* ESCUCHADORES **********************/

// Botones
class DlgMenuActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DlgMenu adaptee;
  ActionEvent evt;

  DlgMenuActionAdapter(DlgMenu adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {

      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }

      s.desbloquea(adaptee);
    }
  }

} // endclass  DlgMenuActionAdapter
