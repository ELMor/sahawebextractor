// 16/02/00 (JMT): Introduccion del proceso de cobertura
//Di�logo con los datos de un m�dico notificador

package centinelas.cliente.c_mantmednotif;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CMessage;
import capp2.CTexto;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ChoiceCDDS;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_componentes.ContPanAnoSemFecha;
import centinelas.cliente.c_componentes.ContPanFecha;
import centinelas.cliente.c_componentes.PanAnoSemFecha;
import centinelas.cliente.c_componentes.PanFecha;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.MedCent;
import componentes.CPnlDosCodigos;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

//------------------- Machacar esta l�nea
//import centinelas.servidor.cobertura.*;

public class DiaMantmednotif
    extends CDialog
    implements CInicializar, ContCPnlCodigoExt, ContPanAnoSemFecha,
    ContPanFecha {
  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  // Constantes del proceso de cobertura
  final protected int AVANCE_SEMGEN = 0; // Avance de semGen
  final protected int MIN_SEMALTA_INSMED = 1; // Minima A�o/Sem alta en la inseri�n de un m�dico
  final protected int INSMED = 2; // Inserci�n de un m�dico
  final protected int MAXMIN_SEMALTA_MODMED = 3; // Maxima/Minima A�o/Sem alta en la inserci�n de un m�dico
  final protected int MODMED = 4; // Modificaci�n de un m�dico
  final protected int MIN_SEMBAJA_BORMED = 5; // Minimo A�o/Sem de baja en el borrado de un medico
  final protected int BORMED = 6; // Borrado de un medico (fisico o por IT_BAJA)
//  final String srv_cobertura = "servlet/SrvCobertura";
  final String srv_cobertura = nombreservlets.strSERVLET_COBERTURA;

  // Datos sobre la cobertura
  Data dtCoberturaAlta = null;
  Data dtCoberturaMod = null;
  Data dtCoberturaBaja = null;

  boolean primeraInicializacion = true;

  // parametros pasados al di�logo
  public MedCent dtDev = null;

  //par�metros devueltos por el di�logo
  public MedCent dtDevuelto = new MedCent();

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;

  PanAnoSemFecha panEpidem = null;
  PanFecha panFecBaja = null;
  CCodigo txtNotif = new CCodigo(6);
  XYLayout xyLayout1 = new XYLayout();
  Label lblNotif = new Label();
  Label lblPuntCent = new Label();
  Label lblAltaSem = new Label();
  CEntero txtAltaSem = new CEntero(2);
  Label lblApel = new Label();
  CTexto txtApel1 = new CTexto(20);
  CTexto txtApel2 = new CTexto(20);
  Label lblNomb = new Label();
  CTexto txtNomb = new CTexto(20);
  Label lblDNI = new Label();
  CCodigo txtDNI = new CCodigo(10);
  Label lblFechNac = new Label();
  // Correcci�n 22-05-01 (ARS)
  // Poner las barras autom�ticamente
  fechas.CFecha txtFecNac = new fechas.CFecha("N");
  Label lblSexo = new Label();
  ChoiceCDDS chSexo = null;
  Label lblTelefono = new Label();
  CEntero txtTelefono = new CEntero(14);
  Label lblFax = new Label();
  CEntero txtFax = new CEntero(9);
  Label lblEqNotif = new Label();
  Label lblCentro = new Label();
  Label lblModAsis = new Label();
  ChoiceCDDS chModAsis = null;
  Label lblRangEd = new Label();
  CEntero txtEdDesde = new CEntero(3);
  Label lblEdHasta = new Label();
  CEntero txtEdHasta = new CEntero(3);
  Label lblHorDesde = new Label();
  CCodigo txtHorDesde = new CCodigo(5);
  Label lblHorHasta = new Label();
  CCodigo txtHorHasta = new CCodigo(5);
  Label lblFecBaja = new Label();
//  CFecha txtFecBaja = new CFecha();
  Label lblMotBaja = new Label();
  ChoiceCDDS chMotBaja = null;
  GroupBox gbBaja = new GroupBox();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();

  CPnlCodigoExt pnlPuntoCent = null;
  CPnlCodigoExt pnlEqNotif = null;
  CPnlCodigoExt pnlCentro = null;
  CPnlDosCodigos pnlEqCentro = null;

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);
  Label lblApe2 = new Label();

  public DiaMantmednotif(CApp a, int modoop, MedCent dt) {
    super(a);
    modoOperacion = modoop;
    Lista lSexo = new Lista();
    Lista lModAsist = new Lista();
    Lista lMotBaja = new Lista();
    QueryTool qtPuntoCent = new QueryTool();
    QueryTool qtEqNotif = new QueryTool();
    txtEdDesde.setText("0");
    txtEdHasta.setText("0");
    txtHorDesde.setText("0");
    txtHorHasta.setText("0");
    dtDev = dt;

    try {
      lSexo = obtenerSexo();
      lModAsist = obtenerModAsist();
      lMotBaja = obtenerMotBaja();
      //configuro el ChoiceCDDS de sexo
      chSexo = new ChoiceCDDS(false, true, true, "CD_SEXO", "DS_SEXO", lSexo);
      chModAsis = new ChoiceCDDS(false, true, true, "CD_MASIST", "DS_MASIST",
                                 lModAsist);
      chMotBaja = new ChoiceCDDS(true, true, true, "CD_MOTBAJA", "DS_MOTBAJA",
                                 lMotBaja);

      //configuro el panel de punto centinela
      qtPuntoCent.putName("SIVE_PCENTINELA");
      qtPuntoCent.putType("CD_PCENTI", QueryTool.STRING);
      qtPuntoCent.putType("DS_PCENTI", QueryTool.STRING);

      //panel de punto centinela
      pnlPuntoCent = new CPnlCodigoExt(a,
                                       this,
                                       qtPuntoCent,
                                       "CD_PCENTI",
                                       "DS_PCENTI",
                                       true,
                                       "Puntos centinelas",
                                       "Puntos centinelas",
                                       this);

      QueryTool qtCentro = new QueryTool();
      qtCentro.putName("SIVE_C_NOTIF");
      qtCentro.putType("CD_CENTRO", QueryTool.STRING);
      qtCentro.putType("DS_CENTRO", QueryTool.STRING);
      pnlCentro = new CPnlCodigoExt(getApp(),
                                    this,
                                    qtCentro,
                                    "CD_CENTRO",
                                    "DS_CENTRO",
                                    true,
                                    "Centro notificador",
                                    "Centro notificador",
                                    this);

      qtEqNotif = new QueryTool();
      qtEqNotif.putName("SIVE_E_NOTIF");
      qtEqNotif.putType("CD_E_NOTIF", QueryTool.STRING);
      qtEqNotif.putType("DS_E_NOTIF", QueryTool.STRING);

      //V�nculo con tabla tipos. Cambiar� el WhereValue al hacerlo el c�digo de centro
      qtEqNotif.putWhereType("CD_CENTRO", QueryTool.STRING);
      qtEqNotif.putWhereValue("CD_CENTRO", "");
      qtEqNotif.putOperator("CD_CENTRO", "=");

      pnlEqNotif = new CPnlCodigoExt(getApp(),
                                     this,
                                     qtEqNotif,
                                     "CD_E_NOTIF",
                                     "DS_E_NOTIF",
                                     true,
                                     "Equipo Notificador",
                                     "Equipo Notificador",
                                     this);

      pnlEqCentro = new CPnlDosCodigos(pnlCentro, pnlEqNotif, "CD_CENTRO", this,
                                       CPnlDosCodigos.modoVERTICAL);

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(670, 490);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    lblNotif.setText("N� Notificador:");
    lblPuntCent.setText("Punto Centinela:");
    lblAltaSem.setText("Alta desde la semana Epidemiologica:");
    lblApel.setText("Apell1:");
    lblNomb.setText("Nombre:");
    lblDNI.setText("DNI:");
    lblFechNac.setText("Fecha Nacimiento:");
    lblSexo.setText("Sexo:");
    lblTelefono.setText("Telefono:");
    lblFax.setText("Fax:");
    lblEqNotif.setText("Equipo Notif:");
    lblCentro.setText("Centro:");
    lblModAsis.setText("Modelo Asist:");
    lblRangEd.setText("Rango de edades atendidas. Desde:");
    lblEdHasta.setText("Hasta:");
    lblHorDesde.setText("Horario trabajo. Desde:");
    lblHorHasta.setText("Hasta:");
    lblFecBaja.setText("Fecha Baja:");
    lblMotBaja.setText("Motivo Baja:");
    gbBaja.setLabel("Datos sobre la baja");
    this.setLayout(xyLayout1);
    xyLayout1.setHeight(618);
    xyLayout1.setWidth(614);

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    pnlPuntoCent.addMouseListener(new DiaMantmednotif_pnlPuntoCent_mouseAdapter(this));
    lblApe2.setText("Apell2:");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    //ponemos los campos como obligatorios
    txtNotif.setBackground(new Color(255, 255, 150));
    txtNomb.setBackground(new Color(255, 255, 150));
    txtApel1.setBackground(new Color(255, 255, 150));
    txtApel2.setBackground(new Color(255, 255, 150));
    txtAltaSem.setBackground(new Color(255, 255, 150));
    chSexo.setBackground(new Color(255, 255, 150));
    chModAsis.setBackground(new Color(255, 255, 150));
    chMotBaja.setBackground(new Color(255, 255, 150));
    chMotBaja.addItemListener(new DiaMantmednotif_chMotBaja_itemAdapter(this));

    chSexo.writeData();
    chModAsis.writeData();
    chMotBaja.writeData();

    this.add(txtNotif, new XYConstraints(106, 23, 94, -1));
    this.add(lblNotif, new XYConstraints(22, 23, 79, 21));
    this.add(lblPuntCent, new XYConstraints(223, 23, 93, 19));
    this.add(pnlPuntoCent, new XYConstraints(330, 16, 651, 34));
    this.add(lblAltaSem, new XYConstraints(22, 67, 214, 18));
    this.add(lblApel, new XYConstraints(22, 103, 61, 20));
    this.add(txtApel1, new XYConstraints(108, 103, 235, 19));
    this.add(txtApel2, new XYConstraints(463, 103, 172, 19));
    this.add(lblNomb, new XYConstraints(22, 143, 48, 18));
    this.add(txtNomb, new XYConstraints(108, 143, 235, 20));
    this.add(lblDNI, new XYConstraints(400, 143, 27, 25));
    this.add(txtDNI, new XYConstraints(464, 143, 173, 21));
    this.add(lblFechNac, new XYConstraints(22, 183, 105, 21));
    this.add(txtFecNac, new XYConstraints(168, 183, 173, 21));
    this.add(lblSexo, new XYConstraints(400, 183, 36, -1));
    this.add(chSexo, new XYConstraints(464, 183, 172, 15));
    this.add(lblTelefono, new XYConstraints(22, 223, 58, 19));
    this.add(txtTelefono, new XYConstraints(108, 223, 235, 19));
    this.add(lblFax, new XYConstraints(400, 223, 25, 21));
    this.add(txtFax, new XYConstraints(464, 223, 173, 19));

    this.add(lblCentro, new XYConstraints(22, 263, 75, 20));
    this.add(lblEqNotif, new XYConstraints(22, 303, 75, 20));

    this.add(lblModAsis, new XYConstraints(22, 343, 75, 21));
    this.add(chModAsis, new XYConstraints(108, 343, 308, 22));
    this.add(lblRangEd, new XYConstraints(22, 384, 211, 18));
    this.add(txtEdDesde, new XYConstraints(234, 384, 64, 24));
    this.add(lblEdHasta, new XYConstraints(311, 383, 35, 24));
    this.add(txtEdHasta, new XYConstraints(351, 383, 64, 24));
    this.add(lblHorDesde, new XYConstraints(22, 423, 143, 15));
    this.add(txtHorDesde, new XYConstraints(236, 423, 64, 24));
    this.add(lblHorHasta, new XYConstraints(311, 423, 35, 24));
    this.add(txtHorHasta, new XYConstraints(351, 423, 64, 24));

    this.add(lblFecBaja,
             new XYConstraints(105 + 320 + INTERHOR - 2, 303, 66, ALTO));
    this.add(lblMotBaja,
             new XYConstraints(105 + 320 + INTERHOR - 2, 343, 66, ALTO));
    this.add(chMotBaja,
             new XYConstraints(105 + 320 + 2 * INTERHOR + 64, 343, 120, ALTO));

    this.add(btnCancelar, new XYConstraints(553, 419, 88, 29));
    this.add(btnAceptar, new XYConstraints(434, 419, 88, 29));

    verDatos();

    this.add(pnlEqCentro, new XYConstraints(103, 263, 320, 2 * 34));
    this.add(panEpidem, new XYConstraints(280, 63, 515, 34));
    this.add(panFecBaja,
             new XYConstraints(105 + 320 + 2 * INTERHOR + 64, 303, 100, 34));
    this.add(gbBaja,
             new XYConstraints(105 + 320 + INTERHOR - 15, 263,
                               120 + INTERHOR + 66 + 30, 130));
    this.add(lblApe2, new XYConstraints(400, 103, 57, 20));

    Inicializar(CInicializar.NORMAL);

    // ponemos el t�tulo
    switch (modoOperacion) {
      case modoALTA:
        setTitle("ALTA");
        break;
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return ("");
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pnlEqCentro.InicializarEnModo(CInicializar.ESPERA);
        panEpidem.Inicializate(CInicializar.ESPERA);
        panFecBaja.Inicializate(CInicializar.ESPERA);
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        panEpidem.Inicializate(CInicializar.NORMAL);
        panFecBaja.Inicializate(CInicializar.NORMAL);

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);

            if (primeraInicializacion) {
              pnlEqCentro.inicializatePrimeraVezNormal(true);
              primeraInicializacion = false;
            }
            else {
              pnlEqCentro.InicializarEnModo(CInicializar.NORMAL); //LRG
            }

            txtNotif.setEnabled(false);
            panFecBaja.setEnabled(false);
            chMotBaja.setEnabled(false);

            break;

          case modoMODIFICACION:
            if (dtDev.getString("IT_BAJA").equals("S")) {
              this.setEnabled(false);
              btnCancelar.setEnabled(true);
            }
            else {
              this.setEnabled(true);
              if (primeraInicializacion) {
                pnlEqCentro.inicializatePrimeraVezNormal(true);
                primeraInicializacion = false;
              }
              else {
                pnlEqCentro.InicializarEnModo(CInicializar.NORMAL); //LRG
              }
              txtNotif.setEnabled(false);
              pnlPuntoCent.setEnabled(false);
              panFecBaja.setEnabled(false);
              chMotBaja.setEnabled(false);
            }
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            this.setEnabled(false);
            pnlEqCentro.inicializatePrimeraVezNormal(false); //LRG
            if (dtDev.getIT_BAJA().equals("N")) {
              panFecBaja.setEnabled(true);
              chMotBaja.setEnabled(true);
              btnAceptar.setEnabled(true);
            }
            else {
              panFecBaja.setEnabled(false);
              chMotBaja.setEnabled(false);
            }
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  public MedCent recogerDatos() {
    MedCent rec = new MedCent();
    //en alta no se introduce el cd_medcen:secuenciador
    rec.put("CD_PCENTI", pnlPuntoCent.getDatos().getString("CD_PCENTI"));
    rec.put("CD_E_NOTIF", pnlEqNotif.getDatos().getString("CD_E_NOTIF"));
    rec.put("CD_SEXO", chSexo.getChoiceCD());
    rec.put("CD_MASIST", chModAsis.getChoiceCD());
    rec.put("CD_ANOEPIA", panEpidem.getCodAno());
    rec.put("CD_SEMEPIA", panEpidem.getCodSem());
    rec.put("FC_ALTA", panEpidem.getFecSem());
//System.out.println("<recoger datos>"+panEpidem.getFecSem());
    rec.put("DS_APE1", txtApel1.getText());
    rec.put("DS_APE2", txtApel2.getText());
    rec.put("DS_NOMBRE", txtNomb.getText());
    rec.put("DS_DNI", txtDNI.getText());
    rec.put("DS_TELEF", txtTelefono.getText());
    rec.put("DS_FAX", txtFax.getText());
    rec.put("FC_NAC", txtFecNac.getText());
    rec.put("CD_OPE", this.getApp().getParametro("COD_USUARIO"));
    rec.put("FC_ULTACT", "12/12/1999");
    int edDesde = 0;
    rec.put("NMREDADI", txtEdDesde.getText());
    rec.put("NMREDADF", txtEdHasta.getText());
    rec.put("DS_HORAI", txtHorDesde.getText());
    rec.put("DS_HORAF", txtHorHasta.getText());
    //cojo por defecto (aunq no est� de baja) el q aparece en pantalla
    rec.put("CD_MOTBAJA", chMotBaja.getChoiceCD());
    return rec;
  }

  public void rellenarDatos() {
    Data dtCent = new Data();
    dtCent = obtenerCentro();
    txtNotif.setText(dtDev.getCD_MEDCEN());
    pnlPuntoCent.setCodigo(dtDev);
    txtNomb.setText(dtDev.getDS_NOMBRE());
    txtDNI.setText(dtDev.getDS_DNI());
    txtApel1.setText(dtDev.getDS_APE1());
    txtApel2.setText(dtDev.getDS_APE2());
    txtFecNac.setText(dtDev.getFC_NAC());
    chSexo.selectChoiceElement(dtDev.getCD_SEXO());
    txtTelefono.setText(dtDev.getDS_TELEF());
    txtFax.setText(dtDev.getDS_FAX());
    pnlCentro.setCodigo(dtCent);
    pnlEqNotif.setCodigo(dtDev);
    chModAsis.selectChoiceElement(dtDev.getCD_MASIST());
    txtEdDesde.setText(dtDev.getNMREDADI());
    txtEdHasta.setText(dtDev.getNMREDADF());
    txtHorDesde.setText(dtDev.getDS_HORAI());
    txtHorHasta.setText(dtDev.getDS_HORAF());
    if (dtDev.getIT_BAJA().equals("S")) {

      panFecBaja = new PanFecha(this, false, dtDev.getCD_ANOBAJA(),
                                dtDev.getCD_SEMBAJA(), dtDev.getFC_BAJA());
    }
    else {
      panFecBaja = new PanFecha(this, panFecBaja.modoVACIO, false);
    }
    panFecBaja.setEnabled(false);
    chMotBaja.selectChoiceElement(dtDev.getCD_MOTBAJA());
  }

  //Prepara la pantalla para el modo de operaci�n a realizar
  public void verDatos() {

    switch (modoOperacion) {
      case modoALTA:
        panEpidem = new PanAnoSemFecha(this, panEpidem.modoSEMANA_ACTUAL, false);
        panFecBaja = new PanFecha(this, panFecBaja.modoVACIO, false);

        txtNotif.setEnabled(false);
        panFecBaja.setEnabled(false);
        chMotBaja.setEnabled(false);
        chModAsis.selectChoiceElement("1");

        break;

      case modoMODIFICACION:
        panEpidem = new PanAnoSemFecha(this, false, dtDev.getCD_ANOEPIA(),
                                       dtDev.getCD_SEMEPIA(), dtDev.getFC_ALTA());
        rellenarDatos();
        if (dtDev.getString("IT_BAJA").equals("S")) {
          this.setEnabled(false);
          btnCancelar.setEnabled(true);
        }
        else {
          txtNotif.setEnabled(false);
          chMotBaja.setEnabled(false);
          pnlPuntoCent.setEnabled(false);
        }
        break;

      case modoBAJA:

        //solo habilitados los botones de aceptar y cancelar
        panEpidem = new PanAnoSemFecha(this, false, dtDev.getCD_ANOEPIA(),
                                       dtDev.getCD_SEMEPIA(), dtDev.getFC_ALTA());
        rellenarDatos();
        if (dtDev.getString("IT_BAJA").equals("S")) {
          this.setEnabled(false);
          btnCancelar.setEnabled(true);
        }
        else {
          this.setEnabled(false);
          panEpidem.setEnabled(false);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
        }
        if (dtDev.getIT_BAJA().equals("N")) {
          panFecBaja.setEnabled(false);
          chMotBaja.setEnabled(true);
        }
        else {
          panFecBaja.setEnabled(false);
          chMotBaja.setEnabled(false);
        }
        break;
    }
  }

  //Funci�n: select cd_sexo,ds_sexo from sive_sexo
  private Lista obtenerSexo() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool qtSexo = new QueryTool();
    try {
      qtSexo.putName("SIVE_SEXO");
      qtSexo.putType("CD_SEXO", QueryTool.STRING);
      qtSexo.putType("DS_SEXO", QueryTool.STRING);
      p.addElement(qtSexo);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return p1;
  }

  boolean isRangoValid() {
    if ( (txtEdHasta.getText().trim().equals("")) &&
        (txtEdDesde.getText().trim().equals(""))) {
      return true;
    }
    else {
      if ( ( (! (txtEdHasta.getText().trim().equals(""))) &&
            (txtEdDesde.getText().trim().equals("")))
          ||
          ( (! (txtEdDesde.getText().trim().equals(""))) &&
           (txtEdHasta.getText().trim().equals("")))) {
        return false;
      }
      else {
        int edDesde = Integer.parseInt(txtEdDesde.getText().trim());
        int edHasta = Integer.parseInt(txtEdHasta.getText().trim());
        if (edHasta < edDesde) {
          return false;
        }
        else {
          return true;
        }
      } //end si ambos son blancos
    }
  }

  private Data obtenerCentro() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Data d = new Data();
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool2 qtCen = new QueryTool2();
    try {
      qtCen.putName("SIVE_E_NOTIF");

      qtCen.putType("CD_CENTRO", QueryTool.STRING);

      qtCen.putWhereType("CD_E_NOTIF", QueryTool.STRING);
      qtCen.putWhereValue("CD_E_NOTIF", dtDev.getCD_E_NOTIF());
      qtCen.putOperator("CD_E_NOTIF", "=");

      //Descripci�n del centro
      QueryTool qtAdic1 = new QueryTool();
      qtAdic1.putName("SIVE_C_NOTIF");
      qtAdic1.putType("DS_CENTRO", QueryTool.STRING);
      Data dtAdic1 = new Data();
      dtAdic1.put("CD_CENTRO", QueryTool.STRING);
      qtCen.addQueryTool(qtAdic1);
      qtCen.addColumnsQueryTool(dtAdic1);

      p.addElement(qtCen);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    d = (Data) p1.elementAt(0);
    return d;
  }

  //Funci�n: select cd_masist,ds_masist from sive_masistencia
  private Lista obtenerModAsist() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool qtModAsist = new QueryTool();
    try {
      qtModAsist.putName("SIVE_MASISTENCIA");
      qtModAsist.putType("CD_MASIST", QueryTool.STRING);
      qtModAsist.putType("DS_MASIST", QueryTool.STRING);
      p.addElement(qtModAsist);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return p1;
  }

  //Funci�n: select cd_motbaja,ds_motbaja from sive_motivo_baja
  private Lista obtenerMotBaja() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool qtMotBaja = new QueryTool();
    try {
      qtMotBaja.putName("SIVE_MOTIVO_BAJA");
      qtMotBaja.putType("CD_MOTBAJA", QueryTool.STRING);
      qtMotBaja.putType("DS_MOTBAJA", QueryTool.STRING);
      p.addElement(qtMotBaja);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return p1;
  }

  private int buscarNotificaciones() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista p1 = new Lista();
    int i = 0;

    try {
      Inicializar(CInicializar.ESPERA);
      //select cd_enfcie from sive_notif_rmc where cd_medcen=pnl
      QueryTool qtNotif = new QueryTool();
      qtNotif.putName("SIVE_NOTIF_RMC");
      qtNotif.putType("CD_MEDCEN", QueryTool.STRING);

      qtNotif.putWhereType("CD_MEDCEN", QueryTool.STRING);
      qtNotif.putWhereValue("CD_MEDCEN", dtDev.getCD_MEDCEN());
      qtNotif.putOperator("CD_MEDCEN", "=");

      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtNotif);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      i = p1.size();
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
      bAceptar = false;
      dispose();
    }
    return i;
  }

  public Data devuelveData() {
    return dtDevuelto;
  }

  boolean comprobarRangos() {
    if ( (! (txtEdDesde.getText().trim().equals(dtDev.getString("NMREDADI"))))
        || (! (txtEdHasta.getText().trim().equals(dtDev.getString("NMREDADF"))))) {
      return true;
    }
    else {
      return false;
    }
  }

  /******* JMT: Proceso de cobertura **********/
  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    // Traemos el foco al bot�n, para que se actualizen algunos paneles.
    btnAceptar.requestFocus();

    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
//  final String servlet1= "servlet/SrvMantMedNotif";
    final String servlet1 = nombreservlets.strSERVLET_MANT_MED_NOTIF; //"servlet/SrvMantMedNotif";

    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();
    boolean escribirMensaje = false;

    // Modo espera
    Inicializar(CInicializar.ESPERA);

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        if (isRangoValid()) {
          switch (modoOperacion) {

            // ----- ALTA -----------

            case 0:

              //Inicio el alta
              try {

                // Relleno del MedCent
                MedCent altaMed = new MedCent();
                altaMed = recogerDatos();
                if (comprobarMedActivo(altaMed)) {
//                  altaMed.put("FC_ALTA", Format.fechaActual());
                  altaMed.put("IT_BAJA", "N");

                  // Parametros
                  vFiltro.addElement(altaMed);
                  vFiltro.addElement(altaMed.getCD_OPE());

                  /*                   // debug
                                      SrvCobertura srv = new SrvCobertura();
                                      // par�metros jdbc
                       srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                       "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                             "sive_desa",
                                             "sive_desa");
                                      vPetic = srv.doDebug(INSMED, vFiltro);*/

                  vPetic = BDatos.ejecutaSQL(false, getApp(), srv_cobertura,
                                             INSMED, vFiltro);

                  Integer IRes = (Integer) vPetic.firstElement();
                  if (IRes.intValue() == 1) {
                    getApp().showAdvise(
                        "Se ha realizado el alta del m�dico centinela");
                    bAceptar = true;
                    dispose();
                  }
                  else if (IRes.intValue() == 2) {
                    getApp().showError(
                        "Error al realizar el alta del m�dico centinela");
                    bAceptar = false;
                    dispose();
                  }

                  bAceptar = true;
                  dispose();
                } //end comprobarMedActivo
                // error en el servlet
              }
              catch (Exception ex) {
                this.getApp().trazaLog(ex);
                this.getApp().showError(ex.getMessage());
                bAceptar = false;
                dispose();
              }
              break;

            case 1:

              // ---- MODIFICAR ----
              try {
                MedCent modifMed = new MedCent();
                modifMed = recogerDatos();
                modifMed.put("CD_MEDCEN", dtDev.getCD_MEDCEN());
                modifMed.put("IT_BAJA", dtDev.getIT_BAJA());
                modifMed.put("FC_BAJA", dtDev.getFC_BAJA());
                modifMed.put("CD_ANOBAJA", dtDev.getCD_ANOBAJA());
                modifMed.put("CD_SEMBAJA", dtDev.getCD_SEMBAJA());

                // Parametros
                vFiltro.addElement(modifMed);
                vFiltro.addElement(dtDev.getCD_ANOEPIA());
                vFiltro.addElement(dtDev.getCD_SEMEPIA());
                vFiltro.addElement(modifMed.getCD_OPE());
                vPetic = BDatos.ejecutaSQL(false, getApp(), srv_cobertura,
                                           MODMED, vFiltro);

                Integer IRes = (Integer) vPetic.firstElement();
                if (IRes.intValue() == 1) {
                  getApp().showAdvise("Se ha modificado el m�dico centinela");
                  bAceptar = true;
                  dispose();
                }
                else if (IRes.intValue() == 2) {
                  getApp().showError("Error al modificar el m�dico centinela");
                  bAceptar = false;
                  dispose();
                }

                bAceptar = true;
                dispose();
                // error en el servlet
              }
              catch (Exception ex) {
                this.getApp().trazaLog(ex);
                this.getApp().showError(ex.getMessage());
                bAceptar = false;
                dispose();
              }
              break;
            case 2:
              CMessage msgBox = new CMessage(app, CMessage.msgPREGUNTA,
                  "�Est� seguro de que desea eliminar este registro?");
              msgBox.show();

              // el usuario confirma la operaci�n
              if (msgBox.getResponse()) {

                msgBox = null;

                try {
                  MedCent bajaMed = new MedCent();
                  bajaMed = recogerDatos();
                  bajaMed.put("CD_MEDCEN", dtDev.getCD_MEDCEN());
                  bajaMed.put("IT_BAJA", "S");
                  bajaMed.put("FC_BAJA", panFecBaja.getFecSem());
                  bajaMed.put("CD_ANOBAJA", panFecBaja.getCodAno());
                  bajaMed.put("CD_SEMBAJA", panFecBaja.getCodSem());
                  bajaMed.put("CD_MOTBAJA", chMotBaja.getChoiceCD());

                  // Parametros
                  vFiltro.addElement(bajaMed);
                  vFiltro.addElement(bajaMed.getCD_OPE());

                  vPetic = BDatos.ejecutaSQL(false, getApp(), srv_cobertura,
                                             BORMED, vFiltro);

                  Integer IRes = (Integer) vPetic.firstElement();
                  if (IRes.intValue() == 1) {
                    getApp().showAdvise(
                        "Se ha ejecutado la baja del m�dico centinela");
                    bAceptar = true;
                    dispose();
                  }
                  else if (IRes.intValue() == 2) {
                    getApp().showError(
                        "Error al ejecutar la baja del m�dico centinela");
                    bAceptar = false;
                    dispose();
                  }

                  bAceptar = true;
                  dispose();

                  // error en el servlet
                }
                catch (Exception ex) {
                  this.getApp().trazaLog(ex);
                  this.getApp().showError(ex.getMessage());
                  bAceptar = false;
                  dispose();
                }
              } // De la pantallita de Si/No
              msgBox = null;
              break;
          } //end switch

        }
        else {
          this.getApp().showAdvise("Rango de edad incompleto o incorrecto");
          bAceptar = false;
        } //end if isRangoValid
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
    Inicializar(CInicializar.NORMAL);
  } //fin de btn_actionPerformed

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  //comprueba si para el pto centinela introducido hay alg�n m�dico activo
  boolean comprobarMedActivo(MedCent med) {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista p1 = new Lista();
    boolean b;

    Inicializar(CInicializar.ESPERA);
    try {
      QueryTool2 qt = new QueryTool2();
      qt = med.getMedCentActivoOfOnePuntoCent(pnlPuntoCent.getDatos().getString(
          "CD_PCENTI"));
      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() == 0) {
        b = true;
      }
      else {
        this.getApp().showAdvise(
            "Alta no realizada. Ya existe un m�dico activo para este punto centinela.");
        b = false;
      }
    }
    catch (Exception ex) {
      this.getApp().showError(ex.getMessage());
      ex.printStackTrace();
      b = false;
    }
    Inicializar(CInicializar.NORMAL);
    return b;
  } //end comprobarMedActivo

  //Valida q todos los campos obligatorios est�n rellenos.
  private boolean isDataValid() {

    // Comprobacion de campos validados
    if ( (pnlPuntoCent.getDatos() == null) || (pnlCentro.getDatos() == null)
        || (pnlEqNotif.getDatos() == null) || (chSexo.getChoiceCD() == null)
        || (chModAsis.getChoiceCD() == null) || (panEpidem.getCodAno() == null)
        || (panEpidem.getCodSem() == null)) {
      this.getApp().showAdvise("Debe rellenar todos los campos obligatorios");
      return false;
    }
    /******* JMT: Proceso de cobertura **********/

    // Comprobacion de limites de algunas fechas
    String ano;
    String sem;
    String anoUsu;
    String semUsu;

    switch (modoOperacion) {
      case modoALTA:

        // Obtenci�n de ano/sem minimo de alta epidemiologica
        try {
          Lista lQuery;
          Lista lParametros = new Lista();
          String sPCent = pnlPuntoCent.getDatos().getString("CD_PCENTI");
          lParametros.addElement(sPCent);

          lQuery = BDatos.ejecutaSQL(false, getApp(), srv_cobertura,
                                     MIN_SEMALTA_INSMED, lParametros);

          dtCoberturaAlta = (Data) lQuery.firstElement();

        }
        catch (Exception e) {
          getApp().trazaLog(e);
          getApp().showError(e.getMessage());
          bAceptar = false;
          dispose();
        }

        // Comprobacion de ano/sem introducidos con los buscados
        ano = (String) dtCoberturaAlta.get("ANO");
        sem = (String) dtCoberturaAlta.get("SEM");
        if (ano.equals("") || sem.equals("")) {
          return true;
        }
        else {
          anoUsu = panEpidem.getCodAno();
          semUsu = panEpidem.getCodSem();

          if ( (anoUsu + semUsu).compareTo(ano + sem) < 0) {
            getApp().showError("La semana de alta debe ser mayor o igual de " +
                               ano + "/" + sem);
            return false;
          }
          else {
            return true;
          }
        }
        //break;

      case modoMODIFICACION:

        // Obtenci�n de max/min ano/sem de alta epidemiologica
        try {
          Lista lQuery;
          Lista lParametros = new Lista();
          lParametros.addElement(dtDev);

          lQuery = BDatos.ejecutaSQL(false, getApp(), srv_cobertura,
                                     MAXMIN_SEMALTA_MODMED, lParametros);

          dtCoberturaMod = (Data) lQuery.firstElement();

        }
        catch (Exception e) {
          getApp().trazaLog(e);
          getApp().showError(e.getMessage());
          bAceptar = false;
          dispose();
        }

        // Comprobacion de ano/sem introducidos con los buscados
        String anoMax = (String) dtCoberturaMod.get("MAXANO");
        String semMax = (String) dtCoberturaMod.get("MAXSEM");
        String anoMin = (String) dtCoberturaMod.get("MINANO");
        String semMin = (String) dtCoberturaMod.get("MINSEM");

        anoUsu = panEpidem.getCodAno();
        semUsu = panEpidem.getCodSem();
        boolean okMin = true, okMax = true;

        if (anoMin.equals("") || semMin.equals("")) {
          okMin = true;
        }
        else if ( (anoUsu + semUsu).compareTo(anoMin + semMin) < 0) {
          getApp().showError("La semana de alta debe ser mayor o igual de " +
                             anoMin + "/" + semMin);
          return false;
        }

        if (anoMax.equals("") || semMax.equals("")) {
          okMax = true;
        }
        else if ( (anoUsu + semUsu).compareTo(anoMax + semMax) > 0) {
          getApp().showError("La semana de alta debe ser menor o igual de " +
                             anoMax + "/" + semMax);
          return false;
        }

        if (okMin && okMax) {
          return true;
        }
        else {
          return false;
        }

        //break;

      case modoBAJA:
        String sBaja = panFecBaja.getFecSem();
        if (panFecBaja.getFecSem().equals("") ||
            (chMotBaja.getChoiceCD().equals(""))) {
          this.getApp().showAdvise(
              "Debe rellenar todos los datos relacionados con la baja");
          bAceptar = false;
          return false;
        }

        // Obtenci�n de min ano/sem de baja de un medico
        try {
          Lista lQuery;
          Lista lParametros = new Lista();
          lParametros.addElement(dtDev);

          lQuery = BDatos.ejecutaSQL(false, getApp(), srv_cobertura,
                                     MIN_SEMBAJA_BORMED, lParametros);

          dtCoberturaBaja = (Data) lQuery.firstElement();

        }
        catch (Exception e) {
          getApp().trazaLog(e);
          getApp().showError(e.getMessage());
          bAceptar = false;
          dispose();
        }

        // Comprobacion de ano/sem introducidos con los buscados
        ano = (String) dtCoberturaBaja.get("ANO");
        sem = (String) dtCoberturaBaja.get("SEM");
        if (ano.equals("") || sem.equals("")) {
          return true;
        }
        else {
          anoUsu = panFecBaja.getCodAno();
          semUsu = panFecBaja.getCodSem();

          if ( (anoUsu + semUsu).compareTo(ano + sem) < 0) {
            getApp().showError("La semana de baja debe ser mayor o igual de " +
                               ano + "/" + sem);
            return false;
          }
          else {
            return true;
          }
        }
        //break;
    } // Fin switch
    return true;
  } // Fin isDataValid()

//_____IMPLEMENTACION INTERFAZ CONTPANFECHA ________________________________________________________

  /* Para obtener el applet*/
  public CApp getMiCAppFec() {
    return this.app;
  };

  /* M�todos ser�n llamados desde el PanAnoSemFecha despu�s de que el propio PanAnoSemFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAnoFec() {}

  public void alPerderFocoSemanaFec() {}

  public void alPerderFocoFechaFec() {}

//_____________________________________________________________

//_____IMPLEMENTACION INTERFAZ CONTPANANOSEMFECHA________________________________________________________

  /* Para obtener el applet*/
  public CApp getMiCApp() {
    return this.app;
  };

  /* M�todos ser�n llamados desde el PanAnoSemFecha despu�s de que el propio PanAnoSemFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAno() {}

  public void alPerderFocoSemana() {}

  public void alPerderFocoFecha() {}

//_____________________________________________________________

  void chMotBaja_itemStateChanged(ItemEvent e) {
    if (! (chMotBaja.getChoiceCD().equals(""))) {
      panFecBaja.setEnabled(true);
    }
  }

  void pnlPuntoCent_mouseEntered(MouseEvent e) {
    // enviamos el foco al apellido uno para que funcione la lupa.
    pnlPuntoCent.transferFocus();
    pnlPuntoCent.requestFocus();
  }

}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantmednotif adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMantmednotif adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

class DiaMantmednotif_chMotBaja_itemAdapter
    implements java.awt.event.ItemListener {
  DiaMantmednotif adaptee;

  DiaMantmednotif_chMotBaja_itemAdapter(DiaMantmednotif adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chMotBaja_itemStateChanged(e);
  }
}

class DiaMantmednotif_pnlPuntoCent_mouseAdapter
    extends java.awt.event.MouseAdapter {
  DiaMantmednotif adaptee;

  DiaMantmednotif_pnlPuntoCent_mouseAdapter(DiaMantmednotif adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseEntered(MouseEvent e) {
    adaptee.pnlPuntoCent_mouseEntered(e);
  }
}
