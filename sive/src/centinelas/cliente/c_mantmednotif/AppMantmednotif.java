//Applet de mantenimiento de m�dicos notificadores

package centinelas.cliente.c_mantmednotif;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantmednotif
    extends CApp
    implements CInicializar {
  PanMantmednotif pan = null;

  public AppMantmednotif() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento de m�dicos notificadores centinelas");
    pan = new PanMantmednotif(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }
}
