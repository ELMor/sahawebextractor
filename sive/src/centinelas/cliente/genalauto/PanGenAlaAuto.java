//Title:
//Title:        Generacion de alarmas automaticas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina & Luis Rivera
//Company:      Norsistemas
/*
 Description:  Panel en el cual el usuario indica el a�o para el cual desea que se generen las alarmas autom�ticas.
 Panel en el cual el usuario indica el a�o para el cual desea que se generen las alarmas
 autom�ticas. Las alarmas autom�ticas se generar�n recorriendo la lista de
 enfermedades, mostrandose al usuario un mensaje del estado del proceso de
 generaci�n
 */

package centinelas.cliente.genalauto;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.genalauto.ParGenAlaAuto;
import sapp.StubSrvBD;

public class PanGenAlaAuto
    extends CPanel {

  //ctes del panel

  //modos de consulta al servlet de alarmas
  final int servletGEN_AL_AUTO = 2;
  final int servletGEN_AL_AUTO_OPT = 20;
  ResourceBundle res;
  final int servletGEN_VEC_ENF = 8;
  final int servletADV_GEN_AL = 9;

  final int tipoCOMUNIDAD = 3;
  final int tipoNIVEL1 = 4;
  final int tipoNIVEL2 = 5;
  //rutas imagenes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/Magnify.gif"};
  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;
  //Variables
  int modoOperacion;
  int modoServlet;
  protected CLista lista = new CLista();
  //Servlet de gen alarmas

//  final String strSERVLET = "servlet/SrvGenAlaAutoCent";
  final String strSERVLET = nombreservlets.strSERVLET_GEN_ALA_AUTO_CENT;

  protected StubSrvBD stubCliente = new StubSrvBD();
  protected int modoOperacionBk;

  ButtonControl btnGenAla = new ButtonControl();
  //Escuchador
  PanGenAlaAutoActionListener btnActionListener = new
      PanGenAlaAutoActionListener(this);

  protected CCargadorImagen imgs = null;
  Panel panel1 = new Panel();
  Label lblAno = new Label();
  TextField textAno = new TextField();
  BorderLayout borderLayout1 = new BorderLayout();
  XYLayout xYLayout1 = new XYLayout();
  StatusBar statusBar1 = new StatusBar();
  BorderLayout borderLayout3 = new BorderLayout();

//______________________________________________________________________

  public PanGenAlaAuto(CApp a) {
    try {
      setApp(a);
      //modoServlet = m;

      res = ResourceBundle.getBundle("centinelas.cliente.genalauto.Res" +
                                     app.getIdioma());

      jbInit();

      if (app.getParametro("CD_ANO") != null) {
        textAno.setText(app.getParametro("CD_ANO"));
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

//___________________________________________________________________

  void jbInit() throws Exception {

    panel1.setLayout(xYLayout1);
    this.setLayout(borderLayout3);
//    this.setSize(new Dimension(517, 381));

    this.setSize(new Dimension(500, 200));

    lblAno.setText(res.getString("lblAno.Text"));

    btnGenAla.addActionListener(btnActionListener);
    btnGenAla.setLabel(res.getString("btnGenAla.Label"));

    imgs = new CCargadorImagen(app, imgNAME);
    statusBar1.setBevelOuter(BevelPanel.LOWERED);
    statusBar1.setBevelInner(BevelPanel.LOWERED);
    statusBar1.setText("");
    textAno.setBackground(new Color(255, 255, 150));
    imgs.CargaImagenes();
    btnGenAla.setImage(imgs.getImage(0));

    this.add(panel1, BorderLayout.CENTER);
    panel1.add(lblAno, new XYConstraints(27, 35, 122, -1));
    panel1.add(textAno, new XYConstraints(158, 35, 60, -1));

//    panel1.add(btnGenAla, new XYConstraints(287, 139, -1, -1));
    panel1.add(btnGenAla, new XYConstraints(230, 90, -1, -1));

    this.add(statusBar1, BorderLayout.SOUTH);

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

//___________________________________________________________________

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        btnGenAla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnGenAla.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

  }

//__________________________________________________________________________

  void btnGenAla_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    CLista data;

    CLista hayAlarmas = new CLista();
    Vector fallosEnf = new Vector();
    int tipo = 2;

    statusBar1.setText("");
    try {

      if (isDataValid() == true) {
        this.modoOperacion = modoESPERA;
        Inicializar();

        switch (app.getPerfil()) {
          case 2:
            tipo = tipoCOMUNIDAD;
            break;
        }

        // apunta al servlet principal
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

        // Para comprobar si existen e indicarlo al usuario
        modoServlet = servletADV_GEN_AL;
        data = new CLista();

        statusBar1.setText(res.getString("statusBar1.Text"));
        //   data.addElement(new ParGenAlaAuto(textAno.getText().trim(),tipo,txtCodNivel1.getText().trim(),txtCodNivel2.getText().trim()));
        data.addElement(new ParGenAlaAuto(textAno.getText().trim(), tipo));

        hayAlarmas = (CLista) stubCliente.doPost(modoServlet, data);

        /*
                 SrvGenAlaAutoCent srv= new SrvGenAlaAutoCent();
                 //Indica como conectarse a la b.datos
                  srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                 hayAlarmas = (CLista) srv.doDebug(modoServlet, data);
         */

        statusBar1.setText("");
        boolean generar = false;
        if (hayAlarmas != null) {
          Boolean hay = (Boolean) hayAlarmas.firstElement();
          //Si ya hab�a alarmas, no se generan
          if (hay.booleanValue()) {
            msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                  res.getString("msg2.Text"));
            msgBox.show();
            generar = msgBox.getResponse();
            msgBox = null;
          }
          //En caso contrartio se generan
          else {
            generar = true;
          }
        }

        if (generar) {
          generarAlarmas(textAno.getText().trim(), tipo);
        } // generar

      } //if dataValid
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg3.Text"));
        msgBox.show();
        msgBox = null;
      }
      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

//__________________________________________________________________________

  boolean generarAlarmas(String anoEpi, int tipo) throws Exception {
    CMessage msgBox;
    CLista data;
    CLista lisEnfer = new CLista();
    CLista hayAlarmas = new CLista();
    Vector enfermedades = new Vector();
    Vector enfermedad = new Vector();
    Vector fallosEnf = new Vector();

    String sTipo = new String();
    switch (tipo) {
      case tipoCOMUNIDAD:
        sTipo = " comunidad ";
        break;
    }

//__________________________________________________________________________

    // Se realiza el proceso por enfermedades
    // Primero se pide al servlet que genere la estructura
    // que contiene todos los datos
    modoServlet = servletGEN_VEC_ENF;
    data = new CLista();
//    data.addElement(new ParGenAlaAuto(anoEpi,tipo,txtCodNivel1.getText().trim(),txtCodNivel2.getText().trim()));
    data.addElement(new ParGenAlaAuto(anoEpi, tipo));

    statusBar1.setText(res.getString("statusBar2.Text"));

    lisEnfer = (CLista) stubCliente.doPost(modoServlet, data);

    /*
        SrvGenAlaAutoCent srv= new SrvGenAlaAutoCent();
        //Indica como conectarse a la b.datos
        srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                               "sive_desa",
                               "sive_desa");
        lisEnfer = (CLista) srv.doDebug(modoServlet, data);
     */

    statusBar1.setText("");

    if (lisEnfer != null) {
      Boolean error = (Boolean) lisEnfer.firstElement();

      //Si hay true en primer elemento de lista es que no hay registros o no hay a�o
      if (error.booleanValue()) {
        String sError = (String) lisEnfer.elementAt(1);
        //sError = res.getString("msg4.Text") + sTipo + sError;
        msgBox = new CMessage(this.app, CMessage.msgERROR, sError);
        msgBox.show();
        msgBox = null;
        return false;
      }

      else {
        enfermedades = (Vector) lisEnfer.elementAt(1);
        //imprimeVector(enfermedades);

//__________________________________________________________________________
// Cambios 26-04-01 (ARS), para quitar el bucle 'while'
// Se comenta la parte del c�digo antigua

//        modoServlet = servletGEN_AL_AUTO ;
        modoServlet = servletGEN_AL_AUTO_OPT;
        int numEnf = enfermedades.size();

        String cd_enf = new String();
        int indEnf = 0;
//        while (indEnf<enfermedades.size()) {
        try {
//            enfermedad = (Vector) enfermedades.elementAt(indEnf);
//            indEnf++;

//            cd_enf = (String) enfermedad.elementAt(0);

          data = new CLista();
//            data.addElement(new ParGenAlaAuto(anoEpi,enfermedad,tipo,txtCodNivel1.getText().trim(),txtCodNivel2.getText().trim()));
//            data.addElement(new ParGenAlaAuto(anoEpi,enfermedad,tipo));
          data.addElement(new ParGenAlaAuto(anoEpi, enfermedades, tipo));

          statusBar1.setText(res.getString("statusBar1.Text") + sTipo
                             + numEnf + " " + res.getString("msg7.Text"));
          // obtiene la lista
          lista = (CLista) stubCliente.doPost(modoServlet, data);

          /*   SrvGenAlaAutoCent serv= new SrvGenAlaAutoCent();
             lista = (CLista) serv.doPrueba(modoServlet, data);
             serv = null;
              SrvGenAlaAutoCent srv= new SrvGenAlaAutoCent();
              //Indica como conectarse a la b.datos
              srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                     "sive_desa",
                                     "sive_desa");
              lista = (CLista) srv.doDebug(modoServlet, data);
           */

          statusBar1.setText("");
        }
        catch (Exception e) {
          e.printStackTrace();
          fallosEnf.addElement(cd_enf);
        }
//        }// bucle while de proceso de las enfermedades

// Aqu� terminar�n los cambios del 26-04-01 (ARS)
        if (fallosEnf.size() != 0) {
          String sEnfer = new String();
          for (int i = 0; i < fallosEnf.size(); i++) {
            sEnfer = sEnfer + (String) fallosEnf.elementAt(i) + " ";
          }
          sEnfer = res.getString("msg4.Text") + sTipo
              + res.getString("msg8.Text") + sEnfer;
          msgBox = new CMessage(this.app, CMessage.msgERROR, sEnfer);
          msgBox.show();
          msgBox = null;
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg4.Text") + sTipo +
                                res.getString("msg9.Text"));
          msgBox.show();
          msgBox = null;
        }
      } // no error  (else)
    } // data != null

    return true;

  }

  //___________________________________________________________________________

  boolean isDataValid() {

    //Comprueba el a�o
    String aux = textAno.getText().trim();
    if (aux.length() != 4) {
      return false;
    }

    return true;

  }

} //clase ppal

//____________________________ CLASES  DE ESCUCHA___________________

// action listener para los botones
class PanGenAlaAutoActionListener
    implements ActionListener, Runnable {
  PanGenAlaAuto adaptee = null;
  ActionEvent e = null;

  public PanGenAlaAutoActionListener(PanGenAlaAuto adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.btnGenAla_actionPerformed(e);
  }
}
//*****************************************************
