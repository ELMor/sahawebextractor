/*
 Panel con el protocolo de un caso
 */
package centinelas.cliente.c_protocolo;

// Quitar esta l�nea al terminar de depurar.
import java.net.URL;
import java.util.Vector;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Label;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CLista;
import capp2.CApp;
import capp2.CMessage;
import capp2.CPanel;
import centinelas.cliente.c_componentes.CCargadorImagen;
import centinelas.cliente.c_comuncliente.BDatosEDO;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.cliente.c_not.DiaCasoCent;
import centinelas.datos.c_protocolo.CButtonControl;
import centinelas.datos.c_protocolo.CChoice;
import centinelas.datos.c_protocolo.CTextField;
import centinelas.datos.c_protocolo.DataEnfEntCaso;
import centinelas.datos.c_protocolo.DataLayout;
import centinelas.datos.c_protocolo.DataProtocolo;
import centinelas.datos.c_protocolo.DataResp;
import centinelas.datos.c_protocolo.DataRespCompleta;
import centinelas.datos.c_protocolo.DataRespTuber;
import centinelas.datos.c_protocolo.Nodo;
import centinelas.datos.centbasicos.NotifRMC;
import comun.constantes;
import sapp.StubSrvBD;

//Los datos de entrada: Enfermedad y niveles,
//hay que meterlos, en getEnfermedad y en IraBuscarDatos y
//Componentes(N1, N2)
//NOTA: espero que me pasen la descripcion de la Enfermedad tb
//CASO: detras de setProtocolo!!!!!
public class Panel_Informe_Completo
    extends CPanel {

  int CASTELLANO = 0;
  int PERFIL_COM_AUT = 2;

  public String sConstructor = ""; //1: EDO y Centinelas ,2: Tube ,3: Brotes
  public String CD_TSIVE = "";
//  ResourceBundle res;
  public int iTam = 0;

  final int modoESPERA = 2;

  //srvProtocolo
  final int servletNORMAL = 1;
  final int servletNORMAL_B = 2; //para brotes, con TSIVE = 'B'
  final int servletNORMAL_C = 3; //para centi, con TSIVE = 'C'
  //srvactualizarProt
  final int servletVERACTIVOS = 0;
  final int servletVERACTIVOS_B = 1;
  final int servletVERACTIVOS_C = 2;

  protected Panelprueba pprueba = null;
  protected StubSrvBD stub = null;
  protected CLista listadatos = null; //para el LAYOUT
  protected CLista listaListaValores = null; //para las listas
  protected CLista listaValores = null;
  protected CLista listaInsertar = null;
  protected Object CompPerdioFoco = new Object();

  protected boolean NoSeEncontraronDatos = false;

  protected int Num_Grupos = 0;
  protected int Num_Choices = 0;
  protected int Num_Cajas = 0;
  protected int Num_Label_Cajas = 0;
  protected int Num_Label_Choices = 0;
  protected int Num_Botones = 0;
  protected int Num_Descripciones = 0;

  //Datos de un caso
  protected String sCod = "";
  protected String sDes = "";
  protected String sNivel1 = "";
  protected String sNivel2 = "";
  protected String sCa = "";
  protected String sCaso = "";
  protected String sNotificador = "";

  protected NotifRMC notifRMC = null;

  CApp applet = null;

  DiaCasoCent dialogo = null;
  XYLayout xYLayout1 = null;

  // modos de operaci�n Servlet SrvRespEDO
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;

  //Mantenidos por compatibilidad
  final String strSERVLET_RESP = "servlet/SrvRespEDO";
  final String strSERVLET_RESP_B = "servlet/SrvRespBrotes";
  final String strSERVLET_RESP_T = "servlet/SrvRespTuber";
  final String strSERVLET_ACTIVO_OBSOLETO = "servlet/SrvActualizarProt_C";

  //Para insertar respuestas
  final String strSERVLET_RESP_C = nombreservlets.strSERVLET_RESP_CENTI;
  //Para traer protocolo de centinelas
  final String strSERVLET_COMPLETO = nombreservlets.strSERVLET_PROTOCOLO_CENT;

  final int Ancho_Panel = 550; //antes 600
  final int Ancho_GroupBox = Ancho_Panel - 5;
  final int Ancho_Label_Caja = Ancho_GroupBox / 2;

  protected Vector vDescripciones = new Vector();
  protected Vector vCNE = new Vector();
  protected Vector vCA = new Vector();
  protected Vector vN1 = new Vector();
  protected Vector vN2 = new Vector();

  //modos del panel
  final int modoCondicionadaDeshabilitar = 0;
  final int modoRevisarCondicionadas = 1;
  final int modoCargarPlantilla = 2;
  final int modoPerdidaFoco = 3;
  protected int modoOperacion = modoCondicionadaDeshabilitar;

  //Estados del panel
  final int ALTA = 0;
  final int MODIFICACION = 1;
  protected int iEstado = ALTA;

  protected Vector lblT = new Vector();
  protected Vector lblC = new Vector();
  protected Vector Des = new Vector(); //Sobretitulos
  protected Vector cbo = new Vector();
  protected Vector txt = new Vector();
  protected Vector gb = new Vector();
  protected Vector btn = new Vector();

  //clase escuchadora de perdida de foco e itemchange
  Foco_Completo objLostFocus = new Foco_Completo(this);
  Item_Completo objItemListener = new Item_Completo(this);

  //Boton para vaciar
  ButtonControl button1 = null;

  // configura los controles condicionados a InHabilitados
  public void Inicializar() {

    switch (modoOperacion) {

      case modoCondicionadaDeshabilitar:

        //CHOICES
        CChoice cboEtiqueta;
        for (int i = 0; i < Num_Choices; i++) {
          cboEtiqueta = (CChoice) cbo.elementAt(i);
          if (cboEtiqueta.getCondicionada().equals("S")) {
            cboEtiqueta.setEnabled(false);
          }
        }

        //CAJAS
        CTextField txtEtiqueta;
        for (int i = 0; i < Num_Cajas; i++) {
          txtEtiqueta = (CTextField) txt.elementAt(i);
          if (txtEtiqueta.getCondicionada().equals("S")) {
            txtEtiqueta.setEnabled(false);
            txtEtiqueta.setEditable(false);
          }
        }

        break;
      case modoRevisarCondicionadas:
        Revision();
        break;
      case modoPerdidaFoco:
        Revision_del_que_pierde_el_foco();
        break;
      case modoCargarPlantilla:
        CargarPlantilla();
        break;
    } //fin switch

  } //fin Inicializar

  // contructor
  /*public Panel_Informe_Completo(CApp a, DataProtocolo datosproto,
              PanelEDO p, DialogListaEDOIndiv dlg) {
     try  {
      sConstructor = "1"; //1: EDO 2: Tube 3: Brotes
      setApp((CApp)a);
      res = ResourceBundle.getBundle("infproto.Res" + a.getIdioma());
      applet = a;
      setProtocolo (datosproto);
      dialogo = dlg;
      pEDO = p;
      CD_TSIVE = "E";
      stub = new StubSrvBD();
      listadatos = new CLista();
      listaValores = new CLista();
      listaInsertar = new CLista();
      Num_Grupos = 0;
      Num_Choices = 0;
      Num_Cajas = 0;
      Num_Label_Cajas = 0;
      Num_Label_Choices = 0;
      Num_Botones = 0;
      Num_Descripciones = 0;
      IraBuscarDatosLayout(CD_TSIVE);//carga listadatos y listaListaValores
      xYLayout1 = new XYLayout();
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
     }    */

  /*
    // contructor
   public Panel_Informe_Completo(CApp a, DataProtocolo datosproto,
                PanelTuber p, DialogTub dlg) {
       try  {
        sConstructor = "2"; //1: EDO 2: Tube 3: Brotes
        setApp((CApp)a);
        res = ResourceBundle.getBundle("infproto.Res" + a.getIdioma());
        applet = a;
        setProtocolo (datosproto);
        dialogo = dlg;
        pTub = p;
        CD_TSIVE = "E";
        stub = new StubSrvBD();
        listadatos = new CLista();
        listaValores = new CLista();
        listaInsertar = new CLista();
        Num_Grupos = 0;
        Num_Choices = 0;
        Num_Cajas = 0;
        Num_Label_Cajas = 0;
        Num_Label_Choices = 0;
        Num_Botones = 0;
        Num_Descripciones = 0;
        IraBuscarDatosLayout(CD_TSIVE);//carga listadatos y listaListaValores
        xYLayout1 = new XYLayout();
        jbInit();
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    // contructor
    public Panel_Informe_Completo(CApp a, DataProtocolo datosproto,
                PanelBrote p, DialogInvesInfor dlg) {
       try  {
        sConstructor = "3"; //1: EDO 2: Tube 3: Brotes
        setApp((CApp)a);
        res = ResourceBundle.getBundle("infproto.Res" + a.getIdioma());
        applet = a;
        setProtocolo (datosproto);
        dialogo = dlg;
        pBrote = p;
        CD_TSIVE = "B";
        stub = new StubSrvBD();
        listadatos = new CLista();
        listaValores = new CLista();
        listaInsertar = new CLista();
        Num_Grupos = 0;
        Num_Choices = 0;
        Num_Cajas = 0;
        Num_Label_Cajas = 0;
        Num_Label_Choices = 0;
        Num_Botones = 0;
        Num_Descripciones = 0;
        IraBuscarDatosLayout(CD_TSIVE);//carga listadatos y listaListaValores
        xYLayout1 = new XYLayout();
        jbInit();
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
   */
  // contructor CENTINELAS
  public Panel_Informe_Completo(CApp a, DataProtocolo datosproto,
                                Panelprueba p, DiaCasoCent dlg) {
    try {
//      sConstructor = "2"; //1: EDO 2: Tube 3: Brotes
      sConstructor = "4"; //Centinelas
      setApp( (CApp) a);
//      res = ResourceBundle.getBundle("infproto.Res0");
      applet = a;
      setProtocolo(datosproto);
      dialogo = dlg;
      pprueba = p;

//      CD_TSIVE = "E";
//      CD_TSIVE = "B";     //  Considero brotes

      CD_TSIVE = "C";

      stub = new StubSrvBD();
      listadatos = new CLista();
      listaValores = new CLista();
      listaInsertar = new CLista();

      Num_Grupos = 0;
      Num_Choices = 0;
      Num_Cajas = 0;
      Num_Label_Cajas = 0;
      Num_Label_Choices = 0;
      Num_Botones = 0;
      Num_Descripciones = 0;

      IraBuscarDatosLayout(CD_TSIVE); //carga listadatos y listaListaValores

      xYLayout1 = new XYLayout();
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception {
    //NO HAY LAYOUT
    if (listadatos == null) {

      xYLayout1.setHeight(700);
      xYLayout1.setWidth(Ancho_Panel); //550
      this.setLayout(xYLayout1);
      this.doLayout(); //sino no sale!!

    }
    //EXISTE LAYOUT
    else {
      //layout
      //antes, pero lo dejo
      this.setLayout(xYLayout1);

      iTam = 0;
      iTam = Componentes(); //pinta el protocolo

      //Boton Reset------------------
      CCargadorImagen imgs = null;
      final String imgNAME[] = {
          "images/limpiar.gif"};

      imgs = new CCargadorImagen(applet, imgNAME);
      imgs.CargaImagenes();

      button1 = new ButtonControl();
      this.add(button1,
               new XYConstraints(40, iTam + 5, -1, -1));
      button1.setImage(imgs.getImage(0));
      button1.setLabel("Vaciar protocolo");
      button1.setActionCommand("reset");
      button1.addActionListener(new Panel_Informe_Completo_button_actionAdapter(this));
      button1.addMouseListener(new Botones_mouseAdapter(this));

      iTam = iTam + 5;
      //------------------------------

      xYLayout1.setHeight(iTam + 10 + 10 + 10);
      xYLayout1.setWidth(Ancho_Panel); //550
      this.setLayout(xYLayout1);
      this.doLayout(); //sino no sale!!

      if (sCaso.equals("")) {
        listaValores = new CLista();
      }
      else {

        //IraBuscarDatosCaso();//datos del caso (Ya lo tenemos)

        Pintar_Valores();
      }
      if (listaValores.size() == 0) {

        iEstado = ALTA;
        modoOperacion = modoCondicionadaDeshabilitar;
        Inicializar();
        this.doLayout();
      }
      else {

        iEstado = MODIFICACION;
        modoOperacion = modoCondicionadaDeshabilitar;
        Inicializar();
        modoOperacion = modoCargarPlantilla;
        Inicializar();
        this.doLayout();

      }
    }

  }

// Para el modo espera
  public void deshabilitarCampos(int cur) {

    setCursor(new Cursor(cur));
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    CButtonControl btnEtiqueta;

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      txtEtiqueta.setEnabled(false);
      txtEtiqueta.setEditable(false);
      txtEtiqueta.doLayout();
    } //cajas

    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      cboEtiqueta.setEnabled(false);
    } //choices

    for (int ind = 0; ind < Num_Botones; ind++) {
      btnEtiqueta = (CButtonControl) btn.elementAt(ind);
      btnEtiqueta.setEnabled(false);
      btnEtiqueta.doLayout();
    } //botones

    button1.setEnabled(false);
    button1.doLayout();
  }

//para volver al modo de modificacion
  public void habilitarCampos() {

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    CButtonControl btnEtiqueta;

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      txtEtiqueta.setEnabled(true);
      txtEtiqueta.setEditable(true);
      txtEtiqueta.doLayout();
    } //cajas

    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      cboEtiqueta.setEnabled(true);
    } //choices

    for (int ind = 0; ind < Num_Botones; ind++) {
      btnEtiqueta = (CButtonControl) btn.elementAt(ind);
      btnEtiqueta.setEnabled(true);
      btnEtiqueta.doLayout();
    } //cajas

    button1.setEnabled(true);
    button1.doLayout();

    modoOperacion = modoCondicionadaDeshabilitar;
    Inicializar();

    modoOperacion = modoRevisarCondicionadas;
    Inicializar();

    this.doLayout();
  }

  protected void Revision() {
    String sCD = "";
    String sNUM = "";

    Label lblCAfectado;
    CChoice cboAfectado;
    CChoice cboPadre;

    Label lblTAfectado;
    CTextField txtAfectado;
    CTextField txtPadre;

    Vector vChoice = new Vector();
    String sValorTabla = "";
    String sValorPanel = "";

    int iPadreChoice = 0;
    int iAfectado = 0;
    int iPadreText = 0;

    int indice = 0;
    //recorro los choices
    //(Combinacion: Afectado=Choice, Padre: Choice, TextField)
    for (iAfectado = 0; iAfectado < Num_Choices; iAfectado++) {
      cboAfectado = (CChoice) cbo.elementAt(iAfectado);
      //si alguno esta condicionado
      if (cboAfectado.getCondicionada().equals("S")) {

        //SU CondPadre?
        sCD = cboAfectado.getCodM();
        sNUM = cboAfectado.getNumC();
        sValorTabla = cboAfectado.getDesC(); //El afectado es un Choice

        //Busco su Padre (entre choices!!!) -------------------------------
        for (iPadreChoice = 0; iPadreChoice < Num_Choices; iPadreChoice++) {
          cboPadre = (CChoice) cbo.elementAt(iPadreChoice);
          if (cboPadre.getCodM().equals(sCD) && cboPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            sValorPanel = ValorSeleccionadoChoiceNumero(cboPadre);
            //VALOR SELECCIONADO coincide!!!!!!!!!!!!!!
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              cboAfectado.setEnabled(true);
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              cboAfectado.setEnabled(false);
              indice = cboAfectado.getSelectedIndex();
              if (indice != 0) {
                cboAfectado.select(0);
              }

            } //else
          } //fin if cd, num
        } //fin for secundario

        //Busco su Padre (entre textField)------------------------
        for (iPadreText = 0; iPadreText < Num_Cajas; iPadreText++) {
          txtPadre = (CTextField) txt.elementAt(iPadreText);
          if (txtPadre.getCodM().equals(sCD) && txtPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            if (txtPadre.getTipoPregunta().equals("N")) {

              //ya formateado,si Num, y necesito sin formato para comparar con la tabla
              sValorPanel = txtPadre.getNumSinFormato();
            }
            else {
              sValorPanel = txtPadre.getText();

              //VALOR SELECCIONADO!!!!!!!!!!!!!!
            }
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              cboAfectado.setEnabled(true);
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              cboAfectado.setEnabled(false);
              indice = cboAfectado.getSelectedIndex();

              if (indice != 0) {
                cboAfectado.select(0);
              }
            } //else
          } //fin if cd, num

        } //fin for secundario
        //------------------------------------------
      } //fin Choice Condicionado

    } //fin for general

    //recorro los TextField
    //(Combinacion: Afectado=TextField, Padre: Choice, TextField)
    for (iAfectado = 0; iAfectado < Num_Cajas; iAfectado++) {
      txtAfectado = (CTextField) txt.elementAt(iAfectado);
      //si alguno esta condicionado
      if (txtAfectado.getCondicionada().equals("S")) {

        //SU CondPadre?
        sCD = txtAfectado.getCodM();
        sNUM = txtAfectado.getNumC();
        sValorTabla = txtAfectado.getDesC(); //El afectado es un Text

        //Busco su Padre (entre choices!!!) -------------------------------
        for (iPadreChoice = 0; iPadreChoice < Num_Choices; iPadreChoice++) {
          cboPadre = (CChoice) cbo.elementAt(iPadreChoice);
          if (cboPadre.getCodM().equals(sCD) && cboPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            sValorPanel = ValorSeleccionadoChoiceNumero(cboPadre);
            //VALOR SELECCIONADO!!!!!!!!!!!!!!
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              txtAfectado.setEnabled(true);
              txtAfectado.setEditable(true);
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              txtAfectado.setEnabled(false);
              txtAfectado.setText("");
              txtAfectado.setEditable(false);

            } //else

          } //fin if cd, num

        } //fin for secundario

        //Busco su Padre (entre textField)------------------------
        for (iPadreText = 0; iPadreText < Num_Cajas; iPadreText++) {
          txtPadre = (CTextField) txt.elementAt(iPadreText);
          if (txtPadre.getCodM().equals(sCD) && txtPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            if (txtPadre.getTipoPregunta().equals("N")) {

              //ya formateado,si Num, y necesito sin formato para comparar con la tabla
              sValorPanel = txtPadre.getNumSinFormato();
            }
            else {
              sValorPanel = txtPadre.getText();
              //VALOR SELECCIONADO!!!!!!!!!!!!!!
            }
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              txtAfectado.setEnabled(true);
              txtAfectado.setEditable(true);
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              txtAfectado.setEnabled(false);
              txtAfectado.setText("");
              txtAfectado.setEditable(false);
            } //else
          } //fin if cd, num

        } //fin for secundario
        //------------------------------------------
      } //fin text Condicionado

    } //fin for general

  }

  protected void Revision_del_que_pierde_el_foco() {
    String sCD = "";
    String sNUM = "";

    Label lblCAfectado;
    CChoice cboAfectado;
    CChoice cboPadre;

    Label lblTAfectado;
    CTextField txtAfectado;
    CTextField txtPadre;

    Vector vChoice = new Vector();
    String sValorTabla = "";
    String sValorPanel = "";

    int iPadreChoice = 0;
    int iAfectado = 0;
    int iPadreText = 0;

    int indice = 0;

    Object CompPerdioFocoPrimero = new Object();
    boolean b = false;

    //recorro los choices
    //(Combinacion: Afectado=Choice, Padre: Choice, TextField)
    for (iAfectado = 0; iAfectado < Num_Choices; iAfectado++) {
      cboAfectado = (CChoice) cbo.elementAt(iAfectado);
      //si alguno esta condicionado
      if (cboAfectado.getCondicionada().equals("S")) {

        //SU CondPadre?
        sCD = cboAfectado.getCodM();
        sNUM = cboAfectado.getNumC();
        sValorTabla = cboAfectado.getDesC(); //El afectado es un Choice

        //Busco su Padre (entre choices!!!) -------------------------------
        for (iPadreChoice = 0; iPadreChoice < Num_Choices; iPadreChoice++) {
          cboPadre = (CChoice) cbo.elementAt(iPadreChoice);
          if (cboPadre.getCodM().equals(sCD) && cboPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            sValorPanel = ValorSeleccionadoChoiceNumero(cboPadre);
            //VALOR SELECCIONADO coincide!!!!!!!!!!!!!!
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              if (cboPadre.isEnabled()) {
                cboAfectado.setEnabled(true);

              }
              if (CompPerdioFoco.equals(cboPadre)) {

                if (!b) {
                  b = true;
                  CompPerdioFocoPrimero = cboAfectado;
                }
              }
            }

            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              cboAfectado.setEnabled(false);
              indice = cboAfectado.getSelectedIndex();
              if (indice != 0) {
                cboAfectado.select(0);
              }

            } //else
          } //fin if cd, num
        } //fin for secundario

        //Busco su Padre (entre textField)------------------------
        for (iPadreText = 0; iPadreText < Num_Cajas; iPadreText++) {
          txtPadre = (CTextField) txt.elementAt(iPadreText);
          if (txtPadre.getCodM().equals(sCD) && txtPadre.getNum().equals(sNUM)) {
            //Lo he encontrado

            if (txtPadre.getTipoPregunta().equals("N")) {

              //ya formateado,si Num, y necesito sin formato para comparar con la tabla
              sValorPanel = txtPadre.getNumSinFormato();
            }
            else {
              sValorPanel = txtPadre.getText();

              //VALOR SELECCIONADO!!!!!!!!!!!!!!
            }
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              if (txtPadre.isEnabled()) {
                cboAfectado.setEnabled(true);
              }
              if (CompPerdioFoco.equals(txtPadre)) {

                if (!b) {
                  b = true;
                  CompPerdioFocoPrimero = cboAfectado;
                }
              }
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              cboAfectado.setEnabled(false);
              indice = cboAfectado.getSelectedIndex();

              if (indice != 0) {
                cboAfectado.select(0);
              }

            } //else
          } //fin if cd, num

        } //fin for secundario
        //------------------------------------------
      } //fin Choice Condicionado

    } //fin for general

    //recorro los TextField
    //(Combinacion: Afectado=TextField, Padre: Choice, TextField)
    for (iAfectado = 0; iAfectado < Num_Cajas; iAfectado++) {
      txtAfectado = (CTextField) txt.elementAt(iAfectado);
      //si alguno esta condicionado
      if (txtAfectado.getCondicionada().equals("S")) {

        //SU CondPadre?
        sCD = txtAfectado.getCodM();
        sNUM = txtAfectado.getNumC();
        sValorTabla = txtAfectado.getDesC(); //El afectado es un Text

        //Busco su Padre (entre choices!!!) -------------------------------
        for (iPadreChoice = 0; iPadreChoice < Num_Choices; iPadreChoice++) {
          cboPadre = (CChoice) cbo.elementAt(iPadreChoice);
          if (cboPadre.getCodM().equals(sCD) && cboPadre.getNum().equals(sNUM)) {
            //Lo he encontrado

            sValorPanel = ValorSeleccionadoChoiceNumero(cboPadre);
            //VALOR SELECCIONADO!!!!!!!!!!!!!!
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              if (cboPadre.isEnabled()) {
                txtAfectado.setEnabled(true);
              }
              if (cboPadre.isEnabled()) {
                txtAfectado.setEditable(true);
              }
              if (CompPerdioFoco.equals(cboPadre)) {

                if (!b) {
                  b = true;
                  CompPerdioFocoPrimero = txtAfectado;
                }
              }
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              txtAfectado.setEnabled(false);
              txtAfectado.setText("");
              txtAfectado.setEditable(false);

            } //else

          } //fin if cd, num

        } //fin for secundario

        //Busco su Padre (entre textField)------------------------
        for (iPadreText = 0; iPadreText < Num_Cajas; iPadreText++) {
          txtPadre = (CTextField) txt.elementAt(iPadreText);
          if (txtPadre.getCodM().equals(sCD) && txtPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            if (txtPadre.getTipoPregunta().equals("N")) {

              //ya formateado,si Num, y necesito sin formato para comparar con la tabla
              sValorPanel = txtPadre.getNumSinFormato();
            }
            else {
              sValorPanel = txtPadre.getText();
              //VALOR SELECCIONADO!!!!!!!!!!!!!!
            }
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              if (txtPadre.isEnabled()) {
                txtAfectado.setEnabled(true);
              }
              if (txtPadre.isEnabled()) {
                txtAfectado.setEditable(true);

              }
              if (CompPerdioFoco.equals(txtPadre)) {

                if (!b) {
                  b = true;
                  CompPerdioFocoPrimero = txtAfectado;
                }
              }
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              txtAfectado.setEnabled(false);
              txtAfectado.setText("");
              txtAfectado.setEditable(false);

            } //else
          } //fin if cd, num

        } //fin for secundario
        //------------------------------------------
      } //fin text Condicionado

    } //fin for general
    if (b) {
      if (CompPerdioFocoPrimero.getClass().getName().equals(
          "centinelas.datos.c_protocolo.CTextField")) {
        CTextField txtPrim = (CTextField) CompPerdioFocoPrimero;
        txtPrim.requestFocus();
      }
      if (CompPerdioFocoPrimero.getClass().getName().equals(
          "centinelas.datos.c_protocolo.CChoice")) {
        CChoice cboPrim = (CChoice) CompPerdioFocoPrimero;
        cboPrim.requestFocus();
      }
    }
  }

  protected void CargarPlantilla() {

    String CodModel = "";
    String NumLin = "";
    String CodPreg = ""; //por si acaso
    String Nivel = "";
    String it_ok = "";
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    CButtonControl btnEtiqueta;

    //botones
    for (int i = 0; i < listaValores.size(); i++) {
      DataRespCompleta datosbotones = (DataRespCompleta) listaValores.elementAt(
          i);
      if (!datosbotones.getCodModelo().equals(CodModel)) {

        CodModel = datosbotones.getCodModelo();
        Nivel = datosbotones.getNivel();
        it_ok = datosbotones.getIT_OK();

        if (it_ok.equals("N")) { //modelo con protocolo obsoleto
          //buscamos su boton
          for (int j = 0; j < Num_Botones; j++) {
            CButtonControl btnBoton = (CButtonControl) btn.elementAt(j);
            if (btnBoton.getNivel().equals(Nivel)) {

              /*
               Descomentado y puesto visible el bot�n de actualizar protocolo
                   el 29-11-2000 para hacer pruebas. Copia de seguridad hecha del paquete.
               */

              /*         CODIGO DE MOMENTO INHABILITADO */
              btnBoton.setActionCommand(Nivel);
              btnBoton.addActionListener(new
                  Panel_Informe_Completo_button_actionAdapter(this));
              btnBoton.addMouseListener(new Botones_mouseAdapter(this));
              btnBoton.setVisible(true);
              btnBoton.setCodModelo(CodModel);
              /**/
              btnBoton.setVisible(true); //Se HACE INVISIBLE BOTON HASTA QUE SE IMPLEMENTE
              break;
            }
          }
        } //if
      } //if
    }

    for (int i = 0; i < listaValores.size(); i++) {
      DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);

      //busco ese registro entre las cajas
      for (int ind = 0; ind < Num_Cajas; ind++) {
        txtEtiqueta = (CTextField) txt.elementAt(ind);

        if (txtEtiqueta.getCodM().trim().equals(dat.getCodModelo().trim()) &&
            txtEtiqueta.getNum().trim().equals(dat.getNumLinea().trim()) &&
            txtEtiqueta.getCodPregunta().trim().equals(dat.getCodPregunta().
            trim())) {
          txtEtiqueta.setText(dat.getDesPregunta().trim());
          //simular que pierde el foco!!(y me despreocupo de Habilitados..., y formatos)
          //Gained, no hace falta, porque los num se graban desformateados
          objfocusLost(new FocusEvent( (Component) txtEtiqueta,
                                      java.awt.event.FocusEvent.FOCUS_LOST));
        }
      } //cajas
      //busco ese registro entre los choices
      for (int ind = 0; ind < Num_Choices; ind++) {
        cboEtiqueta = (CChoice) cbo.elementAt(ind);

        if (cboEtiqueta.getCodM().trim().equals(dat.getCodModelo().trim()) &&
            cboEtiqueta.getNum().trim().equals(dat.getNumLinea().trim()) &&
            cboEtiqueta.getCodPregunta().trim().equals(dat.getCodPregunta().
            trim())) {

          cboEtiqueta.CargarVector(dat.getValorLista(), dat.getDesPregunta());
          //simular que pierde el foco!! (y me despreocupo de Habilitados...)
          objfocusLost(new FocusEvent( (Component) cboEtiqueta,
                                      java.awt.event.FocusEvent.FOCUS_LOST));
        }
      } //choices

    } //fin for

    LostGained();

  }

  protected void LostGained() {

    CTextField txtEtiqueta;
    CChoice cboEtiqueta;

    //bucle de perdida de foco,
    //para vaciar datos Condicionados que fueran incoherentes.
    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      objfocusGained(new FocusEvent( (Component) txtEtiqueta,
                                    java.awt.event.FocusEvent.FOCUS_GAINED));
      objfocusLost(new FocusEvent( (Component) txtEtiqueta,
                                  java.awt.event.FocusEvent.FOCUS_LOST));
    }
    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      objfocusGained(new FocusEvent( (Component) cboEtiqueta,
                                    java.awt.event.FocusEvent.FOCUS_GAINED));
      objfocusLost(new FocusEvent( (Component) cboEtiqueta,
                                  java.awt.event.FocusEvent.FOCUS_LOST));
    }
  }

//Tama�o de las cajas de texto (N, C)
//(Max = Ancho_Label_Caja) (Min = 35)
  protected int MaxMin(String Tipo, int longi, int ente, int deci) {
    int tam = 0;
    if (Tipo.equals("C")) {
      tam = (150 / 20) * longi;
      if (tam > Ancho_Label_Caja) {
        tam = Ancho_Label_Caja;
      }
      if (tam < 35) {
        tam = 35;
      }
    }
    else if (Tipo.equals("N")) {
      tam = (150 / 20) * (LongNumero(ente, deci) + 3);
      if (tam > Ancho_Label_Caja) {
        tam = Ancho_Label_Caja;
      }
      if (tam < 35) {
        tam = 35;
      }
    }
    return (tam);
  }

//Valor guardado en el vector para esa Choice y el indice selected
//return : "" o "H"
  protected String ValorSeleccionadoChoiceNumero(CChoice cboP) {

    String sValor = "";
    Vector vChoice = new Vector();
    int ind = 0;
    CMessage msgBox = null;
    boolean b = true;

    try {

      vChoice = (Vector) cboP.getVectorCodLista();
      //siempre tiene un blanco, ptt el tama�o es 1
      if (vChoice.size() == 1 || vChoice.size() < 1) {
        b = false; //en realidad, como si no tuviera datos

      }
      else {
        ind = cboP.getSelectedIndex();
        if (ind != 0 && ind != -1) {
          sValor = (String) vChoice.elementAt(ind);
        }
        else {
          sValor = ""; //retorna un vacio
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (b) {
      return (sValor.trim()); //si esta seleccionado el 0, devuelve ""
    }
    else {
      return ("-1"); //caso de error
    }
  }

//LRG: Busca datos para pintar el protocolo correspondeinte al TSIVE indicado
// y a los datos de enrfermedad,CAut, nivel 1, nivel2 y notificaci�n ya indicados en
// variables sCod, sCa, sNivel1, sNivel2, sCaso,notifRMC
//(Adaptado solo funciona para centinelas)
  protected void IraBuscarDatosLayout(String TSive) {
    CMessage msgBox = null;
    CLista ListaEntrada = null;
    listadatos = null;
    listaValores = null;

    DataEnfEntCaso datosEntrada = null;

    try {

      datosEntrada = new DataEnfEntCaso(sCod, sCa, sNivel1, sNivel2, sCaso,
                                        notifRMC);

      ListaEntrada = new CLista();
      ListaEntrada.addElement(datosEntrada);

      CLista listaSalida = new CLista();

      ListaEntrada.setIdioma(CASTELLANO);
      ListaEntrada.setLogin(this.app.getParametro("CD_USUARIO"));
      ListaEntrada.setPerfil(PERFIL_COM_AUT);

      stub.setUrl(new URL(app.getParametro("URL_SERVLET") + strSERVLET_COMPLETO));

      if (TSive.trim().equals("E")) {
        listaSalida = (CLista) stub.doPost(servletNORMAL, ListaEntrada);
      }
      else if (TSive.trim().equals("B")) {
        listaSalida = (CLista) stub.doPost(servletNORMAL_B, ListaEntrada);
      }
      else if (TSive.trim().equals("C")) {
        listaSalida = BDatosEDO.ejecutaSQL(false, app, strSERVLET_COMPLETO,
                                           servletNORMAL_C, ListaEntrada);

      }

      ListaEntrada = null;

      //listaSalida: La de resp y la de layout
      //si null, sale por catch  (error)
      if (listaSalida != null) {
        listaValores = (CLista) listaSalida.elementAt(0);
        listadatos = (CLista) listaSalida.elementAt(1);
      }

      if (listadatos == null) {
        NoSeEncontraronDatos = true;
      }

      /*//controlar que el primer tipo debe ser descripcion
           else{
        //leo
        for (int indice=0; indice < listadatos.size(); indice++) {
          DataLayout datosSalida = (DataLayout)listadatos.elementAt(indice);
        }
           } */
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

//______________________________________________________________________
//LRG : Pinta el protocolo
  protected int Componentes() {

    Label lblTexto;
    Label lblChoice;
    Label DesEtiqueta; //SobreTitulos: Cne, ca, nivel1, nivel2
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    GroupBox gbEtiqueta;
    CButtonControl btnEtiqueta;

    String sAlmacenDescGrupo = "";
    String sAlmTipo = "";
    String sDesX = "";
    int iTamLista = listadatos.size();
    int indice = 0;

    int iGrupo = 0;
    int iLabelChoice = 0;
    int iLabelTexto = 0;
    int iCaja = 0;
    int iChoice = 0;
    int iBotones = 0;
    int iDescripciones = 0;

    int xObj = 20;
    int xGrupo = 10;
    int MGO = 15; // entre grupo-obj
    int MG = 5; // entre grupos  y para el marco dowm
    int M = 1; //entre objetos
    int Alto = 30; // alto de los objetos

    int y = 0; //donde estamos
    int yUp = 0;
    int yDown = 0;

    CCargadorImagen imgs = null;
    final String imgNAME_BOTONES[] = {
        "images/refrescar.gif"};

    imgs = new CCargadorImagen(applet, imgNAME_BOTONES);
    imgs.CargaImagenes();

    //recorremos la listadatos
    for (indice = 0; indice < iTamLista; indice++) {
      DataLayout datUnaLinea = (DataLayout) listadatos.elementAt(indice);

      //DESCRIPCION
      if (datUnaLinea.getTipoPreg().equals("X") &&
          indice == 0) {

        y = y + MGO; //situarnos (con un margen)
        //label
        Des.addElement(new Label());
        DesEtiqueta = (Label) Des.elementAt(iDescripciones);
        this.add(DesEtiqueta,
                 new XYConstraints(xObj, y, Ancho_GroupBox - 150, -1));

        DesEtiqueta.setAlignment(Label.LEFT);

        DesEtiqueta.setFont(new Font("Dialog", 1, 12));
        DesEtiqueta.setForeground(Color.white);
        DesEtiqueta.setBackground(SystemColor.activeCaption);

        if (datUnaLinea.getDesTexto().equals("CNE")) {
          DesEtiqueta.setText(" " + "Centro Nacional de Epidemiolog�a");
          sDesX = "CNE";

          //---- boton
          btn.addElement(new CButtonControl(null, "CNE"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel("Actualizar protocolo");
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----

        }
        else if (datUnaLinea.getDesTexto().equals("CA")) {
          DesEtiqueta.setText(" " + "Comunidad Aut�noma");
          sDesX = "CA";
          //---- boton
          btn.addElement(new CButtonControl(null, "CA"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel("Actualizar protocolo");
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("N1")) {
          DesEtiqueta.setText(" " + this.app.getParametro("NIVEL1") + " " +
                              datUnaLinea.getNIVEL_1());
          sDesX = "N1";
          //---- boton
          btn.addElement(new CButtonControl(null, "N1"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel("Actualizar protocolo");
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("N2")) {
          DesEtiqueta.setText(" " + this.app.getParametro("NIVEL2") + " " +
                              datUnaLinea.getNIVEL_2());
          sDesX = "N2";
          //---- boton
          btn.addElement(new CButtonControl(null, "N2"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel("Actualizar protocolo");
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        //arbol descripciones
        vDescripciones.addElement
            (new Nodo(DesEtiqueta.getText(), y));

        iDescripciones++;
        sAlmTipo = "X";
        y = y + Alto; //lo que ocupa la descripcion

      }
      else if (datUnaLinea.getTipoPreg().equals("X") &&
               indice != 0) {

        //acaba el groupBox Anterior****
        //pintar grupo anterior--------
        gb.addElement(new GroupBox());
        gbEtiqueta = (GroupBox) gb.elementAt(iGrupo);
        gbEtiqueta.setLabel(sAlmacenDescGrupo);
        gbEtiqueta.setFont(new Font("Dialog", 1, 12));
        gbEtiqueta.setForeground(new Color(153, 0, 0));
        yDown = y + MG; //le ponemos el peque�o
        this.add(gbEtiqueta,
                 new XYConstraints(xGrupo, yUp, Ancho_GroupBox, yDown - yUp));

        //arbol de detalle-----
        if (sDesX.equals("CNE")) {
          vCNE.addElement
              (new Nodo(sAlmacenDescGrupo, yUp));
        }
        else if (sDesX.equals("CA")) {
          vCA.addElement
              (new Nodo(sAlmacenDescGrupo, yUp));
        }
        else if (sDesX.equals("N1")) {
          vN1.addElement
              (new Nodo(sAlmacenDescGrupo, yUp));
        }
        else if (sDesX.equals("N2")) {
          vN2.addElement
              (new Nodo(sAlmacenDescGrupo, yUp));
        }
        //------------------
        iGrupo++;
        y = yDown;
        sAlmTipo = "X";

        //pinta descripcion*****
        y = y + MGO; //situarnos (con un margen)
        //label
        Des.addElement(new Label());
        DesEtiqueta = (Label) Des.elementAt(iDescripciones);
        this.add(DesEtiqueta,
                 new XYConstraints(xObj, y, Ancho_GroupBox - 150, -1));
        DesEtiqueta.setAlignment(Label.LEFT);

        DesEtiqueta.setFont(new Font("Dialog", 1, 12));
        DesEtiqueta.setForeground(Color.white);
        DesEtiqueta.setBackground(SystemColor.activeCaption);

        if (datUnaLinea.getDesTexto().equals("CNE")) {
          DesEtiqueta.setText(" " + "Centro Nacional de Epidemiolog�a");
          sDesX = "CNE";
          //---- boton
          btn.addElement(new CButtonControl(null, "CNE"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel("Actualizar protocolo");
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("CA")) {
          DesEtiqueta.setText(" " + "Comunidad Aut�noma");
          sDesX = "CA";
          //---- boton
          btn.addElement(new CButtonControl(null, "CA"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel("Actualizar protocolo");
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("N1")) {
          DesEtiqueta.setText(" " + this.app.getParametro("NIVEL1") + " " +
                              datUnaLinea.getNIVEL_1());
          sDesX = "N1";
          //---- boton
          btn.addElement(new CButtonControl(null, "N1"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel("Actualizar protocolo");
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("N2")) {
          DesEtiqueta.setText(" " + this.app.getParametro("NIVEL2") + " " +
                              datUnaLinea.getNIVEL_2());
          sDesX = "N2";
          //---- boton
          btn.addElement(new CButtonControl(null, "N2"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel("Actualizar protocolo");
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        //arbol descripciones
        vDescripciones.addElement
            (new Nodo(DesEtiqueta.getText(), y));

        iDescripciones++;
        y = y + Alto;

      }
      //GRUPO primero. Inicia un GroupBox
      else if (datUnaLinea.getTipoPreg().equals("D") &&
               indice == 1) { //primer grupo  (X, D)

        sAlmacenDescGrupo = datUnaLinea.getDesTexto();
        sAlmTipo = "";
        y = y + MG;
        yUp = y;

      }
      //Grupo Detras de una Pregunta pppDpp: Acaba e inicia otro GroupBox
      else if (datUnaLinea.getTipoPreg().equals("D") &&
               indice != 1 &&
               (sAlmTipo.equals("L") || sAlmTipo.equals("C"))) {

        //pintar grupo anterior--------
        gb.addElement(new GroupBox());
        gbEtiqueta = (GroupBox) gb.elementAt(iGrupo);
        gbEtiqueta.setLabel(sAlmacenDescGrupo);
        gbEtiqueta.setFont(new Font("Dialog", 1, 12));
        gbEtiqueta.setForeground(new Color(153, 0, 0));
        yDown = y + MG; //le ponemos el peque�o
        this.add(gbEtiqueta,
                 new XYConstraints(xGrupo, yUp, Ancho_GroupBox, yDown - yUp));

        //arbol de detalle-----
        if (sDesX.equals("CNE")) {
          vCNE.addElement
              (new Nodo(sAlmacenDescGrupo, yUp));
        }
        else if (sDesX.equals("CA")) {
          vCA.addElement
              (new Nodo(sAlmacenDescGrupo, yUp));
        }
        else if (sDesX.equals("N1")) {
          vN1.addElement
              (new Nodo(sAlmacenDescGrupo, yUp));
        }
        else if (sDesX.equals("N2")) {
          vN2.addElement
              (new Nodo(sAlmacenDescGrupo, yUp));
        }
        //------------------

        iGrupo++;
        y = yDown;
        //-----------------------------
        yUp = yDown + MG;
        y = yUp;
        sAlmacenDescGrupo = datUnaLinea.getDesTexto();
        sAlmTipo = "";
      }

      //Grupo Detras de una Decripcion. Inicia un GroupBox
      else if (datUnaLinea.getTipoPreg().equals("D") &&
               indice != 1 && sAlmTipo.equals("X")) {

        yUp = y + 1; //en vez de MG pongo 1
        y = yUp;
        sAlmacenDescGrupo = datUnaLinea.getDesTexto();
        sAlmTipo = "";

      }
//!!!!!!!!!!!!!
//CAJA!!!!!!!!!
//!!!!!!!!!!!!!
      else if (datUnaLinea.getTipoPreg().equals("C") ||
               datUnaLinea.getTipoPreg().equals("N") ||
               datUnaLinea.getTipoPreg().equals("F")) {

        //si acaba de darse un groupBox: MGO, si era otra caja M
        if (sAlmTipo.equals("")) {
          y = y + MGO;
        }
        else {
          y = y + M;

          //label
        }
        lblT.addElement(new Label());
        lblTexto = (Label) lblT.elementAt(iLabelTexto);
        this.add(lblTexto,
                 new XYConstraints(xObj, y, Ancho_Label_Caja - 20, -1));
        lblTexto.setAlignment(Label.RIGHT);
        lblTexto.setText(datUnaLinea.getDesTexto() + " : ");
        iLabelTexto++; //contador = cajas

        //caja
        //datos int
        int ilon = 0;
        int idec = 0;
        int ient = 0;
        if (datUnaLinea.getLongitud() != null) {
          if (!datUnaLinea.getLongitud().trim().equals("")) {
            Integer Lon = new Integer(datUnaLinea.getLongitud());
            ilon = Lon.intValue();
          }
        }
        if (datUnaLinea.getDecimal() != null) {
          if (!datUnaLinea.getDecimal().trim().equals("")) {
            Integer Dec = new Integer(datUnaLinea.getDecimal());
            idec = Dec.intValue();
          }
        }
        if (datUnaLinea.getEntera() != null) {
          if (!datUnaLinea.getEntera().trim().equals("")) {
            Integer Ent = new Integer(datUnaLinea.getEntera());
            ient = Ent.intValue();
          }
        }

        txt.addElement(new CTextField(datUnaLinea.getOblig(),
                                      datUnaLinea.getTipoPreg(),
                                      ilon, ient, idec,
                                      datUnaLinea.getCondicionada(),
                                      datUnaLinea.getCodModelo(),
                                      datUnaLinea.getNumLinea(),
                                      datUnaLinea.getNumLineaCond(),
                                      datUnaLinea.getDesPreguntaCond(),
                                      datUnaLinea.getCodPregunta()));

        txtEtiqueta = (CTextField) txt.elementAt(iCaja);

        //(150 pixeles, 20 caracteres) TAMA�O
        if (datUnaLinea.getTipoPreg().equals("C")) {
          this.add(txtEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y,
                                     (MaxMin("C", ilon, ient, idec)), -1));
        }
        else if (datUnaLinea.getTipoPreg().equals("F")) {
          this.add(txtEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y, ( (160 / 20) * 12),
                                     -1));
        }
        else if (datUnaLinea.getTipoPreg().equals("N")) {
          this.add(txtEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y,
                                     (MaxMin("N", ilon, ient, idec)), -1));
        }
        //escuchador
        txtEtiqueta.addFocusListener(objLostFocus);
        iCaja++;

        sAlmTipo = "C";
        y = y + Alto;

      }
//!!!!!!!!!!!!!
//LISTA!!!!!!!!
//!!!!!!!!!!!!!
      else if (datUnaLinea.getTipoPreg().equals("B") ||
               datUnaLinea.getTipoPreg().equals("L")) {
        //si acaba de darse una descripcion: MGO, si era otra caja M
        if (sAlmTipo.equals("")) {
          y = y + MGO;
        }
        else {
          y = y + M;

          //label ---LA Y LA MARCAMOS CON LAS LABELS
        }
        lblC.addElement(new Label());
        lblChoice = (Label) lblC.elementAt(iLabelChoice);
        this.add(lblChoice,
                 new XYConstraints(xObj, y, Ancho_Label_Caja - 20, -1));
        lblChoice.setAlignment(Label.RIGHT);
        lblChoice.setText(datUnaLinea.getDesTexto() + " : ");
        iLabelChoice++;
        //Lista
        cbo.addElement(new CChoice(app,
                                   datUnaLinea.getOblig(),
                                   datUnaLinea.getCodLista(),
                                   datUnaLinea.getCondicionada(),
                                   datUnaLinea.getCodModelo(),
                                   datUnaLinea.getNumLinea(),
                                   datUnaLinea.getNumLineaCond(),
                                   datUnaLinea.getDesPreguntaCond(),
                                   datUnaLinea.getCodPregunta(),
                                   datUnaLinea.getVALORES()));

        cboEtiqueta = (CChoice) cbo.elementAt(iChoice);
        //TAMA�O
        if (datUnaLinea.getTipoPreg().equals("B")) {
          this.add(cboEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y, (150 / 20) * 10, -1));

        }
        else {
          this.add(cboEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y, Ancho_Label_Caja, -1));
        }
        //escuchador
        cboEtiqueta.addFocusListener(objLostFocus);
        cboEtiqueta.addItemListener(objItemListener);
        iChoice++;

        sAlmTipo = "L";
        y = y + Alto;
      }

    } //listadatos

    //El ultimo Grupo
    gb.addElement(new GroupBox());
    gbEtiqueta = (GroupBox) gb.elementAt(iGrupo); //me situo en el que quiero
    gbEtiqueta.setLabel(sAlmacenDescGrupo);
    gbEtiqueta.setFont(new Font("Dialog", 1, 12)); //ESTE ERA!!!
    gbEtiqueta.setForeground(new Color(153, 0, 0));
    yDown = y + MG; //le ponemos el peque�o
    this.add(gbEtiqueta,
             new XYConstraints(xGrupo, yUp, Ancho_GroupBox, yDown - yUp));
    //arbol de detalle-----
    if (sDesX.equals("CNE")) {
      vCNE.addElement
          (new Nodo(sAlmacenDescGrupo, yUp));
    }
    else if (sDesX.equals("CA")) {
      vCA.addElement
          (new Nodo(sAlmacenDescGrupo, yUp));
    }
    else if (sDesX.equals("N1")) {
      vN1.addElement
          (new Nodo(sAlmacenDescGrupo, yUp));
    }
    else if (sDesX.equals("N2")) {
      vN2.addElement
          (new Nodo(sAlmacenDescGrupo, yUp));
    }
    //------------------

    y = yDown;

    //llenamos los totales
    Num_Grupos = iGrupo;
    Num_Choices = iChoice;
    Num_Cajas = iCaja;
    Num_Descripciones = iDescripciones;
    Num_Label_Cajas = iLabelTexto;
    Num_Label_Choices = iLabelChoice;
    Num_Botones = iBotones;

    return y;

  }

//Calcula la longitud permitida del numero (con formato)
  protected int LongNumero(int iEnt, int iDec) {

    int iLen = 0;
    int iResto = 0;
    int iDiv = 0;
    int iNumPtos = 0;

    if (iEnt == 0) { //N(0,?)
      iLen = 0;
    }
    else {
      iDiv = (int) (iEnt / 3);
      iResto = iEnt % 3;

      if (iResto == 0) {
        iNumPtos = iDiv - 1;
      }
      else {
        iNumPtos = iDiv;
      }
    }
    if (iDec == 0) {
      iLen = iEnt + iNumPtos;
    }
    else {
      iLen = iEnt + iNumPtos + 1 + iDec;

    }
    return (iLen);
  }

//PERDIDA DE FOCO**********
  public void objfocusLost(FocusEvent e) {

    CompPerdioFoco = e.getComponent();
    if (CompPerdioFoco.getClass().getName().equals(
        "centinelas.datos.c_protocolo.CChoice")) {
      return;
    }

    if (CompPerdioFoco.getClass().getName().equals(
        "centinelas.datos.c_protocolo.CTextField")) {
      CTextField txtAnalizado = (CTextField) CompPerdioFoco;
      String Tipo = txtAnalizado.getTipoPregunta();
      //CARACTER
      if (Tipo.equals("C")) {
        //nada especial
      }
      //FECHA
      if (Tipo.equals("F")) {
        txtAnalizado.ValidarFecha(); //validamos el dato
        if (txtAnalizado.getValid().equals("N")) {
          //opcional vaciarlo
          txtAnalizado.setText(txtAnalizado.getFecha());
          //opcional si obligatorio
          //Cfecha.selectAll();
          //Cfecha.requestFocus();
        }
        else if (txtAnalizado.getValid().equals("S")) {
          txtAnalizado.setText(txtAnalizado.getFecha());
        }

      }
      //NUMERICO
      if (Tipo.equals("N")) {
        String Alm = txtAnalizado.getText().trim(); //antes de formatear
        if (!Alm.equals("")) {
          txtAnalizado.FormatNumero();
        }
        else { //vacio
          txtAnalizado.ResetsNumSinFormato(); //a vacio!
        }
      }
      //Si llamo a FormatNumero, esta cargado NumSinFormato.
      //Si no lo llamo, NumSinFormato =""

    } //fin de text

    modoOperacion = modoPerdidaFoco;
    this.Inicializar();
  }

//cambiar de item en las choices
  public void objitemchange(ItemEvent e) {

    modoOperacion = modoPerdidaFoco;
    this.Inicializar();

  }

//Ganar Foco
  public void objfocusGained(FocusEvent e) {

    if (!e.getComponent().isEnabled()) {
      e.getComponent().transferFocus();
      return;
    }
    //numeros!!!!!
    if (e.getComponent().getClass().getName().equals(
        "centinelas.datos.c_protocolo.CTextField")) {
      CTextField txtAnalizado = (CTextField) e.getComponent();
      String Tipo = txtAnalizado.getTipoPregunta();
      if (Tipo.equals("N") && !txtAnalizado.getText().equals("")) {
        txtAnalizado.DesFormatNumero();
        //mlm txtAnalizado.select(txtAnalizado.getText().length(),txtAnalizado.getText().length() );
        txtAnalizado.select(0, 0);
      }
    }
  }

//botones de actualizar protocolo
  void button_actionPerformed(ActionEvent e) {

    CButtonControl botonAfectado = null;
    CMessage msgBox = null;

    CLista listaLayout1 = new CLista();
    CLista listaLayout2 = new CLista();
    CLista listaLayout2Obsoleta = new CLista();
    CLista listaLayout3 = new CLista();
    CLista listaValores1 = new CLista();
    CLista listaValores2 = new CLista();
    CLista listaValores2Obsoleta = new CLista();
    CLista listaValores3 = new CLista();

    int modo = dialogo.modoOperacion;
    dialogo.Inicializar(modoESPERA);

    try {

      //encontrar boton
      for (int j = 0; j < Num_Botones; j++) {
        CButtonControl btnBoton = (CButtonControl) btn.elementAt(j);
        if (btnBoton.getNivel().equals(e.getActionCommand())) {
          //ese es el boton pulsado--> coger el modelo para restringir listaValores
          botonAfectado = (CButtonControl) btn.elementAt(j);
          break;
        }
      }

      //PARCHE SI QUITAN DATOS de niveles DEL CASO
      if (botonAfectado.getNivel().equals("N1") && sNivel1.equals("")) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "No tiene sentido actualizar el protocolo de Area, porque el expediente no tiene informado este nivel de Zonificaci�n.");
        msgBox.show();
        msgBox = null;
        dialogo.Inicializar(modo);
        return;
      }
      if (botonAfectado.getNivel().equals("N2") && sNivel2.equals("")) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "No tiene sentido actualizar el protocolo de Distrito, porque el expediente no tiene informado este nivel de Zonificaci�n.");
        msgBox.show();
        msgBox = null;
        dialogo.Inicializar(modo);
        return;
      }
      //-------------------------------

      //try{  //quitar

      //subdivido la lista de datos//******** ***** **** *** *** ** ** * * * * * * *
       int iParteFinal1 = 0;
      int iParteInic2 = 0;
      String codAlm = "";

      for (int i = 0; i < listadatos.size(); i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        if (dat.getCodModelo() != null) {
          if (dat.getCodModelo().trim().equals(botonAfectado.getCodModelo().
                                               trim())) {
            iParteFinal1 = i;
            codAlm = dat.getCodModelo().trim();
            break;
          }
          ; //----------------------
        }
      } //for
      for (int i = iParteFinal1; i < listadatos.size(); i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        if (dat.getCodModelo() != null) {
          if (!dat.getCodModelo().trim().equals(codAlm)) {
            iParteInic2 = i;
            break;
          } //----------------------
        }
      } //for
      //rectificacion por las "X"
      if (iParteInic2 == 0) { //era el ultimo
        iParteInic2 = listadatos.size();
        iParteFinal1--;
      }
      else {
        iParteFinal1--;
        iParteInic2--;
      }

      for (int i = 0; i < iParteFinal1; i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        listaLayout1.addElement(dat);
      }
      for (int i = iParteFinal1; i < iParteInic2; i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        listaLayout2Obsoleta.addElement(dat);
      }

      for (int i = iParteInic2; i < listadatos.size(); i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        listaLayout3.addElement(dat);
      }
      //subdivido la lista de datos//******** ***** **** *** *** ** ** * * * * * * *

       //subdivido la lista de valores//******** ***** **** *** *** **
//   /*
         iParteFinal1 = 0;
      iParteInic2 = 0;
      codAlm = "";
      for (int i = 0; i < listaValores.size(); i++) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
        if (dat.getCodModelo().trim().equals(botonAfectado.getCodModelo().trim())) {
          iParteFinal1 = i;
          codAlm = dat.getCodModelo().trim();
          break;
        }
        ;
      } //for
      for (int i = iParteFinal1; i < listaValores.size(); i++) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
        if (!dat.getCodModelo().trim().equals(codAlm)) {
          iParteInic2 = i;
          break;
        } //----------------------
      } //for

      if (iParteFinal1 == 0 && iParteInic2 == 0) { // en este parte no hay datos para ese modelo
        iParteFinal1 = 0;
        iParteInic2 = 0;
      }

      if (iParteInic2 == 0 && iParteFinal1 != 0) { //era el ultimo
        iParteInic2 = listaValores.size();
      }

      for (int i = 0; i < iParteFinal1; i++) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
        listaValores1.addElement(dat);
      }
      for (int i = iParteFinal1; i < iParteInic2; i++) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
        listaValores2Obsoleta.addElement(dat);
      }
      for (int i = iParteInic2; i < listaValores.size(); i++) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
        listaValores3.addElement(dat);
      }
      //subdivido la lista de valores//******** ***** **** *** *** **
//  */

      CLista listaEntrada = new CLista();
      DataProtocolo dataEntrada = null;
      //en la Descripcion va el Nivel
      dataEntrada = new DataProtocolo(sCod, botonAfectado.getNivel(), sNivel1,
                                      sNivel2, sCa, sCaso, notifRMC);
      listaEntrada.addElement(dataEntrada);

      //deshabilitarCampos(Cursor.WAIT_CURSOR);
      listaEntrada.setIdioma(CASTELLANO);
      listaEntrada.setLogin(this.app.getParametro("COD_USUARIO"));
      listaEntrada.setPerfil(PERFIL_COM_AUT);
      stub.setUrl(new URL(app.getParametro("URL_SERVLET") +
                          strSERVLET_ACTIVO_OBSOLETO));

      if (CD_TSIVE.equals("E")) {
        listaLayout2 = (CLista) stub.doPost(servletVERACTIVOS, listaEntrada);
      }
      else if (CD_TSIVE.equals("B")) {
        listaLayout2 = (CLista) stub.doPost(servletVERACTIVOS_B, listaEntrada);
      }
      else if (CD_TSIVE.equals("C")) {
        listaLayout2 = (CLista) stub.doPost(servletVERACTIVOS_C, listaEntrada);
        /*          SrvActualizarProt srv = new SrvActualizarProt ();
                  srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                        "sive_desa",
                                        "sive_desa");
             listaLayout2 = (CLista) srv.doDebug(servletVERACTIVOS_C, listaEntrada);*/
      }

      listaEntrada = null;

    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
      dialogo.Inicializar(modo);
      // habilitarCampos(); // E 25/01/2000 Estaba comentada
      return;
    }

    //si listaLayout2.size == null es porque el nivel ya no procede en el
    //en el expediente (arreglado al principio)

    //No existe otro protocolo activo
    if (listaLayout2 == null) {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            "No existe Protocolo Activo para este nivel");
      msgBox.show();
      msgBox = null;
      dialogo.Inicializar(modo);
      //no se hace nada
      // habilitarCampos(); // E 25/01/2000 Estaba comentada
      return;
    } //--------------------------------------------------------------
    else {
      //listaLayout2: dataLayout  (layout del nuevo)
      //si null, sale por catch  (error)
      //montamos el nuevo listadatos
      listadatos = new CLista();
      listadatos.appendData(listaLayout1);
      listadatos.appendData(listaLayout2);
      listadatos.appendData(listaLayout3);

      // Descomentado el 29-11-2000 para protocolo de centinelas.
      //componer la lista de valores
      listaValores2 = Recomponer_RespEDO(listaValores2Obsoleta, listaLayout2);
      listaValores = new CLista();
      listaValores.appendData(listaValores1);
      listaValores.appendData(listaValores2);
      listaValores.appendData(listaValores3);
      //----------------------------
      // fin del cambio del 29-11-2000

      //$$$$$$$$$$$$$$$$$$
      //extraigo la listaValores2Obsoleta  (para partes puede ser vacia) -------
      for (int i = 0; i < listaValores.size(); i++) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
        if (dat.getCodModelo().trim().equals(botonAfectado.getCodModelo().trim())) {
          listaValores2Obsoleta.addElement(dat);
        }
      }
      //le paso el recomponer
      listaValores2 = Recomponer_RespEDO(listaValores2Obsoleta, listaLayout2);

      //reconstruyo la lista de Valores.
      CLista listaCopiaSinBloque = new CLista();
      for (int i = 0; i < listaValores.size(); i++) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
        listaCopiaSinBloque.addElement(dat);
      }
      int PuntoComienzo = 0;
      for (int i = 0; i < listaValores.size(); i++) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
        if (dat.getCodModelo().trim().equals(botonAfectado.getCodModelo().trim())) {
          PuntoComienzo = i;
          break;
        }
      }
// 29-11-2000. Quito esto, porque al parecer tanto �ndice da
// problemas.

      /*  for (int i = 0; i < listaValores.size(); i++){
           DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(i);
           if ( dat.getCodModelo().trim().equals(botonAfectado.getCodModelo().trim())){
           if  (listaCopiaSinBloque.size() == 1)  listaCopiaSinBloque = new CLista();
               else listaCopiaSinBloque.removeElementAt(i);
            }
        }
       */

// Lo sustituyo por :

      int tmp = listaValores.size();
      for (int k = tmp; k > 0; k--) {
        DataRespCompleta dat = (DataRespCompleta) listaValores.elementAt(k - 1);
        if (dat.getCodModelo().trim().equals(botonAfectado.getCodModelo().trim())) {
          if (listaCopiaSinBloque.size() == 1) {
            listaCopiaSinBloque = new CLista();
          }
          else {
            listaCopiaSinBloque.removeElementAt(k - 1);
          }
        }
      } // del for

// final del cambio del 29-11-2000.

      CLista listaBien = new CLista();
      //pegar en el pto
      for (int i = 0; i < listaCopiaSinBloque.size(); i++) {
        DataRespCompleta dat = (DataRespCompleta) listaCopiaSinBloque.elementAt(
            i);
        if (i != PuntoComienzo) {
          listaBien.addElement(dat); //Valores1
        }
        if (i == PuntoComienzo) {
          listaBien.appendData(listaValores2); //Valores2
          break;
        }
      }

      //resto
      for (int i = PuntoComienzo; i < listaCopiaSinBloque.size(); i++) {
        DataRespCompleta dat = (DataRespCompleta) listaCopiaSinBloque.elementAt(
            i);
        listaBien.addElement(dat);
      } //Valores3

      if (listaCopiaSinBloque.size() == 0) {
        listaBien.appendData(listaValores2);

      }
      listaValores = new CLista();
      listaValores.appendData(listaBien);
      //----------------------------------------------------------------
      //%%%%%%%%%%%%%%%%%%%%%%%%

      //String smsg = "Existen " + listaValores2.size () + " preguntas coincidentes en el protocolo activo de este nivel";
      String smsg =
          "Se mantendr\u00E1n las respuestas para las preguntas coincidentes.";
      msgBox = new CMessage(this.app, CMessage.msgAVISO, smsg);
      msgBox.show();
      msgBox = null;
      // 29-11-2000; Esto no sirve para nada.
//      Pintar_Valores();

      //cerrar el protocolo y volver a abrirlo
      if (sConstructor.equals("2")) { //1: EDO 2: Tube 3: Brotes
//        Volver_a_Abrir_Tuber();
      }
      else if (sConstructor.equals("3")) {
//        Volver_a_Abrir_Brotes();
      }
      else {
        Volver_a_Abrir(); //EDO y CENTINELAS
        /*
          29-11-2000; en este caso, deshabilitamos el m�todo Volver_a_Abrir
          para poderlo utilizar en Centinelas. Se encuentra m�s abajo.
         */

      }
      dialogo.pProtocolo.panelScroll.doLayout();
      dialogo.pProtocolo.panelScroll2.doLayout();
      dialogo.pProtocolo.doLayout();
      dialogo.doLayout();

      //--------------------
      dialogo.Inicializar(modo);
      this.setLayout(xYLayout1);
      this.doLayout();

    } //fin else
  } //boton

  public void Pintar_Valores() {
    /*System.out.println("LAYOUT");
       for (int j = 0; j < listadatos.size(); j++){
      DataLayout data =(DataLayout) listadatos.elementAt(j);
      System.out.println(data.getCA() + "   " +
      data.getNIVEL_1() + "   " +
      data.getNIVEL_2() + "   " +
      data.getCodModelo() + "   " +
      data.getCodPregunta()  + "   " + data.getNumLinea()
      + "   " +data.getDesTexto() );
       }
       System.out.println("RESPEDO");
       for (int i = 0; i < listaValores.size(); i++){
      DataRespCompleta data =(DataRespCompleta) listaValores.elementAt(i);
        System.out.println(data.getCodModelo() + "   " +
        data.getNumLinea() + "   " +
        data.getCodPregunta()
         + "   " +data.getDesPregunta() );
       } */
  }

  public CLista Recomponer_RespEDO(CLista listaResp, CLista listaPlantilla) {
    CLista listaResp2 = new CLista();

    //encontrar preguntas iguales
    for (int i = 0; i < listaResp.size(); i++) {
      DataRespCompleta dataObsoleto = (DataRespCompleta) listaResp.elementAt(i);

      for (int j = 0; j < listaPlantilla.size(); j++) {
        DataLayout dataActivo = (DataLayout) listaPlantilla.elementAt(j);
        if (dataActivo.getCodPregunta() != null) {
          if (dataObsoleto.getCodPregunta().trim().equals(dataActivo.
              getCodPregunta().trim())) {
            DataRespCompleta dataInsertar = new DataRespCompleta(
                sCaso,
                dataActivo.getCodModelo(),
                dataActivo.getNumLinea(),
                dataActivo.getCodPregunta(),
                dataObsoleto.getNivel(),
                dataActivo.getCA(),
                dataActivo.getNIVEL_1(),
                dataActivo.getNIVEL_2(),
                "S",
                dataObsoleto.getDesPregunta(),
                dataObsoleto.getValorLista(), dataObsoleto.getTipoPregunta());

            listaResp2.addElement(dataInsertar);
          }
        }
      }
    } //for ----------------------

    //Ver si las que estan condicionadas deben aparecer o no en la
    //lista de valores nueva

    CLista listaRespResul = new CLista();
    DataLayout layPreg = null;
    String linCond = "";
    String codCond = "";
    String valorCond = "";
    DataRespCompleta preg = null;
    DataRespCompleta preg1 = null;

    CLista lisCopia = new CLista();
    for (int t = 0; t < listaResp2.size(); t++) {
      lisCopia.addElement( (DataRespCompleta) listaResp2.elementAt(t));
    }

    for (int t = 0; t < listaPlantilla.size(); t++) {
      layPreg = (DataLayout) listaPlantilla.elementAt(t);
      if ( (!layPreg.getTipoPreg().equals("X")) &&
          (!layPreg.getTipoPreg().equals("D"))) {
        if (layPreg.getCondicionada().equals("S")) {
          //busco el valor de la pregunta condicionante
          preg = buscarPregListaValores(layPreg.getCodPreguntaCond(), lisCopia);
          if (preg != null) {
            if (!preg.getDesPregunta().equals(layPreg.getDesPreguntaCond())) {
              // se borra de la lista de valores
              preg1 = buscarPregListaValores(layPreg.getCodPregunta(), lisCopia);
              if (preg1 != null) {
                listaResp2.removeElement(preg1);
              }
            }
          }
          else {
            // se borra de la lista de valores
            preg1 = buscarPregListaValores(layPreg.getCodPregunta(), lisCopia);
            if (preg1 != null) {
              listaResp2.removeElement(preg1);
            }
          }
        }
      }
    }
    CLista listaOrdenada = new CLista();
    listaOrdenada = Ordenar_listaRespuestas(listaResp2);
    return (listaOrdenada);
  }

//para el trozo de modelo, ordena por nm_lin
  public CLista Ordenar_listaRespuestas(CLista lista) {

    if (lista.size() == 0) {
      return lista;
    }

    CLista listaOrdenadatmp = new CLista();
    DataRespCompleta dat = (DataRespCompleta) lista.firstElement();
    listaOrdenadatmp.addElement(dat);

    int iPosicion = 0;
    boolean cadaboolean = false;
    int cada = 0;
    int cadatmp = 0;
    for (int i = 1; i < lista.size(); i++) {
      DataRespCompleta datcada = (DataRespCompleta) lista.elementAt(i);
      cada = (new Integer(datcada.getNumLinea())).intValue();
      cadaboolean = false;

      for (int j = 0; j < listaOrdenadatmp.size(); j++) {
        if (!cadaboolean) {

          DataRespCompleta datcadatmp = (DataRespCompleta) listaOrdenadatmp.
              elementAt(j);
          cadatmp = (new Integer(datcadatmp.getNumLinea())).intValue();
          if (cada > cadatmp) {
            //continua...de momento detras
            iPosicion = j + 1;
          }
          if (cada < cadatmp) {
            iPosicion = iPosicion;
            cadaboolean = true;
          }
        }
      } //for

      listaOrdenadatmp.insertElementAt(datcada, iPosicion);
    } //for

    return (listaOrdenadatmp);
  }

  private DataRespCompleta buscarPregListaValores(String cod, CLista lista) {
    DataRespCompleta preg = null;
    for (int i = 0; i < lista.size(); i++) {
      preg = (DataRespCompleta) lista.elementAt(i);
      if (preg.getCodPregunta().trim().equals(cod)) {
        return preg;
      }
    }
    return null;
  }

  /*
   void Volver_a_Abrir_Tuber(){
    //dialogo.tabPanel.VerPanel("Cabecera");
    this.removeAll();
    listaInsertar = new CLista();
    Num_Grupos = 0;
    Num_Choices = 0;
    Num_Cajas = 0;
    Num_Label_Cajas = 0;
    Num_Label_Choices = 0;
    Num_Botones = 0;
    Num_Descripciones = 0;
    vDescripciones = new Vector();
    vCNE = new Vector();
    vCA = new Vector();
    vN1 = new Vector();
    vN2 = new Vector();
    lblT = new Vector();
    lblC = new Vector();
    Des = new Vector(); //Sobretitulos
    cbo = new Vector();
    txt = new Vector();
    gb = new Vector();
    btn = new Vector();
    xYLayout1 = new XYLayout();
    try{
      jbInit();  //componentes
      pTub.Pintar_Arbol(this);
      pTub.xYLayout2.setWidth(pTub.iAncho + 40);
      pTub.xYLayout2.setHeight(iTam);
      pTub.pnl2.setLayout(pTub.xYLayout2);
      pTub.pnl2.doLayout();
      pTub.doLayout();
      this.doLayout();
    }catch(Exception e){
      e.printStackTrace();
    }
    habilitarCampos();
    this.setLayout(xYLayout1);
    this.doLayout();
    //dialogo.tabPanel.VerPanel("Protocolo");
   }
   */

  /*
   void Volver_a_Abrir_Brotes(){
    //dialogo.tabPanel.VerPanel("Cabecera");
    this.removeAll();
    listaInsertar = new CLista();
    Num_Grupos = 0;
    Num_Choices = 0;
    Num_Cajas = 0;
    Num_Label_Cajas = 0;
    Num_Label_Choices = 0;
    Num_Botones = 0;
    Num_Descripciones = 0;
    vDescripciones = new Vector();
    vCNE = new Vector();
    vCA = new Vector();
    vN1 = new Vector();
    vN2 = new Vector();
    lblT = new Vector();
    lblC = new Vector();
    Des = new Vector(); //Sobretitulos
    cbo = new Vector();
    txt = new Vector();
    gb = new Vector();
    btn = new Vector();
    xYLayout1 = new XYLayout();
    try{
      jbInit();  //componentes
      pBrote.Pintar_Arbol(this);
      pBrote.xYLayout2.setWidth(pBrote.iAncho + 40);
      pBrote.xYLayout2.setHeight(iTam);
      pBrote.pnl2.setLayout(pBrote.xYLayout2);
      pBrote.pnl2.doLayout();
      pBrote.doLayout();
      this.doLayout();
    }catch(Exception e){
      e.printStackTrace();
    }
    habilitarCampos();
    this.setLayout(xYLayout1);
    this.doLayout();
    //dialogo.tabPanel.VerPanel("Protocolo");
   }
   */

  void Volver_a_Abrir() {
    //dialogo.tabPanel.VerPanel("Cabecera");
    this.removeAll();
    listaInsertar = new CLista();

    Num_Grupos = 0;
    Num_Choices = 0;
    Num_Cajas = 0;
    Num_Label_Cajas = 0;
    Num_Label_Choices = 0;
    Num_Botones = 0;
    Num_Descripciones = 0;

    vDescripciones = new Vector();
    vCNE = new Vector();
    vCA = new Vector();
    vN1 = new Vector();
    vN2 = new Vector();
    lblT = new Vector();
    lblC = new Vector();
    Des = new Vector(); //Sobretitulos
    cbo = new Vector();
    txt = new Vector();
    gb = new Vector();
    btn = new Vector();

    xYLayout1 = new XYLayout();
    try {

      jbInit(); //componentes
      dialogo.pProtocolo.Pintar_Arbol(this);

      dialogo.pProtocolo.xYLayout2.setWidth(dialogo.pProtocolo.iAncho + 40);
      dialogo.pProtocolo.xYLayout2.setHeight(iTam);
      dialogo.pProtocolo.pnl2.setLayout(dialogo.pProtocolo.xYLayout2);
      dialogo.pProtocolo.pnl2.doLayout();
      dialogo.pProtocolo.doLayout();
      this.doLayout();

    }
    catch (Exception e) {
      e.printStackTrace();
    }

    habilitarCampos();
    this.setLayout(xYLayout1);
    this.doLayout();

    //dialogo.tabPanel.VerPanel("Protocolo");
  }

//Boton reset
  void button1_actionPerformed(ActionEvent e) {

    setCursor(new Cursor(Cursor.WAIT_CURSOR));

    CTextField txtEtiqueta;
    CChoice cboEtiqueta;

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      txtEtiqueta.setText("");
    } //cajas

    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      cboEtiqueta.select(0);
    } //choices

    //nota: podemos estar en alta o modif
    modoOperacion = modoCondicionadaDeshabilitar;
    Inicializar();
    this.doLayout();

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  } //fin boton reset

  public Vector getEnfermedad() {
    Vector vAlmacen = new Vector();
    Nodo pto;
    vAlmacen.addElement(new Nodo(sDes, 0));
    return (vAlmacen);
  }

  public Vector getDescripciones() {
    return (vDescripciones);
  }

  public Vector getCNE() {
    return (vCNE);
  }

  public Vector getCA() {
    return (vCA);
  }

  public Vector getN1() {
    return (vN1);
  }

  public Vector getN2() {
    return (vN2);
  }

//PARA INSERTAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//true : si listo para insertar
//true:   si modifica borrando todos los datos.
//false:  faltan campos obligatorios
//false:  si es un alta, pulsaria aceptar, pero no lleva datos
  public boolean ValidarObligatorios() {

    boolean bResultado = false;
    CMessage msgBox = null;

    LostGained(); //Perdida de foco, para validar el ultimo

    //voy leyendo cajas: flag, leo listas y flag
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;

    boolean bUnoEncontradoCaja = false;
    boolean bUnoEncontradoChoice = false;

    Object CompVolvera = new Object();
    Object CompVolveraCaja = new Object();
    Object CompVolveraChoice = new Object();

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      if (txtEtiqueta.getObligatorio().equals("S") &&
          txtEtiqueta.getText().equals("")) {
        bUnoEncontradoCaja = true;
        CompVolveraCaja = (CTextField) txtEtiqueta;
        txtEtiqueta.requestFocus();
        break;
      }
    }
    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      if (cboEtiqueta.getSelectedIndex() == -1 &&
          cboEtiqueta.getObligatorio().equals("S")) {
        bUnoEncontradoChoice = true;
        CompVolveraChoice = (CChoice) cboEtiqueta;
        cboEtiqueta.requestFocus();
        break;
      }
      if (cboEtiqueta.getObligatorio().equals("S") &&
          cboEtiqueta.getSelectedIndex() != -1 &&
          cboEtiqueta.getItem(cboEtiqueta.getSelectedIndex()).equals("") &&
          !bUnoEncontradoChoice) {
        bUnoEncontradoChoice = true;
        CompVolveraChoice = (CChoice) cboEtiqueta;
        cboEtiqueta.requestFocus();
        break;
      }
    }
    //veo cual queda mas alto
    //solo choice
    if (bUnoEncontradoCaja && bUnoEncontradoChoice) {

      cboEtiqueta = (CChoice) CompVolveraChoice;
      txtEtiqueta = (CTextField) CompVolveraCaja;
      float yChoice = cboEtiqueta.getAlignmentY();
      float yCaja = txtEtiqueta.getAlignmentY();

      if (yChoice > yCaja) {
        CompVolvera = (CChoice) CompVolveraChoice;
      }
      else {
        CompVolvera = (CTextField) CompVolveraCaja;
      }
    }
    if (bUnoEncontradoCaja && !bUnoEncontradoChoice) {

      txtEtiqueta = (CTextField) CompVolveraCaja;
      CompVolvera = (CTextField) CompVolveraCaja;
    }
    if (!bUnoEncontradoCaja && bUnoEncontradoChoice) {

      cboEtiqueta = (CChoice) CompVolveraChoice;
      CompVolvera = (CChoice) CompVolveraChoice;
    }

    //mensaje y foco o continuo
    if (bUnoEncontradoCaja || bUnoEncontradoChoice) {
      //mensaje   //foco
      bResultado = false;
      msgBox = new CMessage(app, CMessage.msgAVISO,
                            "Faltan campos obligatorios en el Protocolo.");
      msgBox.show();
      msgBox = null;
      if (CompVolvera.getClass().getName().equals(
          "centinelas.datos.c_protocolo.CChoice")) {
        cboEtiqueta = (CChoice) CompVolvera;
        this.requestFocus();
        cboEtiqueta.requestFocus();
      }
      else {
        txtEtiqueta = (CTextField) CompVolvera;
        this.requestFocus();
        txtEtiqueta.requestFocus();
      }
      bResultado = false;
    }
    else {
      bResultado = true; //de momento
      this.getListaParaInsertar();

      // Comento la comprobaci�n de que se hayan metido datos.
      // Puede ocurrir que el usuario no haya querido introducir datos.
      // ARS 14-05-01
      if (listaInsertar.size() == 0) {
        if (iEstado == ALTA) {
//          msgBox = new CMessage(app,CMessage.msgAVISO,
//                "No hay datos para insertar en el protocolo.");
//          msgBox.show();
//          msgBox = null;
          bResultado = true; // Antes era false, pero se pone a true ARS 14-05-01
        }
        else if (iEstado == MODIFICACION) {
//          msgBox = new CMessage(app,CMessage.msgAVISO,
//                      "Borrar� todos los datos del protocolo.");
//          msgBox.show();
//          msgBox = null;

          //paso al menos el nm_edo (caso)
          DataResp datoRegistro = null;
          //envio caso, y el CodModelo a "", para distinguirlo en el srv
          datoRegistro = new DataResp(notifRMC, sCaso,
                                      "", null, null, null, null);

          listaInsertar.addElement(datoRegistro);

          bResultado = true;
        }
      }
      else if (listaInsertar.size() > 0) {
        bResultado = true;
      }
    }

    return (bResultado);
  }

//para sin volver a abrir el protocolo, absorba el nm_edo
//llamar antes de ValidarObligatorios()!!!!
  public void Cargar_nm_edo(String caso) {
    sCaso = caso;
  }

  public void getListaParaInsertar() {

    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    DataResp datoRegistro = null;
    DataRespTuber datoRegistroTuber = null;

    listaInsertar.removeAllElements();

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      if (!txtEtiqueta.getText().equals("")) {
        if (txtEtiqueta.getTipoPregunta().equals("N")) {
          if (sConstructor.equals("2")) {
            datoRegistroTuber = new DataRespTuber(new Integer(sCaso).intValue(),
                                                  txtEtiqueta.getCodM(),
                                                  new Integer(txtEtiqueta.
                getNum()).intValue(),
                                                  txtEtiqueta.getCodPregunta(),
                                                  txtEtiqueta.getNumSinFormato().
                                                  trim(),
                                                  null,
                                                  "");
          }
          else if (sConstructor.equals("4")) { //CENTINELAS
            datoRegistro = new DataResp(notifRMC, sCaso,
                                        txtEtiqueta.getCodM(),
                                        txtEtiqueta.getNum(),
                                        txtEtiqueta.getCodPregunta(),
                                        txtEtiqueta.getNumSinFormato().trim(),
                                        null);

          }
          else {
            datoRegistro = new DataResp(sCaso,
                                        txtEtiqueta.getCodM(),
                                        txtEtiqueta.getNum(),
                                        txtEtiqueta.getCodPregunta(),
                                        txtEtiqueta.getNumSinFormato().trim(),
                                        null);
          }
        }
        else {
          if (sConstructor.equals("2")) {
            datoRegistroTuber = new DataRespTuber(new Integer(sCaso).intValue(),
                                                  txtEtiqueta.getCodM(),
                                                  new Integer(txtEtiqueta.
                getNum()).intValue(),
                                                  txtEtiqueta.getCodPregunta(),
                                                  txtEtiqueta.getText().trim(),
                                                  null,
                                                  "");

          }
          else if (sConstructor.equals("4")) {
            datoRegistro = new DataResp(notifRMC, sCaso,
                                        txtEtiqueta.getCodM(),
                                        txtEtiqueta.getNum(),
                                        txtEtiqueta.getCodPregunta(),
                                        txtEtiqueta.getText().trim(),
                                        null);

          }
          else {
            datoRegistro = new DataResp(sCaso,
                                        txtEtiqueta.getCodM(),
                                        txtEtiqueta.getNum(),
                                        txtEtiqueta.getCodPregunta(),
                                        txtEtiqueta.getText().trim(),
                                        null);
          }
        }
        if (sConstructor.equals("2")) {
          listaInsertar.addElement(datoRegistroTuber);
        }
        else {
          listaInsertar.addElement(datoRegistro); //EDO y CENTINELAS
        }
      }

    }
    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);

      if (cboEtiqueta.getSelectedIndex() != -1) {
        if (!cboEtiqueta.getItem(cboEtiqueta.getSelectedIndex()).equals("")) {

          if (sConstructor.equals("2")) {
            datoRegistroTuber = new DataRespTuber(new Integer(sCaso).intValue(),
                                                  cboEtiqueta.getCodM(),
                                                  new Integer(cboEtiqueta.
                getNum()).intValue(),
                                                  cboEtiqueta.getCodPregunta(),
                                                  ValorSeleccionadoChoiceNumero(
                cboEtiqueta),
                                                  cboEtiqueta.getSelectedItem(),
                                                  "");
            listaInsertar.addElement(datoRegistroTuber);

          }
          else if (sConstructor.equals("4")) {
            datoRegistro = new DataResp(notifRMC, sCaso,
                                        cboEtiqueta.getCodM(),
                                        cboEtiqueta.getNum(),
                                        cboEtiqueta.getCodPregunta(),
                                        ValorSeleccionadoChoiceNumero(
                cboEtiqueta),
                                        cboEtiqueta.getSelectedItem());
            listaInsertar.addElement(datoRegistro);

          }
          else {
            datoRegistro = new DataResp(sCaso,
                                        cboEtiqueta.getCodM(),
                                        cboEtiqueta.getNum(),
                                        cboEtiqueta.getCodPregunta(),
                                        ValorSeleccionadoChoiceNumero(
                cboEtiqueta),
                                        cboEtiqueta.getSelectedItem());
            listaInsertar.addElement(datoRegistro);
          }

        }
      }
    }

  }

  public void Insertar() {
    CMessage msgBox = null;
    int modoOperacionServlet = 0;

    try {

      setCursor(new Cursor(Cursor.WAIT_CURSOR));

      //ALTA----------
      if (iEstado == ALTA) {
        modoOperacionServlet = servletALTA;
        // apunta al servlet
        listaInsertar.setIdioma(CASTELLANO);
        listaInsertar.setLogin(this.app.getParametro("COD_USUARIO"));
        listaInsertar.setPerfil(PERFIL_COM_AUT);

        if (CD_TSIVE.trim().equals("E")) {
          if (sConstructor.equals("1")) {
            stub.setUrl(new URL(this.app.getParametro("URL_SERVLET") +
                                strSERVLET_RESP));
          }
          else if (sConstructor.equals("2")) {
            //$$$  // Aqu� se insertar� tuberculosis
            stub.setUrl(new URL(this.app.getParametro("URL_SERVLET") +
                                strSERVLET_RESP_T));
            modoOperacionServlet = constantes.modoALTA;
          }
        }
        else if (CD_TSIVE.trim().equals("B")) {
          stub.setUrl(new URL(this.app.getParametro("URL_SERVLET") +
                              strSERVLET_RESP_B));

        }
        else if (CD_TSIVE.trim().equals("C")) {
          stub.setUrl(new URL(this.app.getParametro("URL_SERVLET") +
                              strSERVLET_RESP_C));

          //Se ejecuta en cualquier caso
        }
        this.stub.doPost(modoOperacionServlet, listaInsertar);
//      BDatosEDO.ejecutaSQL(false,app,strSERVLET_RESP_C,modoOperacionServlet,listaInsertar);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //14/09/99 quitar msg Enrique
        /*msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg8.Text"));
               msgBox.show();
               msgBox = null;*/
        iEstado = MODIFICACION; //PASAMOS A MODIFICACION
      }
      else if (iEstado == MODIFICACION) {
        modoOperacionServlet = servletMODIFICAR;
        // apunta al servlet
        listaInsertar.setIdioma(CASTELLANO);
        listaInsertar.setLogin(this.app.getParametro("COD_USUARIO"));
        listaInsertar.setPerfil(PERFIL_COM_AUT);
        if (CD_TSIVE.trim().equals("E")) {
          if (sConstructor.equals("1")) {
            stub.setUrl(new URL(this.app.getParametro("URL_SERVLET") +
                                strSERVLET_RESP));
          }
          else if (sConstructor.equals("2")) {
            // $$$ // Aqu� van datos de tuberculosis
            stub.setUrl(new URL(this.app.getParametro("URL_SERVLET") +
                                strSERVLET_RESP_T));
            modoOperacionServlet = constantes.modoMODIFICACION;
          }
        }
        else if (CD_TSIVE.trim().equals("B")) {
          stub.setUrl(new URL(this.app.getParametro("URL_SERVLET") +
                              strSERVLET_RESP_B));

        }
        else if (CD_TSIVE.trim().equals("C")) { //Centinelas
          stub.setUrl(new URL(this.app.getParametro("URL_SERVLET") +
                              strSERVLET_RESP_C));

        }

        this.stub.doPost(modoOperacionServlet, listaInsertar);
//      BDatosEDO.ejecutaSQL(false,app,strSERVLET_RESP_C,modoOperacionServlet,listaInsertar);
        /*SrvRespCenti srv = new SrvRespCenti();
         srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                              "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                              "sive_desa",
                              "sive_desa");
         srv.doDebug(modoOperacionServlet,listaInsertar);*/

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //14/09/99 quitar msg Enrique
        /*msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg9.Text"));
               msgBox.show();
               msgBox = null; */

      }
      // error en el proceso
    }
    catch (Exception e) {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;

    }
  }

///////////////////////////////////////////////////

  /*
   public void setProtocolo(DataProtocolo datosEntrada){
    sCod = datosEntrada.getCod();
    sDes =  datosEntrada.getDes();
    sNivel1 = datosEntrada.getNivel1();
    sNivel2 = datosEntrada.getNivel2();
    sCa = datosEntrada.getCa();
    sCaso = datosEntrada.getCaso(); // para brotes es (CD_ANONM_ALERBRO)
    sNotificador = datosEntrada.getNotificador();
   }
   */
  public void setProtocolo(DataProtocolo datosEntrada) {

    sCod = datosEntrada.getCod();
    sDes = datosEntrada.getDes();
    sNivel1 = datosEntrada.getNivel1();
    sNivel2 = datosEntrada.getNivel2();
    sCa = datosEntrada.getCa();
    sCaso = datosEntrada.getCaso();
//  sNotificador = datosEntrada.getNotificador();
    notifRMC = datosEntrada.getNotifRMC();

  }

  public boolean getNoSeEncontraronDatos() {
    return (NoSeEncontraronDatos);
  }

  void boton_mouseEntered(MouseEvent e) {
    //encontrar boton, entre los de "actualizar protocolo"
    boolean es_actualizar = true;

    for (int j = 0; j < Num_Botones; j++) {
      CButtonControl btnBoton = (CButtonControl) btn.elementAt(j);
      if (btnBoton.getName().equals(e.getComponent().getName())) {
        btnBoton.transferFocus();
        btnBoton.requestFocus();
        es_actualizar = false;
      }
    }
    // Si no es un bot�n de actualizar protocolo, el que queda
    // es el de limpiar protocolo
    if (es_actualizar) {
      button1.transferFocus();
      button1.requestFocus();
    }
  }

} //FIN DE CLASE PPAL******************************************

class Item_Completo
    implements ItemListener {
  Panel_Informe_Completo adaptee;
  ItemEvent e = null;

  Item_Completo(Panel_Informe_Completo adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.objitemchange(e);
  }

}

class Foco_Completo
    implements FocusListener {
  Panel_Informe_Completo adaptee;
  FocusEvent e = null;

  Foco_Completo(Panel_Informe_Completo adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.objfocusLost(e);
  }

  public void focusGained(FocusEvent e) {
    adaptee.objfocusGained(e);
  }
}

//Botones
class Panel_Informe_Completo_button_actionAdapter
    implements java.awt.event.ActionListener {
  Panel_Informe_Completo adaptee;

  Panel_Informe_Completo_button_actionAdapter(Panel_Informe_Completo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("reset")) {
      adaptee.button1_actionPerformed(e);
    }
    else {
      adaptee.button_actionPerformed(e);
    }
  }
}

// Para corregir el problema del doble 'clic' en los botones.
class Botones_mouseAdapter
    extends java.awt.event.MouseAdapter {
  Panel_Informe_Completo adaptee;

  Botones_mouseAdapter(Panel_Informe_Completo adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseEntered(MouseEvent e) {
    adaptee.boton_mouseEntered(e);
  }
}
