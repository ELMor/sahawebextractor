// Clase con m�todos para manipulaci�n de fechas y obtenci�n de edades a partir de fechas

package centinelas.datos.comun;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Fechas {

  /** variable de formateo de fechas */
  protected static SimpleDateFormat formatter = new SimpleDateFormat(
      "dd/MM/yyyy");

  /** milisegundos en un dia (1000L * 60L * 60L * 24L) */
  public final static long mili_dia = 86400000L;

  /** cantidad de milisegiundos en un mes de 30.4 d�as mili_mes = (1000L * 60L * 60L * 24L * 30.4 */
  public final static long mili_mes = 2626560000L;
  /** cantidad de milisegiundos en un a�o de 365.25 d�as  mili_anio = 1000L * 60L * 60L * 24L * 365.25; */
  public final static long mili_anio = 31557600000L;

  public Fechas() {
  }

  /**
   *  esta funcion formatea una fecha  a cadena
   *
   * @param a_date es la fecha a formatear
   * @return es la cadena de textocon el formato formatter
   */
  public final static String date2String(Date a_date) {

    if (a_date == null) {
      return null;
    }
    try {

      return formatter.format(a_date).toString();
    }
    catch (Exception exc) {
      return "";
    }
  }

  /**
   *  esta funcion formatea una fecha
   *
   * @param a_String es la cadena de texto a traducir a fecha
   * @return es la fecha que describ�a la cadena
   */
  public final static Date string2Date(String a_String) {
    if (a_String == null || a_String.trim().length() == 0) {
      return null;
    }

    try {
      ParsePosition pos = new ParsePosition(0);
      return formatter.parse(a_String, pos);
    }
    catch (Exception exc) {
      return null;
    }
  }

  /**
   *  esta funci�n resta un n�mero de meses y a�os a una fecha
   *
   * @param iTimepo es un entero que indica la cantidad de a�os o meses
       * @param is_meses es un booleano, tru si iTiempo son meses y false si son a�os
   * @ a_data es la fecha a partir dela cual hay que restar
   * @return es la fecha una vez restada
   */
  public final static Date restaTiempo(int iTiempo, boolean is_meses,
                                       Date a_date) {
    /*   long tiempo = a_date.getTime();
       // calculamos lo milisegundos de ese tiempo diferencial
       long diferencia_tiempo = ((long) iTiempo) * mili_mes;
       if (!is_meses){
          // comola diferencia son a�os no meses  * 12
          diferencia_tiempo =  diferencia_tiempo * 12L;
       }
       long result = tiempo - diferencia_tiempo;
       return  new Date(result);
     */

    SimpleDateFormat formato3 = new SimpleDateFormat("dd/MM/yyyy",
        new Locale("es", "ES"));

    //System.out.println("Date: " + a_date.toString());

    String strDate = "";
    try {
      strDate = formato3.format(a_date);
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    //String strDate = date2String(aux);

    //System.out.println("antes de strDate.substring: " + strDate + " "+strDate.length());

    int anios = Integer.parseInt(strDate.substring(6));
    int meses = Integer.parseInt(strDate.substring(3, 5));
    int dias = Integer.parseInt(strDate.substring(0, 2));
    //meses--; // la cuenta del mes empieza en cero
    if (is_meses) {
      meses -= iTiempo;
      if (meses < 0) {
        meses += 12;
        anios--;
      }
    }
    else {
      anios -= iTiempo;
    }
    return string2Date( (new Integer(dias)).toString() + "/" +
                       (new Integer(meses)).toString() + "/" +
                       (new Integer(anios)).toString());
  }

  /**
   *  esta funci�n devuelve la edad en a�os de una persona
   *
   *  @param  fecNacimiento es la fecha de nacimiento de la persona
   *  @return  son los a�osd que tiene
   */
  public final static int edadAnios(Date fecNacimiento) {
    Date hoy = new Date();
    if (fecNacimiento == null) {
      return 0;
    }

    long edad_mili = hoy.getTime() - fecNacimiento.getTime();

    if (edad_mili > 0) {
      return (int) (edad_mili / mili_anio);
    }
    else {
      return 0;
    }
  }

  /**
   *  esta funci�n devuelve la edad en meses de una persona
   *
   *  @param  fecNacimiento es la fecha de nacimiento de la persona
   *  @return  son los meses que tiene
   */
  public final static int edadMeses(Date fecNacimiento) {
    Date hoy = new Date();
    if (fecNacimiento == null) {
      return 0;
    }

    long edad_mili = hoy.getTime() - fecNacimiento.getTime();

    if (edad_mili > 0) {
      return (int) (edad_mili / mili_mes);
    }
    else {
      return 0;
    }
  }

  /**
   *  indica si la edad es en a�os o en meses
   *
   *  @param  fecNacimiento es la fecha de nacimiento de la persona
   *  @return  true si son a�os, false si son meses
   */
  public final static boolean edadTipo(Date fecNacimiento) {
    Date hoy = new Date();
    int edad = 0;
    if (fecNacimiento == null) {
      return true;
    }

    long edad_mili = hoy.getTime() - fecNacimiento.getTime();

    if (edad_mili > 0) {
      edad = (int) (edad_mili / mili_anio);
    }
    else {
      edad = 0;
    }

    return (edad > 1);
  }

  /**
   *  devuelve la edad es en a�os o en meses
   *
   *  @param  fecNacimiento es la fecha de nacimiento de la persona
   *  @return  un entero con los a�os o con los meses
   */
  public final static int edadDato(Date fecNacimiento) {
    Date hoy = new Date();
    int edad = 0;
    if (fecNacimiento == null) {
      return 0;
    }

    long edad_mili = hoy.getTime() - fecNacimiento.getTime();

    if (edad_mili > 0) {
      edad = (int) (edad_mili / mili_anio);
    }
    else {
      edad = 0;
    }

    if (edad < 2) {
      edad = (int) (edad_mili / mili_mes);
    }

    return edad;
  }

  //Devuelve el a�o indicado en una fecha.
  public final static int getAno(Date dFecha) {
    int ano;
    Calendar calen = new GregorianCalendar();
    calen.setTime(dFecha);
    ano = calen.get(Calendar.YEAR);
    return ano;
  }

  //Devuelve el mes indicado en una fecha ( 1 ser�a enero y 12 diciembre)
  public final static int getMes(Date dFecha) {
    int mes;
    Calendar calen = new GregorianCalendar();
    calen.setTime(dFecha);
    mes = calen.get(Calendar.MONTH) + 1; //Se suma 1 porque la clase Calendar consiedera enero es 0
    return mes;
  }

  //Construye una fecha en el formato "dd/MM/yyyy" a partir de datos de dia, mes, a�o
  public final static String getFecha(int dia, int mes, int ano) {
    String fecha = "";
    String sDia = "";
    String sMes = "";
    if (dia < 10) {
      sDia = "0" + Integer.toString(dia);
    }
    else {
      sDia = Integer.toString(dia);
    }
    if (mes < 10) {
      sMes = "0" + Integer.toString(mes);
    }
    else {
      sMes = Integer.toString(mes);
    }
    fecha = sDia + "/" + sMes + "/" + Integer.toString(ano);
    return fecha;
  }

} ///_____________________________________________ END CLASS
