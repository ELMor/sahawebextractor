package centinelas.datos.c_protocolo;

import com.borland.jbcl.model.GraphLocation;

// referencias del arbol
public class Nodo2 {
  protected int y;
  protected GraphLocation g;

  public Nodo2(GraphLocation g, int y) {
    this.y = y;
    this.g = g;
  }

  public GraphLocation getGraphLocation() {
    return g;
  }

  public int getY() {
    return y;
  }
}
