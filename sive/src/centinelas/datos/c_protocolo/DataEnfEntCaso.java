package centinelas.datos.c_protocolo;

import java.io.Serializable;

//Centinelas LRG
import centinelas.datos.centbasicos.NotifRMC;

//soporta los datos para hacer la query base
public class DataEnfEntCaso
    implements Serializable {

  protected String sCodEnf = "";
  protected String sCA = "";
  protected String sN1 = "";
  protected String sN2 = "";
  protected String sNM_EDO = "";

  protected NotifRMC notifRMC = null;

  /*
    public DataEnfEntCaso(String CodEnf,
                          String sComunidad,
                          String Nivel1,
                          String Nivel2,
                          String Expediente
                          ) {
      sCodEnf = CodEnf;
      sCA = sComunidad;
      sN1 = Nivel1;
      sN2 = Nivel2;
      sNM_EDO = Expediente;
    }
   */

  public DataEnfEntCaso(String CodEnf,
                        String sComunidad,
                        String Nivel1,
                        String Nivel2,
                        String Expediente,
                        NotifRMC not) {

    sCodEnf = CodEnf;
    sCA = sComunidad;
    sN1 = Nivel1;
    sN2 = Nivel2;
    sNM_EDO = Expediente;
    notifRMC = not;
  }

  public String getCodEnfermedad() {
    return sCodEnf;
  }

  public String getComunidad() {
    return sCA;
  }

  public String getNivelUNO() {
    return sN1;
  }

  public String getNivelDOS() {
    return sN2;
  }

  public String getNM_EDO() {
    return sNM_EDO;
  }

  public NotifRMC getNotifRMC() {
    return notifRMC;
  }

}
