//Title:        Generacion de alarmas automaticas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio && Luis Rivera
//Company:      Norsistemas

//Estructura de datos para generaci�n de alrmas autom�ticas
//Los atributos m�s representativos son el a�o y un vector de enfermedades
// en el cual se almacena la infomaci�n de enfermedades

package centinelas.datos.genalauto;

import java.io.Serializable;
import java.util.Vector;

public class ParGenAlaAuto
    implements Serializable {

  public String anoEpi = new String();
  public int tipo = 0;
  public Vector enfermedad = new Vector();

  public String sCodNiv1 = "";
  public String sCodNiv2 = "";

  public ParGenAlaAuto() {
  }

  //Centinelas
  public ParGenAlaAuto(String ano) {
    anoEpi = ano;
  }

  public ParGenAlaAuto(String ano, int t) {
    anoEpi = ano;
    tipo = t;

  }

  public ParGenAlaAuto(String ano, Vector e, int t) {
    anoEpi = ano;
    enfermedad = e;
    tipo = t;

  }

  public ParGenAlaAuto(String ano, String cod1, String cod2) {
    anoEpi = ano;
    sCodNiv1 = cod1;
    sCodNiv2 = cod2;
  }

  public ParGenAlaAuto(String ano, int t, String cod1, String cod2) {
    anoEpi = ano;
    tipo = t;
    sCodNiv1 = cod1;
    sCodNiv2 = cod2;

  }

  public ParGenAlaAuto(String ano, Vector e, int t, String cod1, String cod2) {
    anoEpi = ano;
    enfermedad = e;
    tipo = t;
    sCodNiv1 = cod1;
    sCodNiv2 = cod2;

  }

}