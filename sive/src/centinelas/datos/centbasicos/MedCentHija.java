
package centinelas.datos.centbasicos;

import java.util.Enumeration;

import sapp2.Data;

public class MedCentHija
    extends MedCent {

  public MedCentHija() {
  }

  // Constructor a partir de un Data
  public MedCentHija(Data d) {
    Enumeration e1 = d.keys();
    Enumeration e2 = d.elements();

    for (e1 = d.keys(), e2 = d.elements(); e1.hasMoreElements(); ) {
      this.put( (String) e1.nextElement(), (String) e2.nextElement());
    }
  }

}