/**
 * Clase: MedCent
 * Paquete: centinelas.datos.centbasicos
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 12/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Estructura de datos de un medico centinela.
 * JMT (12/01/2000) Constructor por defecto y a partir de un Data
 * JMT (18/01/2000) Queries m�s utilizadas sobre MedCent
 */

package centinelas.datos.centbasicos;

import java.util.Enumeration;

import sapp2.Data;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class MedCent
    extends Data {

  // Constructor por defecto
  public MedCent() {
  }

  public MedCent(String sCD_PCENTI, String sCD_MEDCEN, String sCD_E_NOTIF,
                 String sCD_SEXO, String sCD_MASIST, String sCD_ANOEPIA,
                 String sCD_SEMEPIA, String sDS_APE1, String sDS_APE2,
                 String sDS_NOMBRE, String sDS_DNI, String sDS_TELEF,
                 String sDS_FAX, String sFC_NAC, String sFC_ALTA,
                 String sNMREDADI, String sNMREDADF, String sDS_HORAI,
                 String sDS_HORAF,
                 String sIT_BAJA, String sFC_BAJA,
                 String sCD_ANOBAJA, String sCD_SEMBAJA, String sCD_MOTBAJA,
                 String sCD_OPE, String sFC_ULTACT) {

    if (sCD_PCENTI == null) {
      put("CD_PCENTI", "");
    }
    else {
      put("CD_PCENTI", sCD_PCENTI);
    }
    if (sCD_MEDCEN == null) {
      put("CD_MEDCEN", "");
    }
    else {
      put("CD_MEDCEN", sCD_MEDCEN);
    }
    if (sCD_E_NOTIF == null) {
      put("CD_E_NOTIF", "");
    }
    else {
      put("CD_E_NOTIF", sCD_E_NOTIF);
    }
    if (sCD_SEXO == null) {
      put("CD_SEXO", "");
    }
    else {
      put("CD_SEXO", sCD_SEXO);
    }
    if (sCD_MASIST == null) {
      put("CD_MASIST", "");
    }
    else {
      put("CD_MASIST", sCD_MASIST);
    }
    if (sCD_ANOEPIA == null) {
      put("CD_ANOEPIA", "");
    }
    else {
      put("CD_ANOEPIA", sCD_ANOEPIA);
    }
    if (sCD_SEMEPIA == null) {
      put("CD_SEMEPIA", "");
    }
    else {
      put("CD_SEMEPIA", sCD_SEMEPIA);
    }
    if (sDS_APE1 == null) {
      put("DS_APE1", "");
    }
    else {
      put("DS_APE1", sDS_APE1);
    }
    if (sDS_APE2 == null) {
      put("DS_APE2", "");
    }
    else {
      put("DS_APE2", sDS_APE2);
    }
    if (sDS_NOMBRE == null) {
      put("DS_NOMBRE", "");
    }
    else {
      put("DS_NOMBRE", sDS_NOMBRE);
    }
    if (sDS_DNI == null) {
      put("DS_DNI", "");
    }
    else {
      put("DS_DNI", sDS_DNI);
    }
    if (sDS_TELEF == null) {
      put("DS_TELEF", "");
    }
    else {
      put("DS_TELEF", sDS_TELEF);
    }
    if (sDS_FAX == null) {
      put("DS_FAX", "");
    }
    else {
      put("DS_FAX", sDS_FAX);
    }
    if (sFC_NAC == null) {
      put("FC_NAC", "");
    }
    else {
      put("FC_NAC", sFC_NAC);
    }
    if (sFC_ALTA == null) {
      put("FC_ALTA", "");
    }
    else {
      put("FC_ALTA", sFC_ALTA);
    }
    if (sNMREDADI == null) {
      put("NMREDADI", "");
    }
    else {
      put("NMREDADI", sNMREDADI);
    }
    if (sNMREDADF == null) {
      put("NMREDADF", "");
    }
    else {
      put("NMREDADF", sNMREDADF);
    }
    if (sDS_HORAI == null) {
      put("DS_HORAI", "");
    }
    else {
      put("DS_HORAI", sDS_HORAI);
    }
    if (sDS_HORAF == null) {
      put("DS_HORAF", "");
    }
    else {
      put("DS_HORAF", sDS_HORAF);
    }
    if (sIT_BAJA == null) {
      put("IT_BAJA", "");
    }
    else {
      put("IT_BAJA", sIT_BAJA);
    }
    if (sFC_BAJA == null) {
      put("FC_BAJA", "");
    }
    else {
      put("FC_BAJA", sFC_BAJA);
    }
    if (sCD_ANOBAJA == null) {
      put("CD_ANOBAJA", "");
    }
    else {
      put("CD_ANOBAJA", sCD_ANOBAJA);
    }
    if (sCD_SEMBAJA == null) {
      put("CD_SEMBAJA", "");
    }
    else {
      put("CD_SEMBAJA", sCD_SEMBAJA);
    }
    if (sCD_MOTBAJA == null) {
      put("CD_MOTBAJA", "");
    }
    else {
      put("CD_MOTBAJA", sCD_MOTBAJA);
    }
    if (sCD_OPE == null) {
      put("CD_OPE", "");
    }
    else {
      put("CD_OPE", sCD_OPE);
    }
    if (sFC_ULTACT == null) {
      put("FC_ULTACT", "");
    }
    else {
      put("FC_ULTACT", sFC_ULTACT);
    }
  }

  // Constructor a partir de un Data
  public MedCent(Data d) {
    Enumeration e1 = d.keys();
    Enumeration e2 = d.elements();

    for (e1 = d.keys(), e2 = d.elements(); e1.hasMoreElements(); ) {
      this.put( (String) e1.nextElement(), (String) e2.nextElement());
    }

    // Campos que pueden ser nulos
    if (d.get("CD_PCENTI") == null) {
      this.put("CD_PCENTI", "");
    }
    if (d.get("CD_MEDCEN") == null) {
      this.put("CD_MEDCEN", "");
    }
    if (d.get("CD_E_NOTIF") == null) {
      this.put("CD_E_NOTIF", "");
    }
    if (d.get("CD_SEXO") == null) {
      this.put("CD_SEXO", "");
    }
    if (d.get("CD_MASIST") == null) {
      this.put("CD_MASIST", "");
    }
    if (d.get("CD_ANOEPIA") == null) {
      this.put("CD_ANOEPIA", "");
    }
    if (d.get("CD_SEMEPIA") == null) {
      this.put("CD_SEMEPIA", "");
    }
    if (d.get("DS_APE1") == null) {
      this.put("DS_APE1", "");
    }
    if (d.get("DS_APE2") == null) {
      this.put("DS_APE2", "");
    }
    if (d.get("DS_NOMBRE") == null) {
      this.put("DS_NOMBRE", "");
    }
    if (d.get("DS_DNI") == null) {
      this.put("DS_DNI", "");
    }
    if (d.get("DS_TELEF") == null) {
      this.put("DS_TELEF", "");
    }
    if (d.get("DS_FAX") == null) {
      this.put("DS_FAX", "");
    }
    if (d.get("FC_NAC") == null) {
      this.put("FC_NAC", "");
    }
    if (d.get("FC_ALTA") == null) {
      this.put("FC_ALTA", "");
    }
    if (d.get("NMREDADI") == null) {
      this.put("NMREDADI", "");
    }
    if (d.get("NMREDADF") == null) {
      this.put("NMREDADF", "");
    }
    if (d.get("DS_HORAI") == null) {
      this.put("DS_HORAI", "");
    }
    if (d.get("DS_HORAF") == null) {
      this.put("DS_HORAF", "");
    }
    if (d.get("IT_BAJA") == null) {
      this.put("IT_BAJA", "");
    }
    if (d.get("FC_BAJA") == null) {
      this.put("FC_BAJA", "");
    }
    if (d.get("CD_ANOBAJA") == null) {
      this.put("CD_ANOBAJA", "");
    }
    if (d.get("CD_SEMBAJA") == null) {
      this.put("DS_SEMBAJA", "");
    }
    if (d.get("CD_MOTBAJA") == null) {
      this.put("CD_MOTBAJA", "");
    }
    if (d.get("CD_OPE") == null) {
      this.put("CD_OPE", "");
    }
    if (d.get("FC_ULTACT") == null) {
      this.put("FC_ULTACT", "");
    }

  }

//_______________________________________________________

  public String getCD_PCENTI() {
    return getString("CD_PCENTI");
  } // Fin getCD_PCENTI()

  public String getCD_MEDCEN() {
    return getString("CD_MEDCEN");
  } // Fin getCD_MEDCEN()

  public String getCD_E_NOTIF() {
    return getString("CD_E_NOTIF");
  } // Fin getCD_E_NOTIF()

  public String getCD_SEXO() {
    return getString("CD_SEXO");
  } // Fin getCD_SEXO()

  public String getCD_MASIST() {
    return getString("CD_MASIST");
  } // Fin getCD_MASIST()

  public String getCD_ANOEPIA() {
    return getString("CD_ANOEPIA");
  } // Fin getCD_ANOEPIA()

  public String getCD_SEMEPIA() {
    return getString("CD_SEMEPIA");
  } // Fin getCD_SEMEPIA()

  public String getDS_APE1() {
    return getString("DS_APE1");
  } // Fin getDS_APE1()

  public String getDS_APE2() {
    return getString("DS_APE2");
  } // Fin getDS_APE2()

  public String getDS_NOMBRE() {
    return getString("DS_NOMBRE");
  } // Fin getDS_NOMBRE()

  public String getDS_DNI() {
    return getString("DS_DNI");
  } // Fin getDS_DNI()

  public String getDS_TELEF() {
    return getString("DS_TELEF");
  } // Fin getDS_TELEF()

  public String getDS_FAX() {
    return getString("DS_FAX");
  } // Fin getDS_FAX()

  public String getFC_NAC() {
    return getString("FC_NAC");
  } // Fin getFC_NAC()

  public String getFC_ALTA() {
    return getString("FC_ALTA");
  } // Fin getFC_ALTA()

  public String getNMREDADI() {
    return getString("NMREDADI");
  } // Fin getNMREDADI()

  public String getNMREDADF() {
    return getString("NMREDADF");
  } // Fin getNMREDADF()

  public String getDS_HORAI() {
    return getString("DS_HORAI");
  } // Fin getDS_HORAI()

  public String getDS_HORAF() {
    return getString("DS_HORAF");
  } // Fin getDS_HORAF()

  public String getIT_BAJA() {
    return getString("IT_BAJA");
  } // Fin getIT_BAJA()

  public String getFC_BAJA() {
    return getString("FC_BAJA");
  } // Fin getFC_BAJA()

  public String getCD_ANOBAJA() {
    return getString("CD_ANOBAJA");
  } // Fin getCD_ANOBAJA()

  public String getCD_SEMBAJA() {
    return getString("CD_SEMBAJA");
  } // Fin getCD_SEMBAJA()

  public String getCD_MOTBAJA() {
    return getString("CD_MOTBAJA");
  } // Fin getCD_MOTBAJA()

  public String getCD_OPE() {
    return getString("CD_OPE");
  } // Fin getCD_OPE()

  public String getFC_ULTACT() {
    return getString("FC_ULTACT");
  } // Fin getFC_ULTACT()

  public void setCD_OPE(String o) {
    remove("CD_OPE");
    put("CD_OPE", o);
  } // Fin getCD_OPE()

  public void setFC_ULTACT(String f) {
    remove("FC_ULTACT");
    put("FC_ULTACT", f);
  } // Fin getFC_ULTACT()

//_______________________________________________________________

  // SELECT * FROM SIVE_MCENTINELA
  public static QueryTool2 getAllMedCent() {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_MCENTINELA");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_E_NOTIF", QueryTool.STRING);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("CD_MASIST", QueryTool.STRING);
    qt.putType("CD_ANOEPIA", QueryTool.STRING);
    qt.putType("CD_SEMEPIA", QueryTool.STRING);
    qt.putType("DS_APE1", QueryTool.STRING);
    qt.putType("DS_APE2", QueryTool.STRING);
    qt.putType("DS_NOMBRE", QueryTool.STRING);
    qt.putType("DS_DNI", QueryTool.STRING);
    qt.putType("DS_TELEF", QueryTool.STRING);
    qt.putType("DS_FAX", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("NMREDADI", QueryTool.INTEGER);
    qt.putType("NMREDADF", QueryTool.INTEGER);
    qt.putType("DS_HORAI", QueryTool.STRING);
    qt.putType("DS_HORAF", QueryTool.STRING);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("FC_BAJA", QueryTool.DATE);
    qt.putType("CD_ANOBAJA", QueryTool.STRING);
    qt.putType("CD_SEMBAJA", QueryTool.STRING);
    qt.putType("CD_MOTBAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    return qt;
  } // Fin getAllMedCent()

//_______________________________________________________________

  // SELECT * FROM SIVE_MCENTINELA WHERE CD_PCENTI=? AND CD_MEDCEN=?
  public static QueryTool2 getOneMedCent(String codP, String codM) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_MCENTINELA");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_E_NOTIF", QueryTool.STRING);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("CD_MASIST", QueryTool.STRING);
    qt.putType("CD_ANOEPIA", QueryTool.STRING);
    qt.putType("CD_SEMEPIA", QueryTool.STRING);
    qt.putType("DS_APE1", QueryTool.STRING);
    qt.putType("DS_APE2", QueryTool.STRING);
    qt.putType("DS_NOMBRE", QueryTool.STRING);
    qt.putType("DS_DNI", QueryTool.STRING);
    qt.putType("DS_TELEF", QueryTool.STRING);
    qt.putType("DS_FAX", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("NMREDADI", QueryTool.INTEGER);
    qt.putType("NMREDADF", QueryTool.INTEGER);
    qt.putType("DS_HORAI", QueryTool.STRING);
    qt.putType("DS_HORAF", QueryTool.STRING);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("FC_BAJA", QueryTool.DATE);
    qt.putType("CD_ANOBAJA", QueryTool.STRING);
    qt.putType("CD_SEMBAJA", QueryTool.STRING);
    qt.putType("CD_MOTBAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", codP);
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", codM);
    qt.putOperator("CD_MEDCEN", "=");

    return qt;
  } // Fin getOneMedCent()

//_______________________________________________________________

  // INSERT INTO SIVE_MCENTINELA (...) VALUES (...);
  public static QueryTool2 getInsertOneMedCent(MedCent m) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (INTO)
    qt.putName("SIVE_MCENTINELA");

    // Campos que se quieren insertar
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_E_NOTIF", QueryTool.STRING);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("CD_MASIST", QueryTool.STRING);
    qt.putType("CD_ANOEPIA", QueryTool.STRING);
    qt.putType("CD_SEMEPIA", QueryTool.STRING);
    qt.putType("DS_APE1", QueryTool.STRING);
    qt.putType("DS_APE2", QueryTool.STRING);
    qt.putType("DS_NOMBRE", QueryTool.STRING);
    qt.putType("DS_DNI", QueryTool.STRING);
    qt.putType("DS_TELEF", QueryTool.STRING);
    qt.putType("DS_FAX", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("NMREDADI", QueryTool.INTEGER);
    qt.putType("NMREDADF", QueryTool.INTEGER);
    qt.putType("DS_HORAI", QueryTool.STRING);
    qt.putType("DS_HORAF", QueryTool.STRING);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("FC_BAJA", QueryTool.DATE);
    qt.putType("CD_ANOBAJA", QueryTool.STRING);
    qt.putType("CD_SEMBAJA", QueryTool.STRING);
    qt.putType("CD_MOTBAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores del nuevo registro
    qt.putValue("CD_PCENTI", m.getCD_PCENTI());
    qt.putValue("CD_MEDCEN", m.getCD_MEDCEN());
    qt.putValue("CD_E_NOTIF", m.getCD_E_NOTIF());
    qt.putValue("CD_SEXO", m.getCD_SEXO());
    qt.putValue("CD_MASIST", m.getCD_MASIST());
    qt.putValue("CD_ANOEPIA", m.getCD_ANOEPIA());
    qt.putValue("CD_SEMEPIA", m.getCD_SEMEPIA());
    qt.putValue("DS_APE1", m.getDS_APE1());
    qt.putValue("DS_APE2", m.getDS_APE2());
    qt.putValue("DS_NOMBRE", m.getDS_NOMBRE());
    qt.putValue("DS_DNI", m.getDS_DNI());
    qt.putValue("DS_TELEF", m.getDS_TELEF());
    qt.putValue("DS_FAX", m.getDS_FAX());
    qt.putValue("FC_NAC", m.getFC_NAC());
    qt.putValue("FC_ALTA", m.getFC_ALTA());
    qt.putValue("NMREDADI", m.getNMREDADI());
    qt.putValue("NMREDADF", m.getNMREDADF());
    qt.putValue("DS_HORAI", m.getDS_HORAI());
    qt.putValue("DS_HORAF", m.getDS_HORAF());
    qt.putValue("IT_BAJA", m.getIT_BAJA());
    qt.putValue("FC_BAJA", m.getFC_BAJA());
    qt.putValue("CD_ANOBAJA", m.getCD_ANOBAJA());
    qt.putValue("CD_SEMBAJA", m.getCD_SEMBAJA());
    qt.putValue("CD_MOTBAJA", m.getCD_MOTBAJA());
    qt.putValue("CD_OPE", m.getCD_OPE());
    qt.putValue("FC_ULTACT", m.getFC_ULTACT());

    return qt;
  } // Fin getInsertOneMedCent()

//_______________________________________________________________

  // UPDATE SIVE_MCENTINELA SET ... WHERE CD_PCENTI=? AND CD_MEDCEN=?
  public static QueryTool2 getUpdateOneMedCent(MedCent m) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (INTO)
    qt.putName("SIVE_MCENTINELA");

    // Campos que se quieren modificar
    //qt.putType("CD_PCENTI",QueryTool.STRING);
    //qt.putType("CD_MEDCEN",QueryTool.STRING);
    qt.putType("CD_E_NOTIF", QueryTool.STRING);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("CD_MASIST", QueryTool.STRING);
    qt.putType("CD_ANOEPIA", QueryTool.STRING);
    qt.putType("CD_SEMEPIA", QueryTool.STRING);
    qt.putType("DS_APE1", QueryTool.STRING);
    qt.putType("DS_APE2", QueryTool.STRING);
    qt.putType("DS_NOMBRE", QueryTool.STRING);
    qt.putType("DS_DNI", QueryTool.STRING);
    qt.putType("DS_TELEF", QueryTool.STRING);
    qt.putType("DS_FAX", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("NMREDADI", QueryTool.INTEGER);
    qt.putType("NMREDADF", QueryTool.INTEGER);
    qt.putType("DS_HORAI", QueryTool.STRING);
    qt.putType("DS_HORAF", QueryTool.STRING);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("FC_BAJA", QueryTool.DATE);
    qt.putType("CD_ANOBAJA", QueryTool.STRING);
    qt.putType("CD_SEMBAJA", QueryTool.STRING);
    qt.putType("CD_MOTBAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores de actualizaci�n
    //qt.putType("CD_PCENTI",m.getCD_PCENTI());
    //qt.putType("CD_MEDCEN",m.getCD_MEDCEN());
    qt.putValue("CD_E_NOTIF", m.getCD_E_NOTIF());
    qt.putValue("CD_SEXO", m.getCD_SEXO());
    qt.putValue("CD_MASIST", m.getCD_MASIST());
    qt.putValue("CD_ANOEPIA", m.getCD_ANOEPIA());
    qt.putValue("CD_SEMEPIA", m.getCD_SEMEPIA());
    qt.putValue("DS_APE1", m.getDS_APE1());
    qt.putValue("DS_APE2", m.getDS_APE2());
    qt.putValue("DS_NOMBRE", m.getDS_NOMBRE());
    qt.putValue("DS_DNI", m.getDS_DNI());
    qt.putValue("DS_TELEF", m.getDS_TELEF());
    qt.putValue("DS_FAX", m.getDS_FAX());
    qt.putValue("FC_NAC", m.getFC_NAC());
    qt.putValue("FC_ALTA", m.getFC_ALTA());
    qt.putValue("NMREDADI", m.getNMREDADI());
    qt.putValue("NMREDADF", m.getNMREDADF());
    qt.putValue("DS_HORAI", m.getDS_HORAI());
    qt.putValue("DS_HORAF", m.getDS_HORAF());
    qt.putValue("IT_BAJA", m.getIT_BAJA());
    qt.putValue("FC_BAJA", m.getFC_BAJA());
    qt.putValue("CD_ANOBAJA", m.getCD_ANOBAJA());
    qt.putValue("CD_SEMBAJA", m.getCD_SEMBAJA());
    qt.putValue("CD_MOTBAJA", m.getCD_MOTBAJA());
    qt.putValue("CD_OPE", m.getCD_OPE());
    qt.putValue("FC_ULTACT", m.getFC_ULTACT());

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", m.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", m.getCD_MEDCEN());
    qt.putOperator("CD_MEDCEN", "=");

    return qt;
  } // Fin getUpdateOneMedCent()

//_______________________________________________________________

  // DELETE FROM SIVE_MCENTINELA WHERE CD_PCENTI=? AND CD_MEDCEN=?
  public static QueryTool2 getDeleteOneMedCent(MedCent m) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_MCENTINELA");

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", m.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", m.getCD_MEDCEN());
    qt.putOperator("CD_MEDCEN", "=");

    return qt;
  } // Fin getDeleteOneMedCent()

//_______________________________________________________________

  // Otras queries

  // SELECT * FROM SIVE_MCENTINELA
  // WHERE CD_PCENTI = ?  AND IT_BAJA='N'
  public static QueryTool2 getMedCentActivoOfOnePuntoCent(String pCent) {
    // SELECT + FROM
    QueryTool2 qt = getAllMedCent();

    // Valores del WHERE
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", pCent);
    qt.putOperator("CD_PCENTI", "=");

    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "N");
    qt.putOperator("IT_BAJA", "=");

    return qt;

  } // Fin getMedCentPCentiAno()

} // endclass MedCent