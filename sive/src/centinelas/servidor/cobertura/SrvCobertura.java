/**
 * Clase: SrvCobertura
 * Paquete: centinelas.servidor.cobertura
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 08/02/2000
 * Analisis Funcional: Proceso de cobertura
 * Descripcion: Servlet que implementa los diferentes  modos de
 *   acceso al proceso de generaci�n de la cobertura
 */

package centinelas.servidor.cobertura;

import centinelas.datos.centbasicos.MedCent;
import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

public class SrvCobertura
    extends DBServlet {

  final protected int AVANCE_SEMGEN = 0; // Avance de semGen
  final protected int MIN_SEMALTA_INSMED = 1; // Minima A�o/Sem alta en la inseri�n de un m�dico
  final protected int INSMED = 2; // Inserci�n de un m�dico
  final protected int MAXMIN_SEMALTA_MODMED = 3; // Maxima/Minima A�o/Sem alta en la inserci�n de un m�dico
  final protected int MODMED = 4; // Modificaci�n de un m�dico
  final protected int MIN_SEMBAJA_BORMED = 5; // Minimo A�o/Sem de baja en el borrado de un medico
  final protected int BORMED = 6; // Borrado de un medico (fisico o por IT_BAJA)

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // Lista resultado
    Lista vResulset = new Lista();
    int iRes;
    Data dtRes;

    try {
      switch (opmode) {
        case AVANCE_SEMGEN:

          // Recogida de parametros
          String sAnoASG = (String) vParametros.elementAt(0);
          String sSemASG = (String) vParametros.elementAt(1);
          String sOpeASG = (String) vParametros.elementAt(2);

          // Generaci�n de cobertura si es necesario
          iRes = Cobertura.generarCobertura0(con, sAnoASG, sSemASG, sOpeASG);
          vResulset.addElement(new Integer(iRes));

          break;

        case MIN_SEMALTA_INSMED:

          // Recogida de parametros
          String sPuntoCentMSIM = (String) vParametros.elementAt(0);

          // Generaci�n de cobertura si es necesario
          dtRes = Cobertura.obtenerMinAnoSemAltaInsMed(con, sPuntoCentMSIM);
          vResulset.addElement(dtRes);

          break;

        case INSMED:

          // Recogida de parametros
          MedCent medIM = (MedCent) vParametros.elementAt(0);
          String sOpeIM = (String) vParametros.elementAt(1);

          // Generaci�n de cobertura si es necesario
          iRes = Cobertura.generarCobertura1(con, medIM, sOpeIM);
          vResulset.addElement(new Integer(iRes));

          break;

        case MAXMIN_SEMALTA_MODMED:

          // Recogida de parametros
          MedCent medMMSAMM = (MedCent) vParametros.elementAt(0);

          // Generaci�n de cobertura si es necesario
          dtRes = Cobertura.obtenerMaxMinAnoSemAltaModMed(con, medMMSAMM);
          vResulset.addElement(dtRes);

          break;

        case MODMED:

          // Recogida de parametros
          MedCent medMM = (MedCent) vParametros.elementAt(0);
          String oldAnoMM = (String) vParametros.elementAt(1);
          String oldSemMM = (String) vParametros.elementAt(2);
          String sOpeMM = (String) vParametros.elementAt(3);

          // Generaci�n de cobertura si es necesario
          iRes = Cobertura.generarCobertura2(con, medMM, oldAnoMM, oldSemMM,
                                             sOpeMM);
          vResulset.addElement(new Integer(iRes));

          break;

        case MIN_SEMBAJA_BORMED:

          // Recogida de parametros
          MedCent medMSBBM = (MedCent) vParametros.elementAt(0);

          // Generaci�n de cobertura si es necesario
          dtRes = Cobertura.obtenerMinAnoSemBajaBorMed(con, medMSBBM);
          vResulset.addElement(dtRes);

          break;

        case BORMED:

          // Recogida de parametros
          MedCent medBM = (MedCent) vParametros.elementAt(0);
          String sOpeBM = (String) vParametros.elementAt(1);

          // Generaci�n de cobertura si es necesario
          iRes = Cobertura.generarCobertura3(con, medBM, sOpeBM);
          vResulset.addElement(new Integer(iRes));

          break;

      } // Fin switch
    }
    catch (Exception e) {
      throw e;
    } // Fin try .. catch

    return vResulset;
  } // Fin doWork()

} // endclass SrvCobertura
