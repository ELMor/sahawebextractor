/*
 Servlet que env�a al cliente datos sobre los casos RMC
 ��MUY IMPORTANTE!!   (ARS 10-05-01)
 El protocolo de gripe, ha de crearse forzosamente con todas
 las preguntas que aparecen en el fax enviado por ICM y en el orden
 que muestra la estructura del fichero, es decir:
 1) Antecedente
 2) Sistema
 3) Env�o muestra
 4) Fecha env�o
 5) Aislamiento
 6) T�cnica
 7) Tipo
 8) Subtipo
 9) Cepa.
 Cualquier cambio de orden u omisi�n o exceso de preguntas
 provocar� que el volcado de fichero falle.
 ++++++++ (Actualizaci�n 17-05-01) +++++++++++++
 Se resuelve este problema, creando una tabla nueva:
 SIVE_PROTOCOLO_RMC, donde se almacenan posiciones de preguntas
 y c�digos de las mismas. De esta manera, es posible cambiar
 la pregunta, manteniendo el orden en que ser�n almacenadas en el fichero.
 Las posiciones de las preguntas, ser�n fijas, es decir, la primera
 pregunta siempre corresponder� con "Antecedente", la segunda
 con "Sistema", etc, etc. �nicamente puede cambiar el enunciado
 y la respuesta, pero la respuesta no puede tener un formato
 distinto al campo que le corresponda en el fichero, excepto
 la fecha, que se desbarra.
 */
package centinelas.servidor.enviocasosrmccne;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import comun.Fechas;
import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;

public class SrvEnvioCasosRmcCne
    extends DBServlet {

  final protected int servletSEL_CASOS = 1; //env�a al cliente datos sobre los casos RMC
  final protected int servletSEL_IND_CASOS = 2;

  final int[] longitud_de_campos = {
      1, 1, 1, 8, 1, 1, 1, 6, 25};

  protected Lista doWork(int opmode, Lista param) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    Lista listaSalida = new Lista();
    Data dataEntrada = null;
    Data dataSalida = null;
    boolean bFiltro = false;
    int iValorFiltro = 0;
    Lista lisPeticion = null;

    // *^********** DATOS PARA EL 2� MODO ************

    String sLinea = null; //Linea de un fichero
    int i = 1;
    int k; //Para �ndice b�squeda columnas
    int num_respuesta = 0; // Para recorrer la matriz con las longitudes de los ficheros.

    // objetos de datos
    Lista lData = null; //lista de resultado al cliente (lineas, es decir, Strings)
//    Data datFich = null;   //Para datos de petici�n
    Lista lDataAux = null; //lista auxiliar local (uso en Servlet) para brotes o brotes hist�ricos
    Lista lDevuelto = new Lista(); // Lista para devolver los resultados
    Lista lErrores = new Lista(); // Lista para devolver los errores.
    Data dtTemp = null;

    //Par�metros recibidos para generar fichero
    String sAnio_ini = "";
    String sAnio_fin = "";
    String sSemana_ini = "";
    String sSemana_fin = "";
    String sEnfcie = "";
    String sTSive = "";

    //Datos del modelo
    String sCodMod = "";
    String sDesRes = "";
    Lista vLineas = null;

    //Consulta que recoge los centinelas
    final String sLISTADO_CENTINELAS_SEM =
        "select CD_ENFCIE, CD_PCENTI, CD_MEDCEN, " +
        "CD_ANOEPI, CD_SEMEPI, NM_ORDEN, CD_SEXO, FC_NAC from SIVE_CASOS_CENT " +
        "WHERE CD_ENFCIE =? AND CD_ANOEPI=? AND CD_SEMEPI>=? AND CD_SEMEPI<=? ORDER BY CD_ANOEPI, CD_SEMEPI";

    final String sLISTADO_CENTINELAS_ANIO =
        "select CD_ENFCIE, CD_PCENTI, CD_MEDCEN, " +
        "CD_ANOEPI, CD_SEMEPI, NM_ORDEN, CD_SEXO, FC_NAC from SIVE_CASOS_CENT " +
        "WHERE CD_ENFCIE = ? AND (CD_ANOEPI=? AND CD_SEMEPI>=? OR CD_ANOEPI>? AND CD_ANOEPI<? OR CD_ANOEPI=? " +
        "AND CD_SEMEPI<=?) ORDER BY CD_ANOEPI, CD_SEMEPI";

    //Para recoger el modelo con respuestas correspondiente a un brote y al CNE
    // 10-05-01 (ARS) Cambio: CD_CA is not null
    // y IT_OK='S'
    final String sLISTADO_MODELOS = "select CD_MODELO from SIVE_MODELO where CD_TSIVE = 'C' and CD_NIVEL_1 is null and " +
        "CD_NIVEL_2 is null and CD_CA is not null and IT_OK='S' and CD_MODELO in " +
        "(select CD_MODELO from SIVE_RESP_CENTI where CD_ENFCIE = ? and CD_PCENTI = ? and " +
        "CD_MEDCEN = ? and CD_ANOEPI = ? and CD_SEMEPI = ? and NM_ORDEN = ? and CD_TSIVE = 'C')";

    final String sLISTADO_LINEAS =
        "select NM_LIN from SIVE_LINEA_ITEM where CD_MODELO = ? and CD_TSIVE='C' " +
        "order by NM_LIN";

    final String sLISTADO_RESPUESTAS =
        "select DS_RESPUESTA from SIVE_RESP_CENTI where CD_ENFCIE = ? and NM_ORDEN = ? " +
        "and CD_PCENTI =? and CD_MEDCEN =? and CD_ANOEPI =? and CD_SEMEPI =? " +
        "and CD_TSIVE ='C' and CD_MODELO = ? and NM_LIN = ? ";

    //_________________________ FIN 2� MODO __________________________________

    final String sSEL_CASOSSEMANA = "SELECT CD_ENFCIE, CD_PCENTI, " +
        "CD_MEDCEN, CD_ANOEPI, CD_SEMEPI, NM_ORDEN, FC_NAC " +
        "FROM SIVE_CASOS_CENT WHERE CD_ENFCIE=? " +
        "AND CD_ANOEPI=? AND " +
        "CD_SEMEPI>=? AND " +
        "CD_SEMEPI<=? " +
        "ORDER BY 4, 5";

    final String sSEL_CASOSANOSEMANA = "SELECT CD_ENFCIE, CD_PCENTI, " +
        "CD_MEDCEN, CD_ANOEPI, CD_SEMEPI, NM_ORDEN, FC_NAC " +
        "FROM SIVE_CASOS_CENT " +
        " WHERE CD_ENFCIE=? " +
        "AND ((CD_ANOEPI=? AND CD_SEMEPI>=?) OR " +
        "(CD_ANOEPI>? AND CD_ANOEPI<?) OR " +
        "(CD_ANOEPI=? AND CD_SEMEPI<=? ))" +
        "ORDER BY CD_ANOEPI, CD_SEMEPI";

    // Correcciones 16-05-01 (ARS).
    final String sSEL_COD_PREG =
        "select * from SIVE_PROTOCOLO_RMC order by NM_LIN asc";
    final String sSEL_DES_PREG =
        "select DS_RESPUESTA from SIVE_RESP_CENTI where CD_ENFCIE = ? and NM_ORDEN = ? " +
        "and CD_PCENTI =? and CD_MEDCEN =? and CD_ANOEPI =? and CD_SEMEPI =? " +
        "and CD_TSIVE ='C' and CD_MODELO = ? and CD_PREGUNTA = ? ";

    String codigo_centro = "";

    try {
      con.setAutoCommit(false);
      if (param.size() != 0) {
        dataEntrada = (Data) param.elementAt(0);
      }
      codigo_centro = (String) param.getParameter("COD_CENTRO");

      switch (opmode) {
        //Data q recibe: anoepini,semepiini,anoepifin,semepifin,cd_enfcie,cd_indalar
        case servletSEL_CASOS:
          String anoIni = dataEntrada.getString("CD_ANOEPIINI");
          String semIni = dataEntrada.getString("CD_SEMEPIINI");
          String anoFin = dataEntrada.getString("CD_ANOEPIFIN");
          String semFin = dataEntrada.getString("CD_SEMEPIFIN");

          int nmanoIni = Integer.parseInt(anoIni);
          int nmsemIni = Integer.parseInt(semIni);
          int nmanoFin = Integer.parseInt(anoFin);
          int nmsemFin = Integer.parseInt(semFin);

          //caso1: a�os iguales->filtro por semana (sSEL_CASOSSEMANA)
          if (nmanoIni == nmanoFin) {
            st = con.prepareStatement(sSEL_CASOSSEMANA);
            st.setString(1, dataEntrada.getString("CD_ENFCIE"));
            st.setString(2, anoIni);
            st.setString(3, semIni);
            st.setString(4, semFin);

          }
          else { //caso2: a�os distintos->filtro por a�o-semana(sSEL_CASOSANOSEMANA)
            st = con.prepareStatement(sSEL_CASOSANOSEMANA);

            st.setString(1, dataEntrada.getString("CD_ENFCIE"));
            st.setString(2, anoIni);
            st.setString(3, semIni);
            st.setString(4, anoIni);
            st.setString(5, anoFin);
            st.setString(6, anoFin);
            st.setString(7, semFin);

          }
          rs = st.executeQuery();
          while (rs.next()) {
            dataSalida = new Data();
            dataSalida.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
            dataSalida.put("CD_PCENTI", rs.getString("CD_PCENTI"));
            dataSalida.put("CD_MEDCEN", rs.getString("CD_MEDCEN"));
            dataSalida.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
            dataSalida.put("CD_SEMEPI", rs.getString("CD_SEMEPI"));
            dataSalida.put("NM_ORDEN",
                           (new Integer(rs.getInt("NM_ORDEN"))).toString());
            Date iDeclar = rs.getDate("FC_NAC");
            if (rs.wasNull()) {
              dataSalida.put("FC_NAC", "");
            }
            else {
              String sFecha = Fechas.date2String(iDeclar);
              dataSalida.put("FC_NAC", sFecha);
            }

            listaSalida.addElement(dataSalida);
          } //end while
          break;

// *****************************************************************
// *************************** OPCI�N 2 ****************************
// *****************************************************************
// Se comentan las l�neas del programa, para realizar la correcci�n.
// Se a�aden las l�neas del programa que son nuevas. (ARS 16-05-01)

        case servletSEL_IND_CASOS:

          // prepara la lista de resultados
          lData = new Lista();
          //Recogida Par�mettros enviados
          sAnio_ini = dataEntrada.getString("CD_ANOEPIINI");
          sSemana_ini = dataEntrada.getString("CD_SEMEPIINI");
          sAnio_fin = dataEntrada.getString("CD_ANOEPIFIN");
          sSemana_fin = dataEntrada.getString("CD_SEMEPIFIN");
          sEnfcie = dataEntrada.getString("CD_ENFCIE");
          sTSive = dataEntrada.getString("CD_TSIVE");

          try {
            //No se va a modificar la b. datos
            con.setAutoCommit(true);
            //Se crea la nueva lista para
            lDataAux = new Lista();

            // prepara la consulta
            // A�os iguales
            if (sAnio_ini.equals(sAnio_fin)) {
              st = con.prepareStatement(sLISTADO_CENTINELAS_SEM);
              st.setString(1, sEnfcie);
              st.setString(2, sAnio_ini);
              st.setString(3, sSemana_ini);
              st.setString(4, sSemana_fin);
            }
            else {
              st = con.prepareStatement(sLISTADO_CENTINELAS_ANIO);
              st.setString(1, sEnfcie);
              st.setString(2, sAnio_ini);
              st.setString(3, sSemana_ini);
              st.setString(4, sAnio_ini);
              st.setString(5, sAnio_fin);
              st.setString(6, sAnio_fin);
              st.setString(7, sSemana_fin);
            }
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              dtTemp = new Data();

              dtTemp.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
              dtTemp.put("CD_PCENTI", rs.getString("CD_PCENTI"));
              dtTemp.put("CD_MEDCEN", rs.getString("CD_MEDCEN"));
              dtTemp.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
              dtTemp.put("CD_SEMEPI", rs.getString("CD_SEMEPI"));
              dtTemp.put("NM_ORDEN", String.valueOf(rs.getInt("NM_ORDEN")));
              if (rs.getString("CD_SEXO") != null) {
                dtTemp.put("CD_SEXO", rs.getString("CD_SEXO"));
              }
              else {
                dtTemp.put("CD_SEXO", "");
              }
              if (rs.getDate("FC_NAC") != null) {
                dtTemp.put("FC_NAC", Format.date2String(rs.getDate("FC_NAC")));
              }
              else {
                dtTemp.put("FC_NAC", "");

                // a�ade un nodo
              }
              lDataAux.addElement(dtTemp);
              dtTemp = null;
            }

            rs.close();
            st.close();
            rs = null;
            st = null;

            dtTemp = null;
            dtTemp = new Data();

            for (int contador = 0; contador < lDataAux.size(); contador++) {
              dtTemp = (Data) (lDataAux.elementAt(contador));

              int numModelos = 0; //N�mero de modelos que cumplen la query LISTADO_MODELO
              //Si no hay error debe ser solo uno
              st = con.prepareStatement(sLISTADO_MODELOS);
              st.setString(1, sEnfcie);
              st.setString(2, dtTemp.getString("CD_PCENTI"));
              st.setString(3, dtTemp.getString("CD_MEDCEN"));
              st.setString(4, dtTemp.getString("CD_ANOEPI"));
              st.setString(5, dtTemp.getString("CD_SEMEPI"));
              st.setString(6, dtTemp.getString("NM_ORDEN"));
              rs = st.executeQuery();

              // extrae la p�gina requerida
              while (rs.next()) {
                // obtiene los campos
                sCodMod = rs.getString("CD_MODELO");
                numModelos++;
              }
              rs.close();
              st.close();
              st = null;
              rs = null;

              //Si hay un solo modelo todo OK
              if (numModelos == 1) {
                sLinea = codigo_centro +
                    completaLongitud(dtTemp.getString("CD_SEMEPI"), 2, true) +
                    completaLongitud(dtTemp.getString("CD_ANOEPI"), 4, true) +
                    completaLongitud(calcularEdad(dtTemp.getString("FC_NAC")),
                                     2, false) +
                    completaLongitud(dtTemp.getString("CD_SEXO"), 1, true);

// Parte comentada (16-05-01 ARS)
// Se crea una nueva consulta.

                    /*                   st = con.prepareStatement(sLISTADO_LINEAS);
                                   st.setString(1,sCodMod);
                                   rs = st.executeQuery();
                                   // extrae la p�gina requerida
                                   vLineas = new Lista();
                                   Data cargaLinea = null;
                                   int iNumLin = 0;
                                   while (rs.next()) {
                                      // obtiene los campos
                                      iNumLin = rs.getInt("NM_LIN");
                                      cargaLinea = new Data ();
                     cargaLinea.put("LINEA",String.valueOf(iNumLin));
                                      vLineas.addElement(cargaLinea);
                                      cargaLinea = null;
                                   }
                                   rs.close();
                                   st.close();
                                   st = null;
                                   rs = null;*/

                st = con.prepareStatement(sSEL_COD_PREG);
                rs = st.executeQuery();

                // extrae la p�gina requerida
                vLineas = new Lista();
                Data cargaLinea = null;
                String cd_preg = "";

                while (rs.next()) {
                  // obtiene los campos
                  cd_preg = rs.getString("CD_PREGUNTA");
                  cargaLinea = new Data();
                  cargaLinea.put("CD_PREGUNTA", cd_preg);
                  vLineas.addElement(cargaLinea);
                  cargaLinea = null;
                }
                rs.close();
                st.close();
                st = null;
                rs = null;

// Esta consulta tambi�n se quita (ARS 16-05-01)
// Se sustituye.

                /*                  num_respuesta = 0;
                                  for (int j =0; j< vLineas.size();j++)  {
                     st = con.prepareStatement(sLISTADO_RESPUESTAS);
                                    st.setString(1, sEnfcie);
                     st.setInt(2, Integer.parseInt(dtTemp.getString("NM_ORDEN")));
                     st.setString(3,dtTemp.getString("CD_PCENTI"));
                     st.setString(4,dtTemp.getString("CD_MEDCEN"));
                     st.setString(5,dtTemp.getString("CD_ANOEPI"));
                     st.setString(6,dtTemp.getString("CD_SEMEPI"));
                                    st.setString(7,sCodMod);
                     st.setInt(8,Integer.parseInt(((Data)vLineas.elementAt(j)).getString("LINEA")));
                                    rs = st.executeQuery();
                                    // extrae la p�gina requerida
                                     while (rs.next()) {
                                      // obtiene los campos
                                        sDesRes = rs.getString("DS_RESPUESTA");
                                        if (num_respuesta == 3)
                                      // Correcci�n r�pida de �ltima hora (ARS 10-05-01)
                                      // si es fecha, desb�rrala
//                        if (esFecha(sDesRes))
                                          sDesRes = desbarraFecha (sDesRes);
                                        sLinea = sLinea + completaLongitud(sDesRes,longitud_de_campos[num_respuesta],true) ;
                                     }
                                    rs.close();
                                    st.close();
                                    st = null;
                                    rs = null;
                                    num_respuesta++;
                                  } //For recorre lineas
                 +++++ FIN comentario 16-05-01
                 */
                num_respuesta = 0;
                for (int j = 0; j < vLineas.size(); j++) {
                  st = con.prepareStatement(sSEL_DES_PREG);
                  st.setString(1, sEnfcie);
                  st.setInt(2, Integer.parseInt(dtTemp.getString("NM_ORDEN")));
                  st.setString(3, dtTemp.getString("CD_PCENTI"));
                  st.setString(4, dtTemp.getString("CD_MEDCEN"));
                  st.setString(5, dtTemp.getString("CD_ANOEPI"));
                  st.setString(6, dtTemp.getString("CD_SEMEPI"));
                  st.setString(7, sCodMod);
                  st.setString(8,
                               ( (Data) vLineas.elementAt(j)).getString("CD_PREGUNTA"));
                  rs = st.executeQuery();

                  // extrae la p�gina requerida
                  while (rs.next()) {
                    // obtiene los campos
                    sDesRes = rs.getString("DS_RESPUESTA");

                    if (num_respuesta == 3) {

                      // Correcci�n (ARS 10-05-01)
                      // si es fecha, desb�rrala
//                        if (esFecha(sDesRes))
                      sDesRes = desbarraFecha(sDesRes);
                    }
                    sLinea = sLinea +
                        completaLongitud(sDesRes,
                                         longitud_de_campos[num_respuesta], true);
                  }

                  rs.close();
                  st.close();
                  st = null;
                  rs = null;
                  num_respuesta++;
                } // final del bucle 'for'
// ++++++++++++++ FIN de la modificaci�n
              }
              else {
                //if hay  m�s de un modelo , error

                if (numModelos > 1) {
                  throw new Exception("Encontrado m�s de un modelo");
                }
                else {

                  // No hay modelos para brote (numModelos = 0). Se meten datos del modelo
                  sLinea = codigo_centro +
                      completaLongitud(dtTemp.getString("CD_SEMEPI"), 2, true) +
                      completaLongitud(dtTemp.getString("CD_ANOEPI"), 4, true) +
                      completaLongitud(calcularEdad(dtTemp.getString("FC_NAC")),
                                       2, false) +
                      completaLongitud(dtTemp.getString("CD_SEXO"), 1, true);
                }
              } // Del if del n�mero de modelos, 1, m�s o 0.
              for (int p = sLinea.length(); p < 56; p++) {
                sLinea += " ";
              }
              lDevuelto.addElement(sLinea);
            } //while recorrido de vector
          }
          catch (Exception ex) {
            throw ex;
          }
          listaSalida.addElement(lDevuelto);
          listaSalida.addElement(lErrores);
          break;
      } //end switch

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);
    return listaSalida;
  } // Fin doWork()

  /*
     private boolean esFecha (String sDato) {
    boolean estado = true;
    int num_separador = 0;
    int elemento = 0;
    StringTokenizer st = new StringTokenizer (sDato,"/");
    num_separador = st.countTokens()-1;
    if (num_separador==2) {
        while(st.hasMoreElements()) {
          try {
            elemento = Integer.parseInt((String)st.nextElement());
            estado = estado & true;
          } catch (Exception e) {
            estado = estado & false;
          }
        }
    } else
      estado = estado & false;
    return estado;
     }
   */
  //Con la fecha de nacimiento te calcula la edad actual en a�os
  private String calcularEdad(String fecNac) {
    String edadA = null;
    Integer aux = null;
    Date dateNac = Fechas.string2Date(fecNac);

    if (dateNac != null) {
      Date hoy = new Date();
      long mili_anio = 31557600000L;
      long mili_mes = 2626560000L;
      long edad_mili = hoy.getTime() - dateNac.getTime();

      if (edad_mili > 0) {
        aux = new Integer( (int) (edad_mili / mili_anio));
      }
      else {
        aux = new Integer(0);
      }
      edadA = aux.toString();
    }
    else {
      edadA = "99";
    }
    return edadA;
  }

  private String completaLongitud(String valor, int longitud, boolean izq) {
    String salida = valor;
    int contador = 0;
    if (valor.length() < longitud) {
      for (contador = valor.length(); contador < longitud; contador++) {
        if (izq) {
          salida += " ";
        }
        else {
          salida = " " + salida;
        }
      }
    }
    else {
      salida = salida.substring(0, longitud);
    }
    return salida;
  }

  private String desbarraFecha(String fecha) {
    String salida = "";
    salida = fecha.substring(0, 2);
    salida += fecha.substring(3, 5);
    salida += fecha.substring(6, 10);
    return salida;
  }

// PARA PROBAR ***************************

  public static void main(String args[]) {
    SrvEnvioCasosRmcCne srv = new SrvEnvioCasosRmcCne();
    Data dtDatos = new Data();
    Lista palServidor = new Lista();
    Lista devuelto = new Lista();
    Lista lParaqui = new Lista();
    Lista lListilla = new Lista();

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    dtDatos.put("CD_ANOEPIINI", "2001");
    dtDatos.put("CD_ANOEPIFIN", "2001");
    dtDatos.put("CD_SEMEPIINI", "12");
    dtDatos.put("CD_SEMEPIFIN", "14");
    dtDatos.put("CD_ENFCIE", "487");
    dtDatos.put("CD_TSIVE", "C");

    palServidor.addElement(dtDatos);

    lParaqui = srv.doDebug(2, palServidor);
    srv = null;
    for (int i = 0; i < lParaqui.size(); i++) {
      lListilla = (Lista) lParaqui.elementAt(i);
      for (int j = 0; j < lListilla.size(); j++) {
        System.out.println("<" + i + "," + j + ">" + lListilla.elementAt(j));
      }
    }
  }
}
