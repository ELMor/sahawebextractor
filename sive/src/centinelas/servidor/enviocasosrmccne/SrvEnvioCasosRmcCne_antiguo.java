/*
 Servlet que env�a al cliente datos sobre los casos RMC
 */
package centinelas.servidor.enviocasosrmccne;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import comun.Fechas;
import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

public class SrvEnvioCasosRmcCne_antiguo
    extends DBServlet {

  final protected int servletSEL_CASOS = 1; //env�a al cliente datos sobre los casos RMC

  protected Lista doWork(int opmode, Lista param) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    Lista listaSalida = new Lista();
    Data dataEntrada = null;
    Data dataSalida = null;
    boolean bFiltro = false;
    int iValorFiltro = 0;
    Lista lisPeticion = null;

    final String sSEL_CASOSSEMANA = "SELECT CD_ENFCIE, CD_PCENTI, " +
        "CD_MEDCEN, CD_ANOEPI, CD_SEMEPI, NM_ORDEN, FC_NAC " +
        "FROM SIVE_CASOS_CENT WHERE CD_ENFCIE=? " +
        "AND CD_ANOEPI=? AND " +
        "CD_SEMEPI>=? AND " +
        "CD_SEMEPI<=? " +
        "ORDER BY 4, 5";

    final String sSEL_CASOSANOSEMANA = "SELECT CD_ENFCIE, CD_PCENTI, " +
        "CD_MEDCEN, CD_ANOEPI, CD_SEMEPI, NM_ORDEN, FC_NAC " +
        "FROM SIVE_CASOS_CENT " +
        " WHERE CD_ENFCIE=? " +
        "AND ((CD_ANOEPI=? AND CD_SEMEPI>=?) OR " +
        "(CD_ANOEPI>? AND CD_ANOEPI<?) OR " +
        "(CD_ANOEPI=? AND CD_SEMEPI<=? ))" +
        "ORDER BY CD_ANOEPI, CD_SEMEPI";

    try {
      con.setAutoCommit(false);
      if (param.size() != 0) {
        dataEntrada = (Data) param.elementAt(0);
      }

      switch (opmode) {
        //Data q recibe: anoepini,semepiini,anoepifin,semepifin,cd_enfcie,cd_indalar
        case servletSEL_CASOS:
          String anoIni = dataEntrada.getString("CD_ANOEPIINI");
          String semIni = dataEntrada.getString("CD_SEMEPIINI");
          String anoFin = dataEntrada.getString("CD_ANOEPIFIN");
          String semFin = dataEntrada.getString("CD_SEMEPIFIN");

          int nmanoIni = Integer.parseInt(anoIni);
          int nmsemIni = Integer.parseInt(semIni);
          int nmanoFin = Integer.parseInt(anoFin);
          int nmsemFin = Integer.parseInt(semFin);

          //caso1: a�os iguales->filtro por semana (sSEL_CASOSSEMANA)
          if (nmanoIni == nmanoFin) {
            st = con.prepareStatement(sSEL_CASOSSEMANA);

            st.setString(1, dataEntrada.getString("CD_ENFCIE"));
            st.setString(2, anoIni);
            st.setString(3, semIni);
            st.setString(4, semFin);

          }
          else { //caso2: a�os distintos->filtro por a�o-semana(sSEL_CASOSANOSEMANA)
            st = con.prepareStatement(sSEL_CASOSANOSEMANA);

            st.setString(1, dataEntrada.getString("CD_ENFCIE"));
            st.setString(2, anoIni);
            st.setString(3, semIni);
            st.setString(4, anoIni);
            st.setString(5, anoFin);
            st.setString(6, anoFin);
            st.setString(7, semFin);

          }
          rs = st.executeQuery();
          while (rs.next()) {
            dataSalida = new Data();
            dataSalida.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
            dataSalida.put("CD_PCENTI", rs.getString("CD_PCENTI"));
            dataSalida.put("CD_MEDCEN", rs.getString("CD_MEDCEN"));
            dataSalida.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
            dataSalida.put("CD_SEMEPI", rs.getString("CD_SEMEPI"));
            dataSalida.put("NM_ORDEN",
                           (new Integer(rs.getInt("NM_ORDEN"))).toString());
            Date iDeclar = rs.getDate("FC_NAC");
            if (rs.wasNull()) {
              dataSalida.put("FC_NAC", "");
            }
            else {
              String sFecha = Fechas.date2String(iDeclar);
              dataSalida.put("FC_NAC", sFecha);
            }

            listaSalida.addElement(dataSalida);
          } //end while
          break;
      } //end switch

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);
    return listaSalida;
  } // Fin doWork()

}