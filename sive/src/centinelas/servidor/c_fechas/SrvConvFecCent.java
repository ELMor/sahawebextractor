//_____________

/*Clase
 * Autor: Luis Rivera, Jesus
 * Fecha Inicio: 1999
 * Descripcion: Srvlet Para crear a�os,semanas epidemiol�gicas y obtenerlos
 * Modificaciones:
 * Modificada para adaptaci�n de EDO a centinelas (cambio nombres tablas)
 *
 *
 **/

package centinelas.servidor.c_fechas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Vector;

import capp.CLista;
import centinelas.datos.c_fechas.DataConvFec;
import centinelas.datos.c_fechas.FecyNum;
import sapp.DBServlet;

public class SrvConvFecCent
    extends DBServlet {

  // modos de operaci�n
  final int servletGENERAR_FECHAS = 0;
  final int servletGENERAR_FECHAS_DE_UN_A�O = 1;
  final int servletGENERAR_A�O_Y_COPIAR_POBLAC = 2;
  final int servletOBTENER = 4;

  SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int k; //Para �ndice b�squeda columnas

    // objetos de datos
    CLista data = null;
    DataConvFec datConvFec = null;
    Enumeration enum; //Para recorrer listas

    //Para generar todas las fechas solamente
    String sFecInicial = ""; //fecha ult sem a�o epidem anterior
    String anoIni = ""; //A�o de partida
    int numAnos = 1; //numero de a�os de los que queremos generar fechas

    //Variables auxiliares para generar todas las fechas
    java.util.Date fecFinInicial; // fecha  de fin de ultima sem epid del primer a�o tratado
    java.util.Date fecFinAnt; // fecha  de fin de ultima sem epid del a�o anterior
    int iAno;
    String sFecUvas; // Fecha del 31 de a�o en cuestion
    java.util.Date fecUvas; // Fecha del 31 de a�o en cuestion
    java.util.Date fecRef; //Para avanzar con las semanas
    java.util.Date fecSig; //Para avanzar con las semanas
    boolean encontrado; //Para ver si hemos llegado a fin de a�o
    Calendar cal = new GregorianCalendar();
    java.util.Date fecComAno; //fecha de comienzo del a�o epid (dia sigte a fecha de fin del anterior)

    //Para las fechas de un a�o  (generarlas o obtenerlas)
    String sAno = ""; //A�o
    Vector vFec = new Vector(); //Vector que contiene los FecyNum de ese a�o
    boolean bHayFec; //Dice si hay fechas o no

    //En cada semana epidemiol�gica
    FecyNum semana;
    int iNum; //numero de semana ep en el a�o
    String sNum; //numero de semana ep en el a�o
    String query = "";
    java.util.Date dFec; //Fecha correspondte a la semana
    java.sql.Date sqlFec; //Fecha correspondte a la semana

    //  Crear campos para conversiones: distinto tipo en elem de CLista  y en b.datos
    //hacer en dos pasos

    final String sALTA_ANOEPI = "insert into SIVE_ANO_EPI_RMC (CD_ANOEPI, FC_INIEPI, FC_FINEPI) values (?, ?, ?)";
    final String sALTA_SEMEPI = "insert into SIVE_SEMANA_EPI_RMC (CD_ANOEPI, CD_SEMEPI, FC_FINEPI) values (?, ?, ?)";
    final String sBUSQUEDA = "select CD_ANOEPI, CD_SEMEPI, FC_FINEPI from SIVE_SEMANA_EPI_RMC where  CD_ANOEPI = ? order by FC_FINEPI";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    datConvFec = (DataConvFec) param.firstElement();

    try {

      // modos de operaci�n
      switch (opmode) {

        case servletGENERAR_FECHAS_DE_UN_A�O:
          anoIni = datConvFec.getAnoIni();
          int anyoAuxiliar = Integer.parseInt(anoIni);

          query =
              "select * from sive_semana_epi_rmc where CD_ANOEPI=? order by CD_SEMEPI desc";
          st = con.prepareStatement(query);
          st.setString(1, Integer.toString(anyoAuxiliar));
          rs = st.executeQuery();

          //Si no existen semanas a�o indicado:
          if (!rs.next()) {

            st.close();
            rs.close();
            st = null;
            rs = null;

            //Busca fecha �ltima semana del a�o anterior
            anyoAuxiliar = Integer.parseInt(anoIni) - 1;
            query =
                "select * from sive_semana_epi_rmc where CD_ANOEPI=? order by CD_SEMEPI desc";
            st = con.prepareStatement(query);
            st.setString(1, Integer.toString(anyoAuxiliar));
            rs = st.executeQuery();

            if (!rs.next()) { // error-no estaba creado el anterior
              Exception ew = null;
              ew = new Exception("No est� generado el a�o anterior.");
              throw ew;
            }
            else {
              numAnos = datConvFec.getNumAnos();
              sFecInicial = formateador.format(rs.getDate("FC_FINEPI"));
              //Pasa el string de fecha inicial a Date inicial
              fecFinInicial = formateador.parse(sFecInicial);
              //ultima fecha (de comienzo sem epi )del a�o anterior
              fecFinAnt = fecFinInicial;
              for (int cont = 0; cont < numAnos; cont++) {
                vFec.removeAllElements();
                //Calculo el String del a�o;
                iAno = Integer.parseInt(anoIni) + cont;
                sAno = Integer.toString(iAno);
                //Calculo la fecha del 31 de dic. de ese a�o
                sFecUvas = "31/12/" + sAno;

                fecUvas = formateador.parse(sFecUvas);
                //calculo fechas de sems

                //Fecha de ult sem pasa a ser como lo  que tenia sComAno

                fecRef = fecFinAnt;
                encontrado = false;
                int j = 0;

                while ( (encontrado == false) && (j < 54)) {
                  fecSig = new java.util.Date(0);
                  //A�ade 7 dias m�s en cada ciclo  a fecSig ( se multiplica por num de milisegundos de un dia)
                  fecSig.setTime(fecRef.getTime() +
                                 (long) (7 * 24 * 3600 * 1000));

                  //Si no hemos llegado al fin de a�o
                  if (fecUvas.after(fecSig)) {
                    FecyNum elem = new FecyNum(j + 1, fecSig);
                    vFec.addElement(elem);
                    //avanzamos en la referencia
                    fecRef = fecSig;
                    encontrado = false;
                  }

                  //Si hemos llegado al fin de a�o
                  else {
                    encontrado = true;

                    //ve si hay que meter tambi�n en el a�o ep. la semana del 31 de diciembre
                    cal.setTime(fecUvas);

                    if ( //Semana entra
                        (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) ||
                        (cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) ||
                        (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) ||
                        (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                        ) {
                      FecyNum elem = new FecyNum(j + 1, fecSig);
                      vFec.addElement(elem);
                      fecRef = fecSig;
                    }
                    else {
                    }
                  }
                  j++;

                } //while  de fechas de un a�o

                //A�ade ese a�o a tabla a�o epid
                // prepara la query
                st = con.prepareStatement(sALTA_ANOEPI);
                k = 0;
                //C�digo de a�o
                k++;
                st.setString(k, sAno);
                //Fecha inicio de a�o epidemoiol�gico (fin del anterior)
                //A�ade 1 dias m�s a fecFinAnt( se multiplica por num de milisegundos de un dia)
                k++;
                fecComAno = new java.util.Date(0);
                fecComAno.setTime(fecFinAnt.getTime() +
                                  (long) (24 * 3600 * 1000));
                st.setDate(k, new java.sql.Date(fecComAno.getTime()));
                //Fecha fin de a�o epidemoiol�gico
                k++;
                st.setDate(k, new java.sql.Date(fecRef.getTime()));
                // lanza la query
                st.executeUpdate();
                st.close();
                st = null;

                //Deja preparada la referencia para c�lculo fechas del sigte a�o
                fecFinAnt = fecRef; //ultima fecha metida en vector del a�o anterior

                //A�ade cada semana epidemiol�gica a tabla de semanas epid de un a�o
                enum = vFec.elements();
                while (enum.hasMoreElements()) {
                  semana = (FecyNum) (enum.nextElement());
                  //A�ade esa fecha a tabla sem epid
                  // prepara la query
                  st = con.prepareStatement(sALTA_SEMEPI);
                  k = 0;
                  //C�digo de a�o
                  k++;
                  st.setString(k, sAno);
                  //C�digo de semana
                  k++;
                  sNum = Integer.toString(semana.getnum());
                  if (sNum.length() == 1) {
                    sNum = "0" + sNum;
                  }
                  st.setString(k, sNum);
                  //Fecha fin de semana epidemoiol�gica
                  k++;
                  st.setDate(k, new java.sql.Date(semana.getFec().getTime()));
                  // lanza la query
                  st.executeUpdate();
                  st.close();
                  st = null;

                }

              }
            }
          }
          else {
            Exception ew = null;
            ew = new Exception("El a�o ya estaba generado.");
            throw ew;
          }
          break;

          // alta
        case servletGENERAR_FECHAS:

          sFecInicial = datConvFec.getFecFinAnt().trim();
          anoIni = datConvFec.getAnoIni().trim();
//           //# System.Out.println("anoIni " + anoIni   );//*******************
          numAnos = datConvFec.getNumAnos();
          //          //# System.Out.println("numAnos " + numAnos   );//*******************

           //Pasa el string de fecha inicial a Date inicial
          fecFinInicial = formateador.parse(sFecInicial);

          //ultima fecha (de comienzo sem epi )del a�o anterior
          fecFinAnt = fecFinInicial;
//           //# System.Out.println("Fecha fin ant " +formateador.format(fecFinAnt )  );//*******************

          for (int cont = 0; cont < numAnos; cont++) {

            vFec.removeAllElements();

            //Calculo el String del a�o;
            iAno = Integer.parseInt(anoIni) + cont;
            sAno = Integer.toString(iAno);

            //Calculo la fecha del 31 de dic. de ese a�o
            sFecUvas = "31/12/" + sAno;
            fecUvas = formateador.parse(sFecUvas);

            //          //# System.Out.println("Fecha uvas " +formateador.format(fecUvas )  );//*******************
             //calculo fechas de sems

             //Fecha de ult sem pasa a ser como lo  que tenia sComAno

            fecRef = fecFinAnt;
            //         //# System.Out.println("Fecha ref " +formateador.format(fecRef )  );
            encontrado = false;
            int j = 0;

            while ( (encontrado == false) && (j < 54)) {
              fecSig = new java.util.Date(0);

              //A�ade 7 dias m�s en cada ciclo  a fecSig ( se multiplica por num de milisegundos de un dia)
              fecSig.setTime(fecRef.getTime() + (long) (7 * 24 * 3600 * 1000));
              //         //# System.Out.println("Fecha sig " +formateador.format(fecSig )  );//***********

               //Si no hemos llegado al fin de a�o
              if (fecUvas.after(fecSig)) {
                FecyNum elem = new FecyNum(j + 1, fecSig);
//              //# System.Out.println("meto elem "+ j +"  "+formateador.format( elem.getFec() )  );
                vFec.addElement(elem);
                //avanzamos en la referencia
                fecRef = fecSig;
                encontrado = false;
              }

              //Si hemos llegado al fin de a�o
              else {
                encontrado = true;

                //ve si hay que meter tambi�n en el a�o ep. la semana del 31 de diciembre
                cal.setTime(fecUvas);

                if ( //Semana entra
                    (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) ||
                    (cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) ||
                    (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) ||
                    (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                    ) {
                  FecyNum elem = new FecyNum(j + 1, fecSig);
//                   //# System.Out.println("meto elem "+j +"  " + formateador.format( elem.getFec() )  );
//                   //# System.Out.println("sem 31 incluida pilla en "  + cal.get(Calendar.DAY_OF_WEEK));
                  vFec.addElement(elem);
                  fecRef = fecSig;
                }
                else {
//              //# System.Out.println("sem 31 NO incl pilla en "  + cal.get(Calendar.DAY_OF_WEEK));
                }
              }
              j++;

            } //while  de fechas de un a�o

//            //# System.Out.println("tam vFec" + vFec.size() );

            //A�ade ese a�o a tabla a�o epid
            // prepara la query
            st = con.prepareStatement(sALTA_ANOEPI);
            k = 0;
            //C�digo de a�o
            k++;
            st.setString(k, sAno);
            //Fecha inicio de a�o epidemoiol�gico (fin del anterior)
            //A�ade 1 dias m�s a fecFinAnt( se multiplica por num de milisegundos de un dia)
            k++;
            fecComAno = new java.util.Date(0);
            fecComAno.setTime(fecFinAnt.getTime() + (long) (24 * 3600 * 1000));
            st.setDate(k, new java.sql.Date(fecComAno.getTime()));
            //Fecha fin de a�o epidemoiol�gico
            k++;
            st.setDate(k, new java.sql.Date(fecRef.getTime()));
            // lanza la query
            st.executeUpdate();
            st.close();
            st = null;

            //Deja preparada la referencia para c�lculo fechas del sigte a�o
            fecFinAnt = fecRef; //ultima fecha metida en vector del a�o anterior

            //A�ade cada semana epidemiol�gica a tabla de semanas epid de un a�o
            enum = vFec.elements();
            while (enum.hasMoreElements()) {
              semana = (FecyNum) (enum.nextElement());
              //A�ade esa fecha a tabla sem epid
              // prepara la query
              st = con.prepareStatement(sALTA_SEMEPI);
              k = 0;
              //C�digo de a�o
              k++;
              st.setString(k, sAno);
              //C�digo de semana
              k++;
              sNum = Integer.toString(semana.getnum());
              if (sNum.length() == 1) {
                sNum = "0" + sNum;
              }
              st.setString(k, sNum);
              //Fecha fin de semana epidemoiol�gica
              k++;
              st.setDate(k, new java.sql.Date(semana.getFec().getTime()));
              // lanza la query
              st.executeUpdate();
              st.close();
              st = null;

            }

          }

          break;

          // Opci�n de generar el a�o epidemiol�gico copiando adem�s las pobla-
          // ciones de los m�dicos centinelas que siguen dados de alta.
        case servletGENERAR_A�O_Y_COPIAR_POBLAC:
          anoIni = datConvFec.getAnoIni();
          anyoAuxiliar = Integer.parseInt(anoIni);

          query =
              "select * from sive_semana_epi_rmc where CD_ANOEPI=? order by CD_SEMEPI desc";
          st = con.prepareStatement(query);
          st.setString(1, Integer.toString(anyoAuxiliar));
          rs = st.executeQuery();

          //Si no existen semanas a�o indicado:
          if (!rs.next()) {

            st.close();
            rs.close();
            st = null;
            rs = null;

            //Busca fecha �ltima semana del a�o anterior
            anyoAuxiliar = Integer.parseInt(anoIni) - 1;
            query =
                "select * from sive_semana_epi_rmc where CD_ANOEPI=? order by CD_SEMEPI desc";
            st = con.prepareStatement(query);
            st.setString(1, Integer.toString(anyoAuxiliar));
            rs = st.executeQuery();

            if (!rs.next()) { // error-no estaba creado el anterior
              Exception ew = null;
              ew = new Exception("No est� generado el a�o anterior.");
              throw ew;
            }
            else {
              numAnos = datConvFec.getNumAnos();
              sFecInicial = formateador.format(rs.getDate("FC_FINEPI"));
              //Pasa el string de fecha inicial a Date inicial
              fecFinInicial = formateador.parse(sFecInicial);
              //ultima fecha (de comienzo sem epi )del a�o anterior
              fecFinAnt = fecFinInicial;
              for (int cont = 0; cont < numAnos; cont++) {
                vFec.removeAllElements();
                //Calculo el String del a�o;
                iAno = Integer.parseInt(anoIni) + cont;
                sAno = Integer.toString(iAno);
                //Calculo la fecha del 31 de dic. de ese a�o
                sFecUvas = "31/12/" + sAno;

                fecUvas = formateador.parse(sFecUvas);
                //calculo fechas de sems

                //Fecha de ult sem pasa a ser como lo  que tenia sComAno

                fecRef = fecFinAnt;
                encontrado = false;
                int j = 0;

                while ( (encontrado == false) && (j < 54)) {
                  fecSig = new java.util.Date(0);
                  //A�ade 7 dias m�s en cada ciclo  a fecSig ( se multiplica por num de milisegundos de un dia)
                  fecSig.setTime(fecRef.getTime() +
                                 (long) (7 * 24 * 3600 * 1000));

                  //Si no hemos llegado al fin de a�o
                  if (fecUvas.after(fecSig)) {
                    FecyNum elem = new FecyNum(j + 1, fecSig);
                    vFec.addElement(elem);
                    //avanzamos en la referencia
                    fecRef = fecSig;
                    encontrado = false;
                  }

                  //Si hemos llegado al fin de a�o
                  else {
                    encontrado = true;

                    //ve si hay que meter tambi�n en el a�o ep. la semana del 31 de diciembre
                    cal.setTime(fecUvas);

                    if ( //Semana entra
                        (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) ||
                        (cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) ||
                        (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) ||
                        (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                        ) {
                      FecyNum elem = new FecyNum(j + 1, fecSig);
                      vFec.addElement(elem);
                      fecRef = fecSig;
                    }
                    else {
                    }
                  }
                  j++;

                } //while  de fechas de un a�o

                //A�ade ese a�o a tabla a�o epid
                // prepara la query
                st = con.prepareStatement(sALTA_ANOEPI);
                k = 0;
                //C�digo de a�o
                k++;
                st.setString(k, sAno);
                //Fecha inicio de a�o epidemoiol�gico (fin del anterior)
                //A�ade 1 dias m�s a fecFinAnt( se multiplica por num de milisegundos de un dia)
                k++;
                fecComAno = new java.util.Date(0);
                fecComAno.setTime(fecFinAnt.getTime() +
                                  (long) (24 * 3600 * 1000));
                st.setDate(k, new java.sql.Date(fecComAno.getTime()));
                //Fecha fin de a�o epidemoiol�gico
                k++;
                st.setDate(k, new java.sql.Date(fecRef.getTime()));
                // lanza la query
                st.executeUpdate();
                st.close();
                st = null;

                //Deja preparada la referencia para c�lculo fechas del sigte a�o
                fecFinAnt = fecRef; //ultima fecha metida en vector del a�o anterior

                //A�ade cada semana epidemiol�gica a tabla de semanas epid de un a�o
                enum = vFec.elements();
                while (enum.hasMoreElements()) {
                  semana = (FecyNum) (enum.nextElement());
                  //A�ade esa fecha a tabla sem epid
                  // prepara la query
                  st = con.prepareStatement(sALTA_SEMEPI);
                  k = 0;
                  //C�digo de a�o
                  k++;
                  st.setString(k, sAno);
                  //C�digo de semana
                  k++;
                  sNum = Integer.toString(semana.getnum());
                  if (sNum.length() == 1) {
                    sNum = "0" + sNum;
                  }
                  st.setString(k, sNum);
                  //Fecha fin de semana epidemoiol�gica
                  k++;
                  st.setDate(k, new java.sql.Date(semana.getFec().getTime()));
                  // lanza la query
                  st.executeUpdate();
                  st.close();
                  st = null;

                }

                //Ahora copiamos las poblaciones de los m�dicos que est�n
                // dados de alta en el nuevo a�o.
                // Primero: buscamos las poblaciones de los m�dicos dados de alta del a�o anterior.
                st = con.prepareStatement(
                    "select * from SIVE_POBLAC_TS where CD_MEDCEN " +
                    "in (select CD_MEDCEN from SIVE_MCENTINELA where IT_BAJA='N') " +
                    "and CD_ANOEPI = ? ");
                st.setString(1, Integer.toString(anyoAuxiliar));
                // lanza la query
                rs = st.executeQuery();
                while (rs.next()) {
                  st = con.prepareStatement("insert into SIVE_POBLAC_TS (CD_EDADF, CD_EDADI, CD_SEXO, CD_PCENTI, CD_MEDCEN, NM_POBLAC, CD_ANOEPI) values (?,?,?,?,?,?,?)");
                  st.setInt(1, rs.getInt("CD_EDADF"));
                  st.setInt(2, rs.getInt("CD_EDADI"));
                  st.setString(3, rs.getString("CD_SEXO"));
                  st.setString(4, rs.getString("CD_PCENTI"));
                  st.setString(5, rs.getString("CD_MEDCEN"));
                  st.setInt(6, rs.getInt("NM_POBLAC"));
                  String anio_tmp = rs.getString("CD_ANOEPI");
                  int anllo_tmp = Integer.parseInt(anio_tmp);
                  anllo_tmp++;
                  st.setString(7, String.valueOf(anllo_tmp));
                  st.executeUpdate();
                  st.close();
                  st = null;
                }
                rs.close();
                rs = null;

                //Pues ahora copiamos el rango del a�o anterior en el a�o nuevo.
                // Primero: buscamos las poblaciones de los m�dicos dados de alta del a�o anterior.
                st = con.prepareStatement(
                    "select CD_TIPORANGO from SIVE_RANGO_ANO where CD_ANOEPI = ? ");
                st.setString(1, Integer.toString(anyoAuxiliar));
                // lanza la query
                rs = st.executeQuery();
                while (rs.next()) {
                  st = con.prepareStatement(
                      "insert into SIVE_RANGO_ANO (CD_ANOEPI, CD_TIPORANGO) values (?,?)");
                  st.setString(1, Integer.toString(anyoAuxiliar + 1));
                  st.setString(2, rs.getString("CD_TIPORANGO"));
                  st.executeUpdate();
                  st.close();
                  st = null;
                }
                rs.close();
                rs = null;
              }
            }
          }
          else {
            Exception ew = null;
            ew = new Exception("El a�o ya estaba generado.");
            throw ew;
          }
          break;

        case servletOBTENER:

          // prepara la query
          st = con.prepareStatement(sBUSQUEDA);

          // prepara la lista de resultados
          data = new CLista();

          // filtro
          st.setString(1, datConvFec.getAno().trim()); //Sin el %  ���
          rs = st.executeQuery();
          //QQ:
          //# System.Out.println("A�o a buscar: " + datConvFec.getAno().trim());

          // obtiene los campos
          vFec = new Vector();
          // extrae la p�gina requerida  Exactamente como en el select normalmente
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            sNum = rs.getString("CD_SEMEPI");
            iNum = Integer.parseInt(sNum);

            //Fecha
            sqlFec = rs.getDate("FC_FINEPI");
            semana = new FecyNum(iNum, sqlFec, formateador.format(sqlFec));
            vFec.addElement(semana);
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          if (vFec.isEmpty() == false) {
            bHayFec = true;

          }
          else {
            bHayFec = false;
            // a�ade un nodo
          }
          data.addElement(new DataConvFec(datConvFec.getAno(), vFec, bHayFec));

          //Si ese a�o tiene fechas se a�ade adem�s
          if (bHayFec == true) {

            java.util.Date fecPriSem;
            //Deja preparada la fecha del primer d�a del a�o epid
            // Restando 6 dias a la fecha de primera semana ( se multiplica por num de milisegundos de un dia)
            fecPriSem = ( (FecyNum) (vFec.firstElement())).getFec();
            fecComAno = new java.util.Date(0);
            fecComAno.setTime(fecPriSem.getTime() -
                              (long) (6 * 24 * 3600 * 1000));
            ( (DataConvFec) (data.firstElement())).setComAno(formateador.format(
                fecComAno));
          } //if

          //********************************************//
          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      //# System.Out.println(ex.getMessage() );
      ex.printStackTrace();
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }

  /*
   M�todo main para pruebas
   */

  public static void main(String args[]) {
    SrvConvFecCent srv = new SrvConvFecCent();
    CLista vParam = new CLista();
    CLista vSalida = new CLista();
    DataConvFec elDato = new DataConvFec("", "2006", 1);
    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
    vParam.addElement(elDato);

    try {
      vSalida = srv.doDebug(2, vParam);
    }
    catch (Exception e) {
      System.err.println("Error " + e);
    }
    srv = null;
    System.out.println(vSalida);
  } // del m�todo main.

}