//Title:        Generacion de alarmas automaticas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina && Luis Rivera
//Company:      Norsistemas
//Description: Servlet que genera alarmas autom�ticas

package centinelas.servidor.genalauto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import capp.CLista;
import capp.CMessage;
import centinelas.datos.genalauto.ParGenAlaAuto;
import sapp.DBServlet;

public class SrvGenAlaAutoCent
    extends DBServlet {

  //modos de consulta al servlet

  final int servletADV_GEN_AL = 9; //Para indicar al cliente si ya hay alarmas de ese a�o o no
  final int servletGEN_AL_AUTO = 2; //Genera las alarmas autom�ticas de un a�o y una enfermedad
  final int servletGEN_AL_AUTO_OPT = 20; //Genera las alarmas autom�ticas de un a�o y una enfermedad
  final int servletGEN_VEC_ENF = 8; //Genera una lista con una estructura de vectores anidados que representan
  //enfermedades, semanas, casos y la devuelve

  final int tipoCOMUNIDAD = 3;
  final int tipoNIVEL1 = 4;
  final int tipoNIVEL2 = 5;

  final int tipoMEDIA = 6;
  final int tipoNO_MEDIA = 7;

  final String ERROR_NO_DATOS =
      "No existen datos de los a�os anteriores para calcular los estad�sticos";
  final String ERROR_NO_ANO = "No es posible calcular los estad�sticos para ese a�o porque no esta generado en el sistema";

  final String R = "-"; //Car�cter de relleno para c�digos de nivel 1 o nivel 2 que no lleguen a su valor m�ximo

  //Auxiliar   (ARS 26-04-01)
  String queryB = "";
  String codigo = "";
  int indSem = 0;
  Vector semana = new Vector();
  Vector casos = new Vector();
  int semanas = 0;
  boolean insertarIndicadores = true;
  Vector semProc = new Vector();
  String sSem = "";
  Double cero = null;

  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // objetos JDBC
  Connection con = null;
  PreparedStatement st = null;
  ResultSet rs = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    CMessage msgBox;
    boolean noHayReg = false;
    boolean noHayAno = false;
    boolean hayAlarmas = false;

    int anteriores = -1;

    // objetos de datos
    CLista data = null; //lista de resultado al cliente (lineas, es decir, Strings)
    ParGenAlaAuto parGen = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    parGen = (ParGenAlaAuto) param.firstElement();

    String cd_indalar = new String();
    String cd_izquier = new String();
    String cd_secuen[] = new String[3];
    String cd_nivel1 = new String();
    String cd_nivel2 = new String();
    String ds_indalar = new String();
    String cd_enfcie = new String();
    String it_activo = new String();
    String it_tipo = new String();

    String cd_anoepi = parGen.anoEpi;
    String cd_semepi = new String();
    Double nm_valor = null;
    Double nm_desv = null;

    Vector izquierdas = new Vector();
    Vector secIzq = new Vector();

    Vector registrosSD = new Vector();
    Vector enfermedad = new Vector();

    try {

      // modos de operaci�n
      switch (opmode) {

        //__________________________________________________________

        //Devuelve un objeto de tipo Boolean en el que se indica si hay generadas
        //alarmas autom�ticas en este a�o
        case servletADV_GEN_AL:

          String queryCom = "select count(*) from sive_ind_alarrmc_ano "
              + " where cd_anoepi = ? and "
              + " cd_indalar like ? ";
          st = con.prepareStatement(queryCom);
          st.setString(1, parGen.anoEpi);

          switch (parGen.tipo) {
            //Se buscan alarmas de comunidad
            case tipoCOMUNIDAD:
              st.setString(2, "CC" + R + R + "A%");
              break;

          }
          rs = st.executeQuery();
          if (rs.next()) {
            int hay = rs.getInt(1);
            if (hay > 0) {
              hayAlarmas = true;
            }
          }
          st.close();
          st = null;

          data = new CLista();
          data.addElement(new Boolean(hayAlarmas));

          if (data != null) {
            data.trimToSize();

            // cierra la conexion
          }
          closeConnection(con);
          break;

          //__________________________________________________________

        case servletGEN_VEC_ENF:

          //Comprobacion de que el a�o se ha generado en sive_semana_epi_rmc
          String queryAno = "select count(*) from sive_semana_epi_rmc "
              + " where cd_ANOEPI = ? ";
          st = con.prepareStatement(queryAno);
          st.setString(1, parGen.anoEpi);
          rs = st.executeQuery();
          if (rs.next()) {
            int sems = rs.getInt(1);
            if ( (sems != 52) && (sems != 53)) {
              noHayAno = true;
            }
          }
          st.close();
          st = null;

          Integer ano = new Integer(parGen.anoEpi);
          Integer anoI = new Integer(ano.intValue() - 5);
          Integer anoF = new Integer(ano.intValue() - 1);

          //Si el a�o epidemiol�gico est� generado
          if (!noHayAno) {
            switch (parGen.tipo) {
              case tipoCOMUNIDAD:

                String queryComSD =
                    "select sum(NM_CASOSDEC), CD_ENFCIE, CD_SEMEPI, CD_ANOEPI "
                    + " from SIVE_NOTIF_RMC "
                    + " where CD_ANOEPI <= ? and CD_ANOEPI >= ? and "
                    + " CD_ENFCIE in (select CD_ENFCIE from SIVE_ENF_CENTI) "
                    + " group by CD_ENFCIE, CD_SEMEPI, CD_ANOEPI "
                    + " order by CD_ENFCIE, CD_SEMEPI, CD_ANOEPI";

                registrosSD = calcularVectorOrdenadoCasos(tipoCOMUNIDAD,
                    queryComSD, anoI, anoF, "", "");

                // Compruebo si hay registros para calcular los estadisticos
                // Es suficiente con que haya datos de un ano anterior
                if (registrosSD.size() == 0) {
                  noHayReg = true;
//                  //# System.Out.println("SrvGenAla no hay registros para comunidad");
                }

////# System.Out.println("&& Vect com");
                break;

            }
          }
          else { //no hay a�o
            data = new CLista();
            data.addElement(new Boolean(true));
            data.addElement(new String(ERROR_NO_ANO));
          }

          if (noHayReg) {
            //No hay registros
            data = new CLista();
            data.addElement(new Boolean(true));
            data.addElement(new String(ERROR_NO_DATOS));
          }

          if (! (noHayAno) && ! (noHayReg)) {
            //  hay a�o y reg
            data = new CLista();
            data.addElement(new Boolean(false));
            data.addElement(registrosSD);
          }

          if (data != null) {
            data.trimToSize();

            // cierra la conexion
          }
          closeConnection(con);

          break;

          //__________________________________________________________

        case servletGEN_AL_AUTO:

          String queryB = "";
          String codigo = ""; //Auxiliar
          // NOTAS PARA LA GENERACION AUTOMATICA DE ALARMAS:
          //  - para generar las alarmas de un a�o debe estar generado
          //    antes en sive_ano_epi_rmc, y en sive_semana_epi_rmc
          //  - para generar las alarmas de un a�o es suficiente con que
          //    haya datos de alguno de los cinco a�os anteriores al que
          //    se esta generando
          //  - en la tabla sive_notif_rmc puede faltar alguna semana
          //    en algun a�o
          //  - los estadisticos se generan con los datos que haya para
          //    una semana de como maximo 5 a�os anteriores, por lo tanto
          //    en un a�o los estadisticos de una semana pueden estar
          //    calculados con 5 a�os anteriores, los de otra semana
          //    con 3, ...
          //  - si para una semana no hay ningun dato de a�os anteriores
          //    los estadisticos son 0
          //  - se genera un indicador para media, otro para mediana y
          //    otro para maximo para un a�o, para cada enfermedad,
          //    ademas de las variaciones de nivel1 y nivel2

          enfermedad = parGen.enfermedad;
          cd_enfcie = (String) enfermedad.elementAt(0);
          ////# System.Out.println("SrvGenAla: enfermedad " + cd_enfcie
          //                   + " tipo " + parGen.tipo);
          ////# System.Out.println("SrvGenAla: Borrado de las alarmas de esa endfermedad en ese a�o");

          int indSem = 0;
          Vector semana = new Vector();
          Vector casos = new Vector();
          int semanas = 0;
          boolean insertarIndicadores = true;

          Vector semProc = new Vector();

          switch (parGen.tipo) {

            case tipoCOMUNIDAD:

              //Borrado de las alarmas de esa enfermedad en ese a�o
              queryB = " delete from sive_alarma_rmc "
                  + " where CD_ANOEPI = ? and "
                  + " CD_INDALAR like ?  ";
              st = con.prepareStatement(queryB);
              st.setString(1, parGen.anoEpi);
              st.setString(2,
                           calculaIzquier(tipoCOMUNIDAD, cd_enfcie, "", "") + "%");
              st.executeUpdate();
              st.close();
              st = null;
              break;

          } //Switch

          // Empiezo a procesar las semanas de la enfermedad
          semanas = enfermedad.size();
          indSem = 1;

          // Para cada enfermedad se generan tres indicadores
          // para la media, para la mediana y para el max
          cd_izquier = calculaIzquier(parGen.tipo,
                                      cd_enfcie,
                                      cd_nivel1,
                                      cd_nivel2);
          // Media
          cd_secuen[0] = "001";
          cd_indalar = cd_izquier + cd_secuen[0];
          ds_indalar = "Media " + cd_enfcie;
          it_activo = "S";
          it_tipo = "A";
          insertarIndicador(parGen.tipo, con,
                            cd_indalar, ds_indalar, cd_enfcie,
                            it_activo, it_tipo, cd_izquier, cd_secuen[0],
                            cd_nivel1, cd_nivel2);

//   System.out.println ("_______________________________________________ " );
//   System.out.println ("**** INDICADOR MEDIA "+"de cd_indalar " + cd_indalar + " enf " + cd_enfcie);

          insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1.25));
          // Mediana
          cd_secuen[1] = "002";
          cd_indalar = cd_izquier + cd_secuen[1];
          ds_indalar = "Mediana " + cd_enfcie;
          it_activo = "S";
          it_tipo = "A";
          insertarIndicador(parGen.tipo, con,
                            cd_indalar, ds_indalar, cd_enfcie,
                            it_activo, it_tipo, cd_izquier, cd_secuen[1],
                            cd_nivel1, cd_nivel2);
//   System.out.println ("_______________________________________________ " );
//   System.out.println ("**** INDICADOR MEDIANA "+"de cd_indalar " + cd_indalar + " enf " + cd_enfcie);

          insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1.25));

          // Maximo
          cd_secuen[2] = "003";
          cd_indalar = cd_izquier + cd_secuen[2];
          ds_indalar = "Maximo " + cd_enfcie;
          it_activo = "S";
          it_tipo = "A";
          insertarIndicador(parGen.tipo, con,
                            cd_indalar, ds_indalar, cd_enfcie,
                            it_activo, it_tipo, cd_izquier, cd_secuen[2],
                            cd_nivel1, cd_nivel2);
//   System.out.println ("_______________________________________________ " );
//   System.out.println ("**** INDICADOR MAXIMO "+"de cd_indalar " + cd_indalar + " enf " + cd_enfcie);

          insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1));

          /*
                    // determina el n� de semanas del a�o que va a generar
                    st = con.prepareStatement("select COUNT(CD_ANOEPI) from sive_semana_epi_rmc where CD_ANOEPI = ?");
                    st.setString(1, parGen.anoEpi);
                    rs = st.executeQuery();
                    if (rs.next()) {
                      semanas = rs.getInt(1);
                    }
                    rs.close();
                    rs = null;
                    st.close();
                    st = null;
           */

          while (indSem < semanas) {

            semana = (Vector) enfermedad.elementAt(indSem);
            indSem++;
            cd_semepi = (String) semana.elementAt(0);
            semProc.addElement(cd_semepi);

//   System.out.println ("**** Semana"+ cd_semepi);

            // Se obtienen los registros que corresponden a esa
            // enfermedad y esa semana
            casos = (Vector) semana.elementAt(1);

            //Si es necesario Se ordenan casos segun valor de menor a mayor
            if (casos.size() > 1) {
              ordenaVectorCasosDeMenorAMayor(casos);

            }

            anteriores = casos.size();

            if (anteriores == 0) {
              // si falta una semana pongo los estadisticos a 0
              //continue;
//                System.out.println("*** Anteriores 0");
              nm_valor = new Double(0);
              nm_desv = new Double(0);
            }
            else {
//                System.out.println("*** Hay Anteriores ");
              Long sum = new Long(0);
              String auxS = new String();
              Long auxL = new Long(0);
              for (int s = 0; s < anteriores; s++) {
                auxS = (String) casos.elementAt(s);
                auxL = new Long(auxS);
                sum = new Long(sum.longValue() + auxL.longValue());
              }

              Integer iAnt = new Integer(anteriores);
              nm_valor = new Double(sum.doubleValue() / iAnt.doubleValue());

              // se calcula la desviacion estandar
              double dAux = 0;
              // sumatorio
              for (int i = 0; i < anteriores; i++) {
                dAux = dAux +
                    Math.pow( (new Double( (String) casos.elementAt(i))).
                             doubleValue()
                             - nm_valor.doubleValue(), 2);
              }
              dAux = dAux / anteriores;
              dAux = Math.sqrt(dAux);
              nm_desv = new Double(dAux);
            }
//            System.out.println("*** Antes insertar Media ");
            insertarAlarma(tipoMEDIA, con,
                           parGen.anoEpi, cd_izquier + cd_secuen[0],
                           cd_semepi, nm_valor, nm_desv);

            //Para la mediana
            if (anteriores == 0) {
              // si falta una semana pongo los estadisticos a 0
              //continue;
              nm_valor = new Double(0);
              nm_desv = new Double(0);
            }
            else {
              if ( (anteriores % 2) != 0) {

                // Las restas de 1 son porque el vector empieza en 0
                nm_valor = new Double( (String) casos.elementAt( (anteriores +
                    1) / 2 - 1));
              }
              else {
                nm_valor = new Double( (String) casos.elementAt(anteriores / 2 -
                    1));
                Double aux = new Double( (String) casos.elementAt(anteriores /
                    2 + 1 - 1));
                nm_valor = new Double( (nm_valor.doubleValue() +
                                        aux.doubleValue()) / 2);
              }
            }
//            System.out.println("*** Antes insertar Mediana ");
            // Inserto la alarma de mediana
            insertarAlarma(tipoNO_MEDIA, con,
                           parGen.anoEpi, cd_izquier + cd_secuen[1],
                           cd_semepi, nm_valor, nm_desv);

            // Para el maximo
            if (anteriores == 0) {
              // si falta una semana pongo los estadisticos a 0
              //continue;
              nm_valor = new Double(0);
            }
            else {
              nm_valor = calcularMaximo(casos);
            }
//            System.out.println("*** Antes insertar Maximo ");
            // Inserto la alarma de maximo
            insertarAlarma(tipoNO_MEDIA, con,
                           parGen.anoEpi, cd_izquier + cd_secuen[2],
                           cd_semepi, nm_valor, nm_desv);
          } //semanas

          // Para las semanas que faltan se meten ceros
          sSem = new String();
          cero = new Double(0);

          for (int j = 1; j < 53; j++) {
            Integer iSem = new Integer(j);
            sSem = iSem.toString();
            if (sSem.length() == 1) {
              sSem = '0' + sSem;
            }
            if (!semProc.contains(sSem)) {
              // Inserto la alarma de media
              insertarAlarma(tipoMEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[0],
                             sSem, cero, cero);
              // Inserto la alarma de mediana
              insertarAlarma(tipoNO_MEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[1],
                             sSem, cero, cero);
              // Inserto la alarma de maximo
              insertarAlarma(tipoNO_MEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[2],
                             sSem, cero, cero);
            }
          }

          // valida la transacci�n
          con.commit();

          // cierra la conexion
          closeConnection(con);

          break;

        case servletGEN_AL_AUTO_OPT:

          queryB = "";
          codigo = "";
          // NOTAS PARA LA GENERACION AUTOMATICA DE ALARMAS:
          //  Igual que en el modo servletGEN_AL_AUTO, salvo que
          // las enfermedades se recorren aqu�, en vez de en el cliente

          int indEnf = 0;
          Vector enfermedades = parGen.enfermedad;

          // Metemos el 'mientras' que recorrer� las enfermedades

          while (indEnf < enfermedades.size()) {

            enfermedad = (Vector) enfermedades.elementAt(indEnf);
            cd_enfcie = (String) enfermedad.elementAt(0);
            // P-arriba el contador... que no se olvide.
            indEnf++;

            indSem = 0;
            semana = new Vector();
            casos = new Vector();
            semanas = 0;
            insertarIndicadores = true;

            semProc = new Vector();

            switch (parGen.tipo) {
              case tipoCOMUNIDAD:

                //Borrado de las alarmas de esa enfermedad en ese a�o
                queryB = " delete from sive_alarma_rmc "
                    + " where CD_ANOEPI = ? and "
                    + " CD_INDALAR like ?  ";
                st = con.prepareStatement(queryB);
                st.setString(1, parGen.anoEpi);
                st.setString(2,
                             calculaIzquier(tipoCOMUNIDAD, cd_enfcie, "", "") + "%");
                st.executeUpdate();
                st.close();
                st = null;
                break;
            } //Switch

            // Empiezo a procesar las semanas de la enfermedad
            semanas = enfermedad.size();
            indSem = 1;

            // Para cada enfermedad se generan tres indicadores
            // para la media, para la mediana y para el max
            cd_izquier = calculaIzquier(parGen.tipo,
                                        cd_enfcie,
                                        cd_nivel1,
                                        cd_nivel2);
            // Media
            cd_secuen[0] = "001";
            cd_indalar = cd_izquier + cd_secuen[0];
            ds_indalar = "Media " + cd_enfcie;
            it_activo = "S";
            it_tipo = "A";
            insertarIndicador(parGen.tipo, con,
                              cd_indalar, ds_indalar, cd_enfcie,
                              it_activo, it_tipo, cd_izquier, cd_secuen[0],
                              cd_nivel1, cd_nivel2);

            insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1.25));
            // Mediana
            cd_secuen[1] = "002";
            cd_indalar = cd_izquier + cd_secuen[1];
            ds_indalar = "Mediana " + cd_enfcie;
            it_activo = "S";
            it_tipo = "A";
            insertarIndicador(parGen.tipo, con,
                              cd_indalar, ds_indalar, cd_enfcie,
                              it_activo, it_tipo, cd_izquier, cd_secuen[1],
                              cd_nivel1, cd_nivel2);

            insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1.25));

            // Maximo
            cd_secuen[2] = "003";
            cd_indalar = cd_izquier + cd_secuen[2];
            ds_indalar = "Maximo " + cd_enfcie;
            it_activo = "S";
            it_tipo = "A";
            insertarIndicador(parGen.tipo, con,
                              cd_indalar, ds_indalar, cd_enfcie,
                              it_activo, it_tipo, cd_izquier, cd_secuen[2],
                              cd_nivel1, cd_nivel2);

            insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1));

            while (indSem < semanas) {

              semana = (Vector) enfermedad.elementAt(indSem);
              indSem++;
              cd_semepi = (String) semana.elementAt(0);
              semProc.addElement(cd_semepi);

              // Se obtienen los registros que corresponden a esa
              // enfermedad y esa semana
              casos = (Vector) semana.elementAt(1);

              //Si es necesario Se ordenan casos segun valor de menor a mayor
              if (casos.size() > 1) {
                ordenaVectorCasosDeMenorAMayor(casos);

              }

              anteriores = casos.size();

              if (anteriores == 0) {
                // si falta una semana pongo los estadisticos a 0
                //continue;
                nm_valor = new Double(0);
                nm_desv = new Double(0);
              }
              else {
                Long sum = new Long(0);
                String auxS = new String();
                Long auxL = new Long(0);
                for (int s = 0; s < anteriores; s++) {
                  auxS = (String) casos.elementAt(s);
                  auxL = new Long(auxS);
                  sum = new Long(sum.longValue() + auxL.longValue());
                }

                Integer iAnt = new Integer(anteriores);
                nm_valor = new Double(sum.doubleValue() / iAnt.doubleValue());

                // se calcula la desviacion estandar
                double dAux = 0;
                // sumatorio
                for (int i = 0; i < anteriores; i++) {
                  dAux = dAux +
                      Math.pow( (new Double( (String) casos.elementAt(i))).
                               doubleValue()
                               - nm_valor.doubleValue(), 2);
                }
                dAux = dAux / anteriores;
                dAux = Math.sqrt(dAux);
                nm_desv = new Double(dAux);
              }

              insertarAlarma(tipoMEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[0],
                             cd_semepi, nm_valor, nm_desv);

              //Para la mediana
              if (anteriores == 0) {
                // si falta una semana pongo los estadisticos a 0
                //continue;
                nm_valor = new Double(0);
                nm_desv = new Double(0);
              }
              else {
                if ( (anteriores % 2) != 0) {

                  // Las restas de 1 son porque el vector empieza en 0
                  nm_valor = new Double( (String) casos.elementAt( (anteriores +
                      1) / 2 - 1));
                }
                else {
                  nm_valor = new Double( (String) casos.elementAt(anteriores /
                      2 - 1));
                  Double aux = new Double( (String) casos.elementAt(anteriores /
                      2 + 1 - 1));
                  nm_valor = new Double( (nm_valor.doubleValue() +
                                          aux.doubleValue()) / 2);
                }
              }

              // Inserto la alarma de mediana
              insertarAlarma(tipoNO_MEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[1],
                             cd_semepi, nm_valor, nm_desv);

              // Para el maximo
              if (anteriores == 0) {
                // si falta una semana pongo los estadisticos a 0
                //continue;
                nm_valor = new Double(0);
              }
              else {
                nm_valor = calcularMaximo(casos);
              }

              // Inserto la alarma de maximo
              insertarAlarma(tipoNO_MEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[2],
                             cd_semepi, nm_valor, nm_desv);
            } //semanas

            // Para las semanas que faltan se meten ceros
            sSem = new String();
            cero = new Double(0);

            for (int j = 1; j < 53; j++) {
              Integer iSem = new Integer(j);
              sSem = iSem.toString();
              if (sSem.length() == 1) {
                sSem = '0' + sSem;
              }
              if (!semProc.contains(sSem)) {
                // Inserto la alarma de media
                insertarAlarma(tipoMEDIA, con,
                               parGen.anoEpi, cd_izquier + cd_secuen[0],
                               sSem, cero, cero);
                // Inserto la alarma de mediana
                insertarAlarma(tipoNO_MEDIA, con,
                               parGen.anoEpi, cd_izquier + cd_secuen[1],
                               sSem, cero, cero);
                // Inserto la alarma de maximo
                insertarAlarma(tipoNO_MEDIA, con,
                               parGen.anoEpi, cd_izquier + cd_secuen[2],
                               sSem, cero, cero);
              } // Del if
            } // Del for
          } // del while

          // valida la transacci�n
          con.commit();
          // cierra la conexion
          closeConnection(con);
          break;
      } //fin switch

      //fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
//        System.out.println("srvGenAla: despues de rollback excepcion " + cd_enfcie +
//                           " " + cd_semepi + ex.getMessage());
      ex.printStackTrace();

      throw ex;
    }

    return data;
  } // doWork

  //__________________________________________________________
  //__________________________________________________________

  //Devuelve String con el que comiennzan las alarmas de un a�o y enfermedad
  String calculaIzquier(int tipo, String cd_enfcie,
                        String cd_nivel1, String cd_nivel2) {
    String cd_izquier = new String();
    switch (tipo) {
      case tipoCOMUNIDAD:
        cd_izquier = "CC" + R + R + "A";
        for (int i = 0; i < 6 - cd_enfcie.length(); i++) {
          cd_izquier += R;
        }
        cd_izquier += cd_enfcie;
        break;
    }
    return cd_izquier;
  }

  //________________________________________________________

  Double calcularMaximo(Vector casos) {
    Long max = new Long(0);
    Long aux = new Long(0);
    for (int i = 0; i < casos.size(); i++) {
      aux = new Long( (String) casos.elementAt(i));
      if (aux.longValue() > max.longValue()) {
        max = aux;
      }
    }
    return new Double(max.toString());
  }

  //________________________________________________________

  void imprimirVector(Vector v) {
    Vector aux = new Vector();

    for (int s = 0; s < v.size(); s++) {
      try {
        aux = (Vector) v.elementAt(s);
        imprimirVector(aux);

      }
      catch (ClassCastException ex) {
//        System.out.print("En imprimirVector ");
//        System.out.print(" " + (String) v.elementAt(s) + " ");
      }
    }

  }

  //________________________________________________________

  String calculaSecuen(String izq) throws Exception {

    String sec = new String();
    String query = "select max(cd_secuen) from sive_ind_alar_rmc "
        + " where cd_izquier = ? ";

    st = con.prepareStatement(query);
    st.setString(1, izq);
    rs = st.executeQuery();
    if (rs.next()) {
      if (rs == null) {
        sec = "001";
      }
      else {
        Integer iSec = new Integer(rs.getInt(1) + 1);
        sec = iSec.toString();
        int lsecuen = sec.length();
        for (int i = 0; i < 3 - lsecuen; i++) {
          sec = "0" + sec;
        }
      }
    }
    return sec;
  }

  //________________________________________________________

  //COMENTADO POR LUIS RIVERA
  //ESTO ES UN COMENTARIO Y LO DEMAS SON TONTERIAS
  //Nos devuelve un vector con toda la estructuar de datos
  // que se utilizar� luego para insertar alarmas
  // La estructura es la siguiente:
  //Vector devuelto regsSD :  {enfer1, ... , enferN}
  //  donde cada enferi es un vector para cada enfemedad
  //Vector que representa enfermedad enferi : {enfer, sem1, sem2, ... ,sem52}
  //  donde enfer es un String con el c�digo de enfermedad y el resto son vectores
  //  para las semanas en las que hay notificaci�n de esa enfeemedad
  //  con campo NM_CASOSDEC no nulo
  //Vector para semana semi :  {semana, numCas}
  //  donde semana es el c�digo de la semana y numCas es un vector que representa
  // los casos de las notificaciones (con NM_CASOSDEC no nulo) para esa enfemedad y semana
  // dadas en los cinco ultimos a�os
  //Vector numCas :{caso1,...,cason}
  //  donde casoi es un String que representa un valor (se pasa a long de java) con
  // el n�mero de casos de la notificacion dadas en un a�o, semana y enfermedad concretos

  Vector regsSD = new Vector(); //
  Vector enfer = new Vector(); //
  Vector sem = new Vector(); // {semana, vector de casos}
  // {semana, n11, ... , n1N}
  Vector numCas = new Vector(); // {caso1,...,cason}

  Vector calcularVectorOrdenadoCasos(int tipo, String query,
                                     Integer aI, Integer aF,
                                     String cod1, String cod2) throws Exception { //C�digos a�adidos *******
    boolean camEnf = false;
    boolean hayReg = false;

    st = con.prepareStatement(query);
    st.setString(2, aI.toString());
    st.setString(1, aF.toString());
    //A�ade par�metros para buscar el niv 1 y niv2 si es necesario
    switch (tipo) {
      case tipoNIVEL1:
        st.setString(3, cod1);
        break;
      case tipoNIVEL2:
        st.setString(3, cod1);
        st.setString(4, cod2);
        break;
    }

    rs = st.executeQuery();

    Vector regsSD = new Vector(); // {enfer1, ... , enferN} Comunidad
    Vector enfer = new Vector(); // {enfer, sem1, sem2, ... ,sem52}
    Vector sem = new Vector(); // {semana, numCas}
    Vector numCas = new Vector(); // {caso1,...,cason}

    String antEnf = new String();
    String antSem = new String();

    //NM_CASOS, CD_ENFCIE, CD_SEMEPI, CD_NIVEL_1, CD_NIVEL_2 CD_ANOEPI
    while (rs.next()) {

      hayReg = true;
//# System.Out.println("&&&&& Entra en Hay Reg**********");
      String reg[] = new String[5];
      reg[0] = rs.getString(1); //SUM(NM_CASOS)
      reg[1] = rs.getString(2); //CD_ENFCIE
////# System.Out.println("&& Enfermedad***********************" + reg[1]);
      reg[2] = rs.getString(3); //CD_SEMEPI

      //Solo se a�ade el registro de esta semana al vector si hay casos para esta semana
      if (reg[0] != null) {

        //Primera enfermedad : Se a�ade un registro de enfermedad
        if (antEnf.equals(new String())) {
          antEnf = reg[1]; //Se guarda dato de enfermedad para ver si lugo ha cambiado la enfermedad
          enfer.addElement(reg[1]);
        }

        //Enfermedades que no son la primera ***********
        //S�lo se a�ade un objetoa al vector enfer si realmente cambia la enfermedad
        else
        if (!antEnf.equals(reg[1])) {
          if (!camEnf) {
            camEnf = true;
            //cambio de enfermedad
            // por tanto tambien hay cambio de semana
          }
          sem.addElement(numCas); //A�ade el �ltimo numcas a vector que contiene semana y sus casos
          enfer.addElement(sem); // a�ado la ultima semana a vector de enfermedad y sus semanas
          regsSD.addElement(enfer); //A�ado �ltima enf a vector que contiemne enfemedades

          enfer = new Vector();
          enfer.addElement(reg[1]); //Se a�ade una enfermedad ultima recogida
          antEnf = reg[1]; //Marca como �ltima enfermdad la recogida ya
          antSem = new String();
          sem = new Vector();
          numCas = new Vector();
        }

        if (antSem.equals(new String())) {
          // Primera semana de la enfermedad
          antSem = reg[2];
          sem = new Vector();
          sem.addElement(reg[2]);
        }
        else // Veo si hay un cambio de semana
        if (!antSem.equals(reg[2])) {
          //cambio de semana
          sem.addElement(numCas);
          enfer.addElement(sem);
          antSem = reg[2];
          sem = new Vector();
          sem.addElement(reg[2]);
          numCas = new Vector();
        }

        // Se a�ade el numero de casos
        numCas.addElement(reg[0]);

      } //if SUM(NM_CASOS) no es null
    } //While
    rs.close();
    st.close();
    rs = null;
    st = null;

    if (hayReg == true) {
//# System.Out.println("&& A�ade registro ala salir");
      sem.addElement(numCas);
      enfer.addElement(sem); // a�ado la ultima semana
      regsSD.addElement(enfer);
      //# System.Out.println("&& Tam  regsd " + regsSD.size() );
    }
    return regsSD;
  }

  //________________________________________________________

  //Inserta un indicador de alarma
  void insertarIndicador(int tipo, Connection con,
                         String cd_ind, String ds_ind, String cd_enf,
                         String it_ac, String it_ti, String cd_iz, String cd_se,
                         String cd_n1, String cd_n2) throws Exception {

    String insert = "insert into sive_ind_alar_rmc "
        + "(CD_INDALAR, DS_INDALAR, CD_NIVEL_1, CD_NIVEL_2, "
        + " CD_ENFCIE, IT_ACTIVO, IT_TIPO, CD_IZQUIER, CD_SECUEN) "
        + "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    PreparedStatement stIn = null;
    try {
      stIn = con.prepareStatement(insert);
      stIn.setString(1, cd_ind);
      stIn.setString(2, ds_ind);
      switch (tipo) {
        case tipoCOMUNIDAD:
          stIn.setNull(3, java.sql.Types.VARCHAR);
          stIn.setNull(4, java.sql.Types.VARCHAR);
          break;
      }
      stIn.setString(5, cd_enf);
      stIn.setString(6, it_ac);
      stIn.setString(7, it_ti);
      stIn.setString(8, cd_iz);
      stIn.setString(9, cd_se);

      stIn.executeUpdate();
      stIn.close();
      stIn = null;
    }
    catch (SQLException e) {
//      System.out.println ("SrvGenAla: ya estaba el indicador " + e.getMessage());
      try {
        stIn.close();
        stIn = null;
      }
      catch (Exception ex) {
        throw ex;
      }
    }
    catch (Exception ex) {
//      System.out.println ("En  insertarIndicador" + ex.getMessage());
      throw ex;
    }
  }

  //________________________________________________________

  //Inserta un registro en sive_ind_alarrmc_ano
  void insertarIndAno(Connection con,
                      String cd_ind, String cd_ano, Double nm_coef) throws
      Exception {
    // Inserto el indicador
    String insert = "insert into sive_ind_alarrmc_ano "
        + "(CD_INDALAR,CD_ANOEPI, NM_COEF) "
        + "values (?, ?, ?)";

    PreparedStatement stIn = null;
    try {
      stIn = con.prepareStatement(insert);

      stIn.setString(1, cd_ind);

      stIn.setString(2, cd_ano);

      stIn.setDouble(3, nm_coef.doubleValue());

      stIn.executeUpdate();
      stIn.close();
      stIn = null;

    }
    catch (SQLException e) {
      try {
        stIn.close();
        stIn = null;
      }
      catch (Exception ex) {
        throw ex;
      }
//    System.out.println ("SrvGenAla: ya estaba el ind_ano " + e.getMessage());
    }
    catch (Exception ex) {
//      System.out.println ("En  insertarIndAno" + ex.getMessage());
      System.out.println(ex.getMessage());
      ex.printStackTrace();
      throw ex;
    }
  }

  //________________________________________________________

  //Inserta una alarma
  void insertarAlarma(int tipo, Connection con,
                      String cd_ano, String cd_ind, String cd_sem,
                      Double nm_v, Double nm_d) throws Exception {
    String insert = "insert into sive_alarma_rmc "
        + "(CD_ANOEPI, CD_INDALAR, CD_SEMEPI,NM_VALOR, NM_DSTANDAR ) "
        + " values (?, ?, ?, ?, ?)";

    PreparedStatement stIn = con.prepareStatement(insert);
    stIn.setString(1, cd_ano);
    stIn.setString(2, cd_ind);
    stIn.setString(3, cd_sem);
    stIn.setDouble(4, nm_v.doubleValue());
    //stIn.setString(4, nm_v.toString());
    if (tipo == tipoMEDIA) {
      stIn.setDouble(5, nm_d.doubleValue());
    }
    else {
      stIn.setNull(5, java.sql.Types.NUMERIC);
    }
    stIn.executeUpdate();
    stIn.close();
    stIn = null;

  }

  //________________________________________________________

  //Ordena el vector de casos de menor a mayor
  //Para ordenarlo usa algoritmo QUICKSORT (ver libros)
  void ordenaVectorCasosDeMenorAMayor(Vector vCasos) {
    quickSort(vCasos, 0, vCasos.size() - 1);
  }

  //________________________________________________________

  //ALgoritmo de ordenacion QUICKSORT (VER LIBROS) aplicado a Vector con Strings que representan valores long
  //Un String ser� menor que otro si su long correspondiente lo es
  //Ordena el vector de casos de menor a mayor
  public void quickSort(Vector vCasos, int primero, int ultimo) {
    int izq, der; //Variables auxiliares para indices recorrido
    String elemRef = null;

    izq = primero; //inicialmente el menor indice del vector
    der = ultimo; //inicialmente el mayor indice del vector

    elemRef = (String) vCasos.elementAt( (izq + der) / 2); //Elemento de referencia
    long valorRef = (new Long(elemRef)).longValue(); //Valor del elemento de referencia

    while (izq < der) {

      String elemIzq = (String) vCasos.elementAt(izq); //Elemento de izqda
      long valorIzq = (new Long(elemIzq)).longValue(); //Valor del elemento de izqda
      String elemDer = (String) vCasos.elementAt(der); //Elemento de dcha
      long valorDer = new Long(elemDer).longValue(); //Valor del elemento de dcha

      while (valorIzq < valorRef) {
        izq++;
        elemIzq = (String) vCasos.elementAt(izq); //Elemento de izqda
        valorIzq = (new Long(elemIzq)).longValue(); //Valor del elemento de izqda
      }

      while (valorDer > valorRef) {
        der--;
        elemDer = (String) vCasos.elementAt(der); //Elemento de dcha
        valorDer = new Long(elemDer).longValue(); //Valor del elemento de dcha
      }

      //Si indice de izqda es menor que el de la dche
      if (izq <= der) {
        //Se intercambian los elemntos izqda y decha (se implementa intercambiando valores)

        //Se mete en elem de la izqda uno con valor del de la dche
        vCasos.removeElementAt(izq);
        vCasos.insertElementAt(Long.toString(valorDer), izq);

        //Se mete en elem de la dcha uno con valor del de la izqda
        vCasos.removeElementAt(der);
        vCasos.insertElementAt(Long.toString(valorIzq), der);

        izq++;
        der--;
      }

    } //while

    if (primero < der) {
      quickSort(vCasos, primero, der);

    }
    if (izq < ultimo) {
      quickSort(vCasos, izq, ultimo);
    }
  }

}
