package centinelas.servidor.c_mantmednotif;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import centinelas.datos.centbasicos.MedCent;
import centinelas.datos.comun.Secuenciador;
import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;

/*Servidor para realizar el alta de M�dicos Notificadores, asignando
 un codmed obtenido mediante un secuenciador general*/
public class SrvMantMedNotif
    extends DBServlet {

  public SrvMantMedNotif() {}

  final protected int servletALTA_MEDNOTIF = 3; //Alta de un m�dico notif
  final protected int servletOBTENER_MED_DE_PTO_ANO_SEM = 10; //Da el m�dico activo de un pto, a�o, sem
  final protected int servletOBTENER_MED_DE_PTO_ANO_SEM_MED_ALTA = 11; // Da el m�dico activo de un pto, a�o, sem,
  // incluyendo el m�dico que est� dado de alta.

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {
    // control de error
    boolean bError = false;
    String sMsg = null;

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // vector con el resultado
    Lista snapShot = new Lista();

    //peticion
    Lista lisPeticion = null;
    Data dtRec = null;
    Data dtDetalle = null;

    // querys fijas
    final String sALTA_MEDNOTIF =
        " insert into SIVE_MCENTINELA  "
        + "( CD_PCENTI, CD_MEDCEN, CD_E_NOTIF,"
        + " CD_SEXO, CD_MASIST, CD_ANOEPIA,"
        + " CD_SEMEPIA, DS_APE1, DS_APE2, DS_NOMBRE,"
        + " DS_DNI, DS_TELEF, DS_FAX, FC_NAC, FC_ALTA,"
        + " CD_OPE, FC_ULTACT, NMREDADI, NMREDADF, DS_HORAI,"
        + " DS_HORAF, IT_BAJA, FC_BAJA, CD_MOTBAJA,CD_ANOBAJA,CD_SEMBAJA)"
        + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    //Seleeciona el m�dico que estaba activo en un pto, a�o y sem
    // Seleccionar todos los medicos cuyos a�o,sem alta sean anteriores que uno dado
    // y cuyos a�o, sem baja sean posteriores a ese
    final String sMedActivoPtoAnoSem =
        "SELECT CD_PCENTI,CD_MEDCEN,CD_E_NOTIF,CD_SEXO," +
        "CD_MASIST,CD_ANOEPIA,CD_SEMEPIA,DS_APE1,DS_APE2," +
        "DS_NOMBRE,DS_DNI,DS_TELEF,DS_FAX,FC_NAC,FC_ALTA," +
        "CD_ANOBAJA,CD_SEMBAJA,NMREDADI,NMREDADF,DS_HORAI,DS_HORAF," +
        "IT_BAJA,FC_BAJA,CD_MOTBAJA,CD_OPE,FC_ULTACT " +
        "FROM SIVE_MCENTINELA"
        + " WHERE (CD_PCENTI = ? )"
        + " AND  ( (CD_ANOEPIA||CD_SEMEPIA) <= ( ? ||? )) "
        + " AND  ( (CD_ANOBAJA||CD_SEMBAJA) >= ( ? ||? ))  ";

    // Selecciona el m�dico activo en un punto centinela, tanto los que est�n
    // dados de baja, como que actualmente est� dado de alta.
    // (ARS 03-04-01)
    final String sMedActivoPtoAnoSemMedAlta =
        "SELECT CD_PCENTI,CD_MEDCEN,CD_E_NOTIF,CD_SEXO, " +
        "CD_MASIST,CD_ANOEPIA,CD_SEMEPIA,DS_APE1,DS_APE2, " +
        "DS_NOMBRE,DS_DNI,DS_TELEF,DS_FAX,FC_NAC,FC_ALTA, " +
        "CD_ANOBAJA,CD_SEMBAJA,NMREDADI,NMREDADF,DS_HORAI,DS_HORAF, " +
        "IT_BAJA,FC_BAJA,CD_MOTBAJA,CD_OPE,FC_ULTACT " +
        "FROM SIVE_MCENTINELA " +
        "WHERE (CD_PCENTI = ? ) " +
        "AND  ( (CD_ANOEPIA||CD_SEMEPIA) <= ( ? || ? )) " +
        "AND  ( (IT_BAJA='S' AND ( (CD_ANOBAJA||CD_SEMBAJA) >= ( ? || ? )) ) OR IT_BAJA='N') ";

    try {
      //No se realiza el commit automaticamente
      con.setAutoCommit(false);

      dtRec = (Data) vParametros.elementAt(0);

      switch (opmode) {

        /*data->todos los datos recogidos de la pantalla
                  /recoger el c�digo de la tabla de secuenciadores->data
                  /hacer el alta*/
        case servletALTA_MEDNOTIF:

          //Secuenciador
          String cod = Secuenciador.getSecuenciador(con, st, rs, "NM_MEDCENTI",
              6);
          dtRec.put("CD_MEDCEN", cod);

          // prepara la query
          st = con.prepareStatement(sALTA_MEDNOTIF);
          // completa los valores de la cl�usula WHERE
          st.setString(1, (String) dtRec.get("CD_PCENTI"));
          st.setString(2, (String) dtRec.get("CD_MEDCEN"));
          st.setString(3, (String) dtRec.get("CD_E_NOTIF"));
          st.setString(4, (String) dtRec.get("CD_SEXO"));
          st.setString(5, (String) dtRec.get("CD_MASIST"));
          st.setString(6, (String) dtRec.get("CD_ANOEPIA"));
          st.setString(7, (String) dtRec.get("CD_SEMEPIA"));
          st.setString(8, (String) dtRec.get("DS_APE1"));
          st.setString(9, (String) dtRec.get("DS_APE2"));
          st.setString(10, (String) dtRec.get("DS_NOMBRE"));
          st.setString(11, (String) dtRec.get("DS_DNI"));
          st.setString(12, (String) dtRec.get("DS_TELEF"));
          st.setString(13, (String) dtRec.get("DS_FAX"));
          if (dtRec.getString("FC_NAC").equals("")) {
            st.setNull(14, Types.DATE);
          }
          else {
            st.setDate(14, Format.string2Date( (String) dtRec.get("FC_NAC")));
          }
          st.setDate(15, Format.string2Date( (String) dtRec.get("FC_ALTA")));
          st.setString(16, (String) dtRec.get("CD_OPE"));
          st.setTimestamp(17, Format.string2Timestamp(Format.fechaHoraActual()));
          st.setInt(18, (new Integer( (String) dtRec.get("NMREDADI"))).intValue());
          st.setInt(19, (new Integer( (String) dtRec.get("NMREDADF"))).intValue());
          st.setString(20, (String) dtRec.get("DS_HORAI"));
          st.setString(21, (String) dtRec.get("DS_HORAF"));
          st.setString(22, (String) dtRec.get("IT_BAJA"));
          st.setDate(23, Format.string2Date( (String) dtRec.get("FC_BAJA")));
          st.setString(24, (String) dtRec.get("CD_MOTBAJA"));
          st.setString(25, (String) dtRec.get("CD_ANOBAJA"));
          st.setString(26, (String) dtRec.get("CD_SEMBAJA"));

          // ejecuta la query
          st.executeUpdate();
          // cierra los recursos
          st.close();
          st = null;

          snapShot.addElement(dtRec); //LRG

          break;

          //____________________________________________

        case servletOBTENER_MED_DE_PTO_ANO_SEM:

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sMedActivoPtoAnoSem);

          // completa los valores de la cl�usula WHERE
          st.setString(1, (String) dtRec.get("CD_PCENTI"));
          st.setString(2, (String) dtRec.get("CD_ANOEPI"));
          st.setString(3, (String) dtRec.get("CD_SEMEPI"));
          st.setString(4, (String) dtRec.get("CD_ANOEPI"));
          st.setString(5, (String) dtRec.get("CD_SEMEPI"));

          // Ejecucion de la query, obteniendo un ResultSet
          rs = st.executeQuery();

          // Procesamiento de cada registro obtenido de la BD
          while (rs.next()) {
            // Recuperacion de los datos a partir del ResultSet de la
            //  query realizada sobre la BD
            String sCD_PCENTI = rs.getString("CD_PCENTI");
            String sCD_MEDCEN = rs.getString("CD_MEDCEN");
            String sCD_E_NOTIF = rs.getString("CD_E_NOTIF");
            String sCD_SEXO = rs.getString("CD_SEXO");
            String sCD_MASIST = rs.getString("CD_MASIST");
            String sCD_ANOEPIA = rs.getString("CD_ANOEPIA");
            String sCD_SEMEPIA = rs.getString("CD_SEMEPIA");
            String sDS_APE1 = rs.getString("DS_APE1");
            String sDS_APE2 = rs.getString("DS_APE2");
            String sDS_NOMBRE = rs.getString("DS_NOMBRE");
            String sDS_DNI = rs.getString("DS_DNI");
            String sDS_TELEF = rs.getString("DS_TELEF");
            String sDS_FAX = rs.getString("DS_FAX");
            java.sql.Date dFC_NAC = rs.getDate("FC_NAC");
            String sFC_NAC = Format.date2String(dFC_NAC);
            java.sql.Date dFC_ALTA = rs.getDate("FC_ALTA");
            String sFC_ALTA = Format.date2String(dFC_ALTA);
            int iNMREDADI = rs.getInt("NMREDADI");
            String sNMREDADI = "" + iNMREDADI;
            int iNMREDADF = rs.getInt("NMREDADF");
            String sNMREDADF = "" + iNMREDADF;
            String sDS_HORAI = rs.getString("DS_HORAI");
            String sDS_HORAF = rs.getString("DS_HORAF");
            String sIT_BAJA = rs.getString("IT_BAJA");
            java.sql.Date dFC_BAJA = rs.getDate("FC_BAJA");
            String sFC_BAJA = Format.date2String(dFC_BAJA);
            String sCD_ANOBAJA = rs.getString("CD_ANOBAJA");
            String sCD_SEMBAJA = rs.getString("CD_SEMBAJA");
            String sCD_MOTBAJA = rs.getString("CD_MOTBAJA");
            String sCD_OPE = rs.getString("CD_OPE");
            java.sql.Date dFC_ULTACT = rs.getDate("FC_ULTACT");
            String sFC_ULTACT = Format.date2String(dFC_ULTACT);

            // Relleno del registro de salida
            //  por los registros devueltos por la query
            dtRec = new MedCent(sCD_PCENTI, sCD_MEDCEN, sCD_E_NOTIF, sCD_SEXO,
                                sCD_MASIST,
                                sCD_ANOEPIA, sCD_SEMEPIA, sDS_APE1, sDS_APE2,
                                sDS_NOMBRE, sDS_DNI,
                                sDS_TELEF, sDS_FAX, sFC_NAC, sFC_ALTA,
                                sNMREDADI, sNMREDADF,
                                sDS_HORAI, sDS_HORAF, sIT_BAJA, sFC_BAJA,
                                sCD_ANOBAJA, sCD_SEMBAJA,
                                sCD_MOTBAJA, sCD_OPE, sFC_ULTACT);

            snapShot.addElement(dtRec);
          } //Fin while

          // cierra los recursos
          st.close();
          st = null;
          break;

          //____________________________________________
          // (ARS 03-04-01)

        case servletOBTENER_MED_DE_PTO_ANO_SEM_MED_ALTA:

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sMedActivoPtoAnoSemMedAlta);

          // completa los valores de la cl�usula WHERE
          st.setString(1, (String) dtRec.get("CD_PCENTI"));
          st.setString(2, (String) dtRec.get("CD_ANOEPI"));
          st.setString(3, (String) dtRec.get("CD_SEMEPI"));
          st.setString(4, (String) dtRec.get("CD_ANOEPI"));
          st.setString(5, (String) dtRec.get("CD_SEMEPI"));

          // Ejecucion de la query, obteniendo un ResultSet
          rs = st.executeQuery();

          // Procesamiento de cada registro obtenido de la BD
          while (rs.next()) {
            // Recuperacion de los datos a partir del ResultSet de la
            //  query realizada sobre la BD
            String sCD_PCENTI = rs.getString("CD_PCENTI");
            String sCD_MEDCEN = rs.getString("CD_MEDCEN");
            String sCD_E_NOTIF = rs.getString("CD_E_NOTIF");
            String sCD_SEXO = rs.getString("CD_SEXO");
            String sCD_MASIST = rs.getString("CD_MASIST");
            String sCD_ANOEPIA = rs.getString("CD_ANOEPIA");
            String sCD_SEMEPIA = rs.getString("CD_SEMEPIA");
            String sDS_APE1 = rs.getString("DS_APE1");
            String sDS_APE2 = rs.getString("DS_APE2");
            String sDS_NOMBRE = rs.getString("DS_NOMBRE");
            String sDS_DNI = rs.getString("DS_DNI");
            String sDS_TELEF = rs.getString("DS_TELEF");
            String sDS_FAX = rs.getString("DS_FAX");
            java.sql.Date dFC_NAC = rs.getDate("FC_NAC");
            String sFC_NAC = Format.date2String(dFC_NAC);
            java.sql.Date dFC_ALTA = rs.getDate("FC_ALTA");
            String sFC_ALTA = Format.date2String(dFC_ALTA);
            int iNMREDADI = rs.getInt("NMREDADI");
            String sNMREDADI = "" + iNMREDADI;
            int iNMREDADF = rs.getInt("NMREDADF");
            String sNMREDADF = "" + iNMREDADF;
            String sDS_HORAI = rs.getString("DS_HORAI");
            String sDS_HORAF = rs.getString("DS_HORAF");
            String sIT_BAJA = rs.getString("IT_BAJA");
            java.sql.Date dFC_BAJA = rs.getDate("FC_BAJA");
            String sFC_BAJA = Format.date2String(dFC_BAJA);
            String sCD_ANOBAJA = rs.getString("CD_ANOBAJA");
            String sCD_SEMBAJA = rs.getString("CD_SEMBAJA");
            String sCD_MOTBAJA = rs.getString("CD_MOTBAJA");
            String sCD_OPE = rs.getString("CD_OPE");
            java.sql.Date dFC_ULTACT = rs.getDate("FC_ULTACT");
            String sFC_ULTACT = Format.date2String(dFC_ULTACT);

            // Relleno del registro de salida
            //  por los registros devueltos por la query
            dtRec = new MedCent(sCD_PCENTI, sCD_MEDCEN, sCD_E_NOTIF, sCD_SEXO,
                                sCD_MASIST,
                                sCD_ANOEPIA, sCD_SEMEPIA, sDS_APE1, sDS_APE2,
                                sDS_NOMBRE, sDS_DNI,
                                sDS_TELEF, sDS_FAX, sFC_NAC, sFC_ALTA,
                                sNMREDADI, sNMREDADF,
                                sDS_HORAI, sDS_HORAF, sIT_BAJA, sFC_BAJA,
                                sCD_ANOBAJA, sCD_SEMBAJA,
                                sCD_MOTBAJA, sCD_OPE, sFC_ULTACT);

            snapShot.addElement(dtRec);
          } //Fin while

          // cierra los recursos
          st.close();
          st = null;
          break;

      } //fin del switch
    }
    catch (SQLException e1) {
      // traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error al leer";

    }
    catch (Exception e2) {
      // traza el error
      trazaLog(e2);
      bError = true;
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      con.rollback();
      throw new Exception(sMsg);
    }

    // valida la transacci�n
    con.commit();
//    snapShot.addElement(dtRec);
    return snapShot;
  }

}