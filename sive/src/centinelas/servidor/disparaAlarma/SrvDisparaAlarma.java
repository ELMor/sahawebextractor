package centinelas.servidor.disparaAlarma;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

/**
 * @autor ARS
 * @version 1.0
 * Este servlet va a realizar la comparaci�n del n�mero de casos
 * en Centinelas, EDO (en un futuro), etc. Y si supera los umbrales
 * definidos por el indicador, devolver� el c�digo y la descripci�n
 * de cada indicador que ha superado.
 * Desde cliente se le env�a un chorizo o sea un vector con:
 * CD_ANOEPI, CD_SEMEPI, CD_ENFCIE (A�o, semana y enfermedad)
 * Tambien se le pasa el tipo de alarma: A = autom�ticas
 * M = manuales y T = todas.
 * y el n�mero de casos para ese a�o;enfermedad;semana. dados de
 * alta.
 * El servlet devuelve: Una lista con C�digo/Descripci�n de los
 * indicadores que han saltado.
 */

public class SrvDisparaAlarma
    extends DBServlet {

  protected Lista doWork(int modoOp, Lista vParametros) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = "";

    // objetos JDBC
    PreparedStatement st = null;

    // Lista de entrada;
    Lista lsEntrada = new Lista();
    lsEntrada = vParametros;

    // vector con el resultado
    Lista lsResultado = null;

    // filtro
    Data dtRegEnt = null;
    Data dtRegSal = null;
    String sConsulta = null;
    // Distintos par�metros.
    String sCd_enfcie = "";
    String sCd_anoepi = "";
    String sCd_semepi = "";
    String sCd_tipo = "";
    String sCd_indalar = "";
    String sDs_indalar = "";
    double dUmbral = 0.0;
    int nNum_casos = 0;

    switch (modoOp) {

      case 0:
        try {
          con.setAutoCommit(false);
          // Recuperar los datos del cliente.
          dtRegEnt = (Data) lsEntrada.elementAt(0);
          sCd_enfcie = dtRegEnt.getString("CD_ENFCIE");
          sCd_anoepi = dtRegEnt.getString("CD_ANOEPI");
          sCd_semepi = dtRegEnt.getString("CD_SEMEPI");
          sCd_tipo = dtRegEnt.getString("IT_TIPO");
          nNum_casos = Integer.parseInt(dtRegEnt.getString("NUM_CASOS"));
          // Iniciamos el resultset.
          ResultSet rs = null;
          // Idem para el Data de salida:
          dtRegSal = new Data();
          // Lopispo para la lista de salida.
          lsResultado = new Lista();
          // Definimos la consulta:
          sConsulta = "select A.CD_INDALAR, C.DS_INDALAR, (B.NM_COEF*A.NM_VALOR), B.NM_COEF, A.NM_VALOR " +
              "from SIVE_ALARMA_RMC A, SIVE_IND_ALARRMC_ANO B, SIVE_IND_ALAR_RMC C " +
              "where B.CD_ANOEPI=A.CD_ANOEPI and B.CD_INDALAR = A.CD_INDALAR " +
              "and C.CD_INDALAR = A.CD_INDALAR  and A.CD_ANOEPI=? " +
              "and A.CD_SEMEPI=? and  A.CD_INDALAR " +
              "in (select CD_INDALAR from SIVE_IND_ALAR_RMC " +
              "where CD_ENFCIE=? and IT_ACTIVO='S')";
          // P'hacerlo mas general le ubico un filtro por tipo de alarma.
          if (!sCd_tipo.equals("T")) {
            sConsulta = sConsulta.substring(0, sConsulta.length() - 1); // Para quitarle el ")"
            sConsulta += " and IT_TIPO=?)";
          }
          // Ahora asignamos la consulta ... (o como se llame el proceso)
          st = con.prepareStatement(sConsulta);
          st.setString(1, sCd_anoepi);
          st.setString(2, sCd_semepi);
          st.setString(3, sCd_enfcie);
          if (!sCd_tipo.equals("T")) {
            st.setString(4, sCd_tipo);
          }
          rs = st.executeQuery();
          while (rs.next()) {
            sCd_indalar = rs.getString("CD_INDALAR");
            sDs_indalar = rs.getString("DS_INDALAR");
            dUmbral = rs.getDouble("(B.NM_COEF*A.NM_VALOR)");
            // Si ocurre que hay m�s casos que el umbral
            // de uno de los indicadores, pues al vector
            // de salida.
            if (nNum_casos > dUmbral) {
              dtRegSal.put("CD_INDALAR", sCd_indalar);
              dtRegSal.put("DS_INDALAR", sDs_indalar);
              // Este se lo meto, por si acaso hiciera falta.
              dtRegSal.put("NUM_UMBRAL", Double.toString(dUmbral));
              dtRegSal.put("NM_COEF", Double.toString(rs.getDouble("NM_COEF")));
              dtRegSal.put("NM_VALOR", Double.toString(rs.getDouble("NM_VALOR")));
              lsResultado.addElement(dtRegSal);
              dtRegSal = new Data();
            }
          }
          con.commit();
        }
        catch (SQLException e1) {
          trazaLog(e1);
          bError = true;
          /*              if (e1.toString().indexOf("unique constraint")!=-1)
                          sMsg += "Clave repetida.";
                        if (e1.toString().indexOf("parent key not found")!=-1)
                          sMsg += "Registro padre no encontrado.";*/
          sMsg += "Error al leer en la BBDD un registro: ";
          sMsg += e1.toString();
          con.rollback();
        }
        catch (Exception e2) {
          trazaLog(e2);
          bError = true;
          sMsg = "Error inesperado: ";
          sMsg += e2.toString();
          con.rollback();
        }
        break;
    } // Fin del switch.

    // devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }
    return lsResultado;
  }

  public static void main(String args[]) {
    SrvDisparaAlarma srv = new SrvDisparaAlarma();
    Lista vParam = new Lista();
    Lista vDevuelto = new Lista();
    Data dtReg = new Data();

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    // ALTA
    dtReg.put("CD_ANOEPI", "2001");
    dtReg.put("CD_SEMEPI", "03");
    dtReg.put("CD_ENFCIE", "487");
    dtReg.put("NUM_CASOS", "7");
    dtReg.put("IT_TIPO", "T");
    /***/
    vParam.addElement(dtReg);

    try {
      vDevuelto = srv.doDebug(0, vParam);
    }
    catch (Exception e) {
      System.err.println("Error " + e);
    }
    for (int contador = 0; contador < vDevuelto.size(); contador++) {
      System.out.println( (Data) vDevuelto.elementAt(contador));
    }
    srv = null;
  }
}