
package centinelas.servidor.c_mantcartas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;

/*Servidor para realizar el alta de modelos de cartas*/
public class SrvMantCartas
    extends DBServlet {

  public SrvMantCartas() {}

  final protected int servletALTA_MODELO = 1;
  final protected int servletMODIFICACION_MODELO = 2;
  final protected int servletBAJA_MODELO = 3;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {
    // control de error
    boolean bError = false;
    String sMsg = null;

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // vector con el resultado
    Lista snapShot = new Lista();

    //peticion
    Lista lisPeticion = null;
    Data dtRec = null;
    Data dtDetalle = null;

    //ByteArrayInputStream baiStream=null;
    //InputStream isEntrada = null;
    //byte [] arrayPlantilla; //Array de bytes de plantilla

    // Alta de modelo
    final String sALTA_MODELO =
        " insert into SIVE_PLANTILLA_RMC "
        + " (CD_PLANTILLA, DS_PLANTILLA, CD_OPE, FC_ULTACT, IT_BAJA) "
        + " values (?,?,?,SYSDATE,?) ";

    /*    final String sALTA_MODELO =
          " insert into SIVE_PLANTILLA_RMC "
         + " (CD_PLANTILLA, DS_PLANTILLA,CD_OPE, FC_ULTACT, IT_BAJA, LR_PLANTILLA) "
        + " values (?,?,?,SYSDATE,?,?) ";
     */

    // Modificacion de modelo con plantilla
    final String sMODIFICACION_MODELO =
        " update SIVE_PLANTILLA_RMC "
        + " set DS_PLANTILLA = ?, CD_OPE = ?, FC_ULTACT = SYSDATE, "
        + " IT_BAJA = ? "
        + "where CD_PLANTILLA = ?";

    /*    final String sMODIFICACION_MODELO =
          " update SIVE_PLANTILLA_RMC "
        + " set DS_PLANTILLA = ?, CD_OPE = ?, FC_ULTACT = SYSDATE, "
        + " IT_BAJA = ?, LR_PLANTILLA = ? "
        + "where CD_PLANTILLA = ?";
     */

    final String sBAJA_MODELO =
        "delete SIVE_PLANTILLA_RMC where CD_PLANTILLA = ?";

    final String sBAJA_MODELO2 =
        "update SIVE_PLANTILLA_RMC set IT_BAJA='S', DS_MOTIVBAJA=?, FC_ULTACT = ? where CD_PLANTILLA = ?";

    try {
      //No se realiza el commit automaticamente
      con.setAutoCommit(false);

      dtRec = (Data) vParametros.elementAt(0);

      switch (opmode) {

        case servletALTA_MODELO:

          // prepara la query
          st = con.prepareStatement(sALTA_MODELO);
          // completa los valores de la cl�usula WHERE
          st.setString(1, (String) dtRec.get("CD_PLANTILLA"));
          st.setString(2, (String) dtRec.get("DS_PLANTILLA"));
          st.setString(3, (String) dtRec.get("CD_OPE"));
          st.setString(4, (String) dtRec.get("IT_BAJA"));
          //Bits de la plantilla
          /*         arrayPlantilla= (byte[]) (dtRec.get("LR_PLANTILLA"));
                   baiStream= new ByteArrayInputStream(arrayPlantilla);
                   st.setBinaryStream(5,baiStream, arrayPlantilla.length );   //********* Repasar tama�o
            */
           // ejecuta la query
          st.executeUpdate();
          // cierra los recursos
          // baiStream.close();
          st.close();
          st = null;

          break;

        case servletMODIFICACION_MODELO:

          // prepara la query
          st = con.prepareStatement(sMODIFICACION_MODELO);
          // completa los valores de la cl�usula WHERE
          st.setString(1, (String) dtRec.getString("DS_PLANTILLA"));
          st.setString(2, (String) dtRec.getString("CD_OPE"));
          st.setString(3, (String) dtRec.getString("IT_BAJA"));
          st.setString(4, (String) dtRec.getString("CD_PLANTILLA"));
          /*          //Bits de la plantilla
                    arrayPlantilla= (byte[]) (dtRec.get("LR_PLANTILLA"));
                    baiStream= new ByteArrayInputStream(arrayPlantilla);
                   int tope=0;
                   if (arrayPlantilla.length<=2000) tope=2001;
                   else tope = arrayPlantilla.length;
               st.setBinaryStream(4, baiStream, tope);   //********* Repasar tama�o
                    st.setString(5, (String)dtRec.get("CD_PLANTILLA"));
            */
           // ejecuta la query
          st.executeUpdate();
          // cierra los recursos
          //baiStream.close();
          st.close();

          st = null;

          break;

        case servletBAJA_MODELO:

          // prepara la consulta seg�n tenga o no avisos.
          if (dtRec.getString("TIENE_AVISOS").equals("S")) {
            st = con.prepareStatement(sBAJA_MODELO2);
            // completa los valores de la cl�usula WHERE
            st.setString(1, dtRec.getString("DS_MOTIVBAJA"));
            st.setTimestamp(2, Format.string2Timestamp(Format.fechaHoraActual()));
            st.setString(3, (String) dtRec.getString("CD_PLANTILLA"));

          }
          else {
            st = con.prepareStatement(sBAJA_MODELO);
            // completa los valores de la cl�usula WHERE
            st.setString(1, (String) dtRec.getString("CD_PLANTILLA"));
          }

          // ejecuta la query
          st.executeUpdate();
          // cierra los recursos
          st.close();
          st = null;

          break;

      } //fin del switch
    }
    catch (SQLException e1) {
      // traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error operando";

    }
    catch (Exception e2) {
      // traza el error
      trazaLog(e2);
      bError = true;
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      con.rollback();
      throw new Exception(sMsg);
    }

    // valida la transacci�n
    con.commit();
    return snapShot;
  }

}