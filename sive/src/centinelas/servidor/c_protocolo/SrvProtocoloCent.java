package centinelas.servidor.c_protocolo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import centinelas.datos.c_protocolo.DataEnfEnt;
import centinelas.datos.c_protocolo.DataEnfEntCaso;
import centinelas.datos.c_protocolo.DataLayout;
import centinelas.datos.c_protocolo.DataLineasM;
import centinelas.datos.c_protocolo.DataListaValores;
import centinelas.datos.c_protocolo.DataRespCompleta;
import centinelas.datos.c_protocolo.DataValores;
import comun.datosParte;
import comun.datosPreg;
import sapp.DBServlet;

//return: listaDevolver con 2 listas: lista con resp edo
//                                    lista con layout

//si fallo: listaDevolver == null
//si no datos de layout:   listaDevolver: -> 2� lista == null
//si no datos en resp edo: listaDevolver: -> 1� lista == null

public class SrvProtocoloCent
    extends DBServlet {

  protected CLista listaDevolver = null;
  protected CLista listaDataLineasM = null; //lista para la primera query
  protected CLista listaModelos = null; //aprovecho DataLineasM para guardarlo
  protected CLista listaDataPregunta = null; // lista para la segunda query (DataLayout)
  protected CLista listaDataLayout = null; //lista para la total

  protected CLista listaListas = null; //lista para cargar las Choices L que haya
  protected CLista listaListaValores = null; //ya cargadas

  protected CLista listaSalida = null; //intermedia
  protected CLista listaSalidaResp = null; //las respuestas (dataRespCompleta)

  final int servletADIC = 0;
  final int servletNORMAL = 1;
  final int servletNORMAL_B = 2; //para brotes, con TSIVE = 'B'
  final int servletNORMAL_C = 3; //para cent, con TSIVE = 'C'

  String des = "";
  String desL = "";
  String sQuery = "";

  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos de datos
    listaDataLineasM = new CLista();
    listaModelos = new CLista();
    listaDataLayout = new CLista();
    listaDataPregunta = new CLista();
    listaListas = new CLista();
    listaListaValores = new CLista();
    listaSalida = new CLista();
    listaSalidaResp = new CLista();

    DataLineasM datosLineasM = null;
    //DataLayout datosLayout = null;    //total

    //campos LAYOUT************
    String sCD_TPREG = "";
    //sDS_TEXTO = "";
    String sOBLIG = "";
    String sLONG = "";
    String sENT = "";
    String sDEC = "";
    //sCD_MODELO = "";
    //sNM_LIN = "";
    String sCD_PREGUNTA = "";
    String sIT_COND = "";
    Integer iNM_LIN_COND = null;
    String sNM_LIN_COND = "";
    String sCD_PREGUNTA_COND = "";
    String sDS_PREGUNTA_COND = "";
    String sCD_LISTA = "";
    //*************************

    DataEnfEntCaso datosEntrada = null; //entrada al Servlet
    DataRespCompleta dataSalidaResp = null;

    //variable de control para la secuencialidad
    boolean b = true;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 0;
    con = openConnection();
    con.setAutoCommit(false); //aunque son selects

    try {

      // modos de operaci�n
      switch (opmode) {

        //NORMAL*****************************************
        case servletNORMAL:
        case servletNORMAL_B:
        case servletNORMAL_C:

          datosEntrada = (DataEnfEntCaso) param.firstElement();
          if (!datosEntrada.getNM_EDO().trim().equals("")) {

            if (opmode == servletNORMAL) {

              // lanza la query
              sQuery = " select "
                  + "CD_MODELO, NM_LIN, CD_PREGUNTA, "
                  + " DS_RESPUESTA "
                  + "from SIVE_RESP_EDO "
                  +
                  "where CD_TSIVE = ? AND NM_EDO = ? order by CD_MODELO, NM_LIN";

              st = con.prepareStatement(sQuery);
              st.setString(1, "E");
              Integer iCasoSelect = new Integer(datosEntrada.getNM_EDO().trim());
              st.setInt(2, iCasoSelect.intValue());

              iCasoSelect = null;
            }

            //____________________ LRG ___________________________________

            else if (opmode == servletNORMAL_C) {

              // lanza la query
              sQuery = " select "
                  + "CD_MODELO, NM_LIN, CD_PREGUNTA, "
                  + " DS_RESPUESTA "
                  + "from SIVE_RESP_CENTI "
                  + "where CD_TSIVE = ? and NM_ORDEN = ? "
                  + "and CD_ENFCIE = ? and CD_PCENTI = ? and CD_MEDCEN = ? and CD_ANOEPI = ? and CD_SEMEPI = ? "
                  + "order by CD_MODELO, NM_LIN";

              st = con.prepareStatement(sQuery);
              st.setString(1, "C");
              Integer iCasoSelect = new Integer(datosEntrada.getNM_EDO().trim());
              st.setInt(2, iCasoSelect.intValue());
              iCasoSelect = null;
              st.setString(3, datosEntrada.getNotifRMC().getCD_ENFCIE());
              st.setString(4, datosEntrada.getNotifRMC().getCD_PCENTI());
              st.setString(5, datosEntrada.getNotifRMC().getCD_MEDCEN());
              st.setString(6, datosEntrada.getNotifRMC().getCD_ANOEPI());
              st.setString(7, datosEntrada.getNotifRMC().getCD_SEMEPI());
            }

            //_______________

            else if (opmode == servletNORMAL_B) {

              // lanza la query
              sQuery = " select "
                  + "CD_MODELO, NM_LIN, CD_PREGUNTA, "
                  + " DS_RESPUESTA "
                  + "from SIVE_RESP_BROTES "
                  + "where CD_TSIVE = ? AND CD_ANO = ? AND NM_ALERBRO = ? "
                  + " order by CD_MODELO, NM_LIN";

              st = con.prepareStatement(sQuery);
              st.setString(1, "B");
              String sCD_ANO = datosEntrada.getNM_EDO().trim().substring(0, 4);
              Integer iNM_ALERBROSelect = new Integer(datosEntrada.getNM_EDO().
                  trim().substring(4));
              st.setString(2, sCD_ANO);
              st.setInt(3, iNM_ALERBROSelect.intValue());

              sCD_ANO = null;
              iNM_ALERBROSelect = null;
            }

            rs = st.executeQuery();

            // extrae los registros encontrados
            while (rs.next()) {

              Integer NumLinWhile = new Integer(rs.getInt("NM_LIN"));

              // a�ade un nodo
              dataSalidaResp = new DataRespCompleta(datosEntrada.getNM_EDO().
                  trim(),
                  rs.getString("CD_MODELO"),
                  NumLinWhile.toString(),
                  rs.getString("CD_PREGUNTA"),
                  null, null, null, null, null,
                  rs.getString("DS_RESPUESTA"), null, null);

              listaSalida.addElement(dataSalidaResp);
              NumLinWhile = null;
            } //fin while
            rs.close();
            rs = null;
            st.close();
            st = null;

            //RECOMPONEMOS LA PARTE QUE NOS FALTA EN LA listaSalida (valor)
            for (i = 0; i < listaSalida.size(); i++) {
              dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(i);

              sQuery = "select "
                  + " a.DS_LISTAP, a.DSL_LISTAP  "
                  + "from SIVE_LISTA_VALORES a, SIVE_PREGUNTA b where "
                  + "(b.CD_PREGUNTA = ? and a.CD_LISTAP = ? "
                  + " and a.CD_LISTA = b.CD_LISTA)";

              st = con.prepareStatement(sQuery);
              st.setString(1, dataSalidaResp.getCodPregunta()); //cd_pregunta
              st.setString(2, dataSalidaResp.getDesPregunta()); //cd_listap
              rs = st.executeQuery();
              //solo saldra un registro, o (ninguno: no se modifica)
              String valor = "";
              while (rs.next()) {
                //modifico el dataSalidaResp
                listaSalida.removeElementAt(i);
                des = rs.getString("DS_LISTAP");
                desL = rs.getString("DSL_LISTAP");

                if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                    && (desL != null)) {
                  valor = desL;
                }
                else {
                  valor = des;

                }

                listaSalida.insertElementAt
                    (new DataRespCompleta(dataSalidaResp.getCaso(),
                                          dataSalidaResp.getCodModelo(),
                                          dataSalidaResp.getNumLinea(),
                                          dataSalidaResp.getCodPregunta(),
                                          null, null, null, null, null,
                                          dataSalidaResp.getDesPregunta(),
                                          valor, null), i);
              }
            }

          } //fin de if de caso informado

          //veo si esos modelos son obligatorios y su nivel
          for (i = 0; i < listaSalida.size(); i++) {
            dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(i);
            sQuery = "SELECT CD_NIVEL_1, CD_NIVEL_2, "
                + " CD_CA , IT_OK FROM SIVE_MODELO WHERE "
                + " CD_TSIVE = ? AND CD_MODELO = ?";

            st = con.prepareStatement(sQuery);

            if (opmode == servletNORMAL) {
              st.setString(1, "E");
            }
            else if (opmode == servletNORMAL_B) {
              st.setString(1, "B");
            }
            else if (opmode == servletNORMAL_C) {
              st.setString(1, "C");

            }

            st.setString(2, dataSalidaResp.getCodModelo());
            rs = st.executeQuery();
            String n1 = null;
            String n2 = null;
            String ca = null;
            String nivel = null; //"CNE"; "CA"; "N1", "N2"
            while (rs.next()) {

              //modifico el dataSalidaResp
              listaSalida.removeElementAt(i);
              n1 = rs.getString("CD_NIVEL_1");
              n2 = rs.getString("CD_NIVEL_2");
              ca = rs.getString("CD_CA");

              //"CNE"; "CA"; "N1", "N2"
              if (n1 == null && n2 == null && ca == null) {
                nivel = "CNE";
              }
              else if (n1 == null && n2 == null && ca != null) {
                nivel = "CA";
              }
              else if (n1 != null && n2 == null && ca != null) {
                nivel = "N1";
              }
              else if (n1 != null && n2 != null && ca != null) {
                nivel = "N2";

              }

              listaSalida.insertElementAt
                  (new DataRespCompleta(dataSalidaResp.getCaso(),
                                        dataSalidaResp.getCodModelo(),
                                        dataSalidaResp.getNumLinea(),
                                        dataSalidaResp.getCodPregunta(), nivel,
                                        ca, n1, n2,
                                        rs.getString("IT_OK"),
                                        dataSalidaResp.getDesPregunta(),
                                        dataSalidaResp.getValorLista(),
                                        dataSalidaResp.getTipoPregunta()), i);

            } //while

          } //for

          //------------------------------------------------

          //la salida de datos (igual a listaCopia)
          CLista listaSalidaResp = new CLista();
          for (i = 0; i < listaSalida.size(); i++) {
            dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(i);
            listaSalidaResp.addElement(dataSalidaResp);

          } //for

          Ordenar_listaSalida();
          if (opmode == servletNORMAL) {
            cuatro_consultas(con, st, rs, datosEntrada, param, "E");
          }
          else if (opmode == servletNORMAL_B) {
            cuatro_consultas(con, st, rs, datosEntrada, param, "B");
          }
          else if (opmode == servletNORMAL_C) {
            cuatro_consultas(con, st, rs, datosEntrada, param, "C");

          }

          if (listaDataLineasM.size() == 0) {
            b = false;
            listaDataLayout = null;
          }

          //TENEMOS !!!!!!!!!!!!!!!!!!!!!!!!!!!
          //lista de Salida con dataSalidaResp (respedo)
          // listaDataLineasM con los modelos

          //NUEVA ETAPA********************************
          if (b) {

            EncontrarModelos(); // lo carga en Modelos, con DataLineasM

            query = ConsultaPregunta();

            st = con.prepareStatement(query);

            for (int ind = 0; ind < listaModelos.size(); ind++) {

              //nos movemos por Modelos
              DataLineasM datosCodModelo = (DataLineasM) listaModelos.elementAt(
                  ind);
              if (opmode == servletNORMAL) {
                st.setString(1, "E");
              }
              else if (opmode == servletNORMAL_B) {
                st.setString(1, "B");
              }
              else if (opmode == servletNORMAL_C) {
                st.setString(1, "C");

              }

              st.setString(2, datosCodModelo.getCodModelo());
              rs = st.executeQuery();

              while (rs.next()) {
                //vamos llenando la lista  listaDataPregunta en un obj DataLayout
                listaDataPregunta.addElement(new DataLayout
                                             (rs.getString("CD_TPREG"),
                                              "",
                                              rs.getString("IT_OBLIGATORIO"),
                                              new Integer(rs.getInt("NM_LONG")).
                                              toString(),
                                              new Integer(rs.getInt("NM_ENT")).
                                              toString(),
                                              new Integer(rs.getInt("NM_DEC")).
                                              toString(),
                                              rs.getString("CD_MODELO"),
                                              new Integer(rs.getInt("NM_LIN")).
                                              toString(),
                                              rs.getString("CD_PREGUNTA"),
                                              rs.getString("IT_CONDP"),
                                              new Integer(rs.getInt("NM_LIN_PC")).
                                              toString(),
                                              rs.getString("CD_PREGUNTA_PC"),
                                              rs.getString("DS_VPREGUNTA_PC"),
                                              rs.getString("CD_LISTA"), null,
                                              datosCodModelo.getComunidad(),
                                              datosCodModelo.getNivel1(),
                                              datosCodModelo.getNivel2()));

              } //fin del while
              rs.close();
              rs = null;

            } //fin for
            st.close();
            st = null;

            if (listaDataPregunta.size() == 0) {
              b = false;
              listaDataLayout = null;
            }

          } //fin de b

          //NUEVA ETAPA************
          if (b) { //si sigue siendo true
            RecomponerLista();
          }

          //si en algun momento fue true,  listaDataLayout = null
          //en otro caso, esta cargada con datos
          if (listaDataLayout != null) {

            //existen listas en el protocolo
            if (listaListas != null) {

              Recomponer_solouncod_porLista();

              //CARGAMOS LISTALISTAVALORES-------------------
              String sCod = "";
              String sDes = "";

              for (i = 0; i < listaListas.size(); i++) {
                DataListaValores dataListas = null;
                dataListas = (DataListaValores) listaListas.elementAt(i);

                query = ConsultaLista();
                st = con.prepareStatement(query);
                st.setString(1, dataListas.getCodLista().trim());
                rs = st.executeQuery();

                // extrae LOS registroS encontradoS
                while (rs.next()) {

                  sCod = rs.getString("CD_LISTAP");
                  des = rs.getString("DS_LISTAP");
                  desL = rs.getString("DSL_LISTAP");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (desL != null)) {
                    sDes = desL;
                  }
                  else {
                    sDes = des;

                    // a�ade un nodo
                  }
                  DataListaValores dataSalida = new DataListaValores
                      (dataListas.getCodLista().trim(),
                       sCod,
                       sDes);

                  listaListaValores.addElement(dataSalida);
                  dataSalida = null;

                } //fin del while

                rs.close();
                rs = null;
                st.close();
                st = null;

              } //for, para cada codlista
              //-----------------------------
              Recomponer_ListaValores_conVector();
              Recomponer_ListaDataLayout();
              listaDataLayout.trimToSize();
            }
            else {
              listaDataLayout.trimToSize();
            }
          }
          //no hay layout
          else {
            listaDataLayout = null;
          }

          listaDevolver = new CLista();
          listaSalidaResp.trimToSize();
          listaDevolver.addElement(listaSalidaResp);
          listaDevolver.addElement(listaDataLayout);
          listaDevolver.trimToSize();

          break;
        case servletADIC:

          //param: listaEntrada con DataEnfEnt
          //       listaValores con dataPreg
          Vector listaSalidaCris = new Vector();
          DataEnfEnt datosEntradaCris = null;
          datosPreg dataCris = null;

          CLista listaEntorno = (CLista) param.elementAt(0);
          datosEntradaCris = (DataEnfEnt) listaEntorno.elementAt(0);

          listaSalidaCris = (Vector) param.elementAt(1); //TODAS las listaValores (listaPartes)
          listaSalida = new CLista();

          for (int iPartes = 0; iPartes < listaSalidaCris.size(); iPartes++) {
            datosParte datPARTE = (datosParte) listaSalidaCris.elementAt(
                iPartes);
            Vector vValores = (Vector) datPARTE.preguntas;
            for (int y = 0; y < vValores.size(); y++) {
              dataCris = (datosPreg) vValores.elementAt(y);
              DataRespCompleta datosnuevos = new DataRespCompleta("",
                  dataCris.modelo,
                  (new Integer(dataCris.linea)).toString(),
                  dataCris.pregunta, dataCris.nivel,
                  dataCris.ca,
                  dataCris.n1,
                  dataCris.n2,
                  dataCris.it_ok, dataCris.respuesta,
                  dataCris.ValorLista,
                  "");
              listaSalida.addElement(datosnuevos);

            }

          }
          Ordenar_listaSalida();

          //resconstruir dataEntrada de dataenfent -> dataenfentcaso
          DataEnfEntCaso datosRecons = new DataEnfEntCaso(
              datosEntradaCris.getCodEnfermedad(),
              datosEntradaCris.getComunidad(),
              datosEntradaCris.getNivelUNO(),
              datosEntradaCris.getNivelDOS(),
              "",
              null); //RETOCADO CENTI
          //----------
          cuatro_consultas(con, st, rs, datosRecons, param, "E");

          if (listaDataLineasM.size() == 0) {
            b = false;
            listaDataLayout = null;
          }

          //TENEMOS !!!!!!!!!!!!!!!!!!!!!!!!!!!
          //lista de Salida con dataSalidaResp (respedo)
          // listaDataLineasM con los modelos

          //NUEVA ETAPA********************************
          if (b) {
            EncontrarModelos(); // lo carga en Modelos, con DataLineasM

            query = ConsultaPregunta();
            st = con.prepareStatement(query);

            for (int ind = 0; ind < listaModelos.size(); ind++) {
              //nos movemos por Modelos
              DataLineasM datosCodModelo = (DataLineasM) listaModelos.elementAt(
                  ind);
              st.setString(1, "E");
              st.setString(2, datosCodModelo.getCodModelo());
              rs = st.executeQuery();

              while (rs.next()) {
                //vamos llenando la lista  listaDataPregunta en un obj DataLayout
                listaDataPregunta.addElement(new DataLayout
                                             (rs.getString("CD_TPREG"),
                                              "",
                                              rs.getString("IT_OBLIGATORIO"),
                                              new Integer(rs.getInt("NM_LONG")).
                                              toString(),
                                              new Integer(rs.getInt("NM_ENT")).
                                              toString(),
                                              new Integer(rs.getInt("NM_DEC")).
                                              toString(),
                                              rs.getString("CD_MODELO"),
                                              new Integer(rs.getInt("NM_LIN")).
                                              toString(),
                                              rs.getString("CD_PREGUNTA"),
                                              rs.getString("IT_CONDP"),
                                              new Integer(rs.getInt("NM_LIN_PC")).
                                              toString(),
                                              rs.getString("CD_PREGUNTA_PC"),
                                              rs.getString("DS_VPREGUNTA_PC"),
                                              rs.getString("CD_LISTA"), null,
                                              datosCodModelo.getComunidad(),
                                              datosCodModelo.getNivel1(),
                                              datosCodModelo.getNivel2()));

              } //fin del while
              rs.close();
              rs = null;

            } //fin for
            st.close();
            st = null;

            if (listaDataPregunta.size() == 0) {
              b = false;
              listaDataLayout = null;
            }

          } //fin de b

          //NUEVA ETAPA************
          if (b) { //si sigue siendo true
            RecomponerLista();
          }

          //si en algun momento fue true,  listaDataLayout = null
          //en otro caso, esta cargada con datos
          if (listaDataLayout != null) {

            //existen listas en el protocolo
            if (listaListas != null) {

              Recomponer_solouncod_porLista();

              //CARGAMOS LISTALISTAVALORES-------------------
              String sCod = "";
              String sDes = "";

              for (i = 0; i < listaListas.size(); i++) {
                DataListaValores dataListas = null;
                dataListas = (DataListaValores) listaListas.elementAt(i);

                query = ConsultaLista();
                st = con.prepareStatement(query);
                st.setString(1, dataListas.getCodLista().trim());
                rs = st.executeQuery();

                // extrae LOS registroS encontradoS
                while (rs.next()) {

                  sCod = rs.getString("CD_LISTAP");
                  des = rs.getString("DS_LISTAP");
                  desL = rs.getString("DSL_LISTAP");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (desL != null)) {
                    sDes = desL;
                  }
                  else {
                    sDes = des;

                    // a�ade un nodo
                  }
                  DataListaValores dataSalida = new DataListaValores
                      (dataListas.getCodLista().trim(),
                       sCod,
                       sDes);

                  listaListaValores.addElement(dataSalida);
                  dataSalida = null;

                } //fin del while

                rs.close();
                rs = null;
                st.close();
                st = null;

              } //for, para cada codlista
              //-----------------------------
              Recomponer_ListaValores_conVector();
              Recomponer_ListaDataLayout();
              listaDataLayout.trimToSize();
            }
            else {
              listaDataLayout.trimToSize();
            }
          }
          //no hay layout
          else {
            listaDataLayout = null;
          }

          listaDevolver = new CLista();
          listaSalidaCris.trimToSize(); //
          listaDevolver.addElement(listaSalidaCris); //
          listaDevolver.addElement(listaDataLayout);
          listaDevolver.trimToSize();

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaDevolver = null;
      throw ex;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    return listaDevolver;

  } //fin doPost

//encontrar cd_modelo y en el orden.  (Modelos: 1, 3, 2)
  protected void EncontrarModelos() {
    String sAlm = "";

    for (int indice = 0; indice < listaDataLineasM.size(); indice++) {
      DataLineasM datosParaBusqueda = (DataLineasM) listaDataLineasM.elementAt(
          indice);
      if (!sAlm.equals(datosParaBusqueda.getCodModelo())) { //modelo diferente
        listaModelos.addElement(datosParaBusqueda);
      }
      sAlm = datosParaBusqueda.getCodModelo();

    }
  }

  protected String ConsultaLista() {

    return ("select "
            + "CD_LISTAP, DS_LISTAP, DSL_LISTAP "
            + "from SIVE_LISTA_VALORES "
            + "where "
            + "CD_LISTA = ? ");
  }

//Consulta sobre sive_lineas_item y sive_pregunta
  protected String ConsultaPregunta() {

    return ("select " +
            " b.CD_MODELO, b.NM_LIN, b.CD_PREGUNTA, b.CD_PREGUNTA_PC, " +
            " b.IT_OBLIGATORIO, b.IT_CONDP, b.NM_LIN_PC, b.DS_VPREGUNTA_PC, " +
            " a.CD_TPREG, a.CD_LISTA, a.NM_LONG, a.NM_ENT, a.NM_DEC " +
            " from SIVE_LINEA_ITEM b, SIVE_PREGUNTA a " +
            " where ((b.CD_TSIVE = ?) and (b.CD_MODELO = ?) and " +
            "(b.CD_TSIVE = a.CD_TSIVE and  b.CD_PREGUNTA = a.CD_PREGUNTA )) " +
            " order by NM_LIN");

  }

//tenemos listaListaValores + listaDataLayout = listaDataLayout;
  protected void Recomponer_ListaDataLayout() {

    DataLayout data = null;
    DataListaValores datalistavalores = null;
    DataListaValores datareserva = null;
    boolean bSalirfor = false;

    for (int i = 0; i < listaDataLayout.size(); i++) {
      data = (DataLayout) listaDataLayout.elementAt(i);
      if (data.getCodLista() != null) {
        //buscar en listaListaValores su vector
        for (int k = 0; k < listaListaValores.size(); k++) {
          if (!bSalirfor) {

            datalistavalores = (DataListaValores) listaListaValores.elementAt(k);
            if (datalistavalores.getCodLista().trim().equals(data.getCodLista().
                trim())) {
              listaDataLayout.removeElementAt(i);
              listaDataLayout.insertElementAt(new DataLayout(data.getTipoPreg(),
                  data.getDesTexto(), data.getOblig(),
                  data.getLongitud(), data.getEntera(),
                  data.getDecimal(), data.getCodModelo(),
                  data.getNumLinea(), data.getCodPregunta(),
                  data.getCondicionada(), data.getNumLineaCond(),
                  data.getCodPreguntaCond(), data.getDesPreguntaCond(),
                  data.getCodLista(), datalistavalores.getVALORES(),
                  data.getCA(), data.getNIVEL_1(), data.getNIVEL_2()), i);

            }
          }
        } //for
      }
    } //for
  }

//listaListaValores: DataListaValores(sCodLista, cod, des) --> listaListaValores: DataListaValores(sCodLista, vector)
  protected void Recomponer_ListaValores_conVector() {

    CLista lista = new CLista();
    DataListaValores dataEntrada = null;
    DataListaValores data = null;
    String sCodLista = "";
    String sCodListaBK = "";

    Vector vValores = new Vector();
    DataValores dat = null;
    for (int i = 0; i < listaListaValores.size(); i++) {

      dataEntrada = (DataListaValores) listaListaValores.elementAt(i);

      if (i == 0) {

        sCodLista = dataEntrada.getCodLista();
        vValores.addElement(new DataValores(dataEntrada.getCod(),
                                            dataEntrada.getDes()));
        sCodListaBK = sCodLista;
      }
      else {
        sCodLista = dataEntrada.getCodLista();

        if (sCodLista.equals(sCodListaBK)) { //la misma lista
          vValores.addElement(new DataValores(dataEntrada.getCod(),
                                              dataEntrada.getDes()));
          sCodListaBK = sCodLista;
        }
        else { //cambiamos a otro codlista
          //*******
           Vector vNuevo = new Vector();
          for (int k = 0; k < vValores.size(); k++) {
            dat = (DataValores) vValores.elementAt(k);
            vNuevo.addElement(dat);
          }
          //*******
           lista.addElement(new DataListaValores(sCodListaBK, vNuevo));
          vNuevo = null;

          vValores.removeAllElements();
          vValores.addElement(new DataValores(dataEntrada.getCod(),
                                              dataEntrada.getDes()));
          sCodListaBK = sCodLista;
        }
      } //if-else
    } //for

    //para el ultimo
    //*******
     Vector vNuevo = new Vector();
    for (int k = 0; k < vValores.size(); k++) {
      dat = (DataValores) vValores.elementAt(k);
      vNuevo.addElement(dat);
    }
    //*******
     lista.addElement(new DataListaValores(sCodListaBK, vNuevo));
    vNuevo = null;
    vValores = null;

    //volcamos lista a listaListaValores
    listaListaValores = lista;

  } //fin

//A partir de listaListas, puede existir cod de listas repetidas
  protected void Recomponer_solouncod_porLista() {

    String sCod = "";
    DataListaValores dataEntrada = null;
    DataListaValores dataUsar = null;
    CLista listaUsar = new CLista();

    for (int i = 0; i < listaListas.size(); i++) {

      dataEntrada = (DataListaValores) listaListas.elementAt(i);
      sCod = dataEntrada.getCodLista().trim();

      if (i == 0) {
        dataUsar = new DataListaValores(sCod, "", "");
        listaUsar.addElement(dataUsar);
      }
      else {
        boolean byaesta = false;
        for (int u = 0; u < listaUsar.size(); u++) {
          dataUsar = (DataListaValores) listaUsar.elementAt(u);
          if (dataUsar.getCodLista().trim().equals(sCod)) {
            byaesta = true;
          }
        } //for

        if (byaesta) { //si esta en listaUsar, no se hace nada
          //registro que ya sobra y esta almacenado
        }
        else {
          dataUsar = new DataListaValores(sCod, "", "");
          listaUsar.addElement(dataUsar);
        }
      }
    } //fin for ppal

    //COPIA   en listaListas, la de usar
    listaListas.removeAllElements();
    for (int i = 0; i < listaUsar.size(); i++) {
      dataUsar = (DataListaValores) listaUsar.elementAt(i);
      listaListas.addElement(dataUsar);
    }

  } //fin recomponercoduno

  protected void RecomponerLista() {

    boolean b = true;
    //leemos una lineaM: si D, "D", null, null...add
    //                   si P, leemos...add

    int iListaLineasM = 0; //ppal
    int iListaPregunta = 0; //para movernos por la lista secundaria

    //tipos de registros
    String sTipoRegistro = "";
    String sAlmacen = "-1";

    for (iListaLineasM = 0; iListaLineasM < listaDataLineasM.size();
         iListaLineasM++) {
      if (b) {
        DataLineasM datosLinea = (DataLineasM) listaDataLineasM.elementAt(
            iListaLineasM);
        //si la primera no es descripcion, lista=null------------------------
        if (iListaLineasM == 0 && !datosLinea.getTipoLinea().equals("D")) {

          if (b) {
            b = false;
          }
        }
        //si es una descripcion, a a�adimos a listaDataLayout----------------
        if (datosLinea.getTipoLinea().equals("D")) {

          //vemos en que tipo de registro estamos
          if (datosLinea.getComunidad() == null &&
              datosLinea.getNivel1() == null &&
              datosLinea.getNivel2() == null) {
            sTipoRegistro = "CNE";
          }
          else if (datosLinea.getComunidad() != null &&
                   datosLinea.getNivel1() == null &&
                   datosLinea.getNivel2() == null) {
            sTipoRegistro = "CA";
          }
          else if (datosLinea.getComunidad() != null &&
                   datosLinea.getNivel1() != null &&
                   datosLinea.getNivel2() == null) {
            sTipoRegistro = "N1";
          }
          else if (datosLinea.getComunidad() != null &&
                   datosLinea.getNivel1() != null &&
                   datosLinea.getNivel2() != null) {
            sTipoRegistro = "N2";
          }

          //a�adimos una Descripcion de tipo X: CNE, CA, N1, N2
          //se a�ade cuando el registro es diferente del almacen
          if (!sAlmacen.trim().equals(sTipoRegistro.trim())) {

            listaDataLayout.addElement(new DataLayout
                                       ("X", sTipoRegistro.trim(), null, null, null, null,
                                        null, null,
                                        null, null, null, null, null, null, null,
                                        datosLinea.getComunidad(),
                                        datosLinea.getNivel1(),
                                        datosLinea.getNivel2()));

            sAlmacen = sTipoRegistro;
          }

          //a�adimos D en tipoPregunta

          listaDataLayout.addElement(new DataLayout
                                     ("D",
                                      datosLinea.getDesTexto(), null, null, null, null,
                                      datosLinea.getCodModelo(),
                                      datosLinea.getNmLinea(),
                                      null, null, null, null, null, null, null,
                                      datosLinea.getComunidad(),
                                      datosLinea.getNivel1(),
                                      datosLinea.getNivel2()));

        }
        //si es una pregunta--------------------------------------------------
        if (datosLinea.getTipoLinea().equals("P")) {

          DataLayout datosPregunta = (DataLayout) listaDataPregunta.elementAt(
              iListaPregunta);

          //comprobaciones  F-K
          if (!datosLinea.getCodModelo().equals(datosPregunta.getCodModelo()) ||
              !datosLinea.getNmLinea().equals(datosPregunta.getNumLinea())) {
            if (b) {
              b = false;
            }
          }

          //a�adimos a lista definitiva
          if (b) {

            //vemos el tipo de pregunta:
            if (datosPregunta.getTipoPreg().equals("L")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          null, null, null,
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(), null,
                                          datosPregunta.getCA(),
                                          datosPregunta.getNIVEL_1(),
                                          datosPregunta.getNIVEL_2()));

              //cargamos listaListas con las choices L que encontramos
              listaListas.addElement(new DataListaValores
                                     (datosPregunta.getCodLista(), "", ""));

            }
            if (datosPregunta.getTipoPreg().equals("B")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          null, null, null,
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(),
                                          null,
                                          datosPregunta.getCA(),
                                          datosPregunta.getNIVEL_1(),
                                          datosPregunta.getNIVEL_2()));
            }
            if (datosPregunta.getTipoPreg().equals("N")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          datosPregunta.getLongitud(),
                                          datosPregunta.getEntera(),
                                          datosPregunta.getDecimal(),
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(),
                                          null,
                                          datosPregunta.getCA(),
                                          datosPregunta.getNIVEL_1(),
                                          datosPregunta.getNIVEL_2()));
            }
            if (datosPregunta.getTipoPreg().equals("C")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          datosPregunta.getLongitud(), null, null,
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(),
                                          null,
                                          datosPregunta.getCA(),
                                          datosPregunta.getNIVEL_1(),
                                          datosPregunta.getNIVEL_2()));
            }
            if (datosPregunta.getTipoPreg().equals("F")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          null, null, null,
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(),
                                          null,
                                          datosPregunta.getCA(),
                                          datosPregunta.getNIVEL_1(),
                                          datosPregunta.getNIVEL_2()));
            }

          } //fin de a�adir  (b)

          iListaPregunta = iListaPregunta + 1;
        } //P ---------------------------------------------------------------------
      } //continuamos si es true!!
    } //fin for
    if (!b) {
      listaDataLayout = null;
    }
    else {
      for (int i = 0; i < listaDataLayout.size(); i++) {
        DataLayout dat = (DataLayout) (listaDataLayout.elementAt(i));

      }
    }

  }

  public void Ordenar_listaSalida() {

    DataRespCompleta dataSalidaResp = null;
    int i = 0;
    //ordenamos la lista por niveles
    CLista listaCopia = new CLista();
    for (i = 0; i < listaSalida.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(i);
      listaCopia.addElement(dataSalidaResp);
    } //for

    listaSalida = new CLista();
    boolean bExiste = false;
    for (i = 0; i < listaCopia.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaCopia.elementAt(i);
      if (dataSalidaResp.getNivel().trim().equals("CNE")) {
        listaSalida.addElement(dataSalidaResp);
        bExiste = true;
        break;
      }
    }
    if (!bExiste) {
      listaSalida.addElement(new DataRespCompleta(null, null, null, null, "", null, null, null, null, null, null, null));

    }

    bExiste = false;
    for (i = 0; i < listaCopia.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaCopia.elementAt(i);
      if (dataSalidaResp.getNivel().trim().equals("CA")) {
        listaSalida.addElement(dataSalidaResp);
        bExiste = true;
        break;
      }
    }
    if (!bExiste) {
      listaSalida.addElement(new DataRespCompleta(null, null, null, null, "", null, null, null, null, null, null, null));

    }

    bExiste = false;
    for (i = 0; i < listaCopia.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaCopia.elementAt(i);
      if (dataSalidaResp.getNivel().trim().equals("N1")) {
        listaSalida.addElement(dataSalidaResp);
        bExiste = true;
        break;
      }
    }
    if (!bExiste) {
      listaSalida.addElement(new DataRespCompleta(null, null, null, null, "", null, null, null, null, null, null, null));

    }
    bExiste = false;
    for (i = 0; i < listaCopia.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaCopia.elementAt(i);
      if (dataSalidaResp.getNivel().trim().equals("N2")) {
        listaSalida.addElement(dataSalidaResp);
        bExiste = true;
        break;
      }
    }
    if (!bExiste) {
      listaSalida.addElement(new DataRespCompleta(null, null, null, null, "", null, null, null, null, null, null, null));

      //------------------------------------------------
    }
    listaCopia = null;

  }

  public void cuatro_consultas(Connection con,
                               PreparedStatement st, ResultSet rs,
                               DataEnfEntCaso datosEntrada, CLista param,
                               String sTSive) throws Exception {

    DataRespCompleta dataSalidaResp = null;

    // campos primera consulta
    String sCD_MODELO = "";
    String sNM_LIN = "";
    Integer iNM_LIN = null;
    String sCD_TLINEA = "";
    String sDS_TEXTO = "";
    String sCA = "";
    String sN1 = "";
    String sN2 = "";
    String TSIVE = sTSive;

    //hacemos 4 consultas
    //CNE!!!!!!
    //!!!!!!!!!
    dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(0);

    if (dataSalidaResp.getNivel().trim().equals("CNE")) {

      sQuery = " select "
          + "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO "
          + "from SIVE_LINEASM "
          + "where CD_TSIVE = ? and  CD_MODELO = ? order by  CD_MODELO, NM_LIN";

      st = con.prepareStatement(sQuery);

      st.setString(1, TSIVE);
      st.setString(2, dataSalidaResp.getCodModelo().trim());

    }
    else {
      sQuery = " select "
          + "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  "
          + "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 "
          + "from SIVE_MODELO a, SIVE_LINEASM b  "
          + "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) "
          + "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)"
          + "and (a.CD_ENFCIE = ?) "
          + "and ( "
          +
          "(a.CD_CA is null and a.CD_NIVEL_1 is null and a.CD_NIVEL_2 is null)  "
          + ") "
          + ") "
          + "order by b.CD_MODELO, b.NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, TSIVE);
      st.setString(2, "S");
      st.setString(3, datosEntrada.getCodEnfermedad().trim());

    } //else de cne

    rs = st.executeQuery();

    // extrae el registro encontrado y lo carga en listaDataLineasM
    while (rs.next()) {
      des = rs.getString("DS_TEXTO");
      desL = rs.getString("DSL_TEXTO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
          && (desL != null)) {
        sDS_TEXTO = desL;
      }
      else {
        sDS_TEXTO = des;

      }
      sCD_MODELO = rs.getString("CD_MODELO");
      iNM_LIN = new Integer(rs.getInt("NM_LIN"));
      sNM_LIN = iNM_LIN.toString();
      sCD_TLINEA = rs.getString("CD_TLINEA");

      if (dataSalidaResp.getNivel().trim().equals("CNE")) {
        sCA = dataSalidaResp.getCA();
        sN1 = dataSalidaResp.getN1();
        sN2 = dataSalidaResp.getN2();
      }
      else {
        sCA = null;
        sN1 = null;
        sN2 = null;
      }

      // a�ade un nodo
      listaDataLineasM.addElement(new DataLineasM
                                  (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                   sCA, sN1, sN2));
    } //while
    rs.close();
    rs = null;
    st.close();
    st = null;

    //CA!!!!!!!
    //!!!!!!!!!
    dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(1);

    if (dataSalidaResp.getNivel().trim().equals("CA")) {

      sQuery = " select " +
          "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
          "from SIVE_LINEASM " +
          "where CD_TSIVE = ? and  CD_MODELO = ? order by  CD_MODELO, NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, TSIVE);
      st.setString(2, dataSalidaResp.getCodModelo().trim());

    }
    else {
      sQuery = " select " +
          "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
          "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
          "from SIVE_MODELO a, SIVE_LINEASM b  " +
          "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
          "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
          "and (a.CD_ENFCIE = ?) " +
          "and ( " +
          "(a.CD_CA = ? and a.CD_NIVEL_1 is null and a.CD_NIVEL_2 is null)  " +
          ") " +
          ") " +
          "order by b.CD_MODELO, b.NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, TSIVE);
      st.setString(2, "S");
      st.setString(3, datosEntrada.getCodEnfermedad().trim());
      st.setString(4, datosEntrada.getComunidad());
    } //else de cne

    rs = st.executeQuery();

    // extrae el registro encontrado y lo carga en listaDataLineasM
    while (rs.next()) {
      des = rs.getString("DS_TEXTO");
      desL = rs.getString("DSL_TEXTO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
          && (desL != null)) {
        sDS_TEXTO = desL;
      }
      else {
        sDS_TEXTO = des;

      }
      sCD_MODELO = rs.getString("CD_MODELO");
      iNM_LIN = new Integer(rs.getInt("NM_LIN"));
      sNM_LIN = iNM_LIN.toString();
      sCD_TLINEA = rs.getString("CD_TLINEA");

      if (dataSalidaResp.getNivel().trim().equals("CA")) {

        sCA = dataSalidaResp.getCA();
        sN1 = dataSalidaResp.getN1();
        sN2 = dataSalidaResp.getN2();
      }
      else {
        sCA = datosEntrada.getComunidad();
        sN1 = null;
        sN2 = null;
      }

      // a�ade un nodo
      listaDataLineasM.addElement(new DataLineasM
                                  (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                   sCA, sN1, sN2));
    } //while
    rs.close();
    rs = null;
    st.close();
    st = null;

    //N1!!!!!!!
    //!!!!!!!!!

    dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(2);

    if (dataSalidaResp.getNivel().trim().equals("N1")) {

      sQuery = " select " +
          "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
          "from SIVE_LINEASM " +
          "where CD_TSIVE = ? and  CD_MODELO = ? order by  CD_MODELO, NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, TSIVE);
      st.setString(2, dataSalidaResp.getCodModelo().trim());

    }
    else {
      sQuery = " select " +
          "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
          "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
          "from SIVE_MODELO a, SIVE_LINEASM b  " +
          "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
          "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
          "and (a.CD_ENFCIE = ?) " +
          "and ( " +
          "(a.CD_CA = ? and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 is null)  " +
          ") " +
          ") " +
          "order by b.CD_MODELO, b.NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, TSIVE);
      st.setString(2, "S");
      st.setString(3, datosEntrada.getCodEnfermedad().trim());
      st.setString(4, datosEntrada.getComunidad());
      st.setString(5, datosEntrada.getNivelUNO());
    } //else de cne

    rs = st.executeQuery();

    // extrae el registro encontrado y lo carga en listaDataLineasM
    while (rs.next()) {
      des = rs.getString("DS_TEXTO");
      desL = rs.getString("DSL_TEXTO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
          && (desL != null)) {
        sDS_TEXTO = desL;
      }
      else {
        sDS_TEXTO = des;

      }
      sCD_MODELO = rs.getString("CD_MODELO");
      iNM_LIN = new Integer(rs.getInt("NM_LIN"));
      sNM_LIN = iNM_LIN.toString();
      sCD_TLINEA = rs.getString("CD_TLINEA");

      if (dataSalidaResp.getNivel().trim().equals("N1")) {
        sCA = dataSalidaResp.getCA();
        sN1 = dataSalidaResp.getN1();
        sN2 = dataSalidaResp.getN2();
      }
      else {
        sCA = datosEntrada.getComunidad();
        sN1 = datosEntrada.getNivelUNO();
        sN2 = null;
      }

      // a�ade un nodo
      listaDataLineasM.addElement(new DataLineasM
                                  (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                   sCA, sN1, sN2));
    } //while
    rs.close();
    rs = null;
    st.close();
    st = null;

    //N2!!!!!!!
    //!!!!!!!!!
    dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(3);

    if (dataSalidaResp.getNivel().trim().equals("N2")) {

      sQuery = " select " +
          "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
          "from SIVE_LINEASM " +
          "where CD_TSIVE = ? and  CD_MODELO = ? order by  CD_MODELO, NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, TSIVE);
      st.setString(2, dataSalidaResp.getCodModelo().trim());

    }
    else {
      sQuery = " select " +
          "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
          "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
          "from SIVE_MODELO a, SIVE_LINEASM b  " +
          "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
          "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
          "and (a.CD_ENFCIE = ?) " +
          "and ( " +
          "(a.CD_CA = ? and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 = ?)  " +
          ") " +
          ") " +
          "order by b.CD_MODELO, b.NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, TSIVE);
      st.setString(2, "S");
      st.setString(3, datosEntrada.getCodEnfermedad().trim());
      st.setString(4, datosEntrada.getComunidad());
      st.setString(5, datosEntrada.getNivelUNO());
      st.setString(6, datosEntrada.getNivelDOS());
    } //else de cne

    rs = st.executeQuery();

    // extrae el registro encontrado y lo carga en listaDataLineasM
    while (rs.next()) {
      des = rs.getString("DS_TEXTO");
      desL = rs.getString("DSL_TEXTO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
          && (desL != null)) {
        sDS_TEXTO = desL;
      }
      else {
        sDS_TEXTO = des;

      }
      sCD_MODELO = rs.getString("CD_MODELO");
      iNM_LIN = new Integer(rs.getInt("NM_LIN"));
      sNM_LIN = iNM_LIN.toString();
      sCD_TLINEA = rs.getString("CD_TLINEA");

      if (dataSalidaResp.getNivel().trim().equals("N1")) {
        sCA = dataSalidaResp.getCA();
        sN1 = dataSalidaResp.getN1();
        sN2 = dataSalidaResp.getN2();
      }
      else {
        sCA = datosEntrada.getComunidad();
        sN1 = datosEntrada.getNivelUNO();
        sN2 = datosEntrada.getNivelDOS();
      }

      // a�ade un nodo
      listaDataLineasM.addElement(new DataLineasM
                                  (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                   sCA, sN1, sN2));
    } //while
    rs.close();
    rs = null;
    st.close();
    st = null;

  }
} //fin de la clase
