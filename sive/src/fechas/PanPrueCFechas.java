package fechas;

import java.awt.Button;
import java.awt.Cursor;
import java.awt.event.ActionEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;

/**
 * Panel a trav�s del que se realiza el volcado de existencias
 * de un almac�n
 * �nicamente incluye dos campos de texto, a�o y fichero, y un bot�n de aceptar
 * @autor ARS
 * @version 1.0
 */

public class PanPrueCFechas
    extends CPanel
    implements CInicializar { //CPanel

  CFecha fecha1 = new CFecha("N");
  CFecha1 fecha2 = new CFecha1("N");
  XYLayout xYLayout1 = new XYLayout();
  Button button1 = new Button();

  // constructor del panel PanMant
  public PanPrueCFechas(CApp a) {
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel PanMant
  public void jbInit() throws Exception {

    xYLayout1.setWidth(98);
    xYLayout1.setHeight(136);
    button1.setLabel("Pinchar");
    button1.addActionListener(new PanPrueCFechas_button1_actionAdapter(this));

    this.setLayout(xYLayout1);

    this.add(fecha1, new XYConstraints(15, 24, 90, 24));
    this.add(fecha2, new XYConstraints(115, 24, 90, 24));
    this.add(button1, new XYConstraints(14, 81, 59, 27));

  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  void button1_actionPerformed(ActionEvent e) {
    fecha1.ValidarFecha();
  }

} // DE toda la clase

class PanPrueCFechas_button1_actionAdapter
    implements java.awt.event.ActionListener {
  PanPrueCFechas adaptee;

  PanPrueCFechas_button1_actionAdapter(PanPrueCFechas adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.button1_actionPerformed(e);
  }
}
