/*
 * Autor        Fecha               Accion
 *  JRM         11/07/2000          La escribe
 */
package informes;

import java.io.Serializable;

/**
 * Paquete de datos con cada uno de los registros de la consulta.
 * Ser� el paquete de datos gen�rico para los informes. Tiene 10
 * atributos publicos de tipo string, suponiendo que 10 es el n�mero
 * m�ximo de columnas que va a tener un informe.
 *
 * @author  JRM
 * @version 11/07/2000
 */
public class DataInforme
    implements Serializable {
  /**
   * Columna 1 del informe
   */
  public String C1;
  /**
   * Columna 2 del informe
   */
  public String C2;
  /**
   * Columna 3 del informe
   */
  public String C3;
  /**
   * Columna 4 del informe
   */
  public String C4;
  /**
   * Columna 5 del informe
   */
  public String C5;
  /**
   * Columna 6 del informe
   */
  public String C6;
  /**
   * Columna 7 del informe
   */
  public String C7;
  /**
   * Columna 8 del informe
   */
  public String C8;
  /**
   * Columna 9 del informe
   */
  public String C9;
  /**
   * Columna 10 del informe
   */
  public String C10;

  /**
   * Inicializa 10 columnas
   */
  public DataInforme(String C1,
                     String C2,
                     String C3,
                     String C4,
                     String C5,
                     String C6,
                     String C7,
                     String C8,
                     String C9,
                     String C10
                     ) {
    this.C1 = C1;
    this.C2 = C2;
    this.C3 = C3;
    this.C4 = C4;
    this.C5 = C5;
    this.C6 = C6;
    this.C7 = C7;
    this.C8 = C8;
    this.C9 = C9;
    this.C10 = C10;
  }

  /**
   * Inicializa 9 columnas
   */
  public DataInforme(String C1,
                     String C2,
                     String C3,
                     String C4,
                     String C5,
                     String C6,
                     String C7,
                     String C8,
                     String C9
                     ) {
    this.C1 = C1;
    this.C2 = C2;
    this.C3 = C3;
    this.C4 = C4;
    this.C5 = C5;
    this.C6 = C6;
    this.C7 = C7;
    this.C8 = C8;
    this.C9 = C9;
  }

  /**
   * Inicializa 8 columnas
   */
  public DataInforme(String C1,
                     String C2,
                     String C3,
                     String C4,
                     String C5,
                     String C6,
                     String C7,
                     String C8
                     ) {
    this.C1 = C1;
    this.C2 = C2;
    this.C3 = C3;
    this.C4 = C4;
    this.C5 = C5;
    this.C6 = C6;
    this.C7 = C7;
    this.C8 = C8;
  }

  /**
   * Inicializa 7 columnas
   */
  public DataInforme(String C1,
                     String C2,
                     String C3,
                     String C4,
                     String C5,
                     String C6,
                     String C7
                     ) {
    this.C1 = C1;
    this.C2 = C2;
    this.C3 = C3;
    this.C4 = C4;
    this.C5 = C5;
    this.C6 = C6;
    this.C7 = C7;
  }

  /**
   * Inicializa 6 columnas
   */
  public DataInforme(String C1,
                     String C2,
                     String C3,
                     String C4,
                     String C5,
                     String C6
                     ) {
    this.C1 = C1;
    this.C2 = C2;
    this.C3 = C3;
    this.C4 = C4;
    this.C5 = C5;
    this.C6 = C6;
  }

  /**
   * Inicializa 5 columnas
   */
  public DataInforme(String C1,
                     String C2,
                     String C3,
                     String C4,
                     String C5
                     ) {
    this.C1 = C1;
    this.C2 = C2;
    this.C3 = C3;
    this.C4 = C4;
    this.C5 = C5;
  }

  /**
   * Inicializa 4 columnas
   */
  public DataInforme(String C1,
                     String C2,
                     String C3,
                     String C4
                     ) {
    this.C1 = C1;
    this.C2 = C2;
    this.C3 = C3;
    this.C4 = C4;
  }

  /**
   * Inicializa 3 columnas
   */
  public DataInforme(String C1,
                     String C2,
                     String C3
                     ) {
    this.C1 = C1;
    this.C2 = C2;
    this.C3 = C3;
  }

  /**
   * Inicializa 2 columnas
   */
  public DataInforme(String C1,
                     String C2
                     ) {
    this.C1 = C1;
    this.C2 = C2;
  }

  /**
   * Inicializa 1 columna
   */
  public DataInforme(String C1) {
    this.C1 = C1;
  }

} // DataC1
