package informes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

/**
 * Est� clase ser� utilizada para proporcionar los datos que ser�n utilizados
 * en los informes de la aplicaci�n.
 * Recibe un CLista cuyo primer elemento es la select que se quiere ejecutar y
 * cuyo segundo elemento es el n�mero de campos que tiene dicha select.
 * El resultado ser� un CLista donde el primer elemento es un Vector de
 * DataInforme. Ese vector contiene todas las filas de la consulta.
 * @author  JRM
 * @version 12/07/2000
 */
public class SrvInformes
    extends DBServlet {

  /**
   * trabajo del servlet. opmode se conserva por tradici�n.
   */
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    // Variables para realizar la consulta
    String strSelect = null;
    Integer numCampos = null;
    DataInforme dato = null;

    // objetos de datos
    CLista dataLista = new CLista();
    Vector data = new Vector();

    try {
      // establece la conexi�n con la base de datos
      con = openConnection();
      con.setAutoCommit(true);

      // Nos quedamos con la consulta.
      strSelect = (String) param.elementAt(0);
      // Nos quedamos con el n�mero de campos de la consulta.
      numCampos = (Integer) param.elementAt(1);
      // Preparamos y ejecutamos la consulta
      st = con.prepareStatement(strSelect);
      rs = st.executeQuery();
      while (rs.next()) {
        // Por cada uno de los registros vamos creando un DataInforme en el que
        // se asignan los valores de las columnas.
        // Seg�n el n�mero de par�metros de la consulta hay que llamar a un
        // constructor o a otro. Ya s� que esta no es la mejor soluci�n pero
        // en este caso no se pod�a optar por resolverlo de forma din�mica
        // debido al funcionamiento del RegisterTable de la herramienta de
        // de informes.
        switch (numCampos.intValue()) {
          case 1:
            dato = new DataInforme(rs.getString(1));
            break;
          case 2:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2));
            break;
          case 3:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2),
                                   rs.getString(3));
            break;
          case 4:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2),
                                   rs.getString(3),
                                   rs.getString(4));
            break;
          case 5:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2),
                                   rs.getString(3),
                                   rs.getString(4),
                                   rs.getString(5));
            break;
          case 6:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2),
                                   rs.getString(3),
                                   rs.getString(4),
                                   rs.getString(5),
                                   rs.getString(6));
            break;
          case 7:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2),
                                   rs.getString(3),
                                   rs.getString(4),
                                   rs.getString(5),
                                   rs.getString(6),
                                   rs.getString(7));
            break;
          case 8:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2),
                                   rs.getString(3),
                                   rs.getString(4),
                                   rs.getString(5),
                                   rs.getString(6),
                                   rs.getString(7),
                                   rs.getString(8));
            break;
          case 9:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2),
                                   rs.getString(3),
                                   rs.getString(4),
                                   rs.getString(5),
                                   rs.getString(6),
                                   rs.getString(7),
                                   rs.getString(8),
                                   rs.getString(9));
            break;
          case 10:
            dato = new DataInforme(rs.getString(1),
                                   rs.getString(2),
                                   rs.getString(3),
                                   rs.getString(4),
                                   rs.getString(5),
                                   rs.getString(6),
                                   rs.getString(7),
                                   rs.getString(8),
                                   rs.getString(9),
                                   rs.getString(10));
            break;

        } // swicth (numCampos)
        data.addElement(dato);
      } // while (rs.next)

      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      throw exc;
    }
    // Cerramos la conexi�n
    closeConnection(con);
    // Devolvemos los datos

    dataLista.addElement(data);
    return dataLista;
  }
} // SrvInformes
