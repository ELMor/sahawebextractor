package sapp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class Funciones {

  public Funciones() {
  }

  /**
   * Consulta de la tabla SIVE_CASOS_CENT
   * Devuelve el NM_ORDEN incrementado para el resto de la clave dada
   */
  public static int SecOrden_Centinelas(Connection conec,
                                        PreparedStatement stat,
                                        ResultSet rset,
                                        String CD_ENFCIE,
                                        String CD_PCENTI,
                                        String CD_MEDCEN,
                                        String CD_ANOEPI,
                                        String CD_SEMEPI) throws SQLException {

    int iNM_ORDEN = 0;

    String sQuery = "SELECT "
        + " MAX(NM_ORDEN) "
        + " FROM SIVE_CASOS_CENT "
        + " WHERE CD_ENFCIE = ? AND  CD_PCENTI = ? AND "
        + " CD_MEDCEN = ? AND CD_ANOEPI = ? AND CD_SEMEPI = ?";

    stat = conec.prepareStatement(sQuery);
    stat.setString(1, CD_ENFCIE);
    stat.setString(2, CD_PCENTI);
    stat.setString(3, CD_MEDCEN);
    stat.setString(4, CD_ANOEPI);
    stat.setString(5, CD_SEMEPI);

    rset = stat.executeQuery();

    if (rset.next()) {
      iNM_ORDEN = rset.getInt(1) + 1;
    }
    rset.close();
    rset = null;
    stat.close();
    stat = null;

    return (iNM_ORDEN);
  }

  /**
   * Inserta/Actualiza en la tabla sive_sec_anual
   * Devuelve el Secuenciador ya incrementado
   */
  public static int SecAnual(Connection conec,
                             PreparedStatement stat,
                             ResultSet rset,
                             String sNombreColumAno,
                             String sValorAno,
                             String sNombreColum) throws SQLException {

    int iNM = 0;
//// System_out.println("Secuenciador anual");
    String sQuery = "SELECT * " +
        " FROM SIVE_SEC_ANUAL WHERE " + sNombreColumAno + " = ?";
//// System_out.println("Consulta="+sQuery);
    stat = conec.prepareStatement(sQuery);
    stat.setString(1, sValorAno);
    rset = stat.executeQuery();
//// System_out.println("SUPER�: Secuenciador anual");
    boolean bAnoExiste = false;
    if (rset.next()) {
      if (rset == null) {
        //insertar: todos a cero excepto el mio que va a 1
        bAnoExiste = false;
      }
      else {
        //update: solo al campo en cuestion
        bAnoExiste = true;
        iNM = rset.getInt(sNombreColum);
      }
    }

    if (bAnoExiste) {

      rset.close();
      rset = null;
      stat.close();
      stat = null;
//// System_out.println("Modificar secuenciador anual");
      sQuery = "UPDATE SIVE_SEC_ANUAL SET " + sNombreColum + " = ? " +
          " WHERE " + sNombreColumAno + " = ?";
//// System_out.println("Consulta="+sQuery);
      stat = conec.prepareStatement(sQuery);
      iNM++;
      stat.setInt(1, iNM);
      stat.setString(2, sValorAno);

      stat.executeUpdate();
      stat.close();
      stat = null;
//// System_out.println("SUPER� Modificar secuenciador anual");
    } //existe el a�o

    else if (!bAnoExiste) {
      //Insert de todo a 0 excepto de XXX a 1
      iNM = 1;
      int i = 0;
      ResultSetMetaData rsmd = rset.getMetaData(); //no lo cerre antes
      int numCols = rsmd.getColumnCount();
//// System_out.println("Insertar Secuenciador anual");
      sQuery = "INSERT INTO SIVE_SEC_ANUAL " +
          "( " + sNombreColumAno + ", " + sNombreColum;

      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColumAno) &&
            !rsmd.getColumnLabel(i).equals(sNombreColum)) {
          sQuery = sQuery + ", " + rsmd.getColumnLabel(i);
        }
      }
      sQuery = sQuery + ") ";
      sQuery = sQuery + " values (?, ?";

      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColumAno) &&
            !rsmd.getColumnLabel(i).equals(sNombreColum)) {
          sQuery = sQuery + ", " + "?";
        }
      }
      sQuery = sQuery + " ) ";

      int iValor = 1;
//// System_out.println("Consulta="+sQuery);
      stat = conec.prepareStatement(sQuery);
      stat.setString(iValor, sValorAno);
      iValor++;
      stat.setInt(iValor, 1);
      iValor++;
      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColumAno) &&
            !rsmd.getColumnLabel(i).equals(sNombreColum)) {
          stat.setInt(iValor, 0);
          iValor++;
        }
      }

      stat.executeUpdate();
      stat.close();
      stat = null;
//// System_out.println("SUPER�: Insertar Secuenciador anual");
    } //no existe el a�o

    return (iNM);
  }

  /**
   * Inserta/Actualiza en la tabla sive_sec_general
   * Devuelve el Secuenciador ya incrementado
   */
  public static int SecGeneral(Connection conec,
                               PreparedStatement stat,
                               ResultSet rset,
                               String sNombreColum) throws SQLException {

    int iNM = 0;

    String sQuery = "SELECT * " +
        " FROM SIVE_SEC_GENERAL ";

    stat = conec.prepareStatement(sQuery);
    rset = stat.executeQuery();

    boolean bExisteRegistro = false;
    if (rset.next()) {
      if (rset == null) {
        //insertar: todos a cero excepto el mio que va a 1
        bExisteRegistro = false;
      }
      else {
        //update: solo al campo en cuestion
        bExisteRegistro = true;
        iNM = rset.getInt(sNombreColum);
      }
    }

    if (bExisteRegistro) {

      rset.close();
      rset = null;
      stat.close();
      stat = null;
//// System_out.println("Actualizar registro...");
      sQuery = "UPDATE SIVE_SEC_GENERAL SET " + sNombreColum + " = ? ";
//// System_out.println("Consulta="+sQuery);
      stat = conec.prepareStatement(sQuery);
      iNM++;
      stat.setInt(1, iNM);

      stat.executeUpdate();
      stat.close();
      stat = null;
//// System_out.println("Sin problema al actualizar registro.");
    } //existe el registro

    else if (!bExisteRegistro) {
      //Insert de todo a 0 excepto de XXX a 1
      iNM = 1;
      int i = 0;
      ResultSetMetaData rsmd = rset.getMetaData(); //no lo cerre antes
      int numCols = rsmd.getColumnCount();

      sQuery = "INSERT INTO SIVE_SEC_GENERAL " +
          "( " + sNombreColum;

      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColum)) {
          sQuery = sQuery + ", " + rsmd.getColumnLabel(i);
        }
      }
      sQuery = sQuery + ") ";
      sQuery = sQuery + " values (?";

      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColum)) {
          sQuery = sQuery + ", " + "?";
        }
      }
      sQuery = sQuery + " ) ";

      int iValor = 1;
//// System_out.println("Consulta insertar registro="+sQuery);
      stat = conec.prepareStatement(sQuery);
      stat.setInt(iValor, 1);
      iValor++;
      for (i = 1; i <= numCols; i++) {
        if (!rsmd.getColumnLabel(i).equals(sNombreColum)) {
          if (rsmd.getColumnLabel(i).equals("NM_SECAGB")) {
            stat.setString(iValor, "0");
          }
          else {
            stat.setInt(iValor, 0);
          }
          iValor++;
        }
      }

      stat.executeUpdate();
      stat.close();
      stat = null;

    } //no existe el registro

    return (iNM);
  }

  /**
   *  ARG:
   *  Obtiene la clave de acceso a la BD de un usuario
   */
  public static String getPassword(Connection con,
                                   PreparedStatement st,
                                   ResultSet rset,
                                   String sUser) throws SQLException {
    String sPassword = "";

    // La select obtiene la password del usuario de la tabla "GAT_T002"
    // que tiene el sinonimo "Usuario"
    String sQuery = "Select PASSWORD from Usuario where COD_USUARIO = ?";

    st = con.prepareStatement(sQuery);
    st.setString(1, sUser);
    rset = st.executeQuery();

    if (rset.next()) {
      sPassword = rset.getString("PASSWORD");
    }

    rset.close();
    rset = null;
    st.close();
    st = null;

    return sPassword;
  }

}