/* Clase: DBServlet
* Autor: Varios
* Fecha Inicio: 1999
* Descripcion:
* Modificaciones:
* 17/01/2000 (LRG) A�adidos m�todos setJdbcEnvironmment y doDebug (para depuraci�n)
*
*
*
**/

/*
 *  Autor     Fecha             Modificaci�n
 *   JRM      25/04/01          Resuelve problema de concatenaci�n de cadenas
 *                              para Oracle y SQLServer
 */

package sapp;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;
import capp.*;
import java.util.*;
import java.math.*;
import jdbcpool.*;

public abstract class DBServlet extends HttpServlet {

  /**
  * Modo debug
  */
  protected boolean debugMode = true;

  //M�ximo tama�o de transmisi�n
  final public static int maxSIZE = 50;

  //Initialize global variables
  protected String driverName = "";
  protected String dbUrl = "";
  protected String userName = "";
  protected String userPwd = "";

  protected JDCConnectionDriver jdcdriver;
  protected Connection conOpen;

  // JRM: Este ser� el atributo que se utilizar� como concatenaci�n
  // Esta inicializado a + porque es el que utilizan casi todos los
  // gestores de bases de datos.
  // En el m�todo init() se inicializa a || si el gestor es ORACLE.
  protected String MAS = "+";

  /**
  * Establece las variables JDBC para testear el servlet en local
  *
  * @param driver driver
  * @param url url
  * @param uid usuario
  * @param pwd password
  */
  public void setJdbcEnvironment(String  driver, String url, String uid, String pwd) {
    driverName = driver;
    dbUrl = url;
    userName = uid;
    userPwd = pwd;
  }

  /**
  * Banco de pruebas en local de la l�gica de negocio del servlet
  *
  * @param opmode modo de operaci�n del servelt
  * @param vParametros vector de par�metros recibidos desde el applet

  * NOTA: No se abre y cierra la conexi�n porque eso se hace en cada Servlet
  */
  public CLista doDebug(int opmode, CLista vParametros) {
    CLista v = new CLista();
    try {
      jdcdriver = new JDCConnectionDriver(driverName, dbUrl, userName, userPwd);

      v = doWork(opmode, vParametros);
    } catch(Exception e) {
      // error en modo debug
      e.printStackTrace();
    }
    return v;
  }


  /**
  * Traza en el log el mensaje leido
  *
  * @param msg Mensaje para trazar
  */
  public void trazaLog(Object msg) {
    if (debugMode)
      System.out.println( msg.toString() );
  }

  public void init(ServletConfig config) throws ServletException {

    // carga las constantes de acceso JDBC
    driverName = config.getInitParameter("drivername");
    dbUrl = config.getInitParameter("dbUrl");
    userName = config.getInitParameter("userName");
    userPwd = config.getInitParameter("userPwd");

    // JRM: Hay que inicializar a || el atributo MAS si el gestor es ORACLE
    if (driverName.substring(0,6).toUpperCase().equals("ORACLE"))
      MAS = "||";





    try {
      // carga el driver JDBC
      jdcdriver = new JDCConnectionDriver(driverName, dbUrl, userName, userPwd);
    } catch(Exception e) {
      // error al abrir la conexi�n con la base de datos
      e.printStackTrace();
      throw new ServletException("Error al conectar con la base de datos.");
    }
  }

  public void destroy() {
    try {
      jdcdriver.clearPool();
      jdcdriver = null;
    } catch(Exception e) {;}
    super.destroy();
  }

  // crea una conexi�n
  protected synchronized Connection openConnection() throws Exception {
    Connection con = jdcdriver.connect();
    conOpen = con; // se guarda una referencia para cerrarla si se produce una excepci�n
    return con;
  }

  // cierra una conexion
  protected synchronized void closeConnection(Connection con) {
    try {jdcdriver.disconnect(con);} catch (Exception e) {}
  }

  // funcionalidad del servlet
  protected abstract CLista doWork(int opmode, CLista param) throws Exception;

  // servicio
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    // par�metros
    CLista param = null;
    CLista data = null;

    // streams
    ObjectOutputStream out = null;
    ObjectInputStream in = null;

    // modo de operaci�n
    int opmode;

    // comunicaciones servlet-applet
    try {

      // lee el modo de operaci�n
      in = new ObjectInputStream(request.getInputStream());
      opmode = in.readInt();

      // lee los par�metros
      param = (CLista) in.readObject();

      // abre el stream de salida
      response.setContentType("application/octet-stream");
      out = new ObjectOutputStream((OutputStream) response.getOutputStream());
      out.flush();

      // obtiene los resultados
      conOpen = null;
      data = doWork(opmode, param);

      // envia resultados
      if (data != null)
        data.trimToSize();
      out.writeObject(data);
      out.flush();

      // cierra los streams
      out.close();

    // envia la excepci�n
    } catch (Exception e) {
      e.printStackTrace();
      out.writeObject(new Exception(e.getClass() + ": " + e.getMessage()));
      out.flush();
      out.close();

      // cierra los recursos
      if (conOpen != null)
        closeConnection(conOpen);
    }
  }
}

