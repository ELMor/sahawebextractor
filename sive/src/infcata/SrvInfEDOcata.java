
package infcata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

//import infcobsem.*;
// LORTAD
import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvInfEDOcata
    extends DBServlet {

  protected String query = "";
  protected String periodo = "";

  // informes
  final int erwCASOS_EDO = 1;
  final int erwTASAS_EDO = 2;

  //float tas = (float) 0;
  //float pob = (float) 0;
  //float cas = (float) 0;
  float tas = 0;
  float pob = 0;
  float cas = 0;

  // modificacion jlt
  // calculamos el total de las tasas dividiendo el total
  // de casos por la poblacion total
  float castotal = 0;

  // Casos de enfermedades con tratamiento especial
  // Nota: se est� empleando, a modo de prueba, la gripe, ya que la enfermedad
  // par�lisis fl�ccida a�n no ha sido dada de alta.
  //public static final String CD_PARALISIS = "357.0";
  public static final String CD_PARALISIS = "61";
  // 61 es el c�digo EDO, el c�digo CIE es 357.0

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;

    ResultSet rs = null;

    // control
    int i = 1;
    DataEDOcata Auxiliar = null;
    Param_C9_3_14 datoEnf = null;
    DataEDOcata datoSem = null;
    Vector totales = new Vector();
    int paso = 0;
    float add = 0;
    String anoActual = "";
    String semActual = "";
    String cadena = "";
    String cadenaaux = "";
    int elValor = 0;
    int totalRegistros = 0;

    // buffers
    String sCod;
    int iNum;
    DataEDOcata dat = null;
    Param_C9_3_14 datoAuxi = null;

    // objetos de datos
    CLista data = new CLista();
    Vector registros = new Vector();
    Vector registrosAux = new Vector();
    Vector vectorSemanas = new Vector();
    Vector vectorEnfer = new Vector();

    //Para recuperar descripci�n en idiomas
    String sDesProL = "";

    // Para calcular poblaciones;
    int iPoblacion = 0;
    Vector vectorPS = new Vector(); // Poblaci�n semanal
    Vector vectorPSEdad = new Vector();
    String strArea = null;
    String strDistrito = null;
    String strA�oAnterior = null;

    // Casos de enfermedades con tratamiento especial
    String strCD_ENFERMEDAD = null;
    final int TOPE_EDAD = 15;

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);
    con.setAutoCommit(true);

    dat = (DataEDOcata) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      // l�neas del modelo
      case erwCASOS_EDO:
      case erwTASAS_EDO:

        try {
          // Se recuperan estos dos datos
          if (opmode == erwTASAS_EDO) { // E
            strArea = dat.sCdArea;
            strDistrito = dat.sCdDistrito;
            strA�oAnterior = new String();
          }

          // rellenamos el vector de las semanas
          query = "select CD_ANOEPI, CD_SEMEPI from sive_semana_epi ";

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            periodo =
                "where CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
          }
          else {
            periodo = "where ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";

          }
          query += periodo;
          query += " order by CD_ANOEPI, CD_SEMEPI";

          st = con.prepareStatement(query);

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {

            // semana desde
            st.setString(1, dat.sSemDesde);
            // semana hasta
            st.setString(2, dat.sSemHasta);
            // a�o
            st.setString(3, dat.sAnoDesde);

          }
          else { // los a�os inicial y final son distintos
            // a�o inicial
            st.setString(1, dat.sAnoDesde);
            st.setString(3, dat.sAnoDesde);
            // semana inicial
            st.setString(2, dat.sSemDesde);
            // a�o final
            st.setString(4, dat.sAnoHasta);
            st.setString(5, dat.sAnoHasta);
            // semana final
            st.setString(6, dat.sSemHasta);
          }
          rs = st.executeQuery();

          paso = dat.numSemanas;

          while (rs.next()) {
            anoActual = rs.getString("CD_ANOEPI");
            semActual = rs.getString("CD_SEMEPI");

            if (paso == dat.numSemanas) {
              Auxiliar = new DataEDOcata(anoActual, semActual, anoActual,
                                         semActual, "", "", "",
                                         "", "", "", "", "", "", "", 0);

              if (paso == 0) {
                vectorSemanas.addElement(Auxiliar);
                Auxiliar = null;

                if (opmode == erwTASAS_EDO) { // E
                  if (strA�oAnterior.equals(anoActual)) {
                    vectorPS.addElement( (Integer) vectorPS.lastElement());
                    vectorPSEdad.addElement( (Integer) vectorPSEdad.lastElement());
                  }
                  else {
                    vectorPS.addElement(new Integer(getPoblacion(con, anoActual,
                        strArea, strDistrito, -1)));
                    vectorPSEdad.addElement(new Integer(getPoblacion(con,
                        anoActual, strArea, strDistrito, TOPE_EDAD)));
                  }
                  strA�oAnterior = new String(anoActual);
                }
              }
              else {
                paso--;
              }
            }
            else if (paso == 0) {
              Auxiliar.sAnoHasta = anoActual;
              Auxiliar.sSemHasta = semActual;
              paso = dat.numSemanas;

              if (opmode == erwTASAS_EDO) { // E
                if (Auxiliar.sAnoDesde.equals(anoActual)) {
                  if (strA�oAnterior.equals(anoActual)) {
                    vectorPS.addElement( (Integer) vectorPS.lastElement());
                    vectorPSEdad.addElement( (Integer) vectorPSEdad.lastElement());
                  }
                  else {
                    vectorPS.addElement(new Integer(getPoblacion(con, anoActual,
                        strArea, strDistrito, -1)));
                    vectorPSEdad.addElement(new Integer(getPoblacion(con,
                        anoActual, strArea, strDistrito, TOPE_EDAD)));
                  }
                }
                else {
                  vectorPS.addElement(new Integer(
                      (getPoblacion(con, anoActual, strArea, strDistrito, -1) +
                       getPoblacion(con, Auxiliar.sAnoDesde, strArea,
                                    strDistrito, -1)) / 2)
                                      );
                  vectorPSEdad.addElement(new Integer(
                      (getPoblacion(con, anoActual, strArea, strDistrito,
                                    TOPE_EDAD) +
                       getPoblacion(con, Auxiliar.sAnoDesde, strArea,
                                    strDistrito, TOPE_EDAD)) / 2)
                                          );
                }
                strA�oAnterior = new String(anoActual);
              }

              vectorSemanas.addElement(Auxiliar);
              Auxiliar = null;
            }
            else {
              paso--;
            }
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          // // System_out.println("Infcata: tama�o: " + String.valueOf(vectorPS.size()));
//************************************
           // vector enfermedades
          query = "select a.CD_ENFCIE,a.CD_TVIGI,b.DS_PROCESO ,b.DSL_PROCESO "
              + " from sive_enferedo a, sive_procesos b "
              + " where a.CD_ENFCIE = b.CD_ENFCIE";

          //query += " order by a.CD_ENFCIE";
          // modificacion jlt 24/10/2001 ordenamos por proceso
          query += " order by b.DS_PROCESO, a.CD_ENFCIE";

          st = con.prepareStatement(query);

          rs = st.executeQuery();

          vectorEnfer = new Vector();

          while (rs.next()) {
            datoAuxi = new Param_C9_3_14();
            datoAuxi.CD_ENFERMEDAD = rs.getString("CD_ENFCIE");
            datoAuxi.DS_ENFERMEDAD = rs.getString("DS_PROCESO");

            // obtiene la descripcion auxiliar en funci�n del idioma
            if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
              sDesProL = rs.getString("DSL_PROCESO");
              if (sDesProL != null) {
                datoAuxi.DS_ENFERMEDAD = sDesProL;
              }
            }
            datoAuxi.CD_TVIGI = rs.getString("CD_TVIGI");
            vectorEnfer.addElement(datoAuxi);
            datoAuxi = null;
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          // ahora generamos las combinaciones

          for (int xr = 0; xr < vectorEnfer.size(); xr++) {
            datoEnf = new Param_C9_3_14();
            datoEnf = (Param_C9_3_14) vectorEnfer.elementAt(xr);
            paso = 0;

            if (opmode == erwTASAS_EDO) { // E
              strCD_ENFERMEDAD = new String(getCDEnfermedad(con,
                  datoEnf.CD_ENFERMEDAD));
              if (strCD_ENFERMEDAD.equals(CD_PARALISIS)) {
                datoEnf.DS_ENFERMEDAD = new String("* " + datoEnf.DS_ENFERMEDAD);
              }
            }

            for (int j = 0; j < vectorSemanas.size(); j++) {
              datoSem = new DataEDOcata();
              datoSem = (DataEDOcata) vectorSemanas.elementAt(j);

              if (datoEnf.CD_TVIGI.equals("I")) {
                query = "select count(NM_EDO) from sive_edoind ";
              }
              else {
                query = "select sum(NM_CASOS) from sive_edonum ";
              }

              query += "where CD_ENFCIE = ? ";

              if (datoSem.sAnoDesde.equals(datoSem.sAnoHasta)) {
                periodo =
                    "and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
              }
              else {
                periodo = "and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                    + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                    + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";

              }
              query += periodo;

              if (datoEnf.CD_TVIGI.equals("I")) {
                if (dat.sCdArea.length() > 0) {
                  query += " and CD_NIVEL_1='" + dat.sCdArea + "'";

                  if (dat.sCdDistrito.length() > 0) {
                    query += " and CD_NIVEL_2='" + dat.sCdDistrito + "'";
                  }
                }
              }
              else {
                if (dat.sCdArea.length() > 0) {
                  query = query.substring(0, query.length() - 1); // linea a�adida para quitar el parentesis
                  query +=
                      "and CD_E_NOTIF in (select CD_E_NOTIF from sive_e_notif";
                  query += " where CD_NIVEL_1='" + dat.sCdArea + "')";
                  if (dat.sCdDistrito.length() > 0) {
                    query = query.substring(0, query.length() - 1); // linea a�adida para quitar el parentesis
                    query += " and CD_NIVEL_2='" + dat.sCdDistrito + "')";
                  }
                }
              }

              st = con.prepareStatement(query);
              // c�digo enfermedad
              st.setString(1, datoEnf.CD_ENFERMEDAD);
              if (datoEnf.CD_TVIGI.equals("I")) {
                registroConsultas.insertarParametro(datoEnf.CD_ENFERMEDAD);
              }

              if (datoSem.sAnoDesde.equals(datoSem.sAnoHasta)) {
                // semana desde
                st.setString(2, datoSem.sSemDesde);
                // semana hasta
                st.setString(3, datoSem.sSemHasta);
                // a�o
                st.setString(4, datoSem.sAnoDesde);
                if (datoEnf.CD_TVIGI.equals("I")) {
                  registroConsultas.insertarParametro(datoSem.sSemDesde);
                  registroConsultas.insertarParametro(datoSem.sSemHasta);
                  registroConsultas.insertarParametro(datoSem.sAnoDesde);
                }
              }
              else { // los a�os inicial y final son distintos
                // a�o inicial
                st.setString(2, datoSem.sAnoDesde);
                st.setString(4, datoSem.sAnoDesde);
                // semana inicial
                st.setString(3, datoSem.sSemDesde);
                // a�o final
                st.setString(5, datoSem.sAnoHasta);
                st.setString(6, datoSem.sAnoHasta);
                // semana final
                st.setString(7, datoSem.sSemHasta);
                if (datoEnf.CD_TVIGI.equals("I")) {
                  registroConsultas.insertarParametro(datoSem.sAnoDesde);
                  registroConsultas.insertarParametro(datoSem.sAnoDesde);
                  registroConsultas.insertarParametro(datoSem.sSemDesde);
                  registroConsultas.insertarParametro(datoSem.sAnoHasta);
                  registroConsultas.insertarParametro(datoSem.sAnoHasta);
                  registroConsultas.insertarParametro(datoSem.sSemHasta);
                }
              }

              // Registramos la consulta si es individual.
              if (datoEnf.CD_TVIGI.equals("I")) {
                registroConsultas.registrar("SIVE_EDOIND",
                                            query,
                                            "CD_ENFERMO",
                                            "",
                                            "SrvInfEDOCata",
                                            true);
              }
              rs = st.executeQuery();

              if (rs.next()) {
                elValor = rs.getInt(1);
              }

              rs.close();
              st.close();
              rs = null;
              st = null;
              datoSem = null;

              if (opmode == erwTASAS_EDO) { // E
                if (strCD_ENFERMEDAD.equals(CD_PARALISIS)) {
                  //elValor = (elValor * 100000) / ((Integer) vectorPSEdad.elementAt(j)).intValue();
                  //cas = elValor* 100000;
                  cas = elValor;
                  castotal += cas;
                  pob = ( (Integer) vectorPSEdad.elementAt(j)).intValue();
                  tas = cas * 100000 / pob;

                }
                else {
                  //elValor = (elValor * 100000) / ((Integer) vectorPS.elementAt(j)).intValue();

                  //cas = elValor* 100000;
                  cas = elValor;
                  castotal += cas;
                  pob = ( (Integer) vectorPS.elementAt(j)).intValue();
                  //tas = cas/pob;
                  tas = cas * 100000 / pob;

                }

                cadena = Float.toString(tas);
//                    cadena = "78.99";

                /////////////  modificacion jlt
                String e = cadena.substring(0, cadena.indexOf('.'));
                String d = cadena.substring(cadena.indexOf('.') + 1,
                                            cadena.length());
                cadena = e + ',' + d;
                cadenaaux = e + '.' + d;
                if (d.length() >= 2) {
                  String d1 = d.substring(0, 1);
                  String d2 = d.substring(1, 2);
                  //String d3 = d.substring(2,3);
                  //Integer i3 = new Integer(d3);
                  Integer i2 = new Integer(d2);
                  Integer i1 = new Integer(d1);
                  Integer en = new Integer(e);
                  int suma = 0;
                  //if (i3.intValue() >= 5) {
                  if (i2.intValue() >= 5) {

                    //if(i2.intValue() == 9) {
                    if (i1.intValue() == 9) {
                      //suma = i1.intValue() + 1;
                      //d = (new Integer(suma)).toString() + (new Integer(0)).toString();
                      d = (new Integer(0)).toString();
                      suma = en.intValue() + 1;
                      e = (new Integer(suma)).toString();

                    }
                    else {

                      //suma = i2.intValue() + 1;
                      suma = i1.intValue() + 1;
                      //d = d1 + (new Integer(suma)).toString();
                      d = (new Integer(suma)).toString();
                    }

                  }
                  else {
                    //d = d1 + d2;
                    d = d1;
                  }

                  cadena = e + "," + d;
                  cadenaaux = e + "." + d;

                }
                //////////////

              }
              else {

                cadena = Integer.toString(elValor);
                cadenaaux = cadena;

              }

              float valor2 = 0;
              Float valor = new Float( (String) cadenaaux);
              valor2 = valor.floatValue();

//              valor = (new Float(cadena)).floatValue();

              switch (j) {
                case 0:
                  datoEnf.C1 = cadena;
                  //paso += elValor;
                  add += valor2;
                  break;
                case 1:
                  datoEnf.C2 = cadena;
                  add += valor2;
                  break;
                case 2:
                  datoEnf.C3 = cadena;
                  add += valor2;
                  break;
                case 3:
                  datoEnf.C4 = cadena;
                  add += valor2;
                  break;
                case 4:
                  datoEnf.C5 = cadena;
                  add += valor2;
                  break;
                case 5:
                  datoEnf.C6 = cadena;
                  add += valor2;
                  break;
                case 6:
                  datoEnf.C7 = cadena;
                  add += valor2;
                  break;
                case 7:
                  datoEnf.C8 = cadena;
                  add += valor2;
                  break;
                case 8:
                  datoEnf.C9 = cadena;
                  add += valor2;
                  break;
                case 9:
                  datoEnf.C10 = cadena;
                  add += valor2;
                  break;
                case 10:
                  datoEnf.C11 = cadena;
                  add += valor2;
                  break;
                case 11:
                  datoEnf.C12 = cadena;
                  add += valor2;
                  break;
                case 12:
                  datoEnf.C13 = cadena;
                  add += valor2;
                  break;
              }

            }

            if (opmode == erwTASAS_EDO) {

              //datoEnf.CTOTAL = Integer.toString(paso);
              /////////////  modificacion jlt
              //String addtot = Float.toString(add);
              // modificacion jlt 12/12/2001
              // el total de las tasas es a partir del
              //n� total de casos
              String addtot = Float.toString(castotal * 100000 / pob);
              String e = addtot.substring(0, addtot.indexOf('.'));
              String d = addtot.substring(addtot.indexOf('.') + 1,
                                          addtot.length());
              addtot = e + ',' + d;
              //cadenaaux = e + '.' + d;

              if (d.length() >= 3) {
                String d1 = d.substring(0, 1);
                String d2 = d.substring(1, 2);
                //String d3 = d.substring(2,3);
                //Integer i3 = new Integer(d3);
                Integer i2 = new Integer(d2);
                Integer i1 = new Integer(d1);
                Integer en = new Integer(e);
                int suma = 0;
                if (i2.intValue() >= 5) {
                  if (i1.intValue() == 9) {
                    //suma = i1.intValue() + 1;
                    //d = (new Integer(suma)).toString() + (new Integer(0)).toString();
                    d = (new Integer(0)).toString();
                    suma = en.intValue() + 1;
                    e = (new Integer(suma)).toString();
                  }
                  else {
                    suma = i1.intValue() + 1;
                    d = (new Integer(suma)).toString();
                  }

                }
                else {
                  d = d1;
                }

                addtot = e + "," + d;
                //cadenaaux = e + "." + d;
              }

              //datoEnf.CTOTAL = Float.toString(add);
              datoEnf.CTOTAL = addtot;

            }
            else {
              // no ponemos coma en el total
              int t = (new Float(add)).intValue();
              datoEnf.CTOTAL = (new Integer(t)).toString();

            }

            //if (paso != 0)
            if (add != 0) {
              totales.addElement(datoEnf);
            }
            datoEnf = null;
            add = 0;
            // modificacion jlt
            castotal = 0;
          }

//************************************
        }
        catch (Exception exp) {
          // System_out.println("Infcata: " + exp.getMessage());
        }

        break;

//*************************************************************************
//*************************************************************************
//*************************************************************************

           /*      case erwCASOS_EDO:
                 try{
                     // rellenamos el vector de las semanas
                query = "select CD_ANOEPI, CD_SEMEPI from sive_semana_epi ";
                     if (dat.sAnoDesde.equals(dat.sAnoHasta))
                periodo = "where CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
                     else
                periodo =  "where ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                                 + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                                 + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";
                     query += periodo;
                     query += " order by CD_ANOEPI, CD_SEMEPI";
                     st = con.prepareStatement(query);
                     if (dat.sAnoDesde.equals(dat.sAnoHasta)){
                       // semana desde
                       st.setString(1, dat.sSemDesde);
                       // semana hasta
                       st.setString(2, dat.sSemHasta);
                       // a�o
                       st.setString(3, dat.sAnoDesde);
                     }else{ // los a�os inicial y final son distintos
                       // a�o inicial
                       st.setString(1, dat.sAnoDesde);
                       st.setString(3, dat.sAnoDesde);
                       // semana inicial
                       st.setString(2, dat.sSemDesde);
                       // a�o final
                       st.setString(4, dat.sAnoHasta);
                       st.setString(5, dat.sAnoHasta);
                       // semana final
                       st.setString(6, dat.sSemHasta);
                     }
                     rs = st.executeQuery();
                     paso = dat.numSemanas;
                     while(rs.next()){
                       anoActual = rs.getString("CD_ANOEPI");
                       semActual = rs.getString("CD_SEMEPI");
                       if (paso == dat.numSemanas){
                Auxiliar = new DataEDOcata(anoActual,semActual,anoActual,semActual,"","","",
                                     "","","","","","","",0);
                         if (paso == 0){
                           vectorSemanas.addElement(Auxiliar);
                           Auxiliar = null;
                         }else{
                           paso --;
                         }
                       }else if (paso == 0){
                         Auxiliar.sAnoHasta = anoActual;
                         Auxiliar.sSemHasta = semActual;
                         paso = dat.numSemanas;
                         vectorSemanas.addElement(Auxiliar);
                         Auxiliar = null;
                       }else{
                         paso --;
                       }
                     }
                     rs.close();
                     st.close();
                     rs = null;
                     st = null;
                     // vector enfermedades
                     query = "select a.CD_ENFCIE,a.CD_TVIGI,b.DS_PROCESO "
                         + " from sive_enferedo a, sive_procesos b "
                         + " where a.CD_ENFCIE = b.CD_ENFCIE";
                     query += " order by a.CD_ENFCIE";
                     st = con.prepareStatement(query);
                     rs = st.executeQuery();
                     vectorEnfer = new Vector();
                     while (rs.next()){
                       datoAuxi = new Param_C9_3_14();
                       datoAuxi.CD_ENFERMEDAD = rs.getString("CD_ENFCIE");
                       datoAuxi.DS_ENFERMEDAD = rs.getString("DS_PROCESO");
                       datoAuxi.CD_TVIGI = rs.getString("CD_TVIGI");
                       vectorEnfer.addElement(datoAuxi);
                       datoAuxi = null;
                     }
                     rs.close();
                     st.close();
                     rs = null;
                     st = null;
                     // ahora generamos las combinaciones
                     for (int xr = 0;xr < vectorEnfer.size();xr++){
                       datoEnf = new Param_C9_3_14();
                       datoEnf = (Param_C9_3_14) vectorEnfer.elementAt(xr);
                       paso = 0;
                       for(int j = 0;j < vectorSemanas.size();j++){
                         datoSem = new DataEDOcata();
                         datoSem = (DataEDOcata) vectorSemanas.elementAt(j);
                         if (datoEnf.CD_TVIGI.equals("I")){
                           query = "select count(NM_EDO) from sive_edoind ";
                         }else{
                           query = "select sum(NM_CASOS) from sive_edonum ";
                         }
                         query += "where CD_ENFCIE = ? ";
                         if (datoSem.sAnoDesde.equals(datoSem.sAnoHasta))
                periodo = "and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
                         else
                periodo =  "and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                                 + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                                 + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";
                         query += periodo;
                         if (datoEnf.CD_TVIGI.equals("I")){
                           if (dat.sCdArea.length() > 0){
                             query += " and CD_NIVEL_1='" + dat.sCdArea + "'";
                             if (dat.sCdDistrito.length() > 0){
                query += " and CD_NIVEL_2='" + dat.sCdDistrito + "'";
                             }
                           }
                         }else{
                           if (dat.sCdArea.length() > 0){
                query += "and CD_E_NOTIF in (select CD_E_NOTIF from sive_e_notif";
                query += " where CD_NIVEL_1='" + dat.sCdArea + "')";
                             if (dat.sCdDistrito.length() > 0){
                query += " and CD_NIVEL_2='" + dat.sCdDistrito + "')";
                             }
                           }
                         }
                         st = con.prepareStatement(query);
                         // c�digo enfermedad
                         st.setString(1, datoEnf.CD_ENFERMEDAD);
                         if (datoSem.sAnoDesde.equals(datoSem.sAnoHasta)){
                           // semana desde
                           st.setString(2, datoSem.sSemDesde);
                           // semana hasta
                           st.setString(3, datoSem.sSemHasta);
                           // a�o
                           st.setString(4, datoSem.sAnoDesde);
                         }else{ // los a�os inicial y final son distintos
                           // a�o inicial
                           st.setString(2, datoSem.sAnoDesde);
                           st.setString(4, datoSem.sAnoDesde);
                           // semana inicial
                           st.setString(3, datoSem.sSemDesde);
                           // a�o final
                           st.setString(5, datoSem.sAnoHasta);
                           st.setString(6, datoSem.sAnoHasta);
                           // semana final
                           st.setString(7, datoSem.sSemHasta);
                         }
                         rs = st.executeQuery();
                         if (rs.next()){
                           elValor = rs.getInt(1);
                         }
                         rs.close();
                         st.close();
                         rs = null;
                         st = null;
                         datoSem = null;
                         cadena = Integer.toString(elValor);
                         switch (j){
                           case 0:
                             datoEnf.C1 = cadena;
                             paso += elValor;
                           break;
                           case 1:
                             datoEnf.C2 = cadena;
                             paso += elValor;
                           break;
                           case 2:
                             datoEnf.C3 = cadena;
                             paso += elValor;
                           break;
                           case 3:
                             datoEnf.C4 = cadena;
                             paso += elValor;
                           break;
                           case 4:
                             datoEnf.C5 = cadena;
                             paso += elValor;
                           break;
                           case 5:
                             datoEnf.C6 = cadena;
                             paso += elValor;
                           break;
                           case 6:
                             datoEnf.C7 = cadena;
                             paso += elValor;
                           break;
                           case 7:
                             datoEnf.C8 = cadena;
                             paso += elValor;
                           break;
                           case 8:
                             datoEnf.C9 = cadena;
                             paso += elValor;
                           break;
                           case 9:
                             datoEnf.C10 = cadena;
                             paso += elValor;
                           break;
                           case 10:
                             datoEnf.C11 = cadena;
                             paso += elValor;
                           break;
                           case 11:
                             datoEnf.C12 = cadena;
                             paso += elValor;
                           break;
                           case 12:
                             datoEnf.C13 = cadena;
                             paso += elValor;
                           break;
                         }
                       }
                       datoEnf.CTOTAL = Integer.toString(paso);
                       if (paso != 0)
                         totales.addElement(datoEnf);
                       datoEnf = null;
                     }
                     //totales = new Vector();
                     //totales.addElement(datoAuxi);
                 }catch(Exception er){
                   er.printStackTrace();
                 }
      break;*/
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (totales.size() > 0) {
      vectorSemanas.trimToSize();
      data.addElement(vectorSemanas);
      totales.trimToSize();
      data.addElement(totales);
      // registros totales
      data.trimToSize();

    }
    else {
      data = null;

    }

    return data;
  }

  //********************************************************************
   // Para calcular la poblaci�n
   //********************************************************************

    public static int getPoblacion(Connection conexion, String aActual,
                                   String nUno, String nDos, int edad) throws
        Exception {

      int iValor = 0;

      boolean bEdad = false;

      boolean bNivel1 = false;
      boolean bNivel2 = false;

      PreparedStatement st = null;
      ResultSet rs = null;
      String strQuery = null;

      try {
        bNivel1 = nUno != null && !nUno.equals("");
        bNivel2 = nDos != null && !nDos.equals("");

        bEdad = edad != -1;

        //strQuery = " select sum(NM_POBLACION) from SIVE_POBLACION_NS where " +
        //           " CD_ANOEPI = ? ";

        // modificacion jlt 17/09/01
        strQuery = " select sum(NM_POBLACION) from SIVE_POBLACION_NS where " +
            " CD_ANOEPI IN (SELECT MAX(CD_ANOEPI) FROM  " +
            " SIVE_POBLACION_NS WHERE CD_ANOEPI <= ? ) ";

        if (bNivel1) {
          strQuery = new String(strQuery + " and CD_NIVEL_1 = ? ");
          if (bNivel2) {
            strQuery = new String(strQuery + " and CD_NIVEL_2 = ? ");
          }
        }

        if (bEdad) {
          strQuery = new String(strQuery + " and NM_EDAD < ? ");
        }

        // // System_out.println(strQuery);

        st = conexion.prepareStatement(strQuery);

        st.setString(1, aActual);
        //st.setInt(2, iTopeEdad);

        if (bNivel1) {
          st.setString(2, nUno);
          if (bNivel2) {
            st.setString(3, nDos);
            if (bEdad) {
              st.setInt(4, edad);
            }
          }
          else {
            if (bEdad) {
              st.setInt(3, edad);
            }
          }
        }
        else {
          if (bEdad) {
            st.setInt(2, edad);
          }
        }

        rs = st.executeQuery();

        if (rs.next()) {
          iValor = rs.getInt(1);
        }

        rs.close();
        st.close();
        rs = null;
        st = null;

      }
      catch (Exception exp) {
        // System_out.println("SrvInfEDOcata " + exp.getMessage());
      }

      return (iValor == 0 ? 100000 : iValor);

    }

  public static String getCDEnfermedad(Connection conexion, String CD_ENFCIE) throws
      Exception {

    String strValor = null;

    PreparedStatement st = null;
    ResultSet rs = null;
    String strQuery = null;

    try {
      strQuery = " select CD_ENFERE from SIVE_ENFEREDO where " +
          " CD_ENFCIE = ? ";

      // // System_out.println(CD_ENFEREDO);

      st = conexion.prepareStatement(strQuery);

      st.setString(1, CD_ENFCIE);

      rs = st.executeQuery();

      if (rs.next()) {
        strValor = rs.getString(1);
      }

      // // System_out.println(CD_ENFCIE + " " + strValor);

      rs.close();
      st.close();
      rs = null;
      st = null;

    }
    catch (Exception exp) {
      // System_out.println("SrvInfEDOcata " + exp.getMessage());
    }

    return strValor;
  }
}
