
package infcata;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosEDOcata
    extends CApp {

  ResourceBundle res;

  public CasosEDOcata() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("infcata.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.text"));
    CApp a = (CApp)this;

    VerPanel(res.getString("msg2.Text"), new Pan_EDOcata(a), true);
    VerPanel(res.getString("msg2.Text"));
  }

}
