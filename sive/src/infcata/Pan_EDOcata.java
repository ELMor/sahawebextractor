package infcata;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import cn.DataCN;
import nivel1.DataNivel1;
import notutil.UtilEDO;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import zbs.DataZBS2;

public class Pan_EDOcata
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();
  protected StubSrvBD stubNivAsis = null;

  public DataEDOcata paramC2;
  protected int modoOperacion = modoNORMAL;

  protected Pan_InfEDOcata informe;
  CLista datan = null;
  CLista listaNivAsis = null;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLET_NIV2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
//  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETcn = "servlet/SrvCN";

  final String strSERVLETEquipo = "servlet/SrvEqNotCen";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;
  public static final int servletSELECCION_NIVASIS_X_CODIGO = 7;
  public static final int servletOBTENER_NIVASIS_X_CODIGO = 8;
  public static final int servletSELECCION_NIVASIS_X_DESCRIPCION = 9;
  public static final int servletOBTENER_NIVASIS_X_DESCRIPCION = 10;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  //focusAdapter txtFocusAdapter = new focusAdapter(this);
  //textAdapter txtTextAdapter = new textAdapter(this);
  //actionListener btnActionListener = new actionListener(this);
  Label lblArea = new Label();
  CCampoCodigo txtArea = new CCampoCodigo(); /*E*/
  ButtonControl btnArea = new ButtonControl();
  TextField txtDesArea = new TextField();
  CCampoCodigo txtDistrito = new CCampoCodigo(); /*E*/
  ButtonControl btnDistrito = new ButtonControl();
  TextField txtDesDistrito = new TextField();
  Label lblDistrito = new Label();
  Label lblSemanas = new Label();
  TextField txtSemanas = new TextField();
  Label label1 = new Label();
  Label lblSeleccion = new Label();
  CheckboxGroup checkboxGroup2 = new CheckboxGroup();
  Checkbox chkCasos = new Checkbox();
  Checkbox chkTasas = new Checkbox();

  public Pan_EDOcata(CApp a) {
    try {

      setApp(a);

      res = ResourceBundle.getBundle("infcata.Res" + a.getIdioma());
      informe = new Pan_InfEDOcata(a);
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      chkCasos.setState(true);
      informe.setEnabled(false);

      stubNivAsis = new StubSrvBD(new URL(this.app.getURL() + strSERVLETcn));

      // ahora leemos la tabla de nivel asistencial
      datan = new CLista();
      datan.setFilter("");
      datan.setState(CLista.listaVACIA);
      DataCN datosAuxil = null;

      datosAuxil = new DataCN("", "", "", "", "", "", "", "", "", "", "", "",
                              "", "", "", "", "");
      datan.addElement(datosAuxil);

      listaNivAsis = new CLista();
      listaNivAsis = (CLista) stubNivAsis.doPost(
          servletSELECCION_NIVASIS_X_CODIGO, datan);

      this.txtArea.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesArea.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtDistrito.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDistrito.setText(this.app.getDS_NIVEL2_DEFECTO());

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(378);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    lblArea.setText(res.getString("lblArea.Text"));
    txtArea.addFocusListener(new Pan_EDOnoMA_txtArea_focusAdapter(this));
    txtArea.addKeyListener(new Pan_EDOnoMA_txtArea_keyAdapter(this));

    // gestores de eventos
    /*btnEnfermedad.addActionListener(btnActionListener);
         btnLimpiar.addActionListener(btnActionListener);
         btnInforme.addActionListener(btnActionListener);
         txtEnfermedad.addTextListener(txtTextAdapter);
         txtEnfermedad.addFocusListener(txtFocusAdapter);
     */

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    txtDesArea.setEditable(false);
    txtDesArea.setEnabled(false); /*E*/
    txtDistrito.addFocusListener(new Pan_EDOvarNot_txtDistrito_focusAdapter(this));
    txtDistrito.addKeyListener(new Pan_EDOvarNot_txtDistrito_keyAdapter(this));
    btnDistrito.setActionCommand("buscarArea");
    btnDistrito.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    txtDesDistrito.setEditable(false);
    txtDesDistrito.setEnabled(false); /*E*/
    lblDistrito.setText(res.getString("lblDistrito.Text"));
    lblSemanas.setText(res.getString("lblSemanas.Text"));
    txtSemanas.setBackground(new Color(255, 255, 150));
    label1.setText(res.getString("label1.Text"));
    lblSeleccion.setText(res.getString("lblSeleccion.Text"));
    chkCasos.setLabel(res.getString("chkCasos.Label"));
    chkCasos.setCheckboxGroup(checkboxGroup2);
    chkTasas.setLabel(res.getString("chkTasas.Label"));
    chkTasas.setCheckboxGroup(checkboxGroup2);
    btnDistrito.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnDistrito.addActionListener(new Pan_EDOvarNot_btnDistrito_actionAdapter(this));
    //btnDistrito.addActionListener(new Pan_EDOnoMA_btnArea1_actionAdapter(this));
    //txtDistrito.addFocusListener(new Pan_EDOnoMA_txtArea1_focusAdapter(this));
    //txtDistrito.addKeyListener(new Pan_EDOnoMA_txtArea1_keyAdapter(this));
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    //btnArea.addActionListener(btnActionListener);
    btnArea.setActionCommand("buscarArea");
    btnArea.setImageURL(new URL(app.getCodeBase(), imgLUPA));

    lblHasta.setText(res.getString("lblHasta.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 42, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 42, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 80, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 80, -1, -1));
    this.add(lblSemanas, new XYConstraints(26, 137, -1, -1));
    this.add(txtSemanas, new XYConstraints(143, 137, 50, -1));
    this.add(label1, new XYConstraints(196, 137, -1, -1));
    this.add(lblArea, new XYConstraints(26, 171, -1, -1));
    this.add(txtArea, new XYConstraints(143, 171, 77, -1));
    this.add(btnArea, new XYConstraints(225, 171, -1, -1));
    this.add(txtDesArea, new XYConstraints(254, 171, 287, -1));
    this.add(txtDistrito, new XYConstraints(143, 205, 77, -1));
    this.add(btnDistrito, new XYConstraints(225, 205, -1, -1));
    this.add(txtDesDistrito, new XYConstraints(254, 205, 287, -1));
    this.add(lblDistrito, new XYConstraints(26, 205, -1, -1));
    this.add(lblSeleccion, new XYConstraints(26, 241, -1, -1));
    this.add(chkCasos, new XYConstraints(143, 241, -1, -1));
    this.add(chkTasas, new XYConstraints(254, 241, -1, -1));
    this.add(btnLimpiar, new XYConstraints(389, 280, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 280, -1, -1));

    btnArea.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnArea.addActionListener(new Pan_EDOnoMA_btnArea_actionAdapter(this));
    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnLimpiar.addActionListener(new Pan_EDOnoMA_btnLimpiar_actionAdapter(this));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));
    btnInforme.addActionListener(new Pan_EDOnoMA_btnInforme_actionAdapter(this));

    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;
    String msg = "";
    CMessage msgBox;
    UtilEDO util = new UtilEDO(); //Para comparar fechas

    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0) ||
        (txtSemanas.getText().length() == 0)) {
      bDatosCompletos = false;
      msg = res.getString("msg3.Text");
    }
    //LRG
    else if (util.fecha1MayorqueFecha2(fechasDesde.txtFecSem.getText(),
                                       fechasHasta.txtFecSem.getText())) {
      bDatosCompletos = false;
      msg = "Fecha 'Desde' no debe ser posterior a fecha 'Hasta'";
    }

    if (!bDatosCompletos) {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA, msg);
      msgBox.show();
      msgBox = null;
    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtArea.setEnabled(true);
        btnArea.setEnabled(true);
        //txtDesArea.setEnabled(true); /*E*/
        txtDesArea.setEditable(false);

        if (this.txtDesArea.getText().length() > 0) {
          btnDistrito.setEnabled(true);
          txtDistrito.setEnabled(true);
        }
        else {
          btnDistrito.setEnabled(false);
          txtDistrito.setEnabled(false);
        }

        //txtDesDistrito.setEnabled(true); /*E*/
        txtDesDistrito.setEditable(false);

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtArea.setEnabled(false);
        btnArea.setEnabled(false);
        //txtDesArea.setEnabled(false); /*E*/
        txtDesArea.setEditable(false);

        txtDistrito.setEnabled(false);
        btnDistrito.setEnabled(false);
        //txtDesDistrito.setEnabled(false); /*E*/
        txtDesDistrito.setEditable(false);

        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    modoOperacion = modoESPERA;
    Inicializar();

    txtArea.setText("");
    txtDesArea.setText("");
    txtDistrito.setText("");
    txtDesDistrito.setText("");

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnInforme_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC2 = new DataEDOcata();

    if (isDataValid()) {
      modoOperacion = modoESPERA;
      Inicializar();

      if (!txtDesArea.getText().equals("")) {
        paramC2.sDsArea = txtDesArea.getText();
        paramC2.sCdArea = txtArea.getText();

        if (!txtDesDistrito.getText().equals("")) {
          paramC2.sDsDistrito = txtDesDistrito.getText();
          paramC2.sCdDistrito = txtDistrito.getText();
        }

      }

      paramC2.sAnoDesde = fechasDesde.txtAno.getText();
      paramC2.sAnoHasta = fechasHasta.txtAno.getText();

      if (fechasDesde.txtCodSem.getText().length() == 1) {
        paramC2.sSemDesde = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        paramC2.sSemDesde = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        paramC2.sSemHasta = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        paramC2.sSemHasta = fechasHasta.txtCodSem.getText();

      }
      paramC2.numSemanas = Integer.parseInt(txtSemanas.getText());
      paramC2.numSemanas--;

      paramC2.bCasos = chkCasos.getState(); // E

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.datosPar = paramC2;
      if (informe.GenerarInforme()) {
        ( (Pan_InfEDOcata) informe).show();
      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    // Mensaje se saca en isDataValid si es necesario (LRG)
    /*
         else {
          msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA, res.getString("msg4.Text"));
          msgBox.show();
          msgBox = null;
        }
     */
  }

  void btnArea_actionPerformed(ActionEvent e) {
    DataNivel1 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaArea lista = new CListaArea(this.app,
                                        res.getString("msg5.Text"),
                                        stubCliente,
                                        strSERVLETNivel1,
                                        servletOBTENER_X_CODIGO,
                                        servletOBTENER_X_DESCRIPCION,
                                        servletSELECCION_X_CODIGO,
                                        servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtArea.setText(data.getCod());
      txtDesArea.setText(data.getDes());
      txtDistrito.setText("");
      txtDesDistrito.setText("");
      btnDistrito.setEnabled(true);
      txtDistrito.setEnabled(true);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void txtArea_keyPressed(KeyEvent e) {
    txtDesArea.setText("");
    txtDistrito.setText("");
    txtDesDistrito.setText("");
    btnDistrito.setEnabled(false);
    txtDistrito.setEnabled(false);
  }

  void txtArea_focusLost(FocusEvent e) {
    // datos de envio
    DataNivel1 nivel1;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // busca el item
    if (txtArea.getText().length() > 0) {

      try {
        modoOperacion = modoESPERA;
        Inicializar();
        // gestion de datos
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataNivel1(txtArea.getText()));
        strServlet = strSERVLETNivel1;
        modoServlet = servletOBTENER_X_CODIGO;

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          nivel1 = (DataNivel1) param.firstElement();
          txtArea.setText(nivel1.getCod());
          txtDesArea.setText(nivel1.getDes());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  void txtDistrito_keyPressed(KeyEvent e) {
    txtDesDistrito.setText("");
  }

  void txtDistrito_focusLost(FocusEvent e) {
    // datos de envio
    DataZBS2 nivel2;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;
    // gestion de datos
    param = new CLista();
    param.setIdioma(app.getIdioma());
    param.addElement(new DataZBS2(txtArea.getText(), txtDistrito.getText(), "",
                                  ""));
    strServlet = strSERVLET_NIV2;
    modoServlet = servletOBTENER_NIV2_X_CODIGO;

    // busca el item
    if (txtDistrito.getText().compareTo("") != 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          nivel2 = (DataZBS2) param.firstElement();
          txtDistrito.setText(nivel2.getNiv2());
          txtDesDistrito.setText(nivel2.getDes());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  void btnDistrito_actionPerformed(ActionEvent e) {

    DataZBS2 data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIV2));
      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg7.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLET_NIV2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        txtDesDistrito.setText(data.getDes());
        txtDistrito.setText(data.getNiv2());
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    Inicializar();
  }

} //clase

////////////////////// Clases para listas

// lista de valores
class CListaArea
    extends CListaValores {
  protected Pan_EDOcata panel;

  public CListaArea(CApp app,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(app,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

class Pan_EDOnoMA_btnArea_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOcata adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnArea_actionAdapter(Pan_EDOcata adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnArea_actionPerformed(e);
  }
}

class Pan_EDOnoMA_txtArea_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOcata adaptee;

  Pan_EDOnoMA_txtArea_keyAdapter(Pan_EDOcata adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtArea_keyPressed(e);
  }
}

class Pan_EDOnoMA_txtArea_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_EDOcata adaptee;
  FocusEvent e;

  Pan_EDOnoMA_txtArea_focusAdapter(Pan_EDOcata adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtArea_focusLost(e);
  }
}

class Pan_EDOnoMA_btnLimpiar_actionAdapter
    implements java.awt.event.ActionListener {
  Pan_EDOcata adaptee;

  Pan_EDOnoMA_btnLimpiar_actionAdapter(Pan_EDOcata adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnLimpiar_actionPerformed(e);
  }
}

class Pan_EDOnoMA_btnInforme_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOcata adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnInforme_actionAdapter(Pan_EDOcata adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnInforme_actionPerformed(e);
  }
}

class Pan_EDOvarNot_txtDistrito_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOcata adaptee;

  Pan_EDOvarNot_txtDistrito_keyAdapter(Pan_EDOcata adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtDistrito_keyPressed(e);
  }
}

class Pan_EDOvarNot_txtDistrito_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_EDOcata adaptee;
  FocusEvent e;

  Pan_EDOvarNot_txtDistrito_focusAdapter(Pan_EDOcata adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtDistrito_focusLost(e);
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected Pan_EDOcata panel;

  public CListaZBS2(Pan_EDOcata p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtArea.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

class Pan_EDOvarNot_btnDistrito_actionAdapter
    implements java.awt.event.ActionListener {
  Pan_EDOcata adaptee;

  Pan_EDOvarNot_btnDistrito_actionAdapter(Pan_EDOcata adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnDistrito_actionPerformed(e);
  }
}
