package infcata;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class Pan_InfEDOcata
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvInfEDOcata";
  final String strLOGO_CCAA = "images/ccaa.gif";

  final int erwCASOS_EDO = 1;
  final int erwTASAS_EDO = 2;

  public Param_C9_3_14 paramC1 = new Param_C9_3_14();
  public DataEDOcata datosPar = null;

  // estructuras de datos
  protected Vector vAgrupaciones;
  protected Vector vTotales;
  protected Vector vCasos;
  protected CLista lista;
  public CApp app;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // Para pedir mas paginas
  protected Vector ultSemana = new Vector();

  // report
  ERW erw = null;

  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;
  Integer regMostrados = new Integer(0);

  public Pan_InfEDOcata(CApp a) {
    super(a);
    app = a;
    try {
      res = ResourceBundle.getBundle("infcata.Res" + a.getIdioma());
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWEDOcata.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  public void MostrarGrafica() {

  }

  // todo el informe
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();
        param.setFilter(lista.getFilter());
        // obtiene los datos del servidor
        //datosPar.iPagina++;
        param.addElement(datosPar);
        param.setFilter("");
        param.setIdioma(app.getIdioma());
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));

        param.setLogin(app.getLogin());
        param.setLortad(app.getParameter("LORTAD"));
        lista = (CLista) stub.doPost(datosPar.bCasos ? erwCASOS_EDO :
                                     erwTASAS_EDO, param); // E

        if (lista.size() > 0) {
          v = (Vector) lista.elementAt(0);
          vTotales = (Vector) lista.elementAt(1);
          // repintado
          erwClient.refreshReport(true);
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();
    boolean elBoolean = true;

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      // obtiene los datos del servidor
      paramC1.iPagina = 0;

      // �todos los datos?
      datosPar.bInformeCompleto = conTodos;

      //param.addElement(paramC1);
      param.addElement(datosPar);
      param.setIdioma(app.getIdioma());
      param.trimToSize();

      stub.setUrl(new URL(app.getURL() + strSERVLET));

      lista = new CLista();

      lista.setState(CLista.listaVACIA);

      param.setLogin(app.getLogin());
      param.setLortad(app.getParameter("LORTAD"));
      lista = (CLista) stub.doPost(datosPar.bCasos ? erwCASOS_EDO :
                                   erwTASAS_EDO, param);

      /*      SrvInfEDOcata srv = new SrvInfEDOcata();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                   "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                   "pista",
                                   "loteb98");
           lista = srv.doDebug(datosPar.bCasos ? erwCASOS_EDO : erwTASAS_EDO, param);
       */

      // control de registros
      if (lista == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg8.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        elBoolean = false;
      }
      else if (lista.size() > 0) {

        vCasos = (Vector) lista.elementAt(0);
        vTotales = (Vector) lista.elementAt(1);

        TemplateManager tm = null;

        // plantilla
        tm = erw.GetTemplateManager();
        DataEDOcata cabeceras = new DataEDOcata();

        tm.SetLabel("LAB009", "");
        tm.SetLabel("LAB010", "");
        tm.SetLabel("LAB011", "");
        tm.SetLabel("LAB012", "");
        tm.SetLabel("LAB013", "");
        tm.SetLabel("LAB014", "");
        tm.SetLabel("LAB016", "");
        tm.SetLabel("LAB017", "");
        tm.SetLabel("LAB018", "");
        tm.SetLabel("LAB019", "");
        tm.SetLabel("LAB020", "");
        tm.SetLabel("LAB021", "");
        tm.SetLabel("LAB022", "");
        tm.SetLabel("LAB024", "");

        for (int ft = 0; ft < vCasos.size(); ft++) {
          cabeceras = (DataEDOcata) vCasos.elementAt(ft);

          switch (ft) {
            case 0:
              tm.SetLabel("LAB009",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 1:
              tm.SetLabel("LAB010",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 2:
              tm.SetLabel("LAB011",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 3:
              tm.SetLabel("LAB012",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 4:
              tm.SetLabel("LAB013",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 5:
              tm.SetLabel("LAB014",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 6:
              tm.SetLabel("LAB016",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 7:
              tm.SetLabel("LAB017",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 8:
              tm.SetLabel("LAB018",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 9:
              tm.SetLabel("LAB019",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 10:
              tm.SetLabel("LAB020",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 11:
              tm.SetLabel("LAB021",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;

            case 12:
              tm.SetLabel("LAB022",
                          cabeceras.sSemDesde + "-" + cabeceras.sSemHasta);
              break;
          }
        }

        //Integer tot = (Integer) lista.elementAt(1);
        //this.setTotalRegistros(tot.toString());
        //this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("CD_ENFERMEDAD = CD_ENFERMEDAD");
        retval.addElement("DS_ENFERMEDAD = DS_ENFERMEDAD");
        retval.addElement("C1 = C1");
        retval.addElement("C2 = C2");
        retval.addElement("C3 = C3");
        retval.addElement("C4 = C4");
        retval.addElement("C5 = C5");
        retval.addElement("C6 = C6");
        retval.addElement("C7 = C7");
        retval.addElement("C8 = C8");
        retval.addElement("C9 = C9");
        retval.addElement("C10 = C10");
        retval.addElement("C11 = C11");
        retval.addElement("C12 = C12");
        retval.addElement("C13 = C13");
        retval.addElement("CTOTAL = CTOTAL");
        retval.addElement("CD_AREA = CD_AREA");
        retval.addElement("DS_AREA = DS_AREA");
        retval.addElement("CD_DISTRITO = CD_DISTRITO");
        retval.addElement("DS_DISTRITO = DS_DISTRITO");
        retval.addElement("CD_TVIGI = CD_TVIGI");

        dataHandler.RegisterTable(vTotales, "SIVE_C9_3_14", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));
        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        elBoolean = true;
        this.setRegistrosMostrados(Integer.toString(vTotales.size()));
        this.setTotalRegistros(Integer.toString(vTotales.size()));
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    //int iEtiqueta = 0;

    TemplateManager tm = null;

    // etiquetas implicadas en los criterios
    String sEtiqueta[] = {
        "LABCRITERIO1", "LABCRITERIO2"};

    // plantilla
    tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    if (datosPar.bCasos) {
      tm.SetLabel("TITULO", res.getString("msg9.Text"));
      tm.SetLabel("PIE_PAGINA", res.getString("msg10.Text"));
      tm.SetLabel("ETIQUETA_POBLACION", "");
    }
    else {
      tm.SetLabel("TITULO", res.getString("msg11.Text"));
      tm.SetLabel("PIE_PAGINA", res.getString("msg12.Text"));
      tm.SetLabel("ETIQUETA_POBLACION", res.getString("msg13.Text"));
    }

    // carga los criterios
    tm.SetLabel(sEtiqueta[0],
                res.getString("msg14.Text") + datosPar.sAnoDesde +
                res.getString("msg15.Text") + datosPar.sSemDesde +
                ", " + res.getString("msg16.Text") + datosPar.sAnoHasta +
                res.getString("msg17.Text") + datosPar.sSemHasta +
                ". " + res.getString("msg18.Text") +
                Integer.toString( (datosPar.numSemanas) + 1) + " " +
                (datosPar.numSemanas == 0 ? res.getString("msg19.Text") :
                 res.getString("msg20.Text")));

    if (datosPar.sDsArea != "") {
      tm.SetLabel(sEtiqueta[1],
                  res.getString("msg21.Text") + datosPar.sCdArea + " " +
                  datosPar.sDsArea);
      if (datosPar.sDsDistrito != "") {
        tm.SetLabel(sEtiqueta[1],
                    tm.GetLabel(sEtiqueta[1]) + ", " +
                    res.getString("msg22.Text") + datosPar.sCdDistrito + " " +
                    datosPar.sDsDistrito);
      }
    }
    else {
      tm.SetLabel(sEtiqueta[1], "");
    }

    //En todas las p�ginas
    tm.SetLabel("LAB024",
                EPanel.PERIODO + datosPar.sAnoDesde + EPanel.SEPARADOR_ANO_SEM +
                datosPar.sSemDesde
                + EPanel.HASTA + datosPar.sAnoHasta + EPanel.SEPARADOR_ANO_SEM +
                datosPar.sSemHasta
                + EPanel.AGRUPACION +
                Integer.toString( (datosPar.numSemanas) + 1) +
                res.getString("msg23.Text")
                + EPanel.AREA + datosPar.sDsArea
                + "-" + datosPar.sDsDistrito);

  }
}
