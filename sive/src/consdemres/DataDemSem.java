package consdemres;

import java.util.Vector;

import capp.CLista;
import conspacenf.Data;
import sapp.DBServlet;

public class DataDemSem
    extends Data {

  public boolean bInformeCompleto = false;

  //  MODOS
  public static final int modoCENTRO = 0;
  public static final int modoCENTRO_EQ = 1;
  public static final int modoCENTRO_CN = 2;
  public static final int modoCENTRO_ORDER = 3;
  public static final int modoCENTRO_COUNT = 4;

  public static final int modoEQUIPO = 10;
  public static final int modoEQUIPO_EQ = 11;
  public static final int modoEQUIPO_CN = 12;
  public static final int modoEQUIPO_ORDER = 13;
  public static final int modoEQUIPO_COUNT = 14;

  // CAMPOS  que se van a devolver
  public static final int COUNT = 0;
  public static final int NOMBRE = 1;
  public static final int DESDE_ANIO = 2;
  public static final int DESDE_SEM = 3;
  public static final int HASTA_ANIO = 4;
  public static final int HASTA_SEM = 5;
  public static final int MED_DEMORA = 6;
  public static final int COBERTURA = 7;
  public static final int TOTAL_DEMORA = 8;
  public static final int ENT_DEM = 9;
  public static final int DEC_DEM = 10;
  public static final int ENT_COB = 11;
  public static final int DEC_COB = 12;
  public static final int COD = 13;

  // PARAMETROS
  public static final int P_PAGINA = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_DESDE_CD_ANOEPI1 = 1;
  public static final int P_DESDE_CD_SEMEPI2 = 2;
  public static final int P_DESDE_CD_ANOEPI3 = 3;
  public static final int P_HASTA_CD_ANOEPI4 = 4;
  public static final int P_HASTA_CD_ANOEPI5 = 5;
  public static final int P_HASTA_CD_SEMEPI6 = 6;
  public static final int P_CD_E_NOTIF = 7;
  public static final int P_CD_C_NOTIF = 8;
  public static final int P_CD_NIVEL_1 = 9;
  public static final int P_CD_NIVEL_2 = 10;
  public static final int P_CD_ZBS = 11;
  public static final int P_CD_PROV = 12;
  public static final int P_CD_MUN = 13;
  public static final int P_AGRUPADO = 14;
  public static final int P_AGRU_EQ = 15; // indica si se agrupa por equipos o no

  protected static String select_eq = " SELECT " +
      " CD_E_NOTIF, CD_SEMEPI, CD_ANOEPI, FC_RECEP, NM_NNOTIFR, CD_E_NOTIF " +
      " FROM SIVE_NOTIFEDO WHERE IT_RESSEM='S' AND  ";

  protected static String eNotif_eq = " AND CD_E_NOTIF IN " +
      " ( SELECT CD_E_NOTIF FROM SIVE_E_NOTIF " +
      "      WHERE CD_E_NOTIF LIKE ? AND " +
      "            CD_CENTRO LIKE ? AND " +
      "            CD_NIVEL_1 LIKE ? AND " +
      "            CD_NIVEL_2 LIKE ? AND " +
      "            CD_ZBS LIKE ?  ";

  protected static String eNotif_cn = " AND CD_CENTRO IN " + // parametros opcionales
      " ( SELECT CD_CENTRO FROM SIVE_C_NOTIF " +
      "      WHERE CD_PROV LIKE ? AND " +
      "            CD_MUN LIKE ? ";

  protected static String order_eq =
      " ORDER BY CD_ANOEPI, CD_SEMEPI, CD_E_NOTIF ";

  public static String group_eq = " GROUP BY CD_ANOEPI, CD_SEMEPI, CD_E_NOTIF ";

  protected static String select_cn = " SELECT " +
      " a.CD_E_NOTIF, a.CD_SEMEPI, a.CD_ANOEPI, a.FC_RECEP, a.NM_NNOTIFR, a.CD_E_NOTIF, b.CD_CENTRO " +
      " FROM SIVE_NOTIFEDO a, SIVE_C_NOTIF b WHERE a.CD_E_NOTIF = b.CD_E_NOTIF AND a.IT_RESSEM='S' AND ";

  public static String filtroNoIguales_cn =
      " ( ( a.CD_ANOEPI = ? AND a.CD_SEMEPI >= ? ) OR " +
      " ( a.CD_ANOEPI > ? AND a.CD_ANOEPI < ? ) OR " +
      " ( a.CD_ANOEPI = ? AND a.CD_SEMEPI <= ? ) ) ";

  public static String filtroIguales_cn =
      " ( a.CD_ANOEPI = ? AND a.CD_SEMEPI >= ? AND " +
      "  a.CD_ANOEPI = ? AND a.CD_ANOEPI = ?  AND " +
      "  a.CD_ANOEPI = ? AND a.CD_SEMEPI <= ?  ) ";

  protected static String cNotif_eq = " AND a.CD_E_NOTIF IN " +
      " ( SELECT CD_E_NOTIF, CD_CENTRO FROM SIVE_E_NOTIF " +
      "      WHERE CD_E_NOTIF LIKE ? AND " +
      "            CD_CENTRO LIKE ? AND " +
      "            CD_NIVEL_1 LIKE ? AND " +
      "            CD_NIVEL_2 LIKE ? AND " +
      "            CD_ZBS LIKE ?  ";

  protected static String cNotif_cn = " AND CD_CENTRO IN " + // parametros opcionales
      " ( SELECT CD_CENTRO FROM SIVE_C_NOTIF " +
      "      WHERE CD_PROV LIKE ? AND " +
      "            CD_MUN LIKE ? ";

  protected static String order_cn =
      " ORDER BY CD_ANOEPI, CD_SEMEPI, CD_CENTRO ";

  public static String group_cn = " GROUP BY CD_ANOEPI, CD_SEMEPI, CD_CENTRO ";

  public static String filtroNoIguales_eq =
      " ( ( CD_ANOEPI = ? AND CD_SEMEPI >= ? ) OR " +
      " ( CD_ANOEPI > ? AND CD_ANOEPI < ? ) OR " +
      " ( CD_ANOEPI = ? AND CD_SEMEPI <= ? ) ) ";

  public static String filtroIguales_eq =
      " ( CD_ANOEPI = ? AND CD_SEMEPI >= ? AND " +
      "  CD_ANOEPI = ? AND CD_ANOEPI = ?  AND " +
      "  CD_ANOEPI = ? AND CD_SEMEPI <= ?  ) ";

  public DataDemSem(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[16];
  }

  public Object getNewData() {
    return new DataDemSem(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "NOMBRE";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    int num_result = DBServlet.maxSIZE;
    Data param_item = (Data) ini_param.firstElement();
    String hasta_ano = (String) param_item.get(DataFinSem.P_HASTA_CD_ANOEPI5);
    String desde_ano = (String) param_item.get(DataFinSem.P_DESDE_CD_ANOEPI1);

    switch (opmode) {
      case modoCENTRO:
        if (hasta_ano.equals(desde_ano)) {
          strSQL.append(select_cn + filtroIguales_cn);
        }
        else {
          strSQL.append(select_cn + filtroNoIguales_cn);
        }
        break;
      case modoCENTRO_EQ:
        strSQL.append(cNotif_eq);
        break;
      case modoCENTRO_CN:
        strSQL.append(cNotif_cn);
        break;
      case modoCENTRO_ORDER:
        strSQL.append(order_cn);
        break;

      case modoEQUIPO:
        if (hasta_ano.equals(desde_ano)) {
          strSQL.append(select_eq + filtroIguales_eq);
        }
        else {
          strSQL.append(select_eq + filtroNoIguales_eq);
        }
        break;
      case modoEQUIPO_EQ:
        strSQL.append(eNotif_eq);
        break;
      case modoEQUIPO_CN:
        strSQL.append(eNotif_cn);
        break;
      case modoEQUIPO_ORDER:
        strSQL.append(order_eq);
        break;

    }

    // indicamos le n�merod e registros que queremos
    return num_result; // por defecto
  }

  public String getNOMBRE() {
    if (arrayDatos.length > NOMBRE) {
      return (String) arrayDatos[NOMBRE];
    }
    else {
      return " ";
    }
  }

  public String getDESDE_ANIO() {
    if (arrayDatos.length > DESDE_ANIO) {
      return (String) arrayDatos[DESDE_ANIO];
    }
    else {
      return " ";
    }
  }

  public String getDESDE_SEM() {
    if (arrayDatos.length > DESDE_SEM) {
      return (String) arrayDatos[DESDE_SEM];
    }
    else {
      return " ";
    }
  }

  public String getHASTA_ANIO() {
    if (arrayDatos.length > HASTA_ANIO) {
      return (String) arrayDatos[HASTA_ANIO];
    }
    else {
      return " ";
    }
  }

  public String getHASTA_SEM() {
    if (arrayDatos.length > HASTA_SEM) {
      return (String) arrayDatos[HASTA_SEM];
    }
    else {
      return " ";
    }
  }

  public String getMED_DEMORA() {
    if (arrayDatos.length > MED_DEMORA) {
      return (String) arrayDatos[MED_DEMORA];
    }
    else {
      return " ";
    }
  }

  public String getCOBERTURA() {
    if (arrayDatos.length > COBERTURA) {
      return (String) arrayDatos[COBERTURA];
    }
    else {
      return " ";
    }
  }

  public String getCOD() {
    if (arrayDatos.length > COD) {
      return (String) arrayDatos[COD];
    }
    else {
      return " ";
    }
  }

  public String getENT_DEM() {
    if (arrayDatos.length > ENT_DEM) {
      return (String) arrayDatos[ENT_DEM];
    }
    else {
      return " ";
    }
  }

  public String getDEC_DEM() {
    if (arrayDatos.length > DEC_DEM) {
      return (String) arrayDatos[DEC_DEM];
    }
    else {
      return " ";
    }
  }

  public String getENT_COB() {
    if (arrayDatos.length > ENT_COB) {
      return (String) arrayDatos[ENT_COB];
    }
    else {
      return " ";
    }
  }

  public String getDEC_COB() {
    if (arrayDatos.length > DEC_COB) {
      return (String) arrayDatos[DEC_COB];
    }
    else {
      return " ";
    }
  }

  /** indica al report como leer los datos */
  public Vector getFieldVector() {
    Vector retval = new Vector();

    retval.addElement("NOMBRE  = getNOMBRE");
    retval.addElement("DESDE_ANIO  = getDESDE_ANIO");
    retval.addElement("DESDE_SEM  = getDESDE_SEM");
    retval.addElement("HASTA_ANIO  = getHASTA_ANIO");
    retval.addElement("HASTA_SEM  = getHASTA_SEM");
    retval.addElement("MED_DEMORA  = getMED_DEMORA");
    retval.addElement("COBERTURA  = getCOBERTURA");
    retval.addElement("ENT_DEM  = getENT_DEM");
    retval.addElement("DEC_DEM  = getDEC_DEM");
    retval.addElement("ENT_COB  = getENT_COB");
    retval.addElement("DEC_COB  = getDEC_COB");
    retval.addElement("COD  = getCOD");
    return retval;
  }

} //________________________________________  END CLASS