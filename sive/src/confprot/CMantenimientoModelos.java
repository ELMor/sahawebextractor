package confprot;

import capp.CApp;
import capp.CListaMantenimiento;
import comun.constantes;
import sapp.StubSrvBD;

public class CMantenimientoModelos
    extends CListaMantenimiento {

  public CMantenimientoModelos(ApConfProt a) { //^^^^^^^^^^^^^^

    super( (CApp) a, //^^^^^^^^^^^^^^^^^^^^^^^^
          3,
          "100\n400\n62",
          a.sEtCodigo + "\n" + a.sEtModelo + "\n" + a.sEtOperativo, //^^^^^^^^
          new StubSrvBD(),
//          "servlet/SrvGesMod",
          constantes.strSERVLET_MOD,
          5,
          6);

    ActivarAuxiliar();
  }

  //Constructor que activa/desativa botones en funci�n de autorizaciones
  // del usuario
  public CMantenimientoModelos(ApConfProt a, boolean USU) { //^^^^^^^^^^^^^^

    super( (CApp) a, //^^^^^^^^^^^^^^^^^^^^^^^^
          3,
          "100\n400\n62",
          a.sEtCodigo + "\n" + a.sEtModelo + "\n" + a.sEtOperativo, //^^^^^^^^
          new StubSrvBD(),
//          "servlet/SrvGesMod",
          constantes.strSERVLET_MOD,
          5,
          6,
          constantes.keyBtnAnadir,
          constantes.keyBtnModificar,
          constantes.keyBtnBorrar,
          constantes.keyBtnAux);

    ActivarAuxiliar();
  }

  // confeccionar modelo
  public void btnAuxiliar_actionPerformed(int i) {
    modoOperacion = modoESPERA;
    Inicializar();
    DataModelo dat = (DataModelo) lista.elementAt(i);
    PanelModelo panel = new PanelModelo(this.app,
                                        new StubSrvBD(),
                                        dat.getCD_Modelo(),
                                        dat.getDS_Modelo(),
                                        dat.getIT_OK());
    panel.show();
    panel = null;

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    modoOperacion = modoESPERA;
    Inicializar();
    DialMantModelo panel = new DialMantModelo(this.app,
                                              DialMantModelo.modoALTA,
                                              null);
    try {
      panel.show();

      if (panel.bAceptar) {
        lista.addElement(panel.datModelo);
        RellenaTabla();
      }

      panel = null;

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {

    modoOperacion = modoESPERA;
    Inicializar();
    DialMantModelo panel = new DialMantModelo(this.app,
                                              DialMantModelo.modoMODIFICAR,
                                              (DataModelo) lista.elementAt(i));

    try {
      panel.show();

      if (panel.bAceptar) {
        //Si modelo est� operativo se recarga otra vez toda la lista
        //Por si es necesario actualizar a no operat. otros modelos de lista
        if (panel.datModelo.getIT_OK().compareTo("S") == 0) {
          btnCtrlCodModelo_actionPerformed();
        }
        //Si modelo no est� operativo simplemente se modifica ese elemento en lista
        else {
          lista.removeElementAt(i);
          lista.insertElementAt(panel.datModelo, i);
          RellenaTabla();

        } //else

      }
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {

    modoOperacion = modoESPERA;
    Inicializar();
    DialMantModelo panel = new DialMantModelo(this.app,
                                              DialMantModelo.modoBAJA,
                                              (DataModelo) lista.elementAt(i));
    panel.show();

    if (panel.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataModelo(app.getTSive(),
                          s,
                          "",
                          "",
                          "",
                          "",
                          app.getCA(),
                          "",
                          app.getParametro("CD_TUBERCULOSIS"), //"",
                          "",
                          "");
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataModelo dat = (DataModelo) o;
    String sLinea = dat.getCD_Modelo() + '&' +
        dat.getDS_Modelo() + '&' +
        dat.getIT_OK();
    return sLinea;
  }

}
