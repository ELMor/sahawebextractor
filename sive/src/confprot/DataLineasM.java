package confprot;

import java.io.Serializable;

public class DataLineasM
    implements Serializable {
  String sCD_TSIVE = "";
  String sCD_MODELO = "";
  int iNM_LIN;
  String sCD_TLINEA = "";
  String sDS_TEXTO = "";
  String sDSL_TEXTO = "";

  public DataLineasM(String CD_TSIVE, String CD_MODELO, int NM_LIN,
                     String CD_TLINEA, String DS_TEXTO, String DSL_TEXTO) {

    sCD_TSIVE = CD_TSIVE;
    sCD_MODELO = CD_MODELO;
    iNM_LIN = NM_LIN;
    sCD_TLINEA = CD_TLINEA;
    sDS_TEXTO = DS_TEXTO;
    if (DSL_TEXTO != null) {
      sDSL_TEXTO = DSL_TEXTO;
    }
  }

  public String getCD_TSIVE() {
    return sCD_TSIVE;
  }

  public void setCD_TSIVE(String ts) {
    sCD_TSIVE = ts;
  }

  public String getCD_MODELO() {
    return sCD_MODELO;
  }

  public void setCD_MODELO(String cdmod) {
    sCD_MODELO = cdmod;
  }

  public int getNM_LIN() {
    return iNM_LIN;
  }

  public String getCD_TLINEA() {
    return sCD_TLINEA;
  }

  public String getDS_TEXTO() {
    return sDS_TEXTO;
  }

  public String getDSL_TEXTO() {
    return sDSL_TEXTO;
  }

}