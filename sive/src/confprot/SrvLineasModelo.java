package confprot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;

public class SrvLineasModelo
    extends DBServlet {

  final int servletCONSULTAR_RESPUESTAS = 2;
  final int servletOBTENER_LINEAS = 3;
  final int servletCONSULTAR_LINEAS = 4;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    // objetos de datos
    CLista data = new CLista();
    DataMantLineasModelo datModelo;
    DataMantLinea datLinea;
    String sIndicador = "N";
    int j;

    //Para recoger texto de linea modelo
    String sTexto = "";
    String sTextoL = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    datModelo = (DataMantLineasModelo) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      // indicador de modelo con respuestas
      case servletCONSULTAR_RESPUESTAS:

        // prepara la lista de resultados
        data = new CLista();
        if (datModelo.sTSive.trim().equals("E")) {
          query = "select COUNT(*) from SIVE_RESP_EDO " +
              " where CD_TSIVE = ? and CD_MODELO = ?";
        }

        // Contactos de Tuberculosis
        if (datModelo.sTSive.trim().equals("T")) {
          query = "select COUNT(*) from SIVE_RESPCONTACTO " +
              " where CD_TSIVE = ? and CD_MODELO = ?";
        }

        // Centinelas
        if (datModelo.sTSive.trim().equals("C")) {
          query = "select COUNT(*) from SIVE_RESP_CENTI" +
              " where CD_TSIVE = ? and CD_MODELO = ?";
        }

        else if (datModelo.sTSive.trim().equals("B")) {
          query = "select COUNT(*) from SIVE_RESP_BROTES " +
              " where CD_TSIVE = ? and CD_MODELO = ?";
        }
        st = con.prepareStatement(query);

        // tsive
        st.setString(1, datModelo.sTSive.trim());
        // modelo
        st.setString(2, datModelo.sCodModelo.trim());

        rs = st.executeQuery();

        // graba el indicador
        if (rs.next()) {
          if (rs.getInt(1) == 0) {
            sIndicador = "N";
          }
          else {
            sIndicador = "S";
          }
        }

        // cierra los resulsets
        rs.close();
        st.close();

        data.addElement(sIndicador);

        break;

        // indicador de modelo con lineas
      case servletCONSULTAR_LINEAS:

        // prepara la lista de resultados
        data = new CLista();

        query = "select COUNT(*) from SIVE_LINEASM " +
            " where CD_TSIVE = ? and CD_MODELO = ?";

        st = con.prepareStatement(query);
        // tsive-modelo
        st.setString(1, datModelo.sTSive.trim());
        st.setString(2, datModelo.sCodModelo.trim());

        rs = st.executeQuery();

        // graba el indicador
        if (rs.next()) {
          if (rs.getInt(1) == 0) {
            sIndicador = "N";
          }
          else {
            sIndicador = "S";
          }
        }

        // cierra los resulsets
        rs.close();
        st.close();

        data.addElement(sIndicador);

        break;

        // l�neas del modelo
      case servletOBTENER_LINEAS:

        // prepara la lista de resultados
        data = new CLista();

        query = "select NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
            " from SIVE_LINEASM " +
            " where CD_TSIVE = ? and CD_MODELO = ? " +
            " order by NM_LIN";

        st = con.prepareStatement(query);
        // tsive //modelo
        st.setString(1, datModelo.sTSive.trim());
        st.setString(2, datModelo.sCodModelo.trim());

        rs = st.executeQuery();

        // extrae las l�neas
        while (rs.next()) {

//________________________

          sTexto = rs.getString("DS_TEXTO");
          sTextoL = rs.getString("DSL_TEXTO");

          // a�ade las componentes
          datLinea = new DataMantLinea(rs.getInt("NM_LIN"),
                                       rs.getString("CD_TLINEA"),
                                       sTexto,
                                       sTextoL);
          //_______________________

          data.addElement(datLinea);
        }

        rs.close();
        st.close();

        // recorre la lista, y para cada linea de tipo pregunta, recupera sus atributos
        for (j = 0; j < data.size(); j++) {

          // recupera la l�nea
          datLinea = (DataMantLinea) data.elementAt(j);

          // l�nea tipo pregunta
          if (datLinea.sTipo.equals(DataMantLinea.tipoPREGUNTA)) {

            query = "select CD_PREGUNTA, IT_OBLIGATORIO, " +
                "IT_CONDP, NM_LIN_PC, DS_VPREGUNTA_PC " +
                " from SIVE_LINEA_ITEM " +
                " where CD_TSIVE = ? and CD_MODELO = ?  " +
                " and NM_LIN = ?";

            // par�metros adicionales
            st = con.prepareStatement(query);

            // par�metros
            st.setString(1, datModelo.sTSive.trim());
            st.setString(2, datModelo.sCodModelo.trim());
            st.setInt(3, datLinea.iLinea);

            // obtiene par�metros
            rs = st.executeQuery();

            if (rs.next()) {
              // c�digo de pregunta
              datLinea.sCodPregunta = rs.getString("CD_PREGUNTA");

              // obligatoria
              if (rs.getString("IT_OBLIGATORIO").equals("S")) {
                datLinea.bObligatoria = true;

                // condicionada
              }
              if (rs.getString("IT_CONDP").equals("S")) {
                datLinea.bCondicionada = true;

                // datos condicionados
              }
              if (datLinea.bCondicionada) {

                // l�nea condicionada
                datLinea.iCondALinea = rs.getInt("NM_LIN_PC");

                // valor condicionado
                datLinea.sCondAValor = rs.getString("DS_VPREGUNTA_PC");
              }
            }

            rs.close();
            st.close();
          }
        }

        // recorre la lista, y para cada linea de tipo pregunta, recupera los atributos de la pregunta
        for (j = 0; j < data.size(); j++) {

          // recupera la l�nea
          datLinea = (DataMantLinea) data.elementAt(j);

          // l�nea tipo pregunta
          if (datLinea.sTipo.equals(DataMantLinea.tipoPREGUNTA)) {

            // par�metros adicionales
            st = con.prepareStatement("select CD_TPREG, NM_LONG, NM_ENT, NM_DEC from SIVE_PREGUNTA where CD_TSIVE = ? and CD_PREGUNTA = ?");

            // par�metros
            st.setString(1, datModelo.sTSive.trim());
            st.setString(2, datLinea.sCodPregunta);

            // obtiene par�metros
            rs = st.executeQuery();

            if (rs.next()) {
              datLinea.sTPreg = rs.getString("CD_TPREG");
              datLinea.iLong = rs.getInt("NM_LONG");
              datLinea.iEnt = rs.getInt("NM_ENT");
              datLinea.iDec = rs.getInt("NM_DEC");
            }

            rs.close();
            st.close();
          }

        }

        break;

    } //switch

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();
    }
    return data;
  } // proc doWork
}
