package confprot;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import COM.objectspace.jgl.Pair;
import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.control.TreeControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import com.borland.jbcl.model.BasicViewManager;
import com.borland.jbcl.model.GraphLocation;
import com.borland.jbcl.model.GraphSelectionEvent;
import com.borland.jbcl.model.SingleGraphSelection;
import com.borland.dx.text.Alignment;
import com.borland.jbcl.view.CompositeItemPainter;
import com.borland.jbcl.view.FocusableItemPainter;
import com.borland.jbcl.view.ImageItemPainter;
import com.borland.jbcl.view.SelectableItemPainter;
import com.borland.jbcl.view.TextItemPainter;
import com.borland.jbcl.view.TreeView;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import comun.constantes;
import sapp.StubSrvBD;

public class PanelModelo
    extends CDialog {

  // im�genes
  final String imgNAME[] = {
      "images/alta2.gif",
      "images/alta3.gif",
      "images/baja2.gif",
      "images/modificacion2.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/texto.gif",
      "images/pregunta.gif",
      "images/pregunta2.gif"};
  ResourceBundle res = ResourceBundle.getBundle("confprot.Res" +
                                                this.app.getIdioma());
  //     "images/inspreg.gif",
  //     "images/inslista.gif"

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  /*  // servlets
    final String strSERVLET = "servlet/SrvModelo";
    final String strSERVLET_LINEAS = "servlet/SrvLineasModelo";
   */

  final String strSERVLET = constantes.strSERVLET_DE_MODELO;
  final String strSERVLET_LINEAS = constantes.strSERVLET_MODELO;

  // modos del servlet
  final int servletMODIFICAR = 1;
  final int servletCONSULTAR_RESPUESTAS = 2;
  final int servletOBTENER_LINEAS = 3;

  // modos de operaci�n
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  // modos de operacion
  protected int modoOperacion; //modo op. guardado si es necesario

  // vectores de datos
  CLista lineasModelo = new CLista();
  Vector listaNodos = new Vector();

  // stub
  StubSrvBD stubCliente = null;

  // c�digo del modelo
  String sCodModelo;
  String sDesModelo;

  // indicador de respuestas
  String sIndicadorRespuestas;

  // nodo del arbol seleccionado
  GraphLocation nodoSeleccionado = null;

  // controles
  Panel pnlArbol = new Panel();
  Panel pnlStatus = new Panel();
  FlowLayout flowLayout1 = new FlowLayout();
  StatusBar statusOperativo = new StatusBar();
  StatusBar statusRespuestas = new StatusBar();

  Panel panelBotones = new Panel();
  XYLayout xYLayout1 = new XYLayout();
  XYLayout xYLayout3 = new XYLayout();
  ButtonControl btnInsertar = new ButtonControl();
  ButtonControl btnModificar = new ButtonControl();
  ButtonControl btnBorrar = new ButtonControl();
  ButtonControl btnInsertarC = new ButtonControl();
  TreeControl arbolModelo = new TreeControl();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
//  ButtonControl btnPreg = new ButtonControl();
//  ButtonControl btnLista = new ButtonControl();

  btnActionListener actionListener = new btnActionListener(this);
  BorderLayout borderLayout2 = new BorderLayout();
  XYLayout xYLayout2 = new XYLayout();
  BorderLayout borderLayout1 = new BorderLayout();
  ScrollPane panelScroll = new ScrollPane();
  Panel pnl = new Panel();

  // constructor
  public PanelModelo(CApp app, StubSrvBD stub, String sCod, String sDes,
                     String sOperativo) {
    super(app);

    try {
      sCodModelo = sCod;
      sDesModelo = sDes;
      stubCliente = stub;

      // obtiene las lineas del modelo
      ObtenerLineasModelo();

      // obtiene el indicador del modelo
      ObtenerIndicadorRespuestas();
      jbInit();

      // arbol
      GenerarArbol();

      // graba barras de estado
      statusOperativo.setText(res.getString("statusOperativo.Text") +
                              sOperativo);
      statusRespuestas.setText(res.getString("statusRespuestas.Text") +
                               sIndicadorRespuestas);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    arbolModelo.addMouseListener(new PanelModelo_pnlArbol_mouseAdapter(this));
    btnInsertar.setImage(imgs.getImage(0));
    btnInsertarC.setImage(imgs.getImage(1));
    btnBorrar.setImage(imgs.getImage(2));
    btnModificar.setImage(imgs.getImage(3));
    btnAceptar.setImage(imgs.getImage(4));
    btnCancelar.setImage(imgs.getImage(5));
//    btnPreg.setImage(imgs.getImage(9));
//    btnLista.setImage(imgs.getImage(10));

    // panel de botones
    xYLayout2.setHeight(100);
    xYLayout2.setWidth(560);
    panelBotones.setLayout(xYLayout1);
    panelBotones.add(btnInsertar, new XYConstraints(10, 2, -1, -1));
    panelBotones.add(btnModificar, new XYConstraints(40, 2, -1, -1));
    panelBotones.add(btnBorrar, new XYConstraints(70, 2, -1, -1));
    panelBotones.add(btnInsertarC, new XYConstraints(100, 2, -1, -1));
//    panelBotones.add(btnPreg, new XYConstraints(170, 2, -1, -1));
//    panelBotones.add(btnLista, new XYConstraints(200, 2, -1, -1));
    panelBotones.add(btnAceptar, new XYConstraints(400, 2, 80, -1));
    panelBotones.add(btnCancelar, new XYConstraints(488, 2, 80, -1));

    // panel de estatus
    pnlStatus.setLayout(borderLayout2);
    pnlStatus.add(statusRespuestas, BorderLayout.CENTER);
    pnlStatus.add(statusOperativo, BorderLayout.WEST);
    statusOperativo.setBevelInner(BevelPanel.LOWERED);
    statusOperativo.setBevelOuter(BevelPanel.LOWERED);
    statusRespuestas.setBevelOuter(BevelPanel.LOWERED);
    statusRespuestas.setBevelInner(BevelPanel.LOWERED);

    // arbol
    arbolModelo.setBackground(Color.white);
    arbolModelo.setStyle(TreeView.STYLE_PLUSES);
    arbolModelo.setAutoEdit(false);
    arbolModelo.setBoxSize(new Dimension(9, 9));
    arbolModelo.setExpandByDefault(true);
    pnl.setLayout(xYLayout3);
    xYLayout3.setHeight(2000);
    xYLayout3.setWidth(570);
    pnl.add(arbolModelo, new XYConstraints(0, 0, 570, 2000));
    panelScroll.add(pnl);

    // arbol + botones
    pnlArbol.setLayout(xYLayout2);
    pnlArbol.add(panelScroll, new XYConstraints(0, 0, 570, 460));
    pnlArbol.add(panelBotones, new XYConstraints(0, 460, 580, 100));

    // establece el visor imagen+texto
    arbolModelo.setViewManager(new BasicViewManager(new FocusableItemPainter(new
        SelectableItemPainter(
        new CompositeItemPainter(new ImageItemPainter(this, Alignment.LEFT),
                                 new TextItemPainter())))));

    // panel completo
    this.setLayout(borderLayout1);
    this.setEnabled(true);
    this.setSize(new Dimension(590, 560));
    this.add(pnlArbol, BorderLayout.CENTER);
    this.add(pnlStatus, BorderLayout.SOUTH);

    // botonera
//    btnPreg.setActionCommand("pregunta");
//    btnLista.setActionCommand("lista");
    btnAceptar.setActionCommand("aceptar");
    btnInsertar.setActionCommand("insertar");
    btnModificar.setActionCommand("modificar");
    btnBorrar.setActionCommand("borrar");
    btnInsertarC.setActionCommand("insertarc");
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setActionCommand("cancelar");
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    panelBotones.setLayout(xYLayout1);

    // panel
    this.doLayout();

    // controladores de eventos
    arbolModelo.addSelectionListener(new selectionAdapter(this));
//    btnPreg.addActionListener(actionListener);
//    btnLista.addActionListener(actionListener);
    btnAceptar.addActionListener(actionListener);
    btnCancelar.addActionListener(actionListener);
    btnInsertar.addActionListener(actionListener);
    btnInsertarC.addActionListener(actionListener);
    btnModificar.addActionListener(actionListener);
    btnBorrar.addActionListener(actionListener);

    setTitle(res.getString("msg36.Text"));
  }

  // pone el arbol en modo consulta si hay respuestas
  void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        if (sIndicadorRespuestas.equals("S")) {
          btnAceptar.setEnabled(false);
          btnInsertar.setEnabled(false);
          btnInsertarC.setEnabled(false);
          btnModificar.setEnabled(false);
          btnBorrar.setEnabled(false);
        }
        else {
          btnAceptar.setEnabled(true);
          btnInsertar.setEnabled(true);
          btnInsertarC.setEnabled(true);
          btnModificar.setEnabled(true);
          btnBorrar.setEnabled(true);
        }

//        btnPreg.setEnabled(true);
//        btnLista.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnInsertar.setEnabled(false);
        btnInsertarC.setEnabled(false);
        btnModificar.setEnabled(false);
//        btnPreg.setEnabled(false);
//        btnLista.setEnabled(false);
        btnBorrar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // obtenci�n de las l�neas del modelo
  public void ObtenerLineasModelo() throws Exception {
    CLista data;

    // prepara los par�metros
    data = new CLista();

    data.addElement(new DataMantLineasModelo(sCodModelo, app.getTSive()));

    /*
        SrvLineasModelo srv= new SrvLineasModelo();
        lineasModelo= srv.doDebug(servletOBTENER_LINEAS, data);
     */
    stubCliente.setUrl(new URL(app.getURL() + strSERVLET_LINEAS));
    lineasModelo = (CLista) stubCliente.doPost(servletOBTENER_LINEAS, data);

    data = null;
  }

  // determina si el modelo tiene respuestas
  public void ObtenerIndicadorRespuestas() throws Exception {

    CLista data;

    // prepara los par�metros
    data = new CLista();

    data.addElement(new DataMantLineasModelo(sCodModelo, app.getTSive()));

    /*
        SrvLineasModelo misrvlet= new SrvLineasModelo();
        misrvlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                               "sive_desa",
                               "sive_desa");
        data= misrvlet.doDebug(servletCONSULTAR_RESPUESTAS, data);
     */

    stubCliente.setUrl(new URL(app.getURL() + strSERVLET_LINEAS));
    data = (CLista) stubCliente.doPost(servletCONSULTAR_RESPUESTAS, data);

    if (data.size() != 0) {
      sIndicadorRespuestas = (String) data.firstElement();
    }
    else {
      sIndicadorRespuestas = "S"; // bloquea el modelo

    }
    data = null;
  }

  // genera el arbol
  public void GenerarArbol() {
    GraphLocation root;
    GraphLocation texto = null;
    GraphLocation pregunta = null;
    GraphLocation raiz = null;
    DataMantLinea linea;

    nodoSeleccionado = null;

    modoOperacion = modoESPERA;
    Inicializar();

    if (arbolModelo.getRoot() != null) {
      arbolModelo.removeChildren(arbolModelo.getRoot());

      // nodo raiz
    }
    listaNodos = new CLista();
    listaNodos.addElement(arbolModelo.setRoot(new Pair(null, sDesModelo)));
    root = (GraphLocation) listaNodos.firstElement();

    // recorre el modelo
    for (int i = 0; i < lineasModelo.size(); i++) {
      linea = (DataMantLinea) lineasModelo.elementAt(i);

      // System_out.print(linea.sTipo + " " + linea.iLinea + " " + linea.sCodPregunta + " " + linea.iCondALinea);
      //# System_Out.println();
      // nueva secci�n
      if (linea.sTipo.equals("D")) {
        texto = null;

        // nodo T (secci�n)
      }
      if (texto == null) {
        texto = arbolModelo.addChild(root,
                                     new Pair(imgs.getImage(6), linea.sDescGeneral));
        listaNodos.addElement(texto);
      }
      // nodo P (pregunta)
      else {

        raiz = texto;

        // seleccion del nodo padre
        if (linea.bCondicionada) {
          raiz = (GraphLocation) listaNodos.elementAt(linea.iCondALinea);

          // determina si obligatoria
        }
        if (linea.bObligatoria) {
          pregunta = arbolModelo.addChild(raiz,
                                          new Pair(imgs.getImage(8),
              linea.sDescGeneral));
        }
        else {
          pregunta = arbolModelo.addChild(raiz,
                                          new Pair(imgs.getImage(7),
              linea.sDescGeneral));

        }
        listaNodos.addElement(pregunta);
      }
    }

    // inicializa el panel arbol
    //xYLayout3.setHeight(lineasModelo.size()*15);
    //panelScroll.doLayout();

    modoOperacion = modoNORMAL;
    Inicializar();
    validate();
  }

  // eliminar nodo
  public void EliminarNodo() {
    CMessage msgBox = null;
    int i;
    int iNumNodos;
    int j;
    DataMantLinea linea;

    if ( (nodoSeleccionado != null) &&
        (nodoSeleccionado != arbolModelo.getRoot())) {

      msgBox = new CMessage(app, CMessage.msgPREGUNTA,
                            res.getString("msg37.Text"));
      msgBox.show();

      modoOperacion = modoESPERA;
      Inicializar();

      // borra el nodo correspondiente
      if ( (msgBox.getResponse()) == true) {

        // posici�n del nodo selecionado
        i = iNodo(nodoSeleccionado);

        // elimina los nodos hijos
        iNumNodos = iNumeroNodosHijo(nodoSeleccionado);

        for (j = i; j < i + iNumNodos; j++) {
          listaNodos.removeElementAt(i);
          lineasModelo.removeElementAt(i - 1);
        }

        // renumera las lineas
        if (lineasModelo.size() >= i) {
          for (j = i - 1; j < lineasModelo.size(); j++) {
            linea = (DataMantLinea) lineasModelo.elementAt(j);
            linea.iLinea = linea.iLinea - iNumNodos;
            if (linea.bCondicionada) {
              linea.iCondALinea = linea.iCondALinea - iNumNodos;
            }
          }
        }

        GenerarArbol();
        nodoSeleccionado = null;
      }
      msgBox = null;
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // determina el n�mero de nodos en un ramal
  protected int iNumeroNodosHijo(GraphLocation nodo) {

    GraphLocation[] hijos;
    int iNum = 1;

    if (nodo.hasChildren() > 0) {
      hijos = nodo.getChildren();
      for (int i = 0; i < hijos.length; i++) {
        iNum = iNum + iNumeroNodosHijo(hijos[i]);
      }
    }

    return iNum;
  }

  // determina la posicion de un nodo en
  public int iNodo(GraphLocation nodo) {
    int i;

    // se posiciona en la lista
    for (i = 0; i < listaNodos.size(); i++) {
      if (nodo == (GraphLocation) listaNodos.elementAt(i)) {
        break;
      }
    }

    return i;
  }

  // insertar nodo
  public void InsertarNodo() {
    GraphLocation padre;
    DataMantLinea linea = null;
    DataMantLinea nuevaLinea = null;
    int i;
    int modo;
    String sTitulo;
    DialMantUnaLineaModelo lineaBox = null;
    String sVal;
    int iNumC;
    int iNum = 0;
    int iCond = 0;
    boolean bCond;

    padre = nodoSeleccionado;

    modoOperacion = modoESPERA;
    Inicializar();

    // determina el tipo de nodo
    if ( (padre == null) ||
        (padre == arbolModelo.getRoot())) {
      padre = arbolModelo.getRoot();
      modo = DialMantUnaLineaModelo.modoTEXTO;
      i = 0;
      sTitulo = res.getString("msg38.Text");
      iNum = lineasModelo.size() + 1;
    }
    else {
      i = iNodo(padre);
      linea = (DataMantLinea) lineasModelo.elementAt(i - 1);

      // determina el tipo de inserci�n
      if (linea.sTipo.equals("D")) {

        DialTextoPregunta dialTipo = new DialTextoPregunta(imgs.getImage(4),
            getCApp());
        dialTipo.show();

        // nuevo bloque
        if (dialTipo.bInsertarTexto()) {
          sTitulo = res.getString("msg39.Text");
          modo = DialMantUnaLineaModelo.modoTEXTO;
          iNum = i + iNumeroNodosHijo(padre);
          // primera pregunta
        }
        else {
          sTitulo = res.getString("msg40.Text");
          modo = DialMantUnaLineaModelo.modoPREGUNTA;
        }

        // pregunta
      }
      else {
        sTitulo = res.getString("msg41.Text");
        if (linea.bCondicionada) {
          modo = DialMantUnaLineaModelo.modoPREGUNTA_CONDICIONADA;
          iCond = linea.iCondALinea;
        }
        else {
          modo = DialMantUnaLineaModelo.modoPREGUNTA;
        }
      }
    }

    // muestra el dialogo
    lineaBox = new DialMantUnaLineaModelo(this, modo, null, sTitulo, iCond, 0);
    lineaBox.show();

    // inserta el nuevo nodo
    if (lineaBox.getEstado()) {

      switch (modo) {
        // nuevo texto
        case DialMantUnaLineaModelo.modoTEXTO:
          nuevaLinea = new DataMantLinea(iNum, new String("D"), lineaBox.getDes(),
                                         lineaBox.getDesL());
          break;

          // nueva pregunta
        default:
          if (linea.bCondicionada) {
            sVal = lineaBox.getValorCond();
            iNumC = linea.iCondALinea;
            bCond = true;
          }
          else {
            sVal = "";
            iNumC = 0;
            bCond = false;
          }
          iNum = i + 1;
          if (linea.sTipo.equals("P")) {
            iNum = iNum + iNumeroNodosHijo(padre) - 1;
          }
          nuevaLinea = new DataMantLinea(iNum,
                                         new String("P"),
                                         lineaBox.getDes(),
                                         lineaBox.getDesL(),
                                         lineaBox.getPregunta(),
                                         lineaBox.getObligatoria(),
                                         bCond,
                                         iNumC,
                                         sVal);

          break;

      }

      // crea la linea
      lineasModelo.insertElementAt(nuevaLinea, iNum - 1);

      // renumera las lineas
      if (lineasModelo.size() >= iNum) {
        for (int j = iNum; j < lineasModelo.size(); j++) {
          linea = (DataMantLinea) lineasModelo.elementAt(j);
          linea.iLinea = linea.iLinea + 1;
          if (nuevaLinea.bCondicionada) {
            if ( (linea.bCondicionada) &&
                (linea.iCondALinea != nuevaLinea.iCondALinea)) {
              linea.iCondALinea = linea.iCondALinea + 1;
            }
          }
          else {
            if (linea.bCondicionada) {
              linea.iCondALinea = linea.iCondALinea + 1;
            }
          }
        }
      }

      // regenera el arbol
      GenerarArbol();
    }

    lineaBox = null;
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // insertar nodo condicionado
  public void InsertarNodoCondicionado() {
    GraphLocation padre;
    DataMantLinea nuevaLinea = null;
    DataMantLinea linea = null;
    int i = 0;
    String sTitulo;
    DialMantUnaLineaModelo lineaBox = null;

    modoOperacion = modoESPERA;
    Inicializar();

    padre = nodoSeleccionado;

    // determina el tipo de nodo
    if ( (nodoSeleccionado != null) &&
        (nodoSeleccionado != arbolModelo.getRoot())) {

      i = iNodo(padre); // numero de linea posicionado
      //# System_Out.println(i);

      if ( ( (DataMantLinea) lineasModelo.elementAt(i - 1)).sTipo.equals("P")) {

        // muestra el dialogo
        lineaBox = new DialMantUnaLineaModelo(this,
                                              DialMantUnaLineaModelo.
                                              modoPREGUNTA_CONDICIONADA,
                                              null,
                                              res.getString("msg42.Text"),
                                              i, 0);
        lineaBox.show();

        // inserta el nuevo nodo
        if (lineaBox.getEstado()) {
          nuevaLinea = new DataMantLinea(i,
                                         new String("P"),
                                         lineaBox.getDes(),
                                         lineaBox.getDesL(),
                                         lineaBox.getPregunta(),
                                         lineaBox.getObligatoria(),
                                         true,
                                         i - 1,
                                         lineaBox.getValorCond());

          // crea la linea
          lineasModelo.insertElementAt(nuevaLinea, i);

          // renumera las lineas
          if (lineasModelo.size() > i) {
            for (int j = i; j < lineasModelo.size(); j++) {
              linea = (DataMantLinea) lineasModelo.elementAt(j);
              linea.iLinea = linea.iLinea + 1;
              if (linea.bCondicionada) {
                linea.iCondALinea = linea.iCondALinea + 1;
              }
            }
          }

          // regenera el arbol
          GenerarArbol();
        }
      }
    }
    lineaBox = null;
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // actualizar nodo
  public void ActualizarNodo(int tipo) {
    DataMantLinea linea = null;
    int i;
    int modo;
    String sTitulo;
    DialMantUnaLineaModelo lineaBox = null;
    int iCond = 0;

    modoOperacion = modoESPERA;
    Inicializar();

    if ( (nodoSeleccionado != null) &&
        (nodoSeleccionado != arbolModelo.getRoot())) {

      i = iNodo(nodoSeleccionado);
      linea = (DataMantLinea) lineasModelo.elementAt(i - 1);

      if (tipo == 0) { // modo actualizacion

        // determina el tipo de nodo
        if (linea.sTipo.equals("D")) {
          modo = DialMantUnaLineaModelo.modoTEXTO;
          sTitulo = res.getString("msg43.Text");
        }
        else {
          if (linea.bCondicionada) {
            if (sIndicadorRespuestas.equals("S")) {
              modo = DialMantUnaLineaModelo.modoPREGUNTA_CONDICIONADA_PROTEGIDO;
            }
            else {
              modo = DialMantUnaLineaModelo.modoPREGUNTA_CONDICIONADA;
            }
            iCond = linea.iCondALinea;
          }
          else {
            if (sIndicadorRespuestas.equals("S")) {
              modo = DialMantUnaLineaModelo.modoPREGUNTA_PROTEGIDO;
            }
            else {
              modo = DialMantUnaLineaModelo.modoPREGUNTA;
            }
          }
          sTitulo = res.getString("msg44.Text");
        }
      }
      else { // modo consulta
        // determina el tipo de nodo
        if (linea.sTipo.equals("D")) {
          modo = DialMantUnaLineaModelo.modoTEXTO;
          sTitulo = res.getString("msg45.Text");
        }
        else {
          if (linea.bCondicionada) {
            if (sIndicadorRespuestas.equals("S")) {
              modo = DialMantUnaLineaModelo.modoPREGUNTA_CONDICIONADA_PROTEGIDO;
            }
            else {
              modo = DialMantUnaLineaModelo.modoPREGUNTA_CONDICIONADA;
            }
            iCond = linea.iCondALinea;
          }
          else {
            if (sIndicadorRespuestas.equals("S")) {
              modo = DialMantUnaLineaModelo.modoPREGUNTA_PROTEGIDO;
            }
            else {
              modo = DialMantUnaLineaModelo.modoPREGUNTA;
            }
          }
          sTitulo = res.getString("msg46.Text");
        }
      }

      // muestra el dialogo
      lineaBox = new DialMantUnaLineaModelo(this, modo, linea, sTitulo, iCond,
                                            tipo);
      lineaBox.show();

      // actualiza el nodo
      if (lineaBox.getEstado()) {

        switch (modo) {
          // texto
          case DialMantUnaLineaModelo.modoTEXTO:
            linea.sDescGeneral = lineaBox.getDes();
            linea.sDescLocal = lineaBox.getDesL();
            break;

            // pregunta
          default:
            linea.sDescGeneral = lineaBox.getDes();
            linea.sDescLocal = lineaBox.getDesL();
            linea.sCodPregunta = lineaBox.getPregunta();
            linea.bObligatoria = lineaBox.getObligatoria();
            linea.sCondAValor = lineaBox.getValorCond();
            break;
        }

        // regenera el arbol
        GenerarArbol();
      }
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    lineaBox = null;
  }

  // aceptar
  public void Aceptar() {
    CMessage msgBox = null;
    CLista data = null;
    DataMantLineasModelo datModelo;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      data = new CLista();
      datModelo = new DataMantLineasModelo(sCodModelo, app.getTSive());
      datModelo.setLineas(lineasModelo);
      data.addElement(datModelo);
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

      stubCliente.doPost(servletMODIFICAR, data);
      /*
             SrvModelo srv= new SrvModelo();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                             "dba_edo",
                             "manager");
             srv.doDebug(servletMODIFICAR, data);
       */
      modoOperacion = modoNORMAL;
      Inicializar();

      // error en el proceso
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // cancelar
  public void Cancelar() {
    dispose();
  }

  void pnlArbol_mouseClicked(MouseEvent e) {
    int i = -1;
    if (e.getClickCount() == 2) {
      ////# System_Out.println("Doble click");
      i = iNodo(nodoSeleccionado);
      ////# System_Out.println("nodo: "+Integer.toString(i));
      if (sIndicadorRespuestas.equals("S")) {
        ActualizarNodo(2);
      }
      else {
        ActualizarNodo(0);
      }
    }
  }

} // fin de clase

// seleccion de un nodo
class selectionAdapter
    extends com.borland.jbcl.model.GraphSelectionAdapter {
  PanelModelo adaptee;

  public selectionAdapter(PanelModelo adaptee) {
    this.adaptee = adaptee;
  }

  // graba el nodo seleccionado
  public void selectionChanged(GraphSelectionEvent e) {
    GraphLocation[] selections;
    int i;

    // copia el nodo seleccionado
    if (e.getSelection().getCount() > 0) {
      selections = e.getSelection().getAll();
      adaptee.nodoSeleccionado = selections[0];
      i = adaptee.iNodo(selections[0]);

    }
    else {
      adaptee.nodoSeleccionado = null;
      adaptee.arbolModelo.setSelection(new SingleGraphSelection(adaptee.
          arbolModelo.getRoot()));
      i = 0;
    }
  }
}

// action listener de evento en botones
class btnActionListener
    implements ActionListener, Runnable {
  PanelModelo adaptee = null;
  ActionEvent e = null;

  public btnActionListener(PanelModelo adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if (e.getActionCommand().equals("aceptar")) { // aceptar
      adaptee.Aceptar();
    }
    else if (e.getActionCommand().equals("cancelar")) { // cancelar
      adaptee.Cancelar();
    }
    if (e.getActionCommand().equals("insertar")) { // insertar
      adaptee.InsertarNodo();
    }
    else if (e.getActionCommand().equals("insertarc")) { // insertarc
      adaptee.InsertarNodoCondicionado();
    }
    else if (e.getActionCommand().equals("modificar")) { // modificar
      adaptee.ActualizarNodo(0);
    }
    else if (e.getActionCommand().equals("borrar")) { // borrar
      adaptee.EliminarNodo();
//    else if (e.getActionCommand().equals("pregunta")) // insertar pregunta
//      adaptee.InsertarPregunta();
//    else if (e.getActionCommand().equals("lista")) // insertar lista de valores
//      adaptee.InsertarLista();
    }
  }
}

class PanelModelo_pnlArbol_mouseAdapter
    extends java.awt.event.MouseAdapter {
  PanelModelo adaptee;

  PanelModelo_pnlArbol_mouseAdapter(PanelModelo adaptee) {
    this.adaptee = adaptee;
  }

  public void mouseClicked(MouseEvent e) {
    adaptee.pnlArbol_mouseClicked(e);
  }
}
