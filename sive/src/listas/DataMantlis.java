package listas;

import java.io.Serializable;

import capp.CLista;

public class DataMantlis
    implements Serializable {

  protected String sCD_LISTA = "";
  protected String sDS_LISTA = null;
  protected String sDSL_LISTA = null;

  protected CLista listaValores = null;

  public DataMantlis(String cod) {
    sCD_LISTA = cod;
  }

  public DataMantlis(String cdlista, String dslista, String dsllista) {
    sCD_LISTA = cdlista;
    sDS_LISTA = dslista;

    if (dsllista != null) {
      sDSL_LISTA = dsllista;
    }
  }

  public String getCdlista() {
    return sCD_LISTA;
  }

  public String getDslista() {
    return sDS_LISTA;
  }

  public String getDsllista() {
    return sDSL_LISTA;
  }

  public void setValores(CLista l) {
    listaValores = l;
  }

  public CLista getValores() {
    return listaValores;
  }
}
