package enfedo;

import java.applet.Applet;

import java.awt.Button;
import java.awt.CardLayout;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;

public class App_MantEnferEDO
    extends Applet {
  XYLayout xYLayout1 = new XYLayout();
  boolean isStandalone = false;

  Label label1 = new Label();
  BevelPanel bevelPanel1 = new BevelPanel();
  Label label2 = new Label();
  Label label3 = new Label();
  Label label4 = new Label();
  Label label5 = new Label();
  Checkbox checkbox1 = new Checkbox();
  Checkbox checkbox2 = new Checkbox();
  Checkbox checkbox3 = new Checkbox();
  Checkbox checkbox4 = new Checkbox();
  Choice choice1 = new Choice();
  TextField textField1 = new TextField();
  Choice choice2 = new Choice();
  TextField textField2 = new TextField();
  Choice choice3 = new Choice();

  Button button1 = new Button();
  Button button3 = new Button();
  Button button4 = new Button();

  public App_MantEnferEDO() {
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void jbInit() throws Exception {
    xYLayout1.setWidth(521);
    xYLayout1.setHeight(300);
    label1.setFont(new Font("Dialog", 1, 12));
    label1.setText("Mantenimiento de Enfermedades EDO");
    label2.setText("C�digo CIE:");
    label3.setText("Enfermedad EDO CNE:");
    label4.setText("Tipo de Declaraci�n:");
    label5.setText("Se Env�a al Centro Nacional de Epidemiolog�a:");
    checkbox1.setLabel("Resumen Semanal");
    checkbox2.setLabel("Individual Semanal");
    checkbox3.setLabel("Individual Urgente");
    checkbox4.setLabel("Resumen Anual");
    choice1.setEnabled(false);
    textField1.setEnabled(false);
    textField1.setEditable(false);
    choice2.setEnabled(false);
    button1.setLabel("A�adir");
    button3.setEnabled(false);
    button3.setLabel("Modificar");
    button4.setEnabled(false);
    button4.setLabel("Borrar");
    this.setLayout(xYLayout1);
    this.add(label1, new XYConstraints(153, 0, 215, -1));
    this.add(bevelPanel1, new XYConstraints(10, 23, 501, 230));
    bevelPanel1.add(label2, new XYConstraints(12, 12, 70, -1));
    bevelPanel1.add(label3, new XYConstraints(12, 41, 131, -1));
    bevelPanel1.add(label4, new XYConstraints(12, 71, -1, -1));
    bevelPanel1.add(label5, new XYConstraints(12, 100, 260, -1));
    bevelPanel1.add(checkbox1, new XYConstraints(292, 100, -1, -1));
    bevelPanel1.add(checkbox2, new XYConstraints(292, 131, -1, -1));
    bevelPanel1.add(checkbox3, new XYConstraints(292, 163, -1, -1));
    bevelPanel1.add(checkbox4, new XYConstraints(292, 194, -1, -1));
    bevelPanel1.add(choice1, new XYConstraints(150, 13, 72, -1));
    bevelPanel1.add(textField1, new XYConstraints(229, 12, 260, -1));
    bevelPanel1.add(choice2, new XYConstraints(150, 42, 72, -1));
    bevelPanel1.add(textField2, new XYConstraints(229, 41, 260, -1));
    bevelPanel1.add(choice3, new XYConstraints(150, 72, 273, -1));
    this.add(button1, new XYConstraints(10, 259, 63, -1));
    this.add(button3, new XYConstraints(86, 259, -1, -1));
    this.add(button4, new XYConstraints(162, 259, 63, -1));

    choice3.addItem("");
    choice3.addItem("Num�rica");
    choice3.addItem("Individual");
    choice3.addItem("Num�rica e Individual");
    choice3.addItem("Num�rica con datos adicionales");
  }

  void button2_actionPerformed(ActionEvent e) {
    ( (CardLayout) getParent().getLayout()).show(getParent(),
                                                 "PAN_MantTablasPpales");
  }
}
