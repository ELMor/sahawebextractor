package enfedo;

import java.util.ResourceBundle;

import capp.CApp;

public class MantEnferEDO
    extends CApp {
  // parámetros del Servlet
  final String strSERVLET = "servlet/SrvEnfedo";
  ResourceBundle res;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  public void init() {
    super.init();
  }

  public void start() {
    CMantenimientoEDO ventana = null;
    CApp a = (CApp)this;
    res = ResourceBundle.getBundle("enfedo.Res" + this.getIdioma());
    this.setTitulo(res.getString("msg2.Text"));
    ventana = new CMantenimientoEDO(this);
    VerPanel("", ventana);
  }

}
