// 11/04/2000 (PDP) Applet que extiende de CApp utilizado
//  en el enganche de capp.CApp y capp2.CApp

package pseudocapp;

import capp.CApp;

public class Pseudocapp
    extends CApp {

  public Pseudocapp() {
    super();
  }

  public void init() {
    super.init();
    setTitulo("PSEUDOCAPP");
    this.setName("Pseudocapp");
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
  }

}
