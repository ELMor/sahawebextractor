package bipdatos;

import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.TextField;
import java.awt.event.FocusEvent;

import bidata.DataBrote;
import bidata.DataCDDS;
import bidial.DialogInvesInfor;
import com.borland.jbcl.control.TextAreaControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import obj.CFechaSimple;
import obj.CHora;

public class PanelBIDatos
    extends CPanel {

  final int modoESPERA = 2;
  ResourceBundle res;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  protected int modoOperacion = modoALTA;

  // dialog que contiene a este panel
  DialogInvesInfor dlgB;

  //scroll
  ScrollPane panelScroll = new ScrollPane();
  Panel panel = new Panel();

  //fechas y horas

  //listas

  CLista listaTNotif = new CLista();
  CLista listaMecTrans = new CLista();

  // controles
  XYLayout xYLayout = new XYLayout();

  XYLayout xYLayout1 = new XYLayout();
  XYLayout xYLayout2 = new XYLayout();

  Hashtable hash = null;

  Label lTNotificador = new Label();
  Label lMecTrans = new Label();

  Choice choTNotif = new Choice();
  CheckboxGroup checkGroup = new CheckboxGroup();
  Choice choMecTrans = new Choice();
  Label lObserv = new Label();
  TextAreaControl textAreaObserv = new TextAreaControl();
  obj.CFechaSimple fechaExp = new obj.CFechaSimple("N");
  focusAdapter focusAdap = null;
  focusAdapterHora focusAdapHora = null;

  TextField txtExp = new TextField();
  obj.CHora horaExp = new obj.CHora("N");
  Label lExp = new Label();
  Label lPersRiesgo = new Label();
  Label lIngHosp = new Label();
  TextField txtDefun = new TextField();
  Label lDefun = new Label();
  TextField txtEnf = new TextField();
  TextField txtIH = new TextField();
  Label lEnf = new Label();
  Label lFecExp = new Label();
  Label lMani = new Label();
  Label lEV = new Label();
  TextField txtMani = new TextField();
  Label lNVNE = new Label();
  TextField txtVNE = new TextField();
  Label lVNE = new Label();
  TextField txtNVNE = new TextField();
  Label lVE = new Label();
  TextField txtVE = new TextField();
  Label lNVE = new Label();
  TextField txtNVE = new TextField();

  // constructor
  public PanelBIDatos(DialogInvesInfor dlg,
                      int modo,
                      Hashtable hsCompleta) {

    try {

      setApp(dlg.getCApp());
      res = ResourceBundle.getBundle("bipdatos.Res" + getApp().getIdioma());
      dlgB = dlg;

      hash = hsCompleta;

      listaTNotif = (CLista) hsCompleta.get("TNOTIFICADOR");
      listaMecTrans = (CLista) hsCompleta.get("MECTRANS");

      jbInit();
      modoOperacion = modo;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(270); //(280);  270
    xYLayout.setWidth(735); //(745);

    xYLayout2.setHeight(320); //   320
    xYLayout2.setWidth(600);

    panelScroll.setSize(735, 270);
    panel.setBounds(new Rectangle(10, 10, 715, 315));
    panel.setSize(new Dimension(600, 400));
    panel.setLayout(xYLayout2);

    choTNotif.setName("choTNotif");
    choTNotif.setBackground(new Color(255, 255, 150));
    choMecTrans.setName("choMecTrans");
    choMecTrans.setBackground(Color.white);

    //rellenar las listas
    DataCDDS aux = null;
    for (int i = 0; i < listaTNotif.size(); i++) {
      aux = (DataCDDS) listaTNotif.elementAt(i);
      choTNotif.addItem(aux.getCD() + " " + aux.getDS());
    }

    choMecTrans.addItem(""); //no es obligatorio
    for (int i = 0; i < listaMecTrans.size(); i++) {
      aux = (DataCDDS) listaMecTrans.elementAt(i);
      choMecTrans.addItem(aux.getCD() + " " + aux.getDS());
    }

    fechaExp.setName("fechaExp");
    horaExp.setName("horaExp");
    focusAdap = new focusAdapter(this);
    focusAdapHora = new focusAdapterHora(this);
    fechaExp.addFocusListener(focusAdap);
    horaExp.addFocusListener(focusAdapHora);

    lTNotificador.setForeground(Color.black);
    lTNotificador.setFont(new Font("Dialog", 0, 12));
    lTNotificador.setText(res.getString("lTNotificador.Text"));
    lMecTrans.setForeground(Color.black);
    lMecTrans.setFont(new Font("Dialog", 0, 12));
    lMecTrans.setText(res.getString("lMecTrans.Text"));
    lObserv.setForeground(Color.black);
    lObserv.setFont(new Font("Dialog", 0, 12));
    lObserv.setText(res.getString("lObserv.Text"));
    fechaExp.setName("fechaExp");
    horaExp.setName("horaExp");
    lExp.setText(res.getString("lExp.Text"));
    lPersRiesgo.setText(res.getString("lPersRiesgo.Text"));
    lIngHosp.setText(res.getString("lIngHosp.Text"));

    lDefun.setText(res.getString("lDefun.Text"));
    lEnf.setText(res.getString("lEnf.Text"));
    lFecExp.setText(res.getString("lFecExp.Text"));
    lMani.setText(res.getString("lMani.Text"));
    lEV.setText(res.getString("lEV.Text"));
    lNVNE.setForeground(Color.black);
    lNVNE.setFont(new Font("Dialog", 0, 12));
    lNVNE.setText(res.getString("lNVNE.Text"));
    lVNE.setText(res.getString("lVNE.Text"));
    lVE.setText(res.getString("lVE.Text"));
    lNVE.setText(res.getString("lNVE.Text"));
    lNVE.setFont(new Font("Dialog", 0, 12));
    lNVE.setForeground(Color.black);
    lVE.setFont(new Font("Dialog", 0, 12));
    lVE.setForeground(Color.black);
    lVNE.setFont(new Font("Dialog", 0, 12));
    lVNE.setForeground(Color.black);
    lMani.setFont(new Font("Dialog", 0, 12));
    lMani.setForeground(Color.black);
    lEV.setFont(new Font("Dialog", 0, 12));
    lEV.setForeground(Color.black);
    lFecExp.setFont(new Font("Dialog", 0, 12));
    lFecExp.setForeground(Color.black);
    lEnf.setFont(new Font("Dialog", 0, 12));
    lEnf.setForeground(Color.black);
    lDefun.setFont(new Font("Dialog", 0, 12));
    lDefun.setForeground(Color.black);
    lIngHosp.setFont(new Font("Dialog", 0, 12));
    lIngHosp.setForeground(Color.black);
    lPersRiesgo.setFont(new Font("Dialog", 0, 12));
    lPersRiesgo.setForeground(Color.black);
    lExp.setFont(new Font("Dialog", 0, 12));
    lExp.setForeground(Color.black);

    this.setLayout(xYLayout);
//735,280gui
//730,460
    this.add(panelScroll, new XYConstraints(0, 0, 735, 270));
    panelScroll.add(panel, new XYConstraints(0, 0, 600, 320));
    panel.add(lTNotificador, new XYConstraints(9, 10, 98, -1));
    panel.add(choTNotif, new XYConstraints(116, 10, 200, -1));
    panel.add(lPersRiesgo, new XYConstraints(9, 45, 117, -1));
    panel.add(lExp, new XYConstraints(135, 45, 67, -1));
    panel.add(txtExp, new XYConstraints(202, 45, 36, -1));
    panel.add(lEnf, new XYConstraints(254, 45, 65, -1));
    panel.add(txtEnf, new XYConstraints(321, 45, 36, -1));
    panel.add(lIngHosp, new XYConstraints(377, 45, 130, -1));
    panel.add(txtIH, new XYConstraints(510, 45, 36, -1));
    panel.add(lDefun, new XYConstraints(562, 45, 77, -1));
    panel.add(txtDefun, new XYConstraints(642, 45, 36, -1));
    panel.add(lFecExp, new XYConstraints(9, 80, 108, -1));
    panel.add(fechaExp, new XYConstraints(116, 80, 84, -1));
    panel.add(horaExp, new XYConstraints(210, 80, 52, -1));
    panel.add(lMecTrans, new XYConstraints(283, 83, 217, -1));
    panel.add(choMecTrans, new XYConstraints(501, 82, 200, -1));
    panel.add(lObserv, new XYConstraints(9, 114, 113, -1));
    panel.add(textAreaObserv, new XYConstraints(9, 141, 327, 154));
    panel.add(lMani, new XYConstraints(445, 138, 130, -1));
    panel.add(lEV, new XYConstraints(445, 138, 130, -1));
    panel.add(txtMani, new XYConstraints(597, 140, 50, -1));
    panel.add(lVNE, new XYConstraints(377, 167, 152, -1));
    panel.add(txtVNE, new XYConstraints(587, 167, 50, -1));
    panel.add(lVE, new XYConstraints(377, 202, 134, -1));
    panel.add(txtVE, new XYConstraints(587, 202, 50, -1));
    panel.add(lNVNE, new XYConstraints(377, 237, 169, -1));
    panel.add(txtNVNE, new XYConstraints(587, 237, 50, -1));
    panel.add(lNVE, new XYConstraints(377, 272, 145, -1));
    panel.add(txtNVE, new XYConstraints(587, 272, 50, -1));

    setBorde(false);

  }

//devuelve el cod del notificador
  public String getNotif() {

    String iG = "";
    iG = choTNotif.getSelectedItem().trim().substring(0, 1);

    return (iG);
  } //getNotif

//devuelve el cod del mecanismo  (no es obligatorio)
  public String getMec() {

    String iG = "";
    if (choMecTrans.getSelectedIndex() == 0) {
      iG = "";
    }
    else {
      iG = choMecTrans.getSelectedItem().trim().substring(0, 1);

    }
    return (iG);
  } //getMec

  public DataBrote recogerDatos(DataBrote resul) {

    resul.insert("CD_TNOTIF", getNotif());
    resul.insert("CD_TRANSMIS", getMec()); //si vacio-> ""

    resul.insert("NM_EXPUESTOS", txtExp.getText().trim());
    resul.insert("NM_ENFERMOS", txtEnf.getText().trim());
    resul.insert("NM_INGHOSP", txtIH.getText().trim());
    resul.insert("NM_DEFUNCION", txtDefun.getText().trim());

    fechaExp_focusLost();
    resul.insert("FC_EXPOSICION_F", fechaExp.getText().trim());
    horaExp_focusLost();
    resul.insert("FC_EXPOSICION_H", horaExp.getText().trim());

    resul.insert("DS_OBSERV", textAreaObserv.getText().trim());

    /*
          0 - Toxico-Alimentarios
          1 - Vacunables
          2 - Otras
     */

    String auxGrupo = dlgB.panelSuperior.getGrupo().trim();

    if (auxGrupo.equals("1")) { //vacunables
      resul.insert("NM_NVACENF", txtNVE.getText().trim());
      resul.insert("NM_NVACNENF", txtNVNE.getText().trim());
      resul.insert("NM_VACENF", txtVE.getText().trim());
      resul.insert("NM_VACNENF", txtVNE.getText().trim());
      resul.insert("NM_MANIPUL", "");
    }
    else if (auxGrupo.equals("0")) { //toxico-alimentario
      resul.insert("NM_MANIPUL", txtMani.getText().trim());
      resul.insert("NM_NVACENF", "");
      resul.insert("NM_NVACNENF", "");
      resul.insert("NM_VACENF", "");
      resul.insert("NM_VACNENF", "");
    }
    else {
      resul.insert("NM_MANIPUL", "");
      resul.insert("NM_NVACENF", "");
      resul.insert("NM_NVACNENF", "");
      resul.insert("NM_VACENF", "");
      resul.insert("NM_VACNENF", "");

    }

    return resul;
  }

  public boolean validarDatos() {

    boolean correcto = true;

    //1. perdidas de foco
    //2. longitudes
    if (txtExp.getText().length() > 4) {
      ShowError(res.getString("msg.1"));
      txtExp.setText("");
      txtExp.requestFocus();
      return false;
    }
    if (txtEnf.getText().length() > 4) {
      ShowError(res.getString("msg.2"));
      txtEnf.setText("");
      txtEnf.requestFocus();
      return false;
    }
    if (txtIH.getText().length() > 4) {
      ShowError(res.getString("msg.3"));
      txtIH.setText("");
      txtIH.requestFocus();
      return false;
    }
    if (txtDefun.getText().length() > 4) {
      ShowError(res.getString("msg.4"));
      txtDefun.setText("");
      txtDefun.requestFocus();
      return false;
    }

    if (txtVNE.getText().length() > 4) {
      ShowError(res.getString("msg.5"));
      txtVNE.setText("");
      this.requestFocus();
      txtVNE.requestFocus();
      return false;
    }
    if (txtVE.getText().length() > 4) {
      ShowError(res.getString("msg.6"));
      txtVE.setText("");
      this.requestFocus();
      txtVE.requestFocus();
      return false;
    }
    if (txtNVNE.getText().length() > 4) {
      ShowError(res.getString("msg.7"));
      txtNVNE.setText("");
      this.requestFocus();
      txtNVNE.requestFocus();
      return false;
    }
    if (txtNVE.getText().length() > 4) {
      ShowError(res.getString("msg.8"));
      txtNVE.setText("");
      this.requestFocus();
      txtNVE.requestFocus();
      return false;
    }

    if (txtMani.getText().length() > 4) {
      ShowError(res.getString("msg.9"));
      txtMani.setText("");
      this.requestFocus();
      txtMani.requestFocus();
      return false;
    }

    if (textAreaObserv.getText().length() > 1000) {
      ShowError(res.getString("msg.10"));
      textAreaObserv.setText("");
      textAreaObserv.requestFocus();
      return false;
    }

    // 3. Datos numericos
    if (!txtMani.getText().equals("")) {
      if (!isNum(txtMani)) {
        ShowError(res.getString("msg.11"));
        txtMani.requestFocus();
        return false;
      }
    }
    if (!txtVNE.getText().equals("")) {
      if (!isNum(txtVNE)) {
        ShowError(res.getString("msg.12"));
        txtVNE.requestFocus();
        return false;
      }
    }
    if (!txtVE.getText().equals("")) {
      if (!isNum(txtVE)) {
        ShowError(res.getString("msg.13"));
        txtVE.requestFocus();
        return false;
      }
    }
    if (!txtNVNE.getText().equals("")) {
      if (!isNum(txtNVNE)) {
        ShowError(res.getString("msg.14"));
        txtNVNE.requestFocus();
        return false;
      }
    }
    if (!txtNVE.getText().equals("")) {
      if (!isNum(txtNVE)) {
        ShowError(res.getString("msg.15"));
        txtNVE.requestFocus();
        return false;
      }
    }
    if (!txtExp.getText().equals("")) {
      if (!isNum(txtExp)) {
        ShowError(res.getString("msg.16"));
        txtExp.requestFocus();
        return false;
      }
    }
    if (!txtEnf.getText().equals("")) {
      if (!isNum(txtEnf)) {
        ShowError(res.getString("msg.17"));
        txtEnf.requestFocus();
        return false;
      }
    }
    if (!txtIH.getText().equals("")) {
      if (!isNum(txtIH)) {
        ShowError(res.getString("msg.18"));
        txtIH.requestFocus();
        return false;
      }
    }
    if (!txtDefun.getText().equals("")) {
      if (!isNum(txtDefun)) {
        ShowError(res.getString("msg.19"));
        txtDefun.requestFocus();
        return false;
      }
    }

    //3. Obligatorios:  notificador (la combo lo obliga)
    if (getNotif().trim().equals("")) {
      ShowError(res.getString("msg.20"));
      this.requestFocus();
      choTNotif.requestFocus();
      return false;
    }

    return correcto;

  }

  //Validar Numericos
  public boolean isNum(TextField txt) {

    boolean b = true; //Es Numerico

    try {
      Integer f = new Integer(txt.getText());
      int i = f.intValue();
      if (i < 0) {
        b = false;

      }
    }
    catch (Exception e) {
      b = false;
    }
    return b;
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
      case modoMODIFICACION:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
        enableControls(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  private void enableControls(boolean en) {

    choTNotif.setEnabled(en);
    txtExp.setEnabled(en);
    txtEnf.setEnabled(en);
    txtIH.setEnabled(en);
    txtDefun.setEnabled(en);
    fechaExp.setEnabled(en);
    horaExp.setEnabled(en);
    choMecTrans.setEnabled(en);
    textAreaObserv.setEnabled(en);
    habilitaEV_Manip(dlgB.panelSuperior.getGrupo().trim(), en);
  }

  public void habilitaEV_Manip(String cdGrupo, boolean en) {

    /*
      0 - Toxico-Alimentarios
      1 - Vacunables
      2 - Otras
     */
    if (cdGrupo.equals("0")) { //manipuladores

      //manipuladores
      lEV.setVisible(false);
      lVNE.setVisible(false);
      lVE.setVisible(false);
      lNVNE.setVisible(false);
      lNVE.setVisible(false);

      txtVNE.setVisible(false);
      txtVE.setVisible(false);
      txtNVNE.setVisible(false);
      txtNVE.setVisible(false);

      txtVNE.setText("");
      txtVE.setText("");
      txtNVNE.setText("");
      txtNVE.setText("");

      lMani.setVisible(true);
      txtMani.setVisible(true);

      txtMani.setEnabled(en);
      txtVNE.setEnabled(en);
      txtVE.setEnabled(en);
      txtNVNE.setEnabled(en);
      txtNVE.setEnabled(en);

    }
    else if (cdGrupo.equals("1")) { //VACUNABLES

      //vacunables

      lMani.setVisible(false);
      txtMani.setVisible(false);

      lEV.setVisible(true);
      lVNE.setVisible(true);
      lVE.setVisible(true);
      lNVNE.setVisible(true);
      lNVE.setVisible(true);
      txtVNE.setVisible(true);
      txtVE.setVisible(true);
      txtNVNE.setVisible(true);
      txtNVE.setVisible(true);

      txtMani.setText("");

      txtMani.setEnabled(en);
      txtVNE.setEnabled(en);
      txtVE.setEnabled(en);
      txtNVNE.setEnabled(en);
      txtNVE.setEnabled(en);

    }
    else { //otros

      //otros

      lMani.setVisible(false);
      txtMani.setVisible(false);

      lEV.setVisible(false);
      lVNE.setVisible(false);
      lVE.setVisible(false);
      lNVNE.setVisible(false);
      lNVE.setVisible(false);
      txtVNE.setVisible(false);
      txtVE.setVisible(false);
      txtNVNE.setVisible(false);
      txtNVE.setVisible(false);

      txtVNE.setText("");
      txtVE.setText("");
      txtNVNE.setText("");
      txtNVE.setText("");
      txtMani.setText("");

      txtMani.setEnabled(en);
      txtVNE.setEnabled(en);
      txtVE.setEnabled(en);
      txtNVNE.setEnabled(en);
      txtNVE.setEnabled(en);

    }

  }

//dado un cod, situar la Choice en ese registro
//(es obligatorio-> select (i))
  public void selectNotif(String cd) {
    if (cd.equals("")) {
      choTNotif.select(0);
    }
    else {
      for (int i = 0; i < listaTNotif.size(); i++) {
        DataCDDS data = (DataCDDS) listaTNotif.elementAt(i);
        if (data.getCD().trim().equals(cd.trim())) {
          choTNotif.select(i);
          break;
        }
      }
    }
  }

//dado un cod, situar la Choice en ese registro
//(NO es obligatorio-> select (i +1))
  public void selectMec(String cd) {
    if (cd.equals("")) {
      choMecTrans.select(0);
    }
    else {
      for (int i = 0; i < listaMecTrans.size(); i++) {
        DataCDDS data = (DataCDDS) listaMecTrans.elementAt(i);
        if (data.getCD().trim().equals(cd.trim())) {
          choMecTrans.select(i + 1);
          break;
        }
      }
    }
  }

  public void rellena(DataBrote data) {

    String cdTNotif = "";
    String cdMecTrans = "";
    String dsObserv = "";

    if (data.getCD_TNOTIF() != null) {
      cdTNotif = data.getCD_TNOTIF();
    }
    selectNotif(cdTNotif);

    if (data.getCD_TNOTIF() != null) {
      cdMecTrans = data.getCD_TNOTIF();
    }
    selectMec(cdMecTrans);

    if (data.getDS_OBSERV() != null) {
      dsObserv = data.getDS_OBSERV();
    }
    textAreaObserv.setText(dsObserv);

    String fcExpF = "";
    String fcExpH = "";
    if (data.getFC_EXPOSICION_F() != null) {
      fcExpF = data.getFC_EXPOSICION_F();
    }
    fechaExp.setText(fcExpF);

    if (data.getFC_EXPOSICION_H() != null) {
      fcExpH = data.getFC_EXPOSICION_H();
    }
    horaExp.setText(fcExpH);

    String nmExp = "";
    String nmEnf = "";
    String nmIH = "";
    String nmDef = "";
    if (data.getNM_EXPUESTOS() != null) {
      nmExp = data.getNM_EXPUESTOS();
    }
    txtExp.setText(nmExp);

    if (data.getNM_ENFERMOS() != null) {
      nmEnf = data.getNM_ENFERMOS();
    }
    txtEnf.setText(nmEnf);

    if (data.getNM_INGHOSP() != null) {
      nmIH = data.getNM_INGHOSP();
    }
    txtIH.setText(nmIH);

    if (data.getNM_DEFUNCION() != null) {
      nmDef = data.getNM_DEFUNCION();
    }
    txtDefun.setText(nmDef);

    String nmManip = "";
    if (data.getNM_MANIPUL() != null) {
      nmManip = data.getNM_MANIPUL();
    }
    txtMani.setText(nmManip);

    String nmVNE = "";
    String nmVE = "";
    String nmNVNE = "";
    String nmNVE = "";

    if (data.getNM_VACNENF() != null) {
      nmVNE = data.getNM_VACNENF();
    }
    txtVNE.setText(nmVNE);

    if (data.getNM_VACENF() != null) {
      nmVE = data.getNM_VACENF();
    }
    txtVE.setText(nmVE);

    if (data.getNM_NVACNENF() != null) {
      nmNVNE = data.getNM_NVACNENF();
    }
    txtNVNE.setText(nmNVNE);

    if (data.getNM_NVACENF() != null) {
      nmNVE = data.getNM_NVACENF();
    }
    txtNVE.setText(nmNVE);

  } //fin

  private void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  private void ShowError(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgERROR, sMessage);
    msgBox.show();
    msgBox = null;
  }

  void fechaExp_focusLost() {
    fechaExp.ValidarFecha(); //validamos el dato
    if (fechaExp.getValid().equals("N")) {
      //opcional vaciarlo
      fechaExp.setText(fechaExp.getFecha());
      horaExp.setText("");
    }
    else if (fechaExp.getValid().equals("S")) {
      fechaExp.setText(fechaExp.getFecha());
      if (horaExp.getText().equals("")) {
        horaExp.setText("00:00");
      }
    }
  }

  void horaExp_focusLost() {
    horaExp.ValidarFecha(); //validamos el dato
    if (horaExp.getValid().equals("N")) {
      //opcional vaciarlo
      horaExp.setText(horaExp.getFecha());
    }
    else if (horaExp.getValid().equals("S")) {
      horaExp.setText(horaExp.getFecha());
    }
  }

} //clase

class focusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanelBIDatos adaptee;
  FocusEvent evt;

  focusAdapter(PanelBIDatos adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {
    if ( ( (CFechaSimple) evt.getSource()).getName().equals("fechaExp")) {
      adaptee.fechaExp_focusLost();
    }
  }

}

class focusAdapterHora
    implements java.awt.event.FocusListener, Runnable {
  PanelBIDatos adaptee;
  FocusEvent evt;

  focusAdapterHora(PanelBIDatos adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {

    if ( ( (CHora) evt.getSource()).getName().equals("horaExp")) {
      adaptee.horaExp_focusLost();
    }
  }

}
