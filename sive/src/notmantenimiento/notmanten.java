package notmantenimiento;

import java.util.ResourceBundle;

import capp.CApp;

public class notmanten
    extends CApp {

  ResourceBundle res;

  public void init() {
    super.init();

  }

  public void start() {
    res = ResourceBundle.getBundle("notmantenimiento.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    PanelBusca miPanel = new PanelBusca(a);
    VerPanel("", miPanel);
  }

}