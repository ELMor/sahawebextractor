
package carga;

import java.io.Serializable;

public class DataCarga
    implements Serializable {

  String sCodTab = ""; //Tabla usada
  String sCodVer = ""; //C�digo de la versi�n usada
  String sCodOpe = ""; //Operador
  String sItOk = ""; //Indica si todo ha ido bien
  String sNomFic = ""; //Nombre del fichero usado para carga

  public DataCarga() {
  }

  public DataCarga(String codTab, String codVer, String codOpe, String ok,
                   String nomFic) {
    sCodTab = codTab;
    sCodVer = codVer;
    sCodOpe = codOpe;
    sItOk = ok;
    sNomFic = nomFic;
  }

}
