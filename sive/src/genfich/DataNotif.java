
package genfich;

import java.io.Serializable;

public class DataNotif
    implements Serializable {

  //DAtos de tabla SIVE_NOTIF_EDO
  protected String sCodEquNot = "";
  protected String sCodAno = "";
  protected String sCodSem = "";
  protected java.sql.Date dFecRec;
  protected int iNotRea = 0;
  protected java.sql.Date dFecNot;
  protected String sResSem = "";

  //DAtos de tabla SIVE_EDONUM
  protected int iNumCas = 0;

  //DAtos de tabla SIVE_EDOIND
  protected int iNumEdo = 0;

//Datos de tablas SIVE_E_NOTIF y SIVE_C_NOTIF
  protected String sCodCenNot = "";
  protected String sCodNiv1 = "";
  protected String sCodNiv2 = "";
  protected String sCodZbs = "";
  protected String sCodPro = "";
  protected String sCodMun = "";

  public DataNotif() {
  }

  public DataNotif(String equNot, String codAno, String codSem,
                   java.sql.Date fecRec,
                   int notRea, java.sql.Date fecNot, String resSem) {

    sCodEquNot = equNot;
    sCodAno = codAno;
    sCodSem = codSem;
    dFecRec = fecRec;
    iNotRea = notRea;
    dFecNot = fecNot;
    sResSem = resSem;

  }

  public void setNumCas(int numCas) {
    iNumCas = numCas;
  }

  public void setNumEdo(int numEdo) {
    iNumEdo = numEdo;
  }

  public void setCenNot(String cenNot, String codNiv1, String codNiv2,
                        String codZbs) {
    sCodCenNot = cenNot;
    sCodNiv1 = codNiv1;
    sCodNiv2 = codNiv2;
    sCodZbs = codZbs;
  }

  public void setProMun(String codPro, String codMun) {
    sCodPro = codPro;
    sCodMun = codMun;
  }

  // String codPro, String codMun

  //Cod util para replicar: datos obligatorios

  public String getCodEquNot() {
    return sCodEquNot;
  }

  public String getCodAno() {
    return sCodAno;
  }

  public String getCodSem() {
    return sCodSem;
  }

  public java.sql.Date getFecRec() {
    return dFecRec;
  }

  public int getNotRea() {
    return iNotRea;
  }

  public java.sql.Date getFecNot() {
    return dFecNot;
  }

  public String getResSem() {
    return sResSem;
  }

  public String getCodCenNot() {
    return sCodCenNot;
  }

  public String getCodNiv1() {
    return sCodNiv1;
  }

  public String getCodNiv2() {
    return sCodNiv2;
  }

  public String getCodZbs() {
    return sCodZbs;
  }

  public String getCodPro() {
    return sCodPro;
  }

  public String getCodMun() {
    return sCodMun;
  }

  public int getNumCas() {
    return iNumCas;
  }

  public int getNumEdo() {
    return iNumEdo;
  }

}
