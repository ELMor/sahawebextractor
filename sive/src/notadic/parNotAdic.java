//Title:        Notificacion datos adicionales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Notificacion de enfermedades EDO de declaracion numerica
//que necesitan datos adicionales.

package notadic;

import java.io.Serializable;

public class parNotAdic
    implements Serializable {

  final int modoNORMAL = 0;
  final int modoVISUAL = 2;

  public int modo = 1;

  public String ds_enf = "";
  public int parteIni = 0;
  public int totalCasos = 0;

  //info de la declaracion
  public String cd_enf = "";
  public String equipo = "";
  public String ano = "";
  public String semana = "";
  public String fRecep = "";
  public String fNotif = "";
  //public String ope = "";
  //public String fUltAct = "";

  //info para el protocolo
  public String nivel1 = "";
  public String nivel2 = "";
  public String Ca = "";

  public parNotAdic() {
  }

  public parNotAdic(int m, String cd, String ds, int c, int p,
                    String a, String s, String e,
                    String fn, String fr, //String op, String fu,
                    String n1, String n2, String ca) {

    modo = m;
    parteIni = p;
    totalCasos = c;

    cd_enf = cd;
    ds_enf = ds;
    ano = a;
    semana = s;
    equipo = e;
    fNotif = fn;
    fRecep = fr;
    //ope = op;
    //fUltAct = fu;

    nivel1 = n1;
    nivel2 = n2;
    Ca = ca;
  }

}