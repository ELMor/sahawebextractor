//Title:        Notificacion datos adicionales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Notificacion de enfermedades EDO de declaracion numerica
//que necesitan datos adicionales.

package notadic;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Label;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import infprotoedo.CButtonControl;
import infprotoedo.CChoice;
import infprotoedo.CTextField;
import infprotoedo.DataEnfEnt;
import infprotoedo.DataLayout;
import infprotoedo.DataLineasM;
import infprotoedo.DataProtocolo;
import sapp.StubSrvBD;

//Los datos de entrada: Enefermedad y niveles,
//hay que meterlos, en getEnfermedad y en IraBuscarDatos y
//Componentes(N1, N2)
//NOTA: espero que me pasen la descripcion de la Enfermedad tb
//CASO: detras de setProtocolo!!!!!
public class panelInfNotAdic
    extends CPanel {

  final int modoESPERA = 2;
  ResourceBundle resour;
  final int servletADIC = 0;
  final public int modoVISUAL = 2;

  public boolean bPulsarAceptarOCancelar = false;
  public String NIVEL = "";

  public CLista listadatosReservada = null;
  public Vector listaValoresReservada = null; //vector de vectores

  protected StubSrvBD stub = null;
  public CLista listadatos = null; //para el LAYOUT
  protected Vector listaValores = null;
  protected CLista listaInsertar = null;
  protected Object CompPerdioFoco = new Object();

  protected boolean NoSeEncontraronDatos = false;

  protected int Num_Grupos = 0;
  protected int Num_Choices = 0;
  protected int Num_Cajas = 0;
  protected int Num_Botones = 0;
  protected int Num_Label_Cajas = 0;
  protected int Num_Label_Choices = 0;
  protected int Num_Descripciones = 0;

  public datosParte parteValido = null;
  protected dialogoNotAdic dialogo = null;

  ButtonControl button1 = null;
  ButtonControl button2 = null;

  protected String sCod = "";
  protected String sDes = "";
  protected String sNivel1 = "";
  protected String sNivel2 = "";
  protected String sCa = "";

  protected int totalCasos = 0;
  protected String sEq = "";
  protected String sAno = "";
  protected String sSem = "";
  protected String sFR = "";
  protected String sFN = "";
  protected String sOpe = "";
  protected String sFU = "";
  protected String sSec = "";
  protected String sTSive = "";

  CApp applet = null;

  // modos de operaci�n Servlet
  // inserciones en sive_edo_dadic y sive_resp_adic
  final int servletALTA = 0;
  // borrado e insercion sive_resp_adic y sive_edo_dadic
  final int servletMODIFICAR = 1;
  // recuperacion de los partes
  final int servletPARTES = 2;
  // modificacion con comprobacion
  final int servletCOMP_MOD = 3;
  // borrado con comprobacion
  final int servletCOMP_BORR = 4;
  // modificacion
  final int servletMODIFICACION = 5;
  //borrado
  final int servletBORRADO = 6;

  final int servletUN_PARTE = 7;
  //cuando se actualiza el protocolo comprueba si se
  //deben guardar todos los partes
  final int servletACTUALIZAR_PARTES = 8;
  final int servletCOMP_ACTUALIZAR_PARTES = 9;

  final int servletRECARGAR_PARTES = 10;

  final int servletVERACTIVOS = 0;

  final String strSERVLET = "servlet/SrvProtocolo";
  final String strSERVLET_RESP = "servlet/SrvNotAdic";
  final String strSERVLET_ACTIVO_OBSOLETO = "servlet/SrvActualizarProt";

  final int Ancho_Panel = 550; //antes 600
  final int Ancho_GroupBox = Ancho_Panel - 5;
  final int Ancho_Label_Caja = Ancho_GroupBox / 2;

  //modos del panel
  final int modoCondicionadaDeshabilitar = 0;
  final int modoRevisarCondicionadas = 1;
  final int modoCargarPlantilla = 2;
  final int modoPerdidaFoco = 3;
  protected int modoOperacion = modoCondicionadaDeshabilitar;

  //Estados del panel
  final int ALTA = 0;
  final int MODIFICACION = 1;
  protected int iEstado = ALTA;

  // controles
  XYLayout xYLayout1 = null;

  protected Vector lblT = new Vector();
  protected Vector lblC = new Vector();
  protected Vector Des = new Vector(); //Sobretitulos
  protected Vector cbo = new Vector();
  protected Vector txt = new Vector();
  protected Vector gb = new Vector();
  protected Vector btn = new Vector();

  //clase escuchadora de perdida de foco e itemchange para choices
  Foco objLostFocus = new Foco(this);
  Item objItemListener = new Item(this);

  public void cambiarEstado(int modo) {
    iEstado = modo;
  }

  // configura los controles condicionados a InHabilitados
  public void Inicializar() {

    switch (modoOperacion) {

      case modoCondicionadaDeshabilitar:

        //CHOICES
        CChoice cboEtiqueta;
        for (int i = 0; i < Num_Choices; i++) {
          cboEtiqueta = (CChoice) cbo.elementAt(i);

          if (cboEtiqueta.getCondicionada().equals("S")) {
            cboEtiqueta.setEnabled(false);
          }
        }

        //CAJAS
        CTextField txtEtiqueta;
        for (int i = 0; i < Num_Cajas; i++) {
          txtEtiqueta = (CTextField) txt.elementAt(i);

          if (txtEtiqueta.getCondicionada().equals("S")) {
            txtEtiqueta.setEnabled(false);
            txtEtiqueta.setEditable(false);

          }
        }
        break;
      case modoRevisarCondicionadas:
        Revision();
        break;
      case modoPerdidaFoco:
        Revision_del_que_pierde_el_foco();
        break;
      case modoCargarPlantilla:
        CargarPlantilla();
        break;
    } //fin switch

  } //fin Inicializar

  // contructor
  public panelInfNotAdic(CApp a, datosParte par, int casos,
                         dialogoNotAdic dial) {
    try {
      setApp( (CApp) a);
      applet = a;
      resour = ResourceBundle.getBundle("notadic.Res" + a.getIdioma());
      setDatosParte(par);

      totalCasos = casos;
      parteValido = par;
      dialogo = dial;

      stub = new StubSrvBD();
      listadatos = new CLista();
      listaInsertar = new CLista();
      listaValores = new Vector();

      listaValores = par.preguntas;

      Num_Grupos = 0;
      Num_Choices = 0;
      Num_Cajas = 0;
      Num_Botones = 0;
      Num_Label_Cajas = 0;
      Num_Label_Choices = 0;

      boolean hayProto = false;
      hayProto = IraBuscarDatosLayout(); //ya cargamos las dos listas
      Pintar_Valores();

      if (!hayProto) {
        dialogo.cerrar = true;
      }

      xYLayout1 = new XYLayout();

      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception {
    //NO HAY LAYOUT
    if (listadatos == null) {

      xYLayout1.setHeight(700);
      xYLayout1.setWidth(Ancho_Panel); //550
      this.setLayout(xYLayout1);
      this.doLayout(); //sino no sale!!

    }
    //EXISTE LAYOUT
    else {
      //layout
      //antes, pero lo dejo
      this.setLayout(xYLayout1);

      int iTam = Componentes();

      //Boton Reset------------------
      CCargadorImagen imgs = null;
      final String imgNAME[] = {
          "images/limpiar2.gif", "images/refrescar.gif"};

      imgs = new CCargadorImagen(applet, imgNAME);
      imgs.CargaImagenes();

      button1 = new ButtonControl();
      this.add(button1,
               new XYConstraints(40, iTam + 5, -1, -1));
      button1.setImage(imgs.getImage(0));
      button1.setLabel(resour.getString("button1.Label"));
      button1.setEnabled(false);
      button1.addActionListener(new panelInfNotAdic_button1_actionAdapter(this));

      button2 = new ButtonControl();
      this.add(button2,
               new XYConstraints(200, iTam + 5, -1, -1));
      button2.setImage(imgs.getImage(1));
      button2.setLabel(resour.getString("button2.Label"));
      button2.addActionListener(new panelInfNotAdic_button2_actionAdapter(this));

      iTam = iTam + 5;
      //------------------------------

      //mlm: de momento lo quitamos
      button2.setVisible(false);
      //------------------------

      xYLayout1.setHeight(iTam + 10 + 10 + 10);
      xYLayout1.setWidth(Ancho_Panel); //550
      this.setLayout(xYLayout1);
      this.doLayout(); //sino no sale!!

      if (listaValores.size() == 0) {
        iEstado = ALTA;
        modoOperacion = modoCondicionadaDeshabilitar;
        Inicializar();
        this.doLayout();
      }
      else {
        iEstado = MODIFICACION;
        modoOperacion = modoCondicionadaDeshabilitar;
        Inicializar();
        modoOperacion = modoCargarPlantilla;
        Inicializar();
        this.doLayout();

      }
    }

  }

  protected void deshabilitarCampos(int cur) {

    setCursor(new Cursor(cur));
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    CButtonControl btnEtiqueta;

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      txtEtiqueta.setEnabled(false);
      txtEtiqueta.setEditable(false);
    } //cajas

    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      cboEtiqueta.setEnabled(false);
    } //choices

    for (int ind = 0; ind < Num_Botones; ind++) {
      btnEtiqueta = (CButtonControl) btn.elementAt(ind);
      btnEtiqueta.setEnabled(false);
      btnEtiqueta.doLayout();
    } //botones

    button1.setEnabled(false);

    dialogo.doLayout();
  }

  protected void habilitarCampos() {

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    //iEstado = MODIFICACION;
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    CButtonControl btnEtiqueta;

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      txtEtiqueta.setEnabled(true);
      txtEtiqueta.setEditable(true);
    } //cajas

    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      cboEtiqueta.setEnabled(true);
    } //choices

    for (int ind = 0; ind < Num_Botones; ind++) {
      btnEtiqueta = (CButtonControl) btn.elementAt(ind);
      btnEtiqueta.setEnabled(true);
      btnEtiqueta.doLayout();
    } //botones

    button1.setEnabled(true);

    modoOperacion = modoCondicionadaDeshabilitar;
    Inicializar();

    modoOperacion = modoRevisarCondicionadas;
    Inicializar();

    this.doLayout();
  }

  protected void Revision_del_que_pierde_el_foco() {

    String sCD = "";
    String sNUM = "";

    Label lblCAfectado;
    CChoice cboAfectado;
    CChoice cboPadre;

    Label lblTAfectado;
    CTextField txtAfectado;
    CTextField txtPadre;

    Vector vChoice = new Vector();
    String sValorTabla = "";
    String sValorPanel = "";

    int iPadreChoice = 0;
    int iAfectado = 0;
    int iPadreText = 0;

    int indice = 0;

    Object CompPerdioFocoPrimero = new Object();
    boolean b = false;

    //recorro los choices
    //(Combinacion: Afectado=Choice, Padre: Choice, TextField)
    for (iAfectado = 0; iAfectado < Num_Choices; iAfectado++) {
      cboAfectado = (CChoice) cbo.elementAt(iAfectado);
      //si alguno esta condicionado
      if (cboAfectado.getCondicionada().equals("S")) {

        //SU CondPadre?
        sCD = cboAfectado.getCodM();
        sNUM = cboAfectado.getNumC();
        sValorTabla = cboAfectado.getDesC(); //El afectado es un Choice

        //Busco su Padre (entre choices!!!) -------------------------------
        for (iPadreChoice = 0; iPadreChoice < Num_Choices; iPadreChoice++) {
          cboPadre = (CChoice) cbo.elementAt(iPadreChoice);
          if (cboPadre.getCodM().equals(sCD) && cboPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            sValorPanel = ValorSeleccionadoChoiceNumero(cboPadre);
            //VALOR SELECCIONADO coincide!!!!!!!!!!!!!!
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              if (cboPadre.isEnabled()) {
                cboAfectado.setEnabled(true);
              }
              if (CompPerdioFoco.equals(cboPadre)) {

                if (!b) {
                  b = true;
                  CompPerdioFocoPrimero = cboAfectado;
                }
              }
            }

            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              cboAfectado.setEnabled(false);
              indice = cboAfectado.getSelectedIndex();
              if (indice != 0) {
                cboAfectado.select(0);
              }

            } //else
          } //fin if cd, num
        } //fin for secundario

        //Busco su Padre (entre textField)------------------------
        for (iPadreText = 0; iPadreText < Num_Cajas; iPadreText++) {
          txtPadre = (CTextField) txt.elementAt(iPadreText);
          if (txtPadre.getCodM().equals(sCD) && txtPadre.getNum().equals(sNUM)) {
            //Lo he encontrado

            if (txtPadre.getTipoPregunta().equals("N")) {

              //ya formateado,si Num, y necesito sin formato para comparar con la tabla
              sValorPanel = txtPadre.getNumSinFormato();
            }
            else {
              sValorPanel = txtPadre.getText();

              //VALOR SELECCIONADO!!!!!!!!!!!!!!
            }
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              if (txtPadre.isEnabled()) {
                cboAfectado.setEnabled(true);
              }
              if (CompPerdioFoco.equals(txtPadre)) {
                //cboAfectado.requestFocus();
                if (!b) {
                  b = true;
                  CompPerdioFocoPrimero = cboAfectado;
                }
              }
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              cboAfectado.setEnabled(false);
              indice = cboAfectado.getSelectedIndex();

              if (indice != 0) {
                cboAfectado.select(0);
              }

            } //else
          } //fin if cd, num

        } //fin for secundario
        //------------------------------------------
      } //fin Choice Condicionado

    } //fin for general

    //recorro los TextField
    //(Combinacion: Afectado=TextField, Padre: Choice, TextField)
    for (iAfectado = 0; iAfectado < Num_Cajas; iAfectado++) {
      txtAfectado = (CTextField) txt.elementAt(iAfectado);
      //si alguno esta condicionado
      if (txtAfectado.getCondicionada().equals("S")) {

        //SU CondPadre?
        sCD = txtAfectado.getCodM();
        sNUM = txtAfectado.getNumC();
        sValorTabla = txtAfectado.getDesC(); //El afectado es un Text

        //Busco su Padre (entre choices!!!) -------------------------------
        for (iPadreChoice = 0; iPadreChoice < Num_Choices; iPadreChoice++) {
          cboPadre = (CChoice) cbo.elementAt(iPadreChoice);
          if (cboPadre.getCodM().equals(sCD) && cboPadre.getNum().equals(sNUM)) {
            //Lo he encontrado

            sValorPanel = ValorSeleccionadoChoiceNumero(cboPadre);
            //VALOR SELECCIONADO!!!!!!!!!!!!!!
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              if (cboPadre.isEnabled()) {
                txtAfectado.setEnabled(true);
              }
              if (cboPadre.isEnabled()) {
                txtAfectado.setEditable(true);
              }
              if (CompPerdioFoco.equals(cboPadre)) {
                if (!b) {
                  b = true;
                  CompPerdioFocoPrimero = txtAfectado;
                }
              }
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              txtAfectado.setEnabled(false);
              txtAfectado.setText("");
              txtAfectado.setEditable(false);

            } //else

          } //fin if cd, num

        } //fin for secundario

        //Busco su Padre (entre textField)------------------------
        for (iPadreText = 0; iPadreText < Num_Cajas; iPadreText++) {
          txtPadre = (CTextField) txt.elementAt(iPadreText);
          if (txtPadre.getCodM().equals(sCD) && txtPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            if (txtPadre.getTipoPregunta().equals("N")) {

              //ya formateado,si Num, y necesito sin formato para comparar con la tabla
              sValorPanel = txtPadre.getNumSinFormato();
            }
            else {
              sValorPanel = txtPadre.getText();
              //VALOR SELECCIONADO!!!!!!!!!!!!!!
            }
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              if (txtPadre.isEnabled()) {
                txtAfectado.setEnabled(true);
              }
              if (txtPadre.isEnabled()) {
                txtAfectado.setEditable(true);
              }
              if (CompPerdioFoco.equals(txtPadre)) {

                if (!b) {
                  b = true;
                  CompPerdioFocoPrimero = txtAfectado;
                }
              }
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              txtAfectado.setEnabled(false);
              txtAfectado.setText("");
              txtAfectado.setEditable(false);

            } //else
          } //fin if cd, num

        } //fin for secundario
        //------------------------------------------
      } //fin text Condicionado

    } //fin for general
    if (b) {
      if (CompPerdioFocoPrimero.getClass().getName().equals(
          "infproto.CTextField")) {
        CTextField txtPrim = (CTextField) CompPerdioFocoPrimero;
        txtPrim.requestFocus();
      }
      if (CompPerdioFocoPrimero.getClass().getName().equals("infproto.CChoice")) {
        CChoice cboPrim = (CChoice) CompPerdioFocoPrimero;
        cboPrim.requestFocus();
      }
    }
  }

  protected void Revision() {
    String sCD = "";
    String sNUM = "";

    Label lblCAfectado;
    CChoice cboAfectado;
    CChoice cboPadre;

    Label lblTAfectado;
    CTextField txtAfectado;
    CTextField txtPadre;

    Vector vChoice = new Vector();
    String sValorTabla = "";
    String sValorPanel = "";

    int iPadreChoice = 0;
    int iAfectado = 0;
    int iPadreText = 0;

    int indice = 0;
    //recorro los choices
    //(Combinacion: Afectado=Choice, Padre: Choice, TextField)
    for (iAfectado = 0; iAfectado < Num_Choices; iAfectado++) {
      cboAfectado = (CChoice) cbo.elementAt(iAfectado);
      //si alguno esta condicionado
      if (cboAfectado.getCondicionada().equals("S")) {

        //SU CondPadre?
        sCD = cboAfectado.getCodM();
        sNUM = cboAfectado.getNumC();
        sValorTabla = cboAfectado.getDesC(); //El afectado es un Choice

        //Busco su Padre (entre choices!!!) -------------------------------
        for (iPadreChoice = 0; iPadreChoice < Num_Choices; iPadreChoice++) {
          cboPadre = (CChoice) cbo.elementAt(iPadreChoice);
          if (cboPadre.getCodM().equals(sCD) && cboPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            sValorPanel = ValorSeleccionadoChoiceNumero(cboPadre);
            //VALOR SELECCIONADO coincide!!!!!!!!!!!!!!
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              cboAfectado.setEnabled(true);
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              cboAfectado.setEnabled(false);
              indice = cboAfectado.getSelectedIndex();
              if (indice != 0) {
                cboAfectado.select(0);
              }

            } //else
          } //fin if cd, num
        } //fin for secundario

        //Busco su Padre (entre textField)------------------------
        for (iPadreText = 0; iPadreText < Num_Cajas; iPadreText++) {
          txtPadre = (CTextField) txt.elementAt(iPadreText);
          if (txtPadre.getCodM().equals(sCD) && txtPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            if (txtPadre.getTipoPregunta().equals("N")) {

              //ya formateado,si Num, y necesito sin formato para comparar con la tabla
              sValorPanel = txtPadre.getNumSinFormato();
            }
            else {
              sValorPanel = txtPadre.getText();

              //VALOR SELECCIONADO!!!!!!!!!!!!!!
            }
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              cboAfectado.setEnabled(true);
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              cboAfectado.setEnabled(false);
              indice = cboAfectado.getSelectedIndex();

              if (indice != 0) {
                cboAfectado.select(0);
              }
            } //else
          } //fin if cd, num

        } //fin for secundario
        //------------------------------------------
      } //fin Choice Condicionado

    } //fin for general

    //recorro los TextField
    //(Combinacion: Afectado=TextField, Padre: Choice, TextField)
    for (iAfectado = 0; iAfectado < Num_Cajas; iAfectado++) {
      txtAfectado = (CTextField) txt.elementAt(iAfectado);
      //si alguno esta condicionado
      if (txtAfectado.getCondicionada().equals("S")) {

        //SU CondPadre?
        sCD = txtAfectado.getCodM();
        sNUM = txtAfectado.getNumC();
        sValorTabla = txtAfectado.getDesC(); //El afectado es un Text

        //Busco su Padre (entre choices!!!) -------------------------------
        for (iPadreChoice = 0; iPadreChoice < Num_Choices; iPadreChoice++) {
          cboPadre = (CChoice) cbo.elementAt(iPadreChoice);
          if (cboPadre.getCodM().equals(sCD) && cboPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            sValorPanel = ValorSeleccionadoChoiceNumero(cboPadre);
            //VALOR SELECCIONADO!!!!!!!!!!!!!!
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              txtAfectado.setEnabled(true);
              txtAfectado.setEditable(true);
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              txtAfectado.setEnabled(false);
              txtAfectado.setText("");
              txtAfectado.setEditable(false);

            } //else

          } //fin if cd, num

        } //fin for secundario

        //Busco su Padre (entre textField)------------------------
        for (iPadreText = 0; iPadreText < Num_Cajas; iPadreText++) {
          txtPadre = (CTextField) txt.elementAt(iPadreText);
          if (txtPadre.getCodM().equals(sCD) && txtPadre.getNum().equals(sNUM)) {
            //Lo he encontrado
            if (txtPadre.getTipoPregunta().equals("N")) {

              //ya formateado,si Num, y necesito sin formato para comparar con la tabla
              sValorPanel = txtPadre.getNumSinFormato();
            }
            else {
              sValorPanel = txtPadre.getText();
              //VALOR SELECCIONADO!!!!!!!!!!!!!!
            }
            if (sValorPanel.trim().equals(sValorTabla.trim())) {
              txtAfectado.setEnabled(true);
              txtAfectado.setEditable(true);
            }
            //VALOR NO SELECCIONADO!!!!!!!!!!!!!!
            else { //vaciarlo y Deshabilitarlo y que NO tome el foco!!!!
              txtAfectado.setEnabled(false);
              txtAfectado.setText("");
              txtAfectado.setEditable(false);
            } //else
          } //fin if cd, num

        } //fin for secundario
        //------------------------------------------
      } //fin text Condicionado

    } //fin for general

  }

  void PintarBotones() {

    String CodModel = "";
    String NumLin = "";
    String CodPreg = ""; //por si acaso
    String Nivel = "";
    String it_ok = "";

    //hay que buscar en todas las listasValores de listaPartes.
    //Puede que la primera no tenga valores ara ese modelo con lo que no sale el boton
    //pero el segundo partes si!!
    for (int iPartes = 0; iPartes < dialogo.listaPartes.size(); iPartes++) {
      datosParte datPARTE = (datosParte) dialogo.listaPartes.elementAt(iPartes);
      Vector vValores = (Vector) datPARTE.preguntas;
      for (int y = 0; y < vValores.size(); y++) {
        datosPreg datosbotones = (datosPreg) vValores.elementAt(y);
        if (!datosbotones.modelo.equals(CodModel)) {
          CodModel = datosbotones.modelo;
          Nivel = datosbotones.nivel;
          it_ok = datosbotones.it_ok;

          if (it_ok.equals("N")) { //modelo con protocolo obsoleto
            //buscamos su boton
            for (int j = 0; j < Num_Botones; j++) {
              CButtonControl btnBoton = (CButtonControl) btn.elementAt(j);
              if (btnBoton.getNivel().equals(Nivel)) {

                if (!btnBoton.bListenerCargado) {
                  btnBoton.setActionCommand(Nivel);
                  btnBoton.addActionListener(new
                      Panel_Informe_Completo_button_actionAdapter(this));
                  btnBoton.bListenerCargado = true;
                  btnBoton.setVisible(true);
                  btnBoton.setCodModelo(CodModel);
                }

                break;
              } //if nivel
            } //for
          } //if de N
        } //if de modelo

      } //for de cada vValores
    } //fin del for de Partes
  }

  protected void CargarPlantilla() {

    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    CButtonControl btnEtiqueta;

    //botones ---------------- buscar en todas las listaValores de listaPartes
    PintarBotones();
    //------------------------------------------------------------------------

    for (int i = 0; i < listaValores.size(); i++) {
      datosPreg dat = (datosPreg) listaValores.elementAt(i);

      //busco ese registro entre las cajas
      for (int ind = 0; ind < Num_Cajas; ind++) {
        txtEtiqueta = (CTextField) txt.elementAt(ind);

        if (txtEtiqueta.getCodM().trim().equals(dat.modelo.trim()) &&
            txtEtiqueta.getNum().trim().equals( (new Integer(dat.linea)).
                                               toString()) &&
            txtEtiqueta.getCodPregunta().trim().equals(dat.pregunta.trim())) {
          txtEtiqueta.setText(dat.respuesta.trim());
          //simular que pierde el foco!!(y me despreocupo de Habilitados..., y formatos)
          //Gained, no hace falta, porque los num se graban desformateados
          objfocusLost(new FocusEvent( (Component) txtEtiqueta,
                                      java.awt.event.FocusEvent.FOCUS_LOST));
        }
      } //cajas
      //busco ese registro entre los choices
      for (int ind = 0; ind < Num_Choices; ind++) {
        cboEtiqueta = (CChoice) cbo.elementAt(ind);

        if (cboEtiqueta.getCodM().trim().equals(dat.modelo.trim()) &&
            cboEtiqueta.getNum().trim().equals( (new Integer(dat.linea)).
                                               toString()) &&
            cboEtiqueta.getCodPregunta().trim().equals(dat.pregunta.trim())) {

          //primero limpio la caja
          //cboEtiqueta.Resetear_Choice();
          //despues cargo los valores
          cboEtiqueta.CargarVector(dat.ValorLista, dat.respuesta);
          //simular que pierde el foco!! (y me despreocupo de Habilitados...)
          objfocusLost(new FocusEvent( (Component) cboEtiqueta,
                                      java.awt.event.FocusEvent.FOCUS_LOST));
        }
      } //choices

    } //fin for

    LostGained();
  }

  protected void LostGained() {

    CTextField txtEtiqueta;
    CChoice cboEtiqueta;

    //bucle de perdida de foco,
    //para vaciar datos Condicionados que fueran incoherentes.
    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      objfocusGained(new FocusEvent( (Component) txtEtiqueta,
                                    java.awt.event.FocusEvent.FOCUS_GAINED));
      objfocusLost(new FocusEvent( (Component) txtEtiqueta,
                                  java.awt.event.FocusEvent.FOCUS_LOST));
    }
    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      objfocusGained(new FocusEvent( (Component) cboEtiqueta,
                                    java.awt.event.FocusEvent.FOCUS_GAINED));
      objfocusLost(new FocusEvent( (Component) cboEtiqueta,
                                  java.awt.event.FocusEvent.FOCUS_LOST));
    }
  }

//Valor guardado en el vector para esa Choice y el indice selected
//return : "" o "H"
  protected String ValorSeleccionadoChoiceNumero(CChoice cboP) {

    String sValor = "";
    Vector vChoice = new Vector();
    int ind = 0;
    CMessage msgBox = null;
    boolean b = true;

    try {

      vChoice = (Vector) cboP.getVectorCodLista();
      //siempre tiene un blanco, ptt el tama�o es 1
      if (vChoice.size() == 1 || vChoice.size() < 1) {
        b = false; //en realidad, como si no tuviera datos

      }
      else {
        ind = cboP.getSelectedIndex();
        if (ind != 0 && ind != -1) {
          sValor = (String) vChoice.elementAt(ind);
        }
        else {
          sValor = ""; //retorna un vacio
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (b) {
      return (sValor.trim()); //si esta seleccionado el 0, devuelve ""
    }
    else {
      return ("-1"); //caso de error
    }
  }

  protected boolean IraBuscarDatosLayout() {
    CMessage msgBox = null;
    CLista ListaEntrada = null;
    listadatos = null;

    DataEnfEnt datosEntrada = null;

    try {

      datosEntrada = new DataEnfEnt(sCod, sCa, sNivel1, sNivel2);
      CLista listaEntorno = new CLista();
      listaEntorno.addElement(datosEntrada);

      CLista listaEntrada = new CLista();
      listaEntrada.addElement(listaEntorno);
      listaEntrada.addElement(dialogo.listaPartes);

      stub.setUrl(new URL(app.getURL() + strSERVLET));
      CLista listaSalida = (CLista) stub.doPost(servletADIC, listaEntrada);
      listaEntrada = null;

      //listaSalida: resp , layout
      if (listaSalida != null) {
        listadatos = (CLista) listaSalida.elementAt(1);
        //listaValores ya la tenemos
      }
      if (listadatos == null) {

        NoSeEncontraronDatos = true;
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              resour.getString("msg5.Text"));
        msgBox.show();
        msgBox = null;
        //dialogo.dispose();
        return false;
      }
      //controlar que el primer tipo debe ser descripcion
      else {
        /*//leo
                for (int indice=0; indice < listadatos.size(); indice++) {
         DataLayout datosSalida = (DataLayout)listadatos.elementAt(indice);
         //# // System_out.println(datosSalida.getTipoPreg());
                } */
       return true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      return false;
    }
  }

  protected int Componentes() {

    Label lblTexto;
    Label lblChoice;
    Label DesEtiqueta; //SobreTitulos: Cne, ca, nivel1, nivel2
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;
    GroupBox gbEtiqueta;
    CButtonControl btnEtiqueta;

    String sAlmacenDescGrupo = "";
    String sAlmTipo = "";
    String sDesX = "";
    int iTamLista = listadatos.size();
    int indice = 0;

    int iGrupo = 0;
    int iLabelChoice = 0;
    int iLabelTexto = 0;
    int iCaja = 0;
    int iChoice = 0;
    int iBotones = 0;
    int iDescripciones = 0;

    int xObj = 20;
    int xGrupo = 10;
    int MGO = 15; // entre grupo-obj
    int MG = 5; // entre grupos  y para el marco dowm
    int M = 1; //entre objetos
    int Alto = 30; // alto de los objetos

    int y = 0; //donde estamos
    int yUp = 0;
    int yDown = 0;

    CCargadorImagen imgs = null;
    final String imgNAME_BOTONES[] = {
        "images/refrescar.gif"};

    imgs = new CCargadorImagen(applet, imgNAME_BOTONES);
    imgs.CargaImagenes();

    //recorremos la listadatos
    for (indice = 0; indice < iTamLista; indice++) {
      DataLayout datUnaLinea = (DataLayout) listadatos.elementAt(indice);

      //DESCRIPCION
      if (datUnaLinea.getTipoPreg().equals("X") &&
          indice == 0) {

        y = y + MGO; //situarnos (con un margen)
        //label
        Des.addElement(new Label());
        DesEtiqueta = (Label) Des.elementAt(iDescripciones);
        this.add(DesEtiqueta,
                 new XYConstraints(xObj, y, Ancho_GroupBox - 150, -1));

        DesEtiqueta.setAlignment(Label.LEFT);

        DesEtiqueta.setFont(new Font("Dialog", 1, 12));
        DesEtiqueta.setForeground(Color.white);
        DesEtiqueta.setBackground(SystemColor.activeCaption);

        if (datUnaLinea.getDesTexto().equals("CNE")) {
          DesEtiqueta.setText(" " + resour.getString("DesEtiqueta.Text"));
          sDesX = "CNE";

          //---- boton
          btn.addElement(new CButtonControl(null, "CNE"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel(resour.getString("btnEtiqueta.Label"));
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----

        }
        else if (datUnaLinea.getDesTexto().equals("CA")) {
          DesEtiqueta.setText(" " + resour.getString("DesEtiqueta.Text1"));
          sDesX = "CA";
          //---- boton
          btn.addElement(new CButtonControl(null, "CA"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel(resour.getString("btnEtiqueta.Label"));
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("N1")) {
          DesEtiqueta.setText(" " + this.app.getNivel1() + " " +
                              datUnaLinea.getNIVEL_1());
          sDesX = "N1";
          //---- boton
          btn.addElement(new CButtonControl(null, "N1"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel(resour.getString("btnEtiqueta.Label"));
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("N2")) {
          DesEtiqueta.setText(" " + this.app.getNivel2() + " " +
                              datUnaLinea.getNIVEL_2());
          sDesX = "N2";
          //---- boton
          btn.addElement(new CButtonControl(null, "N2"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel(resour.getString("btnEtiqueta.Label"));
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }

        iDescripciones++;
        sAlmTipo = "X";
        y = y + Alto; //lo que ocupa la descripcion

      }
      else if (datUnaLinea.getTipoPreg().equals("X") &&
               indice != 0) {

        //acaba el groupBox Anterior****
        //pintar grupo anterior--------
        gb.addElement(new GroupBox());
        gbEtiqueta = (GroupBox) gb.elementAt(iGrupo);
        gbEtiqueta.setLabel(sAlmacenDescGrupo);
        gbEtiqueta.setFont(new Font("Dialog", 1, 12));
        gbEtiqueta.setForeground(new Color(153, 0, 0));
        yDown = y + MG; //le ponemos el peque�o
        this.add(gbEtiqueta,
                 new XYConstraints(xGrupo, yUp, Ancho_GroupBox, yDown - yUp));

        //------------------
        iGrupo++;
        y = yDown;
        sAlmTipo = "X";

        //pinta descripcion*****
        y = y + MGO; //situarnos (con un margen)
        //label
        Des.addElement(new Label());
        DesEtiqueta = (Label) Des.elementAt(iDescripciones);
        this.add(DesEtiqueta,
                 new XYConstraints(xObj, y, Ancho_GroupBox - 150, -1));
        DesEtiqueta.setAlignment(Label.LEFT);

        DesEtiqueta.setFont(new Font("Dialog", 1, 12));
        DesEtiqueta.setForeground(Color.white);
        DesEtiqueta.setBackground(SystemColor.activeCaption);

        if (datUnaLinea.getDesTexto().equals("CNE")) {
          DesEtiqueta.setText(" " + resour.getString("DesEtiqueta.Text"));
          sDesX = "CNE";
          //---- boton
          btn.addElement(new CButtonControl(null, "CNE"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel(resour.getString("btnEtiqueta.Label"));
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("CA")) {
          DesEtiqueta.setText(" " + resour.getString("DesEtiqueta.Text1"));
          sDesX = "CA";
          //---- boton
          btn.addElement(new CButtonControl(null, "CA"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel(resour.getString("btnEtiqueta.Label"));
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("N1")) {
          DesEtiqueta.setText(" " + this.app.getNivel1() + " " +
                              datUnaLinea.getNIVEL_1());
          sDesX = "N1";
          //---- boton
          btn.addElement(new CButtonControl(null, "N1"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel(resour.getString("btnEtiqueta.Label"));
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }
        else if (datUnaLinea.getDesTexto().equals("N2")) {
          DesEtiqueta.setText(" " + this.app.getNivel2() + " " +
                              datUnaLinea.getNIVEL_2());
          sDesX = "N2";
          //---- boton
          btn.addElement(new CButtonControl(null, "N2"));
          btnEtiqueta = (CButtonControl) btn.elementAt(iBotones);
          this.add(btnEtiqueta,
                   new XYConstraints(xObj + Ancho_GroupBox - 150 + 3,
                                     y, -1, -1));
          btnEtiqueta.setLabel(resour.getString("btnEtiqueta.Label"));
          btnEtiqueta.setImage(imgs.getImage(0));
          iBotones++;
          //-----
        }

        iDescripciones++;
        y = y + Alto;

      }
      //GRUPO primero. Inicia un GroupBox
      else if (datUnaLinea.getTipoPreg().equals("D") &&
               indice == 1) { //primer grupo  (X, D)

        sAlmacenDescGrupo = datUnaLinea.getDesTexto();
        sAlmTipo = "";
        y = y + MG;
        yUp = y;

      }
      //Grupo Detras de una Pregunta pppDpp: Acaba e inicia otro GroupBox
      else if (datUnaLinea.getTipoPreg().equals("D") &&
               indice != 1 &&
               (sAlmTipo.equals("L") || sAlmTipo.equals("C"))) {

        //pintar grupo anterior--------
        gb.addElement(new GroupBox());
        gbEtiqueta = (GroupBox) gb.elementAt(iGrupo);
        gbEtiqueta.setLabel(sAlmacenDescGrupo);
        gbEtiqueta.setFont(new Font("Dialog", 1, 12));
        gbEtiqueta.setForeground(new Color(153, 0, 0));
        yDown = y + MG; //le ponemos el peque�o
        this.add(gbEtiqueta,
                 new XYConstraints(xGrupo, yUp, Ancho_GroupBox, yDown - yUp));

        iGrupo++;
        y = yDown;
        //-----------------------------
        yUp = yDown + MG;
        y = yUp;
        sAlmacenDescGrupo = datUnaLinea.getDesTexto();
        sAlmTipo = "";
      }

      //Grupo Detras de una Decripcion. Inicia un GroupBox
      else if (datUnaLinea.getTipoPreg().equals("D") &&
               indice != 1 && sAlmTipo.equals("X")) {

        yUp = y + 1; //en vez de MG pongo 1
        y = yUp;
        sAlmacenDescGrupo = datUnaLinea.getDesTexto();
        sAlmTipo = "";

      }
//!!!!!!!!!!!!!
//CAJA!!!!!!!!!
//!!!!!!!!!!!!!
      else if (datUnaLinea.getTipoPreg().equals("C") ||
               datUnaLinea.getTipoPreg().equals("N") ||
               datUnaLinea.getTipoPreg().equals("F")) {

        //si acaba de darse un groupBox: MGO, si era otra caja M
        if (sAlmTipo.equals("")) {
          y = y + MGO;
        }
        else {
          y = y + M;

          //label
        }
        lblT.addElement(new Label());
        lblTexto = (Label) lblT.elementAt(iLabelTexto);
        this.add(lblTexto,
                 new XYConstraints(xObj, y, Ancho_Label_Caja - 20, -1));
        lblTexto.setAlignment(Label.RIGHT);
        lblTexto.setText(datUnaLinea.getDesTexto() + " : ");
        iLabelTexto++; //contador = cajas

        //caja
        //datos int
        int ilon = 0;
        int idec = 0;
        int ient = 0;
        if (datUnaLinea.getLongitud() != null) {
          if (!datUnaLinea.getLongitud().trim().equals("")) {
            Integer Lon = new Integer(datUnaLinea.getLongitud());
            ilon = Lon.intValue();
          }
        }
        if (datUnaLinea.getDecimal() != null) {
          if (!datUnaLinea.getDecimal().trim().equals("")) {
            Integer Dec = new Integer(datUnaLinea.getDecimal());
            idec = Dec.intValue();
          }
        }
        if (datUnaLinea.getEntera() != null) {
          if (!datUnaLinea.getEntera().trim().equals("")) {
            Integer Ent = new Integer(datUnaLinea.getEntera());
            ient = Ent.intValue();
          }
        }

        txt.addElement(new CTextField(datUnaLinea.getOblig(),
                                      datUnaLinea.getTipoPreg(),
                                      ilon, ient, idec,
                                      datUnaLinea.getCondicionada(),
                                      datUnaLinea.getCodModelo(),
                                      datUnaLinea.getNumLinea(),
                                      datUnaLinea.getNumLineaCond(),
                                      datUnaLinea.getDesPreguntaCond(),
                                      datUnaLinea.getCodPregunta()));

        txtEtiqueta = (CTextField) txt.elementAt(iCaja);

        //(150 pixeles, 20 caracteres) TAMA�O
        if (datUnaLinea.getTipoPreg().equals("C")) {
          this.add(txtEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y,
                                     (MaxMin("C", ilon, ient, idec)), -1));
        }
        else if (datUnaLinea.getTipoPreg().equals("F")) {
          this.add(txtEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y, ( (160 / 20) * 12),
                                     -1));
        }
        else if (datUnaLinea.getTipoPreg().equals("N")) {
          this.add(txtEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y,
                                     (MaxMin("N", ilon, ient, idec)), -1));
        }
        //escuchador
        txtEtiqueta.addFocusListener(objLostFocus);
        iCaja++;

        sAlmTipo = "C";
        y = y + Alto;

      }
//!!!!!!!!!!!!!
//LISTA!!!!!!!!
//!!!!!!!!!!!!!
      else if (datUnaLinea.getTipoPreg().equals("B") ||
               datUnaLinea.getTipoPreg().equals("L")) {
        //si acaba de darse una descripcion: MGO, si era otra caja M
        if (sAlmTipo.equals("")) {
          y = y + MGO;
        }
        else {
          y = y + M;

          //label ---LA Y LA MARCAMOS CON LAS LABELS
        }
        lblC.addElement(new Label());
        lblChoice = (Label) lblC.elementAt(iLabelChoice);
        this.add(lblChoice,
                 new XYConstraints(xObj, y, Ancho_Label_Caja - 20, -1));
        lblChoice.setAlignment(Label.RIGHT);
        lblChoice.setText(datUnaLinea.getDesTexto() + " : ");
        iLabelChoice++;
        //Lista
        cbo.addElement(new CChoice(app,
                                   datUnaLinea.getOblig(),
                                   datUnaLinea.getCodLista(),
                                   datUnaLinea.getCondicionada(),
                                   datUnaLinea.getCodModelo(),
                                   datUnaLinea.getNumLinea(),
                                   datUnaLinea.getNumLineaCond(),
                                   datUnaLinea.getDesPreguntaCond(),
                                   datUnaLinea.getCodPregunta(),
                                   datUnaLinea.getVALORES()));

        cboEtiqueta = (CChoice) cbo.elementAt(iChoice);
        //TAMA�O
        if (datUnaLinea.getTipoPreg().equals("B")) {
          this.add(cboEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y, (150 / 20) * 10, -1));

        }
        else {
          this.add(cboEtiqueta,
                   new XYConstraints(Ancho_Label_Caja, y, Ancho_Label_Caja, -1));
        }
        //escuchador
        cboEtiqueta.addFocusListener(objLostFocus);
        cboEtiqueta.addItemListener(objItemListener);
        iChoice++;

        sAlmTipo = "L";
        y = y + Alto;
      }

    } //listadatos

    //El ultimo Grupo
    gb.addElement(new GroupBox());
    gbEtiqueta = (GroupBox) gb.elementAt(iGrupo); //me situo en el que quiero
    gbEtiqueta.setLabel(sAlmacenDescGrupo);
    gbEtiqueta.setFont(new Font("Dialog", 1, 12)); //ESTE ERA!!!
    gbEtiqueta.setForeground(new Color(153, 0, 0));
    yDown = y + MG; //le ponemos el peque�o
    this.add(gbEtiqueta,
             new XYConstraints(xGrupo, yUp, Ancho_GroupBox, yDown - yUp));

    y = yDown;

    //llenamos los totales
    Num_Grupos = iGrupo;
    Num_Choices = iChoice;
    Num_Cajas = iCaja;
    Num_Descripciones = iDescripciones;
    Num_Label_Cajas = iLabelTexto;
    Num_Label_Choices = iLabelChoice;
    Num_Botones = iBotones;

    return y;

  }

//Calcula la longitud permitida del numero (con formato)
  protected int LongNumero(int iEnt, int iDec) {

    int iLen = 0;
    int iResto = 0;
    int iDiv = 0;
    int iNumPtos = 0;

    if (iEnt == 0) { //N(0,?)
      iLen = 0;
    }
    else {
      iDiv = (int) (iEnt / 3);
      iResto = iEnt % 3;

      if (iResto == 0) {
        iNumPtos = iDiv - 1;
      }
      else {
        iNumPtos = iDiv;
      }
    }
    if (iDec == 0) {
      iLen = iEnt + iNumPtos;
    }
    else {
      iLen = iEnt + iNumPtos + 1 + iDec;

    }
    return (iLen);
  }

//Tama�o de las cajas de texto (N, C)
//(Max = Ancho_Label_Caja) (Min = 35)
  protected int MaxMin(String Tipo, int longi, int ente, int deci) {
    int tam = 0;
    if (Tipo.equals("C")) {
      tam = (150 / 20) * longi;
      if (tam > Ancho_Label_Caja) {
        tam = Ancho_Label_Caja;
      }
      if (tam < 35) {
        tam = 35;
      }
    }
    else if (Tipo.equals("N")) {
      tam = (150 / 20) * (LongNumero(ente, deci) + 3);
      if (tam > Ancho_Label_Caja) {
        tam = Ancho_Label_Caja;
      }
      if (tam < 35) {
        tam = 35;
      }
    }
    return (tam);
  }

//PERDIDA DE FOCO**********
  public void objfocusLost(FocusEvent e) {

    CompPerdioFoco = e.getComponent();
    if (CompPerdioFoco.getClass().getName().equals("infproto.CChoice")) {
      return;
    }
    if (CompPerdioFoco.getClass().getName().equals("infproto.CTextField")) {
      CTextField txtAnalizado = (CTextField) CompPerdioFoco;
      String Tipo = txtAnalizado.getTipoPregunta();
      //CARACTER
      if (Tipo.equals("C")) {
        //nada especial
      }
      //FECHA
      if (Tipo.equals("F")) {
        txtAnalizado.ValidarFecha(); //validamos el dato
        if (txtAnalizado.getValid().equals("N")) {
          //opcional vaciarlo
          txtAnalizado.setText(txtAnalizado.getFecha());
          //opcional si obligatorio
          //Cfecha.selectAll();
          //Cfecha.requestFocus();
        }
        else if (txtAnalizado.getValid().equals("S")) {
          txtAnalizado.setText(txtAnalizado.getFecha());
        }

      }
      //NUMERICO
      if (Tipo.equals("N")) {
        String Alm = txtAnalizado.getText().trim(); //antes de formatear
        if (!Alm.equals("")) {
          txtAnalizado.FormatNumero();
        }
        else { //vacio
          txtAnalizado.ResetsNumSinFormato(); //a vacio!
        }
      }
      //Si llamo a FormatNumero, esta cargado NumSinFormato.
      //Si no lo llamo, NumSinFormato =""

    } //fin de text

    modoOperacion = modoPerdidaFoco;
    this.Inicializar();
  }

//cambiar de item en las choices
  public void objitemchange(ItemEvent e) {

    modoOperacion = modoPerdidaFoco;
    this.Inicializar();

  }

//Ganar Foco
  public void objfocusGained(FocusEvent e) {

    if (!e.getComponent().isEnabled()) {
      e.getComponent().transferFocus();
      return;
    }
    //numeros!!!!!
    if (e.getComponent().getClass().getName().equals("infproto.CTextField")) {
      CTextField txtAnalizado = (CTextField) e.getComponent();
      String Tipo = txtAnalizado.getTipoPregunta();
      if (Tipo.equals("N") && !txtAnalizado.getText().equals("")) {
        txtAnalizado.DesFormatNumero();
        txtAnalizado.select(0, 0);
      }
    }
  }

  void Cargar_Reserva_Layout_datos() {
    //RESERVA ---------------------------------------
    listadatosReservada = new CLista();
    listaValoresReservada = new Vector(); //vector de vectores

    for (int i = 0; i < listadatos.size(); i++) {
      DataLayout dat = (DataLayout) listadatos.elementAt(i);
      listadatosReservada.addElement(dat);
    }
    for (int t = 0; t < dialogo.listaPartes.size(); t++) {
      listaValoresReservada.addElement(dialogo.CadalistaValores(t));
    }
  } //fin de reserva

  void Volver_a_Reserva() {

    listadatos = new CLista();
    listaValores = new Vector();

    for (int i = 0; i < listadatosReservada.size(); i++) {
      DataLayout dat = (DataLayout) listadatosReservada.elementAt(i);
      listadatos.addElement(dat);
    }
    for (int i = 0; i < listaValoresReservada.size(); i++) {
      Vector cadalista = (Vector) listaValoresReservada.elementAt(i);
      dialogo.Cambiar_listaPartes(i, cadalista);
    }
  }

  void button_actionPerformed(ActionEvent e) {

    if (!ValidarObligatorios_Simple()) {
      return;
    }

    Cargar_Reserva_Layout_datos();

    CButtonControl botonAfectado = null;
    CMessage msgBox = null;

    CLista listaLayout1 = new CLista();
    CLista listaLayout2 = new CLista();
    CLista listaLayout2Obsoleta = new CLista();
    CLista listaLayout3 = new CLista();

    Vector listaValores1 = new Vector();
    Vector listaValores2 = new Vector();
    Vector listaValores2Obsoleta = new Vector();
    Vector listaValores3 = new Vector();

    int iParteFinal1 = 0;
    int iParteInic2 = 0;
    String codAlm = "";

    int modo = dialogo.modoOperacion;
    dialogo.modoOperacion = dialogo.modoESPERA;
    dialogo.Inicializar();
    this.deshabilitarCampos(Cursor.WAIT_CURSOR);

    try {

      //encontrar boton
      for (int j = 0; j < Num_Botones; j++) {
        CButtonControl btnBoton = (CButtonControl) btn.elementAt(j);
        if (btnBoton.getNivel().equals(e.getActionCommand())) {
          //ese es el boton pulsado--> coger el modelo para restringir listaValores
          botonAfectado = (CButtonControl) btn.elementAt(j);
          break;
        }
      }

      //subdivido la lista de datos  -----------
      for (int i = 0; i < listadatos.size(); i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        if (dat.getCodModelo() != null) {
          if (dat.getCodModelo().trim().equals(botonAfectado.getCodModelo().
                                               trim())) {
            iParteFinal1 = i;
            codAlm = dat.getCodModelo().trim();
            break;
          }
          ; //
        }
      } //for
      for (int i = iParteFinal1; i < listadatos.size(); i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        if (dat.getCodModelo() != null) {
          if (!dat.getCodModelo().trim().equals(codAlm)) {
            iParteInic2 = i;
            break;
          } //
        }
      } //for
      //rectificacion por las "X"

      //cris if ( iParteInic2 <= 0){ mlm:  iParteInic2 == 0
      if (iParteInic2 == 0) { //era el ultimo
        iParteInic2 = listadatos.size();
        iParteFinal1--;
      }
      else {
        iParteFinal1--;
        iParteInic2--;
      }

      for (int i = 0; i < iParteFinal1; i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        listaLayout1.addElement(dat);
      }
      for (int i = iParteFinal1; i < iParteInic2; i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        listaLayout2Obsoleta.addElement(dat);
      }

      for (int i = iParteInic2; i < listadatos.size(); i++) {
        DataLayout dat = (DataLayout) listadatos.elementAt(i);
        listaLayout3.addElement(dat);
      }
      //----------------------------------------

      //ver si existe otro protocolo para ese nivel
      CLista listaEntrada = new CLista();
      DataProtocolo dataEntrada = null;
      NIVEL = botonAfectado.getNivel();
      //en la Descripcion va el Nivel
      dataEntrada = new DataProtocolo(sCod, botonAfectado.getNivel(), sNivel1,
                                      sNivel2, sCa, "");
      listaEntrada.addElement(dataEntrada);

      stub.setUrl(new URL(app.getURL() + strSERVLET_ACTIVO_OBSOLETO));
      listaLayout2 = (CLista) stub.doPost(servletVERACTIVOS, listaEntrada);
      listaEntrada = null;

      if (listaLayout2 == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              resour.getString("msg6.Text"));
        msgBox.show();
        msgBox = null;
        dialogo.modoOperacion = modo;
        dialogo.Inicializar();
        this.habilitarCampos();
        return;
      }

    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
      dialogo.modoOperacion = modo;
      dialogo.Inicializar();
      this.habilitarCampos();
      return;
    }

    //FIN DEL CONTROL DE ERRORES  sin haber realizados cambios visuales ni de listas

    try {

      //listaLayout2: dataLayout  (layout del nuevo)
      //si null, sale por catch  (error)
      //montamos el nuevo listadatos
      listadatos = new CLista();
      listadatos.appendData(listaLayout1);
      listadatos.appendData(listaLayout2);
      listadatos.appendData(listaLayout3);

      //PARA CADA PARTE
      int numPar = 0;
      if (iEstado == ALTA) {
        numPar = dialogo.listaPartes.size() + 1;
      }
      else {
        numPar = dialogo.listaPartes.size();

      }
      for (int t = 0; t < numPar; t++) {

        listaValores1 = new Vector();
        listaValores2 = new Vector();
        listaValores2Obsoleta = new Vector();
        listaValores3 = new Vector();

        //subdivido la lista de valores
        iParteFinal1 = 0;
        iParteInic2 = 0;
        codAlm = "";

        listaValores = new Vector();

        if (t == dialogo.listaPartes.size()) {

          //estamos en una actualizacion durante
          //la insercion de un parte nuevo
          listaValores = parteValido.preguntas;
        }
        else {

          //estamos en una actualizacion durante
          //la modificacion de un parte
          listaValores = dialogo.CadalistaValores(t);

          /* 17/11/99 mlm cambio el recomponer listaValores
                 for (int i = 0; i < listaValores.size(); i++){
            datosPreg dat = (datosPreg) listaValores.elementAt(i);
               if ( dat.modelo.trim().equals(botonAfectado.getCodModelo().trim())){
                iParteFinal1 = i;
                codAlm = dat.modelo.trim();
                break;
           }
                 }//for
                 for (int i = iParteFinal1; i < listaValores.size(); i++){
            datosPreg dat = (datosPreg) listaValores.elementAt(i);
              if (!dat.modelo.trim().equals(codAlm)){
                 iParteInic2 = i;
                 break;
              }//----------------------
                 }//for
                 if (iParteFinal1 == 0 && iParteInic2 == 0){ // en este parte no hay datos para ese modelo
             iParteFinal1 = 0;
             iParteInic2 = 0;
                 }
                 if ( iParteInic2 == 0 && iParteFinal1 != 0){  //era el ultimo
             iParteInic2 = listaValores.size();
                 }
                 for (int i = 0; i < iParteFinal1; i++){
             datosPreg dat = (datosPreg) listaValores.elementAt(i);
             listaValores1.addElement(dat);
                 }
                 for (int i = iParteFinal1; i < iParteInic2; i++){
             datosPreg dat = (datosPreg) listaValores.elementAt(i);
             listaValores2Obsoleta.addElement(dat);
                 }
                 for (int i = iParteInic2; i < listaValores.size(); i++){
             datosPreg dat = (datosPreg) listaValores.elementAt(i);
             listaValores3.addElement(dat);
                 }
                 //subdivido la lista de valores
                 //componer las listas de valores
               listaValores2 = Recomponer_RespEDO (listaValores2Obsoleta, listaLayout2);
                 listaValores = new Vector();
                 for (int y =0; y < listaValores1.size(); y++){
            listaValores.addElement(listaValores1.elementAt(y));
                 }
                 for (int y =0; y < listaValores2.size(); y++){
            listaValores.addElement(listaValores2.elementAt(y));
                 }
                 for (int y =0; y < listaValores3.size(); y++){
            listaValores.addElement(listaValores3.elementAt(y));
                 } */

          //$$$$$$$$$$$$$$$$$$
          //extraigo la listaValores2Obsoleta  (para partes puede ser vacia) -------
        }
        for (int i = 0; i < listaValores.size(); i++) {
          datosPreg dat = (datosPreg) listaValores.elementAt(i);
          if (dat.modelo.trim().equals(botonAfectado.getCodModelo().trim())) {
            listaValores2Obsoleta.addElement(dat);
          }
        }
        //le paso el recomponer
        listaValores2 = Recomponer_RespEDO(listaValores2Obsoleta, listaLayout2);

        //reconstruyo la lista de Valores.
        Vector listaCopiaSinBloque = new Vector();
        for (int i = 0; i < listaValores.size(); i++) {
          datosPreg dat = (datosPreg) listaValores.elementAt(i);
          listaCopiaSinBloque.addElement(dat);
        }
        int PuntoComienzo = 0;
        for (int i = 0; i < listaValores.size(); i++) {
          datosPreg dat = (datosPreg) listaValores.elementAt(i);
          if (dat.modelo.trim().equals(botonAfectado.getCodModelo().trim())) {
            PuntoComienzo = i;
            break;
          }
        }
        for (int i = 0; i < listaValores.size(); i++) {
          datosPreg dat = (datosPreg) listaValores.elementAt(i);
          if (dat.modelo.trim().equals(botonAfectado.getCodModelo().trim())) {
            if (listaCopiaSinBloque.size() == 1) {
              listaCopiaSinBloque = new Vector();

            }
            else {
              listaCopiaSinBloque.removeElementAt(i);
            }
          }
        }
        Vector listaBien = new Vector();

        //pegar en el pto
        for (int i = 0; i < listaCopiaSinBloque.size(); i++) {
          datosPreg dat = (datosPreg) listaCopiaSinBloque.elementAt(i);
          if (i != PuntoComienzo) {
            listaBien.addElement(dat); //Valores1
          }
          if (i == PuntoComienzo) {
            for (int y = 0; y < listaValores2.size(); y++) {
              listaBien.addElement(listaValores2.elementAt(y));
            } //valores2
            return;
          }
        }
        //resto
        for (int i = PuntoComienzo; i < listaCopiaSinBloque.size(); i++) {
          datosPreg dat = (datosPreg) listaCopiaSinBloque.elementAt(i);
          listaBien.addElement(dat);
        } //Valores3

        if (listaCopiaSinBloque.size() == 0) {
          for (int y = 0; y < listaValores2.size(); y++) {
            listaBien.addElement(listaValores2.elementAt(y));
          }
        }
        listaValores = new Vector();
        for (int y = 0; y < listaBien.size(); y++) {
          listaValores.addElement(listaBien.elementAt(y));
        }
        //----------------------------------------------------------------
        //%%%%%%%%%%%%%%%%%%%%%%%%

        if (t == dialogo.listaPartes.size()) {
          //estamos en una actualizacion durante
          //la insercion de un parte nuevo
          parteValido.preguntas = listaValores;
          dialogo.listaPartes.addElement(parteValido);
        }
        else {
          //estamos en una actualizacion durante
          //la modificacion de un parte
          dialogo.Cambiar_listaPartes(t, listaValores);
        }

      } //for de cada parte

      //LISTA VALORES NUEVA PARA CADA PARTE
      String smsg = resour.getString("msg7.Text");
      msgBox = new CMessage(this.app, CMessage.msgAVISO, smsg);
      msgBox.show();
      msgBox = null;
      //MODO CON BOTONES DE RECORRER ACTIVOS + ACEPTAR Y CANCELAR
      //cargar la plantilla
      bPulsarAceptarOCancelar = true;

      Volver_a_Abrir(); //LAYOUT
      //PARTE VALIDO
      //cris parteValido = (datosParte)dialogo.listaPartes.elementAt(dialogo.parteAct-1);
      parteValido = (datosParte) dialogo.listaPartes.elementAt(dialogo.
          listaPartes.size() - 1);

      dialogo.modoOperacion = modo;
      dialogo.Inicializar();
      this.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

      //Parche de botones: modo con botones de +- + - deshabi
      dialogo.btnBorrar.setEnabled(false);
      dialogo.btnModificar.setEnabled(false);
      dialogo.btnAnadir.setEnabled(false);
      this.button1.setEnabled(false);
      this.button2.setEnabled(false);
      dialogo.doLayout();
      //hasta que no se acepte o cancele , no se habilitan

    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
      bPulsarAceptarOCancelar = false;
      Volver_a_Reserva();
      Volver_a_Abrir(); //LAYOUT

      dialogo.modoOperacion = modo;
      dialogo.Inicializar();
      this.habilitarCampos();
    }

  } //boton

  void Volver_a_Abrir() {

    this.removeAll();
    listaInsertar = new CLista();

    Num_Grupos = 0;
    Num_Choices = 0;
    Num_Cajas = 0;
    Num_Label_Cajas = 0;
    Num_Label_Choices = 0;
    Num_Botones = 0;
    Num_Descripciones = 0;

    lblT = new Vector();
    lblC = new Vector();
    Des = new Vector(); //Sobretitulos
    cbo = new Vector();
    txt = new Vector();
    gb = new Vector();
    btn = new Vector();

    xYLayout1 = new XYLayout();
    try {
      listaValores = new Vector();
      listaValores = dialogo.CadalistaValores(dialogo.parteAct - 1);
      jbInit(); //componentes
      dialogo.panelScroll.doLayout();
      dialogo.panelInf.doLayout();
      dialogo.doLayout();

    }
    catch (Exception e) {
      e.printStackTrace();
    }

    this.setLayout(xYLayout1);
    this.doLayout();
  }

  public CLista Recomponer_RespEDO(Vector listaResp, CLista listaPlantilla) {
    CLista listaResp2 = new CLista();

    //encontrar preguntas iguales
    for (int i = 0; i < listaResp.size(); i++) {
      datosPreg dataObsoleto = (datosPreg) listaResp.elementAt(i);

      for (int j = 0; j < listaPlantilla.size(); j++) {
        DataLayout dataActivo = (DataLayout) listaPlantilla.elementAt(j);
        if (dataActivo.getCodPregunta() != null) {
          if (dataObsoleto.pregunta.trim().equals(dataActivo.getCodPregunta().
                                                  trim())) {

            Integer Ilinea = new Integer(dataActivo.getNumLinea());
            int linea = Ilinea.intValue();
            datosPreg dataInsertar = new datosPreg(linea,
                dataActivo.getCodPregunta(),
                dataObsoleto.respuesta,
                dataActivo.getCodModelo(),
                dataObsoleto.ValorLista,
                dataObsoleto.nivel,
                "S",
                dataActivo.getCA(),
                dataActivo.getNIVEL_1(),
                dataActivo.getNIVEL_2());

            listaResp2.addElement(dataInsertar);
          }
        }
      }
    } //for ----------------------

    //Ver si las que estan condicionadas deben aparecer o no en la
    //lista de valores nueva

    CLista listaRespResul = new CLista();
    DataLayout layPreg = null;
    String linCond = "";
    String codCond = "";
    String valorCond = "";
    datosPreg preg = null;
    datosPreg preg1 = null;

    CLista lisCopia = new CLista();
    for (int t = 0; t < listaResp2.size(); t++) {
      lisCopia.addElement( (datosPreg) listaResp2.elementAt(t));
    }

    for (int t = 0; t < listaPlantilla.size(); t++) {
      layPreg = (DataLayout) listaPlantilla.elementAt(t);
      if ( (!layPreg.getTipoPreg().equals("X")) &&
          (!layPreg.getTipoPreg().equals("D"))) {
        if (layPreg.getCondicionada().equals("S")) {
          //busco el valor de la pregunta condicionante
          preg = buscarPregListaValores(layPreg.getCodPreguntaCond(), lisCopia);
          if (preg != null) {
            if (!preg.respuesta.equals(layPreg.getDesPreguntaCond())) {
              // se borra de la lista de valores
              preg1 = buscarPregListaValores(layPreg.getCodPregunta(), lisCopia);
              if (preg1 != null) {
                listaResp2.removeElement(preg1);
              }
            }
          }
          else {
            // se borra de la lista de valores
            preg1 = buscarPregListaValores(layPreg.getCodPregunta(), lisCopia);
            if (preg1 != null) {
              listaResp2.removeElement(preg1);
            }
          }
        }
      }
    }
    CLista listaOrdenada = new CLista();
    listaOrdenada = Ordenar_listaRespuestas(listaResp2);
    return (listaOrdenada);
  }

  public CLista Ordenar_listaRespuestas(CLista lista) {

    if (lista.size() == 0) {
      return lista;
    }

    CLista listaOrdenadatmp = new CLista();
    datosPreg dat = (datosPreg) lista.firstElement();
    listaOrdenadatmp.addElement(dat);

    int iPosicion = 0;
    boolean cadaboolean = false;
    int cada = 0;
    int cadatmp = 0;
    for (int i = 1; i < lista.size(); i++) {
      datosPreg datcada = (datosPreg) lista.elementAt(i);
      cada = datcada.linea;
      cadaboolean = false;

      for (int j = 0; j < listaOrdenadatmp.size(); j++) {
        if (!cadaboolean) {

          datosPreg datcadatmp = (datosPreg) listaOrdenadatmp.elementAt(j);
          cadatmp = datcadatmp.linea;
          if (cada > cadatmp) {
            //continua...de momento detras
            iPosicion = j + 1;
          }
          if (cada < cadatmp) {
            iPosicion = iPosicion;
            cadaboolean = true;
          }
        }
      } //for

      listaOrdenadatmp.insertElementAt(datcada, iPosicion);
    } //for

    return (listaOrdenadatmp);
  }

  private datosPreg buscarPregListaValores(String cod, CLista lista) {
    datosPreg preg = null;
    for (int i = 0; i < lista.size(); i++) {
      preg = (datosPreg) lista.elementAt(i);
      if (preg.pregunta.equals(cod)) {
        return preg;
      }
    }
    return null;
  }

  public void Pintar_Valores() {
    /*
      // System_out.println("LAYOUT");
      for (int j = 0; j < listadatos.size(); j++){
        DataLayout data =(DataLayout) listadatos.elementAt(j);
        System_out.println(data.getCodModelo() + "   " +
        data.getCodPregunta()  + "   " + data.getNumLinea()
        + "   " +data.getDesTexto()+ "   " + data.getCA()
        + "   " +data.getNIVEL_1()+ "   " + data.getNIVEL_2() );
      }
      // System_out.println("RESPEDO");
      for (int i = 0; i < listaValores.size(); i++){
        datosPreg datap =(datosPreg) listaValores.elementAt(i);
          System_out.println(datap.modelo + "   " +
          datap.linea + "   " +
          datap.pregunta
           + "   " +datap.respuesta );
      }*/
  }

//Boton reset
  void button1_actionPerformed(ActionEvent e) {

    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    for (int i = 0; i < Num_Choices; i++) {
      CChoice CSelec;
      CSelec = (CChoice) cbo.elementAt(i);
      CSelec.select(0);
      objfocusLost(new FocusEvent( (Component) CSelec,
                                  java.awt.event.FocusEvent.FOCUS_LOST));
    }
    for (int i = 0; i < Num_Cajas; i++) {
      CTextField TSelec;
      TSelec = (CTextField) txt.elementAt(i);
      TSelec.setText("");
      objfocusLost(new FocusEvent( (Component) TSelec,
                                  java.awt.event.FocusEvent.FOCUS_LOST));
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  } //fin boton reset

  void button2_actionPerformed(ActionEvent e) {

    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    // Buscar los partes
    if (dialogo.listaPartes.size() == 0) {
      dialogo.recuperarPartes(new parNotAdic(0, dialogo.cd_enf, dialogo.ds_enf,
                                             dialogo.casos,
                                             dialogo.totalPartes, dialogo.ano,
                                             dialogo.semana, dialogo.equipo,
                                             dialogo.fNotif, dialogo.fRecep,
                                             dialogo.nivel1, dialogo.nivel2,
                                             dialogo.Ca));
    }
    else {
      CLista resul = new CLista();
      CMessage msgBox = null;

      try {

        CLista data = new CLista();
        //listaParte, DataEnfEnt, listalineasM
        data.setLogin(getApp().getLogin());
        data.addElement(dialogo.listaPartes);
        DataEnfEnt datosEnt = new DataEnfEnt(sCod, sCa, sNivel1, sNivel2);
        data.addElement(datosEnt);
        data.addElement(this.Pasar_DataLayout_DataLineasM());
        data.addElement(NIVEL);

        stub.setUrl(new URL(app.getURL() + strSERVLET_RESP));
        resul = (CLista) stub.doPost(servletRECARGAR_PARTES, data);
        //recogere listaPartes para pillar ope, fc_ultact
        if (resul != null) {
          boolean res = ( (Boolean) resul.firstElement()).booleanValue();
          if (res) {
            dialogo.listaPartes = (Vector) resul.elementAt(1);
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  resour.getString("msg8.Text"));
            msgBox.show();
            msgBox = null;
          }
          else {
            //se ha cambiado el layout y no se recargan
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  (String) resul.elementAt(1));
            msgBox.show();
            msgBox = null;
            return;
          }
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msgBox.show();
        msgBox = null;
        return;
      }
    }
    if (dialogo.listaPartes.size() > 0) {
      dialogo.parte = (datosParte) dialogo.listaPartes.elementAt(0);
      dialogo.parte.nivel1 = dialogo.nivel1;
      dialogo.parte.nivel2 = dialogo.nivel2;
      dialogo.parte.Ca = dialogo.Ca;
      //parte.ope = ope;
      //parte.fUltAct = fUltAct;
      dialogo.parteAct = 1;
      dialogo.barra.setText(resour.getString("dialogo.barra.Text") +
                            dialogo.parteAct);
    }
    else {
      dialogo.parte = new datosParte(dialogo.cd_enf, -1, "E",
                                     dialogo.ano, dialogo.semana,
                                     dialogo.equipo,
                                     dialogo.fNotif, dialogo.fRecep,
                                     dialogo.nivel1, dialogo.nivel2, dialogo.Ca,
                                     "", "");
      dialogo.parteAct = 0;
      dialogo.barra.setText(resour.getString("dialogo.barra1.Text"));
    }

    setDatosParte(dialogo.parte);

    totalCasos = dialogo.casos;

    parteValido = dialogo.parte;

    listaValores = dialogo.parte.preguntas;

    //IraBuscarDatosLayout();

    dialogo.lblPartes.setText(resour.getString("dialogo.lblPartes.Text") +
                              dialogo.totalPartes);

    mostrarParte(parteValido);

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  } //fin boton reset

//true :  no faltan campos obligatorios
//false:  faltan campos obligatorios
  public boolean ValidarObligatorios_Simple() {

    boolean bResultado = false;
    CMessage msgBox = null;

    LostGained(); //Perdida de foco, para validar el ultimo

    //voy leyendo cajas: flag, leo listas y flag
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;

    boolean bUnoEncontradoCaja = false;
    boolean bUnoEncontradoChoice = false;

    Object CompVolvera = new Object();
    Object CompVolveraCaja = new Object();
    Object CompVolveraChoice = new Object();

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      if (txtEtiqueta.getObligatorio().equals("S") &&
          txtEtiqueta.getText().equals("")) {
        bUnoEncontradoCaja = true;
        CompVolveraCaja = (CTextField) txtEtiqueta;
        txtEtiqueta.requestFocus();
        break;
      }
    }
    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      if (cboEtiqueta.getSelectedIndex() == -1 &&
          cboEtiqueta.getObligatorio().equals("S")) {
        bUnoEncontradoChoice = true;
        CompVolveraChoice = (CChoice) cboEtiqueta;
        cboEtiqueta.requestFocus();
        break;
      }
      if (cboEtiqueta.getObligatorio().equals("S") &&
          cboEtiqueta.getSelectedIndex() != -1 &&
          cboEtiqueta.getItem(cboEtiqueta.getSelectedIndex()).equals("") &&
          !bUnoEncontradoChoice) {
        bUnoEncontradoChoice = true;
        CompVolveraChoice = (CChoice) cboEtiqueta;
        cboEtiqueta.requestFocus();
        break;
      }
    }
    //veo cual queda mas alto
    //solo choice
    if (bUnoEncontradoCaja && bUnoEncontradoChoice) {

      cboEtiqueta = (CChoice) CompVolveraChoice;
      txtEtiqueta = (CTextField) CompVolveraCaja;
      float yChoice = cboEtiqueta.getAlignmentY();
      float yCaja = txtEtiqueta.getAlignmentY();

      if (yChoice > yCaja) {
        CompVolvera = (CChoice) CompVolveraChoice;
      }
      else {
        CompVolvera = (CTextField) CompVolveraCaja;
      }
    }
    if (bUnoEncontradoCaja && !bUnoEncontradoChoice) {

      txtEtiqueta = (CTextField) CompVolveraCaja;
      CompVolvera = (CTextField) CompVolveraCaja;
    }
    if (!bUnoEncontradoCaja && bUnoEncontradoChoice) {

      cboEtiqueta = (CChoice) CompVolveraChoice;
      CompVolvera = (CChoice) CompVolveraChoice;
    }

    //mensaje y foco o continuo
    if (bUnoEncontradoCaja || bUnoEncontradoChoice) {
      //mensaje   //foco
      bResultado = false;
      msgBox = new CMessage(app, CMessage.msgAVISO,
                            resour.getString("msg9.Text"));
      msgBox.show();
      msgBox = null;
      if (CompVolvera.getClass().getName().equals("infproto.CChoice")) {
        cboEtiqueta = (CChoice) CompVolvera;
        this.requestFocus();
        cboEtiqueta.requestFocus();

      }
      else {
        txtEtiqueta = (CTextField) CompVolvera;
        this.requestFocus();
        txtEtiqueta.requestFocus();

      }
      bResultado = false;
    }
    else {

      bResultado = true;

      //
      parteValido = this.getListaParaInsertar(); //CUIDADIN!!!
      setDatosParte(parteValido);

      if (parteValido.preguntas.size() == 0) {
        if (iEstado == ALTA) {
          msgBox = new CMessage(app, CMessage.msgAVISO,
                                resour.getString("msg10.Text"));
          msgBox.show();
          msgBox = null;
          bResultado = false;
        }
        else if (iEstado == MODIFICACION) {
          msgBox = new CMessage(app, CMessage.msgAVISO,
                                resour.getString("msg11.Text"));
          msgBox.show();
          msgBox = null;

          //paso al menos el nm_edo (caso)
          datosPreg preg = null;
          //parte valido lleva el parte
          preg = new datosPreg( -1, null, null, "", "", "", "", "", "");
          parteValido.preguntas.addElement(preg);

          bResultado = true;
        }
      }
      else if (listaInsertar.size() > 0) {
        bResultado = true;
      }

      //
    }
    return (bResultado);
  }

//PARA INSERTAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//true : si listo para insertar
//true:   si modifica borrando todos los datos.
//false:  faltan campos obligatorios
//false:  si es un alta, pulsaria aceptar, pero no lleva datos
  public boolean ValidarObligatorios() {

    //parteValido = new datosParte();

    boolean bResultado = false;
    CMessage msgBox = null;

    LostGained(); //Perdida de foco, para validar el ultimo

    //voy leyendo cajas: flag, leo listas y flag
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;

    boolean bUnoEncontradoCaja = false;
    boolean bUnoEncontradoChoice = false;

    Object CompVolvera = new Object();
    Object CompVolveraCaja = new Object();
    Object CompVolveraChoice = new Object();

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      if (txtEtiqueta.getObligatorio().equals("S") &&
          txtEtiqueta.getText().equals("")) {
        bUnoEncontradoCaja = true;
        CompVolveraCaja = (CTextField) txtEtiqueta;
        txtEtiqueta.requestFocus();
        break;
      }
    }
    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      if (cboEtiqueta.getSelectedIndex() == -1 &&
          cboEtiqueta.getObligatorio().equals("S")) {
        bUnoEncontradoChoice = true;
        CompVolveraChoice = (CChoice) cboEtiqueta;
        cboEtiqueta.requestFocus();
        break;
      }
      if (cboEtiqueta.getObligatorio().equals("S") &&
          cboEtiqueta.getSelectedIndex() != -1 &&
          cboEtiqueta.getItem(cboEtiqueta.getSelectedIndex()).equals("") &&
          !bUnoEncontradoChoice) {
        bUnoEncontradoChoice = true;
        CompVolveraChoice = (CChoice) cboEtiqueta;
        cboEtiqueta.requestFocus();
        break;
      }
    }
    //veo cual queda mas alto
    //solo choice
    if (bUnoEncontradoCaja && bUnoEncontradoChoice) {

      cboEtiqueta = (CChoice) CompVolveraChoice;
      txtEtiqueta = (CTextField) CompVolveraCaja;
      float yChoice = cboEtiqueta.getAlignmentY();
      float yCaja = txtEtiqueta.getAlignmentY();

      if (yChoice > yCaja) {
        CompVolvera = (CChoice) CompVolveraChoice;
      }
      else {
        CompVolvera = (CTextField) CompVolveraCaja;
      }
    }
    if (bUnoEncontradoCaja && !bUnoEncontradoChoice) {

      txtEtiqueta = (CTextField) CompVolveraCaja;
      CompVolvera = (CTextField) CompVolveraCaja;
    }
    if (!bUnoEncontradoCaja && bUnoEncontradoChoice) {

      cboEtiqueta = (CChoice) CompVolveraChoice;
      CompVolvera = (CChoice) CompVolveraChoice;
    }

    //mensaje y foco o continuo
    if (bUnoEncontradoCaja || bUnoEncontradoChoice) {
      //mensaje   //foco
      bResultado = false;
      msgBox = new CMessage(app, CMessage.msgAVISO,
                            resour.getString("msg9.Text"));
      msgBox.show();
      msgBox = null;
      if (CompVolvera.getClass().getName().equals("infproto.CChoice")) {
        cboEtiqueta = (CChoice) CompVolvera;
        this.requestFocus();
        cboEtiqueta.requestFocus();

      }
      else {
        txtEtiqueta = (CTextField) CompVolvera;
        this.requestFocus();
        txtEtiqueta.requestFocus();

      }
      bResultado = false;
    }
    else {

      bResultado = true; //de momento
      parteValido = this.getListaParaInsertar(); //CUIDADIN!!!
      setDatosParte(parteValido);

      if (parteValido.preguntas.size() == 0) {
        if (iEstado == ALTA) {
          msgBox = new CMessage(app, CMessage.msgAVISO,
                                resour.getString("msg10.Text"));
          msgBox.show();
          msgBox = null;
          bResultado = false;
        }
        else if (iEstado == MODIFICACION) {
          msgBox = new CMessage(app, CMessage.msgAVISO,
                                resour.getString("msg11.Text"));
          msgBox.show();
          msgBox = null;

          //paso al menos el nm_edo (caso)
          datosPreg preg = null;
          //parte valido lleva el parte
          preg = new datosPreg( -1, null, null, "", "", "", "", "", "");
          parteValido.preguntas.addElement(preg);

          bResultado = true;
        }
      }
      else if (listaInsertar.size() > 0) {
        bResultado = true;
      }
    }

    return (bResultado);
  }

  public datosParte getListaParaInsertar() {

    CTextField txtEtiqueta;
    CChoice cboEtiqueta;

    listaInsertar.removeAllElements();
    datosParte nParte = new datosParte(sCod, (new Integer(sSec)).intValue(),
                                       "E", sAno, sSem,
                                       sEq, sFN, sFR,
                                       sNivel1, sNivel2, sCa,
                                       parteValido.ope,
                                       parteValido.fUltAct);

    datosPreg nPreg = null;
    //los valores debemos meterlos en orden del layout
    for (int i = 0; i < listadatos.size(); i++) {
      DataLayout datos = (DataLayout) listadatos.elementAt(i);
      if (datos.getCodPregunta() != null) {

        //CAJAS
        if (datos.getTipoPreg().equals("N") ||
            datos.getTipoPreg().equals("C") ||
            datos.getTipoPreg().equals("F")) {
          for (int ind = 0; ind < Num_Cajas; ind++) {
            txtEtiqueta = (CTextField) txt.elementAt(ind);
            if (datos.getCodModelo().equals(txtEtiqueta.getCodM()) &&
                datos.getNumLinea().equals(txtEtiqueta.getNum())) {
              //----
              if (!txtEtiqueta.getText().equals("")) {
                if (txtEtiqueta.getTipoPregunta().equals("N")) {
                  nPreg = new datosPreg( (new Integer(txtEtiqueta.getNum())).
                                        intValue(),
                                        txtEtiqueta.getCodPregunta(),
                                        txtEtiqueta.getNumSinFormato().trim(),
                                        txtEtiqueta.getCodM(), "", "", "", "",
                                        "");
                  nPreg.ValorLista = null;
                }
                else {
                  nPreg = new datosPreg( (new Integer(txtEtiqueta.getNum())).
                                        intValue(),
                                        txtEtiqueta.getCodPregunta(),
                                        txtEtiqueta.getText().trim(),
                                        txtEtiqueta.getCodM(), "", "", "", "",
                                        "");
                  nPreg.ValorLista = null;
                }
                nParte.preguntas.addElement(nPreg);
              } //-----

            }
          } //for de buscar en cajas
        } //cajas
        //LISTAS
        else {
          for (int ind = 0; ind < Num_Choices; ind++) {
            cboEtiqueta = (CChoice) cbo.elementAt(ind);
            if (datos.getCodModelo().equals(cboEtiqueta.getCodM()) &&
                datos.getNumLinea().equals(cboEtiqueta.getNum())) {
              if (cboEtiqueta.getSelectedIndex() != -1) {
                if (!cboEtiqueta.getItem(cboEtiqueta.getSelectedIndex()).equals(
                    "")) {
                  nPreg = new datosPreg( (new Integer(cboEtiqueta.getNum())).
                                        intValue(),
                                        cboEtiqueta.getCodPregunta(),
                                        ValorSeleccionadoChoiceNumero(
                      cboEtiqueta),
                                        cboEtiqueta.getCodM(), "", "", "", "",
                                        "");
                  nPreg.ValorLista = cboEtiqueta.getSelectedItem();
                  nParte.preguntas.addElement(nPreg);
                }
              }
            }
          } //for de buscar en choices
        } //listas
      }
    }
    //------------------------------------------------

    return nParte;
  }

  protected void limpiarRespuestas() {
    CTextField txtEtiqueta;
    CChoice cboEtiqueta;

    for (int ind = 0; ind < Num_Cajas; ind++) {
      txtEtiqueta = (CTextField) txt.elementAt(ind);
      txtEtiqueta.setText("");
    } //cajas

    for (int ind = 0; ind < Num_Choices; ind++) {
      cboEtiqueta = (CChoice) cbo.elementAt(ind);
      cboEtiqueta.select(0);

    } //choices

    //iEstado = ALTA;
    modoOperacion = modoCondicionadaDeshabilitar;
    Inicializar();
    this.doLayout();

    //LostGained();

  }

  protected void mostrarParte(datosParte parte) {
    listaValores = parte.preguntas;
    parteValido = parte;
    setDatosParte(parteValido);

    //descargarChoices();
    limpiarRespuestas();

    //iEstado = MODIFICACION;
    modoOperacion = modoCondicionadaDeshabilitar;
    Inicializar();
    modoOperacion = modoCargarPlantilla;
    Inicializar();
    this.doLayout();
  }

  public boolean borrarParte() {
    CMessage msgBox = null;
    CLista param = null;
    CLista resul = null;

    boolean borrado = false;

    try {

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      //param(0): parte que se va a borrar
      param = new CLista();
      param.setLogin(getApp().getLogin());
      param.addElement(parteValido);

      // apunta al servlet
      stub.setUrl(new URL(this.app.getURL() + strSERVLET_RESP));
      //param(0): parte que se va a borrar
      //no se comprueba el bloqueo
      resul = (CLista)this.stub.doPost(servletCOMP_BORR, param);
      //va bien: listaSalida = (true) o
      //         listaSalida = (true, mensaje pregunta)
      //va mal: listaSalida = null y exc
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      if (resul != null) {
        Boolean res = (Boolean) resul.elementAt(0);
        if (res.booleanValue()) {
          if (resul.size() == 1) {
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  resour.getString("msg12.Text"));
            msgBox.show();
            msgBox = null;
            parteValido = null;
            borrado = true;
          }
          else {
            //pregunta si se quiere borrar aunque alguien lo haya modificado
            String pre = (String) resul.elementAt(1);
            msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                  pre);
            msgBox.show();
            if (msgBox.getResponse()) {
              setCursor(new Cursor(Cursor.WAIT_CURSOR));
              msgBox = null;
              //borrar
              param = new CLista();
              param.setLogin(getApp().getLogin());
              param.addElement(parteValido);
              //param(0): parte que se va a borrar
              //no se comprueba le bloqueo
              stub.setUrl(new URL(this.app.getURL() + strSERVLET_RESP));
              resul = (CLista)this.stub.doPost(servletBORRADO, param);
              //va bien: listaSalida.size()=0
              //va mal: listaSalida = null y exc
              if (resul != null) {
                if (resul.size() == 0) {
                  msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                        resour.getString("msg13.Text"));
                  msgBox.show();
                  msgBox = null;
                  parteValido = null;
                  borrado = true;
                }
              }
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
            else {
              //el usuario decide no borrar
              borrado = false;
            }
          }
        }
      }
      // error en el proceso
    }
    catch (Exception e) {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      borrado = false;
    }

    return borrado;
  }

  public void Actualizar_Todos() {

    CLista resul = new CLista();
    CMessage msgBox = null;

    try {

      CLista data = new CLista();
      //listaParte, DataEnfEnt, listalineasM
      data.setLogin(getApp().getLogin());
      data.addElement(dialogo.listaPartes);
      DataEnfEnt datosEnt = new DataEnfEnt(sCod, sCa, sNivel1, sNivel2);
      data.addElement(datosEnt);
      data.addElement(this.Pasar_DataLayout_DataLineasM());
      data.addElement(NIVEL);

      stub.setUrl(new URL(app.getURL() + strSERVLET_RESP));
      resul = (CLista) stub.doPost(servletCOMP_ACTUALIZAR_PARTES, data);
      //recogere listaPartes para pillar ope, fc_ultact
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
      //reserva y volver a pintar el layout y dejar en el actual
      Volver_a_Reserva();
      Volver_a_Abrir(); //LAYOUT
      return;
    }

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    Boolean res = (Boolean) resul.elementAt(0);
    //SALIR
    if (!res.booleanValue()) {
      String error = (String) resul.elementAt(1);
      msgBox = new CMessage(this.app, CMessage.msgERROR, error);
      msgBox.show();
      msgBox = null;
      //reserva y volver a pintar el layout y dejar en el actual
      Volver_a_Reserva();
      Volver_a_Abrir(); //LAYOUT

      return;
    }
    String pre = "";
    try {
      //pregunta si se quiere sobreescribir el parte
      pre = (String) resul.elementAt(1);
    }
    catch (ClassCastException e) {
      dialogo.listaPartes = (Vector) resul.elementAt(1);
      //Cris
      parteValido = (datosParte) dialogo.listaPartes.elementAt(dialogo.
          listaPartes.size() - 1);
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            resour.getString("msg14.Text"));
      msgBox.show();
      msgBox = null;
      return; //FIN
    }
    //ALGUIEN LO HA MODIFICADO
    msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, pre);
    msgBox.show();
    //si, quiero //sobreescribir
    if (msgBox.getResponse()) {
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      msgBox = null;

      try {
        CLista param = new CLista();
        param.setLogin(getApp().getLogin());
        param.addElement(dialogo.listaPartes);
        stub.setUrl(new URL(this.app.getURL() + strSERVLET_RESP));
        resul = (CLista)this.stub.doPost(servletACTUALIZAR_PARTES, param);
        param = null;
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
        msgBox.show();
        msgBox = null;
        //reserva y volver a pintar el layout y dejar en el actual
        Volver_a_Reserva();
        Volver_a_Abrir(); //LAYOUT
        return;
      }
      //va bien: listaSalida = (parte)
      //va mal: listaSalida = null y exc
      if (resul != null) {
        if (resul.size() == 1) {
          dialogo.listaPartes = (Vector) resul.elementAt(0);
          //Cris
          parteValido = (datosParte) dialogo.listaPartes.elementAt(dialogo.
              parteAct - 1);
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                resour.getString("msg14.Text"));
          msgBox.show();
          msgBox = null;
          return;
        }
      }
    }
    //NO QUIERE SOBREESCRIBIR
    else {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      String salir = resour.getString("msg15.Text");
      msgBox = new CMessage(this.app, CMessage.msgERROR, salir);
      msgBox.show();
      msgBox = null;
      //reserva y volver a pintar el layout y dejar en el actual
      Volver_a_Reserva();
      Volver_a_Abrir(); //LAYOUT
      return;
    }

  }

// Se modifica un parte comprobando que otro usuario lo haya borrado
// o modificado desde que se obtuvo de la BD
  public datosParte modificarParte(datosParte parte) {
    CMessage msgBox = null;
    CLista param = null;
    CLista resul = null;

    try {

      setCursor(new Cursor(Cursor.WAIT_CURSOR));

      param = new CLista();
      param.setLogin(getApp().getLogin());
      param.addElement(parte);
      param.addElement(new Integer(totalCasos));
      //cris
      param.addElement(new DataEnfEnt(parte.cd_enf, sCa, sNivel1, sNivel2));
      param.addElement(Pasar_DataLayout_DataLineasM());

      //param(0): parte que se va a dar de alta
      //param(1): casos de la declaracion
      //param(2): entorno para el layout
      //param(3): lista del layout

      stub.setUrl(new URL(this.app.getURL() + strSERVLET_RESP));
      resul = (CLista)this.stub.doPost(servletCOMP_MOD, param);
      //va bien: listaSalida = (true, parte) o
      //         listaSalida = (true, mensaje pregunta) o
      //         listaSalida = (false, mensaje error)
      //va mal: listaSalida = null y exc

      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      if (resul != null) {
        Boolean res = (Boolean) resul.elementAt(0);
        if (res.booleanValue()) {
          try {
            //pregunta si se quiere sobreescribir el parte
            String pre = (String) resul.elementAt(1);
            msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                  pre);
            msgBox.show();
            if (msgBox.getResponse()) {
              setCursor(new Cursor(Cursor.WAIT_CURSOR));
              msgBox = null;
              //sobreescribir
              param = new CLista();
              param.setLogin(getApp().getLogin());
              param.addElement(parte);
              //param(0): parte que se va a dar de alta
              stub.setUrl(new URL(this.app.getURL() + strSERVLET_RESP));
              resul = (CLista)this.stub.doPost(servletMODIFICACION, param);
              //va bien: listaSalida = (parte)
              //va mal: listaSalida = null y exc
              if (resul != null) {
                if (resul.size() == 1) {
                  msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                        resour.getString("msg16.Text"));
                  msgBox.show();
                  msgBox = null;
                  parte = (datosParte) resul.elementAt(0);

                }
              }
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
            else {
              limpiarRespuestas();
              setCursor(new Cursor(Cursor.WAIT_CURSOR));
              dialogo.barra.setText(resour.getString("dialogo.barra2.Text"));
              //el usuario decide no sobreescribir
              //se cargan lo nuevos datos del parte

              //NOTA : creo que no es necesario comprobar el layout
              //porque antes se ha visto que era el mismo

              //CUIDADO porque podria, aunque seria muy raro, haberse
              //cambiado el layout en esos instantes

              param = new CLista();
              param.setLogin(getApp().getLogin());
              param.addElement(parte);
              //param(0): parte que se va a dar de alta
              stub.setUrl(new URL(this.app.getURL() + strSERVLET_RESP));
              resul = (CLista)this.stub.doPost(servletUN_PARTE, param);
              //va bien: listaSalida = (parte)
              //va mal: listaSalida = null y exc
              if (resul != null) {
                parte = (datosParte) resul.elementAt(0);
                mostrarParte(parte);
              }
              else {
                msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                      resour.getString("msg17.Text"));
                msgBox.show();
                msgBox = null;
              }
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
          }
          catch (ClassCastException e) {
            // todo ha ido bien y lo que trae es el parte modificado
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  resour.getString("msg16.Text"));
            msgBox.show();
            msgBox = null;
            parte = (datosParte) resul.elementAt(1);

          }
        }
        else {
          String error = (String) resul.elementAt(1);
          msgBox = new CMessage(this.app, CMessage.msgERROR, error);
          msgBox.show();
          msgBox = null;
        }
      }
      // error en el proceso
    }
    catch (Exception e) {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      return null;

    }

    parteValido = parte;

    return parte;
  }

//m�luisa
  public CLista Pasar_DataLayout_DataLineasM() {
    CLista lista = new CLista();

    for (int i = 0; i < listadatos.size(); i++) {
      DataLayout data = (DataLayout) listadatos.elementAt(i);

      if (data.getCodModelo() != null) { //--------------------

        DataLineasM datosM = new DataLineasM(data.getCodModelo(),
                                             data.getNumLinea(), "", "",
                                             "", "", "");

        lista.addElement(datosM);

      } //-------------------------------------------------------
    }
    return (lista);
  }

// Se inserta un parte nuevo comprobando que el numero de partes
// no supere al numero de casos
  public datosParte insertarParte() {
    CMessage msgBox = null;
    CLista param = null;
    CLista resul = null;

    try {

      setCursor(new Cursor(Cursor.WAIT_CURSOR));

      param = new CLista();
      param.setLogin(getApp().getLogin());
      param.addElement(parteValido);
      param.addElement(new Integer(totalCasos));
      //cris
      param.addElement(new DataEnfEnt(parteValido.cd_enf, sCa, sNivel1, sNivel2));
      param.addElement(Pasar_DataLayout_DataLineasM());
      //param(0): parte que se va a dar de alta
      //param(1): casos de la declaracion
      //param(2): entorno para el layout
      //param(3): lista del layout

      stub.setUrl(new URL(this.app.getURL() + strSERVLET_RESP));
      resul = (CLista)this.stub.doPost(servletALTA, param);
      //va bien: listaSalida = (true, parte) o listaSalida = (false, mensaje error)
      //va mal: listaSalida = null y exc

      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      if (resul != null) {
        Boolean res = (Boolean) resul.elementAt(0);
        if (res.booleanValue()) {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                resour.getString("msg18.Text"));
          msgBox.show();
          msgBox = null;
          parteValido = (datosParte) resul.elementAt(1);
        }
        else {
          String error = (String) resul.elementAt(1);
          msgBox = new CMessage(this.app, CMessage.msgERROR, error);
          msgBox.show();
          msgBox = null;
          //Cris
          return null; //no se ha insertado ningun parte

          //cris
          // ATENCION !!!
          // ahora creo que no se va a permitir actualizar los partes
          /*
                   msgBox = new CMessage(this.app, CMessage.msgPREGUNTA , resour.getString("msg19.Text"));
                   msgBox.show();
                   if (msgBox.getResponse()){
                   // Actualiza los partes
                   }
                   else{
            if (dialogo.listaPartes.size()!=0)
              mostrarParte((datosParte) dialogo.listaPartes.elementAt(0));
            else
              limpiarRespuestas();
                   }
                   msgBox = null;
           */

        }
      }
      // error en el proceso
    }
    catch (Exception e) {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      return null;

    }

    return parteValido;
  }

///////////////////////////////////////////////////

  public void setDatosParte(datosParte datosEntrada) {

    sCod = datosEntrada.cd_enf;
    //sDes =  datosEntrada.ds_enf;
    sNivel1 = datosEntrada.nivel1;
    sNivel2 = datosEntrada.nivel2;
    sCa = datosEntrada.Ca;

    sEq = datosEntrada.equipo;
    sAno = datosEntrada.ano;
    sSem = datosEntrada.semana;
    sFR = datosEntrada.fRecep;
    sFN = datosEntrada.fNotif;
    sOpe = datosEntrada.ope;
    sFU = datosEntrada.fUltAct;

    sSec = (new Integer(datosEntrada.secuencial)).toString();
    sTSive = datosEntrada.tSive;

  }

  public boolean getNoSeEncontraronDatos() {
    return (NoSeEncontraronDatos);
  }

} //FIN DE CLASE PPAL******************************************

class Foco
    implements FocusListener {
  panelInfNotAdic adaptee;
  FocusEvent e = null;

  Foco(panelInfNotAdic adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.objfocusLost(e);
  }

  public void focusGained(FocusEvent e) {
    adaptee.objfocusGained(e);
  }
}

class Item
    implements ItemListener {
  panelInfNotAdic adaptee;
  ItemEvent e = null;

  Item(panelInfNotAdic adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.objitemchange(e);
  }

}

//Boton Reset
class panelInfNotAdic_button1_actionAdapter
    implements java.awt.event.ActionListener {
  panelInfNotAdic adaptee;

  panelInfNotAdic_button1_actionAdapter(panelInfNotAdic adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.button1_actionPerformed(e);
  }
}

class panelInfNotAdic_button2_actionAdapter
    implements java.awt.event.ActionListener {
  panelInfNotAdic adaptee;

  panelInfNotAdic_button2_actionAdapter(panelInfNotAdic adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.button2_actionPerformed(e);
  }
}

// configuraci�n del �rbol
class NodoEDO {
  protected int y;
  protected String s;

  public NodoEDO(String s, int y) {
    this.y = y;
    this.s = s;
  }

  String getDescripcion() {
    return s;
  }

  int getY() {
    return y;
  }
}

//Botones
class Panel_Informe_Completo_button_actionAdapter
    implements java.awt.event.ActionListener {
  panelInfNotAdic adaptee;

  Panel_Informe_Completo_button_actionAdapter(panelInfNotAdic adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.button_actionPerformed(e);
  }
}
