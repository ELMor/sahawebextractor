//Title:        Notificacion datos adicionales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Notificacion de enfermedades EDO de declaracion numerica
//que necesitan datos adicionales.

package notadic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import infprotoedo.DataEnfEnt;
import infprotoedo.DataEnfEntCaso;
import infprotoedo.DataLineasM;
import infprotoedo.DataRespCompleta;
import sapp.DBServlet;

public class SrvNotAdic
    extends DBServlet {

  // inserciones en sive_edo_dadic y sive_resp_adic
  final int servletALTA = 0; //
  // borrado e insercion sive_resp_adic y sive_edo_dadic
  final int servletMODIFICAR = 1;
  // recuperacion de los partes
  final int servletPARTES = 2; //
  // modificacion con comprobacion
  final int servletCOMP_MOD = 3; //
  // borrado con comprobacion
  final int servletCOMP_BORR = 4; //
  // modificacion
  final int servletMODIFICACION = 5;
  //borrado
  final int servletBORRADO = 6; //

  final int servletUN_PARTE = 7; //

  //cuando se actualiza el protocolo comprueba si se
  //deben guardar todos los partes
  final int servletACTUALIZAR_PARTES = 8; //
  final int servletCOMP_ACTUALIZAR_PARTES = 9; //

  final int servletRECARGAR_PARTES = 10; //

  final String sQuery = "SELECT CD_NIVEL_1, CD_NIVEL_2, "
      + " CD_CA , IT_OK FROM SIVE_MODELO WHERE "
      + " CD_TSIVE = ? AND CD_MODELO = ?";

  final String sALTA_CAB = "insert into SIVE_EDO_DADIC "
      + "(CD_ENFCIE, SEQ_CODIGO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, "
      + " FC_FECNOTIF, FC_RECEP, CD_OPE, FC_ULTACT) "
      + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

  final String sALTA_RESP = "insert into SIVE_RESP_ADIC "
      + "(CD_ENFCIE, CD_E_NOTIF, CD_ANOEPI, CD_TSIVE, CD_SEMEPI, "
      + " FC_FECNOTIF, FC_RECEP, CD_MODELO, SEQ_CODIGO, "
      + " NM_LIN, CD_PREGUNTA, DS_RESPUESTA) "
      + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  final String sBORRAR_RESP = "DELETE FROM SIVE_RESP_ADIC "
      + " WHERE ( CD_ENFCIE = ? and CD_E_NOTIF = ? and "
      + " CD_ANOEPI = ? and CD_TSIVE = ? and CD_SEMEPI = ? "
      + " and FC_FECNOTIF = ? and FC_RECEP = ? and "
      + " SEQ_CODIGO = ? )";

  final String sBORRAR_RESP_TODOS = "DELETE FROM SIVE_RESP_ADIC "
      + " WHERE ( CD_ENFCIE = ? and CD_E_NOTIF = ? and "
      + " CD_ANOEPI = ? and CD_TSIVE = ? and CD_SEMEPI = ? "
      + " and FC_FECNOTIF = ? and FC_RECEP = ? )";

  final String sBORRAR_CAB = "DELETE FROM SIVE_EDO_DADIC "
      + " WHERE ( CD_ENFCIE = ? and SEQ_CODIGO = ? and "
      + " CD_E_NOTIF = ? and  CD_ANOEPI = ? and CD_SEMEPI = ? "
      + " and FC_FECNOTIF = ? and FC_RECEP = ? )";

  final String sSELECT_VALORES = " select "
      + " a.DS_LISTAP, a.DSL_LISTAP "
      + "from SIVE_LISTA_VALORES a, SIVE_PREGUNTA b where "
      + "(b.CD_PREGUNTA = ? and a.CD_LISTAp = ? "
      + " and a.CD_LISTA = b.CD_LISTA)";

  //parche de nivel  (modos de actualizar)
  String NIVEL = "";

  //fechas
  java.util.Date dFecha;
  java.sql.Date sqlFec;
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
  SimpleDateFormat formUltAct = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
      new Locale("es", "ES"));
  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    NIVEL = "";

    //fechas
    dFecha = new java.util.Date();
    sqlFec = new java.sql.Date(dFecha.getTime());
    formater = new SimpleDateFormat("dd/MM/yyyy");
    formUltAct = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                      new Locale("es", "ES"));

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    int iEstado;

    int j = 0;

    Vector resConsulta = new Vector();

    // objetos de datos
    CLista data = new CLista();
    Vector listaPartesAct = new Vector();
    datosParte parte = null;
    datosPreg preg = null;
    parNotAdic parCons = null;

    DataEnfEnt entornoLayout = null;
    CLista listaLayout = new CLista();

    //para el informe
    // objetos de datos
    CLista listaSalida = new CLista();

    // campos
    String sCod = "";
    String equipo = "";
    String ano = "";
    String semana = "";
    String fRecep = "";
    String fNotif = "";
    String CodModelo = "";
    String sec = "";
    String Num = "";
    String CodPregunta = "";
    String Des = ""; //V
    String valor = ""; //Varon

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    try {
      // modos de operaci�n
      switch (opmode) {

        //Consulta de los partes de una declaracion
        case servletPARTES:

          //apa�o
          NIVEL = "";

          parCons = (parNotAdic) param.firstElement();

          // para recuperar el operador y la fecha de actualizacion
          // de cada uno de los partes

          Vector secs = new Vector();
          Vector ope_fec = new Vector();

          query = " select SEQ_CODIGO, CD_OPE, FC_ULTACT "
              + " from SIVE_EDO_DADIC "
              + " where CD_ENFCIE = ? "
              + " and CD_E_NOTIF = ? "
              + " and CD_ANOEPI = ? and CD_SEMEPI = ? "
              + " and FC_FECNOTIF = ? and FC_RECEP = ? "
              + " order by SEQ_CODIGO ";

          st = con.prepareStatement(query);

          st.setString(1, parCons.cd_enf);
          st.setString(2, parCons.equipo);
          st.setString(3, parCons.ano);
          st.setString(4, parCons.semana);
          dFecha = formater.parse(parCons.fNotif.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(5, sqlFec);
          dFecha = formater.parse(parCons.fRecep.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(6, sqlFec);

          rs = st.executeQuery();
          int ind = 0;
          Integer aux = null;
          String opFc[] = new String[2];
          while (rs.next()) {
            aux = new Integer(rs.getInt("SEQ_CODIGO"));
            secs.addElement(aux);
            ind = secs.indexOf(aux);
            opFc[0] = rs.getString("CD_OPE");
            opFc[1] = timestamp_a_cadena(rs.getTimestamp("FC_ULTACT"));

            ope_fec.insertElementAt(opFc, ind);
            opFc = new String[2];
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          query =
              " select CD_MODELO, SEQ_CODIGO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA "
              + " from SIVE_RESP_ADIC "
              + " where CD_ENFCIE = ? and CD_E_NOTIF = ? "
              + " and CD_ANOEPI = ? and CD_SEMEPI = ? "
              + " and CD_TSIVE = ? and FC_FECNOTIF = ? "
              + " and FC_RECEP = ? "
              + " order by SEQ_CODIGO, CD_MODELO, NM_LIN ";

          st = con.prepareStatement(query);

          st.setString(1, parCons.cd_enf);
          st.setString(2, parCons.equipo);
          st.setString(3, parCons.ano);
          st.setString(4, parCons.semana);
          st.setString(5, "E");
          dFecha = formater.parse(parCons.fNotif.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(6, sqlFec);
          dFecha = formater.parse(parCons.fRecep.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(7, sqlFec);

          rs = st.executeQuery();

          int ultSec = -1;
          String mod = "";
          int lin = 0;
          Integer isec = null;

          boolean hay = false;
          while (rs.next()) {
            opFc = new String[2];
            isec = new Integer(rs.getInt("SEQ_CODIGO"));
            mod = rs.getString("CD_MODELO");
            ind = secs.indexOf(isec);
            opFc = (String[]) ope_fec.elementAt(ind);

            if (ultSec != isec.intValue()) {
              if (ultSec != -1) {
                resConsulta.addElement(parte);
              }
              ultSec = isec.intValue();
              hay = true;

              parte = new datosParte(parCons.cd_enf, isec.intValue(), "E",
                                     parCons.ano, parCons.semana,
                                     parCons.equipo, parCons.fNotif,
                                     parCons.fRecep,
                                     "", "", "",
                                     opFc[0], opFc[1]);
              parte.preguntas.addElement(new datosPreg(rs.getInt(3),
                  rs.getString(4),
                  rs.getString(5), mod,
                  "", "", "", "", ""));
            }
            else {
              parte.preguntas.addElement(new datosPreg(rs.getInt(3),
                  rs.getString(4),
                  rs.getString(5), mod,
                  "", "", "", "", ""));
            }
          }
          if (hay) {
            resConsulta.addElement(parte);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //ahora se recorren los partes para buscar las listas
          //de valores de las preguntas

          parte = new datosParte();
          Vector pregs = new Vector();
          for (int i = 0; i < resConsulta.size(); i++) {

            parte = (datosParte) resConsulta.elementAt(i);
            pregs = parte.preguntas;
            preg = null;

            for (int s = 0; s < pregs.size(); s++) {
              preg = (datosPreg) pregs.elementAt(s);

              // lanza la query
              st = con.prepareStatement(sSELECT_VALORES);

              st.setString(1, preg.pregunta); //cd_pregunta
              st.setString(2, preg.respuesta); //cd_listap

              rs = st.executeQuery();
              //solo saldra un registro, o (ninguno: no se modifica)
              while (rs.next()) {
                //pregs.removeElementAt(s);

                String des = rs.getString("DS_LISTAP");
                String desL = rs.getString("DSL_LISTAP");

                if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                    && (desL != null)) {
                  valor = desL;
                }
                else {
                  valor = des;

                }
                preg.ValorLista = valor;
              }
              rs.close();
              rs = null;
              st.close();
              st = null;
              //completamos los datos de la estructura
              //veo si esos modelos son obligatorios y su nivel

              st = con.prepareStatement(sQuery);

              st.setString(1, "E");
              st.setString(2, preg.modelo);
              rs = st.executeQuery();
              String n1 = null;
              String n2 = null;
              String ca = null;
              String nivel = null; //"CNE"; "CA"; "N1", "N2"
              while (rs.next()) {
                n1 = rs.getString("CD_NIVEL_1");
                n2 = rs.getString("CD_NIVEL_2");
                ca = rs.getString("CD_CA");
                //"CNE"; "CA"; "N1", "N2"
                if (n1 == null && n2 == null && ca == null) {
                  nivel = "CNE";
                }
                else if (n1 == null && n2 == null && ca != null) {
                  nivel = "CA";
                }
                else if (n1 != null && n2 == null && ca != null) {
                  nivel = "N1";
                }
                else if (n1 != null && n2 != null && ca != null) {
                  nivel = "N2";

                }
                preg.nivel = nivel;
                preg.it_ok = rs.getString("IT_OK");
                preg.ca = ca;
                preg.n1 = n1;
                preg.n2 = n2;

              } //while
              rs.close();
              rs = null;
              st.close();
              st = null;

              pregs.removeElementAt(s);
              pregs.insertElementAt(preg, s);

            }
            resConsulta.removeElementAt(i);
            datosParte nuevo = new datosParte(parte.cd_enf, parte.secuencial,
                                              parte.tSive, parte.ano,
                                              parte.semana, parte.equipo,
                                              parte.fNotif, parte.fRecep,
                                              parte.nivel1, parte.nivel2,
                                              parte.Ca, parte.ope,
                                              parte.fUltAct);
            nuevo.preguntas = pregs;
            resConsulta.insertElementAt(nuevo, i);
          }

          listaSalida.addElement(resConsulta);

          parte = null;
          break;

          // recupera un parte modificado
        case servletUN_PARTE:

          //apa�o
          NIVEL = "";

          parte = (datosParte) param.firstElement();
          parte.preguntas = new Vector();

          // para recuperar el operador y la fecha de actualizacion
          // de cada uno de los partes

          query = " select CD_OPE, FC_ULTACT "
              + " from SIVE_EDO_DADIC "
              + " where CD_ENFCIE = ? "
              + " and CD_E_NOTIF = ? "
              + " and CD_ANOEPI = ? and CD_SEMEPI = ? "
              + " and FC_FECNOTIF = ? and FC_RECEP = ? "
              + " and SEQ_CODIGO = ? ";

          st = con.prepareStatement(query);

          st.setString(1, parte.cd_enf);
          st.setString(2, parte.equipo);
          st.setString(3, parte.ano);
          st.setString(4, parte.semana);
          dFecha = formater.parse(parte.fNotif.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(5, sqlFec);
          dFecha = formater.parse(parte.fRecep.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(6, sqlFec);
          st.setInt(7, parte.secuencial);

          rs = st.executeQuery();
          ind = 0;
          aux = null;
          while (rs.next()) {
            parte.ope = rs.getString("CD_OPE");
            //parte.fUltAct = formUltAct.format(rs.getDate("FC_ULTACT"));
            parte.fUltAct = timestamp_a_cadena(rs.getTimestamp("FC_ULTACT"));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          query = " select CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA "
              + " from SIVE_RESP_ADIC "
              + " where CD_ENFCIE = ? and CD_E_NOTIF = ? "
              + " and CD_ANOEPI = ? and CD_SEMEPI = ? "
              + " and CD_TSIVE = ? and FC_FECNOTIF = ? "
              + " and FC_RECEP = ? "
              + " and SEQ_CODIGO = ? order by CD_MODELO, NM_LIN";

          st = con.prepareStatement(query);

          st.setString(1, parte.cd_enf);
          st.setString(2, parte.equipo);
          st.setString(3, parte.ano);
          st.setString(4, parte.semana);
          st.setString(5, "E");
          dFecha = formater.parse(parte.fNotif.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(6, sqlFec);
          dFecha = formater.parse(parte.fRecep.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(7, sqlFec);
          st.setInt(8, parte.secuencial);

          rs = st.executeQuery();

          mod = "";
          lin = 0;

          while (rs.next()) {
            mod = rs.getString("CD_MODELO");
            parte.preguntas.addElement(new datosPreg(rs.getInt("NM_LIN"),
                rs.getString("CD_PREGUNTA"),
                rs.getString("DS_RESPUESTA"),
                mod, "", "", "", "", ""));
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //ahora se recorren los partes para buscar las listas
          //de valores de las preguntas

          pregs = new Vector();
          pregs = parte.preguntas;
          preg = null;
          for (int s = 0; s < pregs.size(); s++) {
            preg = (datosPreg) pregs.elementAt(s);

            // lanza la query
            st = con.prepareStatement(sSELECT_VALORES);

            st.setString(1, preg.pregunta); //cd_pregunta
            st.setString(2, preg.respuesta); //cd_listap

            rs = st.executeQuery();
            //solo saldra un registro, o (ninguno: no se modifica)
            while (rs.next()) {
              //pregs.removeElementAt(s);

              String des = rs.getString("DS_LISTAP");
              String desL = rs.getString("DSL_LISTAP");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desL != null)) {
                valor = desL;
              }
              else {
                valor = des;

              }
              preg.ValorLista = valor;

            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            //completamos los datos de la estructura
            //veo si esos modelos son obligatorios y su nivel

            st = con.prepareStatement(sQuery);
            st.setString(1, "E");
            st.setString(2, preg.modelo);
            rs = st.executeQuery();
            String n1 = null;
            String n2 = null;
            String ca = null;
            String nivel = null; //"CNE"; "CA"; "N1", "N2"
            while (rs.next()) {
              n1 = rs.getString("CD_NIVEL_1");
              n2 = rs.getString("CD_NIVEL_2");
              ca = rs.getString("CD_CA");
              //"CNE"; "CA"; "N1", "N2"
              if (n1 == null && n2 == null && ca == null) {
                nivel = "CNE";
              }
              else if (n1 == null && n2 == null && ca != null) {
                nivel = "CA";
              }
              else if (n1 != null && n2 == null && ca != null) {
                nivel = "N1";
              }
              else if (n1 != null && n2 != null && ca != null) {
                nivel = "N2";

              }
              preg.nivel = nivel;
              preg.it_ok = rs.getString("IT_OK");
              preg.ca = ca;
              preg.n1 = n1;
              preg.n2 = n2;
            } //while
            rs.close();
            rs = null;
            st.close();
            st = null;

            pregs.removeElementAt(s);
            pregs.insertElementAt(preg, s);
          }
          parte.preguntas = pregs;
          listaSalida.addElement(parte);

          break;

          //Comprobacion y si corresponde alta de un parte
        case servletALTA:

          //apa�o
          NIVEL = "";

          //param(0): parte que se va a dar de alta
          //param(1): casos de la declaracion
          //param(2): entorno para el layout
          //param(3): lista del layout

          //va bien: listaSalida = (true, parte)
          //       o listaSalida = (false, mensaje error)
          //va mal: listaSalida = null y exc

          parte = (datosParte) param.elementAt(0);
          Integer casos = (Integer) param.elementAt(1);
          entornoLayout = (DataEnfEnt) param.elementAt(2);
          listaLayout = (CLista) param.elementAt(3);

          //Comprobacion de que con este nuevo parte no se supera
          //del numero de casos declarados

          if (comprobarNumPartes(parte, casos, con, st, rs) == casos.intValue()) {
            //error
            String mens = "No se puede a�adir un nuevo parte porque se superar�a el n�mero de casos declarados.";
            listaSalida.addElement(new Boolean(false));
            listaSalida.addElement(mens);
          }
          else {

            //se comprueba el layout
            if (compararLayout(parte, entornoLayout, listaLayout,
                               con, st, rs, param)) {
              //se graba el parte

              // se obtiene el max secuencial para dar el siguiente
              String queryMax = "select max(SEQ_CODIGO) from SIVE_EDO_DADIC ";
              int nuevoSec = 0;
              st = con.prepareStatement(queryMax);
              rs = st.executeQuery();
              if (rs.next()) {
                nuevoSec = rs.getInt(1) + 1;
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

              parte = nuevoParte(parte, con, st, rs, param.getLogin(), nuevoSec);
              parte = completaPreguntas(parte, con, st, rs);

              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement(parte);
            }
            else {
              //alguien ha modificado el layout y se recomienda cerrar
              String mens = "Mientras se estaba rellenando el parte se ha actualizado el protocolo. Se recomienda pulsar Salir y volver a entrar.";
              listaSalida.addElement(new Boolean(false));
              listaSalida.addElement(mens);
            }
          } //else de comprobacion de numero de casos

          break;

          //MODIFICAR******************************************
        case servletCOMP_MOD:

          //apa�o
          NIVEL = "";

          //va bien: listaSalida = (true, parte) o
          //         listaSalida = (true, mensaje pregunta) o
          //         listaSalida = (false, mensaje error)
          //va mal: listaSalida = null y exc

          //param(0): parte que se va a dar de alta
          //param(1): casos de la declaracion
          //param(2): entorno para el layout
          //param(3): lista del layout

          parte = (datosParte) param.elementAt(0);
          casos = (Integer) param.elementAt(1);
          entornoLayout = (DataEnfEnt) param.elementAt(2);
          listaLayout = (CLista) param.elementAt(3);

          // Comprobar si el parte que se esta modificando ha sido borrado
          // o modificado

          //se comprueba el layout
          if (compararLayout(parte, entornoLayout, listaLayout,
                             con, st, rs, param)) {

            /*
                       listaSalida.addElement(new Boolean(true));
                       listaSalida.addElement("Mientras se alteraba el contenido del parte ha sido modificado por otro usuario. �Desea sobreescribir sus cambios?");
             */
            Vector auxV = new Vector();

            auxV = comprobarModBor(parte, con, st, rs, param.getLogin());
            if ( ( (Integer) auxV.elementAt(0)).intValue() == 0) {
              //no ha cambiado ninguna fila
              //se lanza el select con la clave para ver si el parte
              //ha sido modificado o borrado

              String resMod = modificado_o_borrado(parte, con, st, rs);
              if (resMod.equals("M")) {

                //resultado modificado
                listaSalida.addElement(new Boolean(true));
                listaSalida.addElement("Mientras se alteraba el contenido del parte ha sido modificado por otro usuario. �Desea sobreescribir sus cambios?");
              }
              else if (resMod.equals("B")) {
                //resultado borrado

                if (comprobarNumPartes(parte, casos, con, st, rs) ==
                    casos.intValue()) {
                  //error
                  String mens = "No se puede modificar el parte porque habia sido borrado y si se inserta un nuevo parte porque se superar�a el n�mero de casos declarados.";
                  listaSalida.addElement(new Boolean(false));
                  listaSalida.addElement(mens);

                }
                else {
                  String mens = "Mientras se alteraba el contenido del parte ha sido borrado por otro usuario. �Desea guardar los cambios?";
                  listaSalida.addElement(new Boolean(true));
                  listaSalida.addElement(mens);

                }
              }
            } // el parte habia sido modificado o borrado
            else {
              //MODIFICACION

              // ya se ha hecho el update sobre sive_edo_dadic
              // cuando se comprobaba si habia sido modificado
              parte = (datosParte) auxV.elementAt(1);

              //borradoParte(parte, con, st, rs);
              borradoRespuestas(parte, con, st, rs);
              //parte = introduccionParte(parte, con, st, rs, param.getLogin());
              parte = introduccionRespuestas(parte, con, st, rs);
              parte = completaPreguntas(parte, con, st, rs);
              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement(parte);

            } //el parte no habia sido modificado ni borrado

          }
          else {

            listaSalida.addElement(new Boolean(false));
            listaSalida.addElement("Mientras se estaba rellenando el parte se ha actualizado el protocolo. Se recomienda pulsar Salir y volver a entrar.");
          }
          break;

        case servletCOMP_BORR:

          //apa�o
          NIVEL = "";

          //param(0): parte que se va a borrar

          //va bien: listaSalida = (true) o
          //         listaSalida = (true, mensaje pregunta)
          //va mal: listaSalida = null y exc

          parte = (datosParte) param.elementAt(0);

          // Comprobar si el parte que se esta modificando ha sido borrado
          // o modificado

          Vector auxV = new Vector();
          auxV = comprobarModBor(parte, con, st, rs, param.getLogin());
          if ( ( (Integer) auxV.firstElement()).intValue() == 0) {
            //no ha cambiado ninguna fila
            //se lanza el select con la clave para ver si el parte
            //ha sido modificado o borrado

            String resMod = modificado_o_borrado(parte, con, st, rs);
            if (resMod.equals("M")) {

              //resultado modificado
              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement("El parte ha sido modificado por otro usuario. �Desea borrarlo descartando asi la �ltima modificaci�n?");
            }
            else if (resMod.equals("B")) {
              //resultado: ya habia sido borrado
              listaSalida.addElement(new Boolean(true));
            }
          } // el parte habia sido modificado o borrado
          else {
            //BORRADO

            //el parte se ha modificado en la comprobacion
            parte = (datosParte) auxV.elementAt(1);
            borradoParte(parte, con, st, rs);
            listaSalida.addElement(new Boolean(true));

          }
          break;

        case servletBORRADO:

          //apa�o
          NIVEL = "";

          //param(0): parte que se va a dar de alta

          //va bien: listaSalida.size()=0
          //va mal: listaSalida = null y exc

          parte = (datosParte) param.elementAt(0);
          //BORRADO
          borradoParte(parte, con, st, rs);

          break;

        case servletMODIFICACION:

          //apa�o
          NIVEL = "";

          //param(0): parte que se va a dar de alta

          //va bien: listaSalida = (parte)
          //va mal: listaSalida = null y exc

          parte = (datosParte) param.elementAt(0);
          //MODIFICACION
          borradoParte(parte, con, st, rs);
          //actualizo el ope y la fUltAct
          parte = introduccionParte(parte, con, st, rs, param.getLogin());
          parte = completaPreguntas(parte, con, st, rs);
          listaSalida.addElement(parte);

          break;

        case servletCOMP_ACTUALIZAR_PARTES:

          //va bien: listaSalida = (true, listaPartes) o
          //         listaSalida = (true, mensaje pregunta) o
          //         listaSalida = (false, mensaje error)
          //va mal: listaSalida = null y exc

          //param(0): lista de partes que se van a actualizar
          //param(1): casos de la declaracion  (no se si se usa)
          //param(2): entorno para el layout
          //param(3): lista del layout

          listaPartesAct = (Vector) param.elementAt(0);
          //casos = (Integer) param.elementAt(1);
          entornoLayout = (DataEnfEnt) param.elementAt(1);
          listaLayout = (CLista) param.elementAt(2);
          NIVEL = (String) param.elementAt(3);

          Vector listaPartesFinal = new Vector();

          //1. Comprobar el bloqueo para cada uno de los partes
          boolean fin = false;

          for (int i = 0; i < listaPartesAct.size(); i++) {
            parte = (datosParte) listaPartesAct.elementAt(i);
            //1. se comprueba el layout

            if (compararLayout(parte, entornoLayout, listaLayout,
                               con, st, rs, param)) {

              auxV = new Vector();
              auxV = comprobarModBor(parte, con, st, rs, param.getLogin());
              if ( ( (Integer) auxV.firstElement()).intValue() == 0) {
                //no ha cambiado ninguna fila
                //se lanza el select con la clave para ver si el parte
                //ha sido modificado o borrado

                String resMod = modificado_o_borrado(parte, con, st, rs);
                if (resMod.equals("M")) {

                  //resultado modificado
                  listaSalida.addElement(new Boolean(true));
                  listaSalida.addElement("Mientras se alteraba el contenido del parte ha sido modificado por otro usuario. �Desea sobreescribir sus cambios?");
                  fin = true;
                  break;

                }
                else if (resMod.equals("B")) {
                  //resultado: ya habia sido borrado

                  //3. si borrado se recomienda cerrar
                  listaSalida.addElement(new Boolean(false));
                  listaSalida.addElement("Un parte ha sido borrado y no es posible realizar la actualizaci�n del protocolo. Se recomienda pulsar Salir y volver a entrar.");
                  fin = true;
                  break;
                }
              } // el parte habia sido modificado o borrado
              else {
                //// System_out.println("SrvNotAdic: parte no modificado ni borrado");
              }
            }
            else {
              listaSalida.addElement(new Boolean(false));
              listaSalida.addElement("Mientras se estaba rellenando el parte se ha actualizado el protocolo. Se recomienda pulsar Salir y volver a entrar.");
              fin = true;
              break;
            }

          } //for de los partes
          if (!fin) {
            //4. se borran las respuestas de todos los partes
            borradoRespuestasTodos( (datosParte) listaPartesAct.firstElement(),
                                   con, st, rs);

            //5. se insertan los nuevos partes
            for (int i = 0; i < listaPartesAct.size(); i++) {
              parte = (datosParte) listaPartesAct.elementAt(i);
              parte = introduccionRespuestas(parte, con, st, rs);
              parte = completaPreguntas(parte, con, st, rs);
              listaPartesFinal.addElement(parte);
            }
            listaSalida.addElement(new Boolean(true));
            listaSalida.addElement(listaPartesFinal);
          }

          break;
        case servletACTUALIZAR_PARTES:

          //apa�o
          NIVEL = "";

          //va bien: listaSalida = (listaPartes)
          //va mal: listaSalida = null y exc

          //param(0): lista de partes que se van a actualizar

          listaPartesAct = (Vector) param.elementAt(0);
          listaPartesFinal = new Vector();

          //1. se borran las respuestas de todos los partes
          borradoRespuestasTodos( (datosParte) listaPartesAct.firstElement(),
                                 con, st, rs);
          //2. se insertan los nuevos partes
          for (int i = 0; i < listaPartesAct.size(); i++) {
            parte = (datosParte) listaPartesAct.elementAt(i);
            parte = introduccionRespuestas(parte, con, st, rs);
            parte = completaPreguntas(parte, con, st, rs);
            listaPartesFinal.addElement(parte);
          }
          listaSalida.addElement(listaPartesFinal);

          break;

        case servletRECARGAR_PARTES:

          //va bien: listaSalida = (true, listaPartes) o
          //         listaSalida = (true, mensaje pregunta) o
          //         listaSalida = (false, mensaje error)
          //va mal: listaSalida = null y exc

          //param(0): lista de partes que se van a actualizar
          //param(1): casos de la declaracion  (no se si se usa)
          //param(2): entorno para el layout
          //param(3): lista del layout

          listaPartesAct = (Vector) param.elementAt(0);
          //casos = (Integer) param.elementAt(1);
          entornoLayout = (DataEnfEnt) param.elementAt(1);
          listaLayout = (CLista) param.elementAt(2);
          NIVEL = (String) param.elementAt(3);

          listaPartesFinal = new Vector();

          //1. Comprobar el bloqueo para cada uno de los partes
          fin = false;

          for (int i = 0; i < listaPartesAct.size(); i++) {
            parte = (datosParte) listaPartesAct.elementAt(i);
            //1. se comprueba el layout

            if (compararLayout(parte, entornoLayout, listaLayout,
                               con, st, rs, param)) {

            }
            else {
              listaSalida.addElement(new Boolean(false));
              listaSalida.addElement("Mientras se estaba rellenando el parte se ha actualizado el protocolo. Se recomienda pulsar Salir y volver a entrar.");
              fin = true;
              break;
            }

          } //for de los partes
          if (!fin) {
            //4. se borran las respuestas de todos los partes
            borradoRespuestasTodos( (datosParte) listaPartesAct.firstElement(),
                                   con, st, rs);

            //5. se insertan los nuevos partes
            for (int i = 0; i < listaPartesAct.size(); i++) {
              parte = (datosParte) listaPartesAct.elementAt(i);
              parte = introduccionRespuestas(parte, con, st, rs);
              parte = completaPreguntas(parte, con, st, rs);
              listaPartesFinal.addElement(parte);
            }
            listaSalida.addElement(new Boolean(true));
            listaSalida.addElement(listaPartesFinal);
          }

          break;

      } // fin del switch para los modos de consulta

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;

      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;

  }

  private boolean compararLayout(datosParte parte,
                                 DataEnfEnt entorno,
                                 CLista lisLayout,
                                 Connection con,
                                 PreparedStatement st,
                                 ResultSet rs,
                                 CLista param) throws Exception {

    //Entrada: (datosPreg, DataEnfEnt, (DataLineasM,...,DataLineasM))
    Vector listaPreg = new Vector();

    CLista lisLayoutBD = new CLista();

    //1. Recupero las preguntas de los partes para buscar el layout

    //1.1 Recupero todas las preguntas de todos los partes
    String query = " select CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA "
        + " from SIVE_RESP_ADIC "
        + " where CD_ENFCIE = ? and CD_E_NOTIF = ? "
        + " and CD_ANOEPI = ? and CD_SEMEPI = ? "
        + " and CD_TSIVE = ? and FC_FECNOTIF = ? "
        + " and FC_RECEP = ? order by CD_MODELO, NM_LIN";

    st = con.prepareStatement(query);

    st.setString(1, parte.cd_enf);
    st.setString(2, parte.equipo);
    st.setString(3, parte.ano);
    st.setString(4, parte.semana);
    st.setString(5, "E");
    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(7, sqlFec);

    rs = st.executeQuery();

    while (rs.next()) {
      listaPreg.addElement(new datosPreg(rs.getInt("NM_LIN"),
                                         rs.getString("CD_PREGUNTA"),
                                         rs.getString("DS_RESPUESTA"),
                                         rs.getString("CD_MODELO"),
                                         "", "", "", "", ""));
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    //1.2 Completo las preguntas con nivel e it_ok
    datosPreg preg = null;
    for (int s = 0; s < listaPreg.size(); s++) {
      preg = (datosPreg) listaPreg.elementAt(s);
      //veo si esos modelos son obligatorios y su nivel
      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, preg.modelo);
      rs = st.executeQuery();
      String n1 = null;
      String n2 = null;
      String ca = null;
      String nivel = null; //"CNE"; "CA"; "N1", "N2"
      while (rs.next()) {
        n1 = rs.getString("CD_NIVEL_1");
        n2 = rs.getString("CD_NIVEL_2");
        ca = rs.getString("CD_CA");
        //"CNE"; "CA"; "N1", "N2"
        if (n1 == null && n2 == null && ca == null) {
          nivel = "CNE";
        }
        else if (n1 == null && n2 == null && ca != null) {
          nivel = "CA";
        }
        else if (n1 != null && n2 == null && ca != null) {
          nivel = "N1";
        }
        else if (n1 != null && n2 != null && ca != null) {
          nivel = "N2";

        }
        preg.nivel = nivel;
        preg.it_ok = rs.getString("IT_OK");
        preg.ca = ca;
        preg.n1 = n1;
        preg.n2 = n2;
      } //while
      rs.close();
      rs = null;
      st.close();
      st = null;

      listaPreg.removeElementAt(s);
      listaPreg.insertElementAt(preg, s);
    }

    // 2. Se ordena la lista para localizar los codigos de los
    // niveles que componen el layout

    //2.1 Se adapta la lista para que pueda ser ordenada para
    //localizar los codigos de los niveles que componen el layout
    CLista listaSalida = new CLista();
    for (int i = 0; i < listaPreg.size(); i++) {
      preg = (datosPreg) listaPreg.elementAt(i);
      DataRespCompleta datosnuevos = new DataRespCompleta("", preg.modelo,
          (new Integer(preg.linea)).toString(),
          preg.pregunta, preg.nivel,
          preg.ca, preg.n1, preg.n2,
          preg.it_ok,
          preg.respuesta,
          preg.ValorLista,
          "");
      listaSalida.addElement(datosnuevos);
    }

    //2.2 se ordena la lista
    listaSalida = Ordenar_listaSalida(listaSalida);

    //reconstruir dataEntrada de dataenfent -> dataenfentcaso
    DataEnfEntCaso datosRecons = new DataEnfEntCaso(
        parte.cd_enf,
        entorno.getComunidad(),
        entorno.getNivelUNO(),
        entorno.getNivelDOS(),
        "");
    // 3. Consultas para averiguar si los modelos son obsoletos o no
    // y obtener el layout
    lisLayoutBD = cuatro_consultas(con, st, rs, datosRecons, param, listaSalida);

    // 4. Comparacion de la dos listas con los layouts
    //Cada lista contiene un DataLineasM por cada linea del modelo
    if (lisLayoutBD.size() == 0) {
      lisLayoutBD = null;
      return false;
    }
    if (lisLayoutBD.size() != lisLayout.size()) {
      return false;
    }

    DataLineasM dato = null;
    DataLineasM datoBD = null;
    for (int j = 0; j < lisLayout.size(); j++) {
      dato = (DataLineasM) lisLayout.elementAt(j);
      datoBD = (DataLineasM) lisLayoutBD.elementAt(j);

      if (!dato.getCodModelo().equals(datoBD.getCodModelo())) {
        return false;
      }
      if (!dato.getNmLinea().equals(datoBD.getNmLinea())) {
        return false;
      }
      //no puedo comparar el codigo de la pregunta porque no aparece
      //en DataLineasM

    }
    return true;

  }

  public CLista Ordenar_listaSalida(CLista lista) {
    //ordenamos la lista por niveles
    //Entrada: lista con todas las preguntas de los partes (datosPreg)
    //Salida: lista con un DataRespCompleta para CNE, otro paraCA,
    //otro para N1 y otro para N2

    CLista lisSalida = new CLista();

    DataRespCompleta dataSalidaResp = null;
    int i = 0;

    //copia
    CLista listaCopia = new CLista();
    for (i = 0; i < lista.size(); i++) {
      dataSalidaResp = (DataRespCompleta) lista.elementAt(i);
      listaCopia.addElement(dataSalidaResp);
    } //for

    boolean bExiste = false;
    for (i = 0; i < listaCopia.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaCopia.elementAt(i);
      if (dataSalidaResp.getNivel().trim().equals("CNE")) {
        lisSalida.addElement(dataSalidaResp);
        bExiste = true;
        break;
      }
    }
    if (!bExiste) {
      lisSalida.addElement(new DataRespCompleta(null, null, null, null, "", null, null, null, null, null, null, null));

    }
    bExiste = false;
    for (i = 0; i < listaCopia.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaCopia.elementAt(i);
      if (dataSalidaResp.getNivel().trim().equals("CA")) {
        lisSalida.addElement(dataSalidaResp);
        bExiste = true;
        break;
      }
    }
    if (!bExiste) {
      lisSalida.addElement(new DataRespCompleta(null, null, null, null, "", null, null, null, null, null, null, null));

    }
    bExiste = false;
    for (i = 0; i < listaCopia.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaCopia.elementAt(i);
      if (dataSalidaResp.getNivel().trim().equals("N1")) {
        lisSalida.addElement(dataSalidaResp);
        bExiste = true;
        break;
      }
    }
    if (!bExiste) {
      lisSalida.addElement(new DataRespCompleta(null, null, null, null, "", null, null, null, null, null, null, null));

    }
    bExiste = false;
    for (i = 0; i < listaCopia.size(); i++) {
      dataSalidaResp = (DataRespCompleta) listaCopia.elementAt(i);
      if (dataSalidaResp.getNivel().trim().equals("N2")) {
        lisSalida.addElement(dataSalidaResp);
        bExiste = true;
        break;
      }
    }
    if (!bExiste) {
      lisSalida.addElement(new DataRespCompleta(null, null, null, null, "", null, null, null, null, null, null, null));

      //------------------------------------------------
    }
    listaCopia = null;

    return lisSalida;

  }

  public CLista cuatro_consultas(Connection con,
                                 PreparedStatement st,
                                 ResultSet rs,
                                 DataEnfEntCaso datosEntrada,
                                 CLista param,
                                 CLista listaSalida) throws Exception {

    DataRespCompleta dataSalidaResp = null;
    CLista listaDataLineasM = new CLista();

    // campos primera consulta
    String sCD_MODELO = "";
    String sNM_LIN = "";
    Integer iNM_LIN = null;
    String sCD_TLINEA = "";
    String sDS_TEXTO = "";
    String sCA = "";
    String sN1 = "";
    String sN2 = "";

    String sQuery = "";
    //hacemos 4 consultas
    //CNE!!!!!!

    String des = "";
    String desL = "";
    //reconstruyo si procede listaSalida
    if (!NIVEL.equals("")) { //eliminar NIVEL
      for (int i = 0; i < listaSalida.size(); i++) {
        dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(i);
        if (NIVEL.trim().equals(dataSalidaResp.getNivel().trim())) {
          listaSalida.removeElementAt(i);
          listaSalida.insertElementAt(new DataRespCompleta(null, null, null, null,
              "", null, null, null, null, null, null, null), i);
        }
      }
    }

    dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(0);
    if (dataSalidaResp.getNivel().trim().equals("CNE")) {
      sQuery = " select "
          + "CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO "
          + "from SIVE_LINEASM "
          + "where CD_TSIVE = ? and  CD_MODELO = ? "
          + " order by  CD_MODELO, NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, dataSalidaResp.getCodModelo().trim());
    }
    else {
      sQuery = " select b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, "
          + " b.DS_TEXTO, b.DSL_TEXTO, a.CD_CA, a.CD_NIVEL_1, "
          + " a.CD_NIVEL_2 "
          + " from SIVE_MODELO a, SIVE_LINEASM b  "
          +
          " where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) "
          + " and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)"
          + " and (a.CD_ENFCIE = ?) "
          + " and ( "
          +
          " (a.CD_CA is null and a.CD_NIVEL_1 is null and a.CD_NIVEL_2 is null)  "
          + " ) "
          + " ) "
          + " order by b.CD_MODELO, b.NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, "S");
      st.setString(3, datosEntrada.getCodEnfermedad().trim());
    } //else de cne

    rs = st.executeQuery();

    // extrae el registro encontrado y lo carga en listaDataLineasM
    while (rs.next()) {
      des = rs.getString("DS_TEXTO");
      desL = rs.getString("DSL_TEXTO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
          && (desL != null)) {
        sDS_TEXTO = desL;
      }
      else {
        sDS_TEXTO = des;

      }
      sCD_MODELO = rs.getString("CD_MODELO");
      iNM_LIN = new Integer(rs.getInt("NM_LIN"));
      sNM_LIN = iNM_LIN.toString();
      sCD_TLINEA = rs.getString("CD_TLINEA");

      if (dataSalidaResp.getNivel().trim().equals("CNE")) {
        sCA = dataSalidaResp.getCA();
        sN1 = dataSalidaResp.getN1();
        sN2 = dataSalidaResp.getN2();
      }
      else {
        sCA = null;
        sN1 = null;
        sN2 = null;
      }

      // a�ade un nodo
      listaDataLineasM.addElement(new DataLineasM
                                  (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                   sCA, sN1, sN2));
    } //while
    rs.close();
    rs = null;
    st.close();
    st = null;

    //CA!!!!!!!
    //!!!!!!!!!
    dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(1);
    if (dataSalidaResp.getNivel().trim().equals("CA")) {
      sQuery = " select CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO "
          + " from SIVE_LINEASM "
          + " where CD_TSIVE = ? and  CD_MODELO = ? "
          + " order by  CD_MODELO, NM_LIN ";

      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, dataSalidaResp.getCodModelo().trim());

    }
    else {
      sQuery = " select b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, "
          + " b.DSL_TEXTO, a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 "
          + " from SIVE_MODELO a, SIVE_LINEASM b  "
          +
          " where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) "
          + " and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)"
          + " and (a.CD_ENFCIE = ?) "
          + " and ( "
          +
          " (a.CD_CA = ? and a.CD_NIVEL_1 is null and a.CD_NIVEL_2 is null)  "
          + " ) "
          + " ) "
          + " order by b.CD_MODELO, b.NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, "S");
      st.setString(3, datosEntrada.getCodEnfermedad().trim());
      st.setString(4, datosEntrada.getComunidad());
    } //else de ca

    rs = st.executeQuery();

    // extrae el registro encontrado y lo carga en listaDataLineasM
    while (rs.next()) {
      des = rs.getString("DS_TEXTO");
      desL = rs.getString("DSL_TEXTO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
          && (desL != null)) {
        sDS_TEXTO = desL;
      }
      else {
        sDS_TEXTO = des;

      }
      sCD_MODELO = rs.getString("CD_MODELO");
      iNM_LIN = new Integer(rs.getInt("NM_LIN"));
      sNM_LIN = iNM_LIN.toString();
      sCD_TLINEA = rs.getString("CD_TLINEA");

      if (dataSalidaResp.getNivel().trim().equals("CA")) {
        sCA = dataSalidaResp.getCA();
        sN1 = dataSalidaResp.getN1();
        sN2 = dataSalidaResp.getN2();
      }
      else {
        sCA = datosEntrada.getComunidad();
        sN1 = null;
        sN2 = null;
      }

      // a�ade un nodo
      listaDataLineasM.addElement(new DataLineasM
                                  (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                   sCA, sN1, sN2));
    } //while
    rs.close();
    rs = null;
    st.close();
    st = null;

    //N1!!!!!!!
    //!!!!!!!!!
    dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(2);
    if (dataSalidaResp.getNivel().trim().equals("N1")) {
      sQuery = " select CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO "
          + " from SIVE_LINEASM "
          + " where CD_TSIVE = ? and  CD_MODELO = ? "
          + " order by  CD_MODELO, NM_LIN";
      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, dataSalidaResp.getCodModelo().trim());

    }
    else {
      sQuery = " select b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, "
          + " b.DSL_TEXTO, a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 "
          + " from SIVE_MODELO a, SIVE_LINEASM b  "
          +
          " where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) "
          + " and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)"
          + " and (a.CD_ENFCIE = ?) "
          + " and ( "
          + " (a.CD_CA = ? and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 is null)  "
          + " ) "
          + " ) "
          + " order by b.CD_MODELO, b.NM_LIN";
      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, "S");
      st.setString(3, datosEntrada.getCodEnfermedad().trim());
      st.setString(4, datosEntrada.getComunidad());
      st.setString(5, datosEntrada.getNivelUNO());
    } //else de n1

    rs = st.executeQuery();

    // extrae el registro encontrado y lo carga en listaDataLineasM
    while (rs.next()) {
      des = rs.getString("DS_TEXTO");
      desL = rs.getString("DSL_TEXTO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
          && (desL != null)) {
        sDS_TEXTO = desL;
      }
      else {
        sDS_TEXTO = des;

      }
      sCD_MODELO = rs.getString("CD_MODELO");
      iNM_LIN = new Integer(rs.getInt("NM_LIN"));
      sNM_LIN = iNM_LIN.toString();
      sCD_TLINEA = rs.getString("CD_TLINEA");

      if (dataSalidaResp.getNivel().trim().equals("N1")) {
        sCA = dataSalidaResp.getCA();
        sN1 = dataSalidaResp.getN1();
        sN2 = dataSalidaResp.getN2();
      }
      else {
        sCA = datosEntrada.getComunidad();
        sN1 = datosEntrada.getNivelUNO();
        sN2 = null;
      }

      // a�ade un nodo
      listaDataLineasM.addElement(new DataLineasM
                                  (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                   sCA, sN1, sN2));
    } //while
    rs.close();
    rs = null;
    st.close();
    st = null;

    //N2!!!!!!!
    //!!!!!!!!!
    dataSalidaResp = (DataRespCompleta) listaSalida.elementAt(3);
    if (dataSalidaResp.getNivel().trim().equals("N2")) {
      sQuery = " select CD_MODELO, NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO "
          + " from SIVE_LINEASM "
          + " where CD_TSIVE = ? and  CD_MODELO = ? "
          + " order by  CD_MODELO, NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, dataSalidaResp.getCodModelo().trim());
    }
    else {
      sQuery = " select b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, "
          + " b.DSL_TEXTO, a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 "
          + " from SIVE_MODELO a, SIVE_LINEASM b  "
          +
          " where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) "
          + " and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)"
          + " and (a.CD_ENFCIE = ?) "
          + " and ( "
          + "(a.CD_CA = ? and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 = ?)  "
          + " ) "
          + " ) "
          + " order by b.CD_MODELO, b.NM_LIN";

      st = con.prepareStatement(sQuery);
      st.setString(1, "E");
      st.setString(2, "S");
      st.setString(3, datosEntrada.getCodEnfermedad().trim());
      st.setString(4, datosEntrada.getComunidad());
      st.setString(5, datosEntrada.getNivelUNO());
      st.setString(6, datosEntrada.getNivelDOS());
    } //else de cne

    rs = st.executeQuery();

    // extrae el registro encontrado y lo carga en listaDataLineasM
    while (rs.next()) {
      des = rs.getString("DS_TEXTO");
      desL = rs.getString("DSL_TEXTO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
          && (desL != null)) {
        sDS_TEXTO = desL;
      }
      else {
        sDS_TEXTO = des;

      }
      sCD_MODELO = rs.getString("CD_MODELO");
      iNM_LIN = new Integer(rs.getInt("NM_LIN"));
      sNM_LIN = iNM_LIN.toString();
      sCD_TLINEA = rs.getString("CD_TLINEA");

      if (dataSalidaResp.getNivel().trim().equals("N2")) {
        sCA = dataSalidaResp.getCA();
        sN1 = dataSalidaResp.getN1();
        sN2 = dataSalidaResp.getN2();
      }
      else {
        sCA = datosEntrada.getComunidad();
        sN1 = datosEntrada.getNivelUNO();
        sN2 = datosEntrada.getNivelDOS();
      }

      // a�ade un nodo
      listaDataLineasM.addElement(new DataLineasM
                                  (sCD_MODELO, sNM_LIN, sCD_TLINEA, sDS_TEXTO,
                                   sCA, sN1, sN2));
    } //while
    rs.close();
    rs = null;
    st.close();
    st = null;

    return listaDataLineasM;

  } //fin de cuatro_consultas

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  }

  private datosParte nuevoParte(datosParte parte,
                                Connection con,
                                PreparedStatement st,
                                ResultSet rs,
                                String login,
                                int sec) throws Exception {

    parte.secuencial = sec;
    //actualizo el ope y la fUltAct
    parte = introduccionParte(parte, con, st, rs, login);
    return parte;
  }

  private datosParte introduccionRespuestas(datosParte parte,
                                            Connection con,
                                            PreparedStatement st,
                                            ResultSet rs) throws Exception {

    datosPreg preg = null;
    //recorremos la lista de entrada
    for (int i = 0; i < parte.preguntas.size(); i++) {
      preg = (datosPreg) parte.preguntas.elementAt(i);

      st = con.prepareStatement(sALTA_RESP);

      st.setString(1, parte.cd_enf.trim());
      st.setString(2, parte.equipo.trim());
      st.setString(3, parte.ano.trim());
      st.setString(4, "E");
      st.setString(5, parte.semana.trim());
      dFecha = formater.parse(parte.fNotif.trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(6, sqlFec);
      dFecha = formater.parse(parte.fRecep.trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(7, sqlFec);
      st.setString(8, preg.modelo.trim());
      st.setInt(9, (new Integer(parte.secuencial)).intValue());
      st.setInt(10, (new Integer(preg.linea)).intValue());
      st.setString(11, preg.pregunta.trim());
      st.setString(12, preg.respuesta.trim());

      st.executeUpdate();
      st.close();
      st = null;
    } //fin for
    return parte;
  }

  private datosParte completaPreguntas(datosParte parte,
                                       Connection con,
                                       PreparedStatement st,
                                       ResultSet rs) throws Exception {

    Vector pregs = new Vector();
    pregs = parte.preguntas;
    datosPreg preg = null;

    for (int s = 0; s < pregs.size(); s++) {
      preg = (datosPreg) pregs.elementAt(s);

      //veo si esos modelos son obligatorios y su nivel

      st = con.prepareStatement(sQuery);

      st.setString(1, "E");
      st.setString(2, preg.modelo);
      rs = st.executeQuery();
      String n1 = null;
      String n2 = null;
      String ca = null;
      String nivel = null; //"CNE"; "CA"; "N1", "N2"
      while (rs.next()) {
        n1 = rs.getString("CD_NIVEL_1");
        n2 = rs.getString("CD_NIVEL_2");
        ca = rs.getString("CD_CA");
        //"CNE"; "CA"; "N1", "N2"
        if (n1 == null && n2 == null && ca == null) {
          nivel = "CNE";
        }
        else if (n1 == null && n2 == null && ca != null) {
          nivel = "CA";
        }
        else if (n1 != null && n2 == null && ca != null) {
          nivel = "N1";
        }
        else if (n1 != null && n2 != null && ca != null) {
          nivel = "N2";

        }
        preg.nivel = nivel;
        preg.it_ok = rs.getString("IT_OK");

      } //while
      rs.close();
      rs = null;
      st.close();
      st = null;

      pregs.removeElementAt(s);
      pregs.insertElementAt(preg, s);
    }
    parte.preguntas = pregs;
    return parte;

  }

  private datosParte introduccionParte(datosParte parte,
                                       Connection con,
                                       PreparedStatement st,
                                       ResultSet rs,
                                       String login) throws Exception {

    st = con.prepareStatement(sALTA_CAB);

    st.setString(1, parte.cd_enf.trim());
    st.setInt(2, (new Integer(parte.secuencial)).intValue());
    st.setString(3, parte.equipo.trim());
    st.setString(4, parte.ano.trim());
    st.setString(5, parte.semana.trim());
    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(7, sqlFec);
    st.setString(8, login); //parte.ope.trim());

    // Se cambia la fecha de ultima actualizacion
    //new date
    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string
    //partirla!!
    st.setTimestamp(9, cadena_a_timestamp(sFecha));

    //para modificaciones y borrados de este parte
    parte.ope = login;
    parte.fUltAct = sFecha;

    st.executeUpdate();
    st.close();
    st = null;

    datosPreg preg = null;
    //recorremos la lista de entrada
    for (int i = 0; i < parte.preguntas.size(); i++) {
      preg = (datosPreg) parte.preguntas.elementAt(i);

      st = con.prepareStatement(sALTA_RESP);

      st.setString(1, parte.cd_enf.trim());
      st.setString(2, parte.equipo.trim());
      st.setString(3, parte.ano.trim());
      st.setString(4, "E");
      st.setString(5, parte.semana.trim());
      dFecha = formater.parse(parte.fNotif.trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(6, sqlFec);
      dFecha = formater.parse(parte.fRecep.trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(7, sqlFec);
      st.setString(8, preg.modelo.trim());
      st.setInt(9, (new Integer(parte.secuencial)).intValue());
      st.setInt(10, (new Integer(preg.linea)).intValue());
      st.setString(11, preg.pregunta.trim());
      st.setString(12, preg.respuesta.trim());

      st.executeUpdate();
      st.close();
      st = null;
    } //fin for
    return parte;
  }

  private void borradoRespuestasTodos(datosParte parte,
                                      Connection con,
                                      PreparedStatement st,
                                      ResultSet rs) throws Exception {

    st = con.prepareStatement(sBORRAR_RESP_TODOS);

    st.setString(1, parte.cd_enf.trim());
    st.setString(2, parte.equipo.trim());
    st.setString(3, parte.ano.trim());
    st.setString(4, "E");
    st.setString(5, parte.semana.trim());
    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(7, sqlFec);

    st.executeUpdate();
    st.close();
    st = null;
  }

  private void borradoRespuestas(datosParte parte,
                                 Connection con,
                                 PreparedStatement st,
                                 ResultSet rs) throws Exception {

    st = con.prepareStatement(sBORRAR_RESP);

    st.setString(1, parte.cd_enf.trim());
    st.setString(2, parte.equipo.trim());
    st.setString(3, parte.ano.trim());
    st.setString(4, "E");
    st.setString(5, parte.semana.trim());
    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(7, sqlFec);
    st.setInt(8, (new Integer(parte.secuencial)).intValue());

    st.executeUpdate();
    st.close();
    st = null;
  }

  private void borradoParte(datosParte parte,
                            Connection con,
                            PreparedStatement st,
                            ResultSet rs) throws Exception {

    st = con.prepareStatement(sBORRAR_RESP);

    st.setString(1, parte.cd_enf.trim());
    st.setString(2, parte.equipo.trim());
    st.setString(3, parte.ano.trim());
    st.setString(4, "E");
    st.setString(5, parte.semana.trim());
    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(7, sqlFec);
    st.setInt(8, (new Integer(parte.secuencial)).intValue());

    st.executeUpdate();
    st.close();
    st = null;

    st = con.prepareStatement(sBORRAR_CAB);

    st.setString(1, parte.cd_enf.trim());
    st.setInt(2, (new Integer(parte.secuencial)).intValue());
    st.setString(3, parte.equipo.trim());
    st.setString(4, parte.ano.trim());
    st.setString(5, parte.semana.trim());
    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(7, sqlFec);

    st.executeUpdate();
    st.close();
    st = null;

  }

  private int comprobarNumPartes(datosParte parte,
                                 Integer casos,
                                 Connection con,
                                 PreparedStatement st,
                                 ResultSet rs) throws Exception {

    String queryComp = " select count(*) from SIVE_EDO_DADIC "
        + " where CD_ENFCIE = ? "
        + " and CD_E_NOTIF = ? "
        + " and CD_ANOEPI = ? and CD_SEMEPI = ? "
        + " and FC_FECNOTIF = ? and FC_RECEP = ? ";
    st = con.prepareStatement(queryComp);

    st.setString(1, parte.cd_enf);
    st.setString(2, parte.equipo);
    st.setString(3, parte.ano);
    st.setString(4, parte.semana);
    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(5, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);

    rs = st.executeQuery();

    int numPartes = 0;
    if (rs.next()) {
      numPartes = rs.getInt(1);
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return numPartes;
  }

  private Vector comprobarModBor(datosParte parte,
                                 Connection con,
                                 PreparedStatement st,
                                 ResultSet rs,
                                 String login) throws Exception {

    String queryComp = " update SIVE_EDO_DADIC "
        + " set CD_OPE = ?, FC_ULTACT = ? "
        + " where CD_ENFCIE = ? and CD_E_NOTIF = ? "
        + " and SEQ_CODIGO = ? "
        + " and CD_ANOEPI = ? and CD_SEMEPI = ? "
        + " and FC_FECNOTIF = ? and FC_RECEP = ? "
        + " and CD_OPE = ? and FC_ULTACT = ?  ";

    st = con.prepareStatement(queryComp);

    //operador actual
    st.setString(1, login);

    //la fecha de ultima actualizacion no es un date
    //es un timestamp, fecha actual
    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string

    java.sql.Timestamp ts = cadena_a_timestamp(sFecha);
    st.setTimestamp(2, ts);

    st.setString(3, parte.cd_enf.trim());
    st.setString(4, parte.equipo.trim());
    st.setInt(5, parte.secuencial);
    st.setString(6, parte.ano.trim());
    st.setString(7, parte.semana.trim());

    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());

    st.setDate(8, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());

    st.setDate(9, sqlFec);

    //operador antiguo
    st.setString(10, parte.ope.trim());

    //la fecha de ultima actualizacion no es un date
    //es un timestamp
    //esta es la antigua
    ts = cadena_a_timestamp(parte.fUltAct);
    st.setTimestamp(11, ts);

    int resUpdate = st.executeUpdate();

    if (resUpdate != 0) {

      parte.ope = login;
      parte.fUltAct = sFecha;

    }

    Vector aux = new Vector();
    aux.addElement(new Integer(resUpdate));
    aux.addElement(parte);

    return aux;
  }

  private String modificado_o_borrado(datosParte parte,
                                      Connection con,
                                      PreparedStatement st,
                                      ResultSet rs) throws Exception {

    String resultado = "";

    String queryComp = " select * from SIVE_EDO_DADIC "
        + " where CD_ENFCIE = ? and CD_E_NOTIF = ? "
        + " and SEQ_CODIGO = ? "
        + " and CD_ANOEPI = ? and CD_SEMEPI = ? "
        + " and FC_FECNOTIF = ? and FC_RECEP = ? ";
    st = con.prepareStatement(queryComp);
    st.setString(1, parte.cd_enf.trim());
    st.setString(2, parte.equipo.trim());
    st.setInt(3, parte.secuencial);
    st.setString(4, parte.ano.trim());
    st.setString(5, parte.semana.trim());

    dFecha = formater.parse(parte.fNotif.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);
    dFecha = formater.parse(parte.fRecep.trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(7, sqlFec);

    rs = st.executeQuery();
    if (rs.next()) {
      //resultado modificado
      resultado = "M";
    }
    else {
      //resultado borrado
      resultado = "B";
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;

  }

}
