//Title:        Casos o Tasas por 100000 habitantes por Area
//Version:
//Copyright:    Copyright (c) 1998
//Author:
//Company:
//Description:  Consultas 9.3.10, 9.3.11 y 9.3.12

package casostasascienarea;

import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class panelInforme
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res = ResourceBundle.getBundle("casostasascienarea.Res" +
                                                getCApp().getIdioma());
  final int modoESPERA = 1;

  final String strLOGO_CCAA = "images/ccaa.gif";

  final String strSERVLET = "servlet/srvCTCienArea";

  // modos de operaci�n del servlet
  final int servletCTCA = 0;
  final int servletCTAREA = 1;
  final int servletCTDIST = 2;

  // estructuras de datos
  protected Vector resultado;
  protected Vector vTotalCasos;
  protected Vector vTotalLineas;
  protected Vector vAgrupaciones;
  protected boolean tasas = false;

  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  public parCons93 parCons;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  public panelInforme(CApp a) {
    super(a);

    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(getCApp().getCodeBase().toString() +
                     "erw/casosTasasCienArea.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    //this .setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

//E    //# System.Out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {

        PrepararInforme();

        // obtiene los datos del servidor
        parCons.numPagina++;
        param.addElement(parCons);
        param.setIdioma(app.getIdioma());
        param.trimToSize();

        stub.setUrl(new URL(app.getURL() + strSERVLET));
        int modoServ = 0;
        if (parCons.area.equals("")) {
          modoServ = servletCTCA;
        }
        else if (parCons.distrito.equals("")) {
          modoServ = servletCTAREA;
        }
        else {
          modoServ = servletCTDIST;

        }
        lista = (CLista) stub.doPost(modoServ, param);

        /*  srvCTCienArea srv = new srvCTCienArea();
          srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                               "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                               "dba_edo",
                               "manager");
          lista = srv.doDebug(modoServ ,param);
         */

        v = (Vector) lista.firstElement();

        for (int j = 0; j < v.size(); j++) {
          resultado.addElement(v.elementAt(j));

          // control de registros
        }
        this.setTotalRegistros( ( (Integer) lista.elementAt(1)).toString());
        this.setRegistrosMostrados( (new Integer(resultado.size())).toString());
        this.setTotal("");

        // comentado por jlt
        //datosPoblaNoti((dataCons93) resultado.firstElement(), modoServ);
        datosPoblaNoti(resultado, modoServ);
        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, "");
    CLista param = new CLista();
    CLista listaca = new CLista();
    listaca.addElement(this.app.getCA());

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      // obtiene los datos del servidor
      parCons.numPagina = 0;
      parCons.bInformeCompleto = conTodos;
      param.addElement(parCons);

      /////////
      param.addElement(listaca);
      /////////

      param.setIdioma(app.getIdioma());
      param.trimToSize();
      stub.setUrl(new URL(app.getURL() + strSERVLET));
      int modoServ = 0;
      if (parCons.area.equals("")) {
        modoServ = servletCTCA;
      }
      else if (parCons.distrito.equals("")) {
        modoServ = servletCTAREA;
      }
      else {
        modoServ = servletCTDIST;

      }

      lista = (CLista) stub.doPost(modoServ, param);

      /*        srvCTCienArea srv = new srvCTCienArea();
              srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                   "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                   "pista",
                                   "loteb98");
              lista = srv.doDebug(modoServ ,param);
       */

      resultado = (Vector) lista.firstElement();

      // control de registros
      if (resultado.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg5.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }
      else {

        /*
                 Integer total = new Integer (calcularTotalRegistros(parCons));
                 Integer mostrados =  (new Integer(vAgrupaciones.size()));
                 if (mostrados.intValue()>total.intValue())
          this .setTotalRegistros(mostrados.toString());
                 else
          this .setTotalRegistros(total.toString());
             this .setRegistrosMostrados( (new Integer(vAgrupaciones.size())).toString());
                 this .setTotal(vTotalCasos.elementAt(0).toString());
         */
        this.setTotalRegistros( ( (Integer) lista.elementAt(1)).toString());
        this.setRegistrosMostrados(new Integer(resultado.size()).toString());
        this.setTotal("");

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("ENFERMEDAD = ENFERMEDAD");
        retval.addElement("COL1 = COL1");
        retval.addElement("COL2 = COL2");
        retval.addElement("COL3 = COL3");
        retval.addElement("COL4 = COL4");
        retval.addElement("COL5 = COL5");
        retval.addElement("COLT = COLT");

        retval.addElement("ACT1 = ACT1");
        retval.addElement("ACT2 = ACT2");
        retval.addElement("ACT3 = ACT3");
        retval.addElement("ACT4 = ACT4");
        retval.addElement("ACT5 = ACT5");
        retval.addElement("ACTT = ACTT");

        retval.addElement("ACU1 = ACU1");
        retval.addElement("ACU2 = ACU2");
        retval.addElement("ACU3 = ACU3");
        retval.addElement("ACU4 = ACU4");
        retval.addElement("ACU5 = ACU5");
        retval.addElement("ACUT = ACUT");

        retval.addElement("POB1 = POB1");
        retval.addElement("POB2 = POB2");
        retval.addElement("POB3 = POB3");
        retval.addElement("POB4 = POB4");
        retval.addElement("POB5 = POB5");
        retval.addElement("POBT = POBT");

        retval.addElement("NOTTACT1 = NOTTACT1");
        retval.addElement("NOTTACT2 = NOTTACT2");
        retval.addElement("NOTTACT3 = NOTTACT3");
        retval.addElement("NOTTACT4 = NOTTACT4");
        retval.addElement("NOTTACT5 = NOTTACT5");
        retval.addElement("NOTTACTT = NOTTACTT");

        retval.addElement("NOTTACU1 = NOTTACU1");
        retval.addElement("NOTTACU2 = NOTTACU2");
        retval.addElement("NOTTACU3 = NOTTACU3");
        retval.addElement("NOTTACU4 = NOTTACU4");
        retval.addElement("NOTTACU5 = NOTTACU5");
        retval.addElement("NOTTACUT = NOTTACUT");

        retval.addElement("NOTRACT1 = NOTRACT1");
        retval.addElement("NOTRACT2 = NOTRACT2");
        retval.addElement("NOTRACT3 = NOTRACT3");
        retval.addElement("NOTRACT4 = NOTRACT4");
        retval.addElement("NOTRACT5 = NOTRACT5");
        retval.addElement("NOTRACTT = NOTRACTT");

        retval.addElement("NOTRACU1 = NOTRACU1");
        retval.addElement("NOTRACU2 = NOTRACU2");
        retval.addElement("NOTRACU3 = NOTRACU3");
        retval.addElement("NOTRACU4 = NOTRACU4");
        retval.addElement("NOTRACU5 = NOTRACU5");
        retval.addElement("NOTRACUT = NOTRACUT");

        retval.addElement("COBACT1 = COBACT1");
        retval.addElement("COBACT2 = COBACT2");
        retval.addElement("COBACT3 = COBACT3");
        retval.addElement("COBACT4 = COBACT4");
        retval.addElement("COBACT5 = COBACT5");
        retval.addElement("COBACTT = COBACTT");

        retval.addElement("COBACU1 = COBACU1");
        retval.addElement("COBACU2 = COBACU2");
        retval.addElement("COBACU3 = COBACU3");
        retval.addElement("COBACU4 = COBACU4");
        retval.addElement("COBACU5 = COBACU5");
        retval.addElement("COBACUT = COBACUT");

        dataHandler.RegisterTable(resultado, "SIVE_C_9_3_10", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        //datosPoblaNoti((dataCons93) resultado.firstElement(), modoServ);
        datosPoblaNoti(resultado, modoServ);

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  protected void ocultarColumna1(boolean s, boolean casos, TemplateManager tm) {

    tm.SetVisible("LAB084", s);
    tm.SetVisible("LIN008", s);
    tm.SetVisible("LAB001", s);
    tm.SetVisible("LIN001", s);
    tm.SetVisible("LAB023", s);
    tm.SetVisible("LAB024", s);
    tm.SetVisible("LIN007", s);
    if (!casos) {
      tm.SetVisible("FIE020", s);
      tm.SetVisible("FIE021", s);
    }
    else {
      tm.SetVisible("FIE002", s);
      tm.SetVisible("FIE004", s);
    }
    tm.SetVisible("LAB040", s);
    tm.SetVisible("LAB046", s);
    tm.SetVisible("LAB047", s);
    tm.SetVisible("LAB059", s);
    tm.SetVisible("LAB060", s);
    tm.SetVisible("LAB072", s);
    tm.SetVisible("LAB073", s);

  }

  protected void ocultarColumna2(boolean s, boolean casos, TemplateManager tm) {

    tm.SetVisible("LAB085", s);
    tm.SetVisible("LIN009", s);
    tm.SetVisible("LAB025", s);
    tm.SetVisible("LIN010", s);
    tm.SetVisible("LAB026", s);
    tm.SetVisible("LAB027", s);
    tm.SetVisible("LIN011", s);
    if (!casos) {
      tm.SetVisible("FIE022", s);
      tm.SetVisible("FIE023", s);
    }
    else {
      tm.SetVisible("FIE001", s);
      tm.SetVisible("FIE011", s);
    }
    tm.SetVisible("LAB041", s);
    tm.SetVisible("LAB048", s);
    tm.SetVisible("LAB049", s);
    tm.SetVisible("LAB061", s);
    tm.SetVisible("LAB062", s);
    tm.SetVisible("LAB074", s);
    tm.SetVisible("LAB075", s);

  }

  protected void ocultarColumna3(boolean s, boolean casos, TemplateManager tm) {

    tm.SetVisible("LAB086", s);
    tm.SetVisible("LIN012", s);
    tm.SetVisible("LAB028", s);
    tm.SetVisible("LIN013", s);
    tm.SetVisible("LAB029", s);
    tm.SetVisible("LAB030", s);
    tm.SetVisible("LIN014", s);
    if (!casos) {
      tm.SetVisible("FIE024", s);
      tm.SetVisible("FIE025", s);
    }
    else {
      tm.SetVisible("FIE012", s);
      tm.SetVisible("FIE013", s);
    }
    tm.SetVisible("LAB042", s);
    tm.SetVisible("LAB050", s);
    tm.SetVisible("LAB051", s);
    tm.SetVisible("LAB063", s);
    tm.SetVisible("LAB064", s);
    tm.SetVisible("LAB076", s);
    tm.SetVisible("LAB077", s);

  }

  protected void ocultarColumna4(boolean s, boolean casos, TemplateManager tm) {

    tm.SetVisible("LAB087", s);
    tm.SetVisible("LIN015", s);
    tm.SetVisible("LAB031", s);
    tm.SetVisible("LIN016", s);
    tm.SetVisible("LAB032", s);
    tm.SetVisible("LAB033", s);
    tm.SetVisible("LIN017", s);
    if (!casos) {
      tm.SetVisible("FIE026", s);
      tm.SetVisible("FIE027", s);
    }
    else {
      tm.SetVisible("FIE014", s);
      tm.SetVisible("FIE015", s);
    }
    tm.SetVisible("LAB043", s);
    tm.SetVisible("LAB052", s);
    tm.SetVisible("LAB053", s);
    tm.SetVisible("LAB065", s);
    tm.SetVisible("LAB066", s);
    tm.SetVisible("LAB078", s);
    tm.SetVisible("LAB079", s);

  }

  protected void ocultarColumna5(boolean s, boolean casos, TemplateManager tm) {

    tm.SetVisible("LAB088", s);
    tm.SetVisible("LIN018", s);
    tm.SetVisible("LAB034", s);
    tm.SetVisible("LIN019", s);
    tm.SetVisible("LAB035", s);
    tm.SetVisible("LAB036", s);
    tm.SetVisible("LIN020", s);
    if (!casos) {
      tm.SetVisible("FIE028", s);
      tm.SetVisible("FIE029", s);
    }
    else {
      tm.SetVisible("FIE016", s);
      tm.SetVisible("FIE017", s);
    }
    tm.SetVisible("LAB044", s);
    tm.SetVisible("LAB054", s);
    tm.SetVisible("LAB055", s);
    tm.SetVisible("LAB067", s);
    tm.SetVisible("LAB068", s);
    tm.SetVisible("LAB080", s);
    tm.SetVisible("LAB081", s);

  }

  //protected void datosPoblaNoti(dataCons93 dat, int opmode) throws Exception {
  // modificacion jlt
  // necesito informaci�n de todo el vector, no s�lo del primer elemento
  protected void datosPoblaNoti(Vector resul, int opmode) throws Exception {

    dataCons93 dat = new dataCons93();

    // modificacion jlt
    // para arreglar problema con las poblaciones
    dataCons93 hsresul = new dataCons93(); // primer elemento del vector
    // lleva informaci�n general
    hsresul = (dataCons93) resul.elementAt(0);
    boolean notfound = false;
    Vector listar = new Vector();
    Vector listar2 = new Vector();

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    tm.SetLabel("LAB084", "");
    tm.SetLabel("LAB085", "");
    tm.SetLabel("LAB086", "");
    tm.SetLabel("LAB087", "");
    tm.SetLabel("LAB088", "");

    //para la codificacion de las zonas
    Hashtable hash = new Hashtable();
    int ind = 0;
    int indbackup = 0;
    String cd = "";
    String ds = "";

    String fallos = "";

    // modificacion jlt
    // for que recorre todas las enfermedades
    // guardo en un vector los vectores cols que
    // contienen los niveles
    for (int j = 0; j < resul.size(); j++) {
      dat = null;
      dat = (dataCons93) resul.elementAt(j);
      listar.addElement( (Vector) dat.COLS);

    }

    for (int i = 0; i < parCons.lista.size(); i++) {
      hash = (Hashtable) parCons.lista.elementAt(i);

      switch (opmode) {
        case servletCTCA:
          cd = (String) hash.get("CD_NIVEL_1");
          if (cd != null) {
            //modificacion jlt
            //ind = dat.COLS.indexOf(cd);
            for (int k = 0; k < listar.size(); k++) {
              listar2 = (Vector) listar.elementAt(k); // vector con los niveles
              if (listar2.contains( (String) cd)) {
                notfound = false;
                break;
              }
              else {
                notfound = true;
              }
            } // for

            if ( (String) hash.get("DS_NIVEL_1") != null) {
              ds = (String) hash.get("DS_NIVEL_1");
            }
            else if ( (String) hash.get("DSL_NIVEL_1") != null) {
              ds = (String) hash.get("DSL_NIVEL_1");
            }
            else {
              ds = (String) hash.get("CD_NIVEL_1");
            }

            //if (ind==-1) fallos = fallos + ds + ", ";
            if (notfound == true) {
              fallos = fallos + ds + ", ";
              ind = -1;
              // modificacion jlt
              // si hay un nivel sin datos, se borra la
              // informaci�n de poblacion de ese nivel para
              // que no salga en el informe
              //tambi�n hay que disminuir el tama�o de la
              // poblacion total
              long restar = 0;

              // para evitar problemas debidos a que haya una nivel
              // sin poblacion asociada
              if (i < hsresul.POBS.size()) {

                restar = ( (Long) hsresul.POBS.elementAt(i)).longValue();
                ( (Vector) hsresul.POBS).setElementAt(new Long( -1), i);
              }

            }
          }

          break;
        case servletCTAREA:
          cd = (String) hash.get("CD_NIVEL_2");
          if (cd != null) {

            //comparo los niveles seleccionados con los recuperados que
            // contienen informaci�n
            for (int k = 0; k < listar.size(); k++) {
              listar2 = (Vector) listar.elementAt(k); // vector con los niveles
              if (listar2.contains( (String) cd)) {
                notfound = false;
                break;
              }
              else {
                notfound = true;
              }
            }

            if ( (String) hash.get("DS_NIVEL_2") != null) {
              ds = (String) hash.get("DS_NIVEL_2");
            }
            else if ( (String) hash.get("DSL_NIVEL_2") != null) {
              ds = (String) hash.get("DSL_NIVEL_2");
            }
            else {
              ds = (String) hash.get("CD_NIVEL_2");
            }

            if (notfound == true) {
              fallos = fallos + ds + ", ";
              ind = -1;

              // si hay un nivel sin datos, se borra la
              // informaci�n de poblacion de ese nivel para
              // que no salga en el informe
              //tambi�n hay que disminuir el tama�o de la
              // poblacion total
              long restar = 0;

              // para evitar problemas debidos a que haya una nivel
              // sin poblacion asociada
              if (i < hsresul.POBS.size()) {
                restar = ( (Long) hsresul.POBS.elementAt(i)).longValue();
                ( (Vector) hsresul.POBS).setElementAt(new Long( -1), i);
              }

            }

          }

          break;
        case servletCTDIST:
          cd = (String) hash.get("CD_ZBS");
          if (cd != null) {

            for (int k = 0; k < listar.size(); k++) {
              listar2 = (Vector) listar.elementAt(k); // vector con los niveles
              if (listar2.contains( (String) cd)) {
                notfound = false;
                break;
              }
              else {
                notfound = true;
              }
            }

            if ( (String) hash.get("DS_ZBS") != null) {
              ds = (String) hash.get("DS_ZBS");
            }
            else if ( (String) hash.get("DSL_ZBS") != null) {
              ds = (String) hash.get("DSL_ZBS");
            }
            else {
              ds = (String) hash.get("CD_ZBS");
            }

            if (notfound == true) {
              fallos = fallos + ds + ", ";
              ind = -1;

              // si hay un nivel sin datos, se borra la
              // informaci�n de poblacion de ese nivel para
              // que no salga en el informe
              //tambi�n hay que disminuir el tama�o de la
              // poblacion total
              long restar = 0;

              // para evitar problemas debidos a que haya una nivel
              // sin poblacion asociada
              if (i < hsresul.POBS.size()) {
                restar = ( (Long) hsresul.POBS.elementAt(i)).longValue();
                ( (Vector) hsresul.POBS).setElementAt(new Long( -1), i);
              }

            }
          }

          break;
      }
      switch (ind) {
        case 0:
          tm.SetLabel("LAB084", cd);
          tm.SetLabel("LAB091", cd + ":");
          tm.SetLabel("LAB092", ds);

          tm.SetVisible("LAB084", true);
          tm.SetVisible("LAB091", true);
          tm.SetVisible("LAB092", true);
          ind++;
          indbackup++;

          break;
        case 1:
          tm.SetLabel("LAB085", cd);
          tm.SetLabel("LAB093", cd + ":");
          tm.SetLabel("LAB094", ds);
          tm.SetVisible("LAB085", true);
          tm.SetVisible("LAB093", true);
          tm.SetVisible("LAB094", true);
          ind++;
          indbackup++;

          break;
        case 2:
          tm.SetLabel("LAB086", cd);
          tm.SetLabel("LAB095", cd + ":");
          tm.SetLabel("LAB096", ds);
          tm.SetVisible("LAB086", true);
          tm.SetVisible("LAB095", true);
          tm.SetVisible("LAB096", true);
          ind++;
          indbackup++;

          break;
        case 3:
          tm.SetLabel("LAB087", cd);
          tm.SetLabel("LAB097", cd + ":");
          tm.SetLabel("LAB098", ds);
          tm.SetVisible("LAB087", true);
          tm.SetVisible("LAB097", true);
          tm.SetVisible("LAB098", true);
          ind++;
          indbackup++;

          break;
        case 4:
          tm.SetLabel("LAB088", cd);
          tm.SetLabel("LAB099", cd + ":");
          tm.SetLabel("LAB100", ds);
          tm.SetVisible("LAB088", true);
          tm.SetVisible("LAB099", true);
          tm.SetVisible("LAB100", true);

          break;

        case -1:
          ind = indbackup;
          break;
      }

    }

    CMessage msgBox = null;
    if (!fallos.equals("")) {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg6.Text") +
                            fallos.substring(0, fallos.lastIndexOf(',')));
      msgBox.show();
      msgBox = null;
    }

    if (tm.GetLabel("LAB084").equals("")) {
      ocultarColumna1(false, parCons.casosTasas, tm);
      tm.SetVisible("LAB091", false);
      tm.SetVisible("LAB092", false);

    }
    if (tm.GetLabel("LAB085").equals("")) {
      ocultarColumna2(false, parCons.casosTasas, tm);
      tm.SetVisible("LAB093", false);
      tm.SetVisible("LAB094", false);
    }

    if (tm.GetLabel("LAB086").equals("")) {
      ocultarColumna3(false, parCons.casosTasas, tm);
      tm.SetVisible("LAB095", false);
      tm.SetVisible("LAB096", false);
    }

    if (tm.GetLabel("LAB087").equals("")) {
      ocultarColumna4(false, parCons.casosTasas, tm);
      tm.SetVisible("LAB097", false);
      tm.SetVisible("LAB098", false);
    }

    if (tm.GetLabel("LAB088").equals("")) {
      ocultarColumna5(false, parCons.casosTasas, tm);
      tm.SetVisible("LAB099", false);
      tm.SetVisible("LAB100", false);
    }

    // modificacion jlt
    // sacamos solo informacion de las poblaciones que
    // tienen datos en casos o tasas
    dat = (dataCons93) resul.elementAt(0);

    /*    //poblacion
        if ((dat.POB1==-1)||(dat.COL1.equals("")))
          tm.SetLabel("LAB040", res.getString("msg7.Text"));
        else
          tm.SetLabel("LAB040", (new Long (dat.POB1)).toString());
        if ((dat.POB2==-1)||(dat.COL2.equals("")))
          tm.SetLabel("LAB041", res.getString("msg7.Text"));
        else
          tm.SetLabel("LAB041", (new Long (dat.POB2)).toString());
        if ((dat.POB3==-1)||(dat.COL3.equals("")))
          tm.SetLabel("LAB042", res.getString("msg7.Text"));
        else
          tm.SetLabel("LAB042", (new Long (dat.POB3)).toString());
        if ((dat.POB4==-1)||(dat.COL4.equals("")))
          tm.SetLabel("LAB043", res.getString("msg7.Text"));
        else
          tm.SetLabel("LAB043", (new Long (dat.POB4)).toString());
        if ((dat.POB5==-1)||(dat.COL5.equals("")))
          tm.SetLabel("LAB044", res.getString("msg7.Text"));
        else
          tm.SetLabel("LAB044", (new Long (dat.POB5)).toString());
        if (dat.POBT<0)
          tm.SetLabel("LAB045", res.getString("msg7.Text"));
        else
          tm.SetLabel("LAB045", (new Long (dat.POBT)).toString());
     */

    //poblacion

    int l = 0;
    int n = 0;
    String etiquetas[] = {
        "LAB040", "LAB041", "LAB042", "LAB043", "LAB044"};

    while (l < dat.POBS.size()) {

      if ( ( (Long) dat.POBS.elementAt(l)).longValue() == -1) {

        // jlt
        // borro estos elementos de las listas correpondientes
        // para que no salgan en el informe. Si no hay informaci�n de
        // un nivel, no sale nada de ese nivel en el informe
        /*dat.NOTRACTS.removeElementAt(l);
                   dat.NOTRACUS.removeElementAt(l);
                   dat.NOTTACTS.removeElementAt(l);
                   dat.NOTTACUS.removeElementAt(l);
                   dat.COBACTS.removeElementAt(l);
                   dat.COBACUS.removeElementAt(l);
         */
        // vamos restando del total

        dat.NOTRACTT = dat.NOTRACTT -
            ( (Integer) dat.NOTRACTS.elementAt(l)).intValue();
        dat.NOTRACTS.setElementAt(new Integer( -1), l);

        dat.NOTRACUT = dat.NOTRACUT -
            ( (Integer) dat.NOTRACUS.elementAt(l)).intValue();
        dat.NOTRACUS.setElementAt(new Integer( -1), l);

        dat.NOTTACTT = dat.NOTTACTT -
            ( (Integer) dat.NOTTACTS.elementAt(l)).intValue();
        dat.NOTTACTS.setElementAt(new Integer( -1), l);

        dat.NOTTACUT = dat.NOTTACUT -
            ( (Integer) dat.NOTTACUS.elementAt(l)).intValue();
        dat.NOTTACUS.setElementAt(new Integer( -1), l);

        float aux = 0;
        aux = ( (Float) dat.COBACTS.elementAt(l)).floatValue();
        if (! (new Float(aux).toString().startsWith("N"))) {
          dat.COBACTT = dat.COBACTT -
              ( (Float) dat.COBACTS.elementAt(l)).floatValue();
        }
        dat.COBACTS.setElementAt(new Float( -1), l);

        aux = ( (Float) dat.COBACUS.elementAt(l)).floatValue();
        if (! (new Float(aux).toString().startsWith("N"))) {
          dat.COBACUT = dat.COBACUT -
              ( (Float) dat.COBACUS.elementAt(l)).floatValue();
        }
        dat.COBACUS.setElementAt(new Float( -1), l);

        //tm.SetLabel(etiquetas[l], res.getString("msg7.Text"));
        l++;
      }
      else {
        tm.SetLabel(etiquetas[n], ( (Long) dat.POBS.elementAt(l)).toString());
        l++;
        n++;
      }

    } // fin del while

    if (dat.POBT < 0) {
      tm.SetLabel("LAB045", res.getString("msg7.Text"));
    }
    else {
      tm.SetLabel("LAB045", (new Long(dat.POBT)).toString());

      //notificadores reales
      /*    tm.SetLabel("LAB046", (new Integer (dat.NOTRACT1)).toString());
          tm.SetLabel("LAB047", (new Integer (dat.NOTRACU1)).toString());
          tm.SetLabel("LAB048", (new Integer (dat.NOTRACT2)).toString());
          tm.SetLabel("LAB049", (new Integer (dat.NOTRACU2)).toString());
          tm.SetLabel("LAB050", (new Integer (dat.NOTRACT3)).toString());
          tm.SetLabel("LAB051", (new Integer (dat.NOTRACU3)).toString());
          tm.SetLabel("LAB052", (new Integer (dat.NOTRACT4)).toString());
          tm.SetLabel("LAB053", (new Integer (dat.NOTRACU4)).toString());
          tm.SetLabel("LAB054", (new Integer (dat.NOTRACT5)).toString());
          tm.SetLabel("LAB055", (new Integer (dat.NOTRACU5)).toString());
          tm.SetLabel("LAB056", (new Integer (dat.NOTRACTT)).toString());
          tm.SetLabel("LAB057", (new Integer (dat.NOTRACUT)).toString());
       */

      //notificadores reales actuales
    }
    int i = 0;
    int j = 0;
    String etiquetasra[] = {
        "LAB046", "LAB048", "LAB050",
        "LAB052", "LAB054"};

    //while (i<dat.NOTRACTS.size() ) {
//    int p = ((Integer)dat.NOTRACTS.elementAt(i)).intValue();

    /*    while (i<dat.NOTRACTS.size() &&
          ((Integer)dat.NOTRACTS.elementAt(i)).intValue() != -1  ) {
     */

    while (i < dat.NOTRACTS.size()) {

      // if ( ((Integer)dat.NOTRACTS.elementAt(i)).intValue() == 0 ) {
      //   i++;
      // } else {

      if ( ( (Integer) dat.NOTRACTS.elementAt(i)).intValue() != -1) {

        tm.SetLabel(etiquetasra[j],
                    ( (Integer) dat.NOTRACTS.elementAt(i)).toString());
        //i ++;
        j++;
      }

      i++;

      // }

    } // fin del while
    // total real actual
    tm.SetLabel("LAB056", (new Integer(dat.NOTRACTT)).toString());

    // notificadores reales acumulados
    int a = 0;
    int b = 0;
    String etiquetasrac[] = {
        "LAB047", "LAB049",
        "LAB051", "LAB053", "LAB055"};

    /*    while (a<dat.NOTRACUS.size() &&
          ((Integer)dat.NOTRACUS.elementAt(a)).intValue() != -1 ) {
     */while (a < dat.NOTRACUS.size()) {

      // if ( ((Integer)dat.NOTRACUS.elementAt(a)).intValue() == 0 ) {
      //   a++;
      // } else {

      if ( ( (Integer) dat.NOTRACUS.elementAt(a)).intValue() != -1) {
        tm.SetLabel(etiquetasrac[b],
                    ( (Integer) dat.NOTRACUS.elementAt(a)).toString());
        //a ++;
        b++;
      }

      a++;
      // }

    } // fin del while
    // total real acumulados
    tm.SetLabel("LAB057", (new Integer(dat.NOTRACUT)).toString());

    /*    //notificadores teoricos
        tm.SetLabel("LAB059", (new Integer (dat.NOTTACT1)).toString());
        tm.SetLabel("LAB060", (new Integer (dat.NOTTACU1)).toString());
        tm.SetLabel("LAB061", (new Integer (dat.NOTTACT2)).toString());
        tm.SetLabel("LAB062", (new Integer (dat.NOTTACU2)).toString());
        tm.SetLabel("LAB063", (new Integer (dat.NOTTACT3)).toString());
        tm.SetLabel("LAB064", (new Integer (dat.NOTTACU3)).toString());
        tm.SetLabel("LAB065", (new Integer (dat.NOTTACT4)).toString());
        tm.SetLabel("LAB066", (new Integer (dat.NOTTACU4)).toString());
        tm.SetLabel("LAB067", (new Integer (dat.NOTTACT5)).toString());
        tm.SetLabel("LAB068", (new Integer (dat.NOTTACU5)).toString());
        tm.SetLabel("LAB069", (new Integer (dat.NOTTACTT)).toString());
        tm.SetLabel("LAB070", (new Integer (dat.NOTTACUT)).toString());
     */

    // notificadores teoricos actuales
    int c = 0;
    int d = 0;
    String etiquetasta[] = {
        "LAB059", "LAB061",
        "LAB063", "LAB065", "LAB067"};

    /*    while (c<dat.NOTTACTS.size() &&
          ((Integer)dat.NOTTACTS.elementAt(c)).intValue() != -1 ) {
     */

    while (c < dat.NOTTACTS.size()) {

      // if ( ((Integer)dat.NOTTACTS.elementAt(c)).intValue() == 0 ) {
      //   c++;
      //  } else {

      if ( ( (Integer) dat.NOTTACTS.elementAt(c)).intValue() != -1) {
        tm.SetLabel(etiquetasta[d],
                    ( (Integer) dat.NOTTACTS.elementAt(c)).toString());
        //c ++;
        d++;
      }

      c++;
      // }

    } // fin del while
    // total teoricos actuales
    tm.SetLabel("LAB069", (new Integer(dat.NOTTACTT)).toString());

    // notificadores teoricos acumulados
    int e = 0;
    int f = 0;
    String etiquetastac[] = {
        "LAB060", "LAB062",
        "LAB064", "LAB066", "LAB068"};

    /*    while (e<dat.NOTTACUS.size() &&
          ((Integer)dat.NOTTACUS.elementAt(e)).intValue() != -1 ) {
     */

    while (e < dat.NOTTACUS.size()) {

      //  if ( ((Integer)dat.NOTTACUS.elementAt(e)).intValue() == 0 ) {
      //    e++;
      // } else {

      if ( ( (Integer) dat.NOTTACUS.elementAt(e)).intValue() != -1) {
        tm.SetLabel(etiquetastac[f],
                    ( (Integer) dat.NOTTACUS.elementAt(e)).toString());
        //e ++;
        f++;
      }

      e++;
      // }

    } // fin del while
    // total teoricos acumulados
    tm.SetLabel("LAB070", (new Integer(dat.NOTTACUT)).toString());

    //COBERTURAS

    //System.out.println(" cobertura " + (new Float(dat.COBACT1)).toString());

    /*
        String cob = (new Float(dat.COBACT1)).toString();
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-";
        tm.SetLabel("LAB072",cob );
        cob = (new Float(dat.COBACU1)).toString();
        //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-" ;
        tm.SetLabel("LAB073", cob);
        cob = (new Float(dat.COBACT2)).toString();
        //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-";
        tm.SetLabel("LAB074", cob);
        cob = (new Float(dat.COBACU2)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-" ;
        tm.SetLabel("LAB075", cob);
        cob = (new Float(dat.COBACT3)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-";
        tm.SetLabel("LAB076", cob);
        cob = (new Float(dat.COBACU3)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-";
        tm.SetLabel("LAB077", cob);
        cob = (new Float(dat.COBACT4)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-" ;
        tm.SetLabel("LAB078", cob);
        cob = (new Float(dat.COBACU4)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-" ;
        tm.SetLabel("LAB079", cob);
        cob = (new Float(dat.COBACT5)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-";
        tm.SetLabel("LAB080", cob);
        cob = (new Float(dat.COBACU5)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-";
        tm.SetLabel("LAB081", cob);
        cob = (new Float(dat.COBACTT)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-";
        tm.SetLabel("LAB082", cob);
        cob = (new Float(dat.COBACUT)).toString();
            //System.out.println(" cobertura " + cob);
        if (cob.indexOf('.')!=-1){
          cob = cob.substring(0, cob.indexOf('.')+2);
        }else if (cob.startsWith("N"))
          cob = "-";
        tm.SetLabel("LAB083", cob);
     */
    /////////////
    // coberturas actuales
    int x = 0;
    int y = 0;
    String cob = new String();

    String etiquetasca[] = {
        "LAB072", "LAB074",
        "LAB076", "LAB078", "LAB080"};

    /*    while (x<dat.COBACTS.size() &&
          ((Float)dat.COBACTS.elementAt(x)).floatValue() != -1 ) {
     */

    while (x < dat.COBACTS.size()) {

      if ( ( (Float) dat.COBACTS.elementAt(x)).floatValue() != -1) {
        cob = ( (Float) dat.COBACTS.elementAt(x)).toString();
        if (cob.indexOf('.') != -1) {
          cob = cob.substring(0, cob.indexOf('.') + 2);
          //tm.SetLabel(etiquetasca[y],cob );
          //x ++;
          //y ++;
        }
        else if (cob.startsWith("N")) {
          cob = "-";
          //x ++;
          //y ++;
        }
        tm.SetLabel(etiquetasca[y], cob);
        //x ++;
        y++;

      }

      x++;

    } // fin del while
    // total cobertura actual
    cob = (new Float(dat.COBACTT)).toString();
    if (cob.indexOf('.') != -1) {
      cob = cob.substring(0, cob.indexOf('.') + 2);
    }
    else if (cob.startsWith("N")) {
      cob = "-";
    }
    tm.SetLabel("LAB082", cob);

    // coberturas acumuladas
    int v = 0;
    int w = 0;
    String etiquetascac[] = {
        "LAB073", "LAB075",
        "LAB077", "LAB079", "LAB081"};

    /*    while (v <dat.COBACUS.size() &&
          ((Float)dat.COBACUS.elementAt(v)).floatValue() != -1 ) {
     */

    while (v < dat.COBACUS.size()) {

      if ( ( (Float) dat.COBACUS.elementAt(v)).floatValue() != -1) {
        cob = ( (Float) dat.COBACUS.elementAt(v)).toString();
        if (cob.indexOf('.') != -1) {
          cob = cob.substring(0, cob.indexOf('.') + 2);
          //tm.SetLabel(etiquetascac[w],cob );
          //v ++;
          //w ++;
        }
        else if (cob.startsWith("N")) {
          cob = "-";
          //v++;
        }
        tm.SetLabel(etiquetascac[w], cob);
        //v ++;
        w++;

      }

      v++;

    } // fin del while
    // total cobertura acumulada
    cob = (new Float(dat.COBACUT)).toString();
    if (cob.indexOf('.') != -1) {
      cob = cob.substring(0, cob.indexOf('.') + 2);
    }
    else if (cob.startsWith("N")) {
      cob = "-";
    }
    tm.SetLabel("LAB083", cob);

    /////////////

    /*
         switch(parCons.lista.size()){
      case 1:
        tm.SetVisible("LAB041", false);
        tm.SetVisible("LAB042", false);
        tm.SetVisible("LAB043", false);
        tm.SetVisible("LAB044", false);
        tm.SetVisible("LAB048", false);
        tm.SetVisible("LAB049", false);
        tm.SetVisible("LAB050", false);
        tm.SetVisible("LAB051", false);
        tm.SetVisible("LAB052", false);
        tm.SetVisible("LAB053", false);
        tm.SetVisible("LAB054", false);
        tm.SetVisible("LAB055", false);
        tm.SetVisible("LAB061", false);
        tm.SetVisible("LAB062", false);
        tm.SetVisible("LAB063", false);
        tm.SetVisible("LAB064", false);
        tm.SetVisible("LAB065", false);
        tm.SetVisible("LAB066", false);
        tm.SetVisible("LAB067", false);
        tm.SetVisible("LAB068", false);
        tm.SetVisible("LAB074", false);
        tm.SetVisible("LAB075", false);
        tm.SetVisible("LAB076", false);
        tm.SetVisible("LAB077", false);
        tm.SetVisible("LAB078", false);
        tm.SetVisible("LAB079", false);
        tm.SetVisible("LAB080", false);
        tm.SetVisible("LAB081", false);
        break;
      case 2:
        tm.SetVisible("LAB041", true);
        tm.SetVisible("LAB048", true);
        tm.SetVisible("LAB049", true);
        tm.SetVisible("LAB061", true);
        tm.SetVisible("LAB062", true);
        tm.SetVisible("LAB074", true);
        tm.SetVisible("LAB075", true);
        tm.SetVisible("LAB042", false);
        tm.SetVisible("LAB043", false);
        tm.SetVisible("LAB044", false);
        tm.SetVisible("LAB050", false);
        tm.SetVisible("LAB051", false);
        tm.SetVisible("LAB052", false);
        tm.SetVisible("LAB053", false);
        tm.SetVisible("LAB054", false);
        tm.SetVisible("LAB055", false);
        tm.SetVisible("LAB059", false);
        tm.SetVisible("LAB063", false);
        tm.SetVisible("LAB064", false);
        tm.SetVisible("LAB065", false);
        tm.SetVisible("LAB066", false);
        tm.SetVisible("LAB067", false);
        tm.SetVisible("LAB068", false);
        tm.SetVisible("LAB076", false);
        tm.SetVisible("LAB077", false);
        tm.SetVisible("LAB078", false);
        tm.SetVisible("LAB079", false);
        tm.SetVisible("LAB080", false);
        tm.SetVisible("LAB081", false);
        break;
      case 3:
        tm.SetVisible("LAB041", true);
        tm.SetVisible("LAB048", true);
        tm.SetVisible("LAB049", true);
        tm.SetVisible("LAB061", true);
        tm.SetVisible("LAB062", true);
        tm.SetVisible("LAB074", true);
        tm.SetVisible("LAB075", true);
        tm.SetVisible("LAB042", true);
        tm.SetVisible("LAB050", true);
        tm.SetVisible("LAB051", true);
        tm.SetVisible("LAB063", true);
        tm.SetVisible("LAB064", true);
        tm.SetVisible("LAB076", true);
        tm.SetVisible("LAB077", true);
        tm.SetVisible("LAB043", false);
        tm.SetVisible("LAB044", false);
        tm.SetVisible("LAB052", false);
        tm.SetVisible("LAB053", false);
        tm.SetVisible("LAB054", false);
        tm.SetVisible("LAB055", false);
        tm.SetVisible("LAB059", false);
        tm.SetVisible("LAB065", false);
        tm.SetVisible("LAB066", false);
        tm.SetVisible("LAB067", false);
        tm.SetVisible("LAB068", false);
        tm.SetVisible("LAB078", false);
        tm.SetVisible("LAB079", false);
        tm.SetVisible("LAB080", false);
        tm.SetVisible("LAB081", false);
        break;
      case 4:
        tm.SetVisible("LAB041", true);
        tm.SetVisible("LAB048", true);
        tm.SetVisible("LAB049", true);
        tm.SetVisible("LAB061", true);
        tm.SetVisible("LAB062", true);
        tm.SetVisible("LAB074", true);
        tm.SetVisible("LAB075", true);
        tm.SetVisible("LAB042", true);
        tm.SetVisible("LAB050", true);
        tm.SetVisible("LAB051", true);
        tm.SetVisible("LAB063", true);
        tm.SetVisible("LAB064", true);
        tm.SetVisible("LAB076", true);
        tm.SetVisible("LAB077", true);
        tm.SetVisible("LAB043", true);
        tm.SetVisible("LAB052", true);
        tm.SetVisible("LAB053", true);
        tm.SetVisible("LAB065", true);
        tm.SetVisible("LAB066", true);
        tm.SetVisible("LAB079", true);
        tm.SetVisible("LAB080", true);
        tm.SetVisible("LAB044", false);
        tm.SetVisible("LAB054", false);
        tm.SetVisible("LAB055", false);
        tm.SetVisible("LAB059", false);
        tm.SetVisible("LAB067", false);
        tm.SetVisible("LAB068", false);
        tm.SetVisible("LAB078", false);
        tm.SetVisible("LAB079", false);
        break;
      case 5:
        tm.SetVisible("LAB041", true);
        tm.SetVisible("LAB048", true);
        tm.SetVisible("LAB049", true);
        tm.SetVisible("LAB061", true);
        tm.SetVisible("LAB062", true);
        tm.SetVisible("LAB074", true);
        tm.SetVisible("LAB075", true);
        tm.SetVisible("LAB042", true);
        tm.SetVisible("LAB050", true);
        tm.SetVisible("LAB051", true);
        tm.SetVisible("LAB063", true);
        tm.SetVisible("LAB064", true);
        tm.SetVisible("LAB076", true);
        tm.SetVisible("LAB077", true);
        tm.SetVisible("LAB043", true);
        tm.SetVisible("LAB052", true);
        tm.SetVisible("LAB053", true);
        tm.SetVisible("LAB065", true);
        tm.SetVisible("LAB066", true);
        tm.SetVisible("LAB078", true);
        tm.SetVisible("LAB079", true);
        tm.SetVisible("LAB044", true);
        tm.SetVisible("LAB055", true);
        tm.SetVisible("LAB056", true);
        tm.SetVisible("LAB068", true);
        tm.SetVisible("LAB069", true);
        tm.SetVisible("LAB080", true);
        tm.SetVisible("LAB081", true);
        break;
         }
     */
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;
    String sEtiqueta[] = {
        "CRITERIO1",
        "CRITERIO2",
        "CRITERIO3",
        "CRITERIO4",
        "CRITERIO5"};

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    for (int i = 0; i < 5; i++) {
      tm.SetLabel(sEtiqueta[i], "");

    }
    tm.SetLabel(sEtiqueta[iEtiqueta],
                res.getString("msg8.Text") + parCons.semD + " " +
                res.getString("msg9.Text") + parCons.semH + " " +
                res.getString("msg10.Text") + " " + parCons.ano);
    iEtiqueta++;
    if (!parCons.area.equals("")) {
      tm.SetLabel(sEtiqueta[iEtiqueta],
                  res.getString("msg11.Text") + parCons.area + " " +
                  parCons.areaDesc);
    }
    iEtiqueta++;
    if (!parCons.distrito.equals("")) {
      tm.SetLabel(sEtiqueta[iEtiqueta],
                  res.getString("msg12.Text") + parCons.distrito + " " +
                  parCons.distritoDesc);
    }
    iEtiqueta++;

    //titulo
    String zona = "";
    if (parCons.area.equals("")) {
      zona = res.getString("msg13.Text");
      tm.SetLabel("LAB017", res.getString("msg14.Text"));
      tm.SetLabel("LAB090", res.getString("msg15.Text"));
    }
    else if (parCons.distrito.equals("")) {
      zona = res.getString("msg16.Text");
      tm.SetLabel("LAB017",
                  res.getString("msg17.Text") + parCons.area + " " +
                  parCons.areaDesc);
      tm.SetLabel("LAB090", res.getString("msg18.Text"));
    }
    else {
      zona = res.getString("msg19.Text");
      tm.SetLabel("LAB090", res.getString("msg20.Text"));
      tm.SetLabel("LAB017",
                  res.getString("msg21.Text") + parCons.distrito + " " +
                  parCons.distritoDesc);
    }

//_______________  C�digo que se cepill� el wizzard !!!!!!!!!!!

    if (parCons.casosTasas) {
      if (parCons.semD.equals(parCons.semH)) {
        //tm.SetLabel("LAB000", "Casos notificados por " + zona + " en la semana " + parCons.ano + " " + parCons.semD + " y acumulados desde el inicio del a�o epidemiol�gico");
        tm.SetLabel("LAB000",
                    "Casos notificados por " + zona + " en la semana " +
                    parCons.semD + " del a�o " + parCons.ano +
                    " y acumulados desde el inicio del a�o epidemiol�gico");
      }
      else {
        //tm.SetLabel("LAB000", "Casos notificados por " + zona + " en las semanas " + parCons.ano + " " + parCons.semD + " a " + parCons.semH +  " y acumulados desde el inicio del a�o epidemiol�gico");
        tm.SetLabel("LAB000",
                    "Casos notificados por " + zona + " en las semanas " +
                    parCons.semD + " a " + parCons.semH + " del a�o " +
                    parCons.ano +
                    " y acumulados desde el inicio del a�o epidemiol�gico");
      }

    }
    else {

      if (parCons.semD.equals(parCons.semH)) {
        //tm.SetLabel("LAB000", "Casos notificados por " + zona + " en la semana " + parCons.ano + " " + parCons.semD + " y acumulados desde el inicio del a�o epidemiol�gico");
        tm.SetLabel("LAB000",
                    "Tasas notificadas por " + zona + " en la semana " +
                    parCons.semD + " del a�o " + parCons.ano +
                    " y acumulados desde el inicio del a�o epidemiol�gico");
      }
      else {
        //tm.SetLabel("LAB000", "Casos notificados por " + zona + " en las semanas " + parCons.ano + " " + parCons.semD + " a " + parCons.semH +  " y acumulados desde el inicio del a�o epidemiol�gico");
        tm.SetLabel("LAB000",
                    "Tasas notificadas por " + zona + " en las semanas " +
                    parCons.semD + " a " + parCons.semH + " del a�o " +
                    parCons.ano +
                    " y acumulados desde el inicio del a�o epidemiol�gico");
      }

    }

    //casos o tasas

    if (parCons.casosTasas) {

      tm.SetLabel("LAB001", "Casos");
      tm.SetLabel("LAB025", "Casos");
      tm.SetLabel("LAB028", "Casos");
      tm.SetLabel("LAB031", "Casos");
      tm.SetLabel("LAB034", "Casos");
      tm.SetLabel("LAB037", "Casos");

      tm.SetVisible("FIE002", true);
      tm.SetVisible("FIE004", true);
      tm.SetVisible("FIE001", true);
      tm.SetVisible("FIE011", true);
      tm.SetVisible("FIE012", true);
      tm.SetVisible("FIE013", true);
      tm.SetVisible("FIE014", true);
      tm.SetVisible("FIE015", true);
      tm.SetVisible("FIE016", true);
      tm.SetVisible("FIE017", true);
      tm.SetVisible("FIE018", true);
      tm.SetVisible("FIE019", true);

      tm.SetVisible("FIE020", false);
      tm.SetVisible("FIE021", false);
      tm.SetVisible("FIE022", false);
      tm.SetVisible("FIE023", false);
      tm.SetVisible("FIE024", false);
      tm.SetVisible("FIE025", false);
      tm.SetVisible("FIE026", false);
      tm.SetVisible("FIE027", false);
      tm.SetVisible("FIE028", false);
      tm.SetVisible("FIE029", false);
      tm.SetVisible("FIE030", false);
      tm.SetVisible("FIE031", false);

    }
    else {

      tm.SetLabel("LAB001", "Tasas");
      tm.SetLabel("LAB025", "Tasas");
      tm.SetLabel("LAB028", "Tasas");
      tm.SetLabel("LAB031", "Tasas");
      tm.SetLabel("LAB034", "Tasas");
      tm.SetLabel("LAB037", "Tasas");

      tm.SetVisible("FIE002", false);
      tm.SetVisible("FIE004", false);
      tm.SetVisible("FIE001", false);
      tm.SetVisible("FIE011", false);
      tm.SetVisible("FIE012", false);
      tm.SetVisible("FIE013", false);
      tm.SetVisible("FIE014", false);
      tm.SetVisible("FIE015", false);
      tm.SetVisible("FIE016", false);
      tm.SetVisible("FIE017", false);
      tm.SetVisible("FIE018", false);
      tm.SetVisible("FIE019", false);

      tm.SetVisible("FIE020", true);
      tm.SetVisible("FIE021", true);
      tm.SetVisible("FIE022", true);
      tm.SetVisible("FIE023", true);
      tm.SetVisible("FIE024", true);
      tm.SetVisible("FIE025", true);
      tm.SetVisible("FIE026", true);
      tm.SetVisible("FIE027", true);
      tm.SetVisible("FIE028", true);
      tm.SetVisible("FIE029", true);
      tm.SetVisible("FIE030", true);
      tm.SetVisible("FIE031", true);

      // modificacion jlt 18/09/01
      tm.SetLabel("ETIQUETA_POBLACION", res.getString("msg44.Text"));

    }

    switch (parCons.lista.size()) {
      case 1:
        ocultarColumna1(true, parCons.casosTasas, tm);
        ocultarColumna2(false, parCons.casosTasas, tm);
        ocultarColumna3(false, parCons.casosTasas, tm);
        ocultarColumna4(false, parCons.casosTasas, tm);
        ocultarColumna5(false, parCons.casosTasas, tm);
        /*
          tm.SetVisible("LIN009", false);
          tm.SetVisible("LIN012", false);
          tm.SetVisible("LIN015", false);
          tm.SetVisible("LIN018", false);
          tm.SetVisible("LAB025", false);
          tm.SetVisible("LAB028", false);
          tm.SetVisible("LAB031", false);
          tm.SetVisible("LAB034", false);
          tm.SetVisible("LIN010", false);
          tm.SetVisible("LIN013", false);
          tm.SetVisible("LIN016", false);
          tm.SetVisible("LIN019", false);
          tm.SetVisible("LAB026", false);
          tm.SetVisible("LAB027", false);
          tm.SetVisible("LAB029", false);
          tm.SetVisible("LAB030", false);
          tm.SetVisible("LAB032", false);
          tm.SetVisible("LAB033", false);
          tm.SetVisible("LAB035", false);
          tm.SetVisible("LAB036", false);
          tm.SetVisible("LIN011", false);
          tm.SetVisible("LIN014", false);
          tm.SetVisible("LIN017", false);
          tm.SetVisible("LIN020", false);
          tm.SetVisible("FIE022", false);
          tm.SetVisible("FIE023", false);
          tm.SetVisible("FIE024", false);
          tm.SetVisible("FIE025", false);
          tm.SetVisible("FIE026", false);
          tm.SetVisible("FIE027", false);
          tm.SetVisible("FIE028", false);
          tm.SetVisible("FIE029", false);
          tm.SetVisible("FIE001", false);
          tm.SetVisible("FIE011", false);
          tm.SetVisible("FIE012", false);
          tm.SetVisible("FIE013", false);
          tm.SetVisible("FIE014", false);
          tm.SetVisible("FIE015", false);
          tm.SetVisible("FIE016", false);
          tm.SetVisible("FIE017", false);
         */

        break;
      case 2:
        ocultarColumna1(true, parCons.casosTasas, tm);
        ocultarColumna2(true, parCons.casosTasas, tm);
        ocultarColumna3(false, parCons.casosTasas, tm);
        ocultarColumna4(false, parCons.casosTasas, tm);
        ocultarColumna5(false, parCons.casosTasas, tm);
        /*
           tm.SetVisible("LIN009", true);
           tm.SetVisible("LAB025", true);
           tm.SetVisible("LIN010", true);
           tm.SetVisible("LAB026", true);
           tm.SetVisible("LAB027", true);
           tm.SetVisible("LIN011", true);
           if (!parCons.casosTasas){
             tm.SetVisible("FIE022", true);
             tm.SetVisible("FIE023", true);
           }
           else{
             tm.SetVisible("FIE001", true);
             tm.SetVisible("FIE011", true);
           }
           tm.SetVisible("LIN012", false);
           tm.SetVisible("LIN015", false);
           tm.SetVisible("LIN018", false);
           tm.SetVisible("LAB028", false);
           tm.SetVisible("LAB031", false);
           tm.SetVisible("LAB034", false);
           tm.SetVisible("LIN013", false);
           tm.SetVisible("LIN016", false);
           tm.SetVisible("LIN019", false);
           tm.SetVisible("LAB029", false);
           tm.SetVisible("LAB030", false);
           tm.SetVisible("LAB032", false);
           tm.SetVisible("LAB033", false);
           tm.SetVisible("LAB035", false);
           tm.SetVisible("LAB036", false);
           tm.SetVisible("LIN014", false);
           tm.SetVisible("LIN017", false);
           tm.SetVisible("LIN020", false);
           tm.SetVisible("FIE024", false);
           tm.SetVisible("FIE025", false);
           tm.SetVisible("FIE026", false);
           tm.SetVisible("FIE027", false);
           tm.SetVisible("FIE028", false);
           tm.SetVisible("FIE029", false);
           tm.SetVisible("FIE012", false);
           tm.SetVisible("FIE013", false);
           tm.SetVisible("FIE014", false);
           tm.SetVisible("FIE015", false);
           tm.SetVisible("FIE016", false);
           tm.SetVisible("FIE017", false);
         */
        break;
      case 3:
        ocultarColumna1(true, parCons.casosTasas, tm);
        ocultarColumna2(true, parCons.casosTasas, tm);
        ocultarColumna3(true, parCons.casosTasas, tm);
        ocultarColumna4(false, parCons.casosTasas, tm);
        ocultarColumna5(false, parCons.casosTasas, tm);
        /*
          tm.SetVisible("LIN009", true);
          tm.SetVisible("LAB025", true);
          tm.SetVisible("LIN010", true);
          tm.SetVisible("LAB026", true);
          tm.SetVisible("LAB027", true);
          tm.SetVisible("LIN011", true);
          tm.SetVisible("LIN012", true);
          tm.SetVisible("LAB028", true);
          tm.SetVisible("LIN013", true);
          tm.SetVisible("LAB029", true);
          tm.SetVisible("LAB030", true);
          tm.SetVisible("LIN014", true);
          if (!parCons.casosTasas){
            tm.SetVisible("FIE024", true);
            tm.SetVisible("FIE025", true);
            tm.SetVisible("FIE022", true);
            tm.SetVisible("FIE023", true);
          }
          else {
            tm.SetVisible("FIE012", true);
            tm.SetVisible("FIE013", true);
            tm.SetVisible("FIE001", true);
            tm.SetVisible("FIE011", true);
          }
          tm.SetVisible("LIN015", false);
          tm.SetVisible("LIN018", false);
          tm.SetVisible("LAB031", false);
          tm.SetVisible("LAB034", false);
          tm.SetVisible("LIN016", false);
          tm.SetVisible("LIN019", false);
          tm.SetVisible("LAB032", false);
          tm.SetVisible("LAB033", false);
          tm.SetVisible("LAB035", false);
          tm.SetVisible("LAB036", false);
          tm.SetVisible("LIN017", false);
          tm.SetVisible("LIN020", false);
          tm.SetVisible("FIE026", false);
          tm.SetVisible("FIE027", false);
          tm.SetVisible("FIE028", false);
          tm.SetVisible("FIE029", false);
          tm.SetVisible("FIE014", false);
          tm.SetVisible("FIE015", false);
          tm.SetVisible("FIE016", false);
          tm.SetVisible("FIE017", false);
         */
        break;
      case 4:
        ocultarColumna1(true, parCons.casosTasas, tm);
        ocultarColumna2(true, parCons.casosTasas, tm);
        ocultarColumna3(true, parCons.casosTasas, tm);
        ocultarColumna4(true, parCons.casosTasas, tm);
        ocultarColumna5(false, parCons.casosTasas, tm);
        /*
          tm.SetVisible("LIN009", true);
          tm.SetVisible("LAB025", true);
          tm.SetVisible("LIN010", true);
          tm.SetVisible("LAB026", true);
          tm.SetVisible("LAB027", true);
          tm.SetVisible("LIN011", true);
          tm.SetVisible("LIN012", true);
          tm.SetVisible("LAB028", true);
          tm.SetVisible("LIN013", true);
          tm.SetVisible("LAB029", true);
          tm.SetVisible("LAB030", true);
          tm.SetVisible("LIN014", true);
          tm.SetVisible("LIN015", true);
          tm.SetVisible("LAB031", true);
          tm.SetVisible("LIN016", true);
          tm.SetVisible("LAB032", true);
          tm.SetVisible("LAB033", true);
          tm.SetVisible("LIN017", true);
          if (!parCons.casosTasas){
            tm.SetVisible("FIE026", true);
            tm.SetVisible("FIE027", true);
            tm.SetVisible("FIE024", true);
            tm.SetVisible("FIE025", true);
            tm.SetVisible("FIE022", true);
            tm.SetVisible("FIE023", true);
          }
          else {
            tm.SetVisible("FIE012", true);
            tm.SetVisible("FIE013", true);
            tm.SetVisible("FIE001", true);
            tm.SetVisible("FIE011", true);
            tm.SetVisible("FIE014", true);
            tm.SetVisible("FIE015", true);
          }
          tm.SetVisible("LIN018", false);
          tm.SetVisible("LAB034", false);
          tm.SetVisible("LIN019", false);
          tm.SetVisible("LAB035", false);
          tm.SetVisible("LAB036", false);
          tm.SetVisible("LIN020", false);
          tm.SetVisible("FIE028", false);
          tm.SetVisible("FIE029", false);
          tm.SetVisible("FIE016", false);
          tm.SetVisible("FIE017", false);
         */
        break;
      case 5:

        ocultarColumna1(true, parCons.casosTasas, tm);
        ocultarColumna2(true, parCons.casosTasas, tm);
        ocultarColumna3(true, parCons.casosTasas, tm);
        ocultarColumna4(true, parCons.casosTasas, tm);
        ocultarColumna5(true, parCons.casosTasas, tm);
        /*
          tm.SetVisible("LIN009", true);
          tm.SetVisible("LAB025", true);
          tm.SetVisible("LIN010", true);
          tm.SetVisible("LAB026", true);
          tm.SetVisible("LAB027", true);
          tm.SetVisible("LIN011", true);
          tm.SetVisible("LIN012", true);
          tm.SetVisible("LAB028", true);
          tm.SetVisible("LIN013", true);
          tm.SetVisible("LAB029", true);
          tm.SetVisible("LAB030", true);
          tm.SetVisible("LIN014", true);
          tm.SetVisible("LIN015", true);
          tm.SetVisible("LAB031", true);
          tm.SetVisible("LIN016", true);
          tm.SetVisible("LAB032", true);
          tm.SetVisible("LAB033", true);
          tm.SetVisible("LIN017", true);
          tm.SetVisible("LIN018", true);
          tm.SetVisible("LAB034", true);
          tm.SetVisible("LIN019", true);
          tm.SetVisible("LAB035", true);
          tm.SetVisible("LAB036", true);
          tm.SetVisible("LIN020", true);
          if (!parCons.casosTasas){
            tm.SetVisible("FIE028", true);
            tm.SetVisible("FIE029", true);
            tm.SetVisible("FIE026", true);
            tm.SetVisible("FIE027", true);
            tm.SetVisible("FIE024", true);
            tm.SetVisible("FIE025", true);
            tm.SetVisible("FIE022", true);
            tm.SetVisible("FIE023", true);
          }
          else {
            tm.SetVisible("FIE012", true);
            tm.SetVisible("FIE013", true);
            tm.SetVisible("FIE001", true);
            tm.SetVisible("FIE011", true);
            tm.SetVisible("FIE014", true);
            tm.SetVisible("FIE015", true);
            tm.SetVisible("FIE016", true);
            tm.SetVisible("FIE017", true);
          }
         */
        break;

    }

  }

  private void setTotal(String total) {
    TemplateManager tm = erw.GetTemplateManager();
    //tm.SetLabel ("LAB019", total);
  }

  public void MostrarGrafica() {
  }
}
