//Title:        Casos o Tasas por 100000 habitantes por Area
//Version:
//Copyright:    Copyright (c) 1998
//Author:
//Company:
//Description:  Consultas 9.3.10, 9.3.11 y 9.3.12

package casostasascienarea;

import java.util.ResourceBundle;

import capp.CApp;

public class casosTasasCienArea
    extends CApp {

  public panelConsulta parametros;

  public casosTasasCienArea() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("casostasascienarea.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;

    parametros = new panelConsulta(a);
    VerPanel("", parametros);

    /*
     // modos de la lista
        int AREA = 12;
        int DISTRITO = 13;
        int ZBS = 14;
        int ENFERMEDAD = 15;
     ListaADZE dia = new ListaADZE(a,ENFERMEDAD,2, new CLista());
     dia.show();
     if (dia.resultado()){
       CLista lis = dia.listaRes();
       System.out.println("ListaRes : " + lis);
       dia = new ListaADZE(a,ENFERMEDAD,2,lis);
       dia.show();
       if (dia.resultado()){
         lis = dia.listaRes();
         System.out.println("ListaRes : " + lis);
       }
     }
     */

  }

}
