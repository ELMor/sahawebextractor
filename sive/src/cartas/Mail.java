package cartas;

import java.net.URL;

import capp.CApp;
import capp.CLista;
import capp.CMail;
import nivel1.DataNivel1;
import sapp.StubSrvBD;

public class Mail {

  //Para obtener datos de nivel 1
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final int servletOBTENER_X_CODIGO = 3;
  protected StubSrvBD stubCliente = new StubSrvBD();

  String miHost = null;

  public Mail(String host) {
    miHost = host;
  }

  public Mail() {
    miHost = "";
  }

  // realizar una notificacion urgente
  public void notificacionUrgente(CApp app,
                                  String NM_EDO,
                                  String CD_NIVEL_1,
                                  String CD_ENOTIF,
                                  String DS_ENOTIF,
                                  String CD_ENFCIE,
                                  String DS_ENFCIE,
                                  String DS_EMAIL) {

    //Para obtener datos de direcciones de correo
    CLista param;
    DataNivel1 nivel1 = null;
    //Para enviar los correos
    CMail cmail1 = null;
    CMail cmail2 = null;
    CMail cmail3 = null;
    String sTitulo = "Alta de un caso EDO urgente";
    String sCuerpo = "Enfermedad " + CD_ENFCIE + " " + DS_ENFCIE +
        " Caso n�: " + NM_EDO + " Equipo " + CD_ENOTIF + " " + DS_ENOTIF;

    //Se buscan los datos de correo de ese area
    param = new CLista();
    param.addElement(new DataNivel1(CD_NIVEL_1));

    try {

      stubCliente.setUrl(new URL(app.getURL() + strSERVLETNivel1));
      param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO, param);

      // rellena los datos
      if (param.size() > 0) {
        nivel1 = (DataNivel1) param.firstElement();

        if (miHost.equals("")) {
          cmail1 = new CMail();
        }
        else {
          cmail1 = new CMail(miHost);
        }
        cmail1.setTo(nivel1.getEMail1());
        cmail1.setFrom(DS_EMAIL);
        cmail1.setSubject(sTitulo);
        cmail1.setBody(sCuerpo);
        cmail1.sendMail();

        if (miHost.equals("")) {
          cmail2 = new CMail();
        }
        else {
          cmail2 = new CMail(miHost);
        }
        cmail2.setTo(nivel1.getEMail2());
        cmail2.setFrom(DS_EMAIL);
        cmail2.setSubject(sTitulo);
        cmail2.setBody(sCuerpo);
        cmail2.sendMail();

        if (miHost.equals("")) {
          cmail3 = new CMail();
        }
        else {
          cmail3 = new CMail(miHost);
        }
        cmail3.setTo(nivel1.getEMail3());
        cmail3.setFrom(DS_EMAIL);
        cmail3.setSubject(sTitulo);
        cmail3.setBody(sCuerpo);
        cmail3.sendMail();

      }

    }
    catch (Exception ex) {

      System.out.println(ex.toString());
    }

  }

} //FIN CLASE