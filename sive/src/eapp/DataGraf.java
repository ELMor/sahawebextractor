package eapp;

import java.util.Vector;

import jclass.chart.Chartable;

public class DataGraf
    implements Chartable {

  protected Vector series = null;
  protected String title = null;
  protected String names[] = null;
  protected String pointlabels[] = null;

  public DataGraf(String tit_leyenda,
                  double rawData[][],
                  String nom_series[],
                  String nom_ejey[]) {
    title = tit_leyenda;
    names = nom_series;
    pointlabels = nom_ejey;

    // rellena los datos
    series = new Vector();
    for (int i = 0; i < rawData.length; i++) {
      Vector row = new Vector();
      for (int j = 0; j < rawData[i].length; j++) {
        row.addElement(new Double(rawData[i][j]));
      }
      series.addElement(row);
    }

  }

  public int getDataInterpretation() {
    return Chartable.ARRAY;
  }

  // devuelve un elemento de la colecci�n de datos
  public Object getDataItem(int row, int column) {
    Object rval = null;
    try {
      rval = ( (Vector) series.elementAt(row)).elementAt(column);
    }
    catch (Exception e) {
    }
    return rval;
  }

  // devuelve una serie completa
  public Vector getRow(int row) {
    Vector rval = null;
    try {
      rval = (Vector) series.elementAt(row);
    }
    catch (Exception e) {
    }
    return rval;
  }

  // devuelve el n�mero de series
  public int getNumRows() {
    if (series == null) {
      return 0;
    }
    return series.size();
  }

  public String[] getPointLabels() {
    return pointlabels;
  }

  public String getSeriesName(int row) {
    return names[row];
  }

  public String getSeriesLabel(int row) {
    return names[row];
  }

  public String getName() {
    return title;
  }

}
