package eapp;

import capp.CApp;

public class prueba
    extends CApp {

  public prueba() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    // datos de prueba
    double rawData[][] = {
        {
        1, 2, 3}
        , {
        40, -10, 20}
    };

    String series[] = {
        "area 1", "area 2", "area 3"};
    String series2[] = {
        "porcentaje"};

    DataGraf data = new DataGraf("Leyenda", rawData, series2, series);

    GPanel panel = new GPanel(this, PanelChart.BARRAS_INVERTIDAS);
    panel.chart.setDatos(data);
    panel.chart.setCriterios("c1", "c2", "c3");
    panel.chart.setTitulo("titulo");
    panel.chart.setTituloEje("�reas", "%");

    panel.show();
  }
}