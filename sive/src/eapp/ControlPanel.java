package eapp;

import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonBar;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.dx.text.Alignment;

public class ControlPanel
    extends Panel {
  BorderLayout borderLayout = new BorderLayout();
  ResourceBundle res;
  ButtonControl btnSalir = new ButtonControl();
  ButtonBar buttonBar = new ButtonBar();
  StatusBar statusBar1 = new StatusBar();
  StatusBar statusBar2 = new StatusBar();
  Panel pnl = new Panel();

  protected EPanel epnl;

  public ControlPanel(EPanel p) {
    try {
      epnl = p;
      res = ResourceBundle.getBundle("eapp.Res" + epnl.getCApp().getIdioma());
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(650, 26));
    btnSalir.setLabel(res.getString("btnSalir.Label"));
    btnSalir.addActionListener(new ControlPanel_btnSalir_actionAdapter(this));

    // status bar
    statusBar1.setBevelInner(BevelPanel.LOWERED);
    statusBar1.setBevelOuter(BevelPanel.LOWERED);
    statusBar1.setMargins(new Insets(0, 0, 0, 0));
    statusBar2.setBevelInner(BevelPanel.LOWERED);
    statusBar2.setBevelOuter(BevelPanel.LOWERED);
    statusBar2.setMargins(new Insets(0, 0, 0, 0));
    pnl.setLayout(new BorderLayout());
    pnl.add(statusBar1, BorderLayout.WEST);
    pnl.add(statusBar2, BorderLayout.CENTER);
    pnl.add(btnSalir, BorderLayout.EAST);
    Image img = epnl.getCApp().getImage(epnl.getCApp().getCodeBase(),
                                        "images/salir.gif");
    btnSalir.setImage(img);
    // botonera
    buttonBar.setBevelOuter(BevelPanel.LOWERED);
    buttonBar.setBevelInner(BevelPanel.LOWERED);
    buttonBar.setButtonAlignment(Alignment.LEFT |
                                 com.borland.dx.text.Alignment.MIDDLE);
    buttonBar.setButtonType(ButtonBar.IMAGE_ONLY);
    buttonBar.setHgap(0);
    buttonBar.setImageBase(epnl.getCApp().getCodeBase().toString());
    buttonBar.setVgap(0);
    buttonBar.setImageBase(epnl.getCApp().getCodeBase().toString());
    buttonBar.setImageNames(new String[] {"images/completo.gif",
                            "images/masdatos.gif", "images/refrescar.gif"});
    buttonBar.setLabels(new String[] {res.getString("buttonBar.Labels"),
                        res.getString("buttonBar.Labels1"),
                        res.getString("buttonBar.Labels2")});

    // posici�n de los objectos
    this.setLayout(borderLayout);
    this.add(buttonBar, BorderLayout.WEST);
    this.add(pnl, BorderLayout.CENTER);

    // gestores de eventos
    buttonBar.addActionListener(new buttonBar_actionAdapter(this));
  }

  void buttonBar_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("masdatos")) {
      epnl.MasDatos();
    }
    else if (e.getActionCommand().equals("refrescar")) {
      epnl.GenerarInforme();
    }
    else if (e.getActionCommand().equals("completo")) {
      epnl.InformeCompleto();
    }
  }

  public void setStatus1(String s) {
    statusBar1.setText(s);
  }

  public void setStatus2(String s) {
    statusBar2.setText(s);
  }

  void btnSalir_actionPerformed(ActionEvent e) {
    epnl.dispose();
  }
}

class buttonBar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  ControlPanel adaptee;
  ActionEvent evt;

  buttonBar_actionAdapter(ControlPanel adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    Thread th = new Thread(this);
    this.evt = e;
    th.start();
  }

  public void run() {
    adaptee.buttonBar_actionPerformed(evt);
  }
}

class ControlPanel_btnSalir_actionAdapter
    implements java.awt.event.ActionListener {
  ControlPanel adaptee;

  ControlPanel_btnSalir_actionAdapter(ControlPanel adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnSalir_actionPerformed(e);
  }
}
