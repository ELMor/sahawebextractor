package mantdatos;

import java.net.URL;

import java.awt.Image;

import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CImage;

//import javax.swing.UIManager;
public class mantdatos
    extends CApp {
  XYLayout xYLayout1 = new XYLayout();
  boolean isStandalone = false;
  public Pan_MantDatos tPan_Param;
  public Image img = null;

//Get a parameter value

  public String getParameter(String key, String def) {
    return isStandalone ? System.getProperty(key, def) :
        (getParameter(key) != null ? getParameter(key) : def);
  }

//Initialize the applet

  public void init() {
    URL url = null;

    try {
      url = new URL(getCodeBase(), "images/Magnify.gif");
      img = (new CImage(url, 20, 20)).getImage();
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //static {
  //  try {
  //    //UIManager.setLookAndFeel(new javax.swing.plaf.metal.MetalLookAndFeel());
  //    //UIManager.setLookAndFeel(new com.sun.java.swing.plaf.motif.MotifLookAndFeel());
  //    UIManager.setLookAndFeel(new com.sun.java.swing.plaf.windows.WindowsLookAndFeel());
  //  }
  //  catch (Exception e) {}
  //}
//Component initialization

  private void jbInit() throws Exception {
    super.init();
    /*xYLayout1.setWidth(400);
         xYLayout1.setHeight(300);
         this.setLayout(xYLayout1);*/

  }

//Start the applet

  public void start() {
    setTitulo("Datos del Servicio de Epidemiología");
    CApp a = (CApp)this;
    tPan_Param = new Pan_MantDatos(a);
    tPan_Param.Inicializar();
    VerPanel("", tPan_Param);

  }

//Stop the applet

  public void stop() {
  }

//Destroy the applet

  public void destroy() {
  }

//Get Applet information

  public String getAppletInfo() {
    return "Applet Information";
  }

//Get parameter info

  public String[][] getParameterInfo() {
    return null;
  }
}
