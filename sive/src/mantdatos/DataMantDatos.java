package mantdatos;

import java.io.Serializable;

public class DataMantDatos
    implements Serializable {
  protected String sCD_CA = "";
  protected String sDS_CONSEJ = "";
  protected String sDS_DIRECG = "";
  protected String sDS_SERV = "";
  protected String sDS_DIREC = "";
  protected String sCD_POSTAL = "";
  protected String sDS_POB = "";
  protected String sDS_JEFESERV = "";
  protected String sDS_TELEF = "";
  protected String sDS_FAX = "";
  protected String sDS_EMAIL = "";

  public DataMantDatos(String codCA, String dsConsej, String dsDirecG,
                       String dsServ,
                       String dsDireccion, String CP, String dsPoblacion,
                       String dsJefe,
                       String telefono, String fax, String email) {

    sCD_CA = codCA;
    sDS_CONSEJ = dsConsej;
    sDS_DIRECG = dsDirecG;
    sDS_SERV = dsServ;
    sDS_DIREC = dsDireccion;
    sCD_POSTAL = CP;
    sDS_POB = dsPoblacion;
    sDS_JEFESERV = dsJefe;
    sDS_TELEF = telefono;
    sDS_FAX = fax;
    sDS_EMAIL = email;
  }

  public String getCCAA() {
    return sCD_CA;
  }

  public String getCodConsejero() {
    return sDS_CONSEJ;
  }

  public String getDirGeneral() {
    return sDS_DIRECG;
  }

  public String getServicio() {
    return sDS_SERV;
  }

  public String getDireccion() {
    return sDS_DIREC;
  }

  public String getCP() {
    return sCD_POSTAL;
  }

  public String getPoblacion() {
    return sDS_POB;
  }

  public String getJefeServ() {
    return sDS_JEFESERV;
  }

  public String getTelefono() {
    return sDS_TELEF;
  }

  public String getFax() {
    return sDS_FAX;
  }

  public String getEMail() {
    return sDS_EMAIL;
  }

}
