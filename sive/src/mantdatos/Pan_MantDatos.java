package mantdatos;

import java.net.URL;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import sapp.StubSrvBD;

public class Pan_MantDatos
    extends CPanel {

  int idioma;

  //Modos de operaci�n de la ventana
  final int modoALTA = 0;
  final int modoMODIFICAR = 1;
  final int modoESPERA = 2;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //Constantes del panel
  final String imgLUPA = "images/Magnify.gif";
  final String imgACEPTAR = "images/aceptar.gif";
  final String strSERVLETParam = "servlet/SrvMantDatos";
  final String strSERVLETCat = "servlet/SrvCat";

  //Par�metros
  protected int modo = modoALTA;
  protected int modoAnterior; //Para saber si tenemos que volver al modo ALTA o MODIFICAR tras la selecci�n de un valor
  protected StubSrvBD stubClienteParam = null;
  protected DataMantDatos dataParamAuxi = null;

  protected StubSrvBD stubClienteCat = null;
  protected CLista listaResultadoCat = null;
  protected CLista listaResultadoParam = null;

  final int minLongitud = 1;
  final int maxLongitud = 10;
  final int minEnteros = 1;
  final int maxEnteros = 10;
  final int minDecimales = 1;
  final int maxDecimales = 10;
  int Longitud;
  int Enteros;
  int Decimales;

  XYLayout xYLayout1 = new XYLayout();
  boolean isStandalone = false;
  Label lblPregunta = new Label();
  Label lblDescPregunta = new Label();
  TextField txtDirGeneral = new TextField();
  ButtonControl btnAnadir = new ButtonControl();
  TextField txtConsejeria = new TextField();

  Label label1 = new Label();
  TextField txtServicio = new TextField();
  Label label2 = new Label();
  TextArea txtDireccion = new TextArea("", 0, 0,
                                       java.awt.TextArea.SCROLLBARS_VERTICAL_ONLY);
  TextField txtCP = new TextField();
  Label label3 = new Label();
  Label label4 = new Label();
  TextField txtMunicipio = new TextField();
  TextArea txtTelefono = new TextArea("", 0, 0,
                                      java.awt.TextArea.SCROLLBARS_VERTICAL_ONLY);
  Label lblNivel3 = new Label();
  Label label5 = new Label();
  TextField txtJefe = new TextField();
  TextField txtEMail = new TextField();
  Label label6 = new Label();
  Label label7 = new Label();
  TextField txtFax = new TextField();

  public Pan_MantDatos(CApp a) {
    URL u;
    CLista data = new CLista();
    CLista dataAuxi = new CLista();
    DataCat dataCatAuxi = null;

    try {
      setApp(a);
      idioma = app.getIdioma();
      jbInit();
      u = app.getCodeBase();
      stubClienteParam = new StubSrvBD(new URL(app.getURL() + strSERVLETParam));
      CLista listaResultadoParam = new CLista(); // Va a ser una CList (hereda de Vector) en la que cada elem. tiene dos Strings
      listaResultadoParam = new CLista();
      listaResultadoParam.setState(CLista.listaNOVALIDA);

      stubClienteCat = new StubSrvBD(new URL(app.getURL() + strSERVLETCat));
      CLista listaResultadoCat = new CLista(); // Va a ser una CList (hereda de Vector) en la que cada elem. tiene dos Strings
      listaResultadoCat.setState(CLista.listaNOVALIDA);

      data.setLogin(this.app.getLogin());
      // Primero se lee el registro la tabla
      data.addElement(new DataMantDatos("", "", "", "", "", "", "", "", "", "",
                                        ""));
      dataAuxi = (CLista) stubClienteParam.doPost(servletOBTENER_X_CODIGO, data);
      // Ahora escribimos los valores de los componentes
      dataParamAuxi = (DataMantDatos) dataAuxi.firstElement();
      // Comprobamos que no est� vac�a
      if (dataParamAuxi.getDireccion().compareTo("") != 0) {
        /*
                // Primero leemos el c�digo de la CCAA
                txtConsejeria.setText(dataParamAuxi.getCCAA());
         */
        // Primero leemos el c�digo de la Consejeria
        txtConsejeria.setText(dataParamAuxi.getCodConsejero());
        // Y ahora el resto de los datos...
        txtDirGeneral.setText(dataParamAuxi.getDirGeneral());
        txtServicio.setText(dataParamAuxi.getServicio());
        txtDireccion.setText(dataParamAuxi.getDireccion());
        txtCP.setText(dataParamAuxi.getCP());
        txtMunicipio.setText(dataParamAuxi.getPoblacion());
        txtJefe.setText(dataParamAuxi.getJefeServ());
        txtTelefono.setText(dataParamAuxi.getTelefono());
        txtFax.setText(dataParamAuxi.getFax());
        txtEMail.setText(dataParamAuxi.getEMail());
      }
      modo = modoALTA;
      Inicializar();
      txtConsejeria.requestFocus();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void jbInit() throws Exception {

    xYLayout1.setHeight(416);
    xYLayout1.setWidth(559);
    lblPregunta.setText("Consejeria:");
    lblDescPregunta.setText("Direccion General:");
    btnAnadir.setEnabled(false);
    btnAnadir.setLabel("Aceptar");
    //btnCtrlCodPregunta.addActionListener(new Pan_Param_btnCtrlCodPregunta_actionAdapter(this));
    label1.setText("Servicio:");
    label2.setText("Direccion:");
    txtDireccion.setBackground(new Color(255, 255, 150));
    label3.setText("Codigo Postal:");
    label4.setText("Municipio:");
    txtMunicipio.setBackground(new Color(255, 255, 150));

    lblNivel3.setText("Jefe del Servicio:");
    txtJefe.setBackground(new Color(255, 255, 150));
    label5.setText("Telefono:");
    label6.setText("E-Mail:");
    label7.setText("FAX:");
    btnAnadir.addActionListener(new Pan_Param_btnAnadir_actionAdapter(this));
//    btnModificar.setLabel("Modificar");
    this.setLayout(xYLayout1);

    //A�adimos las list aqu� paa que se vean bien y superpongan a
    //otros componentes cuando sea necesario*************

    this.add(lblPregunta, new XYConstraints(24, 30, 128, -1));
    this.add(lblDescPregunta, new XYConstraints(24, 57, 108, -1));
    this.add(txtConsejeria, new XYConstraints(201, 30, 328, -1));
    this.add(txtDirGeneral, new XYConstraints(201, 58, 328, -1));
    this.add(label1, new XYConstraints(24, 84, 95, -1));
    this.add(txtServicio, new XYConstraints(201, 84, 328, -1));
    this.add(label2, new XYConstraints(24, 111, 171, -1));
    this.add(txtDireccion, new XYConstraints(201, 111, 327, 46));
    this.add(txtCP, new XYConstraints(201, 162, 78, -1));
    this.add(label3, new XYConstraints(24, 162, 95, -1));
    this.add(label4, new XYConstraints(24, 189, 171, -1));
    this.add(txtMunicipio, new XYConstraints(201, 189, 328, -1));
    this.add(txtJefe, new XYConstraints(201, 216, 328, -1));
    this.add(txtTelefono, new XYConstraints(201, 243, 139, 55));
    this.add(txtFax, new XYConstraints(393, 243, 136, -1));
    this.add(lblNivel3, new XYConstraints(24, 216, -1, -1));
    this.add(label5, new XYConstraints(24, 243, 171, -1));
    this.add(txtEMail, new XYConstraints(201, 302, 192, -1));
    this.add(label6, new XYConstraints(24, 302, 171, -1));
    this.add(label7, new XYConstraints(353, 243, 33, -1));
    this.add(btnAnadir, new XYConstraints(448, 302, -1, -1));

    //btnCtrlCodPregunta.addActionListener(new Pan_Param_btnCtrlCodPregunta_actionAdapter(this));
    btnAnadir.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
    //btnAnadir.addActionListener(btnActionListener);
    txtConsejeria.setForeground(Color.black);
    txtConsejeria.setBackground(new Color(255, 255, 150));
    txtServicio.setForeground(Color.black);
    txtServicio.setBackground(new Color(255, 255, 150));
    txtCP.setForeground(Color.black);
    txtCP.setBackground(new Color(255, 255, 150));
    txtDirGeneral.setForeground(Color.black);
    txtDirGeneral.setBackground(new Color(255, 255, 150));

    Inicializar();

  }

  public void Inicializar() {

    switch (modo) {
      case modoALTA:

        // modo alta

        txtConsejeria.setEnabled(true);
        //txtDescCA.setEnabled(false);
        txtDirGeneral.setEnabled(true);
        btnAnadir.setEnabled(true);
        //txtCodPregunta.setVisible(true);
        txtServicio.setEnabled(true);
        txtDireccion.setEnabled(true);
        txtCP.setEnabled(true);
        txtMunicipio.setEnabled(true);
        txtJefe.setEnabled(true);
        txtTelefono.setEnabled(true);
        txtEMail.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        txtConsejeria.setEnabled(true);
        //txtDescCA.setEnabled(false);
        txtDirGeneral.setEnabled(true);
        btnAnadir.setEnabled(false);
        //txtCodPregunta.setVisible(true);
        txtJefe.setEnabled(true);
        txtTelefono.setEnabled(true);
        txtEMail.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        /*case modoSELECCIONCodPregunta:
          // modo selecci�n lista de Param
          txtCodPregunta.setEnabled(false);
          btnCtrlCodPregunta.setEnabled(false);
          txtDescPregunta.setEnabled(false);
          btnAnadir.setEnabled(false);
          //txtCodPregunta.setVisible(false);
          txtNivel1.setEnabled(false);
          txtNivel1L.setEnabled(false);
          txtNivel2.setEnabled(false);
          txtNivel2L.setEnabled(false);
          tramero.setEnabled(false);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          this.doLayout();        //Repintamos el CPanel
          break;
         */

        /*case modoSELECCION:
          // modo selecci�n lista de valores
          txtCodPregunta.setEnabled(false);
          btnCtrlCodPregunta.setEnabled(false);
          txtDescPregunta.setEnabled(false);
          btnAnadir.setEnabled(false);
          txtNivel1.setEnabled(false);
          txtNivel1L.setEnabled(false);
          txtNivel2.setEnabled(false);
          txtNivel2L.setEnabled(false);
          tramero.setEnabled(false);
          //txtCodPregunta.setVisible(true);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          this.doLayout();        //Repintamos el CPanel
          break;
      */
      case modoESPERA:

        // modo espera
        txtConsejeria.setEnabled(false);
        //txtDescCA.setEnabled(false);
        txtDirGeneral.setEnabled(false);
        btnAnadir.setEnabled(false);
        //txtCodPregunta.setVisible(false);
        txtServicio.setEnabled(false);
        txtDireccion.setEnabled(false);
        txtCP.setEnabled(false);
        txtMunicipio.setEnabled(false);
        txtJefe.setEnabled(false);
        txtTelefono.setEnabled(false);
        txtEMail.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

  }

  void btnCtrlCodPregunta_actionPerformed(ActionEvent e) {
    DataCat data;

    modoAnterior = modo;
    modo = modoESPERA;
    Inicializar();

    CListaCat lista = new CListaCat(app,
                                    "Selecci�n de la Comunidad Aut�noma",
                                    stubClienteCat,
                                    strSERVLETCat,
                                    servletOBTENER_X_CODIGO +
                                    catalogo.Catalogo.catCCAA,
                                    servletOBTENER_X_DESCRIPCION +
                                    catalogo.Catalogo.catCCAA,
                                    servletSELECCION_X_CODIGO +
                                    catalogo.Catalogo.catCCAA,
                                    servletSELECCION_X_DESCRIPCION +
                                    catalogo.Catalogo.catCCAA);
    lista.show();
    data = (DataCat) lista.getComponente();
    if (data != null) {
      txtConsejeria.setText(data.getCod());

    }
    modo = modoAnterior;
    Inicializar();

  }

  void btnAnadir_actionPerformed(ActionEvent e) {
    CLista data = null;
    CMessage dialogo = new CMessage(this.app, 2,
                                    "�Desea actualizar el registro?");
    CMessage msgBox;

    dialogo.setSize(250, 150);
    dialogo.show();
    if (dialogo.getResponse() == true) {
      // Primero se debe comprobar si todos los datos son v�lidos
      if (this.isDataValid()) {
        try {
          this.modo = modoESPERA;
          Inicializar();
          data = new CLista();
          data.setLogin(this.app.getLogin());
          data.addElement(new DataMantDatos(this.app.getCA(),
                                            txtConsejeria.getText(),
                                            txtDirGeneral.getText(),
                                            txtServicio.getText(),
                                            txtDireccion.getText(),
                                            txtCP.getText(),
                                            txtMunicipio.getText(),
                                            txtJefe.getText(),
                                            txtTelefono.getText(),
                                            txtFax.getText(), txtEMail.getText()));
          stubClienteParam.doPost(modoALTA, data);

          this.modo = modoALTA;

        }
        catch (Exception ex) {
          this.modo = modoALTA;
          ex.printStackTrace();
          msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
          msgBox.show();
          msgBox = null;
        } //catch
        Inicializar();
      } //if is DataValid
    } //if SI

    this.doLayout();
  }

  /*  void btnBorrar_actionPerformed(ActionEvent e) {
      CMessage msgBox=null;
      CLista data=null;
      msgBox = new CMessage(app, CMessage.msgPREGUNTA, "�Est� seguro de que desea borrar la enfermedad?");
      msgBox.show();
      if (msgBox.getResponse()) {
        try {
          this.modo=modoESPERA;
          Inicializar();
          data=new CLista();
          data.addElement(new DataParam("","","","","","","") );
          stubClienteParam.doPost(modoBAJAselect * from sive_e_notifSystem_out.println("S� ,data);
          this.modo = modoALTA;
          vaciarTextoPantalla();
        }
        catch (Exception ex){
          ex.printStackTrace();
          msgBox=new CMessage(app,CMessage.msgERROR,ex.getMessage() );
          msgBox.show();
          msgBox=null;
          modo=modoMODIFICAR;
        }//catch
       Inicializar();
      }
      else {
        msgBox = null;
      } //else
    }
   */

  void vaciarTextoPantalla() {
    txtConsejeria.setText("");
    txtDirGeneral.setText("");

  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;
    if ( (
        (txtConsejeria.getText().length() > 0) &&
        (txtDirGeneral.getText().length() > 0)
        && (txtServicio.getText().length() > 0) &&
        (txtDireccion.getText().length() > 0)
        && (txtCP.getText().length() > 0) &&
        (txtMunicipio.getText().length() > 0)
        && (txtJefe.getText().length() > 0))) {
      b = true;
      if (txtConsejeria.getText().length() > 40) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtConsejeria.selectAll();
      }
      if (txtDirGeneral.getText().length() > 40) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtDirGeneral.selectAll();
      }
      if (txtEMail.getText().length() > 30) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtEMail.selectAll();
      }
      if (txtServicio.getText().length() > 40) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtServicio.selectAll();
      }
      if (txtDireccion.getText().length() > 255) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtDireccion.selectAll();
      }
      if (txtCP.getText().length() > 5) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtCP.selectAll();
      }
      if (txtMunicipio.getText().length() > 30) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtMunicipio.selectAll();
      }
      if (txtJefe.getText().length() > 40) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtJefe.selectAll();
      }
      if (txtTelefono.getText().length() > 14) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtTelefono.selectAll();
      }
      if (txtServicio.getText().length() > 40) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtServicio.selectAll();
      }
      if (txtFax.getText().length() > 9) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtFax.selectAll();
      }
    }
    else {
      msg = "Faltan campos obligatorios.";
    }
    if (!b) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, msg);
      msgBox.show();
      msgBox = null;
    }
    return b;
  }

  void verMensajeAviso(String mensaje) {
    CMessage msgBox;
    msgBox = new CMessage(app, CMessage.msgAVISO, mensaje);
    msgBox.show();
    msgBox = null;
  }

  void txtConsejeria_keyPressed(KeyEvent e) {

  }

} //fin de clase

/**************************Trat de eventos*************************/

// action listener de evento en bot�nes o en txtCodPregunta

class Pan_Param_btnCtrlCodPregunta_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_MantDatos adaptee;
  ActionEvent e;

  Pan_Param_btnCtrlCodPregunta_actionAdapter(Pan_MantDatos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    //adaptee.btnCtrlCodPregunta_actionPerformed(e);
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnCtrlCodPregunta_actionPerformed(e);
  }
}

class Pan_Param_btnAnadir_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_MantDatos adaptee;
  ActionEvent e;

  Pan_Param_btnAnadir_actionAdapter(Pan_MantDatos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    //adaptee.btnAnadir_actionPerformed(e);
    this.e = e;
    new Thread(this).start();

  }

  public void run() {
    adaptee.btnAnadir_actionPerformed(e);
  }
}

// lista de valores
class CListaCat
    extends CListaValores {

  public CListaCat(CApp a,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataCat(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}
