package bidata;

import java.util.Hashtable;

//Limite: 49 si, 50 NO
//Estructura para sive_alerta_brotes, sive_alerta_adic, sive_alerta_colec
public class DataAlerta
    extends Hashtable {

//CONSTRUCTORES
  public DataAlerta() {
    super();
  }

//Si es un nulo, no hara NADA, y al hacer el get tendre null
//Si no es nulo, hara un put y al hacer el get obtendre el dato
  public synchronized void insert(Object key, Object value) {
    if ( (key != null) && (value != null)) {
      put(key, value);
    }
  }

  public String getCD_ANO() {
    return (String) get("CD_ANO");
  }

  public String getNM_ALERBRO() {
    return (String) get("NM_ALERBRO");
  }

  public String getFC_FECHAHORA_F() {
    return (String) get("FC_FECHAHORA_F");
  }

  public String getFC_FECHAHORA_H() {
    return (String) get("FC_FECHAHORA_H");
  }

  public String getCD_NIVEL_1_EX() {
    return (String) get("CD_NIVEL_1_EX");
  }

  public String getDS_NIVEL_1_EX() {
    return (String) get("DS_NIVEL_1_EX");
  }

  public String getCD_NIVEL_1() {
    return (String) get("CD_NIVEL_1");
  }

  public String getDS_NIVEL_1() {
    return (String) get("DS_NIVEL_1");
  }

  public String getDS_ALERTA() {
    return (String) get("DS_ALERTA");
  }

  public String getCD_GRUPO() {
    return (String) get("CD_GRUPO");
  }

  public String getCD_SITALERBRO() {
    return (String) get("CD_SITALERBRO");
  }

  public String getFC_ALERBRO() {
    return (String) get("FC_ALERBRO");
  }

  public String getCD_OPE() {
    return (String) get("CD_OPE");
  }

  public String getFC_ULTACT() {
    return (String) get("FC_ULTACT");
  }

  public String getCD_LEXCA() {
    return (String) get("CD_LEXCA");
  } //NO ESTA EN LA TABLA

  public String getCD_LEXPROV() {
    return (String) get("CD_LEXPROV");
  }

  public String getCD_LEXMUN() {
    return (String) get("CD_LEXMUN");
  }

  public String getDS_LEXMUN() {
    return (String) get("DS_LEXMUN");
  }

  public String getCD_NIVEL_2_EX() {
    return (String) get("CD_NIVEL_2_EX");
  }

  public String getDS_NIVEL_2_EX() {
    return (String) get("DS_NIVEL_2_EX");
  }

  public String getCD_ZBS_EX() {
    return (String) get("CD_ZBS_EX");
  }

  public String getDS_ZBS_EX() {
    return (String) get("DS_ZBS_EX");
  }

  public String getCD_NIVEL_2() {
    return (String) get("CD_NIVEL_2");
  }

  public String getDS_NIVEL_2() {
    return (String) get("DS_NIVEL_2");
  }

  public String getCD_E_NOTIF() {
    return (String) get("CD_E_NOTIF");
  }

  public String getDS_E_NOTIF() {
    return (String) get("DS_E_NOTIF");
  }

  public String getCD_CENTRO() {
    return (String) get("CD_CENTRO");
  }

  public String getDS_CENTRO() {
    return (String) get("DS_CENTRO");
  }

  public String getNM_EXPUESTOS() {
    return (String) get("NM_EXPUESTOS");
  }

  public String getNM_ENFERMOS() {
    return (String) get("NM_ENFERMOS");
  }

  public String getNM_INGHOSP() {
    return (String) get("NM_INGHOSP");
  }

  public String getFC_EXPOSICION_F() {
    return (String) get("FC_EXPOSICION_F");
  }

  public String getFC_EXPOSICION_H() {
    return (String) get("FC_EXPOSICION_H");
  }

  public String getFC_INISINTOMAS_F() {
    return (String) get("FC_INISINTOMAS_F");
  }

  public String getFC_INISINTOMAS_H() {
    return (String) get("FC_INISINTOMAS_H");
  }

  public String getDS_LEXP() {
    return (String) get("DS_LEXP");
  }

  public String getDS_LEXPDIREC() {
    return (String) get("DS_LEXPDIREC");
  }

  public String getDS_LEXPPISO() {
    return (String) get("DS_LEXPPISO");
  }

  public String getCD_LEXPPOST() {
    return (String) get("CD_LEXPPOST");
  }

  public String getDS_LNMCALLE() {
    return (String) get("DS_LNMCALLE");
  }

  public String getDS_LEXPTELEF() {
    return (String) get("DS_LEXPTELEF");
  }

  public String getIT_VALIDADA() {
    return (String) get("IT_VALIDADA");
  }

  public String getFC_FECVALID() {
    return (String) get("FC_FECVALID");
  }

//adic
  public String getCD_TNOTIF() {
    return (String) get("CD_TNOTIF");
  }

  public String getDS_TNOTIF() {
    return (String) get("DS_TNOTIF");
  }

  public String getCD_MINCA() {
    return (String) get("CD_MINCA");
  } //NO ESTA EN LA TABLA

  public String getCD_MINPROV() {
    return (String) get("CD_MINPROV");
  }

  public String getCD_MINMUN() {
    return (String) get("CD_MINMUN");
  }

  public String getDS_MINMUN() {
    return (String) get("DS_MINMUN");
  }

  public String getDS_NOTIFINST() {
    return (String) get("DS_NOTIFINST");
  }

  public String getCD_NOTIFCA() {
    return (String) get("CD_NOTIFCA");
  } //NO ESTA EN LA TABLA

  public String getCD_NOTIFPROV() {
    return (String) get("CD_NOTIFPROV");
  }

  public String getCD_NOTIFMUN() {
    return (String) get("CD_NOTIFMUN");
  }

  public String getDS_NOTIFMUN() {
    return (String) get("DS_NOTIFMUN");
  }

  public String getDS_NOTIFICADOR() {
    return (String) get("DS_NOTIFICADOR");
  }

  public String getDS_NOTIFDIREC() {
    return (String) get("DS_NOTIFDIREC");
  }

  public String getCD_NOTIFPOSTAL() {
    return (String) get("CD_NOTIFPOSTAL");
  }

  public String getDS_NOTIFPISO() {
    return (String) get("DS_NOTIFPISO");
  }

  public String getDS_NNMCALLE() {
    return (String) get("DS_NNMCALLE");
  }

  public String getDS_NOTIFTELEF() {
    return (String) get("DS_NOTIFTELEF");
  }

  public String getDS_MINFPER() {
    return (String) get("DS_MINFPER");
  }

  public String getDS_MINFDIREC() {
    return (String) get("DS_MINFDIREC");
  }

  public String getDS_MINFPISO() {
    return (String) get("DS_MINFPISO");
  }

  public String getCD_MINFPOSTAL() {
    return (String) get("CD_MINFPOSTAL");
  }

  public String getDS_MINFTELEF() {
    return (String) get("DS_MINFTELEF");
  }

  public String getDS_MNMCALLE() {
    return (String) get("DS_MNMCALLE");
  }

  public String getDS_OBSERV() {
    return (String) get("DS_OBSERV");
  }

  public String getDS_ALISOSP() {
    return (String) get("DS_ALISOSP");
  }

  public String getDS_SINTOMAS() {
    return (String) get("DS_SINTOMAS");
  }

//colec
  public String getCD_CA() {
    return (String) get("CD_CA");
  } //NO ESTA EN LA TABLA

  public String getCD_PROV() {
    return (String) get("CD_PROV");
  }

  public String getCD_MUN() {
    return (String) get("CD_MUN");
  }

  public String getDS_MUN() {
    return (String) get("DS_MUN");
  }

  public String getDS_NOMCOL() {
    return (String) get("DS_NOMCOL");
  }

  public String getDS_DIRCOL() {
    return (String) get("DS_DIRCOL");
  }

  public String getDS_PISOCOL() {
    return (String) get("DS_PISOCOL");
  }

  public String getDS_NMCALLE() {
    return (String) get("DS_NMCALLE");
  }

  public String getCD_POSTALCOL() {
    return (String) get("CD_POSTALCOL");
  }

  public String getDS_TELCOL() {
    return (String) get("DS_TELCOL");
  }

//para hospitales
  public String getNM_ALERHOSP() {
    return (String) get("NM_ALERHOSP");
  }

  public String getDS_HOSPITAL() {
    return (String) get("DS_HOSPITAL");
  }

  public String getFC_INGHOSP() {
    return (String) get("FC_INGHOSP");
  }

//Cuidado porque en la tabla es DS_OBSERV pero ya hay otro
//esta hash
  public String getDS_OBSERV_HOSP() {
    return (String) get("DS_OBSERV_HOSP");
  }

} //FIN DE LA CLASE
