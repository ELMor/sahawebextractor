
package infcobper;

import java.io.Serializable;

public class ParamC11_12
    implements Serializable {
  public String sAnoI = "";
  public String sAnoF = "";
  public String sSemanaI = "";
  public String sSemanaF = "";
  public int iGrupo = 0;
  public String sGrupo = "";
  public String sEN = "";
  public String sENdesc = "";
  public String sCN = "";
  public String sCNdesc = "";
  public int iPagina = 0;
  public boolean bInformeCompleto = false;

  public ParamC11_12() {
    sAnoI = "";
    sAnoF = "";
    sSemanaI = "";
    sSemanaF = "";
    iGrupo = 0;
    sGrupo = "";
    sEN = "";
    sENdesc = "";
    sCN = "";
    sCNdesc = "";
    iPagina = 0;
  }

  public ParamC11_12(String aI, String aF, String sI, String sF,
                     int g, String gru, String eq, String cen) {
    sAnoI = aI;
    sAnoF = aF;
    sSemanaI = sI;
    sSemanaF = sF;
    iGrupo = 0;
    sGrupo = gru;
    sEN = eq;
    sCN = cen;
    iPagina = 0;
  }

}
