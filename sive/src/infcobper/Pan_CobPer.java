package infcobper;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import catalogo2.DataCat2;
import cn.DataCN;
import eqNot.DataEqNot;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import vCasIn.DataMun;
import zbs.DataZBS;
import zbs.DataZBS2;

public class Pan_CobPer
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();

  public ParamC11_12 paramC2;

  protected int modoOperacion = modoNORMAL;

  protected Pan_InfCobPer informe;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNotCen";
  final String strSERVLETCentro = "servlet/SrvCN";
  final String strSERVLETEnf = "servlet/SrvEnfVigi";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblCenNot = new Label();
  TextField txtCodCenNot = new CCampoCodigo();
  ButtonControl btnCtrlBuscarCenNot = new ButtonControl();
  TextField txtDesCenNot = new TextField();
  Label lblEquNot = new Label();
  TextField txtCodEquNot = new CCampoCodigo();
  ButtonControl btnCtrlBuscarEquNot = new ButtonControl();
  TextField txtDesEquNot = new TextField();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  txt_keyAdapter txtTextAdapter = new txt_keyAdapter(this);
  actionListener btnActionListener = new actionListener(this);
  Label lblAgrupacion = new Label();
  TextField txtAgrup = new TextField();

  public Pan_CobPer(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("infcobper.Res" + a.getIdioma());
      informe = new Pan_InfCobPer(a);
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      informe.setEnabled(false);
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(301);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));

    // gestores de eventos
    btnCtrlBuscarCenNot.addActionListener(btnActionListener);
    btnCtrlBuscarEquNot.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtAgrup.addKeyListener(txtTextAdapter);
    txtCodCenNot.addKeyListener(txtTextAdapter);
    txtCodEquNot.addKeyListener(txtTextAdapter);

    txtCodCenNot.addFocusListener(txtFocusAdapter);
    txtCodEquNot.addFocusListener(txtFocusAdapter);

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnCtrlBuscarCenNot.setActionCommand("buscarCenNot");
    txtDesCenNot.setEditable(false);
    txtDesCenNot.setEnabled(false); /*E*/
    btnCtrlBuscarEquNot.setActionCommand("buscarEquNot");
    txtDesEquNot.setEditable(false);
    txtDesEquNot.setEnabled(false); /*E*/
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtAgrup.setName("txtAgrup");
    txtCodCenNot.setName("txtCodCenNot");
    txtCodCenNot.setBackground(new Color(255, 255, 150));
    txtCodEquNot.setName("txtCodEquNot");

    lblAgrupacion.setText(res.getString("lblAgrupacion.Text"));
    lblHasta.setText(res.getString("lblHasta.Text"));
    lblCenNot.setText(res.getString("lblCenNot.Text"));
    lblEquNot.setText(res.getString("lblEquNot.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    txtAgrup.setBackground(new Color(255, 255, 150));
    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 42, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 42, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 80, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 80, -1, -1));
    this.add(lblAgrupacion, new XYConstraints(26, 120, 75, -1));
    this.add(txtAgrup, new XYConstraints(143, 120, 77, -1));
    this.add(lblCenNot, new XYConstraints(26, 160, 101, -1));
    this.add(txtCodCenNot, new XYConstraints(143, 160, 77, -1));
    this.add(btnCtrlBuscarCenNot, new XYConstraints(225, 160, -1, -1));
    this.add(txtDesCenNot, new XYConstraints(254, 160, 287, -1));
    this.add(lblEquNot, new XYConstraints(26, 200, 110, -1));
    this.add(txtCodEquNot, new XYConstraints(143, 200, 77, -1));
    this.add(btnCtrlBuscarEquNot, new XYConstraints(225, 200, -1, -1));
    this.add(txtDesEquNot, new XYConstraints(254, 200, 287, -1));
    this.add(btnLimpiar, new XYConstraints(389, 250, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 250, -1, -1));

    btnCtrlBuscarCenNot.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarEquNot.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    // Comprobacion de la agrupacion
    if (txtAgrup.getText().length() == 0) {
      bDatosCompletos = false;
    }
    else {
      try {
        Integer i = new Integer(txtAgrup.getText().trim());
      }
      catch (NumberFormatException e) {
        bDatosCompletos = false;
      }
    }

    // Comprobacion de la descripcion de la enfermedad
    if (txtDesCenNot.getText().length() == 0) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtAgrup.setEnabled(true);
        txtCodCenNot.setEnabled(true);
        //txtDesCenNot.setEnabled(true); /*E*/
        //txtDesEquNot.setEnabled(true);

        btnCtrlBuscarCenNot.setEnabled(true);
        // control eq
        if (!txtDesCenNot.getText().equals("")) {
          btnCtrlBuscarEquNot.setEnabled(true);
          txtCodEquNot.setEnabled(true);
          //txtDesEquNot.setEnabled(true); /*E*/
        }
        else {
          txtCodEquNot.setEnabled(false);
          btnCtrlBuscarEquNot.setEnabled(false);
          //txtDesEquNot.setEnabled(false); /*E*/
        }

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtAgrup.setEnabled(false);
        txtCodCenNot.setEnabled(false);
        txtCodEquNot.setEnabled(false);
        btnCtrlBuscarCenNot.setEnabled(false);
        btnCtrlBuscarEquNot.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  void btnCtrlbuscarCenNot_actionPerformed(ActionEvent evt) {
    DataCN data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaCN lista = new CListaCN(this.app,
                                    res.getString("msg2.Text"),
                                    stubCliente,
                                    strSERVLETCentro,
                                    servletOBTENER_X_CODIGO,
                                    servletOBTENER_X_DESCRIPCION,
                                    servletSELECCION_X_CODIGO,
                                    servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataCN) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodCenNot.removeKeyListener(txtTextAdapter);
      txtCodCenNot.setText(data.getCodCentro());
      txtDesCenNot.setText(data.getCentroDesc());
      txtCodEquNot.setText("");
      txtDesEquNot.setText("");
      txtCodCenNot.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();

  }

  void btnCtrlbuscarEquNot_actionPerformed(ActionEvent evt) {

    DataEqNot data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaEqNot lista = new CListaEqNot(this,
                                          res.getString("msg3.Text"),
                                          stubCliente,
                                          strSERVLETEquipo,
                                          servletOBTENER_X_CODIGO,
                                          servletOBTENER_X_DESCRIPCION,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataEqNot) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodEquNot.removeKeyListener(txtTextAdapter);
      txtCodEquNot.setText(data.getEquipo());
      txtDesEquNot.setText(data.getDesEquipo());
      txtCodEquNot.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtAgrup.setText("");
    txtCodEquNot.setText("");
    txtDesEquNot.setText("");
    txtCodCenNot.setText("");
    txtDesCenNot.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC2 = new ParamC11_12();

    boolean eq = false;
    boolean cen = false;
    boolean tasas = false;

    if (isDataValid()) {
      if (!txtAgrup.getText().equals("")) {
        Integer i = new Integer(txtAgrup.getText());
        paramC2.iGrupo = i.intValue();
      }
      if (!txtDesCenNot.getText().equals("")) {
        paramC2.sCN = txtCodCenNot.getText();
        paramC2.sCNdesc = txtDesCenNot.getText();
      }
      if (!txtDesEquNot.getText().equals("")) {
        paramC2.sEN = txtCodEquNot.getText();
        paramC2.sENdesc = txtDesEquNot.getText();
      }

      paramC2.sAnoI = fechasDesde.txtAno.getText();
      paramC2.sAnoF = fechasHasta.txtAno.getText();
      if (fechasDesde.txtCodSem.getText().length() == 1) {
        paramC2.sSemanaI = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        paramC2.sSemanaI = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        paramC2.sSemanaF = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        paramC2.sSemanaF = fechasHasta.txtCodSem.getText();

        // habilita el panel
      }
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.paramC1 = paramC2;
      boolean hay = informe.GenerarInforme();
      if (hay) {
        //#// System_out.println("SIIIIIIII  Muestro el informe");
        informe.show();
      }
      { //#// System_out.println("NOOOOOOOO Muestro el informe");
      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg4.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;
    DataEqNot eqnot;
    DataCN cnnot;
    DataCat2 prov;
    DataCat enf;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodEquNot")) &&
        (txtCodEquNot.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataEqNot(txtCodEquNot.getText(), "",
                                     txtCodCenNot.getText(), "", "", "", "", "",
                                     "", "",
                                     "", 0, "", "", false));
      strServlet = strSERVLETEquipo;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodCenNot")) &&
             (txtCodCenNot.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCN(txtCodCenNot.getText(), "", "", "", "", "",
                                  "",
                                  "", "", "", "", "", "", "", "", "", ""));
      strServlet = strSERVLETCentro;
      modoServlet = servletSELECCION_X_CODIGO;

    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodEquNot")) {
            eqnot = (DataEqNot) param.firstElement();
            txtCodEquNot.removeKeyListener(txtTextAdapter);
            txtCodEquNot.setText(eqnot.getEquipo());
            txtDesEquNot.setText(eqnot.getDesEquipo());
            txtCodEquNot.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodCenNot")) {
            cnnot = (DataCN) param.firstElement();
            txtCodCenNot.removeKeyListener(txtTextAdapter);
            txtCodCenNot.setText(cnnot.getCodCentro());
            txtDesCenNot.setText(cnnot.getCentroDesc());
            txtCodCenNot.addKeyListener(txtTextAdapter);
          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodEquNot")) &&
        (txtDesEquNot.getText().length() > 0)) {
      //txtCodEquNot.setText("");
      txtDesEquNot.setText("");
    }
    else if ( (txt.getName().equals("txtCodCenNot")) &&
             (txtDesCenNot.getText().length() > 0)) {
      //txtCodCenNot.setText("");
      txtCodEquNot.setText("");
      txtDesCenNot.setText("");
      txtDesEquNot.setText("");
    }

    Inicializar();
  }

} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  Pan_CobPer adaptee = null;
  ActionEvent e = null;

  public actionListener(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarCenNot")) {
      adaptee.btnCtrlbuscarCenNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarEquNot")) {
      adaptee.btnCtrlbuscarEquNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_CobPer adaptee;
  FocusEvent event;

  focusAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// cambio en una caja de codigos
class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_CobPer adaptee;

  txt_keyAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}

/*
 class textAdapter implements java.awt.event.TextListener {
  Pan_CobPer adaptee;
  textAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }
  public void textValueChanged(TextEvent e) {
    adaptee.textValueChanged(e);
  }
 }
 */
////////////////////// Clases para listas

// lista de valores
class CListaEqNot
    extends CListaValores {
  protected Pan_CobPer panel;

  public CListaEqNot(Pan_CobPer pnl,
                     String title,
                     StubSrvBD stub,
                     String servlet,
                     int obtener_x_codigo,
                     int obtener_x_descricpcion,
                     int seleccion_x_codigo,
                     int seleccion_x_descripcion) {
    super(pnl.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = pnl;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataEqNot(s, "", panel.txtCodCenNot.getText(), "", "", "", "",
                         "", "", "",
                         "", 0, this.app.getLogin(), "", false);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEqNot) o).getEquipo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEqNot) o).getDesEquipo());
  }
}

// lista de valores
class CListaCN
    extends CListaValores {

  public CListaCN(CApp a,
                  String title,
                  StubSrvBD stub,
                  String servlet,
                  int obtener_x_codigo,
                  int obtener_x_descricpcion,
                  int seleccion_x_codigo,
                  int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCN(s, "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                      "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataCN) o).getCodCentro());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCN) o).getCentroDesc());
  }
}
/*
 import java.awt.*;
 import capp.*;
 import sapp.*;
 import com.borland.jbcl.control.*;
 import com.borland.jbcl.layout.*;
 import java.net.*;
 import java.awt.event.*;
 import nivel1.*;
 import cn.*;
 import eqNot.*;
 import infcobsem.*;
 import java.util.*;
 public class Pan_CobPer extends CPanel {
  // im�genes
  final String imgLUPA= "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";
  // Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  final int modoESPERA = 1;
  protected int modoOperacion = modoNORMAL;
  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  // informes
  final int erwCASOS_EDO = 1;
  protected Pan_InfCobPer informe;
  // constantes del panel
  protected String centroActual = "";
  protected String equipoActual = "";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETPeriodo = "servlet/SrvInfCobPer";
  // stub's
  protected StubSrvBD stubEquipo = null;
  protected StubSrvBD stubPeriodo = null;
    // controles de la ventana
  GroupBox groupBox1 = new GroupBox();
  XYLayout xYLayout1 = new XYLayout();
  Label label1 = new Label();
  XYLayout xYLayout2 = new XYLayout();
  Label label2 = new Label();
  TextField txtAnyoDesde = new TextField();
  TextField txtSemanaDesde = new TextField();
  Label label3 = new Label();
  TextField txtCentro = new TextField();
  ButtonControl btnCentro = new ButtonControl();
  Label label4 = new Label();
  TextField txtCentroDesc = new TextField();
  TextField txtEquipo = new TextField();
  TextField txtEquipoDesc = new TextField();
  ButtonControl btnEquipo = new ButtonControl();
  Label label5 = new Label();
  TextField txtSemGrup = new TextField();
  Label label6 = new Label();
  Label label7 = new Label();
  TextField txtSemanaHasta = new TextField();
  TextField txtAnyoHasta = new TextField();
  Label label8 = new Label();
  Label label9 = new Label();
  Label label10 = new Label();
  ButtonControl btnObtener = new ButtonControl();
  ButtonControl btnLimpiar = new ButtonControl();
  public Pan_CobPer(CApp a, Pan_InfCobPer pi) {
    try  {
      setApp(a);
      informe = pi;
      jbInit();
      informe.setEnabled(false);
      stubEquipo= new StubSrvBD(new URL(this.app.getURL() + strSERVLETEquipo));
     stubPeriodo= new StubSrvBD(new URL(this.app.getURL() + strSERVLETPeriodo));
      equipoActual = "";
      centroActual = "";
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception {
    label2.setForeground(Color.black);
    label2.setFont(new Font("Dialog", 0, 12));
    label1.setForeground(Color.black);
    label1.setFont(new Font("Dialog", 0, 12));
    label1.setText("A�o:");
    label2.setText("Semana:");
    txtAnyoDesde.setBackground(new Color(255, 255, 150));
    txtAnyoDesde.setForeground(Color.black);
    txtAnyoDesde.setFont(new Font("Dialog", 0, 12));
    txtSemanaDesde.setBackground(new Color(255, 255, 150));
    txtSemanaDesde.setForeground(Color.black);
    txtSemanaDesde.setFont(new Font("Dialog", 0, 12));
    label3.setText("Centro:");
    txtCentro.setBackground(new Color(255, 255, 150));
    txtCentro.addKeyListener(new Pan_CobPer_txtCentro_keyAdapter(this));
    btnCentro.setLabel("");
    btnCentro.setImageURL(new URL(app.getCodeBase(),imgLUPA));
    btnCentro.addActionListener(new Pan_CobPer_btnCentro_actionAdapter(this));
    label4.setFont(new Font("Dialog", 1, 12));
    label4.setText("Agrupado por:");
    txtCentroDesc.setEditable(false);
    txtEquipo.addKeyListener(new Pan_CobPer_txtEquipo_keyAdapter(this));
    txtEquipoDesc.setEditable(false);
    btnEquipo.setLabel("");
    btnEquipo.setImageURL(new URL(app.getCodeBase(),imgLUPA));
    btnEquipo.addActionListener(new Pan_CobPer_btnEquipo_actionAdapter(this));
    label5.setText("Equipo:");
    txtSemGrup.setBackground(new Color(255, 255, 150));
    label6.setText("semanas");
    label7.setFont(new Font("Dialog", 1, 12));
    label7.setText("Desde:");
    txtSemanaHasta.setBackground(new Color(255, 255, 150));
    txtSemanaHasta.setForeground(Color.black);
    txtSemanaHasta.setFont(new Font("Dialog", 0, 12));
    txtAnyoHasta.setBackground(new Color(255, 255, 150));
    txtAnyoHasta.setForeground(Color.black);
    txtAnyoHasta.setFont(new Font("Dialog", 0, 12));
    label8.setForeground(Color.black);
    label8.setFont(new Font("Dialog", 0, 12));
    label8.setText("A�o:");
    label9.setForeground(Color.black);
    label9.setFont(new Font("Dialog", 0, 12));
    label9.setText("Semana:");
    label10.setText("Hasta:");
    btnObtener.setLabel("Obtener");
    btnObtener.setImageURL(new URL(app.getCodeBase(),this.imgGENERAR));
     btnObtener.addActionListener(new Pan_CobPer_btnObtener_actionAdapter(this));
    btnLimpiar.setLabel("Limpiar");
    btnLimpiar.setImageURL(new URL(app.getCodeBase(),this.imgLIMPIAR));
     btnLimpiar.addActionListener(new Pan_CobPer_btnLimpiar_actionAdapter(this));
    label10.setFont(new Font("Dialog", 1, 12));
    this.setLayout(xYLayout1);
    groupBox1.setForeground(new Color(155, 0, 0));
    groupBox1.setLayout(xYLayout2);
    groupBox1.setFont(new Font("Dialog", 3, 12));
    groupBox1.setLabel("Cobertura de una semana epidemiol�gica");
    this.add(label1, new XYConstraints(70, 37, -1, -1));
    this.add(label2, new XYConstraints(177, 37, -1, -1));
    this.add(txtAnyoDesde, new XYConstraints(112, 37, 56, -1));
    this.add(txtSemanaDesde, new XYConstraints(245, 37, 29, -1));
    this.add(label3, new XYConstraints(28, 104, 38, -1));
    this.add(txtCentro, new XYConstraints(70, 104, 56, -1));
    this.add(label4, new XYConstraints(28, 175, -1, -1));
    this.add(btnCentro, new XYConstraints(130, 104, -1, -1));
    this.add(txtCentroDesc, new XYConstraints(169, 104, 106, -1));
    this.add(txtEquipo, new XYConstraints(70, 136, 56, -1));
    this.add(txtEquipoDesc, new XYConstraints(169, 136, 106, -1));
    this.add(btnEquipo, new XYConstraints(130, 136, -1, -1));
    this.add(label5, new XYConstraints(28, 136, 38, -1));
    this.add(txtSemGrup, new XYConstraints(130, 175, 35, -1));
    this.add(label6, new XYConstraints(169, 175, -1, -1));
    this.add(label7, new XYConstraints(28, 37, 41, -1));
    this.add(txtSemanaHasta, new XYConstraints(245, 69, 29, -1));
    this.add(txtAnyoHasta, new XYConstraints(112, 69, 56, -1));
    this.add(label8, new XYConstraints(70, 69, -1, -1));
    this.add(label9, new XYConstraints(177, 69, -1, -1));
    this.add(label10, new XYConstraints(28, 69, 41, -1));
    this.add(btnObtener, new XYConstraints(193, 222, -1, -1));
    this.add(btnLimpiar, new XYConstraints(110, 222, -1, -1));
    this.add(groupBox1, new XYConstraints(8, 12, 385, 280));
  }
  public void Inicializar(){
    switch(modoOperacion){
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        btnCentro.setEnabled(true);
        if(centroActual.compareTo("") != 0)
          btnEquipo.setEnabled(true);
        else
          btnEquipo.setEnabled(false);
        btnLimpiar.setEnabled(true);
        btnObtener.setEnabled(true);
      break;
      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        btnCentro.setEnabled(false);
        btnEquipo.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnObtener.setEnabled(false);
      break;
    }
    this.doLayout();
  }
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;
    boolean b = false;
    // comprueba que esten informados los campos obligatorios
    if ((txtAnyoDesde.getText().length() > 0)) {
      b = true;
      if (txtAnyoDesde.getText().length() > 4){
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtAnyoDesde.selectAll();
      }
      if (txtSemanaDesde.getText().length() > 2){
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtSemanaDesde.selectAll();
      }
    // advertencia de requerir datos
    } else {
      msg = "Faltan campos obligatorios.";
    }
    if (!b){
      msgBox = new CMessage(app,CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    return b;
  }
  void btnCentro_actionPerformed(ActionEvent e) {
    DataCN datoCen = null;
    cn.Dialog1 dia1;
    int modo = modoOperacion;
    CMessage mensaje = null;
    btnCentro.setEnabled(false);
    modoOperacion = modoESPERA;
    Inicializar();
    try{
    dia1 = new cn.Dialog1(this.app);
    dia1.show();
    btnCentro.setEnabled(true);
    if (dia1.acepto){
     datoCen = (DataCN) dia1.listaCentro.elementAt(dia1.tabla.getSelectedIndex());
      txtCentro.setText(datoCen.getCodCentro());
      txtCentroDesc.setText(datoCen.getCentroDesc());
      centroActual = datoCen.getCodCentro();
      equipoActual = "";
    }
    }catch (Exception excepc){
      excepc.printStackTrace();
      mensaje=new CMessage(this.app,CMessage.msgERROR,excepc.getMessage());
      mensaje.show();
    }
      modoOperacion = modo;
      Inicializar();
  }
  void txtCentro_keyPressed(KeyEvent e) {
    centroActual = "";
    txtCentroDesc.setText("");
  }
  void btnEquipo_actionPerformed(ActionEvent e) {
    DataEqNot data;
    CMessage msgBox;
    int modo = modoOperacion;
    try{
    modoOperacion = modoESPERA;
    Inicializar();
    CListaEqNot lista = new CListaEqNot(  this,
                                          "Selecci�n de Equipos Notificadores",
                                          stubEquipo,
                                          strSERVLETEquipo,
                                          servletOBTENER_X_CODIGO,
                                          servletOBTENER_X_DESCRIPCION,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION);
    lista.show();
    data = (DataEqNot) lista.getComponente();
    modoOperacion = modo;
    if (data != null) {
      txtEquipo.setText(data.getEquipo());
      txtEquipoDesc.setText(data.getDesEquipo());
      equipoActual = data.getEquipo();
      modoOperacion = modo;
    }
  }catch (Exception er){
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modo;
  }
  Inicializar();
  }
  void txtEquipo_keyPressed(KeyEvent e) {
    equipoActual = "";
    txtEquipoDesc.setText("");
  }
  void btnObtener_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    informe.paramC1 = new ParamC11_12();
    if (isDataValid()) {
      if ((equipoActual.compareTo("") != 0)&&(equipoActual != null)){
        informe.paramC1.sEN = equipoActual;
      }else{
        informe.paramC1.sEN = "";
      }
      informe.paramC1.sAnoI = txtAnyoDesde.getText();
      informe.paramC1.sAnoF = txtAnyoHasta.getText();
      informe.paramC1.sSemanaI = txtSemanaDesde.getText();
      informe.paramC1.sSemanaF = txtSemanaHasta.getText();
      informe.paramC1.sCN = centroActual;
      Integer iAux = new Integer(txtSemGrup.getText());
      informe.paramC1.iGrupo = iAux.intValue();
      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.GenerarInforme();
      modoOperacion = modoNORMAL;
      Inicializar();
    } else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA, "Faltan campos obligatorios.");
      msgBox.show();
      msgBox = null;
    }
  }
  void btnLimpiar_actionPerformed(ActionEvent e) {
    modoOperacion = modoESPERA;
    Inicializar();
    txtAnyoDesde.setText("");
    txtAnyoHasta.setText("");
    txtCentro.setText("");
    txtCentroDesc.setText("");
    txtEquipo.setText("");
    txtEquipoDesc.setText("");
    txtSemanaDesde.setText("");
    txtSemanaHasta.setText("");
    txtSemGrup.setText("");
    centroActual = "";
    equipoActual = "";
    modoOperacion = modoNORMAL;
    Inicializar();
  }
 }
 class Pan_CobPer_btnCentro_actionAdapter implements java.awt.event.ActionListener, Runnable{
  Pan_CobPer adaptee;
  ActionEvent e;
  Pan_CobPer_btnCentro_actionAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }
  public void run(){
    adaptee.btnCentro_actionPerformed(e);
  }
 }
 class Pan_CobPer_txtCentro_keyAdapter extends java.awt.event.KeyAdapter {
  Pan_CobPer adaptee;
  Pan_CobPer_txtCentro_keyAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }
  public void keyPressed(KeyEvent e) {
    adaptee.txtCentro_keyPressed(e);
  }
 }
// lista de valores
 class CListaEqNot extends CListaValores {
  protected Pan_CobPer panel;
  public CListaEqNot( Pan_CobPer p,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super( p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    panel = p;
  }
  public Object setComponente(String s) {
    return new DataEqNot(s,"",panel.txtCentro.getText(),"","","","","","","",
    "",0,this.app.getLogin(),"",false);
  }
  public String getCodigo(Object o) {
    return ( ((DataEqNot)o).getEquipo() );
  }
  public String getDescripcion(Object o) {
    return ( ((DataEqNot)o).getDesEquipo() );
  }
 }
 class Pan_CobPer_btnEquipo_actionAdapter implements java.awt.event.ActionListener, Runnable{
  Pan_CobPer adaptee;
  ActionEvent e;
  Pan_CobPer_btnEquipo_actionAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }
  public void run(){
    adaptee.btnEquipo_actionPerformed(e);
  }
 }
 class Pan_CobPer_txtEquipo_keyAdapter extends java.awt.event.KeyAdapter {
  Pan_CobPer adaptee;
  Pan_CobPer_txtEquipo_keyAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }
  public void keyPressed(KeyEvent e) {
    adaptee.txtEquipo_keyPressed(e);
  }
 }
 class Pan_CobPer_btnObtener_actionAdapter implements java.awt.event.ActionListener, Runnable {
  Pan_CobPer adaptee;
  ActionEvent e;
  Pan_CobPer_btnObtener_actionAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }
  public void run(){
    adaptee.btnObtener_actionPerformed(e);
  }
 }
 class Pan_CobPer_btnLimpiar_actionAdapter implements java.awt.event.ActionListener, Runnable{
  Pan_CobPer adaptee;
  ActionEvent e;
  Pan_CobPer_btnLimpiar_actionAdapter(Pan_CobPer adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }
  public void run(){
    adaptee.btnLimpiar_actionPerformed(e);
  }
 }
 */
