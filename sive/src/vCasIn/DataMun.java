package vCasIn;

import java.io.Serializable;

public class DataMun
    implements Serializable {
  protected String sCD_PROV = "";
  protected String sCD_MUN = "";
  protected String sCD_NIVEL_1 = "";
  protected String sCD_NIVEL_2 = "";
  protected String sCD_ZBS = "";
  protected String sDS_MUN = "";

  public DataMun(String prov, String mun, String niv1, String niv2, String zbs,
                 String dsmun) {
    sCD_PROV = prov;
    sCD_MUN = mun;
    sCD_NIVEL_1 = niv1;
    sCD_NIVEL_2 = niv2;
    sCD_ZBS = zbs;
    sDS_MUN = dsmun;
  }

  public String getProvincia() {
    return sCD_PROV;
  }

  public String getMunicipio() {
    return sCD_MUN;
  }

  public String getNivel1() {
    return sCD_NIVEL_1;
  }

  public String getNivel2() {
    return sCD_NIVEL_2;
  }

  public String getZBS() {
    return sCD_ZBS;
  }

  public String getDescMun() {
    return sDS_MUN;
  }
}
