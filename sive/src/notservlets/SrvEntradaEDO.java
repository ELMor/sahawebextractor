/*
 *  Persona           Fecha                 Accion
 *    JRM             13/06/2000            Modifica para permitir la selecci�n
 *                                          conjunta de distrito y zona b�sica
 *                                          de salud.
 */
package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import notdata.DataEntradaEDO;
import sapp.DBServlet;

public class SrvEntradaEDO
    extends DBServlet {

  // JRM: Selecci�n de distrito/Zona b�sica de salud
  final int servletSELECCION_NIV2_X_CODIGO_CON_ZBS = 30;
  final int servletOBTENER_NIV2_X_CODIGO_CON_ZBS = 31;
  final int servletSELECCION_NIV2_X_DESCRIPCION_CON_ZBS = 32;
  final int servletOBTENER_NIV2_X_DESCRIPCION_CON_ZBS = 33;

  // modos de operaci�n del servlet
  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;

  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  final int servletSELECCION_ZBS_X_CODIGO = 11;
  final int servletOBTENER_ZBS_X_CODIGO = 12;
  final int servletSELECCION_ZBS_X_DESCRIPCION = 13;
  final int servletOBTENER_ZBS_X_DESCRIPCION = 14;

  final int servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ = 19;
  final int servletOBTENER_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 20;
  final int servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ = 21;
  final int servletSELECCION_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 22;
  final int servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ = 23;
  final int servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ = 24;
  final int servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ = 25;
  final int servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ = 26;

  // ARG: Nuevos modos de operaci�n del servlet
  final int servletOBTENER_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ = 34;
  final int servletOBTENER_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 35;
  final int servletSELECCION_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ = 36;
  final int servletSELECCION_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 37;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // objetos de datos
    CLista data = new CLista();
    DataEntradaEDO nivel = null;

    // Configuraci�n
    String sCampoCod = "CODIGO", sCampoDes = "DESC", sCampoDesL = "DESC_LOCAL";
    String sQuery = "", sAutorizacion1 = "", sAutorizacion2 = "";
    String sCod, sDes, sDesL;

    // JRM a�ade la muestra de �reas mediante el vector CD_NIVEL_1_AUTORIZACIONES
    // que es un par�metro de los applets que viene por param.getVN1()
    String strAreas = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    nivel = (DataEntradaEDO) param.firstElement();

    Vector areas;
    areas = param.getVN1();
    // Recorremos el vector
    String strAux = "";

    Enumeration enum;
    try {
      enum = areas.elements();
    }
    catch (Exception e) {
      enum = null;
    }

    while (enum.hasMoreElements()) {
      strAux = strAux + enum.nextElement();
      if (enum.hasMoreElements()) {
        strAux = strAux + ",";
      }
    }

    if ( (strAux.trim().equals("")) || (strAux.trim().equals("N"))) {
      strAreas = "";
    }
    else {
      strAreas = " and CD_NIVEL_1 in (" + strAux + ")";

      //// System_out.println(strAreas);

      // autorizaciones (s�lo para x_codigo y x_descripcion)
      // autorizaciones (s�lo para x_codigo y x_descripcion)

    }
    switch (opmode) {

      case servletOBTENER_NIV1_X_CODIGO:
      case servletOBTENER_NIV1_X_DESCRIPCION:
      case servletSELECCION_NIV1_X_CODIGO:
      case servletSELECCION_NIV1_X_DESCRIPCION:
      case servletSELECCION_NIV2_X_CODIGO:
      case servletOBTENER_NIV2_X_CODIGO:
      case servletSELECCION_NIV2_X_DESCRIPCION:
      case servletOBTENER_NIV2_X_DESCRIPCION:

        switch (param.getPerfil()) {
          case CApp.perfilCA:
            sAutorizacion1 = "";
            sAutorizacion2 = "";
            break;

          case CApp.perfilNIVEL1:

            //sAutorizacion1 = " and CD_NIVEL_1 IN (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?)";
            sAutorizacion1 = "";
            sAutorizacion2 = "";
            break;

          case CApp.perfilNIVEL2:

            //sAutorizacion1 = " and CD_NIVEL_1 IN (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?)";
            //sAutorizacion2 = " and CD_NIVEL_2 IN (select CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ? and CD_NIVEL_1 = ?)";
            sAutorizacion1 = "";
            sAutorizacion2 = "";
            break;
        }

        break;
    }

    // modos de operaci�n
    switch (opmode) {

      case servletOBTENER_NIV1_X_CODIGO:
      case servletOBTENER_NIV1_X_DESCRIPCION:
      case servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ:
      case servletOBTENER_NIV1_X_DESCRIPCION_TODAS_AUTORIZ:
      case servletOBTENER_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ:
      case servletOBTENER_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ:

        // configuracion
        sCampoCod = "CD_NIVEL_1";
        sCampoDes = "DS_NIVEL_1";
        sCampoDesL = "DSL_NIVEL_1";

        // query
        // ARG: upper (9/5/02)
        if ( (opmode == servletOBTENER_NIV1_X_CODIGO) ||
            (opmode == servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ) ||
            (opmode == servletOBTENER_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ)) {
          sQuery = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 = ?";
        }
        else {
          sQuery = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where upper(DS_NIVEL_1) = upper(?)";

        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_1 > ?";

          // autorizaciones (s�lo podr�n tener x_c�digo y x_descripci�n)
        }
        if (sAutorizacion1.length() > 0) {
          sQuery = sQuery + sAutorizacion1;

          //JRM: a�adimos las �reas permitidas ARG: dependiendo del modo de operaci�n
        }
        if ( (opmode != servletOBTENER_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ) &&
            (opmode != servletOBTENER_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ)) {
          sQuery = sQuery + strAreas;

        }
        sQuery = sQuery + " order by CD_NIVEL_1";

        // System_out.println(sQuery);

        st = con.prepareStatement(sQuery);

        // filtro
        st.setString(iValor, nivel.getCod().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        if (sAutorizacion1.length() > 0) {
          st.setString(iValor, param.getLogin().trim());

        }
        break;

      case servletSELECCION_NIV1_X_CODIGO:
      case servletSELECCION_NIV1_X_DESCRIPCION:
      case servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ:
      case servletSELECCION_NIV1_X_DESCRIPCION_TODAS_AUTORIZ:
      case servletSELECCION_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ:
      case servletSELECCION_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ:

        // configuracion
        sCampoCod = "CD_NIVEL_1";
        sCampoDes = "DS_NIVEL_1";
        sCampoDesL = "DSL_NIVEL_1";

        // query
        // ARG: upper (9/5/02)
        if ( (opmode == servletSELECCION_NIV1_X_CODIGO) ||
            (opmode == servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ) ||
            (opmode == servletSELECCION_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ)) {
          sQuery = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 like ?";
        }
        else {
          sQuery = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where upper(DS_NIVEL_1) like upper(?)";

        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_1 > ?";

          // autorizaciones (s�lo podr�n tener x_c�digo y x_descripci�n)
        }
        if (sAutorizacion1.length() > 0) {
          sQuery = sQuery + sAutorizacion1;

          //JRM: a�adimos las �reas permitidas ARG: dependiendo del modo de operaci�n
        }
        if ( (opmode != servletSELECCION_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ) &&
            (opmode != servletSELECCION_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ)) {
          sQuery = sQuery + strAreas;

        }
        sQuery = sQuery + " order by CD_NIVEL_1";

        // System_out.println(sQuery);

        st = con.prepareStatement(sQuery);

        // filtro
        if ( (opmode == servletSELECCION_NIV1_X_CODIGO) ||
            (opmode == servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ) ||
            (opmode == servletSELECCION_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ)) {
          st.setString(iValor, nivel.getCod().trim() + "%");
        }
        else {
          st.setString(iValor, "%" + nivel.getCod().trim() + "%");

        }
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        if (sAutorizacion1.length() > 0) {
          st.setString(iValor, param.getLogin().trim());

        }
        break;

      case servletOBTENER_NIV2_X_CODIGO:
      case servletOBTENER_NIV2_X_DESCRIPCION:
      case servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ:
      case servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ:

        // configuracion
        sCampoCod = "CD_NIVEL_2";
        sCampoDes = "DS_NIVEL_2";
        sCampoDesL = "DSL_NIVEL_2";

        // query
        // ARG: upper (9/5/02)
        if ( (opmode == servletOBTENER_NIV2_X_CODIGO) ||
            (opmode == servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ)) {
          sQuery = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 = ? and CD_NIVEL_1 = ?";
        }
        else {
          sQuery = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where upper(DS_NIVEL_2) = upper(?) and upper(CD_NIVEL_1) = upper(?)";

        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_2 > ?";

          // autorizaciones (s�lo podr�n tener x_c�digo y x_descripci�n)
        }
        if (sAutorizacion2.length() > 0) {
          sQuery = sQuery + sAutorizacion2;

        }
        sQuery = sQuery + " order by CD_NIVEL_2";

        st = con.prepareStatement(sQuery);

        // filtro
        st.setString(iValor, nivel.getCod().trim());
        iValor++;

        st.setString(iValor, nivel.getNivel1().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        if (sAutorizacion2.length() > 0) {
          st.setString(iValor, param.getLogin().trim());
          st.setString(iValor + 1, nivel.getNivel1().trim());
        }

        break;

      case servletSELECCION_NIV2_X_CODIGO:
      case servletSELECCION_NIV2_X_DESCRIPCION:
      case servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ:
      case servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ:

        // configuraci�n
        sCampoCod = "CD_NIVEL_2";
        sCampoDes = "DS_NIVEL_2";
        sCampoDesL = "DSL_NIVEL_2";

        // query
        // ARG: upper (9/5/02)
        if ( (opmode == servletSELECCION_NIV2_X_CODIGO) ||
            (opmode == servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ)) {
          sQuery = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 like ?";
        }
        else {
          sQuery = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where upper(DS_NIVEL_2) like upper(?)";

        }
        sQuery = sQuery + " and upper(CD_NIVEL_1) = upper(?)";
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_2 > ?";

          // autorizaciones (s�lo podr�n tener x_c�digo y x_descripci�n)
        }
        if (sAutorizacion2.length() > 0) {
          sQuery = sQuery + sAutorizacion2;

        }
        sQuery = sQuery + " order by CD_NIVEL_2";

        st = con.prepareStatement(sQuery);

        // filtro
        if ( (opmode == servletSELECCION_NIV2_X_CODIGO) ||
            (opmode == servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ)) {
          st.setString(iValor, nivel.getCod().trim() + "%");
        }
        else {
          st.setString(iValor, "%" + nivel.getCod().trim() + "%");

        }
        iValor++;

        st.setString(iValor, nivel.getNivel1().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        if (sAutorizacion2.length() > 0) {
          st.setString(iValor, param.getLogin().trim());
          st.setString(iValor + 1, nivel.getNivel1().trim());
        }
        break;

      case servletOBTENER_ZBS_X_CODIGO:
      case servletOBTENER_ZBS_X_DESCRIPCION:

        // configuraci�n
        sCampoCod = "CD_ZBS";
        sCampoDes = "DS_ZBS";
        sCampoDesL = "DSL_ZBS";

        // query
        // ARG: upper (9/5/02)
        if (opmode == servletOBTENER_ZBS_X_CODIGO) {
          sQuery = "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where CD_ZBS = ? and CD_NIVEL_1 = ? and CD_NIVEL_2 = ?";
        }
        else {
          sQuery = "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where upper(DS_ZBS) = upper(?) and upper(CD_NIVEL_1) = upper(?) and upper(CD_NIVEL_2) = upper(?)";

        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_ZBS > ?";

          // autorizaciones
          //if (sAutorizacion2.length() > 0)
          //  sQuery = sQuery + sAutorizacion2;

        }
        sQuery = sQuery + " order by CD_ZBS";

        st = con.prepareStatement(sQuery);

        // filtro
        st.setString(iValor, nivel.getCod().trim());
        iValor++;

        st.setString(iValor, nivel.getNivel1().trim());
        iValor++;

        st.setString(iValor, nivel.getNivel2().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        break;

      case servletSELECCION_ZBS_X_CODIGO:
      case servletSELECCION_ZBS_X_DESCRIPCION:

        // configuraci�n
        sCampoCod = "CD_ZBS";
        sCampoDes = "DS_ZBS";
        sCampoDesL = "DSL_ZBS";

        // query
        // ARG: upper (9/5/02)
        if (opmode == servletSELECCION_ZBS_X_CODIGO) {
          sQuery =
              "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where CD_ZBS like ?";
        }
        else {
          sQuery = "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where upper(DS_ZBS) like upper(?)";

        }
        sQuery = sQuery +
            " and upper(CD_NIVEL_1) = upper(?) and upper(CD_NIVEL_2) = upper(?)";
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_ZBS > ?";

        }
        sQuery = sQuery + " order by CD_ZBS";
        st = con.prepareStatement(sQuery);

        // filtro
        if (opmode == servletSELECCION_ZBS_X_CODIGO) {
          st.setString(iValor, nivel.getCod().trim() + "%");
        }
        else {
          st.setString(iValor, "%" + nivel.getCod().trim() + "%");

        }
        iValor++;

        st.setString(iValor, nivel.getNivel1().trim());
        iValor++;

        st.setString(iValor, nivel.getNivel2().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        break;
        // JRM
      case servletOBTENER_NIV2_X_CODIGO_CON_ZBS:
      case servletOBTENER_NIV2_X_DESCRIPCION_CON_ZBS:

        // configuracion
        sCampoCod = "CD_NIVEL_2";
        sCampoDes = "DS_NIVEL_2";
        sCampoDesL = "DSL_NIVEL_2";

        // query
        if (opmode == servletOBTENER_NIV2_X_CODIGO_CON_ZBS) {
          sQuery = "select d.CD_NIVEL_2 || zb.CD_ZBS as CD_NIVEL_2, " +
              "d.DS_NIVEL_2 || ' - ' || zb.DS_ZBS as DS_NIVEL_2, " +
              "d.DSL_NIVEL_2|| ' - ' || zb.DSL_ZBS as DSL_NIVEL_2 " +
              "from   SIVE_NIVEL2_S d, SIVE_ZONA_BASICA zb " +
              "where  d.CD_NIVEL_1 = zb.CD_NIVEL_1 and " +
              "       d.CD_NIVEL_2 = zb.CD_NIVEL_2 and " +
              "       d.CD_NIVEL_2 || zb.CD_ZBS like '" + nivel.getCod().trim() +
              "%'" +
              "       and d.CD_NIVEL_1 = '" + nivel.getNivel1().trim() + "'";
        }
        else {
          sQuery = "select d.CD_NIVEL_2 || zb.CD_ZBS as CD_NIVEL_2, " +
              "d.DS_NIVEL_2 || ' - ' || zb.DS_ZBS as DS_NIVEL_2, " +
              "d.DSL_NIVEL_2|| ' - ' || zb.DSL_ZBS as DSL_NIVEL_2 " +
              "from   SIVE_NIVEL2_S d, SIVE_ZONA_BASICA zb " +
              "where  d.CD_NIVEL_1 = zb.CD_NIVEL_1 and " +
              "       d.CD_NIVEL_2 = zb.CD_NIVEL_2 and " +
              "       d.DS_NIVEL_2 || ' - ' || zb.DS_ZBS = '" +
              nivel.getCod().trim() + "'" +
              "       and d.CD_NIVEL_1 = '" + nivel.getNivel1().trim() + "'";

          // �?
        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_2 > ?";

        }
        sQuery = sQuery + " order by d.DS_NIVEL_2, zb.DS_ZBS";
        st = con.prepareStatement(sQuery);

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }
        break;

        // JRM
      case servletSELECCION_NIV2_X_CODIGO_CON_ZBS:
      case servletSELECCION_NIV2_X_DESCRIPCION_CON_ZBS:

        // configuraci�n
        sCampoCod = "CD_NIVEL_2";
        sCampoDes = "DS_NIVEL_2";
        sCampoDesL = "DSL_NIVEL_2";

        // query
        if (opmode == servletSELECCION_NIV2_X_CODIGO_CON_ZBS) {
          sQuery = "select d.CD_NIVEL_2 || zb.CD_ZBS as CD_NIVEL_2, " +
              "d.DS_NIVEL_2 || ' - ' || zb.DS_ZBS as DS_NIVEL_2, " +
              "d.DSL_NIVEL_2|| ' - ' || zb.DSL_ZBS as DSL_NIVEL_2 " +
              "from   SIVE_NIVEL2_S d, SIVE_ZONA_BASICA zb " +
              "where  d.CD_NIVEL_1 = zb.CD_NIVEL_1 and " +
              "       d.CD_NIVEL_2 = zb.CD_NIVEL_2 and " +
              "       d.CD_NIVEL_2 || zb.CD_ZBS like '" + nivel.getCod().trim() +
              "%'";
        }
        else {
          sQuery = "select d.CD_NIVEL_2 || zb.CD_ZBS as CD_NIVEL_2, " +
              "d.DS_NIVEL_2 || ' - ' || zb.DS_ZBS as DS_NIVEL_2, " +
              "d.DSL_NIVEL_2|| ' - ' || zb.DSL_ZBS as DSL_NIVEL_2 " +
              "from   SIVE_NIVEL2_S d, SIVE_ZONA_BASICA zb " +
              "where  d.CD_NIVEL_1 = zb.CD_NIVEL_1 and " +
              "       d.CD_NIVEL_2 = zb.CD_NIVEL_2 and " +
              "       d.DS_NIVEL_2 || ' - ' || zb.DS_ZBS like '" +
              nivel.getCod().trim() + "%'";

        }
        sQuery = sQuery + " and d.CD_NIVEL_1 = '" + nivel.getNivel1().trim() +
            "'";

        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_2 > ?";

        }
        sQuery = sQuery + " order by d.DS_NIVEL_2, zb.DS_ZBS";

        st = con.prepareStatement(sQuery);

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        break;
    }

    rs = st.executeQuery();

    // extrae la p�gina requerida
    while (rs.next()) {

      // control de tama�o
      if (i > DBServlet.maxSIZE) {
        data.setState(CLista.listaINCOMPLETA);
        data.setFilter( ( (DataEntradaEDO) data.lastElement()).getCod());
        break;
      }

      // control de estado
      if (data.getState() == CLista.listaVACIA) {
        data.setState(CLista.listaLLENA);
      }

      // obtiene los campos
      sCod = rs.getString(sCampoCod);
      sDes = rs.getString(sCampoDes);

      // si aplica campo de descripci�n local, lo recogemos
      if (!sCampoDesL.equals("")) {

        sDesL = rs.getString(sCampoDesL);

        // obtiene la descripcion auxiliar en funci�n del idioma
        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
          sDes = sDesL;
        }
      }

      // a�ade un nodo
      data.addElement(new DataEntradaEDO(sCod, sDes));

      i++;
    }
    rs.close();
    st.close();

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }

    return data;
  }
}
