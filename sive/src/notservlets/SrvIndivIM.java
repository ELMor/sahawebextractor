package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

// LORTAD
import Registro.RegistroConsultas;
import capp.CLista;
import notdata.DataEnfCaso;
import notdata.DataMovEnfermo;
import notdata.DataTab;
import sapp.DBServlet;

//import jdbcpool.*; // $$$ modo debug

public class SrvIndivIM
    extends DBServlet {

  //********************************************************************************************************
   //caso 1: CD_ENFERMO no informado: nuevo enfermo, nuevo caso, posible varias notif_edoi
   //ESTRUCTURA DE LA LISTA:
   //primer registro :
   //datos de enfermo (Excepto Cod max +1), datos de caso (Excepto nm_edo max +1), datos declarante
   //Nota: hay un bucle pero solo se inserta un declarante)
   //listaSalida --> DataEnfCaso con CodEnf, y Num_Edo (strings)

   //Si lista vacia, no hace nada: listaSalida.size() == 0

   final int servletSIN_CD_ENFERMO_INSERT_INSERT_INSERT = 0;
  //************************************************************************************************************

   //NUEVOS TIPOS DE SRVLET: SE PUEDE MODIFICAR DATOS ENFERMO

   //************************************************************************************************************
    //caso 4:(PARALELO AL 2) CD_ENFERMo informado y SUS DATOS
    //           NM_EDO no informado
    //          , nuevo caso, UNA notif_edoi nueva
    //ESTRUCTURAenfermo  DE LA LISTA:
    //primer registro :
    //(incluye Cod enfermo Y SUS DATOS), datos de caso (Excepto nm_edo max +1), datos del declarante
    //listaSalida --> DataEnfCaso con CodEnf, y Num_Edo (strings)
    //Nota: hay un bucle pero solo se inserta un declarante)
    //Si lista vacia, no puede ser (todas tablas tienen campos obligatorios), no hace nada
    final int servletCON_CD_ENFERMO_SIN_NM_EDO_MODIF_INSERT_INSERT = 3;
  //************************************************************************************************************

   //************************************************************************************************************
    //caso 5:(PARALELO AL 3) CD_ENFERMo informado Y SUS DATOS.
    //           NM_EDO  informado
    //         , modifico edoind , borro sus notif_edoi y la vuelvo a cargar
    //ESTRUCTURAenfermo  DE LA LISTA:
    //primer registro :
    //(incluye Cod  de enfermo Y SUS DATOS), datos de caso (incluye nm_edo ), datos del  declarante
    //siguientes registros:
    //datos de los siguientes declarantes
    //listaSalida --> DataEnfCaso con CodEnf, y Num_Edo (strings)

    //Si lista vacia, no puede ser (todas tablas tienen campos obligatorios), no hace nada
    final int servletCON_CD_ENFERMO_CON_NM_EDO_MODIF_MODIF_BUCLE = 4;
  //************************************************************************************************************

   public SimpleDateFormat Format_Horas = new SimpleDateFormat(
       "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  public java.sql.Timestamp deString_ATimeStamp(String sFecha) {

    String sFecha_larga = sFecha;
    //partirla!!
    int sdd = (new Integer(sFecha_larga.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_larga.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_larga.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_larga.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_larga.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_larga.substring(17, 19))).intValue();

    java.sql.Timestamp TSFecha = new java.sql.Timestamp(syyyy - 1900,
        smm - 1,
        sdd, shh, smi, sss, 0);
    return (TSFecha);

  }

  boolean comprobarEnfermedadA(DataTab dataEntrada, Connection con,
                               PreparedStatement st, ResultSet rs) throws
      Exception {

    //fechas
    java.util.Date dFecha = new java.util.Date();
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    String query = "";
    //comprobacion del numero de individualizadas
    int numIndiv = 0;

    query = " select a.NM_EDO from SIVE_EDOIND a, SIVE_NOTIF_EDOI b "
        + " where a.NM_EDO = b.NM_EDO and  a.CD_ENFCIE = ? "
        + " and b.CD_E_NOTIF = ? "
        + " and b.CD_ANOEPI = ? and b.CD_SEMEPI = ? "
        + " and b.FC_RECEP = ? and b.FC_FECNOTIF = ? "
        + " and b.CD_FUENTE = 'E' ";

    st = con.prepareStatement(query);
    st.setString(1, dataEntrada.getCD_ENFCIE());
    st.setString(2, dataEntrada.getCD_E_NOTIF());
    st.setString(3, dataEntrada.getCD_ANOEPI());
    st.setString(4, dataEntrada.getCD_SEMEPI());

    registroConsultas.insertarParametro(dataEntrada.getCD_ENFCIE());
    registroConsultas.insertarParametro(dataEntrada.getCD_E_NOTIF());
    registroConsultas.insertarParametro(dataEntrada.getCD_ANOEPI());
    registroConsultas.insertarParametro(dataEntrada.getCD_SEMEPI());

    dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(5, sqlFec);
    registroConsultas.insertarParametro(
        "TO_DATE('" +
        dataEntrada.getFC_RECEP().trim()
        +
        "','DD/MM/YYYY')");

    dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);
    registroConsultas.insertarParametro(
        "TO_DATE('" +
        dataEntrada.getFC_FECNOTIF().trim()
        +
        "','DD/MM/YYYY')");

    registroConsultas.registrar("SIVE_EDOIND",
                                query,
                                "CD_ENFERMO",
                                "",
                                "SrvIndivIM",
                                true);

    rs = st.executeQuery();

    while (rs.next()) {
      numIndiv = numIndiv + 1;
    }
    numIndiv = numIndiv + 1; //para incluir el alta que voy a dar
    rs.close();
    rs = null;
    st.close();
    st = null;

    //comprobacion del numero de numericas
    int numNum = 0;
    query = " select nm_casos from sive_edonum "
        + " where cd_enfcie = ? and cd_e_notif = ? "
        + " and cd_anoepi = ? and cd_semepi = ? "
        + " and fc_recep = ? and fc_fecnotif = ? ";

    st = con.prepareStatement(query);
    st.setString(1, dataEntrada.getCD_ENFCIE());
    st.setString(2, dataEntrada.getCD_E_NOTIF());
    st.setString(3, dataEntrada.getCD_ANOEPI());
    st.setString(4, dataEntrada.getCD_SEMEPI());

    dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(5, sqlFec);
    dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(6, sqlFec);

    rs = st.executeQuery();

    if (rs.next()) {
      numNum = rs.getInt("NM_CASOS");
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    if (numIndiv > numNum) {
      return true;
    }
    else {
      return false;
    }
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    DataTab dataEntrada = null;
    DataMovEnfermo dataMovEnfermo = null;
    CLista listaSalida = new CLista();
    DataEnfCaso dataSalida = null;

    //fecha actual-----------------------------------------------------------
    //para devolver (sFecha_Actual), para insertar (st.setTimestamp(9, TSFec))
    java.util.Date dFecha_Actual = new java.util.Date();
    String sFecha_Actual = Format_Horas.format(dFecha_Actual); //string
    //partirla!!
    int sdd = (new Integer(sFecha_Actual.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_Actual.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_Actual.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_Actual.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_Actual.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_Actual.substring(17, 19))).intValue();

    java.sql.Timestamp TSFec = new java.sql.Timestamp(syyyy - 1900, smm - 1,
        sdd, shh, smi, sss, 0);
    //st.setTimestamp(9, TSFec);
    // --------------------------------------------------------------------

    boolean Existe_elenfermo_paraotro_caso = false;

    //SALIDA: CodEnf, sCaso
    String sCaso = "";
    String CodEnf = "";
    String sMaxEnfermo = "";

    //variables
    String cd_prov = "";
    String cd_pais = "";
    String cd_mun = "";
    String cd_tdoc = "";
    //String cd_via   = "";
    //String cd_vial= "";
    String ds_ape1 = "";
    String ds_ape2 = "";
    String ds_nombre = "";
    String ds_fonoape1 = "";
    String ds_fonoape2 = "";
    String ds_fononombre = "";
    String it_calc = "";
    //String fc_nac    = "";
    String cd_sexo = "";
    String cd_postal = "";
    String ds_direc = "";
    String ds_num = "";
    String ds_piso = "";
    String ds_telef = "";
    String cd_nivel_1 = "";
    String cd_nivel_2 = "";
    String cd_zbs = "";
    String cd_ope = "";
    String fc_ultact = "";
    String it_revisado = "";
    String cd_motbaja = "";
    //String fc_baja    = "";
    String ds_ndoc = "";
    String siglas = "";
    String ds_observ = "";
    String cd_prov2 = "";
    String cd_muni2 = "";
    String cd_post2 = "";
    String ds_direc2 = "";
    String ds_num2 = "";
    String ds_piso2 = "";
    String ds_telef2 = "";
    String ds_observ2 = "";
    String cd_prov3 = "";
    String cd_muni3 = "";
    String cd_post3 = "";
    String ds_direc3 = "";
    String ds_num3 = "";
    String ds_piso3 = "";
    String ds_telef3 = "";
    String ds_observ3 = "";
    // suca
    String cdvial = "";
    String cdtvia = "";
    String cdtnum = "";
    String dscalnum = "";

    //querys
    String ALTA = "";
    String sQuery = "";

    final String queryPrevia = "SELECT MAX(NM_EDO) "
        + "FROM SIVE_EDOIND WHERE NM_EDO < ?";
    final String queryPrevia2 = "SELECT MAX(CD_ENFERMO) "
        + "FROM SIVE_ENFERMO WHERE CD_ENFERMO < ?";
    final String sMaxBajaEnfermo = "SELECT MAX(NM_MOVENF) "
        + "FROM SIVE_MOV_ENFERMO WHERE NM_MOVENF < ?";

    //fechas
    java.util.Date dFecha = new java.util.Date();
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    //SimpleDateFormat String_to_Date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss SSS");

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);
    con.setAutoCommit(false);

    try {

      switch (opmode) {

        case servletSIN_CD_ENFERMO_INSERT_INSERT_INSERT:

          if (param.size() != 0) { //comienzo
            dataEntrada = (DataTab) param.firstElement();

            // se hace una insercion en EDOIND
            // compruebo si la enfermedad es de tipo A y si es asi,
            // que el numero de individuales sea menor que el numero
            // total de casos en EDONUM

            boolean salirA = false;

            if (dataEntrada.getCD_TVIGI().equals("A")) {
              salirA = comprobarEnfermedadA(dataEntrada, con, st, rs);

              // el resto del proceso se hace para todas las I y para
              // las A que han superado la comprobacion
            }
            if (dataEntrada.getCD_TVIGI().equals("I") || !salirA) {

              //select max de nm_edo --------------------------------------------------

              dataEntrada = (DataTab) param.firstElement();
              //modificacion momentanea
              /*        st = con.prepareStatement(queryPrevia);
                      Integer NumMax = new Integer(999999);
                      st.setInt(1, NumMax.intValue());
                      rs = st.executeQuery();
                      if (rs.next()){
                        if (rs == null){
                          sCaso = "1";
                        }
                        else{
                          int iC = rs.getInt(1) + 1;
                          Integer isCaso = new Integer(iC);
                          sCaso = isCaso.toString();
                        }
                      }
                      rs.close();
                      rs=null;
                      st.close();
                      st=null;
               */

              //select max de sive_enfermo-------------------------------------------
              // modificacion momentanea
              // se accede a la tabla sive_sec_general para obtener el
              // valor del secuencial y aumentarlo en uno

              int iNM_ENFERMO = 0;
              iNM_ENFERMO = sapp.Funciones.SecGeneral(con, st, rs, "NM_ENFERMO");
              CodEnf = Integer.toString(iNM_ENFERMO);

              int iNM_EDO = 0;
              iNM_EDO = sapp.Funciones.SecGeneral(con, st, rs, "NM_EDO");
              sCaso = Integer.toString(iNM_EDO);

              ////////////////////////////////////////////
              /*        dataEntrada = (DataTab) param.firstElement();
                      st = con.prepareStatement(queryPrevia2);
                      Integer NumMaxEnf = new Integer(999999);
                      st.setInt(1, NumMaxEnf.intValue());
                      rs = st.executeQuery();
                      if (rs.next()){
                        if (rs==null){
                          CodEnf = "1";
                        }
                        else{
                          int iE = rs.getInt(1) + 1;
                          Integer iCodEnf = new Integer(iE);
                          CodEnf = iCodEnf.toString();
                        }
                      }
                      rs.close();
                      rs=null;
                      st.close();
                      st=null;
               */

              //insertamos en enfermo, en edoind, y en notif_edoi

              //!!!!!!! ENFERMO !!!!!!!!!!!!
              //leemos SOLO DEL  primer REGISTRO ----------------------------
              dataEntrada = (DataTab) param.firstElement(); //repetitivo

              ALTA = Pregunta_Alta_Enfermo();

              st = con.prepareStatement(ALTA);

              //lo que hemos obtenido
              st.setInt(1, new Integer(CodEnf.trim()).intValue());

              if (dataEntrada.getCD_PROV().trim().length() > 0) {
                st.setString(2, dataEntrada.getCD_PROV().trim());
              }
              else {
                st.setNull(2, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_PAIS().trim().length() > 0) {
                st.setString(3, dataEntrada.getCD_PAIS().trim());
              }
              else {
                st.setNull(3, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_MUN().trim().length() > 0) {
                st.setString(4, dataEntrada.getCD_MUN().trim());
              }
              else {
                st.setNull(4, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_TDOC().trim().length() > 0) {
                st.setString(5, dataEntrada.getCD_TDOC().trim());
              }
              else {
                st.setNull(5, java.sql.Types.VARCHAR);

                /*" *DS_APE1,DS_APE2,DS_NOMBRE , *DS_FONOAPE1, DS_FONOAPE2, */

              }
              st.setString(6, dataEntrada.getDS_APE1().trim());

              //DS_APE2
              if (dataEntrada.getDS_APE2().trim().length() > 0) {
                st.setString(7, dataEntrada.getDS_APE2().trim());
              }
              else {
                st.setNull(7, java.sql.Types.VARCHAR);

                //DS_NOMBRE
              }
              if (dataEntrada.getDS_NOMBRE().trim().length() > 0) {
                st.setString(8, dataEntrada.getDS_NOMBRE().trim());
              }
              else {
                st.setNull(8, java.sql.Types.VARCHAR);

                //FONOAPE1 (NO LE HACEMOS TRIM)
              }
              st.setString(9, dataEntrada.getDS_FONOAPE1());

              //FONOAPE2 (NO LE HACEMOS TRIM)
              if (dataEntrada.getDS_FONOAPE2().length() > 0) {
                st.setString(10, dataEntrada.getDS_FONOAPE2());
              }
              else {
                st.setNull(10, java.sql.Types.VARCHAR);

                /* DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL, */

                //FONONOMBRE (NO LE HACEMOS TRIM)
              }
              if (dataEntrada.getDS_FONONOMBRE().length() > 0) {
                st.setString(11, dataEntrada.getDS_FONONOMBRE());
              }
              else {
                st.setNull(11, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getIT_CALC().trim().length() > 0) {
                st.setString(12, dataEntrada.getIT_CALC().trim());
              }
              else {
                st.setNull(12, java.sql.Types.VARCHAR);
                ////////////
              }
              if (dataEntrada.getFC_NAC().trim().length() > 0) {
                dFecha = formater.parse(dataEntrada.getFC_NAC().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(13, sqlFec);
              }
              else {
                st.setNull(13, java.sql.Types.VARCHAR);
                /////////
              }
              if (dataEntrada.getCD_SEXO().trim().length() > 0) {
                st.setString(14, dataEntrada.getCD_SEXO().trim());
              }
              else {
                st.setNull(14, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_POSTAL().trim().length() > 0) {
                st.setString(15, dataEntrada.getCD_POSTAL().trim());
              }
              else {
                st.setNull(15, java.sql.Types.VARCHAR);

                    /*  DS_DIREC,DS_NUM,DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2" */
              }
              if (dataEntrada.getDS_DIREC().trim().length() > 0) {
                st.setString(16, dataEntrada.getDS_DIREC().trim());
              }
              else {
                st.setNull(16, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_NUM().trim().length() > 0) {
                st.setString(17, dataEntrada.getDS_NUM().trim());
              }
              else {
                st.setNull(17, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_PISO().trim().length() > 0) {
                st.setString(18, dataEntrada.getDS_PISO().trim());
              }
              else {
                st.setNull(18, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_TELEF().trim().length() > 0) {
                st.setString(19, dataEntrada.getDS_TELEF().trim());
              }
              else {
                st.setNull(19, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_NIVEL_1().trim().length() > 0) {
                st.setString(20, dataEntrada.getCD_NIVEL_1().trim());
              }
              else {
                st.setNull(20, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_NIVEL_2().trim().length() > 0) {
                st.setString(21, dataEntrada.getCD_NIVEL_2().trim());
              }
              else {
                st.setNull(21, java.sql.Types.VARCHAR);

                    /*  CD_ZBS, *CD_OPE, *FC_ULTACT,*IT_REVISADO, CD_MOTBAJA, FC_BAJA,*/
              }
              if (dataEntrada.getCD_ZBS().trim().length() > 0) {
                st.setString(22, dataEntrada.getCD_ZBS().trim());
              }
              else {
                st.setNull(22, java.sql.Types.VARCHAR);

              }
              st.setString(23, dataEntrada.getCD_OPE().trim());
              ////////////
              /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                          sqlFec = new java.sql.Date(dFecha.getTime());
                          st.setDate(24, sqlFec);*/
              st.setTimestamp(24, TSFec);
              /////////

              //al insertar cargo un 'N'
              st.setString(25, "N");

              if (dataEntrada.getDS_NDOC().trim().length() > 0) {
                st.setString(26, dataEntrada.getDS_NDOC().trim());
              }
              else {
                st.setNull(26, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getSIGLAS().trim().length() > 0) {
                st.setString(27, dataEntrada.getSIGLAS().trim());
              }
              else {
                st.setNull(27, java.sql.Types.VARCHAR);

                // suca
              }
              if (dataEntrada.getCDVIAL().trim().length() > 0) {
                st.setString(28, dataEntrada.getCDVIAL().trim());
              }
              else {
                st.setNull(28, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCDTVIA().trim().length() > 0) {
                st.setString(29, dataEntrada.getCDTVIA().trim());
              }
              else {
                st.setNull(29, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCDTNUM().trim().length() > 0) {
                st.setString(30, dataEntrada.getCDTNUM().trim());
              }
              else {
                st.setNull(30, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDSCALNUM().trim().length() > 0) {
                st.setString(31, dataEntrada.getDSCALNUM().trim());
              }
              else {
                st.setNull(31, java.sql.Types.VARCHAR);

              }
              st.setString(32, "S"); // modificacion 11/05 para la integraci�n

              st.setString(33, "N");

              st.executeUpdate();
              st.close();
              st = null;

//!!!! EDOIND  !!!!!!!!!!!!!
              //Leemos del primer REGISTRO -----------------------------
              dataEntrada = (DataTab) param.firstElement(); //repetitivo

              ALTA = Pregunta_Alta_Edoind();
              st = con.prepareStatement(ALTA);

              st.setInt(1, new Integer(sCaso.trim()).intValue());
              st.setString(2, dataEntrada.getCD_ANOEPI().trim());
              st.setString(3, dataEntrada.getCD_SEMEPI().trim());

              if (dataEntrada.getCD_ANOURG().trim().length() > 0) {
                st.setString(4, dataEntrada.getCD_ANOURG().trim());
              }
              else {
                st.setNull(4, java.sql.Types.VARCHAR);

              }

              if (dataEntrada.getNM_ENVIOURGSEM().trim().length() > 0) {
                st.setInt(5,
                          new Integer(dataEntrada.getNM_ENVIOURGSEM().trim()).intValue());
              }
              else {
                st.setNull(5, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_CLASIFDIAG().trim().length() > 0) {
                st.setString(6, dataEntrada.getCD_CLASIFDIAG().trim());
              }
              else {
                st.setNull(6, java.sql.Types.VARCHAR);

                /*+ " IT_PENVURG, *CD_ENFERMO, CD_PROV,CD_MUN, " //4  */
              }
              if (dataEntrada.getIT_PENVURG().trim().length() > 0) {
                st.setString(7, dataEntrada.getIT_PENVURG().trim());
              }
              else {
                st.setNull(7, java.sql.Types.VARCHAR);

              }
              st.setInt(8, new Integer(CodEnf.trim()).intValue());

              if (dataEntrada.getCD_PROV_EDOIND().trim().length() > 0) {
                st.setString(9, dataEntrada.getCD_PROV_EDOIND().trim());
              }
              else {
                st.setNull(9, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_MUN_EDOIND().trim().length() > 0) {
                st.setString(10, dataEntrada.getCD_MUN_EDOIND().trim());
              }
              else {
                st.setNull(10, java.sql.Types.VARCHAR);

                    /* + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, " //5 */

              }
              if (dataEntrada.getCD_NIVEL_1_EDOIND().trim().length() > 0) {
                st.setString(11, dataEntrada.getCD_NIVEL_1_EDOIND().trim());
              }
              else {
                st.setNull(11, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_NIVEL_2_EDOIND().trim().length() > 0) {
                st.setString(12, dataEntrada.getCD_NIVEL_2_EDOIND().trim());
              }
              else {
                st.setNull(12, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_ZBS_EDOIND().trim().length() > 0) {
                st.setString(13, dataEntrada.getCD_ZBS_EDOIND().trim());
              }
              else {
                st.setNull(13, java.sql.Types.VARCHAR);

                //fecha OBLIGATORIA
              }
              dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(14, sqlFec);

              if (dataEntrada.getDS_CALLE().trim().length() > 0) {
                st.setString(15, dataEntrada.getDS_CALLE().trim());
              }
              else {
                st.setNull(15, java.sql.Types.VARCHAR);

                    /* + " DS_NMCALLE, DS_PISO, CD_POSTAL, *CD_OPE, *FC_ULTACT, "  //5*/
              }
              if (dataEntrada.getDS_NMCALLE().trim().length() > 0) {
                st.setString(16, dataEntrada.getDS_NMCALLE().trim());
              }
              else {
                st.setNull(16, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_PISO_EDOIND().trim().length() > 0) {
                st.setString(17, dataEntrada.getDS_PISO_EDOIND().trim());
              }
              else {
                st.setNull(17, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_POSTAL_EDOIND().trim().length() > 0) {
                st.setString(18, dataEntrada.getCD_POSTAL_EDOIND().trim());
              }
              else {
                st.setNull(18, java.sql.Types.VARCHAR);

              }
              st.setString(19, dataEntrada.getCD_OPE_EDOIND().trim());

                  /*dFecha = formater.parse(dataEntrada.getFC_ULTACT_EDOIND().trim());
                        sqlFec = new java.sql.Date(dFecha.getTime());
                        st.setDate(21, sqlFec); */
              st.setTimestamp(20, TSFec);

              /*  + " CD_ANOOTRC, NM_ENVOTRC, *CD_ENFCIE "  //3   */
              if (dataEntrada.getCD_ANOOTRC().trim().length() > 0) {
                st.setString(21, dataEntrada.getCD_ANOOTRC().trim());
              }
              else {
                st.setNull(21, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getNM_ENVOTRC().trim().length() > 0) {
                st.setInt(22,
                          new Integer(dataEntrada.getNM_ENVOTRC().trim()).intValue());
              }
              else {
                st.setNull(22, java.sql.Types.VARCHAR);

              }
              st.setString(23, dataEntrada.getCD_ENFCIE().trim());

              /*     + " *FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, "// 3+
                     + " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, "// 3+
                     + " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO ) "// 3+  */

              dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(24, sqlFec);

              if (dataEntrada.getIT_DERIVADO().trim().length() > 0) {
                st.setString(25, dataEntrada.getIT_DERIVADO().trim());
              }
              else {
                st.setNull(25, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_CENTRODER().trim().length() > 0) {
                st.setString(26, dataEntrada.getDS_CENTRODER().trim());
              }
              else {
                st.setNull(26, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getIT_DIAGCLI().trim().length() > 0) {
                st.setString(27, dataEntrada.getIT_DIAGCLI().trim());
              }
              else {
                st.setNull(27, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getFC_INISNT().trim().length() > 0) {
                dFecha = formater.parse(dataEntrada.getFC_INISNT().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(28, sqlFec);
              }
              else {
                st.setNull(28, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_COLECTIVO().trim().length() > 0) {
                st.setString(29, dataEntrada.getDS_COLECTIVO().trim());
              }
              else {
                st.setNull(29, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getIT_ASOCIADO().trim().length() > 0) {
                st.setString(30, dataEntrada.getIT_ASOCIADO().trim());
              }
              else {
                st.setNull(30, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getIT_DIAGMICRO().trim().length() > 0) {
                st.setString(31, dataEntrada.getIT_DIAGMICRO().trim());
              }
              else {
                st.setNull(31, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_ASOCIADO().trim().length() > 0) {
                st.setString(32, dataEntrada.getDS_ASOCIADO().trim());
              }
              else {
                st.setNull(32, java.sql.Types.VARCHAR);
                /* suca
                     if (dataEntrada.getCD_VIA_EDOIND().trim() .length() > 0)
                  st.setString(34, dataEntrada.getCD_VIA_EDOIND().trim());
                          else
                    st.setNull(34, java.sql.Types.VARCHAR);
                     if (dataEntrada.getCD_VIAL_EDOIND().trim() .length() > 0)
                  st.setString(35, dataEntrada.getCD_VIAL_EDOIND().trim());
                          else
                    st.setNull(35, java.sql.Types.VARCHAR);
                 */
              }
              if (dataEntrada.getIT_DIAGSERO().trim().length() > 0) {
                st.setString(33, dataEntrada.getIT_DIAGSERO().trim());
              }
              else {
                st.setNull(33, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_DIAGOTROS().trim().length() > 0) {
                st.setString(34, dataEntrada.getDS_DIAGOTROS().trim());
              }
              else {
                st.setNull(34, java.sql.Types.VARCHAR);

                // suca
              }
              if (dataEntrada.getCDVIAL().trim().length() > 0) {
                st.setString(35, dataEntrada.getCDVIAL().trim());
              }
              else {
                st.setNull(35, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCDTVIA().trim().length() > 0) {
                st.setString(36, dataEntrada.getCDTVIA().trim());
              }
              else {
                st.setNull(36, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCDTNUM().trim().length() > 0) {
                st.setString(37, dataEntrada.getCDTNUM().trim());
              }
              else {
                st.setNull(37, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDSCALNUM().trim().length() > 0) {
                st.setString(38, dataEntrada.getDSCALNUM().trim());
              }
              else {
                st.setNull(38, java.sql.Types.VARCHAR);

              }

              st.executeUpdate();
              st.close();
              st = null;

//!!!! NOTIF EDO!!!!!!!!!!!!!!!!!!!!!!
//recorremos la lista de entrada. Tantas altas como declarantes haya.
              for (i = 0; i < param.size(); i++) {
                dataEntrada = (DataTab) param.elementAt(i);
                // JRM
                String NM_EDO;
                String CD_E_NOTIF;
                String CD_ANOEPI;
                String CD_SEMEPI;
                String FC_RECEP;
                String DS_DECLARANTE;
                String IT_PRIMERO;
                String CD_OPE;
                String FC_ULTACT;
                String FC_FECNOTIF;
                String CD_FUENTE;

                NM_EDO = sCaso.trim();
                CD_E_NOTIF = dataEntrada.getCD_E_NOTIF().trim();
                CD_ANOEPI = dataEntrada.getCD_ANOEPI_NOTIF_EDOI().trim();
                CD_SEMEPI = dataEntrada.getCD_SEMEPI_NOTIF_EDOI().trim();
                FC_RECEP = dataEntrada.getFC_RECEP_NOTIF_EDOI().trim();
                DS_DECLARANTE = dataEntrada.getDS_DECLARANTE().trim();
                IT_PRIMERO = dataEntrada.getIT_PRIMERO().trim();
                CD_OPE = dataEntrada.getCD_OPE_NOTIF_EDOI().trim();
                //ARG: Ya no se usa "sysdate"
                //FC_ULTACT     = "sysdate";

                //st.setTimestamp(9, TSFec);
                FC_FECNOTIF = dataEntrada.getFC_FECNOTIF_NOTIF_EDOI().trim();
                CD_FUENTE = "E";

                // ARG: Se quitan los TO_DATE y el FC_ULTACT ("sysdate")
                /*ALTA =  "insert into SIVE_NOTIF_EDOI " +
                        "        ( NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, FC_RECEP, DS_DECLARANTE, IT_PRIMERO, CD_OPE,  FC_ULTACT, FC_FECNOTIF, CD_FUENTE )" +
                        " values ( " +
                        NM_EDO + "," +
                        "'" + CD_E_NOTIF + "'," +
                        "'" + CD_ANOEPI + "'," +
                        "'" + CD_SEMEPI + "'," +
                        "TO_DATE('" + FC_RECEP + "','DD/MM/YYYY'),"    +
                        "'" + DS_DECLARANTE + "'," +
                        "'" + IT_PRIMERO + "'," +
                        "'" + CD_OPE + "'," +
                        FC_ULTACT + ","    +
                        "TO_DATE('" + FC_FECNOTIF + "','DD/MM/YYYY'),"    +
                        "'" + CD_FUENTE + "'" +
                        ") "; */

                ALTA = "insert into SIVE_NOTIF_EDOI " +
                    "        ( NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, FC_RECEP, DS_DECLARANTE, IT_PRIMERO, CD_OPE,  FC_ULTACT, FC_FECNOTIF, CD_FUENTE )" +
                    " values ( " +
                    NM_EDO + "," +
                    "'" + CD_E_NOTIF + "'," +
                    "'" + CD_ANOEPI + "'," +
                    "'" + CD_SEMEPI + "'," +
                    " ? ," + // FC_RECEP
                    "'" + DS_DECLARANTE + "'," +
                    "'" + IT_PRIMERO + "'," +
                    "'" + CD_OPE + "'," +
                    " ? ," + // FC_ULTACT
                    " ? ," + // FC_FECNOTIF
                    "'" + CD_FUENTE + "'" +
                    ") ";

                st = con.prepareStatement(ALTA);

                // ARG
                st.setTimestamp(1, deString_ATimeStamp(FC_RECEP + " 00:00:00"));
                st.setTimestamp(2, TSFec);
                st.setTimestamp(3,
                                deString_ATimeStamp(FC_FECNOTIF + " 00:00:00"));

                st.executeUpdate();
                st.close();
                st = null;
              } //fin for ---lista con datos

              //cargar la lista de vuelta (con Cod_Enfermo, Nm_edo, sFecha_Actual)
              //
              dataSalida = new DataEnfCaso(CodEnf, sCaso, sFecha_Actual);
              listaSalida.addElement(dataSalida);
            } // fin del if (dataEntrada.getCD_TVIGI().equals("I") || !salirA)
            else {
              listaSalida = new CLista();
            }
          } //fin de if (param.size() != 0

          break;

/////////////////////////////////////////////////////////////////////////////
///////////////
/////////////////////////////////////////////////////////////////////////////
/////////////// ENFERMO MODIFICADO Y NUEVO CASO ( MODIF - ALTA )

        case servletCON_CD_ENFERMO_SIN_NM_EDO_MODIF_INSERT_INSERT:
          if (param.size() != 0) {

            dataEntrada = (DataTab) param.firstElement();

            // se hace una insercion en EDOIND
            // compruebo si la enfermedad es de tipo A y si es asi,
            // que el numero de individuales sea menor que el numero
            // total de casos en EDONUM

            boolean salirA = false;

            if (dataEntrada.getCD_TVIGI().equals("A")) {
              salirA = comprobarEnfermedadA(dataEntrada, con, st, rs);

              // el resto del proceso se hace para todas las I y para
              // las A que han superado la comprobacion
            }
            if (dataEntrada.getCD_TVIGI().equals("I") || !salirA) {

//!!!!!!ENFERMO!!!!!!!
//!!!!!!!!!!!!!!!!!!!!

              dataEntrada = (DataTab) param.firstElement();
              CodEnf = dataEntrada.getCD_ENFERMO().trim();

//PASAMOS A HISTORICOS EL ENFERMO??????
              if (dataEntrada.getIT_MOV_ENFERMO().equals("S")) {

//PASO A MOV:
//SELECT DEL MAX EN MOV_ENFERMO
//SELECT DEL ENFERMO
//INSERT EN MOV_ENFERMO

//select max de nm_movenf --------------------------------------------------

                // modificacion momentanea
                // se accede a la tabla sec_general a trav�s de esta funci�n
                // para obtener y aumentar el secuencial
                int iNM_MOVENF = 0;
                iNM_MOVENF = sapp.Funciones.SecGeneral(con, st, rs, "NM_MOVENF");
                sMaxEnfermo = Integer.toString(iNM_MOVENF);
                //String sMaxEnfermo = "";

                /*
                        st = con.prepareStatement(sMaxBajaEnfermo);
                        Integer NumMax = new Integer(999999);
                        st.setInt(1, NumMax.intValue());
                        rs = st.executeQuery();
                        if (rs.next()){
                          if (rs == null){
                            sMaxEnfermo = "1";
                          }
                          else{
                            int iC = rs.getInt(1) + 1;
                            Integer isMaxEnfermo = new Integer(iC);
                            sMaxEnfermo = isMaxEnfermo.toString();
                          }
                        }
                        rs.close();
                        rs=null;
                        st.close();
                        st=null;
                 */

//SELECT - INSERT -------------------------------

                st = con.prepareStatement(Pregunta_Select_Enfermo());
                st.setInt(1, new Integer(CodEnf.trim()).intValue());

                registroConsultas.insertarParametro(CodEnf.trim());
                registroConsultas.registrar("SIVE_ENFERMO",
                                            Pregunta_Select_Enfermo(),
                                            "CD_ENFERMO",
                                            CodEnf.trim(),
                                            "SrvIndivIM",
                                            true);

                rs = st.executeQuery();

                String FC_NAC = "";
                java.util.Date dFC_NAC;
                String FC_BAJA = "";
                java.util.Date dFC_BAJA;
                java.sql.Timestamp fecRecu = null;
                String sfecRecu = "";

                while (rs.next()) {
                  //campos criticos:-----------------------------------
                  dFC_NAC = rs.getDate("FC_NAC");
                  if (dFC_NAC == null) {
                    FC_NAC = "";
                  }
                  else {
                    FC_NAC = formater.format(dFC_NAC);

                  }

                  dFC_BAJA = rs.getDate("FC_BAJA");
                  if (dFC_BAJA == null) {
                    FC_BAJA = "";
                  }
                  else {
                    FC_BAJA = formater.format(dFC_BAJA);

                  }
                  fecRecu = rs.getTimestamp("FC_ULTACT");
                  sfecRecu = timestamp_a_cadena(fecRecu);
                  //---------------------------------------------------
                  cd_prov = rs.getString("CD_PROV");
                  if (cd_prov == null) {
                    cd_prov = "";

                  }

                  cd_pais = rs.getString("CD_PAIS");
                  if (cd_pais == null) {
                    cd_pais = "";

                  }
                  cd_mun = rs.getString("CD_MUN");
                  if (cd_mun == null) {
                    cd_mun = "";

                  }
                  cd_tdoc = rs.getString("CD_TDOC");
                  if (cd_tdoc == null) {
                    cd_tdoc = "";
                    /*
                               cd_via =  rs.getString ("CD_VIA");
                               if (  cd_via == null)
                      cd_via =  "";
                     */

                    //" DS_APE1,DS_APE2,DS_NOMBRE , DS_FONOAPE1, DS_FONOAPE2, "

                  }
                  ds_ape1 = rs.getString("DS_APE1");

                  ds_ape2 = rs.getString("DS_APE2");
                  if (ds_ape2 == null) {
                    ds_ape2 = "";

                  }
                  ds_nombre = rs.getString("DS_NOMBRE");
                  if (ds_nombre == null) {
                    ds_nombre = "";

                  }
                  ds_fonoape1 = rs.getString("DS_FONOAPE1");

                  ds_fonoape2 = rs.getString("DS_FONOAPE2");
                  if (ds_fonoape2 == null) {
                    ds_fonoape2 = "";

                    //DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL,

                  }
                  ds_fononombre = rs.getString("DS_FONONOMBRE");
                  if (ds_fononombre == null) {
                    ds_fononombre = "";

                  }
                  it_calc = rs.getString("IT_CALC");
                  if (it_calc == null) {
                    it_calc = "";

                  }
                  cd_sexo = rs.getString("CD_SEXO");
                  if (cd_sexo == null) {
                    cd_sexo = "";

                  }
                  cd_postal = rs.getString("CD_POSTAL");
                  if (cd_postal == null) {
                    cd_postal = "";

                    //DS_DIREC,DS_NUM,DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2,

                  }
                  ds_direc = rs.getString("DS_DIREC");
                  if (ds_direc == null) {
                    ds_direc = "";

                  }
                  ds_num = rs.getString("DS_NUM");
                  if (ds_num == null) {
                    ds_num = "";

                  }
                  ds_piso = rs.getString("DS_PISO");
                  if (ds_piso == null) {
                    ds_piso = "";

                  }
                  ds_telef = rs.getString("DS_TELEF");
                  if (ds_telef == null) {
                    ds_telef = "";

                  }
                  cd_nivel_1 = rs.getString("CD_NIVEL_1");
                  if (cd_nivel_1 == null) {
                    cd_nivel_1 = "";

                  }
                  cd_nivel_2 = rs.getString("CD_NIVEL_2");
                  if (cd_nivel_2 == null) {
                    cd_nivel_2 = "";

                    //CD_ZBS, CD_OPE, FC_ULTACT,IT_REVISADO, CD_MOTBAJA, FC_BAJA,

                  }
                  cd_zbs = rs.getString("CD_ZBS");
                  if (cd_zbs == null) {
                    cd_zbs = "";

                  }
                  cd_ope = rs.getString("CD_OPE");
                  /*fc_ultact =  formater.format(rs.getDate ("FC_ULTACT"));*/

                  fc_ultact = sfecRecu;
                  it_revisado = rs.getString("IT_REVISADO");

                  cd_motbaja = rs.getString("CD_MOTBAJA");
                  if (cd_motbaja == null) {
                    cd_motbaja = "";

                    //DS_OBSERV, CD_PROV2, CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2,

                  }
                  ds_observ = rs.getString("DS_OBSERV");
                  if (ds_observ == null) {
                    ds_observ = "";

                  }
                  cd_prov2 = rs.getString("CD_PROV2");
                  if (cd_prov2 == null) {
                    cd_prov2 = "";

                  }
                  cd_muni2 = rs.getString("CD_MUNI2");
                  if (cd_muni2 == null) {
                    cd_muni2 = "";

                  }
                  cd_post2 = rs.getString("CD_POST2");
                  if (cd_post2 == null) {
                    cd_post2 = "";

                  }
                  ds_direc2 = rs.getString("DS_DIREC2");
                  if (ds_direc2 == null) {
                    ds_direc2 = "";

                  }
                  ds_num2 = rs.getString("DS_NUM2");
                  if (ds_num2 == null) {
                    ds_num2 = "";

                    //" DS_PISO2, DS_TELEF2, DS_OBSERV2, "

                  }
                  ds_piso2 = rs.getString("DS_PISO2");
                  if (ds_piso2 == null) {
                    ds_piso2 = "";

                  }
                  ds_telef2 = rs.getString("DS_TELEF2");
                  if (ds_telef2 == null) {
                    ds_telef2 = "";

                  }
                  ds_observ2 = rs.getString("DS_OBSERV2");
                  if (ds_observ2 == null) {
                    ds_observ2 = "";

                    //" CD_PROV3,CD_MUNI3, CD_POST3, DS_DIREC3, "

                  }
                  cd_prov3 = rs.getString("CD_PROV3");
                  if (cd_prov3 == null) {
                    cd_prov3 = "";

                  }
                  cd_muni3 = rs.getString("CD_MUNI3");
                  if (cd_muni3 == null) {
                    cd_muni3 = "";

                  }
                  ds_direc3 = rs.getString("DS_DIREC3");
                  if (ds_direc3 == null) {
                    ds_direc3 = "";

                    // " DS_NUM3, DS_PISO3, DS_TELEF3, DS_OBSERV3,

                  }
                  ds_num3 = rs.getString("DS_NUM3");
                  if (ds_num3 == null) {
                    ds_num3 = "";

                  }
                  ds_piso3 = rs.getString("DS_PISO3");
                  if (ds_piso3 == null) {
                    ds_piso3 = "";

                  }
                  ds_telef3 = rs.getString("DS_TELEF3");
                  if (ds_telef3 == null) {
                    ds_telef3 = "";

                  }
                  ds_observ3 = rs.getString("DS_OBSERV3");
                  if (ds_observ3 == null) {
                    ds_observ3 = "";

                    //DS_NDOC, SIGLAS, CD_VIAL "

                  }
                  ds_ndoc = rs.getString("DS_NDOC");
                  if (ds_ndoc == null) {
                    ds_ndoc = "";

                  }
                  siglas = rs.getString("SIGLAS");
                  if (siglas == null) {
                    siglas = "";
                    /*
                             cd_vial =  rs.getString ("CD_VIAL");
                             if ( cd_vial == null)
                            cd_vial =  "";
                     */

                    // suca
                  }
                  cdvial = rs.getString("CDVIAL");
                  if (cdvial == null) {
                    cdvial = "";

                  }
                  cdtvia = rs.getString("CDTVIA");
                  if (cdtvia == null) {
                    cdtvia = "";

                  }
                  cdtnum = rs.getString("CDTNUM");
                  if (cdtnum == null) {
                    cdtnum = "";

                  }
                  dscalnum = rs.getString("DSCALNUM");
                  if (dscalnum == null) {
                    dscalnum = "";

                  }
                  dataMovEnfermo = new DataMovEnfermo(CodEnf,
                      cd_prov, cd_pais, cd_mun,
                      cd_tdoc,
                      ds_ape1, ds_ape2, ds_nombre, ds_fonoape1, ds_fonoape2,
                      ds_fononombre,
                      it_calc, FC_NAC, cd_sexo,
                      cd_postal, ds_direc, ds_num, ds_piso, ds_telef,
                      cd_nivel_1, cd_nivel_2, cd_zbs,
                      cd_ope, fc_ultact, it_revisado,
                      cd_motbaja, FC_BAJA, ds_ndoc, siglas,
                      ds_observ,
                      cd_prov2, cd_muni2, cd_post2, ds_direc2, ds_num2,
                      ds_piso2, ds_telef2, ds_observ2,
                      cd_prov3, cd_muni3, cd_post3,
                      ds_direc3, ds_num3, ds_piso3, ds_telef3, ds_observ3,
                      cdvial, cdtvia, cdtnum, dscalnum);

                } //fin del while
                rs.close();
                rs = null;
                st.close();
                st = null;

                //INSERT ******
                sQuery = Pregunta_Insert_MovEnfermo();
                st = con.prepareStatement(sQuery);

                st.setInt(1, (new Integer(sMaxEnfermo)).intValue());
                st.setInt(2,
                          (new Integer(dataMovEnfermo.getCD_ENFERMO().trim())).intValue());

                //CD_PROV, CD_PAIS, CD_MUN, CD_TDOC, CD_VIA,

                if (dataMovEnfermo.getCD_PROV().trim().length() > 0) {
                  st.setString(3, dataMovEnfermo.getCD_PROV().trim());
                }
                else {
                  st.setNull(3, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_PAIS().trim().length() > 0) {
                  st.setString(4, dataMovEnfermo.getCD_PAIS().trim());
                }
                else {
                  st.setNull(4, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_MUN().trim().length() > 0) {
                  st.setString(5, dataMovEnfermo.getCD_MUN().trim());
                }
                else {
                  st.setNull(5, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_TDOC().trim().length() > 0) {
                  st.setString(6, dataMovEnfermo.getCD_TDOC().trim());
                }
                else {
                  st.setNull(6, java.sql.Types.VARCHAR);

                  // vial de suca
                }
                if (dataMovEnfermo.getCDVIAL().trim().length() > 0) {
                  st.setString(7, dataMovEnfermo.getCDVIAL().trim());
                }
                else {
                  st.setNull(7, java.sql.Types.VARCHAR);

                  //DS_APE1, DS_APE2, DS_NOMBRE , DS_FONOAPE1, DS_FONOAPE2,

                }
                st.setString(8, dataMovEnfermo.getDS_APE1().trim());

                if (dataMovEnfermo.getDS_APE2().trim().length() > 0) {
                  st.setString(9, dataMovEnfermo.getDS_APE2().trim());
                }
                else {
                  st.setNull(9, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_NOMBRE().trim().length() > 0) {
                  st.setString(10, dataMovEnfermo.getDS_NOMBRE().trim());
                }
                else {
                  st.setNull(10, java.sql.Types.VARCHAR);

                }
                st.setString(11, dataMovEnfermo.getDS_FONOAPE1());

                if (dataMovEnfermo.getDS_FONOAPE2().length() > 0) {
                  st.setString(12, dataMovEnfermo.getDS_FONOAPE2());
                }
                else {
                  st.setNull(12, java.sql.Types.VARCHAR);

                  //DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL

                }
                if (dataMovEnfermo.getDS_FONONOMBRE().length() > 0) {
                  st.setString(13, dataMovEnfermo.getDS_FONONOMBRE());
                }
                else {
                  st.setNull(13, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getIT_CALC().trim().length() > 0) {
                  st.setString(14, dataMovEnfermo.getIT_CALC().trim());
                }
                else {
                  st.setNull(14, java.sql.Types.VARCHAR);
                  ////////////
                }
                if (dataMovEnfermo.getFC_NAC().trim().length() > 0) {
                  dFecha = formater.parse(dataMovEnfermo.getFC_NAC().trim());
                  sqlFec = new java.sql.Date(dFecha.getTime());
                  st.setDate(15, sqlFec);
                }
                else {
                  st.setNull(15, java.sql.Types.VARCHAR);
                  /////////

                }
                if (dataMovEnfermo.getCD_SEXO().trim().length() > 0) {
                  st.setString(16, dataMovEnfermo.getCD_SEXO().trim());
                }
                else {
                  st.setNull(16, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_POSTAL().trim().length() > 0) {
                  st.setString(17, dataMovEnfermo.getCD_POSTAL().trim());
                }
                else {
                  st.setNull(17, java.sql.Types.VARCHAR);

                  //DS_DIREC, DS_NUM, DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2,

                }
                if (dataMovEnfermo.getDS_DIREC().trim().length() > 0) {
                  st.setString(18, dataMovEnfermo.getDS_DIREC().trim());
                }
                else {
                  st.setNull(18, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_NUM().trim().length() > 0) {
                  st.setString(19, dataMovEnfermo.getDS_NUM().trim());
                }
                else {
                  st.setNull(19, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_PISO().trim().length() > 0) {
                  st.setString(20, dataMovEnfermo.getDS_PISO().trim());
                }
                else {
                  st.setNull(20, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_TELEF().trim().length() > 0) {
                  st.setString(21, dataMovEnfermo.getDS_TELEF().trim());
                }
                else {
                  st.setNull(21, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_NIVEL_1().trim().length() > 0) {
                  st.setString(22, dataMovEnfermo.getCD_NIVEL_1().trim());
                }
                else {
                  st.setNull(22, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_NIVEL_2().trim().length() > 0) {
                  st.setString(23, dataMovEnfermo.getCD_NIVEL_2().trim());
                }
                else {
                  st.setNull(23, java.sql.Types.VARCHAR);

                  //CD_ZBS, CD_OPE, FC_ULTACT, IT_REVISADO, CD_MOTBAJA, FC_BAJA,

                }
                if (dataMovEnfermo.getCD_ZBS().trim().length() > 0) {
                  st.setString(24, dataMovEnfermo.getCD_ZBS().trim());
                }
                else {
                  st.setNull(24, java.sql.Types.VARCHAR);

                }
                st.setString(25, dataMovEnfermo.getCD_OPE().trim());

                ////////////
                    /*dFecha = formater.parse(dataMovEnfermo.getFC_ULTACT().trim());
                            sqlFec = new java.sql.Date(dFecha.getTime());
                            st.setDate(26, sqlFec); */
                st.setTimestamp(26,
                                deString_ATimeStamp(dataMovEnfermo.getFC_ULTACT().
                    trim()));
                /////////

                st.setString(27, dataMovEnfermo.getIT_REVISADO().trim());

                if (dataMovEnfermo.getCD_MOTBAJA().trim().length() > 0) {
                  st.setString(28, dataMovEnfermo.getCD_MOTBAJA().trim());
                }
                else {
                  st.setNull(28, java.sql.Types.VARCHAR);

                  ////////////
                }
                if (dataMovEnfermo.getFC_BAJA().trim().length() > 0) {
                  dFecha = formater.parse(dataMovEnfermo.getFC_BAJA().trim());
                  sqlFec = new java.sql.Date(dFecha.getTime());
                  st.setDate(29, sqlFec);
                }
                else {
                  st.setNull(29, java.sql.Types.VARCHAR);
                  /////////

                  //DS_OBSERV, CD_PROV2, CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2,

                }
                if (dataMovEnfermo.getDS_OBSERV().trim().length() > 0) {
                  st.setString(30, dataMovEnfermo.getDS_OBSERV().trim());
                }
                else {
                  st.setNull(30, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_PROV2().trim().length() > 0) {
                  st.setString(31, dataMovEnfermo.getCD_PROV2().trim());
                }
                else {
                  st.setNull(31, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_MUNI2().trim().length() > 0) {
                  st.setString(32, dataMovEnfermo.getCD_MUNI2().trim());
                }
                else {
                  st.setNull(32, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_POST2().trim().length() > 0) {
                  st.setString(33, dataMovEnfermo.getCD_POST2().trim());
                }
                else {
                  st.setNull(33, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_DIREC2().trim().length() > 0) {
                  st.setString(34, dataMovEnfermo.getDS_DIREC2().trim());
                }
                else {
                  st.setNull(34, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_NUM2().trim().length() > 0) {
                  st.setString(35, dataMovEnfermo.getDS_NUM2().trim());
                }
                else {
                  st.setNull(35, java.sql.Types.VARCHAR);

                  //DS_PISO2, DS_TELEF2, DS_OBSERV2,
                }
                if (dataMovEnfermo.getDS_PISO2().trim().length() > 0) {
                  st.setString(36, dataMovEnfermo.getDS_PISO2().trim());
                }
                else {
                  st.setNull(36, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_TELEF2().trim().length() > 0) {
                  st.setString(37, dataMovEnfermo.getDS_TELEF2().trim());
                }
                else {
                  st.setNull(37, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_OBSERV2().trim().length() > 0) {
                  st.setString(38, dataMovEnfermo.getDS_OBSERV2().trim());
                }
                else {
                  st.setNull(38, java.sql.Types.VARCHAR);

                  //CD_PROV3, CD_MUNI3, CD_POST3, DS_DIREC3,
                }
                if (dataMovEnfermo.getCD_PROV3().trim().length() > 0) {
                  st.setString(39, dataMovEnfermo.getCD_PROV3().trim());
                }
                else {
                  st.setNull(39, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_MUNI3().trim().length() > 0) {
                  st.setString(40, dataMovEnfermo.getCD_MUNI3().trim());
                }
                else {
                  st.setNull(40, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCD_POST3().trim().length() > 0) {
                  st.setString(41, dataMovEnfermo.getCD_POST3().trim());
                }
                else {
                  st.setNull(41, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_DIREC3().trim().length() > 0) {
                  st.setString(42, dataMovEnfermo.getDS_DIREC3().trim());
                }
                else {
                  st.setNull(42, java.sql.Types.VARCHAR);

                  //DS_NUM3, DS_PISO3, DS_TELEF3, DS_OBSERV3

                }
                if (dataMovEnfermo.getDS_NUM3().trim().length() > 0) {
                  st.setString(43, dataMovEnfermo.getDS_NUM3().trim());
                }
                else {
                  st.setNull(43, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_PISO3().trim().length() > 0) {
                  st.setString(44, dataMovEnfermo.getDS_PISO3().trim());
                }
                else {
                  st.setNull(44, java.sql.Types.VARCHAR);
                }
                if (dataMovEnfermo.getDS_TELEF3().trim().length() > 0) {
                  st.setString(45, dataMovEnfermo.getDS_TELEF3().trim());
                }
                else {
                  st.setNull(45, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDS_OBSERV3().trim().length() > 0) {
                  st.setString(46, dataMovEnfermo.getDS_OBSERV3().trim());
                }
                else {
                  st.setNull(46, java.sql.Types.VARCHAR);

                  //DS_NDOC, SIGLAS

                }
                if (dataMovEnfermo.getDS_NDOC().trim().length() > 0) {
                  st.setString(47, dataMovEnfermo.getDS_NDOC().trim());
                }
                else {
                  st.setNull(47, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getSIGLAS().trim().length() > 0) {
                  st.setString(48, dataMovEnfermo.getSIGLAS().trim());
                }
                else {
                  st.setNull(48, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCDTVIA().trim().length() > 0) {
                  st.setString(49, dataMovEnfermo.getCDTVIA().trim());
                }
                else {
                  st.setNull(49, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getCDTNUM().trim().length() > 0) {
                  st.setString(50, dataMovEnfermo.getCDTNUM().trim());
                }
                else {
                  st.setNull(50, java.sql.Types.VARCHAR);

                }
                if (dataMovEnfermo.getDSCALNUM().trim().length() > 0) {
                  st.setString(51, dataMovEnfermo.getDSCALNUM().trim());
                }
                else {
                  st.setNull(51, java.sql.Types.VARCHAR);

                }
                st.setString(52, "S"); // modificacion 11/05 para la integridad

                st.setString(53, "N");

                st.executeUpdate();
                st.close();
                st = null;

//------------------------------------------------
////********* MODIFICACION ******
                sQuery = Pregunta_Modifica_Enfermo();
                st = con.prepareStatement(sQuery);

                if (dataEntrada.getCD_TDOC().trim().length() > 0) {
                  st.setString(1, dataEntrada.getCD_TDOC().trim());
                }
                else {
                  st.setNull(1, java.sql.Types.VARCHAR);

                  //" *DS_APE1,DS_APE2,DS_NOMBRE , *DS_FONOAPE1, DS_FONOAPE2,
                }
                st.setString(2, dataEntrada.getDS_APE1().trim());

                if (dataEntrada.getDS_APE2().trim().length() > 0) {
                  st.setString(3, dataEntrada.getDS_APE2().trim());
                }
                else {
                  st.setNull(3, java.sql.Types.VARCHAR);

                }
                if (dataEntrada.getDS_NOMBRE().trim().length() > 0) {
                  st.setString(4, dataEntrada.getDS_NOMBRE().trim());
                }
                else {
                  st.setNull(4, java.sql.Types.VARCHAR);

                }
                st.setString(5, dataEntrada.getDS_FONOAPE1());

                if (dataEntrada.getDS_FONOAPE2().length() > 0) {
                  st.setString(6, dataEntrada.getDS_FONOAPE2());
                }
                else {
                  st.setNull(6, java.sql.Types.VARCHAR);

                  // DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO

                }
                if (dataEntrada.getDS_FONONOMBRE().length() > 0) {
                  st.setString(7, dataEntrada.getDS_FONONOMBRE());
                }
                else {
                  st.setNull(7, java.sql.Types.VARCHAR);

                }
                if (dataEntrada.getIT_CALC().trim().length() > 0) {
                  st.setString(8, dataEntrada.getIT_CALC().trim());
                }
                else {
                  st.setNull(8, java.sql.Types.VARCHAR);

                  ////////////
                }
                if (dataEntrada.getFC_NAC().trim().length() > 0) {
                  dFecha = formater.parse(dataEntrada.getFC_NAC().trim());
                  sqlFec = new java.sql.Date(dFecha.getTime());
                  st.setDate(9, sqlFec);
                }
                else {
                  st.setNull(9, java.sql.Types.VARCHAR);
                  /////////

                }
                if (dataEntrada.getCD_SEXO().trim().length() > 0) {
                  st.setString(10, dataEntrada.getCD_SEXO().trim());
                }
                else {
                  st.setNull(10, java.sql.Types.VARCHAR);

                  //  DS_TELEF,

                }
                if (dataEntrada.getDS_TELEF().trim().length() > 0) {
                  st.setString(11, dataEntrada.getDS_TELEF().trim());
                }
                else {
                  st.setNull(11, java.sql.Types.VARCHAR);

                  //  *CD_OPE, *FC_ULTACT

                }
                st.setString(12, dataEntrada.getCD_OPE().trim());
                ////////////
                /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                            sqlFec = new java.sql.Date(dFecha.getTime());
                            st.setDate(13, sqlFec);*/
                st.setTimestamp(13, TSFec);
                /////////

                // DS_NDOC,SIGLAS

                if (dataEntrada.getDS_NDOC().trim().length() > 0) {
                  st.setString(14, dataEntrada.getDS_NDOC().trim());
                }
                else {
                  st.setNull(14, java.sql.Types.VARCHAR);

                }
                if (dataEntrada.getSIGLAS().trim().length() > 0) {
                  st.setString(15, dataEntrada.getSIGLAS().trim());
                }
                else {
                  st.setNull(15, java.sql.Types.VARCHAR);

                  //WHERE
                }
                st.setInt(16, new Integer(CodEnf.trim()).intValue());

                st.executeUpdate();
                st.close();
                st = null;

              } //fin de si hay que modificar enfermo y pasarlo a historico

              //modificacion momentane

              int iNM_EDO = 0;
              iNM_EDO = sapp.Funciones.SecGeneral(con, st, rs, "NM_EDO");
              sCaso = Integer.toString(iNM_EDO);
              /*
                      //select max de nm_edo ---------------------------
                      st = con.prepareStatement(queryPrevia);
                      Integer NumMaxedo = new Integer(999999);
                      st.setInt(1, NumMaxedo.intValue());
                      rs = st.executeQuery();
                      if (rs.next()){
                        if (rs == null){
                          sCaso = "1";
                        }
                        else{
                          int iC = rs.getInt(1) + 1;
                          Integer isCaso = new Integer(iC);
                          sCaso = isCaso.toString();
                        }
                      }
                      rs.close();
                      rs=null;
                      st.close();
                      st=null;
               */

//!!!! EDOIND  !!!!!!!!!!!!!

              //Leemos del primer REGISTRO -----------------------------
              dataEntrada = (DataTab) param.firstElement(); //repetitivo

              ALTA = Pregunta_Alta_Edoind();
              st = con.prepareStatement(ALTA);

              st.setInt(1, new Integer(sCaso.trim()).intValue());
              st.setString(2, dataEntrada.getCD_ANOEPI().trim());
              st.setString(3, dataEntrada.getCD_SEMEPI().trim());

              if (dataEntrada.getCD_ANOURG().trim().length() > 0) {
                st.setString(4, dataEntrada.getCD_ANOURG().trim());
              }
              else {
                st.setNull(4, java.sql.Types.VARCHAR);

              }

              if (dataEntrada.getNM_ENVIOURGSEM().trim().length() > 0) {
                st.setInt(5,
                          new Integer(dataEntrada.getNM_ENVIOURGSEM().trim()).intValue());
              }
              else {
                st.setNull(5, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_CLASIFDIAG().trim().length() > 0) {
                st.setString(6, dataEntrada.getCD_CLASIFDIAG().trim());
              }
              else {
                st.setNull(6, java.sql.Types.VARCHAR);

                //+ " IT_PENVURG, *CD_ENFERMO, CD_PROV,CD_MUN, " //4
              }
              if (dataEntrada.getIT_PENVURG().trim().length() > 0) {
                st.setString(7, dataEntrada.getIT_PENVURG().trim());
              }
              else {
                st.setNull(7, java.sql.Types.VARCHAR);

              }
              st.setInt(8, new Integer(CodEnf.trim()).intValue());

              if (dataEntrada.getCD_PROV_EDOIND().trim().length() > 0) {
                st.setString(9, dataEntrada.getCD_PROV_EDOIND().trim());
              }
              else {
                st.setNull(9, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_MUN_EDOIND().trim().length() > 0) {
                st.setString(10, dataEntrada.getCD_MUN_EDOIND().trim());
              }
              else {
                st.setNull(10, java.sql.Types.VARCHAR);

                // + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, " //5

              }

              if (dataEntrada.getCD_NIVEL_1_EDOIND().trim().length() > 0) {
                st.setString(11, dataEntrada.getCD_NIVEL_1_EDOIND().trim());
              }
              else {
                st.setNull(11, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_NIVEL_2_EDOIND().trim().length() > 0) {
                st.setString(12, dataEntrada.getCD_NIVEL_2_EDOIND().trim());
              }
              else {
                st.setNull(12, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_ZBS_EDOIND().trim().length() > 0) {
                st.setString(13, dataEntrada.getCD_ZBS_EDOIND().trim());
              }
              else {
                st.setNull(13, java.sql.Types.VARCHAR);

                //fecha OBLIGATORIA
              }
              dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(14, sqlFec);

              if (dataEntrada.getDS_CALLE().trim().length() > 0) {
                st.setString(15, dataEntrada.getDS_CALLE().trim());
              }
              else {
                st.setNull(15, java.sql.Types.VARCHAR);

                // + " DS_NMCALLE, DS_PISO, CD_POSTAL, *CD_OPE, *FC_ULTACT, "  //5
              }
              if (dataEntrada.getDS_NMCALLE().trim().length() > 0) {
                st.setString(16, dataEntrada.getDS_NMCALLE().trim());
              }
              else {
                st.setNull(16, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_PISO_EDOIND().trim().length() > 0) {
                st.setString(17, dataEntrada.getDS_PISO_EDOIND().trim());
              }
              else {
                st.setNull(17, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCD_POSTAL_EDOIND().trim().length() > 0) {
                st.setString(18, dataEntrada.getCD_POSTAL_EDOIND().trim());
              }
              else {
                st.setNull(18, java.sql.Types.VARCHAR);

              }
              st.setString(19, dataEntrada.getCD_OPE_EDOIND().trim());

                  /*dFecha = formater.parse(dataEntrada.getFC_ULTACT_EDOIND().trim());
                        sqlFec = new java.sql.Date(dFecha.getTime());
                        st.setDate(21, sqlFec);*/
              st.setTimestamp(20, TSFec);

              //  + " CD_ANOOTRC, NM_ENVOTRC, *CD_ENFCIE "  //3
              if (dataEntrada.getCD_ANOOTRC().trim().length() > 0) {
                st.setString(21, dataEntrada.getCD_ANOOTRC().trim());
              }
              else {
                st.setNull(21, java.sql.Types.VARCHAR);

              }

              if (dataEntrada.getNM_ENVOTRC().trim().length() > 0) {
                st.setInt(22,
                          new Integer(dataEntrada.getNM_ENVOTRC().trim()).intValue());
              }
              else {
                st.setNull(22, java.sql.Types.VARCHAR);

              }

              st.setString(23, dataEntrada.getCD_ENFCIE().trim());

              //    + " *FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, "// 3+
              //+ " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, "// 3+
              //+ " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO ) "// 3+

              dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(24, sqlFec);

              if (dataEntrada.getIT_DERIVADO().trim().length() > 0) {
                st.setString(25, dataEntrada.getIT_DERIVADO().trim());
              }
              else {
                st.setNull(25, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_CENTRODER().trim().length() > 0) {
                st.setString(26, dataEntrada.getDS_CENTRODER().trim());
              }
              else {
                st.setNull(26, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getIT_DIAGCLI().trim().length() > 0) {
                st.setString(27, dataEntrada.getIT_DIAGCLI().trim());
              }
              else {
                st.setNull(27, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getFC_INISNT().trim().length() > 0) {
                dFecha = formater.parse(dataEntrada.getFC_INISNT().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(28, sqlFec);
              }
              else {
                st.setNull(28, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_COLECTIVO().trim().length() > 0) {
                st.setString(29, dataEntrada.getDS_COLECTIVO().trim());
              }
              else {
                st.setNull(29, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getIT_ASOCIADO().trim().length() > 0) {
                st.setString(30, dataEntrada.getIT_ASOCIADO().trim());
              }
              else {
                st.setNull(30, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getIT_DIAGMICRO().trim().length() > 0) {
                st.setString(31, dataEntrada.getIT_DIAGMICRO().trim());
              }
              else {
                st.setNull(31, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_ASOCIADO().trim().length() > 0) {
                st.setString(32, dataEntrada.getDS_ASOCIADO().trim());
              }
              else {
                st.setNull(32, java.sql.Types.VARCHAR);
                /* SUCA
                     if (dataEntrada.getCD_VIA_EDOIND().trim() .length() > 0)
                  st.setString (34, dataEntrada.getCD_VIA_EDOIND().trim());
                          else
                    st.setNull(34, java.sql.Types.VARCHAR);
                     if (dataEntrada.getCD_VIAL_EDOIND().trim() .length() > 0)
                  st.setString (35, dataEntrada.getCD_VIAL_EDOIND().trim());
                          else
                    st.setNull(35, java.sql.Types.VARCHAR);
                 */
              }
              if (dataEntrada.getIT_DIAGSERO().trim().length() > 0) {
                st.setString(33, dataEntrada.getIT_DIAGSERO().trim());
              }
              else {
                st.setNull(33, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_DIAGOTROS().trim().length() > 0) {
                st.setString(34, dataEntrada.getDS_DIAGOTROS().trim());
              }
              else {
                st.setNull(34, java.sql.Types.VARCHAR);

                // SUCA
              }
              if (dataEntrada.getCDVIAL().trim().length() > 0) {
                st.setString(35, dataEntrada.getCDVIAL().trim());
              }
              else {
                st.setNull(35, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCDTVIA().trim().length() > 0) {
                st.setString(36, dataEntrada.getCDTVIA().trim());
              }
              else {
                st.setNull(36, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getCDTNUM().trim().length() > 0) {
                st.setString(37, dataEntrada.getCDTNUM().trim());
              }
              else {
                st.setNull(37, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDSCALNUM().trim().length() > 0) {
                st.setString(38, dataEntrada.getDSCALNUM().trim());
              }
              else {
                st.setNull(38, java.sql.Types.VARCHAR);

              }
              st.executeUpdate();
              st.close();
              st = null;

//!!!! NOTIF EDO!!!!!!!!!!!!!!!!!!!!!!
              //recorremos la lista de entrada. Tantas altas como declarantes haya.
              for (i = 0; i < param.size(); i++) {
                dataEntrada = (DataTab) param.elementAt(i);

                ALTA = Pregunta_Alta_Notif_Edoi();
                st = con.prepareStatement(ALTA);

                st.setInt(1, new Integer(sCaso.trim()).intValue());
                st.setString(2, dataEntrada.getCD_E_NOTIF().trim());
                st.setString(3, dataEntrada.getCD_ANOEPI_NOTIF_EDOI().trim());
                st.setString(4, dataEntrada.getCD_SEMEPI_NOTIF_EDOI().trim());

                dFecha = formater.parse(dataEntrada.getFC_RECEP_NOTIF_EDOI().
                                        trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(5, sqlFec);

                st.setString(6, dataEntrada.getDS_DECLARANTE().trim());

                st.setString(7, dataEntrada.getIT_PRIMERO().trim());

                st.setString(8, dataEntrada.getCD_OPE_NOTIF_EDOI().trim());

                    /*dFecha = formater.parse(dataEntrada.getFC_ULTACT_NOTIF_EDOI().trim());
                           sqlFec = new java.sql.Date(dFecha.getTime());
                           st.setDate(9, sqlFec);*/
                st.setTimestamp(9, TSFec);

                dFecha = formater.parse(dataEntrada.getFC_FECNOTIF_NOTIF_EDOI().
                                        trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(10, sqlFec);

                st.setString(11, "E");

                st.executeUpdate();
                st.close();
                st = null;
              } //fin for ---lista con datos

              //cargar la lista de vuelta (con Cod_Enfermo, Nm_edo, sFecha_Actual)
              dataSalida = new DataEnfCaso(CodEnf, sCaso, sFecha_Actual);
              listaSalida.addElement(dataSalida);

            } //if (dataEntrada.getCD_TVIGI().equals("I") || !salirA){
            else {
              listaSalida = new CLista();
            }

          } //fin de if (param.size() != 0
          break;
          /////////////////////////////////////////////////////////////////////////////
///////////////
/////////////////////////////////////////////////////////////////////////////
/////////////// ENFERMO MODIFICADO Y CASO MODIFICADO ( MODIF - MODIF )

        case servletCON_CD_ENFERMO_CON_NM_EDO_MODIF_MODIF_BUCLE:

          if (param.size() != 0) {

            dataEntrada = (DataTab) param.firstElement();
            CodEnf = dataEntrada.getCD_ENFERMO().trim();
            sCaso = dataEntrada.getNM_EDO().trim();

//PASAMOS A HISTORICOS EL ENFERMO??????
            if (dataEntrada.getIT_MOV_ENFERMO().equals("S")) {

//PASO A MOV:
//SELECT DEL MAX EN MOV_ENFERMO
//SELECT DEL ENFERMO
//INSERT EN MOV_ENFERMO

//select max de nm_movenf --------------------------------------------------

              // modificacion momentanea
              int iNM_MOVENF = 0;
              iNM_MOVENF = sapp.Funciones.SecGeneral(con, st, rs, "NM_MOVENF");
              sMaxEnfermo = Integer.toString(iNM_MOVENF);

              /*        String sMaxEnfermo = "";
                      st = con.prepareStatement(sMaxBajaEnfermo);
                      Integer NumMax = new Integer(999999);
                      st.setInt(1, NumMax.intValue());
                      rs = st.executeQuery();
                      if (rs.next()){
                        if (rs == null){
                          sMaxEnfermo = "1";
                        }
                        else{
                          int iC = rs.getInt(1) + 1;
                          Integer isMaxEnfermo = new Integer(iC);
                          sMaxEnfermo = isMaxEnfermo.toString();
                        }
                      }
                      rs.close();
                      rs=null;
                      st.close();
                      st=null;
               */

//SELECT - INSERT -------------------------------

              st = con.prepareStatement(Pregunta_Select_Enfermo());
              st.setInt(1, new Integer(CodEnf.trim()).intValue());
              rs = st.executeQuery();

              String FC_NAC = "";
              java.util.Date dFC_NAC;
              String FC_BAJA = "";
              java.util.Date dFC_BAJA;
              java.sql.Timestamp fecRecu = null;
              String sfecRecu = "";
              while (rs.next()) {
                //campos criticos:-----------------------------------
                dFC_NAC = rs.getDate("FC_NAC");
                if (dFC_NAC == null) {
                  FC_NAC = "";
                }
                else {
                  FC_NAC = formater.format(dFC_NAC);

                }

                dFC_BAJA = rs.getDate("FC_BAJA");
                if (dFC_BAJA == null) {
                  FC_BAJA = "";
                }
                else {
                  FC_BAJA = formater.format(dFC_BAJA);

                }
                fecRecu = rs.getTimestamp("FC_ULTACT");
                sfecRecu = timestamp_a_cadena(fecRecu);
                //---------------------------------------------------
                cd_prov = rs.getString("CD_PROV");
                if (cd_prov == null) {
                  cd_prov = "";

                }
                cd_pais = rs.getString("CD_PAIS");
                if (cd_pais == null) {
                  cd_pais = "";

                }
                cd_mun = rs.getString("CD_MUN");
                if (cd_mun == null) {
                  cd_mun = "";

                }
                cd_tdoc = rs.getString("CD_TDOC");
                if (cd_tdoc == null) {
                  cd_tdoc = "";
                  /*
                             cd_via =  rs.getString ("CD_VIA");
                             if (  cd_via == null)
                    cd_via =  "";
                   */
                  // " DS_APE1,DS_APE2,DS_NOMBRE , DS_FONOAPE1, DS_FONOAPE2, "
                }
                ds_ape1 = rs.getString("DS_APE1");

                ds_ape2 = rs.getString("DS_APE2");
                if (ds_ape2 == null) {
                  ds_ape2 = "";

                }
                ds_nombre = rs.getString("DS_NOMBRE");
                if (ds_nombre == null) {
                  ds_nombre = "";

                }
                ds_fonoape1 = rs.getString("DS_FONOAPE1");

                ds_fonoape2 = rs.getString("DS_FONOAPE2");
                if (ds_fonoape2 == null) {
                  ds_fonoape2 = "";

                  //  DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL,
                }
                ds_fononombre = rs.getString("DS_FONONOMBRE");
                if (ds_fononombre == null) {
                  ds_fononombre = "";

                }
                it_calc = rs.getString("IT_CALC");
                if (it_calc == null) {
                  it_calc = "";

                }
                cd_sexo = rs.getString("CD_SEXO");
                if (cd_sexo == null) {
                  cd_sexo = "";

                }
                cd_postal = rs.getString("CD_POSTAL");
                if (cd_postal == null) {
                  cd_postal = "";

                  // DS_DIREC,DS_NUM,DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2,
                }
                ds_direc = rs.getString("DS_DIREC");
                if (ds_direc == null) {
                  ds_direc = "";

                }
                ds_num = rs.getString("DS_NUM");
                if (ds_num == null) {
                  ds_num = "";

                }
                ds_piso = rs.getString("DS_PISO");
                if (ds_piso == null) {
                  ds_piso = "";

                }
                ds_telef = rs.getString("DS_TELEF");
                if (ds_telef == null) {
                  ds_telef = "";

                }
                cd_nivel_1 = rs.getString("CD_NIVEL_1");
                if (cd_nivel_1 == null) {
                  cd_nivel_1 = "";

                }
                cd_nivel_2 = rs.getString("CD_NIVEL_2");
                if (cd_nivel_2 == null) {
                  cd_nivel_2 = "";

                  // CD_ZBS, CD_OPE, FC_ULTACT,IT_REVISADO, CD_MOTBAJA, FC_BAJA,
                }
                cd_zbs = rs.getString("CD_ZBS");
                if (cd_zbs == null) {
                  cd_zbs = "";

                }
                cd_ope = rs.getString("CD_OPE");
                /*fc_ultact =  formater.format(rs.getDate ("FC_ULTACT"));*/
                fc_ultact = sfecRecu;
                it_revisado = rs.getString("IT_REVISADO");

                cd_motbaja = rs.getString("CD_MOTBAJA");
                if (cd_motbaja == null) {
                  cd_motbaja = "";

                  //DS_OBSERV, CD_PROV2, CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2,
                }
                ds_observ = rs.getString("DS_OBSERV");
                if (ds_observ == null) {
                  ds_observ = "";

                }
                cd_prov2 = rs.getString("CD_PROV2");
                if (cd_prov2 == null) {
                  cd_prov2 = "";

                }
                cd_muni2 = rs.getString("CD_MUNI2");
                if (cd_muni2 == null) {
                  cd_muni2 = "";

                }
                cd_post2 = rs.getString("CD_POST2");
                if (cd_post2 == null) {
                  cd_post2 = "";

                }
                ds_direc2 = rs.getString("DS_DIREC2");
                if (ds_direc2 == null) {
                  ds_direc2 = "";

                }
                ds_num2 = rs.getString("DS_NUM2");
                if (ds_num2 == null) {
                  ds_num2 = "";

                  //   + " DS_PISO2, DS_TELEF2, DS_OBSERV2, "
                }
                ds_piso2 = rs.getString("DS_PISO2");
                if (ds_piso2 == null) {
                  ds_piso2 = "";

                }
                ds_telef2 = rs.getString("DS_TELEF2");
                if (ds_telef2 == null) {
                  ds_telef2 = "";

                }
                ds_observ2 = rs.getString("DS_OBSERV2");
                if (ds_observ2 == null) {
                  ds_observ2 = "";

                  // " CD_PROV3,CD_MUNI3, CD_POST3, DS_DIREC3, "
                }
                cd_prov3 = rs.getString("CD_PROV3");
                if (cd_prov3 == null) {
                  cd_prov3 = "";

                }
                cd_muni3 = rs.getString("CD_MUNI3");
                if (cd_muni3 == null) {
                  cd_muni3 = "";

                }
                ds_direc3 = rs.getString("DS_DIREC3");
                if (ds_direc3 == null) {
                  ds_direc3 = "";

                  //  + " DS_NUM3, DS_PISO3, DS_TELEF3, DS_OBSERV3, "
                }
                ds_num3 = rs.getString("DS_NUM3");
                if (ds_num3 == null) {
                  ds_num3 = "";

                }
                ds_piso3 = rs.getString("DS_PISO3");
                if (ds_piso3 == null) {
                  ds_piso3 = "";

                }
                ds_telef3 = rs.getString("DS_TELEF3");
                if (ds_telef3 == null) {
                  ds_telef3 = "";

                }
                ds_observ3 = rs.getString("DS_OBSERV3");
                if (ds_observ3 == null) {
                  ds_observ3 = "";

                  //" DS_NDOC, SIGLAS, CD_VIAL "
                }
                ds_ndoc = rs.getString("DS_NDOC");
                if (ds_ndoc == null) {
                  ds_ndoc = "";

                }
                siglas = rs.getString("SIGLAS");
                if (siglas == null) {
                  siglas = "";
                  /*
                           cd_vial =  rs.getString ("CD_VIAL");
                           if ( cd_vial == null)
                          cd_vial =  "";
                   */

                  // suca
                }
                cdvial = rs.getString("CDVIAL");
                if (cdvial == null) {
                  cdvial = "";

                }
                cdtvia = rs.getString("CDTVIA");
                if (cdtvia == null) {
                  cdtvia = "";

                }
                cdtnum = rs.getString("CDTNUM");
                if (cdtnum == null) {
                  cdtnum = "";

                }
                dscalnum = rs.getString("DSCALNUM");
                if (dscalnum == null) {
                  dscalnum = "";

                }
                dataMovEnfermo = new DataMovEnfermo(CodEnf,
                    cd_prov, cd_pais, cd_mun,
                    cd_tdoc,
                    ds_ape1, ds_ape2, ds_nombre, ds_fonoape1, ds_fonoape2,
                    ds_fononombre,
                    it_calc, FC_NAC, cd_sexo,
                    cd_postal, ds_direc, ds_num, ds_piso, ds_telef,
                    cd_nivel_1, cd_nivel_2, cd_zbs,
                    cd_ope, fc_ultact, it_revisado,
                    cd_motbaja, FC_BAJA, ds_ndoc, siglas,
                    ds_observ,
                    cd_prov2, cd_muni2, cd_post2, ds_direc2, ds_num2,
                    ds_piso2, ds_telef2, ds_observ2,
                    cd_prov3, cd_muni3, cd_post3,
                    ds_direc3, ds_num3, ds_piso3, ds_telef3, ds_observ3,
                    cdvial, cdtvia, cdtnum, dscalnum);

              } //fin del while
              rs.close();
              rs = null;
              st.close();
              st = null;

//INSERT ******
              sQuery = Pregunta_Insert_MovEnfermo();
              st = con.prepareStatement(sQuery);

              st.setInt(1, (new Integer(sMaxEnfermo)).intValue());
              st.setInt(2,
                        (new Integer(dataMovEnfermo.getCD_ENFERMO().trim())).intValue());

              // + " CD_PROV, CD_PAIS, CD_MUN, CD_TDOC, CD_VIA, "
              if (dataMovEnfermo.getCD_PROV().trim().length() > 0) {
                st.setString(3, dataMovEnfermo.getCD_PROV().trim());
              }
              else {
                st.setNull(3, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_PAIS().trim().length() > 0) {
                st.setString(4, dataMovEnfermo.getCD_PAIS().trim());
              }
              else {
                st.setNull(4, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_MUN().trim().length() > 0) {
                st.setString(5, dataMovEnfermo.getCD_MUN().trim());
              }
              else {
                st.setNull(5, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_TDOC().trim().length() > 0) {
                st.setString(6, dataMovEnfermo.getCD_TDOC().trim());
              }
              else {
                st.setNull(6, java.sql.Types.VARCHAR);

                // SUCA
              }
              if (dataMovEnfermo.getCDVIAL().trim().length() > 0) {
                st.setString(7, dataMovEnfermo.getCDVIAL().trim());
              }
              else {
                st.setNull(7, java.sql.Types.VARCHAR);

                //DS_APE1, DS_APE2, DS_NOMBRE , DS_FONOAPE1, DS_FONOAPE2,
              }
              st.setString(8, dataMovEnfermo.getDS_APE1().trim());

              if (dataMovEnfermo.getDS_APE2().trim().length() > 0) {
                st.setString(9, dataMovEnfermo.getDS_APE2().trim());
              }
              else {
                st.setNull(9, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_NOMBRE().trim().length() > 0) {
                st.setString(10, dataMovEnfermo.getDS_NOMBRE().trim());
              }
              else {
                st.setNull(10, java.sql.Types.VARCHAR);

              }
              st.setString(11, dataMovEnfermo.getDS_FONOAPE1().trim());

              if (dataMovEnfermo.getDS_FONOAPE2().trim().length() > 0) {
                st.setString(12, dataMovEnfermo.getDS_FONOAPE2().trim());
              }
              else {
                st.setNull(12, java.sql.Types.VARCHAR);

                //  DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL,

              }
              if (dataMovEnfermo.getDS_FONONOMBRE().trim().length() > 0) {
                st.setString(13, dataMovEnfermo.getDS_FONONOMBRE().trim());
              }
              else {
                st.setNull(13, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getIT_CALC().trim().length() > 0) {
                st.setString(14, dataMovEnfermo.getIT_CALC().trim());
              }
              else {
                st.setNull(14, java.sql.Types.VARCHAR);

                ////////////
              }
              if (dataMovEnfermo.getFC_NAC().trim().length() > 0) {
                dFecha = formater.parse(dataMovEnfermo.getFC_NAC().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(15, sqlFec);
              }
              else {
                st.setNull(15, java.sql.Types.VARCHAR);
                /////////

              }
              if (dataMovEnfermo.getCD_SEXO().trim().length() > 0) {
                st.setString(16, dataMovEnfermo.getCD_SEXO().trim());
              }
              else {
                st.setNull(16, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_POSTAL().trim().length() > 0) {
                st.setString(17, dataMovEnfermo.getCD_POSTAL().trim());
              }
              else {
                st.setNull(17, java.sql.Types.VARCHAR);

                // DS_DIREC, DS_NUM, DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2,
              }
              if (dataMovEnfermo.getDS_DIREC().trim().length() > 0) {
                st.setString(18, dataMovEnfermo.getDS_DIREC().trim());
              }
              else {
                st.setNull(18, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_NUM().trim().length() > 0) {
                st.setString(19, dataMovEnfermo.getDS_NUM().trim());
              }
              else {
                st.setNull(19, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_PISO().trim().length() > 0) {
                st.setString(20, dataMovEnfermo.getDS_PISO().trim());
              }
              else {
                st.setNull(20, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_TELEF().trim().length() > 0) {
                st.setString(21, dataMovEnfermo.getDS_TELEF().trim());
              }
              else {
                st.setNull(21, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_NIVEL_1().trim().length() > 0) {
                st.setString(22, dataMovEnfermo.getCD_NIVEL_1().trim());
              }
              else {
                st.setNull(22, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_NIVEL_2().trim().length() > 0) {
                st.setString(23, dataMovEnfermo.getCD_NIVEL_2().trim());
              }
              else {
                st.setNull(23, java.sql.Types.VARCHAR);
                // CD_ZBS, CD_OPE, FC_ULTACT, IT_REVISADO, CD_MOTBAJA, FC_BAJA,
              }
              if (dataMovEnfermo.getCD_ZBS().trim().length() > 0) {
                st.setString(24, dataMovEnfermo.getCD_ZBS().trim());
              }
              else {
                st.setNull(24, java.sql.Types.VARCHAR);

              }
              st.setString(25, dataMovEnfermo.getCD_OPE().trim());

              ////////////
              /*dFecha = formater.parse(dataMovEnfermo.getFC_ULTACT().trim());
                          sqlFec = new java.sql.Date(dFecha.getTime());
                          st.setDate(26, sqlFec);*/
              st.setTimestamp(26,
                              deString_ATimeStamp(dataMovEnfermo.getFC_ULTACT().
                                                  trim()));
              /////////
              st.setString(27, dataMovEnfermo.getIT_REVISADO().trim());

              if (dataMovEnfermo.getCD_MOTBAJA().trim().length() > 0) {
                st.setString(28, dataMovEnfermo.getCD_MOTBAJA().trim());
              }
              else {
                st.setNull(28, java.sql.Types.VARCHAR);

                ////////////
              }
              if (dataMovEnfermo.getFC_BAJA().trim().length() > 0) {
                dFecha = formater.parse(dataMovEnfermo.getFC_BAJA().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(29, sqlFec);
              }
              else {
                st.setNull(29, java.sql.Types.VARCHAR);
                /////////

                //   DS_OBSERV, CD_PROV2, CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2,
              }
              if (dataMovEnfermo.getDS_OBSERV().trim().length() > 0) {
                st.setString(30, dataMovEnfermo.getDS_OBSERV().trim());
              }
              else {
                st.setNull(30, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_PROV2().trim().length() > 0) {
                st.setString(31, dataMovEnfermo.getCD_PROV2().trim());
              }
              else {
                st.setNull(31, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_MUNI2().trim().length() > 0) {
                st.setString(32, dataMovEnfermo.getCD_MUNI2().trim());
              }
              else {
                st.setNull(32, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_POST2().trim().length() > 0) {
                st.setString(33, dataMovEnfermo.getCD_POST2().trim());
              }
              else {
                st.setNull(33, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_DIREC2().trim().length() > 0) {
                st.setString(34, dataMovEnfermo.getDS_DIREC2().trim());
              }
              else {
                st.setNull(34, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_NUM2().trim().length() > 0) {
                st.setString(35, dataMovEnfermo.getDS_NUM2().trim());
              }
              else {
                st.setNull(35, java.sql.Types.VARCHAR);

                //DS_PISO2, DS_TELEF2, DS_OBSERV2, "
              }
              if (dataMovEnfermo.getDS_PISO2().trim().length() > 0) {
                st.setString(36, dataMovEnfermo.getDS_PISO2().trim());
              }
              else {
                st.setNull(36, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_TELEF2().trim().length() > 0) {
                st.setString(37, dataMovEnfermo.getDS_TELEF2().trim());
              }
              else {
                st.setNull(37, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_OBSERV2().trim().length() > 0) {
                st.setString(38, dataMovEnfermo.getDS_OBSERV2().trim());
              }
              else {
                st.setNull(38, java.sql.Types.VARCHAR);

                //CD_PROV3, CD_MUNI3, CD_POST3, DS_DIREC3, "
              }
              if (dataMovEnfermo.getCD_PROV3().trim().length() > 0) {
                st.setString(39, dataMovEnfermo.getCD_PROV3().trim());
              }
              else {
                st.setNull(39, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_MUNI3().trim().length() > 0) {
                st.setString(40, dataMovEnfermo.getCD_MUNI3().trim());
              }
              else {
                st.setNull(40, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCD_POST3().trim().length() > 0) {
                st.setString(41, dataMovEnfermo.getCD_POST3().trim());
              }
              else {
                st.setNull(41, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_DIREC3().trim().length() > 0) {
                st.setString(42, dataMovEnfermo.getDS_DIREC3().trim());
              }
              else {
                st.setNull(42, java.sql.Types.VARCHAR);
                //DS_NUM3, DS_PISO3, DS_TELEF3, DS_OBSERV3,
              }
              if (dataMovEnfermo.getDS_NUM3().trim().length() > 0) {
                st.setString(43, dataMovEnfermo.getDS_NUM3().trim());
              }
              else {
                st.setNull(43, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_PISO3().trim().length() > 0) {
                st.setString(44, dataMovEnfermo.getDS_PISO3().trim());
              }
              else {
                st.setNull(44, java.sql.Types.VARCHAR);
              }
              if (dataMovEnfermo.getDS_TELEF3().trim().length() > 0) {
                st.setString(45, dataMovEnfermo.getDS_TELEF3().trim());
              }
              else {
                st.setNull(45, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDS_OBSERV3().trim().length() > 0) {
                st.setString(46, dataMovEnfermo.getDS_OBSERV3().trim());
              }
              else {
                st.setNull(46, java.sql.Types.VARCHAR);

                // " DS_NDOC, SIGLAS

              }
              if (dataMovEnfermo.getDS_NDOC().trim().length() > 0) {
                st.setString(47, dataMovEnfermo.getDS_NDOC().trim());
              }
              else {
                st.setNull(47, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getSIGLAS().trim().length() > 0) {
                st.setString(48, dataMovEnfermo.getSIGLAS().trim());
              }
              else {
                st.setNull(48, java.sql.Types.VARCHAR);

                // suca
              }
              if (dataMovEnfermo.getCDTVIA().trim().length() > 0) {
                st.setString(49, dataMovEnfermo.getCDTVIA().trim());
              }
              else {
                st.setNull(49, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getCDTNUM().trim().length() > 0) {
                st.setString(50, dataMovEnfermo.getCDTNUM().trim());
              }
              else {
                st.setNull(50, java.sql.Types.VARCHAR);

              }
              if (dataMovEnfermo.getDSCALNUM().trim().length() > 0) {
                st.setString(51, dataMovEnfermo.getDSCALNUM().trim());
              }
              else {
                st.setNull(51, java.sql.Types.VARCHAR);

              }
              st.setString(52, "S");

              st.setString(53, "N");

              st.executeUpdate();
              st.close();
              st = null;

//!!!!! ENFERMO !!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              //MODIFICAMOS EL ENFERMO
              sQuery = Pregunta_Modifica_Enfermo();
              st = con.prepareStatement(sQuery);

              if (dataEntrada.getCD_TDOC().trim().length() > 0) {
                st.setString(1, dataEntrada.getCD_TDOC().trim());
              }
              else {
                st.setNull(1, java.sql.Types.VARCHAR);

                //" *DS_APE1,DS_APE2,DS_NOMBRE , *DS_FONOAPE1, DS_FONOAPE2,

              }
              st.setString(2, dataEntrada.getDS_APE1().trim());

              if (dataEntrada.getDS_APE2().trim().length() > 0) {
                st.setString(3, dataEntrada.getDS_APE2().trim());
              }
              else {
                st.setNull(3, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getDS_NOMBRE().trim().length() > 0) {
                st.setString(4, dataEntrada.getDS_NOMBRE().trim());
              }
              else {
                st.setNull(4, java.sql.Types.VARCHAR);

              }
              st.setString(5, dataEntrada.getDS_FONOAPE1().trim());

              if (dataEntrada.getDS_FONOAPE2().trim().length() > 0) {
                st.setString(6, dataEntrada.getDS_FONOAPE2().trim());
              }
              else {
                st.setNull(6, java.sql.Types.VARCHAR);

                // DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO

              }
              if (dataEntrada.getDS_FONONOMBRE().trim().length() > 0) {
                st.setString(7, dataEntrada.getDS_FONONOMBRE().trim());
              }
              else {
                st.setNull(7, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getIT_CALC().trim().length() > 0) {
                st.setString(8, dataEntrada.getIT_CALC().trim());
              }
              else {
                st.setNull(8, java.sql.Types.VARCHAR);

                ////////////
              }
              if (dataEntrada.getFC_NAC().trim().length() > 0) {
                dFecha = formater.parse(dataEntrada.getFC_NAC().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(9, sqlFec);
              }
              else {
                st.setNull(9, java.sql.Types.VARCHAR);
                /////////

              }
              if (dataEntrada.getCD_SEXO().trim().length() > 0) {
                st.setString(10, dataEntrada.getCD_SEXO().trim());
              }
              else {
                st.setNull(10, java.sql.Types.VARCHAR);

                //  DS_TELEF,

              }
              if (dataEntrada.getDS_TELEF().trim().length() > 0) {
                st.setString(11, dataEntrada.getDS_TELEF().trim());
              }
              else {
                st.setNull(11, java.sql.Types.VARCHAR);

                //   *CD_OPE, *FC_ULTACT
              }
              st.setString(12, dataEntrada.getCD_OPE().trim());

              ////////////
              /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                          sqlFec = new java.sql.Date(dFecha.getTime());
                          st.setDate(13, sqlFec);*/
              st.setTimestamp(13, TSFec);
              /////////

              // DS_NDOC,SIGLAS
              if (dataEntrada.getDS_NDOC().trim().length() > 0) {
                st.setString(14, dataEntrada.getDS_NDOC().trim());
              }
              else {
                st.setNull(14, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getSIGLAS().trim().length() > 0) {
                st.setString(15, dataEntrada.getSIGLAS().trim());
              }
              else {
                st.setNull(15, java.sql.Types.VARCHAR);

                //WHERE
              }
              st.setInt(16, new Integer(CodEnf.trim()).intValue());

              st.executeUpdate();
              st.close();
              st = null;
            } //fin de si modificamos enfermo y pasamos a historico

            //modificamos edoind---------------------------------------------------
            //sQuery=Pregunta_Modifica_Edoin();
            sQuery = Pregunta_Modifica_Edoin2();
            st = con.prepareStatement(sQuery);

            /*          st.setString (1, dataEntrada.getCD_ANOEPI().trim());
                      st.setString (2, dataEntrada.getCD_SEMEPI().trim());
                      if (dataEntrada.getCD_ANOURG().trim() .length() > 0)
                         st.setString (3, dataEntrada.getCD_ANOURG().trim());
                      else
                         st.setNull(3, java.sql.Types.VARCHAR);
                      if (dataEntrada.getNM_ENVIOURGSEM().trim().length() > 0)
                 st.setInt(4, (new Integer(dataEntrada.getNM_ENVIOURGSEM().trim())).intValue());
                      else
                         st.setNull(4, java.sql.Types.VARCHAR);
                      if (dataEntrada.getCD_CLASIFDIAG().trim() .length() > 0)
                 st.setString (5, dataEntrada.getCD_CLASIFDIAG().trim());
                      else
                         st.setNull(5, java.sql.Types.VARCHAR);
                      if (dataEntrada.getIT_PENVURG().trim() .length() > 0)
                         st.setString (6, dataEntrada.getIT_PENVURG().trim());
                      else
                         st.setNull(6, java.sql.Types.VARCHAR);
                      st.setInt(7, (new Integer(CodEnf.trim())).intValue());
                      if (dataEntrada.getCD_PROV_EDOIND().trim().length() > 0)
                 st.setString (8, dataEntrada.getCD_PROV_EDOIND().trim());
                      else
                         st.setNull(8, java.sql.Types.VARCHAR);
                      if (dataEntrada.getCD_MUN_EDOIND().trim() .length() > 0)
                 st.setString (9, dataEntrada.getCD_MUN_EDOIND().trim());
                      else
                         st.setNull(9, java.sql.Types.VARCHAR);
                 if (dataEntrada.getCD_NIVEL_1_EDOIND().trim() .length() > 0)
                 st.setString (10, dataEntrada.getCD_NIVEL_1_EDOIND().trim());
                     else
                         st.setNull(10, java.sql.Types.VARCHAR);
                 if (dataEntrada.getCD_NIVEL_2_EDOIND().trim() .length() > 0)
                 st.setString (11, dataEntrada.getCD_NIVEL_2_EDOIND().trim());
                     else
                         st.setNull(11, java.sql.Types.VARCHAR);
                     if (dataEntrada.getCD_ZBS_EDOIND().trim() .length() > 0)
                 st.setString (12, dataEntrada.getCD_ZBS_EDOIND().trim());
                     else
                         st.setNull(12, java.sql.Types.VARCHAR);
                     //fecha  obligatoria
                     dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
                     sqlFec = new java.sql.Date(dFecha.getTime());
                     st.setDate(13, sqlFec);
                     if (dataEntrada.getDS_CALLE().trim() .length() > 0)
                         st.setString (14, dataEntrada.getDS_CALLE().trim());
                     else
                         st.setNull(14, java.sql.Types.VARCHAR);
                    //+ " DS_NMCALLE, DS_PISO, CD_POSTAL, *CD_OPE, *FC_ULTACT, "  //5
                     if (dataEntrada.getDS_NMCALLE().trim() .length() > 0)
                         st.setString (15, dataEntrada.getDS_NMCALLE().trim());
                     else
                         st.setNull(15, java.sql.Types.VARCHAR);
                     if (dataEntrada.getDS_PISO_EDOIND().trim() .length() > 0)
                 st.setString (16, dataEntrada.getDS_PISO_EDOIND().trim());
                     else
                         st.setNull(16, java.sql.Types.VARCHAR);
                 if (dataEntrada.getCD_POSTAL_EDOIND().trim() .length() > 0)
                 st.setString (17, dataEntrada.getCD_POSTAL_EDOIND().trim());
                     else
                         st.setNull(17, java.sql.Types.VARCHAR);
                     st.setString (18, dataEntrada.getCD_OPE_EDOIND().trim());
                     //dFecha = formater.parse(dataEntrada.getFC_ULTACT_EDOIND().trim());
                     //sqlFec = new java.sql.Date(dFecha.getTime());
                     //st.setDate(20, sqlFec);
                     st.setTimestamp(19, TSFec);
                      // + " CD_ANOOTRC, NM_ENVOTRC, *CD_ENFCIE "  //3
                     if (dataEntrada.getCD_ANOOTRC().trim() .length() > 0)
                         st.setString (20, dataEntrada.getCD_ANOOTRC().trim());
                     else
                         st.setNull(20, java.sql.Types.VARCHAR);
                     if (dataEntrada.getNM_ENVOTRC().trim() .length() > 0)
                 st.setInt(21, (new Integer(dataEntrada.getNM_ENVOTRC().trim())).intValue());
                     else
                         st.setNull(21, java.sql.Types.VARCHAR);
                     st.setString (22, dataEntrada.getCD_ENFCIE().trim());
                  //    + " *FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, "// 3+
                  //+ " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, "// 3+
                  //+ " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO ) "// 3+
                 dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
                     sqlFec = new java.sql.Date(dFecha.getTime());
                     st.setDate(23, sqlFec);
                     if (dataEntrada.getIT_DERIVADO().trim() .length() > 0)
                        st.setString (24, dataEntrada.getIT_DERIVADO().trim());
                     else
                         st.setNull(24, java.sql.Types.VARCHAR);
                     if (dataEntrada.getDS_CENTRODER().trim() .length() > 0)
                 st.setString (25, dataEntrada.getDS_CENTRODER().trim());
                     else
                         st.setNull(25, java.sql.Types.VARCHAR);
                     if (dataEntrada.getIT_DIAGCLI().trim() .length() > 0)
                      st.setString (26, dataEntrada.getIT_DIAGCLI().trim());
                     else
                         st.setNull(26, java.sql.Types.VARCHAR);
                     if (dataEntrada.getFC_INISNT().trim() .length() > 0){
                 dFecha = formater.parse(dataEntrada.getFC_INISNT().trim());
                      sqlFec = new java.sql.Date(dFecha.getTime());
                      st.setDate(27, sqlFec);
                     }
                     else
                         st.setNull(27, java.sql.Types.VARCHAR);
                     if (dataEntrada.getDS_COLECTIVO().trim() .length() > 0)
                 st.setString (28, dataEntrada.getDS_COLECTIVO().trim());
                     else
                         st.setNull(28, java.sql.Types.VARCHAR);
                     if (dataEntrada.getIT_ASOCIADO().trim() .length() > 0)
                       st.setString (29, dataEntrada.getIT_ASOCIADO().trim());
                     else
                         st.setNull(29, java.sql.Types.VARCHAR);
                     if (dataEntrada.getIT_DIAGMICRO().trim() .length() > 0)
                       st.setString (30, dataEntrada.getIT_DIAGMICRO().trim());
                     else
                         st.setNull(30, java.sql.Types.VARCHAR);
                     if (dataEntrada.getDS_ASOCIADO().trim() .length() > 0)
                       st.setString (31, dataEntrada.getDS_ASOCIADO().trim());
                     else
                         st.setNull(31, java.sql.Types.VARCHAR);
                     // suca
                     //if (dataEntrada.getCD_VIA_EDOIND().trim() .length() > 0)
                     //  st.setString (33, dataEntrada.getCD_VIA_EDOIND().trim());
                     //else
                     //    st.setNull(33, java.sql.Types.VARCHAR);
                     //if (dataEntrada.getCD_VIAL_EDOIND().trim().length() > 0)
                     //  st.setString (34, dataEntrada.getCD_VIAL_EDOIND().trim());
                     //else
                     //    st.setNull(34, java.sql.Types.VARCHAR);
                     if (dataEntrada.getIT_DIAGSERO().trim() .length() > 0)
                       st.setString (32, dataEntrada.getIT_DIAGSERO().trim());
                     else
                         st.setNull(32, java.sql.Types.VARCHAR);
                     if (dataEntrada.getDS_DIAGOTROS().trim() .length() > 0)
                       st.setString (33, dataEntrada.getDS_DIAGOTROS().trim());
                     else
                         st.setNull(33, java.sql.Types.VARCHAR);
                     // SUCA
                     if (dataEntrada.getCDVIAL().trim() .length() > 0)
                       st.setString (34, dataEntrada.getCDVIAL().trim());
                     else
                         st.setNull(34, java.sql.Types.VARCHAR);
                     if (dataEntrada.getCDTVIA().trim() .length() > 0)
                       st.setString (35, dataEntrada.getCDTVIA().trim());
                     else
                         st.setNull(35, java.sql.Types.VARCHAR);
                     if (dataEntrada.getCDTNUM().trim() .length() > 0)
                       st.setString (36, dataEntrada.getCDTNUM().trim());
                     else
                         st.setNull(36, java.sql.Types.VARCHAR);
                     if (dataEntrada.getDSCALNUM().trim() .length() > 0)
                       st.setString (37, dataEntrada.getDSCALNUM().trim());
                     else
                         st.setNull(37, java.sql.Types.VARCHAR);
                    //WHERE
                    st.setInt(38, (new Integer(sCaso.trim())).intValue());
             */

//        st.setString (1, dataEntrada.getCD_ANOEPI().trim());
//          st.setString (2, dataEntrada.getCD_SEMEPI().trim());

            if (dataEntrada.getCD_ANOURG().trim().length() > 0) {
              st.setString(1, dataEntrada.getCD_ANOURG().trim());
            }
            else {
              st.setNull(1, java.sql.Types.VARCHAR);

            }

            if (dataEntrada.getNM_ENVIOURGSEM().trim().length() > 0) {
              st.setInt(2,
                        (new Integer(dataEntrada.getNM_ENVIOURGSEM().trim())).intValue());
            }
            else {
              st.setNull(2, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getCD_CLASIFDIAG().trim().length() > 0) {
              st.setString(3, dataEntrada.getCD_CLASIFDIAG().trim());
            }
            else {
              st.setNull(3, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getIT_PENVURG().trim().length() > 0) {
              st.setString(4, dataEntrada.getIT_PENVURG().trim());
            }
            else {
              st.setNull(4, java.sql.Types.VARCHAR);

            }
            st.setInt(5, (new Integer(CodEnf.trim())).intValue());

            if (dataEntrada.getCD_PROV_EDOIND().trim().length() > 0) {
              st.setString(6, dataEntrada.getCD_PROV_EDOIND().trim());
            }
            else {
              st.setNull(6, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getCD_MUN_EDOIND().trim().length() > 0) {
              st.setString(7, dataEntrada.getCD_MUN_EDOIND().trim());
            }
            else {
              st.setNull(7, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getCD_NIVEL_1_EDOIND().trim().length() > 0) {
              st.setString(8, dataEntrada.getCD_NIVEL_1_EDOIND().trim());
            }
            else {
              st.setNull(8, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getCD_NIVEL_2_EDOIND().trim().length() > 0) {
              st.setString(9, dataEntrada.getCD_NIVEL_2_EDOIND().trim());
            }
            else {
              st.setNull(9, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getCD_ZBS_EDOIND().trim().length() > 0) {
              st.setString(10, dataEntrada.getCD_ZBS_EDOIND().trim());
            }
            else {
              st.setNull(10, java.sql.Types.VARCHAR);

              //fecha  obligatoria
            }
            dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(11, sqlFec);

            if (dataEntrada.getDS_CALLE().trim().length() > 0) {
              st.setString(12, dataEntrada.getDS_CALLE().trim());
            }
            else {
              st.setNull(12, java.sql.Types.VARCHAR);

              //+ " DS_NMCALLE, DS_PISO, CD_POSTAL, *CD_OPE, *FC_ULTACT, "  //5
            }
            if (dataEntrada.getDS_NMCALLE().trim().length() > 0) {
              st.setString(13, dataEntrada.getDS_NMCALLE().trim());
            }
            else {
              st.setNull(13, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getDS_PISO_EDOIND().trim().length() > 0) {
              st.setString(14, dataEntrada.getDS_PISO_EDOIND().trim());
            }
            else {
              st.setNull(14, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getCD_POSTAL_EDOIND().trim().length() > 0) {
              st.setString(15, dataEntrada.getCD_POSTAL_EDOIND().trim());
            }
            else {
              st.setNull(15, java.sql.Types.VARCHAR);

            }
            st.setString(16, dataEntrada.getCD_OPE_EDOIND().trim());

                /*dFecha = formater.parse(dataEntrada.getFC_ULTACT_EDOIND().trim());
                      sqlFec = new java.sql.Date(dFecha.getTime());
                      st.setDate(20, sqlFec); */
            st.setTimestamp(17, TSFec);

            // + " CD_ANOOTRC, NM_ENVOTRC, *CD_ENFCIE "  //3
            if (dataEntrada.getCD_ANOOTRC().trim().length() > 0) {
              st.setString(18, dataEntrada.getCD_ANOOTRC().trim());
            }
            else {
              st.setNull(18, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getNM_ENVOTRC().trim().length() > 0) {
              st.setInt(19,
                        (new Integer(dataEntrada.getNM_ENVOTRC().trim())).intValue());
            }
            else {
              st.setNull(19, java.sql.Types.VARCHAR);

            }

            st.setString(20, dataEntrada.getCD_ENFCIE().trim());

            //    + " *FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, "// 3+
            //+ " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, "// 3+
            //+ " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO ) "// 3+

            dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(21, sqlFec);

            if (dataEntrada.getIT_DERIVADO().trim().length() > 0) {
              st.setString(22, dataEntrada.getIT_DERIVADO().trim());
            }
            else {
              st.setNull(22, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getDS_CENTRODER().trim().length() > 0) {
              st.setString(23, dataEntrada.getDS_CENTRODER().trim());
            }
            else {
              st.setNull(23, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getIT_DIAGCLI().trim().length() > 0) {
              st.setString(24, dataEntrada.getIT_DIAGCLI().trim());
            }
            else {
              st.setNull(24, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getFC_INISNT().trim().length() > 0) {
              dFecha = formater.parse(dataEntrada.getFC_INISNT().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(25, sqlFec);
            }
            else {
              st.setNull(25, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getDS_COLECTIVO().trim().length() > 0) {
              st.setString(26, dataEntrada.getDS_COLECTIVO().trim());
            }
            else {
              st.setNull(26, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getIT_ASOCIADO().trim().length() > 0) {
              st.setString(27, dataEntrada.getIT_ASOCIADO().trim());
            }
            else {
              st.setNull(27, java.sql.Types.VARCHAR);

            }

            if (dataEntrada.getIT_DIAGMICRO().trim().length() > 0) {
              st.setString(28, dataEntrada.getIT_DIAGMICRO().trim());
            }
            else {
              st.setNull(28, java.sql.Types.VARCHAR);

            }

            if (dataEntrada.getDS_ASOCIADO().trim().length() > 0) {
              st.setString(29, dataEntrada.getDS_ASOCIADO().trim());
            }
            else {
              st.setNull(29, java.sql.Types.VARCHAR);
              /* suca
                   if (dataEntrada.getCD_VIA_EDOIND().trim() .length() > 0)
                st.setString (33, dataEntrada.getCD_VIA_EDOIND().trim());
                        else
                  st.setNull(33, java.sql.Types.VARCHAR);
                   if (dataEntrada.getCD_VIAL_EDOIND().trim().length() > 0)
                st.setString (34, dataEntrada.getCD_VIAL_EDOIND().trim());
                        else
                  st.setNull(34, java.sql.Types.VARCHAR);
               */

            }
            if (dataEntrada.getIT_DIAGSERO().trim().length() > 0) {
              st.setString(30, dataEntrada.getIT_DIAGSERO().trim());
            }
            else {
              st.setNull(30, java.sql.Types.VARCHAR);

            }

            if (dataEntrada.getDS_DIAGOTROS().trim().length() > 0) {
              st.setString(31, dataEntrada.getDS_DIAGOTROS().trim());
            }
            else {
              st.setNull(31, java.sql.Types.VARCHAR);

              // SUCA
            }
            if (dataEntrada.getCDVIAL().trim().length() > 0) {
              st.setString(32, dataEntrada.getCDVIAL().trim());
            }
            else {
              st.setNull(32, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getCDTVIA().trim().length() > 0) {
              st.setString(33, dataEntrada.getCDTVIA().trim());
            }
            else {
              st.setNull(33, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getCDTNUM().trim().length() > 0) {
              st.setString(34, dataEntrada.getCDTNUM().trim());
            }
            else {
              st.setNull(34, java.sql.Types.VARCHAR);

            }
            if (dataEntrada.getDSCALNUM().trim().length() > 0) {
              st.setString(35, dataEntrada.getDSCALNUM().trim());
            }
            else {
              st.setNull(35, java.sql.Types.VARCHAR);

              //WHERE
            }
            st.setInt(36, (new Integer(sCaso.trim())).intValue());

            st.executeUpdate();
            st.close();
            st = null;

            //borrar registros de notif_edoi para ese nm_edo
            String sBorrar = Llamar_Borrar();

            st = con.prepareStatement(sBorrar);
            st.setInt(1, (new Integer(sCaso.trim())).intValue());

            st.executeUpdate();
            st.close();
            st = null;

            //insertamos de nuevo
//!!!! NOTIF EDOI!!!!!!!!!!!!!!!!!!!!!!
            //recorremos la lista de entrada. Tantas altas como declarantes haya.
            for (i = 0; i < param.size(); i++) {

              dataEntrada = (DataTab) param.elementAt(i);
              /*return("insert into SIVE_NOTIF_EDOI "
                         + "( NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI," //4
                         + " FC_RECEP, DS_DECLARANTE, IT_PRIMERO, CD_OPE, " //4
                         + " FC_ULTACT, FC_FECNOTIF )" //2
                         + "values (?, ?, ?, ?,  ?, ?, ?, ?, ?, ?) ");
               */
              ALTA = Pregunta_Alta_Notif_Edoi();
              st = con.prepareStatement(ALTA);

              st.setInt(1, new Integer(sCaso.trim()).intValue());

              st.setString(2, dataEntrada.getCD_E_NOTIF().trim());
              st.setString(3, dataEntrada.getCD_ANOEPI_NOTIF_EDOI().trim());

              st.setString(4, dataEntrada.getCD_SEMEPI_NOTIF_EDOI().trim());

              dFecha = formater.parse(dataEntrada.getFC_RECEP_NOTIF_EDOI().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);

              st.setString(6, dataEntrada.getDS_DECLARANTE().trim());
              st.setString(7, dataEntrada.getIT_PRIMERO().trim());
              st.setString(8, dataEntrada.getCD_OPE_NOTIF_EDOI().trim());
                  /*dFecha = formater.parse(dataEntrada.getFC_ULTACT_NOTIF_EDOI().trim());
                         sqlFec = new java.sql.Date(dFecha.getTime());
                         st.setDate(9, sqlFec); */
              st.setTimestamp(9, TSFec);

              dFecha = formater.parse(dataEntrada.getFC_FECNOTIF_NOTIF_EDOI().
                                      trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(10, sqlFec);

              st.setString(11, "E");

              st.executeUpdate();
              st.close();
              st = null;

            } //fin for ---lista con datos

            //cargar la lista de vuelta (con Cod_Enfermo, Nm_edo, sFecha_Actual)
            dataSalida = new DataEnfCaso(CodEnf, sCaso, sFecha_Actual);
            listaSalida.addElement(dataSalida);

          } //fin de if (param.size() != 0
          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

//segun Enrique solo se insertan los campos indicados en la query
  public String Pregunta_Alta_Enfermo() {
    return ("insert into SIVE_ENFERMO "
            + "(CD_ENFERMO, CD_PROV, CD_PAIS, CD_MUN, CD_TDOC,  " //5
            + " DS_APE1,DS_APE2,DS_NOMBRE , DS_FONOAPE1, DS_FONOAPE2, " //5
            + " DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL, " //5
            + " DS_DIREC,DS_NUM,DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2, " //6
            + " CD_ZBS, CD_OPE, FC_ULTACT,IT_REVISADO, " //4
            + " DS_NDOC,SIGLAS, " //2
            + " CDVIAL, CDTVIA, CDTNUM, DSCALNUM, IT_ENFERMO, IT_CONTACTO " // suca 4
            + " ) "
            +
            "values ( ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, "
            + " ?,?, ?,?,?,?,?,?)");

  }

  public String Pregunta_Select_Enfermo() {
    //Para pasarlo a movimiento enfermos cuando modifiquemos
    //enfermo: tiene cd_vial, mov_enfermo NO
    return ("select "
            + " CD_PROV, CD_PAIS, CD_MUN, CD_TDOC, "
            + " DS_APE1,DS_APE2,DS_NOMBRE , DS_FONOAPE1, DS_FONOAPE2, "
            + " DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL, "
            + " DS_DIREC,DS_NUM,DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2, "
            + " CD_ZBS, CD_OPE, FC_ULTACT,IT_REVISADO, CD_MOTBAJA, FC_BAJA, "
            + " DS_OBSERV, CD_PROV2, CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2,  "
            + " DS_PISO2, DS_TELEF2, DS_OBSERV2, "
            + " CD_PROV3,CD_MUNI3, CD_POST3, DS_DIREC3, "
            + " DS_NUM3, DS_PISO3, DS_TELEF3, DS_OBSERV3, "
            + " DS_NDOC, SIGLAS, CDVIAL, CDTVIA, CDTNUM, DSCALNUM "
            + " FROM SIVE_ENFERMO WHERE CD_ENFERMO = ? ");

  }

  public String Pregunta_Insert_MovEnfermo() {
    //Para pasarlo a movimiento enfermos cuando modifiquemos
    //enfermo: tiene cd_vial, mov_enfermo NO
    return ("insert into SIVE_MOV_ENFERMO "
            + " ( NM_MOVENF, CD_ENFERMO, " //2
            + " CD_PROV, CD_PAIS, CD_MUN, CD_TDOC, CDVIAL, " //5
            + " DS_APE1, DS_APE2, DS_NOMBRE , DS_FONOAPE1, DS_FONOAPE2, " //5
            + " DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL, " //5
            + " DS_DIREC, DS_NUM, DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2, " //6
            + " CD_ZBS, CD_OPE, FC_ULTACT, IT_REVISADO, CD_MOTBAJA, FC_BAJA, " //6
            + " DS_OBSERV, CD_PROV2, CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2,  " //6
            + " DS_PISO2, DS_TELEF2, DS_OBSERV2, " //3
            + " CD_PROV3, CD_MUNI3, CD_POST3, DS_DIREC3, " //4
            + " DS_NUM3, DS_PISO3, DS_TELEF3, DS_OBSERV3, " //4
            +
        " DS_NDOC, SIGLAS, CDTVIA, CDTNUM, DSCALNUM, IT_ENFERMO, IT_CONTACTO ) " //5    //53 total
            + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, " //17
            + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, " //35
            + "?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ? , ?, ?, ?, ?, ?, ? )"); //50

  }

  public String Pregunta_Alta_Edoind() {

    return ("insert into SIVE_EDOIND "
            + "(NM_EDO,  CD_ANOEPI,  CD_SEMEPI, " //3
            + " CD_ANOURG,  NM_ENVIOURGSEM, CD_CLASIFDIAG, " //3
            + " IT_PENVURG, CD_ENFERMO, CD_PROV,CD_MUN, " //4
            + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, " //5
            + " DS_NMCALLE, DS_PISO, CD_POSTAL, CD_OPE, FC_ULTACT, " //5
            + " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE,  " //3
            + " FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, " // 3+
            + " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, " // 3+
            + " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO, " //3+
            + " IT_DIAGSERO, DS_DIAGOTROS, " // 2+
            + " CDVIAL, CDTVIA, CDTNUM, DSCALNUM ) " // 4+
            + "values (?, ?, ?, ?, ?, ?,  ?, ?, ?, ?,  "
            + " ?, ?, ?, ?, ?,   ?, ?, ?, ?, ?,  ?, ?, ?, "
            + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )"); // 9 nuevos+4

  }

  public String Pregunta_Modifica_Edoin() {
    return ("update SIVE_EDOIND "
            + " set CD_ANOEPI = ?,  CD_SEMEPI = ?, " //3
            + " CD_ANOURG = ?, NM_ENVIOURGSEM  = ?, CD_CLASIFDIAG = ?, " //4
            + " IT_PENVURG = ?, CD_ENFERMO = ?, CD_PROV = ?, CD_MUN = ?, " //4
            +
        " CD_NIVEL_1 = ?, CD_NIVEL_2 = ?, CD_ZBS = ?, FC_RECEP = ?, DS_CALLE = ?, " //5
            +
        " DS_NMCALLE = ?, DS_PISO = ?, CD_POSTAL = ?, CD_OPE = ?, FC_ULTACT = ?, " //5
            + " CD_ANOOTRC = ?, NM_ENVOTRC = ?, CD_ENFCIE = ? , "
            + " FC_FECNOTIF = ? ,  IT_DERIVADO = ?, DS_CENTRODER = ?, " // 3+
            + " IT_DIAGCLI = ?,  FC_INISNT = ?, DS_COLECTIVO = ?, " // 3+
            + " IT_ASOCIADO = ?,  IT_DIAGMICRO = ?, DS_ASOCIADO = ? , " // 3+
            + " IT_DIAGSERO= ?, DS_DIAGOTROS= ? , " // 2+
            + " CDVIAL= ?, CDTVIA= ?, CDTNUM=?, DSCALNUM=? " // 4+
            + " where NM_EDO = ? ");
  }

// no se actualiza el a�o ni la semana
  public String Pregunta_Modifica_Edoin2() {
    return ("update SIVE_EDOIND "
            + " set " //3
            + " CD_ANOURG = ?, NM_ENVIOURGSEM  = ?, CD_CLASIFDIAG = ?, " //4
            + " IT_PENVURG = ?, CD_ENFERMO = ?, CD_PROV = ?, CD_MUN = ?, " //4
            +
        " CD_NIVEL_1 = ?, CD_NIVEL_2 = ?, CD_ZBS = ?, FC_RECEP = ?, DS_CALLE = ?, " //5
            +
        " DS_NMCALLE = ?, DS_PISO = ?, CD_POSTAL = ?, CD_OPE = ?, FC_ULTACT = ?, " //5
            + " CD_ANOOTRC = ?, NM_ENVOTRC = ?, CD_ENFCIE = ? , "
            + " FC_FECNOTIF = ? ,  IT_DERIVADO = ?, DS_CENTRODER = ?, " // 3+
            + " IT_DIAGCLI = ?,  FC_INISNT = ?, DS_COLECTIVO = ?, " // 3+
            + " IT_ASOCIADO = ?,  IT_DIAGMICRO = ?, DS_ASOCIADO = ? , " // 3+
            + " IT_DIAGSERO= ?, DS_DIAGOTROS= ? , " // 2+
            + " CDVIAL= ?, CDTVIA= ?, CDTNUM=?, DSCALNUM=? " // 4+
            + " where NM_EDO = ? ");
  }

//NOTA telefono es un dato de enfermo, ptt debe ir
  public String Pregunta_Modifica_Enfermo() {
    return ("update SIVE_ENFERMO "
            + " set CD_TDOC = ?, "
            +
        " DS_APE1 = ?,DS_APE2 = ?,DS_NOMBRE = ?, DS_FONOAPE1 = ?, DS_FONOAPE2 = ?, "
            + " DS_FONONOMBRE= ?, IT_CALC= ?, FC_NAC= ?, CD_SEXO= ?,  "
            + " DS_TELEF = ?, "
            + " CD_OPE = ?, FC_ULTACT = ?, "
            + " DS_NDOC = ?, SIGLAS = ? "
            + " where CD_ENFERMO = ? ");
  }

  public String Llamar_Borrar() {

    return ("delete from SIVE_NOTIF_EDOI "
            + "where NM_EDO = ? and CD_FUENTE = 'E' ");

  }

  public String Pregunta_Alta_Notif_Edoi() {

    return ("insert into SIVE_NOTIF_EDOI "
            + "( NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI," //4
            + " FC_RECEP, DS_DECLARANTE, IT_PRIMERO, CD_OPE, " //4
            + " FC_ULTACT, FC_FECNOTIF, CD_FUENTE )" //3
            + "values (?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?) ");

  }

  public String Select_enfermo_edoind() {

    return ("select NM_EDO from SIVE_EDOIND "
            + "where CD_ENFERMO = ? and NM_EDO <> ?");
  }

  public String Borrar_Enfermo() {
    return ("delete from SIVE_ENFERMO "
            + "where CD_ENFERMO = ?");
  }

  public String Borrar_Mov_Enfermo() {
    return ("delete from SIVE_MOV_ENFERMO "
            +
            "where CD_ENFERMO = ? and IT_ENFERMO = 'S' and IT_CONTACTO = 'N' ");
  }

  /** prueba del servlet */
  /*
    public CLista doPrueba(int operacion, CLista parametros) throws Exception {
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                             "sive_desa",
                             "sive_desa");
      Connection con = null;
      con = openConnection();
      return doWork(operacion, parametros);
    }
   */

} //fin de la clase
