package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import alarmas.DataEnferedo;
import capp.CApp;
import capp.CLista;
import notdata.DataIndivEnfermedad;
import sapp.DBServlet;

public class SrvEnfermedad
    extends DBServlet {

  int idioma;

  // modos de operaci�n del servlet

  public final int servletSELECCION_LISTA_ENFER = 9;
  public static final int servletSELECCION_LISTA_ENFER_TDOC_SEXO = 100;

  // modificacion jlt 27/11/2001
  // para recuperar enfermedades dadas de baja en
  // modo modificacion
  public static final int servletSELECCION_LISTA_ENFER_TDOC_SEXO_M = 101;

  // modos para modelos y alarmas
  public final int servletSELECCION_MODELOS_X_CODIGO = 200;
  public final int servletOBTENER_MODELOS_X_DESCRIPCION = 203;
  public final int servletSELECCION_MODELOS_X_DESCRIPCION = 201;
  public final int servletOBTENER_MODELOS_X_CODIGO = 202;

  public final int servletSELECCION_ALARMAS_X_CODIGO = 300;
  public final int servletOBTENER_ALARMAS_X_DESCRIPCION = 303;
  public final int servletSELECCION_ALARMAS_X_DESCRIPCION = 301;
  public final int servletOBTENER_ALARMAS_X_CODIGO = 302;

  private String strOrdenar = null;
  private String strCampoQuery = null;

  private String desMod = null;
  private String desLMod = null;

  private String strParametrosQuery = null;

  //public static final int servletSELECCION_LISTA_ENFER_TDOC_SEXO_PAIS_CA = 110;

  // objetos de datos
  CLista listaSalida = null;
  CLista listaEntrada = new CLista();

  DataEnferedo dataModelosSalida = null;
  DataEnferedo dataModelosEntrada = null;

  DataIndivEnfermedad dataEntrada = null;
  DataIndivEnfermedad dataSalida = null;

  // par�metros
  String sQuery = "";

  // objetos JDBC
  Connection con = null;
  PreparedStatement st = null;
  ResultSet rs = null;

  //fechas
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
  java.util.Date dFecha = new java.util.Date();
  java.sql.Date sqlFec;

  public SrvEnfermedad() {}

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    this.idioma = param.getIdioma();

    listaSalida = new CLista();

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {
      switch (opmode) {

        //******** ALARMAS y MODELOS: OPERACIONES DE SELECCI�N
         case servletSELECCION_ALARMAS_X_DESCRIPCION:
        case servletSELECCION_ALARMAS_X_CODIGO:
        case servletSELECCION_MODELOS_X_DESCRIPCION:
        case servletSELECCION_MODELOS_X_CODIGO:

          if (opmode == servletSELECCION_ALARMAS_X_DESCRIPCION ||
              opmode == servletSELECCION_ALARMAS_X_CODIGO) {
            strParametrosQuery = "(?, ?)";
          }
          else {
            strParametrosQuery = "(?, ?, ?)";
          }

          dataModelosEntrada = (DataEnferedo) param.firstElement();

          // ARG: upper (15/5/02)
          if (opmode == servletSELECCION_ALARMAS_X_CODIGO ||
              opmode == servletSELECCION_MODELOS_X_CODIGO)

          {
            strOrdenar = new String("a.CD_ENFCIE");
            strCampoQuery = new String("a.CD_ENFCIE like ? and ");
          }
          else {
            strOrdenar = new String("b.DS_PROCESO");
            strCampoQuery = new String("upper(b.DS_PROCESO) like upper(?) and ");
          }

          sQuery = "select a.CD_ENFCIE, a.CD_TVIGI, b.DS_PROCESO," +
              " b.DSL_PROCESO, a.IT_INDURG from SIVE_ENFEREDO a, " +
              " SIVE_PROCESOS b" +
              " where (a.CD_TVIGI in " + strParametrosQuery + "  and " +
              strCampoQuery +
              " a.CD_ENFCIE = b.CD_ENFCIE) order by " + strOrdenar;

          st = con.prepareStatement(sQuery);

          st.setString(1, "I");

          st.setString(2, "A");

          if (opmode == servletSELECCION_ALARMAS_X_DESCRIPCION ||
              opmode == servletSELECCION_ALARMAS_X_CODIGO) {

            if (opmode == servletSELECCION_ALARMAS_X_CODIGO) {
              st.setString(3, (String) dataModelosEntrada.getCod() + "%");
            }
            else {
              st.setString(3, "%" + (String) dataModelosEntrada.getDes() + "%");
            }
          }
          else {
            st.setString(3, "X");
            if (opmode == servletSELECCION_MODELOS_X_CODIGO) {
              st.setString(4, (String) dataModelosEntrada.getCod() + "%");
            }
            else {
              st.setString(4, "%" + (String) dataModelosEntrada.getDes() + "%");
            }
          }

          rs = st.executeQuery();
          desMod = "";
          desLMod = "";

          while (rs.next()) {
            desMod = rs.getString("DS_PROCESO");
            desLMod = rs.getString("DSL_PROCESO");

            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (desLMod != null)) {

              dataModelosSalida = new DataEnferedo(
                  rs.getString("CD_ENFCIE"),
                  desLMod);
              //desLMod, rs.getString("IT_INDURG"));

            }
            else {
              dataModelosSalida = new DataEnferedo(
                  rs.getString("CD_ENFCIE"),
                  desMod);
              //desMod, rs.getString("IT_INDURG"));

            }
            listaSalida.addElement(dataModelosSalida);
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

        case servletOBTENER_ALARMAS_X_DESCRIPCION:
        case servletOBTENER_ALARMAS_X_CODIGO:
        case servletOBTENER_MODELOS_X_DESCRIPCION:
        case servletOBTENER_MODELOS_X_CODIGO:

          if (opmode == servletOBTENER_ALARMAS_X_DESCRIPCION ||
              opmode == servletOBTENER_ALARMAS_X_CODIGO) {
            strParametrosQuery = "(?, ?)";
          }
          else {
            strParametrosQuery = "(?, ?, ?)";
          }

          dataModelosEntrada = (DataEnferedo) param.firstElement();

          if (opmode == servletOBTENER_ALARMAS_X_CODIGO ||
              opmode == servletOBTENER_MODELOS_X_CODIGO) {
            strOrdenar = "a.CD_ENFCIE";
            strCampoQuery = new String("a.CD_ENFCIE = ? and ");
          }
          else {
            strOrdenar = "b.DS_PROCESO";
            strCampoQuery = new String("b.DS_PROCESO = ? and ");
          }

          sQuery = "select a.CD_ENFCIE, a.CD_TVIGI, b.DS_PROCESO," +
              " b.DSL_PROCESO, a.IT_INDURG from SIVE_ENFEREDO a, " +
              " SIVE_PROCESOS b" +
              " where (a.CD_TVIGI in " + strParametrosQuery + " and " +
              strCampoQuery +
              " a.CD_ENFCIE = b.CD_ENFCIE) order by " + strOrdenar;

          st = con.prepareStatement(sQuery);

          st.setString(1, "I");

          st.setString(2, "A");

          if (opmode == servletOBTENER_ALARMAS_X_CODIGO ||
              opmode == servletOBTENER_ALARMAS_X_DESCRIPCION) {

            if (opmode == servletOBTENER_ALARMAS_X_CODIGO) {
              st.setString(3, (String) dataModelosEntrada.getCod());
            }
            else {
              st.setString(3, (String) dataModelosEntrada.getDes());
            }
          }
          else {
            st.setString(3, "X");
            if (opmode == servletOBTENER_MODELOS_X_CODIGO) {
              st.setString(4, (String) dataModelosEntrada.getCod());
            }
            else {
              st.setString(4, (String) dataModelosEntrada.getDes());
            }
          }

          rs = st.executeQuery();
          desMod = "";
          desLMod = "";

          while (rs.next()) {
            desMod = rs.getString("DS_PROCESO");
            desLMod = rs.getString("DSL_PROCESO");

            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (desLMod != null)) {

              dataModelosSalida = new DataEnferedo(
                  rs.getString("CD_ENFCIE"),
                  desLMod);
              //desLMod, rs.getString("IT_INDURG"));

            }
            else {
              dataModelosSalida = new DataEnferedo(
                  rs.getString("CD_ENFCIE"),
                  desMod);
              //desMod, rs.getString("IT_INDURG"));

            }
            listaSalida.addElement(dataModelosSalida);
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

              /****************************************************************************/
              /****************************************************************************/
              /****************************************************************************/

        case servletSELECCION_LISTA_ENFER_TDOC_SEXO:
          listaSalida.addElement(traeEnfermedades(param));
          listaSalida.addElement(getDataEnfermo("CD_TDOC", "DS_TIPODOC",
                                                "DSL_TIPODOC", "SIVE_T_DOC"));
          listaSalida.addElement(getDataEnfermo("CD_SEXO", "DS_SEXO",
                                                "DSL_SEXO", "SIVE_SEXO"));

          break;

          // modificacion jlt 27/11/2001
        case servletSELECCION_LISTA_ENFER_TDOC_SEXO_M:
          listaSalida.addElement(traeEnfermedades_M(param));
          listaSalida.addElement(getDataEnfermo("CD_TDOC", "DS_TIPODOC",
                                                "DSL_TIPODOC", "SIVE_T_DOC"));
          listaSalida.addElement(getDataEnfermo("CD_SEXO", "DS_SEXO",
                                                "DSL_SEXO", "SIVE_SEXO"));

          break;

        case servletSELECCION_LISTA_ENFER:

//// System_out.println("@@@notservlets->SrvEnfermedad->servletSELECCION_LISTA_ENFER");
          dataEntrada = (DataIndivEnfermedad) param.firstElement();

          if (dataEntrada.getCD_TVIGI().trim().equals("N")) {
            sQuery = "select a.CD_ENFCIE, a.CD_TVIGI, b.DS_PROCESO," +
                " b.DSL_PROCESO, a.IT_INDURG from SIVE_ENFEREDO a, " +
                " SIVE_PROCESOS b" +
                " where (a.CD_TVIGI in (? , ?, ?)  and " +
                " a.CD_ENFCIE = b.CD_ENFCIE and " +
                // ARG: Se filtran las enfermedades que no esten dadas de baja
                " a.IT_BAJA = 'N')";
            st = con.prepareStatement(sQuery);
            st.setString(1, "N");
            st.setString(2, "A");
            st.setString(3, "X");
          }
          else if (dataEntrada.getCD_TVIGI().trim().equals("I")) {
            sQuery = "select a.CD_ENFCIE, a.CD_TVIGI, b.DS_PROCESO," +
                " b.DSL_PROCESO, a.IT_INDURG from SIVE_ENFEREDO a, " +
                " SIVE_PROCESOS b" +
                " where (a.CD_TVIGI in (? , ?)  and " +
                " a.CD_ENFCIE = b.CD_ENFCIE)";
            st = con.prepareStatement(sQuery);
            st.setString(1, "I");
            st.setString(2, "A");
          }

          rs = st.executeQuery();
          String des = "";
          String desL = "";

          while (rs.next()) {
            des = rs.getString("DS_PROCESO");
            desL = rs.getString("DSL_PROCESO");

            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (desL != null)) {

              dataSalida = new DataIndivEnfermedad(
                  rs.getString("CD_TVIGI"),
                  rs.getString("CD_ENFCIE"),
                  desL, rs.getString("IT_INDURG"));

            }
            else {
              dataSalida = new DataIndivEnfermedad(
                  rs.getString("CD_TVIGI"),
                  rs.getString("CD_ENFCIE"),
                  des, rs.getString("IT_INDURG"));

            }
            listaSalida.addElement(dataSalida);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

  protected CLista getDataEnfermo(String a_clave, String a_descrip,
                                  String a_descripL, String a_tabla) throws
      Exception {
    enfermo.DataEnfermo d = null;
    String clave, dato, datoL;
    CLista result = new CLista();

    if (a_clave == null || a_descrip == null || a_descripL == null || a_tabla == null) {
      //# // System_out.println("Faltan datos para la query ");
      return null; // no se pueden hallar los datos
    }

    st = con.prepareStatement("SELECT " + a_clave + " , " + a_descrip + " , " +
                              a_descripL + " FROM " + a_tabla + " ORDER BY " +
                              a_descrip);
    rs = st.executeQuery();
    while (rs.next()) {
      d = new enfermo.DataEnfermo(a_clave);

      clave = rs.getString(1);
      if (clave != null) {
        d.put(a_clave, clave);
      }
      dato = rs.getString(2);
      if (dato != null) {
        d.put(a_descrip, dato);
      }
      datoL = rs.getString(3);
      if (datoL != null) {
        d.put(a_descripL, datoL);
      }
      //Si idioma no es por defectomete desL en des
      if ( (idioma != CApp.idiomaPORDEFECTO) &&
          (datoL != null)) {
        d.put(a_descrip, datoL);
      }

      result.addElement(d);
    }
    rs.close();
    st.close();
    rs = null;
    st = null;

    return result;
  }

  protected CLista getDataEnfermo(String a_clave, String a_descrip,
                                  String a_tabla) throws Exception {
    enfermo.DataEnfermo d = null;
    String clave, dato;
    CLista result = new CLista();

    if (a_clave == null || a_descrip == null || a_tabla == null) {
      //# // System_out.println("Faltan datos para la query ");
      return null; // no se pueden hallar los datos
    }

    st = con.prepareStatement("SELECT " + a_clave + " , " + a_descrip +
                              " FROM " + a_tabla + " ORDER BY " + a_descrip);
    rs = st.executeQuery();
    while (rs.next()) {
      d = new enfermo.DataEnfermo(a_clave);
      clave = rs.getString(1);
      if (clave != null) {
        d.put(a_clave, clave);
      }
      dato = rs.getString(2);
      if (dato != null) {
        d.put(a_descrip, dato);
      }

      result.addElement(d);
    }
    rs.close();
    st.close();
    rs = null;
    st = null;

    return result;
  }

  protected CLista traeEnfermedades(CLista param) throws Exception {
    DataIndivEnfermedad dataEntrada = (DataIndivEnfermedad) param.firstElement();
    CLista listaSalida = new CLista();
    Hashtable hash = (Hashtable) param.elementAt(1);

    //Trae enfermedades para la pesta�a de pCabecera: Enfermedades de tipo I, A

    sQuery = "select a.CD_ENFCIE, a.CD_TVIGI, b.DS_PROCESO," +
        " b.DSL_PROCESO, a.IT_INDURG from SIVE_ENFEREDO a, " +
        " SIVE_PROCESOS b" +
        " where (a.CD_TVIGI in ( ?, ? ) and " +
        " a.CD_ENFCIE = b.CD_ENFCIE and " +
        // ARG: Se filtran las enfermedades que no esten dadas de baja
        " a.IT_BAJA = 'N')" +
        " order by b.DS_PROCESO"; //^^

    /* consulta probada en el sqlplus
     select a.CD_ENFCIE, a.CD_TVIGI, b.DS_PROCESO,b.DSL_PROCESO, a.IT_INDURG
     from SIVE_ENFEREDO a, SIVE_PROCESOS b
     where ((a.CD_TVIGI = 'I') or
         (a.CD_TVIGI = 'A' and a.CD_ENFCIE in (select e.CD_ENFCIE from SIVE_EDONUM e
                                            where e.CD_E_NOTIf = 'H01A'
                                              and e.CD_ANOEPI = '1999'
                                              and e.CD_SEMEPI = '24'
                                              and e.FC_RECEP = '21/06/99'
                                              and e.FC_FECNOTIF = '19/06/99')))
     and (a.CD_ENFCIE = b.CD_ENFCIE)
     */

    /* sQuery = "select a.CD_ENFCIE, a.CD_TVIGI, b.DS_PROCESO,b.DSL_PROCESO, a.IT_INDURG"
            + " from SIVE_ENFEREDO a,  SIVE_PROCESOS b "
            + " where ((a.CD_TVIGI = ?) or "
            + " (a.CD_TVIGI = ? and a.CD_ENFCIE in "
            + "(select e.CD_ENFCIE from SIVE_EDONUM e "
            + " where e.CD_E_NOTIf = ? and e.CD_ANOEPI = ? "
            + " and e.CD_SEMEPI = ? and e.FC_RECEP = ? "
            + " and e.FC_FECNOTIF = ?))) "
            + " and (a.CD_ENFCIE = b.CD_ENFCIE) "; */

    st = con.prepareStatement(sQuery);
    st.setString(1, "I");
    st.setString(2, "A");
    /*  st.setString(3, (String)hash.get("CD_E_NOTIF"));
      st.setString(4, (String)hash.get("CD_ANOEPI"));
      st.setString(5, (String)hash.get("CD_SEMEPI"));
      String recep =  (String)hash.get("FC_RECEP");
      String notif =  (String)hash.get("FC_FECNOTIF");
      dFecha = formater.parse(recep);
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(6, sqlFec);
      dFecha = formater.parse(notif);
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(7, sqlFec);   */

    rs = st.executeQuery();
    String des = "";
    String desL = "";

    while (rs.next()) {
      des = rs.getString("DS_PROCESO");
      desL = rs.getString("DSL_PROCESO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
          (desL != null)) {

        dataSalida = new DataIndivEnfermedad(
            rs.getString("CD_TVIGI"),
            rs.getString("CD_ENFCIE"),
            desL, rs.getString("IT_INDURG"));

      }
      else {
        dataSalida = new DataIndivEnfermedad(
            rs.getString("CD_TVIGI"),
            rs.getString("CD_ENFCIE"),
            des, rs.getString("IT_INDURG"));

      }
      listaSalida.addElement(dataSalida);
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return listaSalida;
  }

  // modificacion jlt 27/11/2001
  protected CLista traeEnfermedades_M(CLista param) throws Exception {
    DataIndivEnfermedad dataEntrada = (DataIndivEnfermedad) param.firstElement();
    CLista listaSalida = new CLista();
    Hashtable hash = (Hashtable) param.elementAt(1);

    //Trae enfermedades para la pesta�a de pCabecera: Enfermedades de tipo I, A

    sQuery = "select a.CD_ENFCIE, a.CD_TVIGI, b.DS_PROCESO," +
        " b.DSL_PROCESO, a.IT_INDURG from SIVE_ENFEREDO a, " +
        " SIVE_PROCESOS b" +
        " where (a.CD_TVIGI in ( ?, ? ) and " +
        " a.CD_ENFCIE = b.CD_ENFCIE)" +
        //  " a.IT_BAJA = 'N')" +
        " order by b.DS_PROCESO"; //^^

    st = con.prepareStatement(sQuery);
    st.setString(1, "I");
    st.setString(2, "A");

    rs = st.executeQuery();
    String des = "";
    String desL = "";

    while (rs.next()) {
      des = rs.getString("DS_PROCESO");
      desL = rs.getString("DSL_PROCESO");

      if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
          (desL != null)) {

        dataSalida = new DataIndivEnfermedad(
            rs.getString("CD_TVIGI"),
            rs.getString("CD_ENFCIE"),
            desL, rs.getString("IT_INDURG"));

      }
      else {
        dataSalida = new DataIndivEnfermedad(
            rs.getString("CD_TVIGI"),
            rs.getString("CD_ENFCIE"),
            des, rs.getString("IT_INDURG"));

      }
      listaSalida.addElement(dataSalida);
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return listaSalida;
  }

} //fin clase
