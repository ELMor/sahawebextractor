package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import notdata.DataMunicipioEDO;
import sapp.DBServlet;

public class SrvMunicipio
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletSELECCION_MUNICIPIO_X_CODIGO = 9;
  final int servletOBTENER_MUNICIPIO_X_CODIGO = 10;
  final int servletSELECCION_MUNICIPIO_X_DESCRIPCION = 11;
  final int servletOBTENER_MUNICIPIO_X_DESCRIPCION = 12;

  // par�metros
  DataMunicipioEDO dataMunLinea = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "", sFiltro = "";

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // objetos de datos
    CLista data = new CLista();
    DataMunicipioEDO dataMun = null;
    DataMunicipioEDO entMun = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      switch (opmode) {

        case servletOBTENER_MUNICIPIO_X_CODIGO:
        case servletOBTENER_MUNICIPIO_X_DESCRIPCION:

          entMun = (DataMunicipioEDO) param.firstElement();

          // query
          if (opmode == servletOBTENER_MUNICIPIO_X_CODIGO) {

            sQuery = "SELECT CD_MUN, DS_MUN, CD_PROV, " +
                " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS" +
                " FROM SIVE_MUNICIPIO " +
                " WHERE " +
                " CD_MUN = ? AND CD_PROV LIKE ?";
          }
          else {

            sQuery = "SELECT CD_MUN, DS_MUN, CD_PROV, " +
                " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS" +
                " FROM SIVE_MUNICIPIO " +
                " WHERE " +
                " DS_MUN = ? AND CD_PROV LIKE ?";
          }

          st = con.prepareStatement(sQuery);

          // filtro
          st.setString(iValor, entMun.getCodMun().trim());
          iValor++;
          st.setString(iValor, entMun.getCodProv().trim() + "%");
          iValor++;

          break;

          //TRAMAR!!!
        case servletSELECCION_MUNICIPIO_X_CODIGO:
        case servletSELECCION_MUNICIPIO_X_DESCRIPCION:

          entMun = (DataMunicipioEDO) param.firstElement();

          // query
          // ARG: upper (15/5/02)
          if (param.getFilter().length() > 0) {

            if (opmode == servletSELECCION_MUNICIPIO_X_CODIGO) {
              sQuery = "SELECT CD_MUN, DS_MUN, CD_PROV, " +
                  " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS " +
                  " FROM SIVE_MUNICIPIO " +
                  " WHERE " +
                  " CD_MUN LIKE ? AND CD_MUN > ? " +
                  " AND CD_PROV LIKE ? order by CD_MUN";

            }
            else {
              sQuery = "SELECT CD_MUN, DS_MUN, CD_PROV, " +
                  " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS " +
                  " FROM SIVE_MUNICIPIO " +
                  " WHERE " +
                  " upper(DS_MUN) LIKE upper(?) AND upper(DS_MUN) > upper(?) " +
                  " AND CD_PROV LIKE ? order by DS_MUN";
            }
          }
          else {
            if (opmode == servletSELECCION_MUNICIPIO_X_CODIGO) {
              sQuery = "SELECT CD_MUN, DS_MUN, CD_PROV, " +
                  " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS " +
                  " FROM SIVE_MUNICIPIO " +
                  " WHERE " +
                  " CD_MUN LIKE ?  " +
                  " AND CD_PROV LIKE ? order by CD_MUN";

            }
            else {
              sQuery = "SELECT CD_MUN, DS_MUN, CD_PROV, " +
                  " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS " +
                  " FROM SIVE_MUNICIPIO " +
                  " WHERE " +
                  " upper(DS_MUN) LIKE upper(?)  " +
                  " AND CD_PROV LIKE ? order by DS_MUN";
            }
          }

          st = con.prepareStatement(sQuery);

          // filtro normal
          if (opmode == servletSELECCION_MUNICIPIO_X_CODIGO) {
            st.setString(iValor, entMun.getCodMun().trim() + "%");
          }
          else {
            st.setString(iValor, "%" + entMun.getCodMun().trim() + "%");
          }
          iValor++;

          if (param.getFilter().length() > 0) {
            st.setString(iValor, param.getFilter());
            iValor++;
          }

          st.setString(iValor, entMun.getCodProv().trim() + "%");
          iValor++;

          break;
      }
      // Acabo el switch y ejecuto la query

      rs = st.executeQuery();

      //valores nulos:
      String cd_prov = "";
      String cd_nivel_1 = "";
      String cd_nivel_2 = "";
      String cd_zbs = "";

      String des = "";
      String desL = "";

      while (rs.next()) {

        //TRAMAR ---------------- ------------- --------- --------
        if ( (opmode == servletSELECCION_MUNICIPIO_X_CODIGO) ||
            (opmode == servletSELECCION_MUNICIPIO_X_DESCRIPCION)) {

          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            if (opmode == servletSELECCION_MUNICIPIO_X_CODIGO) {
              data.setFilter( ( (DataMunicipioEDO) data.lastElement()).
                             getCodMun());
            }
            else {
              data.setFilter( ( (DataMunicipioEDO) data.lastElement()).
                             getDesMun());

            }
            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
        } //------- tramar ---------- ------- --------- -------

        //nulos
        cd_prov = rs.getString("CD_PROV");
        cd_nivel_1 = rs.getString("CD_NIVEL_1");
        cd_nivel_2 = rs.getString("CD_NIVEL_2");
        cd_zbs = rs.getString("CD_ZBS");

        if (cd_prov == null) {
          cd_prov = "";
        }
        if (cd_nivel_1 == null) {
          cd_nivel_1 = "";
        }
        if (cd_nivel_2 == null) {
          cd_nivel_2 = "";
        }
        if (cd_zbs == null) {
          cd_zbs = "";

        }
        dataMun = new DataMunicipioEDO(
            rs.getString("CD_MUN"), rs.getString("DS_MUN"),
            cd_prov, "",
            cd_nivel_1, "",
            cd_nivel_2, "",
            cd_zbs, "",
            "");

        // a�ade un nodo
        data.addElement(dataMun);
        i++;
      } //fin de while ppal

      rs.close();
      rs = null;
      st.close();
      st = null;

      for (int t = 0; t < data.size(); t++) {

        dataMun = (DataMunicipioEDO) data.elementAt(t);

        cd_prov = dataMun.getCodProv();
        cd_nivel_1 = dataMun.getCodNivel1();
        cd_nivel_2 = dataMun.getCodNivel2();
        cd_zbs = dataMun.getCodZBS();

        //des de provincia y ca
        if (!cd_prov.equals("")) {
          sQuery = " SELECT CD_CA, DS_PROV, DSL_PROV "
              + " FROM SIVE_PROVINCIA  "
              + " WHERE CD_PROV = ? ";

          st = con.prepareStatement(sQuery);
          st.setString(1, cd_prov);
          rs = st.executeQuery();

          while (rs.next()) {
            des = rs.getString("DS_PROV");
            desL = rs.getString("DSL_PROV");

            //provincia
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (desL != null)) {
              dataMun.sDesProv = desL;
            }
            else {
              dataMun.sDesProv = des;

              //ca
            }
            dataMun.sCodCa = rs.getString("CD_CA");

          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        } //FIN DE IF PROV - CA
      } //fin del for

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      data = null;
      throw ex;

    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    data.trimToSize();

    return data;
  }

}