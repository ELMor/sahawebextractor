/*
 *  Persona             Fecha                 Accion
 *   JRM              13/06/2000              Modifica para adaptar
 *                                            la petici�n de equipo a ZBS
 *   JRM              11/09/2000              Hay que tener en cuenta la fecha
 *                                            en la que se da de alta el equipo
 *                                            notificador.
 */
package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import capp.CLista;
import notdata.DataEntradaEDO;
import notdata.DataNotifEDO;
import sapp.DBServlet;

/**
 * JRM modifica
 */
public class SrvMaestroEDO
    extends DBServlet {

  // modos de operaci�n del servlet
  //final int servletOBTENER_FLAGS_USU = 3;
  // Obtener los datos sin ZBS (Madrid)
  final int servletOBTENER_EQUIPO_X_CODIGO = 4;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION = 5;
  final int servletSELECCION_EQUIPO_X_CODIGO = 6;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION = 7;

  final int servletOBTENER_EQUIPO = 9;

  // JRM para peticion de ZBS
  // Obtener los datos con ZBS (CyL)
  final int servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS = 10;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION_CON_ZBS = 11;
  final int servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS = 12;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION_CON_ZBS = 13;

  // JRM2 para peticion de notificadores de baja
  final int servletOBTENER_EQUIPO_X_CODIGO_NOTIF = 20;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION_NOTIF = 21;
  final int servletSELECCION_EQUIPO_X_CODIGO_NOTIF = 22;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION_NOTIF = 23;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));

  // par�metros
  DataEntradaEDO dataEDO = null;
  DataEntradaEDO dataLinea = null;

  // ARG: Para usar fechas en las selects
  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);
    return TSFec;
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss

    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "", sFiltro = "";
    String sfc_ultact = "";
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;
    // Para las condiciones de la semana y a�o epidemiologico
    String strSemana = "";
    // ARG: Para las condiciones de fecha
    String strFechaNotif = "";

    // ARG: Para usar fechas en las selects
    java.sql.Timestamp fec = null;
    String sFec = "";

    // objetos de datos
    CLista data = new CLista();
    DataEntradaEDO ent = null;
    DataNotifEDO notif = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      ent = (DataEntradaEDO) param.firstElement();

      switch (opmode) {

        case servletOBTENER_EQUIPO:

          //AIC
          //ent       = (DataEntradaEDO) param.firstElement();
          sQuery = "select CD_E_NOTIF,DS_E_NOTIF, " +
              " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
              " CD_OPE, FC_ALTA, FC_ULTACT, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
              " where CD_E_NOTIF = ?";

          st = con.prepareStatement(sQuery);

          st.setString(1, ent.getCod().trim());

          break;

        case servletOBTENER_EQUIPO_X_CODIGO:
        case servletOBTENER_EQUIPO_X_DESCRIPCION:

          //AIC
          //ent       = (DataEntradaEDO) param.firstElement();

          strSemana = condicionFechas(ent);
          // ARG: Fecha de notificacion (Se almacena en la fecha de alta!!!)
          strFechaNotif = ent.getFC_ALTA();

          // el nivel1 siempre va a venir relleno, porque incluso
          // al usuario de CA se le obliga a introducir nivel1
          if ( (ent.getNivel2().trim().equals("")) ||
              (ent.getNivel2() == null)) {
            sFiltro = "";
          }
          else {
            sFiltro = " and CD_NIVEL_2 = ?";

            // ARG: Se filtra por la fecha de notificacion cuando el notificador
            //      esta dado de baja si nos llega una fecha de notificacion
            //if (!strFechaNotif.equals(""))
            //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
            //   "(IT_BAJA = 'S' and (FC_ALTA <= to_date('" + strFechaNotif + "','dd/MM/yyyy') " +
            //   " and FC_ULTACT >= to_date('" + strFechaNotif + "','dd/MM/yyyy'))))";

            //AIC el filtro lo hacemos despu�s.
            //if (!strFechaNotif.equals(""))
            //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
            //   "(IT_BAJA = 'S' and (FC_ALTA <= ? and FC_ULTACT >= ? )))";

            // query
          }
          if (opmode == servletOBTENER_EQUIPO_X_CODIGO) {

            sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                " where CD_E_NOTIF = ?";

          }
          else {

            // opmode = servletOBTENER_EQUIPO_X_DESCRIPCION
            sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                " where DS_E_NOTIF = ?";

          }
          if (sFiltro.length() > 0) {
            sQuery = sQuery + sFiltro;

            // ARG: Ahora si se pueden seleccionar los que esten dados de baja
            // sQuery = sQuery + " and CD_NIVEL_1 = ? and IT_BAJA = ? " +
            //          strSemana + " order by CD_E_NOTIF";
          }
          sQuery = sQuery + " and CD_NIVEL_1 = ?" +
              strSemana + " order by CD_E_NOTIF";

          //# // System_out.println("GUI sQuery "+sQuery);

          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCod().trim());
          iValor++;

          // filtro de nivel2
          if ( (sFiltro.length() > 0) &&
              (ent.getNivel2() != null) &&
              ! (ent.getNivel2().trim().equals(""))
              ) {
            st.setString(iValor, ent.getNivel2().trim());
            iValor++;
          }

          //AIC Ya no procede...
          // ARG: Se a�ade la fecha por la que hay que filtrar, si procede
          //if (!strFechaNotif.equals(""))
          //{
          //   sFec = strFechaNotif + " 00:00:00";
          //   fec  = (java.sql.Timestamp)cadena_a_timestamp(sFec);
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //}

          st.setString(iValor, ent.getNivel1().trim());
          iValor++;

          // ARG: Ahora si se pueden seleccionar los que esten dados de baja
          // st.setString(iValor, "N");
          // iValor++;

          break;

          //TRAMAR ****
        case servletSELECCION_EQUIPO_X_CODIGO:
        case servletSELECCION_EQUIPO_X_DESCRIPCION:

          //AIC
          //ent       = (DataEntradaEDO) param.firstElement();
          strSemana = condicionFechas(ent);
          // ARG: Fecha de notificacion (Se almacena en la fecha de alta!!!)
          strFechaNotif = ent.getFC_ALTA();

          // el nivel1 siempre va a venir relleno, porque incluso
          // al usuario de CA se le obliga a introducir nivel1
          if ( (ent.getNivel2().trim().equals("")) || (ent.getNivel2() == null)) {
            sFiltro = "";
          }
          else {
            sFiltro = " and CD_NIVEL_2 = ?";

            // ARG: Se filtra por la fecha de notificacion cuando el notificador
            //      esta dado de baja si nos llega una fecha de notificacion
            //if (!strFechaNotif.equals(""))
            //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
            //   "(IT_BAJA = 'S' and (FC_ALTA <= to_date('" + strFechaNotif + "','dd/MM/yyyy') " +
            //   " and FC_ULTACT >= to_date('" + strFechaNotif + "','dd/MM/yyyy'))))";

            //AIC el filtro lo hacemos despu�s....
            //if (!strFechaNotif.equals(""))
            //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
            //   "(IT_BAJA = 'S' and (FC_ALTA <= ? and FC_ULTACT >= ? )))";

            // query

            //tramar
            // ARG: upper (13/5/02)
          }
          if (param.getFilter().length() > 0) {

            if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? and CD_E_NOTIF > ? ";

            }
            else {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where upper(DS_E_NOTIF) like upper(?) and upper(DS_E_NOTIF) > upper(?) ";
            }
          }
          else {
            if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? ";

            }
            else {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where upper(DS_E_NOTIF) like upper(?) ";
            }
          }

          if (sFiltro.length() > 0) {
            sQuery = sQuery + sFiltro;

            // ARG: Ahora si se pueden seleccionar los que esten dados de baja
            //sQuery = sQuery + " and CD_NIVEL_1 = ? and IT_BAJA = ? " + strSemana;
          }
          sQuery = sQuery + " and CD_NIVEL_1 = ? " + strSemana;

          //order by
          if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
            sQuery = sQuery + " order by CD_E_NOTIF";
          }
          else {
            sQuery = sQuery + " order by DS_E_NOTIF";

            //pregunta
          }
          st = con.prepareStatement(sQuery);

          // filtro normal
          if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
            st.setString(iValor, ent.getCod().trim() + "%");
          }
          else {
            st.setString(iValor, "%" + ent.getCod().trim() + "%");
          }
          iValor++;

          //trama
          if (param.getFilter().length() > 0) {
            st.setString(iValor, param.getFilter().trim());
            iValor++;
          }

          // filtro de nivel2
          if ( (sFiltro.length() > 0) && (ent.getNivel2() != null)
              &&
              ! (ent.getNivel2().trim().equals(""))
              ) {
            //if (sFiltro.length() > 0) {
            st.setString(iValor, ent.getNivel2().trim());
            iValor++;
          }

          // ARG: Se a�ade la fecha por la que hay que filtrar, si procede
          // AIC Ya no procede...
          //if (!strFechaNotif.equals(""))
          //{
          //   sFec = strFechaNotif + " 00:00:00";
          //   fec  = (java.sql.Timestamp)cadena_a_timestamp(sFec);
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //}

          st.setString(iValor, ent.getNivel1().trim());
          iValor++;

          // ARG: Ahora si se pueden seleccionar los que esten dados de baja
          //st.setString(iValor, "N");
          //iValor++;

          break;

          // JRM PARA CyL, Modificado por JRM2
        case servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS:
        case servletOBTENER_EQUIPO_X_DESCRIPCION_CON_ZBS:

          // en nivel2 tenemos algo de la forma xx/yy siendo
          // xx el distrito e yy la zona b�sica de salud.
          //AIC
          //ent       = (DataEntradaEDO) param.firstElement();
          strSemana = condicionFechas(ent);
          // ARG: Fecha de notificacion (Se almacena en la fecha de alta!!!)
          strFechaNotif = ent.getFC_ALTA();

          String Distrito;
          String ZBS;

          // el nivel1 siempre va a venir relleno, porque incluso
          // al usuario de CA se le obliga a introducir nivel1
          if ( (ent.getNivel2().trim().equals("")) ||
              (ent.getNivel2() == null)) {
            sFiltro = "";
            Distrito = "";
            ZBS = "";
          }
          else {
            // El distrito y la zona b�sica de salud vienen como algo asi XXYY
            // donde XX es el distrito e YY la zona b�sica de salud
            Distrito = ent.getNivel2().substring(0, 2);
            ZBS = ent.getNivel2().substring(2, 4);
            sFiltro = " and CD_NIVEL_2 = '" + Distrito + "' and CD_ZBS = '" +
                ZBS + "'";
          }

          // ARG: Se filtra por la fecha de notificacion cuando el notificador
          //      esta dado de baja si nos llega una fecha de notificacion
          //if (!strFechaNotif.equals(""))
          //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
          //   "(IT_BAJA = 'S' and (FC_ALTA <= to_date('" + strFechaNotif + "','dd/MM/yyyy') " +
          //   " and FC_ULTACT >= to_date('" + strFechaNotif + "','dd/MM/yyyy'))))";

          //AIC El filtro lo hacemos despu�s.
          //if (!strFechaNotif.equals(""))
          //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
          //   "(IT_BAJA = 'S' and (FC_ALTA <= ? and FC_ULTACT >= ? )))";

          // query
          if (opmode == servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS) {

            sQuery = "select CD_E_NOTIF, decode (IT_BAJA,'S',DS_E_NOTIF || ' (B)','N',DS_E_NOTIF) as DS_E_NOTIF, " +
                " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                " where CD_E_NOTIF = ?";

          }
          else {

            sQuery = "select CD_E_NOTIF, decode (IT_BAJA,'S',DS_E_NOTIF || ' (B)','N',DS_E_NOTIF) as DS_E_NOTIF, " +
                " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                " where DS_E_NOTIF = ?";

          }
          if (sFiltro.length() > 0) {
            sQuery = sQuery + sFiltro;

          }
          sQuery = sQuery + " and CD_NIVEL_1 = ? " +
              strSemana +
              " order by CD_E_NOTIF";

          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCod().trim());
          iValor++;

          //AIC Ya no procede...
          // ARG: Se a�ade la fecha por la que hay que filtrar, si procede
          //if (!strFechaNotif.equals(""))
          //{
          //   sFec = strFechaNotif + " 00:00:00";
          //   fec  = (java.sql.Timestamp)cadena_a_timestamp(sFec);
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //}

          st.setString(iValor, ent.getNivel1().trim());
          iValor++;

          break;

          // JRM
        case servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS:
        case servletSELECCION_EQUIPO_X_DESCRIPCION_CON_ZBS:

          // en nivel2 tenemos algo de la forma xx/yy siendo
          // xx el distrito e yy la zona b�sica de salud.
          //AIC
          //ent       = (DataEntradaEDO) param.firstElement();
          strSemana = condicionFechas(ent);
          // ARG: Fecha de notificacion (Se almacena en la fecha de alta!!!)
          strFechaNotif = ent.getFC_ALTA();

          // el nivel1 siempre va a venir relleno, porque incluso
          // al usuario de CA se le obliga a introducir nivel1
          if ( (ent.getNivel2().trim().equals("")) ||
              (ent.getNivel2() == null)) {
            sFiltro = "";
            Distrito = "";
            ZBS = "";
          }
          else {
            // El distrito y la zona b�sica de salud vienen como algo asi XXYY
            // donde XX es el distrito e YY la zona b�sica de salud
            Distrito = ent.getNivel2().substring(0, 2);
            ZBS = ent.getNivel2().substring(2, 4);
            sFiltro = " and CD_NIVEL_2 = '" + Distrito + "' and CD_ZBS = '" +
                ZBS + "'";
          }

          // ARG: Se filtra por la fecha de notificacion cuando el notificador
          //      esta dado de baja si nos llega una fecha de notificacion
          //if (!strFechaNotif.equals(""))
          //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
          //   "(IT_BAJA = 'S' and (FC_ALTA <= to_date('" + strFechaNotif + "','dd/MM/yyyy') " +
          //   " and FC_ULTACT >= to_date('" + strFechaNotif + "','dd/MM/yyyy'))))";

          //AIC El filtro lo hacemos despu�s.
          //if (!strFechaNotif.equals(""))
          //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
          //   "(IT_BAJA = 'S' and (FC_ALTA <= ? and FC_ULTACT >= ? )))";

          // query

          //tramar
          // ARG: upper (13/5/02)
          if (param.getFilter().length() > 0) {

            if (opmode == servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS) {
              sQuery = "select CD_E_NOTIF, decode (IT_BAJA,'S',DS_E_NOTIF || ' (B)','N',DS_E_NOTIF) as DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? and CD_E_NOTIF > ? ";

            }
            else {
              sQuery = "select CD_E_NOTIF, decode (IT_BAJA,'S',DS_E_NOTIF || ' (B)','N',DS_E_NOTIF) as DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where upper(DS_E_NOTIF) like upper(?) and upper(DS_E_NOTIF) > upper(?) ";
            }
          }
          else {
            if (opmode == servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS) {
              sQuery = "select CD_E_NOTIF, decode (IT_BAJA,'S',DS_E_NOTIF || ' (B)','N',DS_E_NOTIF) as DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? ";

            }
            else {
              sQuery = "select CD_E_NOTIF, decode (IT_BAJA,'S',DS_E_NOTIF || ' (B)','N',DS_E_NOTIF) as DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where upper(DS_E_NOTIF) like upper(?) ";

            }

          }

          if (sFiltro.length() > 0) {
            sQuery = sQuery + sFiltro;

          }
          sQuery = sQuery + " and CD_NIVEL_1 = ? " + strSemana;

          //order by
          if (opmode == servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS) {
            sQuery = sQuery + " order by CD_E_NOTIF";
          }
          else {
            sQuery = sQuery + " order by DS_E_NOTIF";

            //pregunta
          }
          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCod().trim() + "%");
          iValor++;

          //trama
          if (param.getFilter().length() > 0) {
            st.setString(iValor, param.getFilter().trim());
            iValor++;
          }

          // ARG: Se a�ade la fecha por la que hay que filtrar, si procede
          //if (!strFechaNotif.equals(""))
          //{
          //   sFec = strFechaNotif + " 00:00:00";
          //   fec  = (java.sql.Timestamp)cadena_a_timestamp(sFec);
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //}

          st.setString(iValor, ent.getNivel1().trim());
          iValor++;

          break;

          // JRM2 Para ver los Notificadores de Madrid
        case servletOBTENER_EQUIPO_X_CODIGO_NOTIF:
        case servletOBTENER_EQUIPO_X_DESCRIPCION_NOTIF:

          //AIC
          //ent       = (DataEntradaEDO) param.firstElement();
          strSemana = condicionFechas(ent);

          // el nivel1 siempre va a venir relleno, porque incluso
          // al usuario de CA se le obliga a introducir nivel1
          if ( (ent.getNivel2().trim().equals("")) ||
              (ent.getNivel2() == null)) {
            sFiltro = "";
          }
          else {
            sFiltro = " and CD_NIVEL_2 = ?";

            // query
          }
          if (opmode == servletOBTENER_EQUIPO_X_CODIGO_NOTIF) {

            sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                " where CD_E_NOTIF = ?";

          }
          else {

            // opmode = servletOBTENER_EQUIPO_X_DESCRIPCION_NOTIF
            sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                " where DS_E_NOTIF = ?";

          }
          if (sFiltro.length() > 0) {
            sQuery = sQuery + sFiltro;

          }
          sQuery = sQuery + " and CD_NIVEL_1 = ? " +
              strSemana + " order by CD_E_NOTIF";

          //# // System_out.println("GUI sQuery "+sQuery);

          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCod().trim());
          iValor++;

          // filtro de nivel2
          if ( (sFiltro.length() > 0) && (ent.getNivel2() != null)
              &&
              ! (ent.getNivel2().trim().equals(""))
              ) {
            //if (sFiltro.length() > 0) {
            st.setString(iValor, ent.getNivel2().trim());
            iValor++;
          }

          st.setString(iValor, ent.getNivel1().trim());
          iValor++;

          break;

          //TRAMAR ****
        case servletSELECCION_EQUIPO_X_CODIGO_NOTIF:
        case servletSELECCION_EQUIPO_X_DESCRIPCION_NOTIF:

          //AIC
          //ent       = (DataEntradaEDO) param.firstElement();

          strSemana = condicionFechas(ent);
          // ARG: Fecha de notificacion (Se almacena en la fecha de alta!!!)
          strFechaNotif = ent.getFC_ALTA();

          // el nivel1 siempre va a venir relleno, porque incluso
          // al usuario de CA se le obliga a introducir nivel1
          if ( (ent.getNivel2().trim().equals("")) || (ent.getNivel2() == null)) {
            sFiltro = "";
          }
          else {
            sFiltro = " and CD_NIVEL_2 = ?";

            // ARG: Se filtra por la fecha de notificacion cuando el notificador
            //      esta dado de baja si nos llega una fecha de notificacion
            //if (!strFechaNotif.equals(""))
            //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
            //   "(IT_BAJA = 'S' and (FC_ALTA <= to_date('" + strFechaNotif + "','dd/MM/yyyy') " +
            //   " and FC_ULTACT >= to_date('" + strFechaNotif + "','dd/MM/yyyy'))))";

            //AIC El filtro lo hacemos despu�s.
            //if (!strFechaNotif.equals(""))
            //   sFiltro = sFiltro + " and ((IT_BAJA = 'N') or " +
            //   "(IT_BAJA = 'S' and (FC_ALTA <= ? and FC_ULTACT >= ? )))";

            // query

            //tramar
            // ARG: upper (13/5/02)
          }
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_EQUIPO_X_CODIGO_NOTIF) {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? and CD_E_NOTIF > ? ";
            }
            else {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where upper(DS_E_NOTIF) like upper(?) and upper(DS_E_NOTIF) > upper(?) ";
            }
          }
          else {
            if (opmode == servletSELECCION_EQUIPO_X_CODIGO_NOTIF) {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? ";
            }
            else {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
                  " CD_OPE, FC_ALTA, FC_ULTACT, IT_BAJA, CD_ANOEPI, CD_SEMEPI from SIVE_E_NOTIF " +
                  " where upper(DS_E_NOTIF) like upper(?) ";
            }
          }

          if (sFiltro.length() > 0) {
            sQuery = sQuery + sFiltro;

          }
          sQuery = sQuery + " and CD_NIVEL_1 = ? " + strSemana;

          //order by
          if (opmode == servletSELECCION_EQUIPO_X_CODIGO_NOTIF) {
            sQuery = sQuery + " order by CD_E_NOTIF";
          }
          else {
            sQuery = sQuery + " order by DS_E_NOTIF";

            //pregunta
          }
          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCod().trim() + "%");
          iValor++;

          //trama
          if (param.getFilter().length() > 0) {
            st.setString(iValor, param.getFilter().trim());
            iValor++;
          }

          // filtro de nivel2
          //if (sFiltro.length() > 0) {
          if ( (sFiltro.length() > 0) && (ent.getNivel2() != null)
              &&
              ! (ent.getNivel2().trim().equals(""))
              ) {
            st.setString(iValor, ent.getNivel2().trim());
            iValor++;
          }

          // ARG: Se a�ade la fecha por la que hay que filtrar, si procede
          //if (!strFechaNotif.equals(""))
          //{
          //   sFec = strFechaNotif + " 00:00:00";
          //   fec  = (java.sql.Timestamp)cadena_a_timestamp(sFec);
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //   st.setTimestamp(iValor,fec);
          //   iValor++;
          //}

          st.setString(iValor, ent.getNivel1().trim());
          iValor++;

          break;
          // FIN JRM2 Para los Notificadores de Madrid
      }

      // System_out.println(sQuery);

//resto de la querys para equipos***************************
      if ( (opmode == servletSELECCION_EQUIPO_X_CODIGO) ||
          (opmode == servletSELECCION_EQUIPO_X_DESCRIPCION) ||
          (opmode == servletOBTENER_EQUIPO_X_CODIGO) ||
          (opmode == servletOBTENER_EQUIPO_X_DESCRIPCION) ||
          (opmode == servletOBTENER_EQUIPO) ||
          (opmode == servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS) ||
          (opmode == servletSELECCION_EQUIPO_X_DESCRIPCION_CON_ZBS) ||
          (opmode == servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS) ||
          (opmode == servletOBTENER_EQUIPO_X_DESCRIPCION_CON_ZBS) ||
          (opmode == servletSELECCION_EQUIPO_X_CODIGO_NOTIF) ||
          (opmode == servletSELECCION_EQUIPO_X_DESCRIPCION_NOTIF) ||
          (opmode == servletOBTENER_EQUIPO_X_CODIGO_NOTIF) ||
          (opmode == servletOBTENER_EQUIPO_X_DESCRIPCION_NOTIF)
          ) {

        //AIC Eliminar
        //// System_out.println( sQuery);

        rs = st.executeQuery();

        java.sql.Timestamp fecRecu = null;
        String sfecRecAlta = "";
        String sfecRecActu = "";
        while (rs.next()) {
          //# // System_out.println("GUI dentro de while");
          //tramar ---
          if ( (opmode == servletSELECCION_EQUIPO_X_CODIGO) ||
              (opmode == servletSELECCION_EQUIPO_X_DESCRIPCION) ||
              (opmode == servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS) ||
              (opmode == servletSELECCION_EQUIPO_X_DESCRIPCION_CON_ZBS)
              ) {

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);

              if ( (opmode == servletSELECCION_EQUIPO_X_CODIGO) ||
                  (opmode == servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS)) {
                data.setFilter( ( (DataEntradaEDO) data.lastElement()).getCod());
              }
              else {
                data.setFilter( ( (DataEntradaEDO) data.lastElement()).getDes());

              }
              break;
            }

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
          } //tramar ------

          //NM_NOTIFT puede ser nulo, ptt lo recuperamos con getString
          //NOTA: NM_NOTIFT ocupa el lugar de cod_nivel_1 en la estructura de datos!!!
          //NOTA: 28/04 -> lo sacamos de notif_sem

          /*sqlFec = rs.getDate("FC_ULTACT");
                   sfc_ultact = formater.format(sqlFec);*/
          // ARG: Fecha de alta
          fecRecu = rs.getTimestamp("FC_ALTA");
          sfecRecAlta = timestamp_a_cadena(fecRecu);
          // ARG: Fecha de ultima actualizacion
          fecRecu = rs.getTimestamp("FC_ULTACT");
          sfecRecActu = timestamp_a_cadena(fecRecu);

              /* JRM2  Modificando para devolver en la descripcion, si se est� de baja
               HAY QUE MIRAR EL CAMPO IT_Baja, y si est� a TRUE, se le a�ade "(B)"
                   JRM2*/

          //AIC Eliminar
          //// System_out.println("SrvMaestro : CD_E_NOTIF " + rs.getString("CD_E_NOTIF"));
          //Si no es v�lido se salta la introducci�n del registro y pasa al siguiente.
          if (!esValido(rs.getString("CD_E_NOTIF"), rs.getString("CD_ANOEPI"),
                        rs.getString("CD_SEMEPI"), rs.getString("IT_BAJA"), ent)) {
            continue;
          }

          dataEDO = new DataEntradaEDO(
              rs.getString("CD_E_NOTIF"),
              rs.getString("DS_E_NOTIF"),
              "", //rs.getString ("NM_NOTIFT"), //en nivel 1 va nm_notift
              rs.getString("CD_NIVEL_2"),
              rs.getString("CD_CENTRO"),
              "", //ds_centro
              "", //it_cobertura
              null, //nm_ntotreal
              rs.getString("CD_OPE"), //datos del equipo
              sfecRecAlta,
              sfecRecActu);

          // a�ade un nodo
          data.addElement(dataEDO);
          i++;
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        for (int j = 0; j < data.size(); j++) {
          dataLinea = (DataEntradaEDO) data.elementAt(j);

          //CENTRO***************************************************************
          sQuery = "select CD_CENTRO, DS_CENTRO, IT_COBERTURA "
              + " from SIVE_C_NOTIF where CD_CENTRO = ?";

          st = con.prepareStatement(sQuery);
          st.setString(1, dataLinea.getCodCentro());
          rs = st.executeQuery();

          while (rs.next()) {
            // obtiene el resto de campos para cada elemento de data
            dataLinea.sCodCentro = rs.getString("CD_CENTRO");
            dataLinea.sDesCentro = rs.getString("DS_CENTRO");
            dataLinea.sDato7 = rs.getString("IT_COBERTURA");

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //# // System_out.println("GUI detras de centro");

          if (opmode != servletOBTENER_EQUIPO) {
            sQuery = "select NM_NTOTREAL, NM_NNOTIFT " +
                " from SIVE_NOTIF_SEM " +
                " where (CD_E_NOTIF = ? and " +
                " CD_ANOEPI = ? and " +
                " CD_SEMEPI = ? )";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataLinea.getCod());
            st.setString(2, ent.getAno());
            st.setString(3, ent.getSem());

            rs = st.executeQuery();

            dataLinea.sDato8 = null;
            while (rs.next()) {
              dataLinea.sDato8 = rs.getString("NM_NTOTREAL"); //con getString, puede ser null
              dataLinea.sNivel1 = rs.getString("NM_NNOTIFT"); //con getString, puede ser null
            }
            //si la clave no esta en notif_sem, devolvemos nulo en este campo
            rs.close();
            rs = null;
            st.close();
            st = null;

          } //fin ifpegote

        } //for

      } //fin del if de equipos

      //# // System_out.println("GUI detras de saltar pegote");
      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      data = null;
      throw ex;

    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    data.trimToSize();

    return data;
  }

  /**
   * Tiene en cuenta la semana y el a�o epidemiologico a la hora de mostrar
   * los notificadores
   * @return String con la condicion que se debe a�adir a la select.
   */
  String condicionFechas(DataEntradaEDO param) {
    String semana = param.getSem();
    String anno = param.getAno();
    String strAux = "";

    //ARG: Hace una comprobaci�n por a�o y semana
    strAux = strAux + " and ((CD_ANOEPI < '" + anno + "') or ";
    if (!semana.trim().equals("")) {
      strAux = strAux + "(CD_ANOEPI = '" + anno + "' and CD_SEMEPI <= '" +
          rellenoCeros(semana, 2) + "'))";
    }
    else {
      //ARG: Si no hay semana, se compara con la semana 52.
      //     Es lo mismo que no tenerla en cuenta, ya que las semanas son <= 52
      strAux = strAux + "(CD_ANOEPI = '" + anno + "' and CD_SEMEPI <= '52' ))";
    }

    return strAux;
  }

  /**
   * Mete ceros a la izquierda de Cadena hasta alcanzar el tama�o especificado en Maximo.
   * @param     cadena: Cadena a rellenar con ceros
   * @param     maximo: Maximo longitud de la cadena despues de a�adir ceros
   */
  String rellenoCeros(String cadena, int maximo) {
    int l;
    int i;
    String cadAux = "";

    l = cadena.length();
    if (l < maximo) {
      for (i = 1; i <= maximo - l; i++) {
        cadAux = cadAux + '0';
      }
      cadAux = cadAux + cadena;

      return cadAux;
    }
    else {

      return cadena;
    }
  }

  //AIC
  // devuelve true si la semana y a�o de notificaci�n son validos para el notificador
  // solo tiene sentido si el notificador est� dado de baja.
  boolean esValido(String cd_notificador, String anyoAlta, String semAlta,
                   String it_baja,
                   DataEntradaEDO entrada) throws Exception {
    boolean resultado = false;
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int iSem;
    int iAnyo;
    int iSemMax;
    int iAnyoMax;
    int iAnyoAlta;
    int iSemAlta;
    //String sFechaNotif;
    //SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
    //java.util.Date fNotif;
    //java.util.Date fAlta;
    String query;

    iSem = Integer.parseInt(entrada.getSem());
    iAnyo = Integer.parseInt(entrada.getAno());

    iSemAlta = Integer.parseInt(semAlta);
    iAnyoAlta = Integer.parseInt(anyoAlta);

    if (it_baja.equals("N")) {
      resultado = true;
    }
    else {
      if ( (iAnyoAlta > iAnyo) || ( (iAnyoAlta == iAnyo) && (iSemAlta > iSem))) {
        resultado = false;
        // System_out.println(cd_notificador + " SrvMaestro fechas de Alta-Notificacion: " + semAlta + anyoAlta  + "    " + entrada.getSem() + entrada.getAno());
      }
      else {
        con = openConnection();
        query =
            " Select max(cd_semepi) as SEM, cd_anoepi as ANYO from sive_notif_sem " +
            " where cd_e_notif = '" + cd_notificador + "'" +
            " and cd_anoepi in( select max(cd_anoepi) " +
            " from sive_notif_sem where cd_e_notif = '" + cd_notificador +
            "') " +
            " group by cd_anoepi ";
        st = con.prepareStatement(query);

        rs = st.executeQuery();

        if (rs.next()) {
          iSemMax = rs.getInt("SEM");
          iAnyoMax = rs.getInt("ANYO");

          resultado = (iAnyo < iAnyoMax) ||
              ( (iAnyo == iAnyoMax) && (iSem <= iSemMax));

          // System_out.println("SrvMaestro  Veamos : " + cd_notificador + " " + String.valueOf(resultado) + " Anyo-AnyoMax: " +
          //                   String.valueOf(iAnyo) + "-" + String.valueOf(iAnyoMax) + " Sem-SemMax" +
          //                   String.valueOf(iSem) + "-" + String.valueOf(iSemMax));

        }
        else { //NO PUEDE SER PERO....
          resultado = false;
          // System_out.println("SrvMaestro " + cd_notificador + " Esto no deber�a estar pasando: " + query);
        }

        rs.close();
        st.close();
        closeConnection(con);

      }
    }

    return resultado;
  }

}