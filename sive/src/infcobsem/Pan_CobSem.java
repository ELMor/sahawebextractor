package infcobsem;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.SystemColor;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import infcobper.ParamC11_12;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import zbs.DataZBS2;

public class Pan_CobSem
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/grafico.gif"};

  protected CCargadorImagen imgs = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  public ParamC11_12 paramC2;

  protected int modoOperacion = modoNORMAL;

  protected Pan_InfCobSem informe = null;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETCentro = "servlet/SrvCN";
  final String strSERVLETEnf = "servlet/SrvEnfVigi";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblFecha = new Label();
  Label lblArea = new Label();
  TextField txtCodArea = new CCampoCodigo();
  ButtonControl btnCtrlBuscarArea = new ButtonControl();
  TextField txtDesArea = new TextField();
  Label lblAgrupar = new Label();
  PanFechas panelFecha;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  txt_keyAdapter txtTextAdapter = new txt_keyAdapter(this);
  actionListener btnActionListener = new actionListener(this);
  CheckboxGroup checkboxAgrupar = new CheckboxGroup();
  Checkbox cbEquipo = new Checkbox();
  Checkbox cbCentro = new Checkbox();
  Label lblDist = new Label();
  TextField txtCodDist = new CCampoCodigo();
  ButtonControl btnCtrlBuscarDist = new ButtonControl();
  TextField txtDesDist = new TextField();

  public Pan_CobSem(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("infcobsem.Res" + a.getIdioma());
      informe = new Pan_InfCobSem(a);
      panelFecha = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      informe.setEnabled(false);
      this.txtCodArea.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesArea.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDist.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDist.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();
      this.cbEquipo.setState(true);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(301);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));

    // gestores de eventos

    btnCtrlBuscarArea.addActionListener(btnActionListener);
    btnCtrlBuscarDist.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtCodArea.addKeyListener(txtTextAdapter);

    txtCodArea.addFocusListener(txtFocusAdapter);

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnCtrlBuscarArea.setActionCommand("buscarArea");
    txtDesArea.setEditable(false);
    txtDesArea.setEnabled(false); /*E*/
    btnCtrlBuscarDist.setActionCommand("buscarDist");
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtCodArea.setName("txtCodArea");
    txtCodArea.setBackground(new Color(255, 255, 150));

    lblArea.setText(res.getString("lblArea.Text"));
    cbEquipo.setLabel(res.getString("cbEquipo.Label"));
    cbEquipo.setCheckboxGroup(checkboxAgrupar);
    cbCentro.setLabel(res.getString("cbCentro.Label"));
    cbCentro.setCheckboxGroup(checkboxAgrupar);
    lblDist.setText(res.getString("lblDist.Text"));

    txtCodDist.addKeyListener(txtTextAdapter);
    txtCodDist.addFocusListener(txtFocusAdapter);
    txtCodDist.setName("txtCodDist");
    txtCodDist.setBackground(SystemColor.control);
    txtDesDist.setEditable(false);
    txtDesDist.setEnabled(false); /*E*/
    lblAgrupar.setText(res.getString("lblAgrupar.Text"));
    lblFecha.setText(res.getString("lblFecha.Text"));

    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblFecha, new XYConstraints(26, 42, 51, -1));
    this.add(panelFecha, new XYConstraints(40, 42, -1, -1));
    this.add(lblArea, new XYConstraints(26, 82, 101, -1));
    this.add(txtCodArea, new XYConstraints(143, 82, 77, -1));
    this.add(btnCtrlBuscarArea, new XYConstraints(225, 82, -1, -1));
    this.add(txtDesArea, new XYConstraints(254, 82, 287, -1));
    this.add(lblDist, new XYConstraints(26, 120, 101, -1));
    this.add(txtCodDist, new XYConstraints(143, 122, 77, -1));
    this.add(btnCtrlBuscarDist, new XYConstraints(225, 122, -1, -1));
    this.add(txtDesDist, new XYConstraints(254, 122, 287, -1));
    this.add(lblAgrupar, new XYConstraints(26, 162, 76, -1));
    this.add(cbEquipo, new XYConstraints(143, 162, -1, -1));
    this.add(cbCentro, new XYConstraints(254, 162, -1, -1));
    this.add(btnLimpiar, new XYConstraints(389, 162, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 162, -1, -1));

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnCtrlBuscarArea.setImage(imgs.getImage(0));
    btnCtrlBuscarDist.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));
    btnInforme.setImage(imgs.getImage(2));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (panelFecha.txtAno.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    // Comprobacion de la descripcion de la enfermedad
    if (txtDesArea.getText().length() == 0) {
      bDatosCompletos = false;

    }
    if (checkboxAgrupar.getSelectedCheckbox() == null) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodArea.setEnabled(true);
        //txtDesArea.setEnabled(true); /*E*/

        btnCtrlBuscarArea.setEnabled(true);
        if (!txtDesArea.getText().equals("")) {
          btnCtrlBuscarDist.setEnabled(true);
          txtCodDist.setEnabled(true);
          //txtDesDist.setEnabled(true); /*E*/
        }
        else {
          btnCtrlBuscarDist.setEnabled(false);
          txtCodDist.setEnabled(false);
          //txtCodDist.setEnabled(false); /*E*/
          //txtDesDist.setEnabled(false); /*E*/
          txtCodDist.setText("");
          txtDesDist.setText("");
        }

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodArea.setEnabled(false);
        btnCtrlBuscarArea.setEnabled(false);
        txtCodDist.setEnabled(false);
        btnCtrlBuscarDist.setEnabled(false);

        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarArea_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg3.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodArea.removeKeyListener(txtTextAdapter);
      txtCodArea.setText(data.getCod());
      txtDesArea.setText(data.getDes());
      txtCodDist.setText("");
      txtDesDist.setText("");
      txtCodArea.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los niveles2 (distritos)
  //busca el distrito (Niv2)
  void btnCtrlbuscarDist_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg4.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDist.removeKeyListener(txtTextAdapter);
      txtCodDist.setText(data.getNiv2());
      txtDesDist.setText(data.getDes());
      txtCodDist.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtCodArea.setText("");
    txtDesArea.setText("");
    txtCodDist.setText("");
    txtDesDist.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC2 = new ParamC11_12();

    if (isDataValid()) {
      if (!txtDesArea.getText().equals("")) {
        paramC2.sCN = txtCodArea.getText();
        paramC2.sCNdesc = txtDesArea.getText();
      } //area
      if (!txtDesDist.getText().equals("")) {
        paramC2.sEN = txtCodDist.getText();
        paramC2.sENdesc = txtDesDist.getText();
      } //distrito

      if (cbCentro.getState()) {
        paramC2.sGrupo = "2";
      }
      else {
        paramC2.sGrupo = "1";

      }

      paramC2.sAnoI = panelFecha.txtAno.getText();

      if (panelFecha.txtCodSem.getText().length() == 1) {
        paramC2.sSemanaI = "0" + panelFecha.txtCodSem.getText();
      }
      else {
        paramC2.sSemanaI = panelFecha.txtCodSem.getText();

        // habilita el panel
      }
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.paramC1 = paramC2;
      boolean hay = informe.GenerarInforme();
      if (hay) {
        informe.show();
      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg5.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodArea")) &&
        (txtCodArea.getText().length() > 0)) {
      //E //#// System_out.println("Pierde el foco el textArea");
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodArea.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDist")) &&
             (txtCodDist.getText().length() > 0)) {
      //E //#// System_out.println("Pierde el foco el textDist");
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodArea.getText(), txtCodDist.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodArea")) {
            //E //#// System_out.println("realiza el cast y rellena los datos del area");
            nivel1 = (DataNivel1) param.firstElement();
            txtCodArea.removeKeyListener(txtTextAdapter);
            txtCodArea.setText(nivel1.getCod());
            txtDesArea.setText(nivel1.getDes());
            txtCodArea.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodDist")) {
            //E //#// System_out.println("realiza el cast y rellena los datos del dist");
            nivel2 = (DataZBS2) param.firstElement();
            txtCodDist.removeKeyListener(txtTextAdapter);
            txtCodDist.setText(nivel2.getNiv2());
            txtDesDist.setText(nivel2.getDes());
            txtCodDist.addKeyListener(txtTextAdapter);
          }

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodArea")) &&
        (txtDesArea.getText().length() > 0)) {
      txtDesArea.setText("");
      txtCodDist.setText("");
      txtDesDist.setText("");

    }
    else if ( (txt.getName().equals("txtCodDist")) &&
             (txtDesDist.getText().length() > 0)) {
      txtDesDist.setText("");
    }
    Inicializar();
  }

} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  Pan_CobSem adaptee = null;
  ActionEvent e = null;

  public actionListener(Pan_CobSem adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarArea")) {
      adaptee.btnCtrlbuscarArea_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDist")) {
      adaptee.btnCtrlbuscarDist_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_CobSem adaptee;
  FocusEvent event;

  focusAdapter(Pan_CobSem adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// cambio en una caja de codigos
class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_CobSem adaptee;

  txt_keyAdapter(Pan_CobSem adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}

////////////////////// Clases para listas

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

class CListaZBS2
    extends CListaValores {

  protected Pan_CobSem panel;

  public CListaZBS2(Pan_CobSem p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodArea.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}
