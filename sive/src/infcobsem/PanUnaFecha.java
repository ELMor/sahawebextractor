package infcobsem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.FocusEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import fechas.conversorfechas;
import obj.CFechaSimple;

public class PanUnaFecha
    extends Panel {

  //modos de inicializacion
  public static final int modoINICIO_SEMANA = 1;
  public static final int modoSEMANA_ACTUAL = 2;

  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variables
  int modoOperacion;

  protected CLista listaFich = new CLista();
  protected CPanel pnlContenedor;

  //objeto para conversi�n de num semana a fecha y viceversa
  conversorfechas convDesde = null;
  conversorfechas convHasta = null;

  //Variables y objetos para recuperar datos si cambios no tienen �xito
  conversorfechas convDesdeBk = null;
  String sAnoBk = "";
  String sCodSemBk = "";
  String sFecSemBk = "";

  Enumeration enum;

  int tipIni;
  boolean bFechaProtegida;

  XYLayout xYLayout1 = new XYLayout();
  public TextField txtAno = new TextField();
  public TextField txtCodSem = new TextField();
  CFechaSimple txtFecSem = new CFechaSimple("S");

  Label lblAno = new Label();
  Label lblSem = new Label();

  public PanUnaFecha(CPanel pnl, int iModo, boolean bFecha) {
    try {
      pnlContenedor = pnl;
      tipIni = iModo;
      bFechaProtegida = bFecha;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(390, 30));
    xYLayout1.setHeight(30);
    xYLayout1.setWidth(390);

    txtFecSem.addFocusListener(new PanUnaFecha_txtFecSem_focusAdapter(this));
    txtCodSem.addFocusListener(new PanUnaFecha_txtCodSem_focusAdapter(this));
    txtAno.addFocusListener(new PanUnaFecha_txtAno_focusAdapter(this));

    lblSem.setText("Semana:");
    lblAno.setText("A�o:");
    this.setLayout(xYLayout1);

    this.add(lblAno, new XYConstraints(3, 2, 34, -1));
    this.add(txtAno, new XYConstraints(36, 2, 77, -1));
    this.add(lblSem, new XYConstraints(143, 2, 51, -1));
    this.add(txtCodSem, new XYConstraints(206, 2, 43, -1));
    this.add(txtFecSem, new XYConstraints(260, 2, 97, -1));

    txtAno.setForeground(Color.black);
    txtAno.setBackground(new Color(255, 255, 150));

    txtCodSem.setForeground(Color.black);
    txtCodSem.setBackground(new Color(255, 255, 150));

    if (bFechaProtegida) {
      txtFecSem.setEditable(false);
    }
    else {
      txtFecSem.setForeground(Color.black);
      txtFecSem.setBackground(new Color(255, 255, 150));
    }

    inicializarFechas();

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtAno.setEnabled(true);
        txtCodSem.setEnabled(true);
        if (!bFechaProtegida) {
          txtFecSem.setEnabled(true);
        }
        pnlContenedor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        pnlContenedor.setEnabled(true);
        break;

      case modoESPERA:
        txtAno.setEnabled(false);
        txtCodSem.setEnabled(false);
        if (!bFechaProtegida) {
          txtFecSem.setEnabled(false);
        }
        pnlContenedor.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pnlContenedor.setEnabled(false);
        break;
    }
  }

  void inicializarFechas() {
    String sFecActual;
    java.util.Date fecActual = null;
    String sFecIni;
    String sAnoDesde;
    int res;
    CMessage msgBox;

    try {

      //Obtiene la fecha actual
      fecActual = new java.util.Date();

      //Pasa la fecha actual de Date a String
      SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
      sFecActual = formater.format(fecActual);

      txtFecSem.setText(sFecActual.trim());

      //Si la fecha no ha cambiado no hace nada
      if (txtFecSem.getText().equals(sFecSemBk)) {
        return;
      }

      //valida el dato
      txtFecSem.ValidarFecha();
      //Si no es v�lido recupera el antiguo
      if (txtFecSem.getValid().equals("N")) {
        txtFecSem.setText(sFecSemBk);
        return;
      }
      //Si es v�lido le da el formato adecuado
      else if (txtFecSem.getValid().equals("S")) {
        txtFecSem.setText(txtFecSem.getFecha());
      }

      this.modoOperacion = modoESPERA;
      Inicializar();

      sFecIni = txtFecSem.getText();
      convDesde = conversorfechas.getConvDeFecha(sFecIni, pnlContenedor.getApp());

      this.modoOperacion = modoNORMAL;
      Inicializar();

      // si a�o no es v�lido recupera todos los datos y muestra mensaje
      if (convDesde == null) {
        convDesde = convDesdeBk;
        txtAno.setText(sAnoBk);
        txtCodSem.setText(sCodSemBk);
        txtFecSem.setText(sFecSemBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              "No hay datos de ese a�o epidemiol�gico");
        msgBox.show();
        msgBox = null;
      }
      // si es v�lido rellena campo de a�o y guarda datos del a�o para posible recuperaci�n
      //NO se vac�an las cajas de semana
      else {
        convDesdeBk = convDesde;
        sAnoDesde = convDesde.getAno();
        txtAno.setText(sAnoDesde);
        sAnoBk = sAnoDesde;

      }
      //si no hay un a�o v�lido todav�a no hace nada y sale
      if (sAnoBk == "") {
        return;
      }

      /// Vamos a por la semana y fecha

      //Si tipo de inicial. es 1 pone primer dia y semana del a�o desde
      if (tipIni == 1) {
        txtCodSem.setText("1");
        sCodSemBk = txtCodSem.getText();
        txtFecSem.setText(convDesde.getFecFinPriSem());
        sFecSemBk = txtFecSem.getText();
      }
      //Si se inicializa el panel con otro valor se pone la fecha de hoy
      else {

        //indeptemente de si he traido a�o o sigue el que habia, busca semana:
        res = convDesde.getNumSem(txtFecSem.getText());

        //Si esa semana no es v�lida recupera los datos anteriores de semana y muestra un mensaje
        if (res == -1) {
          txtCodSem.setText(sCodSemBk);
          txtFecSem.setText(sFecSemBk);
          msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
              "Esa fecha no pertenece al a�o epidemiol�gico indicado");
          msgBox.show();
          msgBox = null;
        }
        else {
          txtCodSem.setText(Integer.toString(res));
          sCodSemBk = txtCodSem.getText();
          sFecSemBk = txtFecSem.getText();
        }

      }
      // envia la excepci�n
    }
    catch (Exception e) {
      //#System_Out.println(e.getMessage() );
      e.printStackTrace();
    }
  }

  void txtCodSem_focusLost(FocusEvent evt) {

//#System_Out.println("evento en codSem");//*******************
     int numero;
    String res;
    CMessage msgBox;
    try {

      txtCodSem.setText(txtCodSem.getText().trim());

      //si no ha cambiado el valor de la caja de texto no hace nada
      if (txtCodSem.getText().equals(sCodSemBk)) {
        return;
      }

      //chequea cod semana.Debe ser entero entre 1 y 53
      //Si no es ocrrecto o no hay a�o v�lido recupera el valor antiguo y ya est�
      if (
          (sAnoBk == "") || (ChequearEntero(txtCodSem.getText(), 0, 54, 2) == false)
          ) {
        txtCodSem.setText(sCodSemBk);
        return;
      }

      //Si hay a�o valido
      numero = Integer.parseInt(txtCodSem.getText());
      res = convDesde.getFec(numero);
      //Si no es num semana v�lido recupera los datos anteriores y muestra mensaje
      if (res.equals("")) {
        txtCodSem.setText(sCodSemBk);
        txtFecSem.setText(sFecSemBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
            "Esa semana no pertenece al a�o epidemiol�gico indicado");
        msgBox.show();
        msgBox = null;

      }
      //si es semana v�lida actualiza la fecha de fin de sem y guarda los datos de reserva
      else {
        txtFecSem.setText(res);
        sCodSemBk = txtCodSem.getText();
        sFecSemBk = res;

      }

      // envia la excepci�n
    }
    catch (Exception e) {
      //#System_Out.println(e.getMessage() );
      e.printStackTrace();

    }
    //#System_Out.println(txtFecSem.getText());//*******************
  }

  void txtFecSem_focusLost(FocusEvent evt) {
//#System_Out.println("evento lostfocus en FecSem");//*******************
//#System_Out.println("valor SCodSemBk *********" + sCodSemBk);//*******************

      java.util.Date dFecha;
    Calendar calen = new GregorianCalendar();
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    int ano;

    String sFecIni;
    String sAnoDesde; //A�o que se obttiene a partir de fecha indicada por usuario
    int res;
    CMessage msgBox;

    try {

      txtFecSem.setText(txtFecSem.getText().trim());

      //Si la fecha no ha cambiado no hace nada
      if (txtFecSem.getText().equals(sFecSemBk)) {
        return;
      }

      //valida el dato
      txtFecSem.ValidarFecha();
      //Si no es v�lido recupera el antiguo
      if (txtFecSem.getValid().equals("N")) {
        txtFecSem.setText(sFecSemBk);
        return;
      }
      //Si es v�lido le da el formato adecuado
      else if (txtFecSem.getValid().equals("S")) {
        txtFecSem.setText(txtFecSem.getFecha());
      }

      //Pasa el string de fecha a Date
      dFecha = formater.parse(txtFecSem.getText());
      //obtiene el a�o cronol�gico correspondiente a esa fecha
      calen.setTime(dFecha);
      ano = calen.get(Calendar.YEAR);

      // trae fechas del a�o epidemiol�gico de b.datos si es necesario
      if (
          //Si el a�o cronol�gico ha cambiado en fecha
          (! (Integer.toString(ano).equals(sAnoBk))) ||
          //o fecha es de �ltimos de diciembre
          ( (calen.get(Calendar.MONTH) == Calendar.DECEMBER) &&
           (calen.get(Calendar.DAY_OF_MONTH) > 23)) ||
          //o fecha es de principios de enero
          ( (calen.get(Calendar.MONTH) == Calendar.JANUARY) &&
           (calen.get(Calendar.DAY_OF_MONTH) < 9))
          ) {

        //Se va a por el a�o epidemiol�gico  (podr� coincidir con el cronol�gico o no )

        this.modoOperacion = modoESPERA;
        Inicializar();

        sFecIni = txtFecSem.getText();
        convDesde = conversorfechas.getConvDeFecha(sFecIni,
            pnlContenedor.getApp());

        this.modoOperacion = modoNORMAL;
        Inicializar();

        // si a�o no es v�lido recupera todos los datos y muestra mensaje
        if (convDesde == null) {
          convDesde = convDesdeBk;
          txtAno.setText(sAnoBk);
          txtCodSem.setText(sCodSemBk);
          txtFecSem.setText(sFecSemBk);
          msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                                "No hay datos de ese a�o epidemiol�gico");
          msgBox.show();
          msgBox = null;
        }
        // si es v�lido rellena campo de a�o y guarda datos  para posible recuperaci�n
        //NO se vac�an las cajas de semana
        else {
          convDesdeBk = convDesde;
          sAnoDesde = convDesde.getAno();
          txtAno.setText(sAnoDesde);
          sAnoBk = sAnoDesde;
        }
      }

//#System_Out.println("Vamos a por la semana"); //********************

       //si no hay un a�o v�lido todav�a no hace nada y sale
       if (sAnoBk == "") {
         return;
       }

      //indeptemente de si he traido a�o o sigue el que habia, busca semana:
      res = convDesde.getNumSem(txtFecSem.getText());

      //Si esa semana no es v�lida recupera los datos anteriores de semana y muestra un mensaje
      if (res == -1) {
        txtCodSem.setText(sCodSemBk);
        txtFecSem.setText(sFecSemBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
            "Esa fecha no pertenece al a�o epidemiol�gico indicado");
        msgBox.show();
        msgBox = null;
      }
      else {
        //Si semana es v�lida guarda los datos y muestra su c�d de semana
        txtCodSem.setText(Integer.toString(res));
        sCodSemBk = txtCodSem.getText();
        sFecSemBk = txtFecSem.getText();
      }
      // envia la excepci�n
    }
    catch (Exception e) {
      //#System_Out.println(e.getMessage() );
      e.printStackTrace();
    }
  }

  void txtAno_focusLost(FocusEvent e) {

    CMessage msgBox;

    String sAnoDesde;
//#System_Out.println("evento en Ano");//*******************

    try {

      txtAno.setText(txtAno.getText().trim());

      //Si el a�o no ha cambiado no hace nada
      if (txtAno.getText().equals(sAnoBk)) {
        return;
      }

      //chequea el a�o.Debe ser entero entre 0 y 9999
      //Si no es ocrrecto recupera el valor antiguo y ya est�
      if (ChequearEntero(txtAno.getText(), 0, 9999, 2) == false) {
        txtAno.setText(sAnoBk);
        return;
      }

      this.modoOperacion = modoESPERA;
      Inicializar();

      //objeto para conversi�n de num semana a fecha y viceversa
      sAnoDesde = txtAno.getText();
      convDesde = new conversorfechas(sAnoDesde, pnlContenedor.getApp());

      //recupera datos y muestra mensaje si a�o no es v�lido
      if (convDesde.anoValido() == false) {
        convDesde = convDesdeBk;
//#System_Out.println("sAnoBk  "+ sAnoBk); //*****************

        txtAno.setText(sAnoBk);
        txtCodSem.setText(sCodSemBk);
        txtFecSem.setText(sFecSemBk);
        msgBox = new CMessage(pnlContenedor.getApp(), CMessage.msgAVISO,
                              "No hay datos de ese a�o epidemiol�gico");
        msgBox.show();
        msgBox = null;
      }
      // si es v�lido vacia cajas de semana y guarda datos del a�o para posible recuperaci�n
      else {

        convDesdeBk = convDesde;
        sAnoBk = txtAno.getText();
        //Primer dia y semana del a�o Desde
        txtCodSem.setText("1");
        sCodSemBk = txtCodSem.getText();
        txtFecSem.setText(convDesde.getFecFinPriSem());
        sFecSemBk = txtFecSem.getText();

      }

      // envia la excepci�n
    }
    catch (Exception ex) {
      //#System_Out.println(ex.getMessage() );
      ex.printStackTrace();
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString.equals("")) {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString.equals("")) {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     //comprobar valor
     if (b) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

//#System_Out.println("Devuelve b **********" + b);
     return (b);
   } //fin de ChequearEntero

  boolean isDataValid() {

    boolean b = false;
    if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!sAnoBk.equals("")) &&
        (!sCodSemBk.equals("")) &&
        (!sFecSemBk.equals("")) &&
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos v�lidos
        (txtAno.getText().trim().equals(sAnoBk)) &&
        (txtCodSem.getText().trim().equals(sCodSemBk)) &&
        (txtFecSem.getText().trim().equals(sFecSemBk))

        ) {
      return true;
    }
    else {
      return false;
    }

  }
} //clase

class PanUnaFecha_txtCodSem_focusAdapter
    extends java.awt.event.FocusAdapter {
  PanUnaFecha adaptee;

  PanUnaFecha_txtCodSem_focusAdapter(PanUnaFecha adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtCodSem_focusLost(e);
  }
}

class PanUnaFecha_txtFecSem_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  FocusEvent e = null;
  PanUnaFecha adaptee;

  PanUnaFecha_txtFecSem_focusAdapter(PanUnaFecha adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtFecSem_focusLost(e);
  }
}

class PanUnaFecha_txtAno_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanUnaFecha adaptee;
  FocusEvent e = null;

  PanUnaFecha_txtAno_focusAdapter(PanUnaFecha adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtAno_focusLost(e);
  }
}
