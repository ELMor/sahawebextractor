package infcobsem;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosCobSem
    extends CApp {

  ResourceBundle res;

  public CasosCobSem() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("infcobsem.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;

    VerPanel(res.getString("msg2.Text"), new Pan_CobSem(a), false);
    VerPanel(res.getString("msg2.Text"));
  }

}