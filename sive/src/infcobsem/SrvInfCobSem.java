
package infcobsem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CLista;
import eqNot.DataEqNot;
import sapp.DBServlet;

public class SrvInfCobSem
    extends DBServlet {

  // informes
  final int erwCASOS_EQUIPO = 1;
  final int erwCASOS_CENTRO = 2;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    DataEqNot datosEq = null;

    Float aux3 = new Float(0);

    // control
    int i = 1;
    String sCodigos = null;
    CLista sDescripciones = null;
    String sTempo = null;
    int totalRegistros = 0;

    // buffers
    String sCod;
    int pDes = 0;
    int iNum;
    DataInfCobSem dat = null;
    Float aux = new Float(0);

    // objetos de datos
    CLista data = new CLista();
    data.setState(CLista.listaVACIA);

    Vector registros = new Vector();
    Vector registrosAux = new Vector();
    Vector totales = new Vector();
    int totReal = 0;
    int notifTotal = 0;
    int coberi = 0;
    int coberr = 0;
    String cadi = "";
    int num_pagina = 0;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    datosEq = (DataEqNot) param.firstElement();

    //E //#// System_out.println("SrvInfCobSem: paramneteos " +  datosEq.getNiv1()
    // 		      + " " +  datosEq.getNiv2() + " " + datosEq.getAnyo()
    // 		      + " " + datosEq.getSemana());

    // modos de operaci�n
    switch (opmode) {

      // l�neas del modelo
      case erwCASOS_EQUIPO:

        // query equipos
        if ( (datosEq.getNiv1().compareTo("") != 0)
            && (datosEq.getNiv2().compareTo("") != 0)) {
          // Filtro
          if (param.getFilter().length() > 0) {

            /*query = "select CD_E_NOTIF, DS_E_NOTIF from sive_e_notif where CD_NIVEL_1=? and CD_NIVEL_2=? and CD_CENTRO in"+
                 "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and"+
                  " IT_BAJA='N')and CD_E_NOTIF > ? order by CD_E_NOTIF";*/

            query =
                "select a.cd_e_notif, b.ds_e_notif  from sive_notif_sem a, " +
                " sive_e_notif b where " +
                " a.cd_anoepi = ? and " +
                " a.cd_semepi = ? and " +
                " a.cd_e_notif = b.cd_e_notif and " +
                " b.cd_nivel_1 = ? and b.cd_nivel_2 = ? " +
                " and a.cd_e_notif > ? " +
                " and b.cd_centro in (select cd_centro from " +
                " sive_c_notif  where it_cobertura = 'S' and it_baja = 'N')" +
                " order by a.cd_e_notif";
          }
          else {

            /* query = "select CD_E_NOTIF, DS_E_NOTIF from sive_e_notif where CD_NIVEL_1=? and CD_NIVEL_2=? and CD_CENTRO in"+
             "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and"+
             " IT_BAJA='N')order by CD_E_NOTIF";
                // lanza la query
                st = con.prepareStatement(query);
                // nivel 1
                st.setString(1, datosEq.getNiv1());
                // nivel 2
                st.setString(2, datosEq.getNiv2());*/
            ////////////////// JLT
            query =
                "select a.cd_e_notif, b.ds_e_notif  from sive_notif_sem a, " +
                " sive_e_notif b where " +
                " a.cd_anoepi = ? and " +
                " a.cd_semepi = ? and " +
                " a.cd_e_notif = b.cd_e_notif and " +
                " b.cd_nivel_1 = ? and b.cd_nivel_2 = ? " +
                " and b.cd_centro in (select cd_centro from " +
                " sive_c_notif  where it_cobertura = 'S' and it_baja = 'N')" +
                " order by a.cd_e_notif";

          }
          st = con.prepareStatement(query);
          // a�o
          st.setString(1, datosEq.getAnyo());
          // semana
          st.setString(2, datosEq.getSemana());
          // nivel 1
          st.setString(3, datosEq.getNiv1());
          // nivel 2
          st.setString(4, datosEq.getNiv2());
          ///////////////////////
          // filtro
          if (param.getFilter().length() > 0) {

            //st.setString(3, param.getFilter());
            st.setString(5, param.getFilter());
          }
        }
        else if ( (datosEq.getNiv1().compareTo("") != 0)
                 && (datosEq.getNiv2().compareTo("") == 0)) {
          // filtro
          //E //#// System_out.println("SRV:1");
          if (param.getFilter().length() > 0) {
            /*query = "select CD_E_NOTIF, DS_E_NOTIF from sive_e_notif where CD_NIVEL_1=? and cd_centro in"+
              "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and"+
               " IT_BAJA='N')and CD_E_NOTIF > ? order by CD_E_NOTIF";*/

            query =
                "select a.cd_e_notif, b.ds_e_notif  from sive_notif_sem a, " +
                " sive_e_notif b where " +
                " a.cd_anoepi = ? and " +
                " a.cd_semepi = ? and " +
                " a.cd_e_notif = b.cd_e_notif and " +
                " b.cd_nivel_1 = ? " +
                " and a.cd_e_notif > ? " +
                " and b.cd_centro in (select cd_centro from " +
                " sive_c_notif  where it_cobertura = 'S' and it_baja = 'N')" +
                " order by a.cd_e_notif ";
          }
          else {
            /*query = "select CD_E_NOTIF, DS_E_NOTIF from sive_e_notif "
              + "where CD_NIVEL_1=? and cd_centro in"
                 + "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and IT_BAJA='N')"
              + " order by CD_E_NOTIF";*/
            query =
                "select a.cd_e_notif, b.ds_e_notif  from sive_notif_sem a, " +
                " sive_e_notif b where " +
                " a.cd_anoepi = ? and " +
                " a.cd_semepi = ? and " +
                " a.cd_e_notif = b.cd_e_notif and " +
                " b.cd_nivel_1 = ? " +
                " and b.cd_centro in (select cd_centro from " +
                " sive_c_notif  where it_cobertura = 'S' and it_baja = 'N')" +
                " order by a.cd_e_notif";
          }
          // lanza la query
          //E //#// System_out.println("SRV:2");
          //E //#// System_out.println(query);
          //E //#// System_out.println(query);
          st = con.prepareStatement(query);
          // a�o
          st.setString(1, datosEq.getAnyo());
          // semana
          st.setString(2, datosEq.getSemana());
          // nivel 1
          st.setString(3, datosEq.getNiv1());

          // nivel 1
          //E //#// System_out.println("SRV:3");
          //st.setString(1, datosEq.getNiv1());
          // filtro
          //E //#// System_out.println("SRV:4");
          if (param.getFilter().length() > 0) {

            //st.setString(2, param.getFilter());
            st.setString(4, param.getFilter());

          }
        } // si no hay elegido ni nivel1 ni nivel2, pero esto no es posible
        else {
          // filtro
          if (param.getFilter().length() > 0) {
            query =
                "select CD_E_NOTIF, DS_E_NOTIF from sive_e_notif where cd_centro in" +
                "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and" +
                " IT_BAJA='N')and CD_E_NOTIF > ? order by CD_E_NOTIF";
          }
          else {
            query =
                "select CD_E_NOTIF, DS_E_NOTIF from sive_e_notif where cd_centro in" +
                "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and" +
                " IT_BAJA='N')order by CD_E_NOTIF";
            // lanza la query
          }
          st = con.prepareStatement(query);
          // filtro
          if (param.getFilter().length() > 0) {
            st.setString(1, param.getFilter());
          }
        }

        // despues de preparar la query

        //E //#// System_out.println("SRV:5 " + query);
        rs = st.executeQuery();
        //E //#// System_out.println("SRV:6");
        sCodigos = " ";
        //E //#// System_out.println("SRV:7");
        sDescripciones = new CLista();
        //E //#// System_out.println("SRV:8");

        // Copiado por ARS de consedo.
        // Arreglo. Se env�a el n�mero de p�gina en el n�mero de Fax.
        if (datosEq.getFax().length() > 0) {
          num_pagina = Integer.parseInt(datosEq.getFax());
          for (i = 0; i < num_pagina * DBServlet.maxSIZE; i++) {
            rs.next();
          }
        }

        i = 0;
        // extrae la p�gina requerida

        //E //#// System_out.println("SRV:9");
        while (rs.next()) {
          //E //#// System_out.println("SRV:10");
          // control de tama�o
          if ( (i >= DBServlet.maxSIZE) && (! (datosEq.bInformeCompleto))) {
            cadi = ( (DataInfCobSem) registros.lastElement()).DS_NOMBRE;
            data.setState(CLista.listaINCOMPLETA);
            pDes = cadi.indexOf("&");
            data.setFilter(cadi.substring(0, pDes));
            break;
          }
          //E //#// System_out.println("SRV:11");
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
            //E //#// System_out.println("SRV:12");
            //Recogemos datos modelo
          }
          sTempo = "'" + rs.getString("CD_E_NOTIF") + "'";
          //E //#// System_out.println("SRV:13");
          sDescripciones.addElement(rs.getString("DS_E_NOTIF"));
          //E //#// System_out.println("SRV:14");
          String tmp = new String();
          tmp = rs.getString("DS_E_NOTIF");
          dat = new DataInfCobSem(sTempo + '&' + tmp, 0, 0, 0, 0);
          //E //#// System_out.println("SRV:15");
          registros.addElement(dat);
          //E //#// System_out.println("SRV:16");
          sCodigos += "," + sTempo;
          i++;

        } //while que recorre el ResultSet

        rs.close();
        rs = null;
        st.close();
        st = null;

        if (sCodigos.compareTo(" ") != 0) {
          sCodigos = sCodigos.substring(2);

          // totales
// **********************************
// ARS 18-07-01... correcci�n para no pasarle una lista muy larga.
// Se pone condici�n dependiente del n�mero de datos que se pidan.
          if (!datosEq.bInformeCompleto) {
            query = "select * from SIVE_NOTIF_SEM where CD_E_NOTIF in" +
                "(" + sCodigos +
                ") and CD_ANOEPI=? and CD_SEMEPI=? order by CD_E_NOTIF";
          }
          else {
            query =
                "select a.CD_E_NOTIF, b.DS_E_NOTIF, a.CD_ANOEPI, a.CD_SEMEPI, " +
                "a.NM_NTOTREAL, a.NM_NNOTIFT " +
                "from SIVE_NOTIF_SEM a, sive_e_notif b, sive_c_notif c " +
                "where a.CD_E_NOTIF = b.CD_E_NOTIF and " +
                "c.CD_CENTRO = b.CD_CENTRO and " +
                "c.IT_COBERTURA = 'S' and " +
                "c.IT_BAJA = 'N' ";

            // Pasamos a montar el 'where'
            if (datosEq.getAnyo().length() > 0) {
              query += "and a.CD_ANOEPI = ? ";
            }
            if (datosEq.getSemana().length() > 0) {
              query += "and a.CD_SEMEPI = ? ";
            }
            if (datosEq.getNiv1().length() > 0) {
              query += "and b.CD_NIVEL_1 = ? ";
            }
            if (datosEq.getNiv2().length() > 0) {
              query += "and b.CD_NIVEL_2 = ? ";
            }
            if (param.getFilter().length() > 0) {
              query += "and CD_E_NOTIF > ? ";
              // Ya est� el 'where'
            }
            query += "order by a.CD_E_NOTIF";
          }

          st = con.prepareStatement(query);

          // Repetimos la misma tarea, dependiente de bInformeCompleto

          if (datosEq.bInformeCompleto) {
            int indice_consulta = 1;

            // a�o
            if (datosEq.getAnyo().length() > 0) {
              st.setString(indice_consulta, datosEq.getAnyo());
              indice_consulta++;
            }
            // semana
            if (datosEq.getSemana().length() > 0) {
              st.setString(indice_consulta, datosEq.getSemana());
              indice_consulta++;
            }
            // nivel 1
            if (datosEq.getNiv1().length() > 0) {
              st.setString(indice_consulta, datosEq.getNiv1());
              indice_consulta++;
            }
            // nivel 2
            if (datosEq.getNiv2().length() > 0) {
              st.setString(indice_consulta, datosEq.getNiv2());
              indice_consulta++;
            }
            ///////////////////////
            // filtro
            if (param.getFilter().length() > 0) {
              //st.setString(3, param.getFilter());
              st.setString(indice_consulta, param.getFilter());
              indice_consulta++;
            }
          }
          else {
            // a�o epidemiol�gico
            st.setString(1, datosEq.getAnyo());
            // semana epidemiol�gica
            st.setString(2, datosEq.getSemana());
          }

// ****************
// FIN CAMBIOS ARS

          rs = st.executeQuery();

          // a�ado los datos que faltan
          i = 0;
          totales = new Vector();
          registrosAux = new Vector();

          while (rs.next()) {

            totReal = rs.getInt("NM_NTOTREAL");
            notifTotal = rs.getInt("NM_NNOTIFT");
            if (notifTotal != 0) {
              // jlt
              aux3 = new Float( (float) totReal / (float) notifTotal *
                               (float) 100);
              aux = new Float( (float) totReal / (float) notifTotal *
                              (float) 100);
              coberi = aux.intValue();

              /////////////  modificacion jlt
              String a = aux3.toString();
              String e = a.substring(0, a.indexOf('.'));
              String d = a.substring(a.indexOf('.') + 1, a.length());

              if (d.length() >= 3) {
                String d1 = d.substring(0, 1);
                String d2 = d.substring(1, 2);
                String d3 = d.substring(2, 3);
                Integer i3 = new Integer(d3);
                Integer i2 = new Integer(d2);
                Integer i1 = new Integer(d1);
                int suma = 0;
                if (i3.intValue() >= 5) {
                  suma = i2.intValue() + 1;
                  d = d1 + (new Integer(suma)).toString();

                  if (i2.intValue() == 9) {
                    suma = i1.intValue() + 1;
                    d = (new Integer(suma)).toString() +
                        (new Integer(0)).toString();
                  }

                }
                else {
                  d = d1 + d2;
                }

                coberr = (new Float(d)).intValue();
                //////////////
              }
              else {
                coberr = (new Float( (aux.floatValue() - ( (float) coberi)) *
                                    (float) 100)).intValue();
              }

              //coberr = (new Float((aux.floatValue() - ((float)coberi))*(float)100)).intValue();
            }
            else {
              coberi = 0;
              coberr = 0;
            }
            registrosAux.addElement(new DataInfCobSem(rs.getString("CD_E_NOTIF"),
                totReal, notifTotal, coberi, coberr));
            i++;
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          // ahora vamos a por las descripciones
          // totales
          totales = new Vector();
          for (int j = 0; j < registrosAux.size(); j++) {
            dat = (DataInfCobSem) registrosAux.elementAt(j);
            // jlt
            String cod = (String) dat.DS_NOMBRE;

            query = "select * from SIVE_E_NOTIF where CD_E_NOTIF=?";
            // lanza la query
            st = con.prepareStatement(query);
            // a�o epidemiol�gico
            st.setString(1, dat.DS_NOMBRE);

            rs = st.executeQuery();

            // a�ado los datos que faltan
            i = 0;

            while (rs.next()) {
              totReal = dat.NM_NTOTREAL;
              notifTotal = dat.NM_NNOTIFT;
              //Si lo dejo se calcula dos veces
              if (notifTotal != 0) {
                //jlt
                aux3 = new Float( (float) totReal / (float) notifTotal *
                                 (float) 100);
                aux = new Float( (float) totReal / (float) notifTotal *
                                (float) 100);
                coberi = aux.intValue();

                /////////////  modificacion jlt
                String a = aux3.toString();
                String e = a.substring(0, a.indexOf('.'));
                String d = a.substring(a.indexOf('.') + 1, a.length());

                if (d.length() >= 3) {
                  String d1 = d.substring(0, 1);
                  String d2 = d.substring(1, 2);
                  String d3 = d.substring(2, 3);
                  Integer i3 = new Integer(d3);
                  Integer i2 = new Integer(d2);
                  Integer i1 = new Integer(d1);
                  int suma = 0;
                  if (i3.intValue() >= 5) {
                    suma = i2.intValue() + 1;
                    d = d1 + (new Integer(suma)).toString();

                    if (i2.intValue() == 9) {
                      suma = i1.intValue() + 1;
                      d = (new Integer(suma)).toString() +
                          (new Integer(0)).toString();
                    }

                  }
                  else {
                    d = d1 + d2;
                  }

                  coberr = (new Float(d)).intValue();
                }
                else {

                  coberr = (new Float( (aux.floatValue() - ( (float) coberi)) *
                                      (float) 100)).intValue();
                }

                //coberr = (new Float((aux.floatValue() - ((float)coberi))*(float)100)).intValue();

              }
              else {
                coberi = 0;
                coberr = 0;
              }

              //totales.addElement(new DataInfCobSem(rs.getString("DS_E_NOTIF"),
              //totReal,notifTotal,coberi,coberr));
              totales.addElement(new DataInfCobSem(cod + '-' +
                  rs.getString("DS_E_NOTIF"),
                  totReal, notifTotal, coberi, coberr));
              i++;
            }

            rs.close();
            rs = null;
            st.close();
            st = null;
          }
        }

        // para el total del panel
        if ( (datosEq.getNiv1().compareTo("") != 0)
            && (datosEq.getNiv2().compareTo("") != 0)) {
          // Filtro
          if (param.getFilter().length() > 0) {

            /*query = "select count(*) from sive_e_notif where CD_NIVEL_1=? and CD_NIVEL_2=? and CD_CENTRO in"+
                 "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and"+
                  " IT_BAJA='N')and CD_E_NOTIF > ? ";*/
            query =
                "select count(*) from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_nivel_2 = ? and " +
                " cd_centro in (select cd_centro from " +
                " sive_c_notif where it_baja = 'N' and it_cobertura = 'S') and " +
                " cd_e_notif in (select cd_e_notif from sive_notif_sem where cd_anoepi = ? and " +
                " cd_semepi = ? ) and cd_e_notif > ?";
          }
          else {

            /*    query = "select count(*) from sive_e_notif where CD_NIVEL_1=? and CD_NIVEL_2=? and CD_CENTRO in"+
                 "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and"+
                " IT_BAJA='N') ";
              // lanza la query
              st = con.prepareStatement(query);
              // nivel 1
              st.setString(1, datosEq.getNiv1());
              // nivel 2
              st.setString(2, datosEq.getNiv2());*/
            ////////////////////////// JLT
            query =
                "select count(*) from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_nivel_2 = ? and " +
                " cd_centro in (select cd_centro from " +
                " sive_c_notif where it_baja = 'N' and it_cobertura = 'S') and " +
                " cd_e_notif in (select cd_e_notif from sive_notif_sem where cd_anoepi = ? and " +
                " cd_semepi = ? ) ";
          }
          st = con.prepareStatement(query);
          // nivel 1
          st.setString(1, datosEq.getNiv1());
          // nivel 2
          st.setString(2, datosEq.getNiv2());
          st.setString(3, datosEq.getAnyo());
          // nivel 2
          st.setString(4, datosEq.getSemana());

          ////////////////////////
          // filtro
          if (param.getFilter().length() > 0) {

            //st.setString(3, param.getFilter());
            st.setString(5, param.getFilter());
          }
        }
        else if ( (datosEq.getNiv1().compareTo("") != 0)
                 && (datosEq.getNiv2().compareTo("") == 0)) {
          // filtro
          //E //#// System_out.println("SRV:1");
          if (param.getFilter().length() > 0) {
                /*ery = "select count(*) from sive_e_notif where CD_NIVEL_1=? and cd_centro in"+
              "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and"+
               " IT_BAJA='N')and CD_E_NOTIF > ? ";*/
            query =
                "select count(*) from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_centro in (select cd_centro from " +
                " sive_c_notif where it_baja = 'N' and it_cobertura = 'S') and " +
                " cd_e_notif in (select cd_e_notif from sive_notif_sem where cd_anoepi = ? and " +
                " cd_semepi = ? ) and cd_e_notif > ?";
          }
          else {
            /*query = "select count(*) from sive_e_notif "
              + "where CD_NIVEL_1=? and cd_centro in"
              + "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and IT_BAJA='N')";*/
            query =
                "select count(*) from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_centro in (select cd_centro from " +
                " sive_c_notif where it_baja = 'N' and it_cobertura = 'S') and " +
                " cd_e_notif in (select cd_e_notif from sive_notif_sem where cd_anoepi = ? and " +
                " cd_semepi = ? ) ";
          }
          // lanza la query
          //E //#// System_out.println("SRV:2");
          //E //#// System_out.println(query);
          //E //#// System_out.println(query);
          st = con.prepareStatement(query);
          // nivel 1
          //E //#// System_out.println("SRV:3");
          st.setString(1, datosEq.getNiv1());
          st.setString(2, datosEq.getAnyo());
          st.setString(3, datosEq.getSemana());
          // filtro
          //E //#// System_out.println("SRV:4");
          if (param.getFilter().length() > 0) {

            //st.setString(2, param.getFilter());
            st.setString(4, param.getFilter());

          }
        }
        else { // aqui no puede entrar
          // filtro
          if (param.getFilter().length() > 0) {
            query = "select count(*) from sive_e_notif where cd_centro in" +
                "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and" +
                " IT_BAJA='N')and CD_E_NOTIF > ? ";
          }
          else {
            query = "select count(*) from sive_e_notif where cd_centro in" +
                "(select CD_CENTRO from sive_c_notif where IT_COBERTURA='S' and" +
                " IT_BAJA='N') ";
            // lanza la query
          }
          st = con.prepareStatement(query);
          // filtro
          if (param.getFilter().length() > 0) {
            st.setString(1, param.getFilter());
          }
        }

        //E //#// System_out.println("SRV:5");
        rs = st.executeQuery();
        if (rs.next()) {
          totalRegistros = rs.getInt(1);

        }
        rs.close();
        rs = null;
        st.close();
        st = null;
        //E //#// System_out.println("SrvInfCobSem salgo de casoEquipo");
        break;

      case erwCASOS_CENTRO:

        // query centros
        /*if (param.getFilter().length() > 0)
             query = "select CD_CENTRO, DS_CENTRO from sive_c_notif where IT_COBERTURA='S'"+
            " and IT_BAJA='N' and CD_CENTRO > ? order by CD_CENTRO";
          else
             query = "select CD_CENTRO, DS_CENTRO from sive_c_notif where IT_COBERTURA='S'"+
            " and IT_BAJA='N' order by CD_CENTRO";*/

        if ( (datosEq.getNiv1().compareTo("") != 0)
            && (datosEq.getNiv2().compareTo("") != 0)) {

          if (param.getFilter().length() > 0) {

            query =
                "select CD_CENTRO, DS_CENTRO from sive_c_notif where it_baja <> 'S' " +
                " and it_cobertura = 'S' and cd_centro in (select cd_centro " +
                " from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_nivel_2 = ? and cd_e_notif in " +
                " (select cd_e_notif from sive_notif_sem where " +
                " cd_anoepi = ? and cd_semepi = ?)) and cd_centro > ?";
          }
          else {

            query =
                "select CD_CENTRO, DS_CENTRO from sive_c_notif where it_baja <> 'S' " +
                " and it_cobertura = 'S' and cd_centro in (select cd_centro " +
                " from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_nivel_2 = ? and cd_e_notif in " +
                " (select cd_e_notif from sive_notif_sem where " +
                " cd_anoepi = ? and cd_semepi = ?))";
            ////////////////////////
            // lanza la query
          }
          st = con.prepareStatement(query);

          st.setString(1, datosEq.getNiv1());
          st.setString(2, datosEq.getNiv2());
          st.setString(3, datosEq.getAnyo());
          st.setString(4, datosEq.getSemana());
          // filtro
          if (param.getFilter().length() > 0) {
            st.setString(5, param.getFilter());

          }

        }
        else if ( (datosEq.getNiv1().compareTo("") != 0)
                 && (datosEq.getNiv2().compareTo("") == 0)) {

          if (param.getFilter().length() > 0) {

            query =
                "select CD_CENTRO, DS_CENTRO from sive_c_notif where it_baja <> 'S' " +
                " and it_cobertura = 'S' and cd_centro in (select cd_centro " +
                " from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_e_notif in " +
                " (select cd_e_notif from sive_notif_sem where " +
                " cd_anoepi = ? and cd_semepi = ?)) and cd_centro > ?";
          }
          else {

            query =
                "select CD_CENTRO, DS_CENTRO from sive_c_notif where it_baja <> 'S' " +
                " and it_cobertura = 'S' and cd_centro in (select cd_centro " +
                " from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_e_notif in " +
                " (select cd_e_notif from sive_notif_sem where " +
                " cd_anoepi = ? and cd_semepi = ?))";
            // lanza la query
          }
          st = con.prepareStatement(query);

          st.setString(1, datosEq.getNiv1());
          st.setString(2, datosEq.getAnyo());
          st.setString(3, datosEq.getSemana());
          // filtro
          if (param.getFilter().length() > 0) {
            st.setString(4, param.getFilter());

          }
        } // fin del if

        // lanza la query
        //st = con.prepareStatement(query);

        // filtro
        //if (param.getFilter().length() > 0)
        //  st.setString(1, param.getFilter());

        rs = st.executeQuery();
        sCodigos = "";

        // Copiado de consedo.	ARS
        // Arreglo. Se env�a el n�mero de p�gina en el n�mero de Fax.
        if (datosEq.getFax().length() > 0) {
          num_pagina = Integer.parseInt(datosEq.getFax());
          for (i = 0; i < num_pagina * DBServlet.maxSIZE; i++) {
            rs.next();
          }
        }

        i = 0;

        // extrae la p�gina requerida
        while (rs.next()) {

          // control de tama�o
          if ( (i >= DBServlet.maxSIZE) && (! (datosEq.bInformeCompleto))) {
            data.setState(CLista.listaINCOMPLETA);
            cadi = ( (DataInfCobSem) registros.lastElement()).DS_NOMBRE;
            //pDes = cadi.indexOf("&");
            pDes = cadi.indexOf("-");

            data.setFilter(cadi.substring(0, pDes));
            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);

            // prepara los datos
            //registros.addElement(new DataInfCobSem(rs.getString("CD_CENTRO")+"&"+
            //  rs.getString("DS_CENTRO"),0,0,0,0));
          }
          registros.addElement(new DataInfCobSem(rs.getString("CD_CENTRO") +
                                                 "-" +
                                                 rs.getString("DS_CENTRO"), 0,
                                                 0, 0, 0));
          i++;

        }
        rs.close();
        st.close();
        rs = null;
        st = null;

        totales = new Vector();
        for (int v = 0; v < registros.size(); v++) {
          // query equipos
          if ( (datosEq.getNiv1().compareTo("") != 0) &&
              (datosEq.getNiv2().compareTo("") != 0)) {
            query =
                "select sum(NM_NNOTIFT), sum(NM_NTOTREAL) from sive_notif_sem " +
                "where CD_ANOEPI=? and CD_SEMEPI=? and CD_E_NOTIF in " +
                "(select CD_E_NOTIF from sive_e_notif where CD_CENTRO=? and CD_NIVEL_1=? and CD_NIVEL_2=?)";

            // lanza la query
            st = con.prepareStatement(query);
            // a�o epidemiol�gico

            st.setString(1, datosEq.getAnyo());
            // semana epidemiol�gica

            st.setString(2, datosEq.getSemana());
            // centro epidemiol�gico
            dat = (DataInfCobSem) registros.elementAt(v);
            //pDes = dat.DS_NOMBRE.indexOf("&");
            pDes = dat.DS_NOMBRE.indexOf("-");

            st.setString(3, dat.DS_NOMBRE.substring(0, pDes));
            // nivel 1
            st.setString(4, datosEq.getNiv1());
            // nivel 2
            st.setString(5, datosEq.getNiv2());
          }
          else if ( (datosEq.getNiv1().compareTo("") != 0) &&
                   (datosEq.getNiv2().compareTo("") == 0)) {
            query =
                "select sum(NM_NNOTIFT), sum(NM_NTOTREAL) from sive_notif_sem " +
                "where CD_ANOEPI=? and CD_SEMEPI=? and CD_E_NOTIF in " +
                "(select CD_E_NOTIF from sive_e_notif where CD_CENTRO=? and CD_NIVEL_1=?)";

            // lanza la query
            st = con.prepareStatement(query);
            // a�o epidemiol�gico
            st.setString(1, datosEq.getAnyo());
            // semana epidemiol�gica
            st.setString(2, datosEq.getSemana());
            // centro epidemiol�gico
            dat = (DataInfCobSem) registros.elementAt(v);
            //E //#// System_out.println("Un dato");
            //pDes = dat.DS_NOMBRE.indexOf("&");
            pDes = dat.DS_NOMBRE.indexOf("-");

            //E //#// System_out.println(dat.DS_NOMBRE.substring(0, pDes));
            st.setString(3, dat.DS_NOMBRE.substring(0, pDes));
            // nivel 1
            st.setString(4, datosEq.getNiv1());
          }
          else {
            query =
                "select sum(NM_NNOTIFT), sum(NM_NTOTREAL) from sive_notif_sem " +
                "where CD_ANOEPI=? and CD_SEMEPI=? and CD_E_NOTIF in " +
                "(select CD_E_NOTIF from sive_e_notif where CD_CENTRO=?)";

            // lanza la query
            st = con.prepareStatement(query);
            // a�o epidemiol�gico
            st.setString(1, datosEq.getAnyo());
            // semana epidemiol�gica
            st.setString(2, datosEq.getSemana());
            // centro epidemiol�gico
            dat = (DataInfCobSem) registros.elementAt(v);
            //pDes = dat.DS_NOMBRE.indexOf("&");
            pDes = dat.DS_NOMBRE.indexOf("-");

            st.setString(3, dat.DS_NOMBRE.substring(0, pDes));
          }
          rs = st.executeQuery();

          //totales=null;
          // extrae la p�gina requerida
          while (rs.next()) {
            // prepara los datos
            notifTotal = rs.getInt(1);
            totReal = rs.getInt(2);
            if (notifTotal != 0) {
              // jlt
              aux3 = new Float( (float) totReal / (float) notifTotal *
                               (float) 100);
              aux = new Float( (float) totReal / (float) notifTotal *
                              (float) 100);
              coberi = aux.intValue();

              /////////////  modificacion jlt
              String a = aux3.toString();
              String e = a.substring(0, a.indexOf('.'));
              String d = a.substring(a.indexOf('.') + 1, a.length());

              if (d.length() >= 3) {
                String d1 = d.substring(0, 1);
                String d2 = d.substring(1, 2);
                String d3 = d.substring(2, 3);
                Integer i3 = new Integer(d3);
                Integer i2 = new Integer(d2);
                Integer i1 = new Integer(d1);
                int suma = 0;
                if (i3.intValue() >= 5) {
                  suma = i2.intValue() + 1;
                  d = d1 + (new Integer(suma)).toString();

                  if (i2.intValue() == 9) {
                    suma = i1.intValue() + 1;
                    d = (new Integer(suma)).toString() +
                        (new Integer(0)).toString();
                  }

                }
                else {
                  d = d1 + d2;
                }

                coberr = (new Float(d)).intValue();
                //////////////
              }
              else {
                coberr = (new Float( (aux.floatValue() - ( (float) coberi)) *
                                    (float) 100)).intValue();
              }

//	   coberr = (new Float((aux.floatValue() - ((float)coberi))*(float)100)).intValue();

              //cober = (float)totReal/(float)notifTotal*(float)100;
            }
            else {
              coberi = 0;
              coberr = 0;
            }
            // a�ade datos
            //totales.addElement(new DataInfCobSem(dat.DS_NOMBRE.substring(dat.DS_NOMBRE.indexOf("&")+1)
            //  ,totReal,notifTotal,coberi,coberr));

            totales.addElement(new DataInfCobSem(dat.DS_NOMBRE,
                                                 totReal, notifTotal, coberi,
                                                 coberr));

            //E //#// System_out.println("Elemento");
          }
          rs.close();
          st.close();
          rs = null;
          st = null;
        }

        // Para el total de registros del panel del informe
        if ( (datosEq.getNiv1().compareTo("") != 0)
            && (datosEq.getNiv2().compareTo("") != 0)) {

          if (param.getFilter().length() > 0) {

            /*query = "select count(*) "
               + " from sive_c_notif "
               + " where IT_COBERTURA='S' "
               + " and IT_BAJA='N' and CD_CENTRO > ? ";*/
            query = "select count(*) from sive_c_notif where it_baja <> 'S' " +
                " and it_cobertura = 'S' and cd_centro in (select cd_centro " +
                " from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_nivel_2 = ? and cd_e_notif in " +
                " (select cd_e_notif from sive_notif_sem where " +
                " cd_anoepi = ? and cd_semepi = ?)) and cd_centro > ?";
          }
          else {

            /*query = "select count(*) from sive_c_notif "
               + " where IT_COBERTURA='S' "
               + " and IT_BAJA='N' ";*/
            /////////////////// JLT
            query = "select count(*) from sive_c_notif where it_baja <> 'S' " +
                " and it_cobertura = 'S' and cd_centro in (select cd_centro " +
                " from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_nivel_2 = ? and cd_e_notif in " +
                " (select cd_e_notif from sive_notif_sem where " +
                " cd_anoepi = ? and cd_semepi = ?))";
            ////////////////////////
            // lanza la query
          }
          st = con.prepareStatement(query);

          st.setString(1, datosEq.getNiv1());
          st.setString(2, datosEq.getNiv2());
          st.setString(3, datosEq.getAnyo());
          st.setString(4, datosEq.getSemana());
          // filtro
          if (param.getFilter().length() > 0) {
            st.setString(5, param.getFilter());

          }

        }
        else if ( (datosEq.getNiv1().compareTo("") != 0)
                 && (datosEq.getNiv2().compareTo("") == 0)) {

          if (param.getFilter().length() > 0) {

            /*query = "select count(*) "
               + " from sive_c_notif "
               + " where IT_COBERTURA='S' "
               + " and IT_BAJA='N' and CD_CENTRO > ? ";*/
            query = "select count(*) from sive_c_notif where it_baja <> 'S' " +
                " and it_cobertura = 'S' and cd_centro in (select cd_centro " +
                " from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_e_notif in " +
                " (select cd_e_notif from sive_notif_sem where " +
                " cd_anoepi = ? and cd_semepi = ?)) and cd_centro > ?";
          }
          else {

            /*query = "select count(*) from sive_c_notif "
               + " where IT_COBERTURA='S' "
               + " and IT_BAJA='N' ";*/
            /////////////////// JLT
            query = "select count(*) from sive_c_notif where it_baja <> 'S' " +
                " and it_cobertura = 'S' and cd_centro in (select cd_centro " +
                " from sive_e_notif where cd_nivel_1 = ? and " +
                " cd_e_notif in " +
                " (select cd_e_notif from sive_notif_sem where " +
                " cd_anoepi = ? and cd_semepi = ?))";
            ////////////////////////
            // lanza la query
          }
          st = con.prepareStatement(query);

          st.setString(1, datosEq.getNiv1());
          st.setString(2, datosEq.getAnyo());
          st.setString(3, datosEq.getSemana());
          // filtro
          if (param.getFilter().length() > 0) {
            st.setString(4, param.getFilter());

          }
        } // fin del if niveles

        rs = st.executeQuery();

        if (rs.next()) {
          totalRegistros = rs.getInt(1);

        }
        rs.close();
        st.close();
        rs = null;
        st = null;

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    // prepara los datos
    if (totales.size() > 0) {

      totales.trimToSize();
      data.addElement(totales);
      data.addElement(new Integer(totalRegistros));
      data.trimToSize();
      //E //#// System_out.println("SrvInfCobSem devuelvo registrso");
    }
    else {
      data = null;
      //data.setState(CLista.listaVACIA);
    }

    return data;
  }
}
