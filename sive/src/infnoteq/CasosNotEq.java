package infnoteq;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosNotEq
    extends CApp {

  ResourceBundle res;

  public CasosNotEq() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("infnoteq.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;

    VerPanel(res.getString("msg2.Text"), new Pan_NotEq(a), false);
    VerPanel(res.getString("msg2.Text"));
  }

}