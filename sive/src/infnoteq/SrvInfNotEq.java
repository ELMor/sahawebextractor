
package infnoteq;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//import infcobsem.*;
import java.text.SimpleDateFormat;
import java.util.Vector;

import Registro.RegistroConsultas;
import capp.CLista;
import sapp.DBServlet;

public class SrvInfNotEq
    extends DBServlet {

  protected String query = "";
  protected String periodo = "";

  // informes
  final int erwCASOS_EDO = 1;

  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;

    ResultSet rs = null;

    // control
    int i = 1;
    int posTotal = 1;
    int posRelativa = 1;
    int paso = 1;
    //String totCod = null;
    int totTeorico = 0;
    int totReal = 0;
    float totCobertura = 0;
    int todoTeorico = 0;
    int todoReal = 0;
    float todoCobertura = 0;
    int todoLineas = 0;
    //String sCodigos = null;
    //DataInfNotEq Auxiliar = null;
    Vector totales = new Vector();
    int totalRegistros = 0;

    // buffers
    String sCod;
    int iNum;
    DataInfNotEq dat = null;
    Param_C10 datoAuxi = null;

    // objetos de datos
    CLista data = new CLista();
    Vector registros = new Vector();
    Vector registrosAux = new Vector();
    Vector ultSem = new Vector();

    java.util.Date dFecha = null;
    java.sql.Date sqlFec = null;

    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    dat = (DataInfNotEq) param.firstElement();
    // modos de operaci�n
    switch (opmode) {

      // l�neas del modelo
      case erwCASOS_EDO:
        try {

          // primero seleccionamos de sive_notif_edo
          query =
              "select CD_ANOEPI,CD_SEMEPI,FC_RECEP,IT_RESSEM, FC_FECNOTIF, NM_NNOTIFR "
              + "from sive_notifedo where CD_E_NOTIF=? ";

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            periodo =
                " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
          }
          else {
            periodo = " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";
          }
          query += periodo;
          // System_out.println("SrvInfNotEq control 1");

          st = con.prepareStatement(query);
          // c�digo equipo notificador
          st.setString(1, dat.sCdEquipo);

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            // semana desde
            st.setString(2, dat.sSemDesde);
            // semana hasta
            st.setString(3, dat.sSemHasta);
            // a�o
            st.setString(4, dat.sAnoDesde);
          }
          else { // los a�os inicial y final son distintos
            // a�o inicial
            st.setString(2, dat.sAnoDesde);
            st.setString(4, dat.sAnoDesde);
            // semana inicial
            st.setString(3, dat.sSemDesde);
            // a�o final
            st.setString(5, dat.sAnoHasta);
            st.setString(6, dat.sAnoHasta);
            // semana final
            st.setString(7, dat.sSemHasta);
          }
          rs = st.executeQuery();
          registros = new Vector();

          while (rs.next()) {
            sqlFec = rs.getDate("FC_RECEP");
            java.sql.Date d = rs.getDate("FC_FECNOTIF");
            // System_out.println("SrvInfNotEq  Comprobacion 1.1");

            //AIC
            String anoEpi = rs.getString("CD_ANOEPI");
            if (anoEpi != null) {
              anoEpi = anoEpi.trim();
            }
            else {
              anoEpi = "";

            }
            String semEpi = rs.getString("CD_SEMEPI");
            if (semEpi != null) {
              semEpi = semEpi.trim();
            }
            else {
              semEpi = "";

            }
            String itRessem = rs.getString("IT_RESSEM");
            if (itRessem != null) {
              itRessem = itRessem.trim();
            }
            else {
              itRessem = "";

            }
            String nmNotifr = rs.getString("NM_NNOTIFR");
            if (nmNotifr != null) {
              nmNotifr = nmNotifr.trim();
            }
            else {
              nmNotifr = "";

              /*datoAuxi = new Param_C10( rs.getString("CD_ANOEPI").trim()+"-"+rs.getString("CD_SEMEPI").trim(),
                   formater.format(sqlFec),//(rs.getString("FC_RECEP").substring(0,10)),
                   "","",rs.getString("IT_RESSEM").trim(),"",rs.getString("NM_NNOTIFR").trim(),
                                        formater.format(d));*/
            }
            datoAuxi = new Param_C10(anoEpi + "-" + semEpi,
                                     formater.format(sqlFec), //(rs.getString("FC_RECEP").substring(0,10)),
                                     "", "", itRessem, "", nmNotifr,
                                     formater.format(d));
            //\AIC

            // System_out.println("SrvInfNotEq  Comprobacion 1.2");
            registros.addElement(datoAuxi);

          }

          rs.close();
          st.close();
          rs = null;
          st = null;
          // n�mero total de registros
          totalRegistros = registros.size();

          // System_out.println("SrvInfNotEq control 2");

          // ahora accedemos a sive_edonum
          if (registros.size() > 0) {
            registrosAux = new Vector();
            // recorremos el vector
            for (int k = 0; k < registros.size(); k++) {
              datoAuxi = (Param_C10) registros.elementAt(k);

              query = "select sum(NM_CASOS) from sive_edonum " +
                  "where CD_E_NOTIF=? and CD_ANOEPI=? and" +
                  " CD_SEMEPI=? and " +
                  " FC_RECEP=? and FC_FECNOTIF=? "; //group by CD_E_NOTIF";

              st = con.prepareStatement(query);

              // c�digo equipo notificador
              st.setString(1, dat.sCdEquipo);
              // a�o epidemiol�gico
              st.setString(2,
                           datoAuxi.DS_SEM.substring(0,
                  (datoAuxi.DS_SEM.indexOf("-"))));
              // semana epidemiol�gica
              st.setString(3,
                           datoAuxi.DS_SEM.substring( (datoAuxi.DS_SEM.
                  indexOf("-")) + 1));

              dFecha = formater.parse(datoAuxi.FC_RECEP);
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(4, sqlFec);
              dFecha = formater.parse(datoAuxi.FC_NOTIF);
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);

              rs = st.executeQuery();
              if (rs.next()) {
                datoAuxi.NM_NUM = Integer.toString(rs.getInt(1));

              }

              registrosAux.addElement(datoAuxi);
              rs.close();
              st.close();
              rs = null;
              st = null;
            }
          }

          // ahora sive_edoind
          registros = new Vector();
          if (registrosAux.size() > 0) {
            registros = new Vector();
            // recorremos el vector
            for (int k = 0; k < registrosAux.size(); k++) {
              datoAuxi = (Param_C10) registrosAux.elementAt(k);

              query = "select count(*) from sive_edoind " +
                  "where NM_EDO in (select NM_EDO from sive_notif_edoi " +
                  "where CD_ANOEPI=? and CD_SEMEPI=? and CD_E_NOTIF=? and FC_RECEP=? and FC_FECNOTIF = ? and CD_FUENTE = 'E' )";
              //and FC_NOTIF=?";

              // System_out.println("SrvInfNotEq control 3");

              st = con.prepareStatement(query);

              // c�digo equipo notificador
              st.setString(3, dat.sCdEquipo);
              // a�o epidemiol�gico
              st.setString(1,
                           datoAuxi.DS_SEM.substring(0,
                  (datoAuxi.DS_SEM.indexOf("-"))));
              // semana epidemiol�gica
              st.setString(2,
                           datoAuxi.DS_SEM.substring( (datoAuxi.DS_SEM.
                  indexOf("-")) + 1));

              dFecha = formater.parse(datoAuxi.FC_RECEP);
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(4, sqlFec);
              dFecha = formater.parse(datoAuxi.FC_NOTIF);
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);

              // ARG: Se a�aden parametros
              registroConsultas.insertarParametro(datoAuxi.DS_SEM.substring(0,
                  (datoAuxi.DS_SEM.indexOf("-"))));
              registroConsultas.insertarParametro(datoAuxi.DS_SEM.substring( (
                  datoAuxi.DS_SEM.indexOf("-")) + 1));
              registroConsultas.insertarParametro(dat.sCdEquipo);
              registroConsultas.insertarParametro("TO_DATE('" +
                                                  datoAuxi.FC_RECEP +
                                                  "','DD/MM/YYYY')");
              registroConsultas.insertarParametro("TO_DATE('" +
                                                  datoAuxi.FC_NOTIF +
                                                  "','DD/MM/YYYY')");

              rs = st.executeQuery();

              // ARG: Registro LORTAD
              registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                          "", "SrvInfNotEq", true);

              if (rs.next()) {
                int el_Valor = 0;
                el_Valor = rs.getInt(1);
                datoAuxi.NM_INDIV = Integer.toString(el_Valor);

              }
              registros.addElement(datoAuxi);
              rs.close();
              st.close();
              rs = null;
              st = null;
            }
          }
          // System_out.println("SrvInfNotEq control 4");
          // ahora sive_notif_sem
          registrosAux = new Vector();
          if (registros.size() > 0) {
            // recorremos el vector
            for (int k = 0; k < registros.size(); k++) {
              datoAuxi = (Param_C10) registros.elementAt(k);
              query =
                  "select sum(NM_NNOTIFT), sum(NM_NTOTREAL) from sive_notif_sem " +
                  "where CD_E_NOTIF=? and CD_ANOEPI=? and" +
                  " CD_SEMEPI=?   group by CD_E_NOTIF";
              st = con.prepareStatement(query);
              // c�digo equipo notificador
              st.setString(1, dat.sCdEquipo);
              // a�o epidemiol�gico
              st.setString(2,
                           datoAuxi.DS_SEM.substring(0,
                  (datoAuxi.DS_SEM.indexOf("-"))));
              // semana epidemiol�gica
              st.setString(3,
                           datoAuxi.DS_SEM.substring( (datoAuxi.DS_SEM.
                  indexOf("-")) + 1));

//              dFecha = formater.parse(datoAuxi.FC_RECEP);
//              sqlFec = new java.sql.Date(dFecha.getTime());
//        st.setDate(4, sqlFec);
              //  dFecha = formater.parse(datoAuxi.FC_NOTIF);
              // sqlFec = new java.sql.Date(dFecha.getTime());
              // st.setDate(5, sqlFec);

              rs = st.executeQuery();
              if (rs.next()) {
                if (datoAuxi.IT_RES.equals("S")) {
                  datoAuxi.NM_NTEO = Integer.toString(rs.getInt(1));
                  //ARG: El valor de obtiene de SIVE_NOTIFEDO
                  //datoAuxi.NM_NREAL = Integer.toString(rs.getInt(2));
                }
                else {
                  datoAuxi.NM_NTEO = " ";
                  datoAuxi.NM_NREAL = " ";
                }
              }
              totales.addElement(datoAuxi);
              rs.close();
              st.close();
              rs = null;
              st = null;
            }
          }

        }
        catch (Exception er) {
          er.printStackTrace();

        }
        break;
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    // System_out.println("SrvInfNotEq  Comprobaci�n final");
    if (totales.size() > 0) {
      totales.trimToSize();
      data.addElement(totales);
      data.addElement(new Integer(totalRegistros));
      data.trimToSize();

    }
    else {
      data = null;

    }

    return data;
  }

}
