package infnoteq;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class Pan_InfNotEq
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvInfNotEq";
  final String strLOGO_CCAA = "images/ccaa.gif";

  final int erwCASOS_EDO = 1;
  public Param_C10 paramC1 = new Param_C10();
  public DataInfNotEq datosPar = null;

  // estructuras de datos
  protected Vector vAgrupaciones;
  protected Vector vTotales;
  protected Vector vCasos;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // Para pedir mas paginas
  protected Vector ultSemana = new Vector();

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;

  Integer regMostrados = new Integer(0);

  public Pan_InfNotEq(CApp a) {
    super(a);
    app = a;

    try {
      res = ResourceBundle.getBundle("infnoteq.Res" + a.getIdioma());
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWNotEq.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // todo el informe
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();
        param.setFilter(lista.getFilter());
        // obtiene los datos del servidor
        //datosPar.iPagina++;
        param.addElement(datosPar);
        param.setFilter("");
        param.setIdioma(app.getIdioma());

        // ARG: Se introduce el login
        param.setLogin(app.getLogin());
        // ARG: Se introduce la lortad
        param.setLortad(app.getParameter("LORTAD"));

        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));

        lista = (CLista) stub.doPost(erwCASOS_EDO, param);

        v = (Vector) lista.elementAt(0);
        vTotales = (Vector) lista.elementAt(1);

        for (int j = 0; j < v.size(); j++) {
          vCasos.addElement(v.elementAt(j));

          // control de registros
        }
        Integer tot = (Integer) lista.elementAt(1);
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();
    boolean elBoolean = true;

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      // obtiene los datos del servidor
      paramC1.iPagina = 0;

      // �todos los datos?
      datosPar.bInformeCompleto = conTodos;

      //param.addElement(paramC1);
      param.addElement(datosPar);

      param.setIdioma(app.getIdioma());

      // ARG: Se introduce el login
      param.setLogin(app.getLogin());
      // ARG: Se introduce la lortad
      param.setLortad(app.getParameter("LORTAD"));

      param.trimToSize();

      stub.setUrl(new URL(app.getURL() + strSERVLET));

      lista = new CLista();

      lista.setState(CLista.listaVACIA);

      lista = (CLista) stub.doPost(erwCASOS_EDO, param);

      /*
             SrvInfNotEq servlet = new SrvInfNotEq();
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                             "dba_edo",
                             "manager");
            lista = (CLista) servlet.doDebug(erwCASOS_EDO, param);
       */

      // control de registros
      if (lista == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg3.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        elBoolean = false;
      }
      else {

        vCasos = (Vector) lista.elementAt(0);
        Integer tot = (Integer) lista.elementAt(1);
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());
        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("DS_SEM = DS_SEM");
        retval.addElement("FC_RECEP = FC_RECEP");
        retval.addElement("NM_NUM = NM_NUM");
        retval.addElement("NM_INDIV = NM_INDIV");
        retval.addElement("IT_RES = IT_RES");
        retval.addElement("NM_NTEO = NM_NTEO");
        retval.addElement("NM_NREAL = NM_NREAL");

        dataHandler.RegisterTable(vCasos, "SIVE_C10", retval, null);

        // ARG: Se define la resoluci�n de la impresora
        erwClient.setPrinterResolution(false, 72);

        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        elBoolean = true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;
  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    // carga los citerios
    tm.SetLabel("CRITERIO1",
                res.getString("msg4.Text") + datosPar.sAnoDesde + " " +
                res.getString("msg5.Text") + datosPar.sSemDesde
                + res.getString("msg6.Text") + datosPar.sAnoHasta + " " +
                res.getString("msg7.Text") + datosPar.sSemHasta);
    tm.SetLabel("CRITERIO2",
                res.getString("msg8.Text") + datosPar.sCdEquipo + " " +
                datosPar.sDsEquipo);

  }
}
