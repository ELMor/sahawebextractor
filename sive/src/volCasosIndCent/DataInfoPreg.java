//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package volCasosIndCent;

import java.io.Serializable;

public class DataInfoPreg
    implements Serializable {
// Clase para recuperar el codigo y la descripcion
// de modelos y preguntas

  public String codMod = new String();
  public String desMod = new String();
  public String fechaAlta = new String();
  public String fechaBaja = new String();
  public String linea = new String();
  public String codPreg = new String();
  public String desPreg = new String();
  public String tSive = new String();

  public DataInfoPreg() {
  }

  public DataInfoPreg(String cMod, String dMod,
                      String fA, String fB, String l,
                      String cPreg, String dPreg, String tS) {
    codMod = cMod;
    desMod = dMod;
    fechaAlta = fA;
    fechaBaja = fB;
    linea = l;
    codPreg = cPreg;
    desPreg = dPreg;
    tSive = tS;
  }

}
