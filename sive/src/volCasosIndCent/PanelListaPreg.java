//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package volCasosIndCent;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import centinelas.cliente.c_comuncliente.nombreservlets;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;
import sapp.StubSrvBD;

public class PanelListaPreg
    extends CPanel {

  int numCol;
  ResourceBundle res;
  String tamCol;
  String nomCol;

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int modoINFOPREG = 7;

  // lista para el servlet
  public CLista lista = null;

  // Para el 'where'
  private String condicion = "";

  // stub
  public StubSrvBD stubCliente = null;

  public Vector listaPreg = new Vector();

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  // servlet
  String sUrlServlet;

  // filtro de b�squeda
  protected String sFiltro;

  // modo del servlet
  public int modoServlet;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/refrescar.gif",
      "images/alta2.gif",
      "images/baja2.gif",
      "images/modificacion2.gif",
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif",
      "images/auxiliar.gif"};

  // indica ela fila seleccionada en la tabla */
 public int m_itemSelecc = -1;

  XYLayout xYLayout1 = new XYLayout();
  CheckboxGroup checkboxGroup1 = new CheckboxGroup();
  public CTabla tabla = new CTabla();
  ButtonControl btnAnadir = new ButtonControl();
  ButtonControl btnBorrar = new ButtonControl();
  ButtonControl btnPrimero = new ButtonControl();
  ButtonControl btnAnterior = new ButtonControl();
  ButtonControl btnSiguiente = new ButtonControl();
  ButtonControl btnUltimo = new ButtonControl();
  ButtonControl btnAux = new ButtonControl();

  PanelListaPregbtnNormalActionListener btnNormalActionListener = new
      PanelListaPregbtnNormalActionListener(this);
  PanelListaPreg_tabla_actionAdapter tablaActionListener = new
      PanelListaPreg_tabla_actionAdapter(this);
  PanelListaPregItemListener multlstItemListener = new
      PanelListaPregItemListener(this);
  Label lblPreguntas = new Label();

  DialogoMasPreg masPreg = null;

  //_____________________________________________________

  // constructor
  public PanelListaPreg(CApp a) {

    try {
      stubCliente = new StubSrvBD();
//      sUrlServlet = "servlet/SrvModPregCent";
      sUrlServlet = nombreservlets.strSERVLET_MOD_PREG_CENT;
      lista = new CLista();
      setApp(a);
      res = ResourceBundle.getBundle("volCasosIndCent.Res" + app.getIdioma());
      jbInit();
      masPreg = new DialogoMasPreg(a);

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //_____________________________________________________

  // inicializa el aspecto del panel
  public void jbInit() throws Exception {
    xYLayout1.setWidth(608);
    xYLayout1.setHeight(350);
    this.setLayout(xYLayout1);

    btnAnadir.setActionCommand("A�adir");
    btnBorrar.setActionCommand("Borrar");
    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");
    btnAux.setActionCommand("Auxiliar");

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAnadir.setImage(imgs.getImage(1));
    btnBorrar.setImage(imgs.getImage(2));
    btnPrimero.setImage(imgs.getImage(4));
    btnAnterior.setImage(imgs.getImage(5));
    btnSiguiente.setImage(imgs.getImage(6));
    btnUltimo.setImage(imgs.getImage(7));
    btnAux.setImage(imgs.getImage(8));
    lblPreguntas.setAlignment(1);
    lblPreguntas.setText(res.getString("lblPreguntas2.Text"));

    this.add(lblPreguntas, new XYConstraints(10, 20, 580, -1));
    this.add(tabla, new XYConstraints(10, 50, 580, 200));
    this.add(btnAnadir, new XYConstraints(10, 257, 26, 26));
    this.add(btnBorrar, new XYConstraints(50, 257, 26, 26));
    this.add(btnAux, new XYConstraints(262, 257, 26, 26));
    this.add(btnPrimero, new XYConstraints(459, 257, 22, 22));
    this.add(btnAnterior, new XYConstraints(494, 257, 22, 22));
    this.add(btnSiguiente, new XYConstraints(529, 257, 22, 22));
    this.add(btnUltimo, new XYConstraints(564, 257, 22, 22));

    // configuraci�n de la tabla
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla3.ColumnButtonsStrings")), '\n'));
    tabla.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "170\n393"), '\n'));
    //tabla.setColumnLabelsStrings(jclass.util.JCUtilConverter.toStringList(new String("Codigo\nDescripcion"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla4.ColumnButtonsStrings")), '\n'));
    tabla.setNumColumns(2);
    tabla.setAutoSelect(true);

    tabla.addActionListener(tablaActionListener);
    tabla.addItemListener(multlstItemListener);

    btnPrimero.addActionListener(btnNormalActionListener);
    btnAnterior.addActionListener(btnNormalActionListener);
    btnSiguiente.addActionListener(btnNormalActionListener);
    btnUltimo.addActionListener(btnNormalActionListener);

    btnAnadir.addActionListener(btnNormalActionListener);
    btnBorrar.addActionListener(btnNormalActionListener);
    btnAux.addActionListener(btnNormalActionListener);

    btnAux.setVisible(false);

    Inicializar();
  }

  //_____________________________________________________

  // activaci�n del bot�n auxiliar
  public void ActivarAuxiliar() {
    btnAux.setVisible(true);
  }

  //_____________________________________________________

  public void Inicializar() {
    switch (modoOperacion) {

      case modoNORMAL:

        // modo modificaci�n y baja
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        btnAnadir.setEnabled(true);
        btnBorrar.setEnabled(true);
        btnAux.setEnabled(true);
        tabla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.doLayout();
        break;

      case modoESPERA:
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        btnAnadir.setEnabled(false);
        btnAux.setEnabled(false);
        btnBorrar.setEnabled(false);
        tabla.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.doLayout();
        break;
    }
    doLayout();
  }

  //_____________________________________________________

  // limpia la pantalla
  void vaciarPantalla() {
    // Vaciamos tambi�n el dialoguito.
    masPreg.vaciarPantalla();
    condicion = "";
    // Vaciamos totalmente la tabla:
    listaPreg.removeAllElements();
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);

    }
  }

  //_____________________________________________________

  // Pasa el par�metro para el'where' de las listas
  void pasaWhere(String condicion) {
    this.condicion = condicion;
  }

  //_____________________________________________________

  // boton borrar/auxiliar
  public void btn_actionPermormed(ActionEvent e) {
    /*int indice = tabla.getSelectedIndex();
         if (indice != BWTEnum.NOTFOUND) {
      if (e.getActionCommand().equals("Borrar")) // borrar
        btnBorrar_actionPerformed(indice);
      else if (e.getActionCommand().equals("Auxiliar")) // boton auxiliar
        btnAuxiliar_actionPerformed(indice);
         } */
  }

  //_____________________________________________________

  // doble click en la tabla
  // Se muestra informacion acerca de la pregunta
  //  modelo/FechAlta/FechBaja/Linea/Pregunta
  public void tabla_actionPerformed() {

    //#// System_out.println("En tabla_actionPerformed");

    CMessage msgBox = null;
    CLista data = null;
    int iLast;
    Object componente;
    int indice = tabla.getSelectedIndex();

    // procesa
    try {

      modoOperacion = modoESPERA;
      Inicializar();

      // �ltimo indice
      iLast = tabla.countItems() - 1;

      // obtiene y pinta la lista nueva
      data = new CLista();

      // alamcena los par�metros para recuperar las tramas
      data.setFilter(lista.getFilter());

      // idioma
      data.setIdioma(app.getIdioma());

      // estado
      data.setState(CLista.listaINCOMPLETA);

      //Extraigo los datos para la consulta
      DataModPreg dato = (DataModPreg) listaPreg.elementAt(indice);

      data.addElement(setComponente(dato.tSive + '&' + dato.codPreg));

      stubCliente.setUrl(new URL(app.getURL() + sUrlServlet));

      data = (CLista) stubCliente.doPost(this.modoINFOPREG, data);

      /*
                  SrvModPregCent servlet = new SrvModPregCent();
                 //Indica como conectarse a la b.datos
           servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
                 data = (CLista) servlet.doDebug(this .modoINFOPREG, data);
       */

      //#// System_out.println("En tabla_actionPerformed despues del servlet ");

      DialogoInfoPreg infoPreg = new DialogoInfoPreg(this.getApp());

      infoPreg.rellenarTabla(data);
      infoPreg.setVisible(true);
      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //_____________________________________________________

  void tabla_itemStateChanged(JCItemEvent evt) {

    m_itemSelecc = tabla.getSelectedIndex();

  }

  //_____________________________________________________

  // pinta la tabla
  public void rellenarTabla() {
    DataModPreg dato;
    tabla.clear();
    //#// System_out.println("He vaciado la tabla");
    for (int i = 0; i < listaPreg.size(); i++) {
      dato = (DataModPreg) listaPreg.elementAt(i);
      tabla.addItem(setLinea(dato), '&');
      //#// System_out.println("He puesto una fila");
    }
    //#// System_out.println("He rellenado la tabla");
  }

  //_____________________________________________________

  // confecciona los botones de mantenimiento
  public void ConfigModo(boolean bAlta,
                         boolean bModif,
                         boolean bBaja) {
    final int iY = 257;
    final int iX[] = {
        10, 45, 80};
    int i;

    // remueve todos los botones
    this.remove(btnAnadir);
    this.remove(btnBorrar);
    i = 0;

    // +
    if (bAlta) {
      this.add(btnAnadir, new XYConstraints(iX[i], iY, -1, -1));
      i++;
    }

    // +-
    /*
         if (bModif) {
      this.add(btnModificar, new XYConstraints(iX[i], iY, -1, -1));
      i++;
         }
     */
    // -
    if (bBaja) {
      this.add(btnBorrar, new XYConstraints(iX[i], iY, -1, -1));
    }
  }

  //_____________________________________________________

  // bot�n auxiliar
  public void btnAuxiliar_actionPerformed(int i) {
    //#// System_out.println(i);
  }

  //_____________________________________________________

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {

    //#// System_out.println("Boton nueva fila en la tabla");

    modoOperacion = modoESPERA;
    Inicializar();

    DataModPreg filaTabla = new DataModPreg();

    masPreg.txtCodPreg.setText("");
    masPreg.txtDesPreg.setText("");
    masPreg.setWhere(condicion);
    masPreg.setVisible(true);

    if (masPreg.resultado()) {
      // Para que si se cierra con los controles de la ventana
      // no inserte null null
      filaTabla = masPreg.obtenerPregunta();
      listaPreg.addElement(filaTabla);
      //He obtenido la fila y voy a rellenar la tabla
      rellenarTabla();
    }
    //#// System_out.println("Salgo del boton para insertar");
    modoOperacion = modoNORMAL;
    Inicializar();

  }

  //_____________________________________________________

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed() {
    listaPreg.removeElementAt(tabla.getSelectedIndex());
    rellenarTabla();
  }

  //_____________________________________________________

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    String tS = s.substring(0, s.indexOf('&'));
    String cPreg = s.substring(s.indexOf('&') + 1);
    return new DataInfoPreg("", "", "", "", "", cPreg, "", tS);
  }

  //_____________________________________________________

  // formatea el componente para ser insertado en una fila
  public String setLinea(DataModPreg o) {
    return new String(o.codPreg + '&' + o.desPreg);
  }

  //_____________________________________________________

  public String obtenerCodPreg() {
    String cods = new String();

    if (listaPreg.size() > 0) {
      cods = "(";
      DataModPreg dato = new DataModPreg();
      for (int j = 0; j < listaPreg.size(); j++) {
        dato = (DataModPreg) listaPreg.elementAt(j);
        cods = cods + "'" + dato.codPreg + "'";
        if (j != listaPreg.size() - 1) {
          cods = cods + ",";
        }
      }
      cods = cods + ")";
    }
    else {
      cods = "('')";
    }

    return cods;
  }

}

//FIN CLASE PUBLICA
    /******************************************************************************/

class PanelListaPregbtnNormalActionListener
    implements ActionListener, Runnable {
  PanelListaPreg adaptee = null;
  ActionEvent e = null;

  public PanelListaPregbtnNormalActionListener(PanelListaPreg adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    this.e = e;
    if (e.getActionCommand().equals("A�adir")) { // a�adir
      adaptee.btnAnadir_actionPerformed();
    }
    else if (e.getActionCommand().equals("Borrar")) { // a�adir
      adaptee.btnBorrar_actionPerformed();
    }
    else if (e.getActionCommand() == "primero") {
      if (adaptee.tabla.countItems() > 0) {
        adaptee.m_itemSelecc = 0;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        adaptee.tabla.setTopRow(adaptee.m_itemSelecc);
      }
    }
    else if (e.getActionCommand() == "anterior") {
      if (adaptee.m_itemSelecc > 0) {
        adaptee.m_itemSelecc--;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.m_itemSelecc - adaptee.tabla.getTopRow() <= 0) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() - 1);
        }
      }
    }
    else if (e.getActionCommand() == "siguiente") {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (adaptee.m_itemSelecc < ultimo) {
        adaptee.m_itemSelecc++;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.m_itemSelecc - adaptee.tabla.getTopRow() >= 8) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() + 1);
        }
      }
    }
    else if (e.getActionCommand() == "ultimo") {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (ultimo >= 0) {
        adaptee.m_itemSelecc = ultimo;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.tabla.countItems() >= 8) {
          adaptee.tabla.setTopRow(adaptee.tabla.countItems() - 8);
        }
        else {
          adaptee.tabla.setTopRow(0);
        }
      }
    }
  }

}

// escuchador de los click en la tabla
class PanelListaPreg_tabla_actionAdapter
    implements jclass.bwt.JCActionListener {
  PanelListaPreg adaptee;

  PanelListaPreg_tabla_actionAdapter(PanelListaPreg adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    GestionTabla th = new GestionTabla(adaptee);
    th.run();
  }
}

//Tambi�n implementa Runnable por si usuario pulsa m�s..
class PanelListaPregItemListener
    implements JCItemListener, Runnable {
  PanelListaPreg adaptee = null;
  JCItemEvent e = null;

  public PanelListaPregItemListener(PanelListaPreg adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      this.e = e;
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

//    if (((JCMultiColumnList)e.getSource()).getName() == "tabla")
    adaptee.tabla_itemStateChanged(e);
  }
}

// hilo de ejecuci�n para la gesti�n del doble click en la tabla
class GestionTabla
    implements Runnable {
  PanelListaPreg adaptee;

  public GestionTabla(PanelListaPreg l) {
    adaptee = l;
    Thread th = new Thread();
    th.run();
  }

  // obtiene la nueva trama de la lista
  public void run() {
    adaptee.tabla_actionPerformed();
  }
}
