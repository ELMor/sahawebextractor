
package disnotasis;

import java.util.Vector;

import capp.CLista;
import conspacenf.Data;
import sapp.DBServlet;

/** Distribuci�n de los casos notificados seg�n nivel asistencial
 *
 *  filtros:  desde a�o y semana
 *            hasta a�o y semana
 *            enfermedad
 *            distrito
 *            �rea  // por defecto Comunidad de Madrid
 *
 */
public class DataDisNotAsis
    extends Data {

  /** campos que va a tener */
  public static final int COUNT = 0;
  public static final int DS_PROCESO = 1; // enfermedad
  public static final int DS_AP_1 = 2;
  public static final int DS_AP_2 = 3;
  public static final int DS_AE_1 = 4;
  public static final int DS_AE_2 = 5;
  public static final int DS_ON_1 = 6;
  public static final int DS_ON_2 = 7;

  /** parametros que tiene */
  // n�mero de par�metros de esta sentencia
  public static final int P_NUM_C3 = 5;

  // posici�n de los diferentes par�metrso en la select
  public static final int P_PAGINA = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_DESDE_CD_ANOEPI1 = 1;
  public static final int P_DESDE_CD_SEMEPI2 = 2;
  public static final int P_DESDE_CD_ANOEPI3 = 3;
  public static final int P_HASTA_CD_ANOEPI4 = 4;
  public static final int P_HASTA_CD_ANOEPI5 = 5;
  public static final int P_HASTA_CD_SEMEPI6 = 6;
  public static final int P_CD_NIVEL_1 = 7;
  public static final int P_CD_NIVEL_2 = 8;
  public static final int P_PERMISO = 9;
  public static final int P_DS_NIVEL_1 = 10;
  public static final int P_DS_NIVEL_2 = 11;

  /** modos que tiene */
  public static final int modoDISNOTASIS = 1;
  public static final int modoDISNOTASISCOUNT = 2;

  //Total registros del informe (los que van si se indica informe completo
  protected int iTotales = 0;

  public void setTotales(int tot) {
    iTotales = tot;
  }

  public int getTotales() {
    return iTotales;
  }

  public DataDisNotAsis(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[10];
  }

  public Object getNewData() {
    return new DataDisNotAsis(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "NM_EDO";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  /** esta funci�n devuelve la sentencia a ejecutar */
  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    Data param_item = (Data) ini_param.firstElement();
    String hasta_ano = (String) param_item.get(DataDisNotAsis.
                                               P_HASTA_CD_ANOEPI5);
    String desde_ano = (String) param_item.get(DataDisNotAsis.
                                               P_DESDE_CD_ANOEPI1);

    switch (opmode) {
      case modoDISNOTASIS:

        /*             if (hasta_ano.equals(desde_ano)){
                         strSQL.append(sqlJOIN_ENFERMO_EDOIND + filtroIguales);
                     }else{
             strSQL.append(sqlJOIN_ENFERMO_EDOIND + filtroNoIguales);
                     }
         */break;

    }

    // indicamos le n�merod e registros que queremos
    return DBServlet.maxSIZE; // por defecto
  }

  public String getDS_PROCESO() {
    if (arrayDatos.length > DS_PROCESO) {
      return (String) arrayDatos[DS_PROCESO];
    }
    else {
      return " ";
    }
  }

  public int getDS_AP_1() {
    String dato;
    if (arrayDatos.length > DS_AP_1) {
      dato = (String) arrayDatos[DS_AP_1];
      if (dato != null) {
        return Integer.parseInt(dato);
      }
    }

    return 0;
  }

  public int getDS_AP_2() {
    String dato;
    if (arrayDatos.length > DS_AP_2) {
      dato = (String) arrayDatos[DS_AP_2];
      if (dato != null) {
        return Integer.parseInt(dato);
      }
    }

    return 0;
  }

  public int getDS_AE_1() {
    String dato;
    if (arrayDatos.length > DS_AE_1) {
      dato = (String) arrayDatos[DS_AE_1];
      if (dato != null) {
        return Integer.parseInt(dato);
      }
    }

    return 0;
  }

  public int getDS_AE_2() {
    String dato;
    if (arrayDatos.length > DS_AE_2) {
      dato = (String) arrayDatos[DS_AE_2];
      if (dato != null) {
        return Integer.parseInt(dato);
      }
    }

    return 0;
  }

  public int getDS_ON_1() {
    String dato;
    if (arrayDatos.length > DS_ON_1) {
      dato = (String) arrayDatos[DS_ON_1];
      if (dato != null) {
        return Integer.parseInt(dato);
      }
    }

    return 0;
  }

  public int getDS_ON_2() {
    String dato;
    if (arrayDatos.length > DS_ON_2) {
      dato = (String) arrayDatos[DS_ON_2];
      if (dato != null) {
        return Integer.parseInt(dato);
      }
    }

    return 0;
  }

  /** indica al report como leer los datos */
  public Vector getFieldVector() {
    Vector retval = new Vector();

    retval.addElement("DS_PROCESO  = getDS_PROCESO");
    retval.addElement("DS_AP_1  = getDS_AP_1");
    retval.addElement("DS_AP_2  = getDS_AP_2");
    retval.addElement("DS_AE_1  = getDS_AE_1");
    retval.addElement("DS_AE_2  = getDS_AE_2");
    retval.addElement("DS_ON_1  = getDS_ON_1");
    retval.addElement("DS_ON_2  = getDS_ON_2");

    return retval;
  }

} //____________________________________ END_CLASS