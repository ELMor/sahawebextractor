
package disnotasis;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class PanelInforme
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvDisNotAsis";

  final int MODO_SERVLET = DataDisNotAsis.modoDISNOTASIS;
  final int MODO_SERVLETCOUNT = DataDisNotAsis.modoDISNOTASISCOUNT;

  /** guarda el n�mero de registros */
  protected String num_reg = "0";

  // estructuras de datos
  protected Vector vCasos = null;

  /** esta es la lista que recoge los datos */
  protected CLista lista;

  /** esta es la lista que contiene los par�metros */
  protected CLista param = null;

  /** n�mero de p�gina por el que se va */
  int num_pag = 0;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // vector y nombre de la tabla y fichero
  String m_file = null;
  String m_table = null;
  Vector m_column = null;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;
  PnlParam p = null;

  public PanelInforme(PnlParam pp, StubSrvBD s, CApp a, String a_file,
                      String a_nombreTabla, Vector a_column) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("disnotasis.Res" + app.getIdioma());
    p = pp;
    m_file = a_file;
    m_table = a_nombreTabla;
    m_column = a_column;
    vCasos = new Vector();

    try {
      stub = s;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(getCApp().getCodeBase().toString() + m_file);

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;

    // incrementamos el n�mero de p�gina
    num_pag++;

    if (lista != null && lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        // obtiene los datos del servidor
        DataDisNotAsis data = (DataDisNotAsis) param.firstElement();
        param.setIdioma(app.getIdioma());

        // ARG: Se introduce el login
        param.setLogin(app.getLogin());

        // ARG: Se introduce la lortad
        param.setLortad(app.getParameter("LORTAD"));

        data.put(DataDisNotAsis.P_PAGINA, Integer.toString(num_pag));
        stub.setUrl(new URL(getCApp().getURL() + strSERVLET));
        lista = (CLista) stub.doPost(MODO_SERVLET, param);

        // ponemos las etiquetas del report
        fijarEtiqueas();

        if (lista != null) {
          for (int j = 0; j < lista.size(); j++) {
            vCasos.addElement(lista.elementAt(j));
          }

          // control de registros
          this.setTotalRegistros(num_reg);
          this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

          // repintado
          erwClient.refreshReport(true);
        }
        else {
          this.setTotalRegistros("0");
          this.setRegistrosMostrados("0");
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(getCApp(), CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  protected void fijarEtiqueas() {
    String dato = null;
    String linea = null;

    // cambiamos las etiquetas
    DataDisNotAsis DataDisNotAsis = (DataDisNotAsis) param.firstElement();
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000",
                   getCApp().getCodeBase().toString() + "images/ccaa.gif");
    tm.SetImageURL("IMA001",
                   getCApp().getCodeBase().toString() + "images/ccaa.gif");

    // criterio temporal
    tm.SetLabel("CRITERIO1",
                res.getString("msg2.Text") +
                (String) DataDisNotAsis.get(DataDisNotAsis.P_DESDE_CD_ANOEPI1) +
                res.getString("msg3.Text") +
                (String) DataDisNotAsis.get(DataDisNotAsis.P_DESDE_CD_SEMEPI2) +
                res.getString("msg4.Text") +
                (String) DataDisNotAsis.get(DataDisNotAsis.P_HASTA_CD_ANOEPI4) +
                res.getString("msg5.Text") +
                (String) DataDisNotAsis.get(DataDisNotAsis.P_HASTA_CD_SEMEPI6));

    // zonificacion
    /*
        if (DataDisNotAsis.get(DataDisNotAsis.P_CD_NIVEL_1) != null){
          linea = "�rea: " + (String)DataDisNotAsis.get(DataDisNotAsis.P_CD_NIVEL_1) +" "+ (String) DataDisNotAsis.get(DataDisNotAsis.P_DS_NIVEL_1);
          if (DataDisNotAsis.get(DataDisNotAsis.P_CD_NIVEL_2) != null)
            linea = linea + ", Distrito: " + (String)DataDisNotAsis.get(DataDisNotAsis.P_CD_NIVEL_2) + " "+(String) DataDisNotAsis.get(DataDisNotAsis.P_DS_NIVEL_2);
        } else {
          linea = "";
        }
        tm.SetLabel("CRITERIO2", linea);
     */
    if (! (String.valueOf(DataDisNotAsis.get(DataDisNotAsis.P_CD_NIVEL_1)).
           equals("null"))) {
      linea = res.getString("msg6.Text") +
          String.valueOf(DataDisNotAsis.get(DataDisNotAsis.P_CD_NIVEL_1)) + " " +
          p.txtDesAre.getText();
      if (! (String.valueOf(DataDisNotAsis.get(DataDisNotAsis.P_CD_NIVEL_2)).
             equals("null"))) {
        linea = linea + res.getString("msg7.Text") +
            String.valueOf(DataDisNotAsis.get(DataDisNotAsis.P_CD_NIVEL_2)) +
            " " + p.txtDesDis.getText();
      }
    }
    else {
      linea = "";
    }
    tm.SetLabel("CRITERIO2", linea);
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    String dato = null;

    // lo ponemo al inico de la p�gina y  borramos los datos que hab�a
    num_pag = 0;
    vCasos.setSize(0);

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      // obtiene los datos del servidor
      DataDisNotAsis data = (DataDisNotAsis) param.firstElement();
      data.put(DataDisNotAsis.P_PAGINA, Integer.toString(num_pag));
      data.bInformeCompleto = conTodos;
      param.setIdioma(app.getIdioma());

      // ARG: Se introduce el login
      param.setLogin(app.getLogin());

      // ARG: Se introduce la lortad
      param.setLortad(app.getParameter("LORTAD"));

      param.trimToSize();
      stub.setUrl(new URL(getCApp().getURL() + strSERVLET));
      lista = (CLista) stub.doPost(MODO_SERVLETCOUNT, param);

      /*      SrvDisNotAsis srv = new SrvDisNotAsis();
           srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
           lista = srv.doDebug(MODO_SERVLETCOUNT, param);
       */

      if (lista != null && lista.size() > 0) {
        vCasos = (Vector) lista;
        // ponemos las etiquetas del report
        fijarEtiqueas();

        num_reg = (String) ( (DataDisNotAsis) lista.firstElement()).get(
            DataDisNotAsis.COUNT);

        // System_out.println("caso de primaria para primer caso ");
        // System_out.println( ((DataDisNotAsis)lista.firstElement()).getDS_AP_1() );

        this.setTotalRegistros(num_reg);
        this.setRegistrosMostrados(Integer.toString(lista.size()));

        // establece las matrices de datos
        dataHandler.RegisterTable(vCasos, m_table, m_column, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;

      }
      else {
        vCasos.setSize(0);
        msgBox = new CMessage(getCApp(), CMessage.msgAVISO,
                              res.getString("msg8.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(getCApp(), CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  public void MostrarGrafica() {
    TemplateManager tm = erw.GetTemplateManager();

    if (lista != null && lista.getState() == CLista.listaLLENA) {

      // oculta una de las secciones
      if (tm.IsVisible("RP_FTR")) {
        tm.SetVisible("PG_HDR", true);
        tm.SetVisible("DTL_00", true);
        tm.SetVisible("RP_FTR", false);
      }
      else {
        tm.SetVisible("PG_HDR", false);
        tm.SetVisible("DTL_00", false);
        tm.SetVisible("RP_FTR", true);
      }

      // repinta el grafico
      erwClient.refreshReport(true);

    }
  }

  public void setListaParam(CLista a_param) {
    param = a_param;
  }

  public CLista getListaParam() {
    return param;
  }

} //___________________________________ END_CLASS
