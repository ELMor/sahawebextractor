//Title:        Consulta de alarmas automaticas de un periodo
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:

package consAlaAutoPer;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class panInfAlaAutoPer
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final int servletCONSULTA = 7;

  final String strLOGO_CCAA = "images/ccaa.gif";

  final String strSERVLET = "servlet/SrvIndAuto";

  // estructuras de datos
  protected Vector vResultado;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  //uno o varios indicadores
  protected boolean unIndicador = false;

  // conexion con el servlet
  protected StubSrvBD stub;

  public parConsAlaAutoPer parCons;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;

  public panInfAlaAutoPer(CApp a) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("consAlaAutoPer.Res" + a.getIdioma());
    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/Consulta78.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    //E System_out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {

        PrepararInforme();

        // obtiene los datos del servidor
        parCons.numPagina++;

        // para buscar desde la semana en la que se quedo
        parCons.anoDesde = (String) parCons.primSem.elementAt(0);
        parCons.semDesde = (String) parCons.primSem.elementAt(1);

        param.addElement(parCons);
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));
        lista = (CLista) stub.doPost(servletCONSULTA, param);
        String total = "";
        if (lista != null) {
          v = (Vector) lista.elementAt(0);
          if (lista.getState() == CLista.listaINCOMPLETA) {
            parCons.primSem = (Vector) lista.elementAt(2);
            total = res.getString("msg23.Text");
          }
          else {
            total = res.getString("msg24.Text");
          }
          for (int j = 0; j < v.size(); j++) {
            vResultado.addElement(v.elementAt(j));

            // control de registros
          }
          if (vResultado.size() == 0) {
            msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                  res.getString("msg25.Text"));
            msgBox.show();
            msgBox = null;
            this.setLimpiarStatus();
          }
          else {
            this.setRegistrosMostrados( (new Integer(vResultado.size())).
                                       toString());
            //this .setTotalRegistros(calculaTotalRegistros(parCons));
            this.setTotalRegistros(total);
            this.setTotalCasos( (Integer) lista.elementAt(1));

            // repintado
            erwClient.refreshReport(true);
          }
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                res.getString("msg25.Text"));
          msgBox.show();
          msgBox = null;
          this.setLimpiarStatus();
        }

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // para saber el formato del informe
  public void numeroIndicadores(boolean ind) {
    unIndicador = ind;
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      //E System_out.println (res.getString("msg26.Text"));

      // obtiene los datos del servidor
      parCons.numPagina = 0;
      parCons.bInformeCompleto = conTodos;
      param.addElement(parCons);
      param.trimToSize();
      stub.setUrl(new URL(app.getURL() + strSERVLET));
      //E System_out.println ("Voy al servlet de la primera pagina");
      lista = (CLista) stub.doPost(servletCONSULTA, param);
      //E System_out.println ("Vuelvo del servlet de la primera pagina");
//SrvIndAuto srv = new SrvIndAuto();
//lista = srv.doPrueba(servletCONSULTA, param);
//srv = null;

      String total = "";
      if (lista != null) {
        vResultado = (Vector) lista.elementAt(0);
        if (lista.getState() == CLista.listaINCOMPLETA) {
          parCons.primSem = (Vector) lista.elementAt(2);
          total = res.getString("msg23.Text");
        }
        else {
          total = res.getString("msg24.Text");
          // control de registros

        }
        if (vResultado.size() == 0) {
          msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                res.getString("msg25.Text"));
          msgBox.show();
          msgBox = null;
          this.setLimpiarStatus();

          modoOperacion = modoNORMAL;
          Inicializar();
          return false;
        }
        else {
          //this .setTotalRegistros(calculaTotalRegistros(parCons));
          this.setTotalRegistros(total);
          this.setRegistrosMostrados( (new Integer(vResultado.size())).toString());
          this.setTotalCasos( (Integer) lista.elementAt(1));
          //DataConsIndAuto totalDat = new  DataConsIndAuto(res.getString("msg27.Text"),((Integer) lista.elementAt(1)).intValue(), 0, (float)0, (float)0);
          //vResultado.addElement(totalDat);
          // establece las matrices de datos
          Vector retval = new Vector();
          retval.addElement("SEMANA = SEMANA");
          retval.addElement("NM_CASOS = NM_CASOS");
          retval.addElement("UMBRAL_INF = UMBRAL_INF");
          retval.addElement("UMBRAL_SUP = UMBRAL_SUP");
          retval.addElement("NM_VALOR1 = NM_VALOR1");
          retval.addElement("NM_VALOR2 = NM_VALOR2");
          retval.addElement("NM_VALOR3 = NM_VALOR3");
          dataHandler.RegisterTable(vResultado, "SIVE_C8", retval, null);
          erwClient.setInputProperties(erw.GetTemplateManager(),
                                       erw.getDATReader(true));

          // repintado
          //AIC
          this.pack();
          erwClient.prv_setActivePage(0);
          //erwClient.refreshReport(true);

          modoOperacion = modoNORMAL;
          Inicializar();
          return true;
        }
      }
      else {
        msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                              res.getString("msg25.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  private void setTotalCasos(Integer casos) {
    TemplateManager tm = erw.GetTemplateManager();
    tm.SetLabel("LAB022", res.getString("msg28.Text"));
    tm.SetLabel("LAB023", casos.toString());
  }

  private String calculaTotalRegistros(parConsAlaAutoPer par) {
    Integer aD = new Integer(par.anoDesde);
    Integer aH = new Integer(par.anoHasta);
    Integer sD = new Integer(par.semDesde);
    Integer sH = new Integer(par.semHasta);

    int tot = 52 - sD.intValue() + 1;
    tot += sH.intValue();
    tot += (aH.intValue() - aD.intValue() - 1) * 52;

    return (new Integer(tot)).toString();
  }

  protected void PrepararInforme() throws Exception {
    // NOTA:
    // puede haber tres casos segun el numero de indicadores
    // (esto se controla en panconsAlaAutoPer en inicializar()
    // 1.- solo el primer indicador
    // 2.- el primero y el segundo
    // 3.- los tres indicadores

    String sBuffer;

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    // carga los citerios
    /* tm.SetLabel("LAB005", "A�o desde: " + parCons.anoDesde );
         tm.SetLabel("LAB007", "Semana desde: " + parCons.semDesde );
         tm.SetLabel("LAB008", "A�o hasta: " + parCons.anoHasta );
         tm.SetLabel("LAB009", "Semana hasta: " + parCons.semHasta );
         tm.SetLabel("LAB012", "Enfermedad: " + parCons.enfermedad );*/

    tm.SetLabel("CRITERIO1",
                res.getString("msg29.Text") + parCons.anoDesde + " " +
                res.getString("msg30.Text") + parCons.semDesde
                + res.getString("msg31.Text") + parCons.anoHasta + " " +
                res.getString("msg32.Text") + " " + parCons.semHasta);

    tm.SetLabel("CRITERIO2",
                res.getString("msg33.Text") + parCons.enfermedad + " " +
                parCons.nombreEnfermedad);
    tm.SetLabel("CRITERIO3",
                res.getString("msg34.Text") + parCons.ind1 + " " +
                parCons.nombreInd1);

    if (parCons.coef_ds.equals("c")) {
      tm.SetLabel("LAB013", res.getString("msg35.Text") + parCons.ind1);
    }
    else if (parCons.coef_ds.equals("d")) {
      tm.SetLabel("LAB013", res.getString("msg36.Text") + parCons.ind1);
    }
    else {
      tm.SetLabel("LAB013",
                  res.getString("msg37.Text") + parCons.ind1 + " " +
                  parCons.ind1);

    }
    if (!parCons.ind2.equals("")) { //E
      tm.SetLabel("LAB014", res.getString("msg38.Text") + parCons.ind2);
      tm.SetLabel("CRITERIO4",
                  res.getString("msg38.Text") + parCons.ind2 + " " +
                  parCons.nombreInd2);
      tm.SetVisible("CRITERIO4", true);
    }
    else { //E
      tm.SetVisible("CRITERIO4", false);
    }

    if (!parCons.ind3.equals("")) { //E
      tm.SetLabel("LAB015", res.getString("msg39.Text") + parCons.ind3);
      tm.SetLabel("CRITERIO5",
                  res.getString("msg39.Text") + parCons.ind3 + " " +
                  parCons.nombreInd3);
      tm.SetVisible("CRITERIO5", true);
    }
    else { //E
      tm.SetVisible("CRITERIO5", false);
    }

    if (parCons.tipoPres.equals("g")) {
      tm.SetLabel("LAB016", res.getString("msg40.Text"));
      tm.SetVisible("GH_00", false);
      tm.SetVisible("DTL_00", false);
      tm.SetVisible("GF_00", true);
    }
    if (parCons.tipoPres.equals("n")) {
      tm.SetLabel("LAB016", res.getString("msg41.Text"));
      tm.SetVisible("GH_00", true);
      tm.SetVisible("DTL_00", true);
      tm.SetVisible("GF_00", false);

      if (unIndicador) {
        //E System_out.println("informe con un solo indicador");
        tm.SetLabel("LAB010", parCons.ind1);
        tm.SetVisible("LAB010", true);
        tm.SetVisible("LIN006", true);
        tm.SetVisible("FIE003", true);

        tm.SetLabel("LAB011", res.getString("msg42.Text"));
        tm.SetVisible("LAB011", true);
        tm.SetVisible("LIN005", true);
        tm.SetVisible("FIE005", true);
        tm.SetVisible("FIE001", false);

        tm.SetLabel("LAB017", res.getString("msg43.Text"));
        tm.SetVisible("LAB017", true);
        tm.SetVisible("LIN007", true);
        tm.SetVisible("FIE006", true);
        tm.SetVisible("FIE004", false);

      }
      else {
        //E System_out.println("informe con varios indicadores");
        tm.SetVisible("FIE005", false);
        tm.SetVisible("FIE006", false);

        tm.SetLabel("LAB010", parCons.ind1);
        tm.SetVisible("FIE003", true);

        tm.SetLabel("LAB011", parCons.ind2);
        tm.SetVisible("FIE001", true);

        if ( (parCons.ind3.length() > 0)) {
          // tres indicadores
          tm.SetVisible("LAB017", true);
          tm.SetVisible("LIN007", true);
          tm.SetVisible("FIE004", true);
          tm.SetLabel("LAB017", parCons.ind3);
        }
        else {
          tm.SetVisible("LAB017", false);
          tm.SetVisible("LIN007", false);
          tm.SetVisible("FIE004", false);
        }
      }
    }
  }

  public void MostrarGrafica() {}

}
