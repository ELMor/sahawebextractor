//Title:        Consulta de alarmas automaticas de un periodo
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:

package consAlaAutoPer;

import java.io.Serializable;

public class DataConsIndAuto
    implements Serializable {

  public String SEMANA = "";
  public int NM_CASOS = 0;
  public float UMBRAL_INF = 0;
  public float UMBRAL_SUP = 0;
  public float NM_VALOR1 = 0;
  public float NM_VALOR2 = 0;
  public float NM_VALOR3 = 0;

  public DataConsIndAuto(String sem, int casos, int inds,
                         float inf, float sup) {
    if (inds == 1) {
      SEMANA = sem;
      NM_CASOS = casos;
      UMBRAL_INF = inf;
      UMBRAL_SUP = sup;
    }
    else {
      SEMANA = sem;
      NM_CASOS = casos;
      NM_VALOR1 = inf;
      NM_VALOR2 = sup;
    }
  }

  public DataConsIndAuto(String sem, int casos,
                         float v1, float v2, float v3) {
    SEMANA = sem;
    NM_CASOS = casos;
    NM_VALOR1 = v1;
    NM_VALOR2 = v2;
    NM_VALOR3 = v3;
  }

  public DataConsIndAuto(String sem, int casos, float valor, int inds,
                         float inf, float sup) {

    SEMANA = sem;
    NM_CASOS = casos;
    NM_VALOR1 = valor;
    UMBRAL_INF = inf;
    UMBRAL_SUP = sup;

  }

}