
package enfermo;

//______________________________________________ IMPORT
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Vector;

/** esta clase contiene los datos de la pantalla de enfermo
 *  sirve para intercambiar con el servlet
 */
public class DataEnfermo
    extends Data
    implements Serializable {

  /** indica si se quieren ver los dados de baja */
  protected boolean mostarBajas = false;

  public DataEnfermo(Object a_campo_filtro) {
    super(a_campo_filtro);
  }

  /** esta funci�n devuelve el tipo de dato del servlet */
  protected Object getNewData() {
    return new DataEnfermo(campo_filtro);
  }

  /** nos devuelve la fecha de ancimiento */
  public String getFechaNacimiento() {
    //return fc_nac;
    Object dd = get("FC_NAC");
    if (dd != null) {
      if (dd.getClass() == (new String()).getClass() ||
          dd.getClass() == (new StringBuffer()).getClass()) {
        return dd.toString();
      }
      else {
        return Fechas.date2String( (java.util.Date) dd);
      }
    }
    else {
      return " ";
    }

  }

  public void setFechaNacimiento(String a_date) {
    //java.util.Date date = Fechas.string2Date(a_date);

    if (a_date != null && a_date.trim().length() > 0) {
      // se le suma un dia porque serializando pierde uno
      put("FC_NAC", new StringBuffer(a_date)); // + Fechas.mili_dia));
    }
    else {
      put("FC_NAC", new String(""));
    }
  }

  /** trata la fecha de baja*/
  public String getFechaBaja() {
    //return fc_baja;
    Object dd = get("FC_BAJA");
    if (dd != null) {
      if (dd.getClass() == (new String()).getClass() ||
          dd.getClass() == (new StringBuffer()).getClass()) {
        return dd.toString();
      }
      else {
        return Fechas.date2String( (java.util.Date) dd);
      }
    }
    else {
      return " ";
    }
  }

  public void setFechaBaja(String a_date) {
    //java.util.Date date = Fechas.string2Date(a_date);
    if (a_date != null && a_date.trim().length() > 0) {
      // se le suma un dia porque serializando pierde uno
      //put("FC_BAJA", new java.sql.Date(date.getTime() + Fechas.mili_dia));
      put("FC_BAJA", new StringBuffer(a_date));
    }
    else {
      //// System_out.println("la fecha de baja es nULL");
      put("FC_BAJA", new String(""));
    }
  }

  /** trata la fecha de baja*/
  public String getFechaActual() {
    //return fc_actual;
    Object dd = get("FC_ULTACT");
    if (dd != null) {
      if (dd.getClass() == (new String()).getClass() ||
          dd.getClass() == (new StringBuffer()).getClass()) {

//            // System_out.println("en getFechaActual " + dd.toString());

        return dd.toString();
      }
      else {
        SimpleDateFormat formato3 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss",
            new Locale("es", "ES"));
        String strDate = "";
        try {
          strDate = formato3.format( (java.util.Date) dd);
        }
        catch (Exception e) {
          e.printStackTrace();
        } //// System_out.println("Excepcion en el formato3 raro");}

        //// System_out.println("en getFechaActual " + strDate);

        return strDate;
        //return  Fechas.date2String((java.util.Date) dd);
      }
    }
    else {
      //// System_out.println("en getFechaActual la fecha es NULA" );
      return " ";
    }
  }

  public void setFechaActual(String a_date) {
    //java.util.Date date = Fechas.string2Date(a_date);
    if (a_date != null && a_date.trim().length() > 0) {
      // se le suma un dia porque serializando pierde uno
      put("FC_ULTACT", new StringBuffer(a_date));
    }
    else {
      put("FC_ULTACT", new String(""));
    }
  }

///////  es solo una prueba
  public int getCD_ENFERMO() {
    Integer in = (Integer) get("CD_ENFERMO");
    if (in != null) {
      return in.intValue();
    }
    else {
      return 0;
    }
  }

  public String getDS_NOMBRE() {
    return (String) get("DS_NOMBRE");
  }

  public String getDS_APE1() {
    return (String) get("DS_APE1");
  }

  public String getDS_APE2() {
    return (String) get("DS_APE2");
  }

  public String getSIGLAS() {
    return (String) get("SIGLAS");
  }

  public String getDS_NOMBREENTERO() {
    StringBuffer strResult = new StringBuffer();
    Object o = null;

    o = get("DS_APE1");
    if (o != null) {
      strResult.append(o.toString());
    }
    o = get("DS_APE2");
    if (o != null) {
      strResult.append(o.toString());
    }
    o = get("DS_NOMBRE");
    if (o != null) {
      strResult.append(o.toString());
    }

    return strResult.toString();
  }

  public String getDS_FONO() {
    StringBuffer strResult = new StringBuffer();
    Object o = null;

    o = get("DS_FONOAPE1");
    if (o != null) {
      strResult.append(o.toString());
    }
    o = get("DS_FONONOMBRE");
    if (o != null) {
      strResult.append(o.toString());
    }

    return strResult.toString();
  }

  public String getCD_MUN() {
    return (String) get("CD_MUN");
  }

  public String getDS_DIRECCION() {
    StringBuffer strResult = new StringBuffer();
    Object o = null;

    o = get("DS_DIREC");
    if (o != null) {
      strResult.append(o.toString());
    }
    o = get("DS_NUM");
    if (o != null) {
      strResult.append(o.toString());
    }

    return strResult.toString();
  }

  public void setMostrarBajas(boolean a_dat) {
    mostarBajas = a_dat;
  }

  public boolean getMostrarBajas() {
    return mostarBajas;
  }

  public Vector getFieldVector(boolean permiso) {
    Vector retval = new Vector();

    retval.addElement("DS_FONO  = getDS_FONO");
    retval.addElement("CD_ENFERMO  = getCD_ENFERMO");

    if (permiso) {
      retval.addElement("DS_NOMBREENTERO  = getDS_NOMBREENTERO");
    }
    else {
      retval.addElement("DS_NOMBREENTERO  = getgetSIGLAS");
    }

    retval.addElement("FC_NAC  = getFechaNacimiento");
    retval.addElement("CD_MUN  = getCD_MUN");
    retval.addElement("DS_DIRECCION  = getDS_DIRECCION");

    return retval;
  }

} //________________________________________________  END_CLASS
