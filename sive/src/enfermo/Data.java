
package enfermo;

//______________________________________________ IMPORT
import java.io.Serializable;
import java.util.Hashtable;

/** esta clase contiene los datos de la pantalla de enfermo
 *  sirve para intercambiar con el servlet
 */
public class Data
    extends Hashtable
    implements Serializable {

  /** es el campo para filtrar */
  protected String campo_filtro = null;

  /** es el dato para filtrar */
  protected String filtro = null;

  //Se indica a qu� campo de la b. datos se refiere el objeto indicado por el usuario
  public Data(Object a_campo_filtro) {
    super();
    campo_filtro = (String) a_campo_filtro;
  }

  //Se pone el filtro indicado por el usuario
  public void setFiltro(Object a_filtro) {
    filtro = (String) a_filtro;
  }

  public Object getFiltro() {
    return filtro;
  }

  public void setCampoFiltro(Object a_filtro) {
    campo_filtro = (String) a_filtro;
  }

  public Object getCampoFiltro() {
    return campo_filtro;
  }

  /** esta funci�n devuelve el tipo de dato del servlet */
  protected Object getNewData() {
    return new Data(campo_filtro);
  }

} //________________________________________________  END_CLASS
