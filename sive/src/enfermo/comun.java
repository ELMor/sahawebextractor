
package enfermo;

import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import java.awt.Choice;
import java.awt.Color;

import capp.CApp;
import capp.CLista;
import capp.CMessage;
import sapp.StubSrvBD;

public class comun {

  //constantes del panel
  public static final String imgLUPA = "images/Magnify.gif";
  public static final String imgALTA = "images/alta.gif";
  public static final String imgMODIFICACION = "images/modificacion.gif";
  public static final String imgBORRAR = "images/baja.gif";
  public static final String imgCANCELAR = "images/cancelar.gif";
  public static final String imgBUSCAR = "images/search1.gif";
  public static final String imgACEPTAR = "images/aceptar.gif";
  public static final String imgLIMPIAR = "images/vaciar.gif"; //limpiar
  public static final String imgALTA2 = "images/alta2.gif";
  public static final String imgMODIFICAR2 = "images/modificacion2.gif";
  public static final String imgPRIMERO = "images/primero.gif";
  public static final String imgANTERIOR = "images/anterior.gif";
  public static final String imgSIGUIENTE = "images/siguiente.gif";
  public static final String imgULTIMO = "images/ultimo.gif";
  public static final String imgACTUALIZAR = "images/actualizar.gif";
  public static final String imgREFRESCAR = "images/refrescar.gif";
  public static final String imgGENERAR = "images/grafico.gif";

  public static final String strSERVLET_CAT = "servlet/SrvCat";
  public static final String strSERVLET_ENFERMO = "servlet/SrvEnfermo";
  public static final String strSERVLET_NIV2 = "servlet/SrvZBS2";
  public static final String strSERVLET_GENERAL = "servlet/SrvGeneral";
  public static final String strSERVLET_GEN = "servlet/SrvGen";

  public static Color amarillo = new Color(255, 255, 150);

  public comun() {
  }

  /**
   *  esta funci�n a�ade los datos de una CLista a una list
   *  @param app  es el applet para mostrar los mensajes
   *  @param lst  es el List que mostrar� los datos
   *  @param lista es la CLista que contiene los datos a mostrar
   *  @param name_campo es el campo dela base de datos que quermos mostrar
   *  @param cancel es un booleano que indica si se quiere dejar una opci�n en blanco
   */
  protected static boolean writeLista(CApp app, java.awt.List lst, CLista lista,
                                      String name_campo, boolean cancel) {
    CMessage msgBox;
    Hashtable data = null;

    lst.removeAll();

    // opci�n cancelar
    if (cancel) {
      lst.add("      ");
    }

    if (lista.size() > 0) {

      // vuelca la lista
      for (int j = 0; j < lista.size(); ++j) {
        data = (Hashtable) lista.elementAt(j);
        lst.add( (String) data.get(name_campo));
      }

      // opci�n m�s datos
      if (lista.getState() == CLista.listaINCOMPLETA) {
        lst.add("M�s ...");

        // mensaje de lista vacia
      }
    }
    else {
      msgBox = new CMessage(app, CMessage.msgAVISO, "No se encontraron datos.");
      msgBox.show();
      msgBox = null;
      return false;
    }

    return true;
  }

  /**
   *  esta funci�n vacia coge los datosde la primera lista
   * y rellena con ellos ls segunda, borrando los que tenga
   */
  public static void suplantaLista(CLista a_lista, CLista a_appList) {
    // quitamos todos los elementos de ls segunda lista
    a_appList.removeAllElements();
    Enumeration enum = a_lista.elements();
    while (enum.hasMoreElements()) {
      a_appList.addElement(enum.nextElement());
    }

    a_appList.setState(a_lista.getState());
  }

  /**
   *  esta funci�n va a buscar una  CLista de un servlet
   * @param app es el applet, se usa para mostrar pantallas
   * @param stubCliente es el stub que see usa para traer datos
   * @param a_strServlet  es el nombre del servlet
       * @param a_modo  es un entero que le indica al servlet lo que tiene que hacer
   * @param a_data  es el dato que sirve de  filtro
   */
  public static CLista traerDatos(CApp app, StubSrvBD stubCliente,
                                  String a_strServlet, int a_modo,
                                  CLista a_data) {
    CLista lista = null;
    try {
      URL u = new URL(app.getURL() + a_strServlet);
      stubCliente.setUrl(u);

      a_data.setIdioma(app.getIdioma());

      // ARG: Se introducen el login y la lortad
      a_data.setLogin(app.getLogin());
      a_data.setLortad(app.getParameter("LORTAD"));

      lista = (CLista) stubCliente.doPost(a_modo, a_data);

      /*
             SrvEnfermo srv = new SrvEnfermo();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                    "jdbc:oracle:thin:@192.168.0.13:1521:ICM",
                                    "dba_edo",
                                    "manager");
             lista = srv.doDebug(a_modo, a_data);
       */

      return lista;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      CMessage msgBox = new CMessage(app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
      return null;
    }
  }

  /**
   *  esta funci�n va a buscar la desripcion de un c�digo
   * @param app es el applet, se usa para mostrar pantallas
   * @param stubCliente es el stub que see usa para traer datos
   * @param a_strServlet  es el nombre del servlet
       * @param a_modo  es un entero que le indica al servlet lo que tiene que hacer
   * @param a_cod  es un String que indica el campo del codigo
   * @param a_des  es un string que indica el campo de la descripcion
   * @param a_data es el valor por el que se filtra
   * @return  devuelve la descripci�n del campo
   */
  public static String traerDescripcion(CApp app, StubSrvBD stubCliente,
                                        String a_strServlet, int a_modo,
                                        String a_cod, String a_des,
                                        String a_data) {
    CLista lista = null;
    // rellenamos la CLista que vamso amandar
    if (a_data == null || a_data.trim().length() == 0) {
      // no hay con que filtrar
      return "";
    }

    CLista data = new CLista();
    data.setIdioma(app.getIdioma());
    DataEnfermo dEnfer = null;
    dEnfer = new DataEnfermo(a_cod);
    dEnfer.put(a_cod, a_data);
    data.addElement(dEnfer);

    try {
      URL u = new URL(app.getURL() + a_strServlet);
      stubCliente.setUrl(u);

      lista = (CLista) stubCliente.doPost(a_modo, data);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      CMessage msgBox = new CMessage(app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
      return "";
    }

    // recogemos le valor pedido
    if (lista == null || lista.isEmpty()) {
      // no ha traido ning�n dato
      return "";
    }
    Hashtable hash = (Hashtable) lista.firstElement();
    String result = (String) hash.get(a_des);
    if (result == null) {
      return "";
    }
    else {
      return result;
    }
  }

  /**
   *  Escribimos todos los datos del choice que nos pasan
   *  se le a�ade un choice vacio que es el que se selecciona
   *  @param a_choice ,  objeto choice que hay que rellenar
   *  @param a_lista ,   lista que contiene los datos que hay que rellenar
   *  @ param a_null,  true indica que se a�ada como primer elemento vacio y se seleccione
   */
  public static void writeChoice(CApp app, Choice a_choice, CLista a_lista,
                                 boolean a_null, String name_campo) {
    String dato = null;
    // rellena los tipos de preguntas
    // insertamos la opci�n vacia y se selecciona esta
    if (a_null) {
      a_choice.add("  ");
      a_choice.select(0);
    }
    if (a_lista != null) {
      // insertamos le resto de la lista
      for (int j = 0; j < a_lista.size(); j++) {
        dato = (String) ( (Hashtable) a_lista.elementAt(j)).get(name_campo);
        if (dato != null) {
          a_choice.add(dato);
        }
      }
    }
  }

  /**
   *  esta funci�n extrae un campo concreto de la lista de hashtable
   *  con un n�mero indicado
   */
  public Object leerCampo(CLista a_lista, int a_indice, String a_campo) {
    Hashtable h = (Hashtable) a_lista.elementAt(a_indice);
    return h.get(a_campo);
  }

  /**
   *  esta funci�n busca dentro de lso elementos de la hashtable
   *  y devuelve el c�digo
   *  @param a_hash lista que contieen los datos
   *  @param a_campo es el dato delk que debemos buscar la clave
   */
  public static Object buscaCodCampo(Hashtable a_has, String a_campo) {
    String clave = null, elto = null;

    for (Enumeration e = a_has.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();
      elto = (String) a_has.get(clave);
      if (elto != null && elto.equals(a_campo)) {
        return clave;
      }
    }

    return null;
  }

  /**
   *  esta funcion recibe una CLista y un codigo
   *  y devuelve la posici�n dentro de la CLista en la que est� ese c�digo
   * @param a_lista es la lista dentro de la que hay que buscar
   * @param campo es el nombre del campo en el que se debe de buscar
   * @param a_dato es el dato que se debe de buscar
   * @return -1 si no lo encuentra, el n�mero de orden si lo encuentra
   */
  public static int buscaDato(CLista a_lista, String campo, String a_dato) {
    Hashtable hash = null;
    String dato = null;
    boolean encontrado = false;

    if (a_lista == null || a_dato == null || campo == null) {
      // devolvemos elerro no existe
      return -1;
    }

    int i = 0;
    for (Enumeration e = a_lista.elements(); (!encontrado) && e.hasMoreElements(); ) {
      hash = (Hashtable) e.nextElement();
      dato = (String) hash.get(campo);
      if (dato != null && dato.equalsIgnoreCase(a_dato)) {
        encontrado = true;
      }
      else {
        i++;
      }
    }

    if (encontrado) {
      return i;
    }
    else {
      return -1;
    }
  }

  /**
   *  esta funci�n hace la transcripci�n  fon�tica de las palabras
   *  elimina las vocales, pone todo a may�sculas,
   *  y ciertos fonemas son sustituidos
   *  el if que mira si son letras distintas es la funci�n iguales
   * @param a_data es la cadena a traducir fon�ticamente
   * @result es una cadena de longitud 4
   */
  public static String traduccionFonetica(String a_data) {
    StringBuffer result = new StringBuffer();
    String data_in = a_data.toUpperCase();
    char siguienteLetra, actualLetra, anteriorLetra;
    int longitud = a_data.length();
    char car;
    // el n�mero m�ximo de un campo fonetico es 6

    if (a_data == null) {
      return "    ";
    }

    actualLetra = 'A';
    for (int i = 0; i < longitud; i++) {
      anteriorLetra = actualLetra;
      if (i + 1 == longitud) {
        siguienteLetra = '\0';
      }
      else {
        siguienteLetra = data_in.charAt(i + 1);
      }
      actualLetra = data_in.charAt(i);
      switch (actualLetra) {
        case 'A': // nos saltamos la letra
        case '�': // nos saltamos la letra
          break;
        case 'E': // nos saltamos la letra
        case '�': // nos saltamos la letra
          break;
        case 'I': // nos saltamos la letra
        case '�': // nos saltamos la letra
          break;
        case 'O': // nos saltamos la letra
        case '�': // nos saltamos la letra
          break;
        case 'U': // nos saltamos la letra
        case '�': // nos saltamos la letra
          break;
        case ' ': // nos saltamos la letra
          break;
        case '-': // nos saltamos la letra
          break;
        case '\\': // nos saltamos la letra
          break;
        case 'H': // nos saltamos la letra
          break;
        case 'B': // se transforma en V
        case 'W': // se transforma en V
          if (actualLetra != siguienteLetra || longitud == 2) {
            result.append('V');
          }
          break;
        case 'Q': // se transforma en K
          result.append('K');
          break;
        case 'C':
          switch (siguienteLetra) {
            case 'A':
            case '�':
            case 'O':
            case '�':
            case 'U':
            case '�':
              result.append('K');
              break;
            case 'E':
            case '�':
            case 'I':
            case '�':
              if (actualLetra != siguienteLetra || longitud == 2) {
                result.append('Z');
              }
              break;
            case 'H':
              if (actualLetra != siguienteLetra || longitud == 2) {
                result.append('C');
              }
              break;
            default:
              if (actualLetra != siguienteLetra || longitud == 2) {
                result.append('C');
              }
          }
          break;
        case 'G':
          if (siguienteLetra == '�' || siguienteLetra == '�' ||
              siguienteLetra == 'E' || siguienteLetra == 'I') {
            if (actualLetra != siguienteLetra || longitud == 2) {
              result.append('J');
            }
          }
          else {
            if (actualLetra != siguienteLetra || longitud == 2) {
              result.append('G');
            }
          }
          break;
        case 'L':
          if (siguienteLetra == 'L' && i + 2 < data_in.length()) {
            if (actualLetra != siguienteLetra || longitud == 2) {
              result.append('Y');
            }
          }
          else {
            if (actualLetra != siguienteLetra || longitud == 2) {
              result.append('L');
            }
          }
          break;
        case 'Y':

          // si no hay ninguna vocal cerca se quita por entenderse que es una vocal
          if ( (siguienteLetra != 'A' && siguienteLetra != '�' &&
                siguienteLetra != 'E' && siguienteLetra != '�' &&
                siguienteLetra != 'I' && siguienteLetra != '�' &&
                siguienteLetra != 'O' && siguienteLetra != '�' &&
                siguienteLetra != 'U' && siguienteLetra != '�') ||
              (anteriorLetra != 'A' && anteriorLetra != '�' &&
               anteriorLetra != 'E' && anteriorLetra != '�' &&
               anteriorLetra != 'I' && anteriorLetra != '�' &&
               anteriorLetra != 'O' && anteriorLetra != '�' &&
               anteriorLetra != 'U' && anteriorLetra != '�')) {
            if ( (i != 0) ||
                ( (! (i > 0)) && siguienteLetra != 'A' && siguienteLetra != 'E' &&
                 siguienteLetra != 'I' && siguienteLetra != 'O' &&
                 siguienteLetra != 'U')) {
              break;
            }
          }
          if ( (siguienteLetra == ' ' || i + 1 == data_in.length()) &&
              (anteriorLetra != 'A' || anteriorLetra != '�' ||
               anteriorLetra != 'E' || anteriorLetra != '�' ||
               anteriorLetra != 'I' || anteriorLetra != '�' ||
               anteriorLetra != 'O' || anteriorLetra != '�' ||
               anteriorLetra != 'U' || anteriorLetra != '�')) {
            break;
          }
          if (actualLetra != siguienteLetra || longitud == 2) {
            result.append('Y');
          }
          break;
        default:
          if (actualLetra != siguienteLetra || actualLetra == 'K' ||
              longitud == 2) {
            result.append(actualLetra);
          }
          break;
      }
    } // rof

    /// comprobamos quela longitud sea 4
    for (int i = result.length(); i < 4; i++) {
      result.append(" ");
    }
    result.setLength(4);
    return result.toString();
  }

  /** indica si los datos son disitnto o iguales */
  public static boolean ha_cambiado(String a_dato, String datoInicial) {
    //Si antes dato era null(no hab�a dato), ha cambiado si ahora hay dato (de long no vac�a)
    if (datoInicial == null) {
      return a_dato.trim().length() != 0;
    }

    return!a_dato.equals(datoInicial.trim());
  }

} //_______________________________________________________ END CLASS
