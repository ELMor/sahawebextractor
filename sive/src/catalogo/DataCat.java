package catalogo;

import java.io.Serializable;

public class DataCat
    implements Serializable {
  protected String sCod = "";
  protected String sDes = "";
  protected String sDesL = "";

  // constructor vacio
  public DataCat() {
  }

  // constructor con c�digo
  public DataCat(String cod) {
    sCod = cod;
  }

  // constructor completo
  public DataCat(String cod, String des, String desL) {
    sCod = cod;
    sDes = des;
    if (desL != null) {
      sDesL = desL;
    }
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getDesL() {
    String s;

    s = sDesL;
    if (s == null) {
      s = "";
    }
    return s;
  }
}
