package catalogo;

import java.net.URL;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import comun.constantes;
import sapp.StubSrvBD;

public class PanelCat
    extends CDialog {

  // modos de operaci�n de la ventana
  final static int modoALTA = 0;
  final static int modoMODIFICAR = 1;
  final static int modoBAJA = 2;
  final static int modoESPERA = 5;

  public boolean valido = false;

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;

  // constantes del panel
//  final String strSERVLET = "servlet/SrvCat";
  final String strSERVLET = constantes.strSERVLET_CAT;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // par�metros
  protected int modoOperacion;
  public boolean bAceptar = false;
  protected int iCatalogo;
  protected String sTitulo;
  protected StubSrvBD stubCliente;
  protected CCargadorImagen imgs = null;

  //longitudes m�ximas de los campos. Se obtienen del applet
  int iMaxCod;
  int iMaxDes;
  // controles
  XYLayout xyLyt = new XYLayout();
  Panel pnl = new Panel();
  Label lbl1 = new Label();
  CCampoCodigo txtCod = new CCampoCodigo();
  Label lbl2 = new Label();
  TextField txtDes = new TextField();
  Label lbl3 = new Label();
  TextField txtDesL = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  catActionListener btnActionListener = new catActionListener(this);
  FlowLayout flowLayout1 = new FlowLayout();

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:

        // modo alta
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(true);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        // modo modificaci�n y baja
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(false);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // contructor
  public PanelCat(CApp a, int modo, int cat, String s) {
    super(a);

    try {
      iMaxCod = ( (Catalogo) a).iMaxCod;
      iMaxDes = ( (Catalogo) a).iMaxDes;
      sTitulo = s;
      iCatalogo = cat;
      modoOperacion = modo;
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setLayout(flowLayout1);
    xyLyt.setHeight(157);
    xyLyt.setWidth(384);
    this.setSize(384, 180);
    pnl.setLayout(xyLyt);
    lbl1.setText("C�digo:");
    txtCod.setBackground(new Color(255, 255, 150));
    lbl2.setText("Descripci�n:");
    txtDes.setBackground(new Color(255, 255, 150));
    btnAceptar.setLabel("Aceptar");
    btnCancelar.setLabel("Cancelar");
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    pnl.add(lbl1, new XYConstraints(2, 1, 94, 25));
    pnl.add(txtCod, new XYConstraints(96, 1, 77, -1));
    pnl.add(lbl2, new XYConstraints(2, 38, -1, -1));
    pnl.add(txtDes, new XYConstraints(96, 40, 275, -1));
    pnl.add(lbl3, new XYConstraints(2, 80, 143, -1));
    pnl.add(txtDesL, new XYConstraints(172, 80, 201, -1));
    pnl.add(btnAceptar, new XYConstraints(196, 122, 80, 26));
    pnl.add(btnCancelar, new XYConstraints(294, 122, 80, 26));
    this.add(pnl);

    setTitle(sTitulo);

    // establece los escuchadores
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDesL.setVisible(false);
      lbl3.setVisible(false);
    }
    else {
      lbl3.setText("Descripci�n (" + app.getIdiomaLocal() + "):");
    }

    // establece el modo de operaci�n
    Inicializar();
  }

  // procesa opci�n a�adir al cat�logo
  public void Anadir() {
    CMessage msgBox = null;
    CLista data = null;

    // comprueba la validad de los datos
    if (this.isDataValid()) {
      try {
        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara los datos y los envia
        data = new CLista();
        data.addElement(new DataCat(txtCod.getText(), txtDes.getText(),
                                    txtDesL.getText()));
        this.stubCliente.doPost(servletALTA + iCatalogo, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoALTA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  }

  // procesa opci�n modificar
  public void Modificar() {
    CMessage msgBox = null;
    CLista data = null;

    // comprueba la validad de los datos
    if (this.isDataValid()) {
      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara y envia los datos
        data = new CLista();
        data.addElement(new DataCat(txtCod.getText(), txtDes.getText(),
                                    txtDesL.getText()));
        this.stubCliente.doPost(servletMODIFICAR + iCatalogo, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoMODIFICAR;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  }

  // procesa opci�n borrar
  public void Borrar() {
    CMessage msgBox = null;
    CLista data = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA,
                          "�Est� seguro de que desea eliminar este registro?");
    msgBox.show();

    if (msgBox.getResponse()) {

      msgBox = null;
      // borra el item del cat�logo
      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataCat(txtCod.getText()));
        this.stubCliente.doPost(servletBAJA + iCatalogo, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoBAJA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
    else {
      msgBox = null;
    }
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = "";

    boolean b = false;

    // comprueba que esten informados los campos obligatorios
    if ( (txtCod.getText().length() > 0) && (txtDes.getText().length() > 0)) {
      b = true;

      // comprueba la longitud m�xima de los campos
      if (txtCod.getText().length() > iMaxCod) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtCod.selectAll();
      }

      if (txtDes.getText().length() > iMaxDes) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtDes.selectAll();
      }

      if (txtDesL.getText().length() > iMaxDes) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtDesL.selectAll();
      }

      // advertencia de requerir datos
    }
    else {
      msg = "Faltan campos obligatorios.";
    }

    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    bAceptar = b;
    return b;
  }
}

// action listener
class catActionListener
    implements ActionListener, Runnable {
  PanelCat adaptee = null;
  ActionEvent e = null;

  public catActionListener(PanelCat adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case PanelCat.modoALTA:
          adaptee.Anadir();
          break;
        case PanelCat.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case PanelCat.modoBAJA:
          adaptee.Borrar();
          break;
      }

    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }
  }
}
