package catalogo;

import java.util.ResourceBundle;

import capp.CApp;
import comun.constantes;

public class Catalogo
    extends CApp {

  // catalogos
  public static final int catCCAA = 10;
  ResourceBundle res;
  public static final int catEDO = 20;
  public static final int catTLINEA = 30;
  public static final int catTPREGUNTA = 40;
  public static final int catTSIVE = 50;
  public static final int catTVIGILANCIA = 60;
  public static final int catMOTBAJA = 70;
  public static final int catNIVASISTENCIAL = 80;
  public static final int catPROCESOS = 90;
  public static final int catSEXO = 100;
  public static final int catTASISTENCIA = 110;
  public static final int catESPECIALIDAD = 120;
  public static final int catNIVEL1 = 130;
  public static final int catNIVEL2 = 140;
  public static final int catPREGUNTA = 150;
  public static final int catLISTAS = 160;
  public static final int catINDICADOR = 170;
  public static final int catCLASIFDIAG = 180;

  public static final int catGBROTE = 190;
  public static final int catTNOTIF = 200;
  public static final int catMTRANSMISION = 210;
  public static final int catTCOLECTIVO = 220;
  public static final int catSALERTA = 230;

  // Para Tuberculosis
  public static final int catFUENTES = constantes.CATALOGO_FUENTES;
  public static final int catMOTIVOS_SALIDA = constantes.
      CATALOGO_MOTIVOS_SALIDA;
  public static final int catMOTIVOS_INICIO = constantes.
      CATALOGO_MOTIVOS_INICIO;
  public static final int catMUESTRAS = constantes.CATALOGO_MUESTRAS;
  public static final int catTECNICAS = constantes.CATALOGO_TECNICAS;
  public static final int catVALORES_MUESTRAS = constantes.
      CATALOGO_VALORES_MUESTRAS;
  public static final int catMICOBACTERIAS = constantes.CATALOGO_MICOBACTERIAS;
  public static final int catESTUDIOS_RESISTENCIAS = constantes.
      CATALOGO_ESTUDIOS_RESISTENCIAS;

  //Para m�dicos centinelas
  public static final int catCONGLOM = 300;
  public static final int catMASISTENCIA = 310;
  public static final int catTPCENTINELA = 320;
  public static final int catINDACTMC = 330;

  // modos de operaci�n del servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // constantes del panel
//  final String strSERVLET = "servlet/SrvCat";
  final String strSERVLET = constantes.strSERVLET_CAT;

  // cat�logo
  int iCatalogo;
  String sTituloPanel = "";

  // parametros
  int iMaxCod;
  int iMaxDes;
  String sLabel;
  String[] labels = new String[4];
  String sEtiqueta;
  String[] etiquetas = new String[4];

  int maxLongDes; //Longitud m�x del campo descripci�n

  public void init() {
    super.init();

    // cat�logo
    iCatalogo = Integer.parseInt(getParameter("catalogo", null));

    res = ResourceBundle.getBundle("catalogo.Res" + getIdioma());

    switch (iCatalogo) {
      case Catalogo.catCCAA:
        sEtiqueta = res.getString("msg.1");
        sLabel = res.getString("msg.2");
        sTituloPanel = res.getString("msg.3");

        iMaxCod = 2;
        iMaxDes = 50;
        break;
      case Catalogo.catEDO:
        sEtiqueta = res.getString("msg.4");
        sLabel = res.getString("msg.5");
        sTituloPanel = res.getString("msg.6");

        iMaxCod = 3;
        iMaxDes = 40;
        break;
      case Catalogo.catTLINEA:
        sEtiqueta = res.getString("msg.7");
        sLabel = res.getString("msg.8");
        sTituloPanel = res.getString("msg.9");

        iMaxCod = 1;
        iMaxDes = 30;
        break;
      case Catalogo.catTPREGUNTA:
        sEtiqueta = res.getString("msg.10");
        sLabel = res.getString("msg.11");
        sTituloPanel = res.getString("msg.12");

        iMaxCod = 1;
        iMaxDes = 30;
        break;
      case Catalogo.catTSIVE:
        sEtiqueta = res.getString("msg.13");
        sLabel = res.getString("msg.14");
        sTituloPanel = res.getString("msg.15");

        iMaxCod = 1;
        iMaxDes = 30;
        break;
      case Catalogo.catTVIGILANCIA:
        sEtiqueta = res.getString("msg.16");
        sLabel = res.getString("msg.17");
        sTituloPanel = res.getString("msg.18");

        iMaxCod = 1;
        iMaxDes = 30;
        break;
      case Catalogo.catMOTBAJA:
        sEtiqueta = res.getString("msg.19");
        sLabel = res.getString("msg.20");
        sTituloPanel = res.getString("msg.21");

        iMaxCod = 2;
        iMaxDes = 30; //30 o 40???????????????????? Comentar
        break;
      case Catalogo.catNIVASISTENCIAL:
        sEtiqueta = res.getString("msg.22");
        sLabel = res.getString("msg.23");
        sTituloPanel = res.getString("msg.24");

        iMaxCod = 1;
        iMaxDes = 15;
        break;
      case Catalogo.catPROCESOS:
        sEtiqueta = res.getString("msg.25");
        sLabel = res.getString("msg.26");
        sTituloPanel = res.getString("msg.27");

        iMaxCod = 6;
        iMaxDes = 40;
        break;
      case Catalogo.catSEXO:
        sEtiqueta = res.getString("msg.28");
        sLabel = res.getString("msg.29");
        sTituloPanel = res.getString("msg.30");

        iMaxCod = 1;
        iMaxDes = 12;
        break;
        /*      case Catalogo.catTASISTENCIA:
             sEtiqueta = miTraductor.traduce(miTraductor.tMantTipoAsis,getIdioma());
             sLabel = miTraductor.traduce(miTraductor.tCodigo,getIdioma())+"\n"
             +miTraductor.traduce(miTraductor.tTipoAsis,getIdioma());
             sTituloPanel = miTraductor.traduce(miTraductor.tTipoAsis,getIdioma());
                iMaxCod = 1;
                iMaxDes = 15;
                break;
              case Catalogo.catESPECIALIDAD:
             sEtiqueta = miTraductor.traduce(miTraductor.tMantEspec,getIdioma());
             sLabel = miTraductor.traduce(miTraductor.tCodigo,getIdioma())+"\n"
                        +miTraductor.traduce(miTraductor.tEspec,getIdioma());
             sTituloPanel = miTraductor.traduce(miTraductor.tEspec,getIdioma());
                iMaxCod = 1;
                iMaxDes = 15;
      break;*/
      case Catalogo.catNIVEL1:
        sEtiqueta = res.getString("msg.31") + this.getNivel1();
        sLabel = res.getString("msg.32") + this.getNivel1();
        sTituloPanel = this.getNivel1();

        iMaxCod = 2;
        iMaxDes = 30;
        break;

      case Catalogo.catCLASIFDIAG:
        sEtiqueta = res.getString("msg.33");
        sLabel = res.getString("msg.34");
        sTituloPanel = res.getString("msg.35");

        iMaxCod = 2;
        iMaxDes = 30;
        break;

        // brotes
      case Catalogo.catGBROTE:
        sEtiqueta = res.getString("msg.36");
        sLabel = res.getString("msg.37");
        sTituloPanel = res.getString("msg.38");

        iMaxCod = 1;
        iMaxDes = 25;
        break;

      case Catalogo.catTNOTIF:
        sEtiqueta = res.getString("msg.39");
        sLabel = res.getString("msg.40");
        sTituloPanel = res.getString("msg.41");

        iMaxCod = 1;
        iMaxDes = 25;
        break;

      case Catalogo.catMTRANSMISION:
        sEtiqueta = res.getString("msg.42");
        sLabel = res.getString("msg.43");
        sTituloPanel = res.getString("msg.44");

        iMaxCod = 4;
        iMaxDes = 40;
        break;

      case Catalogo.catTCOLECTIVO:
        sEtiqueta = res.getString("msg.45");
        sLabel = res.getString("msg.46");
        sTituloPanel = res.getString("msg.47");

        iMaxCod = 2;
        iMaxDes = 40;
        break;

      case Catalogo.catSALERTA:
        sEtiqueta = res.getString("msg.48");
        sLabel = res.getString("msg.49");
        sTituloPanel = res.getString("msg.50");

        iMaxCod = 1;
        iMaxDes = 40;
        break;

        //M�dicos centinelas

      case Catalogo.catCONGLOM:
        sEtiqueta = "Mantenimiento de conglomerado";
        sLabel = "C�digo";
        sTituloPanel = "Conglomerado";

        iMaxCod = 5;
        iMaxDes = 30;
        break;

      case Catalogo.catMASISTENCIA:
        sEtiqueta = "Mantenimiento de modelos de asistencia";
        sLabel = "C�digo";
        sTituloPanel = "Modelo de asistencia";

        iMaxCod = 1;
        iMaxDes = 15;
        break;

      case Catalogo.catTPCENTINELA:
        sEtiqueta = "Mantenimiento de tipos de punto centinela";
        sLabel = "C�digo";
        sTituloPanel = "Tipo de punto centinela";

        iMaxCod = 1;
        iMaxDes = 20;
        break;

      case Catalogo.catINDACTMC:
        sEtiqueta = "Mantenimiento de �ndices de actividad asistencial";
        sLabel = "C�digo";
        sTituloPanel = "Indice de actividad asistencial";

        iMaxCod = 3;
        iMaxDes = 30;
        break;

        // Tuberculosis
      case Catalogo.catFUENTES:
        sEtiqueta = "Mantenimiento de fuentes de notificaci�n";
        sLabel = "C�digo";
        sTituloPanel = "Fuente de notificaci�n";

        iMaxCod = 1;
        iMaxDes = 30;
        break;

      case Catalogo.catMOTIVOS_SALIDA:
        sEtiqueta =
            "Mantenimiento de motivos de salida del Registro de Tuberculosis";
        sLabel = "C�digo";
        sTituloPanel = "Motivo de salida";

        iMaxCod = 2;
        iMaxDes = 40;
        break;

      case Catalogo.catMOTIVOS_INICIO:
        sEtiqueta = "Mantenimiento de motivos de inicio/fin del tratamiento";
        sLabel = "C�digo";
        sTituloPanel = "Motivo de inicio/fin";

        iMaxCod = 2;
        iMaxDes = 20;
        break;

      case Catalogo.catMUESTRAS:
        sEtiqueta = "Mantenimiento de muestras";
        sLabel = "C�digo";
        sTituloPanel = "Muestra";

        iMaxCod = 2;
        iMaxDes = 30;
        break;

      case Catalogo.catTECNICAS:
        sEtiqueta = "Mantenimiento de t�cnicas de laboratorio";
        sLabel = "C�digo";
        sTituloPanel = "T�cnica de laboratorio";

        iMaxCod = 2;
        iMaxDes = 30;
        break;

      case Catalogo.catVALORES_MUESTRAS:
        sEtiqueta = "Mantenimiento de valores de muestras";
        sLabel = "C�digo";
        sTituloPanel = "Valor de muestra";

        iMaxCod = 1;
        iMaxDes = 30;
        break;

      case Catalogo.catMICOBACTERIAS:
        sEtiqueta = "Mantenimiento de tipos de micobacterias";
        sLabel = "C�digo";
        sTituloPanel = "Tipo de micobacteria";

        iMaxCod = 1;
        iMaxDes = 30;
        break;

      case Catalogo.catESTUDIOS_RESISTENCIAS:
        sEtiqueta = "Mantenimiento de estudios de resistencias";
        sLabel = "C�digo";
        sTituloPanel = "Tipo de estudio";

        iMaxCod = 2;
        iMaxDes = 30;
        break;

    }
  }

  public void start() {
    setTitulo(sEtiqueta);
//    VerPanel("", new CListaCatalogo(this));

    //Centinelas: Constructor con el USU
    //if ( this.getTSive().equals("C") ) {
    VerPanel("", new CListaCatalogo(this, true));
    //}
    //else {
    //  VerPanel("", new CListaCatalogo(this));
    //}

  }
}
