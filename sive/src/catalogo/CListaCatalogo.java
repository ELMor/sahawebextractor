
package catalogo;

import capp.CApp;
import capp.CListaMantenimiento;
import comun.constantes;
import sapp.StubSrvBD;

public class CListaCatalogo
    extends CListaMantenimiento {

  protected int iCatalogo;
  protected String sTitulo;

  public CListaCatalogo(Catalogo a) {

    super( (CApp) a,
          2,
          "100\n462",
          a.sLabel + "\n" + a.sTituloPanel,
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO + a.iCatalogo,
          a.servletSELECCION_X_DESCRIPCION + a.iCatalogo);

    iCatalogo = a.iCatalogo;
    sTitulo = a.sTituloPanel;

    this.setBorde(false);
  }

  //Constructor que activa/desativa botones en funci�n de autorizaciones
  // del usuario
  public CListaCatalogo(Catalogo a, boolean USU) {

    //Llama a constructor con el USU
    super( (CApp) a,
          2,
          "100\n462",
          a.sLabel + "\n" + a.sTituloPanel,
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO + a.iCatalogo,
          a.servletSELECCION_X_DESCRIPCION + a.iCatalogo,
          constantes.keyBtnAnadir,
          constantes.keyBtnModificar,
          constantes.keyBtnBorrar,
          constantes.keyBtnAux);

    iCatalogo = a.iCatalogo;
    sTitulo = a.sTituloPanel;

    this.setBorde(false);
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    DataCat cat;

    modoOperacion = modoESPERA;
    Inicializar();
    PanelCat panel = new PanelCat(this.app, PanelCat.modoALTA, iCatalogo,
                                  sTitulo);
    panel.show();

    if (panel.bAceptar) {
      cat = new DataCat(panel.txtCod.getText().toUpperCase(),
                        panel.txtDes.getText(),
                        panel.txtDesL.getText());
      lista.addElement(cat);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    DataCat cat;

    modoOperacion = modoESPERA;
    Inicializar();
    PanelCat panel = new PanelCat(this.app, PanelCat.modoMODIFICAR, iCatalogo,
                                  sTitulo);

    // rellena los datos
    cat = (DataCat) lista.elementAt(i);
    panel.txtCod.setText(cat.getCod());
    panel.txtDes.setText(cat.getDes());
    panel.txtDesL.setText(cat.getDesL());

    panel.show();

    if (panel.bAceptar) {
      cat = new DataCat(panel.txtCod.getText(),
                        panel.txtDes.getText(),
                        panel.txtDesL.getText());
      lista.removeElementAt(i);
      lista.insertElementAt(cat, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {
    DataCat cat;

    modoOperacion = modoESPERA;
    Inicializar();
    PanelCat panel = new PanelCat(this.app, PanelCat.modoBAJA, iCatalogo,
                                  sTitulo);

    // rellena los datos
    cat = (DataCat) lista.elementAt(i);
    panel.txtCod.setText(cat.getCod());
    panel.txtDes.setText(cat.getDes());
    panel.txtDesL.setText(cat.getDesL());

    panel.show();

    if ( (panel.bAceptar)) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataCat(s);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataCat cat = (DataCat) o;

    return cat.getCod() + "&" + cat.getDes();
  }

}