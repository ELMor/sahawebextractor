package usu2;

import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CColumna;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPanel;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

/**
 * Panel a trav�s del que se consultan los usuarios de la aplicacion
 * @autor LSR
 * @version 1.0
 */
public class PnlUsuarios
    extends CPanel
    implements CInicializar, CFiltro {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xYLayout1 = new XYLayout();
  CCodigo txtCod = new CCodigo();
  TextField txtDes = new TextField();
  CListaMantenimiento clmMantenimiento = null;
  ButtonControl btnBuscar = new ButtonControl();
  CheckboxControl chckCod = new CheckboxControl();
  CheckboxControl chckDes = new CheckboxControl();
  CheckboxGroup chkboxGrupo = new CheckboxGroup();

  // filtro
  private Data dtFiltro = null;

  // constructor del panel PanMant
  public PnlUsuarios(CApp a) {

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {
      setApp(a);

      // etiquetas
      vLabels.addElement(new CColumna("Usuario",
                                      "80",
                                      "COD_USUARIO"));

      vLabels.addElement(new CColumna("Nombre",
                                      "230",
                                      "NOMBRE"));

      vLabels.addElement(new CColumna("Grupo",
                                      "170",
                                      "DESCRIP_GRU"));

      vLabels.addElement(new CColumna("Fecha Alta",
                                      "80",
                                      "FC_ALTA"));

      clmMantenimiento = new CListaMantenimiento(a,
                                                 vLabels,
                                                 vBotones,
                                                 this,
                                                 this,
                                                 294,
                                                 600);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel PanMant
  public void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";

    // carga la imagen
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));

    // Escuchadores de eventos
    PnlUsuariosActionAdapter actionAdapter = new PnlUsuariosActionAdapter(this);
    PnlUsuariosItemAdapter itemAdapter = new PnlUsuariosItemAdapter(this);

    chckCod.setLabel("Usuario");
    chckCod.setCheckboxGroup(chkboxGrupo);
    chckCod.addItemListener(itemAdapter);
    chckCod.setState(true);
    chckDes.setLabel("Nombre");
    chckDes.setCheckboxGroup(chkboxGrupo);
    chckDes.setState(false);
    chckDes.addItemListener(itemAdapter);

    xYLayout1.setWidth(620);
    xYLayout1.setHeight(380);

    txtCod.setName("codigo");
    txtDes.setName("descripcion");

    txtCod.setVisible(true);
    txtDes.setVisible(false);

    btnBuscar.setActionCommand("buscar");
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(actionAdapter);

    this.setLayout(xYLayout1);
    this.add(txtCod, new XYConstraints(13, 6, 312, 24));
    this.add(txtDes, new XYConstraints(13, 6, 312, 24));
    this.add(chckCod, new XYConstraints(13, 36, 130, -1));
    this.add(chckDes, new XYConstraints(150, 36, 130, -1));
    this.add(btnBuscar, new XYConstraints(510, 36, 79, 24));
    this.add(clmMantenimiento, new XYConstraints(10, 80, 600, 294));

    Inicializar(CInicializar.NORMAL);
  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  // gesti�n de los botones
  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("buscar")) {
      clmMantenimiento.setPrimeraPagina(this.primeraPagina());
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    // lista para el filtro
    Lista v = new Lista();
    Lista vFiltro = new Lista();

    Inicializar(CInicializar.ESPERA);

    try {
      QueryTool qt = new QueryTool();

      qt.putName("V_APLIC_USU_GRU");
      qt.putType("COD_USUARIO", QueryTool.STRING);
      qt.putType("NOMBRE", QueryTool.STRING);
      qt.putType("DESCRIP_GRU", QueryTool.STRING);
      qt.putType("FC_ALTA", QueryTool.DATE);

      // filtro de la aplicaci�n
      qt.putWhereType("COD_APLICACION", QueryTool.STRING);
      qt.putWhereValue("COD_APLICACION", getApp().getParameter("COD_APLICACION"));
      qt.putOperator("COD_APLICACION", "=");

      // filtro de c�digo
      if (chckCod.getState()) {
        qt.putWhereType("COD_USUARIO", QueryTool.STRING);
        qt.putWhereValue("COD_USUARIO", txtCod.getText().trim() + "%");
        qt.putOperator("COD_USUARIO", "like");
      }
      else {
        // filtro de descripci�n
        qt.putWhereType("NOMBRE", QueryTool.STRING);
        qt.putWhereValue("NOMBRE", txtDes.getText().trim() + "%");
        qt.putOperator("NOMBRE", "like");
      }

      qt.addOrderField("COD_USUARIO");

      vFiltro.addElement(qt);

      // consulta el servidor
      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      v = (Lista)this.getApp().getStub().doPost(1, vFiltro);

      if (v.size() == 0) {
        this.getApp().showAdvise(
            "No hay usuarios definidos con estos criterios");
      }

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    Inicializar(CInicializar.NORMAL);

    return v;
  }

  // solicita la siguiente trama de datos
  public Lista siguientePagina() {
    return new Lista();
  }

  // operaciones de la botonera
  public void realizaOperacion(int j) {
  }

  // cambio codigo <-> descripci�n
  void chkItemStateChanged(ItemEvent e) {
    if (e.getItem().equals("Usuario")) {
      txtCod.setVisible(true);
      txtCod.setText("");
      txtDes.setVisible(false);
    }
    else {
      txtCod.setVisible(false);
      txtDes.setVisible(true);
      txtDes.setText("");
    }
    doLayout();
  }
}

// botones de centro, almac�n y buscar
class PnlUsuariosActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PnlUsuarios adaptee;
  ActionEvent e;

  PnlUsuariosActionAdapter(PnlUsuarios adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

// control de cambio de buscar por c�digo/descripci�n
class PnlUsuariosItemAdapter
    implements java.awt.event.ItemListener {
  PnlUsuarios adaptee;

  PnlUsuariosItemAdapter(PnlUsuarios adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkItemStateChanged(e);
  }
}
