
package catalogo2;

import java.io.Serializable;

public class DataCat2
    implements Serializable {
  protected String sCod = "";
  protected String sDes = "";
  protected String sDesL = "";
  protected String sCodAux = "";
  protected String sDesAux = "";

  public DataCat2() {
  }

  public DataCat2(String cod) {
    sCod = cod;
  }

  public DataCat2(String cod, String des, String desL, String codaux,
                  String desaux) {
    sCod = cod;
    sDes = des;
    if (sDesL != null) {
      sDesL = desL;
    }
    sCodAux = codaux;
    sDesAux = desaux;
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getDesL() {
    String s;

    s = sDesL;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public String getCodAux() {
    return sCodAux;
  }

  public String getDesAux() {
    return sDesAux;
  }
}
