
package catalogo2;

import capp.CApp;
import capp.CListaMantenimiento;
import sapp.StubSrvBD;

public class CListaMantCat2
    extends CListaMantenimiento {

  protected int iCatPral;
  protected int iCatAux;
  protected String sTitulo;

  public CListaMantCat2(Catalogo2 a) {
    super( (CApp) a,
          2,
          "100\n462",
          a.res.getString("msg4.Text") + a.sLabel,
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO + a.iCatPral,
          a.servletSELECCION_X_DESCRIPCION + a.iCatPral);

    iCatPral = a.iCatPral;
    iCatAux = a.iCatAux;
    sTitulo = a.sEtiqueta;
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    DataCat2 cat;

    modoOperacion = modoESPERA;
    Inicializar();
    DialCat2 dial = new DialCat2(this.app, DialCat2.modoALTA, iCatPral, iCatAux,
                                 sTitulo);
    dial.show();

    if ( (dial.bAceptar) && (dial.valido)) {
      cat = new DataCat2(dial.txtCod.getText(),
                         dial.txtDes.getText(),
                         dial.txtDesL.getText(),
                         dial.txtCodAux.getText(),
                         dial.txtDesAux.getText());

      lista.addElement(cat);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    DataCat2 cat;

    modoOperacion = modoESPERA;
    Inicializar();
    DialCat2 dial = new DialCat2(this.app, DialCat2.modoMODIFICAR, iCatPral,
                                 iCatAux, sTitulo);

    // rellena los datos
    cat = (DataCat2) lista.elementAt(i);
    dial.txtCod.setText(cat.getCod());
    dial.txtDes.setText(cat.getDes());
    dial.txtDesL.setText(cat.getDesL());
    dial.txtCodAux.setText(cat.getCodAux());
    dial.sCodAuxBk = cat.getCodAux();
    dial.txtDesAux.setText(cat.getDesAux());

    dial.show();

    if ( (dial.bAceptar) && (dial.valido)) {
      cat = new DataCat2(dial.txtCod.getText(),
                         dial.txtDes.getText(),
                         dial.txtDesL.getText(),
                         dial.txtCodAux.getText(),
                         dial.txtDesAux.getText());
      lista.removeElementAt(i);
      lista.insertElementAt(cat, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {
    DataCat2 cat;

    modoOperacion = modoESPERA;
    Inicializar();
    DialCat2 dial = new DialCat2(this.app, DialCat2.modoBAJA, iCatPral, iCatAux,
                                 sTitulo);

    // rellena los datos
    cat = (DataCat2) lista.elementAt(i);
    dial.txtCod.setText(cat.getCod());
    dial.txtDes.setText(cat.getDes());
    dial.txtDesL.setText(cat.getDesL());
    dial.txtCodAux.setText(cat.getCodAux());
    dial.txtDesAux.setText(cat.getDesAux());

    dial.show();

    if (dial.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataCat2(s);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataCat2 cat = (DataCat2) o;

    //return cat.getCod() + "&" + cat.getDes();
    //<AIC>
    return cat.getCodAux() + "-" + cat.getCod() + "&" + cat.getDesAux() + " - " +
        cat.getDes();
    //</AIC>
  }

}