
package catalogo2;

import capp.CApp;
import capp.CListaValores;
import sapp.StubSrvBD;

public class CListaCat2
    extends CListaValores {

  public CListaCat2(CApp a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataCat2(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataCat2) o).getCod());
  }

  public String getDescripcion(Object o) {
    DataCat2 dat = (DataCat2) o;
    String sDes = dat.getDes();
    if ( (this.app.getIdioma() > 0) && (dat.getDesL().length() > 0)) {
      sDes = dat.getDesL();
    }
    return (sDes);
  }
}
