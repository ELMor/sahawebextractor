package cifrado;

public class Cifrar {

  private static int[][] K = new int[17][49];
  private static int[][] L = new int[17][33];
  private static int[][] R = new int[17][33];
  private static int paso;
  private static int[] entrada = new int[65];
  private static int[] salida = new int[65];
  private static int[] clave_vect = new int[65];
  private static int[][][] s = {
      {
      {
      14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7}
      , {
      0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8}
      , {
      4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0}
      , {
      15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}
  }
      ,

      {
      {
      15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10}
      , {
      3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5}
      , {
      0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15}
      , {
      13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}
  }
      ,

      {
      {
      10, 0, 9, 14, 6, 4, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8}
      , {
      13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1}
      , {
      13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7}
      , {
      1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}
  }
      ,

      {
      {
      7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15}
      , {
      13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9}
      , {
      10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4}
      , {
      3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}
  }
      ,

      {
      {
      2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9}
      , {
      14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6}
      , {
      4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14}
      , {
      11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 5, 4, 3}
  }
      ,

      {
      {
      12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11}
      , {
      10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8}
      , {
      9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6}
      , {
      4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}
  }
      ,

      {
      {
      4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1}
      , {
      13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6}
      , {
      1, 4, 11, 13, 12, 3, 7, 14, 10, 25, 6, 8, 0, 5, 9, 2}
      , {
      6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}
  }
      ,

      {
      {
      13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7}
      , {
      15, 13, 8, 6, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2}
      , {
      7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8}
      , {
      2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}
  }
  };

  private static int[] expansion = {
      32, 1, 2, 3, 4, 5,
      4, 5, 6, 7, 8, 9,
      8, 9, 10, 11, 12, 13,
      12, 13, 14, 15, 16, 17,
      16, 17, 18, 19, 20, 21,
      20, 21, 22, 23, 24, 25,
      24, 25, 26, 27, 28, 29,
      28, 29, 30, 31, 32, 1};

  private static int[] P = {
      16, 7, 20, 21,
      29, 12, 28, 17,
      1, 15, 23, 26,
      5, 18, 31, 10,
      2, 8, 24, 14,
      32, 27, 3, 9,
      19, 13, 30, 6,
      22, 11, 4, 25};

  public static int[] cifralo(String clave, String texto) {
    // Veamos si la longitud de la clave es 8...
    // y la ajustamos a esa longitud...

    int longitud_clave = clave.length();
    int longitud_texto = texto.length();
    int[] tec = new int[17]; // Pasamos a int en vez de char
    int mascara = 1;

    // inicio tec:

    for (int contador = 0; contador < 17; contador++) {
      tec[contador] = 0;

      /******************* OPERAMOS CON LA CLAVE *****************/

    }

    if (longitud_clave < 8) {
      for (int i = 0; i < 8 - longitud_clave; i++) {
        clave += " ";
      }
    }
    else if (longitud_clave > 8) {
      clave = clave.substring(0, 8);

// La mostramos para ver que pita...
// System_out.println("La clave introducida es: "+clave+"<");

      // Ahora reconstruimos la clave...
      // Pasamos la clave a una matriz 8x1.

    }
    for (int contador = 0; contador < 8; contador++) {
      tec[contador] = (int) clave.charAt(contador);

    }
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {

        if ( (tec[i] & mascara) == 0) {
          clave_vect[j + (8 * i)] = 0;
        }
        else {
          clave_vect[j + (8 * i)] = 1;
        }
        mascara = mascara << 1;
      }
      mascara = 1;
    }

// QUE QUEDA...

    for (int i = 0; i < 17; i++) {
      tec[i] = 0;

    }
    for (int i = 0; i < 8; i++) {
      mascara = 1;
      for (int j = 0; j < 8; j++) {
        if (clave_vect[j + (8 * i)] != 0) {
          tec[i] = tec[i] ^ mascara;
        }
        mascara = mascara << 1;
      }
    }

    generar_claves();

    /*    System_out.println("La clave reconstruida, es :");
        int valor_de_prueba;
        for (int i=0;i<17;i++) {
          valor_de_prueba= tec[i];
          System_out.print((char)valor_de_prueba);
         }
        System_out.println();*/

    /******************* OPERAMOS CON EL TEXTO *****************/

// TAMA�O = 8

    for (int contador = 0; contador < 17; contador++) {
      tec[contador] = 0;

    }
    if (longitud_texto < 8) {
      for (int k = longitud_texto; k < 8; k++) {
        texto += " ";
      }
    }
    else if (longitud_texto > 8) {
      texto = texto.substring(0, 8);

    }
    for (int contador = 0; contador < 8; contador++) {
      tec[contador] = (int) texto.charAt(contador);

// System_out.println("Texto introducido: "+texto);

// PREPARADO PARA CIFRAR....

      // Ahora reconstruimos el texto .....
    }
    for (int i = 0; i < 8; i++) {
      mascara = 1;
      for (int j = 0; j < 8; j++) {
        if ( (tec[i] & mascara) == 0) {
          entrada[j + (8 * i)] = 0;
        }
        else {
          entrada[j + (8 * i)] = 1;
        }
        mascara = mascara << 1;
      }
    }

    // CIFRARLO

    cifrado();

// QUE QUEDA...

    for (int i = 0; i < 16; i++) {
      tec[i] = 0;

    }

    for (int i = 0; i < 16; i++) {
      mascara = 1;

      for (int j = 0; j < 4; j++) {
        if (salida[j + (4 * i)] != 0) {
          tec[i] = tec[i] ^ mascara;
        }
        mascara = mascara << 1;
      }
    }

    /*     System_out.println("Texto cifrado = ");
         for (int i=0;i<16;i++) {
          valor_de_prueba = tec[i];
          System_out.print(valor_de_prueba+"<>");
         }
     System_out.println();*/
    return tec;
  }

  /***************************************************/
  /***************** FUNCIONES ***********************/
  /***************************************************/

  /* Generar el vector de claves K de 1 a 16 */

  private static void generar_claves() {
    int k, j;
    int[][] c = new int[17][28];
    int[][] d = new int[17][28];
    int[] cd = new int[57];

    int[] ciclo = {
        1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

    int[] PC_1 = {
        57, 49, 41, 33, 25, 17, 9,
        1, 58, 50, 42, 34, 26, 18,
        10, 2, 59, 51, 43, 35, 27,
        19, 11, 3, 60, 52, 44, 36,
        63, 55, 47, 39, 31, 23, 15,
        7, 62, 54, 46, 38, 30, 22,
        14, 6, 61, 53, 45, 37, 29,
        21, 13, 5, 28, 20, 12, 4};

    int[] PC_2 = {
        14, 17, 11, 24, 1, 5,
        3, 28, 15, 6, 21, 10,
        23, 19, 12, 4, 26, 8,
        16, 7, 27, 20, 13, 2,
        41, 52, 31, 37, 47, 55,
        30, 40, 51, 45, 33, 48,
        44, 49, 39, 56, 34, 53,
        46, 42, 50, 36, 29, 32};

    for (k = 0; k < 28; k++) {
      c[0][k] = clave_vect[PC_1[k] - 1];
      d[0][k] = clave_vect[PC_1[k + 28] - 1];
    }

    for (k = 1; k < 17; k++) {
      /* desplazar c*/
      for (j = 0; j < 28; j++) {
        c[k][j] = c[k - 1][ (j - 1 + ciclo[k - 1]) % 28];
        /* desplazar d*/
      }
      for (j = 0; j < 28; j++) {
        d[k][j] = d[k - 1][ (j - 1 + ciclo[k - 1]) % 28];
        /* PC2[k] */
      }
      for (j = 0; j < 28; j++) {
        cd[j] = c[k][j];
        cd[j + 28] = d[k][j];
      }
      for (j = 0; j < 48; j++) {
        K[k][j] = cd[PC_2[j] - 1];
      }
    }
  }

  /* cifrado */

  private static void cifrado() {

    permutacion_inicial(0);

    for (paso = 1; paso < 17; paso++) {
      for (int i = 0; i < 32; i++) {
        L[paso][i] = R[paso - 1][i];
//System_out.print(L[paso][i]);
//System_out.println(" "+paso);

        /******** HASTA AQU� SI FUNCIONA ********/

      }
      funcion_cifrado(paso);
    }
    permutacion_final(16);
  }

  private static void permutacion_inicial(int indice) {
    int[] pi = {
        58, 50, 42, 34, 26, 18, 10, 2,
        60, 52, 44, 36, 28, 20, 12, 4,
        62, 54, 46, 38, 30, 22, 14, 6,
        64, 56, 48, 40, 32, 24, 16, 8,
        57, 49, 41, 33, 25, 17, 9, 1,
        59, 51, 43, 35, 27, 19, 11, 3,
        61, 53, 45, 37, 29, 21, 13, 5,
        63, 55, 47, 39, 31, 23, 15, 7};

    for (int i = 0; i < 32; i++) {
      if (indice == 0) {
        L[0][i] = entrada[pi[i] - 1];
        R[0][i] = entrada[pi[i + 32] - 1];
      }
      else {
        R[16][i] = entrada[pi[i] - 1];
        L[16][i] = entrada[pi[i + 32] - 1];
      }
    }
  }

  private static void funcion_cifrado(int indice) {
    int i2, j, a, h;
    int[] F = new int[33];
    int[][] suma = new int[9][7];
    int[] exp_R = new int[49];
    int[] comp32 = new int[33];

    for (i2 = 0; i2 < 48; i2++) {
      exp_R[i2] = R[indice - 1][expansion[i2] - 1];

    }
    for (i2 = 0; i2 < 8; i2++) {
      for (j = 0; j < 6; j++) {
        suma[i2][j] = exp_R[i2 * 6 + j] ^ K[indice][i2 * 6 + j];

      }
    }
    for (h = 0; h < 8; h++) {
      i2 = suma[h][5] + 2 * suma[h][0];
      j = suma[h][4] + suma[h][3] * 2 + suma[h][2] * 4 + suma[h][1] * 8;
      a = s[h][i2][j];

      if ( (a != 0) && (8 != 0)) {
        comp32[h * 4] = 1;
      }
      else {
        comp32[h * 4] = 0;

      }
      if ( (a != 0) && (4 != 0)) {
        comp32[h * 4 + 1] = 1;
      }
      else {
        comp32[h * 4 + 1] = 0;

      }
      if ( (a != 0) && (2 != 0)) {
        comp32[h * 4 + 2] = 1;
      }
      else {
        comp32[h * 4 + 2] = 0;

      }
      if (a != 0) {
        comp32[h * 4 + 3] = 1;
      }
      else {
        comp32[h * 4 + 3] = 0;

        /*      if ((a&8)!=0)
                comp32[h*4] = 1;
              else
                comp32[h*4] = 0;
              if ((a&4)!=0)
                comp32 [h*4 + 1] =1;
              else
                comp32 [h*4 + 1] =0;
              if ((a&2)!=0)
                comp32[h*4 + 2] =1;
              else
                comp32[h*4 + 2] =0;
              if ((a&1)!=0)
                comp32[h*4 + 3] =1;
              else
                comp32[h*4 + 3] =0;*/
      }
    }

    for (i2 = 0; i2 < 32; i2++) {
      F[i2] = comp32[P[i2] - 1];

    }
    for (i2 = 0; i2 < 32; i2++) {
      R[indice][i2] = L[indice - 1][i2] ^ F[i2];
    }
  }

  private static void permutacion_final(int indice) {
    int[] pf = {
        40, 8, 48, 16, 56, 24, 64, 32,
        39, 7, 47, 15, 55, 23, 63, 31,
        38, 6, 46, 14, 54, 22, 62, 30,
        37, 5, 45, 13, 53, 21, 61, 29,
        36, 4, 44, 12, 52, 20, 60, 28,
        35, 3, 43, 11, 51, 19, 59, 27,
        34, 2, 42, 10, 50, 18, 58, 26,
        33, 1, 41, 9, 49, 17, 57, 25};

    int i;
    int[] r1 = new int[65];

    for (i = 0; i < 32; i++) {
      if (indice == 0) {
        r1[i] = L[0][i];
        r1[i + 32] = R[0][i];
      }
      else {
        r1[i] = R[16][i];
        r1[i + 32] = L[16][i];
      }
    }
    for (i = 0; i < 64; i++) {
      salida[i] = r1[pf[i] - 1];
    }
  }

  private static char entHex(int entrada) {
    char salida = ' ';
    switch (entrada) {
      case 0:
        salida = '0';
        break;
      case 1:
        salida = '1';
        break;
      case 2:
        salida = '2';
        break;
      case 3:
        salida = '3';
        break;
      case 4:
        salida = '4';
        break;
      case 5:
        salida = '5';
        break;
      case 6:
        salida = '6';
        break;
      case 7:
        salida = '7';
        break;
      case 8:
        salida = '8';
        break;
      case 9:
        salida = '9';
        break;
      case 10:
        salida = 'A';
        break;
      case 11:
        salida = 'B';
        break;
      case 12:
        salida = 'C';
        break;
      case 13:
        salida = 'D';
        break;
      case 14:
        salida = 'E';
        break;
      case 15:
        salida = 'F';
        break;
      default:
        salida = 'X';
    }
    return salida;
  }

  public static String cifraClave(String entrada) {

    String salida = "";

    int a[];
    int b[];
    char d[] = new char[16];
    int p = 0;

    a = cifralo(entrada, entrada);

    for (p = 0; p < a.length - 1; p++) {
      d[p] = entHex( (int) a[p]);

      // Modificaci�n "a boleo" para que salga el resultado del ejecutable en 'Delphi'.

    }
    salida = "P";
    for (p = 0; p < d.length - 5; p++) {
      salida += d[p];

      /*  b = cifralo (String.valueOf(d),String.valueOf(d));
         for (p=0;p<b.length-1;p++)
          salida+=entHex((int)b[p]);*/

    }
    return salida;
  }

  /*
    public static void main(String[] args) {
      System_out.println("GVADMON  "+ Cifrar.cifraClave("GVADMON"));
      System_out.println("GVDIST  "+ Cifrar.cifraClave("GVDIST"));
      System_out.println("GVCENT  "+ Cifrar.cifraClave("GVCENT"));
      System_out.println("GVPRU  "+ Cifrar.cifraClave("GVPRU"));
      System_out.println("CVADMON  "+Cifrar.cifraClave("CVADMON"));
      System_out.println("CVCENTRO  "+Cifrar.cifraClave("CVCENTRO"));
      System_out.println("CVPRU  "+Cifrar.cifraClave("CVPRU"));
      System_out.println("CHEMA  "+Cifrar.cifraClave("CHEMA"));
      System_out.println("C  "+Cifrar.cifraClave("C"));
      System_out.println("ADMON  "+Cifrar.cifraClave("ADMON"));
      System_out.println("CRISTINA  "+Cifrar.cifraClave("CRISTINA"));
      System_out.println("HUERTAS  "+Cifrar.cifraClave("HUERTAS"));
      System_out.println("ENRIQUE  "+Cifrar.cifraClave("ENRIQUE"));
      System_out.println("FERNANDO  "+Cifrar.cifraClave("FERNANDO"));
      System_out.println("FELIX  "+Cifrar.cifraClave("FELIX"));
      System_out.println("MARIBEL  "+Cifrar.cifraClave("MARIBEL"));
      System_out.println("MAYTE  "+Cifrar.cifraClave("MAYTE"));
      System_out.println("AMARAL  "+Cifrar.cifraClave("AMARAL"));
      System_out.println("AGUIRRE  "+Cifrar.cifraClave("AGUIRRE"));
      System_out.println("FABIROL  "+Cifrar.cifraClave("FABIROL"));
      System_out.println("CAU  "+Cifrar.cifraClave("CAU"));
      System_out.println("DESASTRE  "+Cifrar.cifraClave("DESASTRE"));
      System_out.println("sive_desa"+Cifrar.cifraClave("sive_desa"));
        // JRM
      System_out.println("NORSISTEMAS"+Cifrar.cifraClave("NORSISTEMAS"));
      try {
        System_out.println("ADMON".getBytes("cp437"));
      }
      catch (Exception exc){
         System_out.println("excepcion");
      }
    }
   */

}
