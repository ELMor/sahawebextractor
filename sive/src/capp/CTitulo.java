package capp;

import java.net.URL;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;

public class CTitulo
    extends CPanel {

  final String imgNAME[] = {
      "images/salir.gif",
      "images/ayuda.gif"};

  BorderLayout borderLayout = new BorderLayout();
  BorderLayout borderLayoutB = new BorderLayout();
  Label lblTitulo = new Label();
  ButtonControl btnCerrar = new ButtonControl();
  ButtonControl btnAyuda = new ButtonControl();
  Panel pnlButton = new Panel();

  protected String sPaginaAyuda;

  public CTitulo(CApp a, String tit) {
    try {
      setApp(a);
      lblTitulo.setText(" " + tit);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void Inicializar() {
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    setLayout(borderLayout);
    setForeground(SystemColor.activeCaptionText);
    setBackground(SystemColor.activeCaption);
    lblTitulo.setFont(new Font("Dialog", 1, 12));
    btnCerrar.setImage(imgs.getImage(0));
    btnAyuda.addActionListener(new CTitulo_btnAyuda_actionAdapter(this));
    btnAyuda.setImage(imgs.getImage(1));
    btnCerrar.addActionListener(new CTitulo_btnCerrar_actionAdapter(this));
    pnlButton.setLayout(borderLayoutB);
    pnlButton.add(btnCerrar, BorderLayout.EAST);
    pnlButton.add(btnAyuda, BorderLayout.WEST);
    add(lblTitulo, BorderLayout.CENTER);
    add(pnlButton, BorderLayout.EAST);
  }

  void btnCerrar_actionPerformed(ActionEvent e) {
    try {
      System.runFinalization();
      System.gc();
      app.getAppletContext().showDocument(new URL(app.getCodeBase(),
                                                  "default.html"), "_self");
    }
    catch (Exception ex) {}
  }

  void btnAyuda_actionPerformed(ActionEvent e) {
    try {
//      app.getAppletContext().showDocument( new URL(app.getCodeBase(), sPaginaAyuda),"_ayuda");
      app.getAppletContext().showDocument(new URL(app.getCodeBase(),
                                                  sPaginaAyuda), "_blank");
    }
    catch (Exception ex) {}
  }

  public void setPaginaAyuda(String pagina) {
    sPaginaAyuda = pagina;
  }
}

class CTitulo_btnCerrar_actionAdapter
    implements java.awt.event.ActionListener {
  CTitulo adaptee;

  CTitulo_btnCerrar_actionAdapter(CTitulo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnCerrar_actionPerformed(e);
  }
}

class CTitulo_btnAyuda_actionAdapter
    implements java.awt.event.ActionListener {
  CTitulo adaptee;

  CTitulo_btnAyuda_actionAdapter(CTitulo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAyuda_actionPerformed(e);
  }
}
