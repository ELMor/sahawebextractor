package capp;

import java.util.Vector;

public class CLista
    extends Vector {

  // estados de la lista
  final static public int listaVACIA = 0;
  final static public int listaLLENA = 1;
  final static public int listaINCOMPLETA = 3;
  final static public int listaNOVALIDA = 4;

  protected int iState;
  protected String sFilter;
  protected int iIdioma;
  protected int iPerfil;
  protected String sLogin;
  protected String sTSive;
  protected String sCdEnfermedad;
  // ARG: Lortad
  protected boolean bLortad;
  protected Vector vN1;
  protected Vector vN2;

  protected boolean bMantenimiento = false; //Indica si estamos en un mantenimiento
  //En un mantenimiento habr� que traerse las dos descripciones (del c�digo principal)

  public CLista(int size) {
    super(size);

    // inicilaiza el estado
    iState = CLista.listaVACIA;
    iIdioma = CApp.idiomaPORDEFECTO;
    sLogin = null;
    sFilter = "";
    sTSive = null;
    sCdEnfermedad = null;
    vN1 = null;
    vN2 = null;
    bLortad = false;
  }

  public CLista() {
    super();

    // inicilaiza el estado
    iState = CLista.listaVACIA;
    iIdioma = CApp.idiomaPORDEFECTO;
    sLogin = null;
    sFilter = "";
    sTSive = null;
    sCdEnfermedad = null;
    vN1 = null;
    vN2 = null;
    bLortad = false;
  }

  public int getState() {
    return iState;
  }

  public void setState(int s) {
    iState = s;
  }

  public int getIdioma() {
    return iIdioma;
  }

  public void setIdioma(int i) {
    iIdioma = i;
  }

  public void setFilter(String f) {
    sFilter = f;
  }

  public String getFilter() {
    return sFilter;
  }

  public void setLogin(String u) {
    sLogin = u;
  }

  public String getLogin() {
    return sLogin;
  }

  public void setPerfil(int p) {
    iPerfil = p;
  }

  public int getPerfil() {
    return iPerfil;
  }

  public void setTSive(String ts) {
    sTSive = ts;
  }

  public String getTSive() {
    return sTSive;
  }

  public void setVN1(Vector V1) {
    vN1 = V1;
  }

  public Vector getVN1() {
    return vN1;
  }

  public void setVN2(Vector V2) {
    vN2 = V2;
  }

  public Vector getVN2() {
    return vN2;
  }

  public void setMantenimiento(boolean mant) {
    bMantenimiento = mant;
  }

  public boolean getMantenimiento() {
    return bMantenimiento;
  }

  public String getCdEnfermedad() {
    return sCdEnfermedad;
  }

  public void setCdEnfermedad(String s) {
    sCdEnfermedad = s;
  }

  // ARG: Obtiene si hay que aplicar la Lortad
  public boolean getLortad() {
    return bLortad;
  }

  // ARG: Asigna el valor a sLortad
  public void setLortad(String sLortad) {
    if (sLortad != null) {
      if (sLortad.equals("S")) {
        bLortad = true;
      }
      else {
        bLortad = false;
      }
    }
    else {
      bLortad = false;
    }
  }

  public void appendData(CLista data) {

    for (int j = 0; j < data.elementCount; j++) {
      this.addElement(data.elementAt(j));
    }

    // los datos de control son los devueltos en la nueva lista
    this.iState = data.getState();
    this.sFilter = data.getFilter();
  }
}
