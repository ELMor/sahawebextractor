/* 20/03/2000 (JMT) Arreglo de los eventos: eliminacion de clases anonimas */
package capp;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;

public class CMessage
    extends Dialog {

  // propiedades
  protected CApp app = null;
  ResourceBundle res;
  protected boolean response = false;
  protected CImage image = null;

  // tama�o predefinido (s�lo en altura)
  final int msgHEIGHT = 130;
  protected int msgWIDTH = 300;

  // componentes
  ButtonControl btn1 = new ButtonControl();
  ButtonControl btn2 = new ButtonControl();
  Label lbl = new Label();
  Panel pnl = new Panel();
  XYLayout xyLyt = new XYLayout();

  // constantes de configuraci�n
  final static public int msgERROR = 0;
  final static public int msgADVERTENCIA = 1;
  final static public int msgPREGUNTA = 2;
  final static public int msgAVISO = 3;

  // constantes de respuesta
  final static public boolean msgSI = true;
  final static public boolean msgNO = false;
  final static public boolean msgACEPTAR = true;
  final static public boolean msgCANCELAR = false;

  final String imgERROR = "images/msg0.gif";
  final String imgADVERTENCIA = "images/msg1.gif";
  final String imgPREGUNTA = "images/msg2.gif";
  final String imgAVISO = "images/msg3.gif";
  final String imgACEPTAR = "images/aceptar.gif";
  final String imgCANCELAR = "images/cancelar.gif";

  public CMessage(CApp a, int type, String message) {
    super(a.getParentFrame(), "", true);

    String title = "";
    String filename = "";
    app = a;
    res = ResourceBundle.getBundle("capp.Res" + this.app.getIdioma());

    // colores y tipo de letra
    this.setLayout(xyLyt);
    this.setBackground(Color.lightGray);
    this.setForeground(Color.black);
    pnl.setBackground(Color.lightGray);
    pnl.setForeground(Color.black);
    lbl.setFont(new Font("Dialog", 1, 12));

    // selecciona el tipo de ventana
    try {
      switch (type) {
        case msgERROR:
          title = res.getString("msg4.Text");
          btn1.setLabel(res.getString("btn1.Label"));
          btn1.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
          btn2.setVisible(false);
          filename = imgERROR;
          break;
        case msgAVISO:
          title = res.getString("msg5.Text");
          btn1.setLabel(res.getString("btn1.Label"));
          btn1.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
          btn2.setVisible(false);
          filename = imgAVISO;
          break;
        case msgPREGUNTA:
          title = res.getString("msg6.Text");
          btn1.setLabel(res.getString("btn1.Label1"));
          btn1.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
          btn2.setLabel(res.getString("btn2.Label"));
          btn2.setImageURL(new URL(app.getCodeBase(), imgCANCELAR));
          filename = imgPREGUNTA;
          break;
        case msgADVERTENCIA:
          title = res.getString("msg7.Text");
          btn1.setLabel(res.getString("btn1.Label"));
          btn1.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
          btn2.setLabel(res.getString("btn2.Label1"));
          btn2.setImageURL(new URL(app.getCodeBase(), imgCANCELAR));
          filename = imgADVERTENCIA;
          break;
      }
    }
    catch (Exception e) {
      dispose();
    }
    setTitle(title);

    Graphics g = app.getGraphics();
    g.setFont(new Font("Dialog", 1, 12));

    if (message == null) {
      message = " ";

    }
    try {
      msgWIDTH = g.getFontMetrics().stringWidth(message);
      lbl.setText(message);
    }
    catch (Exception e) {
    }

    // tama�o del di�logo
    this.setSize(msgWIDTH + 70, msgHEIGHT);
    setResizable(false);

    // imagen
    try {
      image = new CImage(new URL(app.getCodeBase(), filename), 40, 40);
      this.add(image, new XYConstraints(5, 5, 45, 45));
    }
    catch (MalformedURLException e) {
      image = null;
    }

    // etiqueta
    this.add(lbl, new XYConstraints(50, 20, -1, -1));

    // botones
    pnl.setLayout(new FlowLayout());
    pnl.add(btn1);
    pnl.add(btn2);
    this.add(pnl, new XYConstraints(0, 50, msgWIDTH, -1));

    // instala los escuchadores
    this.addWindowListener(new MiWindowAdapter(this));

    btn1.setActionCommand("btn1");
    btn1.addActionListener(new MiActionAdapter(this));

    btn2.setActionCommand("btn2");
    btn2.addActionListener(new MiActionAdapter(this));

  } //end CMessage

  public void setWidth(int width) {
    setSize(width, msgHEIGHT);
  } //end CMessage

  public void show() {
    if (app != null) {
      app.getParentFrame().setEnabled(false);
      Dimension dimCapp = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension dimCmsg = getSize();
      setLocation( (dimCapp.width - dimCmsg.width) / 2,
                  (dimCapp.height - dimCmsg.height) / 2);
    } //endif
    super.show();
  } //end show

  void mi_windowClosed(WindowEvent e) {
    if (app != null) {
      app.getParentFrame().setEnabled(true); // Siempre que se cierra
    } //endif
  } //end mi_windowClosed

  void mi_windowClosing(WindowEvent e) {
    dispose(); // Se cierra con la cruz
  } //end mi_windowClosing

  void btn_actionPerformed(ActionEvent e, boolean r) {
    response = r;
    dispose();
  }

  public boolean getResponse() {
    return (response);
  }
}

// Cierre de Ventana
class MiWindowAdapter
    extends WindowAdapter {
  CMessage adaptee;

  MiWindowAdapter(CMessage adaptee) {
    this.adaptee = adaptee;
  }

  public void windowClosed(WindowEvent e) {
    adaptee.mi_windowClosed(e);
  }

  public void windowClosing(WindowEvent e) {
    adaptee.mi_windowClosing(e);
  }

} // endclass  MiWindowAdapter

// Botones
class MiActionAdapter
    implements ActionListener {
  CMessage adaptee;

  MiActionAdapter(CMessage adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("btn1")) {
      adaptee.btn_actionPerformed(e, true);
    }
    else if (e.getActionCommand().equals("btn2")) {
      adaptee.btn_actionPerformed(e, false);
    }
  }
} // endclass  MiActionAdapter
