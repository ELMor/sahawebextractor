package capp;

import java.awt.Color;

import jclass.bwt.JCMultiColumnList;

public class CTabla
    extends JCMultiColumnList {

  public CTabla() {
    super();

    getList().setBackground(Color.white);
    getList().setHighlightColor(Color.lightGray);
    setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    setAutoSelect(true);
    setColumnLabelSort(false); //No ordena nunca las filas
  }
}
