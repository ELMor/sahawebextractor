package capp;

import java.util.Vector;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.Graphics;

//Canvas para el selector de etiqueta
public class CSolapa
    extends Canvas {

  //Vector name = new Vector();
  //int chosen = 0;

  // modificacion 19/06/2000
  // para poder extender CSolapa y CTabPanel
  public Vector name = new Vector();
  public int chosen = 0;

  //Fuente para las etiquetas
  Font font = new Font("Helvetica", Font.PLAIN, 12),
      chfont = new Font(font.getName(), Font.BOLD, 12);

  //Constructor.
  // modificacion 19/06/2000  (hago publico el constructor)
  // para poder extender CSolapa y CTabPanel
  public CSolapa() {
    setBackground(new Color(192, 192, 192));
  }

  //Funcion para a�adir elementos
  void addItem(String n) {
    boolean esta = false;

    for (int i = 0; i < name.size() && !esta; i++) {
      esta = ( (String) name.elementAt(i)).equals(n);
    } //endfor

    if (!esta) {
      name.addElement(n);
      paint(getGraphics());
    } //endif
  }

  //Funcion para la seleccion de un elemento
  void choose(String n) {
    for (int i = 0; i < name.size(); i++) {
      if ( ( (String) name.elementAt(i)).equals(n)) {
        chosen = i;
        paint(getGraphics());
      }
    }
  }

  public void update(Graphics g) {
    setBackground(getParent().getBackground());
    paint(g);
  }

  //METODO PAINT DEL CANVAS
  public void paint(Graphics g) {
    if (g == null || name.size() == 0) {
      return;
    }

    g.setColor(getParent().getBackground());
    g.fillRect(0, 0, getBounds().width, getBounds().height);

    //Pintado, a pedal, de los bordes.
    int tw = getBounds().width / name.size();
    int th = getBounds().height;

    for (int i = 0; i < name.size(); i++) {
      int x = tw * i;

      if (i == chosen) {
        g.setColor(getBackground().darker());
        g.drawLine(x + tw - 3, 1, x + tw - 3, th - 1);
        g.setColor(getBackground().brighter());
        g.drawLine(x, 0, x, th - 1);
        g.drawLine(x, 0, x + tw - 4, 0);
        g.drawLine(x + tw - 3, th - 1, x + tw - 1, th - 1);
      }
      else {
        g.setColor(getBackground().darker());
        g.drawLine(x + tw - 3, 6, x + tw - 3, th - 1);
        g.setColor(getBackground().brighter());
        g.drawLine(x, 5, x, th - 1);
        g.drawLine(x, 5, x + tw - 4, 5);
        g.drawLine(x, th - 1, x + tw - 1, th - 1);
      } //endif

      g.setColor(Color.black);

      //Cambio el font del solapa seg�n se haya seleccionado o no el solapa.
      if (i == chosen) {
        g.setFont(chfont);
      }
      else {
        g.setFont(font);

      }
      String str = (String) name.elementAt(i);
      int textw = g.getFontMetrics().stringWidth(str);
      int texth = g.getFontMetrics().getHeight();
      if (textw < tw - 5) {
        g.drawString(str, x + (tw - textw) / 2, (th - texth) / 2 + texth);
      }
    }
  }

  //Eventos sobre el canvas.
  public boolean mouseDown(Event evt, int x, int y) {
    if (name.size() == 0) {
      return false;
    }
    chosen = x / (getBounds().width / name.size());
    paint(getGraphics());
    ( (CTabPanel) getParent()).VerPanel( (String) name.elementAt(chosen));
    return true;
  }

  //Minimum size del canvas.
  public Dimension minimumSize() {
    return new Dimension(50, 25);
  }

  public Dimension preferredSize() {
    return minimumSize();
  }

  // countTabs
  // Devuelve el n�mero de solapas
  public int countTabs() {
    return name.size();
  }

  // delItem
  // Elimina la solapa i
  public void delItem(int i) {
    if (i >= 0 && i <= name.size()) {
      name.removeElementAt(i);
    }
  }

  public void delAll() {
    name.removeAllElements();
    setVisible(false);
  }

  // label
  // Nombre de la solapa i
  public String label(int i) {
    return (String) name.elementAt(i);
  }

  // getCurrentPanelNdx
  // indice de solapa seleccionada
  public int getCurrentPanelNdx() {
    return chosen;
  }

}
