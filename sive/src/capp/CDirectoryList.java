//Title:        capp
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:  capp

package capp;

import java.io.File;
import java.io.FilenameFilter;

import java.awt.Dimension;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;

public class CDirectoryList
    extends CPanel {
  TextField txtDir = new TextField();
  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnListar = new ButtonControl();
  java.awt.List lstFichero = new java.awt.List();
  protected CCargadorImagen imgs = null;
  final String imgBROWSER = "images/browser.gif";
  final String dirDEFAULT = "c:\\";

  public CDirectoryList(CApp a) {
    try {
      setApp(a);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(313, 269));
    xYLayout1.setHeight(230);
    xYLayout1.setWidth(339);
    txtDir.setText(dirDEFAULT);
    this.setLayout(xYLayout1);
    this.add(txtDir, new XYConstraints(3, 3, 291, -1));
    this.add(btnListar, new XYConstraints(301, 3, -1, -1));
    this.add(lstFichero, new XYConstraints(4, 33, 324, 185));
    imgs = new CCargadorImagen(app);
    txtDir.addTextListener(new CDirectoryList_txtDir_textAdapter(this));
    imgs.setImage(imgBROWSER);
    imgs.CargaImagenes();
    btnListar.setImage(imgs.getImage(0));
    btnListar.addActionListener(new CDirectoryList_btnListar_actionAdapter(this));
    this.setBorde(false);
    lstFichero.setMultipleMode(true);
  }

  public void Inicializar() {
  }

  public void setEnabled(boolean e) {
    txtDir.setEnabled(e);
    btnListar.setEnabled(e);
    lstFichero.setEnabled(e);
  }

  void btnListar_actionPerformed(ActionEvent e) {
    File f = null;
    String asFicheros[] = null;

    if (txtDir.getText().trim().length() == 0) {
      txtDir.setText(dirDEFAULT);

    }
    f = new File(txtDir.getText().trim());
    asFicheros = f.list(new FileChecker());

    lstFichero.removeAll();

    if (asFicheros != null) {
      for (int j = 0; j < asFicheros.length; j++) {
        lstFichero.addItem(asFicheros[j]);
      }
    }
  }

  public String getPath() {
    String path = txtDir.getText().trim();

    if (!path.endsWith("\\")) {
      path = path + "\\";

    }
    return path;
  }

  public String[] getFicheros() {
    return lstFichero.getSelectedItems();
  }

  void txtDir_textValueChanged(TextEvent e) {
    if (lstFichero.getItemCount() > 0) {
      lstFichero.removeAll();
    }
  }
}

class CDirectoryList_btnListar_actionAdapter
    implements java.awt.event.ActionListener {
  CDirectoryList adaptee;

  CDirectoryList_btnListar_actionAdapter(CDirectoryList adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnListar_actionPerformed(e);
  }
}

class FileChecker
    implements FilenameFilter {
  public boolean accept(File dir, String name) {

    for (int i = 0; i < name.length(); i++) {
      if ( (name.substring(i, i + 1).equals(".")) && (i > 0)) {
        return true;
      }
    }

    return false;
  }
}

class CDirectoryList_txtDir_textAdapter
    implements java.awt.event.TextListener {
  CDirectoryList adaptee;

  CDirectoryList_txtDir_textAdapter(CDirectoryList adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    adaptee.txtDir_textValueChanged(e);
  }
}
