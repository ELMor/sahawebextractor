package bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import bidata.DataAlerta;
import bidata.DataBrote;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvBISelect
    extends DBServlet {

  final int servletSELECT = 0;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  private String fecha_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    return f.substring(0, f.indexOf(' ', 0));
  }

  private String hora_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    f = f.substring(f.indexOf(' ', 0) + 1); //descartar ss
    f = f.substring(0, 5);
    return f;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    DataBrote dataEntrada = null;
    CLista listaSalida = new CLista();
    DataBrote dataSalida = new DataBrote();

    //querys
    String sQuery = "";

    //fechas
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //STRING
    String desMunicipio = null;

    String codCadef = null;

    String desNivel1LCA = null;
    String desNivel2LCA = null;
    String desZBSLCA = null;

    String desNivel1LE = null;
    String desNivel2LE = null;
    String desZBSLE = null;

    String desNivel1_normal = null;
    String desNivel2_normal = null;

    boolean hayRegistro = false;

    try {

      switch (opmode) {

        //con SUCA cambia la seleccion de descripciones de pais, ca, prov y mun
        case servletSELECT:

          //param(0) = DataBrote
          //devuelve:  ( DataBrote )

          dataEntrada = (DataBrote) param.firstElement();

          // System_out.println("SrvBISelect: antes Pregunta_Select_Brotes()");
          // System_out.println("SrvBISelect: a�o " + dataEntrada.getCD_ANO().trim());
          // System_out.println("SrvBISelect: nm_alerbro " + dataEntrada.getNM_ALERBRO().trim());

          sQuery = Pregunta_Select_Brotes();
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2,
                    (new Integer(dataEntrada.getNM_ALERBRO().trim())).intValue());

          rs = st.executeQuery();

          //TS
          java.sql.Timestamp dFC_FECHAHORA = null;
          String FC_FECHAHORA_F = "";
          String FC_FECHAHORA_H = "";
          java.sql.Timestamp dFC_EXPOSICION = null;
          String FC_EXPOSICION_F = "";
          String FC_EXPOSICION_H = "";
          java.sql.Timestamp dFC_FSINPRIMC = null;
          String FC_FSINPRIMC_F = "";
          String FC_FSINPRIMC_H = "";
          java.sql.Timestamp dFC_ISINPRIMC = null;
          String FC_ISINPRIMC_F = "";
          String FC_ISINPRIMC_H = "";

          //ULTACT
          fecRecu = null;
          sfecRecu = "";

          // extrae el registro encontrado
          while (rs.next()) {

            hayRegistro = true;

            //TS
            dFC_FSINPRIMC = rs.getTimestamp("FC_FSINPRIMC");
            if (dFC_FSINPRIMC == null) {
              FC_FSINPRIMC_F = null;
              FC_FSINPRIMC_H = null;
            }
            else {
              FC_FSINPRIMC_F = fecha_de_fecha(timestamp_a_cadena(dFC_FSINPRIMC));
              FC_FSINPRIMC_H = hora_de_fecha(timestamp_a_cadena(dFC_FSINPRIMC));
            }

            dFC_ISINPRIMC = rs.getTimestamp("FC_ISINPRIMC");
            if (dFC_ISINPRIMC == null) {
              FC_ISINPRIMC_F = null;
              FC_ISINPRIMC_H = null;
            }
            else {
              FC_ISINPRIMC_F = fecha_de_fecha(timestamp_a_cadena(dFC_ISINPRIMC));
              FC_ISINPRIMC_H = hora_de_fecha(timestamp_a_cadena(dFC_ISINPRIMC));
            }

            dFC_EXPOSICION = rs.getTimestamp("FC_EXPOSICION");
            if (dFC_EXPOSICION == null) {
              FC_EXPOSICION_F = null;
              FC_EXPOSICION_H = null;
            }
            else {
              FC_EXPOSICION_F = fecha_de_fecha(timestamp_a_cadena(
                  dFC_EXPOSICION));
              FC_EXPOSICION_H = hora_de_fecha(timestamp_a_cadena(dFC_EXPOSICION));
            }

            dFC_FECHAHORA = rs.getTimestamp("FC_FECHAHORA");
            // fechaHora no puede ser nulo !!!!
            if (dFC_FECHAHORA == null) {
              FC_FECHAHORA_F = null;
              FC_FECHAHORA_H = null;
            }
            else {
              FC_FECHAHORA_F = fecha_de_fecha(timestamp_a_cadena(dFC_FECHAHORA));
              FC_FECHAHORA_H = hora_de_fecha(timestamp_a_cadena(dFC_FECHAHORA));
            }

            fecRecu = rs.getTimestamp("FC_ULTACT");
            sfecRecu = timestamp_a_cadena(fecRecu);

            dataSalida.insert( (String) "FC_FSINPRIMC_F", FC_FSINPRIMC_F);
            dataSalida.insert( (String) "FC_FSINPRIMC_H", FC_FSINPRIMC_H);
            dataSalida.insert( (String) "FC_ISINPRIMC_F", FC_ISINPRIMC_F);
            dataSalida.insert( (String) "FC_ISINPRIMC_H", FC_ISINPRIMC_H);

            dataSalida.insert( (String) "NM_PERINMIN",
                              rs.getString("NM_PERINMIN"));
            dataSalida.insert( (String) "NM_PERINMAX",
                              rs.getString("NM_PERINMAX"));
            dataSalida.insert( (String) "NM_PERINMED",
                              rs.getString("NM_PERINMED"));

            dataSalida.insert( (String) "NM_ENFERMOS",
                              rs.getString("NM_ENFERMOS"));
            dataSalida.insert( (String) "FC_EXPOSICION_F", FC_EXPOSICION_F);
            dataSalida.insert( (String) "FC_EXPOSICION_H", FC_EXPOSICION_H);
            dataSalida.insert( (String) "NM_INGHOSP", rs.getString("NM_INGHOSP"));
            dataSalida.insert( (String) "NM_DEFUNCION",
                              rs.getString("NM_DEFUNCION"));
            dataSalida.insert( (String) "NM_EXPUESTOS",
                              rs.getString("NM_EXPUESTOS"));

            dataSalida.insert( (String) "CD_TRANSMIS",
                              rs.getString("CD_TRANSMIS"));

            dataSalida.insert( (String) "NM_NVACENF", rs.getString("NM_NVACENF"));

            dataSalida.insert( (String) "CD_OPE", rs.getString("CD_OPE"));
            dataSalida.insert( (String) "FC_ULTACT", sfecRecu);

            dataSalida.insert( (String) "NM_DCUACMIN",
                              rs.getString("NM_DCUACMIN"));
            dataSalida.insert( (String) "NM_DCUACMED",
                              rs.getString("NM_DCUACMED"));
            dataSalida.insert( (String) "NM_DCUACMAX",
                              rs.getString("NM_DCUACMAX"));

            dataSalida.insert( (String) "NM_NVACNENF",
                              rs.getString("NM_NVACNENF"));
            dataSalida.insert( (String) "NM_VACNENF", rs.getString("NM_VACNENF"));
            dataSalida.insert( (String) "NM_VACENF", rs.getString("NM_VACENF"));

            dataSalida.insert( (String) "DS_OBSERV", rs.getString("DS_OBSERV"));

            dataSalida.insert( (String) "CD_TIPOCOL", rs.getString("CD_TIPOCOL"));
            dataSalida.insert( (String) "DS_NOMCOL", rs.getString("DS_NOMCOL"));
            dataSalida.insert( (String) "DS_DIRCOL", rs.getString("DS_DIRCOL"));
            dataSalida.insert( (String) "CD_POSTALCOL",
                              rs.getString("CD_POSTALCOL"));
            dataSalida.insert( (String) "CD_PROVCOL", rs.getString("CD_PROVCOL"));

            dataSalida.insert( (String) "CD_ANO", rs.getString("CD_ANO"));
            dataSalida.insert( (String) "NM_ALERBRO",
                              (new Integer(rs.getString("NM_ALERBRO"))).
                              toString().trim());

            dataSalida.insert( (String) "DS_BROTE", rs.getString("DS_BROTE"));
            dataSalida.insert( (String) "CD_GRUPO", rs.getString("CD_GRUPO"));

            dataSalida.insert( (String) "FC_FECHAHORA_F", FC_FECHAHORA_F);
            dataSalida.insert( (String) "FC_FECHAHORA_H", FC_FECHAHORA_H);

            dataSalida.insert( (String) "NM_MANIPUL", rs.getString("NM_MANIPUL"));

            dataSalida.insert( (String) "CD_ZBS_LE", rs.getString("CD_ZBS_LE"));
            dataSalida.insert( (String) "CD_NIVEL_2_LE",
                              rs.getString("CD_NIVEL_2_LE"));

            dataSalida.insert( (String) "CD_TNOTIF", rs.getString("CD_TNOTIF"));

            dataSalida.insert( (String) "IT_RESCALC", rs.getString("IT_RESCALC"));

            dataSalida.insert( (String) "CD_MUNCOL", rs.getString("CD_MUNCOL"));
            dataSalida.insert( (String) "CD_NIVEL_1_LCA",
                              rs.getString("CD_NIVEL_1_LCA"));
            dataSalida.insert( (String) "DS_TELCOL", rs.getString("DS_TELCOL"));
            dataSalida.insert( (String) "CD_NIVEL_1_LE",
                              rs.getString("CD_NIVEL_1_LE"));
            dataSalida.insert( (String) "CD_NIVEL_2_LCA",
                              rs.getString("CD_NIVEL_2_LCA"));
            dataSalida.insert( (String) "CD_ZBS_LCA", rs.getString("CD_ZBS_LCA"));

            dataSalida.insert( (String) "DS_NMCALLE", rs.getString("DS_NMCALLE"));
            dataSalida.insert( (String) "CD_TBROTE", rs.getString("CD_TBROTE"));
            dataSalida.insert( (String) "IT_PERIN", rs.getString("IT_PERIN"));
            dataSalida.insert( (String) "IT_DCUAC", rs.getString("IT_DCUAC"));
            dataSalida.insert( (String) "DS_PISOCOL", rs.getString("DS_PISOCOL"));
          } //fin del while

          rs.close();
          rs = null;
          st.close();
          st = null;

          // System_out.println("SrvBISelect: despues de Pregunta_Select_Brotes ");

          if (!hayRegistro) {
            listaSalida = new CLista();
            break;
          }

          //completamos las descripciones

          // Municipio
          if (dataSalida.getCD_PROVCOL() != null &&
              dataSalida.getCD_MUNCOL() != null) {

            // System_out.println("SrvBISelect: antes de Pregunta_Select_Municipio");

            sQuery = Pregunta_Select_Municipio();

            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_PROVCOL().trim());
            st.setString(2, dataSalida.getCD_MUNCOL().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              desMunicipio = rs.getString("DS_MUN");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            // System_out.println("SrvBISelect: despues de Pregunta_Select_Municipio");
          }

          // c�digo de la comunidad aut�noma
          if (dataSalida.getCD_PROVCOL() != null) {
            sQuery = "select CD_CA from SIVE_PROVINCIA where CD_PROV = ?";

            // System_out.println("SrvBISelect: antes de recupera el c�digo de la comunidad aut�noma ");

            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_PROVCOL().trim());
            rs = st.executeQuery();
            while (rs.next()) {
              codCadef = rs.getString("CD_CA");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            // System_out.println("SrvBISelect: despues de recupera el c�digo de la comunidad aut�noma ");

          }

          //nivel 1 LCA
          if (dataSalida.getCD_NIVEL_1_LCA() != null) {
            // System_out.println("SrvBISelect: antes de Pregunta_Select_Nivel1 ");
            sQuery = Pregunta_Select_Nivel1();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_LCA().trim());
            rs = st.executeQuery();
            String desN1 = "";
            String desLN1 = "";

            while (rs.next()) {
              desN1 = rs.getString("DS_NIVEL_1");
              desLN1 = rs.getString("DSL_NIVEL_1");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN1 != null)) {
                desNivel1LCA = desLN1;
              }
              else {
                desNivel1LCA = desN1;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            // System_out.println("SrvBISelect: despues de Pregunta_Select_Nivel1 ");
          }

          //nivel2 LCA
          if (dataSalida.getCD_NIVEL_1_LCA() != null &&
              dataSalida.getCD_NIVEL_2_LCA() != null) {

            // System_out.println("SrvBISelect: despues de Pregunta_Select_Nivel2 ");
            sQuery = Pregunta_Select_Nivel2();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_LCA().trim());
            st.setString(2, dataSalida.getCD_NIVEL_2_LCA().trim());
            rs = st.executeQuery();
            String desN2 = "";
            String desLN2 = "";

            while (rs.next()) {
              desN2 = rs.getString("DS_NIVEL_2");
              desLN2 = rs.getString("DSL_NIVEL_2");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN2 != null)) {
                desNivel2LCA = desLN2;
              }
              else {
                desNivel2LCA = desN2;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            // System_out.println("SrvBISelect: despues de Pregunta_Select_Nivel2 ");
          }

          //Zona Basica LCA
          if (dataSalida.getCD_NIVEL_1_LCA() != null &&
              dataSalida.getCD_NIVEL_2_LCA() != null &&
              dataSalida.getCD_ZBS_LCA() != null) {

            // System_out.println("SrvBISelect: antes de Pregunta_Select_ZonaBasica ");
            sQuery = Pregunta_Select_ZonaBasica();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_LCA().trim());
            st.setString(2, dataSalida.getCD_NIVEL_2_LCA().trim());
            st.setString(3, dataSalida.getCD_ZBS_LCA().trim());
            rs = st.executeQuery();
            String desZona = "";
            String desLZona = "";

            while (rs.next()) {
              desZona = rs.getString("DS_ZBS");
              desLZona = rs.getString("DSL_ZBS");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (rs.getString("DSL_ZBS") != null)) {
                desZBSLCA = desLZona;
              }
              else {
                desZBSLCA = desZona;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            // System_out.println("SrvBISelect: despues de Pregunta_Select_ZonaBasica ");
          }

          //nivel 1 LE
          if (dataSalida.getCD_NIVEL_1_LE() != null) {
            // System_out.println("SrvBISelect: antes de Pregunta_Select_Nivel1 ");
            sQuery = Pregunta_Select_Nivel1();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_LE().trim());
            rs = st.executeQuery();
            String desN1 = "";
            String desLN1 = "";

            while (rs.next()) {
              desN1 = rs.getString("DS_NIVEL_1");
              desLN1 = rs.getString("DSL_NIVEL_1");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN1 != null)) {
                desNivel1LE = desLN1;
              }
              else {
                desNivel1LE = desN1;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            // System_out.println("SrvBISelect: despues de Pregunta_Select_Nivel1 ");
          }

          //nivel2 LE
          if (dataSalida.getCD_NIVEL_1_LE() != null &&
              dataSalida.getCD_NIVEL_2_LE() != null) {

            // System_out.println("SrvBISelect: despues de Pregunta_Select_Nivel2 ");
            sQuery = Pregunta_Select_Nivel2();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_LE().trim());
            st.setString(2, dataSalida.getCD_NIVEL_2_LE().trim());
            rs = st.executeQuery();
            String desN2 = "";
            String desLN2 = "";

            while (rs.next()) {
              desN2 = rs.getString("DS_NIVEL_2");
              desLN2 = rs.getString("DSL_NIVEL_2");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN2 != null)) {
                desNivel2LE = desLN2;
              }
              else {
                desNivel2LE = desN2;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            // System_out.println("SrvBISelect: despues de Pregunta_Select_Nivel2 ");
          }

          //Zona Basica LE
          if (dataSalida.getCD_NIVEL_1_LE() != null &&
              dataSalida.getCD_NIVEL_2_LE() != null &&
              dataSalida.getCD_ZBS_LE() != null) {

            // System_out.println("SrvBISelect: antes de Pregunta_Select_ZonaBasica ");
            sQuery = Pregunta_Select_ZonaBasica();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataSalida.getCD_NIVEL_1_LE().trim());
            st.setString(2, dataSalida.getCD_NIVEL_2_LE().trim());
            st.setString(3, dataSalida.getCD_ZBS_LE().trim());
            rs = st.executeQuery();
            String desZona = "";
            String desLZona = "";

            while (rs.next()) {
              desZona = rs.getString("DS_ZBS");
              desLZona = rs.getString("DSL_ZBS");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (rs.getString("DSL_ZBS") != null)) {
                desZBSLE = desLZona;
              }
              else {
                desZBSLE = desZona;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            // System_out.println("SrvBISelect: despues de Pregunta_Select_ZonaBasica ");
          }

          //Se insertan las descripciones
          dataSalida.insert( (String) "DS_MUNCOL", desMunicipio);
          dataSalida.insert( (String) "CD_CACOL", codCadef);
          dataSalida.insert( (String) "DS_NIVEL_1_LCA", desNivel1LCA);
          dataSalida.insert( (String) "DS_NIVEL_2_LCA", desNivel2LCA);
          dataSalida.insert( (String) "DS_ZBS_LCA", desZBSLCA);
          dataSalida.insert( (String) "DS_NIVEL_1_LE", desNivel1LE);
          dataSalida.insert( (String) "DS_NIVEL_2_LE", desNivel2LE);
          dataSalida.insert( (String) "DS_ZBS_LE", desZBSLE);

          DataAlerta dataAlerSal = new DataAlerta();
          sQuery = Pregunta_Select_Alerta();
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2,
                    (new Integer(dataEntrada.getNM_ALERBRO().trim())).intValue());

          rs = st.executeQuery();
          while (rs.next()) {
            String FC_ALERBRO = formater.format(rs.getDate("FC_ALERBRO"));
            dataAlerSal.insert( (String) "CD_ANO", dataEntrada.getCD_ANO().trim());
            dataAlerSal.insert( (String) "NM_ALERBRO",
                               dataEntrada.getNM_ALERBRO().trim());
            dataAlerSal.insert( (String) "CD_SITALERBRO",
                               rs.getString("CD_SITALERBRO"));
            dataAlerSal.insert( (String) "FC_ALERBRO", FC_ALERBRO);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //ATENCION//////////////////////////
          /*
           �Que otras cosas habria que buscar para rellenar
           el resto de las pantallas para SIVE?
           Y para PISTA, �que hace falta?
           */
          listaSalida.addElement(dataAlerSal);
          listaSalida.addElement(dataSalida);

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

  public String Pregunta_Select_Brotes() {
    return ("select "
            + " FC_FSINPRIMC, FC_ISINPRIMC, "
            + " NM_PERINMIN, NM_PERINMAX, NM_PERINMED, "
            + " NM_ENFERMOS, FC_EXPOSICION, NM_INGHOSP, "
            + " NM_DEFUNCION, NM_EXPUESTOS, "
            + " CD_TRANSMIS, NM_NVACENF, "
            + " CD_OPE, FC_ULTACT, "
            + " NM_DCUACMIN, NM_DCUACMED, NM_DCUACMAX, "
            + " NM_NVACNENF, NM_VACNENF, NM_VACENF, "
            + " DS_OBSERV, CD_TIPOCOL, DS_NOMCOL, DS_DIRCOL, "
            + " CD_POSTALCOL, CD_PROVCOL, NM_ALERBRO, CD_ANO, "
            + " DS_BROTE, CD_GRUPO, FC_FECHAHORA, NM_MANIPUL, "
            + " CD_ZBS_LE, CD_NIVEL_2_LE, CD_TNOTIF, IT_RESCALC, "
            + " CD_MUNCOL, CD_NIVEL_1_LCA, DS_TELCOL, CD_NIVEL_1_LE, "
            + " CD_NIVEL_2_LCA, CD_ZBS_LCA, "
            + " IT_PERIN, IT_DCUAC, DS_NMCALLE, CD_TBROTE, DS_PISOCOL "
            + " from SIVE_BROTES where CD_ANO = ? AND NM_ALERBRO = ? ");
  }

  public String Pregunta_Select_Alerta() {
    return ("select "
            + " CD_SITALERBRO, FC_ALERBRO"
            + " from SIVE_ALERTA_BROTES where CD_ANO = ? AND NM_ALERBRO = ? ");
  }

  public String Pregunta_Select_Municipio() {
    return ("select DS_MUN "
            + " FROM SIVE_MUNICIPIO WHERE CD_PROV = ? AND CD_MUN = ? ");
  }

  public String Pregunta_Select_Nivel1() {
    return ("select "
            + " DS_NIVEL_1, DSL_NIVEL_1 "
            + " FROM  SIVE_NIVEL1_S  WHERE CD_NIVEL_1 = ? ");
  }

  public String Pregunta_Select_Nivel2() {
    return ("select "
            + " DS_NIVEL_2, DSL_NIVEL_2 "
            + " FROM  SIVE_NIVEL2_S  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?");
  }

  public String Pregunta_Select_ZonaBasica() {
    return ("select "
            + " DS_ZBS, DSL_ZBS "
            + " FROM  SIVE_ZONA_BASICA"
            + "  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND CD_ZBS = ? ");

  }
} //fin de la clase
