package param;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import sapp.StubSrvBD;

public class Pan_Param
    extends CPanel {

  int idioma;
  ResourceBundle res;

  //Modos de operaci�n de la ventana
  final int modoALTA = 0;
  final int modoMODIFICAR = 1;
  final int modoESPERA = 2;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //Constantes del panel
  final String imgLUPA = "images/Magnify.gif";
  final String imgALTA = "images/alta.gif";
  final String imgMODIFICACION = "images/modificacion.gif";
  final String imgBORRAR = "images/baja.gif";
  final String strSERVLETParam = "servlet/SrvParam";
  final String strSERVLETCat = "servlet/SrvCat";
  final String imgACEPTAR = "images/aceptar.gif";
  //Par�metros
  protected int modo = modoALTA;
  protected int modoAnterior; //Para saber si tenemos que volver al modo ALTA o MODIFICAR tras la selecci�n de un valor
  protected StubSrvBD stubClienteParam = null;
  protected DataParam dataParamAuxi = null;

  protected StubSrvBD stubClienteCat = null;
  protected CLista listaResultadoParam = null;

  final int minLongitud = 1;
  final int maxLongitud = 10;
  final int minEnteros = 1;
  final int maxEnteros = 10;
  final int minDecimales = 1;
  final int maxDecimales = 10;
  int Longitud;
  int Enteros;
  int Decimales;

  XYLayout xYLayout1 = new XYLayout();
  boolean isStandalone = false;
  Label lblPregunta = new Label();
  Label lblDescPregunta = new Label();
  TextField txtDescPregunta = new TextField();
  ButtonControl btnAnadir = new ButtonControl();
  CCampoCodigo txtCodPregunta = new CCampoCodigo();
  ButtonControl btnCtrlCodPregunta = new ButtonControl();

  Pan_ParamBotonesActionListener btnActionListener = new
      Pan_ParamBotonesActionListener(this);

  Label label1 = new Label();
  TextField txtNivel1 = new TextField();
  TextField txtDescCA = new TextField();
  Label label2 = new Label();
  TextField txtNivel1L = new TextField();
  TextField txtNivel2 = new TextField();
  Label label3 = new Label();
  Label label4 = new Label();
  TextField txtNivel2L = new TextField();
  Checkbox tramero = new Checkbox();
  TextField txtNivel3L = new TextField();
  Label lblNivel3 = new Label();
  Label label5 = new Label();
  TextField txtNivel3 = new TextField();
  Checkbox fuentes = new Checkbox();
  TextField txtIdLocal = new TextField();
  Label label6 = new Label();
  Label label7 = new Label();
  TextField txtServCNE = new TextField();

  public Pan_Param(CApp a) {
    URL u;
    CLista data = new CLista();
    CLista dataAuxi = new CLista();
    DataCat dataCatAuxi = null;

    try {
      setApp(a);
      res = ResourceBundle.getBundle("param.Res" + app.getIdioma());
      idioma = app.getIdioma();
      jbInit();
      u = app.getCodeBase();
      stubClienteParam = new StubSrvBD(new URL(app.getURL() + strSERVLETParam));
      CLista listaResultadoParam = new CLista(); // Va a ser una CList (hereda de Vector) en la que cada elem. tiene dos Strings
      listaResultadoParam = new CLista();
      listaResultadoParam.setState(CLista.listaNOVALIDA);

      stubClienteCat = new StubSrvBD(new URL(app.getURL() + strSERVLETCat));

      data.setLogin(this.app.getLogin());
      // Primero se lee el registro la tabla
      data.addElement(new DataParam("", "", "", "", "", "", "", "", "", "", "",
                                    "", ""));
      dataAuxi = (CLista) stubClienteParam.doPost(servletOBTENER_X_CODIGO, data);
      // Ahora escribimos los valores de los componentes
      dataParamAuxi = (DataParam) dataAuxi.firstElement();
      // Comprobamos que no est� vac�a
      if (dataParamAuxi.getCodCA() != null) {
        // Primero leemos el c�digo de la CCAA
        txtCodPregunta.setText(dataParamAuxi.getCodCA());
        // Y ahora el resto de los datos...
        txtDescPregunta.setText(dataParamAuxi.getWebURL());
        txtNivel1.setText(dataParamAuxi.getDsNivel1());
        txtNivel1L.setText(dataParamAuxi.getDslNivel1());
        txtNivel2.setText(dataParamAuxi.getDsNivel2());
        txtNivel2L.setText(dataParamAuxi.getDslNivel2());
        txtNivel3.setText(dataParamAuxi.getDsNivel3());
        txtNivel3L.setText(dataParamAuxi.getDslNivel3());
        tramero.setState(false);
        if (dataParamAuxi.getTramero().trim().compareTo("S") == 0) {
          //#// System_out.println("Dentro");
          tramero.setState(true);
        }
        fuentes.setState(false);
        if (dataParamAuxi.getFun().trim().compareTo("S") == 0) {
          //#// System_out.println("Dentro");
          fuentes.setState(true);
        }
        // Finalmente escribimos la descripci�n de la CCAA
        txtDescCA.setText(dataParamAuxi.getDescCCAA());
        //#// System_out.println("Tramero");
        //#// System_out.println(dataParamAuxi.getTramero());
        txtIdLocal.setText(dataParamAuxi.getIdLocal());
        txtServCNE.setText(dataParamAuxi.getURLcne());
        txtDescCA.setEditable(false);
        txtDescCA.setEnabled(false);
        btnCtrlCodPregunta.setFocusAware(false);
        txtCodPregunta.requestFocus();
      }
      modo = modoALTA;
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void jbInit() throws Exception {

    xYLayout1.setHeight(356);
    xYLayout1.setWidth(559);
    lblPregunta.setText(res.getString("lblPregunta.Text"));
    lblDescPregunta.setText(res.getString("lblDescPregunta.Text"));
    btnAnadir.setEnabled(false);
    btnAnadir.setLabel(res.getString("btnAnadir.Label"));
    btnCtrlCodPregunta.setActionCommand("BuscarCodPregunta");
    label1.setText(res.getString("label1.Text"));
    label2.setText(res.getString("label2.Text"));
    label3.setText(res.getString("label3.Text"));
    label4.setText(res.getString("label4.Text"));
    tramero.setLabel(res.getString("tramero.Label"));
    lblNivel3.setText(res.getString("lblNivel3.Text"));
    txtNivel3.setBackground(new Color(255, 255, 150));
    label5.setText(res.getString("label5.Text"));
    fuentes.setLabel(res.getString("fuentes.Label"));
    label6.setText(res.getString("label6.Text"));
    label7.setText(res.getString("label7.Text"));
    txtServCNE.setBackground(new Color(255, 255, 150));
    this.setLayout(xYLayout1);

    //A�adimos las list aqu� paa que se vean bien y superpongan a
    //otros componentes cuando sea necesario*************

    this.add(lblPregunta, new XYConstraints(24, 10, 128, -1));
    this.add(txtCodPregunta, new XYConstraints(201, 10, 61, -1));
    this.add(btnCtrlCodPregunta, new XYConstraints(275, 8, 30, 30));
    this.add(txtDescCA, new XYConstraints(319, 10, 210, -1));
    this.add(lblDescPregunta, new XYConstraints(24, 40, 78, -1));
    this.add(txtDescPregunta, new XYConstraints(201, 40, 328, -1));
    this.add(label1, new XYConstraints(24, 70, 95, -1));
    this.add(txtNivel1, new XYConstraints(201, 70, 328, -1));
    this.add(label2, new XYConstraints(24, 100, 171, -1));
    this.add(txtNivel1L, new XYConstraints(201, 100, 327, -1));
    this.add(txtNivel2, new XYConstraints(201, 130, 328, -1));
    this.add(label3, new XYConstraints(24, 130, 95, -1));
    this.add(label4, new XYConstraints(24, 160, 171, -1));
    this.add(txtNivel2L, new XYConstraints(201, 160, 328, -1));
    this.add(lblNivel3, new XYConstraints(24, 190, 95, -1));
    this.add(txtNivel3, new XYConstraints(201, 190, 328, -1));
    this.add(txtNivel3L, new XYConstraints(201, 220, 328, -1));
    this.add(label5, new XYConstraints(24, 220, 171, -1));
    this.add(txtIdLocal, new XYConstraints(201, 250, 328, -1));
    this.add(label6, new XYConstraints(24, 250, 171, -1));
    this.add(tramero, new XYConstraints(24, 315, -1, -1));
    this.add(fuentes, new XYConstraints(201, 315, -1, -1));
    this.add(btnAnadir, new XYConstraints(448, 315, -1, -1));
    this.add(label7, new XYConstraints(24, 281, -1, -1));
    this.add(txtServCNE, new XYConstraints(201, 281, 328, -1));

    btnCtrlCodPregunta.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnAnadir.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
    btnAnadir.setActionCommand("a�adir");
    btnCtrlCodPregunta.addActionListener(btnActionListener);
    btnAnadir.addActionListener(btnActionListener);
    txtCodPregunta.addFocusListener(new ModfocusAdapter(this));
    txtCodPregunta.setForeground(Color.black);
    txtCodPregunta.setBackground(new Color(255, 255, 150));
    txtCodPregunta.addKeyListener(new Pan_Param_txtCodPregunta_keyAdapter(this));
    txtDescCA.setForeground(Color.black);
    txtDescCA.setEditable(false);
    txtNivel1.setForeground(Color.black);
    txtNivel1.setBackground(new Color(255, 255, 150));
    txtNivel2.setForeground(Color.black);
    txtNivel2.setBackground(new Color(255, 255, 150));
    txtDescPregunta.setForeground(Color.black);
    txtDescPregunta.setBackground(new Color(255, 255, 150));
    Inicializar();
  }

  public void Inicializar() {

    switch (modo) {
      case modoALTA:

        // modo alta
        txtCodPregunta.setEnabled(true);
        btnCtrlCodPregunta.setEnabled(true);
        txtDescPregunta.setEnabled(true);
        btnAnadir.setEnabled(true);
        txtNivel1.setEnabled(true);
        txtNivel1L.setEnabled(true);
        txtNivel2.setEnabled(true);
        txtNivel2L.setEnabled(true);
        txtNivel3.setEnabled(true);
        txtNivel3L.setEnabled(true);
        tramero.setEnabled(true);
        fuentes.setEnabled(true);
        txtIdLocal.setEnabled(true);
        txtServCNE.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        txtCodPregunta.setEnabled(true);
        btnCtrlCodPregunta.setEnabled(true);
        //txtDescCA.setEnabled(false);
        txtDescPregunta.setEnabled(true);
        btnAnadir.setEnabled(false);
        txtNivel3.setEnabled(true);
        txtNivel3L.setEnabled(true);
        tramero.setEnabled(true);
        fuentes.setEnabled(true);
        txtIdLocal.setEnabled(true);
        txtServCNE.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        // modo espera
        txtCodPregunta.setEnabled(false);
        btnCtrlCodPregunta.setEnabled(false);
        //txtDescCA.setEnabled(false);
        txtDescPregunta.setEnabled(false);
        btnAnadir.setEnabled(false);
        //txtCodPregunta.setVisible(false);
        txtNivel1.setEnabled(false);
        txtNivel1L.setEnabled(false);
        txtNivel2.setEnabled(false);
        txtNivel2L.setEnabled(false);
        tramero.setEnabled(false);
        txtNivel3.setEnabled(false);
        txtNivel3L.setEnabled(false);
        fuentes.setEnabled(false);
        txtIdLocal.setEnabled(false);
        txtServCNE.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

  }

  void btnCtrlCodPregunta_actionPerformed(ActionEvent e) {
    DataCat data;

    modoAnterior = modo;
    modo = modoESPERA;
    Inicializar();

    CListaCat lista = new CListaCat(app,
                                    res.getString("msg1.Text"),
                                    stubClienteCat,
                                    strSERVLETCat,
                                    servletOBTENER_X_CODIGO +
                                    catalogo.Catalogo.catCCAA,
                                    servletOBTENER_X_DESCRIPCION +
                                    catalogo.Catalogo.catCCAA,
                                    servletSELECCION_X_CODIGO +
                                    catalogo.Catalogo.catCCAA,
                                    servletSELECCION_X_DESCRIPCION +
                                    catalogo.Catalogo.catCCAA);
    lista.show();
    data = (DataCat) lista.getComponente();
    if (data != null) {
      txtCodPregunta.setText(data.getCod());
      txtDescCA.setText(data.getDes());
    }
    modo = modoAnterior;
    Inicializar();

  }

  void btnAnadir_actionPerformed(ActionEvent e) {
    CLista data = null;
    CMessage dialogo = new CMessage(this.app, 2, res.getString("msg2.Text"));
    CMessage msgBox;
    String tramVal;
    String funVal;

    dialogo.setSize(250, 150);
    dialogo.show();
    //#// System_out.println(dialogo.getResponse());
    if (dialogo.getResponse() == true) {
      // Primero se debe comprobar si todos los datos son v�lidos
      if (this.isDataValid()) {
        try {
          tramVal = "N";
          if (tramero.getState()) {
            tramVal = "S";
          }
          funVal = "N";
          if (fuentes.getState()) {
            funVal = "S";
          }
          this.modo = modoESPERA;
          Inicializar();
          data = new CLista();
          data.setLogin(this.app.getLogin());
          // Primero se borra toda la tabla
          data.addElement(new DataParam("", "", "", "", "", "", "", "", "", "",
                                        "", "", ""));
          stubClienteParam.doPost(servletBAJA, data);
          // Despu�s se a�ade el �nico registro en CA_PARAMETROS
          data.removeAllElements();
          data.setLogin(this.app.getLogin());
          //#// System_out.println("a�adimox");
          data.addElement(new DataParam(txtCodPregunta.getText(),
                                        txtDescPregunta.getText(),
                                        txtNivel1.getText(), txtNivel1L.getText(),
                                        txtNivel2.getText(), txtNivel2L.getText(),
                                        txtNivel3.getText(),
                                        txtNivel3L.getText(), tramVal, "",
                                        funVal, txtIdLocal.getText(),
                                        txtServCNE.getText()));
          //#// System_out.println("llamamox");
          stubClienteParam.doPost(modoALTA, data);
          //#// System_out.println("ejecutamox");
          // pasa a modo modificar
          this.modo = modoALTA;
          // almacenamos los valores actuales
          tramVal = "N";
          if (tramero.getState()) {
            tramVal = "S";
          }
          dataParamAuxi = new DataParam(txtCodPregunta.getText(),
                                        txtDescPregunta.getText(),
                                        txtNivel1.getText(), txtNivel1L.getText(),
                                        txtNivel2.getText(), txtNivel2L.getText(),
                                        txtNivel3.getText(),
                                        txtNivel3L.getText(), tramVal, "",
                                        funVal, txtIdLocal.getText(),
                                        txtServCNE.getText());
        }
        catch (Exception ex) {
          this.modo = modoALTA;
          ex.printStackTrace();
          msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
          msgBox.show();
          msgBox = null;
        } //catch
        Inicializar();
      } //if is DataValid
    } //if SI
    else {
      if (tramero.getState()) {
        tramVal = "S";
      }
      txtCodPregunta.setText(dataParamAuxi.getCodCA());
      txtDescPregunta.setText(dataParamAuxi.getWebURL());
      txtNivel1.setText(dataParamAuxi.getDsNivel1());
      txtNivel1L.setText(dataParamAuxi.getDslNivel1());
      txtNivel2.setText(dataParamAuxi.getDsNivel2());
      txtNivel2L.setText(dataParamAuxi.getDslNivel2());
      if (dataParamAuxi.getTramero().trim().compareTo("S") == 0) {
        tramero.setState(true);
      }
      else {
        tramero.setState(false);
      }
      // Finalmente escribimos la descripci�n de la CCAA
      txtDescCA.setText(dataParamAuxi.getDescCCAA());
    }
    this.doLayout();
  }

  void vaciarTextoPantalla() {
    txtCodPregunta.setText("");
    txtDescPregunta.setText("");
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;
    if ( (
        (txtCodPregunta.getText().length() > 0) &&
        (txtDescPregunta.getText().length() > 0)
        && (txtDescCA.getText().length() > 0) &&
        (txtNivel1.getText().length() > 0)
        && (txtNivel2.getText().length() > 0) &&
        (txtNivel3.getText().length() > 0)
        && (txtServCNE.getText().length() > 0))) {
      b = true;
      if (txtCodPregunta.getText().length() > 2) {
        b = false;
        msg = res.getString("msg3.Text");
        txtCodPregunta.selectAll();
      }
      if (txtDescPregunta.getText().length() > 100) {
        b = false;
        msg = res.getString("msg3.Text");
        txtDescPregunta.selectAll();
      }
      if (txtServCNE.getText().length() > 100) {
        b = false;
        msg = res.getString("msg3.Text");
        txtServCNE.selectAll();
      }
      if (txtIdLocal.getText().length() > 30) {
        b = false;
        msg = res.getString("msg3.Text");
        txtIdLocal.selectAll();
      }
      if (txtNivel1.getText().length() > 30) {
        b = false;
        msg = res.getString("msg3.Text");
        txtNivel1.selectAll();
      }
      if (txtNivel1L.getText().length() > 30) {
        b = false;
        msg = res.getString("msg3.Text");
        txtNivel1L.selectAll();
      }
      if (txtNivel2.getText().length() > 30) {
        b = false;
        msg = res.getString("msg3.Text");
        txtNivel2.selectAll();
      }
      if (txtNivel2L.getText().length() > 30) {
        b = false;
        msg = res.getString("msg3.Text");
        txtNivel2L.selectAll();
      }
      if (txtNivel3.getText().length() > 30) {
        b = false;
        msg = res.getString("msg3.Text");
        txtNivel3.selectAll();
      }
      if (txtNivel3L.getText().length() > 30) {
        b = false;
        msg = res.getString("msg3.Text");
        txtNivel3L.selectAll();
      }

    }
    else {
      msg = res.getString("msg4.Text");
    }
    if (!b) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, msg);
      msgBox.show();
      msgBox = null;
    }
    return b;
  }

  void verMensajeAviso(String mensaje) {
    CMessage msgBox;
    msgBox = new CMessage(app, CMessage.msgAVISO, mensaje);
    msgBox.show();
    msgBox = null;
  }

  // perdida de foco de cajas de c�digos
  void focusLost() {

    // datos de envio
    DataCat data;
    CLista param = null;
    CMessage msg;
    String sDes;

    if (this.txtCodPregunta.getText().length() > 0) {
      try {
        param = new CLista();
        param.addElement(new DataCat(txtCodPregunta.getText()));

        // consulta en modo espera
        modo = modoESPERA;
        Inicializar();

        param = (CLista) stubClienteCat.doPost(servletSELECCION_X_CODIGO +
                                               catalogo.Catalogo.catCCAA, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataCat) param.elementAt(0);

          txtCodPregunta.setText(data.getCod());
          sDes = data.getDes();
          //#// System_out.println(sDes);
          if ( (this.app.getIdioma() != CApp.idiomaPORDEFECTO) && (data.getDesL() != null)) {
            sDes = data.getDesL();
          }
          txtDescCA.setText(sDes);

        }
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg5.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }

    // modo normal
    modo = modoALTA;
    Inicializar();
  }

  void txtCodPregunta_keyPressed(KeyEvent e) {
    txtDescCA.setText("");
  }
} //fin de clase

/**************************Trat de eventos*************************/

class Pan_ParamBotonesActionListener
    implements java.awt.event.ActionListener, Runnable {
  Pan_Param adaptee;
  ActionEvent e;

  Pan_ParamBotonesActionListener(Pan_Param adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    if (e.getActionCommand().equals("a�adir")) {
      adaptee.btnAnadir_actionPerformed(e);
    }
    else {
      adaptee.btnCtrlCodPregunta_actionPerformed(e);
    }
  }
}

class ModfocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_Param adaptee;

  ModfocusAdapter(Pan_Param adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost();
  }
}

class Pan_Param_txtCodPregunta_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_Param adaptee;

  Pan_Param_txtCodPregunta_keyAdapter(Pan_Param adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodPregunta_keyPressed(e);
  }
}

// lista de valores
class CListaCat
    extends CListaValores {

  public CListaCat(CApp a,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCat(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}
