package notutil;

import java.net.URL;

import capp.CApp;
import capp.CLista;
import sapp.StubSrvBD;

// objeto comunicador
public class Comunicador {

  public Comunicador() {
  }

  public static final CLista Communicate(CApp app,
                                         StubSrvBD stubCliente,
                                         int iMode,
                                         String servlet,
                                         CLista data) throws Exception {

    CLista lista = null;
    lista = new CLista();

    // perfil
    data.setPerfil(app.getPerfil());
    // login
    data.setLogin(app.getLogin());
    // idioma
    data.setIdioma(app.getIdioma());
    // Lortad
    data.setLortad(app.getParameter("LORTAD"));

    // filter
    data.setFilter("");

    stubCliente.setUrl(new URL(app.getURL() + servlet));
    /*
           if (servlet.equals("servlet/SrvIndivIM"))
           {
       notservlets.SrvIndivIM srv = new notservlets.SrvIndivIM();
       srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                              "jdbc:oracle:thin:@192.168.0.13:1521:SIVE",
                              "dba_edo",
                              "manager");
       lista = srv.doDebug(iMode,data);
           }
     */
    lista = (CLista) stubCliente.doPost(iMode, data);

    lista.trimToSize();
    data = null;
    return lista;
  }
}
