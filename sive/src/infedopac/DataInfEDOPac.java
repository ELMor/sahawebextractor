
package infedopac;

import java.io.Serializable;

public class DataInfEDOPac
    implements Serializable {

  public String DS_ENF = "";
  public String DS_SEMEPI = "";
  public String DS_CENTRO = "";
  public String EDAD = "";
  public String DS_ZBS = "";
  public String CLASIF = "";
  public String CD_N1 = "";
  public String CD_N2 = "";

  public int iPagina = 0;
  public boolean bInformeCompleto = false;

  public DataInfEDOPac(String cd_enf, String ds_sem, String ds_cen,
                       String edad, String ds_zbs, String clasif) {
    DS_ENF = cd_enf;
    DS_SEMEPI = ds_sem;
    DS_CENTRO = ds_cen;
    EDAD = edad;
    DS_ZBS = ds_zbs;
    CLASIF = clasif;
  }
}
