
package infedopac;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosEDOPac
    extends CApp {

  ResourceBundle res;

  public CasosEDOPac() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("infedopac.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    VerPanel(res.getString("msg2.Text"), new Pan_EDOPac(a), false);
    VerPanel(res.getString("msg2.Text"));
  }

}