package infedovarnot;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.SystemColor;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import cn.DataCN;
import cn.DataTA;
import nivel1.DataNivel1;
import notutil.UtilEDO;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import zbs.DataZBS2;

public class Pan_EDOvarNot
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();
  protected StubSrvBD stubNivAsis = null;

  public DataEDOvarNot paramC2;
  protected int modoOperacion = modoNORMAL;

  protected Pan_InfEDOvarNot informe;
  CLista datan = null;
  CLista listaNivAsis = null;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLET_NIV2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  //E final String strSERVLETEnfermedad = "servlet/SrvEnfCie";
  final String strSERVLETEnfermedad = "servlet/SrvEnfVigi";
  //E
  final String strSERVLETcn = "servlet/SrvCN";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;
  public static final int servletSELECCION_NIVASIS_X_CODIGO = 7;
  public static final int servletOBTENER_NIVASIS_X_CODIGO = 8;
  public static final int servletSELECCION_NIVASIS_X_DESCRIPCION = 9;
  public static final int servletOBTENER_NIVASIS_X_DESCRIPCION = 10;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblEnfermedad = new Label();
  TextField txtEnfermedad = new CCampoCodigo();
  ButtonControl btnEnfermedad = new ButtonControl();
  TextField txtDesEnfermedad = new TextField();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  //focusAdapter txtFocusAdapter = new focusAdapter(this);
  //textAdapter txtTextAdapter = new textAdapter(this);
  //actionListener btnActionListener = new actionListener(this);
  Label lblArea = new Label();
  TextField txtArea = new CCampoCodigo();
  ButtonControl btnArea = new ButtonControl();
  TextField txtDesArea = new TextField();
  Label lblNivAsis = new Label();
  CheckboxGroup checkboxGroup1 = new CheckboxGroup();
  TextField txtNiv1 = new CCampoCodigo();
  ButtonControl btnNiv1 = new ButtonControl();
  TextField txtDesNiv1 = new TextField();
  ButtonControl btnNiv2 = new ButtonControl();
  TextField txtDesNiv2 = new TextField();
  TextField txtNiv2 = new CCampoCodigo();
  TextField txtDistrito = new CCampoCodigo();
  ButtonControl btnDistrito = new ButtonControl();
  TextField txtDesDistrito = new TextField();
  Label lblDistrito = new Label();

  public Pan_EDOvarNot(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("infedovarnot.Res" + a.getIdioma());
      informe = new Pan_InfEDOvarNot(a);
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      this.txtArea.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesArea.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtDistrito.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDistrito.setText(this.app.getDS_NIVEL2_DEFECTO());

      informe.setEnabled(false);

      stubNivAsis = new StubSrvBD(new URL(this.app.getURL() + strSERVLETcn));

      // ahora leemos la tabla de nivel asistencial
      datan = new CLista();
      datan.setFilter("");
      datan.setState(CLista.listaVACIA);
      DataCN datosAuxil = null;

      datosAuxil = new DataCN("", "", "", "", "", "", "", "", "", "", "", "",
                              "", "", "", "", "");
      datan.addElement(datosAuxil);

      listaNivAsis = new CLista();
      listaNivAsis = (CLista) stubNivAsis.doPost(
          servletSELECCION_NIVASIS_X_CODIGO, datan);

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(378);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    lblArea.setText(res.getString("lblArea.Text"));
    txtArea.addFocusListener(new Pan_EDOnoMA_txtArea_focusAdapter(this));
    txtArea.addKeyListener(new Pan_EDOnoMA_txtArea_keyAdapter(this));

    // gestores de eventos
    /*btnEnfermedad.addActionListener(btnActionListener);
         btnLimpiar.addActionListener(btnActionListener);
         btnInforme.addActionListener(btnActionListener);
         txtEnfermedad.addTextListener(txtTextAdapter);
         txtEnfermedad.addFocusListener(txtFocusAdapter);
     */

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnEnfermedad.setActionCommand("buscarEnfermedad");
    txtDesEnfermedad.setEditable(false);
    txtDesEnfermedad.setEnabled(false); /*E*/
    txtDesArea.setEditable(false);
    txtDesArea.setEnabled(false); /*E*/
    lblNivAsis.setText(res.getString("lblNivAsis.Text"));
    txtNiv1.setBackground(new Color(255, 255, 150));
    txtNiv1.addFocusListener(new Pan_EDOvarNot_txtNiv1_focusAdapter(this));
    txtNiv1.addKeyListener(new Pan_EDOvarNot_txtNiv1_keyAdapter(this));
    txtDesNiv1.setEditable(false);
    txtDesNiv1.setEnabled(false); /*E*/
    txtDesNiv2.setEditable(false);
    txtDesNiv2.setEnabled(false); /*E*/
    txtNiv2.setBackground(new Color(255, 255, 150));
    txtDistrito.addFocusListener(new Pan_EDOvarNot_txtDistrito_focusAdapter(this));
    txtDistrito.addKeyListener(new Pan_EDOvarNot_txtDistrito_keyAdapter(this));
    btnDistrito.setActionCommand("buscarArea");
    btnDistrito.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    txtDesDistrito.setEditable(false);
    txtDesDistrito.setEnabled(false); /*E*/
    lblDistrito.setText(res.getString("lblDistrito.Text"));
    btnDistrito.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnDistrito.addActionListener(new Pan_EDOvarNot_btnDistrito_actionAdapter(this));
    //btnDistrito.addActionListener(new Pan_EDOnoMA_btnArea1_actionAdapter(this));
    //txtDistrito.addFocusListener(new Pan_EDOnoMA_txtArea1_focusAdapter(this));
    //txtDistrito.addKeyListener(new Pan_EDOnoMA_txtArea1_keyAdapter(this));
    txtNiv2.addFocusListener(new Pan_EDOvarNot_txtNiv2_focusAdapter(this));
    txtNiv2.addKeyListener(new Pan_EDOvarNot_txtNiv2_keyAdapter(this));
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtEnfermedad.setName("txtCodEquNot");
    txtEnfermedad.setBackground(SystemColor.control);
    txtEnfermedad.addFocusListener(new Pan_EDOnoMA_txtEnfermedad_focusAdapter(this));
    txtEnfermedad.addKeyListener(new Pan_EDOnoMA_txtEnfermedad_keyAdapter(this));
    //btnArea.addActionListener(btnActionListener);
    btnArea.setActionCommand("buscarArea");
    btnArea.setImageURL(new URL(app.getCodeBase(), imgLUPA));

    lblHasta.setText(res.getString("lblHasta.Text"));
    lblEnfermedad.setText(res.getString("lblEnfermedad.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 5, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 5, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 35, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 35, -1, -1));
    this.add(lblEnfermedad, new XYConstraints(26, 65, 110, -1));
    this.add(txtEnfermedad, new XYConstraints(143, 65, 77, -1));
    this.add(btnEnfermedad, new XYConstraints(225, 65, -1, -1));
    this.add(txtDesEnfermedad, new XYConstraints(254, 65, 287, -1));
    this.add(lblArea, new XYConstraints(26, 95, -1, -1));
    this.add(txtArea, new XYConstraints(143, 95, 77, -1));
    this.add(btnArea, new XYConstraints(225, 95, -1, -1));
    this.add(txtDesArea, new XYConstraints(254, 95, 287, -1));
    this.add(lblDistrito, new XYConstraints(26, 125, -1, -1));
    this.add(txtDistrito, new XYConstraints(143, 125, 77, -1));
    this.add(btnDistrito, new XYConstraints(225, 125, -1, -1));
    this.add(txtDesDistrito, new XYConstraints(254, 125, 287, -1));
    this.add(lblNivAsis, new XYConstraints(26, 155, 194, -1));
    this.add(txtNiv1, new XYConstraints(254, 155, 55, -1));
    this.add(btnNiv1, new XYConstraints(315, 155, -1, -1));
    this.add(txtDesNiv1, new XYConstraints(359, 155, 182, -1));
    this.add(txtNiv2, new XYConstraints(254, 185, 55, -1));
    this.add(btnNiv2, new XYConstraints(315, 185, -1, -1));
    this.add(txtDesNiv2, new XYConstraints(359, 185, 182, -1));

    this.add(btnLimpiar, new XYConstraints(389, 215, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 215, -1, -1));
    //this.add(checkboxGroup1);

    btnArea.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnArea.addActionListener(new Pan_EDOnoMA_btnArea_actionAdapter(this));
    btnEnfermedad.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnEnfermedad.addActionListener(new Pan_EDOnoMA_btnEnfermedad_actionAdapter(this));
    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnLimpiar.addActionListener(new Pan_EDOnoMA_btnLimpiar_actionAdapter(this));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));
    btnInforme.addActionListener(new Pan_EDOnoMA_btnInforme_actionAdapter(this));
    btnNiv1.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnNiv1.addActionListener(new Pan_EDOvarNot_btnNiv1_actionAdapter(this));
    btnNiv2.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnNiv2.addActionListener(new Pan_EDOvarNot_btnNiv2_actionAdapter(this));

    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;
    String msg = "";
    CMessage msgBox;
    UtilEDO util = new UtilEDO(); //Para comparar fechas
    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0) ||
        (txtDesNiv1.getText().length() == 0) ||
        (txtDesNiv2.getText().length() == 0)) {
      bDatosCompletos = false;
      msg = res.getString("msg3.Text");
    }

    else if (util.fecha1MayorqueFecha2(fechasDesde.txtFecSem.getText(),
                                       fechasHasta.txtFecSem.getText())) {
      /*
            Enumeration en= res.getKeys();
            while (en.hasMoreElements()) {
              String sigte= (String)(en.nextElement());
              // System_out.println("Clave  "+sigte);
            }
       */

      bDatosCompletos = false;
      msg = res.getString("msg16.Text");
    }
    else if (txtNiv1.getText().compareTo(txtNiv2.getText()) == 0) {
      bDatosCompletos = false;
      msg = res.getString("msg4.Text");
    }

    if (!bDatosCompletos) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, msg);
      msgBox.show();
      msgBox = null;
    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtEnfermedad.setEnabled(true);
        btnEnfermedad.setEnabled(true);
        //txtDesEnfermedad.setEnabled(true); /*E*/
        txtDesEnfermedad.setEditable(false);

        txtArea.setEnabled(true);
        btnArea.setEnabled(true);
        //txtDesArea.setEnabled(true); /*E*/
        txtDesArea.setEditable(false);

        if (this.txtDesArea.getText().length() > 0) {
          btnDistrito.setEnabled(true);
          txtDistrito.setEnabled(true);
        }
        else {
          btnDistrito.setEnabled(false);
          txtDistrito.setEnabled(false);
        }

        //txtDesDistrito.setEnabled(true); /*E*/
        txtDesDistrito.setEditable(false);

        txtNiv1.setEnabled(true);
        //txtDesNiv1.setEnabled(true); /*E*/
        txtNiv2.setEnabled(true);
        //txtDesNiv2.setEnabled(true); /*E*/

        btnNiv1.setEnabled(true);
        btnNiv2.setEnabled(true);

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtEnfermedad.setEnabled(false);
        btnEnfermedad.setEnabled(false);
        //txtDesEnfermedad.setEnabled(false); /*E*/
        txtDesEnfermedad.setEditable(false);

        txtArea.setEnabled(false);
        btnArea.setEnabled(false);
        //txtDesArea.setEnabled(false); /*E*/
        txtDesArea.setEditable(false);

        txtDistrito.setEnabled(false);
        btnDistrito.setEnabled(false);
        //txtDesDistrito.setEnabled(false); /*E*/
        txtDesDistrito.setEditable(false);

        txtNiv1.setEnabled(false);
        //txtDesNiv1.setEnabled(false); /*E*/
        txtNiv2.setEnabled(false);
        //txtDesNiv2.setEnabled(false); /*E*/

        btnNiv1.setEnabled(false);
        btnNiv2.setEnabled(false);

        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    modoOperacion = modoESPERA;
    Inicializar();

    txtEnfermedad.setText("");
    txtDesEnfermedad.setText("");
    txtArea.setText("");
    txtDesArea.setText("");
    txtDistrito.setText("");
    txtDesDistrito.setText("");
    txtNiv1.setText("");
    txtDesNiv1.setText("");
    txtNiv2.setText("");
    txtDesNiv2.setText("");

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnInforme_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC2 = new DataEDOvarNot();

    if (isDataValid()) {
      modoOperacion = modoESPERA;
      Inicializar();

      if (!txtDesEnfermedad.getText().equals("")) {
        paramC2.sDsEnfermedad = txtDesEnfermedad.getText();
        paramC2.sCdEnfermedad = txtEnfermedad.getText();
      }

      if (!txtDesArea.getText().equals("")) {
        paramC2.sDsArea = txtDesArea.getText();
        paramC2.sCdArea = txtArea.getText();

        if (!txtDesDistrito.getText().equals("")) {
          paramC2.sDsDistrito = txtDesDistrito.getText();
          paramC2.sCdDistrito = txtDistrito.getText();
        }

      }

      paramC2.sAnoDesde = fechasDesde.txtAno.getText();
      paramC2.sAnoHasta = fechasHasta.txtAno.getText();

      if (fechasDesde.txtCodSem.getText().length() == 1) {
        paramC2.sSemDesde = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        paramC2.sSemDesde = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        paramC2.sSemHasta = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        paramC2.sSemHasta = fechasHasta.txtCodSem.getText();

      }
      paramC2.sCdNiv1 = txtNiv1.getText();
      paramC2.sCdNiv2 = txtNiv2.getText();
      paramC2.sDsNiv1 = txtDesNiv1.getText();
      paramC2.sDsNiv2 = txtDesNiv2.getText();
      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.datosPar = paramC2;
      //E //#// System_out.println("1");
      //E //#// System_out.println(paramC2.sAnoDesde);
      //E //#// System_out.println(paramC2.sSemDesde);
      //E //#// System_out.println(paramC2.sAnoHasta);
      //E //#// System_out.println(paramC2.sSemHasta);
      if (informe.GenerarInforme()) {
        informe.show();
      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
  }

  void btnEnfermedad_actionPerformed(ActionEvent e) {
    //E DataEnfCie data = null;
    DataCat data = null;

    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaEnf lista = new CListaEnf(this.app,
                                      "Selecci�n de Enfermedad",
                                      stubCliente,
                                      strSERVLETEnfermedad,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
      lista.show();
      //E data = (DataEnfCie) lista.getComponente();
      data = (DataCat) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtEnfermedad.setText(data.getCod());
      txtDesEnfermedad.setText(data.getDes());
      //txtEnfermedad.addTextListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnArea_actionPerformed(ActionEvent e) {
    DataNivel1 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaArea lista = new CListaArea(this.app,
                                        res.getString("msg5.Text"),
                                        stubCliente,
                                        strSERVLETNivel1,
                                        servletOBTENER_X_CODIGO,
                                        servletOBTENER_X_DESCRIPCION,
                                        servletSELECCION_X_CODIGO,
                                        servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtArea.setText(data.getCod());
      txtDesArea.setText(data.getDes());
      txtDistrito.setText("");
      txtDesDistrito.setText("");

    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void txtEnfermedad_keyPressed(KeyEvent e) {
    txtDesEnfermedad.setText("");
  }

  void txtArea_keyPressed(KeyEvent e) {
    txtDesArea.setText("");
    txtDistrito.setText("");
    txtDesDistrito.setText("");
    btnDistrito.setEnabled(false);
    txtDistrito.setEnabled(false);
  }

  void txtEnfermedad_focusLost(FocusEvent e) {
    // datos de envio
    //DataEnfCie enfcie;
    DataCat enfcie;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // gestion de datos
    param = new CLista();
    param.setIdioma(app.getIdioma());
    //E param.addElement(new DataEnfCie(txtEnfermedad.getText()));
    param.addElement(new DataCat(txtEnfermedad.getText()));
    //E
    strServlet = strSERVLETEnfermedad;
    modoServlet = servletOBTENER_X_CODIGO;

    // busca el item
    if (txtEnfermedad.getText().length() > 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          //enfcie = (DataEnfCie) param.firstElement();
          enfcie = (DataCat) param.firstElement();
          txtEnfermedad.setText(enfcie.getCod());
          txtDesEnfermedad.setText(enfcie.getDes());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  void txtArea_focusLost(FocusEvent e) {
    // datos de envio
    DataNivel1 nivel1;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // gestion de datos
    param = new CLista();
    param.setIdioma(app.getIdioma());
    param.addElement(new DataNivel1(txtArea.getText()));
    strServlet = strSERVLETNivel1;
    modoServlet = servletOBTENER_X_CODIGO;

    // busca el item
    if (txtArea.getText().length() > 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          nivel1 = (DataNivel1) param.firstElement();
          txtArea.setText(nivel1.getCod());
          txtDesArea.setText(nivel1.getDes());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  void btnNiv1_actionPerformed(ActionEvent e) {
    DataTA data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaNivAsis lista = new CListaNivAsis(this.app,
                                              res.getString("msg7.Text"),
                                              stubNivAsis,
                                              strSERVLETcn,
                                              servletOBTENER_NIVASIS_X_CODIGO,
          servletOBTENER_NIVASIS_X_DESCRIPCION,
          servletSELECCION_NIVASIS_X_CODIGO,
          servletSELECCION_NIVASIS_X_DESCRIPCION);
      lista.show();
      data = (DataTA) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtNiv1.setText(data.getCodigo());
      txtDesNiv1.setText(data.getDesc());

    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnNiv2_actionPerformed(ActionEvent e) {
    DataTA data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaNivAsis lista = new CListaNivAsis(this.app,
                                              res.getString("msg7.Text"),
                                              stubNivAsis,
                                              strSERVLETcn,
                                              servletOBTENER_NIVASIS_X_CODIGO,
          servletOBTENER_NIVASIS_X_DESCRIPCION,
          servletSELECCION_NIVASIS_X_CODIGO,
          servletSELECCION_NIVASIS_X_DESCRIPCION);
      lista.show();
      data = (DataTA) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtNiv2.setText(data.getCodigo());
      txtDesNiv2.setText(data.getDesc());

    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void txtNiv1_keyPressed(KeyEvent e) {
    txtDesNiv1.setText("");
  }

  void txtNiv2_keyPressed(KeyEvent e) {
    txtDesNiv2.setText("");
  }

  void txtNiv1_focusLost(FocusEvent e) {
    // datos de envio
    DataCN centro;
    DataTA centroV;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // busca el item
    if (txtNiv1.getText().length() > 0) {
      // gestion de datos
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCN(txtNiv1.getText().trim()));
      strServlet = strSERVLETcn;
      modoServlet = servletOBTENER_NIVASIS_X_CODIGO;

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          centroV = (DataTA) param.firstElement();
          txtNiv1.setText(centroV.getCodigo());
          txtDesNiv1.setText(centroV.getDesc());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  void txtNiv2_focusLost(FocusEvent e) {
    // datos de envio
    DataCN centro;
    DataTA centroV;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // busca el item
    if (txtNiv2.getText().length() > 0) {
      // gestion de datos
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCN(txtNiv2.getText()));
      strServlet = strSERVLETcn;
      modoServlet = servletOBTENER_NIVASIS_X_CODIGO;

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          centroV = (DataTA) param.firstElement();
          txtNiv2.setText(centroV.getCodigo());
          txtDesNiv2.setText(centroV.getDesc());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  void txtDistrito_keyPressed(KeyEvent e) {
    txtDesDistrito.setText("");
  }

  void txtDistrito_focusLost(FocusEvent e) {
    // datos de envio
    DataZBS2 nivel2;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // gestion de datos
    param = new CLista();
    param.addElement(new DataZBS2(txtArea.getText(), txtDistrito.getText(), "",
                                  ""));
    strServlet = strSERVLET_NIV2;
    modoServlet = servletOBTENER_NIV2_X_CODIGO;

    // busca el item
    if (txtDistrito.getText().length() > 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          nivel2 = (DataZBS2) param.firstElement();
          txtDistrito.setText(nivel2.getNiv2());
          txtDesDistrito.setText(nivel2.getDes());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  void btnDistrito_actionPerformed(ActionEvent e) {

    DataZBS2 data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIV2));
      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg8.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLET_NIV2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        txtDesDistrito.setText(data.getDes());
        txtDistrito.setText(data.getNiv2());
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    Inicializar();
  }

} //clase

////////////////////// Clases para listas

// lista de valores
class CListaArea
    extends CListaValores {
  protected Pan_EDOvarNot panel;

  public CListaArea(CApp app,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(app,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaEnf
    extends CListaValores {

  public CListaEnf(CApp a,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    //E return new DataEnfCie(s);
    return new DataCat(s);
  }

  public String getCodigo(Object o) {
    //E return ( ((DataEnfCie)o).getCod() );
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    //E return ( ((DataEnfCie)o).getDes() );
    return ( ( (DataCat) o).getDes());
  }
}

// lista de valores
class CListaNivAsis
    extends CListaValores {

  public CListaNivAsis(CApp a,
                       String title,
                       StubSrvBD stub,
                       String servlet,
                       int obtener_x_codigo,
                       int obtener_x_descricpcion,
                       int seleccion_x_codigo,
                       int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCN(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataTA) o).getCodigo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataTA) o).getDesc());
  }
}

class Pan_EDOnoMA_btnEnfermedad_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOvarNot adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnEnfermedad_actionAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnEnfermedad_actionPerformed(e);
  }
}

class Pan_EDOnoMA_btnArea_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOvarNot adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnArea_actionAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnArea_actionPerformed(e);
  }
}

class Pan_EDOnoMA_txtEnfermedad_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOvarNot adaptee;

  Pan_EDOnoMA_txtEnfermedad_keyAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtEnfermedad_keyPressed(e);
  }
}

class Pan_EDOnoMA_txtArea_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOvarNot adaptee;

  Pan_EDOnoMA_txtArea_keyAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtArea_keyPressed(e);
  }
}

class Pan_EDOnoMA_txtEnfermedad_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_EDOvarNot adaptee;
  FocusEvent e;

  Pan_EDOnoMA_txtEnfermedad_focusAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtEnfermedad_focusLost(e);
  }
}

class Pan_EDOnoMA_txtArea_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_EDOvarNot adaptee;
  FocusEvent e;

  Pan_EDOnoMA_txtArea_focusAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtArea_focusLost(e);
  }
}

class Pan_EDOnoMA_btnLimpiar_actionAdapter
    implements java.awt.event.ActionListener {
  Pan_EDOvarNot adaptee;

  Pan_EDOnoMA_btnLimpiar_actionAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnLimpiar_actionPerformed(e);
  }
}

class Pan_EDOnoMA_btnInforme_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOvarNot adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnInforme_actionAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnInforme_actionPerformed(e);
  }
}

class Pan_EDOvarNot_btnNiv1_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOvarNot adaptee;
  ActionEvent e;

  Pan_EDOvarNot_btnNiv1_actionAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnNiv1_actionPerformed(e);
  }
}

class Pan_EDOvarNot_btnNiv2_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOvarNot adaptee;
  ActionEvent e;

  Pan_EDOvarNot_btnNiv2_actionAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnNiv2_actionPerformed(e);
  }
}

class Pan_EDOvarNot_txtNiv1_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOvarNot adaptee;

  Pan_EDOvarNot_txtNiv1_keyAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtNiv1_keyPressed(e);
  }
}

class Pan_EDOvarNot_txtNiv2_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOvarNot adaptee;

  Pan_EDOvarNot_txtNiv2_keyAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtNiv2_keyPressed(e);
  }
}

class Pan_EDOvarNot_txtNiv1_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_EDOvarNot adaptee;
  FocusEvent e;

  Pan_EDOvarNot_txtNiv1_focusAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtNiv1_focusLost(e);
  }
}

class Pan_EDOvarNot_txtNiv2_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_EDOvarNot adaptee;
  FocusEvent e;

  Pan_EDOvarNot_txtNiv2_focusAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtNiv2_focusLost(e);
  }

}

class Pan_EDOvarNot_txtDistrito_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOvarNot adaptee;

  Pan_EDOvarNot_txtDistrito_keyAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtDistrito_keyPressed(e);
  }
}

class Pan_EDOvarNot_txtDistrito_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_EDOvarNot adaptee;
  FocusEvent e;

  Pan_EDOvarNot_txtDistrito_focusAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtDistrito_focusLost(e);
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected Pan_EDOvarNot panel;

  public CListaZBS2(Pan_EDOvarNot p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtArea.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

class Pan_EDOvarNot_btnDistrito_actionAdapter
    implements java.awt.event.ActionListener {
  Pan_EDOvarNot adaptee;

  Pan_EDOvarNot_btnDistrito_actionAdapter(Pan_EDOvarNot adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnDistrito_actionPerformed(e);
  }
}
