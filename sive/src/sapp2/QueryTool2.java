package sapp2;

import java.sql.Connection;
import java.util.Enumeration;
import java.util.Vector;

public class QueryTool2
    extends QueryTool {
  /**
   * QueryTools adicionales (nombre/querytool)
   */
  protected Vector adicQueryTool = new Vector();

  /**
   * Columnas que utiliza para traer la informaci�n (nombre/vector de columnas)
   */
  protected Vector adicColumns = new Vector();

  /**
   * Constructor
   */
  public QueryTool2(String tabla,
                    Data types,
                    Data values,
                    Data wtypes,
                    Data wvalues,
                    Data wop,
                    Vector order,
                    Data wsubquery,
                    Data wtypevalue,
                    Vector adicQT,
                    Vector adicCol) {
    super(tabla,
          types,
          values,
          wtypes,
          wvalues,
          wop,
          order,
          wsubquery,
          wtypevalue);
    adicQueryTool = adicQT;
    adicColumns = adicCol;
  }

  public QueryTool2() {
    super();
  }

  /**
   * AQ�ade una QT
   *
   * @param qt    QueryTool
   */
  public void addQueryTool(QueryTool qt) {
    adicQueryTool.addElement(qt);
  }

  /**
   * Graba la estructura de tipos de la cla�sula where
   * dependiente de cada registro
   *
   * @param cols  Columnas
   */
  public void addColumnsQueryTool(Data cols) {
    adicColumns.addElement(cols);
  }

  /**
   * Realiza sentencias de tipo SELECT sin tramar
   *
   * @param Connection conexi�n JDBC
   * @return Lista con los resultados
   */
  public Lista doSelect(Connection con) throws Exception {

    // colecciones de campos
    Enumeration enum1 = null;
    Enumeration enum2 = null;

    // buffers
    int i = 1;
    int j = 1;
    Data line = null;
    QueryTool qt = null;
    Data dtColumn = null;
    String column = null;
    Data lineAdic = null;

    // lista de con el resultado
    Lista snapShot = super.doSelect(con);

    // listas para los datos adicionales
    Lista datAdic = new Lista();

    // a�ado la informaci�n adicional
    for (i = 0; i < snapShot.size(); i++) {
      line = (Data) snapShot.elementAt(i);

      // recorre las QT adicionales
      for (j = 0; j < adicQueryTool.size(); j++) {

        qt = (QueryTool) adicQueryTool.elementAt(j);
        dtColumn = (Data) adicColumns.elementAt(j);

        // completa la cla�sula WHERE
        enum2 = dtColumn.elements();
        for (enum1 = dtColumn.keys(); enum1.hasMoreElements(); ) {
          column = (String) enum1.nextElement();
          qt.putWhereType(column, ( (Integer) enum2.nextElement()).intValue());
          qt.putWhereValue(column, line.getString(column));
          qt.putOperator(column, "=");
        }

        datAdic = (Lista) qt.doSelect(con);

        // a�ade las columnas nuevas
        if (datAdic.size() > 0) {
          lineAdic = (Data) datAdic.elementAt(0);
          dtColumn = qt.getDatType();

          for (enum1 = dtColumn.keys(); enum1.hasMoreElements(); ) {
            column = (String) enum1.nextElement();
            line.put(column, lineAdic.getString(column));
          }
        }
      }
    }

    // devuelve la matriz de salida
    return snapShot;
  }

  /**
   * Realiza sentencias de tipo SELECT tramando
   *
   * @param sTrama campo de la trama
   * @param sValorTrama valor de la trama
   * @param sOperadorTrama operador de la trama
   * @param sOrdenTrama cla�sula DESC si fuese necesario
   * @param Connection conexi�n JDBC
   * @return Lista con los resultados
   */
  public Lista doGetPage(String sTrama,
                         String sValorTrama,
                         String sOperadorTrama,
                         String sOrdenTrama,
                         Connection con) throws Exception {

    // colecciones de campos
    Enumeration enum1 = null;
    Enumeration enum2 = null;

    // buffers
    int i = 1;
    int j = 1;
    Data line = null;
    QueryTool qt = null;
    Data dtColumn = null;
    String column = null;
    Data lineAdic = null;

    // lista de con el resultado
    Lista snapShot = super.doGetPage(sTrama, sValorTrama, sOperadorTrama,
                                     sOrdenTrama, con);

    // listas para los datos adicionales
    Lista datAdic = new Lista();

    // a�ado la informaci�n adicional
    for (i = 0; i < snapShot.size(); i++) {
      line = (Data) snapShot.elementAt(i);

      // recorre las QT adicionales
      for (j = 0; j < adicQueryTool.size(); j++) {

        qt = (QueryTool) adicQueryTool.elementAt(j);
        dtColumn = (Data) adicColumns.elementAt(j);

        // completa la cla�sula WHERE
        enum2 = dtColumn.elements();
        for (enum1 = dtColumn.keys(); enum1.hasMoreElements(); ) {
          column = (String) enum1.nextElement();
          qt.putWhereType(column, ( (Integer) enum2.nextElement()).intValue());
          qt.putWhereValue(column, line.getString(column));
          qt.putOperator(column, "=");
        }

        datAdic = (Lista) qt.doSelect(con);

        // a�ade las columnas nuevas
        if (datAdic.size() > 0) {
          lineAdic = (Data) datAdic.elementAt(0);
          dtColumn = qt.getDatType();

          for (enum1 = dtColumn.keys(); enum1.hasMoreElements(); ) {
            column = (String) enum1.nextElement();
            line.put(column, lineAdic.getString(column));
          }
        }
      }
    }

    // devuelve la matriz de salida
    return snapShot;
  }
}
