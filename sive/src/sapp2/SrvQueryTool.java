package sapp2;

import java.sql.SQLException;
import java.util.StringTokenizer;

/**
 * Servlet que implementa todas las operaciones de la clase SQuery.
 *
 * @autor LSR
 * @version 1.0
 */

/**
 * Modificacion para admitir el bloqueo:  dos nuevos modos
 *
 * @autor CGA
 * @version 2.0
 */

public class SrvQueryTool
    extends DBServlet {

  final protected int servletDO_SELECT = 1;
  final protected int servletDO_GETPAGE = 2;
  final protected int servletDO_INSERT = 3;
  final protected int servletDO_UPDATE = 4;
  final protected int servletDO_DELETE = 5;
  final protected int servletDO_UPDATE_BLO = 10006;
  final protected int servletDO_DELETE_BLO = 10007;
  final protected int servletDO_GETPAGE_DESC = 8;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // vector con el resultado
    Lista vResulset = new Lista();

    // query tool
    QueryTool queryTool = null;

    try {
      queryTool = (QueryTool) vParametros.elementAt(0);

      con.setAutoCommit(true);

      switch (opmode) {
        case servletDO_SELECT:

          vResulset = queryTool.doSelect(con);
          break;

        case servletDO_GETPAGE:
          vResulset = queryTool.doGetPage(vParametros.getCampoTrama(),
                                          vParametros.getValorTrama(),
                                          ">",
                                          "",
                                          con);
          break;

        case servletDO_GETPAGE_DESC:
          vResulset = queryTool.doGetPage(vParametros.getCampoTrama(),
                                          vParametros.getValorTrama(),
                                          "<",
                                          "DESC",
                                          con);
          break;

        case servletDO_INSERT:
          vResulset = queryTool.doInsert(con);
          break;

        case servletDO_UPDATE_BLO:
        case servletDO_UPDATE:
          vResulset = queryTool.doUpdate(con);
          break;

        case servletDO_DELETE_BLO:
        case servletDO_DELETE:
          vResulset = queryTool.doDelete(con);
          break;
      }
    }
    catch (SQLException e1) {

      // traza el error
      trazaLog(e1);

      // filtramos el error, para poder analizarlo posteriormente.
      String elError = e1.toString();
      String muestra = null;
      String mensaje_error = null;
      StringTokenizer st = new StringTokenizer(elError);
      while (st.hasMoreTokens()) {
        muestra = st.nextToken();
        if (muestra.indexOf("ORA") != -1) {
          mensaje_error = muestra.substring(0, muestra.length() - 1);
        }
      }

      bError = true;

      // selecciona el mensaje de error
      switch (opmode) {
        case servletDO_SELECT:
        case servletDO_GETPAGE:
          sMsg = "Error al leer " + queryTool.getName() + ": " + mensaje_error;
          break;

        case servletDO_INSERT:
          sMsg = "Error al insertar " + queryTool.getName() + ": " +
              mensaje_error;
          break;

        case servletDO_UPDATE:
          sMsg = "Error al actualizar " + queryTool.getName() + ": " +
              mensaje_error;
          break;

        case servletDO_DELETE:
          sMsg = "Error al borrar " + queryTool.getName() + ": " +
              mensaje_error;
          break;
      }
    }
    catch (Exception e2) {
      // traza el error
      trazaLog(e2);
      bError = true;

      // selecciona el mensaje de error
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }

    return vResulset;
  }

  public static void main(String args[]) {
    SrvQueryTool srv = new SrvQueryTool();
    Lista vParam = new Lista();
    QueryTool query = new QueryTool();

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sip_desa",
                           "sip_desa");
    //select
    /*
         query.putName("SIP_PETICION_DET");
         query.putType("NM_ANOPET", QueryTool.INTEGER);
         query.putType("NM_PETICION", QueryTool.INTEGER);
         query.putType("CD_PRODUCTO", QueryTool.STRING);
         query.putType("CD_ALMACEN", QueryTool.STRING);
         query.putWhereType("CD_PRODUCTO", QueryTool.STRING);
         query.putWhereValue("CD_PRODUCTO", "00000001");
         query.putOperator("CD_PRODUCTO", "=");
         String subQuery = " select NM_ANOPET, NM_PETICION from SIP_PETICION_CAB "
                    + " where CD_CENTROVAC = ? ";
         query.putSubquery("(NM_ANOPET, NM_PETICION)",subQuery);
         java.util.Vector vTV = new java.util.Vector();
         Data dtTV = new Data();
         dtTV.put(new Integer(QueryTool.STRING),"000001");
         vTV.addElement(dtTV);
         query.putVectorSubquery("(NM_ANOPET, NM_PETICION)", vTV);
         query.addOrderField("NM_ANOPET");
         query.addOrderField("NM_PETICION");
         query.addOrderField("CD_PRODUCTO");
         query.addOrderField("CD_ALMACEN");
         vParam.addElement(query);
         vParam.setTrama("NM_PETICION","1");
         srv.doDebug(2, vParam);
     */
    QueryTool qt = new QueryTool();

    // tabla de familias de productos
    qt.putName("SIP_INSPECCION");

    // campos que se escriben
    qt.putType("IT_N_UBIC_FRIG", 1);
    qt.putValue("IT_N_UBIC_FRIG", "s");

    qt.putType("IT_BASU_SANIT", QueryTool.STRING);
    qt.putValue("IT_BASU_SANIT", "s");

    qt.putType("IT_CONEX_A_RED", QueryTool.STRING);
    qt.putValue("IT_CONEX_A_RED", "s");

    qt.putType("IT_DEV_SRS", QueryTool.STRING);

    qt.putValue("IT_DEV_SRS", "s");

    qt.putType("IT_PUERTA_UNICA", QueryTool.STRING);

    qt.putValue("IT_PUERTA_UNICA", "s");

    qt.putType("IT_AMBOS", QueryTool.STRING);

    qt.putValue("IT_AMBOS", "s");

    qt.putType("IT_DESC_AUTOM", QueryTool.STRING);
    qt.putValue("IT_DESC_AUTOM", "s");

    qt.putType("IT_OTROS_PROC", QueryTool.STRING);

    qt.putValue("IT_OTROS_PROC", "s");

    qt.putType("IT_LOC_ADECUADA", QueryTool.STRING);

    qt.putValue("IT_LOC_ADECUADA", "s");

    qt.putType("IT_TERM_MAX_MIN", QueryTool.STRING);

    qt.putValue("IT_TERM_MAX_MIN", "s");

    qt.putType("IT_ACUM_CONGE", QueryTool.STRING);

    qt.putValue("IT_ACUM_CONGE", "s");

    qt.putType("IT_COL_ADEC_TERM", QueryTool.STRING);

    qt.putValue("IT_COL_ADEC_TERM", "s");

    qt.putType("IT_ACUM_ESTANT", QueryTool.STRING);

    qt.putValue("IT_ACUM_ESTANT", "s");

    qt.putType("IT_REG_T_DIARIA", QueryTool.STRING);

    qt.putValue("IT_REG_T_DIARIA", "s");

    qt.putType("IT_COL_ADEC_PAC", QueryTool.STRING);

    qt.putValue("IT_COL_ADEC_PAC", "s");

    qt.putType("IT_CORTES_LUZ", QueryTool.STRING);

    qt.putValue("IT_CORTES_LUZ", "s");

    qt.putType("IT_VAC_ORD_CAD", QueryTool.STRING);

    qt.putValue("IT_VAC_ORD_CAD", "s");

    qt.putType("IT_CONGELACION", QueryTool.STRING);
    qt.putValue("IT_CONGELACION", "s");

    qt.putType("IT_EXT_VAC_CAD", QueryTool.STRING);

    qt.putValue("IT_EXT_VAC_CAD", "s");

    qt.putType("IT_OTR_RUPTURAS", QueryTool.STRING);

    qt.putValue("IT_OTR_RUPTURAS", "s");

    qt.putType("IT_USO_EX_VAC", QueryTool.STRING);

    qt.putValue("IT_USO_EX_VAC", "s");

    qt.putType("DS_OBSERV", QueryTool.STRING);
    qt.putValue("DS_OBSERV", "DYSEYSYRSE");

    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putValue("CD_OPE", "OPE");

    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qt.putValue("FC_ULTACT", "12/12/1999");

    qt.putWhereType("CD_CENTROVAC", QueryTool.STRING);
    qt.putWhereValue("CD_CENTROVAC", "00005");
    qt.putOperator("CD_CENTROVAC", "=");

    qt.putWhereType("NM_FRIGO", QueryTool.INTEGER);
    qt.putWhereValue("NM_FRIGO", "1");
    qt.putOperator("NM_FRIGO", "=");

    qt.putWhereType("FC_INSPEC", QueryTool.DATE);
    qt.putWhereValue("FC_INSPEC", "11/11/1999");
    qt.putOperator("FC_INSPEC", "=");

    vParam.addElement(qt);

    srv.doDebug(4, vParam);

    /*    //trama
        query.putName("SIP_CENTRO_VACUNACION");
        query.putType("CD_CENTROVAC", QueryTool.STRING);
        query.putType("CDPAIS", QueryTool.STRING);
        query.putWhereType("CD_CENTROVAC", QueryTool.STRING);
        query.putWhereValue("CD_CENTROVAC", "%");
        query.putOperator("CD_CENTROVAC", "like");
        vParam.addElement(query);
        vParam.setTrama("CD_CENTROVAC","CEN01");
        srv.doDebug(2, vParam);*/

    /*    //insert
        query.putName("SIP_PRODUCTO");
        query.putType("CD_PRODUCTO", QueryTool.STRING);
        query.putType("DS_PRODUCTO", QueryTool.STRING);
        query.putType("CD_FPERSENT", QueryTool.STRING);
        query.putType("CD_GPROD", QueryTool.STRING);
        query.putValue("CD_PRODUCTO", "00000003");
        query.putValue("DS_PRODUCTO", "JARABESS");
        query.putValue("CD_FPERSENT", "001");
        query.putValue("CD_GPROD", "000001");
        vParam.addElement(query);
        srv.doDebug(3, vParam);*/

    /*    //update
        query.putName("SIP_PRODUCTO");
        query.putType("DS_PRODUCTO", QueryTool.STRING);
        query.putValue("DS_PRODUCTO", "JARABES");
        query.putWhereType("CD_PRODUCTO", QueryTool.STRING);
        query.putWhereValue("CD_PRODUCTO", "00000003");
        query.putOperator("CD_CENTROVAC", "=");
        vParam.addElement(query);
        srv.doDebug(4, vParam);*/

    /*    //delete
        query.putName("SIP_PRODUCTO");
        query.putWhereType("CD_PRODUCTO", QueryTool.STRING);
        query.putWhereValue("CD_PRODUCTO", "00000003");
        query.putOperator("CD_CENTROVAC", "=");
        vParam.addElement(query);
        srv.doDebug(5, vParam);
     */

    // centros de vacunacion
    /*        query.putName("SIP_CENTRO_VACUNACION");
            // campos que se leen
            query.putType("CD_CENTROVAC", QueryTool.STRING);
            query.putType("DS_CENTROVAC", QueryTool.STRING);
            query.putType("CDPAIS", QueryTool.STRING);
            query.putType("CDCOMU", QueryTool.STRING);
            query.putType("CDPROV", QueryTool.STRING);
            query.putType("CDMUNI", QueryTool.STRING);
            query.putType("DS_RESPONSABLE", QueryTool.STRING);
            query.putType("DS_TELEF", QueryTool.STRING);
            // filtro de �reas
            query.putWhereType("CD_AREAS", QueryTool.STRING);
            query.putWhereValue("CD_AREAS", "01");
            query.putOperator("CD_AREAS", "=");
            // filtro de c�digo
            query.putWhereType("CD_CENTROVAC", QueryTool.STRING);
            query.putWhereValue("CD_CENTROVAC", "CEN01");
            query.putOperator("CD_CENTROVAC", "=");
            vParam.addElement(query);
            srv.doDebug(1, vParam);*/

    /*
          QueryTool qtStock = new QueryTool();
          // stock del producto
          qtStock.putName("SIP_STOCK_PRODUCTO");
          // campos que se leen
          qtStock.putType("CD_PRODUCTO",QueryTool.STRING);
          qtStock.putType("CD_ALMACEN",QueryTool.STRING);
          qtStock.putType("NM_STMIN",QueryTool.INTEGER);
          qtStock.putType("NM_STMAX",QueryTool.INTEGER);
          qtStock.putType("NM_CANTINI",QueryTool.INTEGER);
          qtStock.putType("NM_CANTREC",QueryTool.INTEGER);
          qtStock.putType("NM_CANTREGUL",QueryTool.INTEGER);
          qtStock.putType("NM_CANTDIST",QueryTool.INTEGER);
          qtStock.putType("NM_CANTPED",QueryTool.INTEGER);
          qtStock.putType("NM_CANTSOLI",QueryTool.INTEGER);
          qtStock.putType("NM_CANTDEV",QueryTool.INTEGER);
          // filtro de producto
          qtStock.putWhereType("CD_PRODUCTO", QueryTool.STRING);
          qtStock.putWhereValue("CD_PRODUCTO", "00000001");
          qtStock.putOperator("CD_PRODUCTO", "=");
          // filtro de almac�n
          qtStock.putWhereType("CD_ALMACEN", QueryTool.STRING);
          qtStock.putWhereValue("CD_ALMACEN", "001");
          qtStock.putOperator("CD_ALMACEN", "=");
          Lista vStock = new Lista();
          vStock.addElement(qtStock);
          srv.doDebug(1, vStock);
     */

    srv = null;
  }
}