package sapp2;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Query tool.
 *
 * @autor LSR
 * @version 1.0
 *
 * Modificaci�n 4/11/99: admite subconsultas en el WHERE
 * @autor CGA
 *
 *Modif 24/01/99 Chema : Admite cl�usula distinct
 */
public class QueryTool
    implements Serializable, Cloneable {
  /**
   * Tipos de datos
   */
  public static final int STRING = 1;
  public static final int INTEGER = 2;
  public static final int REAL = 3;
  public static final int DATE = 4;
  public static final int TIMESTAMP = 5;
  public static final int VOID = 6;

  public static String FECHA_ACTUAL = "FECHA_ACTUAL"; //Sring para indicar que se inserte fecha actual de Servidor

  /**
   * Nombre de la tabla
   */
  protected String sName = new String("");

  /**
   * Tipo de datos de los campo
   */
  protected Data datType = new Data();

  /**
   * Valor de los campo
   */
  protected Data datValue = new Data();

  /**
   * Cla�sula order
   */
  protected Vector ordFields = new Vector();

  /**
   * Tipo de datos de los campo
   */
  protected Data whrType = new Data();

  /**
   * Valores de la cla�sula
   */
  protected Data whrValue = new Data();

  /**
   * Operadores de la cla�sula
   */
  protected Data whrOperator = new Data();

  /**
   * Cla�sula de subconsultas
   */
  protected Data whrSubquery = new Data();

  /**
   * Tipos y valores de las subconsultas
   */
  protected Data whrTypeValue = new Data();

  /**
   * Booleano que indica la adici�n del modificador DISTINCT
   */
  protected boolean distinct = false;

  /**
   * Constructor
   */
  public QueryTool(String tabla,
                   Data types,
                   Data values,
                   Data wtypes,
                   Data wvalues,
                   Data wop,
                   Vector order,
                   Data wsubquery,
                   Data wtypevalue) {
    sName = tabla;
    datType = types;
    datValue = values;
    ordFields = order;
    whrType = wtypes;
    whrValue = wvalues;
    whrOperator = wop;
    whrSubquery = wsubquery;
    whrTypeValue = wtypevalue;

  }

  public QueryTool() {
  }

  /**
   * Clonaci�n
   */
  public Object clone() {
    return new QueryTool(new String(sName),
                         (Data) datType.clone(),
                         (Data) datValue.clone(),
                         (Data) whrType.clone(),
                         (Data) whrValue.clone(),
                         (Data) whrOperator.clone(),
                         (Vector) ordFields.clone(),
                         (Data) whrSubquery.clone(),
                         (Data) whrTypeValue.clone());

  }

  /**
   * Graba el nombre de la tabla
   *
   * @param name  Nombre del campo
   */
  public void putName(String name) {
    sName = new String(name);
  }

  /**
   * Lee el nombre de la tabla
   *
   * @return name
   */
  public String getName() {
    return sName;
  }

  /**
   * Establece el modificar DISTINCT
   *
   * @return void
   */
  public void setDistinct(boolean state) {
    distinct = state;
  }

  /**
   * Lee los campos del select
   *
   * @return Vector
   */
  public Vector getNamesSelect() {
    Enumeration enum = null;
    Vector res = new Vector();
    for (enum = datType.keys(); enum.hasMoreElements(); ) {
      res.addElement( (String) enum.nextElement());
    }
    return res;
  }

  /**
   * Graba una pareja (campo,tipo)
   *
   * @param name  Nombre del campo
   * @param type  Tipo de datos
   */
  public void putType(String name, int type) {
    datType.put(name, new Integer(type));
  }

  /**
   * Lee el tipo de un campo
   *
   * @param name  Nombre del campo
   * @return type
   */
  public int getType(String name) {
    return ( (Integer) datType.get(name)).intValue();
  }

  /**
   * Graba una pareja (campo,valor)
   *
   * @param name  Nombre del campo
   * @param valor  Valor
   */
  public void putValue(String name, Object valor) {
    datValue.put(name, valor);
  }

  /**
   * Lee el valor de un campo
   *
   * @param name  Nombre del campo
   * @return valor
   */
  public Object getValue(String name) {
    return datValue.get(name);
  }

  /**
   * A�ade un campo a la cla�sula order
   *
   * @param name  Nombre del campo
   */
  public void addOrderField(String name) {
    ordFields.addElement(name);
  }

  /**
   * Graba el tipo de datos de los campos de la cla�sula WHERE
   *
   * @param name  Nombre del campo
   * @param type  Tipo de datos
   */
  public void putWhereType(String name, int type) {
    whrType.put(name, new Integer(type));
  }

  /**
   * Lee el tipo de un campo de la cla�sula WHERE
   *
   * @param name  Nombre del campo
   * @return type
   */
  public int getWhereType(String name) {
    return ( (Integer) whrType.get(name)).intValue();
  }

  /**
   * Graba el valor de un campo de la cla�sula WHERE
   *
   * @param name  Nombre del campo
   * @param valor  Valor
   */
  public void putWhereValue(String name, Object valor) {
    whrValue.put(name, valor);
  }

  /**
   * Lee el valor de un campo de la cla�sula WHERE
   *
   * @param name  Nombre del campo
   * @return valor
   */
  public Object getWhereValue(String name) {
    return whrValue.get(name);
  }

  /**
   * Graba un operador de la cla�sula WHERE
   *
   * @param name  Nombre del campo
   * @param op  Operador
   */
  public void putOperator(String name, String op) {
    whrOperator.put(name, op);
  }

  /**
   * Lee el operador de un campo
   *
   * @param name  Nombre del campo
   * @return op
   */
  public String getOperator(String name) {
    return (String) whrOperator.get(name);
  }

  /**
   * Graba una subconsulta
   *
   * @param name  Nombre del campo
   * @param subquery  Subconsulta
   */
  public void putSubquery(String name, String subquery) {
    whrSubquery.put(name, subquery);
  }

  /**
   * Lee la subconsulta de un campo de la cla�sula WHERE
   *
   * @param name  Nombre del campo
   * @return Subconsulta
   */
  public Object getWhereSubquery(String name) {
    return whrSubquery.get(name);
  }

  /**
   * Graba el vector con los tipos y valores de una subconsulta
   *
   * @param name  Nombre del campo
   * @param vTypeValue  Vector de tipos y valores
   */
  public void putVectorSubquery(String name, Vector vTypeValue) {
    whrTypeValue.put(name, vTypeValue);
  }

  /**
   * Lee el vector con los tipos y valores de una subconsulta
   *
   * @param name  Nombre del campo
   * @return Vector de tipos y valores
   */
  public Object getVectorSubquery(String name) {
    return whrTypeValue.get(name);
  }

  /**
   * Realiza sentencias de tipo SELECT sin tramar
   *
   * @param Connection conexi�n JDBC
   * @return Lista con los resultados
   */
  public Lista doSelect(Connection con) throws Exception {

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // cadenas para preparar la sentencia
    String query = "";
    String where = "";

    // colecciones de campos
    Enumeration enum1 = null;
    Enumeration enum2 = null;
    Enumeration enum3 = null;

    // buffers
    int i = 1;
    Data line = null;

    // lista de con el resultado
    Lista snapShot = new Lista();
    String value = null;

    // prepara la query
    query = "SELECT ";
    if (distinct) {
      query = query + "DISTINCT ";
    }
    for (enum1 = datType.keys(); enum1.hasMoreElements(); ) {
      query = query + (String) enum1.nextElement() + ",";
    }
    query = query.substring(0, query.length() - 1) + " FROM " + sName;

    // cl�usula where
    if (whrType.size() != 0) {
      enum2 = whrOperator.elements();
      enum3 = whrType.elements();
      for (enum1 = whrType.keys(); enum1.hasMoreElements(); ) {
        if (!where.equals("")) {
          where = where + " AND ";
        }
        where = where + (String) enum1.nextElement() + " " +
            (String) enum2.nextElement() + " ";
        // si el tipo es VOID no requiere valor
        if ( ( (Integer) enum3.nextElement()).intValue() != VOID) {
          where = where + "?";
        }
      }
    }

    //para las subconsultas
    if (whrSubquery.size() != 0) {
      //campos con operador in y subconsultas
      enum2 = whrSubquery.elements();
      for (enum1 = whrSubquery.keys(); enum1.hasMoreElements(); ) {
        if (!where.equals("")) {
          where = where + " AND ";
        }
        where = where + (String) enum1.nextElement() + " in ( " +
            (String) enum2.nextElement() + " ) ";
      }
    }

    if (!where.equals("")) {
      query = query + " WHERE " + where;

      // cl�usula order by
    }
    if (ordFields.size() != 0) {
      query = query + " ORDER BY ";
      for (i = 0; i < ordFields.size(); i++) {
        query = query + (String) ordFields.elementAt(i) + ",";
      }
      query = query.substring(0, query.length() - 1);
    }

    // prepara la consulta
    st = con.prepareStatement(query);

    // completa los valores de la cla�sula where
    i = 1;
    if (whrValue.size() != 0) {
      enum2 = whrType.elements();
      for (enum1 = whrValue.elements(); enum1.hasMoreElements(); ) {

        switch ( ( (Integer) enum2.nextElement()).intValue()) {
          case QueryTool.DATE:
            st.setDate(i++, Format.string2Date( (String) enum1.nextElement()));
            break;
          case QueryTool.INTEGER:
            st.setInt(i++, (new Integer( (String) enum1.nextElement()).intValue()));
            break;
          case QueryTool.REAL:
            st.setDouble(i++,
                         Format.string2Double( (String) enum1.nextElement()).doubleValue());
            break;
          case QueryTool.STRING:
            st.setString(i++, (String) enum1.nextElement());
            break;
          case QueryTool.TIMESTAMP:
            st.setTimestamp(i++,
                            Format.string2Timestamp( (String) enum1.nextElement()));
            break;
          case QueryTool.VOID:
            enum1.nextElement();
            break;
        }
      }
    }

    //completa los valores de las subconsultas
    Vector vTypeValue = new Vector();
    Data dtTV = new Data();

    if (whrTypeValue.size() != 0) {
      //un vector para cada subconsulta
      for (enum1 = whrTypeValue.elements(); enum1.hasMoreElements(); ) {
        vTypeValue = (Vector) enum1.nextElement();
        for (int s = 0; s < vTypeValue.size(); s++) {
          dtTV = (Data) vTypeValue.elementAt(s);
          enum2 = dtTV.keys(); //tipo
          enum3 = dtTV.elements(); //valor
          switch ( ( (Integer) enum2.nextElement()).intValue()) {
            case QueryTool.DATE:
              st.setDate(i++, Format.string2Date( (String) enum3.nextElement()));
              break;
            case QueryTool.INTEGER:
              st.setInt(i++,
                        (new Integer( (String) enum3.nextElement()).intValue()));
              break;
            case QueryTool.REAL:
              st.setDouble(i++,
                           Format.string2Double( (String) enum3.nextElement()).
                           doubleValue());
              break;
            case QueryTool.STRING:
              st.setString(i++, (String) enum3.nextElement());
              break;
            case QueryTool.TIMESTAMP:
              st.setTimestamp(i++,
                              Format.string2Timestamp( (String) enum3.nextElement()));
              break;
          }
        }
      }
    }

    // realiza la query
    rs = st.executeQuery();

    // rellena la matriz de salida
    while (rs.next()) {
      line = new Data();
      i = 1;
      enum2 = datType.elements();
      for (enum1 = datType.keys(); enum1.hasMoreElements(); ) {
        switch ( ( (Integer) enum2.nextElement()).intValue()) {
          case QueryTool.DATE:
            value = Format.date2String(rs.getDate(i++));
            if (rs.wasNull()) {
              value = null;
            }
            break;
          case QueryTool.INTEGER:
            value = (new Integer(rs.getInt(i++))).toString();
            if (rs.wasNull()) {
              value = null;
            }
            break;
          case QueryTool.REAL:
            value = Format.double2String(new Double(rs.getDouble(i++)));
            if (rs.wasNull()) {
              value = null;
            }
            break;
          case QueryTool.STRING:
            value = rs.getString(i++);
            if (rs.wasNull()) {
              value = null;
            }
            break;
          case QueryTool.TIMESTAMP:
            value = Format.timestamp2String(rs.getTimestamp(i++));
            if (rs.wasNull()) {
              value = null;
            }
            break;
        }

        if (value != null) {
          line.put(enum1.nextElement(), value);
        }
        else {
          enum1.nextElement();
        }
      }
      snapShot.addElement(line);
    }

    // cierra los recursos
    rs.close();
    rs = null;
    st.close();
    st = null;

    // devuelve la matriz de salida
    return snapShot;
  }

  /**
   * Realiza sentencias de tipo SELECT tramando
   *
   * @param sTrama campo de la trama
   * @param sValorTrama valor de la trama
   * @param sOperadorTrama operador de la trama
   * @param sOrdenTrama cla�sula DESC si fuese necesario
   * @param Connection conexi�n JDBC
   * @return Lista con los resultados
   */
  public Lista doGetPage(String sTrama,
                         String sValorTrama,
                         String sOperadorTrama,
                         String sOrdenTrama,
                         Connection con) throws Exception {

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // cadenas para preparar la sentencia
    String query = "";
    String where = "";

    // colecciones de campos
    Enumeration enum1 = null;
    Enumeration enum2 = null;
    Enumeration enum3 = null;

    // buffers
    int i = 1;
    int counter = 1;
    Data line = null;

    // lista de con el resultado
    Lista snapShot = new Lista();
    Object value = null;

    // prepara la query
    query = "SELECT ";
    if (distinct) {
      query = query + "DISTINCT ";
    }
    for (enum1 = datType.keys(); enum1.hasMoreElements(); ) {
      query = query + (String) enum1.nextElement() + ",";
    }
    query = query.substring(0, query.length() - 1) + " FROM " + sName;

    // cl�usula where
    // trama
    if (!sValorTrama.equals("")) {
      where = sTrama + sOperadorTrama + "?";
    }

    // filtro
    if (whrType.size() != 0) {
      enum2 = whrOperator.elements();
      enum3 = whrType.elements();
      for (enum1 = whrType.keys(); enum1.hasMoreElements(); ) {
        if (!where.equals("")) {
          where = where + " AND ";
        }
        where = where + "upper(" + (String) enum1.nextElement() + ") " +
            (String) enum2.nextElement() + " ";
        // si el tipo es VOID no requiere valor
        if ( ( (Integer) enum3.nextElement()).intValue() != VOID) {
          where = where + "upper(?)";
        }
      }
    }

    //para las subconsultas
    if (whrSubquery.size() != 0) {
      //campos con operador in y subconsultas
      enum2 = whrSubquery.elements();
      for (enum1 = whrSubquery.keys(); enum1.hasMoreElements(); ) {
        if (!where.equals("")) {
          where = where + " AND ";
        }
        where = where + (String) enum1.nextElement() + " in ( " +
            (String) enum2.nextElement() + " ) ";
      }
    }

    if (!where.equals("")) {
      query = query + " WHERE " + where;

      // ordenamiento por la trama
    }
    if (!sTrama.equals("")) {
      query = query + " ORDER BY " + sTrama + " " + sOrdenTrama;
    }

    // cl�usula order by
    if (ordFields.size() != 0) {
      if (!sTrama.equals("")) {
        query = query + ",";
      }
      else {
        query = query + " ORDER BY ";
      }
      for (i = 0; i < ordFields.size(); i++) {
        query = query + (String) ordFields.elementAt(i) + ",";
      }
      query = query.substring(0, query.length() - 1);
    }

    // prepara la consulta
    st = con.prepareStatement(query);

    // completa los valores de la cla�sula where
    // trama
    i = 1;
    if (!sValorTrama.equals("")) {
      st.setString(i++, sValorTrama);
    }

    // filtro
    if (whrValue.size() != 0) {
      enum2 = whrType.elements();
      for (enum1 = whrValue.elements(); enum1.hasMoreElements(); ) {

        switch ( ( (Integer) enum2.nextElement()).intValue()) {
          case QueryTool.DATE:
            st.setDate(i++, Format.string2Date( (String) enum1.nextElement()));
            break;
          case QueryTool.INTEGER:
            st.setInt(i++, (new Integer( (String) enum1.nextElement()).intValue()));
            break;
          case QueryTool.REAL:
            st.setDouble(i++,
                         Format.string2Double( (String) enum1.nextElement()).doubleValue());
            break;
          case QueryTool.STRING:
            st.setString(i++, (String) enum1.nextElement());
            break;
          case QueryTool.TIMESTAMP:
            st.setTimestamp(i++,
                            Format.string2Timestamp( (String) enum1.nextElement()));
            break;
          case QueryTool.VOID:
            enum1.nextElement();
            break;
        }
      }
    }

    //completa los valores de las subconsultas
    Vector vTypeValue = new Vector();
    Data dtTV = new Data();

    if (whrTypeValue.size() != 0) {
      //un vector para cada subconsulta
      for (enum1 = whrTypeValue.elements(); enum1.hasMoreElements(); ) {
        vTypeValue = (Vector) enum1.nextElement();
        for (int s = 0; s < vTypeValue.size(); s++) {
          dtTV = (Data) vTypeValue.elementAt(s);
          enum2 = dtTV.keys(); //tipo
          enum3 = dtTV.elements(); //valor
          switch ( ( (Integer) enum2.nextElement()).intValue()) {
            case QueryTool.DATE:
              st.setDate(i++, Format.string2Date( (String) enum3.nextElement()));
              break;
            case QueryTool.INTEGER:
              st.setInt(i++,
                        (new Integer( (String) enum3.nextElement()).intValue()));
              break;
            case QueryTool.REAL:
              st.setDouble(i++,
                           Format.string2Double( (String) enum3.nextElement()).
                           doubleValue());
              break;
            case QueryTool.STRING:
              st.setString(i++, (String) enum3.nextElement());
              break;
            case QueryTool.TIMESTAMP:
              st.setTimestamp(i++,
                              Format.string2Timestamp( (String) enum3.nextElement()));
              break;
          }
        }
      }
    }

    // realiza la query
    rs = st.executeQuery();

    // rellena la matriz de salida
    while (rs.next()) {
      line = new Data();
      i = 1;
      enum2 = datType.elements();
      for (enum1 = datType.keys(); enum1.hasMoreElements(); ) {
        switch ( ( (Integer) enum2.nextElement()).intValue()) {
          case QueryTool.DATE:
            value = Format.date2String(rs.getDate(i++));
            if (rs.wasNull()) {
              value = null;
            }
            break;
          case QueryTool.INTEGER:
            value = (new Integer(rs.getInt(i++))).toString();
            if (rs.wasNull()) {
              value = null;
            }
            break;
          case QueryTool.REAL:
            value = Format.double2String(new Double(rs.getDouble(i++)));
            if (rs.wasNull()) {
              value = null;
            }
            break;
          case QueryTool.STRING:
            value = rs.getString(i++);
            if (rs.wasNull()) {
              value = null;
            }
            break;
          case QueryTool.TIMESTAMP:
            value = Format.timestamp2String(rs.getTimestamp(i++));
            if (rs.wasNull()) {
              value = null;
            }
            break;
        }

        if (value != null) {
          line.put(enum1.nextElement(), value);
        }
        else {
          enum1.nextElement();

        }
      }
      snapShot.addElement(line);

      // trama
      if (counter >= Lista.PAGE_SIZE) {
        snapShot.setIncomplet();
        snapShot.setTrama(sTrama, (String) line.get(sTrama));
        break;
      }
      counter++;

    }

    // cierra los recursos
    rs.close();
    rs = null;
    st.close();
    st = null;

    // devuelve la matriz de salida
    return snapShot;

  }

  /**
   * Realiza sentencias de tipo INSERT
   *
   * @param Connection conexi�n JDBC
   * @return Lista con los resultados
   */
  public Lista doInsert(Connection con) throws Exception {

    // buffers
    String query = null;
    String values = null;
    String value = null;
    String key = null;
    int i = 1;

    // objetos JDBC
    PreparedStatement st = null;

    // objetos enum
    Enumeration enum1 = null;
    Enumeration enum2 = null;
    Enumeration enum3 = null;

    // lista para devolver el n�mero de registros afectados
    Lista vRegistros;

    // Fecha Ultima Actualizacion
    String fAct = "";

    // prepara la query
    query = "INSERT INTO " + sName + " (";
    values = ") VALUES (";
    for (enum1 = datType.keys(); enum1.hasMoreElements(); ) {
      query = query + (String) enum1.nextElement() + ",";
      values = values + "?,";
    }
    query = query.substring(0, query.length() - 1) +
        values.substring(0, values.length() - 1) + ")";

    // prepara la consulta
    st = con.prepareStatement(query);

    // completa los valores de la cla�sula where
    enum2 = datValue.elements();
    enum3 = datType.keys();
    for (enum1 = datType.elements(); enum1.hasMoreElements(); ) {
      value = (String) enum2.nextElement();
      key = (String) enum3.nextElement();
      if (key.equals("FC_ULTACT")) {
        fAct = Format.fechaHoraActual();
        st.setTimestamp(i++, Format.string2Timestamp(fAct));
        enum1.nextElement();
      }
      else {
        switch ( ( (Integer) enum1.nextElement()).intValue()) {
          case QueryTool.DATE:
            if (value.equals("")) {
              st.setNull(i++, Types.DATE);
            }
            else if (value.equals(FECHA_ACTUAL)) {
              st.setDate(i++, new java.sql.Date(new java.util.Date().getTime()));
            }
            else {
              st.setDate(i++, Format.string2Date(value));
            }
            break;
          case QueryTool.INTEGER:
            if (value.equals("")) {
              st.setNull(i++, Types.INTEGER);
            }
            else {
              st.setInt(i++, (new Integer(value).intValue()));
            }
            break;
          case QueryTool.REAL:
            if (value.equals("")) {
              st.setNull(i++, Types.DOUBLE);
            }
            else {
              st.setDouble(i++, Format.string2Double(value).doubleValue());
            }
            break;
          case QueryTool.STRING:
            if (value.equals("")) {
              st.setNull(i++, Types.VARCHAR);
            }
            else {
              st.setString(i++, value);
            }
            break;
          case QueryTool.TIMESTAMP:
            if (value.equals("")) {
              st.setNull(i++, Types.TIMESTAMP);
            }
            else {
              st.setTimestamp(i++, Format.string2Timestamp(value));
            }
            break;
        }
      }
    }

    // realiza la query
    i = st.executeUpdate();

    st.close();
    st = null;

    vRegistros = new Lista();
    vRegistros.addElement(new Integer(i));
    vRegistros.setFC_ULTACT(fAct);

    return vRegistros;
  }

  /**
   * Realiza sentencias de tipo UPDATE
   *
   * @param Connection conexi�n JDBC
   * @return Lista con los resultados
   */
  public Lista doUpdate(Connection con) throws Exception {

    // buffers
    String query = null;
    int i = 1;
    String value = null;
    String where = null; ;
    String key = null;

    // objetos JDBC
    PreparedStatement st = null;

    // objetos enum
    Enumeration enum1 = null;
    Enumeration enum2 = null;
    Enumeration enum3 = null;

    // lista para devolver el n�mero de registros afectados
    Lista vRegistros;

    // Fecha Ultima Actualizacion
    String fAct = "";

    // prepara la query
    query = "UPDATE " + sName + " SET ";
    for (enum1 = datType.keys(); enum1.hasMoreElements(); ) {
      query = query + (String) enum1.nextElement() + "=?,";
    }
    query = query.substring(0, query.length() - 1);

    // cl�usula where
    if (whrType.size() != 0) {
      where = "";
      enum2 = whrOperator.elements();
      for (enum1 = whrType.keys(); enum1.hasMoreElements(); ) {
        if (!where.equals("")) {
          where = where + " AND ";
        }
        where = where + (String) enum1.nextElement() + " " +
            (String) enum2.nextElement() + " ?";
      }
      query = query + " WHERE " + where;
    }

    // prepara la consulta
    st = con.prepareStatement(query);

    // rellena los valores
    enum2 = datValue.elements();
    enum3 = datType.keys();
    for (enum1 = datType.elements(); enum1.hasMoreElements(); ) {
      value = (String) enum2.nextElement();
      key = (String) enum3.nextElement();
      if (key.equals("FC_ULTACT")) {
        fAct = Format.fechaHoraActual();
        st.setTimestamp(i++, Format.string2Timestamp(fAct));
        enum1.nextElement();
      }
      else {
        switch ( ( (Integer) enum1.nextElement()).intValue()) {
          case QueryTool.DATE:
            if (value.equals("")) {
              st.setNull(i++, Types.DATE);
            }
            else {
              st.setDate(i++, Format.string2Date(value));
            }
            break;
          case QueryTool.INTEGER:
            if (value.equals("")) {
              st.setNull(i++, Types.INTEGER);
            }
            else {
              st.setInt(i++, (new Integer(value).intValue()));
            }
            break;
          case QueryTool.REAL:
            if (value.equals("")) {
              st.setNull(i++, Types.DOUBLE);
            }
            else {
              st.setDouble(i++, Format.string2Double(value).doubleValue());
            }
            break;
          case QueryTool.STRING:
            if (value.equals("")) {
              st.setNull(i++, Types.VARCHAR);
            }
            else {
              st.setString(i++, value);
            }
            break;
          case QueryTool.TIMESTAMP:
            if (value.equals("")) {
              st.setNull(i++, Types.TIMESTAMP);
            }
            else {
              st.setTimestamp(i++, Format.string2Timestamp(value));
            }
            break;
        }
      }
    }

    // completa los valores de la cla�sula where
    if (whrValue.size() != 0) {
      enum2 = whrType.elements();
      enum3 = whrType.keys();
      for (enum1 = whrValue.elements(); enum1.hasMoreElements(); ) {
        key = (String) enum3.nextElement();
        switch ( ( (Integer) enum2.nextElement()).intValue()) {
          case QueryTool.DATE:
            st.setDate(i++, Format.string2Date( (String) enum1.nextElement()));
            break;
          case QueryTool.INTEGER:
            st.setInt(i++, (new Integer( (String) enum1.nextElement()).intValue()));
            break;
          case QueryTool.REAL:
            st.setDouble(i++,
                         Format.string2Double( (String) enum1.nextElement()).doubleValue());
            break;
          case QueryTool.STRING:
            st.setString(i++, (String) enum1.nextElement());
            break;
          case QueryTool.TIMESTAMP:
            st.setTimestamp(i++,
                            Format.string2Timestamp( (String) enum1.nextElement()));
            break;
        }

      }
    }

    // realiza la query
    i = st.executeUpdate();

    // cierra los recursos
    st.close();
    st = null;

    vRegistros = new Lista();
    vRegistros.addElement(new Integer(i));
    vRegistros.setFC_ULTACT(fAct);

    return vRegistros;
  }

  /**
   * Realiza sentencias de tipo DELETE
   *
   * @param Connection conexi�n JDBC
   * @return Lista con los resultados
   */
  public Lista doDelete(Connection con) throws Exception {

    // buffers
    String query = null;
    String where = null;
    int i = 1;

    // objetos JDBC
    PreparedStatement st = null;

    // objetos enum
    Enumeration enum1 = null;
    Enumeration enum2 = null;

    // lista para devolver el n�mero de registros afectados
    Lista vRegistros;

    // prepara la query
    query = "DELETE FROM " + sName;

    // cl�usula where
    if (whrType.size() != 0) {
      where = "";
      enum2 = whrOperator.elements();
      for (enum1 = whrType.keys(); enum1.hasMoreElements(); ) {
        if (!where.equals("")) {
          where = where + " AND ";
        }
        where = where + (String) enum1.nextElement() + " " +
            (String) enum2.nextElement() + " ?";
      }
      query = query + " WHERE " + where;
    }

    // prepara la consulta
    st = con.prepareStatement(query);

    // completa los valores de la cla�sula where
    if (whrValue.size() != 0) {
      enum2 = whrType.elements();
      for (enum1 = whrValue.elements(); enum1.hasMoreElements(); ) {

        switch ( ( (Integer) enum2.nextElement()).intValue()) {
          case QueryTool.DATE:
            st.setDate(i++, Format.string2Date( (String) enum1.nextElement()));
            break;
          case QueryTool.INTEGER:
            st.setInt(i++, (new Integer( (String) enum1.nextElement()).intValue()));
            break;
          case QueryTool.REAL:
            st.setDouble(i++,
                         Format.string2Double( (String) enum1.nextElement()).doubleValue());
            break;
          case QueryTool.STRING:
            st.setString(i++, (String) enum1.nextElement());
            break;
          case QueryTool.TIMESTAMP:
            st.setTimestamp(i++,
                            Format.string2Timestamp( (String) enum1.nextElement()));
            break;
        }
      }
    }

    // realiza la query
    i = st.executeUpdate();

    // cierra los recursos
    st.close();
    st = null;

    vRegistros = new Lista();
    vRegistros.addElement(new Integer(i));

    return vRegistros;
  }

//____________ METODOS PARA FACILITAR DEPURACION______
//Muestran estructura interna de la queryTool_________________

  public Data getDatType() {
    return datType;
  }

  public Data getDatValue() {
    return datValue;
  }

  public Vector getOrdFields() {
    return ordFields;
  }

  public Data getWhrType() {
    return whrType;
  }

  public Data whrValue() {
    return whrValue;
  }

  public Data whrOperator() {
    return whrOperator;
  }

  public Data getWhrSubquery() {
    return whrSubquery;
  }

  public Data whrTypeValue() {
    return whrTypeValue;
  }

//____________________________________________________

}
