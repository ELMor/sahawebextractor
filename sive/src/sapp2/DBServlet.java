package sapp2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbcpool.JDCConnectionDriver;

/**
 * Clase gen�rica de la que heredan todos los servlets.
 *
 * @autor LSR
 * @version 1.0
 */
public abstract class DBServlet
    extends HttpServlet {

  /**
   * Nombre del driver JDBC
   */
  protected String driverName = "";
  /**
   * URL de conexi�n JDBC
   */
  protected String dbUrl = "";
  /**
   * Usuario para la conexi�n JDBC
   */
  protected String userName = "";
  /**
   * Password para la conexi�n JDBC
   */
  protected String userPwd = "";
  /**
   * Pool de conexiones
   */
  protected JDCConnectionDriver jdcdriver;
  /**
   * Conexi�n activa
   */
  protected Connection con;
  /**
   * Modo debug
   */
  protected boolean debugMode = true;

  /**
   * Inicializaci�n de los par�metros de conexi�n JDBC
   *
   * @param config  par�metros de configuraci�n del servlet
   */
  public void init(ServletConfig config) throws ServletException {

    // carga las constantes de acceso JDBC
    driverName = config.getInitParameter("drivername");
    dbUrl = config.getInitParameter("dbUrl");
    userName = config.getInitParameter("userName");
    userPwd = config.getInitParameter("userPwd");

    // desactiva el modo debug
    if (config.getInitParameter("debugMode") != null) {
      if (config.getInitParameter("debugMode").equals("off")) {
        debugMode = false;

      }
    }
    try {
      // carga el driver JDBC
      jdcdriver = new JDCConnectionDriver(driverName, dbUrl, userName, userPwd);
    }
    catch (Exception e) {
      // error al abrir la conexi�n con la base de datos
      e.printStackTrace();
      throw new ServletException("Error al conectar con la base de datos.");
    }
  }

  /**
   * Libera los recursos abiertos para descargar el servlet
   */
  public void destroy() {
    try {
      jdcdriver.clearPool();
      jdcdriver = null;
    }
    catch (Exception e) {
      ;
    }
    super.destroy();
  }

  /**
   * Abre una conexi�n JDBC
   *
   * @return devuelve una conexi�n JDBC
   */
  protected synchronized Connection openConnection() throws Exception {
    return jdcdriver.connect();
  }

  /**
   * Cierra una conexi�n JDBC
   *
   * @param con conexi�n
   */
  protected synchronized void closeConnection(Connection con) {
    try {
      jdcdriver.disconnect(con);
    }
    catch (Exception e) {}
  }

  /**
   * L�gica de negocio del servlet
   *
   * @param opmode modo de operaci�n del servelt
   * @param vParametros vector de par�metros recibidos desde el applet
   * @return vector con los resultados de la operaci�n solicitada
   */
  protected abstract Lista doWork(int opmode, Lista vParametros) throws
      Exception;

  /**
   * Procesa peticiones POST recibidas por el servlet
   *
   * @param request objeto HttpServletRequest
   * @param response objeto HttpServletResponse
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    // par�metros
    Lista vParametros = null;
    Lista vResultado = null;

    // streams
    ObjectOutputStream out = null;
    ObjectInputStream in = null;

    // objetos para controlar el bloqueo
    QueryTool qtBlock = null;
    Lista vSnapShot = null;
    boolean bCambio = true;
    String sMsg = null;
    Data dtBlockInfo = null;
    Data dtBlock = null;
    Lista lisBlock = null;
    Lista lisQuery = null;

    // modo de operaci�n
    int opmode;

    // comunicaciones servlet-applet
    try {
      // lee el modo de operaci�n
      in = new ObjectInputStream(request.getInputStream());
      opmode = in.readInt();

      // lee los par�metros
      vParametros = (Lista) in.readObject();

      // abre el stream de salida
      response.setContentType("application/octet-stream");
      out = new ObjectOutputStream( (OutputStream) response.getOutputStream());
      out.flush();

      // abre la conexi�n con la base de datos
      con = openConnection();

      // determina si debe analizar informaci�n de bloqueo
      if ( (opmode / 10000) == 1) {

        // lee la informaci�n de bloqueo almacenada
        try {
          dtBlock = (Data) vParametros.elementAt(1);

          // ejecuta la select para obtener CD_OPE y FC_ULTACT
          qtBlock = (QueryTool) vParametros.elementAt(0);
          vSnapShot = qtBlock.doSelect(con);

          // determina si hay cambio en esta informaci�n
          bCambio = true;

          if (vSnapShot.size() > 0) {
            dtBlockInfo = (Data) vSnapShot.firstElement();

            // comprueba los datos
            if (dtBlock.getString("CD_OPE").equals(dtBlockInfo.getString(
                "CD_OPE")) &&
                dtBlock.getString("FC_ULTACT").equals(dtBlockInfo.getString(
                "FC_ULTACT"))) {

              // no hay cambio
              bCambio = false;
            }
            else {

              // graba el mensaje para devolver
              sMsg = "Los datos que desea actualizar han sido modificados por " +
                  dtBlockInfo.getString("CD_OPE") + " el " +
                  dtBlockInfo.getString("FC_ULTACT");
            }
          }
          else {
            // graba el mensaje para devolver
            sMsg = "La informaci�n que trata de modificar ha sido borrada";
            dtBlockInfo = null;
          }

          // si hay cambio lanza la excepci�n
          if (bCambio) {
            throw new BlockException(sMsg, dtBlockInfo);

            // no hay cambio -> sigue con la operaci�n en curso
          }
          else {
            vParametros = (Lista) vParametros.elementAt(2);
          }

        }
        catch (ClassCastException e) {
          lisBlock = (Lista) vParametros.elementAt(1);

          // ejecuta la select para obtener CD_OPE y FC_ULTACT
          lisQuery = (Lista) vParametros.elementAt(0);

          for (int j = 0; j < lisQuery.size(); j++) {
            qtBlock = (QueryTool) lisQuery.elementAt(j);
            dtBlock = (Data) lisBlock.elementAt(j);
            vSnapShot = qtBlock.doSelect(con);

            // determina si hay cambio en esta informaci�n
            bCambio = true;

            if (vSnapShot.size() > 0) {
              dtBlockInfo = (Data) vSnapShot.firstElement();

              // comprueba los datos
              if (dtBlock.getString("CD_OPE").equals(dtBlockInfo.getString(
                  "CD_OPE")) &&
                  dtBlock.getString("FC_ULTACT").equals(dtBlockInfo.getString(
                  "FC_ULTACT"))) {

                // no hay cambio
                bCambio = false;
              }
              else {
                dtBlock.put("CD_OPE", dtBlockInfo.getString("CD_OPE"));
                dtBlock.put("FC_ULTACT", dtBlockInfo.getString("FC_ULTACT"));

                // graba el mensaje para devolver
                sMsg =
                    "Los datos que desea actualizar han sido modificados por " +
                    dtBlockInfo.getString("CD_OPE") + " el " +
                    dtBlockInfo.getString("FC_ULTACT");
              }
            }
            else {
              // graba el mensaje para devolver
              sMsg = "La informaci�n que trata de moficiar ha sido borrada";
              dtBlockInfo = null;
            }

          } //for por las QueryTool

          // si hay cambio lanza la excepci�n
          if (bCambio) {
            throw new BlockException(sMsg, lisBlock);

            // no hay cambio -> sigue con la operaci�n en curso
          }
          else {
            vParametros = (Lista) vParametros.elementAt(2);
          }
        }

      }

      // obtiene los resultados
      vResultado = doWork(opmode, vParametros);

      // libera la conexi�n
      closeConnection(con);

      // envia resultados
      if (vResultado != null) {
        vResultado.trimToSize();
      }
      out.writeObject(vResultado);
      out.flush();

      // cierra los streams
      out.close();

      // envia la excepci�n
    }
    catch (Exception e) {
      // libera la conexi�n
      closeConnection(con);

      e.printStackTrace();
      out.writeObject(e);
      out.flush();
      out.close();
    }
  }

  /**
   * Banco de pruebas en local de la l�gica de negocio del servlet
   *
   * @param opmode modo de operaci�n del servlet
   * @param vParametros vector de par�metros recibidos desde la applet
   */
  public Lista doDebug(int opmode, Lista vParametros) {
    Lista v = new Lista();
    try {
      jdcdriver = new JDCConnectionDriver(driverName, dbUrl, userName, userPwd);
      con = openConnection();
      v = doWork(opmode, vParametros);
    }
    catch (Exception e) {
      // error en modo debug
      e.printStackTrace();
    }
    closeConnection(con);
    return v;
  }

  /**
   * Establece las variables JDBC para testear el servlet en local
   *
   * @param driver driver
   * @param url url
   * @param uid usuario
   * @param pwd password
   */
  public void setJdbcEnvironment(String driver, String url, String uid,
                                 String pwd) {
    driverName = driver;
    dbUrl = url;
    userName = uid;
    userPwd = pwd;
  }

  /**
   * Traza en el log el mensaje leido
   *
   * @param msg Mensaje para trazar
   */
  public void trazaLog(Object msg) {
    // if (debugMode)
    // System_out.println( msg.toString() );
  }
}
