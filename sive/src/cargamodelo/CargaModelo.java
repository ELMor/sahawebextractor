
package cargamodelo;

import java.util.ResourceBundle;

import capp.CApp;
import comun.constantes;
import sapp.StubSrvBD;

public class CargaModelo
    extends CApp {

  final int servletSELECCION_X_CODIGO = 5;
  ResourceBundle res;
  final int servletSELECCION_X_DESCRIPCION = 6;

  StubSrvBD stubCliente = null;
//  final String strSERVLET = "servlet/SrvCarga";
  final String strSERVLET = constantes.strSERVLET_CARGA;

  public void init() {
    super.init();

  }

  public void start() {
    try {
      CApp a = (CApp)this;
      res = ResourceBundle.getBundle("cargamodelo.Res" + this.getIdioma());
      setTitulo(res.getString("msg1.Text"));
      VerPanel("", new DialCargaModelo(a));

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

}
