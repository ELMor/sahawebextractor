
package infconsenf;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosConsEnf
    extends CApp {
  ResourceBundle res;
  public CasosConsEnf() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("infconsenf.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;

    VerPanel(res.getString("msg2.Text"), new Pan_ConsEnf(a), true);
    VerPanel(res.getString("msg2.Text"));
  }

}
