package alarmas;

import java.io.Serializable;

public class DataIndEnf
    implements Serializable {
  protected String sCodInd = "";
  protected String sDesInd = "";
  protected String sCodEnf = "";
  protected String sDesEnf = "";

  public DataIndEnf() {
  }

  public DataIndEnf(String cod, String codenf) {

    sCodInd = cod;
    sCodEnf = codenf;
    sDesInd = null;
    sDesEnf = null;

  } //fin construct

  // se usa para el par (enf, indic) de mantalus
  public DataIndEnf(String cod, String des, String codenf, String desenf) {
    sCodInd = cod;
    sDesInd = des;
    sCodEnf = codenf;
    sDesEnf = desenf;
  }

  public String getCodInd() {
    return sCodInd;
  }

  public String getDesInd() {
    return sDesInd;
  }

  public String getCodEnf() {
    return sCodEnf;
  }

  public String getDesEnf() {
    return sDesEnf;
  }

}
