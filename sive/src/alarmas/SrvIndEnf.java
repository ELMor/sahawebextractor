package alarmas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

/*  Selecciones:
 *    Dado un Indicador, los datos de la tabla SIVE_INDiCADOR
 *    Dada una enfermedad, si existe en SIVE_INDICADOR (cdenf, desenf)*/

public class SrvIndEnf
    extends DBServlet {

  // modos de operaci�n ** para mantalus
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  final int servletOBTENER_X_CODIGO_IND = 7;
  final int servletOBTENER_X_DESCRIPCION_IND = 8;
  final int servletSELECCION_X_CODIGO_IND = 9;
  final int servletSELECCION_X_DESCRIPCION_IND = 0;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    //Nombres de tablas parametrizados seg�n la aplicaci�n
    //Por defecto toman nombres de EDO
    String sTablaInd = "SIVE_INDICADOR";

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = new CLista();
    DataIndEnf indenf = null;

    // campos
    String sCod = "";
    String sDes = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    //Segun el tipo SIVE utilizado tendremos una aplicaci�n distinta
    // y por tanto se utilizar�n tablas distintas
    String sTSive = param.getTSive();
    //Aplicaci�n centinelas
    if (sTSive.equals("C")) {
      sTablaInd = "SIVE_IND_ALAR_RMC";
    }

    //cargamos indenf, iIdioma, con que ha llegado en param
    indenf = (DataIndEnf) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      // like de Enfermedades para Mantalus
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        // peticion de trama
        if (param.getFilter().length() > 0) {

          if (opmode == servletSELECCION_X_CODIGO) { //codigo

            query = "select distinct a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
                + "from " + sTablaInd + " a, SIVE_PROCESOS b "
                + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
                + " a.CD_ENFCIE like ? and a.CD_ENFCIE > ? "
                + " order by a.CD_ENFCIE ";

          }
          else { //descripcion
            if ( (param.getIdioma() == CApp.idiomaPORDEFECTO)) {
              query =
                  "select distinct a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
                  + "from " + sTablaInd + " a, SIVE_PROCESOS b "
                  + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
                  + " b.DS_PROCESO like ? and a.CD_ENFCIE > ? "
                  + "order by a.CD_ENFCIE ";
            }
            else {
              query =
                  "select distinct a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
                  + "from " + sTablaInd + " a, SIVE_PROCESOS b "
                  + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
                  + " b.DSL_PROCESO like ? and a.CD_ENFCIE > ? "
                  + "order by a.CD_ENFCIE ";
            }
          }
          // peticion de la primera trama
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO) { //codigo

            query = "select distinct a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
                + "from " + sTablaInd + " a, SIVE_PROCESOS b "
                + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
                + " a.CD_ENFCIE like ? order by a.CD_ENFCIE ";

          }
          else { //descripcion
            if ( (param.getIdioma() == CApp.idiomaPORDEFECTO)) {
              query =
                  "select distinct a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
                  + "from " + sTablaInd + " a, SIVE_PROCESOS b "
                  + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
                  + " b.DS_PROCESO like ? "
                  + "order by a.CD_ENFCIE ";
            }
            else {
              query =
                  "select distinct a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
                  + "from " + sTablaInd + " a, SIVE_PROCESOS b "
                  + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
                  + " b.DSL_PROCESO like ? "
                  + "order by a.CD_ENFCIE ";
            }
          }
        }

        // lanza la query
        st = con.prepareStatement(query);
        // filtro: caja de entrada
        st.setString(1, indenf.getCodEnf().trim() + "%");
        // paginaci�n (codigo)
        if (param.getFilter().length() > 0) {
          st.setString(2, param.getFilter().trim());
        }
        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataIndEnf) data.lastElement()).getCodEnf());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          // a�ade un nodo
          sCod = rs.getString("CD_ENFCIE");
          String desPro = rs.getString("DS_PROCESO");
          String desLPro = rs.getString("DSL_PROCESO");

          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (desLPro != null)) {
            sDes = desLPro;
          }
          else {
            sDes = desPro;

          }
          data.addElement(new DataIndEnf
                          ("", "", sCod, sDes));

          i++;
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

        // select de Enfermedades para Mantalus
      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:

        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO) { //codigo

          query = "select distinct a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
              + "from " + sTablaInd + " a, SIVE_PROCESOS b "
              + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
              + " a.CD_ENFCIE = ? ";

        }
        else { //descripcion
          if ( (param.getIdioma() == CApp.idiomaPORDEFECTO)) {
            query = "select distinct a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
                + "from " + sTablaInd + " a, SIVE_PROCESOS b "
                + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
                + " b.DS_PROCESO = ?";
          }
          else {
            query =
                "select distinct a.CD_ENFCIE, , b.DS_PROCESO, b.DSL_PROCESO "
                + "from " + sTablaInd + " a, SIVE_PROCESOS b "
                + "WHERE  a.CD_ENFCIE = b.CD_ENFCIE AND"
                + " b.DSL_PROCESO = ?";
          }
        }

        // lanza la query
        st = con.prepareStatement(query);
        //caja de entrada
        st.setString(1, indenf.getCodEnf().trim());
        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {

          // a�ade un nodo
          sCod = rs.getString("CD_ENFCIE");
          String des = rs.getString("DS_PROCESO");
          String desL = rs.getString("DSL_PROCESO");

          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
              && (desL != null)) {
            sDes = desL;
          }
          else {
            sDes = des;

          }
          data.addElement(new DataIndEnf
                          (sCod, sDes));
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;
//*****************************************************************
         // like de Indicadores para Mantalus
      case servletSELECCION_X_CODIGO_IND:
      case servletSELECCION_X_DESCRIPCION_IND:

        // peticion de trama
        if (param.getFilter().length() > 0) {

          if (opmode == servletSELECCION_X_CODIGO_IND) { //codigo

            query = "select  "
                + "CD_INDALAR, DS_INDALAR "
                + "from " + sTablaInd + " "
                + "where CD_INDALAR like ? and "
                + "CD_ENFCIE = ? and CD_INDALAR > ? "
                + "order by CD_INDALAR";
          }
          else { //descripcion

            query = "select  "
                + "CD_INDALAR, DS_INDALAR "
                + "from " + sTablaInd + " "
                + "where DS_INDALAR like ? and "
                + "CD_ENFCIE = ? and CD_INDALAR > ? "
                + "order by CD_INDALAR";
          }
          // peticion de la primera trama
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO_IND) { //codigo
            query = "select  "
                + "CD_INDALAR, DS_INDALAR "
                + "from " + sTablaInd + " "
                + "where CD_INDALAR like ? and "
                + "CD_ENFCIE = ? "
                + "order by CD_INDALAR";
          }
          else { //descripcion
            query = "select  "
                + "CD_INDALAR, DS_INDALAR "
                + "from " + sTablaInd + " "
                + "where DS_INDALAR like ? and "
                + "CD_ENFCIE = ? "
                + "order by CD_INDALAR";
          }
        }

        // lanza la query
        st = con.prepareStatement(query);
        // filtro: caja de entrada
        st.setString(1, indenf.getCodInd().trim() + "%");
        //condicion: la enfermedad
        st.setString(2, indenf.getCodEnf().trim());

        // paginaci�n (codigo)
        if (param.getFilter().length() > 0) {
          st.setString(3, param.getFilter().trim());
        }
        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataIndEnf) data.lastElement()).getCodInd());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          // a�ade un nodo
          data.addElement(new DataIndEnf
                          (rs.getString("CD_INDALAR"),
                           rs.getString("DS_INDALAR"),
                           "", ""));
          i++;
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;
        // select de Indicadores para Mantalus
      case servletOBTENER_X_CODIGO_IND:
      case servletOBTENER_X_DESCRIPCION_IND:

        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO_IND) { //codigo
          query = "select "
              + "CD_INDALAR, DS_INDALAR "
              + "from " + sTablaInd + " "
              + "where CD_INDALAR = ? "
              + "and CD_ENFCIE = ? ";
        }
        else { //descripcion
          query = "select "
              + "CD_INDALAR, DS_INDALAR "
              + "from " + sTablaInd + " "
              + "where DS_INDALAR = ? "
              + "and CD_ENFCIE = ? ";
        }

        // lanza la query
        st = con.prepareStatement(query);
        //caja de entrada
        st.setString(1, indenf.getCodInd().trim());
        //condicion: la enfermedad
        st.setString(2, indenf.getCodEnf().trim());

        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {
          // a�ade un nodo
          data.addElement(new DataIndEnf
                          (rs.getString("CD_INDALAR"),
                           rs.getString("DS_INDALAR"),
                           "", ""));
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

    } //fin de switch.

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
