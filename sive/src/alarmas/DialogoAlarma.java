package alarmas;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CDialog;
import capp.CMessage;
import obj.CTextField;

public class DialogoAlarma
    extends CDialog {

  Panel pnl = new Panel();
  ResourceBundle res = ResourceBundle.getBundle("alarmas.Res" + app.getIdioma());
  XYLayout xYLayout = new XYLayout();
  TextField txtSem = new TextField();
  Label lblSem = new Label();
//  TextField txtValor = new TextField();

  CTextField txtValor = new CTextField
      ("S", //Obligatorio
       "N", //Campo num�rico
       10, //irrelevante
       8, //Long parte entera
       2, //Long parte decimal
       "", "",
       "",
       "", "",
       "");

  Label lblValor = new Label();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  final String imgACEPTAR = "images/aceptar.gif";
  final String imgCANCELAR = "images/cancelar.gif";

  protected Panel_Mantalus pnlMantalus;

  DialogoAlarma_actionListener actionListener = new
      DialogoAlarma_actionListener(this);

  public boolean bAceptar;
  protected boolean bModificar = false; //por defecto es alta

  public DialogoAlarma(CApp app, Panel_Mantalus p, boolean b) {
    super(app);
    try {
      pnlMantalus = p;
      bModificar = b; //si es modificar va a true
      jbInit();
      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // init
  void jbInit() throws Exception {

    setSize(236, 117);
    setTitle(res.getString("msg15.Text"));
    xYLayout.setHeight(117);
    txtSem.setBackground(new Color(255, 255, 150));
    xYLayout.setWidth(236);

    lblSem.setText(res.getString("lblSem.Text"));
    txtValor.setBackground(new Color(255, 255, 150));
    lblValor.setText(res.getString("lblValor.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));

    btnAceptar.setActionCommand("Aceptar");
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));

    pnl.setLayout(xYLayout);

    btnAceptar.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
    btnCancelar.setImageURL(new URL(app.getCodeBase(), imgCANCELAR));

    //tabular
    pnl.add(lblSem, new XYConstraints(7, 12, 65, 20));
    pnl.add(txtSem, new XYConstraints(72, 12, 49, 21));
    pnl.add(lblValor, new XYConstraints(7, 42, 63, 20));
    pnl.add(txtValor, new XYConstraints(73, 39, 109, 21));
    pnl.add(btnAceptar, new XYConstraints(32, 75, 80, 30));
    pnl.add(btnCancelar, new XYConstraints(138, 75, 80, 30));
    add(pnl, BorderLayout.CENTER);

    //escuchadores
    btnCancelar.addActionListener(actionListener);
    btnAceptar.addActionListener(actionListener);

    if (bModificar) {
      txtSem.setEnabled(false);
    }
    else {
      txtSem.setEnabled(true);

    }
  }

  //Formatea Semana con 2 digitos
  void Formatear_Semana() {
    if (txtSem.getText().length() == 1) {
      txtSem.setText("0" + txtSem.getText());
    }
  }

  //Validar Valor
  boolean isValidValor() {
    /*
        CMessage msgBox;
        boolean bValor=true;
        try {
          Float fValor = new Float(txtValor.getText());
          if (fValor.floatValue() < 0)
              bValor=false;
          if (fValor.floatValue() > 99999999.99)
              bValor=false;
        }catch (Exception e) {
          bValor=false;
        }
        if (!bValor) {
         msgBox = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg16.Text"));
            msgBox.show();
            msgBox = null;
        }
        return bValor;
     */
    return true;
  }

  //Validar Semana
  boolean isNumSemana() {

    CMessage msgBox;
    boolean bSem = true; //Es Numerico

    try {
      Integer fSem = new Integer(txtSem.getText());

    }
    catch (Exception e) {
      bSem = false;
    }

    if (!bSem) {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg17.Text"));
      msgBox.show();
      msgBox = null;
    }
    return bSem;
  }

  //PULSAR UN BOTON (ACEPTAR O CANCELAR)*********
  void btn_actionListener(ActionEvent e) {
    boolean bDispose = true;
    String sMsg = "";
    CMessage msgBox;

    // aceptar
    if (e.getActionCommand().equals("Aceptar")) {

      // datos m�nimos
      if (txtSem.getText().length() == 0 || txtValor.getText().length() == 0) {
        bDispose = false;
        sMsg = res.getString("msg18.Text");
      }
      else {

        //Si es Numerica la semana
        if (bDispose) {

          bDispose = isNumSemana();
        }
        //Respecto a Valor
        if (bDispose) {

          bDispose = isValidValor();
        }
        //Longitud de la semana
        if (txtSem.getText().length() > 2) {
          bDispose = false;
          sMsg = res.getString("msg19.Text");
        }
        //Formatear Semana
        Formatear_Semana();

        // Semana no repetida
        if ( (bDispose) && (!bModificar)) {
          for (int j = 0; j < pnlMantalus.listaValoresPantalla.size(); j++) {
            if ( ( (DataAlarma) pnlMantalus.listaValoresPantalla.elementAt(j)).
                getSem().equals(txtSem.getText())) {
              bDispose = false;
              sMsg = res.getString("msg20.Text");
              break;
            }
          }
        }

      } //fin de if-else
      // mensaje de aviso
      if (sMsg != "") {
        msgBox = new CMessage(this.app, CMessage.msgAVISO, sMsg);
        msgBox.show();
        msgBox = null;
      }
      if (bDispose) {
        bAceptar = true; //SE LO PASO A PANEL

      }
    } //fin de si pulso aceptar

    //si es cancelar, o aceptar con datos OK, la escondo
    if (bDispose) {
      dispose();
    }
  } //fin de btn

  //Para poner solicitar los datos desde el panel********
  String getSem() {
    return txtSem.getText();
  }

  Float getValor() {
    Float fValor = new Float(txtValor.getText());
    return fValor;
  }

  //*****************************************************

   //Cuando pulso Modificar en el panel, para sacar los datos
   //en el Dialog******************************************
  void setSem(String cadena) {
    txtSem.setText(cadena);
  }

  void setValor(Float numero) {
    txtValor.setText(numero.toString());
  }
  //*****************************************************

} //FIN DE LA CLASE**********************************************

// escuchador de los eventos de los botones
class DialogoAlarma_actionListener
    implements ActionListener {
  DialogoAlarma adaptee;

  DialogoAlarma_actionListener(DialogoAlarma adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btn_actionListener(e);
  }
}
