
package alarmas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;

public class SrvIndAno
    extends DBServlet {

  // modos de operaci�n
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    //Nombres de tablas parametrizados seg�n la aplicaci�n
    //Por defecto toman nombres de EDO
    String sTablaInd = "SIVE_INDICADOR";
    String sTablaIndAno = "SIVE_IND_ANO";

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    String sCondPerfil = "";

    // objetos de datos
    CLista data = new CLista();
    DataIndAno indano = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    //Segun el tipo SIVE utilizado tendremos una aplicaci�n distinta
    // y por tanto se utilizar�n tablas distintas
    String sTSive = param.getTSive();
    //Aplicaci�n centinelas
    if (sTSive.equals("C")) {
      sTablaInd = "SIVE_IND_ALAR_RMC";
      sTablaIndAno = "SIVE_IND_ALARRMC_ANO";

    }

    indano = (DataIndAno) param.firstElement();

    switch (param.getPerfil()) {
      case 1:
      case 2: // ccaa
        sCondPerfil = sCondPerfil;
        break;
      case 3: // niv 1
        sCondPerfil = sCondPerfil +
            //"and b.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) ";
            "and b.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) ";
        break;
      case 4: // niv 2
        sCondPerfil = sCondPerfil +
            //"and b.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) " +
            //"and b.CD_NIVEL_2 in (select CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) ";
            "and b.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) " +
            "and b.CD_NIVEL_2 in (select CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) ";
        break;
    }

    // modos de operaci�n
    switch (opmode) {

      // b�squeda de Indicadores Informados
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        // peticion de tramas did�stintas a la primera
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_X_CODIGO) { //codigo
            query = "select a.CD_INDALAR, b.DS_INDALAR, a.CD_ANOEPI "
                + "from " + sTablaIndAno + " a, " + sTablaInd + " b "
                + "WHERE  a.CD_INDALAR = b.CD_INDALAR AND"
                + " a.CD_INDALAR like ? and a.CD_INDALAR > ? "
                + sCondPerfil
                + " order by a.CD_INDALAR";
          }
          else { //descripcion
            query = "select a.CD_INDALAR, b.DS_INDALAR, a.CD_ANOEPI "
                + "from " + sTablaIndAno + " a, " + sTablaInd + " b "
                + "WHERE  a.CD_INDALAR = b.CD_INDALAR AND"
                + " b.DS_INDALAR like ? and b.DS_INDALAR > ? "
                + sCondPerfil
                + " order by b.DS_INDALAR";

            // peticion de la primera trama
          }
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO) { //codigo
            query = "select a.CD_INDALAR, b.DS_INDALAR, a.CD_ANOEPI "
                + "from " + sTablaIndAno + " a, " + sTablaInd + " b "
                + "WHERE  a.CD_INDALAR = b.CD_INDALAR "
                + " and a.CD_INDALAR like ? "
                + sCondPerfil
                + " order by a.CD_INDALAR";
          }
          else { //descripcion
            query = "select a.CD_INDALAR, b.DS_INDALAR, a.CD_ANOEPI "
                + "from " + sTablaIndAno + " a, " + sTablaInd + " b "
                + "WHERE  a.CD_INDALAR = b.CD_INDALAR "
                + " and b.DS_INDALAR like ? "
                + sCondPerfil
                + " order by b.DS_INDALAR";
          }
        }

        // lanza la query
        st = con.prepareStatement(query);
        // filtro
        st.setString(1, indano.getCod().trim() + "%"); //caja entrada

        // paginaci�n (el codigo)
        if (param.getFilter().length() > 0) {
          //C�digo para paginacion
          st.setString(2, param.getFilter().trim());
          //Datos de autorizaciones

          switch (param.getPerfil()) {
            case 2: // ccaa
              break;
            case 3: // niv 1
              st.setString(3, param.getLogin().trim());
              break;
            case 4: // niv 2
              st.setString(3, param.getLogin().trim());
              st.setString(4, param.getLogin().trim());
              break;
          }
        }

        else {
          //Datos de autorizaciones
          switch (param.getPerfil()) {
            case 2: // ccaa
              break;
            case 3: // niv 1
              st.setString(2, param.getLogin().trim());
              break;
            case 4: // niv 2
              st.setString(2, param.getLogin().trim());
              st.setString(3, param.getLogin().trim());
              break;
          }
        }

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataIndAno) data.lastElement()).getCod());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          // a�ade un nodo
          data.addElement(new DataIndAno
                          (rs.getString("CD_INDALAR"),
                           rs.getString("DS_INDALAR"),
                           rs.getString("CD_ANOEPI")));
          i++;
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:

        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO) { //codigo
          query = "select a.CD_INDALAR, b.DS_INDALAR, a.CD_ANOEPI "
              + "from " + sTablaIndAno + " a, " + sTablaInd + " b "
              + "WHERE  a.CD_INDALAR = b.CD_INDALAR AND"
              + " a.CD_INDALAR = ? "
              + sCondPerfil;
        }
        else { //descripcion
          query = "select a.CD_INDALAR, b.DS_INDALAR, a.CD_ANOEPI "
              + "from " + sTablaIndAno + " a, " + sTablaInd + " b "
              + "WHERE  a.CD_INDALAR = b.CD_INDALAR AND "
              + " b.DS_INDALAR = ? "
              + sCondPerfil;

          // lanza la query
        }
        st = con.prepareStatement(query);
        st.setString(1, indano.getCod().trim());

        switch (param.getPerfil()) {
          case 2: // ccaa
            break;
          case 3: // niv 1
            st.setString(2, param.getLogin().trim());
            break;
          case 4: // niv 2
            st.setString(2, param.getLogin().trim());
            st.setString(3, param.getLogin().trim());
            break;
        }

        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {
          // a�ade un nodo
          data.addElement(new DataIndAno
                          (rs.getString("CD_INDALAR"),
                           rs.getString("DS_INDALAR"),
                           rs.getString("CD_ANOEPI")));
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }
}
