package alarmas;

import java.io.Serializable;

public class DataAlarma
    implements Serializable {
  protected String sAno = "";
  protected String sCodInd = "";
  protected String sSem = "";
  protected Float fValor = null;
  protected Float fCoef = null;

  protected boolean bHayDatos = false; //*************
  protected String sCodUltSem = "";

  public DataAlarma() {
  }

  //para hacer la select (peticion cliente)
  public DataAlarma(String codind, String ano) {
    sCodInd = codind;
    sAno = ano;
    sSem = null;
    fValor = null;
    fCoef = null;
  }

  //para recuperar datos
  public DataAlarma(String codind, String ano, String sem, Float valor,
                    Float coef) {
    sCodInd = codind;
    sAno = ano;
    sSem = sem;
    fValor = valor;
    fCoef = coef;
  }

  public String getAno() {
    return sAno;
  }

  public String getCodInd() {
    return sCodInd;
  }

  public String getSem() {
    return sSem;
  }

  public Float getValor() {
    return fValor;
  }

  public Float getCoef() {
    return fCoef;
  }

  //Funciones para diferenciar si hay datos de ese a�o o no

  public boolean getHayDatos() {
    return bHayDatos;
  }

  public void setHayDatos(boolean hayDatos) {
    bHayDatos = hayDatos;
  }

  //Funcion para fijar el num semanas de un a�o
  //Si no hay datos del a�o Servlet devuelve solo el num sem
  public void setCodUltSem(String codUltSem) {
    sCodUltSem = codUltSem;
  }

  //Funcion para recoger codigo de ultima semana
  public String getCodUltSem() {
    return sCodUltSem;
  }

}
