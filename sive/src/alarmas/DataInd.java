package alarmas;

import java.io.Serializable;

//soporta los datos de la ventana:
//Mantenimiento de Indicadores + los datos de vuelta

public class DataInd
    implements Serializable {
  protected String sCodInd = "";
  protected String sDesInd = "";
  protected String sCodEnf = "";
  protected String sDesEnf = "";
  protected String sCodNivel1 = "";
  protected String sDesNivel1 = "";
  protected String sCodNivel2 = "";
  protected String sDesNivel2 = "";
  protected String sChkActivo = "";
  protected String iTipo = "";
  protected String sSecuencial = "";
  protected String sIzquierda = "";

  public DataInd() {
  }

  //para la lista de valores
  // Tambi�n para que al a�adir el Servlet devuelva el c�digo generado
  public DataInd(String cod) {
    sCodInd = cod;
    sDesInd = null;
    sCodEnf = null;
    sDesEnf = null;
    sCodNivel1 = null;
    sDesNivel1 = null;
    sCodNivel2 = null;
    sDesNivel2 = null;
    sChkActivo = null;
    iTipo = null;
    sIzquierda = null;
    sSecuencial = null;
  }

  //busqueda concreta de Indicador (para la vuelta de la consulta o a�adir)
  public DataInd(String cod, String des,
                 String codenf, String desenf,
                 String cod1, String des1,
                 String cod2, String des2,
                 String chk, String itipo, String izquierda, String secuencial) {
    sCodInd = cod;
    sDesInd = des;
    sCodEnf = codenf;
    sDesEnf = desenf;
    sCodNivel1 = cod1;
    sDesNivel1 = des1;
    sCodNivel2 = cod2;
    sDesNivel2 = des2;
    sChkActivo = chk;
    iTipo = itipo;
    sIzquierda = izquierda;
    sSecuencial = secuencial;

  }

  //sin descripciones de niveles y de enfermo  (cuando se a�ade)
  public DataInd(String cod, String des,
                 String codenf,
                 String cod1,
                 String cod2,
                 String chk, String itipo) {
    sCodInd = cod;
    sDesInd = des;
    sCodEnf = codenf;
    sDesEnf = null;
    sCodNivel1 = cod1;
    sDesNivel1 = null;
    sCodNivel2 = cod2;
    sDesNivel2 = null;
    sChkActivo = chk;
    iTipo = itipo;
  }

  public String getCodInd() {
    return sCodInd;
  }

  public String getDesInd() {
    return sDesInd;
  }

  public String getCodEnf() {
    return sCodEnf;
  }

  public String getDesEnf() {
    return sDesEnf;
  }

  public String getCodNivel1() {
    return sCodNivel1;
  }

  public String getDesNivel1() {
    return sDesNivel1;
  }

  public String getCodNivel2() {
    return sCodNivel2;
  }

  public String getDesNivel2() {
    return sDesNivel2;
  }

  public String getChkActivo() {
    return sChkActivo;
  }

  public String getiTipo() {
    return iTipo;
  }

  public String getsIzquierda() {
    return sIzquierda;
  }

  public String getsSecuencial() {
    return sSecuencial;
  }
}
