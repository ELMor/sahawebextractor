package alarmas;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Enumeration;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CTabla;
import comun.constantes;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import obj.CTextField;
import sapp.StubSrvBD;

public class Panel_Mantalus
    extends CDialog {

  DataInd indic = null;
  ResourceBundle res = ResourceBundle.getBundle("alarmas.Res" + app.getIdioma());

  //Guarda el c�digo del a�o de la �ltima vez que se perdi� el foco
  public String sAnoBk = "";
  //Guarda el c�digo del coeficiente de la �ltima vez que se perdi� el foco
  public String sCoefBk = "";
  //Guarda el c�digo del valor por defecto de la �ltima vez que se perdi� el foco
  public String sValDefBk = "";

  //modos de operaci�n de la ventana
  final int modoCAPTURAR_PAR = 0;
  final int modoOBTENER_DATOS = 7;
  final int modoMODIFICAR = 1;
  final int modoALTA = 3;
  final int modoESPERA = 5;

  final int modoALARMA_AUTOMATICA = 6;

  //modos servlets
  //para la alarma (SrvAlar)
  final int modoSELECT_ALAR = 3; //obtener datos
  final int modoMODIFICAR_MAXIVO = 0;
  //para enfermedad (SrvIndEnf) y para "Partir de" (SrvIndAno)
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  //para Indicador (SrvIndEnf)
  final int servletOBTENER_X_CODIGO_IND = 7;
  final int servletOBTENER_X_DESCRIPCION_IND = 8;
  final int servletSELECCION_X_CODIGO_IND = 9;
  final int servletSELECCION_X_DESCRIPCION_IND = 0;

  //constantes del panel.
  final String imgALTA2 = "images/alta2.gif";
  final String imgMODIFICACION2 = "images/modificacion2.gif";
  final String imgBORRAR2 = "images/baja2.gif";
  final String imgOBTENER = "images/obtener.gif";
  final String imgACTUALIZACION = "images/actualizar.gif";
  final String imgLUPA = "images/Magnify.gif";

  /*
    final String strSERVLET_ALAR = "servlet/SrvAlar";
    final String strSERVLET_INDANO = "servlet/SrvIndAno";
   */

  final String strSERVLET_ALAR = constantes.strSERVLET_ALAR;
  final String strSERVLET_INDANO = constantes.strSERVLET_INDANO;

  // par�metros
  protected int modoOperacion = modoCAPTURAR_PAR;
  protected int modoOperacionBk = modoCAPTURAR_PAR;

  CLista listaValoresPantalla = null;

  protected StubSrvBD stubCliente = null;

  protected final String strCabeceraLista = res.getString("msg22.Text");

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/alta2.gif",
      "images/modificacion2.gif",
      "images/baja2.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  //controles
  XYLayout xYLayout1 = new XYLayout();
  Label lblAno = new Label();
  Label lblInd = new Label();
  Label lblComentario = new Label();
  Label lblCoef = new Label();
  TextField txtAno = new TextField();
  TextField txtCodInd = new TextField();
  TextField txtCodIndAux = new TextField();
  TextField txtDesInd = new TextField();

//  TextField txtCoef = new TextField();
  CTextField txtCoef = new CTextField
      ("S", //Obligatorio
       "N", //Campo num�rico
       10, //irrelevante
       3, //Long parte entera
       3, //Long parte decimal
       "", "",
       "",
       "", "",
       "");

  ButtonControl btnBuscarIndAux = new ButtonControl();
  TextField txtDesIndAux = new TextField();
  ButtonControl btn2 = new ButtonControl();
  ButtonControl btn4 = new ButtonControl();
  CTabla tbl = new CTabla();

  //  ESCUCHADOR PULSAR BOTONES:
  Pan_MantalusbtnActionListener btnActionListener = new
      Pan_MantalusbtnActionListener(this);
  // ESCUCHADOR DE CLICK SOBRE TABLA:
  Pan_Mantalus_tabla_actionAdapter tablaActionListener = new
      Pan_Mantalus_tabla_actionAdapter(this);
  //Esc p�rdida de foco
  Panel_MantalusFocusAdapter focusListener = new Panel_MantalusFocusAdapter(this);
  //Esc. pulsaci�n tecla
  Panel_MantalusTxt_keyAdapter keyListener = new Panel_MantalusTxt_keyAdapter(this);

  ButtonControl btnCancelar = new ButtonControl();
  Label lblPorDefecto = new Label();

//  TextField txtValDef = new TextField();

  CTextField txtValDef = new CTextField
      ("S", //Obligatorio
       "N", //Campo numerico
       10, //irrelevante
       3, //Long parte entera
       3, //Long parte decimal
       "", "",
       "",
       "", "",
       "");

  //Formato del valor de alarma en tabla
  DecimalFormat miFormato;
  //Formato del valor del umbral en tabla
  DecimalFormat formatoUmbral;
  DecimalFormatSymbols misSimbolos;

  //___________________________________________________________________________

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      // modo capturar el par Enfermedad-Indicador
      case modoCAPTURAR_PAR:
        txtAno.setEnabled(false);
        txtCoef.setEnabled(false);
        txtValDef.setEnabled(false);
        txtCodIndAux.setEnabled(false);
        btnBuscarIndAux.setEnabled(false);

        btn2.setEnabled(false);
        btn4.setEnabled(false);
        tbl.setEnabled(false);
        btnCancelar.setEnabled(true);
//        InicPar();
        Resetear();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        //Enf, Indic,Ano, Btn
      case modoOBTENER_DATOS:
        txtAno.setEnabled(true);
        txtCoef.setEnabled(false);
        txtValDef.setEnabled(false);
        txtCodIndAux.setEnabled(false);
        btnBuscarIndAux.setEnabled(false);
        tbl.setEnabled(false);
        btn2.setEnabled(false);
        btn4.setEnabled(false);
        Resetear();
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //this.doLayout();
        break;

        //Enf, Indic,Ano, Btn
      case modoALARMA_AUTOMATICA:
        txtAno.setEnabled(true);
        txtCoef.setEnabled(false);
        txtValDef.setEnabled(false);
        txtCodIndAux.setEnabled(false);
        btnBuscarIndAux.setEnabled(false);
        tbl.setEnabled(true);
        btn2.setEnabled(false);
        btn4.setEnabled(false);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //this.doLayout();
        break;

      case modoMODIFICAR:
        txtAno.setEnabled(true);
        txtCoef.setEnabled(true);
        txtValDef.setEnabled(true);
        txtCodIndAux.setEnabled(false);
        btnBuscarIndAux.setEnabled(false);
        tbl.setEnabled(true);
        btn2.setEnabled(true);
        btn4.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //this.doLayout();
        break;

      case modoALTA:
        txtCodInd.setEnabled(false);
        txtAno.setEnabled(true);
        txtCoef.setEnabled(true);
        txtValDef.setEnabled(true);
        txtCodIndAux.setEnabled(false);
        btnBuscarIndAux.setEnabled(true);
        tbl.setEnabled(true);
        btn2.setEnabled(true);
        btn4.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //this.doLayout();
        break;

      case modoESPERA:
        btn2.setEnabled(false);
        btn4.setEnabled(false);
        btnBuscarIndAux.setEnabled(false);
        txtCodInd.setEnabled(false);
        txtCodIndAux.setEnabled(false);
        txtAno.setEnabled(false);
        txtCoef.setEnabled(false);
        txtValDef.setEnabled(false);
        tbl.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.doLayout(); //lo he puesto!
        break;
    }
  } //fin Inicializa

  /*
    // actualiza el estado del PAR
    protected void InicPar() {
      if (txtDesEnf.getText().length() == 0) {
        txtCodInd.setText("");
        txtDesInd.setText("");
        btnBuscarInd.setEnabled(false);
      }
      else {
        btnBuscarInd.setEnabled(true);
      }
    }//fin InicPar
   */

  //___________________________________________________________________________
  // Resetea toda la pantalla(excepto PAR, que se resetea solo)
  public void Resetear() {

    txtAno.setText("");
    txtCodIndAux.setText("");
    txtDesIndAux.setText("");
    txtCoef.setText("0.0");
    txtValDef.setText("0.0");

    BorrarTabla();

  } //fin ResetearInferior

  //___________________________________________________________________________

  // contructor
  public Panel_Mantalus(CApp a, DataInd datEntrada) {
    super(a);

    try {
      indic = datEntrada;
      this.setTitle(res.getString("this.Title"));
      jbInit();
      //apunta a SrvAlar  p.ej.
      stubCliente = new StubSrvBD(new URL(this.app.getURL() + strSERVLET_ALAR));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //___________________________________________________________________________
//Component initialization

  private void jbInit() throws Exception {

    xYLayout1.setWidth(539);
    xYLayout1.setHeight(389);
    this.setLayout(xYLayout1);
    this.setSize(539, 389);

    lblAno.setText(res.getString("lblAno.Text"));
    lblInd.setText(res.getString("lblInd.Text"));
    lblComentario.setText(res.getString("lblComentario.Text"));
    lblCoef.setText(res.getString("lblCoef.Text"));

    txtCodInd.setName("codi");
    txtCodInd.setBackground(new Color(255, 255, 150));
    txtCodIndAux.setName("codiaux");
    txtCoef.setName("coef");
    txtCoef.setBackground(new Color(255, 255, 150));

    txtAno.setName("ano");
    txtAno.setBackground(new Color(255, 255, 150));

    txtValDef.setName("valdef");

//    btnBuscarInd.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnBuscarIndAux.setActionCommand("BuscarIndAux");
//    btnBuscarIndAux.setImageURL(new URL(app.getCodeBase(), imgLUPA));
//    btn1.setImageURL(new URL(app.getCodeBase(), imgALTA2));
    btn2.setActionCommand("Modificar");
//    btn2.setImageURL(new URL(app.getCodeBase(), imgMODIFICACION2));
//    btn3.setImageURL(new URL(app.getCodeBase(), imgBORRAR2));
    btn4.setActionCommand("Aceptar");
    btn4.setLabel(res.getString("btn4.Label"));
//    btn4.setImageURL(new URL(app.getCodeBase(), imgACTUALIZACION));

    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    lblPorDefecto.setText(res.getString("lblPorDefecto.Text"));
    txtCodInd.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        txtCodInd_actionPerformed(e);
      }
    });
    imgs.CargaImagenes();

    btnBuscarIndAux.setImage(imgs.getImage(0));
    btn2.setImage(imgs.getImage(2));
    btn4.setImage(imgs.getImage(4));
    btnCancelar.setImage(imgs.getImage(5));

    // configuraci�n de la tabla
//    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(strCabeceraLista[app.getIdioma()], '\n') );
    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        strCabeceraLista, '\n'));
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "120\n120\n120"), '\n'));
    tbl.setNumColumns(3);

    //tabular
    this.add(lblInd, new XYConstraints(34, 20, 65, -1));
    this.add(txtCodInd, new XYConstraints(106, 20, 123, 22));
    this.add(txtDesInd, new XYConstraints(240, 20, 197, -1));
    this.add(lblAno, new XYConstraints(34, 54, 47, 20));
    this.add(txtAno, new XYConstraints(99, 54, 48, -1));
    this.add(lblCoef, new XYConstraints(174, 54, 71, 20));
    this.add(txtCoef, new XYConstraints(249, 54, 65, -1));
    this.add(txtValDef, new XYConstraints(154, 88, 122, -1));
    this.add(lblComentario, new XYConstraints(34, 120, 114, -1));
    this.add(txtCodIndAux, new XYConstraints(154, 120, 122, 22));
    this.add(btnBuscarIndAux, new XYConstraints(286, 120, -1, -1));
    this.add(txtDesIndAux, new XYConstraints(317, 120, 197, -1));
    this.add(tbl, new XYConstraints(34, 165, 366, 142));
    this.add(btn2, new XYConstraints(436, 225, -1, -1));
    this.add(btn4, new XYConstraints(296, 319, 98, 30));
    this.add(btnCancelar, new XYConstraints(416, 319, 98, 30));
    this.add(lblPorDefecto, new XYConstraints(34, 88, 104, -1));

    //establece los escuchadores

    btn2.addActionListener(btnActionListener);
    btn4.addActionListener(btnActionListener);
    btnBuscarIndAux.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);
    txtAno.addFocusListener(focusListener);
    txtCoef.addFocusListener(focusListener);
    txtValDef.addFocusListener(focusListener);
    txtAno.addKeyListener(keyListener);

    // controles con estado fijo
    txtCodInd.setEnabled(false);
    txtDesInd.setEnabled(false);
    txtDesInd.setEditable(false);
    txtDesIndAux.setEnabled(false);
    txtDesIndAux.setEditable(false);

    // establece el modo de operaci�n
//    this.modoOperacion = modoCAPTURAR_PAR; //?????????????????

    // establece el modo de operaci�n
    //indicador alarma autom�tica
    if (indic.getiTipo().equals("A")) {
      this.modoOperacion = modoALARMA_AUTOMATICA;
      Resetear();
    }
    //Manual
    else {
      this.modoOperacion = modoOBTENER_DATOS;
      tbl.addActionListener(tablaActionListener);
    }

    //Ajuste del formato para los n�meros reales de alarmas que van a la tabla
    miFormato = new DecimalFormat("########.##");
    DecimalFormatSymbols misSimbolos = miFormato.getDecimalFormatSymbols();
    //Por defecto, considera separador parte entera y decimal el , (Espa�a)
    //Para poner el '.' como car�cter separador , es necesario aplicarle el DecimalFormatSymbols
    misSimbolos.setDecimalSeparator('.');
    miFormato.setDecimalFormatSymbols(misSimbolos);

    //Ajuste del formato para los n�meros reales de  umbrales que van a la tabla
    //Un d�gito m�s pues puede ser hasta
    formatoUmbral = new DecimalFormat("#########.##");
    DecimalFormatSymbols simbolos = formatoUmbral.getDecimalFormatSymbols();
    simbolos.setDecimalSeparator('.');
    formatoUmbral.setDecimalFormatSymbols(simbolos);

    Inicializar();

  } //fin jbInit

  //___________________________________________________________________________

  //BOTONES*****************************************
  // procesa opci�n a�adir un item*******************
  public void btn1_actionPerformed(ActionEvent evt) {

    if (txtCoef.getText().trim().equals("")
        && listaValoresPantalla.isEmpty()) {
      txtCoef.setText("0");

//     if (txtCoef.getText().trim().equals("")
      //^^^^^^^^
    }
    if (txtCoef.getText().trim().equals("")

        && !listaValoresPantalla.isEmpty()) {
      DataAlarma datCapturarDato = (DataAlarma) listaValoresPantalla.elementAt(
          listaValoresPantalla.size() - 1);
      txtCoef.setText(datCapturarDato.getCoef().toString());

    }
    DialogoAlarma dial = new DialogoAlarma(app, this, false);
    dial.show();

    if (dial.bAceptar) {

      listaValoresPantalla.addElement(new DataAlarma
                                      (txtCodInd.getText(),
                                       txtAno.getText(),
                                       dial.getSem(),
                                       dial.getValor(),
                                       new Float(txtCoef.getText()) //^^^^^^^^
                                       ));

      AddLineaTabla();

    }
    dial = null;
  } //fin btn1

  //___________________________________________________________________________

  // procesa opci�n modificar***********************
  public void btn2_actionPerformed(ActionEvent evt) {

    DialogoAlarma dial;
    int indice = tbl.getSelectedIndex();

    if (indice != BWTEnum.NOTFOUND) {

      DataAlarma datValor = (DataAlarma) listaValoresPantalla.elementAt(indice);

      dial = new DialogoAlarma(app, this, true);

      // graba el valor a modificar
      dial.setSem(datValor.getSem());
      dial.setValor(new Float(datValor.getValor().floatValue()));
      dial.show();

      // actualiza los datos en la lista
      if (dial.bAceptar) {
        listaValoresPantalla.removeElementAt(indice);
        listaValoresPantalla.insertElementAt
            (new DataAlarma(txtCodInd.getText(), txtAno.getText(),
                            dial.getSem(), dial.getValor(),
                            new Float(txtCoef.getText())), //^^^^^^^^^^^^^^
             indice);

        // repinta la tabla
        BorrarTabla();
        PintarTabla();
      }

      dial = null;
    }

  } //fin btn2

  //___________________________________________________________________________

  // procesa opci�n borrar***********************
  public void btn3_actionPerformed(ActionEvent evt) {
    CMessage msgBox = null;

    if (tbl.getSelectedItem() != null) {
      CMessage dialogo = new CMessage(this.app, 2, res.getString("msg21.Text"));
      dialogo.show();
      try {
        if (dialogo.getResponse() == true) {
          listaValoresPantalla.removeElementAt(tbl.getSelectedIndex());
          BorrarTabla();
          if (!listaValoresPantalla.isEmpty()) {
            PintarTabla();
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  } //fin btn3

  //___________________________________________________________________________
  // procesa opci�n aceptar***********************
  public void btn4_actionPerformed(ActionEvent evt) {
    CMessage msgBox = null;
    CLista listaVacia = null;

    this.modoOperacionBk = this.modoOperacion;

    try {
      this.modoOperacion = modoESPERA;
      Inicializar();

      if (txtCoef.getText().trim().equals("") //^^^^^^
          && !listaValoresPantalla.isEmpty()) {
        DataAlarma datCapturarDato = (DataAlarma) listaValoresPantalla.
            elementAt(listaValoresPantalla.size() - 1);
        txtCoef.setText(datCapturarDato.getCoef().toString());

      }

      //Todo Borrado
      if (listaValoresPantalla.isEmpty()) {

        //paso solo un registro con CodInd y Ano.
        listaVacia = new CLista();
        listaVacia.addElement(new DataAlarma(txtCodInd.getText(),
                                             txtAno.getText()));

        listaVacia.setIdioma(app.getIdioma());
        listaVacia.setLogin(app.getLogin());
        listaVacia.setPerfil(app.getPerfil());
        listaVacia.setTSive(app.getTSive());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_ALAR));
        this.stubCliente.doPost(modoMODIFICAR_MAXIVO, listaVacia);
        /*
                 SrvAlar servlet = new SrvAlar();
                 //Indica como conectarse a la b.datos
                 servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                  servlet.doDebug(modoMODIFICAR_MAXIVO, listaVacia);
         */

        listaVacia = null;

      }
      else {
        listaValoresPantalla.setIdioma(app.getIdioma());
        listaValoresPantalla.setLogin(app.getLogin());
        listaValoresPantalla.setPerfil(app.getPerfil());
        listaValoresPantalla.setTSive(app.getTSive());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_ALAR));
        this.stubCliente.doPost(modoMODIFICAR_MAXIVO, listaValoresPantalla);
        /*
                 SrvAlar servlet = new SrvAlar();
                 //Indica como conectarse a la b.datos
                 servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                  servlet.doDebug(modoMODIFICAR_MAXIVO, listaValoresPantalla);
         */

      }
      this.dispose();

      // error en el proceso
    }
    catch (Exception e) {
      this.modoOperacion = modoOperacionBk;
      Inicializar();
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

  } //fin btn4

  //___________________________________________________________________________

  // OBTENER DATOS******llamada al Srv**********************
  //Borrar Tabla
  protected void BorrarTabla() {
    if (tbl.countItems() > 0) {
      tbl.deleteItems(0, tbl.countItems() - 1);
    }
  }

  //___________________________________________________________________________

  //A�adir una linea a la Tabla
  protected void AddLineaTabla() {
    DataAlarma datUnaLinea;
    String datLineaEscribir = new String("");

    datUnaLinea = (DataAlarma) listaValoresPantalla.lastElement();

    datLineaEscribir = datUnaLinea.getAno() + " " + datUnaLinea.getSem() + "&" +
        miFormato.format(datUnaLinea.getValor().floatValue()) + "&" +
        formatoUmbral.format(datUnaLinea.getValor().floatValue() *
                             datUnaLinea.getCoef().floatValue());

    tbl.addItem(datLineaEscribir, '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)

  }

  //___________________________________________________________________________

  // rellena coef
  protected void PintarCoef() {
    DataAlarma datUnaLinea;
    datUnaLinea = (DataAlarma) listaValoresPantalla.firstElement();
    float fCoef = 0;

    txtCoef.setText(datUnaLinea.getCoef().toString());

  }

  //___________________________________________________________________________

  //Recargar ListaValoresPantalla con Coef
  protected void Recargar_ListaValoresPantalla_con_Coef() {

    int iTamLista = listaValoresPantalla.size();
    int indice = 0;
    String sSem = "";
    Float fValor = null;
    CMessage msgBox = null;

    try {
      for (indice = 0; indice < iTamLista; indice++) {

        DataAlarma datCapturarDatosRegistro = (DataAlarma) listaValoresPantalla.
            elementAt(indice);
        sSem = datCapturarDatosRegistro.getSem();
        fValor = datCapturarDatosRegistro.getValor();
        datCapturarDatosRegistro = null;
        listaValoresPantalla.removeElementAt(indice);

        listaValoresPantalla.insertElementAt
            (new DataAlarma(txtCodInd.getText(), txtAno.getText(),
                            sSem, fValor, new Float(txtCoef.getText())) //^^^^^^^^^^
             , indice);

      }
      // error en el proceso
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

  //___________________________________________________________________________

  //Recargar ListaValoresPantalla con Coef
  protected void Recargar_ListaValoresPantalla_con_ValDef() {

    int iTamLista = listaValoresPantalla.size();
    int indice = 0;
    String sSem = "";
    Float fValor = null;
    CMessage msgBox = null;

    try {
      for (indice = 0; indice < iTamLista; indice++) {

        DataAlarma datCapturarDatosRegistro = (DataAlarma) listaValoresPantalla.
            elementAt(indice);
        sSem = datCapturarDatosRegistro.getSem();
//        fValor = datCapturarDatosRegistro.getValor();
        //Todos con el valor por defecto
        fValor = new Float(txtValDef.getText());
        datCapturarDatosRegistro = null;
        listaValoresPantalla.removeElementAt(indice);

        listaValoresPantalla.insertElementAt
            (new DataAlarma(txtCodInd.getText(), txtAno.getText(),
                            sSem, fValor, new Float(txtCoef.getText())) //^^^^^^^^^^
             , indice);

      }
      // error en el proceso
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

  //___________________________________________________________________________

  //Recargar ListaValoresPantalla con Nuevo Ano
  protected void Recargar_ListaValoresPantalla_con_Ano() {
    int iTamLista = listaValoresPantalla.size();
    int indice = 0;
    String sSem = "";
    Float fValor = null;
    Float fCoef = null;
    CMessage msgBox = null;

    try {
      for (indice = 0; indice < iTamLista; indice++) {

        DataAlarma datCapturarDatosRegistro = (DataAlarma) listaValoresPantalla.
            elementAt(indice);
        sSem = datCapturarDatosRegistro.getSem();
        fValor = datCapturarDatosRegistro.getValor();
        fCoef = datCapturarDatosRegistro.getCoef();
        datCapturarDatosRegistro = null;
        listaValoresPantalla.removeElementAt(indice);

        listaValoresPantalla.insertElementAt
            (new DataAlarma(txtCodInd.getText(), txtAno.getText(),
                            sSem, fValor, fCoef), indice);

      }
      // error en el proceso
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

  //___________________________________________________________________________

  // rellena Tabla
  protected void PintarTabla() {
    DataAlarma datUnaLinea;
    String datLineaEscribir = new String("");
    Enumeration enum = listaValoresPantalla.elements();
    float fCoef = 0;

    datUnaLinea = (DataAlarma) listaValoresPantalla.firstElement();
    fCoef = datUnaLinea.getCoef().floatValue();

    while (enum.hasMoreElements()) {
      //Recogemos los datos de una l�nea
      datUnaLinea = (DataAlarma) (enum.nextElement());

      datLineaEscribir = datUnaLinea.getAno() + " " + datUnaLinea.getSem() +
          "&" +
          miFormato.format(datUnaLinea.getValor().floatValue()) + "&";
      datLineaEscribir = datLineaEscribir +
          formatoUmbral.format(datUnaLinea.getValor().floatValue() * fCoef);

      //A�adimos la l�nea a la tabla
      tbl.addItem(datLineaEscribir, '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)
    }

  }

  //___________________________________________________________________________

  // rellena la tabla y el coef
  protected void PintarPantalla() {
    BorrarTabla();
    PintarCoef();
    PintarTabla();
  }

  //___________________________________________________________________________

  //Realiza la Select y carga en listaValoresPantalla
  //Nota :modo NO es el modo de pantalla
  protected void ObtenerValores(int modo, DataAlarma datosUsar) {
    CMessage msgBox = null;
    CLista data = null;
    try {

      // modo espera
      this.modoOperacion = modoESPERA;
      Inicializar();
      // prepara los par�metros
      data = new CLista();

      //voy a pedir LOS DATOS de la pantalla o de Partir de (Ind, Ano):
      if (modo == modoMODIFICAR) {
        data.addElement(new DataAlarma(txtCodInd.getText(), txtAno.getText()));

      }
      else if (modo == modoALTA) {
        data.addElement(datosUsar);

      }

      // apunta al servlet principal
      data.setIdioma(this.app.getIdioma());
      data.setLogin(this.app.getLogin());
      data.setPerfil(this.app.getPerfil());
      data.setTSive(app.getTSive());
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_ALAR));
      listaValoresPantalla = (CLista) stubCliente.doPost(modoSELECT_ALAR, data);
      /*
               SrvAlar servlet = new SrvAlar();
               //Indica como conectarse a la b.datos
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
           listaValoresPantalla = (CLista) servlet.doDebug(modoSELECT_ALAR, data);
       */

      data = null;
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  } //fin

  //___________________________________________________________________________

  //Pulsar Obtener datos

  public void txtAnoFocusLost(FocusEvent evt) {
    CMessage msgBox = null;
    CLista data = null;
    DataAlarma priEle;
    String sCodUltSem = "";
    int iUltSem;

    //Si ha cambiado el c�digo indicado por usuario
    if (!txtAno.getText().equals(sAnoBk)) {

      txtCodIndAux.setText("");
      txtDesIndAux.setText("");
      BorrarTabla();

      // determina si los datos est�n completos(
      //en la func: Faltan datos)
      if (this.isDataValidObtenerDatos() && this.isValidAno()) {

        //Nota: llamada a esta func no modifica modo de pantalla
        ObtenerValores(modoMODIFICAR, new DataAlarma("", ""));

        //Tras llamada a ObtenerValores se actualiza listaValoresPantalla
        priEle = (DataAlarma) (listaValoresPantalla.firstElement());

        //Se recoge ultima semana del a�o epid.
        sCodUltSem = priEle.getCodUltSem();

        //Si existe ese a�o en b. datos
        if (! (sCodUltSem.equals(""))) {

          //Si habia alarmas de ese a�o
          if (priEle.getHayDatos() == true) {
            PintarPantalla();

            //indicador alarma autom�tica
            if (indic.getiTipo().equals("A")) {
              this.modoOperacion = modoALARMA_AUTOMATICA;
            }
            //Manual
            else {
              this.modoOperacion = modoMODIFICAR;
            }

            Inicializar();
          }
          //Si no habia alarmas de ese a�o
          //Se forma aa lista de elementos que ir�n a la tabla
          else {
            iUltSem = Integer.parseInt(sCodUltSem);
            listaValoresPantalla.removeAllElements();
            for (int k = 1; k < iUltSem + 1; k++) {
              String sCodSem = Integer.toString(k);

              if (sCodSem.length() == 1) {
                sCodSem = "0" + sCodSem;

                //????????????????????????????????????????????????????????????
                //    Estas dos inst solo  SI CAJAS COEF Y VAL DEF NOOO
                //ESTAN HABILITADOS ANTES DE OBTENER A�O
                //Se inicializan los campos de valor por defecto y coeficiente
              }
              txtValDef.setText("0.0");
              txtCoef.setText("0.0");
              //????????????????????????????????????????????????????????????

              // Coef y valor!!!!!!!!!!!!!
              DataAlarma elem = new DataAlarma(txtCodInd.getText(),
                                               txtAno.getText(),
                                               sCodSem,
                                               new Float(txtValDef.getText()),
                                               new Float(txtCoef.getText())); //^^^^^^^^
              listaValoresPantalla.addElement(elem);
            } //for

            PintarPantalla();
            //indicador alarma autom�tica
            if (indic.getiTipo().equals("A")) {
              this.modoOperacion = modoALARMA_AUTOMATICA;
            }
            //Manual
            else {
              this.modoOperacion = modoALTA;
            }

            Inicializar();
            txtCoef.requestFocus();

          } //else de no hay alarmas de ese a�o

        } //if existe a�o
        //Si jno hay a�o
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg23.Text"));
          msgBox.show();
          msgBox = null;

          //indicador alarma autom�tica
          if (indic.getiTipo().equals("A")) {
            this.modoOperacion = modoALARMA_AUTOMATICA;
            Resetear();
          }
          //Manual
          else {
            this.modoOperacion = modoOBTENER_DATOS;
          }

          Inicializar();
        }

      } //if datos pant correctos

      //Si datos pant no son correctos
      else { //faltan datos obligatorios en pantalla
        //indicador alarma autom�tica
        if (indic.getiTipo().equals("A")) {
          this.modoOperacion = modoALARMA_AUTOMATICA;
          Resetear();
        }
        //Manual
        else {
          this.modoOperacion = modoOBTENER_DATOS;
        }

        Inicializar();
      }
      //Guarda el a�o
      sAnoBk = txtAno.getText();

    } //if cambia ano
  } //fin de btnDatos

  //___________________________________________________________________________
  // comprueba que los datos (Enf, Ind, Ano) esten informados******
  protected boolean isDataValidObtenerDatos() {
    CMessage msgBox;
    boolean b = true;

    if (
        (txtAno.getText().length() > 0)) {
      b = true;
    }
    else { //faltan datos obligatorios
      b = false;
    }
    //mensaje:
    if (b == false) {
      msgBox = new CMessage(app, CMessage.msgAVISO, res.getString("msg24.Text "));
      msgBox.show();
      msgBox = null;
    }
    return b;
  }

  //___________________________________________________________________________
  //Validar Coef
  protected boolean isValidCoef() {
    /*
        CMessage msgBox;
        boolean bValor=true;
        try {
          Float fCoef = new Float(txtCoef.getText());  //^^^^^^^^
          if (fCoef.floatValue() < 0)
              bValor=false;
          if (fCoef.floatValue() > 999.999)
              bValor=false;
        }catch (Exception e) {
          bValor=false;}
        if (!bValor) {
         msgBox = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg25.Text"));
            msgBox.show();
            msgBox = null;
            txtCoef.selectAll();
        }
        return bValor;
     */
    return true;
  }

  //___________________________________________________________________________

  //Validar Valor por defecto
  boolean isValidValorPorDefecto() {
    /*
        CMessage msgBox;
        boolean bValor=true;
        try {
          Float fValor = new Float(txtValDef.getText());
          if (fValor.floatValue() < 0)
              bValor=false;
          if (fValor.floatValue() > 99999999.99)
              bValor=false;
        }catch (Exception e) {
          bValor=false;
        }
        if (!bValor) {
         msgBox = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg26.Text"));
            msgBox.show();
            msgBox = null;
        }
        return bValor;
     */
    return true;
  }

  //___________________________________________________________________________

  // comprueba Longitud de Campos de Entrada
  protected boolean isValidAno() {
    CMessage msgBox;
    boolean bAno = true;

    try {
      Integer iAno = new Integer(txtAno.getText());
      if (iAno.intValue() < 0) {
        bAno = false;
      }
      if (iAno.intValue() > 9999) {
        bAno = false;
      }
    }
    catch (Exception e) {
      bAno = false;
    }

    if (!bAno) {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg27.Text"));
      msgBox.show();
      msgBox = null;
      txtAno.selectAll();
    }
    return bAno;
  }

  //___________________________________________________________________________

  public void btnCancelar_actionPerformed(ActionEvent evt) {
    this.dispose();
  }

  //BOTONES DE BUSQUEDA
  //Obtiene la lista de ENFERMEDADES en SIVE_INDICADOR
  /*
    public void btnBuscarEnf_actionPerformed(ActionEvent evt) {
      DataIndEnf data;
      txtCodEnf.setText("");
      txtDesEnf.setText("");
      txtCodInd.setText("");
      txtDesInd.setText("");
      this.modoOperacion = modoESPERA;
      Inicializar();
      CListaEnf lista = new CListaEnf(app,
      "Enfermedades asociadas a Indicadores",
          stubCliente,
          strSERVLET_INDENF,
          servletOBTENER_X_CODIGO,
          servletOBTENER_X_DESCRIPCION,
          servletSELECCION_X_CODIGO,
          servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataIndEnf) lista.getComponente();
      //he traido datos
      if (data != null) {
         txtCodEnf.setText(data.getCodEnf());
         txtDesEnf.setText(data.getDesEnf());
      }
      this.modoOperacion = modoCAPTURAR_PAR;
      Inicializar();
    }//fin Enfermedades
   */

  // procesa la opci�n de b�squeda
  void btnBuscarIndAux_actionPerformed(ActionEvent evt) {

    DataIndAno data;
    this.modoOperacion = modoESPERA;
    Inicializar();

    CListaIndAno lista = new CListaIndAno(app,
                                          res.getString("msg28.Text"),
                                          stubCliente,
                                          strSERVLET_INDANO,
                                          servletOBTENER_X_CODIGO,
                                          servletOBTENER_X_DESCRIPCION,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION);
    lista.show();
    data = (DataIndAno) lista.getComponente();

    //he traido datos
    if (data != null) {
      txtCodIndAux.setText(data.getCod());
      txtDesIndAux.setText("(" + data.getAno() + ") " + data.getDes());

      ObtenerValores(modoALTA, new DataAlarma(data.getCod(), data.getAno()));

      PintarCoef();
      BorrarTabla();
      Recargar_ListaValoresPantalla_con_Ano();
      PintarTabla();

    }
    this.modoOperacion = modoALTA;
    Inicializar();
  }

  // perdida de foco de cajas de c�digos  (Partir de....)
  void txtCodIndAuxFocusLost(FocusEvent e) {
    /*
        //Datos de envio
        DataIndAno data;
        CLista param = null;
        CMessage msg;
        int modo = modoOperacion; //Modo regreso de pantalla
        String strServlet = null;
        int modoServlet = 0;
        TextField txt = (TextField) e.getSource();
        param = new CLista();
        param.setIdioma(app.getIdioma() );
        param.setPerfil(app.getPerfil() );
        param.setLogin(app.getLogin());
        // gestion de datos
        param.addElement(new DataIndAno(txtAno.getText()));
        strServlet = strSERVLET_INDANO;
        modoServlet = servletOBTENER_X_CODIGO;
        // busca el item
        if (param != null) {
          try {
            // consulta en modo espera
            modoOperacion = modoESPERA;
            Inicializar();
            stubCliente.setUrl(new URL(app.getURL() + strServlet));
            param = (CLista) stubCliente.doPost(modoServlet, param);
            // rellena los datos
            if (param.size() > 0) {
              // realiza el cast y rellena los datos
                data = (DataIndAno) param.firstElement();
              //he traido datos
              if (data != null) {
                txtCodIndAux.setText(data.getCod());
                txtDesIndAux.setText(data.getDes() + " - " + data.getAno());
         ObtenerValores(modoALTA, new DataAlarma (data.getCod(),data.getAno()));
                txtCoef.removeTextListener(txtTextListener);
                PintarCoef();
                txtCoef.addTextListener(txtTextListener);
                BorrarTabla();
                Recargar_ListaValoresPantalla_con_Ano();
                PintarTabla();
            }
            }
            // no hay datos
            else {
              msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
              msg.show();
              msg = null;
            }
          } catch(Exception ex) {
              msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
              msg.show();
              msg = null;
          }
          // consulta en modo normal
          modoOperacion = modo;
          Inicializar();
        }
     */
  }

  // perdida de foco de cajas de c�digos  (Partir de....)
  void txtCoefFocusLost(FocusEvent e) {

    //Si ha cambiado el c�digo indicado por usuario
    if (!txtCoef.getText().equals(sCoefBk)) { //^^^^^^^^^^^^

      if (this.modoOperacion == modoMODIFICAR
          || this.modoOperacion == modoALTA) {

        this.modoOperacionBk = this.modoOperacion;
        if (!txtCoef.getText().trim().equals("")) { //^^^^^^^^^^^^
          if (isValidCoef()) {
            BorrarTabla();
            Recargar_ListaValoresPantalla_con_Coef();
            if (!listaValoresPantalla.isEmpty()) {
              PintarTabla();
            }
          }
        } //fin de !vacio
        /* else if (txtCoef.getText().trim().equals("")
           && !listaValoresPantalla.isEmpty()) {
           this.txtCoef.removeTextListener(txtTextListener);
           DataAlarma datCapturarDato = (DataAlarma)listaValoresPantalla.elementAt(listaValoresPantalla.size()-1);
           txtCoef.setText(datCapturarDato.getCoef().toString());
           this.txtCoef.addTextListener(txtTextListener);
         }  */
        this.modoOperacion = this.modoOperacionBk;
        Inicializar();
      } //fin del modo

      sCoefBk = txtCoef.getText();
    }

  }

  // perdida de foco de cajas de c�digos  (Partir de....)
  void txtValDefFocusLost(FocusEvent e) {

    //Si ha cambiado el c�digo indicado por usuario
    if (!txtValDef.getText().equals(sValDefBk)) {

      if (this.modoOperacion == modoMODIFICAR
          || this.modoOperacion == modoALTA) {

        this.modoOperacionBk = this.modoOperacion;
        if (!txtValDef.getText().trim().equals("")) {
          if (isValidValorPorDefecto()) {
            BorrarTabla();
            Recargar_ListaValoresPantalla_con_ValDef();
            if (!listaValoresPantalla.isEmpty()) {
              PintarTabla();
            }
          }
        }
        this.modoOperacion = this.modoOperacionBk;
        Inicializar();

      } //fin del modo
      sValDefBk = txtValDef.getText();
    } //Si ha cambiado cod

  }

  // procesa el cambio de c�digos usando teclado
  public void txt_keyPressed(KeyEvent e) {

    if ( ( (TextField) e.getSource()).getName() == "ano") {

      if (this.modoOperacion != modoOBTENER_DATOS &&
          this.modoOperacion != modoCAPTURAR_PAR) {
        Resetear();
        this.modoOperacion = modoOBTENER_DATOS;
        Inicializar();
      }
    }

  }

  void txtCodInd_actionPerformed(ActionEvent e) {

  }

} //FIN DE CLASE PPAL******************************************

// lista de valores: PARTIR DE INDICADOR
class CListaIndAno
    extends CListaValores {

  public CListaIndAno(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataIndAno(s);
  }

  public String getCodigo(Object o) {
    return ( (DataIndAno) o).getCod();
  }

  public String getDescripcion(Object o) {
    return "(" + ( (DataIndAno) o).getAno() + ") " + ( (DataIndAno) o).getDes();
  }
}

// ESCUCHADORES******PULSAR BOTONES***************************
// action listener para los botones
class Pan_MantalusbtnActionListener
    implements ActionListener, Runnable {
  Panel_Mantalus adaptee = null;
  ActionEvent e = null;

  public Pan_MantalusbtnActionListener(Panel_Mantalus adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("BuscarIndAux")) { // Enfermedad
      adaptee.btnBuscarIndAux_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("A�adir")) { // A�adir
      adaptee.btn1_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("Modificar")) { // Modificar
      adaptee.btn2_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("Borrar")) { // Borrar
      adaptee.btn3_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("Aceptar")) { // Aceptar
      adaptee.btn4_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("Cancelar")) { // Cancelar
      adaptee.btnCancelar_actionPerformed(e);

    }
  }
}

// escuchador de los click en la tabla
class Pan_Mantalus_tabla_actionAdapter
    implements jclass.bwt.JCActionListener {
  Panel_Mantalus adaptee;

  Pan_Mantalus_tabla_actionAdapter(Panel_Mantalus adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    adaptee.btn2_actionPerformed(e);
  }
}

// perdida del foco de una caja de codigo
class Panel_MantalusFocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Panel_Mantalus adaptee;
  FocusEvent event;

  Panel_MantalusFocusAdapter(Panel_Mantalus adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    TextField txt = (TextField) event.getSource();
    if ( (txt.getName().equals("ano")) &&
        (adaptee.txtAno.getText().length() > 0)) {
      adaptee.txtAnoFocusLost(event);
    }
    else if ( (txt.getName().equals("codiaux")) &&
             (adaptee.txtCodIndAux.getText().length() > 0)) {
      adaptee.txtCodIndAuxFocusLost(event);
    }
    else if ( (txt.getName().equals("coef")) &&
             (adaptee.txtCoef.getText().length() > 0)) {
      adaptee.txtCoefFocusLost(event);
    }
    else if ( (txt.getName().equals("valdef")) &&
             (adaptee.txtValDef.getText().length() > 0)) {
      adaptee.txtValDefFocusLost(event);

    }
  }
}

class Panel_MantalusTxt_keyAdapter
    extends java.awt.event.KeyAdapter {
  Panel_Mantalus adaptee;

  Panel_MantalusTxt_keyAdapter(Panel_Mantalus adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
