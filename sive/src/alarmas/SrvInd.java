package alarmas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

/*  Selecciones:
 *    Dado un Indicador, los datos de la tabla SIVE_INDiCADOR*/

public class SrvInd
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //tama�o de campos
  final int tamCD_ENFCIE = 6;
  final int tamCD_SECUEN = 3;
  final int tamCD_NIVEL_1 = 2;
  final int tamCD_NIVEL_2 = 2;

  final String R = "-"; //Car�cter de relleno para c�digos de nivel 1 o nivel 2 que no lleguen a su valor m�ximo

  // Formatea con ceros a la izquierda
  protected String Formatear_Ceros_Izquierda(String sValor, int iHasta) {
    String sCadena = "";
    int iNum = 0;

    sCadena = sValor;
    iNum = iHasta;

    while (sCadena.length() < iNum) {
      sCadena = "0" + sCadena;
    }
    return sCadena;
  } //fin Formatear

  // Formatea con UNDERSCORES a la izquierda
  protected String Formatear_Under_Izquierda(String sValor, int iHasta) {
    String sCadena = "";
    int iNum = 0;

    sCadena = sValor;
    iNum = iHasta;

    while (sCadena.length() < iNum) {
      sCadena = R + sCadena;
    }
    return sCadena;
  } //fin Formatear

  // Formatea Secuencial
  protected String Formatear_Secuencial(int iSiguiente) {
    String sVolver = "";
    int iNum = iSiguiente;
    Integer iIntermedio = new Integer(iSiguiente);

    sVolver = iIntermedio.toString();

    while (sVolver.length() < 3) {
      sVolver = "0" + sVolver;
    }
    return sVolver;
  } //fin Formatear

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    //Nombres de tablas parametrizados seg�n la aplicaci�n
    //Por defecto toman nombres de EDO
    String sTablaIndAno = "SIVE_IND_ANO";
    String sTablaAlarma = "SIVE_ALARMA";
    String sTablaInd = "SIVE_INDICADOR";

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    PreparedStatement stSELECTPREVIA = null;
    ResultSet rs = null;
    String query = "";
    int i = 1;
    int k = 1; //Para indice de query

    // objetos de datos
    CLista data = new CLista();
    DataInd ind = null;

    // campos
    String sCodInd = "";
    String sDesInd = "";
    String sCodEnf = "";
    String sDesEnf = "";
    String sCodNivel1 = "";
    String sDesNivel1 = "";
    String sDesLNivel1 = "";
    String sCodNivel2 = "";
    String sDesNivel2 = "";
    String sDesLNivel2 = "";
    String sChkActivo = "";
    String iTipo = "";

    //Strings
    String sIzquierda = "";
    String sSecuencial = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //Segun el tipo SIVE utilizado tendremos una aplicaci�n distinta
    // y por tanto se utilizar�n tablas distintas
    String sTSive = param.getTSive();
    //Aplicaci�n centinelas
    if (sTSive.equals("C")) {
      sTablaInd = "SIVE_IND_ALAR_RMC";
      sTablaIndAno = "SIVE_IND_ALARRMC_ANO";
      sTablaAlarma = "SIVE_ALARMA_RMC";
    }

    //cargamos ind, con que ha llegado en param
    ind = (DataInd) param.firstElement();

    try {

      // modos de operaci�n
      switch (opmode) {

        //ALTA******************************************
        case servletALTA:

          final String sALTA_IND = "insert into " + sTablaInd + " "
              + "(CD_INDALAR, DS_INDALAR, CD_NIVEL_1, CD_NIVEL_2, "
              + " CD_ENFCIE, IT_ACTIVO, IT_TIPO, CD_IZQUIER, CD_SECUEN) "
              + "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

          st = con.prepareStatement(sALTA_IND);

          //NO OBLIGATORIOS **********************
          //Nivel1
          if (ind.getCodNivel1().trim().length() > 0) {
            st.setString(3, ind.getCodNivel1().trim());
            sIzquierda = sIzquierda +
                Formatear_Under_Izquierda(ind.getCodNivel1().trim(),
                                          tamCD_NIVEL_1);
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);
            sIzquierda = sIzquierda + "CC";
          }

          //nivel 2:
          if (ind.getCodNivel2().trim().length() > 0) {
            st.setString(4, ind.getCodNivel2().trim());
            sIzquierda = sIzquierda +
                Formatear_Under_Izquierda(ind.getCodNivel2().trim(),
                                          tamCD_NIVEL_2);
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);
            sIzquierda = sIzquierda +
                Formatear_Under_Izquierda("", tamCD_NIVEL_2);
          }

          //OBLIGATORIOS *************************
          //Auto/manual
          st.setString(7, ind.getiTipo().trim());
          sIzquierda = sIzquierda + ind.getiTipo().trim();

          //enfcie
          st.setString(5, ind.getCodEnf().trim());
          sIzquierda = sIzquierda +
              Formatear_Under_Izquierda(ind.getCodEnf().trim(), tamCD_ENFCIE);

          //formada la cadena ***********************
          final String queryPrevia = "SELECT MAX(CD_SECUEN) "
              + "FROM " + sTablaInd + " WHERE CD_IZQUIER = ?";

          stSELECTPREVIA = con.prepareStatement(queryPrevia);

          stSELECTPREVIA.setString(1, sIzquierda.trim());
          rs = stSELECTPREVIA.executeQuery();

          if (rs.next()) {
            if (rs == null) {
              sSecuencial = "001";
            }
            else {
              sSecuencial = Formatear_Secuencial(rs.getInt(1) + 1);
            }
          }
          rs.close();
          rs = null;
          stSELECTPREVIA.close();
          stSELECTPREVIA = null;

          //realizo el alta
          if (ind.getDesInd().trim().length() > 0) {
            st.setString(2, ind.getDesInd().trim());
          }
          else {
            st.setNull(2, java.sql.Types.VARCHAR);

          }
          st.setString(6, ind.getChkActivo().trim());
          st.setString(8, sIzquierda.trim());
          st.setString(9, sSecuencial.trim());
          st.setString(1, sIzquierda.trim().trim() + sSecuencial.trim());

          st.executeUpdate();
          st.close();
          st = null;

          //________________ Se devuelve el c�digo al cliente___________________

          data = new CLista();
          DataInd datosDevolver;

          // a�ade un nodo
          datosDevolver = new DataInd(sIzquierda.trim().trim() +
                                      sSecuencial.trim());
          data.addElement(datosDevolver);

          break;

          //MODIFICAR******************************************
        case servletMODIFICAR:

          final String sACTUALIZA = "update " + sTablaInd + " "
              + " set DS_INDALAR = ?, IT_ACTIVO = ? where CD_INDALAR = ?";

          st = con.prepareStatement(sACTUALIZA);

          if (ind.getDesInd().trim().length() > 0) {
            st.setString(1, ind.getDesInd().trim());
          }
          else {
            st.setNull(1, java.sql.Types.VARCHAR);

          }
          st.setString(2, ind.getChkActivo().trim());
          st.setString(3, ind.getCodInd().trim());

          st.executeUpdate();
          st.close();
          st = null;

          break;

        case servletBAJA:
          final String sBORRAR_DETALLE = "DELETE FROM " + sTablaAlarma + " "
              + "WHERE (CD_INDALAR = ?) ";
          final String sBORRAR_PADRE = "DELETE FROM " + sTablaIndAno + " "
              + "WHERE (CD_INDALAR = ?) ";

          query = "DELETE FROM " + sTablaInd + " "
              + "WHERE (CD_INDALAR = ?) ";

          // lanza la query:detalle, padre, y query
          //ind esta cargado con lo que ha llegado
          st = con.prepareStatement(sBORRAR_DETALLE);
          st.setString(1, ind.getCodInd().trim());
          st.executeUpdate();
          st.close();
          st = null;

          st = con.prepareStatement(sBORRAR_PADRE);
          st.setString(1, ind.getCodInd().trim());
          st.executeUpdate();
          st.close();
          st = null;

          st = con.prepareStatement(query);
          st.setString(1, ind.getCodInd().trim());
          st.executeUpdate();
          st.close();
          st = null;

          break;
          //SELECT***********************************************
          //busqueda concreta de Indicador
        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:

          query = /*query +*/ "select "
              + "a.CD_INDALAR, a.DS_INDALAR, "
              + "a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO, "
              + "a.CD_NIVEL_1, "
              + "a.CD_NIVEL_2,  "
              + "a.IT_ACTIVO, a.IT_TIPO "
              + "from " + sTablaInd + " a, SIVE_PROCESOS b "
              + "where a.CD_ENFCIE = b.CD_ENFCIE ";

          switch (param.getPerfil()) {
            case 1:
            case 2: // ccaa
              query = query;
              break;
            case 3: // niv 1
              query = query + "and a.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) ";
              break;
            case 4: // niv 2
              query = query + "and a.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) " +
                  "and a.CD_NIVEL_2 in (select CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) ";
              break;
          }

          // prepara la query
          if (opmode == servletOBTENER_X_CODIGO) {
            query = query + "and a.CD_INDALAR = ? ";
          }
          else {
            query = query + "and a.DS_INDALAR = ? ";

            // lanza la query
          }
          st = con.prepareStatement(query);

          k = 1;
          switch (param.getPerfil()) {
            case 3: // niv 1
              st.setString(k, param.getLogin().trim());
              k++;
              break;
            case 4: // niv 2
              st.setString(k, param.getLogin().trim());
              k++;
              st.setString(k, param.getLogin().trim());
              k++;
              break;
          }

          st.setString(k, ind.getCodInd().trim());
          k++;
          rs = st.executeQuery();

          data = new CLista();
          DataInd datosBusqueda;

          // extrae el registro encontrado
          while (rs.next()) {
            sCodInd = rs.getString("CD_INDALAR");
            sDesInd = rs.getString("DS_INDALAR");
            sCodEnf = rs.getString("CD_ENFCIE");
            String desPro = rs.getString("DS_PROCESO");
            String desLPro = rs.getString("DSL_PROCESO");

            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (desLPro != null)) {
              sDesEnf = desLPro;
            }
            else {
              sDesEnf = desPro;

            }
            sCodNivel1 = rs.getString("CD_NIVEL_1");
            sCodNivel2 = rs.getString("CD_NIVEL_2");
            sChkActivo = rs.getString("IT_ACTIVO");
            iTipo = rs.getString("IT_TIPO");

            // a�ade un nodo
            datosBusqueda = new DataInd(sCodInd, sDesInd,
                                        sCodEnf, sDesEnf,
                                        sCodNivel1, "",
                                        sCodNivel2, "",
                                        sChkActivo, iTipo, "", "");

            data.addElement(datosBusqueda);
          } //fin while
          rs.close();
          rs = null;
          st.close();
          st = null;

          //RECUPERAMOS LA DESCRIPCIONES DE NIVELES
          st = con.prepareStatement(
              "select DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 = ? ");

          for (int j = 0; j < data.size(); j++) {

            //recupero el registro de la lista
            datosBusqueda = (DataInd) data.elementAt(j);

            if (datosBusqueda.sCodNivel1 != null) {
              st.setString(1, datosBusqueda.sCodNivel1);

              rs = st.executeQuery();
              rs.next();
              String desN1 = rs.getString("DS_NIVEL_1");
              String desLN1 = rs.getString("DSL_NIVEL_1");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN1 != null)) {
                datosBusqueda.sDesNivel1 = desLN1;
              }
              else {
                datosBusqueda.sDesNivel1 = desN1;
              }
              rs.close();
              rs = null;
            } //fin del if

          } //fin for

          st.close();
          st = null;

          st = con.prepareStatement("select DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? ");
          for (int j = 0; j < data.size(); j++) {

            //recupero el registro de la lista
            datosBusqueda = (DataInd) data.elementAt(j);

            if (datosBusqueda.sCodNivel2 != null) {

              st.setString(1, datosBusqueda.sCodNivel1);
              st.setString(2, datosBusqueda.sCodNivel2);
              rs = st.executeQuery();
              rs.next();
              String desN2 = rs.getString("DS_NIVEL_2");
              String desLN2 = rs.getString("DSL_NIVEL_2");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN2 != null)) {
                datosBusqueda.sDesNivel2 = desLN2;
              }
              else {
                datosBusqueda.sDesNivel2 = desN2;
              }
              rs.close();
              rs = null;
            } //fin del if

          } //fin for

          st.close();
          st = null;

          break;

          //LIKE***********************************************
          // like de datos Indicador
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:

          query = "select "
              + "a.CD_INDALAR, a.DS_INDALAR, "
              + "a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO, "
              + "a.CD_NIVEL_1, "
              + "a.CD_NIVEL_2, "
              + "a.IT_ACTIVO, a.IT_TIPO "
              + "from " + sTablaInd + " a, "
              + "SIVE_PROCESOS b "
              + "where a.CD_ENFCIE = b.CD_ENFCIE ";

          switch (param.getPerfil()) {
            case 1:
            case 2: // ccaa
              query = query;
              break;
            case 3: // niv 1
              query = query + "and a.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) ";
              break;
            case 4: // niv 2
              query = query + "and a.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) " +
                  "and a.CD_NIVEL_2 in (select CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) ";
              break;
          }

          // peticion de trama
          if (param.getFilter().length() > 0) {

            if (opmode == servletSELECCION_X_CODIGO) {
              query = query +
                  "and a.CD_INDALAR like ? and a.CD_INDALAR > ? order by a.CD_INDALAR";
            }
            else {

              // ARG: upper (7/5/02)
              query = query + "and upper(a.DS_INDALAR) like upper(?) and upper(a.DS_INDALAR) > upper(?) order by a.DS_INDALAR";

              // peticion de la primera trama
            }
          }
          else {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = query + "and a.CD_INDALAR like ? order by a.CD_INDALAR";
            }
            else {

              // ARG: upper (7/5/02)
              query = query +
                  "and upper(a.DS_INDALAR) like upper(?) order by a.DS_INDALAR";
            }
          }

          // prepara la lista de resultados
          data = new CLista();
          DataInd datosPantalla;

          // lanza la query
          st = con.prepareStatement(query);

          k = 1;
          switch (param.getPerfil()) {
            case 3: // niv 1
              st.setString(k, param.getLogin().trim());
              k++;
              break;
            case 4: // niv 2
              st.setString(k, param.getLogin().trim());
              k++;
              st.setString(k, param.getLogin().trim());
              k++;
              break;
          }

//____________________________________________________

          // filtro: caja de texto de lista de valores
          //En ese campo cod podr�a viajar la descripcion
          if (opmode == servletSELECCION_X_CODIGO) {
            st.setString(k, ind.getCodInd().trim() + "%");
          }
          else {
            st.setString(k, "%" + ind.getCodInd().trim() + "%");
          }
          k++;

          // paginaci�n
          if (param.getFilter().length() > 0) {
            st.setString(k, param.getFilter().trim());
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              data.setFilter( ( (DataInd) data.lastElement()).getCodInd());
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // obtiene los campos
            sCodInd = rs.getString("CD_INDALAR");
            sDesInd = rs.getString("DS_INDALAR");
            sCodEnf = rs.getString("CD_ENFCIE");
            String desPro = rs.getString("DS_PROCESO");
            String desLPro = rs.getString("DSL_PROCESO");

            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (desLPro != null)) {
              sDesEnf = desLPro;
            }
            else {
              sDesEnf = desPro;

            }
            sCodNivel1 = rs.getString("CD_NIVEL_1");
            sCodNivel2 = rs.getString("CD_NIVEL_2");
            sChkActivo = rs.getString("IT_ACTIVO");
            iTipo = rs.getString("IT_TIPO");

            // a�ade un nodo
            datosPantalla = new DataInd(sCodInd, sDesInd,
                                        sCodEnf, sDesEnf,
                                        sCodNivel1, "",
                                        sCodNivel2, "",
                                        sChkActivo, iTipo, "", "");

            data.addElement(datosPantalla);
            i++;
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          //RECUPERAMOS LA DESCRIPCIONES DE NIVELES
          st = con.prepareStatement(
              "select DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 = ? ");

          for (int j = 0; j < data.size(); j++) {

            //recupero el registro de la lista
            datosPantalla = (DataInd) data.elementAt(j);

            if (datosPantalla.sCodNivel1 != null) {

              st.setString(1, datosPantalla.sCodNivel1);
              rs = st.executeQuery();
              rs.next();
              String desNivel1 = rs.getString("DS_NIVEL_1");
              String desLNivel1 = rs.getString("DSL_NIVEL_1");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLNivel1 != null)) {
                datosPantalla.sDesNivel1 = desLNivel1;
              }
              else {
                datosPantalla.sDesNivel1 = desNivel1;
              }
              rs.close();
              rs = null;
            } //fin del if

          } //fin for

          st.close();
          st = null;

          st = con.prepareStatement("select DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? ");
          for (int j = 0; j < data.size(); j++) {

            //recupero el registro de la lista
            datosPantalla = (DataInd) data.elementAt(j);

            if (datosPantalla.sCodNivel2 != null) {

              st.setString(1, datosPantalla.sCodNivel1);
              st.setString(2, datosPantalla.sCodNivel2);
              rs = st.executeQuery();
              rs.next();
              String desNivel2 = rs.getString("DS_NIVEL_2");
              String desLNivel2 = rs.getString("DSL_NIVEL_2");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                  (desLNivel2 != null)) {
                datosPantalla.sDesNivel2 = desLNivel2;
              }
              else {
                datosPantalla.sDesNivel2 = desNivel2;
              }
              rs.close();
              rs = null;
            } //fin del if

          } //fin for

          st.close();
          st = null;

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();

      data = null; //para comprobar que la transaccion ha ido mal!

      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
