package notentrada;

import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Insets;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.util.JCVector;
import notdata.DataListaEDOIndiv;
import notdata.DataListaNotifEDO;
import notdata.DataOpeFc;
import notindiv.DialogListaEDOIndiv;
import notutil.Comunicador;
import sapp.StubSrvBD;

public class PanelListaEDOIndiv
    extends PanelListaEDO {

  PanelMaestroEDO MaestroEDO;

  //tipo de enfermedad cuando se esta a�adiendo
  private String TipoEnfermedad = "";
  ResourceBundle res;

  // modos de operaci�n del panel
  final int modoNORMAL = 0;
  final int modoINICIO = 1;
  final int modoESPERA = 2;

  // modos de operaci�n del servlet
  final int servletUPDATE_EDO_INDIV = 4;
  final int servletINSERT_EDO_INDIV = 5;
  final int servletDELETE_EDO_INDIV = 6;
  final int servletSELECCION_LISTA_EDO_INDIV = 3;
  final int servletSELECCION_LISTA_EDO_FECHA_INDIV = 7;

  // modos de operaci�n del panel
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;

  // modos de operaci�n del panelmaestro
  final int modoCABECERA = 13;
  final int modoBOTONES = 14;
  final int modoCABECERAYBOTONES = 15;
  final int modoCONSULTAYBOTONES = 16;

  // modos de actualizaci�n de la lista
  final int modoADD = 20;
  final int modoMODIFY = 21;
  final int modoDELETE = 22;

  // constantes del panel
  final String strSERVLET_EDOIND = "servlet/SrvEDOIndiv";

  // listas con los datos de las choices
  protected CLista listaPanelCabecera = null;
  protected CLista listaPanelCaso = null;

  // par�metros
  JCVector itemsSemana = new JCVector();
  CLista listaTblSemana = new CLista();
  JCVector itemsDia = new JCVector();
  CLista listaTblDia = new CLista();
  StubSrvBD stubCliente;
  NotificacionEDO applet;
  protected Hashtable hashNotifEDO = null;
  CLista parametros = null;

  // controles
  XYLayout xYLayout1 = new XYLayout();

  // constructor
  public PanelListaEDOIndiv(NotificacionEDO a, Hashtable hashNotifEDO) {
    super( (CApp) a);
    try {
      setApp( (CApp) a);
      res = ResourceBundle.getBundle("notentrada.Res" + a.getIdioma());
      applet = a;
      this.hashNotifEDO = hashNotifEDO;
      jbInit();
      stubCliente = new StubSrvBD(new URL(app.getURL() + strSERVLET_EDOIND));
      Inicializar(modoINICIO);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  protected void jbInit() throws Exception {

    tblSemana.getList().setBackground(Color.white);
    tblSemana.getList().setHighlightColor(Color.lightGray);
    tblSemana.setInsets(new Insets(5, 5, 5, 5));
    tblSemana.setColumnButtons(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tblSemana.ColumnButtons")), '\n'));
    tblSemana.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    tblSemana.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "65\n190\n60\n80\n80\n80"), '\n'));
    tblSemana.setNumColumns(6);
    tblSemana.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    tblSemana.setAutoSelect(true);
    this.add(tblSemana, new XYConstraints(0, 10, 590, 100));

    tblDia.getList().setBackground(Color.white);
    tblDia.getList().setHighlightColor(Color.lightGray);
    tblDia.setInsets(new Insets(5, 5, 5, 5));
    tblDia.setColumnButtons(jclass.util.JCUtilConverter.toStringList(new String(
        res.getString("tblDia.ColumnButtons")), '\n'));
    tblDia.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    tblDia.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "65\n190\n60\n80"), '\n'));
    tblDia.setNumColumns(4);
    tblDia.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    tblDia.setAutoSelect(true);
    this.add(tblDia, new XYConstraints(90, 10, 425, 100));

    btnPartes.setVisible(false);
    btnPartes.setEnabled(false);
  }

  public void InitPanel() {
    super.InitPanel();

    if (itemsSemana.size() > 0) {
      itemsSemana.removeAllElements();
    }
    if (listaTblSemana.size() > 0) {
      listaTblSemana.removeAllElements();
    }
    if (itemsDia.size() > 0) {
      itemsDia.removeAllElements();
    }
    if (listaTblDia.size() > 0) {
      listaTblDia.removeAllElements();
    }
  }

  public void Inicializar(int modo) {

    switch (modo) {
      case modoNORMAL:
      case modoINICIO:
        super.chkValidar.setVisible(false);
        super.lblValidar.setVisible(false);
        super.lblEtiquetaValidar.setVisible(false);
        super.IT_VALIDADA_PRIM = "";
        super.FC_FECVALID_PRIM = "";
        break;
    }

    super.modoOperacion = modo;
    Inicializar();
  }

  public void actualizar(int modo) {
    super.Actualizar_valores_validar(modo);
  }

  public void Vaciar() {
    itemsSemana = new JCVector();
    listaTblSemana = new CLista();
    itemsDia = new JCVector();
    listaTblDia = new CLista();
    tblDia.clear();
    tblSemana.clear();
  }

  public void RellenaLista(String codequipo, String anno, String semana,
                           String fecharecep, String fechanotif) {
    CLista lista = new CLista();
    Object componente;

    DataListaEDOIndiv dataIndiv = new DataListaEDOIndiv();
    dataIndiv.sCodEquipo = codequipo;
    dataIndiv.sAnno = anno;
    dataIndiv.sSemana = semana;
    dataIndiv.sFechaRecep = fecharecep;
    dataIndiv.sFechaNotif = fechanotif;

    parametros = new CLista();
    parametros.addElement(dataIndiv);

    // Por LORTAD

    parametros.setLogin(app.getLogin());

    parametros.setLortad(app.getParameter("LORTAD"));

    // Lista de d�a  **************************
    if (itemsDia.size() > 0) {
      itemsDia.removeAllElements();
    }
    if (listaTblDia.size() > 0) {
      listaTblDia.removeAllElements();
    }
    tblDia.clear();

    try {

      lista = Comunicador.Communicate(getApp(),
                                      stubCliente,
                                      servletSELECCION_LISTA_EDO_FECHA_INDIV,
                                      strSERVLET_EDOIND,
                                      parametros);

      lista.trimToSize();
      if (lista.size() == 0) {
        return;
      }

      if (lista.size() != 0) {
        // JRM2 Para ordenar alfab�ticamente la lista de las enfermedades
        Object componenteAux;
        Object componenteAux2;
        String primero;
        String siguiente;
        for (int i = 0; i < lista.size() - 1; i++) { //Ordenacion por Burbuja de la lista
          for (int j = 0; j < (lista.size() - 1) - i; j++) {
            primero = ( (DataListaEDOIndiv) lista.elementAt(j)).
                getDesEnfermedad();
            siguiente = ( (DataListaEDOIndiv) lista.elementAt(j + 1)).
                getDesEnfermedad();
            if (primero.compareTo(siguiente) > 0) { // Menor el siguiente
              componenteAux = (DataListaEDOIndiv) lista.elementAt(j); // Intercambio
              componenteAux2 = (DataListaEDOIndiv) lista.elementAt(j + 1);
              lista.removeElementAt(j);
              lista.insertElementAt(componenteAux2, j);
              lista.removeElementAt(j + 1);
              lista.insertElementAt(componenteAux, j + 1);
            }
          }
        }
        // JRM2 Para ordenar alfab�ticamente las enfermedades (fin)

        for (int i = 0; i < lista.size(); i++) {
          componente = lista.elementAt(i);
          addRowFecha(modoADD, (DataListaEDOIndiv) componente, -1);

          tblDia.setItems(itemsDia);
        }
        tblDia.setItems(itemsDia);
      }
    }
    catch (Exception e) {
      ShowWarning(res.getString("msg20.Text"));
      btnCancelar_actionPerformed(); //lo deja en modo CABECERABOTONES
      return;
    }

    // lista de semana  ************************
    if (itemsSemana.size() > 0) {
      itemsSemana.removeAllElements();
    }
    if (listaTblSemana.size() > 0) {
      listaTblSemana.removeAllElements();
    }
    tblSemana.clear();

    try {
      // Por LORTAD
      parametros.setLogin(app.getLogin());
      parametros.setLortad(app.getParameter("LORTAD"));

      lista = Comunicador.Communicate(getApp(),
                                      stubCliente,
                                      servletSELECCION_LISTA_EDO_INDIV,
                                      strSERVLET_EDOIND,
                                      parametros);

      lista.trimToSize();
      if (lista.size() == 0) {
        return;
      }

      for (int i = 0; i < lista.size(); i++) {
        componente = lista.elementAt(i);
        addRow(modoADD, (DataListaEDOIndiv) componente, -1);
      }

      tblSemana.setItems(itemsSemana);
    }
    catch (Exception e) {
      ShowWarning(res.getString("msg21.Text"));
      btnCancelar_actionPerformed(); //lo deja en modo CABECERABOTONES
      return;
    }
  }

  //SEMANA*******************************************************
  public void addRow(int modo, DataListaEDOIndiv data, int iSel) {
    JCVector row1 = new JCVector();

    row1.addElement(data.getExpediente());
    row1.addElement(data.getDesEnfermedad());
    row1.addElement(data.getIniciales());
    row1.addElement(data.getFechaNacto());
    row1.addElement(data.getFechaNotif());
    row1.addElement(data.getFechaRecep());

    // testea que el expediente no exista
    if (modo == modoADD) {
      for (int i = 0; i < listaTblSemana.size(); i++) {
        if ( ( (DataListaEDOIndiv) listaTblSemana.elementAt(i)).getExpediente().
            equals(data.getExpediente())) {
          modo = modoMODIFY;
          iSel = i;
          break;
        }
      }
    }

    switch (modo) {

      case modoADD:
        itemsSemana.addElement(row1);
        listaTblSemana.addElement(data);
        break;

      case modoMODIFY:
        itemsSemana.setElementAt(row1, iSel);
        listaTblSemana.setElementAt(data, iSel);
        break;

      case modoDELETE:
        listaTblSemana.removeElementAt(iSel);
        itemsSemana.removeElementAt(iSel);
        break;
    }
  }

  //DIA*************************************************************
  public void addRowFecha(int modo, DataListaEDOIndiv data, int iSel) {

    JCVector row1 = new JCVector();

    row1.addElement(data.getExpediente());
    row1.addElement(data.getDesEnfermedad());
    row1.addElement(data.getIniciales());
    row1.addElement(data.getFechaNacto());

    // testea que el expediente no exista
    if (modo == modoADD) {
      for (int i = 0; i < listaTblDia.size(); i++) {
        if ( ( (DataListaEDOIndiv) listaTblDia.elementAt(i)).getExpediente().
            equals(data.getExpediente())) {
          modo = modoMODIFY;
          iSel = i;
          break;
        }
      }
    }

    switch (modo) {
      case modoADD:
        itemsDia.addElement(row1);
        listaTblDia.addElement(data);
        //si a�ade una  Indiv de tipo A, debe a�adir parte a Numericas
        //TipoEnfermedad
        if (TipoEnfermedad.trim().equals("A")) {
          applet.pListaEDONum.A�adir_desde_Indiv_paraTipoA(data.
              getCodEnfermedad());
          TipoEnfermedad = "";
        } //-------------
        break;

      case modoMODIFY:
        itemsDia.setElementAt(row1, iSel);
        listaTblDia.setElementAt(data, iSel);
        break;

      case modoDELETE:
        listaTblDia.removeElementAt(iSel);
        itemsDia.removeElementAt(iSel);
        //si borra una  Indiv de tipo A, debe bajar parte a Numericas
        //TipoEnfermedad
        if (TipoEnfermedad.trim().equals("A")) {
          applet.pListaEDONum.Restar_desde_Indiv_paraTipoA(data.
              getCodEnfermedad());
          TipoEnfermedad = "";
        } //-------------
        break;

    }
  }

  public void Cargar_Validar(DataListaNotifEDO datos) {
    super.Cargar_Validar(datos);
  }

  //Encuentra el indice equivalente en Semana****************
  protected int Calcular_Indice_Semana(DataListaEDOIndiv data) {
    //data contiene los datos actuales de dia incluido caso
    //lista de datos de Semana (listaTblSemana)
    int iIndex = -1;
    boolean bEncontrado = false;
    DataListaEDOIndiv datosLinea;
    datosLinea = new DataListaEDOIndiv();

    for (int j = 0; j < listaTblSemana.size(); j++) {

      datosLinea = (DataListaEDOIndiv) listaTblSemana.elementAt(j);

      if (data.getExpediente().trim().equals(datosLinea.getExpediente().trim())
          ) {
        bEncontrado = true;
        iIndex = j + 1;
      }
    }
    return (iIndex);
  }

  protected void btnPartes_actionPerformed() {

  }

  //ALTA*********************************************************************
  protected void btnAlta_actionPerformed() {
    // comprobar que entramos por alta -> habr� que dar de alta
    // en primer lugar notifedo

    // VALIDO FECHA RECEPCI�N *************
    // modificacion jlt 22/10/2001 valido la fecha de recepci�n
    // que ahora viene en blanco por defecto
    MaestroEDO = applet.pMaestroEDO;
    String n = new String();
    n = MaestroEDO.getFechaRecep().trim();
    if (n.equals("") || n.equals(null)) {

      ShowWarning(res.getString("msg80.Text"));
      return;
    }

    int modo = super.modoOperacion;

    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);

    if (applet.pMaestroEDO.modoOperacion == modoALTA) {
      CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                     res.getString("msg22.Text"));
      msgBox.show();

      if (msgBox.getResponse()) { //SI

        applet.pMaestroEDO.AltaNotificacion(applet.pListaEDONum.listaTblDia);
        // si en maestro se sigue en modoalta, no se ha conseguido dar de alta
        if (applet.pMaestroEDO.modoOperacion == modoALTA) {
          return;
        }

        // el sistema est� en modo modificar
        modo = modoMODIFICACION;
      }

      else { //NO
        //ShowWarning("No se dar� de alta la notificaci�n, y tampoco la notificaci�n individualizada");
        super.modoOperacion = modo;
        applet.pListaEDONum.Inicializar(modo);
        applet.pListaEDOIndiv.Inicializar(modo);
        return;
      }

      msgBox = null;
    }

    //continuamos
    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);

    //dialogo de PanelIndiv
    hashNotifEDO.put("CD_ENFCIE", "");
    hashNotifEDO.put("DS_PROCESO", "");
    hashNotifEDO.put("NM_EDO", "");

    DialogListaEDOIndiv dlg = new DialogListaEDOIndiv(this.getApp(),
        "Indiv",
        stubCliente,
        "",
        modoALTA,
        new DataListaEDOIndiv(),
        hashNotifEDO);

    dlg.setListaPanelCabecera(listaPanelCabecera);
    dlg.setListaPanelCaso(listaPanelCaso);

    dlg.show();

    listaPanelCabecera = dlg.getListaPanelCabecera();
    listaPanelCaso = dlg.getListaPanelCaso();

    //CUANDO TERMINA!!!!!!(conozco BK, NM_EDO)
    if (dlg.iOut != -1) {
      DataListaEDOIndiv data = (DataListaEDOIndiv) dlg.getComponente();
      TipoEnfermedad = dlg.enfermedadTipoA();
      if (data != null) {

        //semana*********************
        addRow(modoADD, data, -1);
        tblSemana.setItems(itemsSemana);
        //dia*********************
        addRowFecha(modoADD, data, -1); //TipoEnfermedad
        tblDia.setItems(itemsDia);
      }
    }

    dlg = null;

    super.modoOperacion = modo;
    // lo pasamos al modo anterior
    applet.pListaEDONum.Inicializar(modo);
    applet.pListaEDOIndiv.Inicializar(modo);
  }

  //MODIFICACION**************************************************
  protected void btnModificacion_actionPerformed() {
    //limpio la hash por si acaso
    hashNotifEDO.put("NM_EDO", "");

    // desde PanelMaestroEDO se ejecuta el modoESPERA total
    // ( PanelMaestroEDO + PanelListaEDONUm + PanelListaEDOIndiv )
    //applet.pMaestroEDO.Inicializar(modoESPERA);

    int modo = super.modoOperacion;

    super.modoOperacion = modoESPERA;

    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);

    int iSel = tblDia.getSelectedIndex();
    if (iSel != BWTEnum.NOTFOUND) {

      DataListaEDOIndiv dataInd = (DataListaEDOIndiv) listaTblDia.elementAt(
          tblDia.getSelectedIndex());
      hashNotifEDO.put("CD_ENFCIE", dataInd.getCodEnfermedad());
      hashNotifEDO.put("DS_PROCESO", dataInd.getDesEnfermedad());
      hashNotifEDO.put("NM_EDO", dataInd.getExpediente());

      //puedo entrar como modificacion o consulta o alta = modif
      //borrado maxivo no entra
      int iEstado = 0;
      if (modo == modoCONSULTA) {
        iEstado = modoCONSULTA;
      }
      else {
        iEstado = modoMODIFICACION;

      }
      DialogListaEDOIndiv dlg = new DialogListaEDOIndiv(this.getApp(),
          "Indiv", stubCliente,
          "", iEstado,
          new DataListaEDOIndiv(), hashNotifEDO);

      dlg.setListaPanelCabecera(listaPanelCabecera);
      dlg.setListaPanelCaso(listaPanelCaso);
      dlg.show();
      listaPanelCabecera = dlg.getListaPanelCabecera();
      listaPanelCaso = dlg.getListaPanelCaso();

      if (dlg.iOut != -1) {
        DataListaEDOIndiv data = (DataListaEDOIndiv) dlg.getComponente();

        if (data != null) {
          //DIA**************************
          addRowFecha(modoMODIFY, data, iSel);
          tblDia.setItems(itemsDia);

          //SEMANA**********************
          //la lista semana es igual, porque no podre cambiar nada de
          //lo que sale en la lista
        }
      }

      dlg = null;
    }
    //modoOperacion = modoNORMAL;
    //Inicializar();

    //applet.pMaestroEDO.Inicializar(modoNORMAL);
    super.modoOperacion = modo;
    //JM Inicializar();
    applet.pListaEDONum.Inicializar(modo);
    applet.pListaEDOIndiv.Inicializar(modo);

  }

  //BAJA********  *******************************************
  protected void btnBaja_actionPerformed() { //borrado Indiv

    int modo = super.modoOperacion;

    super.modoOperacion = modoESPERA;
    //JM Inicializar();
    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);

    if ( (tblDia.countItems() == 1)
        && (applet.pListaEDONum.tblDia.countItems() == 0)) {
      ShowWarning(res.getString("msg23.Text"));
      //modoOperacion = modoNORMAL;
      //Inicializar();

      // desde PanelMaestroEDO se ejecuta el modoNORMAL total
      // ( PanelMaestroEDO + PanelListaEDONUm + PanelListaEDOIndiv )
      //applet.pMaestroEDO.Inicializar(modoNORMAL);
      super.modoOperacion = modo;
      //JM Inicializar();
      applet.pListaEDONum.Inicializar(modo);
      applet.pListaEDOIndiv.Inicializar(modo);

      return;
    }

    //si existen datos suficientes para poder borrar ESTA, continuamos
    int iSel = tblDia.getSelectedIndex();
    if (iSel != BWTEnum.NOTFOUND) {
      Object componente = listaTblDia.elementAt(tblDia.getSelectedIndex());

      DataListaEDOIndiv dataInd = (DataListaEDOIndiv) listaTblDia.elementAt(
          tblDia.getSelectedIndex());
      hashNotifEDO.put("CD_ENFCIE", dataInd.getCodEnfermedad());
      hashNotifEDO.put("DS_PROCESO", dataInd.getDesEnfermedad());
      hashNotifEDO.put("NM_EDO", dataInd.getExpediente());

      //antes de abrir el dlg cargo listaopeultactTabla en la hashNotifEDO
      //Reserva de la listaOpeUltactTabla (en ppo debe llevar:equipo,notifsem, notifedo)
      CLista listaReservada = new CLista();
      DataOpeFc dataProvisional = null;
      for (int i = 0; i < applet.pMaestroEDO.listaOpeUltactTabla.size(); i++) {
        dataProvisional = (DataOpeFc) applet.pMaestroEDO.listaOpeUltactTabla.
            elementAt(i);
        listaReservada.addElement(dataProvisional);
      }
      hashNotifEDO.put("listaOpeUltactTabla", listaReservada);
      hashNotifEDO.put("IT_COBERTURA", applet.pMaestroEDO.getsCobertura());
      //------------------------------------------------------

      DialogListaEDOIndiv dlg = new DialogListaEDOIndiv(this.getApp(),
          "Indiv",
          stubCliente,
          "",
          modoBAJA,
          new DataListaEDOIndiv(),
          hashNotifEDO);
      dlg.setListaPanelCabecera(listaPanelCabecera);
      dlg.setListaPanelCaso(listaPanelCaso);
      dlg.show();
      listaPanelCabecera = dlg.getListaPanelCabecera();
      listaPanelCaso = dlg.getListaPanelCaso();

      //si sali por borrar(aceptar) en el dialogo
      if (dlg.iOut != -1) {
        DataListaEDOIndiv data = (DataListaEDOIndiv) dlg.getComponente();
        TipoEnfermedad = dlg.enfermedadTipoA();
        if (data != null) {

          //LA DOY DE BAJA
          //SEMANA****************
          int indiceSemana = Calcular_Indice_Semana(data); //data , vale el caso
          addRow(modoDELETE, data, indiceSemana - 1); //data, no vale para nada
          tblSemana.setItems(itemsSemana);
          //DIA*******************
          addRowFecha(modoDELETE, data, iSel); ////TipoEnfermedad, data, no vale para nada
          tblDia.setItems(itemsDia);

        }
      }
      //si sale por cancelar, nada
      dlg = null;

    }
    //no selcciona nada de la lista
    else {
      ShowWarning(res.getString("msg24.Text"));
    }

    // desde PanelMaestroEDO se ejecuta el modoINICIO total
    // ( PanelMaestroEDO + PanelListaEDONUm + PanelListaEDOIndiv )

    //old
    //applet.pMaestroEDO.Inicializar(modoINICIO);

    //jm
    super.modoOperacion = modo;
    //JM Inicializar();

    applet.pListaEDONum.Inicializar(modo);
    applet.pListaEDOIndiv.Inicializar(modo);

  } //fin de borrar ****** ***** ***** ***** *****

  protected void btnAceptar_actionPerformed() {

    String n = new String();
    n = applet.pMaestroEDO.getFechaRecep().trim();
    if (n.equals("") || n.equals(null)) {
      ShowWarning(res.getString("msg80.Text"));
      return;
    }

    CLista listaAux = new CLista();
    int modo = super.modoOperacion;

    // ARS 28-06-01
    // Si las notificaciones reales son correctas
    // seguimos adelante.
    if (applet.pMaestroEDO.bNotifRealesBien) {

      super.modoOperacion = modoESPERA;
      applet.pListaEDONum.Inicializar(modoESPERA);
      applet.pListaEDOIndiv.Inicializar(modoESPERA);

      switch (modo) {
        case modoALTA:

          // no se permite dar de alta una notificaci�n sin EDO num�ricas ni individualizadas
          if ( (applet.pListaEDONum.tblDia.countItems() == 0)
              && (tblDia.countItems() == 0)
              && !applet.pMaestroEDO.chkbxResSem.getState()) {
            ShowWarning(res.getString("msg23.Text"));
            super.modoOperacion = modo; //ALTA
            applet.pListaEDONum.Inicializar(modo);
            applet.pListaEDOIndiv.Inicializar(modo);
            return;
          }

          if ( (applet.pListaEDONum.tblDia.countItems() == 0)
              && (tblDia.countItems() == 0)
              && applet.pMaestroEDO.chkbxResSem.getState()) {
            CMessage msgBox = new CMessage(this.app,
                                           CMessage.msgPREGUNTA,
                                           res.getString("msg25.Text"));
            msgBox.show();
            if (msgBox.getResponse()) { //SI
              //continua
            }
            else { //NO
              super.modoOperacion = modo; //ALTA
              applet.pListaEDONum.Inicializar(modo);
              applet.pListaEDOIndiv.Inicializar(modo);
              return;
            }
          } //fin ne notificaciones CEROS CASOS -----------------

          applet.pMaestroEDO.AltaNotificacion(applet.pListaEDONum.listaTblDia);
          return; //cambio a modif si bien o alta si mal//NO NECESITA INICIALIZAR

        case modoMODIFICACION:
          if ( (applet.pListaEDONum.tblDia.countItems() == 0)
              && (tblDia.countItems() == 0)
              && !applet.pMaestroEDO.chkbxResSem.getState()) {
            ShowWarning(res.getString("msg23.Text"));
            super.modoOperacion = modo; //MODIFICACION
            applet.pListaEDONum.Inicializar(modo);
            applet.pListaEDOIndiv.Inicializar(modo);
            return;
          }

          applet.pMaestroEDO.ModNotificacion(applet.pListaEDONum.listaTblDia);
          //control de error al rellenarlistas
          if (applet.pMaestroEDO.modoOperacion == modoCABECERAYBOTONES) {
            return;
          } //**************************************
          super.modoOperacion = modo;
          applet.pListaEDONum.Inicializar(modo);
          applet.pListaEDOIndiv.Inicializar(modo);
          return;

        case modoBAJA:
          applet.pMaestroEDO.BajaNotificacion();
          //ya lo ponen en modoCABECERABOTONES
          return;
      }

    } // ARS 28-06-01
    // Fin del "si las notificaciones reales son correctas"
    // Reiniciamos la variable bNotifRealesBien
    applet.pMaestroEDO.bNotifRealesBien = true;

  } //fin aceptar

  /*
   *  Autor         Fecha                 Accion
   *    JRM         23/06/2000            Modicica para que cambie al siguiente
   *                                      equipo notificador.
   */
  public void btnCancelar_actionPerformed() {
    //validacion
    // Comento esto de abajo por la misma razon que JRM (est� en el Cancelar de las notif num)
    //super.chkValidar.setVisible(false);
    //super.lblValidar.setVisible(false);
    //super.lblEtiquetaValidar.setVisible(false);
    //super.IT_VALIDADA_PRIM = "";
    //super.FC_FECVALID_PRIM = "";
    //applet.pMaestroEDO.chkbxResSem.setState(false);
    // JRM2 Modifica para que cuando se d� al cancelar y estuviese activo un notificador
    //  dado de baja, no se pueda modificar
    // Comento esto de abajo por la misma razon que JRM (est� en el Cancelar de las notif num)
    /*boolean enMadrid;
     if (getApp().getParameter("ORIGEN").equals("M"))
         {
          enMadrid = true;
         }
      else
         {
         enMadrid = false;
         }
         if   ((applet.pMaestroEDO.modoOperacion == modoCONSULTA) && (enMadrid)) applet.pMaestroEDO.Inicializar(modoCABECERAYBOTONES);
      else {
          applet.pArDiAn.tfDistrito.requestFocus();
          applet.pArDiAn.tfDistrito.enable();
          applet.pArDiAn.tfAnno.requestFocus();
          applet.pMaestroEDO.txtCodEquipo.requestFocus();
          }
         pArDiAn.habilitarPanel(true);*/

    // JRM: Este c�digo aparece comentado porque ya est� en el cancelar de las
    // notificaciones num�ricas. Como cuando se pulsa el cancelar se ejecutan
    // tanto el m�todo de cancelar de las notificaciones num�ricas como las de las
    // individualizadas (aunque todav�a no s� porque raz�n) lo comentamos en
    // este m�todo.
    /*
         if (applet.getPerfil() != 5 )
         {
      String codNot = applet.pMaestroEDO.equiposNotificadores.siguiente();
      if (!codNot.equals(""))
      {
        applet.pMaestroEDO.actEquipoNotificador(codNot);
      }
         }
         // habilitamos panel para que pueda cambiar de notificador
         if (applet.getPerfil() != 5)
      applet.pMaestroEDO.enableControlsCab(true);
     */
  }

  protected void tbl_item(JCItemEvent e) {

  }

  protected void tbl_actionPerformed(JCActionEvent e) {
    //en el Borrado Maxivo no dejamos ni ver
    //en consulta, dejara ver
    if (super.modoOperacion != modoBAJA) {
      btnModificacion_actionPerformed();
    }
  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  private void ShowWarning(String sMessage) {
    applet.pMaestroEDO.ShowWarning(sMessage);
  }

}
