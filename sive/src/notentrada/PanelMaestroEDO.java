/*
 *  Autor         Fecha               Acci�n
 *    JRM         12/06/2000          Elimina los textos de �rea y distrito
 */
package notentrada;

//AIC
import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import cn.DataCN;
import fechas.CFecha;
import fechas.conversorfechas;
import notdata.DataBorrarMasivo;
import notdata.DataEntradaEDO;
import notdata.DataListaEDONum;
import notdata.DataListaNotifEDO;
import notdata.DataNumerica;
import notdata.DataOpeFc;
import notutil.Comunicador;
import notutil.UtilEDO;
import obj.CFechaSimple;
import sapp.StubSrvBD;

public class PanelMaestroEDO
    extends CPanel {

  public CLista listaOpeUltactTabla = new CLista();
  ResourceBundle resBun;

  //BACKUP DE REALES -teorico
  public String sRealesAntes = "0";
  public String sRealesDespues = "0";
  public boolean bCambiarReales = false;
  public String sTeorAntes = "0";
  public String sTeorDespues = "0";
  public boolean bCambiarTeor = false;

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/alta2.gif",
      "images/modificacion2.gif",
      "images/baja2.gif"};

  // modos de operaci�n del panel
  final int modoNORMAL = 0;
  final int modoINICIO = 1;
  final int modoESPERA = 2;

  // modos de operaci�n de los paneles detalle
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;
  final int modoCABECERA = 13;
  final int modoBOTONES = 14;
  final int modoCABECERAYBOTONES = 15;
  final int modoCONSULTAYBOTONES = 16;

  // modos de selecci�n de la fecha
  final int modoSELPORFECHA = 30;
  final int modoSELPORSEMANA = 31;

  // modos de operaci�n del servlet SrvMaestroEDO
  final int servletOBTENER_FLAGS_USU = 3;

  final int servletOBTENER_EQUIPO_X_CODIGO = 4;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION = 5;
  final int servletSELECCION_EQUIPO_X_CODIGO = 6;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION = 7;
  final int servletOBTENER_NMNOTIFS = 8;
  final int servletOBTENER_EQUIPO = 9;

  // JRM para peticion de ZBS
  final int servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS = 10;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION_CON_ZBS = 11;
  final int servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS = 12;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION_CON_ZBS = 13;

  // JRM2 para peticion de notificadores de baja
  final int servletOBTENER_EQUIPO_X_CODIGO_NOTIF = 20;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION_NOTIF = 21;
  final int servletSELECCION_EQUIPO_X_CODIGO_NOTIF = 22;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION_NOTIF = 23;

  // modos de operaci�n del servlet SrvNotifEDO
  final int servletSELECCION_LISTA_NOTIF = 3;
  final int servletSELECCION_LISTA_NOTIF_FECHA = 7;
  final int servletSELECCION_DATOS_NOTIF_SEM = 16;
  final int servletSELECCIONVALIDAR = 17;

  // modos de operaci�n del servlet SrvNumerica
  final int servletINSERT_EDO_NUMERICA = 0;
  final int servletUPDATE_EDO_NUMERICA = 1;

  // modos de operaci�n del servlet SrvCN
  final int servletGENERA_COBERTURA = 11;

  // modos de operaci�n del servlet SrvBorrarMasivo
  final int servletBORRAR = 0;

  // modos de operaci�n del servlet SrvBloqueo
  final int servletBLOQUEO_SELECT = 0;

  // constantes del panel
  final String strSERVLET_BLOQUEO = "servlet/SrvBloqueo";
  final String strSERVLET_MAESTRO = "servlet/SrvMaestroEDO";
  final String strSERVLET_NOTIFEDO = "servlet/SrvNotifEDO";
  final String strSERVLET_EDO_NUMERICA = "servlet/SrvNumerica";
  final String strSERVLET_CN = "servlet/SrvCN";
  final String strSERVLET_BORRADO_MASIVO = "servlet/SrvBorrarMasivo";

  // par�metros
  protected CLista parametros = null, result = null;
  CMessage msgBox = null;

  // booleans para habilitar botones seg�n flags de usuario
  boolean bAlta = false, bBaja = false, bMod = false;

  // Variables y objetos para recuperar fecha a partir de semana y viceversa
  conversorfechas convDesde = null;
  String sAno = "";
  String sCodSemIniBk = "";
  String sFecSemIniBk = "";
  String sCodEquipoIni = "";
  public int modoOperacion = modoNORMAL;
  protected int modoSEL = modoSELPORFECHA;
  protected int modoMOD = modoCABECERA;
  protected CLista lista = null;
  protected StubSrvBD stubCliente = null;
  protected String sCodNivel1 = "", sDesNivel1 = "", sCodNivel2 = "",
      sDesNivel2 = "", sAnno = "";
  protected String sCodEquipo = "", sDesEquipo = "", sCodCentro = "",
      sDesCentro = "";
  protected String sCobertura = "";
  protected String IT_RESSEM_PRIM = "N";
  protected boolean bListaNumVacia = true;
  protected String sNotifRealesPrim = "0", sTotNotifRealesPrim = "0";
  protected String sNotifRealesAnt = "0", sTotNotifRealesAnt = "0";
  protected int iNotifRealesAnt = 0, iTotNotifRealesAnt = 0;
  protected String sNotifTeorPrim = "", sNotifTeorAnt = "";
  NotificacionEDO applet;
  CLista listaTbl = null;
  boolean bNotifTeorChanged = false;
  // hashtable NotifEDO
  protected Hashtable hashNotifEDO = null;
  // boolean
  public boolean bAltaNotif = false;

  // controles
  XYLayout xYLayout = new XYLayout();
  //Label lDistrito = new Label();
  Label lFechaFinNotif = new Label();

  //AIC
  Label lNotificacion = new Label();

  Label lFechaIniNotif = new Label();

  Label lSemanaEpi = new Label();

  // ARS para Madrid
  Label lFechaNotif = new Label();

  //AIC
  CFechaSimple CfechaFinNotif = new CFechaSimple("N");
  CFechaSimple CfechaIniNotif = new CFechaSimple("N");
  // ARS para Madrid
  // ARG: Para rellenar automaticamente las barras
  //CFechaSimple CfechaNotif = new CFechaSimple("S");
  CFecha CfechaNotif = new CFecha("S");

  TextField txtSemanaEpi = new TextField();
  Label lNotif = new Label();
  CCampoCodigo txtCodEquipo = new CCampoCodigo();
  ButtonControl btnEquipo = new ButtonControl();

  // String para Madrid o CyL o donde sea
  String sLaComunidad = "";

  TextField txtDesEquipo = new TextField();
  Label lCentro = new Label();
  TextField txtCentro = new TextField();
  ButtonControl btnAlta = new ButtonControl();
  ButtonControl btnModificacion = new ButtonControl();
  ButtonControl btnBaja = new ButtonControl();
  ButtonControl btnNotif = new ButtonControl();
  Label lFechaRecep = new Label();
  // ARG: Para rellenar automaticamente las barras
  //CFechaSimple CfechaRecep = new CFechaSimple("S");
  CFecha CfechaRecep = new CFecha("S");
  Label lResSem = new Label();
  Checkbox chkbxResSem = new Checkbox();
  Label lNotifTeor = new Label();
  TextField txtNotifTeor = new TextField();
  Label lNotifReales = new Label();
  TextField txtNotifReales = new TextField();
  Label lTotNotifReales = new Label();
  TextField txtTotNotifReales = new TextField();
  Label lCobertura = new Label();
  TextField txtCobertura = new TextField();
  //LabelControl lblAviso = new LabelControl();

  ButtonControl btnValidar = new ButtonControl();

  // listeners
  PanelMaestroEDOactionAdapter actionAdapter = new PanelMaestroEDOactionAdapter(this);
  PanelMaestroEDOfocusAdapter focusAdapter = new PanelMaestroEDOfocusAdapter(this);

  /**
   * Panel de �rea, distrito y a�o. Se utilizar� desde aqui para habilitar
   * y deshabilitar el panel.
   * Este atributo se establece con el m�todo setPanelArDiAn llamado desde
   * NotificacionEDO
   */
  PanelArDiAn pArDiAn;

  /** Variable que permite controlar si las notificaciones reales
   * son correctas, para poder as� grabar.  ARS 28-06-01
   */
  public boolean bNotifRealesBien = true;

  /**
   * Objeto para guardar los equipos notificadores seleccionados
   */
  public ENotificadores equiposNotificadores = null;

  /**
   * Actualizamos el c�digo notificador
   */
  public void actEquipoNotificador(String codNot) {
    txtCodEquipo.setText(codNot);
    txtCodEquipo_focusLost();
  }

  /**
   * Actualiza el acceso al pabel de �rea, distrito y a�o
   * @param pArDiAn panel de �rea, distrito y a�o ya existente.
   */
  void setPanelArDiAn(PanelArDiAn pArDiAn) {
    this.pArDiAn = pArDiAn;
  }

  // constructor
  public PanelMaestroEDO(NotificacionEDO a, Hashtable hashNotifEDO) {
    try {
      setApp( (CApp) a);
      resBun = ResourceBundle.getBundle("notentrada.Res" + a.getIdioma());
      applet = a;
      this.hashNotifEDO = hashNotifEDO;
      // Cargamos la comunidad.
      sLaComunidad = this.getApp().getParameter("ORIGEN");
      if ( (sLaComunidad == null) || (sLaComunidad.length() == 0)) {
        sLaComunidad = "M";
        //cargamos autorizaciones
      }
      Vector vN1 = (Vector) applet.getCD_NIVEL_1_AUTORIZACIONES();
      Vector vN2 = (Vector) applet.getCD_NIVEL_2_AUTORIZACIONES();
      hashNotifEDO.put("CD_NIVEL_1_AUTORIZACIONES", vN1);
      hashNotifEDO.put("CD_NIVEL_2_AUTORIZACIONES", vN2);
      //perfil
      Integer perfil = new Integer(applet.getPerfil());
      hashNotifEDO.put("PERFIL", (String) perfil.toString());

      stubCliente = new StubSrvBD(new URL(this.app.getURL() +
                                          strSERVLET_MAESTRO));
      jbInit();
    }
    catch (Exception e) {
      ;
    }
  }

  private void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnEquipo.setImage(imgs.getImage(0));
    btnNotif.setImage(imgs.getImage(0));
    btnAlta.setImage(imgs.getImage(1));
    btnModificacion.setImage(imgs.getImage(2));
    btnBaja.setImage(imgs.getImage(3));
    btnValidar.setImage(imgs.getImage(0));

    // controles
    xYLayout.setHeight(168);
    xYLayout.setWidth(664);
    this.setLayout(xYLayout);
    this.setBackground(Color.lightGray);
    //lDistrito.setText(this.app.getNivel2() + ":");
    // ARS para Madrid
    if (sLaComunidad.equals("M")) {
      lFechaNotif.setText(resBun.getString("lNotificacion.Text"));
    }
    else {
      //AIC
      lNotificacion.setText(resBun.getString("lNotificacion.Text"));
      lFechaFinNotif.setText(resBun.getString("lFechaNotifFin.Text"));
      lFechaIniNotif.setText(resBun.getString("lFechaNotifIni.Text"));

      CfechaFinNotif.setEnabled(false);
      CfechaFinNotif.setEditable(false);
      CfechaIniNotif.setEnabled(false);
      CfechaIniNotif.setEditable(false);
    }
    lSemanaEpi.setText(resBun.getString("lSemanaEpi.Text"));
    txtSemanaEpi.setBackground(new Color(255, 255, 150));
    lNotif.setText(resBun.getString("lNotif.Text"));
    txtCodEquipo.setBackground(new Color(255, 255, 150));
    lCentro.setText(resBun.getString("lCentro.Text"));
    txtCentro.setEnabled(false);
    txtCentro.setEditable(false);
    lTotNotifReales.setText(resBun.getString("lTotNotifReales.Text"));
    txtTotNotifReales.setEditable(false);
    lCobertura.setText(resBun.getString("lCobertura.Text"));
    txtCobertura.setEditable(false);
    txtDesEquipo.setEnabled(false);
    txtDesEquipo.setEditable(false);
    lNotifTeor.setText(resBun.getString("lNotifTeor.Text"));
    btnBaja.setLabel(resBun.getString("btnBaja.Label"));
    btnModificacion.setLabel(resBun.getString("btnModificacion.Label"));
    lFechaRecep.setText(resBun.getString("lFechaRecep.Text"));
    lNotifReales.setText(resBun.getString("lNotifReales.Text"));
    lResSem.setText(resBun.getString("lResSem.Text"));
    btnAlta.setLabel(resBun.getString("btnAlta.Label"));
    btnNotif.setLabel(resBun.getString("btnNotif.Label"));
    btnValidar.setLabel(resBun.getString("btnValidar.Label"));

    //this.add(lblAviso, new XYConstraints(-10, 0, 0, 0));
    //this.add(lDistrito, new XYConstraints(285, 25, 48, 18));

    this.add(lSemanaEpi, new XYConstraints(21, 11, 143, -1));
    this.add(txtSemanaEpi, new XYConstraints(180, 11, 35, -1));

    if (sLaComunidad.equals("M")) {
      this.add(lFechaNotif, new XYConstraints(325, 11, 113, -1));
      this.add(CfechaNotif, new XYConstraints(440, 11, 85, -1));
    }
    else {
      //AIC
      this.add(lNotificacion, new XYConstraints(230, 11, 113, -1));
      this.add(lFechaIniNotif, new XYConstraints(345, 11, 70, -1));
      this.add(CfechaIniNotif, new XYConstraints(420, 11, 75, -1));
      this.add(lFechaFinNotif, new XYConstraints(511, 11, 60, -1));
      this.add(CfechaFinNotif, new XYConstraints(576, 11, 75, -1));
    }

    this.add(lNotif, new XYConstraints(21, 36, 68, -1));
    this.add(txtCodEquipo, new XYConstraints(92, 36, 50, -1));
    this.add(btnEquipo, new XYConstraints(150, 35, -1, -1));
    this.add(txtDesEquipo, new XYConstraints(180, 36, 163, -1));
    this.add(lCentro, new XYConstraints(371, 36, 67, -1));
    this.add(txtCentro, new XYConstraints(440, 36, 211, -1));
    this.add(lResSem, new XYConstraints(21, 102, 65, -1));
    this.add(chkbxResSem, new XYConstraints(92, 102, -1, -1));
    this.add(lNotifTeor, new XYConstraints(127, 102, 61, -1));
    this.add(txtNotifTeor, new XYConstraints(187, 102, 50, -1));
    this.add(lNotifReales, new XYConstraints(246, 102, 71, -1));
    this.add(txtNotifReales, new XYConstraints(317, 102, 46, -1));
    this.add(lTotNotifReales, new XYConstraints(370, 102, 94, -1));
    this.add(txtTotNotifReales, new XYConstraints(471, 102, 44, -1));
    this.add(lCobertura, new XYConstraints(523, 102, 62, -1));
    this.add(txtCobertura, new XYConstraints(585, 102, 66, -1));
    this.add(lFechaRecep, new XYConstraints(21, 126, 59, -1));
    this.add(CfechaRecep, new XYConstraints(92, 126, 97, -1));
    this.add(btnAlta, new XYConstraints(21, 66, 100, -1));
    this.add(btnModificacion, new XYConstraints(127, 66, 100, -1));
    this.add(btnBaja, new XYConstraints(234, 66, 100, -1));
    this.add(btnNotif, new XYConstraints(340, 66, 100, -1));
    this.add(btnValidar, new XYConstraints(446, 66, -1, -1));

    btnEquipo.setActionCommand("equipo");
    btnNotif.setActionCommand("notificacion");
    btnAlta.setActionCommand("alta");
    btnBaja.setActionCommand("baja");
    btnModificacion.setActionCommand("modificacion");
    btnNotif.addActionListener(actionAdapter);

    btnEquipo.addActionListener(actionAdapter);
    btnAlta.addActionListener(actionAdapter);
    btnBaja.addActionListener(actionAdapter);
    btnModificacion.addActionListener(actionAdapter);

    btnValidar.setActionCommand("validar");
    btnValidar.addActionListener(actionAdapter);

    txtSemanaEpi.setName("semanaepi");
    txtCodEquipo.setName("equipo");
    txtNotifReales.setName("notifreales");
    txtNotifTeor.setName("notifteor");
    txtTotNotifReales.setName("totnotifreales");
    txtCobertura.setName("cobertura");
    txtSemanaEpi.addFocusListener(focusAdapter);
    txtCodEquipo.addFocusListener(focusAdapter);

    txtNotifReales.addFocusListener(focusAdapter);

    txtNotifTeor.addFocusListener(focusAdapter);
    txtTotNotifReales.addFocusListener(focusAdapter);
    txtCobertura.addFocusListener(focusAdapter);

    chkbxResSem.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        chkbxResSem_itemStateChanged();
      }
    });

    // ARS para Madrid
    if (sLaComunidad.equals("M")) {
      CfechaNotif.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(FocusEvent e) {
          CfechaNotif_focusLost(e);
        }
      });
    }

    CfechaRecep.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusGained(FocusEvent e) {
        CfechaRecep_focusGanied(e);
      }

      public void focusLost(FocusEvent e) {
        CfechaRecep_focusLost(e);
      }
    });

    setBorde(false);

    lista = new CLista();
    listaTbl = new CLista();
    buttonControl1.setLabel("buttonControl1");
    listaTbl.setState(CLista.listaNOVALIDA);

    txtCobertura.setEnabled(false);
    txtTotNotifReales.setEnabled(false);
    //lblAviso.setVisible(false);
    doLayout();

    // inicia el panel
    InitMaster();
    // recupera los flags de usuario
    GetFlagsUser();

    // LLAMAR AL M�TODO EnableWithFlags() UNA VEZ QUE SE
    // IMPLEMENTEN LAS RESTRICCIONES DE OPERACI�N (MODIFICAR, ETC)
    // EN FUNCI�N DE LOS FLAGS DE USUARIO
    //EnableWithFlags();

    //validacion
    btnValidar.setVisible(false);

    //CARGAR PEGOTE!!! perfil 5
    if (applet.getIT_MODULO_3().trim().equals("S") &&
        applet.getPerfil() == 5) {
      Reconstruir();
    } //--------------------------------------------

    modoOperacion = modoCABECERA;
    Inicializar();
  }

  public void Reconstruir() {
    //pinta el equipo y carga la lista de bloqueo del equipo
    modoOperacion = modoESPERA;
    Inicializar();

    listaOpeUltactTabla = new CLista();

    txtCodEquipo.setText(applet.getCD_E_NOTIF().trim());
    CLista plista = new CLista();
    CLista result = new CLista();
    plista.addElement(new DataEntradaEDO
                      (getCodEquipo(),
                       (String) hashNotifEDO.get("CD_ANOEPI"),
                       "",
                       sCodNivel1,
                       sCodNivel2));
    try {
      result = Comunicador.Communicate(getApp(),
                                       stubCliente,
                                       servletOBTENER_EQUIPO,
                                       strSERVLET_MAESTRO,
                                       plista);

      if (result.size() == 0) {
        //no puede ser
      }
      else {

        DataEntradaEDO datos = (DataEntradaEDO) result.firstElement();
        hashNotifEDO.put("CD_NIVEL_1", sCodNivel1);
        hashNotifEDO.put("CD_NIVEL_2", datos.getNivel2().trim());

        ActDatosCabecera( (DataEntradaEDO) result.elementAt(0));
        if (modoOperacion != modoESPERA) {
          modoOperacion = modoESPERA;
          Inicializar();
        }
      }
    }
    catch (Exception e) {
      ShowWarning(resBun.getString("msg34.Text"));
      modoOperacion = modoCABECERA;
      Inicializar();
      applet.VerNotificaciones();

    }
  } //fin reconstruir

  public void Pintar_Validar() {

    //boton validar: si procede modulo 3 + soy 2,3,4 + permiso para validar
    if (this.getApp().getIT_MODULO_3().equals("S") &&
        this.getApp().getIT_FG_VALIDAR().equals("S") &&
        (this.getApp().getPerfil() == 2 ||
         this.getApp().getPerfil() == 3 ||
         this.getApp().getPerfil() == 4)) {
      btnValidar.setVisible(false);
      btnValidar.setVisible(true);
    }
    else {
      btnValidar.setVisible(false);
      //---------------------------------------------------------
    }
  }

  public void habilitar_Validar() {

    //boton validar: si procede modulo 3 + soy 2,3,4 + permiso para validar
    if (this.getApp().getIT_MODULO_3().equals("S") &&
        this.getApp().getIT_FG_VALIDAR().equals("S") &&
        (this.getApp().getPerfil() == 2 ||
         this.getApp().getPerfil() == 3 ||
         this.getApp().getPerfil() == 4)) {
      btnValidar.setEnabled(true);
    }
    else {
      btnValidar.setEnabled(true);
      //---------------------------------------------------------
    }
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      /* case modoNORMAL:
         EnableWithFlags();
           //# // System_out.println("modoNORMAL");
           Pintar_Validar();
         setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
         break;*/

      // modoINICIO: modo inicial de operaci�n
      // s�lo controles de cabecera habilitados (e iniciados)
      case modoINICIO:
        initControlsNotif();
        lResSem.setVisible(false);
        chkbxResSem.setVisible(false);
        lFechaRecep.setVisible(false);
        CfechaRecep.setVisible(false);
        showControls(false);
        enableButtons(false);
        applet.pListaEDONum.Inicializar(modoINICIO);
        applet.pListaEDOIndiv.Inicializar(modoINICIO);

        habilitar_Validar();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        // modoESPERA: todos los controles deshabilitados
      case modoESPERA:
        enableControls(false);
        applet.pListaEDONum.Inicializar(modoESPERA);
        applet.pListaEDOIndiv.Inicializar(modoESPERA);
        //repetir detras de cada wait
        btnEquipo.setEnabled(true);
        btnEquipo.setEnabled(false);
        btnEquipo.doLayout();

        habilitar_Validar();

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        //modoALTA: controles de notificaci�n habilitados
        //paneles Num e Indiv, iniciados a modoALTA
      case modoALTA:
        boolean ChequeadoCheck = chkbxResSem.getState();

        enableControlsCab(false);
        enableButtons(false);
        initControlsNotif();
        enableControlsNotif(true);
        applet.pListaEDONum.Inicializar(modoALTA);
        applet.pListaEDOIndiv.Inicializar(modoALTA);

        lFechaRecep.setVisible(true);
        CfechaRecep.setVisible(true);
        CfechaRecep.setEnabled(true);

        ActDatosNotif(); //los pinta  (los Prim)
        if (ChequeadoCheck) {
          chkbxResSem.setState(true);

        }
        habilitar_Validar();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        //modoMODIFICACI�N: controles de notificaci�n habilitados
        //paneles Num e Indiv, iniciadoa a modoMODIFICACI�N
      case modoMODIFICACION:
        enableControlsCab(false);
        enableButtons(false);
        enableControlsNotif(true);

        applet.pListaEDONum.Inicializar(modoMODIFICACION);
        applet.pListaEDOIndiv.Inicializar(modoMODIFICACION);

        lFechaRecep.setVisible(true);
        CfechaRecep.setVisible(true);
        CfechaRecep.setEnabled(false);

        habilitar_Validar();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        //modoCONSULTA: parecido a modif pero deshabilitado cajas de notif
        //paneles Num e Indiv, iniciados a modoMODIFICACI�N
      case modoCONSULTA:
        enableControlsCab(false);
        enableButtons(false);
        enableControlsNotif(true); //cajas deshabilitadas
        //chkbxResSem.setEnabled(en);
        txtNotifTeor.setEnabled(false);
        txtNotifReales.setEnabled(false);
        txtTotNotifReales.setEnabled(false);
        txtCobertura.setEnabled(false);
        applet.pListaEDONum.Inicializar(modoCONSULTA);
        applet.pListaEDOIndiv.Inicializar(modoCONSULTA);

        lFechaRecep.setVisible(true);
        CfechaRecep.setVisible(true);
        CfechaRecep.setEnabled(false);

        habilitar_Validar();

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        //modoCONSULTAYBOTONES: modo CONSULTA y cambio de notificador habilitado.
      case modoCONSULTAYBOTONES:
        enableControlsCab(false);
        enableButtons(false);
        enableControlsNotif(true); //cajas deshabilitadas
        //chkbxResSem.setEnabled(en);
        txtNotifTeor.setEnabled(false);
        txtNotifReales.setEnabled(false);
        txtTotNotifReales.setEnabled(false);
        txtCobertura.setEnabled(false);
        applet.pListaEDONum.Inicializar(modoCONSULTA);
        applet.pListaEDOIndiv.Inicializar(modoCONSULTA);

        lFechaRecep.setVisible(true);
        CfechaRecep.setVisible(true);
        CfechaRecep.setEnabled(false);

        //habilitar equipo
        if (sLaComunidad.equals("M")) {
          if (!txtSemanaEpi.getText().equals("")
              && !CfechaNotif.getText().equals("")) {
            txtCodEquipo.setEnabled(true);
            btnEquipo.setEnabled(true);
          }
        }
        else {
          if (!txtSemanaEpi.getText().equals("")
              && !CfechaFinNotif.getText().equals("")) {
            txtCodEquipo.setEnabled(true);
            btnEquipo.setEnabled(true);
          }
        }

        habilitar_Validar();

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        //modoBAJA: controles de notificaci�n habilitados
        //paneles Num e Indiv, iniciados a modoBAJA
      case modoBAJA:
        enableControlsCab(false);
        enableButtons(false);
        enableControlsNotif(false);
        applet.pListaEDONum.Inicializar(modoBAJA);
        applet.pListaEDOIndiv.Inicializar(modoBAJA);

        lFechaRecep.setVisible(true);
        CfechaRecep.setVisible(true);
        CfechaRecep.setEnabled(false);

        habilitar_Validar();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        // Modo CABECERA (controles de cabecera habilitados, resto no)
      case modoCABECERA:

        // ARS para Madrid
        if (sLaComunidad.equals("M")) {
          CfechaNotif.setEnabled(true);
        }
        else {
          //AIC
          CfechaFinNotif.setEnabled(false);
          CfechaIniNotif.setEnabled(false);
        }

        txtSemanaEpi.setEnabled(true);
        txtCodEquipo.setEnabled(false);
        btnEquipo.setEnabled(false);
        enableButtons(false);
        showControls(false);
        lResSem.setVisible(false);
        chkbxResSem.setVisible(false);
        initControlsNotif();
        lFechaRecep.setVisible(false);
        CfechaRecep.setVisible(false);
        CfechaRecep.setEnabled(false);
        applet.pListaEDONum.Inicializar(modoINICIO);
        applet.pListaEDOIndiv.Inicializar(modoINICIO);

        //habilitar equipo
        // Para Madrid 26-03-01 (ARS)
        if (sLaComunidad.equals("M")) {
          if (!txtSemanaEpi.getText().equals("")
              && !CfechaNotif.getText().equals("")) {
            txtCodEquipo.setEnabled(true);
            btnEquipo.setEnabled(true);
          }
        }
        else {
          if (!txtSemanaEpi.getText().equals("")
              && !CfechaFinNotif.getText().equals("")) {
            txtCodEquipo.setEnabled(true);
            btnEquipo.setEnabled(true);
          }
        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        btnValidar.setEnabled(false);

        break;

        //  Modo BOTONES (se ha superado correctamente la cabecera)
        //    Controles de cabecera deshabilitados
        //    Botones habilitados
        //    Controles de notificaci�n deshabilitados
        //    Controles de detalle deshabilitados
        //    Opcion de Cancelar y volver a CabeceraYBotones
      case modoBOTONES:
        EnableWithFlags();
        lResSem.setVisible(false);
        chkbxResSem.setVisible(false);
        showControls(false);
        lFechaRecep.setVisible(false);
        CfechaRecep.setVisible(false);
        CfechaRecep.setEnabled(false);
        applet.pListaEDONum.Inicializar(modoINICIO);
        applet.pListaEDOIndiv.Inicializar(modoINICIO);

        //Pintar_Validar();

        //para poder cancelar (perfil 5 sin sentido)
        if (applet.getPerfil() != 5) {
          applet.pListaEDONum.btnCancelar.setEnabled(true);
          applet.pListaEDOIndiv.btnCancelar.setEnabled(true);
        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        // Desde el bot�n "Cancelar" de las listas EDO NUM e INDIV
        // se permite modificar la cabecera, o bien la acci�n
        //    Cabecera habilitada
        //    Botones habilitados
        //    Controles de notificacion deshabilitados
        //    Controles de detalle deshabilitados
      case modoCABECERAYBOTONES:
        enableControlsCab(true);
        EnableWithFlags();
        lResSem.setVisible(false);
        chkbxResSem.setVisible(false);
        showControls(false);
        lFechaRecep.setVisible(false);
        CfechaRecep.setVisible(false);
        CfechaRecep.setEnabled(false);
        applet.pListaEDONum.Inicializar(modoINICIO);
        applet.pListaEDOIndiv.Inicializar(modoINICIO);

        //Pintar_Validar();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }

    //CARGAR PEGOTE!!! perfil 5
    if (applet.getIT_MODULO_3().trim().equals("S") &&
        applet.getPerfil() == 5) {
      txtCodEquipo.setEnabled(false);
      btnEquipo.setEnabled(false);
    }
    doLayout();
  }

  // actua el modo de "modificaci�n" del panel al modo "modo"
  public void setModoMod(int modo) {
    modoMOD = modo;
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // habilita / deshabilita los controles de cabecera, que son:
  //    CfechaFinNotif, txtSemanaEpi, txtCodEquipo, btnEquipo
  public void enableControlsCab(boolean en) {
    // ARS para Madrid
    if (sLaComunidad.equals("M")) {
      CfechaNotif.setEnabled(en);
    }
    //AIC
    //CfechaFinNotif.setEnabled(en);
    txtSemanaEpi.setEnabled(en);
    txtCodEquipo.setEnabled(en);
    btnEquipo.setEnabled(en);
  }

  /**
   * Habilita/deshabilita la parte de equipo notificador. Este m�todo es
   * importante porque cuando el notificador es de perfil 5 �ste tiene asignado
   * un equipo notificador que no puede cambiarse
   */
  void habilitarEqNot(boolean bOK) {
    txtCodEquipo.setEnabled(bOK);
    btnEquipo.setEnabled(bOK);
  }

  /**
   * Sirve para habilitar/deshabilitar desde el panel de
   * �rea/distrito y zona b�sica de salud.
   */
  public void habilitarDesdePanelArDiAn(boolean bOK) {
    // ARS para Madrid
    if (sLaComunidad.equals("M")) {
      CfechaNotif.setEnabled(bOK);
      // Cabecera, botones.
      //AIC
      //CfechaFinNotif.setEnabled(bOK);

    }
    txtSemanaEpi.setEnabled(bOK);
    if (!bOK) {
      txtCodEquipo.setEnabled(bOK);
      btnEquipo.setEnabled(bOK);
      btnAlta.setEnabled(bOK);
      btnBaja.setEnabled(bOK);
      btnModificacion.setEnabled(bOK);
      btnNotif.setEnabled(bOK);
      chkbxResSem.setEnabled(bOK);
      txtNotifTeor.setEnabled(bOK);
      txtNotifReales.setEnabled(bOK);
      txtTotNotifReales.setEnabled(bOK);
      txtCobertura.setEnabled(bOK);
      Pintar_Validar();
    }

    validate();
  }

  // habilita / deshabilita los controles del panel
  private void enableControls(boolean en) {
    if (sLaComunidad.equals("M")) {
      CfechaNotif.setEnabled(en);

      //AIC
      //CfechaFinNotif.setEnabled(en);

    }
    txtSemanaEpi.setEnabled(en);
    txtCodEquipo.setEnabled(en);
    btnEquipo.setEnabled(true);
    btnEquipo.setEnabled(en);
    btnAlta.setEnabled(en);
    btnBaja.setEnabled(en);
    btnModificacion.setEnabled(en);
    btnNotif.setEnabled(en);
    chkbxResSem.setEnabled(en);
    txtNotifTeor.setEnabled(en);
    txtNotifReales.setEnabled(en);
    txtTotNotifReales.setEnabled(en);
    txtCobertura.setEnabled(en);
    //btnValidar.setEnabled(en);
    //btnValidar.setEnabled(en);

    validate();
  }

  // habilita / deshabilita los botones de acciones de panel, que son:
  //    btnAlta, btnBaja, btnModificacion, btnNotif
  private void enableButtons(boolean en) {
    btnAlta.setEnabled(en);
    btnBaja.setEnabled(en);
    // JRM2 Si entra en modo consulta, por lo menos que deje mirar
    if (modoOperacion == modoCONSULTA || modoOperacion == modoCONSULTAYBOTONES) {
      btnModificacion.setEnabled(true);
    }
    else {
      btnModificacion.setEnabled(en);
    }
    btnNotif.setEnabled(en);
    validate();
  }

  // habilita / deshabilita los controles de notificaion, que son:
  //    chkbxResSem, txtNotifTeor, txtNotifReales, txtTotNotifReales,
  //    txtCobertura
  private void enableControlsNotif(boolean en) {
    chkbxResSem.setEnabled(en);
    txtNotifTeor.setEnabled(en);
    txtNotifReales.setEnabled(en);
    txtTotNotifReales.setEnabled(en);
    txtCobertura.setEnabled(en);
  }

  // inicia los controles correspondientes a la notificaci�n:
  //    CfechaRecep, chkbxResSem, txtNotifTeor, txtNotifReales,
  //    txtTotNotifReales, txtCobertura
  private void initControlsNotif() {

    //ActCFecha(CfechaRecep, UtilEDO.getFechaAct());
    // modificacion jlt 22/10/2001
    // se deja en blanco la fecha de recepci�n, aunque
    // sigue siendo obligatoria
    CfechaRecep.setText("");

    chkbxResSem.setState(false); //visible, no clickeado
    txtNotifTeor.setText("");
    txtNotifReales.setText("");
    txtTotNotifReales.setText("");
    txtCobertura.setText("");
  }

  // muestra / oculta controles correspondientes a la notificaci�n
  private void showControls(boolean sh) {
    lNotifTeor.setVisible(sh);
    txtNotifTeor.setVisible(sh);
    lNotifReales.setVisible(sh);
    txtNotifReales.setVisible(sh);
    lTotNotifReales.setVisible(sh);
    txtTotNotifReales.setVisible(sh);
    lCobertura.setVisible(sh);
    txtCobertura.setVisible(sh);
    validate();
  }

  // actua datos de hashNotifEDO con los datos de cabecera
  //    c�digo y descripci�n de equipo, a�o y semana
  // se llama a este m�todo desde la p�rdida del foco de txtCodEquipo
  //    ( si es que los datos de cabecera son correctos)
  private void ActDatosHash() {
    hashNotifEDO.put("CD_E_NOTIF", this.getCodEquipo());
    hashNotifEDO.put("DS_E_NOTIF", this.getDesEquipo());
    hashNotifEDO.put("CD_ANOEPI", this.getAnno());
    hashNotifEDO.put("CD_SEMEPI", this.getSemana());
  }

  // inicia el panel
  public void InitMaster() {

  }

  /**
   * Funci�n llamada desde el panel de �rea, distrito y a�o para
   * que se actuaicen correctamente las fechas.
   */
  /*
   * Autor           Fecha             Acci�n
   *  JRM            12/06/2000        La crea
   */
  public void ActFechaDesdePanelArDiAn(String sAnno,
                                       String CodArea,
                                       String DesArea,
                                       String CodDistrito,
                                       String DesDistrito) {
    // Para los posibles procesos que lo puedan recoger.
    hashNotifEDO.put("CD_ANOEPI", sAnno);
    this.sAnno = sAnno;

    //objeto para conversi�n de num semana a fecha y viceversa
    convDesde = new conversorfechas(sAnno, app);

    //AIC... TRUCULENTO PERO EFECTIVO.
    sCodSemIniBk = "";
    txtSemanaEpi_focusLost();

    // por defecto, la fecha de recepci�n es la del d�a
    //ActCFecha(CfechaRecep, UtilEDO.getFechaAct());
    // modificacion jlt 22/10/2001
    // ponemos la fecha de recepci�n en blanco
    CfechaRecep.setText("");

    // se toman los datos recibidos en hashNotifEDO
    hashNotifEDO.put("CD_NIVEL_1", CodArea);
    hashNotifEDO.put("DS_NIVEL_1", DesArea);
    this.sCodNivel1 = CodArea;
    this.sDesNivel1 = DesArea;

    // el nivel 2 puede venir a null por no ser necesario
    //AIC
    //if (!CodDistrito.equals("")){
    hashNotifEDO.put("CD_NIVEL_2", CodDistrito);
    this.sCodNivel2 = CodDistrito;
    //}
    //if (!DesDistrito.equals("")){
    hashNotifEDO.put("DS_NIVEL_2", DesDistrito);
    this.sDesNivel2 = DesDistrito;
    //}
  }

// m�todo que recoge los flas de usuario
  public void GetFlagsUser() {

    //leemos del applet
    //permisos
    if (this.getApp().getIT_AUTALTA().equals("S")) {
      bAlta = true;
    }
    else {
      bAlta = false;
    }
    if (this.getApp().getIT_AUTBAJA().equals("S")) {
      bBaja = true;
    }
    else {
      bBaja = false;
    }
    if (this.getApp().getIT_AUTMOD().equals("S")) {
      bMod = true;
    }
    else {
      bMod = false;

      //confidencialidad
    }
    hashNotifEDO.put("IT_FG_ENFERMO", this.getApp().getIT_FG_ENFERMO());

  } //fin getFlag

  // habilitar los botones btnAlta, btnBaja, btnModificacion
  // y btnNotif en funci�n de los flags de usuario
  void EnableWithFlags() {
    btnAlta.setEnabled(bAlta);
    btnBaja.setEnabled(bBaja);
    btnModificacion.setEnabled(bMod);
    btnNotif.setEnabled(bMod);

    //MODO CONSULTA  (si no tiene permisos de nada, -> consulta)
    if (!bAlta && !bBaja && !bMod) {
      btnNotif.setEnabled(true);
    }
  }

  // realiza la baja de la notificaci�n: Borrado Masivo
  public void BajaNotificacion() {

    //Reserva de la listaOpeUltactTabla
    CLista listaReservada = new CLista(); //lleva e_notif, notif_sem, notifedo
    DataOpeFc dataProvisional = null;
    for (int i = 0; i < listaOpeUltactTabla.size(); i++) {
      dataProvisional = (DataOpeFc) listaOpeUltactTabla.elementAt(i);
      listaReservada.addElement(dataProvisional);
    } //----------------------------------

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();

    CLista data = new CLista();

    //control bloqueo
    //en la lista dejo solo notif_sem
    listaOpeUltactTabla.removeElementAt(0); //quito e_notif
    //mi lista tiene 0: notif_sem, 1: notifedo
    listaOpeUltactTabla.removeElementAt(1); //quito notifedo

    if (!ComprobarOpeFc_borrar_Maxivo()) {
      ActListaBloqueo(listaReservada, false, ""); //carga la reserva
      //los datos de pantalla son los que debe haber
      modoOperacion = modo;
      Inicializar();
      return;
    } //-----------------------------

    String TotalReales = "";
    String it_actualizar_notif_sem = "";

    if (sCobertura.equals("N")) {
      sCobertura = "N"; //borrar� notif_sem
      it_actualizar_notif_sem = "N";
    }
    if (sCobertura.equals("S") &&
        IT_RESSEM_PRIM.equals("N")) { //antes No
      sCobertura = "S"; //no la borra
      it_actualizar_notif_sem = "N"; //no la actualiza
    }
    if (sCobertura.equals("S") &&
        IT_RESSEM_PRIM.equals("S")) { //antes Si
      sCobertura = "S"; //no la borra
      it_actualizar_notif_sem = "S"; //si la actualiza
      TotalReales = getDefNotifReales();
    }
    //-------------------------------------------
    DataBorrarMasivo dataBorrar = new DataBorrarMasivo(
        sCobertura, it_actualizar_notif_sem,
        getCodEquipo(),
        getAnno(),
        getSemana(),
        getFechaRecep(),
        getFechaNotif(),
        TotalReales,
        getLogin(),
        getFechaAct());

    parametros = new CLista();
    parametros.addElement(dataBorrar);

    try {
      result = Comunicador.Communicate(getApp(),
                                       stubCliente,
                                       servletBORRAR,
                                       strSERVLET_BORRADO_MASIVO,
                                       parametros);
      //si devuelve en la lista un String ="OK" fue bien
      //si devuelve en la lista un String ="Sin Permisos" No puede borrar
      String mensaje = (String) result.firstElement();
      if (mensaje.equals("OK")) {
        ShowWarning(resBun.getString("msg35.Text"));
        modo = modoCABECERAYBOTONES;
        //actualizamos valores validacion //ha ido bien
        applet.pListaEDOIndiv.actualizar(modoBAJA);
        applet.pListaEDONum.actualizar(modoBAJA);
        applet.pListaEDONum.btnCancelar_actionPerformed();
      }
      else {
        ShowWarning(resBun.getString("msg36.Text"));
        ActListaBloqueo(listaReservada, false, ""); //carga la reserva
      }

    }
    catch (Exception e) {
      ShowWarning(resBun.getString("msg37.Text"));
      ActListaBloqueo(listaReservada, false, ""); //carga la reserva
    }

    modoOperacion = modo;
    Inicializar();
  }

//Si procede cobertura, lo tiene clickeado, y
//ha cambiado Total Reales y Teoricos
  public boolean NohaCambiadoTeorTotalReales() {
    if (!getNotifTeorPrim().equals(txtNotifTeor.getText().trim())
        || !sTotNotifRealesPrim.equals(txtTotNotifReales.getText().trim())) {
      return (false);
    }
    else {
      return (true);
    }

  }

  public void MontarLista_Modif(String ActNotif_Sem, String sT, String sTPrim) {

    //Montar la listaOpeUltactTabla con lo que debamos comprobar-------------
    if (ActNotif_Sem.equals("N")) { //solo notifedo
      listaOpeUltactTabla.removeElementAt(0); //quito e_notif
      listaOpeUltactTabla.removeElementAt(0); //quito notif_sem (1)
    }
    else { //procede notif_sem y puede que e_notif
      //�tengo que actualizar tb e_notif?, solo si Nuevo <> Antiguo y 'S'
      if (!sT.trim().equals(sTPrim.trim())
          && getResSem().equals("S")) {
        //actualizare e_notif, y logicamente notif_sem  (3)
      }
      else {
        listaOpeUltactTabla.removeElementAt(0); //quito comprobar e_notif (2)
      }
    }
    //comentario MODIF: 1 notifedo, 2 notif_sem, notifedo, 3 e, notif_sem, notifedo
    //comentario ALTA: 1 notifedo, 2 notif_sem, notifedo, 3 e, notif_sem, notifedo
  }

//-------
  public boolean Actualizar_Notif_Sem() {

    if (sCobertura.equals("N")) {
      return (false);
    }
    if (sCobertura.equals("S") &&
        chkbxResSem.getState() && //ahora clickeado
        IT_RESSEM_PRIM.equals("N")) { //antes No
      //solo si ha cambiado
      if (NohaCambiadoTeorTotalReales()) {
        return (false);
      }
      else {
        return (true);
      }
    }
    if (sCobertura.equals("S") &&
        !chkbxResSem.getState() && //ahora no clickeado
        IT_RESSEM_PRIM.equals("S")) { //antes Si
      //actualizar las cajas:
      txtNotifTeor.setText(sNotifTeorPrim);
      txtTotNotifReales.setText(getDefNotifReales());
      txtNotifReales.setText("0");
      return (true);
      //luego guardare en primeros, y llamare a ActDatosNotif() .. actCobertura y anteriores
    }
    if (sCobertura.equals("S") &&
        !chkbxResSem.getState() && //ahora no clickeado
        IT_RESSEM_PRIM.equals("N")) { //antes No
      return (false);
    }
    if (sCobertura.equals("S") &&
        chkbxResSem.getState() && //ahora clickeado
        IT_RESSEM_PRIM.equals("S")) { //antes Si
      //solo si ha cambiado
      if (NohaCambiadoTeorTotalReales()) {
        return (false);
      }
      else {
        return (true);
      }
    }
    return (true);
  }

//Carga la estructura de datos para realizar un Alta
  public CLista Cargar_Data_Alta(CLista listaTablaDia,
                                 String sTeoricoPrim_puedeque_cambiado,
                                 String it_actualizar_notif_sem) {

    CLista parametros = new CLista();
    DataListaEDONum dataListaNum;
    DataNumerica dataNum;

    // ahorallena
    if (listaTablaDia.size() > 0) {
      for (int i = 0; i < listaTablaDia.size(); i++) {
        dataListaNum = (DataListaEDONum) listaTablaDia.elementAt(i);
        dataNum = new DataNumerica("", "N", it_actualizar_notif_sem,
                                   (String) hashNotifEDO.get("CD_E_NOTIF"),
                                   (String) hashNotifEDO.get("CD_ANOEPI"),
                                   (String) hashNotifEDO.get("CD_SEMEPI"),
                                   getFechaRecep(), getNotifReales(),
                                   getFechaNotif(),
                                   getResSem(), this.getApp().getLogin(),
                                   UtilEDO.getFechaAct(),
                                   applet.pListaEDONum.getIT_VALIDADA(),
                                   applet.pListaEDONum.getFC_FECVALID(),
                                   dataListaNum.getCodEnfer(),
                                   dataListaNum.getCasos(),
                                   sTeoricoPrim_puedeque_cambiado,
                                   getNotifTeorAct(), getTotNotifReales());

        parametros.addElement(dataNum);
      } //for
    }
    //tabla vacia
    if (listaTablaDia.size() == 0) {

      dataNum = new DataNumerica("", "S", it_actualizar_notif_sem,
                                 (String) hashNotifEDO.get("CD_E_NOTIF"),
                                 (String) hashNotifEDO.get("CD_ANOEPI"),
                                 (String) hashNotifEDO.get("CD_SEMEPI"),
                                 getFechaRecep(), getNotifReales(),
                                 getFechaNotif(),
                                 getResSem(),
                                 this.getApp().getLogin(), UtilEDO.getFechaAct(),
                                 applet.pListaEDONum.getIT_VALIDADA(),
                                 applet.pListaEDONum.getFC_FECVALID(),
                                 "", "",
                                 sTeoricoPrim_puedeque_cambiado,
                                 getNotifTeorAct(), getTotNotifReales());
      parametros.addElement(dataNum);
    }

    return (parametros);
  }

//Carga la estructura de datos para realizar una Modificacion
  public CLista Cargar_Data_Modif(CLista listaTablaDia,
                                  String sTeoricoPrim_puedeque_cambiado,
                                  String it_actualizar_notif_sem) {

    CLista parametros = new CLista();
    DataListaEDONum dataListaNum;
    DataNumerica dataNum;

    // ahorallena  o ahoravacia + antesllena
    if (listaTablaDia.size() > 0 ||
        (listaTablaDia.size() == 0 && !bListaNumVacia)) {

      int i = 0;
      for (i = 0; i < listaTablaDia.size(); i++) {
        dataListaNum = (DataListaEDONum) listaTablaDia.elementAt(i);
        dataNum = new DataNumerica("N", "", it_actualizar_notif_sem,
                                   (String) hashNotifEDO.get("CD_E_NOTIF"),
                                   (String) hashNotifEDO.get("CD_ANOEPI"),
                                   (String) hashNotifEDO.get("CD_SEMEPI"),
                                   getFechaRecep(), getNotifReales(),
                                   getFechaNotif(),
                                   getResSem(), this.getApp().getLogin(),
                                   UtilEDO.getFechaAct(),
                                   applet.pListaEDONum.getIT_VALIDADA(),
                                   applet.pListaEDONum.getFC_FECVALID(),
                                   dataListaNum.getCodEnfer(),
                                   dataListaNum.getCasos(),
                                   sTeoricoPrim_puedeque_cambiado,
                                   getNotifTeorAct(), getTotNotifReales());

        parametros.addElement(dataNum);
      } //for
      if (i == 0) { //lo borre todo
        dataNum = new DataNumerica("N", "", it_actualizar_notif_sem,
                                   (String) hashNotifEDO.get("CD_E_NOTIF"),
                                   (String) hashNotifEDO.get("CD_ANOEPI"),
                                   (String) hashNotifEDO.get("CD_SEMEPI"),
                                   getFechaRecep(), getNotifReales(),
                                   getFechaNotif(),
                                   getResSem(), this.getApp().getLogin(),
                                   UtilEDO.getFechaAct(),
                                   applet.pListaEDONum.getIT_VALIDADA(),
                                   applet.pListaEDONum.getFC_FECVALID(),
                                   "", "",
                                   sTeoricoPrim_puedeque_cambiado,
                                   getNotifTeorAct(), getTotNotifReales());
        parametros.addElement(dataNum);

      }

    }
    //ahoravacia + antesvacia
    if (listaTablaDia.size() == 0 && bListaNumVacia) {
      dataNum = new DataNumerica("S", "", it_actualizar_notif_sem,
                                 (String) hashNotifEDO.get("CD_E_NOTIF"),
                                 (String) hashNotifEDO.get("CD_ANOEPI"),
                                 (String) hashNotifEDO.get("CD_SEMEPI"),
                                 getFechaRecep(), getNotifReales(),
                                 getFechaNotif(),
                                 getResSem(), this.getApp().getLogin(),
                                 UtilEDO.getFechaAct(),
                                 applet.pListaEDONum.getIT_VALIDADA(),
                                 applet.pListaEDONum.getFC_FECVALID(),
                                 "", "",
                                 sTeoricoPrim_puedeque_cambiado,
                                 getNotifTeorAct(), getTotNotifReales());

      parametros.addElement(dataNum);
    }
    return (parametros);

  } //fin cargar

// m�todo para modificaci�n de notif. + EDO num�ricas
  public void ModNotificacion(CLista listaTblDia) {

    //FALTAN LOS MODOS!!!!!!!!!!!!!!!!
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    //Reserva de la listaOpeUltactTabla
    CLista listaReservada = new CLista(); //lleva e_notif, notif_sem, notifedo
    DataOpeFc dataProvisional = null;
    for (int i = 0; i < listaOpeUltactTabla.size(); i++) {
      dataProvisional = (DataOpeFc) listaOpeUltactTabla.elementAt(i);
      listaReservada.addElement(dataProvisional);
    } //----------------------------------

    //variables que entran al servlet para cambiar
    String sTeor = "", sTeorPrim = "", sReal = "", sTotReal = "";

    //Almacen para restaurar valores en caso de fallo
    //valores iniciales ie al llegar a la funcion VALORES DE PANTALLA
    String sPantallaTeor = getNotifTeorAct();
    String sPantallaReal = getNotifReales();
    String sPantallaTotReal = getTotNotifReales();

    //cargar valores de la pantalla  en la variables locales al metodo
    sReal = sPantallaReal;
    sTotReal = sPantallaTotReal;
    sTeor = sPantallaTeor;

    //valores de la Base de datos (los de Primeros):
    //las variables globales primeras no se han tocado
    String sBD_NOTIF_SEM_TeorPrim = getNotifTeorPrim();
    String sBDReal = sNotifRealesPrim;
    String sBDTotReal = sTotNotifRealesPrim;

////E_NOTIF!!!!!!!
//!!!!!!!!!!!!!!!! cambiara  sTeorPrim

    //las cajas tienen lo que deben llevar
    //pasar a Prim y actdatosNotif() (cobertura , anteriores)
    if (sCobertura.equals("S") &&
        chkbxResSem.getState() &&
        !NohaCambiadoTeorTotalReales()) { //veo lo de equipo!!!

      //teor antiguo <> teor nuevo :
      //Este apartado cambia sTeorPrim si procede, al dato de pantalla
      //responde SI: habra que actualizar lo de la pantalla en:
      //sTeorPrim del programa  , pero se hace depues
      //E_NOTIF y NOTIF_SEM
      //teor antiguo <> teor nuevo :
      //Este apartado cambia sTeorPrim si procede, al dato de pantalla
      //responde NO: habra que actualizar lo de la pantalla en:
      //sTeorPrim del programa  , pero se hace depues
      // y NOTIF_SEM

      if (!sBD_NOTIF_SEM_TeorPrim.equals(sPantallaTeor)) {
        // si no coinciden, se pide confirmaci�n
        CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                       resBun.getString("msg38.Text"));
        msgBox.show();
        if (msgBox.getResponse()) { //SI, sTeorPrim debe ser distinto a sTeor para que el servelt actualice e_notif
          sTeorPrim = sBD_NOTIF_SEM_TeorPrim;
        }
        else { //NO: ira a notif_sem, pero no a e_notif
          sTeorPrim = sTeor;
          /////// modificacion JLT
          txtNotifTeor.setText(sBD_NOTIF_SEM_TeorPrim);
        }
        msgBox = null;
      } //if
      else {
        sTeorPrim = sBD_NOTIF_SEM_TeorPrim; //sera igual a sTeor
      }
    } //if equipo

//!!NOTIF_SEM!!!!!!
//!!!!!!!!!!!!!!!!!

    String IT_ACTUALIZAR_NOTIF_SEM = "";
    if (Actualizar_Notif_Sem()) {
      IT_ACTUALIZAR_NOTIF_SEM = "S";
    }
    else {
      IT_ACTUALIZAR_NOTIF_SEM = "N";

      //+++++++++++++++++++++++++++++++++++++++++
    }
    MontarLista_Modif(IT_ACTUALIZAR_NOTIF_SEM, sTeor, sTeorPrim);

    if (!ComprobarOpeFc_Numericas_Modif()) {
      ActListaBloqueo(listaReservada, false, ""); //carga la reserva
      //las variables globales primeras no se han tocado
      ActDatosNotif(); //datos primeros a su sitio (A LAS CAJAS)
      return;
    }
    //+++++++++++++++++++++++++++++++++++++++++

    //continuamos
    parametros = new CLista();

    parametros = Cargar_Data_Modif(listaTblDia, sTeorPrim,
                                   IT_ACTUALIZAR_NOTIF_SEM);
    try {
      result = Comunicador.Communicate(getApp(),
                                       stubCliente,
                                       servletUPDATE_EDO_NUMERICA,
                                       strSERVLET_EDO_NUMERICA,
                                       parametros);

      DataNumerica dataFechaActual = (DataNumerica) result.firstElement();

      String sFecha_Actual = dataFechaActual.getFC_ULTACT();

          /*applet.pListaEDONum.RellenaLista((String)hashNotifEDO.get("CD_E_NOTIF"),
                          (String)hashNotifEDO.get("CD_ANOEPI"),
                          (String)hashNotifEDO.get("CD_SEMEPI"),
                          getFechaRecep(),
                          getFechaNotif());//actualiza blistaNumvacia*/

      //NO LLAMA A SERVLET ALGUNO
      applet.pListaEDONum.RellenaLista_Imcompleta( (String) hashNotifEDO.get(
          "CD_E_NOTIF"),
                                                  (String) hashNotifEDO.get(
          "CD_ANOEPI"),
                                                  (String) hashNotifEDO.get(
          "CD_SEMEPI"),
                                                  getFechaRecep(),
                                                  getFechaNotif(),
                                                  modoMODIFICACION);

      ShowWarning(resBun.getString("msg39.Text"));

      /*//control de error al rellenarlistas, no se llama a Servlet
            if (modoOperacion == modoCABECERAYBOTONES){
         return;
            }//**************************************   */

      //si ha ido bien
      ActListaBloqueo(listaReservada, true, sFecha_Actual); //carga lo de BD

      //***suponemos que en este bloque no casca ***  *************
       //ACTUALIZAMOS LOS DATOS que supuestamente recogeriamos de la BD
       //variables globales al Panel, que guardan lo procedente de la BD, que son los de pantalla
       //cargar valores de la pantalla
      sNotifTeorPrim = txtNotifTeor.getText(); //lo que esta en NOTIF_SEM
      sNotifRealesPrim = txtNotifReales.getText(); //lo que esta en NOTIFEDO
      sTotNotifRealesPrim = txtTotNotifReales.getText(); //lo que esta en NOTIF_SEM
      if (chkbxResSem.getState()) {
        IT_RESSEM_PRIM = "S";
      }
      else {
        IT_RESSEM_PRIM = "N";
      }
      ActDatosNotif();
      //***********  *************  ************* *************

       /*//AIC
             DataNumerica datosAlta;
             try
             {
        for(int i = 0; i < parametros.size(); i++)
        {
          datosAlta = (DataNumerica)parametros.elementAt(i);
          miraIndicadores miraInd = new miraIndicadores(getApp());
          //parNotifAlar par = new parNotifAlar(datosAlta.getCD_ENFCIE() , datosAlta.getCD_ANOEPI(),
          //                 datosAlta.getCD_SEMEPI(), "01", "01");
          parNotifAlar par = new parNotifAlar(datosAlta.getCD_ENFCIE() , datosAlta.getCD_ANOEPI(),
           datosAlta.getCD_SEMEPI(), getNivel1(), getNivel2());
          miraInd.compruebaIndSuperados(par);
           ShowWarning(datosAlta.getCD_ENFCIE()+ "  " + datosAlta.getCD_ANOEPI()+ "  " +
           datosAlta.getCD_SEMEPI()+ "  " + getNivel1()+ "  " + getNivel2());
        }
             }
             catch(Exception e)
             {
        ShowWarning(e.toString());
        e.printStackTrace();
             }*/

      //actualizamos valores validacion //ha ido bien
      applet.pListaEDOIndiv.actualizar(modoMODIFICACION);
      applet.pListaEDONum.actualizar(modoMODIFICACION);

    }
    catch (Exception e) {
      ShowWarning(resBun.getString("msg40.Text"));
      ActListaBloqueo(listaReservada, false, ""); //carga la reserva
      ActDatosNotif(); //con los Prim que no se han tocado
      //restaurar valores al inicio de funcion:(suponemos peta en Servlet)
      applet.pListaEDONum.RellenaLista( (String) hashNotifEDO.get("CD_E_NOTIF"),
                                       (String) hashNotifEDO.get("CD_ANOEPI"),
                                       (String) hashNotifEDO.get("CD_SEMEPI"),
                                       getFechaRecep(),
                                       getFechaNotif()); //resetea blistaNumvacia

      //control de error al rellenarlistas
      if (modoOperacion == modoCABECERAYBOTONES) {
        return;
      } //**************************************

    }

  } //FIN DE MODIFICAR NOTIFICACION

  public void quitamos_notifedo_en_alta() {
    //1 notifedo, 2 notif_sem, notifedo, 3 e, notif_sem, notifedo
    if (listaOpeUltactTabla.size() == 1) {
      listaOpeUltactTabla.removeElementAt(0); //quito comprobar notifedo
      return;
    }
    if (listaOpeUltactTabla.size() == 2) {
      listaOpeUltactTabla.removeElementAt(1); //quito comprobar notifedo
      return;
    }
    if (listaOpeUltactTabla.size() == 3) {
      listaOpeUltactTabla.removeElementAt(2); //quito comprobar notifedo
      return;
    }

  }

  public void AltaNotificacion(CLista listaTblDia) {

    DataOpeFc data = new DataOpeFc();
    if (listaOpeUltactTabla.size() < 3) {
      listaOpeUltactTabla.addElement(data); //equivalente a notifedo

      //Reserva de la listaOpeUltactTabla
    }
    CLista listaReservada = new CLista(); //lleva e_notif, notif_sem (puede que a null, si no procede cobertura) y notifedo a null
    DataOpeFc dataProvisional = null;
    for (int i = 0; i < listaOpeUltactTabla.size(); i++) {
      dataProvisional = (DataOpeFc) listaOpeUltactTabla.elementAt(i);
      listaReservada.addElement(dataProvisional);
    } //----------------------------------

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();

    //variables que entran al servlet para cambiar
    String sTeor = "", sTeorPrim = "", sReal = "", sTotReal = "";

    //Almacen para restaurar valores en caso de fallo o responder NO
    //valores iniciales ie al llegar a la funcion VALORES DE PANTALLA
    String sPantallaTeor = getNotifTeorAct();
    String sPantallaReal = getNotifReales();
    String sPantallaTotReal = getTotNotifReales();

    //cargar valores de la pantalla
    sReal = sPantallaReal;
    sTotReal = sPantallaTotReal;
    sTeor = sPantallaTeor;

    //valores de la Base de datos
    String sBD_NOTIF_SEM_TeorPrim = getNotifTeorPrim();
    String sBDReal = sNotifRealesPrim;
    String sBDTotReal = sTotNotifRealesPrim;

//!!E_NOTIF!!!!
//!!!!!!!!!!!!
    if (sCobertura.equals("S") &&
        chkbxResSem.getState() &&
        !NohaCambiadoTeorTotalReales()) { //veo lo de equipo!!!
      //teor antiguo <> teor nuevo  ------------
      if (!getNotifTeorPrim().equals(getNotifTeorAct())) {

        // si no coinciden, se pide confirmaci�n
        CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                       resBun.getString("msg38.Text"));
        msgBox.show();

        if (msgBox.getResponse()) { //SI, sTeorPrim debe ser distinto a sTeor para que el servelt actualice e_notif
          sTeorPrim = sBD_NOTIF_SEM_TeorPrim;
        }
        else { //NO
          sTeorPrim = sTeor;
          /////// modificacion JLT
          txtNotifTeor.setText(sBD_NOTIF_SEM_TeorPrim);
        }
        msgBox = null;
      } //if
      else {
        sTeorPrim = sBD_NOTIF_SEM_TeorPrim; //sera igual a sTeor

      }
    } //if equipo

//NOTIF_SEM!!!!!!
//!!!!!!!!!!!!!!!
    String IT_ACTUALIZAR_NOTIF_SEM = "";
    if (Actualizar_Notif_Sem()) {
      IT_ACTUALIZAR_NOTIF_SEM = "S";
    }
    else {
      IT_ACTUALIZAR_NOTIF_SEM = "N";

      //+++++++++++++++++++++++++++++++++++++++++
    }
    MontarLista_Modif(IT_ACTUALIZAR_NOTIF_SEM, sTeor, sTeorPrim);
    quitamos_notifedo_en_alta();

    if (!ComprobarOpeFc_Numericas_Alta()) {
      ActListaBloqueo(listaReservada, false, ""); //carga la reserva
      ActDatosNotif(); //datos primeros a su sitio
      modoOperacion = modo;
      Inicializar();
      return;
    }
    //+++++++++++++++++++++++++++++++++++++++++

    //continuamos
    parametros = new CLista();
    parametros = Cargar_Data_Alta(listaTblDia,
                                  sTeorPrim,
                                  IT_ACTUALIZAR_NOTIF_SEM);

    try {

      /*
       * JRM quita doDebug que permanec�a, posiblemente est�
       *      en el cliente con este fallo
       */

      // obtiene la lista
      //SrvNumerica servlet = new SrvNumerica ();
      //Indica como conectarse a la b.datos
      //servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
      //               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
      //                         "sive_desa",
      //               "sive_desa");

      //result = (CLista) servlet.doDebug(servletINSERT_EDO_NUMERICA, parametros);

      result = Comunicador.Communicate(getApp(),
                                       stubCliente,
                                       servletINSERT_EDO_NUMERICA,
                                       strSERVLET_EDO_NUMERICA,
                                       parametros);

      DataNumerica dataFechaActual = (DataNumerica) result.firstElement();
      String sFecha_Actual = dataFechaActual.getFC_ULTACT();

          /*applet.pListaEDONum.RellenaLista((String)hashNotifEDO.get("CD_E_NOTIF"),
                          (String)hashNotifEDO.get("CD_ANOEPI"),
                          (String)hashNotifEDO.get("CD_SEMEPI"),
                          getFechaRecep(),
                          getFechaNotif());//actualiza bListaNumvacia */

      //solo llama al servlet de SEMANA
      applet.pListaEDONum.RellenaLista_Imcompleta( (String) hashNotifEDO.get(
          "CD_E_NOTIF"),
                                                  (String) hashNotifEDO.get(
          "CD_ANOEPI"),
                                                  (String) hashNotifEDO.get(
          "CD_SEMEPI"),
                                                  getFechaRecep(),
                                                  getFechaNotif(), modoALTA);
      // JRM2 Para que no se muestre el mensaje de Alta Efectuada
      //ShowWarning(resBun.getString("msg41.Text"));

      //control de error al rellenarlistas (se llama al servlet de SEMANA)
      if (modoOperacion == modoCABECERAYBOTONES) {
        return;
      } //**************************************

      modo = modoMODIFICACION; //CAMBIO
      bAltaNotif = true;

      //!!//si ha ido bien llenar la lista con  los 3, como en Modificacion
      ActListaBloqueo(listaReservada, sFecha_Actual);
      ////////////////////////////////////////////////////////////////////

      //***suponemos que en este bloque no casca ***  *************
       //ACTUALIZAMOS LOS DATOS que supuestamente recogeriamos de la BD
       //variables globales al Panel, que guardan lo procedente de la BD, que son los de pantalla
       //cargar valores de la pantalla
      sNotifTeorPrim = txtNotifTeor.getText(); //lo que esta en NOTIF_SEM
      sNotifRealesPrim = txtNotifReales.getText(); //lo que esta en NOTIFEDO
      sTotNotifRealesPrim = txtTotNotifReales.getText(); //lo que esta en NOTIF_SEM
      if (chkbxResSem.getState()) {
        IT_RESSEM_PRIM = "S";
      }
      else {
        IT_RESSEM_PRIM = "N";
      }
      ActDatosNotif();
      //***********  *************  ************* *************

       /*//AIC
             DataNumerica datosAlta;
             try
             {
        for(int i = 0; i < parametros.size(); i++)
        {
          datosAlta = (DataNumerica)parametros.elementAt(i);
          miraIndicadores miraInd = new miraIndicadores(getApp());
          //parNotifAlar par = new parNotifAlar(datosAlta.getCD_ENFCIE() , datosAlta.getCD_ANOEPI(),
          //                 datosAlta.getCD_SEMEPI(), "01", "01");
          parNotifAlar par = new parNotifAlar(datosAlta.getCD_ENFCIE() , datosAlta.getCD_ANOEPI(),
           datosAlta.getCD_SEMEPI(), getNivel1(), getNivel2());
          miraInd.compruebaIndSuperados(par);
           ShowWarning(datosAlta.getCD_ENFCIE()+ "  " + datosAlta.getCD_ANOEPI()+ "  " +
           datosAlta.getCD_SEMEPI()+ "  " + getNivel1()+ "  " + getNivel2());
        }
             }
             catch(Exception e)
             {
        ShowWarning(e.toString());
        e.printStackTrace();
             } */

      //actualizamos valores validacion //ha ido bien
      applet.pListaEDOIndiv.actualizar(modoALTA);
      applet.pListaEDONum.actualizar(modoALTA);

    }
    catch (Exception e) {
      ShowWarning(resBun.getString("msg42.Text"));
      ActListaBloqueo(listaReservada, false, ""); //carga la reserva
      ActDatosNotif();
      modo = modoALTA; //resetea el estado de check y cobertura
      //dentro de Inicializar(alta) hemos metido actDatosNotif y metemos el check
      bAltaNotif = false;

      //vaciar la lista
      applet.pListaEDONum.Vaciar();
      applet.pListaEDOIndiv.Vaciar();
    }

    modoOperacion = modo;
    Inicializar();

  } //FIN DE alta

//true si bien
//false si mal y se debe hacer return en MODIFICAR NOTIFICACION
  public boolean ComprobarOpeFc_Numericas_Modif() {

    String sm = "";

    //llamada al servlet de bloqueo
    CLista listaVuelta = new CLista();
    try {

      listaVuelta = Comunicador.Communicate(getApp(),
                                            stubCliente,
                                            servletBLOQUEO_SELECT, //modo
                                            strSERVLET_BLOQUEO, //servlet
                                            listaOpeUltactTabla);

//SrvBloqueo srv = new SrvBloqueo();
//listaVuelta = srv.doPrueba(servletBLOQUEO_SELECT, listaOpeUltactTabla);
//srv = null;

    }
    catch (Exception e) {
      ShowWarning(resBun.getString("msg43.Text"));
      //restaurar valores al inicio de funcion: en la que la llama
      return (false);
    }

    //listaVuelta debe tener tama�o 1//
    DataOpeFc dataVuelta = null;
    for (int i = 0; i < listaVuelta.size(); i++) {
      dataVuelta = (DataOpeFc) listaVuelta.elementAt(i);
    } //---va a ser un solo dato

    if (!dataVuelta.getOK_KO()) { //false, ie EXISTE CAMBIO

      if (dataVuelta.getBORRADA()) { //borrada

        //se trata de NOTIFEDO
        if (dataVuelta.getTABLA().trim().equals("SIVE_NOTIFEDO")) {

          sm = resBun.getString("msg44.Text");

          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();

          Inicializar(modoCABECERAYBOTONES);
          msgBox = null;

          return (false);
        }
        //se trata de otra tabla
        else {

          sm = resBun.getString("msg45.Text") + dataVuelta.getTABLA() +
              resBun.getString("msg46.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          applet.setTitulo(resBun.getString("applet.Titulo"));
          applet.VerEntradaEDO();
          msgBox = null;
          return (false);
        }
      }
      else { //Cambiada pero no borrada
        sm = resBun.getString("msg47.Text") +
            dataVuelta.getTABLA() + ")" +
            resBun.getString("msg48.Text");
        msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
        msgBox.show();

        //SI //continuar

        if (!msgBox.getResponse()) { //NO
          //restaurar valores al inicio de funcion:
          //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
          msgBox = null;
          return (false);
        }

      } //Cambiada pero no borrada

    } //EXISTE CAMBIO

    return (true);
  } //fin de ComprobarOpeFc

//true si bien
//false si mal y se debe hacer return en ALTA NOTIFICACION
  public boolean ComprobarOpeFc_Numericas_Alta() {

    String sm = "";

    if (listaOpeUltactTabla.size() == 0) {
      return (true);
    }

    //llamada al servlet de bloqueo
    CLista listaVuelta = new CLista();
    try {

      listaVuelta = Comunicador.Communicate(getApp(),
                                            stubCliente,
                                            servletBLOQUEO_SELECT, //modo
                                            strSERVLET_BLOQUEO, //servlet
                                            listaOpeUltactTabla);

    }
    catch (Exception e) {
      ShowWarning(resBun.getString("msg43.Text"));
      //restaurar valores al inicio de funcion: en la que la llama
      return (false);
    }

    //listaVuelta debe tener tama�o 1//
    DataOpeFc dataVuelta = null;
    for (int i = 0; i < listaVuelta.size(); i++) {
      dataVuelta = (DataOpeFc) listaVuelta.elementAt(i);
    } //---va a ser un solo dato

    if (!dataVuelta.getOK_KO()) { //false, ie EXISTE CAMBIO

      if (dataVuelta.getBORRADA()) { //borrada

        sm = resBun.getString("msg45.Text") + dataVuelta.getTABLA() +
            resBun.getString("msg49.Text");
        CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
        msgBox.show();
        applet.setTitulo(resBun.getString("applet.Titulo"));
        applet.VerEntradaEDO();
        msgBox = null;
        return (false);

      }
      else { //Cambiada pero no borrada
        sm = resBun.getString("msg47.Text") +
            dataVuelta.getTABLA() + ")" +
            resBun.getString("msg50.Text");
        msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
        msgBox.show();

        //SI //continuar

        if (!msgBox.getResponse()) { //NO
          //restaurar valores al inicio de funcion:
          //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
          msgBox = null;
          return (false);
        }

      } //Cambiada pero no borrada

    } //EXISTE CAMBIO

    return (true);
  } //fin de ComprobarOpeFc

//true si bien
//false si mal y se debe hacer return en BORRADO MAXIVO
  public boolean ComprobarOpeFc_borrar_Maxivo() {

    String sm = "";

    //llamada al servlet de bloqueo
    CLista listaVuelta = new CLista();
    try {

      listaVuelta = Comunicador.Communicate(getApp(),
                                            stubCliente,
                                            servletBLOQUEO_SELECT, //modo
                                            strSERVLET_BLOQUEO, //servlet
                                            listaOpeUltactTabla);

    }
    catch (Exception e) {
      ShowWarning(resBun.getString("msg43.Text"));
      //restaurar valores al inicio de funcion:
      //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
      return (false);
    }

    //listaVuelta debe tener tama�o 1//
    DataOpeFc dataVuelta = null;
    for (int i = 0; i < listaVuelta.size(); i++) {
      dataVuelta = (DataOpeFc) listaVuelta.elementAt(i);
    } //---va a ser un solo dato

    if (!dataVuelta.getOK_KO()) { //false, ie EXISTE CAMBIO

      if (dataVuelta.getBORRADA()) { //borrada
        //se trata de NOTIFEDO
        if (dataVuelta.getTABLA().trim().equals("SIVE_NOTIF_SEM")) {
          sm = resBun.getString("msg51.Text");
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, sm);
          msgBox.show();
          //NO HACEMOS NADA DE MOMENTO
          msgBox = null;
          return (false);
        }

      }
      else { //Cambiada pero no borrada
        sm = resBun.getString("msg47.Text") +
            dataVuelta.getTABLA() + ")" +
            resBun.getString("msg52.Text");
        msgBox = new CMessage(this.app, CMessage.msgPREGUNTA, sm);
        msgBox.show();

        //SI //continuar

        if (!msgBox.getResponse()) { //NO
          //restaurar valores al inicio de funcion:
          //en la pantalla siguen los mismos y las variables que guardan lo procedente de BD, no se han cambiado
          msgBox = null;
          return (false);
        }

      } //Cambiada pero no borrada

    } //EXISTE CAMBIO

    return (true);
  } //fin de ComprobarOpeFc

//especial para alta (se va a actualizar, no a tomar de reserva)
  public void ActListaBloqueo(CLista listaReserva, String sFechaInsertada) {
    int iTamLista = 0;
    iTamLista = listaOpeUltactTabla.size();

    //ACTUALIZAMOS listaOpeUltactTabla con los datos que hay en la tabla!!!
    //para que PUEDA seguir MODIFICANDO
    DataOpeFc dataReservaEquipo = null;
    DataOpeFc dataReservaNotifSem = null;
    DataOpeFc dataReservaNotifedo = null;

    dataReservaEquipo = (DataOpeFc) listaReserva.elementAt(0);
    dataReservaNotifSem = (DataOpeFc) listaReserva.elementAt(1);
    dataReservaNotifedo = (DataOpeFc) listaReserva.elementAt(2);

    //que hay en listaOpeUltactTabla????
    //0: respetar e_notif, respetar notif_sem, nuevo notifedo
    //1: respetar e_notif, nuevo notif_sem, nuevo notifedo
    //2: nuevo e_notif, nuevo notif_sem, nuevo notifedo

    if (iTamLista == 0) { //respetar e_notif, respetar notif_sem, nuevo notifedo
      listaOpeUltactTabla = new CLista();
      //primer data, e_notif ANTIGUO
      listaOpeUltactTabla.addElement(dataReservaEquipo);
      //primer data, notif_sem ANTIGUO
      listaOpeUltactTabla.addElement(dataReservaNotifSem);
      //NUEVA NOTIFEDO
      DataOpeFc datosnotifedo;
      Vector vnotifedo = new Vector();
      DataOpeFc datavnotifedo = null;
      datavnotifedo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("FC_RECEP", CfechaRecep.getText(), "F");
      vnotifedo.addElement(datavnotifedo);
      // Para Madrid (ARS 26-03-01)
      if (sLaComunidad.equals("M")) {
        datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaNotif.getText(), "F");
      }
      else {
        datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaFinNotif.getText(),
                                      "F");
      }
      vnotifedo.addElement(datavnotifedo);

      datosnotifedo = new DataOpeFc(this.getApp().getLogin(),
                                    sFechaInsertada,
                                    "SIVE_NOTIFEDO", vnotifedo);

      listaOpeUltactTabla.addElement(datosnotifedo);

    } //fin 0
    if (iTamLista == 1) { //respetar e_notif, nuevo notif_sem, nuevo notifedo
      listaOpeUltactTabla = new CLista();
      //primer data, e_notif ANTIGUO
      listaOpeUltactTabla.addElement(dataReservaEquipo);
      //notif_sem NUEVO
      DataOpeFc datosnotifsem;
      Vector vnotifsem = new Vector();
      DataOpeFc datavnotifsem = null;
      datavnotifsem = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
      vnotifsem.addElement(datavnotifsem);
      datavnotifsem = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
      vnotifsem.addElement(datavnotifsem);
      datavnotifsem = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
      vnotifsem.addElement(datavnotifsem);

      datosnotifsem = new DataOpeFc(this.getApp().getLogin(),
                                    sFechaInsertada,
                                    "SIVE_NOTIF_SEM", vnotifsem);

      listaOpeUltactTabla.addElement(datosnotifsem);

      //NUEVA NOTIFEDO
      DataOpeFc datosnotifedo;
      Vector vnotifedo = new Vector();
      DataOpeFc datavnotifedo = null;
      datavnotifedo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("FC_RECEP", CfechaRecep.getText(), "F");
      vnotifedo.addElement(datavnotifedo);
      if (sLaComunidad.equals("M")) {
        datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaNotif.getText(), "F");
      }
      else {
        datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaFinNotif.getText(),
                                      "F");
      }
      vnotifedo.addElement(datavnotifedo);

      datosnotifedo = new DataOpeFc(this.getApp().getLogin(),
                                    sFechaInsertada,
                                    "SIVE_NOTIFEDO", vnotifedo);

      listaOpeUltactTabla.addElement(datosnotifedo);
    } //fin 1
    if (iTamLista == 2) { //nuevo e_notif, nuevo notif_sem, nuevo notifedo
      listaOpeUltactTabla = new CLista();
      //primer data, e_notif NUEVO
      DataOpeFc datosequipo;
      Vector vEquipo = new Vector();
      DataOpeFc datavEquipo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
      vEquipo.addElement(datavEquipo);

      datosequipo = new DataOpeFc(this.getApp().getLogin(),
                                  sFechaInsertada,
                                  "SIVE_E_NOTIF", vEquipo);

      listaOpeUltactTabla.addElement(datosequipo);

      //notif_sem NUEVO
      DataOpeFc datosnotifsem;
      Vector vnotifsem = new Vector();
      DataOpeFc datavnotifsem = null;
      datavnotifsem = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
      vnotifsem.addElement(datavnotifsem);
      datavnotifsem = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
      vnotifsem.addElement(datavnotifsem);
      datavnotifsem = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
      vnotifsem.addElement(datavnotifsem);

      datosnotifsem = new DataOpeFc(this.getApp().getLogin(),
                                    sFechaInsertada,
                                    "SIVE_NOTIF_SEM", vnotifsem);

      listaOpeUltactTabla.addElement(datosnotifsem);

      //NUEVA NOTIFEDO
      DataOpeFc datosnotifedo;
      Vector vnotifedo = new Vector();
      DataOpeFc datavnotifedo = null;
      datavnotifedo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
      vnotifedo.addElement(datavnotifedo);
      datavnotifedo = new DataOpeFc("FC_RECEP", CfechaRecep.getText(), "F");
      vnotifedo.addElement(datavnotifedo);
      if (sLaComunidad.equals("M")) {
        datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaNotif.getText(), "F");
      }
      else {
        datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaFinNotif.getText(),
                                      "F");
      }
      vnotifedo.addElement(datavnotifedo);

      datosnotifedo = new DataOpeFc(this.getApp().getLogin(),
                                    sFechaInsertada,
                                    "SIVE_NOTIFEDO", vnotifedo);

      listaOpeUltactTabla.addElement(datosnotifedo);
    } //fin 2
  }

  public void ActListaBloqueo(CLista listaReserva, boolean bActualizar,
                              String sFechaInsertada) {
    int iTamLista = 0;
    iTamLista = listaOpeUltactTabla.size();

    //ACTUALIZAMOS listaOpeUltactTabla con los datos que hay en la tabla!!!
    //para que PUEDA seguir MODIFICANDO
    DataOpeFc dataReservaEquipo = null;
    DataOpeFc dataReservaNotifSem = null;
    DataOpeFc dataReservaNotifedo = null;

    dataReservaEquipo = (DataOpeFc) listaReserva.elementAt(0);
    dataReservaNotifSem = (DataOpeFc) listaReserva.elementAt(1);
    dataReservaNotifedo = (DataOpeFc) listaReserva.elementAt(2);

    if (!bActualizar) { //volver a valores antiguos
      listaOpeUltactTabla = new CLista();
      listaOpeUltactTabla.addElement(dataReservaEquipo);
      listaOpeUltactTabla.addElement(dataReservaNotifSem);
      listaOpeUltactTabla.addElement(dataReservaNotifedo);
    }
    else {
      if (iTamLista == 1) { //modifique solo notifedo
        listaOpeUltactTabla = new CLista();
        //primer data, e_notif ANTIGUO
        listaOpeUltactTabla.addElement(dataReservaEquipo);
        //primer data, notif_sem ANTIGUO
        listaOpeUltactTabla.addElement(dataReservaNotifSem);
        //tercer data, notifedo NUEVO
        DataOpeFc datosnotifedo;
        Vector vnotifedo = new Vector();
        DataOpeFc datavnotifedo = null;
        datavnotifedo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("FC_RECEP", CfechaRecep.getText(), "F");
        vnotifedo.addElement(datavnotifedo);
        if (sLaComunidad.equals("M")) {
          datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaNotif.getText(),
                                        "F");
        }
        else {
          datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaFinNotif.getText(),
                                        "F");
        }
        vnotifedo.addElement(datavnotifedo);

        datosnotifedo = new DataOpeFc(this.getApp().getLogin(),
                                      sFechaInsertada,
                                      "SIVE_NOTIFEDO", vnotifedo);

        listaOpeUltactTabla.addElement(datosnotifedo);
      }
      else if (iTamLista == 2) { //modifique solo notif_sem - notifedo
        listaOpeUltactTabla = new CLista();
        //primer data, e_notif ANTIGUO
        listaOpeUltactTabla.addElement(dataReservaEquipo);

        //segundo data, notif_sem NUEVO
        DataOpeFc datosnotifsem;
        Vector vnotifsem = new Vector();
        DataOpeFc datavnotifsem = null;
        datavnotifsem = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
        vnotifsem.addElement(datavnotifsem);
        datavnotifsem = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
        vnotifsem.addElement(datavnotifsem);
        datavnotifsem = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
        vnotifsem.addElement(datavnotifsem);

        datosnotifsem = new DataOpeFc(this.getApp().getLogin(),
                                      sFechaInsertada,
                                      "SIVE_NOTIF_SEM", vnotifsem);

        listaOpeUltactTabla.addElement(datosnotifsem);

        //tercer data, notifedo NUEVO
        DataOpeFc datosnotifedo;
        Vector vnotifedo = new Vector();
        DataOpeFc datavnotifedo = null;
        datavnotifedo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("FC_RECEP", CfechaRecep.getText(), "F");
        vnotifedo.addElement(datavnotifedo);
        // ARS para Madrid
        if (sLaComunidad.equals("M")) {
          datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaNotif.getText(),
                                        "F");
        }
        else {
          datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaFinNotif.getText(),
                                        "F");
        }
        vnotifedo.addElement(datavnotifedo);

        datosnotifedo = new DataOpeFc(this.getApp().getLogin(),
                                      sFechaInsertada,
                                      "SIVE_NOTIFEDO", vnotifedo);

        listaOpeUltactTabla.addElement(datosnotifedo);

      }

      else if (iTamLista == 3) { //modifico e_notif + notif_sem + notifedo
        listaOpeUltactTabla = new CLista();
        //primer data, e_notif NUEVO
        DataOpeFc datosequipo;
        Vector vEquipo = new Vector();
        DataOpeFc datavEquipo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
        vEquipo.addElement(datavEquipo);

        datosequipo = new DataOpeFc(this.getApp().getLogin(),
                                    sFechaInsertada,
                                    "SIVE_E_NOTIF", vEquipo);

        listaOpeUltactTabla.addElement(datosequipo);
        //segundo data, notif_sem NUEVO
        DataOpeFc datosnotifsem;
        Vector vnotifsem = new Vector();
        DataOpeFc datavnotifsem = null;
        datavnotifsem = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
        vnotifsem.addElement(datavnotifsem);
        datavnotifsem = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
        vnotifsem.addElement(datavnotifsem);
        datavnotifsem = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
        vnotifsem.addElement(datavnotifsem);

        datosnotifsem = new DataOpeFc(this.getApp().getLogin(),
                                      sFechaInsertada,
                                      "SIVE_NOTIF_SEM", vnotifsem);

        listaOpeUltactTabla.addElement(datosnotifsem);
        //tercer data, notifedo NUEVO
        DataOpeFc datosnotifedo;
        Vector vnotifedo = new Vector();
        DataOpeFc datavnotifedo = null;
        datavnotifedo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
        vnotifedo.addElement(datavnotifedo);
        datavnotifedo = new DataOpeFc("FC_RECEP", CfechaRecep.getText(), "F");
        vnotifedo.addElement(datavnotifedo);
        if (sLaComunidad.equals("M")) {
          datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaNotif.getText(),
                                        "F");
        }
        else {
          datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaFinNotif.getText(),
                                        "F");
        }
        vnotifedo.addElement(datavnotifedo);

        datosnotifedo = new DataOpeFc(this.getApp().getLogin(),
                                      sFechaInsertada,
                                      "SIVE_NOTIFEDO", vnotifedo);

        listaOpeUltactTabla.addElement(datosnotifedo);

      }
    }
  } //fin ActListaBloqueo

// actualiza el campo cobertura con los notif. te�ricos y total reales
  void ActCobertura() {

    if (sCobertura.trim().equals("S")) {
      int iNotifTeor = 0, iTotNotifReales = 0;
      //String sCob;
      try {
        if (!txtNotifTeor.getText().trim().equals("")) {
          iNotifTeor = (new Integer(txtNotifTeor.getText().trim())).intValue();
        }
        if (!txtTotNotifReales.getText().trim().equals("")) {
          iTotNotifReales = (new Integer(txtTotNotifReales.getText().trim())).
              intValue();
          //if (( iNotifTeor > 0) && ( iTotNotifReales > 0 ))

          // Cobertura = (total_reales / te�ricos) * 100
        }
        if (iNotifTeor == 0) {
          txtCobertura.setText("0 %");
        }
        else {
          txtCobertura.setText( (new Integer(100 * iTotNotifReales / iNotifTeor)).
                               toString() + " %");
        }

      }
      catch (Exception e) {
        txtCobertura.setText("");
      }
    }
  }

// m�todo que recupera de una estructura DataEntradaEDO
// los datos de la cabecera
// Se actualizan tambi�n en la Hashtable hashNotifEDO
  private void ActDatosCabecera(DataEntradaEDO data) {

    /* mlm: rellenar:  sNotifTeorPrim,  sNotifTeorAnt,
                  sTotNotifRealesPrim, sTotNotifRealesAnt */

    // se recuperan los datos
    sCodEquipo = data.getCod().trim();
    sDesEquipo = data.getDes().trim();

    //el backup ES EL PRIMERO!!!! (en nivel 1 va nm_notift de notif_sem -mlm-)
    //nota en Nueva, puede no tener el registro en notif_sem generado!!!!
    if (data.getNivel1() != null && !data.getNivel1().trim().equals("")) {
      sNotifTeorPrim = data.getNivel1().trim();
      sNotifTeorAnt = sNotifTeorPrim;
    }

    String sCodNivel2Eq = data.getNivel2().trim();
    sCodCentro = data.getCodCentro().trim();
    sDesCentro = data.getDesCentro().trim();
    sCobertura = data.getDato7().trim();

    //totalReales de notif_sem -mlm-
    //nota en Nueva, puede no tener el registro en notif_sem generado!!!!
    if (data.getDato8() != null && !data.getDato8().trim().equals("")) {
      sTotNotifRealesPrim = data.getDato8().trim();
      sTotNotifRealesAnt = sTotNotifRealesPrim;
    }

    //cobertura
    if (sCobertura.equals("S")) {
      lResSem.setVisible(true);
      chkbxResSem.setVisible(true);
    }
    else {
      lResSem.setVisible(false);
      chkbxResSem.setVisible(false);
    }

    // se insertan en los campos correspondientes
    txtCodEquipo.setText(sCodEquipo);
    sCodEquipoIni = sCodEquipo;
    txtDesEquipo.setText(sDesEquipo);
    txtCentro.setText(sCodCentro + "     " + sDesCentro);

    // se actualiza hashNotifEDO con los valores anteriores
    hashNotifEDO.put("CD_E_NOTIF", sCodEquipo);
    hashNotifEDO.put("CD_ANOEPI", this.getAnno());
    hashNotifEDO.put("CD_SEMEPI", this.getSemana());
    hashNotifEDO.put("DS_E_NOTIF", sDesEquipo);
    hashNotifEDO.put("CD_NIVEL_2_EQ", sCodNivel2Eq);
    hashNotifEDO.put("CD_ZBS_EQ", "");
    hashNotifEDO.put("CD_C_NOTIF", sCodCentro);
    hashNotifEDO.put("DS_C_NOTIF", sDesCentro);
    hashNotifEDO.put("IT_COBERTURA", sCobertura);
    hashNotifEDO.put("FC_FECNOTIF", getFechaNotif());
    hashNotifEDO.put("FC_RECEP", getFechaRecep());

    //cargar listaopeultacttabla
    DataOpeFc datosequipo;
    Vector vEquipo = new Vector();
    DataOpeFc datavEquipo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
    vEquipo.addElement(datavEquipo);

    datosequipo = new DataOpeFc(data.getCD_OPE(),
                                data.getFC_ULTACT(), //HH:mm:SS
                                "SIVE_E_NOTIF", vEquipo);

    listaOpeUltactTabla = new CLista();
    listaOpeUltactTabla.addElement(datosequipo);
    //-------------------------
  }

  // fija el dato sFecha en el campo CFecha
//AIC
  // ARG: En vez de usar CFechaSimple usamos CFecha
  //private void ActCFecha(CFechaSimple CFecha, String sFecha) {
  private void ActCFecha(CFecha CFechaAct, String sFecha) {
    CFechaAct.setText(sFecha);
    CFechaAct.ValidarFecha();
    CFechaAct.setText(CFechaAct.getFecha());
  } // fin m�todo ActCFecha

  private void ActCFecha(CFechaSimple CFechaFin, CFechaSimple CFechaIni,
                         String sFecha) {
    CFechaFin.setText(sFecha);
    CFechaFin.ValidarFecha();
    CFechaFin.setText(CFechaFin.getFecha());

    CFechaIni.setText(CFechaFin.annadirDias( -5));

  } // fin m�todo ActCFecha

  private void ActCFecha(CFechaSimple CFechaFin, CFechaSimple CFechaIni,
                         String sFechaAct, String sFechaReal) {
    CFechaFin.setText(sFechaReal);
    CFechaFin.ValidarFecha();
    CFechaFin.setText(CFechaFin.getFecha());

    CFechaIni.setText(CFechaFin.annadirDias( -5));

    CFechaFin.setText(sFechaAct);
    CFechaFin.ValidarFecha();
    CFechaFin.setText(CFechaFin.getFecha());

  } // fin m�todo ActCFecha

// inicia los datos correspondientes a la notificaci�n:
//    notificadores te�ricos, reales y total reales
//    llama al c�lculo de la cobertura
  private void ActDatosNotif() {

    txtNotifTeor.setText(sNotifTeorPrim);
    // iniciamos los anteriores
    sTeorAntes = sNotifTeorPrim;
    txtNotifReales.setText(sNotifRealesPrim);
    sRealesAntes = sNotifRealesPrim;
    txtTotNotifReales.setText(sTotNotifRealesPrim);
    ActCobertura();
  } // fin m�todo ActDatosNotif()

// actualiza los datos correspondientes a la notificaci�n a partir
// de los datos contenidos en data (notificaci�n seleccionada):
//    fecha de notificaci�n, resumen semanal, fecha de recepci�n,
//    notificadores te�ricos, reales y total reales
//    llama al c�lculo de la cobertura
  private void ActDatosNotif(DataListaNotifEDO data) {
    String sNotifReales = "0", sNotifTeor = "0", sTotNotifReales = "0";

    try {

      if (sLaComunidad.equals("M")) {
        ActCFecha(CfechaNotif, data.getFechaNotif());
      }
      else {
        ActCFecha(CfechaFinNotif, CfechaIniNotif, data.getFechaNotif());
      }
      ActCFecha(CfechaRecep, data.getFechaRecep());

      hashNotifEDO.put("FC_FECNOTIF", data.getFechaNotif());
      hashNotifEDO.put("FC_RECEP", data.getFechaRecep());

      // si e st� en cobertura, podr� ser resumen semanal o no
      // se recupera el n�merod e notificadores reales
      if (sCobertura.equals("S")) {
        lResSem.setVisible(true);
        chkbxResSem.setVisible(true);

        // notificadores te�ricos
        if (data.getNotifTeor() != null) {
          sNotifTeor = data.getNotifTeor();
        }
        txtNotifTeor.setText(sNotifTeor);
        sNotifTeorPrim = sNotifTeor;
        sNotifTeorAnt = sNotifTeor;

        // total notificadores reales
        if (data.getTotNotifReales() != null) {
          sTotNotifReales = data.getTotNotifReales();
        }
        txtTotNotifReales.setText(sTotNotifReales);
        sTotNotifRealesPrim = sTotNotifReales;
        sTotNotifRealesAnt = sTotNotifReales;

        if (!sTotNotifRealesPrim.equals("")) {
          iTotNotifRealesAnt = (new Integer(sTotNotifRealesPrim)).intValue();

          // notificadores reales
        }
        if (data.getNotifReales() != null) {
          sNotifReales = data.getNotifReales();
        }
        txtNotifReales.setText(sNotifReales);
        sNotifRealesPrim = sNotifReales;
        sNotifRealesAnt = sNotifReales;

        // inicia los anteriores
        sTeorAntes = sNotifTeorPrim;
        sRealesAntes = sNotifRealesPrim;

        if (!sNotifRealesPrim.equals("")) {
          iNotifRealesAnt = (new Integer(sNotifRealesPrim)).intValue();

        }
        ActCobertura();

        // si es resumen semanal, se marca el checkbox
        if (data.getResSem().equals("S")) {
          chkbxResSem.setState(true);
          IT_RESSEM_PRIM = "S";
          showControls(true);
        }
        else {
          chkbxResSem.setState(false);
          IT_RESSEM_PRIM = "N";
          showControls(false);
        }
      }
      // si no est� en cobertura, se oculta
      else {
        lResSem.setVisible(false);
        chkbxResSem.setVisible(false);
      }

      //respecto Validacion
      applet.pListaEDONum.Cargar_Validar(data);
      applet.pListaEDOIndiv.Cargar_Validar(data);

    }
    catch (Exception e) {
      ;
    }
  } // fin m�todo ActDatosNotif(...)

  /**
   * Determina si hay zona b�sica de salud en el distrito
   */
  boolean hayZonaBasicaSalud() {
    // Si hay zona b�sica de salud la longitud de la cadena de entrada es de
    // 4 caracteres: XXYY siendo XX el distrito e YY la ZBS
    return (sCodNivel2.length() == 4);
  }

// se ejecuta en respuesta al actionPerformed en btnEquipo
// llama a CListaEquipos para permitir la selecci�n de un equipo
// por c�digo, o por descripci�n
  void btnEquipo_actionPerformed() {
    //JRM2  Modifica para que solo entre en modo CONSULTA cuando el notificador
    // est� de Baja
    DataEntradaEDO data;
    int modo = modoOperacion;
    boolean hayZBS = hayZonaBasicaSalud();
    String fechaNotifEquipo = getFechaNotifEquipo();

    CListaEquipos lista;

    modoOperacion = modoESPERA;
    Inicializar();

    // Arreglado para Madrid 26-03-01
//    if (sLaComunidad.equals("M")) {
    // Para Madrid.

    if (!hayZBS) {
      lista = new CListaEquipos(this,
                                resBun.getString("msg53.Text"),
                                stubCliente,
                                strSERVLET_MAESTRO,
                                this.getApp().getParameter("ORIGEN"),
                                servletOBTENER_EQUIPO_X_CODIGO,
                                servletOBTENER_EQUIPO_X_DESCRIPCION,
                                servletSELECCION_EQUIPO_X_CODIGO,
                                servletSELECCION_EQUIPO_X_DESCRIPCION,
                                fechaNotifEquipo);
    }
    else {
      lista = new CListaEquipos(this,
                                resBun.getString("msg53.Text"),
                                stubCliente,
                                strSERVLET_MAESTRO,
                                this.getApp().getParameter("ORIGEN"),
                                servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS,
                                servletOBTENER_EQUIPO_X_DESCRIPCION_CON_ZBS,
                                servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS,
                                servletSELECCION_EQUIPO_X_DESCRIPCION_CON_ZBS,
                                fechaNotifEquipo);
      /*
          } else {
            // Para otras comunidades.
            if (!hayZBS)
                 lista = new CListaEquipos(this,
                                         resBun.getString("msg53.Text"),
                                         stubCliente,
           strSERVLET_MAESTRO, //JRM2 CAMBIA por *_NOTIF
                                         this.getFechaNotif(),
                                         servletOBTENER_EQUIPO_X_CODIGO_NOTIF,
           servletOBTENER_EQUIPO_X_DESCRIPCION_NOTIF,
           servletSELECCION_EQUIPO_X_CODIGO_NOTIF,
           servletSELECCION_EQUIPO_X_DESCRIPCION_NOTIF,
                                         fechaNotifEquipo);
            else
                lista = new CListaEquipos(this,
                                        resBun.getString("msg53.Text"),
                                        stubCliente,
                                        strSERVLET_MAESTRO,
                                        this.getFechaNotif(),
                                        servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS,
           servletOBTENER_EQUIPO_X_DESCRIPCION_CON_ZBS,
           servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS,
           servletSELECCION_EQUIPO_X_DESCRIPCION_CON_ZBS,
                                        fechaNotifEquipo);
          }
       */

    }
    lista.show();
    data = (DataEntradaEDO) lista.getComponente();

    if (data != null) {
      //NUEVO  mlm
      hashNotifEDO.put("CD_NIVEL_1", sCodNivel1);
      //AIC
      //hashNotifEDO.put("CD_NIVEL_2", data.getNivel2().trim());
      hashNotifEDO.put("CD_NIVEL_2", sCodNivel2);

      // se actualizan los datos de la cabecera
      //mlm: los datos de teoricos y totalreales de notif_sem,
      //  pueden estar vacios, si la notificacion NO se ha dado y Generado Cobertura
      ActDatosCabecera(data);
      modo = modoBOTONES;

      // Creamos el objeto con los datos de los notificadores.
      equiposNotificadores = new ENotificadores(lista.getLista(),
                                                txtCodEquipo.getText());
      //JRM2
//       // System_out.println ("Modo : " + modoOperacion );
      //// System_out.println ("Descripcion : " + data.getDes() );
      int longitudDes = data.getDes().length();
      //// System_out.println ("Longitud : " + longitudDes );
      //// System_out.println ("El ultimo caracter es :" + data.getDes().charAt (longitudDes -1) );

          /* // ARG: Ahora el servlet es el que filtra los notificadores dados de baja
              if (longitudDes > 2 )
              {
        if (data.getDes().charAt (longitudDes -1) == ')' &&
            data.getDes().charAt (longitudDes -2) == 'B' &&
            data.getDes().charAt (longitudDes -3) == '(' )
        {
           // ARG: Se comprueba que la fecha de notificacion este entre la
           //      fecha de alta y la de baja
           java.util.Date dFechaNotif;
           java.util.Date dFechaAlta;
           java.util.Date dFechaBaja;
           SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
           try
           {
              CfechaIniNotif.ValidarFecha();
              // Fecha de Notificaci�n
              String fechaNotif = this.getFechaNotif();
              dFechaNotif = formater.parse(fechaNotif);
              // Fecha de alta
              String fechaAlta = data.getFC_ALTA();
              dFechaAlta = formater.parse(fechaAlta);
              // Fecha de baja
              String fechaBaja = data.getFC_ULTACT();
              dFechaBaja = formater.parse(fechaBaja);
           if (dFechaNotif.before(dFechaAlta) || dFechaNotif.after(dFechaBaja))
              {
                 // System_out.println ("Efectivamente esta de baja, cambiamos el modo");
                 modo = modoCONSULTA;
              }
           } catch (Exception e) {
               e.printStackTrace();
           }
        }//Fin de si esta de BAJA
              } */

    } // Fin de si data != null

    modoOperacion = modo;
    Inicializar();
  } // fin m�todo btnEquipo_actionPerformed()

  // comprueba que todos los datos obligatorios est�n rellenos
  // y, adem�s, que los datos introducidos sean correctos
  //    ( c�digos de nivel1 y nivel2, y c�digo de equipo notif.)
  private boolean isDataValid() {

    try {
      parametros = new CLista();
      parametros.addElement(new DataEntradaEDO
                            (getCodEquipo(),
                             (String) hashNotifEDO.get("CD_ANOEPI"),
                             txtSemanaEpi.getText().trim(),
                             sCodNivel1,
                             sCodNivel2,
                             this.getFechaNotifEquipo()));

      if (pArDiAn.CON_ZBS) {
        result = Comunicador.Communicate(getApp(),
                                         stubCliente,
                                         servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS,
                                         strSERVLET_MAESTRO,
                                         parametros);
      }
      else {
        result = Comunicador.Communicate(getApp(),
                                         stubCliente,
                                         servletOBTENER_EQUIPO_X_CODIGO,
                                         strSERVLET_MAESTRO,
                                         parametros);

      }

      if (result.size() == 0) {
        ShowWarning(resBun.getString("msg54.Text"));
        txtCodEquipo.setText("");
        listaOpeUltactTabla = new CLista();
        return false;
      }
      // si devuelve datos: NUEVO mlm
      DataEntradaEDO datos = (DataEntradaEDO) result.firstElement();
      hashNotifEDO.put("CD_NIVEL_1", sCodNivel1);
      hashNotifEDO.put("CD_NIVEL_2", datos.getNivel2().trim());

      //actualiza los datos de cabecera
      ActDatosCabecera( (DataEntradaEDO) result.elementAt(0));

    }
    catch (Exception e) {
      return false;
    }
    return true;
  } // fin m�todo isDataValid()

  /**
   * comprueba que todos los datos de cabecera se encuentren rellenos
   */
  /*
   * Autor          Fecha           Acci�n
   *  JRM           17/07/2000      Actualiza a perfil 5
   *
   */
  private boolean isCabFull() {

    // no pueden faltar al mismo tiempo la semana epidemiol�gica
    // y la fecha de notificaci�n
    // JRM: salvo que el usuario sea de perfil 5 con lo que tiene asignado ya
    // un notificador fijo.
    if (app.getPerfil() != 5) {
      // Para Madrid 26-03-01  (ARS)
      if (sLaComunidad.equals("M")) {
        // Madrid
        if ( (CfechaNotif.getText().trim().equals(""))
            & (txtSemanaEpi.getText().trim().equals(""))) {
          ShowWarning(resBun.getString("msg55.Text"));
          return false;
        }
      }
      else {
        // Otras comunidades
        if ( (CfechaFinNotif.getText().trim().equals(""))
            & (txtSemanaEpi.getText().trim().equals(""))) {
          ShowWarning(resBun.getString("msg55.Text"));
          return false;
        }
      }
    }
    // el c�digo del equipo debe encontrarse relleno
    if (txtCodEquipo.getText().trim().equals("")) {
      //ShowWarning("Introduzca notificador"); (mlm, si por validar, no le hace falta)
      return false;
    }

    return true;
  } // fin m�todo isCabFull()

  // comprueba que los datos de notificaci�n est�n rellenos
  private boolean isNotifFull() {

    // la fecha de recepci�n debe estar rellena
    if (CfechaRecep.getText().trim().equals("")) {
      return false;
    }

    return true;
  } // fin m�todo isNotifFull()

  // retorna el c�digo del equipo seleccionado
  public String getCodEquipo() {
    return (txtCodEquipo.getText().trim());
  }

  // retorna la descripci�n del equipo seleccionado
  public String getDesEquipo() {
    return (txtDesEquipo.getText().trim());
  }

  // retorna el a�o seleccionado
  public String getAnno() {
    return sAnno;
  }

  //it_cobertura:cobertura
  public String getsCobertura() {
    return sCobertura;
  }

  // retorna la fecha seleccionada formateada a dos caracteres
  public String getSemana() {
    String sSem = txtSemanaEpi.getText().trim();
    if (sSem.length() == 2) {
      return sSem;
    }
    else if (sSem.length() == 1) {
      return ("0" + sSem);
    }
    return "";
  }

  // retorna la fecha de recepci�n
  public String getFechaRecep() {
    // se llama al m�todo ActCFecha para fijar la fecha de recepci�n
    ActCFecha(CfechaRecep, CfechaRecep.getText().trim());
    return CfechaRecep.getText().trim();
  }

  // ARG: retorna la fecha de notificaci�n para su uso en la busqueda de un equipo
  public String getFechaNotifEquipo() {
    if (sLaComunidad.equals("M")) {
      // Madrid
      // se llama al m�todo ActCFecha para fijar la fecha de notificaci�n
      ActCFecha(CfechaNotif, CfechaNotif.getText().trim());
      return CfechaNotif.getText().trim();
    }
    else {
      // Otras comunidades
      // se llama al m�todo ActCFecha para fijar la fecha de notificaci�n
      ActCFecha(CfechaFinNotif, CfechaIniNotif, CfechaFinNotif.getText().trim());
      return CfechaIniNotif.getText().trim();
    }
  }

  // retorna la fecha de notificaci�n
  public String getFechaNotif() {
    // Para Madrid 26-03-01 (ARS)
    if (sLaComunidad.equals("M")) {
      // Madrid
      // se llama al m�todo ActCFecha para fijar la fecha de notificaci�n
      ActCFecha(CfechaNotif, CfechaNotif.getText().trim());
      return CfechaNotif.getText().trim();
    }
    else {
      // Otras comunidades
      // se llama al m�todo ActCFecha para fijar la fecha de notificaci�n
      ActCFecha(CfechaFinNotif, CfechaIniNotif, CfechaFinNotif.getText().trim());
      return CfechaFinNotif.getText().trim();
    }
  }

  // retorna si es resumen semanal
  public String getResSem() {
    if (chkbxResSem.getState()) {
      return "S";
    }
    return "N";
  }

  // retorna el c�digo del operador
  public String getLogin() {
    return app.getLogin();
  }

  // retorna la fecha actual
  public String getFechaAct() {
    return UtilEDO.getFechaAct();
  }

  public String getNivel1() {
    return sCodNivel1;
  }

  public String getNivel2() {
    sCodNivel2 = "";
    if ( ( (String) hashNotifEDO.get("CD_NIVEL_2")) != null) {
      sCodNivel2 = (String) hashNotifEDO.get("CD_NIVEL_2");
    }
    return sCodNivel2;
  }

  // devuelve el valor inicial de los notificadores te�ricos
  // s�lo si el equipo seleccionado se encuentra en cobertura
  public String getNotifTeorPrim() {
    if ( ( (String) hashNotifEDO.get("IT_COBERTURA")) != null) {
      return sNotifTeorPrim;
    }
    else {
      return "";
    }
  }

  // devuelve el valor actual de los notificadores te�ricos
  // s�lo si el equipo seleccionado se encuentra en cobertura
  public String getNotifTeorAct() {
    if ( ( (String) hashNotifEDO.get("IT_COBERTURA")) != null) {
      return txtNotifTeor.getText().trim();
    }
    return "";
  }

// devuelve el valor de los notificadores reales
// s�lo si el equipo seleccionado se encuentra en cobertura
  public String getNotifReales() {
    if ( ( (String) hashNotifEDO.get("IT_COBERTURA")) != null) {
      String sNotifReal = txtNotifReales.getText().trim();
      if (sNotifReal.equals("0")) {
        return "";
      }
      else {
        return sNotifReal;
      }
    }
    return "";
  }

  // devuelve el valor del total de notificadores te�ricos
  // s�lo si el equipo seleccionado se encuentra en cobertura
  public String getTotNotifReales() {
    if ( ( (String) hashNotifEDO.get("IT_COBERTURA")) != null) {
      return txtTotNotifReales.getText().trim();
    }
    return "";
  }

  // devuelve el valor defenitivo de notificadores reales
  //  (totreales - reales)
  public String getDefNotifReales() {
    String sTotReales = getTotNotifReales();
    String sReales = getNotifReales();

    if (sTotReales.equals("") || sTotReales.equals("0")) {
      return "";
    }
    if (sReales.equals("")) {
      sReales = "0";
    }
    int iTotNotifReales = (new Integer(sTotReales)).intValue();
    int iNotifReales = (new Integer(sReales)).intValue();
    return (new Integer(iTotNotifReales - iNotifReales)).toString();
  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    if (msgBox != null) {
      msgBox = null;
    }
    msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  // se ejecuta en respuesta al actionPerformed de btnSalir
  void btnSalir_actionPerformed() {
    // se vuelve a la pantalla de entrada al m�dulo de notificaciones
    applet.VerEntradaEDO();
  }

// se ejecuta en respuesta al actionPerformed de btnAlta
  void btnAlta_actionPerformed() {

    applet.pListaEDOIndiv.IT_VALIDADA_PRIM = "";
    applet.pListaEDOIndiv.FC_FECVALID_PRIM = "";
    applet.pListaEDONum.IT_VALIDADA_PRIM = "";
    applet.pListaEDONum.FC_FECVALID_PRIM = "";

    //previo a dar el alta, actualizo datos
    // ARS para Madrid
    if (sLaComunidad.equals("M")) {
      ActCFecha(CfechaNotif, CfechaNotif.getText());
    }
    else {
      ActCFecha(CfechaFinNotif, CfechaIniNotif, CfechaFinNotif.getText());
    }
    ActCFecha(CfechaRecep, CfechaRecep.getText());

    // genera la cobertura
    modoOperacion = modoESPERA;
    Inicializar();
    GeneradorDeCobertura();

    //pedimos los datos de notif_sem:
    //si NO cobertura: la generacion, no genera su registro en notif_sem
    if (sCobertura.equals("S")) {
      int imodo = Datos_Notif_Sem(); //los carga en variables y los pinta
      //carga el la listaOpeUltactTabla el dato con notif_sem

      if (imodo == modoCABECERAYBOTONES) { //ERROR en Datos_Notif_Sem
        modoOperacion = modoCABECERAYBOTONES;
        Inicializar();
        return;
      } //--------------------------------------------

    }
    else {
      //llenamos notif_sem con nada
      DataOpeFc data = new DataOpeFc();
      listaOpeUltactTabla.addElement(data);
    }
    //------------------------------

    modoOperacion = modoALTA;
    Inicializar();

    //pintada  de datos!!!
    //sNotifRealesPrim = "0";
    if (sCobertura.equals("S")) {
      lResSem.setVisible(true);
      chkbxResSem.setVisible(true);

      //txtNotifTeor.setText(sNotifTeorPrim);
      //txtNotifReales.setText(sNotifRealesPrim);
      //txtTotNotifReales.setText(sTotNotifRealesPrim);
    }
    else {
      lResSem.setVisible(false);
      chkbxResSem.setVisible(false);
    }
    //validacion
    DataListaNotifEDO datos = Validar_en_Nueva();
    applet.pListaEDONum.Cargar_Validar(datos);
    applet.pListaEDOIndiv.Cargar_Validar(datos);
    //-----------------------------

    doLayout();

    // el foco de entrada se pasa a la fecha de recepci�n
    // (para que as�, cuando lo pierda, se hagan las comprobaciones)
    CfechaRecep.select(CfechaRecep.getText().length(),
                       CfechaRecep.getText().length());
    CfechaRecep.requestFocus();

    // JRM: Deshabilitamos al dar al aceptar el panel de �reas, dist y a�o
    pArDiAn.habilitarPanel(false);

  }

  public DataListaNotifEDO Validar_en_Nueva() {

    String it_validada = "";
    String fc_validada = "";

    if (this.app.getIT_MODULO_3().equals("N")) {
      it_validada = "S";
      fc_validada = this.app.getFC_ACTUAL();
    }
    //modulo 3
    else if (this.app.getIT_MODULO_3().equals("S")) {
      if (this.app.getPerfil() == 5) { //equipo
        it_validada = "N";
        fc_validada = "";
      }
      else if (this.app.getPerfil() == 2 || //epidemiologo
               this.app.getPerfil() == 3 ||
               this.app.getPerfil() == 4) {
        it_validada = "S";
        fc_validada = this.app.getFC_ACTUAL();
      }
      else { //tipo 1, no procede
        it_validada = "N";
        fc_validada = "";
      }
    }

    DataListaNotifEDO datos = new DataListaNotifEDO
        ("", "", "", "", "", "", "",
         "", "", "", "", "", "",
         it_validada, fc_validada, "", "");

    return (datos);
  }

  void btnBaja_actionPerformed() {

    applet.pListaEDOIndiv.IT_VALIDADA_PRIM = "";
    applet.pListaEDOIndiv.FC_FECVALID_PRIM = "";
    applet.pListaEDONum.IT_VALIDADA_PRIM = "";
    applet.pListaEDONum.FC_FECVALID_PRIM = "";

    //previo a dar la baja, actualizo datos
// ARS para Madrid  26-03-01
    if (sLaComunidad.equals("M")) {
      ActCFecha(CfechaNotif, CfechaNotif.getText());
    }
    else {
      ActCFecha(CfechaFinNotif, CfechaIniNotif, CfechaFinNotif.getText());
    }

    ActCFecha(CfechaRecep, CfechaRecep.getText());
    modoOperacion = modoBAJA;
    //Inicializar(); se hace en show

    showListaNotifEDO(servletSELECCION_LISTA_NOTIF_FECHA);

    // JRM: Deshabilitamos al dar al aceptar el panel de �reas, dist y a�o
    pArDiAn.habilitarPanel(false);
  }

  void btnModificacion_actionPerformed() {
    applet.pListaEDOIndiv.IT_VALIDADA_PRIM = "";
    applet.pListaEDOIndiv.FC_FECVALID_PRIM = "";
    applet.pListaEDONum.IT_VALIDADA_PRIM = "";
    applet.pListaEDONum.FC_FECVALID_PRIM = "";

    //previo a dar la modificaci�n, actualizo datos
// ARS para Madrid  26-03-01
    if (sLaComunidad.equals("M")) {
      ActCFecha(CfechaNotif, CfechaNotif.getText());
    }
    else {
      ActCFecha(CfechaFinNotif, CfechaIniNotif, CfechaFinNotif.getText());
    }
    ActCFecha(CfechaRecep, CfechaRecep.getText());

    // JRM2 Modifico las lineas de abajo por si entra desde el modo CONSULTAR
    //  Cuando el Notificador este de BAJA se entrara en modo CONSULTAR
    if (modoOperacion != modoCONSULTA && modoOperacion != modoCONSULTAYBOTONES) {
      modoOperacion = modoMODIFICACION;
    }
    //Inicializar();  se hace en Show

    showListaNotifEDO(servletSELECCION_LISTA_NOTIF_FECHA);

    // JRM: Deshabilitamos al dar al aceptar el panel de �reas, dist y a�o
    pArDiAn.habilitarPanel(false);

  }

  //NOTA: mlm supongo que el que puede validar, puede modificar
  void btnValidar_actionPerformed() {

    applet.pListaEDOIndiv.IT_VALIDADA_PRIM = "";
    applet.pListaEDOIndiv.FC_FECVALID_PRIM = "";
    applet.pListaEDONum.IT_VALIDADA_PRIM = "";
    applet.pListaEDONum.FC_FECVALID_PRIM = "";

    if (!bMod) {
      ShowWarning(resBun.getString("msg56.Text"));
      return;
    }
    //previo a dar la modificaci�n, actualizo datos
// ARS para Madrid
    if (sLaComunidad.equals("M")) {
      // Madrid
      ActCFecha(CfechaNotif, CfechaNotif.getText());
    }
    else {
      // Otras comunidades
      ActCFecha(CfechaFinNotif, CfechaIniNotif, CfechaFinNotif.getText());
    }
    ActCFecha(CfechaRecep, CfechaRecep.getText());

    //modoOperacion = modoMODIFICACION; se hace en Show
    //Inicializar();  se hace en Show

    showListaNotifEDO_Validar(servletSELECCIONVALIDAR);
  }

  void btnNotif_actionPerformed() {
    applet.pListaEDOIndiv.IT_VALIDADA_PRIM = "";
    applet.pListaEDOIndiv.FC_FECVALID_PRIM = "";
    applet.pListaEDONum.IT_VALIDADA_PRIM = "";
    applet.pListaEDONum.FC_FECVALID_PRIM = "";

    //previo a dar alta, actualizo datos
// ARS para Madrid
    if (sLaComunidad.equals("M")) {
      // Madrid
      ActCFecha(CfechaNotif, CfechaNotif.getText());
    }
    else {
      // Otras comunidades
      ActCFecha(CfechaFinNotif, CfechaIniNotif, CfechaFinNotif.getText());
    }
    ActCFecha(CfechaRecep, CfechaRecep.getText());

    if (!bAlta && !bBaja && !bMod) { //modoCONSULTA
      modoOperacion = modoCONSULTA;
    }
    else {
      modoOperacion = modoMODIFICACION;
      //Inicializar();  se hace en show

    }
    showListaNotifEDO(servletSELECCION_LISTA_NOTIF);

    // JRM: Deshabilitamos al dar al aceptar el panel de �reas, dist y a�o
    pArDiAn.habilitarPanel(false);

  }

  private void showListaNotifEDO(int iSel) {

    //hasta aqui debe venir la listaOpeFcTabla con e_notif SOLO
    //si procede de Alta -> modificacion, quitarla.
    int iTamListaAqui = listaOpeUltactTabla.size();
    if (iTamListaAqui > 1) {
      //cargar listaOpeUltactTabla con solo e_notif:
      DataOpeFc dataReservaEquipo = null;
      dataReservaEquipo = (DataOpeFc) listaOpeUltactTabla.elementAt(0);
      listaOpeUltactTabla = new CLista();
      listaOpeUltactTabla.addElement(dataReservaEquipo);
    } //*******************************************************

    int iEstado = 0;
    int modo = modoOperacion; //modificacion o borrado masivo o lupa
    int modoInicial = modo;

    modoOperacion = modoESPERA;
    Inicializar();

    CLista data = new CLista(); //resultado
    DataListaNotifEDO dataNotif = new DataListaNotifEDO(); //resultado dlg
    DataListaNotifEDO dataEDO = new DataListaNotifEDO("", getFechaNotif(),
        "", "", "", "", getCodEquipo(), getAnno(),
        getSemana(), "", "", "", "", "", "", "", ""); //entrada

    try {

      parametros = new CLista();
      parametros.addElement(dataEDO);
      //parametros.setLogin(app.getLogin());  lo hace comunicate
      data = Comunicador.Communicate(getApp(),
                                     stubCliente,
                                     iSel,
                                     strSERVLET_NOTIFEDO,
                                     parametros);

      String title = "";
      if (iSel == 3) {
        title = resBun.getString("msg57.Text");
      }
      if (iSel == 7) {
        title = resBun.getString("msg58.Text");
        //existen datos
      }
      if (data.size() != 0) {

        DialogMaestroEDO dlg = new DialogMaestroEDO(app, data, title);
        dlg.show();

        if (dlg.iOut == 0) {
          dataNotif = (DataListaNotifEDO) dlg.getComponente();

          //realmente pasamos a MODIFICACION -----(o a borrado masivo)
          if (dataNotif != null) {

            ActDatosNotif(dataNotif); //se cargan los Prim

            //cargar listaopeultacttabla   NOTIF SEM
            DataOpeFc datosnotifsem;
            Vector vnotifsem = new Vector();
            DataOpeFc datavnotifsem = null;
            datavnotifsem = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
            vnotifsem.addElement(datavnotifsem);
            datavnotifsem = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
            vnotifsem.addElement(datavnotifsem);
            datavnotifsem = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
            vnotifsem.addElement(datavnotifsem);

            datosnotifsem = new DataOpeFc(dataNotif.getCD_OPE_NOTIF_SEM(),
                                          dataNotif.getFC_ULTACT_NOTIF_SEM(), //HH:mm:SSS
                                          "SIVE_NOTIF_SEM", vnotifsem);

            listaOpeUltactTabla.addElement(datosnotifsem);

            //NOTIFEDO
            DataOpeFc datosnotifedo;
            Vector vnotifedo = new Vector();
            DataOpeFc datavnotifedo = null;
            datavnotifedo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
            vnotifedo.addElement(datavnotifedo);
            datavnotifedo = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
            vnotifedo.addElement(datavnotifedo);
            datavnotifedo = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
            vnotifedo.addElement(datavnotifedo);
            datavnotifedo = new DataOpeFc("FC_RECEP", dataNotif.getFechaRecep(),
                                          "F");
            vnotifedo.addElement(datavnotifedo);
            if (sLaComunidad.equals("M")) {
              datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaNotif.getText(),
                                            "F");
            }
            else {
              datavnotifedo = new DataOpeFc("FC_FECNOTIF",
                                            dataNotif.getFechaNotif(), "F");
            }
            vnotifedo.addElement(datavnotifedo);

            datosnotifedo = new DataOpeFc(dataNotif.getCD_OPE_NOTIFEDO(),
                                          dataNotif.getFC_ULTACT_NOTIFEDO(), //HH:mm:SSS
                                          "SIVE_NOTIFEDO", vnotifedo);

            listaOpeUltactTabla.addElement(datosnotifedo);
            //-------------------------

            RellenaListas(getCodEquipo(),
                          getAnno(),
                          getSemana(),
                          getFechaRecep(),
                          getFechaNotif());

            //control de error al rellenarlistas
            if (modoOperacion == modoCABECERAYBOTONES) {
              return;
            } //**************************************

            enableButtons(false);
            iEstado = 1; //OK;
          } //-------- ----- - - - - - - - - - - - -
          else {
            ShowWarning(resBun.getString("msg59.Text"));
            modo = modoCABECERAYBOTONES;
          }
        }
        else { // JRM2 Modifica para los de Baja, con modoCONSULTA van los de Baja  "modoCABECERAYBOTONES"
          if (modoInicial == modoCONSULTA ||
              modoInicial == modoCONSULTAYBOTONES) {
            modo = modoCONSULTA;
          }
          else {
            modo = modoCABECERAYBOTONES;
          }
        }
        dlg = null;
      }
      //sin datos
      else {
        ShowWarning(resBun.getString("msg59.Text"));
        //AIC -- para que a la salida mantenga el modo consulta.
        if (modo != modoCONSULTA && modo != modoCONSULTAYBOTONES) {
          modo = modoCABECERAYBOTONES;
        }
      }
    }
    catch (Exception e) {
      //AIC -- para que a la salida mantenga el modo consulta.
      if (modo != modoCONSULTA && modo != modoCONSULTAYBOTONES) {
        modo = modoCABECERAYBOTONES;
      }
    }

    //CARGAR PEGOTE!!! perfil 5
    if (iEstado == 1 &&
        applet.getIT_MODULO_3().trim().equals("S") &&
        applet.getPerfil() == 5 &&
        (modoInicial == modoMODIFICACION
         || modoInicial == modoBAJA)
        && dataNotif.getIT_VALIDADA().trim().equals("S")) {
      modo = modoCONSULTA;

    } //--------------------------------------------

    modoOperacion = modo; // JRM2 Debe cambiar el modo de salida al darle al cancelar
    Inicializar();

  } //fin show

  private void showListaNotifEDO_Validar(int iSel) {

    int modo = modoOperacion; //pulsando validar

    modoOperacion = modoESPERA;
    Inicializar();

    CLista data = new CLista(); //resultado
    DataListaNotifEDO dataNotif = new DataListaNotifEDO(); //resultado dlg
    DataListaNotifEDO dataEDO = new DataListaNotifEDO(); //entrada

    dataEDO.sCodEquipo = getCodEquipo();
    dataEDO.sAnno = getAnno();
    dataEDO.sSemana = getSemana();
    dataEDO.sFechaNotif = getFechaNotif();
    dataEDO.CD_NIVEL_1 = sCodNivel1;
    dataEDO.CD_NIVEL_2 = sCodNivel2;

    try {

      parametros = new CLista();
      parametros.addElement(dataEDO);
      parametros.setLogin(app.getLogin());
      data = Comunicador.Communicate(getApp(),
                                     stubCliente,
                                     iSel,
                                     strSERVLET_NOTIFEDO,
                                     parametros);

      //TITULO:
      String title = resBun.getString("msg60.Text");
      if (!getCodEquipo().trim().equals("")) {
        title = title + resBun.getString("msg61.Text");
      }
      else {
        if (!getSemana().trim().equals("")) {
          title = title + resBun.getString("msg62.Text");
        }
        else {
          title = title + resBun.getString("msg63.Text");
        }
      } //titulo ----------------------------

      //existen datos
      if (data.size() != 0) {

        DialogMaestroEDO dlg = new DialogMaestroEDO(app, data, title);
        dlg.show();

        if (dlg.iOut == 0) { //aceptar
          dataNotif = (DataListaNotifEDO) dlg.getComponente();

          //realmente pasamos a MODIFICACION -----
          if (dataNotif != null) {

            // ARS para Madrid
            if (sLaComunidad.equals("M")) {
              // Magerit.
              //cargar las cajas superiores (FECHA NOTIF)
              if (CfechaNotif.getText().trim().equals("")) {
                CfechaNotif.setText(dataNotif.getFechaNotif());
                CfechaNotif_focusLost(new java.awt.event.FocusEvent(CfechaNotif,
                    java.awt.event.FocusEvent.FOCUS_LOST));
                if (modoOperacion != modoESPERA) {
                  modoOperacion = modoESPERA;
                  Inicializar();
                }

              }
            }
            else {
              // Otras comunidades.
              //cargar las cajas superiores (FECHA NOTIF)
              if (CfechaFinNotif.getText().trim().equals("")) {
                //AIC
                ActCFecha(CfechaFinNotif, CfechaIniNotif,
                          dataNotif.getFechaNotif());
                CfechaFinNotif.setText(dataNotif.getFechaNotif());
                CfechaIniNotif.setText(CfechaFinNotif.annadirDias( -7));
                CfechaFinNotif_focusLost(new java.awt.event.FocusEvent(
                    CfechaFinNotif, java.awt.event.FocusEvent.FOCUS_LOST));
              }

              if (modoOperacion != modoESPERA) {
                modoOperacion = modoESPERA;
                Inicializar();
              }

            }
            //CON EQUIPO
            if (!txtCodEquipo.getText().trim().equals("")) {
              //hasta aqui debe venir la listaOpeFcTabla con e_notif SOLO
              //si procede de Alta -> modificacion, quitarla.
              int iTamListaAqui = listaOpeUltactTabla.size();
              if (iTamListaAqui > 1) {
                //cargar listaOpeUltactTabla con solo e_notif:
                DataOpeFc dataReservaEquipo = null;
                dataReservaEquipo = (DataOpeFc) listaOpeUltactTabla.elementAt(0);
                listaOpeUltactTabla = new CLista();
                listaOpeUltactTabla.addElement(dataReservaEquipo);
              } //*******************************************************
            }
            //SIN EQUIPO
            if (txtCodEquipo.getText().trim().equals("")) {
              listaOpeUltactTabla = new CLista();

              txtCodEquipo.setText(dataNotif.getCodEquipo());
              CLista plista = new CLista();
              plista.addElement(new DataEntradaEDO
                                (getCodEquipo(),
                                 (String) hashNotifEDO.get("CD_ANOEPI"),
                                 txtSemanaEpi.getText().trim(),
                                 sCodNivel1,
                                 sCodNivel2));

              result = Comunicador.Communicate(getApp(),
                                               stubCliente,
                                               servletOBTENER_EQUIPO_X_CODIGO,
                                               strSERVLET_MAESTRO,
                                               plista);

              if (result.size() == 0) {
                //no puede ser
              }
              else {
                ActDatosCabecera( (DataEntradaEDO) result.elementAt(0));
                if (modoOperacion != modoESPERA) {
                  modoOperacion = modoESPERA;
                  Inicializar();
                }

              }
            } //sin equipo

            ActDatosNotif(dataNotif); //se cargan los Prim
            if (modoOperacion != modoESPERA) {
              modoOperacion = modoESPERA;
              Inicializar();
            }

            //cargar listaopeultacttabla   NOTIF SEM
            DataOpeFc datosnotifsem;
            Vector vnotifsem = new Vector();
            DataOpeFc datavnotifsem = null;
            datavnotifsem = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
            vnotifsem.addElement(datavnotifsem);
            datavnotifsem = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
            vnotifsem.addElement(datavnotifsem);
            datavnotifsem = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
            vnotifsem.addElement(datavnotifsem);

            datosnotifsem = new DataOpeFc(dataNotif.getCD_OPE_NOTIF_SEM(),
                                          dataNotif.getFC_ULTACT_NOTIF_SEM(), //HH:mm:SSS
                                          "SIVE_NOTIF_SEM", vnotifsem);

            listaOpeUltactTabla.addElement(datosnotifsem);

            //NOTIFEDO
            DataOpeFc datosnotifedo;
            Vector vnotifedo = new Vector();
            DataOpeFc datavnotifedo = null;
            datavnotifedo = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
            vnotifedo.addElement(datavnotifedo);
            datavnotifedo = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
            vnotifedo.addElement(datavnotifedo);
            datavnotifedo = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
            vnotifedo.addElement(datavnotifedo);
            datavnotifedo = new DataOpeFc("FC_RECEP", dataNotif.getFechaRecep(),
                                          "F");
            vnotifedo.addElement(datavnotifedo);
            if (sLaComunidad.equals("M")) {
              datavnotifedo = new DataOpeFc("FC_FECNOTIF", CfechaNotif.getText(),
                                            "F");
            }
            else {
              datavnotifedo = new DataOpeFc("FC_FECNOTIF",
                                            dataNotif.getFechaNotif(), "F");
            }
            vnotifedo.addElement(datavnotifedo);

            datosnotifedo = new DataOpeFc(dataNotif.getCD_OPE_NOTIFEDO(),
                                          dataNotif.getFC_ULTACT_NOTIFEDO(), //HH:mm:SSS
                                          "SIVE_NOTIFEDO", vnotifedo);

            listaOpeUltactTabla.addElement(datosnotifedo);
            //-------------------------

            RellenaListas(getCodEquipo(),
                          getAnno(),
                          getSemana(),
                          getFechaRecep(),
                          getFechaNotif());

            //control de error al rellenarlistas
            if (modoOperacion == modoCABECERAYBOTONES) {
              return;
            } //**************************************

            enableButtons(false);
            modo = modoMODIFICACION;

          } //fin de if (dataNotif != null)
          else {
            ShowWarning(resBun.getString("msg59.Text"));
          }
        }
        else { //pulso cancelar en el dlg
          //nada
        }
        dlg = null;
      }
      //sin datos
      else {
        ShowWarning(resBun.getString("msg59.Text"));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    modoOperacion = modo;
    Inicializar();

  } //fin show

  // llama a los m�todos correspondientes de los paneles
  // PanelListaEDONUm y PanelListaEDOIndiv, que rellenan las listas
  // de EDO Num. y EDO Ind., tanto para d�a como para semana.
  public void RellenaListas(String codequipo, String anno, String semana,
                            String fecharecep, String fechanotif) {
    applet.pListaEDOIndiv.RellenaLista(codequipo,
                                       anno,
                                       semana,
                                       fecharecep,
                                       fechanotif);

    //control de error al rellenarlistas
    if (modoOperacion == modoCABECERAYBOTONES) {
      return;
    } //**************************************

    applet.pListaEDONum.RellenaLista(codequipo,
                                     anno,
                                     semana,
                                     fecharecep,
                                     fechanotif);

    bListaNumVacia = applet.pListaEDONum.getbListaNumVacia();

  }

  // llama a los m�todos correspondientes de los paneles
  // PanelListaEDOIndiv y PanelListaEDONum, que vac�an las listas
  // de EDO Num y EDO Ind., tanto para d�a como para semana
  /*public void VaciaListas() {
    applet.pListaEDONum.InitPanel();
    applet.pListaEDOIndiv.InitPanel();
     } */

  // se ejecuta en respuesta al itemStateChanged de chkbxResSem
  void chkbxResSem_itemStateChanged() {
    // dependiendo de si est� o no seleccionado, se habr�n de
    // visualizar o no ciertos controles
    if ( (chkbxResSem.getState()) && (sCobertura.equals("S"))) {
      showControls(true);
      ActDatosNotif();
    }
    else {
      showControls(false);
    }
  }

  // se ejecuta en respuesta la focuslost de CfechaNotif
  void CfechaNotif_focusLost(FocusEvent e) {

    // si la fecha no ha cambiado, no se hace nada
    if (getFechaNotif().equals(sFecSemIniBk)) {
      //mlm
      hashNotifEDO.put("FC_FECNOTIF", CfechaNotif.getText().trim());
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
      return;
    }

    // y si se ha modificado, se inician las comprobaciones:
    //  1. se comprueba que la fecha de notificaci�n es correcta
    //  2. la fecha de notificaci�n no puede ser mayor que la actual
    //  3. la fecha de notif. no puede ser mayor que la de recepci�n

    try {
      //RecuperaSemanaDeFecha();

      // 1. se comprueba que la fecha de notificaci�n sea correcta
      // si la fecha de notificaci�n no es v�lida, terminar
      if (!CompruebaFechaNotif()) {
        //mlm
        hashNotifEDO.put("FC_FECNOTIF", CfechaNotif.getText().trim());
        hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
        return;
      }

      // 2. la fecha de notificaci�n no puede ser mayor que la actual
      if (UtilEDO.fecha1MayorqueFecha2(getFechaNotif(), getFechaAct())) {

        ShowWarning(resBun.getString("msg64.Text"));
        ActCFecha(CfechaNotif, getFechaAct());
      }

      // 3. la fecha de notif. no puede ser mayor que la de recepci�n
      if ( (!getFechaRecep().equals(""))) {
        if (UtilEDO.fecha1MayorqueFecha2(getFechaNotif(), getFechaRecep())) {
// modificacion jlt
//      if (UtilEDO.fecha1MayorqueFecha2(getFechaNotif(), getFechaRecep())
//          && (!getFechaRecep().equals("")) ) {

          ShowWarning(resBun.getString("msg65.Text"));
          ActCFecha(CfechaNotif, getFechaRecep());
        }
      }

      // envia la excepci�n
    }
    catch (Exception ex) {
      hashNotifEDO.put("FC_FECNOTIF", CfechaNotif.getText().trim());
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
      return;
    }

    //CARGAR PEGOTE!!! perfil 5
    if (applet.getIT_MODULO_3().trim().equals("S") &&
        applet.getPerfil() == 5) {

      RecuperaSemanaDeFecha(CfechaNotif.getText().trim());
      hashNotifEDO.put("FC_FECNOTIF", CfechaNotif.getText().trim());
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
      // se actualiza hashNotifEDO con los valores anteriores
      hashNotifEDO.put("CD_E_NOTIF", sCodEquipo);
      hashNotifEDO.put("CD_ANOEPI", this.getAnno());
      hashNotifEDO.put("CD_SEMEPI", this.getSemana());
      hashNotifEDO.put("DS_E_NOTIF", sDesEquipo);
      hashNotifEDO.put("CD_C_NOTIF", sCodCentro);
      hashNotifEDO.put("DS_C_NOTIF", sDesCentro);
      hashNotifEDO.put("IT_COBERTURA", sCobertura);
      hashNotifEDO.put("FC_FECNOTIF", getFechaNotif());
      hashNotifEDO.put("FC_RECEP", getFechaRecep());

      modoOperacion = modoBOTONES;
      Inicializar();
    } //--------------------------------------------
    else {
      // si todo ha ido bien, se recupera la semana correspondiente
      // a la fecha de notificaci�n
      RecuperaSemanaDeFecha(CfechaNotif.getText().trim());
      txtCodEquipo.setText("");
      sCodEquipoIni = "";
      listaOpeUltactTabla = new CLista();
      txtDesEquipo.setText("");
      txtCentro.setText("");

      // si pasa por aqu�, se ha modificado y se deja en modo CABECERA
      hashNotifEDO.put("FC_FECNOTIF", CfechaNotif.getText().trim());
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());

      modoOperacion = modoCABECERA;
      Inicializar();
    }

  }

  // se ejecuta en respuesta la focuslost de CfechaFinNotif
  void CfechaFinNotif_focusLost(FocusEvent e) {

    // si la fecha no ha cambiado, no se hace nada
    if (getFechaNotif().equals(sFecSemIniBk)) {
      //mlm
      hashNotifEDO.put("FC_FECNOTIF", CfechaFinNotif.getText().trim());
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
      return;
    }

    // y si se ha modificado, se inician las comprobaciones:
    //  1. se comprueba que la fecha de notificaci�n es correcta
    //  2. la fecha de notificaci�n no puede ser mayor que la actual
    //  3. la fecha de notif. no puede ser mayor que la de recepci�n

    try {
      //RecuperaSemanaDeFecha();

      // 1. se comprueba que la fecha de notificaci�n sea correcta
      // si la fecha de notificaci�n no es v�lida, terminar
      if (!CompruebaFechaNotif()) {
        //mlm
        hashNotifEDO.put("FC_FECNOTIF", CfechaFinNotif.getText().trim());
        hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
        return;
      }

      // 2. la fecha de notificaci�n no puede ser mayor que la actual
      if (UtilEDO.fecha1MayorqueFecha2(getFechaNotif(), getFechaAct())) {

        ShowWarning(resBun.getString("msg64.Text"));
        ActCFecha(CfechaFinNotif, CfechaIniNotif, getFechaAct());
      }

      // 3. la fecha de notif. no puede ser mayor que la de recepci�n
      if (UtilEDO.fecha1MayorqueFecha2(getFechaNotif(), getFechaRecep())) {

        ShowWarning(resBun.getString("msg65.Text"));
        ActCFecha(CfechaFinNotif, CfechaIniNotif, getFechaRecep());
      }

      // envia la excepci�n
    }
    catch (Exception ex) {
      hashNotifEDO.put("FC_FECNOTIF", CfechaFinNotif.getText().trim());
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
      return;
    }

    //CARGAR PEGOTE!!! perfil 5
    if (applet.getIT_MODULO_3().trim().equals("S") &&
        applet.getPerfil() == 5) {

      RecuperaSemanaDeFecha(CfechaFinNotif.getText().trim());
      hashNotifEDO.put("FC_FECNOTIF", CfechaFinNotif.getText().trim());
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
      // se actualiza hashNotifEDO con los valores anteriores
      hashNotifEDO.put("CD_E_NOTIF", sCodEquipo);
      hashNotifEDO.put("CD_ANOEPI", this.getAnno());
      hashNotifEDO.put("CD_SEMEPI", this.getSemana());
      hashNotifEDO.put("DS_E_NOTIF", sDesEquipo);
      hashNotifEDO.put("CD_C_NOTIF", sCodCentro);
      hashNotifEDO.put("DS_C_NOTIF", sDesCentro);
      hashNotifEDO.put("IT_COBERTURA", sCobertura);
      hashNotifEDO.put("FC_FECNOTIF", getFechaNotif());
      hashNotifEDO.put("FC_RECEP", getFechaRecep());

      modoOperacion = modoBOTONES;
      Inicializar();
    } //--------------------------------------------
    else {
      // si todo ha ido bien, se recupera la semana correspondiente
      // a la fecha de notificaci�n
      RecuperaSemanaDeFecha(CfechaFinNotif.getText().trim());
      txtCodEquipo.setText("");
      sCodEquipoIni = "";
      listaOpeUltactTabla = new CLista();
      txtDesEquipo.setText("");
      txtCentro.setText("");

      // si pasa por aqu�, se ha modificado y se deja en modo CABECERA
      hashNotifEDO.put("FC_FECNOTIF", CfechaFinNotif.getText().trim());
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());

      modoOperacion = modoCABECERA;
      Inicializar();
    }

  }

  // m�todo que comprueba la fecha de notificaci�n
  private boolean CompruebaFechaNotif() {

    try {
      // Para Madrid (ARS) 26-03-01
      if (sLaComunidad.equals("M")) {
        //valida el dato para Madrid
        CfechaNotif.ValidarFecha();
      }
      else {
        //valida el dato para otras comunidades
        CfechaFinNotif.ValidarFecha();
      }

      // Para Madrid 26-03-01 (ARS)
      if (sLaComunidad.equals("M")) {
        // Madrid
        // Si no es v�lido, recupera la fecha del s�bado corresp. a la semana
        if (CfechaNotif.getValid().equals("N")) {
          //Si hay a�o v�lido
          int numero = Integer.parseInt(txtSemanaEpi.getText().trim());
          String resf = convDesde.getFec(numero);
          if (resf.equals("")) {
            txtSemanaEpi.setText(sCodSemIniBk);
            ActCFecha(CfechaNotif, sFecSemIniBk);
          }
          //si es semana v�lida actualiza la fecha de fin de sem y guarda los datos de reserva
          else {
            ActCFecha(CfechaNotif, sFecSemIniBk);
            sCodSemIniBk = txtSemanaEpi.getText();
            sFecSemIniBk = resf;
          }
          return false;
        }
        //Si es v�lido le da el formato adecuado
        else if (CfechaNotif.getValid().equals("S")) {
          CfechaNotif.setText(CfechaNotif.getFecha());
          return true;
        }
      }
      else {
        // Otras comunidades
        // Si no es v�lido, recupera la fecha del s�bado corresp. a la semana
        if (CfechaFinNotif.getValid().equals("N")) {
          //Si hay a�o v�lido
          int numero = Integer.parseInt(txtSemanaEpi.getText().trim());
          String resf = convDesde.getFec(numero);
          if (resf.equals("")) {
            txtSemanaEpi.setText(sCodSemIniBk);
            //AIC
            txtSemanaEpi.setText(getSemana());
            ActCFecha(CfechaFinNotif, CfechaIniNotif, sFecSemIniBk);
          }
          //si es semana v�lida actualiza la fecha de fin de sem y guarda los datos de reserva
          else {
            ActCFecha(CfechaFinNotif, CfechaIniNotif, sFecSemIniBk);
            //AIC
            //sCodSemIniBk = txtSemanaEpi.getText();
            sCodSemIniBk = getSemana();
            sFecSemIniBk = resf;
          }
          return false;
        }
        //Si es v�lido le da el formato adecuado
        else if (CfechaFinNotif.getValid().equals("S")) {
          CfechaFinNotif.setText(CfechaFinNotif.getFecha());
          return true;
        }
      }
    }
    catch (Exception e) {
      return false;
    }
    return false;
  } // fin m�todo CompruebaFechaNotif()

  // m�todo que obtiene la semana epidemiol�gica a partir de la fecha
  protected void RecuperaSemanaDeFecha(String sFecIni) {
    int res = 0;
    try {
      // Para Madrid (ARS) 26-03-01
      if (sLaComunidad.equals("M")) {
        //indeptemente de si he traido a�o o sigue el que hab�a, busca semana:
        res = convDesde.getNumSem(CfechaNotif.getText());
      }
      else {
        //indeptemente de si he traido a�o o sigue el que hab�a, busca semana:
        res = convDesde.getNumSem(CfechaFinNotif.getText());
      }

      //Si esa semana no es v�lida recupera los datos anteriores de semana y muestra un mensaje
      if (res == -1) {
        txtSemanaEpi.setText(sCodSemIniBk);
        //AIC
        txtSemanaEpi.setText(getSemana());
        // Para Madrid 26-03-01 (ARS)
        if (sLaComunidad.equals("M")) {
          ActCFecha(CfechaNotif, sFecSemIniBk);
        }
        else {
          ActCFecha(CfechaFinNotif, CfechaIniNotif, sFecSemIniBk);
        }
        ShowWarning(resBun.getString("msg66.Text"));
      }
      else {
        //Si semana es v�lida guarda los datos y muestra su c�d de semana
        txtSemanaEpi.setText(Integer.toString(res));
        //AIC
        txtSemanaEpi.setText(getSemana());
        sCodSemIniBk = txtSemanaEpi.getText();
        // Para Madrid 26-03-01 (ARS)
        if (sLaComunidad.equals("M")) {
          sFecSemIniBk = CfechaNotif.getText();
        }
        else {
          sFecSemIniBk = CfechaFinNotif.getText();
        }
        modoSEL = modoSELPORFECHA;
      }
    }
    catch (Exception ex) {
      txtSemanaEpi.setText("");
      sCodSemIniBk = "";
      // Madrid 26-03-01 (ARS)
      if (sLaComunidad.equals("M")) {
        sFecSemIniBk = CfechaNotif.getText();
      }
      else {
        sFecSemIniBk = CfechaFinNotif.getText();
      }
    }
  }

  void RecuperaFechaDeSemana(String sSemanaEpi) {
    int numero = 0;
    String res = "";
    String resReal = "";

    try {

      txtSemanaEpi.setText(txtSemanaEpi.getText().trim());

      //AIC
      txtSemanaEpi.setText(getSemana());

      //si no ha cambiado el valor de la caja de texto no hace nada
//      if   ((sSemanaEpi.equals(sCodSemIniBk)) ||
//            (sSemanaEpi.length() == 0))
//        return;

      if ( (sSemanaEpi.length() == 0)) {
        return;
      }

      //Si hay a�o v�lido
      numero = Integer.parseInt(txtSemanaEpi.getText());
      res = convDesde.getFec(numero); //luis devuelve el string de la base de datos

      //Si no es num semana v�lido recupera los datos anteriores y muestra mensaje
      // Si la semana es posterior a la actual, recupera los datos anteriores
      if (res.equals("")) {
        // Para Madrid 26-03-01 (ARS)
        if (sLaComunidad.equals("M")) {
          ActCFecha(CfechaNotif, sFecSemIniBk);
        }
        else {
          ActCFecha(CfechaFinNotif, CfechaIniNotif, sFecSemIniBk);

        }
        txtSemanaEpi.setText(sCodSemIniBk);

        //AIC
        txtSemanaEpi.setText(getSemana());

        ShowWarning(resBun.getString("msg67.Text"));
      }
      //si es semana v�lida actualiza la fecha de fin de sem y guarda los datos de reserva
      else {
        // la fecha de notificaci�n no puede ser mayor que la actual
        if (UtilEDO.fecha1MayorqueFecha2(res, getFechaAct())) {
          //AIC
          resReal = res;
          res = getFechaAct();
        }
        //AIC
        if (resReal.equals("")) {
          // Madrid 26-03-01 (ARS)
          if (sLaComunidad.equals("M")) {
            ActCFecha(CfechaNotif, res);
          }
          else {
            ActCFecha(CfechaFinNotif, CfechaIniNotif, res);

          }
        }
        else {
          // Madrid 26-03-01 (ARS)
          if (sLaComunidad.equals("M")) {
            ActCFecha(CfechaNotif, res);
          }
          else {
            ActCFecha(CfechaFinNotif, CfechaIniNotif, res, resReal);
          }
        }
        sCodSemIniBk = sSemanaEpi;
        sFecSemIniBk = res;
      }

      // Despu�s de actualizar el d�o semana - fecha notificaci�n,
      // se llama al generador de cobertura
      //GeneradorDeCobertura();
      // fin llamada la generador de cobertura

      // envia la excepci�n
    }
    catch (Exception e) {
      ShowWarning(resBun.getString("msg68.Text"));
    }
  }

  void txtSemanaEpi_focusLost() {

    int semact = 0; // semana actual
    int semint = 0; // semana intentada

    txtSemanaEpi.setText(getSemana());
    // si la semana no ha cambiado, no se hace nada
//    if (txtSemanaEpi.getText().trim().equals(sCodSemIniBk))
//      return;

    // si lo deja vacio
    if (txtSemanaEpi.getText().trim().equals("")) {
      txtSemanaEpi.setText(sCodSemIniBk);
      //AIC
      txtSemanaEpi.setText(getSemana());

      return;
    }

    int numero;
    String res;

    // en primer lugar se comprueba que la semana tenga formato num�rico
    try {
      semint = Integer.parseInt(txtSemanaEpi.getText().trim());
      semact = convDesde.getNumSem(getFechaAct());

      // la semana introducida no puede ser mayor que la actual
      // �����ojo!!!!! si estamos en el a�o en curso

      // si el a�o es igual al actual, comprobar la semana

      if ( (semint > semact) &&
          // Utilizamos sAnnoPanel en vez de sAnno que era la que exist�a
          (UtilEDO.getAnnoAct().equals(sAnno))) {
        // ARG: Para que no se dispare el evento de perdida de foco en area
        pArDiAn.bFoco1 = true;
        ShowWarning(resBun.getString("msg69.Text"));
        pArDiAn.bFoco1 = false;
        RecuperaFechaDeSemana(Integer.toString(semact));
        txtSemanaEpi.setText(Integer.toString(semact));
        txtSemanaEpi.setText(getSemana());
        return;
      }

      //CARGAR PEGOTE!!! perfil 5
      if (applet.getIT_MODULO_3().trim().equals("S") &&
          applet.getPerfil() == 5) {
        RecuperaFechaDeSemana(txtSemanaEpi.getText().trim());
        // se actualiza hashNotifEDO con los valores anteriores
        hashNotifEDO.put("CD_E_NOTIF", sCodEquipo);
        hashNotifEDO.put("CD_ANOEPI", this.getAnno());
        hashNotifEDO.put("CD_SEMEPI", this.getSemana());
        hashNotifEDO.put("DS_E_NOTIF", sDesEquipo);
        hashNotifEDO.put("CD_C_NOTIF", sCodCentro);
        hashNotifEDO.put("DS_C_NOTIF", sDesCentro);
        hashNotifEDO.put("IT_COBERTURA", sCobertura);
        hashNotifEDO.put("FC_FECNOTIF", getFechaNotif());
        hashNotifEDO.put("FC_RECEP", getFechaRecep());

        modoOperacion = modoBOTONES;
        Inicializar();

      }
      else {
        RecuperaFechaDeSemana(txtSemanaEpi.getText().trim());
        txtCodEquipo.setText("");
        sCodEquipoIni = "";
        listaOpeUltactTabla = new CLista();
        txtDesEquipo.setText("");
        txtCentro.setText("");
        modoOperacion = modoCABECERA;
        Inicializar();
      }

    }
    catch (Exception e) {
      txtSemanaEpi.setText(sCodSemIniBk);
      //AIC
      txtSemanaEpi.setText(getSemana());
    }

  }

//mlm: Trae datos de notif_sem, despues de Generar Cobertura
  public int Datos_Notif_Sem() {
    int modo = modoOperacion; //alta

    modoOperacion = modoESPERA;
    Inicializar();

    CLista data = new CLista(); //resultado
    DataListaNotifEDO dataNotif = new DataListaNotifEDO(); //resultado
    DataListaNotifEDO dataEDO = new DataListaNotifEDO(); //entrada

    dataEDO.sCodEquipo = getCodEquipo();
    dataEDO.sAnno = getAnno();
    dataEDO.sSemana = getSemana();
    try {

      parametros = new CLista();
      parametros.addElement(dataEDO);
      parametros.setLogin(app.getLogin());

      data = Comunicador.Communicate(getApp(),
                                     stubCliente,
                                     servletSELECCION_DATOS_NOTIF_SEM,
                                     strSERVLET_NOTIFEDO,
                                     parametros);

      //existen datos DEBE EXISTIR!!!!
      if (data.size() != 0) {

        dataNotif = (DataListaNotifEDO) data.firstElement();

        //cargar las variables (se trata de equipo en Cobertura)
        String sNotifReales = "0";
        String sNotifTeor = "0";
        String sTotNotifReales = "0";

        // notificadores te�ricos
        if (dataNotif.getNotifTeor() != null) {
          sNotifTeor = dataNotif.getNotifTeor();
        }
        txtNotifTeor.setText(sNotifTeor);
        sNotifTeorPrim = sNotifTeor;
        sNotifTeorAnt = sNotifTeor;

        // total notificadores reales
        if (dataNotif.getTotNotifReales() != null) {
          sTotNotifReales = dataNotif.getTotNotifReales();
        }
        txtTotNotifReales.setText(sTotNotifReales);
        sTotNotifRealesPrim = sTotNotifReales;
        sTotNotifRealesAnt = sTotNotifReales;

        if (!sTotNotifRealesPrim.equals("")) {
          iTotNotifRealesAnt = (new Integer(sTotNotifRealesPrim)).intValue();

          // notificadores reales
        }
        sNotifReales = "0";
        txtNotifReales.setText(sNotifReales);
        sNotifRealesPrim = sNotifReales;
        sNotifRealesAnt = sNotifReales;

        // inicia los anteriores
        sTeorAntes = sNotifTeorPrim;
        sRealesAntes = sNotifRealesPrim;

        if (!sNotifRealesPrim.equals("")) {
          iNotifRealesAnt = (new Integer(sNotifRealesPrim)).intValue();

        }
        ActCobertura();

        //cargar listaopeultacttabla   NOTIF SEM
        DataOpeFc datosnotifsem;
        Vector vnotifsem = new Vector();
        DataOpeFc datavnotifsem = null;
        datavnotifsem = new DataOpeFc("CD_E_NOTIF", sCodEquipo, "C");
        vnotifsem.addElement(datavnotifsem);
        datavnotifsem = new DataOpeFc("CD_ANOEPI", getAnno(), "C");
        vnotifsem.addElement(datavnotifsem);
        datavnotifsem = new DataOpeFc("CD_SEMEPI", getSemana(), "C");
        vnotifsem.addElement(datavnotifsem);

        datosnotifsem = new DataOpeFc(dataNotif.getCD_OPE_NOTIF_SEM(),
                                      dataNotif.getFC_ULTACT_NOTIF_SEM(), //HH:mm:SSS
                                      "SIVE_NOTIF_SEM", vnotifsem);

        listaOpeUltactTabla.addElement(datosnotifsem);
        //-------------------------

      }
      //sin datos
      else {
        ShowWarning(resBun.getString("msg70.Text"));
        modo = modoCABECERAYBOTONES;
        //no carga la lista
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      ShowWarning(resBun.getString("msg71.Text"));
      modo = modoCABECERAYBOTONES;
      //no carga la lista
      return (modo);
    }

    return (modo);

  } //fin traerdatosnotif_sem

// Si la semana actual es v�lida y no est� generada, lanza el
// proceso que se encarga de generar todas las semanas que falten
// en sive_notif_sem hasta la semana seleccionada
  private void GeneradorDeCobertura() {
    CLista datos;
    try {
      datos = new CLista();
      DataCN datacn = new DataCN("", "", "", "", "", "", "",
                                 "", "", "", "", "S", "",
                                 this.app.getLogin(), "N",
                                 this.getAnno(), this.getSemana());
      datos.addElement(datacn);

      Comunicador.Communicate(this.getApp(), stubCliente,
                              servletGENERA_COBERTURA,
                              strSERVLET_CN, datos);

    }
    catch (Exception e) {
      ;
    }
  } // fin m�todo GeneradorDeCobertura()

  void txtCodEquipo_focusLost() {

    String fechaNotifEquipo = getFechaNotifEquipo();

    // si el equipo no ha cambiado, no se hace nada
    if (getCodEquipo().equals(sCodEquipoIni)) {
      return;
    }

    // si se ha modificado, se inician campos desc. equipo
    // y centro
    txtDesEquipo.setText("");
    txtCentro.setText("");
    sCodEquipoIni = "";

    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    // si los datos de cabecera est�n rellenos, habilitar resto
    if (isCabFull()) {

      // se comprueba que los datos introducidos sean correctos
      // (comprueba el c�digo del equipo, y nos traemos desc., etc)
      // se actualizan datos en la hashtable NotifEDO
      if (isDataValid()) {
        modo = modoBOTONES;

        //AIC

        if (equiposNotificadores == null) {
          CListaEquipos lista = null;
          // Madrid 26-03-01 (ARS)
          //if (!sLaComunidad.equals("M")) {

          if (!hayZonaBasicaSalud()) {
            lista = new CListaEquipos(this,
                                      resBun.getString("msg53.Text"),
                                      stubCliente,
                                      strSERVLET_MAESTRO, //JRM2 CAMBIA por *_NOTIF
                                      this.getApp().getParameter("ORIGEN"),
                                      servletOBTENER_EQUIPO_X_CODIGO_NOTIF,
                                      servletOBTENER_EQUIPO_X_DESCRIPCION_NOTIF,
                                      servletSELECCION_EQUIPO_X_CODIGO_NOTIF,
                servletSELECCION_EQUIPO_X_DESCRIPCION_NOTIF,
                fechaNotifEquipo);
          }
          else {
            lista = new CListaEquipos(this,
                                      resBun.getString("msg53.Text"),
                                      stubCliente,
                                      strSERVLET_MAESTRO,
                                      this.getApp().getParameter("ORIGEN"),
                                      servletOBTENER_EQUIPO_X_CODIGO_CON_ZBS,
                servletOBTENER_EQUIPO_X_DESCRIPCION_CON_ZBS,
                servletSELECCION_EQUIPO_X_CODIGO_CON_ZBS,
                servletSELECCION_EQUIPO_X_DESCRIPCION_CON_ZBS,
                fechaNotifEquipo);
            //}

          }
          if ( (lista != null) && (lista.getComponente() != null)) {
            // Creamos el objeto con los datos de los notificadores.
            equiposNotificadores = new ENotificadores(lista.getLista(),
                txtCodEquipo.getText());
          }
        }
        else {
          equiposNotificadores.marcarSeleccionado(txtCodEquipo.getText());
        }

        String desEquipo = txtDesEquipo.getText();
        int longitudDes = desEquipo.length();
        //// System_out.println ("Longitud : " + longitudDes );
        //// System_out.println ("El ultimo caracter es :" + data.getDes().charAt (longitudDes -1) );

            /* // ARG: Ahora el servlet es el que filtra los notificadores dados de baja
                   if (longitudDes > 2 )
                   {
          if (desEquipo.charAt (longitudDes -1) == ')' &&
              desEquipo.charAt (longitudDes -2) == 'B' &&
              desEquipo.charAt (longitudDes -3) == '(' )
          {
             // System_out.println ("Efectivamente esta de baja, cambiamos el modo");
             modo = modoCONSULTA;
          }//Fin de si esta de BAJA
                   } */

        ActDatosHash();

      }
      else {
        //mlm entra si el cod equipo no vale!!, pero deja modocabecera
        // se ha intentado modificar y se deja en modo CABECERA

        modo = modoCABECERA;
      }
    } //-----------iscabfull
    else {
      modo = modoCABECERA;
    }

    modoOperacion = modo;
    Inicializar();
  } //fin de lostfocus de cod equipo

  void txtNotifReales_textValueChanged(TextEvent e) {
    sRealesDespues = txtNotifReales.getText();

    if (sRealesDespues.equals("")) {
      sRealesDespues = "0";
    }
    if (txtTotNotifReales.getText().trim().equals("")) {
      txtTotNotifReales.setText("0");
    }
    if (txtNotifTeor.getText().trim().equals("")) {
      txtNotifTeor.setText("0");

      //calculamos total reales
    }
    int total = 0;
    //relaes+ base de datos
    total = (new Integer(sRealesDespues)).intValue() +
        (new Integer(sNotifRealesPrim.trim())).intValue();

    //si mayor que teorico (pantalla) volver a anterior de Reales
    int iTeor = (new Integer(txtNotifTeor.getText().trim())).intValue();
    if (total > iTeor) {
      bCambiarReales = true;
      return;
    }

    bCambiarReales = false;

  } //fin change

  protected boolean bFocusLost = true;
  protected void CfechaRecep_focusGanied(FocusEvent e) {
    bFocusLost = false;
  }

  protected void CfechaRecep_focusLost(FocusEvent e) {

    synchronized (this) {
      if (bFocusLost) {
        // ya se ha ejecutado
        return;
      }
      else {
        // ya lo ejecutamos nosotros y quitamos el resto
        bFocusLost = true;
      }
    }

    CfechaRecep.ValidarFecha(); //validamos el dato
    if (CfechaRecep.getValid().equals("N")) {
      CfechaRecep.setText(CfechaRecep.getFecha());
    }
    else if (CfechaRecep.getValid().equals("S")) {
      CfechaRecep.setText(CfechaRecep.getFecha());
    }

    //ver si acepta vacios!!!MLM HACER!!!
    // la fecha de recepci�n no puede ser mayor que la fecha actual
    if (UtilEDO.fecha1MayorqueFecha2(getFechaRecep(), getFechaAct())) {
      ShowWarning(resBun.getString("msg72.Text"));
      ActCFecha(CfechaRecep, getFechaAct());
      bFocusLost = false;
      //mlm
      if (sLaComunidad.equals("M")) {
        hashNotifEDO.put("FC_FECNOTIF", CfechaNotif.getText().trim());
      }
      else {
        hashNotifEDO.put("FC_FECNOTIF", CfechaFinNotif.getText().trim());
      }
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());
      return;
    }

    // la fecha de notificaci�n no puede ser mayor que la de recepci�n
    if (UtilEDO.fecha1MayorqueFecha2(getFechaNotif(), getFechaRecep())) {
      ShowWarning(resBun.getString("msg73.Text"));

      // hay que tener cuidado, y comprobar antes que la fecha de notificaci�n
      // no sea superior a la fecha actual (por ahber seleccionado semana)

      // si la fecha de notificaci�n es mayor que la actual, se actualizan
      // tanto la fecha de notificaci�n como la de recepci�n a la actual

      if (UtilEDO.fecha1MayorqueFecha2(getFechaNotif(), getFechaAct())) {
        ActCFecha(CfechaRecep, getFechaAct());
        if (sLaComunidad.equals("M")) {
          ActCFecha(CfechaNotif, getFechaAct());
        }
        else {
          ActCFecha(CfechaFinNotif, CfechaIniNotif, getFechaAct());
        }
      }
      // si la fecha de notificaci�n es menor o igual a la fecha actual,
      // se actualiza s�lo la de recepci�n con la fecha de notificaci�n

      else {
        ActCFecha(CfechaRecep, getFechaNotif());
      }

      //mlm
      if (sLaComunidad.equals("M")) {
        hashNotifEDO.put("FC_FECNOTIF", CfechaNotif.getText().trim());
      }
      else {
        hashNotifEDO.put("FC_FECNOTIF", CfechaFinNotif.getText().trim());
      }
      hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());

      return;
    }
    //mlm
    if (sLaComunidad.equals("M")) {
      hashNotifEDO.put("FC_FECNOTIF", CfechaNotif.getText().trim());
    }
    else {
      hashNotifEDO.put("FC_FECNOTIF", CfechaFinNotif.getText().trim());

    }
    hashNotifEDO.put("FC_RECEP", CfechaRecep.getText().trim());

  } //fin de funcion

  protected boolean bFocoNotifReales = false;
  public void txtNotifReales_focusLost() {

    if (bFocoNotifReales) {
      return;
    }

    bFocoNotifReales = true;

    try {
      int iNotifTeor = Integer.parseInt(txtNotifTeor.getText().trim());
      int iNotifReales = Integer.parseInt(txtNotifReales.getText().trim());
      int iTotNotifReales = 0;

      if (iNotifReales < 0) {
        iNotifReales = 0;
        txtNotifReales.setText("0");
      }

      if (sRealesAntes != null &&
          sRealesAntes.equals(txtNotifReales.getText().trim())) {
        // no ha cambiado el n�mero
        bFocoNotifReales = false;
        return;
      }

      // se ha cambiado el n�mero de notificadores reales
      if ( (sRealesAntes != null) && (Integer.parseInt(sRealesAntes) > 0)) {
        iTotNotifReales = Integer.parseInt(txtTotNotifReales.getText()) -
            Integer.parseInt(sRealesAntes);
      }
      else {
        iTotNotifReales = Integer.parseInt(txtTotNotifReales.getText());

      }
      if (iNotifReales < 0 || iNotifTeor < iTotNotifReales + iNotifReales) {
        // Variable para comprobar si es correcto el n�mero de notificaciones
        // ARS 28-06-01
        bNotifRealesBien = false;
        /// deshacemos el cambio
        ShowWarning(resBun.getString("msg74.Text"));
        txtNotifReales.setText(sRealesAntes);
      }
      else {
        // Variable para comprobar si es correcto el n�mero de notificaciones
        // ARS 28-06-01
        bNotifRealesBien = true;
        /// calculamos el total y la cobertura
        iTotNotifReales += iNotifReales;
        txtTotNotifReales.setText(Integer.toString(iTotNotifReales));
        ActCobertura();
        sRealesAntes = txtNotifReales.getText();
      }
    }
    catch (Exception exc) {
      txtNotifReales.setText(sRealesAntes);
    }
    bFocoNotifReales = false;
  }

  private void ActTotNotifReales() {

    String sNotifReales = txtNotifReales.getText().trim();
    if (sNotifReales.equals("")) {
      sNotifReales = "0";
    }
    String sTotNotifReales = txtTotNotifReales.getText().trim();
    if (sTotNotifReales.equals("")) {
      sTotNotifReales = "0";

    }

    try {
      int iNotifReales = (new Integer(sNotifReales)).intValue();
      int iTotNotifReales = (new Integer(sTotNotifReales)).intValue();
      if (iNotifReales >= 0) {
        iTotNotifReales = iTotNotifReales + (iNotifReales - iNotifRealesAnt);
        txtTotNotifReales.setText( (new Integer(iTotNotifReales)).toString());

        iNotifRealesAnt = iNotifReales;
        ActCobertura();
      }
      else {
        txtNotifReales.setText( (new Integer(iNotifRealesAnt)).toString());
      }
    }
    catch (Exception e) {
      ;
    }
  }

  /**
   *   recibe la p�rdida de foco de notificadores te�ricos
   *   si es negativo omenor que la suma de los reales
   *   no se permite el cambio
   */
  protected boolean bFocoNotifTeor = false;
  ButtonControl buttonControl1 = new ButtonControl();
  public void txtNotifTeor_focusLost() {

    if (bFocoNotifTeor) {
      return;
    }

    bFocoNotifTeor = true;

    try {
      int iNotifTeor = Integer.parseInt(txtNotifTeor.getText().trim());
      int iTotNotifReales = Integer.parseInt(txtTotNotifReales.getText().trim());
      ;

      if (iNotifTeor < 0) {
        iNotifTeor = 0;
        txtNotifTeor.setText("0");
      }

      // comprobamos que ha cambiado el n�mero
      if (sTeorAntes != null && sTeorAntes.equals(txtNotifTeor.getText().trim())) {
        // no ha cambiado el n�mero
        bFocoNotifTeor = false;
        return;
      }

      if (iNotifTeor < 0 || iNotifTeor < iTotNotifReales) {
        /// deshacemos el cambio
        ShowWarning(resBun.getString("msg75.Text"));
        txtNotifTeor.setText(sTeorAntes);
      }
      else {
        ActCobertura();
        sTeorAntes = txtNotifTeor.getText();
      }
    }
    catch (Exception exc) {
      txtNotifTeor.setText(sTeorAntes);
    }
    bFocoNotifTeor = false;
  }

  void txtNotifTeor_textValueChanged(TextEvent e) {
    bNotifTeorChanged = true; //Nuria

    sTeorDespues = txtNotifTeor.getText();

    if (sTeorDespues.equals("")) {
      sTeorDespues = "0";
    }
    if (txtNotifTeor.getText().trim().equals("")) {
      txtNotifTeor.setText("0");
    }
    if (txtTotNotifReales.getText().trim().equals("")) {
      txtTotNotifReales.setText("0");

      //si teorico menor que total reales->mal
    }
    int iTeor = (new Integer(txtNotifTeor.getText().trim())).intValue();
    int totalReales = (new Integer(txtTotNotifReales.getText().trim())).
        intValue();
    if (iTeor < totalReales) {
      bCambiarTeor = true;
      return;
    }

    bCambiarTeor = false;

  } //fin change

  /**
   * Actualiza el c�digo de �rea.
   */
  public void setCodNivel1(String codNivel1) {
    sCodNivel1 = codNivel1;
  }

  /**
   * Actualiza el c�digo de distrito.
   */
  public void setCodNivel2(String codNivel2) {
    sCodNivel2 = codNivel2;
  }

} //FIN DE CLASE

/*
 *  Autor        Fecha             Accion
 *   JRM         23/06/2000        Modifica para poder devolver
 *                                 la lista de datos de los equipos.
 */
class CListaEquipos
    extends CListaValores {

  protected PanelMaestroEDO panel;
  private String sComunidad = "M";

  private String fechaNotificacion = "";

  /**
   * Acceso a los datos de los equipos notificadores.
   * @return Una lista con los datos de los equipos. Es de la clase base.
   */
  public CLista getLista() {
    return lista;
  }

  public CListaEquipos(PanelMaestroEDO p,
                       String title,
                       StubSrvBD stub,
                       String servlet,
                       String sComunidad,
                       int obtener_x_codigo,
                       int obtener_x_descripcion,
                       int seleccion_x_codigo,
                       int seleccion_x_descripcion,
                       String fechaNotificacion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    this.sComunidad = sComunidad;
    this.fechaNotificacion = fechaNotificacion;
    btnSearch_actionPerformed();
  }

  // Constructor para el modo Madrid (26-03-01 ARS)
  public CListaEquipos(PanelMaestroEDO p,
                       String title,
                       StubSrvBD stub,
                       String servlet,
                       String sComunidad,
                       int obtener_x_codigo,
                       int obtener_x_descripcion,
                       int seleccion_x_codigo,
                       int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    this.sComunidad = sComunidad;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    /*
         // Para Madrid
         if (sComunidad.equals("M"))
      return new DataEntradaEDO(s,
                               (String)panel.hashNotifEDO.get("CD_ANOEPI"),
                                panel.txtSemanaEpi.getText().trim(),
                                panel.sCodNivel1,
                                panel.sCodNivel2);
         else
      return new DataEntradaEDO(s,
                               (String)panel.hashNotifEDO.get("CD_ANOEPI"),
                                panel.txtSemanaEpi.getText().trim(),
                                panel.sCodNivel1,
                                panel.sCodNivel2,
                                panel.getFechaNotif());
     */
    return new DataEntradaEDO(s,
                              (String) panel.hashNotifEDO.get("CD_ANOEPI"),
                              panel.txtSemanaEpi.getText().trim(),
                              panel.sCodNivel1,
                              panel.sCodNivel2,
                              fechaNotificacion);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

class PanelMaestroEDOactionAdapter
    implements java.awt.event.ActionListener, Runnable {
//class PanelMaestroEDOactionAdapter implements java.awt.event.ActionListener {
  PanelMaestroEDO adaptee;
  ActionEvent evt;

  PanelMaestroEDOactionAdapter(PanelMaestroEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    //public void actionPerformed(ActionEvent evt) {
    if (evt.getActionCommand().equals("equipo")) {
      adaptee.btnEquipo_actionPerformed();
    }
    else if (evt.getActionCommand().equals("salir")) {
      adaptee.btnSalir_actionPerformed();
    }
    else if (evt.getActionCommand().equals("alta")) {
      adaptee.btnAlta_actionPerformed();
    }
    else if (evt.getActionCommand().equals("baja")) {
      adaptee.btnBaja_actionPerformed();
    }
    else if (evt.getActionCommand().equals("modificacion")) {
      adaptee.btnModificacion_actionPerformed();
    }
    else if (evt.getActionCommand().equals("notificacion")) {
      adaptee.btnNotif_actionPerformed();
    }
    else if (evt.getActionCommand().equals("validar")) {
      adaptee.btnValidar_actionPerformed();
    }
  }
}

class PanelMaestroEDOfocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanelMaestroEDO adaptee;
  FocusEvent evt;

  PanelMaestroEDOfocusAdapter(PanelMaestroEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    if ( ( (TextField) evt.getSource()).getName().equals("notifreales")) {
      adaptee.txtNotifReales_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("notifteor")) {
      adaptee.txtNotifTeor_focusLost();
    }
    else {
      run();
    }
  }

  public void focusGained(FocusEvent e) {
  }

  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("equipo")) {
      adaptee.txtCodEquipo_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("semanaepi")) {
      adaptee.txtSemanaEpi_focusLost();
    }
  }
}
