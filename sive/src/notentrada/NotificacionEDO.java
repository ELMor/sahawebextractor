/*
 *  Autor           Fecha               Acci�n
 *   JRM            09/06/2000          Modificaci�n notificaci�n
 *                                      En principio este applet mostraba
 *                                      un panel con selecci�n de �rea
 *                                      distrito y a�o.Ahora este panel
 *                                      se ha eliminado y dicha selecci�n
 *                                      se ha llevado al applet de notificaciones.
 *                                      En la selecci�n de distrito de permite
 *                                      seleccionar mediante constante si se quiere
 *                                      introducir la zona b�sica de salud (a
 *                                      petici�n de Castilla Le�n)
 *                                      Se han comentado todas las referencias
 *                                      a pEntradaEDO.
 */

package notentrada;

import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;

public class NotificacionEDO
    extends CApp
    implements ItemListener, ActionListener {

  /**
   * Ret�cula para colocar los controles
   */
  XYLayout xYLayout = new XYLayout();

  CApp a;
  ResourceBundle res;
  boolean bEntrada = false;

  // hastable de datos comunes a todo el m�dulo de notificaciones EDO
  protected Hashtable hashNotifEDO = null;

  //PanelListaNotifEDO pListaNotifEDO;
  PanelListaEDONum pListaEDONum;
  PanelListaEDOIndiv pListaEDOIndiv;
  PanelMaestroEDO pMaestroEDO;
  // JRM: Desaparece panel de entrada EDO
  //PanelEntradaEDO pEntradaEDO;

  //JRM: Panel para la selecci�n de �rea, distrito (ZBS) y a�o.
  PanelArDiAn pArDiAn;

  /**
   * Devoluci�n de qui�n es el panel maestro
   * @return Devuelve el panel maestro del applet.
   */
  public PanelMaestroEDO getPanelMaestro() {
    return pMaestroEDO;
  }

  /**
   * Devoluci�n de qui�n es el panel de �rea, distrito y a�o.
   * @return Devuelve el panel maestro del applet.
   */
  public PanelArDiAn getPanelArDiAn() {
    return pArDiAn;
  }

  public void init() {
    super.init();
    this.setBackground(Color.lightGray);
    hashNotifEDO = new Hashtable();
    bEntrada = false;
  }

  public void itemStateChanged(ItemEvent e) {

    //SI 2: DESELECCIONADO, 1:SELECCIONADO
    pListaEDONum.prueba(e.getStateChange());
    pListaEDOIndiv.prueba(e.getStateChange());
  }

  public void actionPerformed(ActionEvent e) {
    this.pMaestroEDO.CfechaRecep_focusLost(null);
    this.pMaestroEDO.txtNotifTeor_focusLost();
    this.pMaestroEDO.txtNotifReales_focusLost();

    if (e.getActionCommand().equals("cancelar")) {
      pListaEDONum.btnCancelar_actionPerformed();
      pListaEDOIndiv.btnCancelar_actionPerformed();
    }
  }

  public void start() {
    res = ResourceBundle.getBundle("notentrada.Res" + this.getIdioma());
    // si no se ha entrado todav�a, se ejecuta el start
    // (previsi�n contra navegadores)
    if (!bEntrada) {
      setTitulo(res.getString("NotificacionEDO.Title"));
      a = (CApp)this;

      pListaEDONum = new PanelListaEDONum(this, hashNotifEDO);
      pListaEDOIndiv = new PanelListaEDOIndiv(this, hashNotifEDO);

      VerEntradaEDO();

      bEntrada = true;
    }
  }

  public void VerEntradaEDO() {
    if (pMaestroEDO != null) {
      remove(pMaestroEDO);

      // JRM quita pegote
      //pegote!! perfil 5 -----------------
      /*
           if (this.getIT_MODULO_3().trim().equals("S") &&
          this.getPerfil() == 5 )
          {
            DataListaEDONum data = new DataListaEDONum();
            DialogAnno dlg = new DialogAnno (this, "");
            dlg.show();
            data = (DataListaEDONum) dlg.getComponente();
            if (data != null) {
              hashNotifEDO.put("CD_ANOEPI", (String)data.getAnno());
            }
            dlg = null;
           // Actualiza datos en la hashtable NotifEDO
          hashNotifEDO.put("CD_NIVEL_1", this.getCD_NIVEL_1_EQ().trim());
          hashNotifEDO.put("DS_NIVEL_1", this.getDS_NIVEL_1_EQ().trim());
          if (!this.getCD_NIVEL_2_EQ().trim().equals("")) {
            hashNotifEDO.put("CD_NIVEL_2", this.getCD_NIVEL_2_EQ().trim());
            hashNotifEDO.put("DS_NIVEL_2", this.getCD_NIVEL_2_EQ().trim());
          }
          VerNotificaciones();
          return;
           }//-----------------  -----------------
       */

      /* JRM: quitamos panel de entrada con �rea, distrito y a�o.
       * pEntradaEDO = new PanelEntradaEDO(this, hashNotifEDO);
       * VerPanel("entrada", pEntradaEDO);
       */
    }
    VerNotificaciones();
    validate();
  }

  /*
   *  Autor           Fecha               Accion
   *   JRM            12/06/2000          Mofica para adaptarlo al nuevo
   *                                      filtro.
   */
  public void VerNotificaciones() {
    // JRM: Dimensiones
    int yArDiAn = 30; // 30 comienzo en y
    int yMaestro = yArDiAn + 80;
    //AIC
    //int yTabPanel  = yMaestro  +  167;
    int yTabPanel = yMaestro + 157;

    //JRM: Cambiamos el borderlayout del padre a un XYLayout
    super.setLayout(xYLayout);

    // JRM: El panel de t�tulo tambi�n es ajustado.
    super.add(pnlTitulo, new XYConstraints(0, 0, 700, -1));

    pMaestroEDO = new PanelMaestroEDO(this, hashNotifEDO);
    pArDiAn = new PanelArDiAn(this, hashNotifEDO);

    // JRM: Hacemos que el panel maestro conozca al panel �rea, distrito y a�o.
    pMaestroEDO.setPanelArDiAn(pArDiAn);
    ( (PanelListaEDO) pListaEDONum).setPanelArDiAn(pArDiAn);
    ( (PanelListaEDO) pListaEDOIndiv).setPanelArDiAn(pArDiAn);

    tabPanel.setVisible(false);
    VerPanel(res.getString("msg11.Text"), pListaEDONum, true);
    VerPanel(res.getString("msg12.Text"), pListaEDOIndiv, true);
    VerPanel(res.getString("msg11.Text"));

    // Colocamos los paneles de notificaciones num�ricas e individuales.
    //AIC
    //super.add(tabPanel, new XYConstraints(10, yTabPanel, -1, -1));
    //tabPanel.setSize(-1,180);
    super.add(tabPanel, new XYConstraints(10, yTabPanel, -1, 210));

    tabPanel.setVisible(true);

    /*  JRM: Desaparece panel de entrada EDO
         if (pEntradaEDO != null) {
      BorrarPanel("entrada");
      pEntradaEDO = null;
         }
     */
    pListaEDONum.InitPanel();
    pListaEDOIndiv.InitPanel();

    // JRM: Quitamos antiguas referencias a BordersLayout.
    //add("East", new PanelLat());
    //add("West", new PanelLat());
    //addPanelSuperior(pMaestroEDO);

    // JRM: Colocamos el panel maestro
    super.add(pMaestroEDO, new XYConstraints(10, yMaestro, -1, -1));

    // JRM: Mostramos panel de area,distrito y a�o
    super.add(pArDiAn, new XYConstraints(10, yArDiAn, -1, -1));
    validate();
  }

} // END CLASS Applet

class PanelLat
    extends Panel {
  XYLayout xYLayout = new XYLayout();
  void Panel() {
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    xYLayout.setWidth(3);
    xYLayout.setHeight(400);
    this.setLayout(xYLayout);
    setSize(3, 400);
  }

} ///  END class
