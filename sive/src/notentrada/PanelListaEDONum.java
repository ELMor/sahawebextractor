package notentrada;

import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Insets;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.util.JCVector;
import notadic.dialogoNotAdic;
import notadic.parNotAdic;
import notdata.DataListaEDOIndiv;
import notdata.DataListaEDONum;
import notdata.DataListaNotifEDO;
import notifnum.DialNum;
import notutil.Comunicador;
import sapp.StubSrvBD;

public class PanelListaEDONum
    extends PanelListaEDO {

  PanelMaestroEDO MaestroEDO;

  protected boolean bListaNumVacia = true;
  ResourceBundle res;

  protected CLista listaEDONUMBD = new CLista();
  protected JCItemEvent evento = null;

  // modos de operaci�n del panel
  final int modoNORMAL = 0; //residuo
  final int modoINICIO = 1;
  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  // modos de operaci�n del servlet SrvEDONum
  final int servletSELECCION_LISTA_EDO_NUM = 3;
  final int servletSELECCION_LISTA_EDO_NUM_FECHA = 8;
  final int servletUPDATE_LISTA_EDO_NUM = 4;
  final int servletINSERT_LISTA_EDO_NUM = 5;
  final int servletDELETE_LISTA_EDO_NUM = 6;
  final int servletSELECCION_LISTA_ENFER = 7;

  // modos de operaci�n del servlet SrvNotifEDO
  final int servletINSERT_LISTA_NOTIF = 5;

  // modos de operaci�n del servlet SrvNumerica
  final int servletINSERT_EDO_NUMERICA = 0;
  final int servletUPDATE_EDO_NUMERICA = 1;

  // modos de actualizaci�n de la lista
  final int modoADD = 20;
  final int modoMODIFY = 21;
  final int modoDELETE = 22;

  // modos de operaci�n del panelmaestro
  final int modoCABECERA = 13;
  final int modoBOTONES = 14;
  final int modoCABECERAYBOTONES = 15;
  final int modoCONSULTAYBOTONES = 16;

  // constantes del panel
  final String strSERVLET_EDONUM = "servlet/SrvEDONum";

  // par�metros
  NotificacionEDO applet;
  JCVector itemsSemana = new JCVector();
  CLista listaTblSemana = new CLista();
  JCVector itemsDia = new JCVector();
  CLista listaTblDia = new CLista();
  StubSrvBD stubCliente;
  protected Hashtable hashNotifEDO = null;
  protected CLista parametros = null, result = null;

  final String imgNAME_PARTES[] = {
      "images/partes.gif"};

  // controles
  XYLayout xYLayout1 = new XYLayout();

  // constructor
  public PanelListaEDONum(NotificacionEDO a, Hashtable hashNotifEDO) {
    super( (CApp) a);
    try {
      setApp( (CApp) a);
      res = ResourceBundle.getBundle("notentrada.Res" + a.getIdioma());
      applet = a;
      this.hashNotifEDO = hashNotifEDO;
      jbInit();
      stubCliente = new StubSrvBD(new URL(app.getURL() + strSERVLET_EDONUM));

      super.modoOperacion = modoINICIO;
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnPartes.setImage(imgs.getImage(0));
    btnPartes.addActionListener(actionAdapter);
    btnPartes.setLabel(res.getString("Partes"));
    btnPartes.setActionCommand("partes");

    tblSemana.getList().setBackground(Color.white);
    tblSemana.getList().setHighlightColor(Color.lightGray);
    tblSemana.setInsets(new Insets(5, 5, 5, 5));
    /*tblSemana.setColumnButtons(jclass.util.JCUtilConverter.toStringList(new String("Enfermedad\nCasos\nF. Notif.\nF. Recep."), '\n'));
         tblSemana.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
         tblSemana.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String("290\n50\n90\n90"), '\n'));
         tblSemana.setNumColumns(4);
         tblSemana.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
         tblSemana.setAutoSelect(true);
         this.add(tblSemana, new XYConstraints(10, 10, 550, 100));*/

    tblSemana.setColumnButtons(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("Enfermedad_nCasos")), '\n'));
    tblSemana.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    tblSemana.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "240\n48\n48\n80\n80"), '\n'));
    tblSemana.setNumColumns(5);
    tblSemana.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    tblSemana.setAutoSelect(true);
    this.add(tblSemana, new XYConstraints(3, 10, 526, 100));

    this.add(btnPartes, new XYConstraints(529, 60, 100, -1));

    tblDia.getList().setBackground(Color.white);
    tblDia.getList().setHighlightColor(Color.lightGray);
    tblDia.setInsets(new Insets(5, 5, 5, 5));

    tblDia.setColumnButtons(jclass.util.JCUtilConverter.toStringList(new String(
        res.getString("Enfermedad_nCasos1")), '\n'));
    tblDia.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    tblDia.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "290\n50\n50"), '\n'));
    tblDia.setNumColumns(3);
    tblDia.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    tblDia.setAutoSelect(true);
    this.add(tblDia, new XYConstraints(100, 10, 420, 100));

    btnPartes.setVisible(false);
    btnPartes.setEnabled(false);
    btnPartes.addActionListener(actionAdapter);
  }

  public void InitPanel() {

    super.InitPanel();

    if (itemsSemana.size() > 0) {
      itemsSemana.removeAllElements();
    }
    if (listaTblSemana.size() > 0) {
      listaTblSemana.removeAllElements();

    }
    if (itemsDia.size() > 0) {
      itemsDia.removeAllElements();
    }
    if (listaTblDia.size() > 0) {
      listaTblDia.removeAllElements();
    }
  }

  public void actualizar(int modo) {
    super.Actualizar_valores_validar(modo);
  }

  public void Cargar_Validar(DataListaNotifEDO datos) {
    super.Cargar_Validar(datos);
  }

  public void Inicializar(int modo) {
    //partes: Respecto a btnPartes
    switch (modo) {
      case modoNORMAL:
      case modoINICIO:
        bListaNumVacia = true;
        listaEDONUMBD = new CLista();

        btnPartes.setVisible(false);
        super.chkValidar.setVisible(false);
        super.lblValidar.setVisible(false);
        super.lblEtiquetaValidar.setVisible(false);
        super.IT_VALIDADA_PRIM = "";
        super.FC_FECVALID_PRIM = "";
        break;
      case modoALTA:
        btnPartes.setVisible(false);
        break;

      case modoESPERA:
        btnPartes.setEnabled(false);
        break;
    }

    super.modoOperacion = modo;
    Inicializar();

    if ( (modo == modoMODIFICACION ||
          modo == modoBAJA ||
          modo == modoCONSULTA)
        && tblDia.countItems() != 0) {
      btnPartes.setVisible(true);
      btnPartes.setEnabled(false);
      if (evento != null) {
        tbl_item(evento);
      }
    }
    this.doLayout();
  }

  public boolean getbListaNumVacia() {
    return (bListaNumVacia);
  }

  public void RellenaLista(String codequipo,
                           String anno, String semana,
                           String fecharecep,
                           String fechanotif) {

    bListaNumVacia = true;
    CLista lista = new CLista();
    Object componente = new Object();

    DataListaEDONum dataNum = new DataListaEDONum();

    dataNum.sCodEquipo = codequipo;
    dataNum.sAnno = anno;
    dataNum.sSemana = semana;
    dataNum.sFechaNotif = fechanotif;
    dataNum.sFechaRecep = fecharecep;

    parametros = new CLista();
    parametros.addElement(dataNum);

    //lista de semana *****************
    if (itemsSemana.size() > 0) {
      itemsSemana.removeAllElements();
    }
    if (listaTblSemana.size() > 0) {
      listaTblSemana.removeAllElements();
    }
    tblSemana.clear();

    try {
      result = Comunicador.Communicate(getApp(),
                                       stubCliente,
                                       servletSELECCION_LISTA_EDO_NUM,
                                       strSERVLET_EDONUM,
                                       parametros);

      result.trimToSize();

      if (result.size() != 0) {
        // JRM2 Para ordenar alfab�ticamente las enfermedades
        Object componenteAux;
        Object componenteAux2;
        String primero;
        String siguiente;
        for (int i = 0; i < result.size() - 1; i++) { //Ordenacion de la lista por Burbuja
          for (int j = 0; j < (result.size() - 1) - i; j++) {
            primero = ( (DataListaEDONum) result.elementAt(j)).getDesEnfer();
            siguiente = ( (DataListaEDONum) result.elementAt(j + 1)).
                getDesEnfer();
            if (primero.compareTo(siguiente) > 0) { // Menor el siguiente
              componenteAux = (DataListaEDONum) result.elementAt(j); // Intercambio
              componenteAux2 = (DataListaEDONum) result.elementAt(j + 1);
              result.removeElementAt(j);
              result.insertElementAt(componenteAux2, j);
              result.removeElementAt(j + 1);
              result.insertElementAt(componenteAux, j + 1);
            }
          }
        }
        // JRM2 Para ordenar alfab�ticamente las enfermedades (fin)
        for (int i = 0; i < result.size(); i++) {
          componente = result.elementAt(i);
          addRow(modoADD, (DataListaEDONum) componente, -1);
        }
        tblSemana.setItems(itemsSemana);
      }

    }
    catch (Exception e) {
      ShowWarning(res.getString("msg26.Text"));
      btnCancelar_actionPerformed(); //lo deja en modo CABECERABOTONES
      return;
    }

    // Lista de d�a  ********************
    if (itemsDia.size() > 0) {
      itemsDia.removeAllElements();
    }
    if (listaTblDia.size() > 0) {
      listaTblDia.removeAllElements();
    }
    tblDia.clear();

    try {
      result = Comunicador.Communicate(getApp(),
                                       stubCliente,
                                       servletSELECCION_LISTA_EDO_NUM_FECHA,
                                       strSERVLET_EDONUM,
                                       parametros);

      result.trimToSize();
      if (result.size() != 0) {
        bListaNumVacia = false; //entrar� desde Modif-Borrar
        listaEDONUMBD = new CLista();
        for (int i = 0; i < result.size(); i++) {
          componente = result.elementAt(i);
          listaEDONUMBD.addElement( (DataListaEDONum) componente);
          addRowFecha(modoADD, (DataListaEDONum) componente, -1);
          tblDia.setItems(itemsDia);
        }
      }
    }
    catch (Exception e) {
      ShowWarning(res.getString("msg26.Text"));
      btnCancelar_actionPerformed(); //lo deja en modo CABECERABOTONES
      return;
    }

  }

  public void RellenaLista_Imcompleta(String codequipo,
                                      String anno, String semana,
                                      String fecharecep,
                                      String fechanotif,
                                      int modo) {

    bListaNumVacia = true;
    CLista lista = new CLista();
    Object componente = new Object();

    DataListaEDONum dataNum = new DataListaEDONum();

    dataNum.sCodEquipo = codequipo;
    dataNum.sAnno = anno;
    dataNum.sSemana = semana;
    dataNum.sFechaNotif = fechanotif;
    dataNum.sFechaRecep = fecharecep;

    parametros = new CLista();
    parametros.addElement(dataNum);

    switch (modo) {
      case modoALTA:

        //lista de semana *****************
        if (itemsSemana.size() > 0) {
          itemsSemana.removeAllElements();
        }
        if (listaTblSemana.size() > 0) {
          listaTblSemana.removeAllElements();
        }
        tblSemana.clear();

        try {
          result = Comunicador.Communicate(getApp(),
                                           stubCliente,
                                           servletSELECCION_LISTA_EDO_NUM,
                                           strSERVLET_EDONUM,
                                           parametros);

          result.trimToSize();

          if (result.size() != 0) {
            for (int i = 0; i < result.size(); i++) {
              componente = result.elementAt(i);
              addRow(modoADD, (DataListaEDONum) componente, -1);
            }
            tblSemana.setItems(itemsSemana);
          }

        }
        catch (Exception e) {
          ShowWarning(res.getString("msg26.Text"));
          btnCancelar_actionPerformed(); //lo deja en modo CABECERABOTONES
          return;
        }
        break;
      case modoMODIFICACION:

    } //switch

    // Lista de d�a  ********************
    if (listaTblDia.size() > 0) {
      bListaNumVacia = false; //entrar� desde Alta
      listaEDONUMBD = new CLista();

      for (int i = 0; i < listaTblDia.size(); i++) {
        componente = listaTblDia.elementAt(i);
        listaEDONUMBD.addElement( (DataListaEDONum) componente);
      }
    }
    else {
      bListaNumVacia = true;
      listaEDONUMBD = new CLista();
    }

  }

  //LISTA SEMANA************************************************
  public void addRow(int modo, DataListaEDONum data, int iSel) {

    String sCero = "0";
    JCVector row1 = new JCVector();
    row1.addElement(data.getDesEnfer());
    row1.addElement(data.getCasos());

    if (data.getVigi().equals("X")
        && !data.getPartes().equals("")) {
      row1.addElement(data.getPartes());
    }
    else if (data.getVigi().equals("X")
             && data.getPartes().equals("")) {
      row1.addElement(sCero);
      data.sPartes = "0";
    }

    else if (data.getVigi().equals("N")) {
      row1.addElement("  ");

    }
    else if (data.getVigi().equals("A")) { //leer de Indiv ******
      String partes = Buscar_Partes_Semana_A(data.getCodEnfer(),
                                             data.getFechaRecep(),
                                             data.getFechaNotif());
      row1.addElement(partes);
      data.sPartes = partes;
    }
    /*
     /if (data.getVigi().equals("A")){//leer de Indiv ******
       String partes = Buscar_Partes_Semana_A(data.getCodEnfer(),
                                              data.getFechaRecep(),
                                              data.getFechaNotif());
       row1.addElement(partes);
       data.sPartes = partes;
     }//fin de A ********
     */

    row1.addElement(data.getFechaNotif());
    row1.addElement(data.getFechaRecep());

    switch (modo) {
      case modoADD:
        itemsSemana.addElement(row1);
        listaTblSemana.addElement(data);
        break;

      case modoMODIFY:
        itemsSemana.setElementAt(row1, iSel);
        listaTblSemana.setElementAt(data, iSel);
        break;

      case modoDELETE:
        listaTblSemana.removeElementAt(iSel);
        itemsSemana.removeElementAt(iSel);
        break;
    }
  }

  public String Buscar_Partes_Dia_A(String cd_enfcie) {
    //busco el numero de lineas de esa enfermedad en dia-indiv
    int Contador = 0;

    for (int i = 0; i < applet.pListaEDOIndiv.listaTblDia.size(); i++) {
      DataListaEDOIndiv datos = (DataListaEDOIndiv) applet.pListaEDOIndiv.
          listaTblDia.elementAt(i);
      if (datos.getCodEnfermedad().trim().equals(cd_enfcie)) {
        Contador = Contador + 1;
      }
    }
    Integer IContador = new Integer(Contador);
    return (IContador.toString());
  } //buscar A

  public String Buscar_Partes_Semana_A(String cd_enfcie,
                                       String fc_recep,
                                       String fc_fecnotif) {
    //busco el numero de lineas de esa enfermedad en semana-indiv, para las fechas dadas
    int Contador = 0;

    for (int i = 0; i < applet.pListaEDOIndiv.listaTblSemana.size(); i++) {
      DataListaEDOIndiv datos = (DataListaEDOIndiv) applet.pListaEDOIndiv.
          listaTblSemana.elementAt(i);
      if (datos.getCodEnfermedad().trim().equals(cd_enfcie)
          && datos.getFechaRecep().trim().equals(fc_recep)
          && datos.getFechaNotif().trim().equals(fc_fecnotif)) {
        Contador = Contador + 1;
      }
    }
    Integer IContador = new Integer(Contador);
    return (IContador.toString());
  } //buscar A

  //LISTA DIA******************************************************
  public void addRowFecha(int modo, DataListaEDONum data, int iSel) {
    String sCero = "0";
    JCVector row1 = new JCVector();
    row1.addElement(data.getDesEnfer());
    row1.addElement(data.getCasos());

    if (data.getVigi().equals("X")
        && !data.getPartes().equals("")) {
      row1.addElement(data.getPartes());

    }
    else if (data.getVigi().equals("X")
             && data.getPartes().equals("")) {
      row1.addElement(sCero);
      data.sPartes = "0";
    }
    else if (data.getVigi().equals("N")) {
      row1.addElement("  ");

    }
    else if (data.getVigi().equals("A")) { //leer de Indiv ******
      String partes = Buscar_Partes_Dia_A(data.getCodEnfer());
      row1.addElement(partes);
      data.sPartes = partes;
    }

    row1.addElement(data.getFechaNotif());
    row1.addElement(data.getFechaRecep());

    /*
          /if (data.getVigi().equals("A")){//leer de Indiv ******
            String partes = Buscar_Partes_Dia_A(data.getCodEnfer());
            row1.addElement(partes);
            data.sPartes = partes;
          }//fin de A ********
     */
    switch (modo) {
      case modoADD:
        itemsDia.addElement(row1);
        listaTblDia.addElement(data);

        break;

      case modoMODIFY:
        itemsDia.setElementAt(row1, iSel);
        listaTblDia.setElementAt(data, iSel);
        break;

      case modoDELETE:
        listaTblDia.removeElementAt(iSel);
        itemsDia.removeElementAt(iSel);
        break;
    }
  }

//Encuentra el indice equivalente en Semana****************
  protected int Calcular_Indice_Semana(DataListaEDONum data) {
    //data contiene los datos actuales de dia incluso notif, recep (del modificado)
    //lista de datos de Semana (listaTblSemana)
    int iIndex = -1;
    boolean bEncontrado = false;
    DataListaEDONum datosLinea;
    datosLinea = new DataListaEDONum();

    for (int j = 0; j < listaTblSemana.size(); j++) {

      datosLinea = (DataListaEDONum) listaTblSemana.elementAt(j);

      if (data.getDesEnfer().trim().equals(datosLinea.getDesEnfer().trim())) {
        bEncontrado = true;
        iIndex = j + 1;
      }
    }
    return (iIndex);
  }

//true: Esta en la lista inicial (BD)
//false: No esa en la lista inicial (BD)
  protected boolean EstaEn_Lista_EDONUMDB(String enfermedad,
                                          String casos,
                                          String partes) {

    int iTam = listaEDONUMBD.size();
    for (int i = 0; i < iTam; i++) {
      DataListaEDONum datosBD = (DataListaEDONum) listaEDONUMBD.elementAt(i);

      if (datosBD.getCodEnfer().trim().equals(enfermedad.trim())) {
        if (datosBD.getCasos().trim().equals(casos.trim())) {
          return true;
        }
      }
    }
    return false;
  } //fin EstaEn_Lista_EDONUMDB

  protected void btnPartes_actionPerformed() {

    int iEstado_Partes = 0;
    int modonormal = 0;
    int modovisual = 2;

    if (super.modoOperacion == modoMODIFICACION) {
      iEstado_Partes = modonormal;
    }
    if (super.modoOperacion == modoBAJA ||
        super.modoOperacion == modoCONSULTA) {
      iEstado_Partes = modovisual;
    }
    //continuamos
    int iSel = tblDia.getSelectedIndex(); //en lista DIA***
    DataListaEDONum datalinea = null;
    if (iSel != BWTEnum.NOTFOUND) {
      datalinea = (DataListaEDONum) (listaTblDia.elementAt(iSel));
      Integer ICasos = new Integer(datalinea.getCasos());

      if (datalinea.getPartes().equals("")) {
        datalinea.sPartes = "0";

      }
      Integer IPartes = new Integer(datalinea.getPartes());
      int icasos = ICasos.intValue();
      int ipartes = IPartes.intValue();
      parNotAdic parEntrada = new parNotAdic(iEstado_Partes,
                                             datalinea.getCodEnfer(),
                                             datalinea.getDesEnfer(),
                                             icasos,
                                             ipartes,
                                             datalinea.getAnno(),
                                             datalinea.getSemana(),
                                             datalinea.getCodEquipo(),
                                             datalinea.getFechaNotif(),
                                             datalinea.getFechaRecep(),
                                             (String) hashNotifEDO.get(
          "CD_NIVEL_1"), //equipo
                                             (String) hashNotifEDO.get(
          "CD_NIVEL_2"),
                                             applet.getCA()); //ca applet

      int modo = super.modoOperacion;
      applet.pListaEDONum.Inicializar(modoESPERA);
      applet.pListaEDOIndiv.Inicializar(modoESPERA);

      dialogoNotAdic dlgCris = new dialogoNotAdic(applet, parEntrada);
      if (!dlgCris.cerrar) {
        dlgCris.show();
      }
      int iTotalPartes = dlgCris.gettotalPartes();

      //repintar linea (dia - semana)
      if (iTotalPartes != ipartes) {
        Integer I = new Integer(iTotalPartes);
        String P = I.toString();
        DataListaEDONum dataNueva = new DataListaEDONum(
            datalinea.getCodEnfer(),
            datalinea.getDesEnfer(),
            datalinea.getCasos(),
            datalinea.getFechaRecep(),
            datalinea.getVigi(),
            datalinea.getCodEquipo(),
            datalinea.getAnno(), datalinea.getSemana(),
            datalinea.getCodOpe(), datalinea.getFechaUltAct(),
            datalinea.getFechaNotif(),
            P);

        //DIA******
        addRowFecha(modoMODIFY, dataNueva, iSel);
        tblDia.setItems(itemsDia);
        //SEMANA****
        int indiceSemana = Calcular_Indice_Semana(dataNueva);
        if (indiceSemana == -1) {
          ShowWarning(res.getString("msg27.Text"));
        }
        else {
          addRow(modoMODIFY, dataNueva, indiceSemana - 1); //UN -1 NO SE POR QUE!!!
          tblSemana.setItems(itemsSemana);
        }
      } //fin repintar ------------

      dlgCris = null;
      super.modoOperacion = modo;
      // lo pasamos al modo anterior
      applet.pListaEDONum.Inicializar(modo);
      applet.pListaEDOIndiv.Inicializar(modo);
    }
  } //fin btnpartes

//CAMBIO 19/02/2001 AIC
  protected void btnAlta_actionPerformed() {

    // VALIDO FECHA RECEPCI�N *************
    // modificacion jlt 22/10/2001 valido la fecha de recepci�n
    // que ahora viene en blanco por defecto
    MaestroEDO = applet.pMaestroEDO;
    String n = new String();
    n = MaestroEDO.getFechaRecep().trim();
    if (n.equals("") || n.equals(null)) {

      ShowWarning(res.getString("msg80.Text"));
      return;
    }

    int modo = super.modoOperacion;

    super.modoOperacion = modoESPERA;

    //JM Inicializar();
    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);

//    DataListaEDONum dataLinea = new DataListaEDONum();

    DialNum dlg = new DialNum(this.getApp(), hashNotifEDO, modoALTA);
    if (dlg.addPanelProt()) {
      dlg.show();

//    if (dlg.iOut == 0){//aceptar

      CLista listaDevuelta = dlg.devuelveDatos();

      if (listaDevuelta != null) {
        Hashtable dtTmp = null;
        // Metemos esto en un bucle, para insertar todos los datos de la lista.
        for (int indice = 0; indice < listaDevuelta.size(); indice++) {
          //     DataListaEDONum datosNuevosDia =  (DataListaEDONum) dlg.getComponente();
          dtTmp = new Hashtable();
          dtTmp = (Hashtable) listaDevuelta.elementAt(indice);
          String sDS_Enfer = (String) dtTmp.get("ENFERMEDAD");
          String sCasos = (String) dtTmp.get("NUM_CASOS");
          String sVigi = (String) dtTmp.get("CD_TVIGI");
          String sEnfer = (String) dtTmp.get("CD_ENFCIE");
          sDS_Enfer = sDS_Enfer.substring(0, sDS_Enfer.length() - 2); // Quitar los dos puntos.
          // Para que meta �nicamente los datos que tienen casos:
          if (sCasos.trim().length() > 0) {
            DataListaEDONum datosNuevosDia = new DataListaEDONum(sEnfer,
                sDS_Enfer,
                sCasos,
                (String) hashNotifEDO.get("FC_FECNOTIF"),
                sVigi);

            DataListaEDONum datosNuevosSemana = new DataListaEDONum(
                datosNuevosDia.getCodEnfer(),
                datosNuevosDia.getDesEnfer(),
                datosNuevosDia.getCasos(),
                datosNuevosDia.getFechaRecep(),
                datosNuevosDia.getVigi(),
                "", "", "", "", "",
                datosNuevosDia.getFechaNotif(),
                datosNuevosDia.getPartes());

            if (datosNuevosDia != null) {

              datosNuevosDia.sCodEquipo = (String) hashNotifEDO.get(
                  "CD_E_NOTIF"); //sCodEquipo;
              datosNuevosDia.sAnno = (String) hashNotifEDO.get("CD_ANOEPI"); //sAnno;
              datosNuevosDia.sSemana = (String) hashNotifEDO.get("CD_SEMEPI"); //sSemana;
              datosNuevosDia.sFechaNotif = (String) hashNotifEDO.get(
                  "FC_FECNOTIF");
              datosNuevosDia.sFechaRecep = (String) hashNotifEDO.get("FC_RECEP");

              datosNuevosSemana.sCodEquipo = (String) hashNotifEDO.get(
                  "CD_E_NOTIF"); //sCodEquipo;
              datosNuevosSemana.sAnno = (String) hashNotifEDO.get("CD_ANOEPI"); //sAnno;
              datosNuevosSemana.sSemana = (String) hashNotifEDO.get("CD_SEMEPI"); //sSemana;
              datosNuevosSemana.sFechaNotif = (String) hashNotifEDO.get(
                  "FC_FECNOTIF");
              datosNuevosSemana.sFechaRecep = (String) hashNotifEDO.get(
                  "FC_RECEP");

              //DIA**** (-1 no afecta. Se a�ade al final)
              //addRowFecha(modoADD, data, -1);
              //tblDia.setItems(itemsDia);
              Recomponer_Tabla_Dia(datosNuevosDia);

              //SEMANA***** (-1 no afecta. Se a�ade al final)
              //addRow(modoADD, data, -1);
              //tblSemana.setItems(itemsSemana);
              Recomponer_Tabla_Semana(datosNuevosSemana);
            }
          } //Final del "if" ese de si tiene o no casos.
          dtTmp = null;
        } // Final del bucle que recorre la lista.
      } // Final del "si lista devuelta tiene datos"

      dlg = null;
      // *************** FINAL DE HAY DATOS.
    }
    else {
      CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                     "No se encontraron datos");
      msgBox.show();
      msgBox = null;
    }

    //super.modoOperacion = modoNORMAL;
    super.modoOperacion = modo;
    //JM Inicializar();
    applet.pListaEDONum.Inicializar(modo);
    applet.pListaEDOIndiv.Inicializar(modo);

  }

//CAMBIO 19/02/2001 AIC

  //ALTA*********************************
  /*protected void btnAlta_actionPerformed() {
    int modo = super.modoOperacion;
    super.modoOperacion = modoESPERA;
    //JM Inicializar();
    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);
    DataListaEDONum dataLinea = new DataListaEDONum();
    DialogListaEDONum dlg = new DialogListaEDONum(this.getApp(),
                                                  res.getString("EDO"),
                                                  stubCliente,
                                                  "",
                                                  modoALTA,
                                                  dataLinea);
    dlg.show();
    if (dlg.iOut == 0){//aceptar
      DataListaEDONum datosNuevosDia =  (DataListaEDONum) dlg.getComponente();
      DataListaEDONum datosNuevosSemana = new DataListaEDONum(
          datosNuevosDia.getCodEnfer(),
          datosNuevosDia.getDesEnfer(),
          datosNuevosDia.getCasos(),
          datosNuevosDia.getFechaRecep(),
          datosNuevosDia.getVigi(),
          "","","","", "",
          datosNuevosDia.getFechaNotif(),
          datosNuevosDia.getPartes());
      if (datosNuevosDia != null) {
        datosNuevosDia.sCodEquipo = (String)hashNotifEDO.get("CD_E_NOTIF"); //sCodEquipo;
        datosNuevosDia.sAnno = (String)hashNotifEDO.get("CD_ANOEPI"); //sAnno;
       datosNuevosDia.sSemana = (String)hashNotifEDO.get("CD_SEMEPI"); //sSemana;
        datosNuevosDia.sFechaNotif = (String)hashNotifEDO.get("FC_FECNOTIF");
        datosNuevosDia.sFechaRecep = (String)hashNotifEDO.get("FC_RECEP");
        datosNuevosSemana.sCodEquipo = (String)hashNotifEDO.get("CD_E_NOTIF"); //sCodEquipo;
       datosNuevosSemana.sAnno = (String)hashNotifEDO.get("CD_ANOEPI"); //sAnno;
       datosNuevosSemana.sSemana = (String)hashNotifEDO.get("CD_SEMEPI"); //sSemana;
       datosNuevosSemana.sFechaNotif = (String)hashNotifEDO.get("FC_FECNOTIF");
        datosNuevosSemana.sFechaRecep = (String)hashNotifEDO.get("FC_RECEP");
        //DIA**** (-1 no afecta. Se a�ade al final)
        //addRowFecha(modoADD, data, -1);
        //tblDia.setItems(itemsDia);
        Recomponer_Tabla_Dia(datosNuevosDia);
        //SEMANA***** (-1 no afecta. Se a�ade al final)
        //addRow(modoADD, data, -1);
        //tblSemana.setItems(itemsSemana);
        Recomponer_Tabla_Semana(datosNuevosSemana);
      }
    }else if (dlg.iOut == -1){
     //cancelar
    }
    dlg = null;
    //super.modoOperacion = modoNORMAL;
    super.modoOperacion = modo;
    //JM Inicializar();
        applet.pListaEDONum.Inicializar(modo);
        applet.pListaEDOIndiv.Inicializar(modo);
     } */
  protected void Recomponer_Tabla_Dia(DataListaEDONum datos) {

    //Reserva
    CLista listaReserva = new CLista();
    DataListaEDONum dataProvisional = null;
    for (int i = 0; i < listaTblDia.size(); i++) {
      dataProvisional = (DataListaEDONum) listaTblDia.elementAt(i);
      listaReserva.addElement(dataProvisional);
    } //----------------------------------

    itemsDia = new JCVector();
    listaTblDia = new CLista();
    tblDia.clear();
    tblDia.doLayout();

    String enfermedad = datos.getCodEnfer();
    String casosnuevos = datos.getCasos();
    CLista listaNueva = new CLista();

    boolean encontrado = false;
    for (int i = 0; i < listaReserva.size(); i++) {
      DataListaEDONum datosTabla = (DataListaEDONum) listaReserva.elementAt(i);
      if (datosTabla.getCodEnfer().trim().equals(enfermedad.trim())) {
        //existe otro con igual enfermedad
        Integer IcasosOld = new Integer(datosTabla.getCasos());
        Integer IcasosNew = new Integer(casosnuevos);
        int iSuma = (IcasosOld.intValue()) + (IcasosNew.intValue());
        Integer ISuma = new Integer(iSuma);
        datosTabla.sCasos = ISuma.toString();
        encontrado = true;
      }
      listaNueva.addElement(datosTabla); //debe llevar los partes
    }
    if (!encontrado) {
      listaNueva.addElement(datos); //los partes no van

    }
    Object componente = new Object();
    for (int i = 0; i < listaNueva.size(); i++) {
      componente = listaNueva.elementAt(i);
      addRowFecha(modoADD, (DataListaEDONum) componente, -1);
    }
    tblDia.setItems(itemsDia);
  } //recomponer

  protected void Recomponer_Tabla_Semana(DataListaEDONum datos) {

    //Reserva
    CLista listaReserva = new CLista();
    DataListaEDONum dataProvisional = null;
    for (int i = 0; i < listaTblSemana.size(); i++) {
      dataProvisional = (DataListaEDONum) listaTblSemana.elementAt(i);
      listaReserva.addElement(dataProvisional);
    } //----------------------------------

    itemsSemana = new JCVector();
    listaTblSemana = new CLista();
    tblSemana.clear();
    tblSemana.doLayout();

    String enfermedad = datos.getCodEnfer();
    String recep = datos.getFechaRecep();
    String notif = datos.getFechaNotif();
    String casosnuevos = datos.getCasos();

    CLista listaNueva = new CLista();
    boolean encontrado = false;

    for (int i = 0; i < listaReserva.size(); i++) {
      DataListaEDONum datosTabla = (DataListaEDONum) listaReserva.elementAt(i);
      if (datosTabla.getCodEnfer().trim().equals(enfermedad.trim()) &&
          datosTabla.getFechaRecep().trim().equals(recep.trim()) &&
          datosTabla.getFechaNotif().trim().equals(notif.trim())) {
        //existe otro con igual enfermedad + fechas
        Integer IcasosOld = new Integer(datosTabla.getCasos());
        Integer IcasosNew = new Integer(casosnuevos);
        int iSuma = (IcasosOld.intValue()) + (IcasosNew.intValue());
        Integer ISuma = new Integer(iSuma);
        datosTabla.sCasos = ISuma.toString();
        encontrado = true;
      }
      listaNueva.addElement(datosTabla);
    }
    if (!encontrado) {
      listaNueva.addElement(datos);

    }
    Object componente = new Object();
    for (int i = 0; i < listaNueva.size(); i++) {
      componente = listaNueva.elementAt(i);
      addRow(modoADD, (DataListaEDONum) componente, -1);
    }
    tblSemana.setItems(itemsSemana);
  } //recomponer

  public void A�adir_desde_Indiv_paraTipoA(String cd_enfcie) {

    int iSel = 0;

    //encontrar datos e iSel de la enfermedad en la listaDia de Numericas
    for (int i = 0; i < listaTblDia.size(); i++) {
      DataListaEDONum datosLinea = (DataListaEDONum) listaTblDia.elementAt(i);
      if (datosLinea.getCodEnfer().trim().equals(cd_enfcie)) {
        iSel = i;
        break;
      }
    }
    DataListaEDONum datosLinea = (DataListaEDONum) listaTblDia.elementAt(iSel);
    DataListaEDONum data = new DataListaEDONum(datosLinea.getCodEnfer(),
                                               datosLinea.getDesEnfer(),
                                               datosLinea.getCasos(),
                                               datosLinea.getFechaRecep(),
                                               datosLinea.getVigi(),
                                               datosLinea.getCodEquipo(),
                                               datosLinea.getAnno(),
                                               datosLinea.getSemana(),
                                               datosLinea.getCodOpe(),
                                               datosLinea.getFechaUltAct(),
                                               datosLinea.getFechaNotif(),
                                               datosLinea.getPartes());

    //aumentar un parte
    Integer IPartes = new Integer(datosLinea.sPartes);
    int ipartes = IPartes.intValue() + 1;
    Integer NewIPartes = new Integer(ipartes);
    data.sPartes = NewIPartes.toString();

    //DIA******
    addRowFecha(modoMODIFY, data, iSel);
    tblDia.setItems(itemsDia);

    //SEMANA*****
    //detectar el indice equivalente en Semana
    //(fecha del dia + enfermedad)
    int indiceSemana = Calcular_Indice_Semana(data);
    if (indiceSemana == -1) {
      ShowWarning(res.getString("msg27.Text"));
    }
    else {
      addRow(modoMODIFY, data, indiceSemana - 1); //UN -1 NO SE POR QUE!!!
      tblSemana.setItems(itemsSemana);
    }
  }

  public void Restar_desde_Indiv_paraTipoA(String cd_enfcie) {

    int iSel = 0;

    //encontrar datos e iSel de la enfermedad en la listaDia de Numericas
    for (int i = 0; i < listaTblDia.size(); i++) {
      DataListaEDONum datosLinea = (DataListaEDONum) listaTblDia.elementAt(i);
      if (datosLinea.getCodEnfer().trim().equals(cd_enfcie)) {
        iSel = i;
        break;
      }
    }
    DataListaEDONum datosLinea = (DataListaEDONum) listaTblDia.elementAt(iSel);
    DataListaEDONum data = new DataListaEDONum(datosLinea.getCodEnfer(),
                                               datosLinea.getDesEnfer(),
                                               datosLinea.getCasos(),
                                               datosLinea.getFechaRecep(),
                                               datosLinea.getVigi(),
                                               datosLinea.getCodEquipo(),
                                               datosLinea.getAnno(),
                                               datosLinea.getSemana(),
                                               datosLinea.getCodOpe(),
                                               datosLinea.getFechaUltAct(),
                                               datosLinea.getFechaNotif(),
                                               datosLinea.getPartes());

    //aumentar un parte
    Integer IPartes = new Integer(datosLinea.sPartes);
    int ipartes = IPartes.intValue() - 1;
    Integer NewIPartes = new Integer(ipartes);
    data.sPartes = NewIPartes.toString();

    //DIA******
    addRowFecha(modoMODIFY, data, iSel);
    tblDia.setItems(itemsDia);

    //SEMANA*****
    //detectar el indice equivalente en Semana
    //(fecha del dia + enfermedad)
    int indiceSemana = Calcular_Indice_Semana(data);
    if (indiceSemana == -1) {
      ShowWarning(res.getString("msg27.Text"));
    }
    else {
      addRow(modoMODIFY, data, indiceSemana - 1); //UN -1 NO SE POR QUE!!!
      tblSemana.setItems(itemsSemana);
    }
  }

//CAMBIO 19/02/2001 AIC
  protected void btnModificacion_actionPerformed() {
    int modo = super.modoOperacion;
    super.modoOperacion = modoESPERA;

    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);

    DataListaEDONum datalinea = null;
    DataListaEDONum data = null;

    DialNum dlg = new DialNum(this.getApp(), hashNotifEDO, modoMODIFICACION);
    if (dlg.addPanelProt()) {
      dlg.show();

      //data = (DataListaEDONum) dlg.getComponente();
      CLista listaDevuelta = dlg.devuelveDatos();

      if (listaDevuelta != null) {

        Hashtable dtTmp = null;
        // Metemos esto en un bucle, para insertar todos los datos de la lista.
        for (int indice = 0; indice < listaDevuelta.size(); indice++) {

          dtTmp = new Hashtable();
          dtTmp = (Hashtable) listaDevuelta.elementAt(indice);
          String sDS_Enfer = (String) dtTmp.get("ENFERMEDAD");
          String sCasos = (String) dtTmp.get("NUM_CASOS");
          String sVigi = (String) dtTmp.get("CD_TVIGI");
          String sEnfer = (String) dtTmp.get("CD_ENFCIE");
          sDS_Enfer = sDS_Enfer.substring(0, sDS_Enfer.length() - 2); // Quitar los dos puntos.

          // datos de la l�nea
          datalinea = (DataListaEDONum) (listaTblDia.elementAt(indice));

          data = new DataListaEDONum(sEnfer,
                                     sDS_Enfer,
                                     sCasos,
                                     (String) hashNotifEDO.get("FC_FECNOTIF"),
                                     sVigi);

          if (data != null) {
            data.sCodEquipo = (String) hashNotifEDO.get(res.getString(
                "CD_E_NOTIF")); //sCodEquipo;
            data.sAnno = (String) hashNotifEDO.get("CD_ANOEPI"); //sAnno;
            data.sSemana = (String) hashNotifEDO.get("CD_SEMEPI"); //sSemana;
            data.sFechaNotif = (String) hashNotifEDO.get("FC_FECNOTIF");
            data.sFechaRecep = (String) hashNotifEDO.get("FC_RECEP");
            data.sPartes = datalinea.getPartes();

            //DIA******
            addRowFecha(modoMODIFY, data, indice);
            tblDia.setItems(itemsDia);

            //SEMANA*****
            //detectar el indice equivalente en Semana
            //(fecha del dia + enfermedad)
            int indiceSemana = Calcular_Indice_Semana(data);
            if (indiceSemana == -1) {
              ShowWarning(res.getString("msg27.Text"));
            }
            else {
              addRow(modoMODIFY, data, indiceSemana - 1); //UN -1 NO SE POR QUE!!!
              tblSemana.setItems(itemsSemana);
            }
          }
          dtTmp = null;
        } // Final del bucle que recorre la lista.
      } // Final del "si lista devuelta es no nula"
      dlg = null;
      // ********** FINAL DEL HAY DATOS.
    }
    else {
      CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                     "No se encontraron datos");
      msgBox.show();
      msgBox = null;
    }
    super.modoOperacion = modo;
    //JM Inicializar();
    applet.pListaEDONum.Inicializar(modo);
    applet.pListaEDOIndiv.Inicializar(modo);

  }

//CAMBIO 19/02/2001 AIC

  //MODIFICACION*********************************
  /*protected void btnModificacion_actionPerformed() {
    int modo = super.modoOperacion;
    super.modoOperacion = modoESPERA;
    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);
    int iSel = tblDia.getSelectedIndex(); //en lista DIA***
    DataListaEDONum datalinea = null;
    DataListaEDONum data = null;
    if (iSel != BWTEnum.NOTFOUND) {
      // datos de la l�nea
      datalinea = (DataListaEDONum)(listaTblDia.elementAt(iSel));
      //puedo entrar como modificacion o consulta o alta = modif
      //borrado maxivo no entra
      int iEstado = 0;
      if (modo == modoCONSULTA)
        iEstado = modoCONSULTA;
      else
        iEstado = modoMODIFICACION;
      DialogListaEDONum dlg = new DialogListaEDONum(this.getApp(),
          res.getString("EDO"), stubCliente,"",
          iEstado, datalinea);
      dlg.show();
      data = (DataListaEDONum) dlg.getComponente();
      if (data != null) {
        data.sCodEquipo = (String)hashNotifEDO.get(res.getString("CD_E_NOTIF")); //sCodEquipo;
        data.sAnno = (String)hashNotifEDO.get("CD_ANOEPI"); //sAnno;
        data.sSemana = (String)hashNotifEDO.get("CD_SEMEPI"); //sSemana;
        data.sFechaNotif = (String)hashNotifEDO.get("FC_FECNOTIF");
        data.sFechaRecep = (String)hashNotifEDO.get("FC_RECEP");
        data.sPartes = datalinea.getPartes();
        //DIA******
        addRowFecha(modoMODIFY, data, iSel);
        tblDia.setItems(itemsDia);
        //SEMANA*****
        //detectar el indice equivalente en Semana
        //(fecha del dia + enfermedad)
        int indiceSemana = Calcular_Indice_Semana(data);
        if (  indiceSemana == -1 ){
          ShowWarning(res.getString("msg27.Text"));
          }
        else{
          addRow(modoMODIFY, data, indiceSemana -1);//UN -1 NO SE POR QUE!!!
          tblSemana.setItems(itemsSemana);
        }
      }
      dlg = null;
    }
    else {
      ShowWarning(res.getString("msg28.Text"));
    }
    super.modoOperacion =  modo;
    //JM Inicializar();
        applet.pListaEDONum.Inicializar(modo);
        applet.pListaEDOIndiv.Inicializar(modo);
     }*/
  //BAJA*********************************
  protected void btnBaja_actionPerformed() {
    int modo = super.modoOperacion;
    super.modoOperacion = modoESPERA;

    applet.pListaEDONum.Inicializar(modoESPERA);
    applet.pListaEDOIndiv.Inicializar(modoESPERA);

    if ( (tblDia.countItems() == 1) &&
        (applet.pListaEDOIndiv.tblDia.countItems() == 0)) {
      ShowWarning(res.getString("msg29.Text"));
      super.modoOperacion = modo;

      applet.pListaEDONum.Inicializar(modo);
      applet.pListaEDOIndiv.Inicializar(modo);

      return;
    }
    int iSel = tblDia.getSelectedIndex();
    if (iSel != BWTEnum.NOTFOUND) {
      DataListaEDONum data = new DataListaEDONum();
      data = (DataListaEDONum) listaTblDia.elementAt(iSel);

      //si es de tipo A debe borrar antes los partes Individualizados
      if (data.getVigi().trim().equals("A")
          && !data.getPartes().trim().equals("0")) {
        ShowWarning(res.getString("msg30.Text"));
        super.modoOperacion = modo;

        applet.pListaEDONum.Inicializar(modo);
        applet.pListaEDOIndiv.Inicializar(modo);
        return;
      }
      //*************************************************************

       CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                      res.getString("msg31.Text"));
      msgBox.show();

      if (msgBox.getResponse()) {
        data.sCodEquipo = (String) hashNotifEDO.get("CD_E_NOTIF");
        data.sAnno = (String) hashNotifEDO.get("CD_ANOEPI");
        data.sSemana = (String) hashNotifEDO.get("CD_SEMEPI");

        //DIA*****
        addRowFecha(modoDELETE, data, iSel);
        tblDia.setItems(itemsDia);

        //SEMANA*****
        //detectar el indice equivalente en Semana
        //(fecha del dia + enfermedad)
        data.sFechaNotif = (String) hashNotifEDO.get("FC_FECNOTIF");
        data.sFechaRecep = (String) hashNotifEDO.get("FC_RECEP");
        int indiceSemana = Calcular_Indice_Semana(data);
        if (indiceSemana == -1) {
          ShowWarning(res.getString("msg27.Text"));
        }
        else {
          //-1 NO SE POR QUE!!!
          addRow(modoDELETE, data, indiceSemana - 1); //data no vale
          tblSemana.setItems(itemsSemana);
        }
      }
      msgBox = null;
    }
    else {
      ShowWarning(res.getString("msg28.Text"));
    }

    super.modoOperacion = modo;
    //JM Inicializar();
    applet.pListaEDONum.Inicializar(modo);
    applet.pListaEDOIndiv.Inicializar(modo);

  }

  protected void btnAceptar_actionPerformed() {

    // modificacion jlt 22/10/2001
    String n = new String();
    n = applet.pMaestroEDO.getFechaRecep().trim();
    if (n.equals("") || n.equals(null)) {
      ShowWarning(res.getString("msg80.Text"));
      return;
    }

    CLista listaAux = new CLista();
    int modo = super.modoOperacion;

    // ARS 28-06-01
    // Si las notificaciones reales son correctas
    // seguimos adelante.
    if (applet.pMaestroEDO.bNotifRealesBien) {

      super.modoOperacion = modoESPERA;
      applet.pListaEDONum.Inicializar(modoESPERA);
      applet.pListaEDOIndiv.Inicializar(modoESPERA);

      switch (modo) {
        case modoALTA:

          // no se permite dar de alta una notificaci�n
          //sin EDO num�ricas ni individualizadas
          if ( (tblDia.countItems() == 0)
              && (applet.pListaEDOIndiv.tblDia.countItems() == 0)
              && !applet.pMaestroEDO.chkbxResSem.getState()) {
            ShowWarning(res.getString("msg32.Text"));
            super.modoOperacion = modo; //ALTA
            applet.pListaEDONum.Inicializar(modo);
            applet.pListaEDOIndiv.Inicializar(modo);
            return;
          }

          if ( (tblDia.countItems() == 0)
              && (applet.pListaEDOIndiv.tblDia.countItems() == 0)
              && applet.pMaestroEDO.chkbxResSem.getState()) {
            CMessage msgBox = new CMessage(this.app,
                                           CMessage.msgPREGUNTA,
                                           res.getString("msg33.Text"));
            msgBox.show();
            if (msgBox.getResponse()) { //SI
              //continua
            }
            else { //NO
              super.modoOperacion = modo; //ALTA
              applet.pListaEDONum.Inicializar(modo);
              applet.pListaEDOIndiv.Inicializar(modo);
              return;
            }
          } //fin ne notificaciones CEROS CASOS -----------------

          applet.pMaestroEDO.AltaNotificacion(listaTblDia);
          //cambio a modif si bien o alta si mal//NO NECESITA INICIALIZAR
          return;

        case modoMODIFICACION:
          if ( (tblDia.countItems() == 0)
              && (applet.pListaEDOIndiv.tblDia.countItems() == 0)
              && !applet.pMaestroEDO.chkbxResSem.getState()) {
            ShowWarning(res.getString("msg32.Text"));
            super.modoOperacion = modo; //MODIFICACION
            applet.pListaEDONum.Inicializar(modo);
            applet.pListaEDOIndiv.Inicializar(modo);
            return;
          }
          applet.pMaestroEDO.ModNotificacion(listaTblDia);
          //control de error al rellenarlistas
          if (applet.pMaestroEDO.modoOperacion == modoCABECERAYBOTONES) {
            return;
          } //**************************************

          super.modoOperacion = modo;
          applet.pListaEDONum.Inicializar(modo);
          applet.pListaEDOIndiv.Inicializar(modo);
          return;

        case modoBAJA:
          applet.pMaestroEDO.BajaNotificacion();
          //ya lo ponen a modoCabecerayBotones!!!
          return;
      }

    } // ARS 28-06-01
    // Fin del "si las notificaciones reales son correctas"
    // Reiniciamos la variable bNotifRealesBien
    applet.pMaestroEDO.bNotifRealesBien = true;
  } //fin de aceptar ****

  public void Vaciar() {
    itemsSemana = new JCVector();
    listaTblSemana = new CLista();
    itemsDia = new JCVector();
    listaTblDia = new CLista();
    tblDia.clear();
    tblSemana.clear();
    bListaNumVacia = true;
    listaEDONUMBD = new CLista();
  }

  /*
   * Autor        Fecha         Accion
       *  JRM         12/07/2000    Modifica para que cambie al siguiente notificador.
   */
  public void btnCancelar_actionPerformed() {
    //validacion
    super.chkValidar.setVisible(false);
    super.lblValidar.setVisible(false);
    super.lblEtiquetaValidar.setVisible(false);
    super.IT_VALIDADA_PRIM = "";
    super.FC_FECVALID_PRIM = "";
    applet.pMaestroEDO.chkbxResSem.setState(false);
    // JRM2 Modifica para que cuando se d� al cancelar y estuviese activo un notificador
    //  dado de baja, no se pueda modificar
    boolean enMadrid;
    if (getApp().getParameter("ORIGEN").equals("M")) {
      enMadrid = true;
    }
    else {
      enMadrid = false;
    }
    if (!enMadrid) { // Es Castilla y Leon
      pArDiAn.habilitarPanel(true);

      if (applet.getPerfil() != 5) {
        String codNot = applet.pMaestroEDO.equiposNotificadores.siguiente();
        if (!codNot.equals("")) {
          applet.pMaestroEDO.actEquipoNotificador(codNot);
        }
      }
      // habilitamos panel para que pueda cambiar de notificador
      if (applet.getPerfil() != 5) {
        applet.pMaestroEDO.enableControlsCab(true);
      }

      if (applet.pMaestroEDO.modoOperacion == modoCONSULTA ||
          applet.pMaestroEDO.modoOperacion == modoCONSULTAYBOTONES) {
        applet.pArDiAn.tfDistrito.requestFocus();
        //applet.pArDiAn.tfDistrito.enable();
        applet.pArDiAn.tfAnno.requestFocus();
        applet.pMaestroEDO.txtCodEquipo.requestFocus();
        //AIC
        applet.pMaestroEDO.Inicializar(modoCONSULTAYBOTONES);
        //applet.pMaestroEDO.Inicializar(modoCONSULTA);
      }

    }
    else { //En Madrid
      pArDiAn.habilitarPanel(true);

      // JRM:
      //JRM2 Para que en Madrid no pase al siguiente notificador
      // recogo el dato del flag que me dir� si es Madrid

      /*if (applet.getPerfil() != 5 )
            {
        String codNot = applet.pMaestroEDO.equiposNotificadores.toString();
       if (!codNot.equals(""))
       {
         applet.pMaestroEDO.actEquipoNotificador(codNot);
       }
            }
            // habilitamos panel para que pueda cambiar de notificador
            if (applet.getPerfil() != 5 )
        applet.pMaestroEDO.enableControlsCab(true);
       COMENTADO PARA QUE NO CAMBIE DE NOTIFICADOR  */

      if (applet.pMaestroEDO.modoOperacion != modoCONSULTA &&
          applet.pMaestroEDO.modoOperacion != modoCONSULTAYBOTONES) {
        applet.pMaestroEDO.Inicializar(modoCABECERAYBOTONES);
      }
      else {
        applet.pArDiAn.tfDistrito.requestFocus();
        //applet.pArDiAn.tfDistrito.enable();
        applet.pArDiAn.tfAnno.requestFocus();
        applet.pMaestroEDO.txtCodEquipo.requestFocus();
        //AIC
        applet.pMaestroEDO.Inicializar(modoCONSULTAYBOTONES);
        //applet.pMaestroEDO.Inicializar(modoCONSULTA);
      }

    } //Fin de En Madrid

  }

  protected void tbl_item(JCItemEvent e) {
    evento = e;

    //en lista DIA***
    int iSel = tblDia.getSelectedIndex();
    DataListaEDONum datalinea = null;
    if (iSel != BWTEnum.NOTFOUND) {
      datalinea = (DataListaEDONum) (listaTblDia.elementAt(iSel));
      if (datalinea.getVigi().trim().equals("X") &&
          EstaEn_Lista_EDONUMDB(datalinea.getCodEnfer(),
                                datalinea.getCasos(),
                                datalinea.getPartes())) {

        btnPartes.setEnabled(false);
        btnPartes.setEnabled(true);
      }
      else {
        btnPartes.setEnabled(false);
      }
    }
    else {
      btnPartes.setEnabled(false);

    }
    this.doLayout();

  }

  protected void tbl_actionPerformed(JCActionEvent e) {

    btnPartes.setEnabled(false);
    this.doLayout();

    //en el Borrado Maxivo no dejamos ni ver
    //en consulta, dejara ver
    if (super.modoOperacion != modoBAJA) {
      btnModificacion_actionPerformed();
    }

  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  private void ShowWarning(String sMessage) {
    applet.pMaestroEDO.ShowWarning(sMessage);
  }
}
