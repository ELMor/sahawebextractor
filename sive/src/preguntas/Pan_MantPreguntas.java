package preguntas;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import catalogo.Catalogo;
import catalogo.DataCat;
import comun.constantes;
import listas.DataMantlis;
import listas.PanelLista;
import sapp.StubSrvBD;

public class Pan_MantPreguntas
    extends CDialog {

  public boolean valido = false;
  ResourceBundle res = ResourceBundle.getBundle("preguntas.Res" +
                                                this.app.getIdioma());

  // modos de operaci�n de la ventana
  public final static int modoALTA = 0;
  public final static int modoMODIFICAR = 1;
  public final static int modoBAJA = 2;
  public final static int modoCONSULTA = 3;
  final int modoESPERA = 5;

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletPREGUNTA_SELECCIONADA = 7;

  //Constantes del panel
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/inslista.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  /*
    final String strSERVLET= "servlet/SrvMantPreguntas";
    final String strSERVLET_AUX= "servlet/SrvMantlis";
    final String strSERVLET_CAT= "servlet/SrvCat";
   */

  final String strSERVLET = constantes.strSERVLET_MANT_PREGUNTAS;
  final String strSERVLET_AUX = constantes.strSERVLET_MANT_LIS;
  final String strSERVLET_CAT = constantes.strSERVLET_CAT;

  final int maxLONGITUD_CADENA = 40;

  //Par�metros
  protected int modoOperacion;

  protected CLista listaTipos = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  protected String sTipoPregunta = "";

  protected int iLongitud;
  protected int iEnteros;
  protected int iDecimales;

  public boolean bAceptar = false; //P�blico para poder a�adir preg�s a lista preg�s desde modelos

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  public DataMantPreguntas pregunta;

  /*
    //Etiquetas para idiomas
    String strPregunta[] = {"Pregunta:","CA","EU","GA"};
    String strDescPregunta[] = {"Descripci�n:","CA:","EU:","GA:"};
    String strEnteros[]= { "Enteros:","CA","EU","GA"};
    String strListaValores[] = {"Lista de Valores:","CA","EU","GA"};
    String strDecimales[]= {"Decimales:","CA:","EU:","GA:"};
    String strLongitud[]= {"Longitud:","CA:","EU:","GA:"};
    String strTipo[]= {"Tipo:","CA:","EU:","GA:"};
    String strAnadir[] = {"A�adir","CA:","EU:","GA:"};
    String strBorrar[] = {"Borrar","CA:","EU:","GA:"};
    String strModificar[] ={"Modificar","CA:","EU:","GA:"};
   */

  XYLayout xYLayout = new XYLayout();
  Panel pnl = new Panel();
  Label lblPregunta = new Label();
  Label lblDescPregunta = new Label();
  TextField txtDescPregunta = new TextField();
  Label lblListaValores = new Label();
  Label lblTipo = new Label();
  Choice choiceTipo = new Choice();
  Label lblLongitud = new Label();
  TextField txtLongitud = new TextField();
  Label lblEnteros = new Label();
  TextField txtEnteros = new TextField();
  Label lblDecimales = new Label();
  TextField txtDecimales = new TextField();
  ButtonControl btnCancelar = new ButtonControl();
  CCampoCodigo txtCodPregunta = new CCampoCodigo();
  ButtonControl btnAceptar = new ButtonControl();
  CCampoCodigo txtListaValores = new CCampoCodigo();
  ButtonControl btnCtrlListaValores = new ButtonControl();
  StatusBar statusBar = new StatusBar();
  BorderLayout borderLayout = new BorderLayout();

  TextField txtDescLista = new TextField();
  Label lblDescLocal = new Label();
  TextField txtDescLPregunta = new TextField();

  ButtonControl btnLista = new ButtonControl(); //***********

  // gestores de eventos
  PreguntasBtnActionListener btnActionListener = new PreguntasBtnActionListener(this);
  PreguntasChoiceItemListener choiceItemListener = new
      PreguntasChoiceItemListener(this);
  keyAdapter txtKeyAdapter = new keyAdapter(this);
  PreguntasTxtKeyListener keyListener = new PreguntasTxtKeyListener(this);
  focusAdapter txtFocusAdapter = new focusAdapter(this);

  public Pan_MantPreguntas(CApp a,
                           int modo,
                           DataMantPreguntas preg) {
    super(a);
    try {
      modoOperacion = modo;
      pregunta = preg;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void jbInit() throws Exception {
    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnCtrlListaValores.setImage(imgs.getImage(0));
    btnLista.setImage(imgs.getImage(1));
    btnAceptar.setImage(imgs.getImage(2));
    btnCancelar.setImage(imgs.getImage(3));

    xYLayout.setHeight(300);
    xYLayout.setWidth(507);
    lblPregunta.setText(res.getString("lblPregunta.Text"));
    lblDescPregunta.setText(res.getString("lblDescPregunta.Text"));
    txtDescPregunta.setBackground(new Color(255, 255, 192));
    lblListaValores.setText(res.getString("lblListaValores.Text"));
    lblTipo.setText(res.getString("lblTipo.Text"));
    lblLongitud.setText(res.getString("lblLongitud.Text"));
    lblEnteros.setText(res.getString("lblEnteros.Text"));
    lblDecimales.setText(res.getString("lblDecimales.Text"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    lblDescLocal.setText(res.getString("lblDescLocal.Text"));
    statusBar.setBevelOuter(BevelPanel.LOWERED);
    statusBar.setBevelInner(BevelPanel.LOWERED);
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    txtDescLista.setEditable(false);
    txtDescLista.setEnabled(false);

    btnCtrlListaValores.setActionCommand("buscar");
    btnLista.setActionCommand("lista");

    btnCancelar.setActionCommand("cancelar");
    btnAceptar.setActionCommand("aceptar");
    txtCodPregunta.setBackground(new Color(255, 255, 192));

    pnl.setLayout(xYLayout);

    pnl.add(lblPregunta, new XYConstraints(10, 8, 82, -1));
    pnl.add(txtCodPregunta, new XYConstraints(131, 8, 101, -1));
    pnl.add(lblDescPregunta, new XYConstraints(10, 52, 78, -1));
    pnl.add(txtDescPregunta, new XYConstraints(131, 52, 306, -1));
    pnl.add(lblTipo, new XYConstraints(10, 140, 34, -1));
    pnl.add(txtDescLPregunta, new XYConstraints(131, 96, 305, -1));
    pnl.add(choiceTipo, new XYConstraints(51, 140, 219, -1));
    pnl.add(lblDescLocal, new XYConstraints(10, 96, 113, -1));
    pnl.add(lblListaValores, new XYConstraints(10, 184, 97, -1));
    pnl.add(txtListaValores, new XYConstraints(131, 184, 101, -1));
    pnl.add(btnCtrlListaValores, new XYConstraints(240, 184, -1, -1));
    pnl.add(txtDescLista, new XYConstraints(271, 184, 256, -1));
    pnl.add(lblLongitud, new XYConstraints(11, 228, 55, -1));
    pnl.add(txtLongitud, new XYConstraints(73, 228, 38, -1));
    pnl.add(lblEnteros, new XYConstraints(119, 228, 51, -1));
    pnl.add(txtEnteros, new XYConstraints(176, 228, 38, -1));
    pnl.add(txtDecimales, new XYConstraints(299, 228, 42, -1));
    pnl.add(lblDecimales, new XYConstraints(222, 228, 67, -1));
    pnl.add(btnLista, new XYConstraints(247, 270, -1, 26)); //*************
    pnl.add(btnAceptar, new XYConstraints(347, 270, 80, 26));
    pnl.add(btnCancelar, new XYConstraints(447, 270, 80, 26));

    this.setLayout(borderLayout);

    //this.setSize(540,350);
    this.setSize(540, 360);

    this.add(pnl, BorderLayout.NORTH);
    this.add(statusBar, BorderLayout.SOUTH);

    txtCodPregunta.setForeground(Color.black);
    txtCodPregunta.setBackground(new Color(255, 255, 150));

    txtDescPregunta.setForeground(Color.black);
    txtDescPregunta.setBackground(new Color(255, 255, 150));

    // gestores click botones
    btnCtrlListaValores.addActionListener(btnActionListener);
    btnLista.addActionListener(btnActionListener); //********
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    // cambio del c�digo de la lista
    txtListaValores.addKeyListener(txtKeyAdapter);

    // control num�rico en cajas de texto
    txtEnteros.addKeyListener(keyListener);
    txtDecimales.addKeyListener(keyListener);
    txtLongitud.addKeyListener(keyListener);

    // cambio de la lista de tipos de preguntas
    choiceTipo.addItemListener(choiceItemListener);

    // perdida de foco en la caja de codigos de listas
    txtListaValores.addFocusListener(txtFocusAdapter);

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDescLPregunta.setVisible(false);
      lblDescLocal.setVisible(false);
    }
    else {
      lblDescLocal.setText(res.getString("lblDescLocal.Text") +
                           app.getIdiomaLocal() + "):");
    }

    InicializarTPregunta();

    // recupera los datos de la pregunta
    if (pregunta != null) {
      txtCodPregunta.setText(pregunta.getCodPregunta());
      txtDescPregunta.setText(pregunta.getDescPregunta());
      txtDescLPregunta.setText(pregunta.getDescLPregunta());
      txtListaValores.setText(pregunta.getListaValores());
      txtDescLista.setText(pregunta.getDescLista());
      txtLongitud.setText(Integer.toString(pregunta.getLongitud()));
      txtEnteros.setText(Integer.toString(pregunta.getEnteros()));
      txtDecimales.setText(Integer.toString(pregunta.getDecimales()));
      sTipoPregunta = pregunta.getTipo();
      SeleccionaTipo(sTipoPregunta);

      // consulta si la pregunta est� seleccionada un modelo
      String sStatus = res.getString("msg3.Text");

      CLista param = new CLista();
      param.addElement(new DataMantPreguntas(pregunta.getCodPregunta()));
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
      param = (CLista) stubCliente.doPost(servletPREGUNTA_SELECCIONADA, param);

      // rellena los datos
      int modo = modoOperacion;
      modoOperacion = modoCONSULTA;

      if (param.size() > 0) {
        sStatus = sStatus + (String) param.firstElement();

        if ( ( (String) param.firstElement()).equals("N")) {
          modoOperacion = modo;

        }
      }
      else {
        sStatus = sStatus + "S";
      }

      statusBar.setText(sStatus);
    }

    Inicializar();
  }

  // modo inicializar
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:

        // modo alta
        setTitle(res.getString("msg4.Text"));
        txtCodPregunta.setEnabled(true);
        txtDescPregunta.setEnabled(true);
        txtDescLPregunta.setEnabled(true);
        choiceTipo.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        InicializaTipoPregunta();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        setTitle(res.getString("msg5.Text"));
        txtCodPregunta.setEnabled(false);
        txtDescPregunta.setEnabled(true);
        txtDescLPregunta.setEnabled(true);
        InicializaTipoPregunta();
        if (sTipoPregunta.equals(DataMantPreguntas.tipoLISTA)) {
          txtListaValores.setEnabled(true);
          btnCtrlListaValores.setEnabled(true);
          btnLista.setEnabled(true); //*****************
        }
        else {
          txtListaValores.setEnabled(false);
          btnCtrlListaValores.setEnabled(false);
          btnLista.setEnabled(false); //*****************
        }
        choiceTipo.setEnabled(true); //E
        //txtLongitud.setEnabled(false); //E
        //txtEnteros.setEnabled(false);  //E
        //txtDecimales.setEnabled(false); //E
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoCONSULTA:
        setTitle(res.getString("msg6.Text"));
        txtCodPregunta.setEnabled(false);
        txtDescPregunta.setEnabled(false);
        txtDescLPregunta.setEnabled(false);
        //choiceTipo.setEnabled(false); //E
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtListaValores.setEnabled(false);
        btnCtrlListaValores.setEnabled(false);
        btnLista.setEnabled(false);
        choiceTipo.setEnabled(false);
        txtLongitud.setEnabled(false);
        txtEnteros.setEnabled(false);
        txtDecimales.setEnabled(false);
        btnAceptar.setVisible(false);
        btnCancelar.setEnabled(true);
        btnCancelar.setLabel(res.getString("btnCancelar.Label"));
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
        setTitle(res.getString("msg7.Text"));
        txtCodPregunta.setEnabled(false);
        txtDescPregunta.setEnabled(false);
        txtDescLPregunta.setEnabled(false);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtListaValores.setEnabled(false);
        btnCtrlListaValores.setEnabled(false);
        btnLista.setEnabled(false);
        choiceTipo.setEnabled(false);
        txtLongitud.setEnabled(false);
        txtEnteros.setEnabled(false);
        txtDecimales.setEnabled(false);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        // modo espera
        txtCodPregunta.setEnabled(false);
        txtDescPregunta.setEnabled(false);
        txtDescLPregunta.setEnabled(false);
        txtListaValores.setEnabled(false);
        btnCtrlListaValores.setEnabled(false);
        btnLista.setEnabled(false);
        choiceTipo.setEnabled(false); //E
        //txtLongitud.setEnabled(false); //E
        //txtEnteros.setEnabled(false);  //E
        //txtDecimales.setEnabled(false); //E
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // gesti�n del tipo de pregunta
  void InicializaTipoPregunta() {
    if (sTipoPregunta.equals(DataMantPreguntas.tipoBOOLEANO)) {
      txtLongitud.setEnabled(false);
      txtDecimales.setEnabled(false);
      txtEnteros.setEnabled(false);
      txtListaValores.setEnabled(false);
      btnCtrlListaValores.setEnabled(false);
      btnLista.setEnabled(false);
    }
    else if (sTipoPregunta.equals(DataMantPreguntas.tipoCADENA)) {
      txtLongitud.setEnabled(true);
      txtDecimales.setEnabled(false);
      txtEnteros.setEnabled(false);
      txtListaValores.setEnabled(false);
      btnCtrlListaValores.setEnabled(false);
      btnLista.setEnabled(false);
    }
    else if (sTipoPregunta.equals(DataMantPreguntas.tipoFECHA)) {
      txtLongitud.setEnabled(false);
      txtDecimales.setEnabled(false);
      txtEnteros.setEnabled(false);
      txtListaValores.setEnabled(false);
      btnCtrlListaValores.setEnabled(false);
      btnLista.setEnabled(false);
    }
    else if (sTipoPregunta.equals(DataMantPreguntas.tipoLISTA)) {
      txtLongitud.setEnabled(false);
      txtDecimales.setEnabled(false);
      txtEnteros.setEnabled(false);
      txtListaValores.setEnabled(true);
      btnCtrlListaValores.setEnabled(true);
      btnLista.setEnabled(true);
    }
    else if (sTipoPregunta.equals(DataMantPreguntas.tipoNUMERICO)) {
      txtLongitud.setEnabled(false);
      txtDecimales.setEnabled(true);
      txtEnteros.setEnabled(true);
      txtListaValores.setEnabled(false);
      btnCtrlListaValores.setEnabled(false);
      btnLista.setEnabled(false);
    }
    else {
      txtLongitud.setEnabled(false);
      txtDecimales.setEnabled(false);
      txtEnteros.setEnabled(false);
      txtListaValores.setEnabled(false);
      btnCtrlListaValores.setEnabled(false);
      btnLista.setEnabled(false);
    }
  }

  // selecciona el tipo en el choice de tipos
  void SeleccionaTipo(String sTipo) {
    int j;

    for (j = 0; j < listaTipos.size(); j++) {
      if (sTipo.equals( ( (DataCat) listaTipos.elementAt(j)).getCod())) {
        break;
      }
    }
    choiceTipo.select(j);
  }

  // a�adir
  void A�adir() {
    CMessage msgBox;
    CLista data = null;
    String miDato = "";

    if (this.isDataValid()) {

      // modo espera
      this.modoOperacion = modoESPERA;
      Inicializar();

      try {
        iLongitud = 0;
        iDecimales = 0;
        iEnteros = 0;
        if (txtLongitud.getText().length() > 0) {
          iLongitud = Integer.parseInt(txtLongitud.getText());
        }
        if (txtEnteros.getText().length() > 0) {
          iEnteros = Integer.parseInt(txtEnteros.getText());
        }
        if (txtDecimales.getText().length() > 0) {
          iDecimales = Integer.parseInt(txtDecimales.getText());

          // par�metros de envio
        }
        data = new CLista();
        data.setLogin(this.app.getLogin());

        switch (this.app.getCA().length()) {
          case 0:
            miDato = "_00";
            break;
          case 1:
            miDato = "_0" + this.app.getCA();
            break;
          case 2:
            miDato = "_" + this.app.getCA();
            break;
        }
        pregunta = new DataMantPreguntas(app.getTSive(),
                                         txtCodPregunta.getText() + miDato,
                                         txtDescPregunta.getText(),
                                         txtDescLPregunta.getText(),
                                         txtListaValores.getText(),
                                         sTipoPregunta,
                                         iLongitud,
                                         iEnteros,
                                         iDecimales);

        // a�ade la nueva pregunta
        data.addElement(pregunta);
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        stubCliente.doPost(modoALTA, data);

        dispose();
      }
      catch (Exception ex) {
        this.modoOperacion = modoALTA;
        ex.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
        msgBox.show();
        msgBox = null;

      } //catch
      modoOperacion = modoALTA;
      Inicializar();
    } //if is DataValid
  }

  // modificar
  void Modificar() {
    CMessage msgBox;
    CLista data = null;

    if (this.isDataValid()) {

      // modo espera
      this.modoOperacion = modoESPERA;
      Inicializar();

      //modifica la enfernmedad
      try {
        iLongitud = 0;
        iDecimales = 0;
        iEnteros = 0;
        if (txtLongitud.getText().length() > 0) {
          iLongitud = Integer.parseInt(txtLongitud.getText());
        }
        if (txtEnteros.getText().length() > 0) {
          iEnteros = Integer.parseInt(txtEnteros.getText());
        }
        if (txtDecimales.getText().length() > 0) {
          iDecimales = Integer.parseInt(txtDecimales.getText());

          // par�metros de envio
        }
        data = new CLista();
        data.setLogin(this.app.getLogin());
        data.setTSive(app.getTSive()); //no hace falta, va en el data

        pregunta = new DataMantPreguntas(app.getTSive(),
                                         txtCodPregunta.getText(),
                                         txtDescPregunta.getText(),
                                         txtDescLPregunta.getText(),
                                         txtListaValores.getText(),
                                         sTipoPregunta,
                                         iLongitud,
                                         iEnteros,
                                         iDecimales);

        // a�ade la nueva pregunta
        data.addElement(pregunta);
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        stubCliente.doPost(modoMODIFICAR, data);
        dispose();
      }
      catch (Exception ex) {
        ex.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
        msgBox.show();
        msgBox = null;

      } //catch

      // pasa a modo modificar
      this.modoOperacion = modoMODIFICAR;
      Inicializar();
    }
  }

  // borrar
  void Borrar() {
    CMessage msgBox = null;
    CLista data = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA, res.getString("msg8.Text"));
    msgBox.show();

    if (msgBox.getResponse()) {
      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.setTSive(app.getTSive()); //la PK: TSIVE + CD_PERGUNTA
        data.addElement(new DataMantPreguntas(txtCodPregunta.getText()));

        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        stubCliente.doPost(servletBAJA, data);

        dispose();
      }
      catch (Exception ex) {
        ex.printStackTrace();
        msgBox = new CMessage(app, CMessage.msgERROR, ex.getMessage());
        msgBox.show();
        msgBox = null;

      } //catch
      this.modoOperacion = modoBAJA;
      Inicializar();
    }
    else {
      msgBox = null;
    } //else                    */
  }

  /* // limpia la pantalla
   void vaciarTextoPantalla()
   {
     txtCodPregunta.setText("");
     txtDescPregunta.setText("");
     txtDescLPregunta.setText("");
     txtLongitud.setText("0");
     txtEnteros.setText("0");
     txtDecimales.setText("0");
     txtListaValores.setText("");
     txtDescLista.setText("");
     choiceTipo.select(0);
     if (listaTipos.size() >0)
       sTipoPregunta = ((DataCat)listaTipos.elementAt(0)).getCod();
   }  */

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {

    CMessage msgBox;
    String msg = null;
    boolean b = true;

    // comprueba que esten informados los campos obligatorios
    if (sTipoPregunta.equals(DataMantPreguntas.tipoCADENA)) {

      // cadenas de longitud > 0 y < max
      if (txtLongitud.getText().length() > 0) {

        b = true;
      }
      else {
        b = false;
        msg = res.getString("msg9.Text");
      }
      // comprueba la longitud m�xima de los campos
      if ( (b == true) && ( (txtLongitud.getText().length()) > 3)) {
        b = false;
        msg = res.getString("msg10.Text");
        txtLongitud.selectAll();
      }

      // comprueba el valor indicado en los campos
      if ( (b == true) &&
          ( (Integer.parseInt(txtLongitud.getText()) > maxLONGITUD_CADENA) ||
           (Integer.parseInt(txtLongitud.getText()) < 1))
          ) {
        b = false;
        msg = res.getString("msg11.Text");
        txtLongitud.selectAll();
      }

    }
    else if (sTipoPregunta.equals(DataMantPreguntas.tipoLISTA)) {

      // lista seleccionada
      if (txtDescLista.getText().length() > 0) {
        b = true;
      }
      else {
        b = false;
        msg = res.getString("msg9.Text");
      }

    }
    else if (sTipoPregunta.equals(DataMantPreguntas.tipoNUMERICO)) {

      // numeros con decimales y enteros y de longitud < max
      if ( (txtDecimales.getText().length() > 0) &&
          (txtEnteros.getText().length() > 0)
          ) {
        if ( (Integer.parseInt(txtDecimales.getText()) == 0) &&
            (Integer.parseInt(txtEnteros.getText()) == 0)) {
          b = false;
          msg = res.getString("msg9.Text");
        }
        else {
          b = true;
        }
      }
      else {
        b = false;
        msg = res.getString("msg9.Text");
      }

      // comprueba la longitud m�xima de los campos
      if ( (b == true) && ( (txtEnteros.getText().length()) > 3)) {
        b = false;
        msg = res.getString("msg10.Text");
        txtEnteros.selectAll();
      }
      // comprueba la longitud m�xima de los campos
      if ( (b == true) && ( (txtDecimales.getText().length()) > 3)) {
        b = false;
        msg = res.getString("msg10.Text");
        txtDecimales.selectAll();
      }

      // comprueba el valor indicado en los campos
      if ( (b == true) &&
          ( (Integer.parseInt(txtEnteros.getText()) +
             Integer.parseInt(txtDecimales.getText()) >
             maxLONGITUD_CADENA - 1) /* Por la coma decimal */ ||
           (Integer.parseInt(txtEnteros.getText()) +
            Integer.parseInt(txtDecimales.getText()) < 1))

          ) {
        b = false;
        msg = res.getString("msg11.Text");
        txtDecimales.selectAll();
        txtEnteros.selectAll();
      }

    }

    // campos m�nimos en cualquier caso
    if (b == true) {
      if ( (txtCodPregunta.getText().length() > 0) &&
          (txtDescPregunta.getText().length() > 0)) {
        b = true;
      }
      else {
        b = false;
        msg = res.getString("msg9.Text");
      }
    }

    // comprueba la longitud m�xima de los campos
    if (txtCodPregunta.getText().length() > 8) {
      b = false;
      msg = res.getString("msg10.Text");
      txtCodPregunta.selectAll();

    }
    if (txtDescPregunta.getText().length() > 40) {
      b = false;
      msg = res.getString("msg10.Text");
      txtDescPregunta.selectAll();

    }

    if (txtDescLPregunta.getText().length() > 40) {
      b = false;
      msg = res.getString("msg10.Text");
      txtDescLPregunta.selectAll();

    }

    if (b == false) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      //#// System_out.println("Men Aviso" + msg);
      msgBox.show();
      msgBox = null;
    }
    valido = b;
    return b; //Devolvemos true No hay error
  }

  // obtener una lista de valores
  void focusLost() {

    CMessage msgBox = null;
    DataMantlis datMantlis = null;
    CLista datos = null;
    String sDes;
    int modoAnterior;

    modoAnterior = modoOperacion;

    if (txtListaValores.getText().length() > 0) {

      try {

        // par�metros
        datos = new CLista();
        datos.addElement(new DataMantlis(txtListaValores.getText()));

        // modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_AUX));
        datos = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO, datos);

        //determina el modo del applet
        if (datos.size() > 0) {
          // escribe la descripci�n de la lista
          datMantlis = (DataMantlis) datos.firstElement();
          sDes = datMantlis.getDslista();
          if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (datMantlis.getDsllista().length() > 0)) {
            sDes = datMantlis.getDsllista();
          }
          txtDescLista.setText(sDes);
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg12.Text"));
          msgBox.show();
          msgBox = null;
        }
      } //try
      catch (Exception ex) {
        ex.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
        msgBox.show();
        msgBox = null;
      }

      modoOperacion = modoAnterior;
      Inicializar();
    }
  }

  // buscar una lista de valores
  void btnCtrlListaValores_actionPerformed(ActionEvent e) {

    String sDes;
    DataMantlis data;
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    CListaLisValores listaLisValores = new CListaLisValores(app,
        res.getString("msg13.Text"),
        stubCliente,
        strSERVLET_AUX,
        servletOBTENER_X_CODIGO,
        servletOBTENER_X_DESCRIPCION,
        servletSELECCION_X_CODIGO,
        servletSELECCION_X_DESCRIPCION);
    listaLisValores.show();

    data = (DataMantlis) listaLisValores.getComponente();
    listaLisValores = null;

    if (data != null) {
      txtListaValores.setText(data.getCdlista());

      sDes = data.getDslista();
      // cambia al idioma local, si procede
      if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
          (data.getDsllista() != null)) {

        //                (data.getDsllista().length() > 0))

        sDes = data.getDsllista();
      }
      txtDescLista.setText(sDes);

    }
    modoOperacion = modo;
    Inicializar();

  }

  void txtListaValores_keyPressed(KeyEvent e) {
    txtDescLista.setText("");
  }

  // carga la lista TPregunta
  protected void InicializarTPregunta() throws Exception {
    CLista data = null;

    // obtiene la lista
    listaTipos = new CLista();

    data = new CLista();
    data.addElement(new DataCat(""));

    // invoca al servlet
    stubCliente.setUrl(new URL(app.getURL() + strSERVLET_CAT));
    listaTipos = (CLista) stubCliente.doPost(servletSELECCION_X_CODIGO +
                                             Catalogo.catTPREGUNTA, data);

    // rellena los tipos de preguntas
    for (int j = 0; j < listaTipos.size(); j++) {

      /*
            if  ((app.getIdioma() == CApp.idiomaPORDEFECTO)
              choiceTipo.add( ((DataCat)listaTipos.elementAt(j)).getDes());
            else
              choiceTipo.add( ((DataCat)listaTipos.elementAt(j)).getDesL());
       */

      if ( ( (app.getIdioma() == CApp.idiomaPORDEFECTO) ||
            ( ( (DataCat) listaTipos.elementAt(j)).getDesL().equals("")))) {
        choiceTipo.add( ( (DataCat) listaTipos.elementAt(j)).getDes());
      }
      else {
        choiceTipo.add( ( (DataCat) listaTipos.elementAt(j)).getDesL());

        // selecciona la primera
      }
      if (j == 0) {
        choiceTipo.select(j);
        sTipoPregunta = ( (DataCat) listaTipos.elementAt(j)).getCod();
      }
    }
  }

  // cambio de tipo de respuesta
  public void choiceTipos_itemStateChanged(ItemEvent e) {
    sTipoPregunta = ( (DataCat) listaTipos.elementAt(choiceTipo.
        getSelectedIndex())).getCod();
    txtLongitud.setText("0");
    txtDecimales.setText("0");
    txtEnteros.setText("0");
    txtListaValores.setText("");
    txtDescLista.setText("");
    InicializaTipoPregunta();
  }

  // insertar lista
  //Para bot�n auxiliar de inserci�n de  una lista sin tener que volver al men� principal
  public void InsertarLista() {

    String sDes;
    DataMantlis data = null;

    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();
    //Di�logo para poder insertar una lista de valores nueva
    PanelLista panel = new PanelLista(this.app,
                                      PanelLista.modoALTA,
                                      null);
    panel.show();

    if ( (panel.bAceptar) && (panel.valido)) {
      //Si se ha introducido lista nueva, por defecto se lleva a esta pregunta
      data = panel.datLista;
      txtListaValores.setText(data.getCdlista());
      sDes = data.getDslista();
      // cambia al idioma local, si procede
      if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
          (data.getDsllista().length() > 0)) {
        sDes = data.getDsllista();
      }
      txtDescLista.setText(sDes);

    }

    panel = null;

    modoOperacion = modo;
    Inicializar();

  }

} //fin de clase

/**************************Trat de eventos*************************/

// action listener de evento en bot�nes
class PreguntasBtnActionListener
    implements ActionListener, Runnable {
  Pan_MantPreguntas adaptee = null;
  ActionEvent e = null;

  public PreguntasBtnActionListener(Pan_MantPreguntas adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscar")) { // buscar c�d. lista
      adaptee.btnCtrlListaValores_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("lista")) { // insertar lista de valores
      adaptee.InsertarLista();
    }
    else if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case Pan_MantPreguntas.modoALTA:
          adaptee.A�adir();
          break;
        case Pan_MantPreguntas.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case Pan_MantPreguntas.modoBAJA:
          adaptee.Borrar();
          break;
      }

    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }
  }
}

// control de valor num�rico en las cajas
class PreguntasTxtKeyListener
    implements java.awt.event.KeyListener {
  Pan_MantPreguntas adaptee;

  PreguntasTxtKeyListener(Pan_MantPreguntas adaptee) {
    this.adaptee = adaptee;
  }

  public void keyTyped(KeyEvent e) {
  }

  // s�lo permite valores num�ricos
  public void keyPressed(KeyEvent e) {

    switch (e.getKeyCode()) {

      // teclas permitidas
      case KeyEvent.VK_0:
      case KeyEvent.VK_NUMPAD0: //E
      case KeyEvent.VK_1:
      case KeyEvent.VK_NUMPAD1: //E
      case KeyEvent.VK_2:
      case KeyEvent.VK_NUMPAD2: //E
      case KeyEvent.VK_3:
      case KeyEvent.VK_NUMPAD3: //E
      case KeyEvent.VK_4:
      case KeyEvent.VK_NUMPAD4: //E
      case KeyEvent.VK_5:
      case KeyEvent.VK_NUMPAD5: //E
      case KeyEvent.VK_6:
      case KeyEvent.VK_NUMPAD6: //E
      case KeyEvent.VK_7:
      case KeyEvent.VK_NUMPAD7: //E
      case KeyEvent.VK_8:
      case KeyEvent.VK_NUMPAD8: //E
      case KeyEvent.VK_9:
      case KeyEvent.VK_NUMPAD9: //E
      case KeyEvent.VK_DELETE:
      case KeyEvent.VK_TAB:
      case KeyEvent.VK_BACK_SPACE:
        break;

        // teclas no permitidas
      default:
        e.setKeyCode(KeyEvent.VK_ENTER);
    }
  }

  public void keyReleased(KeyEvent e) {
  }
}

// selecci�n de la choice de tipos de pregunta
class PreguntasChoiceItemListener
    implements java.awt.event.ItemListener {
  Pan_MantPreguntas adaptee;

  PreguntasChoiceItemListener(Pan_MantPreguntas adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.choiceTipos_itemStateChanged(e);
  }
}

// lista de valores
class CListaLisValores
    extends CListaValores {

  public CListaLisValores(CApp a,
                          String title,
                          StubSrvBD stub,
                          String servlet,
                          int obtener_x_codigo,
                          int obtener_x_descricpcion,
                          int seleccion_x_codigo,
                          int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataMantlis(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataMantlis) o).getCdlista());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMantlis) o).getDslista());
  }
}

class keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_MantPreguntas adaptee;

  keyAdapter(Pan_MantPreguntas adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtListaValores_keyPressed(e);
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_MantPreguntas adaptee;

  focusAdapter(Pan_MantPreguntas adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost();
  }
}
