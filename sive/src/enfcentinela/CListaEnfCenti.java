package enfcentinela;

import capp.CApp;
import capp.CListaValores;
import sapp.StubSrvBD;

// lista de valores de Enfermedades Centinelas
public class CListaEnfCenti
    extends CListaValores {

  public CListaEnfCenti(CApp a,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {

    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEnfCenti(s, s); // El servlet se encarga de tomar la cadena
    // necesaria.
  }

  public String getCodigo(Object o) {
    return ( ( (DataEnfCenti) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnfCenti) o).getDes());
  }
}
