package gba.bios;

import gba.cpu.CPU;
import gba.debug.Dumper;




public final class BIOS
{

    static char sca[] = {
        '\0', '\u0192', '\u0323', '\u04B5', '\u0645', '\u07D5', '\u0964', '\u0AF1', '\u0C7C', '\u0E05', 
        '\u0F8C', '\u1111', '\u1294', '\u1413', '\u158F', '\u1708', '\u187D', '\u19EF', '\u1B5D', '\u1CC6', 
        '\u1E2B', '\u1F8B', '\u20E7', '\u223D', '\u238E', '\u24DA', '\u261F', '\u275F', '\u2899', '\u29CD', 
        '\u2AFA', '\u2C21', '\u2D41', '\u2E5A', '\u2F6B', '\u3076', '\u3179', '\u3274', '\u3367', '\u3453', 
        '\u3536', '\u3612', '\u36E5', '\u37AF', '\u3871', '\u392A', '\u39DA', '\u3A82', '\u3B20', '\u3BB6', 
        '\u3C42', '\u3CC5', '\u3D3E', '\u3DAE', '\u3E14', '\u3E71', '\u3EC5', '\u3F0E', '\u3F4E', '\u3F84', 
        '\u3FB1', '\u3FD3', '\u3FEC', '\u3FFB', '\u4000', '\u3FFB', '\u3FEC', '\u3FD3', '\u3FB1', '\u3F84', 
        '\u3F4E', '\u3F0E', '\u3EC5', '\u3E71', '\u3E14', '\u3DAE', '\u3D3E', '\u3CC5', '\u3C42', '\u3BB6', 
        '\u3B20', '\u3A82', '\u39DA', '\u392A', '\u3871', '\u37AF', '\u36E5', '\u3612', '\u3536', '\u3453', 
        '\u3367', '\u3274', '\u3179', '\u3076', '\u2F6B', '\u2E5A', '\u2D41', '\u2C21', '\u2AFA', '\u29CD', 
        '\u2899', '\u275F', '\u261F', '\u24DA', '\u238E', '\u223D', '\u20E7', '\u1F8B', '\u1E2B', '\u1CC6', 
        '\u1B5D', '\u19EF', '\u187D', '\u1708', '\u158F', '\u1413', '\u1294', '\u1111', '\u0F8C', '\u0E05', 
        '\u0C7C', '\u0AF1', '\u0964', '\u07D5', '\u0645', '\u04B5', '\u0323', '\u0192', '\0', '\uFE6E', 
        '\uFCDD', '\uFB4B', '\uF9BB', '\uF82B', '\uF69C', '\uF50F', '\uF384', '\uF1FB', '\uF074', '\uEEEF', 
        '\uED6C', '\uEBED', '\uEA71', '\uE8F8', '\uE783', '\uE611', '\uE4A3', '\uE33A', '\uE1D5', '\uE075', 
        '\uDF19', '\uDDC3', '\uDC72', '\uDB26', '\uD9E1', '\uD8A1', '\uD767', '\uD633', '\uD506', '\uD3DF', 
        '\uD2BF', '\uD1A6', '\uD095', '\uCF8A', '\uCE87', '\uCD8C', '\uCC99', '\uCBAD', '\uCACA', '\uC9EE', 
        '\uC91B', '\uC851', '\uC78F', '\uC6D6', '\uC626', '\uC57E', '\uC4E0', '\uC44A', '\uC3BE', '\uC33B', 
        '\uC2C2', '\uC252', '\uC1EC', '\uC18F', '\uC13B', '\uC0F2', '\uC0B2', '\uC07C', '\uC04F', '\uC02D', 
        '\uC014', '\uC005', '\uC000', '\uC005', '\uC014', '\uC02D', '\uC04F', '\uC07C', '\uC0B2', '\uC0F2', 
        '\uC13B', '\uC18F', '\uC1EC', '\uC252', '\uC2C2', '\uC33B', '\uC3BE', '\uC44A', '\uC4E0', '\uC57E', 
        '\uC626', '\uC6D6', '\uC78F', '\uC851', '\uC91B', '\uC9EE', '\uCACA', '\uCBAD', '\uCC99', '\uCD8C', 
        '\uCE87', '\uCF8A', '\uD095', '\uD1A6', '\uD2BF', '\uD3DF', '\uD506', '\uD633', '\uD767', '\uD8A1', 
        '\uD9E1', '\uDB26', '\uDC72', '\uDDC3', '\uDF19', '\uE075', '\uE1D5', '\uE33A', '\uE4A3', '\uE611', 
        '\uE783', '\uE8F8', '\uEA71', '\uEBED', '\uED6C', '\uEEEF', '\uF074', '\uF1FB', '\uF384', '\uF50F', 
        '\uF69C', '\uF82B', '\uF9BB', '\uFB4B', '\uFCDD', '\uFE6E'
    };

    public BIOS()
    {
    }

    static void bios0x0F_ObjAffineSet(CPU cpu)
    {
        int reg0 = cpu.reg[0];
        int reg1 = cpu.reg[1];
        int reg2 = cpu.reg[2];
        int reg3 = cpu.reg[3];
        char seg0[] = cpu.memoria.vals[reg0 >> 24 & 0xff];
        char seg1[] = cpu.memoria.vals[reg1 >> 24 & 0xff];
        reg0 &= 0xffffff;
        reg1 &= 0xffffff;
        for(int reg2_index = 0; reg2_index < reg2; reg2_index++)
        {
            short w01 = (short)(seg0[reg0 + 1] << 8 | seg0[reg0]);
            reg0 += 2;
            short w02 = (short)(seg0[reg0 + 1] << 8 | seg0[reg0]);
            reg0 += 2;
            char c = seg0[reg0 + 1];
            reg0 += 4;
            short word0 = (short)sca[c + 64 & 0xff];
            short word1 = (short)sca[c];
            short word4 = (short)(w01 * word0 >> 14);
            short word5 = (short)(w01 * word1 >> 14);
            word5 = (short)(-word5);
            short word6 = (short)(w02 * word1 >> 14);
            short word7 = (short)(w02 * word0 >> 14);
            seg1[reg1] = (char)(word4 & 0xff);
            seg1[reg1 + 1] = (char)(word4 >> 8 & 0xff);
            reg1 += reg3;
            seg1[reg1] = (char)(word5 & 0xff);
            seg1[reg1 + 1] = (char)(word5 >> 8 & 0xff);
            reg1 += reg3;
            seg1[reg1] = (char)(word6 & 0xff);
            seg1[reg1 + 1] = (char)(word6 >> 8 & 0xff);
            reg1 += reg3;
            seg1[reg1] = (char)(word7 & 0xff);
            seg1[reg1 + 1] = (char)(word7 >> 8 & 0xff);
            reg1 += reg3;
        }

    }

    static void bios0x0E_BgAffineSet(CPU cpu)
    {
        int r0 = cpu.reg[0];
        int r1 = cpu.reg[1];
        int r2 = cpu.reg[2];
        char seg1[] = cpu.memoria.vals[r0 >> 24 & 0xff];
        char seg2[] = cpu.memoria.vals[r1 >> 24 & 0xff];
        r0 &= 0xffffff;
        r1 &= 0xffffff;
        for(int i = 0; i < r2; i++)
        {
            int addr1 = seg1[r0 + 3] << 24 | seg1[r0 + 2] << 16 | seg1[r0 + 1] << 8 | seg1[r0];
            r0 += 4;
            int addr2 = seg1[r0 + 3] << 24 | seg1[r0 + 2] << 16 | seg1[r0 + 1] << 8 | seg1[r0];
            r0 += 4;
            short par1 = (short)(seg1[r0 + 1] << 8 | seg1[r0]);
            r0 += 2;
            short par2 = (short)(seg1[r0 + 1] << 8 | seg1[r0]);
            r0 += 2;
            short par3 = (short)(seg1[r0 + 1] << 8 | seg1[r0]);
            r0 += 2;
            short par4 = (short)(seg1[r0 + 1] << 8 | seg1[r0]);
            r0 += 2;
            char par5 = seg1[r0 + 1];
            r0 += 4;
            short trx1 = (short)sca[par5 + 64 & 0xff];
            short trx2 = (short)sca[par5];
            short word6 = (short)(par3 * trx1 >> 14);
            short word7 = (short)(par3 * trx2 >> 14);
            word7 = (short)(-word7);
            short word8 = (short)(par4 * trx2 >> 14);
            short word9 = (short)(par4 * trx1 >> 14);
            seg2[r1] = (char)(word6 & 0xff);
            seg2[r1 + 1] = (char)(word6 >> 8 & 0xff);
            r1 += 2;
            seg2[r1] = (char)(word7 & 0xff);
            seg2[r1 + 1] = (char)(word7 >> 8 & 0xff);
            r1 += 2;
            seg2[r1] = (char)(word8 & 0xff);
            seg2[r1 + 1] = (char)(word8 >> 8 & 0xff);
            r1 += 2;
            seg2[r1] = (char)(word9 & 0xff);
            seg2[r1 + 1] = (char)(word9 >> 8 & 0xff);
            r1 += 2;
            int l1 = (addr1 - word6 * par1) + word7 * par2;
            int i2 = addr2 - word8 * par1 - word9 * par2;
            seg2[r1] = (char)(l1 & 0xff);
            seg2[r1 + 1] = (char)(l1 >> 8 & 0xff);
            seg2[r1 + 2] = (char)(l1 >> 16 & 0xff);
            seg2[r1 + 3] = (char)(l1 >> 24 & 0xff);
            r1 += 4;
            seg2[r1] = (char)(i2 & 0xff);
            seg2[r1 + 1] = (char)(i2 >> 8 & 0xff);
            seg2[r1 + 2] = (char)(i2 >> 16 & 0xff);
            seg2[r1 + 3] = (char)(i2 >> 24 & 0xff);
            r1 += 4;
        }

    }

    static void bios0x06_Div(CPU cpu)
    {
        int r0 = cpu.reg[0];
        int r1 = cpu.reg[1];
        int res = r0 / r1;
        cpu.reg[0] = res;
        cpu.reg[1] = r0 - res * r1;
        cpu.reg[3] = res >= 0 ? res : -res;
    }

    static void bios0x07_DivArm(CPU cpu)
    {
        int i = cpu.reg[1];
        int k = cpu.reg[0];
        int l = i / k;
        cpu.reg[0] = l;
        cpu.reg[1] = i - l * k;
        cpu.reg[3] = l >= 0 ? l : -l;
    }

    static void bios0x08_Sqrt(CPU cpu)
    {
        cpu.reg[0] = (int)Math.sqrt(cpu.reg[0]) & 0xffff;
    }

    static void bios0x0B_CpuSet(CPU cpu)
    {
        int r0 = cpu.reg[0];
        int r1 = cpu.reg[1];
        int r2 = cpu.reg[2];
        int i1 = r2 & 0xffffff;
        char seg0[] = cpu.memoria.vals[r0 >> 24 & 0xff];
        char seg1[] = cpu.memoria.vals[r1 >> 24 & 0xff];
        switch(r2 >> 24 & 0xf)
        {
        case 0: // '\0'
            r0 &= 0xfffffe;
            r1 &= 0xfffffe;
            i1 <<= 1;
            for(int j1 = i1; --j1 >= 0;)
                seg1[r1++] = seg0[r0++];

            break;

        case 1: // '\001'
            r0 &= 0xfffffe;
            r1 &= 0xfffffe;
            char c = seg0[r0];
            i1 <<= 1;
            for(int k1 = i1; --k1 >= 0;)
                seg1[r1++] = c;

            break;

        case 4: // '\004'
            r0 &= 0xfffffc;
            r1 &= 0xfffffc;
            i1 <<= 2;
            for(int l1 = i1; --l1 >= 0;)
                seg1[r1++] = seg0[r0++];

            break;

        case 5: // '\005'
            r0 &= 0xfffffc;
            r1 &= 0xfffffc;
            char c1 = seg0[r0];
            i1 <<= 2;
            for(int i2 = i1; --i2 >= 0;)
                seg1[r1++] = c1;

            break;

        case 2: // '\002'
        case 3: // '\003'
        default:
        	Dumper.cpuStatus(cpu);
            cpu.log.put("BIOS 0x0B CPUSet Error!!! (Segmentos 2 o 3)");
            break;
        }
    }

    static void bios0x0C_CpuFastSet(CPU cpu)
    {
        int i = cpu.reg[0];
        int k = cpu.reg[1];
        int l = cpu.reg[2];
        int i1 = (l & 0xffffff) << 2;
        char ac[] = cpu.memoria.vals[i >> 24 & 0xff];
        char ac1[] = cpu.memoria.vals[k >> 24 & 0xff];
        i &= 0xfffffc;
        k &= 0xfffffc;
        switch(l >> 24 & 0xf)
        {
        case 0: // '\0'
            for(int j1 = i1; --j1 >= 0;)
                ac1[k++] = ac[i++];

            break;

        case 1: // '\001'
            char c = ac[i];
            for(int k1 = i1; --k1 >= 0;)
                ac1[k++] = c;

            break;

        default:
        	Dumper.cpuStatus(cpu);
            cpu.log.put("BIOS 0x0C CpuFastSet ERROR !!!");
            break;
        }
    }

    static void bios0x11_LZ77Uncompr(CPU cpu)
    {
        int i = cpu.reg[0];
        int k = cpu.reg[1];
        char ac[] = cpu.memoria.vals[i >> 24 & 0xff];
        char ac1[] = cpu.memoria.vals[k >> 24 & 0xff];
        i &= 0xfffffc;
        k &= 0xffffff;
        int i1 = ac[i + 3] << 16 | ac[i + 2] << 8 | ac[i + 1];
        i += 4;
        while(i1 > 0) 
        {
            char c = ac[i++];
            for(int j1 = 8; --j1 >= 0;)
            {
                if(i1 <= 0)
                    break;
                if((c >> j1 & 1) != 0)
                {
                    char c1 = ac[i++];
                    int l1 = (c1 >> 4 & 0xf) + 3;
                    int i2 = (c1 & 0xf) << 8;
                    i2 += ac[i++] + 1;
                    int l = k - i2;
                    for(int k1 = 0; k1 < l1; k1++)
                        ac1[k++] = ac1[l + k1 % i2];

                    i1 -= l1;
                } else
                {
                    ac1[k++] = ac[i++];
                    i1--;
                }
            }

        }
    }

    static void bios0x14_RLUncompr(CPU cpu)
    {
        int i = cpu.reg[0];
        int k = cpu.reg[1];
        char ac[] = cpu.memoria.vals[i >> 24 & 0xff];
        char ac1[] = cpu.memoria.vals[k >> 24 & 0xff];
        i &= 0xffffff;
        k &= 0xffffff;
        int l = ac[i + 3] << 16 | ac[i + 2] << 8 | ac[i + 1];
        i += 4;
        while(l > 0) 
        {
            int k1 = ac[i++];
            if((k1 & 0x80) != 0)
            {
                k1 &= 0x7f;
                k1 += 3;
                char c = ac[i++];
                for(int i1 = 0; i1 < k1; i1++)
                    ac1[k++] = c;

                l -= k1;
            } else
            {
                k1 &= 0x7f;
                k1++;
                for(int j1 = 0; j1 < k1; j1++)
                    ac1[k++] = ac[i++];

                l -= k1;
            }
        }
    }

    static void bios0x13_HuffUncompr(CPU cpu)
    {
        int r0 = cpu.reg[0];
        int r1 = cpu.reg[1];
        char ac[] = cpu.memoria.vals[r0 >> 24 & 0xff];
        char ac1[] = cpu.memoria.vals[r1 >> 24 & 0xff];
        r0 &= 0xfffffc;
        r1 &= 0xffffff;
        int l2 = ac[r0] & 0xf;
        int i1 = ac[r0 + 3] << 16 | ac[r0 + 2] << 8 | ac[r0 + 1];
        r0 += 4;
        char c2 = ac[r0++];
        int l = r0;
        r0 += (c2 + 1) * 2 - 1;
        int i3 = 1;
        if(l2 == 8)
        {
            int j2 = 0;
            while(i1 > 0) 
            {
                for(int l1 = 3; l1 >= 0; l1--)
                {
                    char c = ac[r0 + l1];
                    for(int j1 = 7; j1 >= 0; j1--)
                    {
                        if(i1 < 0)
                            break;
                        char c3 = ac[l + j2];
                        j2 += (1 + (c3 & 0x3f)) * 2 - i3;
                        if((c >> j1 & 1) != 0)
                        {
                            j2++;
                            if((c3 & 0x40) != 0)
                            {
                                ac1[r1++] = ac[l + j2];
                                j2 = 0;
                                i1--;
                                i3 = 1;
                            } else
                            {
                                i3 = 1;
                            }
                        } else
                        if((c3 & 0x80) != 0)
                        {
                            ac1[r1++] = ac[l + j2];
                            j2 = 0;
                            i1--;
                            i3 = 1;
                        } else
                        {
                            i3 = 0;
                        }
                    }

                }

                r0 += 4;
            }
        } else
        {
            boolean flag = false;
            int k2 = 0;
            while(i1 > 0) 
            {
                for(int i2 = 3; i2 >= 0; i2--)
                {
                    char c1 = ac[r0 + i2];
                    for(int k1 = 7; k1 >= 0; k1--)
                    {
                        if(i1 <= 0)
                            break;
                        char c4 = ac[l + k2];
                        k2 += (1 + (c4 & 0x3f)) * 2 - i3;
                        if((c1 >> k1 & 1) != 0)
                        {
                            k2++;
                            if((c4 & 0x40) != 0)
                            {
                                if(flag)
                                {
                                    ac1[r1++] |= ac[l + k2] << 4;
                                    i1--;
                                } else
                                {
                                    ac1[r1] = ac[l + k2];
                                }
                                k2 = 0;
                                i3 = 1;
                                flag = !flag;
                            } else
                            {
                                i3 = 1;
                            }
                        } else
                        if((c4 & 0x80) != 0)
                        {
                            if(flag)
                            {
                                ac1[r1++] |= ac[l + k2] << 4;
                                i1--;
                            } else
                            {
                                ac1[r1] = ac[l + k2];
                            }
                            k2 = 0;
                            i3 = 1;
                            flag = !flag;
                        } else
                        {
                            i3 = 0;
                        }
                    }

                }

                r0 += 4;
            }
        }
    }

    static void bios0x16_Diff8bitUnfilter(CPU cpu)
    {
        int r0 = cpu.reg[0];
        int r1 = cpu.reg[1];
        char seg0[] = cpu.memoria.vals[r0 >> 24 & 0xff];
        char seg1[] = cpu.memoria.vals[r1 >> 24 & 0xff];
        r0 &= 0xfffffc;
        r1 &= 0xffffff;
        int l = seg0[r0 + 3] << 16 | seg0[r0 + 2] << 8 | seg0[r0 + 1];
        r0 += 4;
        char c = '\0';
        for(; l > 0; l--)
        {
            c += seg0[r0];
            r0++;
            seg1[r1++] = (char)(c & 0xff);
        }

    }

    static void bios0x18_Diff16bitUnfilter(CPU cpu)
    {
        int i = cpu.reg[0];
        int k = cpu.reg[1];
        char ac[] = cpu.memoria.vals[i >> 24 & 0xff];
        char ac1[] = cpu.memoria.vals[k >> 24 & 0xff];
        i &= 0xfffffc;
        k &= 0xffffff;
        int l = ac[i + 3] << 16 | ac[i + 2] << 8 | ac[i + 1];
        i += 4;
        char c = '\0';
        for(; l > 0; l -= 2)
        {
            c += ac[i + 1] << 8 | ac[i];
            i += 2;
            ac1[k++] = (char)(c & 0xff);
            ac1[k++] = (char)(c >> 8 & 0xff);
        }

    }

    public static void execBIOS(CPU cpu, int i)
    {
        switch(i)
        {
        case 0x02: // '\002'
        case 0x05: // '\005'
            cpu.VBlankIntrWait = true;
            break;

        case 0x06: // '\006'
            bios0x06_Div(cpu);
            break;

        case 0x07: // '\007'
            bios0x07_DivArm(cpu);
            break;

        case 0x08: // '\b'
            bios0x08_Sqrt(cpu);
            break;

        case 0x0B: // '\013'
            bios0x0B_CpuSet(cpu);
            break;

        case 0x0C: // '\f'
            bios0x0C_CpuFastSet(cpu);
            break;

        case 0x0E: // '\016'
            bios0x0E_BgAffineSet(cpu);
            break;

        case 0x0F: // '\017'
            bios0x0F_ObjAffineSet(cpu);
            break;

        case 0x11: // '\021'
        case 0x12: // '\022'
            bios0x11_LZ77Uncompr(cpu);
            break;

        case 0x13: // '\023'
            bios0x13_HuffUncompr(cpu);
            break;

        case 0x14: // '\024'
        case 0x15: // '\025'
            bios0x14_RLUncompr(cpu);
            break;

        case 0x16: // '\026'
        case 0x17: // '\027'
            bios0x16_Diff8bitUnfilter(cpu);
            break;

        case 0x18: // '\030'
            bios0x18_Diff16bitUnfilter(cpu);
            break;

        case 0x03: // '\003'
        case 0x04: // '\004'
        case 0x0D: // '\r'
        case 0x10: // '\020'
        case 0x19: // '\031'
        default:
        	Dumper.cpuStatus(cpu);
        	cpu.log.put("BIOS " + Integer.toHexString(i)+" No implementada");
        	cpu.haltError=true;
            break;

        case 0x00: // '\0'
        case 0x01: // '\001'
        case 0x09: // '\t'
        case 0x0A: // '\n'
        case 0x1A: // '\032'
        case 0x1B: // '\033'
        case 0x1C: // '\034'
        case 0x1D: // '\035'
        case 0x1E: // '\036'
        case 0x1F: // '\037'
            break;
        }
    }
}
