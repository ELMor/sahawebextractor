package gba.video;
import gba.GBAPanel;

public class Video
{

    static final int m1[] = {
        8, 16, 32, 64, 16, 32, 32, 64, 8, 8, 
        16, 32, 0, 0, 0, 0
    };
    static final int m2[] = {
        8, 16, 32, 64, 8, 8, 16, 32, 16, 32, 
        32, 64, 0, 0, 0, 0
    };
    static int m3[] = {
        0, 0
    };
    static int m4[] = {
        0, 0
    };
    static int m5[] = {
        0, 0
    };
    static int m6[] = {
        0, 0
    };
    static int twoFramesVideoBuffer[] = new int[0x10000];

    public Video()
    {
    }

    static int getRGB(int pix)
    {
        int R = pix & 0x1f;
        R <<= 19;
        int G = (pix & 0x3e0) >> 5;
        G <<= 11;
        int B = (pix & 0x7c00) >> 10;
        B <<= 3;
        return R | G | B;
    }

    static void drawSprites(GBAPanel gbaf, int rowOffset, int VCOUNT, int number)
    {
        int offsetOAM = 1024;
        int i1 = 128;
        while(--i1 >= 0) 
        {
            offsetOAM -= 8;
            char ndx05 = gbaf.cpu.memoria.mOAM[offsetOAM + 5];
            if((ndx05 >> 2 & 3) != number)
                continue;
            char ndx01 = gbaf.cpu.memoria.mOAM[offsetOAM + 1];
            if((ndx01 & 3) == 2)
                continue;
            char ndx00 = gbaf.cpu.memoria.mOAM[offsetOAM];
            char ndx03 = gbaf.cpu.memoria.mOAM[offsetOAM + 3];
            int j3 = ndx01 >> 4 & 0xc | ndx03 >> 6 & 3;
            int l3 = m2[j3];
            boolean flag = (ndx01 & 2) != 0;
            int j4;
            if(flag)
                j4 = l3 << 1;
            else
                j4 = l3;
            int hiLimit = ndx00;
            int lowLimit = ndx00 + j4;
            if(hiLimit > 159)
            {
                if(lowLimit <= 256)
                    continue;
                lowLimit &= 0xff;
                if(VCOUNT >= lowLimit)
                    continue;
                hiLimit = 256 - hiLimit;
                ndx00 = '\0';
            } else
            {
                if(VCOUNT < hiLimit || VCOUNT >= lowLimit)
                    continue;
                hiLimit = 0;
            }
            int k3 = m1[j3];
            int i4;
            if(flag)
                i4 = k3 << 1;
            else
                i4 = k3;
            char c6 = gbaf.cpu.memoria.mOAM[offsetOAM + 2];
            int i3 = (c6 | ndx03 << 8) & 0x1ff;
            int l12 = i3;
            int i13 = i3 + i4;
            if(l12 > 239)
            {
                if(i13 <= 512)
                    continue;
                i13 &= 0x1ff;
                l12 = 512 - i3;
                i3 = 0;
            } else
            {
                if(i13 > 240)
                    i13 = 240 - i3;
                else
                    i13 = i4;
                l12 = 0;
            }
            int i7 = (VCOUNT - ndx00) + hiLimit;
            if((ndx01 & '\001') != 0)
            {
                int j18 = (ndx03 >> 1 & 0x1f) << 5;
                int l15 = gbaf.cpu.memoria.mOAM[j18 + 6]  | gbaf.cpu.memoria.mOAM[j18 + 7] << 8;
                int i16 = gbaf.cpu.memoria.mOAM[j18 + 14] | gbaf.cpu.memoria.mOAM[j18 + 15] << 8;
                int j16 = gbaf.cpu.memoria.mOAM[j18 + 22] | gbaf.cpu.memoria.mOAM[j18 + 23] << 8;
                int k16 = gbaf.cpu.memoria.mOAM[j18 + 30] | gbaf.cpu.memoria.mOAM[j18 + 31] << 8;
                if((l15 & 0x8000) != 0)
                    l15 |= 0xffff0000;
                if((i16 & 0x8000) != 0)
                    i16 |= 0xffff0000;
                if((j16 & 0x8000) != 0)
                    j16 |= 0xffff0000;
                if((k16 & 0x8000) != 0)
                    k16 |= 0xffff0000;
                int l16 = (k3 << 8) >> 1;
                int i17 = (l3 << 8) >> 1;
                l16 -= (j4 >> 1) * i16;
                i17 -= (j4 >> 1) * k16;
                l16 -= (i4 >> 1) * l15;
                i17 -= (i4 >> 1) * j16;
                l16 += i7 * i16;
                i17 += i7 * k16;
                l16 += l12 * l15;
                i17 += l12 * j16;
                if((ndx01 & 0x20) != 0)
                {
                    int j7 = (gbaf.cpu.memoria.mOAM[offsetOAM + 4] | ndx05 << 8) & 0x3ff;
                    int j8 = 0x10000 + (j7 << 5);
                    int j9 = (gbaf.cpu.memoria.mIO[0] & 0x40) == 0 ? 16 : k3 >> 3;
                    for(int k4 = 0; k4 < i13; k4++)
                    {
                        int j17 = l16 >> 8;
                        int l17 = i17 >> 8;
                        if(j17 >= 0 && j17 < k3 && l17 >= 0 && l17 < l3)
                        {
                            int k1 = gbaf.cpu.memoria.mVRAM[j8 + (j17 & 7) + ((l17 & 7) << 3) + ((l17 >> 3) * j9 + (j17 >> 3) << 6)];
                            if(k1 != 0)
                            {
                                k1 = k1 + 256 << 1;
                                gbaf.videoMemory[rowOffset + (i3 + k4)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k1 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k1]];
                            }
                        }
                        l16 += l15;
                        i17 += j16;
                    }

                } else
                {
                    int j12 = ndx05 & 0xf0;
                    int k7 = (gbaf.cpu.memoria.mOAM[offsetOAM + 4] | ndx05 << 8) & 0x3ff;
                    int k8 = 0x10000 + (k7 << 5);
                    int k9 = (gbaf.cpu.memoria.mIO[0] & 0x40) == 0 ? 32 : k3 >> 3;
                    for(int l4 = 0; l4 < i13; l4++)
                    {
                        int k17 = l16 >> 8;
                        int i18 = i17 >> 8;
                        if(k17 >= 0 && k17 < k3 && i18 >= 0 && i18 < l3)
                        {
                            int l1 = gbaf.cpu.memoria.mVRAM[k8 + (k17 >> 1 & 3) + ((i18 & 7) << 2) + ((i18 >> 3) * k9 + (k17 >> 3) << 5)];
                            if((k17 & 1) != 0)
                                l1 = l1 >> 4 & 0xf;
                            else
                                l1 &= 0xf;
                            if(l1 != 0)
                            {
                                l1 = l1 + 256 + j12 << 1;
                                gbaf.videoMemory[rowOffset + (i3 + l4)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[l1 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[l1]];
                            }
                        }
                        l16 += l15;
                        i17 += j16;
                    }

                }
            } else
            if((ndx01 & 0x20) != 0)
            {
                int l7 = (gbaf.cpu.memoria.mOAM[offsetOAM + 4] | ndx05 << 8) & 0x3ff;
                int l8 = 0x10000 + (l7 << 5);
                int l9 = (gbaf.cpu.memoria.mIO[0] & 0x40) == 0 ? 16 : k3 >> 3;
                switch(ndx03 & 0x30)
                {
                case 0: // '\0'
                    for(int i5 = 0; i5 < i13; i5++)
                    {
                        int l13 = i5 + l12;
                        int i2 = gbaf.cpu.memoria.mVRAM[l8 + (l13 & 7) + ((i7 & 7) << 3) + ((i7 >> 3) * l9 + (l13 >> 3) << 6)];
                        if(i2 != 0)
                        {
                            i2 = i2 + 256 << 1;
                            gbaf.videoMemory[rowOffset + (i3 + i5)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i2 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i2]];
                        }
                    }

                    break;

                case 16: // '\020'
                    for(int j5 = 0; j5 < i13; j5++)
                    {
                        int i14 = j5 + l12;
                        int j2 = gbaf.cpu.memoria.mVRAM[l8 + (7 - (i14 & 7)) + ((i7 & 7) << 3) + ((i7 >> 3) * l9 + (k3 - 1 - i14 >> 3) << 6)];
                        if(j2 != 0)
                        {
                            j2 = j2 + 256 << 1;
                            gbaf.videoMemory[rowOffset + (i3 + j5)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[j2 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[j2]];
                        }
                    }

                    break;

                case 32: // ' '
                    for(int k5 = 0; k5 < i13; k5++)
                    {
                        int j14 = k5 + l12;
                        int k2 = gbaf.cpu.memoria.mVRAM[l8 + (j14 & 7) + (7 - (i7 & 7) << 3) + ((l3 - 1 - i7 >> 3) * l9 + (j14 >> 3) << 6)];
                        if(k2 != 0)
                        {
                            k2 = k2 + 256 << 1;
                            gbaf.videoMemory[rowOffset + (i3 + k5)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k2 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k2]];
                        }
                    }

                    break;

                case 48: // '0'
                    for(int l5 = 0; l5 < i13; l5++)
                    {
                        int k14 = l5 + l12;
                        int l2 = gbaf.cpu.memoria.mVRAM[l8 + (7 - (k14 & 7)) + (7 - (i7 & 7) << 3) + ((l3 - 1 - i7 >> 3) * l9 + (k3 - 1 - k14 >> 3) << 6)];
                        if(l2 != 0)
                        {
                            l2 = l2 + 256 << 1;
                            gbaf.videoMemory[rowOffset + (i3 + l5)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[l2 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[l2]];
                        }
                    }

                    break;
                }
            } else
            {
                int k12 = ndx05 & 0xf0;
                int i8 = (gbaf.cpu.memoria.mOAM[offsetOAM + 4] | ndx05 << 8) & 0x3ff;
                int i9 = 0x10000 + (i8 << 5);
                int i10 = (gbaf.cpu.memoria.mIO[0] & 0x40) == 0 ? 32 : k3 >> 3;
                switch(ndx03 & 0x30)
                {
                default:
                    break;

                case 0: // '\0'
                    for(int i6 = 0; i6 < i13; i6++)
                    {
                        int l14 = i6 + l12;
                        char c1 = gbaf.cpu.memoria.mVRAM[i9 + (l14 >> 1 & 3) + ((i7 & 7) << 2) + ((i7 >> 3) * i10 + (l14 >> 3) << 5)];
                        int j10 = c1 & 0xf;
                        if(j10 != 0)
                        {
                            j10 = j10 + 256 + k12 << 1;
                            gbaf.videoMemory[rowOffset + (i3 + i6)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[j10 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[j10]];
                        }
                        if(++i6 != i13)
                        {
                            int k10 = c1 >> 4 & 0xf;
                            if(k10 != 0)
                            {
                                k10 = k10 + 256 + k12 << 1;
                                gbaf.videoMemory[rowOffset + (i3 + i6)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k10 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k10]];
                            }
                        }
                    }

                    break;

                case 16: // '\020'
                    for(int j6 = 0; j6 < i13; j6++)
                    {
                        int i15 = j6 + l12;
                        char c2 = gbaf.cpu.memoria.mVRAM[i9 + (3 - (i15 >> 1 & 3)) + ((i7 & 7) << 2) + ((i7 >> 3) * i10 + (k3 - 1 - i15 >> 3) << 5)];
                        int l10 = c2 >> 4 & 0xf;
                        if(l10 != 0)
                        {
                            l10 = l10 + 256 + k12 << 1;
                            gbaf.videoMemory[rowOffset + (i3 + j6)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[l10 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[l10]];
                        }
                        if(++j6 != i13)
                        {
                            int i11 = c2 & 0xf;
                            if(i11 != 0)
                            {
                                i11 = i11 + 256 + k12 << 1;
                                gbaf.videoMemory[rowOffset + (i3 + j6)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i11 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i11]];
                            }
                        }
                    }

                    break;

                case 32: // ' '
                    for(int k6 = 0; k6 < i13; k6++)
                    {
                        int j15 = k6 + l12;
                        char c3 = gbaf.cpu.memoria.mVRAM[i9 + (j15 >> 1 & 3) + (7 - (i7 & 7) << 2) + ((l3 - 1 - i7 >> 3) * i10 + (j15 >> 3) << 5)];
                        int j11 = c3 & 0xf;
                        if(j11 != 0)
                        {
                            j11 = j11 + 256 + k12 << 1;
                            gbaf.videoMemory[rowOffset + (i3 + k6)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[j11 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[j11]];
                        }
                        if(++k6 != i13)
                        {
                            int k11 = c3 >> 4 & 0xf;
                            if(k11 != 0)
                            {
                                k11 = k11 + 256 + k12 << 1;
                                gbaf.videoMemory[rowOffset + (i3 + k6)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k11 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k11]];
                            }
                        }
                    }

                    break;

                case 48: // '0'
                    for(int l6 = 0; l6 < i13; l6++)
                    {
                        int k15 = l6 + l12;
                        char c4 = gbaf.cpu.memoria.mVRAM[i9 + (3 - (k15 >> 1 & 3)) + (7 - (i7 & 7) << 2) + ((l3 - 1 - i7 >> 3) * i10 + (k3 - 1 - k15 >> 3) << 5)];
                        int l11 = c4 >> 4 & 0xf;
                        if(l11 != 0)
                        {
                            l11 = l11 + 256 + k12 << 1;
                            gbaf.videoMemory[rowOffset + (i3 + l6)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[l11 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[l11]];
                        }
                        if(++l6 != i13)
                        {
                            int i12 = c4 & 0xf;
                            if(i12 != 0)
                            {
                                i12 = i12 + 256 + k12 << 1;
                                gbaf.videoMemory[rowOffset + (i3 + l6)] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i12 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i12]];
                            }
                        }
                    }

                    break;
                }
            }
        }
    }

    static void displayMode0(GBAPanel gbaf, int i)
    {
        char c3 = '\0';
        int i5 = 0;
        int j5 = 0;
        int i6 = 240 * i;
        int val = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[1] << 8 | gbaf.cpu.memoria.mGBOBJ[0]];
        for(int l2 = 0; l2 < 240; l2++)
            gbaf.videoMemory[i6 + l2] = val;

        for(int k = 4; --k >= 0;)
        {
            for(int l = 4; --l >= 0;)
            {
                int k1 = 1 << l;
                if((gbaf.cpu.memoria.mIO[1] & k1) != 0)
                {
                    int i1 = l << 1;
                    char c1 = gbaf.cpu.memoria.mIO[8 + i1];
                    if((c1 & 3) == k)
                    {
                        char c2 = gbaf.cpu.memoria.mIO[9 + i1];
                        int l1 = (c2 & 0x1f) * 2048;
                        int i2 = (c1 >> 2 & 3) * 16384;
                        int j1 = l << 2;
                        int j2 = (gbaf.cpu.memoria.mIO[17 + j1] & '\001') << 8 | gbaf.cpu.memoria.mIO[16 + j1];
                        int k2 = (gbaf.cpu.memoria.mIO[19 + j1] & '\001') << 8 | gbaf.cpu.memoria.mIO[18 + j1];
                        switch(c2 >> 6 & 3)
                        {
                        default:
                            break;

                        case 0: // '\0'
                            c3 = '\u0100';
                            i5 = i + k2 & 0xff;
                            break;

                        case 1: // '\001'
                            c3 = '\u0200';
                            i5 = i + k2 & 0xff;
                            break;

                        case 2: // '\002'
                            c3 = '\u0100';
                            i5 = i + k2 & 0x1ff;
                            break;

                        case 3: // '\003'
                            c3 = '\u0200';
                            i5 = i + k2 & 0x1ff;
                            if(i5 > 255)
                                l1 += 2048;
                            break;
                        }
                        l1 += (i5 >> 3) << 6;
                        if((c1 >> 7 & 1) != 0)
                        {
                            int j6 = (i5 & 7) << 3;
                            int l6 = 7 - (i5 & 7) << 3;
                            for(int i3 = 0; i3 < 240; i3++)
                            {
                                int k4 = i3 + j2 & c3 - 1;
                                int i4 = l1 + (k4 >> 2 & 0xfe);
                                if(k4 > 255)
                                    i4 += 1984;
                                int k3 = gbaf.cpu.memoria.mVRAM[i4] | gbaf.cpu.memoria.mVRAM[i4 + 1] << 8;
                                switch(k3 >> 10 & 3)
                                {
                                case 0: // '\0'
                                    j5 = i2 + ((k3 & 0x3ff) << 6) + (k4 & 7) + j6;
                                    break;

                                case 1: // '\001'
                                    j5 = i2 + ((k3 & 0x3ff) << 6) + (7 - (k4 & 7)) + j6;
                                    break;

                                case 2: // '\002'
                                    j5 = i2 + ((k3 & 0x3ff) << 6) + (k4 & 7) + l6;
                                    break;

                                case 3: // '\003'
                                    j5 = i2 + ((k3 & 0x3ff) << 6) + (7 - (k4 & 7)) + l6;
                                    break;
                                }
                                int k5 = gbaf.cpu.memoria.mVRAM[j5];
                                if(k5 != 0)
                                {
                                    k5 <<= 1;
                                    gbaf.videoMemory[i6 + i3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k5 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k5]];
                                }
                            }

                        } else
                        {
                            int k6 = (i5 & 7) << 2;
                            int i7 = 7 - (i5 & 7) << 2;
                            for(int j3 = 0; j3 < 240; j3++)
                            {
                                int l4 = j3 + j2 & c3 - 1;
                                int j4 = l1 + (l4 >> 2 & 0xfe);
                                if(l4 > 255)
                                    j4 += 1984;
                                int l3 = gbaf.cpu.memoria.mVRAM[j4] | gbaf.cpu.memoria.mVRAM[j4 + 1] << 8;
                                l4 >>= 1;
                                switch(l3 >> 10 & 3)
                                {
                                default:
                                    break;

                                case 0: // '\0'
                                    j5 = i2 + ((l3 & 0x3ff) << 5) + (l4 & 3) + k6;
                                    char c8 = gbaf.cpu.memoria.mVRAM[j5];
                                    int j7 = c8 & 0xf;
                                    if(j7 != 0)
                                    {
                                        j7 = j7 + (l3 >> 8 & 0xf0) << 1;
                                        gbaf.videoMemory[i6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[j7 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[j7]];
                                    }
                                    j3++;
                                    j7 = c8 >> 4 & 0xf;
                                    if(j7 != 0)
                                    {
                                        j7 = j7 + (l3 >> 8 & 0xf0) << 1;
                                        gbaf.videoMemory[i6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[j7 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[j7]];
                                    }
                                    break;

                                case 1: // '\001'
                                    j5 = i2 + ((l3 & 0x3ff) << 5) + (3 - (l4 & 3)) + k6;
                                    char c9 = gbaf.cpu.memoria.mVRAM[j5];
                                    int k7 = c9 >> 4 & 0xf;
                                    if(k7 != 0)
                                    {
                                        k7 = k7 + (l3 >> 8 & 0xf0) << 1;
                                        gbaf.videoMemory[i6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k7 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k7]];
                                    }
                                    j3++;
                                    k7 = c9 & 0xf;
                                    if(k7 != 0)
                                    {
                                        k7 = k7 + (l3 >> 8 & 0xf0) << 1;
                                        gbaf.videoMemory[i6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k7 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k7]];
                                    }
                                    break;

                                case 2: // '\002'
                                    j5 = i2 + ((l3 & 0x3ff) << 5) + (l4 & 3) + i7;
                                    char c10 = gbaf.cpu.memoria.mVRAM[j5];
                                    int l7 = c10 & 0xf;
                                    if(l7 != 0)
                                    {
                                        l7 = l7 + (l3 >> 8 & 0xf0) << 1;
                                        gbaf.videoMemory[i6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[l7 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[l7]];
                                    }
                                    j3++;
                                    l7 = c10 >> 4 & 0xf;
                                    if(l7 != 0)
                                    {
                                        l7 = l7 + (l3 >> 8 & 0xf0) << 1;
                                        gbaf.videoMemory[i6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[l7 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[l7]];
                                    }
                                    break;

                                case 3: // '\003'
                                    j5 = i2 + ((l3 & 0x3ff) << 5) + (3 - (l4 & 3)) + i7;
                                    char c11 = gbaf.cpu.memoria.mVRAM[j5];
                                    int i8 = c11 >> 4 & 0xf;
                                    if(i8 != 0)
                                    {
                                        i8 = i8 + (l3 >> 8 & 0xf0) << 1;
                                        gbaf.videoMemory[i6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i8]];
                                    }
                                    j3++;
                                    i8 = c11 & 0xf;
                                    if(i8 != 0)
                                    {
                                        i8 = i8 + (l3 >> 8 & 0xf0) << 1;
                                        gbaf.videoMemory[i6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i8]];
                                    }
                                    break;
                                }
                            }

                        }
                    }
                }
            }

            if((gbaf.cpu.memoria.mIO[1] & 0x10) != 0)
                drawSprites(gbaf, i6, i, k);
        }

    }

    static void displayMode1(GBAPanel gbaf, int i)
    {
        char c3 = '\0';
        char c4 = '\0';
        int k5 = 0;
        int l5 = 0;
        int l6 = 240 * i;
        int k6 = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[1] << 8 | gbaf.cpu.memoria.mGBOBJ[0]];
        for(int l2 = 0; l2 < 240; l2++)
            gbaf.videoMemory[l6 + l2] = k6;

        for(int k = 4; --k >= 0;)
        {
            for(int l = 3; --l >= 0;)
            {
                int k1 = 1 << l;
                if((gbaf.cpu.memoria.mIO[1] & k1) != 0)
                {
                    int i1 = l << 1;
                    char c1 = gbaf.cpu.memoria.mIO[8 + i1];
                    if((c1 & 3) == k)
                    {
                        char c2 = gbaf.cpu.memoria.mIO[9 + i1];
                        int l1 = (c2 & 0x1f) * 2048;
                        int i2 = (c1 >> 2 & 3) * 16384;
                        if(l == 2)
                        {
                            switch(c2 >> 6 & 3)
                            {
                            case 0: // '\0'
                                c3 = '\200';
                                c4 = '\200';
                                break;

                            case 1: // '\001'
                                c3 = '\u0100';
                                c4 = '\u0100';
                                break;

                            case 2: // '\002'
                                c3 = '\u0200';
                                c4 = '\u0200';
                                break;

                            case 3: // '\003'
                                c3 = '\u0400';
                                c4 = '\u0400';
                                break;
                            }
                            if(i == 0)
                            {
                                m3[0] = gbaf.cpu.memoria.mIO[40] | gbaf.cpu.memoria.mIO[41] << 8 | gbaf.cpu.memoria.mIO[42] << 16 | (gbaf.cpu.memoria.mIO[43] & 0xf) << 24;
                                m4[0] = gbaf.cpu.memoria.mIO[44] | gbaf.cpu.memoria.mIO[45] << 8 | gbaf.cpu.memoria.mIO[46] << 16 | (gbaf.cpu.memoria.mIO[47] & 0xf) << 24;
                                m5[0] = m3[0];
                                m6[0] = m4[0];
                            } else
                            {
                                int k10 = gbaf.cpu.memoria.mIO[40] | gbaf.cpu.memoria.mIO[41] << 8 | gbaf.cpu.memoria.mIO[42] << 16 | (gbaf.cpu.memoria.mIO[43] & 0xf) << 24;
                                int l10 = gbaf.cpu.memoria.mIO[44] | gbaf.cpu.memoria.mIO[45] << 8 | gbaf.cpu.memoria.mIO[46] << 16 | (gbaf.cpu.memoria.mIO[47] & 0xf) << 24;
                                if(k10 != m5[0] || l10 != m6[0])
                                {
                                    m3[0] = k10;
                                    m4[0] = l10;
                                    m5[0] = m3[0];
                                    m6[0] = m4[0];
                                } else
                                {
                                    int k9 = gbaf.cpu.memoria.mIO[34] | gbaf.cpu.memoria.mIO[35] << 8;
                                    if((k9 & 0x8000) != 0)
                                        k9 |= 0xffff0000;
                                    m3[0] += k9;
                                    int i10 = gbaf.cpu.memoria.mIO[38] | gbaf.cpu.memoria.mIO[39] << 8;
                                    if((i10 & 0x8000) != 0)
                                        i10 |= 0xffff0000;
                                    m4[0] += i10;
                                }
                            }
                            boolean flag = (c2 & 0x20) == 32;
                            if(!flag)
                            {
                                if((m3[0] & 0x8000000) != 0)
                                    m3[0] |= 0xf0000000;
                                if((m4[0] & 0x8000000) != 0)
                                    m4[0] |= 0xf0000000;
                            }
                            int i9 = m3[0];
                            int j9 = m4[0];
                            int l9 = gbaf.cpu.memoria.mIO[32] | gbaf.cpu.memoria.mIO[33] << 8;
                            if((l9 & 0x8000) != 0)
                                l9 |= 0xffff0000;
                            int j10 = gbaf.cpu.memoria.mIO[36] | gbaf.cpu.memoria.mIO[37] << 8;
                            if((j10 & 0x8000) != 0)
                                j10 |= 0xffff0000;
                            for(int i3 = 0; i3 < 240; i3++)
                            {
                                int j11 = j9 >> 8;
                                int i11 = i9 >> 8;
                                if(flag || i11 >= 0 && i11 < c3 && j11 >= 0 && j11 < c4)
                                {
                                    i11 &= c3 - 1;
                                    j11 &= c4 - 1;
                                    int j4 = l1 + (j11 >> 3) * (c3 >> 3) + (i11 >> 3);
                                    l5 = i2 + (gbaf.cpu.memoria.mVRAM[j4] << 6);
                                    l5 += (i11 & 7) + ((j11 & 7) << 3);
                                    int i6 = gbaf.cpu.memoria.mVRAM[l5];
                                    if(i6 != 0)
                                    {
                                        i6 <<= 1;
                                        gbaf.videoMemory[l6 + i3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i6 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i6]];
                                    }
                                }
                                i9 += l9;
                                j9 += j10;
                            }

                        } else
                        {
                            int j1 = l << 2;
                            int j2 = (gbaf.cpu.memoria.mIO[17 + j1] & '\001') << 8 | gbaf.cpu.memoria.mIO[16 + j1];
                            int k2 = (gbaf.cpu.memoria.mIO[19 + j1] & '\001') << 8 | gbaf.cpu.memoria.mIO[18 + j1];
                            switch(c2 >> 6 & 3)
                            {
                            default:
                                break;

                            case 0: // '\0'
                                c3 = '\u0100';
                                c4 = '\u0100';
                                k5 = i + k2 & 0xff;
                                break;

                            case 1: // '\001'
                                c3 = '\u0200';
                                c4 = '\u0100';
                                k5 = i + k2 & 0xff;
                                break;

                            case 2: // '\002'
                                c3 = '\u0100';
                                c4 = '\u0200';
                                k5 = i + k2 & 0x1ff;
                                break;

                            case 3: // '\003'
                                c3 = '\u0200';
                                c4 = '\u0200';
                                k5 = i + k2 & 0x1ff;
                                if(k5 > 255)
                                    l1 += 2048;
                                break;
                            }
                            l1 += (k5 >> 3) << 6;
                            if((c1 >> 7 & 1) != 0)
                            {
                                int i7 = (k5 & 7) << 3;
                                int k7 = 7 - (k5 & 7) << 3;
                                for(int j3 = 0; j3 < 240; j3++)
                                {
                                    int i5 = j3 + j2 & c3 - 1;
                                    int k4 = l1 + ((i5 >> 3) << 1);
                                    if(i5 > 255)
                                        k4 += 1984;
                                    int l3 = gbaf.cpu.memoria.mVRAM[k4] | gbaf.cpu.memoria.mVRAM[k4 + 1] << 8;
                                    switch(l3 >> 10 & 3)
                                    {
                                    case 0: // '\0'
                                        l5 = i2 + ((l3 & 0x3ff) << 6) + (i5 & 7) + i7;
                                        break;

                                    case 1: // '\001'
                                        l5 = i2 + ((l3 & 0x3ff) << 6) + (7 - (i5 & 7)) + i7;
                                        break;

                                    case 2: // '\002'
                                        l5 = i2 + ((l3 & 0x3ff) << 6) + (i5 & 7) + k7;
                                        break;

                                    case 3: // '\003'
                                        l5 = i2 + ((l3 & 0x3ff) << 6) + (7 - (i5 & 7)) + k7;
                                        break;
                                    }
                                    int j6 = gbaf.cpu.memoria.mVRAM[l5];
                                    if(j6 != 0)
                                    {
                                        j6 <<= 1;
                                        gbaf.videoMemory[l6 + j3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[j6 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[j6]];
                                    }
                                }

                            } else
                            {
                                int j7 = (k5 & 7) << 2;
                                int l7 = 7 - (k5 & 7) << 2;
                                for(int k3 = 0; k3 < 240; k3++)
                                {
                                    int j5 = k3 + j2 & c3 - 1;
                                    int l4 = l1 + ((j5 >> 3) << 1);
                                    if(j5 > 255)
                                        l4 += 1984;
                                    int i4 = gbaf.cpu.memoria.mVRAM[l4] | gbaf.cpu.memoria.mVRAM[l4 + 1] << 8;
                                    j5 >>= 1;
                                    switch(i4 >> 10 & 3)
                                    {
                                    default:
                                        break;

                                    case 0: // '\0'
                                        l5 = i2 + ((i4 & 0x3ff) << 5) + (j5 & 3) + j7;
                                        char c5 = gbaf.cpu.memoria.mVRAM[l5];
                                        int i8 = c5 & 0xf;
                                        if(i8 != 0)
                                        {
                                            i8 = i8 + (i4 >> 8 & 0xf0) << 1;
                                            gbaf.videoMemory[l6 + k3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i8]];
                                        }
                                        k3++;
                                        i8 = c5 >> 4 & 0xf;
                                        if(i8 != 0)
                                        {
                                            i8 = i8 + (i4 >> 8 & 0xf0) << 1;
                                            gbaf.videoMemory[l6 + k3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i8]];
                                        }
                                        break;

                                    case 1: // '\001'
                                        l5 = i2 + ((i4 & 0x3ff) << 5) + (3 - (j5 & 3)) + j7;
                                        char c6 = gbaf.cpu.memoria.mVRAM[l5];
                                        int j8 = c6 >> 4 & 0xf;
                                        if(j8 != 0)
                                        {
                                            j8 = j8 + (i4 >> 8 & 0xf0) << 1;
                                            gbaf.videoMemory[l6 + k3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[j8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[j8]];
                                        }
                                        k3++;
                                        j8 = c6 & 0xf;
                                        if(j8 != 0)
                                        {
                                            j8 = j8 + (i4 >> 8 & 0xf0) << 1;
                                            gbaf.videoMemory[l6 + k3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[j8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[j8]];
                                        }
                                        break;

                                    case 2: // '\002'
                                        l5 = i2 + ((i4 & 0x3ff) << 5) + (j5 & 3) + l7;
                                        char c7 = gbaf.cpu.memoria.mVRAM[l5];
                                        int k8 = c7 & 0xf;
                                        if(k8 != 0)
                                        {
                                            k8 = k8 + (i4 >> 8 & 0xf0) << 1;
                                            gbaf.videoMemory[l6 + k3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k8]];
                                        }
                                        k3++;
                                        k8 = c7 >> 4 & 0xf;
                                        if(k8 != 0)
                                        {
                                            k8 = k8 + (i4 >> 8 & 0xf0) << 1;
                                            gbaf.videoMemory[l6 + k3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[k8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[k8]];
                                        }
                                        break;

                                    case 3: // '\003'
                                        l5 = i2 + ((i4 & 0x3ff) << 5) + (3 - (j5 & 3)) + l7;
                                        char c8 = gbaf.cpu.memoria.mVRAM[l5];
                                        int l8 = c8 >> 4 & 0xf;
                                        if(l8 != 0)
                                        {
                                            l8 = l8 + (i4 >> 8 & 0xf0) << 1;
                                            gbaf.videoMemory[l6 + k3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[l8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[l8]];
                                        }
                                        k3++;
                                        l8 = c8 & 0xf;
                                        if(l8 != 0)
                                        {
                                            l8 = l8 + (i4 >> 8 & 0xf0) << 1;
                                            gbaf.videoMemory[l6 + k3] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[l8 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[l8]];
                                        }
                                        break;
                                    }
                                }

                            }
                        }
                    }
                }
            }

            if((gbaf.cpu.memoria.mIO[1] & 0x10) != 0)
                drawSprites(gbaf, l6, i, k);
        }

    }

    static void displayMode2(GBAPanel gbaf, int i)
    {
        char c3 = '\0';
        char c4 = '\0';
        byte byte1 = 0;
        int k3 = 240 * i;
        int j3 = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[1] << 8 | gbaf.cpu.memoria.mGBOBJ[0]];
        for(int i2 = 0; i2 < 240; i2++)
            gbaf.videoMemory[k3 + i2] = j3;

        for(int k = 4; --k >= 0;)
        {
            for(int l = 4; --l >= 2;)
            {
                int j1 = 1 << l;
                if((gbaf.cpu.memoria.mIO[1] & j1) != 0)
                {
                    int i1 = l << 1;
                    char c1 = gbaf.cpu.memoria.mIO[8 + i1];
                    if((c1 & 3) == k)
                    {
                        char c2 = gbaf.cpu.memoria.mIO[9 + i1];
                        int k1 = (c2 & 0x1f) * 2048;
                        int l1 = (c1 >> 2 & 3) * 16384;
                        switch(c2 >> 6 & 3)
                        {
                        case 0: // '\0'
                            c3 = '\200';
                            c4 = '\200';
                            byte1 = 4;
                            break;

                        case 1: // '\001'
                            c3 = '\u0100';
                            c4 = '\u0100';
                            byte1 = 5;
                            break;

                        case 2: // '\002'
                            c3 = '\u0200';
                            c4 = '\u0200';
                            byte1 = 6;
                            break;

                        case 3: // '\003'
                            c3 = '\u0400';
                            c4 = '\u0400';
                            byte1 = 7;
                            break;
                        }
                        byte byte0 = ((byte)(l != 3 ? 0 : 16));
                        if(i == 0)
                        {
                            m3[l - 2] = gbaf.cpu.memoria.mIO[40 + byte0] | gbaf.cpu.memoria.mIO[41 + byte0] << 8 | gbaf.cpu.memoria.mIO[42 + byte0] << 16 | (gbaf.cpu.memoria.mIO[43 + byte0] & 0xf) << 24;
                            m4[l - 2] = gbaf.cpu.memoria.mIO[44 + byte0] | gbaf.cpu.memoria.mIO[45 + byte0] << 8 | gbaf.cpu.memoria.mIO[46 + byte0] << 16 | (gbaf.cpu.memoria.mIO[47 + byte0] & 0xf) << 24;
                            m5[l - 2] = m3[l - 2];
                            m6[l - 2] = m4[l - 2];
                        } else
                        {
                            int j5 = gbaf.cpu.memoria.mIO[40 + byte0] | gbaf.cpu.memoria.mIO[41 + byte0] << 8 | gbaf.cpu.memoria.mIO[42 + byte0] << 16 | (gbaf.cpu.memoria.mIO[43 + byte0] & 0xf) << 24;
                            int k5 = gbaf.cpu.memoria.mIO[44 + byte0] | gbaf.cpu.memoria.mIO[45 + byte0] << 8 | gbaf.cpu.memoria.mIO[46 + byte0] << 16 | (gbaf.cpu.memoria.mIO[47 + byte0] & 0xf) << 24;
                            if(j5 != m5[l - 2] || k5 != m6[l - 2])
                            {
                                m3[l - 2] = j5;
                                m4[l - 2] = k5;
                                m5[l - 2] = m3[l - 2];
                                m6[l - 2] = m4[l - 2];
                            } else
                            {
                                int j4 = gbaf.cpu.memoria.mIO[34 + byte0] | gbaf.cpu.memoria.mIO[35 + byte0] << 8;
                                if((j4 & 0x8000) != 0)
                                    j4 |= 0xffff0000;
                                m3[l - 2] += j4;
                                int l4 = gbaf.cpu.memoria.mIO[38 + byte0] | gbaf.cpu.memoria.mIO[39 + byte0] << 8;
                                if((l4 & 0x8000) != 0)
                                    l4 |= 0xffff0000;
                                m4[l - 2] += l4;
                            }
                        }
                        boolean flag2 = (c2 & 0x20) == 32;
                        if((m3[l - 2] & 0x8000000) != 0)
                            m3[l - 2] |= 0xf0000000;
                        if((m4[l - 2] & 0x8000000) != 0)
                            m4[l - 2] |= 0xf0000000;
                        int l3 = m3[l - 2];
                        int i4 = m4[l - 2];
                        int k4 = gbaf.cpu.memoria.mIO[32 + byte0] | gbaf.cpu.memoria.mIO[33 + byte0] << 8;
                        if((k4 & 0x8000) != 0)
                            k4 |= 0xffff0000;
                        int i5 = gbaf.cpu.memoria.mIO[36 + byte0] | gbaf.cpu.memoria.mIO[37 + byte0] << 8;
                        if((i5 & 0x8000) != 0)
                            i5 |= 0xffff0000;
                        for(int j2 = 0; j2 < 240; j2++)
                        {
                            int i6 = i4 >> 8;
                            int l5 = l3 >> 8;
                            if(flag2 || l5 >= 0 && l5 < c3 && i6 >= 0 && i6 < c4)
                            {
                                l5 &= c3 - 1;
                                i6 &= c4 - 1;
                                int k2 = k1 + ((i6 >> 3) << byte1) + (l5 >> 3);
                                int l2 = l1 + (gbaf.cpu.memoria.mVRAM[k2] << 6);
                                l2 += (l5 & 7) + ((i6 & 7) << 3);
                                int i3 = gbaf.cpu.memoria.mVRAM[l2];
                                if(i3 != 0)
                                {
                                    i3 <<= 1;
                                    gbaf.videoMemory[k3 + j2] = twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[i3 + 1] << 8 | gbaf.cpu.memoria.mGBOBJ[i3]];
                                }
                            }
                            l3 += k4;
                            i4 += i5;
                        }

                    }
                }
            }

            if((gbaf.cpu.memoria.mIO[1] & 0x10) != 0)
                drawSprites(gbaf, k3, i, k);
        }
    }

    public static void draw(GBAPanel gbaf)
    {
        char VCOUNT = gbaf.cpu.memoria.mIO[6];
        switch(gbaf.cpu.memoria.mIO[0] & 7)  //DISPCNT BG mode
        {
        default: break;
        case 0: displayMode0(gbaf, VCOUNT); break;
        case 1: displayMode1(gbaf, VCOUNT); break;
        case 2: displayMode2(gbaf, VCOUNT); break;
        case 3: displayMode3(gbaf, VCOUNT); break;
        case 4: displayMode4(gbaf, VCOUNT); break;
        case 5: 
            if(VCOUNT < 128)
            {
                int rowOff3 = 240 * (VCOUNT + 16);
                for(int i = 0; i < 40; i++)
                {
                    gbaf.videoMemory[rowOff3 + i] = 0;
                    gbaf.videoMemory[(rowOff3 + 239) - i] = 0;
                }

                int i2 = rowOff3 += 40;
                int frame5 = 320 * VCOUNT + ((gbaf.cpu.memoria.mIO[0] & 0x10) == 0 ? 0 : 40960);
                for(int i = 160; --i >= 0;)
                {
                    gbaf.videoMemory[i2++] = 
                    	twoFramesVideoBuffer[gbaf.cpu.memoria.mVRAM[frame5 + 1] << 8 | 
                    	   gbaf.cpu.memoria.mVRAM[frame5]
                    	  ];
                    frame5 += 2;
                }

                if((gbaf.cpu.memoria.mIO[1] & 0x10) != 0)
                {
                    drawSprites(gbaf, rowOff3, VCOUNT, 3);
                    drawSprites(gbaf, rowOff3, VCOUNT, 2);
                    drawSprites(gbaf, rowOff3, VCOUNT, 1);
                    drawSprites(gbaf, rowOff3, VCOUNT, 0);
                }
                break;
            }
            int offset;
            if(VCOUNT < 144)
                offset = 240 * (VCOUNT - 128);
            else
                offset = 240 * VCOUNT;
            for(int j1 = 0; j1 < 240; j1++)
                gbaf.videoMemory[offset + j1] = 0;

            break;
        }
    }

	private static void displayMode4(GBAPanel gbaf, char VCOUNT) {
		int rwOff2 = 240 * VCOUNT;
		int off2 = rwOff2;
		int frame = (gbaf.cpu.memoria.mIO[0] & 0x10) == 0 ? 0 : 40960; //Display Frame Select
		for(int i = 240; --i >= 0;)
		{
		    int pix = gbaf.cpu.memoria.mVRAM[off2 + frame] << 1;
		    gbaf.videoMemory[off2++] = 
		    	twoFramesVideoBuffer[gbaf.cpu.memoria.mGBOBJ[pix + 1] << 8 | 
		    	   gbaf.cpu.memoria.mGBOBJ[pix]];
		}

		if((gbaf.cpu.memoria.mIO[1] & 0x10) != 0)
		{
		    drawSprites(gbaf, rwOff2, VCOUNT, 3);
		    drawSprites(gbaf, rwOff2, VCOUNT, 2);
		    drawSprites(gbaf, rwOff2, VCOUNT, 1);
		    drawSprites(gbaf, rwOff2, VCOUNT, 0);
		}
	}

	private static void displayMode3(GBAPanel gbaf, char VCOUNT) {
		int rwOff1 = 240 * VCOUNT;
		int off = rwOff1;
		int boff = off << 1;
		for(int i = 240; --i >= 0;)
		{
		    gbaf.videoMemory[off++] = 
		    	twoFramesVideoBuffer[gbaf.cpu.memoria.mVRAM[boff + 1] << 8 | 
		    	   gbaf.cpu.memoria.mVRAM[boff]
		    	  ];
		    boff += 2;
		}
		if((gbaf.cpu.memoria.mIO[1] & 0x10) != 0) //Screen Display OBJ
		{
		    drawSprites(gbaf, rwOff1, VCOUNT, 3);
		    drawSprites(gbaf, rwOff1, VCOUNT, 2);
		    drawSprites(gbaf, rwOff1, VCOUNT, 1);
		    drawSprites(gbaf, rwOff1, VCOUNT, 0);
		}
	}

    public static void copyFrame()
    {
        for(int i = 0; i < 32768; i++)
        {
            twoFramesVideoBuffer[i] = getRGB(i);
            twoFramesVideoBuffer[i + 32768] = twoFramesVideoBuffer[i];
        }

    }

}
