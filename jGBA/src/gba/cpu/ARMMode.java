package gba.cpu;
import gba.bios.BIOS;
import gba.debug.Dumper;


public final class ARMMode
{

    int regDes;
    int firstOp;
    int offReg;
    int secondOp;
    int shiftReg;
    int specASR;
    int instruc;
    int shiftResult;
    int shiftAmount;
    int addr;
    CPU cpu;

    public ARMMode(CPU c)
    {
    	cpu=c;
    }

    void incPC()
    {
        cpu.pc += 4;
        cpu.log.trace(cpu.pc);
}

    int getNibble0()
    {
        return instruc & 0xf;
    }

    int getNibble1()
    {
        return instruc >> 8 & 0xf;
    }

    int getNibble2()
    {
        return instruc >> 16 & 0xf;
    }

    int getRegMNdx()
    {
        return instruc >> 12 & 0xf;
    }

    int getNibble02()
    {
        return instruc >> 16 & 0xf;
    }

    int getRegIndexSame()
    {
        return instruc >> 12 & 0xf;
    }

    void setNibble0()
    {
        offReg = instruc & 0xf;
    }

    void loadFirstOp()
    {
        firstOp = instruc >> 16 & 0xf;
    }

    void setRegDes()
    {
        regDes = instruc >> 12 & 0xf;
    }

    void loadSecondOpShort()
    {
        secondOp = instruc & 0xf | instruc >> 4 & 0xf0;
    }

    void loadSecondOp()
    {
        secondOp = instruc & 0xfff;
    }

    void loadSecondOpAndShift()
    {
        secondOp = instruc & 0xff;
        shiftReg = instruc >> 7 & 0x1e;
    }

    void setSpecASR()
    {
        specASR = secondOp >> shiftReg & cpu.shRegs[shiftReg] | secondOp << 32 - shiftReg;
    }

    int getSpecASR()
    {
        return secondOp >> shiftReg & cpu.shRegs[shiftReg] | secondOp << 32 - shiftReg;
    }

    void setSpecASRWithOverflow()
    {
        specASR = secondOp >> shiftReg & cpu.shRegs[shiftReg] | secondOp << 32 - shiftReg;
        if((instruc & 0xf00) != 0)
            cpu.setOverflow(specASR >> 31);
    }

    void calcShift()
    {
        shiftAmount = instruc >> 7 & 0x1f;
        switch(instruc >> 5 & 3)
        {
        default:
            break;

        case 0: // '\0'
            shiftResult = cpu.reg[getNibble0()] << shiftAmount;
            break;

        case 1: // '\001'
            if(shiftAmount == 0)
                shiftResult = 0;
            else
                shiftResult = cpu.reg[getNibble0()] >> shiftAmount & cpu.shRegs[shiftAmount];
            break;

        case 2: // '\002'
            if(shiftAmount == 0)
            {
                shiftResult = (cpu.reg[getNibble0()] & 0x80000000) == 0 ? 0 : -1;
            } else
            {
                setNibble0();
                shiftResult = (cpu.reg[offReg] & 0x80000000) == 0 ? cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount] : -1 << 32 - shiftAmount | cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
            }
            break;

        case 3: // '\003'
            if(shiftAmount == 0)
            {
                shiftResult = cpu.reg[getNibble0()] >> 1 & 0x7fffffff | cpu.getCarry() << 31;
            } else
            {
                setNibble0();
                shiftResult = cpu.reg[offReg] << 32 - shiftAmount | cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
            }
            break;
        }
    }

    void calcShiftCC()
    {
        shiftAmount = instruc >> 7 & 0x1f;
        switch(instruc >> 5 & 3)
        {
        default:
            break;

        case 0: // '\0'
            setNibble0();
            if(shiftAmount != 0)
                cpu.setOverflow(cpu.reg[offReg] >> 32 - shiftAmount & 1);
            shiftResult = cpu.reg[offReg] << shiftAmount;
            break;

        case 1: // '\001'
            if(shiftAmount == 0)
            {
                cpu.setOverflow(cpu.reg[getNibble0()] >> 31);
                shiftResult = 0;
            } else
            {
                setNibble0();
                cpu.setOverflow(cpu.reg[offReg] >> shiftAmount - 1 & 1);
                shiftResult = cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
            }
            break;

        case 2: // '\002'
            if(shiftAmount == 0)
            {
                setNibble0();
                cpu.setOverflow(cpu.reg[offReg] >> 31);
                shiftResult = (cpu.reg[offReg] & 0x80000000) == 0 ? 0 : -1;
            } else
            {
                setNibble0();
                cpu.setOverflow(cpu.reg[offReg] >> shiftAmount - 1 & 1);
                shiftResult = (cpu.reg[offReg] & 0x80000000) == 0 ? cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount] : -1 << 32 - shiftAmount | cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
            }
            break;

        case 3: // '\003'
            if(shiftAmount == 0)
            {
                setNibble0();
                shiftResult = cpu.reg[offReg] >> 1 & 0x7fffffff | cpu.getCarry() << 31;
                cpu.setOverflow(cpu.reg[offReg] & 1);
            } else
            {
                setNibble0();
                cpu.setOverflow(cpu.reg[offReg] >> shiftAmount - 1 & 1);
                shiftResult = cpu.reg[offReg] << 32 - shiftAmount | cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
            }
            break;
        }
    }

    void genCalcShift()
    {
        if((instruc & 0x10) != 0)
        {
            cpu.reg[15] += 4;
            shiftAmount = cpu.reg[getNibble1()] & 0xff;
            switch(instruc >> 5 & 3)
            {
            case 0: // '\0'
                if(shiftAmount == 0)
                    shiftResult = cpu.reg[getNibble0()];
                else
                if(shiftAmount >= 32)
                    shiftResult = 0;
                else
                    shiftResult = cpu.reg[getNibble0()] << shiftAmount;
                break;

            case 1: // '\001'
                if(shiftAmount == 0)
                    shiftResult = cpu.reg[getNibble0()];
                else
                if(shiftAmount >= 32)
                    shiftResult = 0;
                else
                    shiftResult = cpu.reg[getNibble0()] >> shiftAmount & cpu.shRegs[shiftAmount];
                break;

            case 2: // '\002'
                if(shiftAmount == 0)
                    shiftResult = cpu.reg[getNibble0()];
                else
                if(shiftAmount >= 32)
                {
                    shiftResult = (cpu.reg[getNibble0()] & 0x80000000) == 0 ? 0 : -1;
                } else
                {
                    setNibble0();
                    shiftResult = (cpu.reg[offReg] & 0x80000000) == 0 ? cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount] : -1 << 32 - shiftAmount | cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
                }
                break;

            case 3: // '\003'
                shiftAmount &= 0x1f;
                if(shiftAmount == 0)
                {
                    shiftResult = cpu.reg[getNibble0()];
                } else
                {
                    setNibble0();
                    shiftResult = cpu.reg[offReg] << 32 - shiftAmount | cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
                }
                break;
            }
        } else
        {
            calcShift();
        }
    }

    void genCalcShiftCC()
    {
        if((instruc & 0x10) != 0)
        {
            cpu.reg[15] += 4;
            shiftAmount = cpu.reg[getNibble1()] & 0xff;
            switch(instruc >> 5 & 3)
            {
            case 0: // '\0'
                if(shiftAmount == 0)
                    shiftResult = cpu.reg[getNibble0()];
                else
                if(shiftAmount == 32)
                {
                    cpu.setOverflow(cpu.reg[getNibble0()] & 1);
                    shiftResult = 0;
                } else
                if(shiftAmount > 32)
                {
                    cpu.setNotCarry();
                    shiftResult = 0;
                } else
                {
                    setNibble0();
                    cpu.setOverflow(cpu.reg[offReg] >> 32 - shiftAmount & 1);
                    shiftResult = cpu.reg[offReg] << shiftAmount;
                }
                break;

            case 1: // '\001'
                if(shiftAmount == 0)
                    shiftResult = cpu.reg[getNibble0()];
                else
                if(shiftAmount == 32)
                {
                    cpu.setOverflow(cpu.reg[getNibble0()] >> 31 & 1);
                    shiftResult = 0;
                } else
                if(shiftAmount > 32)
                {
                    cpu.setNotCarry();
                    shiftResult = 0;
                } else
                {
                    setNibble0();
                    cpu.setOverflow(cpu.reg[offReg] >> shiftAmount - 1 & 1);
                    shiftResult = cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
                }
                break;

            case 2: // '\002'
                if(shiftAmount == 0)
                    shiftResult = cpu.reg[getNibble0()];
                else
                if(shiftAmount >= 32)
                {
                    setNibble0();
                    cpu.setOverflow(cpu.reg[offReg] >> 31);
                    shiftResult = (cpu.reg[offReg] & 0x80000000) == 0 ? 0 : -1;
                } else
                {
                    setNibble0();
                    cpu.setOverflow(cpu.reg[offReg] >> shiftAmount - 1 & 1);
                    shiftResult = (cpu.reg[offReg] & 0x80000000) == 0 ? cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount] : -1 << 32 - shiftAmount | cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
                }
                break;

            case 3: // '\003'
                if(shiftAmount == 0)
                {
                    shiftResult = cpu.reg[getNibble0()];
                } else
                {
                    shiftAmount &= 0x1f;
                    if(shiftAmount == 0)
                    {
                        setNibble0();
                        cpu.setOverflow(cpu.reg[offReg] >> 31);
                        shiftResult = cpu.reg[offReg];
                    } else
                    {
                        setNibble0();
                        cpu.setOverflow(cpu.reg[offReg] >> shiftAmount - 1 & 1);
                        shiftResult = cpu.reg[offReg] << 32 - shiftAmount | cpu.reg[offReg] >> shiftAmount & cpu.shRegs[shiftAmount];
                    }
                }
                break;
            }
        } else
        {
            calcShiftCC();
        }
    }

    void loadProgramCounter()
    {
        if(regDes == 15){
            cpu.pc = cpu.reg[regDes];
            cpu.log.trace(cpu.pc);
        }else{
            cpu.pc += 4;
            cpu.log.trace(cpu.pc);
        }
    }

    int gotoThumbMode(int thumbInstruction, int j1) throws StoppedCPUException
    {
        if(regDes == 15)
        {
            IRQManager.enabled = true;
            cpu.pc = cpu.reg[regDes];
            cpu.log.trace(cpu.pc);
            int cbits = cpu.getControlBits();
            cpu.lRegs1[cbits] = cpu.reg[13];
            cpu.lRegs2[cbits] = cpu.reg[14];
            cpu.CPSR = cpu.save[cbits];
            cbits = cpu.getControlBits();
            cpu.reg[13] = cpu.lRegs1[cbits];
            cpu.reg[14] = cpu.lRegs2[cbits];
            if(cpu.getARM() != 0)
            {
                cpu.thumbMode = 1;
                cpu.thumbEnv.execOp(thumbInstruction);
                return 0;
            }
            cpu.thumbMode = 0;
        } else
        {
            cpu.pc += 4;
            cpu.log.trace(cpu.pc);
        }
        return j1;
    }

    void cmd01010100()
    {
        loadSecondOp();
        cpu.memoria.writeByte(cpu.reg[getNibble2()] - secondOp, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void cmd01011100()
    {
        loadSecondOp();
        cpu.memoria.writeByte(cpu.reg[getNibble2()] + secondOp, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void cmd01000100()
    {
        loadSecondOp();
        loadFirstOp();
        cpu.memoria.writeByte(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] -= secondOp;
        incPC();
    }

    void cmd01001100()
    {
        loadSecondOp();
        loadFirstOp();
        cpu.memoria.writeByte(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] += secondOp;
        incPC();
    }

    void cmd01100110()
    {
        calcShift();
        loadFirstOp();
        cpu.memoria.writeByte(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] -= shiftResult;
        incPC();
    }

    void cmd01110100()
    {
        calcShift();
        cpu.memoria.writeByte(cpu.reg[getNibble2()] - shiftResult, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void cmd01111100()
    {
        calcShift();
        cpu.memoria.writeByte(cpu.reg[getNibble2()] + shiftResult, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void cmd01010110()
    {
        loadSecondOp();
        loadFirstOp();
        addr = cpu.reg[firstOp] - secondOp;
        cpu.memoria.writeByte(addr, cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd01011110()
    {
        loadSecondOp();
        loadFirstOp();
        addr = cpu.reg[firstOp] + secondOp;
        cpu.memoria.writeByte(addr, cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd01111110()
    {
        calcShift();
        loadFirstOp();
        addr = cpu.reg[firstOp] + shiftResult;
        cpu.memoria.writeByte(addr, cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd01001000()
    {
        loadSecondOp();
        loadFirstOp();
        cpu.memoria.writeWord(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] += secondOp;
        incPC();
    }

    void cmd01010010()
    {
        loadSecondOp();
        loadFirstOp();
        addr = cpu.reg[firstOp] - secondOp;
        cpu.memoria.writeWord(addr, cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd01011010()
    {
        loadSecondOp();
        loadFirstOp();
        addr = cpu.reg[firstOp] + secondOp;
        cpu.memoria.writeWord(addr, cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd01010000()
    {
        loadSecondOp();
        cpu.memoria.writeWord(cpu.reg[getNibble2()] - secondOp, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void cmd01011000()
    {
        loadSecondOp();
        cpu.memoria.writeWord(cpu.reg[getNibble2()] + secondOp, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void cmd01100000()
    {
        calcShift();
        loadFirstOp();
        cpu.memoria.writeWord(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] -= shiftResult;
        incPC();
    }

    void cmd01110000()
    {
        calcShift();
        cpu.memoria.writeWord(cpu.reg[getNibble2()] - shiftResult, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void cmd01101000()
    {
        calcShift();
        loadFirstOp();
        cpu.memoria.writeWord(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] += shiftResult;
        incPC();
    }

    void cmd01111000()
    {
        calcShift();
        cpu.memoria.writeWord(cpu.reg[getNibble2()] + shiftResult, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void storeDec()
    {
        loadSecondOpShort();
        loadFirstOp();
        cpu.memoria.writeHalfWord(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] -= secondOp;
        incPC();
    }

    void storeInc()
    {
        loadSecondOpShort();
        loadFirstOp();
        cpu.memoria.writeHalfWord(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] += secondOp;
        incPC();
    }

    void storeIncReg()
    {
        loadFirstOp();
        cpu.memoria.writeHalfWord(cpu.reg[firstOp], cpu.reg[getRegMNdx()]);
        cpu.reg[firstOp] += cpu.reg[getNibble0()];
        incPC();
    }

    void storeIndexedRegs()
    {
        cpu.memoria.writeHalfWord(cpu.reg[getNibble2()] + cpu.reg[getNibble0()], cpu.reg[getRegMNdx()]);
        incPC();
    }

    void storeNegIndexedRegs()
    {
        loadSecondOpShort();
        cpu.memoria.writeHalfWord(cpu.reg[getNibble2()] - secondOp, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void storeIndexedImm()
    {
        loadSecondOpShort();
        cpu.memoria.writeHalfWord(cpu.reg[getNibble2()] + secondOp, cpu.reg[getRegMNdx()]);
        incPC();
    }

    void cmd01000111()
    {
        loadSecondOp();
        loadFirstOp();
        setRegDes();
        cpu.reg[regDes] = cpu.memoria.readByte(cpu.reg[firstOp]);
        if(regDes != firstOp)
            cpu.reg[firstOp] -= secondOp;
        incPC();
    }

    void cmd01001101()
    {
        loadSecondOp();
        loadFirstOp();
        setRegDes();
        cpu.reg[regDes] = cpu.memoria.readByte(cpu.reg[firstOp]);
        if(regDes != firstOp)
            cpu.reg[firstOp] += secondOp;
        incPC();
    }

    void cmd01010101()
    {
        loadSecondOp();
        cpu.reg[getRegMNdx()] = cpu.memoria.readByte(cpu.reg[getNibble2()] - secondOp);
        incPC();
    }

    void cmd01011101()
    {
        loadSecondOp();
        cpu.reg[getRegMNdx()] = cpu.memoria.readByte(cpu.reg[getNibble2()] + secondOp);
        incPC();
    }

    void cmd01110101()
    {
        calcShift();
        cpu.reg[getRegMNdx()] = cpu.memoria.readByte(cpu.reg[getNibble2()] - shiftResult);
        incPC();
    }

    void cmd01111101()
    {
        calcShift();
        cpu.reg[getRegMNdx()] = cpu.memoria.readByte(cpu.reg[getNibble2()] + shiftResult);
        incPC();
    }

    void cmd01011111()
    {
        loadSecondOp();
        loadFirstOp();
        setRegDes();
        addr = cpu.reg[firstOp] + secondOp;
        cpu.reg[regDes] = cpu.memoria.readByte(addr);
        if(regDes != firstOp)
            cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd01111111()
    {
        calcShift();
        loadFirstOp();
        setRegDes();
        addr = cpu.reg[firstOp] + shiftResult;
        cpu.reg[regDes] = cpu.memoria.readByte(addr);
        if(regDes != firstOp)
            cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd01011001()
    {
        loadSecondOp();
        setRegDes();
        cpu.reg[regDes] = cpu.memoria.readWord(cpu.reg[getNibble2()] + secondOp);
        loadProgramCounter();
    }

    void cmd01010001()
    {
        loadSecondOp();
        setRegDes();
        cpu.reg[regDes] = cpu.memoria.readWord(cpu.reg[getNibble2()] - secondOp);
        loadProgramCounter();
    }

    void cmd01100001()
    {
        calcShift();
        loadFirstOp();
        setRegDes();
        cpu.reg[regDes] = cpu.memoria.readWord(cpu.reg[firstOp]);
        if(regDes != firstOp)
            cpu.reg[firstOp] -= shiftResult;
        loadProgramCounter();
    }

    void cmd01111001()
    {
        calcShift();
        setRegDes();
        cpu.reg[regDes] = cpu.memoria.readWord(cpu.reg[getNibble2()] + shiftResult);
        loadProgramCounter();
    }

    void cmd01001001()
    {
        loadSecondOp();
        loadFirstOp();
        setRegDes();
        cpu.reg[regDes] = cpu.memoria.readWord(cpu.reg[firstOp]);
        if(regDes != firstOp)
            cpu.reg[firstOp] += secondOp;
        loadProgramCounter();
    }

    void cmd01011011()
    {
        loadSecondOp();
        loadFirstOp();
        addr = cpu.reg[firstOp] + secondOp;
        setRegDes();
        cpu.reg[regDes] = cpu.memoria.readWord(addr);
        if(regDes != firstOp)
            cpu.reg[firstOp] = addr;
        loadProgramCounter();
    }

    void loadRegImm()
    {
        loadSecondOpShort();
        loadFirstOp();
        setRegDes();
        switch(instruc >> 5 & 3)
        {
        case 1: // '\001'
            cpu.reg[regDes] = cpu.memoria.readHalfWord(cpu.reg[firstOp]);
            break;

        case 2:
				int i1 = cpu.memoria.readByte(cpu.reg[firstOp]); // '\002'
            cpu.reg[regDes] = (i1 & 0x80) == 0 ? i1 : 0xffffff00 | i1;
            break;

        case 3:
				int i11 = cpu.memoria.readHalfWord(cpu.reg[firstOp]); // '\003'
            cpu.reg[regDes] = (i11 & 0x8000) == 0 ? i11 : 0xffff0000 | i11;
            break;
        }
        if(regDes != firstOp)
            cpu.reg[firstOp] += secondOp;
        incPC();
    }

    void loadRegister()
    {
        setRegDes();
        loadFirstOp();
        addr = cpu.reg[firstOp] + cpu.reg[getNibble0()];
        switch(instruc >> 5 & 3)
        {
        case 1: // '\001'
            cpu.reg[regDes] = cpu.memoria.readHalfWord(addr);
            break;

        case 2:
				int i1 = cpu.memoria.readByte(addr); // '\002'
            cpu.reg[regDes] = (i1 & 0x80) == 0 ? i1 : 0xffffff00 | i1;
            break;

        case 3:
				int i11 = cpu.memoria.readHalfWord(addr); // '\003'
            cpu.reg[regDes] = (i11 & 0x8000) == 0 ? i11 : 0xffff0000 | i11;
            break;
        }
        if(regDes != firstOp)
            cpu.reg[firstOp] = addr;
        incPC();
    }

    void m1()
    {
        switch(instruc >> 5 & 3)
        {
        case 1: // '\001'
            cpu.reg[getRegMNdx()] = cpu.memoria.readHalfWord(cpu.reg[getNibble2()] + cpu.reg[getNibble0()]);
            break;

        case 2:
				int i1 = cpu.memoria.readByte(cpu.reg[getNibble2()] + cpu.reg[getNibble0()]); // '\002'
            cpu.reg[getRegMNdx()] = (i1 & 0x80) == 0 ? i1 : 0xffffff00 | i1;
            break;

        case 3:
				int i11 = cpu.memoria.readHalfWord(cpu.reg[getNibble2()] + cpu.reg[getNibble0()]); // '\003'
            cpu.reg[getRegMNdx()] = (i11 & 0x8000) == 0 ? i11 : 0xffff0000 | i11;
            break;
        }
        incPC();
    }

    void loadRegisterNegImm()
    {
        loadSecondOpShort();
        switch(instruc >> 5 & 3)
        {
        case 1: // '\001'
            cpu.reg[getRegMNdx()] = cpu.memoria.readHalfWord(cpu.reg[getNibble2()] - secondOp);
            break;

        case 2:
				int i1 = cpu.memoria.readByte(cpu.reg[getNibble2()] - secondOp); // '\002'
            cpu.reg[getRegMNdx()] = (i1 & 0x80) == 0 ? i1 : 0xffffff00 | i1;
            break;

        case 3:
				int i11 = cpu.memoria.readHalfWord(cpu.reg[getNibble2()] - secondOp); // '\003'
            cpu.reg[getRegMNdx()] = (i11 & 0x8000) == 0 ? i11 : 0xffff0000 | i11;
            break;
        }
        incPC();
    }

    void m2()
    {
        loadSecondOpShort();
        switch(instruc >> 5 & 3)
        {
        case 1: // '\001'
            cpu.reg[getRegMNdx()] = cpu.memoria.readHalfWord(cpu.reg[getNibble2()] + secondOp);
            break;

        case 2:
				int i1 = cpu.memoria.readByte(cpu.reg[getNibble2()] + secondOp); // '\002'
            cpu.reg[getRegMNdx()] = (i1 & 0x80) == 0 ? i1 : 0xffffff00 | i1;
            break;

        case 3:
				int i11 = cpu.memoria.readHalfWord(cpu.reg[getNibble2()] + secondOp); // '\003'
            cpu.reg[getRegMNdx()] = (i11 & 0x8000) == 0 ? i11 : 0xffff0000 | i11;
            break;
        }
        incPC();
    }

    void ldr()
    {
        loadSecondOpShort();
        loadFirstOp();
        setRegDes();
        addr = cpu.reg[firstOp] + secondOp;
        switch(instruc >> 5 & 3)
        {
        case 1: // '\001'
            cpu.reg[regDes] = cpu.memoria.readHalfWord(addr);
            break;

        case 2:
				int i1 = cpu.memoria.readByte(addr); // '\002'
            cpu.reg[regDes] = (i1 & 0x80) == 0 ? i1 : 0xffffff00 | i1;
            break;

        case 3:
				int i11 = cpu.memoria.readHalfWord(addr); // '\003'
            cpu.reg[regDes] = (i11 & 0x8000) == 0 ? i11 : 0xffff0000 | i11;
            break;
        }
        if(regDes != firstOp)
            cpu.reg[firstOp] = addr;
        incPC();
    }

    void unknownARMOp(int i1)
    {
    	Dumper.cpuStatus(cpu);
        if(i1 != 0)
            cpu.log.put("UNKNOWN ARM " + Integer.toHexString(instruc >> 20 & 0xff));
        else
        	cpu.log.put("UNKNOWN ARM " + Integer.toHexString(instruc >> 20 & 0xff));
        cpu.haltError=true;
    }

    void cmd00000000()
    {
        if((instruc & 0xff0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            cpu.reg[getNibble02()] = cpu.reg[getNibble0()] * cpu.reg[getNibble1()];
            incPC();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = cpu.reg[getNibble2()] & shiftResult;
            loadProgramCounter();
            return;
        }
    }

    void cmd00000001()
    {
        if((instruc & 0xf90) == 0x90)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            addr = cpu.reg[getNibble0()] * cpu.reg[getNibble1()];
            cpu.setCarry(addr);
            cpu.reg[getNibble02()] = addr;
            incPC();
            return;
        } else
        {
            genCalcShiftCC();
            setRegDes();
            addr = cpu.reg[getNibble2()] & shiftResult;
            cpu.setCarry(addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00000010()
    {
        if((instruc & 0xff0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            cpu.reg[getNibble02()] = cpu.reg[getNibble0()] * cpu.reg[getNibble1()] + cpu.reg[getRegIndexSame()];
            incPC();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = cpu.reg[getNibble2()] ^ shiftResult;
            loadProgramCounter();
            return;
        }
    }

    void cmd00000011()
    {
        if((instruc & 0xf90) == 0x90)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            addr = cpu.reg[getNibble0()] * cpu.reg[getNibble1()] + cpu.reg[getRegIndexSame()];
            cpu.setCarry(addr);
            cpu.reg[getNibble02()] = addr;
            incPC();
            return;
        } else
        {
            genCalcShiftCC();
            setRegDes();
            addr = cpu.reg[getNibble2()] ^ shiftResult;
            cpu.setCarry(addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00000100()
    {
        if((instruc & 0xf0) == 0xB0)
        {
            storeDec();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = cpu.reg[getNibble2()] - shiftResult;
            loadProgramCounter();
            return;
        }
    }

    void cmd00000101()
    {
        if((instruc & 0x90) == 0x90)
        {
            unknownARMOp(0);
            return;
        } else
        {
            genCalcShift();
            loadFirstOp();
            setRegDes();
            addr = cpu.reg[firstOp] - shiftResult;
            cpu.setStatusBits(cpu.reg[firstOp], shiftResult, addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00000110()
    {
        if((instruc & 0xf0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = shiftResult - cpu.reg[getNibble2()];
            loadProgramCounter();
            return;
        }
    }

    void cmd00000111()
    {
        if((instruc & 0x90) == 0x90)
        {
            unknownARMOp(0);
            return;
        } else
        {
            genCalcShift();
            loadFirstOp();
            setRegDes();
            addr = shiftResult - cpu.reg[firstOp];
            cpu.setStatusBits(shiftResult, cpu.reg[firstOp], addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00001000()
    {
        if((instruc & 0xff0) == 0xB0)
        {
            storeIncReg();
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            long l1 = ((long)cpu.reg[getNibble0()] << 32) >>> 32;
            long l2 = ((long)cpu.reg[getNibble1()] << 32) >>> 32;
            long l3 = l1 * l2;
            cpu.reg[instruc >> 12 & 0xf] = (int)(l3 & -1L);
            cpu.reg[instruc >> 16 & 0xf] = (int)(l3 >> 32 & -1L);
            incPC();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = cpu.reg[getNibble2()] + shiftResult;
            loadProgramCounter();
            return;
        }
    }

    void cmd00001001()
    {
        if((instruc & 0xf90) == 0x90)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            unknownARMOp(1);
            return;
        } else
        {
            genCalcShift();
            loadFirstOp();
            setRegDes();
            addr = cpu.reg[firstOp] + shiftResult;
            cpu.setStatusBits02(cpu.reg[firstOp], shiftResult, addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00001010()
    {
        if((instruc & 0xff0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            long l1 = ((long)cpu.reg[getNibble0()] << 32) >>> 32;
            long l2 = ((long)cpu.reg[getNibble1()] << 32) >>> 32;
            long l3 = l1 * l2;
            l2 = ((long)cpu.reg[getRegIndexSame()] << 32) >>> 32;
            long l4 = (long)cpu.reg[getNibble02()] << 32;
            l4 |= l2;
            cpu.reg[instruc >> 12 & 0xf] = (int)(l3 & -1L);
            cpu.reg[instruc >> 16 & 0xf] = (int)(l3 >> 32 & -1L);
            incPC();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = cpu.reg[getNibble2()] + shiftResult + cpu.getCarry();
            loadProgramCounter();
            return;
        }
    }

    void cmd00001011()
    {
        if((instruc & 0xf90) == 0x90)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            unknownARMOp(1);
            return;
        } else
        {
            genCalcShift();
            loadFirstOp();
            setRegDes();
            addr = cpu.reg[firstOp] + shiftResult + cpu.getCarry();
            cpu.setStatusBits02(cpu.reg[firstOp], shiftResult, addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00001100()
    {
        if((instruc & 0xf0) == 0xB0)
        {
            storeInc();
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            long l1 = (long)cpu.reg[getNibble0()] * (long)cpu.reg[getNibble1()];
            cpu.reg[instruc >> 12 & 0xf] = (int)(l1 & -1L);
            cpu.reg[instruc >> 16 & 0xf] = (int)(l1 >> 32 & -1L);
            incPC();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = cpu.reg[getNibble2()] - shiftResult - (cpu.getCarry() != 0 ? 0 : 1);
            loadProgramCounter();
            return;
        }
    }

    void cmd00001101()
    {
        if((instruc & 0x90) == 0x90)
        {
            loadRegImm();
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            unknownARMOp(1);
            return;
        } else
        {
            genCalcShift();
            loadFirstOp();
            setRegDes();
            addr = cpu.reg[firstOp] - shiftResult - (cpu.getCarry() != 0 ? 0 : 1);
            cpu.setStatusBits(cpu.reg[firstOp], shiftResult, addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00001110()
    {
        if((instruc & 0xf0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            long l1 = (long)cpu.reg[instruc >> 16 & 0xf] & -1L;
            l1 <<= 32;
            l1 |= (long)cpu.reg[instruc >> 12 & 0xf] & -1L;
            l1 += (long)cpu.reg[getNibble0()] * (long)cpu.reg[getNibble1()];
            cpu.reg[instruc >> 12 & 0xf] = (int)(l1 & -1L);
            cpu.reg[instruc >> 16 & 0xf] = (int)(l1 >> 32 & -1L);
            incPC();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = shiftResult - cpu.reg[getNibble2()] - (cpu.getCarry() != 0 ? 0 : 1);
            loadProgramCounter();
            return;
        }
    }

    void cmd00001111()
    {
        if((instruc & 0x90) == 0x90)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xf0) == 0x90)
        {
            unknownARMOp(1);
            return;
        } else
        {
            genCalcShift();
            loadFirstOp();
            setRegDes();
            addr = shiftResult - cpu.reg[firstOp] - (cpu.getCarry() != 0 ? 0 : 1);
            cpu.setStatusBits(shiftResult, cpu.reg[firstOp], addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00010000()
    {
        if((instruc & 0xff0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        }
        if((instruc & 0xff0) == 0x90)
        {
            loadFirstOp();
            addr = cpu.memoria.readWord(cpu.reg[firstOp]);
            cpu.memoria.writeWord(cpu.reg[firstOp], cpu.reg[getNibble0()]);
            cpu.reg[getRegMNdx()] = addr;
            incPC();
            return;
        } else
        {
            cpu.reg[instruc >> 12 & 0xf] = cpu.CPSR;
            incPC();
            return;
        }
    }

    void cmd00010001()
    {
        if((instruc & 0xf90) == 0x90)
        {
            unknownARMOp(0);
            return;
        } else
        {
            genCalcShiftCC();
            addr = cpu.reg[getNibble2()] & shiftResult;
            cpu.setCarry(addr);
            incPC();
            return;
        }
    }

    int cmd00010010(int i1, int j1) throws StoppedCPUException
    {
        if((instruc & 0xff0) == 0xB0)
        {
            unknownARMOp(0);
            return j1;
        }
        if((instruc & 0xffffff0) == 0x12fff10)
        {
            setNibble0();
            if((cpu.reg[offReg] & 1) != 0)
            {
                cpu.pc = cpu.reg[offReg] & -2;
                cpu.log.trace(cpu.pc);
                
                cpu.thumbMode = 1;
                cpu.setThumbMode();
                cpu.thumbEnv.execOp(i1);
                return 0;
            } else
            {
                cpu.pc = cpu.reg[offReg] & -4;
                cpu.log.trace(cpu.pc);
                return j1;
            }
        }
        if(cpu.getControlBits() == 16)
        {
            cpu.CPSR &= 0xfffffff;
            cpu.CPSR |= cpu.reg[getNibble0()] & 0xf0000000;
        } else
        {
            cpu.lRegs1[cpu.getControlBits()] = cpu.reg[13];
            cpu.lRegs2[cpu.getControlBits()] = cpu.reg[14];
            cpu.CPSR = cpu.reg[getNibble0()];
            cpu.reg[13] = cpu.lRegs1[cpu.getControlBits()];
            cpu.reg[14] = cpu.lRegs2[cpu.getControlBits()];
        }
        incPC();
        return j1;
    }

    void cmd00010011()
    {
        if((instruc & 0xf90) == 0x90)
        {
            unknownARMOp(0);
            return;
        } else
        {
            genCalcShiftCC();
            addr = cpu.reg[getNibble2()] ^ shiftResult;
            cpu.setCarry(addr);
            incPC();
            return;
        }
    }

    void cmd00010100()
    {
        if((instruc & 0xf0) == 0xB0)
        {
            storeNegIndexedRegs();
            return;
        }
        if((instruc & 0xff0) == 0x90)
        {
            loadFirstOp();
            addr = cpu.memoria.readByte(cpu.reg[firstOp]);
            cpu.memoria.writeByte(cpu.reg[firstOp], cpu.reg[getNibble0()]);
            cpu.reg[getRegMNdx()] = addr;
            incPC();
            return;
        } else
        {
            cpu.reg[instruc >> 12 & 0xf] = cpu.save[cpu.getControlBits()];
            incPC();
            return;
        }
    }

    void cmd00010101()
    {
        if((instruc & 0x90) == 0x90)
        {
            loadRegisterNegImm();
            return;
        } else
        {
            genCalcShift();
            loadFirstOp();
            addr = cpu.reg[firstOp] - shiftResult;
            cpu.setStatusBits(cpu.reg[firstOp], shiftResult, addr);
            incPC();
            return;
        }
    }

    void cmd00010110()
    {
        if((instruc & 0xf0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        } else
        {
            genCalcShift();
            cpu.save[cpu.getControlBits()] = shiftResult;
            incPC();
            return;
        }
    }

    void cmd00010111()
    {
        if((instruc & 0x90) == 0x90)
        {
            loadSecondOpShort();
			loadFirstOp();
			setRegDes();
			addr = cpu.reg[firstOp] - secondOp;
			switch(instruc >> 5 & 3)
			{
			case 1: // '\001'
			    cpu.reg[regDes] = cpu.memoria.readHalfWord(addr);
			    break;
			
			case 2:
					int i1 = cpu.memoria.readByte(addr); // '\002'
			    cpu.reg[regDes] = (i1 & 0x80) == 0 ? i1 : 0xffffff00 | i1;
			    break;
			
			case 3:
					int i11 = cpu.memoria.readHalfWord(addr); // '\003'
			    cpu.reg[regDes] = (i11 & 0x8000) == 0 ? i11 : 0xffff0000 | i11;
			    break;
			}
			if(regDes != firstOp)
			    cpu.reg[firstOp] = addr;
			incPC();
            return;
        } else
        {
            genCalcShift();
            loadFirstOp();
            addr = cpu.reg[firstOp] + shiftResult;
            cpu.setStatusBits02(cpu.reg[firstOp], shiftResult, addr);
            incPC();
            return;
        }
    }

    void cmd00011000()
    {
        if((instruc & 0xff0) == 0xB0)
        {
            storeIndexedRegs();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = cpu.reg[getNibble2()] | shiftResult;
            loadProgramCounter();
            return;
        }
    }

    void cmd00011001()
    {
        if((instruc & 0xf90) == 0x90)
        {
            m1();
            return;
        } else
        {
            genCalcShiftCC();
            setRegDes();
            addr = cpu.reg[getNibble2()] | shiftResult;
            cpu.setCarry(addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00011010()
    {
        if((instruc & 0xff0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = shiftResult;
            loadProgramCounter();
            return;
        }
    }

    void cmd00011011()
    {
        if((instruc & 0xf90) == 0x90)
        {
            loadRegister();
            return;
        } else
        {
            genCalcShiftCC();
            setRegDes();
            addr = shiftResult;
            cpu.setCarry(addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00011100()
    {
        if((instruc & 0xf0) == 0xB0)
        {
            storeIndexedImm();
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = cpu.reg[getNibble2()] & ~shiftResult;
            loadProgramCounter();
            return;
        }
    }

    void cmd00011101()
    {
        if((instruc & 0x90) == 0x90)
        {
            m2();
            return;
        } else
        {
            genCalcShiftCC();
            setRegDes();
            addr = cpu.reg[getNibble2()] & ~shiftResult;
            cpu.setCarry(addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00011110()
    {
        if((instruc & 0xf0) == 0xB0)
        {
            unknownARMOp(0);
            return;
        } else
        {
            genCalcShift();
            setRegDes();
            cpu.reg[regDes] = ~shiftResult;
            loadProgramCounter();
            return;
        }
    }

    void cmd00011111()
    {
        if((instruc & 0x90) == 0x90)
        {
            ldr();
            return;
        } else
        {
            genCalcShiftCC();
            setRegDes();
            addr = ~shiftResult;
            cpu.setCarry(addr);
            cpu.reg[regDes] = addr;
            loadProgramCounter();
            return;
        }
    }

    void cmd00100000()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = cpu.reg[getNibble2()] & getSpecASR();
        loadProgramCounter();
    }

    void cmd00100001()
    {
        loadSecondOpAndShift();
        setSpecASRWithOverflow();
        setRegDes();
        addr = cpu.reg[getNibble2()] & specASR;
        cpu.setCarry(addr);
        cpu.reg[regDes] = addr;
        loadProgramCounter();
    }

    void cmd00100010()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = cpu.reg[getNibble2()] ^ getSpecASR();
        loadProgramCounter();
    }

    void cmd00100011()
    {
        loadSecondOpAndShift();
        setSpecASRWithOverflow();
        setRegDes();
        addr = cpu.reg[getNibble2()] ^ specASR;
        cpu.setCarry(addr);
        cpu.reg[regDes] = addr;
        loadProgramCounter();
    }

    void cmd00100100()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = cpu.reg[getNibble2()] - getSpecASR();
        loadProgramCounter();
    }

    int cmd00100101(int i1, int j1) throws StoppedCPUException
    {
        loadSecondOpAndShift();
        setSpecASR();
        loadFirstOp();
        setRegDes();
        addr = cpu.reg[firstOp] - specASR;
        cpu.setStatusBits(cpu.reg[firstOp], specASR, addr);
        cpu.reg[regDes] = addr;
        return gotoThumbMode(i1, j1);
    }

    void cmd00100110()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = getSpecASR() - cpu.reg[getNibble2()];
        loadProgramCounter();
    }

    void cmd00100111()
    {
        loadSecondOpAndShift();
        setSpecASR();
        loadFirstOp();
        setRegDes();
        addr = specASR - cpu.reg[firstOp];
        cpu.setStatusBits(specASR, cpu.reg[firstOp], addr);
        cpu.reg[regDes] = addr;
        loadProgramCounter();
    }

    void cmd00101000()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = cpu.reg[getNibble2()] + getSpecASR();
        loadProgramCounter();
    }

    void cmd00101001()
    {
        loadSecondOpAndShift();
        setSpecASR();
        loadFirstOp();
        setRegDes();
        addr = cpu.reg[firstOp] + specASR;
        cpu.setStatusBits02(cpu.reg[firstOp], specASR, addr);
        cpu.reg[regDes] = addr;
        loadProgramCounter();
    }

    void cmd00101010()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = cpu.reg[getNibble2()] + getSpecASR() + cpu.getCarry();
        loadProgramCounter();
    }

    void cmd00101011()
    {
        loadSecondOpAndShift();
        setSpecASR();
        loadFirstOp();
        setRegDes();
        addr = cpu.reg[firstOp] + specASR + cpu.getCarry();
        cpu.setStatusBits02(cpu.reg[firstOp], specASR, addr);
        cpu.reg[regDes] = addr;
        loadProgramCounter();
    }

    void cmd00101100()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = cpu.reg[getNibble2()] - getSpecASR() - (cpu.getCarry() != 0 ? 0 : 1);
        loadProgramCounter();
    }

    void cmd00101101()
    {
        loadSecondOpAndShift();
        setSpecASR();
        loadFirstOp();
        setRegDes();
        addr = cpu.reg[firstOp] - specASR - (cpu.getCarry() != 0 ? 0 : 1);
        cpu.setStatusBits(cpu.reg[firstOp], specASR, addr);
        cpu.reg[regDes] = addr;
        loadProgramCounter();
    }

    void cmd00101110()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = getSpecASR() - cpu.reg[getNibble2()] - (cpu.getCarry() != 0 ? 0 : 1);
        loadProgramCounter();
    }

    void cmd00101111()
    {
        loadSecondOpAndShift();
        setSpecASR();
        loadFirstOp();
        setRegDes();
        addr = specASR - cpu.reg[firstOp] - (cpu.getCarry() != 0 ? 0 : 1);
        cpu.setStatusBits(specASR, cpu.reg[firstOp], addr);
        cpu.reg[regDes] = addr;
        loadProgramCounter();
    }

    void cmd00110001()
    {
        loadSecondOpAndShift();
        setSpecASRWithOverflow();
        addr = cpu.reg[getNibble2()] & specASR;
        cpu.setCarry(addr);
        incPC();
    }

    void cmd00110011()
    {
        loadSecondOpAndShift();
        setSpecASRWithOverflow();
        addr = cpu.reg[getNibble2()] ^ specASR;
        cpu.setCarry(addr);
        incPC();
    }

    void cmd00110100()
    {
        loadSecondOpAndShift();
        setSpecASR();
        loadFirstOp();
        addr = cpu.reg[firstOp] - specASR;
        cpu.setStatusBits(cpu.reg[firstOp], specASR, addr);
        incPC();
    }

    void cmd00110111()
    {
        loadSecondOpAndShift();
        setSpecASR();
        loadFirstOp();
        addr = cpu.reg[firstOp] + specASR;
        cpu.setStatusBits02(cpu.reg[firstOp], specASR, addr);
        incPC();
    }

    void cmd00111000()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = cpu.reg[getNibble2()] | getSpecASR();
        loadProgramCounter();
    }

    void cmd00111010()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = getSpecASR();
        loadProgramCounter();
    }

    void cmd00111100()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = cpu.reg[getNibble2()] & ~getSpecASR();
        loadProgramCounter();
    }

    void cmd00111101()
    {
        loadSecondOpAndShift();
        setSpecASRWithOverflow();
        setRegDes();
        addr = cpu.reg[getNibble2()] & ~specASR;
        cpu.setCarry(addr);
        cpu.reg[regDes] = addr;
        loadProgramCounter();
    }

    void cmd00111110()
    {
        loadSecondOpAndShift();
        setRegDes();
        cpu.reg[regDes] = ~getSpecASR();
        loadProgramCounter();
    }

    void cmd10000000()
    {
        cpu.reg[15] += 4;
        addr = cpu.reg[getNibble2()];
        for(int i1 = 15; i1 >= 0; i1--)
            if((instruc >> i1 & 1) != 0)
            {
                cpu.memoria.writeWord(addr, cpu.reg[i1]);
                addr -= 4;
            }

        incPC();
    }

    void cmd10000001()
    {
        addr = cpu.reg[getNibble2()];
        for(int i1 = 15; i1 >= 0; i1--)
            if((instruc >> i1 & 1) != 0)
            {
                cpu.reg[i1] = cpu.memoria.readWord(addr);
                addr -= 4;
            }

        if((instruc & 0x8000) != 0){
            cpu.pc = cpu.reg[15];
            cpu.log.trace(cpu.pc);
        }else{
            cpu.pc += 4;
            cpu.log.trace(cpu.pc);
        }
    }

    void cmd10000010()
    {
        cpu.reg[15] += 4;
        loadFirstOp();
        addr = cpu.reg[firstOp];
        for(int regNdx = 15; regNdx >= 0; regNdx--)
            if((instruc >> regNdx & 1) != 0)
            {
                cpu.memoria.writeWord(addr, cpu.reg[regNdx]);
                addr -= 4;
            }

        cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd10001000()
    {
        cpu.reg[15] += 4;
        addr = cpu.reg[instruc >> 16 & 0xf];
        if((instruc & 1) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[0]);
            addr += 4;
        }
        if((instruc & 2) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[1]);
            addr += 4;
        }
        if((instruc & 4) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[2]);
            addr += 4;
        }
        if((instruc & 8) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[3]);
            addr += 4;
        }
        if((instruc & 0x10) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[4]);
            addr += 4;
        }
        if((instruc & 0x20) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[5]);
            addr += 4;
        }
        if((instruc & 0x40) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[6]);
            addr += 4;
        }
        if((instruc & 0x80) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[7]);
            addr += 4;
        }
        if((instruc & 0x100) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[8]);
            addr += 4;
        }
        if((instruc & 0x200) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[9]);
            addr += 4;
        }
        if((instruc & 0x400) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[10]);
            addr += 4;
        }
        if((instruc & 0x800) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[11]);
            addr += 4;
        }
        if((instruc & 0x1000) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[12]);
            addr += 4;
        }
        if((instruc & 0x2000) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[13]);
            addr += 4;
        }
        if((instruc & 0x4000) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[14]);
            addr += 4;
        }
        if((instruc & 0x8000) != 0)
        {
            cpu.memoria.writeWord(addr, cpu.reg[15]);
            addr += 4;
        }
        cpu.pc += 4;
        cpu.log.trace(cpu.pc);
    }

    void cmd10001001()
    {
        addr = cpu.reg[instruc >> 16 & 0xf];
        for(int i1 = 0; i1 < 16; i1++)
            if((instruc >> i1 & 1) != 0)
            {
                cpu.reg[i1] = cpu.memoria.readWord(addr);
                addr += 4;
            }

        if((instruc & 0x8000) != 0){
            cpu.pc = cpu.reg[15];
            cpu.log.trace(cpu.pc);
        }else{
            cpu.pc += 4;
            cpu.log.trace(cpu.pc);
        }
    }

    void cmd10001010()
    {
        cpu.reg[15] += 4;
        loadFirstOp();
        addr = cpu.reg[firstOp];
        for(int i1 = 0; i1 < 16; i1++)
            if((instruc >> i1 & 1) != 0)
            {
                cpu.memoria.writeWord(addr, cpu.reg[i1]);
                addr += 4;
            }

        cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd10001011()
    {
        loadFirstOp();
        addr = cpu.reg[firstOp];
        for(int i1 = 0; i1 < 16; i1++)
            if((instruc >> i1 & 1) != 0)
            {
                cpu.reg[i1] = cpu.memoria.readWord(addr);
                addr += 4;
            }

        cpu.reg[firstOp] = addr;
        if((instruc & 0x8000) != 0){
            cpu.pc = cpu.reg[15];
            cpu.log.trace(cpu.pc);
        }else
            incPC();
    }

    void cmd10010000()
    {
        cpu.reg[15] += 4;
        addr = cpu.reg[getNibble2()];
        for(int i1 = 15; i1 >= 0; i1--)
            if((instruc >> i1 & 1) != 0)
            {
                addr -= 4;
                cpu.memoria.writeWord(addr, cpu.reg[i1]);
            }

        incPC();
    }

    void cmd10010001()
    {
        loadFirstOp();
        addr = cpu.reg[firstOp];
        for(int i1 = 15; i1 >= 0; i1--)
            if((instruc >> i1 & 1) != 0)
            {
                addr -= 4;
                cpu.reg[i1] = cpu.memoria.readWord(addr);
            }

        if((instruc & 0x8000) != 0){
            cpu.pc = cpu.reg[15];
            cpu.log.trace(cpu.pc);
        }else
            incPC();
    }

    void cmd10010010()
    {
        cpu.reg[15] += 4;
        loadFirstOp();
        addr = cpu.reg[firstOp];
        for(int i1 = 15; i1 >= 0; i1--)
            if((instruc >> i1 & 1) != 0)
            {
                addr -= 4;
                cpu.memoria.writeWord(addr, cpu.reg[i1]);
            }

        cpu.reg[firstOp] = addr;
        incPC();
    }

    void cmd10010011()
    {
        loadFirstOp();
        addr = cpu.reg[firstOp];
        for(int i1 = 15; i1 >= 0; i1--)
            if((instruc >> i1 & 1) != 0)
            {
                addr -= 4;
                cpu.reg[i1] = cpu.memoria.readWord(addr);
            }

        cpu.reg[firstOp] = addr;
        if((instruc & 0x8000) != 0){
            cpu.pc = cpu.reg[15];
            cpu.log.trace(cpu.pc);
        }else
            incPC();
    }

    void cmd10011000()
    {
        cpu.reg[15] += 4;
        addr = cpu.reg[getNibble2()];
        for(int i1 = 0; i1 < 16; i1++)
            if((instruc >> i1 & 1) != 0)
            {
                addr += 4;
                cpu.memoria.writeWord(addr, cpu.reg[i1]);
            }

        incPC();
    }

    void cmd10011001()
    {
        addr = cpu.reg[getNibble2()];
        for(int i1 = 0; i1 < 16; i1++)
            if((instruc >> i1 & 1) != 0)
            {
                addr += 4;
                cpu.reg[i1] = cpu.memoria.readWord(addr);
            }

        if((instruc & 0x8000) != 0){
            cpu.pc = cpu.reg[15];
            cpu.log.trace(cpu.pc);
        }else
            incPC();
    }

    void cmd10011011()
    {
        loadFirstOp();
        addr = cpu.reg[firstOp];
        for(int i1 = 0; i1 < 16; i1++)
            if((instruc >> i1 & 1) != 0)
            {
                addr += 4;
                cpu.reg[i1] = cpu.memoria.readWord(addr);
            }

        cpu.reg[firstOp] = addr;
        if((instruc & 0x8000) != 0){
            cpu.pc = cpu.reg[15];
            
            cpu.log.trace(cpu.pc);
        }else
            incPC();
    }

    void cmd10100000()
    {
        cpu.pc += ((instruc & 0x7fffff) << 2) + 8;
        cpu.log.trace(cpu.pc);
    }

    void cmd10101000()
    {
        cpu.pc += ((instruc & 0xffffff) << 2 | 0xff000000) + 8;
        cpu.log.trace(cpu.pc);
    }

    void cmd10110000()
    {
        cpu.reg[14] = cpu.pc + 4;
        cpu.pc += ((instruc & 0x7fffff) << 2) + 8;
        cpu.log.trace(cpu.pc);
    }

    void cmd10111000()
    {
        cpu.reg[14] = cpu.pc + 4;
        cpu.pc += ((instruc & 0xffffff) << 2 | 0xff000000) + 8;
        cpu.log.trace(cpu.pc);
    }

    int cmd11110000(int i1)
    {
        BIOS.execBIOS(cpu,instruc >> 16 & 0xff);
        cpu.pc += 4;
        cpu.log.trace(cpu.pc);
        if(cpu.VBlankIntrWait)
            return 0;
        else
            return i1;
    }

    void unknownARMInstruction()
    {
    	Dumper.cpuStatus(cpu);
    	cpu.log.put("ARMMode ERROR: " + Integer.toHexString(instruc >> 20 & 0xff)+" desconocido");
        cpu.haltError=true;
    }

    public void execOp(int ciclosMax) throws StoppedCPUException
    {
    	if(cpu.haltError)
    		throw new StoppedCPUException("ARM mode: stop CPU.");
        for(int k = 0; k < ciclosMax; k += 4)
        {
            int safePC = cpu.pc & 0xffffff;
            char seg[] = cpu.memoria.vals[cpu.pc >> 24 & 0xff];
            instruc = seg[safePC + 3] << 24 | 
                     seg[safePC + 2] << 16 | 
                     seg[safePC + 1] << 8 | 
                     seg[safePC];
            
            switch(instruc >> 28 & 0xf)	//Condition Codes
            {
            case 14: // All
            default:
                break;

            case 0: // ifNotZero
                if(cpu.getZero() == 0)
                {
                    cpu.pc += 4;
                    cpu.log.trace(cpu.pc);
                    continue;
                }
                break;
            case 1: // ifZero
                if(cpu.getZero() == 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 2: // ifCarry
                if(cpu.getCarry() != 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 3: //ifNotCarry
                if(cpu.getCarry() == 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 4: // ifSigned
                if(cpu.getSign() != 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 5: // ifNotSigned
                if(cpu.getSign() == 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 6: // ifNotOverflow
                if(cpu.getOverflow() != 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 7: // ifOverflow
                if(cpu.getOverflow() == 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 8: // ifCarryZero
                if(cpu.getCarry() != 0 && cpu.getZero() == 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 9: // ifCarryOrNotZero
                if(cpu.getCarry() == 0 || cpu.getZero() != 0)
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 10: // ifSignedIsOverflow
                if(cpu.getSign() == cpu.getOverflow())
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 11: // ifSignIsNotOverflow
                if(cpu.getSign() != cpu.getOverflow())
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 12: // ifNotZeroAndSignIsOverflow
                if(cpu.getZero() == 0 && cpu.getSign() == cpu.getOverflow())
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 13: // ifNotZeroandSignIsNotOverflow
                if(cpu.getZero() != 0 || cpu.getSign() != cpu.getOverflow())
                    break;
                cpu.pc += 4;
                cpu.log.trace(cpu.pc);
                continue;

            case 15: // �?
            	Dumper.cpuStatus(cpu);
            	cpu.log.put("WRONG CONDITION CODE");
                cpu.haltError=true;
                continue;
            }
            
            cpu.reg[15] = cpu.pc + 8;
            switch(instruc >> 20 & 0xff)
            {
            case 0:   cmd00000000(); break;
            case 1:   cmd00000001(); break;
            case 2:   cmd00000010(); break;
            case 3:   cmd00000011(); break;
            case 4:   cmd00000100(); break;
            case 5:   cmd00000101(); break;
            case 6:   cmd00000110(); break;
            case 7:   cmd00000111(); break;
            case 8:   cmd00001000(); break;
            case 9:   cmd00001001(); break;
            case 10:  cmd00001010(); break;
            case 11:  cmd00001011(); break;
            case 12:  cmd00001100(); break;
            case 13:  cmd00001101(); break;
            case 14:  cmd00001110(); break;
            case 15:  cmd00001111(); break;
            case 16:  cmd00010000(); break;
            case 17:  cmd00010001(); break;
            case 18:  ciclosMax = cmd00010010(ciclosMax - k, ciclosMax); break;
            case 19:  cmd00010011(); break;
            case 20:  cmd00010100(); break;
            case 21:  cmd00010101(); break;
            case 22:  cmd00010110(); break;
            case 23:  cmd00010111(); break;
            case 24:  cmd00011000(); break;
            case 25:  cmd00011001(); break;
            case 26:  cmd00011010(); break;
            case 27:  cmd00011011(); break;
            case 28:  cmd00011100(); break;
            case 29:  cmd00011101(); break;
            case 30:  cmd00011110(); break;
            case 31:  cmd00011111(); break;
            case 32:  cmd00100000(); break;
            case 33:  cmd00100001(); break;
            case 34:  cmd00100010(); break;
            case 35:  cmd00100011(); break;
            case 36:  cmd00100100(); break;
            case 37:  ciclosMax = cmd00100101(ciclosMax - k, ciclosMax); break;
            case 38:  cmd00100110(); break;
            case 39:  cmd00100111(); break;
            case 40:  cmd00101000(); break;
            case 41:  cmd00101001(); break;
            case 42:  cmd00101010(); break;
            case 43:  cmd00101011(); break;
            case 44:  cmd00101100(); break;
            case 45:  cmd00101101(); break;
            case 46:  cmd00101110(); break;
            case 47:  cmd00101111(); break;
            case 49:  cmd00110001(); break;
            case 51:  cmd00110011(); break;
            case 52: 
            case 53:  cmd00110100(); break;
            case 55:  cmd00110111(); break;
            case 56:  cmd00111000(); break;
            case 58:  cmd00111010(); break;
            case 60:  cmd00111100(); break;
            case 61:  cmd00111101(); break;
            case 62:  cmd00111110(); break;
            case 68:  cmd01000100(); break;
            case 71:  cmd01000111(); break;
            case 72:  cmd01001000(); break;
            case 73:  cmd01001001(); break;
            case 76:  cmd01001100(); break;
            case 77:  cmd01001101(); break;
            case 80:  cmd01010000(); break;
            case 81:  cmd01010001(); break;
            case 82:  cmd01010010(); break;
            case 84:  cmd01010100(); break;
            case 85:  cmd01010101(); break;
            case 86:  cmd01010110(); break;
            case 88:  cmd01011000(); break;
            case 89:  cmd01011001(); break;
            case 90:  cmd01011010(); break;
            case 91:  cmd01011011(); break;
            case 92:  cmd01011100(); break;
            case 93:  cmd01011101(); break;
            case 94:  cmd01011110(); break;
            case 95:  cmd01011111(); break;
            case 96:  cmd01100000(); break;
            case 97:  cmd01100001(); break;
            case 102: cmd01100110(); break;
            case 104: cmd01101000(); break;
            case 112: cmd01110000(); break;
            case 116: cmd01110100(); break;
            case 117: cmd01110101(); break;
            case 120: cmd01111000(); break;
            case 121: cmd01111001(); break;
            case 124: cmd01111100(); break;
            case 125: cmd01111101(); break;
            case 126: cmd01111110(); break;
            case 127: cmd01111111(); break;
            case 128: cmd10000000(); break;
            case 129: cmd10000001(); break;
            case 130: cmd10000010(); break;
            case 136: cmd10001000(); break;
            case 137: cmd10001001(); break;
            case 138: cmd10001010(); break;
            case 139: cmd10001011(); break;
            case 144: cmd10010000(); break;
            case 145: cmd10010001(); break;
            case 146: cmd10010010(); break;
            case 147: cmd10010011(); break;
            case 152: cmd10011000(); break;
            case 153: cmd10011001(); break;
            case 155: cmd10011011(); break;
            case 160: 
            case 161: 
            case 162: 
            case 163: 
            case 164: 
            case 165: 
            case 166: 
            case 167: cmd10100000(); break;
            case 168: 
            case 169: 
            case 170: 
            case 171: 
            case 172: 
            case 173: 
            case 174: 
            case 175: cmd10101000(); break;
            case 176: 
            case 177: 
            case 178: 
            case 179: 
            case 180: 
            case 181: 
            case 182: 
            case 183: cmd10110000(); break;
            case 184: 
            case 185: 
            case 186: 
            case 187: 
            case 188: 
            case 189: 
            case 190: 
            case 191: cmd10111000(); break;
            case 240: 
            case 241: 
            case 242: 
            case 243: 
            case 244: 
            case 245: 
            case 246: 
            case 247: 
            case 248: 
            case 249: 
            case 250: 
            case 251: 
            case 252: 
            case 253: 
            case 254: 
            case 255: ciclosMax = cmd11110000(ciclosMax); break;

            case 48: 
            case 50: 
            case 54: 
            case 57: 
            case 59: 
            case 63: 
            case 64: 
            case 65: 
            case 66: 
            case 67: 
            case 69: 
            case 70: 
            case 74: 
            case 75: 
            case 78: 
            case 79: 
            case 83: 
            case 87: 
            case 98: 
            case 99: 
            case 100: 
            case 101: 
            case 103: 
            case 105: 
            case 106: 
            case 107: 
            case 108: 
            case 109: 
            case 110: 
            case 111: 
            case 113: 
            case 114: 
            case 115: 
            case 118: 
            case 119: 
            case 122: 
            case 123: 
            case 131: 
            case 132: 
            case 133: 
            case 134: 
            case 135: 
            case 140: 
            case 141: 
            case 142: 
            case 143: 
            case 148: 
            case 149: 
            case 150: 
            case 151: 
            case 154: 
            case 156: 
            case 157: 
            case 158: 
            case 159: 
            case 192: 
            case 193: 
            case 194: 
            case 195: 
            case 196: 
            case 197: 
            case 198: 
            case 199: 
            case 200: 
            case 201: 
            case 202: 
            case 203: 
            case 204: 
            case 205: 
            case 206: 
            case 207: 
            case 208: 
            case 209: 
            case 210: 
            case 211: 
            case 212: 
            case 213: 
            case 214: 
            case 215: 
            case 216: 
            case 217: 
            case 218: 
            case 219: 
            case 220: 
            case 221: 
            case 222: 
            case 223: 
            case 224: 
            case 225: 
            case 226: 
            case 227: 
            case 228: 
            case 229: 
            case 230: 
            case 231: 
            case 232: 
            case 233: 
            case 234: 
            case 235: 
            case 236: 
            case 237: 
            case 238: 
            case 239: 
            default:
                unknownARMInstruction();
                break;
            }
        }

    }

}
