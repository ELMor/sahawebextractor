package gba.cpu;
import gba.bios.BIOS;
import gba.debug.Dumper;


public final class ThumbMode
{

    int opCode;
    int aux01;
    int aux02;
    int aux03;
    int aux04;
    int aux05;
    CPU cpu;
    
    public ThumbMode(CPU c)
    {
    	cpu=c;
    }

    void incPC()
    {
        cpu.pc += 2;
        cpu.log.trace(cpu.pc);
    }

    int maskFFFFFF00(int i1)
    {
        return (i1 & 0x80) == 0 ? i1 : 0xffffff00 | i1;
    }

    int maskFFFF0000(int i1)
    {
        return (i1 & 0x8000) == 0 ? i1 : 0xffff0000 | i1;
    }

    void shiftLeft()
    {
        aux01 = cpu.reg[opCode >> 3 & 7];
        aux05 = opCode >> 6 & 0x1f;
        if(aux05 == 0)
        {
            aux04 = aux01;
            cpu.reg[opCode & 7] = aux04;
        } else
        {
            cpu.setOverflow(aux01 >> 32 - aux05 & 1);
            aux04 = aux01 << aux05;
            cpu.reg[opCode & 7] = aux04;
        }
        cpu.setCarry(aux04);
        incPC();
    }

    void shiftRightLog()
    {
        aux01 = cpu.reg[opCode >> 3 & 7];
        aux05 = opCode >> 6 & 0x1f;
        if(aux05 == 0)
        {
            cpu.setOverflow(aux01 >> 31);
            aux04 = 0;
            cpu.reg[opCode & 7] = aux04;
        } else
        {
            cpu.setOverflow(aux01 >> aux05 - 1 & 1);
            aux04 = aux01 >> aux05 & cpu.shRegs[aux05];
            cpu.reg[opCode & 7] = aux04;
        }
        cpu.setCarry(aux04);
        incPC();
    }

    void shiftRightAri()
    {
        aux01 = cpu.reg[opCode >> 3 & 7];
        aux05 = opCode >> 6 & 0x1f;
        if(aux05 == 0)
        {
            cpu.setOverflow(aux01 >> 31);
            aux04 = (aux01 & 0x80000000) == 0 ? 0 : -1;
            cpu.reg[opCode & 7] = aux04;
        } else
        {
            cpu.setOverflow(aux01 >> aux05 - 1 & 1);
            aux04 = (aux01 & 0x80000000) == 0 ? 
            			aux01 >> aux05 & cpu.shRegs[aux05] : 
            			-1 << 32 - aux05 | aux01 >> aux05 & cpu.shRegs[aux05];
            cpu.reg[opCode & 7] = aux04;
        }
        cpu.setCarry(aux04);
        incPC();
    }

    void move()
    {
        aux01 = cpu.reg[opCode >> 3 & 7];
        aux02 = cpu.reg[opCode >> 6 & 7];
        aux04 = aux01 + aux02;
        cpu.reg[opCode & 7] = aux04;
        cpu.setStatusBits02(aux01, aux02, aux04);
        incPC();
    }

    void compara()
    {
        aux01 = cpu.reg[opCode >> 3 & 7];
        aux02 = cpu.reg[opCode >> 6 & 7];
        aux04 = aux01 - aux02;
        cpu.reg[opCode & 7] = aux04;
        cpu.setStatusBits(aux01, aux02, aux04);
        incPC();
    }

    void addInmediato()
    {
        aux01 = cpu.reg[opCode >> 3 & 7];
        aux02 = opCode >> 6 & 7;
        aux04 = aux01 + aux02;
        cpu.reg[opCode & 7] = aux04;
        cpu.setStatusBits02(aux01, aux02, aux04);
        incPC();
    }

    void subInmediato()
    {
        aux01 = cpu.reg[opCode >> 3 & 7];
        aux02 = opCode >> 6 & 7;
        aux04 = aux01 - aux02;
        cpu.reg[opCode & 7] = aux04;
        cpu.setStatusBits(aux01, aux02, aux04);
        incPC();
    }

    void loadInmediato(int regIndex)
    {
        aux04 = opCode & 0xff;
        cpu.reg[regIndex] = aux04;
        cpu.setCarry(aux04);
        incPC();
    }

    void comparaInmediato(int i1)
    {
        aux05 = opCode & 0xff;
        aux04 = cpu.reg[i1] - aux05;
        cpu.setStatusBits(cpu.reg[i1], aux05, aux04);
        incPC();
    }

    void addInmediato(int i1)
    {
        aux05 = opCode & 0xff;
        aux04 = cpu.reg[i1] + aux05;
        cpu.setStatusBits02(cpu.reg[i1], aux05, aux04);
        cpu.reg[i1] = aux04;
        incPC();
    }

    void subInmediato(int i1)
    {
        aux05 = opCode & 0xff;
        aux04 = cpu.reg[i1] - aux05;
        cpu.setStatusBits(cpu.reg[i1], aux05, aux04);
        cpu.reg[i1] = aux04;
        incPC();
    }

    void alu()
    {
        switch(opCode >> 6 & 3)
        {
        default:
            break;

        case 0: // AND
            aux04 = cpu.reg[opCode & 7];
            aux04 &= cpu.reg[opCode >> 3 & 7];
            cpu.reg[opCode & 7] = aux04;
            cpu.setCarry(aux04);
            incPC();
            break;

        case 1: // XOR
            aux04 = cpu.reg[opCode & 7];
            aux04 ^= cpu.reg[opCode >> 3 & 7];
            cpu.reg[opCode & 7] = aux04;
            cpu.setCarry(aux04);
            incPC();
            break;

        case 2: // '\002'
            aux03 = opCode & 7;
            aux01 = cpu.reg[opCode >> 3 & 7] & 0xff;
            if(aux01 != 0)
                if(aux01 == 32)
                {
                    cpu.setOverflow(cpu.reg[aux03] & 1);
                    cpu.reg[aux03] = 0;
                } else
                if(aux01 > 32)
                {
                    cpu.setNotCarry();
                    cpu.reg[aux03] = 0;
                } else
                {
                    cpu.setOverflow(cpu.reg[aux03] >> 32 - aux01 & 1);
                    cpu.reg[aux03] = cpu.reg[aux03] << aux01;
                }
            cpu.setCarry(cpu.reg[aux03]);
            incPC();
            break;

        case 3: // '\003'
            aux03 = opCode & 7;
            aux01 = cpu.reg[opCode >> 3 & 7] & 0xff;
            if(aux01 != 0)
                if(aux01 == 32)
                {
                    cpu.setOverflow(cpu.reg[aux03] >> 31);
                    cpu.reg[aux03] = 0;
                } else
                if(aux01 > 32)
                {
                    cpu.setNotCarry();
                    cpu.reg[aux03] = 0;
                } else
                {
                    cpu.setOverflow(cpu.reg[aux03] >> aux01 - 1 & 1);
                    cpu.reg[aux03] = cpu.reg[aux03] >> aux01 & cpu.shRegs[aux01];
                }
            cpu.setCarry(cpu.reg[aux03]);
            incPC();
            break;
        }
    }

    void cmd65()
    {
        switch(opCode >> 6 & 3)
        {
        default:
            break;

        case 0: // '\0'
            aux03 = opCode & 7;
            aux01 = cpu.reg[opCode >> 3 & 7] & 0xff;
            if(aux01 != 0)
                if(aux01 >= 32)
                {
                    cpu.setOverflow(cpu.reg[aux03] >> 31);
                    cpu.reg[aux03] = (cpu.reg[aux03] & 0x80000000) == 0 ? 0 : -1;
                } else
                {
                    cpu.setOverflow(cpu.reg[aux03] >> aux01 - 1 & 1);
                    cpu.reg[aux03] = (cpu.reg[aux03] & 0x80000000) == 0 ? cpu.reg[aux03] >> aux01 & cpu.shRegs[aux01] : -1 << 32 - aux01 | cpu.reg[aux03] >> aux01 & cpu.shRegs[aux01];
                }
            cpu.setCarry(cpu.reg[aux03]);
            incPC();
            break;

        case 1: // '\001'
            aux03 = opCode & 7;
            aux01 = cpu.reg[opCode >> 3 & 7];
            aux04 = cpu.reg[aux03] + aux01 + cpu.getCarry();
            cpu.setStatusBits02(cpu.reg[aux03], aux01, aux04);
            cpu.reg[aux03] = aux04;
            incPC();
            break;

        case 2: // '\002'
            aux03 = opCode & 7;
            aux01 = cpu.reg[opCode >> 3 & 7];
            aux04 = cpu.reg[aux03] - aux01 - (cpu.getCarry() == 0 ? 1 : 0);
            cpu.setStatusBits(cpu.reg[aux03], aux01, aux04);
            cpu.reg[aux03] = aux04;
            incPC();
            break;

        case 3: // '\003'
            aux03 = opCode & 7;
            aux01 = cpu.reg[opCode >> 3 & 7] & 0xff;
            if(aux01 != 0)
            {
                aux01 &= 0x1f;
                if(aux01 == 0)
                {
                    cpu.setOverflow(cpu.reg[aux03] >> 31);
                } else
                {
                    cpu.setOverflow(cpu.reg[aux03] >> aux01 - 1 & 1);
                    cpu.reg[aux03] = cpu.reg[aux03] << 32 - aux01 | cpu.reg[aux03] >> aux01 & cpu.shRegs[aux01];
                }
            }
            cpu.setCarry(cpu.reg[aux03]);
            incPC();
            break;
        }
    }

    void cmd66()
    {
        switch(opCode >> 6 & 3)
        {
        case 0: // '\0'
            aux04 = cpu.reg[opCode & 7] & cpu.reg[opCode >> 3 & 7];
            cpu.setCarry(aux04);
            incPC();
            break;

        case 1: // '\001'
            aux02 = cpu.reg[opCode >> 3 & 7];
            aux04 = 0 - aux02;
            cpu.reg[opCode & 7] = aux04;
            cpu.setStatusBits(0, aux02, aux04);
            incPC();
            break;

        case 2: // '\002'
            aux03 = cpu.reg[opCode & 7];
            aux02 = cpu.reg[opCode >> 3 & 7];
            aux04 = aux03 - aux02;
            cpu.setStatusBits(aux03, aux02, aux04);
            incPC();
            break;

        case 3: // '\003'
            aux03 = cpu.reg[opCode & 7];
            aux02 = cpu.reg[opCode >> 3 & 7];
            aux04 = aux03 + aux02;
            cpu.setStatusBits02(aux03, aux02, aux04);
            incPC();
            break;
        }
    }

    void cmd67()
    {
        switch(opCode >> 6 & 3)
        {
        case 0: // '\0'
            aux03 = opCode & 7;
            aux04 = cpu.reg[aux03] | cpu.reg[opCode >> 3 & 7];
            cpu.reg[aux03] = aux04;
            cpu.setCarry(aux04);
            incPC();
            break;

        case 1: // '\001'
            aux03 = opCode & 7;
            aux04 = cpu.reg[opCode >> 3 & 7] * cpu.reg[aux03];
            cpu.reg[aux03] = aux04;
            cpu.setCarry(aux04);
            incPC();
            break;

        case 2: // '\002'
            aux03 = opCode & 7;
            aux04 = cpu.reg[aux03] & ~cpu.reg[opCode >> 3 & 7];
            cpu.reg[aux03] = aux04;
            cpu.setCarry(aux04);
            incPC();
            break;

        case 3: // '\003'
            aux04 = ~cpu.reg[opCode >> 3 & 7];
            cpu.reg[opCode & 7] = aux04;
            cpu.setCarry(aux04);
            incPC();
            break;
        }
    }

    void add01()
    {
        cpu.reg[15] = cpu.pc + 4 & -2;
        aux03 = (opCode & 7) + ((opCode & 0x80) >> 4);
        cpu.reg[aux03] += cpu.reg[(opCode >> 3 & 7) + ((opCode & 0x40) >> 3)];
        if(aux03 == 15){
            cpu.pc = cpu.reg[15] & -2;
            cpu.log.trace(cpu.pc);
        }else
            incPC();
    }

    void cmd69()
    {
        cpu.reg[15] = cpu.pc + 4 & -2;
        aux03 = cpu.reg[(opCode & 7) + ((opCode & 0x80) >> 4)];
        aux02 = cpu.reg[(opCode >> 3 & 7) + ((opCode & 0x40) >> 3)];
        aux04 = aux03 - aux02;
        cpu.setStatusBits(aux03, aux02, aux04);
        incPC();
    }

    void loadRegFromRegs()
    {
        cpu.reg[15] = cpu.pc + 4 & -2;
        aux03 = (opCode & 7) + ((opCode & 0x80) >> 4);
        cpu.reg[aux03] = cpu.reg[(opCode >> 3 & 7) + ((opCode & 0x40) >> 3)];
        if(aux03 == 15){
            cpu.pc = cpu.reg[15] & -2;
            cpu.log.trace(cpu.pc);
        }
        else
            incPC();
    }

    int bx(int armOpCode, int j1) throws StoppedCPUException
    {
        cpu.reg[15] = cpu.pc + 4 & -2;
        aux01 = cpu.reg[opCode >> 3 & 0xf];
        if((aux01 & 1) != 0)
        {
            cpu.pc = aux01 & -2;
            cpu.log.trace(cpu.pc);
        } else
        {
            cpu.thumbMode = 0;
            cpu.setARMMode();
            cpu.pc = aux01 & -4;
            cpu.log.trace(cpu.pc);
            cpu.armEnv.execOp(armOpCode);
            return 0;
        }
        return j1;
    }

    void loadWFromPC(int i1)
    {
        cpu.reg[i1] = cpu.memoria.readWord((cpu.pc & -3) + 4 + ((opCode & 0xff) << 2));
        incPC();
    }

    void writeWIndirect()
    {
        cpu.memoria.writeWord(cpu.reg[opCode >> 3 & 7] + cpu.reg[opCode >> 6 & 7], cpu.reg[opCode & 7]);
        incPC();
    }

    void writeHFIndirect()
    {
        cpu.memoria.writeHalfWord(cpu.reg[opCode >> 3 & 7] + cpu.reg[opCode >> 6 & 7], cpu.reg[opCode & 7]);
        incPC();
    }

    void writeByeIndirect()
    {
        cpu.memoria.writeByte(cpu.reg[opCode >> 3 & 7] + cpu.reg[opCode >> 6 & 7], cpu.reg[opCode & 7]);
        incPC();
    }

    void loadBIndirect()
    {
        cpu.reg[opCode & 7] = maskFFFFFF00(cpu.memoria.readByte(cpu.reg[opCode >> 3 & 7] + cpu.reg[opCode >> 6 & 7]));
        incPC();
    }

    void loadWIndirect()
    {
        cpu.reg[opCode & 7] = cpu.memoria.readWord(cpu.reg[opCode >> 3 & 7] + cpu.reg[opCode >> 6 & 7]);
        incPC();
    }

    void loadHFIndirectUnsafe()
    {
        cpu.reg[opCode & 7] = cpu.memoria.readHalfWord(cpu.reg[opCode >> 3 & 7] + cpu.reg[opCode >> 6 & 7]);
        incPC();
    }

    void loadBIndirectUnsafe()
    {
        cpu.reg[opCode & 7] = cpu.memoria.readByte(cpu.reg[opCode >> 3 & 7] + cpu.reg[opCode >> 6 & 7]);
        incPC();
    }

    void loadHFIndirect()
    {
        cpu.reg[opCode & 7] = 
        	maskFFFF0000(
        			cpu.memoria.readHalfWord(cpu.reg[opCode >> 3 & 7] + 
        					            cpu.reg[opCode >> 6 & 7]));
        incPC();
    }

    void writeToStack(int i1)
    {
        cpu.memoria.writeWord(cpu.reg[13] + ((opCode & 0xff) << 2), cpu.reg[i1]);
        incPC();
    }

    void loadFromStack(int i1)
    {
        cpu.reg[i1] = cpu.memoria.readWord(cpu.reg[13] + ((opCode & 0xff) << 2));
        incPC();
    }

    void loadPCReference(int i1)
    {
        cpu.reg[i1] = (cpu.pc + 4 & -3) + ((opCode & 0xff) << 2);
        incPC();
    }

    void loadStackPointer(int i)
    {
        cpu.reg[i] = cpu.reg[13] + ((opCode & 0xff) << 2);
        incPC();
    }

    void moveStackPointer()
    {
        aux04 = (opCode & 0x7f) << 2;
        if((opCode & 0x80) != 0)
            cpu.reg[13] -= aux04;
        else
            cpu.reg[13] += aux04;
        incPC();
    }

    void push()
    {
        aux04 = cpu.reg[13];
        for(int i1 = 7; i1 >= 0; i1--)
            if((opCode >> i1 & 1) != 0)
            {
                aux04 -= 4;
                cpu.memoria.writeWord(aux04, cpu.reg[i1]);
            }

        cpu.reg[13] = aux04;
        incPC();
    }

    void pushAndCall()
    {
        aux04 = cpu.reg[13];
        aux04 -= 4;
        cpu.memoria.writeWord(aux04, cpu.reg[14]);
        for(int i1 = 7; i1 >= 0; i1--)
            if((opCode >> i1 & 1) != 0)
            {
                aux04 -= 4;
                cpu.memoria.writeWord(aux04, cpu.reg[i1]);
            }

        cpu.reg[13] = aux04;
        incPC();
    }

    void pop()
    {
        aux04 = cpu.reg[13];
        for(int i1 = 0; i1 < 8; i1++)
            if((opCode >> i1 & 1) != 0)
            {
                cpu.reg[i1] = cpu.memoria.readWord(aux04);
                aux04 += 4;
            }

        cpu.reg[13] = aux04;
        incPC();
    }

    void popAndReturn()
    {
        aux04 = cpu.reg[13];
        for(int i1 = 0; i1 < 8; i1++)
            if((opCode >> i1 & 1) != 0)
            {
                cpu.reg[i1] = cpu.memoria.readWord(aux04);
                aux04 += 4;
            }

        cpu.pc = cpu.memoria.readWord(aux04) & -2;
        cpu.log.trace(cpu.pc);
        aux04 += 4;
        cpu.reg[13] = aux04;
    }

    void store8RegCond(int rn)
    {
        aux04 = cpu.reg[rn];
        for(int i = 0; i < 8; i++)
            if((opCode >> i & 1) != 0)
            {
                cpu.memoria.writeWord(aux04, cpu.reg[i]);
                aux04 += 4;
            }

        cpu.reg[rn] = aux04;
        incPC();
    }

    void load8RegCond(int rn)
    {
        aux04 = cpu.reg[rn];
        for(int i = 0; i < 8; i++)
            if((opCode >> i & 1) != 0)
            {
                cpu.reg[i] = cpu.memoria.readWord(aux04);
                aux04 += 4;
            }

        cpu.reg[rn] = aux04;
        incPC();
    }

    int thumbSWI(int ret)
    {
        BIOS.execBIOS(cpu, opCode & 0xff);
        cpu.pc += 2;
        cpu.log.trace(cpu.pc);
        if(cpu.VBlankIntrWait)
            return 0;
        else
            return ret;
    }

    void insOperandUnknown()
    {
    	Dumper.cpuStatus(cpu);
    	cpu.log.put("TERROR: " + Integer.toHexString(opCode >> 8 & 0xff));
        cpu.haltError=true;
    }

    public void execOp(int i1) throws StoppedCPUException
    {
    	if(cpu.haltError)
    		throw new StoppedCPUException("ARM mode: stop CPU.");
        for(int j1 = 0; j1 < i1; j1 += 2)
        {
            int safePC = cpu.pc & 0xffffff;
            char aux[] = cpu.memoria.vals[cpu.pc >> 24 & 0xff];
            opCode = aux[safePC + 1] << 8 | aux[safePC];
            switch(opCode >> 8 & 0xff)
            {
            case 0: // '\0'
            case 1: // '\001'
            case 2: // '\002'
            case 3: // '\003'
            case 4: // '\004'
            case 5: // '\005'
            case 6: // '\006'
            case 7: // '\007'
                shiftLeft();
                break;

            case 8: // '\b'
            case 9: // '\t'
            case 10: // '\n'
            case 11: // '\013'
            case 12: // '\f'
            case 13: // '\r'
            case 14: // '\016'
            case 15: // '\017'
                shiftRightLog();
                break;

            case 16: // '\020'
            case 17: // '\021'
            case 18: // '\022'
            case 19: // '\023'
            case 20: // '\024'
            case 21: // '\025'
            case 22: // '\026'
            case 23: // '\027'
                shiftRightAri();
                break;

            case 24: // '\030'
            case 25: // '\031'
                move();
                break;

            case 26: // '\032'
            case 27: // '\033'
                compara();
                break;

            case 28: // '\034'
            case 29: // '\035'
                addInmediato();
                break;

            case 30: // 00011 11 0 xxxxxxxx
            case 31: // 00011 11 1 xxxxxxxx
                subInmediato();
                break;

            case 32: // 00100000 xxxxxxxx
                loadInmediato(0);
                break;

            case 33: // '!'
                loadInmediato(1);
                break;

            case 34: // '"'
                loadInmediato(2);
                break;

            case 35: // '#'
                loadInmediato(3);
                break;

            case 36: // '$'
                loadInmediato(4);
                break;

            case 37: // '%'
                loadInmediato(5);
                break;

            case 38: // '&'
                loadInmediato(6);
                break;

            case 39: // '\''
                loadInmediato(7);
                break;

            case 40: // '(' 
                comparaInmediato(0);
                break;

            case 41: // ')'
                comparaInmediato(1);
                break;

            case 42: // '*'
                comparaInmediato(2);
                break;

            case 43: // '+'
                comparaInmediato(3);
                break;

            case 44: // ','
                comparaInmediato(4);
                break;

            case 45: // '-'
                comparaInmediato(5);
                break;

            case 46: // '.'
                comparaInmediato(6);
                break;

            case 47: // '/'
                comparaInmediato(7);
                break;

            case 48: // '0'
                addInmediato(0);
                break;

            case 49: // '1'
                addInmediato(1);
                break;

            case 50: // '2'
                addInmediato(2);
                break;

            case 51: // '3'
                addInmediato(3);
                break;

            case 52: // '4'
                addInmediato(4);
                break;

            case 53: // '5'
                addInmediato(5);
                break;

            case 54: // '6'
                addInmediato(6);
                break;

            case 55: // '7'
                addInmediato(7);
                break;

            case 56: // '8'
                subInmediato(0);
                break;

            case 57: // '9'
                subInmediato(1);
                break;

            case 58: // ':'
                subInmediato(2);
                break;

            case 59: // ';'
                subInmediato(3);
                break;

            case 60: // '<'
                subInmediato(4);
                break;

            case 61: // '='
                subInmediato(5);
                break;

            case 62: // '>'
                subInmediato(6);
                break;

            case 63: // '?'
                subInmediato(7);
                break;

            case 64: // '@'
                alu();
                break;

            case 65: // 'A'
                cmd65();
                break;

            case 66: // 'B'
                cmd66();
                break;

            case 67: // 'C'
                cmd67();
                break;

            case 68: // 'D'
                add01();
                break;

            case 69: // 'E'
                cmd69();
                break;

            case 70: // 'F'
                loadRegFromRegs();
                break;

            case 71: // 'G'
                i1 = bx(i1 - j1, i1);
                break;

            case 72: // 'H'
                loadWFromPC(0);
                break;

            case 73: // 'I'
                loadWFromPC(1);
                break;

            case 74: // 'J'
                loadWFromPC(2);
                break;

            case 75: // 'K'
                loadWFromPC(3);
                break;

            case 76: // 'L'
                loadWFromPC(4);
                break;

            case 77: // 'M'
                loadWFromPC(5);
                break;

            case 78: // 'N'
                loadWFromPC(6);
                break;

            case 79: // 'O'
                loadWFromPC(7);
                break;

            case 80: // 'P'
            case 81: // 'Q'
                writeWIndirect();
                break;

            case 82: // 'R'
            case 83: // 'S'
                writeHFIndirect();
                break;

            case 84: // 'T'
            case 85: // 'U'
                writeByeIndirect();
                break;

            case 86: // 'V'
            case 87: // 'W'
                loadBIndirect();
                break;

            case 88: // 'X'
            case 89: // 'Y'
                loadWIndirect();
                break;

            case 90: // 'Z'
            case 91: // '['
                loadHFIndirectUnsafe();
                break;

            case 92: // '\\'
            case 93: // ']'
                loadBIndirectUnsafe();
                break;

            case 94: // '^'
            case 95: // '_'
                loadHFIndirect();
                break;

            case 96: // '`'
            case 97: // 'a'
            case 98: // 'b'
            case 99: // 'c'
            case 100: // 'd'
            case 101: // 'e'
            case 102: // 'f'
            case 103: // 'g'
                cpu.memoria.writeWord(cpu.reg[opCode >> 3 & 7] + ((opCode >> 6 & 0x1f) << 2), cpu.reg[opCode & 7]);
                incPC();
                break;

            case 104: // 'h'
            case 105: // 'i'
            case 106: // 'j'
            case 107: // 'k'
            case 108: // 'l'
            case 109: // 'm'
            case 110: // 'n'
            case 111: // 'o'
                cpu.reg[opCode & 7] = cpu.memoria.readWord(cpu.reg[opCode >> 3 & 7] + ((opCode >> 6 & 0x1f) << 2));
                incPC();
                break;

            case 112: // 'p'
            case 113: // 'q'
            case 114: // 'r'
            case 115: // 's'
            case 116: // 't'
            case 117: // 'u'
            case 118: // 'v'
            case 119: // 'w'
                cpu.memoria.writeByte(cpu.reg[opCode >> 3 & 7] + (opCode >> 6 & 0x1f), cpu.reg[opCode & 7]);
                incPC();
                break;

            case 120: // 'x'
            case 121: // 'y'
            case 122: // 'z'
            case 123: // '{'
            case 124: // '|'
            case 125: // '}'
            case 126: // '~'
            case 127: // '\177'
                cpu.reg[opCode & 7] = cpu.memoria.readByte(cpu.reg[opCode >> 3 & 7] + (opCode >> 6 & 0x1f));
                incPC();
                break;

            case 128: 
            case 129: 
            case 130: 
            case 131: 
            case 132: 
            case 133: 
            case 134: 
            case 135: 
                cpu.memoria.writeHalfWord(cpu.reg[opCode >> 3 & 7] + ((opCode >> 6 & 0x1f) << 1), cpu.reg[opCode & 7]);
                incPC();
                break;

            case 136: 
            case 137: 
            case 138: 
            case 139: 
            case 140: 
            case 141: 
            case 142: 
            case 143: 
                cpu.reg[opCode & 7] = cpu.memoria.readHalfWord(cpu.reg[opCode >> 3 & 7] + ((opCode >> 6 & 0x1f) << 1));
                incPC();
                break;

            case 144: 
                writeToStack(0);
                break;

            case 145: 
                writeToStack(1);
                break;

            case 146: 
                writeToStack(2);
                break;

            case 147: 
                writeToStack(3);
                break;

            case 148: 
                writeToStack(4);
                break;

            case 149: 
                writeToStack(5);
                break;

            case 150: 
                writeToStack(6);
                break;

            case 151: 
                writeToStack(7);
                break;

            case 152: 
                loadFromStack(0);
                break;

            case 153: 
                loadFromStack(1);
                break;

            case 154: 
                loadFromStack(2);
                break;

            case 155: 
                loadFromStack(3);
                break;

            case 156: 
                loadFromStack(4);
                break;

            case 157: 
                loadFromStack(5);
                break;

            case 158: 
                loadFromStack(6);
                break;

            case 159: 
                loadFromStack(7);
                break;

            case 160: 
                loadPCReference(0);
                break;

            case 161: 
                loadPCReference(1);
                break;

            case 162: 
                loadPCReference(2);
                break;

            case 163: 
                loadPCReference(3);
                break;

            case 164: 
                loadPCReference(4);
                break;

            case 165: 
                loadPCReference(5);
                break;

            case 166: 
                loadPCReference(6);
                break;

            case 167: 
                loadPCReference(7);
                break;

            case 168: 
                loadStackPointer(0);
                break;

            case 169: 
                loadStackPointer(1);
                break;

            case 170: 
                loadStackPointer(2);
                break;

            case 171: 
                loadStackPointer(3);
                break;

            case 172: 
                loadStackPointer(4);
                break;

            case 173: 
                loadStackPointer(5);
                break;

            case 174: 
                loadStackPointer(6);
                break;

            case 175: 
                loadStackPointer(7);
                break;

            case 176: 
                moveStackPointer();
                break;

            case 180: 
                push();
                break;

            case 181: 
                pushAndCall();
                break;

            case 188: 
                pop();
                break;

            case 189: 
                popAndReturn();
                break;

            case 192: 
                store8RegCond(0);
                break;

            case 193: 
                store8RegCond(1);
                break;

            case 194: 
                store8RegCond(2);
                break;

            case 195: 
                store8RegCond(3);
                break;

            case 196: 
                store8RegCond(4);
                break;

            case 197: 
                store8RegCond(5);
                break;

            case 198: 
                store8RegCond(6);
                break;

            case 199: 
                store8RegCond(7);
                break;

            case 200: 
                load8RegCond(0);
                break;

            case 201: 
                load8RegCond(1);
                break;

            case 202: 
                load8RegCond(2);
                break;

            case 203: 
                load8RegCond(3);
                break;

            case 204: 
                load8RegCond(4);
                break;

            case 205: 
                load8RegCond(5);
                break;

            case 206: 
                load8RegCond(6);
                break;

            case 207: 
                load8RegCond(7);
                break;

            case 208: 
                if(cpu.getZero() != 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                	cpu.log.trace(cpu.pc);

                }else
                    incPC();
                break;

            case 209: 
                if(cpu.getZero() == 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 210: 
                if(cpu.getCarry() != 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 211: 
                if(cpu.getCarry() == 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 212: 
                if(cpu.getSign() != 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 213: 
                if(cpu.getSign() == 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 214: 
                if(cpu.getOverflow() != 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }
                else
                    incPC();
                break;

            case 215: 
                if(cpu.getOverflow() == 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 216: 
                if(cpu.getCarry() != 0 && cpu.getZero() == 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 217: 
                if(cpu.getCarry() == 0 || cpu.getZero() != 0){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);                    
                }else
                    incPC();
                break;

            case 218: 
                if(cpu.getSign() == cpu.getOverflow()){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 219: 
                if(cpu.getSign() != cpu.getOverflow()){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 220: 
                if(cpu.getZero() == 0 && cpu.getSign() == cpu.getOverflow()){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 221: 
                if(cpu.getZero() != 0 || cpu.getSign() != cpu.getOverflow()){
                    cpu.pc += 4 + ((opCode & 0x7f) << 1 | ((opCode & 0x80) == 0 ? 0 : 0xffffff00));
                    cpu.log.trace(cpu.pc);
                }else
                    incPC();
                break;

            case 223: 
                i1 = thumbSWI(i1);
                break;

            case 224: 
            case 225: 
            case 226: 
            case 227: 
            case 228: 
            case 229: 
            case 230: 
            case 231: 
                aux04 = (opCode & 0x7ff) << 1;
                cpu.pc += 4 + ((opCode & 0x400) == 0 ? aux04 : 0xfffff000 | aux04);
                cpu.log.trace(cpu.pc);
                break;

            case 240: 
            case 241: 
            case 242: 
            case 243: 
            case 244: 
            case 245: 
            case 246: 
            case 247: 
                cpu.reg[14] = cpu.pc + 4 + ((opCode & 0x7ff) << 12 | ((opCode & 0x400) == 0 ? 0 : 0xff800000));
                incPC();
                break;

            case 248: 
            case 249: 
            case 250: 
            case 251: 
            case 252: 
            case 253: 
            case 254: 
            case 255: 
                aux04 = cpu.pc + 2;
                cpu.pc = cpu.reg[14] + ((opCode & 0x7ff) << 1);
                cpu.log.trace(cpu.pc);

                cpu.reg[14] = aux04 | 1;
                break;

            case 177: 
            case 178: 
            case 179: 
            case 182: 
            case 183: 
            case 184: 
            case 185: 
            case 186: 
            case 187: 
            case 190: 
            case 191: 
            case 222: 
            case 232: 
            case 233: 
            case 234: 
            case 235: 
            case 236: 
            case 237: 
            case 238: 
            case 239: 
            default:
                insOperandUnknown();
                break;
            }
        }

    }
}
