package gba.memoria;

import java.io.InputStream;

public final class Memoria
{

    public char vals[][];
    static int contTamanos[] = new int[256];
    static char mBIOS[];
    static char mWRAMExt[];
    static char mWRAMInt[];
    public char mIO[];
    public char mGBOBJ[];
    public char mVRAM[];
    public char mOAM[];
    public char mGamePak[];
    
    static char gbaMemSeg090B0D[];
    static char gbaMemSeg0E[];


    public Memoria(){
        vals = new char[256][];

        contTamanos[0x00] = 0x4000;
        vals[0x00] = mBIOS = new char[contTamanos[0x00]];
        
        contTamanos[0x01] = 1;
        vals[0x01] = new char[contTamanos[0x01]];
        
        contTamanos[0x02] = 0x40000;
        vals[0x02] = mWRAMExt = new char[contTamanos[0x02]];

        contTamanos[0x03] = 32768;
        vals[0x03] = mWRAMInt = new char[contTamanos[0x03]];

        contTamanos[0x04] = 2048;
        vals[0x04] = mIO = new char[contTamanos[0x04]];

        contTamanos[0x05] = 1024;
        vals[0x05] = mGBOBJ = new char[contTamanos[0x05]];

        contTamanos[0x06] = 0x20000;
        vals[0x06] = mVRAM = new char[contTamanos[0x06]];

        contTamanos[0x07] = 1024;
        vals[0x07] = mOAM = new char[contTamanos[0x07]];

        contTamanos[0x08] = 0x2000000;
        vals[0x08] = mGamePak = new char[contTamanos[0x08]];

        contTamanos[0x09] = 0x20000;
        vals[0x09] = gbaMemSeg090B0D = new char[contTamanos[0x09]];

        contTamanos[0x0A] = contTamanos[0x08];
        vals[0x0A] = mGamePak;

        contTamanos[0x0B] = contTamanos[0x09];
        vals[0x0B] = gbaMemSeg090B0D;

        contTamanos[0x0C] = contTamanos[0x08];
        vals[0x0C] = mGamePak;

        contTamanos[0x0D] = contTamanos[0x09];
        vals[0x0D] = gbaMemSeg090B0D;

        contTamanos[0x0E] = 0x10000;
        vals[0x0E] = gbaMemSeg0E = new char[contTamanos[0x0E]];
        
        
        //Inicializamos
        
        for(int i1 = 15; i1 <= 255; i1++)
        {
            vals[i1] = gbaMemSeg0E;
            contTamanos[i1] = 0x10000;
        }

        mIO[0x130] = 0xFF;   // Keyinput Key status
        mIO[0x131] = 0xFF;  
        for(int j1 = 0; j1 < 16384; j1++)
            mBIOS[j1] = '\0';

        for(int k1 = 0; k1 < 0x40000; k1++)
            mWRAMExt[k1] = '\0';

        for(int l1 = 0; l1 < 32768; l1++)
            mWRAMInt[l1] = '\0';

        for(int i2 = 0; i2 < 2048; i2++)
            mIO[i2] = '\0';

        for(int j2 = 0; j2 < 1024; j2++)
            mGBOBJ[j2] = '\0';

        for(int k2 = 0; k2 < 0x20000; k2++)
            mVRAM[k2] = '\0';

        for(int l2 = 0; l2 < 1024; l2++)
            mOAM[l2] = '\0';

        for(int i3 = 0; i3 < 0x20000; i3++)
            gbaMemSeg090B0D[i3] = '\0';

        for(int j3 = 0; j3 < 0x10000; j3++)
            gbaMemSeg0E[j3] = '\0';

        mIO[0x000] = '\200';	//DISPCNT LCD Control
        mIO[0x021] = '\001';	//BG2PA
        mIO[0x027] = '\001';	//BG2PD
        mIO[0x031] = '\001';	//BG3PA
        mIO[0x037] = '\001';	//BG3PD
        mIO[0x130] = '\377';	//KEYINPUT
        mIO[0x131] = '\377';
        try {
        	InputStream is=getClass().getResourceAsStream("GBA.BIOS");
	        for(int i=0;i<16*1024;i++){
	        	int c=is.read();
	        	mBIOS[i]=(char)(c&0xFF);
	        }
	        is.close();
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        mBIOS[0x2C] = '\017';		
        mBIOS[0x2D] = 'P';
        mBIOS[0x2E] = '\275';
        mBIOS[0x2F] = '\350';
        mBIOS[0x30] = '\004';
        mBIOS[0x31] = '\360';
        mBIOS[0x32] = '^';
        mBIOS[0x33] = '\342';
    }
    
    public int readByte(int addr)
    {
        int seg = addr >> 24;
        return vals[seg][addr & contTamanos[seg] - 1];
    }

    public int readHalfWord(int addr)
    {
        int seg = addr >> 24;
        char aux[] = vals[seg];
        int safeAddr = addr & contTamanos[seg] - 1;
        return aux[safeAddr + 1] << 8 | aux[safeAddr];
    }

    public int readWord(int addr)
    {
        int seg = addr >> 24 & 0xff;
        char aux[] = vals[seg];
        int safeAddr = addr & contTamanos[seg] - 1;
        safeAddr &= 0xfffffc; 
        return aux[safeAddr + 3] << 24 | 
               aux[safeAddr + 2] << 16 | 
               aux[safeAddr + 1] <<  8 | 
               aux[safeAddr];
    }

    public void dmaHandler(int dmaChannel, int j1)
    {
        int destSize = 0;
        int srcSize = 0;
        boolean notUpdateDestAddr = false;
        char controlDMAHi=mIO[dmaChannel + 0x0B];
        if((controlDMAHi & 0x80) == 0) //Control
            return;
        switch(j1)
        {
        default:
            break;

        case 0: // '\0'
            if((controlDMAHi & 0x30) != 16)
                return;
            break;

        case 1: // '\001'
            if((controlDMAHi & 0x30) != 32)
                return;
            break;

        case 2: // '\002'
            if((controlDMAHi & 0x30) != 0)
                return;
            break;
        }
        char srcAddrSegment = mIO[dmaChannel + 3];
        int srcAddr = mIO[dmaChannel + 2] << 16 | 
                      mIO[dmaChannel + 1] << 8 | 
                      mIO[dmaChannel + 0];
        char destAddrSegment = mIO[dmaChannel + 7];
        int destAddr = mIO[dmaChannel + 6] << 16 | 
                       mIO[dmaChannel + 5] << 8 | 
                       mIO[dmaChannel + 4];
        int wordsTotal = mIO[dmaChannel + 9] << 8 | 
                         mIO[dmaChannel + 8];
        int controlDMALow=mIO[dmaChannel + 10];
        switch( controlDMALow >> 5 & 3)
        {
        case 0: // '\0'
            destSize = 2;
            notUpdateDestAddr = false;
            break;

        case 1: // '\001'
            destSize = -2;
            notUpdateDestAddr = false;
            break;

        case 2: // '\002'
            destSize = 0;
            notUpdateDestAddr = false;
            break;

        case 3: // '\003'
            destSize = 2;
            notUpdateDestAddr = true;
            break;
        }
        switch(controlDMAHi << 1 & 2 | controlDMALow >> 7 & 1 & 3)
        {
        case 0: // '\0'
            srcSize = 2;
            break;

        case 1: // '\001'
            srcSize = -2;
            break;

        case 2: // '\002'
            srcSize = 0;
            break;

        case 3: // '\003'
            srcSize = 0;
            break;
        }
        char srcData[] = vals[srcAddrSegment];
        char dstData[] = vals[destAddrSegment];
        boolean transferType32bits;
        if((controlDMAHi & 4) != 0)
            transferType32bits = true;
        else
            transferType32bits = false;
        if(transferType32bits)
        {
            destSize <<= 1;
            srcSize <<= 1;
            if(wordsTotal > contTamanos[destAddrSegment] >> 2)
                wordsTotal = contTamanos[destAddrSegment] >> 2;
            if(wordsTotal > contTamanos[srcAddrSegment] >> 2)
                wordsTotal = contTamanos[srcAddrSegment] >> 2;
            for(int l2 = wordsTotal; --l2 >= 0;)
            {
                dstData[destAddr] = srcData[srcAddr];
                dstData[destAddr + 1] = srcData[srcAddr + 1];
                dstData[destAddr + 2] = srcData[srcAddr + 2];
                dstData[destAddr + 3] = srcData[srcAddr + 3];
                destAddr += destSize;
                srcAddr += srcSize;
            }

        } else
        {
            for(int i3 = wordsTotal; --i3 >= 0;)
            {
                dstData[destAddr] = srcData[srcAddr];
                dstData[destAddr + 1] = srcData[srcAddr + 1];
                destAddr += destSize;
                srcAddr += srcSize;
            }

        }
        if(!notUpdateDestAddr)
        {
            mIO[dmaChannel + 6] = (char)(destAddr >> 16 & 0xff);
            mIO[dmaChannel + 5] = (char)(destAddr >> 8 & 0xff);
            mIO[dmaChannel + 4] = (char)(destAddr & 0xff);
        }
        mIO[dmaChannel + 2] = (char)(srcAddr >> 16 & 0xff);
        mIO[dmaChannel + 1] = (char)(srcAddr >> 8 & 0xff);
        mIO[dmaChannel + 0] = (char)(srcAddr & 0xff);
        if((controlDMAHi & 2) == 0)
            mIO[dmaChannel + 11] &= '\177';
    }

    public void writeWord(int addr, int value)
    {
        int seg = addr >> 24 & 0xff;
        char aux[] = vals[seg];
        int safeAddr = addr & contTamanos[seg] - 1;
        aux[safeAddr]     = (char)(value       & 0xff);
        aux[safeAddr + 1] = (char)(value >> 8  & 0xff);
        aux[safeAddr + 2] = (char)(value >> 16 & 0xff);
        aux[safeAddr + 3] = (char)(value >> 24 & 0xff);
        
        if(addr == 0x40000dc)      dmaHandler(0xD4, 2); //DMA3CNT_L
        else if(addr == 0x40000b8) dmaHandler(0xB0, 2); //DMA0CNT_L
        else if(addr == 0x40000c4) dmaHandler(0xBC, 2); //DMA1CNT_L
    }

    public void writeHalfWord(int adr, int value)
    {
        int seg = adr >> 24 & 0xff;
        char aux[] = vals[seg];
        int safeAddr = adr & contTamanos[seg] - 1;
        aux[safeAddr] =     (char)(value & 0xff);
        aux[safeAddr + 1] = (char)(value >> 8 & 0xff);
        
        if     (adr == 0x40000de) dmaHandler(0xD4, 2); //DMA3CNT_H
        else if(adr == 0x40000ba) dmaHandler(0xB0, 2); //DMA0CNT_H
        else if(adr == 0x40000c6) dmaHandler(0xBC, 2); //DMA1CNT_H
    }

    public void writeByte(int addr, int value)
    {
        int safeAddr = addr >> 24;
        vals[safeAddr][addr & contTamanos[safeAddr] - 1] = (char)(value & 0xff);
        
        if(addr == 0x40000df)      dmaHandler(0xD4, 2);
        else if(addr == 0x40000bb) dmaHandler(0xB0, 2);
        else if(addr == 0x40000c7) dmaHandler(0xBC, 2);
    }

}
