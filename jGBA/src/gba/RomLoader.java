package gba;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public final class RomLoader {

	public static boolean loadROM(String romName, GBAPanel gba) {
		if (romName.toLowerCase().endsWith(".gba")) {
			return loadROMUnzipped("file:///"+romName, gba);
		} else if (romName.toLowerCase().endsWith(".zip")) {
			return loadROMZipped("file:///"+romName, gba);
		}
		return false;
	}

	public static boolean loadROMUnzipped(String romName, GBAPanel gba) {
		boolean retOK = true;
		try {
			URLConnection conn = new URL(romName).openConnection();
			conn.setUseCaches(true);
			loadFromInputStream(gba, conn.getInputStream(), conn
					.getContentLength());
		} catch (Exception e) {
			e.printStackTrace();
			retOK = false;
		}
		return retOK;
	}

	public static boolean loadROMZipped(String romName, GBAPanel gba) {
		boolean retOK;
		try {
			URL tgt = new URL(romName);
			URLConnection conn = tgt.openConnection();
			conn.setUseCaches(true);
			ZipInputStream zis = new ZipInputStream(conn.getInputStream());
			boolean foundGBA = false;
			ZipEntry ze = null;
			while (!foundGBA) {
				ze = zis.getNextEntry();
				if (ze == null)
					return false;
				String rname = ze.getName().toLowerCase();
				if (rname.lastIndexOf(".gba") >= 0
						|| rname.lastIndexOf(".bin") >= 0
						|| rname.lastIndexOf(".agb") >= 0)
					foundGBA = true;
			}
			int romSize = (int) ze.getSize();
			loadFromInputStream(gba, zis, romSize);
			zis.close();
			retOK = true;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			retOK = false;
		}
		return retOK;
	}

	private static void loadFromInputStream(GBAPanel gbaf, InputStream is,
			int romSize) throws IOException {
		byte data[] = new byte[romSize];
		int remainingLength = romSize;
		while(remainingLength > 0) {
			remainingLength -= is.read(data, romSize - remainingLength, remainingLength);
		}

		for (int l2 = 0; l2 < romSize; l2++)
			gbaf.cpu.memoria.mGamePak[l2] = (char) (data[l2] + 256 & 0xff);
	}
}
