package gba.debug;

import java.util.Observable;

public class Logger extends Observable{
	String msgs[]=new String[512];
	int stackPointer=0;
	boolean reached=false;
	long lastTimeSet;
	
	public void put(String msg){
		if(stackPointer>=msgs.length){
			stackPointer=0;
			reached=true;
		}
		msgs[stackPointer]=msg.replace('\n', ' ');
		stackPointer++;
		if(System.currentTimeMillis()-lastTimeSet>2000)
		{
			lastTimeSet=System.currentTimeMillis();
			setChanged();
			notifyObservers();
		}
	}
	
	public void setChanged(){
		super.setChanged();
		notifyObservers();
	}
	
	public String giveMeTheLast(int n){
		if(n>msgs.length)
			n=msgs.length;
		StringBuffer sb=new StringBuffer();
		int current=stackPointer-1;
		for(int i=0;i<n;i++){
			if(current<0)
				if(reached)
					current=msgs.length-1;
				else
					break;
			sb.insert(0,msgs[current--]+"\n");
		}
		return sb.toString();
	}
	
	public Logger(){
		lastTimeSet=System.currentTimeMillis();
	}
	
	public static int perLine=0;
	public void trace(int pc){
		return;/*
		if(++perLine>16)
		{ perLine=0; System.out.println("");}
		System.out.print(Integer.toHexString(pc)+".");
		*/
	}
}
