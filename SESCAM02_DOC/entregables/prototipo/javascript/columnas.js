var numColumnas; 
var columnasSeleccionadas;

function cuentaColumnas()
{
	numColumnas = cabecera.cells.length/cabecera.rows.length;
	columnasSeleccionadas = new Array(numColumnas);
	
	for (i=0; i < numColumnas; i++)
		columnasSeleccionadas[i] = 0;
		
}

function mOver(elem) {
	seleccionaColumnaTabla(cabecera, elem, "columnaTitularPress");
	//selecionaColumna(elem, "columnaTitularPress", "columnaDatosPress");
}

function mOut(elem){
	//selecionaColumna(elem, "", "");
	seleccionaColumnaTabla(cabecera, elem, "");
}

function mDown(elem){
	var i = elem.cellIndex % numColumnas;
	
	if (columnasSeleccionadas[i] == 0)
	{
		selecionaColumna(elem, "columnaTitularPress", "columnaDatosPress");
		columnasSeleccionadas[i] = 1;
	}
	else
	{
		selecionaColumna(elem, "", "");
		columnasSeleccionadas[i] = 0;
	}
}


function selecionaColumna(elem, newClassCabecera, newClassDatos)
{
	seleccionaColumnaTabla(cabecera, elem, newClassCabecera);	
	seleccionaColumnaTabla(datos, elem, newClassDatos);	
}

function seleccionaColumnaTabla(tabla, elem, newClass)
{
	
	// Obtengo el n� de la columna a seleccionar
	column = elem.cellIndex % numColumnas;
	// En cada una de las filas, selecciono la columana indicada
	for (i=0; i < tabla.cells.length; i++)
	{
		if ((tabla.cells[i].cellIndex % numColumnas) == column)
			tabla.cells[i].className = newClass;
		
	}
}

function seleccionaFila(filas, id)
{
	var i, j;
	for (i=0; i < filas.length; i++)
	{
		j = i % 2;
		if (j == 0)
			filas[i].className = "filaPar";
		else
			filas[i].className = "filaImPar";
	}
	//id="'"+id+"'";
	filas[id].className = "filaSelecionada";	
}