CREATE OR REPLACE VIEW CONSULTATRAMOS ( CIP, 
APE1, APE2, NOMB, NORDEN, 
CENTRO, DESCENTRO, PROC, DELI, 
CQ, CATALOG ) AS select l.cip,l.ape1,l.ape2,l.nomb,l.norden,l.centro,c.descentro, l.cpr1,o.TLE deli, 'Q' cq, l.CATALOG catalog  
from lesp l,centrosescam c , oltle o 
where l.centro = c.codcentro  
and o.CENTRO=l.CENTRO  
and o.TIPO=1  
and o.CLAVEINICIAL=legase.CLAVE_INICIAL(l.CENTRO,1,l.NORDEN)  
and (l.LINK IS NULL OR l.LINK = -l.norden)

CREATE OR REPLACE VIEW CONSULTATRAMOSCEX ( CIP, 
APE1, APE2, NOMB, NORDEN, 
CENTRO, DESCENTRO, PROC, DELI, 
TIPO, CATALOG, CQ ) AS select l.cip,l.ape1,l.ape2,l.nomb,l.ncita,l.cega,c.descentro, l.PRESTACI,o.TLE deli, l.IDENTPRES tipo, l.CATALOG catalog, 'C' CQ    
from lespcex l,centrosescam c , oltle o   
where l.cega = c.codcentro    
and o.CENTRO=l.CEGA    
and o.TIPO=2    
and o.CLAVEINICIAL=legase.CLAVE_INICIAL(l.CEGA,2,l.NCITA)

CREATE OR REPLACE VIEW MAPEO_PROCS_AUTO ( PKPROC, 
CODPROC, DESCPROC, CODTIPO, CENTRO, 
DESCENTRO, CODPROCMAP, MODIFICADO ) AS SELECT distinct sp.PROC_PK,sp.PROC_COD codproc,sp.PROC_DESC_CORTA descproc,'Q' codtipo,l.centro centro, cen.DESCENTRO descentro,l.CPR1 codprocmap,0 modificado 
FROM lesp l,ses_procedimientos sp,ses_proc_tipos sptipo,centrosescam cen 
WHERE l.CPR1 = sp.proc_cod and sp.PROC_tipo = sptipo.PROC_TIPO and sptipo.PROC_TCOD = 'Q' and l.centro = cen.codcentro 
and l.CPR1 not in (select smp.PROC_CEN_COD from ses_proc_map smp where smp.CENTRO = l.centro and smp.PROC_CEN_TIPO = 'Q') 
UNION ALL 
SELECT distinct sp.PROC_PK,sp.PROC_COD codproc,sp.PROC_DESC_CORTA DESCPROC,prest.identpres CODTIPO,prest.centro centro, cen.DESCENTRO descentro, prest.codprest codprocmap,0 modificado 
FROM prestacion prest,ses_procedimientos sp,ses_proc_tipos sptipo,centrosescam cen 
WHERE trim(prest.cnormal) = sp.proc_cod and prest.identpres = sptipo.PROC_TCOD and sp.PROC_TIPO = sptipo.PROC_TIPO and prest.centro = cen.codcentro and 
prest.CODPREST not in (select smp.PROC_CEN_COD from ses_proc_map smp where smp.CENTRO = prest.CENTRO and smp.PROC_CEN_TIPO = prest.IDENTPRES) 
UNION ALL 
SELECT distinct sp.PROC_PK,sp.PROC_COD codproc,sp.PROC_DESC_CORTA DESCPROC,prest.identpres CODTIPO,prest.centro centro, cen.DESCENTRO descentro, prest.codprest codprocmap, 1 modificado
FROM prestacion prest,ses_procedimientos sp,ses_proc_tipos sptipo,centrosescam cen 
,ses_proc_map spm 
WHERE trim(prest.cnormal) = sp.proc_cod and prest.identpres = sptipo.PROC_TCOD and sp.PROC_TIPO = sptipo.PROC_TIPO and prest.centro = cen.codcentro 
and spm.PROC_CEN_COD=prest.CODPREST and spm.PROC_CEN_TIPO= prest.IDENTPRES and spm.centro = prest.CENTRO 
and sp.PROC_pk<>spm.proc_pk

CREATE OR REPLACE VIEW MAPEO_PROCS ( COD_SESCAM, 
DESCRIP, COD_MAPEO, CODTIPO, TIPO, 
CODTIPOMAPEO, CENTRO, PKTIPOMAPEO ) AS SELECT distinct sp.PROC_COD cod_sescam, sp.PROC_DESC_CORTA descrip, smp.PROC_CEN_COD cod_mapeo,sptipo1.PROC_TCOD codtipo, sp.proc_tipo tipo,smp.PROC_CEN_TIPO,smp.CENTRO centro,sptipo.PROC_TIPO pktipomapeo 
FROM ses_proc_map smp,ses_procedimientos sp, ses_proc_tipos sptipo,ses_proc_tipos sptipo1 
WHERE smp.PROC_PK = sp.PROC_PK and smp.PROC_CEN_TIPO = sptipo.PROC_TCOD and sp.PROC_TIPO = sptipo1.PROC_TIPO

CREATE OR REPLACE VIEW MAPEO_DIAG ( COD_SESCAM, 
DESCRIP, COD_MAPEO, CENTRO ) AS SELECT distinct dg.DIAG_COD cod_sescam, dg.DIAG_DESC descrip, dmg.DIAG_CEN_COD cod_mapeo,dmg.CENTRO centro 
FROM ses_diag_map dmg, ses_diagnosticos dg 
WHERE  dmg.DIAG_PK = dg.DIAG_PK


