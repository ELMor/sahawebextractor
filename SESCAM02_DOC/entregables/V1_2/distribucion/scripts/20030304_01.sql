-- ============================================================
--   Database name:  sescam                                    
--   DBMS name:      ORACLE Version 8                          
--   Created on:     4/3/03  12.11                             
-- ============================================================

alter table ses_estadisticas_sync add (esync_tramos CLOB  null);

alter table ses_estadisticas add (est_catalog  VARCHAR2(6) null) ;

commit;

