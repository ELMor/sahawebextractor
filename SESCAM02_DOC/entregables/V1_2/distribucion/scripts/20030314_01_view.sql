CREATE OR REPLACE VIEW MAPEO_PROCS_AUTO_2 ( PKPROC, 
CODPROC, TIPOPROC, DESCPROC, CODTIPO, 
CENTRO, DESCENTRO, CODPROCMAP, MODIFICADO
 ) AS SELECT distinct sp.PROC_PK,sp.PROC_COD codproc,'Q'TIPOPROC, sp.PROC_DESC_CORTA descproc,'Q' codtipo,l.centro centro, cen.DESCENTRO descentro,l.CPR1 codprocmap,0 modificado 
FROM lesp l,ses_procedimientos sp,ses_proc_tipos sptipo,centrosescam cen 
WHERE l.cpr1 is not null and l.cpr1=sp.proc_cod and sp.PROC_tipo = sptipo.PROC_TIPO and sptipo.PROC_TCOD = 'Q' and l.centro = cen.codcentro 
and l.CPR1 not in (select smp.PROC_CEN_COD from ses_proc_map smp where smp.CENTRO = l.centro and smp.PROC_CEN_TIPO = 'Q') 
UNION ALL 
SELECT distinct sp.PROC_PK,sp.PROC_COD codproc, sptipo.PROC_TCOD TIPOPROC,sp.PROC_DESC_CORTA DESCPROC,prest.identpres CODTIPO,prest.centro centro, cen.DESCENTRO descentro, prest.codprest codprocmap,0 modificado 
FROM prestacion prest,ses_procedimientos sp,ses_proc_tipos sptipo,centrosescam cen 
WHERE prest.cnormal=sp.proc_cod and prest.cnormal is not null and prest.cnormal <>'     ' and prest.cnormal not like 'C%' 
and sp.PROC_TIPO = sptipo.PROC_TIPO and prest.centro = cen.codcentro and 
prest.CODPREST not in (select smp.PROC_CEN_COD from ses_proc_map smp where smp.CENTRO = prest.CENTRO and smp.PROC_CEN_TIPO = prest.IDENTPRES) 
UNION ALL 
SELECT distinct sp.PROC_PK,sp.PROC_COD codproc, sptipo.PROC_TCOD TIPOPROC,sp.PROC_DESC_CORTA DESCPROC,prest.identpres CODTIPO,prest.centro centro, cen.DESCENTRO descentro, prest.codprest codprocmap,0 modificado 
FROM prestacion prest,ses_procedimientos sp,ses_proc_tipos sptipo,centrosescam cen 
WHERE prest.cnormal=sp.proc_cod and prest.cnormal is not null and prest.cnormal <>'     ' and prest.cnormal LIKE 'C%' 
and prest.identpres<>'S'  and sp.PROC_TIPO = sptipo.PROC_TIPO and prest.centro = cen.codcentro and 
prest.CODPREST not in (select smp.PROC_CEN_COD from ses_proc_map smp where smp.CENTRO = prest.CENTRO and smp.PROC_CEN_TIPO = prest.IDENTPRES) 
UNION ALL 
SELECT distinct sp.PROC_PK,sp.PROC_COD codproc,sptipo.PROC_TCOD TIPOPROC,sp.PROC_DESC_CORTA DESCPROC,prest.identpres CODTIPO,prest.centro centro, cen.DESCENTRO descentro, prest.codprest codprocmap, 1 modificado 
FROM prestacion prest,ses_procedimientos sp,ses_proc_tipos sptipo,centrosescam cen 
,ses_proc_map spm 
WHERE trim(prest.cnormal) = sp.proc_cod and sp.PROC_TIPO = sptipo.PROC_TIPO and prest.centro = cen.codcentro 
and spm.PROC_CEN_COD=prest.CODPREST and spm.PROC_CEN_TIPO= prest.IDENTPRES and spm.centro = prest.CENTRO 
and sp.PROC_pk<>spm.proc_pk

