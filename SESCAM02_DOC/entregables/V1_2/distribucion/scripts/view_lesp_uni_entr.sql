create or replace view lesp_uni_entr as
        select
        	linklesp.cip cip,
        	linklesp.centro centro,
			
        	linklesp.norden norden,
        	linklesp.cpr1 cproc,
        	lesp.dpr1 dproc,
        	lesp.hicl hicl,
            lesp.moti moti,
            lesp.fsal fsal ,
            lesp.link link,
        	linklesp.deli antiguedad,
			'Q' tipo_cq
        from 
        	lesp,
        	(select
        		centro,
        		norden,
        		cip,
        		hicl,
        		ape1,
        		ape2,
        		nomb,
        		cpr1,
        	        trunc( sum(decode(moti,17,finc,18,finc,19,finc,9,fsde,              
                               decode(fsde,null,nvl(fsal,sysdate),fsde))-finc)   
        	          -sum(decode(moti,9,nvl(tdes,0),0)))                           
        	          as deli                                                       
        	from   lesp                                                             
        	start with (link is null or norden=-link) 
			--and cip='CBLP290862911012' 
        	connect by prior norden=link                                            
        	group by centro,norden,cip,hicl,ape1,ape2,nomb,cpr1                     
        	) linklesp                                                              
        where                                                                    
        	  lesp.centro=linklesp.centro                                           
          and lesp.norden=linklesp.norden                                        
          and (lesp.link is null or lesp.link=-lesp.norden)                      
union
        select
        	cip,
        	cega         centro,
        	ncita        norden,
        	prestaci     cproc,
        	prestaci_des dproc,
        	hicl,
            null moti,
            null fsal ,
            null link,
        	fechacit-finclusi antiguedad,
			'C' tipo_cq

			
		from lespcex
  	    --where cip='CBLP290862911012'
