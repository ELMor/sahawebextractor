create or replace view lesp_uni_procs as
 SELECT cpr1 cod, dpr1 descrip 
 FROM lesp
 WHERE cpr1 not in (SELECT proc_cod FROM ses_procedimientos WHERE cpr1=proc_cod) 
UNION 
SELECT prestaci cod, prestaci_des descrip 
FROM lespcex 
WHERE prestaci not in (SELECT proc_cod FROM ses_procedimientos WHERE prestaci=proc_cod) ;