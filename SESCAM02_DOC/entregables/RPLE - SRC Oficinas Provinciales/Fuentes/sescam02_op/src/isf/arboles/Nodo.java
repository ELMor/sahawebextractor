/**
 * Copyright
*  1999 Uni�n Fenosa International Software Factory,
 *
*   S.A.. All Rights Reserved.
 *
*
 *
*   This software is the confidential and proprietary information of
 *
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
 *
*   disclose such confidential information and shall not accordance
 *
*   with the terms of the license agreement you entered into with
 *
*   I.S.F..
 *
 *
 */

package isf.arboles;
import java.util.Vector;
import java.util.Enumeration;
import isf.exceptions.*;
import isf.negocio.NegEvento;

/**
 * @author JDZ
 * @version 1.0
 * @since 7-10-2002
 * Clase abstracta que representa un nodo de un arbol
 */
public abstract class Nodo
{
  private long id;
  private long nType;
  private long dbType;
  private long proc_pk;
  private long cantidadHijos;
  private String desc;
  private String href;
  private Nodo parent;
  private Vector childs;
  private long parentN;
  private long levelsN;
  private long iEstado;// Estados posibles 0-> Cargado 1-> Insertado 2-> Borrado 3->Modificado
  public final long MAX_RESULT_SET=5000;
  /**
   * @since 7-10-2002
   */
  public Nodo()
  {
    childs=new Vector();
    iEstado=-1;
  }
  /**
   * @param _id
   * @since 7-10-2002
   */
  public Nodo(long _id)
  {
    id = _id;
  }
  /**
   * Carga las variables propias del Nodo
   * id  : Identificaci�n de Nodo
   * desc: Descripci�n del Nodo
   * type: Tipo de Nodo
   * href: URL llamada al cliquear el nodo.
   * Previamente se debe llamar al constructor con ID o setId;
   */

  public void load()
  {

  }

  public void SetNegEvento (NegEvento evento) {
  }

  public void loadChilds()
  {

  }

  /**
   * Devuelve una hashtable que representa los atributos del nodo. Previamente
   * se ha tenido que hacer un read para leer de la bd toda la informaci�n del nodo.
   * @return java.util.Hashtable
   * @since 7-10-2002
   */

  public String getDesc()
  {
    if (desc==null) return "";
    return desc;
  }

  public Nodo getParent()
  {
    return parent;
  }
  public void setParent(Nodo _parent)
  {
    this.parent=_parent;
  }

  public String toString()
  {
    return("["+this.getId()+","+this.getNType()+",\""+this.getDesc()+"\",\""+this.getHref()+"\"]");
  }
  public String childsToString(long pID){
    this.setId(pID);
    return(this.childsToString());
  }
  public String childsToString()
  {
    if (this.childs.size()==0) this.loadChilds();
    Enumeration e = childs.elements();
    Nodo nh;
    StringBuffer cadena= new StringBuffer("[");
    boolean bFt=true;
    while (e.hasMoreElements()){
      nh=(Nodo)e.nextElement();
      if (!bFt) cadena.append(","); else bFt=false;
      cadena.append(nh.toString());
    }
    return(cadena.append("]").toString());
  }

  public void setId(long pId)
  {
    id = pId;
  }
  public long getId() {
    return id;
  }

  public void setDesc(String pDesc)
  {
    desc = pDesc;
  }
  public String getHref() {
    if (href==null) return "";
    return href;
  }
  public void setHref(String href) {
    this.href = href;
  }
  public long getCantidadHijos() {
    return cantidadHijos;
  }
  public void setCantidadHijos(long cantidadHijos) {
    this.cantidadHijos = cantidadHijos;
  }
  public void incCantidadHijos() {
    this.cantidadHijos++;
  }
  public void setNType(long nType) {
    this.nType = nType;
  }
  public long getNType()
  {
    return nType;
  }
  public Vector getChilds()
  {
    if (this.childs.size()==0)
      this.loadChilds();
    return this.childs;
  }
  public void addChild(Nodo hijo){
    hijo.setParent(this);
    this.childs.add(hijo);
  }
  public void clearChilds(){
    this.childs.clear();
  }

  public void drop(long pID) throws ExceptionSESCAM,Exception {
    try {
      this.setId(pID);
      this.drop();
   }catch (ExceptionSESCAM e) {
     throw e;
   }
   catch (Exception ex) {
     throw ex;
   }
  }

  public void drop() throws ExceptionSESCAM,Exception {
  }
  public void insert() throws ExceptionSESCAM {
  }
  public void modify() throws ExceptionSESCAM {
  }

  public void delete(){
  }
  public void deleteChilds(){
    if (this.childs.size()==0) return;
    Enumeration e = childs.elements();
    Nodo nh;
    while (e.hasMoreElements()){
      nh=(Nodo)e.nextElement();
      nh.delete();
    }
  }
  public void save() throws ExceptionSESCAM {
    try {
      if (this.getIEstado()==1) this.insert(); //Inserta nuevos hijos
      else if (this.getIEstado()==2) {
            this.deleteChilds();
            this.delete();
           }
           else if (this.getIEstado()==3) this.modify();
   }catch (ExceptionSESCAM ex) {
     throw ex;
   }
  }

  public void saveChilds() throws ExceptionSESCAM {
    if (this.childs.size()==0) return;
    Enumeration e = childs.elements();
    Nodo nh;
    try {
      while (e.hasMoreElements()){
        nh=(Nodo)e.nextElement();
        nh.save();
      }
    }
    catch (ExceptionSESCAM ex) {
      throw ex;
    }
  }

  public void ParentsNumber(){

  }

  public void LevelsCount(){

  }

  public long getIEstado() {
    return iEstado;
  }
  public void setIEstado(long iEstado) {
    this.iEstado = iEstado;
  }
  public long getDbType() {
    return dbType;
  }
  public void setDbType(long dbType) {
    this.dbType = dbType;
  }
  public long getProc_pk() {
    return proc_pk;
  }
  public void setProc_pk(long proc_pk) {
    this.proc_pk = proc_pk;
  }
  public long getLevelsN() {
    return levelsN;
  }
  public void setLevelsN(long levelsN) {
    this.levelsN = levelsN;
  }
  public long getParentN() {
    return parentN;
  }
  public void setParentN(long parentN) {
    this.parentN = parentN;
  }
}

