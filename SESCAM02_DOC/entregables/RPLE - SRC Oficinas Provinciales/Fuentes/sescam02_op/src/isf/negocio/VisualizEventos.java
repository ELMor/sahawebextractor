/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: VisualizEventos </p>
 * <p>Descripcion: Clase que guarda informaci�n referente a un evento</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 12/12/2002
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */

public class VisualizEventos {
    /**
     * Nombre de la Oficina a la cual se le va a realizar la solicitud
     */
    public String 	evtoOficina="SD";
    /**
      * Login del usuario que realiza la sesion
      */
    public String 	evtoLogin="SD";
    /**
     *  pk del codigo de evento, de la tabla Evento_Tipo
     */
    public long        evtoCodigo ='0';
    /**
      * Descripcion del evento
      */
    public String 	evtoDesc="SD";
    /**
      * Fecha en la que se ha el inicio de la sesion
      */

    public String 	evtoFecha ="SD";

    /**
     * Evento PK seleccionado
     */
    public long         evtoevto_pk = 0;

    /**
     * Evento Sesion
     */
    public long         evtosess_pk = 0;


}