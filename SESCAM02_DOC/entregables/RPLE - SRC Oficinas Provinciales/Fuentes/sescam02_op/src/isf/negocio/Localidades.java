package isf.negocio;


/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import conf.*;

/**
 * <p>Clase: Localidades </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesLocalidades</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 04/11/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class Localidades {
  /**
   * Nombre de la clase de persistencia asociada a la clase de Negocio Localidades
   */
        public static final String ID_TABLA="SES_LOCALIDADES";

        private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


        // Devuelve un vector DE OBJETOS SeSLocalidades la clase de persistencia)
        // con las localidades que cumplan la clausula del where.

        /**
        * Devuelve un vector DE OBJETOS SesLocalidades(la clase de persistencia) con las localidades que cumplan la clausula del where.
        * @param String _where Clausula where para la consulta de objetos de clase SesLocalidades
        * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
        * @return Devuelve un vector DE OBJETOS SesLocalidades(la clase de persistencia) con las localidades que cumplan la clausula del where.
        * @autor Felix Alberto Bravo Hermoso
        */
        public Vector busquedaLoc(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);

                  v = SesLocalidades.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar la Localidad:"+ e) ;
                }

                finally{
                        pool.freeConnection(DB,conexion);
                }
                return v;
        }

        //Realiza la insercion de la Localidad.

        /**
         * Realiza la insercion de la Localidad.
         * @param SesLocalidades _sesLoc Objeto de clase SesLocalidades (clase de persistencia asociada a tabla Ses_Localidades)
         * @autor Felix Alberto Bravo Hermoso
         * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Localidades
        */
        public void insertarLoc(SesLocalidades _sesLoc) throws SQLException {
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                //System.out.println("Entra en insertarPerm");
                _sesLoc.newId(DB);
                _sesLoc.insert(conexion);
                conexion.commit();
                }catch(SQLException e){
                        conexion.rollback();
                        System.out.println("Error al Insertar la Localidad:"+ e);
                }catch(Exception ex){
                        conexion.rollback();
                        System.out.println("Error al Insertar la Localidad:"+ ex);
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

        //Realiza la modificacion de la Localidad.
        /**
         * Realiza la modificacion de la Localidad.
         * @param SesLocalidades _sesLoc Objeto de clase SesLocalidades (clase de persistencia asociada a tabla Ses_Localidades)
         * @autor Felix Alberto Bravo Hermoso
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Localidades
         */
        public void modificarLoc(SesLocalidades _sesLoc) throws SQLException  {
                Pool pool=null;
                Connection conexion = null;
                try{
                //System.out.println("Entra en modificarPerm");
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                _sesLoc.update(conexion);
                conexion.commit();
                }catch(SQLException e){
                        System.out.println("Error al Modificar la Localidad:"+ e);
                        conexion.rollback();
                }catch(Exception ex){
                        conexion.rollback();
                        System.out.println("Error al Modificar la Localidad:"+ ex);
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

        //Realiza la eliminacion de la Localidad.
        /**
         * Realiza la eliminacion de la Localidad..
         * @param SesLocalidades _sesLoc Objeto de clase SesLocalidades (clase de persistencia asociada a tabla Ses_Localidades)
         * @autor Felix Alberto Bravo Hermoso
         * @throws ExceptionSESCAM Exception producida si queremos eliminar una localidad que tenga registros asociados en otras tablas
         */
        public void eliminarLoc(SesLocalidades _sesLoc) throws ExceptionSESCAM,Exception {
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                SesOficina sOfi = new SesOficina();
                try{
                //System.out.println("Entra en metodo eliminarPerm");
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                v = sOfi.search(conexion,"PK_LOCALIDAD"+_sesLoc.getLocalidad(),"");
                //System.out.println("Tama�o v:"+v.size());
                if (v.size()>0){
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.LOCALIDAD_USADA);
                        throw es;
                }else{
                 _sesLoc.delete(conexion);
                 conexion.commit();
                 //System.out.println("Delete hecho");
                }

                }catch(SQLException e){
                        System.out.println("Error al Eliminar la Localidad:"+ e);
                        conexion.rollback();
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

        public String[] DameProvinciaLocalidad(long pk_localidad){
               Provincias p = new Provincias();
               Vector v = new Vector();
               long provincia_pk=0;
               String retorno[]= new String[2];
               try {
                 v = this.busquedaLoc("PK_LOCALIDAD=" + pk_localidad, "");
                 if (v.size() > 0) {
                   SesLocalidades sesL = (SesLocalidades) v.elementAt(0);
                   retorno[0]=sesL.getLocalidad();
                   provincia_pk = sesL.getProvinciaPk();
                 }
                 v.clear();
                 v = p.busquedaProv("PROVINCIA_PK=" + provincia_pk, "");
                 if (v.size() > 0) {
                   SesProvincias sesP = (SesProvincias) v.elementAt(0);
                   retorno[1] = sesP.getProvincia();
                 }
               }catch(Exception e){
                 System.out.println("Error en metodo DameProvincia: "+e);
               }
                 return retorno;
        }

} //fin de la clase

