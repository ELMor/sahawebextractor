/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;


// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
/**
 * <p>Clase : Procedimientos </p>
 * <p>Descripcion: Clase de negocio que maneja la clase de persistencia SesProcedimientos </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 04/11/2002
 * @author Jos� Vicente Nieto-M�rquez Fdez-Medina
 */


public class Procedimientos {

  /**
    * Nombre de la clase de persistencia asociada a la clase de Negocio Procedimientos
    */
        public static final String ID_TABLA="SES_PROCEDIMIENTOS";

        private static final String DB = "db"; //Nombre de la conexi�n a la base de datos

        private NegEvento negevent;

        // Devuelve un vector DE OBJETOS SesProcedimientos (la clase de persistencia)
        // con los procedimientos que cumplan la clausula del where.

        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }

        /**
          * Devuelve un vector DE OBJETOS SesProcedimientos(la clase de persistencia) con los procedimientos que cumplan la clausula del where.
          * @param String _where Clausula where para la consulta de objetos de clase SesProcedimientos
          * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesProcedimientos (la clase de persistencia) con los procedimientos que cumplan la clausula del where.
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */
        public Vector busquedaProced(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);

                  v = SesProcedimientos.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Procedimientos:"+ e) ;
                }

                finally{
                        pool.freeConnection(DB,conexion);
                }
                return v;
        }

        public Vector consulta_clasificacion(long clas_pk) throws Exception {

           // Validaci�n de parametros
           String sentBusqueda = new String("");
           Connection conn = null;
           Pool pool=null;

           Vector vec_Proc = new Vector();
           sentBusqueda= " select p.proc_pk,p.proc_cod, p.proc_desc_corta,p.proc_desc, "+
                         " p.proc_activo,p.proc_garantia,p.proc_monto_max_est,p.proc_tipo "+
                         " from ses_clasificaciones s, ses_procedimientos p "+
                         " where  s.sesclas_pk = ? "+
                         " and s.proc_pk = p.proc_pk ";
           pool = Pool.getInstance();
           conn = pool.getConnection("db");
           try{
             conn.setAutoCommit(false);
             PreparedStatement st=conn.prepareStatement(sentBusqueda);

             st.setLong(1,clas_pk);

             ResultSet rs=st.executeQuery();
             while(rs.next()) {
               SesProcedimientos sesproc =new SesProcedimientos();
               sesproc.setProcPk(rs.getLong(1));
               sesproc.setProcCod(rs.getString(2));
               sesproc.setProcDescCorta(rs.getString(3));
               sesproc.setProcDesc(rs.getString(4));
               sesproc.setProcActivo(rs.getLong(5));
               sesproc.setProcGarantia(rs.getLong(6));
               sesproc.setProcMontoMaxEst(rs.getDouble(7));
               sesproc.setProcTipo(rs.getLong(8));
               vec_Proc.addElement(sesproc);
             }
             conn.commit();
             try {
                 rs.close();
                 st.close();
               }
             catch (Exception ex){}
             }catch(Exception e)
             {
               try{conn.rollback();} catch(Exception esql){}
               e.printStackTrace();
               throw e;
             }
             finally{
               pool.freeConnection("db", conn);
             }
             return vec_Proc;
  }
        //Realiza la insercion del Procedimiento.
        /**
          * Realiza la insercion del Procedimiento
          * @param SesProcedimientos _sesproc Objeto de clase SesProcedimientos (clase de persistencia asociada a tabla Ses_Procedimientos)
          * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Procedimientos
         */
        public void insertarProced(SesProcedimientos _sesproc) throws SQLException,ExceptionSESCAM {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                //System.out.println("Entra en insertarPerm");
                _sesproc.newId(DB);
                _sesproc.insert(conexion);

                try {
                  if (negevent != null) {
                    Long dato = new Long(_sesproc.getProcPk());
                    negevent.addVar(1,dato.toString());
                    negevent.addVar(2,_sesproc.getProcCod());
                    negevent.addVar(3,_sesproc.getProcDescCorta());
                    dato = new Long (_sesproc.getProcActivo());
                    negevent.addVar(4,dato.toString());
                    dato = new Long (_sesproc.getProcGarantia());
                    negevent.addVar(5,dato.toString());
                    Double monto = new Double (_sesproc.getProcMontoMaxEst());
                    negevent.addVar(6,monto.toString());
                    negevent.guardar(conexion);
                  }
                }
                catch (SQLException ex) {
                  fichero_log = Log.getInstance();
                  fichero_log.warning(" No se ha realizado el evento de inserci�n del nuevo procedimiento con codigo " + _sesproc.getProcCod() + " Error -> " + ex.toString());
                }

                conexion.commit();
                }catch(SQLException e){
                        conexion.rollback();
//                        System.out.println("Error al Insertar el Procedimiento:"+ e);
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha podido realizar la insercion del procedimiento con codigo " + _sesproc.getProcCod() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
                }catch(Exception ex){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha podido realizar la insercion del procedimiento con codigo " + _sesproc.getProcCod() + " Error -> " + ex.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
//                        System.out.println("Error al Insertar el Procedimiento:"+ ex);
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }


        //Realiza la modificacion del Procedimiento.
        /**
          * Realiza la modificaci�n del Procedimiento
          * @param SesProcedimientos _sesproc Objeto de clase SesProcedimientos (clase de persistencia asociada a tabla Ses_Procedimientos)
          * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Procedimientos
         */
        public void modificarProced(SesProcedimientos _sesproc) throws SQLException,ExceptionSESCAM  {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                try{

                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                _sesproc.update(conexion);
                try {
                   if (negevent != null) {
                    Long dato = new Long(_sesproc.getProcPk());
                    negevent.addVar(7,dato.toString());
                    negevent.addVar(8,_sesproc.getProcCod());
                    negevent.addVar(9,_sesproc.getProcDescCorta());
                    dato = new Long (_sesproc.getProcActivo());
                    negevent.addVar(10,dato.toString());
                    dato = new Long (_sesproc.getProcGarantia());
                    negevent.addVar(11,dato.toString());
                    Double monto = new Double (_sesproc.getProcMontoMaxEst());
                    negevent.addVar(12,monto.toString());
                    negevent.guardar(conexion);
                  }
                }catch (SQLException e) {
                  fichero_log = Log.getInstance();
                  fichero_log.warning(" No se ha realizado el evento de modificaci�n del procedimiento con codigo " + _sesproc.getProcCod() + " Error -> " + e.toString());
                }
                 conexion.commit();
                }catch(SQLException e){
                     //   System.out.println("Error al Modificar el Procedimiento:"+ e);
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha podido realizar la modificaci�n del procedimiento con codigo " + _sesproc.getProcCod() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                }catch(Exception ex){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha podido realizar la modificaci�n del procedimiento con codigo " + _sesproc.getProcCod() + " Error -> " + ex.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                  //      System.out.println("Error al Modificar el Procedimiento:"+ ex);
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

        //Realiza la eliminacion del Procedimiento.
        /**
         * Realiza la eliminaci�n del Procedimiento
         * @param SesProcedimientos _sesproc Objeto de clase SesProcedimientos (clase de persistencia asociada a tabla Ses_Procedimientos)
         * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
         * @throws Exception Exception producida por la operacion DELETE sobre tabla Ses_Procedimientos
         */

        public void eliminarProcedimiento(SesProcedimientos _sesproc) throws Exception,ExceptionSESCAM {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;

                try{
                 pool = Pool.getInstance();
                 conexion = pool.getConnection(DB);
                 _sesproc.delete(conexion);
                 conexion.commit();
                }catch(SQLException e){
 //                System.out.println("Error al borrar el Procedimiento:"+ e);
                  fichero_log = Log.getInstance();
                  fichero_log.error(" No se ha podido realizar la eliminaci�n del procedimiento con codigo " + _sesproc.getProcCod() + " Error -> " + e.toString());
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                  throw es;
                }catch(Exception ex){
                  conexion.rollback();
//                  System.out.println("Error al Modificar el Procedimiento:"+ ex);
                  fichero_log = Log.getInstance();
                  fichero_log.error(" No se ha podido realizar la eliminaci�n del procedimiento con codigo " + _sesproc.getProcCod() + " Error -> " + ex.toString());
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                  throw es;
                }
                finally{
                  pool.freeConnection(DB,conexion);
                }
        }


/*		public static void main(String args[]) {
            Vector v = new Vector();
                Usuarios user = new Usuarios();
                try
                 {
                v = user.busquedaUsu("ACTIVO_SN=1 AND SYSLOGIN='adm'","APELLIDO1 ASC");
                System.out.println("4") ;
                for(int int_pos = 0;int_pos < v.size(); int_pos++){
                                                SysUsu x=(SysUsu)v.elementAt(int_pos);
                                                System.out.print(x.getApellido1() +"-");
                                                System.out.print(x.getApellido2() +"-");
                                                System.out.println(x.getNombre() +"");
                        }
                 }catch (Exception e){
                        System.out.println("Error Excepcion Main:"+ e) ;
                 }

                  }		*/


} //fin de la clase

