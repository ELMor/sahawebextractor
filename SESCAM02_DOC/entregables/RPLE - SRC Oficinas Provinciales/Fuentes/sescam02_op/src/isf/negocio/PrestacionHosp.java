/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import conf.Constantes;

/**
 * <p>Clase: PrestacionHosp </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia Prestacion </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 28/03/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class PrestacionHosp {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio Prestacion
            */
	public static final String ID_TABLA="PRESTACION";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS Prestacion(la clase de persistencia) con las prestaciones que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase Prestacion
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS Prestacion (la clase de persistencia) con los servicios que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

	public Vector busquedaPrest(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
		try{

	  		v = Prestacion.search(conexion,_where,_sort);
		}
		catch(Exception e){
			System.out.println("Error al Buscar Prestacion Hospitalaria:"+ e) ;
		}

		finally{
 			pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
		}
		return v;
	}
} //fin de la clase

