/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import conf.*;
import isf.util.log.Log;
/**
 * <p>Clase : Oficina </p>
 * <p>Descripcion: Clase de negocio que maneja la clase de persistencia SesOficina</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 04/11/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class Oficina {

  /**
     * Nombre de la clase de persistencia asociada a la clase de Negocio Oficina
     */

        public static final String ID_TABLA="SES_OFICINA";

        private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


        private NegEvento negevent;



        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }

        // Devuelve un vector DE OBJETOS SesOficina(la clase de persistencia)
        // con las oficinas que cumplan la clausula del where.

        /**
        * Devuelve un vector DE OBJETOS SesOficina(la clase de persistencia) con las oficinas que cumplan la clausula del where.
        * @param String _where Clausula where para la consulta de objetos de clase SesOficina
        * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
        * @return Devuelve un vector DE OBJETOS SesOficina(la clase de persistencia) con las oficinas que cumplan la clausula del where.
        * @autor Felix Alberto Bravo Hermoso
        */
        public Vector busquedaOfi(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                v = SesOficina.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Oficina:"+ e) ;
                }

                finally{
                        pool.freeConnection(DB,conexion);
                }
                return v;
        }

        //Realiza la insercion de la Oficina.
        /**
         * Realiza la insercion de la Oficina.
         * @param SesOficina _sesOfi Objeto de clase SesOficina (clase de persistencia asociada a tabla Ses_Oficina)
         * @autor Felix Alberto Bravo Hermoso
         * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Oficina
         */

        public void insertarOfi(SesOficina _sesOfi) throws SQLException,ExceptionSESCAM {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                try{
                 pool = Pool.getInstance();
                 conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                 //System.out.println("Entra en insertarOfi");
                 _sesOfi.newId(DB);
                 _sesOfi.insert(conexion);
                 try {
                   if (negevent != null) {
                     Long dato = new Long (_sesOfi.getOficinaPk());
                     negevent.addVar(42,dato.toString());
                     negevent.addVar(43,_sesOfi.getOficinaNombre());
                     negevent.addVar(44,_sesOfi.getOficinaDireccion());
                     dato = new Long (_sesOfi.getOficinaTelefono());
                     negevent.addVar(45,dato.toString());
                     dato = new Long (_sesOfi.getOficinaCodpostal());
                     negevent.addVar(46,dato.toString());
                     dato = new Long (_sesOfi.getPkLocalidad());
                     negevent.addVar(47,dato.toString());
                     negevent.addVar(48,_sesOfi.getSyslogin());
                     negevent.guardar(conexion);
                   }
                 }
                 catch (Exception e) {
                   fichero_log = Log.getInstance();
                   fichero_log.warning(" No se ha realizado el evento de inserci�n de la nueva oficina con nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + e.toString());
                 }
                 conexion.commit();
                }catch(SQLException e){
                        conexion.rollback();
//                        System.out.println("Error al Insertar la Oficina:"+ e);
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion de la nueva oficina con nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;

                }catch(Exception ex){
                        conexion.rollback();
                        //System.out.println("Error al Insertar la Oficina:"+ ex);
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion de la nueva oficina con nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + ex.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;

                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

        //Realiza la modificacion de la Oficina.
        /**
         * Realiza la modificacion de la Oficina.
         * @param SesOficina _sesOfi Objeto de clase SesOficina (clase de persistencia asociada a tabla Ses_Oficina)
         * @autor Felix Alberto Bravo Hermoso
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Oficina
         */
        public void modificarOfi(SesOficina _sesOfi) throws SQLException,ExceptionSESCAM  {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                try{
                //System.out.println("Entra en modificarOfi");
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                _sesOfi.update(conexion);
                try {
                  if (negevent != null) {
                   Long dato = new Long (_sesOfi.getOficinaPk());
                   negevent.addVar(49,dato.toString());
                   negevent.addVar(50,_sesOfi.getOficinaNombre());
                   negevent.addVar(51,_sesOfi.getOficinaDireccion());
                   dato = new Long (_sesOfi.getOficinaTelefono());
                   negevent.addVar(52,dato.toString());
                   dato = new Long (_sesOfi.getOficinaCodpostal());
                   negevent.addVar(53,dato.toString());
                   dato = new Long (_sesOfi.getPkLocalidad());
                   negevent.addVar(54,dato.toString());
                   negevent.addVar(55,_sesOfi.getSyslogin());

                   negevent.guardar(conexion);
                 }
                }catch (Exception e) {
                  fichero_log = Log.getInstance();
                  fichero_log.warning(" No se ha realizado el evento de modificaci�n de la oficina de nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + e.toString());
                }
                conexion.commit();
                }catch(SQLException e){
                        //System.out.println("Error al Modificar la Oficina:"+ e);
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificaci�n de la  oficina de nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                }catch(Exception ex){
                        conexion.rollback();
//                        System.out.println("Error al Modificar la Oficina:"+ ex);
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificaci�n de la  oficina de nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + ex.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

        //Realiza la eliminacion de la Oficina.
        /**
         * Realiza la eliminacion de la Oficina..
         * @param SesOficina _sesOfi Objeto de clase SesOficina (clase de persistencia asociada a tabla Ses_Oficina)
         * @autor Felix Alberto Bravo Hermoso
         * @throws ExceptionSESCAM Exception producida al incumplirse las restricciones de integridad con alguna de las tablas asociadas
         */

        public void eliminarOfi(SesOficina _sesOfi) throws ExceptionSESCAM,Exception {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{
                  _sesOfi.delete(conexion);
                  try {
                    if (negevent != null) {
                      Long dato = new Long (_sesOfi.getOficinaPk());
                      negevent.addVar(56,dato.toString());
                      negevent.guardar(conexion);
                    }
                  }
                  catch (SQLException ex) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha realizado el evento de eliminaci�n de la oficina de nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + ex.toString());
                  }
                  conexion.commit();
                }catch(SQLException e){
                  String mensaje = new String(e.getMessage());
                  if (mensaje.indexOf("FK_SYS_USU_RELATION__") > 0) {
                   conexion.rollback();
                   fichero_log = Log.getInstance();
                   fichero_log.error(" No se ha realizado la eliminaci�n de la  oficina de nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + e.toString());

                   ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.OFICINA_USADA);
                   throw es;
                  }
                  else {
                    if (mensaje.indexOf("FK_SES_GARA") > 0) {
                     conexion.rollback();
                     fichero_log = Log.getInstance();
                     fichero_log.error(" No se ha realizado la eliminaci�n de la  oficina de nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + e.toString());

                     ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.OFICINA_TIENE_TRAMO_GARANTIA);
                     throw es;
                    }
                    else {
                      conexion.rollback();
                      fichero_log = Log.getInstance();
                      fichero_log.error(" No se ha realizado la eliminaci�n de la  oficina de nombre  = " + _sesOfi.getOficinaNombre() + " Error -> " + e.toString());
                      ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                      throw es;
//                      System.out.println(" Error al borrar una oficina: "+e);
                    }
                  }
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }


/*		public static void main(String args[]) {
            Vector v = new Vector();
                Usuarios user = new Usuarios();
                try
                 {
                v = user.busquedaUsu("ACTIVO_SN=1 AND SYSLOGIN='adm'","APELLIDO1 ASC");
                System.out.println("4") ;
                for(int int_pos = 0;int_pos < v.size(); int_pos++){
                                                SysUsu x=(SysUsu)v.elementAt(int_pos);
                                                System.out.print(x.getApellido1() +"-");
                                                System.out.print(x.getApellido2() +"-");
                                                System.out.println(x.getNombre() +"");
                        }
                 }catch (Exception e){
                        System.out.println("Error Excepcion Main:"+ e) ;
                 }

                  }		*/


} //fin de la clase

