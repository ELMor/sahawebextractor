/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

import java.sql.ResultSet;
import java.sql.SQLException;
import beans.ConsultaEntradasLE;

/**
 * <p>Clase: DatosEstadisticasLEQ </p>
 * <p>Descripcion: Clase generica donde se guardan los registros obtenidos por la consulta</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 29/01/2003
 * @author Jose Vicente Nieto
 */

public class DatosEstadisticasLEQ {

     //   public DatosEstadisticasLEQ() {}

	/**
	 * constructor avanzado de la clase, recibe el n�mero de tramos y
	 * la fila en forma de resulset para su inicializacion autom�tica.
	 * @param ntramos numero de tramos de los que se tienen datos
	 * @param rs Resultset en los que se tienen los datos de la estadistica.
	 * @throws SQLException exception por problemas al acceder al resultset
	 * @author Fco. Pascual Romero
	 */

	public DatosEstadisticasLEQ(int ntramos, ResultSet rs) throws SQLException {

              // entidad estudiada
              DatoGroupby  = rs.getString(1);
              // datos de los tramos
              tramos = new long[ntramos];
    for (int int_pos = 0;int_pos<ntramos;int_pos++) {
			 tramos[int_pos] = rs.getInt(int_pos+2);
    }
		// datos de las situaciones
		int nsituaciones =  ConsultaEntradasLE.STR_SITUACION.length - 3;
    situaciones = new long[nsituaciones];
    for (int int_pos = 0;int_pos< nsituaciones;int_pos++) {
			situaciones[int_pos] = rs.getInt(ntramos+int_pos+2);
    }
		// campos fijos
		pacientes  =rs.getInt(ntramos+nsituaciones+2);
 		demoramedia = (pacientes != 0)  ?
								 rs.getDouble(ntramos+nsituaciones+3) / pacientes : 0;
		pendientes = rs.getInt(ntramos+nsituaciones+4);
    demmediapendientes = rs.getInt(ntramos+nsituaciones+5);
	}


     /**
       * Nombre del Dato por el que se a agrupar el listado puede ser nombre servicio, nombre de centro, especialidad ..
       */
     public String 	DatoGroupby="SD";
     /**
      *  Valor obtenido en la estadistica para cada uno de los tramos
      */

     public long tramos[] = null;

		 /**
      *  Valor del total de pacientes por situacion
      */
     public long situaciones[] =null;
     /**
      *  Valor del total de pacientes resultantes de la estadistica
      */
     public long        pacientes=0;
     /**
      *  Valor del total de pacientes pendientes resultantes de la estadistica
      */
     public long        pendientes=0;
     /**
      *  Valor de la demora media para ese servicio segun el numero de pacientes
      */
     public double       demoramedia=0;
     /**
      *  Valor de la demora media para pendientes
      */
     public double       demmediapendientes=0;

}