/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;

/**
 * <p>Clase : LogFormularios </p>
 * <p>Descripcion: Clase de negocio que maneja el Log de impresion de documentos </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 28/10/2002
 * @author Iv�n Enrique Caneri
 */
public class LogFormularios {


	public static final String ID_TABLA="";

	private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


	// Devuelve un vector DE OBJETOS SysUsu(la clase de persistencia)
	// con los usuarios que cumplan la clausula del where.
        /**
          * Devuelve un vector DE OBJETOS FormLog (la clase de persistencia) con los registros que cumplan la clausula del where.
          * @param String _where Clausula where para la consulta de objetos de clase FormLog
          * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS FormLog (la clase de persistencia) con los registros que cumplan la clausula del where.
          * @autor IEC
          */
	public Vector busquedaLogForm(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		try{
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);

	  		v = Formlog.search(conexion,_where,_sort);
		}
		catch(Exception e){
			System.out.println("Error al Buscar Usuarios:"+ e) ;
		}

		finally{
			pool.freeConnection(DB,conexion);
		}
		return v;
	}

        /**
          * Realiza la insercion de un registro en tabla FormLog
          * @param FormLog _formlog Objeto de clase FormLog (clase de persistencia asociada a tabla FormLog)
          * @autor IEC
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla FormLog
          */
	public void insertarLogForm(Formlog _formlog) throws SQLException{
		Pool pool=null;
		Connection conexion = null;
		try{
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);

	  	_formlog.insert(conexion);
                conexion.commit();
		}
		catch(Exception e){
                  conexion.rollback();
		  System.out.println("Error al Insertar el Usuarios:"+ e) ;
		}

		finally{
			pool.freeConnection(DB,conexion);
		}

	}

	//Realiza la modificacion del FomLog.
        /**
          * Realiza la modificacion de los datos del log de impresi�n de documentos.
          * @param FormLog _formlog Objeto de clase FormLog (clase de persistencia asociada a tabla FormLog)
          * @autor IEC
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla FormLog
          */

	public void modificarLogForm(Formlog _formlog) throws SQLException{
		Pool pool=null;
		Connection conexion = null;
		try{
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);

	  	_formlog.update(conexion);
                conexion.commit();
		}
		catch(Exception e){
                  conexion.rollback();
                  System.out.println("Error al Modificar el Usuario:"+ e) ;
		}

		finally{
			pool.freeConnection(DB,conexion);
		}

	}


/*		public static void main(String args[]) {
	    Vector v = new Vector();
		Usuarios user = new Usuarios();
		try
 		{
		v = user.busquedaUsu("ACTIVO_SN=1 AND SYSLOGIN='adm'","APELLIDO1 ASC");
		System.out.println("4") ;
		for(int int_pos = 0;int_pos < v.size(); int_pos++){
						SysUsu x=(SysUsu)v.elementAt(int_pos);
						System.out.print(x.getApellido1() +"-");
						System.out.print(x.getApellido2() +"-");
						System.out.println(x.getNombre() +"");
			}
 		}catch (Exception e){
			System.out.println("Error Excepcion Main:"+ e) ;
 		}

  		}		*/


} //fin de la clase

