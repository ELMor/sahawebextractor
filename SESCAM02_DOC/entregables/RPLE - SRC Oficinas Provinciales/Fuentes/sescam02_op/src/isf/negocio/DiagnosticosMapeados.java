/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;


/**
 * <p>Clase: ProcedimientosMapeados </p>
 * <p>Descripcion: Clase que maneja el bean de consulta de procedimientos mapeados</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 19/02/2003
 * @author Jos� Vicente Nieto-M�rquez Fdez-Medina
 */

public class DiagnosticosMapeados {
     /**
        * Codigo del Diagnostico  obtenido en la consulta
        */
       public String 	CodDiagSESCAM="SD";
       /**
         * Descripci�n del diagnostico obtenido en la consulta
         */
       public String 	DiagDesc="SD";

       /**
        *  Codigo Mapeado
        */
       public String        CodDiagMapeado="SD";
       /**
        *  Centro
        */
       public long      centro=0;
}
