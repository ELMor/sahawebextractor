/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

import isf.db.*;
import isf.exceptions.*;
import isf.arboles.Nodo;
import java.sql.*;
import isf.persistencia.*;
import java.util.Vector;
import conf.Constantes;
import isf.util.log.Log;

public class NodoArbol extends Nodo{
  private NegEvento negevent;

  public NodoArbol(){
    super();
  }



   /**
     * Instancia el objeto de clase NegEvento
     * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
     * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
     */

   public void SetNegEvento (NegEvento evento) {
     negevent = evento;
   }

  public void loadChilds()
  {
    // Validaci�n de parametros
    if (this.getId()==0){
      return;
    }
    Connection conn = null;
    Pool ref_pool=null;
    PreparedStatement st;
    ResultSet rs;
    String sentBusqueda;
    long lNodoTipo=0;
    if (this.getId()==-1)
      sentBusqueda=" select sesclas_pk, sp.proc_pk proc_pk, "+
                   "        sesclas_desc, sesclas_tipo, pt.PROC_TCOD||' '||proc_cod||' '||proc_desc proc_desc "+
                   " from ses_clasificaciones sc, ses_procedimientos sp , ses_proc_tipos pt "+
                   " where sc.proc_pk=sp.proc_pk(+) and sesclas_pkpadre is null and sp.PROC_TIPO=pt.PROC_TIPO(+) ";
      else
      sentBusqueda=" select sesclas_pk, sp.proc_pk proc_pk, "+
                   "        sesclas_desc, sesclas_tipo, sesclas_tipo, pt.PROC_TCOD||' '||proc_cod||' '||proc_desc proc_desc "+
                   " from ses_clasificaciones sc, ses_procedimientos sp , ses_proc_tipos pt "+
                   " where sc.proc_pk=sp.proc_pk(+) and sesclas_pkpadre=? and sp.PROC_TIPO=pt.PROC_TIPO(+) ";
    this.setCantidadHijos(0);
    this.clearChilds();
    ref_pool = Pool.getInstance();
    conn = ref_pool.getConnection("db");
    try{
      conn.setAutoCommit(false);
      st=conn.prepareStatement(sentBusqueda);
      if (getId()!=-1)
        st.setLong(1,getId());
      rs=st.executeQuery();
      while(rs.next()) {
        incCantidadHijos();
        NodoArbol nodoHijo=new NodoArbol();
        nodoHijo.setId(rs.getLong("sesclas_pk"));
        nodoHijo.setIEstado(0);// Estado Nodo Cargado
        lNodoTipo=rs.getLong("sesclas_tipo");
        if ((lNodoTipo==1)||(lNodoTipo==2)||(lNodoTipo==4))
          nodoHijo.setNType(1);
        else
          nodoHijo.setNType(0);
        if(nodoHijo.getNType()==0)
          nodoHijo.setDesc(rs.getString("proc_desc"));
        else
          nodoHijo.setDesc(rs.getString("sesclas_desc"));
        if (lNodoTipo==1) nodoHijo.setHref("javascript:window.menuClasificacion.id_clas="+nodoHijo.getId()+";window.showMenu(window.menuClasificacion,100);");
          else if (lNodoTipo==2) nodoHijo.setHref("javascript:window.menuClasificacion.id_clas="+nodoHijo.getId()+";window.showMenu(window.menuClasificacion,100);");
          else if (lNodoTipo==3) nodoHijo.setHref("javascript:window.menuProcedimientos.id_clas="+nodoHijo.getId()+";window.showMenu(window.menuProcedimientos,100);");
          else if (lNodoTipo==4) nodoHijo.setHref("javascript:window.menuClasificacionProc.id_clas="+nodoHijo.getId()+";window.showMenu(window.menuClasificacionProc,100);");
        if (getCantidadHijos()==MAX_RESULT_SET) break;
        this.addChild(nodoHijo);
      }
      conn.commit();
       try {
          rs.close();
          st.close();
        }
       catch (Exception ex){ex.printStackTrace();}
      }catch(Exception e){
        try{conn.rollback();} catch(Exception esql){}
      }
      finally{
        ref_pool.freeConnection("db", conn);
      }
  }
  public void drop() throws ExceptionSESCAM,SQLException
  {
    Log fichero_log = null;
    String dato = "";
    if (this.getId()==0) return;
    Connection conn = null;
    Pool ref_pool=null;
    PreparedStatement st;
    ResultSet rs;
    String sentDelete;
    sentDelete="delete ses_clasificaciones where sesclas_pk in "+
               "(select sesclas_pk from ses_clasificaciones "+
               "start with sesclas_pk=? connect by prior sesclas_pk= sesclas_pkpadre)";
    ref_pool = Pool.getInstance();
    conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
    try{
     conn.setAutoCommit(false);
     st=conn.prepareStatement(sentDelete);
     st.setLong(1,getId());
     rs=st.executeQuery();
     try{
       if (negevent != null) {
         dato =  new Long(this.getId()).toString();
         negevent.addVar(129,dato);
         negevent.guardar(conn);
       }
     }catch(Exception e){
       fichero_log = Log.getInstance();
       fichero_log.warning(" No se ha insertado el evento de eliminacion de la Clasificacion  con Pk = " + this.getId() + " Error -> " + e.toString());
     }
      conn.commit();

      try {
          rs.close();
          st.close();
      }catch (Exception ex){}
      }catch(SQLException e){
          conn.rollback();
          fichero_log = Log.getInstance();
          fichero_log.error(" No se ha realizado la eliminaci�n de la Clasificacion = " + this.getId() + " Error -> " + e.toString());
          ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
          throw es;
      }
      finally{
        ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conn);
      }
  }
  public void insert() throws ExceptionSESCAM {
    SesClasificaciones sc = new SesClasificaciones();
    Clasificaciones clasif = new Clasificaciones();
    if(this.getDbType()==1)
      sc.setNullSesclasPkpadre();
    else
      sc.setSesclasPkpadre(this.getParent().getId());
    sc.setSesclasDesc(this.getDesc());
    sc.setSesclasTipo(this.getDbType());

    if (this.getCantidadHijos()==0)
      sc.setNullSesclasNiveles();
    else
      sc.setSesclasNiveles(this.getCantidadHijos());
    if (this.getProc_pk()==0)
      sc.setNullProcPk();
    else
      sc.setProcPk(this.getProc_pk());
    try{
      clasif.insertarClasificacion(sc);
      /* Despues de insertar el nodo conozco el ID, recien enntonces seteo el ID*/
      this.setId(sc.getSesclasPk());
      /*El href tambien depende del ID*/

      if (this.getDbType()==1) this.setHref("javascript:window.menuClasificacion.id_clas="+this.getId()+";window.showMenu(window.menuClasificacion,100);");
      else if (this.getDbType()==2) this.setHref("javascript:window.menuClasificacion.id_clas="+this.getId()+";window.showMenu(window.menuClasificacion,100);");
      else if (this.getDbType()==3) this.setHref("javascript:window.menuProcedimientos.id_clas="+this.getId()+";window.showMenu(window.menuProcedimientos,100);");
      else if (this.getDbType()==4) this.setHref("javascript:window.menuClasificacionProc.id_clas="+this.getId()+";window.showMenu(window.menuClasificacionProc,100);");
    }catch ( ExceptionSESCAM eses ){
      eses.printStackTrace();
      throw eses;
    }
    catch ( Exception e ){
      e.printStackTrace();
    }

  }



  public void delete(){
    Clasificaciones clasif = new Clasificaciones();
    SesClasificaciones sc;
    Vector v = new Vector();
    v = clasif.busquedaClasificaciones("SESCLAS_PK="+this.getId(),"");
    if (v.size()==0) return;
    try{
      sc = (SesClasificaciones) v.elementAt(0);
      clasif.eliminarClasificacion(sc);
    }catch ( Exception eses ){
      eses.printStackTrace();
    }
  }

  public void LevelsCount(){
    if (this.getId()==0){
     return;
    }
    Connection conn = null;
    Pool ref_pool=null;
    PreparedStatement st;
    ResultSet rs;
    String sentBusqueda="";
    long lLevels=0;
    sentBusqueda=   "select sesclas_niveles from ses_clasificaciones "+
                    "where sesclas_pkpadre is null "+
                    "start with sesclas_pk=? connect by sesclas_pk=  prior sesclas_pkpadre";
    ref_pool = Pool.getInstance();
    conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
    try{
      conn.setAutoCommit(false);
      st=conn.prepareStatement(sentBusqueda);
      st.setLong(1,getId());
      rs=st.executeQuery();
      while(rs.next()) {
        this.setLevelsN(rs.getLong("sesclas_niveles"));
      }
      conn.commit();
      try {
        rs.close();
        st.close();
      }catch (Exception ex){}
    }catch(Exception e){
     try{conn.rollback();} catch(Exception esql){}
   }
   finally{
     ref_pool.freeConnection("db", conn);
   }
  }

  public void ParentsNumber(){
    if (this.getId()==0){
     return;
    }
    Connection conn = null;
    Pool ref_pool=null;
    PreparedStatement st;
    ResultSet rs;
    String sentBusqueda="";
    long lParents=0;
    sentBusqueda= "select max(level) parents from ses_clasificaciones "+
                  "where sesclas_pkpadre is null "+
                  "start with sesclas_pk=? connect by sesclas_pk=  prior sesclas_pkpadre";
    ref_pool = Pool.getInstance();
    conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
    try{
      conn.setAutoCommit(false);
      st=conn.prepareStatement(sentBusqueda);
      st.setLong(1,getId());
      rs=st.executeQuery();
      while(rs.next()) {
        this.setParentN(rs.getLong("parents"));
      }
      conn.commit();
      try {
        rs.close();
        st.close();
      }catch (Exception ex){}
    }catch(Exception e){
     try{conn.rollback();} catch(Exception esql){}
   }
   finally{
     ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conn);
   }
   return;
  }



  public static void main(String args[]){

//          NodoArbol np=new NodoArbol();
//          System.out.println(np.childsToString(-1));

          String descripcion = "Prueba";
          long niveles = 2;
          Nodo nodoPadre = new NodoArbol();
          Nodo nodoHijo  = new NodoArbol();
/*        nodoPadre.setId(359);
          nodoHijo.setIEstado(1);// Estado Nodo Insertado
          nodoHijo.setNType(1);
          nodoHijo.setDbType(2);
          nodoHijo.setId(-1);
          nodoHijo.setDesc(descripcion);
          nodoHijo.setCantidadHijos(niveles);
          nodoPadre.addChild(nodoHijo);
     //     nodoPadre.saveChilds();

          String retorno = nodoHijo.toString();
          System.out.println(retorno);*/

          nodoPadre.setId(2);
          nodoPadre.ParentsNumber();
          nodoPadre.LevelsCount();
          System.out.println("Numero de Padres:"+nodoPadre.getParentN());
          System.out.println("Numero de Niveles del raiz:"+nodoPadre.getLevelsN());

          }


}
