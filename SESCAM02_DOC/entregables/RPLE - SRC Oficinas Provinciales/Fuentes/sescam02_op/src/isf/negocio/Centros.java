/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
  import java.sql.*;
  import java.util.Vector;
  import isf.db.*;
  import isf.persistencia.*;
  import conf.*;

/**
 * <p>Clase: Centros</p>
 * <p>Descripcion: Clase de negocio de la tabla Centrosescam</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 30/10/2002
 * @author F�lix Alberto Bravo Hermoso
 */
  public class Centros {

    /**
       * Nombre de la clase de persistencia asociada a la clase de Negocio Cargos
    */
          public static final String ID_TABLA="CENTROSESCAM";


          // Devuelve un vector DE OBJETOS Centrosescam(la clase de persistencia)
          // con los permisos que cumplan la clausula del where.

          /**
           * Devuelve un vector DE OBJETOS Centrosescam(la clase de persistencia)con los centros que cumplan la clausula del where.
           * @param String _where Clausula where para la consulta de objetos de clase Centrosescam
           * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
           * @return  Devuelve un vector DE OBJETOS Centrosescam(la clase de persistencia)con los Centros que cumplan la clausula del where.
           * @autor   Felix Alberto Bravo Hermoso
           */
          public Vector busquedaCentro(String _where,String _sort) {
                  Vector v = new Vector();
                  Pool pool=null;
                  Connection conexion = null;
                  try{
                  pool = Pool.getInstance();
                  conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                  v = Centrosescam.search(conexion,_where,_sort);
                  }
                  catch(Exception e){
                          System.out.println("Error al Buscar Centro:"+ e) ;
                  }

                  finally{
                          pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                  }
                  return v;
          }

          /**
           * Metodo que dado un pk de centro devuelve su descripcion.
           * @param cent ,long, pk del centro.
           * @return String, codigo y ldescripcion o nombre del centro.
           */
          public String dameDescCentro(long cent) {
                  String desc="";
                  Vector v = new Vector();
                  Pool pool=null;
                  Connection conexion = null;
                  try{
                   pool = Pool.getInstance();
                   conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                   v = Centrosescam.search(conexion,"CODCENTRO="+cent,"");
                   if (v!=null){
                      Centrosescam cses = (Centrosescam)v.elementAt(0);
//                    desc=new Long(new Double(cses.getCodcentro()).longValue()).toString()+"-"+cses.getDescentro();
                      desc=cses.getDescentro();
                   }
                  }catch(Exception e){
                          System.out.println("Error al Obtener descripcion del Centro:"+ e) ;
                  }

                  finally{
                          pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                  }
                  return desc;
          }


          /**
           * Metodo que dado un pk de centro devuelve su descripcion larga.
           * @param cent ,long, pk del centro.
           * @return String, descripcion larga .
           */

          public String dameDescCentroLarga(long cent) {
            String desc="";
            Vector v = new Vector();
            Pool pool=null;
            Connection conexion = null;
            try{
              pool = Pool.getInstance();
              conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
              v = Centrosescam.search(conexion,"CODCENTRO="+cent,"");
              if (v!=null){
                Centrosescam cses = (Centrosescam)v.elementAt(0);
                desc=cses.getDescentrolarga();
              }
            }catch(Exception e){
              System.out.println("Error al Obtener descripcion del Centro:"+ e) ;
            }

            finally{
              pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
            }
            return desc;
          }


  } //fin de la clase



