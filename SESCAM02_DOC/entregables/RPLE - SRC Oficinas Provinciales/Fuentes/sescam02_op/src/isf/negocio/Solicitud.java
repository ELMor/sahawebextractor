/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: Solicitud </p>
 * <p>Descripcion: Clase que guarda informaci�n de una solicitud de garantia tanto de pacientes en LESP como en LESPCEX</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 04/11/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class Solicitud {
    /**
      * Nombre de la Oficina a la cual se le va a realizar la solicitud
      */
     public String 	fOficina="SD";
     /**
       * Nombre del Centro al cual se le va a realizar la solicitud (tabla CentrSesCam)
       */
     public String 	fCentro="SD";
     /**
      *  pk Centro, valor del campo CENTRO si la solicitud la ha realizado un paciente de LESP o valor del campo CEGA si viene de un paciente en LEPCEX
      */
     public long        fCodCentro='0';
     /**
       * Cip del paciente que realiza la solicitud
       */
     public String 	fCip="SD";
     /**
       * Historia Clinica  del paciente que realiza la solicitud
       */
     public long        fHicl=0;
     /**
       * Estado en el que se encuentre la solicitud (1 - Solicitada, 2 - Aprobada, 3 - Denegada, 4 - Renunciada)
       */
     public int         fEstado=0;
     /**
       * Primer Apellido del paciente que realiza la solicitud
       */
     public String 	fApe1="SD";
     /**
      * Segundo Apellido del paciente que realiza la solicitud
      */
     public String 	fApe2="SD";
     /**
       * Nombre del paciente que realiza la solicitud
       */
     public String 	fNomb="SD";
     /**
       * Fecha de la ultima operacion realizada sobre  la solicitud
       */
     public String 	fFecha ="SD";
     /**
       * Numero de Orden (campo NOrden) si la solicitud se realiza sobre paciente de LESP o Numero de Cita (Campo NCita) si la solicitud se realiza sobre paciente de LESPCEX
       */
     public long        fNorden=0;

     /**
       * Distingue si se trata de una solicitud para un paciente en LESP (Q) o si la es solicitud para un paciente en LESPCEX (C)
       */

     public String      fCq="SD";
}