/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import conf.*;

/**
 * <p>Clase:  GaranTram</p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesGaranTram</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @sinece 30/10/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class GaranTram {
        /**
         * Nombre de la clase de persistencia asociada a la clase de Negocio Cargos
        */
        public static final String ID_TABLA="SES_GARANTRAM";

        // Devuelve un vector DE OBJETOS SesGaranTram(la clase de persistencia)
        // con los tramos que cumplan la clausula del where.

       /**
        * Devuelve un vector DE OBJETOS SesGaranTram(la clase de persistencia) con los tramos que cumplan la clausula del where.
        * @param String _where Clausula where para la consulta de objetos de clase SesGaranTram
        * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
        * @return Devuelve un vector DE OBJETOS SesGaranTram(la clase de persistencia) con los tramos de garantia que cumplan la clausula del where.
        * @autor Felix Alberto Bravo Hermoso (30/11/2002)
        */
        public Vector busquedaGaranTram(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);

                v = SesGaranTram.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar tramo de garantia:"+ e) ;
                }

                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }

        //Realiza la insercion del tramo de garantia.
        /**

         * Realiza la insercion del tramo de garantia.
         * @param SesGaranTram _sesTram Objeto de clase SesGaranTram (clase de persistencia asociada a tabla Ses_Garan_Tram)
         * @autor Felix Alberto Bravo Hermoso (30/11/2002)
         * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Garan_Tram
         */
        public void insertarGaranTram(SesGaranTram _sesTram) throws SQLException {
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                //System.out.println("Entra en insertarGaranTram");
                _sesTram.newId(Constantes.FICHERO_CONFIGURACION);
                _sesTram.insert(conexion);
                conexion.commit();
                }catch(SQLException e){
                        conexion.rollback();
                        System.out.println("Error al Insertar el tramo de garantia:"+ e);
                }catch(Exception ex){
                        conexion.rollback();
                        System.out.println("Error al Insertar el tramo de garantia:"+ ex);
                }
                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
        }


        //Realiza la modificacion del tramo de garantia.
        /**
         * Realiza la modificacion del tramo de garantia.
         * @param SesGaranTram _sesTram Objeto de clase SesGaranTram (clase de persistencia asociada a tabla Ses_Garan_Tram)
         * @autor Felix Alberto Bravo Hermoso (30/11/2002)
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Garan_Tram
         */

        public void modificarGaranTram(SesGaranTram _sesTram) throws SQLException  {
                Pool pool=null;
                Connection conexion = null;
                try{
                //System.out.println("Entra en modificarGaranTram");
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                _sesTram.update(conexion);
                conexion.commit();
                }catch(SQLException e){
                        System.out.println("Error al Modificar el tramo de garantia:"+ e);
                        conexion.rollback();
                }catch(Exception ex){
                        conexion.rollback();
                        System.out.println("Error al Modificar el tramo de garantia:"+ ex);
                }
                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
        }


        //Realiza la eliminacion del tramo de garantia.
        /**
         * Realiza la eliminacion del tramo de garantia..
         * @param SesGaranTram _sesTram Objeto de clase SesGaranTram (clase de persistencia asociada a tabla Ses_Garan_Tram)
         * @autor Felix Alberto Bravo Hermoso (30/11/2002)
         * @throws SQLException Exception producida por la operacion DELETE sobre tabla Ses_Garan_Tram
         */

        public void eliminarGaranTram(SesGaranTram _sesTram) throws SQLException,Exception {
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                SysPermRol spr = new SysPermRol();
                try{
                //System.out.println("Entra en metodo eliminarGaranTram");
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                _sesTram.delete(conexion);
                conexion.commit();
                //System.out.println("Delete hecho");
                }catch(SQLException e){
                        System.out.println("Error al Modificar el Permiso:"+ e);
                        conexion.rollback();
                }
                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
        }



} //fin de la clase

