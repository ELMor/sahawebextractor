/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: EntradaNoLE </p>
 * <p>Descripcion: Clase de negocio que contiene informaci�n de una entrada en NoLesp </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 28/05/2003
 * @author F�lix Alberto Bravo Hermoso
 * @version 1.0
 */
public class EntradaNoLE{


   /**
   * Numero de Registro Nolesp (garpk)
   */
   public long 		fNnolesp=0;

   public long 		fCentro=0;
   public long		fHicl=0;
   public String 	fCip="SD";
   public String 	fApellido1="SD";
   public String 	fApellido2="SD";
   public String 	fNombre="SD";
   public String	fDomicilio="SD";
   public String	fPoblacion="SD";
   public String	fProvincia="SD";
   public String  	fFechaNac=null;
   public String	fPrestacion="SD";
   public String        fTelefono="SD";
   public String 	fNSS="SD";
   public String        fResp_dni="SD";
   public String        fResp_apenom="SD";
   public long          fOficina=0;
   public String        fUsuario="SD";
   public String        fDesCentro="SD";
   public String        fFecha="SD";
   public String        fOfi_coordinador="SD";
   public String        fOfi_provincia="SD";
   public String        fOfi_nombre="SD";
   public String        fMotivo_rechazo="SD";

  public String getFApellido1() {
    return fApellido1;
  }
  public void setFApellido1(String fApellido1) {
    this.fApellido1 = fApellido1;
  }
  public String getFApellido2() {
    return fApellido2;
  }
  public void setFApellido2(String fApellido2) {
    this.fApellido2 = fApellido2;
  }
  public long getFCentro() {
    return fCentro;
  }
  public void setFCentro(long fCentro) {
    this.fCentro = fCentro;
  }
  public String getFCip() {
    return fCip;
  }
  public void setFCip(String fCip) {
    this.fCip = fCip;
  }
  public String getFDomicilio() {
    return fDomicilio;
  }
  public void setFDomicilio(String fDomicilio) {
    this.fDomicilio = fDomicilio;
  }
  public String getFFechaNac() {
    return fFechaNac;
  }
  public void setFFechaNac(String fFechaNac) {
    this.fFechaNac = fFechaNac;
  }
  public long getFHicl() {
    return fHicl;
  }
  public void setFHicl(long fHicl) {
    this.fHicl = fHicl;
  }
  public long getFNnolesp() {
    return fNnolesp;
  }
  public void setFNnolesp(long fNnolesp) {
    this.fNnolesp = fNnolesp;
  }
  public String getFNombre() {
    return fNombre;
  }
  public void setFNombre(String fNombre) {
    this.fNombre = fNombre;
  }
  public String getFNSS() {
    return fNSS;
  }
  public void setFNSS(String fNSS) {
    this.fNSS = fNSS;
  }
  public long getFOficina() {
    return fOficina;
  }
  public void setFOficina(long fOficina) {
    this.fOficina = fOficina;
  }
  public String getFPoblacion() {
    return fPoblacion;
  }
  public void setFPoblacion(String fPoblacion) {
    this.fPoblacion = fPoblacion;
  }
  public String getFPrestacion() {
    return fPrestacion;
  }
  public void setFPrestacion(String fPrestacion) {
    this.fPrestacion = fPrestacion;
  }
  public String getFProvincia() {
    return fProvincia;
  }
  public void setFProvincia(String fProvincia) {
    this.fProvincia = fProvincia;
  }
  public String getFResp_apenom() {
    return fResp_apenom;
  }
  public void setFResp_apenom(String fResp_apenom) {
    this.fResp_apenom = fResp_apenom;
  }
  public String getFResp_dni() {
    return fResp_dni;
  }
  public void setFResp_dni(String fResp_dni) {
    this.fResp_dni = fResp_dni;
  }
  public String getFTelefono() {
    return fTelefono;
  }
  public void setFTelefono(String fTelefono) {
    this.fTelefono = fTelefono;
  }
  public String getFUsuario() {
    return fUsuario;
  }
  public void setFUsuario(String fUsuario) {
    this.fUsuario = fUsuario;
  }
  public String getFDesCentro() {
    return fDesCentro;
  }
  public void setFDesCentro(String fDesCentro) {
    this.fDesCentro = fDesCentro;
  }
  public String getFFecha() {
    return fFecha;
  }
  public void setFFecha(String fFecha) {
    this.fFecha = fFecha;
  }
  public String getFOfi_coordinador() {
    return fOfi_coordinador;
  }
  public void setFOfi_coordinador(String fOfi_coordinador) {
    this.fOfi_coordinador = fOfi_coordinador;
  }
  public String getFOfi_nombre() {
    return fOfi_nombre;
  }
  public void setFOfi_nombre(String fOfi_nombre) {
    this.fOfi_nombre = fOfi_nombre;
  }
  public String getFOfi_provincia() {
    return fOfi_provincia;
  }
  public void setFOfi_provincia(String fOfi_provincia) {
    this.fOfi_provincia = fOfi_provincia;
  }
  public String getFMotivo_rechazo() {
    return fMotivo_rechazo;
  }
  public void setFMotivo_rechazo(String fMotivo_rechazo) {
    this.fMotivo_rechazo = fMotivo_rechazo;
  }

}
