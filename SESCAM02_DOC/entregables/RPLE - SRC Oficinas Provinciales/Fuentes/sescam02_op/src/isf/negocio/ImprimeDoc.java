/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import isf.util.Impresion;
import isf.util.Utilidades;
import isf.util.FrameTokenizer;
import isf.negocio.EntradaLEDet;
import beans.ConsultaEntradasLEDet;
import beans.ConsultaGaranTram;
import beans.ConsultaServMapeados;
import beans.ConsultaMapeosProc;
import beans.AccesoBean;


/**
 * <p>Clase: ImprimeDoc </p>
 * <p>Descripcion: Clase de negocio que se encarga de la impresion de los documentos.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 15/10/2002
 * @author Ivan Enrique Caneri
 */

public class ImprimeDoc extends HttpServlet {
  private final int iMaxLengthMot=6;
  private int Var_Evento1;


  /**
   * Metodo que convierte el parametro si es nulo a cadena vacia.
   * @param str ,String, cadena para forzar no nulo.
   * @return String, "" si es nulo y el mismo paramtro si no los es.
   * @since 25/11/2002
   * @author Felix Alberto Bravo Hermoso
   */
  private String ForzarNoNulo(String str){
    if (str==null){
      return "";
    }else{
      return str;
    }
  }

  /**
   * Traduccion de los campos en los tag del documento.
   * @param translate Hashtable,
   * @param pStrCentro
   * @param pStrNOrden
   * @param pStrCQ
   * @param pStrDocumento
   * @param piGarpk
   * @param piOperacion
   * @return si la carga de campos se ha efectuado correctamente o no
   * @since 15/10/2002
   * @author Ivan Enrique Caneri
   */
  private boolean cargaCampos(Hashtable translate, String pStrCentro, String pStrNOrden,String pStrCQ, String pStrDocumento,int piGarpk,int piOperacion){
    ConsultaEntradasLEDet cpb=new ConsultaEntradasLEDet();
    ConsultaGaranTram cGT=new ConsultaGaranTram();
    ConsultaServMapeados cSM= new ConsultaServMapeados();
    ConsultaMapeosProc cPM =  new ConsultaMapeosProc();
    EntradaLEDet entLEDet;
    GarTramImp garTI;
    ServMapeados srvmap;
    ProcedimientosMapeados proc = new ProcedimientosMapeados();
    Vector vp;
    int lenght_maxima;
    String cod_servicio="SD";
    String cod_seccion="SD";
    String cod_proc="SD";

    /*************** Entradas LEDet ****************************/
    cpb.setCQ(pStrCQ);
    cpb.setCentro(pStrCentro);
    cpb.setNOrden(pStrNOrden);
    java.util.Date oFecha=new java.util.Date();
    try{
    vp=cpb.resultado();
    }catch(Exception ex){return false;}
    if (vp!=null){
      Enumeration e = vp.elements();
      if (e.hasMoreElements()) {
        entLEDet=(EntradaLEDet)e.nextElement();
        translate.put("norden", String.valueOf(entLEDet.fNOrden));
        translate.put("centro_cod", String.valueOf(entLEDet.fCentro));
        translate.put("orden", String.valueOf(entLEDet.fOrden));
        translate.put("fecha_demora", entLEDet.getfFDemora());
        translate.put("tiempo_demora", String.valueOf(entLEDet.fTDemora));
        translate.put("fecha_inclusion", entLEDet.getfEntrada());
        translate.put("fecha_salida", entLEDet.getfSalida());
        translate.put("motivo_salida", String.valueOf(entLEDet.fMotivo));
        translate.put("hicl",String.valueOf(entLEDet.fHicl));
        translate.put("cip", entLEDet.getfCip());
        translate.put("apellido1", entLEDet.getfApe1());
        translate.put("apellido2", entLEDet.getfApe2());
        translate.put("nombre", entLEDet.getfNomb());
        String nomape1ape2 =entLEDet.getfNomb()+' '+entLEDet.getfApe1()+' '+entLEDet.getfApe2();
        translate.put("nombreape1ape2",nomape1ape2);
        translate.put("fec_nac", entLEDet.getfFnac());
        translate.put("sexo", entLEDet.getfSexo());
        translate.put("domicilio", entLEDet.getfDomi());
        translate.put("poblacion", entLEDet.getfPobl());
        translate.put("cod_pos", String.valueOf(entLEDet.fCpos));
        translate.put("provincia", entLEDet.getfProv());
        translate.put("telefono1", entLEDet.getfTel1());
        translate.put("telefono2", entLEDet.getfTel2());
        translate.put("telefono3", entLEDet.getfTel3());
        translate.put("financiador", entLEDet.getfFina());
        translate.put("garante",String.valueOf(entLEDet.fGara));
        cod_servicio = entLEDet.getfServ();
        translate.put("servicio", cod_servicio);
        cod_seccion = entLEDet.getfSecc();
        translate.put("seccion", cod_seccion);
        translate.put("area_clinica", entLEDet.getfAcli());
        translate.put("medico_codigo", String.valueOf(entLEDet.fCmed));
        translate.put("medico_nombre", entLEDet.getfApno_med());
        translate.put("diag1_codigo", entLEDet.getfCdi1());
        translate.put("diag1_descr", entLEDet.getfDdi1());
        translate.put("diag2_codigo", entLEDet.getfCdi2());
        translate.put("diag2_descr", entLEDet.getfDdi2());
        translate.put("proc1_codigo", entLEDet.getfCpr1());
        cod_proc=entLEDet.getfCpr1();
        if (entLEDet.getfProc().length() < 40) {
          lenght_maxima = entLEDet.getfProc().length();
        } else{
          lenght_maxima = 40;
        }
        translate.put("proc1_descr", entLEDet.getfProc().substring(0,lenght_maxima));
        translate.put("proc2_codigo", entLEDet.getfCpr2());
        translate.put("proc2_descr", entLEDet.getfDpr2());
        translate.put("tipo_cirugia", entLEDet.getfTcir());
        translate.put("tipo_anestesia", entLEDet.getfTane());
        translate.put("prioridad", entLEDet.getfPrio());
        translate.put("ofi_prov", "");
        translate.put("hoy_DD_nro", "");
        translate.put("hoy_MM_txt", "");
        translate.put("hoy_YYYY_nro", "");
        translate.put("centro_nombre",entLEDet.getfDescentro());
        translate.put("centro_nombre_largo",entLEDet.fDescentroLarga);
        //System.out.println(entLEDet.fDescentroLarga);
        translate.put("situacion_laboral", entLEDet.getfSlab());
        if ((entLEDet.getfSlab()).equals("N")){
          translate.put("pac_sit_lab_it","X");
          translate.put("pac_sit_lab_act","");
        }else{
          translate.put("pac_sit_lab_it","");
          translate.put("pac_sit_lab_act","X");
        }
      }else return false;
    }else return false;

    /*********************** GaranTram **********************/
    vp.clear();
    cGT.setGar_operacion(piOperacion);
    cGT.setGar_pk(piGarpk);
    try{
    vp=cGT.resultado();
    }catch(Exception ex){return false;}
    if (vp!=null){
      int iCnt=0;
      String strNom="";
      Enumeration e = vp.elements();
      if (e.hasMoreElements()) {
        garTI=(GarTramImp)e.nextElement();
        translate.put("monto_aprobacion", String.valueOf(garTI.fgar_monto));
        translate.put("resp_legal_dni", garTI.getfgar_repleg_dni());
        if (garTI.getfgar_repleg_apenom().length() < 65) {
          lenght_maxima = garTI.getfgar_repleg_apenom().length();
        }else{
          lenght_maxima = 65;
        }
        translate.put("resp_legal_nombre", garTI.getfgar_repleg_apenom().substring(0,lenght_maxima));
        translate.put("pac_nss",garTI.getfpac_nss());
        translate.put("ofi_coordinador",garTI.getfofi_coordinador());
        translate.put("ofi_prov", garTI.getfofi_prov());
        translate.put("fecha_de_hoy",garTI.getfgar_fechor());
        if(garTI.getfgar_cq().equals("Q")){
          translate.put("inscripcion_nro",String.valueOf(entLEDet.fCentro)+"/"+String.valueOf(entLEDet.fNOrden));
          translate.put("tipo_proc_QC","Quir�rgico");
        }else{
        translate.put("inscripcion_nro", String.valueOf(entLEDet.fNOrden));
        translate.put("tipo_proc_QC","Ambulatorio");
        }
        try {
          translate.put("hoy_DD_nro", garTI.getfgar_fechor().substring(0,2));
          translate.put("hoy_MM_txt", Utilidades.nombreMes(Integer.parseInt(garTI.getfgar_fechor().substring(3,5))));
          translate.put("hoy_YYYY_nro",garTI.getfgar_fechor().substring(6));
        }
        catch (NumberFormatException ex) {
          translate.put("hoy_DD_nro", "");
          translate.put("hoy_MM_txt", "");
          translate.put("hoy_YYYY_nro","");
        }
        StringTokenizer st= new StringTokenizer(garTI.getfgar_motivo_rech(),"\n",false);

        while (st.hasMoreElements()) {
          FrameTokenizer ft= new  FrameTokenizer((String)st.nextElement(),120);
          while (ft.hasMoreElements()) {
            iCnt++;
            strNom="motivo_denegacion"+Integer.toString(iCnt);
            translate.put(strNom,(String)ft.nextElement());
          }
        }
        while (iCnt<iMaxLengthMot){
            iCnt++;
            strNom="motivo_denegacion"+Integer.toString(iCnt);
            translate.put(strNom," ");
        }
      }else return false;
    }else return false;

    /************************ ServMapeados ***********/
    vp.clear();
    long centro = new Long(pStrCentro).longValue();
    cSM.setCentro(centro);
    cSM.setCod_mapeo(cod_seccion);
    try{
    vp=cSM.ServiciosMapeados();
    }catch(Exception ex){return false;}
    if (vp.size()>0){
      Enumeration e = vp.elements();
      if (e.hasMoreElements()) {
        srvmap=(ServMapeados)e.nextElement();
        translate.put("cod_serv_map", srvmap.CodServSESCAM);
        translate.put("desc_serv_map", srvmap.DescServSESCAM);
      }else return false;
    }else{
      translate.put("cod_serv_map", " ");
      translate.put("desc_serv_map", " ");
      //return false;
    }

    /************************ ProcMapeados ************/
    vp.clear();
    cPM.setCentro(centro);
    cPM.setCodigoMapeo(cod_proc);
    if (pStrCQ.equals("Q")) cPM.setCodtipoMapeo("Q");
    else cPM.setCodtipoMapeo(pStrCQ);
    try{
      vp=cPM.MapeosProcedimientos();
    }catch(Exception ex){return false;}
    if (vp.size()>0){
     Enumeration e = vp.elements();
     if (e.hasMoreElements()){
       proc =(ProcedimientosMapeados)e.nextElement();
       translate.put("cod_proc_map",proc.CodProcSESCAM);
       translate.put("desc_proc_map",proc.ProcDesc);
     }else return false;
    }else {
      translate.put("cod_proc_map"," ");
      translate.put("desc_proc_map"," ");
//    return false;
    }

   /***********************************************/
   // Toma informaci�n de la solicitud.
    if ((piOperacion==3)||(piOperacion==4)){
      vp.clear();
      cGT.setGar_operacion(1);
      cGT.setGar_pk(piGarpk);
      try{
        vp=cGT.resultado();
        }catch(Exception ex){return false;}
        if (vp.size()>0){
          Enumeration e = vp.elements();
          if (e.hasMoreElements()) {
            garTI=(GarTramImp)e.nextElement();
            if (garTI.getfgar_fechor()!=null) translate.put("fecha_acred",garTI.getfgar_fechor());
            else translate.put("fecha_acred"," ");
          }else{
            translate.put("fecha_acred"," ");
          };
        }else return false;
    }
    /*********************************************/
    // Toma informaci�n de la aprobaci�n.
    if (piOperacion==4){
      vp.clear();
      cGT.setGar_operacion(2);
      cGT.setGar_pk(piGarpk);
      try{
        vp=cGT.resultado();
        }catch(Exception ex){return false;}
        if (vp!=null){
          Enumeration e = vp.elements();
          if (e.hasMoreElements()) {
            garTI=(GarTramImp)e.nextElement();
            translate.remove("monto_aprobado");
            translate.put("monto_aprobado",garTI.getfgar_fechor());
          }else{
            translate.remove("monto_aprobado");
            translate.put("monto_aprobado"," ");
          };
        }else return false;
    }

/**************************************/

  return true;
  }

  /**
   *
   * @throws ServletException
   */
  public void init() throws ServletException {
  }
  //Process the HTTP Get request
  /**
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    ServletOutputStream outStr = response.getOutputStream();
    String strCentro,strNOrden,strDocumento,strDocName,strTipo,strCQ,strReimpre;
    boolean lb_lanzar_evento = false;
    int iGarpk,iOperacion;
    strDocumento = request.getParameter("documento");
    strCentro = request.getParameter("centro");
    strNOrden = request.getParameter("norden");
    strCQ = request.getParameter("cq");
    strReimpre = request.getParameter("impresion");

    request.getSession().getServletContext();


    if (strReimpre.equals("1"))  {
       lb_lanzar_evento = true;
    }
    try {
      iGarpk=Integer.parseInt(request.getParameter("garpk"));
      iOperacion=Integer.parseInt(request.getParameter("operacion"));
    }
    catch (NumberFormatException ex) {
      iGarpk=0;
      iOperacion=0;
    }
    String strUsuario = request.getParameter("usuario");
    Impresion impDoc= new Impresion();
    //Control de acceso directo
    HttpSession session = request.getSession();
    AccesoBean accesoBeanId= (AccesoBean) session.getAttribute("accesoBeanId");

    if ((accesoBeanId == null)||(!accesoBeanId.accesoA("acceso_LEQ"))){
      outStr.println("<html>");
      outStr.println("<head>");
      outStr.println("<title>Acceso denegado</title>");
      outStr.println("</head>");
      outStr.println("<body>");
      outStr.println("<script language='javascript'>");
      outStr.println("	document.location = 'cierra.html'");
      outStr.println("</script>");
      outStr.println("<body>");
      outStr.println("</html>");
      return;
    }
    // Carga la Hashtable con el valor de los campos
    Hashtable translate = new Hashtable();
    cargaCampos(translate,strCentro, strNOrden,strCQ,strDocumento,iGarpk,iOperacion);
    Properties docProps = new Properties();
    InputStream is = getClass().getResourceAsStream ("/Impresion.properties");
    docProps.load(is);
    strTipo=docProps.getProperty(strDocumento+"_TIPO");
    strDocName=docProps.getProperty(strDocumento+"_DOC");
    if (strTipo.equals("RTF")){
      response.setHeader("Content-Disposition","inline;filename="+strDocName);
      response.setContentType("application/rtf");
    }
    else if (strTipo.equals("XLS")){
      response.setContentType("application/vnd.ms-excel");
      response.setHeader("Content-Disposition","inline;filename="+strDocName);
    }
    else if (strTipo.equals("PDF")){
      response.setHeader("Content-Disposition","inline;charset='PDF';filename="+strDocName);
      response.setContentType("application/pdf");
    }
    else{
      response.setHeader("Content-Disposition","inline;filename="+strDocName);
      response.setContentType("application/msword");
    }
    try{
      impDoc.imprimir(strDocumento,translate,outStr);

      //Registro de impresi�n de documentos
      if (lb_lanzar_evento == true) {

         NegEventoTram evento = accesoBeanId.getNegEventoTramInstance(14);
         if (evento != null) {

           evento.setGarPk(iGarpk);
           evento.setGarOperacion(iOperacion);
           evento.addVar(57,strDocumento);
           evento.addVar(58,strUsuario);
           evento.addVar(59,strCentro);
           evento.addVar(60,strNOrden);

           evento.guardar();
         }
      }
    }catch(IOException ioe) {
        throw ioe;
     }
     catch(Exception sqle) {
      System.out.println("Error al grabar el registro de impresion de documentos"+
                                "Documento:"+strDocumento+" Usuario:"+strUsuario+" Centro:"+strCentro+" Norden:"+strNOrden);
     }
  }
  //Clean up resources
  public void destroy() {
  }
}

