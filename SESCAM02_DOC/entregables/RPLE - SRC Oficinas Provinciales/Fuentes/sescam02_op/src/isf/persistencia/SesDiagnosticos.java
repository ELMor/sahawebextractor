//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesDiagnosticos
// Table: SES_DIAGNOSTICOS
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_DIAGNOSTICOS
 * Columns for the table:
 * 	(diag_pk - LONG), 
 * 	(diag_cod - STRING), 
 * 	(diag_desc - STRING), 
 * Primary columns for the table:
 * 	(diag_pk - LONG) 
 */
public class SesDiagnosticos
{
	// Properties
	private long diagPk;
	private boolean diagPkNull=true;
	private boolean diagPkModified=false;
	private String diagCod;
	private boolean diagCodNull=true;
	private boolean diagCodModified=false;
	private String diagDesc;
	private boolean diagDescNull=true;
	private boolean diagDescModified=false;
	// Access Method
	/** DiagPk
	 * Get the value of the property DiagPk.
	 * The column for the database is 'DIAG_PK'DiagPk.
	 */
	public long getDiagPk() {
		return diagPk;
	}
	/** DiagPk
	 * Set the value of the property DiagPk.
	 * The column for the database is 'DIAG_PK'DiagPk.
	 */
	public void setDiagPk(long _diagPk) {
		this.diagPk=_diagPk;
		this.diagPkModified=true;
		this.diagPkNull=false;
	}
	/** DiagPk
	 * Set Null the value of the property DiagPk.
	 * The column for the database is 'DIAG_PK'DiagPk.
	 */
	public void setNullDiagPk() {
		this.diagPk=0;
		this.diagPkModified=true;
		this.diagPkNull=true;
	}
	/** DiagPk
	 * Sumatory of value of the property DiagPk.
	 * The column for the database is 'DIAG_PK'DiagPk.
	 */
	static public double sumDiagPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"DIAG_PK");
	}
	/** DiagPk
	 * Indicates if the value of the property DiagPk is null or not.
	 * The column for the database is 'DIAG_PK'DiagPk.
	 */
	public boolean isNullDiagPk() {
		return diagPkNull;
	}
	/** DiagCod
	 * Get the value of the property DiagCod.
	 * The column for the database is 'DIAG_COD'DiagCod.
	 */
	public String getDiagCod() {
		if (diagCod==null) return "";
		if (diagCod.compareTo("null")==0) return "";
		return diagCod;
	}
	/** DiagCod
	 * Set the value of the property DiagCod.
	 * The column for the database is 'DIAG_COD'DiagCod.
	 */
	public void setDiagCod(String _diagCod) {
		this.diagCod=_diagCod;
		this.diagCodModified=true;
		this.diagCodNull=false;
	}
	/** DiagCod
	 * Set Null the value of the property DiagCod.
	 * The column for the database is 'DIAG_COD'DiagCod.
	 */
	public void setNullDiagCod() {
		this.diagCod=null;
		this.diagCodModified=true;
		this.diagCodNull=true;
	}
	/** DiagCod
	 * Indicates if the value of the property DiagCod is null or not.
	 * The column for the database is 'DIAG_COD'DiagCod.
	 */
	public boolean isNullDiagCod() {
		return diagCodNull;
	}
	/** DiagDesc
	 * Get the value of the property DiagDesc.
	 * The column for the database is 'DIAG_DESC'DiagDesc.
	 */
	public String getDiagDesc() {
		if (diagDesc==null) return "";
		if (diagDesc.compareTo("null")==0) return "";
		return diagDesc;
	}
	/** DiagDesc
	 * Set the value of the property DiagDesc.
	 * The column for the database is 'DIAG_DESC'DiagDesc.
	 */
	public void setDiagDesc(String _diagDesc) {
		this.diagDesc=_diagDesc;
		this.diagDescModified=true;
		this.diagDescNull=false;
	}
	/** DiagDesc
	 * Set Null the value of the property DiagDesc.
	 * The column for the database is 'DIAG_DESC'DiagDesc.
	 */
	public void setNullDiagDesc() {
		this.diagDesc=null;
		this.diagDescModified=true;
		this.diagDescNull=true;
	}
	/** DiagDesc
	 * Indicates if the value of the property DiagDesc is null or not.
	 * The column for the database is 'DIAG_DESC'DiagDesc.
	 */
	public boolean isNullDiagDesc() {
		return diagDescNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!diagPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DIAG_PK");
			ls_values.append((lb_FirstTime?"":",")+diagPk);
			lb_FirstTime = false;
		}
		if (!diagCodNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DIAG_COD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(diagCod)+"'");
			lb_FirstTime = false;
		}
		if (!diagDescNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DIAG_DESC");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(diagDesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_DIAGNOSTICOS ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (diagPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DIAG_PK");
			if(diagPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+diagPk);
			lb_FirstTime = false;
		}
		if (diagCodModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DIAG_COD");
			if(diagCodNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(diagCod)+"'");
			lb_FirstTime = false;
		}
		if (diagDescModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DIAG_DESC");
			if(diagDescNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(diagDesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("DIAG_PK");
			ls_where.append("="+diagPk);
		} else {
			ls_where.append(" AND "+"DIAG_PK");
			ls_where.append("="+diagPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_DIAGNOSTICOS SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("DIAG_PK");
			ls_where.append("="+diagPk);
		} else {
			ls_where.append(" AND "+"DIAG_PK");
			ls_where.append("="+diagPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_DIAGNOSTICOS WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _diagPk
	 * @return SesDiagnosticos - Retrieved object
	 */
	static public SesDiagnosticos read(Connection _connection,long _diagPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesDiagnosticos ls_SesDiagnosticos=new SesDiagnosticos();
		ls_where="DIAG_PK = " + _diagPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_DIAGNOSTICOS WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesDiagnosticos.loadResultSet(ls_rs);
		} else {
			ls_SesDiagnosticos=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesDiagnosticos;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		diagPk=_rs.getLong("DIAG_PK");
		diagPkNull=_rs.wasNull();
		diagCod=_rs.getString("DIAG_COD");
		diagCodNull=_rs.wasNull();
		diagDesc=_rs.getString("DIAG_DESC");
		diagDescNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_DIAGNOSTICOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesDiagnosticos mySesDiagnosticos;
		Vector mySesDiagnosticoses = new Vector();
		while (ls_rs.next()) {
			mySesDiagnosticos = new SesDiagnosticos();
			mySesDiagnosticos.loadResultSet (ls_rs);
			mySesDiagnosticoses.addElement(mySesDiagnosticos);
		}
		ls_rs.close();
		myStatement.close();
		return mySesDiagnosticoses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_DIAGNOSTICOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_DIAGNOSTICOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_DIAGNOSTICOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setDiagPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
