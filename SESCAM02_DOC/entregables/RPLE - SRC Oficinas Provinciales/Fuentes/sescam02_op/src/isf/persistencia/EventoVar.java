//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: EventoVar
// Table: EVENTO_VAR
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:EVENTO_VAR
 * Columns for the table:
 * 	(evto_var_pk - LONG), 
 * 	(evto_pk - LONG), 
 * 	(tpoevto_var_pk - LONG), 
 * 	(evto_var_valor - STRING), 
 * Primary columns for the table:
 * 	(evto_var_pk - LONG) 
 */
public class EventoVar
{
	// Properties
	private long evtoVarPk;
	private boolean evtoVarPkNull=true;
	private boolean evtoVarPkModified=false;
	private long evtoPk;
	private boolean evtoPkNull=true;
	private boolean evtoPkModified=false;
	private long tpoevtoVarPk;
	private boolean tpoevtoVarPkNull=true;
	private boolean tpoevtoVarPkModified=false;
	private String evtoVarValor;
	private boolean evtoVarValorNull=true;
	private boolean evtoVarValorModified=false;
	// Access Method
	/** EvtoVarPk
	 * Get the value of the property EvtoVarPk.
	 * The column for the database is 'EVTO_VAR_PK'EvtoVarPk.
	 */
	public long getEvtoVarPk() {
		return evtoVarPk;
	}
	/** EvtoVarPk
	 * Set the value of the property EvtoVarPk.
	 * The column for the database is 'EVTO_VAR_PK'EvtoVarPk.
	 */
	public void setEvtoVarPk(long _evtoVarPk) {
		this.evtoVarPk=_evtoVarPk;
		this.evtoVarPkModified=true;
		this.evtoVarPkNull=false;
	}
	/** EvtoVarPk
	 * Set Null the value of the property EvtoVarPk.
	 * The column for the database is 'EVTO_VAR_PK'EvtoVarPk.
	 */
	public void setNullEvtoVarPk() {
		this.evtoVarPk=0;
		this.evtoVarPkModified=true;
		this.evtoVarPkNull=true;
	}
	/** EvtoVarPk
	 * Sumatory of value of the property EvtoVarPk.
	 * The column for the database is 'EVTO_VAR_PK'EvtoVarPk.
	 */
	static public double sumEvtoVarPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"EVTO_VAR_PK");
	}
	/** EvtoVarPk
	 * Indicates if the value of the property EvtoVarPk is null or not.
	 * The column for the database is 'EVTO_VAR_PK'EvtoVarPk.
	 */
	public boolean isNullEvtoVarPk() {
		return evtoVarPkNull;
	}
	/** EvtoPk
	 * Get the value of the property EvtoPk.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	public long getEvtoPk() {
		return evtoPk;
	}
	/** EvtoPk
	 * Set the value of the property EvtoPk.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	public void setEvtoPk(long _evtoPk) {
		this.evtoPk=_evtoPk;
		this.evtoPkModified=true;
		this.evtoPkNull=false;
	}
	/** EvtoPk
	 * Set Null the value of the property EvtoPk.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	public void setNullEvtoPk() {
		this.evtoPk=0;
		this.evtoPkModified=true;
		this.evtoPkNull=true;
	}
	/** EvtoPk
	 * Sumatory of value of the property EvtoPk.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	static public double sumEvtoPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"EVTO_PK");
	}
	/** EvtoPk
	 * Indicates if the value of the property EvtoPk is null or not.
	 * The column for the database is 'EVTO_PK'EvtoPk.
	 */
	public boolean isNullEvtoPk() {
		return evtoPkNull;
	}
	/** TpoevtoVarPk
	 * Get the value of the property TpoevtoVarPk.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	public long getTpoevtoVarPk() {
		return tpoevtoVarPk;
	}
	/** TpoevtoVarPk
	 * Set the value of the property TpoevtoVarPk.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	public void setTpoevtoVarPk(long _tpoevtoVarPk) {
		this.tpoevtoVarPk=_tpoevtoVarPk;
		this.tpoevtoVarPkModified=true;
		this.tpoevtoVarPkNull=false;
	}
	/** TpoevtoVarPk
	 * Set Null the value of the property TpoevtoVarPk.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	public void setNullTpoevtoVarPk() {
		this.tpoevtoVarPk=0;
		this.tpoevtoVarPkModified=true;
		this.tpoevtoVarPkNull=true;
	}
	/** TpoevtoVarPk
	 * Sumatory of value of the property TpoevtoVarPk.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	static public double sumTpoevtoVarPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"TPOEVTO_VAR_PK");
	}
	/** TpoevtoVarPk
	 * Indicates if the value of the property TpoevtoVarPk is null or not.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	public boolean isNullTpoevtoVarPk() {
		return tpoevtoVarPkNull;
	}
	/** EvtoVarValor
	 * Get the value of the property EvtoVarValor.
	 * The column for the database is 'EVTO_VAR_VALOR'EvtoVarValor.
	 */
	public String getEvtoVarValor() {
		if (evtoVarValor==null) return "";
		if (evtoVarValor.compareTo("null")==0) return "";
		return evtoVarValor;
	}
	/** EvtoVarValor
	 * Set the value of the property EvtoVarValor.
	 * The column for the database is 'EVTO_VAR_VALOR'EvtoVarValor.
	 */
	public void setEvtoVarValor(String _evtoVarValor) {
		this.evtoVarValor=_evtoVarValor;
		this.evtoVarValorModified=true;
		this.evtoVarValorNull=false;
	}
	/** EvtoVarValor
	 * Set Null the value of the property EvtoVarValor.
	 * The column for the database is 'EVTO_VAR_VALOR'EvtoVarValor.
	 */
	public void setNullEvtoVarValor() {
		this.evtoVarValor=null;
		this.evtoVarValorModified=true;
		this.evtoVarValorNull=true;
	}
	/** EvtoVarValor
	 * Indicates if the value of the property EvtoVarValor is null or not.
	 * The column for the database is 'EVTO_VAR_VALOR'EvtoVarValor.
	 */
	public boolean isNullEvtoVarValor() {
		return evtoVarValorNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!evtoVarPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"EVTO_VAR_PK");
			ls_values.append((lb_FirstTime?"":",")+evtoVarPk);
			lb_FirstTime = false;
		}
		if (!evtoPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"EVTO_PK");
			ls_values.append((lb_FirstTime?"":",")+evtoPk);
			lb_FirstTime = false;
		}
		if (!tpoevtoVarPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"TPOEVTO_VAR_PK");
			ls_values.append((lb_FirstTime?"":",")+tpoevtoVarPk);
			lb_FirstTime = false;
		}
		if (!evtoVarValorNull) {
			ls_columns.append((lb_FirstTime?"":",")+"EVTO_VAR_VALOR");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(evtoVarValor)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO EVENTO_VAR ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (evtoVarPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("EVTO_VAR_PK");
			if(evtoVarPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+evtoVarPk);
			lb_FirstTime = false;
		}
		if (evtoPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("EVTO_PK");
			if(evtoPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+evtoPk);
			lb_FirstTime = false;
		}
		if (tpoevtoVarPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("TPOEVTO_VAR_PK");
			if(tpoevtoVarPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+tpoevtoVarPk);
			lb_FirstTime = false;
		}
		if (evtoVarValorModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("EVTO_VAR_VALOR");
			if(evtoVarValorNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(evtoVarValor)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("EVTO_VAR_PK");
			ls_where.append("="+evtoVarPk);
		} else {
			ls_where.append(" AND "+"EVTO_VAR_PK");
			ls_where.append("="+evtoVarPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE EVENTO_VAR SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("EVTO_VAR_PK");
			ls_where.append("="+evtoVarPk);
		} else {
			ls_where.append(" AND "+"EVTO_VAR_PK");
			ls_where.append("="+evtoVarPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE EVENTO_VAR WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _evtoVarPk
	 * @return EventoVar - Retrieved object
	 */
	static public EventoVar read(Connection _connection,long _evtoVarPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		EventoVar ls_EventoVar=new EventoVar();
		ls_where="EVTO_VAR_PK = " + _evtoVarPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM EVENTO_VAR WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_EventoVar.loadResultSet(ls_rs);
		} else {
			ls_EventoVar=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_EventoVar;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		evtoVarPk=_rs.getLong("EVTO_VAR_PK");
		evtoVarPkNull=_rs.wasNull();
		evtoPk=_rs.getLong("EVTO_PK");
		evtoPkNull=_rs.wasNull();
		tpoevtoVarPk=_rs.getLong("TPOEVTO_VAR_PK");
		tpoevtoVarPkNull=_rs.wasNull();
		evtoVarValor=_rs.getString("EVTO_VAR_VALOR");
		evtoVarValorNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM EVENTO_VAR ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		EventoVar myEventoVar;
		Vector myEventoVares = new Vector();
		while (ls_rs.next()) {
			myEventoVar = new EventoVar();
			myEventoVar.loadResultSet (ls_rs);
			myEventoVares.addElement(myEventoVar);
		}
		ls_rs.close();
		myStatement.close();
		return myEventoVares;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM EVENTO_VAR ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM EVENTO_VAR ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM EVENTO_VAR ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setEvtoVarPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
