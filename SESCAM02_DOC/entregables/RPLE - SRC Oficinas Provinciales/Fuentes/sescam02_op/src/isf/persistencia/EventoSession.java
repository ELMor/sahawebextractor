//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: EventoSession
// Table: EVENTO_SESSION
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:EVENTO_SESSION
 * Columns for the table:
 * 	(sess_acc_cod - LONG), 
 * 	(sess_pk - LONG), 
 * 	(syslogin - STRING), 
 * 	(oficina_pk - LONG), 
 * 	(sess_fecha_inicio - DATE), 
 * 	(sess_id - STRING), 
 * 	(sess_srv_rmt - STRING), 
 * Primary columns for the table:
 * 	(sess_pk - LONG) 
 */
public class EventoSession
{
	// Properties
	private long sessAccCod;
	private boolean sessAccCodNull=false;
	private boolean sessAccCodModified=false;
	private long sessPk;
	private boolean sessPkNull=false;
	private boolean sessPkModified=false;
	private String syslogin;
	private boolean sysloginNull=false;
	private boolean sysloginModified=false;
	private long oficinaPk;
	private boolean oficinaPkNull=false;
	private boolean oficinaPkModified=false;
	private java.sql.Date sessFechaInicio;
	private boolean sessFechaInicioNull=false;
	private boolean sessFechaInicioModified=false;
	private String sessId;
	private boolean sessIdNull=false;
	private boolean sessIdModified=false;
	private String sessSrvRmt;
	private boolean sessSrvRmtNull=false;
	private boolean sessSrvRmtModified=false;
	// Access Method
	/** SessAccCod
	 * Get the value of the property SessAccCod.
	 * The column for the database is 'SESS_ACC_COD'SessAccCod.
	 */
	public long getSessAccCod() {
		return sessAccCod;
	}
	/** SessAccCod
	 * Set the value of the property SessAccCod.
	 * The column for the database is 'SESS_ACC_COD'SessAccCod.
	 */
	public void setSessAccCod(long _sessAccCod) {
		this.sessAccCod=_sessAccCod;
		this.sessAccCodModified=true;
		this.sessAccCodNull=false;
	}
	/** SessAccCod
	 * Set Null the value of the property SessAccCod.
	 * The column for the database is 'SESS_ACC_COD'SessAccCod.
	 */
	public void setNullSessAccCod() {
		this.sessAccCod=0;
		this.sessAccCodModified=true;
		this.sessAccCodNull=true;
	}
	/** SessAccCod
	 * Sumatory of value of the property SessAccCod.
	 * The column for the database is 'SESS_ACC_COD'SessAccCod.
	 */
	static public double sumSessAccCod(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SESS_ACC_COD");
	}
	/** SessPk
	 * Get the value of the property SessPk.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	public long getSessPk() {
		return sessPk;
	}
	/** SessPk
	 * Set the value of the property SessPk.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	public void setSessPk(long _sessPk) {
		this.sessPk=_sessPk;
		this.sessPkModified=true;
		this.sessPkNull=false;
	}
	/** SessPk
	 * Set Null the value of the property SessPk.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	public void setNullSessPk() {
		this.sessPk=0;
		this.sessPkModified=true;
		this.sessPkNull=true;
	}
	/** SessPk
	 * Sumatory of value of the property SessPk.
	 * The column for the database is 'SESS_PK'SessPk.
	 */
	static public double sumSessPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SESS_PK");
	}
	/** Syslogin
	 * Get the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public String getSyslogin() {
		if (syslogin==null) return "";
		if (syslogin.compareTo("null")==0) return "";
		return syslogin;
	}
	/** Syslogin
	 * Set the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setSyslogin(String _syslogin) {
		this.syslogin=_syslogin;
		this.sysloginModified=true;
		this.sysloginNull=false;
	}
	/** Syslogin
	 * Set Null the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setNullSyslogin() {
		this.syslogin=null;
		this.sysloginModified=true;
		this.sysloginNull=true;
	}
	/** OficinaPk
	 * Get the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public long getOficinaPk() {
		return oficinaPk;
	}
	/** OficinaPk
	 * Set the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public void setOficinaPk(long _oficinaPk) {
		this.oficinaPk=_oficinaPk;
		this.oficinaPkModified=true;
		this.oficinaPkNull=false;
	}
	/** OficinaPk
	 * Set Null the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public void setNullOficinaPk() {
		this.oficinaPk=0;
		this.oficinaPkModified=true;
		this.oficinaPkNull=true;
	}
	/** OficinaPk
	 * Sumatory of value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	static public double sumOficinaPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"OFICINA_PK");
	}
	/** SessFechaInicio
	 * Get the value of the property SessFechaInicio.
	 * The column for the database is 'SESS_FECHA_INICIO'SessFechaInicio.
	 */
	public java.sql.Date getSessFechaInicio() {
		return sessFechaInicio;
	}
	/** SessFechaInicio
	 * Set the value of the property SessFechaInicio.
	 * The column for the database is 'SESS_FECHA_INICIO'SessFechaInicio.
	 */
	public void setSessFechaInicio(java.sql.Date _sessFechaInicio) {
		this.sessFechaInicio=_sessFechaInicio;
		this.sessFechaInicioModified=true;
		this.sessFechaInicioNull=false;
	}
	/** SessFechaInicio
	 * Set Null the value of the property SessFechaInicio.
	 * The column for the database is 'SESS_FECHA_INICIO'SessFechaInicio.
	 */
	public void setNullSessFechaInicio() {
		this.sessFechaInicio=null;
		this.sessFechaInicioModified=true;
		this.sessFechaInicioNull=true;
	}
	/** SessId
	 * Get the value of the property SessId.
	 * The column for the database is 'SESS_ID'SessId.
	 */
	public String getSessId() {
		if (sessId==null) return "";
		if (sessId.compareTo("null")==0) return "";
		return sessId;
	}
	/** SessId
	 * Set the value of the property SessId.
	 * The column for the database is 'SESS_ID'SessId.
	 */
	public void setSessId(String _sessId) {
		this.sessId=_sessId;
		this.sessIdModified=true;
		this.sessIdNull=false;
	}
	/** SessId
	 * Set Null the value of the property SessId.
	 * The column for the database is 'SESS_ID'SessId.
	 */
	public void setNullSessId() {
		this.sessId=null;
		this.sessIdModified=true;
		this.sessIdNull=true;
	}
	/** SessSrvRmt
	 * Get the value of the property SessSrvRmt.
	 * The column for the database is 'SESS_SRV_RMT'SessSrvRmt.
	 */
	public String getSessSrvRmt() {
		if (sessSrvRmt==null) return "";
		if (sessSrvRmt.compareTo("null")==0) return "";
		return sessSrvRmt;
	}
	/** SessSrvRmt
	 * Set the value of the property SessSrvRmt.
	 * The column for the database is 'SESS_SRV_RMT'SessSrvRmt.
	 */
	public void setSessSrvRmt(String _sessSrvRmt) {
		this.sessSrvRmt=_sessSrvRmt;
		this.sessSrvRmtModified=true;
		this.sessSrvRmtNull=false;
	}
	/** SessSrvRmt
	 * Set Null the value of the property SessSrvRmt.
	 * The column for the database is 'SESS_SRV_RMT'SessSrvRmt.
	 */
	public void setNullSessSrvRmt() {
		this.sessSrvRmt=null;
		this.sessSrvRmtModified=true;
		this.sessSrvRmtNull=true;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		String ls_columns = new String();
		String ls_values  = new String();
		boolean lb_FirstTime = true;
		if (sessAccCodModified) {
			if (lb_FirstTime ) { 
				ls_columns+="SESS_ACC_COD";
				if(sessAccCodNull) ls_values+="null";
				else ls_values+=sessAccCod;
			} else {
				ls_columns+=","+"SESS_ACC_COD";
				if(sessAccCodNull) ls_values+=",null";
				else ls_values+=","+sessAccCod;
			}
			lb_FirstTime = false;
		}
		if (sessPkModified) {
			if (lb_FirstTime ) { 
				ls_columns+="SESS_PK";
				if(sessPkNull) ls_values+="null";
				else ls_values+=sessPk;
			} else {
				ls_columns+=","+"SESS_PK";
				if(sessPkNull) ls_values+=",null";
				else ls_values+=","+sessPk;
			}
			lb_FirstTime = false;
		}
		if (sysloginModified) {
			if (lb_FirstTime ) { 
				ls_columns+="SYSLOGIN";
				if(sysloginNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(syslogin)+"'";
			} else {
				ls_columns+=","+"SYSLOGIN";
				if(sysloginNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(syslogin)+"'";
			}
			lb_FirstTime = false;
		}
		if (oficinaPkModified) {
			if (lb_FirstTime ) { 
				ls_columns+="OFICINA_PK";
				if(oficinaPkNull) ls_values+="null";
				else ls_values+=oficinaPk;
			} else {
				ls_columns+=","+"OFICINA_PK";
				if(oficinaPkNull) ls_values+=",null";
				else ls_values+=","+oficinaPk;
			}
			lb_FirstTime = false;
		}
		if (sessFechaInicioModified) {
			if (lb_FirstTime ) { 
				ls_columns+="SESS_FECHA_INICIO";
				if(sessFechaInicioNull) ls_values+="null";
				else ls_values+= "'" + sessFechaInicio.toString() + "'" ; 
			} else {
				ls_columns+=","+"SESS_FECHA_INICIO";
				if(sessFechaInicioNull) ls_values+=",null";
				else ls_values+= ",'"+sessFechaInicio.toString() + "'" ; 
			}
			lb_FirstTime = false;
		}
		if (sessIdModified) {
			if (lb_FirstTime ) { 
				ls_columns+="SESS_ID";
				if(sessIdNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(sessId)+"'";
			} else {
				ls_columns+=","+"SESS_ID";
				if(sessIdNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(sessId)+"'";
			}
			lb_FirstTime = false;
		}
		if (sessSrvRmtModified) {
			if (lb_FirstTime ) { 
				ls_columns+="SESS_SRV_RMT";
				if(sessSrvRmtNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(sessSrvRmt)+"'";
			} else {
				ls_columns+=","+"SESS_SRV_RMT";
				if(sessSrvRmtNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(sessSrvRmt)+"'";
			}
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO EVENTO_SESSION ("+ls_columns+") VALUES ("+ls_values+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		String ls_setValues = new String();
		String ls_where  = new String();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (sessAccCodModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="SESS_ACC_COD";
				if(sessAccCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+sessAccCod;
			} else {
				ls_setValues+=","+"SESS_ACC_COD";
				if(sessAccCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+sessAccCod;
			}
			lb_FirstTime = false;
		}
		if (sessPkModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="SESS_PK";
				if(sessPkNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+sessPk;
			} else {
				ls_setValues+=","+"SESS_PK";
				if(sessPkNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+sessPk;
			}
			lb_FirstTime = false;
		}
		if (sysloginModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="SYSLOGIN";
				if(sysloginNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(syslogin)+"'";
			} else {
				ls_setValues+=","+"SYSLOGIN";
				if(sysloginNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(syslogin)+"'";
			}
			lb_FirstTime = false;
		}
		if (oficinaPkModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="OFICINA_PK";
				if(oficinaPkNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+oficinaPk;
			} else {
				ls_setValues+=","+"OFICINA_PK";
				if(oficinaPkNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+oficinaPk;
			}
			lb_FirstTime = false;
		}
		if (sessFechaInicioModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="SESS_FECHA_INICIO";
				if(sessFechaInicioNull) {
					ls_setValues+="=null";
				} else ls_setValues+= "='" + sessFechaInicio.toString() + "'"; 
			} else {
				ls_setValues+=","+"SESS_FECHA_INICIO";
				if(sessFechaInicioNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+ sessFechaInicio.toString() + "'"; 
			}
			lb_FirstTime = false;
		}
		if (sessIdModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="SESS_ID";
				if(sessIdNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(sessId)+"'";
			} else {
				ls_setValues+=","+"SESS_ID";
				if(sessIdNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(sessId)+"'";
			}
			lb_FirstTime = false;
		}
		if (sessSrvRmtModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="SESS_SRV_RMT";
				if(sessSrvRmtNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(sessSrvRmt)+"'";
			} else {
				ls_setValues+=","+"SESS_SRV_RMT";
				if(sessSrvRmtNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(sessSrvRmt)+"'";
			}
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where+="SESS_PK";
			ls_where+="="+sessPk;
		} else {
			ls_where+=" AND "+"SESS_PK";
			ls_where+="="+sessPk;
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE EVENTO_SESSION SET "+ls_setValues+" WHERE "+ls_where);
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		String ls_where  = new String();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where+="SESS_PK";
			ls_where+="="+sessPk;
		} else {
			ls_where+=" AND "+"SESS_PK";
			ls_where+="="+sessPk;
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE EVENTO_SESSION WHERE "+ls_where);
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _sessPk
	 * @return EventoSession - Retrieved object
	 */
	static public EventoSession read(Connection _connection,long _sessPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		EventoSession ls_EventoSession=new EventoSession();
		ls_where="SESS_PK = " + _sessPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM EVENTO_SESSION WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_EventoSession.loadResultSet(ls_rs);
		} else {
			ls_EventoSession=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_EventoSession;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		sessAccCod=_rs.getLong("SESS_ACC_COD");
		sessPk=_rs.getLong("SESS_PK");
		syslogin=_rs.getString("SYSLOGIN");
		oficinaPk=_rs.getLong("OFICINA_PK");
		if (_rs.getDate("SESS_FECHA_INICIO")!=null) {
			sessFechaInicio=new java.sql.Date(_rs.getDate("SESS_FECHA_INICIO").getTime());
		} else {
				sessFechaInicio=null;
		}
		sessId=_rs.getString("SESS_ID");
		sessSrvRmt=_rs.getString("SESS_SRV_RMT");
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM EVENTO_SESSION ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		EventoSession myEventoSession;
		Vector myEventoSessiones = new Vector();
		while (ls_rs.next()) {
			myEventoSession = new EventoSession();
			myEventoSession.loadResultSet (ls_rs);
			myEventoSessiones.addElement(myEventoSession);
		}
		ls_rs.close();
		myStatement.close();
		return myEventoSessiones;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM EVENTO_SESSION ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM EVENTO_SESSION ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM EVENTO_SESSION ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setSessPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
