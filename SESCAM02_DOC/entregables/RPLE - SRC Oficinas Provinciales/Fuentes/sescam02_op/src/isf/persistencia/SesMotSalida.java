//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesMotSalida
// Table: SES_MOT_SALIDA
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_MOT_SALIDA
 * Columns for the table:
 * 	(motsal_cod - LONG), 
 * 	(motsal_desc - STRING), 
 * Primary columns for the table:
 * 	(motsal_cod - LONG) 
 */
public class SesMotSalida
{
	// Properties
	private long motsalCod;
	private boolean motsalCodNull=true;
	private boolean motsalCodModified=false;
	private String motsalDesc;
	private boolean motsalDescNull=true;
	private boolean motsalDescModified=false;
	// Access Method
	/** MotsalCod
	 * Get the value of the property MotsalCod.
	 * The column for the database is 'MOTSAL_COD'MotsalCod.
	 */
	public long getMotsalCod() {
		return motsalCod;
	}
	/** MotsalCod
	 * Set the value of the property MotsalCod.
	 * The column for the database is 'MOTSAL_COD'MotsalCod.
	 */
	public void setMotsalCod(long _motsalCod) {
		this.motsalCod=_motsalCod;
		this.motsalCodModified=true;
		this.motsalCodNull=false;
	}
	/** MotsalCod
	 * Set Null the value of the property MotsalCod.
	 * The column for the database is 'MOTSAL_COD'MotsalCod.
	 */
	public void setNullMotsalCod() {
		this.motsalCod=0;
		this.motsalCodModified=true;
		this.motsalCodNull=true;
	}
	/** MotsalCod
	 * Sumatory of value of the property MotsalCod.
	 * The column for the database is 'MOTSAL_COD'MotsalCod.
	 */
	static public double sumMotsalCod(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"MOTSAL_COD");
	}
	/** MotsalCod
	 * Indicates if the value of the property MotsalCod is null or not.
	 * The column for the database is 'MOTSAL_COD'MotsalCod.
	 */
	public boolean isNullMotsalCod() {
		return motsalCodNull;
	}
	/** MotsalDesc
	 * Get the value of the property MotsalDesc.
	 * The column for the database is 'MOTSAL_DESC'MotsalDesc.
	 */
	public String getMotsalDesc() {
		if (motsalDesc==null) return "";
		if (motsalDesc.compareTo("null")==0) return "";
		return motsalDesc;
	}
	/** MotsalDesc
	 * Set the value of the property MotsalDesc.
	 * The column for the database is 'MOTSAL_DESC'MotsalDesc.
	 */
	public void setMotsalDesc(String _motsalDesc) {
		this.motsalDesc=_motsalDesc;
		this.motsalDescModified=true;
		this.motsalDescNull=false;
	}
	/** MotsalDesc
	 * Set Null the value of the property MotsalDesc.
	 * The column for the database is 'MOTSAL_DESC'MotsalDesc.
	 */
	public void setNullMotsalDesc() {
		this.motsalDesc=null;
		this.motsalDescModified=true;
		this.motsalDescNull=true;
	}
	/** MotsalDesc
	 * Indicates if the value of the property MotsalDesc is null or not.
	 * The column for the database is 'MOTSAL_DESC'MotsalDesc.
	 */
	public boolean isNullMotsalDesc() {
		return motsalDescNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!motsalCodNull) {
			ls_columns.append((lb_FirstTime?"":",")+"MOTSAL_COD");
			ls_values.append((lb_FirstTime?"":",")+motsalCod);
			lb_FirstTime = false;
		}
		if (!motsalDescNull) {
			ls_columns.append((lb_FirstTime?"":",")+"MOTSAL_DESC");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(motsalDesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_MOT_SALIDA ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (motsalCodModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("MOTSAL_COD");
			if(motsalCodNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+motsalCod);
			lb_FirstTime = false;
		}
		if (motsalDescModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("MOTSAL_DESC");
			if(motsalDescNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(motsalDesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("MOTSAL_COD");
			ls_where.append("="+motsalCod);
		} else {
			ls_where.append(" AND "+"MOTSAL_COD");
			ls_where.append("="+motsalCod);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_MOT_SALIDA SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("MOTSAL_COD");
			ls_where.append("="+motsalCod);
		} else {
			ls_where.append(" AND "+"MOTSAL_COD");
			ls_where.append("="+motsalCod);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_MOT_SALIDA WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _motsalCod
	 * @return SesMotSalida - Retrieved object
	 */
	static public SesMotSalida read(Connection _connection,long _motsalCod) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesMotSalida ls_SesMotSalida=new SesMotSalida();
		ls_where="MOTSAL_COD = " + _motsalCod;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_MOT_SALIDA WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesMotSalida.loadResultSet(ls_rs);
		} else {
			ls_SesMotSalida=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesMotSalida;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		motsalCod=_rs.getLong("MOTSAL_COD");
		motsalCodNull=_rs.wasNull();
		motsalDesc=_rs.getString("MOTSAL_DESC");
		motsalDescNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_MOT_SALIDA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesMotSalida mySesMotSalida;
		Vector mySesMotSalidaes = new Vector();
		while (ls_rs.next()) {
			mySesMotSalida = new SesMotSalida();
			mySesMotSalida.loadResultSet (ls_rs);
			mySesMotSalidaes.addElement(mySesMotSalida);
		}
		ls_rs.close();
		myStatement.close();
		return mySesMotSalidaes;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_MOT_SALIDA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_MOT_SALIDA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_MOT_SALIDA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setMotsalCod(myPool.getSequence(this.getClass().getName(),profile));
	}
}
