//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesEstadisticas
// Table: SES_ESTADISTICAS
// Package: isf.persistence
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_ESTADISTICAS
 * Columns for the table:
 * 	(est_pk - LONG), 
 * 	(codcentro - DECIMAL), 
 * 	(codtramo - LONG), 
 * 	(posicion - LONG), 
 * 	(est_periodo - STRING), 
 * 	(est_tlista - STRING), 
 * 	(tproc_cen_cod - STRING), 
 * 	(proc_cen_cod - STRING), 
 * 	(serv_cen_cod - STRING), 
 * 	(esp_cen_cod - STRING), 
 * 	(diag_cen_cod - STRING), 
 * 	(est_cnt_pac - LONG), 
 * 	(est_cnt_dias - LONG), 
 * 	(est_catalog - STRING), 
 * Primary columns for the table:
 * 	(est_pk - LONG) 
 */
public class SesEstadisticas
{
	// Properties
	private long estPk;
	private boolean estPkNull=false;
	private boolean estPkModified=false;
	private double codcentro;
	private boolean codcentroNull=false;
	private boolean codcentroModified=false;
	private long codtramo;
	private boolean codtramoNull=false;
	private boolean codtramoModified=false;
	private long posicion;
	private boolean posicionNull=false;
	private boolean posicionModified=false;
	private String estPeriodo;
	private boolean estPeriodoNull=false;
	private boolean estPeriodoModified=false;
	private String estTlista;
	private boolean estTlistaNull=false;
	private boolean estTlistaModified=false;
	private String tprocCenCod;
	private boolean tprocCenCodNull=false;
	private boolean tprocCenCodModified=false;
	private String procCenCod;
	private boolean procCenCodNull=false;
	private boolean procCenCodModified=false;
	private String servCenCod;
	private boolean servCenCodNull=false;
	private boolean servCenCodModified=false;
	private String espCenCod;
	private boolean espCenCodNull=false;
	private boolean espCenCodModified=false;
	private String diagCenCod;
	private boolean diagCenCodNull=false;
	private boolean diagCenCodModified=false;
	private long estCntPac;
	private boolean estCntPacNull=false;
	private boolean estCntPacModified=false;
	private long estCntDias;
	private boolean estCntDiasNull=false;
	private boolean estCntDiasModified=false;
	private String estCatalog;
	private boolean estCatalogNull=false;
	private boolean estCatalogModified=false;
	// Access Method
	/** EstPk
	 * Get the value of the property EstPk.
	 * The column for the database is 'EST_PK'EstPk.
	 */
	public long getEstPk() {
		return estPk;
	}
	/** EstPk
	 * Set the value of the property EstPk.
	 * The column for the database is 'EST_PK'EstPk.
	 */
	public void setEstPk(long _estPk) {
		this.estPk=_estPk;
		this.estPkModified=true;
		this.estPkNull=false;
	}
	/** EstPk
	 * Set Null the value of the property EstPk.
	 * The column for the database is 'EST_PK'EstPk.
	 */
	public void setNullEstPk() {
		this.estPk=0;
		this.estPkModified=true;
		this.estPkNull=true;
	}
	/** EstPk
	 * Sumatory of value of the property EstPk.
	 * The column for the database is 'EST_PK'EstPk.
	 */
	static public double sumEstPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"EST_PK");
	}
	/** Codcentro
	 * Get the value of the property Codcentro.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	public double getCodcentro() {
		return codcentro;
	}
	/** Codcentro
	 * Set the value of the property Codcentro.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	public void setCodcentro(double _codcentro) {
		this.codcentro=_codcentro;
		this.codcentroModified=true;
		this.codcentroNull=false;
	}
	/** Codcentro
	 * Set Null the value of the property Codcentro.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	public void setNullCodcentro() {
		this.codcentro=0;
		this.codcentroModified=true;
		this.codcentroNull=true;
	}
	/** Codcentro
	 * Sumatory of value of the property Codcentro.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	static public double sumCodcentro(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CODCENTRO");
	}
	/** Codtramo
	 * Get the value of the property Codtramo.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	public long getCodtramo() {
		return codtramo;
	}
	/** Codtramo
	 * Set the value of the property Codtramo.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	public void setCodtramo(long _codtramo) {
		this.codtramo=_codtramo;
		this.codtramoModified=true;
		this.codtramoNull=false;
	}
	/** Codtramo
	 * Set Null the value of the property Codtramo.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	public void setNullCodtramo() {
		this.codtramo=0;
		this.codtramoModified=true;
		this.codtramoNull=true;
	}
	/** Codtramo
	 * Sumatory of value of the property Codtramo.
	 * The column for the database is 'CODTRAMO'Codtramo.
	 */
	static public double sumCodtramo(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CODTRAMO");
	}
	/** Posicion
	 * Get the value of the property Posicion.
	 * The column for the database is 'POSICION'Posicion.
	 */
	public long getPosicion() {
		return posicion;
	}
	/** Posicion
	 * Set the value of the property Posicion.
	 * The column for the database is 'POSICION'Posicion.
	 */
	public void setPosicion(long _posicion) {
		this.posicion=_posicion;
		this.posicionModified=true;
		this.posicionNull=false;
	}
	/** Posicion
	 * Set Null the value of the property Posicion.
	 * The column for the database is 'POSICION'Posicion.
	 */
	public void setNullPosicion() {
		this.posicion=0;
		this.posicionModified=true;
		this.posicionNull=true;
	}
	/** Posicion
	 * Sumatory of value of the property Posicion.
	 * The column for the database is 'POSICION'Posicion.
	 */
	static public double sumPosicion(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"POSICION");
	}
	/** EstPeriodo
	 * Get the value of the property EstPeriodo.
	 * The column for the database is 'EST_PERIODO'EstPeriodo.
	 */
	public String getEstPeriodo() {
		if (estPeriodo==null) return "";
		if (estPeriodo.compareTo("null")==0) return "";
		return estPeriodo;
	}
	/** EstPeriodo
	 * Set the value of the property EstPeriodo.
	 * The column for the database is 'EST_PERIODO'EstPeriodo.
	 */
	public void setEstPeriodo(String _estPeriodo) {
		this.estPeriodo=_estPeriodo;
		this.estPeriodoModified=true;
		this.estPeriodoNull=false;
	}
	/** EstPeriodo
	 * Set Null the value of the property EstPeriodo.
	 * The column for the database is 'EST_PERIODO'EstPeriodo.
	 */
	public void setNullEstPeriodo() {
		this.estPeriodo=null;
		this.estPeriodoModified=true;
		this.estPeriodoNull=true;
	}
	/** EstTlista
	 * Get the value of the property EstTlista.
	 * The column for the database is 'EST_TLISTA'EstTlista.
	 */
	public String getEstTlista() {
		if (estTlista==null) return "";
		if (estTlista.compareTo("null")==0) return "";
		return estTlista;
	}
	/** EstTlista
	 * Set the value of the property EstTlista.
	 * The column for the database is 'EST_TLISTA'EstTlista.
	 */
	public void setEstTlista(String _estTlista) {
		this.estTlista=_estTlista;
		this.estTlistaModified=true;
		this.estTlistaNull=false;
	}
	/** EstTlista
	 * Set Null the value of the property EstTlista.
	 * The column for the database is 'EST_TLISTA'EstTlista.
	 */
	public void setNullEstTlista() {
		this.estTlista=null;
		this.estTlistaModified=true;
		this.estTlistaNull=true;
	}
	/** TprocCenCod
	 * Get the value of the property TprocCenCod.
	 * The column for the database is 'TPROC_CEN_COD'TprocCenCod.
	 */
	public String getTprocCenCod() {
		if (tprocCenCod==null) return "";
		if (tprocCenCod.compareTo("null")==0) return "";
		return tprocCenCod;
	}
	/** TprocCenCod
	 * Set the value of the property TprocCenCod.
	 * The column for the database is 'TPROC_CEN_COD'TprocCenCod.
	 */
	public void setTprocCenCod(String _tprocCenCod) {
		this.tprocCenCod=_tprocCenCod;
		this.tprocCenCodModified=true;
		this.tprocCenCodNull=false;
	}
	/** TprocCenCod
	 * Set Null the value of the property TprocCenCod.
	 * The column for the database is 'TPROC_CEN_COD'TprocCenCod.
	 */
	public void setNullTprocCenCod() {
		this.tprocCenCod=null;
		this.tprocCenCodModified=true;
		this.tprocCenCodNull=true;
	}
	/** ProcCenCod
	 * Get the value of the property ProcCenCod.
	 * The column for the database is 'PROC_CEN_COD'ProcCenCod.
	 */
	public String getProcCenCod() {
		if (procCenCod==null) return "";
		if (procCenCod.compareTo("null")==0) return "";
		return procCenCod;
	}
	/** ProcCenCod
	 * Set the value of the property ProcCenCod.
	 * The column for the database is 'PROC_CEN_COD'ProcCenCod.
	 */
	public void setProcCenCod(String _procCenCod) {
		this.procCenCod=_procCenCod;
		this.procCenCodModified=true;
		this.procCenCodNull=false;
	}
	/** ProcCenCod
	 * Set Null the value of the property ProcCenCod.
	 * The column for the database is 'PROC_CEN_COD'ProcCenCod.
	 */
	public void setNullProcCenCod() {
		this.procCenCod=null;
		this.procCenCodModified=true;
		this.procCenCodNull=true;
	}
	/** ServCenCod
	 * Get the value of the property ServCenCod.
	 * The column for the database is 'SERV_CEN_COD'ServCenCod.
	 */
	public String getServCenCod() {
		if (servCenCod==null) return "";
		if (servCenCod.compareTo("null")==0) return "";
		return servCenCod;
	}
	/** ServCenCod
	 * Set the value of the property ServCenCod.
	 * The column for the database is 'SERV_CEN_COD'ServCenCod.
	 */
	public void setServCenCod(String _servCenCod) {
		this.servCenCod=_servCenCod;
		this.servCenCodModified=true;
		this.servCenCodNull=false;
	}
	/** ServCenCod
	 * Set Null the value of the property ServCenCod.
	 * The column for the database is 'SERV_CEN_COD'ServCenCod.
	 */
	public void setNullServCenCod() {
		this.servCenCod=null;
		this.servCenCodModified=true;
		this.servCenCodNull=true;
	}
	/** EspCenCod
	 * Get the value of the property EspCenCod.
	 * The column for the database is 'ESP_CEN_COD'EspCenCod.
	 */
	public String getEspCenCod() {
		if (espCenCod==null) return "";
		if (espCenCod.compareTo("null")==0) return "";
		return espCenCod;
	}
	/** EspCenCod
	 * Set the value of the property EspCenCod.
	 * The column for the database is 'ESP_CEN_COD'EspCenCod.
	 */
	public void setEspCenCod(String _espCenCod) {
		this.espCenCod=_espCenCod;
		this.espCenCodModified=true;
		this.espCenCodNull=false;
	}
	/** EspCenCod
	 * Set Null the value of the property EspCenCod.
	 * The column for the database is 'ESP_CEN_COD'EspCenCod.
	 */
	public void setNullEspCenCod() {
		this.espCenCod=null;
		this.espCenCodModified=true;
		this.espCenCodNull=true;
	}
	/** DiagCenCod
	 * Get the value of the property DiagCenCod.
	 * The column for the database is 'DIAG_CEN_COD'DiagCenCod.
	 */
	public String getDiagCenCod() {
		if (diagCenCod==null) return "";
		if (diagCenCod.compareTo("null")==0) return "";
		return diagCenCod;
	}
	/** DiagCenCod
	 * Set the value of the property DiagCenCod.
	 * The column for the database is 'DIAG_CEN_COD'DiagCenCod.
	 */
	public void setDiagCenCod(String _diagCenCod) {
		this.diagCenCod=_diagCenCod;
		this.diagCenCodModified=true;
		this.diagCenCodNull=false;
	}
	/** DiagCenCod
	 * Set Null the value of the property DiagCenCod.
	 * The column for the database is 'DIAG_CEN_COD'DiagCenCod.
	 */
	public void setNullDiagCenCod() {
		this.diagCenCod=null;
		this.diagCenCodModified=true;
		this.diagCenCodNull=true;
	}
	/** EstCntPac
	 * Get the value of the property EstCntPac.
	 * The column for the database is 'EST_CNT_PAC'EstCntPac.
	 */
	public long getEstCntPac() {
		return estCntPac;
	}
	/** EstCntPac
	 * Set the value of the property EstCntPac.
	 * The column for the database is 'EST_CNT_PAC'EstCntPac.
	 */
	public void setEstCntPac(long _estCntPac) {
		this.estCntPac=_estCntPac;
		this.estCntPacModified=true;
		this.estCntPacNull=false;
	}
	/** EstCntPac
	 * Set Null the value of the property EstCntPac.
	 * The column for the database is 'EST_CNT_PAC'EstCntPac.
	 */
	public void setNullEstCntPac() {
		this.estCntPac=0;
		this.estCntPacModified=true;
		this.estCntPacNull=true;
	}
	/** EstCntPac
	 * Sumatory of value of the property EstCntPac.
	 * The column for the database is 'EST_CNT_PAC'EstCntPac.
	 */
	static public double sumEstCntPac(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"EST_CNT_PAC");
	}
	/** EstCntDias
	 * Get the value of the property EstCntDias.
	 * The column for the database is 'EST_CNT_DIAS'EstCntDias.
	 */
	public long getEstCntDias() {
		return estCntDias;
	}
	/** EstCntDias
	 * Set the value of the property EstCntDias.
	 * The column for the database is 'EST_CNT_DIAS'EstCntDias.
	 */
	public void setEstCntDias(long _estCntDias) {
		this.estCntDias=_estCntDias;
		this.estCntDiasModified=true;
		this.estCntDiasNull=false;
	}
	/** EstCntDias
	 * Set Null the value of the property EstCntDias.
	 * The column for the database is 'EST_CNT_DIAS'EstCntDias.
	 */
	public void setNullEstCntDias() {
		this.estCntDias=0;
		this.estCntDiasModified=true;
		this.estCntDiasNull=true;
	}
	/** EstCntDias
	 * Sumatory of value of the property EstCntDias.
	 * The column for the database is 'EST_CNT_DIAS'EstCntDias.
	 */
	static public double sumEstCntDias(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"EST_CNT_DIAS");
	}
	/** EstCatalog
	 * Get the value of the property EstCatalog.
	 * The column for the database is 'EST_CATALOG'EstCatalog.
	 */
	public String getEstCatalog() {
		if (estCatalog==null) return "";
		if (estCatalog.compareTo("null")==0) return "";
		return estCatalog;
	}
	/** EstCatalog
	 * Set the value of the property EstCatalog.
	 * The column for the database is 'EST_CATALOG'EstCatalog.
	 */
	public void setEstCatalog(String _estCatalog) {
		this.estCatalog=_estCatalog;
		this.estCatalogModified=true;
		this.estCatalogNull=false;
	}
	/** EstCatalog
	 * Set Null the value of the property EstCatalog.
	 * The column for the database is 'EST_CATALOG'EstCatalog.
	 */
	public void setNullEstCatalog() {
		this.estCatalog=null;
		this.estCatalogModified=true;
		this.estCatalogNull=true;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		String ls_columns = new String();
		String ls_values  = new String();
		boolean lb_FirstTime = true;
		if (estPkModified) {
			if (lb_FirstTime ) { 
				ls_columns+="EST_PK";
				if(estPkNull) ls_values+="null";
				else ls_values+=estPk;
			} else {
				ls_columns+=","+"EST_PK";
				if(estPkNull) ls_values+=",null";
				else ls_values+=","+estPk;
			}
			lb_FirstTime = false;
		}
		if (codcentroModified) {
			if (lb_FirstTime ) { 
				ls_columns+="CODCENTRO";
				if(codcentroNull) ls_values+="null";
				else ls_values+=codcentro;
			} else {
				ls_columns+=","+"CODCENTRO";
				if(codcentroNull) ls_values+=",null";
				else ls_values+=","+codcentro;
			}
			lb_FirstTime = false;
		}
		if (codtramoModified) {
			if (lb_FirstTime ) { 
				ls_columns+="CODTRAMO";
				if(codtramoNull) ls_values+="null";
				else ls_values+=codtramo;
			} else {
				ls_columns+=","+"CODTRAMO";
				if(codtramoNull) ls_values+=",null";
				else ls_values+=","+codtramo;
			}
			lb_FirstTime = false;
		}
		if (posicionModified) {
			if (lb_FirstTime ) { 
				ls_columns+="POSICION";
				if(posicionNull) ls_values+="null";
				else ls_values+=posicion;
			} else {
				ls_columns+=","+"POSICION";
				if(posicionNull) ls_values+=",null";
				else ls_values+=","+posicion;
			}
			lb_FirstTime = false;
		}
		if (estPeriodoModified) {
			if (lb_FirstTime ) { 
				ls_columns+="EST_PERIODO";
				if(estPeriodoNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(estPeriodo)+"'";
			} else {
				ls_columns+=","+"EST_PERIODO";
				if(estPeriodoNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(estPeriodo)+"'";
			}
			lb_FirstTime = false;
		}
		if (estTlistaModified) {
			if (lb_FirstTime ) { 
				ls_columns+="EST_TLISTA";
				if(estTlistaNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(estTlista)+"'";
			} else {
				ls_columns+=","+"EST_TLISTA";
				if(estTlistaNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(estTlista)+"'";
			}
			lb_FirstTime = false;
		}
		if (tprocCenCodModified) {
			if (lb_FirstTime ) { 
				ls_columns+="TPROC_CEN_COD";
				if(tprocCenCodNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(tprocCenCod)+"'";
			} else {
				ls_columns+=","+"TPROC_CEN_COD";
				if(tprocCenCodNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(tprocCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (procCenCodModified) {
			if (lb_FirstTime ) { 
				ls_columns+="PROC_CEN_COD";
				if(procCenCodNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(procCenCod)+"'";
			} else {
				ls_columns+=","+"PROC_CEN_COD";
				if(procCenCodNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(procCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (servCenCodModified) {
			if (lb_FirstTime ) { 
				ls_columns+="SERV_CEN_COD";
				if(servCenCodNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(servCenCod)+"'";
			} else {
				ls_columns+=","+"SERV_CEN_COD";
				if(servCenCodNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(servCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (espCenCodModified) {
			if (lb_FirstTime ) { 
				ls_columns+="ESP_CEN_COD";
				if(espCenCodNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(espCenCod)+"'";
			} else {
				ls_columns+=","+"ESP_CEN_COD";
				if(espCenCodNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(espCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (diagCenCodModified) {
			if (lb_FirstTime ) { 
				ls_columns+="DIAG_CEN_COD";
				if(diagCenCodNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(diagCenCod)+"'";
			} else {
				ls_columns+=","+"DIAG_CEN_COD";
				if(diagCenCodNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(diagCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (estCntPacModified) {
			if (lb_FirstTime ) { 
				ls_columns+="EST_CNT_PAC";
				if(estCntPacNull) ls_values+="null";
				else ls_values+=estCntPac;
			} else {
				ls_columns+=","+"EST_CNT_PAC";
				if(estCntPacNull) ls_values+=",null";
				else ls_values+=","+estCntPac;
			}
			lb_FirstTime = false;
		}
		if (estCntDiasModified) {
			if (lb_FirstTime ) { 
				ls_columns+="EST_CNT_DIAS";
				if(estCntDiasNull) ls_values+="null";
				else ls_values+=estCntDias;
			} else {
				ls_columns+=","+"EST_CNT_DIAS";
				if(estCntDiasNull) ls_values+=",null";
				else ls_values+=","+estCntDias;
			}
			lb_FirstTime = false;
		}
		if (estCatalogModified) {
			if (lb_FirstTime ) { 
				ls_columns+="EST_CATALOG";
				if(estCatalogNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(estCatalog)+"'";
			} else {
				ls_columns+=","+"EST_CATALOG";
				if(estCatalogNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(estCatalog)+"'";
			}
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_ESTADISTICAS ("+ls_columns+") VALUES ("+ls_values+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		String ls_setValues = new String();
		String ls_where  = new String();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (estPkModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="EST_PK";
				if(estPkNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+estPk;
			} else {
				ls_setValues+=","+"EST_PK";
				if(estPkNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+estPk;
			}
			lb_FirstTime = false;
		}
		if (codcentroModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="CODCENTRO";
				if(codcentroNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+codcentro;
			} else {
				ls_setValues+=","+"CODCENTRO";
				if(codcentroNull) {
						ls_setValues+="=null";
				} else ls_setValues+="="+codcentro;
			}
			lb_FirstTime = false;
		}
		if (codtramoModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="CODTRAMO";
				if(codtramoNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+codtramo;
			} else {
				ls_setValues+=","+"CODTRAMO";
				if(codtramoNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+codtramo;
			}
			lb_FirstTime = false;
		}
		if (posicionModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="POSICION";
				if(posicionNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+posicion;
			} else {
				ls_setValues+=","+"POSICION";
				if(posicionNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+posicion;
			}
			lb_FirstTime = false;
		}
		if (estPeriodoModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="EST_PERIODO";
				if(estPeriodoNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(estPeriodo)+"'";
			} else {
				ls_setValues+=","+"EST_PERIODO";
				if(estPeriodoNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(estPeriodo)+"'";
			}
			lb_FirstTime = false;
		}
		if (estTlistaModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="EST_TLISTA";
				if(estTlistaNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(estTlista)+"'";
			} else {
				ls_setValues+=","+"EST_TLISTA";
				if(estTlistaNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(estTlista)+"'";
			}
			lb_FirstTime = false;
		}
		if (tprocCenCodModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="TPROC_CEN_COD";
				if(tprocCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(tprocCenCod)+"'";
			} else {
				ls_setValues+=","+"TPROC_CEN_COD";
				if(tprocCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(tprocCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (procCenCodModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="PROC_CEN_COD";
				if(procCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(procCenCod)+"'";
			} else {
				ls_setValues+=","+"PROC_CEN_COD";
				if(procCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(procCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (servCenCodModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="SERV_CEN_COD";
				if(servCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(servCenCod)+"'";
			} else {
				ls_setValues+=","+"SERV_CEN_COD";
				if(servCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(servCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (espCenCodModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="ESP_CEN_COD";
				if(espCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(espCenCod)+"'";
			} else {
				ls_setValues+=","+"ESP_CEN_COD";
				if(espCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(espCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (diagCenCodModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="DIAG_CEN_COD";
				if(diagCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(diagCenCod)+"'";
			} else {
				ls_setValues+=","+"DIAG_CEN_COD";
				if(diagCenCodNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(diagCenCod)+"'";
			}
			lb_FirstTime = false;
		}
		if (estCntPacModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="EST_CNT_PAC";
				if(estCntPacNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+estCntPac;
			} else {
				ls_setValues+=","+"EST_CNT_PAC";
				if(estCntPacNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+estCntPac;
			}
			lb_FirstTime = false;
		}
		if (estCntDiasModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="EST_CNT_DIAS";
				if(estCntDiasNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+estCntDias;
			} else {
				ls_setValues+=","+"EST_CNT_DIAS";
				if(estCntDiasNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+estCntDias;
			}
			lb_FirstTime = false;
		}
		if (estCatalogModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="EST_CATALOG";
				if(estCatalogNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(estCatalog)+"'";
			} else {
				ls_setValues+=","+"EST_CATALOG";
				if(estCatalogNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(estCatalog)+"'";
			}
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where+="EST_PK";
			ls_where+="="+estPk;
		} else {
			ls_where+=" AND "+"EST_PK";
			ls_where+="="+estPk;
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_ESTADISTICAS SET "+ls_setValues+" WHERE "+ls_where);
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		String ls_where  = new String();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where+="EST_PK";
			ls_where+="="+estPk;
		} else {
			ls_where+=" AND "+"EST_PK";
			ls_where+="="+estPk;
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_ESTADISTICAS WHERE "+ls_where);
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _estPk
	 * @return SesEstadisticas - Retrieved object
	 */
	static public SesEstadisticas read(Connection _connection,long _estPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesEstadisticas ls_SesEstadisticas=new SesEstadisticas();
		ls_where="EST_PK = " + _estPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_ESTADISTICAS WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesEstadisticas.loadResultSet(ls_rs);
		} else {
			ls_SesEstadisticas=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesEstadisticas;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		estPk=_rs.getLong("EST_PK");
		codcentro=_rs.getDouble("CODCENTRO");
		codtramo=_rs.getLong("CODTRAMO");
		posicion=_rs.getLong("POSICION");
		estPeriodo=_rs.getString("EST_PERIODO");
		estTlista=_rs.getString("EST_TLISTA");
		tprocCenCod=_rs.getString("TPROC_CEN_COD");
		procCenCod=_rs.getString("PROC_CEN_COD");
		servCenCod=_rs.getString("SERV_CEN_COD");
		espCenCod=_rs.getString("ESP_CEN_COD");
		diagCenCod=_rs.getString("DIAG_CEN_COD");
		estCntPac=_rs.getLong("EST_CNT_PAC");
		estCntDias=_rs.getLong("EST_CNT_DIAS");
		estCatalog=_rs.getString("EST_CATALOG");
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_ESTADISTICAS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesEstadisticas mySesEstadisticas;
		Vector mySesEstadisticases = new Vector();
		while (ls_rs.next()) {
			mySesEstadisticas = new SesEstadisticas();
			mySesEstadisticas.loadResultSet (ls_rs);
			mySesEstadisticases.addElement(mySesEstadisticas);
		}
		ls_rs.close();
		myStatement.close();
		return mySesEstadisticases;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_ESTADISTICAS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_ESTADISTICAS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_ESTADISTICAS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setEstPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
