//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: EventoTipoVar
// Table: EVENTO_TIPO_VAR
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:EVENTO_TIPO_VAR
 * Columns for the table:
 * 	(tpoevto_var_pk - LONG), 
 * 	(tpoevto_pk - LONG), 
 * 	(tpoevto_var_desc - STRING), 
 * 	(tpoevto_var_act - LONG), 
 * Primary columns for the table:
 * 	(tpoevto_var_pk - LONG) 
 */
public class EventoTipoVar
{
	// Properties
	private long tpoevtoVarPk;
	private boolean tpoevtoVarPkNull=true;
	private boolean tpoevtoVarPkModified=false;
	private long tpoevtoPk;
	private boolean tpoevtoPkNull=true;
	private boolean tpoevtoPkModified=false;
	private String tpoevtoVarDesc;
	private boolean tpoevtoVarDescNull=true;
	private boolean tpoevtoVarDescModified=false;
	private long tpoevtoVarAct;
	private boolean tpoevtoVarActNull=true;
	private boolean tpoevtoVarActModified=false;
	// Access Method
	/** TpoevtoVarPk
	 * Get the value of the property TpoevtoVarPk.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	public long getTpoevtoVarPk() {
		return tpoevtoVarPk;
	}
	/** TpoevtoVarPk
	 * Set the value of the property TpoevtoVarPk.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	public void setTpoevtoVarPk(long _tpoevtoVarPk) {
		this.tpoevtoVarPk=_tpoevtoVarPk;
		this.tpoevtoVarPkModified=true;
		this.tpoevtoVarPkNull=false;
	}
	/** TpoevtoVarPk
	 * Set Null the value of the property TpoevtoVarPk.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	public void setNullTpoevtoVarPk() {
		this.tpoevtoVarPk=0;
		this.tpoevtoVarPkModified=true;
		this.tpoevtoVarPkNull=true;
	}
	/** TpoevtoVarPk
	 * Sumatory of value of the property TpoevtoVarPk.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	static public double sumTpoevtoVarPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"TPOEVTO_VAR_PK");
	}
	/** TpoevtoVarPk
	 * Indicates if the value of the property TpoevtoVarPk is null or not.
	 * The column for the database is 'TPOEVTO_VAR_PK'TpoevtoVarPk.
	 */
	public boolean isNullTpoevtoVarPk() {
		return tpoevtoVarPkNull;
	}
	/** TpoevtoPk
	 * Get the value of the property TpoevtoPk.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	public long getTpoevtoPk() {
		return tpoevtoPk;
	}
	/** TpoevtoPk
	 * Set the value of the property TpoevtoPk.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	public void setTpoevtoPk(long _tpoevtoPk) {
		this.tpoevtoPk=_tpoevtoPk;
		this.tpoevtoPkModified=true;
		this.tpoevtoPkNull=false;
	}
	/** TpoevtoPk
	 * Set Null the value of the property TpoevtoPk.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	public void setNullTpoevtoPk() {
		this.tpoevtoPk=0;
		this.tpoevtoPkModified=true;
		this.tpoevtoPkNull=true;
	}
	/** TpoevtoPk
	 * Sumatory of value of the property TpoevtoPk.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	static public double sumTpoevtoPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"TPOEVTO_PK");
	}
	/** TpoevtoPk
	 * Indicates if the value of the property TpoevtoPk is null or not.
	 * The column for the database is 'TPOEVTO_PK'TpoevtoPk.
	 */
	public boolean isNullTpoevtoPk() {
		return tpoevtoPkNull;
	}
	/** TpoevtoVarDesc
	 * Get the value of the property TpoevtoVarDesc.
	 * The column for the database is 'TPOEVTO_VAR_DESC'TpoevtoVarDesc.
	 */
	public String getTpoevtoVarDesc() {
		if (tpoevtoVarDesc==null) return "";
		if (tpoevtoVarDesc.compareTo("null")==0) return "";
		return tpoevtoVarDesc;
	}
	/** TpoevtoVarDesc
	 * Set the value of the property TpoevtoVarDesc.
	 * The column for the database is 'TPOEVTO_VAR_DESC'TpoevtoVarDesc.
	 */
	public void setTpoevtoVarDesc(String _tpoevtoVarDesc) {
		this.tpoevtoVarDesc=_tpoevtoVarDesc;
		this.tpoevtoVarDescModified=true;
		this.tpoevtoVarDescNull=false;
	}
	/** TpoevtoVarDesc
	 * Set Null the value of the property TpoevtoVarDesc.
	 * The column for the database is 'TPOEVTO_VAR_DESC'TpoevtoVarDesc.
	 */
	public void setNullTpoevtoVarDesc() {
		this.tpoevtoVarDesc=null;
		this.tpoevtoVarDescModified=true;
		this.tpoevtoVarDescNull=true;
	}
	/** TpoevtoVarDesc
	 * Indicates if the value of the property TpoevtoVarDesc is null or not.
	 * The column for the database is 'TPOEVTO_VAR_DESC'TpoevtoVarDesc.
	 */
	public boolean isNullTpoevtoVarDesc() {
		return tpoevtoVarDescNull;
	}
	/** TpoevtoVarAct
	 * Get the value of the property TpoevtoVarAct.
	 * The column for the database is 'TPOEVTO_VAR_ACT'TpoevtoVarAct.
	 */
	public long getTpoevtoVarAct() {
		return tpoevtoVarAct;
	}
	/** TpoevtoVarAct
	 * Set the value of the property TpoevtoVarAct.
	 * The column for the database is 'TPOEVTO_VAR_ACT'TpoevtoVarAct.
	 */
	public void setTpoevtoVarAct(long _tpoevtoVarAct) {
		this.tpoevtoVarAct=_tpoevtoVarAct;
		this.tpoevtoVarActModified=true;
		this.tpoevtoVarActNull=false;
	}
	/** TpoevtoVarAct
	 * Set Null the value of the property TpoevtoVarAct.
	 * The column for the database is 'TPOEVTO_VAR_ACT'TpoevtoVarAct.
	 */
	public void setNullTpoevtoVarAct() {
		this.tpoevtoVarAct=0;
		this.tpoevtoVarActModified=true;
		this.tpoevtoVarActNull=true;
	}
	/** TpoevtoVarAct
	 * Sumatory of value of the property TpoevtoVarAct.
	 * The column for the database is 'TPOEVTO_VAR_ACT'TpoevtoVarAct.
	 */
	static public double sumTpoevtoVarAct(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"TPOEVTO_VAR_ACT");
	}
	/** TpoevtoVarAct
	 * Indicates if the value of the property TpoevtoVarAct is null or not.
	 * The column for the database is 'TPOEVTO_VAR_ACT'TpoevtoVarAct.
	 */
	public boolean isNullTpoevtoVarAct() {
		return tpoevtoVarActNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!tpoevtoVarPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"TPOEVTO_VAR_PK");
			ls_values.append((lb_FirstTime?"":",")+tpoevtoVarPk);
			lb_FirstTime = false;
		}
		if (!tpoevtoPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"TPOEVTO_PK");
			ls_values.append((lb_FirstTime?"":",")+tpoevtoPk);
			lb_FirstTime = false;
		}
		if (!tpoevtoVarDescNull) {
			ls_columns.append((lb_FirstTime?"":",")+"TPOEVTO_VAR_DESC");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(tpoevtoVarDesc)+"'");
			lb_FirstTime = false;
		}
		if (!tpoevtoVarActNull) {
			ls_columns.append((lb_FirstTime?"":",")+"TPOEVTO_VAR_ACT");
			ls_values.append((lb_FirstTime?"":",")+tpoevtoVarAct);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO EVENTO_TIPO_VAR ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (tpoevtoVarPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("TPOEVTO_VAR_PK");
			if(tpoevtoVarPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+tpoevtoVarPk);
			lb_FirstTime = false;
		}
		if (tpoevtoPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("TPOEVTO_PK");
			if(tpoevtoPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+tpoevtoPk);
			lb_FirstTime = false;
		}
		if (tpoevtoVarDescModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("TPOEVTO_VAR_DESC");
			if(tpoevtoVarDescNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(tpoevtoVarDesc)+"'");
			lb_FirstTime = false;
		}
		if (tpoevtoVarActModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("TPOEVTO_VAR_ACT");
			if(tpoevtoVarActNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+tpoevtoVarAct);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("TPOEVTO_VAR_PK");
			ls_where.append("="+tpoevtoVarPk);
		} else {
			ls_where.append(" AND "+"TPOEVTO_VAR_PK");
			ls_where.append("="+tpoevtoVarPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE EVENTO_TIPO_VAR SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("TPOEVTO_VAR_PK");
			ls_where.append("="+tpoevtoVarPk);
		} else {
			ls_where.append(" AND "+"TPOEVTO_VAR_PK");
			ls_where.append("="+tpoevtoVarPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE EVENTO_TIPO_VAR WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _tpoevtoVarPk
	 * @return EventoTipoVar - Retrieved object
	 */
	static public EventoTipoVar read(Connection _connection,long _tpoevtoVarPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		EventoTipoVar ls_EventoTipoVar=new EventoTipoVar();
		ls_where="TPOEVTO_VAR_PK = " + _tpoevtoVarPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM EVENTO_TIPO_VAR WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_EventoTipoVar.loadResultSet(ls_rs);
		} else {
			ls_EventoTipoVar=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_EventoTipoVar;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		tpoevtoVarPk=_rs.getLong("TPOEVTO_VAR_PK");
		tpoevtoVarPkNull=_rs.wasNull();
		tpoevtoPk=_rs.getLong("TPOEVTO_PK");
		tpoevtoPkNull=_rs.wasNull();
		tpoevtoVarDesc=_rs.getString("TPOEVTO_VAR_DESC");
		tpoevtoVarDescNull=_rs.wasNull();
		tpoevtoVarAct=_rs.getLong("TPOEVTO_VAR_ACT");
		tpoevtoVarActNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM EVENTO_TIPO_VAR ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		EventoTipoVar myEventoTipoVar;
		Vector myEventoTipoVares = new Vector();
		while (ls_rs.next()) {
			myEventoTipoVar = new EventoTipoVar();
			myEventoTipoVar.loadResultSet (ls_rs);
			myEventoTipoVares.addElement(myEventoTipoVar);
		}
		ls_rs.close();
		myStatement.close();
		return myEventoTipoVares;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM EVENTO_TIPO_VAR ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM EVENTO_TIPO_VAR ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM EVENTO_TIPO_VAR ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setTpoevtoVarPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
