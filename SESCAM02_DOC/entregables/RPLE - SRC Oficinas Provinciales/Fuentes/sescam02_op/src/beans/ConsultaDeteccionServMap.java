/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaServMapeados </p>
 * <p>Description: Bean que realiza la consulta de los servicios que no han sido mapeados.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author F�lix Alberto Bravo Hermoso
 * @version 1.0
 */
public class ConsultaDeteccionServMap {
    private long centro;
    private int resultCount;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 2000;

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de servicios no mapeados.
     * Utiliza objetos de la clase ServMapeados para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos ServMapeados, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase ServMapeados.
     * @throws Exception
     */
    public Vector resultado() throws Exception {
        String sentBusqueda = new String("");
        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_Proc = new Vector();
        String ls_where = "";

        //    sentBusqueda = " select s.CODSERV,s.MAES_SERV from servicios s ";
        //    String ls_where = " where centro=? and s.CODSERV not in"+
        //                      " (select ssm.SRV_CEN_COD from ses_srv_map ssm where centro=?)";
        sentBusqueda = "select s.CODSERV,s.MAES_SERV,s.CENTRO from servicios s ";

        if (centro != 0) {
            ls_where = "where centro=? and " +
                "((s.MAES_SERV is null) or trim(s.MAES_SERV) not in (select trim(ss.SESSRV_COD) from ses_servicios ss)) and " +
                "trim(s.CODSERV) not in (select trim(ssm.SRV_CEN_COD) from ses_srv_map ssm where centro=?)";
        } else {
            ls_where = "where ((s.MAES_SERV is null) or trim(s.MAES_SERV) not in (select trim(ss.SESSRV_COD) from ses_servicios ss)) and " +
                "trim(s.CODSERV) not in (select trim(ssm.SRV_CEN_COD) from ses_srv_map ssm)";
        }

        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            sentBusqueda = sentBusqueda + ls_where +
                " order by centro,codserv ";

            PreparedStatement st = conn.prepareStatement(sentBusqueda);

            if (centro != 0) {
                st.setLong(1, centro);
                st.setLong(2, centro);
            }

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                ServMapeados serv = new ServMapeados();
                serv.CodServMap = rs.getString(1);
                serv.CodEspMapeo = rs.getString(2);
                serv.CodCentroMapeo = rs.getLong(3);

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_Proc.addElement(serv);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_Proc;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param centro :long, l_centro por el que filtramos la consulta.
     * @since 04/03/2003
     * @author ABH
     */
    public void setCentro(long l_centro) {
        if (l_centro > 0) {
            this.centro = l_centro;
        }
    }

    /**
     * Obtiene el parametro al atributo centro de la clase.
     * @return long, centro.
     * @since 04/03/2003
     * @author ABH
     */
    public long Centro() {
        return this.centro;
    }

    /*

      public static void main(String args[]){
              ConsultaNodosProcedimientos cp=new ConsultaNodosProcedimientos();
              NodoProcedimiento np;
              Vector vp=null;
              cp.setCodigo("12");
              cp.setDesc("");
              cp.setPkPadre(3);
              try{
              vp=cp.resultado();
              }catch(Exception ex){}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: Centro /"+cp.getCodigo()+"/ - HICL /"+cp.GetPkPadre()+"/");
                      }
              else        {
                        Enumeration e = vp.elements();
                        while (e.hasMoreElements()) {
                            np=(NodoProcedimiento)e.nextElement();
                            System.out.println(np.NodoDesc);
                            }
                      }
              }
    */
}
