/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;
import java.sql.CallableStatement;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;


/**
 * <p>Clase: ConsultaEntradasLEDet </p>
 * <p>Description: Bean que realiza la consulta de la antiguedad de las entradas en la lista de espera.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Iv�n Enrique Caneri
 * @version 1.0
 */
public class ConsultaEntradasLE {
    static public final int NOCAT = 0;
    static public final int TG = 1;
    static public final int TG_DV = 2;
    static public final int TG_DM = 3;
    static public final int SG_RO = 4;
    static public final int PSG = 5;
    static public final int SG_BPA = 6;
    static public final int HISTOR = 7;
    static public final int NOMAP = 8;
    static public final int SSG = 9;
    static public final String[] STR_SITUACION = {
        "NOCAT", "TG", "TG_DV", "TG_DM", "SG_RO", "PSG", "SG_BPA", "HISTOR",
        "NOMAP","SSG"
    };
    private String cip;
    private int resultCount;
    public final int MAX_RESULT_SET = 500;
    public Vector vec_EntradasLE;

    /**
     * Contructor de la clase ConsultaEntradaLE
     */
    public ConsultaEntradasLE() {
        vec_EntradasLE = new Vector();
    }

    /**
     * Dados los parametros de entrada localiza una entrada en la lista de Espera.
     * @param pCentro  long , Centro.
     * @param pNOrden  long , Norden.
     * @param cq  String, Indica si el registro es de LESP ("Q") o de LESPCEX ("C").
     * @return devuelve un objeto EntradaLE si lo encuentra y sino null.
     * @since 15/10/2002
     * @author IEC
     */
    public EntradaLE existeEntrada(long pCentro, long pNOrden, String cq) {
        EntradaLE entLE;

        if (vec_EntradasLE == null) {
            return null;
        }

        Enumeration e = vec_EntradasLE.elements();

        while (e.hasMoreElements()) {
            entLE = (EntradaLE) e.nextElement();

            if ((entLE.fCentro == pCentro) && (entLE.fNorden == pNOrden) &&
                    (entLE.fTipoCQ.equals(cq))) {
                return entLE;
            }
        }

        //Si no se encontro devuelve -1
        return null;
    }

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de registros de Lista de Espera y de su ejecucion.
     * Utiliza objetos de la clase EntradasLE para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos EntradasLE, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase EntradasLE.
     * @throws Exception
     * @since 15/10/2002
     * @author IEC
     */
    public Vector resultado() throws Exception {
        vec_EntradasLE.clear();

        // Validaci�n de parametros
        if ((cip == null) || (cip.equals(""))) {
            return (null);
        }

        Connection conn = null;
        Pool ref_pool = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        PreparedStatement st;
        ResultSet rs;
        String sentBusqueda = " SELECT " +
            "  distinct lesp.cip cip,lesp.centro centro,lesp.norden norden,lesp.cpr1 cproc,lesp.dpr1 dproc,lesp.hicl hicl,lesp.moti moti, " +
            "  lesp.fsal fsal,lesp.LINK LINK,o.TLE antiguedad,'Q' tipo_cq,lesp.catalog spac,o.ACTIVO activo,lesp.secc serv " +
            " FROM  " + "  lesp lesp,lesp ld,oltle o " + " WHERE  " +
            "  ld.cip =? AND " +
            "  lesp.norden=legase.CLAVE_INICIAL(ld.CENTRO,1,ld.norden) and " +
            "  lesp.centro=ld.centro and " + "  o.CENTRO=lesp.centro and " +
            "  o.CLAVEINICIAL=lesp.NORDEN and " + "  o.TIPO=1 " +
            " UNION ALL " + " SELECT  " +
            "  cip, cega centro, ncita norden, prestaci cproc, prestaci_des||' '||servproc_des dproc,hicl, motisali moti, fsalida fsal, " +
            "  0 LINK,o.TLE antiguedad, lespcex.IDENTPRES tipo_cq , lespcex.catalog spac, o.ACTIVO activo, lespcex.servreal serv" +
            " FROM lespcex, oltle o  " + " WHERE  " + "  CIP=? and " +
            "  o.CENTRO=lespcex.cega and " +
            "  o.CLAVEINICIAL=lespcex.ncita and " + "  o.TIPO=2 ";
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            CallableStatement cs = conn.prepareCall(
                    "begin legase.Simple_cip(?); end;");
            cs.setString(1, cip);
            cs.execute();
        } catch (SQLException ex1) {
        }

        try {
            conn.setAutoCommit(false);
            st = conn.prepareStatement(sentBusqueda);
            st.setString(1, cip);
            st.setString(2, cip);
            rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                EntradaLE entLE = new EntradaLE();
                entLE.fNorden = rs.getInt("norden");
                entLE.fCentro = rs.getInt("centro");
                entLE.fHicl = rs.getInt("hicl");
                entLE.fAntiguedad = rs.getInt("antiguedad");
                entLE.fCProc = rs.getString("cproc");
                entLE.fCip = rs.getString("cip");
                entLE.fDProc = rs.getString("dproc");
                entLE.fSalida = rs.getString("fsal");
                entLE.fActivo = rs.getInt("activo");

                try {
                    entLE.fMotivo = rs.getInt("moti");
                } catch (Exception ei) {
                    entLE.fMotivo = 0;
                }

                entLE.fLink = rs.getInt("link");

                if (rs.getString("spac").equals("PSG")) {
                    entLE.fSituacion = PSG;
                } else if (rs.getString("spac").equals("SG-RO")) {
                    entLE.fSituacion = SG_RO;
                } else if (rs.getString("spac").equals("SG-BPA")) {
                    entLE.fSituacion = SG_BPA;
                } else if (rs.getString("spac").equals("TG-DV")) {
                    entLE.fSituacion = TG_DV;
                } else if (rs.getString("spac").equals("TG-DM")) {
                    entLE.fSituacion = TG_DM;
                } else if (rs.getString("spac").equals("TG")) {
                    entLE.fSituacion = TG;
                } else if (rs.getString("spac").equals("NOCAT")) {
                    entLE.fSituacion = NOCAT;
                } else if (rs.getString("spac").equals("HISTOR")) {
                    entLE.fSituacion = HISTOR;
                } else if (rs.getString("spac").equals("NOMAP")) {
                    entLE.fSituacion = NOMAP;
                } else if (rs.getString("spac").equals("SSG")) {
					entLE.fSituacion = SSG;
                }

                entLE.fTipoCQ = rs.getString("tipo_cq");
                entLE.fServicio = rs.getString("serv");

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_EntradasLE.addElement(entLE);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            vec_EntradasLE.clear();

            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        LlenaProcMax();

        //      LlenaEstancia();
        return vec_EntradasLE;
    }

    /**
     *
     * @return Devuelve
     * @throws Exception
     * @since 15/10/2002
     * @author IEC
     */
    public boolean LlenaProcMax() throws Exception {
        Hashtable hashProcs = new Hashtable();

        // Validaci�n de parametros
        if ((cip == null) || (cip.equals(""))) {
            return false;
        }

        Connection conn = null;
        Pool ref_pool = null;
        String strCant = "0";
        resultCount = 0;

        String sentBusqueda =
            "select p.PROC_pk, p.PROC_COD, g.proc_garantia garantia from " +
            " ses_procedimientos p, ses_proc_garantia g, ses_proc_map m, ses_srv_map sm, ses_servicios s " +
            " where p.PROC_PK=g.PROC_PK and m.PROC_PK=p.PROC_PK and s.sessrv_cod=sm.SESSRV_COD " +
            " and trim(m.PROC_CEN_COD)=? " + " and m.PROC_CEN_TIPO=? " +
            " and m.CENTRO=? " +
            " and (trim(g.SESESP_COD)=trim(s.SESESP_COD) Or g.SESESP_COD Is Null) " +
            " and trim(sm.SRV_CEN_COD)=? " + " and sm.CENTRO=? " +
            " order by g.SESESP_COD";
        Enumeration entr = vec_EntradasLE.elements();
        EntradaLE entLE = null;
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            while (entr.hasMoreElements()) {
                entLE = (EntradaLE) entr.nextElement();
                conn.setAutoCommit(false);

                PreparedStatement st = conn.prepareStatement(sentBusqueda);
                st.setString(1, entLE.fCProc);
                st.setString(2, entLE.fTipoCQ);
                st.setInt(3, entLE.fCentro);
                st.setString(4, entLE.fServicio);
                st.setInt(5, entLE.fCentro);

                ResultSet rs = st.executeQuery();

                if (rs.next()) {
                    try {
                        entLE.fMaxEspera = rs.getInt("garantia");
                    } catch (Exception ei) {
                        entLE.fMaxEspera = 0;
                        System.out.println("Error:" + ei);
                    }
                }

                conn.commit();
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return true;
    }

    /*--------------------------------------------*/
    /* Llena el tiempo de espera para las entradas*/
    /*--------------------------------------------*/
    public boolean LlenaEstancia() throws Exception {
        Hashtable hashProcs = new Hashtable();

        // Validaci�n de parametros
        if ((cip == null) || (cip.equals(""))) {
            return false;
        }

        Connection conn = null;
        Pool ref_pool = null;
        resultCount = 0;

        String sentBusqueda =
            " SELECT centro,tipo,claveinicial,tle FROM OLTLE WHERE CLAVEINICIAL IN " +
            " (select DISTINCT CLAVEINICIAL from solapes " +
            " where centro=? and tipo=? and clave=?) ";
        Enumeration entr = vec_EntradasLE.elements();
        EntradaLE entLE;
        ResultSet rs;
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
        conn.setAutoCommit(false);

        PreparedStatement st = conn.prepareStatement(sentBusqueda);

        try {
            while (entr.hasMoreElements()) {
                entLE = (EntradaLE) entr.nextElement();
                st.setInt(1, entLE.fCentro);

                if (entLE.fTipoCQ.equals("Q")) {
                    st.setInt(2, 1);
                } else {
                    st.setInt(2, 2);
                }

                st.setInt(3, entLE.fNorden);
                rs = st.executeQuery();

                while (rs.next()) {
                    entLE.fAntiguedad = rs.getInt("tle");
                }
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return true;
    }

    /**
     * Asigna el parametro al atributo cip de la clase.
     * @param pCip String,CIP.
     * @since 15/10/2002
     * @author IEC
     */
    public void setCip(String pCip) {
        if (pCip != null) {
            this.cip = pCip;
        }
    }

    /**
     * Obtiene el valor del atributo centro de la clase.
     * @return String,CIP.
     * @since 15/10/2002
     * @author IEC
     */
    public String getCip() {
        return cip;
    }

    /**
     * Obtiene el valor del atributo resultCount de la clase.
     * @return int,
     * @since 15/10/2002
     * @author IEC
     */
    public int getResultCount() {
        return resultCount;
    }

    public static void main(String[] args) {
        ConsultaEntradasLE cpb = new ConsultaEntradasLE();
        EntradaLE entLE;
        Vector vp = null;
        cpb.setCip("#1002049");

        try {
            vp = cpb.resultado();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (vp == null) {
            System.out.println("No hay registros con las condiciones: Cip /" +
                cpb.getCip());
        } else {
            Enumeration e = vp.elements();

            while (e.hasMoreElements()) {
                entLE = (EntradaLE) e.nextElement();
            }

            System.out.println("Cantidad de registros:" + cpb.getResultCount());
        }
    }
}
