/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaMapeosDiag </p>
 * <p>Description: Bean que realiza la consulta de los diagnosticos susceptibles de ser mapeados automaticamente.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Jose Vicente Nieto-Marquez Fdez-Medina
 * @version 1.0
 */
public class ConsultaMapeosDiag {
    private long centro;
    private String cod_sescam = "";
    private String descrip = "";
    private String cod_mapeo = "";
    private String orden = "COD_SESCAM";
    private int resultCount;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 2000;

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de procedimientos susceptibles de ser mapeados automaticamente.
     * Utiliza objetos de la clase ProcMapeados para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos ProcMapeados, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase ProcMapeados.
     * @throws Exception
     */
    public Vector MapeosDiagnosticos() throws Exception {
        String sentWhere = "";
        String sentClausula = " WHERE ";

        // Validaci�n de parametros
        String sentBusqueda = new String("");
        sentBusqueda = " select * from MAPEO_DIAG ";

        if (centro > 0) {
            sentBusqueda = sentBusqueda + sentClausula + " centro = " + centro;
            sentClausula = " AND ";
        }

        if ((cod_sescam != null) && (!cod_sescam.equals(""))) {
            cod_sescam = "%" + cod_sescam + "%";
            sentBusqueda = sentBusqueda + sentClausula + " cod_sescam like '" +
                cod_sescam + "'";
            sentClausula = " AND ";
        }

        if ((descrip != null) && (!descrip.equals(""))) {
            descrip = "%" + descrip + "%";
            sentBusqueda = sentBusqueda + sentClausula + " descrip like '" +
                descrip + "'";
            sentClausula = " AND ";
        }

        if ((cod_mapeo != null) && (!cod_mapeo.equals(""))) {
            cod_mapeo = "%" + cod_mapeo + "%";
            sentBusqueda = sentBusqueda + sentClausula + " cod_mapeo like '" +
                cod_mapeo + "'";
            sentClausula = " AND ";
        }

        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_diag = new Vector();

        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection("db");

        try {
            conn.setAutoCommit(false);
            sentBusqueda = sentBusqueda + " Order by " + orden;

            //    System.out.println(sentBusqueda);
            PreparedStatement st = conn.prepareStatement(sentBusqueda);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                DiagnosticosMapeados diag = new DiagnosticosMapeados();
                diag.CodDiagSESCAM = rs.getString(1);
                diag.DiagDesc = rs.getString(2);
                diag.CodDiagMapeado = rs.getString(3);
                diag.centro = rs.getLong(4);

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_diag.addElement(diag);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection("db", conn);
        }

        return vec_diag;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param centro :long, l_centro por el que filtramos la consulta.
     * @since 10/02/2003
     * @author JVN
     */
    public void setCentro(long l_centro) {
        if (l_centro > 0) {
            this.centro = l_centro;
        }
    }

    /**
     * Obtiene el parametro al atributo centro de la clase.
     * @return long, centro.
     * @since 10/02/2003
     * @author JVN
     */
    public long GetCentro() {
        return this.centro;
    }

    /**
     * Asigna el parametro al atributo cod_sescam de la clase.
     * @param cod_sescam :String, ccodigo por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setCodigoSescam(String ccodigo) {
        if (!ccodigo.equals("")) {
            this.cod_sescam = ccodigo;
        }
    }

    /**
     * Obtiene el parametro al atributo CodSescam de la clase.
     * @return String, cod_sescam.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetCodigoSescam() {
        return this.cod_sescam;
    }

    /**
     * Asigna el parametro al atributo descrip de la clase.
     * @param descrip :String, sdesc por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setDescripSescam(String sdesc) {
        if (!sdesc.equals("")) {
            this.descrip = sdesc;
        }
    }

    /**
     * Obtiene el parametro al atributo descrip de la clase.
     * @return String, descripcion del procedimiento.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetDescripSescam() {
        return this.descrip;
    }

    /**
     * Asigna el parametro al atributo cod_mapeo de la clase.
     * @param cod_mapeo :String, ccodigo por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setCodigoMapeo(String ccodigo) {
        if (!ccodigo.equals("")) {
            this.cod_mapeo = ccodigo;
        }
    }

    /**
     * Obtiene el parametro al atributo CodMapeo de la clase.
     * @return String, cod_mapeo.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetCodigoMapeo() {
        return this.cod_mapeo;
    }

    /**
     * Asigna el parametro al atributo orden de la clase.
     * @param Orden :String, sorden Ordenacion que lleva los registros obtenidos por la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setOrden(String sorden) {
        if (!sorden.equals("")) {
            this.orden = sorden;
        }
    }

    /**
     * Obtiene el parametro al atributo Orden de la clase.
     * @return String, Orden.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetOrden() {
        return this.orden;
    }

    /*

      public static void main(String args[]){
              ConsultaNodosProcedimientos cp=new ConsultaNodosProcedimientos();
              NodoProcedimiento np;
              Vector vp=null;
              cp.setCodigo("12");
              cp.setDesc("");
              cp.setPkPadre(3);
              try{
              vp=cp.resultado();
              }catch(Exception ex){}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: Centro /"+cp.getCodigo()+"/ - HICL /"+cp.GetPkPadre()+"/");
                      }
              else        {
                        Enumeration e = vp.elements();
                        while (e.hasMoreElements()) {
                            np=(NodoProcedimiento)e.nextElement();
                            System.out.println(np.NodoDesc);
                            }
                      }
              }
    */
}
