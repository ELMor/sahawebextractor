/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaServMapeados </p>
 * <p>Description: Bean que realiza la consulta de los servicios mapeados.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Felix Alberto Bravo Hermoso
 * @version 1.0
 */
public class ConsultaServMapeados {
    private long centro;
    private String cod_sescam = "";
    private String descrip = "";
    private String cod_mapeo = "";
    private String descripmapeo = "";
    private String orden = "COD_SESCAM";
    private int resultCount;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 2000;

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de Servicios mapeados.
     * Utiliza objetos de la clase ServMapeados para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos ServMapeados, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase ServMapeados.
     * @throws Exception
     */
    public Vector ServiciosMapeados() throws Exception {
        String sentWhere = "";
        String sentClausula = " WHERE ";

        // Validaci�n de parametros
        String sentBusqueda = new String("");
        sentBusqueda = " select * from mapeo_servs ";

        if (centro > 0) {
            sentBusqueda = sentBusqueda + sentClausula + " centro = " + centro;
            sentClausula = " AND ";
        }

        if ((cod_sescam != null) && (!cod_sescam.equals(""))) {
            cod_sescam = "%" + cod_sescam + "%";
            sentBusqueda = sentBusqueda + sentClausula + " cod_sescam like '" +
                cod_sescam + "'";
            sentClausula = " AND ";
        }

        if ((descrip != null) && (!descrip.equals(""))) {
            descrip = "%" + descrip + "%";
            sentBusqueda = sentBusqueda + sentClausula + " descrip like '" +
                descrip + "'";
            sentClausula = " AND ";
        }

        if ((cod_mapeo != null) && (!cod_mapeo.equals(""))) {
            cod_mapeo = "%" + cod_mapeo + "%";
            sentBusqueda = sentBusqueda + sentClausula + " cod_mapeo like '" +
                cod_mapeo + "'";
            sentClausula = " AND ";
        }

        if ((descripmapeo != null) && (!descripmapeo.equals(""))) {
            descripmapeo = "%" + descripmapeo + "%";
            sentBusqueda = sentBusqueda + sentClausula +
                " descripmapeo like '" + descripmapeo + "'";
        }

        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_Serv = new Vector();

        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            sentBusqueda = sentBusqueda + " Order by " + orden;

            PreparedStatement st = conn.prepareStatement(sentBusqueda);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                ServMapeados servmap = new ServMapeados();
                servmap.CodServSESCAM = rs.getString(1);
                servmap.DescServSESCAM = rs.getString(2);
                servmap.CodServMap = rs.getString(3);
                servmap.DescServMap = rs.getString(4);
                servmap.CodCentroMapeo = rs.getLong(5);

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_Serv.addElement(servmap);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_Serv;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param centro :long, l_centro por el que filtramos la consulta.
     * @since 10/02/2003
     * @author JVN
     */
    public void setCentro(long l_centro) {
        if (l_centro > 0) {
            this.centro = l_centro;
        }
    }

    /**
     * Obtiene el parametro al atributo centro de la clase.
     * @return long, centro.
     * @since 10/02/2003
     * @author JVN
     */
    public long GetCentro() {
        return this.centro;
    }

    /**
     * Obtiene el parametro al atributo CodSescam de la clase.
     * @return String, cod_sescam.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetCodigoSescam() {
        return this.cod_sescam;
    }

    /**
     * Obtiene el parametro al atributo descrip de la clase.
     * @return String, descripcion del procedimiento.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetDescripSescam() {
        return this.descrip;
    }

    /**
     * Obtiene el parametro al atributo CodMapeo de la clase.
     * @return String, cod_mapeo.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetCodigoMapeo() {
        return this.cod_mapeo;
    }

    /**
    * Asigna el parametro al atributo orden de la clase.
    * @param Orden :String, sorden Ordenacion que lleva los registros obtenidos por la consulta.
    * @since 17/02/2003
    * @author JVN
    */
    public void setOrden(String sorden) {
        if (!sorden.equals("")) {
            this.orden = sorden;
        }
    }

    /**
      * Obtiene el parametro al atributo Orden de la clase.
      * @return String, Orden.
      * @since 17/02/2003
      * @author JVN
      */
    public String GetOrden() {
        return this.orden;
    }

    public void setDescripmapeo(String descripmapeo) {
        this.descripmapeo = descripmapeo;
    }

    /**
     * Asigna el parametro al atributo cod_mapeo de la clase.
     * @param cod_mapeo :String, ccodigo por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setCod_mapeo(String cod_mapeo) {
        this.cod_mapeo = cod_mapeo;
    }

    /**
     * Asigna el parametro al atributo cod_sescam de la clase.
     * @param cod_sescam :String, ccodigo por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setCod_sescam(String cod_sescam) {
        this.cod_sescam = cod_sescam;
    }

    /**
     * Asigna el parametro al atributo descrip de la clase.
     * @param descrip :String, sdesc por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    /*
      public static void main(String args[]){
              ConsultaNodosProcedimientos cp=new ConsultaNodosProcedimientos();
              NodoProcedimiento np;
              Vector vp=null;
              cp.setCodigo("12");
              cp.setDesc("");
              cp.setPkPadre(3);
              try{
              vp=cp.resultado();
              }catch(Exception ex){}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: Centro /"+cp.getCodigo()+"/ - HICL /"+cp.GetPkPadre()+"/");
                      }
              else        {
                        Enumeration e = vp.elements();
                        while (e.hasMoreElements()) {
                            np=(NodoProcedimiento)e.nextElement();
                            System.out.println(np.NodoDesc);
                            }
                      }*/

    /***************** PRUEBA TRADUCCION MAPEO *********************/

    /*
    ConsultaMapeosProc cmp=new ConsultaMapeosProc();
    cmp.setCentro(4509);
    cmp.setCodigoMapeo("04.81");
    cmp.setCodtipoMapeo("Q");
    TraduccionProc tp = new TraduccionProc();
    try {
      tp = (TraduccionProc) cmp.Traducir();
    }catch (Exception e){}
    System.out.println("Codigo Sescam:"+tp.codigoSESCAM);
    System.out.println("Desc Sescam:"+tp.descSESCAM);
    System.out.println("Tipo SESCAM:"+tp.tipoSESCAM);
    System.out.println("Monto:"+tp.monto);

    }
    */
}
