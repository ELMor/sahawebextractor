/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.exceptions.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Enumeration;
import java.util.Vector;


/**
 * <p>Clase: ConsultaEventosFicha</p>
 * <p>Descripcion: Bean que realiza la consulta de datos para la ficha de Eventos</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Empresa: Soluziona </p>
 * @author Iv�n Enrique Caneri
 */
public class ConsultaEventosFicha {
    private long evto_pk = 0;
    private long sess_pk = 0;
    private int resultCount = 0;
    public final int MAX_RESULT_SET = 500; //Maximo n� de registros resultantes por consulta.

    /**
     * Metodo que se encarga de la construccion de la consulta, atendiendo a los filtros, y de su ejecucion.
     * Utiliza objetos de la clase VisualizEventos para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos VisualizEventos, en la que cada uno atender� a un evento obtenido
     * en la consulta.
     * @return Vector de objetos de la clase VisualizEventos
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @autor Iv�n Enrique Caneri
     */
    public Vector resultado() throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        boolean bWhere = false;
        Vector vec_eventos = new Vector();
        Pool ref_pool = null;

        // SE COMPRUEBAN LAS VARIABLES GUARDADAS DEL EVENTO EN CUESTION
        String sentBusqueda = "select 1  from evento_var evt where evto_pk = ? ";

        // BUSQUEDA SI TIENE VARIABLES ACTIVADAS
        String sentBusqueda1 = "select ";
        sentBusqueda1 += "s.syslogin,  ";
        sentBusqueda1 += "s.oficina_pk,  ";
        sentBusqueda1 += "o.OFICINA_NOMBRE, ";
        sentBusqueda1 += ("to_char(s.SESS_FECHA_INICIO,'" +
        "DD/MM/YYYY HH24:MI" + "') SESS_FECHA_INICIO, ");
        sentBusqueda1 += "s.sess_srv_rmt, ";
        sentBusqueda1 += "e.TPOEVTO_PK, ";
        sentBusqueda1 += "et.TPOEVTO_DESC, ";
        sentBusqueda1 += "v.EVTO_VAR_VALOR, ";
        sentBusqueda1 += "evt.TPOEVTO_VAR_DESC, ";
        sentBusqueda1 += "1 ";
        sentBusqueda1 += "from evento_session s, evento e, evento_var v, ses_oficina o , evento_tipo et,evento_tipo_var evt ";
        sentBusqueda1 += "where s.sess_pk=e.sess_pk and  ";
        sentBusqueda1 += "e.evto_pk=v.evto_pk and ";
        sentBusqueda1 += "o.oficina_pk=s.oficina_pk and ";
        sentBusqueda1 += "et.TPOEVTO_PK=e.TPOEVTO_PK and ";
        sentBusqueda1 += "evt.TPOEVTO_VAR_PK=v.TPOEVTO_VAR_PK and ";
        sentBusqueda1 += "e.EVTO_PK=? ";

        // BUSQUEDA SI NO TIENE VARIABLES ACTIVADAS
        String sentBusqueda2 = "select ";
        sentBusqueda2 += "s.syslogin, ";
        sentBusqueda2 += "s.oficina_pk, ";
        sentBusqueda2 += "o.OFICINA_NOMBRE,";
        sentBusqueda2 += "to_char(s.SESS_FECHA_INICIO,'DD/MM/YYYY HH24:MI') SESS_FECHA_INICIO, ";
        sentBusqueda2 += "s.sess_srv_rmt, ";
        sentBusqueda2 += "e.TPOEVTO_PK, ";
        sentBusqueda2 += "et.TPOEVTO_DESC, ";
        sentBusqueda2 += "null TPOEVTO_VAR_VALOR, ";
        sentBusqueda2 += "null TPOEVTO_VAR_DESC, ";
        sentBusqueda2 += "1 ";
        sentBusqueda2 += "from evento_session s, evento e, evento_var v, ses_oficina o , evento_tipo et ";
        sentBusqueda2 += " where s.sess_pk=e.sess_pk and  ";
        sentBusqueda2 += " e.evto_pk =v.evto_pk(+) and ";
        sentBusqueda2 += " o.oficina_pk=s.oficina_pk and ";
        sentBusqueda2 += " et.TPOEVTO_PK=e.TPOEVTO_PK and ";
        sentBusqueda2 += " e.EVTO_PK= ?";

        String sentBusqueda3 = "select ";
        sentBusqueda3 += "s.syslogin, ";
        sentBusqueda3 += "s.oficina_pk, ";
        sentBusqueda3 += "o.OFICINA_NOMBRE,";
        sentBusqueda3 += "to_char(s.SESS_FECHA_INICIO,'DD/MM/YYYY HH24:MI') SESS_FECHA_INICIO, ";
        sentBusqueda3 += "s.sess_srv_rmt, ";
        sentBusqueda3 += "et.TPOEVTO_PK, ";
        sentBusqueda3 += "et.TPOEVTO_DESC, ";
        sentBusqueda3 += "null TPOEVTO_VAR_VALOR, ";
        sentBusqueda3 += "null TPOEVTO_VAR_DESC, ";
        sentBusqueda3 += "s.sess_acc_cod ";
        sentBusqueda3 += "from evento_session s, ses_oficina o , evento_tipo et ";
        sentBusqueda3 += " where   o.oficina_pk=s.oficina_pk ";
        sentBusqueda3 += " and et.TPOEVTO_PK= 19  and ";
        sentBusqueda3 += " s.sess_pk= ?";

        //   System.out.println(sentBusqueda);
        // Si no tiene condiciones de busqueda no se hace la consulta
        sentBusqueda2 += " ORDER BY e.TPOEVTO_PK ";
        sentBusqueda1 += " ORDER BY e.TPOEVTO_PK, evt.TPOEVTO_VAR_PK ";
        sentBusqueda3 += " ORDER BY et.TPOEVTO_PK ";

        //   System.out.println(sentBusqueda);
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            PreparedStatement stm;
            ResultSet rs;

            if (this.evto_pk == 0) {
                sentBusqueda = sentBusqueda3;
                this.evto_pk = this.sess_pk;
            } else {
                stm = conn.prepareStatement(sentBusqueda);
                stm.setLong(1, this.evto_pk);
                rs = stm.executeQuery();
                sentBusqueda = sentBusqueda2;

                boolean salir = false;

                while ((rs.next()) && (salir == false)) {
                    salir = true;
                    sentBusqueda = sentBusqueda1;
                }
            }

            stm = conn.prepareStatement(sentBusqueda);
            stm.setLong(1, this.evto_pk);
            rs = stm.executeQuery();

            while (rs.next()) {
                EventosFicha evtofic = new EventosFicha();
                evtofic.fsyslogin = rs.getString(1);
                evtofic.fOficina_pk = rs.getLong(2);
                evtofic.fOficina_nombre = rs.getString(3);
                evtofic.fSess_fecha_inicio = rs.getString(4);
                evtofic.fSess_srv_rmt = rs.getString(5);
                evtofic.fTpoevtoO_pk = rs.getLong(6);
                evtofic.fTpoevto_desc = rs.getString(7);

                if (rs.getString(8) != null) {
                    evtofic.fEvto_var_valor = rs.getString(8);
                } else {
                    evtofic.fEvto_var_valor = "";
                }

                if (rs.getString(9) != null) {
                    evtofic.fTpoevto_var_desc = rs.getString(9);
                } else {
                    evtofic.fTpoevto_var_desc = "";
                }

                switch (rs.getInt(10)) {
                case 1:
                    evtofic.fSess_cod_acc = " Acceso concedido ";

                    break;

                case -5:
                    evtofic.fSess_cod_acc = " Password incorrecta ";

                    break;

                case -2:
                    evtofic.fSess_cod_acc = " Usuario no encontrado � no tiene clave definida en el sistema ";

                    break;

                case -3:
                    evtofic.fSess_cod_acc = " Usuario tiene la clave caducada ";

                    break;

                case -4:
                    evtofic.fSess_cod_acc = " Usuario No Activo ";

                    break;

                case -6:
                    evtofic.fSess_cod_acc = " Usuario bloqueado por acceso erroneo reiterado ";
                }

                if (resultCount == MAX_RESULT_SET) {
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.MAX_RESULTSET);
                    throw es;
                }

                vec_eventos.addElement(evtofic);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_eventos;
    }

    /**
     * Obtiene el valor del atributo evto_pk de la clase.
     * @return long, evto_pk.
     * @since 12/12/2002
     * @author IEC
     */
    public long getEvto_pk() {
        return evto_pk;
    }

    /**
     * Asigna el parametro al atributo evto_pk de la clase.
     * @param pEvto_pk : long, evto_pk
     * @since 12/12/2002
     * @author IEC
     */
    public void setEvto_pk(long pEvto_pk) {
        if (pEvto_pk != 0) {
            this.evto_pk = pEvto_pk;
        }
    }

    /**
     * Obtiene el valor del atributo sess_pk de la clase.
     * @return long, sess_pk.
     * @since 22/01/2003
     * @author JVN
     */
    public long getSess_pk() {
        return sess_pk;
    }

    /**
     * Asigna el parametro al atributo sess_pk de la clase.
     * @param pEvto_pk : long, sess_pk
     * @since 22/01/2003
     * @author JVN
     */
    public void setSess_pk(long psess_pk) {
        if (psess_pk != 0) {
            this.sess_pk = psess_pk;
        }
    }

    public int getResultCount() {
        return resultCount;
    }

    public static void main(String[] args) {
        ConsultaEventosFicha cps = new ConsultaEventosFicha();
        EventosFicha ef;
        Vector vp;
        cps.setEvto_pk(64);

        try {
            vp = cps.resultado();

            if (vp == null) {
                System.out.println("No se trajo nada");
            } else {
                Enumeration e = vp.elements();

                while (e.hasMoreElements()) {
                    ef = (EventosFicha) e.nextElement();
                    System.out.println(ef.fTpoevto_desc);
                }

                System.out.println("Cantidad de registros:" +
                    cps.getResultCount());
            }
        } catch (Exception e) {
            System.out.println("Error al ejecutar la Select.");
            e.printStackTrace();
        }
    }
}
