package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;

public class ficha_nueva_solicitud_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<HTML>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../css/estilo.css\">\r\n");
      java.util.HashMap beanParamListaSolic = null;
      synchronized (session) {
        beanParamListaSolic = (java.util.HashMap) pageContext.getAttribute("beanParamListaSolic", PageContext.SESSION_SCOPE);
        if (beanParamListaSolic == null){
          try {
            beanParamListaSolic = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanParamListaSolic", beanParamListaSolic, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/validarNif.js\">");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<BODY class=\"mainFondoInterno\">\r\n");
      out.write("<form name=\"solicitud\" action=\"guardarSolicitud.jsp\" target=\"oculto1\">\r\n");
      out.write("<a>");
      out.write("<b>");
      out.write("<u>FICHA NUEVA SOLICITUD");
      out.write("</u>");
      out.write("</b>");
      out.write("</a>\r\n");
      out.write("<br>\r\n");
      out.write("<br>\r\n");
      out.write("<FIELDSET >\r\n");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>RESPONSABLE LEGAL");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n   ");
      out.write("<tr>\r\n       ");
      out.write("<td class= \"texto\">Nombre:");
      out.write("</td>\r\n       ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"nombre\" class=\"cajatexto3\" maxlength = 255 value=\"\">");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n   ");
      out.write("<tr>\r\n       ");
      out.write("<td class= \"texto\">N.I.F:");
      out.write("</td>\r\n       ");
      out.write("<td class= \"texto\">");
      out.write("<input maxlength=8 type=\"text\" name=\"dni\" class=\"cajatexto2\" style=\"width:12%\" value=\"\" onkeypress = \"if (((event.keyCode ");
      out.write("< 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;\" onBlur=\"CalculaNIF(this.value);\">\r\n        - ");
      out.write("<input maxlength=1 type=\"text\" name=\"letradni\" style=\"width:3%\" class=\"cajatexto2\" value=\"\" readonly>");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<br>\r\n");
      out.write("</FIELDSET>\r\n");
      out.write("<br>\r\n");
      out.write("<FIELDSET >\r\n");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>PACIENTE");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n   ");
      out.write("<tr>\r\n       ");
      out.write("<td class= \"texto\">N.S.S:");
      out.write("</td>\r\n       ");
      out.write("<td class= \"texto\">");
      out.write("<input align=\"right\" type=\"text\" name=\"nss\" class=\"cajatexto2\" maxlength=12 value=\"\" onkeypress = \"if ((event.keyCode ");
      out.write("< 48) || (event.keyCode > 57)) event.returnValue = false;\">");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<br>\r\n");
      out.write("</FIELDSET>\r\n");
      out.write("</form>\r\n");
      out.write("</BODY>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</HTML>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
